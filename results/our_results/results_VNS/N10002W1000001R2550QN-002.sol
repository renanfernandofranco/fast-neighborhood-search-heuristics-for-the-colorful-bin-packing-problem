Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 10002

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 393810 Color: 7861
Size: 339880 Color: 6056
Size: 266311 Color: 1842

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 385116 Color: 7609
Size: 342358 Color: 6167
Size: 272527 Color: 2319

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 401582 Color: 8074
Size: 339055 Color: 6020
Size: 259364 Color: 1192

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 394558 Color: 7889
Size: 340734 Color: 6104
Size: 264709 Color: 1697

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 375726 Color: 7313
Size: 349213 Color: 6422
Size: 275062 Color: 2507

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 406635 Color: 8199
Size: 336009 Color: 5891
Size: 257357 Color: 994

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 388858 Color: 7716
Size: 351457 Color: 6514
Size: 259686 Color: 1220

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 391427 Color: 7787
Size: 339865 Color: 6054
Size: 268709 Color: 2024

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 393230 Color: 7840
Size: 329388 Color: 5638
Size: 277383 Color: 2670

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 384764 Color: 7602
Size: 342445 Color: 6168
Size: 272792 Color: 2346

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380738 Color: 7469
Size: 334056 Color: 5823
Size: 285207 Color: 3233

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 379697 Color: 7442
Size: 345144 Color: 6270
Size: 275160 Color: 2515

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 378691 Color: 7411
Size: 362237 Color: 6868
Size: 259073 Color: 1162

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 382770 Color: 7533
Size: 350710 Color: 6484
Size: 266521 Color: 1858

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 399253 Color: 8007
Size: 330613 Color: 5686
Size: 270135 Color: 2147

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 374731 Color: 7281
Size: 349139 Color: 6420
Size: 276131 Color: 2585

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372091 Color: 7181
Size: 337469 Color: 5956
Size: 290441 Color: 3576

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 409859 Color: 8279
Size: 317904 Color: 5097
Size: 272238 Color: 2295

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 388882 Color: 7717
Size: 340700 Color: 6101
Size: 270419 Color: 2164

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 380678 Color: 7466
Size: 344034 Color: 6223
Size: 275289 Color: 2529

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 401093 Color: 8058
Size: 325234 Color: 5443
Size: 273674 Color: 2412

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 391282 Color: 7784
Size: 349865 Color: 6447
Size: 258854 Color: 1144

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 394213 Color: 7874
Size: 344717 Color: 6247
Size: 261071 Color: 1360

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 394478 Color: 7886
Size: 344923 Color: 6259
Size: 260600 Color: 1307

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 379453 Color: 7436
Size: 344538 Color: 6241
Size: 276010 Color: 2574

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 363870 Color: 6914
Size: 360331 Color: 6815
Size: 275800 Color: 2565

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 374340 Color: 7267
Size: 372356 Color: 7190
Size: 253305 Color: 515

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 395804 Color: 7915
Size: 338210 Color: 5984
Size: 265987 Color: 1812

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 380377 Color: 7456
Size: 344027 Color: 6222
Size: 275597 Color: 2549

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 384457 Color: 7591
Size: 342509 Color: 6173
Size: 273035 Color: 2360

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 385987 Color: 7635
Size: 341788 Color: 6141
Size: 272226 Color: 2292

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 369962 Color: 7119
Size: 355644 Color: 6663
Size: 274395 Color: 2470

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 402186 Color: 8088
Size: 330615 Color: 5687
Size: 267200 Color: 1910

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 379155 Color: 7431
Size: 350807 Color: 6490
Size: 270039 Color: 2141

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 391749 Color: 7798
Size: 339736 Color: 6049
Size: 268516 Color: 2012

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 398727 Color: 7995
Size: 334608 Color: 5847
Size: 266666 Color: 1873

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 382376 Color: 7519
Size: 344018 Color: 6219
Size: 273607 Color: 2410

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 382184 Color: 7510
Size: 343646 Color: 6202
Size: 274171 Color: 2454

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 393171 Color: 7839
Size: 327482 Color: 5556
Size: 279348 Color: 2827

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 400705 Color: 8045
Size: 338816 Color: 6002
Size: 260480 Color: 1294

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 388732 Color: 7712
Size: 347136 Color: 6353
Size: 264133 Color: 1644

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 383627 Color: 7562
Size: 350556 Color: 6476
Size: 265818 Color: 1800

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 386404 Color: 7647
Size: 344219 Color: 6232
Size: 269378 Color: 2086

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 404405 Color: 8142
Size: 323585 Color: 5373
Size: 272011 Color: 2275

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 384823 Color: 7604
Size: 343662 Color: 6203
Size: 271516 Color: 2237

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 380761 Color: 7471
Size: 344089 Color: 6228
Size: 275151 Color: 2512

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 390817 Color: 7769
Size: 334583 Color: 5845
Size: 274601 Color: 2489

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 374884 Color: 7289
Size: 345331 Color: 6281
Size: 279786 Color: 2861

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 401891 Color: 8083
Size: 340574 Color: 6095
Size: 257536 Color: 1012

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 392006 Color: 7806
Size: 339679 Color: 6046
Size: 268316 Color: 2001

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 389497 Color: 7738
Size: 342255 Color: 6159
Size: 268249 Color: 1992

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 404382 Color: 8141
Size: 333959 Color: 5818
Size: 261660 Color: 1420

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 372744 Color: 7212
Size: 327429 Color: 5553
Size: 299828 Color: 4128

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 385174 Color: 7612
Size: 342290 Color: 6164
Size: 272537 Color: 2320

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 379375 Color: 7434
Size: 370066 Color: 7127
Size: 250560 Color: 105

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 390056 Color: 7746
Size: 340198 Color: 6077
Size: 269747 Color: 2118

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 398419 Color: 7985
Size: 329455 Color: 5643
Size: 272127 Color: 2285

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 409944 Color: 8282
Size: 319012 Color: 5152
Size: 271045 Color: 2215

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 374578 Color: 7277
Size: 356089 Color: 6681
Size: 269334 Color: 2079

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 345173 Color: 6272
Size: 344483 Color: 6238
Size: 310345 Color: 4697

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 387459 Color: 7674
Size: 341306 Color: 6120
Size: 271236 Color: 2224

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 381339 Color: 7486
Size: 342832 Color: 6184
Size: 275830 Color: 2568

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 365873 Color: 6978
Size: 360658 Color: 6826
Size: 273470 Color: 2399

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 385064 Color: 7607
Size: 342285 Color: 6163
Size: 272652 Color: 2333

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 385329 Color: 7616
Size: 350402 Color: 6469
Size: 264270 Color: 1661

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 404470 Color: 8144
Size: 341813 Color: 6143
Size: 253718 Color: 575

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 398766 Color: 7996
Size: 333708 Color: 5810
Size: 267527 Color: 1934

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 411498 Color: 8326
Size: 319522 Color: 5180
Size: 268981 Color: 2047

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 390497 Color: 7760
Size: 340109 Color: 6071
Size: 269395 Color: 2089

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 410603 Color: 8298
Size: 332437 Color: 5758
Size: 256961 Color: 952

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 389319 Color: 7734
Size: 339881 Color: 6057
Size: 270801 Color: 2193

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 389464 Color: 7737
Size: 340348 Color: 6085
Size: 270189 Color: 2150

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 404184 Color: 8138
Size: 321956 Color: 5287
Size: 273861 Color: 2427

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 404124 Color: 8136
Size: 318073 Color: 5108
Size: 277804 Color: 2706

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 390489 Color: 7759
Size: 338891 Color: 6007
Size: 270621 Color: 2176

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 386406 Color: 7648
Size: 314619 Color: 4937
Size: 298976 Color: 4083

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 408514 Color: 8242
Size: 315739 Color: 4993
Size: 275748 Color: 2559

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 342899 Color: 6186
Size: 340694 Color: 6100
Size: 316408 Color: 5022

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 393901 Color: 7862
Size: 337293 Color: 5952
Size: 268807 Color: 2034

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 380314 Color: 7454
Size: 344050 Color: 6226
Size: 275637 Color: 2552

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 398811 Color: 7997
Size: 337914 Color: 5976
Size: 263276 Color: 1574

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 399413 Color: 8011
Size: 338545 Color: 5993
Size: 262043 Color: 1451

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 390177 Color: 7751
Size: 356692 Color: 6701
Size: 253132 Color: 490

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 391991 Color: 7805
Size: 339404 Color: 6036
Size: 268606 Color: 2019

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 390401 Color: 7757
Size: 340130 Color: 6074
Size: 269470 Color: 2093

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 369659 Color: 7110
Size: 352579 Color: 6563
Size: 277763 Color: 2702

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 356710 Color: 6703
Size: 322347 Color: 5307
Size: 320944 Color: 5238

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 365281 Color: 6963
Size: 365233 Color: 6958
Size: 269487 Color: 2095

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 391229 Color: 7783
Size: 351894 Color: 6536
Size: 256878 Color: 940

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 400533 Color: 8039
Size: 338970 Color: 6012
Size: 260498 Color: 1295

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 395030 Color: 7901
Size: 339418 Color: 6039
Size: 265553 Color: 1777

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 374298 Color: 7264
Size: 372175 Color: 7184
Size: 253528 Color: 555

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 388604 Color: 7710
Size: 346027 Color: 6309
Size: 265370 Color: 1757

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 367125 Color: 7017
Size: 365102 Color: 6952
Size: 267774 Color: 1960

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 388023 Color: 7687
Size: 330643 Color: 5688
Size: 281335 Color: 2978

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 391299 Color: 7785
Size: 336241 Color: 5899
Size: 272461 Color: 2313

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 386215 Color: 7642
Size: 360164 Color: 6809
Size: 253622 Color: 569

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 369098 Color: 7087
Size: 366967 Color: 7010
Size: 263936 Color: 1631

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 389559 Color: 7739
Size: 344510 Color: 6239
Size: 265932 Color: 1807

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 389588 Color: 7740
Size: 346672 Color: 6336
Size: 263741 Color: 1615

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 385588 Color: 7624
Size: 341867 Color: 6146
Size: 272546 Color: 2323

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 392721 Color: 7820
Size: 341907 Color: 6151
Size: 265373 Color: 1758

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 390144 Color: 7750
Size: 312972 Color: 4848
Size: 296885 Color: 3967

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 395731 Color: 7913
Size: 340068 Color: 6067
Size: 264202 Color: 1652

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 411793 Color: 8331
Size: 338207 Color: 5983
Size: 250001 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 394755 Color: 7893
Size: 331919 Color: 5732
Size: 273327 Color: 2382

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 411425 Color: 8322
Size: 336344 Color: 5901
Size: 252232 Color: 374

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 392552 Color: 7818
Size: 339412 Color: 6038
Size: 268037 Color: 1976

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 401145 Color: 8060
Size: 333921 Color: 5817
Size: 264935 Color: 1721

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 364629 Color: 6939
Size: 361325 Color: 6841
Size: 274047 Color: 2442

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 387061 Color: 7669
Size: 341393 Color: 6124
Size: 271547 Color: 2238

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 390063 Color: 7747
Size: 355238 Color: 6653
Size: 254700 Color: 711

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 411489 Color: 8325
Size: 323892 Color: 5394
Size: 264620 Color: 1691

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 374872 Color: 7287
Size: 352494 Color: 6554
Size: 272635 Color: 2331

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 394771 Color: 7895
Size: 343021 Color: 6187
Size: 262209 Color: 1471

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 411181 Color: 8314
Size: 338705 Color: 6000
Size: 250115 Color: 19

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 411211 Color: 8316
Size: 337009 Color: 5937
Size: 251781 Color: 306

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 411331 Color: 8319
Size: 322727 Color: 5329
Size: 265943 Color: 1808

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 407479 Color: 8215
Size: 341164 Color: 6116
Size: 251358 Color: 251

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 411217 Color: 8317
Size: 313711 Color: 4887
Size: 275073 Color: 2508

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 411580 Color: 8327
Size: 334937 Color: 5863
Size: 253484 Color: 545

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 394888 Color: 7898
Size: 323798 Color: 5382
Size: 281315 Color: 2976

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 394767 Color: 7894
Size: 338461 Color: 5989
Size: 266773 Color: 1878

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410078 Color: 8285
Size: 338918 Color: 6009
Size: 251005 Color: 190

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 411329 Color: 8318
Size: 330707 Color: 5689
Size: 257965 Color: 1052

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 410903 Color: 8307
Size: 339008 Color: 6016
Size: 250090 Color: 16

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 410994 Color: 8310
Size: 327216 Color: 5544
Size: 261791 Color: 1434

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 410990 Color: 8309
Size: 322892 Color: 5337
Size: 266119 Color: 1825

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 374183 Color: 7259
Size: 346776 Color: 6339
Size: 279042 Color: 2800

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 390612 Color: 7764
Size: 337144 Color: 5944
Size: 272245 Color: 2296

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 375956 Color: 7321
Size: 370122 Color: 7128
Size: 253923 Color: 599

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 410523 Color: 8297
Size: 337907 Color: 5975
Size: 251571 Color: 275

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 379208 Color: 7432
Size: 332551 Color: 5761
Size: 288242 Color: 3439

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 366429 Color: 6992
Size: 362791 Color: 6884
Size: 270781 Color: 2192

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 410788 Color: 8304
Size: 338888 Color: 6006
Size: 250325 Color: 59

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 411472 Color: 8324
Size: 308536 Color: 4596
Size: 279993 Color: 2878

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 410362 Color: 8291
Size: 326352 Color: 5496
Size: 263287 Color: 1576

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 409887 Color: 8280
Size: 317451 Color: 5078
Size: 272663 Color: 2335

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 409365 Color: 8266
Size: 299610 Color: 4114
Size: 291026 Color: 3606

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 411364 Color: 8320
Size: 337639 Color: 5965
Size: 250998 Color: 188

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 409560 Color: 8270
Size: 330740 Color: 5692
Size: 259701 Color: 1223

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 411126 Color: 8312
Size: 326688 Color: 5517
Size: 262187 Color: 1469

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 409827 Color: 8278
Size: 331330 Color: 5708
Size: 258844 Color: 1142

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 403724 Color: 8130
Size: 338111 Color: 5982
Size: 258166 Color: 1068

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 406818 Color: 8204
Size: 338488 Color: 5990
Size: 254695 Color: 710

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 393730 Color: 7858
Size: 325650 Color: 5468
Size: 280621 Color: 2927

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 409649 Color: 8274
Size: 339891 Color: 6060
Size: 250461 Color: 82

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 409252 Color: 8264
Size: 337452 Color: 5955
Size: 253297 Color: 514

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 410027 Color: 8283
Size: 339292 Color: 6032
Size: 250682 Color: 129

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 409366 Color: 8268
Size: 320504 Color: 5216
Size: 270131 Color: 2146

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 391098 Color: 7778
Size: 346385 Color: 6324
Size: 262518 Color: 1513

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 410091 Color: 8286
Size: 336576 Color: 5910
Size: 253334 Color: 516

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 402478 Color: 8097
Size: 325292 Color: 5449
Size: 272231 Color: 2294

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 411147 Color: 8313
Size: 328833 Color: 5611
Size: 260021 Color: 1247

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 406653 Color: 8200
Size: 340004 Color: 6064
Size: 253344 Color: 518

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 406508 Color: 8195
Size: 340054 Color: 6066
Size: 253439 Color: 532

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 409121 Color: 8260
Size: 332088 Color: 5738
Size: 258792 Color: 1136

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 409366 Color: 8267
Size: 325806 Color: 5477
Size: 264829 Color: 1710

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 411097 Color: 8311
Size: 335305 Color: 5872
Size: 253599 Color: 566

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 406520 Color: 8196
Size: 337589 Color: 5963
Size: 255892 Color: 834

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 406465 Color: 8194
Size: 340117 Color: 6072
Size: 253419 Color: 529

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 407841 Color: 8223
Size: 338235 Color: 5985
Size: 253925 Color: 600

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 409053 Color: 8256
Size: 338047 Color: 5980
Size: 252901 Color: 463

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 410457 Color: 8295
Size: 336177 Color: 5893
Size: 253367 Color: 520

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 410346 Color: 8290
Size: 332088 Color: 5739
Size: 257567 Color: 1015

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 408832 Color: 8251
Size: 321606 Color: 5272
Size: 269563 Color: 2108

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 400275 Color: 8034
Size: 346237 Color: 6315
Size: 253489 Color: 546

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 407952 Color: 8227
Size: 340102 Color: 6070
Size: 251947 Color: 330

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 369040 Color: 7085
Size: 359395 Color: 6785
Size: 271566 Color: 2240

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 389232 Color: 7727
Size: 345681 Color: 6291
Size: 265088 Color: 1734

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 408806 Color: 8250
Size: 337107 Color: 5941
Size: 254088 Color: 621

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 408350 Color: 8238
Size: 337047 Color: 5939
Size: 254604 Color: 691

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 408054 Color: 8229
Size: 337522 Color: 5959
Size: 254425 Color: 668

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 408247 Color: 8236
Size: 339865 Color: 6055
Size: 251889 Color: 322

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 408560 Color: 8244
Size: 336920 Color: 5933
Size: 254521 Color: 677

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 408198 Color: 8234
Size: 337249 Color: 5948
Size: 254554 Color: 681

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 408236 Color: 8235
Size: 334120 Color: 5826
Size: 257645 Color: 1023

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 408150 Color: 8232
Size: 336867 Color: 5928
Size: 254984 Color: 742

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 408129 Color: 8231
Size: 337359 Color: 5953
Size: 254513 Color: 676

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 405760 Color: 8177
Size: 334882 Color: 5859
Size: 259359 Color: 1191

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 348379 Color: 6399
Size: 326569 Color: 5510
Size: 325053 Color: 5439

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 407931 Color: 8225
Size: 337026 Color: 5938
Size: 255044 Color: 749

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 404729 Color: 8152
Size: 342262 Color: 6162
Size: 253010 Color: 477

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 408678 Color: 8245
Size: 336838 Color: 5925
Size: 254485 Color: 672

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 408488 Color: 8240
Size: 336789 Color: 5922
Size: 254724 Color: 715

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 403964 Color: 8134
Size: 333897 Color: 5816
Size: 262140 Color: 1467

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 409367 Color: 8269
Size: 338807 Color: 6001
Size: 251827 Color: 311

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 407645 Color: 8219
Size: 337445 Color: 5954
Size: 254911 Color: 736

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 402832 Color: 8109
Size: 336662 Color: 5912
Size: 260507 Color: 1297

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 409567 Color: 8271
Size: 338373 Color: 5986
Size: 252061 Color: 344

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 409040 Color: 8255
Size: 338918 Color: 6010
Size: 252043 Color: 342

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 407488 Color: 8216
Size: 338378 Color: 5987
Size: 254135 Color: 631

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 409238 Color: 8263
Size: 338674 Color: 5999
Size: 252089 Color: 347

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 406901 Color: 8205
Size: 337071 Color: 5940
Size: 256029 Color: 848

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 409347 Color: 8265
Size: 339887 Color: 6059
Size: 250767 Color: 148

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 401853 Color: 8082
Size: 334270 Color: 5834
Size: 263878 Color: 1624

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 402284 Color: 8091
Size: 334531 Color: 5843
Size: 263186 Color: 1562

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 406756 Color: 8203
Size: 340376 Color: 6087
Size: 252869 Color: 457

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 406728 Color: 8202
Size: 337107 Color: 5942
Size: 256166 Color: 864

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 406599 Color: 8198
Size: 339111 Color: 6024
Size: 254291 Color: 650

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 403489 Color: 8124
Size: 341201 Color: 6118
Size: 255311 Color: 770

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 409035 Color: 8254
Size: 334451 Color: 5841
Size: 256515 Color: 898

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 7578
Size: 364873 Color: 6945
Size: 251070 Color: 201

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 375724 Color: 7312
Size: 338525 Color: 5992
Size: 285752 Color: 3269

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 395206 Color: 7902
Size: 347894 Color: 6381
Size: 256901 Color: 943

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 400069 Color: 8029
Size: 347453 Color: 6363
Size: 252479 Color: 413

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 398908 Color: 8001
Size: 340569 Color: 6094
Size: 260524 Color: 1300

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 405992 Color: 8184
Size: 342642 Color: 6179
Size: 251367 Color: 253

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 402136 Color: 8086
Size: 337264 Color: 5949
Size: 260601 Color: 1308

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 407700 Color: 8220
Size: 336685 Color: 5916
Size: 255616 Color: 809

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 408747 Color: 8249
Size: 334166 Color: 5829
Size: 257088 Color: 966

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 405622 Color: 8175
Size: 341682 Color: 6136
Size: 252697 Color: 439

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 405592 Color: 8174
Size: 333475 Color: 5806
Size: 260934 Color: 1350

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 404695 Color: 8150
Size: 333237 Color: 5794
Size: 262069 Color: 1455

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 408098 Color: 8230
Size: 334086 Color: 5825
Size: 257817 Color: 1040

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 388230 Color: 7692
Size: 335099 Color: 5868
Size: 276672 Color: 2629

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 404826 Color: 8158
Size: 336357 Color: 5904
Size: 258818 Color: 1138

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 378441 Color: 7403
Size: 358182 Color: 6751
Size: 263378 Color: 1585

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 404685 Color: 8149
Size: 335354 Color: 5874
Size: 259962 Color: 1240

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 405184 Color: 8165
Size: 342162 Color: 6157
Size: 252655 Color: 434

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 400869 Color: 8052
Size: 341698 Color: 6138
Size: 257434 Color: 1000

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 404166 Color: 8137
Size: 335397 Color: 5875
Size: 260438 Color: 1288

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 405297 Color: 8170
Size: 333619 Color: 5808
Size: 261085 Color: 1361

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 405280 Color: 8169
Size: 341809 Color: 6142
Size: 252912 Color: 465

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 407736 Color: 8222
Size: 337973 Color: 5977
Size: 254292 Color: 651

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 404984 Color: 8160
Size: 345011 Color: 6265
Size: 250006 Color: 3

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 405268 Color: 8168
Size: 337546 Color: 5961
Size: 257187 Color: 978

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 407897 Color: 8224
Size: 329565 Color: 5647
Size: 262539 Color: 1515

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 407725 Color: 8221
Size: 336926 Color: 5934
Size: 255350 Color: 778

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 406654 Color: 8201
Size: 332184 Color: 5742
Size: 261163 Color: 1374

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 407113 Color: 8212
Size: 332805 Color: 5772
Size: 260083 Color: 1256

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 403680 Color: 8128
Size: 337498 Color: 5958
Size: 258823 Color: 1139

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 407075 Color: 8211
Size: 332780 Color: 5771
Size: 260146 Color: 1262

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 401417 Color: 8065
Size: 332624 Color: 5763
Size: 265960 Color: 1810

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 403264 Color: 8117
Size: 333219 Color: 5793
Size: 263518 Color: 1595

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 406911 Color: 8206
Size: 333677 Color: 5809
Size: 259413 Color: 1194

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 403451 Color: 8123
Size: 329867 Color: 5663
Size: 266683 Color: 1874

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 403351 Color: 8120
Size: 340275 Color: 6083
Size: 256375 Color: 881

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 385937 Color: 7632
Size: 336175 Color: 5892
Size: 277889 Color: 2717

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 410851 Color: 8306
Size: 334780 Color: 5856
Size: 254370 Color: 661

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 407009 Color: 8209
Size: 332364 Color: 5755
Size: 260628 Color: 1314

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 403173 Color: 8114
Size: 326406 Color: 5503
Size: 270422 Color: 2165

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 403031 Color: 8113
Size: 343365 Color: 6196
Size: 253605 Color: 567

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 402757 Color: 8104
Size: 332175 Color: 5741
Size: 265069 Color: 1731

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 402902 Color: 8111
Size: 332333 Color: 5749
Size: 264766 Color: 1706

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 402770 Color: 8105
Size: 332385 Color: 5756
Size: 264846 Color: 1713

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 401530 Color: 8072
Size: 332264 Color: 5748
Size: 266207 Color: 1833

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 402648 Color: 8101
Size: 335337 Color: 5873
Size: 262016 Color: 1448

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 406933 Color: 8207
Size: 341880 Color: 6148
Size: 251188 Color: 225

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 402784 Color: 8108
Size: 334976 Color: 5864
Size: 262241 Color: 1476

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 8085
Size: 331635 Color: 5720
Size: 266259 Color: 1837

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 402550 Color: 8099
Size: 336239 Color: 5898
Size: 261212 Color: 1382

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 403623 Color: 8127
Size: 338623 Color: 5996
Size: 257755 Color: 1034

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 402428 Color: 8095
Size: 332030 Color: 5736
Size: 265543 Color: 1776

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 393755 Color: 7859
Size: 345556 Color: 6290
Size: 260690 Color: 1323

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 401910 Color: 8084
Size: 338969 Color: 6011
Size: 259122 Color: 1168

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 406380 Color: 8190
Size: 331520 Color: 5714
Size: 262101 Color: 1463

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 402405 Color: 8093
Size: 346612 Color: 6334
Size: 250984 Color: 183

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 406980 Color: 8208
Size: 331830 Color: 5730
Size: 261191 Color: 1378

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 401734 Color: 8079
Size: 345375 Color: 6282
Size: 252892 Color: 462

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 383147 Color: 7545
Size: 354361 Color: 6620
Size: 262493 Color: 1507

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 385542 Color: 7623
Size: 352081 Color: 6542
Size: 262378 Color: 1487

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 401460 Color: 8066
Size: 346886 Color: 6343
Size: 251655 Color: 290

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 401597 Color: 8075
Size: 345882 Color: 6305
Size: 252522 Color: 418

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 401516 Color: 8070
Size: 344115 Color: 6230
Size: 254370 Color: 662

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 404810 Color: 8156
Size: 332705 Color: 5766
Size: 262486 Color: 1506

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 407595 Color: 8217
Size: 331224 Color: 5703
Size: 261182 Color: 1376

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 401108 Color: 8059
Size: 344336 Color: 6233
Size: 254557 Color: 682

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 401353 Color: 8064
Size: 330834 Color: 5695
Size: 267814 Color: 1963

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 393611 Color: 7854
Size: 354831 Color: 6638
Size: 251559 Color: 273

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 404611 Color: 8147
Size: 329931 Color: 5664
Size: 265459 Color: 1770

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 402150 Color: 8087
Size: 330253 Color: 5675
Size: 267598 Color: 1945

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 366454 Color: 6995
Size: 361644 Color: 6855
Size: 271903 Color: 2270

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 400954 Color: 8056
Size: 347152 Color: 6354
Size: 251895 Color: 324

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 400808 Color: 8048
Size: 346454 Color: 6327
Size: 252739 Color: 443

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 403981 Color: 8135
Size: 330562 Color: 5685
Size: 265458 Color: 1769

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 400789 Color: 8047
Size: 348428 Color: 6402
Size: 250784 Color: 150

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 401495 Color: 8069
Size: 346649 Color: 6335
Size: 251857 Color: 316

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 405247 Color: 8166
Size: 331630 Color: 5719
Size: 263124 Color: 1555

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 398885 Color: 8000
Size: 342614 Color: 6177
Size: 258502 Color: 1110

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 406308 Color: 8188
Size: 339015 Color: 6017
Size: 254678 Color: 701

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 402605 Color: 8100
Size: 325007 Color: 5435
Size: 272389 Color: 2308

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 400303 Color: 8035
Size: 343681 Color: 6204
Size: 256017 Color: 846

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 405972 Color: 8182
Size: 337627 Color: 5964
Size: 256402 Color: 883

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 401335 Color: 8063
Size: 346329 Color: 6319
Size: 252337 Color: 393

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 385655 Color: 7625
Size: 352856 Color: 6571
Size: 261490 Color: 1408

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 400008 Color: 8023
Size: 339407 Color: 6037
Size: 260586 Color: 1306

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 400434 Color: 8038
Size: 340626 Color: 6098
Size: 258941 Color: 1151

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 404564 Color: 8146
Size: 334297 Color: 5836
Size: 261140 Color: 1370

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 370371 Color: 7134
Size: 364182 Color: 6924
Size: 265448 Color: 1765

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 409944 Color: 8281
Size: 329607 Color: 5652
Size: 260450 Color: 1289

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 393345 Color: 7842
Size: 353856 Color: 6609
Size: 252800 Color: 447

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 399547 Color: 8015
Size: 350447 Color: 6473
Size: 250007 Color: 4

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 405817 Color: 8179
Size: 337747 Color: 5969
Size: 256437 Color: 887

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 406038 Color: 8185
Size: 329448 Color: 5642
Size: 264515 Color: 1685

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 399461 Color: 8014
Size: 330007 Color: 5667
Size: 270533 Color: 2172

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 405909 Color: 8180
Size: 329214 Color: 5630
Size: 264878 Color: 1717

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 399348 Color: 8009
Size: 329220 Color: 5632
Size: 271433 Color: 2234

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 376180 Color: 7328
Size: 370694 Color: 7147
Size: 253127 Color: 489

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 400696 Color: 8044
Size: 335659 Color: 5882
Size: 263646 Color: 1609

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 405565 Color: 8173
Size: 339236 Color: 6030
Size: 255200 Color: 760

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 399020 Color: 8002
Size: 343213 Color: 6192
Size: 257768 Color: 1037

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 399574 Color: 8017
Size: 335670 Color: 5883
Size: 264757 Color: 1704

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 399048 Color: 8004
Size: 350582 Color: 6477
Size: 250371 Color: 68

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 398682 Color: 7994
Size: 328908 Color: 5619
Size: 272411 Color: 2309

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 367704 Color: 7038
Size: 359500 Color: 6787
Size: 272797 Color: 2347

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 376568 Color: 7345
Size: 371807 Color: 7172
Size: 251626 Color: 285

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 376975 Color: 7355
Size: 372020 Color: 7178
Size: 251006 Color: 191

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 402657 Color: 8102
Size: 336227 Color: 5896
Size: 261117 Color: 1368

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 356802 Color: 6707
Size: 321671 Color: 5273
Size: 321528 Color: 5266

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 379039 Color: 7424
Size: 328531 Color: 5598
Size: 292431 Color: 3709

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 7080
Size: 364233 Color: 6925
Size: 266911 Color: 1888

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 380996 Color: 7478
Size: 350921 Color: 6494
Size: 268084 Color: 1981

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 368195 Color: 7052
Size: 355306 Color: 6656
Size: 276500 Color: 2617

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 372472 Color: 7199
Size: 356480 Color: 6698
Size: 271049 Color: 2216

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 376179 Color: 7327
Size: 349462 Color: 6432
Size: 274360 Color: 2468

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 375822 Color: 7317
Size: 343575 Color: 6200
Size: 280604 Color: 2920

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 375933 Color: 7320
Size: 364372 Color: 6930
Size: 259696 Color: 1221

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 389604 Color: 7742
Size: 343049 Color: 6189
Size: 267348 Color: 1919

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 375785 Color: 7314
Size: 352499 Color: 6555
Size: 271717 Color: 2255

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 375895 Color: 7319
Size: 341878 Color: 6147
Size: 282228 Color: 3046

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 374681 Color: 7280
Size: 328666 Color: 5604
Size: 296654 Color: 3955

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 368701 Color: 7074
Size: 351637 Color: 6523
Size: 279663 Color: 2851

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 400231 Color: 8032
Size: 327184 Color: 5541
Size: 272586 Color: 2327

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 390352 Color: 7756
Size: 333381 Color: 5800
Size: 276268 Color: 2598

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 355519 Color: 6661
Size: 323065 Color: 5344
Size: 321417 Color: 5258

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 391312 Color: 7786
Size: 328979 Color: 5622
Size: 279710 Color: 2854

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 372027 Color: 7179
Size: 347756 Color: 6376
Size: 280218 Color: 2891

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 374479 Color: 7272
Size: 374467 Color: 7270
Size: 251055 Color: 196

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 375401 Color: 7307
Size: 374351 Color: 7268
Size: 250249 Color: 48

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 374737 Color: 7282
Size: 373855 Color: 7251
Size: 251409 Color: 255

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 374099 Color: 7258
Size: 344829 Color: 6254
Size: 281073 Color: 2956

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 375479 Color: 7308
Size: 337882 Color: 5974
Size: 286640 Color: 3334

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 374186 Color: 7261
Size: 325638 Color: 5466
Size: 300177 Color: 4154

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 403917 Color: 8133
Size: 342009 Color: 6154
Size: 254075 Color: 620

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 411738 Color: 8329
Size: 301660 Color: 4243
Size: 286603 Color: 3332

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 410763 Color: 8302
Size: 336937 Color: 5935
Size: 252301 Color: 387

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 373717 Color: 7247
Size: 325578 Color: 5462
Size: 300706 Color: 4189

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 367556 Color: 7031
Size: 350916 Color: 6493
Size: 281529 Color: 2998

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 411202 Color: 8315
Size: 300655 Color: 4184
Size: 288144 Color: 3433

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 372733 Color: 7210
Size: 345125 Color: 6269
Size: 282143 Color: 3041

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 372738 Color: 7211
Size: 345237 Color: 6276
Size: 282026 Color: 3034

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 372708 Color: 7209
Size: 372650 Color: 7207
Size: 254643 Color: 694

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 372673 Color: 7208
Size: 333019 Color: 5784
Size: 294309 Color: 3820

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 366747 Color: 7006
Size: 358146 Color: 6750
Size: 275108 Color: 2510

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 372370 Color: 7191
Size: 343858 Color: 6211
Size: 283773 Color: 3145

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 372874 Color: 7216
Size: 369508 Color: 7101
Size: 257619 Color: 1018

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 398452 Color: 7987
Size: 317693 Color: 5088
Size: 283856 Color: 3152

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 372418 Color: 7195
Size: 323707 Color: 5377
Size: 303876 Color: 4348

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 372428 Color: 7196
Size: 344602 Color: 6242
Size: 282971 Color: 3090

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 367990 Color: 7047
Size: 350758 Color: 6487
Size: 281253 Color: 2971

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 390178 Color: 7752
Size: 342255 Color: 6158
Size: 267568 Color: 1940

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 376226 Color: 7332
Size: 372589 Color: 7202
Size: 251186 Color: 224

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 372415 Color: 7194
Size: 371271 Color: 7160
Size: 256315 Color: 875

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 373710 Color: 7246
Size: 317722 Color: 5089
Size: 308569 Color: 4599

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 376524 Color: 7344
Size: 333718 Color: 5812
Size: 289759 Color: 3537

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 371921 Color: 7174
Size: 322815 Color: 5333
Size: 305265 Color: 4426

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 375655 Color: 7311
Size: 371944 Color: 7175
Size: 252402 Color: 400

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 371823 Color: 7173
Size: 343889 Color: 6213
Size: 284289 Color: 3176

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 402492 Color: 8098
Size: 306746 Color: 4509
Size: 290763 Color: 3596

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 365562 Color: 6973
Size: 362426 Color: 6874
Size: 272013 Color: 2276

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 370469 Color: 7138
Size: 344691 Color: 6246
Size: 284841 Color: 3206

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 371246 Color: 7158
Size: 343361 Color: 6195
Size: 285394 Color: 3248

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 408688 Color: 8247
Size: 304760 Color: 4390
Size: 286553 Color: 3329

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 370632 Color: 7145
Size: 343110 Color: 6190
Size: 286259 Color: 3303

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 376699 Color: 7349
Size: 370340 Color: 7133
Size: 252962 Color: 473

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 370696 Color: 7148
Size: 343266 Color: 6193
Size: 286039 Color: 3290

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 370756 Color: 7149
Size: 321506 Color: 5264
Size: 307739 Color: 4553

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 370950 Color: 7154
Size: 368244 Color: 7055
Size: 260807 Color: 1336

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 370823 Color: 7151
Size: 369327 Color: 7092
Size: 259851 Color: 1231

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 370791 Color: 7150
Size: 320813 Color: 5231
Size: 308397 Color: 4587

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 370426 Color: 7136
Size: 319208 Color: 5161
Size: 310367 Color: 4700

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 381595 Color: 7493
Size: 363748 Color: 6911
Size: 254658 Color: 697

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 370417 Color: 7135
Size: 320893 Color: 5237
Size: 308691 Color: 4606

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 370539 Color: 7139
Size: 356038 Color: 6679
Size: 273424 Color: 2392

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 366449 Color: 6994
Size: 319231 Color: 5165
Size: 314321 Color: 4918

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 380762 Color: 7472
Size: 367989 Color: 7046
Size: 251250 Color: 235

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 379110 Color: 7429
Size: 369957 Color: 7117
Size: 250934 Color: 177

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 365474 Color: 6969
Size: 319781 Color: 5194
Size: 314746 Color: 4947

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 369950 Color: 7116
Size: 319850 Color: 5195
Size: 310201 Color: 4689

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 369750 Color: 7112
Size: 369545 Color: 7105
Size: 260706 Color: 1326

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 369937 Color: 7115
Size: 342261 Color: 6161
Size: 287803 Color: 3418

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 369660 Color: 7111
Size: 367068 Color: 7015
Size: 263273 Color: 1573

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 364954 Color: 6946
Size: 357006 Color: 6719
Size: 278041 Color: 2731

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 370238 Color: 7129
Size: 341520 Color: 6127
Size: 288243 Color: 3440

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 370317 Color: 7132
Size: 353727 Color: 6604
Size: 275957 Color: 2572

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 372755 Color: 7213
Size: 343773 Color: 6206
Size: 283473 Color: 3124

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 372835 Color: 7215
Size: 368760 Color: 7077
Size: 258406 Color: 1100

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 369027 Color: 7083
Size: 341321 Color: 6122
Size: 289653 Color: 3532

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 407639 Color: 8218
Size: 324535 Color: 5414
Size: 267827 Color: 1966

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 373787 Color: 7249
Size: 336755 Color: 5919
Size: 289459 Color: 3517

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 373031 Color: 7221
Size: 350903 Color: 6492
Size: 276067 Color: 2579

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 411832 Color: 8332
Size: 327757 Color: 5568
Size: 260412 Color: 1285

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 369437 Color: 7096
Size: 318650 Color: 5141
Size: 311914 Color: 4781

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 368603 Color: 7070
Size: 317296 Color: 5071
Size: 314102 Color: 4905

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 398489 Color: 7988
Size: 350351 Color: 6466
Size: 251161 Color: 217

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 364423 Color: 6932
Size: 345265 Color: 6278
Size: 290313 Color: 3570

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 368499 Color: 7068
Size: 368308 Color: 7060
Size: 263194 Color: 1564

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 369619 Color: 7109
Size: 339181 Color: 6027
Size: 291201 Color: 3620

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 368323 Color: 7061
Size: 316828 Color: 5045
Size: 314850 Color: 4951

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 343704 Color: 6205
Size: 340613 Color: 6097
Size: 315684 Color: 4990

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 367965 Color: 7045
Size: 340265 Color: 6082
Size: 291771 Color: 3658

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 366960 Color: 7009
Size: 318619 Color: 5138
Size: 314422 Color: 4926

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 403836 Color: 8132
Size: 306265 Color: 4481
Size: 289900 Color: 3544

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 367915 Color: 7044
Size: 316472 Color: 5027
Size: 315614 Color: 4988

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 367623 Color: 7032
Size: 317180 Color: 5065
Size: 315198 Color: 4968

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 343798 Color: 6208
Size: 339102 Color: 6023
Size: 317101 Color: 5060

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 367689 Color: 7037
Size: 317112 Color: 5061
Size: 315200 Color: 4969

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 373674 Color: 7245
Size: 333349 Color: 5798
Size: 292978 Color: 3752

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 378420 Color: 7401
Size: 368629 Color: 7071
Size: 252952 Color: 470

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 375971 Color: 7322
Size: 372900 Color: 7218
Size: 251130 Color: 213

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 367312 Color: 7024
Size: 339661 Color: 6044
Size: 293028 Color: 3754

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 367148 Color: 7018
Size: 354749 Color: 6635
Size: 278104 Color: 2733

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 366216 Color: 6984
Size: 321282 Color: 5254
Size: 312503 Color: 4817

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 367260 Color: 7021
Size: 318297 Color: 5119
Size: 314444 Color: 4928

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 351147 Color: 6503
Size: 336234 Color: 5897
Size: 312620 Color: 4825

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 409095 Color: 8259
Size: 310959 Color: 4727
Size: 279947 Color: 2871

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 369471 Color: 7099
Size: 366023 Color: 6980
Size: 264507 Color: 1684

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 366508 Color: 7000
Size: 361107 Color: 6834
Size: 272386 Color: 2307

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 346864 Color: 6342
Size: 332721 Color: 5768
Size: 320416 Color: 5211

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 367751 Color: 7040
Size: 358783 Color: 6768
Size: 273467 Color: 2398

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 365437 Color: 6968
Size: 322510 Color: 5317
Size: 312054 Color: 4790

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 386837 Color: 7662
Size: 323959 Color: 5399
Size: 289205 Color: 3500

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 365334 Color: 6964
Size: 323915 Color: 5397
Size: 310752 Color: 4721

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 365577 Color: 6974
Size: 337841 Color: 5972
Size: 296583 Color: 3954

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 365030 Color: 6951
Size: 322458 Color: 5314
Size: 312513 Color: 4819

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 365422 Color: 6967
Size: 323874 Color: 5391
Size: 310705 Color: 4719

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 365160 Color: 6954
Size: 354597 Color: 6630
Size: 280244 Color: 2893

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 358639 Color: 6766
Size: 321254 Color: 5252
Size: 320108 Color: 5203

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 376224 Color: 7330
Size: 364585 Color: 6937
Size: 259192 Color: 1180

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 374331 Color: 7266
Size: 364834 Color: 6942
Size: 260836 Color: 1342

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 383587 Color: 7561
Size: 326517 Color: 5508
Size: 289897 Color: 3543

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 368444 Color: 7066
Size: 331788 Color: 5727
Size: 299769 Color: 4123

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 348298 Color: 6394
Size: 341522 Color: 6128
Size: 310181 Color: 4688

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 364425 Color: 6933
Size: 325732 Color: 5473
Size: 309844 Color: 4667

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 371694 Color: 7170
Size: 347691 Color: 6372
Size: 280616 Color: 2926

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 360566 Color: 6820
Size: 359377 Color: 6784
Size: 280058 Color: 2882

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 349403 Color: 6431
Size: 341066 Color: 6113
Size: 309532 Color: 4651

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 364323 Color: 6926
Size: 326386 Color: 5501
Size: 309292 Color: 4641

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 364704 Color: 6940
Size: 326677 Color: 5516
Size: 308620 Color: 4604

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 337196 Color: 5945
Size: 334737 Color: 5854
Size: 328068 Color: 5580

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 410076 Color: 8284
Size: 332964 Color: 5783
Size: 256961 Color: 951

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 363245 Color: 6899
Size: 328718 Color: 5609
Size: 308038 Color: 4566

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 363627 Color: 6909
Size: 327854 Color: 5574
Size: 308520 Color: 4595

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 351764 Color: 6533
Size: 335457 Color: 5876
Size: 312780 Color: 4836

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 363970 Color: 6915
Size: 327652 Color: 5563
Size: 308379 Color: 4586

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 340950 Color: 6111
Size: 330147 Color: 5673
Size: 328904 Color: 5618

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 363067 Color: 6895
Size: 329170 Color: 5628
Size: 307764 Color: 4554

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 365138 Color: 6953
Size: 332928 Color: 5779
Size: 301935 Color: 4255

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 362899 Color: 6887
Size: 337818 Color: 5970
Size: 299284 Color: 4101

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 362939 Color: 6889
Size: 330054 Color: 5669
Size: 307008 Color: 4523

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 360299 Color: 6814
Size: 334565 Color: 5844
Size: 305137 Color: 4415

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 362700 Color: 6881
Size: 329466 Color: 5644
Size: 307835 Color: 4557

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 362871 Color: 6885
Size: 330405 Color: 5679
Size: 306725 Color: 4507

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 392402 Color: 7816
Size: 328742 Color: 5610
Size: 278857 Color: 2782

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 362436 Color: 6875
Size: 334001 Color: 5822
Size: 303564 Color: 4332

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 391005 Color: 7776
Size: 350950 Color: 6495
Size: 258046 Color: 1060

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 362358 Color: 6871
Size: 331829 Color: 5729
Size: 305814 Color: 4456

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 362320 Color: 6870
Size: 333879 Color: 5814
Size: 303802 Color: 4345

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 348823 Color: 6412
Size: 346091 Color: 6311
Size: 305087 Color: 4412

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 377880 Color: 7388
Size: 361914 Color: 6862
Size: 260207 Color: 1268

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 374513 Color: 7273
Size: 361666 Color: 6856
Size: 263822 Color: 1621

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 372123 Color: 7182
Size: 354466 Color: 6625
Size: 273412 Color: 2389

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 375398 Color: 7306
Size: 357297 Color: 6722
Size: 267306 Color: 1915

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 364962 Color: 6948
Size: 356814 Color: 6709
Size: 278225 Color: 2750

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 387831 Color: 7683
Size: 348234 Color: 6390
Size: 263936 Color: 1630

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 363109 Color: 6897
Size: 360773 Color: 6828
Size: 276119 Color: 2583

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 377096 Color: 7359
Size: 360637 Color: 6824
Size: 262268 Color: 1479

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 401747 Color: 8080
Size: 319575 Color: 5183
Size: 278679 Color: 2774

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 260305 Color: 1276
Size: 368338 Color: 7062
Size: 371358 Color: 7161

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 360230 Color: 6811
Size: 360165 Color: 6810
Size: 279606 Color: 2846

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 393782 Color: 7860
Size: 326579 Color: 5511
Size: 279640 Color: 2849

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 395533 Color: 7909
Size: 329840 Color: 5661
Size: 274628 Color: 2491

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 359615 Color: 6794
Size: 352116 Color: 6543
Size: 288270 Color: 3442

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 359736 Color: 6797
Size: 359562 Color: 6792
Size: 280703 Color: 2934

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 387270 Color: 7671
Size: 356211 Color: 6686
Size: 256520 Color: 900

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 403412 Color: 8121
Size: 306394 Color: 4488
Size: 290195 Color: 3567

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 359197 Color: 6782
Size: 359074 Color: 6777
Size: 281730 Color: 3015

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 369511 Color: 7102
Size: 348787 Color: 6411
Size: 281703 Color: 3013

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 400831 Color: 8050
Size: 316754 Color: 5040
Size: 282416 Color: 3059

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 358490 Color: 6759
Size: 358450 Color: 6758
Size: 283061 Color: 3095

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 358592 Color: 6762
Size: 358311 Color: 6753
Size: 283098 Color: 3099

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 404675 Color: 8148
Size: 317449 Color: 5077
Size: 277877 Color: 2715

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 358059 Color: 6746
Size: 357840 Color: 6740
Size: 284102 Color: 3167

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 403438 Color: 8122
Size: 327857 Color: 5575
Size: 268706 Color: 2023

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 369426 Color: 7095
Size: 357809 Color: 6738
Size: 272766 Color: 2343

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 360472 Color: 6819
Size: 355185 Color: 6649
Size: 284344 Color: 3179

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 371641 Color: 7168
Size: 356570 Color: 6699
Size: 271790 Color: 2258

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 357649 Color: 6730
Size: 332352 Color: 5752
Size: 310000 Color: 4677

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 393126 Color: 7837
Size: 330847 Color: 5696
Size: 276028 Color: 2577

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 360287 Color: 6813
Size: 359191 Color: 6781
Size: 280523 Color: 2915

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 356698 Color: 6702
Size: 355198 Color: 6651
Size: 288105 Color: 3431

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 356924 Color: 6714
Size: 356826 Color: 6710
Size: 286251 Color: 3302

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 365643 Color: 6975
Size: 353012 Color: 6580
Size: 281346 Color: 2980

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 377733 Color: 7386
Size: 356850 Color: 6711
Size: 265418 Color: 1761

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 356365 Color: 6695
Size: 356355 Color: 6694
Size: 287281 Color: 3376

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 356313 Color: 6690
Size: 356030 Color: 6678
Size: 287658 Color: 3409

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 356235 Color: 6687
Size: 356155 Color: 6684
Size: 287611 Color: 3405

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 390528 Color: 7761
Size: 355917 Color: 6674
Size: 253556 Color: 559

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 357820 Color: 6739
Size: 355857 Color: 6670
Size: 286324 Color: 3309

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 355833 Color: 6669
Size: 355826 Color: 6668
Size: 288342 Color: 3447

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 355823 Color: 6667
Size: 355810 Color: 6666
Size: 288368 Color: 3451

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 359557 Color: 6791
Size: 353668 Color: 6601
Size: 286776 Color: 3344

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 355412 Color: 6659
Size: 355327 Color: 6657
Size: 289262 Color: 3502

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 372562 Color: 7201
Size: 361155 Color: 6835
Size: 266284 Color: 1838

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 374953 Color: 7292
Size: 353192 Color: 6585
Size: 271856 Color: 2265

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 355075 Color: 6643
Size: 354984 Color: 6642
Size: 289942 Color: 3548

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 354746 Color: 6634
Size: 354700 Color: 6632
Size: 290555 Color: 3585

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 354710 Color: 6633
Size: 354572 Color: 6627
Size: 290719 Color: 3594

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 354683 Color: 6631
Size: 354588 Color: 6628
Size: 290730 Color: 3595

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 374459 Color: 7269
Size: 369610 Color: 7108
Size: 255932 Color: 838

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 379656 Color: 7441
Size: 353239 Color: 6587
Size: 267106 Color: 1900

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 356994 Color: 6718
Size: 351241 Color: 6506
Size: 291766 Color: 3657

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 392400 Color: 7815
Size: 335061 Color: 5866
Size: 272540 Color: 2322

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 354083 Color: 6617
Size: 353807 Color: 6606
Size: 292111 Color: 3686

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 392606 Color: 7819
Size: 333171 Color: 5791
Size: 274224 Color: 2457

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 353631 Color: 6599
Size: 353402 Color: 6594
Size: 292968 Color: 3751

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 353441 Color: 6597
Size: 353423 Color: 6595
Size: 293137 Color: 3758

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 362588 Color: 6878
Size: 342541 Color: 6175
Size: 294872 Color: 3856

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 353240 Color: 6588
Size: 337587 Color: 5962
Size: 309174 Color: 4634

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 355634 Color: 6662
Size: 352961 Color: 6576
Size: 291406 Color: 3637

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 352770 Color: 6568
Size: 352693 Color: 6566
Size: 294538 Color: 3837

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 385814 Color: 7629
Size: 333461 Color: 5804
Size: 280726 Color: 2936

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 352050 Color: 6541
Size: 352032 Color: 6540
Size: 295919 Color: 3912

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 366249 Color: 6985
Size: 352012 Color: 6539
Size: 281740 Color: 3017

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 372814 Color: 7214
Size: 357683 Color: 6732
Size: 269504 Color: 2097

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 350775 Color: 6489
Size: 332239 Color: 5745
Size: 316987 Color: 5053

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 377460 Color: 7378
Size: 344413 Color: 6236
Size: 278128 Color: 2736

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 367883 Color: 7043
Size: 356019 Color: 6677
Size: 276099 Color: 2582

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 350408 Color: 6470
Size: 347371 Color: 6361
Size: 302222 Color: 4266

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 350264 Color: 6463
Size: 350243 Color: 6462
Size: 299494 Color: 4108

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 349971 Color: 6450
Size: 349847 Color: 6446
Size: 300183 Color: 4155

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 367001 Color: 7013
Size: 332842 Color: 5775
Size: 300158 Color: 4151

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 349789 Color: 6445
Size: 349731 Color: 6444
Size: 300481 Color: 4169

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 362530 Color: 6877
Size: 336533 Color: 5909
Size: 300938 Color: 4206

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 398539 Color: 7989
Size: 335557 Color: 5879
Size: 265905 Color: 1803

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 386055 Color: 7637
Size: 314796 Color: 4948
Size: 299150 Color: 4094

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 350179 Color: 6459
Size: 349164 Color: 6421
Size: 300658 Color: 4185

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 410709 Color: 8300
Size: 301784 Color: 4250
Size: 287508 Color: 3400

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 350397 Color: 6468
Size: 348966 Color: 6413
Size: 300638 Color: 4182

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 351526 Color: 6520
Size: 346342 Color: 6320
Size: 302133 Color: 4262

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 391674 Color: 7794
Size: 342740 Color: 6183
Size: 265587 Color: 1779

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 399281 Color: 8008
Size: 309521 Color: 4650
Size: 291199 Color: 3619

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 351256 Color: 6507
Size: 348633 Color: 6408
Size: 300112 Color: 4147

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 351691 Color: 6528
Size: 351328 Color: 6510
Size: 296982 Color: 3973

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 352187 Color: 6547
Size: 351479 Color: 6516
Size: 296335 Color: 3938

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 366253 Color: 6986
Size: 355894 Color: 6672
Size: 277854 Color: 2713

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 357411 Color: 6725
Size: 346369 Color: 6322
Size: 296221 Color: 3929

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 361991 Color: 6863
Size: 358051 Color: 6745
Size: 279959 Color: 2872

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 357698 Color: 6733
Size: 346162 Color: 6314
Size: 296141 Color: 3923

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 356373 Color: 6696
Size: 351710 Color: 6531
Size: 291918 Color: 3670

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 375281 Color: 7304
Size: 332963 Color: 5782
Size: 291757 Color: 3656

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 381829 Color: 7499
Size: 338388 Color: 5988
Size: 279784 Color: 2860

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 350624 Color: 6479
Size: 346410 Color: 6325
Size: 302967 Color: 4304

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 406172 Color: 8187
Size: 315511 Color: 4982
Size: 278318 Color: 2753

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 348977 Color: 6414
Size: 338515 Color: 5991
Size: 312509 Color: 4818

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 379137 Color: 7430
Size: 331341 Color: 5710
Size: 289523 Color: 3524

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 373762 Color: 7248
Size: 367778 Color: 7041
Size: 258461 Color: 1107

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 364181 Color: 6923
Size: 348417 Color: 6401
Size: 287403 Color: 3391

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 364613 Color: 6938
Size: 348379 Color: 6398
Size: 287009 Color: 3357

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 348185 Color: 6388
Size: 348143 Color: 6387
Size: 303673 Color: 4338

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 348296 Color: 6393
Size: 347922 Color: 6382
Size: 303783 Color: 4342

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 392327 Color: 7813
Size: 347728 Color: 6374
Size: 259946 Color: 1238

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 347680 Color: 6371
Size: 347542 Color: 6364
Size: 304779 Color: 4393

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 347362 Color: 6360
Size: 347269 Color: 6356
Size: 305370 Color: 4437

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 7023
Size: 363199 Color: 6898
Size: 269511 Color: 2099

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 347040 Color: 6349
Size: 346908 Color: 6346
Size: 306053 Color: 4470

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 344649 Color: 6244
Size: 336961 Color: 5936
Size: 318391 Color: 5126

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 385145 Color: 7611
Size: 346562 Color: 6330
Size: 268294 Color: 1998

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 346813 Color: 6340
Size: 346580 Color: 6331
Size: 306608 Color: 4498

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 402774 Color: 8106
Size: 345507 Color: 6288
Size: 251720 Color: 298

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 373057 Color: 7223
Size: 318207 Color: 5112
Size: 308737 Color: 4610

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 373151 Color: 7225
Size: 315511 Color: 4983
Size: 311339 Color: 4746

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 373188 Color: 7226
Size: 314630 Color: 4938
Size: 312183 Color: 4799

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 373200 Color: 7227
Size: 314082 Color: 4902
Size: 312719 Color: 4831

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 373219 Color: 7228
Size: 323328 Color: 5356
Size: 303454 Color: 4327

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 373259 Color: 7229
Size: 314632 Color: 4939
Size: 312110 Color: 4793

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 373273 Color: 7230
Size: 316221 Color: 5013
Size: 310507 Color: 4705

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 373304 Color: 7231
Size: 319763 Color: 5192
Size: 306934 Color: 4518

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 373332 Color: 7232
Size: 314252 Color: 4912
Size: 312417 Color: 4809

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 373357 Color: 7233
Size: 313391 Color: 4872
Size: 313253 Color: 4863

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 373422 Color: 7234
Size: 316332 Color: 5018
Size: 310247 Color: 4691

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 373431 Color: 7235
Size: 318294 Color: 5117
Size: 308276 Color: 4582

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 373450 Color: 7236
Size: 314393 Color: 4923
Size: 312158 Color: 4796

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 373462 Color: 7238
Size: 323505 Color: 5368
Size: 303034 Color: 4309

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 373464 Color: 7239
Size: 315298 Color: 4974
Size: 311239 Color: 4743

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 373484 Color: 7240
Size: 319470 Color: 5176
Size: 307047 Color: 4525

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 373489 Color: 7241
Size: 321294 Color: 5255
Size: 305218 Color: 4421

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 373510 Color: 7242
Size: 318307 Color: 5120
Size: 308184 Color: 4577

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 373554 Color: 7243
Size: 317071 Color: 5059
Size: 309376 Color: 4645

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 373618 Color: 7244
Size: 319170 Color: 5160
Size: 307213 Color: 4529

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 374815 Color: 7284
Size: 326622 Color: 5513
Size: 298564 Color: 4063

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 374832 Color: 7285
Size: 316554 Color: 5030
Size: 308615 Color: 4603

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 374855 Color: 7286
Size: 313952 Color: 4896
Size: 311194 Color: 4740

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 374874 Color: 7288
Size: 323804 Color: 5383
Size: 301323 Color: 4229

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 374926 Color: 7291
Size: 345272 Color: 6279
Size: 279803 Color: 2865

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 375048 Color: 7294
Size: 325400 Color: 5455
Size: 299553 Color: 4110

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 375076 Color: 7295
Size: 326441 Color: 5504
Size: 298484 Color: 4060

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 375082 Color: 7296
Size: 313775 Color: 4890
Size: 311144 Color: 4737

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 375101 Color: 7297
Size: 320438 Color: 5213
Size: 304462 Color: 4377

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 375134 Color: 7298
Size: 321264 Color: 5253
Size: 303603 Color: 4335

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 375151 Color: 7299
Size: 323708 Color: 5378
Size: 301142 Color: 4215

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 375174 Color: 7300
Size: 324666 Color: 5424
Size: 300161 Color: 4152

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 375227 Color: 7301
Size: 314690 Color: 4943
Size: 310084 Color: 4680

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 375251 Color: 7302
Size: 314298 Color: 4914
Size: 310452 Color: 4702

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 376323 Color: 7334
Size: 313048 Color: 4853
Size: 310630 Color: 4718

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 376338 Color: 7335
Size: 312224 Color: 4801
Size: 311439 Color: 4753

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 376342 Color: 7336
Size: 320086 Color: 5202
Size: 303573 Color: 4334

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 376395 Color: 7337
Size: 322092 Color: 5296
Size: 301514 Color: 4236

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 376402 Color: 7338
Size: 325891 Color: 5479
Size: 297708 Color: 4013

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 376402 Color: 7339
Size: 318155 Color: 5109
Size: 305444 Color: 4439

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 376437 Color: 7340
Size: 314670 Color: 4942
Size: 308894 Color: 4617

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 376470 Color: 7341
Size: 322702 Color: 5328
Size: 300829 Color: 4198

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 376495 Color: 7343
Size: 311758 Color: 4772
Size: 311748 Color: 4771

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 376790 Color: 7350
Size: 317062 Color: 5058
Size: 306149 Color: 4473

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 376886 Color: 7352
Size: 325494 Color: 5458
Size: 297621 Color: 4008

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 376891 Color: 7353
Size: 318161 Color: 5110
Size: 304949 Color: 4406

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 377121 Color: 7361
Size: 345118 Color: 6268
Size: 277762 Color: 2701

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 377156 Color: 7362
Size: 326605 Color: 5512
Size: 296240 Color: 3930

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 377164 Color: 7363
Size: 316418 Color: 5023
Size: 306419 Color: 4490

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 377166 Color: 7364
Size: 316627 Color: 5034
Size: 306208 Color: 4478

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 377175 Color: 7365
Size: 322691 Color: 5325
Size: 300135 Color: 4148

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 377189 Color: 7366
Size: 328151 Color: 5587
Size: 294661 Color: 3845

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 377199 Color: 7367
Size: 321151 Color: 5246
Size: 301651 Color: 4242

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 377212 Color: 7368
Size: 327660 Color: 5564
Size: 295129 Color: 3875

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 377230 Color: 7369
Size: 316948 Color: 5051
Size: 305823 Color: 4458

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 377295 Color: 7371
Size: 314898 Color: 4952
Size: 307808 Color: 4556

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 377300 Color: 7372
Size: 324311 Color: 5410
Size: 298390 Color: 4054

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 377332 Color: 7373
Size: 322384 Color: 5309
Size: 300285 Color: 4161

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 377361 Color: 7374
Size: 326381 Color: 5499
Size: 296259 Color: 3933

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 377376 Color: 7375
Size: 314930 Color: 4954
Size: 307695 Color: 4551

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 377403 Color: 7376
Size: 321445 Color: 5261
Size: 301153 Color: 4216

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 377555 Color: 7381
Size: 327949 Color: 5579
Size: 294497 Color: 3834

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 377579 Color: 7382
Size: 315863 Color: 4999
Size: 306559 Color: 4497

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 377604 Color: 7383
Size: 324577 Color: 5418
Size: 297820 Color: 4025

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 377837 Color: 7387
Size: 322562 Color: 5321
Size: 299602 Color: 4113

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 377994 Color: 7389
Size: 315867 Color: 5000
Size: 306140 Color: 4472

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 378012 Color: 7390
Size: 344951 Color: 6260
Size: 277038 Color: 2647

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 378115 Color: 7391
Size: 320728 Color: 5225
Size: 301158 Color: 4217

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 378236 Color: 7393
Size: 313628 Color: 4882
Size: 308137 Color: 4575

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 378256 Color: 7395
Size: 313228 Color: 4861
Size: 308517 Color: 4594

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 378277 Color: 7396
Size: 317152 Color: 5063
Size: 304572 Color: 4384

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 378313 Color: 7397
Size: 323002 Color: 5341
Size: 298686 Color: 4069

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 378331 Color: 7398
Size: 324296 Color: 5408
Size: 297374 Color: 3997

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 378366 Color: 7399
Size: 320475 Color: 5215
Size: 301160 Color: 4218

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 378419 Color: 7400
Size: 314082 Color: 4903
Size: 307500 Color: 4540

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 378434 Color: 7402
Size: 344889 Color: 6257
Size: 276678 Color: 2631

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 378519 Color: 7404
Size: 315963 Color: 5005
Size: 305519 Color: 4442

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 378534 Color: 7405
Size: 311655 Color: 4767
Size: 309812 Color: 4664

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 378638 Color: 7407
Size: 311091 Color: 4736
Size: 310272 Color: 4694

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 378640 Color: 7408
Size: 312466 Color: 4813
Size: 308895 Color: 4618

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 378642 Color: 7409
Size: 344755 Color: 6250
Size: 276604 Color: 2622

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 378670 Color: 7410
Size: 311365 Color: 4747
Size: 309966 Color: 4674

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 378754 Color: 7412
Size: 344762 Color: 6251
Size: 276485 Color: 2616

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 378796 Color: 7413
Size: 327059 Color: 5536
Size: 294146 Color: 3805

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 378848 Color: 7415
Size: 319107 Color: 5156
Size: 302046 Color: 4258

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 378864 Color: 7416
Size: 319777 Color: 5193
Size: 301360 Color: 4231

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 378882 Color: 7417
Size: 326897 Color: 5528
Size: 294222 Color: 3812

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 378897 Color: 7418
Size: 312162 Color: 4797
Size: 308942 Color: 4622

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 378922 Color: 7419
Size: 323811 Color: 5384
Size: 297268 Color: 3990

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 378945 Color: 7420
Size: 317946 Color: 5099
Size: 303110 Color: 4312

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 378984 Color: 7421
Size: 328286 Color: 5592
Size: 292731 Color: 3737

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 379007 Color: 7422
Size: 315194 Color: 4967
Size: 305800 Color: 4455

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 379035 Color: 7423
Size: 325236 Color: 5444
Size: 295730 Color: 3901

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 379084 Color: 7425
Size: 323856 Color: 5389
Size: 297061 Color: 3978

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 379087 Color: 7426
Size: 328069 Color: 5581
Size: 292845 Color: 3746

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 379331 Color: 7433
Size: 316396 Color: 5021
Size: 304274 Color: 4368

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 379542 Color: 7438
Size: 327584 Color: 5559
Size: 292875 Color: 3749

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 379646 Color: 7439
Size: 311783 Color: 4775
Size: 308572 Color: 4600

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 379650 Color: 7440
Size: 317613 Color: 5083
Size: 302738 Color: 4291

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 380026 Color: 7447
Size: 322921 Color: 5338
Size: 297054 Color: 3977

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 380266 Color: 7450
Size: 326895 Color: 5527
Size: 292840 Color: 3745

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 380268 Color: 7451
Size: 315277 Color: 4971
Size: 304456 Color: 4376

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 380276 Color: 7452
Size: 312014 Color: 4785
Size: 307711 Color: 4552

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 380276 Color: 7453
Size: 315081 Color: 4961
Size: 304644 Color: 4387

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 380359 Color: 7455
Size: 319225 Color: 5163
Size: 300417 Color: 4165

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 380459 Color: 7457
Size: 325994 Color: 5480
Size: 293548 Color: 3778

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 380462 Color: 7458
Size: 320841 Color: 5234
Size: 298698 Color: 4070

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 380463 Color: 7459
Size: 319568 Color: 5182
Size: 299970 Color: 4136

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 380484 Color: 7461
Size: 328372 Color: 5597
Size: 291145 Color: 3616

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 380518 Color: 7463
Size: 319585 Color: 5186
Size: 299898 Color: 4133

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 380534 Color: 7464
Size: 322253 Color: 5303
Size: 297214 Color: 3984

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 380712 Color: 7467
Size: 316345 Color: 5019
Size: 302944 Color: 4302

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 380720 Color: 7468
Size: 310166 Color: 4687
Size: 309115 Color: 4632

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 380979 Color: 7476
Size: 312355 Color: 4807
Size: 306667 Color: 4501

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 381046 Color: 7479
Size: 343992 Color: 6216
Size: 274963 Color: 2502

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 381193 Color: 7480
Size: 322352 Color: 5308
Size: 296456 Color: 3945

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 381204 Color: 7481
Size: 321790 Color: 5277
Size: 297007 Color: 3975

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 381231 Color: 7483
Size: 310788 Color: 4722
Size: 307982 Color: 4565

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 381308 Color: 7484
Size: 322227 Color: 5301
Size: 296466 Color: 3947

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 381332 Color: 7485
Size: 323946 Color: 5398
Size: 294723 Color: 3849

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 381371 Color: 7487
Size: 323239 Color: 5351
Size: 295391 Color: 3885

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 381413 Color: 7488
Size: 312309 Color: 4803
Size: 306279 Color: 4483

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 381449 Color: 7489
Size: 318958 Color: 5150
Size: 299594 Color: 4112

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 381506 Color: 7490
Size: 317294 Color: 5069
Size: 301201 Color: 4220

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 381578 Color: 7492
Size: 322505 Color: 5316
Size: 295918 Color: 3911

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 381786 Color: 7496
Size: 318834 Color: 5146
Size: 299381 Color: 4106

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 381815 Color: 7497
Size: 317992 Color: 5102
Size: 300194 Color: 4156

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 381949 Color: 7500
Size: 323139 Color: 5347
Size: 294913 Color: 3859

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 381999 Color: 7502
Size: 309917 Color: 4670
Size: 308085 Color: 4571

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 382055 Color: 7503
Size: 310499 Color: 4703
Size: 307447 Color: 4538

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 382093 Color: 7504
Size: 317746 Color: 5092
Size: 300162 Color: 4153

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 382133 Color: 7507
Size: 313910 Color: 4894
Size: 303958 Color: 4354

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 382173 Color: 7508
Size: 325132 Color: 5440
Size: 292696 Color: 3731

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 382176 Color: 7509
Size: 321563 Color: 5267
Size: 296262 Color: 3934

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 382212 Color: 7511
Size: 313312 Color: 4867
Size: 304477 Color: 4379

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 382218 Color: 7512
Size: 322143 Color: 5299
Size: 295640 Color: 3896

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 382220 Color: 7513
Size: 323362 Color: 5358
Size: 294419 Color: 3831

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 382262 Color: 7515
Size: 325253 Color: 5446
Size: 292486 Color: 3718

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 382365 Color: 7516
Size: 308898 Color: 4619
Size: 308738 Color: 4611

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 382370 Color: 7518
Size: 314984 Color: 4958
Size: 302647 Color: 4284

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 382370 Color: 7517
Size: 317824 Color: 5093
Size: 299807 Color: 4127

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 382382 Color: 7520
Size: 323780 Color: 5381
Size: 293839 Color: 3789

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 382404 Color: 7521
Size: 325322 Color: 5451
Size: 292275 Color: 3697

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 382453 Color: 7522
Size: 314349 Color: 4920
Size: 303199 Color: 4315

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 382514 Color: 7523
Size: 327470 Color: 5555
Size: 290017 Color: 3556

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 382520 Color: 7524
Size: 324651 Color: 5423
Size: 292830 Color: 3742

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 382529 Color: 7525
Size: 325019 Color: 5436
Size: 292453 Color: 3713

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 382531 Color: 7526
Size: 326095 Color: 5485
Size: 291375 Color: 3634

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 382543 Color: 7527
Size: 343500 Color: 6199
Size: 273958 Color: 2436

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 382591 Color: 7528
Size: 316813 Color: 5042
Size: 300597 Color: 4176

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 382614 Color: 7529
Size: 325379 Color: 5453
Size: 292008 Color: 3675

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 382972 Color: 7536
Size: 320584 Color: 5219
Size: 296445 Color: 3944

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 383029 Color: 7537
Size: 313171 Color: 4858
Size: 303801 Color: 4344

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 383075 Color: 7539
Size: 313026 Color: 4851
Size: 303900 Color: 4349

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 383119 Color: 7540
Size: 317574 Color: 5082
Size: 299308 Color: 4102

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 383128 Color: 7541
Size: 312042 Color: 4789
Size: 304831 Color: 4397

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 383135 Color: 7542
Size: 325662 Color: 5470
Size: 291204 Color: 3621

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 383137 Color: 7543
Size: 311048 Color: 4733
Size: 305816 Color: 4457

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 383137 Color: 7544
Size: 314395 Color: 4924
Size: 302469 Color: 4277

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 383157 Color: 7546
Size: 320777 Color: 5230
Size: 296067 Color: 3919

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 383199 Color: 7547
Size: 311542 Color: 4760
Size: 305260 Color: 4425

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 383205 Color: 7548
Size: 323248 Color: 5352
Size: 293548 Color: 3779

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 383218 Color: 7549
Size: 309936 Color: 4671
Size: 306847 Color: 4513

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 383232 Color: 7550
Size: 311475 Color: 4755
Size: 305294 Color: 4432

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 383310 Color: 7552
Size: 311412 Color: 4751
Size: 305279 Color: 4427

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 383315 Color: 7553
Size: 322695 Color: 5326
Size: 293991 Color: 3795

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 383324 Color: 7554
Size: 309940 Color: 4672
Size: 306737 Color: 4508

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 383340 Color: 7555
Size: 314395 Color: 4925
Size: 302266 Color: 4270

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 383535 Color: 7559
Size: 323991 Color: 5400
Size: 292475 Color: 3715

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 383539 Color: 7560
Size: 317284 Color: 5067
Size: 299178 Color: 4098

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 383664 Color: 7563
Size: 323506 Color: 5369
Size: 292831 Color: 3743

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 383673 Color: 7564
Size: 315568 Color: 4985
Size: 300760 Color: 4194

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 383673 Color: 7565
Size: 313975 Color: 4897
Size: 302353 Color: 4271

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 383708 Color: 7566
Size: 311770 Color: 4773
Size: 304523 Color: 4380

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 383763 Color: 7568
Size: 314658 Color: 4940
Size: 301580 Color: 4239

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 383767 Color: 7569
Size: 323462 Color: 5366
Size: 292772 Color: 3739

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 383771 Color: 7570
Size: 327187 Color: 5542
Size: 289043 Color: 3489

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 383775 Color: 7571
Size: 309219 Color: 4636
Size: 307007 Color: 4522

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 7577
Size: 342626 Color: 6178
Size: 273317 Color: 2380

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 384093 Color: 7579
Size: 328322 Color: 5596
Size: 287586 Color: 3404

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 384146 Color: 7581
Size: 313071 Color: 4855
Size: 302784 Color: 4292

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 384220 Color: 7582
Size: 324639 Color: 5421
Size: 291142 Color: 3614

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 384232 Color: 7583
Size: 318019 Color: 5104
Size: 297750 Color: 4018

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 384233 Color: 7584
Size: 314063 Color: 4900
Size: 301705 Color: 4244

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 384282 Color: 7586
Size: 327341 Color: 5547
Size: 288378 Color: 3454

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 384367 Color: 7587
Size: 310912 Color: 4725
Size: 304722 Color: 4389

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 384390 Color: 7588
Size: 321058 Color: 5243
Size: 294553 Color: 3840

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 384402 Color: 7589
Size: 313101 Color: 4856
Size: 302498 Color: 4278

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 384473 Color: 7592
Size: 317312 Color: 5074
Size: 298216 Color: 4044

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 384620 Color: 7594
Size: 327297 Color: 5545
Size: 288084 Color: 3429

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 384630 Color: 7595
Size: 308400 Color: 4588
Size: 306971 Color: 4519

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 384633 Color: 7596
Size: 310506 Color: 4704
Size: 304862 Color: 4401

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 384634 Color: 7597
Size: 316989 Color: 5054
Size: 298378 Color: 4053

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 384662 Color: 7598
Size: 328071 Color: 5582
Size: 287268 Color: 3374

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 384769 Color: 7603
Size: 342450 Color: 6169
Size: 272782 Color: 2344

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 385217 Color: 7613
Size: 325024 Color: 5437
Size: 289760 Color: 3538

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 385395 Color: 7617
Size: 311654 Color: 4766
Size: 302952 Color: 4303

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 385410 Color: 7618
Size: 315723 Color: 4991
Size: 298868 Color: 4077

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 385416 Color: 7619
Size: 323314 Color: 5355
Size: 291271 Color: 3627

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 385429 Color: 7620
Size: 316395 Color: 5020
Size: 298177 Color: 4042

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 385446 Color: 7621
Size: 317304 Color: 5073
Size: 297251 Color: 3988

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 385979 Color: 7633
Size: 325797 Color: 5476
Size: 288225 Color: 3437

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 385980 Color: 7634
Size: 307562 Color: 4544
Size: 306459 Color: 4492

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 386103 Color: 7638
Size: 312590 Color: 4822
Size: 301308 Color: 4224

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 386116 Color: 7639
Size: 341778 Color: 6140
Size: 272107 Color: 2283

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 386144 Color: 7640
Size: 318865 Color: 5147
Size: 294992 Color: 3864

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 386163 Color: 7641
Size: 307590 Color: 4546
Size: 306248 Color: 4479

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 386315 Color: 7644
Size: 321450 Color: 5262
Size: 292236 Color: 3695

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 386350 Color: 7645
Size: 313888 Color: 4892
Size: 299763 Color: 4122

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 386384 Color: 7646
Size: 316918 Color: 5048
Size: 296699 Color: 3957

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 386465 Color: 7649
Size: 323421 Color: 5361
Size: 290115 Color: 3563

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 386498 Color: 7651
Size: 317131 Color: 5062
Size: 296372 Color: 3941

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 386528 Color: 7652
Size: 324330 Color: 5411
Size: 289143 Color: 3496

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 386602 Color: 7654
Size: 311043 Color: 4731
Size: 302356 Color: 4272

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 386631 Color: 7655
Size: 319067 Color: 5154
Size: 294303 Color: 3818

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 386711 Color: 7656
Size: 312750 Color: 4832
Size: 300540 Color: 4171

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 386997 Color: 7665
Size: 327343 Color: 5548
Size: 285661 Color: 3263

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 387006 Color: 7666
Size: 312979 Color: 4849
Size: 300016 Color: 4141

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 387014 Color: 7667
Size: 316619 Color: 5033
Size: 296368 Color: 3940

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 387043 Color: 7668
Size: 308114 Color: 4574
Size: 304844 Color: 4398

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 387540 Color: 7675
Size: 319962 Color: 5199
Size: 292499 Color: 3720

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 387610 Color: 7676
Size: 311082 Color: 4735
Size: 301309 Color: 4225

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 387638 Color: 7677
Size: 319656 Color: 5190
Size: 292707 Color: 3734

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 387665 Color: 7678
Size: 321245 Color: 5251
Size: 291091 Color: 3611

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 387819 Color: 7682
Size: 341163 Color: 6115
Size: 271019 Color: 2213

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 387888 Color: 7684
Size: 314365 Color: 4921
Size: 297748 Color: 4017

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 387942 Color: 7685
Size: 325642 Color: 5467
Size: 286417 Color: 3319

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 388173 Color: 7689
Size: 312105 Color: 4792
Size: 299723 Color: 4120

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 388196 Color: 7690
Size: 309747 Color: 4660
Size: 302058 Color: 4259

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 388220 Color: 7691
Size: 340937 Color: 6110
Size: 270844 Color: 2195

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 388264 Color: 7693
Size: 312935 Color: 4843
Size: 298802 Color: 4075

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 388344 Color: 7695
Size: 340890 Color: 6109
Size: 270767 Color: 2190

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 388385 Color: 7696
Size: 315133 Color: 4964
Size: 296483 Color: 3950

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 388416 Color: 7697
Size: 310568 Color: 4709
Size: 301017 Color: 4212

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 388426 Color: 7698
Size: 307409 Color: 4536
Size: 304166 Color: 4365

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 388487 Color: 7701
Size: 310595 Color: 4712
Size: 300919 Color: 4203

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 388505 Color: 7702
Size: 308053 Color: 4568
Size: 303443 Color: 4325

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 7703
Size: 310573 Color: 4711
Size: 300920 Color: 4205

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 388514 Color: 7704
Size: 314233 Color: 4909
Size: 297254 Color: 3989

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 388538 Color: 7705
Size: 324152 Color: 5405
Size: 287311 Color: 3380

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 388556 Color: 7706
Size: 320872 Color: 5236
Size: 290573 Color: 3586

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 388565 Color: 7707
Size: 311425 Color: 4752
Size: 300011 Color: 4139

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 388566 Color: 7708
Size: 310129 Color: 4683
Size: 301306 Color: 4223

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 388584 Color: 7709
Size: 307852 Color: 4558
Size: 303565 Color: 4333

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 388762 Color: 7713
Size: 322114 Color: 5297
Size: 289125 Color: 3495

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 388830 Color: 7714
Size: 310623 Color: 4716
Size: 300548 Color: 4172

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 389025 Color: 7721
Size: 324126 Color: 5403
Size: 286850 Color: 3346

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 389054 Color: 7722
Size: 340657 Color: 6099
Size: 270290 Color: 2155

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 389135 Color: 7723
Size: 326356 Color: 5498
Size: 284510 Color: 3186

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 389147 Color: 7724
Size: 319250 Color: 5167
Size: 291604 Color: 3647

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 389158 Color: 7725
Size: 305473 Color: 4441
Size: 305370 Color: 4438

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 389225 Color: 7726
Size: 323822 Color: 5385
Size: 286954 Color: 3353

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 389246 Color: 7728
Size: 306749 Color: 4510
Size: 304006 Color: 4357

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 389266 Color: 7729
Size: 311672 Color: 4768
Size: 299063 Color: 4089

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 389270 Color: 7730
Size: 318384 Color: 5125
Size: 292347 Color: 3704

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 389273 Color: 7731
Size: 323650 Color: 5375
Size: 287078 Color: 3361

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 389286 Color: 7732
Size: 313487 Color: 4878
Size: 297228 Color: 3986

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 389315 Color: 7733
Size: 309376 Color: 4646
Size: 301310 Color: 4226

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 389418 Color: 7735
Size: 325249 Color: 5445
Size: 285334 Color: 3241

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 389420 Color: 7736
Size: 306273 Color: 4482
Size: 304308 Color: 4370

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 390122 Color: 7749
Size: 340348 Color: 6086
Size: 269531 Color: 2104

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 390347 Color: 7755
Size: 306777 Color: 4511
Size: 302877 Color: 4296

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 390720 Color: 7765
Size: 325547 Color: 5460
Size: 283734 Color: 3141

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 390741 Color: 7766
Size: 315840 Color: 4996
Size: 293420 Color: 3774

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 390743 Color: 7767
Size: 319252 Color: 5170
Size: 290006 Color: 3554

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 390795 Color: 7768
Size: 310028 Color: 4679
Size: 299178 Color: 4097

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 390846 Color: 7770
Size: 319576 Color: 5184
Size: 289579 Color: 3528

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 390855 Color: 7771
Size: 340029 Color: 6065
Size: 269117 Color: 2058

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 390888 Color: 7772
Size: 328280 Color: 5591
Size: 280833 Color: 2942

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 390910 Color: 7773
Size: 320718 Color: 5223
Size: 288373 Color: 3452

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 390915 Color: 7774
Size: 320257 Color: 5208
Size: 288829 Color: 3476

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 390969 Color: 7775
Size: 311925 Color: 4782
Size: 297107 Color: 3980

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 391037 Color: 7777
Size: 314311 Color: 4916
Size: 294653 Color: 3844

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 391143 Color: 7779
Size: 319251 Color: 5168
Size: 289607 Color: 3529

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 391446 Color: 7788
Size: 322930 Color: 5339
Size: 285625 Color: 3259

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 391468 Color: 7789
Size: 305283 Color: 4429
Size: 303250 Color: 4316

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 391673 Color: 7793
Size: 318323 Color: 5122
Size: 290005 Color: 3553

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 391723 Color: 7795
Size: 323359 Color: 5357
Size: 284919 Color: 3216

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 391724 Color: 7796
Size: 327199 Color: 5543
Size: 281078 Color: 2957

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 391847 Color: 7800
Size: 316671 Color: 5036
Size: 291483 Color: 3643

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 391934 Color: 7801
Size: 320749 Color: 5228
Size: 287318 Color: 3381

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 391938 Color: 7802
Size: 304582 Color: 4385
Size: 303481 Color: 4328

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 391942 Color: 7803
Size: 305249 Color: 4423
Size: 302810 Color: 4294

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 391980 Color: 7804
Size: 327416 Color: 5552
Size: 280605 Color: 2921

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 392225 Color: 7808
Size: 323142 Color: 5348
Size: 284634 Color: 3194

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 392243 Color: 7809
Size: 313323 Color: 4868
Size: 294435 Color: 3832

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 392257 Color: 7810
Size: 316422 Color: 5024
Size: 291322 Color: 3628

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 392286 Color: 7811
Size: 312706 Color: 4830
Size: 295009 Color: 3865

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 392295 Color: 7812
Size: 314563 Color: 4934
Size: 293143 Color: 3759

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 392850 Color: 7823
Size: 326750 Color: 5520
Size: 280401 Color: 2904

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 392861 Color: 7824
Size: 323855 Color: 5388
Size: 283285 Color: 3112

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 392864 Color: 7825
Size: 312332 Color: 4804
Size: 294805 Color: 3854

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 392890 Color: 7826
Size: 318340 Color: 5124
Size: 288771 Color: 3473

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 392995 Color: 7829
Size: 326715 Color: 5518
Size: 280291 Color: 2896

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 393000 Color: 7830
Size: 316565 Color: 5031
Size: 290436 Color: 3575

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 393027 Color: 7831
Size: 319277 Color: 5171
Size: 287697 Color: 3412

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 393029 Color: 7832
Size: 312762 Color: 4834
Size: 294210 Color: 3810

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 393035 Color: 7833
Size: 324160 Color: 5406
Size: 282806 Color: 3082

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 393306 Color: 7841
Size: 339173 Color: 6026
Size: 267522 Color: 1933

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 393392 Color: 7843
Size: 304526 Color: 4381
Size: 302083 Color: 4261

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 393397 Color: 7844
Size: 304094 Color: 4359
Size: 302510 Color: 4280

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 393428 Color: 7845
Size: 327376 Color: 5549
Size: 279197 Color: 2810

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 393439 Color: 7846
Size: 317034 Color: 5056
Size: 289528 Color: 3525

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 393515 Color: 7849
Size: 326087 Color: 5484
Size: 280399 Color: 2903

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 393544 Color: 7850
Size: 314012 Color: 4899
Size: 292445 Color: 3711

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 393572 Color: 7851
Size: 315922 Color: 5004
Size: 290507 Color: 3582

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 393579 Color: 7852
Size: 316665 Color: 5035
Size: 289757 Color: 3536

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 393606 Color: 7853
Size: 325133 Color: 5441
Size: 281262 Color: 2973

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 393956 Color: 7863
Size: 323421 Color: 5362
Size: 282624 Color: 3070

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 393982 Color: 7864
Size: 306298 Color: 4486
Size: 299721 Color: 4119

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 394006 Color: 7865
Size: 319909 Color: 5197
Size: 286086 Color: 3296

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 394019 Color: 7866
Size: 314719 Color: 4945
Size: 291263 Color: 3626

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 394048 Color: 7867
Size: 318810 Color: 5145
Size: 287143 Color: 3366

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 394050 Color: 7868
Size: 321962 Color: 5289
Size: 283989 Color: 3161

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 394351 Color: 7876
Size: 327528 Color: 5558
Size: 278122 Color: 2734

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 394357 Color: 7877
Size: 322479 Color: 5315
Size: 283165 Color: 3104

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 394399 Color: 7879
Size: 319225 Color: 5164
Size: 286377 Color: 3313

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 394421 Color: 7880
Size: 311324 Color: 4745
Size: 294256 Color: 3816

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 394436 Color: 7881
Size: 314243 Color: 4911
Size: 291322 Color: 3629

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 394440 Color: 7882
Size: 306618 Color: 4499
Size: 298943 Color: 4080

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 394450 Color: 7884
Size: 321508 Color: 5265
Size: 284043 Color: 3164

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 394477 Color: 7885
Size: 321780 Color: 5276
Size: 283744 Color: 3142

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 394550 Color: 7888
Size: 312790 Color: 4837
Size: 292661 Color: 3728

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 394664 Color: 7890
Size: 326912 Color: 5529
Size: 278425 Color: 2758

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 394701 Color: 7891
Size: 306171 Color: 4476
Size: 299129 Color: 4092

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 394729 Color: 7892
Size: 310111 Color: 4682
Size: 295161 Color: 3879

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 395431 Color: 7905
Size: 318339 Color: 5123
Size: 286231 Color: 3300

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 395453 Color: 7906
Size: 302905 Color: 4299
Size: 301643 Color: 4241

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 395459 Color: 7907
Size: 309439 Color: 4648
Size: 295103 Color: 3871

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 395625 Color: 7911
Size: 318436 Color: 5130
Size: 285940 Color: 3279

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 395667 Color: 7912
Size: 315892 Color: 5002
Size: 288442 Color: 3460

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 395820 Color: 7916
Size: 310088 Color: 4681
Size: 294093 Color: 3799

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 395837 Color: 7917
Size: 319546 Color: 5181
Size: 284618 Color: 3193

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 395878 Color: 7918
Size: 312008 Color: 4784
Size: 292115 Color: 3687

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 395918 Color: 7919
Size: 326455 Color: 5506
Size: 277628 Color: 2689

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 395958 Color: 7920
Size: 312959 Color: 4846
Size: 291084 Color: 3610

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 395994 Color: 7921
Size: 315456 Color: 4980
Size: 288551 Color: 3466

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 396001 Color: 7922
Size: 313999 Color: 4898
Size: 290001 Color: 3552

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 396014 Color: 7923
Size: 324057 Color: 5401
Size: 279930 Color: 2870

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 396015 Color: 7924
Size: 327142 Color: 5539
Size: 276844 Color: 2639

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 396023 Color: 7925
Size: 313268 Color: 4864
Size: 290710 Color: 3593

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 396137 Color: 7926
Size: 315361 Color: 4976
Size: 288503 Color: 3464

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 396178 Color: 7927
Size: 309674 Color: 4657
Size: 294149 Color: 3806

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 396182 Color: 7928
Size: 310605 Color: 4714
Size: 293214 Color: 3766

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 396190 Color: 7929
Size: 317051 Color: 5057
Size: 286760 Color: 3343

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 396310 Color: 7930
Size: 306478 Color: 4494
Size: 297213 Color: 3983

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 396371 Color: 7931
Size: 304264 Color: 4367
Size: 299366 Color: 4104

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 396464 Color: 7932
Size: 323897 Color: 5395
Size: 279640 Color: 2850

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 396471 Color: 7933
Size: 305724 Color: 4450
Size: 297806 Color: 4023

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 396514 Color: 7934
Size: 308956 Color: 4623
Size: 294531 Color: 3836

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 396553 Color: 7935
Size: 305186 Color: 4420
Size: 298262 Color: 4046

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 396577 Color: 7936
Size: 318474 Color: 5132
Size: 284950 Color: 3219

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 396589 Color: 7937
Size: 304767 Color: 4391
Size: 298645 Color: 4068

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 396602 Color: 7938
Size: 326804 Color: 5524
Size: 276595 Color: 2621

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 396631 Color: 7939
Size: 320993 Color: 5241
Size: 282377 Color: 3054

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 396633 Color: 7940
Size: 309012 Color: 4628
Size: 294356 Color: 3825

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 396652 Color: 7941
Size: 315841 Color: 4997
Size: 287508 Color: 3401

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 396728 Color: 7942
Size: 315595 Color: 4986
Size: 287678 Color: 3410

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 396887 Color: 7943
Size: 324520 Color: 5412
Size: 278594 Color: 2770

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 396935 Color: 7944
Size: 326121 Color: 5486
Size: 276945 Color: 2643

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 396969 Color: 7945
Size: 314095 Color: 4904
Size: 288937 Color: 3481

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 396977 Color: 7946
Size: 303907 Color: 4350
Size: 299117 Color: 4091

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 396990 Color: 7947
Size: 323754 Color: 5380
Size: 279257 Color: 2816

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 397010 Color: 7948
Size: 328127 Color: 5585
Size: 274864 Color: 2499

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 397022 Color: 7949
Size: 311409 Color: 4750
Size: 291570 Color: 3646

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 397041 Color: 7950
Size: 328147 Color: 5586
Size: 274813 Color: 2496

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 397068 Color: 7951
Size: 308900 Color: 4620
Size: 294033 Color: 3796

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 397092 Color: 7952
Size: 304970 Color: 4408
Size: 297939 Color: 4032

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 397108 Color: 7953
Size: 301869 Color: 4254
Size: 301024 Color: 4213

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 397149 Color: 7954
Size: 326496 Color: 5507
Size: 276356 Color: 2605

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 397161 Color: 7955
Size: 307078 Color: 4527
Size: 295762 Color: 3902

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 397168 Color: 7956
Size: 317938 Color: 5098
Size: 284895 Color: 3214

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 397170 Color: 7957
Size: 305984 Color: 4464
Size: 296847 Color: 3964

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 397186 Color: 7958
Size: 305618 Color: 4446
Size: 297197 Color: 3982

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 397303 Color: 7959
Size: 303377 Color: 4321
Size: 299321 Color: 4103

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 397331 Color: 7960
Size: 307510 Color: 4541
Size: 295160 Color: 3878

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 397391 Color: 7961
Size: 321976 Color: 5291
Size: 280634 Color: 2930

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 397413 Color: 7962
Size: 327003 Color: 5534
Size: 275585 Color: 2548

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 397418 Color: 7963
Size: 325420 Color: 5456
Size: 277163 Color: 2655

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 397490 Color: 7964
Size: 315296 Color: 4973
Size: 287215 Color: 3369

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 397509 Color: 7965
Size: 317738 Color: 5090
Size: 284754 Color: 3203

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 397527 Color: 7966
Size: 310262 Color: 4693
Size: 292212 Color: 3692

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 397533 Color: 7967
Size: 325159 Color: 5442
Size: 277309 Color: 2665

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 397573 Color: 7968
Size: 317563 Color: 5081
Size: 284865 Color: 3211

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 397587 Color: 7969
Size: 314276 Color: 4913
Size: 288138 Color: 3432

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 397598 Color: 7970
Size: 314581 Color: 4936
Size: 287822 Color: 3419

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 397700 Color: 7971
Size: 310219 Color: 4690
Size: 292082 Color: 3682

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 397736 Color: 7972
Size: 317488 Color: 5079
Size: 284777 Color: 3205

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 397772 Color: 7973
Size: 304163 Color: 4364
Size: 298066 Color: 4036

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 397797 Color: 7974
Size: 315134 Color: 4965
Size: 287070 Color: 3360

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 397813 Color: 7975
Size: 314946 Color: 4956
Size: 287242 Color: 3371

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 397847 Color: 7976
Size: 310252 Color: 4692
Size: 291902 Color: 3669

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 397914 Color: 7977
Size: 307366 Color: 4535
Size: 294721 Color: 3848

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 397948 Color: 7978
Size: 302895 Color: 4297
Size: 299158 Color: 4095

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 397962 Color: 7979
Size: 321429 Color: 5260
Size: 280610 Color: 2923

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 398001 Color: 7980
Size: 314558 Color: 4933
Size: 287442 Color: 3395

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 398019 Color: 7981
Size: 312531 Color: 4820
Size: 289451 Color: 3516

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 411791 Color: 8330
Size: 337986 Color: 5978
Size: 250224 Color: 42

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 411921 Color: 8335
Size: 309749 Color: 4661
Size: 278331 Color: 2754

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 411951 Color: 8336
Size: 304281 Color: 4369
Size: 283769 Color: 3144

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 411990 Color: 8337
Size: 334258 Color: 5833
Size: 253753 Color: 581

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 411997 Color: 8338
Size: 321584 Color: 5270
Size: 266420 Color: 1852

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 412048 Color: 8339
Size: 322767 Color: 5331
Size: 265186 Color: 1744

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 412075 Color: 8340
Size: 305014 Color: 4410
Size: 282912 Color: 3088

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 412108 Color: 8341
Size: 313572 Color: 4879
Size: 274321 Color: 2465

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 412113 Color: 8342
Size: 324572 Color: 5417
Size: 263316 Color: 1580

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 412116 Color: 8343
Size: 311153 Color: 4738
Size: 276732 Color: 2635

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 412136 Color: 8344
Size: 295193 Color: 3881
Size: 292672 Color: 3729

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 412247 Color: 8345
Size: 295140 Color: 3876
Size: 292614 Color: 3726

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 412362 Color: 8346
Size: 326550 Color: 5509
Size: 261089 Color: 1362

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 412377 Color: 8347
Size: 312447 Color: 4810
Size: 275177 Color: 2518

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 412467 Color: 8348
Size: 333441 Color: 5803
Size: 254093 Color: 623

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 412475 Color: 8349
Size: 295647 Color: 3897
Size: 291879 Color: 3666

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 412502 Color: 8350
Size: 312183 Color: 4798
Size: 275316 Color: 2534

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 412554 Color: 8351
Size: 334650 Color: 5850
Size: 252797 Color: 446

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 412610 Color: 8352
Size: 321803 Color: 5279
Size: 265588 Color: 1780

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 412677 Color: 8353
Size: 309285 Color: 4640
Size: 278039 Color: 2730

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 412699 Color: 8354
Size: 306090 Color: 4471
Size: 281212 Color: 2968

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 412710 Color: 8355
Size: 329760 Color: 5657
Size: 257531 Color: 1011

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 412790 Color: 8356
Size: 332161 Color: 5740
Size: 255050 Color: 750

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 412796 Color: 8357
Size: 320238 Color: 5207
Size: 266967 Color: 1893

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 412797 Color: 8358
Size: 324793 Color: 5426
Size: 262411 Color: 1494

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 412803 Color: 8359
Size: 324612 Color: 5419
Size: 262586 Color: 1518

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 412871 Color: 8360
Size: 328567 Color: 5600
Size: 258563 Color: 1113

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 412887 Color: 8361
Size: 327912 Color: 5578
Size: 259202 Color: 1181

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 412907 Color: 8362
Size: 326222 Color: 5492
Size: 260872 Color: 1343

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 412919 Color: 8363
Size: 324613 Color: 5420
Size: 262469 Color: 1504

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 412979 Color: 8364
Size: 322638 Color: 5324
Size: 264384 Color: 1670

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 412980 Color: 8365
Size: 323824 Color: 5387
Size: 263197 Color: 1565

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 412995 Color: 8366
Size: 333068 Color: 5788
Size: 253938 Color: 603

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 413228 Color: 8367
Size: 323261 Color: 5353
Size: 263512 Color: 1593

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 413308 Color: 8368
Size: 330247 Color: 5674
Size: 256446 Color: 890

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 413313 Color: 8369
Size: 297879 Color: 4026
Size: 288809 Color: 3475

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 413395 Color: 8370
Size: 322528 Color: 5319
Size: 264078 Color: 1641

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 413418 Color: 8371
Size: 322092 Color: 5295
Size: 264491 Color: 1681

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 413484 Color: 8372
Size: 307003 Color: 4521
Size: 279514 Color: 2836

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 413485 Color: 8373
Size: 331925 Color: 5733
Size: 254591 Color: 688

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 413498 Color: 8374
Size: 332229 Color: 5743
Size: 254274 Color: 647

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 413589 Color: 8375
Size: 308818 Color: 4613
Size: 277594 Color: 2684

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 413596 Color: 8376
Size: 294123 Color: 3801
Size: 292282 Color: 3698

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 413600 Color: 8377
Size: 298782 Color: 4074
Size: 287619 Color: 3407

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 413689 Color: 8378
Size: 294979 Color: 3863
Size: 291333 Color: 3630

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 413694 Color: 8379
Size: 310624 Color: 4717
Size: 275683 Color: 2554

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 413716 Color: 8380
Size: 305838 Color: 4460
Size: 280447 Color: 2907

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 413719 Color: 8381
Size: 295696 Color: 3899
Size: 290586 Color: 3587

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 413726 Color: 8382
Size: 302663 Color: 4286
Size: 283612 Color: 3136

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 413765 Color: 8383
Size: 312534 Color: 4821
Size: 273702 Color: 2414

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 413850 Color: 8384
Size: 335051 Color: 5865
Size: 251100 Color: 208

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 413899 Color: 8385
Size: 298981 Color: 4084
Size: 287121 Color: 3364

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 413936 Color: 8386
Size: 321818 Color: 5282
Size: 264247 Color: 1658

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 414031 Color: 8387
Size: 318395 Color: 5127
Size: 267575 Color: 1941

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 414036 Color: 8388
Size: 317895 Color: 5096
Size: 268070 Color: 1980

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 414056 Color: 8389
Size: 323441 Color: 5365
Size: 262504 Color: 1509

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 414057 Color: 8390
Size: 310570 Color: 4710
Size: 275374 Color: 2536

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 414082 Color: 8391
Size: 294238 Color: 3814
Size: 291681 Color: 3653

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 414103 Color: 8392
Size: 297496 Color: 4003
Size: 288402 Color: 3456

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 414260 Color: 8393
Size: 296710 Color: 3959
Size: 289031 Color: 3487

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 414273 Color: 8394
Size: 329688 Color: 5655
Size: 256040 Color: 849

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 414274 Color: 8395
Size: 318423 Color: 5128
Size: 267304 Color: 1914

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 414275 Color: 8396
Size: 320602 Color: 5221
Size: 265124 Color: 1740

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 414338 Color: 8397
Size: 320814 Color: 5232
Size: 264849 Color: 1714

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 414344 Color: 8398
Size: 332946 Color: 5781
Size: 252711 Color: 440

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 414361 Color: 8399
Size: 334907 Color: 5861
Size: 250733 Color: 141

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 414383 Color: 8400
Size: 303021 Color: 4307
Size: 282597 Color: 3068

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 414454 Color: 8401
Size: 313670 Color: 4884
Size: 271877 Color: 2267

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 414463 Color: 8402
Size: 330826 Color: 5694
Size: 254712 Color: 713

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 414472 Color: 8403
Size: 304130 Color: 4362
Size: 281399 Color: 2986

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 414491 Color: 8404
Size: 324887 Color: 5429
Size: 260623 Color: 1312

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 414517 Color: 8405
Size: 294368 Color: 3828
Size: 291116 Color: 3612

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 414565 Color: 8406
Size: 296274 Color: 3935
Size: 289162 Color: 3497

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 414582 Color: 8407
Size: 317294 Color: 5070
Size: 268125 Color: 1984

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 414595 Color: 8408
Size: 325034 Color: 5438
Size: 260372 Color: 1283

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 414601 Color: 8409
Size: 307433 Color: 4537
Size: 277967 Color: 2726

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 414665 Color: 8410
Size: 316154 Color: 5011
Size: 269182 Color: 2063

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 414712 Color: 8411
Size: 308307 Color: 4583
Size: 276982 Color: 2645

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 414733 Color: 8412
Size: 319097 Color: 5155
Size: 266171 Color: 1830

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 414771 Color: 8413
Size: 316476 Color: 5028
Size: 268754 Color: 2027

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 414787 Color: 8414
Size: 304807 Color: 4396
Size: 280407 Color: 2905

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 414790 Color: 8415
Size: 295489 Color: 3888
Size: 289722 Color: 3535

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 414827 Color: 8416
Size: 331607 Color: 5716
Size: 253567 Color: 561

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 414857 Color: 8417
Size: 302732 Color: 4289
Size: 282412 Color: 3058

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 414879 Color: 8418
Size: 292980 Color: 3753
Size: 292142 Color: 3689

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 414985 Color: 8419
Size: 301797 Color: 4251
Size: 283219 Color: 3107

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 415080 Color: 8420
Size: 316425 Color: 5025
Size: 268496 Color: 2010

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 415143 Color: 8421
Size: 293365 Color: 3772
Size: 291493 Color: 3644

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 415144 Color: 8422
Size: 316707 Color: 5038
Size: 268150 Color: 1985

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 415154 Color: 8423
Size: 300986 Color: 4210
Size: 283861 Color: 3154

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 415233 Color: 8424
Size: 292477 Color: 3716
Size: 292291 Color: 3700

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 415245 Color: 8425
Size: 326445 Color: 5505
Size: 258311 Color: 1084

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 415320 Color: 8426
Size: 306882 Color: 4516
Size: 277799 Color: 2704

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 415338 Color: 8427
Size: 326308 Color: 5495
Size: 258355 Color: 1094

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 415456 Color: 8428
Size: 295799 Color: 3903
Size: 288746 Color: 3471

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 415562 Color: 8429
Size: 313749 Color: 4889
Size: 270690 Color: 2183

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 415601 Color: 8430
Size: 314579 Color: 4935
Size: 269821 Color: 2128

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 415658 Color: 8431
Size: 324082 Color: 5402
Size: 260261 Color: 1270

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 415665 Color: 8432
Size: 297392 Color: 3999
Size: 286944 Color: 3351

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 415703 Color: 8433
Size: 306920 Color: 4517
Size: 277378 Color: 2669

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 415715 Color: 8434
Size: 324784 Color: 5425
Size: 259502 Color: 1202

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 415739 Color: 8435
Size: 304853 Color: 4399
Size: 279409 Color: 2829

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 415794 Color: 8436
Size: 319472 Color: 5177
Size: 264735 Color: 1701

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 415798 Color: 8437
Size: 331986 Color: 5734
Size: 252217 Color: 366

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 415803 Color: 8438
Size: 329603 Color: 5651
Size: 254595 Color: 689

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 415875 Color: 8439
Size: 330971 Color: 5700
Size: 253155 Color: 497

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 415881 Color: 8440
Size: 333890 Color: 5815
Size: 250230 Color: 44

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 415882 Color: 8441
Size: 316304 Color: 5017
Size: 267815 Color: 1964

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 415892 Color: 8442
Size: 302716 Color: 4288
Size: 281393 Color: 2984

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 415960 Color: 8443
Size: 308414 Color: 4590
Size: 275627 Color: 2550

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 416029 Color: 8444
Size: 311907 Color: 4779
Size: 272065 Color: 2281

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 416051 Color: 8445
Size: 324649 Color: 5422
Size: 259301 Color: 1188

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 416198 Color: 8446
Size: 299052 Color: 4088
Size: 284751 Color: 3202

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 416227 Color: 8447
Size: 296036 Color: 3916
Size: 287738 Color: 3414

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 416234 Color: 8448
Size: 325302 Color: 5450
Size: 258465 Color: 1108

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 416255 Color: 8449
Size: 312867 Color: 4840
Size: 270879 Color: 2203

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 416276 Color: 8450
Size: 309616 Color: 4654
Size: 274109 Color: 2449

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 416292 Color: 8451
Size: 307673 Color: 4550
Size: 276036 Color: 2578

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 416308 Color: 8452
Size: 306045 Color: 4469
Size: 277648 Color: 2691

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 416455 Color: 8453
Size: 307355 Color: 4534
Size: 276191 Color: 2591

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 416512 Color: 8454
Size: 320745 Color: 5226
Size: 262744 Color: 1530

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 416526 Color: 8455
Size: 293510 Color: 3777
Size: 289965 Color: 3550

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 416540 Color: 8456
Size: 332564 Color: 5762
Size: 250897 Color: 171

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 416577 Color: 8457
Size: 327859 Color: 5576
Size: 255565 Color: 800

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 416597 Color: 8458
Size: 318756 Color: 5144
Size: 264648 Color: 1693

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 416603 Color: 8459
Size: 304331 Color: 4372
Size: 279067 Color: 2803

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 416609 Color: 8460
Size: 312634 Color: 4828
Size: 270758 Color: 2188

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 416624 Color: 8461
Size: 329985 Color: 5666
Size: 253392 Color: 524

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 416644 Color: 8462
Size: 300262 Color: 4159
Size: 283095 Color: 3098

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 416674 Color: 8463
Size: 330559 Color: 5684
Size: 252768 Color: 445

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 416707 Color: 8464
Size: 321218 Color: 5250
Size: 262076 Color: 1457

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 416716 Color: 8465
Size: 314317 Color: 4917
Size: 268968 Color: 2046

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 416798 Color: 8466
Size: 305749 Color: 4452
Size: 277454 Color: 2674

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 8467
Size: 299834 Color: 4129
Size: 283352 Color: 3119

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 416877 Color: 8469
Size: 296164 Color: 3925
Size: 286960 Color: 3354

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 416877 Color: 8468
Size: 300013 Color: 4140
Size: 283111 Color: 3101

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 416923 Color: 8470
Size: 321581 Color: 5269
Size: 261497 Color: 1409

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 416997 Color: 8471
Size: 323536 Color: 5371
Size: 259468 Color: 1201

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 416999 Color: 8472
Size: 316939 Color: 5050
Size: 266063 Color: 1819

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 417001 Color: 8473
Size: 292163 Color: 3691
Size: 290837 Color: 3598

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 417101 Color: 8474
Size: 296480 Color: 3949
Size: 286420 Color: 3320

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 417113 Color: 8475
Size: 295639 Color: 3895
Size: 287249 Color: 3373

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 417279 Color: 8476
Size: 319862 Color: 5196
Size: 262860 Color: 1539

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 417319 Color: 8477
Size: 294231 Color: 3813
Size: 288451 Color: 3461

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 417382 Color: 8478
Size: 317301 Color: 5072
Size: 265318 Color: 1753

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 417385 Color: 8479
Size: 331619 Color: 5718
Size: 250997 Color: 187

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 417405 Color: 8480
Size: 314238 Color: 4910
Size: 268358 Color: 2005

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 417422 Color: 8481
Size: 293201 Color: 3764
Size: 289378 Color: 3509

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 8482
Size: 296204 Color: 3927
Size: 286327 Color: 3310

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 417474 Color: 8483
Size: 308728 Color: 4608
Size: 273799 Color: 2423

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 417478 Color: 8484
Size: 305893 Color: 4461
Size: 276630 Color: 2625

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 417522 Color: 8485
Size: 319053 Color: 5153
Size: 263426 Color: 1588

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 417524 Color: 8486
Size: 304854 Color: 4400
Size: 277623 Color: 2688

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 417552 Color: 8487
Size: 300684 Color: 4187
Size: 281765 Color: 3022

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 417567 Color: 8488
Size: 332000 Color: 5735
Size: 250434 Color: 75

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 417583 Color: 8489
Size: 304798 Color: 4395
Size: 277620 Color: 2687

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 417641 Color: 8490
Size: 317625 Color: 5084
Size: 264735 Color: 1702

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 417649 Color: 8491
Size: 310527 Color: 4706
Size: 271825 Color: 2261

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 417666 Color: 8492
Size: 325532 Color: 5459
Size: 256803 Color: 935

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 417713 Color: 8493
Size: 328845 Color: 5613
Size: 253443 Color: 535

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 417715 Color: 8494
Size: 302592 Color: 4282
Size: 279694 Color: 2853

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 417825 Color: 8495
Size: 299517 Color: 4109
Size: 282659 Color: 3073

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 417838 Color: 8496
Size: 324890 Color: 5430
Size: 257273 Color: 987

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 417841 Color: 8497
Size: 317744 Color: 5091
Size: 264416 Color: 1674

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 417875 Color: 8498
Size: 326806 Color: 5525
Size: 255320 Color: 774

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 417986 Color: 8499
Size: 316106 Color: 5007
Size: 265909 Color: 1805

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 418006 Color: 8500
Size: 302420 Color: 4275
Size: 279575 Color: 2840

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 418043 Color: 8501
Size: 309236 Color: 4637
Size: 272722 Color: 2340

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 418063 Color: 8502
Size: 299791 Color: 4126
Size: 282147 Color: 3042

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 418069 Color: 8503
Size: 308162 Color: 4576
Size: 273770 Color: 2420

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 418083 Color: 8504
Size: 328535 Color: 5599
Size: 253383 Color: 523

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 418093 Color: 8505
Size: 318039 Color: 5107
Size: 263869 Color: 1623

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 418124 Color: 8506
Size: 292316 Color: 3703
Size: 289561 Color: 3526

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 418151 Color: 8507
Size: 314215 Color: 4908
Size: 267635 Color: 1948

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 418156 Color: 8508
Size: 299971 Color: 4137
Size: 281874 Color: 3028

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 418167 Color: 8509
Size: 292348 Color: 3705
Size: 289486 Color: 3521

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 418227 Color: 8510
Size: 317541 Color: 5080
Size: 264233 Color: 1656

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 418274 Color: 8511
Size: 329518 Color: 5645
Size: 252209 Color: 364

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 418306 Color: 8512
Size: 304100 Color: 4361
Size: 277595 Color: 2685

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 418320 Color: 8513
Size: 291235 Color: 3623
Size: 290446 Color: 3577

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 418359 Color: 8514
Size: 327405 Color: 5551
Size: 254237 Color: 640

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 418420 Color: 8515
Size: 325614 Color: 5464
Size: 255967 Color: 843

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 418454 Color: 8516
Size: 307973 Color: 4563
Size: 273574 Color: 2407

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 418472 Color: 8517
Size: 300555 Color: 4173
Size: 280974 Color: 2949

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 418553 Color: 8518
Size: 323114 Color: 5345
Size: 258334 Color: 1088

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 418581 Color: 8519
Size: 294970 Color: 3862
Size: 286450 Color: 3321

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 418607 Color: 8520
Size: 323439 Color: 5364
Size: 257955 Color: 1051

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 418629 Color: 8521
Size: 320148 Color: 5205
Size: 261224 Color: 1387

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 418656 Color: 8522
Size: 295488 Color: 3887
Size: 285857 Color: 3275

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 418690 Color: 8523
Size: 308106 Color: 4573
Size: 273205 Color: 2372

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 418760 Color: 8524
Size: 328858 Color: 5614
Size: 252383 Color: 397

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 418769 Color: 8525
Size: 306672 Color: 4502
Size: 274560 Color: 2485

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 418832 Color: 8526
Size: 302166 Color: 4264
Size: 279003 Color: 2794

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 418873 Color: 8527
Size: 313433 Color: 4875
Size: 267695 Color: 1953

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 418883 Color: 8528
Size: 324840 Color: 5427
Size: 256278 Color: 870

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 418884 Color: 8529
Size: 308207 Color: 4578
Size: 272910 Color: 2350

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 418907 Color: 8530
Size: 312775 Color: 4835
Size: 268319 Color: 2002

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 418958 Color: 8531
Size: 313291 Color: 4866
Size: 267752 Color: 1958

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 418960 Color: 8532
Size: 314455 Color: 4929
Size: 266586 Color: 1863

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 418963 Color: 8533
Size: 291822 Color: 3660
Size: 289216 Color: 3501

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 418979 Color: 8534
Size: 295035 Color: 3868
Size: 285987 Color: 3287

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 418980 Color: 8535
Size: 322341 Color: 5306
Size: 258680 Color: 1123

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 419092 Color: 8536
Size: 306426 Color: 4491
Size: 274483 Color: 2477

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 419102 Color: 8537
Size: 298400 Color: 4055
Size: 282499 Color: 3062

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 419105 Color: 8538
Size: 296408 Color: 3943
Size: 284488 Color: 3185

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 419294 Color: 8539
Size: 311494 Color: 4757
Size: 269213 Color: 2067

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 419306 Color: 8540
Size: 328837 Color: 5612
Size: 251858 Color: 317

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 419377 Color: 8541
Size: 297937 Color: 4031
Size: 282687 Color: 3077

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 419534 Color: 8542
Size: 298938 Color: 4079
Size: 281529 Color: 2997

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 419676 Color: 8543
Size: 311564 Color: 4761
Size: 268761 Color: 2030

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 419698 Color: 8544
Size: 293835 Color: 3788
Size: 286468 Color: 3324

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 419715 Color: 8545
Size: 323873 Color: 5390
Size: 256413 Color: 886

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 419735 Color: 8546
Size: 303406 Color: 4324
Size: 276860 Color: 2640

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 419797 Color: 8547
Size: 319579 Color: 5185
Size: 260625 Color: 1313

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 419798 Color: 8548
Size: 328170 Color: 5588
Size: 252033 Color: 340

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 419837 Color: 8549
Size: 322524 Color: 5318
Size: 257640 Color: 1021

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 419869 Color: 8550
Size: 300816 Color: 4196
Size: 279316 Color: 2820

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 419887 Color: 8551
Size: 315076 Color: 4960
Size: 265038 Color: 1729

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 419952 Color: 8552
Size: 320748 Color: 5227
Size: 259301 Color: 1189

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 420008 Color: 8553
Size: 314160 Color: 4906
Size: 265833 Color: 1801

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 420022 Color: 8554
Size: 292123 Color: 3688
Size: 287856 Color: 3420

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 420026 Color: 8555
Size: 293830 Color: 3787
Size: 286145 Color: 3298

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 420072 Color: 8556
Size: 326776 Color: 5522
Size: 253153 Color: 495

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 420223 Color: 8557
Size: 299164 Color: 4096
Size: 280614 Color: 2925

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 420236 Color: 8558
Size: 292703 Color: 3733
Size: 287062 Color: 3359

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 420248 Color: 8559
Size: 303277 Color: 4318
Size: 276476 Color: 2614

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 420304 Color: 8560
Size: 297307 Color: 3992
Size: 282390 Color: 3056

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 420311 Color: 8561
Size: 310889 Color: 4724
Size: 268801 Color: 2032

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 420366 Color: 8562
Size: 319479 Color: 5178
Size: 260156 Color: 1263

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 420429 Color: 8563
Size: 291077 Color: 3609
Size: 288495 Color: 3462

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 420468 Color: 8564
Size: 290195 Color: 3566
Size: 289338 Color: 3505

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 420493 Color: 8565
Size: 319517 Color: 5179
Size: 259991 Color: 1243

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 420519 Color: 8566
Size: 326145 Color: 5489
Size: 253337 Color: 517

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 420580 Color: 8567
Size: 320351 Color: 5209
Size: 259070 Color: 1161

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 420747 Color: 8568
Size: 316237 Color: 5014
Size: 263017 Color: 1546

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 420896 Color: 8569
Size: 305295 Color: 4433
Size: 273810 Color: 2424

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 420951 Color: 8570
Size: 305989 Color: 4466
Size: 273061 Color: 2362

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 8572
Size: 309191 Color: 4635
Size: 269811 Color: 2123

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 420999 Color: 8571
Size: 317669 Color: 5087
Size: 261333 Color: 1394

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 421084 Color: 8573
Size: 294071 Color: 3798
Size: 284846 Color: 3207

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 421100 Color: 8574
Size: 315841 Color: 4998
Size: 263060 Color: 1548

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 421293 Color: 8575
Size: 313920 Color: 4895
Size: 264788 Color: 1708

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 421368 Color: 8576
Size: 290137 Color: 3564
Size: 288496 Color: 3463

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 421410 Color: 8577
Size: 323887 Color: 5393
Size: 254704 Color: 712

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 421414 Color: 8578
Size: 322011 Color: 5294
Size: 256576 Color: 907

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 421417 Color: 8579
Size: 306868 Color: 4515
Size: 271716 Color: 2254

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 421464 Color: 8580
Size: 321686 Color: 5274
Size: 256851 Color: 938

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 421482 Color: 8581
Size: 294133 Color: 3802
Size: 284386 Color: 3183

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 421518 Color: 8582
Size: 323153 Color: 5349
Size: 255330 Color: 776

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 421637 Color: 8583
Size: 306635 Color: 4500
Size: 271729 Color: 2256

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 421669 Color: 8584
Size: 321892 Color: 5285
Size: 256440 Color: 888

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 421743 Color: 8585
Size: 327038 Color: 5535
Size: 251220 Color: 229

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 421796 Color: 8586
Size: 304703 Color: 4388
Size: 273502 Color: 2400

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 421882 Color: 8587
Size: 326386 Color: 5500
Size: 251733 Color: 304

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 421886 Color: 8588
Size: 297494 Color: 4002
Size: 280621 Color: 2928

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 421901 Color: 8589
Size: 292025 Color: 3677
Size: 286075 Color: 3295

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 421972 Color: 8590
Size: 296979 Color: 3972
Size: 281050 Color: 2954

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 421986 Color: 8591
Size: 322876 Color: 5336
Size: 255139 Color: 757

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 421993 Color: 8592
Size: 302801 Color: 4293
Size: 275207 Color: 2521

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 422013 Color: 8593
Size: 305284 Color: 4430
Size: 272704 Color: 2338

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 422057 Color: 8594
Size: 313728 Color: 4888
Size: 264216 Color: 1653

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 422095 Color: 8595
Size: 309019 Color: 4629
Size: 268887 Color: 2042

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 422105 Color: 8596
Size: 327468 Color: 5554
Size: 250428 Color: 74

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 422133 Color: 8597
Size: 316949 Color: 5052
Size: 260919 Color: 1346

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 422297 Color: 8598
Size: 295919 Color: 3913
Size: 281785 Color: 3024

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 422387 Color: 8599
Size: 320951 Color: 5239
Size: 256663 Color: 916

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 422430 Color: 8600
Size: 308040 Color: 4567
Size: 269531 Color: 2103

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 422456 Color: 8601
Size: 318309 Color: 5121
Size: 259236 Color: 1186

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 422502 Color: 8602
Size: 303962 Color: 4355
Size: 273537 Color: 2403

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 422505 Color: 8603
Size: 306847 Color: 4514
Size: 270649 Color: 2178

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 422521 Color: 8604
Size: 305619 Color: 4448
Size: 271861 Color: 2266

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 422633 Color: 8605
Size: 295845 Color: 3906
Size: 281523 Color: 2995

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 422660 Color: 8606
Size: 290614 Color: 3588
Size: 286727 Color: 3338

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 422695 Color: 8607
Size: 309836 Color: 4666
Size: 267470 Color: 1928

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 422713 Color: 8608
Size: 312452 Color: 4811
Size: 264836 Color: 1711

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 422719 Color: 8609
Size: 295009 Color: 3866
Size: 282273 Color: 3048

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 422735 Color: 8610
Size: 300566 Color: 4174
Size: 276700 Color: 2634

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 422764 Color: 8611
Size: 322115 Color: 5298
Size: 255122 Color: 753

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 422815 Color: 8612
Size: 324940 Color: 5433
Size: 252246 Color: 379

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 422832 Color: 8613
Size: 300139 Color: 4149
Size: 277030 Color: 2646

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 422863 Color: 8614
Size: 321814 Color: 5281
Size: 255324 Color: 775

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 422928 Color: 8615
Size: 317842 Color: 5094
Size: 259231 Color: 1185

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 422939 Color: 8616
Size: 303269 Color: 4317
Size: 273793 Color: 2422

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 423055 Color: 8617
Size: 321811 Color: 5280
Size: 255135 Color: 755

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 423059 Color: 8618
Size: 321100 Color: 5244
Size: 255842 Color: 830

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 423060 Color: 8619
Size: 312209 Color: 4800
Size: 264732 Color: 1700

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 423105 Color: 8620
Size: 314810 Color: 4950
Size: 262086 Color: 1461

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 423137 Color: 8621
Size: 322328 Color: 5305
Size: 254536 Color: 679

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 423143 Color: 8622
Size: 302943 Color: 4301
Size: 273915 Color: 2432

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 423186 Color: 8623
Size: 314474 Color: 4930
Size: 262341 Color: 1484

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 423205 Color: 8624
Size: 323422 Color: 5363
Size: 253374 Color: 521

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 423206 Color: 8625
Size: 323909 Color: 5396
Size: 252886 Color: 460

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 423209 Color: 8626
Size: 312792 Color: 4838
Size: 264000 Color: 1636

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 423312 Color: 8627
Size: 303694 Color: 4339
Size: 272995 Color: 2356

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 423423 Color: 8628
Size: 289862 Color: 3541
Size: 286716 Color: 3336

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 423621 Color: 8629
Size: 297311 Color: 3993
Size: 279069 Color: 2805

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 423670 Color: 8630
Size: 320850 Color: 5235
Size: 255481 Color: 794

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 423695 Color: 8631
Size: 289472 Color: 3519
Size: 286834 Color: 3345

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 423740 Color: 8632
Size: 313466 Color: 4876
Size: 262795 Color: 1535

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 423744 Color: 8633
Size: 294271 Color: 3817
Size: 281986 Color: 3031

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 423748 Color: 8634
Size: 297001 Color: 3974
Size: 279252 Color: 2813

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 423756 Color: 8635
Size: 292004 Color: 3674
Size: 284241 Color: 3173

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 423828 Color: 8636
Size: 307056 Color: 4526
Size: 269117 Color: 2060

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 423846 Color: 8637
Size: 309536 Color: 4653
Size: 266619 Color: 1867

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 423860 Color: 8638
Size: 321957 Color: 5288
Size: 254184 Color: 636

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 423991 Color: 8639
Size: 291470 Color: 3641
Size: 284540 Color: 3189

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 424122 Color: 8640
Size: 318688 Color: 5143
Size: 257191 Color: 979

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 424181 Color: 8641
Size: 308238 Color: 4580
Size: 267582 Color: 1942

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 424197 Color: 8642
Size: 300862 Color: 4200
Size: 274942 Color: 2501

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 424209 Color: 8643
Size: 308316 Color: 4584
Size: 267476 Color: 1930

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 424231 Color: 8644
Size: 312144 Color: 4795
Size: 263626 Color: 1607

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 424258 Color: 8645
Size: 293967 Color: 3793
Size: 281776 Color: 3023

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 424273 Color: 8646
Size: 319286 Color: 5172
Size: 256442 Color: 889

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 424334 Color: 8647
Size: 311245 Color: 4744
Size: 264422 Color: 1677

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 424371 Color: 8648
Size: 309276 Color: 4639
Size: 266354 Color: 1847

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 424431 Color: 8649
Size: 292222 Color: 3694
Size: 283348 Color: 3118

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 424498 Color: 8650
Size: 309032 Color: 4630
Size: 266471 Color: 1855

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 424514 Color: 8651
Size: 290942 Color: 3602
Size: 284545 Color: 3190

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 424578 Color: 8652
Size: 325383 Color: 5454
Size: 250040 Color: 8

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 424750 Color: 8653
Size: 324130 Color: 5404
Size: 251121 Color: 209

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 424758 Color: 8654
Size: 307465 Color: 4539
Size: 267778 Color: 1961

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 424844 Color: 8655
Size: 322599 Color: 5323
Size: 252558 Color: 425

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 424851 Color: 8656
Size: 303496 Color: 4329
Size: 271654 Color: 2247

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 424873 Color: 8657
Size: 322533 Color: 5320
Size: 252595 Color: 428

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 424894 Color: 8658
Size: 308981 Color: 4626
Size: 266126 Color: 1826

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 424909 Color: 8659
Size: 313410 Color: 4874
Size: 261682 Color: 1425

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 424931 Color: 8660
Size: 320391 Color: 5210
Size: 254679 Color: 703

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 425074 Color: 8661
Size: 296285 Color: 3936
Size: 278642 Color: 2772

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 425188 Color: 8662
Size: 324533 Color: 5413
Size: 250280 Color: 52

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 425203 Color: 8663
Size: 312760 Color: 4833
Size: 262038 Color: 1450

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 425225 Color: 8664
Size: 323737 Color: 5379
Size: 251039 Color: 192

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 425274 Color: 8665
Size: 317973 Color: 5101
Size: 256754 Color: 930

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 425306 Color: 8666
Size: 293404 Color: 3773
Size: 281291 Color: 2975

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 425365 Color: 8667
Size: 293156 Color: 3760
Size: 281480 Color: 2993

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 425483 Color: 8668
Size: 322696 Color: 5327
Size: 251822 Color: 308

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 425809 Color: 8669
Size: 317010 Color: 5055
Size: 257182 Color: 975

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 425833 Color: 8670
Size: 296003 Color: 3915
Size: 278165 Color: 2740

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 425845 Color: 8671
Size: 293243 Color: 3767
Size: 280913 Color: 2946

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 425847 Color: 8672
Size: 298958 Color: 4082
Size: 275196 Color: 2519

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 425874 Color: 8673
Size: 287395 Color: 3390
Size: 286732 Color: 3339

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 425930 Color: 8674
Size: 317663 Color: 5086
Size: 256408 Color: 885

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 425961 Color: 8675
Size: 305469 Color: 4440
Size: 268571 Color: 2014

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 426037 Color: 8676
Size: 315733 Color: 4992
Size: 258231 Color: 1075

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 426079 Color: 8677
Size: 293949 Color: 3792
Size: 279973 Color: 2876

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 426091 Color: 8678
Size: 304348 Color: 4374
Size: 269562 Color: 2107

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 426149 Color: 8679
Size: 321983 Color: 5292
Size: 251869 Color: 320

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 426379 Color: 8680
Size: 312273 Color: 4802
Size: 261349 Color: 1398

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 426456 Color: 8681
Size: 309791 Color: 4662
Size: 263754 Color: 1617

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 426478 Color: 8682
Size: 318946 Color: 5149
Size: 254577 Color: 686

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 426533 Color: 8683
Size: 316283 Color: 5016
Size: 257185 Color: 976

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 426574 Color: 8684
Size: 293824 Color: 3786
Size: 279603 Color: 2845

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 426725 Color: 8685
Size: 289329 Color: 3503
Size: 283947 Color: 3157

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 426791 Color: 8686
Size: 301552 Color: 4237
Size: 271658 Color: 2248

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 426805 Color: 8687
Size: 288339 Color: 3446
Size: 284857 Color: 3209

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 426832 Color: 8688
Size: 293168 Color: 3761
Size: 280001 Color: 2879

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 426848 Color: 8689
Size: 313246 Color: 4862
Size: 259907 Color: 1235

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 426885 Color: 8690
Size: 300919 Color: 4204
Size: 272197 Color: 2289

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 427005 Color: 8691
Size: 315562 Color: 4984
Size: 257434 Color: 1001

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 427066 Color: 8692
Size: 318675 Color: 5142
Size: 254260 Color: 643

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 427079 Color: 8693
Size: 297710 Color: 4014
Size: 275212 Color: 2524

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 427107 Color: 8694
Size: 302147 Color: 4263
Size: 270747 Color: 2187

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 427122 Color: 8695
Size: 286903 Color: 3349
Size: 285976 Color: 3283

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 427127 Color: 8696
Size: 321192 Color: 5248
Size: 251682 Color: 294

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 427239 Color: 8697
Size: 292873 Color: 3748
Size: 279889 Color: 2868

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 427309 Color: 8698
Size: 287370 Color: 3386
Size: 285322 Color: 3239

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 427389 Color: 8699
Size: 308699 Color: 4607
Size: 263913 Color: 1627

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 427407 Color: 8700
Size: 318626 Color: 5139
Size: 253968 Color: 606

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 427528 Color: 8701
Size: 297900 Color: 4029
Size: 274573 Color: 2487

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 427538 Color: 8702
Size: 288221 Color: 3436
Size: 284242 Color: 3174

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 427546 Color: 8703
Size: 311044 Color: 4732
Size: 261411 Color: 1402

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 427557 Color: 8704
Size: 298705 Color: 4071
Size: 273739 Color: 2418

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 427592 Color: 8705
Size: 317888 Color: 5095
Size: 254521 Color: 678

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 427709 Color: 8706
Size: 320128 Color: 5204
Size: 252164 Color: 359

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 427710 Color: 8707
Size: 294150 Color: 3807
Size: 278141 Color: 2738

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 427713 Color: 8708
Size: 303401 Color: 4323
Size: 268887 Color: 2043

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 427715 Color: 8709
Size: 307896 Color: 4560
Size: 264390 Color: 1672

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 427732 Color: 8710
Size: 298116 Color: 4040
Size: 274153 Color: 2452

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 427811 Color: 8711
Size: 308736 Color: 4609
Size: 263454 Color: 1592

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 427819 Color: 8712
Size: 319114 Color: 5157
Size: 253068 Color: 484

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 427860 Color: 8713
Size: 305253 Color: 4424
Size: 266888 Color: 1887

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 427981 Color: 8714
Size: 303083 Color: 4311
Size: 268937 Color: 2045

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 428097 Color: 8715
Size: 314935 Color: 4955
Size: 256969 Color: 953

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 428158 Color: 8716
Size: 292528 Color: 3723
Size: 279315 Color: 2819

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 428172 Color: 8717
Size: 301314 Color: 4227
Size: 270515 Color: 2168

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 428185 Color: 8718
Size: 313682 Color: 4886
Size: 258134 Color: 1066

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 428272 Color: 8719
Size: 309331 Color: 4644
Size: 262398 Color: 1490

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 428308 Color: 8720
Size: 287969 Color: 3422
Size: 283724 Color: 3139

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 428311 Color: 8721
Size: 290031 Color: 3557
Size: 281659 Color: 3009

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 428329 Color: 8722
Size: 288899 Color: 3479
Size: 282773 Color: 3079

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 428350 Color: 8723
Size: 313677 Color: 4885
Size: 257974 Color: 1056

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 428373 Color: 8724
Size: 308960 Color: 4624
Size: 262668 Color: 1525

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 428408 Color: 8725
Size: 297330 Color: 3995
Size: 274263 Color: 2460

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 428409 Color: 8726
Size: 304096 Color: 4360
Size: 267496 Color: 1931

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 428414 Color: 8727
Size: 294531 Color: 3835
Size: 277056 Color: 2648

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 428430 Color: 8728
Size: 288996 Color: 3483
Size: 282575 Color: 3066

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 428570 Color: 8729
Size: 309626 Color: 4655
Size: 261805 Color: 1436

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 428633 Color: 8730
Size: 301982 Color: 4256
Size: 269386 Color: 2088

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 428636 Color: 8731
Size: 312502 Color: 4816
Size: 258863 Color: 1145

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 428641 Color: 8732
Size: 311197 Color: 4741
Size: 260163 Color: 1265

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 428649 Color: 8733
Size: 290615 Color: 3589
Size: 280737 Color: 2937

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 428668 Color: 8734
Size: 286470 Color: 3325
Size: 284863 Color: 3210

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 428683 Color: 8735
Size: 296474 Color: 3948
Size: 274844 Color: 2498

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 428726 Color: 8736
Size: 315169 Color: 4966
Size: 256106 Color: 856

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 428804 Color: 8737
Size: 300238 Color: 4158
Size: 270959 Color: 2212

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 428805 Color: 8738
Size: 320464 Color: 5214
Size: 250732 Color: 139

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 428823 Color: 8739
Size: 320722 Color: 5224
Size: 250456 Color: 79

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 428863 Color: 8740
Size: 320424 Color: 5212
Size: 250714 Color: 135

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 428875 Color: 8741
Size: 294800 Color: 3853
Size: 276326 Color: 2604

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 428880 Color: 8742
Size: 301305 Color: 4222
Size: 269816 Color: 2126

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 428943 Color: 8743
Size: 286291 Color: 3307
Size: 284767 Color: 3204

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 428978 Color: 8744
Size: 289026 Color: 3486
Size: 281997 Color: 3032

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 428983 Color: 8745
Size: 308505 Color: 4592
Size: 262513 Color: 1512

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 429091 Color: 8746
Size: 291847 Color: 3664
Size: 279063 Color: 2802

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 429194 Color: 8747
Size: 313604 Color: 4880
Size: 257203 Color: 980

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 429214 Color: 8748
Size: 285481 Color: 3252
Size: 285306 Color: 3237

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 429301 Color: 8749
Size: 319398 Color: 5174
Size: 251302 Color: 244

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 429452 Color: 8750
Size: 318015 Color: 5103
Size: 252534 Color: 421

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 429509 Color: 8751
Size: 312034 Color: 4788
Size: 258458 Color: 1106

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 429569 Color: 8752
Size: 309961 Color: 4673
Size: 260471 Color: 1292

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 429653 Color: 8753
Size: 289394 Color: 3511
Size: 280954 Color: 2948

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 429683 Color: 8754
Size: 288032 Color: 3425
Size: 282286 Color: 3049

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 429718 Color: 8755
Size: 318539 Color: 5133
Size: 251744 Color: 305

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 429879 Color: 8756
Size: 295704 Color: 3900
Size: 274418 Color: 2472

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 429910 Color: 8757
Size: 300720 Color: 4191
Size: 269371 Color: 2085

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 429946 Color: 8758
Size: 289166 Color: 3498
Size: 280889 Color: 2945

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 429947 Color: 8759
Size: 305361 Color: 4436
Size: 264693 Color: 1696

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 430014 Color: 8760
Size: 300621 Color: 4179
Size: 269366 Color: 2083

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 430044 Color: 8761
Size: 314210 Color: 4907
Size: 255747 Color: 821

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 430114 Color: 8762
Size: 300684 Color: 4186
Size: 269203 Color: 2066

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 430151 Color: 8763
Size: 300654 Color: 4183
Size: 269196 Color: 2065

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 430162 Color: 8764
Size: 314660 Color: 4941
Size: 255179 Color: 758

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 430169 Color: 8765
Size: 306258 Color: 4480
Size: 263574 Color: 1599

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 430245 Color: 8766
Size: 316111 Color: 5008
Size: 253645 Color: 571

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 430286 Color: 8767
Size: 303954 Color: 4353
Size: 265761 Color: 1793

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 430315 Color: 8768
Size: 292847 Color: 3747
Size: 276839 Color: 2638

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 430327 Color: 8769
Size: 297777 Color: 4021
Size: 271897 Color: 2269

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 430335 Color: 8770
Size: 290069 Color: 3560
Size: 279597 Color: 2844

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 430374 Color: 8771
Size: 291363 Color: 3632
Size: 278264 Color: 2751

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 430378 Color: 8772
Size: 316597 Color: 5032
Size: 253026 Color: 478

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 430380 Color: 8773
Size: 302244 Color: 4268
Size: 267377 Color: 1922

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 430385 Color: 8774
Size: 305938 Color: 4462
Size: 263678 Color: 1611

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 430399 Color: 8775
Size: 314381 Color: 4922
Size: 255221 Color: 764

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 430434 Color: 8776
Size: 286719 Color: 3337
Size: 282848 Color: 3085

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 430440 Color: 8777
Size: 315285 Color: 4972
Size: 254276 Color: 648

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 430547 Color: 8778
Size: 304343 Color: 4373
Size: 265111 Color: 1737

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 430583 Color: 8779
Size: 315915 Color: 5003
Size: 253503 Color: 549

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 430603 Color: 8780
Size: 318633 Color: 5140
Size: 250765 Color: 147

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 430844 Color: 8781
Size: 317437 Color: 5076
Size: 251720 Color: 300

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 430850 Color: 8782
Size: 314505 Color: 4931
Size: 254646 Color: 695

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 430853 Color: 8783
Size: 309111 Color: 4631
Size: 260037 Color: 1252

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 430860 Color: 8784
Size: 305160 Color: 4416
Size: 263981 Color: 1634

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 430872 Color: 8785
Size: 301494 Color: 4234
Size: 267635 Color: 1949

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 430879 Color: 8786
Size: 313226 Color: 4860
Size: 255896 Color: 836

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 430887 Color: 8787
Size: 288348 Color: 3448
Size: 280766 Color: 2939

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 430916 Color: 8788
Size: 307578 Color: 4545
Size: 261507 Color: 1412

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 430932 Color: 8789
Size: 314741 Color: 4946
Size: 254328 Color: 656

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 430939 Color: 8790
Size: 304384 Color: 4375
Size: 264678 Color: 1695

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 430967 Color: 8791
Size: 316152 Color: 5010
Size: 252882 Color: 459

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 431029 Color: 8792
Size: 305766 Color: 4453
Size: 263206 Color: 1566

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 431054 Color: 8793
Size: 299130 Color: 4093
Size: 269817 Color: 2127

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 431157 Color: 8794
Size: 287325 Color: 3382
Size: 281519 Color: 2994

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 431193 Color: 8795
Size: 294321 Color: 3821
Size: 274487 Color: 2478

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 431254 Color: 8796
Size: 315233 Color: 4970
Size: 253514 Color: 552

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 431268 Color: 8797
Size: 292311 Color: 3702
Size: 276422 Color: 2611

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 431369 Color: 8798
Size: 302897 Color: 4298
Size: 265735 Color: 1791

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 431386 Color: 8799
Size: 306360 Color: 4487
Size: 262255 Color: 1478

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 431392 Color: 8800
Size: 302520 Color: 4281
Size: 266089 Color: 1821

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 431404 Color: 8801
Size: 285573 Color: 3256
Size: 283024 Color: 3093

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 431407 Color: 8802
Size: 291669 Color: 3651
Size: 276925 Color: 2642

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 431411 Color: 8803
Size: 297721 Color: 4015
Size: 270869 Color: 2201

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 431414 Color: 8804
Size: 289484 Color: 3520
Size: 279103 Color: 2808

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 431419 Color: 8805
Size: 306168 Color: 4475
Size: 262414 Color: 1496

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 431468 Color: 8806
Size: 297612 Color: 4007
Size: 270921 Color: 2208

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 431482 Color: 8807
Size: 300221 Color: 4157
Size: 268298 Color: 1999

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 431535 Color: 8808
Size: 317292 Color: 5068
Size: 251174 Color: 221

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 431577 Color: 8809
Size: 290942 Color: 3603
Size: 277482 Color: 2678

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 431591 Color: 8810
Size: 297881 Color: 4027
Size: 270529 Color: 2170

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 431614 Color: 8811
Size: 295538 Color: 3891
Size: 272849 Color: 2348

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 431667 Color: 8812
Size: 299999 Color: 4138
Size: 268335 Color: 2003

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 431703 Color: 8813
Size: 290496 Color: 3581
Size: 277802 Color: 2705

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 431714 Color: 8814
Size: 296709 Color: 3958
Size: 271578 Color: 2241

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 431729 Color: 8815
Size: 284698 Color: 3199
Size: 283574 Color: 3134

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 431953 Color: 8816
Size: 295865 Color: 3909
Size: 272183 Color: 2287

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 432189 Color: 8817
Size: 299772 Color: 4124
Size: 268040 Color: 1977

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 432213 Color: 8818
Size: 286583 Color: 3331
Size: 281205 Color: 2965

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 432219 Color: 8819
Size: 303808 Color: 4346
Size: 263974 Color: 1633

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 432242 Color: 8820
Size: 290097 Color: 3561
Size: 277662 Color: 2692

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 432361 Color: 8821
Size: 287903 Color: 3421
Size: 279737 Color: 2858

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 432394 Color: 8822
Size: 286267 Color: 3304
Size: 281340 Color: 2979

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 432447 Color: 8823
Size: 288292 Color: 3443
Size: 279262 Color: 2817

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 432458 Color: 8824
Size: 306191 Color: 4477
Size: 261352 Color: 1399

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 432487 Color: 8825
Size: 305162 Color: 4417
Size: 262352 Color: 1486

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 432506 Color: 8826
Size: 306990 Color: 4520
Size: 260505 Color: 1296

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 432527 Color: 8827
Size: 293260 Color: 3768
Size: 274214 Color: 2456

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 432576 Color: 8828
Size: 284452 Color: 3184
Size: 282973 Color: 3091

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 432581 Color: 8829
Size: 298992 Color: 4086
Size: 268428 Color: 2006

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 432585 Color: 8830
Size: 298087 Color: 4039
Size: 269329 Color: 2078

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 432611 Color: 8831
Size: 313180 Color: 4859
Size: 254210 Color: 637

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 432720 Color: 8832
Size: 284846 Color: 3208
Size: 282435 Color: 3061

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 432977 Color: 8833
Size: 296827 Color: 3963
Size: 270197 Color: 2151

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 433007 Color: 8834
Size: 298727 Color: 4072
Size: 268267 Color: 1995

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 433046 Color: 8835
Size: 285703 Color: 3265
Size: 281252 Color: 2970

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 433054 Color: 8836
Size: 305356 Color: 4435
Size: 261591 Color: 1416

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 433084 Color: 8837
Size: 311611 Color: 4764
Size: 255306 Color: 769

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 433196 Color: 8838
Size: 302871 Color: 4295
Size: 263934 Color: 1629

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 433204 Color: 8839
Size: 303714 Color: 4341
Size: 263083 Color: 1553

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 433238 Color: 8840
Size: 299841 Color: 4130
Size: 266922 Color: 1889

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 433327 Color: 8841
Size: 301502 Color: 4235
Size: 265172 Color: 1741

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 433332 Color: 8842
Size: 311533 Color: 4759
Size: 255136 Color: 756

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 433347 Color: 8843
Size: 316128 Color: 5009
Size: 250526 Color: 98

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 433414 Color: 8844
Size: 300278 Color: 4160
Size: 266309 Color: 1841

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 433475 Color: 8845
Size: 314802 Color: 4949
Size: 251724 Color: 301

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 433476 Color: 8846
Size: 290380 Color: 3573
Size: 276145 Color: 2587

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 433487 Color: 8847
Size: 308547 Color: 4597
Size: 257967 Color: 1054

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 433499 Color: 8848
Size: 296748 Color: 3961
Size: 269754 Color: 2119

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 433639 Color: 8849
Size: 296303 Color: 3937
Size: 270059 Color: 2144

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 433694 Color: 8850
Size: 295534 Color: 3890
Size: 270773 Color: 2191

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 433784 Color: 8851
Size: 297782 Color: 4022
Size: 268435 Color: 2007

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 433796 Color: 8852
Size: 287757 Color: 3415
Size: 278448 Color: 2760

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 433813 Color: 8853
Size: 290386 Color: 3574
Size: 275802 Color: 2566

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 433910 Color: 8854
Size: 286068 Color: 3294
Size: 280023 Color: 2880

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 433959 Color: 8855
Size: 284682 Color: 3197
Size: 281360 Color: 2982

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 434002 Color: 8856
Size: 295413 Color: 3886
Size: 270586 Color: 2173

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 434046 Color: 8857
Size: 291878 Color: 3665
Size: 274077 Color: 2446

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 434253 Color: 8858
Size: 298017 Color: 4035
Size: 267731 Color: 1957

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 434312 Color: 8859
Size: 315486 Color: 4981
Size: 250203 Color: 37

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 434395 Color: 8860
Size: 286402 Color: 3316
Size: 279204 Color: 2811

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 434403 Color: 8861
Size: 307282 Color: 4532
Size: 258316 Color: 1085

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 434409 Color: 8862
Size: 308605 Color: 4601
Size: 256987 Color: 955

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 434517 Color: 8863
Size: 303644 Color: 4337
Size: 261840 Color: 1439

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 434546 Color: 8864
Size: 311407 Color: 4749
Size: 254048 Color: 614

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 434587 Color: 8865
Size: 284915 Color: 3215
Size: 280499 Color: 2913

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 434596 Color: 8866
Size: 285059 Color: 3223
Size: 280346 Color: 2899

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 434626 Color: 8867
Size: 305286 Color: 4431
Size: 260089 Color: 1257

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 434681 Color: 8868
Size: 302733 Color: 4290
Size: 262587 Color: 1519

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 434693 Color: 8869
Size: 295091 Color: 3870
Size: 270217 Color: 2153

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 434725 Color: 8870
Size: 302968 Color: 4305
Size: 262308 Color: 1482

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 434748 Color: 8871
Size: 287338 Color: 3384
Size: 277915 Color: 2721

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 434770 Color: 8872
Size: 285647 Color: 3262
Size: 279584 Color: 2841

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 434810 Color: 8873
Size: 298487 Color: 4061
Size: 266704 Color: 1875

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 434874 Color: 8874
Size: 288754 Color: 3472
Size: 276373 Color: 2608

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 434905 Color: 8875
Size: 292760 Color: 3738
Size: 272336 Color: 2303

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 434911 Color: 8876
Size: 295667 Color: 3898
Size: 269423 Color: 2092

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 434921 Color: 8877
Size: 308508 Color: 4593
Size: 256572 Color: 906

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 435029 Color: 8878
Size: 291375 Color: 3633
Size: 273597 Color: 2409

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 435136 Color: 8880
Size: 283514 Color: 3128
Size: 281351 Color: 2981

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 435136 Color: 8879
Size: 290255 Color: 3568
Size: 274610 Color: 2490

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 435152 Color: 8881
Size: 313858 Color: 4891
Size: 250991 Color: 186

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 435155 Color: 8882
Size: 286873 Color: 3348
Size: 277973 Color: 2727

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 435174 Color: 8883
Size: 290534 Color: 3583
Size: 274293 Color: 2463

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 435185 Color: 8884
Size: 294143 Color: 3804
Size: 270673 Color: 2182

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 435197 Color: 8885
Size: 313472 Color: 4877
Size: 251332 Color: 247

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 435208 Color: 8886
Size: 311888 Color: 4778
Size: 252905 Color: 464

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 435213 Color: 8887
Size: 287471 Color: 3397
Size: 277317 Color: 2666

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 435233 Color: 8888
Size: 292940 Color: 3750
Size: 271828 Color: 2262

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 435260 Color: 8889
Size: 297385 Color: 3998
Size: 267356 Color: 1920

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 435290 Color: 8890
Size: 285980 Color: 3285
Size: 278731 Color: 2777

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 435319 Color: 8891
Size: 303939 Color: 4351
Size: 260743 Color: 1330

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 435389 Color: 8892
Size: 311444 Color: 4754
Size: 253168 Color: 498

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 435466 Color: 8893
Size: 309308 Color: 4642
Size: 255227 Color: 766

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 435489 Color: 8894
Size: 309392 Color: 4647
Size: 255120 Color: 752

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 435529 Color: 8895
Size: 310529 Color: 4707
Size: 253943 Color: 604

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 435535 Color: 8896
Size: 302240 Color: 4267
Size: 262226 Color: 1472

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 435542 Color: 8897
Size: 286737 Color: 3340
Size: 277722 Color: 2698

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 435565 Color: 8898
Size: 298082 Color: 4037
Size: 266354 Color: 1846

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 435605 Color: 8899
Size: 296050 Color: 3917
Size: 268346 Color: 2004

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 435651 Color: 8900
Size: 289640 Color: 3530
Size: 274710 Color: 2493

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 435826 Color: 8901
Size: 284377 Color: 3182
Size: 279798 Color: 2864

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 435828 Color: 8902
Size: 292715 Color: 3735
Size: 271458 Color: 2235

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 435893 Color: 8903
Size: 296911 Color: 3969
Size: 267197 Color: 1909

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 435897 Color: 8904
Size: 309743 Color: 4659
Size: 254361 Color: 660

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 435967 Color: 8905
Size: 288809 Color: 3474
Size: 275225 Color: 2527

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 435976 Color: 8906
Size: 309636 Color: 4656
Size: 254389 Color: 666

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 436024 Color: 8907
Size: 291259 Color: 3625
Size: 272718 Color: 2339

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 436031 Color: 8908
Size: 300823 Color: 4197
Size: 263147 Color: 1557

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 436044 Color: 8909
Size: 308964 Color: 4625
Size: 254993 Color: 743

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 436049 Color: 8910
Size: 309911 Color: 4669
Size: 254041 Color: 613

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 436071 Color: 8911
Size: 287281 Color: 3377
Size: 276649 Color: 2627

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 436179 Color: 8912
Size: 294064 Color: 3797
Size: 269758 Color: 2120

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 436207 Color: 8913
Size: 286403 Color: 3317
Size: 277391 Color: 2671

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 436253 Color: 8914
Size: 306683 Color: 4504
Size: 257065 Color: 961

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 436256 Color: 8915
Size: 296358 Color: 3939
Size: 267387 Color: 1923

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 436270 Color: 8916
Size: 301631 Color: 4240
Size: 262100 Color: 1462

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 436274 Color: 8917
Size: 293669 Color: 3784
Size: 270058 Color: 2143

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 436390 Color: 8918
Size: 313062 Color: 4854
Size: 250549 Color: 102

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 436423 Color: 8919
Size: 282367 Color: 3053
Size: 281211 Color: 2967

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 436487 Color: 8920
Size: 312984 Color: 4850
Size: 250530 Color: 99

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 436514 Color: 8921
Size: 305740 Color: 4451
Size: 257747 Color: 1033

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 436548 Color: 8922
Size: 287269 Color: 3375
Size: 276184 Color: 2590

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 436554 Color: 8923
Size: 283834 Color: 3150
Size: 279613 Color: 2848

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 436593 Color: 8924
Size: 308567 Color: 4598
Size: 254841 Color: 729

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 436597 Color: 8925
Size: 285208 Color: 3234
Size: 278196 Color: 2746

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 436616 Color: 8926
Size: 300890 Color: 4201
Size: 262495 Color: 1508

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 436688 Color: 8927
Size: 294202 Color: 3809
Size: 269111 Color: 2057

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 436778 Color: 8928
Size: 300082 Color: 4145
Size: 263141 Color: 1556

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 436796 Color: 8929
Size: 291891 Color: 3668
Size: 271314 Color: 2231

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 436802 Color: 8930
Size: 309972 Color: 4675
Size: 253227 Color: 503

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 436811 Color: 8931
Size: 303340 Color: 4320
Size: 259850 Color: 1230

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 436832 Color: 8932
Size: 308612 Color: 4602
Size: 254557 Color: 683

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 437194 Color: 8933
Size: 297114 Color: 3981
Size: 265693 Color: 1787

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 437290 Color: 8934
Size: 292285 Color: 3699
Size: 270426 Color: 2166

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 437309 Color: 8935
Size: 300046 Color: 4143
Size: 262646 Color: 1524

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 437312 Color: 8936
Size: 306163 Color: 4474
Size: 256526 Color: 901

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 437386 Color: 8937
Size: 292520 Color: 3722
Size: 270095 Color: 2145

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 437408 Color: 8938
Size: 307556 Color: 4542
Size: 255037 Color: 748

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 437442 Color: 8939
Size: 309440 Color: 4649
Size: 253119 Color: 487

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 437445 Color: 8940
Size: 288374 Color: 3453
Size: 274182 Color: 2455

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 437595 Color: 8941
Size: 284310 Color: 3177
Size: 278096 Color: 2732

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 437653 Color: 8942
Size: 300941 Color: 4207
Size: 261407 Color: 1401

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 437687 Color: 8943
Size: 310161 Color: 4686
Size: 252153 Color: 356

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 437702 Color: 8944
Size: 297692 Color: 4012
Size: 264607 Color: 1690

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 437718 Color: 8945
Size: 302649 Color: 4285
Size: 259634 Color: 1213

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 437849 Color: 8946
Size: 287688 Color: 3411
Size: 274464 Color: 2476

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 437852 Color: 8947
Size: 309329 Color: 4643
Size: 252820 Color: 448

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 437883 Color: 8948
Size: 290455 Color: 3578
Size: 271663 Color: 2249

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 437892 Color: 8949
Size: 300608 Color: 4178
Size: 261501 Color: 1410

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 437963 Color: 8950
Size: 300030 Color: 4142
Size: 262008 Color: 1447

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 438007 Color: 8951
Size: 309985 Color: 4676
Size: 252009 Color: 336

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 438030 Color: 8952
Size: 305280 Color: 4428
Size: 256691 Color: 919

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 438033 Color: 8953
Size: 292056 Color: 3678
Size: 269912 Color: 2133

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 438067 Color: 8954
Size: 290300 Color: 3569
Size: 271634 Color: 2246

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 438103 Color: 8955
Size: 311676 Color: 4769
Size: 250222 Color: 41

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 438129 Color: 8956
Size: 288438 Color: 3459
Size: 273434 Color: 2394

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 438223 Color: 8957
Size: 285575 Color: 3257
Size: 276203 Color: 2592

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 438229 Color: 8958
Size: 285981 Color: 3286
Size: 275791 Color: 2563

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 438254 Color: 8959
Size: 290817 Color: 3597
Size: 270930 Color: 2209

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 8960
Size: 302983 Color: 4306
Size: 258659 Color: 1119

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 8962
Size: 306032 Color: 4468
Size: 255610 Color: 807

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 8961
Size: 305172 Color: 4418
Size: 256470 Color: 895

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 438368 Color: 8963
Size: 298336 Color: 4051
Size: 263297 Color: 1578

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 438383 Color: 8964
Size: 305998 Color: 4467
Size: 255620 Color: 811

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 438429 Color: 8965
Size: 300942 Color: 4208
Size: 260630 Color: 1315

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 438432 Color: 8966
Size: 287220 Color: 3370
Size: 274349 Color: 2467

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 438452 Color: 8967
Size: 303864 Color: 4347
Size: 257685 Color: 1026

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 438577 Color: 8968
Size: 281062 Color: 2955
Size: 280362 Color: 2901

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 438593 Color: 8969
Size: 295633 Color: 3894
Size: 265775 Color: 1795

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 438727 Color: 8970
Size: 302008 Color: 4257
Size: 259266 Color: 1187

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 438727 Color: 8971
Size: 282222 Color: 3045
Size: 279052 Color: 2801

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 438731 Color: 8972
Size: 292221 Color: 3693
Size: 269049 Color: 2053

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 438762 Color: 8973
Size: 293688 Color: 3785
Size: 267551 Color: 1937

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 438922 Color: 8974
Size: 281733 Color: 3016
Size: 279346 Color: 2826

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 439000 Color: 8975
Size: 298369 Color: 4052
Size: 262632 Color: 1522

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 439027 Color: 8976
Size: 291457 Color: 3640
Size: 269517 Color: 2100

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 439030 Color: 8977
Size: 301344 Color: 4230
Size: 259627 Color: 1212

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 439072 Color: 8978
Size: 295219 Color: 3882
Size: 265710 Color: 1788

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 439095 Color: 8979
Size: 282522 Color: 3064
Size: 278384 Color: 2757

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 439114 Color: 8980
Size: 289797 Color: 3540
Size: 271090 Color: 2219

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 439169 Color: 8981
Size: 280542 Color: 2916
Size: 280290 Color: 2895

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 439202 Color: 8982
Size: 309704 Color: 4658
Size: 251095 Color: 207

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 439237 Color: 8983
Size: 283630 Color: 3137
Size: 277134 Color: 2651

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 439286 Color: 8984
Size: 283561 Color: 3133
Size: 277154 Color: 2652

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 439300 Color: 8985
Size: 310556 Color: 4708
Size: 250145 Color: 27

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 439385 Color: 8986
Size: 306282 Color: 4484
Size: 254334 Color: 657

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 439506 Color: 8987
Size: 286926 Color: 3350
Size: 273569 Color: 2406

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 439634 Color: 8988
Size: 307793 Color: 4555
Size: 252574 Color: 426

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 439671 Color: 8989
Size: 284941 Color: 3218
Size: 275389 Color: 2537

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 439745 Color: 8990
Size: 297437 Color: 4001
Size: 262819 Color: 1537

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 439783 Color: 8991
Size: 305224 Color: 4422
Size: 254994 Color: 744

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 439794 Color: 8992
Size: 285031 Color: 3221
Size: 275176 Color: 2517

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 439818 Color: 8993
Size: 296085 Color: 3921
Size: 264098 Color: 1642

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 439831 Color: 8994
Size: 280138 Color: 2889
Size: 280032 Color: 2881

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 439869 Color: 8995
Size: 296064 Color: 3918
Size: 264068 Color: 1640

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 439884 Color: 8996
Size: 288051 Color: 3427
Size: 272066 Color: 2282

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 439887 Color: 8997
Size: 303537 Color: 4331
Size: 256577 Color: 908

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 439931 Color: 8998
Size: 303300 Color: 4319
Size: 256770 Color: 932

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 439965 Color: 8999
Size: 300622 Color: 4180
Size: 259414 Color: 1195

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 439983 Color: 9000
Size: 296952 Color: 3970
Size: 263066 Color: 1549

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 440149 Color: 9001
Size: 300725 Color: 4192
Size: 259127 Color: 1170

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 440164 Color: 9002
Size: 300469 Color: 4168
Size: 259368 Color: 1193

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 440245 Color: 9003
Size: 298288 Color: 4048
Size: 261468 Color: 1407

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 440250 Color: 9004
Size: 306805 Color: 4512
Size: 252946 Color: 468

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 440270 Color: 9005
Size: 304462 Color: 4378
Size: 255269 Color: 767

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 440287 Color: 9006
Size: 297011 Color: 3976
Size: 262703 Color: 1529

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 440291 Color: 9007
Size: 281833 Color: 3027
Size: 277877 Color: 2716

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 440350 Color: 9008
Size: 285367 Color: 3243
Size: 274284 Color: 2461

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 440353 Color: 9009
Size: 292388 Color: 3706
Size: 267260 Color: 1911

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 440417 Color: 9010
Size: 303449 Color: 4326
Size: 256135 Color: 860

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 440446 Color: 9011
Size: 305619 Color: 4447
Size: 253936 Color: 601

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 440503 Color: 9012
Size: 300419 Color: 4166
Size: 259079 Color: 1163

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 440543 Color: 9013
Size: 299617 Color: 4115
Size: 259841 Color: 1229

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 440559 Color: 9014
Size: 305177 Color: 4419
Size: 254265 Color: 644

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 440667 Color: 9015
Size: 296899 Color: 3968
Size: 262435 Color: 1499

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 440823 Color: 9016
Size: 295830 Color: 3904
Size: 263348 Color: 1582

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 440867 Color: 9017
Size: 285881 Color: 3276
Size: 273253 Color: 2377

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 440905 Color: 9018
Size: 298952 Color: 4081
Size: 260144 Color: 1261

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 440927 Color: 9019
Size: 285802 Color: 3271
Size: 273272 Color: 2379

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 440949 Color: 9020
Size: 294332 Color: 3823
Size: 264720 Color: 1699

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 440984 Color: 9021
Size: 307600 Color: 4547
Size: 251417 Color: 256

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 441156 Color: 9022
Size: 307262 Color: 4530
Size: 251583 Color: 277

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 441231 Color: 9023
Size: 283558 Color: 3132
Size: 275212 Color: 2523

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 441244 Color: 9024
Size: 289924 Color: 3547
Size: 268833 Color: 2037

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 441247 Color: 9025
Size: 297941 Color: 4033
Size: 260813 Color: 1339

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 441277 Color: 9026
Size: 300987 Color: 4211
Size: 257737 Color: 1031

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 441333 Color: 9027
Size: 285114 Color: 3227
Size: 273554 Color: 2405

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 441385 Color: 9028
Size: 293337 Color: 3770
Size: 265279 Color: 1750

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 441407 Color: 9029
Size: 300744 Color: 4193
Size: 257850 Color: 1042

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 441411 Color: 9030
Size: 305832 Color: 4459
Size: 252758 Color: 444

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 441416 Color: 9031
Size: 289943 Color: 3549
Size: 268642 Color: 2020

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 441426 Color: 9032
Size: 295622 Color: 3892
Size: 262953 Color: 1543

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 441452 Color: 9033
Size: 282129 Color: 3038
Size: 276420 Color: 2610

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 441474 Color: 9034
Size: 297245 Color: 3987
Size: 261282 Color: 1390

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 441491 Color: 9035
Size: 307919 Color: 4561
Size: 250591 Color: 110

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 441500 Color: 9036
Size: 303963 Color: 4356
Size: 254538 Color: 680

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 441669 Color: 9037
Size: 292658 Color: 3727
Size: 265674 Color: 1784

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 441734 Color: 9038
Size: 291880 Color: 3667
Size: 266387 Color: 1848

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 441762 Color: 9039
Size: 306686 Color: 4505
Size: 251553 Color: 271

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 441775 Color: 9040
Size: 284316 Color: 3178
Size: 273910 Color: 2431

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 441830 Color: 9041
Size: 293208 Color: 3765
Size: 264963 Color: 1723

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 441907 Color: 9042
Size: 305789 Color: 4454
Size: 252305 Color: 388

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 441970 Color: 9043
Size: 296848 Color: 3965
Size: 261183 Color: 1377

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 442047 Color: 9044
Size: 280253 Color: 2894
Size: 277701 Color: 2697

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 442067 Color: 9045
Size: 291439 Color: 3638
Size: 266495 Color: 1857

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 442226 Color: 9046
Size: 283229 Color: 3108
Size: 274546 Color: 2483

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 442382 Color: 9047
Size: 300299 Color: 4163
Size: 257320 Color: 990

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 442387 Color: 9048
Size: 285705 Color: 3266
Size: 271909 Color: 2271

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 442599 Color: 9049
Size: 293972 Color: 3794
Size: 263430 Color: 1590

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 442645 Color: 9050
Size: 305064 Color: 4411
Size: 252292 Color: 386

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 442759 Color: 9051
Size: 288659 Color: 3469
Size: 268583 Color: 2016

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 442763 Color: 9052
Size: 283576 Color: 3135
Size: 273662 Color: 2411

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 443045 Color: 9053
Size: 283726 Color: 3140
Size: 273230 Color: 2374

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 443049 Color: 9054
Size: 295111 Color: 3872
Size: 261841 Color: 1440

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 443128 Color: 9055
Size: 292699 Color: 3732
Size: 264174 Color: 1647

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 443192 Color: 9056
Size: 306396 Color: 4489
Size: 250413 Color: 72

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 443211 Color: 9057
Size: 303951 Color: 4352
Size: 252839 Color: 451

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 443252 Color: 9058
Size: 280679 Color: 2932
Size: 276070 Color: 2580

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 443254 Color: 9059
Size: 278916 Color: 2787
Size: 277831 Color: 2709

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 443286 Color: 9060
Size: 289079 Color: 3492
Size: 267636 Color: 1951

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 443331 Color: 9061
Size: 290054 Color: 3558
Size: 266616 Color: 1866

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 443370 Color: 9062
Size: 302397 Color: 4274
Size: 254234 Color: 639

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 443404 Color: 9063
Size: 287538 Color: 3402
Size: 269059 Color: 2054

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 443414 Color: 9064
Size: 306472 Color: 4493
Size: 250115 Color: 20

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 443447 Color: 9065
Size: 297812 Color: 4024
Size: 258742 Color: 1131

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 443540 Color: 9066
Size: 278884 Color: 2785
Size: 277577 Color: 2682

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 443556 Color: 9067
Size: 278497 Color: 2764
Size: 277948 Color: 2725

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 443635 Color: 9068
Size: 299639 Color: 4116
Size: 256727 Color: 926

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 443637 Color: 9069
Size: 278697 Color: 2775
Size: 277667 Color: 2694

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 443700 Color: 9070
Size: 289348 Color: 3507
Size: 266953 Color: 1891

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 443721 Color: 9071
Size: 289795 Color: 3539
Size: 266485 Color: 1856

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 443782 Color: 9072
Size: 279523 Color: 2837
Size: 276696 Color: 2633

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 443790 Color: 9073
Size: 280076 Color: 2883
Size: 276135 Color: 2586

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 443815 Color: 9074
Size: 300630 Color: 4181
Size: 255556 Color: 799

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 443843 Color: 9075
Size: 279711 Color: 2855
Size: 276447 Color: 2612

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 443940 Color: 9076
Size: 279880 Color: 2867
Size: 276181 Color: 2589

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 443960 Color: 9077
Size: 294752 Color: 3850
Size: 261289 Color: 1391

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 443965 Color: 9078
Size: 293636 Color: 3782
Size: 262400 Color: 1491

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 444051 Color: 9079
Size: 298568 Color: 4064
Size: 257382 Color: 996

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 444089 Color: 9080
Size: 298185 Color: 4043
Size: 257727 Color: 1030

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 444179 Color: 9081
Size: 300286 Color: 4162
Size: 255536 Color: 796

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 444184 Color: 9082
Size: 301718 Color: 4246
Size: 254099 Color: 626

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 444295 Color: 9083
Size: 297573 Color: 4005
Size: 258133 Color: 1065

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 444309 Color: 9084
Size: 305089 Color: 4413
Size: 250603 Color: 112

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 444394 Color: 9085
Size: 283231 Color: 3109
Size: 272376 Color: 2306

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 444406 Color: 9086
Size: 297352 Color: 3996
Size: 258243 Color: 1077

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 444408 Color: 9087
Size: 281019 Color: 2952
Size: 274574 Color: 2488

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 444419 Color: 9088
Size: 293132 Color: 3756
Size: 262450 Color: 1502

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 444426 Color: 9089
Size: 293337 Color: 3771
Size: 262238 Color: 1475

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 444462 Color: 9090
Size: 304916 Color: 4403
Size: 250623 Color: 117

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 444478 Color: 9091
Size: 277938 Color: 2723
Size: 277585 Color: 2683

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 444498 Color: 9092
Size: 287793 Color: 3417
Size: 267710 Color: 1956

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 444515 Color: 9093
Size: 280486 Color: 2911
Size: 275000 Color: 2506

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 444657 Color: 9094
Size: 289024 Color: 3485
Size: 266320 Color: 1844

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 444750 Color: 9095
Size: 299373 Color: 4105
Size: 255878 Color: 833

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 444827 Color: 9096
Size: 278995 Color: 2793
Size: 276179 Color: 2588

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 444849 Color: 9097
Size: 282429 Color: 3060
Size: 272723 Color: 2341

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 444942 Color: 9098
Size: 295626 Color: 3893
Size: 259433 Color: 1198

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 445038 Color: 9099
Size: 279256 Color: 2815
Size: 275707 Color: 2557

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 445061 Color: 9100
Size: 301470 Color: 4233
Size: 253470 Color: 540

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 445099 Color: 9101
Size: 282300 Color: 3051
Size: 272602 Color: 2328

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 445104 Color: 9102
Size: 292824 Color: 3741
Size: 262073 Color: 1456

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 445127 Color: 9104
Size: 285313 Color: 3238
Size: 269561 Color: 2106

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 445127 Color: 9103
Size: 279296 Color: 2818
Size: 275578 Color: 2547

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 445199 Color: 9105
Size: 296734 Color: 3960
Size: 258068 Color: 1061

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 445429 Color: 9106
Size: 298265 Color: 4047
Size: 256307 Color: 874

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 445493 Color: 9107
Size: 294808 Color: 3855
Size: 259700 Color: 1222

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 445766 Color: 9108
Size: 285960 Color: 3281
Size: 268275 Color: 1996

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 445866 Color: 9109
Size: 303699 Color: 4340
Size: 250436 Color: 76

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 445930 Color: 9110
Size: 285150 Color: 3230
Size: 268921 Color: 2044

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 446020 Color: 9111
Size: 300505 Color: 4170
Size: 253476 Color: 542

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 446042 Color: 9112
Size: 288658 Color: 3468
Size: 265301 Color: 1751

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 446056 Color: 9113
Size: 303152 Color: 4314
Size: 250793 Color: 154

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 446065 Color: 9114
Size: 292001 Color: 3673
Size: 261935 Color: 1444

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 446134 Color: 9115
Size: 301749 Color: 4247
Size: 252118 Color: 351

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 446144 Color: 9116
Size: 286273 Color: 3305
Size: 267584 Color: 1943

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 446240 Color: 9117
Size: 282818 Color: 3083
Size: 270943 Color: 2210

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 446369 Color: 9118
Size: 277842 Color: 2711
Size: 275790 Color: 2562

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 446407 Color: 9119
Size: 289337 Color: 3504
Size: 264257 Color: 1659

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 446431 Color: 9120
Size: 301577 Color: 4238
Size: 251993 Color: 334

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 446504 Color: 9121
Size: 283167 Color: 3105
Size: 270330 Color: 2156

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 446523 Color: 9122
Size: 300050 Color: 4144
Size: 253428 Color: 530

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 446555 Color: 9123
Size: 302187 Color: 4265
Size: 251259 Color: 236

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 446572 Color: 9124
Size: 279719 Color: 2856
Size: 273710 Color: 2415

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 446575 Color: 9125
Size: 277987 Color: 2728
Size: 275439 Color: 2542

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 446578 Color: 9126
Size: 281592 Color: 3002
Size: 271831 Color: 2263

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 446588 Color: 9127
Size: 281399 Color: 2985
Size: 272014 Color: 2277

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 446633 Color: 9128
Size: 283553 Color: 3131
Size: 269815 Color: 2124

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 446771 Color: 9129
Size: 292445 Color: 3710
Size: 260785 Color: 1334

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 446811 Color: 9130
Size: 280996 Color: 2950
Size: 272194 Color: 2288

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 446830 Color: 9131
Size: 287376 Color: 3388
Size: 265795 Color: 1797

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 446858 Color: 9132
Size: 277988 Color: 2729
Size: 275155 Color: 2513

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 446873 Color: 9133
Size: 284929 Color: 3217
Size: 268199 Color: 1988

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 446910 Color: 9134
Size: 280108 Color: 2886
Size: 272983 Color: 2355

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 447047 Color: 9135
Size: 276671 Color: 2628
Size: 276283 Color: 2602

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 447224 Color: 9136
Size: 294366 Color: 3826
Size: 258411 Color: 1101

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 447269 Color: 9137
Size: 300445 Color: 4167
Size: 252287 Color: 385

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 447307 Color: 9138
Size: 291015 Color: 3604
Size: 261679 Color: 1424

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 447311 Color: 9139
Size: 283277 Color: 3111
Size: 269413 Color: 2091

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 447325 Color: 9140
Size: 287247 Color: 3372
Size: 265429 Color: 1763

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 447340 Color: 9141
Size: 278576 Color: 2767
Size: 274085 Color: 2448

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 447505 Color: 9142
Size: 291405 Color: 3636
Size: 261091 Color: 1364

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 447549 Color: 9143
Size: 292097 Color: 3684
Size: 260355 Color: 1280

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 447579 Color: 9144
Size: 299883 Color: 4132
Size: 252539 Color: 422

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 447609 Color: 9145
Size: 287414 Color: 3392
Size: 264978 Color: 1724

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 447692 Color: 9146
Size: 288693 Color: 3470
Size: 263616 Color: 1605

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 447784 Color: 9147
Size: 298578 Color: 4066
Size: 253639 Color: 570

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 447881 Color: 9148
Size: 282289 Color: 3050
Size: 269831 Color: 2130

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 447895 Color: 9149
Size: 283814 Color: 3147
Size: 268292 Color: 1997

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 447972 Color: 9150
Size: 287167 Color: 3367
Size: 264862 Color: 1715

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 448159 Color: 9151
Size: 300338 Color: 4164
Size: 251504 Color: 266

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 448242 Color: 9152
Size: 287480 Color: 3398
Size: 264279 Color: 1663

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 448276 Color: 9153
Size: 280840 Color: 2943
Size: 270885 Color: 2205

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 448305 Color: 9154
Size: 292516 Color: 3721
Size: 259180 Color: 1178

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 448347 Color: 9155
Size: 292090 Color: 3683
Size: 259564 Color: 1209

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 448358 Color: 9156
Size: 293297 Color: 3769
Size: 258346 Color: 1092

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 448374 Color: 9157
Size: 292481 Color: 3717
Size: 259146 Color: 1173

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 448385 Color: 9158
Size: 295924 Color: 3914
Size: 255692 Color: 818

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 448396 Color: 9159
Size: 284218 Color: 3172
Size: 267387 Color: 1924

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 448413 Color: 9160
Size: 280929 Color: 2947
Size: 270659 Color: 2180

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 448447 Color: 9161
Size: 298086 Color: 4038
Size: 253468 Color: 539

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 448454 Color: 9162
Size: 297750 Color: 4019
Size: 253797 Color: 583

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 448484 Color: 9163
Size: 277806 Color: 2707
Size: 273711 Color: 2416

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 448555 Color: 9164
Size: 299223 Color: 4099
Size: 252223 Color: 369

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 448565 Color: 9165
Size: 291401 Color: 3635
Size: 260035 Color: 1251

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 448616 Color: 9166
Size: 292543 Color: 3724
Size: 258842 Color: 1141

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 448661 Color: 9167
Size: 298615 Color: 4067
Size: 252725 Color: 442

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 448713 Color: 9168
Size: 296524 Color: 3952
Size: 254764 Color: 718

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 448748 Color: 9169
Size: 285389 Color: 3247
Size: 265864 Color: 1802

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 448768 Color: 9170
Size: 285143 Color: 3229
Size: 266090 Color: 1822

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 448785 Color: 9171
Size: 280321 Color: 2897
Size: 270895 Color: 2207

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 448856 Color: 9172
Size: 284525 Color: 3188
Size: 266620 Color: 1868

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 448916 Color: 9173
Size: 287445 Color: 3396
Size: 263640 Color: 1608

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 448967 Color: 9174
Size: 279032 Color: 2798
Size: 272002 Color: 2274

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 449066 Color: 9175
Size: 291347 Color: 3631
Size: 259588 Color: 1210

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 449113 Color: 9176
Size: 292487 Color: 3719
Size: 258401 Color: 1099

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 449172 Color: 9177
Size: 285804 Color: 3272
Size: 265025 Color: 1728

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 449186 Color: 9178
Size: 278167 Color: 2741
Size: 272648 Color: 2332

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 449208 Color: 9179
Size: 292427 Color: 3708
Size: 258366 Color: 1096

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 449224 Color: 9180
Size: 284636 Color: 3195
Size: 266141 Color: 1828

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 449232 Color: 9181
Size: 285712 Color: 3267
Size: 265057 Color: 1730

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 449288 Color: 9182
Size: 281462 Color: 2992
Size: 269251 Color: 2070

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 449382 Color: 9183
Size: 277942 Color: 2724
Size: 272677 Color: 2337

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 449396 Color: 9184
Size: 288351 Color: 3449
Size: 262254 Color: 1477

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 449398 Color: 9185
Size: 289576 Color: 3527
Size: 261027 Color: 1356

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 449404 Color: 9186
Size: 282069 Color: 3036
Size: 268528 Color: 2013

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 449418 Color: 9187
Size: 287327 Color: 3383
Size: 263256 Color: 1570

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 449487 Color: 9188
Size: 277155 Color: 2653
Size: 273359 Color: 2387

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 449494 Color: 9189
Size: 294550 Color: 3839
Size: 255957 Color: 840

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 449510 Color: 9190
Size: 294348 Color: 3824
Size: 256143 Color: 862

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 449610 Color: 9191
Size: 285078 Color: 3224
Size: 265313 Color: 1752

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 449627 Color: 9192
Size: 285740 Color: 3268
Size: 264634 Color: 1692

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 449639 Color: 9193
Size: 285186 Color: 3231
Size: 265176 Color: 1743

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 449640 Color: 9194
Size: 288379 Color: 3455
Size: 261982 Color: 1446

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 449651 Color: 9195
Size: 296465 Color: 3946
Size: 253885 Color: 590

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 449827 Color: 9196
Size: 295489 Color: 3889
Size: 254685 Color: 706

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 449916 Color: 9197
Size: 292397 Color: 3707
Size: 257688 Color: 1027

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 449942 Color: 9198
Size: 278803 Color: 2779
Size: 271256 Color: 2226

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 449996 Color: 9199
Size: 280425 Color: 2906
Size: 269580 Color: 2110

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 450000 Color: 9200
Size: 293638 Color: 3783
Size: 256363 Color: 879

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 450006 Color: 9201
Size: 299721 Color: 4118
Size: 250274 Color: 51

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 450038 Color: 9202
Size: 297437 Color: 4000
Size: 252526 Color: 420

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 450057 Color: 9203
Size: 276591 Color: 2620
Size: 273353 Color: 2385

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 450085 Color: 9204
Size: 283750 Color: 3143
Size: 266166 Color: 1829

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 450105 Color: 9205
Size: 294608 Color: 3842
Size: 255288 Color: 768

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 450146 Color: 9206
Size: 299555 Color: 4111
Size: 250300 Color: 56

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 450248 Color: 9207
Size: 289019 Color: 3484
Size: 260734 Color: 1329

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 450298 Color: 9208
Size: 277820 Color: 2708
Size: 271883 Color: 2268

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 450377 Color: 9209
Size: 292790 Color: 3740
Size: 256834 Color: 937

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 450425 Color: 9210
Size: 281004 Color: 2951
Size: 268572 Color: 2015

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 450435 Color: 9211
Size: 294246 Color: 3815
Size: 255320 Color: 773

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 450576 Color: 9212
Size: 289117 Color: 3493
Size: 260308 Color: 1278

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 450588 Color: 9213
Size: 291846 Color: 3663
Size: 257567 Color: 1016

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 450658 Color: 9214
Size: 296078 Color: 3920
Size: 253265 Color: 509

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 450684 Color: 9215
Size: 275303 Color: 2533
Size: 274014 Color: 2441

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 450697 Color: 9216
Size: 278218 Color: 2749
Size: 271086 Color: 2218

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 450809 Color: 9217
Size: 296954 Color: 3971
Size: 252238 Color: 376

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 450819 Color: 9218
Size: 276099 Color: 2581
Size: 273083 Color: 2368

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 450889 Color: 9219
Size: 289430 Color: 3514
Size: 259682 Color: 1219

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 450942 Color: 9220
Size: 281177 Color: 2963
Size: 267882 Color: 1970

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 451091 Color: 9221
Size: 295864 Color: 3908
Size: 253046 Color: 481

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 451124 Color: 9222
Size: 291656 Color: 3650
Size: 257221 Color: 983

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 451162 Color: 9223
Size: 295123 Color: 3874
Size: 253716 Color: 574

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 451209 Color: 9224
Size: 277194 Color: 2658
Size: 271598 Color: 2242

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 451239 Color: 9225
Size: 279479 Color: 2833
Size: 269283 Color: 2072

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 451297 Color: 9226
Size: 296690 Color: 3956
Size: 252014 Color: 337

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 451329 Color: 9227
Size: 276642 Color: 2626
Size: 272030 Color: 2280

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 451477 Color: 9228
Size: 274292 Color: 2462
Size: 274232 Color: 2459

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 451583 Color: 9229
Size: 275799 Color: 2564
Size: 272619 Color: 2330

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 451621 Color: 9230
Size: 294367 Color: 3827
Size: 254013 Color: 607

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 451670 Color: 9231
Size: 275773 Color: 2560
Size: 272558 Color: 2326

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 451734 Color: 9232
Size: 282591 Color: 3067
Size: 265676 Color: 1785

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 451755 Color: 9233
Size: 291793 Color: 3659
Size: 256453 Color: 892

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 451785 Color: 9234
Size: 285675 Color: 3264
Size: 262541 Color: 1516

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 451865 Color: 9235
Size: 287975 Color: 3423
Size: 260161 Color: 1264

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 451931 Color: 9236
Size: 285987 Color: 3288
Size: 262083 Color: 1459

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 451953 Color: 9237
Size: 277301 Color: 2664
Size: 270747 Color: 2186

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 451955 Color: 9238
Size: 290869 Color: 3599
Size: 257177 Color: 973

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 451997 Color: 9239
Size: 281404 Color: 2987
Size: 266600 Color: 1864

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 452030 Color: 9240
Size: 284576 Color: 3192
Size: 263395 Color: 1586

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 452076 Color: 9241
Size: 281613 Color: 3005
Size: 266312 Color: 1843

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 452197 Color: 9242
Size: 289039 Color: 3488
Size: 258765 Color: 1135

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 452239 Color: 9243
Size: 294548 Color: 3838
Size: 253214 Color: 501

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 452244 Color: 9244
Size: 286558 Color: 3330
Size: 261199 Color: 1380

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 452309 Color: 9245
Size: 292081 Color: 3681
Size: 255611 Color: 808

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 452392 Color: 9246
Size: 294380 Color: 3829
Size: 253229 Color: 504

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 452542 Color: 9247
Size: 275659 Color: 2553
Size: 271800 Color: 2259

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 452552 Color: 9248
Size: 291142 Color: 3615
Size: 256307 Color: 873

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 452582 Color: 9249
Size: 286052 Color: 3293
Size: 261367 Color: 1400

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 452618 Color: 9250
Size: 287416 Color: 3393
Size: 259967 Color: 1241

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 452728 Color: 9251
Size: 282780 Color: 3080
Size: 264493 Color: 1682

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 452759 Color: 9252
Size: 276481 Color: 2615
Size: 270761 Color: 2189

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 452784 Color: 9253
Size: 282646 Color: 3072
Size: 264571 Color: 1689

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 452815 Color: 9254
Size: 286037 Color: 3289
Size: 261149 Color: 1372

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 452914 Color: 9255
Size: 294922 Color: 3860
Size: 252165 Color: 360

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 452951 Color: 9256
Size: 279431 Color: 2831
Size: 267619 Color: 1947

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 452986 Color: 9257
Size: 279879 Color: 2866
Size: 267136 Color: 1902

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 453023 Color: 9258
Size: 288566 Color: 3467
Size: 258412 Color: 1102

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 453025 Color: 9259
Size: 285244 Color: 3235
Size: 261732 Color: 1428

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 453055 Color: 9260
Size: 273817 Color: 2426
Size: 273129 Color: 2369

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 453057 Color: 9261
Size: 287083 Color: 3362
Size: 259861 Color: 1232

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 453117 Color: 9262
Size: 277247 Color: 2660
Size: 269637 Color: 2113

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 453232 Color: 9263
Size: 289701 Color: 3533
Size: 257068 Color: 962

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 453266 Color: 9264
Size: 281796 Color: 3025
Size: 264939 Color: 1722

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 453285 Color: 9265
Size: 287302 Color: 3379
Size: 259414 Color: 1196

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 453375 Color: 9266
Size: 295838 Color: 3905
Size: 250788 Color: 152

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 453442 Color: 9267
Size: 279021 Color: 2795
Size: 267538 Color: 1936

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 453477 Color: 9268
Size: 282624 Color: 3071
Size: 263900 Color: 1625

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 453493 Color: 9269
Size: 290882 Color: 3600
Size: 255626 Color: 812

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 453584 Color: 9270
Size: 273337 Color: 2383
Size: 273080 Color: 2366

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 453610 Color: 9271
Size: 281642 Color: 3008
Size: 264749 Color: 1703

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 453622 Color: 9272
Size: 293188 Color: 3763
Size: 253191 Color: 499

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 453722 Color: 9273
Size: 287544 Color: 3403
Size: 258735 Color: 1129

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 453754 Color: 9274
Size: 288863 Color: 3477
Size: 257384 Color: 998

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 453799 Color: 9275
Size: 278137 Color: 2737
Size: 268065 Color: 1979

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 453832 Color: 9276
Size: 282126 Color: 3037
Size: 264043 Color: 1638

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 453864 Color: 9277
Size: 286144 Color: 3297
Size: 259993 Color: 1244

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 453870 Color: 9278
Size: 290375 Color: 3572
Size: 255756 Color: 823

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 453913 Color: 9279
Size: 280471 Color: 2909
Size: 265617 Color: 1781

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 453933 Color: 9280
Size: 280794 Color: 2941
Size: 265274 Color: 1748

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 453935 Color: 9281
Size: 286296 Color: 3308
Size: 259770 Color: 1225

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 453983 Color: 9282
Size: 289120 Color: 3494
Size: 256898 Color: 942

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 454041 Color: 9283
Size: 291019 Color: 3605
Size: 254941 Color: 740

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 454044 Color: 9284
Size: 278620 Color: 2771
Size: 267337 Color: 1918

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 454048 Color: 9285
Size: 289424 Color: 3513
Size: 256529 Color: 902

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 454060 Color: 9286
Size: 275290 Color: 2530
Size: 270651 Color: 2179

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 454241 Color: 9287
Size: 279546 Color: 2838
Size: 266214 Color: 1836

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 454284 Color: 9288
Size: 288255 Color: 3441
Size: 257462 Color: 1004

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 454408 Color: 9289
Size: 283831 Color: 3148
Size: 261762 Color: 1431

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 454667 Color: 9290
Size: 287122 Color: 3365
Size: 258212 Color: 1072

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 454712 Color: 9291
Size: 291675 Color: 3652
Size: 253614 Color: 568

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 454717 Color: 9292
Size: 283488 Color: 3126
Size: 261796 Color: 1435

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 454728 Color: 9293
Size: 277413 Color: 2673
Size: 267860 Color: 1969

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 454730 Color: 9294
Size: 293594 Color: 3780
Size: 251677 Color: 293

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 454732 Color: 9295
Size: 281983 Color: 3030
Size: 263286 Color: 1575

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 454872 Color: 9296
Size: 291607 Color: 3648
Size: 253522 Color: 554

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 454885 Color: 9297
Size: 294200 Color: 3808
Size: 250916 Color: 175

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 454959 Color: 9298
Size: 275213 Color: 2525
Size: 269829 Color: 2129

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 454969 Color: 9299
Size: 291639 Color: 3649
Size: 253393 Color: 525

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 454970 Color: 9300
Size: 277476 Color: 2677
Size: 267555 Color: 1938

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 454988 Color: 9301
Size: 282887 Color: 3086
Size: 262126 Color: 1465

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 455065 Color: 9302
Size: 275927 Color: 2571
Size: 269009 Color: 2050

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 455072 Color: 9303
Size: 274081 Color: 2447
Size: 270848 Color: 2198

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 455091 Color: 9304
Size: 280542 Color: 2917
Size: 264368 Color: 1669

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 455104 Color: 9305
Size: 280606 Color: 2922
Size: 264291 Color: 1664

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 455170 Color: 9306
Size: 294217 Color: 3811
Size: 250614 Color: 115

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 455200 Color: 9307
Size: 277499 Color: 2679
Size: 267302 Color: 1913

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 455323 Color: 9308
Size: 291531 Color: 3645
Size: 253147 Color: 492

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 455356 Color: 9309
Size: 287734 Color: 3413
Size: 256911 Color: 944

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 455556 Color: 9310
Size: 294324 Color: 3822
Size: 250121 Color: 24

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 455590 Color: 9311
Size: 283938 Color: 3156
Size: 260473 Color: 1293

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 455891 Color: 9312
Size: 278653 Color: 2773
Size: 265457 Color: 1768

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 455948 Color: 9313
Size: 282835 Color: 3084
Size: 261218 Color: 1384

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 456018 Color: 9314
Size: 291030 Color: 3607
Size: 252953 Color: 471

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 456027 Color: 9315
Size: 279042 Color: 2799
Size: 264932 Color: 1720

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 456074 Color: 9316
Size: 285939 Color: 3278
Size: 257988 Color: 1057

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 456082 Color: 9317
Size: 292450 Color: 3712
Size: 251469 Color: 263

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 456090 Color: 9318
Size: 285637 Color: 3260
Size: 258274 Color: 1078

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 456113 Color: 9319
Size: 288532 Color: 3465
Size: 255356 Color: 779

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 456143 Color: 9320
Size: 274541 Color: 2482
Size: 269317 Color: 2076

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 456193 Color: 9321
Size: 273466 Color: 2397
Size: 270342 Color: 2158

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 456234 Color: 9322
Size: 275726 Color: 2558
Size: 268041 Color: 1978

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 456255 Color: 9323
Size: 277676 Color: 2695
Size: 266070 Color: 1820

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 456282 Color: 9324
Size: 278509 Color: 2765
Size: 265210 Color: 1746

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 456344 Color: 9325
Size: 277641 Color: 2690
Size: 266016 Color: 1814

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 456419 Color: 9326
Size: 277294 Color: 2663
Size: 266288 Color: 1839

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 456484 Color: 9327
Size: 285976 Color: 3284
Size: 257541 Color: 1013

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 456559 Color: 9328
Size: 281692 Color: 3012
Size: 261750 Color: 1430

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 456568 Color: 9329
Size: 280611 Color: 2924
Size: 262822 Color: 1538

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 456583 Color: 9330
Size: 281711 Color: 3014
Size: 261707 Color: 1427

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 456637 Color: 9331
Size: 291117 Color: 3613
Size: 252247 Color: 380

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 456677 Color: 9332
Size: 284680 Color: 3196
Size: 258644 Color: 1118

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 456682 Color: 9333
Size: 282676 Color: 3075
Size: 260643 Color: 1316

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 456692 Color: 9334
Size: 280774 Color: 2940
Size: 262535 Color: 1514

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 456740 Color: 9335
Size: 280712 Color: 2935
Size: 262549 Color: 1517

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 456787 Color: 9336
Size: 289070 Color: 3491
Size: 254144 Color: 633

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 456792 Color: 9337
Size: 273066 Color: 2364
Size: 270143 Color: 2148

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 456843 Color: 9338
Size: 286410 Color: 3318
Size: 256748 Color: 929

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 456860 Color: 9339
Size: 292069 Color: 3680
Size: 251072 Color: 203

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 456936 Color: 9340
Size: 289917 Color: 3546
Size: 253148 Color: 493

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 457011 Color: 9341
Size: 282377 Color: 3055
Size: 260613 Color: 1309

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 457054 Color: 9342
Size: 289466 Color: 3518
Size: 253481 Color: 543

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 457057 Color: 9343
Size: 291971 Color: 3672
Size: 250973 Color: 182

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 457074 Color: 9344
Size: 290931 Color: 3601
Size: 251996 Color: 335

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 457093 Color: 9345
Size: 290670 Color: 3591
Size: 252238 Color: 377

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 457107 Color: 9346
Size: 273238 Color: 2376
Size: 269656 Color: 2114

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 457145 Color: 9347
Size: 282683 Color: 3076
Size: 260173 Color: 1266

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 457160 Color: 9348
Size: 292020 Color: 3676
Size: 250821 Color: 160

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 457170 Color: 9349
Size: 283674 Color: 3138
Size: 259157 Color: 1175

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 457214 Color: 9350
Size: 282364 Color: 3052
Size: 260423 Color: 1287

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 457232 Color: 9351
Size: 283099 Color: 3100
Size: 259670 Color: 1218

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 457274 Color: 9352
Size: 286990 Color: 3356
Size: 255737 Color: 820

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 457393 Color: 9353
Size: 291926 Color: 3671
Size: 250682 Color: 130

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 457395 Color: 9354
Size: 284165 Color: 3170
Size: 258441 Color: 1104

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 457414 Color: 9355
Size: 276010 Color: 2575
Size: 266577 Color: 1862

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 457514 Color: 9356
Size: 271955 Color: 2272
Size: 270532 Color: 2171

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 457525 Color: 9357
Size: 274536 Color: 2481
Size: 267940 Color: 1973

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 457528 Color: 9358
Size: 273078 Color: 2365
Size: 269395 Color: 2090

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 457573 Color: 9359
Size: 283377 Color: 3121
Size: 259051 Color: 1160

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 457636 Color: 9360
Size: 286746 Color: 3341
Size: 255619 Color: 810

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 457776 Color: 9361
Size: 287658 Color: 3408
Size: 254567 Color: 685

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 457809 Color: 9362
Size: 286377 Color: 3314
Size: 255815 Color: 828

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 457842 Color: 9363
Size: 286210 Color: 3299
Size: 255949 Color: 839

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 457993 Color: 9364
Size: 286048 Color: 3291
Size: 255960 Color: 841

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 458001 Color: 9365
Size: 278713 Color: 2776
Size: 263287 Color: 1577

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 458065 Color: 9366
Size: 285642 Color: 3261
Size: 256294 Color: 871

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 458204 Color: 9367
Size: 276450 Color: 2613
Size: 265347 Color: 1756

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 458255 Color: 9368
Size: 279511 Color: 2835
Size: 262235 Color: 1473

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 458264 Color: 9369
Size: 272982 Color: 2354
Size: 268755 Color: 2028

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 458342 Color: 9370
Size: 279682 Color: 2852
Size: 261977 Color: 1445

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 458493 Color: 9371
Size: 290000 Color: 3551
Size: 251508 Color: 267

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 458496 Color: 9372
Size: 273202 Color: 2371
Size: 268303 Color: 2000

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 458545 Color: 9373
Size: 285847 Color: 3273
Size: 255609 Color: 806

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 458548 Color: 9374
Size: 272472 Color: 2317
Size: 268981 Color: 2048

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 458590 Color: 9375
Size: 283130 Color: 3102
Size: 258281 Color: 1080

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 458626 Color: 9376
Size: 287425 Color: 3394
Size: 253950 Color: 605

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 458633 Color: 9377
Size: 287301 Color: 3378
Size: 254067 Color: 617

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 458709 Color: 9378
Size: 272497 Color: 2318
Size: 268795 Color: 2031

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 458802 Color: 9379
Size: 275707 Color: 2556
Size: 265492 Color: 1773

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 458819 Color: 9380
Size: 283189 Color: 3106
Size: 257993 Color: 1058

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 458922 Color: 9381
Size: 286385 Color: 3315
Size: 254694 Color: 709

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 459045 Color: 9382
Size: 289361 Color: 3508
Size: 251595 Color: 280

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 459108 Color: 9383
Size: 286464 Color: 3323
Size: 254429 Color: 669

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 459144 Color: 9384
Size: 281420 Color: 2988
Size: 259437 Color: 1199

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 459381 Color: 9385
Size: 277360 Color: 2668
Size: 263260 Color: 1571

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 459399 Color: 9386
Size: 278127 Color: 2735
Size: 262475 Color: 1505

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 459421 Color: 9387
Size: 277270 Color: 2662
Size: 263310 Color: 1579

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 459453 Color: 9388
Size: 278445 Color: 2759
Size: 262103 Color: 1464

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 459480 Color: 9389
Size: 283398 Color: 3122
Size: 257123 Color: 967

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 459793 Color: 9390
Size: 285330 Color: 3240
Size: 254878 Color: 732

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 459969 Color: 9391
Size: 280516 Color: 2914
Size: 259516 Color: 1204

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 459988 Color: 9392
Size: 285847 Color: 3274
Size: 254166 Color: 635

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 460188 Color: 9393
Size: 281170 Color: 2962
Size: 258643 Color: 1116

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 460213 Color: 9394
Size: 279334 Color: 2822
Size: 260454 Color: 1290

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 460214 Color: 9395
Size: 279205 Color: 2812
Size: 260582 Color: 1305

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 460218 Color: 9396
Size: 280121 Color: 2887
Size: 259662 Color: 1216

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 460265 Color: 9397
Size: 275517 Color: 2546
Size: 264219 Color: 1654

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 460357 Color: 9398
Size: 283077 Color: 3097
Size: 256567 Color: 905

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 460372 Color: 9399
Size: 275203 Color: 2520
Size: 264426 Color: 1678

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 460447 Color: 9400
Size: 273815 Color: 2425
Size: 265739 Color: 1792

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 9403
Size: 278963 Color: 2789
Size: 260559 Color: 1302

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 9401
Size: 287382 Color: 3389
Size: 252140 Color: 355

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 460479 Color: 9402
Size: 288932 Color: 3480
Size: 250590 Color: 109

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 460492 Color: 9404
Size: 278577 Color: 2768
Size: 260932 Color: 1349

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 460523 Color: 9405
Size: 280633 Color: 2929
Size: 258845 Color: 1143

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 460562 Color: 9406
Size: 289347 Color: 3506
Size: 250092 Color: 17

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 460628 Color: 9407
Size: 276122 Color: 2584
Size: 263251 Color: 1569

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 460658 Color: 9408
Size: 286854 Color: 3347
Size: 252489 Color: 415

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 460667 Color: 9409
Size: 271157 Color: 2220
Size: 268177 Color: 1987

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 460684 Color: 9410
Size: 272470 Color: 2316
Size: 266847 Color: 1883

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 460777 Color: 9411
Size: 281320 Color: 2977
Size: 257904 Color: 1046

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 460780 Color: 9412
Size: 273422 Color: 2391
Size: 265799 Color: 1798

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 460792 Color: 9413
Size: 288043 Color: 3426
Size: 251166 Color: 219

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 460981 Color: 9414
Size: 288155 Color: 3434
Size: 250865 Color: 168

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 460982 Color: 9415
Size: 282543 Color: 3065
Size: 256476 Color: 896

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 460999 Color: 9416
Size: 278191 Color: 2745
Size: 260811 Color: 1338

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 461049 Color: 9417
Size: 283548 Color: 3130
Size: 255404 Color: 785

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 461080 Color: 9418
Size: 278967 Color: 2791
Size: 259954 Color: 1239

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 461085 Color: 9419
Size: 277455 Color: 2675
Size: 261461 Color: 1406

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 461115 Color: 9420
Size: 278200 Color: 2747
Size: 260686 Color: 1322

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 461116 Color: 9421
Size: 277790 Color: 2703
Size: 261095 Color: 1365

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 461125 Color: 9422
Size: 283063 Color: 3096
Size: 255813 Color: 827

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 461127 Color: 9423
Size: 270641 Color: 2177
Size: 268233 Color: 1991

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 461147 Color: 9424
Size: 281813 Color: 3026
Size: 257041 Color: 959

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 461278 Color: 9425
Size: 282013 Color: 3033
Size: 256710 Color: 921

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 461342 Color: 9426
Size: 283832 Color: 3149
Size: 254827 Color: 726

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 461368 Color: 9427
Size: 285399 Color: 3249
Size: 253234 Color: 506

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 461423 Color: 9428
Size: 269777 Color: 2121
Size: 268801 Color: 2033

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 461449 Color: 9429
Size: 275404 Color: 2538
Size: 263148 Color: 1558

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 461495 Color: 9430
Size: 281742 Color: 3018
Size: 256764 Color: 931

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 461521 Color: 9431
Size: 277859 Color: 2714
Size: 260621 Color: 1310

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 461556 Color: 9432
Size: 285459 Color: 3251
Size: 252986 Color: 475

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 461559 Color: 9433
Size: 271351 Color: 2233
Size: 267091 Color: 1899

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 461572 Color: 9434
Size: 275683 Color: 2555
Size: 262746 Color: 1531

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 461619 Color: 9436
Size: 278182 Color: 2743
Size: 260200 Color: 1267

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 461619 Color: 9435
Size: 286545 Color: 3328
Size: 251837 Color: 313

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 461679 Color: 9437
Size: 278812 Color: 2780
Size: 259510 Color: 1203

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 461686 Color: 9438
Size: 277747 Color: 2700
Size: 260568 Color: 1303

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 461754 Color: 9439
Size: 285892 Color: 3277
Size: 252355 Color: 394

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 461842 Color: 9440
Size: 281091 Color: 2959
Size: 257068 Color: 963

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 462025 Color: 9441
Size: 274397 Color: 2471
Size: 263579 Color: 1601

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 462070 Color: 9442
Size: 279593 Color: 2843
Size: 258338 Color: 1090

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 462158 Color: 9443
Size: 279567 Color: 2839
Size: 258276 Color: 1079

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 462187 Color: 9444
Size: 284879 Color: 3212
Size: 252935 Color: 466

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 462260 Color: 9445
Size: 277092 Color: 2649
Size: 260649 Color: 1319

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 462365 Color: 9446
Size: 285533 Color: 3254
Size: 252103 Color: 350

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 462376 Color: 9447
Size: 275214 Color: 2526
Size: 262411 Color: 1495

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 462412 Color: 9448
Size: 278149 Color: 2739
Size: 259440 Color: 1200

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 462468 Color: 9449
Size: 286369 Color: 3312
Size: 251164 Color: 218

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 462594 Color: 9450
Size: 283963 Color: 3160
Size: 253444 Color: 536

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 462607 Color: 9451
Size: 283344 Color: 3117
Size: 254050 Color: 615

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 462732 Color: 9452
Size: 284886 Color: 3213
Size: 252383 Color: 398

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 462746 Color: 9453
Size: 276237 Color: 2594
Size: 261018 Color: 1355

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 462794 Color: 9454
Size: 275128 Color: 2511
Size: 262079 Color: 1458

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 462848 Color: 9455
Size: 283429 Color: 3123
Size: 253724 Color: 578

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 462855 Color: 9456
Size: 273968 Color: 2437
Size: 263178 Color: 1560

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 462857 Color: 9457
Size: 280220 Color: 2892
Size: 256924 Color: 948

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 462902 Color: 9458
Size: 273872 Color: 2428
Size: 263227 Color: 1567

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 462907 Color: 9459
Size: 285263 Color: 3236
Size: 251831 Color: 312

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 462970 Color: 9460
Size: 283509 Color: 3127
Size: 253522 Color: 553

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 463012 Color: 9461
Size: 270057 Color: 2142
Size: 266932 Color: 1890

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 463042 Color: 9462
Size: 278881 Color: 2784
Size: 258078 Color: 1062

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 463071 Color: 9463
Size: 270894 Color: 2206
Size: 266036 Color: 1816

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 463078 Color: 9464
Size: 283328 Color: 3116
Size: 253595 Color: 565

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 463084 Color: 9465
Size: 269935 Color: 2136
Size: 266982 Color: 1894

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 463095 Color: 9466
Size: 280847 Color: 2944
Size: 256059 Color: 851

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 463200 Color: 9467
Size: 272248 Color: 2298
Size: 264553 Color: 1687

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 463396 Color: 9468
Size: 284517 Color: 3187
Size: 252088 Color: 346

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 463397 Color: 9469
Size: 285545 Color: 3255
Size: 251059 Color: 198

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 463407 Color: 9470
Size: 279408 Color: 2828
Size: 257186 Color: 977

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 463575 Color: 9471
Size: 281748 Color: 3019
Size: 254678 Color: 702

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 463585 Color: 9472
Size: 273232 Color: 2375
Size: 263184 Color: 1561

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 463616 Color: 9473
Size: 285799 Color: 3270
Size: 250586 Color: 108

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 463692 Color: 9474
Size: 276787 Color: 2636
Size: 259522 Color: 1205

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 463767 Color: 9475
Size: 283297 Color: 3113
Size: 252937 Color: 467

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 463834 Color: 9476
Size: 285031 Color: 3220
Size: 251136 Color: 214

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 463996 Color: 9477
Size: 282270 Color: 3047
Size: 253735 Color: 579

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 464055 Color: 9478
Size: 272358 Color: 2305
Size: 263588 Color: 1602

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 464060 Color: 9479
Size: 278867 Color: 2783
Size: 257074 Color: 964

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 464079 Color: 9480
Size: 278184 Color: 2744
Size: 257738 Color: 1032

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 464094 Color: 9481
Size: 274810 Color: 2495
Size: 261097 Color: 1366

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 464098 Color: 9482
Size: 269021 Color: 2051
Size: 266882 Color: 1886

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 464236 Color: 9483
Size: 283313 Color: 3114
Size: 252452 Color: 407

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 464426 Color: 9484
Size: 276587 Color: 2619
Size: 258988 Color: 1154

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 464455 Color: 9485
Size: 272614 Color: 2329
Size: 262932 Color: 1542

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 464549 Color: 9486
Size: 280079 Color: 2884
Size: 255373 Color: 781

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 464581 Color: 9487
Size: 281968 Color: 3029
Size: 253452 Color: 537

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 464582 Color: 9488
Size: 283149 Color: 3103
Size: 252270 Color: 382

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 464643 Color: 9489
Size: 276014 Color: 2576
Size: 259344 Color: 1190

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 464663 Color: 9490
Size: 284114 Color: 3169
Size: 251224 Color: 230

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 464665 Color: 9491
Size: 284690 Color: 3198
Size: 250646 Color: 123

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 464705 Color: 9492
Size: 276278 Color: 2601
Size: 259018 Color: 1157

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 464787 Color: 9493
Size: 273991 Color: 2439
Size: 261223 Color: 1385

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 464987 Color: 9494
Size: 278292 Color: 2752
Size: 256722 Color: 924

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 464997 Color: 9495
Size: 273229 Color: 2373
Size: 261775 Color: 1433

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 465003 Color: 9496
Size: 282138 Color: 3040
Size: 252860 Color: 455

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 465031 Color: 9497
Size: 274072 Color: 2445
Size: 260898 Color: 1344

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 465134 Color: 9498
Size: 278351 Color: 2755
Size: 256516 Color: 899

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 465135 Color: 9499
Size: 283232 Color: 3110
Size: 251634 Color: 287

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 465154 Color: 9500
Size: 279417 Color: 2830
Size: 255430 Color: 789

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 465184 Color: 9501
Size: 269485 Color: 2094
Size: 265332 Color: 1755

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 465292 Color: 9502
Size: 270180 Color: 2149
Size: 264529 Color: 1686

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 465324 Color: 9503
Size: 278212 Color: 2748
Size: 256465 Color: 894

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 465387 Color: 9504
Size: 281759 Color: 3021
Size: 252855 Color: 454

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 465428 Color: 9505
Size: 282512 Color: 3063
Size: 252061 Color: 343

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 465581 Color: 9506
Size: 269195 Color: 2064
Size: 265225 Color: 1747

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 465653 Color: 9507
Size: 282131 Color: 3039
Size: 252217 Color: 367

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 465708 Color: 9508
Size: 279909 Color: 2869
Size: 254384 Color: 665

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 465959 Color: 9510
Size: 269366 Color: 2082
Size: 264676 Color: 1694

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 465959 Color: 9509
Size: 267829 Color: 1967
Size: 266213 Color: 1835

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 465999 Color: 9511
Size: 274225 Color: 2458
Size: 259777 Color: 1226

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 466029 Color: 9512
Size: 281748 Color: 3020
Size: 252224 Color: 370

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 466149 Color: 9513
Size: 282610 Color: 3069
Size: 251242 Color: 233

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 466159 Color: 9514
Size: 279472 Color: 2832
Size: 254370 Color: 663

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 466160 Color: 9515
Size: 281448 Color: 2991
Size: 252393 Color: 399

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 466242 Color: 9516
Size: 279091 Color: 2807
Size: 254668 Color: 699

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 466358 Color: 9517
Size: 275360 Color: 2535
Size: 258283 Color: 1081

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 466399 Color: 9518
Size: 271192 Color: 2221
Size: 262410 Color: 1493

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 466431 Color: 9519
Size: 280683 Color: 2933
Size: 252887 Color: 461

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 466436 Color: 9520
Size: 272748 Color: 2342
Size: 260817 Color: 1340

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 466494 Color: 9521
Size: 277907 Color: 2719
Size: 255600 Color: 804

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 466613 Color: 9522
Size: 269960 Color: 2138
Size: 263428 Color: 1589

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 466695 Color: 9523
Size: 269951 Color: 2137
Size: 263355 Color: 1583

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 466713 Color: 9524
Size: 275820 Color: 2567
Size: 257468 Color: 1006

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 466783 Color: 9525
Size: 274530 Color: 2480
Size: 258688 Color: 1124

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 466849 Color: 9526
Size: 281538 Color: 2999
Size: 251614 Color: 284

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 467015 Color: 9527
Size: 272979 Color: 2353
Size: 260007 Color: 1246

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 467116 Color: 9528
Size: 281287 Color: 2974
Size: 251598 Color: 281

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 467172 Color: 9529
Size: 279967 Color: 2875
Size: 252862 Color: 456

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 467234 Color: 9530
Size: 269700 Color: 2115
Size: 263067 Color: 1550

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 467277 Color: 9531
Size: 279253 Color: 2814
Size: 253471 Color: 541

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 467648 Color: 9532
Size: 266642 Color: 1870
Size: 265711 Color: 1789

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 467665 Color: 9533
Size: 273949 Color: 2435
Size: 258387 Color: 1098

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 467665 Color: 9534
Size: 277837 Color: 2710
Size: 254499 Color: 673

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 467726 Color: 9535
Size: 269312 Color: 2075
Size: 262963 Color: 1545

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 467763 Color: 9536
Size: 281094 Color: 2960
Size: 251144 Color: 215

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 467909 Color: 9537
Size: 270030 Color: 2140
Size: 262062 Color: 1454

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 467916 Color: 9538
Size: 271617 Color: 2244
Size: 260468 Color: 1291

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 467936 Color: 9539
Size: 271060 Color: 2217
Size: 261005 Color: 1354

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 467985 Color: 9540
Size: 275296 Color: 2531
Size: 256720 Color: 923

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 468056 Color: 9541
Size: 273060 Color: 2361
Size: 258885 Color: 1146

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 468058 Color: 9542
Size: 274572 Color: 2486
Size: 257371 Color: 995

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 468070 Color: 9543
Size: 274432 Color: 2474
Size: 257499 Color: 1009

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 468141 Color: 9544
Size: 277846 Color: 2712
Size: 254014 Color: 608

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 468251 Color: 9545
Size: 275412 Color: 2540
Size: 256338 Color: 878

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 468252 Color: 9546
Size: 274421 Color: 2473
Size: 257328 Color: 991

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 468285 Color: 9547
Size: 275507 Color: 2545
Size: 256209 Color: 868

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 468411 Color: 9548
Size: 281085 Color: 2958
Size: 250505 Color: 94

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 468601 Color: 9549
Size: 266830 Color: 1881
Size: 264570 Color: 1688

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 468609 Color: 9550
Size: 265959 Color: 1809
Size: 265433 Color: 1764

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 468777 Color: 9551
Size: 272328 Color: 2302
Size: 258896 Color: 1148

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 468808 Color: 9552
Size: 276361 Color: 2606
Size: 254832 Color: 728

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 468818 Color: 9553
Size: 267635 Color: 1950
Size: 263548 Color: 1598

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 468962 Color: 9554
Size: 274675 Color: 2492
Size: 256364 Color: 880

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 468963 Color: 9555
Size: 266551 Color: 1861
Size: 264487 Color: 1680

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 468981 Color: 9556
Size: 277243 Color: 2659
Size: 253777 Color: 582

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 469000 Color: 9557
Size: 277893 Color: 2718
Size: 253108 Color: 486

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 469120 Color: 9558
Size: 266037 Color: 1817
Size: 264844 Color: 1712

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 469205 Color: 9559
Size: 275418 Color: 2541
Size: 255378 Color: 782

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 469209 Color: 9560
Size: 273461 Color: 2396
Size: 257331 Color: 992

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 469317 Color: 9561
Size: 279610 Color: 2847
Size: 251074 Color: 204

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 469322 Color: 9562
Size: 277320 Color: 2667
Size: 253359 Color: 519

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 469343 Color: 9563
Size: 265569 Color: 1778
Size: 265089 Color: 1735

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 469376 Color: 9564
Size: 270604 Color: 2174
Size: 260021 Color: 1248

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 469383 Color: 9565
Size: 276243 Color: 2595
Size: 254375 Color: 664

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 469498 Color: 9566
Size: 279992 Color: 2877
Size: 250511 Color: 95

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 469503 Color: 9567
Size: 272114 Color: 2284
Size: 258384 Color: 1097

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 469576 Color: 9568
Size: 276512 Color: 2618
Size: 253913 Color: 596

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 469680 Color: 9569
Size: 271678 Color: 2250
Size: 258643 Color: 1117

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 469772 Color: 9570
Size: 273023 Color: 2358
Size: 257206 Color: 981

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 469851 Color: 9571
Size: 269360 Color: 2081
Size: 260790 Color: 1335

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 469896 Color: 9572
Size: 276823 Color: 2637
Size: 253282 Color: 512

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 469937 Color: 9573
Size: 268642 Color: 2021
Size: 261422 Color: 1403

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 469981 Color: 9574
Size: 269347 Color: 2080
Size: 260673 Color: 1321

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 470000 Color: 9575
Size: 273080 Color: 2367
Size: 256921 Color: 947

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 470032 Color: 9576
Size: 272265 Color: 2299
Size: 257704 Color: 1028

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 470105 Color: 9577
Size: 267473 Color: 1929
Size: 262423 Color: 1497

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 470145 Color: 9578
Size: 269577 Color: 2109
Size: 260279 Color: 1273

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 470250 Color: 9579
Size: 271312 Color: 2230
Size: 258439 Color: 1103

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 470256 Color: 9580
Size: 276262 Color: 2596
Size: 253483 Color: 544

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 470375 Color: 9581
Size: 273064 Color: 2363
Size: 256562 Color: 904

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 470440 Color: 9582
Size: 265449 Color: 1766
Size: 264112 Color: 1643

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 470441 Color: 9583
Size: 277393 Color: 2672
Size: 252167 Color: 362

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 470463 Color: 9584
Size: 279068 Color: 2804
Size: 250470 Color: 85

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 470799 Color: 9585
Size: 270849 Color: 2199
Size: 258353 Color: 1093

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 470837 Color: 9586
Size: 266652 Color: 1871
Size: 262512 Color: 1510

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 470863 Color: 9587
Size: 275101 Color: 2509
Size: 254037 Color: 611

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 470868 Color: 9588
Size: 277554 Color: 2681
Size: 251579 Color: 276

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 470886 Color: 9589
Size: 276625 Color: 2624
Size: 252490 Color: 416

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 470919 Color: 9590
Size: 270609 Color: 2175
Size: 258473 Color: 1109

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 470929 Color: 9591
Size: 279025 Color: 2797
Size: 250047 Color: 9

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 470935 Color: 9592
Size: 277602 Color: 2686
Size: 251464 Color: 260

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 470937 Color: 9593
Size: 268852 Color: 2038
Size: 260212 Color: 1269

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 471011 Color: 9594
Size: 264759 Color: 1705
Size: 264231 Color: 1655

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 471014 Color: 9595
Size: 274390 Color: 2469
Size: 254597 Color: 690

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 471050 Color: 9596
Size: 276307 Color: 2603
Size: 252644 Color: 432

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 471177 Color: 9597
Size: 274314 Color: 2464
Size: 254510 Color: 675

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 471241 Color: 9598
Size: 273371 Color: 2388
Size: 255389 Color: 783

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 471377 Color: 9599
Size: 272659 Color: 2334
Size: 255965 Color: 842

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 471420 Color: 9600
Size: 267959 Color: 1974
Size: 260622 Color: 1311

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 471423 Color: 9601
Size: 276272 Color: 2600
Size: 252306 Color: 390

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 471466 Color: 9602
Size: 270331 Color: 2157
Size: 258204 Color: 1071

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 471517 Color: 9603
Size: 276361 Color: 2607
Size: 252123 Color: 353

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 471557 Color: 9604
Size: 272892 Color: 2349
Size: 255552 Color: 797

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 471612 Color: 9605
Size: 275159 Color: 2514
Size: 253230 Color: 505

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 471685 Color: 9606
Size: 272213 Color: 2290
Size: 256103 Color: 854

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 471808 Color: 9607
Size: 266652 Color: 1872
Size: 261541 Color: 1414

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 471857 Color: 9608
Size: 264242 Color: 1657
Size: 263902 Color: 1626

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 471961 Color: 9609
Size: 265274 Color: 1749
Size: 262766 Color: 1533

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 472023 Color: 9610
Size: 274117 Color: 2450
Size: 253861 Color: 588

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 472023 Color: 9611
Size: 273322 Color: 2381
Size: 254656 Color: 696

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 472043 Color: 9612
Size: 273943 Color: 2434
Size: 254015 Color: 609

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 472127 Color: 9613
Size: 271279 Color: 2228
Size: 256595 Color: 910

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 472132 Color: 9614
Size: 274841 Color: 2497
Size: 253028 Color: 479

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 472232 Color: 9615
Size: 273011 Color: 2357
Size: 254758 Color: 717

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 472234 Color: 9616
Size: 277100 Color: 2650
Size: 250667 Color: 127

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 472401 Color: 9617
Size: 269260 Color: 2071
Size: 258340 Color: 1091

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 472467 Color: 9618
Size: 268515 Color: 2011
Size: 259019 Color: 1158

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 472480 Color: 9619
Size: 271315 Color: 2232
Size: 256206 Color: 866

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 472514 Color: 9620
Size: 272027 Color: 2279
Size: 255460 Color: 790

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 472526 Color: 9621
Size: 273585 Color: 2408
Size: 253890 Color: 592

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 472606 Color: 9622
Size: 274964 Color: 2503
Size: 252431 Color: 405

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 472699 Color: 9623
Size: 277178 Color: 2656
Size: 250124 Color: 25

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 472760 Color: 9624
Size: 264457 Color: 1679
Size: 262784 Color: 1534

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 472788 Color: 9625
Size: 267164 Color: 1906
Size: 260049 Color: 1253

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 472932 Color: 9626
Size: 276615 Color: 2623
Size: 250454 Color: 78

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 473011 Color: 9627
Size: 265172 Color: 1742
Size: 261818 Color: 1438

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 473066 Color: 9628
Size: 264503 Color: 1683
Size: 262432 Color: 1498

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 473097 Color: 9629
Size: 275407 Color: 2539
Size: 251497 Color: 265

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 473201 Color: 9630
Size: 264908 Color: 1719
Size: 261892 Color: 1442

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 473203 Color: 9631
Size: 273526 Color: 2402
Size: 253272 Color: 511

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 473326 Color: 9632
Size: 274509 Color: 2479
Size: 252166 Color: 361

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 473373 Color: 9633
Size: 272339 Color: 2304
Size: 254289 Color: 649

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 473592 Color: 9634
Size: 272248 Color: 2297
Size: 254161 Color: 634

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 473698 Color: 9635
Size: 268114 Color: 1982
Size: 258189 Color: 1070

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 473812 Color: 9636
Size: 265908 Color: 1804
Size: 260281 Color: 1274

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 473868 Color: 9637
Size: 271812 Color: 2260
Size: 254321 Color: 654

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 473872 Color: 9638
Size: 272229 Color: 2293
Size: 253900 Color: 593

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 473910 Color: 9639
Size: 272021 Color: 2278
Size: 254070 Color: 619

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 473981 Color: 9640
Size: 269916 Color: 2134
Size: 256104 Color: 855

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 473987 Color: 9641
Size: 269288 Color: 2073
Size: 256726 Color: 925

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 474066 Color: 9642
Size: 267706 Color: 1954
Size: 258229 Color: 1074

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 474089 Color: 9643
Size: 269222 Color: 2068
Size: 256690 Color: 918

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 474221 Color: 9644
Size: 266132 Color: 1827
Size: 259648 Color: 1215

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 474320 Color: 9645
Size: 274071 Color: 2444
Size: 251610 Color: 283

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 474396 Color: 9646
Size: 267824 Color: 1965
Size: 257781 Color: 1038

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 474501 Color: 9647
Size: 267182 Color: 1907
Size: 258318 Color: 1086

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 474514 Color: 9648
Size: 267309 Color: 1916
Size: 258178 Color: 1069

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 474543 Color: 9649
Size: 272308 Color: 2301
Size: 253150 Color: 494

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 474569 Color: 9650
Size: 264310 Color: 1665
Size: 261122 Color: 1369

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 474573 Color: 9651
Size: 274972 Color: 2504
Size: 250456 Color: 80

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 474674 Color: 9652
Size: 268172 Color: 1986
Size: 257155 Color: 970

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 475170 Color: 9653
Size: 272412 Color: 2310
Size: 252419 Color: 403

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 475354 Color: 9654
Size: 273680 Color: 2413
Size: 250967 Color: 181

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 475375 Color: 9655
Size: 269107 Color: 2056
Size: 255519 Color: 795

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 475394 Color: 9656
Size: 274058 Color: 2443
Size: 250549 Color: 103

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 475478 Color: 9657
Size: 273989 Color: 2438
Size: 250534 Color: 100

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 475482 Color: 9658
Size: 263595 Color: 1603
Size: 260924 Color: 1348

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 475571 Color: 9659
Size: 270863 Color: 2200
Size: 253567 Color: 562

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 475587 Color: 9660
Size: 263603 Color: 1604
Size: 260811 Color: 1337

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 475620 Color: 9661
Size: 268250 Color: 1993
Size: 256131 Color: 859

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 475623 Color: 9662
Size: 269534 Color: 2105
Size: 254844 Color: 730

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 475642 Color: 9663
Size: 273999 Color: 2440
Size: 250360 Color: 65

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 475693 Color: 9664
Size: 271789 Color: 2257
Size: 252519 Color: 417

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 475751 Color: 9665
Size: 265717 Color: 1790
Size: 258533 Color: 1111

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 475905 Color: 9666
Size: 265075 Color: 1732
Size: 259021 Color: 1159

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 475937 Color: 9667
Size: 266777 Color: 1880
Size: 257287 Color: 988

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 475972 Color: 9668
Size: 263520 Color: 1596
Size: 260509 Color: 1298

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 476015 Color: 9669
Size: 266200 Color: 1832
Size: 257786 Color: 1039

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 476077 Color: 9670
Size: 269525 Color: 2101
Size: 254399 Color: 667

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 476171 Color: 9671
Size: 272539 Color: 2321
Size: 251291 Color: 240

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 476191 Color: 9672
Size: 265078 Color: 1733
Size: 258732 Color: 1127

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 476296 Color: 9673
Size: 271239 Color: 2225
Size: 252466 Color: 411

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 476387 Color: 9674
Size: 266324 Color: 1845
Size: 257290 Color: 989

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 476410 Color: 9675
Size: 270873 Color: 2202
Size: 252718 Color: 441

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 476461 Color: 9676
Size: 272414 Color: 2311
Size: 251126 Color: 212

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 476573 Color: 9677
Size: 271997 Color: 2273
Size: 251431 Color: 257

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 476761 Color: 9678
Size: 272441 Color: 2312
Size: 250799 Color: 157

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 476837 Color: 9679
Size: 271632 Color: 2245
Size: 251532 Color: 270

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 476944 Color: 9680
Size: 267450 Color: 1926
Size: 255607 Color: 805

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 476959 Color: 9681
Size: 261699 Color: 1426
Size: 261343 Color: 1397

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 477024 Color: 9682
Size: 268858 Color: 2040
Size: 254119 Color: 627

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 477053 Color: 9683
Size: 268260 Color: 1994
Size: 254688 Color: 708

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 477079 Color: 9684
Size: 265010 Color: 1727
Size: 257912 Color: 1047

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 477122 Color: 9685
Size: 261642 Color: 1419
Size: 261237 Color: 1388

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 477204 Color: 9686
Size: 270201 Color: 2152
Size: 252596 Color: 429

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 477206 Color: 9687
Size: 263785 Color: 1618
Size: 259010 Color: 1156

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 477212 Color: 9688
Size: 264053 Color: 1639
Size: 258736 Color: 1130

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 477229 Color: 9689
Size: 261433 Color: 1404
Size: 261339 Color: 1396

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 477308 Color: 9690
Size: 269043 Color: 2052
Size: 253650 Color: 572

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 477377 Color: 9691
Size: 263689 Color: 1612
Size: 258935 Color: 1150

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 477387 Color: 9692
Size: 271301 Color: 2229
Size: 251313 Color: 245

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 477421 Color: 9693
Size: 271699 Color: 2252
Size: 250881 Color: 170

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 477652 Color: 9694
Size: 270258 Color: 2154
Size: 252091 Color: 348

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 477942 Color: 9695
Size: 264877 Color: 1716
Size: 257182 Color: 974

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 477953 Color: 9696
Size: 268826 Color: 2035
Size: 253222 Color: 502

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 477979 Color: 9697
Size: 262190 Color: 1470
Size: 259832 Color: 1228

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 478006 Color: 9698
Size: 262235 Color: 1474
Size: 259760 Color: 1224

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 478056 Color: 9699
Size: 267672 Color: 1952
Size: 254273 Color: 645

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 478090 Color: 9700
Size: 265419 Color: 1762
Size: 256492 Color: 897

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 478100 Color: 9701
Size: 268029 Color: 1975
Size: 253872 Color: 589

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 478162 Color: 9702
Size: 262758 Color: 1532
Size: 259081 Color: 1164

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 478329 Color: 9703
Size: 266844 Color: 1882
Size: 254828 Color: 727

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 478332 Color: 9704
Size: 269384 Color: 2087
Size: 252285 Color: 384

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 478432 Color: 9705
Size: 268117 Color: 1983
Size: 253452 Color: 538

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 478513 Color: 9706
Size: 270364 Color: 2159
Size: 251124 Color: 211

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 478519 Color: 9707
Size: 265398 Color: 1760
Size: 256084 Color: 853

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 478570 Color: 9708
Size: 266449 Color: 1854
Size: 254982 Color: 741

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 478607 Color: 9709
Size: 263166 Color: 1559
Size: 258228 Color: 1073

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 478711 Color: 9710
Size: 267152 Color: 1904
Size: 254138 Color: 632

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 478782 Color: 9711
Size: 267283 Color: 1912
Size: 253936 Color: 602

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 478960 Color: 9712
Size: 262059 Color: 1453
Size: 258982 Color: 1153

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 478967 Color: 9713
Size: 270808 Color: 2194
Size: 250226 Color: 43

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 478970 Color: 9714
Size: 261034 Color: 1357
Size: 259997 Color: 1245

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 479042 Color: 9715
Size: 270659 Color: 2181
Size: 250300 Color: 55

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 479133 Color: 9716
Size: 261898 Color: 1443
Size: 258970 Color: 1152

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 479223 Color: 9717
Size: 264771 Color: 1707
Size: 256007 Color: 845

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 479407 Color: 9718
Size: 269127 Color: 2062
Size: 251467 Color: 261

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 479519 Color: 9719
Size: 260922 Color: 1347
Size: 259560 Color: 1208

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 479556 Color: 9720
Size: 263850 Color: 1622
Size: 256595 Color: 911

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 479577 Color: 9721
Size: 265009 Color: 1726
Size: 255415 Color: 787

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 479598 Color: 9722
Size: 270399 Color: 2163
Size: 250004 Color: 2

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 479600 Color: 9723
Size: 263270 Color: 1572
Size: 257131 Color: 968

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 479740 Color: 9724
Size: 269619 Color: 2112
Size: 250642 Color: 122

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 479747 Color: 9725
Size: 261146 Color: 1371
Size: 259108 Color: 1167

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 479787 Color: 9726
Size: 269889 Color: 2131
Size: 250325 Color: 60

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 479833 Color: 9727
Size: 265452 Color: 1767
Size: 254716 Color: 714

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 480018 Color: 9728
Size: 267887 Color: 1971
Size: 252096 Color: 349

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 480063 Color: 9729
Size: 260764 Color: 1332
Size: 259174 Color: 1177

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 480195 Color: 9730
Size: 265989 Color: 1813
Size: 253817 Color: 586

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 480196 Color: 9731
Size: 261771 Color: 1432
Size: 258034 Color: 1059

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 480314 Color: 9732
Size: 267002 Color: 1895
Size: 252685 Color: 437

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 480322 Color: 9733
Size: 268829 Color: 2036
Size: 250850 Color: 164

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 480395 Color: 9734
Size: 262448 Color: 1501
Size: 257158 Color: 971

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 480407 Color: 9735
Size: 262513 Color: 1511
Size: 257081 Color: 965

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 480509 Color: 9736
Size: 268996 Color: 2049
Size: 250496 Color: 93

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 480595 Color: 9737
Size: 264180 Color: 1650
Size: 255226 Color: 765

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 480596 Color: 9738
Size: 269123 Color: 2061
Size: 250282 Color: 53

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 480674 Color: 9739
Size: 266873 Color: 1884
Size: 252454 Color: 408

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 480684 Color: 9740
Size: 267154 Color: 1905
Size: 252163 Color: 358

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 480711 Color: 9741
Size: 262604 Color: 1521
Size: 256686 Color: 917

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 481075 Color: 9742
Size: 261070 Color: 1359
Size: 257856 Color: 1044

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 481130 Color: 9743
Size: 260712 Color: 1327
Size: 258159 Color: 1067

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 481254 Color: 9744
Size: 266610 Color: 1865
Size: 252137 Color: 354

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 481273 Color: 9745
Size: 260369 Color: 1282
Size: 258359 Color: 1095

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 481338 Color: 9746
Size: 259524 Color: 1206
Size: 259139 Color: 1171

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 481518 Color: 9747
Size: 260355 Color: 1281
Size: 258128 Color: 1064

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 481526 Color: 9748
Size: 265099 Color: 1736
Size: 253376 Color: 522

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 481683 Color: 9749
Size: 259214 Color: 1183
Size: 259104 Color: 1166

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 481689 Color: 9750
Size: 267018 Color: 1896
Size: 251294 Color: 241

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 481735 Color: 9751
Size: 260295 Color: 1275
Size: 257971 Color: 1055

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 481841 Color: 9752
Size: 263802 Color: 1619
Size: 254358 Color: 659

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 481845 Color: 9753
Size: 262447 Color: 1500
Size: 255709 Color: 819

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 482076 Color: 9754
Size: 260666 Color: 1320
Size: 257259 Color: 985

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 482112 Color: 9755
Size: 263075 Color: 1552
Size: 254814 Color: 721

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 482120 Color: 9756
Size: 265328 Color: 1754
Size: 252553 Color: 424

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 482151 Color: 9757
Size: 260970 Color: 1351
Size: 256880 Color: 941

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 482250 Color: 9758
Size: 261217 Color: 1383
Size: 256534 Color: 903

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 482254 Color: 9759
Size: 259424 Color: 1197
Size: 258323 Color: 1087

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 482273 Color: 9760
Size: 261324 Color: 1393
Size: 256404 Color: 884

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 482432 Color: 9762
Size: 258909 Color: 1149
Size: 258660 Color: 1120

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 482432 Color: 9761
Size: 260918 Color: 1345
Size: 256651 Color: 915

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 482454 Color: 9763
Size: 260690 Color: 1324
Size: 256857 Color: 939

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 482455 Color: 9764
Size: 259915 Color: 1236
Size: 257631 Color: 1019

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 482527 Color: 9765
Size: 261888 Color: 1441
Size: 255586 Color: 803

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 482551 Color: 9766
Size: 260057 Color: 1254
Size: 257393 Color: 999

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 482620 Color: 9767
Size: 262022 Color: 1449
Size: 255359 Color: 780

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 482747 Color: 9768
Size: 260091 Color: 1258
Size: 257163 Color: 972

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 482771 Color: 9769
Size: 266437 Color: 1853
Size: 250793 Color: 155

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 482822 Color: 9770
Size: 265460 Color: 1771
Size: 251719 Color: 297

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 482902 Color: 9771
Size: 260974 Color: 1352
Size: 256125 Color: 858

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 482989 Color: 9772
Size: 263520 Color: 1597
Size: 253492 Color: 547

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 482991 Color: 9773
Size: 259550 Color: 1207
Size: 257460 Color: 1003

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 483079 Color: 9774
Size: 259886 Color: 1233
Size: 257036 Color: 957

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 483127 Color: 9775
Size: 266394 Color: 1850
Size: 250480 Color: 89

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 483147 Color: 9776
Size: 265788 Color: 1796
Size: 251066 Color: 200

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 483231 Color: 9777
Size: 261202 Color: 1381
Size: 255568 Color: 801

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 483301 Color: 9778
Size: 262888 Color: 1540
Size: 253812 Color: 584

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 483315 Color: 9779
Size: 264027 Color: 1637
Size: 252659 Color: 435

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 483332 Color: 9780
Size: 261457 Color: 1405
Size: 255212 Color: 762

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 483452 Color: 9781
Size: 266306 Color: 1840
Size: 250243 Color: 46

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 483453 Color: 9782
Size: 265509 Color: 1775
Size: 251039 Color: 193

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 483522 Color: 9783
Size: 261055 Color: 1358
Size: 255424 Color: 788

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 483528 Color: 9784
Size: 261564 Color: 1415
Size: 254909 Color: 735

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 483549 Color: 9785
Size: 262382 Color: 1489
Size: 254070 Color: 618

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 483555 Color: 9786
Size: 262381 Color: 1488
Size: 254065 Color: 616

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 483633 Color: 9787
Size: 258698 Color: 1125
Size: 257670 Color: 1024

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 483661 Color: 9788
Size: 263073 Color: 1551
Size: 253267 Color: 510

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 483936 Color: 9789
Size: 263026 Color: 1547
Size: 253039 Color: 480

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 483944 Color: 9790
Size: 264342 Color: 1667
Size: 251715 Color: 295

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 483983 Color: 9791
Size: 265389 Color: 1759
Size: 250629 Color: 119

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 484111 Color: 9792
Size: 259143 Color: 1172
Size: 256747 Color: 928

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 484408 Color: 9793
Size: 261673 Color: 1423
Size: 253920 Color: 598

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 484533 Color: 9794
Size: 260647 Color: 1318
Size: 254821 Color: 724

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 484714 Color: 9795
Size: 264716 Color: 1698
Size: 250571 Color: 107

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 484780 Color: 9796
Size: 257886 Color: 1045
Size: 257335 Color: 993

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 484789 Color: 9797
Size: 260704 Color: 1325
Size: 254508 Color: 674

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 484804 Color: 9798
Size: 263371 Color: 1584
Size: 251826 Color: 310

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 484818 Color: 9799
Size: 261091 Color: 1363
Size: 254092 Color: 622

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 484821 Color: 9800
Size: 264318 Color: 1666
Size: 250862 Color: 167

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 484827 Color: 9801
Size: 261614 Color: 1417
Size: 253560 Color: 560

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 484940 Color: 9802
Size: 264880 Color: 1718
Size: 250181 Color: 33

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 484969 Color: 9803
Size: 263441 Color: 1591
Size: 251591 Color: 279

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 485030 Color: 9804
Size: 262136 Color: 1466
Size: 252835 Color: 450

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 485105 Color: 9805
Size: 259891 Color: 1234
Size: 255005 Color: 745

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 485326 Color: 9806
Size: 263932 Color: 1628
Size: 250743 Color: 142

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 485365 Color: 9807
Size: 263339 Color: 1581
Size: 251297 Color: 242

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 485380 Color: 9808
Size: 262685 Color: 1526
Size: 251936 Color: 327

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 485427 Color: 9809
Size: 258895 Color: 1147
Size: 255679 Color: 816

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 485554 Color: 9810
Size: 257708 Color: 1029
Size: 256739 Color: 927

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 485614 Color: 9811
Size: 258569 Color: 1114
Size: 255818 Color: 829

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 485622 Color: 9812
Size: 262351 Color: 1485
Size: 252028 Color: 338

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 485677 Color: 9813
Size: 262696 Color: 1528
Size: 251628 Color: 286

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 485707 Color: 9814
Size: 257255 Color: 984
Size: 257039 Color: 958

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 485727 Color: 9815
Size: 260723 Color: 1328
Size: 253551 Color: 558

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 485980 Color: 9816
Size: 259127 Color: 1169
Size: 254894 Color: 733

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 486106 Color: 9817
Size: 257855 Color: 1043
Size: 256040 Color: 850

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 486123 Color: 9818
Size: 263667 Color: 1610
Size: 250211 Color: 39

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 486153 Color: 9819
Size: 257642 Color: 1022
Size: 256206 Color: 867

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 486222 Color: 9820
Size: 261748 Color: 1429
Size: 252031 Color: 339

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 486231 Color: 9821
Size: 263714 Color: 1613
Size: 250056 Color: 12

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 486284 Color: 9822
Size: 257966 Color: 1053
Size: 255751 Color: 822

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 486329 Color: 9823
Size: 261198 Color: 1379
Size: 252474 Color: 412

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 486391 Color: 9824
Size: 261154 Color: 1373
Size: 252456 Color: 409

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 486443 Color: 9825
Size: 262083 Color: 1460
Size: 251475 Color: 264

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 486621 Color: 9826
Size: 260091 Color: 1259
Size: 253289 Color: 513

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 486650 Color: 9827
Size: 262645 Color: 1523
Size: 250706 Color: 134

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 486665 Color: 9828
Size: 258752 Color: 1132
Size: 254584 Color: 687

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 486689 Color: 9829
Size: 259005 Color: 1155
Size: 254307 Color: 653

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 487134 Color: 9830
Size: 258732 Color: 1128
Size: 254135 Color: 629

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 487136 Color: 9831
Size: 262692 Color: 1527
Size: 250173 Color: 30

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 487140 Color: 9832
Size: 258764 Color: 1134
Size: 254097 Color: 625

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 487214 Color: 9833
Size: 256452 Color: 891
Size: 256335 Color: 877

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 487302 Color: 9834
Size: 261337 Color: 1395
Size: 251362 Color: 252

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 487439 Color: 9835
Size: 256710 Color: 922
Size: 255852 Color: 831

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 487539 Color: 9836
Size: 256986 Color: 954
Size: 255476 Color: 793

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 487564 Color: 9837
Size: 261814 Color: 1437
Size: 250623 Color: 118

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 487622 Color: 9838
Size: 259225 Color: 1184
Size: 253154 Color: 496

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 487687 Color: 9839
Size: 260992 Color: 1353
Size: 251322 Color: 246

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 487849 Color: 9840
Size: 260262 Color: 1271
Size: 251890 Color: 323

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 487936 Color: 9841
Size: 259088 Color: 1165
Size: 252977 Color: 474

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 488000 Color: 9842
Size: 261510 Color: 1413
Size: 250491 Color: 91

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 488041 Color: 9843
Size: 261280 Color: 1389
Size: 250680 Color: 128

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 488151 Color: 9844
Size: 258306 Color: 1083
Size: 253544 Color: 557

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 488197 Color: 9845
Size: 260274 Color: 1272
Size: 251530 Color: 269

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 488320 Color: 9846
Size: 258283 Color: 1082
Size: 253398 Color: 526

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 488348 Color: 9847
Size: 257518 Color: 1010
Size: 254135 Color: 630

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 488421 Color: 9848
Size: 258085 Color: 1063
Size: 253495 Color: 548

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 488684 Color: 9849
Size: 261224 Color: 1386
Size: 250093 Color: 18

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 488721 Color: 9850
Size: 260525 Color: 1301
Size: 250755 Color: 145

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 488749 Color: 9851
Size: 258700 Color: 1126
Size: 252552 Color: 423

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 488775 Color: 9852
Size: 257636 Color: 1020
Size: 253590 Color: 564

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 488864 Color: 9853
Size: 260420 Color: 1286
Size: 250717 Color: 137

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 488945 Color: 9854
Size: 257936 Color: 1050
Size: 253120 Color: 488

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 488972 Color: 9855
Size: 258814 Color: 1137
Size: 252215 Color: 365

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 488995 Color: 9856
Size: 260063 Color: 1255
Size: 250943 Color: 178

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 489010 Color: 9857
Size: 260135 Color: 1260
Size: 250856 Color: 165

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 489022 Color: 9858
Size: 255575 Color: 802
Size: 255404 Color: 786

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 489115 Color: 9859
Size: 260516 Color: 1299
Size: 250370 Color: 67

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 489160 Color: 9860
Size: 258758 Color: 1133
Size: 252083 Color: 345

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 489252 Color: 9861
Size: 257757 Color: 1036
Size: 252992 Color: 476

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 489260 Color: 9862
Size: 256060 Color: 852
Size: 254681 Color: 705

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 489276 Color: 9863
Size: 257153 Color: 969
Size: 253572 Color: 563

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 489365 Color: 9864
Size: 259589 Color: 1211
Size: 251047 Color: 195

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 489427 Color: 9865
Size: 260022 Color: 1249
Size: 250552 Color: 104

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 489498 Color: 9866
Size: 259642 Color: 1214
Size: 250861 Color: 166

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 489650 Color: 9867
Size: 256946 Color: 950
Size: 253405 Color: 527

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 489657 Color: 9868
Size: 257914 Color: 1048
Size: 252430 Color: 404

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 489805 Color: 9869
Size: 258549 Color: 1112
Size: 251647 Color: 288

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 489909 Color: 9870
Size: 257468 Color: 1007
Size: 252624 Color: 431

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 489910 Color: 9871
Size: 255080 Color: 751
Size: 255011 Color: 746

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 489997 Color: 9872
Size: 257052 Color: 960
Size: 252952 Color: 469

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 489999 Color: 9873
Size: 255907 Color: 837
Size: 254095 Color: 624

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 490060 Color: 9874
Size: 255668 Color: 813
Size: 254273 Color: 646

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 490082 Color: 9875
Size: 259186 Color: 1179
Size: 250733 Color: 140

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 490160 Color: 9876
Size: 259209 Color: 1182
Size: 250632 Color: 121

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 490189 Color: 9877
Size: 257497 Color: 1008
Size: 252315 Color: 391

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 490216 Color: 9878
Size: 257923 Color: 1049
Size: 251862 Color: 318

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 490266 Color: 9879
Size: 254931 Color: 739
Size: 254804 Color: 720

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 490427 Color: 9880
Size: 258615 Color: 1115
Size: 250959 Color: 180

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 490472 Color: 9881
Size: 256020 Color: 847
Size: 253509 Color: 551

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 490588 Color: 9882
Size: 257442 Color: 1002
Size: 251971 Color: 331

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 490599 Color: 9883
Size: 258676 Color: 1121
Size: 250726 Color: 138

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 490668 Color: 9884
Size: 255803 Color: 825
Size: 253530 Color: 556

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 490691 Color: 9885
Size: 254680 Color: 704
Size: 254630 Color: 692

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 490772 Color: 9886
Size: 256389 Color: 882
Size: 252840 Color: 452

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 490829 Color: 9887
Size: 257828 Color: 1041
Size: 251344 Color: 249

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 490831 Color: 9888
Size: 254912 Color: 737
Size: 254258 Color: 641

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 490867 Color: 9889
Size: 257219 Color: 982
Size: 251915 Color: 326

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 490988 Color: 9890
Size: 258676 Color: 1122
Size: 250337 Color: 63

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 491170 Color: 9891
Size: 257605 Color: 1017
Size: 251226 Color: 231

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 491352 Color: 9892
Size: 254926 Color: 738
Size: 253723 Color: 577

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 491363 Color: 9893
Size: 256696 Color: 920
Size: 251942 Color: 329

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 491393 Color: 9894
Size: 256326 Color: 876
Size: 252282 Color: 383

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 491653 Color: 9895
Size: 256178 Color: 865
Size: 252170 Color: 363

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 491691 Color: 9896
Size: 256590 Color: 909
Size: 251720 Color: 299

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 491833 Color: 9897
Size: 254259 Color: 642
Size: 253909 Color: 594

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 491940 Color: 9898
Size: 256795 Color: 934
Size: 251266 Color: 237

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 491990 Color: 9899
Size: 254323 Color: 655
Size: 253688 Color: 573

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 492102 Color: 9900
Size: 254686 Color: 707
Size: 253213 Color: 500

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 492162 Color: 9901
Size: 256775 Color: 933
Size: 251064 Color: 199

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 492290 Color: 9902
Size: 255466 Color: 791
Size: 252245 Color: 378

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 492325 Color: 9903
Size: 255219 Color: 763
Size: 252457 Color: 410

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 492341 Color: 9904
Size: 254814 Color: 722
Size: 252846 Color: 453

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 492432 Color: 9905
Size: 256612 Color: 912
Size: 250957 Color: 179

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 492464 Color: 9906
Size: 255671 Color: 814
Size: 251866 Color: 319

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 492552 Color: 9907
Size: 254016 Color: 610
Size: 253433 Color: 531

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 492580 Color: 9908
Size: 255012 Color: 747
Size: 252409 Color: 401

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 492590 Color: 9909
Size: 254800 Color: 719
Size: 252611 Color: 430

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 492684 Color: 9910
Size: 254907 Color: 734
Size: 252410 Color: 402

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 492817 Color: 9911
Size: 256945 Color: 949
Size: 250239 Color: 45

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 492840 Color: 9912
Size: 253718 Color: 576
Size: 253443 Color: 534

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 492947 Color: 9913
Size: 256306 Color: 872
Size: 250748 Color: 144

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 492971 Color: 9914
Size: 255763 Color: 824
Size: 251267 Color: 238

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 493000 Color: 9915
Size: 255810 Color: 826
Size: 251191 Color: 226

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 493038 Color: 9916
Size: 256805 Color: 936
Size: 250158 Color: 28

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 493065 Color: 9917
Size: 254630 Color: 693
Size: 252306 Color: 389

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 493092 Color: 9918
Size: 255985 Color: 844
Size: 250924 Color: 176

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 493123 Color: 9919
Size: 253918 Color: 597
Size: 252960 Color: 472

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 493153 Color: 9920
Size: 255332 Color: 777
Size: 251516 Color: 268

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 493207 Color: 9921
Size: 255129 Color: 754
Size: 251665 Color: 292

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 493300 Color: 9922
Size: 254040 Color: 612
Size: 252661 Color: 436

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 493351 Color: 9923
Size: 256162 Color: 863
Size: 250488 Color: 90

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 493558 Color: 9924
Size: 254222 Color: 638
Size: 252221 Color: 368

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 493645 Color: 9925
Size: 255877 Color: 832
Size: 250479 Color: 88

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 493660 Color: 9926
Size: 255552 Color: 798
Size: 250789 Color: 153

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 493786 Color: 9927
Size: 255674 Color: 815
Size: 250541 Color: 101

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 493826 Color: 9928
Size: 255681 Color: 817
Size: 250494 Color: 92

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 493843 Color: 9929
Size: 255319 Color: 771
Size: 250839 Color: 162

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 493867 Color: 9930
Size: 256118 Color: 857
Size: 250016 Color: 6

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 493987 Color: 9931
Size: 254460 Color: 670
Size: 251554 Color: 272

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 494107 Color: 9932
Size: 255471 Color: 792
Size: 250423 Color: 73

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 494136 Color: 9933
Size: 253745 Color: 580
Size: 252120 Color: 352

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 494580 Color: 9934
Size: 254121 Color: 628
Size: 251300 Color: 243

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 494600 Color: 9935
Size: 255190 Color: 759
Size: 250211 Color: 40

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 494615 Color: 9936
Size: 253506 Color: 550
Size: 251880 Color: 321

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 494919 Color: 9937
Size: 254472 Color: 671
Size: 250610 Color: 114

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 495054 Color: 9938
Size: 253136 Color: 491
Size: 251811 Color: 307

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 495120 Color: 9939
Size: 253057 Color: 483
Size: 251824 Color: 309

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 495154 Color: 9940
Size: 253241 Color: 508
Size: 251606 Color: 282

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 495199 Color: 9941
Size: 253234 Color: 507
Size: 251568 Color: 274

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 495273 Color: 9942
Size: 253889 Color: 591
Size: 250839 Color: 163

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 495457 Color: 9943
Size: 254342 Color: 658
Size: 250202 Color: 36

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 495485 Color: 9944
Size: 253909 Color: 595
Size: 250607 Color: 113

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 495531 Color: 9945
Size: 253415 Color: 528
Size: 251055 Color: 197

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 495683 Color: 9946
Size: 254305 Color: 652
Size: 250013 Color: 5

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 495779 Color: 9947
Size: 252879 Color: 458
Size: 251343 Color: 248

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 495889 Color: 9948
Size: 252645 Color: 433
Size: 251467 Color: 262

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 495920 Color: 9949
Size: 253831 Color: 587
Size: 250250 Color: 49

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 495926 Color: 9950
Size: 253086 Color: 485
Size: 250989 Color: 184

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 495943 Color: 9951
Size: 253814 Color: 585
Size: 250244 Color: 47

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 496281 Color: 9952
Size: 252440 Color: 406
Size: 251280 Color: 239

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 496458 Color: 9953
Size: 252827 Color: 449
Size: 250716 Color: 136

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 496623 Color: 9954
Size: 251728 Color: 302
Size: 251650 Color: 289

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 496638 Color: 9955
Size: 252485 Color: 414
Size: 250878 Color: 169

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 496645 Color: 9956
Size: 252159 Color: 357
Size: 251197 Color: 227

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 496724 Color: 9957
Size: 252366 Color: 395
Size: 250911 Color: 174

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 496772 Color: 9958
Size: 253055 Color: 482
Size: 250174 Color: 32

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 496796 Color: 9959
Size: 252378 Color: 396
Size: 250827 Color: 161

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 496842 Color: 9960
Size: 252248 Color: 381
Size: 250911 Color: 173

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 497019 Color: 9961
Size: 252326 Color: 392
Size: 250656 Color: 126

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 497025 Color: 9962
Size: 252588 Color: 427
Size: 250388 Color: 71

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 497076 Color: 9963
Size: 252228 Color: 372
Size: 250697 Color: 132

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 497239 Color: 9964
Size: 252236 Color: 375
Size: 250526 Color: 97

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 497244 Color: 9965
Size: 251407 Color: 254
Size: 251350 Color: 250

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 497261 Color: 9966
Size: 251939 Color: 328
Size: 250801 Color: 158

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 497358 Color: 9967
Size: 251848 Color: 315
Size: 250795 Color: 156

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 497368 Color: 9968
Size: 251984 Color: 333
Size: 250649 Color: 124

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 497369 Color: 9969
Size: 251589 Color: 278
Size: 251043 Color: 194

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 497376 Color: 9970
Size: 251441 Color: 258
Size: 251184 Color: 222

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 497549 Color: 9971
Size: 251448 Color: 259
Size: 251004 Color: 189

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 497615 Color: 9972
Size: 252227 Color: 371
Size: 250159 Color: 29

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 497620 Color: 9973
Size: 251910 Color: 325
Size: 250471 Color: 86

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 497756 Color: 9974
Size: 251156 Color: 216
Size: 251089 Color: 206

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 497874 Color: 9975
Size: 251664 Color: 291
Size: 250463 Color: 83

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 497899 Color: 9976
Size: 251719 Color: 296
Size: 250383 Color: 70

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 498002 Color: 9977
Size: 251213 Color: 228
Size: 250786 Color: 151

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 498062 Color: 9978
Size: 251171 Color: 220
Size: 250768 Color: 149

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 498107 Color: 9979
Size: 251846 Color: 314
Size: 250048 Color: 10

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 498130 Color: 9980
Size: 251239 Color: 232
Size: 250632 Color: 120

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 498151 Color: 9981
Size: 251732 Color: 303
Size: 250118 Color: 22

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 498163 Color: 9982
Size: 251244 Color: 234
Size: 250594 Color: 111

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 498363 Color: 9983
Size: 251071 Color: 202
Size: 250567 Color: 106

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 498503 Color: 9984
Size: 251185 Color: 223
Size: 250313 Color: 58

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 498531 Color: 9985
Size: 251123 Color: 210
Size: 250347 Color: 64

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 498636 Color: 9986
Size: 250905 Color: 172
Size: 250460 Color: 81

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 498747 Color: 9987
Size: 250810 Color: 159
Size: 250444 Color: 77

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 498767 Color: 9988
Size: 250764 Color: 146
Size: 250470 Color: 84

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 498801 Color: 9989
Size: 250990 Color: 185
Size: 250210 Color: 38

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 498834 Color: 9990
Size: 251085 Color: 205
Size: 250082 Color: 15

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 498968 Color: 9991
Size: 250697 Color: 133
Size: 250336 Color: 62

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 499174 Color: 9992
Size: 250746 Color: 143
Size: 250081 Color: 14

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 499234 Color: 9993
Size: 250475 Color: 87
Size: 250292 Color: 54

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 499311 Color: 9994
Size: 250517 Color: 96
Size: 250173 Color: 31

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 499436 Color: 9995
Size: 250303 Color: 57
Size: 250262 Color: 50

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 499439 Color: 9996
Size: 250361 Color: 66
Size: 250201 Color: 35

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 499531 Color: 9997
Size: 250334 Color: 61
Size: 250136 Color: 26

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 499697 Color: 9998
Size: 250185 Color: 34
Size: 250119 Color: 23

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 499806 Color: 9999
Size: 250117 Color: 21
Size: 250078 Color: 13

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 499910 Color: 10000
Size: 250054 Color: 11
Size: 250037 Color: 7

Bin 2636: 1 of cap free
Amount of items: 3
Items: 
Size: 372896 Color: 7217
Size: 345428 Color: 6284
Size: 281676 Color: 3010

Bin 2637: 1 of cap free
Amount of items: 3
Items: 
Size: 372918 Color: 7219
Size: 345457 Color: 6287
Size: 281625 Color: 3006

Bin 2638: 1 of cap free
Amount of items: 3
Items: 
Size: 382235 Color: 7514
Size: 343618 Color: 6201
Size: 274147 Color: 2451

Bin 2639: 1 of cap free
Amount of items: 3
Items: 
Size: 349056 Color: 6416
Size: 341820 Color: 6145
Size: 309124 Color: 4633

Bin 2640: 1 of cap free
Amount of items: 3
Items: 
Size: 386480 Color: 7650
Size: 341673 Color: 6135
Size: 271847 Color: 2264

Bin 2641: 1 of cap free
Amount of items: 3
Items: 
Size: 392749 Color: 7821
Size: 339468 Color: 6040
Size: 267783 Color: 1962

Bin 2642: 1 of cap free
Amount of items: 3
Items: 
Size: 394079 Color: 7870
Size: 338892 Color: 6008
Size: 267029 Color: 1897

Bin 2643: 1 of cap free
Amount of items: 3
Items: 
Size: 395247 Color: 7903
Size: 338571 Color: 5994
Size: 266182 Color: 1831

Bin 2644: 1 of cap free
Amount of items: 3
Items: 
Size: 376989 Color: 7356
Size: 356993 Color: 6717
Size: 266018 Color: 1815

Bin 2645: 1 of cap free
Amount of items: 3
Items: 
Size: 395496 Color: 7908
Size: 337728 Color: 5968
Size: 266776 Color: 1879

Bin 2646: 1 of cap free
Amount of items: 3
Items: 
Size: 392899 Color: 7828
Size: 339345 Color: 6033
Size: 267756 Color: 1959

Bin 2647: 1 of cap free
Amount of items: 3
Items: 
Size: 377723 Color: 7385
Size: 326393 Color: 5502
Size: 295884 Color: 3910

Bin 2648: 1 of cap free
Amount of items: 3
Items: 
Size: 377065 Color: 7357
Size: 352537 Color: 6560
Size: 270398 Color: 2162

Bin 2649: 1 of cap free
Amount of items: 3
Items: 
Size: 399038 Color: 8003
Size: 332739 Color: 5769
Size: 268223 Color: 1989

Bin 2650: 1 of cap free
Amount of items: 3
Items: 
Size: 372273 Color: 7188
Size: 366063 Color: 6982
Size: 261664 Color: 1421

Bin 2651: 1 of cap free
Amount of items: 3
Items: 
Size: 390095 Color: 7748
Size: 340176 Color: 6076
Size: 269729 Color: 2117

Bin 2652: 1 of cap free
Amount of items: 3
Items: 
Size: 375517 Color: 7309
Size: 372445 Color: 7198
Size: 252038 Color: 341

Bin 2653: 1 of cap free
Amount of items: 3
Items: 
Size: 399669 Color: 8018
Size: 332822 Color: 5774
Size: 267509 Color: 1932

Bin 2654: 1 of cap free
Amount of items: 3
Items: 
Size: 352010 Color: 6538
Size: 350714 Color: 6485
Size: 297276 Color: 3991

Bin 2655: 1 of cap free
Amount of items: 3
Items: 
Size: 366491 Color: 6999
Size: 366468 Color: 6996
Size: 267041 Color: 1898

Bin 2656: 1 of cap free
Amount of items: 3
Items: 
Size: 383883 Color: 7574
Size: 342686 Color: 6180
Size: 273431 Color: 2393

Bin 2657: 1 of cap free
Amount of items: 3
Items: 
Size: 390409 Color: 7758
Size: 340224 Color: 6080
Size: 269367 Color: 2084

Bin 2658: 1 of cap free
Amount of items: 3
Items: 
Size: 386830 Color: 7661
Size: 341616 Color: 6133
Size: 271554 Color: 2239

Bin 2659: 1 of cap free
Amount of items: 3
Items: 
Size: 380785 Color: 7473
Size: 344040 Color: 6224
Size: 275175 Color: 2516

Bin 2660: 1 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 7822
Size: 329528 Color: 5646
Size: 277688 Color: 2696

Bin 2661: 1 of cap free
Amount of items: 3
Items: 
Size: 394508 Color: 7887
Size: 339282 Color: 6031
Size: 266210 Color: 1834

Bin 2662: 1 of cap free
Amount of items: 3
Items: 
Size: 405388 Color: 8172
Size: 344609 Color: 6243
Size: 250003 Color: 1

Bin 2663: 1 of cap free
Amount of items: 3
Items: 
Size: 376205 Color: 7329
Size: 348545 Color: 6405
Size: 275250 Color: 2528

Bin 2664: 1 of cap free
Amount of items: 3
Items: 
Size: 386766 Color: 7659
Size: 341529 Color: 6129
Size: 271705 Color: 2253

Bin 2665: 1 of cap free
Amount of items: 3
Items: 
Size: 370433 Color: 7137
Size: 336526 Color: 5908
Size: 293041 Color: 3755

Bin 2666: 1 of cap free
Amount of items: 3
Items: 
Size: 377516 Color: 7380
Size: 313611 Color: 4881
Size: 308873 Color: 4616

Bin 2667: 1 of cap free
Amount of items: 3
Items: 
Size: 385118 Color: 7610
Size: 342334 Color: 6166
Size: 272548 Color: 2324

Bin 2668: 1 of cap free
Amount of items: 3
Items: 
Size: 383431 Color: 7558
Size: 343028 Color: 6188
Size: 273541 Color: 2404

Bin 2669: 1 of cap free
Amount of items: 3
Items: 
Size: 387741 Color: 7681
Size: 341376 Color: 6123
Size: 270883 Color: 2204

Bin 2670: 1 of cap free
Amount of items: 3
Items: 
Size: 375795 Color: 7316
Size: 347940 Color: 6383
Size: 276265 Color: 2597

Bin 2671: 1 of cap free
Amount of items: 3
Items: 
Size: 377086 Color: 7358
Size: 332247 Color: 5746
Size: 290667 Color: 3590

Bin 2672: 1 of cap free
Amount of items: 3
Items: 
Size: 393623 Color: 7856
Size: 339015 Color: 6018
Size: 267362 Color: 1921

Bin 2673: 1 of cap free
Amount of items: 3
Items: 
Size: 385823 Color: 7630
Size: 341893 Color: 6150
Size: 272284 Color: 2300

Bin 2674: 1 of cap free
Amount of items: 3
Items: 
Size: 382102 Color: 7505
Size: 313118 Color: 4857
Size: 304780 Color: 4394

Bin 2675: 1 of cap free
Amount of items: 3
Items: 
Size: 375991 Color: 7323
Size: 369333 Color: 7093
Size: 254676 Color: 700

Bin 2676: 1 of cap free
Amount of items: 3
Items: 
Size: 386718 Color: 7657
Size: 341682 Color: 6137
Size: 271600 Color: 2243

Bin 2677: 1 of cap free
Amount of items: 3
Items: 
Size: 388304 Color: 7694
Size: 330257 Color: 5676
Size: 281439 Color: 2990

Bin 2678: 1 of cap free
Amount of items: 3
Items: 
Size: 406399 Color: 8192
Size: 333293 Color: 5796
Size: 260308 Color: 1277

Bin 2679: 1 of cap free
Amount of items: 3
Items: 
Size: 368340 Color: 7063
Size: 364509 Color: 6935
Size: 267151 Color: 1903

Bin 2680: 1 of cap free
Amount of items: 3
Items: 
Size: 410978 Color: 8308
Size: 323365 Color: 5359
Size: 265657 Color: 1783

Bin 2681: 1 of cap free
Amount of items: 3
Items: 
Size: 410780 Color: 8303
Size: 324952 Color: 5434
Size: 264268 Color: 1660

Bin 2682: 1 of cap free
Amount of items: 3
Items: 
Size: 410848 Color: 8305
Size: 323173 Color: 5350
Size: 265979 Color: 1811

Bin 2683: 1 of cap free
Amount of items: 3
Items: 
Size: 402779 Color: 8107
Size: 323304 Color: 5354
Size: 273917 Color: 2433

Bin 2684: 1 of cap free
Amount of items: 3
Items: 
Size: 408680 Color: 8246
Size: 326933 Color: 5530
Size: 264387 Color: 1671

Bin 2685: 1 of cap free
Amount of items: 3
Items: 
Size: 410293 Color: 8289
Size: 325558 Color: 5461
Size: 264149 Color: 1645

Bin 2686: 1 of cap free
Amount of items: 3
Items: 
Size: 408468 Color: 8239
Size: 324898 Color: 5431
Size: 266634 Color: 1869

Bin 2687: 1 of cap free
Amount of items: 3
Items: 
Size: 409755 Color: 8277
Size: 327776 Color: 5569
Size: 262469 Color: 1503

Bin 2688: 1 of cap free
Amount of items: 3
Items: 
Size: 409074 Color: 8257
Size: 337483 Color: 5957
Size: 253443 Color: 533

Bin 2689: 1 of cap free
Amount of items: 3
Items: 
Size: 408537 Color: 8243
Size: 334192 Color: 5831
Size: 257271 Color: 986

Bin 2690: 1 of cap free
Amount of items: 3
Items: 
Size: 404749 Color: 8153
Size: 340394 Color: 6089
Size: 254857 Color: 731

Bin 2691: 1 of cap free
Amount of items: 3
Items: 
Size: 410454 Color: 8294
Size: 331212 Color: 5702
Size: 258334 Color: 1089

Bin 2692: 1 of cap free
Amount of items: 3
Items: 
Size: 403550 Color: 8125
Size: 337286 Color: 5951
Size: 259164 Color: 1176

Bin 2693: 1 of cap free
Amount of items: 3
Items: 
Size: 402445 Color: 8096
Size: 341662 Color: 6134
Size: 255893 Color: 835

Bin 2694: 1 of cap free
Amount of items: 3
Items: 
Size: 409204 Color: 8262
Size: 334655 Color: 5851
Size: 256141 Color: 861

Bin 2695: 1 of cap free
Amount of items: 3
Items: 
Size: 406399 Color: 8191
Size: 336685 Color: 5915
Size: 256916 Color: 945

Bin 2696: 1 of cap free
Amount of items: 3
Items: 
Size: 361621 Color: 6854
Size: 334590 Color: 5846
Size: 303789 Color: 4343

Bin 2697: 1 of cap free
Amount of items: 3
Items: 
Size: 409004 Color: 8253
Size: 334382 Color: 5839
Size: 256614 Color: 913

Bin 2698: 1 of cap free
Amount of items: 3
Items: 
Size: 406442 Color: 8193
Size: 336908 Color: 5932
Size: 256650 Color: 914

Bin 2699: 1 of cap free
Amount of items: 3
Items: 
Size: 407155 Color: 8213
Size: 337525 Color: 5960
Size: 255320 Color: 772

Bin 2700: 1 of cap free
Amount of items: 3
Items: 
Size: 405811 Color: 8178
Size: 336643 Color: 5911
Size: 257546 Color: 1014

Bin 2701: 1 of cap free
Amount of items: 3
Items: 
Size: 382644 Color: 7532
Size: 357037 Color: 6720
Size: 260319 Color: 1279

Bin 2702: 1 of cap free
Amount of items: 3
Items: 
Size: 382634 Color: 7530
Size: 357700 Color: 6735
Size: 259666 Color: 1217

Bin 2703: 1 of cap free
Amount of items: 3
Items: 
Size: 400320 Color: 8036
Size: 320655 Color: 5222
Size: 279025 Color: 2796

Bin 2704: 1 of cap free
Amount of items: 3
Items: 
Size: 405024 Color: 8162
Size: 336524 Color: 5907
Size: 258452 Color: 1105

Bin 2705: 1 of cap free
Amount of items: 3
Items: 
Size: 382806 Color: 7534
Size: 357413 Color: 6726
Size: 259781 Color: 1227

Bin 2706: 1 of cap free
Amount of items: 3
Items: 
Size: 403314 Color: 8119
Size: 318202 Color: 5111
Size: 278484 Color: 2763

Bin 2707: 1 of cap free
Amount of items: 3
Items: 
Size: 400882 Color: 8054
Size: 339088 Color: 6022
Size: 260030 Color: 1250

Bin 2708: 1 of cap free
Amount of items: 3
Items: 
Size: 411851 Color: 8333
Size: 332749 Color: 5770
Size: 255400 Color: 784

Bin 2709: 1 of cap free
Amount of items: 3
Items: 
Size: 403281 Color: 8118
Size: 332442 Color: 5759
Size: 264277 Color: 1662

Bin 2710: 1 of cap free
Amount of items: 3
Items: 
Size: 403217 Color: 8116
Size: 332361 Color: 5754
Size: 264422 Color: 1676

Bin 2711: 1 of cap free
Amount of items: 3
Items: 
Size: 403004 Color: 8112
Size: 336350 Color: 5902
Size: 260646 Color: 1317

Bin 2712: 1 of cap free
Amount of items: 3
Items: 
Size: 410496 Color: 8296
Size: 334686 Color: 5852
Size: 254818 Color: 723

Bin 2713: 1 of cap free
Amount of items: 3
Items: 
Size: 402843 Color: 8110
Size: 332338 Color: 5750
Size: 264819 Color: 1709

Bin 2714: 1 of cap free
Amount of items: 3
Items: 
Size: 402186 Color: 8089
Size: 331723 Color: 5723
Size: 266091 Color: 1823

Bin 2715: 1 of cap free
Amount of items: 3
Items: 
Size: 403196 Color: 8115
Size: 336225 Color: 5895
Size: 260579 Color: 1304

Bin 2716: 1 of cap free
Amount of items: 3
Items: 
Size: 410262 Color: 8287
Size: 329797 Color: 5658
Size: 259941 Color: 1237

Bin 2717: 1 of cap free
Amount of items: 3
Items: 
Size: 371458 Color: 7164
Size: 330897 Color: 5697
Size: 297645 Color: 4010

Bin 2718: 1 of cap free
Amount of items: 3
Items: 
Size: 401259 Color: 8061
Size: 330296 Color: 5677
Size: 268445 Color: 2009

Bin 2719: 1 of cap free
Amount of items: 3
Items: 
Size: 403773 Color: 8131
Size: 330749 Color: 5693
Size: 265478 Color: 1772

Bin 2720: 1 of cap free
Amount of items: 3
Items: 
Size: 401463 Color: 8067
Size: 330930 Color: 5699
Size: 267607 Color: 1946

Bin 2721: 1 of cap free
Amount of items: 3
Items: 
Size: 400387 Color: 8037
Size: 330108 Color: 5671
Size: 269505 Color: 2098

Bin 2722: 1 of cap free
Amount of items: 3
Items: 
Size: 400262 Color: 8033
Size: 329844 Color: 5662
Size: 269894 Color: 2132

Bin 2723: 1 of cap free
Amount of items: 3
Items: 
Size: 405988 Color: 8183
Size: 329835 Color: 5660
Size: 264177 Color: 1648

Bin 2724: 1 of cap free
Amount of items: 3
Items: 
Size: 400040 Color: 8026
Size: 335598 Color: 5880
Size: 264362 Color: 1668

Bin 2725: 1 of cap free
Amount of items: 3
Items: 
Size: 409693 Color: 8275
Size: 332845 Color: 5776
Size: 257462 Color: 1005

Bin 2726: 1 of cap free
Amount of items: 3
Items: 
Size: 340390 Color: 6088
Size: 329969 Color: 5665
Size: 329641 Color: 5654

Bin 2727: 1 of cap free
Amount of items: 3
Items: 
Size: 399933 Color: 8021
Size: 335649 Color: 5881
Size: 264418 Color: 1675

Bin 2728: 1 of cap free
Amount of items: 3
Items: 
Size: 387331 Color: 7672
Size: 351346 Color: 6511
Size: 261323 Color: 1392

Bin 2729: 1 of cap free
Amount of items: 3
Items: 
Size: 391210 Color: 7781
Size: 329201 Color: 5629
Size: 279589 Color: 2842

Bin 2730: 1 of cap free
Amount of items: 3
Items: 
Size: 399130 Color: 8005
Size: 335747 Color: 5885
Size: 265123 Color: 1739

Bin 2731: 1 of cap free
Amount of items: 3
Items: 
Size: 400644 Color: 8042
Size: 328982 Color: 5623
Size: 270374 Color: 2160

Bin 2732: 1 of cap free
Amount of items: 3
Items: 
Size: 398828 Color: 7998
Size: 328955 Color: 5621
Size: 272217 Color: 2291

Bin 2733: 1 of cap free
Amount of items: 3
Items: 
Size: 398598 Color: 7991
Size: 350750 Color: 6486
Size: 250652 Color: 125

Bin 2734: 1 of cap free
Amount of items: 3
Items: 
Size: 398591 Color: 7990
Size: 335768 Color: 5887
Size: 265641 Color: 1782

Bin 2735: 1 of cap free
Amount of items: 3
Items: 
Size: 393065 Color: 7835
Size: 329019 Color: 5624
Size: 277916 Color: 2722

Bin 2736: 1 of cap free
Amount of items: 3
Items: 
Size: 376090 Color: 7325
Size: 345005 Color: 6264
Size: 278905 Color: 2786

Bin 2737: 1 of cap free
Amount of items: 3
Items: 
Size: 343980 Color: 6215
Size: 328276 Color: 5590
Size: 327744 Color: 5567

Bin 2738: 1 of cap free
Amount of items: 3
Items: 
Size: 376000 Color: 7324
Size: 345160 Color: 6271
Size: 278840 Color: 2781

Bin 2739: 1 of cap free
Amount of items: 3
Items: 
Size: 349054 Color: 6415
Size: 334131 Color: 5827
Size: 316815 Color: 5043

Bin 2740: 1 of cap free
Amount of items: 3
Items: 
Size: 373035 Color: 7222
Size: 372140 Color: 7183
Size: 254825 Color: 725

Bin 2741: 1 of cap free
Amount of items: 3
Items: 
Size: 374961 Color: 7293
Size: 355922 Color: 6675
Size: 269117 Color: 2059

Bin 2742: 1 of cap free
Amount of items: 3
Items: 
Size: 391163 Color: 7780
Size: 319330 Color: 5173
Size: 289507 Color: 3522

Bin 2743: 1 of cap free
Amount of items: 3
Items: 
Size: 374646 Color: 7279
Size: 352797 Color: 6569
Size: 272557 Color: 2325

Bin 2744: 1 of cap free
Amount of items: 3
Items: 
Size: 373936 Color: 7253
Size: 345903 Color: 6306
Size: 280161 Color: 2890

Bin 2745: 1 of cap free
Amount of items: 3
Items: 
Size: 373976 Color: 7254
Size: 343870 Color: 6212
Size: 282154 Color: 3043

Bin 2746: 1 of cap free
Amount of items: 3
Items: 
Size: 358122 Color: 6748
Size: 340718 Color: 6102
Size: 301160 Color: 4219

Bin 2747: 1 of cap free
Amount of items: 3
Items: 
Size: 401488 Color: 8068
Size: 318031 Color: 5106
Size: 280481 Color: 2910

Bin 2748: 1 of cap free
Amount of items: 3
Items: 
Size: 371178 Color: 7156
Size: 343790 Color: 6207
Size: 285032 Color: 3222

Bin 2749: 1 of cap free
Amount of items: 3
Items: 
Size: 371262 Color: 7159
Size: 343373 Color: 6197
Size: 285365 Color: 3242

Bin 2750: 1 of cap free
Amount of items: 3
Items: 
Size: 371202 Color: 7157
Size: 343416 Color: 6198
Size: 285382 Color: 3246

Bin 2751: 1 of cap free
Amount of items: 3
Items: 
Size: 389905 Color: 7745
Size: 346373 Color: 6323
Size: 263722 Color: 1614

Bin 2752: 1 of cap free
Amount of items: 3
Items: 
Size: 370935 Color: 7153
Size: 342101 Color: 6155
Size: 286964 Color: 3355

Bin 2753: 1 of cap free
Amount of items: 3
Items: 
Size: 370567 Color: 7140
Size: 370281 Color: 7130
Size: 259152 Color: 1174

Bin 2754: 1 of cap free
Amount of items: 3
Items: 
Size: 369997 Color: 7123
Size: 320207 Color: 5206
Size: 309796 Color: 4663

Bin 2755: 1 of cap free
Amount of items: 3
Items: 
Size: 369978 Color: 7120
Size: 342525 Color: 6174
Size: 287497 Color: 3399

Bin 2756: 1 of cap free
Amount of items: 3
Items: 
Size: 372344 Color: 7189
Size: 369985 Color: 7122
Size: 257671 Color: 1025

Bin 2757: 1 of cap free
Amount of items: 3
Items: 
Size: 370000 Color: 7124
Size: 319648 Color: 5187
Size: 310352 Color: 4698

Bin 2758: 1 of cap free
Amount of items: 3
Items: 
Size: 372256 Color: 7187
Size: 366578 Color: 7004
Size: 261166 Color: 1375

Bin 2759: 1 of cap free
Amount of items: 3
Items: 
Size: 369155 Color: 7089
Size: 318249 Color: 5116
Size: 312596 Color: 4823

Bin 2760: 1 of cap free
Amount of items: 3
Items: 
Size: 369037 Color: 7084
Size: 345021 Color: 6266
Size: 285942 Color: 3280

Bin 2761: 1 of cap free
Amount of items: 3
Items: 
Size: 371966 Color: 7176
Size: 336835 Color: 5924
Size: 291199 Color: 3618

Bin 2762: 1 of cap free
Amount of items: 3
Items: 
Size: 374025 Color: 7256
Size: 334738 Color: 5855
Size: 291237 Color: 3624

Bin 2763: 1 of cap free
Amount of items: 3
Items: 
Size: 374540 Color: 7275
Size: 333979 Color: 5820
Size: 291481 Color: 3642

Bin 2764: 1 of cap free
Amount of items: 3
Items: 
Size: 367091 Color: 7016
Size: 339059 Color: 6021
Size: 293850 Color: 3790

Bin 2765: 1 of cap free
Amount of items: 3
Items: 
Size: 343832 Color: 6210
Size: 339733 Color: 6048
Size: 316435 Color: 5026

Bin 2766: 1 of cap free
Amount of items: 3
Items: 
Size: 367631 Color: 7033
Size: 317276 Color: 5066
Size: 315093 Color: 4962

Bin 2767: 1 of cap free
Amount of items: 3
Items: 
Size: 366924 Color: 7008
Size: 318560 Color: 5135
Size: 314516 Color: 4932

Bin 2768: 1 of cap free
Amount of items: 3
Items: 
Size: 366327 Color: 6988
Size: 329034 Color: 5626
Size: 304639 Color: 4386

Bin 2769: 1 of cap free
Amount of items: 3
Items: 
Size: 366984 Color: 7011
Size: 319651 Color: 5189
Size: 313365 Color: 4870

Bin 2770: 1 of cap free
Amount of items: 3
Items: 
Size: 366347 Color: 6989
Size: 321031 Color: 5242
Size: 312622 Color: 4826

Bin 2771: 1 of cap free
Amount of items: 3
Items: 
Size: 365386 Color: 6965
Size: 323030 Color: 5342
Size: 311584 Color: 4763

Bin 2772: 1 of cap free
Amount of items: 3
Items: 
Size: 366470 Color: 6997
Size: 320579 Color: 5218
Size: 312951 Color: 4845

Bin 2773: 1 of cap free
Amount of items: 3
Items: 
Size: 365489 Color: 6971
Size: 322817 Color: 5334
Size: 311694 Color: 4770

Bin 2774: 1 of cap free
Amount of items: 3
Items: 
Size: 366520 Color: 7001
Size: 320543 Color: 5217
Size: 312937 Color: 4844

Bin 2775: 1 of cap free
Amount of items: 3
Items: 
Size: 365175 Color: 6955
Size: 323586 Color: 5374
Size: 311239 Color: 4742

Bin 2776: 1 of cap free
Amount of items: 3
Items: 
Size: 367687 Color: 7036
Size: 334634 Color: 5848
Size: 297679 Color: 4011

Bin 2777: 1 of cap free
Amount of items: 3
Items: 
Size: 364330 Color: 6927
Size: 335070 Color: 5867
Size: 300600 Color: 4177

Bin 2778: 1 of cap free
Amount of items: 3
Items: 
Size: 364103 Color: 6920
Size: 326975 Color: 5531
Size: 308922 Color: 4621

Bin 2779: 1 of cap free
Amount of items: 3
Items: 
Size: 363030 Color: 6892
Size: 334713 Color: 5853
Size: 302257 Color: 4269

Bin 2780: 1 of cap free
Amount of items: 3
Items: 
Size: 363619 Color: 6908
Size: 328297 Color: 5595
Size: 308084 Color: 4570

Bin 2781: 1 of cap free
Amount of items: 3
Items: 
Size: 363079 Color: 6896
Size: 335149 Color: 5870
Size: 301772 Color: 4249

Bin 2782: 1 of cap free
Amount of items: 3
Items: 
Size: 363053 Color: 6894
Size: 329316 Color: 5636
Size: 307631 Color: 4548

Bin 2783: 1 of cap free
Amount of items: 3
Items: 
Size: 353375 Color: 6592
Size: 331180 Color: 5701
Size: 315445 Color: 4979

Bin 2784: 1 of cap free
Amount of items: 3
Items: 
Size: 401036 Color: 8057
Size: 313367 Color: 4871
Size: 285597 Color: 3258

Bin 2785: 1 of cap free
Amount of items: 3
Items: 
Size: 358845 Color: 6769
Size: 335822 Color: 5889
Size: 305333 Color: 4434

Bin 2786: 1 of cap free
Amount of items: 3
Items: 
Size: 361793 Color: 6861
Size: 333247 Color: 5795
Size: 304960 Color: 4407

Bin 2787: 1 of cap free
Amount of items: 3
Items: 
Size: 361715 Color: 6859
Size: 333360 Color: 5799
Size: 304925 Color: 4404

Bin 2788: 1 of cap free
Amount of items: 3
Items: 
Size: 361712 Color: 6858
Size: 333394 Color: 5802
Size: 304894 Color: 4402

Bin 2789: 1 of cap free
Amount of items: 3
Items: 
Size: 368907 Color: 7081
Size: 362506 Color: 6876
Size: 268587 Color: 2017

Bin 2790: 1 of cap free
Amount of items: 3
Items: 
Size: 362650 Color: 6880
Size: 361079 Color: 6833
Size: 276271 Color: 2599

Bin 2791: 1 of cap free
Amount of items: 3
Items: 
Size: 360334 Color: 6816
Size: 359538 Color: 6789
Size: 280128 Color: 2888

Bin 2792: 1 of cap free
Amount of items: 3
Items: 
Size: 359344 Color: 6783
Size: 359072 Color: 6776
Size: 281584 Color: 3001

Bin 2793: 1 of cap free
Amount of items: 3
Items: 
Size: 400026 Color: 8024
Size: 318428 Color: 5129
Size: 281546 Color: 3000

Bin 2794: 1 of cap free
Amount of items: 3
Items: 
Size: 358546 Color: 6760
Size: 331309 Color: 5706
Size: 310145 Color: 4685

Bin 2795: 1 of cap free
Amount of items: 3
Items: 
Size: 364418 Color: 6931
Size: 351667 Color: 6525
Size: 283915 Color: 3155

Bin 2796: 1 of cap free
Amount of items: 3
Items: 
Size: 369295 Color: 7091
Size: 346593 Color: 6333
Size: 284112 Color: 3168

Bin 2797: 1 of cap free
Amount of items: 3
Items: 
Size: 362918 Color: 6888
Size: 351642 Color: 6524
Size: 285440 Color: 3250

Bin 2798: 1 of cap free
Amount of items: 3
Items: 
Size: 356852 Color: 6712
Size: 356810 Color: 6708
Size: 286338 Color: 3311

Bin 2799: 1 of cap free
Amount of items: 3
Items: 
Size: 356725 Color: 6704
Size: 356603 Color: 6700
Size: 286672 Color: 3335

Bin 2800: 1 of cap free
Amount of items: 3
Items: 
Size: 383984 Color: 7576
Size: 328674 Color: 5606
Size: 287342 Color: 3385

Bin 2801: 1 of cap free
Amount of items: 3
Items: 
Size: 356333 Color: 6691
Size: 356295 Color: 6688
Size: 287372 Color: 3387

Bin 2802: 1 of cap free
Amount of items: 3
Items: 
Size: 356337 Color: 6692
Size: 348481 Color: 6403
Size: 295182 Color: 3880

Bin 2803: 1 of cap free
Amount of items: 3
Items: 
Size: 355861 Color: 6671
Size: 355800 Color: 6665
Size: 288339 Color: 3445

Bin 2804: 1 of cap free
Amount of items: 3
Items: 
Size: 358607 Color: 6764
Size: 352968 Color: 6578
Size: 288425 Color: 3458

Bin 2805: 1 of cap free
Amount of items: 3
Items: 
Size: 370287 Color: 7131
Size: 340202 Color: 6078
Size: 289511 Color: 3523

Bin 2806: 1 of cap free
Amount of items: 3
Items: 
Size: 355146 Color: 6648
Size: 355137 Color: 6647
Size: 289717 Color: 3534

Bin 2807: 1 of cap free
Amount of items: 3
Items: 
Size: 354115 Color: 6618
Size: 354050 Color: 6615
Size: 291835 Color: 3661

Bin 2808: 1 of cap free
Amount of items: 3
Items: 
Size: 353673 Color: 6602
Size: 353643 Color: 6600
Size: 292684 Color: 3730

Bin 2809: 1 of cap free
Amount of items: 3
Items: 
Size: 353442 Color: 6598
Size: 331463 Color: 5713
Size: 315095 Color: 4963

Bin 2810: 1 of cap free
Amount of items: 3
Items: 
Size: 352881 Color: 6574
Size: 352813 Color: 6570
Size: 294306 Color: 3819

Bin 2811: 1 of cap free
Amount of items: 3
Items: 
Size: 353091 Color: 6584
Size: 352208 Color: 6548
Size: 294701 Color: 3846

Bin 2812: 1 of cap free
Amount of items: 3
Items: 
Size: 352555 Color: 6562
Size: 352533 Color: 6559
Size: 294912 Color: 3858

Bin 2813: 1 of cap free
Amount of items: 3
Items: 
Size: 352531 Color: 6558
Size: 352507 Color: 6556
Size: 294962 Color: 3861

Bin 2814: 1 of cap free
Amount of items: 3
Items: 
Size: 352517 Color: 6557
Size: 352369 Color: 6551
Size: 295114 Color: 3873

Bin 2815: 1 of cap free
Amount of items: 3
Items: 
Size: 359887 Color: 6804
Size: 345534 Color: 6289
Size: 294579 Color: 3841

Bin 2816: 1 of cap free
Amount of items: 3
Items: 
Size: 350761 Color: 6488
Size: 350689 Color: 6481
Size: 298550 Color: 4062

Bin 2817: 1 of cap free
Amount of items: 3
Items: 
Size: 351035 Color: 6500
Size: 350666 Color: 6480
Size: 298299 Color: 4049

Bin 2818: 1 of cap free
Amount of items: 3
Items: 
Size: 385503 Color: 7622
Size: 352187 Color: 6546
Size: 262310 Color: 1483

Bin 2819: 1 of cap free
Amount of items: 3
Items: 
Size: 351883 Color: 6535
Size: 350356 Color: 6467
Size: 297761 Color: 4020

Bin 2820: 1 of cap free
Amount of items: 3
Items: 
Size: 359984 Color: 6805
Size: 340735 Color: 6105
Size: 299281 Color: 4100

Bin 2821: 1 of cap free
Amount of items: 3
Items: 
Size: 353241 Color: 6589
Size: 349133 Color: 6419
Size: 297626 Color: 4009

Bin 2822: 1 of cap free
Amount of items: 3
Items: 
Size: 350082 Color: 6455
Size: 350048 Color: 6453
Size: 299870 Color: 4131

Bin 2823: 1 of cap free
Amount of items: 3
Items: 
Size: 350069 Color: 6454
Size: 349984 Color: 6451
Size: 299947 Color: 4135

Bin 2824: 1 of cap free
Amount of items: 3
Items: 
Size: 400065 Color: 8028
Size: 329413 Color: 5640
Size: 270522 Color: 2169

Bin 2825: 1 of cap free
Amount of items: 3
Items: 
Size: 349719 Color: 6443
Size: 349504 Color: 6436
Size: 300777 Color: 4195

Bin 2826: 1 of cap free
Amount of items: 3
Items: 
Size: 349396 Color: 6430
Size: 349340 Color: 6427
Size: 301264 Color: 4221

Bin 2827: 1 of cap free
Amount of items: 3
Items: 
Size: 353879 Color: 6612
Size: 349333 Color: 6426
Size: 296788 Color: 3962

Bin 2828: 1 of cap free
Amount of items: 3
Items: 
Size: 349087 Color: 6418
Size: 349060 Color: 6417
Size: 301853 Color: 4253

Bin 2829: 1 of cap free
Amount of items: 3
Items: 
Size: 351301 Color: 6509
Size: 346323 Color: 6318
Size: 302376 Color: 4273

Bin 2830: 1 of cap free
Amount of items: 3
Items: 
Size: 362032 Color: 6865
Size: 340431 Color: 6091
Size: 297537 Color: 4004

Bin 2831: 1 of cap free
Amount of items: 3
Items: 
Size: 351539 Color: 6522
Size: 351235 Color: 6505
Size: 297226 Color: 3985

Bin 2832: 1 of cap free
Amount of items: 3
Items: 
Size: 363610 Color: 6907
Size: 345692 Color: 6292
Size: 290698 Color: 3592

Bin 2833: 1 of cap free
Amount of items: 3
Items: 
Size: 372228 Color: 7185
Size: 357384 Color: 6724
Size: 270388 Color: 2161

Bin 2834: 1 of cap free
Amount of items: 3
Items: 
Size: 351685 Color: 6527
Size: 345715 Color: 6294
Size: 302600 Color: 4283

Bin 2835: 1 of cap free
Amount of items: 3
Items: 
Size: 361398 Color: 6846
Size: 345873 Color: 6301
Size: 292729 Color: 3736

Bin 2836: 1 of cap free
Amount of items: 3
Items: 
Size: 363984 Color: 6916
Size: 347822 Color: 6379
Size: 288194 Color: 3435

Bin 2837: 1 of cap free
Amount of items: 3
Items: 
Size: 367728 Color: 7039
Size: 346895 Color: 6344
Size: 285377 Color: 3245

Bin 2838: 1 of cap free
Amount of items: 3
Items: 
Size: 348081 Color: 6385
Size: 347761 Color: 6377
Size: 304158 Color: 4363

Bin 2839: 1 of cap free
Amount of items: 3
Items: 
Size: 348318 Color: 6395
Size: 346904 Color: 6345
Size: 304778 Color: 4392

Bin 2840: 1 of cap free
Amount of items: 3
Items: 
Size: 349608 Color: 6441
Size: 346353 Color: 6321
Size: 304039 Color: 4358

Bin 2841: 1 of cap free
Amount of items: 3
Items: 
Size: 379416 Color: 7435
Size: 334076 Color: 5824
Size: 286508 Color: 3327

Bin 2842: 1 of cap free
Amount of items: 3
Items: 
Size: 352917 Color: 6575
Size: 340407 Color: 6090
Size: 306676 Color: 4503

Bin 2843: 2 of cap free
Amount of items: 3
Items: 
Size: 376268 Color: 7333
Size: 345262 Color: 6277
Size: 278469 Color: 2762

Bin 2844: 2 of cap free
Amount of items: 3
Items: 
Size: 378135 Color: 7392
Size: 344903 Color: 6258
Size: 276961 Color: 2644

Bin 2845: 2 of cap free
Amount of items: 3
Items: 
Size: 380477 Color: 7460
Size: 344019 Color: 6220
Size: 275503 Color: 2544

Bin 2846: 2 of cap free
Amount of items: 3
Items: 
Size: 380496 Color: 7462
Size: 344023 Color: 6221
Size: 275480 Color: 2543

Bin 2847: 2 of cap free
Amount of items: 3
Items: 
Size: 381219 Color: 7482
Size: 343895 Color: 6214
Size: 274885 Color: 2500

Bin 2848: 2 of cap free
Amount of items: 3
Items: 
Size: 382887 Color: 7535
Size: 343347 Color: 6194
Size: 273765 Color: 2419

Bin 2849: 2 of cap free
Amount of items: 3
Items: 
Size: 388044 Color: 7688
Size: 341108 Color: 6114
Size: 270847 Color: 2196

Bin 2850: 2 of cap free
Amount of items: 3
Items: 
Size: 391808 Color: 7799
Size: 339755 Color: 6051
Size: 268436 Color: 2008

Bin 2851: 2 of cap free
Amount of items: 3
Items: 
Size: 394450 Color: 7883
Size: 338833 Color: 6004
Size: 266716 Color: 1876

Bin 2852: 2 of cap free
Amount of items: 3
Items: 
Size: 377284 Color: 7370
Size: 344990 Color: 6263
Size: 277725 Color: 2699

Bin 2853: 2 of cap free
Amount of items: 3
Items: 
Size: 386545 Color: 7653
Size: 345993 Color: 6308
Size: 267461 Color: 1927

Bin 2854: 2 of cap free
Amount of items: 3
Items: 
Size: 393107 Color: 7836
Size: 325288 Color: 5447
Size: 281604 Color: 3004

Bin 2855: 2 of cap free
Amount of items: 3
Items: 
Size: 395348 Color: 7904
Size: 334167 Color: 5830
Size: 270484 Color: 2167

Bin 2856: 2 of cap free
Amount of items: 3
Items: 
Size: 368384 Color: 7065
Size: 357455 Color: 6727
Size: 274160 Color: 2453

Bin 2857: 2 of cap free
Amount of items: 3
Items: 
Size: 381819 Color: 7498
Size: 334862 Color: 5858
Size: 283318 Color: 3115

Bin 2858: 2 of cap free
Amount of items: 3
Items: 
Size: 349613 Color: 6442
Size: 348562 Color: 6407
Size: 301824 Color: 4252

Bin 2859: 2 of cap free
Amount of items: 3
Items: 
Size: 371756 Color: 7171
Size: 364060 Color: 6919
Size: 264183 Color: 1651

Bin 2860: 2 of cap free
Amount of items: 3
Items: 
Size: 380746 Color: 7470
Size: 339475 Color: 6041
Size: 279778 Color: 2859

Bin 2861: 2 of cap free
Amount of items: 3
Items: 
Size: 392406 Color: 7817
Size: 339749 Color: 6050
Size: 267844 Color: 1968

Bin 2862: 2 of cap free
Amount of items: 3
Items: 
Size: 386901 Color: 7663
Size: 341588 Color: 6132
Size: 271510 Color: 2236

Bin 2863: 2 of cap free
Amount of items: 3
Items: 
Size: 384260 Color: 7585
Size: 339951 Color: 6063
Size: 275788 Color: 2561

Bin 2864: 2 of cap free
Amount of items: 3
Items: 
Size: 351514 Color: 6519
Size: 331317 Color: 5707
Size: 317168 Color: 5064

Bin 2865: 2 of cap free
Amount of items: 3
Items: 
Size: 368771 Color: 7078
Size: 335116 Color: 5869
Size: 296112 Color: 3922

Bin 2866: 2 of cap free
Amount of items: 3
Items: 
Size: 379734 Color: 7444
Size: 339008 Color: 6015
Size: 281257 Color: 2972

Bin 2867: 2 of cap free
Amount of items: 3
Items: 
Size: 389590 Color: 7741
Size: 346256 Color: 6316
Size: 264153 Color: 1646

Bin 2868: 2 of cap free
Amount of items: 3
Items: 
Size: 379965 Color: 7446
Size: 344190 Color: 6231
Size: 275844 Color: 2569

Bin 2869: 2 of cap free
Amount of items: 3
Items: 
Size: 411630 Color: 8328
Size: 337998 Color: 5979
Size: 250371 Color: 69

Bin 2870: 2 of cap free
Amount of items: 3
Items: 
Size: 393059 Color: 7834
Size: 346161 Color: 6313
Size: 260779 Color: 1333

Bin 2871: 2 of cap free
Amount of items: 3
Items: 
Size: 411383 Color: 8321
Size: 322798 Color: 5332
Size: 265818 Color: 1799

Bin 2872: 2 of cap free
Amount of items: 3
Items: 
Size: 407958 Color: 8228
Size: 336829 Color: 5923
Size: 255212 Color: 761

Bin 2873: 2 of cap free
Amount of items: 3
Items: 
Size: 408884 Color: 8252
Size: 334194 Color: 5832
Size: 256921 Color: 946

Bin 2874: 2 of cap free
Amount of items: 3
Items: 
Size: 409143 Color: 8261
Size: 334394 Color: 5840
Size: 256462 Color: 893

Bin 2875: 2 of cap free
Amount of items: 3
Items: 
Size: 406097 Color: 8186
Size: 336882 Color: 5929
Size: 257020 Color: 956

Bin 2876: 2 of cap free
Amount of items: 3
Items: 
Size: 399160 Color: 8006
Size: 340081 Color: 6068
Size: 260758 Color: 1331

Bin 2877: 2 of cap free
Amount of items: 3
Items: 
Size: 411453 Color: 8323
Size: 333986 Color: 5821
Size: 254560 Color: 684

Bin 2878: 2 of cap free
Amount of items: 3
Items: 
Size: 404405 Color: 8143
Size: 327669 Color: 5566
Size: 267925 Color: 1972

Bin 2879: 2 of cap free
Amount of items: 3
Items: 
Size: 405074 Color: 8163
Size: 336682 Color: 5914
Size: 258243 Color: 1076

Bin 2880: 2 of cap free
Amount of items: 3
Items: 
Size: 394051 Color: 7869
Size: 337199 Color: 5946
Size: 268749 Color: 2026

Bin 2881: 2 of cap free
Amount of items: 3
Items: 
Size: 405345 Color: 8171
Size: 336899 Color: 5931
Size: 257755 Color: 1035

Bin 2882: 2 of cap free
Amount of items: 3
Items: 
Size: 408279 Color: 8237
Size: 341030 Color: 6112
Size: 250690 Color: 131

Bin 2883: 2 of cap free
Amount of items: 3
Items: 
Size: 398612 Color: 7992
Size: 339881 Color: 6058
Size: 261506 Color: 1411

Bin 2884: 2 of cap free
Amount of items: 3
Items: 
Size: 386977 Color: 7664
Size: 332533 Color: 5760
Size: 280489 Color: 2912

Bin 2885: 2 of cap free
Amount of items: 3
Items: 
Size: 401719 Color: 8078
Size: 332238 Color: 5744
Size: 266042 Color: 1818

Bin 2886: 2 of cap free
Amount of items: 3
Items: 
Size: 404989 Color: 8161
Size: 312601 Color: 4824
Size: 282409 Color: 3057

Bin 2887: 2 of cap free
Amount of items: 3
Items: 
Size: 401519 Color: 8071
Size: 330918 Color: 5698
Size: 267562 Color: 1939

Bin 2888: 2 of cap free
Amount of items: 3
Items: 
Size: 400861 Color: 8051
Size: 330408 Color: 5680
Size: 268730 Color: 2025

Bin 2889: 2 of cap free
Amount of items: 3
Items: 
Size: 400534 Color: 8040
Size: 335524 Color: 5877
Size: 263941 Color: 1632

Bin 2890: 2 of cap free
Amount of items: 3
Items: 
Size: 400141 Color: 8031
Size: 335678 Color: 5884
Size: 264180 Color: 1649

Bin 2891: 2 of cap free
Amount of items: 3
Items: 
Size: 399450 Color: 8013
Size: 329702 Color: 5656
Size: 270847 Color: 2197

Bin 2892: 2 of cap free
Amount of items: 3
Items: 
Size: 398363 Color: 7983
Size: 328292 Color: 5594
Size: 273344 Color: 2384

Bin 2893: 2 of cap free
Amount of items: 3
Items: 
Size: 375787 Color: 7315
Size: 345228 Color: 6274
Size: 278984 Color: 2792

Bin 2894: 2 of cap free
Amount of items: 3
Items: 
Size: 376109 Color: 7326
Size: 327393 Color: 5550
Size: 296497 Color: 3951

Bin 2895: 2 of cap free
Amount of items: 3
Items: 
Size: 369097 Color: 7086
Size: 350431 Color: 6471
Size: 280471 Color: 2908

Bin 2896: 2 of cap free
Amount of items: 3
Items: 
Size: 374001 Color: 7255
Size: 344787 Color: 6253
Size: 281211 Color: 2966

Bin 2897: 2 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 7073
Size: 348320 Color: 6396
Size: 282990 Color: 3092

Bin 2898: 2 of cap free
Amount of items: 3
Items: 
Size: 369985 Color: 7121
Size: 353320 Color: 6590
Size: 276694 Color: 2632

Bin 2899: 2 of cap free
Amount of items: 3
Items: 
Size: 370878 Color: 7152
Size: 321843 Color: 5284
Size: 307278 Color: 4531

Bin 2900: 2 of cap free
Amount of items: 3
Items: 
Size: 367327 Color: 7025
Size: 347301 Color: 6358
Size: 285371 Color: 3244

Bin 2901: 2 of cap free
Amount of items: 3
Items: 
Size: 370062 Color: 7126
Size: 363529 Color: 6904
Size: 266408 Color: 1851

Bin 2902: 2 of cap free
Amount of items: 3
Items: 
Size: 370626 Color: 7143
Size: 319239 Color: 5166
Size: 310134 Color: 4684

Bin 2903: 2 of cap free
Amount of items: 3
Items: 
Size: 366475 Color: 6998
Size: 349569 Color: 6440
Size: 283955 Color: 3158

Bin 2904: 2 of cap free
Amount of items: 3
Items: 
Size: 368505 Color: 7069
Size: 368306 Color: 7059
Size: 263188 Color: 1563

Bin 2905: 2 of cap free
Amount of items: 3
Items: 
Size: 359164 Color: 6780
Size: 328708 Color: 5607
Size: 312127 Color: 4794

Bin 2906: 2 of cap free
Amount of items: 3
Items: 
Size: 369127 Color: 7088
Size: 318239 Color: 5114
Size: 312633 Color: 4827

Bin 2907: 2 of cap free
Amount of items: 3
Items: 
Size: 369420 Color: 7094
Size: 318226 Color: 5113
Size: 312353 Color: 4806

Bin 2908: 2 of cap free
Amount of items: 3
Items: 
Size: 369501 Color: 7100
Size: 318926 Color: 5148
Size: 311572 Color: 4762

Bin 2909: 2 of cap free
Amount of items: 3
Items: 
Size: 369608 Color: 7107
Size: 347356 Color: 6359
Size: 283035 Color: 3094

Bin 2910: 2 of cap free
Amount of items: 3
Items: 
Size: 368250 Color: 7056
Size: 316824 Color: 5044
Size: 314925 Color: 4953

Bin 2911: 2 of cap free
Amount of items: 3
Items: 
Size: 368287 Color: 7058
Size: 316741 Color: 5039
Size: 314971 Color: 4957

Bin 2912: 2 of cap free
Amount of items: 3
Items: 
Size: 364517 Color: 6936
Size: 320067 Color: 5201
Size: 315415 Color: 4978

Bin 2913: 2 of cap free
Amount of items: 3
Items: 
Size: 367671 Color: 7035
Size: 316925 Color: 5049
Size: 315403 Color: 4977

Bin 2914: 2 of cap free
Amount of items: 3
Items: 
Size: 367382 Color: 7027
Size: 339782 Color: 6052
Size: 292835 Color: 3744

Bin 2915: 2 of cap free
Amount of items: 3
Items: 
Size: 367333 Color: 7026
Size: 317961 Color: 5100
Size: 314705 Color: 4944

Bin 2916: 2 of cap free
Amount of items: 3
Items: 
Size: 369878 Color: 7114
Size: 367160 Color: 7019
Size: 262961 Color: 1544

Bin 2917: 2 of cap free
Amount of items: 3
Items: 
Size: 368462 Color: 7067
Size: 322002 Color: 5293
Size: 309535 Color: 4652

Bin 2918: 2 of cap free
Amount of items: 3
Items: 
Size: 367397 Color: 7028
Size: 344834 Color: 6255
Size: 287768 Color: 3416

Bin 2919: 2 of cap free
Amount of items: 3
Items: 
Size: 351016 Color: 6498
Size: 334646 Color: 5849
Size: 314337 Color: 4919

Bin 2920: 2 of cap free
Amount of items: 3
Items: 
Size: 366578 Color: 7003
Size: 338645 Color: 5998
Size: 294776 Color: 3851

Bin 2921: 2 of cap free
Amount of items: 3
Items: 
Size: 363665 Color: 6910
Size: 323421 Color: 5360
Size: 312913 Color: 4841

Bin 2922: 2 of cap free
Amount of items: 3
Items: 
Size: 365400 Color: 6966
Size: 337272 Color: 5950
Size: 297327 Color: 3994

Bin 2923: 2 of cap free
Amount of items: 3
Items: 
Size: 354819 Color: 6637
Size: 336861 Color: 5927
Size: 308319 Color: 4585

Bin 2924: 2 of cap free
Amount of items: 3
Items: 
Size: 371686 Color: 7169
Size: 347177 Color: 6355
Size: 281136 Color: 2961

Bin 2925: 2 of cap free
Amount of items: 3
Items: 
Size: 361994 Color: 6864
Size: 326131 Color: 5488
Size: 311874 Color: 4776

Bin 2926: 2 of cap free
Amount of items: 3
Items: 
Size: 365184 Color: 6956
Size: 322448 Color: 5313
Size: 312367 Color: 4808

Bin 2927: 2 of cap free
Amount of items: 3
Items: 
Size: 376690 Color: 7348
Size: 329819 Color: 5659
Size: 293490 Color: 3775

Bin 2928: 2 of cap free
Amount of items: 3
Items: 
Size: 363443 Color: 6902
Size: 328576 Color: 5602
Size: 307980 Color: 4564

Bin 2929: 2 of cap free
Amount of items: 3
Items: 
Size: 345877 Color: 6304
Size: 345875 Color: 6302
Size: 308247 Color: 4581

Bin 2930: 2 of cap free
Amount of items: 3
Items: 
Size: 384403 Color: 7590
Size: 334357 Color: 5838
Size: 281239 Color: 2969

Bin 2931: 2 of cap free
Amount of items: 3
Items: 
Size: 362942 Color: 6890
Size: 330021 Color: 5668
Size: 307036 Color: 4524

Bin 2932: 2 of cap free
Amount of items: 3
Items: 
Size: 368381 Color: 7064
Size: 353039 Color: 6582
Size: 278579 Color: 2769

Bin 2933: 2 of cap free
Amount of items: 3
Items: 
Size: 361483 Color: 6850
Size: 361356 Color: 6844
Size: 277160 Color: 2654

Bin 2934: 2 of cap free
Amount of items: 3
Items: 
Size: 362252 Color: 6869
Size: 361336 Color: 6842
Size: 276411 Color: 2609

Bin 2935: 2 of cap free
Amount of items: 3
Items: 
Size: 359837 Color: 6802
Size: 359782 Color: 6799
Size: 280380 Color: 2902

Bin 2936: 2 of cap free
Amount of items: 3
Items: 
Size: 359771 Color: 6798
Size: 359554 Color: 6790
Size: 280674 Color: 2931

Bin 2937: 2 of cap free
Amount of items: 3
Items: 
Size: 359004 Color: 6774
Size: 358958 Color: 6773
Size: 282037 Color: 3035

Bin 2938: 2 of cap free
Amount of items: 3
Items: 
Size: 381951 Color: 7501
Size: 332936 Color: 5780
Size: 285112 Color: 3226

Bin 2939: 2 of cap free
Amount of items: 3
Items: 
Size: 356729 Color: 6705
Size: 356099 Color: 6682
Size: 287171 Color: 3368

Bin 2940: 2 of cap free
Amount of items: 3
Items: 
Size: 356045 Color: 6680
Size: 355952 Color: 6676
Size: 288002 Color: 3424

Bin 2941: 2 of cap free
Amount of items: 3
Items: 
Size: 353858 Color: 6610
Size: 353838 Color: 6608
Size: 292303 Color: 3701

Bin 2942: 2 of cap free
Amount of items: 3
Items: 
Size: 353738 Color: 6605
Size: 353701 Color: 6603
Size: 292560 Color: 3725

Bin 2943: 2 of cap free
Amount of items: 3
Items: 
Size: 354593 Color: 6629
Size: 350503 Color: 6474
Size: 294903 Color: 3857

Bin 2944: 2 of cap free
Amount of items: 3
Items: 
Size: 350172 Color: 6458
Size: 350048 Color: 6452
Size: 299779 Color: 4125

Bin 2945: 2 of cap free
Amount of items: 3
Items: 
Size: 361470 Color: 6849
Size: 337830 Color: 5971
Size: 300699 Color: 4188

Bin 2946: 2 of cap free
Amount of items: 3
Items: 
Size: 349902 Color: 6448
Size: 349388 Color: 6429
Size: 300709 Color: 4190

Bin 2947: 2 of cap free
Amount of items: 3
Items: 
Size: 361178 Color: 6836
Size: 322294 Color: 5304
Size: 316527 Color: 5029

Bin 2948: 2 of cap free
Amount of items: 3
Items: 
Size: 352675 Color: 6565
Size: 351154 Color: 6504
Size: 296170 Color: 3926

Bin 2949: 2 of cap free
Amount of items: 3
Items: 
Size: 379741 Color: 7445
Size: 351026 Color: 6499
Size: 269232 Color: 2069

Bin 2950: 2 of cap free
Amount of items: 3
Items: 
Size: 357042 Color: 6721
Size: 351753 Color: 6532
Size: 291204 Color: 3622

Bin 2951: 2 of cap free
Amount of items: 3
Items: 
Size: 360460 Color: 6818
Size: 349474 Color: 6433
Size: 290065 Color: 3559

Bin 2952: 2 of cap free
Amount of items: 3
Items: 
Size: 354163 Color: 6619
Size: 350689 Color: 6482
Size: 295147 Color: 3877

Bin 2953: 2 of cap free
Amount of items: 3
Items: 
Size: 361405 Color: 6847
Size: 329592 Color: 5650
Size: 309002 Color: 4627

Bin 2954: 2 of cap free
Amount of items: 3
Items: 
Size: 349260 Color: 6423
Size: 347594 Color: 6368
Size: 303145 Color: 4313

Bin 2955: 2 of cap free
Amount of items: 3
Items: 
Size: 367206 Color: 7020
Size: 348052 Color: 6384
Size: 284741 Color: 3201

Bin 2956: 2 of cap free
Amount of items: 3
Items: 
Size: 347853 Color: 6380
Size: 347819 Color: 6378
Size: 304327 Color: 4371

Bin 2957: 2 of cap free
Amount of items: 3
Items: 
Size: 338069 Color: 5981
Size: 333066 Color: 5787
Size: 328864 Color: 5615

Bin 2958: 2 of cap free
Amount of items: 3
Items: 
Size: 380989 Color: 7477
Size: 334914 Color: 5862
Size: 284096 Color: 3166

Bin 2959: 2 of cap free
Amount of items: 3
Items: 
Size: 366395 Color: 6991
Size: 347114 Color: 6351
Size: 286490 Color: 3326

Bin 2960: 2 of cap free
Amount of items: 3
Items: 
Size: 346729 Color: 6338
Size: 346718 Color: 6337
Size: 306552 Color: 4495

Bin 2961: 3 of cap free
Amount of items: 3
Items: 
Size: 341248 Color: 6119
Size: 333330 Color: 5797
Size: 325420 Color: 5457

Bin 2962: 3 of cap free
Amount of items: 3
Items: 
Size: 381616 Color: 7494
Size: 343824 Color: 6209
Size: 274558 Color: 2484

Bin 2963: 3 of cap free
Amount of items: 3
Items: 
Size: 384138 Color: 7580
Size: 342593 Color: 6176
Size: 273267 Color: 2378

Bin 2964: 3 of cap free
Amount of items: 3
Items: 
Size: 390268 Color: 7754
Size: 340226 Color: 6081
Size: 269504 Color: 2096

Bin 2965: 3 of cap free
Amount of items: 3
Items: 
Size: 387111 Color: 7670
Size: 340211 Color: 6079
Size: 272676 Color: 2336

Bin 2966: 3 of cap free
Amount of items: 3
Items: 
Size: 399978 Color: 8022
Size: 326229 Color: 5493
Size: 273791 Color: 2421

Bin 2967: 3 of cap free
Amount of items: 3
Items: 
Size: 379721 Color: 7443
Size: 344354 Color: 6234
Size: 275923 Color: 2570

Bin 2968: 3 of cap free
Amount of items: 3
Items: 
Size: 391493 Color: 7790
Size: 338977 Color: 6013
Size: 269528 Color: 2102

Bin 2969: 3 of cap free
Amount of items: 3
Items: 
Size: 374624 Color: 7278
Size: 314303 Color: 4915
Size: 311071 Color: 4734

Bin 2970: 3 of cap free
Amount of items: 3
Items: 
Size: 383817 Color: 7573
Size: 342730 Color: 6182
Size: 273451 Color: 2395

Bin 2971: 3 of cap free
Amount of items: 3
Items: 
Size: 399423 Color: 8012
Size: 322397 Color: 5310
Size: 278178 Color: 2742

Bin 2972: 3 of cap free
Amount of items: 3
Items: 
Size: 391216 Color: 7782
Size: 335820 Color: 5888
Size: 272962 Color: 2351

Bin 2973: 3 of cap free
Amount of items: 3
Items: 
Size: 373855 Color: 7250
Size: 339901 Color: 6061
Size: 286242 Color: 3301

Bin 2974: 3 of cap free
Amount of items: 3
Items: 
Size: 405678 Color: 8176
Size: 325658 Color: 5469
Size: 268662 Color: 2022

Bin 2975: 3 of cap free
Amount of items: 3
Items: 
Size: 389901 Color: 7744
Size: 340175 Color: 6075
Size: 269922 Color: 2135

Bin 2976: 3 of cap free
Amount of items: 3
Items: 
Size: 404285 Color: 8139
Size: 321837 Color: 5283
Size: 273876 Color: 2429

Bin 2977: 3 of cap free
Amount of items: 3
Items: 
Size: 383043 Color: 7538
Size: 353202 Color: 6586
Size: 263753 Color: 1616

Bin 2978: 3 of cap free
Amount of items: 3
Items: 
Size: 400877 Color: 8053
Size: 327844 Color: 5572
Size: 271277 Color: 2227

Bin 2979: 3 of cap free
Amount of items: 3
Items: 
Size: 409613 Color: 8273
Size: 328111 Color: 5584
Size: 262274 Color: 1480

Bin 2980: 3 of cap free
Amount of items: 3
Items: 
Size: 409571 Color: 8272
Size: 327621 Color: 5562
Size: 262806 Color: 1536

Bin 2981: 3 of cap free
Amount of items: 3
Items: 
Size: 408746 Color: 8248
Size: 329579 Color: 5649
Size: 261673 Color: 1422

Bin 2982: 3 of cap free
Amount of items: 3
Items: 
Size: 405938 Color: 8181
Size: 336677 Color: 5913
Size: 257383 Color: 997

Bin 2983: 3 of cap free
Amount of items: 3
Items: 
Size: 407171 Color: 8214
Size: 327833 Color: 5571
Size: 264994 Color: 1725

Bin 2984: 3 of cap free
Amount of items: 3
Items: 
Size: 410382 Color: 8292
Size: 333386 Color: 5801
Size: 256230 Color: 869

Bin 2985: 3 of cap free
Amount of items: 3
Items: 
Size: 404815 Color: 8157
Size: 336353 Color: 5903
Size: 258830 Color: 1140

Bin 2986: 3 of cap free
Amount of items: 3
Items: 
Size: 403601 Color: 8126
Size: 336000 Color: 5890
Size: 260397 Color: 1284

Bin 2987: 3 of cap free
Amount of items: 3
Items: 
Size: 394232 Color: 7875
Size: 343173 Color: 6191
Size: 262593 Color: 1520

Bin 2988: 3 of cap free
Amount of items: 3
Items: 
Size: 400912 Color: 8055
Size: 330488 Color: 5681
Size: 268598 Color: 2018

Bin 2989: 3 of cap free
Amount of items: 3
Items: 
Size: 400827 Color: 8049
Size: 330316 Color: 5678
Size: 268855 Color: 2039

Bin 2990: 3 of cap free
Amount of items: 3
Items: 
Size: 399557 Color: 8016
Size: 329230 Color: 5633
Size: 271211 Color: 2223

Bin 2991: 3 of cap free
Amount of items: 3
Items: 
Size: 371625 Color: 7166
Size: 349297 Color: 6424
Size: 279076 Color: 2806

Bin 2992: 3 of cap free
Amount of items: 3
Items: 
Size: 375864 Color: 7318
Size: 327601 Color: 5560
Size: 296533 Color: 3953

Bin 2993: 3 of cap free
Amount of items: 3
Items: 
Size: 339210 Color: 6029
Size: 339187 Color: 6028
Size: 321601 Color: 5271

Bin 2994: 3 of cap free
Amount of items: 3
Items: 
Size: 368178 Color: 7051
Size: 368015 Color: 7048
Size: 263805 Color: 1620

Bin 2995: 3 of cap free
Amount of items: 3
Items: 
Size: 366313 Color: 6987
Size: 344728 Color: 6248
Size: 288957 Color: 3482

Bin 2996: 3 of cap free
Amount of items: 3
Items: 
Size: 370574 Color: 7141
Size: 321334 Color: 5256
Size: 308090 Color: 4572

Bin 2997: 3 of cap free
Amount of items: 3
Items: 
Size: 356870 Color: 6713
Size: 321956 Color: 5286
Size: 321172 Color: 5247

Bin 2998: 3 of cap free
Amount of items: 3
Items: 
Size: 369524 Color: 7103
Size: 318587 Color: 5136
Size: 311887 Color: 4777

Bin 2999: 3 of cap free
Amount of items: 3
Items: 
Size: 374217 Color: 7262
Size: 333713 Color: 5811
Size: 292068 Color: 3679

Bin 3000: 3 of cap free
Amount of items: 3
Items: 
Size: 345803 Color: 6298
Size: 341494 Color: 6126
Size: 312701 Color: 4829

Bin 3001: 3 of cap free
Amount of items: 3
Items: 
Size: 368065 Color: 7050
Size: 316270 Color: 5015
Size: 315663 Color: 4989

Bin 3002: 3 of cap free
Amount of items: 3
Items: 
Size: 366379 Color: 6990
Size: 320815 Color: 5233
Size: 312804 Color: 4839

Bin 3003: 3 of cap free
Amount of items: 3
Items: 
Size: 365965 Color: 6979
Size: 321580 Color: 5268
Size: 312453 Color: 4812

Bin 3004: 3 of cap free
Amount of items: 3
Items: 
Size: 352730 Color: 6567
Size: 347121 Color: 6352
Size: 300147 Color: 4150

Bin 3005: 3 of cap free
Amount of items: 3
Items: 
Size: 383380 Color: 7556
Size: 318452 Color: 5131
Size: 298166 Color: 4041

Bin 3006: 3 of cap free
Amount of items: 3
Items: 
Size: 365270 Color: 6962
Size: 336839 Color: 5926
Size: 297889 Color: 4028

Bin 3007: 3 of cap free
Amount of items: 3
Items: 
Size: 364974 Color: 6949
Size: 324307 Color: 5409
Size: 310717 Color: 4720

Bin 3008: 3 of cap free
Amount of items: 3
Items: 
Size: 364486 Color: 6934
Size: 325624 Color: 5465
Size: 309888 Color: 4668

Bin 3009: 3 of cap free
Amount of items: 3
Items: 
Size: 337130 Color: 5943
Size: 335533 Color: 5878
Size: 327335 Color: 5546

Bin 3010: 3 of cap free
Amount of items: 3
Items: 
Size: 364168 Color: 6922
Size: 326979 Color: 5532
Size: 308851 Color: 4615

Bin 3011: 3 of cap free
Amount of items: 3
Items: 
Size: 363442 Color: 6901
Size: 329273 Color: 5634
Size: 307283 Color: 4533

Bin 3012: 3 of cap free
Amount of items: 3
Items: 
Size: 362645 Color: 6879
Size: 334306 Color: 5837
Size: 303047 Color: 4310

Bin 3013: 3 of cap free
Amount of items: 3
Items: 
Size: 359831 Color: 6801
Size: 342260 Color: 6160
Size: 297907 Color: 4030

Bin 3014: 3 of cap free
Amount of items: 3
Items: 
Size: 361345 Color: 6843
Size: 359509 Color: 6788
Size: 279144 Color: 2809

Bin 3015: 3 of cap free
Amount of items: 3
Items: 
Size: 359708 Color: 6796
Size: 353834 Color: 6607
Size: 286456 Color: 3322

Bin 3016: 3 of cap free
Amount of items: 3
Items: 
Size: 359086 Color: 6778
Size: 342467 Color: 6172
Size: 298445 Color: 4058

Bin 3017: 3 of cap free
Amount of items: 3
Items: 
Size: 361325 Color: 6840
Size: 356981 Color: 6716
Size: 281692 Color: 3011

Bin 3018: 3 of cap free
Amount of items: 3
Items: 
Size: 358607 Color: 6765
Size: 358596 Color: 6763
Size: 282795 Color: 3081

Bin 3019: 3 of cap free
Amount of items: 3
Items: 
Size: 358078 Color: 6747
Size: 357707 Color: 6736
Size: 284213 Color: 3171

Bin 3020: 3 of cap free
Amount of items: 3
Items: 
Size: 357921 Color: 6741
Size: 357729 Color: 6737
Size: 284348 Color: 3180

Bin 3021: 3 of cap free
Amount of items: 3
Items: 
Size: 356960 Color: 6715
Size: 356752 Color: 6706
Size: 286286 Color: 3306

Bin 3022: 3 of cap free
Amount of items: 3
Items: 
Size: 358942 Color: 6771
Size: 351669 Color: 6526
Size: 289387 Color: 3510

Bin 3023: 3 of cap free
Amount of items: 3
Items: 
Size: 355288 Color: 6655
Size: 355273 Color: 6654
Size: 289437 Color: 3515

Bin 3024: 3 of cap free
Amount of items: 3
Items: 
Size: 341886 Color: 6149
Size: 332400 Color: 5757
Size: 325712 Color: 5472

Bin 3025: 3 of cap free
Amount of items: 3
Items: 
Size: 354952 Color: 6641
Size: 354947 Color: 6640
Size: 290099 Color: 3562

Bin 3026: 3 of cap free
Amount of items: 3
Items: 
Size: 359475 Color: 6786
Size: 350197 Color: 6460
Size: 290326 Color: 3571

Bin 3027: 3 of cap free
Amount of items: 3
Items: 
Size: 376959 Color: 7354
Size: 328903 Color: 5617
Size: 294136 Color: 3803

Bin 3028: 3 of cap free
Amount of items: 3
Items: 
Size: 352861 Color: 6572
Size: 326168 Color: 5490
Size: 320969 Color: 5240

Bin 3029: 3 of cap free
Amount of items: 3
Items: 
Size: 353438 Color: 6596
Size: 351944 Color: 6537
Size: 294616 Color: 3843

Bin 3030: 3 of cap free
Amount of items: 3
Items: 
Size: 350323 Color: 6465
Size: 350291 Color: 6464
Size: 299384 Color: 4107

Bin 3031: 3 of cap free
Amount of items: 3
Items: 
Size: 349551 Color: 6439
Size: 349477 Color: 6434
Size: 300970 Color: 4209

Bin 3032: 3 of cap free
Amount of items: 3
Items: 
Size: 350995 Color: 6497
Size: 346538 Color: 6329
Size: 302465 Color: 4276

Bin 3033: 3 of cap free
Amount of items: 3
Items: 
Size: 351469 Color: 6515
Size: 346818 Color: 6341
Size: 301711 Color: 4245

Bin 3034: 3 of cap free
Amount of items: 3
Items: 
Size: 351497 Color: 6517
Size: 333464 Color: 5805
Size: 315037 Color: 4959

Bin 3035: 3 of cap free
Amount of items: 3
Items: 
Size: 361217 Color: 6837
Size: 348291 Color: 6392
Size: 290490 Color: 3580

Bin 3036: 3 of cap free
Amount of items: 3
Items: 
Size: 347750 Color: 6375
Size: 347721 Color: 6373
Size: 304527 Color: 4383

Bin 3037: 3 of cap free
Amount of items: 3
Items: 
Size: 347650 Color: 6369
Size: 347407 Color: 6362
Size: 304941 Color: 4405

Bin 3038: 4 of cap free
Amount of items: 3
Items: 
Size: 394203 Color: 7873
Size: 338833 Color: 6003
Size: 266961 Color: 1892

Bin 3039: 4 of cap free
Amount of items: 3
Items: 
Size: 393449 Color: 7847
Size: 339131 Color: 6025
Size: 267417 Color: 1925

Bin 3040: 4 of cap free
Amount of items: 3
Items: 
Size: 390583 Color: 7763
Size: 340125 Color: 6073
Size: 269289 Color: 2074

Bin 3041: 4 of cap free
Amount of items: 3
Items: 
Size: 363799 Color: 6913
Size: 344453 Color: 6237
Size: 291745 Color: 3654

Bin 3042: 4 of cap free
Amount of items: 3
Items: 
Size: 379095 Color: 7428
Size: 353373 Color: 6591
Size: 267529 Color: 1935

Bin 3043: 4 of cap free
Amount of items: 3
Items: 
Size: 382641 Color: 7531
Size: 341726 Color: 6139
Size: 275630 Color: 2551

Bin 3044: 4 of cap free
Amount of items: 3
Items: 
Size: 394882 Color: 7897
Size: 339919 Color: 6062
Size: 265196 Color: 1745

Bin 3045: 4 of cap free
Amount of items: 3
Items: 
Size: 376486 Color: 7342
Size: 341907 Color: 6152
Size: 281604 Color: 3003

Bin 3046: 4 of cap free
Amount of items: 3
Items: 
Size: 388956 Color: 7719
Size: 337843 Color: 5973
Size: 273198 Color: 2370

Bin 3047: 4 of cap free
Amount of items: 3
Items: 
Size: 398413 Color: 7984
Size: 323135 Color: 5346
Size: 278449 Color: 2761

Bin 3048: 4 of cap free
Amount of items: 3
Items: 
Size: 383428 Color: 7557
Size: 344106 Color: 6229
Size: 272463 Color: 2315

Bin 3049: 4 of cap free
Amount of items: 3
Items: 
Size: 400056 Color: 8027
Size: 332345 Color: 5751
Size: 267596 Color: 1944

Bin 3050: 4 of cap free
Amount of items: 3
Items: 
Size: 391521 Color: 7791
Size: 322967 Color: 5340
Size: 285509 Color: 3253

Bin 3051: 4 of cap free
Amount of items: 3
Items: 
Size: 394085 Color: 7871
Size: 345090 Color: 6267
Size: 260822 Color: 1341

Bin 3052: 4 of cap free
Amount of items: 3
Items: 
Size: 410730 Color: 8301
Size: 325690 Color: 5471
Size: 263577 Color: 1600

Bin 3053: 4 of cap free
Amount of items: 3
Items: 
Size: 407939 Color: 8226
Size: 341436 Color: 6125
Size: 250622 Color: 116

Bin 3054: 4 of cap free
Amount of items: 3
Items: 
Size: 403718 Color: 8129
Size: 327518 Color: 5557
Size: 268761 Color: 2029

Bin 3055: 4 of cap free
Amount of items: 3
Items: 
Size: 404355 Color: 8140
Size: 332720 Color: 5767
Size: 262922 Color: 1541

Bin 3056: 4 of cap free
Amount of items: 3
Items: 
Size: 387677 Color: 7679
Size: 357654 Color: 6731
Size: 254666 Color: 698

Bin 3057: 4 of cap free
Amount of items: 3
Items: 
Size: 406588 Color: 8197
Size: 331768 Color: 5725
Size: 261641 Color: 1418

Bin 3058: 4 of cap free
Amount of items: 3
Items: 
Size: 401799 Color: 8081
Size: 332079 Color: 5737
Size: 266119 Color: 1824

Bin 3059: 4 of cap free
Amount of items: 3
Items: 
Size: 401718 Color: 8077
Size: 331757 Color: 5724
Size: 266522 Color: 1859

Bin 3060: 4 of cap free
Amount of items: 3
Items: 
Size: 410434 Color: 8293
Size: 305604 Color: 4445
Size: 283959 Color: 3159

Bin 3061: 4 of cap free
Amount of items: 3
Items: 
Size: 404757 Color: 8154
Size: 299032 Color: 4087
Size: 296208 Color: 3928

Bin 3062: 4 of cap free
Amount of items: 3
Items: 
Size: 374185 Color: 7260
Size: 344776 Color: 6252
Size: 281036 Color: 2953

Bin 3063: 4 of cap free
Amount of items: 3
Items: 
Size: 385272 Color: 7615
Size: 335760 Color: 5886
Size: 278965 Color: 2790

Bin 3064: 4 of cap free
Amount of items: 3
Items: 
Size: 341320 Color: 6121
Size: 331585 Color: 5715
Size: 327092 Color: 5537

Bin 3065: 4 of cap free
Amount of items: 3
Items: 
Size: 369805 Color: 7113
Size: 319214 Color: 5162
Size: 310978 Color: 4728

Bin 3066: 4 of cap free
Amount of items: 3
Items: 
Size: 369451 Color: 7097
Size: 318614 Color: 5137
Size: 311932 Color: 4783

Bin 3067: 4 of cap free
Amount of items: 3
Items: 
Size: 368727 Color: 7076
Size: 317628 Color: 5085
Size: 313642 Color: 4883

Bin 3068: 4 of cap free
Amount of items: 3
Items: 
Size: 373455 Color: 7237
Size: 336893 Color: 5930
Size: 289649 Color: 3531

Bin 3069: 4 of cap free
Amount of items: 3
Items: 
Size: 368254 Color: 7057
Size: 315991 Color: 5006
Size: 315752 Color: 4994

Bin 3070: 4 of cap free
Amount of items: 3
Items: 
Size: 349503 Color: 6435
Size: 331340 Color: 5709
Size: 319154 Color: 5159

Bin 3071: 4 of cap free
Amount of items: 3
Items: 
Size: 394978 Color: 7900
Size: 310302 Color: 4695
Size: 294717 Color: 3847

Bin 3072: 4 of cap free
Amount of items: 3
Items: 
Size: 364839 Color: 6943
Size: 324548 Color: 5415
Size: 310610 Color: 4715

Bin 3073: 4 of cap free
Amount of items: 3
Items: 
Size: 349518 Color: 6437
Size: 347563 Color: 6365
Size: 302916 Color: 4300

Bin 3074: 4 of cap free
Amount of items: 3
Items: 
Size: 363757 Color: 6912
Size: 327787 Color: 5570
Size: 308453 Color: 4591

Bin 3075: 4 of cap free
Amount of items: 3
Items: 
Size: 354871 Color: 6639
Size: 332641 Color: 5764
Size: 312485 Color: 4815

Bin 3076: 4 of cap free
Amount of items: 3
Items: 
Size: 365742 Color: 6977
Size: 360750 Color: 6827
Size: 273505 Color: 2401

Bin 3077: 4 of cap free
Amount of items: 3
Items: 
Size: 360140 Color: 6808
Size: 360069 Color: 6807
Size: 279788 Color: 2863

Bin 3078: 4 of cap free
Amount of items: 3
Items: 
Size: 351134 Color: 6502
Size: 327668 Color: 5565
Size: 321195 Color: 5249

Bin 3079: 4 of cap free
Amount of items: 3
Items: 
Size: 370630 Color: 7144
Size: 355484 Color: 6660
Size: 273883 Color: 2430

Bin 3080: 4 of cap free
Amount of items: 3
Items: 
Size: 354442 Color: 6623
Size: 353398 Color: 6593
Size: 292157 Color: 3690

Bin 3081: 4 of cap free
Amount of items: 3
Items: 
Size: 354447 Color: 6624
Size: 345445 Color: 6286
Size: 300105 Color: 4146

Bin 3082: 4 of cap free
Amount of items: 3
Items: 
Size: 355126 Color: 6646
Size: 351258 Color: 6508
Size: 293613 Color: 3781

Bin 3083: 4 of cap free
Amount of items: 3
Items: 
Size: 352330 Color: 6550
Size: 352279 Color: 6549
Size: 295388 Color: 3884

Bin 3084: 4 of cap free
Amount of items: 3
Items: 
Size: 350954 Color: 6496
Size: 350816 Color: 6491
Size: 298227 Color: 4045

Bin 3085: 4 of cap free
Amount of items: 3
Items: 
Size: 362137 Color: 6867
Size: 348668 Color: 6409
Size: 289192 Color: 3499

Bin 3086: 4 of cap free
Amount of items: 3
Items: 
Size: 375342 Color: 7305
Size: 321133 Color: 5245
Size: 303522 Color: 4330

Bin 3087: 4 of cap free
Amount of items: 3
Items: 
Size: 368224 Color: 7054
Size: 346583 Color: 6332
Size: 285190 Color: 3232

Bin 3088: 5 of cap free
Amount of items: 3
Items: 
Size: 384751 Color: 7601
Size: 342457 Color: 6171
Size: 272788 Color: 2345

Bin 3089: 5 of cap free
Amount of items: 3
Items: 
Size: 371378 Color: 7162
Size: 361488 Color: 6851
Size: 267130 Color: 1901

Bin 3090: 5 of cap free
Amount of items: 3
Items: 
Size: 386221 Color: 7643
Size: 336513 Color: 5906
Size: 277262 Color: 2661

Bin 3091: 5 of cap free
Amount of items: 3
Items: 
Size: 383932 Color: 7575
Size: 342707 Color: 6181
Size: 273357 Color: 2386

Bin 3092: 5 of cap free
Amount of items: 3
Items: 
Size: 372384 Color: 7193
Size: 350695 Color: 6483
Size: 276917 Color: 2641

Bin 3093: 5 of cap free
Amount of items: 3
Items: 
Size: 390195 Color: 7753
Size: 339833 Color: 6053
Size: 269968 Color: 2139

Bin 3094: 5 of cap free
Amount of items: 3
Items: 
Size: 371439 Color: 7163
Size: 358768 Color: 6767
Size: 269789 Color: 2122

Bin 3095: 5 of cap free
Amount of items: 3
Items: 
Size: 393695 Color: 7857
Size: 338983 Color: 6014
Size: 267318 Color: 1917

Bin 3096: 5 of cap free
Amount of items: 3
Items: 
Size: 399711 Color: 8019
Size: 300579 Color: 4175
Size: 299706 Color: 4117

Bin 3097: 5 of cap free
Amount of items: 3
Items: 
Size: 380849 Color: 7475
Size: 345731 Color: 6295
Size: 273416 Color: 2390

Bin 3098: 5 of cap free
Amount of items: 3
Items: 
Size: 408183 Color: 8233
Size: 328570 Color: 5601
Size: 263243 Color: 1568

Bin 3099: 5 of cap free
Amount of items: 3
Items: 
Size: 404784 Color: 8155
Size: 333028 Color: 5785
Size: 262184 Color: 1468

Bin 3100: 5 of cap free
Amount of items: 3
Items: 
Size: 368716 Color: 7075
Size: 319650 Color: 5188
Size: 311630 Color: 4765

Bin 3101: 5 of cap free
Amount of items: 3
Items: 
Size: 398282 Color: 7982
Size: 340607 Color: 6096
Size: 261107 Color: 1367

Bin 3102: 5 of cap free
Amount of items: 3
Items: 
Size: 402291 Color: 8092
Size: 331786 Color: 5726
Size: 265919 Color: 1806

Bin 3103: 5 of cap free
Amount of items: 3
Items: 
Size: 385247 Color: 7614
Size: 329620 Color: 5653
Size: 285129 Color: 3228

Bin 3104: 5 of cap free
Amount of items: 3
Items: 
Size: 389766 Color: 7743
Size: 323484 Color: 5367
Size: 286746 Color: 3342

Bin 3105: 5 of cap free
Amount of items: 3
Items: 
Size: 376225 Color: 7331
Size: 316210 Color: 5012
Size: 307561 Color: 4543

Bin 3106: 5 of cap free
Amount of items: 3
Items: 
Size: 367785 Color: 7042
Size: 340461 Color: 6092
Size: 291750 Color: 3655

Bin 3107: 5 of cap free
Amount of items: 3
Items: 
Size: 351859 Color: 6534
Size: 331445 Color: 5712
Size: 316692 Color: 5037

Bin 3108: 5 of cap free
Amount of items: 3
Items: 
Size: 357986 Color: 6743
Size: 321417 Color: 5259
Size: 320593 Color: 5220

Bin 3109: 5 of cap free
Amount of items: 3
Items: 
Size: 365188 Color: 6957
Size: 323876 Color: 5392
Size: 310932 Color: 4726

Bin 3110: 5 of cap free
Amount of items: 3
Items: 
Size: 365501 Color: 6972
Size: 322583 Color: 5322
Size: 311912 Color: 4780

Bin 3111: 5 of cap free
Amount of items: 3
Items: 
Size: 368663 Color: 7072
Size: 335171 Color: 5871
Size: 296162 Color: 3924

Bin 3112: 5 of cap free
Amount of items: 3
Items: 
Size: 362387 Color: 6873
Size: 331913 Color: 5731
Size: 305696 Color: 4449

Bin 3113: 5 of cap free
Amount of items: 3
Items: 
Size: 361410 Color: 6848
Size: 354569 Color: 6626
Size: 284017 Color: 3163

Bin 3114: 5 of cap free
Amount of items: 3
Items: 
Size: 358437 Color: 6757
Size: 357699 Color: 6734
Size: 283860 Color: 3153

Bin 3115: 5 of cap free
Amount of items: 3
Items: 
Size: 370694 Color: 7146
Size: 345785 Color: 6297
Size: 283517 Color: 3129

Bin 3116: 5 of cap free
Amount of items: 3
Items: 
Size: 358942 Color: 6772
Size: 355079 Color: 6644
Size: 285975 Color: 3282

Bin 3117: 5 of cap free
Amount of items: 3
Items: 
Size: 385067 Color: 7608
Size: 328880 Color: 5616
Size: 286049 Color: 3292

Bin 3118: 5 of cap free
Amount of items: 3
Items: 
Size: 374534 Color: 7274
Size: 333212 Color: 5792
Size: 292250 Color: 3696

Bin 3119: 5 of cap free
Amount of items: 3
Items: 
Size: 347584 Color: 6367
Size: 344745 Color: 6249
Size: 307667 Color: 4549

Bin 3120: 5 of cap free
Amount of items: 3
Items: 
Size: 352544 Color: 6561
Size: 348544 Color: 6404
Size: 298908 Color: 4078

Bin 3121: 5 of cap free
Amount of items: 3
Items: 
Size: 359092 Color: 6779
Size: 351508 Color: 6518
Size: 289396 Color: 3512

Bin 3122: 5 of cap free
Amount of items: 3
Items: 
Size: 365256 Color: 6961
Size: 345875 Color: 6303
Size: 288865 Color: 3478

Bin 3123: 6 of cap free
Amount of items: 3
Items: 
Size: 388437 Color: 7699
Size: 340845 Color: 6107
Size: 270713 Color: 2185

Bin 3124: 6 of cap free
Amount of items: 3
Items: 
Size: 378797 Color: 7414
Size: 344990 Color: 6262
Size: 276208 Color: 2593

Bin 3125: 6 of cap free
Amount of items: 3
Items: 
Size: 411865 Color: 8334
Size: 322439 Color: 5312
Size: 265691 Color: 1786

Bin 3126: 6 of cap free
Amount of items: 3
Items: 
Size: 404886 Color: 8159
Size: 333055 Color: 5786
Size: 262054 Color: 1452

Bin 3127: 6 of cap free
Amount of items: 3
Items: 
Size: 402419 Color: 8094
Size: 331805 Color: 5728
Size: 265771 Color: 1794

Bin 3128: 6 of cap free
Amount of items: 3
Items: 
Size: 401546 Color: 8073
Size: 331266 Color: 5705
Size: 267183 Color: 1908

Bin 3129: 6 of cap free
Amount of items: 3
Items: 
Size: 372638 Color: 7205
Size: 325291 Color: 5448
Size: 302066 Color: 4260

Bin 3130: 6 of cap free
Amount of items: 3
Items: 
Size: 372648 Color: 7206
Size: 324846 Color: 5428
Size: 302501 Color: 4279

Bin 3131: 6 of cap free
Amount of items: 3
Items: 
Size: 372007 Color: 7177
Size: 343993 Color: 6217
Size: 283995 Color: 3162

Bin 3132: 6 of cap free
Amount of items: 3
Items: 
Size: 395535 Color: 7910
Size: 321492 Color: 5263
Size: 282968 Color: 3089

Bin 3133: 6 of cap free
Amount of items: 3
Items: 
Size: 371633 Color: 7167
Size: 366063 Color: 6983
Size: 262299 Color: 1481

Bin 3134: 6 of cap free
Amount of items: 3
Items: 
Size: 368208 Color: 7053
Size: 320760 Color: 5229
Size: 311027 Color: 4730

Bin 3135: 6 of cap free
Amount of items: 3
Items: 
Size: 369462 Color: 7098
Size: 342120 Color: 6156
Size: 288413 Color: 3457

Bin 3136: 6 of cap free
Amount of items: 3
Items: 
Size: 366989 Color: 7012
Size: 319116 Color: 5158
Size: 313890 Color: 4893

Bin 3137: 6 of cap free
Amount of items: 3
Items: 
Size: 348219 Color: 6389
Size: 333528 Color: 5807
Size: 318248 Color: 5115

Bin 3138: 6 of cap free
Amount of items: 3
Items: 
Size: 351353 Color: 6512
Size: 331259 Color: 5704
Size: 317383 Color: 5075

Bin 3139: 6 of cap free
Amount of items: 3
Items: 
Size: 365233 Color: 6959
Size: 324939 Color: 5432
Size: 309823 Color: 4665

Bin 3140: 6 of cap free
Amount of items: 3
Items: 
Size: 364958 Color: 6947
Size: 323508 Color: 5370
Size: 311529 Color: 4758

Bin 3141: 6 of cap free
Amount of items: 3
Items: 
Size: 376609 Color: 7346
Size: 326990 Color: 5533
Size: 296396 Color: 3942

Bin 3142: 6 of cap free
Amount of items: 3
Items: 
Size: 363440 Color: 6900
Size: 328591 Color: 5603
Size: 307964 Color: 4562

Bin 3143: 6 of cap free
Amount of items: 3
Items: 
Size: 366437 Color: 6993
Size: 334489 Color: 5842
Size: 299069 Color: 4090

Bin 3144: 6 of cap free
Amount of items: 3
Items: 
Size: 361293 Color: 6839
Size: 333154 Color: 5790
Size: 305548 Color: 4444

Bin 3145: 6 of cap free
Amount of items: 3
Items: 
Size: 386777 Color: 7660
Size: 332649 Color: 5765
Size: 280569 Color: 2918

Bin 3146: 6 of cap free
Amount of items: 3
Items: 
Size: 356140 Color: 6683
Size: 345444 Color: 6285
Size: 298411 Color: 4057

Bin 3147: 6 of cap free
Amount of items: 3
Items: 
Size: 355188 Color: 6650
Size: 352967 Color: 6577
Size: 291840 Color: 3662

Bin 3148: 6 of cap free
Amount of items: 3
Items: 
Size: 360878 Color: 6831
Size: 326080 Color: 5483
Size: 313037 Color: 4852

Bin 3149: 6 of cap free
Amount of items: 3
Items: 
Size: 350138 Color: 6457
Size: 349937 Color: 6449
Size: 299920 Color: 4134

Bin 3150: 6 of cap free
Amount of items: 3
Items: 
Size: 357314 Color: 6723
Size: 345807 Color: 6299
Size: 296874 Color: 3966

Bin 3151: 6 of cap free
Amount of items: 3
Items: 
Size: 348351 Color: 6397
Size: 348263 Color: 6391
Size: 303381 Color: 4322

Bin 3152: 7 of cap free
Amount of items: 3
Items: 
Size: 388463 Color: 7700
Size: 340829 Color: 6106
Size: 270702 Color: 2184

Bin 3153: 7 of cap free
Amount of items: 3
Items: 
Size: 390580 Color: 7762
Size: 340093 Color: 6069
Size: 269321 Color: 2077

Bin 3154: 7 of cap free
Amount of items: 3
Items: 
Size: 394385 Color: 7878
Size: 338844 Color: 6005
Size: 266765 Color: 1877

Bin 3155: 7 of cap free
Amount of items: 3
Items: 
Size: 385843 Color: 7631
Size: 329413 Color: 5639
Size: 284738 Color: 3200

Bin 3156: 7 of cap free
Amount of items: 3
Items: 
Size: 384671 Color: 7599
Size: 337657 Color: 5966
Size: 277666 Color: 2693

Bin 3157: 7 of cap free
Amount of items: 3
Items: 
Size: 410264 Color: 8288
Size: 325749 Color: 5474
Size: 263981 Color: 1635

Bin 3158: 7 of cap free
Amount of items: 3
Items: 
Size: 377435 Color: 7377
Size: 358942 Color: 6770
Size: 263617 Color: 1606

Bin 3159: 7 of cap free
Amount of items: 3
Items: 
Size: 408514 Color: 8241
Size: 336749 Color: 5918
Size: 254731 Color: 716

Bin 3160: 7 of cap free
Amount of items: 3
Items: 
Size: 369540 Color: 7104
Size: 318961 Color: 5151
Size: 311493 Color: 4756

Bin 3161: 7 of cap free
Amount of items: 3
Items: 
Size: 365254 Color: 6960
Size: 323561 Color: 5372
Size: 311179 Color: 4739

Bin 3162: 7 of cap free
Amount of items: 3
Items: 
Size: 364051 Color: 6918
Size: 327154 Color: 5540
Size: 308789 Color: 4612

Bin 3163: 7 of cap free
Amount of items: 3
Items: 
Size: 357601 Color: 6729
Size: 330054 Color: 5670
Size: 312339 Color: 4805

Bin 3164: 7 of cap free
Amount of items: 3
Items: 
Size: 363579 Color: 6905
Size: 328208 Color: 5589
Size: 308207 Color: 4579

Bin 3165: 7 of cap free
Amount of items: 3
Items: 
Size: 354035 Color: 6614
Size: 353861 Color: 6611
Size: 292098 Color: 3685

Bin 3166: 7 of cap free
Amount of items: 3
Items: 
Size: 351710 Color: 6530
Size: 336251 Color: 5900
Size: 312033 Color: 4787

Bin 3167: 7 of cap free
Amount of items: 3
Items: 
Size: 364361 Color: 6929
Size: 345731 Color: 6296
Size: 289902 Color: 3545

Bin 3168: 7 of cap free
Amount of items: 3
Items: 
Size: 347027 Color: 6348
Size: 347003 Color: 6347
Size: 305964 Color: 4463

Bin 3169: 8 of cap free
Amount of items: 3
Items: 
Size: 373117 Color: 7224
Size: 345235 Color: 6275
Size: 281641 Color: 3007

Bin 3170: 8 of cap free
Amount of items: 3
Items: 
Size: 385777 Color: 7628
Size: 336757 Color: 5920
Size: 277459 Color: 2676

Bin 3171: 8 of cap free
Amount of items: 3
Items: 
Size: 381530 Color: 7491
Size: 339501 Color: 6042
Size: 278962 Color: 2788

Bin 3172: 8 of cap free
Amount of items: 3
Items: 
Size: 350240 Color: 6461
Size: 336787 Color: 5921
Size: 312966 Color: 4847

Bin 3173: 8 of cap free
Amount of items: 3
Items: 
Size: 377108 Color: 7360
Size: 348552 Color: 6406
Size: 274333 Color: 2466

Bin 3174: 8 of cap free
Amount of items: 3
Items: 
Size: 393611 Color: 7855
Size: 331608 Color: 5717
Size: 274774 Color: 2494

Bin 3175: 8 of cap free
Amount of items: 3
Items: 
Size: 410680 Color: 8299
Size: 326194 Color: 5491
Size: 263119 Color: 1554

Bin 3176: 8 of cap free
Amount of items: 3
Items: 
Size: 392376 Color: 7814
Size: 323823 Color: 5386
Size: 283794 Color: 3146

Bin 3177: 8 of cap free
Amount of items: 3
Items: 
Size: 401623 Color: 8076
Size: 333977 Color: 5819
Size: 264393 Color: 1673

Bin 3178: 8 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 8145
Size: 333074 Color: 5789
Size: 262406 Color: 1492

Bin 3179: 8 of cap free
Amount of items: 3
Items: 
Size: 398652 Color: 7993
Size: 305096 Color: 4414
Size: 296245 Color: 3931

Bin 3180: 8 of cap free
Amount of items: 3
Items: 
Size: 349540 Color: 6438
Size: 326800 Color: 5523
Size: 323653 Color: 5376

Bin 3181: 8 of cap free
Amount of items: 3
Items: 
Size: 393160 Color: 7838
Size: 321741 Color: 5275
Size: 285092 Color: 3225

Bin 3182: 8 of cap free
Amount of items: 3
Items: 
Size: 370585 Color: 7142
Size: 321350 Color: 5257
Size: 308058 Color: 4569

Bin 3183: 8 of cap free
Amount of items: 3
Items: 
Size: 372475 Color: 7200
Size: 367537 Color: 7030
Size: 259981 Color: 1242

Bin 3184: 8 of cap free
Amount of items: 3
Items: 
Size: 363607 Color: 6906
Size: 327110 Color: 5538
Size: 309276 Color: 4638

Bin 3185: 8 of cap free
Amount of items: 3
Items: 
Size: 361291 Color: 6838
Size: 333719 Color: 5813
Size: 304983 Color: 4409

Bin 3186: 8 of cap free
Amount of items: 3
Items: 
Size: 362898 Color: 6886
Size: 354395 Color: 6621
Size: 282700 Color: 3078

Bin 3187: 8 of cap free
Amount of items: 3
Items: 
Size: 358415 Color: 6756
Size: 358223 Color: 6752
Size: 283355 Color: 3120

Bin 3188: 8 of cap free
Amount of items: 3
Items: 
Size: 356182 Color: 6685
Size: 345843 Color: 6300
Size: 297968 Color: 4034

Bin 3189: 8 of cap free
Amount of items: 3
Items: 
Size: 387417 Color: 7673
Size: 329100 Color: 5627
Size: 283476 Color: 3125

Bin 3190: 8 of cap free
Amount of items: 3
Items: 
Size: 346421 Color: 6326
Size: 330542 Color: 5683
Size: 323030 Color: 5343

Bin 3191: 8 of cap free
Amount of items: 3
Items: 
Size: 380802 Color: 7474
Size: 328716 Color: 5608
Size: 290475 Color: 3579

Bin 3192: 9 of cap free
Amount of items: 3
Items: 
Size: 357489 Color: 6728
Size: 325600 Color: 5463
Size: 316903 Color: 5047

Bin 3193: 9 of cap free
Amount of items: 3
Items: 
Size: 385713 Color: 7627
Size: 332850 Color: 5777
Size: 281429 Color: 2989

Bin 3194: 9 of cap free
Amount of items: 3
Items: 
Size: 401270 Color: 8062
Size: 319402 Color: 5175
Size: 279320 Color: 2821

Bin 3195: 9 of cap free
Amount of items: 3
Items: 
Size: 399409 Color: 8010
Size: 329385 Color: 5637
Size: 271198 Color: 2222

Bin 3196: 9 of cap free
Amount of items: 3
Items: 
Size: 374305 Color: 7265
Size: 324257 Color: 5407
Size: 301430 Color: 4232

Bin 3197: 9 of cap free
Amount of items: 3
Items: 
Size: 372601 Color: 7204
Size: 318541 Color: 5134
Size: 308850 Color: 4614

Bin 3198: 9 of cap free
Amount of items: 3
Items: 
Size: 365001 Color: 6950
Size: 324571 Color: 5416
Size: 310420 Color: 4701

Bin 3199: 9 of cap free
Amount of items: 3
Items: 
Size: 359886 Color: 6803
Size: 358578 Color: 6761
Size: 281528 Color: 2996

Bin 3200: 10 of cap free
Amount of items: 3
Items: 
Size: 386748 Color: 7658
Size: 341563 Color: 6130
Size: 271680 Color: 2251

Bin 3201: 10 of cap free
Amount of items: 3
Items: 
Size: 399854 Color: 8020
Size: 322230 Color: 5302
Size: 277907 Color: 2720

Bin 3202: 10 of cap free
Amount of items: 3
Items: 
Size: 394934 Color: 7899
Size: 332902 Color: 5778
Size: 272155 Color: 2286

Bin 3203: 10 of cap free
Amount of items: 3
Items: 
Size: 374552 Color: 7276
Size: 326626 Color: 5514
Size: 298813 Color: 4076

Bin 3204: 10 of cap free
Amount of items: 3
Items: 
Size: 368919 Color: 7082
Size: 367654 Color: 7034
Size: 263418 Color: 1587

Bin 3205: 10 of cap free
Amount of items: 3
Items: 
Size: 360656 Color: 6825
Size: 360574 Color: 6821
Size: 278761 Color: 2778

Bin 3206: 10 of cap free
Amount of items: 3
Items: 
Size: 354411 Color: 6622
Size: 325854 Color: 5478
Size: 319726 Color: 5191

Bin 3207: 10 of cap free
Amount of items: 3
Items: 
Size: 352998 Color: 6579
Size: 352878 Color: 6573
Size: 294115 Color: 3800

Bin 3208: 11 of cap free
Amount of items: 3
Items: 
Size: 379465 Color: 7437
Size: 344530 Color: 6240
Size: 275995 Color: 2573

Bin 3209: 11 of cap free
Amount of items: 3
Items: 
Size: 392895 Color: 7827
Size: 339385 Color: 6035
Size: 267710 Color: 1955

Bin 3210: 11 of cap free
Amount of items: 3
Items: 
Size: 383815 Color: 7572
Size: 342452 Color: 6170
Size: 273723 Color: 2417

Bin 3211: 11 of cap free
Amount of items: 3
Items: 
Size: 355120 Color: 6645
Size: 346301 Color: 6317
Size: 298569 Color: 4065

Bin 3212: 11 of cap free
Amount of items: 3
Items: 
Size: 384697 Color: 7600
Size: 330718 Color: 5690
Size: 284575 Color: 3191

Bin 3213: 11 of cap free
Amount of items: 3
Items: 
Size: 360590 Color: 6822
Size: 341818 Color: 6144
Size: 297582 Color: 4006

Bin 3214: 11 of cap free
Amount of items: 3
Items: 
Size: 372383 Color: 7192
Size: 329291 Color: 5635
Size: 298316 Color: 4050

Bin 3215: 11 of cap free
Amount of items: 3
Items: 
Size: 409076 Color: 8258
Size: 299729 Color: 4121
Size: 291185 Color: 3617

Bin 3216: 11 of cap free
Amount of items: 3
Items: 
Size: 360633 Color: 6823
Size: 326884 Color: 5526
Size: 312473 Color: 4814

Bin 3217: 11 of cap free
Amount of items: 3
Items: 
Size: 374478 Color: 7271
Size: 345176 Color: 6273
Size: 280336 Color: 2898

Bin 3218: 11 of cap free
Amount of items: 3
Items: 
Size: 366897 Color: 7007
Size: 350434 Color: 6472
Size: 282659 Color: 3074

Bin 3219: 11 of cap free
Amount of items: 3
Items: 
Size: 367261 Color: 7022
Size: 318296 Color: 5118
Size: 314433 Color: 4927

Bin 3220: 11 of cap free
Amount of items: 3
Items: 
Size: 365474 Color: 6970
Size: 322740 Color: 5330
Size: 311776 Color: 4774

Bin 3221: 11 of cap free
Amount of items: 3
Items: 
Size: 361791 Color: 6860
Size: 361521 Color: 6852
Size: 276678 Color: 2630

Bin 3222: 11 of cap free
Amount of items: 3
Items: 
Size: 360839 Color: 6830
Size: 360790 Color: 6829
Size: 278361 Color: 2756

Bin 3223: 11 of cap free
Amount of items: 3
Items: 
Size: 388666 Color: 7711
Size: 330732 Color: 5691
Size: 280592 Color: 2919

Bin 3224: 12 of cap free
Amount of items: 3
Items: 
Size: 400750 Color: 8046
Size: 330140 Color: 5672
Size: 269099 Color: 2055

Bin 3225: 12 of cap free
Amount of items: 3
Items: 
Size: 376628 Color: 7347
Size: 332814 Color: 5773
Size: 290547 Color: 3584

Bin 3226: 12 of cap free
Amount of items: 3
Items: 
Size: 357940 Color: 6742
Size: 331714 Color: 5722
Size: 310335 Color: 4696

Bin 3227: 12 of cap free
Amount of items: 3
Items: 
Size: 382133 Color: 7506
Size: 345393 Color: 6283
Size: 272463 Color: 2314

Bin 3228: 12 of cap free
Amount of items: 3
Items: 
Size: 391739 Color: 7797
Size: 328288 Color: 5593
Size: 279962 Color: 2874

Bin 3229: 12 of cap free
Amount of items: 3
Items: 
Size: 407070 Color: 8210
Size: 306284 Color: 4485
Size: 286635 Color: 3333

Bin 3230: 12 of cap free
Amount of items: 3
Items: 
Size: 364844 Color: 6944
Size: 321795 Color: 5278
Size: 313350 Color: 4869

Bin 3231: 12 of cap free
Amount of items: 3
Items: 
Size: 362785 Color: 6883
Size: 331673 Color: 5721
Size: 305531 Color: 4443

Bin 3232: 12 of cap free
Amount of items: 3
Items: 
Size: 353076 Color: 6583
Size: 353031 Color: 6581
Size: 293882 Color: 3791

Bin 3233: 12 of cap free
Amount of items: 3
Items: 
Size: 384954 Color: 7605
Size: 336470 Color: 5905
Size: 278565 Color: 2766

Bin 3234: 13 of cap free
Amount of items: 3
Items: 
Size: 405077 Color: 8164
Size: 312018 Color: 4786
Size: 282893 Color: 3087

Bin 3235: 13 of cap free
Amount of items: 3
Items: 
Size: 361590 Color: 6853
Size: 358046 Color: 6744
Size: 280352 Color: 2900

Bin 3236: 13 of cap free
Amount of items: 3
Items: 
Size: 345712 Color: 6293
Size: 340872 Color: 6108
Size: 313404 Color: 4873

Bin 3237: 13 of cap free
Amount of items: 3
Items: 
Size: 367459 Color: 7029
Size: 362714 Color: 6882
Size: 269815 Color: 2125

Bin 3238: 13 of cap free
Amount of items: 3
Items: 
Size: 358396 Color: 6755
Size: 348090 Color: 6386
Size: 293502 Color: 3776

Bin 3239: 13 of cap free
Amount of items: 3
Items: 
Size: 349351 Color: 6428
Size: 349317 Color: 6425
Size: 301320 Color: 4228

Bin 3240: 14 of cap free
Amount of items: 3
Items: 
Size: 400079 Color: 8030
Size: 347678 Color: 6370
Size: 252230 Color: 373

Bin 3241: 14 of cap free
Amount of items: 3
Items: 
Size: 375270 Color: 7303
Size: 326245 Color: 5494
Size: 298472 Color: 4059

Bin 3242: 14 of cap free
Amount of items: 3
Items: 
Size: 366692 Color: 7005
Size: 320004 Color: 5200
Size: 313291 Color: 4865

Bin 3243: 14 of cap free
Amount of items: 3
Items: 
Size: 366554 Color: 7002
Size: 338634 Color: 5997
Size: 294799 Color: 3852

Bin 3244: 14 of cap free
Amount of items: 3
Items: 
Size: 353907 Color: 6613
Size: 351047 Color: 6501
Size: 295033 Color: 3867

Bin 3245: 14 of cap free
Amount of items: 3
Items: 
Size: 356458 Color: 6697
Size: 355914 Color: 6673
Size: 287615 Color: 3406

Bin 3246: 14 of cap free
Amount of items: 3
Items: 
Size: 363473 Color: 6903
Size: 348416 Color: 6400
Size: 288098 Color: 3430

Bin 3247: 15 of cap free
Amount of items: 3
Items: 
Size: 388968 Color: 7720
Size: 359042 Color: 6775
Size: 251976 Color: 332

Bin 3248: 15 of cap free
Amount of items: 3
Items: 
Size: 388902 Color: 7718
Size: 331350 Color: 5711
Size: 279734 Color: 2857

Bin 3249: 15 of cap free
Amount of items: 3
Items: 
Size: 368043 Color: 7049
Size: 328920 Color: 5620
Size: 303023 Color: 4308

Bin 3250: 15 of cap free
Amount of items: 3
Items: 
Size: 383266 Color: 7551
Size: 328667 Color: 5605
Size: 288053 Color: 3428

Bin 3251: 15 of cap free
Amount of items: 3
Items: 
Size: 363032 Color: 6893
Size: 354780 Color: 6636
Size: 282174 Color: 3044

Bin 3252: 16 of cap free
Amount of items: 3
Items: 
Size: 376878 Color: 7351
Size: 325373 Color: 5452
Size: 297734 Color: 4016

Bin 3253: 16 of cap free
Amount of items: 3
Items: 
Size: 359628 Color: 6795
Size: 359605 Color: 6793
Size: 280752 Color: 2938

Bin 3254: 16 of cap free
Amount of items: 3
Items: 
Size: 374914 Color: 7290
Size: 340718 Color: 6103
Size: 284353 Color: 3181

Bin 3255: 16 of cap free
Amount of items: 3
Items: 
Size: 347576 Color: 6366
Size: 326637 Color: 5515
Size: 325772 Color: 5475

Bin 3256: 17 of cap free
Amount of items: 3
Items: 
Size: 358361 Color: 6754
Size: 340518 Color: 6093
Size: 301105 Color: 4214

Bin 3257: 17 of cap free
Amount of items: 3
Items: 
Size: 358142 Color: 6749
Size: 351701 Color: 6529
Size: 290141 Color: 3565

Bin 3258: 17 of cap free
Amount of items: 3
Items: 
Size: 364783 Color: 6941
Size: 336211 Color: 5894
Size: 298990 Color: 4085

Bin 3259: 18 of cap free
Amount of items: 3
Items: 
Size: 386019 Color: 7636
Size: 316900 Color: 5046
Size: 297064 Color: 3979

Bin 3260: 19 of cap free
Amount of items: 3
Items: 
Size: 395769 Color: 7914
Size: 329216 Color: 5631
Size: 274997 Color: 2505

Bin 3261: 19 of cap free
Amount of items: 3
Items: 
Size: 359826 Color: 6800
Size: 326079 Color: 5482
Size: 314077 Color: 4901

Bin 3262: 19 of cap free
Amount of items: 3
Items: 
Size: 368852 Color: 7079
Size: 344016 Color: 6218
Size: 287114 Color: 3363

Bin 3263: 19 of cap free
Amount of items: 3
Items: 
Size: 372443 Color: 7197
Size: 337668 Color: 5967
Size: 289871 Color: 3542

Bin 3264: 19 of cap free
Amount of items: 3
Items: 
Size: 364359 Color: 6928
Size: 337222 Color: 5947
Size: 298401 Color: 4056

Bin 3265: 19 of cap free
Amount of items: 3
Items: 
Size: 375625 Color: 7310
Size: 328101 Color: 5583
Size: 296256 Color: 3932

Bin 3266: 20 of cap free
Amount of items: 3
Items: 
Size: 405255 Color: 8167
Size: 327851 Color: 5573
Size: 266875 Color: 1885

Bin 3267: 21 of cap free
Amount of items: 3
Items: 
Size: 400040 Color: 8025
Size: 334823 Color: 5857
Size: 265117 Color: 1738

Bin 3268: 21 of cap free
Amount of items: 3
Items: 
Size: 374257 Color: 7263
Size: 345936 Color: 6307
Size: 279787 Color: 2862

Bin 3269: 22 of cap free
Amount of items: 3
Items: 
Size: 360388 Color: 6817
Size: 360251 Color: 6812
Size: 279340 Color: 2824

Bin 3270: 23 of cap free
Amount of items: 3
Items: 
Size: 372970 Color: 7220
Size: 315608 Color: 4987
Size: 311400 Color: 4748

Bin 3271: 24 of cap free
Amount of items: 3
Items: 
Size: 378246 Color: 7394
Size: 347282 Color: 6357
Size: 274449 Color: 2475

Bin 3272: 24 of cap free
Amount of items: 3
Items: 
Size: 354053 Color: 6616
Size: 334898 Color: 5860
Size: 311026 Color: 4729

Bin 3273: 25 of cap free
Amount of items: 3
Items: 
Size: 370017 Color: 7125
Size: 319958 Color: 5198
Size: 310001 Color: 4678

Bin 3274: 26 of cap free
Amount of items: 3
Items: 
Size: 394200 Color: 7872
Size: 339385 Color: 6034
Size: 266390 Color: 1849

Bin 3275: 26 of cap free
Amount of items: 3
Items: 
Size: 400675 Color: 8043
Size: 329572 Color: 5648
Size: 269728 Color: 2116

Bin 3276: 26 of cap free
Amount of items: 3
Items: 
Size: 367063 Color: 7014
Size: 326354 Color: 5497
Size: 306558 Color: 4496

Bin 3277: 27 of cap free
Amount of items: 3
Items: 
Size: 394877 Color: 7896
Size: 334146 Color: 5828
Size: 270951 Color: 2211

Bin 3278: 27 of cap free
Amount of items: 3
Items: 
Size: 409724 Color: 8276
Size: 326737 Color: 5519
Size: 263513 Color: 1594

Bin 3279: 27 of cap free
Amount of items: 3
Items: 
Size: 378579 Color: 7406
Size: 310799 Color: 4723
Size: 310596 Color: 4713

Bin 3280: 27 of cap free
Amount of items: 3
Items: 
Size: 365728 Color: 6976
Size: 322144 Color: 5300
Size: 312102 Color: 4791

Bin 3281: 27 of cap free
Amount of items: 3
Items: 
Size: 351367 Color: 6513
Size: 344394 Color: 6235
Size: 304213 Color: 4366

Bin 3282: 27 of cap free
Amount of items: 3
Items: 
Size: 352453 Color: 6553
Size: 352436 Color: 6552
Size: 295085 Color: 3869

Bin 3283: 27 of cap free
Amount of items: 3
Items: 
Size: 381675 Color: 7495
Size: 345273 Color: 6280
Size: 273026 Color: 2359

Bin 3284: 28 of cap free
Amount of items: 3
Items: 
Size: 341585 Color: 6131
Size: 332259 Color: 5747
Size: 326129 Color: 5487

Bin 3285: 28 of cap free
Amount of items: 3
Items: 
Size: 383733 Color: 7567
Size: 336749 Color: 5917
Size: 279491 Color: 2834

Bin 3286: 29 of cap free
Amount of items: 3
Items: 
Size: 387963 Color: 7686
Size: 308400 Color: 4589
Size: 303609 Color: 4336

Bin 3287: 29 of cap free
Amount of items: 3
Items: 
Size: 404724 Color: 8151
Size: 300844 Color: 4199
Size: 294404 Color: 3830

Bin 3288: 30 of cap free
Amount of items: 3
Items: 
Size: 385009 Color: 7606
Size: 346101 Color: 6312
Size: 268861 Color: 2041

Bin 3289: 31 of cap free
Amount of items: 3
Items: 
Size: 392065 Color: 7807
Size: 339680 Color: 6047
Size: 268225 Color: 1990

Bin 3290: 32 of cap free
Amount of items: 3
Items: 
Size: 402744 Color: 8103
Size: 327616 Color: 5561
Size: 269609 Color: 2111

Bin 3291: 33 of cap free
Amount of items: 3
Items: 
Size: 362124 Color: 6866
Size: 344672 Color: 6245
Size: 293172 Color: 3762

Bin 3292: 36 of cap free
Amount of items: 3
Items: 
Size: 360018 Color: 6806
Size: 339054 Color: 6019
Size: 300893 Color: 4202

Bin 3293: 36 of cap free
Amount of items: 3
Items: 
Size: 350101 Color: 6456
Size: 327895 Color: 5577
Size: 321969 Color: 5290

Bin 3294: 37 of cap free
Amount of items: 3
Items: 
Size: 393469 Color: 7848
Size: 322432 Color: 5311
Size: 284063 Color: 3165

Bin 3295: 40 of cap free
Amount of items: 3
Items: 
Size: 366037 Color: 6981
Size: 326774 Color: 5521
Size: 307150 Color: 4528

Bin 3296: 41 of cap free
Amount of items: 3
Items: 
Size: 360989 Color: 6832
Size: 350613 Color: 6478
Size: 288358 Color: 3450

Bin 3297: 43 of cap free
Amount of items: 3
Items: 
Size: 379089 Color: 7427
Size: 355372 Color: 6658
Size: 265497 Color: 1774

Bin 3298: 44 of cap free
Amount of items: 3
Items: 
Size: 372057 Color: 7180
Size: 352602 Color: 6564
Size: 275298 Color: 2532

Bin 3299: 46 of cap free
Amount of items: 3
Items: 
Size: 380116 Color: 7449
Size: 342306 Color: 6165
Size: 277533 Color: 2680

Bin 3300: 46 of cap free
Amount of items: 3
Items: 
Size: 361373 Color: 6845
Size: 351539 Color: 6521
Size: 287043 Color: 3358

Bin 3301: 47 of cap free
Amount of items: 3
Items: 
Size: 400610 Color: 8041
Size: 307890 Color: 4559
Size: 291454 Color: 3639

Bin 3302: 49 of cap free
Amount of items: 3
Items: 
Size: 388835 Color: 7715
Size: 322876 Color: 5335
Size: 288241 Color: 3438

Bin 3303: 52 of cap free
Amount of items: 3
Items: 
Size: 352141 Color: 6545
Size: 346043 Color: 6310
Size: 301765 Color: 4248

Bin 3304: 53 of cap free
Amount of items: 3
Items: 
Size: 371560 Color: 7165
Size: 318023 Color: 5105
Size: 310365 Color: 4699

Bin 3305: 53 of cap free
Amount of items: 3
Items: 
Size: 372238 Color: 7186
Size: 350520 Color: 6475
Size: 277190 Color: 2657

Bin 3306: 54 of cap free
Amount of items: 3
Items: 
Size: 398879 Color: 7999
Size: 316804 Color: 5041
Size: 284264 Color: 3175

Bin 3307: 57 of cap free
Amount of items: 3
Items: 
Size: 406312 Color: 8189
Size: 306687 Color: 4506
Size: 286945 Color: 3352

Bin 3308: 58 of cap free
Amount of items: 3
Items: 
Size: 387712 Color: 7680
Size: 341198 Color: 6117
Size: 271033 Color: 2214

Bin 3309: 66 of cap free
Amount of items: 3
Items: 
Size: 391629 Color: 7792
Size: 312923 Color: 4842
Size: 295383 Color: 3883

Bin 3310: 66 of cap free
Amount of items: 3
Items: 
Size: 364001 Color: 6917
Size: 344861 Color: 6256
Size: 291073 Color: 3608

Bin 3311: 67 of cap free
Amount of items: 3
Items: 
Size: 369584 Color: 7106
Size: 340333 Color: 6084
Size: 290017 Color: 3555

Bin 3312: 69 of cap free
Amount of items: 3
Items: 
Size: 385700 Color: 7626
Size: 334272 Color: 5835
Size: 279960 Color: 2873

Bin 3313: 70 of cap free
Amount of items: 3
Items: 
Size: 374065 Color: 7257
Size: 346526 Color: 6328
Size: 279340 Color: 2825

Bin 3314: 73 of cap free
Amount of items: 3
Items: 
Size: 377471 Color: 7379
Size: 338613 Color: 5995
Size: 283844 Color: 3151

Bin 3315: 81 of cap free
Amount of items: 3
Items: 
Size: 402238 Color: 8090
Size: 344989 Color: 6261
Size: 252693 Color: 438

Bin 3316: 92 of cap free
Amount of items: 3
Items: 
Size: 374783 Color: 7283
Size: 372600 Color: 7203
Size: 252526 Color: 419

Bin 3317: 108 of cap free
Amount of items: 3
Items: 
Size: 370979 Color: 7155
Size: 362384 Color: 6872
Size: 266530 Color: 1860

Bin 3318: 116 of cap free
Amount of items: 3
Items: 
Size: 398450 Color: 7986
Size: 302694 Color: 4287
Size: 298741 Color: 4073

Bin 3319: 137 of cap free
Amount of items: 3
Items: 
Size: 356339 Color: 6693
Size: 355214 Color: 6652
Size: 288311 Color: 3444

Bin 3320: 142 of cap free
Amount of items: 3
Items: 
Size: 380591 Color: 7465
Size: 344058 Color: 6227
Size: 275210 Color: 2522

Bin 3321: 153 of cap free
Amount of items: 3
Items: 
Size: 380072 Color: 7448
Size: 339678 Color: 6045
Size: 280098 Color: 2885

Bin 3322: 165 of cap free
Amount of items: 3
Items: 
Size: 369290 Color: 7090
Size: 326019 Color: 5481
Size: 304527 Color: 4382

Bin 3323: 166 of cap free
Amount of items: 3
Items: 
Size: 369961 Color: 7118
Size: 348681 Color: 6410
Size: 281193 Color: 2964

Bin 3324: 188 of cap free
Amount of items: 3
Items: 
Size: 352133 Color: 6544
Size: 332358 Color: 5753
Size: 315322 Color: 4975

Bin 3325: 228 of cap free
Amount of items: 3
Items: 
Size: 377609 Color: 7384
Size: 329030 Color: 5625
Size: 293134 Color: 3757

Bin 3326: 230 of cap free
Amount of items: 3
Items: 
Size: 361677 Color: 6857
Size: 329421 Color: 5641
Size: 308673 Color: 4605

Bin 3327: 498 of cap free
Amount of items: 3
Items: 
Size: 362999 Color: 6891
Size: 330515 Color: 5682
Size: 305989 Color: 4465

Bin 3328: 504 of cap free
Amount of items: 3
Items: 
Size: 364143 Color: 6921
Size: 342893 Color: 6185
Size: 292461 Color: 3714

Bin 3329: 539 of cap free
Amount of items: 3
Items: 
Size: 384566 Color: 7593
Size: 341934 Color: 6153
Size: 272962 Color: 2352

Bin 3330: 684 of cap free
Amount of items: 3
Items: 
Size: 373903 Color: 7252
Size: 344049 Color: 6225
Size: 281365 Color: 2983

Bin 3331: 734 of cap free
Amount of items: 3
Items: 
Size: 356306 Color: 6689
Size: 347111 Color: 6350
Size: 295850 Color: 3907

Bin 3332: 30483 of cap free
Amount of items: 3
Items: 
Size: 355790 Color: 6664
Size: 319252 Color: 5169
Size: 294476 Color: 3833

Bin 3333: 220677 of cap free
Amount of items: 2
Items: 
Size: 499987 Color: 10001
Size: 279337 Color: 2823

Bin 3334: 368352 of cap free
Amount of items: 2
Items: 
Size: 315761 Color: 4995
Size: 315888 Color: 5001

Bin 3335: 371362 of cap free
Amount of items: 2
Items: 
Size: 289056 Color: 3490
Size: 339583 Color: 6043

Total size: 3334003334
Total free space: 1000001

