Capicity Bin: 16304
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 14490 Color: 1
Size: 1382 Color: 1
Size: 432 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12822 Color: 1
Size: 2902 Color: 1
Size: 580 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 1
Size: 3180 Color: 1
Size: 632 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 1
Size: 5928 Color: 1
Size: 1168 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 1
Size: 7400 Color: 1
Size: 736 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12950 Color: 1
Size: 2000 Color: 1
Size: 1354 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9193 Color: 1
Size: 5927 Color: 1
Size: 1184 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 1
Size: 4290 Color: 1
Size: 662 Color: 0

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 10428 Color: 1
Size: 4044 Color: 1
Size: 1192 Color: 0
Size: 640 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11698 Color: 1
Size: 4124 Color: 1
Size: 482 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 10508 Color: 1
Size: 4156 Color: 1
Size: 872 Color: 0
Size: 768 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 13742 Color: 1
Size: 1662 Color: 1
Size: 472 Color: 0
Size: 428 Color: 0

Bin 13: 0 of cap free
Amount of items: 5
Items: 
Size: 6140 Color: 1
Size: 5314 Color: 1
Size: 3474 Color: 1
Size: 1296 Color: 0
Size: 80 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8162 Color: 1
Size: 7088 Color: 1
Size: 1054 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 1
Size: 5704 Color: 1
Size: 590 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 1
Size: 4220 Color: 1
Size: 840 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9995 Color: 1
Size: 5259 Color: 1
Size: 1050 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9161 Color: 1
Size: 5953 Color: 1
Size: 1190 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 1
Size: 5552 Color: 1
Size: 328 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9860 Color: 1
Size: 5700 Color: 1
Size: 744 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11500 Color: 1
Size: 3448 Color: 1
Size: 1356 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 1
Size: 4754 Color: 1
Size: 872 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9688 Color: 1
Size: 6152 Color: 1
Size: 464 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8940 Color: 1
Size: 6796 Color: 1
Size: 568 Color: 0

Bin 25: 0 of cap free
Amount of items: 4
Items: 
Size: 10088 Color: 1
Size: 4836 Color: 1
Size: 1060 Color: 0
Size: 320 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 1
Size: 5850 Color: 1
Size: 1168 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 1
Size: 4856 Color: 0
Size: 960 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 1
Size: 6872 Color: 1
Size: 1232 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8154 Color: 1
Size: 6794 Color: 1
Size: 1356 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 1
Size: 896 Color: 1
Size: 856 Color: 0

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 11375 Color: 1
Size: 2937 Color: 1
Size: 1184 Color: 0
Size: 808 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8170 Color: 1
Size: 6782 Color: 1
Size: 1352 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8172 Color: 1
Size: 6780 Color: 1
Size: 1352 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8636 Color: 1
Size: 6396 Color: 1
Size: 1272 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 1
Size: 6488 Color: 1
Size: 832 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 9940 Color: 1
Size: 5308 Color: 1
Size: 1056 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 10011 Color: 1
Size: 5245 Color: 1
Size: 1048 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 10168 Color: 1
Size: 5128 Color: 1
Size: 1008 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 1
Size: 4284 Color: 1
Size: 856 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 1
Size: 4200 Color: 1
Size: 832 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11746 Color: 1
Size: 3802 Color: 1
Size: 756 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 1
Size: 3774 Color: 1
Size: 752 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11785 Color: 1
Size: 3767 Color: 1
Size: 752 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11823 Color: 1
Size: 3735 Color: 1
Size: 746 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 1
Size: 3633 Color: 1
Size: 832 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 1
Size: 4136 Color: 1
Size: 288 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 1
Size: 3688 Color: 1
Size: 624 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 1
Size: 4004 Color: 1
Size: 228 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12138 Color: 1
Size: 3118 Color: 1
Size: 1048 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 1
Size: 3608 Color: 1
Size: 544 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 1
Size: 3423 Color: 1
Size: 684 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 1
Size: 3166 Color: 1
Size: 920 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 1
Size: 3196 Color: 1
Size: 780 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 1
Size: 3260 Color: 1
Size: 648 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12427 Color: 1
Size: 3205 Color: 1
Size: 672 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 1
Size: 3124 Color: 1
Size: 704 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 1
Size: 2838 Color: 1
Size: 960 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12556 Color: 1
Size: 3102 Color: 1
Size: 646 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12598 Color: 1
Size: 3090 Color: 1
Size: 616 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12727 Color: 1
Size: 2981 Color: 1
Size: 596 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12761 Color: 1
Size: 2957 Color: 1
Size: 586 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12781 Color: 1
Size: 2903 Color: 1
Size: 620 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 12821 Color: 1
Size: 2535 Color: 1
Size: 948 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 1
Size: 2362 Color: 1
Size: 1066 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 1
Size: 2782 Color: 1
Size: 620 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 1
Size: 3176 Color: 1
Size: 192 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 1
Size: 2860 Color: 1
Size: 478 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 1
Size: 2654 Color: 1
Size: 678 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 12982 Color: 1
Size: 2770 Color: 1
Size: 552 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 1
Size: 2724 Color: 1
Size: 580 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 1
Size: 2731 Color: 1
Size: 544 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13036 Color: 1
Size: 2780 Color: 1
Size: 488 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13072 Color: 1
Size: 2760 Color: 1
Size: 472 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13080 Color: 1
Size: 2936 Color: 1
Size: 288 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 1
Size: 2638 Color: 1
Size: 524 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13201 Color: 1
Size: 2587 Color: 1
Size: 516 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 1
Size: 2570 Color: 1
Size: 512 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 1
Size: 2696 Color: 1
Size: 368 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 1
Size: 2324 Color: 1
Size: 704 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 1
Size: 2521 Color: 1
Size: 504 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13312 Color: 1
Size: 2328 Color: 1
Size: 664 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13356 Color: 1
Size: 2512 Color: 1
Size: 436 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 1
Size: 2462 Color: 1
Size: 420 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 1
Size: 2371 Color: 1
Size: 506 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 1
Size: 2200 Color: 1
Size: 672 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 1
Size: 2444 Color: 1
Size: 424 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13450 Color: 1
Size: 1932 Color: 1
Size: 922 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 1
Size: 2580 Color: 1
Size: 272 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13467 Color: 1
Size: 2365 Color: 1
Size: 472 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 1
Size: 2408 Color: 1
Size: 384 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 1
Size: 2460 Color: 1
Size: 328 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13567 Color: 1
Size: 2173 Color: 1
Size: 564 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13583 Color: 1
Size: 2269 Color: 1
Size: 452 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13592 Color: 1
Size: 2264 Color: 1
Size: 448 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 1
Size: 2524 Color: 1
Size: 108 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 1
Size: 2196 Color: 1
Size: 432 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 1
Size: 2246 Color: 1
Size: 380 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 1
Size: 2131 Color: 1
Size: 424 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 13774 Color: 1
Size: 2138 Color: 1
Size: 392 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 1
Size: 2020 Color: 1
Size: 488 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 2042 Color: 1
Size: 408 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13857 Color: 1
Size: 2041 Color: 1
Size: 406 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 13884 Color: 1
Size: 2092 Color: 1
Size: 328 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 1992 Color: 1
Size: 384 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 1
Size: 1982 Color: 1
Size: 392 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 1
Size: 1914 Color: 1
Size: 440 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 1740 Color: 1
Size: 576 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 1
Size: 1928 Color: 1
Size: 368 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 1
Size: 1962 Color: 1
Size: 332 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1876 Color: 1
Size: 368 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 1
Size: 1868 Color: 1
Size: 368 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14074 Color: 1
Size: 1766 Color: 1
Size: 464 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 1
Size: 1858 Color: 1
Size: 368 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 1
Size: 1774 Color: 1
Size: 442 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 1
Size: 1666 Color: 1
Size: 460 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 1
Size: 1674 Color: 1
Size: 410 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14238 Color: 1
Size: 1722 Color: 1
Size: 344 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14298 Color: 1
Size: 1606 Color: 1
Size: 400 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14300 Color: 1
Size: 1676 Color: 1
Size: 328 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14306 Color: 1
Size: 1718 Color: 1
Size: 280 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14314 Color: 1
Size: 1862 Color: 1
Size: 128 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14316 Color: 1
Size: 1644 Color: 1
Size: 344 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 1
Size: 1620 Color: 1
Size: 352 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 1
Size: 1784 Color: 1
Size: 176 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14364 Color: 1
Size: 1384 Color: 1
Size: 556 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14378 Color: 1
Size: 1492 Color: 1
Size: 434 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 1
Size: 1604 Color: 1
Size: 320 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 1
Size: 1428 Color: 1
Size: 446 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14434 Color: 1
Size: 1562 Color: 1
Size: 308 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 1
Size: 1524 Color: 1
Size: 304 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 1
Size: 1448 Color: 1
Size: 368 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 1
Size: 1506 Color: 1
Size: 300 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 1
Size: 1512 Color: 1
Size: 276 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1484 Color: 1
Size: 288 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 1
Size: 1640 Color: 1
Size: 96 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 1
Size: 1442 Color: 1
Size: 288 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 1
Size: 1412 Color: 1
Size: 296 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1136 Color: 1
Size: 556 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14648 Color: 1
Size: 1024 Color: 1
Size: 632 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 14650 Color: 1
Size: 1224 Color: 1
Size: 430 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 14668 Color: 1
Size: 1364 Color: 1
Size: 272 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 13103 Color: 1
Size: 2568 Color: 1
Size: 632 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 1
Size: 6771 Color: 1
Size: 1344 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 1
Size: 3721 Color: 1
Size: 820 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 1
Size: 4147 Color: 1
Size: 160 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 1
Size: 3399 Color: 1
Size: 828 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 12211 Color: 1
Size: 3604 Color: 1
Size: 488 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 12359 Color: 1
Size: 3320 Color: 1
Size: 624 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 1
Size: 2411 Color: 1
Size: 936 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 1
Size: 2399 Color: 1
Size: 532 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 1
Size: 2221 Color: 1
Size: 472 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13657 Color: 1
Size: 2314 Color: 1
Size: 332 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 13833 Color: 1
Size: 2190 Color: 1
Size: 280 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 11329 Color: 1
Size: 4294 Color: 1
Size: 680 Color: 0

Bin 155: 1 of cap free
Amount of items: 4
Items: 
Size: 9979 Color: 1
Size: 5572 Color: 1
Size: 496 Color: 0
Size: 256 Color: 0

Bin 156: 1 of cap free
Amount of items: 5
Items: 
Size: 8181 Color: 1
Size: 4109 Color: 1
Size: 2153 Color: 1
Size: 1168 Color: 0
Size: 692 Color: 0

Bin 157: 1 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 1
Size: 3231 Color: 1
Size: 824 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 1
Size: 3411 Color: 1
Size: 560 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 1
Size: 5924 Color: 1
Size: 448 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 11618 Color: 1
Size: 3596 Color: 1
Size: 1088 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 8157 Color: 1
Size: 6793 Color: 1
Size: 1352 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 1
Size: 6786 Color: 1
Size: 320 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 1
Size: 5982 Color: 1
Size: 856 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 1
Size: 4684 Color: 1
Size: 464 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 11158 Color: 1
Size: 4904 Color: 1
Size: 240 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 1
Size: 4758 Color: 1
Size: 320 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 11372 Color: 1
Size: 4248 Color: 1
Size: 682 Color: 0

Bin 168: 2 of cap free
Amount of items: 3
Items: 
Size: 12227 Color: 1
Size: 3311 Color: 1
Size: 764 Color: 0

Bin 169: 2 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 1
Size: 3384 Color: 1
Size: 352 Color: 0

Bin 170: 2 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 1
Size: 2798 Color: 1
Size: 712 Color: 0

Bin 171: 2 of cap free
Amount of items: 3
Items: 
Size: 13697 Color: 1
Size: 2061 Color: 1
Size: 544 Color: 0

Bin 172: 2 of cap free
Amount of items: 3
Items: 
Size: 10687 Color: 1
Size: 3505 Color: 1
Size: 2110 Color: 0

Bin 173: 2 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 3528 Color: 1
Size: 590 Color: 0

Bin 174: 2 of cap free
Amount of items: 3
Items: 
Size: 8169 Color: 1
Size: 6781 Color: 1
Size: 1352 Color: 0

Bin 175: 2 of cap free
Amount of items: 3
Items: 
Size: 13354 Color: 1
Size: 2396 Color: 1
Size: 552 Color: 0

Bin 176: 3 of cap free
Amount of items: 3
Items: 
Size: 13530 Color: 1
Size: 2291 Color: 1
Size: 480 Color: 0

Bin 177: 3 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 1
Size: 2382 Color: 1
Size: 656 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 8048 Color: 1
Size: 7452 Color: 1
Size: 800 Color: 0

Bin 179: 4 of cap free
Amount of items: 3
Items: 
Size: 10769 Color: 1
Size: 4603 Color: 1
Size: 928 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 10781 Color: 1
Size: 5246 Color: 1
Size: 272 Color: 0

Bin 181: 7 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 1
Size: 1848 Color: 0
Size: 808 Color: 0

Bin 182: 10 of cap free
Amount of items: 3
Items: 
Size: 10598 Color: 1
Size: 5192 Color: 1
Size: 504 Color: 0

Bin 183: 11 of cap free
Amount of items: 3
Items: 
Size: 13721 Color: 1
Size: 2288 Color: 1
Size: 284 Color: 0

Bin 184: 21 of cap free
Amount of items: 3
Items: 
Size: 10692 Color: 1
Size: 5271 Color: 1
Size: 320 Color: 0

Bin 185: 42 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1358 Color: 0
Size: 736 Color: 1

Bin 186: 42 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 1
Size: 2953 Color: 1
Size: 552 Color: 0

Bin 187: 43 of cap free
Amount of items: 5
Items: 
Size: 9130 Color: 1
Size: 4681 Color: 1
Size: 1422 Color: 1
Size: 756 Color: 0
Size: 272 Color: 0

Bin 188: 44 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 1
Size: 6080 Color: 1
Size: 1660 Color: 0

Bin 189: 46 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 1
Size: 3406 Color: 1
Size: 1528 Color: 0

Bin 190: 53 of cap free
Amount of items: 3
Items: 
Size: 12243 Color: 1
Size: 3592 Color: 1
Size: 416 Color: 0

Bin 191: 77 of cap free
Amount of items: 3
Items: 
Size: 12582 Color: 1
Size: 2669 Color: 1
Size: 976 Color: 0

Bin 192: 130 of cap free
Amount of items: 3
Items: 
Size: 11452 Color: 1
Size: 3906 Color: 1
Size: 816 Color: 0

Bin 193: 520 of cap free
Amount of items: 3
Items: 
Size: 8156 Color: 1
Size: 3842 Color: 0
Size: 3786 Color: 1

Bin 194: 542 of cap free
Amount of items: 3
Items: 
Size: 8153 Color: 1
Size: 7081 Color: 1
Size: 528 Color: 0

Bin 195: 1706 of cap free
Amount of items: 1
Items: 
Size: 14598 Color: 1

Bin 196: 2834 of cap free
Amount of items: 1
Items: 
Size: 13470 Color: 1

Bin 197: 2893 of cap free
Amount of items: 1
Items: 
Size: 13411 Color: 1

Bin 198: 3367 of cap free
Amount of items: 3
Items: 
Size: 5528 Color: 1
Size: 4613 Color: 1
Size: 2796 Color: 0

Bin 199: 3845 of cap free
Amount of items: 1
Items: 
Size: 12459 Color: 1

Total size: 3228192
Total free space: 16304

