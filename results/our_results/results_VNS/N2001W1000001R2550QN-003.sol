Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2001

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 381312 Color: 1499
Size: 362283 Color: 1386
Size: 256406 Color: 162

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 430524 Color: 1756
Size: 304056 Color: 882
Size: 265421 Color: 336

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 477873 Color: 1941
Size: 271244 Color: 443
Size: 250884 Color: 35

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 426270 Color: 1734
Size: 292007 Color: 753
Size: 281724 Color: 622

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 460904 Color: 1872
Size: 277938 Color: 572
Size: 261159 Color: 257

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 456736 Color: 1854
Size: 285885 Color: 677
Size: 257380 Color: 183

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 375781 Color: 1465
Size: 354265 Color: 1340
Size: 269955 Color: 416

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 447302 Color: 1820
Size: 277027 Color: 563
Size: 275672 Color: 539

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 386523 Color: 1524
Size: 355765 Color: 1351
Size: 257713 Color: 187

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 455286 Color: 1849
Size: 275602 Color: 536
Size: 269113 Color: 400

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 370229 Color: 1431
Size: 347349 Color: 1279
Size: 282423 Color: 634

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 434784 Color: 1775
Size: 287272 Color: 700
Size: 277945 Color: 573

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 353249 Color: 1332
Size: 327920 Color: 1118
Size: 318832 Color: 1025

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 381070 Color: 1494
Size: 339899 Color: 1205
Size: 279032 Color: 587

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 381616 Color: 1500
Size: 368280 Color: 1419
Size: 250105 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 362244 Color: 1385
Size: 319053 Color: 1033
Size: 318704 Color: 1022

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 376153 Color: 1468
Size: 355312 Color: 1347
Size: 268536 Color: 391

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 409432 Color: 1648
Size: 334318 Color: 1165
Size: 256251 Color: 161

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 399986 Color: 1594
Size: 340586 Color: 1210
Size: 259429 Color: 226

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 411547 Color: 1655
Size: 335994 Color: 1174
Size: 252460 Color: 82

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379471 Color: 1484
Size: 348950 Color: 1293
Size: 271580 Color: 455

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 372768 Color: 1449
Size: 369407 Color: 1426
Size: 257826 Color: 189

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 377690 Color: 1475
Size: 344777 Color: 1246
Size: 277534 Color: 569

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 399741 Color: 1591
Size: 316893 Color: 1011
Size: 283367 Color: 646

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 377215 Color: 1473
Size: 339303 Color: 1201
Size: 283483 Color: 650

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 388501 Color: 1534
Size: 354351 Color: 1341
Size: 257149 Color: 177

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 390106 Color: 1543
Size: 353330 Color: 1333
Size: 256565 Color: 164

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 405324 Color: 1620
Size: 341495 Color: 1217
Size: 253182 Color: 107

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 405682 Color: 1624
Size: 335565 Color: 1173
Size: 258754 Color: 213

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 408161 Color: 1639
Size: 329481 Color: 1132
Size: 262359 Color: 279

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382744 Color: 1506
Size: 341822 Color: 1222
Size: 275435 Color: 533

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 372219 Color: 1446
Size: 338673 Color: 1199
Size: 289109 Color: 722

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 371901 Color: 1443
Size: 367667 Color: 1417
Size: 260433 Color: 242

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 381114 Color: 1496
Size: 338053 Color: 1190
Size: 280834 Color: 611

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 406255 Color: 1629
Size: 319073 Color: 1034
Size: 274673 Color: 516

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 462547 Color: 1883
Size: 270381 Color: 428
Size: 267073 Color: 363

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 372967 Color: 1453
Size: 362873 Color: 1390
Size: 264161 Color: 310

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 387354 Color: 1530
Size: 352248 Color: 1321
Size: 260399 Color: 239

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 391666 Color: 1548
Size: 328578 Color: 1125
Size: 279757 Color: 595

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 401649 Color: 1601
Size: 301964 Color: 863
Size: 296388 Color: 802

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 373837 Color: 1458
Size: 373632 Color: 1455
Size: 252532 Color: 86

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 368909 Color: 1423
Size: 352410 Color: 1323
Size: 278682 Color: 583

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 400780 Color: 1597
Size: 333606 Color: 1159
Size: 265615 Color: 341

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 386906 Color: 1528
Size: 345745 Color: 1258
Size: 267350 Color: 372

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 392381 Color: 1553
Size: 341858 Color: 1223
Size: 265762 Color: 342

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 398994 Color: 1589
Size: 325289 Color: 1093
Size: 275718 Color: 540

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 395772 Color: 1572
Size: 327233 Color: 1110
Size: 276996 Color: 561

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 392474 Color: 1555
Size: 328418 Color: 1122
Size: 279109 Color: 588

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 381224 Color: 1497
Size: 342315 Color: 1227
Size: 276462 Color: 551

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 370552 Color: 1435
Size: 367456 Color: 1414
Size: 261993 Color: 273

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 396503 Color: 1577
Size: 344978 Color: 1248
Size: 258520 Color: 208

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 408213 Color: 1640
Size: 338117 Color: 1191
Size: 253671 Color: 120

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 405726 Color: 1625
Size: 322732 Color: 1073
Size: 271543 Color: 452

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 377243 Color: 1474
Size: 351382 Color: 1314
Size: 271376 Color: 449

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 372780 Color: 1450
Size: 364475 Color: 1396
Size: 262746 Color: 287

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 370466 Color: 1433
Size: 362222 Color: 1384
Size: 267313 Color: 371

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 397254 Color: 1578
Size: 330682 Color: 1140
Size: 272065 Color: 462

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 397639 Color: 1580
Size: 341881 Color: 1224
Size: 260481 Color: 243

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 393710 Color: 1563
Size: 329672 Color: 1133
Size: 276619 Color: 553

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 389380 Color: 1536
Size: 337906 Color: 1189
Size: 272715 Color: 477

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 399389 Color: 1590
Size: 305812 Color: 903
Size: 294800 Color: 778

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 408557 Color: 1642
Size: 318722 Color: 1023
Size: 272722 Color: 478

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 405117 Color: 1618
Size: 332493 Color: 1153
Size: 262391 Color: 280

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 398536 Color: 1586
Size: 347810 Color: 1281
Size: 253655 Color: 119

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 359312 Color: 1372
Size: 356960 Color: 1355
Size: 283729 Color: 656

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 408879 Color: 1646
Size: 332493 Color: 1154
Size: 258629 Color: 210

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 380871 Color: 1493
Size: 342095 Color: 1225
Size: 277035 Color: 564

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 370533 Color: 1434
Size: 368352 Color: 1420
Size: 261116 Color: 253

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 392710 Color: 1559
Size: 320629 Color: 1047
Size: 286662 Color: 690

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 389827 Color: 1540
Size: 317155 Color: 1014
Size: 293019 Color: 763

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 404034 Color: 1608
Size: 321321 Color: 1054
Size: 274646 Color: 515

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 364241 Color: 1394
Size: 346936 Color: 1270
Size: 288824 Color: 718

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 408572 Color: 1643
Size: 318462 Color: 1020
Size: 272967 Color: 484

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 366074 Color: 1407
Size: 357856 Color: 1359
Size: 276071 Color: 546

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 377707 Color: 1476
Size: 344214 Color: 1243
Size: 278080 Color: 575

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 368863 Color: 1422
Size: 350735 Color: 1309
Size: 280403 Color: 604

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 395926 Color: 1573
Size: 330422 Color: 1138
Size: 273653 Color: 499

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 400873 Color: 1599
Size: 326962 Color: 1107
Size: 272166 Color: 466

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 384334 Color: 1514
Size: 326087 Color: 1097
Size: 289580 Color: 727

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 385403 Color: 1516
Size: 322295 Color: 1066
Size: 292303 Color: 755

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 381627 Color: 1501
Size: 345243 Color: 1252
Size: 273131 Color: 490

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 393711 Color: 1564
Size: 333640 Color: 1160
Size: 272650 Color: 476

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 365716 Color: 1403
Size: 354876 Color: 1345
Size: 279409 Color: 591

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 383331 Color: 1508
Size: 315454 Color: 990
Size: 301216 Color: 856

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 383374 Color: 1509
Size: 331616 Color: 1147
Size: 285011 Color: 667

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 381747 Color: 1503
Size: 327570 Color: 1113
Size: 290684 Color: 742

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 397957 Color: 1581
Size: 313210 Color: 968
Size: 288834 Color: 719

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 400601 Color: 1596
Size: 328511 Color: 1123
Size: 270889 Color: 435

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 392705 Color: 1558
Size: 331461 Color: 1146
Size: 275835 Color: 542

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 389112 Color: 1535
Size: 328540 Color: 1124
Size: 282349 Color: 633

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 375000 Color: 1462
Size: 340465 Color: 1209
Size: 284536 Color: 662

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 374193 Color: 1460
Size: 321364 Color: 1055
Size: 304444 Color: 888

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 365424 Color: 1402
Size: 350615 Color: 1306
Size: 283962 Color: 658

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 369270 Color: 1425
Size: 344495 Color: 1244
Size: 286236 Color: 680

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 413010 Color: 1663
Size: 298889 Color: 828
Size: 288102 Color: 709

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 412599 Color: 1660
Size: 326846 Color: 1106
Size: 260556 Color: 246

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 366604 Color: 1409
Size: 350510 Color: 1305
Size: 282887 Color: 639

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 394400 Color: 1566
Size: 309130 Color: 931
Size: 296471 Color: 806

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 381997 Color: 1505
Size: 336350 Color: 1176
Size: 281654 Color: 621

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 385892 Color: 1519
Size: 339780 Color: 1203
Size: 274329 Color: 511

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 386727 Color: 1526
Size: 322804 Color: 1075
Size: 290470 Color: 737

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 412306 Color: 1657
Size: 320856 Color: 1048
Size: 266839 Color: 360

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 391338 Color: 1547
Size: 357174 Color: 1356
Size: 251489 Color: 58

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 371753 Color: 1439
Size: 314459 Color: 979
Size: 313789 Color: 973

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 379423 Color: 1483
Size: 361035 Color: 1378
Size: 259543 Color: 228

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 343052 Color: 1231
Size: 331895 Color: 1150
Size: 325054 Color: 1090

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 373997 Color: 1459
Size: 370850 Color: 1436
Size: 255154 Color: 147

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 406185 Color: 1628
Size: 342782 Color: 1229
Size: 251034 Color: 38

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 370147 Color: 1430
Size: 364816 Color: 1397
Size: 265038 Color: 325

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 398420 Color: 1585
Size: 326552 Color: 1101
Size: 275029 Color: 522

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 388438 Color: 1533
Size: 316721 Color: 1008
Size: 294842 Color: 780

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 390501 Color: 1545
Size: 344879 Color: 1247
Size: 264621 Color: 317

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 404678 Color: 1614
Size: 336143 Color: 1175
Size: 259180 Color: 221

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 378023 Color: 1480
Size: 320885 Color: 1050
Size: 301093 Color: 855

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 379724 Color: 1486
Size: 348627 Color: 1290
Size: 271650 Color: 458

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 379392 Color: 1482
Size: 341692 Color: 1219
Size: 278917 Color: 586

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 401396 Color: 1600
Size: 302778 Color: 871
Size: 295827 Color: 797

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 389647 Color: 1539
Size: 343177 Color: 1233
Size: 267177 Color: 365

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 405771 Color: 1626
Size: 327889 Color: 1117
Size: 266341 Color: 354

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 408436 Color: 1641
Size: 311699 Color: 961
Size: 279866 Color: 598

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 365118 Color: 1400
Size: 364906 Color: 1398
Size: 269977 Color: 418

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 389591 Color: 1537
Size: 340266 Color: 1207
Size: 270144 Color: 425

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 379650 Color: 1485
Size: 361047 Color: 1381
Size: 259304 Color: 223

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 386589 Color: 1525
Size: 333640 Color: 1161
Size: 279772 Color: 596

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 412130 Color: 1656
Size: 322456 Color: 1068
Size: 265415 Color: 335

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 358397 Color: 1361
Size: 352014 Color: 1319
Size: 289590 Color: 728

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 406909 Color: 1632
Size: 325204 Color: 1091
Size: 267888 Color: 381

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 399791 Color: 1592
Size: 344630 Color: 1245
Size: 255580 Color: 152

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 412596 Color: 1659
Size: 322961 Color: 1077
Size: 264444 Color: 314

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 366523 Color: 1408
Size: 345198 Color: 1251
Size: 288280 Color: 712

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 358794 Color: 1365
Size: 358698 Color: 1364
Size: 282509 Color: 636

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 406057 Color: 1627
Size: 334068 Color: 1163
Size: 259876 Color: 232

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 402379 Color: 1603
Size: 346762 Color: 1268
Size: 250860 Color: 32

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 394392 Color: 1565
Size: 331356 Color: 1145
Size: 274253 Color: 509

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 403845 Color: 1607
Size: 323295 Color: 1084
Size: 272861 Color: 482

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 378002 Color: 1479
Size: 371961 Color: 1445
Size: 250038 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 395332 Color: 1570
Size: 303271 Color: 875
Size: 301398 Color: 858

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 384298 Color: 1513
Size: 360639 Color: 1377
Size: 255064 Color: 144

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 407507 Color: 1635
Size: 306555 Color: 909
Size: 285939 Color: 678

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 352504 Color: 1325
Size: 349265 Color: 1296
Size: 298232 Color: 821

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 352139 Color: 1320
Size: 351666 Color: 1318
Size: 296196 Color: 801

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 353467 Color: 1335
Size: 351636 Color: 1317
Size: 294898 Color: 781

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 377875 Color: 1478
Size: 351220 Color: 1313
Size: 270906 Color: 437

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 386271 Color: 1523
Size: 354127 Color: 1339
Size: 259603 Color: 230

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 363403 Color: 1393
Size: 353072 Color: 1331
Size: 283526 Color: 653

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 372401 Color: 1447
Size: 360409 Color: 1376
Size: 267191 Color: 366

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 395325 Color: 1569
Size: 327598 Color: 1114
Size: 277078 Color: 565

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 350180 Color: 1301
Size: 346556 Color: 1266
Size: 303265 Color: 874

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 413152 Color: 1664
Size: 334359 Color: 1166
Size: 252490 Color: 85

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 404310 Color: 1612
Size: 332300 Color: 1151
Size: 263391 Color: 301

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 345936 Color: 1260
Size: 339760 Color: 1202
Size: 314305 Color: 976

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 385997 Color: 1520
Size: 352744 Color: 1327
Size: 261260 Color: 260

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 348154 Color: 1283
Size: 338477 Color: 1195
Size: 313370 Color: 970

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 405224 Color: 1619
Size: 306567 Color: 910
Size: 288210 Color: 710

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 390603 Color: 1546
Size: 326796 Color: 1103
Size: 282602 Color: 638

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 396274 Color: 1574
Size: 346669 Color: 1267
Size: 257058 Color: 175

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 396343 Color: 1575
Size: 320628 Color: 1046
Size: 283030 Color: 642

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 361042 Color: 1380
Size: 358937 Color: 1370
Size: 280022 Color: 599

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 372835 Color: 1451
Size: 343679 Color: 1236
Size: 283487 Color: 651

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 391846 Color: 1551
Size: 340721 Color: 1212
Size: 267434 Color: 374

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 389620 Color: 1538
Size: 335273 Color: 1171
Size: 275108 Color: 524

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 345688 Color: 1257
Size: 342205 Color: 1226
Size: 312108 Color: 963

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 380387 Color: 1489
Size: 367465 Color: 1415
Size: 252149 Color: 74

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 375115 Color: 1463
Size: 338599 Color: 1196
Size: 286287 Color: 682

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 413750 Color: 1667
Size: 330912 Color: 1142
Size: 255339 Color: 150

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 414563 Color: 1668
Size: 312851 Color: 965
Size: 272587 Color: 475

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 414593 Color: 1669
Size: 311505 Color: 958
Size: 273903 Color: 502

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 414654 Color: 1670
Size: 298813 Color: 826
Size: 286534 Color: 684

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 414810 Color: 1671
Size: 318859 Color: 1027
Size: 266332 Color: 353

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 414931 Color: 1672
Size: 301960 Color: 862
Size: 283110 Color: 645

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 415121 Color: 1674
Size: 316497 Color: 1004
Size: 268383 Color: 389

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 415657 Color: 1676
Size: 312741 Color: 964
Size: 271603 Color: 456

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 415734 Color: 1677
Size: 323044 Color: 1080
Size: 261223 Color: 258

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 415974 Color: 1678
Size: 319738 Color: 1039
Size: 264289 Color: 312

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 416300 Color: 1680
Size: 311460 Color: 957
Size: 272241 Color: 468

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 416757 Color: 1682
Size: 302630 Color: 870
Size: 280614 Color: 607

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 416826 Color: 1683
Size: 294743 Color: 777
Size: 288432 Color: 713

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 416882 Color: 1684
Size: 330471 Color: 1139
Size: 252648 Color: 90

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 417539 Color: 1685
Size: 294966 Color: 783
Size: 287496 Color: 701

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 417592 Color: 1686
Size: 300673 Color: 851
Size: 281736 Color: 623

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 417815 Color: 1687
Size: 319995 Color: 1041
Size: 262191 Color: 276

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 417998 Color: 1689
Size: 326328 Color: 1099
Size: 255675 Color: 153

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 418048 Color: 1690
Size: 316373 Color: 1000
Size: 265580 Color: 339

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 418189 Color: 1691
Size: 315659 Color: 995
Size: 266153 Color: 348

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 418384 Color: 1692
Size: 316405 Color: 1002
Size: 265212 Color: 330

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 418400 Color: 1693
Size: 294982 Color: 785
Size: 286619 Color: 688

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 418759 Color: 1694
Size: 310041 Color: 940
Size: 271201 Color: 442

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 418960 Color: 1695
Size: 311310 Color: 954
Size: 269731 Color: 412

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 419244 Color: 1696
Size: 313495 Color: 971
Size: 267262 Color: 369

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 419479 Color: 1697
Size: 325378 Color: 1094
Size: 255144 Color: 146

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 419493 Color: 1698
Size: 309754 Color: 937
Size: 270754 Color: 433

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 419842 Color: 1699
Size: 299783 Color: 840
Size: 280376 Color: 601

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 420040 Color: 1700
Size: 310559 Color: 946
Size: 269402 Color: 401

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 420344 Color: 1702
Size: 315002 Color: 985
Size: 264655 Color: 318

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 420610 Color: 1703
Size: 314108 Color: 975
Size: 265283 Color: 333

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 420698 Color: 1704
Size: 295691 Color: 793
Size: 283612 Color: 655

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 420727 Color: 1705
Size: 291518 Color: 748
Size: 287756 Color: 703

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 421443 Color: 1706
Size: 304068 Color: 883
Size: 274490 Color: 514

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 421454 Color: 1707
Size: 304475 Color: 890
Size: 274072 Color: 506

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 421457 Color: 1708
Size: 315191 Color: 988
Size: 263353 Color: 300

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 421590 Color: 1709
Size: 309781 Color: 938
Size: 268630 Color: 392

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 422182 Color: 1711
Size: 311433 Color: 956
Size: 266386 Color: 355

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 422184 Color: 1712
Size: 314772 Color: 982
Size: 263045 Color: 290

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 422366 Color: 1713
Size: 304236 Color: 885
Size: 273399 Color: 496

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 422403 Color: 1714
Size: 316448 Color: 1003
Size: 261150 Color: 256

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 423064 Color: 1715
Size: 299325 Color: 836
Size: 277612 Color: 571

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 423138 Color: 1716
Size: 309594 Color: 935
Size: 267269 Color: 370

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 423520 Color: 1718
Size: 306948 Color: 914
Size: 269533 Color: 405

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 423752 Color: 1719
Size: 311322 Color: 955
Size: 264927 Color: 323

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 424127 Color: 1722
Size: 319009 Color: 1030
Size: 256865 Color: 171

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 424294 Color: 1723
Size: 288655 Color: 716
Size: 287052 Color: 697

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 424399 Color: 1724
Size: 318449 Color: 1019
Size: 257153 Color: 178

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 424506 Color: 1725
Size: 301695 Color: 860
Size: 273800 Color: 501

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 424940 Color: 1726
Size: 322255 Color: 1065
Size: 252806 Color: 93

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 425328 Color: 1727
Size: 288056 Color: 705
Size: 286617 Color: 687

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 425411 Color: 1728
Size: 299136 Color: 834
Size: 275454 Color: 534

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 425708 Color: 1729
Size: 316325 Color: 999
Size: 257968 Color: 194

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 425733 Color: 1730
Size: 312044 Color: 962
Size: 262224 Color: 277

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 425998 Color: 1732
Size: 291085 Color: 744
Size: 282918 Color: 640

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 426080 Color: 1733
Size: 294089 Color: 774
Size: 279832 Color: 597

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 426330 Color: 1735
Size: 303010 Color: 873
Size: 270661 Color: 432

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 426359 Color: 1736
Size: 315554 Color: 991
Size: 258088 Color: 198

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 426549 Color: 1737
Size: 321077 Color: 1053
Size: 252375 Color: 80

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 426818 Color: 1738
Size: 322505 Color: 1069
Size: 250678 Color: 28

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 426829 Color: 1739
Size: 291927 Color: 752
Size: 281245 Color: 616

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 427693 Color: 1740
Size: 296405 Color: 803
Size: 275903 Color: 543

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 427804 Color: 1741
Size: 287207 Color: 699
Size: 284990 Color: 666

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 427984 Color: 1742
Size: 320513 Color: 1044
Size: 251504 Color: 59

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 428114 Color: 1743
Size: 302236 Color: 865
Size: 269651 Color: 409

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 428299 Color: 1744
Size: 303350 Color: 876
Size: 268352 Color: 388

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 428485 Color: 1745
Size: 295761 Color: 796
Size: 275755 Color: 541

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 428587 Color: 1746
Size: 292935 Color: 761
Size: 278479 Color: 582

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 428601 Color: 1747
Size: 286856 Color: 695
Size: 284544 Color: 663

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 428636 Color: 1748
Size: 310234 Color: 942
Size: 261131 Color: 255

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 429036 Color: 1749
Size: 309265 Color: 932
Size: 261700 Color: 269

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 429448 Color: 1750
Size: 299590 Color: 839
Size: 270963 Color: 438

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 429677 Color: 1751
Size: 301402 Color: 859
Size: 268922 Color: 398

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 429814 Color: 1752
Size: 293525 Color: 768
Size: 276662 Color: 554

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 429873 Color: 1753
Size: 286616 Color: 686
Size: 283512 Color: 652

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 429952 Color: 1754
Size: 307606 Color: 923
Size: 262443 Color: 282

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 430510 Color: 1755
Size: 295414 Color: 789
Size: 274077 Color: 507

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 431023 Color: 1757
Size: 307754 Color: 925
Size: 261224 Color: 259

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 431073 Color: 1758
Size: 318779 Color: 1024
Size: 250149 Color: 10

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 431336 Color: 1759
Size: 293023 Color: 764
Size: 275642 Color: 537

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 431398 Color: 1760
Size: 293253 Color: 766
Size: 275350 Color: 531

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 431402 Color: 1761
Size: 310931 Color: 949
Size: 257668 Color: 186

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 432092 Color: 1762
Size: 294802 Color: 779
Size: 273107 Color: 487

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 432362 Color: 1763
Size: 306596 Color: 911
Size: 261043 Color: 251

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 432423 Color: 1764
Size: 289132 Color: 723
Size: 278446 Color: 580

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 432456 Color: 1765
Size: 285085 Color: 668
Size: 282460 Color: 635

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 432499 Color: 1766
Size: 288093 Color: 708
Size: 279409 Color: 590

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 432823 Color: 1768
Size: 308602 Color: 929
Size: 258576 Color: 209

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 433176 Color: 1769
Size: 300826 Color: 853
Size: 265999 Color: 347

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 433642 Color: 1770
Size: 299120 Color: 833
Size: 267239 Color: 368

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 433656 Color: 1771
Size: 292398 Color: 756
Size: 273947 Color: 503

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 433770 Color: 1772
Size: 285718 Color: 676
Size: 280513 Color: 606

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 434378 Color: 1773
Size: 312940 Color: 966
Size: 252683 Color: 91

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 434484 Color: 1774
Size: 303402 Color: 877
Size: 262115 Color: 274

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 435028 Color: 1776
Size: 294974 Color: 784
Size: 269999 Color: 420

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 435075 Color: 1777
Size: 303802 Color: 880
Size: 261124 Color: 254

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 435795 Color: 1779
Size: 298974 Color: 829
Size: 265232 Color: 331

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 435849 Color: 1780
Size: 298366 Color: 822
Size: 265786 Color: 343

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 438162 Color: 1781
Size: 290423 Color: 735
Size: 271416 Color: 450

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 438521 Color: 1782
Size: 282221 Color: 628
Size: 279259 Color: 589

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 438605 Color: 1783
Size: 308524 Color: 928
Size: 252872 Color: 99

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 438924 Color: 1784
Size: 296714 Color: 810
Size: 264363 Color: 313

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 439187 Color: 1785
Size: 287876 Color: 704
Size: 272938 Color: 483

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 439380 Color: 1786
Size: 287106 Color: 698
Size: 273515 Color: 498

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 439468 Color: 1787
Size: 307025 Color: 916
Size: 253508 Color: 115

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 439604 Color: 1788
Size: 310352 Color: 945
Size: 250045 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 439702 Color: 1789
Size: 309750 Color: 936
Size: 250549 Color: 24

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 440026 Color: 1791
Size: 295264 Color: 788
Size: 264711 Color: 320

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 440794 Color: 1792
Size: 286647 Color: 689
Size: 272560 Color: 474

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 441439 Color: 1795
Size: 288579 Color: 715
Size: 269983 Color: 419

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 441489 Color: 1796
Size: 299145 Color: 835
Size: 259367 Color: 224

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 441589 Color: 1797
Size: 283389 Color: 648
Size: 275023 Color: 521

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 441611 Color: 1798
Size: 306398 Color: 905
Size: 251992 Color: 70

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 441742 Color: 1799
Size: 296462 Color: 805
Size: 261797 Color: 271

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 442512 Color: 1800
Size: 305748 Color: 902
Size: 251741 Color: 65

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 442626 Color: 1801
Size: 299533 Color: 838
Size: 257842 Color: 190

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 443182 Color: 1802
Size: 296691 Color: 809
Size: 260128 Color: 235

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 443418 Color: 1803
Size: 280138 Color: 600
Size: 276445 Color: 550

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 443449 Color: 1804
Size: 305112 Color: 897
Size: 251440 Color: 56

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 443505 Color: 1805
Size: 299799 Color: 841
Size: 256697 Color: 166

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 443723 Color: 1806
Size: 285624 Color: 675
Size: 270654 Color: 431

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 444066 Color: 1807
Size: 284941 Color: 665
Size: 270994 Color: 439

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 444177 Color: 1808
Size: 293134 Color: 765
Size: 262690 Color: 286

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 444187 Color: 1809
Size: 280700 Color: 610
Size: 275114 Color: 525

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 444573 Color: 1810
Size: 290427 Color: 736
Size: 265001 Color: 324

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 445018 Color: 1811
Size: 300221 Color: 848
Size: 254762 Color: 140

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 445020 Color: 1812
Size: 282207 Color: 627
Size: 272774 Color: 479

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 445056 Color: 1813
Size: 286290 Color: 683
Size: 268655 Color: 394

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 445202 Color: 1814
Size: 279630 Color: 594
Size: 275169 Color: 527

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 445276 Color: 1815
Size: 282270 Color: 630
Size: 272455 Color: 472

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 445289 Color: 1816
Size: 296834 Color: 811
Size: 257878 Color: 191

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 445657 Color: 1817
Size: 288476 Color: 714
Size: 265868 Color: 345

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 445996 Color: 1818
Size: 292457 Color: 759
Size: 261548 Color: 267

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 447155 Color: 1819
Size: 302460 Color: 867
Size: 250386 Color: 18

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 447325 Color: 1821
Size: 286805 Color: 692
Size: 265871 Color: 346

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 448239 Color: 1822
Size: 283083 Color: 644
Size: 268679 Color: 395

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 448249 Color: 1823
Size: 284042 Color: 659
Size: 267710 Color: 377

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 448498 Color: 1824
Size: 299818 Color: 842
Size: 251685 Color: 62

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 448710 Color: 1825
Size: 297684 Color: 816
Size: 253607 Color: 118

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 448805 Color: 1826
Size: 299869 Color: 845
Size: 251327 Color: 51

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 449317 Color: 1827
Size: 284171 Color: 661
Size: 266513 Color: 357

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 449747 Color: 1828
Size: 278707 Color: 584
Size: 271547 Color: 453

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 449870 Color: 1829
Size: 295700 Color: 794
Size: 254431 Color: 134

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 450003 Color: 1830
Size: 281272 Color: 617
Size: 268726 Color: 396

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 450555 Color: 1831
Size: 278416 Color: 579
Size: 271030 Color: 440

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 450788 Color: 1832
Size: 297171 Color: 814
Size: 252042 Color: 71

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 1833
Size: 282942 Color: 641
Size: 266253 Color: 351

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 451802 Color: 1835
Size: 293722 Color: 769
Size: 254477 Color: 136

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 452051 Color: 1836
Size: 274833 Color: 517
Size: 273117 Color: 489

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 452469 Color: 1837
Size: 283373 Color: 647
Size: 264159 Color: 309

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 452887 Color: 1838
Size: 280388 Color: 602
Size: 266726 Color: 359

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 453023 Color: 1839
Size: 278467 Color: 581
Size: 268511 Color: 390

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 453084 Color: 1840
Size: 290097 Color: 732
Size: 256820 Color: 169

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 453600 Color: 1841
Size: 293930 Color: 771
Size: 252471 Color: 84

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 453668 Color: 1842
Size: 276726 Color: 558
Size: 269607 Color: 407

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 453684 Color: 1843
Size: 293916 Color: 770
Size: 252401 Color: 81

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 453742 Color: 1844
Size: 278166 Color: 577
Size: 268093 Color: 384

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 454649 Color: 1845
Size: 274250 Color: 508
Size: 271102 Color: 441

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 454896 Color: 1846
Size: 275396 Color: 532
Size: 269709 Color: 411

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 455279 Color: 1848
Size: 286607 Color: 685
Size: 258115 Color: 199

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 455467 Color: 1850
Size: 272399 Color: 471
Size: 272135 Color: 464

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 456068 Color: 1851
Size: 283393 Color: 649
Size: 260540 Color: 245

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 456134 Color: 1852
Size: 281405 Color: 618
Size: 262462 Color: 283

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 456406 Color: 1853
Size: 277352 Color: 568
Size: 266243 Color: 350

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 457274 Color: 1855
Size: 288266 Color: 711
Size: 254461 Color: 135

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 457710 Color: 1856
Size: 272330 Color: 470
Size: 269961 Color: 417

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 457936 Color: 1857
Size: 277591 Color: 570
Size: 264474 Color: 315

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 458145 Color: 1858
Size: 290687 Color: 743
Size: 251169 Color: 45

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 458260 Color: 1859
Size: 276663 Color: 555
Size: 265078 Color: 327

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 458395 Color: 1860
Size: 277016 Color: 562
Size: 264590 Color: 316

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 458541 Color: 1861
Size: 287733 Color: 702
Size: 253727 Color: 122

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 458612 Color: 1862
Size: 285270 Color: 673
Size: 256119 Color: 159

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 458793 Color: 1863
Size: 290543 Color: 739
Size: 250665 Color: 27

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 459107 Color: 1864
Size: 290564 Color: 740
Size: 250330 Color: 16

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 459204 Color: 1865
Size: 280410 Color: 605
Size: 260387 Color: 238

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 459816 Color: 1866
Size: 288978 Color: 720
Size: 251207 Color: 47

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 459819 Color: 1867
Size: 281795 Color: 624
Size: 258387 Color: 204

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 459990 Color: 1868
Size: 289610 Color: 729
Size: 250401 Color: 20

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 460000 Color: 1869
Size: 270095 Color: 422
Size: 269906 Color: 415

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 460573 Color: 1870
Size: 280627 Color: 609
Size: 258801 Color: 215

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 460740 Color: 1871
Size: 269847 Color: 413
Size: 269414 Color: 402

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 461124 Color: 1873
Size: 288737 Color: 717
Size: 250140 Color: 8

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 461237 Color: 1874
Size: 277338 Color: 567
Size: 261426 Color: 264

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 461316 Color: 1875
Size: 275515 Color: 535
Size: 263170 Color: 296

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 461617 Color: 1876
Size: 283609 Color: 654
Size: 254775 Color: 141

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 461687 Color: 1877
Size: 270119 Color: 424
Size: 268195 Color: 386

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 461852 Color: 1878
Size: 283745 Color: 657
Size: 254404 Color: 132

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 461992 Color: 1879
Size: 272207 Color: 467
Size: 265802 Color: 344

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 462030 Color: 1880
Size: 286878 Color: 696
Size: 251093 Color: 42

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 462078 Color: 1881
Size: 286842 Color: 693
Size: 251081 Color: 40

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 462216 Color: 1882
Size: 270419 Color: 429
Size: 267366 Color: 373

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 462559 Color: 1884
Size: 275668 Color: 538
Size: 261774 Color: 270

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 462677 Color: 1885
Size: 282565 Color: 637
Size: 254759 Color: 139

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 462805 Color: 1886
Size: 272136 Color: 465
Size: 265060 Color: 326

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 463337 Color: 1887
Size: 276987 Color: 560
Size: 259677 Color: 231

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 463526 Color: 1888
Size: 276955 Color: 559
Size: 259520 Color: 227

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 463905 Color: 1889
Size: 280397 Color: 603
Size: 255699 Color: 155

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 464467 Color: 1890
Size: 270381 Color: 427
Size: 265153 Color: 329

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 464477 Color: 1891
Size: 276121 Color: 547
Size: 259403 Color: 225

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 464863 Color: 1892
Size: 272087 Color: 463
Size: 263051 Color: 291

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 464916 Color: 1893
Size: 276172 Color: 548
Size: 258913 Color: 217

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 465326 Color: 1894
Size: 276674 Color: 557
Size: 258001 Color: 196

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 465512 Color: 1895
Size: 271462 Color: 451
Size: 263027 Color: 289

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 465834 Color: 1896
Size: 281563 Color: 619
Size: 252604 Color: 88

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 465965 Color: 1897
Size: 281177 Color: 615
Size: 252859 Color: 96

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 466365 Color: 1898
Size: 269519 Color: 404
Size: 264117 Color: 308

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 466406 Color: 1899
Size: 274420 Color: 513
Size: 259175 Color: 220

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 466526 Color: 1900
Size: 280619 Color: 608
Size: 252856 Color: 95

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 466604 Color: 1901
Size: 279546 Color: 593
Size: 253851 Color: 125

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 466768 Color: 1902
Size: 268996 Color: 399
Size: 264237 Color: 311

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 467260 Color: 1903
Size: 278092 Color: 576
Size: 254649 Color: 138

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 467633 Color: 1904
Size: 275142 Color: 526
Size: 257226 Color: 180

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 467885 Color: 1905
Size: 275290 Color: 529
Size: 256826 Color: 170

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 467967 Color: 1906
Size: 276669 Color: 556
Size: 255365 Color: 151

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 469272 Color: 1907
Size: 272836 Color: 481
Size: 257893 Color: 192

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 469529 Color: 1908
Size: 269897 Color: 414
Size: 260575 Color: 248

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 469960 Color: 1909
Size: 273018 Color: 485
Size: 257023 Color: 173

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 470214 Color: 1910
Size: 267176 Color: 364
Size: 262611 Color: 284

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 470563 Color: 1911
Size: 268032 Color: 383
Size: 261406 Color: 263

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 470680 Color: 1912
Size: 270878 Color: 434
Size: 258443 Color: 205

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 471353 Color: 1913
Size: 275928 Color: 544
Size: 252720 Color: 92

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 471821 Color: 1914
Size: 275311 Color: 530
Size: 252869 Color: 98

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 472362 Color: 1915
Size: 274258 Color: 510
Size: 253381 Color: 113

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 472837 Color: 1916
Size: 273989 Color: 504
Size: 253175 Color: 105

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 473360 Color: 1917
Size: 263507 Color: 302
Size: 263134 Color: 294

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 473770 Color: 1918
Size: 275277 Color: 528
Size: 250954 Color: 36

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 473892 Color: 1919
Size: 268170 Color: 385
Size: 257939 Color: 193

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 473939 Color: 1920
Size: 264767 Color: 322
Size: 261295 Color: 261

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 474081 Color: 1921
Size: 273275 Color: 492
Size: 252645 Color: 89

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 474185 Color: 1922
Size: 267060 Color: 362
Size: 258756 Color: 214

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 474200 Color: 1923
Size: 267807 Color: 379
Size: 257994 Color: 195

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 474289 Color: 1924
Size: 271294 Color: 444
Size: 254418 Color: 133

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 474432 Color: 1925
Size: 271302 Color: 445
Size: 254267 Color: 128

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 474778 Color: 1926
Size: 270431 Color: 430
Size: 254792 Color: 142

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 474866 Color: 1927
Size: 274896 Color: 518
Size: 250239 Color: 12

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 475514 Color: 1928
Size: 273072 Color: 486
Size: 251415 Color: 55

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 475808 Color: 1929
Size: 271305 Color: 446
Size: 252888 Color: 101

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 475923 Color: 1930
Size: 271609 Color: 457
Size: 252469 Color: 83

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 475978 Color: 1931
Size: 262423 Color: 281
Size: 261600 Color: 268

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 476435 Color: 1932
Size: 270036 Color: 421
Size: 253530 Color: 117

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 476697 Color: 1933
Size: 273115 Color: 488
Size: 250189 Color: 11

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 476907 Color: 1934
Size: 268802 Color: 397
Size: 254292 Color: 129

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 476943 Color: 1935
Size: 270194 Color: 426
Size: 252864 Color: 97

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 477101 Color: 1936
Size: 262656 Color: 285
Size: 260244 Color: 237

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 477104 Color: 1937
Size: 264696 Color: 319
Size: 258201 Color: 202

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 477668 Color: 1938
Size: 266642 Color: 358
Size: 255691 Color: 154

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 477736 Color: 1939
Size: 270104 Color: 423
Size: 252161 Color: 75

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 477809 Color: 1940
Size: 271899 Color: 460
Size: 250293 Color: 14

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 478410 Color: 1942
Size: 263095 Color: 292
Size: 258496 Color: 206

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 479020 Color: 1944
Size: 269625 Color: 408
Size: 251356 Color: 53

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 479176 Color: 1945
Size: 265567 Color: 338
Size: 255258 Color: 148

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 479418 Color: 1946
Size: 263844 Color: 306
Size: 256739 Color: 167

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 479429 Color: 1947
Size: 267481 Color: 376
Size: 253091 Color: 104

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 479684 Color: 1948
Size: 263122 Color: 293
Size: 257195 Color: 179

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 479775 Color: 1949
Size: 268635 Color: 393
Size: 251591 Color: 61

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 480217 Color: 1950
Size: 263592 Color: 304
Size: 256192 Color: 160

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 480470 Color: 1951
Size: 265507 Color: 337
Size: 254024 Color: 127

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 480870 Color: 1952
Size: 260484 Color: 244
Size: 258647 Color: 211

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 482024 Color: 1954
Size: 259243 Color: 222
Size: 258734 Color: 212

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 482047 Color: 1955
Size: 260429 Color: 240
Size: 257525 Color: 185

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 482291 Color: 1956
Size: 266264 Color: 352
Size: 251446 Color: 57

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 482533 Color: 1957
Size: 260153 Color: 236
Size: 257315 Color: 182

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 482548 Color: 1958
Size: 265274 Color: 332
Size: 252179 Color: 76

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 482650 Color: 1959
Size: 263830 Color: 305
Size: 253521 Color: 116

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 482987 Color: 1960
Size: 259564 Color: 229
Size: 257450 Color: 184

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 483101 Color: 1961
Size: 260819 Color: 250
Size: 256081 Color: 158

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 483241 Color: 1962
Size: 265604 Color: 340
Size: 251156 Color: 43

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 483730 Color: 1963
Size: 263212 Color: 297
Size: 253059 Color: 103

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 484765 Color: 1964
Size: 262926 Color: 288
Size: 252310 Color: 79

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 484769 Color: 1965
Size: 258155 Color: 201
Size: 257077 Color: 176

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 485240 Color: 1966
Size: 263247 Color: 299
Size: 251514 Color: 60

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 485619 Color: 1967
Size: 261497 Color: 265
Size: 252885 Color: 100

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 485708 Color: 1968
Size: 258501 Color: 207
Size: 255792 Color: 156

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 486185 Color: 1969
Size: 260564 Color: 247
Size: 253252 Color: 110

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 487000 Color: 1970
Size: 261927 Color: 272
Size: 251074 Color: 39

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 487326 Color: 1971
Size: 262341 Color: 278
Size: 250334 Color: 17

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 487667 Color: 1972
Size: 258957 Color: 218
Size: 253377 Color: 112

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 487728 Color: 1973
Size: 260027 Color: 234
Size: 252246 Color: 77

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 488268 Color: 1974
Size: 260430 Color: 241
Size: 251303 Color: 50

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 488942 Color: 1975
Size: 256746 Color: 168
Size: 254313 Color: 130

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 489627 Color: 1976
Size: 256664 Color: 165
Size: 253710 Color: 121

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 489988 Color: 1977
Size: 259904 Color: 233
Size: 250109 Color: 6

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 491210 Color: 1978
Size: 254999 Color: 143
Size: 253792 Color: 123

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 491520 Color: 1979
Size: 256505 Color: 163
Size: 251976 Color: 67

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 493457 Color: 1980
Size: 255329 Color: 149
Size: 251215 Color: 48

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 493743 Color: 1981
Size: 253306 Color: 111
Size: 252952 Color: 102

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 494411 Color: 1982
Size: 254398 Color: 131
Size: 251192 Color: 46

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 494539 Color: 1983
Size: 253214 Color: 109
Size: 252248 Color: 78

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 494563 Color: 1984
Size: 255142 Color: 145
Size: 250296 Color: 15

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 494711 Color: 1985
Size: 254526 Color: 137
Size: 250764 Color: 29

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 495433 Color: 1986
Size: 252577 Color: 87
Size: 251991 Color: 69

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 495675 Color: 1987
Size: 253865 Color: 126
Size: 250461 Color: 21

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 495809 Color: 1988
Size: 252825 Color: 94
Size: 251367 Color: 54

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 496079 Color: 1989
Size: 252090 Color: 73
Size: 251832 Color: 66

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 496260 Color: 1990
Size: 253201 Color: 108
Size: 250540 Color: 22

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 497673 Color: 1991
Size: 251699 Color: 63
Size: 250629 Color: 26

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 497746 Color: 1992
Size: 251708 Color: 64
Size: 250547 Color: 23

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 497794 Color: 1993
Size: 252082 Color: 72
Size: 250125 Color: 7

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 498523 Color: 1994
Size: 251082 Color: 41
Size: 250396 Color: 19

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 498753 Color: 1995
Size: 251165 Color: 44
Size: 250083 Color: 3

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 498838 Color: 1996
Size: 250873 Color: 34
Size: 250290 Color: 13

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 499071 Color: 1997
Size: 250850 Color: 31
Size: 250080 Color: 2

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 499336 Color: 1998
Size: 250560 Color: 25
Size: 250105 Color: 5

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 415223 Color: 1675
Size: 311582 Color: 960
Size: 273195 Color: 491

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 416536 Color: 1681
Size: 325210 Color: 1092
Size: 258254 Color: 203

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 417870 Color: 1688
Size: 330866 Color: 1141
Size: 251264 Color: 49

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 420241 Color: 1701
Size: 291689 Color: 750
Size: 288070 Color: 707

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 421995 Color: 1710
Size: 299977 Color: 846
Size: 278028 Color: 574

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 423820 Color: 1721
Size: 311101 Color: 952
Size: 265079 Color: 328

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 425743 Color: 1731
Size: 323288 Color: 1083
Size: 250969 Color: 37

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 432734 Color: 1767
Size: 299507 Color: 837
Size: 267759 Color: 378

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 435298 Color: 1778
Size: 295100 Color: 787
Size: 269602 Color: 406

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 439890 Color: 1790
Size: 292638 Color: 760
Size: 267472 Color: 375

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 440846 Color: 1793
Size: 309007 Color: 930
Size: 250147 Color: 9

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 454963 Color: 1847
Size: 281894 Color: 625
Size: 263143 Color: 295

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 478991 Color: 1943
Size: 269674 Color: 410
Size: 251335 Color: 52

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 481679 Color: 1953
Size: 261063 Color: 252
Size: 257258 Color: 181

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 405395 Color: 1622
Size: 343732 Color: 1237
Size: 250873 Color: 33

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 342426 Color: 1228
Size: 340748 Color: 1213
Size: 316826 Color: 1010

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 375921 Color: 1467
Size: 351544 Color: 1316
Size: 272535 Color: 473

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 404429 Color: 1613
Size: 319095 Color: 1035
Size: 276476 Color: 552

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 366716 Color: 1411
Size: 361624 Color: 1383
Size: 271660 Color: 459

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 400066 Color: 1595
Size: 333536 Color: 1158
Size: 266398 Color: 356

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 392596 Color: 1557
Size: 323288 Color: 1082
Size: 284116 Color: 660

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 407771 Color: 1636
Size: 324311 Color: 1088
Size: 267918 Color: 382

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 383773 Color: 1512
Size: 357260 Color: 1357
Size: 258967 Color: 219

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 408788 Color: 1645
Size: 329702 Color: 1134
Size: 261510 Color: 266

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 392388 Color: 1554
Size: 326627 Color: 1102
Size: 280985 Color: 613

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 375803 Color: 1466
Size: 318964 Color: 1028
Size: 305233 Color: 898

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 353969 Color: 1338
Size: 348074 Color: 1282
Size: 297957 Color: 818

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 405116 Color: 1617
Size: 341704 Color: 1221
Size: 253180 Color: 106

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 372871 Color: 1452
Size: 322102 Color: 1062
Size: 305027 Color: 896

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 395481 Color: 1571
Size: 319297 Color: 1036
Size: 285222 Color: 672

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 400794 Color: 1598
Size: 329731 Color: 1135
Size: 269475 Color: 403

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 352446 Color: 1324
Size: 347699 Color: 1280
Size: 299855 Color: 843

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 386783 Color: 1527
Size: 361229 Color: 1382
Size: 251988 Color: 68

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 412949 Color: 1661
Size: 325673 Color: 1096
Size: 261378 Color: 262

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 409508 Color: 1649
Size: 332353 Color: 1152
Size: 258139 Color: 200

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 358860 Color: 1369
Size: 358839 Color: 1367
Size: 282301 Color: 631

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 383759 Color: 1511
Size: 342875 Color: 1230
Size: 273366 Color: 494

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 392498 Color: 1556
Size: 322787 Color: 1074
Size: 284715 Color: 664

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 354459 Color: 1343
Size: 353901 Color: 1336
Size: 291640 Color: 749

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 392743 Color: 1560
Size: 321886 Color: 1058
Size: 285371 Color: 674

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 385524 Color: 1517
Size: 329284 Color: 1130
Size: 285192 Color: 671

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 394660 Color: 1568
Size: 337102 Color: 1181
Size: 268238 Color: 387

Bin 510: 1 of cap free
Amount of items: 3
Items: 
Size: 382875 Color: 1507
Size: 343088 Color: 1232
Size: 274037 Color: 505

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 365770 Color: 1404
Size: 329366 Color: 1131
Size: 304864 Color: 894

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 381660 Color: 1502
Size: 322627 Color: 1071
Size: 295713 Color: 795

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 368808 Color: 1421
Size: 340965 Color: 1215
Size: 290227 Color: 733

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 385538 Color: 1518
Size: 350489 Color: 1304
Size: 263973 Color: 307

Bin 515: 2 of cap free
Amount of items: 3
Items: 
Size: 451204 Color: 1834
Size: 275971 Color: 545
Size: 272824 Color: 480

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 408695 Color: 1644
Size: 323447 Color: 1085
Size: 267857 Color: 380

Bin 517: 2 of cap free
Amount of items: 3
Items: 
Size: 403355 Color: 1606
Size: 338907 Color: 1200
Size: 257737 Color: 188

Bin 518: 2 of cap free
Amount of items: 3
Items: 
Size: 376365 Color: 1469
Size: 348642 Color: 1291
Size: 274992 Color: 519

Bin 519: 2 of cap free
Amount of items: 3
Items: 
Size: 394641 Color: 1567
Size: 343227 Color: 1234
Size: 262131 Color: 275

Bin 520: 2 of cap free
Amount of items: 3
Items: 
Size: 372636 Color: 1448
Size: 352334 Color: 1322
Size: 275029 Color: 523

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 368024 Color: 1418
Size: 327021 Color: 1108
Size: 304954 Color: 895

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 397976 Color: 1582
Size: 348188 Color: 1284
Size: 253835 Color: 124

Bin 523: 2 of cap free
Amount of items: 3
Items: 
Size: 396387 Color: 1576
Size: 322511 Color: 1070
Size: 281101 Color: 614

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 381910 Color: 1504
Size: 338618 Color: 1197
Size: 279471 Color: 592

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 366651 Color: 1410
Size: 361041 Color: 1379
Size: 272307 Color: 469

Bin 526: 2 of cap free
Amount of items: 3
Items: 
Size: 369445 Color: 1427
Size: 319017 Color: 1031
Size: 311537 Color: 959

Bin 527: 2 of cap free
Amount of items: 3
Items: 
Size: 365016 Color: 1399
Size: 318607 Color: 1021
Size: 316376 Color: 1001

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 366807 Color: 1412
Size: 346989 Color: 1273
Size: 286203 Color: 679

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 373686 Color: 1456
Size: 343985 Color: 1240
Size: 282328 Color: 632

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 348531 Color: 1288
Size: 346091 Color: 1262
Size: 305377 Color: 899

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 380711 Color: 1491
Size: 314595 Color: 981
Size: 304693 Color: 893

Bin 532: 3 of cap free
Amount of items: 3
Items: 
Size: 365879 Color: 1405
Size: 341694 Color: 1220
Size: 292425 Color: 757

Bin 533: 3 of cap free
Amount of items: 3
Items: 
Size: 407863 Color: 1637
Size: 314951 Color: 984
Size: 277184 Color: 566

Bin 534: 3 of cap free
Amount of items: 3
Items: 
Size: 404204 Color: 1609
Size: 302529 Color: 869
Size: 293265 Color: 767

Bin 535: 3 of cap free
Amount of items: 3
Items: 
Size: 391749 Color: 1550
Size: 341319 Color: 1216
Size: 266930 Color: 361

Bin 536: 3 of cap free
Amount of items: 3
Items: 
Size: 374461 Color: 1461
Size: 367528 Color: 1416
Size: 258009 Color: 197

Bin 537: 3 of cap free
Amount of items: 3
Items: 
Size: 364390 Color: 1395
Size: 318124 Color: 1017
Size: 317484 Color: 1016

Bin 538: 3 of cap free
Amount of items: 3
Items: 
Size: 398069 Color: 1584
Size: 306438 Color: 906
Size: 295491 Color: 790

Bin 539: 3 of cap free
Amount of items: 3
Items: 
Size: 390412 Color: 1544
Size: 311035 Color: 951
Size: 298551 Color: 823

Bin 540: 3 of cap free
Amount of items: 3
Items: 
Size: 389913 Color: 1541
Size: 324935 Color: 1089
Size: 285150 Color: 669

Bin 541: 3 of cap free
Amount of items: 3
Items: 
Size: 371958 Color: 1444
Size: 347205 Color: 1276
Size: 280835 Color: 612

Bin 542: 3 of cap free
Amount of items: 3
Items: 
Size: 388340 Color: 1532
Size: 309574 Color: 934
Size: 302084 Color: 864

Bin 543: 3 of cap free
Amount of items: 3
Items: 
Size: 377781 Color: 1477
Size: 365169 Color: 1401
Size: 257048 Color: 174

Bin 544: 3 of cap free
Amount of items: 3
Items: 
Size: 393573 Color: 1562
Size: 333015 Color: 1156
Size: 273410 Color: 497

Bin 545: 3 of cap free
Amount of items: 3
Items: 
Size: 365893 Color: 1406
Size: 332735 Color: 1155
Size: 301370 Color: 857

Bin 546: 3 of cap free
Amount of items: 3
Items: 
Size: 406837 Color: 1631
Size: 299082 Color: 831
Size: 294079 Color: 773

Bin 547: 3 of cap free
Amount of items: 3
Items: 
Size: 352891 Color: 1330
Size: 352604 Color: 1326
Size: 294503 Color: 775

Bin 548: 3 of cap free
Amount of items: 3
Items: 
Size: 362921 Color: 1391
Size: 320383 Color: 1043
Size: 316694 Color: 1007

Bin 549: 3 of cap free
Amount of items: 3
Items: 
Size: 355331 Color: 1348
Size: 337538 Color: 1186
Size: 307129 Color: 917

Bin 550: 3 of cap free
Amount of items: 3
Items: 
Size: 358947 Color: 1371
Size: 336376 Color: 1177
Size: 304675 Color: 892

Bin 551: 3 of cap free
Amount of items: 3
Items: 
Size: 411039 Color: 1653
Size: 298988 Color: 830
Size: 289971 Color: 731

Bin 552: 4 of cap free
Amount of items: 3
Items: 
Size: 423172 Color: 1717
Size: 320929 Color: 1051
Size: 255896 Color: 157

Bin 553: 4 of cap free
Amount of items: 3
Items: 
Size: 381255 Color: 1498
Size: 314416 Color: 978
Size: 304326 Color: 887

Bin 554: 4 of cap free
Amount of items: 3
Items: 
Size: 404267 Color: 1611
Size: 298853 Color: 827
Size: 296877 Color: 812

Bin 555: 4 of cap free
Amount of items: 3
Items: 
Size: 388214 Color: 1531
Size: 346466 Color: 1265
Size: 265317 Color: 334

Bin 556: 4 of cap free
Amount of items: 3
Items: 
Size: 409602 Color: 1650
Size: 318847 Color: 1026
Size: 271548 Color: 454

Bin 557: 4 of cap free
Amount of items: 3
Items: 
Size: 390044 Color: 1542
Size: 346436 Color: 1264
Size: 263517 Color: 303

Bin 558: 4 of cap free
Amount of items: 3
Items: 
Size: 371811 Color: 1442
Size: 320876 Color: 1049
Size: 307310 Color: 920

Bin 559: 4 of cap free
Amount of items: 3
Items: 
Size: 386044 Color: 1521
Size: 316942 Color: 1012
Size: 297011 Color: 813

Bin 560: 4 of cap free
Amount of items: 3
Items: 
Size: 345003 Color: 1249
Size: 330957 Color: 1143
Size: 324037 Color: 1087

Bin 561: 4 of cap free
Amount of items: 3
Items: 
Size: 407020 Color: 1633
Size: 310739 Color: 948
Size: 282238 Color: 629

Bin 562: 4 of cap free
Amount of items: 3
Items: 
Size: 350678 Color: 1308
Size: 350678 Color: 1307
Size: 298641 Color: 824

Bin 563: 4 of cap free
Amount of items: 3
Items: 
Size: 347336 Color: 1278
Size: 346770 Color: 1269
Size: 305891 Color: 904

Bin 564: 4 of cap free
Amount of items: 3
Items: 
Size: 348258 Color: 1285
Size: 329742 Color: 1136
Size: 321997 Color: 1061

Bin 565: 4 of cap free
Amount of items: 3
Items: 
Size: 343810 Color: 1239
Size: 334219 Color: 1164
Size: 321968 Color: 1059

Bin 566: 4 of cap free
Amount of items: 3
Items: 
Size: 356417 Color: 1354
Size: 337106 Color: 1182
Size: 306474 Color: 908

Bin 567: 5 of cap free
Amount of items: 3
Items: 
Size: 412527 Color: 1658
Size: 298203 Color: 820
Size: 289266 Color: 724

Bin 568: 5 of cap free
Amount of items: 3
Items: 
Size: 376374 Color: 1470
Size: 336769 Color: 1180
Size: 286853 Color: 694

Bin 569: 5 of cap free
Amount of items: 3
Items: 
Size: 412952 Color: 1662
Size: 294031 Color: 772
Size: 293013 Color: 762

Bin 570: 5 of cap free
Amount of items: 3
Items: 
Size: 405392 Color: 1621
Size: 299864 Color: 844
Size: 294740 Color: 776

Bin 571: 5 of cap free
Amount of items: 3
Items: 
Size: 348596 Color: 1289
Size: 344144 Color: 1241
Size: 307256 Color: 919

Bin 572: 5 of cap free
Amount of items: 3
Items: 
Size: 397617 Color: 1579
Size: 345488 Color: 1255
Size: 256891 Color: 172

Bin 573: 5 of cap free
Amount of items: 3
Items: 
Size: 399895 Color: 1593
Size: 303592 Color: 878
Size: 296509 Color: 808

Bin 574: 5 of cap free
Amount of items: 3
Items: 
Size: 363080 Color: 1392
Size: 345502 Color: 1256
Size: 291414 Color: 747

Bin 575: 5 of cap free
Amount of items: 3
Items: 
Size: 358455 Color: 1362
Size: 345405 Color: 1254
Size: 296136 Color: 799

Bin 576: 5 of cap free
Amount of items: 3
Items: 
Size: 347325 Color: 1277
Size: 347184 Color: 1275
Size: 305487 Color: 900

Bin 577: 6 of cap free
Amount of items: 3
Items: 
Size: 407078 Color: 1634
Size: 307740 Color: 924
Size: 285177 Color: 670

Bin 578: 6 of cap free
Amount of items: 3
Items: 
Size: 404860 Color: 1616
Size: 298654 Color: 825
Size: 296481 Color: 807

Bin 579: 6 of cap free
Amount of items: 3
Items: 
Size: 380470 Color: 1490
Size: 329161 Color: 1128
Size: 290364 Color: 734

Bin 580: 6 of cap free
Amount of items: 3
Items: 
Size: 381092 Color: 1495
Size: 321045 Color: 1052
Size: 297858 Color: 817

Bin 581: 6 of cap free
Amount of items: 3
Items: 
Size: 346138 Color: 1263
Size: 346035 Color: 1261
Size: 307822 Color: 926

Bin 582: 6 of cap free
Amount of items: 3
Items: 
Size: 358130 Color: 1360
Size: 327533 Color: 1112
Size: 314332 Color: 977

Bin 583: 6 of cap free
Amount of items: 3
Items: 
Size: 355413 Color: 1349
Size: 328353 Color: 1121
Size: 316229 Color: 997

Bin 584: 6 of cap free
Amount of items: 3
Items: 
Size: 350358 Color: 1303
Size: 336630 Color: 1179
Size: 313007 Color: 967

Bin 585: 7 of cap free
Amount of items: 3
Items: 
Size: 402589 Color: 1605
Size: 307526 Color: 922
Size: 289879 Color: 730

Bin 586: 7 of cap free
Amount of items: 3
Items: 
Size: 359727 Color: 1375
Size: 323726 Color: 1086
Size: 316541 Color: 1005

Bin 587: 7 of cap free
Amount of items: 3
Items: 
Size: 411196 Color: 1654
Size: 300737 Color: 852
Size: 288061 Color: 706

Bin 588: 8 of cap free
Amount of items: 3
Items: 
Size: 353380 Color: 1334
Size: 331317 Color: 1144
Size: 315296 Color: 989

Bin 589: 8 of cap free
Amount of items: 3
Items: 
Size: 377117 Color: 1472
Size: 349499 Color: 1297
Size: 273377 Color: 495

Bin 590: 8 of cap free
Amount of items: 3
Items: 
Size: 346956 Color: 1271
Size: 333095 Color: 1157
Size: 319942 Color: 1040

Bin 591: 8 of cap free
Amount of items: 3
Items: 
Size: 398584 Color: 1587
Size: 310205 Color: 941
Size: 291204 Color: 745

Bin 592: 8 of cap free
Amount of items: 3
Items: 
Size: 371754 Color: 1440
Size: 317111 Color: 1013
Size: 311128 Color: 953

Bin 593: 8 of cap free
Amount of items: 3
Items: 
Size: 352787 Color: 1328
Size: 327672 Color: 1115
Size: 319534 Color: 1037

Bin 594: 8 of cap free
Amount of items: 3
Items: 
Size: 358859 Color: 1368
Size: 331661 Color: 1148
Size: 309473 Color: 933

Bin 595: 9 of cap free
Amount of items: 3
Items: 
Size: 409389 Color: 1647
Size: 339815 Color: 1204
Size: 250788 Color: 30

Bin 596: 9 of cap free
Amount of items: 3
Items: 
Size: 349856 Color: 1299
Size: 349747 Color: 1298
Size: 300389 Color: 849

Bin 597: 10 of cap free
Amount of items: 3
Items: 
Size: 380807 Color: 1492
Size: 344184 Color: 1242
Size: 275000 Color: 520

Bin 598: 10 of cap free
Amount of items: 3
Items: 
Size: 369885 Color: 1428
Size: 337656 Color: 1188
Size: 292450 Color: 758

Bin 599: 10 of cap free
Amount of items: 3
Items: 
Size: 348416 Color: 1287
Size: 338315 Color: 1193
Size: 313260 Color: 969

Bin 600: 10 of cap free
Amount of items: 3
Items: 
Size: 398892 Color: 1588
Size: 329070 Color: 1127
Size: 272029 Color: 461

Bin 601: 10 of cap free
Amount of items: 3
Items: 
Size: 352879 Color: 1329
Size: 351444 Color: 1315
Size: 295668 Color: 792

Bin 602: 10 of cap free
Amount of items: 3
Items: 
Size: 337355 Color: 1184
Size: 334689 Color: 1169
Size: 327947 Color: 1119

Bin 603: 11 of cap free
Amount of items: 3
Items: 
Size: 402588 Color: 1604
Size: 326498 Color: 1100
Size: 270904 Color: 436

Bin 604: 11 of cap free
Amount of items: 3
Items: 
Size: 405605 Color: 1623
Size: 315626 Color: 994
Size: 278759 Color: 585

Bin 605: 11 of cap free
Amount of items: 3
Items: 
Size: 371043 Color: 1437
Size: 322333 Color: 1067
Size: 306614 Color: 912

Bin 606: 12 of cap free
Amount of items: 3
Items: 
Size: 355696 Color: 1350
Size: 343437 Color: 1235
Size: 300856 Color: 854

Bin 607: 13 of cap free
Amount of items: 3
Items: 
Size: 373140 Color: 1454
Size: 331783 Color: 1149
Size: 295065 Color: 786

Bin 608: 13 of cap free
Amount of items: 3
Items: 
Size: 348300 Color: 1286
Size: 338162 Color: 1192
Size: 313526 Color: 972

Bin 609: 13 of cap free
Amount of items: 3
Items: 
Size: 406434 Color: 1630
Size: 322243 Color: 1064
Size: 271311 Color: 447

Bin 610: 13 of cap free
Amount of items: 3
Items: 
Size: 350932 Color: 1311
Size: 326845 Color: 1105
Size: 322211 Color: 1063

Bin 611: 14 of cap free
Amount of items: 3
Items: 
Size: 348799 Color: 1292
Size: 334526 Color: 1167
Size: 316662 Color: 1006

Bin 612: 14 of cap free
Amount of items: 3
Items: 
Size: 410479 Color: 1652
Size: 306467 Color: 907
Size: 283041 Color: 643

Bin 613: 14 of cap free
Amount of items: 3
Items: 
Size: 357758 Color: 1358
Size: 323263 Color: 1081
Size: 318966 Color: 1029

Bin 614: 14 of cap free
Amount of items: 3
Items: 
Size: 349076 Color: 1294
Size: 327966 Color: 1120
Size: 322945 Color: 1076

Bin 615: 15 of cap free
Amount of items: 3
Items: 
Size: 356414 Color: 1353
Size: 328788 Color: 1126
Size: 314784 Color: 983

Bin 616: 15 of cap free
Amount of items: 3
Items: 
Size: 379202 Color: 1481
Size: 316788 Color: 1009
Size: 303996 Color: 881

Bin 617: 15 of cap free
Amount of items: 3
Items: 
Size: 392195 Color: 1552
Size: 354385 Color: 1342
Size: 253406 Color: 114

Bin 618: 16 of cap free
Amount of items: 3
Items: 
Size: 410098 Color: 1651
Size: 329256 Color: 1129
Size: 260631 Color: 249

Bin 619: 17 of cap free
Amount of items: 3
Items: 
Size: 393545 Color: 1561
Size: 341676 Color: 1218
Size: 264763 Color: 321

Bin 620: 17 of cap free
Amount of items: 3
Items: 
Size: 362709 Color: 1388
Size: 326302 Color: 1098
Size: 310973 Color: 950

Bin 621: 18 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 1602
Size: 302957 Color: 872
Size: 294919 Color: 782

Bin 622: 18 of cap free
Amount of items: 3
Items: 
Size: 387123 Color: 1529
Size: 353961 Color: 1337
Size: 258899 Color: 216

Bin 623: 18 of cap free
Amount of items: 3
Items: 
Size: 346987 Color: 1272
Size: 345832 Color: 1259
Size: 307164 Color: 918

Bin 624: 19 of cap free
Amount of items: 3
Items: 
Size: 358455 Color: 1363
Size: 337397 Color: 1185
Size: 304130 Color: 884

Bin 625: 23 of cap free
Amount of items: 3
Items: 
Size: 350821 Color: 1310
Size: 334598 Color: 1168
Size: 314559 Color: 980

Bin 626: 24 of cap free
Amount of items: 3
Items: 
Size: 386128 Color: 1522
Size: 318264 Color: 1018
Size: 295585 Color: 791

Bin 627: 24 of cap free
Amount of items: 3
Items: 
Size: 408010 Color: 1638
Size: 320596 Color: 1045
Size: 271371 Color: 448

Bin 628: 24 of cap free
Amount of items: 3
Items: 
Size: 351015 Color: 1312
Size: 345168 Color: 1250
Size: 303794 Color: 879

Bin 629: 24 of cap free
Amount of items: 3
Items: 
Size: 359368 Color: 1374
Size: 340173 Color: 1206
Size: 300436 Color: 850

Bin 630: 25 of cap free
Amount of items: 3
Items: 
Size: 379776 Color: 1487
Size: 338621 Color: 1198
Size: 281579 Color: 620

Bin 631: 27 of cap free
Amount of items: 3
Items: 
Size: 404820 Color: 1615
Size: 321826 Color: 1057
Size: 273328 Color: 493

Bin 632: 28 of cap free
Amount of items: 3
Items: 
Size: 383527 Color: 1510
Size: 327140 Color: 1109
Size: 289306 Color: 725

Bin 633: 28 of cap free
Amount of items: 3
Items: 
Size: 414973 Color: 1673
Size: 310664 Color: 947
Size: 274336 Color: 512

Bin 634: 28 of cap free
Amount of items: 3
Items: 
Size: 373710 Color: 1457
Size: 321721 Color: 1056
Size: 304542 Color: 891

Bin 635: 28 of cap free
Amount of items: 3
Items: 
Size: 413476 Color: 1666
Size: 296014 Color: 798
Size: 290483 Color: 738

Bin 636: 30 of cap free
Amount of items: 3
Items: 
Size: 413441 Color: 1665
Size: 297474 Color: 815
Size: 289056 Color: 721

Bin 637: 34 of cap free
Amount of items: 3
Items: 
Size: 369195 Color: 1424
Size: 315622 Color: 992
Size: 315150 Color: 987

Bin 638: 38 of cap free
Amount of items: 3
Items: 
Size: 354529 Color: 1344
Size: 330335 Color: 1137
Size: 315099 Color: 986

Bin 639: 38 of cap free
Amount of items: 3
Items: 
Size: 441367 Color: 1794
Size: 282172 Color: 626
Size: 276424 Color: 549

Bin 640: 39 of cap free
Amount of items: 3
Items: 
Size: 384510 Color: 1515
Size: 349260 Color: 1295
Size: 266192 Color: 349

Bin 641: 40 of cap free
Amount of items: 3
Items: 
Size: 375627 Color: 1464
Size: 314090 Color: 974
Size: 310244 Color: 943

Bin 642: 41 of cap free
Amount of items: 3
Items: 
Size: 371165 Color: 1438
Size: 321995 Color: 1060
Size: 306800 Color: 913

Bin 643: 50 of cap free
Amount of items: 3
Items: 
Size: 369962 Color: 1429
Size: 327505 Color: 1111
Size: 302484 Color: 868

Bin 644: 51 of cap free
Amount of items: 3
Items: 
Size: 362808 Color: 1389
Size: 358817 Color: 1366
Size: 278325 Color: 578

Bin 645: 57 of cap free
Amount of items: 3
Items: 
Size: 354929 Color: 1346
Size: 340718 Color: 1211
Size: 304297 Color: 886

Bin 646: 57 of cap free
Amount of items: 3
Items: 
Size: 343772 Color: 1238
Size: 337127 Color: 1183
Size: 319045 Color: 1032

Bin 647: 75 of cap free
Amount of items: 3
Items: 
Size: 338335 Color: 1194
Size: 333833 Color: 1162
Size: 327758 Color: 1116

Bin 648: 82 of cap free
Amount of items: 3
Items: 
Size: 359315 Color: 1373
Size: 334901 Color: 1170
Size: 305703 Color: 901

Bin 649: 91 of cap free
Amount of items: 3
Items: 
Size: 376524 Color: 1471
Size: 315921 Color: 996
Size: 307465 Color: 921

Bin 650: 91 of cap free
Amount of items: 3
Items: 
Size: 340382 Color: 1208
Size: 336531 Color: 1178
Size: 322997 Color: 1079

Bin 651: 148 of cap free
Amount of items: 3
Items: 
Size: 347013 Color: 1274
Size: 335478 Color: 1172
Size: 317362 Color: 1015

Bin 652: 156 of cap free
Amount of items: 3
Items: 
Size: 350341 Color: 1302
Size: 326811 Color: 1104
Size: 322693 Color: 1072

Bin 653: 182 of cap free
Amount of items: 3
Items: 
Size: 391671 Color: 1549
Size: 316253 Color: 998
Size: 291895 Color: 751

Bin 654: 206 of cap free
Amount of items: 3
Items: 
Size: 370241 Color: 1432
Size: 319729 Color: 1038
Size: 309825 Color: 939

Bin 655: 207 of cap free
Amount of items: 3
Items: 
Size: 404259 Color: 1610
Size: 299087 Color: 832
Size: 296448 Color: 804

Bin 656: 211 of cap free
Amount of items: 3
Items: 
Size: 398009 Color: 1583
Size: 301759 Color: 861
Size: 300022 Color: 847

Bin 657: 250 of cap free
Amount of items: 3
Items: 
Size: 356153 Color: 1352
Size: 345398 Color: 1253
Size: 298200 Color: 819

Bin 658: 315 of cap free
Amount of items: 3
Items: 
Size: 416171 Color: 1679
Size: 292250 Color: 754
Size: 291265 Color: 746

Bin 659: 393 of cap free
Amount of items: 3
Items: 
Size: 423810 Color: 1720
Size: 289534 Color: 726
Size: 286264 Color: 681

Bin 660: 430 of cap free
Amount of items: 2
Items: 
Size: 499918 Color: 2000
Size: 499653 Color: 1999

Bin 661: 504 of cap free
Amount of items: 3
Items: 
Size: 371793 Color: 1441
Size: 340965 Color: 1214
Size: 286739 Color: 691

Bin 662: 606 of cap free
Amount of items: 3
Items: 
Size: 380239 Color: 1488
Size: 322980 Color: 1078
Size: 296176 Color: 800

Bin 663: 681 of cap free
Amount of items: 3
Items: 
Size: 366875 Color: 1413
Size: 325496 Color: 1095
Size: 306949 Color: 915

Bin 664: 1699 of cap free
Amount of items: 3
Items: 
Size: 362594 Color: 1387
Size: 320086 Color: 1042
Size: 315622 Color: 993

Bin 665: 2083 of cap free
Amount of items: 3
Items: 
Size: 350049 Color: 1300
Size: 337564 Color: 1187
Size: 310305 Color: 944

Bin 666: 84938 of cap free
Amount of items: 3
Items: 
Size: 308170 Color: 927
Size: 304446 Color: 889
Size: 302447 Color: 866

Bin 667: 168361 of cap free
Amount of items: 3
Items: 
Size: 290671 Color: 741
Size: 273744 Color: 500
Size: 267225 Color: 367

Bin 668: 736759 of cap free
Amount of items: 1
Items: 
Size: 263242 Color: 298

Total size: 667000667
Total free space: 1000001

