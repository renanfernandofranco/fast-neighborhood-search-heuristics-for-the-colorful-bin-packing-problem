Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 395022 Color: 1
Size: 306076 Color: 0
Size: 298903 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 422071 Color: 1
Size: 319712 Color: 1
Size: 258218 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 482533 Color: 1
Size: 265633 Color: 1
Size: 251835 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 471522 Color: 1
Size: 265929 Color: 1
Size: 262550 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 403964 Color: 1
Size: 315112 Color: 1
Size: 280925 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 435029 Color: 1
Size: 314258 Color: 1
Size: 250714 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 491819 Color: 1
Size: 254681 Color: 1
Size: 253501 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 434803 Color: 1
Size: 294298 Color: 1
Size: 270900 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 446247 Color: 1
Size: 296985 Color: 1
Size: 256769 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 367864 Color: 1
Size: 335159 Color: 1
Size: 296978 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 342595 Color: 1
Size: 334969 Color: 1
Size: 322437 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 445285 Color: 1
Size: 295841 Color: 1
Size: 258875 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 486940 Color: 1
Size: 261605 Color: 1
Size: 251456 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 495419 Color: 1
Size: 252876 Color: 1
Size: 251706 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 443941 Color: 1
Size: 294589 Color: 1
Size: 261471 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 441111 Color: 1
Size: 300986 Color: 1
Size: 257904 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 494248 Color: 1
Size: 255100 Color: 1
Size: 250653 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375960 Color: 1
Size: 338118 Color: 1
Size: 285923 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 485027 Color: 1
Size: 263926 Color: 1
Size: 251048 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 491114 Color: 1
Size: 254719 Color: 1
Size: 254168 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 369393 Color: 1
Size: 315448 Color: 0
Size: 315160 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 367448 Color: 1
Size: 321602 Color: 1
Size: 310951 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 482106 Color: 1
Size: 265271 Color: 1
Size: 252624 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 356215 Color: 1
Size: 343398 Color: 1
Size: 300388 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 435029 Color: 1
Size: 289671 Color: 1
Size: 275301 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 455182 Color: 1
Size: 282324 Color: 1
Size: 262495 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 406658 Color: 1
Size: 320090 Color: 1
Size: 273253 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 430436 Color: 1
Size: 284978 Color: 1
Size: 284587 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 453770 Color: 1
Size: 278360 Color: 1
Size: 267871 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 366867 Color: 1
Size: 346936 Color: 1
Size: 286198 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 496166 Color: 1
Size: 253722 Color: 1
Size: 250113 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 413513 Color: 1
Size: 331616 Color: 1
Size: 254872 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 493192 Color: 1
Size: 255621 Color: 1
Size: 251188 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 498415 Color: 1
Size: 251391 Color: 1
Size: 250195 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 436202 Color: 1
Size: 285148 Color: 0
Size: 278651 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 472072 Color: 1
Size: 273053 Color: 1
Size: 254876 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 386906 Color: 1
Size: 360572 Color: 1
Size: 252523 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 446547 Color: 1
Size: 279706 Color: 1
Size: 273748 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 465751 Color: 1
Size: 274853 Color: 1
Size: 259397 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 436548 Color: 1
Size: 306473 Color: 1
Size: 256980 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 496719 Color: 1
Size: 252044 Color: 1
Size: 251238 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 479024 Color: 1
Size: 269112 Color: 1
Size: 251865 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 363399 Color: 1
Size: 349839 Color: 1
Size: 286763 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 368695 Color: 1
Size: 354843 Color: 1
Size: 276463 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 454590 Color: 1
Size: 273592 Color: 1
Size: 271819 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 478696 Color: 1
Size: 267926 Color: 1
Size: 253379 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 489682 Color: 1
Size: 257177 Color: 1
Size: 253142 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 365493 Color: 1
Size: 342716 Color: 1
Size: 291792 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 470987 Color: 1
Size: 266524 Color: 1
Size: 262490 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 385413 Color: 1
Size: 346836 Color: 1
Size: 267752 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 488948 Color: 1
Size: 260190 Color: 1
Size: 250863 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 478090 Color: 1
Size: 261430 Color: 1
Size: 260481 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 388039 Color: 1
Size: 349951 Color: 1
Size: 262011 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 482255 Color: 1
Size: 259170 Color: 1
Size: 258576 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 498426 Color: 1
Size: 250799 Color: 1
Size: 250776 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 436056 Color: 1
Size: 292134 Color: 1
Size: 271811 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 495644 Color: 1
Size: 252595 Color: 1
Size: 251762 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 454240 Color: 1
Size: 295643 Color: 1
Size: 250118 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 446237 Color: 1
Size: 289679 Color: 1
Size: 264085 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 416283 Color: 1
Size: 323091 Color: 1
Size: 260627 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 498374 Color: 1
Size: 251270 Color: 1
Size: 250357 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 424633 Color: 1
Size: 319943 Color: 1
Size: 255425 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 471613 Color: 1
Size: 267208 Color: 1
Size: 261180 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 466685 Color: 1
Size: 277726 Color: 1
Size: 255590 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 469060 Color: 1
Size: 271883 Color: 1
Size: 259058 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 482272 Color: 1
Size: 259642 Color: 1
Size: 258087 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 357901 Color: 1
Size: 349554 Color: 1
Size: 292546 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 404957 Color: 1
Size: 305173 Color: 1
Size: 289871 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 410314 Color: 1
Size: 307775 Color: 1
Size: 281912 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 424636 Color: 1
Size: 301203 Color: 1
Size: 274162 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 476945 Color: 1
Size: 265746 Color: 1
Size: 257310 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 384593 Color: 1
Size: 364999 Color: 1
Size: 250409 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 344245 Color: 1
Size: 329913 Color: 1
Size: 325843 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 395342 Color: 1
Size: 319966 Color: 1
Size: 284693 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 457751 Color: 1
Size: 275857 Color: 1
Size: 266393 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 442407 Color: 1
Size: 289441 Color: 1
Size: 268153 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 463820 Color: 1
Size: 284381 Color: 1
Size: 251800 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 485680 Color: 1
Size: 260832 Color: 1
Size: 253489 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 485387 Color: 1
Size: 262217 Color: 1
Size: 252397 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 364884 Color: 1
Size: 334823 Color: 1
Size: 300294 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 381318 Color: 1
Size: 348797 Color: 1
Size: 269886 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 373977 Color: 1
Size: 347546 Color: 1
Size: 278478 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 414066 Color: 1
Size: 331537 Color: 1
Size: 254398 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 389042 Color: 1
Size: 317420 Color: 1
Size: 293539 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 494989 Color: 1
Size: 253520 Color: 1
Size: 251492 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 381568 Color: 1
Size: 313015 Color: 1
Size: 305418 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 472756 Color: 1
Size: 270155 Color: 1
Size: 257090 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 474509 Color: 1
Size: 272565 Color: 1
Size: 252927 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 447336 Color: 1
Size: 301110 Color: 1
Size: 251555 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 434336 Color: 1
Size: 308296 Color: 1
Size: 257369 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 481108 Color: 1
Size: 260749 Color: 1
Size: 258144 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 377441 Color: 1
Size: 370758 Color: 1
Size: 251802 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 391724 Color: 1
Size: 340252 Color: 1
Size: 268025 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 484124 Color: 1
Size: 265729 Color: 1
Size: 250148 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 472853 Color: 1
Size: 270809 Color: 1
Size: 256339 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 403868 Color: 1
Size: 312812 Color: 1
Size: 283321 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 461245 Color: 1
Size: 278542 Color: 1
Size: 260214 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 401953 Color: 1
Size: 322306 Color: 1
Size: 275742 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 472820 Color: 1
Size: 274220 Color: 1
Size: 252961 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 436012 Color: 1
Size: 288486 Color: 1
Size: 275503 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 410733 Color: 1
Size: 317072 Color: 1
Size: 272196 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 351577 Color: 1
Size: 336861 Color: 1
Size: 311563 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 495013 Color: 1
Size: 252541 Color: 1
Size: 252447 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 388065 Color: 1
Size: 323328 Color: 1
Size: 288608 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 407960 Color: 1
Size: 339573 Color: 1
Size: 252468 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 490276 Color: 1
Size: 258014 Color: 1
Size: 251711 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 440150 Color: 1
Size: 287898 Color: 1
Size: 271953 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 377610 Color: 1
Size: 357179 Color: 1
Size: 265212 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 397167 Color: 1
Size: 344421 Color: 1
Size: 258413 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 377146 Color: 1
Size: 341545 Color: 1
Size: 281310 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 361463 Color: 1
Size: 354495 Color: 1
Size: 284043 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 468249 Color: 1
Size: 273024 Color: 1
Size: 258728 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 435353 Color: 1
Size: 314160 Color: 1
Size: 250488 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 489917 Color: 1
Size: 259904 Color: 1
Size: 250180 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 453963 Color: 1
Size: 289758 Color: 1
Size: 256280 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 482099 Color: 1
Size: 265485 Color: 1
Size: 252417 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 465605 Color: 1
Size: 280369 Color: 1
Size: 254027 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 453927 Color: 1
Size: 278462 Color: 1
Size: 267612 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 377224 Color: 1
Size: 329837 Color: 1
Size: 292940 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 487558 Color: 1
Size: 256307 Color: 1
Size: 256136 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 461322 Color: 1
Size: 279794 Color: 1
Size: 258885 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 358385 Color: 1
Size: 347676 Color: 1
Size: 293940 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 454063 Color: 1
Size: 275295 Color: 1
Size: 270643 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 389335 Color: 1
Size: 333482 Color: 1
Size: 277184 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 440469 Color: 1
Size: 302974 Color: 1
Size: 256558 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 467184 Color: 1
Size: 280021 Color: 1
Size: 252796 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 474359 Color: 1
Size: 265079 Color: 1
Size: 260563 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 371532 Color: 1
Size: 334178 Color: 1
Size: 294291 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 497513 Color: 1
Size: 252344 Color: 1
Size: 250144 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 374225 Color: 1
Size: 358611 Color: 1
Size: 267165 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 411202 Color: 1
Size: 318404 Color: 1
Size: 270395 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 455618 Color: 1
Size: 275414 Color: 1
Size: 268969 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 420335 Color: 1
Size: 304373 Color: 1
Size: 275293 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 384842 Color: 1
Size: 337301 Color: 1
Size: 277858 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 404278 Color: 1
Size: 308791 Color: 1
Size: 286932 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 478408 Color: 1
Size: 267157 Color: 1
Size: 254436 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 401809 Color: 1
Size: 304619 Color: 1
Size: 293573 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 446788 Color: 1
Size: 292362 Color: 1
Size: 260851 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 419435 Color: 1
Size: 322806 Color: 1
Size: 257760 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 360881 Color: 1
Size: 341345 Color: 1
Size: 297775 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 473975 Color: 1
Size: 264384 Color: 1
Size: 261642 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 376886 Color: 1
Size: 350990 Color: 1
Size: 272125 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 457257 Color: 1
Size: 277511 Color: 1
Size: 265233 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 492608 Color: 1
Size: 256722 Color: 1
Size: 250671 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 422700 Color: 1
Size: 316543 Color: 1
Size: 260758 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 384551 Color: 1
Size: 323695 Color: 1
Size: 291755 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 400776 Color: 1
Size: 313293 Color: 1
Size: 285932 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 373536 Color: 1
Size: 340490 Color: 1
Size: 285975 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 371273 Color: 1
Size: 359348 Color: 1
Size: 269380 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 357126 Color: 1
Size: 347305 Color: 1
Size: 295570 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 429667 Color: 1
Size: 307168 Color: 1
Size: 263166 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 434285 Color: 1
Size: 302169 Color: 1
Size: 263547 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 360650 Color: 1
Size: 346325 Color: 1
Size: 293026 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 497304 Color: 1
Size: 251428 Color: 1
Size: 251269 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 493337 Color: 1
Size: 254986 Color: 1
Size: 251678 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 424792 Color: 1
Size: 305021 Color: 1
Size: 270188 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 440507 Color: 1
Size: 301427 Color: 1
Size: 258067 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 425964 Color: 1
Size: 298850 Color: 0
Size: 275187 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 486642 Color: 1
Size: 257168 Color: 1
Size: 256191 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 363009 Color: 1
Size: 318811 Color: 0
Size: 318181 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 394838 Color: 1
Size: 334860 Color: 1
Size: 270303 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 349864 Color: 1
Size: 346480 Color: 1
Size: 303657 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 441047 Color: 1
Size: 302254 Color: 1
Size: 256700 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 448919 Color: 1
Size: 298277 Color: 1
Size: 252805 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 397379 Color: 1
Size: 314452 Color: 1
Size: 288170 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 354371 Color: 1
Size: 327047 Color: 1
Size: 318583 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 466125 Color: 1
Size: 273087 Color: 0
Size: 260789 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 472323 Color: 1
Size: 275847 Color: 1
Size: 251831 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 361459 Color: 1
Size: 353683 Color: 1
Size: 284859 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 469345 Color: 1
Size: 279282 Color: 1
Size: 251374 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 377347 Color: 1
Size: 315341 Color: 0
Size: 307313 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 385842 Color: 1
Size: 323454 Color: 1
Size: 290705 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 459535 Color: 1
Size: 286680 Color: 1
Size: 253786 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 418024 Color: 1
Size: 319963 Color: 1
Size: 262014 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 402589 Color: 1
Size: 315440 Color: 1
Size: 281972 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 366393 Color: 1
Size: 354166 Color: 1
Size: 279442 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 403741 Color: 1
Size: 345204 Color: 1
Size: 251056 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 387113 Color: 1
Size: 321793 Color: 1
Size: 291095 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 444249 Color: 1
Size: 303391 Color: 1
Size: 252361 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 350205 Color: 1
Size: 325953 Color: 1
Size: 323843 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 388996 Color: 1
Size: 333871 Color: 1
Size: 277134 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 396007 Color: 1
Size: 316752 Color: 1
Size: 287242 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 493505 Color: 1
Size: 255948 Color: 1
Size: 250548 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 464200 Color: 1
Size: 277812 Color: 1
Size: 257989 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 454389 Color: 1
Size: 284558 Color: 1
Size: 261054 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 467102 Color: 1
Size: 272845 Color: 1
Size: 260054 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 477609 Color: 1
Size: 263000 Color: 0
Size: 259392 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 461497 Color: 1
Size: 277348 Color: 1
Size: 261156 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 430663 Color: 1
Size: 289681 Color: 1
Size: 279657 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 495973 Color: 1
Size: 254024 Color: 1
Size: 250004 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 486579 Color: 1
Size: 261497 Color: 1
Size: 251925 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 483520 Color: 1
Size: 263387 Color: 1
Size: 253094 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 470956 Color: 1
Size: 274259 Color: 1
Size: 254786 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 387839 Color: 1
Size: 310453 Color: 1
Size: 301709 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 391328 Color: 1
Size: 333773 Color: 1
Size: 274900 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 403435 Color: 1
Size: 317657 Color: 1
Size: 278909 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 397262 Color: 1
Size: 329453 Color: 1
Size: 273286 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 372810 Color: 1
Size: 371792 Color: 1
Size: 255399 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 436162 Color: 1
Size: 296115 Color: 1
Size: 267724 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 369383 Color: 1
Size: 318996 Color: 1
Size: 311622 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 410313 Color: 1
Size: 311974 Color: 1
Size: 277714 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 374382 Color: 1
Size: 361717 Color: 1
Size: 263902 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 383445 Color: 1
Size: 325756 Color: 1
Size: 290800 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 363922 Color: 1
Size: 330610 Color: 1
Size: 305469 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 376020 Color: 1
Size: 333803 Color: 1
Size: 290178 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 425372 Color: 1
Size: 302092 Color: 1
Size: 272537 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 444992 Color: 1
Size: 289594 Color: 1
Size: 265415 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 449761 Color: 1
Size: 289291 Color: 1
Size: 260949 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 378618 Color: 1
Size: 318947 Color: 1
Size: 302436 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 364026 Color: 1
Size: 355236 Color: 1
Size: 280739 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 433010 Color: 1
Size: 291224 Color: 1
Size: 275767 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 484132 Color: 1
Size: 259858 Color: 1
Size: 256011 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 367695 Color: 1
Size: 358152 Color: 1
Size: 274154 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 430774 Color: 1
Size: 302697 Color: 1
Size: 266530 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 393277 Color: 1
Size: 328758 Color: 1
Size: 277966 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 469037 Color: 1
Size: 268031 Color: 1
Size: 262933 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 413433 Color: 1
Size: 310971 Color: 1
Size: 275597 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 383562 Color: 1
Size: 340296 Color: 1
Size: 276143 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 462963 Color: 1
Size: 277100 Color: 1
Size: 259938 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 385945 Color: 1
Size: 314857 Color: 1
Size: 299199 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 379910 Color: 1
Size: 312090 Color: 0
Size: 308001 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 382666 Color: 1
Size: 361934 Color: 1
Size: 255401 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 445841 Color: 1
Size: 282343 Color: 1
Size: 271817 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 424785 Color: 1
Size: 287793 Color: 1
Size: 287423 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 485994 Color: 1
Size: 261715 Color: 1
Size: 252292 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 435920 Color: 1
Size: 291920 Color: 1
Size: 272161 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 490364 Color: 1
Size: 258347 Color: 1
Size: 251290 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 411958 Color: 1
Size: 319226 Color: 1
Size: 268817 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 459005 Color: 1
Size: 288727 Color: 1
Size: 252269 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 382812 Color: 1
Size: 319634 Color: 1
Size: 297555 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 357575 Color: 1
Size: 348390 Color: 1
Size: 294036 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 433150 Color: 1
Size: 296558 Color: 1
Size: 270293 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 414339 Color: 1
Size: 302146 Color: 1
Size: 283516 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 482531 Color: 1
Size: 265446 Color: 1
Size: 252024 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 436919 Color: 1
Size: 295659 Color: 1
Size: 267423 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 401005 Color: 1
Size: 335291 Color: 1
Size: 263705 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 362943 Color: 1
Size: 332015 Color: 1
Size: 305043 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 470774 Color: 1
Size: 272208 Color: 1
Size: 257019 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 473717 Color: 1
Size: 272294 Color: 1
Size: 253990 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 399150 Color: 1
Size: 340484 Color: 1
Size: 260367 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 385448 Color: 1
Size: 361000 Color: 1
Size: 253553 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 408602 Color: 1
Size: 302445 Color: 1
Size: 288954 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 387607 Color: 1
Size: 316928 Color: 1
Size: 295466 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 415048 Color: 1
Size: 306498 Color: 1
Size: 278455 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 415246 Color: 1
Size: 313064 Color: 1
Size: 271691 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 410146 Color: 1
Size: 317191 Color: 1
Size: 272664 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 469616 Color: 1
Size: 273606 Color: 1
Size: 256779 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 464096 Color: 1
Size: 283515 Color: 1
Size: 252390 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 454382 Color: 1
Size: 281354 Color: 1
Size: 264265 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 374827 Color: 1
Size: 372386 Color: 1
Size: 252788 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 420046 Color: 1
Size: 321688 Color: 1
Size: 258267 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 380834 Color: 1
Size: 315435 Color: 1
Size: 303732 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 485544 Color: 1
Size: 263677 Color: 1
Size: 250780 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 394197 Color: 1
Size: 320814 Color: 1
Size: 284990 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 369391 Color: 1
Size: 349376 Color: 1
Size: 281234 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 364927 Color: 1
Size: 324992 Color: 1
Size: 310082 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 377214 Color: 1
Size: 333395 Color: 1
Size: 289392 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 388837 Color: 1
Size: 319906 Color: 1
Size: 291258 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 427587 Color: 1
Size: 311920 Color: 1
Size: 260494 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 455381 Color: 1
Size: 285292 Color: 1
Size: 259328 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 400574 Color: 1
Size: 342066 Color: 1
Size: 257361 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 413024 Color: 1
Size: 308205 Color: 1
Size: 278772 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 430337 Color: 1
Size: 290857 Color: 0
Size: 278807 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 392556 Color: 1
Size: 310710 Color: 1
Size: 296735 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 410028 Color: 1
Size: 318983 Color: 1
Size: 270990 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 375878 Color: 1
Size: 325912 Color: 1
Size: 298211 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 409495 Color: 1
Size: 314425 Color: 1
Size: 276081 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 371309 Color: 1
Size: 333022 Color: 1
Size: 295670 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 450606 Color: 1
Size: 277692 Color: 1
Size: 271703 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 461938 Color: 1
Size: 276171 Color: 1
Size: 261892 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 431135 Color: 1
Size: 299028 Color: 1
Size: 269838 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 395407 Color: 1
Size: 325960 Color: 1
Size: 278634 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 353800 Color: 1
Size: 330081 Color: 1
Size: 316120 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 467966 Color: 1
Size: 276204 Color: 1
Size: 255831 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 487628 Color: 1
Size: 260277 Color: 1
Size: 252096 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 374743 Color: 1
Size: 326621 Color: 1
Size: 298637 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 344434 Color: 1
Size: 343167 Color: 1
Size: 312400 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 410119 Color: 1
Size: 323097 Color: 1
Size: 266785 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 402174 Color: 1
Size: 301561 Color: 1
Size: 296266 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 372545 Color: 1
Size: 328881 Color: 1
Size: 298575 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 435599 Color: 1
Size: 308095 Color: 1
Size: 256307 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 341592 Color: 1
Size: 339184 Color: 1
Size: 319225 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 361100 Color: 1
Size: 333903 Color: 1
Size: 304998 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 385955 Color: 1
Size: 318589 Color: 1
Size: 295457 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 394424 Color: 1
Size: 317185 Color: 1
Size: 288392 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 346163 Color: 1
Size: 330010 Color: 1
Size: 323828 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 450377 Color: 1
Size: 275117 Color: 1
Size: 274507 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 392010 Color: 1
Size: 341124 Color: 1
Size: 266867 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 425728 Color: 1
Size: 309452 Color: 1
Size: 264821 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 379092 Color: 1
Size: 322884 Color: 1
Size: 298025 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 373646 Color: 1
Size: 333354 Color: 1
Size: 293001 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 394037 Color: 1
Size: 307565 Color: 0
Size: 298399 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 383983 Color: 1
Size: 348834 Color: 1
Size: 267184 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 429568 Color: 1
Size: 293121 Color: 1
Size: 277312 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 360858 Color: 1
Size: 341748 Color: 1
Size: 297395 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 478914 Color: 1
Size: 267379 Color: 1
Size: 253708 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 363005 Color: 1
Size: 345983 Color: 1
Size: 291013 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 487057 Color: 1
Size: 258923 Color: 1
Size: 254021 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 380586 Color: 1
Size: 360794 Color: 1
Size: 258621 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 481385 Color: 1
Size: 263805 Color: 1
Size: 254811 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 418317 Color: 1
Size: 312500 Color: 1
Size: 269184 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 389476 Color: 1
Size: 306468 Color: 0
Size: 304057 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 393024 Color: 1
Size: 330498 Color: 1
Size: 276479 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 383655 Color: 1
Size: 340795 Color: 1
Size: 275551 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 498861 Color: 1
Size: 250820 Color: 1
Size: 250320 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 384833 Color: 1
Size: 331836 Color: 1
Size: 283332 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 435767 Color: 1
Size: 303593 Color: 1
Size: 260641 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 422569 Color: 1
Size: 292666 Color: 1
Size: 284766 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 416896 Color: 1
Size: 322096 Color: 1
Size: 261009 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 359469 Color: 1
Size: 325636 Color: 1
Size: 314896 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 442848 Color: 1
Size: 303629 Color: 1
Size: 253524 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 378075 Color: 1
Size: 315221 Color: 1
Size: 306705 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 447872 Color: 1
Size: 297185 Color: 1
Size: 254944 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 367281 Color: 1
Size: 324780 Color: 1
Size: 307940 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 428384 Color: 1
Size: 300572 Color: 1
Size: 271045 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 444491 Color: 1
Size: 283803 Color: 1
Size: 271707 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 373704 Color: 1
Size: 315303 Color: 0
Size: 310994 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 396942 Color: 1
Size: 326114 Color: 1
Size: 276945 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 390513 Color: 1
Size: 343416 Color: 1
Size: 266072 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 474740 Color: 1
Size: 272188 Color: 1
Size: 253073 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 385688 Color: 1
Size: 348588 Color: 1
Size: 265725 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 359806 Color: 1
Size: 331444 Color: 1
Size: 308751 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 415699 Color: 1
Size: 320887 Color: 1
Size: 263415 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 383734 Color: 1
Size: 335436 Color: 1
Size: 280831 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 422388 Color: 1
Size: 314898 Color: 1
Size: 262715 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 412321 Color: 1
Size: 316349 Color: 1
Size: 271331 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 383604 Color: 1
Size: 341981 Color: 1
Size: 274416 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 389865 Color: 1
Size: 322097 Color: 1
Size: 288039 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 391966 Color: 1
Size: 345419 Color: 1
Size: 262616 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 374758 Color: 1
Size: 351158 Color: 1
Size: 274085 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 460217 Color: 1
Size: 273075 Color: 1
Size: 266709 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 437828 Color: 1
Size: 285570 Color: 1
Size: 276603 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 350681 Color: 1
Size: 334548 Color: 1
Size: 314772 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 481582 Color: 1
Size: 267061 Color: 1
Size: 251358 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 400345 Color: 1
Size: 321646 Color: 1
Size: 278010 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 372304 Color: 1
Size: 332450 Color: 1
Size: 295247 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 359103 Color: 1
Size: 334218 Color: 1
Size: 306680 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 460762 Color: 1
Size: 275777 Color: 1
Size: 263462 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 486448 Color: 1
Size: 256911 Color: 1
Size: 256642 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 377744 Color: 1
Size: 327879 Color: 1
Size: 294378 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 423818 Color: 1
Size: 308830 Color: 1
Size: 267353 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 465780 Color: 1
Size: 282079 Color: 1
Size: 252142 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 367936 Color: 1
Size: 320773 Color: 1
Size: 311292 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 353408 Color: 1
Size: 350031 Color: 1
Size: 296562 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 398284 Color: 1
Size: 331756 Color: 1
Size: 269961 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 471179 Color: 1
Size: 275490 Color: 1
Size: 253332 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 477708 Color: 1
Size: 270241 Color: 1
Size: 252052 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 477259 Color: 1
Size: 261390 Color: 1
Size: 261352 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 471229 Color: 1
Size: 266331 Color: 1
Size: 262441 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 415711 Color: 1
Size: 297731 Color: 1
Size: 286559 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 422634 Color: 1
Size: 311552 Color: 1
Size: 265815 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 422970 Color: 1
Size: 291333 Color: 0
Size: 285698 Color: 1

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 462530 Color: 1
Size: 279898 Color: 1
Size: 257573 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 339748 Color: 1
Size: 337863 Color: 1
Size: 322390 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 393625 Color: 1
Size: 338939 Color: 1
Size: 267437 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 469685 Color: 1
Size: 274589 Color: 1
Size: 255727 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 373710 Color: 1
Size: 366924 Color: 1
Size: 259367 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 436131 Color: 1
Size: 305967 Color: 1
Size: 257903 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 448142 Color: 1
Size: 299561 Color: 1
Size: 252298 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 393765 Color: 1
Size: 320139 Color: 0
Size: 286097 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 457574 Color: 1
Size: 286467 Color: 1
Size: 255960 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 388292 Color: 1
Size: 323772 Color: 1
Size: 287937 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 399925 Color: 1
Size: 333112 Color: 1
Size: 266964 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 382111 Color: 1
Size: 319331 Color: 0
Size: 298559 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 391766 Color: 1
Size: 325995 Color: 1
Size: 282240 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 370164 Color: 1
Size: 358855 Color: 1
Size: 270982 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 346257 Color: 1
Size: 335743 Color: 1
Size: 318001 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 363906 Color: 1
Size: 321903 Color: 0
Size: 314192 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 419552 Color: 1
Size: 298586 Color: 1
Size: 281863 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 460916 Color: 1
Size: 280897 Color: 1
Size: 258188 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 458335 Color: 1
Size: 277017 Color: 1
Size: 264649 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 457445 Color: 1
Size: 287910 Color: 1
Size: 254646 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 411760 Color: 1
Size: 309415 Color: 1
Size: 278826 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 494816 Color: 1
Size: 253099 Color: 1
Size: 252086 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 410616 Color: 1
Size: 309704 Color: 1
Size: 279681 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 358012 Color: 1
Size: 334513 Color: 1
Size: 307476 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 376873 Color: 1
Size: 317448 Color: 1
Size: 305680 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 480290 Color: 1
Size: 268117 Color: 1
Size: 251594 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 364733 Color: 1
Size: 325648 Color: 1
Size: 309620 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 397082 Color: 1
Size: 320495 Color: 1
Size: 282424 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 381631 Color: 1
Size: 336024 Color: 1
Size: 282346 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 428269 Color: 1
Size: 296192 Color: 1
Size: 275540 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 487178 Color: 1
Size: 262076 Color: 1
Size: 250747 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 481058 Color: 1
Size: 263564 Color: 1
Size: 255379 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 380283 Color: 1
Size: 343425 Color: 1
Size: 276293 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 352370 Color: 1
Size: 326012 Color: 0
Size: 321619 Color: 1

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 374958 Color: 1
Size: 336494 Color: 1
Size: 288549 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 356994 Color: 1
Size: 350353 Color: 1
Size: 292654 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 376835 Color: 1
Size: 335640 Color: 1
Size: 287526 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 425871 Color: 1
Size: 319252 Color: 1
Size: 254878 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 417806 Color: 1
Size: 309783 Color: 1
Size: 272412 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 374132 Color: 1
Size: 319456 Color: 1
Size: 306413 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 407938 Color: 1
Size: 302559 Color: 1
Size: 289504 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 444646 Color: 1
Size: 299207 Color: 1
Size: 256148 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 379569 Color: 1
Size: 356859 Color: 1
Size: 263573 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 396498 Color: 1
Size: 341690 Color: 1
Size: 261813 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 351357 Color: 1
Size: 334078 Color: 1
Size: 314566 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 406733 Color: 1
Size: 300557 Color: 0
Size: 292711 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 378594 Color: 1
Size: 326985 Color: 1
Size: 294422 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 380784 Color: 1
Size: 344074 Color: 1
Size: 275143 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 399287 Color: 1
Size: 313204 Color: 1
Size: 287510 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 434934 Color: 1
Size: 298961 Color: 1
Size: 266106 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 389294 Color: 1
Size: 329961 Color: 1
Size: 280745 Color: 0

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 363414 Color: 1
Size: 353727 Color: 1
Size: 282859 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 418540 Color: 1
Size: 325078 Color: 1
Size: 256382 Color: 0

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 384626 Color: 1
Size: 346159 Color: 1
Size: 269215 Color: 0

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 348492 Color: 1
Size: 340714 Color: 1
Size: 310794 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 350727 Color: 1
Size: 348596 Color: 1
Size: 300677 Color: 0

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 358366 Color: 1
Size: 354213 Color: 1
Size: 287421 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 408112 Color: 1
Size: 314137 Color: 1
Size: 277751 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 383098 Color: 1
Size: 341228 Color: 1
Size: 275674 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 365487 Color: 1
Size: 343303 Color: 1
Size: 291210 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 404484 Color: 1
Size: 322332 Color: 1
Size: 273184 Color: 0

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 382143 Color: 1
Size: 353020 Color: 1
Size: 264837 Color: 0

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 431844 Color: 1
Size: 315795 Color: 1
Size: 252361 Color: 0

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 378705 Color: 1
Size: 337538 Color: 1
Size: 283757 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 387155 Color: 1
Size: 344881 Color: 1
Size: 267964 Color: 0

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 377796 Color: 1
Size: 324136 Color: 1
Size: 298068 Color: 0

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 385628 Color: 1
Size: 357527 Color: 1
Size: 256845 Color: 0

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 399955 Color: 1
Size: 316620 Color: 0
Size: 283425 Color: 1

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 442608 Color: 1
Size: 297255 Color: 1
Size: 260137 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 380384 Color: 1
Size: 356502 Color: 1
Size: 263114 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 380395 Color: 1
Size: 311671 Color: 1
Size: 307934 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 447053 Color: 1
Size: 289641 Color: 1
Size: 263306 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 401109 Color: 1
Size: 304481 Color: 0
Size: 294410 Color: 1

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 430732 Color: 1
Size: 296706 Color: 1
Size: 272562 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 376527 Color: 1
Size: 365914 Color: 1
Size: 257559 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 395681 Color: 1
Size: 329837 Color: 1
Size: 274482 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 360427 Color: 1
Size: 327492 Color: 1
Size: 312081 Color: 0

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 427769 Color: 1
Size: 310154 Color: 1
Size: 262077 Color: 0

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 364374 Color: 1
Size: 361788 Color: 1
Size: 273838 Color: 0

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 383224 Color: 1
Size: 343565 Color: 1
Size: 273211 Color: 0

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 434153 Color: 1
Size: 286806 Color: 1
Size: 279041 Color: 0

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 423883 Color: 1
Size: 308446 Color: 1
Size: 267671 Color: 0

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 412907 Color: 1
Size: 323031 Color: 1
Size: 264062 Color: 0

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 406295 Color: 1
Size: 317443 Color: 1
Size: 276262 Color: 0

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 351469 Color: 1
Size: 348127 Color: 1
Size: 300404 Color: 0

Bin 438: 2 of cap free
Amount of items: 3
Items: 
Size: 419105 Color: 1
Size: 327326 Color: 1
Size: 253568 Color: 0

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 361044 Color: 1
Size: 322599 Color: 0
Size: 316356 Color: 1

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 414381 Color: 1
Size: 323265 Color: 1
Size: 262353 Color: 0

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 405388 Color: 1
Size: 326516 Color: 1
Size: 268095 Color: 0

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 395420 Color: 1
Size: 317901 Color: 1
Size: 286678 Color: 0

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 372458 Color: 1
Size: 365301 Color: 1
Size: 262240 Color: 0

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 438677 Color: 1
Size: 285367 Color: 1
Size: 275955 Color: 0

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 400860 Color: 1
Size: 331239 Color: 1
Size: 267900 Color: 0

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 376704 Color: 1
Size: 344801 Color: 1
Size: 278494 Color: 0

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 397441 Color: 1
Size: 331051 Color: 1
Size: 271507 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 365177 Color: 1
Size: 344654 Color: 1
Size: 290168 Color: 0

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 406625 Color: 1
Size: 326551 Color: 1
Size: 266823 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 365550 Color: 1
Size: 319383 Color: 0
Size: 315066 Color: 1

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 412838 Color: 1
Size: 294488 Color: 1
Size: 292673 Color: 0

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 448239 Color: 1
Size: 291073 Color: 1
Size: 260687 Color: 0

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 377509 Color: 1
Size: 369412 Color: 1
Size: 253078 Color: 0

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 369871 Color: 1
Size: 353903 Color: 1
Size: 276225 Color: 0

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 396901 Color: 1
Size: 323914 Color: 1
Size: 279184 Color: 0

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 398604 Color: 1
Size: 325650 Color: 1
Size: 275745 Color: 0

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 405268 Color: 1
Size: 331041 Color: 1
Size: 263690 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 372614 Color: 1
Size: 360599 Color: 1
Size: 266786 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 403995 Color: 1
Size: 343826 Color: 1
Size: 252178 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 406358 Color: 1
Size: 309063 Color: 0
Size: 284578 Color: 1

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 409529 Color: 1
Size: 301082 Color: 0
Size: 289388 Color: 1

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 413513 Color: 1
Size: 297235 Color: 0
Size: 289251 Color: 1

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 352971 Color: 1
Size: 331347 Color: 1
Size: 315681 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 400012 Color: 1
Size: 300611 Color: 0
Size: 299376 Color: 1

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 380631 Color: 1
Size: 365646 Color: 1
Size: 253722 Color: 0

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 384768 Color: 1
Size: 340826 Color: 1
Size: 274405 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 444410 Color: 1
Size: 300750 Color: 1
Size: 254839 Color: 0

Bin 468: 3 of cap free
Amount of items: 3
Items: 
Size: 395377 Color: 1
Size: 308472 Color: 0
Size: 296149 Color: 1

Bin 469: 3 of cap free
Amount of items: 3
Items: 
Size: 393838 Color: 1
Size: 325430 Color: 1
Size: 280730 Color: 0

Bin 470: 3 of cap free
Amount of items: 3
Items: 
Size: 443425 Color: 1
Size: 285626 Color: 1
Size: 270947 Color: 0

Bin 471: 3 of cap free
Amount of items: 3
Items: 
Size: 410867 Color: 1
Size: 303289 Color: 1
Size: 285842 Color: 0

Bin 472: 3 of cap free
Amount of items: 3
Items: 
Size: 359269 Color: 1
Size: 342112 Color: 1
Size: 298617 Color: 0

Bin 473: 3 of cap free
Amount of items: 3
Items: 
Size: 359225 Color: 1
Size: 341015 Color: 1
Size: 299758 Color: 0

Bin 474: 3 of cap free
Amount of items: 3
Items: 
Size: 381697 Color: 1
Size: 316873 Color: 1
Size: 301428 Color: 0

Bin 475: 3 of cap free
Amount of items: 3
Items: 
Size: 414606 Color: 1
Size: 320428 Color: 1
Size: 264964 Color: 0

Bin 476: 3 of cap free
Amount of items: 3
Items: 
Size: 386840 Color: 1
Size: 354994 Color: 1
Size: 258164 Color: 0

Bin 477: 3 of cap free
Amount of items: 3
Items: 
Size: 386058 Color: 1
Size: 324273 Color: 1
Size: 289667 Color: 0

Bin 478: 3 of cap free
Amount of items: 3
Items: 
Size: 410001 Color: 1
Size: 303304 Color: 1
Size: 286693 Color: 0

Bin 479: 3 of cap free
Amount of items: 3
Items: 
Size: 381004 Color: 1
Size: 368551 Color: 1
Size: 250443 Color: 0

Bin 480: 3 of cap free
Amount of items: 3
Items: 
Size: 379665 Color: 1
Size: 326236 Color: 1
Size: 294097 Color: 0

Bin 481: 3 of cap free
Amount of items: 3
Items: 
Size: 418058 Color: 1
Size: 322335 Color: 1
Size: 259605 Color: 0

Bin 482: 3 of cap free
Amount of items: 3
Items: 
Size: 449066 Color: 1
Size: 291511 Color: 1
Size: 259421 Color: 0

Bin 483: 3 of cap free
Amount of items: 3
Items: 
Size: 377314 Color: 1
Size: 361931 Color: 1
Size: 260753 Color: 0

Bin 484: 4 of cap free
Amount of items: 3
Items: 
Size: 388422 Color: 1
Size: 321292 Color: 1
Size: 290283 Color: 0

Bin 485: 4 of cap free
Amount of items: 3
Items: 
Size: 384371 Color: 1
Size: 317029 Color: 0
Size: 298597 Color: 1

Bin 486: 4 of cap free
Amount of items: 3
Items: 
Size: 437006 Color: 1
Size: 301428 Color: 1
Size: 261563 Color: 0

Bin 487: 4 of cap free
Amount of items: 3
Items: 
Size: 390749 Color: 1
Size: 317319 Color: 1
Size: 291929 Color: 0

Bin 488: 4 of cap free
Amount of items: 3
Items: 
Size: 358675 Color: 1
Size: 353676 Color: 1
Size: 287646 Color: 0

Bin 489: 4 of cap free
Amount of items: 3
Items: 
Size: 388910 Color: 1
Size: 323789 Color: 1
Size: 287298 Color: 0

Bin 490: 4 of cap free
Amount of items: 3
Items: 
Size: 391211 Color: 1
Size: 354432 Color: 1
Size: 254354 Color: 0

Bin 491: 4 of cap free
Amount of items: 3
Items: 
Size: 349347 Color: 1
Size: 347030 Color: 1
Size: 303620 Color: 0

Bin 492: 4 of cap free
Amount of items: 3
Items: 
Size: 373996 Color: 1
Size: 325755 Color: 1
Size: 300246 Color: 0

Bin 493: 4 of cap free
Amount of items: 3
Items: 
Size: 404576 Color: 1
Size: 322216 Color: 1
Size: 273205 Color: 0

Bin 494: 4 of cap free
Amount of items: 3
Items: 
Size: 388207 Color: 1
Size: 329846 Color: 1
Size: 281944 Color: 0

Bin 495: 4 of cap free
Amount of items: 3
Items: 
Size: 371403 Color: 1
Size: 350359 Color: 1
Size: 278235 Color: 0

Bin 496: 4 of cap free
Amount of items: 3
Items: 
Size: 362571 Color: 1
Size: 354411 Color: 1
Size: 283015 Color: 0

Bin 497: 4 of cap free
Amount of items: 3
Items: 
Size: 379349 Color: 1
Size: 333179 Color: 1
Size: 287469 Color: 0

Bin 498: 4 of cap free
Amount of items: 3
Items: 
Size: 412444 Color: 1
Size: 304812 Color: 1
Size: 282741 Color: 0

Bin 499: 4 of cap free
Amount of items: 3
Items: 
Size: 385838 Color: 1
Size: 326684 Color: 1
Size: 287475 Color: 0

Bin 500: 4 of cap free
Amount of items: 3
Items: 
Size: 381323 Color: 1
Size: 368184 Color: 1
Size: 250490 Color: 0

Bin 501: 5 of cap free
Amount of items: 3
Items: 
Size: 446223 Color: 1
Size: 297155 Color: 1
Size: 256618 Color: 0

Bin 502: 5 of cap free
Amount of items: 3
Items: 
Size: 426045 Color: 1
Size: 315741 Color: 1
Size: 258210 Color: 0

Bin 503: 5 of cap free
Amount of items: 3
Items: 
Size: 437201 Color: 1
Size: 301264 Color: 1
Size: 261531 Color: 0

Bin 504: 5 of cap free
Amount of items: 3
Items: 
Size: 393257 Color: 1
Size: 353702 Color: 1
Size: 253037 Color: 0

Bin 505: 5 of cap free
Amount of items: 3
Items: 
Size: 369363 Color: 1
Size: 350210 Color: 1
Size: 280423 Color: 0

Bin 506: 5 of cap free
Amount of items: 3
Items: 
Size: 368651 Color: 1
Size: 353198 Color: 1
Size: 278147 Color: 0

Bin 507: 5 of cap free
Amount of items: 3
Items: 
Size: 424735 Color: 1
Size: 289190 Color: 1
Size: 286071 Color: 0

Bin 508: 5 of cap free
Amount of items: 3
Items: 
Size: 379532 Color: 1
Size: 323191 Color: 0
Size: 297273 Color: 1

Bin 509: 5 of cap free
Amount of items: 3
Items: 
Size: 439974 Color: 1
Size: 284961 Color: 1
Size: 275061 Color: 0

Bin 510: 5 of cap free
Amount of items: 3
Items: 
Size: 348911 Color: 1
Size: 328936 Color: 0
Size: 322149 Color: 1

Bin 511: 5 of cap free
Amount of items: 3
Items: 
Size: 439699 Color: 1
Size: 306558 Color: 1
Size: 253739 Color: 0

Bin 512: 6 of cap free
Amount of items: 3
Items: 
Size: 435262 Color: 1
Size: 303927 Color: 1
Size: 260806 Color: 0

Bin 513: 6 of cap free
Amount of items: 3
Items: 
Size: 399255 Color: 1
Size: 303204 Color: 0
Size: 297536 Color: 1

Bin 514: 6 of cap free
Amount of items: 3
Items: 
Size: 381175 Color: 1
Size: 323585 Color: 1
Size: 295235 Color: 0

Bin 515: 6 of cap free
Amount of items: 3
Items: 
Size: 410666 Color: 1
Size: 332748 Color: 1
Size: 256581 Color: 0

Bin 516: 6 of cap free
Amount of items: 3
Items: 
Size: 357071 Color: 1
Size: 349844 Color: 1
Size: 293080 Color: 0

Bin 517: 6 of cap free
Amount of items: 3
Items: 
Size: 426233 Color: 1
Size: 323751 Color: 1
Size: 250011 Color: 0

Bin 518: 6 of cap free
Amount of items: 3
Items: 
Size: 435301 Color: 1
Size: 306924 Color: 1
Size: 257770 Color: 0

Bin 519: 6 of cap free
Amount of items: 3
Items: 
Size: 395363 Color: 1
Size: 320439 Color: 0
Size: 284193 Color: 1

Bin 520: 6 of cap free
Amount of items: 3
Items: 
Size: 411141 Color: 1
Size: 312730 Color: 1
Size: 276124 Color: 0

Bin 521: 6 of cap free
Amount of items: 3
Items: 
Size: 432810 Color: 1
Size: 313087 Color: 1
Size: 254098 Color: 0

Bin 522: 6 of cap free
Amount of items: 3
Items: 
Size: 380560 Color: 1
Size: 310974 Color: 0
Size: 308461 Color: 1

Bin 523: 6 of cap free
Amount of items: 3
Items: 
Size: 442016 Color: 1
Size: 307528 Color: 1
Size: 250451 Color: 0

Bin 524: 6 of cap free
Amount of items: 3
Items: 
Size: 404211 Color: 1
Size: 323004 Color: 1
Size: 272780 Color: 0

Bin 525: 6 of cap free
Amount of items: 3
Items: 
Size: 364728 Color: 1
Size: 336833 Color: 1
Size: 298434 Color: 0

Bin 526: 6 of cap free
Amount of items: 3
Items: 
Size: 348629 Color: 1
Size: 341590 Color: 1
Size: 309776 Color: 0

Bin 527: 6 of cap free
Amount of items: 3
Items: 
Size: 450722 Color: 1
Size: 286282 Color: 1
Size: 262991 Color: 0

Bin 528: 7 of cap free
Amount of items: 3
Items: 
Size: 431050 Color: 1
Size: 300784 Color: 1
Size: 268160 Color: 0

Bin 529: 7 of cap free
Amount of items: 3
Items: 
Size: 397386 Color: 1
Size: 314542 Color: 1
Size: 288066 Color: 0

Bin 530: 7 of cap free
Amount of items: 3
Items: 
Size: 362097 Color: 1
Size: 346246 Color: 1
Size: 291651 Color: 0

Bin 531: 7 of cap free
Amount of items: 3
Items: 
Size: 385343 Color: 1
Size: 361313 Color: 1
Size: 253338 Color: 0

Bin 532: 7 of cap free
Amount of items: 3
Items: 
Size: 444336 Color: 1
Size: 303738 Color: 1
Size: 251920 Color: 0

Bin 533: 8 of cap free
Amount of items: 3
Items: 
Size: 393537 Color: 1
Size: 313635 Color: 1
Size: 292821 Color: 0

Bin 534: 8 of cap free
Amount of items: 3
Items: 
Size: 401210 Color: 1
Size: 342898 Color: 1
Size: 255885 Color: 0

Bin 535: 8 of cap free
Amount of items: 3
Items: 
Size: 427223 Color: 1
Size: 296033 Color: 1
Size: 276737 Color: 0

Bin 536: 8 of cap free
Amount of items: 3
Items: 
Size: 403919 Color: 1
Size: 317082 Color: 1
Size: 278992 Color: 0

Bin 537: 8 of cap free
Amount of items: 3
Items: 
Size: 374469 Color: 1
Size: 364295 Color: 1
Size: 261229 Color: 0

Bin 538: 8 of cap free
Amount of items: 3
Items: 
Size: 410451 Color: 1
Size: 309011 Color: 1
Size: 280531 Color: 0

Bin 539: 8 of cap free
Amount of items: 3
Items: 
Size: 365533 Color: 1
Size: 337707 Color: 1
Size: 296753 Color: 0

Bin 540: 8 of cap free
Amount of items: 3
Items: 
Size: 423832 Color: 1
Size: 321064 Color: 1
Size: 255097 Color: 0

Bin 541: 9 of cap free
Amount of items: 3
Items: 
Size: 424613 Color: 1
Size: 314456 Color: 1
Size: 260923 Color: 0

Bin 542: 9 of cap free
Amount of items: 3
Items: 
Size: 369973 Color: 1
Size: 341237 Color: 1
Size: 288782 Color: 0

Bin 543: 9 of cap free
Amount of items: 3
Items: 
Size: 388858 Color: 1
Size: 349640 Color: 1
Size: 261494 Color: 0

Bin 544: 10 of cap free
Amount of items: 3
Items: 
Size: 417503 Color: 1
Size: 307001 Color: 1
Size: 275487 Color: 0

Bin 545: 10 of cap free
Amount of items: 3
Items: 
Size: 372547 Color: 1
Size: 354241 Color: 1
Size: 273203 Color: 0

Bin 546: 10 of cap free
Amount of items: 3
Items: 
Size: 360771 Color: 1
Size: 340129 Color: 1
Size: 299091 Color: 0

Bin 547: 11 of cap free
Amount of items: 3
Items: 
Size: 437708 Color: 1
Size: 298404 Color: 1
Size: 263878 Color: 0

Bin 548: 11 of cap free
Amount of items: 3
Items: 
Size: 436419 Color: 1
Size: 291346 Color: 1
Size: 272225 Color: 0

Bin 549: 11 of cap free
Amount of items: 3
Items: 
Size: 374618 Color: 1
Size: 317724 Color: 1
Size: 307648 Color: 0

Bin 550: 11 of cap free
Amount of items: 3
Items: 
Size: 383875 Color: 1
Size: 340909 Color: 1
Size: 275206 Color: 0

Bin 551: 12 of cap free
Amount of items: 3
Items: 
Size: 393910 Color: 1
Size: 316126 Color: 1
Size: 289953 Color: 0

Bin 552: 12 of cap free
Amount of items: 3
Items: 
Size: 392740 Color: 1
Size: 307859 Color: 0
Size: 299390 Color: 1

Bin 553: 12 of cap free
Amount of items: 3
Items: 
Size: 449563 Color: 1
Size: 290682 Color: 1
Size: 259744 Color: 0

Bin 554: 12 of cap free
Amount of items: 3
Items: 
Size: 375449 Color: 1
Size: 373819 Color: 1
Size: 250721 Color: 0

Bin 555: 12 of cap free
Amount of items: 3
Items: 
Size: 369428 Color: 1
Size: 358904 Color: 1
Size: 271657 Color: 0

Bin 556: 12 of cap free
Amount of items: 3
Items: 
Size: 464325 Color: 1
Size: 283177 Color: 1
Size: 252487 Color: 0

Bin 557: 12 of cap free
Amount of items: 3
Items: 
Size: 364191 Color: 1
Size: 343947 Color: 1
Size: 291851 Color: 0

Bin 558: 13 of cap free
Amount of items: 3
Items: 
Size: 416582 Color: 1
Size: 303251 Color: 1
Size: 280155 Color: 0

Bin 559: 13 of cap free
Amount of items: 3
Items: 
Size: 429299 Color: 1
Size: 308745 Color: 1
Size: 261944 Color: 0

Bin 560: 13 of cap free
Amount of items: 3
Items: 
Size: 405567 Color: 1
Size: 317673 Color: 1
Size: 276748 Color: 0

Bin 561: 14 of cap free
Amount of items: 3
Items: 
Size: 392233 Color: 1
Size: 349148 Color: 1
Size: 258606 Color: 0

Bin 562: 14 of cap free
Amount of items: 3
Items: 
Size: 395772 Color: 1
Size: 340616 Color: 1
Size: 263599 Color: 0

Bin 563: 14 of cap free
Amount of items: 3
Items: 
Size: 412459 Color: 1
Size: 301971 Color: 0
Size: 285557 Color: 1

Bin 564: 15 of cap free
Amount of items: 3
Items: 
Size: 420728 Color: 1
Size: 317839 Color: 1
Size: 261419 Color: 0

Bin 565: 16 of cap free
Amount of items: 3
Items: 
Size: 460232 Color: 1
Size: 277616 Color: 1
Size: 262137 Color: 0

Bin 566: 16 of cap free
Amount of items: 3
Items: 
Size: 394321 Color: 1
Size: 319909 Color: 1
Size: 285755 Color: 0

Bin 567: 16 of cap free
Amount of items: 3
Items: 
Size: 419873 Color: 1
Size: 313733 Color: 1
Size: 266379 Color: 0

Bin 568: 17 of cap free
Amount of items: 3
Items: 
Size: 360209 Color: 1
Size: 350263 Color: 1
Size: 289512 Color: 0

Bin 569: 17 of cap free
Amount of items: 3
Items: 
Size: 348466 Color: 1
Size: 335742 Color: 1
Size: 315776 Color: 0

Bin 570: 18 of cap free
Amount of items: 3
Items: 
Size: 451776 Color: 1
Size: 296781 Color: 1
Size: 251426 Color: 0

Bin 571: 18 of cap free
Amount of items: 3
Items: 
Size: 442335 Color: 1
Size: 284281 Color: 1
Size: 273367 Color: 0

Bin 572: 18 of cap free
Amount of items: 3
Items: 
Size: 383988 Color: 1
Size: 349185 Color: 1
Size: 266810 Color: 0

Bin 573: 18 of cap free
Amount of items: 3
Items: 
Size: 364233 Color: 1
Size: 330605 Color: 1
Size: 305145 Color: 0

Bin 574: 18 of cap free
Amount of items: 3
Items: 
Size: 399679 Color: 1
Size: 308157 Color: 1
Size: 292147 Color: 0

Bin 575: 19 of cap free
Amount of items: 3
Items: 
Size: 426681 Color: 1
Size: 305874 Color: 1
Size: 267427 Color: 0

Bin 576: 19 of cap free
Amount of items: 3
Items: 
Size: 388024 Color: 1
Size: 342582 Color: 1
Size: 269376 Color: 0

Bin 577: 20 of cap free
Amount of items: 3
Items: 
Size: 399875 Color: 1
Size: 338299 Color: 1
Size: 261807 Color: 0

Bin 578: 21 of cap free
Amount of items: 3
Items: 
Size: 411211 Color: 1
Size: 306771 Color: 1
Size: 281998 Color: 0

Bin 579: 21 of cap free
Amount of items: 3
Items: 
Size: 418396 Color: 1
Size: 321197 Color: 1
Size: 260387 Color: 0

Bin 580: 21 of cap free
Amount of items: 3
Items: 
Size: 391134 Color: 1
Size: 312968 Color: 1
Size: 295878 Color: 0

Bin 581: 21 of cap free
Amount of items: 3
Items: 
Size: 365353 Color: 1
Size: 345180 Color: 1
Size: 289447 Color: 0

Bin 582: 22 of cap free
Amount of items: 3
Items: 
Size: 442071 Color: 1
Size: 307599 Color: 1
Size: 250309 Color: 0

Bin 583: 22 of cap free
Amount of items: 3
Items: 
Size: 362206 Color: 1
Size: 321237 Color: 1
Size: 316536 Color: 0

Bin 584: 23 of cap free
Amount of items: 3
Items: 
Size: 384497 Color: 1
Size: 328926 Color: 1
Size: 286555 Color: 0

Bin 585: 24 of cap free
Amount of items: 3
Items: 
Size: 413327 Color: 1
Size: 294151 Color: 1
Size: 292499 Color: 0

Bin 586: 24 of cap free
Amount of items: 3
Items: 
Size: 390259 Color: 1
Size: 332509 Color: 1
Size: 277209 Color: 0

Bin 587: 24 of cap free
Amount of items: 3
Items: 
Size: 389050 Color: 1
Size: 357339 Color: 1
Size: 253588 Color: 0

Bin 588: 24 of cap free
Amount of items: 3
Items: 
Size: 412645 Color: 1
Size: 307829 Color: 1
Size: 279503 Color: 0

Bin 589: 25 of cap free
Amount of items: 3
Items: 
Size: 362614 Color: 1
Size: 354650 Color: 1
Size: 282712 Color: 0

Bin 590: 25 of cap free
Amount of items: 3
Items: 
Size: 441814 Color: 1
Size: 280035 Color: 1
Size: 278127 Color: 0

Bin 591: 25 of cap free
Amount of items: 3
Items: 
Size: 373250 Color: 1
Size: 332834 Color: 1
Size: 293892 Color: 0

Bin 592: 25 of cap free
Amount of items: 3
Items: 
Size: 404784 Color: 1
Size: 310407 Color: 1
Size: 284785 Color: 0

Bin 593: 26 of cap free
Amount of items: 3
Items: 
Size: 376369 Color: 1
Size: 323158 Color: 1
Size: 300448 Color: 0

Bin 594: 28 of cap free
Amount of items: 3
Items: 
Size: 401601 Color: 1
Size: 345086 Color: 1
Size: 253286 Color: 0

Bin 595: 29 of cap free
Amount of items: 3
Items: 
Size: 409752 Color: 1
Size: 301627 Color: 1
Size: 288593 Color: 0

Bin 596: 29 of cap free
Amount of items: 3
Items: 
Size: 413745 Color: 1
Size: 319382 Color: 1
Size: 266845 Color: 0

Bin 597: 30 of cap free
Amount of items: 3
Items: 
Size: 380640 Color: 1
Size: 343246 Color: 1
Size: 276085 Color: 0

Bin 598: 30 of cap free
Amount of items: 3
Items: 
Size: 397881 Color: 1
Size: 336790 Color: 1
Size: 265300 Color: 0

Bin 599: 30 of cap free
Amount of items: 3
Items: 
Size: 450668 Color: 1
Size: 298102 Color: 1
Size: 251201 Color: 0

Bin 600: 31 of cap free
Amount of items: 3
Items: 
Size: 374650 Color: 1
Size: 346073 Color: 1
Size: 279247 Color: 0

Bin 601: 31 of cap free
Amount of items: 3
Items: 
Size: 408637 Color: 1
Size: 318636 Color: 1
Size: 272697 Color: 0

Bin 602: 33 of cap free
Amount of items: 3
Items: 
Size: 471456 Color: 1
Size: 267882 Color: 1
Size: 260630 Color: 0

Bin 603: 36 of cap free
Amount of items: 3
Items: 
Size: 434058 Color: 1
Size: 314153 Color: 1
Size: 251754 Color: 0

Bin 604: 37 of cap free
Amount of items: 3
Items: 
Size: 377584 Color: 1
Size: 338939 Color: 1
Size: 283441 Color: 0

Bin 605: 38 of cap free
Amount of items: 3
Items: 
Size: 407554 Color: 1
Size: 314213 Color: 1
Size: 278196 Color: 0

Bin 606: 39 of cap free
Amount of items: 3
Items: 
Size: 373597 Color: 1
Size: 342773 Color: 1
Size: 283592 Color: 0

Bin 607: 42 of cap free
Amount of items: 3
Items: 
Size: 443114 Color: 1
Size: 295941 Color: 1
Size: 260904 Color: 0

Bin 608: 44 of cap free
Amount of items: 3
Items: 
Size: 363788 Color: 1
Size: 328245 Color: 1
Size: 307924 Color: 0

Bin 609: 45 of cap free
Amount of items: 3
Items: 
Size: 383667 Color: 1
Size: 350879 Color: 1
Size: 265410 Color: 0

Bin 610: 45 of cap free
Amount of items: 3
Items: 
Size: 417764 Color: 1
Size: 326096 Color: 1
Size: 256096 Color: 0

Bin 611: 46 of cap free
Amount of items: 3
Items: 
Size: 410310 Color: 1
Size: 312747 Color: 1
Size: 276898 Color: 0

Bin 612: 48 of cap free
Amount of items: 3
Items: 
Size: 416502 Color: 1
Size: 292481 Color: 1
Size: 290970 Color: 0

Bin 613: 50 of cap free
Amount of items: 3
Items: 
Size: 382443 Color: 1
Size: 312272 Color: 0
Size: 305236 Color: 1

Bin 614: 51 of cap free
Amount of items: 3
Items: 
Size: 435899 Color: 1
Size: 305719 Color: 1
Size: 258332 Color: 0

Bin 615: 53 of cap free
Amount of items: 3
Items: 
Size: 385036 Color: 1
Size: 335984 Color: 1
Size: 278928 Color: 0

Bin 616: 54 of cap free
Amount of items: 3
Items: 
Size: 363066 Color: 1
Size: 330176 Color: 1
Size: 306705 Color: 0

Bin 617: 54 of cap free
Amount of items: 3
Items: 
Size: 376197 Color: 1
Size: 348974 Color: 1
Size: 274776 Color: 0

Bin 618: 58 of cap free
Amount of items: 3
Items: 
Size: 393126 Color: 1
Size: 308429 Color: 0
Size: 298388 Color: 1

Bin 619: 59 of cap free
Amount of items: 3
Items: 
Size: 389993 Color: 1
Size: 333672 Color: 1
Size: 276277 Color: 0

Bin 620: 60 of cap free
Amount of items: 3
Items: 
Size: 375803 Color: 1
Size: 346854 Color: 1
Size: 277284 Color: 0

Bin 621: 61 of cap free
Amount of items: 3
Items: 
Size: 413301 Color: 1
Size: 336225 Color: 1
Size: 250414 Color: 0

Bin 622: 63 of cap free
Amount of items: 3
Items: 
Size: 428158 Color: 1
Size: 316929 Color: 1
Size: 254851 Color: 0

Bin 623: 66 of cap free
Amount of items: 3
Items: 
Size: 420757 Color: 1
Size: 319534 Color: 1
Size: 259644 Color: 0

Bin 624: 66 of cap free
Amount of items: 3
Items: 
Size: 442474 Color: 1
Size: 296271 Color: 1
Size: 261190 Color: 0

Bin 625: 66 of cap free
Amount of items: 3
Items: 
Size: 409383 Color: 1
Size: 309938 Color: 1
Size: 280614 Color: 0

Bin 626: 67 of cap free
Amount of items: 3
Items: 
Size: 398286 Color: 1
Size: 315054 Color: 1
Size: 286594 Color: 0

Bin 627: 69 of cap free
Amount of items: 3
Items: 
Size: 344129 Color: 1
Size: 342858 Color: 1
Size: 312945 Color: 0

Bin 628: 77 of cap free
Amount of items: 3
Items: 
Size: 359429 Color: 1
Size: 323788 Color: 1
Size: 316707 Color: 0

Bin 629: 77 of cap free
Amount of items: 3
Items: 
Size: 387483 Color: 1
Size: 342006 Color: 1
Size: 270435 Color: 0

Bin 630: 79 of cap free
Amount of items: 3
Items: 
Size: 417167 Color: 1
Size: 331417 Color: 1
Size: 251338 Color: 0

Bin 631: 89 of cap free
Amount of items: 3
Items: 
Size: 413072 Color: 1
Size: 324739 Color: 1
Size: 262101 Color: 0

Bin 632: 93 of cap free
Amount of items: 3
Items: 
Size: 432645 Color: 1
Size: 309776 Color: 1
Size: 257487 Color: 0

Bin 633: 99 of cap free
Amount of items: 3
Items: 
Size: 398077 Color: 1
Size: 334916 Color: 1
Size: 266909 Color: 0

Bin 634: 100 of cap free
Amount of items: 3
Items: 
Size: 390557 Color: 1
Size: 357749 Color: 1
Size: 251595 Color: 0

Bin 635: 100 of cap free
Amount of items: 3
Items: 
Size: 411660 Color: 1
Size: 336502 Color: 1
Size: 251739 Color: 0

Bin 636: 107 of cap free
Amount of items: 3
Items: 
Size: 429986 Color: 1
Size: 310614 Color: 1
Size: 259294 Color: 0

Bin 637: 119 of cap free
Amount of items: 3
Items: 
Size: 407822 Color: 1
Size: 337847 Color: 1
Size: 254213 Color: 0

Bin 638: 129 of cap free
Amount of items: 3
Items: 
Size: 381932 Color: 1
Size: 353173 Color: 1
Size: 264767 Color: 0

Bin 639: 131 of cap free
Amount of items: 3
Items: 
Size: 377084 Color: 1
Size: 349257 Color: 1
Size: 273529 Color: 0

Bin 640: 134 of cap free
Amount of items: 3
Items: 
Size: 366065 Color: 1
Size: 360057 Color: 1
Size: 273745 Color: 0

Bin 641: 136 of cap free
Amount of items: 3
Items: 
Size: 415554 Color: 1
Size: 316093 Color: 1
Size: 268218 Color: 0

Bin 642: 137 of cap free
Amount of items: 3
Items: 
Size: 344346 Color: 1
Size: 331522 Color: 1
Size: 323996 Color: 0

Bin 643: 138 of cap free
Amount of items: 3
Items: 
Size: 391742 Color: 1
Size: 315462 Color: 1
Size: 292659 Color: 0

Bin 644: 158 of cap free
Amount of items: 3
Items: 
Size: 459724 Color: 1
Size: 285609 Color: 1
Size: 254510 Color: 0

Bin 645: 174 of cap free
Amount of items: 3
Items: 
Size: 398960 Color: 1
Size: 333594 Color: 1
Size: 267273 Color: 0

Bin 646: 175 of cap free
Amount of items: 3
Items: 
Size: 371243 Color: 1
Size: 340547 Color: 1
Size: 288036 Color: 0

Bin 647: 177 of cap free
Amount of items: 3
Items: 
Size: 372854 Color: 1
Size: 323093 Color: 0
Size: 303877 Color: 1

Bin 648: 177 of cap free
Amount of items: 3
Items: 
Size: 464427 Color: 1
Size: 281590 Color: 1
Size: 253807 Color: 0

Bin 649: 201 of cap free
Amount of items: 3
Items: 
Size: 386975 Color: 1
Size: 332557 Color: 1
Size: 280268 Color: 0

Bin 650: 226 of cap free
Amount of items: 3
Items: 
Size: 365258 Color: 1
Size: 352420 Color: 1
Size: 282097 Color: 0

Bin 651: 234 of cap free
Amount of items: 3
Items: 
Size: 368382 Color: 1
Size: 343979 Color: 1
Size: 287406 Color: 0

Bin 652: 261 of cap free
Amount of items: 3
Items: 
Size: 414283 Color: 1
Size: 329972 Color: 1
Size: 255485 Color: 0

Bin 653: 271 of cap free
Amount of items: 3
Items: 
Size: 410846 Color: 1
Size: 338742 Color: 1
Size: 250142 Color: 0

Bin 654: 338 of cap free
Amount of items: 3
Items: 
Size: 361568 Color: 1
Size: 354170 Color: 1
Size: 283925 Color: 0

Bin 655: 351 of cap free
Amount of items: 3
Items: 
Size: 421899 Color: 1
Size: 302977 Color: 1
Size: 274774 Color: 0

Bin 656: 434 of cap free
Amount of items: 3
Items: 
Size: 366584 Color: 1
Size: 332328 Color: 1
Size: 300655 Color: 0

Bin 657: 478 of cap free
Amount of items: 3
Items: 
Size: 455724 Color: 1
Size: 293201 Color: 1
Size: 250598 Color: 0

Bin 658: 587 of cap free
Amount of items: 3
Items: 
Size: 394420 Color: 1
Size: 334034 Color: 1
Size: 270960 Color: 0

Bin 659: 597 of cap free
Amount of items: 3
Items: 
Size: 467606 Color: 1
Size: 266444 Color: 1
Size: 265354 Color: 0

Bin 660: 647 of cap free
Amount of items: 3
Items: 
Size: 403073 Color: 1
Size: 322594 Color: 1
Size: 273687 Color: 0

Bin 661: 677 of cap free
Amount of items: 3
Items: 
Size: 469307 Color: 1
Size: 269575 Color: 1
Size: 260442 Color: 0

Bin 662: 3095 of cap free
Amount of items: 3
Items: 
Size: 383796 Color: 1
Size: 325910 Color: 1
Size: 287200 Color: 0

Bin 663: 3221 of cap free
Amount of items: 3
Items: 
Size: 436001 Color: 1
Size: 296266 Color: 1
Size: 264513 Color: 0

Bin 664: 7193 of cap free
Amount of items: 3
Items: 
Size: 382117 Color: 1
Size: 342239 Color: 1
Size: 268452 Color: 0

Bin 665: 28439 of cap free
Amount of items: 3
Items: 
Size: 372346 Color: 1
Size: 337118 Color: 1
Size: 262098 Color: 0

Bin 666: 162552 of cap free
Amount of items: 3
Items: 
Size: 291742 Color: 1
Size: 287984 Color: 1
Size: 257723 Color: 0

Bin 667: 265539 of cap free
Amount of items: 2
Items: 
Size: 483909 Color: 1
Size: 250553 Color: 0

Bin 668: 519121 of cap free
Amount of items: 1
Items: 
Size: 480880 Color: 1

Total size: 667000667
Total free space: 1000001

