Capicity Bin: 1976
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 990 Color: 139
Size: 218 Color: 76
Size: 210 Color: 73
Size: 206 Color: 72
Size: 204 Color: 71
Size: 100 Color: 52
Size: 48 Color: 20

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 155
Size: 485 Color: 115
Size: 158 Color: 66

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 161
Size: 456 Color: 111
Size: 106 Color: 54

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1499 Color: 167
Size: 399 Color: 106
Size: 78 Color: 43

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 168
Size: 389 Color: 103
Size: 84 Color: 46

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 171
Size: 402 Color: 107
Size: 50 Color: 22

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 176
Size: 319 Color: 96
Size: 68 Color: 34

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 178
Size: 305 Color: 91
Size: 68 Color: 35

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 182
Size: 213 Color: 74
Size: 128 Color: 60

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 188
Size: 241 Color: 81
Size: 62 Color: 30

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 189
Size: 233 Color: 79
Size: 64 Color: 32

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 190
Size: 246 Color: 82
Size: 48 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 192
Size: 259 Color: 85
Size: 20 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 194
Size: 227 Color: 78
Size: 44 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 196
Size: 216 Color: 75
Size: 42 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 200
Size: 140 Color: 63
Size: 62 Color: 31

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 201
Size: 130 Color: 61
Size: 68 Color: 36

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1318 Color: 153
Size: 657 Color: 126

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 160
Size: 542 Color: 120
Size: 32 Color: 6

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 163
Size: 553 Color: 122

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 165
Size: 502 Color: 116
Size: 8 Color: 0

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1494 Color: 166
Size: 481 Color: 114

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 177
Size: 380 Color: 102

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 185
Size: 317 Color: 95

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 195
Size: 262 Color: 86

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1726 Color: 198
Size: 249 Color: 83

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1507 Color: 169
Size: 467 Color: 113

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1667 Color: 187
Size: 307 Color: 93

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1700 Color: 193
Size: 274 Color: 88

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 197
Size: 253 Color: 84

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 199
Size: 236 Color: 80

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1511 Color: 170
Size: 462 Color: 112

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 173
Size: 430 Color: 110

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1578 Color: 175
Size: 395 Color: 105

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 180
Size: 362 Color: 101

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1627 Color: 181
Size: 346 Color: 99

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 184
Size: 323 Color: 97

Bin 38: 4 of cap free
Amount of items: 4
Items: 
Size: 1395 Color: 159
Size: 537 Color: 119
Size: 24 Color: 3
Size: 16 Color: 1

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 183
Size: 334 Color: 98

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 186
Size: 306 Color: 92

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 1610 Color: 179
Size: 361 Color: 100

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1689 Color: 191
Size: 282 Color: 89

Bin 43: 6 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 150
Size: 668 Color: 127
Size: 40 Color: 8

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1562 Color: 174
Size: 408 Color: 108

Bin 45: 7 of cap free
Amount of items: 4
Items: 
Size: 1374 Color: 158
Size: 531 Color: 118
Size: 32 Color: 5
Size: 32 Color: 4

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1542 Color: 172
Size: 427 Color: 109

Bin 47: 8 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 141
Size: 899 Color: 137
Size: 46 Color: 16

Bin 48: 8 of cap free
Amount of items: 3
Items: 
Size: 1103 Color: 144
Size: 823 Color: 135
Size: 42 Color: 11

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1417 Color: 162
Size: 550 Color: 121

Bin 50: 10 of cap free
Amount of items: 3
Items: 
Size: 1054 Color: 142
Size: 866 Color: 136
Size: 46 Color: 15

Bin 51: 10 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 156
Size: 585 Color: 123
Size: 40 Color: 7

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1368 Color: 157
Size: 598 Color: 124

Bin 53: 11 of cap free
Amount of items: 6
Items: 
Size: 991 Color: 140
Size: 391 Color: 104
Size: 266 Color: 87
Size: 221 Color: 77
Size: 48 Color: 19
Size: 48 Color: 18

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1236 Color: 149
Size: 729 Color: 130

Bin 55: 12 of cap free
Amount of items: 22
Items: 
Size: 144 Color: 64
Size: 136 Color: 62
Size: 116 Color: 59
Size: 116 Color: 58
Size: 110 Color: 57
Size: 108 Color: 56
Size: 108 Color: 55
Size: 104 Color: 53
Size: 96 Color: 51
Size: 94 Color: 50
Size: 92 Color: 49
Size: 92 Color: 48
Size: 76 Color: 40
Size: 72 Color: 39
Size: 72 Color: 38
Size: 72 Color: 37
Size: 64 Color: 33
Size: 60 Color: 29
Size: 60 Color: 28
Size: 60 Color: 27
Size: 56 Color: 26
Size: 56 Color: 25

Bin 56: 12 of cap free
Amount of items: 2
Items: 
Size: 1313 Color: 152
Size: 651 Color: 125

Bin 57: 12 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 164
Size: 502 Color: 117

Bin 58: 18 of cap free
Amount of items: 2
Items: 
Size: 1158 Color: 146
Size: 800 Color: 133

Bin 59: 19 of cap free
Amount of items: 9
Items: 
Size: 989 Color: 138
Size: 170 Color: 70
Size: 166 Color: 69
Size: 164 Color: 68
Size: 164 Color: 67
Size: 150 Color: 65
Size: 52 Color: 24
Size: 52 Color: 23
Size: 50 Color: 21

Bin 60: 19 of cap free
Amount of items: 2
Items: 
Size: 1275 Color: 151
Size: 682 Color: 128

Bin 61: 21 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 148
Size: 718 Color: 129
Size: 40 Color: 9

Bin 62: 23 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 145
Size: 795 Color: 132
Size: 40 Color: 10

Bin 63: 35 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 154
Size: 311 Color: 94
Size: 304 Color: 90

Bin 64: 36 of cap free
Amount of items: 2
Items: 
Size: 1189 Color: 147
Size: 751 Color: 131

Bin 65: 37 of cap free
Amount of items: 3
Items: 
Size: 1075 Color: 143
Size: 822 Color: 134
Size: 42 Color: 12

Bin 66: 1570 of cap free
Amount of items: 5
Items: 
Size: 86 Color: 47
Size: 84 Color: 45
Size: 80 Color: 44
Size: 78 Color: 42
Size: 78 Color: 41

Total size: 128440
Total free space: 1976

