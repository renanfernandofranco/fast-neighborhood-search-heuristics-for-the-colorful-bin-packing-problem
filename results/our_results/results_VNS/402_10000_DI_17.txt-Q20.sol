Capicity Bin: 8264
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4150 Color: 0
Size: 2091 Color: 4
Size: 1305 Color: 7
Size: 374 Color: 10
Size: 344 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4516 Color: 5
Size: 3584 Color: 3
Size: 164 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4730 Color: 2
Size: 3338 Color: 6
Size: 196 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5403 Color: 14
Size: 2381 Color: 2
Size: 480 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 5
Size: 2284 Color: 9
Size: 448 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5566 Color: 13
Size: 2422 Color: 3
Size: 276 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 0
Size: 2108 Color: 6
Size: 160 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6212 Color: 7
Size: 1996 Color: 0
Size: 56 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 7
Size: 1671 Color: 12
Size: 334 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 17
Size: 1692 Color: 7
Size: 160 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6414 Color: 13
Size: 1166 Color: 16
Size: 684 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6502 Color: 12
Size: 1518 Color: 14
Size: 244 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 11
Size: 1542 Color: 13
Size: 196 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6605 Color: 16
Size: 1119 Color: 8
Size: 540 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 12
Size: 1379 Color: 18
Size: 274 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 0
Size: 1320 Color: 10
Size: 264 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 9
Size: 813 Color: 12
Size: 732 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 16
Size: 1361 Color: 4
Size: 138 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 1
Size: 1242 Color: 18
Size: 248 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 11
Size: 1116 Color: 1
Size: 246 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 15
Size: 1111 Color: 4
Size: 190 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6931 Color: 0
Size: 797 Color: 18
Size: 536 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 11
Size: 1114 Color: 5
Size: 180 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 15
Size: 1044 Color: 4
Size: 216 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 13
Size: 908 Color: 17
Size: 336 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 10
Size: 726 Color: 16
Size: 448 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7093 Color: 4
Size: 977 Color: 11
Size: 194 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7137 Color: 7
Size: 923 Color: 17
Size: 204 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7154 Color: 7
Size: 790 Color: 16
Size: 320 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 16
Size: 924 Color: 3
Size: 158 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7201 Color: 13
Size: 841 Color: 1
Size: 222 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7214 Color: 7
Size: 786 Color: 15
Size: 264 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7257 Color: 6
Size: 751 Color: 17
Size: 256 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 12
Size: 740 Color: 0
Size: 264 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 7
Size: 782 Color: 1
Size: 208 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7306 Color: 18
Size: 542 Color: 7
Size: 416 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7354 Color: 2
Size: 684 Color: 19
Size: 226 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7388 Color: 15
Size: 624 Color: 3
Size: 252 Color: 12

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7394 Color: 3
Size: 568 Color: 19
Size: 302 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 7
Size: 588 Color: 15
Size: 272 Color: 10

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5009 Color: 8
Size: 2962 Color: 6
Size: 292 Color: 16

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 19
Size: 2367 Color: 5
Size: 166 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 14
Size: 2097 Color: 12
Size: 136 Color: 13

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 18
Size: 1665 Color: 15
Size: 488 Color: 15

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6245 Color: 15
Size: 1862 Color: 10
Size: 156 Color: 17

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 7
Size: 1683 Color: 14

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 2
Size: 1227 Color: 9
Size: 416 Color: 10

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6793 Color: 18
Size: 1470 Color: 4

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 11
Size: 1397 Color: 12

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 18
Size: 716 Color: 2
Size: 664 Color: 9

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 7039 Color: 10
Size: 1000 Color: 1
Size: 224 Color: 7

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 15
Size: 704 Color: 16
Size: 418 Color: 16

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 7157 Color: 10
Size: 858 Color: 15
Size: 248 Color: 14

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 7281 Color: 0
Size: 982 Color: 3

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 11
Size: 598 Color: 9
Size: 356 Color: 15

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 7322 Color: 12
Size: 941 Color: 16

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 7359 Color: 18
Size: 756 Color: 17
Size: 148 Color: 5

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 5850 Color: 7
Size: 2412 Color: 6

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 6
Size: 1372 Color: 10
Size: 700 Color: 15

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6686 Color: 15
Size: 1576 Color: 10

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 17
Size: 1284 Color: 13

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 7082 Color: 1
Size: 1180 Color: 10

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 7164 Color: 7
Size: 1098 Color: 9

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 7298 Color: 14
Size: 964 Color: 11

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 7325 Color: 19
Size: 937 Color: 11

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 7418 Color: 10
Size: 844 Color: 18

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4140 Color: 1
Size: 3441 Color: 0
Size: 680 Color: 17

Bin 68: 3 of cap free
Amount of items: 4
Items: 
Size: 4262 Color: 12
Size: 3443 Color: 15
Size: 400 Color: 9
Size: 156 Color: 9

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 4673 Color: 3
Size: 3444 Color: 2
Size: 144 Color: 6

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 5001 Color: 9
Size: 3124 Color: 3
Size: 136 Color: 14

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 5362 Color: 6
Size: 2707 Color: 3
Size: 192 Color: 11

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 14
Size: 3764 Color: 2
Size: 180 Color: 0

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 5156 Color: 17
Size: 3104 Color: 14

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6251 Color: 9
Size: 1865 Color: 19
Size: 144 Color: 15

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 6785 Color: 14
Size: 1289 Color: 15
Size: 186 Color: 4

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 13
Size: 1679 Color: 1
Size: 144 Color: 0

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 10
Size: 1383 Color: 0
Size: 144 Color: 19

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 15
Size: 1450 Color: 4
Size: 48 Color: 17

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 0
Size: 1151 Color: 1

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 1
Size: 1021 Color: 11

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 4836 Color: 16
Size: 3422 Color: 5

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 12
Size: 2385 Color: 0
Size: 448 Color: 0

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 5740 Color: 2
Size: 2518 Color: 5

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 6734 Color: 6
Size: 1524 Color: 17

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 5
Size: 1334 Color: 10

Bin 86: 6 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 11
Size: 1054 Color: 18
Size: 24 Color: 2

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6459 Color: 4
Size: 1798 Color: 5

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 6619 Color: 0
Size: 1085 Color: 3
Size: 552 Color: 15

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 12
Size: 1404 Color: 7

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 12
Size: 926 Color: 1

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 0
Size: 1138 Color: 4

Bin 92: 10 of cap free
Amount of items: 5
Items: 
Size: 4134 Color: 15
Size: 1511 Color: 1
Size: 1505 Color: 3
Size: 684 Color: 2
Size: 420 Color: 19

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 5676 Color: 1
Size: 2578 Color: 19

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 6648 Color: 14
Size: 1606 Color: 1

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 18
Size: 1018 Color: 1

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 4135 Color: 1
Size: 3430 Color: 0
Size: 688 Color: 9

Bin 97: 11 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 10
Size: 2713 Color: 4
Size: 826 Color: 4

Bin 98: 11 of cap free
Amount of items: 3
Items: 
Size: 6027 Color: 11
Size: 2114 Color: 8
Size: 112 Color: 12

Bin 99: 11 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 16
Size: 1131 Color: 17
Size: 76 Color: 15

Bin 100: 11 of cap free
Amount of items: 2
Items: 
Size: 7412 Color: 16
Size: 841 Color: 12

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 7365 Color: 1
Size: 887 Color: 14

Bin 102: 13 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 15
Size: 2242 Color: 10
Size: 260 Color: 2

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 6236 Color: 6
Size: 2014 Color: 14

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 7002 Color: 10
Size: 1248 Color: 3

Bin 105: 15 of cap free
Amount of items: 2
Items: 
Size: 5757 Color: 1
Size: 2492 Color: 15

Bin 106: 15 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 0
Size: 2373 Color: 18

Bin 107: 15 of cap free
Amount of items: 2
Items: 
Size: 7428 Color: 4
Size: 821 Color: 19

Bin 108: 16 of cap free
Amount of items: 2
Items: 
Size: 7380 Color: 2
Size: 868 Color: 16

Bin 109: 18 of cap free
Amount of items: 2
Items: 
Size: 7289 Color: 19
Size: 957 Color: 5

Bin 110: 19 of cap free
Amount of items: 3
Items: 
Size: 6267 Color: 11
Size: 1124 Color: 5
Size: 854 Color: 0

Bin 111: 19 of cap free
Amount of items: 2
Items: 
Size: 7069 Color: 9
Size: 1176 Color: 6

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 7364 Color: 19
Size: 878 Color: 2

Bin 113: 24 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 1
Size: 2716 Color: 7
Size: 248 Color: 12

Bin 114: 24 of cap free
Amount of items: 3
Items: 
Size: 6446 Color: 12
Size: 1730 Color: 13
Size: 64 Color: 15

Bin 115: 25 of cap free
Amount of items: 2
Items: 
Size: 6451 Color: 8
Size: 1788 Color: 18

Bin 116: 28 of cap free
Amount of items: 2
Items: 
Size: 4938 Color: 15
Size: 3298 Color: 14

Bin 117: 34 of cap free
Amount of items: 2
Items: 
Size: 6338 Color: 6
Size: 1892 Color: 1

Bin 118: 35 of cap free
Amount of items: 14
Items: 
Size: 902 Color: 7
Size: 854 Color: 6
Size: 802 Color: 13
Size: 783 Color: 0
Size: 724 Color: 8
Size: 688 Color: 7
Size: 542 Color: 2
Size: 512 Color: 15
Size: 512 Color: 12
Size: 496 Color: 16
Size: 476 Color: 17
Size: 474 Color: 17
Size: 232 Color: 12
Size: 232 Color: 5

Bin 119: 38 of cap free
Amount of items: 3
Items: 
Size: 5574 Color: 16
Size: 2596 Color: 14
Size: 56 Color: 18

Bin 120: 38 of cap free
Amount of items: 3
Items: 
Size: 6019 Color: 5
Size: 1871 Color: 17
Size: 336 Color: 0

Bin 121: 39 of cap free
Amount of items: 2
Items: 
Size: 6907 Color: 10
Size: 1318 Color: 5

Bin 122: 41 of cap free
Amount of items: 2
Items: 
Size: 4681 Color: 16
Size: 3542 Color: 4

Bin 123: 46 of cap free
Amount of items: 6
Items: 
Size: 4133 Color: 9
Size: 1278 Color: 8
Size: 1233 Color: 17
Size: 986 Color: 15
Size: 372 Color: 5
Size: 216 Color: 7

Bin 124: 50 of cap free
Amount of items: 2
Items: 
Size: 4142 Color: 13
Size: 4072 Color: 15

Bin 125: 50 of cap free
Amount of items: 2
Items: 
Size: 6666 Color: 10
Size: 1548 Color: 19

Bin 126: 58 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 12
Size: 2860 Color: 4
Size: 334 Color: 2

Bin 127: 73 of cap free
Amount of items: 2
Items: 
Size: 5417 Color: 11
Size: 2774 Color: 16

Bin 128: 80 of cap free
Amount of items: 3
Items: 
Size: 4158 Color: 6
Size: 3438 Color: 17
Size: 588 Color: 11

Bin 129: 82 of cap free
Amount of items: 30
Items: 
Size: 474 Color: 2
Size: 472 Color: 4
Size: 468 Color: 14
Size: 392 Color: 18
Size: 376 Color: 17
Size: 376 Color: 6
Size: 372 Color: 16
Size: 332 Color: 17
Size: 308 Color: 14
Size: 304 Color: 19
Size: 304 Color: 3
Size: 300 Color: 16
Size: 300 Color: 7
Size: 288 Color: 9
Size: 280 Color: 6
Size: 236 Color: 19
Size: 230 Color: 0
Size: 200 Color: 14
Size: 200 Color: 3
Size: 198 Color: 10
Size: 192 Color: 2
Size: 186 Color: 15
Size: 184 Color: 11
Size: 184 Color: 10
Size: 184 Color: 6
Size: 176 Color: 9
Size: 176 Color: 5
Size: 172 Color: 5
Size: 162 Color: 12
Size: 156 Color: 12

Bin 130: 97 of cap free
Amount of items: 2
Items: 
Size: 5174 Color: 11
Size: 2993 Color: 3

Bin 131: 132 of cap free
Amount of items: 2
Items: 
Size: 5017 Color: 3
Size: 3115 Color: 16

Bin 132: 134 of cap free
Amount of items: 2
Items: 
Size: 5409 Color: 1
Size: 2721 Color: 14

Bin 133: 6682 of cap free
Amount of items: 10
Items: 
Size: 176 Color: 1
Size: 176 Color: 0
Size: 172 Color: 9
Size: 168 Color: 8
Size: 162 Color: 2
Size: 152 Color: 10
Size: 152 Color: 7
Size: 144 Color: 12
Size: 144 Color: 3
Size: 136 Color: 5

Total size: 1090848
Total free space: 8264

