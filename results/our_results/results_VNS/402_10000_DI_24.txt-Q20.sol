Capicity Bin: 8192
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4708 Color: 7
Size: 2932 Color: 9
Size: 552 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5415 Color: 18
Size: 2315 Color: 11
Size: 462 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5745 Color: 15
Size: 2041 Color: 10
Size: 406 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 10
Size: 1850 Color: 12
Size: 440 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 3
Size: 2033 Color: 16
Size: 166 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6077 Color: 5
Size: 2037 Color: 19
Size: 78 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 17
Size: 1567 Color: 19
Size: 312 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 2
Size: 1078 Color: 9
Size: 758 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 4
Size: 1572 Color: 5
Size: 168 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 19
Size: 1324 Color: 11
Size: 406 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 8
Size: 1462 Color: 13
Size: 140 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6693 Color: 10
Size: 1255 Color: 11
Size: 244 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 13
Size: 1321 Color: 15
Size: 210 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 15
Size: 1090 Color: 13
Size: 352 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6769 Color: 7
Size: 1187 Color: 19
Size: 236 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 13
Size: 1180 Color: 13
Size: 232 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 4
Size: 953 Color: 0
Size: 366 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 15
Size: 716 Color: 7
Size: 590 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 10
Size: 1014 Color: 12
Size: 270 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 7
Size: 706 Color: 0
Size: 508 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 10
Size: 972 Color: 16
Size: 176 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 11
Size: 828 Color: 12
Size: 336 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7037 Color: 5
Size: 1019 Color: 12
Size: 136 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 16
Size: 922 Color: 17
Size: 184 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 6
Size: 764 Color: 4
Size: 248 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 1
Size: 742 Color: 3
Size: 254 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 10
Size: 730 Color: 2
Size: 216 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 19
Size: 764 Color: 7
Size: 152 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7280 Color: 3
Size: 588 Color: 2
Size: 324 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7284 Color: 4
Size: 818 Color: 14
Size: 90 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 18
Size: 680 Color: 19
Size: 194 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7346 Color: 15
Size: 588 Color: 7
Size: 258 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 5
Size: 584 Color: 15
Size: 284 Color: 14

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 4860 Color: 19
Size: 2951 Color: 16
Size: 380 Color: 7

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 9
Size: 1983 Color: 4
Size: 964 Color: 13

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 13
Size: 2322 Color: 13
Size: 120 Color: 4

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 9
Size: 1988 Color: 18
Size: 166 Color: 12

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 6
Size: 1613 Color: 10
Size: 140 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 7
Size: 1332 Color: 8
Size: 372 Color: 19

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6569 Color: 15
Size: 1622 Color: 7

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 4
Size: 872 Color: 16
Size: 682 Color: 17

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6641 Color: 9
Size: 1222 Color: 17
Size: 328 Color: 11

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 2
Size: 892 Color: 8
Size: 314 Color: 5

Bin 44: 2 of cap free
Amount of items: 17
Items: 
Size: 718 Color: 6
Size: 680 Color: 17
Size: 680 Color: 13
Size: 640 Color: 5
Size: 600 Color: 13
Size: 574 Color: 8
Size: 520 Color: 11
Size: 512 Color: 18
Size: 496 Color: 6
Size: 464 Color: 16
Size: 464 Color: 6
Size: 456 Color: 17
Size: 406 Color: 1
Size: 404 Color: 4
Size: 264 Color: 15
Size: 168 Color: 10
Size: 144 Color: 9

Bin 45: 2 of cap free
Amount of items: 5
Items: 
Size: 4100 Color: 17
Size: 1317 Color: 2
Size: 1232 Color: 10
Size: 1037 Color: 3
Size: 504 Color: 3

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 10
Size: 2524 Color: 8
Size: 136 Color: 6

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 11
Size: 1804 Color: 16
Size: 136 Color: 3

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 6257 Color: 2
Size: 1421 Color: 15
Size: 512 Color: 11

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 6297 Color: 10
Size: 1797 Color: 16
Size: 96 Color: 15

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 6613 Color: 10
Size: 1313 Color: 4
Size: 264 Color: 19

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 9
Size: 1018 Color: 2
Size: 576 Color: 10

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6609 Color: 7
Size: 1581 Color: 12

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 7
Size: 1573 Color: 15

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6819 Color: 13
Size: 1251 Color: 12
Size: 120 Color: 15

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 14
Size: 982 Color: 8
Size: 162 Color: 10

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 7049 Color: 9
Size: 1141 Color: 12

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 9
Size: 1020 Color: 15

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 4609 Color: 10
Size: 3412 Color: 6
Size: 168 Color: 0

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 5069 Color: 1
Size: 2934 Color: 6
Size: 186 Color: 17

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5455 Color: 8
Size: 2534 Color: 14
Size: 200 Color: 17

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 16
Size: 1833 Color: 1
Size: 208 Color: 19

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 4
Size: 1389 Color: 2
Size: 44 Color: 9

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6921 Color: 10
Size: 1092 Color: 15
Size: 176 Color: 13

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 7252 Color: 19
Size: 937 Color: 4

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 15
Size: 3009 Color: 19
Size: 112 Color: 11

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 7
Size: 1365 Color: 2

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 9
Size: 1326 Color: 16

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 7334 Color: 17
Size: 854 Color: 3

Bin 69: 5 of cap free
Amount of items: 3
Items: 
Size: 4544 Color: 1
Size: 3411 Color: 15
Size: 232 Color: 5

Bin 70: 5 of cap free
Amount of items: 3
Items: 
Size: 4649 Color: 8
Size: 2954 Color: 4
Size: 584 Color: 10

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 5761 Color: 1
Size: 2162 Color: 3
Size: 264 Color: 6

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 4
Size: 1910 Color: 17
Size: 244 Color: 10

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 5
Size: 973 Color: 4

Bin 74: 6 of cap free
Amount of items: 5
Items: 
Size: 4101 Color: 10
Size: 1626 Color: 13
Size: 1451 Color: 8
Size: 520 Color: 4
Size: 488 Color: 5

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 5974 Color: 3
Size: 2212 Color: 10

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 6905 Color: 2
Size: 1281 Color: 8

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 7122 Color: 4
Size: 680 Color: 10
Size: 384 Color: 2

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 7302 Color: 15
Size: 884 Color: 19

Bin 79: 7 of cap free
Amount of items: 7
Items: 
Size: 4098 Color: 1
Size: 1061 Color: 4
Size: 790 Color: 14
Size: 760 Color: 6
Size: 724 Color: 19
Size: 376 Color: 5
Size: 376 Color: 2

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 5918 Color: 3
Size: 2027 Color: 8
Size: 240 Color: 6

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 19
Size: 1128 Color: 15
Size: 32 Color: 14

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 19
Size: 2780 Color: 5

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 7160 Color: 6
Size: 1024 Color: 5

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6532 Color: 12
Size: 1650 Color: 2

Bin 85: 11 of cap free
Amount of items: 2
Items: 
Size: 5900 Color: 11
Size: 2281 Color: 16

Bin 86: 11 of cap free
Amount of items: 2
Items: 
Size: 6884 Color: 0
Size: 1297 Color: 10

Bin 87: 11 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 9
Size: 1041 Color: 12

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 1
Size: 1338 Color: 9

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 7070 Color: 1
Size: 1110 Color: 16

Bin 90: 12 of cap free
Amount of items: 2
Items: 
Size: 7286 Color: 4
Size: 894 Color: 17

Bin 91: 13 of cap free
Amount of items: 3
Items: 
Size: 4653 Color: 18
Size: 3308 Color: 2
Size: 218 Color: 1

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 17
Size: 1577 Color: 5

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 5
Size: 3048 Color: 19

Bin 94: 14 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 10
Size: 2570 Color: 6
Size: 436 Color: 8

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 13
Size: 1452 Color: 18

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 6949 Color: 10
Size: 1229 Color: 3

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 6974 Color: 1
Size: 1204 Color: 7

Bin 98: 15 of cap free
Amount of items: 3
Items: 
Size: 4607 Color: 15
Size: 3410 Color: 8
Size: 160 Color: 1

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 11
Size: 1295 Color: 7

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 7340 Color: 3
Size: 836 Color: 8

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 5570 Color: 16
Size: 2605 Color: 10

Bin 102: 18 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 17
Size: 2908 Color: 9
Size: 112 Color: 11

Bin 103: 18 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 16
Size: 1568 Color: 2
Size: 392 Color: 10

Bin 104: 18 of cap free
Amount of items: 2
Items: 
Size: 6972 Color: 18
Size: 1202 Color: 0

Bin 105: 19 of cap free
Amount of items: 3
Items: 
Size: 6036 Color: 15
Size: 2073 Color: 9
Size: 64 Color: 18

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 15
Size: 1763 Color: 12
Size: 656 Color: 19

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 6544 Color: 14
Size: 1628 Color: 1

Bin 108: 21 of cap free
Amount of items: 2
Items: 
Size: 6301 Color: 0
Size: 1870 Color: 14

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 12
Size: 1926 Color: 4

Bin 110: 25 of cap free
Amount of items: 2
Items: 
Size: 6305 Color: 14
Size: 1862 Color: 15

Bin 111: 27 of cap free
Amount of items: 2
Items: 
Size: 5705 Color: 4
Size: 2460 Color: 15

Bin 112: 30 of cap free
Amount of items: 2
Items: 
Size: 7069 Color: 6
Size: 1093 Color: 11

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 6453 Color: 15
Size: 1708 Color: 3

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 5958 Color: 14
Size: 2202 Color: 17

Bin 115: 34 of cap free
Amount of items: 2
Items: 
Size: 6242 Color: 3
Size: 1916 Color: 6

Bin 116: 35 of cap free
Amount of items: 2
Items: 
Size: 5554 Color: 12
Size: 2603 Color: 19

Bin 117: 37 of cap free
Amount of items: 3
Items: 
Size: 5676 Color: 5
Size: 1353 Color: 15
Size: 1126 Color: 6

Bin 118: 37 of cap free
Amount of items: 2
Items: 
Size: 6657 Color: 19
Size: 1498 Color: 15

Bin 119: 47 of cap free
Amount of items: 2
Items: 
Size: 4097 Color: 6
Size: 4048 Color: 10

Bin 120: 48 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 7
Size: 1492 Color: 3

Bin 121: 50 of cap free
Amount of items: 2
Items: 
Size: 5110 Color: 2
Size: 3032 Color: 7

Bin 122: 52 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 0
Size: 3592 Color: 19
Size: 320 Color: 13

Bin 123: 53 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 19
Size: 3341 Color: 9
Size: 148 Color: 6

Bin 124: 56 of cap free
Amount of items: 2
Items: 
Size: 5812 Color: 1
Size: 2324 Color: 7

Bin 125: 56 of cap free
Amount of items: 2
Items: 
Size: 5950 Color: 14
Size: 2186 Color: 16

Bin 126: 58 of cap free
Amount of items: 2
Items: 
Size: 5882 Color: 11
Size: 2252 Color: 10

Bin 127: 61 of cap free
Amount of items: 3
Items: 
Size: 4102 Color: 10
Size: 2953 Color: 12
Size: 1076 Color: 9

Bin 128: 80 of cap free
Amount of items: 28
Items: 
Size: 436 Color: 11
Size: 432 Color: 6
Size: 414 Color: 12
Size: 372 Color: 1
Size: 368 Color: 17
Size: 358 Color: 0
Size: 352 Color: 2
Size: 340 Color: 7
Size: 320 Color: 4
Size: 314 Color: 19
Size: 314 Color: 16
Size: 292 Color: 8
Size: 288 Color: 13
Size: 288 Color: 3
Size: 264 Color: 5
Size: 264 Color: 2
Size: 262 Color: 18
Size: 262 Color: 18
Size: 262 Color: 15
Size: 232 Color: 0
Size: 228 Color: 13
Size: 220 Color: 15
Size: 216 Color: 9
Size: 216 Color: 4
Size: 206 Color: 5
Size: 200 Color: 15
Size: 200 Color: 1
Size: 192 Color: 9

Bin 129: 98 of cap free
Amount of items: 2
Items: 
Size: 5540 Color: 11
Size: 2554 Color: 9

Bin 130: 102 of cap free
Amount of items: 2
Items: 
Size: 4676 Color: 19
Size: 3414 Color: 2

Bin 131: 105 of cap free
Amount of items: 2
Items: 
Size: 4674 Color: 18
Size: 3413 Color: 8

Bin 132: 110 of cap free
Amount of items: 3
Items: 
Size: 5578 Color: 11
Size: 2182 Color: 5
Size: 322 Color: 19

Bin 133: 6366 of cap free
Amount of items: 11
Items: 
Size: 224 Color: 18
Size: 190 Color: 15
Size: 184 Color: 9
Size: 164 Color: 4
Size: 160 Color: 16
Size: 160 Color: 5
Size: 156 Color: 13
Size: 152 Color: 16
Size: 148 Color: 14
Size: 144 Color: 19
Size: 144 Color: 1

Total size: 1081344
Total free space: 8192

