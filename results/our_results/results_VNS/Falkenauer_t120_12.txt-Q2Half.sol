Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 347 Color: 1
Size: 283 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 255 Color: 0
Size: 253 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 255 Color: 0
Size: 348 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 358 Color: 1
Size: 267 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 250 Color: 0
Size: 268 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 280 Color: 1
Size: 273 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 323 Color: 1
Size: 294 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 298 Color: 1
Size: 252 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 339 Color: 1
Size: 253 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 281 Color: 1
Size: 264 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 348 Color: 1
Size: 257 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 271 Color: 1
Size: 269 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 260 Color: 1
Size: 252 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 322 Color: 1
Size: 278 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 290 Color: 1
Size: 281 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 1
Size: 250 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 354 Color: 1
Size: 269 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 329 Color: 1
Size: 260 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 266 Color: 1
Size: 261 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 298 Color: 1
Size: 255 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 1
Size: 250 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 250 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 361 Color: 1
Size: 251 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 261 Color: 0
Size: 259 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 329 Color: 1
Size: 296 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 260 Color: 1
Size: 254 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 341 Color: 1
Size: 302 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 356 Color: 1
Size: 265 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 350 Color: 1
Size: 284 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 1
Size: 260 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 343 Color: 1
Size: 266 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 309 Color: 1
Size: 293 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 278 Color: 1
Size: 272 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 282 Color: 1
Size: 272 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 253 Color: 0
Size: 326 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 1
Size: 283 Color: 0

Total size: 40000
Total free space: 0

