Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 257 Color: 16
Size: 284 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 10
Size: 340 Color: 5
Size: 250 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 10
Size: 273 Color: 15
Size: 262 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 271 Color: 9
Size: 253 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 6
Size: 302 Color: 9
Size: 261 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 14
Size: 348 Color: 14
Size: 296 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 15
Size: 257 Color: 9
Size: 253 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 12
Size: 318 Color: 13
Size: 302 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 13
Size: 291 Color: 6
Size: 250 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 2
Size: 345 Color: 8
Size: 253 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 14
Size: 337 Color: 15
Size: 264 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 14
Size: 276 Color: 5
Size: 254 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 262 Color: 14
Size: 251 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 6
Size: 310 Color: 17
Size: 286 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 357 Color: 14
Size: 266 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 18
Size: 330 Color: 18
Size: 254 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 291 Color: 4
Size: 262 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 360 Color: 7
Size: 256 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 326 Color: 19
Size: 270 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 15
Size: 260 Color: 2
Size: 252 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9
Size: 267 Color: 7
Size: 252 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 8
Size: 256 Color: 9
Size: 252 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 17
Size: 250 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 331 Color: 11
Size: 256 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 14
Size: 312 Color: 0
Size: 273 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 321 Color: 2
Size: 286 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 16
Size: 334 Color: 0
Size: 306 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 308 Color: 15
Size: 292 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 2
Size: 274 Color: 7
Size: 250 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 14
Size: 290 Color: 4
Size: 276 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 6
Size: 322 Color: 19
Size: 279 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 10
Size: 256 Color: 18
Size: 252 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 15
Size: 315 Color: 3
Size: 279 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 18
Size: 262 Color: 5
Size: 258 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 19
Size: 260 Color: 12
Size: 251 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 18
Size: 299 Color: 0
Size: 250 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 10
Size: 354 Color: 17
Size: 288 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 7
Size: 318 Color: 15
Size: 274 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 13
Size: 324 Color: 17
Size: 295 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 13
Size: 350 Color: 7
Size: 271 Color: 5

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 18
Size: 310 Color: 3
Size: 280 Color: 8

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 263 Color: 1
Size: 258 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 18
Size: 320 Color: 7
Size: 266 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 259 Color: 19
Size: 255 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 15
Size: 281 Color: 4
Size: 252 Color: 6

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 11
Size: 338 Color: 9
Size: 290 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 329 Color: 17
Size: 278 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 5
Size: 291 Color: 3
Size: 277 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 335 Color: 0
Size: 280 Color: 13

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 310 Color: 0
Size: 254 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 17
Size: 315 Color: 6
Size: 290 Color: 7

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 12
Size: 368 Color: 8
Size: 260 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 361 Color: 6
Size: 252 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 261 Color: 8
Size: 257 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 6
Size: 272 Color: 12
Size: 272 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 19
Size: 281 Color: 5
Size: 263 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 6
Size: 258 Color: 14
Size: 255 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 15
Size: 305 Color: 5
Size: 254 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 323 Color: 9
Size: 290 Color: 10

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 356 Color: 8
Size: 258 Color: 7

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 363 Color: 6
Size: 257 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 19
Size: 302 Color: 10
Size: 259 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 5
Size: 256 Color: 19
Size: 251 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 355 Color: 1
Size: 263 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 19
Size: 353 Color: 1
Size: 250 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 282 Color: 16
Size: 263 Color: 10

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 4
Size: 302 Color: 17
Size: 286 Color: 17

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 19
Size: 293 Color: 0
Size: 258 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 310 Color: 11
Size: 253 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 18
Size: 256 Color: 1
Size: 252 Color: 10

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 17
Size: 253 Color: 0
Size: 252 Color: 17

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 267 Color: 1
Size: 266 Color: 8

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 325 Color: 4
Size: 282 Color: 10

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 328 Color: 9
Size: 266 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 8
Size: 352 Color: 14
Size: 266 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 16
Size: 265 Color: 1
Size: 263 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 17
Size: 265 Color: 17
Size: 251 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 367 Color: 14
Size: 256 Color: 16

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7
Size: 363 Color: 3
Size: 266 Color: 17

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 17
Size: 294 Color: 17
Size: 265 Color: 7

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 304 Color: 10
Size: 262 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 8
Size: 292 Color: 5
Size: 250 Color: 14

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 10
Size: 293 Color: 2
Size: 289 Color: 3

Total size: 83000
Total free space: 0

