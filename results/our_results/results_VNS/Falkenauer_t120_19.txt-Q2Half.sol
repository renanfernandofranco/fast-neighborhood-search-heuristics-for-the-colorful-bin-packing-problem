Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 309 Color: 1
Size: 281 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 298 Color: 1
Size: 276 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 321 Color: 1
Size: 315 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 271 Color: 1
Size: 257 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 254 Color: 0
Size: 296 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 250 Color: 0
Size: 251 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 369 Color: 1
Size: 260 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 291 Color: 1
Size: 267 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 309 Color: 0
Size: 292 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 330 Color: 1
Size: 296 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 282 Color: 1
Size: 251 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 341 Color: 1
Size: 279 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 333 Color: 1
Size: 309 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 330 Color: 1
Size: 254 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 271 Color: 1
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 275 Color: 1
Size: 265 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 309 Color: 1
Size: 273 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 257 Color: 0
Size: 259 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 1
Size: 253 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 309 Color: 1
Size: 261 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 253 Color: 1
Size: 251 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 320 Color: 1
Size: 273 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 271 Color: 1
Size: 253 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 340 Color: 1
Size: 270 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 276 Color: 1
Size: 256 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 305 Color: 1
Size: 277 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 333 Color: 1
Size: 300 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 326 Color: 1
Size: 288 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 276 Color: 1
Size: 268 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 352 Color: 1
Size: 269 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 350 Color: 1
Size: 269 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 260 Color: 1
Size: 254 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 337 Color: 1
Size: 258 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 331 Color: 1
Size: 274 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 345 Color: 1
Size: 265 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 291 Color: 1
Size: 275 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 319 Color: 1
Size: 301 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 272 Color: 1
Size: 259 Color: 0

Total size: 40000
Total free space: 0

