Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 418593 Color: 1
Size: 315441 Color: 1
Size: 265967 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462498 Color: 1
Size: 286558 Color: 1
Size: 250945 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 471350 Color: 1
Size: 270286 Color: 1
Size: 258365 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 419523 Color: 1
Size: 310656 Color: 1
Size: 269822 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 492531 Color: 1
Size: 256781 Color: 1
Size: 250689 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 491483 Color: 1
Size: 258304 Color: 1
Size: 250214 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 427516 Color: 1
Size: 299119 Color: 1
Size: 273366 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 410326 Color: 1
Size: 329762 Color: 1
Size: 259913 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 430573 Color: 1
Size: 296380 Color: 1
Size: 273048 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 427419 Color: 1
Size: 292129 Color: 1
Size: 280453 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 394417 Color: 1
Size: 307054 Color: 0
Size: 298530 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 377299 Color: 1
Size: 357212 Color: 1
Size: 265490 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 477294 Color: 1
Size: 263177 Color: 0
Size: 259530 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 348334 Color: 1
Size: 338500 Color: 1
Size: 313167 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 387096 Color: 1
Size: 320681 Color: 1
Size: 292224 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 418454 Color: 1
Size: 313156 Color: 1
Size: 268391 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 404886 Color: 1
Size: 314048 Color: 1
Size: 281067 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 446359 Color: 1
Size: 286159 Color: 1
Size: 267483 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 429973 Color: 1
Size: 285664 Color: 1
Size: 284364 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 400950 Color: 1
Size: 317988 Color: 1
Size: 281063 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 467084 Color: 1
Size: 282585 Color: 1
Size: 250332 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 422569 Color: 1
Size: 318281 Color: 1
Size: 259151 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 468676 Color: 1
Size: 272887 Color: 1
Size: 258438 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 374626 Color: 1
Size: 356187 Color: 1
Size: 269188 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 466524 Color: 1
Size: 280927 Color: 1
Size: 252550 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 487955 Color: 1
Size: 259748 Color: 1
Size: 252298 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 392298 Color: 1
Size: 317513 Color: 0
Size: 290190 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 403588 Color: 1
Size: 309265 Color: 1
Size: 287148 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 439217 Color: 1
Size: 282946 Color: 1
Size: 277838 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 432958 Color: 1
Size: 302188 Color: 1
Size: 264855 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 440924 Color: 1
Size: 297894 Color: 1
Size: 261183 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 480549 Color: 1
Size: 262817 Color: 1
Size: 256635 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 414479 Color: 1
Size: 321019 Color: 1
Size: 264503 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 352621 Color: 1
Size: 325353 Color: 1
Size: 322027 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 448695 Color: 1
Size: 295695 Color: 1
Size: 255611 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 480852 Color: 1
Size: 264103 Color: 1
Size: 255046 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 493618 Color: 1
Size: 255030 Color: 1
Size: 251353 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 441323 Color: 1
Size: 286572 Color: 1
Size: 272106 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 489449 Color: 1
Size: 256731 Color: 1
Size: 253821 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 426467 Color: 1
Size: 311703 Color: 1
Size: 261831 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 371945 Color: 1
Size: 340649 Color: 1
Size: 287407 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 377058 Color: 1
Size: 334089 Color: 1
Size: 288854 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 473283 Color: 1
Size: 276276 Color: 1
Size: 250442 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 381848 Color: 1
Size: 338334 Color: 1
Size: 279819 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 429643 Color: 1
Size: 302444 Color: 1
Size: 267914 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 360332 Color: 1
Size: 323897 Color: 0
Size: 315772 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 429909 Color: 1
Size: 309388 Color: 1
Size: 260704 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 382322 Color: 1
Size: 321672 Color: 1
Size: 296007 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 458060 Color: 1
Size: 289965 Color: 1
Size: 251976 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 402711 Color: 1
Size: 318414 Color: 1
Size: 278876 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 417858 Color: 1
Size: 318101 Color: 1
Size: 264042 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 444910 Color: 1
Size: 287503 Color: 1
Size: 267588 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 475243 Color: 1
Size: 271359 Color: 1
Size: 253399 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 458015 Color: 1
Size: 276371 Color: 0
Size: 265615 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 402377 Color: 1
Size: 316869 Color: 1
Size: 280755 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 356633 Color: 1
Size: 343527 Color: 1
Size: 299841 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 484016 Color: 1
Size: 263783 Color: 1
Size: 252202 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 472375 Color: 1
Size: 267693 Color: 1
Size: 259933 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 453242 Color: 1
Size: 280828 Color: 1
Size: 265931 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 382601 Color: 1
Size: 343066 Color: 1
Size: 274334 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 415450 Color: 1
Size: 302064 Color: 0
Size: 282487 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 471206 Color: 1
Size: 272385 Color: 1
Size: 256410 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 378125 Color: 1
Size: 320394 Color: 1
Size: 301482 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 412279 Color: 1
Size: 304125 Color: 0
Size: 283597 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 408793 Color: 1
Size: 309020 Color: 1
Size: 282188 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 496983 Color: 1
Size: 252770 Color: 1
Size: 250248 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 424325 Color: 1
Size: 308558 Color: 1
Size: 267118 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 484439 Color: 1
Size: 265053 Color: 1
Size: 250509 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 371346 Color: 1
Size: 367891 Color: 1
Size: 260764 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 433445 Color: 1
Size: 287306 Color: 0
Size: 279250 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 414585 Color: 1
Size: 312228 Color: 1
Size: 273188 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 381314 Color: 1
Size: 367919 Color: 1
Size: 250768 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 496973 Color: 1
Size: 252218 Color: 1
Size: 250810 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 466971 Color: 1
Size: 276362 Color: 1
Size: 256668 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 493637 Color: 1
Size: 254569 Color: 1
Size: 251795 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 458241 Color: 1
Size: 274420 Color: 1
Size: 267340 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 448104 Color: 1
Size: 293280 Color: 1
Size: 258617 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 431407 Color: 1
Size: 287080 Color: 1
Size: 281514 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 431486 Color: 1
Size: 308342 Color: 1
Size: 260173 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 423481 Color: 1
Size: 308293 Color: 1
Size: 268227 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 410703 Color: 1
Size: 333517 Color: 1
Size: 255781 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 474974 Color: 1
Size: 271283 Color: 1
Size: 253744 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 352379 Color: 1
Size: 346132 Color: 1
Size: 301490 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 453245 Color: 1
Size: 284553 Color: 1
Size: 262203 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 423271 Color: 1
Size: 294634 Color: 1
Size: 282096 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 429566 Color: 1
Size: 287219 Color: 1
Size: 283216 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 384837 Color: 1
Size: 308733 Color: 0
Size: 306431 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 378380 Color: 1
Size: 350326 Color: 1
Size: 271295 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 375394 Color: 1
Size: 321948 Color: 1
Size: 302659 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 473157 Color: 1
Size: 269465 Color: 1
Size: 257379 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 458752 Color: 1
Size: 279509 Color: 1
Size: 261740 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 381838 Color: 1
Size: 323498 Color: 1
Size: 294665 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 438933 Color: 1
Size: 285044 Color: 1
Size: 276024 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 449657 Color: 1
Size: 295813 Color: 1
Size: 254531 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 466914 Color: 1
Size: 267317 Color: 1
Size: 265770 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 459052 Color: 1
Size: 289006 Color: 1
Size: 251943 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 491996 Color: 1
Size: 254776 Color: 1
Size: 253229 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 428554 Color: 1
Size: 290693 Color: 0
Size: 280754 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 474794 Color: 1
Size: 264034 Color: 0
Size: 261173 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 418473 Color: 1
Size: 298052 Color: 0
Size: 283476 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 436718 Color: 1
Size: 307771 Color: 1
Size: 255512 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 456997 Color: 1
Size: 275914 Color: 1
Size: 267090 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 481742 Color: 1
Size: 259454 Color: 0
Size: 258805 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 466814 Color: 1
Size: 278457 Color: 1
Size: 254730 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 436949 Color: 1
Size: 286717 Color: 1
Size: 276335 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 354561 Color: 1
Size: 340851 Color: 1
Size: 304589 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 489845 Color: 1
Size: 259953 Color: 1
Size: 250203 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 463252 Color: 1
Size: 278158 Color: 1
Size: 258591 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 393572 Color: 1
Size: 341420 Color: 1
Size: 265009 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 487513 Color: 1
Size: 258325 Color: 1
Size: 254163 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 434419 Color: 1
Size: 288436 Color: 1
Size: 277146 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 395958 Color: 1
Size: 319697 Color: 1
Size: 284346 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 358791 Color: 1
Size: 323650 Color: 0
Size: 317560 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 432680 Color: 1
Size: 303712 Color: 1
Size: 263609 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 446104 Color: 1
Size: 291568 Color: 1
Size: 262329 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 419364 Color: 1
Size: 314456 Color: 1
Size: 266181 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 354155 Color: 1
Size: 347307 Color: 1
Size: 298539 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 357953 Color: 1
Size: 329276 Color: 0
Size: 312772 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 499359 Color: 1
Size: 250489 Color: 1
Size: 250153 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 471235 Color: 1
Size: 267853 Color: 1
Size: 260913 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409431 Color: 1
Size: 312793 Color: 1
Size: 277777 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 469061 Color: 1
Size: 279581 Color: 1
Size: 251359 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 370252 Color: 1
Size: 346686 Color: 1
Size: 283063 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 384591 Color: 1
Size: 342057 Color: 1
Size: 273353 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 464544 Color: 1
Size: 284372 Color: 1
Size: 251085 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 425947 Color: 1
Size: 311925 Color: 0
Size: 262129 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 493108 Color: 1
Size: 254555 Color: 1
Size: 252338 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 497291 Color: 1
Size: 251780 Color: 1
Size: 250930 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 453603 Color: 1
Size: 279221 Color: 1
Size: 267177 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 444336 Color: 1
Size: 284313 Color: 1
Size: 271352 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 382124 Color: 1
Size: 348022 Color: 1
Size: 269855 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 480672 Color: 1
Size: 268144 Color: 1
Size: 251185 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 366445 Color: 1
Size: 337341 Color: 1
Size: 296215 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 480210 Color: 1
Size: 265775 Color: 1
Size: 254016 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 388926 Color: 1
Size: 343376 Color: 1
Size: 267699 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 443244 Color: 1
Size: 299593 Color: 1
Size: 257164 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 482073 Color: 1
Size: 263129 Color: 1
Size: 254799 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 382367 Color: 1
Size: 343520 Color: 1
Size: 274114 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 375242 Color: 1
Size: 357436 Color: 1
Size: 267323 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 399871 Color: 1
Size: 331863 Color: 1
Size: 268267 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 482375 Color: 1
Size: 264410 Color: 1
Size: 253216 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 400103 Color: 1
Size: 316673 Color: 1
Size: 283225 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 409635 Color: 1
Size: 322006 Color: 1
Size: 268360 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 464847 Color: 1
Size: 282862 Color: 1
Size: 252292 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 485743 Color: 1
Size: 259282 Color: 0
Size: 254976 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 483335 Color: 1
Size: 262629 Color: 1
Size: 254037 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 441477 Color: 1
Size: 292662 Color: 0
Size: 265862 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 414610 Color: 1
Size: 324910 Color: 1
Size: 260481 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 461165 Color: 1
Size: 276406 Color: 1
Size: 262430 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 460881 Color: 1
Size: 284605 Color: 1
Size: 254515 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 401571 Color: 1
Size: 315216 Color: 1
Size: 283214 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 453545 Color: 1
Size: 292187 Color: 1
Size: 254269 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 479896 Color: 1
Size: 265315 Color: 1
Size: 254790 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 360790 Color: 1
Size: 350998 Color: 1
Size: 288213 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 380395 Color: 1
Size: 333262 Color: 1
Size: 286344 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 397454 Color: 1
Size: 319694 Color: 1
Size: 282853 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 406345 Color: 1
Size: 322056 Color: 1
Size: 271600 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 438164 Color: 1
Size: 302269 Color: 1
Size: 259568 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 496243 Color: 1
Size: 253076 Color: 1
Size: 250682 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 433122 Color: 1
Size: 293606 Color: 0
Size: 273273 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 393536 Color: 1
Size: 342074 Color: 1
Size: 264391 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 439489 Color: 1
Size: 291173 Color: 0
Size: 269339 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 453976 Color: 1
Size: 293613 Color: 1
Size: 252412 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 391946 Color: 1
Size: 336619 Color: 1
Size: 271436 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 374554 Color: 1
Size: 324026 Color: 1
Size: 301421 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 431280 Color: 1
Size: 305547 Color: 1
Size: 263174 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 412024 Color: 1
Size: 324917 Color: 1
Size: 263060 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 363533 Color: 1
Size: 348137 Color: 1
Size: 288331 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 426863 Color: 1
Size: 308118 Color: 1
Size: 265020 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 416560 Color: 1
Size: 303010 Color: 1
Size: 280431 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 429232 Color: 1
Size: 300985 Color: 1
Size: 269784 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 358521 Color: 1
Size: 349497 Color: 1
Size: 291983 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 429402 Color: 1
Size: 296409 Color: 1
Size: 274190 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 467239 Color: 1
Size: 274857 Color: 1
Size: 257905 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 363971 Color: 1
Size: 324502 Color: 1
Size: 311528 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 353447 Color: 1
Size: 341836 Color: 1
Size: 304718 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 423495 Color: 1
Size: 313659 Color: 1
Size: 262847 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 404662 Color: 1
Size: 300035 Color: 1
Size: 295304 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 479882 Color: 1
Size: 267502 Color: 1
Size: 252617 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 389915 Color: 1
Size: 344484 Color: 1
Size: 265602 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 450389 Color: 1
Size: 285451 Color: 1
Size: 264161 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 489504 Color: 1
Size: 260250 Color: 1
Size: 250247 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 400250 Color: 1
Size: 329164 Color: 1
Size: 270587 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 361157 Color: 1
Size: 344374 Color: 1
Size: 294470 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 350006 Color: 1
Size: 331051 Color: 1
Size: 318944 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 489570 Color: 1
Size: 258988 Color: 0
Size: 251443 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 484265 Color: 1
Size: 264481 Color: 1
Size: 251255 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 392563 Color: 1
Size: 340695 Color: 1
Size: 266743 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 417057 Color: 1
Size: 312303 Color: 1
Size: 270641 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 385328 Color: 1
Size: 358971 Color: 1
Size: 255702 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 488765 Color: 1
Size: 258486 Color: 1
Size: 252750 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 374038 Color: 1
Size: 337090 Color: 1
Size: 288873 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 481801 Color: 1
Size: 267110 Color: 1
Size: 251090 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 413528 Color: 1
Size: 328118 Color: 1
Size: 258355 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 441969 Color: 1
Size: 285847 Color: 1
Size: 272185 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 425927 Color: 1
Size: 312134 Color: 1
Size: 261940 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 424925 Color: 1
Size: 305082 Color: 1
Size: 269994 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 362919 Color: 1
Size: 333151 Color: 1
Size: 303931 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 404053 Color: 1
Size: 307128 Color: 1
Size: 288820 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 471834 Color: 1
Size: 271156 Color: 1
Size: 257011 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 406098 Color: 1
Size: 305602 Color: 1
Size: 288301 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 493053 Color: 1
Size: 256159 Color: 1
Size: 250789 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 465719 Color: 1
Size: 274422 Color: 1
Size: 259860 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 348362 Color: 1
Size: 339127 Color: 1
Size: 312512 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 497013 Color: 1
Size: 252683 Color: 1
Size: 250305 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 399061 Color: 1
Size: 326171 Color: 1
Size: 274769 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 472441 Color: 1
Size: 269216 Color: 1
Size: 258344 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 433402 Color: 1
Size: 310615 Color: 1
Size: 255984 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 383464 Color: 1
Size: 337659 Color: 1
Size: 278878 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 417433 Color: 1
Size: 328113 Color: 1
Size: 254455 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 432661 Color: 1
Size: 290430 Color: 1
Size: 276910 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 396064 Color: 1
Size: 311413 Color: 1
Size: 292524 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 491458 Color: 1
Size: 255697 Color: 1
Size: 252846 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 435219 Color: 1
Size: 310055 Color: 1
Size: 254727 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 399186 Color: 1
Size: 315855 Color: 1
Size: 284960 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 375261 Color: 1
Size: 362982 Color: 1
Size: 261758 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 444248 Color: 1
Size: 302242 Color: 1
Size: 253511 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 498107 Color: 1
Size: 251038 Color: 0
Size: 250856 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 430643 Color: 1
Size: 293118 Color: 1
Size: 276240 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 482799 Color: 1
Size: 265205 Color: 1
Size: 251997 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 416631 Color: 1
Size: 311110 Color: 1
Size: 272260 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 465462 Color: 1
Size: 272533 Color: 1
Size: 262006 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 443911 Color: 1
Size: 290564 Color: 1
Size: 265526 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 415058 Color: 1
Size: 323605 Color: 1
Size: 261338 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 390969 Color: 1
Size: 328687 Color: 0
Size: 280345 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 467700 Color: 1
Size: 272433 Color: 0
Size: 259868 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 361333 Color: 1
Size: 345413 Color: 1
Size: 293255 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 427160 Color: 1
Size: 304002 Color: 1
Size: 268839 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 407522 Color: 1
Size: 322096 Color: 1
Size: 270383 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 441214 Color: 1
Size: 294661 Color: 1
Size: 264126 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 452798 Color: 1
Size: 286090 Color: 1
Size: 261113 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 450696 Color: 1
Size: 280239 Color: 1
Size: 269066 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 462775 Color: 1
Size: 269412 Color: 1
Size: 267814 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 356954 Color: 1
Size: 342544 Color: 1
Size: 300503 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 457177 Color: 1
Size: 287624 Color: 1
Size: 255200 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 411138 Color: 1
Size: 301077 Color: 1
Size: 287786 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 400146 Color: 1
Size: 329692 Color: 1
Size: 270163 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 399181 Color: 1
Size: 305160 Color: 0
Size: 295660 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 390762 Color: 1
Size: 347530 Color: 1
Size: 261709 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 364809 Color: 1
Size: 361452 Color: 1
Size: 273740 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 377875 Color: 1
Size: 330026 Color: 1
Size: 292100 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 437046 Color: 1
Size: 302374 Color: 1
Size: 260581 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 443181 Color: 1
Size: 300725 Color: 1
Size: 256095 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 392524 Color: 1
Size: 325676 Color: 1
Size: 281801 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 403817 Color: 1
Size: 308014 Color: 1
Size: 288170 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 484246 Color: 1
Size: 262136 Color: 1
Size: 253619 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 386237 Color: 1
Size: 314887 Color: 0
Size: 298877 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 426365 Color: 1
Size: 319217 Color: 1
Size: 254419 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 451111 Color: 1
Size: 292060 Color: 1
Size: 256830 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 459184 Color: 1
Size: 277662 Color: 1
Size: 263155 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 489514 Color: 1
Size: 259203 Color: 1
Size: 251284 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 373768 Color: 1
Size: 352063 Color: 1
Size: 274170 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 370095 Color: 1
Size: 341811 Color: 1
Size: 288095 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 405765 Color: 1
Size: 309307 Color: 1
Size: 284929 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 476732 Color: 1
Size: 270575 Color: 1
Size: 252694 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 391830 Color: 1
Size: 327605 Color: 1
Size: 280566 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 419958 Color: 1
Size: 315190 Color: 1
Size: 264853 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 439862 Color: 1
Size: 296292 Color: 1
Size: 263847 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 452255 Color: 1
Size: 290793 Color: 1
Size: 256953 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 409191 Color: 1
Size: 328964 Color: 1
Size: 261846 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 379655 Color: 1
Size: 328883 Color: 1
Size: 291463 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 399952 Color: 1
Size: 345857 Color: 1
Size: 254192 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 398657 Color: 1
Size: 327991 Color: 1
Size: 273353 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 456328 Color: 1
Size: 290562 Color: 1
Size: 253111 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 482740 Color: 1
Size: 263928 Color: 1
Size: 253333 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 479698 Color: 1
Size: 268524 Color: 1
Size: 251779 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 488943 Color: 1
Size: 258541 Color: 1
Size: 252517 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 472678 Color: 1
Size: 275073 Color: 1
Size: 252250 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 434198 Color: 1
Size: 297898 Color: 1
Size: 267905 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 375077 Color: 1
Size: 363195 Color: 1
Size: 261729 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 426898 Color: 1
Size: 291215 Color: 1
Size: 281888 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 352006 Color: 1
Size: 324315 Color: 1
Size: 323680 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 356380 Color: 1
Size: 327653 Color: 1
Size: 315968 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 384891 Color: 1
Size: 311525 Color: 0
Size: 303585 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 474175 Color: 1
Size: 266713 Color: 1
Size: 259113 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 361330 Color: 1
Size: 329148 Color: 1
Size: 309523 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 391995 Color: 1
Size: 336203 Color: 1
Size: 271803 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 354786 Color: 1
Size: 334891 Color: 1
Size: 310324 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 458332 Color: 1
Size: 285612 Color: 1
Size: 256057 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 396028 Color: 1
Size: 344790 Color: 1
Size: 259183 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 391848 Color: 1
Size: 321900 Color: 1
Size: 286253 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 394931 Color: 1
Size: 337224 Color: 1
Size: 267846 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 463339 Color: 1
Size: 279825 Color: 1
Size: 256837 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 471019 Color: 1
Size: 270078 Color: 1
Size: 258904 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 412985 Color: 1
Size: 308749 Color: 1
Size: 278267 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 444240 Color: 1
Size: 287007 Color: 1
Size: 268754 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 417992 Color: 1
Size: 320671 Color: 1
Size: 261338 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 377525 Color: 1
Size: 353893 Color: 1
Size: 268583 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 412861 Color: 1
Size: 303982 Color: 1
Size: 283158 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 397951 Color: 1
Size: 321889 Color: 1
Size: 280161 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 411087 Color: 1
Size: 329448 Color: 1
Size: 259466 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 371971 Color: 1
Size: 326686 Color: 1
Size: 301344 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 379364 Color: 1
Size: 356548 Color: 1
Size: 264089 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 460348 Color: 1
Size: 287568 Color: 1
Size: 252085 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 432961 Color: 1
Size: 307037 Color: 1
Size: 260003 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 475154 Color: 1
Size: 271413 Color: 1
Size: 253434 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 409846 Color: 1
Size: 306139 Color: 1
Size: 284016 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 421359 Color: 1
Size: 306057 Color: 1
Size: 272585 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 450707 Color: 1
Size: 295636 Color: 1
Size: 253658 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 397983 Color: 1
Size: 333762 Color: 1
Size: 268256 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 440679 Color: 1
Size: 297443 Color: 1
Size: 261879 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 427349 Color: 1
Size: 302684 Color: 1
Size: 269968 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 438325 Color: 1
Size: 284220 Color: 0
Size: 277456 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 436758 Color: 1
Size: 299402 Color: 1
Size: 263841 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 454276 Color: 1
Size: 288750 Color: 1
Size: 256975 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 416573 Color: 1
Size: 293764 Color: 1
Size: 289664 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 401766 Color: 1
Size: 334186 Color: 1
Size: 264049 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 372592 Color: 1
Size: 325460 Color: 1
Size: 301949 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 398265 Color: 1
Size: 325963 Color: 1
Size: 275773 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 406987 Color: 1
Size: 321132 Color: 1
Size: 271882 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 482336 Color: 1
Size: 266843 Color: 1
Size: 250822 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 381606 Color: 1
Size: 339911 Color: 1
Size: 278484 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 409164 Color: 1
Size: 304000 Color: 1
Size: 286837 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 404426 Color: 1
Size: 339951 Color: 1
Size: 255624 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 418073 Color: 1
Size: 320824 Color: 0
Size: 261104 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 412548 Color: 1
Size: 302850 Color: 1
Size: 284603 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 426350 Color: 1
Size: 319692 Color: 1
Size: 253959 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 348788 Color: 1
Size: 330473 Color: 1
Size: 320740 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 416810 Color: 1
Size: 306622 Color: 1
Size: 276569 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 425060 Color: 1
Size: 313178 Color: 1
Size: 261763 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 399867 Color: 1
Size: 325309 Color: 1
Size: 274825 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 384906 Color: 1
Size: 338915 Color: 1
Size: 276180 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 353062 Color: 1
Size: 351695 Color: 1
Size: 295244 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 477651 Color: 1
Size: 268986 Color: 1
Size: 253364 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 386188 Color: 1
Size: 347673 Color: 1
Size: 266140 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 422650 Color: 1
Size: 307634 Color: 1
Size: 269717 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 480917 Color: 1
Size: 260123 Color: 0
Size: 258961 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 429957 Color: 1
Size: 292369 Color: 1
Size: 277675 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 396730 Color: 1
Size: 310197 Color: 1
Size: 293074 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 432091 Color: 1
Size: 294157 Color: 1
Size: 273753 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 453349 Color: 1
Size: 284206 Color: 1
Size: 262446 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 423458 Color: 1
Size: 305450 Color: 1
Size: 271093 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 465405 Color: 1
Size: 279100 Color: 1
Size: 255496 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 425720 Color: 1
Size: 289191 Color: 1
Size: 285090 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 379952 Color: 1
Size: 359724 Color: 1
Size: 260325 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 407307 Color: 1
Size: 309274 Color: 1
Size: 283420 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 380964 Color: 1
Size: 315513 Color: 0
Size: 303524 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 353281 Color: 1
Size: 337396 Color: 1
Size: 309324 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 406008 Color: 1
Size: 340137 Color: 1
Size: 253856 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 359176 Color: 1
Size: 336145 Color: 1
Size: 304680 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 476068 Color: 1
Size: 265631 Color: 1
Size: 258302 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 436755 Color: 1
Size: 303084 Color: 1
Size: 260162 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 412607 Color: 1
Size: 317166 Color: 1
Size: 270228 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 402937 Color: 1
Size: 322759 Color: 1
Size: 274305 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 409418 Color: 1
Size: 312451 Color: 1
Size: 278132 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 404820 Color: 1
Size: 336397 Color: 1
Size: 258784 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 391077 Color: 1
Size: 337629 Color: 1
Size: 271295 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 428820 Color: 1
Size: 308552 Color: 1
Size: 262629 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 445673 Color: 1
Size: 281981 Color: 1
Size: 272347 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 388815 Color: 1
Size: 335972 Color: 1
Size: 275214 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 370117 Color: 1
Size: 338278 Color: 1
Size: 291606 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 367887 Color: 1
Size: 359320 Color: 1
Size: 272794 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 369265 Color: 1
Size: 348111 Color: 1
Size: 282625 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 367540 Color: 1
Size: 359505 Color: 1
Size: 272956 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 427122 Color: 1
Size: 292001 Color: 0
Size: 280878 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 382543 Color: 1
Size: 353102 Color: 1
Size: 264356 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 498226 Color: 1
Size: 251641 Color: 1
Size: 250134 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 494015 Color: 1
Size: 255887 Color: 1
Size: 250099 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 380265 Color: 1
Size: 320806 Color: 0
Size: 298930 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 470128 Color: 1
Size: 279425 Color: 1
Size: 250448 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 382638 Color: 1
Size: 344595 Color: 1
Size: 272768 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 370308 Color: 1
Size: 343443 Color: 1
Size: 286250 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 401567 Color: 1
Size: 314820 Color: 1
Size: 283614 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 381950 Color: 1
Size: 350634 Color: 1
Size: 267417 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 417681 Color: 1
Size: 325781 Color: 1
Size: 256539 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 389233 Color: 1
Size: 334053 Color: 1
Size: 276715 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 402489 Color: 1
Size: 304072 Color: 1
Size: 293440 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 485835 Color: 1
Size: 260921 Color: 1
Size: 253245 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 496652 Color: 1
Size: 252718 Color: 1
Size: 250631 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 358519 Color: 1
Size: 357912 Color: 1
Size: 283570 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 472438 Color: 1
Size: 273583 Color: 1
Size: 253980 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 415495 Color: 1
Size: 318525 Color: 1
Size: 265981 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 474588 Color: 1
Size: 268342 Color: 1
Size: 257071 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 434729 Color: 1
Size: 283621 Color: 0
Size: 281651 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 456169 Color: 1
Size: 292946 Color: 1
Size: 250886 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 359373 Color: 1
Size: 347211 Color: 1
Size: 293417 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 467684 Color: 1
Size: 274443 Color: 1
Size: 257874 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 378866 Color: 1
Size: 354912 Color: 1
Size: 266223 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 438143 Color: 1
Size: 305360 Color: 1
Size: 256498 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 400861 Color: 1
Size: 317392 Color: 1
Size: 281748 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 431211 Color: 1
Size: 309276 Color: 1
Size: 259514 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 498581 Color: 1
Size: 250816 Color: 1
Size: 250604 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 369408 Color: 1
Size: 318361 Color: 1
Size: 312232 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 466799 Color: 1
Size: 273149 Color: 1
Size: 260053 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 455327 Color: 1
Size: 293594 Color: 1
Size: 251080 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 454096 Color: 1
Size: 291971 Color: 1
Size: 253934 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 441607 Color: 1
Size: 283255 Color: 1
Size: 275139 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 435560 Color: 1
Size: 300688 Color: 1
Size: 263753 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 450035 Color: 1
Size: 290343 Color: 1
Size: 259623 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 405395 Color: 1
Size: 317469 Color: 1
Size: 277137 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 412074 Color: 1
Size: 308125 Color: 1
Size: 279802 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 486464 Color: 1
Size: 257596 Color: 0
Size: 255941 Color: 1

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 423344 Color: 1
Size: 311549 Color: 1
Size: 265108 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 472485 Color: 1
Size: 270831 Color: 1
Size: 256685 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 458381 Color: 1
Size: 274651 Color: 1
Size: 266969 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 445671 Color: 1
Size: 299759 Color: 1
Size: 254571 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 409267 Color: 1
Size: 325880 Color: 1
Size: 264854 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 392024 Color: 1
Size: 312797 Color: 0
Size: 295180 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 365179 Color: 1
Size: 337717 Color: 1
Size: 297105 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 384694 Color: 1
Size: 351042 Color: 1
Size: 264265 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 441508 Color: 1
Size: 298503 Color: 1
Size: 259990 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 423244 Color: 1
Size: 308910 Color: 1
Size: 267847 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 400836 Color: 1
Size: 303535 Color: 1
Size: 295630 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 447107 Color: 1
Size: 291595 Color: 1
Size: 261299 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 375530 Color: 1
Size: 347110 Color: 1
Size: 277361 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 420853 Color: 1
Size: 310465 Color: 1
Size: 268683 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 413766 Color: 1
Size: 315633 Color: 1
Size: 270602 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 363692 Color: 1
Size: 321735 Color: 1
Size: 314574 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 481759 Color: 1
Size: 263871 Color: 1
Size: 254371 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 494053 Color: 1
Size: 254628 Color: 1
Size: 251320 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 372504 Color: 1
Size: 360679 Color: 1
Size: 266818 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 462794 Color: 1
Size: 277914 Color: 1
Size: 259293 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 408888 Color: 1
Size: 329338 Color: 1
Size: 261775 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 432208 Color: 1
Size: 314164 Color: 1
Size: 253629 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 399381 Color: 1
Size: 322188 Color: 1
Size: 278432 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 495556 Color: 1
Size: 253828 Color: 1
Size: 250617 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 360374 Color: 1
Size: 344456 Color: 1
Size: 295171 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 354531 Color: 1
Size: 335581 Color: 1
Size: 309889 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 359259 Color: 1
Size: 347230 Color: 1
Size: 293512 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 439627 Color: 1
Size: 301548 Color: 1
Size: 258826 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 353088 Color: 1
Size: 349748 Color: 1
Size: 297165 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 420496 Color: 1
Size: 295074 Color: 0
Size: 284431 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 390287 Color: 1
Size: 325418 Color: 1
Size: 284296 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 384855 Color: 1
Size: 343667 Color: 1
Size: 271479 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 389687 Color: 1
Size: 312983 Color: 1
Size: 297331 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 449347 Color: 1
Size: 295979 Color: 1
Size: 254675 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 470693 Color: 1
Size: 266584 Color: 1
Size: 262724 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 445614 Color: 1
Size: 302163 Color: 1
Size: 252224 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 394352 Color: 1
Size: 347515 Color: 1
Size: 258134 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 410547 Color: 1
Size: 324198 Color: 1
Size: 265256 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 465043 Color: 1
Size: 280060 Color: 1
Size: 254898 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 390718 Color: 1
Size: 344123 Color: 1
Size: 265160 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 463005 Color: 1
Size: 284308 Color: 1
Size: 252688 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 389656 Color: 1
Size: 354617 Color: 1
Size: 255728 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 441022 Color: 1
Size: 303273 Color: 1
Size: 255706 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 445096 Color: 1
Size: 296578 Color: 1
Size: 258327 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 399685 Color: 1
Size: 326297 Color: 1
Size: 274019 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 450384 Color: 1
Size: 280005 Color: 1
Size: 269612 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 351300 Color: 1
Size: 327262 Color: 1
Size: 321439 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 398913 Color: 1
Size: 330587 Color: 1
Size: 270501 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 376707 Color: 1
Size: 334745 Color: 1
Size: 288549 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 443460 Color: 1
Size: 300140 Color: 1
Size: 256401 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 475986 Color: 1
Size: 273697 Color: 1
Size: 250318 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 382100 Color: 1
Size: 325668 Color: 1
Size: 292233 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 425722 Color: 1
Size: 307945 Color: 1
Size: 266334 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 420968 Color: 1
Size: 310359 Color: 1
Size: 268674 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 375646 Color: 1
Size: 359882 Color: 1
Size: 264473 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 410469 Color: 1
Size: 304802 Color: 1
Size: 284730 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 413003 Color: 1
Size: 307844 Color: 1
Size: 279154 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 350086 Color: 1
Size: 344846 Color: 1
Size: 305069 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 436204 Color: 1
Size: 298357 Color: 1
Size: 265440 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 397426 Color: 1
Size: 318449 Color: 0
Size: 284126 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 445018 Color: 1
Size: 300401 Color: 1
Size: 254582 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 401811 Color: 1
Size: 332271 Color: 1
Size: 265919 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 402513 Color: 1
Size: 312832 Color: 1
Size: 284656 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 404735 Color: 1
Size: 304959 Color: 1
Size: 290307 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 357923 Color: 1
Size: 351131 Color: 1
Size: 290947 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 421047 Color: 1
Size: 317021 Color: 1
Size: 261933 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 457580 Color: 1
Size: 278034 Color: 1
Size: 264387 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 383627 Color: 1
Size: 336780 Color: 1
Size: 279594 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 353394 Color: 1
Size: 348041 Color: 1
Size: 298566 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 378808 Color: 1
Size: 351714 Color: 1
Size: 269479 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 463657 Color: 1
Size: 284229 Color: 1
Size: 252115 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 416242 Color: 1
Size: 327609 Color: 1
Size: 256150 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 369058 Color: 1
Size: 324584 Color: 1
Size: 306359 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 415572 Color: 1
Size: 299274 Color: 0
Size: 285155 Color: 1

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 449992 Color: 1
Size: 295733 Color: 1
Size: 254276 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 411436 Color: 1
Size: 304666 Color: 1
Size: 283899 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 459825 Color: 1
Size: 284502 Color: 1
Size: 255674 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 451987 Color: 1
Size: 293627 Color: 1
Size: 254387 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 391633 Color: 1
Size: 348042 Color: 1
Size: 260326 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 400231 Color: 1
Size: 345565 Color: 1
Size: 254205 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 436811 Color: 1
Size: 281714 Color: 0
Size: 281476 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 449468 Color: 1
Size: 287743 Color: 1
Size: 262790 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 399809 Color: 1
Size: 324202 Color: 1
Size: 275990 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 392267 Color: 1
Size: 342465 Color: 1
Size: 265269 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 375961 Color: 1
Size: 363121 Color: 1
Size: 260919 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 422696 Color: 1
Size: 293330 Color: 1
Size: 283975 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 425201 Color: 1
Size: 287702 Color: 0
Size: 287098 Color: 1

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 492633 Color: 1
Size: 254013 Color: 0
Size: 253355 Color: 1

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 420665 Color: 1
Size: 316856 Color: 1
Size: 262480 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 446945 Color: 1
Size: 279372 Color: 0
Size: 273684 Color: 1

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 373483 Color: 1
Size: 336767 Color: 1
Size: 289751 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 375813 Color: 1
Size: 356662 Color: 1
Size: 267526 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 461343 Color: 1
Size: 282506 Color: 1
Size: 256152 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 438739 Color: 1
Size: 291046 Color: 0
Size: 270216 Color: 1

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 433698 Color: 1
Size: 309006 Color: 1
Size: 257297 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 369785 Color: 1
Size: 353397 Color: 1
Size: 276819 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 379224 Color: 1
Size: 327624 Color: 0
Size: 293153 Color: 1

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 367730 Color: 1
Size: 336161 Color: 1
Size: 296110 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 446284 Color: 1
Size: 297914 Color: 1
Size: 255803 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 409421 Color: 1
Size: 308162 Color: 1
Size: 282418 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 393896 Color: 1
Size: 306007 Color: 1
Size: 300098 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 435033 Color: 1
Size: 308959 Color: 1
Size: 256009 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 373952 Color: 1
Size: 346223 Color: 1
Size: 279826 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 392970 Color: 1
Size: 350807 Color: 1
Size: 256224 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 485495 Color: 1
Size: 262044 Color: 1
Size: 252462 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 391849 Color: 1
Size: 314228 Color: 0
Size: 293924 Color: 1

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 388037 Color: 1
Size: 342422 Color: 1
Size: 269542 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 441570 Color: 1
Size: 305180 Color: 1
Size: 253251 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 401054 Color: 1
Size: 339367 Color: 1
Size: 259580 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 441030 Color: 1
Size: 304246 Color: 1
Size: 254725 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 491480 Color: 1
Size: 255056 Color: 1
Size: 253465 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 414405 Color: 1
Size: 321260 Color: 1
Size: 264336 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 446950 Color: 1
Size: 283737 Color: 1
Size: 269314 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 479555 Color: 1
Size: 265901 Color: 1
Size: 254545 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 377513 Color: 1
Size: 321798 Color: 1
Size: 300690 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 405543 Color: 1
Size: 297904 Color: 0
Size: 296554 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 450121 Color: 1
Size: 296707 Color: 1
Size: 253173 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 437662 Color: 1
Size: 307367 Color: 1
Size: 254972 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 466210 Color: 1
Size: 279625 Color: 1
Size: 254166 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 409114 Color: 1
Size: 322157 Color: 0
Size: 268730 Color: 1

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 341864 Color: 1
Size: 331801 Color: 1
Size: 326336 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 446924 Color: 1
Size: 298608 Color: 1
Size: 254469 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 466179 Color: 1
Size: 270244 Color: 1
Size: 263578 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 421249 Color: 1
Size: 318915 Color: 1
Size: 259837 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 406906 Color: 1
Size: 312577 Color: 1
Size: 280518 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 469263 Color: 1
Size: 272858 Color: 1
Size: 257880 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 373464 Color: 1
Size: 365561 Color: 1
Size: 260976 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 380771 Color: 1
Size: 357967 Color: 1
Size: 261263 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 385957 Color: 1
Size: 329797 Color: 1
Size: 284247 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 421048 Color: 1
Size: 301684 Color: 1
Size: 277269 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 364663 Color: 1
Size: 340099 Color: 1
Size: 295239 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 391825 Color: 1
Size: 326945 Color: 1
Size: 281231 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 455250 Color: 1
Size: 274297 Color: 1
Size: 270454 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 476671 Color: 1
Size: 270159 Color: 1
Size: 253171 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 471395 Color: 1
Size: 268547 Color: 1
Size: 260059 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 491672 Color: 1
Size: 257021 Color: 1
Size: 251308 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 468797 Color: 1
Size: 277980 Color: 1
Size: 253224 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 360918 Color: 1
Size: 334177 Color: 1
Size: 304906 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 412914 Color: 1
Size: 332517 Color: 1
Size: 254570 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 399332 Color: 1
Size: 317531 Color: 1
Size: 283138 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 383465 Color: 1
Size: 332305 Color: 1
Size: 284231 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 465893 Color: 1
Size: 280907 Color: 1
Size: 253201 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 498214 Color: 1
Size: 251548 Color: 1
Size: 250239 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 478774 Color: 1
Size: 269717 Color: 1
Size: 251510 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 442975 Color: 1
Size: 280460 Color: 0
Size: 276566 Color: 1

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 410924 Color: 1
Size: 305441 Color: 1
Size: 283636 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 387286 Color: 1
Size: 334387 Color: 1
Size: 278328 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 395617 Color: 1
Size: 337211 Color: 1
Size: 267173 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 391827 Color: 1
Size: 333539 Color: 1
Size: 274635 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 372917 Color: 1
Size: 327771 Color: 1
Size: 299313 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 403898 Color: 1
Size: 299521 Color: 1
Size: 296582 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 453501 Color: 1
Size: 289694 Color: 1
Size: 256806 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 458589 Color: 1
Size: 286727 Color: 1
Size: 254685 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 481854 Color: 1
Size: 263066 Color: 1
Size: 255081 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 447713 Color: 1
Size: 297190 Color: 1
Size: 255098 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 401973 Color: 1
Size: 325551 Color: 1
Size: 272477 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 353361 Color: 1
Size: 344476 Color: 1
Size: 302164 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 358333 Color: 1
Size: 324973 Color: 0
Size: 316695 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 406707 Color: 1
Size: 326217 Color: 1
Size: 267077 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 405787 Color: 1
Size: 320060 Color: 1
Size: 274154 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 493773 Color: 1
Size: 253464 Color: 1
Size: 252764 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 499350 Color: 1
Size: 250473 Color: 1
Size: 250178 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 473376 Color: 1
Size: 275617 Color: 1
Size: 251008 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 412766 Color: 1
Size: 323674 Color: 1
Size: 263561 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 468077 Color: 1
Size: 276691 Color: 1
Size: 255233 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 384629 Color: 1
Size: 330247 Color: 1
Size: 285125 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 399763 Color: 1
Size: 305020 Color: 1
Size: 295218 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 403020 Color: 1
Size: 330377 Color: 1
Size: 266604 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 357419 Color: 1
Size: 347357 Color: 1
Size: 295225 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 448382 Color: 1
Size: 290751 Color: 0
Size: 260868 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 414527 Color: 1
Size: 299902 Color: 1
Size: 285572 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 362512 Color: 1
Size: 343997 Color: 1
Size: 293492 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 389307 Color: 1
Size: 317797 Color: 1
Size: 292897 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 413554 Color: 1
Size: 309971 Color: 1
Size: 276476 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 457487 Color: 1
Size: 280728 Color: 1
Size: 261786 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 378224 Color: 1
Size: 331432 Color: 1
Size: 290345 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 419480 Color: 1
Size: 310875 Color: 1
Size: 269646 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 428457 Color: 1
Size: 314957 Color: 1
Size: 256587 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 439462 Color: 1
Size: 287920 Color: 1
Size: 272619 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 493807 Color: 1
Size: 254958 Color: 1
Size: 251236 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 425353 Color: 1
Size: 308131 Color: 1
Size: 266517 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 362600 Color: 1
Size: 320574 Color: 0
Size: 316827 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 462319 Color: 1
Size: 273318 Color: 1
Size: 264364 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 393889 Color: 1
Size: 342457 Color: 1
Size: 263655 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 368005 Color: 1
Size: 333688 Color: 1
Size: 298308 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 360545 Color: 1
Size: 350294 Color: 1
Size: 289162 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 448737 Color: 1
Size: 297004 Color: 1
Size: 254260 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 430529 Color: 1
Size: 314998 Color: 1
Size: 254474 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 489852 Color: 1
Size: 259234 Color: 1
Size: 250915 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 396510 Color: 1
Size: 318641 Color: 0
Size: 284850 Color: 1

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 393438 Color: 1
Size: 344299 Color: 1
Size: 262264 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 498753 Color: 1
Size: 250845 Color: 1
Size: 250403 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 437021 Color: 1
Size: 298916 Color: 1
Size: 264064 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 455601 Color: 1
Size: 284473 Color: 1
Size: 259927 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 361056 Color: 1
Size: 359777 Color: 1
Size: 279168 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 364921 Color: 1
Size: 364802 Color: 1
Size: 270278 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 417993 Color: 1
Size: 296616 Color: 0
Size: 285392 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 377839 Color: 1
Size: 357297 Color: 1
Size: 264865 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 425756 Color: 1
Size: 306979 Color: 1
Size: 267266 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 446632 Color: 1
Size: 291898 Color: 1
Size: 261471 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 382526 Color: 1
Size: 361224 Color: 1
Size: 256251 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 422497 Color: 1
Size: 326434 Color: 1
Size: 251070 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 489350 Color: 1
Size: 257179 Color: 1
Size: 253472 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 446432 Color: 1
Size: 284319 Color: 1
Size: 269250 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 487014 Color: 1
Size: 262680 Color: 1
Size: 250307 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 497395 Color: 1
Size: 252085 Color: 1
Size: 250521 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 373790 Color: 1
Size: 339074 Color: 1
Size: 287137 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 377775 Color: 1
Size: 338361 Color: 1
Size: 283865 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 433506 Color: 1
Size: 300151 Color: 1
Size: 266344 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 407817 Color: 1
Size: 337992 Color: 1
Size: 254192 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 472451 Color: 1
Size: 271010 Color: 1
Size: 256540 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 370675 Color: 1
Size: 348113 Color: 1
Size: 281213 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 398278 Color: 1
Size: 312532 Color: 0
Size: 289191 Color: 1

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 373313 Color: 1
Size: 342756 Color: 1
Size: 283932 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 430894 Color: 1
Size: 313259 Color: 1
Size: 255848 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 429908 Color: 1
Size: 301411 Color: 1
Size: 268682 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 424606 Color: 1
Size: 309008 Color: 1
Size: 266387 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 411127 Color: 1
Size: 316572 Color: 1
Size: 272302 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 388877 Color: 1
Size: 347514 Color: 1
Size: 263610 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 438128 Color: 1
Size: 307545 Color: 1
Size: 254328 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 375091 Color: 1
Size: 330187 Color: 1
Size: 294723 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 405431 Color: 1
Size: 315623 Color: 1
Size: 278947 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 494113 Color: 1
Size: 255355 Color: 1
Size: 250533 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 473320 Color: 1
Size: 270805 Color: 1
Size: 255876 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 471464 Color: 1
Size: 268927 Color: 1
Size: 259610 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 485237 Color: 1
Size: 261956 Color: 1
Size: 252808 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 463255 Color: 1
Size: 284467 Color: 1
Size: 252279 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 424835 Color: 1
Size: 316645 Color: 1
Size: 258521 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 456819 Color: 1
Size: 281563 Color: 1
Size: 261619 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 424252 Color: 1
Size: 301618 Color: 1
Size: 274131 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 418884 Color: 1
Size: 313439 Color: 1
Size: 267678 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 392542 Color: 1
Size: 317204 Color: 1
Size: 290255 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 367532 Color: 1
Size: 330531 Color: 1
Size: 301938 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 452726 Color: 1
Size: 281257 Color: 1
Size: 266018 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 364870 Color: 1
Size: 363582 Color: 1
Size: 271549 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 469956 Color: 1
Size: 268882 Color: 1
Size: 261163 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 454067 Color: 1
Size: 291734 Color: 1
Size: 254200 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 452743 Color: 1
Size: 285590 Color: 1
Size: 261668 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 401163 Color: 1
Size: 329492 Color: 1
Size: 269346 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 467378 Color: 1
Size: 275897 Color: 1
Size: 256726 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 435932 Color: 1
Size: 308194 Color: 1
Size: 255875 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 399453 Color: 1
Size: 317440 Color: 1
Size: 283108 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 474212 Color: 1
Size: 266954 Color: 0
Size: 258835 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 406030 Color: 1
Size: 334156 Color: 1
Size: 259815 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 453758 Color: 1
Size: 283580 Color: 1
Size: 262663 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 381289 Color: 1
Size: 360222 Color: 1
Size: 258490 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 380922 Color: 1
Size: 332285 Color: 1
Size: 286794 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 444892 Color: 1
Size: 301645 Color: 1
Size: 253464 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 354048 Color: 1
Size: 352666 Color: 1
Size: 293287 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 433938 Color: 1
Size: 295123 Color: 1
Size: 270940 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 476493 Color: 1
Size: 270745 Color: 1
Size: 252763 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 387222 Color: 1
Size: 312735 Color: 1
Size: 300044 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 403845 Color: 1
Size: 311046 Color: 1
Size: 285110 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 408343 Color: 1
Size: 323042 Color: 1
Size: 268616 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 490926 Color: 1
Size: 258464 Color: 1
Size: 250611 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 389176 Color: 1
Size: 326662 Color: 1
Size: 284163 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 478008 Color: 1
Size: 270221 Color: 1
Size: 251772 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 408506 Color: 1
Size: 326247 Color: 1
Size: 265248 Color: 0

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 492079 Color: 1
Size: 254467 Color: 1
Size: 253455 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 492956 Color: 1
Size: 253626 Color: 0
Size: 253419 Color: 1

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 471352 Color: 1
Size: 270188 Color: 1
Size: 258461 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 456248 Color: 1
Size: 285693 Color: 1
Size: 258060 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 355083 Color: 1
Size: 328895 Color: 1
Size: 316023 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 399969 Color: 1
Size: 335733 Color: 1
Size: 264299 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 498393 Color: 1
Size: 251284 Color: 1
Size: 250324 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 388501 Color: 1
Size: 344176 Color: 1
Size: 267324 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 454829 Color: 1
Size: 286925 Color: 1
Size: 258247 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 451718 Color: 1
Size: 289709 Color: 1
Size: 258574 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 451328 Color: 1
Size: 291228 Color: 1
Size: 257445 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 462192 Color: 1
Size: 278588 Color: 1
Size: 259221 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 458489 Color: 1
Size: 277072 Color: 1
Size: 264440 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 472829 Color: 1
Size: 272059 Color: 1
Size: 255113 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 411158 Color: 1
Size: 333601 Color: 1
Size: 255242 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 373593 Color: 1
Size: 364315 Color: 1
Size: 262093 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 490095 Color: 1
Size: 256314 Color: 1
Size: 253592 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 414379 Color: 1
Size: 310884 Color: 1
Size: 274738 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 433096 Color: 1
Size: 303766 Color: 1
Size: 263139 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 395724 Color: 1
Size: 321795 Color: 1
Size: 282482 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 457534 Color: 1
Size: 271472 Color: 0
Size: 270995 Color: 1

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 368238 Color: 1
Size: 340125 Color: 1
Size: 291638 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 495300 Color: 1
Size: 254007 Color: 0
Size: 250694 Color: 1

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 428089 Color: 1
Size: 302344 Color: 1
Size: 269568 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 390240 Color: 1
Size: 344226 Color: 1
Size: 265535 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 472295 Color: 1
Size: 274937 Color: 1
Size: 252769 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 389374 Color: 1
Size: 357901 Color: 1
Size: 252726 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 458004 Color: 1
Size: 288135 Color: 1
Size: 253862 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 383291 Color: 1
Size: 335512 Color: 1
Size: 281198 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 457694 Color: 1
Size: 285694 Color: 1
Size: 256613 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 424328 Color: 1
Size: 305569 Color: 1
Size: 270104 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 429870 Color: 1
Size: 287962 Color: 0
Size: 282169 Color: 1

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 398304 Color: 1
Size: 312382 Color: 1
Size: 289315 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 440872 Color: 1
Size: 298292 Color: 1
Size: 260837 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 409504 Color: 1
Size: 308488 Color: 1
Size: 282009 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 422003 Color: 1
Size: 304300 Color: 1
Size: 273698 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 397268 Color: 1
Size: 340775 Color: 1
Size: 261958 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 394845 Color: 1
Size: 352725 Color: 1
Size: 252431 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 459197 Color: 1
Size: 287903 Color: 1
Size: 252901 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 405405 Color: 1
Size: 315679 Color: 0
Size: 278917 Color: 1

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 421038 Color: 1
Size: 306072 Color: 1
Size: 272891 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 408080 Color: 1
Size: 307736 Color: 1
Size: 284185 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 441969 Color: 1
Size: 297867 Color: 1
Size: 260165 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 444251 Color: 1
Size: 296828 Color: 1
Size: 258922 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 485499 Color: 1
Size: 261570 Color: 1
Size: 252932 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 475456 Color: 1
Size: 274164 Color: 1
Size: 250381 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 445835 Color: 1
Size: 297527 Color: 1
Size: 256639 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 414983 Color: 1
Size: 324039 Color: 1
Size: 260979 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 484018 Color: 1
Size: 262682 Color: 1
Size: 253301 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 378682 Color: 1
Size: 364810 Color: 1
Size: 256509 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 435649 Color: 1
Size: 292752 Color: 1
Size: 271600 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 464310 Color: 1
Size: 283701 Color: 1
Size: 251990 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 371768 Color: 1
Size: 350052 Color: 1
Size: 278181 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 349620 Color: 1
Size: 331899 Color: 1
Size: 318482 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 497385 Color: 1
Size: 252165 Color: 1
Size: 250451 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 348068 Color: 1
Size: 333188 Color: 1
Size: 318745 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 364528 Color: 1
Size: 320636 Color: 1
Size: 314837 Color: 0

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 356293 Color: 1
Size: 343161 Color: 1
Size: 300547 Color: 0

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 452004 Color: 1
Size: 277924 Color: 1
Size: 270073 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 385496 Color: 1
Size: 352759 Color: 1
Size: 261746 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 418169 Color: 1
Size: 312856 Color: 1
Size: 268976 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 420745 Color: 1
Size: 308233 Color: 1
Size: 271023 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 404132 Color: 1
Size: 316564 Color: 1
Size: 279305 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 435589 Color: 1
Size: 300522 Color: 1
Size: 263890 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 405731 Color: 1
Size: 320031 Color: 1
Size: 274239 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 348452 Color: 1
Size: 327159 Color: 1
Size: 324390 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 411222 Color: 1
Size: 328345 Color: 1
Size: 260434 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 430489 Color: 1
Size: 289642 Color: 0
Size: 279870 Color: 1

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 416501 Color: 1
Size: 292879 Color: 0
Size: 290621 Color: 1

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 459747 Color: 1
Size: 282764 Color: 1
Size: 257490 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 363290 Color: 1
Size: 343540 Color: 1
Size: 293171 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 424906 Color: 1
Size: 323154 Color: 1
Size: 251941 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 482785 Color: 1
Size: 258848 Color: 0
Size: 258368 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 372625 Color: 1
Size: 332403 Color: 1
Size: 294973 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 404373 Color: 1
Size: 306710 Color: 1
Size: 288918 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 442806 Color: 1
Size: 300196 Color: 1
Size: 256999 Color: 0

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 424894 Color: 1
Size: 315230 Color: 1
Size: 259877 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 455789 Color: 1
Size: 280316 Color: 1
Size: 263896 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 383475 Color: 1
Size: 331590 Color: 1
Size: 284936 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 455187 Color: 1
Size: 292237 Color: 1
Size: 252577 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 424420 Color: 1
Size: 302571 Color: 0
Size: 273010 Color: 1

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 397777 Color: 1
Size: 306539 Color: 0
Size: 295685 Color: 1

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 493838 Color: 1
Size: 253800 Color: 1
Size: 252363 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 396782 Color: 1
Size: 319997 Color: 1
Size: 283222 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 451660 Color: 1
Size: 282104 Color: 1
Size: 266237 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 400461 Color: 1
Size: 326100 Color: 1
Size: 273440 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 380842 Color: 1
Size: 310226 Color: 0
Size: 308933 Color: 1

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 472174 Color: 1
Size: 271994 Color: 1
Size: 255833 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 422255 Color: 1
Size: 292795 Color: 1
Size: 284951 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 383831 Color: 1
Size: 361667 Color: 1
Size: 254503 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 365047 Color: 1
Size: 319509 Color: 1
Size: 315445 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 437945 Color: 1
Size: 301276 Color: 1
Size: 260780 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 452496 Color: 1
Size: 284906 Color: 1
Size: 262599 Color: 0

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 391639 Color: 1
Size: 330268 Color: 1
Size: 278094 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 364289 Color: 1
Size: 322305 Color: 0
Size: 313407 Color: 1

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 411665 Color: 1
Size: 316216 Color: 1
Size: 272120 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 389559 Color: 1
Size: 310557 Color: 1
Size: 299885 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 369677 Color: 1
Size: 345840 Color: 1
Size: 284484 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 407719 Color: 1
Size: 334508 Color: 1
Size: 257774 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 409891 Color: 1
Size: 329679 Color: 1
Size: 260431 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 457896 Color: 1
Size: 278610 Color: 1
Size: 263495 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 346835 Color: 1
Size: 337802 Color: 1
Size: 315364 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 374673 Color: 1
Size: 362707 Color: 1
Size: 262621 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 355243 Color: 1
Size: 351915 Color: 1
Size: 292843 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 376465 Color: 1
Size: 364761 Color: 1
Size: 258775 Color: 0

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 355015 Color: 1
Size: 347283 Color: 1
Size: 297703 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 447736 Color: 1
Size: 292519 Color: 1
Size: 259746 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 445862 Color: 1
Size: 301114 Color: 1
Size: 253025 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 436722 Color: 1
Size: 300227 Color: 1
Size: 263052 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 435993 Color: 1
Size: 287171 Color: 0
Size: 276837 Color: 1

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 447655 Color: 1
Size: 295906 Color: 1
Size: 256440 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 381560 Color: 1
Size: 352781 Color: 1
Size: 265660 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 448775 Color: 1
Size: 299372 Color: 1
Size: 251854 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 370128 Color: 1
Size: 323817 Color: 1
Size: 306056 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 485687 Color: 1
Size: 262852 Color: 1
Size: 251462 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 365372 Color: 1
Size: 339561 Color: 1
Size: 295068 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 473848 Color: 1
Size: 274851 Color: 1
Size: 251302 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 456668 Color: 1
Size: 290560 Color: 1
Size: 252773 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 473138 Color: 1
Size: 267804 Color: 1
Size: 259059 Color: 0

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 349337 Color: 1
Size: 328704 Color: 0
Size: 321960 Color: 1

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 406768 Color: 1
Size: 312487 Color: 1
Size: 280746 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 372023 Color: 1
Size: 335769 Color: 1
Size: 292209 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 405175 Color: 1
Size: 321779 Color: 1
Size: 273047 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 454504 Color: 1
Size: 276388 Color: 1
Size: 269109 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 389796 Color: 1
Size: 313472 Color: 0
Size: 296733 Color: 1

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 375303 Color: 1
Size: 315302 Color: 1
Size: 309396 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 362657 Color: 1
Size: 352993 Color: 1
Size: 284351 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 471159 Color: 1
Size: 265325 Color: 1
Size: 263517 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 439920 Color: 1
Size: 283380 Color: 1
Size: 276701 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 412331 Color: 1
Size: 308022 Color: 1
Size: 279648 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 400319 Color: 1
Size: 306873 Color: 1
Size: 292809 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 379630 Color: 1
Size: 325674 Color: 1
Size: 294697 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 486467 Color: 1
Size: 262100 Color: 1
Size: 251434 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 440746 Color: 1
Size: 305148 Color: 1
Size: 254107 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 432748 Color: 1
Size: 313170 Color: 1
Size: 254083 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 493634 Color: 1
Size: 253927 Color: 1
Size: 252440 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 421321 Color: 1
Size: 325988 Color: 1
Size: 252692 Color: 0

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 447429 Color: 1
Size: 296367 Color: 1
Size: 256205 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 369498 Color: 1
Size: 356838 Color: 1
Size: 273665 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 375086 Color: 1
Size: 342174 Color: 1
Size: 282741 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 483885 Color: 1
Size: 260546 Color: 1
Size: 255570 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 390102 Color: 1
Size: 327319 Color: 1
Size: 282580 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 397185 Color: 1
Size: 338743 Color: 1
Size: 264073 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 494241 Color: 1
Size: 255337 Color: 1
Size: 250423 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 377558 Color: 1
Size: 359617 Color: 1
Size: 262826 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 400423 Color: 1
Size: 315422 Color: 1
Size: 284156 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 489771 Color: 1
Size: 257761 Color: 1
Size: 252469 Color: 0

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 471397 Color: 1
Size: 267097 Color: 0
Size: 261507 Color: 1

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 423856 Color: 1
Size: 310139 Color: 1
Size: 266006 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 379959 Color: 1
Size: 365674 Color: 1
Size: 254368 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 460658 Color: 1
Size: 285282 Color: 1
Size: 254061 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 462231 Color: 1
Size: 280806 Color: 1
Size: 256964 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 412293 Color: 1
Size: 337577 Color: 1
Size: 250131 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 465805 Color: 1
Size: 279266 Color: 1
Size: 254930 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 492914 Color: 1
Size: 255565 Color: 1
Size: 251522 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 412823 Color: 1
Size: 327623 Color: 1
Size: 259555 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 445128 Color: 1
Size: 291730 Color: 0
Size: 263143 Color: 1

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 481101 Color: 1
Size: 261848 Color: 1
Size: 257052 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 364722 Color: 1
Size: 342130 Color: 1
Size: 293149 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 424505 Color: 1
Size: 297261 Color: 1
Size: 278235 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 350269 Color: 1
Size: 348584 Color: 1
Size: 301148 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 397125 Color: 1
Size: 325512 Color: 1
Size: 277364 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 386888 Color: 1
Size: 347720 Color: 1
Size: 265393 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 488152 Color: 1
Size: 259555 Color: 0
Size: 252294 Color: 1

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 369355 Color: 1
Size: 329422 Color: 1
Size: 301224 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 407850 Color: 1
Size: 305089 Color: 0
Size: 287062 Color: 1

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 401777 Color: 1
Size: 323923 Color: 1
Size: 274301 Color: 0

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 458390 Color: 1
Size: 274690 Color: 1
Size: 266921 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 475165 Color: 1
Size: 273110 Color: 1
Size: 251726 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 402425 Color: 1
Size: 318589 Color: 1
Size: 278987 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 349018 Color: 1
Size: 346922 Color: 1
Size: 304061 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 487183 Color: 1
Size: 258896 Color: 1
Size: 253922 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 410409 Color: 1
Size: 319483 Color: 1
Size: 270109 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 454668 Color: 1
Size: 274043 Color: 1
Size: 271290 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 404197 Color: 1
Size: 318399 Color: 1
Size: 277405 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 395875 Color: 1
Size: 307077 Color: 1
Size: 297049 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 382772 Color: 1
Size: 310677 Color: 1
Size: 306552 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 348734 Color: 1
Size: 338473 Color: 1
Size: 312794 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 366215 Color: 1
Size: 350675 Color: 1
Size: 283111 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 453506 Color: 1
Size: 291434 Color: 1
Size: 255061 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 376501 Color: 1
Size: 338339 Color: 1
Size: 285161 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 463649 Color: 1
Size: 282699 Color: 1
Size: 253653 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 445174 Color: 1
Size: 302636 Color: 1
Size: 252191 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 420326 Color: 1
Size: 308777 Color: 1
Size: 270898 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 358771 Color: 1
Size: 353440 Color: 1
Size: 287790 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 454154 Color: 1
Size: 277985 Color: 1
Size: 267862 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 415448 Color: 1
Size: 298970 Color: 0
Size: 285583 Color: 1

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 491169 Color: 1
Size: 257983 Color: 0
Size: 250849 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 447675 Color: 1
Size: 300660 Color: 1
Size: 251666 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 475482 Color: 1
Size: 270163 Color: 1
Size: 254356 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 428364 Color: 1
Size: 317662 Color: 1
Size: 253975 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 488497 Color: 1
Size: 259376 Color: 1
Size: 252128 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 428847 Color: 1
Size: 310488 Color: 1
Size: 260666 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 385403 Color: 1
Size: 344524 Color: 1
Size: 270074 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 376035 Color: 1
Size: 349608 Color: 1
Size: 274358 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 379841 Color: 1
Size: 329555 Color: 1
Size: 290605 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 396370 Color: 1
Size: 329079 Color: 1
Size: 274552 Color: 0

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 499840 Color: 1
Size: 250099 Color: 1
Size: 250062 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 440739 Color: 1
Size: 290193 Color: 1
Size: 269069 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 382306 Color: 1
Size: 323098 Color: 1
Size: 294597 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 371536 Color: 1
Size: 352926 Color: 1
Size: 275539 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 401280 Color: 1
Size: 323880 Color: 1
Size: 274841 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 380720 Color: 1
Size: 338157 Color: 1
Size: 281124 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 384084 Color: 1
Size: 334032 Color: 1
Size: 281885 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 376099 Color: 1
Size: 339947 Color: 1
Size: 283955 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 419740 Color: 1
Size: 305863 Color: 1
Size: 274398 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 366651 Color: 1
Size: 318299 Color: 1
Size: 315051 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 473197 Color: 1
Size: 273735 Color: 1
Size: 253069 Color: 0

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 422423 Color: 1
Size: 308711 Color: 1
Size: 268867 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 371379 Color: 1
Size: 333709 Color: 1
Size: 294913 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 465222 Color: 1
Size: 269459 Color: 1
Size: 265320 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 439830 Color: 1
Size: 290720 Color: 1
Size: 269451 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 475920 Color: 1
Size: 266652 Color: 1
Size: 257429 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 490212 Color: 1
Size: 259131 Color: 1
Size: 250658 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 471084 Color: 1
Size: 271729 Color: 1
Size: 257188 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 382336 Color: 1
Size: 336993 Color: 1
Size: 280672 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 365360 Color: 1
Size: 358019 Color: 1
Size: 276622 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 498707 Color: 1
Size: 251287 Color: 1
Size: 250007 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 448476 Color: 1
Size: 289297 Color: 0
Size: 262228 Color: 1

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 485703 Color: 1
Size: 260403 Color: 1
Size: 253895 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 371670 Color: 1
Size: 340318 Color: 1
Size: 288013 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 474448 Color: 1
Size: 270955 Color: 1
Size: 254598 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 448135 Color: 1
Size: 280963 Color: 1
Size: 270903 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 446946 Color: 1
Size: 288913 Color: 1
Size: 264142 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 467772 Color: 1
Size: 278571 Color: 1
Size: 253658 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 367224 Color: 1
Size: 364910 Color: 1
Size: 267867 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 374456 Color: 1
Size: 364594 Color: 1
Size: 260951 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 453992 Color: 1
Size: 285459 Color: 1
Size: 260550 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 429767 Color: 1
Size: 303707 Color: 1
Size: 266527 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 479026 Color: 1
Size: 267715 Color: 1
Size: 253260 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 451972 Color: 1
Size: 294582 Color: 1
Size: 253447 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 445267 Color: 1
Size: 296175 Color: 1
Size: 258559 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 491619 Color: 1
Size: 256538 Color: 1
Size: 251844 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 356757 Color: 1
Size: 333918 Color: 1
Size: 309326 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 437950 Color: 1
Size: 296589 Color: 0
Size: 265462 Color: 1

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 401060 Color: 1
Size: 333541 Color: 1
Size: 265400 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 436493 Color: 1
Size: 286970 Color: 1
Size: 276538 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 394877 Color: 1
Size: 353221 Color: 1
Size: 251903 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 400863 Color: 1
Size: 307564 Color: 1
Size: 291574 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 371897 Color: 1
Size: 347409 Color: 1
Size: 280695 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 404399 Color: 1
Size: 342882 Color: 1
Size: 252720 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 375668 Color: 1
Size: 346916 Color: 1
Size: 277417 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 405974 Color: 1
Size: 324224 Color: 1
Size: 269803 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 422785 Color: 1
Size: 310683 Color: 1
Size: 266533 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 392376 Color: 1
Size: 328297 Color: 1
Size: 279328 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 411825 Color: 1
Size: 319568 Color: 1
Size: 268608 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 465980 Color: 1
Size: 282639 Color: 1
Size: 251382 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 411027 Color: 1
Size: 308263 Color: 1
Size: 280711 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 449838 Color: 1
Size: 294916 Color: 1
Size: 255247 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 454453 Color: 1
Size: 279574 Color: 1
Size: 265974 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 370024 Color: 1
Size: 362182 Color: 1
Size: 267795 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 415236 Color: 1
Size: 305930 Color: 1
Size: 278835 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 437662 Color: 1
Size: 300089 Color: 1
Size: 262250 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 483328 Color: 1
Size: 264875 Color: 1
Size: 251798 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 429547 Color: 1
Size: 311856 Color: 1
Size: 258598 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 346725 Color: 1
Size: 342125 Color: 1
Size: 311151 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 391098 Color: 1
Size: 321882 Color: 1
Size: 287021 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 412527 Color: 1
Size: 298669 Color: 0
Size: 288805 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 361238 Color: 1
Size: 319602 Color: 1
Size: 319161 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 420270 Color: 1
Size: 321581 Color: 1
Size: 258150 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 488688 Color: 1
Size: 260856 Color: 1
Size: 250457 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 349878 Color: 1
Size: 338325 Color: 1
Size: 311798 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 408733 Color: 1
Size: 301319 Color: 0
Size: 289949 Color: 1

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 495208 Color: 1
Size: 254240 Color: 1
Size: 250553 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 499708 Color: 1
Size: 250174 Color: 1
Size: 250119 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 367747 Color: 1
Size: 355923 Color: 1
Size: 276331 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 341547 Color: 1
Size: 335387 Color: 1
Size: 323067 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 387456 Color: 1
Size: 321506 Color: 1
Size: 291039 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 485296 Color: 1
Size: 260465 Color: 1
Size: 254240 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 476308 Color: 1
Size: 269802 Color: 1
Size: 253891 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 486409 Color: 1
Size: 260721 Color: 1
Size: 252871 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 482128 Color: 1
Size: 260389 Color: 1
Size: 257484 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 475787 Color: 1
Size: 265466 Color: 1
Size: 258748 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 433988 Color: 1
Size: 303313 Color: 1
Size: 262700 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 408566 Color: 1
Size: 334961 Color: 1
Size: 256474 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 490764 Color: 1
Size: 255987 Color: 1
Size: 253250 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 405520 Color: 1
Size: 342126 Color: 1
Size: 252355 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 470177 Color: 1
Size: 273515 Color: 1
Size: 256309 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 442644 Color: 1
Size: 282271 Color: 1
Size: 275086 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 396995 Color: 1
Size: 332718 Color: 1
Size: 270288 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 355566 Color: 1
Size: 333195 Color: 1
Size: 311240 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 365539 Color: 1
Size: 348541 Color: 1
Size: 285921 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 429465 Color: 1
Size: 302709 Color: 1
Size: 267827 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 464527 Color: 1
Size: 270995 Color: 1
Size: 264479 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 489519 Color: 1
Size: 260361 Color: 1
Size: 250121 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 401328 Color: 1
Size: 312121 Color: 0
Size: 286552 Color: 1

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 386374 Color: 1
Size: 328183 Color: 0
Size: 285444 Color: 1

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 369676 Color: 1
Size: 354272 Color: 1
Size: 276053 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 373103 Color: 1
Size: 337515 Color: 1
Size: 289383 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 357312 Color: 1
Size: 337167 Color: 1
Size: 305522 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 495006 Color: 1
Size: 253720 Color: 1
Size: 251275 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 493225 Color: 1
Size: 255689 Color: 1
Size: 251087 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 452804 Color: 1
Size: 294939 Color: 1
Size: 252258 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 437243 Color: 1
Size: 309750 Color: 1
Size: 253008 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 436551 Color: 1
Size: 305325 Color: 1
Size: 258125 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 390312 Color: 1
Size: 332873 Color: 1
Size: 276816 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 422188 Color: 1
Size: 304728 Color: 0
Size: 273085 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 407888 Color: 1
Size: 301584 Color: 1
Size: 290529 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 488736 Color: 1
Size: 260895 Color: 1
Size: 250370 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 494427 Color: 1
Size: 254948 Color: 1
Size: 250626 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 483683 Color: 1
Size: 263889 Color: 1
Size: 252429 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 416545 Color: 1
Size: 319839 Color: 1
Size: 263617 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 386613 Color: 1
Size: 328945 Color: 1
Size: 284443 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 395809 Color: 1
Size: 326763 Color: 1
Size: 277429 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 463373 Color: 1
Size: 271760 Color: 1
Size: 264868 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 456197 Color: 1
Size: 279279 Color: 1
Size: 264525 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 363139 Color: 1
Size: 361472 Color: 1
Size: 275390 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 484256 Color: 1
Size: 263876 Color: 1
Size: 251869 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 431456 Color: 1
Size: 303943 Color: 1
Size: 264602 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 449323 Color: 1
Size: 293240 Color: 1
Size: 257438 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 464747 Color: 1
Size: 284375 Color: 1
Size: 250879 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 422884 Color: 1
Size: 307824 Color: 1
Size: 269293 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 376546 Color: 1
Size: 360995 Color: 1
Size: 262460 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 372282 Color: 1
Size: 335184 Color: 1
Size: 292535 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 462199 Color: 1
Size: 286190 Color: 1
Size: 251612 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 369262 Color: 1
Size: 321416 Color: 1
Size: 309323 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 431711 Color: 1
Size: 317027 Color: 1
Size: 251263 Color: 0

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 389725 Color: 1
Size: 309210 Color: 0
Size: 301066 Color: 1

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 361052 Color: 1
Size: 344985 Color: 1
Size: 293964 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 487337 Color: 1
Size: 260622 Color: 1
Size: 252042 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 354739 Color: 1
Size: 327333 Color: 1
Size: 317929 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 381322 Color: 1
Size: 330559 Color: 1
Size: 288120 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 472087 Color: 1
Size: 268106 Color: 1
Size: 259808 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 368034 Color: 1
Size: 363803 Color: 1
Size: 268164 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 367252 Color: 1
Size: 324977 Color: 0
Size: 307772 Color: 1

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 422251 Color: 1
Size: 323787 Color: 1
Size: 253963 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 358872 Color: 1
Size: 336991 Color: 1
Size: 304138 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 353061 Color: 1
Size: 347409 Color: 1
Size: 299531 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 495233 Color: 1
Size: 253886 Color: 1
Size: 250882 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 464013 Color: 1
Size: 282256 Color: 1
Size: 253732 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 472369 Color: 1
Size: 272001 Color: 1
Size: 255631 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 487838 Color: 1
Size: 258838 Color: 1
Size: 253325 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 428973 Color: 1
Size: 320037 Color: 1
Size: 250991 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 485127 Color: 1
Size: 260124 Color: 1
Size: 254750 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 416049 Color: 1
Size: 328953 Color: 1
Size: 254999 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 401079 Color: 1
Size: 333975 Color: 1
Size: 264947 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 497930 Color: 1
Size: 251496 Color: 1
Size: 250575 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 390140 Color: 1
Size: 330681 Color: 1
Size: 279180 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 379533 Color: 1
Size: 335542 Color: 1
Size: 284926 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 450182 Color: 1
Size: 294094 Color: 1
Size: 255725 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 459242 Color: 1
Size: 271866 Color: 1
Size: 268893 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 384068 Color: 1
Size: 348409 Color: 1
Size: 267524 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 487025 Color: 1
Size: 261157 Color: 1
Size: 251819 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 466628 Color: 1
Size: 269890 Color: 1
Size: 263483 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 382008 Color: 1
Size: 346837 Color: 1
Size: 271156 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 352796 Color: 1
Size: 350496 Color: 1
Size: 296709 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 437300 Color: 1
Size: 305086 Color: 1
Size: 257615 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 363347 Color: 1
Size: 348261 Color: 1
Size: 288393 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 351673 Color: 1
Size: 325527 Color: 0
Size: 322801 Color: 1

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 464225 Color: 1
Size: 283239 Color: 1
Size: 252537 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 414390 Color: 1
Size: 332125 Color: 1
Size: 253486 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 402293 Color: 1
Size: 316482 Color: 1
Size: 281226 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 465196 Color: 1
Size: 282823 Color: 1
Size: 251982 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 401706 Color: 1
Size: 306973 Color: 0
Size: 291322 Color: 1

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 392231 Color: 1
Size: 328884 Color: 1
Size: 278886 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 353610 Color: 1
Size: 348714 Color: 1
Size: 297677 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 346000 Color: 1
Size: 343576 Color: 1
Size: 310425 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 382356 Color: 1
Size: 319905 Color: 1
Size: 297740 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 358608 Color: 1
Size: 332578 Color: 1
Size: 308815 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 432815 Color: 1
Size: 304745 Color: 1
Size: 262441 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 386399 Color: 1
Size: 324029 Color: 1
Size: 289573 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 487243 Color: 1
Size: 259707 Color: 0
Size: 253051 Color: 1

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 377400 Color: 1
Size: 364064 Color: 1
Size: 258537 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 368170 Color: 1
Size: 356165 Color: 1
Size: 275666 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 377780 Color: 1
Size: 343694 Color: 1
Size: 278527 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 475739 Color: 1
Size: 270901 Color: 1
Size: 253361 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 462016 Color: 1
Size: 285467 Color: 1
Size: 252518 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 369941 Color: 1
Size: 336036 Color: 1
Size: 294024 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 408539 Color: 1
Size: 299451 Color: 1
Size: 292011 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 423258 Color: 1
Size: 320471 Color: 1
Size: 256272 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 417295 Color: 1
Size: 317637 Color: 1
Size: 265069 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 386303 Color: 1
Size: 350256 Color: 1
Size: 263442 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 392804 Color: 1
Size: 341822 Color: 1
Size: 265375 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 460444 Color: 1
Size: 276662 Color: 1
Size: 262895 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 447931 Color: 1
Size: 299038 Color: 1
Size: 253032 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 446036 Color: 1
Size: 282140 Color: 1
Size: 271825 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 465145 Color: 1
Size: 269981 Color: 1
Size: 264875 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 454639 Color: 1
Size: 293029 Color: 1
Size: 252333 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 435690 Color: 1
Size: 282712 Color: 0
Size: 281599 Color: 1

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 382128 Color: 1
Size: 333234 Color: 1
Size: 284639 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 453875 Color: 1
Size: 293894 Color: 1
Size: 252232 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 430984 Color: 1
Size: 316171 Color: 1
Size: 252846 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 455314 Color: 1
Size: 285626 Color: 1
Size: 259061 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 364347 Color: 1
Size: 351281 Color: 1
Size: 284373 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 421619 Color: 1
Size: 319359 Color: 1
Size: 259023 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 379766 Color: 1
Size: 332426 Color: 1
Size: 287809 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 378386 Color: 1
Size: 315039 Color: 1
Size: 306576 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 384065 Color: 1
Size: 353510 Color: 1
Size: 262426 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 377443 Color: 1
Size: 318762 Color: 1
Size: 303796 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 357102 Color: 1
Size: 340810 Color: 1
Size: 302089 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 406029 Color: 1
Size: 334499 Color: 1
Size: 259473 Color: 0

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 377232 Color: 1
Size: 370324 Color: 1
Size: 252445 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 400002 Color: 1
Size: 307340 Color: 0
Size: 292659 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 395301 Color: 1
Size: 313986 Color: 1
Size: 290714 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 413617 Color: 1
Size: 311483 Color: 1
Size: 274901 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 401697 Color: 1
Size: 328967 Color: 1
Size: 269337 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 458419 Color: 1
Size: 288478 Color: 1
Size: 253104 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 461527 Color: 1
Size: 283365 Color: 1
Size: 255109 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 347736 Color: 1
Size: 327685 Color: 1
Size: 324580 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 478465 Color: 1
Size: 266505 Color: 1
Size: 255031 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 458793 Color: 1
Size: 274655 Color: 1
Size: 266553 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 418033 Color: 1
Size: 304849 Color: 1
Size: 277119 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 424536 Color: 1
Size: 322987 Color: 1
Size: 252478 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 395328 Color: 1
Size: 332370 Color: 1
Size: 272303 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 423993 Color: 1
Size: 291048 Color: 1
Size: 284960 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 496115 Color: 1
Size: 253696 Color: 1
Size: 250190 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 369402 Color: 1
Size: 325821 Color: 1
Size: 304778 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 350641 Color: 1
Size: 343949 Color: 1
Size: 305411 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 393547 Color: 1
Size: 328674 Color: 1
Size: 277780 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 384171 Color: 1
Size: 348008 Color: 1
Size: 267822 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 420613 Color: 1
Size: 311918 Color: 1
Size: 267470 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 431089 Color: 1
Size: 318228 Color: 1
Size: 250684 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 445825 Color: 1
Size: 291291 Color: 1
Size: 262885 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 446241 Color: 1
Size: 281671 Color: 1
Size: 272089 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 410327 Color: 1
Size: 297082 Color: 0
Size: 292592 Color: 1

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 404173 Color: 1
Size: 313927 Color: 1
Size: 281901 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 362603 Color: 1
Size: 360319 Color: 1
Size: 277079 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 413960 Color: 1
Size: 335117 Color: 1
Size: 250924 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 415375 Color: 1
Size: 304214 Color: 1
Size: 280412 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 420186 Color: 1
Size: 296680 Color: 1
Size: 283135 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 438976 Color: 1
Size: 296707 Color: 1
Size: 264318 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 446126 Color: 1
Size: 283002 Color: 0
Size: 270873 Color: 1

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 417785 Color: 1
Size: 310506 Color: 1
Size: 271710 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 491577 Color: 1
Size: 255773 Color: 1
Size: 252651 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 431441 Color: 1
Size: 285435 Color: 1
Size: 283125 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 363214 Color: 1
Size: 359867 Color: 1
Size: 276920 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 377372 Color: 1
Size: 336983 Color: 1
Size: 285646 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 438934 Color: 1
Size: 306198 Color: 1
Size: 254869 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 389700 Color: 1
Size: 333424 Color: 1
Size: 276877 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 406125 Color: 1
Size: 306072 Color: 1
Size: 287804 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 439756 Color: 1
Size: 295612 Color: 1
Size: 264633 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 490016 Color: 1
Size: 257942 Color: 1
Size: 252043 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 430419 Color: 1
Size: 317109 Color: 1
Size: 252473 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 494975 Color: 1
Size: 254783 Color: 1
Size: 250243 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 401306 Color: 1
Size: 339091 Color: 1
Size: 259604 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 423349 Color: 1
Size: 310092 Color: 1
Size: 266560 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 402300 Color: 1
Size: 310767 Color: 1
Size: 286934 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 420114 Color: 1
Size: 305505 Color: 1
Size: 274382 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 429602 Color: 1
Size: 309189 Color: 1
Size: 261210 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 391007 Color: 1
Size: 341204 Color: 1
Size: 267790 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 467407 Color: 1
Size: 280839 Color: 1
Size: 251755 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 394767 Color: 1
Size: 320342 Color: 1
Size: 284892 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 376075 Color: 1
Size: 322620 Color: 1
Size: 301306 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 493827 Color: 1
Size: 254011 Color: 1
Size: 252163 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 390099 Color: 1
Size: 327098 Color: 1
Size: 282804 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 404869 Color: 1
Size: 342303 Color: 1
Size: 252829 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 395736 Color: 1
Size: 329160 Color: 1
Size: 275105 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 499259 Color: 1
Size: 250424 Color: 1
Size: 250318 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 482991 Color: 1
Size: 263124 Color: 1
Size: 253886 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 397163 Color: 1
Size: 337876 Color: 1
Size: 264962 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 390234 Color: 1
Size: 348028 Color: 1
Size: 261739 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 382539 Color: 1
Size: 336240 Color: 1
Size: 281222 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 369065 Color: 1
Size: 326908 Color: 1
Size: 304028 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 399234 Color: 1
Size: 310737 Color: 1
Size: 290030 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 361885 Color: 1
Size: 347924 Color: 1
Size: 290192 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 418122 Color: 1
Size: 329599 Color: 1
Size: 252280 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 394551 Color: 1
Size: 321082 Color: 1
Size: 284368 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 481491 Color: 1
Size: 265727 Color: 1
Size: 252783 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 448239 Color: 1
Size: 294816 Color: 1
Size: 256946 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 471777 Color: 1
Size: 273515 Color: 1
Size: 254709 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 450156 Color: 1
Size: 290740 Color: 1
Size: 259105 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 495398 Color: 1
Size: 253891 Color: 1
Size: 250712 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 370223 Color: 1
Size: 338963 Color: 1
Size: 290815 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 395174 Color: 1
Size: 302727 Color: 0
Size: 302100 Color: 1

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 429376 Color: 1
Size: 305106 Color: 1
Size: 265519 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 376670 Color: 1
Size: 342627 Color: 1
Size: 280704 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 451153 Color: 1
Size: 295721 Color: 1
Size: 253127 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 453453 Color: 1
Size: 289821 Color: 0
Size: 256727 Color: 1

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 424123 Color: 1
Size: 318568 Color: 1
Size: 257310 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 375933 Color: 1
Size: 315432 Color: 1
Size: 308636 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 487281 Color: 1
Size: 262399 Color: 1
Size: 250321 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 382788 Color: 1
Size: 347989 Color: 1
Size: 269224 Color: 0

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 352846 Color: 1
Size: 323608 Color: 1
Size: 323547 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 425578 Color: 1
Size: 301049 Color: 1
Size: 273374 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 400893 Color: 1
Size: 329994 Color: 1
Size: 269114 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 361872 Color: 1
Size: 345014 Color: 1
Size: 293115 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 436927 Color: 1
Size: 281626 Color: 0
Size: 281448 Color: 1

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 485515 Color: 1
Size: 259004 Color: 1
Size: 255482 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 362717 Color: 1
Size: 352147 Color: 1
Size: 285137 Color: 0

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 413978 Color: 1
Size: 325147 Color: 1
Size: 260876 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 489522 Color: 1
Size: 258205 Color: 1
Size: 252274 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 394228 Color: 1
Size: 323350 Color: 1
Size: 282423 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 412451 Color: 1
Size: 330208 Color: 1
Size: 257342 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 416644 Color: 1
Size: 310674 Color: 1
Size: 272683 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 466535 Color: 1
Size: 280018 Color: 1
Size: 253448 Color: 0

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 356132 Color: 1
Size: 348888 Color: 1
Size: 294981 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 443888 Color: 1
Size: 288659 Color: 1
Size: 267454 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 486984 Color: 1
Size: 262856 Color: 1
Size: 250161 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 448260 Color: 1
Size: 283731 Color: 0
Size: 268010 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 464254 Color: 1
Size: 271393 Color: 1
Size: 264354 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 414245 Color: 1
Size: 322777 Color: 1
Size: 262979 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 462984 Color: 1
Size: 276353 Color: 1
Size: 260664 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 422560 Color: 1
Size: 304178 Color: 1
Size: 273263 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 477918 Color: 1
Size: 269855 Color: 1
Size: 252228 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 397659 Color: 1
Size: 327657 Color: 1
Size: 274685 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 428213 Color: 1
Size: 289093 Color: 0
Size: 282695 Color: 1

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 446604 Color: 1
Size: 277189 Color: 1
Size: 276208 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 481923 Color: 1
Size: 260660 Color: 1
Size: 257418 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 436712 Color: 1
Size: 311572 Color: 1
Size: 251717 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 359286 Color: 1
Size: 323130 Color: 0
Size: 317585 Color: 1

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 378620 Color: 1
Size: 358246 Color: 1
Size: 263135 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 495919 Color: 1
Size: 253404 Color: 1
Size: 250678 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 404009 Color: 1
Size: 330963 Color: 1
Size: 265029 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 466683 Color: 1
Size: 282180 Color: 1
Size: 251138 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 396041 Color: 1
Size: 312932 Color: 1
Size: 291028 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 496641 Color: 1
Size: 252226 Color: 1
Size: 251134 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 388141 Color: 1
Size: 325544 Color: 1
Size: 286316 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 378475 Color: 1
Size: 312166 Color: 0
Size: 309360 Color: 1

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 366220 Color: 1
Size: 345665 Color: 1
Size: 288116 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 438537 Color: 1
Size: 285897 Color: 1
Size: 275567 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 466671 Color: 1
Size: 269522 Color: 1
Size: 263808 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 415318 Color: 1
Size: 329214 Color: 1
Size: 255469 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 388511 Color: 1
Size: 323050 Color: 1
Size: 288440 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 469774 Color: 1
Size: 279900 Color: 1
Size: 250327 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 417879 Color: 1
Size: 318372 Color: 1
Size: 263750 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 461194 Color: 1
Size: 273088 Color: 1
Size: 265719 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 352923 Color: 1
Size: 334776 Color: 1
Size: 312302 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 462982 Color: 1
Size: 283277 Color: 1
Size: 253742 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 424908 Color: 1
Size: 296014 Color: 1
Size: 279079 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 436277 Color: 1
Size: 304766 Color: 1
Size: 258958 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 404061 Color: 1
Size: 322399 Color: 1
Size: 273541 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 418628 Color: 1
Size: 297860 Color: 1
Size: 283513 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 427092 Color: 1
Size: 287273 Color: 1
Size: 285636 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 372454 Color: 1
Size: 337258 Color: 1
Size: 290289 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 457239 Color: 1
Size: 279184 Color: 1
Size: 263578 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 364055 Color: 1
Size: 341019 Color: 1
Size: 294927 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 370152 Color: 1
Size: 362122 Color: 1
Size: 267727 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 475867 Color: 1
Size: 267189 Color: 1
Size: 256945 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 441602 Color: 1
Size: 288457 Color: 1
Size: 269942 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 390996 Color: 1
Size: 349384 Color: 1
Size: 259621 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 445616 Color: 1
Size: 300892 Color: 1
Size: 253493 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 453905 Color: 1
Size: 290768 Color: 1
Size: 255328 Color: 0

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 404875 Color: 1
Size: 341703 Color: 1
Size: 253423 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 408949 Color: 1
Size: 313425 Color: 1
Size: 277627 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 359363 Color: 1
Size: 327923 Color: 1
Size: 312715 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 433900 Color: 1
Size: 294043 Color: 1
Size: 272058 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 426688 Color: 1
Size: 310841 Color: 1
Size: 262472 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 468815 Color: 1
Size: 270709 Color: 1
Size: 260477 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 365384 Color: 1
Size: 355945 Color: 1
Size: 278672 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 412430 Color: 1
Size: 331577 Color: 1
Size: 255994 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 422865 Color: 1
Size: 320654 Color: 1
Size: 256482 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 367226 Color: 1
Size: 336256 Color: 1
Size: 296519 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 409243 Color: 1
Size: 340240 Color: 1
Size: 250518 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 359593 Color: 1
Size: 340315 Color: 1
Size: 300093 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 492198 Color: 1
Size: 254569 Color: 0
Size: 253234 Color: 1

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 397895 Color: 1
Size: 341016 Color: 1
Size: 261090 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 466042 Color: 1
Size: 269430 Color: 0
Size: 264529 Color: 1

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 409686 Color: 1
Size: 304615 Color: 0
Size: 285700 Color: 1

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 429282 Color: 1
Size: 300463 Color: 1
Size: 270256 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 385139 Color: 1
Size: 337753 Color: 1
Size: 277109 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 375860 Color: 1
Size: 343008 Color: 1
Size: 281133 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 475161 Color: 1
Size: 272896 Color: 1
Size: 251944 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 427102 Color: 1
Size: 296850 Color: 1
Size: 276049 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 487354 Color: 1
Size: 260547 Color: 1
Size: 252100 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 416507 Color: 1
Size: 323676 Color: 0
Size: 259818 Color: 1

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 380067 Color: 1
Size: 367291 Color: 1
Size: 252643 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 483415 Color: 1
Size: 264770 Color: 1
Size: 251816 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 361685 Color: 1
Size: 357726 Color: 1
Size: 280590 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 396284 Color: 1
Size: 313729 Color: 0
Size: 289988 Color: 1

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 385485 Color: 1
Size: 354851 Color: 1
Size: 259665 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 453669 Color: 1
Size: 288159 Color: 1
Size: 258173 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 366692 Color: 1
Size: 344291 Color: 1
Size: 289018 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 419314 Color: 1
Size: 312948 Color: 1
Size: 267739 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 485018 Color: 1
Size: 260016 Color: 1
Size: 254967 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 392962 Color: 1
Size: 304080 Color: 0
Size: 302959 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 357639 Color: 1
Size: 336792 Color: 1
Size: 305570 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 378025 Color: 1
Size: 323238 Color: 0
Size: 298738 Color: 1

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 407077 Color: 1
Size: 310054 Color: 1
Size: 282870 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 470733 Color: 1
Size: 269470 Color: 1
Size: 259798 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 454152 Color: 1
Size: 290637 Color: 1
Size: 255212 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 369051 Color: 1
Size: 335359 Color: 1
Size: 295591 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 368804 Color: 1
Size: 331395 Color: 1
Size: 299802 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 477262 Color: 1
Size: 266371 Color: 1
Size: 256368 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 378854 Color: 1
Size: 338140 Color: 1
Size: 283007 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 421137 Color: 1
Size: 293509 Color: 1
Size: 285355 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 362920 Color: 1
Size: 359752 Color: 1
Size: 277329 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 360128 Color: 1
Size: 359953 Color: 1
Size: 279920 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 384356 Color: 1
Size: 337478 Color: 1
Size: 278167 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 428921 Color: 1
Size: 301969 Color: 1
Size: 269111 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 402020 Color: 1
Size: 313322 Color: 1
Size: 284659 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 411948 Color: 1
Size: 298964 Color: 0
Size: 289089 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 354903 Color: 1
Size: 344598 Color: 1
Size: 300500 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 370835 Color: 1
Size: 326610 Color: 0
Size: 302556 Color: 1

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 365339 Color: 1
Size: 347087 Color: 1
Size: 287575 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 409688 Color: 1
Size: 310542 Color: 1
Size: 279771 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 465906 Color: 1
Size: 270673 Color: 1
Size: 263422 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 424052 Color: 1
Size: 289907 Color: 1
Size: 286042 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 418417 Color: 1
Size: 312159 Color: 1
Size: 269425 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 374944 Color: 1
Size: 332556 Color: 1
Size: 292501 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 360602 Color: 1
Size: 350728 Color: 1
Size: 288671 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 449356 Color: 1
Size: 293967 Color: 1
Size: 256678 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 371541 Color: 1
Size: 339126 Color: 1
Size: 289334 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 437112 Color: 1
Size: 305773 Color: 1
Size: 257116 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 491217 Color: 1
Size: 258233 Color: 1
Size: 250551 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 360652 Color: 1
Size: 351398 Color: 1
Size: 287951 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 425405 Color: 1
Size: 315891 Color: 1
Size: 258705 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 388954 Color: 1
Size: 325555 Color: 1
Size: 285492 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 411614 Color: 1
Size: 296242 Color: 1
Size: 292145 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 474862 Color: 1
Size: 270764 Color: 1
Size: 254375 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 380747 Color: 1
Size: 314511 Color: 0
Size: 304743 Color: 1

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 335123 Color: 1
Size: 333758 Color: 1
Size: 331120 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 443622 Color: 1
Size: 282160 Color: 0
Size: 274219 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 454365 Color: 1
Size: 281453 Color: 1
Size: 264183 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 408503 Color: 1
Size: 310781 Color: 1
Size: 280717 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 388700 Color: 1
Size: 339830 Color: 1
Size: 271471 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 393002 Color: 1
Size: 305176 Color: 1
Size: 301823 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 355250 Color: 1
Size: 347322 Color: 1
Size: 297429 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 418212 Color: 1
Size: 322187 Color: 1
Size: 259602 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 368174 Color: 1
Size: 332781 Color: 1
Size: 299046 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 410649 Color: 1
Size: 330480 Color: 1
Size: 258872 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 424340 Color: 1
Size: 318028 Color: 1
Size: 257633 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 429964 Color: 1
Size: 302164 Color: 1
Size: 267873 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 398721 Color: 1
Size: 344348 Color: 1
Size: 256932 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 474569 Color: 1
Size: 266958 Color: 1
Size: 258474 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 377866 Color: 1
Size: 357435 Color: 1
Size: 264700 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 406173 Color: 1
Size: 328847 Color: 1
Size: 264981 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 420116 Color: 1
Size: 313716 Color: 1
Size: 266169 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 468487 Color: 1
Size: 277813 Color: 1
Size: 253701 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 387074 Color: 1
Size: 324282 Color: 1
Size: 288645 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 399020 Color: 1
Size: 332686 Color: 1
Size: 268295 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 376026 Color: 1
Size: 360412 Color: 1
Size: 263563 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 477925 Color: 1
Size: 270272 Color: 1
Size: 251804 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 390167 Color: 1
Size: 323404 Color: 1
Size: 286430 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 405172 Color: 1
Size: 329435 Color: 1
Size: 265394 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 344579 Color: 1
Size: 336213 Color: 1
Size: 319209 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 444420 Color: 1
Size: 288908 Color: 1
Size: 266673 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 499144 Color: 1
Size: 250797 Color: 1
Size: 250060 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 414428 Color: 1
Size: 308094 Color: 1
Size: 277479 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 400453 Color: 1
Size: 326152 Color: 1
Size: 273396 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 342704 Color: 1
Size: 334086 Color: 1
Size: 323211 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 451454 Color: 1
Size: 284175 Color: 1
Size: 264372 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 396950 Color: 1
Size: 332353 Color: 1
Size: 270698 Color: 0

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 426992 Color: 1
Size: 316223 Color: 1
Size: 256786 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 388918 Color: 1
Size: 340395 Color: 1
Size: 270688 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 444644 Color: 1
Size: 298685 Color: 1
Size: 256672 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 481131 Color: 1
Size: 265929 Color: 1
Size: 252941 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 442771 Color: 1
Size: 284410 Color: 1
Size: 272820 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 363445 Color: 1
Size: 334939 Color: 1
Size: 301617 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 394058 Color: 1
Size: 342849 Color: 1
Size: 263094 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 437810 Color: 1
Size: 283676 Color: 1
Size: 278515 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 498035 Color: 1
Size: 251658 Color: 1
Size: 250308 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 448426 Color: 1
Size: 294892 Color: 1
Size: 256683 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 355228 Color: 1
Size: 351244 Color: 1
Size: 293529 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 442241 Color: 1
Size: 305238 Color: 1
Size: 252522 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 358699 Color: 1
Size: 322549 Color: 0
Size: 318753 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 482172 Color: 1
Size: 260959 Color: 1
Size: 256870 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 383057 Color: 1
Size: 341774 Color: 1
Size: 275170 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 415995 Color: 1
Size: 308731 Color: 0
Size: 275275 Color: 1

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 475230 Color: 1
Size: 264033 Color: 1
Size: 260738 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 374455 Color: 1
Size: 344759 Color: 1
Size: 280787 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 433440 Color: 1
Size: 309711 Color: 1
Size: 256850 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 442202 Color: 1
Size: 295388 Color: 1
Size: 262411 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 494117 Color: 1
Size: 253422 Color: 1
Size: 252462 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 372357 Color: 1
Size: 367567 Color: 1
Size: 260077 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 406020 Color: 1
Size: 311547 Color: 1
Size: 282434 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 450119 Color: 1
Size: 289880 Color: 1
Size: 260002 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 446904 Color: 1
Size: 300115 Color: 1
Size: 252982 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 407943 Color: 1
Size: 329014 Color: 1
Size: 263044 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 372579 Color: 1
Size: 346818 Color: 1
Size: 280604 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 361075 Color: 1
Size: 326467 Color: 1
Size: 312459 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 394705 Color: 1
Size: 321804 Color: 1
Size: 283492 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 488445 Color: 1
Size: 261124 Color: 1
Size: 250432 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 351891 Color: 1
Size: 350181 Color: 1
Size: 297929 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 363304 Color: 1
Size: 325239 Color: 1
Size: 311458 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 397110 Color: 1
Size: 333286 Color: 1
Size: 269605 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 455457 Color: 1
Size: 288849 Color: 1
Size: 255695 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 384347 Color: 1
Size: 317907 Color: 1
Size: 297747 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 365904 Color: 1
Size: 334298 Color: 1
Size: 299799 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 401202 Color: 1
Size: 323536 Color: 1
Size: 275263 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 421893 Color: 1
Size: 308954 Color: 1
Size: 269154 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 433619 Color: 1
Size: 289705 Color: 1
Size: 276677 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 468372 Color: 1
Size: 276658 Color: 0
Size: 254971 Color: 1

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 434336 Color: 1
Size: 309798 Color: 1
Size: 255867 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 354726 Color: 1
Size: 334374 Color: 1
Size: 310901 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 383287 Color: 1
Size: 335768 Color: 1
Size: 280946 Color: 0

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 378698 Color: 1
Size: 357257 Color: 1
Size: 264046 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 460499 Color: 1
Size: 276398 Color: 1
Size: 263104 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 359159 Color: 1
Size: 353747 Color: 1
Size: 287095 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 418465 Color: 1
Size: 304111 Color: 1
Size: 277425 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 425772 Color: 1
Size: 315293 Color: 1
Size: 258936 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 352626 Color: 1
Size: 348217 Color: 1
Size: 299158 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 447607 Color: 1
Size: 285090 Color: 1
Size: 267304 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 480239 Color: 1
Size: 262384 Color: 1
Size: 257378 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 394077 Color: 1
Size: 317991 Color: 1
Size: 287933 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 397935 Color: 1
Size: 313741 Color: 1
Size: 288325 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 423048 Color: 1
Size: 306419 Color: 1
Size: 270534 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 372428 Color: 1
Size: 342436 Color: 1
Size: 285137 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 416889 Color: 1
Size: 310233 Color: 1
Size: 272879 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 424428 Color: 1
Size: 297247 Color: 1
Size: 278326 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 428383 Color: 1
Size: 288197 Color: 1
Size: 283421 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 498088 Color: 1
Size: 251327 Color: 1
Size: 250586 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 428781 Color: 1
Size: 307043 Color: 1
Size: 264177 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 413096 Color: 1
Size: 316012 Color: 1
Size: 270893 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 431146 Color: 1
Size: 296459 Color: 1
Size: 272396 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 480340 Color: 1
Size: 260844 Color: 1
Size: 258817 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 368412 Color: 1
Size: 323247 Color: 0
Size: 308342 Color: 1

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 431565 Color: 1
Size: 311554 Color: 1
Size: 256882 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 478807 Color: 1
Size: 265050 Color: 1
Size: 256144 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 388460 Color: 1
Size: 329908 Color: 1
Size: 281633 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 460720 Color: 1
Size: 273988 Color: 1
Size: 265293 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 366319 Color: 1
Size: 343117 Color: 1
Size: 290565 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 466772 Color: 1
Size: 282749 Color: 1
Size: 250480 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 438920 Color: 1
Size: 304550 Color: 1
Size: 256531 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 434554 Color: 1
Size: 312026 Color: 1
Size: 253421 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 428757 Color: 1
Size: 318886 Color: 1
Size: 252358 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 406871 Color: 1
Size: 315837 Color: 1
Size: 277293 Color: 0

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 433395 Color: 1
Size: 314959 Color: 1
Size: 251647 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 348044 Color: 1
Size: 340671 Color: 1
Size: 311286 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 460261 Color: 1
Size: 272465 Color: 1
Size: 267275 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 369105 Color: 1
Size: 323327 Color: 0
Size: 307569 Color: 1

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 414878 Color: 1
Size: 318523 Color: 1
Size: 266600 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 447769 Color: 1
Size: 295640 Color: 1
Size: 256592 Color: 0

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 366712 Color: 1
Size: 320936 Color: 1
Size: 312353 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 409940 Color: 1
Size: 318462 Color: 1
Size: 271599 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 479588 Color: 1
Size: 269254 Color: 1
Size: 251159 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 476818 Color: 1
Size: 267209 Color: 1
Size: 255974 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 359978 Color: 1
Size: 336640 Color: 1
Size: 303383 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 417682 Color: 1
Size: 321197 Color: 1
Size: 261122 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 426152 Color: 1
Size: 313449 Color: 0
Size: 260400 Color: 1

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 419445 Color: 1
Size: 305463 Color: 1
Size: 275093 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 473036 Color: 1
Size: 267149 Color: 0
Size: 259816 Color: 1

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 454861 Color: 1
Size: 276391 Color: 1
Size: 268749 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 442186 Color: 1
Size: 304514 Color: 1
Size: 253301 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 370996 Color: 1
Size: 315097 Color: 0
Size: 313908 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 407095 Color: 1
Size: 317620 Color: 1
Size: 275286 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 366404 Color: 1
Size: 347182 Color: 1
Size: 286415 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 351414 Color: 1
Size: 349952 Color: 1
Size: 298635 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 467069 Color: 1
Size: 278629 Color: 1
Size: 254303 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 381497 Color: 1
Size: 337527 Color: 1
Size: 280977 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 473845 Color: 1
Size: 275735 Color: 1
Size: 250421 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 430730 Color: 1
Size: 285426 Color: 0
Size: 283845 Color: 1

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 398871 Color: 1
Size: 320855 Color: 1
Size: 280275 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 378677 Color: 1
Size: 335924 Color: 1
Size: 285400 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 479919 Color: 1
Size: 261866 Color: 1
Size: 258216 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 458769 Color: 1
Size: 284077 Color: 1
Size: 257155 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 405090 Color: 1
Size: 308572 Color: 1
Size: 286339 Color: 0

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 407520 Color: 1
Size: 297104 Color: 1
Size: 295377 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 413727 Color: 1
Size: 332197 Color: 1
Size: 254077 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 403487 Color: 1
Size: 318610 Color: 1
Size: 277904 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 420238 Color: 1
Size: 290677 Color: 0
Size: 289086 Color: 1

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 461498 Color: 1
Size: 280579 Color: 1
Size: 257924 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 366808 Color: 1
Size: 352052 Color: 1
Size: 281141 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 455028 Color: 1
Size: 275811 Color: 1
Size: 269162 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 457620 Color: 1
Size: 287689 Color: 1
Size: 254692 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 355904 Color: 1
Size: 322487 Color: 1
Size: 321610 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 383131 Color: 1
Size: 344996 Color: 1
Size: 271874 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 443919 Color: 1
Size: 298874 Color: 1
Size: 257208 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 397497 Color: 1
Size: 335290 Color: 1
Size: 267214 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 403181 Color: 1
Size: 331448 Color: 1
Size: 265372 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 414528 Color: 1
Size: 317640 Color: 1
Size: 267833 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 364723 Color: 1
Size: 319851 Color: 1
Size: 315427 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 414689 Color: 1
Size: 308719 Color: 1
Size: 276593 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 398758 Color: 1
Size: 309370 Color: 1
Size: 291873 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 425380 Color: 1
Size: 313303 Color: 1
Size: 261318 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 383115 Color: 1
Size: 327964 Color: 1
Size: 288922 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 383099 Color: 1
Size: 323864 Color: 1
Size: 293038 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 416666 Color: 1
Size: 318986 Color: 1
Size: 264349 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 474986 Color: 1
Size: 267900 Color: 1
Size: 257115 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 444917 Color: 1
Size: 295503 Color: 1
Size: 259581 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 366948 Color: 1
Size: 341976 Color: 1
Size: 291077 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 499208 Color: 1
Size: 250449 Color: 1
Size: 250344 Color: 0

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 417555 Color: 1
Size: 309928 Color: 1
Size: 272518 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 416799 Color: 1
Size: 297069 Color: 1
Size: 286133 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 386676 Color: 1
Size: 333322 Color: 1
Size: 280003 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 484335 Color: 1
Size: 258168 Color: 0
Size: 257498 Color: 1

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 434505 Color: 1
Size: 304094 Color: 1
Size: 261402 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 407796 Color: 1
Size: 300764 Color: 1
Size: 291441 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 429685 Color: 1
Size: 310889 Color: 1
Size: 259427 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 413439 Color: 1
Size: 334845 Color: 1
Size: 251717 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 485594 Color: 1
Size: 262343 Color: 1
Size: 252064 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 365088 Color: 1
Size: 343234 Color: 1
Size: 291679 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 412654 Color: 1
Size: 329970 Color: 1
Size: 257377 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 399318 Color: 1
Size: 319266 Color: 1
Size: 281417 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 417808 Color: 1
Size: 309389 Color: 1
Size: 272804 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 411914 Color: 1
Size: 320362 Color: 1
Size: 267725 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 463567 Color: 1
Size: 280284 Color: 1
Size: 256150 Color: 0

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 374056 Color: 1
Size: 345180 Color: 1
Size: 280765 Color: 0

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 411094 Color: 1
Size: 311231 Color: 0
Size: 277676 Color: 1

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 375538 Color: 1
Size: 341960 Color: 1
Size: 282503 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 402992 Color: 1
Size: 302712 Color: 1
Size: 294297 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 373392 Color: 1
Size: 325338 Color: 1
Size: 301271 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 397831 Color: 1
Size: 336761 Color: 1
Size: 265409 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 349990 Color: 1
Size: 348244 Color: 1
Size: 301767 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 494358 Color: 1
Size: 253418 Color: 1
Size: 252225 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 456200 Color: 1
Size: 286360 Color: 1
Size: 257441 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 381053 Color: 1
Size: 326145 Color: 1
Size: 292803 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 423013 Color: 1
Size: 319600 Color: 1
Size: 257388 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 448314 Color: 1
Size: 286046 Color: 1
Size: 265641 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 359787 Color: 1
Size: 343515 Color: 1
Size: 296699 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 438383 Color: 1
Size: 304785 Color: 1
Size: 256833 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 379636 Color: 1
Size: 343445 Color: 1
Size: 276920 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 488928 Color: 1
Size: 260204 Color: 1
Size: 250869 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 447778 Color: 1
Size: 287275 Color: 1
Size: 264948 Color: 0

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 393664 Color: 1
Size: 308102 Color: 1
Size: 298235 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 401264 Color: 1
Size: 321844 Color: 1
Size: 276893 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 452202 Color: 1
Size: 285882 Color: 1
Size: 261917 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 365389 Color: 1
Size: 324543 Color: 1
Size: 310069 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 386501 Color: 1
Size: 310781 Color: 1
Size: 302719 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 356941 Color: 1
Size: 330720 Color: 1
Size: 312340 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 433313 Color: 1
Size: 310470 Color: 1
Size: 256218 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 481900 Color: 1
Size: 260410 Color: 0
Size: 257691 Color: 1

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 455785 Color: 1
Size: 275169 Color: 1
Size: 269047 Color: 0

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 387262 Color: 1
Size: 332895 Color: 1
Size: 279844 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 487900 Color: 1
Size: 259286 Color: 1
Size: 252815 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 357825 Color: 1
Size: 346271 Color: 1
Size: 295905 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 496224 Color: 1
Size: 253338 Color: 1
Size: 250439 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 388813 Color: 1
Size: 344254 Color: 1
Size: 266934 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 406779 Color: 1
Size: 329706 Color: 1
Size: 263516 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 405381 Color: 1
Size: 331926 Color: 1
Size: 262694 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 439038 Color: 1
Size: 304339 Color: 1
Size: 256624 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 452694 Color: 1
Size: 295242 Color: 1
Size: 252065 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 349879 Color: 1
Size: 338067 Color: 1
Size: 312055 Color: 0

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 483185 Color: 1
Size: 266689 Color: 1
Size: 250127 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 482634 Color: 1
Size: 266513 Color: 1
Size: 250854 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 456167 Color: 1
Size: 276694 Color: 1
Size: 267140 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 387182 Color: 1
Size: 353849 Color: 1
Size: 258970 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 482140 Color: 1
Size: 261738 Color: 1
Size: 256123 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 471297 Color: 1
Size: 270787 Color: 1
Size: 257917 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 389253 Color: 1
Size: 316773 Color: 0
Size: 293975 Color: 1

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 384842 Color: 1
Size: 330477 Color: 1
Size: 284682 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 447184 Color: 1
Size: 300893 Color: 1
Size: 251924 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 436662 Color: 1
Size: 296104 Color: 1
Size: 267235 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 380905 Color: 1
Size: 345858 Color: 1
Size: 273238 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 421386 Color: 1
Size: 320068 Color: 1
Size: 258547 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 424035 Color: 1
Size: 313882 Color: 1
Size: 262084 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 375351 Color: 1
Size: 321136 Color: 0
Size: 303514 Color: 1

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 347440 Color: 1
Size: 334073 Color: 1
Size: 318488 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 476777 Color: 1
Size: 270923 Color: 1
Size: 252301 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 479781 Color: 1
Size: 262161 Color: 1
Size: 258059 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 391747 Color: 1
Size: 304240 Color: 0
Size: 304014 Color: 1

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 366643 Color: 1
Size: 328048 Color: 1
Size: 305310 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 375511 Color: 1
Size: 339107 Color: 1
Size: 285383 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 420480 Color: 1
Size: 319353 Color: 1
Size: 260168 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 465683 Color: 1
Size: 276526 Color: 1
Size: 257792 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 468426 Color: 1
Size: 280971 Color: 1
Size: 250604 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 379835 Color: 1
Size: 344403 Color: 1
Size: 275763 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 366556 Color: 1
Size: 363163 Color: 1
Size: 270282 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 362631 Color: 1
Size: 328269 Color: 1
Size: 309101 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 397351 Color: 1
Size: 337604 Color: 1
Size: 265046 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 413731 Color: 1
Size: 327661 Color: 1
Size: 258609 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 353858 Color: 1
Size: 338220 Color: 1
Size: 307923 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 379983 Color: 1
Size: 331755 Color: 1
Size: 288263 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 439618 Color: 1
Size: 298406 Color: 1
Size: 261977 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 408810 Color: 1
Size: 338400 Color: 1
Size: 252791 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 447115 Color: 1
Size: 287210 Color: 1
Size: 265676 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 498125 Color: 1
Size: 251754 Color: 1
Size: 250122 Color: 0

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 355572 Color: 1
Size: 336236 Color: 1
Size: 308193 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 393367 Color: 1
Size: 324272 Color: 1
Size: 282362 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 367693 Color: 1
Size: 355766 Color: 1
Size: 276542 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 365062 Color: 1
Size: 328464 Color: 1
Size: 306475 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 465557 Color: 1
Size: 280149 Color: 1
Size: 254295 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 417259 Color: 1
Size: 301940 Color: 1
Size: 280802 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 381076 Color: 1
Size: 362374 Color: 1
Size: 256551 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 375682 Color: 1
Size: 319643 Color: 0
Size: 304676 Color: 1

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 392706 Color: 1
Size: 351855 Color: 1
Size: 255440 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 435605 Color: 1
Size: 308058 Color: 1
Size: 256338 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 390579 Color: 1
Size: 316979 Color: 0
Size: 292443 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 464465 Color: 1
Size: 272001 Color: 1
Size: 263535 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 446125 Color: 1
Size: 288121 Color: 0
Size: 265755 Color: 1

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 374757 Color: 1
Size: 352368 Color: 1
Size: 272876 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 372937 Color: 1
Size: 338454 Color: 1
Size: 288610 Color: 0

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 389042 Color: 1
Size: 329662 Color: 1
Size: 281297 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 483024 Color: 1
Size: 264343 Color: 1
Size: 252634 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 418314 Color: 1
Size: 314211 Color: 1
Size: 267476 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 381763 Color: 1
Size: 319552 Color: 1
Size: 298686 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 458619 Color: 1
Size: 290870 Color: 1
Size: 250512 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 464624 Color: 1
Size: 276855 Color: 1
Size: 258522 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 450238 Color: 1
Size: 281741 Color: 1
Size: 268022 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 483019 Color: 1
Size: 262420 Color: 1
Size: 254562 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 369048 Color: 1
Size: 363657 Color: 1
Size: 267296 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 389916 Color: 1
Size: 308255 Color: 0
Size: 301830 Color: 1

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 370824 Color: 1
Size: 320526 Color: 1
Size: 308651 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 482460 Color: 1
Size: 264942 Color: 1
Size: 252599 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 356773 Color: 1
Size: 355871 Color: 1
Size: 287357 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 431388 Color: 1
Size: 306061 Color: 1
Size: 262552 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 365347 Color: 1
Size: 359054 Color: 1
Size: 275600 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 375285 Color: 1
Size: 342344 Color: 1
Size: 282372 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 390249 Color: 1
Size: 310518 Color: 0
Size: 299234 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 410536 Color: 1
Size: 312871 Color: 1
Size: 276594 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 381087 Color: 1
Size: 353366 Color: 1
Size: 265548 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 359236 Color: 1
Size: 348337 Color: 1
Size: 292428 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 469917 Color: 1
Size: 278525 Color: 1
Size: 251559 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 432375 Color: 1
Size: 300549 Color: 1
Size: 267077 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 402386 Color: 1
Size: 330333 Color: 1
Size: 267282 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 392550 Color: 1
Size: 303763 Color: 1
Size: 303688 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 374561 Color: 1
Size: 326594 Color: 1
Size: 298846 Color: 0

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 413791 Color: 1
Size: 294050 Color: 0
Size: 292160 Color: 1

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 351734 Color: 1
Size: 349262 Color: 1
Size: 299005 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 486221 Color: 1
Size: 257517 Color: 1
Size: 256263 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 375087 Color: 1
Size: 365698 Color: 1
Size: 259216 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 463401 Color: 1
Size: 283981 Color: 1
Size: 252619 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 473534 Color: 1
Size: 270631 Color: 1
Size: 255836 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 460672 Color: 1
Size: 281765 Color: 1
Size: 257564 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 466290 Color: 1
Size: 276594 Color: 1
Size: 257117 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 438269 Color: 1
Size: 302310 Color: 1
Size: 259422 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 445510 Color: 1
Size: 299303 Color: 1
Size: 255188 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 405775 Color: 1
Size: 317425 Color: 0
Size: 276801 Color: 1

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 416405 Color: 1
Size: 300010 Color: 1
Size: 283586 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 465715 Color: 1
Size: 269749 Color: 1
Size: 264537 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 471322 Color: 1
Size: 275450 Color: 1
Size: 253229 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 384187 Color: 1
Size: 330919 Color: 1
Size: 284895 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 416488 Color: 1
Size: 316809 Color: 1
Size: 266704 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 477407 Color: 1
Size: 266011 Color: 0
Size: 256583 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 491308 Color: 1
Size: 255495 Color: 1
Size: 253198 Color: 0

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 475958 Color: 1
Size: 267417 Color: 1
Size: 256626 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 373746 Color: 1
Size: 370926 Color: 1
Size: 255329 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 401481 Color: 1
Size: 339365 Color: 1
Size: 259155 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 354173 Color: 1
Size: 353620 Color: 1
Size: 292208 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 492110 Color: 1
Size: 256694 Color: 1
Size: 251197 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 354030 Color: 1
Size: 352990 Color: 1
Size: 292981 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 437045 Color: 1
Size: 306947 Color: 1
Size: 256009 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 426503 Color: 1
Size: 287501 Color: 1
Size: 285997 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 451476 Color: 1
Size: 290866 Color: 1
Size: 257659 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 375312 Color: 1
Size: 332846 Color: 1
Size: 291843 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 362607 Color: 1
Size: 344245 Color: 1
Size: 293149 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 422117 Color: 1
Size: 295791 Color: 1
Size: 282093 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 411505 Color: 1
Size: 325050 Color: 1
Size: 263446 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 380244 Color: 1
Size: 348469 Color: 1
Size: 271288 Color: 0

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 388180 Color: 1
Size: 350070 Color: 1
Size: 261751 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 464643 Color: 1
Size: 273636 Color: 1
Size: 261722 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 359652 Color: 1
Size: 358033 Color: 1
Size: 282316 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 460190 Color: 1
Size: 285944 Color: 1
Size: 253867 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 349596 Color: 1
Size: 333899 Color: 1
Size: 316506 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 415055 Color: 1
Size: 313489 Color: 1
Size: 271457 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 469149 Color: 1
Size: 267345 Color: 0
Size: 263507 Color: 1

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 392226 Color: 1
Size: 355702 Color: 1
Size: 252073 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 373717 Color: 1
Size: 371737 Color: 1
Size: 254547 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 371019 Color: 1
Size: 337518 Color: 1
Size: 291464 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 445495 Color: 1
Size: 289252 Color: 1
Size: 265254 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 492931 Color: 1
Size: 254580 Color: 1
Size: 252490 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 481910 Color: 1
Size: 267126 Color: 1
Size: 250965 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 431654 Color: 1
Size: 302020 Color: 1
Size: 266327 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 374537 Color: 1
Size: 337273 Color: 1
Size: 288191 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 406537 Color: 1
Size: 332109 Color: 1
Size: 261355 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 372362 Color: 1
Size: 330695 Color: 1
Size: 296944 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 404798 Color: 1
Size: 342037 Color: 1
Size: 253166 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 459629 Color: 1
Size: 290266 Color: 1
Size: 250106 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 449475 Color: 1
Size: 288579 Color: 1
Size: 261947 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 494615 Color: 1
Size: 253013 Color: 0
Size: 252373 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 422986 Color: 1
Size: 313510 Color: 1
Size: 263505 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 473703 Color: 1
Size: 271514 Color: 1
Size: 254784 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 377766 Color: 1
Size: 326474 Color: 1
Size: 295761 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 456888 Color: 1
Size: 286754 Color: 1
Size: 256359 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 455517 Color: 1
Size: 285252 Color: 1
Size: 259232 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 435828 Color: 1
Size: 288848 Color: 0
Size: 275325 Color: 1

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 386808 Color: 1
Size: 317894 Color: 0
Size: 295299 Color: 1

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 375217 Color: 1
Size: 345684 Color: 1
Size: 279100 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 362376 Color: 1
Size: 339696 Color: 1
Size: 297929 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 443626 Color: 1
Size: 296291 Color: 1
Size: 260084 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 390145 Color: 1
Size: 323426 Color: 1
Size: 286430 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 352608 Color: 1
Size: 349767 Color: 1
Size: 297626 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 392723 Color: 1
Size: 322718 Color: 1
Size: 284560 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 448178 Color: 1
Size: 289979 Color: 1
Size: 261844 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 385274 Color: 1
Size: 347195 Color: 1
Size: 267532 Color: 0

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 407421 Color: 1
Size: 310478 Color: 0
Size: 282102 Color: 1

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 363442 Color: 1
Size: 325427 Color: 1
Size: 311132 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 478709 Color: 1
Size: 265582 Color: 1
Size: 255710 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 377658 Color: 1
Size: 320228 Color: 1
Size: 302115 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 371850 Color: 1
Size: 370513 Color: 1
Size: 257638 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 397974 Color: 1
Size: 328944 Color: 0
Size: 273083 Color: 1

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 346298 Color: 1
Size: 331646 Color: 1
Size: 322057 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 428016 Color: 1
Size: 304755 Color: 1
Size: 267230 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 431529 Color: 1
Size: 303440 Color: 1
Size: 265032 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 371780 Color: 1
Size: 339829 Color: 1
Size: 288392 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 485482 Color: 1
Size: 262594 Color: 1
Size: 251925 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 361022 Color: 1
Size: 347296 Color: 1
Size: 291683 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 409045 Color: 1
Size: 298492 Color: 1
Size: 292464 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 483127 Color: 1
Size: 265533 Color: 1
Size: 251341 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 404980 Color: 1
Size: 334268 Color: 1
Size: 260753 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 377782 Color: 1
Size: 339023 Color: 1
Size: 283196 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 388365 Color: 1
Size: 319928 Color: 1
Size: 291708 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 428882 Color: 1
Size: 301566 Color: 1
Size: 269553 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 395339 Color: 1
Size: 303708 Color: 0
Size: 300954 Color: 1

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 342905 Color: 1
Size: 331317 Color: 0
Size: 325779 Color: 1

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 414927 Color: 1
Size: 334490 Color: 1
Size: 250584 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 1
Size: 335390 Color: 1
Size: 262504 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 457279 Color: 1
Size: 292377 Color: 1
Size: 250345 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 475246 Color: 1
Size: 269750 Color: 1
Size: 255005 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 364778 Color: 1
Size: 348862 Color: 1
Size: 286361 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 381437 Color: 1
Size: 327270 Color: 1
Size: 291294 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 415815 Color: 1
Size: 327534 Color: 1
Size: 256652 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 418271 Color: 1
Size: 328697 Color: 1
Size: 253033 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 474271 Color: 1
Size: 267569 Color: 1
Size: 258161 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 407942 Color: 1
Size: 332236 Color: 0
Size: 259823 Color: 1

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 373225 Color: 1
Size: 354918 Color: 1
Size: 271858 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 499089 Color: 1
Size: 250497 Color: 1
Size: 250415 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 385105 Color: 1
Size: 364557 Color: 1
Size: 250339 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 458144 Color: 1
Size: 270983 Color: 1
Size: 270874 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 429410 Color: 1
Size: 296416 Color: 1
Size: 274175 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 470342 Color: 1
Size: 271598 Color: 1
Size: 258061 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 386693 Color: 1
Size: 331701 Color: 1
Size: 281607 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 421341 Color: 1
Size: 321805 Color: 1
Size: 256855 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 375898 Color: 1
Size: 320529 Color: 1
Size: 303574 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 395943 Color: 1
Size: 334909 Color: 1
Size: 269149 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 367870 Color: 1
Size: 357784 Color: 1
Size: 274347 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 402542 Color: 1
Size: 321702 Color: 1
Size: 275757 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 375789 Color: 1
Size: 343697 Color: 1
Size: 280515 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 360614 Color: 1
Size: 340647 Color: 1
Size: 298740 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 434617 Color: 1
Size: 305233 Color: 1
Size: 260151 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 449463 Color: 1
Size: 293523 Color: 1
Size: 257015 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 434273 Color: 1
Size: 302500 Color: 1
Size: 263228 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 383577 Color: 1
Size: 313360 Color: 1
Size: 303064 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 369166 Color: 1
Size: 346072 Color: 1
Size: 284763 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 424633 Color: 1
Size: 301691 Color: 1
Size: 273677 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 448079 Color: 1
Size: 301164 Color: 1
Size: 250758 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 372513 Color: 1
Size: 343372 Color: 1
Size: 284116 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 389879 Color: 1
Size: 335047 Color: 1
Size: 275075 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 442332 Color: 1
Size: 279393 Color: 0
Size: 278276 Color: 1

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 416001 Color: 1
Size: 298839 Color: 1
Size: 285161 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 445789 Color: 1
Size: 287787 Color: 1
Size: 266425 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 389550 Color: 1
Size: 347204 Color: 1
Size: 263247 Color: 0

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 403884 Color: 1
Size: 309394 Color: 0
Size: 286723 Color: 1

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 452507 Color: 1
Size: 279312 Color: 1
Size: 268182 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 479207 Color: 1
Size: 265241 Color: 1
Size: 255553 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 381138 Color: 1
Size: 319052 Color: 1
Size: 299811 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 487322 Color: 1
Size: 261363 Color: 1
Size: 251316 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 347254 Color: 1
Size: 326634 Color: 0
Size: 326113 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 495395 Color: 1
Size: 253762 Color: 1
Size: 250844 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 370847 Color: 1
Size: 339945 Color: 1
Size: 289209 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 366281 Color: 1
Size: 361269 Color: 1
Size: 272451 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 489909 Color: 1
Size: 255498 Color: 1
Size: 254594 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 390541 Color: 1
Size: 308543 Color: 1
Size: 300917 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 404034 Color: 1
Size: 312547 Color: 1
Size: 283420 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 389162 Color: 1
Size: 316018 Color: 1
Size: 294821 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 357014 Color: 1
Size: 324458 Color: 1
Size: 318529 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 499267 Color: 1
Size: 250690 Color: 1
Size: 250044 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 492695 Color: 1
Size: 255807 Color: 1
Size: 251499 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 480907 Color: 1
Size: 263443 Color: 1
Size: 255651 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 405142 Color: 1
Size: 315615 Color: 1
Size: 279244 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 362190 Color: 1
Size: 334513 Color: 1
Size: 303298 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 472061 Color: 1
Size: 266779 Color: 1
Size: 261161 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 394451 Color: 1
Size: 347735 Color: 1
Size: 257815 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 470528 Color: 1
Size: 272281 Color: 1
Size: 257192 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 371640 Color: 1
Size: 370363 Color: 1
Size: 257998 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 396009 Color: 1
Size: 349842 Color: 1
Size: 254150 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 454945 Color: 1
Size: 282752 Color: 1
Size: 262304 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 350233 Color: 1
Size: 348297 Color: 1
Size: 301471 Color: 0

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 445766 Color: 1
Size: 298027 Color: 1
Size: 256208 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 432610 Color: 1
Size: 291438 Color: 1
Size: 275953 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 428201 Color: 1
Size: 318369 Color: 1
Size: 253431 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 371162 Color: 1
Size: 358410 Color: 1
Size: 270429 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 473253 Color: 1
Size: 275879 Color: 1
Size: 250869 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 361902 Color: 1
Size: 352379 Color: 1
Size: 285720 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 424096 Color: 1
Size: 305347 Color: 1
Size: 270558 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 421159 Color: 1
Size: 319109 Color: 1
Size: 259733 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 389574 Color: 1
Size: 305409 Color: 0
Size: 305018 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 380763 Color: 1
Size: 322286 Color: 0
Size: 296952 Color: 1

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 425606 Color: 1
Size: 321909 Color: 1
Size: 252486 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 388126 Color: 1
Size: 328366 Color: 1
Size: 283509 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 387271 Color: 1
Size: 340307 Color: 1
Size: 272423 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 344099 Color: 1
Size: 332168 Color: 1
Size: 323734 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 446458 Color: 1
Size: 282804 Color: 1
Size: 270739 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 436721 Color: 1
Size: 286163 Color: 1
Size: 277117 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 407931 Color: 1
Size: 303035 Color: 1
Size: 289035 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 430122 Color: 1
Size: 316488 Color: 1
Size: 253391 Color: 0

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 377046 Color: 1
Size: 342114 Color: 1
Size: 280841 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 370269 Color: 1
Size: 339403 Color: 1
Size: 290329 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 397921 Color: 1
Size: 347492 Color: 1
Size: 254588 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 466087 Color: 1
Size: 277129 Color: 1
Size: 256785 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 427795 Color: 1
Size: 310856 Color: 1
Size: 261350 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 482414 Color: 1
Size: 259163 Color: 1
Size: 258424 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 459937 Color: 1
Size: 273031 Color: 0
Size: 267033 Color: 1

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 378029 Color: 1
Size: 322297 Color: 1
Size: 299675 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 377338 Color: 1
Size: 348124 Color: 1
Size: 274539 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 389814 Color: 1
Size: 325146 Color: 1
Size: 285041 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 366772 Color: 1
Size: 328078 Color: 1
Size: 305151 Color: 0

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 348012 Color: 1
Size: 342684 Color: 1
Size: 309305 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 367984 Color: 1
Size: 353176 Color: 1
Size: 278841 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 415309 Color: 1
Size: 313852 Color: 1
Size: 270840 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 461755 Color: 1
Size: 278862 Color: 0
Size: 259384 Color: 1

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 363760 Color: 1
Size: 342089 Color: 1
Size: 294152 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 377342 Color: 1
Size: 331515 Color: 1
Size: 291144 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 482289 Color: 1
Size: 265863 Color: 1
Size: 251849 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 435571 Color: 1
Size: 295160 Color: 0
Size: 269270 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 489431 Color: 1
Size: 258415 Color: 1
Size: 252155 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 453962 Color: 1
Size: 292362 Color: 1
Size: 253677 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 371387 Color: 1
Size: 326305 Color: 1
Size: 302309 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 440209 Color: 1
Size: 282451 Color: 1
Size: 277341 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 466633 Color: 1
Size: 283292 Color: 1
Size: 250076 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 381729 Color: 1
Size: 347423 Color: 1
Size: 270849 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 444917 Color: 1
Size: 286585 Color: 0
Size: 268499 Color: 1

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 453491 Color: 1
Size: 288983 Color: 1
Size: 257527 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 355029 Color: 1
Size: 353530 Color: 1
Size: 291442 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 424817 Color: 1
Size: 288665 Color: 1
Size: 286519 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 485394 Color: 1
Size: 257494 Color: 0
Size: 257113 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 369276 Color: 1
Size: 338755 Color: 1
Size: 291970 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 401276 Color: 1
Size: 324102 Color: 1
Size: 274623 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 407720 Color: 1
Size: 318854 Color: 1
Size: 273427 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 420243 Color: 1
Size: 313218 Color: 1
Size: 266540 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 381121 Color: 1
Size: 313484 Color: 0
Size: 305396 Color: 1

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 374708 Color: 1
Size: 346531 Color: 1
Size: 278762 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 416086 Color: 1
Size: 304367 Color: 1
Size: 279548 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 417114 Color: 1
Size: 326919 Color: 1
Size: 255968 Color: 0

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 455364 Color: 1
Size: 293679 Color: 1
Size: 250958 Color: 0

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 456864 Color: 1
Size: 279588 Color: 1
Size: 263549 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 428352 Color: 1
Size: 313625 Color: 1
Size: 258024 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 452871 Color: 1
Size: 296966 Color: 1
Size: 250164 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 451461 Color: 1
Size: 279275 Color: 0
Size: 269265 Color: 1

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 482234 Color: 1
Size: 262816 Color: 1
Size: 254951 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 494009 Color: 1
Size: 253572 Color: 1
Size: 252420 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 420074 Color: 1
Size: 307225 Color: 1
Size: 272702 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 391596 Color: 1
Size: 317734 Color: 1
Size: 290671 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 376694 Color: 1
Size: 348992 Color: 1
Size: 274315 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 462256 Color: 1
Size: 282403 Color: 1
Size: 255342 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 452679 Color: 1
Size: 278665 Color: 1
Size: 268657 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 372045 Color: 1
Size: 353243 Color: 1
Size: 274713 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 449283 Color: 1
Size: 291416 Color: 1
Size: 259302 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 350187 Color: 1
Size: 343092 Color: 1
Size: 306722 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 379209 Color: 1
Size: 346832 Color: 1
Size: 273960 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 405714 Color: 1
Size: 303512 Color: 0
Size: 290775 Color: 1

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 389244 Color: 1
Size: 337195 Color: 1
Size: 273562 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 395815 Color: 1
Size: 323881 Color: 1
Size: 280305 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 479729 Color: 1
Size: 260343 Color: 1
Size: 259929 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 475913 Color: 1
Size: 271555 Color: 1
Size: 252533 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 351855 Color: 1
Size: 340958 Color: 1
Size: 307188 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 371141 Color: 1
Size: 352026 Color: 1
Size: 276834 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 396981 Color: 1
Size: 341972 Color: 1
Size: 261048 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 387307 Color: 1
Size: 324596 Color: 1
Size: 288098 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 398439 Color: 1
Size: 310790 Color: 1
Size: 290772 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 383880 Color: 1
Size: 339064 Color: 1
Size: 277057 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 439077 Color: 1
Size: 305315 Color: 1
Size: 255609 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 411350 Color: 1
Size: 311612 Color: 0
Size: 277039 Color: 1

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 452651 Color: 1
Size: 289857 Color: 1
Size: 257493 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 391069 Color: 1
Size: 315687 Color: 1
Size: 293245 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 451635 Color: 1
Size: 290328 Color: 1
Size: 258038 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 478355 Color: 1
Size: 270840 Color: 1
Size: 250806 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 396609 Color: 1
Size: 334596 Color: 1
Size: 268796 Color: 0

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 416235 Color: 1
Size: 311744 Color: 1
Size: 272022 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 398023 Color: 1
Size: 324797 Color: 1
Size: 277181 Color: 0

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 464710 Color: 1
Size: 274772 Color: 1
Size: 260519 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 371349 Color: 1
Size: 335783 Color: 1
Size: 292869 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 455644 Color: 1
Size: 278110 Color: 1
Size: 266247 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 387283 Color: 1
Size: 310247 Color: 1
Size: 302471 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 474493 Color: 1
Size: 269078 Color: 1
Size: 256430 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 413353 Color: 1
Size: 296732 Color: 0
Size: 289916 Color: 1

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 397433 Color: 1
Size: 327882 Color: 1
Size: 274686 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 399595 Color: 1
Size: 340649 Color: 1
Size: 259757 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 399305 Color: 1
Size: 342478 Color: 1
Size: 258218 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 452616 Color: 1
Size: 294790 Color: 1
Size: 252595 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 458689 Color: 1
Size: 284355 Color: 1
Size: 256957 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 443899 Color: 1
Size: 305741 Color: 1
Size: 250361 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 369967 Color: 1
Size: 354047 Color: 1
Size: 275987 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 436147 Color: 1
Size: 290994 Color: 1
Size: 272860 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 497711 Color: 1
Size: 251174 Color: 1
Size: 251116 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 389329 Color: 1
Size: 356668 Color: 1
Size: 254004 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 451111 Color: 1
Size: 285683 Color: 1
Size: 263207 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 373986 Color: 1
Size: 314075 Color: 1
Size: 311940 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 386399 Color: 1
Size: 352241 Color: 1
Size: 261361 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 379011 Color: 1
Size: 316015 Color: 1
Size: 304975 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 459378 Color: 1
Size: 285027 Color: 1
Size: 255596 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 384487 Color: 1
Size: 350294 Color: 1
Size: 265220 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 437948 Color: 1
Size: 287833 Color: 1
Size: 274220 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 372314 Color: 1
Size: 342994 Color: 1
Size: 284693 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 355949 Color: 1
Size: 336534 Color: 1
Size: 307518 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 361775 Color: 1
Size: 333973 Color: 1
Size: 304253 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 374176 Color: 1
Size: 365253 Color: 1
Size: 260572 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 398926 Color: 1
Size: 334196 Color: 1
Size: 266879 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 486898 Color: 1
Size: 257283 Color: 1
Size: 255820 Color: 0

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 449367 Color: 1
Size: 290700 Color: 1
Size: 259934 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 371547 Color: 1
Size: 316531 Color: 0
Size: 311923 Color: 1

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 379114 Color: 1
Size: 327972 Color: 1
Size: 292915 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 398248 Color: 1
Size: 334766 Color: 1
Size: 266987 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 386733 Color: 1
Size: 327117 Color: 1
Size: 286151 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 477840 Color: 1
Size: 270770 Color: 1
Size: 251391 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 349406 Color: 1
Size: 341374 Color: 1
Size: 309221 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 390849 Color: 1
Size: 312575 Color: 1
Size: 296577 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 404636 Color: 1
Size: 298754 Color: 1
Size: 296611 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 432521 Color: 1
Size: 303152 Color: 1
Size: 264328 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 487735 Color: 1
Size: 259226 Color: 1
Size: 253040 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 419646 Color: 1
Size: 312792 Color: 1
Size: 267563 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 496861 Color: 1
Size: 252116 Color: 1
Size: 251024 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 362703 Color: 1
Size: 349419 Color: 1
Size: 287879 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 429242 Color: 1
Size: 304033 Color: 0
Size: 266726 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 360774 Color: 1
Size: 322870 Color: 0
Size: 316357 Color: 1

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 407039 Color: 1
Size: 312149 Color: 1
Size: 280813 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 439088 Color: 1
Size: 308581 Color: 1
Size: 252332 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 390479 Color: 1
Size: 341493 Color: 1
Size: 268029 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 426515 Color: 1
Size: 295953 Color: 1
Size: 277533 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 397177 Color: 1
Size: 339004 Color: 1
Size: 263820 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 399057 Color: 1
Size: 320750 Color: 1
Size: 280194 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 496149 Color: 1
Size: 253620 Color: 1
Size: 250232 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 367515 Color: 1
Size: 339577 Color: 1
Size: 292909 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 379579 Color: 1
Size: 358709 Color: 1
Size: 261713 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 372833 Color: 1
Size: 345783 Color: 1
Size: 281385 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 381117 Color: 1
Size: 337245 Color: 1
Size: 281639 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 358626 Color: 1
Size: 321521 Color: 1
Size: 319854 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 367999 Color: 1
Size: 331137 Color: 1
Size: 300865 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 402705 Color: 1
Size: 321439 Color: 1
Size: 275857 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 365622 Color: 1
Size: 338077 Color: 1
Size: 296302 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 374491 Color: 1
Size: 342488 Color: 1
Size: 283022 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 444960 Color: 1
Size: 297932 Color: 1
Size: 257109 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 444179 Color: 1
Size: 288480 Color: 1
Size: 267342 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 474062 Color: 1
Size: 270674 Color: 1
Size: 255265 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 357350 Color: 1
Size: 352981 Color: 1
Size: 289670 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 403231 Color: 1
Size: 310192 Color: 1
Size: 286578 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 378542 Color: 1
Size: 338446 Color: 1
Size: 283013 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 387797 Color: 1
Size: 361696 Color: 1
Size: 250508 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 476328 Color: 1
Size: 271617 Color: 1
Size: 252056 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 367409 Color: 1
Size: 325056 Color: 1
Size: 307536 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 494338 Color: 1
Size: 254222 Color: 1
Size: 251441 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 356524 Color: 1
Size: 352159 Color: 1
Size: 291318 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 478558 Color: 1
Size: 265506 Color: 1
Size: 255937 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 358833 Color: 1
Size: 358282 Color: 1
Size: 282886 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 378550 Color: 1
Size: 311567 Color: 0
Size: 309884 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 434181 Color: 1
Size: 307953 Color: 1
Size: 257867 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 469699 Color: 1
Size: 276516 Color: 1
Size: 253786 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 447523 Color: 1
Size: 292407 Color: 1
Size: 260071 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 349384 Color: 1
Size: 346190 Color: 1
Size: 304427 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 478952 Color: 1
Size: 262201 Color: 0
Size: 258848 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 413668 Color: 1
Size: 321961 Color: 1
Size: 264372 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 357677 Color: 1
Size: 335590 Color: 1
Size: 306734 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 443280 Color: 1
Size: 299255 Color: 1
Size: 257466 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 491016 Color: 1
Size: 256087 Color: 0
Size: 252898 Color: 1

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 402926 Color: 1
Size: 314416 Color: 1
Size: 282659 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 420523 Color: 1
Size: 322228 Color: 1
Size: 257250 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 486171 Color: 1
Size: 259353 Color: 1
Size: 254477 Color: 0

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 372179 Color: 1
Size: 337795 Color: 1
Size: 290027 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 371806 Color: 1
Size: 353011 Color: 1
Size: 275184 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 417535 Color: 1
Size: 314468 Color: 1
Size: 267998 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 432978 Color: 1
Size: 306652 Color: 1
Size: 260371 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 385323 Color: 1
Size: 333238 Color: 1
Size: 281440 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 400186 Color: 1
Size: 319266 Color: 1
Size: 280549 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 419593 Color: 1
Size: 324773 Color: 1
Size: 255635 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 355492 Color: 1
Size: 342846 Color: 1
Size: 301663 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 395851 Color: 1
Size: 348371 Color: 1
Size: 255779 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 394548 Color: 1
Size: 346543 Color: 1
Size: 258910 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 438628 Color: 1
Size: 309938 Color: 1
Size: 251435 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 408590 Color: 1
Size: 334767 Color: 1
Size: 256644 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 456666 Color: 1
Size: 290490 Color: 1
Size: 252845 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 472186 Color: 1
Size: 274208 Color: 1
Size: 253607 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 365356 Color: 1
Size: 329300 Color: 0
Size: 305345 Color: 1

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 382480 Color: 1
Size: 345855 Color: 1
Size: 271666 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 399629 Color: 1
Size: 310089 Color: 1
Size: 290283 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 465592 Color: 1
Size: 281462 Color: 1
Size: 252947 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 421205 Color: 1
Size: 316678 Color: 1
Size: 262118 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 395543 Color: 1
Size: 316646 Color: 1
Size: 287812 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 451059 Color: 1
Size: 277434 Color: 1
Size: 271508 Color: 0

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 351773 Color: 1
Size: 334768 Color: 1
Size: 313460 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 489370 Color: 1
Size: 259179 Color: 1
Size: 251452 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 455339 Color: 1
Size: 288418 Color: 1
Size: 256244 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 482724 Color: 1
Size: 266323 Color: 1
Size: 250954 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 377791 Color: 1
Size: 359370 Color: 1
Size: 262840 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 367923 Color: 1
Size: 358398 Color: 1
Size: 273680 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 481375 Color: 1
Size: 267690 Color: 1
Size: 250936 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 412100 Color: 1
Size: 321504 Color: 1
Size: 266397 Color: 0

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 407433 Color: 1
Size: 329782 Color: 1
Size: 262786 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 451894 Color: 1
Size: 297828 Color: 1
Size: 250279 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 496798 Color: 1
Size: 252803 Color: 1
Size: 250400 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 452088 Color: 1
Size: 278214 Color: 0
Size: 269699 Color: 1

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 416168 Color: 1
Size: 300203 Color: 1
Size: 283630 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 373534 Color: 1
Size: 343375 Color: 1
Size: 283092 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 426265 Color: 1
Size: 316030 Color: 1
Size: 257706 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 477908 Color: 1
Size: 271717 Color: 1
Size: 250376 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 440985 Color: 1
Size: 303067 Color: 1
Size: 255949 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 480144 Color: 1
Size: 265372 Color: 1
Size: 254485 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 388835 Color: 1
Size: 332636 Color: 1
Size: 278530 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 439814 Color: 1
Size: 301117 Color: 1
Size: 259070 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 370894 Color: 1
Size: 320207 Color: 1
Size: 308900 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 380664 Color: 1
Size: 350890 Color: 1
Size: 268447 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 437072 Color: 1
Size: 305350 Color: 1
Size: 257579 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 368904 Color: 1
Size: 363461 Color: 1
Size: 267636 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 468560 Color: 1
Size: 277819 Color: 1
Size: 253622 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 385920 Color: 1
Size: 329647 Color: 1
Size: 284434 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 493932 Color: 1
Size: 256038 Color: 1
Size: 250031 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 446785 Color: 1
Size: 296230 Color: 1
Size: 256986 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 441422 Color: 1
Size: 307254 Color: 1
Size: 251325 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 403442 Color: 1
Size: 316929 Color: 1
Size: 279630 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 356339 Color: 1
Size: 337341 Color: 1
Size: 306321 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 387097 Color: 1
Size: 349498 Color: 1
Size: 263406 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 494588 Color: 1
Size: 254551 Color: 1
Size: 250862 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 456737 Color: 1
Size: 278426 Color: 1
Size: 264838 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 365854 Color: 1
Size: 345732 Color: 1
Size: 288415 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 452692 Color: 1
Size: 288611 Color: 1
Size: 258698 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 397982 Color: 1
Size: 325332 Color: 1
Size: 276687 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 362455 Color: 1
Size: 356793 Color: 1
Size: 280753 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 484980 Color: 1
Size: 261611 Color: 1
Size: 253410 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 465214 Color: 1
Size: 275234 Color: 1
Size: 259553 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 415082 Color: 1
Size: 311040 Color: 1
Size: 273879 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 404341 Color: 1
Size: 345147 Color: 1
Size: 250513 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 366543 Color: 1
Size: 346281 Color: 1
Size: 287177 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 465698 Color: 1
Size: 283206 Color: 1
Size: 251097 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 456204 Color: 1
Size: 280739 Color: 1
Size: 263058 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 423623 Color: 1
Size: 314550 Color: 1
Size: 261828 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 421859 Color: 1
Size: 323943 Color: 1
Size: 254199 Color: 0

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 456577 Color: 1
Size: 276090 Color: 1
Size: 267334 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 377011 Color: 1
Size: 336333 Color: 1
Size: 286657 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 363046 Color: 1
Size: 319644 Color: 1
Size: 317311 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 397670 Color: 1
Size: 301745 Color: 1
Size: 300586 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 444782 Color: 1
Size: 301556 Color: 1
Size: 253663 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 469628 Color: 1
Size: 277061 Color: 1
Size: 253312 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 401996 Color: 1
Size: 308358 Color: 0
Size: 289647 Color: 1

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 365892 Color: 1
Size: 329862 Color: 1
Size: 304247 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 433602 Color: 1
Size: 302129 Color: 1
Size: 264270 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 401889 Color: 1
Size: 336912 Color: 1
Size: 261200 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 421120 Color: 1
Size: 295166 Color: 1
Size: 283715 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 411273 Color: 1
Size: 311303 Color: 1
Size: 277425 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 450597 Color: 1
Size: 294441 Color: 1
Size: 254963 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 358447 Color: 1
Size: 350567 Color: 1
Size: 290987 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 400045 Color: 1
Size: 327747 Color: 1
Size: 272209 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 459389 Color: 1
Size: 275065 Color: 1
Size: 265547 Color: 0

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 407450 Color: 1
Size: 325065 Color: 1
Size: 267486 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 388887 Color: 1
Size: 332713 Color: 1
Size: 278401 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 392264 Color: 1
Size: 349883 Color: 1
Size: 257854 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 360454 Color: 1
Size: 352631 Color: 1
Size: 286916 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 417942 Color: 1
Size: 329037 Color: 1
Size: 253022 Color: 0

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 371203 Color: 1
Size: 345493 Color: 1
Size: 283305 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 375002 Color: 1
Size: 345054 Color: 1
Size: 279945 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 364729 Color: 1
Size: 363977 Color: 1
Size: 271295 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 410897 Color: 1
Size: 306304 Color: 1
Size: 282800 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 408168 Color: 1
Size: 317804 Color: 1
Size: 274029 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 423298 Color: 1
Size: 298466 Color: 0
Size: 278237 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 370324 Color: 1
Size: 329941 Color: 1
Size: 299736 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 433349 Color: 1
Size: 315103 Color: 1
Size: 251549 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 396520 Color: 1
Size: 329560 Color: 1
Size: 273921 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 417343 Color: 1
Size: 307218 Color: 1
Size: 275440 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 373350 Color: 1
Size: 325573 Color: 1
Size: 301078 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 367280 Color: 1
Size: 355786 Color: 1
Size: 276935 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 406518 Color: 1
Size: 336012 Color: 1
Size: 257471 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 407881 Color: 1
Size: 315836 Color: 1
Size: 276284 Color: 0

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 499989 Color: 1
Size: 250010 Color: 1
Size: 250002 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 385596 Color: 1
Size: 318872 Color: 1
Size: 295533 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 408563 Color: 1
Size: 338965 Color: 1
Size: 252473 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 389819 Color: 1
Size: 317545 Color: 1
Size: 292637 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 361409 Color: 1
Size: 342414 Color: 1
Size: 296178 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 416288 Color: 1
Size: 318117 Color: 1
Size: 265596 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 359863 Color: 1
Size: 326029 Color: 1
Size: 314109 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 403830 Color: 1
Size: 308650 Color: 1
Size: 287521 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 381430 Color: 1
Size: 330136 Color: 1
Size: 288435 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 386230 Color: 1
Size: 334887 Color: 1
Size: 278884 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 432764 Color: 1
Size: 287122 Color: 1
Size: 280115 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 400509 Color: 1
Size: 311674 Color: 1
Size: 287818 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 403939 Color: 1
Size: 321855 Color: 1
Size: 274207 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 468406 Color: 1
Size: 280540 Color: 1
Size: 251055 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 424054 Color: 1
Size: 316404 Color: 1
Size: 259543 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 470361 Color: 1
Size: 270796 Color: 1
Size: 258844 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 431814 Color: 1
Size: 305751 Color: 1
Size: 262436 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 366080 Color: 1
Size: 349041 Color: 1
Size: 284880 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 431401 Color: 1
Size: 313801 Color: 1
Size: 254799 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 442024 Color: 1
Size: 300637 Color: 1
Size: 257340 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 390182 Color: 1
Size: 312079 Color: 1
Size: 297740 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 483993 Color: 1
Size: 262491 Color: 1
Size: 253517 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 473724 Color: 1
Size: 273675 Color: 1
Size: 252602 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 362848 Color: 1
Size: 358888 Color: 1
Size: 278265 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 383860 Color: 1
Size: 311402 Color: 0
Size: 304739 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 428298 Color: 1
Size: 319917 Color: 1
Size: 251786 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 371200 Color: 1
Size: 348813 Color: 1
Size: 279988 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 428402 Color: 1
Size: 294841 Color: 1
Size: 276758 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 378148 Color: 1
Size: 346833 Color: 1
Size: 275020 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 485791 Color: 1
Size: 258465 Color: 1
Size: 255745 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 390358 Color: 1
Size: 336166 Color: 1
Size: 273477 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 483081 Color: 1
Size: 265855 Color: 1
Size: 251065 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 367346 Color: 1
Size: 348421 Color: 1
Size: 284234 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 399371 Color: 1
Size: 345259 Color: 1
Size: 255371 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 372611 Color: 1
Size: 359259 Color: 1
Size: 268131 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 407428 Color: 1
Size: 317412 Color: 0
Size: 275161 Color: 1

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 395884 Color: 1
Size: 330672 Color: 1
Size: 273445 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 379178 Color: 1
Size: 314789 Color: 1
Size: 306034 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 378518 Color: 1
Size: 341778 Color: 1
Size: 279705 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 414528 Color: 1
Size: 325746 Color: 1
Size: 259727 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 426755 Color: 1
Size: 292483 Color: 1
Size: 280763 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 374223 Color: 1
Size: 350767 Color: 1
Size: 275011 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 402173 Color: 1
Size: 333109 Color: 1
Size: 264719 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 360229 Color: 1
Size: 342123 Color: 1
Size: 297649 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 366647 Color: 1
Size: 346389 Color: 1
Size: 286965 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 466782 Color: 1
Size: 270416 Color: 1
Size: 262803 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 366530 Color: 1
Size: 331809 Color: 1
Size: 301662 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 364734 Color: 1
Size: 341738 Color: 1
Size: 293529 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 357130 Color: 1
Size: 351950 Color: 1
Size: 290921 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 499834 Color: 1
Size: 250108 Color: 1
Size: 250059 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 442783 Color: 1
Size: 299079 Color: 1
Size: 258139 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 423260 Color: 1
Size: 311044 Color: 1
Size: 265697 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 477952 Color: 1
Size: 265811 Color: 1
Size: 256238 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 464298 Color: 1
Size: 281950 Color: 1
Size: 253753 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 491839 Color: 1
Size: 255381 Color: 1
Size: 252781 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 399243 Color: 1
Size: 336512 Color: 1
Size: 264246 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 361016 Color: 1
Size: 330186 Color: 1
Size: 308799 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 463852 Color: 1
Size: 284679 Color: 1
Size: 251470 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 396728 Color: 1
Size: 312944 Color: 1
Size: 290329 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 405132 Color: 1
Size: 308664 Color: 1
Size: 286205 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 393353 Color: 1
Size: 334475 Color: 1
Size: 272173 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 400429 Color: 1
Size: 329202 Color: 1
Size: 270370 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 491091 Color: 1
Size: 257452 Color: 1
Size: 251458 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 384012 Color: 1
Size: 351226 Color: 1
Size: 264763 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 479802 Color: 1
Size: 270164 Color: 1
Size: 250035 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 373775 Color: 1
Size: 315831 Color: 1
Size: 310395 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 369135 Color: 1
Size: 340298 Color: 1
Size: 290568 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 441539 Color: 1
Size: 295607 Color: 1
Size: 262855 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 398105 Color: 1
Size: 346768 Color: 1
Size: 255128 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 360373 Color: 1
Size: 344084 Color: 1
Size: 295544 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 352985 Color: 1
Size: 350498 Color: 1
Size: 296518 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 467657 Color: 1
Size: 267564 Color: 1
Size: 264780 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 445191 Color: 1
Size: 279442 Color: 1
Size: 275368 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 375338 Color: 1
Size: 336765 Color: 1
Size: 287898 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 393764 Color: 1
Size: 333609 Color: 1
Size: 272628 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 421757 Color: 1
Size: 305088 Color: 1
Size: 273156 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 386640 Color: 1
Size: 357227 Color: 1
Size: 256134 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 360402 Color: 1
Size: 329688 Color: 0
Size: 309911 Color: 1

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 380853 Color: 1
Size: 341668 Color: 1
Size: 277480 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 437975 Color: 1
Size: 309606 Color: 1
Size: 252420 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 390393 Color: 1
Size: 320345 Color: 0
Size: 289263 Color: 1

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 460152 Color: 1
Size: 280452 Color: 1
Size: 259397 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 427791 Color: 1
Size: 309620 Color: 1
Size: 262590 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 419891 Color: 1
Size: 311355 Color: 1
Size: 268755 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 379072 Color: 1
Size: 348411 Color: 1
Size: 272518 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 483518 Color: 1
Size: 258819 Color: 1
Size: 257664 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 459957 Color: 1
Size: 288354 Color: 1
Size: 251690 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 418389 Color: 1
Size: 304781 Color: 1
Size: 276831 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 410679 Color: 1
Size: 323839 Color: 1
Size: 265483 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 384540 Color: 1
Size: 360125 Color: 1
Size: 255336 Color: 0

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 492636 Color: 1
Size: 256433 Color: 1
Size: 250932 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 486645 Color: 1
Size: 260805 Color: 1
Size: 252551 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 405521 Color: 1
Size: 321815 Color: 1
Size: 272665 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 374973 Color: 1
Size: 321728 Color: 1
Size: 303300 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 460200 Color: 1
Size: 280274 Color: 1
Size: 259527 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 450684 Color: 1
Size: 289563 Color: 1
Size: 259754 Color: 0

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 419050 Color: 1
Size: 316875 Color: 1
Size: 264076 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 389799 Color: 1
Size: 331663 Color: 1
Size: 278539 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 436827 Color: 1
Size: 291422 Color: 1
Size: 271752 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 408586 Color: 1
Size: 333403 Color: 1
Size: 258012 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 359190 Color: 1
Size: 350142 Color: 1
Size: 290669 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 388742 Color: 1
Size: 309906 Color: 1
Size: 301353 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 378863 Color: 1
Size: 325433 Color: 1
Size: 295705 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 499726 Color: 1
Size: 250241 Color: 1
Size: 250034 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 415745 Color: 1
Size: 319969 Color: 1
Size: 264287 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 374365 Color: 1
Size: 338290 Color: 1
Size: 287346 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 380179 Color: 1
Size: 345733 Color: 1
Size: 274089 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 441260 Color: 1
Size: 287386 Color: 1
Size: 271355 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 377095 Color: 1
Size: 334424 Color: 1
Size: 288482 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 472716 Color: 1
Size: 265081 Color: 1
Size: 262204 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 435355 Color: 1
Size: 285844 Color: 0
Size: 278802 Color: 1

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 450764 Color: 1
Size: 289855 Color: 1
Size: 259382 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 480355 Color: 1
Size: 263369 Color: 1
Size: 256277 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 422582 Color: 1
Size: 311485 Color: 1
Size: 265934 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 350073 Color: 1
Size: 344822 Color: 1
Size: 305106 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 383277 Color: 1
Size: 362842 Color: 1
Size: 253882 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 443794 Color: 1
Size: 281836 Color: 1
Size: 274371 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 407652 Color: 1
Size: 323887 Color: 1
Size: 268462 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 433391 Color: 1
Size: 297605 Color: 1
Size: 269005 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 425492 Color: 1
Size: 288560 Color: 1
Size: 285949 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 466822 Color: 1
Size: 276738 Color: 1
Size: 256441 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 364536 Color: 1
Size: 338828 Color: 1
Size: 296637 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 411429 Color: 1
Size: 316453 Color: 1
Size: 272119 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 493575 Color: 1
Size: 253800 Color: 1
Size: 252626 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 348549 Color: 1
Size: 343822 Color: 1
Size: 307630 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 473765 Color: 1
Size: 275576 Color: 1
Size: 250660 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 414210 Color: 1
Size: 319291 Color: 1
Size: 266500 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 456699 Color: 1
Size: 283926 Color: 1
Size: 259376 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 356982 Color: 1
Size: 331302 Color: 1
Size: 311717 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 354840 Color: 1
Size: 353483 Color: 1
Size: 291678 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 373729 Color: 1
Size: 364034 Color: 1
Size: 262238 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 372446 Color: 1
Size: 320750 Color: 1
Size: 306805 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 404813 Color: 1
Size: 316168 Color: 1
Size: 279020 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 440188 Color: 1
Size: 301730 Color: 1
Size: 258083 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 378265 Color: 1
Size: 328424 Color: 1
Size: 293312 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 477913 Color: 1
Size: 269831 Color: 1
Size: 252257 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 488414 Color: 1
Size: 261308 Color: 1
Size: 250279 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 354706 Color: 1
Size: 349564 Color: 1
Size: 295731 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 495179 Color: 1
Size: 254494 Color: 1
Size: 250328 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 369871 Color: 1
Size: 340348 Color: 1
Size: 289782 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 353741 Color: 1
Size: 324425 Color: 0
Size: 321835 Color: 1

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 427305 Color: 1
Size: 302783 Color: 1
Size: 269913 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 406493 Color: 1
Size: 310570 Color: 1
Size: 282938 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 440668 Color: 1
Size: 294135 Color: 1
Size: 265198 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 379187 Color: 1
Size: 334885 Color: 1
Size: 285929 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 496471 Color: 1
Size: 253341 Color: 1
Size: 250189 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 429910 Color: 1
Size: 314011 Color: 1
Size: 256080 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 379325 Color: 1
Size: 347254 Color: 1
Size: 273422 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 423465 Color: 1
Size: 309764 Color: 1
Size: 266772 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 359064 Color: 1
Size: 344024 Color: 1
Size: 296913 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 369366 Color: 1
Size: 361704 Color: 1
Size: 268931 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 452548 Color: 1
Size: 278309 Color: 1
Size: 269144 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 348897 Color: 1
Size: 346743 Color: 1
Size: 304361 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 474011 Color: 1
Size: 271669 Color: 1
Size: 254321 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 443607 Color: 1
Size: 287462 Color: 1
Size: 268932 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 383950 Color: 1
Size: 354759 Color: 1
Size: 261292 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 470426 Color: 1
Size: 274630 Color: 1
Size: 254945 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 434498 Color: 1
Size: 300380 Color: 1
Size: 265123 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 391639 Color: 1
Size: 336078 Color: 1
Size: 272284 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 412770 Color: 1
Size: 335758 Color: 1
Size: 251473 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 379724 Color: 1
Size: 359020 Color: 1
Size: 261257 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 453618 Color: 1
Size: 280092 Color: 1
Size: 266291 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 364590 Color: 1
Size: 347945 Color: 1
Size: 287466 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 383538 Color: 1
Size: 358942 Color: 1
Size: 257521 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 478293 Color: 1
Size: 269869 Color: 1
Size: 251839 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 450920 Color: 1
Size: 292511 Color: 1
Size: 256570 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 383219 Color: 1
Size: 314223 Color: 0
Size: 302559 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 493692 Color: 1
Size: 256247 Color: 1
Size: 250062 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 434675 Color: 1
Size: 295189 Color: 1
Size: 270137 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 431062 Color: 1
Size: 295615 Color: 1
Size: 273324 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 455443 Color: 1
Size: 287175 Color: 1
Size: 257383 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 414268 Color: 1
Size: 329896 Color: 1
Size: 255837 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 436953 Color: 1
Size: 294302 Color: 1
Size: 268746 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 492433 Color: 1
Size: 256718 Color: 1
Size: 250850 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 460756 Color: 1
Size: 275512 Color: 1
Size: 263733 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 477570 Color: 1
Size: 262906 Color: 0
Size: 259525 Color: 1

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 363141 Color: 1
Size: 330933 Color: 1
Size: 305927 Color: 0

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 448152 Color: 1
Size: 281784 Color: 0
Size: 270065 Color: 1

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 480779 Color: 1
Size: 262027 Color: 1
Size: 257195 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 390082 Color: 1
Size: 347347 Color: 1
Size: 262572 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 408810 Color: 1
Size: 303264 Color: 0
Size: 287927 Color: 1

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 364645 Color: 1
Size: 361411 Color: 1
Size: 273945 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 373858 Color: 1
Size: 370452 Color: 1
Size: 255691 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 406361 Color: 1
Size: 299010 Color: 1
Size: 294630 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 399737 Color: 1
Size: 313064 Color: 0
Size: 287200 Color: 1

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 389494 Color: 1
Size: 329375 Color: 1
Size: 281132 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 391191 Color: 1
Size: 341402 Color: 1
Size: 267408 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 363749 Color: 1
Size: 351958 Color: 1
Size: 284294 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 398341 Color: 1
Size: 327456 Color: 1
Size: 274204 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 450645 Color: 1
Size: 288502 Color: 1
Size: 260854 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 397047 Color: 1
Size: 316017 Color: 0
Size: 286937 Color: 1

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 454209 Color: 1
Size: 288248 Color: 1
Size: 257544 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 388639 Color: 1
Size: 331538 Color: 1
Size: 279824 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 380792 Color: 1
Size: 339685 Color: 1
Size: 279524 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 433686 Color: 1
Size: 305089 Color: 1
Size: 261226 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 390147 Color: 1
Size: 345000 Color: 1
Size: 264854 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 363033 Color: 1
Size: 319644 Color: 0
Size: 317324 Color: 1

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 378206 Color: 1
Size: 311209 Color: 1
Size: 310586 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 383392 Color: 1
Size: 323657 Color: 1
Size: 292952 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 371194 Color: 1
Size: 333684 Color: 1
Size: 295123 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 411891 Color: 1
Size: 315377 Color: 0
Size: 272733 Color: 1

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 460301 Color: 1
Size: 275684 Color: 0
Size: 264016 Color: 1

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 462368 Color: 1
Size: 284989 Color: 1
Size: 252644 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 393002 Color: 1
Size: 340335 Color: 1
Size: 266664 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 409870 Color: 1
Size: 320228 Color: 1
Size: 269903 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 438261 Color: 1
Size: 295017 Color: 1
Size: 266723 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 353528 Color: 1
Size: 347153 Color: 1
Size: 299320 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 419470 Color: 1
Size: 317519 Color: 1
Size: 263012 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 499496 Color: 1
Size: 250461 Color: 1
Size: 250044 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 492982 Color: 1
Size: 256029 Color: 1
Size: 250990 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 453817 Color: 1
Size: 286164 Color: 1
Size: 260020 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 357824 Color: 1
Size: 327866 Color: 1
Size: 314311 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 361619 Color: 1
Size: 333110 Color: 1
Size: 305272 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 441269 Color: 1
Size: 285014 Color: 1
Size: 273718 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 354547 Color: 1
Size: 352047 Color: 1
Size: 293407 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 372734 Color: 1
Size: 317597 Color: 1
Size: 309670 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 471416 Color: 1
Size: 271383 Color: 1
Size: 257202 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 368842 Color: 1
Size: 326504 Color: 1
Size: 304655 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 386124 Color: 1
Size: 343507 Color: 1
Size: 270370 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 497014 Color: 1
Size: 251953 Color: 0
Size: 251034 Color: 1

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 410969 Color: 1
Size: 301974 Color: 0
Size: 287058 Color: 1

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 387428 Color: 1
Size: 307717 Color: 1
Size: 304856 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 378219 Color: 1
Size: 353102 Color: 1
Size: 268680 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 386587 Color: 1
Size: 360140 Color: 1
Size: 253274 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 364512 Color: 1
Size: 324506 Color: 0
Size: 310983 Color: 1

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 476925 Color: 1
Size: 270255 Color: 1
Size: 252821 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 393265 Color: 1
Size: 311912 Color: 0
Size: 294824 Color: 1

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 407493 Color: 1
Size: 300477 Color: 1
Size: 292031 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 402603 Color: 1
Size: 300504 Color: 1
Size: 296894 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 413764 Color: 1
Size: 302135 Color: 1
Size: 284102 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 366382 Color: 1
Size: 362034 Color: 1
Size: 271585 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 486576 Color: 1
Size: 261917 Color: 1
Size: 251508 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 493782 Color: 1
Size: 255141 Color: 1
Size: 251078 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 365194 Color: 1
Size: 351876 Color: 1
Size: 282931 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 381107 Color: 1
Size: 313043 Color: 1
Size: 305851 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 352458 Color: 1
Size: 349875 Color: 1
Size: 297668 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 370324 Color: 1
Size: 364381 Color: 1
Size: 265296 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 361857 Color: 1
Size: 338150 Color: 1
Size: 299994 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 377201 Color: 1
Size: 359633 Color: 1
Size: 263167 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 347984 Color: 1
Size: 340910 Color: 1
Size: 311107 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 361506 Color: 1
Size: 355199 Color: 1
Size: 283296 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 470048 Color: 1
Size: 268730 Color: 1
Size: 261223 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 371118 Color: 1
Size: 329721 Color: 1
Size: 299162 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 363861 Color: 1
Size: 361889 Color: 1
Size: 274251 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 364835 Color: 1
Size: 364476 Color: 1
Size: 270690 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 352592 Color: 1
Size: 347732 Color: 1
Size: 299677 Color: 0

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 353635 Color: 1
Size: 348235 Color: 1
Size: 298131 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 357930 Color: 1
Size: 345542 Color: 1
Size: 296529 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 354300 Color: 1
Size: 331945 Color: 1
Size: 313756 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 415378 Color: 1
Size: 310334 Color: 1
Size: 274289 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 370332 Color: 1
Size: 355170 Color: 1
Size: 274499 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 422269 Color: 1
Size: 301960 Color: 0
Size: 275772 Color: 1

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 374780 Color: 1
Size: 349850 Color: 1
Size: 275371 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 395375 Color: 1
Size: 347966 Color: 1
Size: 256660 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 364908 Color: 1
Size: 362343 Color: 1
Size: 272750 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 354071 Color: 1
Size: 335582 Color: 1
Size: 310348 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 366832 Color: 1
Size: 364662 Color: 1
Size: 268507 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 371343 Color: 1
Size: 348790 Color: 1
Size: 279868 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 373230 Color: 1
Size: 353995 Color: 1
Size: 272776 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 408312 Color: 1
Size: 334170 Color: 1
Size: 257519 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 368140 Color: 1
Size: 333331 Color: 1
Size: 298530 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 359133 Color: 1
Size: 328718 Color: 0
Size: 312150 Color: 1

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 407838 Color: 1
Size: 329482 Color: 1
Size: 262681 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 361172 Color: 1
Size: 359776 Color: 1
Size: 279053 Color: 0

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 374012 Color: 1
Size: 355607 Color: 1
Size: 270382 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 383600 Color: 1
Size: 314926 Color: 1
Size: 301475 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 367713 Color: 1
Size: 331137 Color: 1
Size: 301151 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 410022 Color: 1
Size: 299532 Color: 1
Size: 290447 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 358968 Color: 1
Size: 325132 Color: 1
Size: 315901 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 356281 Color: 1
Size: 327131 Color: 1
Size: 316589 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 358395 Color: 1
Size: 334773 Color: 1
Size: 306833 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 354687 Color: 1
Size: 349174 Color: 1
Size: 296140 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 356011 Color: 1
Size: 339213 Color: 1
Size: 304777 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 413680 Color: 1
Size: 322479 Color: 1
Size: 263842 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 366562 Color: 1
Size: 342019 Color: 1
Size: 291420 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 341700 Color: 1
Size: 338711 Color: 1
Size: 319590 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 394458 Color: 1
Size: 310529 Color: 0
Size: 295014 Color: 1

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 355977 Color: 1
Size: 350284 Color: 1
Size: 293740 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 407093 Color: 1
Size: 302022 Color: 1
Size: 290886 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 350240 Color: 1
Size: 350020 Color: 1
Size: 299741 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 348315 Color: 1
Size: 346209 Color: 1
Size: 305477 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 357458 Color: 1
Size: 328780 Color: 0
Size: 313763 Color: 1

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 394103 Color: 1
Size: 335471 Color: 1
Size: 270427 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 385625 Color: 1
Size: 329514 Color: 1
Size: 284862 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 360885 Color: 1
Size: 357721 Color: 1
Size: 281395 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 353608 Color: 1
Size: 325335 Color: 1
Size: 321058 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 362746 Color: 1
Size: 344479 Color: 1
Size: 292776 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 356302 Color: 1
Size: 333160 Color: 1
Size: 310539 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 355983 Color: 1
Size: 329094 Color: 1
Size: 314924 Color: 0

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 380514 Color: 1
Size: 316335 Color: 1
Size: 303152 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 353041 Color: 1
Size: 330277 Color: 1
Size: 316683 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 350951 Color: 1
Size: 337129 Color: 1
Size: 311921 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 352388 Color: 1
Size: 349904 Color: 1
Size: 297709 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 358555 Color: 1
Size: 341266 Color: 1
Size: 300180 Color: 0

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 368576 Color: 1
Size: 329446 Color: 1
Size: 301979 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 361613 Color: 1
Size: 338681 Color: 1
Size: 299707 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 363042 Color: 1
Size: 329596 Color: 1
Size: 307363 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 363360 Color: 1
Size: 330294 Color: 1
Size: 306347 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 363468 Color: 1
Size: 320164 Color: 1
Size: 316369 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 363489 Color: 1
Size: 344926 Color: 1
Size: 291586 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 363531 Color: 1
Size: 333859 Color: 1
Size: 302611 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 364209 Color: 1
Size: 335874 Color: 1
Size: 299918 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 403819 Color: 1
Size: 311545 Color: 1
Size: 284637 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 365321 Color: 1
Size: 325178 Color: 1
Size: 309502 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 361909 Color: 1
Size: 340741 Color: 1
Size: 297351 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 363498 Color: 1
Size: 346465 Color: 1
Size: 290038 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 365601 Color: 1
Size: 332487 Color: 1
Size: 301913 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 365831 Color: 1
Size: 339785 Color: 1
Size: 294385 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 365928 Color: 1
Size: 328504 Color: 0
Size: 305569 Color: 1

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 366273 Color: 1
Size: 335651 Color: 1
Size: 298077 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 366371 Color: 1
Size: 316832 Color: 0
Size: 316798 Color: 1

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 366412 Color: 1
Size: 346979 Color: 1
Size: 286610 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 366573 Color: 1
Size: 342590 Color: 1
Size: 290838 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 366625 Color: 1
Size: 330561 Color: 1
Size: 302815 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 366718 Color: 1
Size: 321506 Color: 0
Size: 311777 Color: 1

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 367057 Color: 1
Size: 349047 Color: 1
Size: 283897 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 367237 Color: 1
Size: 351997 Color: 1
Size: 280767 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 367263 Color: 1
Size: 339178 Color: 1
Size: 293560 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 367704 Color: 1
Size: 337269 Color: 1
Size: 295028 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 379426 Color: 1
Size: 339014 Color: 1
Size: 281561 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 367894 Color: 1
Size: 343853 Color: 1
Size: 288254 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 368163 Color: 1
Size: 346958 Color: 1
Size: 284880 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 368268 Color: 1
Size: 332089 Color: 1
Size: 299644 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 368476 Color: 1
Size: 328629 Color: 1
Size: 302896 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 368592 Color: 1
Size: 337029 Color: 1
Size: 294380 Color: 0

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 368625 Color: 1
Size: 324960 Color: 1
Size: 306416 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 368685 Color: 1
Size: 335577 Color: 1
Size: 295739 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 369250 Color: 1
Size: 345798 Color: 1
Size: 284953 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 369077 Color: 1
Size: 356500 Color: 1
Size: 274424 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 369421 Color: 1
Size: 340783 Color: 1
Size: 289797 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 369531 Color: 1
Size: 340518 Color: 1
Size: 289952 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 369829 Color: 1
Size: 334340 Color: 1
Size: 295832 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 369969 Color: 1
Size: 320169 Color: 0
Size: 309863 Color: 1

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 370294 Color: 1
Size: 327592 Color: 1
Size: 302115 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 370377 Color: 1
Size: 337509 Color: 1
Size: 292115 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 370414 Color: 1
Size: 339295 Color: 1
Size: 290292 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 370471 Color: 1
Size: 340759 Color: 1
Size: 288771 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 371002 Color: 1
Size: 357587 Color: 1
Size: 271412 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 371024 Color: 1
Size: 316714 Color: 1
Size: 312263 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 371148 Color: 1
Size: 343876 Color: 1
Size: 284977 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 371155 Color: 1
Size: 334415 Color: 1
Size: 294431 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 371308 Color: 1
Size: 328116 Color: 1
Size: 300577 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 371314 Color: 1
Size: 340897 Color: 1
Size: 287790 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 371421 Color: 1
Size: 333939 Color: 1
Size: 294641 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 371454 Color: 1
Size: 335041 Color: 1
Size: 293506 Color: 0

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 371890 Color: 1
Size: 346669 Color: 1
Size: 281442 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 371947 Color: 1
Size: 324825 Color: 0
Size: 303229 Color: 1

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 372451 Color: 1
Size: 318482 Color: 1
Size: 309068 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 372714 Color: 1
Size: 346133 Color: 1
Size: 281154 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 372935 Color: 1
Size: 315357 Color: 0
Size: 311709 Color: 1

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 372964 Color: 1
Size: 341276 Color: 1
Size: 285761 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 373038 Color: 1
Size: 335243 Color: 1
Size: 291720 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 373060 Color: 1
Size: 327089 Color: 1
Size: 299852 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 373384 Color: 1
Size: 356573 Color: 1
Size: 270044 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 373413 Color: 1
Size: 328661 Color: 1
Size: 297927 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 373620 Color: 1
Size: 323594 Color: 1
Size: 302787 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 373684 Color: 1
Size: 338799 Color: 1
Size: 287518 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 374688 Color: 1
Size: 339768 Color: 1
Size: 285545 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 375089 Color: 1
Size: 344174 Color: 1
Size: 280738 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 375096 Color: 1
Size: 341815 Color: 1
Size: 283090 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 375323 Color: 1
Size: 337372 Color: 1
Size: 287306 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 375799 Color: 1
Size: 369072 Color: 1
Size: 255130 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 375823 Color: 1
Size: 353312 Color: 1
Size: 270866 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 375866 Color: 1
Size: 347557 Color: 1
Size: 276578 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 397083 Color: 1
Size: 352569 Color: 1
Size: 250349 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 376601 Color: 1
Size: 335992 Color: 1
Size: 287408 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 376668 Color: 1
Size: 319011 Color: 1
Size: 304322 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 376943 Color: 1
Size: 336883 Color: 1
Size: 286175 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 376954 Color: 1
Size: 354204 Color: 1
Size: 268843 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 377053 Color: 1
Size: 369972 Color: 1
Size: 252976 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 377168 Color: 1
Size: 351054 Color: 1
Size: 271779 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 377391 Color: 1
Size: 356571 Color: 1
Size: 266039 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 377419 Color: 1
Size: 351869 Color: 1
Size: 270713 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 377607 Color: 1
Size: 361681 Color: 1
Size: 260713 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 377611 Color: 1
Size: 347084 Color: 1
Size: 275306 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 377625 Color: 1
Size: 330182 Color: 1
Size: 292194 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 377861 Color: 1
Size: 340535 Color: 1
Size: 281605 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 377871 Color: 1
Size: 329137 Color: 1
Size: 292993 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 378106 Color: 1
Size: 353052 Color: 1
Size: 268843 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 378117 Color: 1
Size: 346808 Color: 1
Size: 275076 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 378348 Color: 1
Size: 343191 Color: 1
Size: 278462 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 378365 Color: 1
Size: 338864 Color: 1
Size: 282772 Color: 0

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 378397 Color: 1
Size: 339101 Color: 1
Size: 282503 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 378447 Color: 1
Size: 330921 Color: 1
Size: 290633 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 378707 Color: 1
Size: 313739 Color: 0
Size: 307555 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 368305 Color: 1
Size: 349083 Color: 1
Size: 282613 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 378901 Color: 1
Size: 327464 Color: 1
Size: 293636 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 378941 Color: 1
Size: 339852 Color: 1
Size: 281208 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 379066 Color: 1
Size: 321323 Color: 1
Size: 299612 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 379200 Color: 1
Size: 325819 Color: 0
Size: 294982 Color: 1

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 379254 Color: 1
Size: 347106 Color: 1
Size: 273641 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 379830 Color: 1
Size: 327124 Color: 1
Size: 293047 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 379846 Color: 1
Size: 321202 Color: 1
Size: 298953 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 379993 Color: 1
Size: 333716 Color: 1
Size: 286292 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 366274 Color: 1
Size: 330649 Color: 1
Size: 303078 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 380386 Color: 1
Size: 357061 Color: 1
Size: 262554 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 380529 Color: 1
Size: 353872 Color: 1
Size: 265600 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 380637 Color: 1
Size: 338627 Color: 1
Size: 280737 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 380702 Color: 1
Size: 327669 Color: 1
Size: 291630 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 380713 Color: 1
Size: 342137 Color: 1
Size: 277151 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 380865 Color: 1
Size: 329220 Color: 1
Size: 289916 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 380970 Color: 1
Size: 337022 Color: 1
Size: 282009 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 382529 Color: 1
Size: 348218 Color: 1
Size: 269254 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 369872 Color: 1
Size: 336728 Color: 1
Size: 293401 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 381498 Color: 1
Size: 331586 Color: 1
Size: 286917 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 381591 Color: 1
Size: 342954 Color: 1
Size: 275456 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 381593 Color: 1
Size: 335170 Color: 1
Size: 283238 Color: 0

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 381633 Color: 1
Size: 329744 Color: 1
Size: 288624 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 382023 Color: 1
Size: 328168 Color: 1
Size: 289810 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 382081 Color: 1
Size: 342180 Color: 1
Size: 275740 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 382618 Color: 1
Size: 322591 Color: 1
Size: 294792 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 382798 Color: 1
Size: 313154 Color: 1
Size: 304049 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 382843 Color: 1
Size: 335607 Color: 1
Size: 281551 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 383153 Color: 1
Size: 339772 Color: 1
Size: 277076 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 413355 Color: 1
Size: 320274 Color: 1
Size: 266372 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 383283 Color: 1
Size: 337640 Color: 1
Size: 279078 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 376664 Color: 1
Size: 340228 Color: 1
Size: 283109 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 383735 Color: 1
Size: 334128 Color: 1
Size: 282138 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 383999 Color: 1
Size: 327573 Color: 1
Size: 288429 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 384046 Color: 1
Size: 335734 Color: 1
Size: 280221 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 384154 Color: 1
Size: 310746 Color: 0
Size: 305101 Color: 1

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 384325 Color: 1
Size: 314510 Color: 0
Size: 301166 Color: 1

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 400246 Color: 1
Size: 331093 Color: 1
Size: 268662 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 384424 Color: 1
Size: 331921 Color: 1
Size: 283656 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 384740 Color: 1
Size: 347115 Color: 1
Size: 268146 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 384792 Color: 1
Size: 313427 Color: 0
Size: 301782 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 385033 Color: 1
Size: 323591 Color: 1
Size: 291377 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 385059 Color: 1
Size: 333591 Color: 1
Size: 281351 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 440057 Color: 1
Size: 308301 Color: 1
Size: 251643 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 385622 Color: 1
Size: 343466 Color: 1
Size: 270913 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 385891 Color: 1
Size: 349323 Color: 1
Size: 264787 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 385904 Color: 1
Size: 334989 Color: 1
Size: 279108 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 380454 Color: 1
Size: 342601 Color: 1
Size: 276946 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 386345 Color: 1
Size: 349314 Color: 1
Size: 264342 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 429700 Color: 1
Size: 293932 Color: 1
Size: 276369 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 386409 Color: 1
Size: 314683 Color: 0
Size: 298909 Color: 1

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 386770 Color: 1
Size: 346260 Color: 1
Size: 266971 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 386942 Color: 1
Size: 355569 Color: 1
Size: 257490 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 387163 Color: 1
Size: 346877 Color: 1
Size: 265961 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 387439 Color: 1
Size: 336629 Color: 1
Size: 275933 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 387512 Color: 1
Size: 350425 Color: 1
Size: 262064 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 387541 Color: 1
Size: 356633 Color: 1
Size: 255827 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 387974 Color: 1
Size: 328498 Color: 1
Size: 283529 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 388070 Color: 1
Size: 343210 Color: 1
Size: 268721 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 388080 Color: 1
Size: 341127 Color: 1
Size: 270794 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 388196 Color: 1
Size: 347408 Color: 1
Size: 264397 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 388311 Color: 1
Size: 352213 Color: 1
Size: 259477 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 388508 Color: 1
Size: 335138 Color: 1
Size: 276355 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 388574 Color: 1
Size: 356794 Color: 1
Size: 254633 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 388846 Color: 1
Size: 334217 Color: 1
Size: 276938 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 388905 Color: 1
Size: 328157 Color: 1
Size: 282939 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 389115 Color: 1
Size: 305462 Color: 1
Size: 305424 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 389121 Color: 1
Size: 319391 Color: 1
Size: 291489 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 389128 Color: 1
Size: 350661 Color: 1
Size: 260212 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 389382 Color: 1
Size: 336629 Color: 1
Size: 273990 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 389468 Color: 1
Size: 314475 Color: 0
Size: 296058 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 389563 Color: 1
Size: 328060 Color: 1
Size: 282378 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 389580 Color: 1
Size: 349663 Color: 1
Size: 260758 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 389731 Color: 1
Size: 326388 Color: 1
Size: 283882 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 389742 Color: 1
Size: 347145 Color: 1
Size: 263114 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 389803 Color: 1
Size: 340808 Color: 1
Size: 269390 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 389809 Color: 1
Size: 329500 Color: 1
Size: 280692 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 389825 Color: 1
Size: 346594 Color: 1
Size: 263582 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 390241 Color: 1
Size: 313168 Color: 0
Size: 296592 Color: 1

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 390380 Color: 1
Size: 320009 Color: 1
Size: 289612 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 390414 Color: 1
Size: 313894 Color: 1
Size: 295693 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 390551 Color: 1
Size: 322435 Color: 1
Size: 287015 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 390660 Color: 1
Size: 305295 Color: 1
Size: 304046 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 369756 Color: 1
Size: 329805 Color: 0
Size: 300440 Color: 1

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 390836 Color: 1
Size: 341883 Color: 1
Size: 267282 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 390849 Color: 1
Size: 338939 Color: 1
Size: 270213 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 390881 Color: 1
Size: 313759 Color: 1
Size: 295361 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 391021 Color: 1
Size: 327945 Color: 1
Size: 281035 Color: 0

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 391051 Color: 1
Size: 339071 Color: 1
Size: 269879 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 391165 Color: 1
Size: 307063 Color: 1
Size: 301773 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 391398 Color: 1
Size: 341730 Color: 1
Size: 266873 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 391814 Color: 1
Size: 317091 Color: 1
Size: 291096 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 392020 Color: 1
Size: 344300 Color: 1
Size: 263681 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 392345 Color: 1
Size: 339071 Color: 1
Size: 268585 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 392624 Color: 1
Size: 321898 Color: 1
Size: 285479 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 392715 Color: 1
Size: 324058 Color: 1
Size: 283228 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 392733 Color: 1
Size: 312456 Color: 1
Size: 294812 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 393112 Color: 1
Size: 329454 Color: 1
Size: 277435 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 393472 Color: 1
Size: 338511 Color: 1
Size: 268018 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 361101 Color: 1
Size: 352724 Color: 1
Size: 286176 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 393826 Color: 1
Size: 333744 Color: 1
Size: 272431 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 394036 Color: 1
Size: 304427 Color: 1
Size: 301538 Color: 0

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 394058 Color: 1
Size: 311701 Color: 1
Size: 294242 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 394141 Color: 1
Size: 354981 Color: 1
Size: 250879 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 394311 Color: 1
Size: 331835 Color: 1
Size: 273855 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 394335 Color: 1
Size: 333797 Color: 1
Size: 271869 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 394394 Color: 1
Size: 330528 Color: 1
Size: 275079 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 395290 Color: 1
Size: 308011 Color: 1
Size: 296700 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 395456 Color: 1
Size: 336836 Color: 1
Size: 267709 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 395511 Color: 1
Size: 315602 Color: 1
Size: 288888 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 395573 Color: 1
Size: 328266 Color: 1
Size: 276162 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 412278 Color: 1
Size: 321287 Color: 1
Size: 266436 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 420981 Color: 1
Size: 303216 Color: 1
Size: 275804 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 396106 Color: 1
Size: 319644 Color: 0
Size: 284251 Color: 1

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 396168 Color: 1
Size: 311814 Color: 1
Size: 292019 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 396172 Color: 1
Size: 350061 Color: 1
Size: 253768 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 396417 Color: 1
Size: 323370 Color: 1
Size: 280214 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 396429 Color: 1
Size: 303335 Color: 1
Size: 300237 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 416645 Color: 1
Size: 304511 Color: 1
Size: 278845 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 396824 Color: 1
Size: 338434 Color: 1
Size: 264743 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 396874 Color: 1
Size: 317530 Color: 1
Size: 285597 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 397165 Color: 1
Size: 323096 Color: 1
Size: 279740 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 397223 Color: 1
Size: 320547 Color: 1
Size: 282231 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 397320 Color: 1
Size: 321821 Color: 1
Size: 280860 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 397415 Color: 1
Size: 316022 Color: 1
Size: 286564 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 397507 Color: 1
Size: 312993 Color: 1
Size: 289501 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 397624 Color: 1
Size: 334274 Color: 1
Size: 268103 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 397891 Color: 1
Size: 303395 Color: 0
Size: 298715 Color: 1

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 397970 Color: 1
Size: 332609 Color: 1
Size: 269422 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 398059 Color: 1
Size: 329335 Color: 1
Size: 272607 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 398072 Color: 1
Size: 344420 Color: 1
Size: 257509 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 398194 Color: 1
Size: 342075 Color: 1
Size: 259732 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 398673 Color: 1
Size: 332480 Color: 1
Size: 268848 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 398843 Color: 1
Size: 335046 Color: 1
Size: 266112 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 399095 Color: 1
Size: 333529 Color: 1
Size: 267377 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 399285 Color: 1
Size: 306188 Color: 1
Size: 294528 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 399485 Color: 1
Size: 317371 Color: 1
Size: 283145 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 399522 Color: 1
Size: 337073 Color: 1
Size: 263406 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 399751 Color: 1
Size: 302272 Color: 1
Size: 297978 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 399806 Color: 1
Size: 328454 Color: 1
Size: 271741 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 399989 Color: 1
Size: 326789 Color: 1
Size: 273223 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 400277 Color: 1
Size: 300153 Color: 1
Size: 299571 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 400381 Color: 1
Size: 320081 Color: 1
Size: 279539 Color: 0

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 400422 Color: 1
Size: 335543 Color: 1
Size: 264036 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 400469 Color: 1
Size: 324143 Color: 1
Size: 275389 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 400659 Color: 1
Size: 311657 Color: 1
Size: 287685 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 400859 Color: 1
Size: 320560 Color: 1
Size: 278582 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 400976 Color: 1
Size: 331625 Color: 1
Size: 267400 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 401112 Color: 1
Size: 318793 Color: 1
Size: 280096 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 401118 Color: 1
Size: 339780 Color: 1
Size: 259103 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 401317 Color: 1
Size: 338287 Color: 1
Size: 260397 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 401377 Color: 1
Size: 312445 Color: 1
Size: 286179 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 401537 Color: 1
Size: 318741 Color: 1
Size: 279723 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 401538 Color: 1
Size: 328966 Color: 1
Size: 269497 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 401781 Color: 1
Size: 335163 Color: 1
Size: 263057 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 401815 Color: 1
Size: 326340 Color: 1
Size: 271846 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 402164 Color: 1
Size: 333108 Color: 1
Size: 264729 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 402249 Color: 1
Size: 301588 Color: 1
Size: 296164 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 388855 Color: 1
Size: 355275 Color: 1
Size: 255871 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 402375 Color: 1
Size: 309657 Color: 1
Size: 287969 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 402421 Color: 1
Size: 318275 Color: 1
Size: 279305 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 402506 Color: 1
Size: 340212 Color: 1
Size: 257283 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 402752 Color: 1
Size: 328295 Color: 1
Size: 268954 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 402849 Color: 1
Size: 324101 Color: 1
Size: 273051 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 402938 Color: 1
Size: 330552 Color: 1
Size: 266511 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 402980 Color: 1
Size: 335748 Color: 1
Size: 261273 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 403170 Color: 1
Size: 315793 Color: 1
Size: 281038 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 403271 Color: 1
Size: 342734 Color: 1
Size: 253996 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 403288 Color: 1
Size: 331873 Color: 1
Size: 264840 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 403400 Color: 1
Size: 325550 Color: 1
Size: 271051 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 403473 Color: 1
Size: 310195 Color: 1
Size: 286333 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 403784 Color: 1
Size: 331972 Color: 1
Size: 264245 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 403933 Color: 1
Size: 332841 Color: 1
Size: 263227 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 403999 Color: 1
Size: 323843 Color: 1
Size: 272159 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 404011 Color: 1
Size: 325340 Color: 1
Size: 270650 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 404273 Color: 1
Size: 344676 Color: 1
Size: 251052 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 404296 Color: 1
Size: 329562 Color: 1
Size: 266143 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 404817 Color: 1
Size: 334948 Color: 1
Size: 260236 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 404842 Color: 1
Size: 314452 Color: 1
Size: 280707 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 404995 Color: 1
Size: 329633 Color: 1
Size: 265373 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 405052 Color: 1
Size: 342588 Color: 1
Size: 252361 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 405330 Color: 1
Size: 336298 Color: 1
Size: 258373 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 405484 Color: 1
Size: 323431 Color: 1
Size: 271086 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 405556 Color: 1
Size: 334349 Color: 1
Size: 260096 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 433128 Color: 1
Size: 298131 Color: 0
Size: 268742 Color: 1

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 405850 Color: 1
Size: 317728 Color: 1
Size: 276423 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 405942 Color: 1
Size: 297138 Color: 1
Size: 296921 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 406004 Color: 1
Size: 324789 Color: 1
Size: 269208 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 408230 Color: 1
Size: 298921 Color: 1
Size: 292850 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 406241 Color: 1
Size: 331946 Color: 1
Size: 261814 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 406531 Color: 1
Size: 299946 Color: 1
Size: 293524 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 406585 Color: 1
Size: 316849 Color: 1
Size: 276567 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 406594 Color: 1
Size: 313851 Color: 1
Size: 279556 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 406907 Color: 1
Size: 321281 Color: 1
Size: 271813 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 407095 Color: 1
Size: 330519 Color: 1
Size: 262387 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 362964 Color: 1
Size: 318852 Color: 1
Size: 318185 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 407634 Color: 1
Size: 320890 Color: 1
Size: 271477 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 407660 Color: 1
Size: 303081 Color: 1
Size: 289260 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 407814 Color: 1
Size: 324469 Color: 1
Size: 267718 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 408059 Color: 1
Size: 341426 Color: 1
Size: 250516 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 408163 Color: 1
Size: 332736 Color: 1
Size: 259102 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 396194 Color: 1
Size: 321127 Color: 1
Size: 282680 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 408529 Color: 1
Size: 328465 Color: 1
Size: 263007 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 362765 Color: 1
Size: 328568 Color: 1
Size: 308668 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 408643 Color: 1
Size: 303829 Color: 1
Size: 287529 Color: 0

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 408666 Color: 1
Size: 322569 Color: 1
Size: 268766 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 409015 Color: 1
Size: 311018 Color: 1
Size: 279968 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 409295 Color: 1
Size: 335026 Color: 1
Size: 255680 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 409487 Color: 1
Size: 312596 Color: 1
Size: 277918 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 398419 Color: 1
Size: 327660 Color: 1
Size: 273922 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 409625 Color: 1
Size: 312874 Color: 1
Size: 277502 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 409658 Color: 1
Size: 303109 Color: 1
Size: 287234 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 410019 Color: 1
Size: 331998 Color: 1
Size: 257984 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 410451 Color: 1
Size: 328848 Color: 1
Size: 260702 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 410498 Color: 1
Size: 317077 Color: 1
Size: 272426 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 410795 Color: 1
Size: 301343 Color: 1
Size: 287863 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 410894 Color: 1
Size: 334320 Color: 1
Size: 254787 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 410896 Color: 1
Size: 321932 Color: 1
Size: 267173 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 410914 Color: 1
Size: 335196 Color: 1
Size: 253891 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 410970 Color: 1
Size: 315615 Color: 1
Size: 273416 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 411029 Color: 1
Size: 312221 Color: 1
Size: 276751 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 411217 Color: 1
Size: 327885 Color: 1
Size: 260899 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 411497 Color: 1
Size: 306766 Color: 0
Size: 281738 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 411639 Color: 1
Size: 308286 Color: 1
Size: 280076 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 411676 Color: 1
Size: 311530 Color: 1
Size: 276795 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 411699 Color: 1
Size: 315978 Color: 1
Size: 272324 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 412053 Color: 1
Size: 315248 Color: 1
Size: 272700 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 412055 Color: 1
Size: 294658 Color: 1
Size: 293288 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 412256 Color: 1
Size: 305050 Color: 1
Size: 282695 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 413065 Color: 1
Size: 314987 Color: 1
Size: 271949 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 413186 Color: 1
Size: 318048 Color: 1
Size: 268767 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 413232 Color: 1
Size: 317771 Color: 1
Size: 268998 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 413238 Color: 1
Size: 328360 Color: 1
Size: 258403 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 413249 Color: 1
Size: 317809 Color: 1
Size: 268943 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 413391 Color: 1
Size: 319336 Color: 1
Size: 267274 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 413470 Color: 1
Size: 311607 Color: 1
Size: 274924 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 413635 Color: 1
Size: 296118 Color: 1
Size: 290248 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 413683 Color: 1
Size: 321368 Color: 1
Size: 264950 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 413708 Color: 1
Size: 319812 Color: 1
Size: 266481 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 414026 Color: 1
Size: 330624 Color: 1
Size: 255351 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 414125 Color: 1
Size: 333737 Color: 1
Size: 252139 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 414364 Color: 1
Size: 329517 Color: 1
Size: 256120 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 414667 Color: 1
Size: 318304 Color: 1
Size: 267030 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 414778 Color: 1
Size: 302379 Color: 1
Size: 282844 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 420240 Color: 1
Size: 329490 Color: 1
Size: 250271 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 415041 Color: 1
Size: 332602 Color: 1
Size: 252358 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 415423 Color: 1
Size: 303665 Color: 1
Size: 280913 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 415675 Color: 1
Size: 306525 Color: 1
Size: 277801 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 415914 Color: 1
Size: 329277 Color: 1
Size: 254810 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 416025 Color: 1
Size: 315108 Color: 1
Size: 268868 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 416127 Color: 1
Size: 308967 Color: 1
Size: 274907 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 416226 Color: 1
Size: 322141 Color: 1
Size: 261634 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 416406 Color: 1
Size: 327251 Color: 1
Size: 256344 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 416624 Color: 1
Size: 302781 Color: 1
Size: 280596 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 416932 Color: 1
Size: 312588 Color: 1
Size: 270481 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 417166 Color: 1
Size: 331360 Color: 1
Size: 251475 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 417900 Color: 1
Size: 308077 Color: 1
Size: 274024 Color: 0

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 418533 Color: 1
Size: 317763 Color: 1
Size: 263705 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 418660 Color: 1
Size: 313606 Color: 0
Size: 267735 Color: 1

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 418953 Color: 1
Size: 306498 Color: 1
Size: 274550 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 418960 Color: 1
Size: 295639 Color: 1
Size: 285402 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 419326 Color: 1
Size: 315315 Color: 1
Size: 265360 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 419795 Color: 1
Size: 318129 Color: 1
Size: 262077 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 420445 Color: 1
Size: 298754 Color: 1
Size: 280802 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 420465 Color: 1
Size: 328551 Color: 1
Size: 250985 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 420509 Color: 1
Size: 319175 Color: 1
Size: 260317 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 420852 Color: 1
Size: 310933 Color: 1
Size: 268216 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 421005 Color: 1
Size: 297421 Color: 1
Size: 281575 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 421116 Color: 1
Size: 327961 Color: 1
Size: 250924 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 421158 Color: 1
Size: 314472 Color: 1
Size: 264371 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 421310 Color: 1
Size: 299689 Color: 1
Size: 279002 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 421338 Color: 1
Size: 292910 Color: 1
Size: 285753 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 421507 Color: 1
Size: 306036 Color: 1
Size: 272458 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 421720 Color: 1
Size: 320947 Color: 1
Size: 257334 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 421920 Color: 1
Size: 298848 Color: 1
Size: 279233 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 422013 Color: 1
Size: 293886 Color: 1
Size: 284102 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 422025 Color: 1
Size: 317140 Color: 1
Size: 260836 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 422103 Color: 1
Size: 323649 Color: 1
Size: 254249 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 422267 Color: 1
Size: 311187 Color: 1
Size: 266547 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 365292 Color: 1
Size: 345768 Color: 1
Size: 288941 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 422430 Color: 1
Size: 296705 Color: 1
Size: 280866 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 422655 Color: 1
Size: 321183 Color: 1
Size: 256163 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 422850 Color: 1
Size: 306586 Color: 1
Size: 270565 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 423048 Color: 1
Size: 324831 Color: 1
Size: 252122 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 423073 Color: 1
Size: 324159 Color: 1
Size: 252769 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 423185 Color: 1
Size: 289983 Color: 1
Size: 286833 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 404824 Color: 1
Size: 300992 Color: 1
Size: 294185 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 423556 Color: 1
Size: 301477 Color: 1
Size: 274968 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 423570 Color: 1
Size: 303528 Color: 1
Size: 272903 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 423780 Color: 1
Size: 307377 Color: 1
Size: 268844 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 423992 Color: 1
Size: 292837 Color: 1
Size: 283172 Color: 0

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 385474 Color: 1
Size: 333769 Color: 1
Size: 280758 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 424642 Color: 1
Size: 315606 Color: 1
Size: 259753 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 424901 Color: 1
Size: 308930 Color: 1
Size: 266170 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 425027 Color: 1
Size: 321962 Color: 1
Size: 253012 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 425253 Color: 1
Size: 317498 Color: 1
Size: 257250 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 425253 Color: 1
Size: 309771 Color: 1
Size: 264977 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 425358 Color: 1
Size: 309476 Color: 1
Size: 265167 Color: 0

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 425412 Color: 1
Size: 317184 Color: 1
Size: 257405 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 425494 Color: 1
Size: 312839 Color: 1
Size: 261668 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 425517 Color: 1
Size: 300846 Color: 1
Size: 273638 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 425521 Color: 1
Size: 288339 Color: 1
Size: 286141 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 426657 Color: 1
Size: 308241 Color: 1
Size: 265103 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 426802 Color: 1
Size: 292183 Color: 0
Size: 281016 Color: 1

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 426860 Color: 1
Size: 309565 Color: 1
Size: 263576 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 427457 Color: 1
Size: 301900 Color: 1
Size: 270644 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 427721 Color: 1
Size: 293924 Color: 1
Size: 278356 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 427960 Color: 1
Size: 313578 Color: 1
Size: 258463 Color: 0

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 428056 Color: 1
Size: 304167 Color: 1
Size: 267778 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 428160 Color: 1
Size: 305575 Color: 1
Size: 266266 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 428284 Color: 1
Size: 313658 Color: 1
Size: 258059 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 428341 Color: 1
Size: 304660 Color: 1
Size: 267000 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 428788 Color: 1
Size: 295762 Color: 1
Size: 275451 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 428532 Color: 1
Size: 303326 Color: 1
Size: 268143 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 428545 Color: 1
Size: 307437 Color: 1
Size: 264019 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 428570 Color: 1
Size: 289228 Color: 0
Size: 282203 Color: 1

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 428584 Color: 1
Size: 289984 Color: 0
Size: 281433 Color: 1

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 428878 Color: 1
Size: 306124 Color: 1
Size: 264999 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 429251 Color: 1
Size: 287065 Color: 1
Size: 283685 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 429347 Color: 1
Size: 316980 Color: 1
Size: 253674 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 429391 Color: 1
Size: 306888 Color: 1
Size: 263722 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 421732 Color: 1
Size: 291745 Color: 0
Size: 286524 Color: 1

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 430390 Color: 1
Size: 317817 Color: 1
Size: 251794 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 430669 Color: 1
Size: 317472 Color: 1
Size: 251860 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 431230 Color: 1
Size: 300564 Color: 1
Size: 268207 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 431286 Color: 1
Size: 286534 Color: 0
Size: 282181 Color: 1

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 441800 Color: 1
Size: 286880 Color: 1
Size: 271321 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 395435 Color: 1
Size: 317746 Color: 1
Size: 286820 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 431693 Color: 1
Size: 311895 Color: 1
Size: 256413 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 431842 Color: 1
Size: 305853 Color: 1
Size: 262306 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 431876 Color: 1
Size: 303825 Color: 1
Size: 264300 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 432453 Color: 1
Size: 296467 Color: 1
Size: 271081 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 432476 Color: 1
Size: 298830 Color: 1
Size: 268695 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 432695 Color: 1
Size: 303006 Color: 1
Size: 264300 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 432736 Color: 1
Size: 304047 Color: 1
Size: 263218 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 432756 Color: 1
Size: 309542 Color: 1
Size: 257703 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 433051 Color: 1
Size: 289881 Color: 0
Size: 277069 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 433496 Color: 1
Size: 316304 Color: 1
Size: 250201 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 434042 Color: 1
Size: 305826 Color: 1
Size: 260133 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 434136 Color: 1
Size: 303918 Color: 1
Size: 261947 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 434178 Color: 1
Size: 303744 Color: 1
Size: 262079 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 434248 Color: 1
Size: 286357 Color: 1
Size: 279396 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 434289 Color: 1
Size: 291460 Color: 1
Size: 274252 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 434350 Color: 1
Size: 293411 Color: 1
Size: 272240 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 434559 Color: 1
Size: 288601 Color: 1
Size: 276841 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 434617 Color: 1
Size: 285727 Color: 0
Size: 279657 Color: 1

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 434638 Color: 1
Size: 304771 Color: 1
Size: 260592 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 435148 Color: 1
Size: 301987 Color: 1
Size: 262866 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 435242 Color: 1
Size: 311082 Color: 1
Size: 253677 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 435516 Color: 1
Size: 291586 Color: 1
Size: 272899 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 435584 Color: 1
Size: 311255 Color: 1
Size: 253162 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 435861 Color: 1
Size: 304281 Color: 0
Size: 259859 Color: 1

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 436331 Color: 1
Size: 298044 Color: 1
Size: 265626 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 436485 Color: 1
Size: 299486 Color: 1
Size: 264030 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 436513 Color: 1
Size: 301889 Color: 1
Size: 261599 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 436520 Color: 1
Size: 284821 Color: 1
Size: 278660 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 436837 Color: 1
Size: 289954 Color: 1
Size: 273210 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 436933 Color: 1
Size: 303004 Color: 1
Size: 260064 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 436976 Color: 1
Size: 299929 Color: 1
Size: 263096 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 437099 Color: 1
Size: 289833 Color: 1
Size: 273069 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 437802 Color: 1
Size: 305462 Color: 1
Size: 256737 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 438014 Color: 1
Size: 305331 Color: 1
Size: 256656 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 438183 Color: 1
Size: 300864 Color: 1
Size: 260954 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 438571 Color: 1
Size: 292925 Color: 1
Size: 268505 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 438661 Color: 1
Size: 292793 Color: 1
Size: 268547 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 438670 Color: 1
Size: 294142 Color: 1
Size: 267189 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 439452 Color: 1
Size: 283255 Color: 1
Size: 277294 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 439539 Color: 1
Size: 285448 Color: 1
Size: 275014 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 439627 Color: 1
Size: 303714 Color: 1
Size: 256660 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 439775 Color: 1
Size: 306509 Color: 1
Size: 253717 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 440398 Color: 1
Size: 285759 Color: 0
Size: 273844 Color: 1

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 440473 Color: 1
Size: 299467 Color: 1
Size: 260061 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 424493 Color: 1
Size: 307533 Color: 1
Size: 267975 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 440809 Color: 1
Size: 296054 Color: 1
Size: 263138 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 440857 Color: 1
Size: 292973 Color: 1
Size: 266171 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 441000 Color: 1
Size: 297352 Color: 1
Size: 261649 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 441262 Color: 1
Size: 285752 Color: 1
Size: 272987 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 441439 Color: 1
Size: 306538 Color: 1
Size: 252024 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 441484 Color: 1
Size: 303043 Color: 1
Size: 255474 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 441894 Color: 1
Size: 286673 Color: 1
Size: 271434 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 442126 Color: 1
Size: 302618 Color: 1
Size: 255257 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 442208 Color: 1
Size: 290634 Color: 1
Size: 267159 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 442667 Color: 1
Size: 284585 Color: 1
Size: 272749 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 442732 Color: 1
Size: 279670 Color: 1
Size: 277599 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 442750 Color: 1
Size: 293284 Color: 1
Size: 263967 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 443076 Color: 1
Size: 297922 Color: 1
Size: 259003 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 443157 Color: 1
Size: 280623 Color: 0
Size: 276221 Color: 1

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 443158 Color: 1
Size: 294567 Color: 1
Size: 262276 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 443469 Color: 1
Size: 301721 Color: 1
Size: 254811 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 443571 Color: 1
Size: 283568 Color: 1
Size: 272862 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 443593 Color: 1
Size: 291226 Color: 1
Size: 265182 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 443624 Color: 1
Size: 292921 Color: 1
Size: 263456 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 443705 Color: 1
Size: 293220 Color: 1
Size: 263076 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 444573 Color: 1
Size: 298612 Color: 1
Size: 256816 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 444891 Color: 1
Size: 293484 Color: 1
Size: 261626 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 444900 Color: 1
Size: 296941 Color: 1
Size: 258160 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 444991 Color: 1
Size: 300897 Color: 1
Size: 254113 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 445694 Color: 1
Size: 293849 Color: 1
Size: 260458 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 445726 Color: 1
Size: 291675 Color: 1
Size: 262600 Color: 0

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 446002 Color: 1
Size: 288160 Color: 1
Size: 265839 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 446090 Color: 1
Size: 283232 Color: 1
Size: 270679 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 446388 Color: 1
Size: 301705 Color: 1
Size: 251908 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 446433 Color: 1
Size: 277611 Color: 1
Size: 275957 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 446620 Color: 1
Size: 289377 Color: 1
Size: 264004 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 446916 Color: 1
Size: 290573 Color: 1
Size: 262512 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 447056 Color: 1
Size: 293082 Color: 1
Size: 259863 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 447333 Color: 1
Size: 291806 Color: 1
Size: 260862 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 447374 Color: 1
Size: 283372 Color: 1
Size: 269255 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 447538 Color: 1
Size: 295508 Color: 1
Size: 256955 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 447781 Color: 1
Size: 298972 Color: 1
Size: 253248 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 448012 Color: 1
Size: 290898 Color: 1
Size: 261091 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 448024 Color: 1
Size: 280134 Color: 1
Size: 271843 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 448083 Color: 1
Size: 296524 Color: 1
Size: 255394 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 448314 Color: 1
Size: 299475 Color: 1
Size: 252212 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 448453 Color: 1
Size: 285391 Color: 1
Size: 266157 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 448488 Color: 1
Size: 297545 Color: 1
Size: 253968 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 448512 Color: 1
Size: 289043 Color: 1
Size: 262446 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 448635 Color: 1
Size: 300439 Color: 1
Size: 250927 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 448986 Color: 1
Size: 300514 Color: 1
Size: 250501 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 449174 Color: 1
Size: 282235 Color: 1
Size: 268592 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 449649 Color: 1
Size: 293482 Color: 1
Size: 256870 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 449910 Color: 1
Size: 295615 Color: 1
Size: 254476 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 450049 Color: 1
Size: 287074 Color: 1
Size: 262878 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 450100 Color: 1
Size: 297810 Color: 1
Size: 252091 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 450332 Color: 1
Size: 293464 Color: 1
Size: 256205 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 450388 Color: 1
Size: 292463 Color: 1
Size: 257150 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 450583 Color: 1
Size: 295663 Color: 1
Size: 253755 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 450726 Color: 1
Size: 296191 Color: 1
Size: 253084 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 450832 Color: 1
Size: 277046 Color: 1
Size: 272123 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 450889 Color: 1
Size: 298911 Color: 1
Size: 250201 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 451006 Color: 1
Size: 283443 Color: 1
Size: 265552 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 451015 Color: 1
Size: 281095 Color: 1
Size: 267891 Color: 0

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 451019 Color: 1
Size: 286917 Color: 1
Size: 262065 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 451791 Color: 1
Size: 278709 Color: 1
Size: 269501 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 452083 Color: 1
Size: 290042 Color: 1
Size: 257876 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 479125 Color: 1
Size: 268506 Color: 1
Size: 252370 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 452247 Color: 1
Size: 288450 Color: 1
Size: 259304 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 452316 Color: 1
Size: 294630 Color: 1
Size: 253055 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 452583 Color: 1
Size: 284634 Color: 1
Size: 262784 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 453139 Color: 1
Size: 276287 Color: 1
Size: 270575 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 453403 Color: 1
Size: 285479 Color: 1
Size: 261119 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 397582 Color: 1
Size: 303159 Color: 1
Size: 299260 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 453854 Color: 1
Size: 291555 Color: 1
Size: 254592 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 454367 Color: 1
Size: 292092 Color: 1
Size: 253542 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 454463 Color: 1
Size: 292357 Color: 1
Size: 253181 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 454576 Color: 1
Size: 281631 Color: 1
Size: 263794 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 454847 Color: 1
Size: 281427 Color: 1
Size: 263727 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 455396 Color: 1
Size: 292648 Color: 1
Size: 251957 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 455452 Color: 1
Size: 292709 Color: 1
Size: 251840 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 455455 Color: 1
Size: 280005 Color: 1
Size: 264541 Color: 0

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 455658 Color: 1
Size: 279495 Color: 1
Size: 264848 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 456072 Color: 1
Size: 278621 Color: 1
Size: 265308 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 456366 Color: 1
Size: 291243 Color: 1
Size: 252392 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 456707 Color: 1
Size: 281059 Color: 1
Size: 262235 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 457173 Color: 1
Size: 287585 Color: 1
Size: 255243 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 457652 Color: 1
Size: 289865 Color: 1
Size: 252484 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 457973 Color: 1
Size: 288211 Color: 1
Size: 253817 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 458531 Color: 1
Size: 285736 Color: 1
Size: 255734 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 458669 Color: 1
Size: 281321 Color: 1
Size: 260011 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 459039 Color: 1
Size: 287951 Color: 1
Size: 253011 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 459065 Color: 1
Size: 274443 Color: 1
Size: 266493 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 459208 Color: 1
Size: 283931 Color: 1
Size: 256862 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 459522 Color: 1
Size: 282834 Color: 1
Size: 257645 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 459563 Color: 1
Size: 284761 Color: 1
Size: 255677 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 459779 Color: 1
Size: 287274 Color: 1
Size: 252948 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 460527 Color: 1
Size: 284859 Color: 1
Size: 254615 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 460748 Color: 1
Size: 286703 Color: 1
Size: 252550 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 460757 Color: 1
Size: 273575 Color: 1
Size: 265669 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 460870 Color: 1
Size: 280260 Color: 1
Size: 258871 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 461067 Color: 1
Size: 279075 Color: 1
Size: 259859 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 461909 Color: 1
Size: 286025 Color: 1
Size: 252067 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 461953 Color: 1
Size: 283456 Color: 1
Size: 254592 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 462132 Color: 1
Size: 285807 Color: 1
Size: 252062 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 462422 Color: 1
Size: 271424 Color: 0
Size: 266155 Color: 1

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 462614 Color: 1
Size: 274635 Color: 1
Size: 262752 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 462649 Color: 1
Size: 280199 Color: 1
Size: 257153 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 463032 Color: 1
Size: 284627 Color: 1
Size: 252342 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 463632 Color: 1
Size: 274486 Color: 1
Size: 261883 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 463944 Color: 1
Size: 280333 Color: 1
Size: 255724 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 464020 Color: 1
Size: 282229 Color: 1
Size: 253752 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 464400 Color: 1
Size: 268342 Color: 0
Size: 267259 Color: 1

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 464530 Color: 1
Size: 276810 Color: 1
Size: 258661 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 465012 Color: 1
Size: 280041 Color: 1
Size: 254948 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 465197 Color: 1
Size: 275358 Color: 1
Size: 259446 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 465329 Color: 1
Size: 268999 Color: 1
Size: 265673 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 465571 Color: 1
Size: 283388 Color: 1
Size: 251042 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 465584 Color: 1
Size: 278635 Color: 1
Size: 255782 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 466019 Color: 1
Size: 282023 Color: 1
Size: 251959 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 466164 Color: 1
Size: 281336 Color: 1
Size: 252501 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 466473 Color: 1
Size: 280004 Color: 1
Size: 253524 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 466610 Color: 1
Size: 270654 Color: 1
Size: 262737 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 467152 Color: 1
Size: 268455 Color: 0
Size: 264394 Color: 1

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 467280 Color: 1
Size: 275597 Color: 1
Size: 257124 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 467784 Color: 1
Size: 276940 Color: 1
Size: 255277 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 364723 Color: 1
Size: 345231 Color: 1
Size: 290047 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 469125 Color: 1
Size: 278852 Color: 1
Size: 252024 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 469596 Color: 1
Size: 279216 Color: 1
Size: 251189 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 469697 Color: 1
Size: 279611 Color: 1
Size: 250693 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 469792 Color: 1
Size: 279447 Color: 1
Size: 250762 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 469799 Color: 1
Size: 276096 Color: 1
Size: 254106 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 469929 Color: 1
Size: 271082 Color: 1
Size: 258990 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 470106 Color: 1
Size: 275631 Color: 1
Size: 254264 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 470310 Color: 1
Size: 277213 Color: 1
Size: 252478 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 470439 Color: 1
Size: 275292 Color: 1
Size: 254270 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 471230 Color: 1
Size: 274913 Color: 1
Size: 253858 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 471703 Color: 1
Size: 265186 Color: 1
Size: 263112 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 471992 Color: 1
Size: 269943 Color: 1
Size: 258066 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 472074 Color: 1
Size: 269344 Color: 1
Size: 258583 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 472145 Color: 1
Size: 274824 Color: 1
Size: 253032 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 472160 Color: 1
Size: 270599 Color: 1
Size: 257242 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 473444 Color: 1
Size: 275724 Color: 1
Size: 250833 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 474077 Color: 1
Size: 275670 Color: 1
Size: 250254 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 474197 Color: 1
Size: 275475 Color: 1
Size: 250329 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 474386 Color: 1
Size: 272841 Color: 1
Size: 252774 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 474392 Color: 1
Size: 270858 Color: 1
Size: 254751 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 474805 Color: 1
Size: 274574 Color: 1
Size: 250622 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 474864 Color: 1
Size: 272901 Color: 1
Size: 252236 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 474968 Color: 1
Size: 263309 Color: 0
Size: 261724 Color: 1

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 475409 Color: 1
Size: 268173 Color: 1
Size: 256419 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 475496 Color: 1
Size: 270353 Color: 1
Size: 254152 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 475524 Color: 1
Size: 273086 Color: 1
Size: 251391 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 475714 Color: 1
Size: 269576 Color: 1
Size: 254711 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 476186 Color: 1
Size: 271681 Color: 1
Size: 252134 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 476303 Color: 1
Size: 270332 Color: 1
Size: 253366 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 476535 Color: 1
Size: 268616 Color: 1
Size: 254850 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 476578 Color: 1
Size: 272789 Color: 1
Size: 250634 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 476707 Color: 1
Size: 270083 Color: 1
Size: 253211 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 476900 Color: 1
Size: 261818 Color: 0
Size: 261283 Color: 1

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 477364 Color: 1
Size: 267258 Color: 1
Size: 255379 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 477605 Color: 1
Size: 264931 Color: 1
Size: 257465 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 477617 Color: 1
Size: 264714 Color: 1
Size: 257670 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 477889 Color: 1
Size: 267670 Color: 1
Size: 254442 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 478096 Color: 1
Size: 270151 Color: 1
Size: 251754 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 478953 Color: 1
Size: 261208 Color: 1
Size: 259840 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 478985 Color: 1
Size: 270007 Color: 1
Size: 251009 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 480475 Color: 1
Size: 264380 Color: 1
Size: 255146 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 480695 Color: 1
Size: 263035 Color: 1
Size: 256271 Color: 0

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 480793 Color: 1
Size: 267854 Color: 1
Size: 251354 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 481416 Color: 1
Size: 264591 Color: 1
Size: 253994 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 481479 Color: 1
Size: 261232 Color: 0
Size: 257290 Color: 1

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 482181 Color: 1
Size: 264951 Color: 1
Size: 252869 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 482212 Color: 1
Size: 265744 Color: 1
Size: 252045 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 482404 Color: 1
Size: 265598 Color: 1
Size: 251999 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 482583 Color: 1
Size: 267026 Color: 1
Size: 250392 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 483314 Color: 1
Size: 261610 Color: 1
Size: 255077 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 483420 Color: 1
Size: 262152 Color: 1
Size: 254429 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 483524 Color: 1
Size: 264902 Color: 1
Size: 251575 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 483924 Color: 1
Size: 262957 Color: 1
Size: 253120 Color: 0

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 483969 Color: 1
Size: 263796 Color: 1
Size: 252236 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 484056 Color: 1
Size: 264704 Color: 1
Size: 251241 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 435554 Color: 1
Size: 303754 Color: 1
Size: 260693 Color: 0

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 484760 Color: 1
Size: 261217 Color: 1
Size: 254024 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 484900 Color: 1
Size: 259976 Color: 1
Size: 255125 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 485432 Color: 1
Size: 259891 Color: 1
Size: 254678 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 485471 Color: 1
Size: 264417 Color: 1
Size: 250113 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 485553 Color: 1
Size: 261895 Color: 1
Size: 252553 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 485950 Color: 1
Size: 263426 Color: 1
Size: 250625 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 486779 Color: 1
Size: 258635 Color: 1
Size: 254587 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 488254 Color: 1
Size: 260703 Color: 1
Size: 251044 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 488913 Color: 1
Size: 256920 Color: 0
Size: 254168 Color: 1

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 488915 Color: 1
Size: 259879 Color: 1
Size: 251207 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 489674 Color: 1
Size: 255386 Color: 1
Size: 254941 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 489892 Color: 1
Size: 259720 Color: 1
Size: 250389 Color: 0

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 490193 Color: 1
Size: 258472 Color: 1
Size: 251336 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 490252 Color: 1
Size: 259729 Color: 1
Size: 250020 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 490352 Color: 1
Size: 258050 Color: 1
Size: 251599 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 490625 Color: 1
Size: 257041 Color: 1
Size: 252335 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 490796 Color: 1
Size: 257683 Color: 1
Size: 251522 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 491483 Color: 1
Size: 257739 Color: 1
Size: 250779 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 491504 Color: 1
Size: 255574 Color: 1
Size: 252923 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 491574 Color: 1
Size: 256733 Color: 1
Size: 251694 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 491885 Color: 1
Size: 254063 Color: 1
Size: 254053 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 491946 Color: 1
Size: 257413 Color: 1
Size: 250642 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 492484 Color: 1
Size: 257192 Color: 1
Size: 250325 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 493213 Color: 1
Size: 254801 Color: 1
Size: 251987 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 493214 Color: 1
Size: 254224 Color: 1
Size: 252563 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 493555 Color: 1
Size: 253591 Color: 1
Size: 252855 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 493620 Color: 1
Size: 254718 Color: 1
Size: 251663 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 494161 Color: 1
Size: 254603 Color: 1
Size: 251237 Color: 0

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 494296 Color: 1
Size: 253976 Color: 1
Size: 251729 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 494298 Color: 1
Size: 255008 Color: 1
Size: 250695 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 494357 Color: 1
Size: 253134 Color: 1
Size: 252510 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 494480 Color: 1
Size: 252963 Color: 1
Size: 252558 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 494627 Color: 1
Size: 254944 Color: 1
Size: 250430 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 494681 Color: 1
Size: 255076 Color: 1
Size: 250244 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 494878 Color: 1
Size: 253832 Color: 1
Size: 251291 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 495107 Color: 1
Size: 253016 Color: 1
Size: 251878 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 495294 Color: 1
Size: 254302 Color: 1
Size: 250405 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 495724 Color: 1
Size: 252866 Color: 1
Size: 251411 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 495818 Color: 1
Size: 254112 Color: 1
Size: 250071 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 495895 Color: 1
Size: 252817 Color: 1
Size: 251289 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 495962 Color: 1
Size: 254013 Color: 1
Size: 250026 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 496253 Color: 1
Size: 253186 Color: 1
Size: 250562 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 496889 Color: 1
Size: 252487 Color: 1
Size: 250625 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 496952 Color: 1
Size: 252800 Color: 1
Size: 250249 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 496954 Color: 1
Size: 251730 Color: 0
Size: 251317 Color: 1

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 498078 Color: 1
Size: 251717 Color: 1
Size: 250206 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 458731 Color: 1
Size: 283316 Color: 1
Size: 257954 Color: 0

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 498445 Color: 1
Size: 251341 Color: 1
Size: 250215 Color: 0

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 498550 Color: 1
Size: 251098 Color: 1
Size: 250353 Color: 0

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 498611 Color: 1
Size: 251084 Color: 1
Size: 250306 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 498663 Color: 1
Size: 251309 Color: 1
Size: 250029 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 499683 Color: 1
Size: 250283 Color: 1
Size: 250035 Color: 0

Bin 2958: 1 of cap free
Amount of items: 3
Items: 
Size: 357658 Color: 1
Size: 338902 Color: 1
Size: 303440 Color: 0

Bin 2959: 1 of cap free
Amount of items: 3
Items: 
Size: 498011 Color: 1
Size: 251971 Color: 1
Size: 250018 Color: 0

Bin 2960: 1 of cap free
Amount of items: 3
Items: 
Size: 399933 Color: 1
Size: 334194 Color: 1
Size: 265873 Color: 0

Bin 2961: 1 of cap free
Amount of items: 3
Items: 
Size: 397452 Color: 1
Size: 330436 Color: 1
Size: 272112 Color: 0

Bin 2962: 1 of cap free
Amount of items: 3
Items: 
Size: 369074 Color: 1
Size: 350544 Color: 1
Size: 280382 Color: 0

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 401630 Color: 1
Size: 313529 Color: 1
Size: 284841 Color: 0

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 429741 Color: 1
Size: 302552 Color: 1
Size: 267707 Color: 0

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 436260 Color: 1
Size: 305970 Color: 1
Size: 257770 Color: 0

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 366512 Color: 1
Size: 328500 Color: 1
Size: 304988 Color: 0

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 443153 Color: 1
Size: 301466 Color: 1
Size: 255381 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 402625 Color: 1
Size: 331371 Color: 1
Size: 266004 Color: 0

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 400348 Color: 1
Size: 339386 Color: 1
Size: 260266 Color: 0

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 402997 Color: 1
Size: 317689 Color: 1
Size: 279314 Color: 0

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 346302 Color: 1
Size: 337144 Color: 1
Size: 316554 Color: 0

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 408626 Color: 1
Size: 305675 Color: 1
Size: 285699 Color: 0

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 346247 Color: 1
Size: 331599 Color: 1
Size: 322154 Color: 0

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 408504 Color: 1
Size: 326716 Color: 1
Size: 264780 Color: 0

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 397583 Color: 1
Size: 333204 Color: 1
Size: 269213 Color: 0

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 394652 Color: 1
Size: 354181 Color: 1
Size: 251167 Color: 0

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 381290 Color: 1
Size: 359032 Color: 1
Size: 259678 Color: 0

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 384392 Color: 1
Size: 331851 Color: 1
Size: 283757 Color: 0

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 447799 Color: 1
Size: 295814 Color: 1
Size: 256387 Color: 0

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 351940 Color: 1
Size: 326181 Color: 1
Size: 321879 Color: 0

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 369279 Color: 1
Size: 348463 Color: 1
Size: 282258 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 435819 Color: 1
Size: 303800 Color: 1
Size: 260381 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 421243 Color: 1
Size: 324237 Color: 1
Size: 254520 Color: 0

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 441194 Color: 1
Size: 285361 Color: 0
Size: 273445 Color: 1

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 450395 Color: 1
Size: 283219 Color: 1
Size: 266386 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 399359 Color: 1
Size: 323169 Color: 1
Size: 277472 Color: 0

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 365860 Color: 1
Size: 325872 Color: 1
Size: 308268 Color: 0

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 479058 Color: 1
Size: 268756 Color: 1
Size: 252186 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 393296 Color: 1
Size: 352390 Color: 1
Size: 254314 Color: 0

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 499163 Color: 1
Size: 250597 Color: 1
Size: 250240 Color: 0

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 423065 Color: 1
Size: 320515 Color: 1
Size: 256420 Color: 0

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 421216 Color: 1
Size: 311923 Color: 1
Size: 266861 Color: 0

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 423352 Color: 1
Size: 323251 Color: 1
Size: 253397 Color: 0

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 444124 Color: 1
Size: 298817 Color: 1
Size: 257059 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 401153 Color: 1
Size: 321167 Color: 1
Size: 277680 Color: 0

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 393783 Color: 1
Size: 307115 Color: 1
Size: 299102 Color: 0

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 385318 Color: 1
Size: 352936 Color: 1
Size: 261746 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 404755 Color: 1
Size: 303309 Color: 1
Size: 291936 Color: 0

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 373280 Color: 1
Size: 328825 Color: 1
Size: 297895 Color: 0

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 363249 Color: 1
Size: 333046 Color: 1
Size: 303705 Color: 0

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 396634 Color: 1
Size: 330888 Color: 1
Size: 272478 Color: 0

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 391580 Color: 1
Size: 352909 Color: 1
Size: 255511 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 430396 Color: 1
Size: 306848 Color: 1
Size: 262756 Color: 0

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 446067 Color: 1
Size: 286262 Color: 1
Size: 267671 Color: 0

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 384941 Color: 1
Size: 312031 Color: 1
Size: 303028 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 420005 Color: 1
Size: 302119 Color: 1
Size: 277876 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 369407 Color: 1
Size: 334307 Color: 1
Size: 296286 Color: 0

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 380449 Color: 1
Size: 348947 Color: 1
Size: 270604 Color: 0

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 498974 Color: 1
Size: 250961 Color: 1
Size: 250065 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 376244 Color: 1
Size: 317612 Color: 1
Size: 306144 Color: 0

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 365699 Color: 1
Size: 363732 Color: 1
Size: 270569 Color: 0

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 396576 Color: 1
Size: 308002 Color: 1
Size: 295422 Color: 0

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 487424 Color: 1
Size: 262178 Color: 1
Size: 250398 Color: 0

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 398630 Color: 1
Size: 321610 Color: 1
Size: 279760 Color: 0

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 424408 Color: 1
Size: 298678 Color: 1
Size: 276914 Color: 0

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 385897 Color: 1
Size: 329304 Color: 1
Size: 284799 Color: 0

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 356614 Color: 1
Size: 355703 Color: 1
Size: 287683 Color: 0

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 401394 Color: 1
Size: 339236 Color: 1
Size: 259370 Color: 0

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 384264 Color: 1
Size: 349976 Color: 1
Size: 265760 Color: 0

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 406433 Color: 1
Size: 298546 Color: 0
Size: 295021 Color: 1

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 364104 Color: 1
Size: 346050 Color: 1
Size: 289846 Color: 0

Bin 3022: 1 of cap free
Amount of items: 3
Items: 
Size: 369261 Color: 1
Size: 330641 Color: 1
Size: 300098 Color: 0

Bin 3023: 1 of cap free
Amount of items: 3
Items: 
Size: 375790 Color: 1
Size: 362953 Color: 1
Size: 261257 Color: 0

Bin 3024: 1 of cap free
Amount of items: 3
Items: 
Size: 358524 Color: 1
Size: 326844 Color: 0
Size: 314632 Color: 1

Bin 3025: 1 of cap free
Amount of items: 3
Items: 
Size: 377416 Color: 1
Size: 322419 Color: 1
Size: 300165 Color: 0

Bin 3026: 1 of cap free
Amount of items: 3
Items: 
Size: 357397 Color: 1
Size: 321781 Color: 0
Size: 320822 Color: 1

Bin 3027: 1 of cap free
Amount of items: 3
Items: 
Size: 371296 Color: 1
Size: 361537 Color: 1
Size: 267167 Color: 0

Bin 3028: 1 of cap free
Amount of items: 3
Items: 
Size: 374124 Color: 1
Size: 365123 Color: 1
Size: 260753 Color: 0

Bin 3029: 1 of cap free
Amount of items: 3
Items: 
Size: 380800 Color: 1
Size: 338390 Color: 1
Size: 280810 Color: 0

Bin 3030: 1 of cap free
Amount of items: 3
Items: 
Size: 364511 Color: 1
Size: 343651 Color: 1
Size: 291838 Color: 0

Bin 3031: 1 of cap free
Amount of items: 3
Items: 
Size: 353593 Color: 1
Size: 352032 Color: 1
Size: 294375 Color: 0

Bin 3032: 1 of cap free
Amount of items: 3
Items: 
Size: 405657 Color: 1
Size: 320759 Color: 1
Size: 273584 Color: 0

Bin 3033: 1 of cap free
Amount of items: 3
Items: 
Size: 367240 Color: 1
Size: 333418 Color: 1
Size: 299342 Color: 0

Bin 3034: 1 of cap free
Amount of items: 3
Items: 
Size: 387832 Color: 1
Size: 320742 Color: 1
Size: 291426 Color: 0

Bin 3035: 1 of cap free
Amount of items: 3
Items: 
Size: 393636 Color: 1
Size: 340738 Color: 1
Size: 265626 Color: 0

Bin 3036: 1 of cap free
Amount of items: 3
Items: 
Size: 348086 Color: 1
Size: 333096 Color: 1
Size: 318818 Color: 0

Bin 3037: 1 of cap free
Amount of items: 3
Items: 
Size: 383017 Color: 1
Size: 348695 Color: 1
Size: 268288 Color: 0

Bin 3038: 1 of cap free
Amount of items: 3
Items: 
Size: 364334 Color: 1
Size: 336987 Color: 1
Size: 298679 Color: 0

Bin 3039: 1 of cap free
Amount of items: 3
Items: 
Size: 371970 Color: 1
Size: 346827 Color: 1
Size: 281203 Color: 0

Bin 3040: 1 of cap free
Amount of items: 3
Items: 
Size: 392268 Color: 1
Size: 333376 Color: 1
Size: 274356 Color: 0

Bin 3041: 1 of cap free
Amount of items: 3
Items: 
Size: 366263 Color: 1
Size: 348346 Color: 1
Size: 285391 Color: 0

Bin 3042: 2 of cap free
Amount of items: 3
Items: 
Size: 445479 Color: 1
Size: 303092 Color: 1
Size: 251428 Color: 0

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 416307 Color: 1
Size: 317423 Color: 1
Size: 266269 Color: 0

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 436595 Color: 1
Size: 298599 Color: 1
Size: 264805 Color: 0

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 372999 Color: 1
Size: 338315 Color: 1
Size: 288685 Color: 0

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 371477 Color: 1
Size: 316646 Color: 1
Size: 311876 Color: 0

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 420397 Color: 1
Size: 324428 Color: 1
Size: 255174 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 409017 Color: 1
Size: 309241 Color: 1
Size: 281741 Color: 0

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 394435 Color: 1
Size: 325487 Color: 1
Size: 280077 Color: 0

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 378257 Color: 1
Size: 348009 Color: 1
Size: 273733 Color: 0

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 376105 Color: 1
Size: 349110 Color: 1
Size: 274784 Color: 0

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 385976 Color: 1
Size: 348475 Color: 1
Size: 265548 Color: 0

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 388365 Color: 1
Size: 315700 Color: 1
Size: 295934 Color: 0

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 376019 Color: 1
Size: 367280 Color: 1
Size: 256700 Color: 0

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 383715 Color: 1
Size: 316069 Color: 1
Size: 300215 Color: 0

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 387185 Color: 1
Size: 329290 Color: 1
Size: 283524 Color: 0

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 368196 Color: 1
Size: 349433 Color: 1
Size: 282370 Color: 0

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 395905 Color: 1
Size: 335373 Color: 1
Size: 268721 Color: 0

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 443400 Color: 1
Size: 296720 Color: 1
Size: 259879 Color: 0

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 403202 Color: 1
Size: 318115 Color: 1
Size: 278682 Color: 0

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 362937 Color: 1
Size: 360785 Color: 1
Size: 276277 Color: 0

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 440340 Color: 1
Size: 302906 Color: 1
Size: 256753 Color: 0

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 419207 Color: 1
Size: 322123 Color: 1
Size: 258669 Color: 0

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 365130 Color: 1
Size: 342032 Color: 1
Size: 292837 Color: 0

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 408887 Color: 1
Size: 310647 Color: 1
Size: 280465 Color: 0

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 401991 Color: 1
Size: 342229 Color: 1
Size: 255779 Color: 0

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 479065 Color: 1
Size: 269279 Color: 1
Size: 251655 Color: 0

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 383146 Color: 1
Size: 356094 Color: 1
Size: 260759 Color: 0

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 374978 Color: 1
Size: 374236 Color: 1
Size: 250785 Color: 0

Bin 3070: 2 of cap free
Amount of items: 3
Items: 
Size: 408413 Color: 1
Size: 337091 Color: 1
Size: 254495 Color: 0

Bin 3071: 2 of cap free
Amount of items: 3
Items: 
Size: 372154 Color: 1
Size: 321929 Color: 1
Size: 305916 Color: 0

Bin 3072: 2 of cap free
Amount of items: 3
Items: 
Size: 365731 Color: 1
Size: 347140 Color: 1
Size: 287128 Color: 0

Bin 3073: 2 of cap free
Amount of items: 3
Items: 
Size: 360269 Color: 1
Size: 356879 Color: 1
Size: 282851 Color: 0

Bin 3074: 2 of cap free
Amount of items: 3
Items: 
Size: 351664 Color: 1
Size: 325935 Color: 1
Size: 322400 Color: 0

Bin 3075: 2 of cap free
Amount of items: 3
Items: 
Size: 358978 Color: 1
Size: 337827 Color: 1
Size: 303194 Color: 0

Bin 3076: 3 of cap free
Amount of items: 3
Items: 
Size: 357856 Color: 1
Size: 324272 Color: 0
Size: 317870 Color: 1

Bin 3077: 3 of cap free
Amount of items: 3
Items: 
Size: 481518 Color: 1
Size: 262416 Color: 1
Size: 256064 Color: 0

Bin 3078: 3 of cap free
Amount of items: 3
Items: 
Size: 385012 Color: 1
Size: 341934 Color: 1
Size: 273052 Color: 0

Bin 3079: 3 of cap free
Amount of items: 3
Items: 
Size: 387992 Color: 1
Size: 327661 Color: 1
Size: 284345 Color: 0

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 390740 Color: 1
Size: 311728 Color: 1
Size: 297530 Color: 0

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 430365 Color: 1
Size: 308089 Color: 1
Size: 261544 Color: 0

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 420608 Color: 1
Size: 298657 Color: 1
Size: 280733 Color: 0

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 401805 Color: 1
Size: 302499 Color: 0
Size: 295694 Color: 1

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 383065 Color: 1
Size: 360373 Color: 1
Size: 256560 Color: 0

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 410789 Color: 1
Size: 311494 Color: 1
Size: 277715 Color: 0

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 361071 Color: 1
Size: 325266 Color: 1
Size: 313661 Color: 0

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 404741 Color: 1
Size: 329793 Color: 1
Size: 265464 Color: 0

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 360164 Color: 1
Size: 341058 Color: 1
Size: 298776 Color: 0

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 433511 Color: 1
Size: 291677 Color: 1
Size: 274810 Color: 0

Bin 3090: 3 of cap free
Amount of items: 3
Items: 
Size: 367550 Color: 1
Size: 342059 Color: 1
Size: 290389 Color: 0

Bin 3091: 3 of cap free
Amount of items: 3
Items: 
Size: 337811 Color: 1
Size: 335508 Color: 1
Size: 326679 Color: 0

Bin 3092: 3 of cap free
Amount of items: 3
Items: 
Size: 446789 Color: 1
Size: 282864 Color: 1
Size: 270345 Color: 0

Bin 3093: 3 of cap free
Amount of items: 3
Items: 
Size: 420686 Color: 1
Size: 303900 Color: 1
Size: 275412 Color: 0

Bin 3094: 3 of cap free
Amount of items: 3
Items: 
Size: 368306 Color: 1
Size: 363331 Color: 1
Size: 268361 Color: 0

Bin 3095: 3 of cap free
Amount of items: 3
Items: 
Size: 395989 Color: 1
Size: 343398 Color: 1
Size: 260611 Color: 0

Bin 3096: 3 of cap free
Amount of items: 3
Items: 
Size: 370806 Color: 1
Size: 328660 Color: 1
Size: 300532 Color: 0

Bin 3097: 3 of cap free
Amount of items: 3
Items: 
Size: 374530 Color: 1
Size: 339619 Color: 1
Size: 285849 Color: 0

Bin 3098: 3 of cap free
Amount of items: 3
Items: 
Size: 372269 Color: 1
Size: 328582 Color: 1
Size: 299147 Color: 0

Bin 3099: 3 of cap free
Amount of items: 3
Items: 
Size: 359707 Color: 1
Size: 334157 Color: 1
Size: 306134 Color: 0

Bin 3100: 3 of cap free
Amount of items: 3
Items: 
Size: 452985 Color: 1
Size: 296581 Color: 1
Size: 250432 Color: 0

Bin 3101: 3 of cap free
Amount of items: 3
Items: 
Size: 390717 Color: 1
Size: 326294 Color: 1
Size: 282987 Color: 0

Bin 3102: 3 of cap free
Amount of items: 3
Items: 
Size: 390096 Color: 1
Size: 320134 Color: 1
Size: 289768 Color: 0

Bin 3103: 4 of cap free
Amount of items: 3
Items: 
Size: 429862 Color: 1
Size: 314328 Color: 1
Size: 255807 Color: 0

Bin 3104: 4 of cap free
Amount of items: 3
Items: 
Size: 453426 Color: 1
Size: 284754 Color: 0
Size: 261817 Color: 1

Bin 3105: 4 of cap free
Amount of items: 3
Items: 
Size: 385796 Color: 1
Size: 332089 Color: 1
Size: 282112 Color: 0

Bin 3106: 4 of cap free
Amount of items: 3
Items: 
Size: 429477 Color: 1
Size: 308468 Color: 1
Size: 262052 Color: 0

Bin 3107: 4 of cap free
Amount of items: 3
Items: 
Size: 353363 Color: 1
Size: 341140 Color: 1
Size: 305494 Color: 0

Bin 3108: 4 of cap free
Amount of items: 3
Items: 
Size: 428572 Color: 1
Size: 298685 Color: 1
Size: 272740 Color: 0

Bin 3109: 4 of cap free
Amount of items: 3
Items: 
Size: 363979 Color: 1
Size: 329722 Color: 1
Size: 306296 Color: 0

Bin 3110: 4 of cap free
Amount of items: 3
Items: 
Size: 413042 Color: 1
Size: 304374 Color: 1
Size: 282581 Color: 0

Bin 3111: 4 of cap free
Amount of items: 3
Items: 
Size: 452210 Color: 1
Size: 278704 Color: 0
Size: 269083 Color: 1

Bin 3112: 4 of cap free
Amount of items: 3
Items: 
Size: 399342 Color: 1
Size: 323526 Color: 1
Size: 277129 Color: 0

Bin 3113: 4 of cap free
Amount of items: 3
Items: 
Size: 382410 Color: 1
Size: 344147 Color: 1
Size: 273440 Color: 0

Bin 3114: 4 of cap free
Amount of items: 3
Items: 
Size: 366607 Color: 1
Size: 351301 Color: 1
Size: 282089 Color: 0

Bin 3115: 4 of cap free
Amount of items: 3
Items: 
Size: 386538 Color: 1
Size: 331874 Color: 1
Size: 281585 Color: 0

Bin 3116: 4 of cap free
Amount of items: 3
Items: 
Size: 357476 Color: 1
Size: 350731 Color: 1
Size: 291790 Color: 0

Bin 3117: 4 of cap free
Amount of items: 3
Items: 
Size: 360379 Color: 1
Size: 336123 Color: 1
Size: 303495 Color: 0

Bin 3118: 4 of cap free
Amount of items: 3
Items: 
Size: 350468 Color: 1
Size: 333615 Color: 1
Size: 315914 Color: 0

Bin 3119: 4 of cap free
Amount of items: 3
Items: 
Size: 407844 Color: 1
Size: 333273 Color: 1
Size: 258880 Color: 0

Bin 3120: 4 of cap free
Amount of items: 3
Items: 
Size: 392551 Color: 1
Size: 344729 Color: 1
Size: 262717 Color: 0

Bin 3121: 4 of cap free
Amount of items: 3
Items: 
Size: 403105 Color: 1
Size: 326800 Color: 1
Size: 270092 Color: 0

Bin 3122: 4 of cap free
Amount of items: 3
Items: 
Size: 367930 Color: 1
Size: 350161 Color: 1
Size: 281906 Color: 0

Bin 3123: 4 of cap free
Amount of items: 3
Items: 
Size: 373334 Color: 1
Size: 315536 Color: 1
Size: 311127 Color: 0

Bin 3124: 4 of cap free
Amount of items: 3
Items: 
Size: 373765 Color: 1
Size: 346982 Color: 1
Size: 279250 Color: 0

Bin 3125: 4 of cap free
Amount of items: 3
Items: 
Size: 364104 Color: 1
Size: 323320 Color: 0
Size: 312573 Color: 1

Bin 3126: 5 of cap free
Amount of items: 3
Items: 
Size: 393975 Color: 1
Size: 335387 Color: 1
Size: 270634 Color: 0

Bin 3127: 5 of cap free
Amount of items: 3
Items: 
Size: 439682 Color: 1
Size: 309290 Color: 1
Size: 251024 Color: 0

Bin 3128: 5 of cap free
Amount of items: 3
Items: 
Size: 471513 Color: 1
Size: 276024 Color: 1
Size: 252459 Color: 0

Bin 3129: 5 of cap free
Amount of items: 3
Items: 
Size: 467755 Color: 1
Size: 273927 Color: 1
Size: 258314 Color: 0

Bin 3130: 5 of cap free
Amount of items: 3
Items: 
Size: 438652 Color: 1
Size: 295002 Color: 1
Size: 266342 Color: 0

Bin 3131: 5 of cap free
Amount of items: 3
Items: 
Size: 365530 Color: 1
Size: 325660 Color: 1
Size: 308806 Color: 0

Bin 3132: 5 of cap free
Amount of items: 3
Items: 
Size: 417470 Color: 1
Size: 291682 Color: 0
Size: 290844 Color: 1

Bin 3133: 5 of cap free
Amount of items: 3
Items: 
Size: 384530 Color: 1
Size: 333420 Color: 1
Size: 282046 Color: 0

Bin 3134: 5 of cap free
Amount of items: 3
Items: 
Size: 361315 Color: 1
Size: 345007 Color: 1
Size: 293674 Color: 0

Bin 3135: 5 of cap free
Amount of items: 3
Items: 
Size: 371114 Color: 1
Size: 349030 Color: 1
Size: 279852 Color: 0

Bin 3136: 5 of cap free
Amount of items: 3
Items: 
Size: 382460 Color: 1
Size: 330272 Color: 1
Size: 287264 Color: 0

Bin 3137: 5 of cap free
Amount of items: 3
Items: 
Size: 385901 Color: 1
Size: 331465 Color: 1
Size: 282630 Color: 0

Bin 3138: 5 of cap free
Amount of items: 3
Items: 
Size: 400359 Color: 1
Size: 341828 Color: 1
Size: 257809 Color: 0

Bin 3139: 6 of cap free
Amount of items: 3
Items: 
Size: 473011 Color: 1
Size: 268574 Color: 1
Size: 258410 Color: 0

Bin 3140: 6 of cap free
Amount of items: 3
Items: 
Size: 351991 Color: 1
Size: 340565 Color: 1
Size: 307439 Color: 0

Bin 3141: 6 of cap free
Amount of items: 3
Items: 
Size: 441336 Color: 1
Size: 292239 Color: 1
Size: 266420 Color: 0

Bin 3142: 6 of cap free
Amount of items: 3
Items: 
Size: 384558 Color: 1
Size: 321304 Color: 1
Size: 294133 Color: 0

Bin 3143: 6 of cap free
Amount of items: 3
Items: 
Size: 431506 Color: 1
Size: 295678 Color: 1
Size: 272811 Color: 0

Bin 3144: 6 of cap free
Amount of items: 3
Items: 
Size: 351943 Color: 1
Size: 350645 Color: 1
Size: 297407 Color: 0

Bin 3145: 6 of cap free
Amount of items: 3
Items: 
Size: 411619 Color: 1
Size: 318659 Color: 1
Size: 269717 Color: 0

Bin 3146: 6 of cap free
Amount of items: 3
Items: 
Size: 357717 Color: 1
Size: 335456 Color: 1
Size: 306822 Color: 0

Bin 3147: 6 of cap free
Amount of items: 3
Items: 
Size: 380183 Color: 1
Size: 332920 Color: 1
Size: 286892 Color: 0

Bin 3148: 6 of cap free
Amount of items: 3
Items: 
Size: 349156 Color: 1
Size: 338061 Color: 1
Size: 312778 Color: 0

Bin 3149: 6 of cap free
Amount of items: 3
Items: 
Size: 359251 Color: 1
Size: 331613 Color: 1
Size: 309131 Color: 0

Bin 3150: 6 of cap free
Amount of items: 3
Items: 
Size: 386715 Color: 1
Size: 343482 Color: 1
Size: 269798 Color: 0

Bin 3151: 6 of cap free
Amount of items: 3
Items: 
Size: 407318 Color: 1
Size: 337292 Color: 1
Size: 255385 Color: 0

Bin 3152: 7 of cap free
Amount of items: 3
Items: 
Size: 446287 Color: 1
Size: 289758 Color: 1
Size: 263949 Color: 0

Bin 3153: 7 of cap free
Amount of items: 3
Items: 
Size: 417276 Color: 1
Size: 315699 Color: 1
Size: 267019 Color: 0

Bin 3154: 7 of cap free
Amount of items: 3
Items: 
Size: 364876 Color: 1
Size: 360977 Color: 1
Size: 274141 Color: 0

Bin 3155: 7 of cap free
Amount of items: 3
Items: 
Size: 369073 Color: 1
Size: 350295 Color: 1
Size: 280626 Color: 0

Bin 3156: 7 of cap free
Amount of items: 3
Items: 
Size: 493790 Color: 1
Size: 254305 Color: 0
Size: 251899 Color: 1

Bin 3157: 7 of cap free
Amount of items: 3
Items: 
Size: 417088 Color: 1
Size: 327725 Color: 1
Size: 255181 Color: 0

Bin 3158: 7 of cap free
Amount of items: 3
Items: 
Size: 398958 Color: 1
Size: 321851 Color: 1
Size: 279185 Color: 0

Bin 3159: 7 of cap free
Amount of items: 3
Items: 
Size: 427123 Color: 1
Size: 305032 Color: 1
Size: 267839 Color: 0

Bin 3160: 7 of cap free
Amount of items: 3
Items: 
Size: 408346 Color: 1
Size: 322089 Color: 1
Size: 269559 Color: 0

Bin 3161: 7 of cap free
Amount of items: 3
Items: 
Size: 362325 Color: 1
Size: 334493 Color: 1
Size: 303176 Color: 0

Bin 3162: 8 of cap free
Amount of items: 3
Items: 
Size: 398089 Color: 1
Size: 313923 Color: 1
Size: 287981 Color: 0

Bin 3163: 8 of cap free
Amount of items: 3
Items: 
Size: 440669 Color: 1
Size: 297497 Color: 1
Size: 261827 Color: 0

Bin 3164: 8 of cap free
Amount of items: 3
Items: 
Size: 464727 Color: 1
Size: 269277 Color: 1
Size: 265989 Color: 0

Bin 3165: 8 of cap free
Amount of items: 3
Items: 
Size: 344955 Color: 1
Size: 334683 Color: 1
Size: 320355 Color: 0

Bin 3166: 8 of cap free
Amount of items: 3
Items: 
Size: 395668 Color: 1
Size: 315167 Color: 1
Size: 289158 Color: 0

Bin 3167: 8 of cap free
Amount of items: 3
Items: 
Size: 342974 Color: 1
Size: 339123 Color: 1
Size: 317896 Color: 0

Bin 3168: 8 of cap free
Amount of items: 3
Items: 
Size: 434135 Color: 1
Size: 288045 Color: 1
Size: 277813 Color: 0

Bin 3169: 8 of cap free
Amount of items: 3
Items: 
Size: 379764 Color: 1
Size: 345433 Color: 1
Size: 274796 Color: 0

Bin 3170: 8 of cap free
Amount of items: 3
Items: 
Size: 482972 Color: 1
Size: 264111 Color: 1
Size: 252910 Color: 0

Bin 3171: 8 of cap free
Amount of items: 3
Items: 
Size: 377228 Color: 1
Size: 313351 Color: 1
Size: 309414 Color: 0

Bin 3172: 8 of cap free
Amount of items: 3
Items: 
Size: 381041 Color: 1
Size: 318208 Color: 0
Size: 300744 Color: 1

Bin 3173: 8 of cap free
Amount of items: 3
Items: 
Size: 445719 Color: 1
Size: 299367 Color: 1
Size: 254907 Color: 0

Bin 3174: 8 of cap free
Amount of items: 3
Items: 
Size: 436887 Color: 1
Size: 303990 Color: 1
Size: 259116 Color: 0

Bin 3175: 9 of cap free
Amount of items: 3
Items: 
Size: 365170 Color: 1
Size: 321306 Color: 0
Size: 313516 Color: 1

Bin 3176: 9 of cap free
Amount of items: 3
Items: 
Size: 421534 Color: 1
Size: 321818 Color: 1
Size: 256640 Color: 0

Bin 3177: 9 of cap free
Amount of items: 3
Items: 
Size: 460694 Color: 1
Size: 289125 Color: 1
Size: 250173 Color: 0

Bin 3178: 9 of cap free
Amount of items: 3
Items: 
Size: 344700 Color: 1
Size: 335775 Color: 1
Size: 319517 Color: 0

Bin 3179: 9 of cap free
Amount of items: 3
Items: 
Size: 424730 Color: 1
Size: 300789 Color: 1
Size: 274473 Color: 0

Bin 3180: 9 of cap free
Amount of items: 3
Items: 
Size: 426521 Color: 1
Size: 309882 Color: 1
Size: 263589 Color: 0

Bin 3181: 9 of cap free
Amount of items: 3
Items: 
Size: 379868 Color: 1
Size: 345179 Color: 1
Size: 274945 Color: 0

Bin 3182: 9 of cap free
Amount of items: 3
Items: 
Size: 401853 Color: 1
Size: 321350 Color: 1
Size: 276789 Color: 0

Bin 3183: 10 of cap free
Amount of items: 3
Items: 
Size: 416849 Color: 1
Size: 316180 Color: 1
Size: 266962 Color: 0

Bin 3184: 10 of cap free
Amount of items: 3
Items: 
Size: 362500 Color: 1
Size: 362187 Color: 1
Size: 275304 Color: 0

Bin 3185: 10 of cap free
Amount of items: 3
Items: 
Size: 377829 Color: 1
Size: 333746 Color: 1
Size: 288416 Color: 0

Bin 3186: 10 of cap free
Amount of items: 3
Items: 
Size: 430149 Color: 1
Size: 306513 Color: 1
Size: 263329 Color: 0

Bin 3187: 10 of cap free
Amount of items: 3
Items: 
Size: 382867 Color: 1
Size: 332938 Color: 1
Size: 284186 Color: 0

Bin 3188: 10 of cap free
Amount of items: 3
Items: 
Size: 389297 Color: 1
Size: 325447 Color: 1
Size: 285247 Color: 0

Bin 3189: 11 of cap free
Amount of items: 3
Items: 
Size: 387001 Color: 1
Size: 317390 Color: 0
Size: 295599 Color: 1

Bin 3190: 11 of cap free
Amount of items: 3
Items: 
Size: 355777 Color: 1
Size: 329129 Color: 1
Size: 315084 Color: 0

Bin 3191: 11 of cap free
Amount of items: 3
Items: 
Size: 373878 Color: 1
Size: 341895 Color: 1
Size: 284217 Color: 0

Bin 3192: 11 of cap free
Amount of items: 3
Items: 
Size: 387353 Color: 1
Size: 327260 Color: 1
Size: 285377 Color: 0

Bin 3193: 11 of cap free
Amount of items: 3
Items: 
Size: 411054 Color: 1
Size: 315993 Color: 1
Size: 272943 Color: 0

Bin 3194: 11 of cap free
Amount of items: 3
Items: 
Size: 390071 Color: 1
Size: 346133 Color: 1
Size: 263786 Color: 0

Bin 3195: 11 of cap free
Amount of items: 3
Items: 
Size: 393128 Color: 1
Size: 309154 Color: 0
Size: 297708 Color: 1

Bin 3196: 11 of cap free
Amount of items: 3
Items: 
Size: 357443 Color: 1
Size: 330475 Color: 1
Size: 312072 Color: 0

Bin 3197: 12 of cap free
Amount of items: 3
Items: 
Size: 381842 Color: 1
Size: 367523 Color: 1
Size: 250624 Color: 0

Bin 3198: 12 of cap free
Amount of items: 3
Items: 
Size: 365868 Color: 1
Size: 323360 Color: 0
Size: 310761 Color: 1

Bin 3199: 12 of cap free
Amount of items: 3
Items: 
Size: 383767 Color: 1
Size: 341240 Color: 1
Size: 274982 Color: 0

Bin 3200: 12 of cap free
Amount of items: 3
Items: 
Size: 374876 Color: 1
Size: 343165 Color: 1
Size: 281948 Color: 0

Bin 3201: 12 of cap free
Amount of items: 3
Items: 
Size: 404776 Color: 1
Size: 331284 Color: 1
Size: 263929 Color: 0

Bin 3202: 12 of cap free
Amount of items: 3
Items: 
Size: 392573 Color: 1
Size: 329693 Color: 1
Size: 277723 Color: 0

Bin 3203: 12 of cap free
Amount of items: 3
Items: 
Size: 353332 Color: 1
Size: 343951 Color: 1
Size: 302706 Color: 0

Bin 3204: 13 of cap free
Amount of items: 3
Items: 
Size: 372133 Color: 1
Size: 346707 Color: 1
Size: 281148 Color: 0

Bin 3205: 13 of cap free
Amount of items: 3
Items: 
Size: 414722 Color: 1
Size: 309701 Color: 1
Size: 275565 Color: 0

Bin 3206: 13 of cap free
Amount of items: 3
Items: 
Size: 450795 Color: 1
Size: 296763 Color: 1
Size: 252430 Color: 0

Bin 3207: 13 of cap free
Amount of items: 3
Items: 
Size: 465679 Color: 1
Size: 271983 Color: 1
Size: 262326 Color: 0

Bin 3208: 13 of cap free
Amount of items: 3
Items: 
Size: 348037 Color: 1
Size: 344594 Color: 1
Size: 307357 Color: 0

Bin 3209: 13 of cap free
Amount of items: 3
Items: 
Size: 403626 Color: 1
Size: 304498 Color: 1
Size: 291864 Color: 0

Bin 3210: 14 of cap free
Amount of items: 3
Items: 
Size: 380919 Color: 1
Size: 348007 Color: 1
Size: 271061 Color: 0

Bin 3211: 14 of cap free
Amount of items: 3
Items: 
Size: 399475 Color: 1
Size: 333821 Color: 1
Size: 266691 Color: 0

Bin 3212: 14 of cap free
Amount of items: 3
Items: 
Size: 414898 Color: 1
Size: 307897 Color: 1
Size: 277192 Color: 0

Bin 3213: 14 of cap free
Amount of items: 3
Items: 
Size: 346995 Color: 1
Size: 329961 Color: 1
Size: 323031 Color: 0

Bin 3214: 14 of cap free
Amount of items: 3
Items: 
Size: 371318 Color: 1
Size: 319215 Color: 1
Size: 309454 Color: 0

Bin 3215: 14 of cap free
Amount of items: 3
Items: 
Size: 380747 Color: 1
Size: 323913 Color: 1
Size: 295327 Color: 0

Bin 3216: 15 of cap free
Amount of items: 3
Items: 
Size: 420447 Color: 1
Size: 320793 Color: 1
Size: 258746 Color: 0

Bin 3217: 15 of cap free
Amount of items: 3
Items: 
Size: 354246 Color: 1
Size: 345819 Color: 1
Size: 299921 Color: 0

Bin 3218: 15 of cap free
Amount of items: 3
Items: 
Size: 389202 Color: 1
Size: 331205 Color: 1
Size: 279579 Color: 0

Bin 3219: 15 of cap free
Amount of items: 3
Items: 
Size: 441802 Color: 1
Size: 287610 Color: 1
Size: 270574 Color: 0

Bin 3220: 16 of cap free
Amount of items: 3
Items: 
Size: 434659 Color: 1
Size: 301444 Color: 1
Size: 263882 Color: 0

Bin 3221: 16 of cap free
Amount of items: 3
Items: 
Size: 378085 Color: 1
Size: 335536 Color: 1
Size: 286364 Color: 0

Bin 3222: 16 of cap free
Amount of items: 3
Items: 
Size: 363031 Color: 1
Size: 348384 Color: 1
Size: 288570 Color: 0

Bin 3223: 16 of cap free
Amount of items: 3
Items: 
Size: 360372 Color: 1
Size: 338872 Color: 1
Size: 300741 Color: 0

Bin 3224: 17 of cap free
Amount of items: 3
Items: 
Size: 456233 Color: 1
Size: 284675 Color: 1
Size: 259076 Color: 0

Bin 3225: 17 of cap free
Amount of items: 3
Items: 
Size: 381266 Color: 1
Size: 315377 Color: 1
Size: 303341 Color: 0

Bin 3226: 17 of cap free
Amount of items: 3
Items: 
Size: 358749 Color: 1
Size: 355064 Color: 1
Size: 286171 Color: 0

Bin 3227: 18 of cap free
Amount of items: 3
Items: 
Size: 419707 Color: 1
Size: 326436 Color: 1
Size: 253840 Color: 0

Bin 3228: 18 of cap free
Amount of items: 3
Items: 
Size: 434982 Color: 1
Size: 289394 Color: 0
Size: 275607 Color: 1

Bin 3229: 18 of cap free
Amount of items: 3
Items: 
Size: 369677 Color: 1
Size: 365489 Color: 1
Size: 264817 Color: 0

Bin 3230: 18 of cap free
Amount of items: 3
Items: 
Size: 377844 Color: 1
Size: 344305 Color: 1
Size: 277834 Color: 0

Bin 3231: 18 of cap free
Amount of items: 3
Items: 
Size: 391265 Color: 1
Size: 344341 Color: 1
Size: 264377 Color: 0

Bin 3232: 18 of cap free
Amount of items: 3
Items: 
Size: 394629 Color: 1
Size: 324817 Color: 1
Size: 280537 Color: 0

Bin 3233: 19 of cap free
Amount of items: 3
Items: 
Size: 417750 Color: 1
Size: 300541 Color: 1
Size: 281691 Color: 0

Bin 3234: 19 of cap free
Amount of items: 3
Items: 
Size: 444949 Color: 1
Size: 297730 Color: 1
Size: 257303 Color: 0

Bin 3235: 20 of cap free
Amount of items: 3
Items: 
Size: 367133 Color: 1
Size: 361286 Color: 1
Size: 271562 Color: 0

Bin 3236: 20 of cap free
Amount of items: 3
Items: 
Size: 484698 Color: 1
Size: 264688 Color: 1
Size: 250595 Color: 0

Bin 3237: 20 of cap free
Amount of items: 3
Items: 
Size: 433080 Color: 1
Size: 304382 Color: 1
Size: 262519 Color: 0

Bin 3238: 21 of cap free
Amount of items: 3
Items: 
Size: 457795 Color: 1
Size: 289039 Color: 1
Size: 253146 Color: 0

Bin 3239: 21 of cap free
Amount of items: 3
Items: 
Size: 372602 Color: 1
Size: 341287 Color: 1
Size: 286091 Color: 0

Bin 3240: 21 of cap free
Amount of items: 3
Items: 
Size: 396156 Color: 1
Size: 316465 Color: 1
Size: 287359 Color: 0

Bin 3241: 22 of cap free
Amount of items: 3
Items: 
Size: 492622 Color: 1
Size: 255551 Color: 1
Size: 251806 Color: 0

Bin 3242: 23 of cap free
Amount of items: 3
Items: 
Size: 393125 Color: 1
Size: 341169 Color: 1
Size: 265684 Color: 0

Bin 3243: 23 of cap free
Amount of items: 3
Items: 
Size: 373984 Color: 1
Size: 360761 Color: 1
Size: 265233 Color: 0

Bin 3244: 24 of cap free
Amount of items: 3
Items: 
Size: 414044 Color: 1
Size: 329485 Color: 1
Size: 256448 Color: 0

Bin 3245: 25 of cap free
Amount of items: 3
Items: 
Size: 435061 Color: 1
Size: 306987 Color: 1
Size: 257928 Color: 0

Bin 3246: 25 of cap free
Amount of items: 3
Items: 
Size: 358097 Color: 1
Size: 341680 Color: 1
Size: 300199 Color: 0

Bin 3247: 25 of cap free
Amount of items: 3
Items: 
Size: 381924 Color: 1
Size: 337036 Color: 1
Size: 281016 Color: 0

Bin 3248: 25 of cap free
Amount of items: 3
Items: 
Size: 376751 Color: 1
Size: 342027 Color: 1
Size: 281198 Color: 0

Bin 3249: 25 of cap free
Amount of items: 3
Items: 
Size: 361043 Color: 1
Size: 339256 Color: 1
Size: 299677 Color: 0

Bin 3250: 25 of cap free
Amount of items: 3
Items: 
Size: 363606 Color: 1
Size: 362516 Color: 1
Size: 273854 Color: 0

Bin 3251: 26 of cap free
Amount of items: 3
Items: 
Size: 383813 Color: 1
Size: 318556 Color: 1
Size: 297606 Color: 0

Bin 3252: 28 of cap free
Amount of items: 3
Items: 
Size: 393497 Color: 1
Size: 324958 Color: 1
Size: 281518 Color: 0

Bin 3253: 29 of cap free
Amount of items: 3
Items: 
Size: 403095 Color: 1
Size: 345446 Color: 1
Size: 251431 Color: 0

Bin 3254: 30 of cap free
Amount of items: 3
Items: 
Size: 425555 Color: 1
Size: 296902 Color: 1
Size: 277514 Color: 0

Bin 3255: 31 of cap free
Amount of items: 3
Items: 
Size: 370446 Color: 1
Size: 366154 Color: 1
Size: 263370 Color: 0

Bin 3256: 33 of cap free
Amount of items: 3
Items: 
Size: 437070 Color: 1
Size: 293946 Color: 1
Size: 268952 Color: 0

Bin 3257: 34 of cap free
Amount of items: 3
Items: 
Size: 354736 Color: 1
Size: 324925 Color: 0
Size: 320306 Color: 1

Bin 3258: 35 of cap free
Amount of items: 3
Items: 
Size: 385026 Color: 1
Size: 353946 Color: 1
Size: 260994 Color: 0

Bin 3259: 36 of cap free
Amount of items: 3
Items: 
Size: 446356 Color: 1
Size: 299121 Color: 1
Size: 254488 Color: 0

Bin 3260: 36 of cap free
Amount of items: 3
Items: 
Size: 363893 Color: 1
Size: 356705 Color: 1
Size: 279367 Color: 0

Bin 3261: 37 of cap free
Amount of items: 3
Items: 
Size: 351101 Color: 1
Size: 328622 Color: 1
Size: 320241 Color: 0

Bin 3262: 38 of cap free
Amount of items: 3
Items: 
Size: 479496 Color: 1
Size: 267587 Color: 1
Size: 252880 Color: 0

Bin 3263: 38 of cap free
Amount of items: 3
Items: 
Size: 344726 Color: 1
Size: 337931 Color: 1
Size: 317306 Color: 0

Bin 3264: 38 of cap free
Amount of items: 3
Items: 
Size: 384296 Color: 1
Size: 356007 Color: 1
Size: 259660 Color: 0

Bin 3265: 38 of cap free
Amount of items: 3
Items: 
Size: 399421 Color: 1
Size: 330933 Color: 1
Size: 269609 Color: 0

Bin 3266: 38 of cap free
Amount of items: 3
Items: 
Size: 383470 Color: 1
Size: 329145 Color: 1
Size: 287348 Color: 0

Bin 3267: 40 of cap free
Amount of items: 3
Items: 
Size: 364136 Color: 1
Size: 323311 Color: 1
Size: 312514 Color: 0

Bin 3268: 41 of cap free
Amount of items: 3
Items: 
Size: 352334 Color: 1
Size: 351595 Color: 1
Size: 296031 Color: 0

Bin 3269: 41 of cap free
Amount of items: 3
Items: 
Size: 471501 Color: 1
Size: 276799 Color: 1
Size: 251660 Color: 0

Bin 3270: 42 of cap free
Amount of items: 3
Items: 
Size: 429467 Color: 1
Size: 310196 Color: 1
Size: 260296 Color: 0

Bin 3271: 43 of cap free
Amount of items: 3
Items: 
Size: 377037 Color: 1
Size: 327039 Color: 1
Size: 295882 Color: 0

Bin 3272: 44 of cap free
Amount of items: 3
Items: 
Size: 408158 Color: 1
Size: 334681 Color: 1
Size: 257118 Color: 0

Bin 3273: 44 of cap free
Amount of items: 3
Items: 
Size: 345480 Color: 1
Size: 338856 Color: 1
Size: 315621 Color: 0

Bin 3274: 44 of cap free
Amount of items: 3
Items: 
Size: 366367 Color: 1
Size: 341792 Color: 1
Size: 291798 Color: 0

Bin 3275: 47 of cap free
Amount of items: 3
Items: 
Size: 418275 Color: 1
Size: 324712 Color: 1
Size: 256967 Color: 0

Bin 3276: 48 of cap free
Amount of items: 3
Items: 
Size: 431733 Color: 1
Size: 313089 Color: 1
Size: 255131 Color: 0

Bin 3277: 50 of cap free
Amount of items: 3
Items: 
Size: 368075 Color: 1
Size: 339598 Color: 1
Size: 292278 Color: 0

Bin 3278: 51 of cap free
Amount of items: 3
Items: 
Size: 350834 Color: 1
Size: 324792 Color: 1
Size: 324324 Color: 0

Bin 3279: 53 of cap free
Amount of items: 3
Items: 
Size: 454660 Color: 1
Size: 282797 Color: 1
Size: 262491 Color: 0

Bin 3280: 54 of cap free
Amount of items: 3
Items: 
Size: 494686 Color: 1
Size: 254806 Color: 1
Size: 250455 Color: 0

Bin 3281: 56 of cap free
Amount of items: 3
Items: 
Size: 355099 Color: 1
Size: 343591 Color: 1
Size: 301255 Color: 0

Bin 3282: 56 of cap free
Amount of items: 3
Items: 
Size: 397310 Color: 1
Size: 315180 Color: 1
Size: 287455 Color: 0

Bin 3283: 58 of cap free
Amount of items: 3
Items: 
Size: 442468 Color: 1
Size: 304165 Color: 1
Size: 253310 Color: 0

Bin 3284: 60 of cap free
Amount of items: 3
Items: 
Size: 362264 Color: 1
Size: 323629 Color: 0
Size: 314048 Color: 1

Bin 3285: 61 of cap free
Amount of items: 3
Items: 
Size: 493754 Color: 1
Size: 254603 Color: 1
Size: 251583 Color: 0

Bin 3286: 61 of cap free
Amount of items: 3
Items: 
Size: 386804 Color: 1
Size: 358607 Color: 1
Size: 254529 Color: 0

Bin 3287: 62 of cap free
Amount of items: 3
Items: 
Size: 479877 Color: 1
Size: 266190 Color: 1
Size: 253872 Color: 0

Bin 3288: 63 of cap free
Amount of items: 3
Items: 
Size: 464736 Color: 1
Size: 278670 Color: 1
Size: 256532 Color: 0

Bin 3289: 64 of cap free
Amount of items: 3
Items: 
Size: 412169 Color: 1
Size: 311449 Color: 1
Size: 276319 Color: 0

Bin 3290: 71 of cap free
Amount of items: 3
Items: 
Size: 361825 Color: 1
Size: 345701 Color: 1
Size: 292404 Color: 0

Bin 3291: 72 of cap free
Amount of items: 3
Items: 
Size: 384703 Color: 1
Size: 327556 Color: 1
Size: 287670 Color: 0

Bin 3292: 72 of cap free
Amount of items: 3
Items: 
Size: 401774 Color: 1
Size: 333071 Color: 1
Size: 265084 Color: 0

Bin 3293: 74 of cap free
Amount of items: 3
Items: 
Size: 491177 Color: 1
Size: 257847 Color: 1
Size: 250903 Color: 0

Bin 3294: 75 of cap free
Amount of items: 3
Items: 
Size: 369276 Color: 1
Size: 351651 Color: 1
Size: 278999 Color: 0

Bin 3295: 77 of cap free
Amount of items: 3
Items: 
Size: 361535 Color: 1
Size: 341523 Color: 1
Size: 296866 Color: 0

Bin 3296: 77 of cap free
Amount of items: 3
Items: 
Size: 380725 Color: 1
Size: 333599 Color: 1
Size: 285600 Color: 0

Bin 3297: 81 of cap free
Amount of items: 3
Items: 
Size: 392544 Color: 1
Size: 342889 Color: 1
Size: 264487 Color: 0

Bin 3298: 93 of cap free
Amount of items: 3
Items: 
Size: 397108 Color: 1
Size: 338840 Color: 1
Size: 263960 Color: 0

Bin 3299: 95 of cap free
Amount of items: 3
Items: 
Size: 380295 Color: 1
Size: 313770 Color: 1
Size: 305841 Color: 0

Bin 3300: 111 of cap free
Amount of items: 3
Items: 
Size: 403706 Color: 1
Size: 329251 Color: 1
Size: 266933 Color: 0

Bin 3301: 129 of cap free
Amount of items: 3
Items: 
Size: 413978 Color: 1
Size: 320633 Color: 1
Size: 265261 Color: 0

Bin 3302: 130 of cap free
Amount of items: 3
Items: 
Size: 373715 Color: 1
Size: 364449 Color: 1
Size: 261707 Color: 0

Bin 3303: 134 of cap free
Amount of items: 3
Items: 
Size: 404580 Color: 1
Size: 343533 Color: 1
Size: 251754 Color: 0

Bin 3304: 136 of cap free
Amount of items: 3
Items: 
Size: 358687 Color: 1
Size: 346382 Color: 1
Size: 294796 Color: 0

Bin 3305: 148 of cap free
Amount of items: 3
Items: 
Size: 464815 Color: 1
Size: 278080 Color: 1
Size: 256958 Color: 0

Bin 3306: 150 of cap free
Amount of items: 3
Items: 
Size: 378597 Color: 1
Size: 317173 Color: 1
Size: 304081 Color: 0

Bin 3307: 150 of cap free
Amount of items: 3
Items: 
Size: 491157 Color: 1
Size: 256476 Color: 1
Size: 252218 Color: 0

Bin 3308: 153 of cap free
Amount of items: 3
Items: 
Size: 362723 Color: 1
Size: 329229 Color: 1
Size: 307896 Color: 0

Bin 3309: 180 of cap free
Amount of items: 3
Items: 
Size: 459924 Color: 1
Size: 283465 Color: 1
Size: 256432 Color: 0

Bin 3310: 181 of cap free
Amount of items: 3
Items: 
Size: 442136 Color: 1
Size: 289218 Color: 1
Size: 268466 Color: 0

Bin 3311: 220 of cap free
Amount of items: 3
Items: 
Size: 380181 Color: 1
Size: 345693 Color: 1
Size: 273907 Color: 0

Bin 3312: 221 of cap free
Amount of items: 3
Items: 
Size: 387374 Color: 1
Size: 341519 Color: 1
Size: 270887 Color: 0

Bin 3313: 223 of cap free
Amount of items: 3
Items: 
Size: 353294 Color: 1
Size: 333149 Color: 1
Size: 313335 Color: 0

Bin 3314: 235 of cap free
Amount of items: 3
Items: 
Size: 366765 Color: 1
Size: 363557 Color: 1
Size: 269444 Color: 0

Bin 3315: 235 of cap free
Amount of items: 3
Items: 
Size: 442882 Color: 1
Size: 284390 Color: 1
Size: 272494 Color: 0

Bin 3316: 281 of cap free
Amount of items: 3
Items: 
Size: 412360 Color: 1
Size: 322101 Color: 1
Size: 265259 Color: 0

Bin 3317: 296 of cap free
Amount of items: 3
Items: 
Size: 344096 Color: 1
Size: 336341 Color: 1
Size: 319268 Color: 0

Bin 3318: 300 of cap free
Amount of items: 3
Items: 
Size: 417085 Color: 1
Size: 324487 Color: 1
Size: 258129 Color: 0

Bin 3319: 300 of cap free
Amount of items: 3
Items: 
Size: 375554 Color: 1
Size: 329129 Color: 1
Size: 295018 Color: 0

Bin 3320: 315 of cap free
Amount of items: 3
Items: 
Size: 359307 Color: 1
Size: 342022 Color: 1
Size: 298357 Color: 0

Bin 3321: 322 of cap free
Amount of items: 3
Items: 
Size: 441577 Color: 1
Size: 303113 Color: 1
Size: 254989 Color: 0

Bin 3322: 370 of cap free
Amount of items: 3
Items: 
Size: 411261 Color: 1
Size: 304245 Color: 1
Size: 284125 Color: 0

Bin 3323: 447 of cap free
Amount of items: 3
Items: 
Size: 448407 Color: 1
Size: 276775 Color: 0
Size: 274372 Color: 1

Bin 3324: 581 of cap free
Amount of items: 3
Items: 
Size: 338414 Color: 1
Size: 336103 Color: 1
Size: 324903 Color: 0

Bin 3325: 645 of cap free
Amount of items: 3
Items: 
Size: 478644 Color: 1
Size: 269449 Color: 1
Size: 251263 Color: 0

Bin 3326: 1145 of cap free
Amount of items: 3
Items: 
Size: 425212 Color: 1
Size: 310491 Color: 1
Size: 263153 Color: 0

Bin 3327: 1531 of cap free
Amount of items: 3
Items: 
Size: 405385 Color: 1
Size: 316606 Color: 1
Size: 276479 Color: 0

Bin 3328: 2092 of cap free
Amount of items: 3
Items: 
Size: 375227 Color: 1
Size: 316421 Color: 1
Size: 306261 Color: 0

Bin 3329: 2114 of cap free
Amount of items: 3
Items: 
Size: 488844 Color: 1
Size: 258763 Color: 1
Size: 250280 Color: 0

Bin 3330: 6599 of cap free
Amount of items: 3
Items: 
Size: 359697 Color: 1
Size: 357841 Color: 1
Size: 275864 Color: 0

Bin 3331: 19979 of cap free
Amount of items: 3
Items: 
Size: 382902 Color: 1
Size: 299733 Color: 1
Size: 297387 Color: 0

Bin 3332: 61378 of cap free
Amount of items: 3
Items: 
Size: 349338 Color: 1
Size: 321426 Color: 1
Size: 267859 Color: 0

Bin 3333: 145750 of cap free
Amount of items: 3
Items: 
Size: 297831 Color: 1
Size: 294886 Color: 1
Size: 261534 Color: 0

Bin 3334: 246728 of cap free
Amount of items: 2
Items: 
Size: 498521 Color: 1
Size: 254752 Color: 0

Bin 3335: 501783 of cap free
Amount of items: 1
Items: 
Size: 498218 Color: 1

Total size: 3334003334
Total free space: 1000001

