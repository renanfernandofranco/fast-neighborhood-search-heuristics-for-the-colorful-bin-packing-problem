Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 13
Size: 277 Color: 0
Size: 273 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 10
Size: 336 Color: 18
Size: 264 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 7
Size: 311 Color: 2
Size: 260 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 257 Color: 16
Size: 252 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 10
Size: 262 Color: 15
Size: 254 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 10
Size: 334 Color: 2
Size: 251 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 272 Color: 11
Size: 254 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 1
Size: 360 Color: 1
Size: 300 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 12
Size: 367 Color: 6
Size: 266 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 14
Size: 327 Color: 7
Size: 274 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 13
Size: 270 Color: 15
Size: 260 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 12
Size: 257 Color: 3
Size: 251 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 15
Size: 361 Color: 0
Size: 250 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 365 Color: 2
Size: 269 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 11
Size: 278 Color: 10
Size: 258 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 12
Size: 309 Color: 14
Size: 279 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 281 Color: 12
Size: 259 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 12
Size: 303 Color: 2
Size: 282 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 7
Size: 336 Color: 0
Size: 311 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 282 Color: 10
Size: 448 Color: 1
Size: 270 Color: 19

Total size: 20000
Total free space: 0

