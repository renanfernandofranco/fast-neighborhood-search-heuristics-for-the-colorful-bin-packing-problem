Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1035 Color: 1
Size: 541 Color: 4
Size: 380 Color: 3
Size: 76 Color: 2
Size: 36 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 3
Size: 861 Color: 3
Size: 170 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 4
Size: 609 Color: 0
Size: 120 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 0
Size: 630 Color: 3
Size: 60 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 0
Size: 486 Color: 3
Size: 136 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 3
Size: 522 Color: 0
Size: 60 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 2
Size: 451 Color: 3
Size: 88 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 0
Size: 429 Color: 3
Size: 50 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 0
Size: 402 Color: 4
Size: 76 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1597 Color: 3
Size: 387 Color: 4
Size: 84 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1612 Color: 4
Size: 386 Color: 3
Size: 70 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 0
Size: 311 Color: 2
Size: 68 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 0
Size: 278 Color: 2
Size: 76 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 2
Size: 296 Color: 3
Size: 56 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 3
Size: 298 Color: 4
Size: 32 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 3
Size: 245 Color: 0
Size: 48 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 1
Size: 233 Color: 3
Size: 68 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1789 Color: 3
Size: 227 Color: 0
Size: 52 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 2
Size: 222 Color: 3
Size: 40 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 2
Size: 189 Color: 3
Size: 36 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1849 Color: 2
Size: 183 Color: 0
Size: 36 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 2
Size: 203 Color: 1
Size: 14 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 2
Size: 110 Color: 3
Size: 100 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 2
Size: 770 Color: 0
Size: 56 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1300 Color: 4
Size: 691 Color: 0
Size: 76 Color: 3

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 4
Size: 480 Color: 3
Size: 64 Color: 2

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 4
Size: 342 Color: 3
Size: 170 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 2
Size: 401 Color: 3
Size: 60 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 3
Size: 345 Color: 2
Size: 60 Color: 4

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 4
Size: 320 Color: 0
Size: 16 Color: 3

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1759 Color: 1
Size: 232 Color: 3
Size: 76 Color: 2

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 3
Size: 212 Color: 0
Size: 42 Color: 2

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 1816 Color: 3
Size: 251 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 0
Size: 198 Color: 3
Size: 72 Color: 4

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 3
Size: 182 Color: 1
Size: 64 Color: 2

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 3
Size: 696 Color: 1
Size: 56 Color: 4

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 4
Size: 521 Color: 3
Size: 124 Color: 4

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 2
Size: 190 Color: 3
Size: 178 Color: 2

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 1834 Color: 4
Size: 232 Color: 0

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1428 Color: 0
Size: 553 Color: 3
Size: 84 Color: 4

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 2
Size: 318 Color: 3
Size: 50 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 0
Size: 207 Color: 1
Size: 8 Color: 4

Bin 43: 4 of cap free
Amount of items: 15
Items: 
Size: 302 Color: 3
Size: 242 Color: 3
Size: 230 Color: 1
Size: 172 Color: 1
Size: 168 Color: 1
Size: 146 Color: 2
Size: 126 Color: 3
Size: 112 Color: 3
Size: 106 Color: 0
Size: 100 Color: 1
Size: 96 Color: 4
Size: 78 Color: 1
Size: 78 Color: 0
Size: 62 Color: 3
Size: 46 Color: 2

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1041 Color: 4
Size: 853 Color: 1
Size: 170 Color: 0

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 3
Size: 281 Color: 1
Size: 136 Color: 4

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 3
Size: 374 Color: 4

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 2
Size: 310 Color: 1
Size: 24 Color: 2

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1550 Color: 4
Size: 511 Color: 0

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 4
Size: 283 Color: 0

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 1049 Color: 1
Size: 694 Color: 0
Size: 317 Color: 3

Bin 51: 8 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 3
Size: 805 Color: 1
Size: 66 Color: 0

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1197 Color: 1
Size: 861 Color: 4

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 2
Size: 851 Color: 1
Size: 168 Color: 0

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1706 Color: 0
Size: 351 Color: 4

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 2
Size: 434 Color: 1

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 3
Size: 213 Color: 0

Bin 57: 14 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 4
Size: 857 Color: 4
Size: 152 Color: 3

Bin 58: 14 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 4
Size: 768 Color: 3
Size: 48 Color: 1

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 4
Size: 259 Color: 0

Bin 60: 16 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 0
Size: 862 Color: 3
Size: 44 Color: 1

Bin 61: 20 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 4
Size: 393 Color: 1

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1307 Color: 3
Size: 733 Color: 2

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 0
Size: 635 Color: 4

Bin 64: 33 of cap free
Amount of items: 2
Items: 
Size: 1457 Color: 0
Size: 578 Color: 1

Bin 65: 35 of cap free
Amount of items: 2
Items: 
Size: 1605 Color: 4
Size: 428 Color: 2

Bin 66: 1728 of cap free
Amount of items: 8
Items: 
Size: 56 Color: 3
Size: 48 Color: 4
Size: 44 Color: 2
Size: 40 Color: 4
Size: 40 Color: 2
Size: 40 Color: 1
Size: 36 Color: 4
Size: 36 Color: 3

Total size: 134420
Total free space: 2068

