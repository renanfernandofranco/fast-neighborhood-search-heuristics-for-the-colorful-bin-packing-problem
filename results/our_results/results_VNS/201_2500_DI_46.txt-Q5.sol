Capicity Bin: 2328
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1218 Color: 2
Size: 1016 Color: 4
Size: 54 Color: 3
Size: 40 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 0
Size: 850 Color: 1
Size: 156 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 0
Size: 565 Color: 0
Size: 112 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 0
Size: 531 Color: 1
Size: 124 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 0
Size: 533 Color: 2
Size: 110 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 0
Size: 554 Color: 3
Size: 64 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 3
Size: 478 Color: 4
Size: 36 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 4
Size: 455 Color: 2
Size: 90 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 0
Size: 342 Color: 2
Size: 88 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 3
Size: 262 Color: 2
Size: 144 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 2
Size: 337 Color: 1
Size: 66 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 3
Size: 323 Color: 0
Size: 64 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 3
Size: 302 Color: 4
Size: 56 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 0
Size: 192 Color: 4
Size: 58 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 2
Size: 202 Color: 4
Size: 68 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 1
Size: 166 Color: 2
Size: 76 Color: 3

Bin 17: 1 of cap free
Amount of items: 17
Items: 
Size: 553 Color: 1
Size: 198 Color: 4
Size: 156 Color: 2
Size: 152 Color: 0
Size: 128 Color: 4
Size: 128 Color: 4
Size: 128 Color: 2
Size: 120 Color: 4
Size: 106 Color: 1
Size: 104 Color: 3
Size: 104 Color: 1
Size: 90 Color: 1
Size: 88 Color: 3
Size: 76 Color: 0
Size: 76 Color: 0
Size: 64 Color: 3
Size: 56 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 1
Size: 926 Color: 1
Size: 76 Color: 3

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 0
Size: 391 Color: 4
Size: 246 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 2
Size: 462 Color: 4
Size: 66 Color: 0

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1861 Color: 0
Size: 466 Color: 3

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 2
Size: 441 Color: 0
Size: 28 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 4
Size: 282 Color: 0
Size: 66 Color: 2

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 4
Size: 333 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1999 Color: 2
Size: 184 Color: 0
Size: 144 Color: 3

Bin 26: 2 of cap free
Amount of items: 5
Items: 
Size: 1165 Color: 1
Size: 469 Color: 4
Size: 448 Color: 4
Size: 192 Color: 1
Size: 52 Color: 3

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 2
Size: 802 Color: 0
Size: 258 Color: 3

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 4
Size: 947 Color: 0
Size: 44 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 4
Size: 537 Color: 1
Size: 322 Color: 3

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 4
Size: 645 Color: 0
Size: 108 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 4
Size: 646 Color: 0
Size: 106 Color: 3

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 2
Size: 561 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1854 Color: 0
Size: 348 Color: 2
Size: 124 Color: 1

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1855 Color: 2
Size: 471 Color: 3

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 4
Size: 210 Color: 0
Size: 94 Color: 2

Bin 36: 3 of cap free
Amount of items: 4
Items: 
Size: 1152 Color: 2
Size: 738 Color: 2
Size: 291 Color: 3
Size: 144 Color: 4

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 3
Size: 723 Color: 4
Size: 48 Color: 1

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 4
Size: 275 Color: 2
Size: 96 Color: 0

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 1
Size: 970 Color: 2
Size: 188 Color: 3

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 4
Size: 864 Color: 2
Size: 90 Color: 1

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 741 Color: 0
Size: 28 Color: 1

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 0
Size: 538 Color: 4
Size: 92 Color: 2

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 4
Size: 630 Color: 1
Size: 28 Color: 1

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 1
Size: 631 Color: 2

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 2
Size: 362 Color: 4
Size: 20 Color: 2

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 2
Size: 226 Color: 3
Size: 4 Color: 0

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 3
Size: 829 Color: 4
Size: 48 Color: 4

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 4
Size: 381 Color: 0
Size: 164 Color: 4

Bin 49: 5 of cap free
Amount of items: 2
Items: 
Size: 1929 Color: 2
Size: 394 Color: 1

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 1871 Color: 2
Size: 451 Color: 3

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 3
Size: 288 Color: 2

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 4
Size: 459 Color: 3
Size: 104 Color: 0

Bin 53: 8 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 3
Size: 341 Color: 1
Size: 192 Color: 1

Bin 54: 8 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 2
Size: 346 Color: 4

Bin 55: 9 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 0
Size: 654 Color: 4

Bin 56: 9 of cap free
Amount of items: 2
Items: 
Size: 1921 Color: 1
Size: 398 Color: 2

Bin 57: 12 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 3
Size: 770 Color: 0

Bin 58: 12 of cap free
Amount of items: 3
Items: 
Size: 1864 Color: 0
Size: 324 Color: 1
Size: 128 Color: 1

Bin 59: 13 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 0
Size: 578 Color: 2
Size: 48 Color: 4

Bin 60: 15 of cap free
Amount of items: 2
Items: 
Size: 1779 Color: 2
Size: 534 Color: 0

Bin 61: 16 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 2
Size: 969 Color: 0
Size: 176 Color: 4

Bin 62: 16 of cap free
Amount of items: 2
Items: 
Size: 2014 Color: 4
Size: 298 Color: 1

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1327 Color: 3
Size: 971 Color: 0

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1461 Color: 1
Size: 835 Color: 3

Bin 65: 36 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 4
Size: 886 Color: 2

Bin 66: 2008 of cap free
Amount of items: 6
Items: 
Size: 92 Color: 4
Size: 56 Color: 3
Size: 52 Color: 2
Size: 40 Color: 3
Size: 40 Color: 1
Size: 40 Color: 0

Total size: 151320
Total free space: 2328

