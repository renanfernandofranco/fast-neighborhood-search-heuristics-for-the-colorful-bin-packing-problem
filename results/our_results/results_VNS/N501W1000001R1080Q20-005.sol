Capicity Bin: 1000001
Lower Bound: 232

Bins used: 234
Amount of Colors: 20

Bin 1: 8 of cap free
Amount of items: 3
Items: 
Size: 657048 Color: 4
Size: 171919 Color: 6
Size: 171026 Color: 0

Bin 2: 16 of cap free
Amount of items: 2
Items: 
Size: 729171 Color: 12
Size: 270814 Color: 13

Bin 3: 18 of cap free
Amount of items: 2
Items: 
Size: 640032 Color: 13
Size: 359951 Color: 17

Bin 4: 20 of cap free
Amount of items: 2
Items: 
Size: 684891 Color: 19
Size: 315090 Color: 15

Bin 5: 33 of cap free
Amount of items: 2
Items: 
Size: 798572 Color: 1
Size: 201396 Color: 19

Bin 6: 53 of cap free
Amount of items: 2
Items: 
Size: 741545 Color: 14
Size: 258403 Color: 6

Bin 7: 68 of cap free
Amount of items: 2
Items: 
Size: 670310 Color: 3
Size: 329623 Color: 2

Bin 8: 69 of cap free
Amount of items: 2
Items: 
Size: 536916 Color: 14
Size: 463016 Color: 19

Bin 9: 74 of cap free
Amount of items: 2
Items: 
Size: 795456 Color: 1
Size: 204471 Color: 10

Bin 10: 159 of cap free
Amount of items: 2
Items: 
Size: 676354 Color: 19
Size: 323488 Color: 13

Bin 11: 160 of cap free
Amount of items: 3
Items: 
Size: 671623 Color: 3
Size: 165731 Color: 13
Size: 162487 Color: 4

Bin 12: 166 of cap free
Amount of items: 2
Items: 
Size: 734399 Color: 8
Size: 265436 Color: 4

Bin 13: 187 of cap free
Amount of items: 2
Items: 
Size: 680807 Color: 14
Size: 319007 Color: 17

Bin 14: 187 of cap free
Amount of items: 3
Items: 
Size: 739953 Color: 9
Size: 141888 Color: 12
Size: 117973 Color: 15

Bin 15: 195 of cap free
Amount of items: 2
Items: 
Size: 544222 Color: 9
Size: 455584 Color: 6

Bin 16: 206 of cap free
Amount of items: 3
Items: 
Size: 657888 Color: 4
Size: 172892 Color: 9
Size: 169015 Color: 5

Bin 17: 213 of cap free
Amount of items: 2
Items: 
Size: 737489 Color: 14
Size: 262299 Color: 17

Bin 18: 214 of cap free
Amount of items: 2
Items: 
Size: 702993 Color: 8
Size: 296794 Color: 5

Bin 19: 233 of cap free
Amount of items: 2
Items: 
Size: 662883 Color: 0
Size: 336885 Color: 14

Bin 20: 239 of cap free
Amount of items: 2
Items: 
Size: 702383 Color: 14
Size: 297379 Color: 19

Bin 21: 244 of cap free
Amount of items: 2
Items: 
Size: 576120 Color: 4
Size: 423637 Color: 6

Bin 22: 254 of cap free
Amount of items: 2
Items: 
Size: 720964 Color: 15
Size: 278783 Color: 2

Bin 23: 257 of cap free
Amount of items: 3
Items: 
Size: 670226 Color: 7
Size: 169099 Color: 4
Size: 160419 Color: 9

Bin 24: 273 of cap free
Amount of items: 2
Items: 
Size: 739109 Color: 3
Size: 260619 Color: 15

Bin 25: 289 of cap free
Amount of items: 3
Items: 
Size: 655483 Color: 4
Size: 184741 Color: 1
Size: 159488 Color: 3

Bin 26: 290 of cap free
Amount of items: 2
Items: 
Size: 631147 Color: 3
Size: 368564 Color: 13

Bin 27: 319 of cap free
Amount of items: 3
Items: 
Size: 794101 Color: 12
Size: 104291 Color: 19
Size: 101290 Color: 6

Bin 28: 326 of cap free
Amount of items: 2
Items: 
Size: 737721 Color: 1
Size: 261954 Color: 18

Bin 29: 328 of cap free
Amount of items: 2
Items: 
Size: 755010 Color: 12
Size: 244663 Color: 7

Bin 30: 336 of cap free
Amount of items: 2
Items: 
Size: 696079 Color: 17
Size: 303586 Color: 7

Bin 31: 346 of cap free
Amount of items: 2
Items: 
Size: 755669 Color: 2
Size: 243986 Color: 18

Bin 32: 352 of cap free
Amount of items: 2
Items: 
Size: 733220 Color: 0
Size: 266429 Color: 5

Bin 33: 382 of cap free
Amount of items: 2
Items: 
Size: 673892 Color: 1
Size: 325727 Color: 3

Bin 34: 413 of cap free
Amount of items: 3
Items: 
Size: 710911 Color: 13
Size: 155426 Color: 7
Size: 133251 Color: 1

Bin 35: 420 of cap free
Amount of items: 2
Items: 
Size: 525943 Color: 10
Size: 473638 Color: 2

Bin 36: 451 of cap free
Amount of items: 2
Items: 
Size: 543457 Color: 10
Size: 456093 Color: 4

Bin 37: 466 of cap free
Amount of items: 2
Items: 
Size: 733148 Color: 10
Size: 266387 Color: 7

Bin 38: 482 of cap free
Amount of items: 3
Items: 
Size: 653899 Color: 13
Size: 173586 Color: 10
Size: 172034 Color: 4

Bin 39: 493 of cap free
Amount of items: 2
Items: 
Size: 553132 Color: 18
Size: 446376 Color: 11

Bin 40: 496 of cap free
Amount of items: 2
Items: 
Size: 787195 Color: 16
Size: 212310 Color: 14

Bin 41: 499 of cap free
Amount of items: 3
Items: 
Size: 718894 Color: 18
Size: 160926 Color: 7
Size: 119682 Color: 1

Bin 42: 523 of cap free
Amount of items: 2
Items: 
Size: 582463 Color: 12
Size: 417015 Color: 10

Bin 43: 528 of cap free
Amount of items: 2
Items: 
Size: 648869 Color: 4
Size: 350604 Color: 0

Bin 44: 537 of cap free
Amount of items: 2
Items: 
Size: 525989 Color: 6
Size: 473475 Color: 3

Bin 45: 539 of cap free
Amount of items: 2
Items: 
Size: 524937 Color: 2
Size: 474525 Color: 19

Bin 46: 551 of cap free
Amount of items: 2
Items: 
Size: 578111 Color: 14
Size: 421339 Color: 19

Bin 47: 589 of cap free
Amount of items: 2
Items: 
Size: 762020 Color: 0
Size: 237392 Color: 15

Bin 48: 599 of cap free
Amount of items: 3
Items: 
Size: 652027 Color: 4
Size: 184655 Color: 5
Size: 162720 Color: 16

Bin 49: 613 of cap free
Amount of items: 2
Items: 
Size: 791296 Color: 0
Size: 208092 Color: 15

Bin 50: 615 of cap free
Amount of items: 2
Items: 
Size: 633919 Color: 12
Size: 365467 Color: 8

Bin 51: 628 of cap free
Amount of items: 2
Items: 
Size: 791027 Color: 5
Size: 208346 Color: 0

Bin 52: 650 of cap free
Amount of items: 2
Items: 
Size: 565577 Color: 8
Size: 433774 Color: 16

Bin 53: 658 of cap free
Amount of items: 2
Items: 
Size: 499885 Color: 19
Size: 499458 Color: 18

Bin 54: 668 of cap free
Amount of items: 2
Items: 
Size: 665765 Color: 19
Size: 333568 Color: 15

Bin 55: 677 of cap free
Amount of items: 2
Items: 
Size: 760609 Color: 14
Size: 238715 Color: 9

Bin 56: 683 of cap free
Amount of items: 3
Items: 
Size: 789446 Color: 13
Size: 107580 Color: 13
Size: 102292 Color: 10

Bin 57: 699 of cap free
Amount of items: 2
Items: 
Size: 708522 Color: 15
Size: 290780 Color: 4

Bin 58: 706 of cap free
Amount of items: 2
Items: 
Size: 615073 Color: 5
Size: 384222 Color: 1

Bin 59: 709 of cap free
Amount of items: 3
Items: 
Size: 653870 Color: 8
Size: 180054 Color: 0
Size: 165368 Color: 6

Bin 60: 733 of cap free
Amount of items: 2
Items: 
Size: 790284 Color: 17
Size: 208984 Color: 0

Bin 61: 747 of cap free
Amount of items: 3
Items: 
Size: 726918 Color: 11
Size: 149829 Color: 1
Size: 122507 Color: 15

Bin 62: 749 of cap free
Amount of items: 2
Items: 
Size: 608244 Color: 3
Size: 391008 Color: 2

Bin 63: 751 of cap free
Amount of items: 2
Items: 
Size: 606375 Color: 3
Size: 392875 Color: 2

Bin 64: 753 of cap free
Amount of items: 2
Items: 
Size: 722842 Color: 4
Size: 276406 Color: 9

Bin 65: 759 of cap free
Amount of items: 2
Items: 
Size: 620898 Color: 8
Size: 378344 Color: 10

Bin 66: 764 of cap free
Amount of items: 3
Items: 
Size: 778821 Color: 7
Size: 113766 Color: 17
Size: 106650 Color: 1

Bin 67: 783 of cap free
Amount of items: 2
Items: 
Size: 548133 Color: 15
Size: 451085 Color: 6

Bin 68: 813 of cap free
Amount of items: 2
Items: 
Size: 532832 Color: 14
Size: 466356 Color: 11

Bin 69: 813 of cap free
Amount of items: 2
Items: 
Size: 538429 Color: 13
Size: 460759 Color: 6

Bin 70: 824 of cap free
Amount of items: 2
Items: 
Size: 671992 Color: 11
Size: 327185 Color: 1

Bin 71: 841 of cap free
Amount of items: 2
Items: 
Size: 674542 Color: 4
Size: 324618 Color: 1

Bin 72: 944 of cap free
Amount of items: 3
Items: 
Size: 772712 Color: 4
Size: 117877 Color: 1
Size: 108468 Color: 2

Bin 73: 954 of cap free
Amount of items: 2
Items: 
Size: 633674 Color: 6
Size: 365373 Color: 18

Bin 74: 958 of cap free
Amount of items: 2
Items: 
Size: 564271 Color: 5
Size: 434772 Color: 1

Bin 75: 961 of cap free
Amount of items: 3
Items: 
Size: 730369 Color: 9
Size: 147153 Color: 0
Size: 121518 Color: 3

Bin 76: 992 of cap free
Amount of items: 2
Items: 
Size: 589348 Color: 16
Size: 409661 Color: 17

Bin 77: 995 of cap free
Amount of items: 2
Items: 
Size: 705042 Color: 10
Size: 293964 Color: 16

Bin 78: 1003 of cap free
Amount of items: 2
Items: 
Size: 713755 Color: 15
Size: 285243 Color: 10

Bin 79: 1021 of cap free
Amount of items: 2
Items: 
Size: 581175 Color: 18
Size: 417805 Color: 10

Bin 80: 1029 of cap free
Amount of items: 2
Items: 
Size: 783844 Color: 10
Size: 215128 Color: 3

Bin 81: 1049 of cap free
Amount of items: 2
Items: 
Size: 713539 Color: 9
Size: 285413 Color: 15

Bin 82: 1055 of cap free
Amount of items: 2
Items: 
Size: 525804 Color: 13
Size: 473142 Color: 10

Bin 83: 1128 of cap free
Amount of items: 2
Items: 
Size: 632083 Color: 1
Size: 366790 Color: 12

Bin 84: 1129 of cap free
Amount of items: 2
Items: 
Size: 700222 Color: 8
Size: 298650 Color: 16

Bin 85: 1157 of cap free
Amount of items: 2
Items: 
Size: 515596 Color: 1
Size: 483248 Color: 2

Bin 86: 1160 of cap free
Amount of items: 2
Items: 
Size: 735760 Color: 8
Size: 263081 Color: 14

Bin 87: 1195 of cap free
Amount of items: 2
Items: 
Size: 696536 Color: 9
Size: 302270 Color: 7

Bin 88: 1218 of cap free
Amount of items: 2
Items: 
Size: 743477 Color: 11
Size: 255306 Color: 6

Bin 89: 1246 of cap free
Amount of items: 2
Items: 
Size: 750110 Color: 1
Size: 248645 Color: 10

Bin 90: 1339 of cap free
Amount of items: 2
Items: 
Size: 595903 Color: 14
Size: 402759 Color: 17

Bin 91: 1341 of cap free
Amount of items: 3
Items: 
Size: 513879 Color: 16
Size: 243058 Color: 19
Size: 241723 Color: 11

Bin 92: 1343 of cap free
Amount of items: 2
Items: 
Size: 703265 Color: 4
Size: 295393 Color: 11

Bin 93: 1385 of cap free
Amount of items: 2
Items: 
Size: 709351 Color: 19
Size: 289265 Color: 6

Bin 94: 1415 of cap free
Amount of items: 2
Items: 
Size: 797050 Color: 16
Size: 201536 Color: 10

Bin 95: 1416 of cap free
Amount of items: 2
Items: 
Size: 668381 Color: 3
Size: 330204 Color: 8

Bin 96: 1418 of cap free
Amount of items: 2
Items: 
Size: 622631 Color: 19
Size: 375952 Color: 2

Bin 97: 1435 of cap free
Amount of items: 2
Items: 
Size: 738480 Color: 4
Size: 260086 Color: 7

Bin 98: 1454 of cap free
Amount of items: 2
Items: 
Size: 512794 Color: 8
Size: 485753 Color: 5

Bin 99: 1455 of cap free
Amount of items: 2
Items: 
Size: 583404 Color: 7
Size: 415142 Color: 4

Bin 100: 1515 of cap free
Amount of items: 2
Items: 
Size: 585117 Color: 7
Size: 413369 Color: 13

Bin 101: 1554 of cap free
Amount of items: 2
Items: 
Size: 513318 Color: 5
Size: 485129 Color: 12

Bin 102: 1580 of cap free
Amount of items: 2
Items: 
Size: 738682 Color: 6
Size: 259739 Color: 7

Bin 103: 1642 of cap free
Amount of items: 2
Items: 
Size: 752550 Color: 8
Size: 245809 Color: 13

Bin 104: 1684 of cap free
Amount of items: 2
Items: 
Size: 563873 Color: 1
Size: 434444 Color: 18

Bin 105: 1720 of cap free
Amount of items: 2
Items: 
Size: 773137 Color: 8
Size: 225144 Color: 18

Bin 106: 1729 of cap free
Amount of items: 3
Items: 
Size: 649783 Color: 18
Size: 174346 Color: 0
Size: 174143 Color: 13

Bin 107: 1777 of cap free
Amount of items: 2
Items: 
Size: 706829 Color: 15
Size: 291395 Color: 19

Bin 108: 1818 of cap free
Amount of items: 2
Items: 
Size: 532785 Color: 1
Size: 465398 Color: 9

Bin 109: 1825 of cap free
Amount of items: 2
Items: 
Size: 799461 Color: 6
Size: 198715 Color: 0

Bin 110: 1890 of cap free
Amount of items: 2
Items: 
Size: 646442 Color: 12
Size: 351669 Color: 4

Bin 111: 1929 of cap free
Amount of items: 2
Items: 
Size: 662820 Color: 11
Size: 335252 Color: 16

Bin 112: 1930 of cap free
Amount of items: 2
Items: 
Size: 545949 Color: 15
Size: 452122 Color: 4

Bin 113: 1936 of cap free
Amount of items: 3
Items: 
Size: 695472 Color: 9
Size: 156853 Color: 17
Size: 145740 Color: 2

Bin 114: 2009 of cap free
Amount of items: 2
Items: 
Size: 588476 Color: 19
Size: 409516 Color: 9

Bin 115: 2040 of cap free
Amount of items: 2
Items: 
Size: 650653 Color: 10
Size: 347308 Color: 1

Bin 116: 2045 of cap free
Amount of items: 3
Items: 
Size: 627074 Color: 12
Size: 189388 Color: 0
Size: 181494 Color: 0

Bin 117: 2073 of cap free
Amount of items: 2
Items: 
Size: 761813 Color: 8
Size: 236115 Color: 2

Bin 118: 2083 of cap free
Amount of items: 2
Items: 
Size: 630651 Color: 0
Size: 367267 Color: 1

Bin 119: 2147 of cap free
Amount of items: 2
Items: 
Size: 611512 Color: 10
Size: 386342 Color: 6

Bin 120: 2254 of cap free
Amount of items: 3
Items: 
Size: 793067 Color: 14
Size: 102374 Color: 11
Size: 102306 Color: 12

Bin 121: 2256 of cap free
Amount of items: 2
Items: 
Size: 778999 Color: 7
Size: 218746 Color: 15

Bin 122: 2312 of cap free
Amount of items: 2
Items: 
Size: 683037 Color: 9
Size: 314652 Color: 18

Bin 123: 2362 of cap free
Amount of items: 2
Items: 
Size: 658833 Color: 7
Size: 338806 Color: 1

Bin 124: 2375 of cap free
Amount of items: 2
Items: 
Size: 517304 Color: 10
Size: 480322 Color: 12

Bin 125: 2429 of cap free
Amount of items: 2
Items: 
Size: 536550 Color: 15
Size: 461022 Color: 13

Bin 126: 2562 of cap free
Amount of items: 2
Items: 
Size: 645291 Color: 7
Size: 352148 Color: 12

Bin 127: 2571 of cap free
Amount of items: 2
Items: 
Size: 725536 Color: 6
Size: 271894 Color: 8

Bin 128: 2581 of cap free
Amount of items: 2
Items: 
Size: 526926 Color: 8
Size: 470494 Color: 12

Bin 129: 2646 of cap free
Amount of items: 2
Items: 
Size: 577845 Color: 6
Size: 419510 Color: 9

Bin 130: 2740 of cap free
Amount of items: 2
Items: 
Size: 774992 Color: 0
Size: 222269 Color: 6

Bin 131: 2760 of cap free
Amount of items: 2
Items: 
Size: 746210 Color: 10
Size: 251031 Color: 17

Bin 132: 2837 of cap free
Amount of items: 3
Items: 
Size: 507476 Color: 5
Size: 254720 Color: 10
Size: 234968 Color: 11

Bin 133: 2852 of cap free
Amount of items: 2
Items: 
Size: 615985 Color: 14
Size: 381164 Color: 9

Bin 134: 2902 of cap free
Amount of items: 2
Items: 
Size: 672114 Color: 3
Size: 324985 Color: 10

Bin 135: 2930 of cap free
Amount of items: 2
Items: 
Size: 766889 Color: 13
Size: 230182 Color: 6

Bin 136: 3240 of cap free
Amount of items: 2
Items: 
Size: 685935 Color: 19
Size: 310826 Color: 17

Bin 137: 3344 of cap free
Amount of items: 3
Items: 
Size: 754502 Color: 18
Size: 131525 Color: 9
Size: 110630 Color: 0

Bin 138: 3350 of cap free
Amount of items: 2
Items: 
Size: 715421 Color: 18
Size: 281230 Color: 4

Bin 139: 3664 of cap free
Amount of items: 2
Items: 
Size: 622489 Color: 1
Size: 373848 Color: 2

Bin 140: 3703 of cap free
Amount of items: 2
Items: 
Size: 778396 Color: 10
Size: 217902 Color: 17

Bin 141: 3711 of cap free
Amount of items: 2
Items: 
Size: 607467 Color: 15
Size: 388823 Color: 12

Bin 142: 3732 of cap free
Amount of items: 2
Items: 
Size: 570482 Color: 15
Size: 425787 Color: 5

Bin 143: 3804 of cap free
Amount of items: 2
Items: 
Size: 525227 Color: 1
Size: 470970 Color: 13

Bin 144: 3841 of cap free
Amount of items: 2
Items: 
Size: 667437 Color: 17
Size: 328723 Color: 7

Bin 145: 3903 of cap free
Amount of items: 2
Items: 
Size: 636983 Color: 13
Size: 359115 Color: 5

Bin 146: 3926 of cap free
Amount of items: 2
Items: 
Size: 658815 Color: 13
Size: 337260 Color: 8

Bin 147: 3943 of cap free
Amount of items: 2
Items: 
Size: 675590 Color: 6
Size: 320468 Color: 15

Bin 148: 3975 of cap free
Amount of items: 2
Items: 
Size: 572338 Color: 11
Size: 423688 Color: 9

Bin 149: 4001 of cap free
Amount of items: 2
Items: 
Size: 503152 Color: 19
Size: 492848 Color: 17

Bin 150: 4028 of cap free
Amount of items: 2
Items: 
Size: 778382 Color: 13
Size: 217591 Color: 11

Bin 151: 4066 of cap free
Amount of items: 2
Items: 
Size: 701997 Color: 12
Size: 293938 Color: 3

Bin 152: 4256 of cap free
Amount of items: 2
Items: 
Size: 548841 Color: 9
Size: 446904 Color: 10

Bin 153: 4441 of cap free
Amount of items: 2
Items: 
Size: 778319 Color: 2
Size: 217241 Color: 5

Bin 154: 4541 of cap free
Amount of items: 2
Items: 
Size: 561907 Color: 8
Size: 433553 Color: 11

Bin 155: 4698 of cap free
Amount of items: 3
Items: 
Size: 524799 Color: 2
Size: 235987 Color: 12
Size: 234517 Color: 3

Bin 156: 4698 of cap free
Amount of items: 2
Items: 
Size: 701527 Color: 8
Size: 293776 Color: 13

Bin 157: 4779 of cap free
Amount of items: 2
Items: 
Size: 535862 Color: 13
Size: 459360 Color: 0

Bin 158: 4801 of cap free
Amount of items: 2
Items: 
Size: 612605 Color: 6
Size: 382595 Color: 13

Bin 159: 5231 of cap free
Amount of items: 3
Items: 
Size: 723676 Color: 16
Size: 140313 Color: 15
Size: 130781 Color: 8

Bin 160: 5254 of cap free
Amount of items: 3
Items: 
Size: 626960 Color: 6
Size: 189141 Color: 17
Size: 178646 Color: 5

Bin 161: 5438 of cap free
Amount of items: 3
Items: 
Size: 759321 Color: 8
Size: 123434 Color: 4
Size: 111808 Color: 18

Bin 162: 5562 of cap free
Amount of items: 2
Items: 
Size: 712090 Color: 3
Size: 282349 Color: 8

Bin 163: 5633 of cap free
Amount of items: 2
Items: 
Size: 722705 Color: 0
Size: 271663 Color: 2

Bin 164: 5667 of cap free
Amount of items: 2
Items: 
Size: 766656 Color: 0
Size: 227678 Color: 13

Bin 165: 5712 of cap free
Amount of items: 2
Items: 
Size: 549154 Color: 10
Size: 445135 Color: 15

Bin 166: 5755 of cap free
Amount of items: 2
Items: 
Size: 503124 Color: 19
Size: 491122 Color: 17

Bin 167: 5774 of cap free
Amount of items: 2
Items: 
Size: 573866 Color: 11
Size: 420361 Color: 15

Bin 168: 5838 of cap free
Amount of items: 2
Items: 
Size: 712931 Color: 8
Size: 281232 Color: 5

Bin 169: 6062 of cap free
Amount of items: 2
Items: 
Size: 534871 Color: 6
Size: 459068 Color: 8

Bin 170: 6106 of cap free
Amount of items: 2
Items: 
Size: 599108 Color: 3
Size: 394787 Color: 9

Bin 171: 6820 of cap free
Amount of items: 3
Items: 
Size: 502373 Color: 3
Size: 247511 Color: 7
Size: 243297 Color: 5

Bin 172: 7226 of cap free
Amount of items: 3
Items: 
Size: 694242 Color: 13
Size: 150190 Color: 8
Size: 148343 Color: 0

Bin 173: 8212 of cap free
Amount of items: 2
Items: 
Size: 524253 Color: 17
Size: 467536 Color: 16

Bin 174: 8309 of cap free
Amount of items: 2
Items: 
Size: 711854 Color: 18
Size: 279838 Color: 6

Bin 175: 8461 of cap free
Amount of items: 2
Items: 
Size: 560100 Color: 4
Size: 431440 Color: 11

Bin 176: 8769 of cap free
Amount of items: 2
Items: 
Size: 711582 Color: 16
Size: 279650 Color: 7

Bin 177: 8971 of cap free
Amount of items: 2
Items: 
Size: 789747 Color: 19
Size: 201283 Color: 18

Bin 178: 9154 of cap free
Amount of items: 2
Items: 
Size: 509863 Color: 5
Size: 480984 Color: 10

Bin 179: 9427 of cap free
Amount of items: 2
Items: 
Size: 596343 Color: 14
Size: 394231 Color: 9

Bin 180: 10307 of cap free
Amount of items: 2
Items: 
Size: 766449 Color: 17
Size: 223245 Color: 1

Bin 181: 10408 of cap free
Amount of items: 2
Items: 
Size: 720892 Color: 9
Size: 268701 Color: 17

Bin 182: 10758 of cap free
Amount of items: 2
Items: 
Size: 698547 Color: 19
Size: 290696 Color: 6

Bin 183: 10938 of cap free
Amount of items: 2
Items: 
Size: 519225 Color: 15
Size: 469838 Color: 17

Bin 184: 10989 of cap free
Amount of items: 2
Items: 
Size: 630008 Color: 13
Size: 359004 Color: 12

Bin 185: 11675 of cap free
Amount of items: 2
Items: 
Size: 629676 Color: 7
Size: 358650 Color: 14

Bin 186: 12143 of cap free
Amount of items: 2
Items: 
Size: 575036 Color: 7
Size: 412822 Color: 6

Bin 187: 12589 of cap free
Amount of items: 2
Items: 
Size: 606183 Color: 1
Size: 381229 Color: 7

Bin 188: 13812 of cap free
Amount of items: 2
Items: 
Size: 506700 Color: 13
Size: 479489 Color: 15

Bin 189: 14408 of cap free
Amount of items: 2
Items: 
Size: 555098 Color: 7
Size: 430495 Color: 17

Bin 190: 14446 of cap free
Amount of items: 2
Items: 
Size: 665722 Color: 3
Size: 319833 Color: 7

Bin 191: 15267 of cap free
Amount of items: 2
Items: 
Size: 577781 Color: 12
Size: 406953 Color: 1

Bin 192: 15270 of cap free
Amount of items: 2
Items: 
Size: 516107 Color: 15
Size: 468624 Color: 17

Bin 193: 15424 of cap free
Amount of items: 2
Items: 
Size: 554420 Color: 5
Size: 430157 Color: 2

Bin 194: 15494 of cap free
Amount of items: 2
Items: 
Size: 665208 Color: 5
Size: 319299 Color: 18

Bin 195: 16130 of cap free
Amount of items: 2
Items: 
Size: 628189 Color: 5
Size: 355682 Color: 19

Bin 196: 16807 of cap free
Amount of items: 2
Items: 
Size: 597778 Color: 9
Size: 385416 Color: 10

Bin 197: 17774 of cap free
Amount of items: 2
Items: 
Size: 552106 Color: 16
Size: 430121 Color: 7

Bin 198: 18500 of cap free
Amount of items: 2
Items: 
Size: 661707 Color: 1
Size: 319794 Color: 4

Bin 199: 19590 of cap free
Amount of items: 2
Items: 
Size: 786253 Color: 6
Size: 194158 Color: 16

Bin 200: 19836 of cap free
Amount of items: 2
Items: 
Size: 550344 Color: 7
Size: 429821 Color: 1

Bin 201: 20972 of cap free
Amount of items: 2
Items: 
Size: 574352 Color: 18
Size: 404677 Color: 19

Bin 202: 21280 of cap free
Amount of items: 2
Items: 
Size: 548917 Color: 14
Size: 429804 Color: 4

Bin 203: 21880 of cap free
Amount of items: 2
Items: 
Size: 786025 Color: 13
Size: 192096 Color: 0

Bin 204: 21914 of cap free
Amount of items: 3
Items: 
Size: 622437 Color: 15
Size: 177882 Color: 3
Size: 177768 Color: 3

Bin 205: 21981 of cap free
Amount of items: 2
Items: 
Size: 698546 Color: 6
Size: 279474 Color: 11

Bin 206: 25635 of cap free
Amount of items: 2
Items: 
Size: 574407 Color: 5
Size: 399959 Color: 8

Bin 207: 26643 of cap free
Amount of items: 2
Items: 
Size: 489270 Color: 16
Size: 484088 Color: 5

Bin 208: 27468 of cap free
Amount of items: 2
Items: 
Size: 572013 Color: 8
Size: 400520 Color: 4

Bin 209: 28056 of cap free
Amount of items: 2
Items: 
Size: 493635 Color: 2
Size: 478310 Color: 3

Bin 210: 29321 of cap free
Amount of items: 2
Items: 
Size: 488452 Color: 12
Size: 482228 Color: 15

Bin 211: 29439 of cap free
Amount of items: 2
Items: 
Size: 570636 Color: 1
Size: 399926 Color: 13

Bin 212: 32796 of cap free
Amount of items: 3
Items: 
Size: 620670 Color: 19
Size: 176981 Color: 7
Size: 169554 Color: 7

Bin 213: 32938 of cap free
Amount of items: 3
Items: 
Size: 477719 Color: 7
Size: 254632 Color: 15
Size: 234712 Color: 8

Bin 214: 33827 of cap free
Amount of items: 2
Items: 
Size: 786686 Color: 10
Size: 179488 Color: 4

Bin 215: 35360 of cap free
Amount of items: 2
Items: 
Size: 693325 Color: 8
Size: 271316 Color: 9

Bin 216: 37709 of cap free
Amount of items: 2
Items: 
Size: 485045 Color: 9
Size: 477247 Color: 8

Bin 217: 39293 of cap free
Amount of items: 2
Items: 
Size: 692146 Color: 11
Size: 268562 Color: 3

Bin 218: 40065 of cap free
Amount of items: 2
Items: 
Size: 691570 Color: 6
Size: 268366 Color: 18

Bin 219: 41788 of cap free
Amount of items: 2
Items: 
Size: 690922 Color: 4
Size: 267291 Color: 9

Bin 220: 41856 of cap free
Amount of items: 2
Items: 
Size: 773123 Color: 4
Size: 185022 Color: 8

Bin 221: 42342 of cap free
Amount of items: 2
Items: 
Size: 690711 Color: 10
Size: 266948 Color: 18

Bin 222: 43290 of cap free
Amount of items: 2
Items: 
Size: 690656 Color: 2
Size: 266055 Color: 13

Bin 223: 44678 of cap free
Amount of items: 2
Items: 
Size: 778045 Color: 14
Size: 177278 Color: 9

Bin 224: 47020 of cap free
Amount of items: 2
Items: 
Size: 690386 Color: 18
Size: 262595 Color: 16

Bin 225: 49429 of cap free
Amount of items: 2
Items: 
Size: 685903 Color: 7
Size: 264669 Color: 18

Bin 226: 50009 of cap free
Amount of items: 2
Items: 
Size: 552169 Color: 1
Size: 397823 Color: 12

Bin 227: 50653 of cap free
Amount of items: 3
Items: 
Size: 620563 Color: 4
Size: 166666 Color: 10
Size: 162119 Color: 14

Bin 228: 60393 of cap free
Amount of items: 2
Items: 
Size: 542123 Color: 16
Size: 397485 Color: 4

Bin 229: 60724 of cap free
Amount of items: 2
Items: 
Size: 681733 Color: 17
Size: 257544 Color: 9

Bin 230: 64287 of cap free
Amount of items: 2
Items: 
Size: 541777 Color: 9
Size: 393937 Color: 7

Bin 231: 85783 of cap free
Amount of items: 2
Items: 
Size: 533949 Color: 9
Size: 380269 Color: 11

Bin 232: 119382 of cap free
Amount of items: 2
Items: 
Size: 685678 Color: 3
Size: 194941 Color: 1

Bin 233: 281517 of cap free
Amount of items: 2
Items: 
Size: 483561 Color: 9
Size: 234923 Color: 11

Bin 234: 521950 of cap free
Amount of items: 1
Items: 
Size: 478051 Color: 12

Total size: 231187939
Total free space: 2812295

