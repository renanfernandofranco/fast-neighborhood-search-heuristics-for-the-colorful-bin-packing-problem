Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 347 Color: 0
Size: 262 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 255 Color: 0
Size: 252 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 278 Color: 0
Size: 263 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 293 Color: 1
Size: 256 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 332 Color: 1
Size: 254 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 255 Color: 0
Size: 250 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 357 Color: 1
Size: 268 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 0
Size: 254 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 298 Color: 1
Size: 297 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 265 Color: 0
Size: 257 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 367 Color: 0
Size: 265 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 316 Color: 1
Size: 258 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 0
Size: 275 Color: 1
Size: 371 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 281 Color: 0
Size: 263 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 281 Color: 1
Size: 258 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 273 Color: 1
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 0
Size: 277 Color: 0
Size: 261 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 293 Color: 0
Size: 278 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 361 Color: 1
Size: 270 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 345 Color: 1
Size: 277 Color: 0

Total size: 20000
Total free space: 0

