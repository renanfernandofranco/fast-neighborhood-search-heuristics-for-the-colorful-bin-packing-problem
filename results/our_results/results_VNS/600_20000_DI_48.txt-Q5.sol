Capicity Bin: 15808
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7912 Color: 3
Size: 2096 Color: 2
Size: 1822 Color: 0
Size: 1392 Color: 1
Size: 1154 Color: 0
Size: 1032 Color: 4
Size: 400 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 1
Size: 6348 Color: 0
Size: 364 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10096 Color: 4
Size: 5456 Color: 0
Size: 256 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 1
Size: 4916 Color: 3
Size: 368 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10652 Color: 0
Size: 4300 Color: 1
Size: 856 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 0
Size: 4306 Color: 1
Size: 774 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10780 Color: 4
Size: 3202 Color: 0
Size: 1826 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 1
Size: 4296 Color: 2
Size: 352 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 2
Size: 4294 Color: 3
Size: 252 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11836 Color: 4
Size: 3684 Color: 2
Size: 288 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 4
Size: 3528 Color: 1
Size: 432 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11920 Color: 0
Size: 3248 Color: 2
Size: 640 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12157 Color: 0
Size: 3145 Color: 2
Size: 506 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12204 Color: 4
Size: 3304 Color: 2
Size: 300 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12268 Color: 3
Size: 3084 Color: 2
Size: 456 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12427 Color: 2
Size: 2301 Color: 1
Size: 1080 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 0
Size: 3128 Color: 2
Size: 216 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12516 Color: 4
Size: 3004 Color: 2
Size: 288 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 2
Size: 1744 Color: 0
Size: 1500 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 0
Size: 2792 Color: 3
Size: 424 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12605 Color: 3
Size: 3043 Color: 2
Size: 160 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12725 Color: 2
Size: 2531 Color: 4
Size: 552 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12788 Color: 2
Size: 2708 Color: 4
Size: 312 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12806 Color: 2
Size: 2662 Color: 3
Size: 340 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12814 Color: 2
Size: 2248 Color: 0
Size: 746 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12875 Color: 2
Size: 2445 Color: 4
Size: 488 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 0
Size: 1796 Color: 4
Size: 1024 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 4
Size: 2424 Color: 2
Size: 384 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13070 Color: 2
Size: 2250 Color: 4
Size: 488 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13076 Color: 2
Size: 2456 Color: 0
Size: 276 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13110 Color: 3
Size: 1714 Color: 0
Size: 984 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13136 Color: 1
Size: 2284 Color: 2
Size: 388 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 2
Size: 2192 Color: 3
Size: 416 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 1
Size: 2128 Color: 2
Size: 488 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13264 Color: 3
Size: 1888 Color: 2
Size: 656 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 4
Size: 1462 Color: 3
Size: 1056 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13420 Color: 2
Size: 1410 Color: 1
Size: 978 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13328 Color: 4
Size: 1312 Color: 0
Size: 1168 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 2
Size: 1404 Color: 3
Size: 956 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 2
Size: 1780 Color: 4
Size: 528 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 2
Size: 1912 Color: 3
Size: 368 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 2
Size: 1692 Color: 3
Size: 544 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13600 Color: 0
Size: 1768 Color: 0
Size: 440 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 0
Size: 1384 Color: 0
Size: 800 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 2
Size: 1316 Color: 0
Size: 832 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 0
Size: 1424 Color: 1
Size: 668 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13746 Color: 4
Size: 1316 Color: 0
Size: 746 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 2
Size: 1312 Color: 0
Size: 752 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13776 Color: 2
Size: 1712 Color: 3
Size: 320 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13810 Color: 0
Size: 1010 Color: 2
Size: 988 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 2
Size: 1628 Color: 0
Size: 316 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 0
Size: 1848 Color: 3
Size: 142 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 4
Size: 1582 Color: 2
Size: 320 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 2
Size: 1632 Color: 0
Size: 268 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13956 Color: 1
Size: 1508 Color: 4
Size: 344 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13998 Color: 1
Size: 1316 Color: 4
Size: 494 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 1
Size: 1460 Color: 0
Size: 344 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 2
Size: 1394 Color: 3
Size: 352 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 3
Size: 1336 Color: 2
Size: 368 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 3
Size: 1362 Color: 4
Size: 328 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14128 Color: 0
Size: 1352 Color: 2
Size: 328 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14138 Color: 0
Size: 1414 Color: 4
Size: 256 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 4
Size: 860 Color: 3
Size: 752 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 4
Size: 1166 Color: 3
Size: 442 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 1
Size: 1104 Color: 0
Size: 526 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 1
Size: 1296 Color: 4
Size: 296 Color: 3

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 8809 Color: 0
Size: 6582 Color: 4
Size: 416 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 9935 Color: 2
Size: 4248 Color: 1
Size: 1624 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11671 Color: 1
Size: 3880 Color: 2
Size: 256 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12093 Color: 1
Size: 2498 Color: 2
Size: 1216 Color: 3

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12403 Color: 2
Size: 3220 Color: 4
Size: 184 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12628 Color: 4
Size: 2571 Color: 2
Size: 608 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12771 Color: 0
Size: 2772 Color: 2
Size: 264 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13153 Color: 4
Size: 2286 Color: 2
Size: 368 Color: 4

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13228 Color: 4
Size: 2189 Color: 2
Size: 390 Color: 4

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 8916 Color: 1
Size: 6586 Color: 2
Size: 304 Color: 4

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 8982 Color: 0
Size: 6568 Color: 1
Size: 256 Color: 4

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 9580 Color: 2
Size: 5690 Color: 2
Size: 536 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 4
Size: 4942 Color: 3
Size: 280 Color: 1

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 10642 Color: 0
Size: 4792 Color: 4
Size: 372 Color: 4

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11184 Color: 3
Size: 4294 Color: 1
Size: 328 Color: 4

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 11323 Color: 1
Size: 3739 Color: 4
Size: 744 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 2
Size: 3782 Color: 4
Size: 496 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11786 Color: 0
Size: 2884 Color: 0
Size: 1136 Color: 2

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12486 Color: 1
Size: 2648 Color: 3
Size: 672 Color: 2

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12529 Color: 4
Size: 2893 Color: 2
Size: 384 Color: 1

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13164 Color: 4
Size: 2458 Color: 0
Size: 184 Color: 3

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13502 Color: 2
Size: 1312 Color: 4
Size: 992 Color: 0

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 0
Size: 2344 Color: 1

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 1
Size: 1452 Color: 0
Size: 736 Color: 2

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 3
Size: 1722 Color: 4
Size: 384 Color: 2

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 13972 Color: 1
Size: 1834 Color: 4

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 8897 Color: 0
Size: 6396 Color: 2
Size: 512 Color: 2

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 10588 Color: 2
Size: 4781 Color: 0
Size: 436 Color: 3

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 11025 Color: 4
Size: 4360 Color: 0
Size: 420 Color: 1

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 9836 Color: 2
Size: 5436 Color: 4
Size: 532 Color: 0

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 10472 Color: 3
Size: 4836 Color: 4
Size: 496 Color: 0

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 10634 Color: 0
Size: 4322 Color: 3
Size: 848 Color: 1

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 10658 Color: 2
Size: 4314 Color: 3
Size: 832 Color: 1

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 10864 Color: 4
Size: 2982 Color: 0
Size: 1958 Color: 2

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 11268 Color: 1
Size: 3848 Color: 2
Size: 688 Color: 4

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 11388 Color: 2
Size: 4144 Color: 4
Size: 272 Color: 4

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 12116 Color: 4
Size: 3408 Color: 2
Size: 280 Color: 0

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 0
Size: 1924 Color: 4
Size: 64 Color: 4

Bin 105: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 3
Size: 1976 Color: 0

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 14152 Color: 2
Size: 1652 Color: 0

Bin 107: 5 of cap free
Amount of items: 3
Items: 
Size: 10544 Color: 0
Size: 4895 Color: 3
Size: 364 Color: 1

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 12072 Color: 4
Size: 3731 Color: 1

Bin 109: 6 of cap free
Amount of items: 10
Items: 
Size: 7906 Color: 0
Size: 1092 Color: 2
Size: 992 Color: 1
Size: 912 Color: 1
Size: 872 Color: 3
Size: 864 Color: 4
Size: 864 Color: 2
Size: 860 Color: 1
Size: 768 Color: 2
Size: 672 Color: 4

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 11339 Color: 3
Size: 3263 Color: 1
Size: 1200 Color: 2

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 11534 Color: 3
Size: 3788 Color: 4
Size: 480 Color: 2

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 13049 Color: 1
Size: 2753 Color: 3

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 13066 Color: 4
Size: 2736 Color: 1

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 3
Size: 1922 Color: 0
Size: 128 Color: 0

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 13762 Color: 0
Size: 1748 Color: 2
Size: 292 Color: 1

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 9743 Color: 2
Size: 3354 Color: 4
Size: 2704 Color: 0

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 12035 Color: 0
Size: 2256 Color: 2
Size: 1510 Color: 0

Bin 118: 8 of cap free
Amount of items: 9
Items: 
Size: 7908 Color: 4
Size: 1432 Color: 0
Size: 1312 Color: 2
Size: 1232 Color: 0
Size: 1144 Color: 1
Size: 1136 Color: 0
Size: 960 Color: 4
Size: 340 Color: 1
Size: 336 Color: 4

Bin 119: 9 of cap free
Amount of items: 5
Items: 
Size: 7914 Color: 0
Size: 2762 Color: 3
Size: 2432 Color: 0
Size: 2347 Color: 1
Size: 344 Color: 2

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 9249 Color: 3
Size: 5686 Color: 2
Size: 864 Color: 1

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 13128 Color: 0
Size: 2671 Color: 4

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 10565 Color: 2
Size: 5232 Color: 4

Bin 123: 11 of cap free
Amount of items: 3
Items: 
Size: 11274 Color: 2
Size: 4371 Color: 3
Size: 152 Color: 3

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 12348 Color: 4
Size: 3449 Color: 1

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 12494 Color: 0
Size: 3303 Color: 4

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 1
Size: 6184 Color: 3
Size: 320 Color: 0

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 11208 Color: 2
Size: 4588 Color: 4

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 12234 Color: 4
Size: 3562 Color: 3

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13592 Color: 3
Size: 2204 Color: 4

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 0
Size: 1728 Color: 3

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 14124 Color: 3
Size: 1672 Color: 0

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 14160 Color: 0
Size: 1636 Color: 1

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 11331 Color: 2
Size: 4464 Color: 0

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 13292 Color: 0
Size: 2502 Color: 1

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 1
Size: 2106 Color: 3

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 12993 Color: 0
Size: 2800 Color: 1

Bin 137: 16 of cap free
Amount of items: 3
Items: 
Size: 7944 Color: 2
Size: 7304 Color: 1
Size: 544 Color: 4

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 10944 Color: 2
Size: 4848 Color: 1

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 12528 Color: 4
Size: 3264 Color: 1

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 12692 Color: 4
Size: 3100 Color: 3

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 10324 Color: 4
Size: 5467 Color: 1

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 10719 Color: 4
Size: 5072 Color: 2

Bin 143: 19 of cap free
Amount of items: 3
Items: 
Size: 8924 Color: 1
Size: 6417 Color: 2
Size: 448 Color: 0

Bin 144: 19 of cap free
Amount of items: 3
Items: 
Size: 9886 Color: 4
Size: 4241 Color: 1
Size: 1662 Color: 0

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 12472 Color: 4
Size: 3316 Color: 0

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 3
Size: 2916 Color: 0

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 1
Size: 2008 Color: 3

Bin 148: 22 of cap free
Amount of items: 3
Items: 
Size: 7920 Color: 3
Size: 6160 Color: 4
Size: 1706 Color: 0

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 13282 Color: 3
Size: 2504 Color: 0

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 14132 Color: 0
Size: 1652 Color: 4

Bin 151: 26 of cap free
Amount of items: 31
Items: 
Size: 660 Color: 1
Size: 656 Color: 0
Size: 636 Color: 1
Size: 628 Color: 1
Size: 608 Color: 0
Size: 600 Color: 3
Size: 592 Color: 1
Size: 576 Color: 4
Size: 576 Color: 3
Size: 576 Color: 2
Size: 544 Color: 4
Size: 544 Color: 2
Size: 544 Color: 2
Size: 544 Color: 0
Size: 544 Color: 0
Size: 512 Color: 2
Size: 512 Color: 1
Size: 500 Color: 2
Size: 496 Color: 4
Size: 496 Color: 4
Size: 496 Color: 2
Size: 468 Color: 4
Size: 464 Color: 4
Size: 464 Color: 1
Size: 458 Color: 2
Size: 416 Color: 3
Size: 384 Color: 3
Size: 360 Color: 3
Size: 336 Color: 3
Size: 320 Color: 3
Size: 272 Color: 2

Bin 152: 26 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 4
Size: 6588 Color: 1
Size: 208 Color: 0

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 13626 Color: 1
Size: 2156 Color: 4

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 13914 Color: 4
Size: 1868 Color: 1

Bin 155: 28 of cap free
Amount of items: 3
Items: 
Size: 8873 Color: 2
Size: 6587 Color: 1
Size: 320 Color: 0

Bin 156: 28 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 4
Size: 2524 Color: 3

Bin 157: 29 of cap free
Amount of items: 12
Items: 
Size: 7905 Color: 0
Size: 860 Color: 0
Size: 856 Color: 1
Size: 848 Color: 1
Size: 832 Color: 0
Size: 768 Color: 4
Size: 712 Color: 2
Size: 696 Color: 2
Size: 688 Color: 4
Size: 618 Color: 4
Size: 612 Color: 3
Size: 384 Color: 2

Bin 158: 31 of cap free
Amount of items: 2
Items: 
Size: 12192 Color: 3
Size: 3585 Color: 1

Bin 159: 32 of cap free
Amount of items: 3
Items: 
Size: 10664 Color: 4
Size: 4984 Color: 3
Size: 128 Color: 3

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 10796 Color: 3
Size: 4980 Color: 1

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 14054 Color: 2
Size: 1722 Color: 4

Bin 162: 32 of cap free
Amount of items: 2
Items: 
Size: 14110 Color: 3
Size: 1666 Color: 0

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 11592 Color: 3
Size: 4180 Color: 0

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 3
Size: 2356 Color: 4

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 3
Size: 1586 Color: 2

Bin 166: 40 of cap free
Amount of items: 3
Items: 
Size: 8392 Color: 4
Size: 6580 Color: 3
Size: 796 Color: 3

Bin 167: 40 of cap free
Amount of items: 3
Items: 
Size: 9552 Color: 2
Size: 5608 Color: 3
Size: 608 Color: 0

Bin 168: 40 of cap free
Amount of items: 2
Items: 
Size: 12728 Color: 4
Size: 3040 Color: 3

Bin 169: 41 of cap free
Amount of items: 3
Items: 
Size: 10071 Color: 3
Size: 5344 Color: 2
Size: 352 Color: 0

Bin 170: 42 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 3
Size: 1832 Color: 0
Size: 192 Color: 2

Bin 171: 44 of cap free
Amount of items: 3
Items: 
Size: 11732 Color: 3
Size: 3856 Color: 4
Size: 176 Color: 2

Bin 172: 46 of cap free
Amount of items: 2
Items: 
Size: 12114 Color: 4
Size: 3648 Color: 1

Bin 173: 47 of cap free
Amount of items: 2
Items: 
Size: 12664 Color: 4
Size: 3097 Color: 0

Bin 174: 48 of cap free
Amount of items: 3
Items: 
Size: 8432 Color: 0
Size: 6576 Color: 2
Size: 752 Color: 1

Bin 175: 50 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 0
Size: 2950 Color: 4

Bin 176: 57 of cap free
Amount of items: 2
Items: 
Size: 13183 Color: 3
Size: 2568 Color: 4

Bin 177: 58 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 3
Size: 1996 Color: 4

Bin 178: 60 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 2
Size: 4938 Color: 2
Size: 1458 Color: 1

Bin 179: 63 of cap free
Amount of items: 4
Items: 
Size: 8776 Color: 2
Size: 6585 Color: 3
Size: 192 Color: 1
Size: 192 Color: 0

Bin 180: 66 of cap free
Amount of items: 2
Items: 
Size: 9878 Color: 2
Size: 5864 Color: 3

Bin 181: 74 of cap free
Amount of items: 3
Items: 
Size: 8008 Color: 0
Size: 5624 Color: 2
Size: 2102 Color: 1

Bin 182: 76 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 1
Size: 3404 Color: 3

Bin 183: 81 of cap free
Amount of items: 2
Items: 
Size: 11155 Color: 3
Size: 4572 Color: 2

Bin 184: 83 of cap free
Amount of items: 2
Items: 
Size: 11845 Color: 3
Size: 3880 Color: 0

Bin 185: 93 of cap free
Amount of items: 2
Items: 
Size: 11728 Color: 1
Size: 3987 Color: 3

Bin 186: 96 of cap free
Amount of items: 2
Items: 
Size: 11833 Color: 4
Size: 3879 Color: 0

Bin 187: 98 of cap free
Amount of items: 2
Items: 
Size: 13610 Color: 1
Size: 2100 Color: 4

Bin 188: 108 of cap free
Amount of items: 2
Items: 
Size: 9976 Color: 1
Size: 5724 Color: 2

Bin 189: 108 of cap free
Amount of items: 2
Items: 
Size: 12618 Color: 1
Size: 3082 Color: 3

Bin 190: 112 of cap free
Amount of items: 2
Items: 
Size: 9296 Color: 4
Size: 6400 Color: 0

Bin 191: 113 of cap free
Amount of items: 2
Items: 
Size: 11970 Color: 4
Size: 3725 Color: 0

Bin 192: 116 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 3
Size: 4872 Color: 3
Size: 2904 Color: 0

Bin 193: 127 of cap free
Amount of items: 2
Items: 
Size: 10626 Color: 1
Size: 5055 Color: 4

Bin 194: 132 of cap free
Amount of items: 2
Items: 
Size: 10480 Color: 3
Size: 5196 Color: 2

Bin 195: 139 of cap free
Amount of items: 2
Items: 
Size: 9888 Color: 3
Size: 5781 Color: 0

Bin 196: 143 of cap free
Amount of items: 2
Items: 
Size: 9832 Color: 3
Size: 5833 Color: 1

Bin 197: 172 of cap free
Amount of items: 2
Items: 
Size: 8940 Color: 1
Size: 6696 Color: 2

Bin 198: 198 of cap free
Amount of items: 5
Items: 
Size: 7907 Color: 1
Size: 2248 Color: 0
Size: 2213 Color: 0
Size: 1720 Color: 4
Size: 1522 Color: 0

Bin 199: 11896 of cap free
Amount of items: 11
Items: 
Size: 448 Color: 4
Size: 416 Color: 4
Size: 416 Color: 1
Size: 392 Color: 4
Size: 384 Color: 2
Size: 340 Color: 1
Size: 332 Color: 1
Size: 320 Color: 3
Size: 320 Color: 0
Size: 272 Color: 3
Size: 272 Color: 3

Total size: 3129984
Total free space: 15808

