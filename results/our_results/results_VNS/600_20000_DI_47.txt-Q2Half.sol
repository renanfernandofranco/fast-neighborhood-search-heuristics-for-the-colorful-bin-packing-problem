Capicity Bin: 15200
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 1
Size: 1522 Color: 1
Size: 304 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 2256 Color: 1
Size: 760 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 1
Size: 1566 Color: 1
Size: 312 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 1
Size: 2580 Color: 1
Size: 500 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 1
Size: 1624 Color: 1
Size: 368 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 1
Size: 3594 Color: 1
Size: 716 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 1
Size: 4916 Color: 1
Size: 976 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 10044 Color: 1
Size: 4228 Color: 1
Size: 512 Color: 0
Size: 416 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 1
Size: 1494 Color: 1
Size: 96 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 1
Size: 1811 Color: 1
Size: 360 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8892 Color: 1
Size: 5260 Color: 1
Size: 1048 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 1
Size: 2264 Color: 1
Size: 288 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 1
Size: 2092 Color: 1
Size: 736 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9290 Color: 1
Size: 4926 Color: 1
Size: 984 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 1
Size: 1719 Color: 1
Size: 342 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 1
Size: 2226 Color: 1
Size: 720 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11050 Color: 1
Size: 3506 Color: 1
Size: 644 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 1
Size: 1324 Color: 1
Size: 280 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7602 Color: 1
Size: 6334 Color: 1
Size: 1264 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 1
Size: 3482 Color: 1
Size: 692 Color: 0

Bin 21: 0 of cap free
Amount of items: 6
Items: 
Size: 7536 Color: 1
Size: 3854 Color: 1
Size: 2266 Color: 1
Size: 816 Color: 0
Size: 416 Color: 0
Size: 312 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 10716 Color: 1
Size: 3820 Color: 1
Size: 664 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 1
Size: 1406 Color: 1
Size: 280 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8522 Color: 1
Size: 6164 Color: 1
Size: 514 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 1
Size: 1448 Color: 1
Size: 290 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 1
Size: 4082 Color: 1
Size: 716 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7608 Color: 1
Size: 7208 Color: 1
Size: 384 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11656 Color: 1
Size: 2844 Color: 1
Size: 700 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13557 Color: 1
Size: 1371 Color: 1
Size: 272 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 1
Size: 1820 Color: 1
Size: 144 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 1428 Color: 1
Size: 112 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12998 Color: 1
Size: 1838 Color: 1
Size: 364 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 10994 Color: 1
Size: 3514 Color: 1
Size: 692 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12911 Color: 1
Size: 1569 Color: 1
Size: 720 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 1
Size: 1644 Color: 1
Size: 168 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 1
Size: 4386 Color: 1
Size: 876 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13091 Color: 1
Size: 1461 Color: 1
Size: 648 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12890 Color: 1
Size: 2290 Color: 1
Size: 20 Color: 0

Bin 39: 0 of cap free
Amount of items: 5
Items: 
Size: 7804 Color: 1
Size: 4741 Color: 1
Size: 1759 Color: 1
Size: 672 Color: 0
Size: 224 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 1
Size: 1681 Color: 1
Size: 376 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 1
Size: 2213 Color: 1
Size: 442 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 1
Size: 1880 Color: 1
Size: 368 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13418 Color: 1
Size: 1434 Color: 1
Size: 348 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 1
Size: 1940 Color: 1
Size: 336 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 1
Size: 4024 Color: 1
Size: 800 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11813 Color: 1
Size: 2823 Color: 1
Size: 564 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12426 Color: 1
Size: 2206 Color: 1
Size: 568 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 1
Size: 4424 Color: 1
Size: 560 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 1
Size: 1684 Color: 1
Size: 496 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 1
Size: 2901 Color: 1
Size: 502 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 1
Size: 1388 Color: 1
Size: 360 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 1
Size: 2008 Color: 1
Size: 400 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 1
Size: 2364 Color: 1
Size: 264 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 1
Size: 1900 Color: 1
Size: 380 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 11496 Color: 1
Size: 3416 Color: 1
Size: 288 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 7607 Color: 1
Size: 6329 Color: 1
Size: 1264 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 7601 Color: 1
Size: 6523 Color: 1
Size: 1076 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 9529 Color: 1
Size: 4727 Color: 1
Size: 944 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 9068 Color: 1
Size: 5116 Color: 1
Size: 1016 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 1
Size: 2196 Color: 1
Size: 576 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 10873 Color: 1
Size: 3231 Color: 1
Size: 1096 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 11047 Color: 1
Size: 3461 Color: 1
Size: 692 Color: 0

Bin 63: 0 of cap free
Amount of items: 5
Items: 
Size: 5664 Color: 1
Size: 4784 Color: 1
Size: 3684 Color: 1
Size: 812 Color: 0
Size: 256 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 1
Size: 3974 Color: 1
Size: 912 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 10410 Color: 1
Size: 3994 Color: 1
Size: 796 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 1
Size: 2002 Color: 1
Size: 400 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 10418 Color: 1
Size: 3986 Color: 1
Size: 796 Color: 0

Bin 68: 0 of cap free
Amount of items: 5
Items: 
Size: 11742 Color: 1
Size: 1678 Color: 1
Size: 1112 Color: 0
Size: 460 Color: 0
Size: 208 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 1
Size: 4924 Color: 1
Size: 984 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8526 Color: 1
Size: 6332 Color: 1
Size: 342 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 12692 Color: 1
Size: 1532 Color: 1
Size: 976 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 1
Size: 1800 Color: 1
Size: 352 Color: 0

Bin 73: 0 of cap free
Amount of items: 5
Items: 
Size: 7604 Color: 1
Size: 4002 Color: 1
Size: 2882 Color: 1
Size: 452 Color: 0
Size: 260 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 7603 Color: 1
Size: 6331 Color: 1
Size: 1266 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 8532 Color: 1
Size: 5564 Color: 1
Size: 1104 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 10904 Color: 1
Size: 3592 Color: 1
Size: 704 Color: 0

Bin 77: 0 of cap free
Amount of items: 5
Items: 
Size: 10306 Color: 1
Size: 2520 Color: 1
Size: 1460 Color: 1
Size: 576 Color: 0
Size: 338 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 12734 Color: 1
Size: 2058 Color: 1
Size: 408 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 8232 Color: 1
Size: 6672 Color: 1
Size: 296 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 9294 Color: 1
Size: 4922 Color: 1
Size: 984 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 9640 Color: 1
Size: 5228 Color: 1
Size: 332 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 1
Size: 4394 Color: 1
Size: 876 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 1
Size: 1486 Color: 1
Size: 620 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 7640 Color: 1
Size: 6860 Color: 1
Size: 700 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 1
Size: 5557 Color: 1
Size: 1110 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 1
Size: 5487 Color: 1
Size: 1112 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 8824 Color: 1
Size: 5320 Color: 1
Size: 1056 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 10034 Color: 1
Size: 4306 Color: 1
Size: 860 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 9472 Color: 1
Size: 5340 Color: 1
Size: 388 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 9948 Color: 1
Size: 4380 Color: 1
Size: 872 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 10026 Color: 1
Size: 4314 Color: 1
Size: 860 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 10042 Color: 1
Size: 4550 Color: 1
Size: 608 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 10108 Color: 1
Size: 4300 Color: 1
Size: 792 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 10667 Color: 1
Size: 3885 Color: 1
Size: 648 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 1
Size: 3276 Color: 1
Size: 946 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 10986 Color: 1
Size: 4074 Color: 1
Size: 140 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 11044 Color: 1
Size: 3468 Color: 1
Size: 688 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 11010 Color: 1
Size: 3236 Color: 1
Size: 954 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 1
Size: 3466 Color: 1
Size: 700 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 11098 Color: 1
Size: 3422 Color: 1
Size: 680 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 1
Size: 2980 Color: 1
Size: 944 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 1
Size: 3356 Color: 1
Size: 520 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 1
Size: 3144 Color: 1
Size: 624 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 11471 Color: 1
Size: 2917 Color: 1
Size: 812 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 11701 Color: 1
Size: 2643 Color: 1
Size: 856 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 11702 Color: 1
Size: 2918 Color: 1
Size: 580 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 11974 Color: 1
Size: 2694 Color: 1
Size: 532 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 1
Size: 2680 Color: 1
Size: 528 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 1
Size: 2652 Color: 1
Size: 440 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 12109 Color: 1
Size: 2643 Color: 1
Size: 448 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 12160 Color: 1
Size: 2458 Color: 1
Size: 582 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 12170 Color: 1
Size: 2526 Color: 1
Size: 504 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 12193 Color: 1
Size: 1909 Color: 1
Size: 1098 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 1
Size: 2440 Color: 1
Size: 480 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 12284 Color: 1
Size: 2436 Color: 1
Size: 480 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 1
Size: 2338 Color: 1
Size: 464 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 1
Size: 2313 Color: 1
Size: 462 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 1
Size: 2126 Color: 1
Size: 592 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 12528 Color: 1
Size: 2352 Color: 1
Size: 320 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 12554 Color: 1
Size: 1954 Color: 1
Size: 692 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 12605 Color: 1
Size: 2163 Color: 1
Size: 432 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 12650 Color: 1
Size: 1598 Color: 1
Size: 952 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 1
Size: 2132 Color: 1
Size: 296 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 12828 Color: 1
Size: 1980 Color: 1
Size: 392 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 12837 Color: 1
Size: 1971 Color: 1
Size: 392 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 1
Size: 2314 Color: 1
Size: 28 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 1
Size: 1846 Color: 1
Size: 368 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 1
Size: 1912 Color: 1
Size: 204 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 1
Size: 1758 Color: 1
Size: 566 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 1
Size: 1672 Color: 1
Size: 272 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 13270 Color: 1
Size: 1610 Color: 1
Size: 320 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 1
Size: 1764 Color: 1
Size: 72 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 13425 Color: 1
Size: 1481 Color: 1
Size: 294 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 13445 Color: 1
Size: 1497 Color: 1
Size: 258 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 7606 Color: 1
Size: 6330 Color: 1
Size: 1264 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 1464 Color: 1
Size: 272 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 13469 Color: 1
Size: 1443 Color: 1
Size: 288 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 13482 Color: 1
Size: 1368 Color: 1
Size: 350 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 1
Size: 1284 Color: 1
Size: 424 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 1
Size: 1340 Color: 1
Size: 320 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 1
Size: 1216 Color: 1
Size: 424 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 1
Size: 1342 Color: 1
Size: 264 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 13620 Color: 1
Size: 1516 Color: 1
Size: 64 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 13629 Color: 1
Size: 1311 Color: 1
Size: 260 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 13319 Color: 1
Size: 1302 Color: 1
Size: 578 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 1
Size: 1572 Color: 1
Size: 464 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 10882 Color: 1
Size: 4059 Color: 1
Size: 258 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 1
Size: 2507 Color: 1
Size: 1064 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 12070 Color: 1
Size: 2521 Color: 1
Size: 608 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 11793 Color: 1
Size: 3054 Color: 1
Size: 352 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 1
Size: 2837 Color: 1
Size: 832 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 9377 Color: 1
Size: 5566 Color: 1
Size: 256 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 10780 Color: 1
Size: 4163 Color: 1
Size: 256 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 11325 Color: 1
Size: 3474 Color: 1
Size: 400 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 1
Size: 2610 Color: 1
Size: 96 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 1
Size: 1699 Color: 1
Size: 312 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 1
Size: 4030 Color: 1
Size: 328 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 1
Size: 3598 Color: 1
Size: 488 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 1
Size: 2968 Color: 1
Size: 434 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 9465 Color: 1
Size: 4853 Color: 1
Size: 880 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 12521 Color: 1
Size: 2181 Color: 1
Size: 496 Color: 0

Bin 162: 3 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 1
Size: 3607 Color: 1
Size: 970 Color: 0

Bin 163: 3 of cap free
Amount of items: 3
Items: 
Size: 9445 Color: 1
Size: 4904 Color: 1
Size: 848 Color: 0

Bin 164: 3 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 1
Size: 2316 Color: 1
Size: 296 Color: 0

Bin 165: 3 of cap free
Amount of items: 3
Items: 
Size: 12177 Color: 1
Size: 2492 Color: 1
Size: 528 Color: 0

Bin 166: 4 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 1
Size: 2568 Color: 1
Size: 608 Color: 0

Bin 167: 4 of cap free
Amount of items: 3
Items: 
Size: 11042 Color: 1
Size: 3522 Color: 1
Size: 632 Color: 0

Bin 168: 4 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 3096 Color: 1
Size: 320 Color: 0

Bin 169: 4 of cap free
Amount of items: 5
Items: 
Size: 8796 Color: 1
Size: 4168 Color: 1
Size: 1560 Color: 1
Size: 512 Color: 0
Size: 160 Color: 0

Bin 170: 4 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 1
Size: 2136 Color: 1
Size: 416 Color: 0

Bin 171: 5 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 1
Size: 5727 Color: 1
Size: 304 Color: 0

Bin 172: 7 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 1
Size: 1291 Color: 1
Size: 566 Color: 0

Bin 173: 7 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 1
Size: 1715 Color: 1
Size: 288 Color: 0

Bin 174: 11 of cap free
Amount of items: 5
Items: 
Size: 5036 Color: 1
Size: 3602 Color: 1
Size: 2864 Color: 0
Size: 2535 Color: 1
Size: 1152 Color: 0

Bin 175: 12 of cap free
Amount of items: 3
Items: 
Size: 7612 Color: 1
Size: 6312 Color: 1
Size: 1264 Color: 0

Bin 176: 14 of cap free
Amount of items: 3
Items: 
Size: 11538 Color: 1
Size: 2852 Color: 1
Size: 796 Color: 0

Bin 177: 14 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 1
Size: 5562 Color: 1
Size: 304 Color: 0

Bin 178: 17 of cap free
Amount of items: 3
Items: 
Size: 11434 Color: 1
Size: 3109 Color: 1
Size: 640 Color: 0

Bin 179: 19 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 1
Size: 1888 Color: 1
Size: 728 Color: 0

Bin 180: 23 of cap free
Amount of items: 3
Items: 
Size: 12445 Color: 1
Size: 2028 Color: 1
Size: 704 Color: 0

Bin 181: 25 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 1
Size: 4285 Color: 1
Size: 456 Color: 0

Bin 182: 26 of cap free
Amount of items: 3
Items: 
Size: 9513 Color: 1
Size: 5501 Color: 1
Size: 160 Color: 0

Bin 183: 27 of cap free
Amount of items: 3
Items: 
Size: 12212 Color: 1
Size: 2577 Color: 1
Size: 384 Color: 0

Bin 184: 29 of cap free
Amount of items: 3
Items: 
Size: 10283 Color: 1
Size: 3640 Color: 1
Size: 1248 Color: 0

Bin 185: 29 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 1
Size: 4781 Color: 1
Size: 494 Color: 0

Bin 186: 63 of cap free
Amount of items: 3
Items: 
Size: 10205 Color: 1
Size: 4648 Color: 1
Size: 284 Color: 0

Bin 187: 84 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 1
Size: 2563 Color: 1
Size: 832 Color: 0

Bin 188: 87 of cap free
Amount of items: 3
Items: 
Size: 10647 Color: 1
Size: 3466 Color: 1
Size: 1000 Color: 0

Bin 189: 162 of cap free
Amount of items: 3
Items: 
Size: 12530 Color: 1
Size: 2064 Color: 1
Size: 444 Color: 0

Bin 190: 219 of cap free
Amount of items: 3
Items: 
Size: 11180 Color: 1
Size: 2841 Color: 1
Size: 960 Color: 0

Bin 191: 335 of cap free
Amount of items: 3
Items: 
Size: 8617 Color: 1
Size: 5816 Color: 1
Size: 432 Color: 0

Bin 192: 704 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 1
Size: 4244 Color: 1
Size: 1232 Color: 0

Bin 193: 1549 of cap free
Amount of items: 1
Items: 
Size: 13651 Color: 1

Bin 194: 1562 of cap free
Amount of items: 1
Items: 
Size: 13638 Color: 1

Bin 195: 1751 of cap free
Amount of items: 1
Items: 
Size: 13449 Color: 1

Bin 196: 1752 of cap free
Amount of items: 1
Items: 
Size: 13448 Color: 1

Bin 197: 1884 of cap free
Amount of items: 1
Items: 
Size: 13316 Color: 1

Bin 198: 2017 of cap free
Amount of items: 1
Items: 
Size: 13183 Color: 1

Bin 199: 2712 of cap free
Amount of items: 1
Items: 
Size: 12488 Color: 1

Total size: 3009600
Total free space: 15200

