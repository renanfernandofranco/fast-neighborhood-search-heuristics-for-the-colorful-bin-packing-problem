Capicity Bin: 2020
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1186 Color: 150
Size: 786 Color: 130
Size: 48 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 165
Size: 453 Color: 105
Size: 156 Color: 62

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 169
Size: 405 Color: 101
Size: 134 Color: 56

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 173
Size: 403 Color: 100
Size: 80 Color: 41

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 175
Size: 374 Color: 98
Size: 72 Color: 34

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 176
Size: 365 Color: 97
Size: 72 Color: 37

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 181
Size: 329 Color: 91
Size: 64 Color: 29

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 182
Size: 286 Color: 84
Size: 100 Color: 47

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 183
Size: 297 Color: 87
Size: 64 Color: 31

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 184
Size: 267 Color: 80
Size: 92 Color: 45

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 186
Size: 214 Color: 73
Size: 136 Color: 57

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 190
Size: 301 Color: 88
Size: 20 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 191
Size: 263 Color: 79
Size: 52 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 196
Size: 202 Color: 71
Size: 60 Color: 27

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 197
Size: 164 Color: 63
Size: 90 Color: 44

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 198
Size: 168 Color: 68
Size: 80 Color: 42

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 200
Size: 166 Color: 67
Size: 68 Color: 33

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 201
Size: 166 Color: 66
Size: 60 Color: 28

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 1268 Color: 154
Size: 671 Color: 121
Size: 40 Color: 11
Size: 40 Color: 10

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1362 Color: 158
Size: 515 Color: 112
Size: 142 Color: 60

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 166
Size: 589 Color: 117
Size: 16 Color: 3

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1466 Color: 167
Size: 363 Color: 96
Size: 190 Color: 69

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1477 Color: 168
Size: 506 Color: 109
Size: 36 Color: 8

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1510 Color: 170
Size: 509 Color: 110

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 172
Size: 462 Color: 106
Size: 22 Color: 5

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 177
Size: 426 Color: 103
Size: 8 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 180
Size: 294 Color: 86
Size: 102 Color: 50

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 185
Size: 354 Color: 95

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1737 Color: 194
Size: 282 Color: 83

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 199
Size: 241 Color: 78

Bin 31: 2 of cap free
Amount of items: 13
Items: 
Size: 224 Color: 76
Size: 222 Color: 75
Size: 220 Color: 74
Size: 212 Color: 72
Size: 198 Color: 70
Size: 166 Color: 65
Size: 164 Color: 64
Size: 116 Color: 52
Size: 116 Color: 51
Size: 102 Color: 49
Size: 102 Color: 48
Size: 100 Color: 46
Size: 76 Color: 40

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 174
Size: 464 Color: 107
Size: 8 Color: 0

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 178
Size: 420 Color: 102

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1706 Color: 192
Size: 312 Color: 89

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1731 Color: 193
Size: 287 Color: 85

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1378 Color: 159
Size: 519 Color: 113
Size: 120 Color: 53

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1407 Color: 164
Size: 610 Color: 118

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 189
Size: 331 Color: 92

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 179
Size: 398 Color: 99

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 188
Size: 338 Color: 94

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1746 Color: 195
Size: 270 Color: 82

Bin 42: 5 of cap free
Amount of items: 5
Items: 
Size: 1161 Color: 147
Size: 666 Color: 120
Size: 84 Color: 43
Size: 52 Color: 21
Size: 52 Color: 20

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1317 Color: 157
Size: 698 Color: 124

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 187
Size: 337 Color: 93

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1399 Color: 161
Size: 614 Color: 119

Bin 46: 10 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 162
Size: 574 Color: 114
Size: 36 Color: 6

Bin 47: 12 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 160
Size: 586 Color: 115
Size: 36 Color: 7

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 171
Size: 491 Color: 108

Bin 49: 14 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 163
Size: 587 Color: 116
Size: 16 Color: 2

Bin 50: 17 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 155
Size: 673 Color: 122
Size: 40 Color: 9

Bin 51: 21 of cap free
Amount of items: 2
Items: 
Size: 1157 Color: 146
Size: 842 Color: 137

Bin 52: 21 of cap free
Amount of items: 2
Items: 
Size: 1315 Color: 156
Size: 684 Color: 123

Bin 53: 24 of cap free
Amount of items: 2
Items: 
Size: 1155 Color: 145
Size: 841 Color: 136

Bin 54: 28 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 149
Size: 775 Color: 129
Size: 48 Color: 16

Bin 55: 33 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 153
Size: 721 Color: 128
Size: 44 Color: 12

Bin 56: 36 of cap free
Amount of items: 6
Items: 
Size: 1011 Color: 138
Size: 322 Color: 90
Size: 269 Color: 81
Size: 230 Color: 77
Size: 76 Color: 39
Size: 76 Color: 38

Bin 57: 38 of cap free
Amount of items: 4
Items: 
Size: 1165 Color: 148
Size: 713 Color: 125
Size: 52 Color: 19
Size: 52 Color: 17

Bin 58: 38 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 152
Size: 721 Color: 127
Size: 44 Color: 13

Bin 59: 42 of cap free
Amount of items: 4
Items: 
Size: 1017 Color: 141
Size: 831 Color: 132
Size: 66 Color: 32
Size: 64 Color: 30

Bin 60: 45 of cap free
Amount of items: 3
Items: 
Size: 1013 Color: 139
Size: 511 Color: 111
Size: 451 Color: 104

Bin 61: 45 of cap free
Amount of items: 3
Items: 
Size: 1078 Color: 144
Size: 841 Color: 135
Size: 56 Color: 22

Bin 62: 46 of cap free
Amount of items: 4
Items: 
Size: 1014 Color: 140
Size: 816 Color: 131
Size: 72 Color: 36
Size: 72 Color: 35

Bin 63: 46 of cap free
Amount of items: 4
Items: 
Size: 1025 Color: 143
Size: 837 Color: 134
Size: 56 Color: 24
Size: 56 Color: 23

Bin 64: 46 of cap free
Amount of items: 3
Items: 
Size: 1213 Color: 151
Size: 717 Color: 126
Size: 44 Color: 14

Bin 65: 52 of cap free
Amount of items: 4
Items: 
Size: 1021 Color: 142
Size: 833 Color: 133
Size: 58 Color: 26
Size: 56 Color: 25

Bin 66: 1328 of cap free
Amount of items: 5
Items: 
Size: 144 Color: 61
Size: 142 Color: 59
Size: 142 Color: 58
Size: 132 Color: 55
Size: 132 Color: 54

Total size: 131300
Total free space: 2020

