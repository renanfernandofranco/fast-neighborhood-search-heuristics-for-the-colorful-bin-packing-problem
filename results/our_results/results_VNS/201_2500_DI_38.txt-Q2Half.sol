Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1678 Color: 1
Size: 662 Color: 1
Size: 72 Color: 0
Size: 60 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 1
Size: 443 Color: 1
Size: 88 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 1
Size: 538 Color: 1
Size: 72 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 230 Color: 1
Size: 124 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 290 Color: 1
Size: 154 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1636 Color: 1
Size: 700 Color: 1
Size: 96 Color: 0
Size: 40 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1817 Color: 1
Size: 547 Color: 1
Size: 108 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 1
Size: 840 Color: 1
Size: 108 Color: 0

Bin 9: 0 of cap free
Amount of items: 6
Items: 
Size: 1254 Color: 1
Size: 500 Color: 1
Size: 430 Color: 1
Size: 152 Color: 0
Size: 72 Color: 0
Size: 64 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 1
Size: 260 Color: 1
Size: 8 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 1
Size: 883 Color: 1
Size: 176 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 1031 Color: 1
Size: 204 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 1
Size: 372 Color: 1
Size: 120 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 576 Color: 1
Size: 162 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 308 Color: 1
Size: 96 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 1
Size: 284 Color: 1
Size: 48 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 302 Color: 1
Size: 272 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 1
Size: 556 Color: 1
Size: 94 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 1
Size: 842 Color: 1
Size: 78 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 1
Size: 412 Color: 1
Size: 184 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 878 Color: 1
Size: 132 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 1
Size: 620 Color: 1
Size: 148 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 1
Size: 1029 Color: 1
Size: 204 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 1
Size: 326 Color: 1
Size: 44 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 1
Size: 638 Color: 1
Size: 272 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1993 Color: 1
Size: 401 Color: 1
Size: 78 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 1
Size: 452 Color: 1
Size: 88 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 418 Color: 1
Size: 56 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 1
Size: 585 Color: 1
Size: 116 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 1
Size: 481 Color: 1
Size: 236 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1388 Color: 1
Size: 908 Color: 1
Size: 176 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 1
Size: 314 Color: 1
Size: 136 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 1
Size: 310 Color: 1
Size: 204 Color: 0

Bin 34: 0 of cap free
Amount of items: 6
Items: 
Size: 819 Color: 1
Size: 796 Color: 1
Size: 643 Color: 1
Size: 108 Color: 0
Size: 66 Color: 0
Size: 40 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 1
Size: 238 Color: 1
Size: 44 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 681 Color: 1
Size: 90 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 1
Size: 386 Color: 1
Size: 80 Color: 0

Bin 38: 0 of cap free
Amount of items: 5
Items: 
Size: 1602 Color: 1
Size: 618 Color: 1
Size: 120 Color: 1
Size: 92 Color: 0
Size: 40 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 1
Size: 378 Color: 1
Size: 32 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1030 Color: 1
Size: 204 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 222 Color: 1
Size: 84 Color: 0

Bin 42: 1 of cap free
Amount of items: 5
Items: 
Size: 1579 Color: 1
Size: 542 Color: 1
Size: 246 Color: 1
Size: 60 Color: 0
Size: 44 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 745 Color: 1
Size: 16 Color: 0

Bin 44: 1 of cap free
Amount of items: 5
Items: 
Size: 2001 Color: 1
Size: 218 Color: 1
Size: 144 Color: 0
Size: 100 Color: 0
Size: 8 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 1
Size: 446 Color: 1
Size: 104 Color: 0

Bin 46: 1 of cap free
Amount of items: 5
Items: 
Size: 1018 Color: 1
Size: 989 Color: 1
Size: 384 Color: 1
Size: 64 Color: 0
Size: 16 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 1
Size: 726 Color: 1
Size: 88 Color: 0

Bin 48: 1 of cap free
Amount of items: 5
Items: 
Size: 1405 Color: 1
Size: 505 Color: 1
Size: 461 Color: 1
Size: 76 Color: 0
Size: 24 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 1
Size: 651 Color: 1
Size: 8 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 1
Size: 398 Color: 1
Size: 134 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 1
Size: 779 Color: 1
Size: 200 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 1
Size: 1000 Color: 1
Size: 48 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 2082 Color: 1
Size: 340 Color: 1
Size: 48 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 802 Color: 1
Size: 128 Color: 0

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 482 Color: 1
Size: 88 Color: 0

Bin 56: 10 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 1
Size: 1044 Color: 1
Size: 172 Color: 0

Bin 57: 23 of cap free
Amount of items: 3
Items: 
Size: 1867 Color: 1
Size: 504 Color: 1
Size: 78 Color: 0

Bin 58: 38 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 1
Size: 1022 Color: 1
Size: 168 Color: 0

Bin 59: 258 of cap free
Amount of items: 1
Items: 
Size: 2214 Color: 1

Bin 60: 266 of cap free
Amount of items: 1
Items: 
Size: 2206 Color: 1

Bin 61: 274 of cap free
Amount of items: 1
Items: 
Size: 2198 Color: 1

Bin 62: 276 of cap free
Amount of items: 1
Items: 
Size: 2196 Color: 1

Bin 63: 283 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 1
Size: 393 Color: 1
Size: 64 Color: 0

Bin 64: 294 of cap free
Amount of items: 1
Items: 
Size: 2178 Color: 1

Bin 65: 362 of cap free
Amount of items: 1
Items: 
Size: 2110 Color: 1

Bin 66: 364 of cap free
Amount of items: 1
Items: 
Size: 2108 Color: 1

Total size: 160680
Total free space: 2472

