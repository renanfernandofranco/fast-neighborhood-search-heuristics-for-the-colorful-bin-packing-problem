Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 19
Items: 
Size: 200 Color: 66
Size: 200 Color: 65
Size: 172 Color: 61
Size: 172 Color: 60
Size: 168 Color: 58
Size: 160 Color: 57
Size: 152 Color: 56
Size: 146 Color: 55
Size: 140 Color: 54
Size: 132 Color: 53
Size: 112 Color: 43
Size: 100 Color: 40
Size: 100 Color: 39
Size: 96 Color: 36
Size: 88 Color: 32
Size: 82 Color: 30
Size: 80 Color: 29
Size: 80 Color: 28
Size: 76 Color: 27

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 1230 Color: 139
Size: 384 Color: 90
Size: 272 Color: 79
Size: 272 Color: 78
Size: 170 Color: 59
Size: 64 Color: 21
Size: 64 Color: 20

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 142
Size: 1018 Color: 133
Size: 96 Color: 37
Size: 52 Color: 16
Size: 52 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 151
Size: 884 Color: 129
Size: 40 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 159
Size: 570 Color: 106
Size: 200 Color: 64

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 166
Size: 590 Color: 109
Size: 116 Color: 44

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 169
Size: 513 Color: 101
Size: 122 Color: 48

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1890 Color: 175
Size: 444 Color: 95
Size: 122 Color: 49

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 176
Size: 382 Color: 89
Size: 176 Color: 62

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2030 Color: 185
Size: 358 Color: 87
Size: 68 Color: 24

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2060 Color: 187
Size: 364 Color: 88
Size: 32 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 188
Size: 262 Color: 76
Size: 132 Color: 52

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 189
Size: 236 Color: 71
Size: 120 Color: 46

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 190
Size: 342 Color: 86
Size: 12 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 192
Size: 266 Color: 77
Size: 60 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 193
Size: 226 Color: 69
Size: 92 Color: 35

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 196
Size: 204 Color: 68
Size: 88 Color: 33

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 197
Size: 202 Color: 67
Size: 84 Color: 31

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 201
Size: 182 Color: 63
Size: 64 Color: 22

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 163
Size: 684 Color: 118
Size: 50 Color: 14

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 164
Size: 604 Color: 110
Size: 126 Color: 50

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1834 Color: 170
Size: 621 Color: 113

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 172
Size: 613 Color: 112

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 183
Size: 449 Color: 96
Size: 8 Color: 1

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1869 Color: 173
Size: 585 Color: 108

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 178
Size: 522 Color: 103

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1970 Color: 181
Size: 484 Color: 98

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1980 Color: 182
Size: 474 Color: 97

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2020 Color: 184
Size: 434 Color: 94

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2122 Color: 191
Size: 332 Color: 85

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 194
Size: 308 Color: 83

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2156 Color: 195
Size: 298 Color: 82

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 198
Size: 282 Color: 81

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 2180 Color: 199
Size: 274 Color: 80

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2202 Color: 200
Size: 252 Color: 75

Bin 36: 3 of cap free
Amount of items: 5
Items: 
Size: 1249 Color: 143
Size: 1020 Color: 134
Size: 88 Color: 34
Size: 48 Color: 13
Size: 48 Color: 12

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 160
Size: 669 Color: 116
Size: 96 Color: 38

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 177
Size: 526 Color: 104
Size: 8 Color: 0

Bin 39: 4 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 141
Size: 540 Color: 105
Size: 504 Color: 100
Size: 120 Color: 47
Size: 52 Color: 17

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1591 Color: 154
Size: 861 Color: 126

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1713 Color: 162
Size: 739 Color: 120

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1774 Color: 167
Size: 678 Color: 117

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1841 Color: 171
Size: 611 Color: 111

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1876 Color: 174
Size: 576 Color: 107

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1938 Color: 179
Size: 514 Color: 102

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 180
Size: 491 Color: 99

Bin 47: 4 of cap free
Amount of items: 2
Items: 
Size: 2046 Color: 186
Size: 406 Color: 92

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 161
Size: 654 Color: 115
Size: 104 Color: 42

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 152
Size: 874 Color: 127
Size: 40 Color: 7

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 1644 Color: 156
Size: 806 Color: 123

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 1732 Color: 165
Size: 718 Color: 119

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1442 Color: 148
Size: 1007 Color: 132

Bin 53: 7 of cap free
Amount of items: 2
Items: 
Size: 1812 Color: 168
Size: 637 Color: 114

Bin 54: 8 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 158
Size: 777 Color: 122
Size: 16 Color: 3

Bin 55: 9 of cap free
Amount of items: 2
Items: 
Size: 1402 Color: 145
Size: 1045 Color: 137

Bin 56: 9 of cap free
Amount of items: 2
Items: 
Size: 1425 Color: 147
Size: 1022 Color: 136

Bin 57: 9 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 149
Size: 954 Color: 130
Size: 48 Color: 10

Bin 58: 10 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 157
Size: 772 Color: 121
Size: 28 Color: 4

Bin 59: 13 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 153
Size: 840 Color: 124
Size: 32 Color: 5

Bin 60: 14 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 146
Size: 984 Color: 131
Size: 48 Color: 11

Bin 61: 15 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 155
Size: 843 Color: 125

Bin 62: 20 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 140
Size: 413 Color: 93
Size: 404 Color: 91
Size: 330 Color: 84
Size: 56 Color: 18

Bin 63: 39 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 144
Size: 1021 Color: 135

Bin 64: 40 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 150
Size: 882 Color: 128
Size: 44 Color: 9

Bin 65: 51 of cap free
Amount of items: 8
Items: 
Size: 1229 Color: 138
Size: 244 Color: 74
Size: 244 Color: 73
Size: 242 Color: 72
Size: 234 Color: 70
Size: 72 Color: 26
Size: 72 Color: 25
Size: 68 Color: 23

Bin 66: 2110 of cap free
Amount of items: 3
Items: 
Size: 128 Color: 51
Size: 116 Color: 45
Size: 102 Color: 41

Total size: 159640
Total free space: 2456

