Capicity Bin: 14688
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8712 Color: 424
Size: 5672 Color: 391
Size: 304 Color: 52

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 461
Size: 2600 Color: 315
Size: 1449 Color: 235

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 483
Size: 2616 Color: 317
Size: 592 Color: 135

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11575 Color: 488
Size: 2595 Color: 313
Size: 518 Color: 120

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 489
Size: 2596 Color: 314
Size: 512 Color: 116

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11646 Color: 493
Size: 2498 Color: 310
Size: 544 Color: 125

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 500
Size: 1882 Color: 271
Size: 928 Color: 183

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 501
Size: 1892 Color: 272
Size: 904 Color: 178

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 510
Size: 2168 Color: 294
Size: 412 Color: 91

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 514
Size: 2332 Color: 303
Size: 204 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 515
Size: 2415 Color: 307
Size: 120 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 518
Size: 1792 Color: 263
Size: 702 Color: 153

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12289 Color: 522
Size: 2081 Color: 287
Size: 318 Color: 60

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 526
Size: 1864 Color: 270
Size: 448 Color: 105

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12379 Color: 527
Size: 1925 Color: 276
Size: 384 Color: 83

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 528
Size: 1788 Color: 262
Size: 520 Color: 121

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12404 Color: 530
Size: 1532 Color: 241
Size: 752 Color: 158

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 532
Size: 1540 Color: 242
Size: 728 Color: 156

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 535
Size: 1444 Color: 233
Size: 776 Color: 164

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12633 Color: 545
Size: 1431 Color: 228
Size: 624 Color: 139

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 546
Size: 1404 Color: 224
Size: 640 Color: 140

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12652 Color: 547
Size: 1332 Color: 215
Size: 704 Color: 154

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12661 Color: 548
Size: 1691 Color: 255
Size: 336 Color: 66

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 555
Size: 1322 Color: 213
Size: 592 Color: 136

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 556
Size: 1244 Color: 205
Size: 668 Color: 145

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12838 Color: 560
Size: 1402 Color: 223
Size: 448 Color: 106

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 562
Size: 1448 Color: 234
Size: 396 Color: 88

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 565
Size: 992 Color: 189
Size: 824 Color: 168

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 570
Size: 1032 Color: 190
Size: 694 Color: 152

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 571
Size: 1456 Color: 236
Size: 268 Color: 30

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 572
Size: 1292 Color: 210
Size: 428 Color: 101

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 574
Size: 1444 Color: 232
Size: 268 Color: 32

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13003 Color: 576
Size: 1405 Color: 225
Size: 280 Color: 43

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 577
Size: 1110 Color: 193
Size: 568 Color: 128

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13021 Color: 579
Size: 1335 Color: 216
Size: 332 Color: 65

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 581
Size: 1272 Color: 208
Size: 358 Color: 74

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13068 Color: 583
Size: 1336 Color: 217
Size: 284 Color: 44

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 584
Size: 1351 Color: 219
Size: 268 Color: 31

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 587
Size: 1220 Color: 201
Size: 376 Color: 79

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13106 Color: 590
Size: 1246 Color: 206
Size: 336 Color: 67

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 591
Size: 1431 Color: 229
Size: 148 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 592
Size: 958 Color: 186
Size: 616 Color: 137

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 593
Size: 1242 Color: 204
Size: 316 Color: 59

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 596
Size: 928 Color: 181
Size: 584 Color: 134

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13180 Color: 597
Size: 1130 Color: 195
Size: 378 Color: 82

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13194 Color: 598
Size: 1222 Color: 203
Size: 272 Color: 36

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 599
Size: 1220 Color: 202
Size: 270 Color: 33

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 600
Size: 1216 Color: 198
Size: 260 Color: 25

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 11379 Color: 481
Size: 2964 Color: 331
Size: 344 Color: 71

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 11494 Color: 484
Size: 2921 Color: 327
Size: 272 Color: 35

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 11572 Color: 487
Size: 3115 Color: 334

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 491
Size: 2140 Color: 291
Size: 932 Color: 184

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 499
Size: 2887 Color: 325

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 12025 Color: 506
Size: 2662 Color: 319

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12113 Color: 511
Size: 1314 Color: 212
Size: 1260 Color: 207

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 513
Size: 2181 Color: 295
Size: 376 Color: 80

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12193 Color: 517
Size: 2082 Color: 288
Size: 412 Color: 92

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 519
Size: 1644 Color: 251
Size: 842 Color: 171

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 520
Size: 1998 Color: 281
Size: 480 Color: 112

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 12409 Color: 531
Size: 2278 Color: 300

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12614 Color: 543
Size: 2073 Color: 286

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 12779 Color: 557
Size: 1908 Color: 274

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 12835 Color: 559
Size: 1852 Color: 269

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 563
Size: 1835 Color: 267

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 12923 Color: 567
Size: 1764 Color: 260

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 12951 Color: 568
Size: 1736 Color: 259

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 589
Size: 1591 Color: 247

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 441
Size: 4514 Color: 370
Size: 276 Color: 38

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 10044 Color: 447
Size: 4642 Color: 374

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 10991 Color: 471
Size: 3471 Color: 349
Size: 224 Color: 10

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 11542 Color: 485
Size: 3144 Color: 336

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 509
Size: 1686 Color: 254
Size: 896 Color: 175

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12363 Color: 525
Size: 2323 Color: 302

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 561
Size: 1846 Color: 268

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 566
Size: 1801 Color: 265

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12956 Color: 569
Size: 1730 Color: 258

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 573
Size: 1713 Color: 257

Bin 78: 3 of cap free
Amount of items: 9
Items: 
Size: 7348 Color: 407
Size: 1302 Color: 211
Size: 1291 Color: 209
Size: 1216 Color: 199
Size: 1184 Color: 197
Size: 1176 Color: 196
Size: 490 Color: 114
Size: 342 Color: 70
Size: 336 Color: 69

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 459
Size: 3833 Color: 356
Size: 256 Color: 20

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 11176 Color: 476
Size: 3349 Color: 341
Size: 160 Color: 4

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 492
Size: 1542 Color: 243
Size: 1503 Color: 238

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 11753 Color: 497
Size: 2932 Color: 329

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 12124 Color: 512
Size: 2561 Color: 312

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 12494 Color: 537
Size: 2191 Color: 296

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 12529 Color: 538
Size: 2156 Color: 293

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 12685 Color: 550
Size: 2000 Color: 282

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13074 Color: 585
Size: 1611 Color: 249

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13087 Color: 586
Size: 1598 Color: 248

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13095 Color: 588
Size: 1590 Color: 246

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 595
Size: 1545 Color: 245

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 9244 Color: 433
Size: 5440 Color: 388

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 9956 Color: 443
Size: 4728 Color: 377

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 12537 Color: 539
Size: 2147 Color: 292

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 12724 Color: 552
Size: 1960 Color: 279

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 12854 Color: 564
Size: 1830 Color: 266

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 575
Size: 1704 Color: 256

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 8739 Color: 425
Size: 5944 Color: 394

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 10643 Color: 462
Size: 3784 Color: 354
Size: 256 Color: 18

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 544
Size: 2067 Color: 285

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 12755 Color: 554
Size: 1928 Color: 277

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 12782 Color: 558
Size: 1901 Color: 273

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 13012 Color: 578
Size: 1671 Color: 253

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 13061 Color: 582
Size: 1622 Color: 250

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 594
Size: 1544 Color: 244

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 9906 Color: 442
Size: 4500 Color: 369
Size: 276 Color: 37

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 12294 Color: 523
Size: 2388 Color: 305

Bin 107: 7 of cap free
Amount of items: 11
Items: 
Size: 7345 Color: 405
Size: 928 Color: 182
Size: 928 Color: 180
Size: 912 Color: 179
Size: 896 Color: 177
Size: 896 Color: 176
Size: 888 Color: 174
Size: 784 Color: 165
Size: 368 Color: 78
Size: 368 Color: 77
Size: 368 Color: 76

Bin 108: 7 of cap free
Amount of items: 7
Items: 
Size: 7354 Color: 410
Size: 1784 Color: 261
Size: 1530 Color: 240
Size: 1528 Color: 239
Size: 1471 Color: 237
Size: 690 Color: 151
Size: 324 Color: 63

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 11745 Color: 496
Size: 2936 Color: 330

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 529
Size: 2289 Color: 301

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 12742 Color: 553
Size: 1939 Color: 278

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 423
Size: 5886 Color: 393
Size: 304 Color: 53

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 9996 Color: 445
Size: 4684 Color: 376

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 11185 Color: 477
Size: 3399 Color: 345
Size: 96 Color: 1

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 11225 Color: 479
Size: 3455 Color: 348

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 11258 Color: 480
Size: 3422 Color: 347

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 10523 Color: 456
Size: 3900 Color: 358
Size: 256 Color: 22

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 10671 Color: 464
Size: 4008 Color: 363

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 10138 Color: 449
Size: 4540 Color: 372

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 11560 Color: 486
Size: 3118 Color: 335

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 11998 Color: 505
Size: 2680 Color: 320

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 580
Size: 1652 Color: 252

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 10012 Color: 446
Size: 4665 Color: 375

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 11419 Color: 482
Size: 3258 Color: 338

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 12073 Color: 508
Size: 2604 Color: 316

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 534
Size: 2221 Color: 298

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 10708 Color: 466
Size: 3720 Color: 353
Size: 248 Color: 16

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 10782 Color: 467
Size: 3646 Color: 352
Size: 248 Color: 15

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 11132 Color: 474
Size: 3336 Color: 340
Size: 208 Color: 7

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 11774 Color: 498
Size: 2902 Color: 326

Bin 131: 13 of cap free
Amount of items: 3
Items: 
Size: 10543 Color: 457
Size: 3876 Color: 357
Size: 256 Color: 21

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 12053 Color: 507
Size: 2622 Color: 318

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 12666 Color: 549
Size: 2009 Color: 283

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 551
Size: 1967 Color: 280

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12156 Color: 516
Size: 2518 Color: 311

Bin 136: 15 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 455
Size: 3948 Color: 361
Size: 258 Color: 23

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 12553 Color: 541
Size: 2120 Color: 290

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 12430 Color: 533
Size: 2242 Color: 299

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 10920 Color: 469
Size: 3519 Color: 350
Size: 232 Color: 12

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 12218 Color: 521
Size: 2453 Color: 309

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 12329 Color: 524
Size: 2342 Color: 304

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 12474 Color: 536
Size: 2197 Color: 297

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 12609 Color: 542
Size: 2062 Color: 284

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 9711 Color: 438
Size: 4959 Color: 380

Bin 145: 18 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 478
Size: 3412 Color: 346
Size: 70 Color: 0

Bin 146: 20 of cap free
Amount of items: 4
Items: 
Size: 10824 Color: 468
Size: 3364 Color: 342
Size: 240 Color: 14
Size: 240 Color: 13

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 11744 Color: 495
Size: 2924 Color: 328

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 11943 Color: 504
Size: 2725 Color: 321

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 12552 Color: 540
Size: 2116 Color: 289

Bin 150: 21 of cap free
Amount of items: 2
Items: 
Size: 11901 Color: 503
Size: 2766 Color: 323

Bin 151: 24 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 427
Size: 5428 Color: 387
Size: 300 Color: 50

Bin 152: 24 of cap free
Amount of items: 3
Items: 
Size: 9118 Color: 431
Size: 5258 Color: 385
Size: 288 Color: 48

Bin 153: 24 of cap free
Amount of items: 3
Items: 
Size: 9210 Color: 432
Size: 5166 Color: 383
Size: 288 Color: 47

Bin 154: 25 of cap free
Amount of items: 2
Items: 
Size: 11582 Color: 490
Size: 3081 Color: 333

Bin 155: 27 of cap free
Amount of items: 3
Items: 
Size: 10611 Color: 460
Size: 3794 Color: 355
Size: 256 Color: 19

Bin 156: 29 of cap free
Amount of items: 2
Items: 
Size: 9635 Color: 437
Size: 5024 Color: 382

Bin 157: 29 of cap free
Amount of items: 2
Items: 
Size: 11900 Color: 502
Size: 2759 Color: 322

Bin 158: 33 of cap free
Amount of items: 3
Items: 
Size: 8935 Color: 426
Size: 5420 Color: 386
Size: 300 Color: 51

Bin 159: 36 of cap free
Amount of items: 3
Items: 
Size: 7896 Color: 416
Size: 6448 Color: 401
Size: 308 Color: 56

Bin 160: 36 of cap free
Amount of items: 3
Items: 
Size: 9810 Color: 440
Size: 4566 Color: 373
Size: 276 Color: 39

Bin 161: 38 of cap free
Amount of items: 3
Items: 
Size: 11118 Color: 473
Size: 3324 Color: 339
Size: 208 Color: 8

Bin 162: 39 of cap free
Amount of items: 3
Items: 
Size: 11050 Color: 472
Size: 3375 Color: 344
Size: 224 Color: 9

Bin 163: 40 of cap free
Amount of items: 3
Items: 
Size: 10089 Color: 448
Size: 4287 Color: 368
Size: 272 Color: 34

Bin 164: 40 of cap free
Amount of items: 2
Items: 
Size: 10582 Color: 458
Size: 4066 Color: 364

Bin 165: 40 of cap free
Amount of items: 2
Items: 
Size: 11670 Color: 494
Size: 2978 Color: 332

Bin 166: 41 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 444
Size: 2862 Color: 324
Size: 1793 Color: 264

Bin 167: 43 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 452
Size: 4149 Color: 366
Size: 264 Color: 27

Bin 168: 46 of cap free
Amount of items: 2
Items: 
Size: 7362 Color: 412
Size: 7280 Color: 404

Bin 169: 46 of cap free
Amount of items: 3
Items: 
Size: 10165 Color: 451
Size: 4211 Color: 367
Size: 266 Color: 28

Bin 170: 49 of cap free
Amount of items: 4
Items: 
Size: 9272 Color: 434
Size: 4795 Color: 378
Size: 288 Color: 46
Size: 284 Color: 45

Bin 171: 51 of cap free
Amount of items: 2
Items: 
Size: 9076 Color: 429
Size: 5561 Color: 390

Bin 172: 51 of cap free
Amount of items: 3
Items: 
Size: 9091 Color: 430
Size: 5252 Color: 384
Size: 294 Color: 49

Bin 173: 54 of cap free
Amount of items: 28
Items: 
Size: 674 Color: 147
Size: 674 Color: 146
Size: 664 Color: 144
Size: 656 Color: 143
Size: 656 Color: 142
Size: 648 Color: 141
Size: 622 Color: 138
Size: 582 Color: 133
Size: 576 Color: 132
Size: 576 Color: 131
Size: 576 Color: 130
Size: 576 Color: 129
Size: 556 Color: 127
Size: 550 Color: 126
Size: 532 Color: 124
Size: 454 Color: 107
Size: 442 Color: 104
Size: 438 Color: 103
Size: 434 Color: 102
Size: 424 Color: 100
Size: 424 Color: 99
Size: 416 Color: 98
Size: 416 Color: 97
Size: 416 Color: 96
Size: 416 Color: 95
Size: 414 Color: 94
Size: 414 Color: 93
Size: 408 Color: 90

Bin 174: 55 of cap free
Amount of items: 3
Items: 
Size: 9545 Color: 436
Size: 4808 Color: 379
Size: 280 Color: 41

Bin 175: 58 of cap free
Amount of items: 3
Items: 
Size: 7560 Color: 413
Size: 6750 Color: 402
Size: 320 Color: 61

Bin 176: 59 of cap free
Amount of items: 7
Items: 
Size: 7352 Color: 409
Size: 1443 Color: 231
Size: 1442 Color: 230
Size: 1428 Color: 227
Size: 1420 Color: 226
Size: 1216 Color: 200
Size: 328 Color: 64

Bin 177: 61 of cap free
Amount of items: 2
Items: 
Size: 10660 Color: 463
Size: 3967 Color: 362

Bin 178: 70 of cap free
Amount of items: 22
Items: 
Size: 856 Color: 173
Size: 848 Color: 172
Size: 832 Color: 170
Size: 828 Color: 169
Size: 812 Color: 167
Size: 784 Color: 166
Size: 776 Color: 163
Size: 768 Color: 162
Size: 768 Color: 161
Size: 766 Color: 160
Size: 756 Color: 159
Size: 736 Color: 157
Size: 724 Color: 155
Size: 684 Color: 150
Size: 680 Color: 149
Size: 678 Color: 148
Size: 400 Color: 89
Size: 392 Color: 87
Size: 386 Color: 86
Size: 384 Color: 85
Size: 384 Color: 84
Size: 376 Color: 81

Bin 179: 92 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 475
Size: 3224 Color: 337
Size: 200 Color: 5

Bin 180: 113 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 465
Size: 3631 Color: 351
Size: 248 Color: 17

Bin 181: 132 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 435
Size: 4984 Color: 381
Size: 280 Color: 42

Bin 182: 135 of cap free
Amount of items: 3
Items: 
Size: 8420 Color: 422
Size: 5829 Color: 392
Size: 304 Color: 54

Bin 183: 138 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 450
Size: 4130 Color: 365
Size: 268 Color: 29

Bin 184: 142 of cap free
Amount of items: 3
Items: 
Size: 10951 Color: 470
Size: 3371 Color: 343
Size: 224 Color: 11

Bin 185: 154 of cap free
Amount of items: 3
Items: 
Size: 9734 Color: 439
Size: 4520 Color: 371
Size: 280 Color: 40

Bin 186: 158 of cap free
Amount of items: 10
Items: 
Size: 7346 Color: 406
Size: 1120 Color: 194
Size: 1048 Color: 192
Size: 1048 Color: 191
Size: 990 Color: 188
Size: 960 Color: 187
Size: 944 Color: 185
Size: 364 Color: 75
Size: 358 Color: 73
Size: 352 Color: 72

Bin 187: 167 of cap free
Amount of items: 3
Items: 
Size: 10333 Color: 454
Size: 3928 Color: 360
Size: 260 Color: 24

Bin 188: 173 of cap free
Amount of items: 2
Items: 
Size: 7626 Color: 414
Size: 6889 Color: 403

Bin 189: 176 of cap free
Amount of items: 2
Items: 
Size: 8388 Color: 421
Size: 6124 Color: 400

Bin 190: 181 of cap free
Amount of items: 2
Items: 
Size: 9032 Color: 428
Size: 5475 Color: 389

Bin 191: 184 of cap free
Amount of items: 2
Items: 
Size: 8382 Color: 420
Size: 6122 Color: 399

Bin 192: 194 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 453
Size: 3916 Color: 359
Size: 264 Color: 26

Bin 193: 196 of cap free
Amount of items: 2
Items: 
Size: 8372 Color: 419
Size: 6120 Color: 398

Bin 194: 221 of cap free
Amount of items: 4
Items: 
Size: 7729 Color: 415
Size: 6114 Color: 395
Size: 316 Color: 58
Size: 308 Color: 57

Bin 195: 223 of cap free
Amount of items: 7
Items: 
Size: 7349 Color: 408
Size: 1386 Color: 222
Size: 1362 Color: 221
Size: 1357 Color: 220
Size: 1346 Color: 218
Size: 1329 Color: 214
Size: 336 Color: 68

Bin 196: 231 of cap free
Amount of items: 5
Items: 
Size: 7356 Color: 411
Size: 2447 Color: 308
Size: 2408 Color: 306
Size: 1924 Color: 275
Size: 322 Color: 62

Bin 197: 251 of cap free
Amount of items: 3
Items: 
Size: 8017 Color: 417
Size: 6116 Color: 396
Size: 304 Color: 55

Bin 198: 286 of cap free
Amount of items: 2
Items: 
Size: 8285 Color: 418
Size: 6117 Color: 397

Bin 199: 9260 of cap free
Amount of items: 11
Items: 
Size: 528 Color: 123
Size: 524 Color: 122
Size: 512 Color: 119
Size: 512 Color: 118
Size: 512 Color: 117
Size: 500 Color: 115
Size: 488 Color: 113
Size: 468 Color: 111
Size: 464 Color: 110
Size: 464 Color: 109
Size: 456 Color: 108

Total size: 2908224
Total free space: 14688

