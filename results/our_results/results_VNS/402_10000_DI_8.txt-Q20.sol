Capicity Bin: 7760
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4328 Color: 10
Size: 3224 Color: 11
Size: 208 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 6
Size: 2608 Color: 5
Size: 376 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 16
Size: 2324 Color: 15
Size: 392 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 8
Size: 2056 Color: 13
Size: 112 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5998 Color: 14
Size: 1612 Color: 11
Size: 150 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 16
Size: 1143 Color: 0
Size: 352 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 8
Size: 1202 Color: 11
Size: 288 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 13
Size: 718 Color: 7
Size: 640 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 8
Size: 1192 Color: 12
Size: 164 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 17
Size: 928 Color: 18
Size: 312 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 3
Size: 702 Color: 7
Size: 484 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6597 Color: 6
Size: 951 Color: 7
Size: 212 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 11
Size: 942 Color: 16
Size: 188 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 2
Size: 924 Color: 19
Size: 192 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 15
Size: 864 Color: 2
Size: 182 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 8
Size: 862 Color: 4
Size: 136 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 7
Size: 756 Color: 1
Size: 160 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6851 Color: 6
Size: 733 Color: 19
Size: 176 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 4
Size: 650 Color: 9
Size: 208 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 11
Size: 678 Color: 6
Size: 164 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6955 Color: 5
Size: 671 Color: 16
Size: 134 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 7
Size: 558 Color: 13
Size: 236 Color: 9

Bin 23: 1 of cap free
Amount of items: 10
Items: 
Size: 3881 Color: 5
Size: 556 Color: 2
Size: 544 Color: 0
Size: 496 Color: 12
Size: 494 Color: 16
Size: 484 Color: 4
Size: 480 Color: 6
Size: 456 Color: 5
Size: 224 Color: 3
Size: 144 Color: 8

Bin 24: 1 of cap free
Amount of items: 7
Items: 
Size: 3885 Color: 7
Size: 759 Color: 4
Size: 731 Color: 12
Size: 662 Color: 3
Size: 640 Color: 13
Size: 640 Color: 2
Size: 442 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 4845 Color: 7
Size: 2774 Color: 1
Size: 140 Color: 18

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 5721 Color: 2
Size: 1894 Color: 0
Size: 144 Color: 12

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 7
Size: 1731 Color: 4
Size: 128 Color: 5

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5962 Color: 12
Size: 1593 Color: 12
Size: 204 Color: 17

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 8
Size: 1289 Color: 15
Size: 250 Color: 2

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6249 Color: 12
Size: 1134 Color: 8
Size: 376 Color: 14

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 1
Size: 1200 Color: 3
Size: 176 Color: 4

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 6389 Color: 2
Size: 1370 Color: 8

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 18
Size: 904 Color: 12
Size: 192 Color: 2

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 0
Size: 821 Color: 14
Size: 258 Color: 2

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 15
Size: 1048 Color: 19
Size: 24 Color: 9

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 19
Size: 744 Color: 6
Size: 312 Color: 11

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6775 Color: 13
Size: 816 Color: 6
Size: 168 Color: 19

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6885 Color: 8
Size: 874 Color: 5

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6967 Color: 6
Size: 464 Color: 8
Size: 328 Color: 12

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 4423 Color: 17
Size: 2791 Color: 16
Size: 544 Color: 17

Bin 41: 2 of cap free
Amount of items: 4
Items: 
Size: 4972 Color: 5
Size: 2308 Color: 13
Size: 338 Color: 0
Size: 140 Color: 2

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 6
Size: 1964 Color: 1
Size: 64 Color: 8

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 6057 Color: 19
Size: 1701 Color: 18

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 6202 Color: 14
Size: 1556 Color: 6

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 14
Size: 1372 Color: 19

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 6672 Color: 10
Size: 930 Color: 3
Size: 156 Color: 6

Bin 47: 3 of cap free
Amount of items: 5
Items: 
Size: 3884 Color: 2
Size: 1597 Color: 10
Size: 1596 Color: 8
Size: 376 Color: 4
Size: 304 Color: 17

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 4403 Color: 2
Size: 3222 Color: 0
Size: 132 Color: 1

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 0
Size: 1876 Color: 17
Size: 276 Color: 11

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 18
Size: 973 Color: 16

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 4
Size: 3136 Color: 6
Size: 144 Color: 0

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 4980 Color: 16
Size: 2776 Color: 13

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 5064 Color: 15
Size: 2410 Color: 13
Size: 282 Color: 3

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 11
Size: 2422 Color: 12
Size: 132 Color: 17

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 5742 Color: 5
Size: 2014 Color: 4

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 12
Size: 1616 Color: 16
Size: 382 Color: 18

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 5774 Color: 14
Size: 1726 Color: 10
Size: 256 Color: 11

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 6509 Color: 2
Size: 1247 Color: 16

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 6694 Color: 17
Size: 1062 Color: 5

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 3
Size: 822 Color: 5

Bin 61: 5 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 19
Size: 2799 Color: 1
Size: 276 Color: 3

Bin 62: 5 of cap free
Amount of items: 3
Items: 
Size: 5490 Color: 6
Size: 1621 Color: 5
Size: 644 Color: 11

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 6097 Color: 5
Size: 1658 Color: 0

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 8
Size: 1421 Color: 4
Size: 178 Color: 16

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 12
Size: 1162 Color: 3

Bin 66: 5 of cap free
Amount of items: 5
Items: 
Size: 6646 Color: 9
Size: 1061 Color: 2
Size: 16 Color: 18
Size: 16 Color: 8
Size: 16 Color: 6

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 17
Size: 895 Color: 0

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6984 Color: 17
Size: 771 Color: 7

Bin 69: 6 of cap free
Amount of items: 6
Items: 
Size: 3890 Color: 12
Size: 1132 Color: 1
Size: 890 Color: 0
Size: 856 Color: 4
Size: 834 Color: 8
Size: 152 Color: 11

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 4
Size: 2781 Color: 11
Size: 118 Color: 8

Bin 71: 7 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 1
Size: 1608 Color: 6
Size: 248 Color: 11

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 6215 Color: 14
Size: 1538 Color: 6

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 6492 Color: 3
Size: 1261 Color: 2

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 6872 Color: 12
Size: 881 Color: 15

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 9
Size: 785 Color: 13

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 4880 Color: 12
Size: 2872 Color: 9

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 5360 Color: 4
Size: 2120 Color: 0
Size: 272 Color: 2

Bin 78: 8 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 2
Size: 1980 Color: 6
Size: 272 Color: 4

Bin 79: 8 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 0
Size: 1792 Color: 5
Size: 132 Color: 4

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 6040 Color: 16
Size: 1302 Color: 4
Size: 410 Color: 19

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 13
Size: 1008 Color: 18

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 19
Size: 915 Color: 1

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 6819 Color: 10
Size: 932 Color: 18

Bin 84: 9 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 7
Size: 769 Color: 5

Bin 85: 10 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 2122 Color: 15
Size: 160 Color: 12

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 5502 Color: 15
Size: 2248 Color: 17

Bin 87: 10 of cap free
Amount of items: 3
Items: 
Size: 6118 Color: 18
Size: 1448 Color: 8
Size: 184 Color: 11

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 0
Size: 1284 Color: 6
Size: 96 Color: 11

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 6660 Color: 4
Size: 1090 Color: 12

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 6600 Color: 11
Size: 1149 Color: 4

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 0
Size: 971 Color: 13

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 6688 Color: 16
Size: 1060 Color: 15

Bin 93: 12 of cap free
Amount of items: 3
Items: 
Size: 6730 Color: 14
Size: 968 Color: 5
Size: 50 Color: 17

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 6759 Color: 17
Size: 989 Color: 10

Bin 95: 14 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 3
Size: 764 Color: 4
Size: 32 Color: 8

Bin 96: 15 of cap free
Amount of items: 2
Items: 
Size: 5832 Color: 6
Size: 1913 Color: 13

Bin 97: 15 of cap free
Amount of items: 2
Items: 
Size: 5929 Color: 14
Size: 1816 Color: 13

Bin 98: 15 of cap free
Amount of items: 3
Items: 
Size: 6344 Color: 14
Size: 1043 Color: 3
Size: 358 Color: 12

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 8
Size: 864 Color: 15

Bin 100: 20 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 15
Size: 2786 Color: 6
Size: 552 Color: 4

Bin 101: 20 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 2
Size: 1304 Color: 6
Size: 320 Color: 17

Bin 102: 23 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 19
Size: 2221 Color: 1

Bin 103: 24 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 17
Size: 2740 Color: 12

Bin 104: 24 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 17
Size: 1884 Color: 1

Bin 105: 25 of cap free
Amount of items: 2
Items: 
Size: 5297 Color: 17
Size: 2438 Color: 9

Bin 106: 25 of cap free
Amount of items: 2
Items: 
Size: 5921 Color: 17
Size: 1814 Color: 6

Bin 107: 27 of cap free
Amount of items: 4
Items: 
Size: 5304 Color: 9
Size: 2053 Color: 6
Size: 248 Color: 11
Size: 128 Color: 12

Bin 108: 27 of cap free
Amount of items: 2
Items: 
Size: 5465 Color: 10
Size: 2268 Color: 17

Bin 109: 27 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 14
Size: 690 Color: 3
Size: 556 Color: 6

Bin 110: 28 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 18
Size: 1242 Color: 7

Bin 111: 30 of cap free
Amount of items: 3
Items: 
Size: 4838 Color: 19
Size: 2732 Color: 15
Size: 160 Color: 4

Bin 112: 31 of cap free
Amount of items: 2
Items: 
Size: 4870 Color: 2
Size: 2859 Color: 14

Bin 113: 32 of cap free
Amount of items: 4
Items: 
Size: 5097 Color: 1
Size: 2421 Color: 12
Size: 146 Color: 2
Size: 64 Color: 7

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 4
Size: 2324 Color: 7

Bin 115: 39 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 12
Size: 2802 Color: 11
Size: 64 Color: 0

Bin 116: 39 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 2
Size: 1146 Color: 1

Bin 117: 40 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 2
Size: 3236 Color: 8

Bin 118: 45 of cap free
Amount of items: 3
Items: 
Size: 4434 Color: 8
Size: 2797 Color: 3
Size: 484 Color: 11

Bin 119: 50 of cap free
Amount of items: 2
Items: 
Size: 5576 Color: 10
Size: 2134 Color: 0

Bin 120: 51 of cap free
Amount of items: 2
Items: 
Size: 6322 Color: 6
Size: 1387 Color: 13

Bin 121: 53 of cap free
Amount of items: 4
Items: 
Size: 3892 Color: 11
Size: 1579 Color: 15
Size: 1572 Color: 14
Size: 664 Color: 16

Bin 122: 58 of cap free
Amount of items: 2
Items: 
Size: 5214 Color: 7
Size: 2488 Color: 17

Bin 123: 58 of cap free
Amount of items: 2
Items: 
Size: 5905 Color: 14
Size: 1797 Color: 7

Bin 124: 58 of cap free
Amount of items: 2
Items: 
Size: 6200 Color: 6
Size: 1502 Color: 12

Bin 125: 61 of cap free
Amount of items: 2
Items: 
Size: 5817 Color: 3
Size: 1882 Color: 16

Bin 126: 74 of cap free
Amount of items: 3
Items: 
Size: 3898 Color: 10
Size: 3228 Color: 16
Size: 560 Color: 11

Bin 127: 80 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 12
Size: 3226 Color: 19
Size: 558 Color: 0

Bin 128: 84 of cap free
Amount of items: 7
Items: 
Size: 3882 Color: 4
Size: 664 Color: 17
Size: 646 Color: 14
Size: 644 Color: 16
Size: 644 Color: 1
Size: 640 Color: 0
Size: 556 Color: 8

Bin 129: 108 of cap free
Amount of items: 2
Items: 
Size: 4418 Color: 9
Size: 3234 Color: 18

Bin 130: 116 of cap free
Amount of items: 2
Items: 
Size: 4411 Color: 17
Size: 3233 Color: 1

Bin 131: 122 of cap free
Amount of items: 2
Items: 
Size: 4407 Color: 1
Size: 3231 Color: 18

Bin 132: 138 of cap free
Amount of items: 26
Items: 
Size: 456 Color: 14
Size: 448 Color: 4
Size: 448 Color: 1
Size: 424 Color: 7
Size: 424 Color: 2
Size: 400 Color: 2
Size: 368 Color: 18
Size: 322 Color: 16
Size: 320 Color: 7
Size: 304 Color: 9
Size: 296 Color: 14
Size: 272 Color: 6
Size: 256 Color: 12
Size: 256 Color: 7
Size: 256 Color: 1
Size: 252 Color: 13
Size: 246 Color: 10
Size: 240 Color: 6
Size: 228 Color: 14
Size: 228 Color: 11
Size: 224 Color: 17
Size: 224 Color: 0
Size: 194 Color: 8
Size: 184 Color: 11
Size: 176 Color: 3
Size: 176 Color: 3

Bin 133: 5664 of cap free
Amount of items: 11
Items: 
Size: 228 Color: 13
Size: 228 Color: 9
Size: 224 Color: 15
Size: 208 Color: 0
Size: 196 Color: 6
Size: 192 Color: 12
Size: 176 Color: 16
Size: 172 Color: 17
Size: 160 Color: 11
Size: 160 Color: 8
Size: 152 Color: 3

Total size: 1024320
Total free space: 7760

