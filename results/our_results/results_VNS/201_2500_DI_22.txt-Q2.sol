Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 6
Items: 
Size: 1021 Color: 0
Size: 569 Color: 0
Size: 190 Color: 1
Size: 168 Color: 1
Size: 52 Color: 0
Size: 36 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 0
Size: 890 Color: 0
Size: 124 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 1
Size: 727 Color: 0
Size: 144 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 0
Size: 721 Color: 0
Size: 144 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 0
Size: 623 Color: 0
Size: 124 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 0
Size: 622 Color: 0
Size: 48 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 1
Size: 541 Color: 0
Size: 106 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 1
Size: 525 Color: 0
Size: 36 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 0
Size: 438 Color: 0
Size: 84 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 0
Size: 361 Color: 0
Size: 70 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 0
Size: 367 Color: 0
Size: 52 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 0
Size: 262 Color: 1
Size: 112 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1664 Color: 0
Size: 316 Color: 1
Size: 56 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 1
Size: 242 Color: 1
Size: 68 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 0
Size: 211 Color: 1
Size: 60 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 1
Size: 187 Color: 1
Size: 36 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 182 Color: 0
Size: 40 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 1815 Color: 1
Size: 185 Color: 0
Size: 28 Color: 1
Size: 8 Color: 0

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 1066 Color: 0
Size: 845 Color: 1
Size: 88 Color: 1
Size: 36 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 0
Size: 537 Color: 0
Size: 104 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 0
Size: 562 Color: 0
Size: 72 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 0
Size: 442 Color: 1
Size: 44 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 1
Size: 362 Color: 0
Size: 64 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1645 Color: 0
Size: 314 Color: 1
Size: 76 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 326 Color: 1
Size: 58 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1719 Color: 1
Size: 224 Color: 1
Size: 92 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 0
Size: 257 Color: 1
Size: 80 Color: 0

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 0
Size: 293 Color: 1

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 1
Size: 225 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 1
Size: 820 Color: 0
Size: 36 Color: 0

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1408 Color: 1
Size: 538 Color: 0
Size: 88 Color: 1

Bin 32: 3 of cap free
Amount of items: 7
Items: 
Size: 1019 Color: 0
Size: 468 Color: 0
Size: 224 Color: 0
Size: 124 Color: 1
Size: 108 Color: 1
Size: 50 Color: 0
Size: 40 Color: 1

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 0
Size: 531 Color: 1
Size: 52 Color: 0

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 0
Size: 490 Color: 1
Size: 36 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 0
Size: 227 Color: 1
Size: 160 Color: 0

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 1
Size: 348 Color: 0

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 0
Size: 316 Color: 1
Size: 20 Color: 0

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1787 Color: 1
Size: 246 Color: 0

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 0
Size: 394 Color: 1
Size: 132 Color: 0

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 1
Size: 298 Color: 0

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1750 Color: 0
Size: 282 Color: 1

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 1
Size: 230 Color: 0

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1785 Color: 0
Size: 247 Color: 1

Bin 44: 4 of cap free
Amount of items: 4
Items: 
Size: 1818 Color: 1
Size: 190 Color: 0
Size: 16 Color: 1
Size: 8 Color: 0

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1741 Color: 0
Size: 290 Color: 1

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 0
Size: 463 Color: 1

Bin 47: 8 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 1
Size: 407 Color: 1
Size: 140 Color: 0

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1762 Color: 0
Size: 265 Color: 1

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 0
Size: 692 Color: 1
Size: 44 Color: 1

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 0
Size: 424 Color: 1

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 1
Size: 198 Color: 0

Bin 52: 13 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 321 Color: 0
Size: 20 Color: 0

Bin 53: 15 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 657 Color: 1
Size: 126 Color: 1

Bin 54: 15 of cap free
Amount of items: 2
Items: 
Size: 1355 Color: 0
Size: 666 Color: 1

Bin 55: 15 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 1
Size: 254 Color: 0

Bin 56: 17 of cap free
Amount of items: 13
Items: 
Size: 989 Color: 0
Size: 168 Color: 0
Size: 106 Color: 1
Size: 104 Color: 0
Size: 96 Color: 0
Size: 88 Color: 1
Size: 76 Color: 1
Size: 76 Color: 1
Size: 76 Color: 1
Size: 64 Color: 1
Size: 64 Color: 0
Size: 56 Color: 1
Size: 56 Color: 0

Bin 57: 17 of cap free
Amount of items: 2
Items: 
Size: 1276 Color: 1
Size: 743 Color: 0

Bin 58: 19 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 0
Size: 327 Color: 1

Bin 59: 22 of cap free
Amount of items: 2
Items: 
Size: 1167 Color: 0
Size: 847 Color: 1

Bin 60: 24 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 1
Size: 441 Color: 0

Bin 61: 24 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 1
Size: 283 Color: 0

Bin 62: 25 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 1
Size: 810 Color: 1
Size: 178 Color: 0

Bin 63: 27 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 0
Size: 718 Color: 1

Bin 64: 27 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 0
Size: 407 Color: 1
Size: 209 Color: 1

Bin 65: 27 of cap free
Amount of items: 2
Items: 
Size: 1620 Color: 1
Size: 389 Color: 0

Bin 66: 1628 of cap free
Amount of items: 9
Items: 
Size: 56 Color: 1
Size: 56 Color: 1
Size: 48 Color: 0
Size: 48 Color: 0
Size: 48 Color: 0
Size: 44 Color: 0
Size: 44 Color: 0
Size: 32 Color: 1
Size: 32 Color: 1

Total size: 132340
Total free space: 2036

