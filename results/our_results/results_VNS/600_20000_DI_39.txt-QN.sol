Capicity Bin: 16320
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 437
Size: 5830 Color: 376
Size: 330 Color: 42

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 10352 Color: 447
Size: 4976 Color: 359
Size: 384 Color: 64
Size: 304 Color: 30
Size: 304 Color: 28

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11752 Color: 468
Size: 3568 Color: 328
Size: 1000 Color: 168

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 472
Size: 2302 Color: 276
Size: 2190 Color: 269

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12210 Color: 481
Size: 3746 Color: 335
Size: 364 Color: 58

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 484
Size: 3292 Color: 318
Size: 716 Color: 136

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 486
Size: 3562 Color: 327
Size: 388 Color: 65

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 488
Size: 2942 Color: 308
Size: 950 Color: 159

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 489
Size: 3362 Color: 321
Size: 518 Color: 101

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12443 Color: 490
Size: 3379 Color: 322
Size: 498 Color: 99

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 495
Size: 2872 Color: 303
Size: 674 Color: 132

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 496
Size: 2568 Color: 290
Size: 976 Color: 161

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 497
Size: 2366 Color: 280
Size: 1164 Color: 184

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 500
Size: 1936 Color: 254
Size: 1548 Color: 216

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 504
Size: 2908 Color: 305
Size: 436 Color: 82

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13172 Color: 508
Size: 2164 Color: 266
Size: 984 Color: 163

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13178 Color: 510
Size: 3022 Color: 310
Size: 120 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13201 Color: 511
Size: 1657 Color: 228
Size: 1462 Color: 211

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13328 Color: 514
Size: 2576 Color: 291
Size: 416 Color: 73

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13329 Color: 515
Size: 2351 Color: 278
Size: 640 Color: 125

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 516
Size: 2002 Color: 256
Size: 984 Color: 162

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 521
Size: 2004 Color: 257
Size: 864 Color: 156

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13653 Color: 529
Size: 2223 Color: 272
Size: 444 Color: 84

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13771 Color: 535
Size: 1549 Color: 217
Size: 1000 Color: 169

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 536
Size: 2216 Color: 271
Size: 304 Color: 29

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13890 Color: 540
Size: 1828 Color: 248
Size: 602 Color: 119

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 542
Size: 2021 Color: 261
Size: 402 Color: 70

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 544
Size: 1908 Color: 252
Size: 496 Color: 97

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 549
Size: 1432 Color: 208
Size: 856 Color: 155

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 552
Size: 1758 Color: 239
Size: 512 Color: 100

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 554
Size: 1668 Color: 230
Size: 520 Color: 102

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 557
Size: 1804 Color: 245
Size: 352 Color: 51

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 558
Size: 1572 Color: 220
Size: 582 Color: 115

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14174 Color: 559
Size: 1792 Color: 243
Size: 354 Color: 54

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14182 Color: 560
Size: 1790 Color: 242
Size: 348 Color: 50

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14230 Color: 563
Size: 1610 Color: 225
Size: 480 Color: 92

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14246 Color: 564
Size: 1342 Color: 188
Size: 732 Color: 137

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14265 Color: 565
Size: 1711 Color: 234
Size: 344 Color: 48

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 570
Size: 1316 Color: 186
Size: 684 Color: 133

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 571
Size: 1772 Color: 240
Size: 224 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14326 Color: 572
Size: 1418 Color: 207
Size: 576 Color: 114

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14333 Color: 573
Size: 1491 Color: 212
Size: 496 Color: 98

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 574
Size: 1562 Color: 218
Size: 400 Color: 69

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14365 Color: 575
Size: 1631 Color: 226
Size: 324 Color: 40

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 577
Size: 1140 Color: 173
Size: 790 Color: 149

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14421 Color: 579
Size: 1583 Color: 221
Size: 316 Color: 35

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 581
Size: 1452 Color: 210
Size: 432 Color: 79

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 583
Size: 1584 Color: 222
Size: 288 Color: 21

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14463 Color: 584
Size: 1521 Color: 215
Size: 336 Color: 44

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14468 Color: 585
Size: 1152 Color: 179
Size: 700 Color: 134

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14501 Color: 587
Size: 1367 Color: 202
Size: 452 Color: 87

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 589
Size: 1662 Color: 229
Size: 126 Color: 6

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 592
Size: 952 Color: 160
Size: 788 Color: 148

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14597 Color: 593
Size: 1437 Color: 209
Size: 286 Color: 20

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 594
Size: 1146 Color: 175
Size: 558 Color: 109

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14622 Color: 595
Size: 1358 Color: 201
Size: 340 Color: 46

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14668 Color: 597
Size: 1160 Color: 183
Size: 492 Color: 96

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14670 Color: 598
Size: 1322 Color: 187
Size: 328 Color: 41

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14672 Color: 599
Size: 1008 Color: 171
Size: 640 Color: 124

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 444
Size: 5711 Color: 369
Size: 312 Color: 34

Bin 61: 1 of cap free
Amount of items: 5
Items: 
Size: 10315 Color: 446
Size: 4964 Color: 357
Size: 416 Color: 76
Size: 312 Color: 33
Size: 312 Color: 32

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 455
Size: 4967 Color: 358
Size: 272 Color: 15

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12163 Color: 480
Size: 3780 Color: 337
Size: 376 Color: 63

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12267 Color: 482
Size: 3744 Color: 334
Size: 308 Color: 31

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12647 Color: 493
Size: 3240 Color: 316
Size: 432 Color: 81

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12825 Color: 499
Size: 3152 Color: 313
Size: 342 Color: 47

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13361 Color: 517
Size: 2958 Color: 309

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13481 Color: 522
Size: 2490 Color: 287
Size: 348 Color: 49

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13563 Color: 525
Size: 2356 Color: 279
Size: 400 Color: 68

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13583 Color: 526
Size: 2560 Color: 289
Size: 176 Color: 10

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 13718 Color: 533
Size: 2601 Color: 293

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13895 Color: 541
Size: 1680 Color: 231
Size: 744 Color: 142

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13985 Color: 547
Size: 1730 Color: 237
Size: 604 Color: 120

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 14269 Color: 566
Size: 1512 Color: 213
Size: 538 Color: 106

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 14372 Color: 576
Size: 1947 Color: 255

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 14446 Color: 582
Size: 1873 Color: 250

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 586
Size: 1824 Color: 247

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 14681 Color: 600
Size: 1638 Color: 227

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 458
Size: 2690 Color: 298
Size: 2456 Color: 285

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11730 Color: 467
Size: 3244 Color: 317
Size: 1344 Color: 190

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11788 Color: 469
Size: 3682 Color: 330
Size: 848 Color: 153

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12290 Color: 483
Size: 3708 Color: 332
Size: 320 Color: 36

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 12372 Color: 487
Size: 3946 Color: 341

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12460 Color: 491
Size: 3826 Color: 340
Size: 32 Color: 0

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13434 Color: 520
Size: 2884 Color: 304

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 13912 Color: 543
Size: 2406 Color: 283

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 551
Size: 2278 Color: 274

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 14214 Color: 562
Size: 2104 Color: 264

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 569
Size: 2018 Color: 259

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 580
Size: 1894 Color: 251

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 588
Size: 1798 Color: 244

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 12002 Color: 477
Size: 4251 Color: 344
Size: 64 Color: 2

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 12965 Color: 503
Size: 3352 Color: 320

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 13177 Color: 509
Size: 1728 Color: 236
Size: 1412 Color: 206

Bin 95: 3 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 513
Size: 3061 Color: 311

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 14036 Color: 550
Size: 2281 Color: 275

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 568
Size: 2021 Color: 260

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 12860 Color: 501
Size: 3424 Color: 323
Size: 32 Color: 1

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13220 Color: 512
Size: 3096 Color: 312

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 532
Size: 2622 Color: 295

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 14272 Color: 567
Size: 2044 Color: 262

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 578
Size: 1912 Color: 253

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 14628 Color: 596
Size: 1688 Color: 233

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 13402 Color: 519
Size: 2913 Color: 306

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13646 Color: 528
Size: 2669 Color: 297

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 10998 Color: 454
Size: 5044 Color: 364
Size: 272 Color: 16

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 502
Size: 3426 Color: 324

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13094 Color: 507
Size: 3220 Color: 314

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 518
Size: 2930 Color: 307

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 545
Size: 2396 Color: 282

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14533 Color: 590
Size: 1781 Color: 241

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 439
Size: 5753 Color: 372
Size: 320 Color: 38

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 10280 Color: 442
Size: 5713 Color: 370
Size: 320 Color: 37

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 11586 Color: 466
Size: 4727 Color: 352

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 475
Size: 4336 Color: 348
Size: 96 Color: 4

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 12806 Color: 498
Size: 3506 Color: 325

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13081 Color: 506
Size: 3231 Color: 315

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 524
Size: 2760 Color: 300

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13684 Color: 531
Size: 2628 Color: 296

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 534
Size: 2588 Color: 292

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14570 Color: 591
Size: 1742 Color: 238

Bin 122: 9 of cap free
Amount of items: 5
Items: 
Size: 10372 Color: 448
Size: 5005 Color: 360
Size: 336 Color: 45
Size: 302 Color: 27
Size: 296 Color: 26

Bin 123: 10 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 471
Size: 3816 Color: 339
Size: 668 Color: 131

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 505
Size: 3294 Color: 319

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 539
Size: 2434 Color: 284

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14185 Color: 561
Size: 2125 Color: 265

Bin 127: 11 of cap free
Amount of items: 3
Items: 
Size: 9264 Color: 422
Size: 6613 Color: 387
Size: 432 Color: 80

Bin 128: 11 of cap free
Amount of items: 3
Items: 
Size: 11182 Color: 459
Size: 3555 Color: 326
Size: 1572 Color: 219

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 12560 Color: 492
Size: 3748 Color: 336

Bin 130: 13 of cap free
Amount of items: 3
Items: 
Size: 11571 Color: 465
Size: 3792 Color: 338
Size: 944 Color: 158

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 548
Size: 2320 Color: 277

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 14136 Color: 556
Size: 2170 Color: 268

Bin 133: 15 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 423
Size: 6587 Color: 386
Size: 432 Color: 78

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 445
Size: 4908 Color: 355
Size: 1088 Color: 172

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 13837 Color: 538
Size: 2467 Color: 286

Bin 136: 17 of cap free
Amount of items: 2
Items: 
Size: 14073 Color: 553
Size: 2230 Color: 273

Bin 137: 18 of cap free
Amount of items: 3
Items: 
Size: 10237 Color: 438
Size: 5741 Color: 371
Size: 324 Color: 39

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 12050 Color: 478
Size: 4252 Color: 345

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 11863 Color: 473
Size: 4438 Color: 350

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 537
Size: 2493 Color: 288

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 14134 Color: 555
Size: 2167 Color: 267

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 456
Size: 4932 Color: 356
Size: 272 Color: 14

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 11792 Color: 470
Size: 4376 Color: 349
Size: 132 Color: 8

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 523
Size: 2800 Color: 302

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 12694 Color: 494
Size: 3602 Color: 329

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 546
Size: 2367 Color: 281

Bin 147: 25 of cap free
Amount of items: 2
Items: 
Size: 12336 Color: 485
Size: 3959 Color: 342

Bin 148: 27 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 530
Size: 2621 Color: 294

Bin 149: 29 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 527
Size: 2701 Color: 299

Bin 150: 31 of cap free
Amount of items: 6
Items: 
Size: 8168 Color: 408
Size: 2797 Color: 301
Size: 2096 Color: 263
Size: 2008 Color: 258
Size: 612 Color: 123
Size: 608 Color: 122

Bin 151: 34 of cap free
Amount of items: 3
Items: 
Size: 11876 Color: 474
Size: 4282 Color: 346
Size: 128 Color: 7

Bin 152: 34 of cap free
Amount of items: 3
Items: 
Size: 11906 Color: 476
Size: 4292 Color: 347
Size: 88 Color: 3

Bin 153: 38 of cap free
Amount of items: 2
Items: 
Size: 12114 Color: 479
Size: 4168 Color: 343

Bin 154: 50 of cap free
Amount of items: 3
Items: 
Size: 11320 Color: 464
Size: 4774 Color: 354
Size: 176 Color: 9

Bin 155: 51 of cap free
Amount of items: 2
Items: 
Size: 11221 Color: 461
Size: 5048 Color: 365

Bin 156: 56 of cap free
Amount of items: 2
Items: 
Size: 11220 Color: 460
Size: 5044 Color: 363

Bin 157: 60 of cap free
Amount of items: 3
Items: 
Size: 10436 Color: 450
Size: 5536 Color: 368
Size: 288 Color: 24

Bin 158: 61 of cap free
Amount of items: 3
Items: 
Size: 11152 Color: 457
Size: 3715 Color: 333
Size: 1392 Color: 205

Bin 159: 68 of cap free
Amount of items: 3
Items: 
Size: 10649 Color: 453
Size: 5331 Color: 366
Size: 272 Color: 17

Bin 160: 83 of cap free
Amount of items: 4
Items: 
Size: 9469 Color: 435
Size: 6064 Color: 383
Size: 352 Color: 53
Size: 352 Color: 52

Bin 161: 87 of cap free
Amount of items: 3
Items: 
Size: 11264 Color: 463
Size: 4753 Color: 353
Size: 216 Color: 11

Bin 162: 111 of cap free
Amount of items: 4
Items: 
Size: 10617 Color: 452
Size: 5032 Color: 362
Size: 280 Color: 19
Size: 280 Color: 18

Bin 163: 114 of cap free
Amount of items: 7
Items: 
Size: 8165 Color: 407
Size: 1832 Color: 249
Size: 1822 Color: 246
Size: 1713 Color: 235
Size: 1380 Color: 204
Size: 648 Color: 127
Size: 646 Color: 126

Bin 164: 118 of cap free
Amount of items: 3
Items: 
Size: 9790 Color: 436
Size: 6080 Color: 384
Size: 332 Color: 43

Bin 165: 124 of cap free
Amount of items: 4
Items: 
Size: 10594 Color: 451
Size: 5026 Color: 361
Size: 288 Color: 23
Size: 288 Color: 22

Bin 166: 126 of cap free
Amount of items: 2
Items: 
Size: 10290 Color: 443
Size: 5904 Color: 382

Bin 167: 133 of cap free
Amount of items: 9
Items: 
Size: 8161 Color: 404
Size: 1356 Color: 197
Size: 1352 Color: 196
Size: 1352 Color: 195
Size: 1004 Color: 170
Size: 748 Color: 143
Size: 742 Color: 141
Size: 736 Color: 140
Size: 736 Color: 139

Bin 168: 136 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 441
Size: 3704 Color: 331
Size: 2204 Color: 270

Bin 169: 173 of cap free
Amount of items: 3
Items: 
Size: 11227 Color: 462
Size: 4664 Color: 351
Size: 256 Color: 13

Bin 170: 178 of cap free
Amount of items: 2
Items: 
Size: 9334 Color: 427
Size: 6808 Color: 401

Bin 171: 186 of cap free
Amount of items: 3
Items: 
Size: 10404 Color: 449
Size: 5442 Color: 367
Size: 288 Color: 25

Bin 172: 190 of cap free
Amount of items: 2
Items: 
Size: 9326 Color: 426
Size: 6804 Color: 400

Bin 173: 190 of cap free
Amount of items: 2
Items: 
Size: 10268 Color: 440
Size: 5862 Color: 381

Bin 174: 198 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 425
Size: 6802 Color: 399

Bin 175: 203 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 424
Size: 6801 Color: 398

Bin 176: 239 of cap free
Amount of items: 4
Items: 
Size: 8385 Color: 420
Size: 6800 Color: 397
Size: 448 Color: 86
Size: 448 Color: 85

Bin 177: 258 of cap free
Amount of items: 4
Items: 
Size: 8173 Color: 411
Size: 6717 Color: 388
Size: 588 Color: 117
Size: 584 Color: 116

Bin 178: 258 of cap free
Amount of items: 4
Items: 
Size: 8178 Color: 413
Size: 6764 Color: 390
Size: 560 Color: 111
Size: 560 Color: 110

Bin 179: 266 of cap free
Amount of items: 15
Items: 
Size: 1352 Color: 194
Size: 1344 Color: 193
Size: 1344 Color: 192
Size: 1344 Color: 191
Size: 1344 Color: 189
Size: 1172 Color: 185
Size: 1160 Color: 182
Size: 1158 Color: 181
Size: 1152 Color: 180
Size: 832 Color: 151
Size: 832 Color: 150
Size: 764 Color: 147
Size: 752 Color: 146
Size: 752 Color: 145
Size: 752 Color: 144

Bin 180: 266 of cap free
Amount of items: 4
Items: 
Size: 8176 Color: 412
Size: 6726 Color: 389
Size: 576 Color: 113
Size: 576 Color: 112

Bin 181: 280 of cap free
Amount of items: 4
Items: 
Size: 8180 Color: 414
Size: 6772 Color: 391
Size: 544 Color: 108
Size: 544 Color: 107

Bin 182: 290 of cap free
Amount of items: 4
Items: 
Size: 8184 Color: 415
Size: 6788 Color: 392
Size: 536 Color: 105
Size: 522 Color: 104

Bin 183: 295 of cap free
Amount of items: 4
Items: 
Size: 9465 Color: 434
Size: 5848 Color: 380
Size: 356 Color: 56
Size: 356 Color: 55

Bin 184: 310 of cap free
Amount of items: 3
Items: 
Size: 8417 Color: 421
Size: 7149 Color: 402
Size: 444 Color: 83

Bin 185: 315 of cap free
Amount of items: 4
Items: 
Size: 9433 Color: 433
Size: 5844 Color: 379
Size: 368 Color: 59
Size: 360 Color: 57

Bin 186: 325 of cap free
Amount of items: 4
Items: 
Size: 9417 Color: 432
Size: 5836 Color: 378
Size: 374 Color: 61
Size: 368 Color: 60

Bin 187: 328 of cap free
Amount of items: 4
Items: 
Size: 8196 Color: 416
Size: 6792 Color: 393
Size: 520 Color: 103
Size: 484 Color: 95

Bin 188: 332 of cap free
Amount of items: 4
Items: 
Size: 8250 Color: 418
Size: 6794 Color: 395
Size: 472 Color: 91
Size: 472 Color: 90

Bin 189: 342 of cap free
Amount of items: 4
Items: 
Size: 8261 Color: 419
Size: 6797 Color: 396
Size: 464 Color: 89
Size: 456 Color: 88

Bin 190: 343 of cap free
Amount of items: 4
Items: 
Size: 9336 Color: 428
Size: 5801 Color: 373
Size: 424 Color: 77
Size: 416 Color: 75

Bin 191: 343 of cap free
Amount of items: 4
Items: 
Size: 9361 Color: 430
Size: 5812 Color: 375
Size: 404 Color: 71
Size: 400 Color: 67

Bin 192: 348 of cap free
Amount of items: 4
Items: 
Size: 9348 Color: 429
Size: 5804 Color: 374
Size: 416 Color: 74
Size: 404 Color: 72

Bin 193: 348 of cap free
Amount of items: 4
Items: 
Size: 9364 Color: 431
Size: 5832 Color: 377
Size: 400 Color: 66
Size: 376 Color: 62

Bin 194: 355 of cap free
Amount of items: 4
Items: 
Size: 8212 Color: 417
Size: 6793 Color: 394
Size: 480 Color: 94
Size: 480 Color: 93

Bin 195: 372 of cap free
Amount of items: 4
Items: 
Size: 8170 Color: 410
Size: 6582 Color: 385
Size: 608 Color: 121
Size: 588 Color: 118

Bin 196: 377 of cap free
Amount of items: 2
Items: 
Size: 8169 Color: 409
Size: 7774 Color: 403

Bin 197: 447 of cap free
Amount of items: 7
Items: 
Size: 8164 Color: 406
Size: 1684 Color: 232
Size: 1604 Color: 224
Size: 1592 Color: 223
Size: 1517 Color: 214
Size: 656 Color: 129
Size: 656 Color: 128

Bin 198: 608 of cap free
Amount of items: 8
Items: 
Size: 8162 Color: 405
Size: 1378 Color: 203
Size: 1358 Color: 200
Size: 1358 Color: 199
Size: 1356 Color: 198
Size: 736 Color: 138
Size: 708 Color: 135
Size: 656 Color: 130

Bin 199: 5162 of cap free
Amount of items: 11
Items: 
Size: 1152 Color: 178
Size: 1152 Color: 177
Size: 1150 Color: 176
Size: 1142 Color: 174
Size: 998 Color: 167
Size: 992 Color: 166
Size: 992 Color: 165
Size: 992 Color: 164
Size: 884 Color: 157
Size: 856 Color: 154
Size: 848 Color: 152

Total size: 3231360
Total free space: 16320

