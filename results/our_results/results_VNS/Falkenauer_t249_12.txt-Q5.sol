Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 332 Color: 1
Size: 259 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 4
Size: 296 Color: 2
Size: 271 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 2
Size: 334 Color: 4
Size: 311 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 272 Color: 1
Size: 259 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 278 Color: 1
Size: 263 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 280 Color: 4
Size: 275 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 278 Color: 0
Size: 274 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 279 Color: 1
Size: 250 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 360 Color: 4
Size: 277 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 352 Color: 0
Size: 276 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 278 Color: 1
Size: 276 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 313 Color: 2
Size: 256 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 0
Size: 317 Color: 4
Size: 258 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 256 Color: 4
Size: 250 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 358 Color: 2
Size: 270 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 366 Color: 3
Size: 250 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 344 Color: 2
Size: 290 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 289 Color: 4
Size: 256 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 356 Color: 0
Size: 260 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 325 Color: 4
Size: 261 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 256 Color: 2
Size: 251 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 328 Color: 0
Size: 260 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 353 Color: 3
Size: 275 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 269 Color: 3
Size: 253 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 337 Color: 2
Size: 267 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 362 Color: 3
Size: 275 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 328 Color: 4
Size: 251 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 288 Color: 4
Size: 257 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 342 Color: 2
Size: 261 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 305 Color: 0
Size: 257 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 324 Color: 4
Size: 304 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 345 Color: 1
Size: 298 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 360 Color: 0
Size: 271 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 288 Color: 2
Size: 255 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 348 Color: 4
Size: 271 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 318 Color: 4
Size: 264 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 373 Color: 1
Size: 252 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 298 Color: 4
Size: 270 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 269 Color: 4
Size: 257 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 272 Color: 3
Size: 256 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 339 Color: 0
Size: 291 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 265 Color: 4
Size: 254 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 292 Color: 1
Size: 252 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 295 Color: 4
Size: 267 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 276 Color: 2
Size: 256 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 256 Color: 4
Size: 253 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 275 Color: 1
Size: 256 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 292 Color: 4
Size: 266 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 295 Color: 1
Size: 287 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 352 Color: 2
Size: 257 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 0
Size: 336 Color: 1
Size: 318 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 277 Color: 2
Size: 274 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 351 Color: 4
Size: 271 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 288 Color: 1
Size: 276 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 313 Color: 4
Size: 254 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 296 Color: 4
Size: 295 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 294 Color: 3
Size: 279 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 312 Color: 0
Size: 310 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 261 Color: 0
Size: 253 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 3
Size: 339 Color: 2
Size: 306 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 295 Color: 0
Size: 280 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 335 Color: 2
Size: 302 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 316 Color: 1
Size: 308 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 276 Color: 4
Size: 251 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 331 Color: 4
Size: 301 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 262 Color: 0
Size: 260 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 316 Color: 3
Size: 270 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 2
Size: 294 Color: 2
Size: 267 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 257 Color: 0
Size: 254 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 347 Color: 2
Size: 250 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 366 Color: 3
Size: 250 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 334 Color: 1
Size: 286 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 4
Size: 353 Color: 1
Size: 262 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 3
Size: 297 Color: 2
Size: 296 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 332 Color: 0
Size: 288 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 345 Color: 3
Size: 254 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 297 Color: 0
Size: 250 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 361 Color: 0
Size: 253 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 258 Color: 2
Size: 254 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 360 Color: 1
Size: 277 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 300 Color: 0
Size: 269 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 354 Color: 4
Size: 269 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 346 Color: 1
Size: 301 Color: 4

Total size: 83000
Total free space: 0

