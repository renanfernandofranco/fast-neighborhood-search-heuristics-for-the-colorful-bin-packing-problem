Capicity Bin: 8400
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4206 Color: 1
Size: 3694 Color: 0
Size: 500 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 0
Size: 3220 Color: 0
Size: 512 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4884 Color: 1
Size: 2932 Color: 0
Size: 584 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5312 Color: 0
Size: 2788 Color: 0
Size: 300 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 1
Size: 2284 Color: 0
Size: 512 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 1
Size: 2606 Color: 0
Size: 156 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5953 Color: 1
Size: 2209 Color: 0
Size: 238 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 0
Size: 1940 Color: 1
Size: 360 Color: 0

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 6217 Color: 1
Size: 1159 Color: 0
Size: 782 Color: 0
Size: 242 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 0
Size: 1979 Color: 1
Size: 200 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 6746 Color: 0
Size: 1486 Color: 1
Size: 144 Color: 1
Size: 24 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 0
Size: 1364 Color: 0
Size: 272 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 1
Size: 1201 Color: 1
Size: 360 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 0
Size: 964 Color: 0
Size: 480 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 1
Size: 804 Color: 0
Size: 352 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 1
Size: 863 Color: 0
Size: 290 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7218 Color: 0
Size: 862 Color: 0
Size: 320 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7249 Color: 1
Size: 795 Color: 1
Size: 356 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 0
Size: 726 Color: 1
Size: 440 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7383 Color: 1
Size: 781 Color: 0
Size: 236 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7444 Color: 0
Size: 572 Color: 1
Size: 384 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7463 Color: 0
Size: 769 Color: 1
Size: 168 Color: 0

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 7492 Color: 0
Size: 876 Color: 1
Size: 16 Color: 1
Size: 16 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7498 Color: 1
Size: 754 Color: 1
Size: 148 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 0
Size: 3491 Color: 0
Size: 688 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 1
Size: 3027 Color: 0
Size: 168 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 1
Size: 2856 Color: 0
Size: 152 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5633 Color: 0
Size: 2576 Color: 1
Size: 190 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 0
Size: 2307 Color: 1
Size: 192 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 0
Size: 1546 Color: 0
Size: 208 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 1
Size: 1604 Color: 0
Size: 166 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 0
Size: 1187 Color: 1
Size: 496 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 7063 Color: 1
Size: 688 Color: 0
Size: 648 Color: 0

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 7185 Color: 1
Size: 1214 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 7413 Color: 1
Size: 986 Color: 0

Bin 36: 2 of cap free
Amount of items: 18
Items: 
Size: 604 Color: 1
Size: 600 Color: 1
Size: 552 Color: 1
Size: 528 Color: 0
Size: 524 Color: 0
Size: 520 Color: 0
Size: 500 Color: 0
Size: 460 Color: 1
Size: 460 Color: 0
Size: 460 Color: 0
Size: 456 Color: 0
Size: 440 Color: 1
Size: 440 Color: 1
Size: 406 Color: 1
Size: 404 Color: 0
Size: 400 Color: 1
Size: 388 Color: 0
Size: 256 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 4262 Color: 1
Size: 4136 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 0
Size: 3022 Color: 0
Size: 146 Color: 1

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 5962 Color: 1
Size: 2436 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 5978 Color: 0
Size: 2236 Color: 1
Size: 184 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 1
Size: 2034 Color: 1
Size: 168 Color: 0

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 1
Size: 1762 Color: 1
Size: 406 Color: 0

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 7290 Color: 0
Size: 1108 Color: 1

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 5622 Color: 1
Size: 2509 Color: 0
Size: 266 Color: 0

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 1
Size: 1746 Color: 0
Size: 208 Color: 0

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 6945 Color: 0
Size: 1451 Color: 1

Bin 47: 4 of cap free
Amount of items: 2
Items: 
Size: 7095 Color: 0
Size: 1301 Color: 1

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 7354 Color: 0
Size: 1042 Color: 1

Bin 49: 4 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 1
Size: 930 Color: 0

Bin 50: 4 of cap free
Amount of items: 2
Items: 
Size: 7495 Color: 1
Size: 901 Color: 0

Bin 51: 5 of cap free
Amount of items: 8
Items: 
Size: 4201 Color: 0
Size: 698 Color: 1
Size: 696 Color: 1
Size: 696 Color: 1
Size: 696 Color: 1
Size: 604 Color: 0
Size: 602 Color: 0
Size: 202 Color: 0

Bin 52: 5 of cap free
Amount of items: 7
Items: 
Size: 4202 Color: 0
Size: 794 Color: 1
Size: 766 Color: 0
Size: 764 Color: 0
Size: 755 Color: 0
Size: 698 Color: 1
Size: 416 Color: 1

Bin 53: 5 of cap free
Amount of items: 4
Items: 
Size: 4209 Color: 0
Size: 3015 Color: 0
Size: 823 Color: 1
Size: 348 Color: 1

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 0
Size: 3284 Color: 1
Size: 152 Color: 1

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 6564 Color: 0
Size: 957 Color: 0
Size: 874 Color: 1

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 7269 Color: 1
Size: 1126 Color: 0

Bin 57: 5 of cap free
Amount of items: 3
Items: 
Size: 7530 Color: 0
Size: 849 Color: 1
Size: 16 Color: 0

Bin 58: 6 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 1
Size: 1344 Color: 0

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 7319 Color: 0
Size: 1075 Color: 1

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 7479 Color: 0
Size: 915 Color: 1

Bin 61: 7 of cap free
Amount of items: 3
Items: 
Size: 5751 Color: 0
Size: 2290 Color: 1
Size: 352 Color: 0

Bin 62: 7 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 0
Size: 1767 Color: 1
Size: 304 Color: 0

Bin 63: 7 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 1
Size: 1477 Color: 0

Bin 64: 7 of cap free
Amount of items: 2
Items: 
Size: 7189 Color: 1
Size: 1204 Color: 0

Bin 65: 7 of cap free
Amount of items: 2
Items: 
Size: 7450 Color: 1
Size: 943 Color: 0

Bin 66: 7 of cap free
Amount of items: 2
Items: 
Size: 7556 Color: 0
Size: 837 Color: 1

Bin 67: 8 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 0
Size: 2668 Color: 1
Size: 240 Color: 0

Bin 68: 8 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 1
Size: 1821 Color: 0
Size: 132 Color: 1

Bin 69: 9 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 1
Size: 1461 Color: 0

Bin 70: 9 of cap free
Amount of items: 2
Items: 
Size: 7076 Color: 0
Size: 1315 Color: 1

Bin 71: 9 of cap free
Amount of items: 2
Items: 
Size: 7202 Color: 1
Size: 1189 Color: 0

Bin 72: 9 of cap free
Amount of items: 3
Items: 
Size: 7482 Color: 1
Size: 877 Color: 0
Size: 32 Color: 1

Bin 73: 10 of cap free
Amount of items: 2
Items: 
Size: 6306 Color: 1
Size: 2084 Color: 0

Bin 74: 10 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 0
Size: 1802 Color: 1
Size: 112 Color: 1

Bin 75: 10 of cap free
Amount of items: 3
Items: 
Size: 6530 Color: 1
Size: 1534 Color: 0
Size: 326 Color: 1

Bin 76: 10 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 0
Size: 1266 Color: 1

Bin 77: 12 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 0
Size: 836 Color: 1
Size: 834 Color: 1

Bin 78: 12 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 0
Size: 1331 Color: 1
Size: 96 Color: 1

Bin 79: 16 of cap free
Amount of items: 2
Items: 
Size: 4468 Color: 1
Size: 3916 Color: 0

Bin 80: 16 of cap free
Amount of items: 2
Items: 
Size: 6258 Color: 1
Size: 2126 Color: 0

Bin 81: 17 of cap free
Amount of items: 2
Items: 
Size: 6649 Color: 0
Size: 1734 Color: 1

Bin 82: 17 of cap free
Amount of items: 2
Items: 
Size: 7370 Color: 0
Size: 1013 Color: 1

Bin 83: 19 of cap free
Amount of items: 2
Items: 
Size: 7349 Color: 0
Size: 1032 Color: 1

Bin 84: 20 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 1
Size: 3648 Color: 0
Size: 528 Color: 1

Bin 85: 21 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 1
Size: 1635 Color: 0

Bin 86: 22 of cap free
Amount of items: 2
Items: 
Size: 6076 Color: 1
Size: 2302 Color: 0

Bin 87: 23 of cap free
Amount of items: 2
Items: 
Size: 5249 Color: 1
Size: 3128 Color: 0

Bin 88: 23 of cap free
Amount of items: 2
Items: 
Size: 7253 Color: 1
Size: 1124 Color: 0

Bin 89: 24 of cap free
Amount of items: 2
Items: 
Size: 7150 Color: 1
Size: 1226 Color: 0

Bin 90: 24 of cap free
Amount of items: 2
Items: 
Size: 7365 Color: 1
Size: 1011 Color: 0

Bin 91: 27 of cap free
Amount of items: 2
Items: 
Size: 7447 Color: 1
Size: 926 Color: 0

Bin 92: 28 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 0
Size: 1810 Color: 1

Bin 93: 29 of cap free
Amount of items: 2
Items: 
Size: 7303 Color: 1
Size: 1068 Color: 0

Bin 94: 34 of cap free
Amount of items: 2
Items: 
Size: 5724 Color: 1
Size: 2642 Color: 0

Bin 95: 35 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 1
Size: 1562 Color: 0

Bin 96: 35 of cap free
Amount of items: 2
Items: 
Size: 7404 Color: 0
Size: 961 Color: 1

Bin 97: 37 of cap free
Amount of items: 2
Items: 
Size: 7402 Color: 0
Size: 961 Color: 1

Bin 98: 39 of cap free
Amount of items: 3
Items: 
Size: 4213 Color: 0
Size: 3450 Color: 1
Size: 698 Color: 0

Bin 99: 39 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 0
Size: 3301 Color: 1

Bin 100: 39 of cap free
Amount of items: 2
Items: 
Size: 6730 Color: 1
Size: 1631 Color: 0

Bin 101: 39 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 0
Size: 1061 Color: 1

Bin 102: 40 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 0
Size: 1537 Color: 1

Bin 103: 41 of cap free
Amount of items: 2
Items: 
Size: 6977 Color: 0
Size: 1382 Color: 1

Bin 104: 41 of cap free
Amount of items: 2
Items: 
Size: 7252 Color: 1
Size: 1107 Color: 0

Bin 105: 42 of cap free
Amount of items: 2
Items: 
Size: 7356 Color: 1
Size: 1002 Color: 0

Bin 106: 50 of cap free
Amount of items: 4
Items: 
Size: 4205 Color: 0
Size: 2869 Color: 0
Size: 812 Color: 1
Size: 464 Color: 1

Bin 107: 50 of cap free
Amount of items: 2
Items: 
Size: 6546 Color: 0
Size: 1804 Color: 1

Bin 108: 50 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 1
Size: 1404 Color: 0

Bin 109: 60 of cap free
Amount of items: 2
Items: 
Size: 7127 Color: 1
Size: 1213 Color: 0

Bin 110: 67 of cap free
Amount of items: 2
Items: 
Size: 6290 Color: 0
Size: 2043 Color: 1

Bin 111: 68 of cap free
Amount of items: 2
Items: 
Size: 6800 Color: 0
Size: 1532 Color: 1

Bin 112: 70 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 1
Size: 1621 Color: 0
Size: 48 Color: 0

Bin 113: 76 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 1
Size: 1394 Color: 0
Size: 48 Color: 0

Bin 114: 79 of cap free
Amount of items: 2
Items: 
Size: 7397 Color: 0
Size: 924 Color: 1

Bin 115: 82 of cap free
Amount of items: 2
Items: 
Size: 6281 Color: 1
Size: 2037 Color: 0

Bin 116: 83 of cap free
Amount of items: 2
Items: 
Size: 7073 Color: 0
Size: 1244 Color: 1

Bin 117: 88 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 1
Size: 974 Color: 0

Bin 118: 89 of cap free
Amount of items: 2
Items: 
Size: 7111 Color: 1
Size: 1200 Color: 0

Bin 119: 92 of cap free
Amount of items: 31
Items: 
Size: 376 Color: 1
Size: 362 Color: 0
Size: 348 Color: 1
Size: 344 Color: 0
Size: 326 Color: 1
Size: 324 Color: 0
Size: 308 Color: 1
Size: 308 Color: 1
Size: 304 Color: 1
Size: 294 Color: 1
Size: 288 Color: 0
Size: 280 Color: 1
Size: 280 Color: 1
Size: 280 Color: 0
Size: 276 Color: 0
Size: 272 Color: 1
Size: 262 Color: 1
Size: 260 Color: 0
Size: 252 Color: 0
Size: 244 Color: 1
Size: 240 Color: 1
Size: 240 Color: 0
Size: 224 Color: 1
Size: 220 Color: 1
Size: 218 Color: 0
Size: 214 Color: 0
Size: 212 Color: 0
Size: 192 Color: 0
Size: 190 Color: 0
Size: 188 Color: 0
Size: 182 Color: 0

Bin 120: 92 of cap free
Amount of items: 2
Items: 
Size: 5274 Color: 1
Size: 3034 Color: 0

Bin 121: 99 of cap free
Amount of items: 2
Items: 
Size: 5969 Color: 1
Size: 2332 Color: 0

Bin 122: 100 of cap free
Amount of items: 2
Items: 
Size: 6898 Color: 1
Size: 1402 Color: 0

Bin 123: 101 of cap free
Amount of items: 2
Items: 
Size: 6455 Color: 1
Size: 1844 Color: 0

Bin 124: 108 of cap free
Amount of items: 2
Items: 
Size: 4799 Color: 0
Size: 3493 Color: 1

Bin 125: 115 of cap free
Amount of items: 2
Items: 
Size: 4783 Color: 1
Size: 3502 Color: 0

Bin 126: 117 of cap free
Amount of items: 2
Items: 
Size: 6242 Color: 0
Size: 2041 Color: 1

Bin 127: 119 of cap free
Amount of items: 2
Items: 
Size: 5654 Color: 1
Size: 2627 Color: 0

Bin 128: 125 of cap free
Amount of items: 2
Items: 
Size: 5957 Color: 0
Size: 2318 Color: 1

Bin 129: 130 of cap free
Amount of items: 2
Items: 
Size: 4769 Color: 1
Size: 3501 Color: 0

Bin 130: 134 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 1
Size: 2022 Color: 0

Bin 131: 138 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 0
Size: 3484 Color: 1

Bin 132: 141 of cap free
Amount of items: 2
Items: 
Size: 4762 Color: 1
Size: 3497 Color: 0

Bin 133: 4826 of cap free
Amount of items: 21
Items: 
Size: 200 Color: 1
Size: 200 Color: 1
Size: 196 Color: 1
Size: 196 Color: 1
Size: 184 Color: 1
Size: 180 Color: 1
Size: 176 Color: 1
Size: 176 Color: 0
Size: 174 Color: 1
Size: 172 Color: 1
Size: 172 Color: 0
Size: 164 Color: 0
Size: 164 Color: 0
Size: 160 Color: 1
Size: 158 Color: 0
Size: 156 Color: 0
Size: 152 Color: 0
Size: 152 Color: 0
Size: 150 Color: 0
Size: 148 Color: 0
Size: 144 Color: 0

Total size: 1108800
Total free space: 8400

