Capicity Bin: 16320
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 13
Items: 
Size: 8164 Color: 2
Size: 856 Color: 6
Size: 856 Color: 5
Size: 752 Color: 6
Size: 732 Color: 9
Size: 708 Color: 19
Size: 684 Color: 17
Size: 674 Color: 5
Size: 640 Color: 13
Size: 576 Color: 18
Size: 576 Color: 9
Size: 558 Color: 1
Size: 544 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8176 Color: 8
Size: 6764 Color: 10
Size: 1380 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 16
Size: 6788 Color: 3
Size: 1352 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8184 Color: 18
Size: 6792 Color: 9
Size: 1344 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9334 Color: 14
Size: 6582 Color: 15
Size: 404 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9790 Color: 9
Size: 5830 Color: 9
Size: 700 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10315 Color: 7
Size: 5005 Color: 1
Size: 1000 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10649 Color: 5
Size: 5331 Color: 14
Size: 340 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11152 Color: 8
Size: 4336 Color: 8
Size: 832 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 18
Size: 3704 Color: 6
Size: 736 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11906 Color: 17
Size: 3746 Color: 8
Size: 668 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12163 Color: 3
Size: 3555 Color: 14
Size: 602 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12210 Color: 3
Size: 3708 Color: 3
Size: 402 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12290 Color: 16
Size: 3744 Color: 13
Size: 286 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 7
Size: 3294 Color: 3
Size: 656 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 3
Size: 3292 Color: 7
Size: 656 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 2
Size: 3562 Color: 0
Size: 330 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 9
Size: 2204 Color: 2
Size: 1342 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 18
Size: 2908 Color: 11
Size: 436 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2320 Color: 15
Size: 984 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13172 Color: 6
Size: 3022 Color: 3
Size: 126 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13178 Color: 11
Size: 2190 Color: 7
Size: 952 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 6
Size: 2002 Color: 4
Size: 984 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 1
Size: 2456 Color: 6
Size: 480 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13481 Color: 16
Size: 1517 Color: 10
Size: 1322 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13653 Color: 19
Size: 2351 Color: 11
Size: 316 Color: 11

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 3
Size: 2560 Color: 17
Size: 88 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13718 Color: 8
Size: 1452 Color: 7
Size: 1150 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13724 Color: 9
Size: 2164 Color: 11
Size: 432 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13771 Color: 9
Size: 1549 Color: 13
Size: 1000 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 1
Size: 1432 Color: 12
Size: 1088 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13912 Color: 10
Size: 1912 Color: 8
Size: 496 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13985 Color: 18
Size: 1583 Color: 1
Size: 752 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 11
Size: 1358 Color: 0
Size: 976 Color: 7

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 12
Size: 1832 Color: 10
Size: 456 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 1
Size: 1604 Color: 4
Size: 584 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14134 Color: 3
Size: 1668 Color: 7
Size: 518 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14182 Color: 4
Size: 1140 Color: 7
Size: 998 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14185 Color: 3
Size: 1781 Color: 18
Size: 354 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14214 Color: 12
Size: 1758 Color: 15
Size: 348 Color: 5

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14272 Color: 11
Size: 1680 Color: 8
Size: 368 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 13
Size: 1008 Color: 11
Size: 992 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 13
Size: 1004 Color: 10
Size: 992 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14326 Color: 16
Size: 1662 Color: 13
Size: 332 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14333 Color: 10
Size: 1491 Color: 8
Size: 496 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 4
Size: 1142 Color: 17
Size: 788 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 7
Size: 1172 Color: 3
Size: 744 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 7
Size: 1352 Color: 18
Size: 520 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14463 Color: 18
Size: 1521 Color: 6
Size: 336 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14468 Color: 17
Size: 1548 Color: 9
Size: 304 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 15
Size: 1356 Color: 16
Size: 444 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1572 Color: 17
Size: 216 Color: 19

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14570 Color: 4
Size: 1462 Color: 8
Size: 288 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14628 Color: 5
Size: 1344 Color: 5
Size: 348 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14668 Color: 18
Size: 1344 Color: 19
Size: 308 Color: 1

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 15
Size: 4727 Color: 0
Size: 1352 Color: 14

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 13
Size: 5741 Color: 0
Size: 302 Color: 17

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 14
Size: 4967 Color: 7
Size: 272 Color: 6

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12267 Color: 17
Size: 3780 Color: 15
Size: 272 Color: 0

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12647 Color: 11
Size: 2930 Color: 5
Size: 742 Color: 7

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13895 Color: 4
Size: 1592 Color: 4
Size: 832 Color: 17

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 2
Size: 2302 Color: 5
Size: 120 Color: 10

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14265 Color: 16
Size: 1798 Color: 8
Size: 256 Color: 18

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 14372 Color: 19
Size: 1947 Color: 5

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14421 Color: 4
Size: 1418 Color: 8
Size: 480 Color: 16

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14446 Color: 12
Size: 1873 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 14533 Color: 8
Size: 1412 Color: 11
Size: 374 Color: 13

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14681 Color: 9
Size: 1638 Color: 14

Bin 69: 2 of cap free
Amount of items: 34
Items: 
Size: 656 Color: 2
Size: 612 Color: 17
Size: 608 Color: 16
Size: 608 Color: 7
Size: 588 Color: 9
Size: 582 Color: 17
Size: 576 Color: 6
Size: 560 Color: 4
Size: 544 Color: 16
Size: 538 Color: 13
Size: 536 Color: 13
Size: 520 Color: 0
Size: 512 Color: 11
Size: 498 Color: 4
Size: 492 Color: 7
Size: 484 Color: 1
Size: 472 Color: 3
Size: 464 Color: 2
Size: 448 Color: 15
Size: 448 Color: 14
Size: 444 Color: 11
Size: 432 Color: 18
Size: 432 Color: 13
Size: 432 Color: 8
Size: 424 Color: 13
Size: 416 Color: 15
Size: 416 Color: 11
Size: 416 Color: 11
Size: 416 Color: 8
Size: 400 Color: 12
Size: 360 Color: 12
Size: 356 Color: 1
Size: 324 Color: 18
Size: 304 Color: 0

Bin 70: 2 of cap free
Amount of items: 4
Items: 
Size: 8162 Color: 17
Size: 6794 Color: 15
Size: 716 Color: 17
Size: 646 Color: 18

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 10998 Color: 0
Size: 4976 Color: 4
Size: 344 Color: 5

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 11788 Color: 12
Size: 3682 Color: 2
Size: 848 Color: 6

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 11
Size: 2576 Color: 7
Size: 522 Color: 0

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 13361 Color: 14
Size: 2601 Color: 6
Size: 356 Color: 8

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13434 Color: 18
Size: 2884 Color: 13

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 11
Size: 2434 Color: 8
Size: 384 Color: 18

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 12
Size: 2278 Color: 14

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 1
Size: 2018 Color: 14

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 10
Size: 1894 Color: 11

Bin 80: 3 of cap free
Amount of items: 9
Items: 
Size: 8165 Color: 5
Size: 1356 Color: 4
Size: 1344 Color: 7
Size: 1160 Color: 10
Size: 1158 Color: 9
Size: 1152 Color: 17
Size: 790 Color: 13
Size: 604 Color: 3
Size: 588 Color: 18

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 10237 Color: 8
Size: 6080 Color: 19

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 10617 Color: 5
Size: 4964 Color: 9
Size: 736 Color: 15

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 11863 Color: 11
Size: 2797 Color: 14
Size: 1657 Color: 14

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 12965 Color: 6
Size: 3352 Color: 4

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 13081 Color: 7
Size: 2872 Color: 0
Size: 364 Color: 6

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 11
Size: 3061 Color: 10

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 14036 Color: 6
Size: 2281 Color: 13

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 14269 Color: 9
Size: 1728 Color: 8
Size: 320 Color: 15

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 10
Size: 2021 Color: 1

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 14
Size: 1822 Color: 3

Bin 91: 4 of cap free
Amount of items: 5
Items: 
Size: 8196 Color: 0
Size: 6772 Color: 4
Size: 764 Color: 11
Size: 304 Color: 3
Size: 280 Color: 16

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 8385 Color: 5
Size: 6587 Color: 1
Size: 1344 Color: 17

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 8
Size: 5536 Color: 12
Size: 472 Color: 19

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 15
Size: 4932 Color: 8
Size: 288 Color: 19

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 14
Size: 2588 Color: 2
Size: 176 Color: 6

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 13563 Color: 19
Size: 2621 Color: 7
Size: 132 Color: 18

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 15
Size: 2622 Color: 1

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 11221 Color: 5
Size: 4774 Color: 15
Size: 320 Color: 13

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 11571 Color: 12
Size: 4292 Color: 0
Size: 452 Color: 17

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 11752 Color: 11
Size: 3715 Color: 11
Size: 848 Color: 12

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 12825 Color: 17
Size: 3362 Color: 1
Size: 128 Color: 8

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 13402 Color: 18
Size: 2913 Color: 9

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 13646 Color: 1
Size: 2669 Color: 18

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 14230 Color: 8
Size: 2021 Color: 14
Size: 64 Color: 10

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13094 Color: 1
Size: 3220 Color: 3

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 17
Size: 2396 Color: 7

Bin 107: 7 of cap free
Amount of items: 4
Items: 
Size: 9465 Color: 16
Size: 5804 Color: 7
Size: 748 Color: 17
Size: 296 Color: 9

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 4
Size: 4753 Color: 16
Size: 388 Color: 18

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 8250 Color: 1
Size: 7774 Color: 11
Size: 288 Color: 10

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 11264 Color: 2
Size: 5048 Color: 13

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 16
Size: 3424 Color: 2

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13684 Color: 12
Size: 2628 Color: 0

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 14670 Color: 17
Size: 1610 Color: 12
Size: 32 Color: 16

Bin 114: 9 of cap free
Amount of items: 6
Items: 
Size: 8178 Color: 0
Size: 2167 Color: 17
Size: 1824 Color: 14
Size: 1792 Color: 0
Size: 1790 Color: 12
Size: 560 Color: 2

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 11792 Color: 13
Size: 3568 Color: 15
Size: 950 Color: 7

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 15
Size: 1730 Color: 3

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 14597 Color: 19
Size: 1713 Color: 14

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 14622 Color: 19
Size: 1688 Color: 13

Bin 119: 11 of cap free
Amount of items: 3
Items: 
Size: 9348 Color: 0
Size: 5801 Color: 12
Size: 1160 Color: 2

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 10404 Color: 1
Size: 5904 Color: 15

Bin 121: 12 of cap free
Amount of items: 3
Items: 
Size: 11320 Color: 12
Size: 3426 Color: 0
Size: 1562 Color: 5

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 12560 Color: 4
Size: 3748 Color: 12

Bin 123: 13 of cap free
Amount of items: 4
Items: 
Size: 8173 Color: 11
Size: 4908 Color: 19
Size: 2490 Color: 18
Size: 736 Color: 11

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 10268 Color: 0
Size: 5711 Color: 2
Size: 328 Color: 10

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 10594 Color: 0
Size: 5713 Color: 15

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 14136 Color: 2
Size: 2170 Color: 19

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 14501 Color: 13
Size: 1804 Color: 12

Bin 128: 16 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 18
Size: 3244 Color: 0
Size: 224 Color: 18

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 13837 Color: 18
Size: 2467 Color: 7

Bin 130: 17 of cap free
Amount of items: 3
Items: 
Size: 11876 Color: 0
Size: 4251 Color: 9
Size: 176 Color: 8

Bin 131: 17 of cap free
Amount of items: 3
Items: 
Size: 13201 Color: 13
Size: 2760 Color: 15
Size: 342 Color: 2

Bin 132: 17 of cap free
Amount of items: 2
Items: 
Size: 14073 Color: 1
Size: 2230 Color: 9

Bin 133: 17 of cap free
Amount of items: 2
Items: 
Size: 14672 Color: 2
Size: 1631 Color: 1

Bin 134: 18 of cap free
Amount of items: 8
Items: 
Size: 8168 Color: 0
Size: 1437 Color: 10
Size: 1378 Color: 0
Size: 1367 Color: 16
Size: 1358 Color: 4
Size: 1358 Color: 3
Size: 884 Color: 2
Size: 352 Color: 6

Bin 135: 18 of cap free
Amount of items: 3
Items: 
Size: 9264 Color: 2
Size: 6726 Color: 8
Size: 312 Color: 5

Bin 136: 18 of cap free
Amount of items: 3
Items: 
Size: 11730 Color: 2
Size: 2568 Color: 16
Size: 2004 Color: 8

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 12050 Color: 3
Size: 4252 Color: 2

Bin 138: 19 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 13
Size: 2493 Color: 3

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 14365 Color: 16
Size: 1936 Color: 13

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 13
Size: 1684 Color: 15

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 10436 Color: 13
Size: 5862 Color: 16

Bin 142: 22 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 10
Size: 4376 Color: 1
Size: 96 Color: 7

Bin 143: 24 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 19
Size: 3506 Color: 5

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 13890 Color: 18
Size: 2406 Color: 14

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 4
Size: 2367 Color: 18

Bin 146: 24 of cap free
Amount of items: 3
Items: 
Size: 14436 Color: 10
Size: 1828 Color: 15
Size: 32 Color: 0

Bin 147: 25 of cap free
Amount of items: 2
Items: 
Size: 12336 Color: 15
Size: 3959 Color: 3

Bin 148: 25 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 0
Size: 3231 Color: 10
Size: 288 Color: 8

Bin 149: 26 of cap free
Amount of items: 3
Items: 
Size: 8261 Color: 2
Size: 6717 Color: 16
Size: 1316 Color: 12

Bin 150: 29 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 9
Size: 2701 Color: 11

Bin 151: 30 of cap free
Amount of items: 2
Items: 
Size: 14246 Color: 5
Size: 2044 Color: 17

Bin 152: 31 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 12
Size: 2125 Color: 15

Bin 153: 33 of cap free
Amount of items: 2
Items: 
Size: 13329 Color: 6
Size: 2958 Color: 3

Bin 154: 34 of cap free
Amount of items: 2
Items: 
Size: 12460 Color: 15
Size: 3826 Color: 1

Bin 155: 36 of cap free
Amount of items: 2
Items: 
Size: 12002 Color: 6
Size: 4282 Color: 2

Bin 156: 38 of cap free
Amount of items: 2
Items: 
Size: 12114 Color: 7
Size: 4168 Color: 6

Bin 157: 42 of cap free
Amount of items: 2
Items: 
Size: 14174 Color: 15
Size: 2104 Color: 14

Bin 158: 46 of cap free
Amount of items: 3
Items: 
Size: 10352 Color: 19
Size: 5442 Color: 7
Size: 480 Color: 2

Bin 159: 46 of cap free
Amount of items: 3
Items: 
Size: 12694 Color: 5
Size: 2008 Color: 18
Size: 1572 Color: 15

Bin 160: 47 of cap free
Amount of items: 3
Items: 
Size: 9469 Color: 11
Size: 5812 Color: 2
Size: 992 Color: 3

Bin 161: 47 of cap free
Amount of items: 2
Items: 
Size: 13177 Color: 18
Size: 3096 Color: 6

Bin 162: 47 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 1
Size: 2690 Color: 14

Bin 163: 47 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 16
Size: 2223 Color: 3

Bin 164: 48 of cap free
Amount of items: 2
Items: 
Size: 13916 Color: 15
Size: 2356 Color: 0

Bin 165: 49 of cap free
Amount of items: 2
Items: 
Size: 11227 Color: 15
Size: 5044 Color: 7

Bin 166: 50 of cap free
Amount of items: 3
Items: 
Size: 12806 Color: 10
Size: 3152 Color: 13
Size: 312 Color: 15

Bin 167: 50 of cap free
Amount of items: 2
Items: 
Size: 13328 Color: 18
Size: 2942 Color: 6

Bin 168: 54 of cap free
Amount of items: 2
Items: 
Size: 11828 Color: 18
Size: 4438 Color: 9

Bin 169: 54 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 4
Size: 1908 Color: 16

Bin 170: 56 of cap free
Amount of items: 2
Items: 
Size: 11220 Color: 19
Size: 5044 Color: 14

Bin 171: 58 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 15
Size: 2096 Color: 16

Bin 172: 61 of cap free
Amount of items: 2
Items: 
Size: 12443 Color: 10
Size: 3816 Color: 7

Bin 173: 62 of cap free
Amount of items: 2
Items: 
Size: 12312 Color: 8
Size: 3946 Color: 9

Bin 174: 68 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 5
Size: 2800 Color: 16

Bin 175: 70 of cap free
Amount of items: 2
Items: 
Size: 11586 Color: 16
Size: 4664 Color: 9

Bin 176: 77 of cap free
Amount of items: 10
Items: 
Size: 8161 Color: 5
Size: 1152 Color: 15
Size: 1152 Color: 15
Size: 1152 Color: 6
Size: 1146 Color: 3
Size: 944 Color: 4
Size: 864 Color: 9
Size: 752 Color: 2
Size: 640 Color: 8
Size: 280 Color: 11

Bin 177: 78 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 18
Size: 2366 Color: 15

Bin 178: 79 of cap free
Amount of items: 2
Items: 
Size: 9433 Color: 5
Size: 6808 Color: 16

Bin 179: 81 of cap free
Amount of items: 2
Items: 
Size: 12860 Color: 4
Size: 3379 Color: 11

Bin 180: 85 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 11
Size: 6613 Color: 6
Size: 336 Color: 2

Bin 181: 88 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 6
Size: 3792 Color: 2

Bin 182: 96 of cap free
Amount of items: 2
Items: 
Size: 10160 Color: 13
Size: 6064 Color: 10

Bin 183: 99 of cap free
Amount of items: 2
Items: 
Size: 9417 Color: 19
Size: 6804 Color: 12

Bin 184: 100 of cap free
Amount of items: 2
Items: 
Size: 10372 Color: 18
Size: 5848 Color: 9

Bin 185: 106 of cap free
Amount of items: 3
Items: 
Size: 8417 Color: 10
Size: 7149 Color: 5
Size: 648 Color: 13

Bin 186: 106 of cap free
Amount of items: 2
Items: 
Size: 11182 Color: 8
Size: 5032 Color: 16

Bin 187: 139 of cap free
Amount of items: 3
Items: 
Size: 8212 Color: 16
Size: 5753 Color: 6
Size: 2216 Color: 13

Bin 188: 144 of cap free
Amount of items: 4
Items: 
Size: 8170 Color: 4
Size: 3602 Color: 15
Size: 3240 Color: 0
Size: 1164 Color: 5

Bin 189: 154 of cap free
Amount of items: 2
Items: 
Size: 9364 Color: 11
Size: 6802 Color: 10

Bin 190: 158 of cap free
Amount of items: 2
Items: 
Size: 9361 Color: 17
Size: 6801 Color: 19

Bin 191: 180 of cap free
Amount of items: 6
Items: 
Size: 8169 Color: 12
Size: 1772 Color: 10
Size: 1711 Color: 5
Size: 1584 Color: 14
Size: 1512 Color: 6
Size: 1392 Color: 13

Bin 192: 180 of cap free
Amount of items: 2
Items: 
Size: 10296 Color: 12
Size: 5844 Color: 5

Bin 193: 194 of cap free
Amount of items: 2
Items: 
Size: 9326 Color: 0
Size: 6800 Color: 15

Bin 194: 194 of cap free
Amount of items: 2
Items: 
Size: 10290 Color: 8
Size: 5836 Color: 11

Bin 195: 203 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 14
Size: 6797 Color: 10

Bin 196: 208 of cap free
Amount of items: 2
Items: 
Size: 10280 Color: 6
Size: 5832 Color: 4

Bin 197: 211 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 15
Size: 6793 Color: 19

Bin 198: 216 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 8
Size: 5026 Color: 14
Size: 1742 Color: 13

Bin 199: 11104 of cap free
Amount of items: 15
Items: 
Size: 404 Color: 15
Size: 400 Color: 17
Size: 400 Color: 8
Size: 400 Color: 5
Size: 376 Color: 17
Size: 376 Color: 6
Size: 368 Color: 19
Size: 352 Color: 16
Size: 352 Color: 14
Size: 324 Color: 2
Size: 320 Color: 16
Size: 312 Color: 12
Size: 288 Color: 1
Size: 272 Color: 18
Size: 272 Color: 18

Total size: 3231360
Total free space: 16320

