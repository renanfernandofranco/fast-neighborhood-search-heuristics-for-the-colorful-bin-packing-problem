Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 245
Size: 253 Color: 15
Size: 252 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 191
Size: 304 Color: 127
Size: 287 Color: 101

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 218
Size: 284 Color: 93
Size: 271 Color: 69

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 242
Size: 257 Color: 30
Size: 256 Color: 28

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 181
Size: 324 Color: 142
Size: 287 Color: 100

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 176
Size: 352 Color: 158
Size: 266 Color: 58

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 238
Size: 263 Color: 50
Size: 260 Color: 41

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 180
Size: 362 Color: 165
Size: 253 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 173
Size: 365 Color: 167
Size: 255 Color: 23

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 184
Size: 343 Color: 155
Size: 265 Color: 56

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 244
Size: 256 Color: 25
Size: 251 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 214
Size: 309 Color: 133
Size: 253 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 219
Size: 300 Color: 122
Size: 254 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 171
Size: 341 Color: 152
Size: 280 Color: 88

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 234
Size: 271 Color: 68
Size: 258 Color: 36

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 215
Size: 294 Color: 112
Size: 267 Color: 61

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 187
Size: 330 Color: 146
Size: 275 Color: 78

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 223
Size: 294 Color: 111
Size: 254 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 213
Size: 287 Color: 98
Size: 276 Color: 80

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 228
Size: 285 Color: 95
Size: 254 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 199
Size: 311 Color: 135
Size: 273 Color: 72

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 217
Size: 282 Color: 91
Size: 276 Color: 81

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 174
Size: 342 Color: 154
Size: 277 Color: 83

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 169
Size: 337 Color: 151
Size: 286 Color: 97

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 189
Size: 328 Color: 144
Size: 269 Color: 67

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 177
Size: 357 Color: 161
Size: 261 Color: 43

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 227
Size: 276 Color: 79
Size: 264 Color: 52

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 160
Size: 354 Color: 159
Size: 290 Color: 105

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 200
Size: 329 Color: 145
Size: 254 Color: 21

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 203
Size: 317 Color: 138
Size: 259 Color: 37

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 230
Size: 273 Color: 75
Size: 261 Color: 44

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 241
Size: 263 Color: 47
Size: 255 Color: 24

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 246
Size: 254 Color: 22
Size: 251 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 210
Size: 308 Color: 131
Size: 258 Color: 34

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 236
Size: 274 Color: 76
Size: 250 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 194
Size: 304 Color: 129
Size: 285 Color: 96

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 172
Size: 351 Color: 156
Size: 269 Color: 66

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 204
Size: 307 Color: 130
Size: 268 Color: 64

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 197
Size: 324 Color: 143
Size: 262 Color: 46

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 297 Color: 118
Size: 272 Color: 71
Size: 431 Color: 207

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 235
Size: 264 Color: 53
Size: 263 Color: 48

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 192
Size: 303 Color: 126
Size: 288 Color: 102

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 237
Size: 266 Color: 59
Size: 258 Color: 35

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 202
Size: 290 Color: 104
Size: 290 Color: 103

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 216
Size: 283 Color: 92
Size: 276 Color: 82

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 240
Size: 261 Color: 45
Size: 258 Color: 33

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 205
Size: 321 Color: 141
Size: 250 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 182
Size: 308 Color: 132
Size: 303 Color: 125

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 224
Size: 277 Color: 84
Size: 271 Color: 70

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 206
Size: 316 Color: 137
Size: 254 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 195
Size: 294 Color: 110
Size: 294 Color: 113

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 243
Size: 257 Color: 29
Size: 253 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 183
Size: 312 Color: 136
Size: 298 Color: 120

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 185
Size: 310 Color: 134
Size: 298 Color: 119

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 198
Size: 336 Color: 150
Size: 250 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 226
Size: 293 Color: 107
Size: 250 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 179
Size: 331 Color: 147
Size: 287 Color: 99

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 247
Size: 254 Color: 20
Size: 250 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 209
Size: 301 Color: 123
Size: 267 Color: 62

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 221
Size: 293 Color: 109
Size: 259 Color: 38

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 220
Size: 279 Color: 87
Size: 274 Color: 77

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 188
Size: 318 Color: 140
Size: 285 Color: 94

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 186
Size: 333 Color: 149
Size: 273 Color: 74

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 168
Size: 365 Color: 166
Size: 259 Color: 39

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 193
Size: 296 Color: 116
Size: 293 Color: 108

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 208
Size: 317 Color: 139
Size: 251 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 233
Size: 267 Color: 63
Size: 263 Color: 51

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 239
Size: 264 Color: 55
Size: 256 Color: 27

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 222
Size: 296 Color: 117
Size: 256 Color: 26

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 170
Size: 361 Color: 163
Size: 261 Color: 42

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 248
Size: 252 Color: 11
Size: 250 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 211
Size: 302 Color: 124
Size: 263 Color: 49

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 232
Size: 281 Color: 90
Size: 252 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 225
Size: 280 Color: 89
Size: 266 Color: 57

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 178
Size: 360 Color: 162
Size: 258 Color: 32

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 164
Size: 341 Color: 153
Size: 298 Color: 121

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 175
Size: 352 Color: 157
Size: 267 Color: 60

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 190
Size: 332 Color: 148
Size: 264 Color: 54

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 196
Size: 295 Color: 115
Size: 291 Color: 106

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 201
Size: 304 Color: 128
Size: 277 Color: 85

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 212
Size: 294 Color: 114
Size: 269 Color: 65

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 229
Size: 279 Color: 86
Size: 258 Color: 31

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 231
Size: 273 Color: 73
Size: 260 Color: 40

Total size: 83000
Total free space: 0

