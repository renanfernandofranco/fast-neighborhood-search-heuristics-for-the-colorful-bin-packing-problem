Capicity Bin: 13904
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 6954 Color: 4
Size: 1152 Color: 1
Size: 1120 Color: 3
Size: 1120 Color: 1
Size: 1014 Color: 3
Size: 720 Color: 0
Size: 720 Color: 0
Size: 640 Color: 1
Size: 464 Color: 2

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 6958 Color: 1
Size: 1431 Color: 1
Size: 1430 Color: 2
Size: 1278 Color: 0
Size: 1237 Color: 4
Size: 1214 Color: 2
Size: 356 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 2
Size: 4792 Color: 0
Size: 944 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8574 Color: 4
Size: 4996 Color: 0
Size: 334 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8824 Color: 1
Size: 4488 Color: 0
Size: 592 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 4
Size: 4040 Color: 0
Size: 528 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 0
Size: 3603 Color: 3
Size: 720 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9643 Color: 2
Size: 3903 Color: 3
Size: 358 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 9702 Color: 1
Size: 3918 Color: 3
Size: 284 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 9848 Color: 0
Size: 3816 Color: 4
Size: 240 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 2
Size: 3608 Color: 3
Size: 428 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 9886 Color: 2
Size: 3730 Color: 0
Size: 288 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 9894 Color: 2
Size: 3342 Color: 0
Size: 668 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 0
Size: 3350 Color: 1
Size: 246 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10370 Color: 4
Size: 3256 Color: 0
Size: 278 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10366 Color: 0
Size: 2946 Color: 2
Size: 592 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 10519 Color: 0
Size: 3031 Color: 1
Size: 354 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 10762 Color: 0
Size: 2894 Color: 3
Size: 248 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 10904 Color: 2
Size: 2760 Color: 4
Size: 240 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 0
Size: 1578 Color: 1
Size: 1292 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11076 Color: 0
Size: 2622 Color: 3
Size: 206 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 0
Size: 2040 Color: 3
Size: 784 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11066 Color: 3
Size: 2578 Color: 0
Size: 260 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11088 Color: 3
Size: 2504 Color: 0
Size: 312 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 3
Size: 1791 Color: 0
Size: 1008 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11256 Color: 3
Size: 2360 Color: 0
Size: 288 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 4
Size: 1756 Color: 0
Size: 676 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 0
Size: 1465 Color: 3
Size: 816 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 1
Size: 2032 Color: 3
Size: 232 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11730 Color: 0
Size: 1798 Color: 4
Size: 376 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11757 Color: 0
Size: 1331 Color: 4
Size: 816 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11804 Color: 0
Size: 1088 Color: 3
Size: 1012 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11813 Color: 3
Size: 1797 Color: 0
Size: 294 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 3
Size: 1156 Color: 0
Size: 896 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11893 Color: 2
Size: 1587 Color: 1
Size: 424 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11933 Color: 1
Size: 1531 Color: 3
Size: 440 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11950 Color: 2
Size: 1378 Color: 4
Size: 576 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11956 Color: 1
Size: 1696 Color: 0
Size: 252 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11998 Color: 4
Size: 1630 Color: 0
Size: 276 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12014 Color: 4
Size: 1320 Color: 2
Size: 570 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 0
Size: 1478 Color: 3
Size: 348 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 1
Size: 1628 Color: 0
Size: 232 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12075 Color: 4
Size: 1223 Color: 2
Size: 606 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 4
Size: 1302 Color: 0
Size: 470 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 0
Size: 1432 Color: 3
Size: 320 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12171 Color: 0
Size: 1353 Color: 3
Size: 380 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12198 Color: 0
Size: 1310 Color: 2
Size: 396 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 1
Size: 952 Color: 0
Size: 752 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12229 Color: 2
Size: 1271 Color: 0
Size: 404 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12236 Color: 4
Size: 1088 Color: 0
Size: 580 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12238 Color: 3
Size: 968 Color: 0
Size: 698 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 2
Size: 1396 Color: 0
Size: 260 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12281 Color: 3
Size: 1255 Color: 2
Size: 368 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12309 Color: 0
Size: 1325 Color: 3
Size: 270 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 0
Size: 1188 Color: 2
Size: 360 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12361 Color: 0
Size: 1287 Color: 3
Size: 256 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12374 Color: 2
Size: 1258 Color: 0
Size: 272 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 2
Size: 1156 Color: 3
Size: 358 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 1
Size: 1262 Color: 0
Size: 244 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12468 Color: 3
Size: 992 Color: 1
Size: 444 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12510 Color: 3
Size: 1162 Color: 1
Size: 232 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 4
Size: 1008 Color: 3
Size: 392 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 7811 Color: 1
Size: 5796 Color: 1
Size: 296 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 7846 Color: 1
Size: 5793 Color: 1
Size: 264 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 7843 Color: 0
Size: 4788 Color: 4
Size: 1272 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 3
Size: 5031 Color: 2
Size: 352 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 8579 Color: 0
Size: 4852 Color: 1
Size: 472 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 8606 Color: 2
Size: 5025 Color: 4
Size: 272 Color: 2

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 9125 Color: 3
Size: 4418 Color: 1
Size: 360 Color: 2

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 4
Size: 3921 Color: 0
Size: 406 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 9871 Color: 4
Size: 3364 Color: 0
Size: 668 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 10418 Color: 0
Size: 2989 Color: 4
Size: 496 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 4
Size: 2855 Color: 4
Size: 352 Color: 3

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 0
Size: 2906 Color: 2
Size: 288 Color: 3

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 10743 Color: 1
Size: 2984 Color: 4
Size: 176 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 0
Size: 1678 Color: 1
Size: 892 Color: 4

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 11351 Color: 4
Size: 2216 Color: 0
Size: 336 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 11461 Color: 2
Size: 2202 Color: 1
Size: 240 Color: 0

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 11517 Color: 0
Size: 1382 Color: 4
Size: 1004 Color: 3

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 11613 Color: 3
Size: 2026 Color: 1
Size: 264 Color: 0

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 11990 Color: 4
Size: 1601 Color: 0
Size: 312 Color: 2

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 12189 Color: 1
Size: 1422 Color: 4
Size: 292 Color: 0

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 12315 Color: 1
Size: 1332 Color: 0
Size: 256 Color: 4

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 12381 Color: 1
Size: 1522 Color: 4

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 12399 Color: 4
Size: 896 Color: 3
Size: 608 Color: 0

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 12421 Color: 4
Size: 1158 Color: 1
Size: 324 Color: 0

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 12437 Color: 3
Size: 1210 Color: 1
Size: 256 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 9556 Color: 3
Size: 3818 Color: 2
Size: 528 Color: 1

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 10348 Color: 0
Size: 3122 Color: 1
Size: 432 Color: 3

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 10302 Color: 4
Size: 3276 Color: 0
Size: 324 Color: 2

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 1
Size: 1768 Color: 0
Size: 604 Color: 1

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 11668 Color: 2
Size: 1982 Color: 1
Size: 252 Color: 0

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 11674 Color: 2
Size: 1944 Color: 0
Size: 284 Color: 4

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 2
Size: 2182 Color: 4

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 11773 Color: 2
Size: 1809 Color: 0
Size: 320 Color: 4

Bin 96: 3 of cap free
Amount of items: 8
Items: 
Size: 6957 Color: 1
Size: 1204 Color: 0
Size: 1176 Color: 1
Size: 1156 Color: 0
Size: 1152 Color: 4
Size: 1152 Color: 2
Size: 832 Color: 4
Size: 272 Color: 4

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 6953 Color: 0
Size: 5942 Color: 1
Size: 1006 Color: 4

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 7867 Color: 3
Size: 5784 Color: 4
Size: 250 Color: 3

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 8539 Color: 4
Size: 5078 Color: 3
Size: 284 Color: 0

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 8972 Color: 4
Size: 2892 Color: 0
Size: 2037 Color: 3

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 9206 Color: 4
Size: 4439 Color: 3
Size: 256 Color: 0

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 3
Size: 2333 Color: 0
Size: 1896 Color: 4

Bin 103: 3 of cap free
Amount of items: 2
Items: 
Size: 10410 Color: 3
Size: 3491 Color: 1

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 11025 Color: 4
Size: 2624 Color: 1
Size: 252 Color: 2

Bin 105: 3 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 1
Size: 2636 Color: 0
Size: 104 Color: 3

Bin 106: 3 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 1
Size: 2231 Color: 0
Size: 408 Color: 4

Bin 107: 3 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 3
Size: 1832 Color: 1

Bin 108: 3 of cap free
Amount of items: 2
Items: 
Size: 12139 Color: 1
Size: 1762 Color: 2

Bin 109: 3 of cap free
Amount of items: 2
Items: 
Size: 12334 Color: 3
Size: 1567 Color: 2

Bin 110: 4 of cap free
Amount of items: 18
Items: 
Size: 1010 Color: 3
Size: 976 Color: 3
Size: 944 Color: 1
Size: 886 Color: 1
Size: 880 Color: 3
Size: 872 Color: 2
Size: 824 Color: 1
Size: 796 Color: 1
Size: 788 Color: 2
Size: 780 Color: 3
Size: 776 Color: 4
Size: 760 Color: 4
Size: 672 Color: 0
Size: 672 Color: 0
Size: 656 Color: 4
Size: 616 Color: 0
Size: 526 Color: 2
Size: 466 Color: 4

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 7814 Color: 0
Size: 5782 Color: 3
Size: 304 Color: 2

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 7916 Color: 0
Size: 4748 Color: 4
Size: 1236 Color: 1

Bin 113: 4 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 0
Size: 3704 Color: 1
Size: 1022 Color: 3

Bin 114: 4 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 3
Size: 3892 Color: 4

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 10232 Color: 0
Size: 3404 Color: 2
Size: 264 Color: 1

Bin 116: 4 of cap free
Amount of items: 3
Items: 
Size: 10436 Color: 2
Size: 3064 Color: 0
Size: 400 Color: 1

Bin 117: 4 of cap free
Amount of items: 2
Items: 
Size: 11506 Color: 3
Size: 2394 Color: 1

Bin 118: 4 of cap free
Amount of items: 2
Items: 
Size: 11576 Color: 3
Size: 2324 Color: 2

Bin 119: 4 of cap free
Amount of items: 3
Items: 
Size: 11620 Color: 1
Size: 2092 Color: 2
Size: 188 Color: 4

Bin 120: 4 of cap free
Amount of items: 2
Items: 
Size: 11790 Color: 4
Size: 2110 Color: 3

Bin 121: 5 of cap free
Amount of items: 3
Items: 
Size: 7875 Color: 3
Size: 5624 Color: 0
Size: 400 Color: 2

Bin 122: 5 of cap free
Amount of items: 2
Items: 
Size: 12428 Color: 3
Size: 1471 Color: 2

Bin 123: 5 of cap free
Amount of items: 2
Items: 
Size: 12454 Color: 1
Size: 1445 Color: 3

Bin 124: 5 of cap free
Amount of items: 2
Items: 
Size: 12502 Color: 1
Size: 1397 Color: 2

Bin 125: 6 of cap free
Amount of items: 3
Items: 
Size: 7320 Color: 4
Size: 5778 Color: 3
Size: 800 Color: 4

Bin 126: 6 of cap free
Amount of items: 3
Items: 
Size: 10814 Color: 0
Size: 2964 Color: 2
Size: 120 Color: 2

Bin 127: 6 of cap free
Amount of items: 2
Items: 
Size: 11077 Color: 1
Size: 2821 Color: 3

Bin 128: 6 of cap free
Amount of items: 3
Items: 
Size: 11454 Color: 2
Size: 2364 Color: 3
Size: 80 Color: 0

Bin 129: 6 of cap free
Amount of items: 2
Items: 
Size: 12084 Color: 3
Size: 1814 Color: 2

Bin 130: 6 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 4
Size: 1590 Color: 1

Bin 131: 6 of cap free
Amount of items: 2
Items: 
Size: 12342 Color: 2
Size: 1556 Color: 4

Bin 132: 7 of cap free
Amount of items: 3
Items: 
Size: 9715 Color: 3
Size: 3890 Color: 2
Size: 292 Color: 0

Bin 133: 7 of cap free
Amount of items: 2
Items: 
Size: 10269 Color: 4
Size: 3628 Color: 2

Bin 134: 7 of cap free
Amount of items: 3
Items: 
Size: 10871 Color: 0
Size: 1777 Color: 1
Size: 1249 Color: 4

Bin 135: 8 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 0
Size: 5050 Color: 2
Size: 332 Color: 3

Bin 136: 8 of cap free
Amount of items: 2
Items: 
Size: 11894 Color: 1
Size: 2002 Color: 4

Bin 137: 9 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 1
Size: 3361 Color: 4
Size: 576 Color: 0

Bin 138: 10 of cap free
Amount of items: 2
Items: 
Size: 11983 Color: 2
Size: 1911 Color: 4

Bin 139: 10 of cap free
Amount of items: 2
Items: 
Size: 12025 Color: 3
Size: 1869 Color: 2

Bin 140: 10 of cap free
Amount of items: 2
Items: 
Size: 12254 Color: 3
Size: 1640 Color: 2

Bin 141: 11 of cap free
Amount of items: 3
Items: 
Size: 9199 Color: 3
Size: 4470 Color: 1
Size: 224 Color: 0

Bin 142: 11 of cap free
Amount of items: 2
Items: 
Size: 11750 Color: 4
Size: 2143 Color: 2

Bin 143: 12 of cap free
Amount of items: 2
Items: 
Size: 9238 Color: 4
Size: 4654 Color: 2

Bin 144: 12 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 4
Size: 2680 Color: 3
Size: 96 Color: 2

Bin 145: 12 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 3
Size: 1868 Color: 4

Bin 146: 13 of cap free
Amount of items: 5
Items: 
Size: 6964 Color: 3
Size: 2529 Color: 2
Size: 1624 Color: 0
Size: 1390 Color: 2
Size: 1384 Color: 4

Bin 147: 13 of cap free
Amount of items: 2
Items: 
Size: 12407 Color: 4
Size: 1484 Color: 1

Bin 148: 14 of cap free
Amount of items: 2
Items: 
Size: 9734 Color: 3
Size: 4156 Color: 4

Bin 149: 14 of cap free
Amount of items: 2
Items: 
Size: 10600 Color: 4
Size: 3290 Color: 3

Bin 150: 14 of cap free
Amount of items: 2
Items: 
Size: 12147 Color: 2
Size: 1743 Color: 1

Bin 151: 15 of cap free
Amount of items: 3
Items: 
Size: 10203 Color: 3
Size: 3478 Color: 4
Size: 208 Color: 4

Bin 152: 15 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 2
Size: 1643 Color: 3

Bin 153: 16 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 2
Size: 6144 Color: 4
Size: 588 Color: 2

Bin 154: 16 of cap free
Amount of items: 2
Items: 
Size: 12484 Color: 3
Size: 1404 Color: 1

Bin 155: 17 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 1
Size: 5111 Color: 0
Size: 692 Color: 4

Bin 156: 18 of cap free
Amount of items: 4
Items: 
Size: 6966 Color: 3
Size: 4494 Color: 0
Size: 2042 Color: 2
Size: 384 Color: 4

Bin 157: 18 of cap free
Amount of items: 2
Items: 
Size: 11290 Color: 2
Size: 2596 Color: 1

Bin 158: 19 of cap free
Amount of items: 4
Items: 
Size: 6968 Color: 3
Size: 5079 Color: 0
Size: 1582 Color: 4
Size: 256 Color: 2

Bin 159: 19 of cap free
Amount of items: 2
Items: 
Size: 9902 Color: 1
Size: 3983 Color: 2

Bin 160: 20 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 3
Size: 6180 Color: 1
Size: 544 Color: 0

Bin 161: 22 of cap free
Amount of items: 2
Items: 
Size: 9766 Color: 3
Size: 4116 Color: 1

Bin 162: 23 of cap free
Amount of items: 2
Items: 
Size: 10796 Color: 2
Size: 3085 Color: 1

Bin 163: 23 of cap free
Amount of items: 2
Items: 
Size: 11749 Color: 1
Size: 2132 Color: 2

Bin 164: 26 of cap free
Amount of items: 3
Items: 
Size: 8667 Color: 3
Size: 5051 Color: 1
Size: 160 Color: 0

Bin 165: 29 of cap free
Amount of items: 2
Items: 
Size: 11474 Color: 3
Size: 2401 Color: 1

Bin 166: 31 of cap free
Amount of items: 3
Items: 
Size: 11348 Color: 2
Size: 2357 Color: 4
Size: 168 Color: 1

Bin 167: 33 of cap free
Amount of items: 2
Items: 
Size: 11880 Color: 4
Size: 1991 Color: 3

Bin 168: 37 of cap free
Amount of items: 2
Items: 
Size: 12190 Color: 1
Size: 1677 Color: 3

Bin 169: 38 of cap free
Amount of items: 2
Items: 
Size: 11958 Color: 0
Size: 1908 Color: 3

Bin 170: 39 of cap free
Amount of items: 2
Items: 
Size: 10434 Color: 2
Size: 3431 Color: 3

Bin 171: 40 of cap free
Amount of items: 2
Items: 
Size: 11229 Color: 4
Size: 2635 Color: 2

Bin 172: 40 of cap free
Amount of items: 2
Items: 
Size: 11735 Color: 1
Size: 2129 Color: 4

Bin 173: 41 of cap free
Amount of items: 2
Items: 
Size: 10479 Color: 2
Size: 3384 Color: 3

Bin 174: 42 of cap free
Amount of items: 2
Items: 
Size: 12006 Color: 4
Size: 1856 Color: 3

Bin 175: 44 of cap free
Amount of items: 2
Items: 
Size: 8924 Color: 4
Size: 4936 Color: 2

Bin 176: 45 of cap free
Amount of items: 2
Items: 
Size: 12237 Color: 2
Size: 1622 Color: 3

Bin 177: 47 of cap free
Amount of items: 2
Items: 
Size: 12328 Color: 3
Size: 1529 Color: 2

Bin 178: 48 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 3
Size: 1464 Color: 2

Bin 179: 53 of cap free
Amount of items: 2
Items: 
Size: 11396 Color: 1
Size: 2455 Color: 3

Bin 180: 54 of cap free
Amount of items: 2
Items: 
Size: 12134 Color: 4
Size: 1716 Color: 3

Bin 181: 59 of cap free
Amount of items: 2
Items: 
Size: 11944 Color: 3
Size: 1901 Color: 4

Bin 182: 60 of cap free
Amount of items: 2
Items: 
Size: 9596 Color: 4
Size: 4248 Color: 1

Bin 183: 65 of cap free
Amount of items: 2
Items: 
Size: 11663 Color: 1
Size: 2176 Color: 3

Bin 184: 68 of cap free
Amount of items: 2
Items: 
Size: 6956 Color: 4
Size: 6880 Color: 3

Bin 185: 74 of cap free
Amount of items: 2
Items: 
Size: 11464 Color: 4
Size: 2366 Color: 1

Bin 186: 78 of cap free
Amount of items: 2
Items: 
Size: 10328 Color: 1
Size: 3498 Color: 3

Bin 187: 78 of cap free
Amount of items: 2
Items: 
Size: 12228 Color: 4
Size: 1598 Color: 1

Bin 188: 81 of cap free
Amount of items: 2
Items: 
Size: 9064 Color: 4
Size: 4759 Color: 1

Bin 189: 83 of cap free
Amount of items: 2
Items: 
Size: 9787 Color: 1
Size: 4034 Color: 3

Bin 190: 112 of cap free
Amount of items: 2
Items: 
Size: 8164 Color: 3
Size: 5628 Color: 2

Bin 191: 121 of cap free
Amount of items: 2
Items: 
Size: 7992 Color: 1
Size: 5791 Color: 2

Bin 192: 138 of cap free
Amount of items: 2
Items: 
Size: 10748 Color: 1
Size: 3018 Color: 4

Bin 193: 142 of cap free
Amount of items: 2
Items: 
Size: 9820 Color: 3
Size: 3942 Color: 4

Bin 194: 160 of cap free
Amount of items: 30
Items: 
Size: 686 Color: 3
Size: 680 Color: 4
Size: 672 Color: 4
Size: 672 Color: 1
Size: 564 Color: 4
Size: 520 Color: 4
Size: 520 Color: 2
Size: 512 Color: 3
Size: 512 Color: 2
Size: 504 Color: 0
Size: 478 Color: 1
Size: 476 Color: 4
Size: 476 Color: 2
Size: 464 Color: 4
Size: 464 Color: 1
Size: 432 Color: 1
Size: 424 Color: 2
Size: 416 Color: 3
Size: 416 Color: 3
Size: 416 Color: 2
Size: 396 Color: 0
Size: 380 Color: 2
Size: 372 Color: 0
Size: 368 Color: 3
Size: 356 Color: 0
Size: 344 Color: 4
Size: 320 Color: 2
Size: 316 Color: 0
Size: 300 Color: 0
Size: 288 Color: 0

Bin 195: 196 of cap free
Amount of items: 2
Items: 
Size: 8212 Color: 2
Size: 5496 Color: 1

Bin 196: 226 of cap free
Amount of items: 2
Items: 
Size: 8542 Color: 1
Size: 5136 Color: 3

Bin 197: 244 of cap free
Amount of items: 3
Items: 
Size: 6974 Color: 2
Size: 5790 Color: 3
Size: 896 Color: 0

Bin 198: 244 of cap free
Amount of items: 3
Items: 
Size: 7771 Color: 0
Size: 4365 Color: 2
Size: 1524 Color: 2

Bin 199: 10356 of cap free
Amount of items: 12
Items: 
Size: 328 Color: 4
Size: 320 Color: 3
Size: 320 Color: 1
Size: 316 Color: 4
Size: 316 Color: 2
Size: 304 Color: 3
Size: 304 Color: 2
Size: 276 Color: 3
Size: 272 Color: 0
Size: 272 Color: 0
Size: 272 Color: 0
Size: 248 Color: 2

Total size: 2752992
Total free space: 13904

