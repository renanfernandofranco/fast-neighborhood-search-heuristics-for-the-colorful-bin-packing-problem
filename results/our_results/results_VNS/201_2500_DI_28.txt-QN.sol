Capicity Bin: 1956
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 987 Color: 141
Size: 809 Color: 134
Size: 68 Color: 39
Size: 48 Color: 24
Size: 44 Color: 22

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 164
Size: 495 Color: 116
Size: 46 Color: 23

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 165
Size: 482 Color: 113
Size: 56 Color: 28

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 166
Size: 491 Color: 114
Size: 38 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 167
Size: 366 Color: 101
Size: 112 Color: 56

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1483 Color: 168
Size: 441 Color: 108
Size: 32 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 169
Size: 403 Color: 106
Size: 66 Color: 37

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 172
Size: 350 Color: 99
Size: 88 Color: 49

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 173
Size: 365 Color: 100
Size: 72 Color: 42

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 174
Size: 382 Color: 102
Size: 36 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 177
Size: 238 Color: 84
Size: 140 Color: 62

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 178
Size: 303 Color: 94
Size: 60 Color: 32

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 179
Size: 199 Color: 72
Size: 162 Color: 67

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 180
Size: 286 Color: 91
Size: 60 Color: 30

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1641 Color: 182
Size: 263 Color: 89
Size: 52 Color: 27

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 185
Size: 219 Color: 77
Size: 78 Color: 47

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 187
Size: 222 Color: 80
Size: 60 Color: 29

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 188
Size: 209 Color: 73
Size: 68 Color: 38

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1680 Color: 189
Size: 162 Color: 68
Size: 114 Color: 57

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1684 Color: 190
Size: 232 Color: 82
Size: 40 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 191
Size: 182 Color: 70
Size: 88 Color: 48

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 196
Size: 210 Color: 74
Size: 40 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 197
Size: 185 Color: 71
Size: 64 Color: 35

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 200
Size: 140 Color: 63
Size: 78 Color: 46

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 201
Size: 173 Color: 69
Size: 34 Color: 6

Bin 26: 1 of cap free
Amount of items: 5
Items: 
Size: 983 Color: 140
Size: 450 Color: 109
Size: 404 Color: 107
Size: 68 Color: 40
Size: 50 Color: 25

Bin 27: 1 of cap free
Amount of items: 5
Items: 
Size: 1382 Color: 160
Size: 491 Color: 115
Size: 42 Color: 19
Size: 20 Color: 4
Size: 20 Color: 3

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 183
Size: 304 Color: 95
Size: 4 Color: 0

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 184
Size: 301 Color: 93

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 192
Size: 265 Color: 90

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 198
Size: 236 Color: 83

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1735 Color: 199
Size: 220 Color: 79

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1498 Color: 170
Size: 456 Color: 111

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1503 Color: 171
Size: 451 Color: 110

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1559 Color: 175
Size: 395 Color: 104

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1563 Color: 176
Size: 391 Color: 103

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1695 Color: 193
Size: 259 Color: 88

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1703 Color: 195
Size: 251 Color: 85

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1227 Color: 148
Size: 726 Color: 133

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1663 Color: 186
Size: 290 Color: 92

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 194
Size: 254 Color: 86

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 156
Size: 561 Color: 121
Size: 40 Color: 12

Bin 43: 8 of cap free
Amount of items: 2
Items: 
Size: 1399 Color: 163
Size: 549 Color: 119

Bin 44: 8 of cap free
Amount of items: 2
Items: 
Size: 1630 Color: 181
Size: 318 Color: 96

Bin 45: 9 of cap free
Amount of items: 2
Items: 
Size: 1223 Color: 147
Size: 724 Color: 132

Bin 46: 10 of cap free
Amount of items: 16
Items: 
Size: 220 Color: 78
Size: 215 Color: 76
Size: 211 Color: 75
Size: 160 Color: 65
Size: 144 Color: 64
Size: 128 Color: 61
Size: 122 Color: 60
Size: 116 Color: 59
Size: 114 Color: 58
Size: 90 Color: 50
Size: 76 Color: 45
Size: 76 Color: 44
Size: 72 Color: 43
Size: 72 Color: 41
Size: 66 Color: 36
Size: 64 Color: 34

Bin 47: 10 of cap free
Amount of items: 3
Items: 
Size: 1348 Color: 155
Size: 558 Color: 120
Size: 40 Color: 13

Bin 48: 10 of cap free
Amount of items: 2
Items: 
Size: 1367 Color: 158
Size: 579 Color: 123

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1387 Color: 162
Size: 543 Color: 118
Size: 16 Color: 1

Bin 50: 11 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 161
Size: 542 Color: 117
Size: 20 Color: 2

Bin 51: 12 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 150
Size: 656 Color: 127
Size: 42 Color: 17

Bin 52: 15 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 142
Size: 811 Color: 135
Size: 44 Color: 21

Bin 53: 16 of cap free
Amount of items: 3
Items: 
Size: 1306 Color: 154
Size: 594 Color: 124
Size: 40 Color: 15

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1363 Color: 157
Size: 575 Color: 122

Bin 55: 20 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 153
Size: 611 Color: 125
Size: 42 Color: 16

Bin 56: 22 of cap free
Amount of items: 2
Items: 
Size: 1267 Color: 152
Size: 667 Color: 129

Bin 57: 23 of cap free
Amount of items: 4
Items: 
Size: 1378 Color: 159
Size: 479 Color: 112
Size: 40 Color: 11
Size: 36 Color: 7

Bin 58: 26 of cap free
Amount of items: 2
Items: 
Size: 1115 Color: 144
Size: 815 Color: 137

Bin 59: 28 of cap free
Amount of items: 3
Items: 
Size: 1232 Color: 149
Size: 654 Color: 126
Size: 42 Color: 18

Bin 60: 29 of cap free
Amount of items: 2
Items: 
Size: 1113 Color: 143
Size: 814 Color: 136

Bin 61: 31 of cap free
Amount of items: 5
Items: 
Size: 982 Color: 139
Size: 402 Color: 105
Size: 331 Color: 98
Size: 160 Color: 66
Size: 50 Color: 26

Bin 62: 32 of cap free
Amount of items: 2
Items: 
Size: 1221 Color: 146
Size: 703 Color: 131

Bin 63: 32 of cap free
Amount of items: 2
Items: 
Size: 1263 Color: 151
Size: 661 Color: 128

Bin 64: 37 of cap free
Amount of items: 3
Items: 
Size: 1174 Color: 145
Size: 701 Color: 130
Size: 44 Color: 20

Bin 65: 39 of cap free
Amount of items: 6
Items: 
Size: 979 Color: 138
Size: 329 Color: 97
Size: 254 Color: 87
Size: 231 Color: 81
Size: 64 Color: 33
Size: 60 Color: 31

Bin 66: 1466 of cap free
Amount of items: 5
Items: 
Size: 108 Color: 55
Size: 98 Color: 54
Size: 98 Color: 53
Size: 94 Color: 52
Size: 92 Color: 51

Total size: 127140
Total free space: 1956

