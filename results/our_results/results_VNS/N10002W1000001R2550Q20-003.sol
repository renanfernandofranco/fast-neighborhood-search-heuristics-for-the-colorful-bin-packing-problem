Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 393224 Color: 3
Size: 314317 Color: 10
Size: 292460 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 365098 Color: 5
Size: 359145 Color: 12
Size: 275758 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 377856 Color: 9
Size: 321795 Color: 18
Size: 300350 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 492931 Color: 13
Size: 256376 Color: 2
Size: 250694 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 443393 Color: 11
Size: 288694 Color: 0
Size: 267914 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 465589 Color: 4
Size: 282659 Color: 3
Size: 251753 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 493866 Color: 17
Size: 254386 Color: 2
Size: 251749 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 372153 Color: 13
Size: 359541 Color: 10
Size: 268307 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 488008 Color: 19
Size: 257335 Color: 4
Size: 254658 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 363556 Color: 7
Size: 322755 Color: 0
Size: 313690 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 455487 Color: 10
Size: 280799 Color: 4
Size: 263715 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 452428 Color: 15
Size: 292999 Color: 12
Size: 254574 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 400131 Color: 17
Size: 304999 Color: 1
Size: 294871 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 420695 Color: 8
Size: 317881 Color: 7
Size: 261425 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 463390 Color: 6
Size: 272400 Color: 0
Size: 264211 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 468361 Color: 3
Size: 280259 Color: 13
Size: 251381 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432714 Color: 18
Size: 308864 Color: 15
Size: 258423 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 396168 Color: 7
Size: 348620 Color: 16
Size: 255213 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 443035 Color: 17
Size: 304840 Color: 7
Size: 252126 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 393470 Color: 14
Size: 336975 Color: 18
Size: 269556 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 400992 Color: 5
Size: 314207 Color: 8
Size: 284802 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 437622 Color: 8
Size: 299150 Color: 18
Size: 263229 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 396653 Color: 10
Size: 333817 Color: 3
Size: 269531 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 448000 Color: 6
Size: 279613 Color: 10
Size: 272388 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 415315 Color: 6
Size: 316798 Color: 9
Size: 267888 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 422704 Color: 4
Size: 302492 Color: 3
Size: 274805 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 419448 Color: 3
Size: 304932 Color: 13
Size: 275621 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 392524 Color: 14
Size: 335159 Color: 19
Size: 272318 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 402417 Color: 17
Size: 341078 Color: 3
Size: 256506 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 490936 Color: 4
Size: 256995 Color: 16
Size: 252070 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 411773 Color: 3
Size: 307799 Color: 2
Size: 280429 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 382996 Color: 2
Size: 339686 Color: 10
Size: 277319 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 384374 Color: 15
Size: 337043 Color: 1
Size: 278584 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 475810 Color: 9
Size: 267134 Color: 1
Size: 257057 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 358666 Color: 14
Size: 354361 Color: 12
Size: 286974 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 371208 Color: 17
Size: 361164 Color: 7
Size: 267629 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 368971 Color: 5
Size: 321817 Color: 17
Size: 309213 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 373480 Color: 14
Size: 336859 Color: 3
Size: 289662 Color: 12

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 457284 Color: 5
Size: 284078 Color: 15
Size: 258639 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 371497 Color: 7
Size: 363042 Color: 11
Size: 265462 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 494827 Color: 7
Size: 255090 Color: 2
Size: 250084 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 465227 Color: 13
Size: 275005 Color: 1
Size: 259769 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 416044 Color: 19
Size: 331863 Color: 1
Size: 252094 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 402108 Color: 12
Size: 304010 Color: 9
Size: 293883 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 361927 Color: 15
Size: 319258 Color: 17
Size: 318816 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 393419 Color: 8
Size: 320709 Color: 14
Size: 285873 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 444914 Color: 8
Size: 278110 Color: 8
Size: 276977 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 347489 Color: 6
Size: 337328 Color: 8
Size: 315184 Color: 6

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 400606 Color: 3
Size: 320498 Color: 7
Size: 278897 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 372290 Color: 11
Size: 346276 Color: 16
Size: 281435 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 483185 Color: 10
Size: 258912 Color: 11
Size: 257904 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 382412 Color: 14
Size: 324790 Color: 6
Size: 292799 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 368332 Color: 1
Size: 354704 Color: 9
Size: 276965 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 422168 Color: 11
Size: 295715 Color: 18
Size: 282118 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 382765 Color: 13
Size: 313142 Color: 7
Size: 304094 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 464593 Color: 1
Size: 282220 Color: 10
Size: 253188 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 349164 Color: 0
Size: 348617 Color: 11
Size: 302220 Color: 5

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 486555 Color: 1
Size: 256984 Color: 5
Size: 256462 Color: 13

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 379164 Color: 5
Size: 360483 Color: 18
Size: 260354 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 399614 Color: 15
Size: 330119 Color: 16
Size: 270268 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 441500 Color: 7
Size: 300323 Color: 1
Size: 258178 Color: 7

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 374226 Color: 12
Size: 319277 Color: 4
Size: 306498 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 420195 Color: 3
Size: 302502 Color: 13
Size: 277304 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 411576 Color: 8
Size: 295712 Color: 18
Size: 292713 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 422977 Color: 11
Size: 320428 Color: 6
Size: 256596 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 479653 Color: 4
Size: 266196 Color: 18
Size: 254152 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 342544 Color: 14
Size: 340812 Color: 11
Size: 316645 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 397463 Color: 11
Size: 337740 Color: 15
Size: 264798 Color: 19

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 351941 Color: 1
Size: 339065 Color: 7
Size: 308995 Color: 16

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 446979 Color: 2
Size: 289620 Color: 8
Size: 263402 Color: 17

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 416148 Color: 7
Size: 310079 Color: 19
Size: 273774 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 373399 Color: 4
Size: 337091 Color: 12
Size: 289511 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 444681 Color: 14
Size: 278763 Color: 12
Size: 276557 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 371973 Color: 6
Size: 336052 Color: 0
Size: 291976 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 458698 Color: 1
Size: 282482 Color: 16
Size: 258821 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 475287 Color: 17
Size: 270646 Color: 15
Size: 254068 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 458495 Color: 5
Size: 285839 Color: 15
Size: 255667 Color: 8

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 447638 Color: 13
Size: 279303 Color: 6
Size: 273060 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 465739 Color: 19
Size: 273700 Color: 9
Size: 260562 Color: 16

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 366552 Color: 16
Size: 334718 Color: 19
Size: 298731 Color: 10

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 367176 Color: 6
Size: 320940 Color: 19
Size: 311885 Color: 8

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 424881 Color: 15
Size: 308293 Color: 7
Size: 266827 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 476441 Color: 6
Size: 273356 Color: 0
Size: 250204 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 373439 Color: 15
Size: 356753 Color: 19
Size: 269809 Color: 14

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 469110 Color: 19
Size: 276526 Color: 13
Size: 254365 Color: 14

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 416695 Color: 18
Size: 304119 Color: 5
Size: 279187 Color: 11

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 375469 Color: 19
Size: 343080 Color: 1
Size: 281452 Color: 13

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 372475 Color: 16
Size: 329864 Color: 14
Size: 297662 Color: 9

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 434618 Color: 1
Size: 292619 Color: 6
Size: 272764 Color: 8

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 399437 Color: 9
Size: 342505 Color: 8
Size: 258059 Color: 15

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 478185 Color: 16
Size: 266227 Color: 10
Size: 255589 Color: 8

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 474189 Color: 13
Size: 275494 Color: 11
Size: 250318 Color: 15

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 389759 Color: 10
Size: 347353 Color: 17
Size: 262889 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 377675 Color: 10
Size: 332377 Color: 18
Size: 289949 Color: 5

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 429706 Color: 0
Size: 290244 Color: 5
Size: 280051 Color: 17

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 472028 Color: 12
Size: 272148 Color: 15
Size: 255825 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 402977 Color: 5
Size: 346831 Color: 18
Size: 250193 Color: 11

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 431308 Color: 10
Size: 298660 Color: 19
Size: 270033 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 438512 Color: 3
Size: 301883 Color: 13
Size: 259606 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 488111 Color: 1
Size: 256026 Color: 5
Size: 255864 Color: 13

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 419558 Color: 6
Size: 309495 Color: 9
Size: 270948 Color: 8

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 432058 Color: 8
Size: 302410 Color: 6
Size: 265533 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 433322 Color: 8
Size: 301017 Color: 2
Size: 265662 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 428439 Color: 6
Size: 321296 Color: 1
Size: 250266 Color: 12

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 384255 Color: 8
Size: 364362 Color: 6
Size: 251384 Color: 8

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 362148 Color: 9
Size: 341117 Color: 6
Size: 296736 Color: 17

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 427595 Color: 12
Size: 288811 Color: 8
Size: 283595 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 407122 Color: 2
Size: 328911 Color: 16
Size: 263968 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 397020 Color: 11
Size: 310718 Color: 17
Size: 292263 Color: 9

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 378534 Color: 6
Size: 313879 Color: 4
Size: 307588 Color: 15

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 475091 Color: 18
Size: 271318 Color: 1
Size: 253592 Color: 14

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 479334 Color: 5
Size: 267929 Color: 13
Size: 252738 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 457517 Color: 1
Size: 274532 Color: 16
Size: 267952 Color: 12

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 394461 Color: 8
Size: 306510 Color: 7
Size: 299030 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 409733 Color: 15
Size: 318976 Color: 18
Size: 271292 Color: 7

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 367221 Color: 6
Size: 324106 Color: 8
Size: 308674 Color: 7

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 384931 Color: 16
Size: 308657 Color: 17
Size: 306413 Color: 15

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 425808 Color: 18
Size: 298674 Color: 16
Size: 275519 Color: 7

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 363579 Color: 3
Size: 363370 Color: 14
Size: 273052 Color: 19

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 376705 Color: 6
Size: 333774 Color: 2
Size: 289522 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 416970 Color: 6
Size: 313195 Color: 10
Size: 269836 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 460702 Color: 15
Size: 273934 Color: 11
Size: 265365 Color: 10

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 388685 Color: 14
Size: 319241 Color: 0
Size: 292075 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 393057 Color: 19
Size: 319772 Color: 6
Size: 287172 Color: 9

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 368495 Color: 9
Size: 345026 Color: 10
Size: 286480 Color: 18

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 452357 Color: 0
Size: 277715 Color: 8
Size: 269929 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 388601 Color: 9
Size: 306118 Color: 16
Size: 305282 Color: 17

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 376134 Color: 5
Size: 350106 Color: 13
Size: 273761 Color: 11

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 434178 Color: 14
Size: 307960 Color: 2
Size: 257863 Color: 10

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 368959 Color: 3
Size: 362968 Color: 19
Size: 268074 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 398027 Color: 18
Size: 343902 Color: 1
Size: 258072 Color: 19

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 396117 Color: 9
Size: 319628 Color: 18
Size: 284256 Color: 18

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 408799 Color: 9
Size: 326801 Color: 0
Size: 264401 Color: 8

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 484525 Color: 16
Size: 262108 Color: 15
Size: 253368 Color: 8

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 472047 Color: 13
Size: 271673 Color: 13
Size: 256281 Color: 7

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 423419 Color: 19
Size: 323604 Color: 13
Size: 252978 Color: 5

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 369247 Color: 6
Size: 329667 Color: 4
Size: 301087 Color: 9

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 493863 Color: 5
Size: 255897 Color: 0
Size: 250241 Color: 3

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 438682 Color: 16
Size: 288798 Color: 6
Size: 272521 Color: 17

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 347139 Color: 3
Size: 327876 Color: 18
Size: 324986 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 409160 Color: 9
Size: 327096 Color: 8
Size: 263745 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 409859 Color: 13
Size: 315338 Color: 17
Size: 274804 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 351025 Color: 12
Size: 328422 Color: 5
Size: 320554 Color: 10

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 378978 Color: 19
Size: 321997 Color: 4
Size: 299026 Color: 7

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 457315 Color: 18
Size: 278845 Color: 4
Size: 263841 Color: 17

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 410808 Color: 5
Size: 327615 Color: 6
Size: 261578 Color: 17

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 414449 Color: 3
Size: 335380 Color: 13
Size: 250172 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 446891 Color: 12
Size: 298628 Color: 2
Size: 254482 Color: 6

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 453712 Color: 8
Size: 286624 Color: 13
Size: 259665 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 423982 Color: 12
Size: 320999 Color: 17
Size: 255020 Color: 10

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 492623 Color: 10
Size: 255690 Color: 7
Size: 251688 Color: 14

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 429409 Color: 11
Size: 301232 Color: 10
Size: 269360 Color: 9

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 407088 Color: 7
Size: 317751 Color: 19
Size: 275162 Color: 9

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 419632 Color: 17
Size: 291922 Color: 3
Size: 288447 Color: 13

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 401603 Color: 16
Size: 347974 Color: 5
Size: 250424 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 456103 Color: 19
Size: 280086 Color: 14
Size: 263812 Color: 11

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 456544 Color: 10
Size: 292109 Color: 15
Size: 251348 Color: 16

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 424444 Color: 18
Size: 308060 Color: 17
Size: 267497 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 471195 Color: 19
Size: 265174 Color: 10
Size: 263632 Color: 15

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 495597 Color: 14
Size: 254239 Color: 18
Size: 250165 Color: 9

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 388733 Color: 3
Size: 357848 Color: 2
Size: 253420 Color: 17

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 468285 Color: 7
Size: 280412 Color: 5
Size: 251304 Color: 14

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 407355 Color: 5
Size: 302255 Color: 11
Size: 290391 Color: 16

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 376952 Color: 2
Size: 332229 Color: 4
Size: 290820 Color: 17

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 346677 Color: 0
Size: 342519 Color: 5
Size: 310805 Color: 19

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 389147 Color: 16
Size: 334930 Color: 10
Size: 275924 Color: 14

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 350969 Color: 11
Size: 336779 Color: 3
Size: 312253 Color: 11

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 439498 Color: 13
Size: 291822 Color: 17
Size: 268681 Color: 19

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 380179 Color: 14
Size: 318215 Color: 8
Size: 301607 Color: 14

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 378066 Color: 2
Size: 349052 Color: 7
Size: 272883 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 406866 Color: 14
Size: 326182 Color: 16
Size: 266953 Color: 17

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 365866 Color: 11
Size: 326096 Color: 13
Size: 308039 Color: 1

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 406112 Color: 11
Size: 325678 Color: 13
Size: 268211 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 472398 Color: 10
Size: 269789 Color: 13
Size: 257814 Color: 16

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 429824 Color: 6
Size: 286428 Color: 5
Size: 283749 Color: 3

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 481439 Color: 9
Size: 267702 Color: 1
Size: 250860 Color: 13

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 418740 Color: 2
Size: 330065 Color: 8
Size: 251196 Color: 2

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 399220 Color: 0
Size: 346638 Color: 3
Size: 254143 Color: 15

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 470834 Color: 6
Size: 274203 Color: 4
Size: 254964 Color: 10

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 470196 Color: 19
Size: 277937 Color: 1
Size: 251868 Color: 5

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 377096 Color: 3
Size: 359089 Color: 6
Size: 263816 Color: 8

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 378293 Color: 15
Size: 339825 Color: 7
Size: 281883 Color: 17

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 407459 Color: 11
Size: 299666 Color: 17
Size: 292876 Color: 15

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 487147 Color: 9
Size: 257605 Color: 1
Size: 255249 Color: 13

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 466712 Color: 18
Size: 275105 Color: 2
Size: 258184 Color: 17

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 456137 Color: 4
Size: 273411 Color: 9
Size: 270453 Color: 13

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 422545 Color: 7
Size: 310064 Color: 2
Size: 267392 Color: 12

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 394482 Color: 19
Size: 309000 Color: 0
Size: 296519 Color: 5

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 388747 Color: 9
Size: 341669 Color: 0
Size: 269585 Color: 11

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 445042 Color: 4
Size: 289016 Color: 5
Size: 265943 Color: 10

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 417119 Color: 12
Size: 307738 Color: 16
Size: 275144 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 452226 Color: 2
Size: 292095 Color: 8
Size: 255680 Color: 7

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 475996 Color: 9
Size: 271032 Color: 19
Size: 252973 Color: 14

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 425189 Color: 5
Size: 320765 Color: 18
Size: 254047 Color: 2

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 425346 Color: 12
Size: 324621 Color: 11
Size: 250034 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 440755 Color: 7
Size: 280028 Color: 1
Size: 279218 Color: 11

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 402443 Color: 8
Size: 333244 Color: 15
Size: 264314 Color: 18

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 360919 Color: 18
Size: 349545 Color: 8
Size: 289537 Color: 5

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 419796 Color: 18
Size: 297687 Color: 8
Size: 282518 Color: 7

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 400133 Color: 15
Size: 308632 Color: 19
Size: 291236 Color: 9

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 479172 Color: 0
Size: 267247 Color: 11
Size: 253582 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 457245 Color: 18
Size: 276641 Color: 11
Size: 266115 Color: 2

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 396606 Color: 7
Size: 346211 Color: 17
Size: 257184 Color: 13

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 434176 Color: 15
Size: 310114 Color: 13
Size: 255711 Color: 7

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 395665 Color: 2
Size: 319879 Color: 16
Size: 284457 Color: 9

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 382679 Color: 8
Size: 346774 Color: 9
Size: 270548 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 400560 Color: 10
Size: 313832 Color: 3
Size: 285609 Color: 8

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 367722 Color: 18
Size: 348559 Color: 2
Size: 283720 Color: 9

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 384912 Color: 7
Size: 327399 Color: 14
Size: 287690 Color: 16

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 387253 Color: 9
Size: 334433 Color: 12
Size: 278315 Color: 15

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 466937 Color: 19
Size: 271637 Color: 16
Size: 261427 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 371570 Color: 0
Size: 360171 Color: 19
Size: 268260 Color: 14

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 439904 Color: 14
Size: 287225 Color: 15
Size: 272872 Color: 16

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 365527 Color: 13
Size: 325414 Color: 5
Size: 309060 Color: 2

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 386276 Color: 19
Size: 321262 Color: 4
Size: 292463 Color: 5

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 390360 Color: 18
Size: 357629 Color: 12
Size: 252012 Color: 15

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 484183 Color: 18
Size: 264265 Color: 3
Size: 251553 Color: 16

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 403026 Color: 6
Size: 325221 Color: 4
Size: 271754 Color: 14

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 437323 Color: 11
Size: 299440 Color: 8
Size: 263238 Color: 17

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 449590 Color: 3
Size: 278764 Color: 12
Size: 271647 Color: 19

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 452980 Color: 8
Size: 289575 Color: 2
Size: 257446 Color: 10

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 382628 Color: 17
Size: 362262 Color: 19
Size: 255111 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 371285 Color: 18
Size: 315589 Color: 19
Size: 313127 Color: 10

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 489724 Color: 6
Size: 258796 Color: 0
Size: 251481 Color: 2

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 367172 Color: 10
Size: 346156 Color: 1
Size: 286673 Color: 3

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 431966 Color: 15
Size: 288571 Color: 4
Size: 279464 Color: 7

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 464279 Color: 9
Size: 275804 Color: 12
Size: 259918 Color: 13

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 446577 Color: 15
Size: 283653 Color: 9
Size: 269771 Color: 2

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 491048 Color: 16
Size: 254605 Color: 13
Size: 254348 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 467617 Color: 18
Size: 281819 Color: 8
Size: 250565 Color: 9

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 469492 Color: 13
Size: 273174 Color: 3
Size: 257335 Color: 11

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 369359 Color: 11
Size: 359454 Color: 4
Size: 271188 Color: 7

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 494639 Color: 2
Size: 254822 Color: 14
Size: 250540 Color: 19

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 440000 Color: 13
Size: 293470 Color: 4
Size: 266531 Color: 18

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 400730 Color: 19
Size: 317147 Color: 5
Size: 282124 Color: 15

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 384149 Color: 7
Size: 326377 Color: 7
Size: 289475 Color: 8

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 497641 Color: 13
Size: 251228 Color: 2
Size: 251132 Color: 15

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 461424 Color: 1
Size: 281299 Color: 14
Size: 257278 Color: 9

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 409436 Color: 1
Size: 319245 Color: 12
Size: 271320 Color: 18

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 493935 Color: 1
Size: 253586 Color: 11
Size: 252480 Color: 14

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 432419 Color: 6
Size: 315687 Color: 2
Size: 251895 Color: 12

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 430388 Color: 19
Size: 310315 Color: 3
Size: 259298 Color: 16

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 489128 Color: 11
Size: 257045 Color: 6
Size: 253828 Color: 9

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 370506 Color: 6
Size: 330052 Color: 16
Size: 299443 Color: 13

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 370480 Color: 0
Size: 356186 Color: 12
Size: 273335 Color: 14

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 467065 Color: 19
Size: 268533 Color: 10
Size: 264403 Color: 19

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 427393 Color: 11
Size: 299092 Color: 4
Size: 273516 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 406584 Color: 12
Size: 337207 Color: 8
Size: 256210 Color: 13

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 388233 Color: 9
Size: 336330 Color: 7
Size: 275438 Color: 8

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 369569 Color: 3
Size: 347541 Color: 3
Size: 282891 Color: 10

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 353856 Color: 2
Size: 353853 Color: 3
Size: 292292 Color: 11

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 370903 Color: 9
Size: 357629 Color: 7
Size: 271469 Color: 17

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 408497 Color: 10
Size: 339175 Color: 9
Size: 252329 Color: 3

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 379491 Color: 14
Size: 356087 Color: 11
Size: 264423 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 457461 Color: 11
Size: 289518 Color: 1
Size: 253022 Color: 14

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 357718 Color: 0
Size: 350142 Color: 19
Size: 292141 Color: 16

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 360082 Color: 12
Size: 328351 Color: 7
Size: 311568 Color: 11

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 372413 Color: 0
Size: 360208 Color: 14
Size: 267380 Color: 17

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 418604 Color: 1
Size: 295627 Color: 9
Size: 285770 Color: 19

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 391813 Color: 18
Size: 325297 Color: 13
Size: 282891 Color: 9

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 443310 Color: 5
Size: 299403 Color: 18
Size: 257288 Color: 9

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 421311 Color: 1
Size: 294069 Color: 19
Size: 284621 Color: 11

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 361865 Color: 0
Size: 353600 Color: 7
Size: 284536 Color: 10

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 384890 Color: 7
Size: 329488 Color: 19
Size: 285623 Color: 17

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 390200 Color: 15
Size: 324231 Color: 4
Size: 285570 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 408005 Color: 8
Size: 338047 Color: 4
Size: 253949 Color: 2

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 486611 Color: 1
Size: 261388 Color: 14
Size: 252002 Color: 11

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 487409 Color: 3
Size: 259855 Color: 0
Size: 252737 Color: 17

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 395039 Color: 11
Size: 329477 Color: 17
Size: 275485 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 358564 Color: 0
Size: 326377 Color: 5
Size: 315060 Color: 17

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 364173 Color: 13
Size: 359449 Color: 2
Size: 276379 Color: 7

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 392137 Color: 3
Size: 353121 Color: 0
Size: 254743 Color: 18

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 411703 Color: 0
Size: 319231 Color: 14
Size: 269067 Color: 17

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 417518 Color: 5
Size: 321230 Color: 19
Size: 261253 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 401036 Color: 9
Size: 299508 Color: 6
Size: 299457 Color: 18

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 382393 Color: 14
Size: 319433 Color: 16
Size: 298175 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 476064 Color: 9
Size: 266630 Color: 0
Size: 257307 Color: 17

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 479814 Color: 2
Size: 265513 Color: 15
Size: 254674 Color: 10

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 444688 Color: 11
Size: 279399 Color: 2
Size: 275914 Color: 15

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 443941 Color: 10
Size: 295929 Color: 14
Size: 260131 Color: 13

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 483207 Color: 2
Size: 259726 Color: 0
Size: 257068 Color: 18

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 468046 Color: 17
Size: 268920 Color: 1
Size: 263035 Color: 3

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 458191 Color: 5
Size: 282784 Color: 13
Size: 259026 Color: 4

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 446930 Color: 11
Size: 288467 Color: 12
Size: 264604 Color: 9

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 414461 Color: 4
Size: 314118 Color: 6
Size: 271422 Color: 10

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 393457 Color: 1
Size: 328534 Color: 9
Size: 278010 Color: 4

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 383922 Color: 10
Size: 337945 Color: 5
Size: 278134 Color: 18

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 405717 Color: 19
Size: 301197 Color: 5
Size: 293087 Color: 10

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 382293 Color: 7
Size: 346100 Color: 19
Size: 271608 Color: 13

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 483698 Color: 4
Size: 264530 Color: 19
Size: 251773 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 367336 Color: 2
Size: 321219 Color: 18
Size: 311446 Color: 16

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 442480 Color: 14
Size: 298502 Color: 15
Size: 259019 Color: 18

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 471227 Color: 6
Size: 275897 Color: 4
Size: 252877 Color: 15

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 393989 Color: 2
Size: 352637 Color: 0
Size: 253375 Color: 13

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 376421 Color: 11
Size: 347046 Color: 12
Size: 276534 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 380992 Color: 1
Size: 345721 Color: 9
Size: 273288 Color: 16

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 448840 Color: 12
Size: 289135 Color: 2
Size: 262026 Color: 6

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 366375 Color: 18
Size: 325588 Color: 15
Size: 308038 Color: 17

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 403489 Color: 9
Size: 303676 Color: 11
Size: 292836 Color: 5

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 487401 Color: 14
Size: 259411 Color: 5
Size: 253189 Color: 12

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 372068 Color: 3
Size: 320422 Color: 7
Size: 307511 Color: 13

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 364633 Color: 19
Size: 322715 Color: 2
Size: 312653 Color: 5

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 448232 Color: 5
Size: 278045 Color: 19
Size: 273724 Color: 9

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 379433 Color: 14
Size: 328637 Color: 12
Size: 291931 Color: 2

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 396791 Color: 1
Size: 310953 Color: 13
Size: 292257 Color: 2

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 482541 Color: 15
Size: 262911 Color: 16
Size: 254549 Color: 17

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 497526 Color: 18
Size: 251481 Color: 2
Size: 250994 Color: 5

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 445451 Color: 0
Size: 301556 Color: 1
Size: 252994 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 405185 Color: 10
Size: 320347 Color: 2
Size: 274469 Color: 8

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 403109 Color: 7
Size: 312447 Color: 0
Size: 284445 Color: 11

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 408924 Color: 15
Size: 312702 Color: 19
Size: 278375 Color: 6

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 441222 Color: 17
Size: 298820 Color: 10
Size: 259959 Color: 6

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 452707 Color: 19
Size: 292669 Color: 5
Size: 254625 Color: 5

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 347195 Color: 12
Size: 330537 Color: 13
Size: 322269 Color: 11

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 379708 Color: 0
Size: 316833 Color: 19
Size: 303460 Color: 19

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 386899 Color: 5
Size: 326057 Color: 6
Size: 287045 Color: 1

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 374963 Color: 7
Size: 341483 Color: 12
Size: 283555 Color: 12

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 421638 Color: 4
Size: 306067 Color: 14
Size: 272296 Color: 13

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 460545 Color: 19
Size: 283250 Color: 3
Size: 256206 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 407014 Color: 2
Size: 342435 Color: 16
Size: 250552 Color: 9

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 402910 Color: 7
Size: 324817 Color: 5
Size: 272274 Color: 10

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 426295 Color: 15
Size: 296239 Color: 19
Size: 277467 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 443284 Color: 15
Size: 281002 Color: 11
Size: 275715 Color: 10

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 406299 Color: 0
Size: 337698 Color: 3
Size: 256004 Color: 2

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 435077 Color: 17
Size: 310616 Color: 1
Size: 254308 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 427579 Color: 10
Size: 305512 Color: 6
Size: 266910 Color: 18

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 410268 Color: 0
Size: 324152 Color: 7
Size: 265581 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 471839 Color: 5
Size: 265203 Color: 5
Size: 262959 Color: 9

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 429983 Color: 16
Size: 318486 Color: 17
Size: 251532 Color: 7

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 403283 Color: 19
Size: 313575 Color: 9
Size: 283143 Color: 17

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 447156 Color: 9
Size: 276772 Color: 11
Size: 276073 Color: 19

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 378022 Color: 16
Size: 346800 Color: 9
Size: 275179 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 387855 Color: 11
Size: 356121 Color: 9
Size: 256025 Color: 6

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 377533 Color: 14
Size: 315016 Color: 8
Size: 307452 Color: 7

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 377401 Color: 10
Size: 325436 Color: 14
Size: 297164 Color: 8

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 431348 Color: 18
Size: 292388 Color: 6
Size: 276265 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 416467 Color: 3
Size: 293832 Color: 4
Size: 289702 Color: 14

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 460261 Color: 14
Size: 278200 Color: 4
Size: 261540 Color: 6

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 466784 Color: 8
Size: 281803 Color: 15
Size: 251414 Color: 18

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 390183 Color: 7
Size: 344189 Color: 8
Size: 265629 Color: 19

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 382609 Color: 15
Size: 327153 Color: 9
Size: 290239 Color: 3

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 389497 Color: 11
Size: 348340 Color: 4
Size: 262164 Color: 6

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 393903 Color: 3
Size: 351657 Color: 8
Size: 254441 Color: 14

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 440571 Color: 15
Size: 306516 Color: 0
Size: 252914 Color: 3

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 444336 Color: 10
Size: 289982 Color: 12
Size: 265683 Color: 6

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 390650 Color: 17
Size: 333294 Color: 11
Size: 276057 Color: 17

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 410412 Color: 12
Size: 328897 Color: 19
Size: 260692 Color: 15

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 368637 Color: 17
Size: 317094 Color: 15
Size: 314270 Color: 9

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 425824 Color: 10
Size: 313845 Color: 0
Size: 260332 Color: 12

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 361602 Color: 18
Size: 329256 Color: 9
Size: 309143 Color: 2

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 462509 Color: 11
Size: 281630 Color: 2
Size: 255862 Color: 3

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 480412 Color: 0
Size: 261276 Color: 6
Size: 258313 Color: 16

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 378211 Color: 3
Size: 325832 Color: 8
Size: 295958 Color: 6

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 454534 Color: 7
Size: 287628 Color: 1
Size: 257839 Color: 3

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 448118 Color: 2
Size: 280765 Color: 4
Size: 271118 Color: 18

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 437946 Color: 19
Size: 308841 Color: 3
Size: 253214 Color: 17

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 376982 Color: 6
Size: 318632 Color: 1
Size: 304387 Color: 17

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 439244 Color: 3
Size: 301224 Color: 1
Size: 259533 Color: 8

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 356340 Color: 19
Size: 354577 Color: 3
Size: 289084 Color: 8

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 415934 Color: 3
Size: 303168 Color: 15
Size: 280899 Color: 15

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 472427 Color: 1
Size: 275959 Color: 16
Size: 251615 Color: 9

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 431247 Color: 19
Size: 298362 Color: 12
Size: 270392 Color: 16

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 411935 Color: 5
Size: 307751 Color: 1
Size: 280315 Color: 7

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 443876 Color: 18
Size: 281719 Color: 5
Size: 274406 Color: 8

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 450956 Color: 13
Size: 296008 Color: 11
Size: 253037 Color: 15

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 411255 Color: 2
Size: 336612 Color: 15
Size: 252134 Color: 2

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 483688 Color: 0
Size: 265974 Color: 7
Size: 250339 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 429321 Color: 14
Size: 297975 Color: 19
Size: 272705 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 444263 Color: 9
Size: 303416 Color: 14
Size: 252322 Color: 7

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 366325 Color: 8
Size: 323989 Color: 4
Size: 309687 Color: 19

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 444996 Color: 9
Size: 281064 Color: 13
Size: 273941 Color: 13

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 414292 Color: 13
Size: 327059 Color: 1
Size: 258650 Color: 2

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 467429 Color: 3
Size: 267193 Color: 10
Size: 265379 Color: 8

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 495413 Color: 8
Size: 252885 Color: 2
Size: 251703 Color: 2

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 364006 Color: 18
Size: 342387 Color: 3
Size: 293608 Color: 11

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 375301 Color: 10
Size: 326429 Color: 11
Size: 298271 Color: 12

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 391482 Color: 17
Size: 307733 Color: 5
Size: 300786 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 416221 Color: 5
Size: 324051 Color: 9
Size: 259729 Color: 17

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 435477 Color: 6
Size: 288676 Color: 7
Size: 275848 Color: 9

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 371785 Color: 14
Size: 340597 Color: 2
Size: 287619 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 429759 Color: 2
Size: 316728 Color: 11
Size: 253514 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 379251 Color: 19
Size: 312810 Color: 4
Size: 307940 Color: 2

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 435244 Color: 2
Size: 312640 Color: 8
Size: 252117 Color: 12

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 470435 Color: 14
Size: 275003 Color: 15
Size: 254563 Color: 1

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 387320 Color: 12
Size: 350110 Color: 3
Size: 262571 Color: 8

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 434347 Color: 8
Size: 307355 Color: 1
Size: 258299 Color: 5

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 367726 Color: 6
Size: 352627 Color: 19
Size: 279648 Color: 13

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 362347 Color: 16
Size: 345554 Color: 7
Size: 292100 Color: 5

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 481600 Color: 10
Size: 267953 Color: 15
Size: 250448 Color: 18

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 352713 Color: 10
Size: 324407 Color: 18
Size: 322881 Color: 14

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 433921 Color: 0
Size: 313459 Color: 14
Size: 252621 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 347258 Color: 19
Size: 327607 Color: 16
Size: 325136 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 359723 Color: 9
Size: 349580 Color: 10
Size: 290698 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 482877 Color: 0
Size: 260469 Color: 6
Size: 256655 Color: 9

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 427547 Color: 18
Size: 313412 Color: 16
Size: 259042 Color: 3

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 360215 Color: 9
Size: 360099 Color: 10
Size: 279687 Color: 19

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 347441 Color: 17
Size: 333007 Color: 19
Size: 319553 Color: 7

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 395671 Color: 5
Size: 336180 Color: 8
Size: 268150 Color: 14

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 391725 Color: 19
Size: 304497 Color: 7
Size: 303779 Color: 4

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 375841 Color: 1
Size: 349726 Color: 18
Size: 274434 Color: 8

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 431344 Color: 0
Size: 311948 Color: 1
Size: 256709 Color: 13

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 490788 Color: 19
Size: 255276 Color: 0
Size: 253937 Color: 15

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 434391 Color: 19
Size: 295160 Color: 7
Size: 270450 Color: 6

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 343802 Color: 17
Size: 330474 Color: 2
Size: 325725 Color: 11

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 380433 Color: 13
Size: 340127 Color: 0
Size: 279441 Color: 12

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 389622 Color: 17
Size: 312671 Color: 8
Size: 297708 Color: 8

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 462750 Color: 5
Size: 270290 Color: 9
Size: 266961 Color: 3

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 470147 Color: 3
Size: 270405 Color: 6
Size: 259449 Color: 19

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 498095 Color: 15
Size: 251680 Color: 7
Size: 250226 Color: 16

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 448675 Color: 4
Size: 286665 Color: 4
Size: 264661 Color: 18

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 464883 Color: 3
Size: 276183 Color: 10
Size: 258935 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 485795 Color: 3
Size: 264199 Color: 10
Size: 250007 Color: 3

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 363060 Color: 10
Size: 345097 Color: 6
Size: 291844 Color: 3

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 359695 Color: 10
Size: 349952 Color: 19
Size: 290354 Color: 1

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 355971 Color: 12
Size: 352523 Color: 12
Size: 291507 Color: 8

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 352891 Color: 11
Size: 327864 Color: 18
Size: 319246 Color: 18

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 362063 Color: 16
Size: 341717 Color: 0
Size: 296221 Color: 9

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 350669 Color: 1
Size: 327492 Color: 11
Size: 321840 Color: 5

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 353781 Color: 2
Size: 330034 Color: 10
Size: 316186 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 364230 Color: 16
Size: 326499 Color: 5
Size: 309272 Color: 16

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 345141 Color: 7
Size: 336896 Color: 10
Size: 317964 Color: 19

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 365419 Color: 7
Size: 325264 Color: 5
Size: 309318 Color: 11

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 373346 Color: 13
Size: 334090 Color: 3
Size: 292565 Color: 10

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 350645 Color: 1
Size: 328040 Color: 16
Size: 321316 Color: 12

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 354337 Color: 16
Size: 343491 Color: 17
Size: 302173 Color: 13

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 353293 Color: 12
Size: 353094 Color: 2
Size: 293614 Color: 5

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 379744 Color: 12
Size: 330378 Color: 9
Size: 289879 Color: 7

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 380320 Color: 7
Size: 350395 Color: 16
Size: 269286 Color: 12

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 352278 Color: 19
Size: 350972 Color: 17
Size: 296751 Color: 7

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 382774 Color: 1
Size: 365986 Color: 0
Size: 251241 Color: 1

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 393940 Color: 8
Size: 342511 Color: 17
Size: 263550 Color: 4

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 394464 Color: 5
Size: 345337 Color: 16
Size: 260200 Color: 13

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 356376 Color: 6
Size: 348118 Color: 5
Size: 295507 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 412822 Color: 9
Size: 333075 Color: 18
Size: 254104 Color: 2

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 347531 Color: 10
Size: 339795 Color: 1
Size: 312675 Color: 3

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 351016 Color: 7
Size: 324721 Color: 6
Size: 324264 Color: 4

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 351918 Color: 15
Size: 327463 Color: 10
Size: 320620 Color: 8

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 373445 Color: 11
Size: 360729 Color: 4
Size: 265827 Color: 15

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 344422 Color: 5
Size: 338464 Color: 13
Size: 317115 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 350220 Color: 13
Size: 338417 Color: 9
Size: 311364 Color: 10

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 348477 Color: 7
Size: 344335 Color: 2
Size: 307189 Color: 7

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 360420 Color: 0
Size: 333352 Color: 12
Size: 306229 Color: 6

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 349904 Color: 17
Size: 337104 Color: 5
Size: 312993 Color: 2

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 382616 Color: 2
Size: 338027 Color: 17
Size: 279358 Color: 12

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 361548 Color: 7
Size: 340685 Color: 3
Size: 297768 Color: 15

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 348524 Color: 18
Size: 329898 Color: 18
Size: 321579 Color: 5

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 349561 Color: 13
Size: 336896 Color: 0
Size: 313544 Color: 19

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 337810 Color: 2
Size: 337154 Color: 6
Size: 325037 Color: 3

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 359816 Color: 5
Size: 346459 Color: 3
Size: 293726 Color: 4

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 354601 Color: 13
Size: 352741 Color: 16
Size: 292659 Color: 4

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 360663 Color: 19
Size: 343587 Color: 14
Size: 295751 Color: 11

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 361953 Color: 0
Size: 347673 Color: 0
Size: 290375 Color: 4

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 337890 Color: 16
Size: 334559 Color: 16
Size: 327552 Color: 9

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 357532 Color: 3
Size: 322035 Color: 15
Size: 320434 Color: 11

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 348757 Color: 5
Size: 340971 Color: 17
Size: 310273 Color: 11

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 340546 Color: 15
Size: 339597 Color: 13
Size: 319858 Color: 12

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 349319 Color: 2
Size: 342794 Color: 5
Size: 307888 Color: 9

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 346066 Color: 6
Size: 338280 Color: 2
Size: 315655 Color: 8

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 372879 Color: 2
Size: 368926 Color: 12
Size: 258196 Color: 4

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 351618 Color: 11
Size: 328705 Color: 7
Size: 319678 Color: 9

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 359494 Color: 12
Size: 325717 Color: 7
Size: 314790 Color: 8

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 353494 Color: 5
Size: 349624 Color: 8
Size: 296883 Color: 12

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 355447 Color: 8
Size: 322912 Color: 10
Size: 321642 Color: 1

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 369234 Color: 13
Size: 350460 Color: 0
Size: 280307 Color: 4

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 360148 Color: 1
Size: 353501 Color: 11
Size: 286352 Color: 8

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 365644 Color: 16
Size: 351117 Color: 13
Size: 283240 Color: 18

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 359698 Color: 6
Size: 335763 Color: 6
Size: 304540 Color: 4

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 345770 Color: 6
Size: 328340 Color: 6
Size: 325891 Color: 11

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 360427 Color: 6
Size: 325414 Color: 8
Size: 314160 Color: 16

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 369203 Color: 5
Size: 359357 Color: 7
Size: 271441 Color: 10

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 361642 Color: 17
Size: 325388 Color: 5
Size: 312971 Color: 3

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 359999 Color: 16
Size: 328964 Color: 5
Size: 311038 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 360102 Color: 10
Size: 354568 Color: 6
Size: 285331 Color: 5

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 361095 Color: 19
Size: 355021 Color: 11
Size: 283885 Color: 10

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 350258 Color: 5
Size: 333200 Color: 0
Size: 316543 Color: 16

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 353854 Color: 8
Size: 339265 Color: 11
Size: 306882 Color: 9

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 347975 Color: 8
Size: 341496 Color: 8
Size: 310530 Color: 6

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 346340 Color: 17
Size: 345756 Color: 9
Size: 307905 Color: 12

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 349927 Color: 0
Size: 331005 Color: 18
Size: 319069 Color: 3

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 358916 Color: 14
Size: 329634 Color: 12
Size: 311451 Color: 15

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 348666 Color: 9
Size: 325754 Color: 4
Size: 325581 Color: 1

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 346487 Color: 3
Size: 341946 Color: 2
Size: 311568 Color: 8

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 348199 Color: 16
Size: 332356 Color: 11
Size: 319446 Color: 13

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 358615 Color: 19
Size: 353757 Color: 1
Size: 287629 Color: 10

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 351957 Color: 2
Size: 334378 Color: 6
Size: 313666 Color: 14

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 348780 Color: 1
Size: 340727 Color: 4
Size: 310494 Color: 14

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 336744 Color: 0
Size: 333461 Color: 13
Size: 329796 Color: 14

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 370184 Color: 0
Size: 361945 Color: 2
Size: 267872 Color: 14

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 351535 Color: 3
Size: 333035 Color: 10
Size: 315431 Color: 9

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 342395 Color: 4
Size: 331620 Color: 2
Size: 325986 Color: 6

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 356685 Color: 15
Size: 328897 Color: 16
Size: 314419 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 358268 Color: 5
Size: 342487 Color: 17
Size: 299246 Color: 1

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 345504 Color: 6
Size: 340501 Color: 19
Size: 313996 Color: 17

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 342018 Color: 8
Size: 330146 Color: 2
Size: 327837 Color: 3

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 356779 Color: 9
Size: 340768 Color: 12
Size: 302454 Color: 4

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 355855 Color: 17
Size: 326138 Color: 8
Size: 318008 Color: 15

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 361048 Color: 11
Size: 346962 Color: 8
Size: 291991 Color: 18

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 356508 Color: 12
Size: 352608 Color: 2
Size: 290885 Color: 9

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 358966 Color: 1
Size: 357177 Color: 16
Size: 283858 Color: 8

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 350833 Color: 6
Size: 325889 Color: 15
Size: 323279 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 347004 Color: 13
Size: 334143 Color: 14
Size: 318854 Color: 8

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 367226 Color: 7
Size: 344596 Color: 17
Size: 288179 Color: 1

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 349703 Color: 10
Size: 335749 Color: 18
Size: 314549 Color: 13

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 365211 Color: 17
Size: 332756 Color: 3
Size: 302034 Color: 6

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 357731 Color: 1
Size: 329253 Color: 8
Size: 313017 Color: 9

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 353384 Color: 10
Size: 325462 Color: 6
Size: 321155 Color: 2

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 338780 Color: 8
Size: 337247 Color: 12
Size: 323974 Color: 17

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 372162 Color: 10
Size: 356294 Color: 11
Size: 271545 Color: 10

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 359948 Color: 5
Size: 339852 Color: 18
Size: 300201 Color: 19

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 336754 Color: 19
Size: 335178 Color: 12
Size: 328069 Color: 13

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 338218 Color: 7
Size: 332535 Color: 14
Size: 329248 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 355776 Color: 15
Size: 355723 Color: 4
Size: 288502 Color: 9

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 375602 Color: 1
Size: 354290 Color: 15
Size: 270109 Color: 14

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 351239 Color: 1
Size: 351237 Color: 14
Size: 297525 Color: 18

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 352360 Color: 19
Size: 325318 Color: 9
Size: 322323 Color: 19

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 408200 Color: 5
Size: 325963 Color: 6
Size: 265838 Color: 1

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 378440 Color: 12
Size: 354465 Color: 13
Size: 267096 Color: 6

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 358579 Color: 9
Size: 353514 Color: 0
Size: 287908 Color: 13

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 335001 Color: 14
Size: 334097 Color: 12
Size: 330903 Color: 5

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 353163 Color: 4
Size: 352066 Color: 10
Size: 294772 Color: 19

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 355814 Color: 0
Size: 350883 Color: 4
Size: 293304 Color: 19

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 403083 Color: 14
Size: 343515 Color: 2
Size: 253403 Color: 17

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 397267 Color: 17
Size: 326745 Color: 19
Size: 275989 Color: 12

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 408121 Color: 0
Size: 317582 Color: 7
Size: 274298 Color: 9

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 387488 Color: 18
Size: 359649 Color: 16
Size: 252864 Color: 17

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 354520 Color: 10
Size: 350062 Color: 1
Size: 295419 Color: 10

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 354826 Color: 13
Size: 346274 Color: 14
Size: 298901 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 349641 Color: 11
Size: 342520 Color: 1
Size: 307840 Color: 12

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 356524 Color: 4
Size: 345666 Color: 10
Size: 297811 Color: 3

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 346003 Color: 14
Size: 344459 Color: 12
Size: 309539 Color: 16

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 346863 Color: 12
Size: 331107 Color: 10
Size: 322031 Color: 12

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 347081 Color: 2
Size: 343448 Color: 3
Size: 309472 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 346737 Color: 6
Size: 326794 Color: 8
Size: 326470 Color: 11

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 354538 Color: 19
Size: 346389 Color: 5
Size: 299074 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 351773 Color: 2
Size: 338771 Color: 5
Size: 309457 Color: 18

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 355735 Color: 4
Size: 345933 Color: 7
Size: 298333 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 350778 Color: 10
Size: 345582 Color: 6
Size: 303641 Color: 8

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 353903 Color: 12
Size: 342505 Color: 2
Size: 303593 Color: 5

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 356111 Color: 10
Size: 346480 Color: 15
Size: 297410 Color: 16

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 353114 Color: 5
Size: 345227 Color: 14
Size: 301660 Color: 2

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 360168 Color: 12
Size: 343422 Color: 11
Size: 296411 Color: 17

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 344753 Color: 15
Size: 343662 Color: 16
Size: 311586 Color: 12

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 360706 Color: 9
Size: 342626 Color: 5
Size: 296669 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 351045 Color: 12
Size: 345978 Color: 2
Size: 302978 Color: 3

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 353565 Color: 10
Size: 341181 Color: 16
Size: 305255 Color: 9

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 334865 Color: 2
Size: 333065 Color: 18
Size: 332071 Color: 19

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 349412 Color: 15
Size: 331836 Color: 5
Size: 318753 Color: 19

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 354677 Color: 3
Size: 327920 Color: 19
Size: 317404 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 338091 Color: 16
Size: 332271 Color: 19
Size: 329639 Color: 11

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 344280 Color: 12
Size: 340499 Color: 3
Size: 315222 Color: 6

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 339375 Color: 2
Size: 337771 Color: 8
Size: 322855 Color: 18

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 359516 Color: 12
Size: 342388 Color: 17
Size: 298097 Color: 5

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 359812 Color: 0
Size: 330051 Color: 17
Size: 310138 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 347577 Color: 2
Size: 340397 Color: 9
Size: 312027 Color: 19

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 339068 Color: 17
Size: 330891 Color: 4
Size: 330042 Color: 4

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 356065 Color: 9
Size: 334087 Color: 4
Size: 309849 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 336906 Color: 17
Size: 333632 Color: 13
Size: 329463 Color: 4

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 347293 Color: 16
Size: 338153 Color: 4
Size: 314555 Color: 2

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 340067 Color: 9
Size: 335798 Color: 18
Size: 324136 Color: 16

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 352124 Color: 1
Size: 326125 Color: 16
Size: 321752 Color: 2

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 344427 Color: 0
Size: 336096 Color: 11
Size: 319478 Color: 13

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 361841 Color: 8
Size: 327677 Color: 0
Size: 310483 Color: 15

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 358016 Color: 19
Size: 338335 Color: 4
Size: 303650 Color: 17

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 337538 Color: 0
Size: 331698 Color: 15
Size: 330765 Color: 18

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 346585 Color: 1
Size: 331628 Color: 0
Size: 321788 Color: 4

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 356114 Color: 5
Size: 343553 Color: 5
Size: 300334 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 356537 Color: 16
Size: 322271 Color: 10
Size: 321193 Color: 13

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 356061 Color: 18
Size: 323194 Color: 1
Size: 320746 Color: 6

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 356727 Color: 13
Size: 335555 Color: 2
Size: 307719 Color: 14

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 356815 Color: 8
Size: 331597 Color: 13
Size: 311589 Color: 4

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 357607 Color: 4
Size: 324518 Color: 1
Size: 317876 Color: 11

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 357966 Color: 12
Size: 357182 Color: 17
Size: 284853 Color: 1

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 357298 Color: 17
Size: 346843 Color: 10
Size: 295860 Color: 3

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 358208 Color: 5
Size: 333828 Color: 1
Size: 307965 Color: 11

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 358586 Color: 18
Size: 325872 Color: 12
Size: 315543 Color: 18

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 358714 Color: 4
Size: 337108 Color: 2
Size: 304179 Color: 12

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 359173 Color: 13
Size: 328772 Color: 12
Size: 312056 Color: 9

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 359544 Color: 1
Size: 329322 Color: 2
Size: 311135 Color: 10

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 359573 Color: 8
Size: 347035 Color: 6
Size: 293393 Color: 15

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 359634 Color: 10
Size: 334201 Color: 4
Size: 306166 Color: 1

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 359685 Color: 18
Size: 329502 Color: 12
Size: 310814 Color: 4

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 359830 Color: 15
Size: 351358 Color: 7
Size: 288813 Color: 9

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 360135 Color: 12
Size: 331164 Color: 13
Size: 308702 Color: 6

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 360189 Color: 19
Size: 343982 Color: 8
Size: 295830 Color: 6

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 360241 Color: 4
Size: 352592 Color: 15
Size: 287168 Color: 13

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 360302 Color: 11
Size: 332417 Color: 2
Size: 307282 Color: 3

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 360330 Color: 1
Size: 332810 Color: 19
Size: 306861 Color: 15

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 360229 Color: 12
Size: 335561 Color: 12
Size: 304211 Color: 17

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 360439 Color: 8
Size: 346458 Color: 11
Size: 293104 Color: 14

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 360520 Color: 19
Size: 322699 Color: 19
Size: 316782 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 360442 Color: 16
Size: 324856 Color: 7
Size: 314703 Color: 16

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 360705 Color: 19
Size: 345346 Color: 7
Size: 293950 Color: 12

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 360792 Color: 3
Size: 319856 Color: 1
Size: 319353 Color: 16

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 360890 Color: 11
Size: 346970 Color: 1
Size: 292141 Color: 9

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 360901 Color: 13
Size: 331794 Color: 1
Size: 307306 Color: 13

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 360925 Color: 18
Size: 325886 Color: 17
Size: 313190 Color: 15

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 360926 Color: 19
Size: 352321 Color: 12
Size: 286754 Color: 10

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 361066 Color: 10
Size: 347004 Color: 19
Size: 291931 Color: 12

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 361128 Color: 10
Size: 325447 Color: 2
Size: 313426 Color: 4

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 361164 Color: 13
Size: 325913 Color: 14
Size: 312924 Color: 8

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 361176 Color: 18
Size: 320540 Color: 15
Size: 318285 Color: 17

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 361192 Color: 8
Size: 334119 Color: 8
Size: 304690 Color: 4

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 361271 Color: 2
Size: 322151 Color: 11
Size: 316579 Color: 1

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 361346 Color: 18
Size: 342899 Color: 8
Size: 295756 Color: 18

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 361362 Color: 7
Size: 357909 Color: 15
Size: 280730 Color: 17

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 361438 Color: 3
Size: 336553 Color: 2
Size: 302010 Color: 2

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 361633 Color: 3
Size: 323968 Color: 9
Size: 314400 Color: 4

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 361640 Color: 15
Size: 344486 Color: 8
Size: 293875 Color: 5

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 361650 Color: 2
Size: 331731 Color: 0
Size: 306620 Color: 3

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 361764 Color: 11
Size: 332271 Color: 5
Size: 305966 Color: 8

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 361783 Color: 2
Size: 340581 Color: 8
Size: 297637 Color: 16

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 361814 Color: 10
Size: 349793 Color: 19
Size: 288394 Color: 5

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 361871 Color: 7
Size: 356272 Color: 1
Size: 281858 Color: 10

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 361878 Color: 11
Size: 333159 Color: 7
Size: 304964 Color: 3

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 361878 Color: 15
Size: 333280 Color: 3
Size: 304843 Color: 9

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 361912 Color: 13
Size: 342754 Color: 19
Size: 295335 Color: 17

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 362045 Color: 14
Size: 336079 Color: 16
Size: 301877 Color: 5

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 362047 Color: 7
Size: 343616 Color: 16
Size: 294338 Color: 10

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 362158 Color: 4
Size: 338820 Color: 16
Size: 299023 Color: 8

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 362263 Color: 2
Size: 320477 Color: 8
Size: 317261 Color: 6

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 362283 Color: 4
Size: 342841 Color: 18
Size: 294877 Color: 13

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 362381 Color: 17
Size: 326093 Color: 7
Size: 311527 Color: 8

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 362489 Color: 9
Size: 323453 Color: 5
Size: 314059 Color: 16

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 362525 Color: 2
Size: 334297 Color: 19
Size: 303179 Color: 7

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 362542 Color: 4
Size: 354928 Color: 5
Size: 282531 Color: 14

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 362673 Color: 13
Size: 338314 Color: 10
Size: 299014 Color: 18

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 362913 Color: 14
Size: 330989 Color: 0
Size: 306099 Color: 19

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 362918 Color: 11
Size: 334196 Color: 11
Size: 302887 Color: 13

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 362974 Color: 2
Size: 337604 Color: 13
Size: 299423 Color: 4

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 362977 Color: 4
Size: 326550 Color: 17
Size: 310474 Color: 19

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 363124 Color: 8
Size: 351562 Color: 0
Size: 285315 Color: 5

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 363205 Color: 14
Size: 344889 Color: 3
Size: 291907 Color: 5

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 363297 Color: 11
Size: 343671 Color: 7
Size: 293033 Color: 2

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 363329 Color: 3
Size: 356557 Color: 18
Size: 280115 Color: 4

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 363334 Color: 14
Size: 347849 Color: 0
Size: 288818 Color: 6

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 363396 Color: 4
Size: 351704 Color: 13
Size: 284901 Color: 2

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 363492 Color: 2
Size: 344681 Color: 15
Size: 291828 Color: 5

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 363505 Color: 3
Size: 353375 Color: 5
Size: 283121 Color: 15

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 363518 Color: 12
Size: 348865 Color: 16
Size: 287618 Color: 6

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 363543 Color: 4
Size: 348306 Color: 14
Size: 288152 Color: 18

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 363585 Color: 6
Size: 337195 Color: 19
Size: 299221 Color: 1

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 363761 Color: 13
Size: 343607 Color: 2
Size: 292633 Color: 2

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 363843 Color: 4
Size: 319396 Color: 13
Size: 316762 Color: 15

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 363866 Color: 6
Size: 322797 Color: 17
Size: 313338 Color: 12

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 363929 Color: 7
Size: 324553 Color: 5
Size: 311519 Color: 8

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 363950 Color: 7
Size: 327489 Color: 8
Size: 308562 Color: 6

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 363992 Color: 16
Size: 351319 Color: 11
Size: 284690 Color: 8

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 364224 Color: 17
Size: 328230 Color: 10
Size: 307547 Color: 1

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 364281 Color: 13
Size: 346005 Color: 14
Size: 289715 Color: 5

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 364351 Color: 8
Size: 322814 Color: 10
Size: 312836 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 364522 Color: 7
Size: 343462 Color: 4
Size: 292017 Color: 11

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 364621 Color: 17
Size: 327371 Color: 14
Size: 308009 Color: 11

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 364634 Color: 13
Size: 359856 Color: 14
Size: 275511 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 364660 Color: 4
Size: 319787 Color: 19
Size: 315554 Color: 15

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 364745 Color: 12
Size: 321220 Color: 19
Size: 314036 Color: 9

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 364761 Color: 10
Size: 324734 Color: 1
Size: 310506 Color: 16

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 364971 Color: 10
Size: 335524 Color: 13
Size: 299506 Color: 9

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 365066 Color: 0
Size: 329234 Color: 11
Size: 305701 Color: 8

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 365075 Color: 4
Size: 363076 Color: 12
Size: 271850 Color: 1

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 365099 Color: 18
Size: 358860 Color: 17
Size: 276042 Color: 6

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 365254 Color: 8
Size: 338570 Color: 14
Size: 296177 Color: 16

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 365280 Color: 16
Size: 320980 Color: 8
Size: 313741 Color: 9

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 365352 Color: 19
Size: 343576 Color: 9
Size: 291073 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 365400 Color: 9
Size: 318173 Color: 14
Size: 316428 Color: 4

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 365462 Color: 12
Size: 326592 Color: 15
Size: 307947 Color: 16

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 365484 Color: 6
Size: 318073 Color: 12
Size: 316444 Color: 1

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 365545 Color: 12
Size: 334087 Color: 15
Size: 300369 Color: 3

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 365545 Color: 11
Size: 350077 Color: 10
Size: 284379 Color: 9

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 365599 Color: 19
Size: 330011 Color: 10
Size: 304391 Color: 8

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 365613 Color: 13
Size: 352047 Color: 4
Size: 282341 Color: 10

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 365631 Color: 3
Size: 324863 Color: 17
Size: 309507 Color: 17

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 365634 Color: 12
Size: 322986 Color: 14
Size: 311381 Color: 5

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 365739 Color: 4
Size: 346338 Color: 11
Size: 287924 Color: 2

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 365772 Color: 8
Size: 334005 Color: 12
Size: 300224 Color: 6

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 365946 Color: 18
Size: 326495 Color: 8
Size: 307560 Color: 3

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 365972 Color: 5
Size: 329939 Color: 6
Size: 304090 Color: 7

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 365991 Color: 3
Size: 364793 Color: 11
Size: 269217 Color: 10

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 365999 Color: 6
Size: 347278 Color: 13
Size: 286724 Color: 2

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 366013 Color: 16
Size: 344308 Color: 17
Size: 289680 Color: 12

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 366046 Color: 7
Size: 360904 Color: 10
Size: 273051 Color: 12

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 366199 Color: 0
Size: 327116 Color: 5
Size: 306686 Color: 2

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 366051 Color: 18
Size: 348107 Color: 16
Size: 285843 Color: 17

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 366276 Color: 10
Size: 333697 Color: 7
Size: 300028 Color: 4

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 366364 Color: 14
Size: 330321 Color: 19
Size: 303316 Color: 15

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 366401 Color: 11
Size: 319891 Color: 5
Size: 313709 Color: 11

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 366508 Color: 15
Size: 350169 Color: 1
Size: 283324 Color: 11

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 366716 Color: 11
Size: 345767 Color: 16
Size: 287518 Color: 15

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 366834 Color: 10
Size: 330853 Color: 19
Size: 302314 Color: 5

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 366872 Color: 7
Size: 359563 Color: 2
Size: 273566 Color: 15

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 366872 Color: 14
Size: 331073 Color: 11
Size: 302056 Color: 1

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 366879 Color: 3
Size: 335299 Color: 19
Size: 297823 Color: 15

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 366893 Color: 8
Size: 333236 Color: 3
Size: 299872 Color: 14

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 366917 Color: 15
Size: 325657 Color: 16
Size: 307427 Color: 6

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 367057 Color: 0
Size: 366289 Color: 14
Size: 266655 Color: 17

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 367111 Color: 0
Size: 326019 Color: 5
Size: 306871 Color: 16

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 366948 Color: 3
Size: 323513 Color: 2
Size: 309540 Color: 2

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 366957 Color: 7
Size: 342209 Color: 14
Size: 290835 Color: 19

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 366962 Color: 9
Size: 343578 Color: 9
Size: 289461 Color: 12

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 367030 Color: 3
Size: 345384 Color: 4
Size: 287587 Color: 6

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 367092 Color: 6
Size: 358950 Color: 3
Size: 273959 Color: 13

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 367264 Color: 0
Size: 350185 Color: 16
Size: 282552 Color: 18

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 367101 Color: 18
Size: 360664 Color: 1
Size: 272236 Color: 15

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 367110 Color: 14
Size: 350198 Color: 15
Size: 282693 Color: 17

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 367273 Color: 13
Size: 343198 Color: 6
Size: 289530 Color: 18

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 367449 Color: 17
Size: 360419 Color: 4
Size: 272133 Color: 1

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 367579 Color: 8
Size: 335719 Color: 9
Size: 296703 Color: 7

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 367595 Color: 19
Size: 330643 Color: 17
Size: 301763 Color: 3

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 367627 Color: 14
Size: 352038 Color: 2
Size: 280336 Color: 16

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 367629 Color: 6
Size: 322038 Color: 18
Size: 310334 Color: 4

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 367632 Color: 10
Size: 362463 Color: 19
Size: 269906 Color: 3

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 367652 Color: 3
Size: 317905 Color: 13
Size: 314444 Color: 12

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 367832 Color: 4
Size: 344588 Color: 6
Size: 287581 Color: 7

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 367839 Color: 19
Size: 331689 Color: 15
Size: 300473 Color: 5

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 367841 Color: 14
Size: 317822 Color: 18
Size: 314338 Color: 4

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 367970 Color: 0
Size: 316933 Color: 5
Size: 315098 Color: 8

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 367844 Color: 5
Size: 360389 Color: 13
Size: 271768 Color: 3

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 367864 Color: 19
Size: 354540 Color: 6
Size: 277597 Color: 7

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 367866 Color: 5
Size: 348884 Color: 17
Size: 283251 Color: 12

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 367868 Color: 14
Size: 316560 Color: 4
Size: 315573 Color: 16

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 367896 Color: 1
Size: 331900 Color: 2
Size: 300205 Color: 2

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 368064 Color: 11
Size: 362171 Color: 8
Size: 269766 Color: 6

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 368154 Color: 1
Size: 340762 Color: 18
Size: 291085 Color: 11

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 368168 Color: 6
Size: 322538 Color: 16
Size: 309295 Color: 2

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 368182 Color: 9
Size: 318695 Color: 9
Size: 313124 Color: 19

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 368213 Color: 13
Size: 337000 Color: 5
Size: 294788 Color: 2

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 368236 Color: 2
Size: 319906 Color: 16
Size: 311859 Color: 19

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 368263 Color: 5
Size: 330465 Color: 17
Size: 301273 Color: 1

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 368264 Color: 18
Size: 332145 Color: 10
Size: 299592 Color: 3

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 368354 Color: 6
Size: 331746 Color: 4
Size: 299901 Color: 10

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 368456 Color: 8
Size: 316832 Color: 0
Size: 314713 Color: 3

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 368498 Color: 15
Size: 328375 Color: 14
Size: 303128 Color: 11

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 368500 Color: 1
Size: 321855 Color: 13
Size: 309646 Color: 17

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 368534 Color: 10
Size: 359396 Color: 19
Size: 272071 Color: 11

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 368630 Color: 0
Size: 354714 Color: 12
Size: 276657 Color: 5

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 368658 Color: 11
Size: 318853 Color: 6
Size: 312490 Color: 3

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 368666 Color: 13
Size: 341273 Color: 15
Size: 290062 Color: 4

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 368731 Color: 16
Size: 331872 Color: 14
Size: 299398 Color: 12

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 368802 Color: 1
Size: 324314 Color: 12
Size: 306885 Color: 6

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 368804 Color: 8
Size: 342308 Color: 3
Size: 288889 Color: 18

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 368850 Color: 7
Size: 368367 Color: 11
Size: 262784 Color: 15

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 368858 Color: 18
Size: 335614 Color: 17
Size: 295529 Color: 7

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 368874 Color: 19
Size: 325837 Color: 17
Size: 305290 Color: 4

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 368920 Color: 16
Size: 351862 Color: 10
Size: 279219 Color: 15

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 368922 Color: 15
Size: 333302 Color: 14
Size: 297777 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 368930 Color: 2
Size: 346544 Color: 12
Size: 284527 Color: 11

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 368995 Color: 12
Size: 361535 Color: 12
Size: 269471 Color: 17

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 369014 Color: 2
Size: 321814 Color: 0
Size: 309173 Color: 19

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 369028 Color: 11
Size: 348854 Color: 18
Size: 282119 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 369057 Color: 5
Size: 366217 Color: 16
Size: 264727 Color: 3

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 369146 Color: 15
Size: 356318 Color: 5
Size: 274537 Color: 12

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 369147 Color: 13
Size: 339779 Color: 11
Size: 291075 Color: 1

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 369197 Color: 14
Size: 333043 Color: 17
Size: 297761 Color: 6

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 369385 Color: 15
Size: 325845 Color: 2
Size: 304771 Color: 19

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 369195 Color: 19
Size: 316222 Color: 15
Size: 314584 Color: 8

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 369449 Color: 16
Size: 361933 Color: 4
Size: 268619 Color: 5

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 369492 Color: 18
Size: 324211 Color: 0
Size: 306298 Color: 1

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 369512 Color: 15
Size: 351843 Color: 9
Size: 278646 Color: 18

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 369534 Color: 10
Size: 353000 Color: 9
Size: 277467 Color: 11

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 369552 Color: 13
Size: 323400 Color: 12
Size: 307049 Color: 2

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 369464 Color: 19
Size: 356475 Color: 1
Size: 274062 Color: 6

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 369595 Color: 17
Size: 319995 Color: 10
Size: 310411 Color: 18

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 369682 Color: 11
Size: 353617 Color: 17
Size: 276702 Color: 14

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 369710 Color: 12
Size: 346911 Color: 6
Size: 283380 Color: 2

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 369818 Color: 3
Size: 355966 Color: 6
Size: 274217 Color: 12

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 369685 Color: 19
Size: 350074 Color: 12
Size: 280242 Color: 13

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 369985 Color: 5
Size: 342885 Color: 17
Size: 287131 Color: 3

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 369820 Color: 12
Size: 329515 Color: 18
Size: 300666 Color: 10

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 369824 Color: 13
Size: 323936 Color: 3
Size: 306241 Color: 15

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 369864 Color: 18
Size: 340010 Color: 10
Size: 290127 Color: 18

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 369953 Color: 12
Size: 341259 Color: 13
Size: 288789 Color: 4

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 370294 Color: 0
Size: 354419 Color: 8
Size: 275288 Color: 17

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 370162 Color: 10
Size: 354817 Color: 13
Size: 275022 Color: 4

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 370205 Color: 5
Size: 356961 Color: 3
Size: 272835 Color: 13

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 370210 Color: 12
Size: 318176 Color: 18
Size: 311615 Color: 16

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 370221 Color: 6
Size: 364990 Color: 14
Size: 264790 Color: 14

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 370479 Color: 10
Size: 318273 Color: 8
Size: 311249 Color: 14

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 370494 Color: 4
Size: 330689 Color: 15
Size: 298818 Color: 10

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 370520 Color: 2
Size: 329582 Color: 17
Size: 299899 Color: 6

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 370713 Color: 9
Size: 338219 Color: 11
Size: 291069 Color: 2

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 370746 Color: 10
Size: 361780 Color: 9
Size: 267475 Color: 6

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 370782 Color: 15
Size: 366197 Color: 4
Size: 263022 Color: 10

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 370825 Color: 2
Size: 317582 Color: 17
Size: 311594 Color: 11

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 370839 Color: 5
Size: 332390 Color: 12
Size: 296772 Color: 5

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 370842 Color: 8
Size: 319008 Color: 4
Size: 310151 Color: 3

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 370862 Color: 18
Size: 370633 Color: 10
Size: 258506 Color: 7

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 371044 Color: 0
Size: 334793 Color: 8
Size: 294164 Color: 9

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 370894 Color: 18
Size: 326934 Color: 4
Size: 302173 Color: 15

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 370940 Color: 7
Size: 363740 Color: 16
Size: 265321 Color: 19

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 370994 Color: 5
Size: 331690 Color: 1
Size: 297317 Color: 16

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 371082 Color: 16
Size: 331747 Color: 0
Size: 297172 Color: 3

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 371107 Color: 1
Size: 325625 Color: 12
Size: 303269 Color: 18

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 371114 Color: 3
Size: 327361 Color: 14
Size: 301526 Color: 6

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 371152 Color: 19
Size: 321250 Color: 16
Size: 307599 Color: 16

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 371211 Color: 10
Size: 326371 Color: 13
Size: 302419 Color: 18

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 371360 Color: 0
Size: 360152 Color: 10
Size: 268489 Color: 17

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 371243 Color: 11
Size: 353480 Color: 8
Size: 275278 Color: 12

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 371288 Color: 3
Size: 357695 Color: 14
Size: 271018 Color: 17

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 371335 Color: 8
Size: 336083 Color: 9
Size: 292583 Color: 3

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 371358 Color: 12
Size: 370554 Color: 10
Size: 258089 Color: 18

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 371391 Color: 17
Size: 368477 Color: 13
Size: 260133 Color: 3

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 371408 Color: 4
Size: 356790 Color: 9
Size: 271803 Color: 3

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 371419 Color: 4
Size: 366940 Color: 15
Size: 261642 Color: 3

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 371480 Color: 7
Size: 329952 Color: 11
Size: 298569 Color: 2

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 371561 Color: 2
Size: 341779 Color: 4
Size: 286661 Color: 6

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 371576 Color: 18
Size: 323658 Color: 19
Size: 304767 Color: 6

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 371659 Color: 10
Size: 321614 Color: 2
Size: 306728 Color: 9

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 371747 Color: 7
Size: 351724 Color: 4
Size: 276530 Color: 19

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 371750 Color: 8
Size: 321127 Color: 2
Size: 307124 Color: 16

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 371809 Color: 17
Size: 354522 Color: 2
Size: 273670 Color: 7

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 371842 Color: 13
Size: 334973 Color: 5
Size: 293186 Color: 7

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 371861 Color: 7
Size: 319866 Color: 16
Size: 308274 Color: 12

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 371895 Color: 7
Size: 322647 Color: 5
Size: 305459 Color: 11

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 371833 Color: 12
Size: 332990 Color: 18
Size: 295178 Color: 19

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 372008 Color: 9
Size: 360224 Color: 3
Size: 267769 Color: 4

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 372062 Color: 17
Size: 344846 Color: 19
Size: 283093 Color: 2

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 372120 Color: 9
Size: 329152 Color: 11
Size: 298729 Color: 4

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 372209 Color: 3
Size: 332701 Color: 15
Size: 295091 Color: 15

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 372221 Color: 4
Size: 359978 Color: 6
Size: 267802 Color: 4

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 372267 Color: 16
Size: 333506 Color: 5
Size: 294228 Color: 13

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 372344 Color: 5
Size: 336866 Color: 8
Size: 290791 Color: 18

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 372410 Color: 3
Size: 356300 Color: 15
Size: 271291 Color: 8

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 372439 Color: 14
Size: 356433 Color: 3
Size: 271129 Color: 4

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 372463 Color: 17
Size: 343450 Color: 5
Size: 284088 Color: 8

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 372598 Color: 8
Size: 319816 Color: 5
Size: 307587 Color: 13

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 372614 Color: 4
Size: 346734 Color: 0
Size: 280653 Color: 10

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 372679 Color: 15
Size: 318780 Color: 3
Size: 308542 Color: 8

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 372718 Color: 4
Size: 317996 Color: 17
Size: 309287 Color: 16

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 372733 Color: 13
Size: 334260 Color: 17
Size: 293008 Color: 19

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 372859 Color: 10
Size: 360054 Color: 8
Size: 267088 Color: 14

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 372886 Color: 16
Size: 340740 Color: 2
Size: 286375 Color: 8

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 372888 Color: 18
Size: 318364 Color: 16
Size: 308749 Color: 7

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 372905 Color: 13
Size: 339238 Color: 8
Size: 287858 Color: 18

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 372945 Color: 8
Size: 333556 Color: 4
Size: 293500 Color: 5

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 372964 Color: 8
Size: 359594 Color: 2
Size: 267443 Color: 10

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 373001 Color: 1
Size: 362711 Color: 4
Size: 264289 Color: 5

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 373063 Color: 19
Size: 319414 Color: 17
Size: 307524 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 373073 Color: 12
Size: 352414 Color: 3
Size: 274514 Color: 13

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 373123 Color: 19
Size: 321220 Color: 3
Size: 305658 Color: 14

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 373133 Color: 19
Size: 314867 Color: 8
Size: 312001 Color: 17

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 373140 Color: 1
Size: 337592 Color: 17
Size: 289269 Color: 16

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 373145 Color: 17
Size: 322030 Color: 16
Size: 304826 Color: 6

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 373154 Color: 8
Size: 329438 Color: 5
Size: 297409 Color: 16

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 373205 Color: 13
Size: 325725 Color: 14
Size: 301071 Color: 1

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 373214 Color: 19
Size: 318881 Color: 6
Size: 307906 Color: 14

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 373314 Color: 6
Size: 322541 Color: 2
Size: 304146 Color: 16

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 373331 Color: 15
Size: 355976 Color: 10
Size: 270694 Color: 8

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 373366 Color: 0
Size: 354940 Color: 1
Size: 271695 Color: 17

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 373394 Color: 16
Size: 366174 Color: 2
Size: 260433 Color: 7

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 373447 Color: 11
Size: 344216 Color: 12
Size: 282338 Color: 5

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 373453 Color: 10
Size: 330200 Color: 15
Size: 296348 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 373603 Color: 1
Size: 349120 Color: 6
Size: 277278 Color: 7

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 373608 Color: 6
Size: 344753 Color: 19
Size: 281640 Color: 11

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 373528 Color: 16
Size: 357330 Color: 14
Size: 269143 Color: 13

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 373655 Color: 17
Size: 327980 Color: 2
Size: 298366 Color: 8

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 373661 Color: 10
Size: 355298 Color: 3
Size: 271042 Color: 2

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 373686 Color: 2
Size: 340599 Color: 1
Size: 285716 Color: 18

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 373731 Color: 3
Size: 334619 Color: 10
Size: 291651 Color: 18

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 373831 Color: 6
Size: 328253 Color: 13
Size: 297917 Color: 14

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 373857 Color: 15
Size: 365200 Color: 0
Size: 260944 Color: 16

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 373898 Color: 9
Size: 359500 Color: 17
Size: 266603 Color: 8

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 373908 Color: 11
Size: 346881 Color: 19
Size: 279212 Color: 19

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 373971 Color: 17
Size: 373587 Color: 3
Size: 252443 Color: 5

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 373990 Color: 6
Size: 348333 Color: 14
Size: 277678 Color: 18

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 374054 Color: 6
Size: 317198 Color: 10
Size: 308749 Color: 1

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 374069 Color: 11
Size: 318866 Color: 12
Size: 307066 Color: 15

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 374156 Color: 15
Size: 337949 Color: 0
Size: 287896 Color: 2

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 374333 Color: 8
Size: 321145 Color: 9
Size: 304523 Color: 4

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 374183 Color: 11
Size: 317324 Color: 10
Size: 308494 Color: 6

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 374203 Color: 7
Size: 358852 Color: 10
Size: 266946 Color: 10

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 374218 Color: 7
Size: 365312 Color: 17
Size: 260471 Color: 11

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 374527 Color: 8
Size: 352846 Color: 5
Size: 272628 Color: 1

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 374381 Color: 12
Size: 331632 Color: 1
Size: 293988 Color: 4

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 374382 Color: 0
Size: 332842 Color: 9
Size: 292777 Color: 5

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 374427 Color: 4
Size: 368432 Color: 0
Size: 257142 Color: 15

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 374548 Color: 18
Size: 314831 Color: 0
Size: 310622 Color: 17

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 374570 Color: 2
Size: 338093 Color: 16
Size: 287338 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 374618 Color: 15
Size: 322829 Color: 1
Size: 302554 Color: 6

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 374657 Color: 2
Size: 337495 Color: 5
Size: 287849 Color: 1

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 374661 Color: 7
Size: 362988 Color: 19
Size: 262352 Color: 14

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 374735 Color: 12
Size: 355124 Color: 4
Size: 270142 Color: 15

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 374781 Color: 5
Size: 339487 Color: 17
Size: 285733 Color: 9

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 374890 Color: 9
Size: 355488 Color: 18
Size: 269623 Color: 14

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 374991 Color: 19
Size: 355070 Color: 2
Size: 269940 Color: 14

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 374995 Color: 16
Size: 318418 Color: 5
Size: 306588 Color: 8

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 375065 Color: 16
Size: 329695 Color: 13
Size: 295241 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 375158 Color: 10
Size: 364367 Color: 7
Size: 260476 Color: 18

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 375185 Color: 15
Size: 358772 Color: 11
Size: 266044 Color: 12

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 375185 Color: 14
Size: 324855 Color: 3
Size: 299961 Color: 3

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 375197 Color: 15
Size: 326836 Color: 11
Size: 297968 Color: 11

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 375234 Color: 4
Size: 364068 Color: 12
Size: 260699 Color: 9

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 375401 Color: 15
Size: 350702 Color: 0
Size: 273898 Color: 9

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 375440 Color: 14
Size: 318259 Color: 16
Size: 306302 Color: 17

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 375466 Color: 14
Size: 328236 Color: 16
Size: 296299 Color: 18

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 375468 Color: 10
Size: 354600 Color: 14
Size: 269933 Color: 8

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 375482 Color: 4
Size: 331747 Color: 18
Size: 292772 Color: 7

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 375484 Color: 7
Size: 373159 Color: 0
Size: 251358 Color: 16

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 375485 Color: 13
Size: 348254 Color: 15
Size: 276262 Color: 15

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 375488 Color: 17
Size: 321313 Color: 11
Size: 303200 Color: 10

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 375526 Color: 13
Size: 315884 Color: 9
Size: 308591 Color: 18

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 375534 Color: 1
Size: 349373 Color: 10
Size: 275094 Color: 13

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 375556 Color: 17
Size: 366800 Color: 7
Size: 257645 Color: 16

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 375692 Color: 12
Size: 365052 Color: 5
Size: 259257 Color: 8

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 375726 Color: 15
Size: 330036 Color: 13
Size: 294239 Color: 15

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 375824 Color: 7
Size: 360720 Color: 5
Size: 263457 Color: 5

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 375831 Color: 6
Size: 355746 Color: 3
Size: 268424 Color: 7

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 375993 Color: 0
Size: 370962 Color: 18
Size: 253046 Color: 4

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 375902 Color: 3
Size: 344457 Color: 15
Size: 279642 Color: 11

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 375908 Color: 2
Size: 367549 Color: 4
Size: 256544 Color: 14

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 375984 Color: 16
Size: 334062 Color: 2
Size: 289955 Color: 3

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 376056 Color: 6
Size: 373587 Color: 1
Size: 250358 Color: 4

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 376184 Color: 4
Size: 355089 Color: 4
Size: 268728 Color: 19

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 376370 Color: 0
Size: 337426 Color: 11
Size: 286205 Color: 5

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 376287 Color: 5
Size: 341139 Color: 11
Size: 282575 Color: 10

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 376390 Color: 18
Size: 330061 Color: 3
Size: 293550 Color: 7

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 376331 Color: 5
Size: 337347 Color: 18
Size: 286323 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 376433 Color: 7
Size: 337819 Color: 0
Size: 285749 Color: 2

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 376512 Color: 14
Size: 340360 Color: 10
Size: 283129 Color: 8

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 376573 Color: 11
Size: 371856 Color: 1
Size: 251572 Color: 2

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 376665 Color: 13
Size: 338948 Color: 19
Size: 284388 Color: 17

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 376704 Color: 19
Size: 360607 Color: 18
Size: 262690 Color: 6

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 376832 Color: 0
Size: 346443 Color: 14
Size: 276726 Color: 6

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 376733 Color: 16
Size: 337787 Color: 17
Size: 285481 Color: 15

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 376786 Color: 8
Size: 329196 Color: 4
Size: 294019 Color: 10

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 376950 Color: 9
Size: 361768 Color: 7
Size: 261283 Color: 8

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 376952 Color: 11
Size: 362659 Color: 3
Size: 260390 Color: 19

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 376964 Color: 15
Size: 319947 Color: 6
Size: 303090 Color: 19

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 377048 Color: 3
Size: 336309 Color: 2
Size: 286644 Color: 15

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 377069 Color: 12
Size: 364586 Color: 17
Size: 258346 Color: 16

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 377073 Color: 3
Size: 352861 Color: 12
Size: 270067 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 377081 Color: 14
Size: 352150 Color: 4
Size: 270770 Color: 16

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 377105 Color: 18
Size: 351580 Color: 6
Size: 271316 Color: 3

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 377131 Color: 1
Size: 358597 Color: 19
Size: 264273 Color: 11

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 377138 Color: 6
Size: 353968 Color: 16
Size: 268895 Color: 1

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 377149 Color: 14
Size: 348258 Color: 18
Size: 274594 Color: 2

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 377436 Color: 0
Size: 313650 Color: 7
Size: 308915 Color: 9

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 377242 Color: 14
Size: 335644 Color: 7
Size: 287115 Color: 10

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 377249 Color: 4
Size: 351535 Color: 11
Size: 271217 Color: 7

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 377262 Color: 11
Size: 357053 Color: 13
Size: 265686 Color: 17

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 377384 Color: 1
Size: 371306 Color: 19
Size: 251311 Color: 6

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 377576 Color: 0
Size: 320818 Color: 4
Size: 301607 Color: 8

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 377450 Color: 16
Size: 349970 Color: 13
Size: 272581 Color: 15

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 377463 Color: 15
Size: 358488 Color: 10
Size: 264050 Color: 15

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 377521 Color: 2
Size: 329792 Color: 2
Size: 292688 Color: 6

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 377650 Color: 0
Size: 340073 Color: 16
Size: 282278 Color: 14

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 377547 Color: 13
Size: 312579 Color: 4
Size: 309875 Color: 5

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 377557 Color: 11
Size: 345760 Color: 15
Size: 276684 Color: 2

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 377588 Color: 4
Size: 317037 Color: 5
Size: 305376 Color: 13

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 377591 Color: 6
Size: 313131 Color: 1
Size: 309279 Color: 2

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 377651 Color: 9
Size: 327169 Color: 19
Size: 295181 Color: 11

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 377682 Color: 9
Size: 313276 Color: 16
Size: 309043 Color: 4

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 377694 Color: 6
Size: 327938 Color: 8
Size: 294369 Color: 2

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 377727 Color: 10
Size: 311834 Color: 4
Size: 310440 Color: 2

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 377756 Color: 17
Size: 324055 Color: 2
Size: 298190 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 377794 Color: 10
Size: 314635 Color: 16
Size: 307572 Color: 13

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 377881 Color: 11
Size: 352357 Color: 5
Size: 269763 Color: 11

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 377902 Color: 3
Size: 362677 Color: 7
Size: 259422 Color: 6

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 377923 Color: 3
Size: 326721 Color: 5
Size: 295357 Color: 11

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 378157 Color: 17
Size: 357365 Color: 7
Size: 264479 Color: 6

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 378315 Color: 3
Size: 346244 Color: 14
Size: 275442 Color: 5

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 378468 Color: 5
Size: 361138 Color: 18
Size: 260395 Color: 10

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 378503 Color: 16
Size: 333935 Color: 4
Size: 287563 Color: 10

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 378533 Color: 6
Size: 355960 Color: 5
Size: 265508 Color: 8

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 378554 Color: 14
Size: 318929 Color: 1
Size: 302518 Color: 15

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 378599 Color: 16
Size: 357277 Color: 17
Size: 264125 Color: 2

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 378637 Color: 4
Size: 311403 Color: 15
Size: 309961 Color: 10

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 378647 Color: 0
Size: 344281 Color: 19
Size: 277073 Color: 7

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 378651 Color: 15
Size: 344680 Color: 1
Size: 276670 Color: 15

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 378688 Color: 3
Size: 362835 Color: 7
Size: 258478 Color: 10

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 378766 Color: 12
Size: 348599 Color: 17
Size: 272636 Color: 16

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 378781 Color: 10
Size: 360470 Color: 17
Size: 260750 Color: 2

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 378854 Color: 10
Size: 352059 Color: 10
Size: 269088 Color: 17

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 378899 Color: 10
Size: 314456 Color: 12
Size: 306646 Color: 10

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 378979 Color: 11
Size: 354513 Color: 13
Size: 266509 Color: 10

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 379036 Color: 1
Size: 350112 Color: 7
Size: 270853 Color: 16

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 379042 Color: 5
Size: 363016 Color: 5
Size: 257943 Color: 2

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 379048 Color: 12
Size: 356429 Color: 9
Size: 264524 Color: 9

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 379088 Color: 9
Size: 339485 Color: 14
Size: 281428 Color: 17

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 379121 Color: 0
Size: 344368 Color: 12
Size: 276512 Color: 8

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 379225 Color: 5
Size: 330627 Color: 6
Size: 290149 Color: 16

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 379272 Color: 17
Size: 325662 Color: 15
Size: 295067 Color: 1

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 379327 Color: 13
Size: 317541 Color: 14
Size: 303133 Color: 9

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 379338 Color: 17
Size: 317095 Color: 14
Size: 303568 Color: 6

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 379399 Color: 14
Size: 321929 Color: 5
Size: 298673 Color: 16

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 379598 Color: 4
Size: 348204 Color: 3
Size: 272199 Color: 6

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 379600 Color: 12
Size: 325847 Color: 6
Size: 294554 Color: 8

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 379620 Color: 3
Size: 337700 Color: 15
Size: 282681 Color: 17

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 379662 Color: 1
Size: 321215 Color: 16
Size: 299124 Color: 17

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 379662 Color: 12
Size: 344340 Color: 2
Size: 275999 Color: 4

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 379676 Color: 17
Size: 346663 Color: 4
Size: 273662 Color: 6

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 379702 Color: 11
Size: 368363 Color: 1
Size: 251936 Color: 11

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 379786 Color: 4
Size: 370140 Color: 2
Size: 250075 Color: 5

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 379799 Color: 4
Size: 350840 Color: 14
Size: 269362 Color: 12

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 379820 Color: 7
Size: 365117 Color: 5
Size: 255064 Color: 18

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 379835 Color: 12
Size: 352214 Color: 5
Size: 267952 Color: 4

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 379831 Color: 17
Size: 326990 Color: 18
Size: 293180 Color: 10

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 379970 Color: 16
Size: 319858 Color: 0
Size: 300173 Color: 2

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 380002 Color: 10
Size: 337966 Color: 13
Size: 282033 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 380014 Color: 14
Size: 324683 Color: 9
Size: 295304 Color: 9

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 380036 Color: 4
Size: 314379 Color: 5
Size: 305586 Color: 3

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 380042 Color: 7
Size: 339645 Color: 17
Size: 280314 Color: 9

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 380093 Color: 3
Size: 311392 Color: 11
Size: 308516 Color: 5

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 380108 Color: 11
Size: 365269 Color: 8
Size: 254624 Color: 2

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 380170 Color: 18
Size: 357342 Color: 19
Size: 262489 Color: 8

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 380291 Color: 15
Size: 313558 Color: 19
Size: 306152 Color: 10

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 380314 Color: 8
Size: 342203 Color: 17
Size: 277484 Color: 15

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 380318 Color: 1
Size: 333034 Color: 5
Size: 286649 Color: 10

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 380375 Color: 19
Size: 350897 Color: 10
Size: 268729 Color: 12

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 380451 Color: 14
Size: 337739 Color: 7
Size: 281811 Color: 6

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 380486 Color: 16
Size: 338647 Color: 7
Size: 280868 Color: 18

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 380486 Color: 1
Size: 334688 Color: 12
Size: 284827 Color: 10

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 380556 Color: 2
Size: 343197 Color: 13
Size: 276248 Color: 17

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 380490 Color: 16
Size: 342448 Color: 16
Size: 277063 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 380505 Color: 12
Size: 353866 Color: 10
Size: 265630 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 380536 Color: 19
Size: 320043 Color: 10
Size: 299422 Color: 16

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 380709 Color: 0
Size: 326463 Color: 11
Size: 292829 Color: 5

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 380733 Color: 12
Size: 335290 Color: 17
Size: 283978 Color: 19

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 380743 Color: 1
Size: 334510 Color: 8
Size: 284748 Color: 5

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 380881 Color: 15
Size: 315578 Color: 4
Size: 303542 Color: 3

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 380893 Color: 4
Size: 355788 Color: 0
Size: 263320 Color: 6

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 380912 Color: 13
Size: 332815 Color: 16
Size: 286274 Color: 6

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 381123 Color: 3
Size: 347382 Color: 3
Size: 271496 Color: 2

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 381281 Color: 12
Size: 328905 Color: 7
Size: 289815 Color: 10

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 381318 Color: 6
Size: 347595 Color: 15
Size: 271088 Color: 14

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 381345 Color: 12
Size: 354733 Color: 14
Size: 263923 Color: 3

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 381431 Color: 12
Size: 361618 Color: 11
Size: 256952 Color: 2

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 381440 Color: 19
Size: 329670 Color: 4
Size: 288891 Color: 12

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 381467 Color: 12
Size: 348734 Color: 10
Size: 269800 Color: 7

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 381496 Color: 16
Size: 345379 Color: 7
Size: 273126 Color: 8

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 381518 Color: 14
Size: 351332 Color: 9
Size: 267151 Color: 11

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 381576 Color: 12
Size: 351929 Color: 9
Size: 266496 Color: 5

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 381617 Color: 12
Size: 331905 Color: 12
Size: 286479 Color: 2

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 381645 Color: 6
Size: 349556 Color: 0
Size: 268800 Color: 14

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 381651 Color: 15
Size: 350954 Color: 18
Size: 267396 Color: 3

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 381679 Color: 8
Size: 367396 Color: 19
Size: 250926 Color: 17

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 381849 Color: 18
Size: 352166 Color: 9
Size: 265986 Color: 19

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 381849 Color: 16
Size: 343667 Color: 5
Size: 274485 Color: 7

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 381855 Color: 6
Size: 354925 Color: 10
Size: 263221 Color: 2

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 382062 Color: 15
Size: 353971 Color: 10
Size: 263968 Color: 18

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 382182 Color: 12
Size: 342376 Color: 8
Size: 275443 Color: 4

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 382220 Color: 18
Size: 350799 Color: 18
Size: 266982 Color: 7

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 382229 Color: 17
Size: 340594 Color: 10
Size: 277178 Color: 7

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 382276 Color: 17
Size: 332042 Color: 5
Size: 285683 Color: 17

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 382295 Color: 15
Size: 339848 Color: 3
Size: 277858 Color: 13

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 382334 Color: 8
Size: 364609 Color: 7
Size: 253058 Color: 16

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 382410 Color: 1
Size: 348926 Color: 19
Size: 268665 Color: 3

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 382411 Color: 8
Size: 346009 Color: 3
Size: 271581 Color: 13

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 382473 Color: 10
Size: 315462 Color: 14
Size: 302066 Color: 11

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 382451 Color: 13
Size: 346654 Color: 6
Size: 270896 Color: 11

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 382474 Color: 3
Size: 367198 Color: 0
Size: 250329 Color: 6

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 382493 Color: 4
Size: 324658 Color: 18
Size: 292850 Color: 12

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 382534 Color: 13
Size: 363929 Color: 12
Size: 253538 Color: 9

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 382556 Color: 3
Size: 354866 Color: 18
Size: 262579 Color: 19

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 382583 Color: 4
Size: 311770 Color: 19
Size: 305648 Color: 5

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 382730 Color: 5
Size: 338713 Color: 15
Size: 278558 Color: 3

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 382732 Color: 5
Size: 325178 Color: 10
Size: 292091 Color: 19

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 382692 Color: 7
Size: 322293 Color: 13
Size: 295016 Color: 4

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 382706 Color: 3
Size: 315737 Color: 9
Size: 301558 Color: 11

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 382764 Color: 16
Size: 336641 Color: 6
Size: 280596 Color: 4

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 382832 Color: 0
Size: 308718 Color: 1
Size: 308451 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 382869 Color: 7
Size: 331358 Color: 8
Size: 285774 Color: 12

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 382928 Color: 8
Size: 320552 Color: 12
Size: 296521 Color: 1

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 382974 Color: 13
Size: 329127 Color: 2
Size: 287900 Color: 6

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 383088 Color: 7
Size: 366104 Color: 2
Size: 250809 Color: 11

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 383090 Color: 18
Size: 326385 Color: 1
Size: 290526 Color: 18

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 383105 Color: 15
Size: 365880 Color: 3
Size: 251016 Color: 19

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 383137 Color: 12
Size: 349872 Color: 0
Size: 266992 Color: 7

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 383180 Color: 13
Size: 341355 Color: 5
Size: 275466 Color: 13

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 383182 Color: 13
Size: 335533 Color: 10
Size: 281286 Color: 7

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 383212 Color: 0
Size: 358333 Color: 16
Size: 258456 Color: 2

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 383240 Color: 14
Size: 309885 Color: 12
Size: 306876 Color: 13

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 383275 Color: 0
Size: 314988 Color: 3
Size: 301738 Color: 4

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 383291 Color: 17
Size: 346774 Color: 7
Size: 269936 Color: 6

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 383343 Color: 5
Size: 349675 Color: 14
Size: 266983 Color: 17

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 383381 Color: 13
Size: 350243 Color: 11
Size: 266377 Color: 8

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 383384 Color: 13
Size: 310071 Color: 1
Size: 306546 Color: 7

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 383507 Color: 2
Size: 314675 Color: 1
Size: 301819 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 383385 Color: 4
Size: 312834 Color: 3
Size: 303782 Color: 8

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 383403 Color: 5
Size: 335421 Color: 16
Size: 281177 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 383452 Color: 19
Size: 356057 Color: 13
Size: 260492 Color: 18

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 383473 Color: 11
Size: 314842 Color: 1
Size: 301686 Color: 11

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 383522 Color: 2
Size: 321698 Color: 10
Size: 294781 Color: 13

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 383505 Color: 0
Size: 354511 Color: 12
Size: 261985 Color: 4

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 383523 Color: 6
Size: 365295 Color: 19
Size: 251183 Color: 19

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 383523 Color: 0
Size: 349516 Color: 12
Size: 266962 Color: 13

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 383530 Color: 7
Size: 311221 Color: 5
Size: 305250 Color: 9

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 383580 Color: 15
Size: 339596 Color: 1
Size: 276825 Color: 3

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 383808 Color: 8
Size: 328441 Color: 2
Size: 287752 Color: 10

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 383900 Color: 6
Size: 313440 Color: 8
Size: 302661 Color: 18

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 383910 Color: 8
Size: 340375 Color: 6
Size: 275716 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 383975 Color: 18
Size: 308624 Color: 8
Size: 307402 Color: 19

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 384026 Color: 4
Size: 342586 Color: 4
Size: 273389 Color: 7

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 384065 Color: 13
Size: 344204 Color: 19
Size: 271732 Color: 1

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 384177 Color: 2
Size: 318850 Color: 18
Size: 296974 Color: 12

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 384119 Color: 7
Size: 354872 Color: 16
Size: 261010 Color: 4

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 384144 Color: 3
Size: 322747 Color: 10
Size: 293110 Color: 1

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 384180 Color: 19
Size: 361198 Color: 16
Size: 254623 Color: 19

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 384187 Color: 6
Size: 316133 Color: 17
Size: 299681 Color: 3

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 384187 Color: 12
Size: 326036 Color: 5
Size: 289778 Color: 12

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 384253 Color: 13
Size: 340781 Color: 15
Size: 274967 Color: 9

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 384291 Color: 13
Size: 313516 Color: 6
Size: 302194 Color: 7

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 384335 Color: 17
Size: 362323 Color: 6
Size: 253343 Color: 11

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 384338 Color: 2
Size: 345930 Color: 16
Size: 269733 Color: 12

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 384353 Color: 16
Size: 343760 Color: 2
Size: 271888 Color: 8

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 384457 Color: 16
Size: 343893 Color: 5
Size: 271651 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 384459 Color: 3
Size: 352203 Color: 5
Size: 263339 Color: 7

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 384560 Color: 17
Size: 309935 Color: 18
Size: 305506 Color: 6

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 384742 Color: 1
Size: 329692 Color: 6
Size: 285567 Color: 2

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 384801 Color: 10
Size: 309236 Color: 8
Size: 305964 Color: 19

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 384935 Color: 6
Size: 359032 Color: 11
Size: 256034 Color: 10

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 384938 Color: 10
Size: 332099 Color: 12
Size: 282964 Color: 2

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 384964 Color: 5
Size: 359804 Color: 8
Size: 255233 Color: 1

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 385018 Color: 19
Size: 325266 Color: 1
Size: 289717 Color: 14

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 385032 Color: 9
Size: 327651 Color: 13
Size: 287318 Color: 11

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 385033 Color: 18
Size: 331659 Color: 4
Size: 283309 Color: 9

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 385058 Color: 5
Size: 356870 Color: 15
Size: 258073 Color: 1

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 385096 Color: 1
Size: 364489 Color: 17
Size: 250416 Color: 5

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 385145 Color: 9
Size: 341985 Color: 7
Size: 272871 Color: 13

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 385164 Color: 16
Size: 318495 Color: 0
Size: 296342 Color: 7

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 385217 Color: 17
Size: 311482 Color: 12
Size: 303302 Color: 16

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 385261 Color: 11
Size: 359929 Color: 18
Size: 254811 Color: 17

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 385271 Color: 4
Size: 364491 Color: 14
Size: 250239 Color: 19

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 385385 Color: 5
Size: 339533 Color: 1
Size: 275083 Color: 3

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 385311 Color: 17
Size: 358289 Color: 16
Size: 256401 Color: 19

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 385370 Color: 3
Size: 332050 Color: 8
Size: 282581 Color: 13

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 385411 Color: 17
Size: 358579 Color: 12
Size: 256011 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 385453 Color: 14
Size: 311270 Color: 6
Size: 303278 Color: 12

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 385538 Color: 10
Size: 346038 Color: 17
Size: 268425 Color: 12

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 385605 Color: 8
Size: 347920 Color: 10
Size: 266476 Color: 16

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 385631 Color: 0
Size: 308919 Color: 16
Size: 305451 Color: 0

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 385637 Color: 18
Size: 356577 Color: 13
Size: 257787 Color: 15

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 385697 Color: 6
Size: 343678 Color: 16
Size: 270626 Color: 4

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 385703 Color: 1
Size: 344062 Color: 10
Size: 270236 Color: 9

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 385724 Color: 19
Size: 329086 Color: 2
Size: 285191 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 385772 Color: 12
Size: 351818 Color: 17
Size: 262411 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 385822 Color: 8
Size: 307258 Color: 0
Size: 306921 Color: 7

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 385851 Color: 18
Size: 308010 Color: 6
Size: 306140 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 385962 Color: 18
Size: 363775 Color: 0
Size: 250264 Color: 17

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 386035 Color: 17
Size: 345785 Color: 11
Size: 268181 Color: 5

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 386160 Color: 6
Size: 343268 Color: 2
Size: 270573 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 386198 Color: 16
Size: 328240 Color: 6
Size: 285563 Color: 8

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 386222 Color: 16
Size: 308870 Color: 7
Size: 304909 Color: 8

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 386227 Color: 14
Size: 350900 Color: 18
Size: 262874 Color: 6

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 386238 Color: 16
Size: 354755 Color: 9
Size: 259008 Color: 7

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 386238 Color: 0
Size: 311894 Color: 11
Size: 301869 Color: 17

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 386288 Color: 2
Size: 333198 Color: 1
Size: 280515 Color: 7

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 386293 Color: 10
Size: 319369 Color: 3
Size: 294339 Color: 4

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 386302 Color: 2
Size: 334172 Color: 1
Size: 279527 Color: 18

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 386332 Color: 10
Size: 328096 Color: 3
Size: 285573 Color: 15

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 386342 Color: 14
Size: 328347 Color: 0
Size: 285312 Color: 17

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 386377 Color: 10
Size: 353398 Color: 19
Size: 260226 Color: 5

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 386541 Color: 10
Size: 323464 Color: 7
Size: 289996 Color: 14

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 386566 Color: 17
Size: 322753 Color: 12
Size: 290682 Color: 13

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 386582 Color: 18
Size: 339799 Color: 9
Size: 273620 Color: 19

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 386690 Color: 12
Size: 330902 Color: 0
Size: 282409 Color: 18

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 386711 Color: 3
Size: 308902 Color: 0
Size: 304388 Color: 8

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 386753 Color: 3
Size: 335945 Color: 3
Size: 277303 Color: 10

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 386790 Color: 12
Size: 359945 Color: 0
Size: 253266 Color: 18

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 386815 Color: 16
Size: 329260 Color: 12
Size: 283926 Color: 9

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 386834 Color: 11
Size: 334859 Color: 13
Size: 278308 Color: 1

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 386931 Color: 13
Size: 316305 Color: 0
Size: 296765 Color: 15

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 386947 Color: 2
Size: 357438 Color: 9
Size: 255616 Color: 5

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 387063 Color: 6
Size: 350487 Color: 0
Size: 262451 Color: 11

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 387112 Color: 9
Size: 362084 Color: 3
Size: 250805 Color: 8

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 387122 Color: 10
Size: 325820 Color: 17
Size: 287059 Color: 2

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 387171 Color: 17
Size: 326874 Color: 18
Size: 285956 Color: 16

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 387236 Color: 17
Size: 328807 Color: 15
Size: 283958 Color: 8

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 387340 Color: 10
Size: 347143 Color: 17
Size: 265518 Color: 11

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 387271 Color: 6
Size: 360463 Color: 13
Size: 252267 Color: 9

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 387479 Color: 6
Size: 343869 Color: 11
Size: 268653 Color: 4

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 387710 Color: 16
Size: 332540 Color: 10
Size: 279751 Color: 18

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 387734 Color: 3
Size: 352343 Color: 8
Size: 259924 Color: 13

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 387902 Color: 13
Size: 345035 Color: 15
Size: 267064 Color: 8

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 387916 Color: 19
Size: 330554 Color: 16
Size: 281531 Color: 11

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 387936 Color: 11
Size: 307957 Color: 2
Size: 304108 Color: 16

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 388003 Color: 10
Size: 343229 Color: 0
Size: 268769 Color: 12

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 387989 Color: 1
Size: 356825 Color: 6
Size: 255187 Color: 15

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 388069 Color: 11
Size: 313556 Color: 13
Size: 298376 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 388094 Color: 13
Size: 313200 Color: 19
Size: 298707 Color: 9

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 388123 Color: 14
Size: 348997 Color: 9
Size: 262881 Color: 6

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 388148 Color: 5
Size: 349125 Color: 19
Size: 262728 Color: 15

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 388165 Color: 5
Size: 344268 Color: 10
Size: 267568 Color: 11

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 388225 Color: 3
Size: 310868 Color: 1
Size: 300908 Color: 7

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 388263 Color: 13
Size: 343107 Color: 4
Size: 268631 Color: 3

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 388294 Color: 3
Size: 320179 Color: 1
Size: 291528 Color: 9

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 388302 Color: 1
Size: 322535 Color: 14
Size: 289164 Color: 19

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 388329 Color: 11
Size: 310198 Color: 10
Size: 301474 Color: 4

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 388338 Color: 14
Size: 306599 Color: 11
Size: 305064 Color: 2

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 388378 Color: 0
Size: 332846 Color: 16
Size: 278777 Color: 8

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 388481 Color: 6
Size: 323626 Color: 11
Size: 287894 Color: 18

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 388518 Color: 4
Size: 320722 Color: 9
Size: 290761 Color: 6

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 388528 Color: 16
Size: 340484 Color: 11
Size: 270989 Color: 5

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 388575 Color: 10
Size: 344146 Color: 1
Size: 267280 Color: 15

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 388545 Color: 3
Size: 343649 Color: 5
Size: 267807 Color: 9

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 388567 Color: 13
Size: 323286 Color: 3
Size: 288148 Color: 17

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 388673 Color: 16
Size: 309111 Color: 0
Size: 302217 Color: 14

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 388723 Color: 1
Size: 353561 Color: 2
Size: 257717 Color: 10

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 388745 Color: 14
Size: 345468 Color: 9
Size: 265788 Color: 2

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 388745 Color: 5
Size: 309625 Color: 16
Size: 301631 Color: 14

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 388756 Color: 12
Size: 358737 Color: 8
Size: 252508 Color: 16

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 388811 Color: 3
Size: 311035 Color: 4
Size: 300155 Color: 15

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 388852 Color: 8
Size: 322243 Color: 10
Size: 288906 Color: 18

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 388855 Color: 2
Size: 316976 Color: 6
Size: 294170 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 389036 Color: 18
Size: 313158 Color: 13
Size: 297807 Color: 4

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 389062 Color: 7
Size: 354062 Color: 15
Size: 256877 Color: 9

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 389064 Color: 2
Size: 309176 Color: 1
Size: 301761 Color: 1

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 389110 Color: 4
Size: 344336 Color: 6
Size: 266555 Color: 9

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 389164 Color: 1
Size: 311834 Color: 4
Size: 299003 Color: 18

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 389249 Color: 9
Size: 353992 Color: 12
Size: 256760 Color: 4

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 389249 Color: 16
Size: 306601 Color: 7
Size: 304151 Color: 5

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 389275 Color: 6
Size: 310601 Color: 4
Size: 300125 Color: 12

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 389298 Color: 13
Size: 333357 Color: 9
Size: 277346 Color: 19

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 389316 Color: 14
Size: 348067 Color: 10
Size: 262618 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 389322 Color: 9
Size: 335255 Color: 4
Size: 275424 Color: 12

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 389327 Color: 15
Size: 315103 Color: 12
Size: 295571 Color: 11

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 389404 Color: 7
Size: 311757 Color: 3
Size: 298840 Color: 4

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 389421 Color: 12
Size: 314368 Color: 6
Size: 296212 Color: 7

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 389550 Color: 2
Size: 325445 Color: 11
Size: 285006 Color: 7

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 389686 Color: 9
Size: 315889 Color: 7
Size: 294426 Color: 19

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 389704 Color: 2
Size: 349413 Color: 16
Size: 260884 Color: 9

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 389743 Color: 16
Size: 324965 Color: 18
Size: 285293 Color: 12

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 389787 Color: 10
Size: 305913 Color: 4
Size: 304301 Color: 17

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 389748 Color: 0
Size: 351516 Color: 1
Size: 258737 Color: 12

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 389873 Color: 14
Size: 331277 Color: 0
Size: 278851 Color: 9

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 389901 Color: 4
Size: 349400 Color: 18
Size: 260700 Color: 7

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 389919 Color: 17
Size: 307600 Color: 1
Size: 302482 Color: 4

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 389923 Color: 1
Size: 344139 Color: 4
Size: 265939 Color: 12

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 390027 Color: 2
Size: 338331 Color: 17
Size: 271643 Color: 10

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 390067 Color: 15
Size: 324071 Color: 11
Size: 285863 Color: 17

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 390070 Color: 0
Size: 309512 Color: 18
Size: 300419 Color: 19

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 390357 Color: 6
Size: 354925 Color: 16
Size: 254719 Color: 6

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 390364 Color: 9
Size: 320791 Color: 4
Size: 288846 Color: 9

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 390374 Color: 8
Size: 321846 Color: 15
Size: 287781 Color: 18

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 390438 Color: 3
Size: 316094 Color: 15
Size: 293469 Color: 19

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 390511 Color: 14
Size: 353889 Color: 2
Size: 255601 Color: 1

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 390540 Color: 2
Size: 355938 Color: 10
Size: 253523 Color: 12

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 390548 Color: 7
Size: 357998 Color: 6
Size: 251455 Color: 10

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 390617 Color: 17
Size: 316530 Color: 4
Size: 292854 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 390720 Color: 2
Size: 333344 Color: 6
Size: 275937 Color: 18

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 390725 Color: 11
Size: 317833 Color: 5
Size: 291443 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 390778 Color: 9
Size: 353726 Color: 1
Size: 255497 Color: 4

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 390802 Color: 18
Size: 348639 Color: 10
Size: 260560 Color: 13

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 390811 Color: 13
Size: 338872 Color: 1
Size: 270318 Color: 12

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 390892 Color: 16
Size: 309959 Color: 9
Size: 299150 Color: 2

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 390902 Color: 6
Size: 315287 Color: 2
Size: 293812 Color: 17

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 390920 Color: 17
Size: 337754 Color: 14
Size: 271327 Color: 9

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 390960 Color: 14
Size: 309045 Color: 12
Size: 299996 Color: 16

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 391022 Color: 0
Size: 349336 Color: 10
Size: 259643 Color: 16

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 391058 Color: 2
Size: 339123 Color: 1
Size: 269820 Color: 8

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 391087 Color: 16
Size: 339121 Color: 8
Size: 269793 Color: 13

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 391113 Color: 14
Size: 320211 Color: 15
Size: 288677 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 391165 Color: 17
Size: 321239 Color: 11
Size: 287597 Color: 9

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 391171 Color: 13
Size: 338965 Color: 17
Size: 269865 Color: 3

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 391174 Color: 9
Size: 307847 Color: 18
Size: 300980 Color: 12

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 391186 Color: 6
Size: 339504 Color: 10
Size: 269311 Color: 13

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 391211 Color: 9
Size: 332917 Color: 14
Size: 275873 Color: 17

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 391312 Color: 15
Size: 353471 Color: 16
Size: 255218 Color: 6

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 391385 Color: 13
Size: 350446 Color: 6
Size: 258170 Color: 13

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 391405 Color: 7
Size: 342861 Color: 5
Size: 265735 Color: 13

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 391438 Color: 14
Size: 341042 Color: 3
Size: 267521 Color: 17

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 391440 Color: 18
Size: 312498 Color: 14
Size: 296063 Color: 6

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 391482 Color: 1
Size: 337959 Color: 3
Size: 270560 Color: 9

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 391505 Color: 11
Size: 337603 Color: 0
Size: 270893 Color: 6

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 391549 Color: 3
Size: 345062 Color: 0
Size: 263390 Color: 10

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 391559 Color: 1
Size: 337981 Color: 2
Size: 270461 Color: 16

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 391560 Color: 19
Size: 352955 Color: 0
Size: 255486 Color: 6

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 391565 Color: 3
Size: 306934 Color: 8
Size: 301502 Color: 18

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 391579 Color: 19
Size: 349769 Color: 2
Size: 258653 Color: 13

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 391624 Color: 14
Size: 355390 Color: 19
Size: 252987 Color: 13

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 391686 Color: 0
Size: 315847 Color: 10
Size: 292468 Color: 2

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 391769 Color: 0
Size: 335749 Color: 0
Size: 272483 Color: 15

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 391670 Color: 6
Size: 307189 Color: 1
Size: 301142 Color: 3

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 391692 Color: 17
Size: 340651 Color: 6
Size: 267658 Color: 15

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 391705 Color: 12
Size: 344297 Color: 14
Size: 263999 Color: 4

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 391796 Color: 0
Size: 313641 Color: 17
Size: 294564 Color: 9

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 391734 Color: 4
Size: 343255 Color: 9
Size: 265012 Color: 3

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 391752 Color: 2
Size: 326700 Color: 1
Size: 281549 Color: 11

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 391767 Color: 13
Size: 312963 Color: 2
Size: 295271 Color: 19

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 391815 Color: 3
Size: 331800 Color: 10
Size: 276386 Color: 12

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 391827 Color: 8
Size: 318557 Color: 0
Size: 289617 Color: 7

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 391844 Color: 17
Size: 327488 Color: 5
Size: 280669 Color: 6

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 391888 Color: 5
Size: 311111 Color: 2
Size: 297002 Color: 2

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 391896 Color: 13
Size: 313852 Color: 15
Size: 294253 Color: 10

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 391985 Color: 7
Size: 353311 Color: 18
Size: 254705 Color: 3

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 391994 Color: 13
Size: 310898 Color: 1
Size: 297109 Color: 19

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 392057 Color: 0
Size: 330517 Color: 18
Size: 277427 Color: 17

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 392014 Color: 16
Size: 339078 Color: 8
Size: 268909 Color: 3

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 392042 Color: 4
Size: 329915 Color: 19
Size: 278044 Color: 7

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 392046 Color: 12
Size: 355442 Color: 6
Size: 252513 Color: 8

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 392076 Color: 5
Size: 341175 Color: 9
Size: 266750 Color: 7

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 392078 Color: 15
Size: 318399 Color: 18
Size: 289524 Color: 14

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 392137 Color: 10
Size: 312148 Color: 4
Size: 295716 Color: 5

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 392145 Color: 19
Size: 342941 Color: 12
Size: 264915 Color: 17

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 392150 Color: 6
Size: 343985 Color: 18
Size: 263866 Color: 16

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 7
Size: 322238 Color: 5
Size: 285609 Color: 4

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 392197 Color: 13
Size: 338929 Color: 4
Size: 268875 Color: 10

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 392210 Color: 19
Size: 341185 Color: 17
Size: 266606 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 392235 Color: 18
Size: 329208 Color: 6
Size: 278558 Color: 16

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 392239 Color: 10
Size: 354233 Color: 12
Size: 253529 Color: 9

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 392258 Color: 7
Size: 324991 Color: 16
Size: 282752 Color: 9

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 392309 Color: 14
Size: 318683 Color: 4
Size: 289009 Color: 17

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 392311 Color: 9
Size: 335684 Color: 15
Size: 272006 Color: 12

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 392402 Color: 11
Size: 348753 Color: 10
Size: 258846 Color: 1

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 392594 Color: 0
Size: 330057 Color: 14
Size: 277350 Color: 6

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 392747 Color: 7
Size: 331388 Color: 19
Size: 275866 Color: 18

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 392751 Color: 8
Size: 312331 Color: 18
Size: 294919 Color: 1

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 392808 Color: 18
Size: 331211 Color: 10
Size: 275982 Color: 1

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 392874 Color: 4
Size: 306240 Color: 15
Size: 300887 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 392949 Color: 3
Size: 332071 Color: 8
Size: 274981 Color: 4

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 392957 Color: 5
Size: 348935 Color: 14
Size: 258109 Color: 11

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 393133 Color: 5
Size: 305015 Color: 9
Size: 301853 Color: 13

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 393221 Color: 18
Size: 305118 Color: 17
Size: 301662 Color: 14

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 393226 Color: 6
Size: 338361 Color: 0
Size: 268414 Color: 15

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 393240 Color: 7
Size: 323860 Color: 8
Size: 282901 Color: 15

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 393243 Color: 9
Size: 346789 Color: 1
Size: 259969 Color: 2

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 393269 Color: 19
Size: 340764 Color: 16
Size: 265968 Color: 6

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 393338 Color: 3
Size: 346538 Color: 10
Size: 260125 Color: 5

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 393380 Color: 1
Size: 345942 Color: 6
Size: 260679 Color: 17

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 393402 Color: 5
Size: 343384 Color: 0
Size: 263215 Color: 11

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 393412 Color: 16
Size: 354224 Color: 13
Size: 252365 Color: 18

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 393428 Color: 13
Size: 324460 Color: 8
Size: 282113 Color: 5

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 393476 Color: 12
Size: 307365 Color: 6
Size: 299160 Color: 1

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 393478 Color: 19
Size: 355637 Color: 9
Size: 250886 Color: 19

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 393480 Color: 18
Size: 307960 Color: 1
Size: 298561 Color: 8

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 393502 Color: 14
Size: 327834 Color: 13
Size: 278665 Color: 17

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 393566 Color: 19
Size: 343717 Color: 10
Size: 262718 Color: 7

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 393629 Color: 10
Size: 350230 Color: 7
Size: 256142 Color: 13

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 393672 Color: 1
Size: 307780 Color: 6
Size: 298549 Color: 11

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 393721 Color: 4
Size: 331082 Color: 15
Size: 275198 Color: 17

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 393796 Color: 8
Size: 330729 Color: 13
Size: 275476 Color: 2

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 393852 Color: 13
Size: 335077 Color: 5
Size: 271072 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 394040 Color: 12
Size: 306520 Color: 12
Size: 299441 Color: 11

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 394132 Color: 19
Size: 343226 Color: 14
Size: 262643 Color: 7

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 394144 Color: 2
Size: 330989 Color: 19
Size: 274868 Color: 6

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 394169 Color: 11
Size: 347606 Color: 13
Size: 258226 Color: 5

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 394186 Color: 1
Size: 319978 Color: 10
Size: 285837 Color: 11

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 394194 Color: 19
Size: 324216 Color: 13
Size: 281591 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 394324 Color: 11
Size: 337050 Color: 13
Size: 268627 Color: 12

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 394345 Color: 3
Size: 305633 Color: 12
Size: 300023 Color: 19

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 394346 Color: 18
Size: 340121 Color: 1
Size: 265534 Color: 2

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 394358 Color: 13
Size: 337113 Color: 4
Size: 268530 Color: 19

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 394363 Color: 4
Size: 337308 Color: 2
Size: 268330 Color: 10

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 394389 Color: 19
Size: 304520 Color: 4
Size: 301092 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 394397 Color: 14
Size: 333556 Color: 1
Size: 272048 Color: 6

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 394420 Color: 17
Size: 328840 Color: 11
Size: 276741 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 394446 Color: 19
Size: 345749 Color: 13
Size: 259806 Color: 18

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 394498 Color: 6
Size: 352736 Color: 3
Size: 252767 Color: 2

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 394505 Color: 14
Size: 353100 Color: 2
Size: 252396 Color: 7

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 394541 Color: 15
Size: 321043 Color: 12
Size: 284417 Color: 5

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 394560 Color: 12
Size: 322837 Color: 18
Size: 282604 Color: 6

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 394496 Color: 10
Size: 327225 Color: 13
Size: 278280 Color: 1

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 394584 Color: 0
Size: 309399 Color: 6
Size: 296018 Color: 19

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 394565 Color: 15
Size: 335795 Color: 14
Size: 269641 Color: 15

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 394572 Color: 1
Size: 305994 Color: 8
Size: 299435 Color: 15

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 394515 Color: 10
Size: 345001 Color: 18
Size: 260485 Color: 18

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 394744 Color: 11
Size: 334740 Color: 0
Size: 270517 Color: 8

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 394819 Color: 5
Size: 310779 Color: 15
Size: 294403 Color: 15

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 394880 Color: 17
Size: 334224 Color: 13
Size: 270897 Color: 1

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 394905 Color: 9
Size: 351134 Color: 12
Size: 253962 Color: 8

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 394913 Color: 2
Size: 322879 Color: 3
Size: 282209 Color: 4

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 394982 Color: 13
Size: 309976 Color: 10
Size: 295043 Color: 17

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 395002 Color: 3
Size: 304023 Color: 9
Size: 300976 Color: 6

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 395044 Color: 1
Size: 345799 Color: 8
Size: 259158 Color: 7

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 395065 Color: 7
Size: 308778 Color: 14
Size: 296158 Color: 11

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 395066 Color: 16
Size: 308105 Color: 18
Size: 296830 Color: 2

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 395140 Color: 7
Size: 333315 Color: 5
Size: 271546 Color: 4

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 395169 Color: 19
Size: 333505 Color: 5
Size: 271327 Color: 2

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 395186 Color: 14
Size: 348500 Color: 0
Size: 256315 Color: 17

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 395296 Color: 2
Size: 323017 Color: 15
Size: 281688 Color: 1

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 395300 Color: 16
Size: 326814 Color: 9
Size: 277887 Color: 2

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 395304 Color: 5
Size: 307592 Color: 11
Size: 297105 Color: 3

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 395364 Color: 7
Size: 340109 Color: 11
Size: 264528 Color: 3

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 395383 Color: 8
Size: 328671 Color: 19
Size: 275947 Color: 15

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 395389 Color: 15
Size: 345759 Color: 0
Size: 258853 Color: 1

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 395493 Color: 9
Size: 334406 Color: 6
Size: 270102 Color: 4

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 395502 Color: 19
Size: 318915 Color: 15
Size: 285584 Color: 14

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 395514 Color: 10
Size: 325504 Color: 13
Size: 278983 Color: 15

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 395527 Color: 7
Size: 351242 Color: 1
Size: 253232 Color: 6

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 395571 Color: 14
Size: 354196 Color: 13
Size: 250234 Color: 5

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 395670 Color: 0
Size: 331365 Color: 9
Size: 272966 Color: 6

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 395624 Color: 15
Size: 330260 Color: 1
Size: 274117 Color: 3

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 395659 Color: 4
Size: 328870 Color: 14
Size: 275472 Color: 10

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 395819 Color: 8
Size: 318053 Color: 6
Size: 286129 Color: 14

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 395868 Color: 13
Size: 323517 Color: 12
Size: 280616 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 395897 Color: 15
Size: 348505 Color: 10
Size: 255599 Color: 10

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 395898 Color: 7
Size: 314699 Color: 17
Size: 289404 Color: 4

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 395970 Color: 13
Size: 314676 Color: 15
Size: 289355 Color: 13

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 395974 Color: 14
Size: 315016 Color: 1
Size: 289011 Color: 19

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 396038 Color: 11
Size: 329177 Color: 3
Size: 274786 Color: 3

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 396141 Color: 0
Size: 305275 Color: 4
Size: 298585 Color: 17

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 396060 Color: 7
Size: 352001 Color: 9
Size: 251940 Color: 2

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 396063 Color: 9
Size: 336764 Color: 8
Size: 267174 Color: 2

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 396139 Color: 5
Size: 328789 Color: 8
Size: 275073 Color: 12

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 396148 Color: 18
Size: 323683 Color: 12
Size: 280170 Color: 18

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 396201 Color: 10
Size: 347201 Color: 8
Size: 256599 Color: 2

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 396243 Color: 18
Size: 330781 Color: 10
Size: 272977 Color: 8

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 396262 Color: 11
Size: 311952 Color: 9
Size: 291787 Color: 12

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 396264 Color: 15
Size: 341979 Color: 7
Size: 261758 Color: 14

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 396295 Color: 18
Size: 351287 Color: 16
Size: 252419 Color: 13

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 396358 Color: 9
Size: 336636 Color: 1
Size: 267007 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 396395 Color: 5
Size: 350599 Color: 9
Size: 253007 Color: 16

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 396455 Color: 8
Size: 306506 Color: 6
Size: 297040 Color: 6

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 396501 Color: 10
Size: 302192 Color: 15
Size: 301308 Color: 17

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 396505 Color: 7
Size: 305903 Color: 18
Size: 297593 Color: 19

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 396630 Color: 14
Size: 327970 Color: 0
Size: 275401 Color: 3

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 396748 Color: 18
Size: 317915 Color: 19
Size: 285338 Color: 14

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 396935 Color: 4
Size: 304442 Color: 15
Size: 298624 Color: 2

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 397035 Color: 13
Size: 307766 Color: 16
Size: 295200 Color: 10

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 397207 Color: 0
Size: 347915 Color: 4
Size: 254879 Color: 12

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 397219 Color: 5
Size: 342695 Color: 6
Size: 260087 Color: 2

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 397277 Color: 2
Size: 328525 Color: 5
Size: 274199 Color: 12

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 397284 Color: 15
Size: 312419 Color: 18
Size: 290298 Color: 3

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 397336 Color: 0
Size: 351640 Color: 10
Size: 251025 Color: 19

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 397297 Color: 3
Size: 338155 Color: 15
Size: 264549 Color: 18

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 397312 Color: 4
Size: 328239 Color: 13
Size: 274450 Color: 5

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 397378 Color: 9
Size: 334798 Color: 8
Size: 267825 Color: 18

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 397462 Color: 8
Size: 324058 Color: 16
Size: 278481 Color: 2

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 397513 Color: 5
Size: 303898 Color: 15
Size: 298590 Color: 15

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 397603 Color: 3
Size: 316313 Color: 7
Size: 286085 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 397616 Color: 4
Size: 335885 Color: 18
Size: 266500 Color: 17

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 397631 Color: 12
Size: 313823 Color: 4
Size: 288547 Color: 12

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 397657 Color: 5
Size: 314135 Color: 1
Size: 288209 Color: 12

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 397674 Color: 2
Size: 308893 Color: 15
Size: 293434 Color: 4

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 397696 Color: 15
Size: 320692 Color: 8
Size: 281613 Color: 7

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 397721 Color: 3
Size: 310922 Color: 9
Size: 291358 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 397754 Color: 17
Size: 320043 Color: 4
Size: 282204 Color: 13

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 397786 Color: 2
Size: 343237 Color: 12
Size: 258978 Color: 1

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 397793 Color: 12
Size: 302506 Color: 11
Size: 299702 Color: 9

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 397903 Color: 1
Size: 315025 Color: 14
Size: 287073 Color: 15

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 397977 Color: 10
Size: 310236 Color: 16
Size: 291788 Color: 4

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 398012 Color: 15
Size: 344926 Color: 10
Size: 257063 Color: 7

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 398075 Color: 4
Size: 349645 Color: 6
Size: 252281 Color: 2

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 398083 Color: 2
Size: 304224 Color: 6
Size: 297694 Color: 15

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 398183 Color: 5
Size: 338648 Color: 16
Size: 263170 Color: 1

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 398228 Color: 1
Size: 332359 Color: 19
Size: 269414 Color: 16

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 398232 Color: 6
Size: 332610 Color: 5
Size: 269159 Color: 10

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 398303 Color: 8
Size: 301438 Color: 16
Size: 300260 Color: 13

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 398311 Color: 6
Size: 308826 Color: 1
Size: 292864 Color: 19

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 398382 Color: 12
Size: 317648 Color: 5
Size: 283971 Color: 3

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 398523 Color: 2
Size: 305918 Color: 5
Size: 295560 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 398590 Color: 4
Size: 342195 Color: 13
Size: 259216 Color: 15

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 398606 Color: 10
Size: 340874 Color: 4
Size: 260521 Color: 4

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 398624 Color: 14
Size: 322068 Color: 9
Size: 279309 Color: 6

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 398746 Color: 4
Size: 340178 Color: 14
Size: 261077 Color: 5

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 398816 Color: 0
Size: 323947 Color: 12
Size: 277238 Color: 19

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 398847 Color: 9
Size: 337158 Color: 1
Size: 263996 Color: 3

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 398892 Color: 4
Size: 319519 Color: 17
Size: 281590 Color: 16

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 398985 Color: 2
Size: 345729 Color: 10
Size: 255287 Color: 6

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 399008 Color: 8
Size: 316480 Color: 9
Size: 284513 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 399082 Color: 17
Size: 312493 Color: 17
Size: 288426 Color: 8

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 399112 Color: 13
Size: 340043 Color: 7
Size: 260846 Color: 17

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 399199 Color: 15
Size: 341733 Color: 13
Size: 259069 Color: 18

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 399227 Color: 18
Size: 305221 Color: 3
Size: 295553 Color: 10

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 399361 Color: 9
Size: 323043 Color: 6
Size: 277597 Color: 15

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 399363 Color: 18
Size: 342695 Color: 18
Size: 257943 Color: 7

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 399363 Color: 16
Size: 342368 Color: 13
Size: 258270 Color: 13

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 399418 Color: 12
Size: 345591 Color: 0
Size: 254992 Color: 5

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 399436 Color: 7
Size: 315178 Color: 15
Size: 285387 Color: 10

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 399458 Color: 4
Size: 330840 Color: 13
Size: 269703 Color: 4

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 399539 Color: 16
Size: 323095 Color: 7
Size: 277367 Color: 1

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 399627 Color: 2
Size: 318698 Color: 17
Size: 281676 Color: 12

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 399728 Color: 0
Size: 318758 Color: 13
Size: 281515 Color: 12

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 399849 Color: 7
Size: 327569 Color: 14
Size: 272583 Color: 16

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 399850 Color: 17
Size: 344724 Color: 5
Size: 255427 Color: 11

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 399865 Color: 3
Size: 349765 Color: 0
Size: 250371 Color: 18

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 399878 Color: 1
Size: 348953 Color: 10
Size: 251170 Color: 1

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 399879 Color: 19
Size: 328935 Color: 2
Size: 271187 Color: 2

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 399910 Color: 4
Size: 304128 Color: 7
Size: 295963 Color: 1

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 399919 Color: 8
Size: 303993 Color: 7
Size: 296089 Color: 7

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 399989 Color: 8
Size: 301338 Color: 6
Size: 298674 Color: 14

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 399998 Color: 12
Size: 316224 Color: 14
Size: 283779 Color: 16

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 400055 Color: 0
Size: 315710 Color: 3
Size: 284236 Color: 1

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 400056 Color: 12
Size: 329054 Color: 10
Size: 270891 Color: 17

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 400083 Color: 1
Size: 312588 Color: 14
Size: 287330 Color: 3

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 400091 Color: 0
Size: 346144 Color: 4
Size: 253766 Color: 15

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 400140 Color: 15
Size: 323846 Color: 1
Size: 276015 Color: 3

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 400196 Color: 11
Size: 339001 Color: 10
Size: 260804 Color: 13

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 400201 Color: 13
Size: 347416 Color: 3
Size: 252384 Color: 12

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 400287 Color: 13
Size: 331805 Color: 4
Size: 267909 Color: 14

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 400296 Color: 17
Size: 335230 Color: 19
Size: 264475 Color: 9

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 400325 Color: 3
Size: 326014 Color: 8
Size: 273662 Color: 14

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 400353 Color: 1
Size: 343662 Color: 7
Size: 255986 Color: 16

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 400461 Color: 15
Size: 319977 Color: 5
Size: 279563 Color: 11

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 400495 Color: 17
Size: 327595 Color: 16
Size: 271911 Color: 6

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 400521 Color: 7
Size: 344332 Color: 8
Size: 255148 Color: 4

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 400531 Color: 19
Size: 336280 Color: 3
Size: 263190 Color: 13

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 400586 Color: 17
Size: 312916 Color: 4
Size: 286499 Color: 8

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 400604 Color: 2
Size: 304351 Color: 8
Size: 295046 Color: 10

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 400648 Color: 2
Size: 300599 Color: 1
Size: 298754 Color: 3

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 400670 Color: 9
Size: 320901 Color: 5
Size: 278430 Color: 18

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 400688 Color: 0
Size: 324917 Color: 17
Size: 274396 Color: 15

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 400803 Color: 10
Size: 323141 Color: 12
Size: 276057 Color: 3

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 400741 Color: 13
Size: 343482 Color: 6
Size: 255778 Color: 17

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 400801 Color: 18
Size: 316115 Color: 4
Size: 283085 Color: 17

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 400839 Color: 2
Size: 318573 Color: 17
Size: 280589 Color: 11

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 400881 Color: 8
Size: 307824 Color: 7
Size: 291296 Color: 6

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 400939 Color: 9
Size: 335834 Color: 19
Size: 263228 Color: 5

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 401075 Color: 10
Size: 308318 Color: 11
Size: 290608 Color: 16

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 401001 Color: 8
Size: 312160 Color: 3
Size: 286840 Color: 13

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 401012 Color: 13
Size: 345804 Color: 4
Size: 253185 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 401114 Color: 11
Size: 333891 Color: 14
Size: 264996 Color: 1

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 401118 Color: 17
Size: 340135 Color: 10
Size: 258748 Color: 14

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 401131 Color: 17
Size: 320642 Color: 8
Size: 278228 Color: 19

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 401162 Color: 3
Size: 308975 Color: 7
Size: 289864 Color: 13

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 401195 Color: 15
Size: 337418 Color: 4
Size: 261388 Color: 16

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 401247 Color: 19
Size: 307124 Color: 5
Size: 291630 Color: 2

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 401282 Color: 13
Size: 318116 Color: 0
Size: 280603 Color: 2

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 401329 Color: 10
Size: 333647 Color: 17
Size: 265025 Color: 14

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 401313 Color: 17
Size: 302712 Color: 14
Size: 295976 Color: 14

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 401327 Color: 8
Size: 334249 Color: 18
Size: 264425 Color: 9

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 401427 Color: 0
Size: 300701 Color: 1
Size: 297873 Color: 14

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 401432 Color: 17
Size: 334553 Color: 4
Size: 264016 Color: 12

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 401603 Color: 15
Size: 317398 Color: 0
Size: 281000 Color: 14

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 401633 Color: 5
Size: 302735 Color: 7
Size: 295633 Color: 18

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 401685 Color: 11
Size: 333661 Color: 1
Size: 264655 Color: 19

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 401824 Color: 12
Size: 331517 Color: 8
Size: 266660 Color: 6

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 401880 Color: 0
Size: 306836 Color: 3
Size: 291285 Color: 16

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 402040 Color: 1
Size: 333413 Color: 2
Size: 264548 Color: 4

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 402096 Color: 19
Size: 316988 Color: 14
Size: 280917 Color: 10

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 402119 Color: 5
Size: 303095 Color: 1
Size: 294787 Color: 15

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 402367 Color: 18
Size: 323153 Color: 3
Size: 274481 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 402449 Color: 14
Size: 306668 Color: 0
Size: 290884 Color: 8

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 402499 Color: 7
Size: 307103 Color: 9
Size: 290399 Color: 6

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 402721 Color: 10
Size: 337968 Color: 16
Size: 259312 Color: 4

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 402552 Color: 9
Size: 340948 Color: 15
Size: 256501 Color: 14

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 402581 Color: 17
Size: 306772 Color: 15
Size: 290648 Color: 5

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 402592 Color: 13
Size: 305363 Color: 15
Size: 292046 Color: 3

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 402649 Color: 2
Size: 317076 Color: 16
Size: 280276 Color: 2

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 402728 Color: 11
Size: 305447 Color: 10
Size: 291826 Color: 16

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 402756 Color: 9
Size: 305366 Color: 17
Size: 291879 Color: 8

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 402782 Color: 2
Size: 300792 Color: 18
Size: 296427 Color: 16

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 402790 Color: 5
Size: 308135 Color: 15
Size: 289076 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 402830 Color: 14
Size: 340713 Color: 7
Size: 256458 Color: 8

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 402878 Color: 8
Size: 344370 Color: 12
Size: 252753 Color: 11

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 402915 Color: 6
Size: 325168 Color: 5
Size: 271918 Color: 5

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 402984 Color: 11
Size: 317936 Color: 17
Size: 279081 Color: 8

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 402992 Color: 18
Size: 308300 Color: 19
Size: 288709 Color: 11

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 403010 Color: 5
Size: 320483 Color: 12
Size: 276508 Color: 16

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 403016 Color: 13
Size: 305471 Color: 16
Size: 291514 Color: 10

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 403090 Color: 6
Size: 311614 Color: 13
Size: 285297 Color: 11

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 403130 Color: 5
Size: 331629 Color: 9
Size: 265242 Color: 16

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 403181 Color: 1
Size: 339262 Color: 10
Size: 257558 Color: 18

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 403183 Color: 11
Size: 309031 Color: 5
Size: 287787 Color: 13

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 403201 Color: 19
Size: 309077 Color: 15
Size: 287723 Color: 16

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 403222 Color: 19
Size: 301041 Color: 8
Size: 295738 Color: 14

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 403248 Color: 12
Size: 326036 Color: 17
Size: 270717 Color: 17

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 403295 Color: 4
Size: 313928 Color: 8
Size: 282778 Color: 10

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 403313 Color: 16
Size: 330942 Color: 6
Size: 265746 Color: 8

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 403386 Color: 4
Size: 310952 Color: 7
Size: 285663 Color: 12

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 403438 Color: 9
Size: 330969 Color: 11
Size: 265594 Color: 9

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 403456 Color: 19
Size: 339834 Color: 14
Size: 256711 Color: 12

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 403474 Color: 0
Size: 344175 Color: 3
Size: 252352 Color: 19

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 403489 Color: 9
Size: 309018 Color: 1
Size: 287494 Color: 10

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 403498 Color: 16
Size: 307361 Color: 7
Size: 289142 Color: 9

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 403521 Color: 19
Size: 336413 Color: 9
Size: 260067 Color: 13

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 403556 Color: 8
Size: 304018 Color: 0
Size: 292427 Color: 3

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 403593 Color: 14
Size: 338411 Color: 2
Size: 257997 Color: 8

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 403614 Color: 10
Size: 317479 Color: 0
Size: 278908 Color: 5

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 403632 Color: 13
Size: 326591 Color: 2
Size: 269778 Color: 8

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 403669 Color: 8
Size: 312562 Color: 19
Size: 283770 Color: 5

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 403686 Color: 0
Size: 316571 Color: 14
Size: 279744 Color: 1

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 403703 Color: 7
Size: 298368 Color: 19
Size: 297930 Color: 12

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 403745 Color: 9
Size: 343557 Color: 17
Size: 252699 Color: 2

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 403838 Color: 5
Size: 344591 Color: 10
Size: 251572 Color: 6

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 403906 Color: 13
Size: 342073 Color: 18
Size: 254022 Color: 2

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 404125 Color: 7
Size: 312597 Color: 3
Size: 283279 Color: 6

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 404168 Color: 1
Size: 336976 Color: 17
Size: 258857 Color: 12

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 404262 Color: 3
Size: 322908 Color: 8
Size: 272831 Color: 2

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 404379 Color: 4
Size: 343721 Color: 16
Size: 251901 Color: 16

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 404384 Color: 13
Size: 334227 Color: 6
Size: 261390 Color: 10

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 404389 Color: 5
Size: 300544 Color: 0
Size: 295068 Color: 6

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 404402 Color: 2
Size: 325270 Color: 0
Size: 270329 Color: 16

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 404487 Color: 13
Size: 304760 Color: 7
Size: 290754 Color: 17

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 404496 Color: 4
Size: 313624 Color: 8
Size: 281881 Color: 19

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 404507 Color: 17
Size: 326275 Color: 13
Size: 269219 Color: 18

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 404567 Color: 18
Size: 344110 Color: 7
Size: 251324 Color: 11

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 404588 Color: 14
Size: 325453 Color: 6
Size: 269960 Color: 6

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 404804 Color: 10
Size: 310413 Color: 6
Size: 284784 Color: 16

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 404770 Color: 3
Size: 342615 Color: 9
Size: 252616 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 404786 Color: 13
Size: 306147 Color: 2
Size: 289068 Color: 15

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 404789 Color: 16
Size: 325297 Color: 12
Size: 269915 Color: 15

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 404793 Color: 14
Size: 344171 Color: 6
Size: 251037 Color: 1

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 404842 Color: 1
Size: 317517 Color: 10
Size: 277642 Color: 5

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 404842 Color: 4
Size: 315201 Color: 2
Size: 279958 Color: 11

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 404854 Color: 8
Size: 332072 Color: 14
Size: 263075 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 404882 Color: 6
Size: 311686 Color: 3
Size: 283433 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 404928 Color: 7
Size: 343973 Color: 14
Size: 251100 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 404988 Color: 12
Size: 326733 Color: 1
Size: 268280 Color: 7

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 405036 Color: 4
Size: 307712 Color: 10
Size: 287253 Color: 11

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 405062 Color: 19
Size: 304522 Color: 2
Size: 290417 Color: 13

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 405072 Color: 15
Size: 325688 Color: 18
Size: 269241 Color: 6

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 405108 Color: 1
Size: 315374 Color: 11
Size: 279519 Color: 6

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 405127 Color: 1
Size: 343102 Color: 11
Size: 251772 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 405150 Color: 12
Size: 328003 Color: 4
Size: 266848 Color: 14

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 405176 Color: 5
Size: 307016 Color: 1
Size: 287809 Color: 18

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 405227 Color: 14
Size: 332134 Color: 8
Size: 262640 Color: 9

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 405256 Color: 8
Size: 309407 Color: 16
Size: 285338 Color: 9

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 405292 Color: 18
Size: 298819 Color: 16
Size: 295890 Color: 12

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 405299 Color: 18
Size: 330599 Color: 12
Size: 264103 Color: 19

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 405306 Color: 12
Size: 306838 Color: 5
Size: 287857 Color: 10

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 405327 Color: 18
Size: 300004 Color: 19
Size: 294670 Color: 6

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 405438 Color: 7
Size: 322247 Color: 16
Size: 272316 Color: 14

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 405477 Color: 7
Size: 335516 Color: 0
Size: 259008 Color: 19

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 405622 Color: 18
Size: 335105 Color: 2
Size: 259274 Color: 8

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 405712 Color: 16
Size: 339056 Color: 19
Size: 255233 Color: 5

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 405887 Color: 14
Size: 338797 Color: 3
Size: 255317 Color: 3

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 405938 Color: 0
Size: 300424 Color: 13
Size: 293639 Color: 3

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 406046 Color: 4
Size: 329636 Color: 5
Size: 264319 Color: 17

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 406088 Color: 15
Size: 341686 Color: 12
Size: 252227 Color: 5

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 406107 Color: 5
Size: 307743 Color: 7
Size: 286151 Color: 18

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 406111 Color: 0
Size: 298793 Color: 10
Size: 295097 Color: 14

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 406152 Color: 4
Size: 327275 Color: 7
Size: 266574 Color: 2

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 406233 Color: 11
Size: 325256 Color: 6
Size: 268512 Color: 16

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 406238 Color: 18
Size: 299597 Color: 1
Size: 294166 Color: 7

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 406271 Color: 3
Size: 331947 Color: 19
Size: 261783 Color: 9

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 406277 Color: 8
Size: 314964 Color: 10
Size: 278760 Color: 9

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 406419 Color: 14
Size: 325251 Color: 14
Size: 268331 Color: 8

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 406439 Color: 18
Size: 297039 Color: 4
Size: 296523 Color: 5

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 406519 Color: 7
Size: 329720 Color: 8
Size: 263762 Color: 19

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 406593 Color: 7
Size: 314409 Color: 1
Size: 278999 Color: 7

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 406736 Color: 10
Size: 305205 Color: 3
Size: 288060 Color: 5

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 406653 Color: 1
Size: 335222 Color: 8
Size: 258126 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 406674 Color: 0
Size: 334597 Color: 12
Size: 258730 Color: 6

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 406688 Color: 17
Size: 325997 Color: 8
Size: 267316 Color: 18

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 406740 Color: 12
Size: 313849 Color: 6
Size: 279412 Color: 13

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 406761 Color: 13
Size: 315176 Color: 13
Size: 278064 Color: 10

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 406799 Color: 8
Size: 307203 Color: 13
Size: 285999 Color: 3

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 406891 Color: 1
Size: 319329 Color: 13
Size: 273781 Color: 15

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 406904 Color: 1
Size: 331418 Color: 11
Size: 261679 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 406928 Color: 2
Size: 302464 Color: 4
Size: 290609 Color: 14

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 407005 Color: 3
Size: 306243 Color: 10
Size: 286753 Color: 5

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 407025 Color: 19
Size: 310813 Color: 8
Size: 282163 Color: 8

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 407028 Color: 7
Size: 332505 Color: 4
Size: 260468 Color: 9

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 407037 Color: 13
Size: 315442 Color: 14
Size: 277522 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 407103 Color: 1
Size: 327464 Color: 19
Size: 265434 Color: 10

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 407184 Color: 18
Size: 324863 Color: 1
Size: 267954 Color: 4

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 407229 Color: 1
Size: 326344 Color: 16
Size: 266428 Color: 19

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 407267 Color: 12
Size: 305814 Color: 9
Size: 286920 Color: 14

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 407383 Color: 0
Size: 304091 Color: 17
Size: 288527 Color: 15

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 407424 Color: 16
Size: 324066 Color: 18
Size: 268511 Color: 10

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 407426 Color: 14
Size: 318178 Color: 11
Size: 274397 Color: 1

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 407435 Color: 19
Size: 301578 Color: 4
Size: 290988 Color: 5

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 407493 Color: 18
Size: 328913 Color: 8
Size: 263595 Color: 19

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 407588 Color: 8
Size: 308622 Color: 18
Size: 283791 Color: 3

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 407647 Color: 7
Size: 300929 Color: 10
Size: 291425 Color: 16

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 407823 Color: 12
Size: 320724 Color: 14
Size: 271454 Color: 11

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 407926 Color: 2
Size: 297679 Color: 5
Size: 294396 Color: 16

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 408034 Color: 3
Size: 313263 Color: 9
Size: 278704 Color: 15

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 408131 Color: 4
Size: 341271 Color: 0
Size: 250599 Color: 8

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 408181 Color: 9
Size: 309213 Color: 12
Size: 282607 Color: 2

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 408410 Color: 10
Size: 338732 Color: 14
Size: 252859 Color: 5

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 408218 Color: 5
Size: 304185 Color: 8
Size: 287598 Color: 18

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 408239 Color: 11
Size: 306924 Color: 0
Size: 284838 Color: 3

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 408306 Color: 6
Size: 337974 Color: 12
Size: 253721 Color: 7

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 408341 Color: 16
Size: 341053 Color: 16
Size: 250607 Color: 9

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 408342 Color: 2
Size: 329848 Color: 5
Size: 261811 Color: 16

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 408408 Color: 4
Size: 305072 Color: 9
Size: 286521 Color: 9

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 408482 Color: 17
Size: 331000 Color: 7
Size: 260519 Color: 18

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 408559 Color: 17
Size: 327193 Color: 7
Size: 264249 Color: 10

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 408657 Color: 3
Size: 312937 Color: 4
Size: 278407 Color: 13

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 408731 Color: 1
Size: 323638 Color: 8
Size: 267632 Color: 17

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 408774 Color: 18
Size: 296373 Color: 8
Size: 294854 Color: 2

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 408788 Color: 11
Size: 298189 Color: 19
Size: 293024 Color: 16

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 408834 Color: 17
Size: 329472 Color: 7
Size: 261695 Color: 10

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 409101 Color: 9
Size: 331185 Color: 11
Size: 259715 Color: 8

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 409125 Color: 1
Size: 309984 Color: 3
Size: 280892 Color: 11

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 409191 Color: 4
Size: 333433 Color: 8
Size: 257377 Color: 12

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 409213 Color: 13
Size: 340212 Color: 10
Size: 250576 Color: 3

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 409260 Color: 14
Size: 302770 Color: 0
Size: 287971 Color: 2

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 409262 Color: 18
Size: 325269 Color: 6
Size: 265470 Color: 15

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 409277 Color: 1
Size: 318247 Color: 17
Size: 272477 Color: 5

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 409308 Color: 4
Size: 337870 Color: 9
Size: 252823 Color: 4

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 409340 Color: 14
Size: 321787 Color: 2
Size: 268874 Color: 5

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 409356 Color: 12
Size: 299073 Color: 1
Size: 291572 Color: 10

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 409364 Color: 14
Size: 326240 Color: 2
Size: 264397 Color: 8

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 409418 Color: 12
Size: 330248 Color: 11
Size: 260335 Color: 9

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 409435 Color: 2
Size: 295314 Color: 12
Size: 295252 Color: 6

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 409436 Color: 18
Size: 325603 Color: 2
Size: 264962 Color: 9

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 409452 Color: 19
Size: 338269 Color: 10
Size: 252280 Color: 14

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 409458 Color: 15
Size: 330462 Color: 5
Size: 260081 Color: 6

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 409551 Color: 8
Size: 327778 Color: 18
Size: 262672 Color: 13

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 409602 Color: 1
Size: 327141 Color: 15
Size: 263258 Color: 8

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 409612 Color: 5
Size: 317017 Color: 1
Size: 273372 Color: 3

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 409747 Color: 15
Size: 335266 Color: 10
Size: 254988 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 409755 Color: 8
Size: 335301 Color: 12
Size: 254945 Color: 5

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 409772 Color: 19
Size: 313545 Color: 1
Size: 276684 Color: 7

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 409813 Color: 3
Size: 322579 Color: 9
Size: 267609 Color: 4

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 409889 Color: 6
Size: 323156 Color: 12
Size: 266956 Color: 11

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 409905 Color: 3
Size: 318148 Color: 3
Size: 271948 Color: 16

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 410057 Color: 4
Size: 306066 Color: 16
Size: 283878 Color: 14

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 410140 Color: 9
Size: 307793 Color: 7
Size: 282068 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 410316 Color: 10
Size: 326702 Color: 0
Size: 262983 Color: 5

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 410268 Color: 9
Size: 315619 Color: 13
Size: 274114 Color: 15

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 410349 Color: 13
Size: 312856 Color: 2
Size: 276796 Color: 7

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 410354 Color: 1
Size: 336383 Color: 6
Size: 253264 Color: 13

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 410382 Color: 6
Size: 295781 Color: 5
Size: 293838 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 410553 Color: 10
Size: 304351 Color: 8
Size: 285097 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 410500 Color: 16
Size: 300065 Color: 3
Size: 289436 Color: 12

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 410523 Color: 12
Size: 326045 Color: 12
Size: 263433 Color: 19

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 410561 Color: 17
Size: 335655 Color: 10
Size: 253785 Color: 8

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 410603 Color: 8
Size: 316660 Color: 4
Size: 272738 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 410619 Color: 17
Size: 321886 Color: 2
Size: 267496 Color: 1

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 410623 Color: 17
Size: 294892 Color: 18
Size: 294486 Color: 18

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 410634 Color: 13
Size: 299905 Color: 1
Size: 289462 Color: 13

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 410647 Color: 12
Size: 318470 Color: 19
Size: 270884 Color: 8

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 410657 Color: 17
Size: 315230 Color: 12
Size: 274114 Color: 2

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 410676 Color: 11
Size: 323658 Color: 8
Size: 265667 Color: 16

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 410960 Color: 10
Size: 306308 Color: 18
Size: 282733 Color: 1

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 410849 Color: 17
Size: 326059 Color: 14
Size: 263093 Color: 11

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 410849 Color: 1
Size: 320846 Color: 17
Size: 268306 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 410883 Color: 6
Size: 334417 Color: 13
Size: 254701 Color: 16

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 410911 Color: 16
Size: 298505 Color: 8
Size: 290585 Color: 2

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 411055 Color: 10
Size: 312391 Color: 15
Size: 276555 Color: 8

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 410917 Color: 15
Size: 338864 Color: 4
Size: 250220 Color: 18

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 411030 Color: 11
Size: 335026 Color: 1
Size: 253945 Color: 4

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 411042 Color: 8
Size: 315300 Color: 16
Size: 273659 Color: 7

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 411064 Color: 18
Size: 320739 Color: 13
Size: 268198 Color: 10

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 411112 Color: 12
Size: 317279 Color: 19
Size: 271610 Color: 4

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 411122 Color: 3
Size: 331513 Color: 4
Size: 257366 Color: 19

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 411137 Color: 15
Size: 320593 Color: 0
Size: 268271 Color: 8

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 411139 Color: 19
Size: 324020 Color: 18
Size: 264842 Color: 13

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 411322 Color: 10
Size: 300443 Color: 3
Size: 288236 Color: 6

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 411312 Color: 13
Size: 335721 Color: 11
Size: 252968 Color: 6

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 411375 Color: 4
Size: 311121 Color: 8
Size: 277505 Color: 12

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 411376 Color: 1
Size: 321879 Color: 2
Size: 266746 Color: 4

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 411495 Color: 13
Size: 328654 Color: 9
Size: 259852 Color: 2

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 411513 Color: 12
Size: 317081 Color: 0
Size: 271407 Color: 7

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 411535 Color: 7
Size: 302674 Color: 3
Size: 285792 Color: 10

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 411547 Color: 13
Size: 313966 Color: 13
Size: 274488 Color: 9

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 411569 Color: 9
Size: 311024 Color: 19
Size: 277408 Color: 11

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 411671 Color: 16
Size: 336454 Color: 15
Size: 251876 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 411709 Color: 7
Size: 324298 Color: 13
Size: 263994 Color: 10

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 411820 Color: 5
Size: 315647 Color: 18
Size: 272534 Color: 9

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 411846 Color: 7
Size: 329729 Color: 5
Size: 258426 Color: 19

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 411965 Color: 17
Size: 296019 Color: 7
Size: 292017 Color: 15

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 412164 Color: 8
Size: 309947 Color: 7
Size: 277890 Color: 10

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 412238 Color: 14
Size: 330745 Color: 9
Size: 257018 Color: 4

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 412249 Color: 19
Size: 315485 Color: 4
Size: 272267 Color: 4

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 412258 Color: 14
Size: 332191 Color: 17
Size: 255552 Color: 1

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 412260 Color: 6
Size: 295217 Color: 15
Size: 292524 Color: 16

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 412275 Color: 12
Size: 312194 Color: 18
Size: 275532 Color: 8

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 412376 Color: 11
Size: 316423 Color: 10
Size: 271202 Color: 6

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 412379 Color: 12
Size: 323223 Color: 1
Size: 264399 Color: 13

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 412429 Color: 5
Size: 304672 Color: 12
Size: 282900 Color: 11

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 412459 Color: 17
Size: 300536 Color: 13
Size: 287006 Color: 19

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 412470 Color: 16
Size: 328326 Color: 7
Size: 259205 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 412472 Color: 9
Size: 336420 Color: 12
Size: 251109 Color: 7

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 412472 Color: 6
Size: 318882 Color: 8
Size: 268647 Color: 5

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 412546 Color: 6
Size: 324876 Color: 10
Size: 262579 Color: 18

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 412587 Color: 16
Size: 325785 Color: 0
Size: 261629 Color: 2

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 412593 Color: 19
Size: 330868 Color: 4
Size: 256540 Color: 15

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 412615 Color: 15
Size: 333701 Color: 5
Size: 253685 Color: 14

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 412685 Color: 12
Size: 328308 Color: 6
Size: 259008 Color: 3

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 412790 Color: 15
Size: 321054 Color: 9
Size: 266157 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 412877 Color: 9
Size: 320100 Color: 18
Size: 267024 Color: 6

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 412889 Color: 14
Size: 327121 Color: 17
Size: 259991 Color: 17

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 412984 Color: 13
Size: 321477 Color: 19
Size: 265540 Color: 4

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 413143 Color: 2
Size: 329093 Color: 1
Size: 257765 Color: 11

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 413205 Color: 0
Size: 303848 Color: 14
Size: 282948 Color: 12

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 413242 Color: 19
Size: 322495 Color: 13
Size: 264264 Color: 10

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 413278 Color: 7
Size: 324119 Color: 8
Size: 262604 Color: 5

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 413428 Color: 18
Size: 316376 Color: 12
Size: 270197 Color: 2

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 413440 Color: 16
Size: 331432 Color: 0
Size: 255129 Color: 4

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 413524 Color: 17
Size: 322782 Color: 0
Size: 263695 Color: 9

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 413588 Color: 11
Size: 302351 Color: 13
Size: 284062 Color: 13

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 413606 Color: 19
Size: 294897 Color: 15
Size: 291498 Color: 16

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 413606 Color: 18
Size: 294002 Color: 5
Size: 292393 Color: 11

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 413746 Color: 10
Size: 294122 Color: 9
Size: 292133 Color: 6

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 413627 Color: 3
Size: 322995 Color: 12
Size: 263379 Color: 4

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 413627 Color: 1
Size: 311151 Color: 17
Size: 275223 Color: 12

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 413655 Color: 7
Size: 298929 Color: 16
Size: 287417 Color: 12

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 413846 Color: 6
Size: 296685 Color: 13
Size: 289470 Color: 17

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 413927 Color: 10
Size: 313414 Color: 15
Size: 272660 Color: 18

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 413912 Color: 17
Size: 335991 Color: 11
Size: 250098 Color: 15

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 413932 Color: 17
Size: 299723 Color: 8
Size: 286346 Color: 3

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 414006 Color: 0
Size: 325139 Color: 1
Size: 260856 Color: 3

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 414064 Color: 8
Size: 316368 Color: 6
Size: 269569 Color: 19

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 414099 Color: 15
Size: 324904 Color: 2
Size: 260998 Color: 10

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 414137 Color: 17
Size: 328663 Color: 14
Size: 257201 Color: 5

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 414148 Color: 4
Size: 311575 Color: 1
Size: 274278 Color: 15

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 414182 Color: 12
Size: 299086 Color: 14
Size: 286733 Color: 19

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 414186 Color: 4
Size: 297678 Color: 5
Size: 288137 Color: 15

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 414257 Color: 8
Size: 304955 Color: 16
Size: 280789 Color: 12

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 414278 Color: 3
Size: 319530 Color: 10
Size: 266193 Color: 12

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 414291 Color: 15
Size: 316509 Color: 1
Size: 269201 Color: 11

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 414356 Color: 18
Size: 318762 Color: 7
Size: 266883 Color: 7

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 414413 Color: 2
Size: 332936 Color: 13
Size: 252652 Color: 3

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 414466 Color: 6
Size: 317985 Color: 14
Size: 267550 Color: 11

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 414481 Color: 3
Size: 332869 Color: 17
Size: 252651 Color: 17

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 414510 Color: 9
Size: 303461 Color: 16
Size: 282030 Color: 2

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 414571 Color: 16
Size: 332163 Color: 15
Size: 253267 Color: 19

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 414572 Color: 17
Size: 295086 Color: 8
Size: 290343 Color: 12

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 414603 Color: 14
Size: 299923 Color: 10
Size: 285475 Color: 18

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 414636 Color: 1
Size: 320809 Color: 12
Size: 264556 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 414644 Color: 13
Size: 327925 Color: 15
Size: 257432 Color: 5

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 414703 Color: 3
Size: 311040 Color: 2
Size: 274258 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 414710 Color: 4
Size: 306651 Color: 1
Size: 278640 Color: 11

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 414761 Color: 6
Size: 321778 Color: 10
Size: 263462 Color: 4

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 414776 Color: 0
Size: 321427 Color: 5
Size: 263798 Color: 18

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 414916 Color: 3
Size: 303444 Color: 8
Size: 281641 Color: 15

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 415065 Color: 16
Size: 292955 Color: 0
Size: 291981 Color: 12

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 415088 Color: 8
Size: 326387 Color: 14
Size: 258526 Color: 4

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 415124 Color: 9
Size: 304836 Color: 3
Size: 280041 Color: 17

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 415166 Color: 2
Size: 296926 Color: 10
Size: 287909 Color: 2

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 415179 Color: 15
Size: 304855 Color: 5
Size: 279967 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 415282 Color: 5
Size: 313871 Color: 17
Size: 270848 Color: 1

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 415306 Color: 19
Size: 293222 Color: 1
Size: 291473 Color: 18

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 415358 Color: 19
Size: 301147 Color: 3
Size: 283496 Color: 15

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 415362 Color: 18
Size: 293803 Color: 4
Size: 290836 Color: 17

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 415391 Color: 2
Size: 312281 Color: 18
Size: 272329 Color: 16

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 415548 Color: 17
Size: 328529 Color: 6
Size: 255924 Color: 15

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 415559 Color: 6
Size: 303626 Color: 17
Size: 280816 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 415569 Color: 9
Size: 314272 Color: 18
Size: 270160 Color: 9

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 415651 Color: 12
Size: 314572 Color: 6
Size: 269778 Color: 9

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 415737 Color: 3
Size: 304563 Color: 18
Size: 279701 Color: 10

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 415792 Color: 14
Size: 297014 Color: 8
Size: 287195 Color: 7

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 415874 Color: 2
Size: 311610 Color: 4
Size: 272517 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 415891 Color: 15
Size: 328095 Color: 7
Size: 256015 Color: 17

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 415959 Color: 18
Size: 321402 Color: 16
Size: 262640 Color: 19

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 415976 Color: 15
Size: 329850 Color: 19
Size: 254175 Color: 10

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 415982 Color: 5
Size: 331340 Color: 16
Size: 252679 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 416005 Color: 16
Size: 330025 Color: 6
Size: 253971 Color: 11

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 416048 Color: 2
Size: 316436 Color: 13
Size: 267517 Color: 5

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 416099 Color: 17
Size: 303568 Color: 4
Size: 280334 Color: 15

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 416138 Color: 10
Size: 313214 Color: 4
Size: 270649 Color: 2

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 416152 Color: 19
Size: 330828 Color: 14
Size: 253021 Color: 12

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 416200 Color: 13
Size: 301683 Color: 14
Size: 282118 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 416217 Color: 14
Size: 293422 Color: 17
Size: 290362 Color: 12

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 416269 Color: 19
Size: 309376 Color: 10
Size: 274356 Color: 12

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 416324 Color: 11
Size: 309825 Color: 13
Size: 273852 Color: 8

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 416340 Color: 16
Size: 300455 Color: 7
Size: 283206 Color: 11

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 416350 Color: 14
Size: 315642 Color: 8
Size: 268009 Color: 6

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 416412 Color: 16
Size: 308438 Color: 13
Size: 275151 Color: 3

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 416463 Color: 11
Size: 318049 Color: 11
Size: 265489 Color: 2

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 416583 Color: 10
Size: 331841 Color: 15
Size: 251577 Color: 7

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 416494 Color: 3
Size: 325022 Color: 1
Size: 258485 Color: 1

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 416507 Color: 2
Size: 311968 Color: 13
Size: 271526 Color: 18

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 416526 Color: 5
Size: 303406 Color: 1
Size: 280069 Color: 6

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 416554 Color: 16
Size: 317115 Color: 5
Size: 266332 Color: 14

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 416586 Color: 0
Size: 332886 Color: 10
Size: 250529 Color: 6

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 416612 Color: 5
Size: 296735 Color: 0
Size: 286654 Color: 13

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 416613 Color: 11
Size: 328720 Color: 3
Size: 254668 Color: 9

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 416617 Color: 11
Size: 315851 Color: 0
Size: 267533 Color: 18

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 416618 Color: 3
Size: 313595 Color: 6
Size: 269788 Color: 2

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 416703 Color: 18
Size: 298336 Color: 14
Size: 284962 Color: 10

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 416706 Color: 11
Size: 295900 Color: 18
Size: 287395 Color: 17

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 416712 Color: 12
Size: 299231 Color: 19
Size: 284058 Color: 1

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 416716 Color: 16
Size: 323268 Color: 12
Size: 260017 Color: 12

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 416771 Color: 19
Size: 320455 Color: 19
Size: 262775 Color: 15

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 416921 Color: 11
Size: 324011 Color: 19
Size: 259069 Color: 18

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 416980 Color: 15
Size: 294512 Color: 8
Size: 288509 Color: 16

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 417026 Color: 1
Size: 293833 Color: 13
Size: 289142 Color: 3

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 417030 Color: 11
Size: 318032 Color: 19
Size: 264939 Color: 6

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 417107 Color: 7
Size: 293335 Color: 1
Size: 289559 Color: 15

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 417319 Color: 18
Size: 331507 Color: 13
Size: 251175 Color: 10

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 417392 Color: 11
Size: 313387 Color: 19
Size: 269222 Color: 12

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 417442 Color: 4
Size: 298840 Color: 0
Size: 283719 Color: 11

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 417443 Color: 19
Size: 298165 Color: 0
Size: 284393 Color: 7

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 417444 Color: 11
Size: 315690 Color: 8
Size: 266867 Color: 12

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 417511 Color: 19
Size: 315261 Color: 12
Size: 267229 Color: 18

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 417533 Color: 5
Size: 313091 Color: 18
Size: 269377 Color: 10

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 417626 Color: 3
Size: 296478 Color: 11
Size: 285897 Color: 1

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 417748 Color: 11
Size: 331112 Color: 2
Size: 251141 Color: 8

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 417774 Color: 18
Size: 301702 Color: 5
Size: 280525 Color: 11

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 417800 Color: 16
Size: 321047 Color: 4
Size: 261154 Color: 9

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 417838 Color: 1
Size: 316054 Color: 15
Size: 266109 Color: 3

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 417920 Color: 10
Size: 322655 Color: 7
Size: 259426 Color: 3

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 417881 Color: 13
Size: 330626 Color: 18
Size: 251494 Color: 9

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 417948 Color: 16
Size: 301632 Color: 15
Size: 280421 Color: 1

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 417958 Color: 8
Size: 299679 Color: 14
Size: 282364 Color: 2

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 417986 Color: 12
Size: 329390 Color: 12
Size: 252625 Color: 13

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 417996 Color: 1
Size: 315657 Color: 0
Size: 266348 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 418052 Color: 19
Size: 327656 Color: 3
Size: 254293 Color: 10

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 418069 Color: 15
Size: 318964 Color: 0
Size: 262968 Color: 13

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 418197 Color: 13
Size: 307984 Color: 1
Size: 273820 Color: 1

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 418210 Color: 9
Size: 306235 Color: 1
Size: 275556 Color: 12

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 418243 Color: 14
Size: 301372 Color: 14
Size: 280386 Color: 18

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 418244 Color: 14
Size: 308200 Color: 4
Size: 273557 Color: 12

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 418282 Color: 14
Size: 313433 Color: 16
Size: 268286 Color: 18

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 418291 Color: 16
Size: 292768 Color: 19
Size: 288942 Color: 8

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 418594 Color: 16
Size: 314063 Color: 0
Size: 267344 Color: 14

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 418691 Color: 12
Size: 291111 Color: 14
Size: 290199 Color: 2

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 418718 Color: 9
Size: 315991 Color: 10
Size: 265292 Color: 15

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 418792 Color: 1
Size: 305469 Color: 7
Size: 275740 Color: 9

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 418863 Color: 8
Size: 292042 Color: 3
Size: 289096 Color: 5

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 418978 Color: 1
Size: 326919 Color: 4
Size: 254104 Color: 5

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 419013 Color: 15
Size: 325741 Color: 6
Size: 255247 Color: 14

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 419044 Color: 15
Size: 324168 Color: 10
Size: 256789 Color: 11

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 419073 Color: 3
Size: 303317 Color: 17
Size: 277611 Color: 6

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 419098 Color: 16
Size: 328413 Color: 3
Size: 252490 Color: 16

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 419112 Color: 16
Size: 294137 Color: 5
Size: 286752 Color: 9

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 419360 Color: 7
Size: 324779 Color: 6
Size: 255862 Color: 8

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 419362 Color: 7
Size: 312394 Color: 18
Size: 268245 Color: 11

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 419368 Color: 8
Size: 295689 Color: 10
Size: 284944 Color: 18

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 419383 Color: 11
Size: 309114 Color: 1
Size: 271504 Color: 14

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 419390 Color: 13
Size: 325768 Color: 0
Size: 254843 Color: 5

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 419408 Color: 12
Size: 307143 Color: 15
Size: 273450 Color: 16

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 419483 Color: 15
Size: 310689 Color: 9
Size: 269829 Color: 3

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 419556 Color: 11
Size: 310521 Color: 10
Size: 269924 Color: 9

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 419570 Color: 6
Size: 316756 Color: 16
Size: 263675 Color: 19

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 419577 Color: 6
Size: 312893 Color: 5
Size: 267531 Color: 14

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 419599 Color: 1
Size: 318547 Color: 6
Size: 261855 Color: 3

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 419632 Color: 0
Size: 293119 Color: 9
Size: 287250 Color: 10

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 419674 Color: 1
Size: 323973 Color: 17
Size: 256354 Color: 2

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 419692 Color: 0
Size: 306105 Color: 0
Size: 274204 Color: 13

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 419708 Color: 13
Size: 296436 Color: 4
Size: 283857 Color: 9

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 419709 Color: 3
Size: 297638 Color: 16
Size: 282654 Color: 2

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 419770 Color: 9
Size: 309775 Color: 15
Size: 270456 Color: 3

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 419763 Color: 10
Size: 326749 Color: 7
Size: 253489 Color: 19

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 419838 Color: 13
Size: 296347 Color: 9
Size: 283816 Color: 6

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 419894 Color: 16
Size: 314575 Color: 6
Size: 265532 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 419908 Color: 18
Size: 291879 Color: 7
Size: 288214 Color: 9

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 419999 Color: 7
Size: 318245 Color: 1
Size: 261757 Color: 9

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 420016 Color: 18
Size: 295320 Color: 10
Size: 284665 Color: 17

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 420110 Color: 12
Size: 313007 Color: 2
Size: 266884 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 420171 Color: 1
Size: 328338 Color: 14
Size: 251492 Color: 15

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 420181 Color: 9
Size: 319797 Color: 1
Size: 260023 Color: 5

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 420207 Color: 12
Size: 313562 Color: 13
Size: 266232 Color: 9

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 420268 Color: 2
Size: 319998 Color: 17
Size: 259735 Color: 13

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 420316 Color: 0
Size: 326780 Color: 18
Size: 252905 Color: 10

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 420331 Color: 11
Size: 290228 Color: 0
Size: 289442 Color: 14

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 420437 Color: 13
Size: 299046 Color: 18
Size: 280518 Color: 5

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 420605 Color: 16
Size: 328439 Color: 16
Size: 250957 Color: 6

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 420608 Color: 13
Size: 294005 Color: 4
Size: 285388 Color: 11

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 420631 Color: 19
Size: 297061 Color: 8
Size: 282309 Color: 8

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 420666 Color: 13
Size: 308775 Color: 4
Size: 270560 Color: 10

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 420675 Color: 14
Size: 292558 Color: 3
Size: 286768 Color: 5

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 420676 Color: 11
Size: 314877 Color: 16
Size: 264448 Color: 16

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 420710 Color: 7
Size: 290669 Color: 0
Size: 288622 Color: 16

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 420717 Color: 2
Size: 292802 Color: 15
Size: 286482 Color: 3

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 420885 Color: 3
Size: 316947 Color: 10
Size: 262169 Color: 16

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 420986 Color: 1
Size: 328023 Color: 19
Size: 250992 Color: 13

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 421054 Color: 17
Size: 320888 Color: 6
Size: 258059 Color: 14

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 421065 Color: 13
Size: 323413 Color: 17
Size: 255523 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 421103 Color: 2
Size: 305327 Color: 11
Size: 273571 Color: 9

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 421104 Color: 15
Size: 303722 Color: 1
Size: 275175 Color: 3

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 421151 Color: 8
Size: 309036 Color: 10
Size: 269814 Color: 4

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 421238 Color: 0
Size: 321702 Color: 9
Size: 257061 Color: 3

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 421312 Color: 18
Size: 318241 Color: 4
Size: 260448 Color: 19

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 421420 Color: 14
Size: 316577 Color: 16
Size: 262004 Color: 16

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 421527 Color: 9
Size: 289357 Color: 2
Size: 289117 Color: 8

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 421560 Color: 17
Size: 325358 Color: 7
Size: 253083 Color: 10

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 421586 Color: 0
Size: 316206 Color: 9
Size: 262209 Color: 12

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 421701 Color: 18
Size: 322287 Color: 6
Size: 256013 Color: 4

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 421707 Color: 13
Size: 296725 Color: 18
Size: 281569 Color: 9

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 421789 Color: 19
Size: 319471 Color: 15
Size: 258741 Color: 6

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 421910 Color: 8
Size: 301095 Color: 10
Size: 276996 Color: 16

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 421932 Color: 13
Size: 317239 Color: 9
Size: 260830 Color: 18

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 421942 Color: 18
Size: 326194 Color: 17
Size: 251865 Color: 15

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 421956 Color: 3
Size: 313354 Color: 4
Size: 264691 Color: 2

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 422089 Color: 5
Size: 325021 Color: 15
Size: 252891 Color: 13

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 422120 Color: 17
Size: 297043 Color: 1
Size: 280838 Color: 9

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 422149 Color: 16
Size: 291328 Color: 5
Size: 286524 Color: 17

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 422233 Color: 17
Size: 323962 Color: 3
Size: 253806 Color: 17

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 422390 Color: 10
Size: 310617 Color: 2
Size: 266994 Color: 12

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 422408 Color: 19
Size: 303565 Color: 18
Size: 274028 Color: 14

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 422448 Color: 7
Size: 311781 Color: 8
Size: 265772 Color: 7

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 422450 Color: 7
Size: 315087 Color: 15
Size: 262464 Color: 10

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 422477 Color: 3
Size: 327125 Color: 2
Size: 250399 Color: 1

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 422527 Color: 15
Size: 315634 Color: 11
Size: 261840 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 422527 Color: 19
Size: 301043 Color: 12
Size: 276431 Color: 3

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 422534 Color: 11
Size: 319834 Color: 0
Size: 257633 Color: 19

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 422558 Color: 3
Size: 318083 Color: 5
Size: 259360 Color: 5

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 422610 Color: 13
Size: 299372 Color: 19
Size: 278019 Color: 9

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 422774 Color: 10
Size: 306462 Color: 6
Size: 270765 Color: 17

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 422687 Color: 18
Size: 298054 Color: 5
Size: 279260 Color: 3

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 422757 Color: 13
Size: 288638 Color: 15
Size: 288606 Color: 8

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 422767 Color: 5
Size: 308930 Color: 3
Size: 268304 Color: 16

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 422850 Color: 10
Size: 313429 Color: 6
Size: 263722 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 422784 Color: 7
Size: 292825 Color: 0
Size: 284392 Color: 7

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 422835 Color: 18
Size: 302530 Color: 7
Size: 274636 Color: 3

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 422857 Color: 11
Size: 312923 Color: 13
Size: 264221 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 422942 Color: 3
Size: 323772 Color: 3
Size: 253287 Color: 1

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 422952 Color: 4
Size: 307751 Color: 8
Size: 269298 Color: 5

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 422975 Color: 4
Size: 323983 Color: 10
Size: 253043 Color: 1

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 423031 Color: 9
Size: 294240 Color: 15
Size: 282730 Color: 5

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 423067 Color: 11
Size: 324824 Color: 16
Size: 252110 Color: 12

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 423078 Color: 7
Size: 324335 Color: 7
Size: 252588 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 423181 Color: 6
Size: 316054 Color: 17
Size: 260766 Color: 6

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 423326 Color: 5
Size: 312317 Color: 16
Size: 264358 Color: 16

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 423411 Color: 10
Size: 319544 Color: 13
Size: 257046 Color: 2

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 423346 Color: 11
Size: 297384 Color: 7
Size: 279271 Color: 19

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 423379 Color: 0
Size: 314033 Color: 13
Size: 262589 Color: 9

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 423416 Color: 1
Size: 321718 Color: 7
Size: 254867 Color: 12

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 423547 Color: 18
Size: 317470 Color: 3
Size: 258984 Color: 14

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 423564 Color: 5
Size: 306936 Color: 13
Size: 269501 Color: 16

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 423572 Color: 4
Size: 288583 Color: 1
Size: 287846 Color: 12

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 423572 Color: 3
Size: 294584 Color: 6
Size: 281845 Color: 17

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 423822 Color: 10
Size: 319616 Color: 18
Size: 256563 Color: 5

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 423588 Color: 9
Size: 292418 Color: 12
Size: 283995 Color: 8

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 423601 Color: 0
Size: 314390 Color: 4
Size: 262010 Color: 13

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 423854 Color: 10
Size: 294224 Color: 13
Size: 281923 Color: 1

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 423864 Color: 9
Size: 295467 Color: 16
Size: 280670 Color: 14

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 423873 Color: 13
Size: 291787 Color: 15
Size: 284341 Color: 5

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 423950 Color: 5
Size: 306902 Color: 18
Size: 269149 Color: 16

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 423955 Color: 19
Size: 312608 Color: 4
Size: 263438 Color: 15

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 424176 Color: 6
Size: 295687 Color: 0
Size: 280138 Color: 15

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 424255 Color: 18
Size: 322082 Color: 16
Size: 253664 Color: 6

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 424293 Color: 5
Size: 296801 Color: 4
Size: 278907 Color: 12

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 424445 Color: 12
Size: 311193 Color: 7
Size: 264363 Color: 7

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 424593 Color: 10
Size: 292079 Color: 0
Size: 283329 Color: 11

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 424538 Color: 19
Size: 323023 Color: 5
Size: 252440 Color: 17

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 424554 Color: 5
Size: 309878 Color: 17
Size: 265569 Color: 19

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 424630 Color: 19
Size: 304342 Color: 18
Size: 271029 Color: 9

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 424695 Color: 15
Size: 290050 Color: 19
Size: 285256 Color: 18

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 424988 Color: 10
Size: 295747 Color: 1
Size: 279266 Color: 15

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 424921 Color: 7
Size: 323864 Color: 7
Size: 251216 Color: 19

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 424956 Color: 6
Size: 324677 Color: 8
Size: 250368 Color: 13

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 425071 Color: 6
Size: 301089 Color: 5
Size: 273841 Color: 2

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 425087 Color: 16
Size: 318340 Color: 5
Size: 256574 Color: 3

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 425157 Color: 8
Size: 311124 Color: 8
Size: 263720 Color: 10

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 425192 Color: 13
Size: 317935 Color: 4
Size: 256874 Color: 4

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 425194 Color: 12
Size: 308906 Color: 2
Size: 265901 Color: 19

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 425202 Color: 7
Size: 319468 Color: 16
Size: 255331 Color: 9

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 425202 Color: 0
Size: 293287 Color: 17
Size: 281512 Color: 6

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 425285 Color: 3
Size: 295404 Color: 12
Size: 279312 Color: 10

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 425286 Color: 12
Size: 312529 Color: 9
Size: 262186 Color: 8

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 425369 Color: 0
Size: 321913 Color: 17
Size: 252719 Color: 11

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 425485 Color: 11
Size: 293783 Color: 14
Size: 280733 Color: 16

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 425505 Color: 11
Size: 294656 Color: 7
Size: 279840 Color: 7

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 425559 Color: 17
Size: 320766 Color: 10
Size: 253676 Color: 18

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 425564 Color: 1
Size: 311855 Color: 13
Size: 262582 Color: 2

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 425592 Color: 18
Size: 305683 Color: 8
Size: 268726 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 425690 Color: 7
Size: 294792 Color: 0
Size: 279519 Color: 8

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 425694 Color: 18
Size: 299541 Color: 7
Size: 274766 Color: 15

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 425801 Color: 8
Size: 288110 Color: 2
Size: 286090 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 425803 Color: 13
Size: 305324 Color: 16
Size: 268874 Color: 6

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 425805 Color: 12
Size: 312201 Color: 6
Size: 261995 Color: 4

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 425857 Color: 6
Size: 303489 Color: 16
Size: 270655 Color: 7

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 425959 Color: 7
Size: 323415 Color: 1
Size: 250627 Color: 17

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 425960 Color: 15
Size: 313252 Color: 9
Size: 260789 Color: 16

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 425983 Color: 0
Size: 287596 Color: 9
Size: 286422 Color: 10

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 426178 Color: 17
Size: 316111 Color: 9
Size: 257712 Color: 15

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 426179 Color: 11
Size: 315570 Color: 0
Size: 258252 Color: 12

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 426316 Color: 2
Size: 288913 Color: 18
Size: 284772 Color: 15

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 426339 Color: 16
Size: 305795 Color: 19
Size: 267867 Color: 8

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 426358 Color: 2
Size: 292179 Color: 7
Size: 281464 Color: 10

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 426483 Color: 4
Size: 287819 Color: 13
Size: 285699 Color: 1

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 426516 Color: 2
Size: 312133 Color: 9
Size: 261352 Color: 1

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 426610 Color: 3
Size: 305122 Color: 1
Size: 268269 Color: 1

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 426652 Color: 15
Size: 319627 Color: 19
Size: 253722 Color: 8

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 426701 Color: 7
Size: 317991 Color: 17
Size: 255309 Color: 13

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 426827 Color: 10
Size: 292877 Color: 17
Size: 280297 Color: 6

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 426779 Color: 4
Size: 301401 Color: 12
Size: 271821 Color: 19

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 426781 Color: 14
Size: 286885 Color: 18
Size: 286335 Color: 9

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 426869 Color: 15
Size: 321128 Color: 3
Size: 252004 Color: 9

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 426923 Color: 6
Size: 287437 Color: 3
Size: 285641 Color: 1

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 426954 Color: 17
Size: 287374 Color: 3
Size: 285673 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 426978 Color: 12
Size: 305206 Color: 19
Size: 267817 Color: 10

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 427000 Color: 5
Size: 319333 Color: 17
Size: 253668 Color: 8

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 427029 Color: 14
Size: 293471 Color: 15
Size: 279501 Color: 13

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 427049 Color: 0
Size: 288092 Color: 15
Size: 284860 Color: 15

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 427072 Color: 11
Size: 310560 Color: 1
Size: 262369 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 427202 Color: 0
Size: 299749 Color: 8
Size: 273050 Color: 6

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 427217 Color: 16
Size: 300330 Color: 3
Size: 272454 Color: 10

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 427232 Color: 18
Size: 319784 Color: 18
Size: 252985 Color: 17

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 427242 Color: 15
Size: 295442 Color: 8
Size: 277317 Color: 13

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 427262 Color: 8
Size: 300573 Color: 9
Size: 272166 Color: 9

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 427272 Color: 11
Size: 304124 Color: 15
Size: 268605 Color: 4

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 427296 Color: 4
Size: 296858 Color: 12
Size: 275847 Color: 11

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 427357 Color: 12
Size: 303557 Color: 17
Size: 269087 Color: 10

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 427374 Color: 7
Size: 317984 Color: 4
Size: 254643 Color: 3

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 427476 Color: 13
Size: 298604 Color: 9
Size: 273921 Color: 12

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 427481 Color: 1
Size: 311754 Color: 18
Size: 260766 Color: 12

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 427534 Color: 15
Size: 289933 Color: 4
Size: 282534 Color: 5

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 427548 Color: 1
Size: 308752 Color: 19
Size: 263701 Color: 18

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 427588 Color: 4
Size: 288104 Color: 7
Size: 284309 Color: 19

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 427606 Color: 8
Size: 289603 Color: 7
Size: 282792 Color: 12

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 427627 Color: 5
Size: 311735 Color: 14
Size: 260639 Color: 15

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 427643 Color: 9
Size: 314286 Color: 3
Size: 258072 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 427809 Color: 10
Size: 303575 Color: 18
Size: 268617 Color: 7

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 427677 Color: 17
Size: 305572 Color: 8
Size: 266752 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 427754 Color: 15
Size: 304138 Color: 2
Size: 268109 Color: 4

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 427831 Color: 14
Size: 288571 Color: 6
Size: 283599 Color: 15

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 427837 Color: 18
Size: 306684 Color: 19
Size: 265480 Color: 19

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 427982 Color: 6
Size: 304225 Color: 8
Size: 267794 Color: 13

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 428095 Color: 10
Size: 309922 Color: 12
Size: 261984 Color: 18

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 427999 Color: 17
Size: 306499 Color: 4
Size: 265503 Color: 15

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 428078 Color: 3
Size: 296029 Color: 7
Size: 275894 Color: 19

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 428113 Color: 8
Size: 286134 Color: 1
Size: 285754 Color: 13

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 428182 Color: 14
Size: 300185 Color: 2
Size: 271634 Color: 10

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 428208 Color: 1
Size: 313764 Color: 17
Size: 258029 Color: 14

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 428293 Color: 19
Size: 300263 Color: 2
Size: 271445 Color: 16

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 428448 Color: 3
Size: 320856 Color: 19
Size: 250697 Color: 6

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 428489 Color: 19
Size: 321099 Color: 9
Size: 250413 Color: 2

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 428542 Color: 2
Size: 299671 Color: 7
Size: 271788 Color: 3

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 428582 Color: 19
Size: 295429 Color: 10
Size: 275990 Color: 18

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 428632 Color: 5
Size: 288098 Color: 11
Size: 283271 Color: 5

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 428661 Color: 16
Size: 289695 Color: 8
Size: 281645 Color: 11

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 428694 Color: 7
Size: 303444 Color: 1
Size: 267863 Color: 14

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 428712 Color: 16
Size: 293926 Color: 13
Size: 277363 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 428725 Color: 14
Size: 316731 Color: 15
Size: 254545 Color: 11

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 428754 Color: 15
Size: 286923 Color: 9
Size: 284324 Color: 10

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 428810 Color: 13
Size: 318317 Color: 1
Size: 252874 Color: 6

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 428939 Color: 13
Size: 318529 Color: 8
Size: 252533 Color: 12

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 428944 Color: 6
Size: 316141 Color: 11
Size: 254916 Color: 1

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 429033 Color: 4
Size: 317311 Color: 8
Size: 253657 Color: 15

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 429093 Color: 8
Size: 287192 Color: 13
Size: 283716 Color: 4

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 429233 Color: 7
Size: 287366 Color: 9
Size: 283402 Color: 10

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 429311 Color: 14
Size: 316385 Color: 2
Size: 254305 Color: 3

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 429324 Color: 6
Size: 295182 Color: 17
Size: 275495 Color: 17

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 429356 Color: 3
Size: 290424 Color: 18
Size: 280221 Color: 16

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 429401 Color: 15
Size: 303777 Color: 9
Size: 266823 Color: 19

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 429423 Color: 7
Size: 315877 Color: 15
Size: 254701 Color: 2

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 429448 Color: 12
Size: 302608 Color: 5
Size: 267945 Color: 3

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 429470 Color: 14
Size: 301951 Color: 9
Size: 268580 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 429562 Color: 16
Size: 310200 Color: 14
Size: 260239 Color: 15

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 429566 Color: 9
Size: 305004 Color: 2
Size: 265431 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 429587 Color: 9
Size: 318787 Color: 0
Size: 251627 Color: 10

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 429590 Color: 2
Size: 302791 Color: 11
Size: 267620 Color: 12

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 429592 Color: 7
Size: 290886 Color: 18
Size: 279523 Color: 17

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 429652 Color: 2
Size: 301063 Color: 2
Size: 269286 Color: 14

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 429735 Color: 15
Size: 289123 Color: 3
Size: 281143 Color: 5

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 429763 Color: 8
Size: 301733 Color: 6
Size: 268505 Color: 3

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 429867 Color: 10
Size: 306708 Color: 5
Size: 263426 Color: 19

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 429812 Color: 13
Size: 314671 Color: 3
Size: 255518 Color: 9

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 429881 Color: 15
Size: 318804 Color: 4
Size: 251316 Color: 13

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 429931 Color: 4
Size: 293658 Color: 10
Size: 276412 Color: 2

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 429948 Color: 15
Size: 316737 Color: 12
Size: 253316 Color: 17

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 429982 Color: 15
Size: 319984 Color: 9
Size: 250035 Color: 2

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 430052 Color: 7
Size: 314315 Color: 15
Size: 255634 Color: 5

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 430095 Color: 13
Size: 313733 Color: 1
Size: 256173 Color: 19

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 430100 Color: 2
Size: 310234 Color: 10
Size: 259667 Color: 17

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 430375 Color: 13
Size: 304219 Color: 7
Size: 265407 Color: 19

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 430443 Color: 13
Size: 290110 Color: 14
Size: 279448 Color: 12

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 430480 Color: 3
Size: 302521 Color: 1
Size: 267000 Color: 14

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 430488 Color: 14
Size: 318905 Color: 3
Size: 250608 Color: 16

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 430503 Color: 9
Size: 301344 Color: 16
Size: 268154 Color: 10

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 430630 Color: 12
Size: 315626 Color: 3
Size: 253745 Color: 4

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 430712 Color: 7
Size: 301915 Color: 0
Size: 267374 Color: 12

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 430773 Color: 0
Size: 299196 Color: 12
Size: 270032 Color: 9

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 430902 Color: 4
Size: 287586 Color: 11
Size: 281513 Color: 8

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 430930 Color: 0
Size: 296249 Color: 3
Size: 272822 Color: 15

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 431118 Color: 15
Size: 306495 Color: 10
Size: 262388 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 431129 Color: 15
Size: 287957 Color: 3
Size: 280915 Color: 18

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 431133 Color: 17
Size: 302180 Color: 16
Size: 266688 Color: 2

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 431145 Color: 1
Size: 304673 Color: 6
Size: 264183 Color: 15

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 431248 Color: 18
Size: 286341 Color: 6
Size: 282412 Color: 5

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 431272 Color: 0
Size: 294074 Color: 13
Size: 274655 Color: 9

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 431313 Color: 12
Size: 289257 Color: 4
Size: 279431 Color: 14

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 431362 Color: 7
Size: 302623 Color: 14
Size: 266016 Color: 4

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 431366 Color: 6
Size: 318393 Color: 0
Size: 250242 Color: 11

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 431416 Color: 18
Size: 287199 Color: 10
Size: 281386 Color: 8

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 431447 Color: 18
Size: 292529 Color: 17
Size: 276025 Color: 13

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 431479 Color: 15
Size: 295155 Color: 6
Size: 273367 Color: 12

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 431522 Color: 6
Size: 294058 Color: 15
Size: 274421 Color: 13

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 431535 Color: 8
Size: 292778 Color: 3
Size: 275688 Color: 9

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 431574 Color: 8
Size: 297463 Color: 1
Size: 270964 Color: 3

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 431576 Color: 11
Size: 291074 Color: 10
Size: 277351 Color: 3

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 431603 Color: 19
Size: 298181 Color: 13
Size: 270217 Color: 4

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 431613 Color: 12
Size: 297911 Color: 2
Size: 270477 Color: 4

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 431644 Color: 14
Size: 290278 Color: 1
Size: 278079 Color: 15

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 431693 Color: 15
Size: 305223 Color: 8
Size: 263085 Color: 7

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 431831 Color: 1
Size: 289438 Color: 17
Size: 278732 Color: 9

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 431840 Color: 19
Size: 286226 Color: 3
Size: 281935 Color: 10

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 431867 Color: 11
Size: 286627 Color: 3
Size: 281507 Color: 9

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 432075 Color: 15
Size: 302199 Color: 3
Size: 265727 Color: 13

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 432178 Color: 1
Size: 305855 Color: 9
Size: 261968 Color: 5

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 432221 Color: 8
Size: 291816 Color: 17
Size: 275964 Color: 10

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 432245 Color: 15
Size: 287292 Color: 9
Size: 280464 Color: 13

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 432264 Color: 19
Size: 298369 Color: 19
Size: 269368 Color: 4

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 432413 Color: 14
Size: 296796 Color: 0
Size: 270792 Color: 19

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 432481 Color: 15
Size: 284685 Color: 9
Size: 282835 Color: 19

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 432532 Color: 14
Size: 293869 Color: 18
Size: 273600 Color: 10

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 432631 Color: 8
Size: 292061 Color: 14
Size: 275309 Color: 6

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 432680 Color: 2
Size: 286888 Color: 16
Size: 280433 Color: 4

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 432787 Color: 5
Size: 313457 Color: 6
Size: 253757 Color: 6

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 432803 Color: 7
Size: 301727 Color: 4
Size: 265471 Color: 4

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 433008 Color: 8
Size: 302759 Color: 9
Size: 264234 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 433070 Color: 4
Size: 303948 Color: 11
Size: 262983 Color: 7

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 433167 Color: 10
Size: 304189 Color: 7
Size: 262645 Color: 15

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 433081 Color: 3
Size: 312430 Color: 17
Size: 254490 Color: 6

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 433129 Color: 15
Size: 316686 Color: 6
Size: 250186 Color: 3

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 433152 Color: 1
Size: 299518 Color: 5
Size: 267331 Color: 4

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 433222 Color: 18
Size: 314144 Color: 10
Size: 252635 Color: 12

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 433232 Color: 2
Size: 291745 Color: 17
Size: 275024 Color: 7

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 433254 Color: 0
Size: 306717 Color: 19
Size: 260030 Color: 14

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 433263 Color: 3
Size: 296054 Color: 8
Size: 270684 Color: 11

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 433277 Color: 6
Size: 297678 Color: 16
Size: 269046 Color: 12

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 433428 Color: 11
Size: 299801 Color: 12
Size: 266772 Color: 1

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 433456 Color: 16
Size: 284926 Color: 7
Size: 281619 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 433550 Color: 10
Size: 290693 Color: 7
Size: 275758 Color: 18

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 433548 Color: 1
Size: 291032 Color: 5
Size: 275421 Color: 8

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 433731 Color: 9
Size: 284402 Color: 5
Size: 281868 Color: 12

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 433819 Color: 19
Size: 292133 Color: 19
Size: 274049 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 433887 Color: 1
Size: 286393 Color: 10
Size: 279721 Color: 14

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 433895 Color: 13
Size: 296913 Color: 11
Size: 269193 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 433987 Color: 8
Size: 287103 Color: 17
Size: 278911 Color: 5

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 434081 Color: 1
Size: 306556 Color: 19
Size: 259364 Color: 11

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 434227 Color: 2
Size: 307764 Color: 1
Size: 258010 Color: 4

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 434252 Color: 14
Size: 283886 Color: 12
Size: 281863 Color: 16

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 434306 Color: 12
Size: 286688 Color: 14
Size: 279007 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 434309 Color: 5
Size: 309496 Color: 9
Size: 256196 Color: 15

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 434455 Color: 9
Size: 291160 Color: 2
Size: 274386 Color: 17

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 434465 Color: 7
Size: 284782 Color: 4
Size: 280754 Color: 3

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 434503 Color: 16
Size: 298330 Color: 6
Size: 267168 Color: 14

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 434601 Color: 16
Size: 283666 Color: 2
Size: 281734 Color: 7

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 434698 Color: 7
Size: 284717 Color: 10
Size: 280586 Color: 17

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 434780 Color: 7
Size: 303330 Color: 18
Size: 261891 Color: 16

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 434890 Color: 12
Size: 286030 Color: 11
Size: 279081 Color: 18

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 434955 Color: 5
Size: 283071 Color: 11
Size: 281975 Color: 4

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 435061 Color: 19
Size: 290863 Color: 6
Size: 274077 Color: 12

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 435150 Color: 4
Size: 288700 Color: 3
Size: 276151 Color: 3

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 435191 Color: 1
Size: 288459 Color: 16
Size: 276351 Color: 13

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 435338 Color: 10
Size: 295804 Color: 12
Size: 268859 Color: 2

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 435298 Color: 12
Size: 309535 Color: 5
Size: 255168 Color: 3

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 435370 Color: 11
Size: 289180 Color: 13
Size: 275451 Color: 17

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 435375 Color: 17
Size: 293351 Color: 10
Size: 271275 Color: 6

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 435459 Color: 15
Size: 300156 Color: 6
Size: 264386 Color: 4

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 435538 Color: 11
Size: 304218 Color: 7
Size: 260245 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 435539 Color: 2
Size: 312890 Color: 18
Size: 251572 Color: 9

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 435561 Color: 3
Size: 308924 Color: 12
Size: 255516 Color: 4

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 435585 Color: 0
Size: 310879 Color: 10
Size: 253537 Color: 19

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 435639 Color: 3
Size: 309207 Color: 6
Size: 255155 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 435795 Color: 19
Size: 302380 Color: 15
Size: 261826 Color: 8

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 435850 Color: 14
Size: 286634 Color: 7
Size: 277517 Color: 2

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 435918 Color: 9
Size: 300379 Color: 17
Size: 263704 Color: 1

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 435930 Color: 5
Size: 290286 Color: 12
Size: 273785 Color: 9

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 436008 Color: 10
Size: 285274 Color: 12
Size: 278719 Color: 11

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 435946 Color: 17
Size: 295106 Color: 8
Size: 268949 Color: 16

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 435950 Color: 1
Size: 297697 Color: 7
Size: 266354 Color: 4

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 435986 Color: 7
Size: 309870 Color: 13
Size: 254145 Color: 2

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 436079 Color: 18
Size: 295145 Color: 19
Size: 268777 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 436106 Color: 8
Size: 310432 Color: 8
Size: 253463 Color: 14

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 436157 Color: 5
Size: 291659 Color: 17
Size: 272185 Color: 17

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 436289 Color: 10
Size: 300251 Color: 11
Size: 263461 Color: 7

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 436248 Color: 8
Size: 304055 Color: 12
Size: 259698 Color: 16

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 436275 Color: 1
Size: 310168 Color: 9
Size: 253558 Color: 5

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 436290 Color: 8
Size: 287023 Color: 18
Size: 276688 Color: 14

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 436321 Color: 0
Size: 289696 Color: 10
Size: 273984 Color: 16

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 436461 Color: 13
Size: 282355 Color: 5
Size: 281185 Color: 14

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 436641 Color: 7
Size: 301440 Color: 14
Size: 261920 Color: 19

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 436702 Color: 2
Size: 297301 Color: 15
Size: 265998 Color: 7

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 436735 Color: 14
Size: 300112 Color: 17
Size: 263154 Color: 8

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 436865 Color: 5
Size: 292009 Color: 4
Size: 271127 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 436948 Color: 9
Size: 299253 Color: 10
Size: 263800 Color: 14

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 437166 Color: 18
Size: 284236 Color: 13
Size: 278599 Color: 14

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 437256 Color: 13
Size: 311908 Color: 1
Size: 250837 Color: 12

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 437372 Color: 0
Size: 288849 Color: 0
Size: 273780 Color: 15

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 437399 Color: 0
Size: 288474 Color: 14
Size: 274128 Color: 4

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 437552 Color: 18
Size: 307782 Color: 18
Size: 254667 Color: 10

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 437558 Color: 12
Size: 283912 Color: 3
Size: 278531 Color: 6

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 437567 Color: 6
Size: 300867 Color: 7
Size: 261567 Color: 4

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 437606 Color: 2
Size: 296603 Color: 18
Size: 265792 Color: 6

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 437678 Color: 19
Size: 286938 Color: 7
Size: 275385 Color: 17

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 437685 Color: 13
Size: 312092 Color: 15
Size: 250224 Color: 10

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 437786 Color: 0
Size: 303985 Color: 12
Size: 258230 Color: 18

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 437965 Color: 12
Size: 294974 Color: 14
Size: 267062 Color: 19

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 437969 Color: 17
Size: 291547 Color: 15
Size: 270485 Color: 11

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 437972 Color: 8
Size: 296638 Color: 4
Size: 265391 Color: 16

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 438030 Color: 3
Size: 301266 Color: 7
Size: 260705 Color: 14

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 438033 Color: 4
Size: 285416 Color: 7
Size: 276552 Color: 10

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 438057 Color: 5
Size: 307394 Color: 17
Size: 254550 Color: 13

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 438060 Color: 2
Size: 308619 Color: 6
Size: 253322 Color: 15

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 438169 Color: 4
Size: 289267 Color: 1
Size: 272565 Color: 9

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 438222 Color: 6
Size: 285358 Color: 13
Size: 276421 Color: 3

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 438224 Color: 16
Size: 298559 Color: 7
Size: 263218 Color: 13

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 438257 Color: 8
Size: 306869 Color: 14
Size: 254875 Color: 10

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 438299 Color: 9
Size: 297328 Color: 5
Size: 264374 Color: 18

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 438528 Color: 12
Size: 288521 Color: 18
Size: 272952 Color: 14

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 438677 Color: 14
Size: 306755 Color: 19
Size: 254569 Color: 11

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 438725 Color: 9
Size: 287924 Color: 17
Size: 273352 Color: 10

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 438768 Color: 1
Size: 300566 Color: 9
Size: 260667 Color: 14

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 438819 Color: 17
Size: 309190 Color: 6
Size: 251992 Color: 16

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 438862 Color: 17
Size: 305623 Color: 16
Size: 255516 Color: 18

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 438873 Color: 7
Size: 290243 Color: 5
Size: 270885 Color: 2

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 438878 Color: 7
Size: 290627 Color: 12
Size: 270496 Color: 3

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 438901 Color: 5
Size: 309638 Color: 11
Size: 251462 Color: 17

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 438933 Color: 17
Size: 282549 Color: 5
Size: 278519 Color: 16

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 439065 Color: 10
Size: 281153 Color: 0
Size: 279783 Color: 12

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 438984 Color: 9
Size: 288537 Color: 11
Size: 272480 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 439025 Color: 9
Size: 292881 Color: 6
Size: 268095 Color: 4

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 439099 Color: 7
Size: 305362 Color: 5
Size: 255540 Color: 16

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 439203 Color: 3
Size: 309864 Color: 4
Size: 250934 Color: 10

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 439210 Color: 19
Size: 302845 Color: 12
Size: 257946 Color: 3

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 439223 Color: 15
Size: 297315 Color: 18
Size: 263463 Color: 4

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 439283 Color: 7
Size: 300796 Color: 15
Size: 259922 Color: 14

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 439364 Color: 8
Size: 282876 Color: 18
Size: 277761 Color: 17

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 439403 Color: 18
Size: 305310 Color: 15
Size: 255288 Color: 10

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 439433 Color: 2
Size: 282347 Color: 19
Size: 278221 Color: 15

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 439515 Color: 12
Size: 300173 Color: 5
Size: 260313 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 439565 Color: 2
Size: 281262 Color: 7
Size: 279174 Color: 8

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 439649 Color: 0
Size: 293182 Color: 17
Size: 267170 Color: 18

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 439777 Color: 10
Size: 295415 Color: 9
Size: 264809 Color: 8

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 439809 Color: 19
Size: 282467 Color: 16
Size: 277725 Color: 7

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 439831 Color: 17
Size: 296313 Color: 4
Size: 263857 Color: 8

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 439939 Color: 4
Size: 306138 Color: 6
Size: 253924 Color: 8

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 440041 Color: 2
Size: 281620 Color: 10
Size: 278340 Color: 12

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 440056 Color: 0
Size: 291924 Color: 12
Size: 268021 Color: 3

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 440164 Color: 16
Size: 280029 Color: 19
Size: 279808 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 440187 Color: 3
Size: 308312 Color: 0
Size: 251502 Color: 9

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 440239 Color: 16
Size: 280542 Color: 11
Size: 279220 Color: 18

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 440250 Color: 12
Size: 285357 Color: 5
Size: 274394 Color: 4

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 440456 Color: 1
Size: 307685 Color: 5
Size: 251860 Color: 10

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 440537 Color: 17
Size: 301604 Color: 14
Size: 257860 Color: 5

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 440554 Color: 12
Size: 286763 Color: 13
Size: 272684 Color: 7

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 440645 Color: 13
Size: 287887 Color: 2
Size: 271469 Color: 19

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 440674 Color: 15
Size: 293301 Color: 1
Size: 266026 Color: 15

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 440681 Color: 15
Size: 300528 Color: 10
Size: 258792 Color: 4

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 440700 Color: 18
Size: 286060 Color: 9
Size: 273241 Color: 15

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 440744 Color: 0
Size: 298363 Color: 5
Size: 260894 Color: 6

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 440768 Color: 15
Size: 290205 Color: 18
Size: 269028 Color: 6

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 440784 Color: 15
Size: 290278 Color: 7
Size: 268939 Color: 11

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 440793 Color: 3
Size: 301656 Color: 6
Size: 257552 Color: 18

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 440872 Color: 7
Size: 306631 Color: 0
Size: 252498 Color: 17

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 440903 Color: 4
Size: 304535 Color: 6
Size: 254563 Color: 15

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 441190 Color: 10
Size: 289270 Color: 4
Size: 269541 Color: 3

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 441044 Color: 17
Size: 295708 Color: 12
Size: 263249 Color: 3

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 441127 Color: 4
Size: 297948 Color: 17
Size: 260926 Color: 6

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 441179 Color: 12
Size: 295685 Color: 0
Size: 263137 Color: 19

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 441261 Color: 19
Size: 302575 Color: 13
Size: 256165 Color: 14

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 441293 Color: 18
Size: 295287 Color: 19
Size: 263421 Color: 3

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 441328 Color: 17
Size: 283639 Color: 11
Size: 275034 Color: 9

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 441337 Color: 8
Size: 285643 Color: 18
Size: 273021 Color: 9

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 441375 Color: 14
Size: 289782 Color: 18
Size: 268844 Color: 11

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 441382 Color: 0
Size: 295847 Color: 19
Size: 262772 Color: 10

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 441399 Color: 16
Size: 301784 Color: 5
Size: 256818 Color: 7

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 441503 Color: 9
Size: 296350 Color: 8
Size: 262148 Color: 13

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 441609 Color: 14
Size: 302083 Color: 19
Size: 256309 Color: 5

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 441658 Color: 6
Size: 290860 Color: 5
Size: 267483 Color: 9

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 441863 Color: 3
Size: 294963 Color: 12
Size: 263175 Color: 3

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 442012 Color: 10
Size: 284153 Color: 15
Size: 273836 Color: 9

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 441937 Color: 17
Size: 293780 Color: 2
Size: 264284 Color: 15

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 441953 Color: 4
Size: 284629 Color: 11
Size: 273419 Color: 3

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 441966 Color: 13
Size: 295746 Color: 4
Size: 262289 Color: 4

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 442036 Color: 5
Size: 307483 Color: 17
Size: 250482 Color: 7

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 442127 Color: 6
Size: 298038 Color: 1
Size: 259836 Color: 10

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 442137 Color: 18
Size: 283942 Color: 6
Size: 273922 Color: 3

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 442158 Color: 7
Size: 287601 Color: 4
Size: 270242 Color: 15

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 442178 Color: 19
Size: 299161 Color: 4
Size: 258662 Color: 11

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 442337 Color: 8
Size: 302018 Color: 9
Size: 255646 Color: 8

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 442345 Color: 2
Size: 285075 Color: 16
Size: 272581 Color: 9

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 442428 Color: 0
Size: 292805 Color: 19
Size: 264768 Color: 10

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 442486 Color: 14
Size: 303551 Color: 6
Size: 253964 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 442488 Color: 8
Size: 290245 Color: 12
Size: 267268 Color: 17

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 442503 Color: 17
Size: 295944 Color: 14
Size: 261554 Color: 18

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 442531 Color: 3
Size: 286231 Color: 8
Size: 271239 Color: 7

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 442601 Color: 16
Size: 296623 Color: 10
Size: 260777 Color: 19

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 442635 Color: 3
Size: 306911 Color: 1
Size: 250455 Color: 2

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 442688 Color: 19
Size: 303507 Color: 9
Size: 253806 Color: 7

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 442725 Color: 9
Size: 291118 Color: 3
Size: 266158 Color: 5

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 442731 Color: 8
Size: 288780 Color: 2
Size: 268490 Color: 4

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 442744 Color: 4
Size: 306454 Color: 6
Size: 250803 Color: 8

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 442801 Color: 16
Size: 283173 Color: 7
Size: 274027 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 443012 Color: 6
Size: 293353 Color: 12
Size: 263636 Color: 11

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 443194 Color: 10
Size: 295750 Color: 4
Size: 261057 Color: 15

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 443052 Color: 13
Size: 280040 Color: 18
Size: 276909 Color: 4

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 443133 Color: 4
Size: 285256 Color: 2
Size: 271612 Color: 3

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 443341 Color: 5
Size: 285155 Color: 18
Size: 271505 Color: 11

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 443351 Color: 17
Size: 292981 Color: 16
Size: 263669 Color: 3

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 443390 Color: 14
Size: 303688 Color: 4
Size: 252923 Color: 3

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 443394 Color: 16
Size: 282730 Color: 12
Size: 273877 Color: 11

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 443477 Color: 8
Size: 281959 Color: 12
Size: 274565 Color: 13

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 443587 Color: 2
Size: 292544 Color: 12
Size: 263870 Color: 19

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 443799 Color: 8
Size: 306161 Color: 5
Size: 250041 Color: 13

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 443833 Color: 19
Size: 304312 Color: 2
Size: 251856 Color: 3

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 443946 Color: 2
Size: 278780 Color: 5
Size: 277275 Color: 12

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 444062 Color: 11
Size: 288150 Color: 14
Size: 267789 Color: 11

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 444335 Color: 12
Size: 286058 Color: 16
Size: 269608 Color: 5

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 444402 Color: 14
Size: 304609 Color: 11
Size: 250990 Color: 7

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 444414 Color: 15
Size: 290960 Color: 10
Size: 264627 Color: 9

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 444466 Color: 8
Size: 278605 Color: 6
Size: 276930 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 444506 Color: 18
Size: 279357 Color: 15
Size: 276138 Color: 13

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 444540 Color: 4
Size: 293503 Color: 14
Size: 261958 Color: 2

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 444551 Color: 9
Size: 281992 Color: 1
Size: 273458 Color: 6

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 444604 Color: 17
Size: 288654 Color: 3
Size: 266743 Color: 12

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 444607 Color: 3
Size: 284472 Color: 10
Size: 270922 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 444746 Color: 0
Size: 298333 Color: 1
Size: 256922 Color: 5

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 445058 Color: 16
Size: 292841 Color: 8
Size: 262102 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 445324 Color: 2
Size: 299716 Color: 18
Size: 254961 Color: 5

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 445428 Color: 16
Size: 298762 Color: 17
Size: 255811 Color: 15

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 445447 Color: 4
Size: 296249 Color: 5
Size: 258305 Color: 19

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 445468 Color: 4
Size: 289367 Color: 2
Size: 265166 Color: 10

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 445481 Color: 6
Size: 301158 Color: 8
Size: 253362 Color: 3

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 445758 Color: 8
Size: 281341 Color: 7
Size: 272902 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 445865 Color: 0
Size: 297442 Color: 17
Size: 256694 Color: 12

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 445865 Color: 15
Size: 280379 Color: 17
Size: 273757 Color: 17

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 445944 Color: 16
Size: 289576 Color: 3
Size: 264481 Color: 2

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 445962 Color: 16
Size: 281562 Color: 10
Size: 272477 Color: 6

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 446003 Color: 9
Size: 303978 Color: 15
Size: 250020 Color: 1

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 446038 Color: 6
Size: 277659 Color: 7
Size: 276304 Color: 16

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 446064 Color: 6
Size: 286017 Color: 9
Size: 267920 Color: 17

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 446210 Color: 6
Size: 280748 Color: 16
Size: 273043 Color: 17

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 446231 Color: 0
Size: 287731 Color: 7
Size: 266039 Color: 4

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 446238 Color: 8
Size: 292802 Color: 7
Size: 260961 Color: 10

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 446249 Color: 8
Size: 277374 Color: 13
Size: 276378 Color: 12

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 446260 Color: 3
Size: 283747 Color: 4
Size: 269994 Color: 7

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 446358 Color: 19
Size: 295613 Color: 11
Size: 258030 Color: 12

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 446433 Color: 6
Size: 279148 Color: 14
Size: 274420 Color: 13

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 446480 Color: 9
Size: 299577 Color: 18
Size: 253944 Color: 7

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 446493 Color: 15
Size: 282461 Color: 10
Size: 271047 Color: 13

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 446574 Color: 5
Size: 290637 Color: 9
Size: 262790 Color: 13

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 446626 Color: 8
Size: 277851 Color: 3
Size: 275524 Color: 18

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 446632 Color: 15
Size: 297233 Color: 3
Size: 256136 Color: 15

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 446636 Color: 6
Size: 295566 Color: 18
Size: 257799 Color: 7

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 446673 Color: 0
Size: 291444 Color: 13
Size: 261884 Color: 17

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 446746 Color: 18
Size: 283122 Color: 0
Size: 270133 Color: 13

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 446772 Color: 15
Size: 302424 Color: 14
Size: 250805 Color: 6

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 446824 Color: 15
Size: 285317 Color: 11
Size: 267860 Color: 8

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 447174 Color: 10
Size: 302347 Color: 12
Size: 250480 Color: 5

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 447231 Color: 10
Size: 300309 Color: 13
Size: 252461 Color: 14

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 447135 Color: 13
Size: 285880 Color: 11
Size: 266986 Color: 7

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 447142 Color: 13
Size: 282666 Color: 3
Size: 270193 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 447185 Color: 15
Size: 287966 Color: 4
Size: 264850 Color: 1

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 447332 Color: 16
Size: 298902 Color: 12
Size: 253767 Color: 5

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 447350 Color: 14
Size: 284940 Color: 0
Size: 267711 Color: 3

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 447462 Color: 10
Size: 297640 Color: 5
Size: 254899 Color: 16

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 447434 Color: 17
Size: 296480 Color: 16
Size: 256087 Color: 16

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 447614 Color: 14
Size: 283238 Color: 5
Size: 269149 Color: 2

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 447687 Color: 12
Size: 280756 Color: 5
Size: 271558 Color: 12

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 447698 Color: 12
Size: 280379 Color: 7
Size: 271924 Color: 14

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 447728 Color: 13
Size: 283539 Color: 11
Size: 268734 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 447793 Color: 3
Size: 292312 Color: 19
Size: 259896 Color: 9

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 448004 Color: 10
Size: 281186 Color: 1
Size: 270811 Color: 6

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 447818 Color: 0
Size: 286825 Color: 8
Size: 265358 Color: 12

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 448066 Color: 19
Size: 288255 Color: 7
Size: 263680 Color: 14

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 448148 Color: 12
Size: 276395 Color: 17
Size: 275458 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 448163 Color: 1
Size: 291938 Color: 1
Size: 259900 Color: 8

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 448176 Color: 13
Size: 294642 Color: 6
Size: 257183 Color: 8

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 448208 Color: 7
Size: 284920 Color: 19
Size: 266873 Color: 14

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 448361 Color: 10
Size: 290682 Color: 3
Size: 260958 Color: 15

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 448314 Color: 13
Size: 287517 Color: 17
Size: 264170 Color: 19

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 448373 Color: 16
Size: 286441 Color: 16
Size: 265187 Color: 19

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 448385 Color: 8
Size: 298829 Color: 10
Size: 252787 Color: 17

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 448393 Color: 17
Size: 285985 Color: 7
Size: 265623 Color: 19

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 448540 Color: 14
Size: 288522 Color: 16
Size: 262939 Color: 16

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 448596 Color: 17
Size: 295483 Color: 14
Size: 255922 Color: 14

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 448605 Color: 1
Size: 281581 Color: 13
Size: 269815 Color: 13

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 448697 Color: 8
Size: 289625 Color: 5
Size: 261679 Color: 17

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 448714 Color: 0
Size: 297193 Color: 3
Size: 254094 Color: 18

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 448820 Color: 10
Size: 285713 Color: 9
Size: 265468 Color: 7

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 448736 Color: 0
Size: 278729 Color: 2
Size: 272536 Color: 1

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 448745 Color: 17
Size: 294367 Color: 5
Size: 256889 Color: 7

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 448915 Color: 3
Size: 291663 Color: 8
Size: 259423 Color: 7

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 448923 Color: 9
Size: 295124 Color: 16
Size: 255954 Color: 11

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 448930 Color: 3
Size: 283597 Color: 2
Size: 267474 Color: 5

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 449014 Color: 16
Size: 283887 Color: 7
Size: 267100 Color: 8

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 449025 Color: 2
Size: 296667 Color: 5
Size: 254309 Color: 2

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 449110 Color: 16
Size: 294050 Color: 7
Size: 256841 Color: 12

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 449268 Color: 10
Size: 276171 Color: 14
Size: 274562 Color: 17

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 449226 Color: 5
Size: 299226 Color: 1
Size: 251549 Color: 4

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 449235 Color: 14
Size: 292713 Color: 17
Size: 258053 Color: 1

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 449327 Color: 12
Size: 296150 Color: 18
Size: 254524 Color: 14

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 449380 Color: 19
Size: 297616 Color: 5
Size: 253005 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 449428 Color: 10
Size: 297053 Color: 17
Size: 253520 Color: 5

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 449429 Color: 1
Size: 291790 Color: 15
Size: 258782 Color: 15

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 449484 Color: 6
Size: 284164 Color: 11
Size: 266353 Color: 9

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 449746 Color: 11
Size: 284060 Color: 8
Size: 266195 Color: 8

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 449781 Color: 7
Size: 286311 Color: 5
Size: 263909 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 449837 Color: 4
Size: 281095 Color: 10
Size: 269069 Color: 8

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 449873 Color: 16
Size: 286796 Color: 13
Size: 263332 Color: 12

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 449873 Color: 2
Size: 287471 Color: 2
Size: 262657 Color: 8

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 449892 Color: 15
Size: 284933 Color: 5
Size: 265176 Color: 17

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 449953 Color: 9
Size: 277370 Color: 14
Size: 272678 Color: 12

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 449983 Color: 3
Size: 295217 Color: 19
Size: 254801 Color: 13

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 450011 Color: 19
Size: 283959 Color: 3
Size: 266031 Color: 10

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 450058 Color: 4
Size: 284587 Color: 12
Size: 265356 Color: 9

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 450119 Color: 19
Size: 282659 Color: 2
Size: 267223 Color: 18

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 450172 Color: 14
Size: 278049 Color: 6
Size: 271780 Color: 16

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 450225 Color: 5
Size: 286268 Color: 3
Size: 263508 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 450283 Color: 8
Size: 288170 Color: 8
Size: 261548 Color: 1

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 450305 Color: 17
Size: 279015 Color: 11
Size: 270681 Color: 10

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 450439 Color: 2
Size: 292707 Color: 8
Size: 256855 Color: 3

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 450470 Color: 5
Size: 277913 Color: 14
Size: 271618 Color: 3

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 450567 Color: 15
Size: 291451 Color: 15
Size: 257983 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 450582 Color: 19
Size: 295415 Color: 7
Size: 254004 Color: 17

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 450617 Color: 18
Size: 283999 Color: 3
Size: 265385 Color: 15

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 450672 Color: 7
Size: 285311 Color: 5
Size: 264018 Color: 1

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 450694 Color: 7
Size: 289470 Color: 10
Size: 259837 Color: 13

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 450749 Color: 7
Size: 286187 Color: 0
Size: 263065 Color: 19

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 451025 Color: 6
Size: 295919 Color: 6
Size: 253057 Color: 8

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 451105 Color: 7
Size: 287852 Color: 14
Size: 261044 Color: 4

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 451116 Color: 19
Size: 277984 Color: 18
Size: 270901 Color: 11

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 451127 Color: 3
Size: 289520 Color: 10
Size: 259354 Color: 7

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 451147 Color: 4
Size: 291815 Color: 7
Size: 257039 Color: 19

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 451198 Color: 13
Size: 298493 Color: 4
Size: 250310 Color: 18

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 451280 Color: 12
Size: 297044 Color: 14
Size: 251677 Color: 16

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 451398 Color: 1
Size: 280440 Color: 19
Size: 268163 Color: 14

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 451486 Color: 4
Size: 282978 Color: 17
Size: 265537 Color: 9

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 451731 Color: 12
Size: 292774 Color: 3
Size: 255496 Color: 5

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 451750 Color: 12
Size: 297612 Color: 15
Size: 250639 Color: 18

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 451858 Color: 10
Size: 285484 Color: 0
Size: 262659 Color: 11

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 451797 Color: 5
Size: 276477 Color: 0
Size: 271727 Color: 17

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 451809 Color: 4
Size: 292787 Color: 2
Size: 255405 Color: 3

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 451852 Color: 15
Size: 277042 Color: 14
Size: 271107 Color: 3

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 451894 Color: 16
Size: 289553 Color: 15
Size: 258554 Color: 13

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 451962 Color: 10
Size: 282554 Color: 9
Size: 265485 Color: 4

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 451915 Color: 6
Size: 288420 Color: 13
Size: 259666 Color: 11

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 452045 Color: 3
Size: 293432 Color: 2
Size: 254524 Color: 6

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 452117 Color: 7
Size: 274064 Color: 1
Size: 273820 Color: 15

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 452149 Color: 8
Size: 276792 Color: 0
Size: 271060 Color: 15

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 452157 Color: 0
Size: 295655 Color: 10
Size: 252189 Color: 12

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 452206 Color: 19
Size: 276380 Color: 3
Size: 271415 Color: 17

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 452404 Color: 9
Size: 294818 Color: 15
Size: 252779 Color: 1

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 452503 Color: 4
Size: 288449 Color: 3
Size: 259049 Color: 10

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 452508 Color: 3
Size: 278631 Color: 6
Size: 268862 Color: 16

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 452517 Color: 12
Size: 278312 Color: 7
Size: 269172 Color: 3

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 452563 Color: 8
Size: 296915 Color: 12
Size: 250523 Color: 19

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 452577 Color: 2
Size: 277819 Color: 6
Size: 269605 Color: 4

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 452647 Color: 13
Size: 274910 Color: 4
Size: 272444 Color: 3

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 452701 Color: 13
Size: 276734 Color: 11
Size: 270566 Color: 10

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 452804 Color: 3
Size: 274969 Color: 9
Size: 272228 Color: 7

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 452919 Color: 8
Size: 280243 Color: 6
Size: 266839 Color: 18

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 452948 Color: 17
Size: 289555 Color: 17
Size: 257498 Color: 4

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 452966 Color: 2
Size: 287280 Color: 3
Size: 259755 Color: 12

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 453001 Color: 18
Size: 288535 Color: 7
Size: 258465 Color: 19

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 453065 Color: 5
Size: 274720 Color: 6
Size: 272216 Color: 1

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 453080 Color: 18
Size: 291865 Color: 16
Size: 255056 Color: 14

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 453362 Color: 17
Size: 280018 Color: 19
Size: 266621 Color: 19

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 453376 Color: 4
Size: 282716 Color: 11
Size: 263909 Color: 14

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 453610 Color: 15
Size: 279526 Color: 10
Size: 266865 Color: 3

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 453644 Color: 7
Size: 287424 Color: 19
Size: 258933 Color: 17

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 453678 Color: 7
Size: 294187 Color: 15
Size: 252136 Color: 11

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 453839 Color: 5
Size: 292758 Color: 0
Size: 253404 Color: 6

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 453909 Color: 14
Size: 292696 Color: 3
Size: 253396 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 453913 Color: 9
Size: 275093 Color: 6
Size: 270995 Color: 8

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 454008 Color: 3
Size: 289478 Color: 7
Size: 256515 Color: 6

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 454099 Color: 10
Size: 290029 Color: 3
Size: 255873 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 454059 Color: 3
Size: 290445 Color: 13
Size: 255497 Color: 5

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 454080 Color: 4
Size: 280142 Color: 7
Size: 265779 Color: 2

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 454140 Color: 16
Size: 278986 Color: 12
Size: 266875 Color: 15

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 454144 Color: 6
Size: 277176 Color: 3
Size: 268681 Color: 16

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 454154 Color: 17
Size: 280977 Color: 13
Size: 264870 Color: 10

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 454207 Color: 16
Size: 293383 Color: 7
Size: 252411 Color: 14

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 454226 Color: 3
Size: 279342 Color: 17
Size: 266433 Color: 15

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 454239 Color: 11
Size: 279157 Color: 7
Size: 266605 Color: 5

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 454247 Color: 19
Size: 286990 Color: 16
Size: 258764 Color: 15

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 454442 Color: 15
Size: 284500 Color: 2
Size: 261059 Color: 2

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 454491 Color: 19
Size: 275475 Color: 10
Size: 270035 Color: 16

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 454561 Color: 11
Size: 274674 Color: 4
Size: 270766 Color: 12

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 454609 Color: 0
Size: 285439 Color: 7
Size: 259953 Color: 14

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 454697 Color: 13
Size: 286087 Color: 7
Size: 259217 Color: 17

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 454725 Color: 4
Size: 294243 Color: 18
Size: 251033 Color: 5

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 454736 Color: 17
Size: 275705 Color: 1
Size: 269560 Color: 6

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 454746 Color: 1
Size: 283763 Color: 15
Size: 261492 Color: 13

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 454764 Color: 2
Size: 291707 Color: 5
Size: 253530 Color: 15

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 454961 Color: 10
Size: 272878 Color: 1
Size: 272162 Color: 16

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 454813 Color: 12
Size: 282402 Color: 18
Size: 262786 Color: 1

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 454968 Color: 15
Size: 290570 Color: 9
Size: 254463 Color: 9

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 455038 Color: 9
Size: 287550 Color: 3
Size: 257413 Color: 14

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 455059 Color: 4
Size: 289124 Color: 0
Size: 255818 Color: 2

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 455079 Color: 15
Size: 278005 Color: 0
Size: 266917 Color: 12

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 455098 Color: 3
Size: 281429 Color: 12
Size: 263474 Color: 5

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 455175 Color: 7
Size: 281158 Color: 15
Size: 263668 Color: 2

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 455188 Color: 1
Size: 288454 Color: 14
Size: 256359 Color: 8

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 455200 Color: 12
Size: 278008 Color: 7
Size: 266793 Color: 2

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 455239 Color: 8
Size: 284322 Color: 15
Size: 260440 Color: 19

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 455363 Color: 17
Size: 287205 Color: 0
Size: 257433 Color: 9

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 455422 Color: 9
Size: 288428 Color: 15
Size: 256151 Color: 2

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 455759 Color: 10
Size: 274331 Color: 1
Size: 269911 Color: 15

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 455559 Color: 16
Size: 288536 Color: 2
Size: 255906 Color: 5

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 455661 Color: 6
Size: 275480 Color: 6
Size: 268860 Color: 16

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 455843 Color: 10
Size: 279424 Color: 16
Size: 264734 Color: 4

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 455758 Color: 14
Size: 286347 Color: 18
Size: 257896 Color: 13

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 455793 Color: 9
Size: 284736 Color: 11
Size: 259472 Color: 4

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 455915 Color: 18
Size: 286891 Color: 10
Size: 257195 Color: 13

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 456022 Color: 11
Size: 284601 Color: 0
Size: 259378 Color: 14

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 456200 Color: 8
Size: 293269 Color: 7
Size: 250532 Color: 18

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 456278 Color: 6
Size: 281526 Color: 16
Size: 262197 Color: 15

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 456317 Color: 16
Size: 292217 Color: 2
Size: 251467 Color: 5

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 456383 Color: 5
Size: 291387 Color: 2
Size: 252231 Color: 2

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 456423 Color: 19
Size: 273398 Color: 11
Size: 270180 Color: 7

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 456438 Color: 9
Size: 277157 Color: 19
Size: 266406 Color: 15

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 456614 Color: 5
Size: 279337 Color: 3
Size: 264050 Color: 16

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 456615 Color: 2
Size: 272695 Color: 10
Size: 270691 Color: 16

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 456724 Color: 17
Size: 273564 Color: 16
Size: 269713 Color: 1

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 456746 Color: 8
Size: 280074 Color: 3
Size: 263181 Color: 2

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 456767 Color: 5
Size: 277110 Color: 6
Size: 266124 Color: 6

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 456991 Color: 2
Size: 291388 Color: 3
Size: 251622 Color: 15

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 456992 Color: 14
Size: 275544 Color: 5
Size: 267465 Color: 16

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 457051 Color: 8
Size: 289364 Color: 10
Size: 253586 Color: 8

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 457076 Color: 1
Size: 289332 Color: 19
Size: 253593 Color: 3

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 457205 Color: 5
Size: 291996 Color: 0
Size: 250800 Color: 15

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 457225 Color: 1
Size: 284749 Color: 2
Size: 258027 Color: 14

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 454622 Color: 17
Size: 282366 Color: 10
Size: 263013 Color: 14

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 457320 Color: 8
Size: 281638 Color: 13
Size: 261043 Color: 18

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 457342 Color: 3
Size: 290064 Color: 12
Size: 252595 Color: 2

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 457381 Color: 16
Size: 284189 Color: 14
Size: 258431 Color: 3

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 457462 Color: 18
Size: 274666 Color: 12
Size: 267873 Color: 5

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 457507 Color: 9
Size: 271430 Color: 14
Size: 271064 Color: 10

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 457587 Color: 13
Size: 278712 Color: 5
Size: 263702 Color: 3

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 457611 Color: 14
Size: 277967 Color: 16
Size: 264423 Color: 5

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 457620 Color: 12
Size: 278969 Color: 5
Size: 263412 Color: 15

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 457687 Color: 1
Size: 276966 Color: 9
Size: 265348 Color: 9

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 457716 Color: 17
Size: 280550 Color: 12
Size: 261735 Color: 10

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 457781 Color: 0
Size: 280009 Color: 4
Size: 262211 Color: 2

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 457844 Color: 4
Size: 277701 Color: 8
Size: 264456 Color: 12

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 457849 Color: 4
Size: 280747 Color: 7
Size: 261405 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 458073 Color: 18
Size: 289591 Color: 5
Size: 252337 Color: 16

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 458371 Color: 10
Size: 283556 Color: 0
Size: 258074 Color: 8

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 458339 Color: 18
Size: 280914 Color: 2
Size: 260748 Color: 3

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 458377 Color: 1
Size: 279289 Color: 11
Size: 262335 Color: 18

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 458541 Color: 9
Size: 271658 Color: 11
Size: 269802 Color: 17

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 458614 Color: 16
Size: 288474 Color: 0
Size: 252913 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 458664 Color: 11
Size: 289941 Color: 13
Size: 251396 Color: 10

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 458671 Color: 11
Size: 288416 Color: 1
Size: 252914 Color: 14

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 458720 Color: 19
Size: 287734 Color: 15
Size: 253547 Color: 19

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 458727 Color: 13
Size: 290310 Color: 11
Size: 250964 Color: 14

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 458780 Color: 7
Size: 289131 Color: 14
Size: 252090 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 458832 Color: 11
Size: 289259 Color: 4
Size: 251910 Color: 10

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 459045 Color: 4
Size: 271705 Color: 16
Size: 269251 Color: 11

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 459115 Color: 19
Size: 284255 Color: 15
Size: 256631 Color: 1

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 459180 Color: 18
Size: 290266 Color: 0
Size: 250555 Color: 8

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 459278 Color: 19
Size: 284536 Color: 1
Size: 256187 Color: 7

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 459325 Color: 8
Size: 280956 Color: 4
Size: 259720 Color: 1

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 459344 Color: 2
Size: 284334 Color: 10
Size: 256323 Color: 19

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 459365 Color: 5
Size: 285188 Color: 13
Size: 255448 Color: 16

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 459376 Color: 6
Size: 290277 Color: 6
Size: 250348 Color: 8

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 459421 Color: 11
Size: 281662 Color: 6
Size: 258918 Color: 13

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 459431 Color: 14
Size: 275170 Color: 1
Size: 265400 Color: 18

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 459520 Color: 9
Size: 271977 Color: 8
Size: 268504 Color: 2

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 459540 Color: 16
Size: 271709 Color: 10
Size: 268752 Color: 9

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 459552 Color: 3
Size: 271286 Color: 16
Size: 269163 Color: 6

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 459578 Color: 1
Size: 282709 Color: 15
Size: 257714 Color: 17

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 459644 Color: 6
Size: 281076 Color: 19
Size: 259281 Color: 8

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 459730 Color: 12
Size: 286962 Color: 11
Size: 253309 Color: 4

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 459762 Color: 13
Size: 286646 Color: 18
Size: 253593 Color: 12

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 459921 Color: 2
Size: 283859 Color: 10
Size: 256221 Color: 1

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 460000 Color: 6
Size: 285161 Color: 5
Size: 254840 Color: 3

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 460031 Color: 2
Size: 275656 Color: 3
Size: 264314 Color: 15

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 460039 Color: 11
Size: 285746 Color: 2
Size: 254216 Color: 11

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 460118 Color: 2
Size: 275009 Color: 7
Size: 264874 Color: 16

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 460148 Color: 3
Size: 280129 Color: 7
Size: 259724 Color: 9

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 460201 Color: 17
Size: 280179 Color: 10
Size: 259621 Color: 11

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 460426 Color: 15
Size: 287385 Color: 0
Size: 252190 Color: 1

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 460525 Color: 19
Size: 271409 Color: 5
Size: 268067 Color: 9

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 460680 Color: 17
Size: 271126 Color: 7
Size: 268195 Color: 6

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 460708 Color: 18
Size: 286066 Color: 9
Size: 253227 Color: 6

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 460719 Color: 14
Size: 284192 Color: 12
Size: 255090 Color: 3

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 460797 Color: 3
Size: 269898 Color: 16
Size: 269306 Color: 2

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 460829 Color: 14
Size: 276886 Color: 3
Size: 262286 Color: 6

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 460917 Color: 3
Size: 285116 Color: 6
Size: 253968 Color: 10

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 461029 Color: 0
Size: 286191 Color: 16
Size: 252781 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 461290 Color: 6
Size: 287828 Color: 10
Size: 250883 Color: 18

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 461321 Color: 6
Size: 286039 Color: 16
Size: 252641 Color: 2

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 461682 Color: 3
Size: 273536 Color: 16
Size: 264783 Color: 5

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 461769 Color: 0
Size: 275093 Color: 13
Size: 263139 Color: 11

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 461823 Color: 7
Size: 277079 Color: 7
Size: 261099 Color: 13

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 461837 Color: 11
Size: 278534 Color: 17
Size: 259630 Color: 10

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 461877 Color: 5
Size: 282859 Color: 3
Size: 255265 Color: 8

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 461945 Color: 5
Size: 283045 Color: 2
Size: 255011 Color: 6

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 462064 Color: 18
Size: 281146 Color: 6
Size: 256791 Color: 2

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 462095 Color: 17
Size: 284590 Color: 13
Size: 253316 Color: 18

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 462262 Color: 14
Size: 279105 Color: 9
Size: 258634 Color: 19

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 462330 Color: 19
Size: 281764 Color: 9
Size: 255907 Color: 15

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 462343 Color: 8
Size: 280059 Color: 3
Size: 257599 Color: 17

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 462454 Color: 4
Size: 280008 Color: 13
Size: 257539 Color: 17

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 462503 Color: 16
Size: 274663 Color: 14
Size: 262835 Color: 1

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 462880 Color: 10
Size: 271791 Color: 4
Size: 265330 Color: 18

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 462790 Color: 1
Size: 275431 Color: 2
Size: 261780 Color: 4

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 462908 Color: 17
Size: 284627 Color: 3
Size: 252466 Color: 17

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 462939 Color: 3
Size: 286866 Color: 4
Size: 250196 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 463040 Color: 7
Size: 275401 Color: 6
Size: 261560 Color: 3

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 463092 Color: 13
Size: 280651 Color: 0
Size: 256258 Color: 17

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 463139 Color: 2
Size: 286550 Color: 17
Size: 250312 Color: 9

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 463413 Color: 10
Size: 270886 Color: 13
Size: 265702 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 463155 Color: 6
Size: 285224 Color: 8
Size: 251622 Color: 19

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 463236 Color: 13
Size: 270284 Color: 5
Size: 266481 Color: 8

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 463433 Color: 10
Size: 271611 Color: 15
Size: 264957 Color: 17

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 463325 Color: 9
Size: 282484 Color: 7
Size: 254192 Color: 8

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 463457 Color: 10
Size: 283951 Color: 0
Size: 252593 Color: 16

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 463409 Color: 17
Size: 285519 Color: 8
Size: 251073 Color: 18

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 463508 Color: 9
Size: 282511 Color: 1
Size: 253982 Color: 19

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 463699 Color: 3
Size: 276787 Color: 7
Size: 259515 Color: 2

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 463707 Color: 17
Size: 280853 Color: 1
Size: 255441 Color: 14

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 463777 Color: 16
Size: 274119 Color: 10
Size: 262105 Color: 17

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 463854 Color: 1
Size: 276859 Color: 11
Size: 259288 Color: 2

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 463860 Color: 2
Size: 271395 Color: 0
Size: 264746 Color: 8

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 463905 Color: 15
Size: 276451 Color: 12
Size: 259645 Color: 7

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 463915 Color: 17
Size: 280796 Color: 18
Size: 255290 Color: 13

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 463942 Color: 6
Size: 269623 Color: 7
Size: 266436 Color: 12

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 464016 Color: 15
Size: 282112 Color: 10
Size: 253873 Color: 3

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 464145 Color: 8
Size: 278683 Color: 8
Size: 257173 Color: 6

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 464164 Color: 1
Size: 278335 Color: 8
Size: 257502 Color: 12

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 464164 Color: 9
Size: 282074 Color: 4
Size: 253763 Color: 7

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 464195 Color: 11
Size: 268419 Color: 6
Size: 267387 Color: 8

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 464224 Color: 14
Size: 271049 Color: 16
Size: 264728 Color: 4

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 464266 Color: 17
Size: 285216 Color: 6
Size: 250519 Color: 10

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 464370 Color: 11
Size: 283005 Color: 2
Size: 252626 Color: 9

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 464377 Color: 5
Size: 269386 Color: 8
Size: 266238 Color: 9

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 464394 Color: 7
Size: 272237 Color: 4
Size: 263370 Color: 17

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 464504 Color: 3
Size: 275802 Color: 3
Size: 259695 Color: 11

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 464608 Color: 11
Size: 276126 Color: 19
Size: 259267 Color: 13

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 464615 Color: 0
Size: 274344 Color: 9
Size: 261042 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 464615 Color: 5
Size: 282313 Color: 7
Size: 253073 Color: 6

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 464651 Color: 8
Size: 277658 Color: 6
Size: 257692 Color: 5

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 464774 Color: 14
Size: 272144 Color: 17
Size: 263083 Color: 19

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 465023 Color: 2
Size: 279160 Color: 19
Size: 255818 Color: 13

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 465101 Color: 11
Size: 281997 Color: 13
Size: 252903 Color: 13

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 465136 Color: 14
Size: 271914 Color: 11
Size: 262951 Color: 1

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 465139 Color: 2
Size: 282091 Color: 4
Size: 252771 Color: 13

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 465152 Color: 17
Size: 270774 Color: 1
Size: 264075 Color: 1

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 465277 Color: 3
Size: 284672 Color: 12
Size: 250052 Color: 10

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 465303 Color: 3
Size: 281330 Color: 11
Size: 253368 Color: 6

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 465341 Color: 16
Size: 279761 Color: 19
Size: 254899 Color: 13

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 465351 Color: 4
Size: 281763 Color: 0
Size: 252887 Color: 5

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 465369 Color: 5
Size: 274808 Color: 3
Size: 259824 Color: 7

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 465387 Color: 11
Size: 269359 Color: 12
Size: 265255 Color: 4

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 465472 Color: 5
Size: 280584 Color: 14
Size: 253945 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 465493 Color: 13
Size: 272421 Color: 0
Size: 262087 Color: 18

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 465611 Color: 10
Size: 280248 Color: 8
Size: 254142 Color: 7

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 465535 Color: 9
Size: 272326 Color: 13
Size: 262140 Color: 16

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 465676 Color: 4
Size: 282131 Color: 8
Size: 252194 Color: 11

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 465723 Color: 4
Size: 269245 Color: 18
Size: 265033 Color: 15

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 465784 Color: 5
Size: 268067 Color: 18
Size: 266150 Color: 11

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 465842 Color: 3
Size: 280976 Color: 13
Size: 253183 Color: 9

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 465843 Color: 12
Size: 268882 Color: 4
Size: 265276 Color: 16

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 466085 Color: 10
Size: 275743 Color: 13
Size: 258173 Color: 12

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 465939 Color: 14
Size: 269089 Color: 5
Size: 264973 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 466010 Color: 16
Size: 281386 Color: 8
Size: 252605 Color: 14

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 466166 Color: 10
Size: 283196 Color: 4
Size: 250639 Color: 8

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 466051 Color: 3
Size: 278087 Color: 14
Size: 255863 Color: 14

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 466290 Color: 5
Size: 278731 Color: 17
Size: 254980 Color: 8

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 466292 Color: 11
Size: 274257 Color: 13
Size: 259452 Color: 3

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 466304 Color: 5
Size: 283356 Color: 10
Size: 250341 Color: 12

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 466324 Color: 8
Size: 273443 Color: 11
Size: 260234 Color: 12

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 466371 Color: 3
Size: 279280 Color: 3
Size: 254350 Color: 6

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 466387 Color: 12
Size: 282391 Color: 14
Size: 251223 Color: 13

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 466410 Color: 17
Size: 281257 Color: 12
Size: 252334 Color: 12

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 466526 Color: 6
Size: 269971 Color: 7
Size: 263504 Color: 5

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 466531 Color: 3
Size: 279317 Color: 10
Size: 254153 Color: 4

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 466536 Color: 14
Size: 272950 Color: 7
Size: 260515 Color: 17

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 466547 Color: 4
Size: 272829 Color: 13
Size: 260625 Color: 14

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 466608 Color: 9
Size: 267577 Color: 5
Size: 265816 Color: 13

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 466637 Color: 6
Size: 280218 Color: 0
Size: 253146 Color: 4

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 466653 Color: 9
Size: 271452 Color: 8
Size: 261896 Color: 1

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 466685 Color: 6
Size: 271008 Color: 10
Size: 262308 Color: 2

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 466714 Color: 14
Size: 280838 Color: 17
Size: 252449 Color: 6

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 466791 Color: 1
Size: 275593 Color: 5
Size: 257617 Color: 13

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 466863 Color: 17
Size: 282644 Color: 12
Size: 250494 Color: 7

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 466885 Color: 7
Size: 282111 Color: 10
Size: 251005 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 466925 Color: 4
Size: 276922 Color: 3
Size: 256154 Color: 11

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 466938 Color: 5
Size: 274586 Color: 8
Size: 258477 Color: 1

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 466979 Color: 7
Size: 271285 Color: 5
Size: 261737 Color: 16

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 467005 Color: 8
Size: 280902 Color: 6
Size: 252094 Color: 4

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 467068 Color: 12
Size: 272677 Color: 0
Size: 260256 Color: 2

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 467097 Color: 3
Size: 280795 Color: 6
Size: 252109 Color: 14

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 467117 Color: 15
Size: 277186 Color: 17
Size: 255698 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 467119 Color: 5
Size: 267637 Color: 9
Size: 265245 Color: 12

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 467149 Color: 11
Size: 267684 Color: 4
Size: 265168 Color: 1

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 467209 Color: 6
Size: 278525 Color: 10
Size: 254267 Color: 17

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 467215 Color: 3
Size: 280757 Color: 19
Size: 252029 Color: 5

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 467262 Color: 18
Size: 281085 Color: 19
Size: 251654 Color: 13

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 467331 Color: 9
Size: 268213 Color: 18
Size: 264457 Color: 3

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 467344 Color: 19
Size: 281897 Color: 0
Size: 250760 Color: 8

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 467344 Color: 16
Size: 278659 Color: 17
Size: 253998 Color: 12

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 467387 Color: 11
Size: 269178 Color: 19
Size: 263436 Color: 12

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 467503 Color: 13
Size: 272807 Color: 8
Size: 259691 Color: 19

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 467528 Color: 0
Size: 281846 Color: 9
Size: 250627 Color: 19

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 467681 Color: 19
Size: 274825 Color: 16
Size: 257495 Color: 9

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 467690 Color: 19
Size: 279821 Color: 9
Size: 252490 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 467834 Color: 15
Size: 277166 Color: 10
Size: 255001 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 467854 Color: 16
Size: 274764 Color: 0
Size: 257383 Color: 19

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 467865 Color: 11
Size: 267958 Color: 4
Size: 264178 Color: 13

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 467957 Color: 0
Size: 279568 Color: 17
Size: 252476 Color: 17

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 468054 Color: 0
Size: 274011 Color: 12
Size: 257936 Color: 14

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 468075 Color: 4
Size: 266429 Color: 8
Size: 265497 Color: 10

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 468131 Color: 3
Size: 271376 Color: 13
Size: 260494 Color: 7

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 468147 Color: 19
Size: 273526 Color: 9
Size: 258328 Color: 15

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 468212 Color: 8
Size: 270825 Color: 19
Size: 260964 Color: 1

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 468222 Color: 5
Size: 280771 Color: 0
Size: 251008 Color: 8

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 468281 Color: 6
Size: 271923 Color: 3
Size: 259797 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 468683 Color: 10
Size: 278474 Color: 8
Size: 252844 Color: 11

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 468554 Color: 14
Size: 266106 Color: 5
Size: 265341 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 468684 Color: 8
Size: 272135 Color: 11
Size: 259182 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 468712 Color: 13
Size: 273133 Color: 10
Size: 258156 Color: 12

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 468731 Color: 7
Size: 267845 Color: 14
Size: 263425 Color: 16

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 468810 Color: 19
Size: 280741 Color: 11
Size: 250450 Color: 9

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 468880 Color: 16
Size: 268137 Color: 3
Size: 262984 Color: 7

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 468902 Color: 3
Size: 270419 Color: 18
Size: 260680 Color: 2

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 468974 Color: 1
Size: 269633 Color: 19
Size: 261394 Color: 12

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 469037 Color: 0
Size: 276878 Color: 10
Size: 254086 Color: 5

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 469050 Color: 9
Size: 271527 Color: 6
Size: 259424 Color: 5

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 469097 Color: 13
Size: 271579 Color: 19
Size: 259325 Color: 8

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 469102 Color: 11
Size: 270915 Color: 17
Size: 259984 Color: 3

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 469159 Color: 6
Size: 274237 Color: 1
Size: 256605 Color: 4

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 469252 Color: 6
Size: 274233 Color: 10
Size: 256516 Color: 19

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 469293 Color: 17
Size: 274033 Color: 1
Size: 256675 Color: 5

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 469359 Color: 16
Size: 279520 Color: 14
Size: 251122 Color: 18

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 469375 Color: 15
Size: 265864 Color: 4
Size: 264762 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 469379 Color: 9
Size: 265908 Color: 14
Size: 264714 Color: 4

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 469463 Color: 3
Size: 274064 Color: 19
Size: 256474 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 469524 Color: 16
Size: 268533 Color: 12
Size: 261944 Color: 8

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 469558 Color: 18
Size: 270863 Color: 16
Size: 259580 Color: 5

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 469729 Color: 10
Size: 265474 Color: 6
Size: 264798 Color: 7

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 469748 Color: 8
Size: 276768 Color: 13
Size: 253485 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 469801 Color: 8
Size: 275801 Color: 1
Size: 254399 Color: 5

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 469807 Color: 17
Size: 265150 Color: 7
Size: 265044 Color: 10

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 470121 Color: 3
Size: 273686 Color: 11
Size: 256194 Color: 7

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 470277 Color: 4
Size: 275164 Color: 11
Size: 254560 Color: 6

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 470430 Color: 0
Size: 265293 Color: 9
Size: 264278 Color: 14

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 470427 Color: 10
Size: 268611 Color: 19
Size: 260963 Color: 11

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 470581 Color: 3
Size: 279265 Color: 2
Size: 250155 Color: 18

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 470640 Color: 14
Size: 269236 Color: 18
Size: 260125 Color: 15

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 470722 Color: 16
Size: 275698 Color: 14
Size: 253581 Color: 9

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 470824 Color: 1
Size: 271348 Color: 6
Size: 257829 Color: 7

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 470948 Color: 3
Size: 267863 Color: 15
Size: 261190 Color: 18

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 471009 Color: 8
Size: 274428 Color: 6
Size: 254564 Color: 17

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 471037 Color: 19
Size: 265806 Color: 9
Size: 263158 Color: 13

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 471059 Color: 5
Size: 269582 Color: 13
Size: 259360 Color: 5

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 471087 Color: 18
Size: 269955 Color: 4
Size: 258959 Color: 2

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 471122 Color: 6
Size: 270339 Color: 11
Size: 258540 Color: 8

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 471196 Color: 14
Size: 267432 Color: 11
Size: 261373 Color: 11

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 471209 Color: 3
Size: 268026 Color: 18
Size: 260766 Color: 9

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 471251 Color: 2
Size: 271649 Color: 6
Size: 257101 Color: 0

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 471296 Color: 7
Size: 264361 Color: 9
Size: 264344 Color: 11

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 471401 Color: 0
Size: 278587 Color: 10
Size: 250013 Color: 5

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 471430 Color: 0
Size: 278495 Color: 6
Size: 250076 Color: 8

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 471484 Color: 19
Size: 269342 Color: 5
Size: 259175 Color: 6

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 471496 Color: 1
Size: 265185 Color: 9
Size: 263320 Color: 4

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 471531 Color: 15
Size: 273928 Color: 19
Size: 254542 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 471605 Color: 9
Size: 275681 Color: 6
Size: 252715 Color: 8

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 471617 Color: 11
Size: 275974 Color: 13
Size: 252410 Color: 10

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 471651 Color: 2
Size: 269852 Color: 4
Size: 258498 Color: 12

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 471807 Color: 3
Size: 277518 Color: 19
Size: 250676 Color: 13

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 471808 Color: 14
Size: 276077 Color: 19
Size: 252116 Color: 16

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 471820 Color: 3
Size: 270068 Color: 18
Size: 258113 Color: 2

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 471897 Color: 7
Size: 276300 Color: 10
Size: 251804 Color: 3

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 471904 Color: 13
Size: 272667 Color: 0
Size: 255430 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 471939 Color: 18
Size: 273753 Color: 11
Size: 254309 Color: 19

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 472151 Color: 1
Size: 267731 Color: 0
Size: 260119 Color: 17

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 472205 Color: 2
Size: 276281 Color: 19
Size: 251515 Color: 15

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 472676 Color: 11
Size: 271642 Color: 14
Size: 255683 Color: 3

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 472965 Color: 4
Size: 274474 Color: 5
Size: 252562 Color: 3

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 473024 Color: 13
Size: 271235 Color: 12
Size: 255742 Color: 3

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 473092 Color: 7
Size: 267380 Color: 15
Size: 259529 Color: 7

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 473122 Color: 2
Size: 263834 Color: 4
Size: 263045 Color: 8

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 473138 Color: 17
Size: 263723 Color: 4
Size: 263140 Color: 7

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 473355 Color: 14
Size: 274347 Color: 7
Size: 252299 Color: 7

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 473599 Color: 10
Size: 274448 Color: 18
Size: 251954 Color: 9

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 473567 Color: 5
Size: 273787 Color: 3
Size: 252647 Color: 1

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 473613 Color: 12
Size: 272565 Color: 10
Size: 253823 Color: 2

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 473640 Color: 0
Size: 271322 Color: 17
Size: 255039 Color: 8

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 473695 Color: 9
Size: 271104 Color: 9
Size: 255202 Color: 13

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 473702 Color: 4
Size: 276261 Color: 5
Size: 250038 Color: 3

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 473796 Color: 11
Size: 266685 Color: 3
Size: 259520 Color: 19

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 473801 Color: 14
Size: 270385 Color: 16
Size: 255815 Color: 4

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 473909 Color: 6
Size: 269977 Color: 8
Size: 256115 Color: 10

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 473912 Color: 4
Size: 271174 Color: 19
Size: 254915 Color: 1

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 474078 Color: 2
Size: 273194 Color: 15
Size: 252729 Color: 12

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 474159 Color: 12
Size: 273638 Color: 16
Size: 252204 Color: 13

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 474319 Color: 2
Size: 263353 Color: 12
Size: 262329 Color: 15

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 474388 Color: 18
Size: 267837 Color: 10
Size: 257776 Color: 19

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 474442 Color: 8
Size: 275123 Color: 16
Size: 250436 Color: 1

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 474568 Color: 15
Size: 263334 Color: 14
Size: 262099 Color: 12

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 474652 Color: 15
Size: 263266 Color: 14
Size: 262083 Color: 9

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 474666 Color: 14
Size: 274101 Color: 12
Size: 251234 Color: 19

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 474787 Color: 3
Size: 263256 Color: 9
Size: 261958 Color: 13

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 474839 Color: 0
Size: 272861 Color: 10
Size: 252301 Color: 12

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 474950 Color: 7
Size: 272117 Color: 9
Size: 252934 Color: 1

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 475129 Color: 2
Size: 271095 Color: 6
Size: 253777 Color: 9

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 475206 Color: 16
Size: 269973 Color: 18
Size: 254822 Color: 16

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 475265 Color: 19
Size: 265315 Color: 4
Size: 259421 Color: 1

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 475295 Color: 13
Size: 265581 Color: 12
Size: 259125 Color: 10

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 475510 Color: 5
Size: 270464 Color: 3
Size: 254027 Color: 18

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 475654 Color: 17
Size: 270765 Color: 11
Size: 253582 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 475725 Color: 9
Size: 269563 Color: 6
Size: 254713 Color: 12

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 475751 Color: 17
Size: 266376 Color: 3
Size: 257874 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 475772 Color: 11
Size: 267001 Color: 19
Size: 257228 Color: 15

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 475780 Color: 2
Size: 263220 Color: 10
Size: 261001 Color: 17

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 475782 Color: 0
Size: 273360 Color: 15
Size: 250859 Color: 7

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 475935 Color: 2
Size: 267392 Color: 9
Size: 256674 Color: 6

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 475969 Color: 6
Size: 266842 Color: 0
Size: 257190 Color: 11

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 476003 Color: 13
Size: 269404 Color: 10
Size: 254594 Color: 1

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 476042 Color: 7
Size: 271799 Color: 15
Size: 252160 Color: 11

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 476070 Color: 5
Size: 269415 Color: 4
Size: 254516 Color: 14

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 476126 Color: 15
Size: 273635 Color: 4
Size: 250240 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 476169 Color: 8
Size: 267897 Color: 18
Size: 255935 Color: 4

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 476255 Color: 9
Size: 271713 Color: 15
Size: 252033 Color: 10

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 476257 Color: 4
Size: 272578 Color: 18
Size: 251166 Color: 1

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 476264 Color: 9
Size: 264242 Color: 8
Size: 259495 Color: 6

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 476270 Color: 18
Size: 270356 Color: 4
Size: 253375 Color: 2

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 476386 Color: 4
Size: 263516 Color: 2
Size: 260099 Color: 14

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 476387 Color: 16
Size: 268621 Color: 12
Size: 254993 Color: 2

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 476431 Color: 2
Size: 267934 Color: 10
Size: 255636 Color: 5

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 476493 Color: 14
Size: 270767 Color: 0
Size: 252741 Color: 5

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 476658 Color: 9
Size: 264752 Color: 17
Size: 258591 Color: 2

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 476793 Color: 4
Size: 264211 Color: 19
Size: 258997 Color: 8

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 477018 Color: 7
Size: 265717 Color: 12
Size: 257266 Color: 14

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 477050 Color: 0
Size: 265472 Color: 10
Size: 257479 Color: 12

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 477149 Color: 9
Size: 265828 Color: 3
Size: 257024 Color: 5

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 477161 Color: 12
Size: 269042 Color: 12
Size: 253798 Color: 4

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 477164 Color: 13
Size: 271891 Color: 3
Size: 250946 Color: 11

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 477193 Color: 12
Size: 265750 Color: 19
Size: 257058 Color: 12

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 477264 Color: 12
Size: 270323 Color: 9
Size: 252414 Color: 15

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 477350 Color: 13
Size: 266939 Color: 11
Size: 255712 Color: 10

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 477667 Color: 6
Size: 270734 Color: 9
Size: 251600 Color: 14

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 477890 Color: 16
Size: 271342 Color: 18
Size: 250769 Color: 7

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 477948 Color: 13
Size: 271065 Color: 19
Size: 250988 Color: 6

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 477950 Color: 11
Size: 271428 Color: 4
Size: 250623 Color: 7

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 478051 Color: 0
Size: 271131 Color: 9
Size: 250819 Color: 3

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 478185 Color: 16
Size: 261229 Color: 0
Size: 260587 Color: 11

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 478187 Color: 11
Size: 268737 Color: 3
Size: 253077 Color: 18

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 478238 Color: 15
Size: 267265 Color: 19
Size: 254498 Color: 11

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 478332 Color: 15
Size: 263752 Color: 12
Size: 257917 Color: 16

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 478346 Color: 2
Size: 261535 Color: 15
Size: 260120 Color: 1

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 478385 Color: 11
Size: 270372 Color: 10
Size: 251244 Color: 11

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 478407 Color: 15
Size: 265048 Color: 8
Size: 256546 Color: 17

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 478439 Color: 8
Size: 264347 Color: 6
Size: 257215 Color: 11

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 478479 Color: 19
Size: 260824 Color: 7
Size: 260698 Color: 15

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 478487 Color: 19
Size: 265873 Color: 9
Size: 255641 Color: 11

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 478525 Color: 11
Size: 262411 Color: 8
Size: 259065 Color: 6

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 478595 Color: 5
Size: 270141 Color: 4
Size: 251265 Color: 10

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 478616 Color: 12
Size: 270672 Color: 13
Size: 250713 Color: 2

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 478616 Color: 14
Size: 269805 Color: 3
Size: 251580 Color: 12

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 478686 Color: 3
Size: 264247 Color: 9
Size: 257068 Color: 13

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 478984 Color: 1
Size: 268575 Color: 8
Size: 252442 Color: 2

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 479028 Color: 11
Size: 261943 Color: 9
Size: 259030 Color: 19

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 479111 Color: 5
Size: 266078 Color: 18
Size: 254812 Color: 10

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 479198 Color: 17
Size: 265938 Color: 4
Size: 254865 Color: 6

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 479335 Color: 17
Size: 263988 Color: 14
Size: 256678 Color: 16

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 479374 Color: 4
Size: 264901 Color: 13
Size: 255726 Color: 11

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 479470 Color: 7
Size: 268433 Color: 4
Size: 252098 Color: 2

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 479508 Color: 10
Size: 265942 Color: 7
Size: 254551 Color: 12

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 479552 Color: 8
Size: 269723 Color: 7
Size: 250726 Color: 9

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 479669 Color: 2
Size: 266740 Color: 18
Size: 253592 Color: 15

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 479695 Color: 17
Size: 263903 Color: 9
Size: 256403 Color: 19

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 479763 Color: 15
Size: 267030 Color: 11
Size: 253208 Color: 12

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 479816 Color: 0
Size: 260683 Color: 18
Size: 259502 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 479835 Color: 6
Size: 270156 Color: 5
Size: 250010 Color: 17

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 479996 Color: 6
Size: 260025 Color: 8
Size: 259980 Color: 4

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 480140 Color: 11
Size: 269413 Color: 4
Size: 250448 Color: 8

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 480179 Color: 2
Size: 260495 Color: 8
Size: 259327 Color: 19

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 480239 Color: 12
Size: 263374 Color: 3
Size: 256388 Color: 10

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 480298 Color: 9
Size: 268622 Color: 18
Size: 251081 Color: 4

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 480438 Color: 0
Size: 262891 Color: 12
Size: 256672 Color: 18

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 480442 Color: 9
Size: 264122 Color: 17
Size: 255437 Color: 5

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 480451 Color: 15
Size: 269406 Color: 14
Size: 250144 Color: 15

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 480468 Color: 18
Size: 264538 Color: 9
Size: 254995 Color: 10

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 480543 Color: 12
Size: 268940 Color: 4
Size: 250518 Color: 4

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 480650 Color: 1
Size: 260853 Color: 8
Size: 258498 Color: 19

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 480732 Color: 18
Size: 269086 Color: 17
Size: 250183 Color: 13

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 480835 Color: 17
Size: 265186 Color: 16
Size: 253980 Color: 2

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 480916 Color: 6
Size: 268009 Color: 15
Size: 251076 Color: 9

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 480985 Color: 18
Size: 260492 Color: 2
Size: 258524 Color: 9

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 481090 Color: 10
Size: 265925 Color: 13
Size: 252986 Color: 15

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 481053 Color: 0
Size: 261752 Color: 13
Size: 257196 Color: 17

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 481076 Color: 19
Size: 266155 Color: 16
Size: 252770 Color: 6

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 481124 Color: 5
Size: 268635 Color: 0
Size: 250242 Color: 1

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 481145 Color: 0
Size: 261269 Color: 16
Size: 257587 Color: 3

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 481203 Color: 3
Size: 262178 Color: 15
Size: 256620 Color: 6

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 481485 Color: 18
Size: 262698 Color: 14
Size: 255818 Color: 3

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 481580 Color: 14
Size: 265888 Color: 5
Size: 252533 Color: 12

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 481584 Color: 16
Size: 267063 Color: 1
Size: 251354 Color: 7

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 481606 Color: 7
Size: 263622 Color: 10
Size: 254773 Color: 2

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 481700 Color: 17
Size: 261187 Color: 16
Size: 257114 Color: 3

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 481763 Color: 0
Size: 261321 Color: 1
Size: 256917 Color: 2

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 481841 Color: 1
Size: 266554 Color: 12
Size: 251606 Color: 19

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 481907 Color: 13
Size: 260513 Color: 19
Size: 257581 Color: 15

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 481934 Color: 4
Size: 259373 Color: 16
Size: 258694 Color: 3

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 481978 Color: 18
Size: 260899 Color: 13
Size: 257124 Color: 10

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 482017 Color: 19
Size: 265488 Color: 7
Size: 252496 Color: 9

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 482208 Color: 19
Size: 259350 Color: 1
Size: 258443 Color: 8

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 482412 Color: 1
Size: 266508 Color: 2
Size: 251081 Color: 8

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 482529 Color: 0
Size: 259957 Color: 15
Size: 257515 Color: 12

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 482554 Color: 19
Size: 264288 Color: 10
Size: 253159 Color: 13

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 482662 Color: 9
Size: 259682 Color: 11
Size: 257657 Color: 17

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 482693 Color: 18
Size: 262133 Color: 17
Size: 255175 Color: 1

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 482771 Color: 3
Size: 266175 Color: 16
Size: 251055 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 482883 Color: 18
Size: 260820 Color: 5
Size: 256298 Color: 6

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 483043 Color: 6
Size: 260434 Color: 8
Size: 256524 Color: 5

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 483050 Color: 19
Size: 261657 Color: 0
Size: 255294 Color: 7

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 483107 Color: 2
Size: 261227 Color: 11
Size: 255667 Color: 14

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 483186 Color: 11
Size: 260786 Color: 9
Size: 256029 Color: 9

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 483213 Color: 4
Size: 262370 Color: 19
Size: 254418 Color: 13

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 483295 Color: 3
Size: 258721 Color: 13
Size: 257985 Color: 10

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 483381 Color: 12
Size: 261930 Color: 7
Size: 254690 Color: 7

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 483390 Color: 0
Size: 259671 Color: 18
Size: 256940 Color: 11

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 483421 Color: 14
Size: 265510 Color: 7
Size: 251070 Color: 19

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 483423 Color: 0
Size: 264729 Color: 16
Size: 251849 Color: 13

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 483480 Color: 2
Size: 260205 Color: 6
Size: 256316 Color: 5

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 483538 Color: 18
Size: 265008 Color: 10
Size: 251455 Color: 1

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 483549 Color: 9
Size: 258946 Color: 6
Size: 257506 Color: 2

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 483606 Color: 0
Size: 260863 Color: 1
Size: 255532 Color: 5

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 483762 Color: 5
Size: 259992 Color: 9
Size: 256247 Color: 13

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 484025 Color: 0
Size: 262008 Color: 3
Size: 253968 Color: 17

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 484026 Color: 2
Size: 263470 Color: 4
Size: 252505 Color: 6

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 484112 Color: 12
Size: 260153 Color: 7
Size: 255736 Color: 15

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 484154 Color: 13
Size: 265837 Color: 11
Size: 250010 Color: 8

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 484282 Color: 11
Size: 259851 Color: 2
Size: 255868 Color: 5

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 484384 Color: 13
Size: 261685 Color: 3
Size: 253932 Color: 10

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 484488 Color: 15
Size: 262585 Color: 8
Size: 252928 Color: 17

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 484501 Color: 18
Size: 261263 Color: 5
Size: 254237 Color: 1

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 484512 Color: 18
Size: 258148 Color: 8
Size: 257341 Color: 11

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 484806 Color: 2
Size: 262366 Color: 1
Size: 252829 Color: 1

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 484818 Color: 16
Size: 264342 Color: 10
Size: 250841 Color: 3

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 484855 Color: 11
Size: 264375 Color: 15
Size: 250771 Color: 1

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 485055 Color: 16
Size: 258678 Color: 11
Size: 256268 Color: 0

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 485076 Color: 19
Size: 263354 Color: 4
Size: 251571 Color: 16

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 485225 Color: 11
Size: 260233 Color: 5
Size: 254543 Color: 6

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 485243 Color: 17
Size: 264418 Color: 3
Size: 250340 Color: 12

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 485335 Color: 5
Size: 258359 Color: 10
Size: 256307 Color: 6

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 485547 Color: 14
Size: 260334 Color: 9
Size: 254120 Color: 17

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 485643 Color: 7
Size: 263027 Color: 0
Size: 251331 Color: 11

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 485668 Color: 4
Size: 261000 Color: 16
Size: 253333 Color: 12

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 485676 Color: 13
Size: 263916 Color: 18
Size: 250409 Color: 6

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 485767 Color: 2
Size: 261327 Color: 16
Size: 252907 Color: 18

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 485814 Color: 18
Size: 262739 Color: 11
Size: 251448 Color: 4

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 485878 Color: 7
Size: 259232 Color: 4
Size: 254891 Color: 1

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 486113 Color: 16
Size: 263410 Color: 12
Size: 250478 Color: 14

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 486142 Color: 5
Size: 262675 Color: 9
Size: 251184 Color: 17

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 486173 Color: 11
Size: 262540 Color: 0
Size: 251288 Color: 10

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 486182 Color: 18
Size: 258889 Color: 17
Size: 254930 Color: 17

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 486597 Color: 19
Size: 257989 Color: 18
Size: 255415 Color: 7

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 486636 Color: 6
Size: 261791 Color: 9
Size: 251574 Color: 15

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 486677 Color: 7
Size: 256904 Color: 12
Size: 256420 Color: 10

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 486749 Color: 18
Size: 259760 Color: 14
Size: 253492 Color: 12

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 486833 Color: 3
Size: 258736 Color: 16
Size: 254432 Color: 1

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 486843 Color: 17
Size: 257110 Color: 9
Size: 256048 Color: 15

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 487131 Color: 17
Size: 262751 Color: 18
Size: 250119 Color: 2

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 487156 Color: 2
Size: 261705 Color: 10
Size: 251140 Color: 16

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 487181 Color: 2
Size: 262806 Color: 16
Size: 250014 Color: 11

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 487185 Color: 19
Size: 257933 Color: 1
Size: 254883 Color: 6

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 487749 Color: 4
Size: 260859 Color: 19
Size: 251393 Color: 12

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 487854 Color: 1
Size: 259326 Color: 15
Size: 252821 Color: 8

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 487902 Color: 3
Size: 261380 Color: 15
Size: 250719 Color: 3

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 487951 Color: 18
Size: 257670 Color: 19
Size: 254380 Color: 4

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 487952 Color: 5
Size: 258018 Color: 19
Size: 254031 Color: 16

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 488280 Color: 10
Size: 261701 Color: 14
Size: 250020 Color: 2

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 488091 Color: 11
Size: 258387 Color: 12
Size: 253523 Color: 8

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 488136 Color: 1
Size: 259833 Color: 4
Size: 252032 Color: 1

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 488152 Color: 13
Size: 256510 Color: 3
Size: 255339 Color: 1

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 488188 Color: 14
Size: 255997 Color: 16
Size: 255816 Color: 13

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 488442 Color: 10
Size: 260926 Color: 3
Size: 250633 Color: 7

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 488399 Color: 12
Size: 258234 Color: 9
Size: 253368 Color: 9

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 488405 Color: 5
Size: 260416 Color: 3
Size: 251180 Color: 6

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 488489 Color: 10
Size: 259562 Color: 4
Size: 251950 Color: 9

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 488425 Color: 2
Size: 258603 Color: 13
Size: 252973 Color: 6

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 488477 Color: 6
Size: 261265 Color: 8
Size: 250259 Color: 9

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 488632 Color: 4
Size: 260561 Color: 19
Size: 250808 Color: 9

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 488681 Color: 17
Size: 258351 Color: 0
Size: 252969 Color: 16

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 488982 Color: 1
Size: 260912 Color: 10
Size: 250107 Color: 4

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 489092 Color: 13
Size: 258039 Color: 15
Size: 252870 Color: 18

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 489197 Color: 15
Size: 258058 Color: 19
Size: 252746 Color: 18

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 489216 Color: 1
Size: 260658 Color: 15
Size: 250127 Color: 3

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 489473 Color: 3
Size: 259909 Color: 13
Size: 250619 Color: 1

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 489516 Color: 19
Size: 258444 Color: 17
Size: 252041 Color: 15

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 489677 Color: 10
Size: 257298 Color: 11
Size: 253026 Color: 4

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 489607 Color: 8
Size: 259459 Color: 11
Size: 250935 Color: 7

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 489667 Color: 18
Size: 257557 Color: 7
Size: 252777 Color: 4

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 489722 Color: 16
Size: 258028 Color: 18
Size: 252251 Color: 18

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 489730 Color: 18
Size: 259110 Color: 10
Size: 251161 Color: 2

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 489864 Color: 2
Size: 255832 Color: 18
Size: 254305 Color: 5

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 489869 Color: 3
Size: 260115 Color: 14
Size: 250017 Color: 7

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 489971 Color: 9
Size: 258430 Color: 7
Size: 251600 Color: 19

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 490062 Color: 12
Size: 256788 Color: 7
Size: 253151 Color: 15

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 490093 Color: 1
Size: 255222 Color: 19
Size: 254686 Color: 19

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 490269 Color: 4
Size: 257222 Color: 1
Size: 252510 Color: 10

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 490389 Color: 9
Size: 259094 Color: 2
Size: 250518 Color: 15

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 490423 Color: 11
Size: 259240 Color: 6
Size: 250338 Color: 4

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 490425 Color: 9
Size: 259140 Color: 1
Size: 250436 Color: 19

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 490466 Color: 12
Size: 256790 Color: 11
Size: 252745 Color: 9

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 490483 Color: 15
Size: 257733 Color: 15
Size: 251785 Color: 19

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 490490 Color: 16
Size: 257589 Color: 6
Size: 251922 Color: 10

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 490602 Color: 15
Size: 256722 Color: 19
Size: 252677 Color: 5

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 490679 Color: 6
Size: 254851 Color: 11
Size: 254471 Color: 4

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 490751 Color: 2
Size: 257156 Color: 8
Size: 252094 Color: 18

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 490803 Color: 17
Size: 255308 Color: 15
Size: 253890 Color: 3

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 490906 Color: 16
Size: 258895 Color: 10
Size: 250200 Color: 14

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 490934 Color: 11
Size: 258435 Color: 1
Size: 250632 Color: 5

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 490964 Color: 1
Size: 257294 Color: 7
Size: 251743 Color: 8

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 491149 Color: 11
Size: 258301 Color: 0
Size: 250551 Color: 8

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 491233 Color: 11
Size: 255637 Color: 16
Size: 253131 Color: 13

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 491381 Color: 0
Size: 257415 Color: 17
Size: 251205 Color: 8

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 491466 Color: 2
Size: 258107 Color: 17
Size: 250428 Color: 7

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 491524 Color: 16
Size: 255508 Color: 1
Size: 252969 Color: 11

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 491587 Color: 16
Size: 254652 Color: 14
Size: 253762 Color: 18

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 491828 Color: 10
Size: 255152 Color: 15
Size: 253021 Color: 14

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 491938 Color: 17
Size: 257035 Color: 8
Size: 251028 Color: 10

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 491997 Color: 14
Size: 254342 Color: 11
Size: 253662 Color: 9

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 492027 Color: 11
Size: 256563 Color: 7
Size: 251411 Color: 7

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 492041 Color: 6
Size: 257840 Color: 12
Size: 250120 Color: 14

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 492099 Color: 1
Size: 256230 Color: 19
Size: 251672 Color: 0

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 492204 Color: 11
Size: 254974 Color: 9
Size: 252823 Color: 9

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 492215 Color: 2
Size: 256052 Color: 17
Size: 251734 Color: 2

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 492287 Color: 12
Size: 255285 Color: 6
Size: 252429 Color: 18

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 492538 Color: 16
Size: 255921 Color: 3
Size: 251542 Color: 17

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 492584 Color: 9
Size: 253874 Color: 18
Size: 253543 Color: 16

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 492635 Color: 18
Size: 256119 Color: 13
Size: 251247 Color: 9

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 492731 Color: 8
Size: 254505 Color: 5
Size: 252765 Color: 16

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 492809 Color: 18
Size: 254063 Color: 5
Size: 253129 Color: 10

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 492987 Color: 18
Size: 255116 Color: 1
Size: 251898 Color: 12

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 493021 Color: 4
Size: 255220 Color: 11
Size: 251760 Color: 9

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 493061 Color: 0
Size: 254749 Color: 5
Size: 252191 Color: 18

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 493123 Color: 14
Size: 256042 Color: 0
Size: 250836 Color: 2

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 493213 Color: 12
Size: 255952 Color: 10
Size: 250836 Color: 3

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 493351 Color: 7
Size: 253539 Color: 17
Size: 253111 Color: 0

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 493436 Color: 1
Size: 254269 Color: 4
Size: 252296 Color: 15

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 493521 Color: 14
Size: 254157 Color: 15
Size: 252323 Color: 8

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 493597 Color: 11
Size: 253829 Color: 17
Size: 252575 Color: 13

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 493623 Color: 11
Size: 256207 Color: 11
Size: 250171 Color: 4

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 493630 Color: 18
Size: 253996 Color: 16
Size: 252375 Color: 10

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 493639 Color: 13
Size: 255257 Color: 16
Size: 251105 Color: 11

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 493777 Color: 7
Size: 255478 Color: 16
Size: 250746 Color: 7

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 493815 Color: 15
Size: 255305 Color: 17
Size: 250881 Color: 0

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 493945 Color: 10
Size: 255354 Color: 8
Size: 250702 Color: 14

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 494001 Color: 17
Size: 255228 Color: 0
Size: 250772 Color: 5

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 494129 Color: 19
Size: 254598 Color: 0
Size: 251274 Color: 0

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 494214 Color: 1
Size: 253476 Color: 13
Size: 252311 Color: 2

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 494277 Color: 5
Size: 255603 Color: 9
Size: 250121 Color: 12

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 494331 Color: 1
Size: 255062 Color: 0
Size: 250608 Color: 10

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 494393 Color: 14
Size: 255500 Color: 18
Size: 250108 Color: 8

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 494497 Color: 2
Size: 255049 Color: 18
Size: 250455 Color: 15

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 494497 Color: 17
Size: 255479 Color: 3
Size: 250025 Color: 12

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 494525 Color: 1
Size: 255047 Color: 3
Size: 250429 Color: 1

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 494561 Color: 2
Size: 255183 Color: 0
Size: 250257 Color: 15

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 494617 Color: 11
Size: 255009 Color: 2
Size: 250375 Color: 10

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 494628 Color: 6
Size: 254232 Color: 11
Size: 251141 Color: 19

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 494645 Color: 9
Size: 253188 Color: 7
Size: 252168 Color: 4

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 494701 Color: 17
Size: 254393 Color: 13
Size: 250907 Color: 9

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 495138 Color: 10
Size: 253239 Color: 0
Size: 251624 Color: 15

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 495126 Color: 0
Size: 254455 Color: 8
Size: 250420 Color: 12

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 495167 Color: 1
Size: 252611 Color: 11
Size: 252223 Color: 15

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 495172 Color: 14
Size: 252640 Color: 0
Size: 252189 Color: 7

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 495421 Color: 1
Size: 253132 Color: 18
Size: 251448 Color: 4

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 495447 Color: 19
Size: 253300 Color: 10
Size: 251254 Color: 11

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 495510 Color: 12
Size: 254373 Color: 9
Size: 250118 Color: 18

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 495547 Color: 1
Size: 253494 Color: 7
Size: 250960 Color: 3

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 495588 Color: 18
Size: 253808 Color: 11
Size: 250605 Color: 9

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 495617 Color: 13
Size: 253494 Color: 2
Size: 250890 Color: 15

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 495886 Color: 9
Size: 252747 Color: 10
Size: 251368 Color: 4

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 496203 Color: 11
Size: 253573 Color: 8
Size: 250225 Color: 5

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 496310 Color: 15
Size: 252843 Color: 4
Size: 250848 Color: 18

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 496329 Color: 15
Size: 253434 Color: 4
Size: 250238 Color: 2

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 496348 Color: 16
Size: 252328 Color: 11
Size: 251325 Color: 6

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 496406 Color: 15
Size: 253105 Color: 5
Size: 250490 Color: 3

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 496419 Color: 18
Size: 251917 Color: 0
Size: 251665 Color: 3

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 496423 Color: 19
Size: 252579 Color: 14
Size: 250999 Color: 6

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 496481 Color: 2
Size: 252307 Color: 13
Size: 251213 Color: 1

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 496583 Color: 10
Size: 252358 Color: 14
Size: 251060 Color: 4

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 496610 Color: 16
Size: 253228 Color: 19
Size: 250163 Color: 9

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 496613 Color: 9
Size: 252811 Color: 3
Size: 250577 Color: 14

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 496831 Color: 6
Size: 252687 Color: 14
Size: 250483 Color: 0

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 496873 Color: 4
Size: 252668 Color: 15
Size: 250460 Color: 10

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 496940 Color: 14
Size: 252897 Color: 2
Size: 250164 Color: 6

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 496963 Color: 5
Size: 252531 Color: 9
Size: 250507 Color: 19

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 497121 Color: 5
Size: 252413 Color: 13
Size: 250467 Color: 13

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 497254 Color: 11
Size: 251445 Color: 17
Size: 251302 Color: 16

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 497293 Color: 19
Size: 251856 Color: 15
Size: 250852 Color: 8

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 497336 Color: 11
Size: 251762 Color: 0
Size: 250903 Color: 15

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 497376 Color: 14
Size: 252014 Color: 16
Size: 250611 Color: 6

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 497421 Color: 4
Size: 251524 Color: 14
Size: 251056 Color: 5

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 497569 Color: 12
Size: 251786 Color: 6
Size: 250646 Color: 5

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 497770 Color: 10
Size: 252161 Color: 12
Size: 250070 Color: 12

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 497591 Color: 14
Size: 252073 Color: 12
Size: 250337 Color: 15

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 498021 Color: 12
Size: 251591 Color: 6
Size: 250389 Color: 17

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 498124 Color: 11
Size: 251178 Color: 8
Size: 250699 Color: 7

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 498197 Color: 2
Size: 251276 Color: 18
Size: 250528 Color: 1

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 498272 Color: 5
Size: 251660 Color: 6
Size: 250069 Color: 8

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 498348 Color: 14
Size: 251165 Color: 11
Size: 250488 Color: 1

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 498386 Color: 9
Size: 251579 Color: 16
Size: 250036 Color: 11

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 498406 Color: 0
Size: 251144 Color: 4
Size: 250451 Color: 9

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 498803 Color: 10
Size: 250898 Color: 14
Size: 250300 Color: 9

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 498455 Color: 19
Size: 251385 Color: 3
Size: 250161 Color: 0

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 498550 Color: 2
Size: 251009 Color: 16
Size: 250442 Color: 0

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 498565 Color: 14
Size: 250895 Color: 5
Size: 250541 Color: 19

Bin 3156: 0 of cap free
Amount of items: 3
Items: 
Size: 498881 Color: 10
Size: 250832 Color: 11
Size: 250288 Color: 11

Bin 3157: 0 of cap free
Amount of items: 3
Items: 
Size: 498672 Color: 9
Size: 250884 Color: 4
Size: 250445 Color: 11

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 498691 Color: 8
Size: 251051 Color: 5
Size: 250259 Color: 13

Bin 3159: 0 of cap free
Amount of items: 3
Items: 
Size: 498890 Color: 10
Size: 251074 Color: 16
Size: 250037 Color: 5

Bin 3160: 0 of cap free
Amount of items: 3
Items: 
Size: 498996 Color: 3
Size: 250535 Color: 10
Size: 250470 Color: 1

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 499361 Color: 1
Size: 250420 Color: 9
Size: 250220 Color: 16

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 499463 Color: 18
Size: 250499 Color: 5
Size: 250039 Color: 13

Bin 3163: 0 of cap free
Amount of items: 3
Items: 
Size: 499517 Color: 13
Size: 250336 Color: 2
Size: 250148 Color: 16

Bin 3164: 0 of cap free
Amount of items: 3
Items: 
Size: 499550 Color: 11
Size: 250388 Color: 0
Size: 250063 Color: 9

Bin 3165: 0 of cap free
Amount of items: 3
Items: 
Size: 499566 Color: 0
Size: 250297 Color: 5
Size: 250138 Color: 18

Bin 3166: 0 of cap free
Amount of items: 3
Items: 
Size: 499985 Color: 10
Size: 250011 Color: 3
Size: 250005 Color: 11

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 359514 Color: 5
Size: 346536 Color: 10
Size: 293950 Color: 2

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 363125 Color: 13
Size: 354841 Color: 17
Size: 282034 Color: 12

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 418263 Color: 11
Size: 330349 Color: 2
Size: 251388 Color: 15

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 342294 Color: 16
Size: 340340 Color: 16
Size: 317366 Color: 15

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 394661 Color: 14
Size: 354039 Color: 10
Size: 251300 Color: 10

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 348831 Color: 0
Size: 344506 Color: 2
Size: 306663 Color: 2

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 352996 Color: 16
Size: 351983 Color: 13
Size: 295021 Color: 18

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 358613 Color: 6
Size: 323295 Color: 13
Size: 318092 Color: 1

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 359104 Color: 3
Size: 328146 Color: 18
Size: 312750 Color: 10

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 361672 Color: 8
Size: 329845 Color: 9
Size: 308483 Color: 3

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 362448 Color: 14
Size: 334055 Color: 3
Size: 303497 Color: 8

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 365571 Color: 12
Size: 344164 Color: 12
Size: 290265 Color: 0

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 367685 Color: 8
Size: 349127 Color: 15
Size: 283188 Color: 16

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 368257 Color: 16
Size: 329394 Color: 8
Size: 302349 Color: 19

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 368404 Color: 9
Size: 339278 Color: 18
Size: 292318 Color: 5

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 370443 Color: 12
Size: 355695 Color: 15
Size: 273862 Color: 19

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 370693 Color: 0
Size: 359137 Color: 1
Size: 270170 Color: 19

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 371387 Color: 12
Size: 366814 Color: 0
Size: 261799 Color: 13

Bin 3185: 1 of cap free
Amount of items: 3
Items: 
Size: 374880 Color: 15
Size: 370760 Color: 6
Size: 254360 Color: 0

Bin 3186: 1 of cap free
Amount of items: 3
Items: 
Size: 379424 Color: 9
Size: 334677 Color: 1
Size: 285899 Color: 2

Bin 3187: 1 of cap free
Amount of items: 3
Items: 
Size: 385581 Color: 15
Size: 349877 Color: 5
Size: 264542 Color: 19

Bin 3188: 1 of cap free
Amount of items: 3
Items: 
Size: 434453 Color: 18
Size: 301207 Color: 10
Size: 264340 Color: 19

Bin 3189: 1 of cap free
Amount of items: 3
Items: 
Size: 367608 Color: 8
Size: 318907 Color: 18
Size: 313485 Color: 2

Bin 3190: 1 of cap free
Amount of items: 3
Items: 
Size: 351303 Color: 15
Size: 337957 Color: 0
Size: 310740 Color: 5

Bin 3191: 1 of cap free
Amount of items: 3
Items: 
Size: 354960 Color: 14
Size: 342601 Color: 4
Size: 302439 Color: 13

Bin 3192: 1 of cap free
Amount of items: 3
Items: 
Size: 357245 Color: 11
Size: 352981 Color: 9
Size: 289774 Color: 17

Bin 3193: 1 of cap free
Amount of items: 3
Items: 
Size: 349665 Color: 16
Size: 326533 Color: 17
Size: 323802 Color: 1

Bin 3194: 1 of cap free
Amount of items: 3
Items: 
Size: 360973 Color: 0
Size: 356365 Color: 10
Size: 282662 Color: 16

Bin 3195: 1 of cap free
Amount of items: 3
Items: 
Size: 365501 Color: 0
Size: 359057 Color: 5
Size: 275442 Color: 5

Bin 3196: 1 of cap free
Amount of items: 3
Items: 
Size: 346883 Color: 6
Size: 334948 Color: 7
Size: 318169 Color: 0

Bin 3197: 1 of cap free
Amount of items: 3
Items: 
Size: 351316 Color: 13
Size: 348176 Color: 15
Size: 300508 Color: 0

Bin 3198: 1 of cap free
Amount of items: 3
Items: 
Size: 355503 Color: 3
Size: 324580 Color: 0
Size: 319917 Color: 3

Bin 3199: 1 of cap free
Amount of items: 3
Items: 
Size: 372628 Color: 7
Size: 351966 Color: 9
Size: 275406 Color: 1

Bin 3200: 1 of cap free
Amount of items: 3
Items: 
Size: 356970 Color: 4
Size: 349551 Color: 19
Size: 293479 Color: 0

Bin 3201: 1 of cap free
Amount of items: 3
Items: 
Size: 375034 Color: 16
Size: 362424 Color: 12
Size: 262542 Color: 14

Bin 3202: 1 of cap free
Amount of items: 3
Items: 
Size: 360218 Color: 14
Size: 353247 Color: 17
Size: 286535 Color: 17

Bin 3203: 1 of cap free
Amount of items: 3
Items: 
Size: 381025 Color: 2
Size: 355398 Color: 18
Size: 263577 Color: 9

Bin 3204: 1 of cap free
Amount of items: 3
Items: 
Size: 363280 Color: 10
Size: 361554 Color: 6
Size: 275166 Color: 15

Bin 3205: 1 of cap free
Amount of items: 3
Items: 
Size: 353019 Color: 12
Size: 351945 Color: 2
Size: 295036 Color: 6

Bin 3206: 1 of cap free
Amount of items: 3
Items: 
Size: 358576 Color: 7
Size: 347378 Color: 7
Size: 294046 Color: 0

Bin 3207: 1 of cap free
Amount of items: 3
Items: 
Size: 352811 Color: 10
Size: 345274 Color: 17
Size: 301915 Color: 19

Bin 3208: 1 of cap free
Amount of items: 3
Items: 
Size: 353578 Color: 2
Size: 339116 Color: 18
Size: 307306 Color: 18

Bin 3209: 1 of cap free
Amount of items: 3
Items: 
Size: 343943 Color: 16
Size: 338674 Color: 15
Size: 317383 Color: 9

Bin 3210: 1 of cap free
Amount of items: 3
Items: 
Size: 415368 Color: 5
Size: 329247 Color: 6
Size: 255385 Color: 10

Bin 3211: 2 of cap free
Amount of items: 3
Items: 
Size: 345447 Color: 12
Size: 330154 Color: 6
Size: 324398 Color: 0

Bin 3212: 2 of cap free
Amount of items: 3
Items: 
Size: 435921 Color: 4
Size: 313599 Color: 8
Size: 250479 Color: 5

Bin 3213: 2 of cap free
Amount of items: 3
Items: 
Size: 374352 Color: 9
Size: 354567 Color: 19
Size: 271080 Color: 17

Bin 3214: 2 of cap free
Amount of items: 3
Items: 
Size: 365692 Color: 4
Size: 330365 Color: 7
Size: 303942 Color: 2

Bin 3215: 2 of cap free
Amount of items: 3
Items: 
Size: 369711 Color: 0
Size: 359953 Color: 14
Size: 270335 Color: 16

Bin 3216: 2 of cap free
Amount of items: 3
Items: 
Size: 377602 Color: 6
Size: 359681 Color: 15
Size: 262716 Color: 17

Bin 3217: 2 of cap free
Amount of items: 3
Items: 
Size: 359455 Color: 10
Size: 350091 Color: 18
Size: 290453 Color: 14

Bin 3218: 2 of cap free
Amount of items: 3
Items: 
Size: 361579 Color: 10
Size: 324402 Color: 2
Size: 314018 Color: 9

Bin 3219: 2 of cap free
Amount of items: 3
Items: 
Size: 373324 Color: 15
Size: 360728 Color: 9
Size: 265947 Color: 1

Bin 3220: 2 of cap free
Amount of items: 3
Items: 
Size: 340362 Color: 7
Size: 334572 Color: 10
Size: 325065 Color: 8

Bin 3221: 2 of cap free
Amount of items: 3
Items: 
Size: 356369 Color: 5
Size: 355471 Color: 18
Size: 288159 Color: 6

Bin 3222: 2 of cap free
Amount of items: 3
Items: 
Size: 358386 Color: 11
Size: 356278 Color: 11
Size: 285335 Color: 5

Bin 3223: 2 of cap free
Amount of items: 3
Items: 
Size: 349547 Color: 9
Size: 328680 Color: 3
Size: 321772 Color: 17

Bin 3224: 2 of cap free
Amount of items: 3
Items: 
Size: 379354 Color: 9
Size: 359362 Color: 1
Size: 261283 Color: 6

Bin 3225: 2 of cap free
Amount of items: 3
Items: 
Size: 343506 Color: 1
Size: 341356 Color: 1
Size: 315137 Color: 17

Bin 3226: 2 of cap free
Amount of items: 3
Items: 
Size: 359987 Color: 1
Size: 321160 Color: 7
Size: 318852 Color: 13

Bin 3227: 2 of cap free
Amount of items: 3
Items: 
Size: 379787 Color: 10
Size: 352521 Color: 11
Size: 267691 Color: 1

Bin 3228: 2 of cap free
Amount of items: 3
Items: 
Size: 344754 Color: 5
Size: 339140 Color: 17
Size: 316105 Color: 5

Bin 3229: 3 of cap free
Amount of items: 3
Items: 
Size: 361463 Color: 9
Size: 347757 Color: 8
Size: 290778 Color: 0

Bin 3230: 3 of cap free
Amount of items: 3
Items: 
Size: 360440 Color: 13
Size: 353262 Color: 18
Size: 286296 Color: 7

Bin 3231: 3 of cap free
Amount of items: 3
Items: 
Size: 365830 Color: 9
Size: 344013 Color: 9
Size: 290155 Color: 13

Bin 3232: 3 of cap free
Amount of items: 3
Items: 
Size: 353915 Color: 6
Size: 337681 Color: 6
Size: 308402 Color: 2

Bin 3233: 3 of cap free
Amount of items: 3
Items: 
Size: 359781 Color: 6
Size: 341924 Color: 0
Size: 298293 Color: 7

Bin 3234: 3 of cap free
Amount of items: 3
Items: 
Size: 361240 Color: 9
Size: 342442 Color: 17
Size: 296316 Color: 10

Bin 3235: 3 of cap free
Amount of items: 3
Items: 
Size: 335831 Color: 19
Size: 333580 Color: 6
Size: 330587 Color: 11

Bin 3236: 3 of cap free
Amount of items: 3
Items: 
Size: 354695 Color: 9
Size: 333693 Color: 19
Size: 311610 Color: 15

Bin 3237: 3 of cap free
Amount of items: 3
Items: 
Size: 347690 Color: 12
Size: 340631 Color: 3
Size: 311677 Color: 10

Bin 3238: 4 of cap free
Amount of items: 3
Items: 
Size: 377141 Color: 9
Size: 350328 Color: 6
Size: 272528 Color: 13

Bin 3239: 4 of cap free
Amount of items: 3
Items: 
Size: 364902 Color: 8
Size: 347009 Color: 8
Size: 288086 Color: 2

Bin 3240: 4 of cap free
Amount of items: 3
Items: 
Size: 351710 Color: 5
Size: 335741 Color: 7
Size: 312546 Color: 1

Bin 3241: 4 of cap free
Amount of items: 3
Items: 
Size: 356303 Color: 8
Size: 332714 Color: 12
Size: 310980 Color: 16

Bin 3242: 4 of cap free
Amount of items: 3
Items: 
Size: 346513 Color: 16
Size: 327938 Color: 11
Size: 325546 Color: 16

Bin 3243: 4 of cap free
Amount of items: 3
Items: 
Size: 354593 Color: 3
Size: 346434 Color: 0
Size: 298970 Color: 4

Bin 3244: 5 of cap free
Amount of items: 3
Items: 
Size: 352545 Color: 0
Size: 351273 Color: 18
Size: 296178 Color: 12

Bin 3245: 5 of cap free
Amount of items: 3
Items: 
Size: 362342 Color: 1
Size: 332937 Color: 12
Size: 304717 Color: 15

Bin 3246: 5 of cap free
Amount of items: 3
Items: 
Size: 363975 Color: 17
Size: 331912 Color: 15
Size: 304109 Color: 12

Bin 3247: 5 of cap free
Amount of items: 3
Items: 
Size: 348688 Color: 12
Size: 334939 Color: 16
Size: 316369 Color: 16

Bin 3248: 5 of cap free
Amount of items: 3
Items: 
Size: 353593 Color: 16
Size: 337095 Color: 5
Size: 309308 Color: 0

Bin 3249: 6 of cap free
Amount of items: 3
Items: 
Size: 358078 Color: 7
Size: 357207 Color: 11
Size: 284710 Color: 17

Bin 3250: 7 of cap free
Amount of items: 3
Items: 
Size: 391507 Color: 10
Size: 349667 Color: 1
Size: 258820 Color: 6

Bin 3251: 7 of cap free
Amount of items: 3
Items: 
Size: 358544 Color: 17
Size: 351648 Color: 19
Size: 289802 Color: 1

Bin 3252: 7 of cap free
Amount of items: 3
Items: 
Size: 337229 Color: 8
Size: 332665 Color: 5
Size: 330100 Color: 19

Bin 3253: 7 of cap free
Amount of items: 3
Items: 
Size: 376963 Color: 7
Size: 348112 Color: 12
Size: 274919 Color: 5

Bin 3254: 7 of cap free
Amount of items: 3
Items: 
Size: 354020 Color: 7
Size: 349146 Color: 2
Size: 296828 Color: 13

Bin 3255: 7 of cap free
Amount of items: 3
Items: 
Size: 348860 Color: 18
Size: 348553 Color: 17
Size: 302581 Color: 7

Bin 3256: 7 of cap free
Amount of items: 3
Items: 
Size: 346127 Color: 4
Size: 335595 Color: 13
Size: 318272 Color: 15

Bin 3257: 8 of cap free
Amount of items: 3
Items: 
Size: 349464 Color: 14
Size: 339335 Color: 8
Size: 311194 Color: 9

Bin 3258: 8 of cap free
Amount of items: 3
Items: 
Size: 381409 Color: 8
Size: 338536 Color: 15
Size: 280048 Color: 13

Bin 3259: 8 of cap free
Amount of items: 3
Items: 
Size: 371075 Color: 16
Size: 367695 Color: 15
Size: 261223 Color: 16

Bin 3260: 9 of cap free
Amount of items: 3
Items: 
Size: 379688 Color: 7
Size: 356414 Color: 6
Size: 263890 Color: 17

Bin 3261: 9 of cap free
Amount of items: 3
Items: 
Size: 337245 Color: 17
Size: 332110 Color: 9
Size: 330637 Color: 11

Bin 3262: 10 of cap free
Amount of items: 3
Items: 
Size: 340096 Color: 17
Size: 339897 Color: 3
Size: 319998 Color: 8

Bin 3263: 11 of cap free
Amount of items: 3
Items: 
Size: 399642 Color: 9
Size: 345222 Color: 15
Size: 255126 Color: 6

Bin 3264: 12 of cap free
Amount of items: 3
Items: 
Size: 394660 Color: 16
Size: 349763 Color: 17
Size: 255566 Color: 0

Bin 3265: 12 of cap free
Amount of items: 3
Items: 
Size: 345677 Color: 1
Size: 340196 Color: 15
Size: 314116 Color: 18

Bin 3266: 14 of cap free
Amount of items: 3
Items: 
Size: 364256 Color: 3
Size: 360770 Color: 9
Size: 274961 Color: 5

Bin 3267: 14 of cap free
Amount of items: 3
Items: 
Size: 377026 Color: 15
Size: 357092 Color: 5
Size: 265869 Color: 3

Bin 3268: 16 of cap free
Amount of items: 3
Items: 
Size: 347919 Color: 14
Size: 326117 Color: 19
Size: 325949 Color: 0

Bin 3269: 18 of cap free
Amount of items: 3
Items: 
Size: 350948 Color: 17
Size: 350752 Color: 1
Size: 298283 Color: 3

Bin 3270: 19 of cap free
Amount of items: 3
Items: 
Size: 391545 Color: 13
Size: 356705 Color: 0
Size: 251732 Color: 14

Bin 3271: 24 of cap free
Amount of items: 3
Items: 
Size: 385829 Color: 5
Size: 352819 Color: 19
Size: 261329 Color: 1

Bin 3272: 24 of cap free
Amount of items: 3
Items: 
Size: 358049 Color: 2
Size: 325759 Color: 16
Size: 316169 Color: 14

Bin 3273: 25 of cap free
Amount of items: 3
Items: 
Size: 339425 Color: 19
Size: 332343 Color: 19
Size: 328208 Color: 5

Bin 3274: 27 of cap free
Amount of items: 3
Items: 
Size: 371243 Color: 2
Size: 359520 Color: 6
Size: 269211 Color: 12

Bin 3275: 28 of cap free
Amount of items: 3
Items: 
Size: 360015 Color: 11
Size: 355282 Color: 12
Size: 284676 Color: 12

Bin 3276: 31 of cap free
Amount of items: 3
Items: 
Size: 363917 Color: 2
Size: 327131 Color: 17
Size: 308922 Color: 2

Bin 3277: 32 of cap free
Amount of items: 3
Items: 
Size: 360692 Color: 10
Size: 360320 Color: 17
Size: 278957 Color: 3

Bin 3278: 37 of cap free
Amount of items: 3
Items: 
Size: 485849 Color: 7
Size: 262490 Color: 9
Size: 251625 Color: 18

Bin 3279: 38 of cap free
Amount of items: 3
Items: 
Size: 381658 Color: 11
Size: 366680 Color: 6
Size: 251625 Color: 16

Bin 3280: 39 of cap free
Amount of items: 3
Items: 
Size: 354585 Color: 14
Size: 324746 Color: 12
Size: 320631 Color: 19

Bin 3281: 39 of cap free
Amount of items: 3
Items: 
Size: 384568 Color: 7
Size: 360239 Color: 0
Size: 255155 Color: 0

Bin 3282: 39 of cap free
Amount of items: 3
Items: 
Size: 361094 Color: 13
Size: 360411 Color: 9
Size: 278457 Color: 16

Bin 3283: 39 of cap free
Amount of items: 3
Items: 
Size: 335392 Color: 18
Size: 334973 Color: 8
Size: 329597 Color: 15

Bin 3284: 44 of cap free
Amount of items: 3
Items: 
Size: 468425 Color: 17
Size: 268021 Color: 0
Size: 263511 Color: 5

Bin 3285: 48 of cap free
Amount of items: 3
Items: 
Size: 448890 Color: 11
Size: 295435 Color: 17
Size: 255628 Color: 19

Bin 3286: 49 of cap free
Amount of items: 3
Items: 
Size: 358991 Color: 1
Size: 341679 Color: 18
Size: 299282 Color: 13

Bin 3287: 49 of cap free
Amount of items: 3
Items: 
Size: 399261 Color: 9
Size: 343615 Color: 8
Size: 257076 Color: 4

Bin 3288: 52 of cap free
Amount of items: 3
Items: 
Size: 338395 Color: 11
Size: 334918 Color: 15
Size: 326636 Color: 2

Bin 3289: 59 of cap free
Amount of items: 3
Items: 
Size: 339101 Color: 10
Size: 333776 Color: 8
Size: 327065 Color: 16

Bin 3290: 61 of cap free
Amount of items: 3
Items: 
Size: 457297 Color: 3
Size: 274121 Color: 19
Size: 268522 Color: 10

Bin 3291: 62 of cap free
Amount of items: 3
Items: 
Size: 339392 Color: 10
Size: 335390 Color: 4
Size: 325157 Color: 5

Bin 3292: 65 of cap free
Amount of items: 3
Items: 
Size: 351902 Color: 19
Size: 329597 Color: 6
Size: 318437 Color: 0

Bin 3293: 68 of cap free
Amount of items: 3
Items: 
Size: 355939 Color: 4
Size: 355896 Color: 13
Size: 288098 Color: 10

Bin 3294: 70 of cap free
Amount of items: 3
Items: 
Size: 381131 Color: 10
Size: 351506 Color: 12
Size: 267294 Color: 14

Bin 3295: 72 of cap free
Amount of items: 3
Items: 
Size: 354249 Color: 14
Size: 344813 Color: 13
Size: 300867 Color: 14

Bin 3296: 75 of cap free
Amount of items: 3
Items: 
Size: 399852 Color: 19
Size: 348905 Color: 9
Size: 251169 Color: 0

Bin 3297: 75 of cap free
Amount of items: 3
Items: 
Size: 363064 Color: 15
Size: 328355 Color: 19
Size: 308507 Color: 10

Bin 3298: 85 of cap free
Amount of items: 3
Items: 
Size: 348568 Color: 18
Size: 348370 Color: 3
Size: 302978 Color: 3

Bin 3299: 97 of cap free
Amount of items: 2
Items: 
Size: 499960 Color: 13
Size: 499944 Color: 14

Bin 3300: 104 of cap free
Amount of items: 3
Items: 
Size: 358077 Color: 10
Size: 350741 Color: 15
Size: 291079 Color: 0

Bin 3301: 105 of cap free
Amount of items: 3
Items: 
Size: 358759 Color: 12
Size: 357795 Color: 15
Size: 283342 Color: 10

Bin 3302: 121 of cap free
Amount of items: 3
Items: 
Size: 347959 Color: 9
Size: 344807 Color: 13
Size: 307114 Color: 4

Bin 3303: 157 of cap free
Amount of items: 3
Items: 
Size: 424292 Color: 17
Size: 323845 Color: 15
Size: 251707 Color: 14

Bin 3304: 164 of cap free
Amount of items: 3
Items: 
Size: 340248 Color: 11
Size: 337823 Color: 3
Size: 321766 Color: 11

Bin 3305: 200 of cap free
Amount of items: 3
Items: 
Size: 377408 Color: 18
Size: 368846 Color: 2
Size: 253547 Color: 8

Bin 3306: 247 of cap free
Amount of items: 3
Items: 
Size: 367482 Color: 1
Size: 364875 Color: 17
Size: 267397 Color: 11

Bin 3307: 251 of cap free
Amount of items: 3
Items: 
Size: 414686 Color: 0
Size: 326304 Color: 0
Size: 258760 Color: 17

Bin 3308: 367 of cap free
Amount of items: 3
Items: 
Size: 349914 Color: 10
Size: 348199 Color: 7
Size: 301521 Color: 19

Bin 3309: 449 of cap free
Amount of items: 3
Items: 
Size: 347424 Color: 11
Size: 326276 Color: 5
Size: 325852 Color: 8

Bin 3310: 519 of cap free
Amount of items: 3
Items: 
Size: 346698 Color: 5
Size: 342875 Color: 3
Size: 309909 Color: 2

Bin 3311: 582 of cap free
Amount of items: 3
Items: 
Size: 358649 Color: 8
Size: 356199 Color: 11
Size: 284571 Color: 6

Bin 3312: 673 of cap free
Amount of items: 3
Items: 
Size: 366736 Color: 0
Size: 362126 Color: 12
Size: 270466 Color: 0

Bin 3313: 1036 of cap free
Amount of items: 3
Items: 
Size: 339811 Color: 18
Size: 329762 Color: 16
Size: 329392 Color: 14

Bin 3314: 1133 of cap free
Amount of items: 3
Items: 
Size: 367988 Color: 5
Size: 361720 Color: 15
Size: 269160 Color: 7

Bin 3315: 1329 of cap free
Amount of items: 2
Items: 
Size: 499346 Color: 2
Size: 499326 Color: 8

Bin 3316: 1595 of cap free
Amount of items: 2
Items: 
Size: 499212 Color: 18
Size: 499194 Color: 7

Bin 3317: 1819 of cap free
Amount of items: 2
Items: 
Size: 499158 Color: 9
Size: 499024 Color: 18

Bin 3318: 3911 of cap free
Amount of items: 3
Items: 
Size: 361703 Color: 17
Size: 359365 Color: 7
Size: 275022 Color: 8

Bin 3319: 11219 of cap free
Amount of items: 3
Items: 
Size: 448171 Color: 8
Size: 271503 Color: 6
Size: 269108 Color: 14

Bin 3320: 16708 of cap free
Amount of items: 3
Items: 
Size: 358566 Color: 0
Size: 357940 Color: 18
Size: 266787 Color: 15

Bin 3321: 23472 of cap free
Amount of items: 3
Items: 
Size: 355202 Color: 14
Size: 355034 Color: 15
Size: 266293 Color: 15

Bin 3322: 26725 of cap free
Amount of items: 3
Items: 
Size: 355023 Color: 6
Size: 354908 Color: 10
Size: 263345 Color: 8

Bin 3323: 28954 of cap free
Amount of items: 3
Items: 
Size: 354841 Color: 15
Size: 353738 Color: 7
Size: 262468 Color: 2

Bin 3324: 31870 of cap free
Amount of items: 3
Items: 
Size: 353512 Color: 1
Size: 353305 Color: 17
Size: 261314 Color: 1

Bin 3325: 32865 of cap free
Amount of items: 3
Items: 
Size: 353196 Color: 16
Size: 352654 Color: 15
Size: 261286 Color: 16

Bin 3326: 34263 of cap free
Amount of items: 3
Items: 
Size: 352429 Color: 18
Size: 352342 Color: 17
Size: 260967 Color: 17

Bin 3327: 36295 of cap free
Amount of items: 3
Items: 
Size: 351810 Color: 0
Size: 351090 Color: 8
Size: 260806 Color: 13

Bin 3328: 39153 of cap free
Amount of items: 3
Items: 
Size: 350635 Color: 11
Size: 349875 Color: 19
Size: 260338 Color: 6

Bin 3329: 49221 of cap free
Amount of items: 3
Items: 
Size: 348154 Color: 6
Size: 343894 Color: 2
Size: 258732 Color: 2

Bin 3330: 54175 of cap free
Amount of items: 3
Items: 
Size: 343720 Color: 1
Size: 343612 Color: 11
Size: 258494 Color: 4

Bin 3331: 58481 of cap free
Amount of items: 3
Items: 
Size: 342858 Color: 14
Size: 342683 Color: 10
Size: 255979 Color: 10

Bin 3332: 77829 of cap free
Amount of items: 3
Items: 
Size: 338173 Color: 19
Size: 328148 Color: 12
Size: 255851 Color: 6

Bin 3333: 234622 of cap free
Amount of items: 3
Items: 
Size: 255726 Color: 7
Size: 254864 Color: 9
Size: 254789 Color: 6

Bin 3334: 238492 of cap free
Amount of items: 3
Items: 
Size: 254134 Color: 17
Size: 253987 Color: 0
Size: 253388 Color: 3

Bin 3335: 241444 of cap free
Amount of items: 3
Items: 
Size: 253219 Color: 19
Size: 252838 Color: 17
Size: 252500 Color: 11

Bin 3336: 747620 of cap free
Amount of items: 1
Items: 
Size: 252381 Color: 18

Total size: 3334003334
Total free space: 2000002

