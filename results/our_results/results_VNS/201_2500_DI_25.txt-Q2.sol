Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1220 Color: 1
Size: 870 Color: 1
Size: 176 Color: 0
Size: 148 Color: 0
Size: 50 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 1
Size: 871 Color: 1
Size: 176 Color: 0
Size: 140 Color: 0
Size: 44 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 1
Size: 810 Color: 0
Size: 52 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 1
Size: 542 Color: 1
Size: 104 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 1
Size: 481 Color: 0
Size: 96 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 1
Size: 478 Color: 0
Size: 92 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1972 Color: 1
Size: 368 Color: 0
Size: 124 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 1
Size: 338 Color: 0
Size: 108 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 0
Size: 262 Color: 0
Size: 168 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 0
Size: 287 Color: 1
Size: 56 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2135 Color: 1
Size: 269 Color: 0
Size: 60 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2169 Color: 1
Size: 267 Color: 0
Size: 28 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2193 Color: 0
Size: 209 Color: 0
Size: 62 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 0
Size: 1025 Color: 1
Size: 204 Color: 0

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 1
Size: 1062 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 0
Size: 887 Color: 1
Size: 82 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 1
Size: 621 Color: 1
Size: 68 Color: 0

Bin 18: 1 of cap free
Amount of items: 5
Items: 
Size: 1815 Color: 1
Size: 250 Color: 0
Size: 214 Color: 0
Size: 112 Color: 0
Size: 72 Color: 1

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 1
Size: 502 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 1
Size: 421 Color: 0
Size: 48 Color: 0

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 2050 Color: 1
Size: 413 Color: 0

Bin 22: 1 of cap free
Amount of items: 4
Items: 
Size: 2062 Color: 0
Size: 331 Color: 1
Size: 54 Color: 1
Size: 16 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 0
Size: 221 Color: 1
Size: 48 Color: 1

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 0
Size: 840 Color: 0
Size: 68 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 0
Size: 715 Color: 1
Size: 84 Color: 0

Bin 26: 2 of cap free
Amount of items: 4
Items: 
Size: 1966 Color: 0
Size: 418 Color: 1
Size: 46 Color: 1
Size: 32 Color: 0

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 2122 Color: 0
Size: 340 Color: 1

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 2201 Color: 0
Size: 261 Color: 1

Bin 29: 2 of cap free
Amount of items: 4
Items: 
Size: 2210 Color: 0
Size: 226 Color: 1
Size: 22 Color: 0
Size: 4 Color: 1

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 1
Size: 662 Color: 1
Size: 42 Color: 0

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 2087 Color: 0
Size: 374 Color: 1

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 2138 Color: 1
Size: 301 Color: 0
Size: 22 Color: 0

Bin 33: 4 of cap free
Amount of items: 4
Items: 
Size: 1403 Color: 1
Size: 771 Color: 0
Size: 204 Color: 0
Size: 82 Color: 1

Bin 34: 4 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 1
Size: 383 Color: 0
Size: 32 Color: 1

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 2098 Color: 0
Size: 362 Color: 1

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 0
Size: 578 Color: 1
Size: 274 Color: 1

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1837 Color: 1
Size: 622 Color: 0

Bin 38: 6 of cap free
Amount of items: 2
Items: 
Size: 2143 Color: 0
Size: 315 Color: 1

Bin 39: 6 of cap free
Amount of items: 2
Items: 
Size: 2183 Color: 0
Size: 275 Color: 1

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1866 Color: 1
Size: 591 Color: 0

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 0
Size: 523 Color: 1

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 0
Size: 351 Color: 1

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 0
Size: 304 Color: 1

Bin 44: 8 of cap free
Amount of items: 2
Items: 
Size: 2150 Color: 1
Size: 306 Color: 0

Bin 45: 9 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 1
Size: 912 Color: 0

Bin 46: 9 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 669 Color: 1
Size: 68 Color: 0

Bin 47: 10 of cap free
Amount of items: 2
Items: 
Size: 2067 Color: 0
Size: 387 Color: 1

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 1
Size: 286 Color: 0

Bin 49: 14 of cap free
Amount of items: 2
Items: 
Size: 2215 Color: 0
Size: 235 Color: 1

Bin 50: 15 of cap free
Amount of items: 2
Items: 
Size: 2024 Color: 0
Size: 425 Color: 1

Bin 51: 15 of cap free
Amount of items: 2
Items: 
Size: 2055 Color: 1
Size: 394 Color: 0

Bin 52: 15 of cap free
Amount of items: 2
Items: 
Size: 2103 Color: 1
Size: 346 Color: 0

Bin 53: 17 of cap free
Amount of items: 2
Items: 
Size: 2005 Color: 1
Size: 442 Color: 0

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1905 Color: 1
Size: 541 Color: 0

Bin 55: 19 of cap free
Amount of items: 20
Items: 
Size: 273 Color: 1
Size: 172 Color: 0
Size: 152 Color: 0
Size: 148 Color: 0
Size: 142 Color: 0
Size: 140 Color: 1
Size: 132 Color: 0
Size: 128 Color: 1
Size: 128 Color: 1
Size: 122 Color: 0
Size: 116 Color: 1
Size: 116 Color: 1
Size: 104 Color: 1
Size: 96 Color: 0
Size: 92 Color: 0
Size: 88 Color: 0
Size: 80 Color: 1
Size: 76 Color: 1
Size: 76 Color: 1
Size: 64 Color: 0

Bin 56: 21 of cap free
Amount of items: 4
Items: 
Size: 1235 Color: 1
Size: 942 Color: 1
Size: 200 Color: 0
Size: 66 Color: 0

Bin 57: 21 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 1
Size: 722 Color: 0

Bin 58: 23 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 0
Size: 302 Color: 1
Size: 184 Color: 1

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 1338 Color: 1
Size: 1102 Color: 0

Bin 60: 27 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 0
Size: 1027 Color: 1
Size: 160 Color: 1

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 1
Size: 1014 Color: 0

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 0
Size: 762 Color: 1

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1969 Color: 0
Size: 467 Color: 1

Bin 64: 30 of cap free
Amount of items: 2
Items: 
Size: 1354 Color: 0
Size: 1080 Color: 1

Bin 65: 38 of cap free
Amount of items: 2
Items: 
Size: 1541 Color: 1
Size: 885 Color: 0

Bin 66: 1942 of cap free
Amount of items: 10
Items: 
Size: 72 Color: 1
Size: 60 Color: 0
Size: 56 Color: 1
Size: 56 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0
Size: 50 Color: 0
Size: 44 Color: 1
Size: 40 Color: 1
Size: 40 Color: 1

Total size: 160160
Total free space: 2464

