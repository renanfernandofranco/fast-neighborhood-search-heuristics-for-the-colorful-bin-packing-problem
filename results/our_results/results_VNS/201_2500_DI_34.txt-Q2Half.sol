Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 522 Color: 1
Size: 236 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 1
Size: 606 Color: 1
Size: 60 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 1
Size: 620 Color: 1
Size: 40 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 1
Size: 192 Color: 0
Size: 70 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1493 Color: 1
Size: 601 Color: 1
Size: 204 Color: 0
Size: 174 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 1
Size: 860 Color: 1
Size: 40 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 1
Size: 382 Color: 1
Size: 72 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 1
Size: 543 Color: 1
Size: 108 Color: 0

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 1876 Color: 1
Size: 372 Color: 1
Size: 120 Color: 0
Size: 104 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 1
Size: 230 Color: 1
Size: 44 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 1
Size: 284 Color: 1
Size: 80 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 1
Size: 469 Color: 1
Size: 92 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 667 Color: 1
Size: 132 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 1
Size: 234 Color: 1
Size: 44 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 1
Size: 340 Color: 1
Size: 152 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 1
Size: 314 Color: 1
Size: 136 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 1
Size: 204 Color: 1
Size: 56 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1926 Color: 1
Size: 466 Color: 1
Size: 80 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 1030 Color: 1
Size: 204 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 1
Size: 200 Color: 1
Size: 76 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 1
Size: 988 Color: 1
Size: 40 Color: 0

Bin 22: 0 of cap free
Amount of items: 5
Items: 
Size: 1244 Color: 1
Size: 634 Color: 1
Size: 378 Color: 1
Size: 120 Color: 0
Size: 96 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 1
Size: 443 Color: 1
Size: 88 Color: 0

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 1974 Color: 1
Size: 282 Color: 1
Size: 124 Color: 0
Size: 92 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 1
Size: 846 Color: 1
Size: 168 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 1000 Color: 1
Size: 54 Color: 0

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 1704 Color: 1
Size: 494 Color: 1
Size: 162 Color: 0
Size: 112 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 1
Size: 294 Color: 1
Size: 120 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 308 Color: 1
Size: 96 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 1
Size: 228 Color: 1
Size: 104 Color: 0

Bin 31: 0 of cap free
Amount of items: 5
Items: 
Size: 1239 Color: 1
Size: 619 Color: 1
Size: 418 Color: 1
Size: 108 Color: 0
Size: 88 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1986 Color: 1
Size: 478 Color: 1
Size: 8 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 1
Size: 882 Color: 1
Size: 32 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 1
Size: 529 Color: 1
Size: 104 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 1
Size: 734 Color: 1
Size: 144 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 1
Size: 558 Color: 1
Size: 168 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 1638 Color: 1
Size: 452 Color: 1
Size: 242 Color: 1
Size: 96 Color: 0
Size: 44 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1676 Color: 1
Size: 700 Color: 1
Size: 96 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 1
Size: 504 Color: 1
Size: 122 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 993 Color: 1
Size: 242 Color: 0

Bin 41: 0 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 1
Size: 840 Color: 1
Size: 64 Color: 0
Size: 16 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 1
Size: 576 Color: 1
Size: 272 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 1
Size: 698 Color: 1
Size: 220 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 1
Size: 1022 Color: 1
Size: 28 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 1
Size: 709 Color: 1
Size: 56 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 1
Size: 1018 Color: 1
Size: 44 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 1
Size: 409 Color: 1
Size: 144 Color: 0

Bin 48: 1 of cap free
Amount of items: 5
Items: 
Size: 1254 Color: 1
Size: 1029 Color: 1
Size: 172 Color: 1
Size: 8 Color: 0
Size: 8 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 1
Size: 204 Color: 1
Size: 80 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 370 Color: 1
Size: 72 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1751 Color: 1
Size: 447 Color: 1
Size: 272 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 1
Size: 500 Color: 1
Size: 88 Color: 0

Bin 53: 3 of cap free
Amount of items: 5
Items: 
Size: 802 Color: 1
Size: 767 Color: 1
Size: 756 Color: 1
Size: 80 Color: 0
Size: 64 Color: 0

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 1
Size: 499 Color: 1
Size: 72 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 1
Size: 412 Color: 1
Size: 124 Color: 0

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 1292 Color: 1
Size: 1140 Color: 1
Size: 36 Color: 0

Bin 57: 8 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 1
Size: 877 Color: 1
Size: 556 Color: 0

Bin 58: 10 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 1
Size: 642 Color: 1
Size: 88 Color: 0

Bin 59: 25 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 1
Size: 817 Color: 1
Size: 384 Color: 0

Bin 60: 91 of cap free
Amount of items: 4
Items: 
Size: 1731 Color: 1
Size: 462 Color: 1
Size: 140 Color: 0
Size: 48 Color: 0

Bin 61: 268 of cap free
Amount of items: 1
Items: 
Size: 2204 Color: 1

Bin 62: 326 of cap free
Amount of items: 1
Items: 
Size: 2146 Color: 1

Bin 63: 350 of cap free
Amount of items: 1
Items: 
Size: 2122 Color: 1

Bin 64: 374 of cap free
Amount of items: 1
Items: 
Size: 2098 Color: 1

Bin 65: 489 of cap free
Amount of items: 1
Items: 
Size: 1983 Color: 1

Bin 66: 501 of cap free
Amount of items: 1
Items: 
Size: 1971 Color: 1

Total size: 160680
Total free space: 2472

