Capicity Bin: 1000001
Lower Bound: 920

Bins used: 925
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 687051 Color: 1
Size: 192926 Color: 0
Size: 120024 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 686349 Color: 1
Size: 179051 Color: 0
Size: 134601 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 693136 Color: 1
Size: 164194 Color: 0
Size: 142671 Color: 1

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 692987 Color: 1
Size: 102997 Color: 0
Size: 102784 Color: 0
Size: 101233 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 748886 Color: 1
Size: 128583 Color: 0
Size: 122532 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 579662 Color: 0
Size: 245100 Color: 0
Size: 175239 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 768191 Color: 0
Size: 119301 Color: 1
Size: 112509 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 649313 Color: 1
Size: 245524 Color: 0
Size: 105164 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 760451 Color: 0
Size: 132302 Color: 1
Size: 107248 Color: 0

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 533519 Color: 1
Size: 466482 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 651468 Color: 1
Size: 175722 Color: 0
Size: 172811 Color: 1

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 668102 Color: 0
Size: 331899 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 680642 Color: 1
Size: 168368 Color: 1
Size: 150991 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 687298 Color: 1
Size: 160236 Color: 0
Size: 152467 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 697131 Color: 1
Size: 159197 Color: 0
Size: 143673 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 701714 Color: 1
Size: 151604 Color: 1
Size: 146683 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 740132 Color: 0
Size: 133679 Color: 1
Size: 126190 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 786694 Color: 0
Size: 108138 Color: 1
Size: 105169 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 645643 Color: 1
Size: 184038 Color: 0
Size: 170319 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 776692 Color: 0
Size: 122585 Color: 0
Size: 100723 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 764542 Color: 1
Size: 129309 Color: 0
Size: 106149 Color: 0

Bin 22: 1 of cap free
Amount of items: 4
Items: 
Size: 451947 Color: 1
Size: 197248 Color: 0
Size: 183885 Color: 1
Size: 166920 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 739859 Color: 1
Size: 133214 Color: 0
Size: 126927 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 655211 Color: 0
Size: 174656 Color: 1
Size: 170133 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 619529 Color: 0
Size: 194485 Color: 1
Size: 185986 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 746765 Color: 0
Size: 144084 Color: 1
Size: 109151 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 729430 Color: 0
Size: 163490 Color: 1
Size: 107080 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 764875 Color: 0
Size: 132863 Color: 1
Size: 102262 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 555529 Color: 1
Size: 269366 Color: 0
Size: 175105 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 450263 Color: 1
Size: 375474 Color: 0
Size: 174263 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 356821 Color: 1
Size: 322321 Color: 1
Size: 320857 Color: 0

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 602153 Color: 1
Size: 397846 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 785878 Color: 1
Size: 108683 Color: 1
Size: 105438 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 723192 Color: 0
Size: 139437 Color: 0
Size: 137370 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 697527 Color: 0
Size: 174455 Color: 1
Size: 128017 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 645751 Color: 1
Size: 232727 Color: 0
Size: 121521 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 700911 Color: 0
Size: 149944 Color: 1
Size: 149144 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 693350 Color: 1
Size: 198491 Color: 0
Size: 108158 Color: 0

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 618300 Color: 1
Size: 192743 Color: 0
Size: 188955 Color: 1

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 770293 Color: 1
Size: 119224 Color: 0
Size: 110481 Color: 1

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 760498 Color: 0
Size: 131209 Color: 1
Size: 108291 Color: 1

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 559912 Color: 0
Size: 268820 Color: 0
Size: 171266 Color: 1

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 652886 Color: 1
Size: 183514 Color: 0
Size: 163597 Color: 0

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 787261 Color: 1
Size: 110494 Color: 1
Size: 102242 Color: 0

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 700233 Color: 0
Size: 199073 Color: 0
Size: 100691 Color: 1

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 612945 Color: 0
Size: 196039 Color: 1
Size: 191013 Color: 1

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 716954 Color: 0
Size: 164006 Color: 0
Size: 119037 Color: 1

Bin 48: 4 of cap free
Amount of items: 3
Items: 
Size: 777711 Color: 0
Size: 111897 Color: 1
Size: 110389 Color: 1

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 768322 Color: 1
Size: 123151 Color: 1
Size: 108524 Color: 0

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 552148 Color: 0
Size: 447848 Color: 1

Bin 51: 5 of cap free
Amount of items: 3
Items: 
Size: 656701 Color: 1
Size: 174265 Color: 1
Size: 169030 Color: 0

Bin 52: 5 of cap free
Amount of items: 3
Items: 
Size: 740758 Color: 0
Size: 136003 Color: 1
Size: 123235 Color: 1

Bin 53: 5 of cap free
Amount of items: 3
Items: 
Size: 724621 Color: 1
Size: 139940 Color: 0
Size: 135435 Color: 0

Bin 54: 5 of cap free
Amount of items: 3
Items: 
Size: 586100 Color: 1
Size: 276136 Color: 0
Size: 137760 Color: 0

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 736632 Color: 0
Size: 141989 Color: 1
Size: 121375 Color: 1

Bin 56: 6 of cap free
Amount of items: 3
Items: 
Size: 772229 Color: 1
Size: 122036 Color: 0
Size: 105730 Color: 1

Bin 57: 6 of cap free
Amount of items: 3
Items: 
Size: 791853 Color: 0
Size: 105506 Color: 1
Size: 102636 Color: 1

Bin 58: 6 of cap free
Amount of items: 3
Items: 
Size: 480782 Color: 1
Size: 377783 Color: 0
Size: 141430 Color: 0

Bin 59: 7 of cap free
Amount of items: 3
Items: 
Size: 571170 Color: 1
Size: 272490 Color: 1
Size: 156334 Color: 0

Bin 60: 7 of cap free
Amount of items: 2
Items: 
Size: 763312 Color: 0
Size: 236682 Color: 1

Bin 61: 7 of cap free
Amount of items: 3
Items: 
Size: 720835 Color: 0
Size: 149654 Color: 1
Size: 129505 Color: 0

Bin 62: 7 of cap free
Amount of items: 3
Items: 
Size: 521299 Color: 1
Size: 291315 Color: 0
Size: 187380 Color: 0

Bin 63: 8 of cap free
Amount of items: 3
Items: 
Size: 689184 Color: 1
Size: 159794 Color: 1
Size: 151015 Color: 0

Bin 64: 9 of cap free
Amount of items: 3
Items: 
Size: 637967 Color: 1
Size: 183744 Color: 1
Size: 178281 Color: 0

Bin 65: 9 of cap free
Amount of items: 3
Items: 
Size: 661817 Color: 0
Size: 170103 Color: 1
Size: 168072 Color: 0

Bin 66: 9 of cap free
Amount of items: 3
Items: 
Size: 692225 Color: 0
Size: 165159 Color: 0
Size: 142608 Color: 1

Bin 67: 10 of cap free
Amount of items: 2
Items: 
Size: 748558 Color: 1
Size: 251433 Color: 0

Bin 68: 10 of cap free
Amount of items: 3
Items: 
Size: 657727 Color: 1
Size: 177681 Color: 1
Size: 164583 Color: 0

Bin 69: 11 of cap free
Amount of items: 2
Items: 
Size: 639266 Color: 0
Size: 360724 Color: 1

Bin 70: 11 of cap free
Amount of items: 3
Items: 
Size: 546006 Color: 1
Size: 311177 Color: 1
Size: 142807 Color: 0

Bin 71: 12 of cap free
Amount of items: 2
Items: 
Size: 649298 Color: 1
Size: 350691 Color: 0

Bin 72: 12 of cap free
Amount of items: 2
Items: 
Size: 768630 Color: 1
Size: 231359 Color: 0

Bin 73: 12 of cap free
Amount of items: 3
Items: 
Size: 733192 Color: 0
Size: 143434 Color: 0
Size: 123363 Color: 1

Bin 74: 12 of cap free
Amount of items: 3
Items: 
Size: 481756 Color: 1
Size: 388612 Color: 0
Size: 129621 Color: 1

Bin 75: 14 of cap free
Amount of items: 3
Items: 
Size: 661612 Color: 0
Size: 184639 Color: 1
Size: 153736 Color: 1

Bin 76: 14 of cap free
Amount of items: 3
Items: 
Size: 553073 Color: 1
Size: 275644 Color: 0
Size: 171270 Color: 1

Bin 77: 14 of cap free
Amount of items: 2
Items: 
Size: 558180 Color: 1
Size: 441807 Color: 0

Bin 78: 14 of cap free
Amount of items: 2
Items: 
Size: 619709 Color: 0
Size: 380278 Color: 1

Bin 79: 14 of cap free
Amount of items: 3
Items: 
Size: 652352 Color: 1
Size: 185114 Color: 1
Size: 162521 Color: 0

Bin 80: 14 of cap free
Amount of items: 2
Items: 
Size: 730685 Color: 1
Size: 269302 Color: 0

Bin 81: 14 of cap free
Amount of items: 3
Items: 
Size: 785905 Color: 1
Size: 111862 Color: 0
Size: 102220 Color: 0

Bin 82: 15 of cap free
Amount of items: 3
Items: 
Size: 657405 Color: 0
Size: 183470 Color: 1
Size: 159111 Color: 0

Bin 83: 15 of cap free
Amount of items: 3
Items: 
Size: 711121 Color: 0
Size: 157586 Color: 1
Size: 131279 Color: 0

Bin 84: 15 of cap free
Amount of items: 3
Items: 
Size: 751283 Color: 0
Size: 133845 Color: 0
Size: 114858 Color: 1

Bin 85: 15 of cap free
Amount of items: 3
Items: 
Size: 375418 Color: 0
Size: 315495 Color: 0
Size: 309073 Color: 1

Bin 86: 15 of cap free
Amount of items: 3
Items: 
Size: 579284 Color: 1
Size: 277202 Color: 1
Size: 143500 Color: 0

Bin 87: 16 of cap free
Amount of items: 3
Items: 
Size: 619508 Color: 0
Size: 198347 Color: 1
Size: 182130 Color: 0

Bin 88: 16 of cap free
Amount of items: 3
Items: 
Size: 649812 Color: 1
Size: 177002 Color: 1
Size: 173171 Color: 0

Bin 89: 16 of cap free
Amount of items: 3
Items: 
Size: 656618 Color: 1
Size: 180347 Color: 0
Size: 163020 Color: 0

Bin 90: 18 of cap free
Amount of items: 3
Items: 
Size: 655418 Color: 1
Size: 191702 Color: 1
Size: 152863 Color: 0

Bin 91: 18 of cap free
Amount of items: 2
Items: 
Size: 655682 Color: 1
Size: 344301 Color: 0

Bin 92: 18 of cap free
Amount of items: 3
Items: 
Size: 722321 Color: 1
Size: 142609 Color: 0
Size: 135053 Color: 1

Bin 93: 18 of cap free
Amount of items: 2
Items: 
Size: 765638 Color: 1
Size: 234345 Color: 0

Bin 94: 19 of cap free
Amount of items: 2
Items: 
Size: 592637 Color: 1
Size: 407345 Color: 0

Bin 95: 21 of cap free
Amount of items: 2
Items: 
Size: 513542 Color: 0
Size: 486438 Color: 1

Bin 96: 21 of cap free
Amount of items: 2
Items: 
Size: 527184 Color: 0
Size: 472796 Color: 1

Bin 97: 21 of cap free
Amount of items: 2
Items: 
Size: 584987 Color: 0
Size: 414993 Color: 1

Bin 98: 21 of cap free
Amount of items: 3
Items: 
Size: 755174 Color: 0
Size: 138070 Color: 0
Size: 106736 Color: 1

Bin 99: 22 of cap free
Amount of items: 2
Items: 
Size: 675719 Color: 0
Size: 324260 Color: 1

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 762911 Color: 0
Size: 237068 Color: 1

Bin 101: 22 of cap free
Amount of items: 3
Items: 
Size: 347769 Color: 1
Size: 326620 Color: 0
Size: 325590 Color: 1

Bin 102: 23 of cap free
Amount of items: 2
Items: 
Size: 562553 Color: 1
Size: 437425 Color: 0

Bin 103: 23 of cap free
Amount of items: 2
Items: 
Size: 678653 Color: 0
Size: 321325 Color: 1

Bin 104: 23 of cap free
Amount of items: 2
Items: 
Size: 785794 Color: 1
Size: 214184 Color: 0

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 798260 Color: 0
Size: 201717 Color: 1

Bin 106: 24 of cap free
Amount of items: 3
Items: 
Size: 698138 Color: 0
Size: 179184 Color: 1
Size: 122655 Color: 1

Bin 107: 25 of cap free
Amount of items: 2
Items: 
Size: 586015 Color: 0
Size: 413961 Color: 1

Bin 108: 25 of cap free
Amount of items: 2
Items: 
Size: 601956 Color: 0
Size: 398020 Color: 1

Bin 109: 25 of cap free
Amount of items: 2
Items: 
Size: 626618 Color: 0
Size: 373358 Color: 1

Bin 110: 25 of cap free
Amount of items: 2
Items: 
Size: 764177 Color: 0
Size: 235799 Color: 1

Bin 111: 25 of cap free
Amount of items: 3
Items: 
Size: 359843 Color: 0
Size: 321313 Color: 0
Size: 318820 Color: 1

Bin 112: 26 of cap free
Amount of items: 2
Items: 
Size: 642481 Color: 0
Size: 357494 Color: 1

Bin 113: 26 of cap free
Amount of items: 3
Items: 
Size: 685724 Color: 1
Size: 162010 Color: 1
Size: 152241 Color: 0

Bin 114: 27 of cap free
Amount of items: 2
Items: 
Size: 513816 Color: 1
Size: 486158 Color: 0

Bin 115: 27 of cap free
Amount of items: 3
Items: 
Size: 777695 Color: 1
Size: 119094 Color: 1
Size: 103185 Color: 0

Bin 116: 28 of cap free
Amount of items: 3
Items: 
Size: 556139 Color: 1
Size: 272208 Color: 1
Size: 171626 Color: 0

Bin 117: 28 of cap free
Amount of items: 3
Items: 
Size: 750583 Color: 0
Size: 144444 Color: 1
Size: 104946 Color: 0

Bin 118: 29 of cap free
Amount of items: 2
Items: 
Size: 660788 Color: 1
Size: 339184 Color: 0

Bin 119: 30 of cap free
Amount of items: 2
Items: 
Size: 527656 Color: 0
Size: 472315 Color: 1

Bin 120: 30 of cap free
Amount of items: 3
Items: 
Size: 650209 Color: 0
Size: 190629 Color: 1
Size: 159133 Color: 0

Bin 121: 30 of cap free
Amount of items: 2
Items: 
Size: 672106 Color: 1
Size: 327865 Color: 0

Bin 122: 30 of cap free
Amount of items: 2
Items: 
Size: 750740 Color: 0
Size: 249231 Color: 1

Bin 123: 30 of cap free
Amount of items: 3
Items: 
Size: 554222 Color: 1
Size: 293440 Color: 0
Size: 152309 Color: 1

Bin 124: 30 of cap free
Amount of items: 3
Items: 
Size: 354259 Color: 0
Size: 342587 Color: 1
Size: 303125 Color: 0

Bin 125: 31 of cap free
Amount of items: 2
Items: 
Size: 634679 Color: 0
Size: 365291 Color: 1

Bin 126: 31 of cap free
Amount of items: 2
Items: 
Size: 732381 Color: 1
Size: 267589 Color: 0

Bin 127: 31 of cap free
Amount of items: 3
Items: 
Size: 753658 Color: 0
Size: 127494 Color: 0
Size: 118818 Color: 1

Bin 128: 31 of cap free
Amount of items: 3
Items: 
Size: 453250 Color: 1
Size: 375415 Color: 0
Size: 171305 Color: 1

Bin 129: 32 of cap free
Amount of items: 2
Items: 
Size: 678394 Color: 0
Size: 321575 Color: 1

Bin 130: 32 of cap free
Amount of items: 3
Items: 
Size: 734799 Color: 1
Size: 132944 Color: 0
Size: 132226 Color: 1

Bin 131: 32 of cap free
Amount of items: 3
Items: 
Size: 551316 Color: 1
Size: 295749 Color: 0
Size: 152904 Color: 0

Bin 132: 32 of cap free
Amount of items: 3
Items: 
Size: 699741 Color: 0
Size: 193192 Color: 1
Size: 107036 Color: 1

Bin 133: 33 of cap free
Amount of items: 3
Items: 
Size: 374017 Color: 0
Size: 348341 Color: 1
Size: 277610 Color: 1

Bin 134: 34 of cap free
Amount of items: 2
Items: 
Size: 557859 Color: 0
Size: 442108 Color: 1

Bin 135: 34 of cap free
Amount of items: 2
Items: 
Size: 562688 Color: 0
Size: 437279 Color: 1

Bin 136: 34 of cap free
Amount of items: 2
Items: 
Size: 709468 Color: 0
Size: 290499 Color: 1

Bin 137: 34 of cap free
Amount of items: 2
Items: 
Size: 764224 Color: 0
Size: 235743 Color: 1

Bin 138: 34 of cap free
Amount of items: 3
Items: 
Size: 562464 Color: 1
Size: 283088 Color: 1
Size: 154415 Color: 0

Bin 139: 35 of cap free
Amount of items: 2
Items: 
Size: 530602 Color: 0
Size: 469364 Color: 1

Bin 140: 35 of cap free
Amount of items: 2
Items: 
Size: 708160 Color: 1
Size: 291806 Color: 0

Bin 141: 36 of cap free
Amount of items: 2
Items: 
Size: 569402 Color: 0
Size: 430563 Color: 1

Bin 142: 38 of cap free
Amount of items: 3
Items: 
Size: 696167 Color: 1
Size: 166442 Color: 1
Size: 137354 Color: 0

Bin 143: 38 of cap free
Amount of items: 2
Items: 
Size: 771465 Color: 0
Size: 228498 Color: 1

Bin 144: 40 of cap free
Amount of items: 2
Items: 
Size: 512463 Color: 1
Size: 487498 Color: 0

Bin 145: 42 of cap free
Amount of items: 2
Items: 
Size: 693424 Color: 1
Size: 306535 Color: 0

Bin 146: 43 of cap free
Amount of items: 3
Items: 
Size: 706108 Color: 0
Size: 148257 Color: 1
Size: 145593 Color: 0

Bin 147: 44 of cap free
Amount of items: 2
Items: 
Size: 571892 Color: 0
Size: 428065 Color: 1

Bin 148: 44 of cap free
Amount of items: 2
Items: 
Size: 675041 Color: 0
Size: 324916 Color: 1

Bin 149: 45 of cap free
Amount of items: 2
Items: 
Size: 574778 Color: 0
Size: 425178 Color: 1

Bin 150: 46 of cap free
Amount of items: 2
Items: 
Size: 588768 Color: 1
Size: 411187 Color: 0

Bin 151: 47 of cap free
Amount of items: 2
Items: 
Size: 564332 Color: 1
Size: 435622 Color: 0

Bin 152: 47 of cap free
Amount of items: 3
Items: 
Size: 346292 Color: 1
Size: 331111 Color: 0
Size: 322551 Color: 1

Bin 153: 48 of cap free
Amount of items: 2
Items: 
Size: 536569 Color: 1
Size: 463384 Color: 0

Bin 154: 48 of cap free
Amount of items: 2
Items: 
Size: 674780 Color: 1
Size: 325173 Color: 0

Bin 155: 49 of cap free
Amount of items: 2
Items: 
Size: 718750 Color: 0
Size: 281202 Color: 1

Bin 156: 49 of cap free
Amount of items: 2
Items: 
Size: 776161 Color: 1
Size: 223791 Color: 0

Bin 157: 51 of cap free
Amount of items: 2
Items: 
Size: 616487 Color: 0
Size: 383463 Color: 1

Bin 158: 52 of cap free
Amount of items: 3
Items: 
Size: 754709 Color: 0
Size: 131653 Color: 1
Size: 113587 Color: 1

Bin 159: 54 of cap free
Amount of items: 3
Items: 
Size: 693528 Color: 1
Size: 171442 Color: 1
Size: 134977 Color: 0

Bin 160: 54 of cap free
Amount of items: 2
Items: 
Size: 598217 Color: 1
Size: 401730 Color: 0

Bin 161: 54 of cap free
Amount of items: 2
Items: 
Size: 622897 Color: 0
Size: 377050 Color: 1

Bin 162: 55 of cap free
Amount of items: 2
Items: 
Size: 631793 Color: 0
Size: 368153 Color: 1

Bin 163: 56 of cap free
Amount of items: 3
Items: 
Size: 375723 Color: 0
Size: 348206 Color: 1
Size: 276016 Color: 0

Bin 164: 57 of cap free
Amount of items: 2
Items: 
Size: 506594 Color: 0
Size: 493350 Color: 1

Bin 165: 58 of cap free
Amount of items: 2
Items: 
Size: 510509 Color: 1
Size: 489434 Color: 0

Bin 166: 58 of cap free
Amount of items: 2
Items: 
Size: 790310 Color: 0
Size: 209633 Color: 1

Bin 167: 60 of cap free
Amount of items: 2
Items: 
Size: 516827 Color: 0
Size: 483114 Color: 1

Bin 168: 60 of cap free
Amount of items: 2
Items: 
Size: 663075 Color: 1
Size: 336866 Color: 0

Bin 169: 61 of cap free
Amount of items: 2
Items: 
Size: 789682 Color: 1
Size: 210258 Color: 0

Bin 170: 63 of cap free
Amount of items: 3
Items: 
Size: 649093 Color: 1
Size: 185098 Color: 1
Size: 165747 Color: 0

Bin 171: 65 of cap free
Amount of items: 2
Items: 
Size: 697800 Color: 0
Size: 302136 Color: 1

Bin 172: 65 of cap free
Amount of items: 2
Items: 
Size: 550746 Color: 1
Size: 449190 Color: 0

Bin 173: 66 of cap free
Amount of items: 3
Items: 
Size: 649474 Color: 0
Size: 175472 Color: 0
Size: 174989 Color: 1

Bin 174: 67 of cap free
Amount of items: 3
Items: 
Size: 692521 Color: 0
Size: 163634 Color: 1
Size: 143779 Color: 1

Bin 175: 67 of cap free
Amount of items: 2
Items: 
Size: 526981 Color: 1
Size: 472953 Color: 0

Bin 176: 68 of cap free
Amount of items: 2
Items: 
Size: 596859 Color: 0
Size: 403074 Color: 1

Bin 177: 69 of cap free
Amount of items: 3
Items: 
Size: 669700 Color: 1
Size: 177164 Color: 1
Size: 153068 Color: 0

Bin 178: 70 of cap free
Amount of items: 2
Items: 
Size: 505188 Color: 0
Size: 494743 Color: 1

Bin 179: 70 of cap free
Amount of items: 3
Items: 
Size: 603190 Color: 1
Size: 270417 Color: 0
Size: 126324 Color: 1

Bin 180: 71 of cap free
Amount of items: 2
Items: 
Size: 585029 Color: 0
Size: 414901 Color: 1

Bin 181: 71 of cap free
Amount of items: 2
Items: 
Size: 632162 Color: 0
Size: 367768 Color: 1

Bin 182: 73 of cap free
Amount of items: 3
Items: 
Size: 357408 Color: 1
Size: 322305 Color: 0
Size: 320215 Color: 1

Bin 183: 74 of cap free
Amount of items: 2
Items: 
Size: 622514 Color: 1
Size: 377413 Color: 0

Bin 184: 74 of cap free
Amount of items: 2
Items: 
Size: 635144 Color: 0
Size: 364783 Color: 1

Bin 185: 75 of cap free
Amount of items: 2
Items: 
Size: 675085 Color: 1
Size: 324841 Color: 0

Bin 186: 75 of cap free
Amount of items: 3
Items: 
Size: 689518 Color: 1
Size: 168836 Color: 1
Size: 141572 Color: 0

Bin 187: 77 of cap free
Amount of items: 3
Items: 
Size: 343337 Color: 1
Size: 339075 Color: 1
Size: 317512 Color: 0

Bin 188: 80 of cap free
Amount of items: 2
Items: 
Size: 713658 Color: 0
Size: 286263 Color: 1

Bin 189: 80 of cap free
Amount of items: 2
Items: 
Size: 746203 Color: 1
Size: 253718 Color: 0

Bin 190: 80 of cap free
Amount of items: 3
Items: 
Size: 523170 Color: 1
Size: 298535 Color: 0
Size: 178216 Color: 0

Bin 191: 82 of cap free
Amount of items: 2
Items: 
Size: 750644 Color: 1
Size: 249275 Color: 0

Bin 192: 82 of cap free
Amount of items: 3
Items: 
Size: 737336 Color: 0
Size: 133927 Color: 1
Size: 128656 Color: 1

Bin 193: 83 of cap free
Amount of items: 2
Items: 
Size: 737933 Color: 0
Size: 261985 Color: 1

Bin 194: 84 of cap free
Amount of items: 2
Items: 
Size: 794605 Color: 1
Size: 205312 Color: 0

Bin 195: 84 of cap free
Amount of items: 3
Items: 
Size: 735588 Color: 0
Size: 148995 Color: 0
Size: 115334 Color: 1

Bin 196: 84 of cap free
Amount of items: 3
Items: 
Size: 709366 Color: 1
Size: 179513 Color: 0
Size: 111038 Color: 1

Bin 197: 85 of cap free
Amount of items: 2
Items: 
Size: 602895 Color: 1
Size: 397021 Color: 0

Bin 198: 85 of cap free
Amount of items: 2
Items: 
Size: 773597 Color: 0
Size: 226319 Color: 1

Bin 199: 86 of cap free
Amount of items: 2
Items: 
Size: 556482 Color: 0
Size: 443433 Color: 1

Bin 200: 86 of cap free
Amount of items: 2
Items: 
Size: 635574 Color: 0
Size: 364341 Color: 1

Bin 201: 89 of cap free
Amount of items: 2
Items: 
Size: 502089 Color: 1
Size: 497823 Color: 0

Bin 202: 89 of cap free
Amount of items: 2
Items: 
Size: 564156 Color: 1
Size: 435756 Color: 0

Bin 203: 90 of cap free
Amount of items: 2
Items: 
Size: 525835 Color: 0
Size: 474076 Color: 1

Bin 204: 91 of cap free
Amount of items: 3
Items: 
Size: 685411 Color: 0
Size: 161873 Color: 1
Size: 152626 Color: 1

Bin 205: 92 of cap free
Amount of items: 2
Items: 
Size: 512566 Color: 0
Size: 487343 Color: 1

Bin 206: 92 of cap free
Amount of items: 2
Items: 
Size: 665658 Color: 1
Size: 334251 Color: 0

Bin 207: 94 of cap free
Amount of items: 2
Items: 
Size: 587330 Color: 1
Size: 412577 Color: 0

Bin 208: 95 of cap free
Amount of items: 3
Items: 
Size: 771022 Color: 1
Size: 118084 Color: 1
Size: 110800 Color: 0

Bin 209: 96 of cap free
Amount of items: 3
Items: 
Size: 357567 Color: 1
Size: 321823 Color: 1
Size: 320515 Color: 0

Bin 210: 97 of cap free
Amount of items: 2
Items: 
Size: 533017 Color: 1
Size: 466887 Color: 0

Bin 211: 97 of cap free
Amount of items: 2
Items: 
Size: 759294 Color: 0
Size: 240610 Color: 1

Bin 212: 97 of cap free
Amount of items: 2
Items: 
Size: 794085 Color: 1
Size: 205819 Color: 0

Bin 213: 100 of cap free
Amount of items: 2
Items: 
Size: 780275 Color: 0
Size: 219626 Color: 1

Bin 214: 100 of cap free
Amount of items: 3
Items: 
Size: 613505 Color: 0
Size: 219671 Color: 0
Size: 166725 Color: 1

Bin 215: 103 of cap free
Amount of items: 2
Items: 
Size: 575955 Color: 0
Size: 423943 Color: 1

Bin 216: 105 of cap free
Amount of items: 2
Items: 
Size: 572789 Color: 0
Size: 427107 Color: 1

Bin 217: 105 of cap free
Amount of items: 2
Items: 
Size: 599924 Color: 0
Size: 399972 Color: 1

Bin 218: 105 of cap free
Amount of items: 2
Items: 
Size: 752318 Color: 0
Size: 247578 Color: 1

Bin 219: 106 of cap free
Amount of items: 2
Items: 
Size: 610489 Color: 0
Size: 389406 Color: 1

Bin 220: 107 of cap free
Amount of items: 2
Items: 
Size: 632275 Color: 0
Size: 367619 Color: 1

Bin 221: 107 of cap free
Amount of items: 2
Items: 
Size: 701547 Color: 0
Size: 298347 Color: 1

Bin 222: 108 of cap free
Amount of items: 2
Items: 
Size: 671108 Color: 1
Size: 328785 Color: 0

Bin 223: 109 of cap free
Amount of items: 2
Items: 
Size: 617518 Color: 0
Size: 382374 Color: 1

Bin 224: 109 of cap free
Amount of items: 3
Items: 
Size: 502646 Color: 1
Size: 320761 Color: 1
Size: 176485 Color: 0

Bin 225: 112 of cap free
Amount of items: 2
Items: 
Size: 621922 Color: 1
Size: 377967 Color: 0

Bin 226: 113 of cap free
Amount of items: 2
Items: 
Size: 664829 Color: 1
Size: 335059 Color: 0

Bin 227: 113 of cap free
Amount of items: 2
Items: 
Size: 766971 Color: 1
Size: 232917 Color: 0

Bin 228: 114 of cap free
Amount of items: 2
Items: 
Size: 564457 Color: 1
Size: 435430 Color: 0

Bin 229: 116 of cap free
Amount of items: 2
Items: 
Size: 721819 Color: 0
Size: 278066 Color: 1

Bin 230: 117 of cap free
Amount of items: 2
Items: 
Size: 763339 Color: 1
Size: 236545 Color: 0

Bin 231: 117 of cap free
Amount of items: 2
Items: 
Size: 764013 Color: 0
Size: 235871 Color: 1

Bin 232: 118 of cap free
Amount of items: 2
Items: 
Size: 531857 Color: 1
Size: 468026 Color: 0

Bin 233: 119 of cap free
Amount of items: 2
Items: 
Size: 634237 Color: 0
Size: 365645 Color: 1

Bin 234: 119 of cap free
Amount of items: 2
Items: 
Size: 643226 Color: 0
Size: 356656 Color: 1

Bin 235: 120 of cap free
Amount of items: 2
Items: 
Size: 575434 Color: 0
Size: 424447 Color: 1

Bin 236: 120 of cap free
Amount of items: 3
Items: 
Size: 768414 Color: 1
Size: 125179 Color: 1
Size: 106288 Color: 0

Bin 237: 121 of cap free
Amount of items: 2
Items: 
Size: 727991 Color: 0
Size: 271889 Color: 1

Bin 238: 121 of cap free
Amount of items: 2
Items: 
Size: 796817 Color: 0
Size: 203063 Color: 1

Bin 239: 122 of cap free
Amount of items: 2
Items: 
Size: 672958 Color: 0
Size: 326921 Color: 1

Bin 240: 122 of cap free
Amount of items: 2
Items: 
Size: 675646 Color: 1
Size: 324233 Color: 0

Bin 241: 122 of cap free
Amount of items: 2
Items: 
Size: 709515 Color: 0
Size: 290364 Color: 1

Bin 242: 123 of cap free
Amount of items: 2
Items: 
Size: 592348 Color: 0
Size: 407530 Color: 1

Bin 243: 125 of cap free
Amount of items: 2
Items: 
Size: 520616 Color: 0
Size: 479260 Color: 1

Bin 244: 125 of cap free
Amount of items: 2
Items: 
Size: 519129 Color: 1
Size: 480747 Color: 0

Bin 245: 125 of cap free
Amount of items: 2
Items: 
Size: 644151 Color: 0
Size: 355725 Color: 1

Bin 246: 125 of cap free
Amount of items: 2
Items: 
Size: 655721 Color: 1
Size: 344155 Color: 0

Bin 247: 126 of cap free
Amount of items: 3
Items: 
Size: 732189 Color: 1
Size: 159612 Color: 1
Size: 108074 Color: 0

Bin 248: 126 of cap free
Amount of items: 2
Items: 
Size: 648490 Color: 1
Size: 351385 Color: 0

Bin 249: 127 of cap free
Amount of items: 2
Items: 
Size: 760505 Color: 1
Size: 239369 Color: 0

Bin 250: 127 of cap free
Amount of items: 3
Items: 
Size: 346702 Color: 1
Size: 329846 Color: 1
Size: 323326 Color: 0

Bin 251: 130 of cap free
Amount of items: 2
Items: 
Size: 570454 Color: 1
Size: 429417 Color: 0

Bin 252: 131 of cap free
Amount of items: 2
Items: 
Size: 635161 Color: 1
Size: 364709 Color: 0

Bin 253: 132 of cap free
Amount of items: 2
Items: 
Size: 644817 Color: 1
Size: 355052 Color: 0

Bin 254: 133 of cap free
Amount of items: 3
Items: 
Size: 661238 Color: 0
Size: 194884 Color: 1
Size: 143746 Color: 0

Bin 255: 133 of cap free
Amount of items: 3
Items: 
Size: 611405 Color: 1
Size: 195665 Color: 0
Size: 192798 Color: 1

Bin 256: 133 of cap free
Amount of items: 3
Items: 
Size: 579052 Color: 1
Size: 241574 Color: 0
Size: 179242 Color: 1

Bin 257: 134 of cap free
Amount of items: 3
Items: 
Size: 577676 Color: 1
Size: 244017 Color: 0
Size: 178174 Color: 1

Bin 258: 135 of cap free
Amount of items: 2
Items: 
Size: 601554 Color: 0
Size: 398312 Color: 1

Bin 259: 135 of cap free
Amount of items: 2
Items: 
Size: 617490 Color: 1
Size: 382376 Color: 0

Bin 260: 135 of cap free
Amount of items: 3
Items: 
Size: 527599 Color: 1
Size: 272766 Color: 1
Size: 199501 Color: 0

Bin 261: 136 of cap free
Amount of items: 2
Items: 
Size: 553310 Color: 1
Size: 446555 Color: 0

Bin 262: 138 of cap free
Amount of items: 2
Items: 
Size: 592062 Color: 1
Size: 407801 Color: 0

Bin 263: 139 of cap free
Amount of items: 2
Items: 
Size: 516005 Color: 0
Size: 483857 Color: 1

Bin 264: 140 of cap free
Amount of items: 2
Items: 
Size: 729388 Color: 0
Size: 270473 Color: 1

Bin 265: 141 of cap free
Amount of items: 2
Items: 
Size: 691179 Color: 0
Size: 308681 Color: 1

Bin 266: 146 of cap free
Amount of items: 2
Items: 
Size: 658448 Color: 0
Size: 341407 Color: 1

Bin 267: 147 of cap free
Amount of items: 3
Items: 
Size: 746382 Color: 1
Size: 127026 Color: 1
Size: 126446 Color: 0

Bin 268: 147 of cap free
Amount of items: 2
Items: 
Size: 722492 Color: 1
Size: 277362 Color: 0

Bin 269: 147 of cap free
Amount of items: 3
Items: 
Size: 366483 Color: 0
Size: 347952 Color: 1
Size: 285419 Color: 1

Bin 270: 148 of cap free
Amount of items: 2
Items: 
Size: 589867 Color: 0
Size: 409986 Color: 1

Bin 271: 148 of cap free
Amount of items: 2
Items: 
Size: 618826 Color: 1
Size: 381027 Color: 0

Bin 272: 150 of cap free
Amount of items: 2
Items: 
Size: 585788 Color: 0
Size: 414063 Color: 1

Bin 273: 150 of cap free
Amount of items: 2
Items: 
Size: 660369 Color: 1
Size: 339482 Color: 0

Bin 274: 150 of cap free
Amount of items: 3
Items: 
Size: 523915 Color: 1
Size: 303191 Color: 1
Size: 172745 Color: 0

Bin 275: 151 of cap free
Amount of items: 2
Items: 
Size: 798955 Color: 0
Size: 200895 Color: 1

Bin 276: 152 of cap free
Amount of items: 2
Items: 
Size: 677751 Color: 0
Size: 322098 Color: 1

Bin 277: 152 of cap free
Amount of items: 2
Items: 
Size: 711776 Color: 0
Size: 288073 Color: 1

Bin 278: 152 of cap free
Amount of items: 3
Items: 
Size: 552273 Color: 0
Size: 276337 Color: 1
Size: 171239 Color: 0

Bin 279: 154 of cap free
Amount of items: 2
Items: 
Size: 763043 Color: 0
Size: 236804 Color: 1

Bin 280: 154 of cap free
Amount of items: 2
Items: 
Size: 792579 Color: 1
Size: 207268 Color: 0

Bin 281: 155 of cap free
Amount of items: 2
Items: 
Size: 523549 Color: 1
Size: 476297 Color: 0

Bin 282: 155 of cap free
Amount of items: 2
Items: 
Size: 715016 Color: 0
Size: 284830 Color: 1

Bin 283: 155 of cap free
Amount of items: 3
Items: 
Size: 360596 Color: 0
Size: 320504 Color: 0
Size: 318746 Color: 1

Bin 284: 156 of cap free
Amount of items: 2
Items: 
Size: 632854 Color: 1
Size: 366991 Color: 0

Bin 285: 159 of cap free
Amount of items: 2
Items: 
Size: 692699 Color: 1
Size: 307143 Color: 0

Bin 286: 160 of cap free
Amount of items: 2
Items: 
Size: 594523 Color: 0
Size: 405318 Color: 1

Bin 287: 160 of cap free
Amount of items: 2
Items: 
Size: 638079 Color: 0
Size: 361762 Color: 1

Bin 288: 160 of cap free
Amount of items: 2
Items: 
Size: 667302 Color: 1
Size: 332539 Color: 0

Bin 289: 160 of cap free
Amount of items: 3
Items: 
Size: 507291 Color: 1
Size: 305497 Color: 1
Size: 187053 Color: 0

Bin 290: 161 of cap free
Amount of items: 2
Items: 
Size: 521801 Color: 0
Size: 478039 Color: 1

Bin 291: 161 of cap free
Amount of items: 2
Items: 
Size: 636357 Color: 1
Size: 363483 Color: 0

Bin 292: 161 of cap free
Amount of items: 2
Items: 
Size: 797159 Color: 0
Size: 202681 Color: 1

Bin 293: 162 of cap free
Amount of items: 2
Items: 
Size: 566824 Color: 0
Size: 433015 Color: 1

Bin 294: 164 of cap free
Amount of items: 3
Items: 
Size: 772367 Color: 0
Size: 124690 Color: 0
Size: 102780 Color: 1

Bin 295: 165 of cap free
Amount of items: 2
Items: 
Size: 572493 Color: 1
Size: 427343 Color: 0

Bin 296: 166 of cap free
Amount of items: 2
Items: 
Size: 612837 Color: 0
Size: 386998 Color: 1

Bin 297: 167 of cap free
Amount of items: 2
Items: 
Size: 572924 Color: 1
Size: 426910 Color: 0

Bin 298: 168 of cap free
Amount of items: 2
Items: 
Size: 672487 Color: 1
Size: 327346 Color: 0

Bin 299: 168 of cap free
Amount of items: 2
Items: 
Size: 786879 Color: 1
Size: 212954 Color: 0

Bin 300: 169 of cap free
Amount of items: 2
Items: 
Size: 753646 Color: 0
Size: 246186 Color: 1

Bin 301: 169 of cap free
Amount of items: 2
Items: 
Size: 599961 Color: 1
Size: 399871 Color: 0

Bin 302: 169 of cap free
Amount of items: 2
Items: 
Size: 794164 Color: 0
Size: 205668 Color: 1

Bin 303: 169 of cap free
Amount of items: 2
Items: 
Size: 798149 Color: 0
Size: 201683 Color: 1

Bin 304: 169 of cap free
Amount of items: 3
Items: 
Size: 619648 Color: 0
Size: 199170 Color: 1
Size: 181014 Color: 0

Bin 305: 173 of cap free
Amount of items: 2
Items: 
Size: 551973 Color: 0
Size: 447855 Color: 1

Bin 306: 173 of cap free
Amount of items: 2
Items: 
Size: 683134 Color: 1
Size: 316694 Color: 0

Bin 307: 173 of cap free
Amount of items: 2
Items: 
Size: 705764 Color: 0
Size: 294064 Color: 1

Bin 308: 174 of cap free
Amount of items: 2
Items: 
Size: 773183 Color: 1
Size: 226644 Color: 0

Bin 309: 175 of cap free
Amount of items: 2
Items: 
Size: 691765 Color: 0
Size: 308061 Color: 1

Bin 310: 178 of cap free
Amount of items: 2
Items: 
Size: 545191 Color: 1
Size: 454632 Color: 0

Bin 311: 179 of cap free
Amount of items: 2
Items: 
Size: 513742 Color: 0
Size: 486080 Color: 1

Bin 312: 179 of cap free
Amount of items: 2
Items: 
Size: 501443 Color: 1
Size: 498379 Color: 0

Bin 313: 179 of cap free
Amount of items: 2
Items: 
Size: 615958 Color: 1
Size: 383864 Color: 0

Bin 314: 179 of cap free
Amount of items: 2
Items: 
Size: 702490 Color: 1
Size: 297332 Color: 0

Bin 315: 180 of cap free
Amount of items: 2
Items: 
Size: 523671 Color: 0
Size: 476150 Color: 1

Bin 316: 181 of cap free
Amount of items: 2
Items: 
Size: 598676 Color: 1
Size: 401144 Color: 0

Bin 317: 181 of cap free
Amount of items: 2
Items: 
Size: 672318 Color: 0
Size: 327502 Color: 1

Bin 318: 182 of cap free
Amount of items: 2
Items: 
Size: 576849 Color: 0
Size: 422970 Color: 1

Bin 319: 185 of cap free
Amount of items: 2
Items: 
Size: 620705 Color: 1
Size: 379111 Color: 0

Bin 320: 186 of cap free
Amount of items: 2
Items: 
Size: 772957 Color: 0
Size: 226858 Color: 1

Bin 321: 189 of cap free
Amount of items: 2
Items: 
Size: 640787 Color: 1
Size: 359025 Color: 0

Bin 322: 190 of cap free
Amount of items: 2
Items: 
Size: 657711 Color: 1
Size: 342100 Color: 0

Bin 323: 193 of cap free
Amount of items: 2
Items: 
Size: 654497 Color: 0
Size: 345311 Color: 1

Bin 324: 193 of cap free
Amount of items: 2
Items: 
Size: 581954 Color: 1
Size: 417854 Color: 0

Bin 325: 193 of cap free
Amount of items: 2
Items: 
Size: 721560 Color: 0
Size: 278248 Color: 1

Bin 326: 194 of cap free
Amount of items: 2
Items: 
Size: 674768 Color: 0
Size: 325039 Color: 1

Bin 327: 194 of cap free
Amount of items: 2
Items: 
Size: 501789 Color: 0
Size: 498018 Color: 1

Bin 328: 196 of cap free
Amount of items: 2
Items: 
Size: 676728 Color: 1
Size: 323077 Color: 0

Bin 329: 197 of cap free
Amount of items: 2
Items: 
Size: 605266 Color: 0
Size: 394538 Color: 1

Bin 330: 198 of cap free
Amount of items: 2
Items: 
Size: 757757 Color: 0
Size: 242046 Color: 1

Bin 331: 200 of cap free
Amount of items: 2
Items: 
Size: 652868 Color: 0
Size: 346933 Color: 1

Bin 332: 201 of cap free
Amount of items: 2
Items: 
Size: 748747 Color: 1
Size: 251053 Color: 0

Bin 333: 201 of cap free
Amount of items: 2
Items: 
Size: 744330 Color: 0
Size: 255470 Color: 1

Bin 334: 202 of cap free
Amount of items: 2
Items: 
Size: 504981 Color: 0
Size: 494818 Color: 1

Bin 335: 203 of cap free
Amount of items: 2
Items: 
Size: 575657 Color: 1
Size: 424141 Color: 0

Bin 336: 203 of cap free
Amount of items: 3
Items: 
Size: 356265 Color: 0
Size: 342778 Color: 1
Size: 300755 Color: 1

Bin 337: 204 of cap free
Amount of items: 2
Items: 
Size: 794817 Color: 1
Size: 204980 Color: 0

Bin 338: 204 of cap free
Amount of items: 3
Items: 
Size: 518377 Color: 1
Size: 280328 Color: 0
Size: 201092 Color: 0

Bin 339: 205 of cap free
Amount of items: 2
Items: 
Size: 592650 Color: 0
Size: 407146 Color: 1

Bin 340: 206 of cap free
Amount of items: 3
Items: 
Size: 672094 Color: 0
Size: 165909 Color: 1
Size: 161792 Color: 0

Bin 341: 207 of cap free
Amount of items: 2
Items: 
Size: 652483 Color: 0
Size: 347311 Color: 1

Bin 342: 209 of cap free
Amount of items: 2
Items: 
Size: 710359 Color: 1
Size: 289433 Color: 0

Bin 343: 210 of cap free
Amount of items: 2
Items: 
Size: 715512 Color: 1
Size: 284279 Color: 0

Bin 344: 211 of cap free
Amount of items: 2
Items: 
Size: 791414 Color: 1
Size: 208376 Color: 0

Bin 345: 213 of cap free
Amount of items: 3
Items: 
Size: 456320 Color: 1
Size: 372280 Color: 0
Size: 171188 Color: 1

Bin 346: 221 of cap free
Amount of items: 2
Items: 
Size: 527360 Color: 0
Size: 472420 Color: 1

Bin 347: 224 of cap free
Amount of items: 2
Items: 
Size: 507432 Color: 0
Size: 492345 Color: 1

Bin 348: 224 of cap free
Amount of items: 2
Items: 
Size: 660404 Color: 0
Size: 339373 Color: 1

Bin 349: 226 of cap free
Amount of items: 2
Items: 
Size: 688126 Color: 1
Size: 311649 Color: 0

Bin 350: 227 of cap free
Amount of items: 2
Items: 
Size: 563904 Color: 0
Size: 435870 Color: 1

Bin 351: 227 of cap free
Amount of items: 2
Items: 
Size: 610101 Color: 0
Size: 389673 Color: 1

Bin 352: 228 of cap free
Amount of items: 2
Items: 
Size: 583797 Color: 0
Size: 415976 Color: 1

Bin 353: 230 of cap free
Amount of items: 2
Items: 
Size: 529566 Color: 0
Size: 470205 Color: 1

Bin 354: 231 of cap free
Amount of items: 2
Items: 
Size: 585720 Color: 0
Size: 414050 Color: 1

Bin 355: 233 of cap free
Amount of items: 2
Items: 
Size: 739828 Color: 0
Size: 259940 Color: 1

Bin 356: 236 of cap free
Amount of items: 2
Items: 
Size: 583425 Color: 0
Size: 416340 Color: 1

Bin 357: 239 of cap free
Amount of items: 2
Items: 
Size: 560807 Color: 1
Size: 438955 Color: 0

Bin 358: 242 of cap free
Amount of items: 3
Items: 
Size: 662369 Color: 0
Size: 173578 Color: 1
Size: 163812 Color: 0

Bin 359: 243 of cap free
Amount of items: 2
Items: 
Size: 516147 Color: 1
Size: 483611 Color: 0

Bin 360: 244 of cap free
Amount of items: 2
Items: 
Size: 593420 Color: 1
Size: 406337 Color: 0

Bin 361: 244 of cap free
Amount of items: 2
Items: 
Size: 635495 Color: 0
Size: 364262 Color: 1

Bin 362: 247 of cap free
Amount of items: 2
Items: 
Size: 717574 Color: 0
Size: 282180 Color: 1

Bin 363: 247 of cap free
Amount of items: 2
Items: 
Size: 759260 Color: 1
Size: 240494 Color: 0

Bin 364: 248 of cap free
Amount of items: 3
Items: 
Size: 612032 Color: 1
Size: 194977 Color: 0
Size: 192744 Color: 1

Bin 365: 248 of cap free
Amount of items: 2
Items: 
Size: 695092 Color: 0
Size: 304661 Color: 1

Bin 366: 249 of cap free
Amount of items: 2
Items: 
Size: 558691 Color: 0
Size: 441061 Color: 1

Bin 367: 249 of cap free
Amount of items: 2
Items: 
Size: 652264 Color: 1
Size: 347488 Color: 0

Bin 368: 250 of cap free
Amount of items: 2
Items: 
Size: 632261 Color: 0
Size: 367490 Color: 1

Bin 369: 253 of cap free
Amount of items: 2
Items: 
Size: 731379 Color: 0
Size: 268369 Color: 1

Bin 370: 254 of cap free
Amount of items: 2
Items: 
Size: 681747 Color: 0
Size: 318000 Color: 1

Bin 371: 254 of cap free
Amount of items: 2
Items: 
Size: 697490 Color: 1
Size: 302257 Color: 0

Bin 372: 254 of cap free
Amount of items: 2
Items: 
Size: 793724 Color: 1
Size: 206023 Color: 0

Bin 373: 258 of cap free
Amount of items: 2
Items: 
Size: 551400 Color: 0
Size: 448343 Color: 1

Bin 374: 258 of cap free
Amount of items: 3
Items: 
Size: 689446 Color: 0
Size: 168188 Color: 1
Size: 142109 Color: 1

Bin 375: 260 of cap free
Amount of items: 2
Items: 
Size: 694302 Color: 0
Size: 305439 Color: 1

Bin 376: 260 of cap free
Amount of items: 2
Items: 
Size: 626014 Color: 1
Size: 373727 Color: 0

Bin 377: 261 of cap free
Amount of items: 2
Items: 
Size: 670305 Color: 1
Size: 329435 Color: 0

Bin 378: 263 of cap free
Amount of items: 2
Items: 
Size: 702413 Color: 1
Size: 297325 Color: 0

Bin 379: 264 of cap free
Amount of items: 2
Items: 
Size: 734475 Color: 1
Size: 265262 Color: 0

Bin 380: 266 of cap free
Amount of items: 2
Items: 
Size: 718598 Color: 0
Size: 281137 Color: 1

Bin 381: 266 of cap free
Amount of items: 2
Items: 
Size: 796280 Color: 0
Size: 203455 Color: 1

Bin 382: 269 of cap free
Amount of items: 2
Items: 
Size: 743944 Color: 1
Size: 255788 Color: 0

Bin 383: 272 of cap free
Amount of items: 2
Items: 
Size: 505621 Color: 1
Size: 494108 Color: 0

Bin 384: 274 of cap free
Amount of items: 2
Items: 
Size: 530235 Color: 1
Size: 469492 Color: 0

Bin 385: 274 of cap free
Amount of items: 2
Items: 
Size: 730121 Color: 1
Size: 269606 Color: 0

Bin 386: 274 of cap free
Amount of items: 2
Items: 
Size: 539487 Color: 1
Size: 460240 Color: 0

Bin 387: 278 of cap free
Amount of items: 2
Items: 
Size: 536823 Color: 0
Size: 462900 Color: 1

Bin 388: 279 of cap free
Amount of items: 2
Items: 
Size: 724721 Color: 1
Size: 275001 Color: 0

Bin 389: 283 of cap free
Amount of items: 2
Items: 
Size: 525805 Color: 1
Size: 473913 Color: 0

Bin 390: 284 of cap free
Amount of items: 2
Items: 
Size: 799645 Color: 1
Size: 200072 Color: 0

Bin 391: 287 of cap free
Amount of items: 2
Items: 
Size: 502085 Color: 0
Size: 497629 Color: 1

Bin 392: 288 of cap free
Amount of items: 3
Items: 
Size: 348490 Color: 1
Size: 327258 Color: 0
Size: 323965 Color: 1

Bin 393: 289 of cap free
Amount of items: 3
Items: 
Size: 686234 Color: 1
Size: 163044 Color: 1
Size: 150434 Color: 0

Bin 394: 290 of cap free
Amount of items: 2
Items: 
Size: 525155 Color: 0
Size: 474556 Color: 1

Bin 395: 290 of cap free
Amount of items: 2
Items: 
Size: 668001 Color: 1
Size: 331710 Color: 0

Bin 396: 291 of cap free
Amount of items: 2
Items: 
Size: 795698 Color: 0
Size: 204012 Color: 1

Bin 397: 294 of cap free
Amount of items: 2
Items: 
Size: 798618 Color: 1
Size: 201089 Color: 0

Bin 398: 298 of cap free
Amount of items: 2
Items: 
Size: 738059 Color: 0
Size: 261644 Color: 1

Bin 399: 302 of cap free
Amount of items: 2
Items: 
Size: 657216 Color: 1
Size: 342483 Color: 0

Bin 400: 303 of cap free
Amount of items: 2
Items: 
Size: 517044 Color: 0
Size: 482654 Color: 1

Bin 401: 303 of cap free
Amount of items: 2
Items: 
Size: 630190 Color: 0
Size: 369508 Color: 1

Bin 402: 305 of cap free
Amount of items: 3
Items: 
Size: 556379 Color: 1
Size: 275060 Color: 1
Size: 168257 Color: 0

Bin 403: 309 of cap free
Amount of items: 2
Items: 
Size: 631505 Color: 1
Size: 368187 Color: 0

Bin 404: 315 of cap free
Amount of items: 2
Items: 
Size: 653779 Color: 1
Size: 345907 Color: 0

Bin 405: 319 of cap free
Amount of items: 2
Items: 
Size: 594809 Color: 1
Size: 404873 Color: 0

Bin 406: 321 of cap free
Amount of items: 2
Items: 
Size: 602411 Color: 1
Size: 397269 Color: 0

Bin 407: 326 of cap free
Amount of items: 2
Items: 
Size: 517944 Color: 1
Size: 481731 Color: 0

Bin 408: 328 of cap free
Amount of items: 3
Items: 
Size: 495434 Color: 1
Size: 386979 Color: 0
Size: 117260 Color: 1

Bin 409: 331 of cap free
Amount of items: 2
Items: 
Size: 561536 Color: 1
Size: 438134 Color: 0

Bin 410: 337 of cap free
Amount of items: 2
Items: 
Size: 583955 Color: 1
Size: 415709 Color: 0

Bin 411: 338 of cap free
Amount of items: 2
Items: 
Size: 586779 Color: 1
Size: 412884 Color: 0

Bin 412: 338 of cap free
Amount of items: 2
Items: 
Size: 664823 Color: 0
Size: 334840 Color: 1

Bin 413: 340 of cap free
Amount of items: 2
Items: 
Size: 732580 Color: 1
Size: 267081 Color: 0

Bin 414: 341 of cap free
Amount of items: 2
Items: 
Size: 795638 Color: 1
Size: 204022 Color: 0

Bin 415: 342 of cap free
Amount of items: 2
Items: 
Size: 624988 Color: 0
Size: 374671 Color: 1

Bin 416: 345 of cap free
Amount of items: 2
Items: 
Size: 599451 Color: 1
Size: 400205 Color: 0

Bin 417: 346 of cap free
Amount of items: 2
Items: 
Size: 510297 Color: 1
Size: 489358 Color: 0

Bin 418: 348 of cap free
Amount of items: 2
Items: 
Size: 563003 Color: 1
Size: 436650 Color: 0

Bin 419: 354 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 0
Size: 386907 Color: 1

Bin 420: 354 of cap free
Amount of items: 2
Items: 
Size: 533235 Color: 0
Size: 466412 Color: 1

Bin 421: 356 of cap free
Amount of items: 2
Items: 
Size: 569720 Color: 0
Size: 429925 Color: 1

Bin 422: 358 of cap free
Amount of items: 2
Items: 
Size: 747774 Color: 0
Size: 251869 Color: 1

Bin 423: 362 of cap free
Amount of items: 2
Items: 
Size: 543922 Color: 0
Size: 455717 Color: 1

Bin 424: 364 of cap free
Amount of items: 2
Items: 
Size: 780097 Color: 0
Size: 219540 Color: 1

Bin 425: 366 of cap free
Amount of items: 2
Items: 
Size: 626898 Color: 1
Size: 372737 Color: 0

Bin 426: 368 of cap free
Amount of items: 2
Items: 
Size: 693649 Color: 0
Size: 305984 Color: 1

Bin 427: 371 of cap free
Amount of items: 2
Items: 
Size: 667034 Color: 0
Size: 332596 Color: 1

Bin 428: 374 of cap free
Amount of items: 2
Items: 
Size: 526886 Color: 1
Size: 472741 Color: 0

Bin 429: 378 of cap free
Amount of items: 2
Items: 
Size: 584786 Color: 1
Size: 414837 Color: 0

Bin 430: 380 of cap free
Amount of items: 2
Items: 
Size: 607638 Color: 0
Size: 391983 Color: 1

Bin 431: 381 of cap free
Amount of items: 2
Items: 
Size: 541680 Color: 1
Size: 457940 Color: 0

Bin 432: 383 of cap free
Amount of items: 2
Items: 
Size: 799468 Color: 0
Size: 200150 Color: 1

Bin 433: 383 of cap free
Amount of items: 2
Items: 
Size: 703978 Color: 1
Size: 295640 Color: 0

Bin 434: 386 of cap free
Amount of items: 2
Items: 
Size: 531090 Color: 0
Size: 468525 Color: 1

Bin 435: 386 of cap free
Amount of items: 2
Items: 
Size: 677980 Color: 1
Size: 321635 Color: 0

Bin 436: 389 of cap free
Amount of items: 2
Items: 
Size: 703443 Color: 1
Size: 296169 Color: 0

Bin 437: 392 of cap free
Amount of items: 2
Items: 
Size: 641533 Color: 1
Size: 358076 Color: 0

Bin 438: 393 of cap free
Amount of items: 2
Items: 
Size: 764731 Color: 0
Size: 234877 Color: 1

Bin 439: 398 of cap free
Amount of items: 2
Items: 
Size: 564000 Color: 1
Size: 435603 Color: 0

Bin 440: 400 of cap free
Amount of items: 2
Items: 
Size: 514543 Color: 1
Size: 485058 Color: 0

Bin 441: 411 of cap free
Amount of items: 2
Items: 
Size: 754927 Color: 0
Size: 244663 Color: 1

Bin 442: 416 of cap free
Amount of items: 2
Items: 
Size: 584360 Color: 0
Size: 415225 Color: 1

Bin 443: 417 of cap free
Amount of items: 2
Items: 
Size: 623806 Color: 0
Size: 375778 Color: 1

Bin 444: 420 of cap free
Amount of items: 2
Items: 
Size: 542228 Color: 0
Size: 457353 Color: 1

Bin 445: 421 of cap free
Amount of items: 2
Items: 
Size: 682845 Color: 0
Size: 316735 Color: 1

Bin 446: 424 of cap free
Amount of items: 2
Items: 
Size: 565367 Color: 0
Size: 434210 Color: 1

Bin 447: 431 of cap free
Amount of items: 2
Items: 
Size: 688092 Color: 1
Size: 311478 Color: 0

Bin 448: 432 of cap free
Amount of items: 2
Items: 
Size: 572306 Color: 1
Size: 427263 Color: 0

Bin 449: 433 of cap free
Amount of items: 2
Items: 
Size: 568306 Color: 0
Size: 431262 Color: 1

Bin 450: 434 of cap free
Amount of items: 2
Items: 
Size: 560345 Color: 0
Size: 439222 Color: 1

Bin 451: 443 of cap free
Amount of items: 2
Items: 
Size: 500435 Color: 1
Size: 499123 Color: 0

Bin 452: 444 of cap free
Amount of items: 2
Items: 
Size: 570286 Color: 0
Size: 429271 Color: 1

Bin 453: 444 of cap free
Amount of items: 2
Items: 
Size: 656716 Color: 1
Size: 342841 Color: 0

Bin 454: 447 of cap free
Amount of items: 2
Items: 
Size: 612651 Color: 1
Size: 386903 Color: 0

Bin 455: 448 of cap free
Amount of items: 2
Items: 
Size: 519935 Color: 1
Size: 479618 Color: 0

Bin 456: 451 of cap free
Amount of items: 2
Items: 
Size: 557091 Color: 0
Size: 442459 Color: 1

Bin 457: 452 of cap free
Amount of items: 3
Items: 
Size: 354533 Color: 0
Size: 342057 Color: 1
Size: 302959 Color: 1

Bin 458: 456 of cap free
Amount of items: 2
Items: 
Size: 713307 Color: 0
Size: 286238 Color: 1

Bin 459: 467 of cap free
Amount of items: 2
Items: 
Size: 681267 Color: 1
Size: 318267 Color: 0

Bin 460: 467 of cap free
Amount of items: 2
Items: 
Size: 782684 Color: 1
Size: 216850 Color: 0

Bin 461: 468 of cap free
Amount of items: 2
Items: 
Size: 676850 Color: 0
Size: 322683 Color: 1

Bin 462: 471 of cap free
Amount of items: 2
Items: 
Size: 517858 Color: 1
Size: 481672 Color: 0

Bin 463: 477 of cap free
Amount of items: 2
Items: 
Size: 726238 Color: 1
Size: 273286 Color: 0

Bin 464: 480 of cap free
Amount of items: 2
Items: 
Size: 514002 Color: 1
Size: 485519 Color: 0

Bin 465: 480 of cap free
Amount of items: 2
Items: 
Size: 571464 Color: 0
Size: 428057 Color: 1

Bin 466: 480 of cap free
Amount of items: 2
Items: 
Size: 646608 Color: 1
Size: 352913 Color: 0

Bin 467: 483 of cap free
Amount of items: 2
Items: 
Size: 762867 Color: 0
Size: 236651 Color: 1

Bin 468: 483 of cap free
Amount of items: 2
Items: 
Size: 703816 Color: 0
Size: 295702 Color: 1

Bin 469: 484 of cap free
Amount of items: 2
Items: 
Size: 706106 Color: 0
Size: 293411 Color: 1

Bin 470: 489 of cap free
Amount of items: 3
Items: 
Size: 373205 Color: 0
Size: 348316 Color: 1
Size: 277991 Color: 0

Bin 471: 492 of cap free
Amount of items: 2
Items: 
Size: 728476 Color: 1
Size: 271033 Color: 0

Bin 472: 501 of cap free
Amount of items: 2
Items: 
Size: 616776 Color: 1
Size: 382724 Color: 0

Bin 473: 501 of cap free
Amount of items: 3
Items: 
Size: 503272 Color: 1
Size: 304615 Color: 1
Size: 191613 Color: 0

Bin 474: 504 of cap free
Amount of items: 2
Items: 
Size: 507430 Color: 0
Size: 492067 Color: 1

Bin 475: 509 of cap free
Amount of items: 2
Items: 
Size: 686744 Color: 1
Size: 312748 Color: 0

Bin 476: 510 of cap free
Amount of items: 2
Items: 
Size: 578427 Color: 0
Size: 421064 Color: 1

Bin 477: 510 of cap free
Amount of items: 2
Items: 
Size: 788553 Color: 1
Size: 210938 Color: 0

Bin 478: 510 of cap free
Amount of items: 3
Items: 
Size: 750322 Color: 0
Size: 125952 Color: 1
Size: 123217 Color: 0

Bin 479: 514 of cap free
Amount of items: 2
Items: 
Size: 789248 Color: 0
Size: 210239 Color: 1

Bin 480: 516 of cap free
Amount of items: 2
Items: 
Size: 725222 Color: 0
Size: 274263 Color: 1

Bin 481: 518 of cap free
Amount of items: 2
Items: 
Size: 532951 Color: 1
Size: 466532 Color: 0

Bin 482: 518 of cap free
Amount of items: 2
Items: 
Size: 572906 Color: 0
Size: 426577 Color: 1

Bin 483: 519 of cap free
Amount of items: 2
Items: 
Size: 574478 Color: 1
Size: 425004 Color: 0

Bin 484: 519 of cap free
Amount of items: 2
Items: 
Size: 597070 Color: 0
Size: 402412 Color: 1

Bin 485: 527 of cap free
Amount of items: 2
Items: 
Size: 648217 Color: 1
Size: 351257 Color: 0

Bin 486: 532 of cap free
Amount of items: 2
Items: 
Size: 678595 Color: 0
Size: 320874 Color: 1

Bin 487: 533 of cap free
Amount of items: 2
Items: 
Size: 666160 Color: 1
Size: 333308 Color: 0

Bin 488: 536 of cap free
Amount of items: 2
Items: 
Size: 555392 Color: 0
Size: 444073 Color: 1

Bin 489: 536 of cap free
Amount of items: 2
Items: 
Size: 694781 Color: 1
Size: 304684 Color: 0

Bin 490: 542 of cap free
Amount of items: 2
Items: 
Size: 784916 Color: 1
Size: 214543 Color: 0

Bin 491: 550 of cap free
Amount of items: 2
Items: 
Size: 747159 Color: 1
Size: 252292 Color: 0

Bin 492: 554 of cap free
Amount of items: 2
Items: 
Size: 559276 Color: 1
Size: 440171 Color: 0

Bin 493: 554 of cap free
Amount of items: 3
Items: 
Size: 562754 Color: 0
Size: 258628 Color: 0
Size: 178065 Color: 1

Bin 494: 556 of cap free
Amount of items: 2
Items: 
Size: 506496 Color: 0
Size: 492949 Color: 1

Bin 495: 559 of cap free
Amount of items: 2
Items: 
Size: 651988 Color: 1
Size: 347454 Color: 0

Bin 496: 572 of cap free
Amount of items: 2
Items: 
Size: 795618 Color: 0
Size: 203811 Color: 1

Bin 497: 572 of cap free
Amount of items: 2
Items: 
Size: 684065 Color: 1
Size: 315364 Color: 0

Bin 498: 580 of cap free
Amount of items: 2
Items: 
Size: 629729 Color: 1
Size: 369692 Color: 0

Bin 499: 580 of cap free
Amount of items: 2
Items: 
Size: 506890 Color: 1
Size: 492531 Color: 0

Bin 500: 588 of cap free
Amount of items: 2
Items: 
Size: 601735 Color: 0
Size: 397678 Color: 1

Bin 501: 590 of cap free
Amount of items: 2
Items: 
Size: 687529 Color: 0
Size: 311882 Color: 1

Bin 502: 591 of cap free
Amount of items: 3
Items: 
Size: 359922 Color: 1
Size: 319749 Color: 0
Size: 319739 Color: 0

Bin 503: 592 of cap free
Amount of items: 2
Items: 
Size: 504699 Color: 1
Size: 494710 Color: 0

Bin 504: 592 of cap free
Amount of items: 2
Items: 
Size: 743303 Color: 1
Size: 256106 Color: 0

Bin 505: 599 of cap free
Amount of items: 2
Items: 
Size: 539206 Color: 1
Size: 460196 Color: 0

Bin 506: 605 of cap free
Amount of items: 2
Items: 
Size: 685711 Color: 0
Size: 313685 Color: 1

Bin 507: 606 of cap free
Amount of items: 2
Items: 
Size: 512700 Color: 1
Size: 486695 Color: 0

Bin 508: 606 of cap free
Amount of items: 2
Items: 
Size: 775297 Color: 1
Size: 224098 Color: 0

Bin 509: 611 of cap free
Amount of items: 2
Items: 
Size: 546912 Color: 1
Size: 452478 Color: 0

Bin 510: 613 of cap free
Amount of items: 2
Items: 
Size: 749175 Color: 0
Size: 250213 Color: 1

Bin 511: 614 of cap free
Amount of items: 2
Items: 
Size: 765801 Color: 0
Size: 233586 Color: 1

Bin 512: 624 of cap free
Amount of items: 2
Items: 
Size: 624609 Color: 1
Size: 374768 Color: 0

Bin 513: 625 of cap free
Amount of items: 2
Items: 
Size: 736890 Color: 0
Size: 262486 Color: 1

Bin 514: 627 of cap free
Amount of items: 2
Items: 
Size: 503169 Color: 0
Size: 496205 Color: 1

Bin 515: 629 of cap free
Amount of items: 2
Items: 
Size: 565859 Color: 1
Size: 433513 Color: 0

Bin 516: 647 of cap free
Amount of items: 2
Items: 
Size: 654402 Color: 0
Size: 344952 Color: 1

Bin 517: 649 of cap free
Amount of items: 2
Items: 
Size: 639172 Color: 1
Size: 360180 Color: 0

Bin 518: 652 of cap free
Amount of items: 2
Items: 
Size: 575059 Color: 0
Size: 424290 Color: 1

Bin 519: 652 of cap free
Amount of items: 2
Items: 
Size: 753233 Color: 1
Size: 246116 Color: 0

Bin 520: 653 of cap free
Amount of items: 2
Items: 
Size: 641464 Color: 1
Size: 357884 Color: 0

Bin 521: 653 of cap free
Amount of items: 2
Items: 
Size: 690665 Color: 1
Size: 308683 Color: 0

Bin 522: 661 of cap free
Amount of items: 2
Items: 
Size: 653516 Color: 1
Size: 345824 Color: 0

Bin 523: 663 of cap free
Amount of items: 2
Items: 
Size: 548932 Color: 0
Size: 450406 Color: 1

Bin 524: 667 of cap free
Amount of items: 2
Items: 
Size: 765090 Color: 1
Size: 234244 Color: 0

Bin 525: 675 of cap free
Amount of items: 2
Items: 
Size: 647525 Color: 1
Size: 351801 Color: 0

Bin 526: 685 of cap free
Amount of items: 2
Items: 
Size: 707462 Color: 1
Size: 291854 Color: 0

Bin 527: 686 of cap free
Amount of items: 2
Items: 
Size: 617070 Color: 0
Size: 382245 Color: 1

Bin 528: 689 of cap free
Amount of items: 2
Items: 
Size: 683375 Color: 1
Size: 315937 Color: 0

Bin 529: 695 of cap free
Amount of items: 2
Items: 
Size: 672661 Color: 0
Size: 326645 Color: 1

Bin 530: 698 of cap free
Amount of items: 2
Items: 
Size: 667875 Color: 1
Size: 331428 Color: 0

Bin 531: 701 of cap free
Amount of items: 2
Items: 
Size: 511746 Color: 0
Size: 487554 Color: 1

Bin 532: 715 of cap free
Amount of items: 2
Items: 
Size: 561352 Color: 1
Size: 437934 Color: 0

Bin 533: 716 of cap free
Amount of items: 2
Items: 
Size: 654633 Color: 1
Size: 344652 Color: 0

Bin 534: 717 of cap free
Amount of items: 2
Items: 
Size: 625993 Color: 0
Size: 373291 Color: 1

Bin 535: 717 of cap free
Amount of items: 2
Items: 
Size: 779897 Color: 0
Size: 219387 Color: 1

Bin 536: 719 of cap free
Amount of items: 2
Items: 
Size: 717592 Color: 1
Size: 281690 Color: 0

Bin 537: 727 of cap free
Amount of items: 2
Items: 
Size: 732620 Color: 0
Size: 266654 Color: 1

Bin 538: 727 of cap free
Amount of items: 2
Items: 
Size: 622290 Color: 0
Size: 376984 Color: 1

Bin 539: 738 of cap free
Amount of items: 2
Items: 
Size: 549816 Color: 1
Size: 449447 Color: 0

Bin 540: 741 of cap free
Amount of items: 2
Items: 
Size: 755883 Color: 0
Size: 243377 Color: 1

Bin 541: 742 of cap free
Amount of items: 2
Items: 
Size: 748359 Color: 1
Size: 250900 Color: 0

Bin 542: 743 of cap free
Amount of items: 2
Items: 
Size: 714093 Color: 1
Size: 285165 Color: 0

Bin 543: 748 of cap free
Amount of items: 2
Items: 
Size: 799215 Color: 1
Size: 200038 Color: 0

Bin 544: 756 of cap free
Amount of items: 2
Items: 
Size: 561227 Color: 0
Size: 438018 Color: 1

Bin 545: 763 of cap free
Amount of items: 2
Items: 
Size: 784381 Color: 0
Size: 214857 Color: 1

Bin 546: 765 of cap free
Amount of items: 2
Items: 
Size: 693672 Color: 1
Size: 305564 Color: 0

Bin 547: 772 of cap free
Amount of items: 2
Items: 
Size: 714584 Color: 0
Size: 284645 Color: 1

Bin 548: 787 of cap free
Amount of items: 2
Items: 
Size: 652858 Color: 0
Size: 346356 Color: 1

Bin 549: 787 of cap free
Amount of items: 2
Items: 
Size: 663135 Color: 0
Size: 336079 Color: 1

Bin 550: 788 of cap free
Amount of items: 2
Items: 
Size: 764994 Color: 1
Size: 234219 Color: 0

Bin 551: 798 of cap free
Amount of items: 2
Items: 
Size: 599359 Color: 0
Size: 399844 Color: 1

Bin 552: 799 of cap free
Amount of items: 2
Items: 
Size: 603025 Color: 1
Size: 396177 Color: 0

Bin 553: 803 of cap free
Amount of items: 2
Items: 
Size: 655515 Color: 1
Size: 343683 Color: 0

Bin 554: 808 of cap free
Amount of items: 2
Items: 
Size: 676755 Color: 0
Size: 322438 Color: 1

Bin 555: 811 of cap free
Amount of items: 3
Items: 
Size: 615177 Color: 0
Size: 195884 Color: 0
Size: 188129 Color: 1

Bin 556: 813 of cap free
Amount of items: 2
Items: 
Size: 703079 Color: 1
Size: 296109 Color: 0

Bin 557: 815 of cap free
Amount of items: 2
Items: 
Size: 554412 Color: 0
Size: 444774 Color: 1

Bin 558: 818 of cap free
Amount of items: 2
Items: 
Size: 543074 Color: 1
Size: 456109 Color: 0

Bin 559: 822 of cap free
Amount of items: 2
Items: 
Size: 511717 Color: 0
Size: 487462 Color: 1

Bin 560: 822 of cap free
Amount of items: 2
Items: 
Size: 707580 Color: 0
Size: 291599 Color: 1

Bin 561: 828 of cap free
Amount of items: 2
Items: 
Size: 605745 Color: 0
Size: 393428 Color: 1

Bin 562: 836 of cap free
Amount of items: 2
Items: 
Size: 613958 Color: 1
Size: 385207 Color: 0

Bin 563: 845 of cap free
Amount of items: 2
Items: 
Size: 706269 Color: 1
Size: 292887 Color: 0

Bin 564: 849 of cap free
Amount of items: 2
Items: 
Size: 519532 Color: 0
Size: 479620 Color: 1

Bin 565: 851 of cap free
Amount of items: 2
Items: 
Size: 614824 Color: 1
Size: 384326 Color: 0

Bin 566: 875 of cap free
Amount of items: 2
Items: 
Size: 687524 Color: 0
Size: 311602 Color: 1

Bin 567: 883 of cap free
Amount of items: 2
Items: 
Size: 790029 Color: 0
Size: 209089 Color: 1

Bin 568: 888 of cap free
Amount of items: 2
Items: 
Size: 601335 Color: 1
Size: 397778 Color: 0

Bin 569: 888 of cap free
Amount of items: 2
Items: 
Size: 681041 Color: 1
Size: 318072 Color: 0

Bin 570: 902 of cap free
Amount of items: 2
Items: 
Size: 733389 Color: 0
Size: 265710 Color: 1

Bin 571: 903 of cap free
Amount of items: 2
Items: 
Size: 757818 Color: 1
Size: 241280 Color: 0

Bin 572: 916 of cap free
Amount of items: 2
Items: 
Size: 648430 Color: 0
Size: 350655 Color: 1

Bin 573: 919 of cap free
Amount of items: 2
Items: 
Size: 632335 Color: 1
Size: 366747 Color: 0

Bin 574: 920 of cap free
Amount of items: 2
Items: 
Size: 759039 Color: 0
Size: 240042 Color: 1

Bin 575: 930 of cap free
Amount of items: 2
Items: 
Size: 672369 Color: 1
Size: 326702 Color: 0

Bin 576: 933 of cap free
Amount of items: 2
Items: 
Size: 500012 Color: 1
Size: 499056 Color: 0

Bin 577: 941 of cap free
Amount of items: 2
Items: 
Size: 629578 Color: 1
Size: 369482 Color: 0

Bin 578: 943 of cap free
Amount of items: 3
Items: 
Size: 644883 Color: 0
Size: 187347 Color: 0
Size: 166828 Color: 1

Bin 579: 943 of cap free
Amount of items: 2
Items: 
Size: 566839 Color: 1
Size: 432219 Color: 0

Bin 580: 949 of cap free
Amount of items: 2
Items: 
Size: 512671 Color: 0
Size: 486381 Color: 1

Bin 581: 962 of cap free
Amount of items: 2
Items: 
Size: 605689 Color: 0
Size: 393350 Color: 1

Bin 582: 967 of cap free
Amount of items: 2
Items: 
Size: 762400 Color: 0
Size: 236634 Color: 1

Bin 583: 969 of cap free
Amount of items: 2
Items: 
Size: 735957 Color: 1
Size: 263075 Color: 0

Bin 584: 980 of cap free
Amount of items: 2
Items: 
Size: 636317 Color: 0
Size: 362704 Color: 1

Bin 585: 982 of cap free
Amount of items: 2
Items: 
Size: 724095 Color: 0
Size: 274924 Color: 1

Bin 586: 990 of cap free
Amount of items: 2
Items: 
Size: 509142 Color: 1
Size: 489869 Color: 0

Bin 587: 994 of cap free
Amount of items: 2
Items: 
Size: 757251 Color: 0
Size: 241756 Color: 1

Bin 588: 1004 of cap free
Amount of items: 2
Items: 
Size: 682326 Color: 0
Size: 316671 Color: 1

Bin 589: 1009 of cap free
Amount of items: 2
Items: 
Size: 716995 Color: 0
Size: 281997 Color: 1

Bin 590: 1014 of cap free
Amount of items: 2
Items: 
Size: 634037 Color: 0
Size: 364950 Color: 1

Bin 591: 1016 of cap free
Amount of items: 2
Items: 
Size: 719075 Color: 0
Size: 279910 Color: 1

Bin 592: 1017 of cap free
Amount of items: 2
Items: 
Size: 574403 Color: 1
Size: 424581 Color: 0

Bin 593: 1020 of cap free
Amount of items: 2
Items: 
Size: 659038 Color: 0
Size: 339943 Color: 1

Bin 594: 1034 of cap free
Amount of items: 2
Items: 
Size: 609952 Color: 0
Size: 389015 Color: 1

Bin 595: 1037 of cap free
Amount of items: 2
Items: 
Size: 790370 Color: 1
Size: 208594 Color: 0

Bin 596: 1039 of cap free
Amount of items: 2
Items: 
Size: 537171 Color: 1
Size: 461791 Color: 0

Bin 597: 1044 of cap free
Amount of items: 2
Items: 
Size: 770819 Color: 0
Size: 228138 Color: 1

Bin 598: 1056 of cap free
Amount of items: 2
Items: 
Size: 699953 Color: 0
Size: 298992 Color: 1

Bin 599: 1067 of cap free
Amount of items: 2
Items: 
Size: 642792 Color: 1
Size: 356142 Color: 0

Bin 600: 1083 of cap free
Amount of items: 2
Items: 
Size: 748719 Color: 0
Size: 250199 Color: 1

Bin 601: 1100 of cap free
Amount of items: 2
Items: 
Size: 772752 Color: 1
Size: 226149 Color: 0

Bin 602: 1102 of cap free
Amount of items: 2
Items: 
Size: 558136 Color: 0
Size: 440763 Color: 1

Bin 603: 1102 of cap free
Amount of items: 2
Items: 
Size: 591432 Color: 0
Size: 407467 Color: 1

Bin 604: 1106 of cap free
Amount of items: 2
Items: 
Size: 618186 Color: 1
Size: 380709 Color: 0

Bin 605: 1116 of cap free
Amount of items: 2
Items: 
Size: 728976 Color: 1
Size: 269909 Color: 0

Bin 606: 1116 of cap free
Amount of items: 2
Items: 
Size: 552180 Color: 0
Size: 446705 Color: 1

Bin 607: 1118 of cap free
Amount of items: 2
Items: 
Size: 711377 Color: 0
Size: 287506 Color: 1

Bin 608: 1128 of cap free
Amount of items: 2
Items: 
Size: 797373 Color: 0
Size: 201500 Color: 1

Bin 609: 1133 of cap free
Amount of items: 2
Items: 
Size: 796085 Color: 1
Size: 202783 Color: 0

Bin 610: 1138 of cap free
Amount of items: 2
Items: 
Size: 641458 Color: 0
Size: 357405 Color: 1

Bin 611: 1155 of cap free
Amount of items: 2
Items: 
Size: 662228 Color: 1
Size: 336618 Color: 0

Bin 612: 1161 of cap free
Amount of items: 2
Items: 
Size: 568217 Color: 1
Size: 430623 Color: 0

Bin 613: 1161 of cap free
Amount of items: 2
Items: 
Size: 795587 Color: 0
Size: 203253 Color: 1

Bin 614: 1162 of cap free
Amount of items: 2
Items: 
Size: 716990 Color: 0
Size: 281849 Color: 1

Bin 615: 1166 of cap free
Amount of items: 2
Items: 
Size: 542933 Color: 1
Size: 455902 Color: 0

Bin 616: 1167 of cap free
Amount of items: 2
Items: 
Size: 680169 Color: 0
Size: 318665 Color: 1

Bin 617: 1205 of cap free
Amount of items: 2
Items: 
Size: 668517 Color: 0
Size: 330279 Color: 1

Bin 618: 1210 of cap free
Amount of items: 2
Items: 
Size: 726055 Color: 1
Size: 272736 Color: 0

Bin 619: 1215 of cap free
Amount of items: 2
Items: 
Size: 749675 Color: 1
Size: 249111 Color: 0

Bin 620: 1228 of cap free
Amount of items: 2
Items: 
Size: 703627 Color: 0
Size: 295146 Color: 1

Bin 621: 1247 of cap free
Amount of items: 2
Items: 
Size: 513747 Color: 1
Size: 485007 Color: 0

Bin 622: 1255 of cap free
Amount of items: 2
Items: 
Size: 538721 Color: 0
Size: 460025 Color: 1

Bin 623: 1273 of cap free
Amount of items: 2
Items: 
Size: 751931 Color: 1
Size: 246797 Color: 0

Bin 624: 1276 of cap free
Amount of items: 2
Items: 
Size: 577998 Color: 0
Size: 420727 Color: 1

Bin 625: 1276 of cap free
Amount of items: 2
Items: 
Size: 737945 Color: 1
Size: 260780 Color: 0

Bin 626: 1286 of cap free
Amount of items: 2
Items: 
Size: 706087 Color: 1
Size: 292628 Color: 0

Bin 627: 1301 of cap free
Amount of items: 2
Items: 
Size: 707535 Color: 0
Size: 291165 Color: 1

Bin 628: 1303 of cap free
Amount of items: 2
Items: 
Size: 659316 Color: 1
Size: 339382 Color: 0

Bin 629: 1304 of cap free
Amount of items: 2
Items: 
Size: 784314 Color: 0
Size: 214383 Color: 1

Bin 630: 1313 of cap free
Amount of items: 2
Items: 
Size: 743914 Color: 1
Size: 254774 Color: 0

Bin 631: 1316 of cap free
Amount of items: 2
Items: 
Size: 782265 Color: 1
Size: 216420 Color: 0

Bin 632: 1321 of cap free
Amount of items: 2
Items: 
Size: 776044 Color: 1
Size: 222636 Color: 0

Bin 633: 1328 of cap free
Amount of items: 2
Items: 
Size: 644675 Color: 0
Size: 353998 Color: 1

Bin 634: 1354 of cap free
Amount of items: 2
Items: 
Size: 575592 Color: 1
Size: 423055 Color: 0

Bin 635: 1357 of cap free
Amount of items: 3
Items: 
Size: 677927 Color: 1
Size: 163712 Color: 1
Size: 157005 Color: 0

Bin 636: 1363 of cap free
Amount of items: 2
Items: 
Size: 788425 Color: 0
Size: 210213 Color: 1

Bin 637: 1363 of cap free
Amount of items: 2
Items: 
Size: 763789 Color: 0
Size: 234849 Color: 1

Bin 638: 1372 of cap free
Amount of items: 2
Items: 
Size: 623417 Color: 0
Size: 375212 Color: 1

Bin 639: 1380 of cap free
Amount of items: 2
Items: 
Size: 531050 Color: 1
Size: 467571 Color: 0

Bin 640: 1386 of cap free
Amount of items: 2
Items: 
Size: 622792 Color: 1
Size: 375823 Color: 0

Bin 641: 1388 of cap free
Amount of items: 2
Items: 
Size: 684312 Color: 0
Size: 314301 Color: 1

Bin 642: 1393 of cap free
Amount of items: 3
Items: 
Size: 720899 Color: 1
Size: 153884 Color: 1
Size: 123825 Color: 0

Bin 643: 1400 of cap free
Amount of items: 2
Items: 
Size: 765184 Color: 0
Size: 233417 Color: 1

Bin 644: 1403 of cap free
Amount of items: 2
Items: 
Size: 664739 Color: 1
Size: 333859 Color: 0

Bin 645: 1407 of cap free
Amount of items: 2
Items: 
Size: 775971 Color: 1
Size: 222623 Color: 0

Bin 646: 1417 of cap free
Amount of items: 2
Items: 
Size: 558439 Color: 1
Size: 440145 Color: 0

Bin 647: 1421 of cap free
Amount of items: 2
Items: 
Size: 515658 Color: 1
Size: 482922 Color: 0

Bin 648: 1426 of cap free
Amount of items: 2
Items: 
Size: 708968 Color: 0
Size: 289607 Color: 1

Bin 649: 1427 of cap free
Amount of items: 2
Items: 
Size: 779819 Color: 0
Size: 218755 Color: 1

Bin 650: 1428 of cap free
Amount of items: 2
Items: 
Size: 618038 Color: 1
Size: 380535 Color: 0

Bin 651: 1431 of cap free
Amount of items: 2
Items: 
Size: 680948 Color: 1
Size: 317622 Color: 0

Bin 652: 1441 of cap free
Amount of items: 2
Items: 
Size: 648011 Color: 0
Size: 350549 Color: 1

Bin 653: 1444 of cap free
Amount of items: 2
Items: 
Size: 621966 Color: 0
Size: 376591 Color: 1

Bin 654: 1455 of cap free
Amount of items: 2
Items: 
Size: 766357 Color: 1
Size: 232189 Color: 0

Bin 655: 1464 of cap free
Amount of items: 2
Items: 
Size: 606874 Color: 0
Size: 391663 Color: 1

Bin 656: 1474 of cap free
Amount of items: 2
Items: 
Size: 789853 Color: 0
Size: 208674 Color: 1

Bin 657: 1475 of cap free
Amount of items: 2
Items: 
Size: 652411 Color: 0
Size: 346115 Color: 1

Bin 658: 1485 of cap free
Amount of items: 2
Items: 
Size: 743791 Color: 0
Size: 254725 Color: 1

Bin 659: 1502 of cap free
Amount of items: 2
Items: 
Size: 507083 Color: 0
Size: 491416 Color: 1

Bin 660: 1506 of cap free
Amount of items: 2
Items: 
Size: 574926 Color: 0
Size: 423569 Color: 1

Bin 661: 1510 of cap free
Amount of items: 2
Items: 
Size: 612709 Color: 0
Size: 385782 Color: 1

Bin 662: 1516 of cap free
Amount of items: 2
Items: 
Size: 600826 Color: 1
Size: 397659 Color: 0

Bin 663: 1518 of cap free
Amount of items: 2
Items: 
Size: 707456 Color: 1
Size: 291027 Color: 0

Bin 664: 1524 of cap free
Amount of items: 2
Items: 
Size: 716854 Color: 1
Size: 281623 Color: 0

Bin 665: 1529 of cap free
Amount of items: 2
Items: 
Size: 564652 Color: 0
Size: 433820 Color: 1

Bin 666: 1547 of cap free
Amount of items: 2
Items: 
Size: 577968 Color: 0
Size: 420486 Color: 1

Bin 667: 1561 of cap free
Amount of items: 2
Items: 
Size: 749446 Color: 1
Size: 248994 Color: 0

Bin 668: 1575 of cap free
Amount of items: 2
Items: 
Size: 625898 Color: 0
Size: 372528 Color: 1

Bin 669: 1581 of cap free
Amount of items: 2
Items: 
Size: 523417 Color: 1
Size: 475003 Color: 0

Bin 670: 1589 of cap free
Amount of items: 2
Items: 
Size: 758449 Color: 0
Size: 239963 Color: 1

Bin 671: 1590 of cap free
Amount of items: 2
Items: 
Size: 631782 Color: 0
Size: 366629 Color: 1

Bin 672: 1631 of cap free
Amount of items: 2
Items: 
Size: 639198 Color: 0
Size: 359172 Color: 1

Bin 673: 1633 of cap free
Amount of items: 2
Items: 
Size: 609757 Color: 0
Size: 388611 Color: 1

Bin 674: 1637 of cap free
Amount of items: 2
Items: 
Size: 609451 Color: 1
Size: 388913 Color: 0

Bin 675: 1647 of cap free
Amount of items: 2
Items: 
Size: 691577 Color: 0
Size: 306777 Color: 1

Bin 676: 1672 of cap free
Amount of items: 2
Items: 
Size: 703460 Color: 0
Size: 294869 Color: 1

Bin 677: 1678 of cap free
Amount of items: 2
Items: 
Size: 770435 Color: 0
Size: 227888 Color: 1

Bin 678: 1693 of cap free
Amount of items: 2
Items: 
Size: 634579 Color: 1
Size: 363729 Color: 0

Bin 679: 1694 of cap free
Amount of items: 2
Items: 
Size: 672017 Color: 0
Size: 326290 Color: 1

Bin 680: 1704 of cap free
Amount of items: 2
Items: 
Size: 748251 Color: 0
Size: 250046 Color: 1

Bin 681: 1710 of cap free
Amount of items: 2
Items: 
Size: 788084 Color: 0
Size: 210207 Color: 1

Bin 682: 1711 of cap free
Amount of items: 2
Items: 
Size: 757487 Color: 1
Size: 240803 Color: 0

Bin 683: 1726 of cap free
Amount of items: 3
Items: 
Size: 532946 Color: 1
Size: 308538 Color: 1
Size: 156791 Color: 0

Bin 684: 1745 of cap free
Amount of items: 2
Items: 
Size: 730197 Color: 0
Size: 268059 Color: 1

Bin 685: 1750 of cap free
Amount of items: 2
Items: 
Size: 574879 Color: 0
Size: 423372 Color: 1

Bin 686: 1754 of cap free
Amount of items: 2
Items: 
Size: 675518 Color: 1
Size: 322729 Color: 0

Bin 687: 1768 of cap free
Amount of items: 2
Items: 
Size: 612699 Color: 0
Size: 385534 Color: 1

Bin 688: 1781 of cap free
Amount of items: 2
Items: 
Size: 711076 Color: 0
Size: 287144 Color: 1

Bin 689: 1786 of cap free
Amount of items: 2
Items: 
Size: 567950 Color: 1
Size: 430265 Color: 0

Bin 690: 1790 of cap free
Amount of items: 2
Items: 
Size: 643903 Color: 1
Size: 354308 Color: 0

Bin 691: 1792 of cap free
Amount of items: 2
Items: 
Size: 770429 Color: 0
Size: 227780 Color: 1

Bin 692: 1820 of cap free
Amount of items: 2
Items: 
Size: 614830 Color: 0
Size: 383351 Color: 1

Bin 693: 1837 of cap free
Amount of items: 2
Items: 
Size: 549131 Color: 1
Size: 449033 Color: 0

Bin 694: 1851 of cap free
Amount of items: 2
Items: 
Size: 528015 Color: 0
Size: 470135 Color: 1

Bin 695: 1852 of cap free
Amount of items: 2
Items: 
Size: 567863 Color: 0
Size: 430286 Color: 1

Bin 696: 1856 of cap free
Amount of items: 2
Items: 
Size: 647312 Color: 1
Size: 350833 Color: 0

Bin 697: 1865 of cap free
Amount of items: 2
Items: 
Size: 543001 Color: 0
Size: 455135 Color: 1

Bin 698: 1876 of cap free
Amount of items: 2
Items: 
Size: 765977 Color: 1
Size: 232148 Color: 0

Bin 699: 1880 of cap free
Amount of items: 2
Items: 
Size: 591366 Color: 0
Size: 406755 Color: 1

Bin 700: 1891 of cap free
Amount of items: 2
Items: 
Size: 510290 Color: 1
Size: 487820 Color: 0

Bin 701: 1912 of cap free
Amount of items: 3
Items: 
Size: 682957 Color: 1
Size: 172210 Color: 0
Size: 142922 Color: 0

Bin 702: 1942 of cap free
Amount of items: 2
Items: 
Size: 758436 Color: 0
Size: 239623 Color: 1

Bin 703: 1962 of cap free
Amount of items: 2
Items: 
Size: 562360 Color: 0
Size: 435679 Color: 1

Bin 704: 2004 of cap free
Amount of items: 2
Items: 
Size: 558289 Color: 1
Size: 439708 Color: 0

Bin 705: 2034 of cap free
Amount of items: 2
Items: 
Size: 518943 Color: 0
Size: 479024 Color: 1

Bin 706: 2041 of cap free
Amount of items: 2
Items: 
Size: 675500 Color: 1
Size: 322460 Color: 0

Bin 707: 2057 of cap free
Amount of items: 2
Items: 
Size: 722010 Color: 0
Size: 275934 Color: 1

Bin 708: 2091 of cap free
Amount of items: 2
Items: 
Size: 689824 Color: 1
Size: 308086 Color: 0

Bin 709: 2093 of cap free
Amount of items: 2
Items: 
Size: 634572 Color: 1
Size: 363336 Color: 0

Bin 710: 2101 of cap free
Amount of items: 2
Items: 
Size: 725226 Color: 1
Size: 272674 Color: 0

Bin 711: 2110 of cap free
Amount of items: 2
Items: 
Size: 711060 Color: 0
Size: 286831 Color: 1

Bin 712: 2110 of cap free
Amount of items: 2
Items: 
Size: 640209 Color: 1
Size: 357682 Color: 0

Bin 713: 2114 of cap free
Amount of items: 2
Items: 
Size: 530093 Color: 0
Size: 467794 Color: 1

Bin 714: 2130 of cap free
Amount of items: 2
Items: 
Size: 716763 Color: 1
Size: 281108 Color: 0

Bin 715: 2138 of cap free
Amount of items: 2
Items: 
Size: 562218 Color: 0
Size: 435645 Color: 1

Bin 716: 2154 of cap free
Amount of items: 2
Items: 
Size: 712726 Color: 1
Size: 285121 Color: 0

Bin 717: 2171 of cap free
Amount of items: 2
Items: 
Size: 658105 Color: 0
Size: 339725 Color: 1

Bin 718: 2182 of cap free
Amount of items: 2
Items: 
Size: 594444 Color: 0
Size: 403375 Color: 1

Bin 719: 2209 of cap free
Amount of items: 2
Items: 
Size: 580155 Color: 1
Size: 417637 Color: 0

Bin 720: 2214 of cap free
Amount of items: 2
Items: 
Size: 538278 Color: 0
Size: 459509 Color: 1

Bin 721: 2224 of cap free
Amount of items: 2
Items: 
Size: 662147 Color: 0
Size: 335630 Color: 1

Bin 722: 2228 of cap free
Amount of items: 2
Items: 
Size: 784313 Color: 0
Size: 213460 Color: 1

Bin 723: 2232 of cap free
Amount of items: 2
Items: 
Size: 798472 Color: 1
Size: 199297 Color: 0

Bin 724: 2246 of cap free
Amount of items: 2
Items: 
Size: 559759 Color: 0
Size: 437996 Color: 1

Bin 725: 2279 of cap free
Amount of items: 2
Items: 
Size: 522842 Color: 0
Size: 474880 Color: 1

Bin 726: 2295 of cap free
Amount of items: 2
Items: 
Size: 738767 Color: 0
Size: 258939 Color: 1

Bin 727: 2299 of cap free
Amount of items: 2
Items: 
Size: 754592 Color: 0
Size: 243110 Color: 1

Bin 728: 2314 of cap free
Amount of items: 2
Items: 
Size: 747641 Color: 0
Size: 250046 Color: 1

Bin 729: 2329 of cap free
Amount of items: 2
Items: 
Size: 546169 Color: 1
Size: 451503 Color: 0

Bin 730: 2347 of cap free
Amount of items: 2
Items: 
Size: 604053 Color: 1
Size: 393601 Color: 0

Bin 731: 2377 of cap free
Amount of items: 2
Items: 
Size: 532631 Color: 1
Size: 464993 Color: 0

Bin 732: 2378 of cap free
Amount of items: 2
Items: 
Size: 614647 Color: 0
Size: 382976 Color: 1

Bin 733: 2382 of cap free
Amount of items: 2
Items: 
Size: 613482 Color: 1
Size: 384137 Color: 0

Bin 734: 2427 of cap free
Amount of items: 2
Items: 
Size: 594275 Color: 0
Size: 403299 Color: 1

Bin 735: 2428 of cap free
Amount of items: 2
Items: 
Size: 746917 Color: 1
Size: 250656 Color: 0

Bin 736: 2443 of cap free
Amount of items: 2
Items: 
Size: 730017 Color: 0
Size: 267541 Color: 1

Bin 737: 2448 of cap free
Amount of items: 2
Items: 
Size: 734964 Color: 1
Size: 262589 Color: 0

Bin 738: 2467 of cap free
Amount of items: 2
Items: 
Size: 633412 Color: 0
Size: 364122 Color: 1

Bin 739: 2473 of cap free
Amount of items: 2
Items: 
Size: 510208 Color: 1
Size: 487320 Color: 0

Bin 740: 2493 of cap free
Amount of items: 2
Items: 
Size: 638896 Color: 0
Size: 358612 Color: 1

Bin 741: 2535 of cap free
Amount of items: 2
Items: 
Size: 618020 Color: 1
Size: 379446 Color: 0

Bin 742: 2537 of cap free
Amount of items: 2
Items: 
Size: 553494 Color: 0
Size: 443970 Color: 1

Bin 743: 2552 of cap free
Amount of items: 2
Items: 
Size: 637270 Color: 1
Size: 360179 Color: 0

Bin 744: 2554 of cap free
Amount of items: 2
Items: 
Size: 546078 Color: 1
Size: 451369 Color: 0

Bin 745: 2637 of cap free
Amount of items: 2
Items: 
Size: 609223 Color: 1
Size: 388141 Color: 0

Bin 746: 2692 of cap free
Amount of items: 2
Items: 
Size: 667999 Color: 0
Size: 329310 Color: 1

Bin 747: 2753 of cap free
Amount of items: 2
Items: 
Size: 522693 Color: 0
Size: 474555 Color: 1

Bin 748: 2783 of cap free
Amount of items: 2
Items: 
Size: 527238 Color: 0
Size: 469980 Color: 1

Bin 749: 2786 of cap free
Amount of items: 2
Items: 
Size: 598535 Color: 0
Size: 398680 Color: 1

Bin 750: 2829 of cap free
Amount of items: 2
Items: 
Size: 716316 Color: 1
Size: 280856 Color: 0

Bin 751: 2846 of cap free
Amount of items: 2
Items: 
Size: 580094 Color: 1
Size: 417061 Color: 0

Bin 752: 2871 of cap free
Amount of items: 2
Items: 
Size: 537783 Color: 0
Size: 459347 Color: 1

Bin 753: 2878 of cap free
Amount of items: 2
Items: 
Size: 789251 Color: 1
Size: 207872 Color: 0

Bin 754: 2964 of cap free
Amount of items: 2
Items: 
Size: 545836 Color: 1
Size: 451201 Color: 0

Bin 755: 2986 of cap free
Amount of items: 2
Items: 
Size: 613206 Color: 1
Size: 383809 Color: 0

Bin 756: 2993 of cap free
Amount of items: 2
Items: 
Size: 584110 Color: 0
Size: 412898 Color: 1

Bin 757: 3015 of cap free
Amount of items: 2
Items: 
Size: 742356 Color: 0
Size: 254630 Color: 1

Bin 758: 3187 of cap free
Amount of items: 2
Items: 
Size: 522414 Color: 0
Size: 474400 Color: 1

Bin 759: 3211 of cap free
Amount of items: 2
Items: 
Size: 605653 Color: 0
Size: 391137 Color: 1

Bin 760: 3226 of cap free
Amount of items: 2
Items: 
Size: 764972 Color: 1
Size: 231803 Color: 0

Bin 761: 3285 of cap free
Amount of items: 2
Items: 
Size: 710821 Color: 0
Size: 285895 Color: 1

Bin 762: 3289 of cap free
Amount of items: 2
Items: 
Size: 574256 Color: 1
Size: 422456 Color: 0

Bin 763: 3364 of cap free
Amount of items: 2
Items: 
Size: 646701 Color: 0
Size: 349936 Color: 1

Bin 764: 3455 of cap free
Amount of items: 2
Items: 
Size: 510710 Color: 0
Size: 485836 Color: 1

Bin 765: 3517 of cap free
Amount of items: 2
Items: 
Size: 584430 Color: 1
Size: 412054 Color: 0

Bin 766: 3529 of cap free
Amount of items: 2
Items: 
Size: 712252 Color: 1
Size: 284220 Color: 0

Bin 767: 3608 of cap free
Amount of items: 2
Items: 
Size: 757236 Color: 1
Size: 239157 Color: 0

Bin 768: 3643 of cap free
Amount of items: 2
Items: 
Size: 702114 Color: 0
Size: 294244 Color: 1

Bin 769: 3732 of cap free
Amount of items: 2
Items: 
Size: 705372 Color: 1
Size: 290897 Color: 0

Bin 770: 3821 of cap free
Amount of items: 2
Items: 
Size: 745616 Color: 1
Size: 250564 Color: 0

Bin 771: 3830 of cap free
Amount of items: 2
Items: 
Size: 720395 Color: 1
Size: 275776 Color: 0

Bin 772: 3843 of cap free
Amount of items: 2
Items: 
Size: 783169 Color: 0
Size: 212989 Color: 1

Bin 773: 3859 of cap free
Amount of items: 2
Items: 
Size: 646489 Color: 0
Size: 349653 Color: 1

Bin 774: 3867 of cap free
Amount of items: 2
Items: 
Size: 510485 Color: 0
Size: 485649 Color: 1

Bin 775: 3892 of cap free
Amount of items: 2
Items: 
Size: 741742 Color: 0
Size: 254367 Color: 1

Bin 776: 4040 of cap free
Amount of items: 2
Items: 
Size: 629575 Color: 1
Size: 366386 Color: 0

Bin 777: 4062 of cap free
Amount of items: 2
Items: 
Size: 602381 Color: 1
Size: 393558 Color: 0

Bin 778: 4073 of cap free
Amount of items: 2
Items: 
Size: 566699 Color: 1
Size: 429229 Color: 0

Bin 779: 4117 of cap free
Amount of items: 2
Items: 
Size: 591306 Color: 1
Size: 404578 Color: 0

Bin 780: 4123 of cap free
Amount of items: 2
Items: 
Size: 502557 Color: 1
Size: 493321 Color: 0

Bin 781: 4209 of cap free
Amount of items: 2
Items: 
Size: 573664 Color: 1
Size: 422128 Color: 0

Bin 782: 4224 of cap free
Amount of items: 2
Items: 
Size: 510176 Color: 0
Size: 485601 Color: 1

Bin 783: 4225 of cap free
Amount of items: 2
Items: 
Size: 621666 Color: 0
Size: 374110 Color: 1

Bin 784: 4330 of cap free
Amount of items: 2
Items: 
Size: 502484 Color: 1
Size: 493187 Color: 0

Bin 785: 4356 of cap free
Amount of items: 2
Items: 
Size: 667526 Color: 0
Size: 328119 Color: 1

Bin 786: 4403 of cap free
Amount of items: 2
Items: 
Size: 782800 Color: 0
Size: 212798 Color: 1

Bin 787: 4405 of cap free
Amount of items: 2
Items: 
Size: 741431 Color: 0
Size: 254165 Color: 1

Bin 788: 4407 of cap free
Amount of items: 2
Items: 
Size: 594029 Color: 0
Size: 401565 Color: 1

Bin 789: 4450 of cap free
Amount of items: 2
Items: 
Size: 700404 Color: 1
Size: 295147 Color: 0

Bin 790: 4498 of cap free
Amount of items: 2
Items: 
Size: 746756 Color: 0
Size: 248747 Color: 1

Bin 791: 4505 of cap free
Amount of items: 2
Items: 
Size: 583728 Color: 1
Size: 411768 Color: 0

Bin 792: 4589 of cap free
Amount of items: 2
Items: 
Size: 756737 Color: 1
Size: 238675 Color: 0

Bin 793: 4616 of cap free
Amount of items: 2
Items: 
Size: 537139 Color: 0
Size: 458246 Color: 1

Bin 794: 4729 of cap free
Amount of items: 2
Items: 
Size: 782669 Color: 0
Size: 212603 Color: 1

Bin 795: 4790 of cap free
Amount of items: 2
Items: 
Size: 509722 Color: 0
Size: 485489 Color: 1

Bin 796: 4845 of cap free
Amount of items: 2
Items: 
Size: 559692 Color: 0
Size: 435464 Color: 1

Bin 797: 4942 of cap free
Amount of items: 2
Items: 
Size: 576789 Color: 0
Size: 418270 Color: 1

Bin 798: 4968 of cap free
Amount of items: 2
Items: 
Size: 787867 Color: 0
Size: 207166 Color: 1

Bin 799: 5256 of cap free
Amount of items: 2
Items: 
Size: 530675 Color: 1
Size: 464070 Color: 0

Bin 800: 5289 of cap free
Amount of items: 2
Items: 
Size: 573577 Color: 1
Size: 421135 Color: 0

Bin 801: 5442 of cap free
Amount of items: 2
Items: 
Size: 756052 Color: 1
Size: 238507 Color: 0

Bin 802: 5536 of cap free
Amount of items: 2
Items: 
Size: 526898 Color: 0
Size: 467567 Color: 1

Bin 803: 5638 of cap free
Amount of items: 2
Items: 
Size: 557915 Color: 1
Size: 436448 Color: 0

Bin 804: 5674 of cap free
Amount of items: 2
Items: 
Size: 501819 Color: 1
Size: 492508 Color: 0

Bin 805: 5707 of cap free
Amount of items: 2
Items: 
Size: 671887 Color: 1
Size: 322407 Color: 0

Bin 806: 5786 of cap free
Amount of items: 2
Items: 
Size: 631735 Color: 0
Size: 362480 Color: 1

Bin 807: 5885 of cap free
Amount of items: 2
Items: 
Size: 557702 Color: 1
Size: 436414 Color: 0

Bin 808: 5912 of cap free
Amount of items: 2
Items: 
Size: 663928 Color: 1
Size: 330161 Color: 0

Bin 809: 5927 of cap free
Amount of items: 2
Items: 
Size: 709972 Color: 1
Size: 284102 Color: 0

Bin 810: 5938 of cap free
Amount of items: 2
Items: 
Size: 699385 Color: 1
Size: 294678 Color: 0

Bin 811: 5940 of cap free
Amount of items: 2
Items: 
Size: 508682 Color: 0
Size: 485379 Color: 1

Bin 812: 6057 of cap free
Amount of items: 2
Items: 
Size: 754554 Color: 0
Size: 239390 Color: 1

Bin 813: 6116 of cap free
Amount of items: 2
Items: 
Size: 781286 Color: 0
Size: 212599 Color: 1

Bin 814: 6175 of cap free
Amount of items: 2
Items: 
Size: 565706 Color: 1
Size: 428120 Color: 0

Bin 815: 6195 of cap free
Amount of items: 2
Items: 
Size: 729202 Color: 0
Size: 264604 Color: 1

Bin 816: 6288 of cap free
Amount of items: 2
Items: 
Size: 621539 Color: 0
Size: 372174 Color: 1

Bin 817: 6340 of cap free
Amount of items: 2
Items: 
Size: 605210 Color: 0
Size: 388451 Color: 1

Bin 818: 6351 of cap free
Amount of items: 2
Items: 
Size: 602373 Color: 1
Size: 391277 Color: 0

Bin 819: 6382 of cap free
Amount of items: 2
Items: 
Size: 633636 Color: 1
Size: 359983 Color: 0

Bin 820: 6672 of cap free
Amount of items: 2
Items: 
Size: 770935 Color: 1
Size: 222394 Color: 0

Bin 821: 6721 of cap free
Amount of items: 2
Items: 
Size: 709528 Color: 1
Size: 283752 Color: 0

Bin 822: 6773 of cap free
Amount of items: 2
Items: 
Size: 611959 Color: 0
Size: 381269 Color: 1

Bin 823: 6846 of cap free
Amount of items: 2
Items: 
Size: 754413 Color: 0
Size: 238742 Color: 1

Bin 824: 7165 of cap free
Amount of items: 2
Items: 
Size: 754370 Color: 0
Size: 238466 Color: 1

Bin 825: 7211 of cap free
Amount of items: 2
Items: 
Size: 734392 Color: 1
Size: 258398 Color: 0

Bin 826: 7217 of cap free
Amount of items: 2
Items: 
Size: 557669 Color: 1
Size: 435115 Color: 0

Bin 827: 7363 of cap free
Amount of items: 2
Items: 
Size: 666763 Color: 0
Size: 325875 Color: 1

Bin 828: 7495 of cap free
Amount of items: 2
Items: 
Size: 593889 Color: 0
Size: 398617 Color: 1

Bin 829: 7530 of cap free
Amount of items: 2
Items: 
Size: 544606 Color: 1
Size: 447865 Color: 0

Bin 830: 7574 of cap free
Amount of items: 2
Items: 
Size: 754239 Color: 0
Size: 238188 Color: 1

Bin 831: 7839 of cap free
Amount of items: 2
Items: 
Size: 620472 Color: 0
Size: 371690 Color: 1

Bin 832: 7973 of cap free
Amount of items: 2
Items: 
Size: 620349 Color: 0
Size: 371679 Color: 1

Bin 833: 7996 of cap free
Amount of items: 2
Items: 
Size: 666203 Color: 0
Size: 325802 Color: 1

Bin 834: 8118 of cap free
Amount of items: 2
Items: 
Size: 557105 Color: 1
Size: 434778 Color: 0

Bin 835: 8228 of cap free
Amount of items: 2
Items: 
Size: 603633 Color: 0
Size: 388140 Color: 1

Bin 836: 8269 of cap free
Amount of items: 2
Items: 
Size: 526169 Color: 0
Size: 465563 Color: 1

Bin 837: 8418 of cap free
Amount of items: 2
Items: 
Size: 580008 Color: 1
Size: 411575 Color: 0

Bin 838: 8419 of cap free
Amount of items: 2
Items: 
Size: 779338 Color: 0
Size: 212244 Color: 1

Bin 839: 8613 of cap free
Amount of items: 2
Items: 
Size: 600386 Color: 1
Size: 391002 Color: 0

Bin 840: 8672 of cap free
Amount of items: 2
Items: 
Size: 620125 Color: 0
Size: 371204 Color: 1

Bin 841: 8735 of cap free
Amount of items: 2
Items: 
Size: 600318 Color: 1
Size: 390948 Color: 0

Bin 842: 8904 of cap free
Amount of items: 2
Items: 
Size: 529746 Color: 1
Size: 461351 Color: 0

Bin 843: 8950 of cap free
Amount of items: 2
Items: 
Size: 556713 Color: 1
Size: 434338 Color: 0

Bin 844: 8980 of cap free
Amount of items: 2
Items: 
Size: 620052 Color: 0
Size: 370969 Color: 1

Bin 845: 9096 of cap free
Amount of items: 2
Items: 
Size: 768940 Color: 1
Size: 221965 Color: 0

Bin 846: 9313 of cap free
Amount of items: 2
Items: 
Size: 574765 Color: 0
Size: 415923 Color: 1

Bin 847: 9480 of cap free
Amount of items: 2
Items: 
Size: 526136 Color: 0
Size: 464385 Color: 1

Bin 848: 9721 of cap free
Amount of items: 2
Items: 
Size: 745418 Color: 1
Size: 244862 Color: 0

Bin 849: 10117 of cap free
Amount of items: 2
Items: 
Size: 591323 Color: 0
Size: 398561 Color: 1

Bin 850: 10121 of cap free
Amount of items: 2
Items: 
Size: 578905 Color: 1
Size: 410975 Color: 0

Bin 851: 10230 of cap free
Amount of items: 2
Items: 
Size: 556361 Color: 0
Size: 433410 Color: 1

Bin 852: 11208 of cap free
Amount of items: 2
Items: 
Size: 528909 Color: 1
Size: 459884 Color: 0

Bin 853: 11357 of cap free
Amount of items: 2
Items: 
Size: 528873 Color: 1
Size: 459771 Color: 0

Bin 854: 11546 of cap free
Amount of items: 2
Items: 
Size: 528796 Color: 1
Size: 459659 Color: 0

Bin 855: 11685 of cap free
Amount of items: 2
Items: 
Size: 504801 Color: 0
Size: 483515 Color: 1

Bin 856: 11831 of cap free
Amount of items: 2
Items: 
Size: 658441 Color: 1
Size: 329729 Color: 0

Bin 857: 12346 of cap free
Amount of items: 2
Items: 
Size: 528723 Color: 1
Size: 458932 Color: 0

Bin 858: 13199 of cap free
Amount of items: 2
Items: 
Size: 599132 Color: 1
Size: 387670 Color: 0

Bin 859: 13513 of cap free
Amount of items: 2
Items: 
Size: 658354 Color: 1
Size: 328134 Color: 0

Bin 860: 13564 of cap free
Amount of items: 2
Items: 
Size: 682175 Color: 0
Size: 304262 Color: 1

Bin 861: 14378 of cap free
Amount of items: 2
Items: 
Size: 752979 Color: 0
Size: 232644 Color: 1

Bin 862: 14587 of cap free
Amount of items: 2
Items: 
Size: 779263 Color: 0
Size: 206151 Color: 1

Bin 863: 14650 of cap free
Amount of items: 2
Items: 
Size: 627943 Color: 1
Size: 357408 Color: 0

Bin 864: 14735 of cap free
Amount of items: 2
Items: 
Size: 527834 Color: 1
Size: 457432 Color: 0

Bin 865: 14757 of cap free
Amount of items: 2
Items: 
Size: 694452 Color: 1
Size: 290792 Color: 0

Bin 866: 14793 of cap free
Amount of items: 2
Items: 
Size: 556077 Color: 0
Size: 429131 Color: 1

Bin 867: 14820 of cap free
Amount of items: 2
Items: 
Size: 573915 Color: 0
Size: 411266 Color: 1

Bin 868: 14915 of cap free
Amount of items: 2
Items: 
Size: 764909 Color: 1
Size: 220177 Color: 0

Bin 869: 15869 of cap free
Amount of items: 2
Items: 
Size: 556090 Color: 1
Size: 428042 Color: 0

Bin 870: 16308 of cap free
Amount of items: 2
Items: 
Size: 573521 Color: 0
Size: 410172 Color: 1

Bin 871: 16439 of cap free
Amount of items: 2
Items: 
Size: 638712 Color: 0
Size: 344850 Color: 1

Bin 872: 17097 of cap free
Amount of items: 2
Items: 
Size: 752278 Color: 0
Size: 230626 Color: 1

Bin 873: 17685 of cap free
Amount of items: 2
Items: 
Size: 526539 Color: 1
Size: 455777 Color: 0

Bin 874: 18311 of cap free
Amount of items: 2
Items: 
Size: 777167 Color: 0
Size: 204523 Color: 1

Bin 875: 18483 of cap free
Amount of items: 2
Items: 
Size: 517481 Color: 0
Size: 464037 Color: 1

Bin 876: 19740 of cap free
Amount of items: 2
Items: 
Size: 516680 Color: 0
Size: 463581 Color: 1

Bin 877: 19825 of cap free
Amount of items: 2
Items: 
Size: 657952 Color: 1
Size: 322224 Color: 0

Bin 878: 20220 of cap free
Amount of items: 2
Items: 
Size: 551850 Color: 0
Size: 427931 Color: 1

Bin 879: 22754 of cap free
Amount of items: 2
Items: 
Size: 746660 Color: 0
Size: 230587 Color: 1

Bin 880: 24368 of cap free
Amount of items: 2
Items: 
Size: 701943 Color: 0
Size: 273690 Color: 1

Bin 881: 25363 of cap free
Amount of items: 2
Items: 
Size: 774956 Color: 0
Size: 199682 Color: 1

Bin 882: 26098 of cap free
Amount of items: 2
Items: 
Size: 603300 Color: 0
Size: 370603 Color: 1

Bin 883: 26248 of cap free
Amount of items: 2
Items: 
Size: 701086 Color: 0
Size: 272667 Color: 1

Bin 884: 27681 of cap free
Amount of items: 2
Items: 
Size: 727883 Color: 1
Size: 244437 Color: 0

Bin 885: 30470 of cap free
Amount of items: 2
Items: 
Size: 649832 Color: 1
Size: 319699 Color: 0

Bin 886: 32258 of cap free
Amount of items: 2
Items: 
Size: 665245 Color: 0
Size: 302498 Color: 1

Bin 887: 34797 of cap free
Amount of items: 2
Items: 
Size: 501771 Color: 0
Size: 463433 Color: 1

Bin 888: 36085 of cap free
Amount of items: 2
Items: 
Size: 649319 Color: 1
Size: 314597 Color: 0

Bin 889: 39062 of cap free
Amount of items: 2
Items: 
Size: 551009 Color: 0
Size: 409930 Color: 1

Bin 890: 40165 of cap free
Amount of items: 2
Items: 
Size: 550337 Color: 0
Size: 409499 Color: 1

Bin 891: 40772 of cap free
Amount of items: 2
Items: 
Size: 501196 Color: 0
Size: 458033 Color: 1

Bin 892: 42555 of cap free
Amount of items: 2
Items: 
Size: 500141 Color: 0
Size: 457305 Color: 1

Bin 893: 46082 of cap free
Amount of items: 2
Items: 
Size: 498812 Color: 0
Size: 455107 Color: 1

Bin 894: 54587 of cap free
Amount of items: 2
Items: 
Size: 491237 Color: 0
Size: 454177 Color: 1

Bin 895: 60048 of cap free
Amount of items: 2
Items: 
Size: 486510 Color: 0
Size: 453443 Color: 1

Bin 896: 61673 of cap free
Amount of items: 2
Items: 
Size: 738745 Color: 0
Size: 199583 Color: 1

Bin 897: 63414 of cap free
Amount of items: 2
Items: 
Size: 484710 Color: 0
Size: 451877 Color: 1

Bin 898: 63706 of cap free
Amount of items: 2
Items: 
Size: 526324 Color: 1
Size: 409971 Color: 0

Bin 899: 64944 of cap free
Amount of items: 2
Items: 
Size: 525611 Color: 1
Size: 409446 Color: 0

Bin 900: 65332 of cap free
Amount of items: 2
Items: 
Size: 525313 Color: 1
Size: 409356 Color: 0

Bin 901: 65344 of cap free
Amount of items: 2
Items: 
Size: 484546 Color: 0
Size: 450111 Color: 1

Bin 902: 68288 of cap free
Amount of items: 2
Items: 
Size: 522741 Color: 1
Size: 408972 Color: 0

Bin 903: 68568 of cap free
Amount of items: 2
Items: 
Size: 522722 Color: 1
Size: 408711 Color: 0

Bin 904: 69356 of cap free
Amount of items: 2
Items: 
Size: 798133 Color: 1
Size: 132512 Color: 0

Bin 905: 73525 of cap free
Amount of items: 2
Items: 
Size: 521949 Color: 1
Size: 404527 Color: 0

Bin 906: 76274 of cap free
Amount of items: 2
Items: 
Size: 520884 Color: 1
Size: 402843 Color: 0

Bin 907: 77852 of cap free
Amount of items: 2
Items: 
Size: 519746 Color: 1
Size: 402403 Color: 0

Bin 908: 78243 of cap free
Amount of items: 2
Items: 
Size: 519581 Color: 1
Size: 402177 Color: 0

Bin 909: 79429 of cap free
Amount of items: 2
Items: 
Size: 550119 Color: 0
Size: 370453 Color: 1

Bin 910: 79506 of cap free
Amount of items: 2
Items: 
Size: 519066 Color: 1
Size: 401429 Color: 0

Bin 911: 87784 of cap free
Amount of items: 2
Items: 
Size: 549938 Color: 0
Size: 362279 Color: 1

Bin 912: 93838 of cap free
Amount of items: 2
Items: 
Size: 518725 Color: 1
Size: 387438 Color: 0

Bin 913: 98393 of cap free
Amount of items: 2
Items: 
Size: 518336 Color: 1
Size: 383272 Color: 0

Bin 914: 106648 of cap free
Amount of items: 2
Items: 
Size: 515545 Color: 1
Size: 377808 Color: 0

Bin 915: 107607 of cap free
Amount of items: 2
Items: 
Size: 736228 Color: 0
Size: 156166 Color: 1

Bin 916: 206695 of cap free
Amount of items: 1
Items: 
Size: 793306 Color: 1

Bin 917: 206739 of cap free
Amount of items: 1
Items: 
Size: 793262 Color: 1

Bin 918: 206949 of cap free
Amount of items: 1
Items: 
Size: 793052 Color: 1

Bin 919: 213243 of cap free
Amount of items: 1
Items: 
Size: 786758 Color: 1

Bin 920: 213248 of cap free
Amount of items: 1
Items: 
Size: 786753 Color: 1

Bin 921: 213527 of cap free
Amount of items: 1
Items: 
Size: 786474 Color: 1

Bin 922: 218996 of cap free
Amount of items: 1
Items: 
Size: 781005 Color: 1

Bin 923: 263978 of cap free
Amount of items: 1
Items: 
Size: 736023 Color: 0

Bin 924: 271818 of cap free
Amount of items: 1
Items: 
Size: 728183 Color: 0

Bin 925: 280008 of cap free
Amount of items: 1
Items: 
Size: 719993 Color: 1

Total size: 919136191
Total free space: 5864734

