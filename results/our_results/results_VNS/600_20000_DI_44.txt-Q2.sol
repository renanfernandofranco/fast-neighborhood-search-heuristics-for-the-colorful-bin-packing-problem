Capicity Bin: 15632
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 27
Items: 
Size: 728 Color: 1
Size: 728 Color: 0
Size: 712 Color: 0
Size: 672 Color: 0
Size: 668 Color: 0
Size: 656 Color: 0
Size: 648 Color: 0
Size: 640 Color: 0
Size: 624 Color: 1
Size: 624 Color: 0
Size: 624 Color: 0
Size: 608 Color: 1
Size: 608 Color: 0
Size: 598 Color: 0
Size: 590 Color: 0
Size: 574 Color: 1
Size: 560 Color: 1
Size: 560 Color: 1
Size: 556 Color: 1
Size: 528 Color: 1
Size: 488 Color: 1
Size: 484 Color: 1
Size: 472 Color: 1
Size: 472 Color: 1
Size: 464 Color: 1
Size: 426 Color: 0
Size: 320 Color: 1

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 7828 Color: 0
Size: 1232 Color: 0
Size: 1088 Color: 0
Size: 1048 Color: 1
Size: 1048 Color: 1
Size: 1032 Color: 1
Size: 1000 Color: 1
Size: 848 Color: 0
Size: 508 Color: 1

Bin 3: 0 of cap free
Amount of items: 7
Items: 
Size: 7832 Color: 0
Size: 1911 Color: 0
Size: 1604 Color: 0
Size: 1441 Color: 0
Size: 1296 Color: 1
Size: 1146 Color: 1
Size: 402 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 1
Size: 5682 Color: 1
Size: 530 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 0
Size: 5036 Color: 0
Size: 1064 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 0
Size: 5688 Color: 0
Size: 332 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 0
Size: 5020 Color: 0
Size: 556 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10658 Color: 0
Size: 4582 Color: 1
Size: 392 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 1
Size: 4076 Color: 1
Size: 496 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11364 Color: 0
Size: 3528 Color: 1
Size: 740 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 2856 Color: 0
Size: 532 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12443 Color: 0
Size: 2791 Color: 0
Size: 398 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 1
Size: 1448 Color: 1
Size: 1120 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 1524 Color: 0
Size: 1016 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 1
Size: 1932 Color: 0
Size: 540 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 1
Size: 1768 Color: 0
Size: 504 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13339 Color: 0
Size: 1561 Color: 0
Size: 732 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 0
Size: 1468 Color: 0
Size: 620 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13494 Color: 1
Size: 1136 Color: 1
Size: 1002 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 0
Size: 1296 Color: 0
Size: 740 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 0
Size: 1138 Color: 1
Size: 792 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 0
Size: 1450 Color: 1
Size: 476 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 0
Size: 1300 Color: 0
Size: 382 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 1
Size: 1334 Color: 1
Size: 264 Color: 0

Bin 25: 1 of cap free
Amount of items: 9
Items: 
Size: 7821 Color: 0
Size: 1040 Color: 0
Size: 1024 Color: 0
Size: 1016 Color: 0
Size: 1010 Color: 0
Size: 1000 Color: 1
Size: 976 Color: 1
Size: 944 Color: 1
Size: 800 Color: 1

Bin 26: 1 of cap free
Amount of items: 5
Items: 
Size: 8747 Color: 0
Size: 4900 Color: 0
Size: 1300 Color: 1
Size: 442 Color: 1
Size: 242 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 1
Size: 6511 Color: 0
Size: 312 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 9324 Color: 0
Size: 5011 Color: 1
Size: 1296 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 9482 Color: 1
Size: 5001 Color: 0
Size: 1148 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 1
Size: 5699 Color: 0
Size: 336 Color: 1

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 10307 Color: 1
Size: 5324 Color: 0

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 12181 Color: 0
Size: 2992 Color: 1
Size: 458 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 0
Size: 3096 Color: 1
Size: 334 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 0
Size: 1671 Color: 1
Size: 1302 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 0
Size: 2441 Color: 1

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 13271 Color: 1
Size: 2360 Color: 0

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 13243 Color: 0
Size: 2388 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 13619 Color: 0
Size: 1678 Color: 1
Size: 334 Color: 1

Bin 39: 2 of cap free
Amount of items: 11
Items: 
Size: 7818 Color: 0
Size: 928 Color: 1
Size: 916 Color: 0
Size: 912 Color: 1
Size: 912 Color: 0
Size: 894 Color: 0
Size: 886 Color: 1
Size: 884 Color: 1
Size: 864 Color: 1
Size: 336 Color: 0
Size: 280 Color: 1

Bin 40: 2 of cap free
Amount of items: 8
Items: 
Size: 7820 Color: 1
Size: 1350 Color: 0
Size: 1324 Color: 0
Size: 1316 Color: 0
Size: 1132 Color: 1
Size: 1124 Color: 1
Size: 1052 Color: 1
Size: 512 Color: 0

Bin 41: 2 of cap free
Amount of items: 5
Items: 
Size: 8072 Color: 0
Size: 3645 Color: 1
Size: 3417 Color: 1
Size: 256 Color: 1
Size: 240 Color: 0

Bin 42: 2 of cap free
Amount of items: 4
Items: 
Size: 8738 Color: 1
Size: 5084 Color: 0
Size: 1348 Color: 1
Size: 460 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 0
Size: 5746 Color: 1
Size: 576 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 1
Size: 5666 Color: 0
Size: 448 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 9619 Color: 1
Size: 5739 Color: 0
Size: 272 Color: 1

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 0
Size: 4146 Color: 0
Size: 420 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 1
Size: 3362 Color: 1
Size: 416 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 1
Size: 3702 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 12722 Color: 0
Size: 1608 Color: 0
Size: 1300 Color: 1

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 13526 Color: 0
Size: 1296 Color: 1
Size: 808 Color: 1

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 1
Size: 1991 Color: 0

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 1
Size: 1822 Color: 0

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 14020 Color: 1
Size: 1610 Color: 0

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 0
Size: 1590 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 0
Size: 1538 Color: 1
Size: 40 Color: 0

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 8795 Color: 0
Size: 6502 Color: 1
Size: 332 Color: 0

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 5689 Color: 1
Size: 604 Color: 0

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 9571 Color: 1
Size: 5626 Color: 0
Size: 432 Color: 0

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 0
Size: 3571 Color: 1
Size: 964 Color: 0

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 11190 Color: 1
Size: 4439 Color: 0

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 11615 Color: 0
Size: 4014 Color: 1

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 1
Size: 2957 Color: 1
Size: 420 Color: 0

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 0
Size: 3349 Color: 1

Bin 64: 4 of cap free
Amount of items: 7
Items: 
Size: 7826 Color: 1
Size: 1560 Color: 0
Size: 1402 Color: 0
Size: 1368 Color: 0
Size: 1336 Color: 0
Size: 1136 Color: 1
Size: 1000 Color: 1

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 1
Size: 6832 Color: 0
Size: 592 Color: 1

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 8834 Color: 0
Size: 6506 Color: 1
Size: 288 Color: 0

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 11400 Color: 1
Size: 3812 Color: 1
Size: 416 Color: 0

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 0
Size: 1436 Color: 1
Size: 1132 Color: 0

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 1
Size: 2246 Color: 0
Size: 288 Color: 0

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 1
Size: 2038 Color: 0

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 0
Size: 1920 Color: 1

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 13790 Color: 1
Size: 1838 Color: 0

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 8807 Color: 0
Size: 6504 Color: 1
Size: 316 Color: 0

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 8818 Color: 1
Size: 5051 Color: 1
Size: 1758 Color: 0

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 1
Size: 1782 Color: 0

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 10114 Color: 0
Size: 5256 Color: 1
Size: 256 Color: 1

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 1
Size: 3130 Color: 1
Size: 280 Color: 0

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 0
Size: 3208 Color: 1

Bin 79: 8 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 0
Size: 3588 Color: 1
Size: 944 Color: 1

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 12507 Color: 1
Size: 2877 Color: 0
Size: 240 Color: 1

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 1
Size: 1820 Color: 0

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 13813 Color: 1
Size: 1811 Color: 0

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 0
Size: 1520 Color: 1
Size: 112 Color: 1

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 0
Size: 1564 Color: 1

Bin 85: 9 of cap free
Amount of items: 2
Items: 
Size: 12821 Color: 1
Size: 2802 Color: 0

Bin 86: 9 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 1
Size: 1859 Color: 0

Bin 87: 10 of cap free
Amount of items: 3
Items: 
Size: 11349 Color: 1
Size: 4001 Color: 0
Size: 272 Color: 0

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 11598 Color: 1
Size: 4024 Color: 0

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 0
Size: 1470 Color: 1
Size: 1380 Color: 1

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 13411 Color: 1
Size: 2211 Color: 0

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 1
Size: 2110 Color: 0

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 13726 Color: 1
Size: 1896 Color: 0

Bin 93: 11 of cap free
Amount of items: 11
Items: 
Size: 7817 Color: 0
Size: 864 Color: 1
Size: 864 Color: 0
Size: 828 Color: 1
Size: 800 Color: 0
Size: 780 Color: 1
Size: 760 Color: 0
Size: 756 Color: 1
Size: 736 Color: 0
Size: 712 Color: 0
Size: 704 Color: 1

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 12520 Color: 1
Size: 3101 Color: 0

Bin 95: 11 of cap free
Amount of items: 3
Items: 
Size: 12703 Color: 0
Size: 2118 Color: 1
Size: 800 Color: 1

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 13219 Color: 1
Size: 2402 Color: 0

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 10440 Color: 1
Size: 5180 Color: 0

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 1
Size: 1890 Color: 0
Size: 1852 Color: 0

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 0
Size: 2668 Color: 1

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 1
Size: 2161 Color: 0

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 0
Size: 2072 Color: 1

Bin 102: 12 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 1
Size: 1724 Color: 0

Bin 103: 12 of cap free
Amount of items: 2
Items: 
Size: 14014 Color: 0
Size: 1606 Color: 1

Bin 104: 13 of cap free
Amount of items: 3
Items: 
Size: 9631 Color: 1
Size: 5808 Color: 0
Size: 180 Color: 1

Bin 105: 13 of cap free
Amount of items: 2
Items: 
Size: 13495 Color: 0
Size: 2124 Color: 1

Bin 106: 13 of cap free
Amount of items: 2
Items: 
Size: 13589 Color: 0
Size: 2030 Color: 1

Bin 107: 13 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 1
Size: 2011 Color: 0

Bin 108: 14 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 1
Size: 1872 Color: 0
Size: 1661 Color: 1

Bin 109: 14 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 0
Size: 2314 Color: 1

Bin 110: 14 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 0
Size: 1742 Color: 1

Bin 111: 14 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 1
Size: 1740 Color: 0

Bin 112: 15 of cap free
Amount of items: 2
Items: 
Size: 12895 Color: 1
Size: 2722 Color: 0

Bin 113: 16 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 0
Size: 3246 Color: 1

Bin 114: 16 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 0
Size: 2248 Color: 1

Bin 115: 16 of cap free
Amount of items: 2
Items: 
Size: 13916 Color: 1
Size: 1700 Color: 0

Bin 116: 17 of cap free
Amount of items: 2
Items: 
Size: 11973 Color: 1
Size: 3642 Color: 0

Bin 117: 17 of cap free
Amount of items: 2
Items: 
Size: 12649 Color: 1
Size: 2966 Color: 0

Bin 118: 18 of cap free
Amount of items: 3
Items: 
Size: 13147 Color: 0
Size: 2343 Color: 1
Size: 124 Color: 0

Bin 119: 19 of cap free
Amount of items: 2
Items: 
Size: 13542 Color: 1
Size: 2071 Color: 0

Bin 120: 19 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 1
Size: 1781 Color: 0

Bin 121: 21 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 1
Size: 2295 Color: 0

Bin 122: 21 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 0
Size: 2181 Color: 1

Bin 123: 22 of cap free
Amount of items: 2
Items: 
Size: 9096 Color: 1
Size: 6514 Color: 0

Bin 124: 22 of cap free
Amount of items: 2
Items: 
Size: 11262 Color: 1
Size: 4348 Color: 0

Bin 125: 22 of cap free
Amount of items: 2
Items: 
Size: 13980 Color: 1
Size: 1630 Color: 0

Bin 126: 24 of cap free
Amount of items: 2
Items: 
Size: 10508 Color: 1
Size: 5100 Color: 0

Bin 127: 24 of cap free
Amount of items: 2
Items: 
Size: 11332 Color: 1
Size: 4276 Color: 0

Bin 128: 24 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 0
Size: 2820 Color: 1

Bin 129: 25 of cap free
Amount of items: 4
Items: 
Size: 13759 Color: 0
Size: 1688 Color: 1
Size: 96 Color: 1
Size: 64 Color: 0

Bin 130: 26 of cap free
Amount of items: 2
Items: 
Size: 12442 Color: 0
Size: 3164 Color: 1

Bin 131: 26 of cap free
Amount of items: 2
Items: 
Size: 12778 Color: 1
Size: 2828 Color: 0

Bin 132: 26 of cap free
Amount of items: 2
Items: 
Size: 13903 Color: 0
Size: 1703 Color: 1

Bin 133: 27 of cap free
Amount of items: 2
Items: 
Size: 13118 Color: 0
Size: 2487 Color: 1

Bin 134: 28 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 0
Size: 2152 Color: 1

Bin 135: 29 of cap free
Amount of items: 2
Items: 
Size: 11174 Color: 1
Size: 4429 Color: 0

Bin 136: 30 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 1
Size: 2794 Color: 0

Bin 137: 31 of cap free
Amount of items: 2
Items: 
Size: 11689 Color: 1
Size: 3912 Color: 0

Bin 138: 32 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 0
Size: 4584 Color: 1
Size: 208 Color: 1

Bin 139: 32 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 0
Size: 1880 Color: 1

Bin 140: 35 of cap free
Amount of items: 2
Items: 
Size: 12938 Color: 0
Size: 2659 Color: 1

Bin 141: 36 of cap free
Amount of items: 2
Items: 
Size: 13627 Color: 0
Size: 1969 Color: 1

Bin 142: 37 of cap free
Amount of items: 2
Items: 
Size: 10319 Color: 0
Size: 5276 Color: 1

Bin 143: 37 of cap free
Amount of items: 2
Items: 
Size: 13403 Color: 1
Size: 2192 Color: 0

Bin 144: 38 of cap free
Amount of items: 2
Items: 
Size: 12560 Color: 1
Size: 3034 Color: 0

Bin 145: 39 of cap free
Amount of items: 2
Items: 
Size: 13211 Color: 1
Size: 2382 Color: 0

Bin 146: 41 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 0
Size: 4479 Color: 1

Bin 147: 41 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 1
Size: 2091 Color: 0

Bin 148: 45 of cap free
Amount of items: 2
Items: 
Size: 11259 Color: 1
Size: 4328 Color: 0

Bin 149: 45 of cap free
Amount of items: 2
Items: 
Size: 13039 Color: 1
Size: 2548 Color: 0

Bin 150: 47 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 0
Size: 3005 Color: 1

Bin 151: 47 of cap free
Amount of items: 2
Items: 
Size: 13294 Color: 1
Size: 2291 Color: 0

Bin 152: 48 of cap free
Amount of items: 2
Items: 
Size: 10136 Color: 0
Size: 5448 Color: 1

Bin 153: 48 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 1
Size: 2019 Color: 1
Size: 1491 Color: 0

Bin 154: 48 of cap free
Amount of items: 2
Items: 
Size: 12979 Color: 0
Size: 2605 Color: 1

Bin 155: 56 of cap free
Amount of items: 2
Items: 
Size: 13150 Color: 1
Size: 2426 Color: 0

Bin 156: 57 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 0
Size: 2997 Color: 1

Bin 157: 59 of cap free
Amount of items: 2
Items: 
Size: 11911 Color: 1
Size: 3662 Color: 0

Bin 158: 59 of cap free
Amount of items: 2
Items: 
Size: 13292 Color: 1
Size: 2281 Color: 0

Bin 159: 59 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 1
Size: 1679 Color: 0

Bin 160: 60 of cap free
Amount of items: 2
Items: 
Size: 12285 Color: 0
Size: 3287 Color: 1

Bin 161: 60 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 0
Size: 1702 Color: 1

Bin 162: 62 of cap free
Amount of items: 2
Items: 
Size: 9958 Color: 0
Size: 5612 Color: 1

Bin 163: 66 of cap free
Amount of items: 2
Items: 
Size: 11238 Color: 1
Size: 4328 Color: 0

Bin 164: 66 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 1
Size: 3782 Color: 0

Bin 165: 66 of cap free
Amount of items: 2
Items: 
Size: 13622 Color: 0
Size: 1944 Color: 1

Bin 166: 67 of cap free
Amount of items: 2
Items: 
Size: 12883 Color: 1
Size: 2682 Color: 0

Bin 167: 70 of cap free
Amount of items: 2
Items: 
Size: 7834 Color: 0
Size: 7728 Color: 1

Bin 168: 70 of cap free
Amount of items: 2
Items: 
Size: 11844 Color: 0
Size: 3718 Color: 1

Bin 169: 71 of cap free
Amount of items: 2
Items: 
Size: 10831 Color: 0
Size: 4730 Color: 1

Bin 170: 74 of cap free
Amount of items: 2
Items: 
Size: 11994 Color: 1
Size: 3564 Color: 0

Bin 171: 80 of cap free
Amount of items: 2
Items: 
Size: 8900 Color: 1
Size: 6652 Color: 0

Bin 172: 81 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 1
Size: 3911 Color: 0

Bin 173: 82 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 0
Size: 2352 Color: 1

Bin 174: 84 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 1
Size: 2936 Color: 0

Bin 175: 86 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 0
Size: 5126 Color: 1

Bin 176: 86 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 1
Size: 1982 Color: 0

Bin 177: 89 of cap free
Amount of items: 2
Items: 
Size: 10941 Color: 0
Size: 4602 Color: 1

Bin 178: 91 of cap free
Amount of items: 2
Items: 
Size: 10259 Color: 1
Size: 5282 Color: 0

Bin 179: 91 of cap free
Amount of items: 2
Items: 
Size: 12879 Color: 1
Size: 2662 Color: 0

Bin 180: 92 of cap free
Amount of items: 2
Items: 
Size: 9298 Color: 0
Size: 6242 Color: 1

Bin 181: 102 of cap free
Amount of items: 2
Items: 
Size: 8882 Color: 1
Size: 6648 Color: 0

Bin 182: 106 of cap free
Amount of items: 2
Items: 
Size: 9756 Color: 0
Size: 5770 Color: 1

Bin 183: 114 of cap free
Amount of items: 2
Items: 
Size: 10258 Color: 1
Size: 5260 Color: 0

Bin 184: 116 of cap free
Amount of items: 2
Items: 
Size: 13446 Color: 1
Size: 2070 Color: 0

Bin 185: 118 of cap free
Amount of items: 2
Items: 
Size: 13366 Color: 0
Size: 2148 Color: 1

Bin 186: 122 of cap free
Amount of items: 3
Items: 
Size: 12037 Color: 1
Size: 1956 Color: 0
Size: 1517 Color: 1

Bin 187: 126 of cap free
Amount of items: 2
Items: 
Size: 11738 Color: 1
Size: 3768 Color: 0

Bin 188: 135 of cap free
Amount of items: 2
Items: 
Size: 13125 Color: 1
Size: 2372 Color: 0

Bin 189: 166 of cap free
Amount of items: 2
Items: 
Size: 10818 Color: 1
Size: 4648 Color: 0

Bin 190: 182 of cap free
Amount of items: 2
Items: 
Size: 13102 Color: 1
Size: 2348 Color: 0

Bin 191: 186 of cap free
Amount of items: 38
Items: 
Size: 560 Color: 0
Size: 520 Color: 0
Size: 472 Color: 0
Size: 468 Color: 0
Size: 464 Color: 1
Size: 464 Color: 1
Size: 464 Color: 0
Size: 464 Color: 0
Size: 458 Color: 0
Size: 456 Color: 1
Size: 432 Color: 0
Size: 424 Color: 1
Size: 416 Color: 1
Size: 416 Color: 1
Size: 414 Color: 1
Size: 412 Color: 1
Size: 404 Color: 1
Size: 404 Color: 0
Size: 402 Color: 0
Size: 400 Color: 0
Size: 384 Color: 1
Size: 384 Color: 0
Size: 384 Color: 0
Size: 376 Color: 1
Size: 370 Color: 0
Size: 368 Color: 1
Size: 364 Color: 1
Size: 364 Color: 0
Size: 362 Color: 1
Size: 360 Color: 1
Size: 356 Color: 1
Size: 356 Color: 1
Size: 356 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 348 Color: 1
Size: 344 Color: 1

Bin 192: 192 of cap free
Amount of items: 2
Items: 
Size: 9244 Color: 0
Size: 6196 Color: 1

Bin 193: 202 of cap free
Amount of items: 2
Items: 
Size: 12274 Color: 0
Size: 3156 Color: 1

Bin 194: 205 of cap free
Amount of items: 2
Items: 
Size: 8914 Color: 0
Size: 6513 Color: 1

Bin 195: 212 of cap free
Amount of items: 2
Items: 
Size: 12820 Color: 0
Size: 2600 Color: 1

Bin 196: 224 of cap free
Amount of items: 2
Items: 
Size: 8172 Color: 1
Size: 7236 Color: 0

Bin 197: 226 of cap free
Amount of items: 2
Items: 
Size: 8898 Color: 0
Size: 6508 Color: 1

Bin 198: 228 of cap free
Amount of items: 2
Items: 
Size: 12858 Color: 1
Size: 2546 Color: 0

Bin 199: 9006 of cap free
Amount of items: 22
Items: 
Size: 348 Color: 0
Size: 344 Color: 0
Size: 340 Color: 0
Size: 340 Color: 0
Size: 320 Color: 1
Size: 320 Color: 1
Size: 304 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 302 Color: 0
Size: 296 Color: 1
Size: 292 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 280 Color: 1
Size: 272 Color: 1
Size: 268 Color: 1
Size: 264 Color: 0
Size: 256 Color: 1

Total size: 3095136
Total free space: 15632

