Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 345622 Color: 10
Size: 334511 Color: 4
Size: 319868 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 418135 Color: 18
Size: 287531 Color: 1
Size: 294335 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 456267 Color: 12
Size: 281900 Color: 19
Size: 261834 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 477466 Color: 18
Size: 266069 Color: 13
Size: 256466 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 396215 Color: 1
Size: 324752 Color: 4
Size: 279034 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 495349 Color: 18
Size: 252462 Color: 13
Size: 252190 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 351884 Color: 7
Size: 326135 Color: 5
Size: 321982 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 496299 Color: 2
Size: 252752 Color: 4
Size: 250950 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 435154 Color: 6
Size: 293357 Color: 0
Size: 271490 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392106 Color: 16
Size: 350160 Color: 16
Size: 257735 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 370920 Color: 7
Size: 317000 Color: 15
Size: 312081 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 398303 Color: 17
Size: 324076 Color: 0
Size: 277622 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 362690 Color: 8
Size: 356709 Color: 3
Size: 280602 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 433365 Color: 14
Size: 310228 Color: 12
Size: 256408 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 388563 Color: 9
Size: 354256 Color: 2
Size: 257182 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 315330 Color: 19
Size: 278098 Color: 1
Size: 406573 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 398770 Color: 19
Size: 347283 Color: 12
Size: 253948 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 459277 Color: 17
Size: 290516 Color: 15
Size: 250208 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 405913 Color: 11
Size: 311279 Color: 19
Size: 282809 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 443360 Color: 7
Size: 281921 Color: 8
Size: 274720 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 386919 Color: 16
Size: 341841 Color: 19
Size: 271241 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 463009 Color: 13
Size: 276237 Color: 19
Size: 260755 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 382659 Color: 5
Size: 327582 Color: 10
Size: 289760 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 492057 Color: 2
Size: 257431 Color: 11
Size: 250513 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 378461 Color: 7
Size: 314381 Color: 12
Size: 307159 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 458002 Color: 6
Size: 272292 Color: 2
Size: 269707 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 442269 Color: 16
Size: 297235 Color: 6
Size: 260497 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 488287 Color: 3
Size: 261368 Color: 19
Size: 250346 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 489758 Color: 3
Size: 256296 Color: 19
Size: 253947 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 472604 Color: 6
Size: 266153 Color: 18
Size: 261244 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 402721 Color: 14
Size: 304659 Color: 3
Size: 292621 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 402095 Color: 4
Size: 326974 Color: 4
Size: 270932 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 454316 Color: 18
Size: 279784 Color: 0
Size: 265901 Color: 12

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 491582 Color: 19
Size: 256318 Color: 19
Size: 252101 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 432029 Color: 16
Size: 313358 Color: 2
Size: 254614 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 365011 Color: 3
Size: 360980 Color: 12
Size: 274010 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 385710 Color: 19
Size: 336928 Color: 14
Size: 277363 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 375783 Color: 11
Size: 346701 Color: 14
Size: 277517 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 432137 Color: 5
Size: 291944 Color: 11
Size: 275920 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 349390 Color: 0
Size: 341083 Color: 18
Size: 309528 Color: 14

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 410734 Color: 3
Size: 326406 Color: 6
Size: 262861 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 391048 Color: 17
Size: 331187 Color: 13
Size: 277766 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 367312 Color: 13
Size: 323893 Color: 17
Size: 308796 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 387555 Color: 9
Size: 358585 Color: 17
Size: 253861 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 417711 Color: 5
Size: 292157 Color: 2
Size: 290133 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 386458 Color: 15
Size: 309679 Color: 14
Size: 303864 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 400994 Color: 11
Size: 305602 Color: 9
Size: 293405 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 401725 Color: 5
Size: 338853 Color: 1
Size: 259423 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 346938 Color: 3
Size: 344911 Color: 4
Size: 308152 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 444578 Color: 18
Size: 304015 Color: 9
Size: 251408 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 364261 Color: 18
Size: 346465 Color: 7
Size: 289275 Color: 7

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 425177 Color: 11
Size: 291729 Color: 8
Size: 283095 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 485704 Color: 3
Size: 262341 Color: 19
Size: 251956 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 390277 Color: 13
Size: 317928 Color: 2
Size: 291796 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 468150 Color: 6
Size: 281602 Color: 15
Size: 250249 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 492858 Color: 12
Size: 253802 Color: 11
Size: 253341 Color: 6

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 375261 Color: 14
Size: 357072 Color: 15
Size: 267668 Color: 9

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 426921 Color: 11
Size: 316482 Color: 18
Size: 256598 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 463727 Color: 11
Size: 280319 Color: 9
Size: 255955 Color: 8

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 365045 Color: 9
Size: 359962 Color: 5
Size: 274994 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 381416 Color: 12
Size: 368131 Color: 4
Size: 250454 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 438298 Color: 6
Size: 286011 Color: 5
Size: 275692 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 486125 Color: 5
Size: 257467 Color: 7
Size: 256409 Color: 10

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 474613 Color: 11
Size: 264310 Color: 10
Size: 261078 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 488596 Color: 6
Size: 259265 Color: 17
Size: 252140 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 429912 Color: 10
Size: 292688 Color: 12
Size: 277401 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 418830 Color: 0
Size: 322460 Color: 2
Size: 258711 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 419420 Color: 2
Size: 323057 Color: 9
Size: 257524 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 488585 Color: 0
Size: 257882 Color: 2
Size: 253534 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 494862 Color: 14
Size: 254641 Color: 9
Size: 250498 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 422109 Color: 3
Size: 294266 Color: 15
Size: 283626 Color: 14

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 417593 Color: 1
Size: 315534 Color: 5
Size: 266874 Color: 17

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 466636 Color: 17
Size: 272220 Color: 10
Size: 261145 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 409622 Color: 6
Size: 295790 Color: 17
Size: 294589 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 479627 Color: 18
Size: 268941 Color: 16
Size: 251433 Color: 13

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 400209 Color: 1
Size: 346284 Color: 5
Size: 253508 Color: 6

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 399512 Color: 18
Size: 338237 Color: 0
Size: 262252 Color: 8

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 388857 Color: 6
Size: 335694 Color: 7
Size: 275450 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 459082 Color: 17
Size: 283925 Color: 11
Size: 256994 Color: 11

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 401161 Color: 9
Size: 307044 Color: 2
Size: 291796 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 367204 Color: 1
Size: 339200 Color: 8
Size: 293597 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 372435 Color: 14
Size: 331546 Color: 10
Size: 296020 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 484437 Color: 8
Size: 258763 Color: 3
Size: 256801 Color: 14

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 358469 Color: 10
Size: 329693 Color: 16
Size: 311839 Color: 14

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 381071 Color: 19
Size: 323651 Color: 8
Size: 295279 Color: 12

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 374021 Color: 17
Size: 345866 Color: 7
Size: 280114 Color: 8

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 391520 Color: 4
Size: 349010 Color: 13
Size: 259471 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 368105 Color: 18
Size: 330267 Color: 19
Size: 301629 Color: 9

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 414992 Color: 15
Size: 303104 Color: 13
Size: 281905 Color: 19

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 452671 Color: 10
Size: 280584 Color: 10
Size: 266746 Color: 9

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 496569 Color: 3
Size: 252170 Color: 15
Size: 251262 Color: 6

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 461461 Color: 3
Size: 272684 Color: 6
Size: 265856 Color: 12

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 363392 Color: 3
Size: 333042 Color: 2
Size: 303567 Color: 16

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 478534 Color: 1
Size: 267127 Color: 12
Size: 254340 Color: 14

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 480726 Color: 9
Size: 267944 Color: 15
Size: 251331 Color: 7

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 450777 Color: 13
Size: 277116 Color: 18
Size: 272108 Color: 8

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 418649 Color: 1
Size: 304849 Color: 14
Size: 276503 Color: 10

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 425358 Color: 14
Size: 308954 Color: 14
Size: 265689 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 423399 Color: 1
Size: 322785 Color: 18
Size: 253817 Color: 12

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 387199 Color: 9
Size: 335186 Color: 7
Size: 277616 Color: 16

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 489243 Color: 17
Size: 256357 Color: 19
Size: 254401 Color: 15

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 433079 Color: 16
Size: 316001 Color: 0
Size: 250921 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 423001 Color: 6
Size: 321868 Color: 12
Size: 255132 Color: 15

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 354425 Color: 3
Size: 323657 Color: 11
Size: 321919 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 415022 Color: 11
Size: 308136 Color: 6
Size: 276843 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 390823 Color: 1
Size: 333810 Color: 12
Size: 275368 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 413604 Color: 18
Size: 314093 Color: 12
Size: 272304 Color: 5

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 399319 Color: 8
Size: 317166 Color: 16
Size: 283516 Color: 7

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 404304 Color: 9
Size: 315014 Color: 12
Size: 280683 Color: 15

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 368052 Color: 15
Size: 341514 Color: 11
Size: 290435 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 468910 Color: 16
Size: 269298 Color: 3
Size: 261793 Color: 14

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 454150 Color: 19
Size: 285050 Color: 19
Size: 260801 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 392383 Color: 0
Size: 342869 Color: 16
Size: 264749 Color: 12

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 384813 Color: 16
Size: 333782 Color: 18
Size: 281406 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 407903 Color: 4
Size: 325020 Color: 14
Size: 267078 Color: 18

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 359634 Color: 4
Size: 320550 Color: 6
Size: 319817 Color: 9

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 380148 Color: 4
Size: 328576 Color: 9
Size: 291277 Color: 7

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 343535 Color: 19
Size: 339532 Color: 13
Size: 316934 Color: 18

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 433060 Color: 15
Size: 314116 Color: 5
Size: 252825 Color: 8

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 444260 Color: 3
Size: 302352 Color: 2
Size: 253389 Color: 14

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 363963 Color: 11
Size: 333892 Color: 11
Size: 302146 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 428317 Color: 13
Size: 311963 Color: 12
Size: 259721 Color: 14

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 492500 Color: 14
Size: 256881 Color: 4
Size: 250620 Color: 5

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 390027 Color: 17
Size: 343780 Color: 8
Size: 266194 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 344284 Color: 12
Size: 341102 Color: 6
Size: 314615 Color: 7

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 377862 Color: 5
Size: 349788 Color: 15
Size: 272351 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 475181 Color: 18
Size: 263582 Color: 0
Size: 261238 Color: 15

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 388537 Color: 16
Size: 316462 Color: 16
Size: 295002 Color: 12

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 431013 Color: 3
Size: 309248 Color: 18
Size: 259740 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 436194 Color: 16
Size: 312675 Color: 1
Size: 251132 Color: 11

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 452156 Color: 18
Size: 292294 Color: 3
Size: 255551 Color: 9

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 458835 Color: 10
Size: 278886 Color: 14
Size: 262280 Color: 7

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 429498 Color: 18
Size: 306117 Color: 6
Size: 264386 Color: 14

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 416284 Color: 1
Size: 330101 Color: 0
Size: 253616 Color: 19

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 391574 Color: 2
Size: 336770 Color: 4
Size: 271657 Color: 7

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 345497 Color: 5
Size: 332403 Color: 10
Size: 322101 Color: 10

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 424859 Color: 12
Size: 306522 Color: 18
Size: 268620 Color: 18

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 395494 Color: 9
Size: 351057 Color: 13
Size: 253450 Color: 4

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 383966 Color: 10
Size: 311544 Color: 1
Size: 304491 Color: 14

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 461790 Color: 5
Size: 278058 Color: 12
Size: 260153 Color: 8

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 393134 Color: 18
Size: 318065 Color: 12
Size: 288802 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 441442 Color: 2
Size: 284249 Color: 15
Size: 274310 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 416460 Color: 5
Size: 327005 Color: 11
Size: 256536 Color: 12

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 378558 Color: 2
Size: 331359 Color: 0
Size: 290084 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 491210 Color: 18
Size: 258243 Color: 7
Size: 250548 Color: 17

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 382485 Color: 13
Size: 312996 Color: 10
Size: 304520 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 491992 Color: 15
Size: 254362 Color: 5
Size: 253647 Color: 5

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 437680 Color: 17
Size: 311659 Color: 7
Size: 250662 Color: 10

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 367010 Color: 16
Size: 347793 Color: 15
Size: 285198 Color: 10

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 470811 Color: 0
Size: 275867 Color: 0
Size: 253323 Color: 2

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 410842 Color: 9
Size: 322647 Color: 10
Size: 266512 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 458212 Color: 14
Size: 275255 Color: 0
Size: 266534 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 364137 Color: 5
Size: 353411 Color: 13
Size: 282453 Color: 3

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 403322 Color: 12
Size: 318811 Color: 6
Size: 277868 Color: 16

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 345681 Color: 14
Size: 329984 Color: 10
Size: 324336 Color: 5

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 379880 Color: 0
Size: 349609 Color: 7
Size: 270512 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 489393 Color: 11
Size: 258275 Color: 18
Size: 252333 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 412740 Color: 0
Size: 308380 Color: 17
Size: 278881 Color: 9

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 443916 Color: 4
Size: 294287 Color: 14
Size: 261798 Color: 10

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 431506 Color: 1
Size: 295205 Color: 16
Size: 273290 Color: 14

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 390878 Color: 9
Size: 350615 Color: 3
Size: 258508 Color: 16

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 425347 Color: 7
Size: 322792 Color: 17
Size: 251862 Color: 6

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 454955 Color: 8
Size: 283555 Color: 15
Size: 261491 Color: 6

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 401296 Color: 0
Size: 336020 Color: 17
Size: 262685 Color: 9

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 367190 Color: 7
Size: 340791 Color: 11
Size: 292020 Color: 14

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 371329 Color: 19
Size: 368813 Color: 10
Size: 259859 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 386900 Color: 12
Size: 321163 Color: 10
Size: 291938 Color: 5

Total size: 167000167
Total free space: 0

