Capicity Bin: 7760
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 31
Items: 
Size: 424 Color: 1
Size: 424 Color: 1
Size: 376 Color: 1
Size: 312 Color: 1
Size: 304 Color: 1
Size: 282 Color: 0
Size: 276 Color: 0
Size: 272 Color: 0
Size: 272 Color: 0
Size: 272 Color: 0
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0
Size: 248 Color: 1
Size: 248 Color: 1
Size: 246 Color: 0
Size: 228 Color: 1
Size: 228 Color: 1
Size: 228 Color: 0
Size: 228 Color: 0
Size: 224 Color: 0
Size: 224 Color: 0
Size: 194 Color: 1
Size: 192 Color: 1
Size: 192 Color: 0
Size: 192 Color: 0
Size: 184 Color: 1
Size: 178 Color: 0
Size: 144 Color: 1
Size: 144 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 3892 Color: 0
Size: 690 Color: 1
Size: 678 Color: 1
Size: 664 Color: 1
Size: 644 Color: 0
Size: 640 Color: 1
Size: 552 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 1
Size: 2608 Color: 1
Size: 156 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5064 Color: 1
Size: 2488 Color: 0
Size: 208 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5502 Color: 1
Size: 2122 Color: 0
Size: 136 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5721 Color: 1
Size: 1701 Color: 1
Size: 338 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 1
Size: 1726 Color: 1
Size: 182 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5929 Color: 0
Size: 1579 Color: 0
Size: 252 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6040 Color: 1
Size: 1608 Color: 0
Size: 112 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5998 Color: 0
Size: 1538 Color: 0
Size: 224 Color: 1

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 6574 Color: 0
Size: 1008 Color: 1
Size: 128 Color: 0
Size: 50 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 1
Size: 650 Color: 0
Size: 480 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 1
Size: 718 Color: 1
Size: 382 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 0
Size: 930 Color: 1
Size: 150 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6672 Color: 1
Size: 640 Color: 0
Size: 448 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 1
Size: 560 Color: 0
Size: 456 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6955 Color: 1
Size: 671 Color: 1
Size: 134 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 1
Size: 2799 Color: 0
Size: 558 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 4434 Color: 1
Size: 2781 Color: 0
Size: 544 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 0
Size: 2732 Color: 1
Size: 172 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 5097 Color: 1
Size: 2438 Color: 0
Size: 224 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 1
Size: 1596 Color: 0
Size: 258 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 0
Size: 1289 Color: 1
Size: 250 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 6389 Color: 1
Size: 1370 Color: 0

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 6597 Color: 1
Size: 1162 Color: 0

Bin 26: 1 of cap free
Amount of items: 4
Items: 
Size: 6687 Color: 1
Size: 924 Color: 0
Size: 132 Color: 0
Size: 16 Color: 1

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 1
Size: 915 Color: 0

Bin 28: 2 of cap free
Amount of items: 7
Items: 
Size: 3881 Color: 1
Size: 1146 Color: 0
Size: 759 Color: 1
Size: 756 Color: 1
Size: 646 Color: 0
Size: 410 Color: 0
Size: 160 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 4328 Color: 1
Size: 3226 Color: 0
Size: 204 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 4838 Color: 0
Size: 2774 Color: 1
Size: 146 Color: 0

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 0
Size: 1200 Color: 1
Size: 358 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 6215 Color: 1
Size: 881 Color: 1
Size: 662 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 0
Size: 1192 Color: 1
Size: 196 Color: 1

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 0
Size: 1372 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 0
Size: 895 Color: 1
Size: 376 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 1
Size: 558 Color: 0
Size: 556 Color: 0

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 6902 Color: 1
Size: 856 Color: 0

Bin 38: 2 of cap free
Amount of items: 4
Items: 
Size: 6982 Color: 1
Size: 744 Color: 0
Size: 16 Color: 1
Size: 16 Color: 0

Bin 39: 3 of cap free
Amount of items: 10
Items: 
Size: 3885 Color: 0
Size: 556 Color: 1
Size: 556 Color: 1
Size: 496 Color: 1
Size: 494 Color: 1
Size: 484 Color: 1
Size: 368 Color: 0
Size: 322 Color: 0
Size: 320 Color: 0
Size: 276 Color: 0

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 5605 Color: 1
Size: 1964 Color: 0
Size: 188 Color: 1

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 1
Size: 1556 Color: 0
Size: 304 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 0
Size: 1284 Color: 1
Size: 208 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 1
Size: 1134 Color: 0
Size: 240 Color: 0

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 6714 Color: 0
Size: 1043 Color: 1

Bin 45: 4 of cap free
Amount of items: 11
Items: 
Size: 3884 Color: 0
Size: 484 Color: 1
Size: 484 Color: 1
Size: 464 Color: 1
Size: 456 Color: 1
Size: 448 Color: 1
Size: 400 Color: 1
Size: 320 Color: 0
Size: 312 Color: 0
Size: 296 Color: 0
Size: 208 Color: 0

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 4980 Color: 1
Size: 2776 Color: 0

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 0
Size: 2422 Color: 0
Size: 132 Color: 1

Bin 48: 4 of cap free
Amount of items: 3
Items: 
Size: 5214 Color: 1
Size: 2410 Color: 0
Size: 132 Color: 1

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 1
Size: 1572 Color: 0
Size: 442 Color: 0

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 1
Size: 821 Color: 0
Size: 733 Color: 1

Bin 51: 4 of cap free
Amount of items: 2
Items: 
Size: 6509 Color: 1
Size: 1247 Color: 0

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 6694 Color: 0
Size: 1062 Color: 1

Bin 53: 5 of cap free
Amount of items: 3
Items: 
Size: 5490 Color: 0
Size: 2053 Color: 1
Size: 212 Color: 1

Bin 54: 5 of cap free
Amount of items: 2
Items: 
Size: 6097 Color: 1
Size: 1658 Color: 0

Bin 55: 5 of cap free
Amount of items: 2
Items: 
Size: 6851 Color: 1
Size: 904 Color: 0

Bin 56: 5 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 1
Size: 874 Color: 0

Bin 57: 6 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 0
Size: 2786 Color: 0
Size: 288 Color: 1

Bin 58: 6 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 1894 Color: 0
Size: 392 Color: 0

Bin 59: 6 of cap free
Amount of items: 2
Items: 
Size: 5774 Color: 1
Size: 1980 Color: 0

Bin 60: 6 of cap free
Amount of items: 2
Items: 
Size: 5962 Color: 1
Size: 1792 Color: 0

Bin 61: 7 of cap free
Amount of items: 4
Items: 
Size: 3882 Color: 1
Size: 2797 Color: 0
Size: 890 Color: 1
Size: 184 Color: 0

Bin 62: 7 of cap free
Amount of items: 2
Items: 
Size: 6156 Color: 1
Size: 1597 Color: 0

Bin 63: 7 of cap free
Amount of items: 2
Items: 
Size: 6492 Color: 1
Size: 1261 Color: 0

Bin 64: 7 of cap free
Amount of items: 2
Items: 
Size: 6663 Color: 1
Size: 1090 Color: 0

Bin 65: 7 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 0
Size: 785 Color: 1

Bin 66: 7 of cap free
Amount of items: 4
Items: 
Size: 6966 Color: 1
Size: 731 Color: 0
Size: 32 Color: 0
Size: 24 Color: 1

Bin 67: 7 of cap free
Amount of items: 2
Items: 
Size: 6984 Color: 1
Size: 769 Color: 0

Bin 68: 8 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 0
Size: 2740 Color: 1
Size: 236 Color: 1

Bin 69: 8 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 0
Size: 1876 Color: 1
Size: 118 Color: 1

Bin 70: 9 of cap free
Amount of items: 2
Items: 
Size: 6249 Color: 1
Size: 1502 Color: 0

Bin 71: 9 of cap free
Amount of items: 2
Items: 
Size: 6703 Color: 0
Size: 1048 Color: 1

Bin 72: 9 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 0
Size: 973 Color: 1

Bin 73: 10 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 0
Size: 816 Color: 1

Bin 74: 11 of cap free
Amount of items: 3
Items: 
Size: 5297 Color: 0
Size: 2308 Color: 0
Size: 144 Color: 1

Bin 75: 11 of cap free
Amount of items: 2
Items: 
Size: 6600 Color: 0
Size: 1149 Color: 1

Bin 76: 11 of cap free
Amount of items: 2
Items: 
Size: 6688 Color: 1
Size: 1061 Color: 0

Bin 77: 13 of cap free
Amount of items: 2
Items: 
Size: 6819 Color: 1
Size: 928 Color: 0

Bin 78: 15 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 0
Size: 2421 Color: 0
Size: 352 Color: 1

Bin 79: 15 of cap free
Amount of items: 3
Items: 
Size: 5304 Color: 0
Size: 1797 Color: 1
Size: 644 Color: 1

Bin 80: 16 of cap free
Amount of items: 2
Items: 
Size: 5730 Color: 1
Size: 2014 Color: 0

Bin 81: 17 of cap free
Amount of items: 2
Items: 
Size: 6322 Color: 0
Size: 1421 Color: 1

Bin 82: 18 of cap free
Amount of items: 2
Items: 
Size: 4870 Color: 0
Size: 2872 Color: 1

Bin 83: 19 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 1
Size: 1913 Color: 0

Bin 84: 20 of cap free
Amount of items: 2
Items: 
Size: 6918 Color: 1
Size: 822 Color: 0

Bin 85: 21 of cap free
Amount of items: 2
Items: 
Size: 4880 Color: 1
Size: 2859 Color: 0

Bin 86: 22 of cap free
Amount of items: 2
Items: 
Size: 6967 Color: 0
Size: 771 Color: 1

Bin 87: 23 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 1
Size: 2248 Color: 0
Size: 1593 Color: 1

Bin 88: 23 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 0
Size: 1621 Color: 1

Bin 89: 24 of cap free
Amount of items: 3
Items: 
Size: 3898 Color: 0
Size: 3136 Color: 0
Size: 702 Color: 1

Bin 90: 24 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 0
Size: 1143 Color: 1

Bin 91: 24 of cap free
Amount of items: 2
Items: 
Size: 6872 Color: 1
Size: 864 Color: 0

Bin 92: 25 of cap free
Amount of items: 2
Items: 
Size: 5921 Color: 0
Size: 1814 Color: 1

Bin 93: 25 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 1
Size: 951 Color: 0

Bin 94: 26 of cap free
Amount of items: 2
Items: 
Size: 6118 Color: 1
Size: 1616 Color: 0

Bin 95: 27 of cap free
Amount of items: 2
Items: 
Size: 5465 Color: 1
Size: 2268 Color: 0

Bin 96: 27 of cap free
Amount of items: 3
Items: 
Size: 6057 Color: 1
Size: 1612 Color: 0
Size: 64 Color: 1

Bin 97: 27 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 0
Size: 971 Color: 1

Bin 98: 28 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 0
Size: 1242 Color: 1

Bin 99: 29 of cap free
Amount of items: 2
Items: 
Size: 6344 Color: 0
Size: 1387 Color: 1

Bin 100: 32 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 1
Size: 2324 Color: 0

Bin 101: 33 of cap free
Amount of items: 2
Items: 
Size: 6759 Color: 0
Size: 968 Color: 1

Bin 102: 34 of cap free
Amount of items: 8
Items: 
Size: 3890 Color: 0
Size: 664 Color: 1
Size: 644 Color: 1
Size: 640 Color: 1
Size: 640 Color: 0
Size: 544 Color: 0
Size: 376 Color: 0
Size: 328 Color: 1

Bin 103: 36 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 1
Size: 864 Color: 0

Bin 104: 38 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 0
Size: 1202 Color: 1

Bin 105: 39 of cap free
Amount of items: 2
Items: 
Size: 5500 Color: 0
Size: 2221 Color: 1

Bin 106: 41 of cap free
Amount of items: 2
Items: 
Size: 6730 Color: 1
Size: 989 Color: 0

Bin 107: 41 of cap free
Amount of items: 2
Items: 
Size: 6885 Color: 0
Size: 834 Color: 1

Bin 108: 42 of cap free
Amount of items: 2
Items: 
Size: 6270 Color: 0
Size: 1448 Color: 1

Bin 109: 43 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 0
Size: 3233 Color: 1

Bin 110: 43 of cap free
Amount of items: 2
Items: 
Size: 6775 Color: 1
Size: 942 Color: 0

Bin 111: 44 of cap free
Amount of items: 2
Items: 
Size: 5832 Color: 0
Size: 1884 Color: 1

Bin 112: 44 of cap free
Amount of items: 2
Items: 
Size: 5900 Color: 1
Size: 1816 Color: 0

Bin 113: 46 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 1
Size: 2134 Color: 0
Size: 64 Color: 0

Bin 114: 46 of cap free
Amount of items: 2
Items: 
Size: 6950 Color: 0
Size: 764 Color: 1

Bin 115: 48 of cap free
Amount of items: 2
Items: 
Size: 5592 Color: 0
Size: 2120 Color: 1

Bin 116: 52 of cap free
Amount of items: 2
Items: 
Size: 6404 Color: 1
Size: 1304 Color: 0

Bin 117: 53 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 0
Size: 1731 Color: 1
Size: 932 Color: 1

Bin 118: 53 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 0
Size: 1132 Color: 1

Bin 119: 54 of cap free
Amount of items: 2
Items: 
Size: 6646 Color: 0
Size: 1060 Color: 1

Bin 120: 56 of cap free
Amount of items: 2
Items: 
Size: 4476 Color: 0
Size: 3228 Color: 1

Bin 121: 56 of cap free
Amount of items: 2
Items: 
Size: 6402 Color: 1
Size: 1302 Color: 0

Bin 122: 61 of cap free
Amount of items: 2
Items: 
Size: 5817 Color: 0
Size: 1882 Color: 1

Bin 123: 61 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 1
Size: 862 Color: 0

Bin 124: 64 of cap free
Amount of items: 3
Items: 
Size: 5576 Color: 0
Size: 2056 Color: 1
Size: 64 Color: 0

Bin 125: 76 of cap free
Amount of items: 2
Items: 
Size: 5360 Color: 0
Size: 2324 Color: 1

Bin 126: 101 of cap free
Amount of items: 2
Items: 
Size: 4423 Color: 1
Size: 3236 Color: 0

Bin 127: 104 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 0
Size: 2802 Color: 1

Bin 128: 108 of cap free
Amount of items: 2
Items: 
Size: 4418 Color: 1
Size: 3234 Color: 0

Bin 129: 118 of cap free
Amount of items: 2
Items: 
Size: 4411 Color: 1
Size: 3231 Color: 0

Bin 130: 124 of cap free
Amount of items: 2
Items: 
Size: 4845 Color: 0
Size: 2791 Color: 1

Bin 131: 129 of cap free
Amount of items: 2
Items: 
Size: 4407 Color: 0
Size: 3224 Color: 1

Bin 132: 135 of cap free
Amount of items: 2
Items: 
Size: 4403 Color: 0
Size: 3222 Color: 1

Bin 133: 4936 of cap free
Amount of items: 18
Items: 
Size: 176 Color: 1
Size: 176 Color: 1
Size: 176 Color: 1
Size: 176 Color: 0
Size: 176 Color: 0
Size: 168 Color: 0
Size: 164 Color: 0
Size: 164 Color: 0
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 1
Size: 152 Color: 0
Size: 152 Color: 0
Size: 140 Color: 0
Size: 140 Color: 0
Size: 128 Color: 1
Size: 96 Color: 1

Total size: 1024320
Total free space: 7760

