Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 77
Size: 367 Color: 71
Size: 254 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 83
Size: 335 Color: 58
Size: 260 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 95
Size: 278 Color: 29
Size: 264 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 97
Size: 272 Color: 25
Size: 253 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 101
Size: 254 Color: 7
Size: 250 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 87
Size: 314 Color: 47
Size: 268 Color: 22

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 89
Size: 320 Color: 50
Size: 253 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 80
Size: 306 Color: 43
Size: 294 Color: 37

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 79
Size: 317 Color: 48
Size: 297 Color: 39

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 78
Size: 365 Color: 70
Size: 255 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 75
Size: 346 Color: 64
Size: 276 Color: 28

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 88
Size: 318 Color: 49
Size: 258 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 86
Size: 327 Color: 51
Size: 256 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 60
Size: 334 Color: 57
Size: 330 Color: 54

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 99
Size: 270 Color: 24
Size: 253 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 94
Size: 282 Color: 31
Size: 260 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 312 Color: 46
Size: 281 Color: 30
Size: 408 Color: 84

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 61
Size: 335 Color: 59
Size: 329 Color: 53

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 74
Size: 353 Color: 67
Size: 273 Color: 26

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 93
Size: 289 Color: 34
Size: 263 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 91
Size: 304 Color: 40
Size: 260 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 69
Size: 349 Color: 65
Size: 292 Color: 36

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 76
Size: 354 Color: 68
Size: 268 Color: 23

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 73
Size: 341 Color: 62
Size: 291 Color: 35

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 92
Size: 305 Color: 42
Size: 250 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 82
Size: 331 Color: 55
Size: 265 Color: 21

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 81
Size: 311 Color: 45
Size: 286 Color: 33

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 98
Size: 264 Color: 20
Size: 259 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 85
Size: 332 Color: 56
Size: 255 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 72
Size: 328 Color: 52
Size: 304 Color: 41

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 100
Size: 259 Color: 13
Size: 251 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 66
Size: 343 Color: 63
Size: 308 Color: 44

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 90
Size: 294 Color: 38
Size: 276 Color: 27

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 96
Size: 282 Color: 32
Size: 251 Color: 3

Total size: 34034
Total free space: 0

