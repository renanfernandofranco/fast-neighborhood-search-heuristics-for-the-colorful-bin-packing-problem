Capicity Bin: 1000001
Lower Bound: 4533

Bins used: 4543
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 601994 Color: 0
Size: 199574 Color: 2
Size: 198433 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 724177 Color: 1
Size: 138955 Color: 2
Size: 136869 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 768712 Color: 1
Size: 119966 Color: 3
Size: 111323 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 786963 Color: 1
Size: 109274 Color: 1
Size: 103764 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 765357 Color: 4
Size: 123231 Color: 0
Size: 111413 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 750858 Color: 4
Size: 127501 Color: 2
Size: 121642 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 786334 Color: 3
Size: 107797 Color: 3
Size: 105870 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 777865 Color: 0
Size: 119162 Color: 3
Size: 102974 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 764741 Color: 1
Size: 121953 Color: 1
Size: 113307 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 759542 Color: 0
Size: 137243 Color: 2
Size: 103216 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 768745 Color: 2
Size: 116191 Color: 3
Size: 115065 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 786327 Color: 4
Size: 113358 Color: 0
Size: 100316 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 645253 Color: 3
Size: 177890 Color: 4
Size: 176858 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 750976 Color: 1
Size: 128053 Color: 2
Size: 120972 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 667237 Color: 3
Size: 167276 Color: 4
Size: 165488 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 676676 Color: 1
Size: 163508 Color: 3
Size: 159817 Color: 3

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 683278 Color: 0
Size: 316723 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 631522 Color: 3
Size: 184248 Color: 3
Size: 184231 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 757381 Color: 0
Size: 136410 Color: 1
Size: 106210 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 773466 Color: 3
Size: 113486 Color: 3
Size: 113049 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 705181 Color: 0
Size: 189177 Color: 1
Size: 105643 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 368302 Color: 0
Size: 353389 Color: 3
Size: 278310 Color: 1

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 567832 Color: 0
Size: 432169 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 599598 Color: 2
Size: 202006 Color: 0
Size: 198397 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 732352 Color: 3
Size: 140663 Color: 0
Size: 126986 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 759515 Color: 4
Size: 121426 Color: 0
Size: 119060 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 752167 Color: 3
Size: 129829 Color: 0
Size: 118005 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 779274 Color: 4
Size: 112392 Color: 1
Size: 108335 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 611062 Color: 2
Size: 194900 Color: 1
Size: 194039 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 634698 Color: 0
Size: 182707 Color: 3
Size: 182596 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 742527 Color: 2
Size: 152631 Color: 3
Size: 104843 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 767891 Color: 0
Size: 125520 Color: 1
Size: 106590 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 764231 Color: 1
Size: 129749 Color: 1
Size: 106021 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 757011 Color: 3
Size: 125237 Color: 2
Size: 117753 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 775614 Color: 1
Size: 113564 Color: 3
Size: 110823 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 703035 Color: 1
Size: 160238 Color: 3
Size: 136728 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 686031 Color: 3
Size: 158470 Color: 1
Size: 155500 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 703218 Color: 0
Size: 149800 Color: 3
Size: 146983 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 754871 Color: 0
Size: 123816 Color: 2
Size: 121314 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 761914 Color: 0
Size: 136063 Color: 4
Size: 102024 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 731836 Color: 4
Size: 135276 Color: 1
Size: 132889 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 763852 Color: 1
Size: 129149 Color: 4
Size: 107000 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 789182 Color: 0
Size: 107247 Color: 2
Size: 103572 Color: 1

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 749057 Color: 0
Size: 250944 Color: 1

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 504068 Color: 2
Size: 495933 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 746582 Color: 0
Size: 126866 Color: 0
Size: 126553 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 778990 Color: 3
Size: 118019 Color: 1
Size: 102992 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 780252 Color: 1
Size: 117057 Color: 4
Size: 102692 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 738947 Color: 3
Size: 136431 Color: 0
Size: 124623 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 783501 Color: 4
Size: 111528 Color: 4
Size: 104972 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 750470 Color: 2
Size: 132682 Color: 3
Size: 116849 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 789890 Color: 3
Size: 109516 Color: 2
Size: 100595 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 738554 Color: 2
Size: 131325 Color: 4
Size: 130122 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 761396 Color: 1
Size: 129476 Color: 4
Size: 109129 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 756847 Color: 4
Size: 137024 Color: 0
Size: 106130 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 626033 Color: 2
Size: 187042 Color: 2
Size: 186926 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 708301 Color: 2
Size: 149537 Color: 0
Size: 142163 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 769015 Color: 0
Size: 120198 Color: 2
Size: 110788 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 752782 Color: 1
Size: 125751 Color: 2
Size: 121468 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 768255 Color: 1
Size: 126537 Color: 4
Size: 105209 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 780536 Color: 1
Size: 117663 Color: 4
Size: 101802 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 717168 Color: 0
Size: 168668 Color: 2
Size: 114165 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 772754 Color: 4
Size: 121114 Color: 2
Size: 106133 Color: 3

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 609295 Color: 2
Size: 390706 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 713944 Color: 2
Size: 147946 Color: 0
Size: 138111 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 692997 Color: 3
Size: 307004 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 729275 Color: 2
Size: 153730 Color: 4
Size: 116996 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 785185 Color: 1
Size: 112999 Color: 3
Size: 101817 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 781820 Color: 3
Size: 113542 Color: 4
Size: 104639 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 596543 Color: 3
Size: 204763 Color: 4
Size: 198695 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 749687 Color: 3
Size: 131233 Color: 0
Size: 119081 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 660729 Color: 1
Size: 169749 Color: 4
Size: 169523 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 725984 Color: 1
Size: 172363 Color: 1
Size: 101654 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 746565 Color: 1
Size: 129659 Color: 3
Size: 123777 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 793681 Color: 0
Size: 104078 Color: 4
Size: 102242 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 749968 Color: 4
Size: 147675 Color: 4
Size: 102358 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 745835 Color: 2
Size: 135324 Color: 3
Size: 118842 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 742319 Color: 2
Size: 138226 Color: 3
Size: 119456 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 779627 Color: 4
Size: 118222 Color: 2
Size: 102152 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 393951 Color: 1
Size: 349699 Color: 2
Size: 256351 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 739297 Color: 4
Size: 150551 Color: 1
Size: 110153 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 788124 Color: 0
Size: 110147 Color: 3
Size: 101730 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 781986 Color: 3
Size: 111188 Color: 2
Size: 106827 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 706004 Color: 2
Size: 147048 Color: 1
Size: 146949 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 712620 Color: 2
Size: 147785 Color: 2
Size: 139596 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 763929 Color: 3
Size: 126067 Color: 1
Size: 110005 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 754864 Color: 4
Size: 124216 Color: 0
Size: 120921 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 751927 Color: 4
Size: 128552 Color: 0
Size: 119522 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 683045 Color: 4
Size: 158654 Color: 1
Size: 158302 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 727586 Color: 3
Size: 137714 Color: 0
Size: 134701 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 741845 Color: 2
Size: 148819 Color: 4
Size: 109337 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 786240 Color: 4
Size: 112349 Color: 1
Size: 101412 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 791329 Color: 1
Size: 107216 Color: 3
Size: 101456 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 744366 Color: 4
Size: 132023 Color: 0
Size: 123612 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 761105 Color: 2
Size: 126864 Color: 2
Size: 112032 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 769847 Color: 4
Size: 126147 Color: 4
Size: 104007 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 786596 Color: 0
Size: 108001 Color: 2
Size: 105404 Color: 2

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 715109 Color: 2
Size: 284892 Color: 4

Bin 99: 0 of cap free
Amount of items: 4
Items: 
Size: 395224 Color: 1
Size: 341100 Color: 1
Size: 138874 Color: 0
Size: 124803 Color: 4

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 584544 Color: 0
Size: 415457 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 725195 Color: 4
Size: 149205 Color: 2
Size: 125601 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 784001 Color: 2
Size: 113787 Color: 1
Size: 102213 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 640679 Color: 2
Size: 179722 Color: 1
Size: 179600 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 698151 Color: 1
Size: 151135 Color: 0
Size: 150715 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 731239 Color: 3
Size: 135538 Color: 0
Size: 133224 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 747267 Color: 1
Size: 136921 Color: 2
Size: 115813 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 721763 Color: 4
Size: 141973 Color: 2
Size: 136265 Color: 2

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 655702 Color: 0
Size: 344299 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 784158 Color: 0
Size: 110332 Color: 1
Size: 105511 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 799307 Color: 4
Size: 100665 Color: 2
Size: 100029 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 724591 Color: 1
Size: 137974 Color: 3
Size: 137436 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 376818 Color: 3
Size: 351130 Color: 3
Size: 272053 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 795624 Color: 4
Size: 104085 Color: 4
Size: 100292 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 788859 Color: 0
Size: 109280 Color: 4
Size: 101862 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 673290 Color: 0
Size: 173544 Color: 3
Size: 153167 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 584141 Color: 2
Size: 258465 Color: 3
Size: 157395 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 785526 Color: 0
Size: 108445 Color: 4
Size: 106030 Color: 4

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 552800 Color: 3
Size: 447201 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 747720 Color: 4
Size: 133546 Color: 2
Size: 118735 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 751726 Color: 3
Size: 126473 Color: 0
Size: 121802 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 749390 Color: 2
Size: 132010 Color: 1
Size: 118601 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 766072 Color: 0
Size: 126175 Color: 2
Size: 107754 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 774702 Color: 0
Size: 118702 Color: 4
Size: 106597 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 778597 Color: 4
Size: 119328 Color: 4
Size: 102076 Color: 2

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 783967 Color: 1
Size: 216034 Color: 3

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 785304 Color: 1
Size: 107884 Color: 1
Size: 106813 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 627927 Color: 3
Size: 187360 Color: 0
Size: 184714 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 729241 Color: 3
Size: 141882 Color: 1
Size: 128878 Color: 0

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 743256 Color: 3
Size: 256745 Color: 2

Bin 130: 0 of cap free
Amount of items: 4
Items: 
Size: 575054 Color: 3
Size: 212588 Color: 0
Size: 111101 Color: 4
Size: 101258 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 751801 Color: 4
Size: 144205 Color: 2
Size: 103995 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 721909 Color: 1
Size: 174167 Color: 2
Size: 103925 Color: 4

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 798705 Color: 3
Size: 100852 Color: 4
Size: 100444 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 785576 Color: 0
Size: 108604 Color: 4
Size: 105821 Color: 0

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 771459 Color: 1
Size: 228542 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 776059 Color: 3
Size: 112105 Color: 1
Size: 111837 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 784841 Color: 1
Size: 111783 Color: 2
Size: 103377 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 584057 Color: 1
Size: 209195 Color: 3
Size: 206749 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 519545 Color: 1
Size: 240696 Color: 1
Size: 239760 Color: 4

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 600489 Color: 0
Size: 399512 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 744258 Color: 3
Size: 137483 Color: 0
Size: 118260 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 771544 Color: 3
Size: 114340 Color: 1
Size: 114117 Color: 0

Bin 143: 0 of cap free
Amount of items: 4
Items: 
Size: 598341 Color: 1
Size: 199160 Color: 4
Size: 101311 Color: 2
Size: 101189 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 743573 Color: 1
Size: 128554 Color: 1
Size: 127874 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 767859 Color: 2
Size: 127792 Color: 3
Size: 104350 Color: 3

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 574208 Color: 4
Size: 425793 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 771816 Color: 0
Size: 117733 Color: 2
Size: 110452 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 760251 Color: 2
Size: 139019 Color: 2
Size: 100731 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 583893 Color: 0
Size: 255856 Color: 1
Size: 160252 Color: 0

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 714571 Color: 0
Size: 285430 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 649434 Color: 4
Size: 200740 Color: 3
Size: 149827 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 692339 Color: 2
Size: 178247 Color: 1
Size: 129415 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 714952 Color: 4
Size: 154071 Color: 0
Size: 130978 Color: 4

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 739087 Color: 0
Size: 260914 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 729516 Color: 3
Size: 145184 Color: 4
Size: 125301 Color: 2

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 798692 Color: 3
Size: 201309 Color: 2

Bin 157: 0 of cap free
Amount of items: 4
Items: 
Size: 573725 Color: 2
Size: 213526 Color: 1
Size: 109726 Color: 0
Size: 103024 Color: 3

Bin 158: 0 of cap free
Amount of items: 4
Items: 
Size: 585403 Color: 4
Size: 207263 Color: 3
Size: 105626 Color: 4
Size: 101709 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 710748 Color: 3
Size: 179680 Color: 2
Size: 109573 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 742692 Color: 3
Size: 131175 Color: 2
Size: 126134 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 378757 Color: 2
Size: 332233 Color: 3
Size: 289011 Color: 4

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 679340 Color: 4
Size: 160710 Color: 0
Size: 159951 Color: 0

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 712000 Color: 1
Size: 288001 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 595905 Color: 4
Size: 303001 Color: 1
Size: 101095 Color: 3

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 774167 Color: 1
Size: 113215 Color: 1
Size: 112619 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 791620 Color: 0
Size: 105611 Color: 2
Size: 102770 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 750762 Color: 4
Size: 136641 Color: 2
Size: 112598 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 719911 Color: 0
Size: 162677 Color: 2
Size: 117413 Color: 4

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 737162 Color: 2
Size: 145245 Color: 2
Size: 117594 Color: 3

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 640639 Color: 0
Size: 250481 Color: 0
Size: 108881 Color: 4

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 776187 Color: 3
Size: 223814 Color: 4

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 731593 Color: 0
Size: 135202 Color: 0
Size: 133206 Color: 3

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 607610 Color: 4
Size: 392391 Color: 3

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 704044 Color: 1
Size: 156338 Color: 4
Size: 139619 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 765341 Color: 4
Size: 123718 Color: 0
Size: 110942 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 349767 Color: 2
Size: 340802 Color: 0
Size: 309432 Color: 3

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 560381 Color: 0
Size: 220422 Color: 4
Size: 219198 Color: 4

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 647500 Color: 4
Size: 176582 Color: 3
Size: 175919 Color: 0

Bin 179: 0 of cap free
Amount of items: 4
Items: 
Size: 568866 Color: 2
Size: 216990 Color: 0
Size: 107177 Color: 2
Size: 106968 Color: 1

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 773917 Color: 2
Size: 116721 Color: 2
Size: 109363 Color: 1

Bin 181: 0 of cap free
Amount of items: 2
Items: 
Size: 507702 Color: 3
Size: 492299 Color: 4

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 658575 Color: 1
Size: 171087 Color: 0
Size: 170339 Color: 1

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 510655 Color: 2
Size: 489346 Color: 1

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 513973 Color: 1
Size: 486028 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 732330 Color: 0
Size: 143537 Color: 4
Size: 124134 Color: 3

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 749764 Color: 3
Size: 142490 Color: 2
Size: 107747 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 789034 Color: 4
Size: 108267 Color: 2
Size: 102700 Color: 3

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 752965 Color: 2
Size: 146764 Color: 3
Size: 100272 Color: 3

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 660866 Color: 3
Size: 169897 Color: 3
Size: 169238 Color: 4

Bin 190: 0 of cap free
Amount of items: 2
Items: 
Size: 791112 Color: 0
Size: 208889 Color: 2

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 762548 Color: 2
Size: 237453 Color: 3

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 599018 Color: 2
Size: 202330 Color: 0
Size: 198653 Color: 1

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 516131 Color: 0
Size: 381971 Color: 0
Size: 101899 Color: 2

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 559076 Color: 4
Size: 440925 Color: 2

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 697454 Color: 2
Size: 151459 Color: 0
Size: 151088 Color: 4

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 763897 Color: 3
Size: 126523 Color: 3
Size: 109581 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 782705 Color: 2
Size: 116397 Color: 0
Size: 100899 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 738222 Color: 2
Size: 140023 Color: 1
Size: 121756 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 776497 Color: 2
Size: 113684 Color: 1
Size: 109820 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 525649 Color: 0
Size: 310554 Color: 4
Size: 163798 Color: 3

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 786256 Color: 4
Size: 107234 Color: 1
Size: 106511 Color: 1

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 508790 Color: 2
Size: 373180 Color: 0
Size: 118031 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 751792 Color: 4
Size: 141564 Color: 0
Size: 106645 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 694339 Color: 2
Size: 163291 Color: 0
Size: 142371 Color: 4

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 797348 Color: 1
Size: 102503 Color: 0
Size: 100150 Color: 3

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 695611 Color: 4
Size: 152272 Color: 2
Size: 152118 Color: 1

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 698644 Color: 3
Size: 301357 Color: 2

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 741940 Color: 3
Size: 258061 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 692041 Color: 2
Size: 154162 Color: 0
Size: 153798 Color: 4

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 725827 Color: 3
Size: 156340 Color: 0
Size: 117834 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 789349 Color: 4
Size: 107068 Color: 1
Size: 103584 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 532516 Color: 0
Size: 235559 Color: 1
Size: 231926 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 584502 Color: 2
Size: 315084 Color: 1
Size: 100415 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 765629 Color: 2
Size: 123203 Color: 0
Size: 111169 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 600406 Color: 0
Size: 200746 Color: 0
Size: 198849 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 583773 Color: 1
Size: 208972 Color: 4
Size: 207256 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 752017 Color: 3
Size: 128709 Color: 2
Size: 119275 Color: 2

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 795269 Color: 3
Size: 103336 Color: 4
Size: 101396 Color: 4

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 646547 Color: 3
Size: 353454 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 770989 Color: 3
Size: 117514 Color: 1
Size: 111498 Color: 4

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 428163 Color: 3
Size: 421397 Color: 1
Size: 150441 Color: 4

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 508013 Color: 3
Size: 376076 Color: 0
Size: 115912 Color: 0

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 725012 Color: 1
Size: 274989 Color: 4

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 584084 Color: 2
Size: 296930 Color: 0
Size: 118987 Color: 2

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 502589 Color: 4
Size: 497412 Color: 3

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 584049 Color: 3
Size: 272047 Color: 4
Size: 143905 Color: 1

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 604979 Color: 4
Size: 395022 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 355304 Color: 1
Size: 341486 Color: 2
Size: 303211 Color: 3

Bin 229: 0 of cap free
Amount of items: 4
Items: 
Size: 499015 Color: 2
Size: 277140 Color: 3
Size: 119082 Color: 2
Size: 104764 Color: 3

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 398355 Color: 0
Size: 325665 Color: 4
Size: 275981 Color: 3

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 394711 Color: 1
Size: 303304 Color: 4
Size: 301986 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 515076 Color: 2
Size: 384444 Color: 0
Size: 100481 Color: 0

Bin 233: 0 of cap free
Amount of items: 4
Items: 
Size: 318711 Color: 2
Size: 311151 Color: 2
Size: 269118 Color: 3
Size: 101021 Color: 1

Bin 234: 0 of cap free
Amount of items: 4
Items: 
Size: 307476 Color: 1
Size: 294807 Color: 0
Size: 294575 Color: 0
Size: 103143 Color: 3

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 513229 Color: 1
Size: 316608 Color: 0
Size: 170164 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 518404 Color: 0
Size: 380971 Color: 0
Size: 100626 Color: 1

Bin 237: 0 of cap free
Amount of items: 4
Items: 
Size: 315906 Color: 1
Size: 302717 Color: 0
Size: 273692 Color: 4
Size: 107686 Color: 4

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 504562 Color: 3
Size: 384163 Color: 4
Size: 111276 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 503732 Color: 2
Size: 384606 Color: 0
Size: 111663 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 503234 Color: 3
Size: 375342 Color: 0
Size: 121425 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 524083 Color: 3
Size: 371894 Color: 0
Size: 104024 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 520130 Color: 2
Size: 369445 Color: 0
Size: 110426 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 506877 Color: 3
Size: 370878 Color: 0
Size: 122246 Color: 4

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 498392 Color: 4
Size: 367430 Color: 0
Size: 134179 Color: 4

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 498087 Color: 1
Size: 362973 Color: 2
Size: 138941 Color: 4

Bin 246: 0 of cap free
Amount of items: 4
Items: 
Size: 276668 Color: 1
Size: 269870 Color: 2
Size: 253847 Color: 3
Size: 199616 Color: 0

Bin 247: 0 of cap free
Amount of items: 5
Items: 
Size: 277041 Color: 1
Size: 263908 Color: 2
Size: 201515 Color: 2
Size: 133094 Color: 3
Size: 124443 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 350816 Color: 2
Size: 350518 Color: 0
Size: 298667 Color: 2

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 350861 Color: 0
Size: 350474 Color: 0
Size: 298666 Color: 3

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 379111 Color: 3
Size: 333464 Color: 2
Size: 287426 Color: 2

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 380554 Color: 1
Size: 337401 Color: 0
Size: 282046 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 397648 Color: 1
Size: 313433 Color: 2
Size: 288920 Color: 2

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 502785 Color: 1
Size: 497216 Color: 2

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 503572 Color: 2
Size: 496429 Color: 1

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 527867 Color: 0
Size: 472134 Color: 2

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 533229 Color: 3
Size: 466772 Color: 2

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 545681 Color: 2
Size: 454320 Color: 3

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 547265 Color: 0
Size: 452736 Color: 3

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 552580 Color: 0
Size: 447421 Color: 4

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 559246 Color: 3
Size: 440755 Color: 4

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 560829 Color: 0
Size: 220216 Color: 3
Size: 218956 Color: 4

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 573457 Color: 1
Size: 426544 Color: 2

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 574223 Color: 0
Size: 212954 Color: 1
Size: 212824 Color: 3

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 578980 Color: 1
Size: 421021 Color: 2

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 591284 Color: 2
Size: 408717 Color: 1

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 595275 Color: 3
Size: 404726 Color: 4

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 596602 Color: 4
Size: 403399 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 608003 Color: 3
Size: 196275 Color: 1
Size: 195723 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 622228 Color: 2
Size: 189996 Color: 2
Size: 187777 Color: 3

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 627735 Color: 2
Size: 372266 Color: 3

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 629810 Color: 3
Size: 185243 Color: 2
Size: 184948 Color: 4

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 636336 Color: 1
Size: 363665 Color: 2

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 636667 Color: 2
Size: 181709 Color: 0
Size: 181625 Color: 4

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 636970 Color: 3
Size: 363031 Color: 1

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 640383 Color: 3
Size: 359618 Color: 1

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 642058 Color: 2
Size: 357943 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 646907 Color: 3
Size: 177337 Color: 2
Size: 175757 Color: 4

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 649119 Color: 3
Size: 175468 Color: 1
Size: 175414 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 649678 Color: 3
Size: 175214 Color: 0
Size: 175109 Color: 2

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 653840 Color: 3
Size: 173698 Color: 1
Size: 172463 Color: 4

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 660668 Color: 0
Size: 170356 Color: 3
Size: 168977 Color: 1

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 672546 Color: 1
Size: 327455 Color: 2

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 679129 Color: 1
Size: 160558 Color: 0
Size: 160314 Color: 1

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 682841 Color: 1
Size: 317160 Color: 3

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 683296 Color: 4
Size: 158638 Color: 1
Size: 158067 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 683712 Color: 4
Size: 158384 Color: 0
Size: 157905 Color: 1

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 684571 Color: 2
Size: 315430 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 685272 Color: 0
Size: 158165 Color: 4
Size: 156564 Color: 2

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 698483 Color: 4
Size: 151115 Color: 0
Size: 150403 Color: 2

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 699695 Color: 4
Size: 300306 Color: 2

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 701618 Color: 0
Size: 298383 Color: 2

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 732477 Color: 1
Size: 133861 Color: 0
Size: 133663 Color: 2

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 737710 Color: 1
Size: 132329 Color: 3
Size: 129962 Color: 3

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 740940 Color: 0
Size: 130380 Color: 1
Size: 128681 Color: 2

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 751132 Color: 4
Size: 248869 Color: 3

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 752101 Color: 2
Size: 247900 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 756755 Color: 1
Size: 121797 Color: 2
Size: 121449 Color: 2

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 758619 Color: 3
Size: 122344 Color: 1
Size: 119038 Color: 2

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 764200 Color: 2
Size: 118030 Color: 1
Size: 117771 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 776418 Color: 1
Size: 112092 Color: 4
Size: 111491 Color: 3

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 778769 Color: 2
Size: 221232 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 786184 Color: 1
Size: 108176 Color: 4
Size: 105641 Color: 0

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 791664 Color: 2
Size: 208337 Color: 4

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 793068 Color: 1
Size: 103632 Color: 3
Size: 103301 Color: 4

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 795719 Color: 0
Size: 102446 Color: 2
Size: 101836 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 796006 Color: 4
Size: 102191 Color: 4
Size: 101804 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 796414 Color: 0
Size: 101799 Color: 3
Size: 101788 Color: 1

Bin 308: 1 of cap free
Amount of items: 3
Items: 
Size: 397331 Color: 3
Size: 334601 Color: 0
Size: 268068 Color: 0

Bin 309: 1 of cap free
Amount of items: 3
Items: 
Size: 400591 Color: 3
Size: 340871 Color: 0
Size: 258538 Color: 0

Bin 310: 1 of cap free
Amount of items: 2
Items: 
Size: 512429 Color: 0
Size: 487571 Color: 4

Bin 311: 1 of cap free
Amount of items: 2
Items: 
Size: 526686 Color: 4
Size: 473314 Color: 1

Bin 312: 1 of cap free
Amount of items: 2
Items: 
Size: 531498 Color: 0
Size: 468502 Color: 4

Bin 313: 1 of cap free
Amount of items: 2
Items: 
Size: 541601 Color: 3
Size: 458399 Color: 0

Bin 314: 1 of cap free
Amount of items: 2
Items: 
Size: 543700 Color: 4
Size: 456300 Color: 2

Bin 315: 1 of cap free
Amount of items: 2
Items: 
Size: 545633 Color: 1
Size: 454367 Color: 0

Bin 316: 1 of cap free
Amount of items: 2
Items: 
Size: 551400 Color: 0
Size: 448600 Color: 4

Bin 317: 1 of cap free
Amount of items: 2
Items: 
Size: 557762 Color: 1
Size: 442238 Color: 3

Bin 318: 1 of cap free
Amount of items: 2
Items: 
Size: 575396 Color: 0
Size: 424604 Color: 1

Bin 319: 1 of cap free
Amount of items: 2
Items: 
Size: 619442 Color: 2
Size: 380558 Color: 1

Bin 320: 1 of cap free
Amount of items: 3
Items: 
Size: 628412 Color: 2
Size: 185912 Color: 4
Size: 185676 Color: 4

Bin 321: 1 of cap free
Amount of items: 2
Items: 
Size: 641645 Color: 1
Size: 358355 Color: 3

Bin 322: 1 of cap free
Amount of items: 2
Items: 
Size: 649803 Color: 1
Size: 350197 Color: 3

Bin 323: 1 of cap free
Amount of items: 3
Items: 
Size: 655666 Color: 3
Size: 172452 Color: 0
Size: 171882 Color: 2

Bin 324: 1 of cap free
Amount of items: 2
Items: 
Size: 667645 Color: 1
Size: 332355 Color: 2

Bin 325: 1 of cap free
Amount of items: 2
Items: 
Size: 669434 Color: 3
Size: 330566 Color: 2

Bin 326: 1 of cap free
Amount of items: 3
Items: 
Size: 683471 Color: 0
Size: 158295 Color: 1
Size: 158234 Color: 3

Bin 327: 1 of cap free
Amount of items: 2
Items: 
Size: 685846 Color: 1
Size: 314154 Color: 4

Bin 328: 1 of cap free
Amount of items: 2
Items: 
Size: 687633 Color: 3
Size: 312367 Color: 2

Bin 329: 1 of cap free
Amount of items: 2
Items: 
Size: 692876 Color: 3
Size: 307124 Color: 0

Bin 330: 1 of cap free
Amount of items: 2
Items: 
Size: 693463 Color: 3
Size: 306537 Color: 4

Bin 331: 1 of cap free
Amount of items: 3
Items: 
Size: 694577 Color: 0
Size: 153053 Color: 3
Size: 152370 Color: 1

Bin 332: 1 of cap free
Amount of items: 3
Items: 
Size: 700828 Color: 3
Size: 149653 Color: 4
Size: 149519 Color: 0

Bin 333: 1 of cap free
Amount of items: 2
Items: 
Size: 707482 Color: 0
Size: 292518 Color: 3

Bin 334: 1 of cap free
Amount of items: 3
Items: 
Size: 710117 Color: 1
Size: 145245 Color: 2
Size: 144638 Color: 0

Bin 335: 1 of cap free
Amount of items: 3
Items: 
Size: 715422 Color: 4
Size: 142516 Color: 1
Size: 142062 Color: 0

Bin 336: 1 of cap free
Amount of items: 2
Items: 
Size: 735511 Color: 3
Size: 264489 Color: 0

Bin 337: 1 of cap free
Amount of items: 3
Items: 
Size: 738912 Color: 1
Size: 131277 Color: 0
Size: 129811 Color: 0

Bin 338: 1 of cap free
Amount of items: 3
Items: 
Size: 741469 Color: 0
Size: 130062 Color: 4
Size: 128469 Color: 2

Bin 339: 1 of cap free
Amount of items: 2
Items: 
Size: 748519 Color: 4
Size: 251481 Color: 1

Bin 340: 1 of cap free
Amount of items: 3
Items: 
Size: 758685 Color: 1
Size: 121653 Color: 0
Size: 119662 Color: 2

Bin 341: 1 of cap free
Amount of items: 2
Items: 
Size: 763585 Color: 1
Size: 236415 Color: 4

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 767328 Color: 1
Size: 116384 Color: 0
Size: 116288 Color: 4

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 658408 Color: 2
Size: 170975 Color: 1
Size: 170617 Color: 4

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 623324 Color: 0
Size: 189310 Color: 2
Size: 187366 Color: 1

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 741603 Color: 2
Size: 155979 Color: 1
Size: 102418 Color: 2

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 713001 Color: 3
Size: 151748 Color: 4
Size: 135251 Color: 3

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 712168 Color: 0
Size: 151492 Color: 1
Size: 136340 Color: 3

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 781360 Color: 3
Size: 111493 Color: 3
Size: 107147 Color: 0

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 743787 Color: 2
Size: 142265 Color: 0
Size: 113948 Color: 0

Bin 350: 1 of cap free
Amount of items: 2
Items: 
Size: 633039 Color: 2
Size: 366961 Color: 3

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 730986 Color: 1
Size: 135062 Color: 3
Size: 133952 Color: 4

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 737058 Color: 1
Size: 131933 Color: 0
Size: 131009 Color: 1

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 764501 Color: 3
Size: 120249 Color: 2
Size: 115250 Color: 0

Bin 354: 1 of cap free
Amount of items: 3
Items: 
Size: 648293 Color: 1
Size: 176612 Color: 3
Size: 175095 Color: 0

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 652832 Color: 4
Size: 175566 Color: 3
Size: 171602 Color: 2

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 677911 Color: 2
Size: 161079 Color: 0
Size: 161010 Color: 4

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 771348 Color: 4
Size: 114330 Color: 3
Size: 114322 Color: 0

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 694903 Color: 0
Size: 152812 Color: 1
Size: 152285 Color: 0

Bin 359: 1 of cap free
Amount of items: 2
Items: 
Size: 703964 Color: 4
Size: 296036 Color: 1

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 755250 Color: 4
Size: 123640 Color: 2
Size: 121110 Color: 3

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 698960 Color: 3
Size: 150621 Color: 1
Size: 150419 Color: 2

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 396181 Color: 2
Size: 352106 Color: 4
Size: 251713 Color: 0

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 592024 Color: 0
Size: 204759 Color: 0
Size: 203217 Color: 4

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 766313 Color: 4
Size: 116845 Color: 3
Size: 116842 Color: 1

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 760416 Color: 2
Size: 131083 Color: 4
Size: 108501 Color: 2

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 690085 Color: 2
Size: 155296 Color: 1
Size: 154619 Color: 3

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 610263 Color: 2
Size: 195080 Color: 2
Size: 194657 Color: 4

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 622091 Color: 1
Size: 189596 Color: 3
Size: 188313 Color: 3

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 784094 Color: 0
Size: 112556 Color: 3
Size: 103350 Color: 3

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 613269 Color: 1
Size: 193464 Color: 3
Size: 193267 Color: 3

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 663402 Color: 4
Size: 168705 Color: 0
Size: 167893 Color: 0

Bin 372: 1 of cap free
Amount of items: 2
Items: 
Size: 660626 Color: 0
Size: 339374 Color: 1

Bin 373: 1 of cap free
Amount of items: 2
Items: 
Size: 789756 Color: 1
Size: 210244 Color: 3

Bin 374: 1 of cap free
Amount of items: 2
Items: 
Size: 702564 Color: 0
Size: 297436 Color: 4

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 700571 Color: 1
Size: 150024 Color: 2
Size: 149405 Color: 1

Bin 376: 1 of cap free
Amount of items: 2
Items: 
Size: 636381 Color: 2
Size: 363619 Color: 0

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 784151 Color: 2
Size: 111630 Color: 0
Size: 104219 Color: 1

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 511692 Color: 1
Size: 385907 Color: 0
Size: 102401 Color: 3

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 682021 Color: 0
Size: 159355 Color: 4
Size: 158624 Color: 0

Bin 380: 1 of cap free
Amount of items: 2
Items: 
Size: 530034 Color: 1
Size: 469966 Color: 0

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 707929 Color: 2
Size: 146547 Color: 3
Size: 145524 Color: 1

Bin 382: 1 of cap free
Amount of items: 2
Items: 
Size: 636144 Color: 2
Size: 363856 Color: 1

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 724247 Color: 2
Size: 138426 Color: 0
Size: 137327 Color: 0

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 607768 Color: 1
Size: 196404 Color: 4
Size: 195828 Color: 3

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 771786 Color: 3
Size: 124461 Color: 2
Size: 103753 Color: 1

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 697514 Color: 0
Size: 151628 Color: 4
Size: 150858 Color: 2

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 762714 Color: 2
Size: 125353 Color: 2
Size: 111933 Color: 4

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 679061 Color: 0
Size: 160660 Color: 1
Size: 160279 Color: 2

Bin 389: 1 of cap free
Amount of items: 2
Items: 
Size: 725906 Color: 3
Size: 274094 Color: 0

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 772405 Color: 4
Size: 114187 Color: 1
Size: 113408 Color: 0

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 609272 Color: 2
Size: 195799 Color: 0
Size: 194929 Color: 1

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 777042 Color: 2
Size: 111490 Color: 4
Size: 111468 Color: 4

Bin 393: 1 of cap free
Amount of items: 2
Items: 
Size: 754434 Color: 1
Size: 245566 Color: 2

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 713657 Color: 1
Size: 143184 Color: 4
Size: 143159 Color: 2

Bin 395: 1 of cap free
Amount of items: 2
Items: 
Size: 570082 Color: 0
Size: 429918 Color: 3

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 668220 Color: 2
Size: 166813 Color: 2
Size: 164967 Color: 1

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 758900 Color: 3
Size: 121779 Color: 0
Size: 119321 Color: 4

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 685479 Color: 3
Size: 200603 Color: 0
Size: 113918 Color: 1

Bin 399: 1 of cap free
Amount of items: 2
Items: 
Size: 545785 Color: 0
Size: 454215 Color: 2

Bin 400: 1 of cap free
Amount of items: 2
Items: 
Size: 591268 Color: 0
Size: 408732 Color: 4

Bin 401: 1 of cap free
Amount of items: 2
Items: 
Size: 799947 Color: 3
Size: 200053 Color: 2

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 772801 Color: 4
Size: 117593 Color: 1
Size: 109606 Color: 3

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 735823 Color: 3
Size: 133390 Color: 1
Size: 130787 Color: 2

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 627501 Color: 1
Size: 188888 Color: 4
Size: 183611 Color: 3

Bin 405: 1 of cap free
Amount of items: 2
Items: 
Size: 737327 Color: 3
Size: 262673 Color: 1

Bin 406: 1 of cap free
Amount of items: 2
Items: 
Size: 772363 Color: 0
Size: 227637 Color: 3

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 685476 Color: 2
Size: 201387 Color: 4
Size: 113137 Color: 3

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 705754 Color: 1
Size: 147196 Color: 3
Size: 147050 Color: 4

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 709537 Color: 4
Size: 154091 Color: 2
Size: 136372 Color: 0

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 597136 Color: 0
Size: 203405 Color: 2
Size: 199459 Color: 3

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 343369 Color: 0
Size: 339380 Color: 4
Size: 317251 Color: 4

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 775112 Color: 1
Size: 112865 Color: 0
Size: 112023 Color: 4

Bin 413: 1 of cap free
Amount of items: 2
Items: 
Size: 528339 Color: 0
Size: 471661 Color: 3

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 636572 Color: 4
Size: 181870 Color: 4
Size: 181558 Color: 0

Bin 415: 1 of cap free
Amount of items: 2
Items: 
Size: 524897 Color: 3
Size: 475103 Color: 2

Bin 416: 1 of cap free
Amount of items: 2
Items: 
Size: 785627 Color: 1
Size: 214373 Color: 0

Bin 417: 1 of cap free
Amount of items: 2
Items: 
Size: 718277 Color: 0
Size: 281723 Color: 1

Bin 418: 1 of cap free
Amount of items: 2
Items: 
Size: 718212 Color: 2
Size: 281788 Color: 3

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 516948 Color: 0
Size: 241787 Color: 2
Size: 241265 Color: 1

Bin 420: 1 of cap free
Amount of items: 2
Items: 
Size: 775576 Color: 0
Size: 224424 Color: 3

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 783088 Color: 4
Size: 112269 Color: 1
Size: 104643 Color: 0

Bin 422: 1 of cap free
Amount of items: 2
Items: 
Size: 738517 Color: 3
Size: 261483 Color: 4

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 362765 Color: 3
Size: 321953 Color: 2
Size: 315282 Color: 4

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 368367 Color: 3
Size: 318312 Color: 2
Size: 313321 Color: 4

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 506370 Color: 3
Size: 384722 Color: 4
Size: 108908 Color: 3

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 502323 Color: 3
Size: 371750 Color: 0
Size: 125927 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 498604 Color: 1
Size: 367454 Color: 4
Size: 133942 Color: 3

Bin 428: 1 of cap free
Amount of items: 4
Items: 
Size: 289480 Color: 3
Size: 268870 Color: 1
Size: 265864 Color: 3
Size: 175786 Color: 4

Bin 429: 2 of cap free
Amount of items: 3
Items: 
Size: 341432 Color: 4
Size: 333190 Color: 1
Size: 325377 Color: 2

Bin 430: 2 of cap free
Amount of items: 3
Items: 
Size: 351018 Color: 0
Size: 335818 Color: 1
Size: 313163 Color: 2

Bin 431: 2 of cap free
Amount of items: 3
Items: 
Size: 397674 Color: 1
Size: 313328 Color: 4
Size: 288997 Color: 4

Bin 432: 2 of cap free
Amount of items: 2
Items: 
Size: 511066 Color: 2
Size: 488933 Color: 0

Bin 433: 2 of cap free
Amount of items: 2
Items: 
Size: 504665 Color: 0
Size: 495334 Color: 3

Bin 434: 2 of cap free
Amount of items: 2
Items: 
Size: 514670 Color: 0
Size: 485329 Color: 4

Bin 435: 2 of cap free
Amount of items: 2
Items: 
Size: 518738 Color: 0
Size: 481261 Color: 1

Bin 436: 2 of cap free
Amount of items: 2
Items: 
Size: 546185 Color: 1
Size: 453814 Color: 4

Bin 437: 2 of cap free
Amount of items: 2
Items: 
Size: 544937 Color: 0
Size: 455062 Color: 1

Bin 438: 2 of cap free
Amount of items: 2
Items: 
Size: 554191 Color: 4
Size: 445808 Color: 3

Bin 439: 2 of cap free
Amount of items: 2
Items: 
Size: 564272 Color: 0
Size: 435727 Color: 3

Bin 440: 2 of cap free
Amount of items: 2
Items: 
Size: 569441 Color: 2
Size: 430558 Color: 4

Bin 441: 2 of cap free
Amount of items: 2
Items: 
Size: 603707 Color: 3
Size: 396292 Color: 1

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 604681 Color: 0
Size: 197985 Color: 2
Size: 197333 Color: 2

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 617639 Color: 4
Size: 191213 Color: 3
Size: 191147 Color: 2

Bin 444: 2 of cap free
Amount of items: 2
Items: 
Size: 624437 Color: 3
Size: 375562 Color: 0

Bin 445: 2 of cap free
Amount of items: 2
Items: 
Size: 627446 Color: 0
Size: 372553 Color: 3

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 642583 Color: 3
Size: 178788 Color: 0
Size: 178628 Color: 1

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 645767 Color: 3
Size: 177391 Color: 2
Size: 176841 Color: 0

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 648202 Color: 3
Size: 176182 Color: 0
Size: 175615 Color: 0

Bin 449: 2 of cap free
Amount of items: 2
Items: 
Size: 650959 Color: 1
Size: 349040 Color: 2

Bin 450: 2 of cap free
Amount of items: 2
Items: 
Size: 654130 Color: 0
Size: 345869 Color: 3

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 658638 Color: 0
Size: 170721 Color: 1
Size: 170640 Color: 4

Bin 452: 2 of cap free
Amount of items: 2
Items: 
Size: 664703 Color: 1
Size: 335296 Color: 4

Bin 453: 2 of cap free
Amount of items: 2
Items: 
Size: 666059 Color: 1
Size: 333940 Color: 3

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 666383 Color: 2
Size: 167847 Color: 0
Size: 165769 Color: 1

Bin 455: 2 of cap free
Amount of items: 2
Items: 
Size: 678385 Color: 1
Size: 321614 Color: 2

Bin 456: 2 of cap free
Amount of items: 2
Items: 
Size: 685545 Color: 1
Size: 314454 Color: 3

Bin 457: 2 of cap free
Amount of items: 2
Items: 
Size: 694900 Color: 3
Size: 305099 Color: 4

Bin 458: 2 of cap free
Amount of items: 2
Items: 
Size: 714230 Color: 2
Size: 285769 Color: 1

Bin 459: 2 of cap free
Amount of items: 2
Items: 
Size: 724926 Color: 4
Size: 275073 Color: 2

Bin 460: 2 of cap free
Amount of items: 2
Items: 
Size: 725338 Color: 0
Size: 274661 Color: 1

Bin 461: 2 of cap free
Amount of items: 2
Items: 
Size: 728421 Color: 4
Size: 271578 Color: 3

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 732172 Color: 0
Size: 135378 Color: 4
Size: 132449 Color: 2

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 736107 Color: 1
Size: 132560 Color: 4
Size: 131332 Color: 2

Bin 464: 2 of cap free
Amount of items: 2
Items: 
Size: 741674 Color: 2
Size: 258325 Color: 1

Bin 465: 2 of cap free
Amount of items: 2
Items: 
Size: 743543 Color: 3
Size: 256456 Color: 0

Bin 466: 2 of cap free
Amount of items: 2
Items: 
Size: 750678 Color: 3
Size: 249321 Color: 2

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 756119 Color: 1
Size: 122345 Color: 3
Size: 121535 Color: 3

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 756136 Color: 1
Size: 122649 Color: 3
Size: 121214 Color: 4

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 756473 Color: 1
Size: 122666 Color: 2
Size: 120860 Color: 3

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 758363 Color: 1
Size: 121827 Color: 4
Size: 119809 Color: 2

Bin 471: 2 of cap free
Amount of items: 2
Items: 
Size: 775681 Color: 0
Size: 224318 Color: 4

Bin 472: 2 of cap free
Amount of items: 2
Items: 
Size: 782940 Color: 1
Size: 217059 Color: 3

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 653816 Color: 3
Size: 173426 Color: 0
Size: 172757 Color: 2

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 749758 Color: 1
Size: 149098 Color: 1
Size: 101143 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 793246 Color: 4
Size: 104773 Color: 0
Size: 101980 Color: 4

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 708651 Color: 4
Size: 145987 Color: 1
Size: 145361 Color: 4

Bin 477: 2 of cap free
Amount of items: 2
Items: 
Size: 766109 Color: 1
Size: 233890 Color: 2

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 714532 Color: 0
Size: 143090 Color: 2
Size: 142377 Color: 3

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 734593 Color: 3
Size: 150508 Color: 3
Size: 114898 Color: 4

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 603063 Color: 4
Size: 198980 Color: 0
Size: 197956 Color: 4

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 723078 Color: 1
Size: 138688 Color: 3
Size: 138233 Color: 4

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 740996 Color: 1
Size: 129847 Color: 0
Size: 129156 Color: 0

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 769923 Color: 0
Size: 118431 Color: 2
Size: 111645 Color: 4

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 381149 Color: 3
Size: 332159 Color: 0
Size: 286691 Color: 2

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 625790 Color: 4
Size: 196662 Color: 1
Size: 177547 Color: 2

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 618685 Color: 1
Size: 190739 Color: 4
Size: 190575 Color: 4

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 666497 Color: 3
Size: 168558 Color: 4
Size: 164944 Color: 4

Bin 488: 2 of cap free
Amount of items: 2
Items: 
Size: 715148 Color: 4
Size: 284851 Color: 0

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 702540 Color: 2
Size: 148793 Color: 4
Size: 148666 Color: 2

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 733392 Color: 2
Size: 135607 Color: 1
Size: 131000 Color: 4

Bin 491: 2 of cap free
Amount of items: 2
Items: 
Size: 622118 Color: 3
Size: 377881 Color: 0

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 789400 Color: 3
Size: 107330 Color: 3
Size: 103269 Color: 1

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 632076 Color: 0
Size: 187322 Color: 3
Size: 180601 Color: 0

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 666783 Color: 1
Size: 167696 Color: 0
Size: 165520 Color: 4

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 758491 Color: 1
Size: 122387 Color: 4
Size: 119121 Color: 0

Bin 496: 2 of cap free
Amount of items: 2
Items: 
Size: 745406 Color: 1
Size: 254593 Color: 4

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 743974 Color: 0
Size: 129142 Color: 1
Size: 126883 Color: 1

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 686910 Color: 1
Size: 156836 Color: 3
Size: 156253 Color: 4

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 374607 Color: 1
Size: 317303 Color: 0
Size: 308089 Color: 4

Bin 500: 2 of cap free
Amount of items: 2
Items: 
Size: 579007 Color: 1
Size: 420992 Color: 4

Bin 501: 2 of cap free
Amount of items: 2
Items: 
Size: 640633 Color: 3
Size: 359366 Color: 1

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 755354 Color: 0
Size: 124613 Color: 4
Size: 120032 Color: 1

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 731832 Color: 3
Size: 136099 Color: 3
Size: 132068 Color: 0

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 621448 Color: 4
Size: 190290 Color: 1
Size: 188261 Color: 4

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 376169 Color: 3
Size: 341582 Color: 2
Size: 282248 Color: 1

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 396175 Color: 2
Size: 339629 Color: 0
Size: 264195 Color: 0

Bin 507: 2 of cap free
Amount of items: 2
Items: 
Size: 744237 Color: 1
Size: 255762 Color: 3

Bin 508: 2 of cap free
Amount of items: 3
Items: 
Size: 398290 Color: 1
Size: 313073 Color: 4
Size: 288636 Color: 2

Bin 509: 2 of cap free
Amount of items: 3
Items: 
Size: 728712 Color: 4
Size: 137364 Color: 2
Size: 133923 Color: 2

Bin 510: 2 of cap free
Amount of items: 3
Items: 
Size: 625755 Color: 4
Size: 196861 Color: 2
Size: 177383 Color: 4

Bin 511: 2 of cap free
Amount of items: 3
Items: 
Size: 717780 Color: 4
Size: 141239 Color: 4
Size: 140980 Color: 2

Bin 512: 2 of cap free
Amount of items: 2
Items: 
Size: 516942 Color: 1
Size: 483057 Color: 4

Bin 513: 2 of cap free
Amount of items: 3
Items: 
Size: 783276 Color: 2
Size: 108369 Color: 3
Size: 108354 Color: 2

Bin 514: 2 of cap free
Amount of items: 2
Items: 
Size: 736771 Color: 2
Size: 263228 Color: 1

Bin 515: 2 of cap free
Amount of items: 3
Items: 
Size: 708570 Color: 2
Size: 146018 Color: 3
Size: 145411 Color: 2

Bin 516: 2 of cap free
Amount of items: 3
Items: 
Size: 783654 Color: 2
Size: 114641 Color: 4
Size: 101704 Color: 1

Bin 517: 2 of cap free
Amount of items: 2
Items: 
Size: 674926 Color: 4
Size: 325073 Color: 3

Bin 518: 2 of cap free
Amount of items: 2
Items: 
Size: 743789 Color: 1
Size: 256210 Color: 0

Bin 519: 2 of cap free
Amount of items: 2
Items: 
Size: 567493 Color: 3
Size: 432506 Color: 0

Bin 520: 2 of cap free
Amount of items: 2
Items: 
Size: 775994 Color: 4
Size: 224005 Color: 2

Bin 521: 2 of cap free
Amount of items: 3
Items: 
Size: 699952 Color: 0
Size: 150081 Color: 4
Size: 149966 Color: 3

Bin 522: 2 of cap free
Amount of items: 3
Items: 
Size: 575042 Color: 1
Size: 212626 Color: 0
Size: 212331 Color: 0

Bin 523: 2 of cap free
Amount of items: 2
Items: 
Size: 590142 Color: 2
Size: 409857 Color: 3

Bin 524: 2 of cap free
Amount of items: 3
Items: 
Size: 675173 Color: 4
Size: 162762 Color: 4
Size: 162064 Color: 0

Bin 525: 2 of cap free
Amount of items: 3
Items: 
Size: 362715 Color: 4
Size: 343978 Color: 1
Size: 293306 Color: 0

Bin 526: 2 of cap free
Amount of items: 2
Items: 
Size: 676568 Color: 2
Size: 323431 Color: 4

Bin 527: 2 of cap free
Amount of items: 2
Items: 
Size: 640405 Color: 1
Size: 359594 Color: 3

Bin 528: 2 of cap free
Amount of items: 3
Items: 
Size: 653329 Color: 0
Size: 174864 Color: 2
Size: 171806 Color: 0

Bin 529: 2 of cap free
Amount of items: 3
Items: 
Size: 689772 Color: 3
Size: 155667 Color: 0
Size: 154560 Color: 2

Bin 530: 2 of cap free
Amount of items: 3
Items: 
Size: 678930 Color: 0
Size: 160596 Color: 0
Size: 160473 Color: 3

Bin 531: 2 of cap free
Amount of items: 2
Items: 
Size: 794017 Color: 1
Size: 205982 Color: 3

Bin 532: 2 of cap free
Amount of items: 2
Items: 
Size: 574321 Color: 3
Size: 425678 Color: 0

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 559270 Color: 0
Size: 220614 Color: 0
Size: 220115 Color: 2

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 376156 Color: 2
Size: 337191 Color: 4
Size: 286652 Color: 4

Bin 535: 2 of cap free
Amount of items: 2
Items: 
Size: 732619 Color: 1
Size: 267380 Color: 0

Bin 536: 2 of cap free
Amount of items: 3
Items: 
Size: 350146 Color: 2
Size: 341093 Color: 4
Size: 308760 Color: 0

Bin 537: 2 of cap free
Amount of items: 2
Items: 
Size: 624487 Color: 0
Size: 375512 Color: 1

Bin 538: 2 of cap free
Amount of items: 2
Items: 
Size: 617807 Color: 0
Size: 382192 Color: 2

Bin 539: 2 of cap free
Amount of items: 2
Items: 
Size: 710410 Color: 0
Size: 289589 Color: 1

Bin 540: 2 of cap free
Amount of items: 2
Items: 
Size: 704582 Color: 4
Size: 295417 Color: 0

Bin 541: 2 of cap free
Amount of items: 3
Items: 
Size: 517868 Color: 3
Size: 241185 Color: 2
Size: 240946 Color: 4

Bin 542: 2 of cap free
Amount of items: 3
Items: 
Size: 750025 Color: 3
Size: 126033 Color: 3
Size: 123941 Color: 0

Bin 543: 2 of cap free
Amount of items: 3
Items: 
Size: 512107 Color: 2
Size: 246825 Color: 1
Size: 241067 Color: 4

Bin 544: 2 of cap free
Amount of items: 3
Items: 
Size: 338253 Color: 0
Size: 336026 Color: 1
Size: 325720 Color: 0

Bin 545: 2 of cap free
Amount of items: 3
Items: 
Size: 371189 Color: 2
Size: 315798 Color: 0
Size: 313012 Color: 2

Bin 546: 2 of cap free
Amount of items: 4
Items: 
Size: 256277 Color: 0
Size: 252567 Color: 4
Size: 251158 Color: 2
Size: 239997 Color: 1

Bin 547: 3 of cap free
Amount of items: 3
Items: 
Size: 358487 Color: 2
Size: 338502 Color: 0
Size: 303009 Color: 4

Bin 548: 3 of cap free
Amount of items: 2
Items: 
Size: 500883 Color: 3
Size: 499115 Color: 4

Bin 549: 3 of cap free
Amount of items: 2
Items: 
Size: 500610 Color: 3
Size: 499388 Color: 0

Bin 550: 3 of cap free
Amount of items: 2
Items: 
Size: 511146 Color: 2
Size: 488852 Color: 1

Bin 551: 3 of cap free
Amount of items: 2
Items: 
Size: 510314 Color: 0
Size: 489684 Color: 3

Bin 552: 3 of cap free
Amount of items: 2
Items: 
Size: 530388 Color: 1
Size: 469610 Color: 0

Bin 553: 3 of cap free
Amount of items: 2
Items: 
Size: 543308 Color: 1
Size: 456690 Color: 4

Bin 554: 3 of cap free
Amount of items: 2
Items: 
Size: 547340 Color: 1
Size: 452658 Color: 3

Bin 555: 3 of cap free
Amount of items: 2
Items: 
Size: 556540 Color: 3
Size: 443458 Color: 0

Bin 556: 3 of cap free
Amount of items: 2
Items: 
Size: 556802 Color: 0
Size: 443196 Color: 1

Bin 557: 3 of cap free
Amount of items: 2
Items: 
Size: 564545 Color: 2
Size: 435453 Color: 1

Bin 558: 3 of cap free
Amount of items: 2
Items: 
Size: 568614 Color: 2
Size: 431384 Color: 0

Bin 559: 3 of cap free
Amount of items: 2
Items: 
Size: 575958 Color: 4
Size: 424040 Color: 1

Bin 560: 3 of cap free
Amount of items: 2
Items: 
Size: 579703 Color: 0
Size: 420295 Color: 3

Bin 561: 3 of cap free
Amount of items: 2
Items: 
Size: 606026 Color: 1
Size: 393972 Color: 3

Bin 562: 3 of cap free
Amount of items: 2
Items: 
Size: 610385 Color: 0
Size: 389613 Color: 2

Bin 563: 3 of cap free
Amount of items: 2
Items: 
Size: 611478 Color: 2
Size: 388520 Color: 4

Bin 564: 3 of cap free
Amount of items: 2
Items: 
Size: 622657 Color: 4
Size: 377341 Color: 1

Bin 565: 3 of cap free
Amount of items: 2
Items: 
Size: 627911 Color: 1
Size: 372087 Color: 3

Bin 566: 3 of cap free
Amount of items: 3
Items: 
Size: 628723 Color: 2
Size: 185666 Color: 0
Size: 185609 Color: 1

Bin 567: 3 of cap free
Amount of items: 3
Items: 
Size: 635905 Color: 2
Size: 182726 Color: 3
Size: 181367 Color: 1

Bin 568: 3 of cap free
Amount of items: 2
Items: 
Size: 637353 Color: 4
Size: 362645 Color: 1

Bin 569: 3 of cap free
Amount of items: 2
Items: 
Size: 649864 Color: 0
Size: 350134 Color: 3

Bin 570: 3 of cap free
Amount of items: 2
Items: 
Size: 658985 Color: 3
Size: 341013 Color: 4

Bin 571: 3 of cap free
Amount of items: 2
Items: 
Size: 679661 Color: 2
Size: 320337 Color: 1

Bin 572: 3 of cap free
Amount of items: 3
Items: 
Size: 686529 Color: 2
Size: 157266 Color: 2
Size: 156203 Color: 0

Bin 573: 3 of cap free
Amount of items: 3
Items: 
Size: 690219 Color: 0
Size: 155354 Color: 1
Size: 154425 Color: 1

Bin 574: 3 of cap free
Amount of items: 2
Items: 
Size: 691437 Color: 2
Size: 308561 Color: 0

Bin 575: 3 of cap free
Amount of items: 2
Items: 
Size: 694934 Color: 2
Size: 305064 Color: 3

Bin 576: 3 of cap free
Amount of items: 3
Items: 
Size: 699907 Color: 3
Size: 150299 Color: 0
Size: 149792 Color: 1

Bin 577: 3 of cap free
Amount of items: 2
Items: 
Size: 701325 Color: 3
Size: 298673 Color: 0

Bin 578: 3 of cap free
Amount of items: 2
Items: 
Size: 701487 Color: 3
Size: 298511 Color: 1

Bin 579: 3 of cap free
Amount of items: 2
Items: 
Size: 707628 Color: 3
Size: 292370 Color: 2

Bin 580: 3 of cap free
Amount of items: 2
Items: 
Size: 735362 Color: 0
Size: 264636 Color: 4

Bin 581: 3 of cap free
Amount of items: 3
Items: 
Size: 739768 Color: 1
Size: 130214 Color: 0
Size: 130016 Color: 3

Bin 582: 3 of cap free
Amount of items: 3
Items: 
Size: 741957 Color: 1
Size: 129551 Color: 3
Size: 128490 Color: 4

Bin 583: 3 of cap free
Amount of items: 3
Items: 
Size: 764765 Color: 4
Size: 117678 Color: 1
Size: 117555 Color: 4

Bin 584: 3 of cap free
Amount of items: 2
Items: 
Size: 784401 Color: 2
Size: 215597 Color: 0

Bin 585: 3 of cap free
Amount of items: 2
Items: 
Size: 784954 Color: 4
Size: 215044 Color: 0

Bin 586: 3 of cap free
Amount of items: 2
Items: 
Size: 788573 Color: 3
Size: 211425 Color: 0

Bin 587: 3 of cap free
Amount of items: 3
Items: 
Size: 773532 Color: 4
Size: 113303 Color: 1
Size: 113163 Color: 3

Bin 588: 3 of cap free
Amount of items: 3
Items: 
Size: 766269 Color: 2
Size: 132439 Color: 2
Size: 101290 Color: 3

Bin 589: 3 of cap free
Amount of items: 2
Items: 
Size: 502472 Color: 3
Size: 497526 Color: 4

Bin 590: 3 of cap free
Amount of items: 2
Items: 
Size: 584671 Color: 3
Size: 415327 Color: 2

Bin 591: 3 of cap free
Amount of items: 2
Items: 
Size: 724429 Color: 0
Size: 275569 Color: 2

Bin 592: 3 of cap free
Amount of items: 3
Items: 
Size: 362687 Color: 1
Size: 327004 Color: 1
Size: 310307 Color: 4

Bin 593: 3 of cap free
Amount of items: 3
Items: 
Size: 775063 Color: 4
Size: 114430 Color: 2
Size: 110505 Color: 3

Bin 594: 3 of cap free
Amount of items: 3
Items: 
Size: 746333 Color: 2
Size: 152498 Color: 4
Size: 101167 Color: 3

Bin 595: 3 of cap free
Amount of items: 2
Items: 
Size: 634212 Color: 3
Size: 365786 Color: 2

Bin 596: 3 of cap free
Amount of items: 3
Items: 
Size: 681390 Color: 1
Size: 159364 Color: 4
Size: 159244 Color: 0

Bin 597: 3 of cap free
Amount of items: 2
Items: 
Size: 624264 Color: 3
Size: 375734 Color: 2

Bin 598: 3 of cap free
Amount of items: 3
Items: 
Size: 651848 Color: 2
Size: 175224 Color: 0
Size: 172926 Color: 4

Bin 599: 3 of cap free
Amount of items: 3
Items: 
Size: 353353 Color: 2
Size: 352466 Color: 1
Size: 294179 Color: 0

Bin 600: 3 of cap free
Amount of items: 3
Items: 
Size: 714609 Color: 3
Size: 142804 Color: 1
Size: 142585 Color: 4

Bin 601: 3 of cap free
Amount of items: 3
Items: 
Size: 633516 Color: 1
Size: 183378 Color: 3
Size: 183104 Color: 4

Bin 602: 3 of cap free
Amount of items: 3
Items: 
Size: 636077 Color: 4
Size: 182748 Color: 3
Size: 181173 Color: 4

Bin 603: 3 of cap free
Amount of items: 2
Items: 
Size: 782174 Color: 2
Size: 217824 Color: 3

Bin 604: 3 of cap free
Amount of items: 2
Items: 
Size: 506158 Color: 4
Size: 493840 Color: 1

Bin 605: 3 of cap free
Amount of items: 3
Items: 
Size: 526672 Color: 4
Size: 237234 Color: 2
Size: 236092 Color: 3

Bin 606: 3 of cap free
Amount of items: 3
Items: 
Size: 675757 Color: 3
Size: 162333 Color: 0
Size: 161908 Color: 2

Bin 607: 3 of cap free
Amount of items: 3
Items: 
Size: 668191 Color: 4
Size: 166430 Color: 1
Size: 165377 Color: 1

Bin 608: 3 of cap free
Amount of items: 3
Items: 
Size: 754246 Color: 4
Size: 125432 Color: 1
Size: 120320 Color: 2

Bin 609: 3 of cap free
Amount of items: 3
Items: 
Size: 602591 Color: 3
Size: 199305 Color: 4
Size: 198102 Color: 1

Bin 610: 3 of cap free
Amount of items: 2
Items: 
Size: 773098 Color: 2
Size: 226900 Color: 4

Bin 611: 3 of cap free
Amount of items: 3
Items: 
Size: 681203 Color: 4
Size: 159570 Color: 1
Size: 159225 Color: 3

Bin 612: 3 of cap free
Amount of items: 2
Items: 
Size: 639724 Color: 2
Size: 360274 Color: 4

Bin 613: 3 of cap free
Amount of items: 2
Items: 
Size: 650494 Color: 0
Size: 349504 Color: 2

Bin 614: 3 of cap free
Amount of items: 2
Items: 
Size: 553483 Color: 3
Size: 446515 Color: 4

Bin 615: 3 of cap free
Amount of items: 3
Items: 
Size: 616232 Color: 0
Size: 192337 Color: 2
Size: 191429 Color: 4

Bin 616: 3 of cap free
Amount of items: 2
Items: 
Size: 551481 Color: 4
Size: 448517 Color: 0

Bin 617: 3 of cap free
Amount of items: 3
Items: 
Size: 572835 Color: 3
Size: 213690 Color: 1
Size: 213473 Color: 2

Bin 618: 3 of cap free
Amount of items: 3
Items: 
Size: 629046 Color: 1
Size: 226488 Color: 2
Size: 144464 Color: 1

Bin 619: 3 of cap free
Amount of items: 3
Items: 
Size: 710877 Color: 0
Size: 144682 Color: 0
Size: 144439 Color: 3

Bin 620: 3 of cap free
Amount of items: 2
Items: 
Size: 570156 Color: 0
Size: 429842 Color: 4

Bin 621: 3 of cap free
Amount of items: 3
Items: 
Size: 495887 Color: 3
Size: 326658 Color: 0
Size: 177453 Color: 3

Bin 622: 3 of cap free
Amount of items: 3
Items: 
Size: 559353 Color: 0
Size: 221246 Color: 1
Size: 219399 Color: 2

Bin 623: 3 of cap free
Amount of items: 3
Items: 
Size: 648730 Color: 4
Size: 176185 Color: 0
Size: 175083 Color: 0

Bin 624: 3 of cap free
Amount of items: 3
Items: 
Size: 728489 Color: 1
Size: 136609 Color: 3
Size: 134900 Color: 3

Bin 625: 3 of cap free
Amount of items: 2
Items: 
Size: 747334 Color: 1
Size: 252664 Color: 4

Bin 626: 3 of cap free
Amount of items: 3
Items: 
Size: 713946 Color: 4
Size: 143820 Color: 3
Size: 142232 Color: 4

Bin 627: 3 of cap free
Amount of items: 3
Items: 
Size: 608116 Color: 1
Size: 196147 Color: 4
Size: 195735 Color: 3

Bin 628: 3 of cap free
Amount of items: 2
Items: 
Size: 689487 Color: 0
Size: 310511 Color: 3

Bin 629: 3 of cap free
Amount of items: 3
Items: 
Size: 385595 Color: 3
Size: 308871 Color: 1
Size: 305532 Color: 0

Bin 630: 3 of cap free
Amount of items: 2
Items: 
Size: 726210 Color: 2
Size: 273788 Color: 0

Bin 631: 3 of cap free
Amount of items: 3
Items: 
Size: 789313 Color: 3
Size: 107968 Color: 0
Size: 102717 Color: 4

Bin 632: 3 of cap free
Amount of items: 2
Items: 
Size: 719414 Color: 2
Size: 280584 Color: 1

Bin 633: 3 of cap free
Amount of items: 3
Items: 
Size: 555436 Color: 4
Size: 222351 Color: 4
Size: 222211 Color: 3

Bin 634: 3 of cap free
Amount of items: 3
Items: 
Size: 583054 Color: 0
Size: 209915 Color: 3
Size: 207029 Color: 4

Bin 635: 3 of cap free
Amount of items: 2
Items: 
Size: 758608 Color: 1
Size: 241390 Color: 0

Bin 636: 3 of cap free
Amount of items: 3
Items: 
Size: 356129 Color: 0
Size: 340776 Color: 4
Size: 303093 Color: 3

Bin 637: 3 of cap free
Amount of items: 4
Items: 
Size: 303433 Color: 0
Size: 303272 Color: 2
Size: 264551 Color: 0
Size: 128742 Color: 2

Bin 638: 3 of cap free
Amount of items: 4
Items: 
Size: 265629 Color: 1
Size: 258591 Color: 1
Size: 254717 Color: 4
Size: 221061 Color: 2

Bin 639: 3 of cap free
Amount of items: 4
Items: 
Size: 253703 Color: 2
Size: 252103 Color: 4
Size: 250868 Color: 4
Size: 243324 Color: 1

Bin 640: 4 of cap free
Amount of items: 3
Items: 
Size: 795361 Color: 4
Size: 104092 Color: 0
Size: 100544 Color: 0

Bin 641: 4 of cap free
Amount of items: 3
Items: 
Size: 654512 Color: 3
Size: 173425 Color: 4
Size: 172060 Color: 0

Bin 642: 4 of cap free
Amount of items: 3
Items: 
Size: 362660 Color: 1
Size: 350767 Color: 2
Size: 286570 Color: 1

Bin 643: 4 of cap free
Amount of items: 3
Items: 
Size: 399630 Color: 1
Size: 324419 Color: 4
Size: 275948 Color: 1

Bin 644: 4 of cap free
Amount of items: 3
Items: 
Size: 421744 Color: 3
Size: 311188 Color: 0
Size: 267065 Color: 0

Bin 645: 4 of cap free
Amount of items: 2
Items: 
Size: 516028 Color: 3
Size: 483969 Color: 4

Bin 646: 4 of cap free
Amount of items: 2
Items: 
Size: 517682 Color: 4
Size: 482315 Color: 1

Bin 647: 4 of cap free
Amount of items: 2
Items: 
Size: 513481 Color: 0
Size: 486516 Color: 1

Bin 648: 4 of cap free
Amount of items: 2
Items: 
Size: 528002 Color: 4
Size: 471995 Color: 0

Bin 649: 4 of cap free
Amount of items: 2
Items: 
Size: 553109 Color: 1
Size: 446888 Color: 2

Bin 650: 4 of cap free
Amount of items: 2
Items: 
Size: 586789 Color: 1
Size: 413208 Color: 2

Bin 651: 4 of cap free
Amount of items: 2
Items: 
Size: 617068 Color: 3
Size: 382929 Color: 0

Bin 652: 4 of cap free
Amount of items: 3
Items: 
Size: 623187 Color: 1
Size: 188926 Color: 0
Size: 187884 Color: 4

Bin 653: 4 of cap free
Amount of items: 3
Items: 
Size: 629724 Color: 2
Size: 185138 Color: 3
Size: 185135 Color: 1

Bin 654: 4 of cap free
Amount of items: 2
Items: 
Size: 641141 Color: 1
Size: 358856 Color: 2

Bin 655: 4 of cap free
Amount of items: 2
Items: 
Size: 643290 Color: 4
Size: 356707 Color: 2

Bin 656: 4 of cap free
Amount of items: 2
Items: 
Size: 654925 Color: 4
Size: 345072 Color: 2

Bin 657: 4 of cap free
Amount of items: 2
Items: 
Size: 659519 Color: 0
Size: 340478 Color: 3

Bin 658: 4 of cap free
Amount of items: 2
Items: 
Size: 663160 Color: 1
Size: 336837 Color: 3

Bin 659: 4 of cap free
Amount of items: 3
Items: 
Size: 667257 Color: 0
Size: 166523 Color: 4
Size: 166217 Color: 4

Bin 660: 4 of cap free
Amount of items: 3
Items: 
Size: 670530 Color: 0
Size: 164770 Color: 2
Size: 164697 Color: 2

Bin 661: 4 of cap free
Amount of items: 2
Items: 
Size: 671244 Color: 0
Size: 328753 Color: 2

Bin 662: 4 of cap free
Amount of items: 2
Items: 
Size: 674857 Color: 0
Size: 325140 Color: 3

Bin 663: 4 of cap free
Amount of items: 2
Items: 
Size: 676692 Color: 2
Size: 323305 Color: 1

Bin 664: 4 of cap free
Amount of items: 3
Items: 
Size: 677483 Color: 1
Size: 161528 Color: 0
Size: 160986 Color: 4

Bin 665: 4 of cap free
Amount of items: 3
Items: 
Size: 690827 Color: 4
Size: 154948 Color: 0
Size: 154222 Color: 1

Bin 666: 4 of cap free
Amount of items: 2
Items: 
Size: 692360 Color: 2
Size: 307637 Color: 3

Bin 667: 4 of cap free
Amount of items: 2
Items: 
Size: 696338 Color: 3
Size: 303659 Color: 1

Bin 668: 4 of cap free
Amount of items: 3
Items: 
Size: 710851 Color: 3
Size: 144630 Color: 0
Size: 144516 Color: 4

Bin 669: 4 of cap free
Amount of items: 3
Items: 
Size: 730694 Color: 0
Size: 135994 Color: 2
Size: 133309 Color: 2

Bin 670: 4 of cap free
Amount of items: 2
Items: 
Size: 731696 Color: 4
Size: 268301 Color: 1

Bin 671: 4 of cap free
Amount of items: 3
Items: 
Size: 731969 Color: 0
Size: 134627 Color: 3
Size: 133401 Color: 2

Bin 672: 4 of cap free
Amount of items: 3
Items: 
Size: 735464 Color: 2
Size: 133803 Color: 1
Size: 130730 Color: 0

Bin 673: 4 of cap free
Amount of items: 3
Items: 
Size: 736761 Color: 3
Size: 133160 Color: 1
Size: 130076 Color: 0

Bin 674: 4 of cap free
Amount of items: 3
Items: 
Size: 745705 Color: 1
Size: 127457 Color: 0
Size: 126835 Color: 4

Bin 675: 4 of cap free
Amount of items: 2
Items: 
Size: 760050 Color: 2
Size: 239947 Color: 4

Bin 676: 4 of cap free
Amount of items: 2
Items: 
Size: 765653 Color: 0
Size: 234344 Color: 4

Bin 677: 4 of cap free
Amount of items: 2
Items: 
Size: 769569 Color: 3
Size: 230428 Color: 2

Bin 678: 4 of cap free
Amount of items: 2
Items: 
Size: 775412 Color: 4
Size: 224585 Color: 2

Bin 679: 4 of cap free
Amount of items: 2
Items: 
Size: 786179 Color: 2
Size: 213818 Color: 4

Bin 680: 4 of cap free
Amount of items: 3
Items: 
Size: 787897 Color: 1
Size: 106227 Color: 0
Size: 105873 Color: 3

Bin 681: 4 of cap free
Amount of items: 2
Items: 
Size: 701383 Color: 1
Size: 298614 Color: 2

Bin 682: 4 of cap free
Amount of items: 2
Items: 
Size: 644405 Color: 3
Size: 355592 Color: 4

Bin 683: 4 of cap free
Amount of items: 3
Items: 
Size: 740599 Color: 0
Size: 131303 Color: 4
Size: 128095 Color: 1

Bin 684: 4 of cap free
Amount of items: 3
Items: 
Size: 371767 Color: 3
Size: 369984 Color: 0
Size: 258246 Color: 0

Bin 685: 4 of cap free
Amount of items: 2
Items: 
Size: 611770 Color: 3
Size: 388227 Color: 1

Bin 686: 4 of cap free
Amount of items: 3
Items: 
Size: 624707 Color: 2
Size: 187916 Color: 4
Size: 187374 Color: 1

Bin 687: 4 of cap free
Amount of items: 3
Items: 
Size: 754919 Color: 2
Size: 124224 Color: 3
Size: 120854 Color: 3

Bin 688: 4 of cap free
Amount of items: 2
Items: 
Size: 793422 Color: 0
Size: 206575 Color: 2

Bin 689: 4 of cap free
Amount of items: 3
Items: 
Size: 744544 Color: 0
Size: 130485 Color: 0
Size: 124968 Color: 3

Bin 690: 4 of cap free
Amount of items: 3
Items: 
Size: 497983 Color: 0
Size: 251566 Color: 3
Size: 250448 Color: 4

Bin 691: 4 of cap free
Amount of items: 3
Items: 
Size: 747286 Color: 4
Size: 126653 Color: 1
Size: 126058 Color: 4

Bin 692: 4 of cap free
Amount of items: 2
Items: 
Size: 647329 Color: 4
Size: 352668 Color: 0

Bin 693: 4 of cap free
Amount of items: 3
Items: 
Size: 724455 Color: 4
Size: 138706 Color: 1
Size: 136836 Color: 3

Bin 694: 4 of cap free
Amount of items: 3
Items: 
Size: 691284 Color: 2
Size: 154759 Color: 2
Size: 153954 Color: 3

Bin 695: 4 of cap free
Amount of items: 3
Items: 
Size: 729205 Color: 3
Size: 135739 Color: 1
Size: 135053 Color: 0

Bin 696: 4 of cap free
Amount of items: 2
Items: 
Size: 591799 Color: 0
Size: 408198 Color: 3

Bin 697: 4 of cap free
Amount of items: 3
Items: 
Size: 696218 Color: 4
Size: 151922 Color: 1
Size: 151857 Color: 1

Bin 698: 4 of cap free
Amount of items: 2
Items: 
Size: 552985 Color: 2
Size: 447012 Color: 1

Bin 699: 4 of cap free
Amount of items: 3
Items: 
Size: 632453 Color: 0
Size: 184082 Color: 3
Size: 183462 Color: 3

Bin 700: 4 of cap free
Amount of items: 3
Items: 
Size: 639494 Color: 4
Size: 180256 Color: 3
Size: 180247 Color: 0

Bin 701: 4 of cap free
Amount of items: 3
Items: 
Size: 604349 Color: 2
Size: 197870 Color: 1
Size: 197778 Color: 2

Bin 702: 4 of cap free
Amount of items: 3
Items: 
Size: 650071 Color: 0
Size: 177581 Color: 2
Size: 172345 Color: 3

Bin 703: 4 of cap free
Amount of items: 3
Items: 
Size: 660308 Color: 0
Size: 170419 Color: 1
Size: 169270 Color: 3

Bin 704: 4 of cap free
Amount of items: 2
Items: 
Size: 735028 Color: 4
Size: 264969 Color: 0

Bin 705: 4 of cap free
Amount of items: 2
Items: 
Size: 784472 Color: 1
Size: 215525 Color: 4

Bin 706: 4 of cap free
Amount of items: 3
Items: 
Size: 668284 Color: 0
Size: 165938 Color: 4
Size: 165775 Color: 0

Bin 707: 4 of cap free
Amount of items: 3
Items: 
Size: 666045 Color: 1
Size: 168777 Color: 3
Size: 165175 Color: 4

Bin 708: 4 of cap free
Amount of items: 2
Items: 
Size: 522140 Color: 4
Size: 477857 Color: 1

Bin 709: 4 of cap free
Amount of items: 3
Items: 
Size: 396528 Color: 4
Size: 349625 Color: 2
Size: 253844 Color: 2

Bin 710: 4 of cap free
Amount of items: 3
Items: 
Size: 545938 Color: 1
Size: 231880 Color: 4
Size: 222179 Color: 3

Bin 711: 4 of cap free
Amount of items: 2
Items: 
Size: 506438 Color: 2
Size: 493559 Color: 4

Bin 712: 4 of cap free
Amount of items: 3
Items: 
Size: 603896 Color: 4
Size: 198152 Color: 0
Size: 197949 Color: 1

Bin 713: 4 of cap free
Amount of items: 2
Items: 
Size: 759107 Color: 4
Size: 240890 Color: 2

Bin 714: 4 of cap free
Amount of items: 3
Items: 
Size: 672182 Color: 3
Size: 164508 Color: 4
Size: 163307 Color: 3

Bin 715: 4 of cap free
Amount of items: 3
Items: 
Size: 372649 Color: 3
Size: 332477 Color: 1
Size: 294871 Color: 0

Bin 716: 4 of cap free
Amount of items: 2
Items: 
Size: 508429 Color: 0
Size: 491568 Color: 3

Bin 717: 4 of cap free
Amount of items: 3
Items: 
Size: 367421 Color: 2
Size: 366713 Color: 2
Size: 265863 Color: 4

Bin 718: 4 of cap free
Amount of items: 3
Items: 
Size: 574656 Color: 1
Size: 212685 Color: 2
Size: 212656 Color: 4

Bin 719: 4 of cap free
Amount of items: 2
Items: 
Size: 768492 Color: 1
Size: 231505 Color: 0

Bin 720: 4 of cap free
Amount of items: 2
Items: 
Size: 640974 Color: 3
Size: 359023 Color: 4

Bin 721: 4 of cap free
Amount of items: 2
Items: 
Size: 710024 Color: 1
Size: 289973 Color: 0

Bin 722: 4 of cap free
Amount of items: 2
Items: 
Size: 716383 Color: 1
Size: 283614 Color: 0

Bin 723: 4 of cap free
Amount of items: 2
Items: 
Size: 566236 Color: 4
Size: 433761 Color: 3

Bin 724: 4 of cap free
Amount of items: 3
Items: 
Size: 384626 Color: 0
Size: 312780 Color: 1
Size: 302591 Color: 0

Bin 725: 4 of cap free
Amount of items: 3
Items: 
Size: 394508 Color: 0
Size: 303322 Color: 4
Size: 302167 Color: 3

Bin 726: 4 of cap free
Amount of items: 4
Items: 
Size: 266454 Color: 2
Size: 264866 Color: 2
Size: 252529 Color: 3
Size: 216148 Color: 0

Bin 727: 5 of cap free
Amount of items: 3
Items: 
Size: 696458 Color: 0
Size: 151772 Color: 2
Size: 151766 Color: 2

Bin 728: 5 of cap free
Amount of items: 3
Items: 
Size: 343206 Color: 4
Size: 338095 Color: 2
Size: 318695 Color: 1

Bin 729: 5 of cap free
Amount of items: 3
Items: 
Size: 385367 Color: 1
Size: 308002 Color: 3
Size: 306627 Color: 1

Bin 730: 5 of cap free
Amount of items: 3
Items: 
Size: 386384 Color: 4
Size: 315670 Color: 0
Size: 297942 Color: 0

Bin 731: 5 of cap free
Amount of items: 3
Items: 
Size: 398222 Color: 2
Size: 343425 Color: 0
Size: 258349 Color: 0

Bin 732: 5 of cap free
Amount of items: 2
Items: 
Size: 514069 Color: 1
Size: 485927 Color: 3

Bin 733: 5 of cap free
Amount of items: 2
Items: 
Size: 520113 Color: 2
Size: 479883 Color: 0

Bin 734: 5 of cap free
Amount of items: 2
Items: 
Size: 536051 Color: 2
Size: 463945 Color: 1

Bin 735: 5 of cap free
Amount of items: 2
Items: 
Size: 536979 Color: 4
Size: 463017 Color: 2

Bin 736: 5 of cap free
Amount of items: 2
Items: 
Size: 575316 Color: 2
Size: 424680 Color: 1

Bin 737: 5 of cap free
Amount of items: 2
Items: 
Size: 585754 Color: 3
Size: 414242 Color: 0

Bin 738: 5 of cap free
Amount of items: 3
Items: 
Size: 602740 Color: 0
Size: 198732 Color: 3
Size: 198524 Color: 4

Bin 739: 5 of cap free
Amount of items: 3
Items: 
Size: 613601 Color: 2
Size: 193874 Color: 4
Size: 192521 Color: 3

Bin 740: 5 of cap free
Amount of items: 2
Items: 
Size: 621823 Color: 4
Size: 378173 Color: 0

Bin 741: 5 of cap free
Amount of items: 2
Items: 
Size: 638582 Color: 1
Size: 361414 Color: 0

Bin 742: 5 of cap free
Amount of items: 2
Items: 
Size: 650864 Color: 0
Size: 349132 Color: 1

Bin 743: 5 of cap free
Amount of items: 3
Items: 
Size: 653165 Color: 3
Size: 174360 Color: 1
Size: 172471 Color: 2

Bin 744: 5 of cap free
Amount of items: 3
Items: 
Size: 655812 Color: 0
Size: 172395 Color: 3
Size: 171789 Color: 4

Bin 745: 5 of cap free
Amount of items: 3
Items: 
Size: 662043 Color: 0
Size: 169117 Color: 4
Size: 168836 Color: 1

Bin 746: 5 of cap free
Amount of items: 2
Items: 
Size: 684768 Color: 4
Size: 315228 Color: 3

Bin 747: 5 of cap free
Amount of items: 3
Items: 
Size: 687251 Color: 2
Size: 156402 Color: 2
Size: 156343 Color: 0

Bin 748: 5 of cap free
Amount of items: 2
Items: 
Size: 706685 Color: 4
Size: 293311 Color: 3

Bin 749: 5 of cap free
Amount of items: 2
Items: 
Size: 714863 Color: 3
Size: 285133 Color: 0

Bin 750: 5 of cap free
Amount of items: 2
Items: 
Size: 721892 Color: 4
Size: 278104 Color: 1

Bin 751: 5 of cap free
Amount of items: 3
Items: 
Size: 731728 Color: 3
Size: 134475 Color: 0
Size: 133793 Color: 2

Bin 752: 5 of cap free
Amount of items: 2
Items: 
Size: 732299 Color: 2
Size: 267697 Color: 3

Bin 753: 5 of cap free
Amount of items: 3
Items: 
Size: 732548 Color: 3
Size: 135139 Color: 0
Size: 132309 Color: 3

Bin 754: 5 of cap free
Amount of items: 2
Items: 
Size: 743540 Color: 4
Size: 256456 Color: 0

Bin 755: 5 of cap free
Amount of items: 3
Items: 
Size: 746313 Color: 2
Size: 126862 Color: 1
Size: 126821 Color: 4

Bin 756: 5 of cap free
Amount of items: 2
Items: 
Size: 753185 Color: 1
Size: 246811 Color: 0

Bin 757: 5 of cap free
Amount of items: 2
Items: 
Size: 760852 Color: 4
Size: 239144 Color: 2

Bin 758: 5 of cap free
Amount of items: 3
Items: 
Size: 771326 Color: 4
Size: 114342 Color: 2
Size: 114328 Color: 4

Bin 759: 5 of cap free
Amount of items: 2
Items: 
Size: 797861 Color: 1
Size: 202135 Color: 2

Bin 760: 5 of cap free
Amount of items: 2
Items: 
Size: 799568 Color: 0
Size: 200428 Color: 2

Bin 761: 5 of cap free
Amount of items: 3
Items: 
Size: 622284 Color: 0
Size: 189877 Color: 4
Size: 187835 Color: 1

Bin 762: 5 of cap free
Amount of items: 3
Items: 
Size: 667156 Color: 4
Size: 166474 Color: 2
Size: 166366 Color: 4

Bin 763: 5 of cap free
Amount of items: 3
Items: 
Size: 680720 Color: 4
Size: 160560 Color: 0
Size: 158716 Color: 0

Bin 764: 5 of cap free
Amount of items: 3
Items: 
Size: 393998 Color: 1
Size: 317390 Color: 0
Size: 288608 Color: 4

Bin 765: 5 of cap free
Amount of items: 3
Items: 
Size: 396046 Color: 4
Size: 329546 Color: 2
Size: 274404 Color: 3

Bin 766: 5 of cap free
Amount of items: 3
Items: 
Size: 399002 Color: 3
Size: 334646 Color: 0
Size: 266348 Color: 1

Bin 767: 5 of cap free
Amount of items: 3
Items: 
Size: 396843 Color: 2
Size: 339312 Color: 4
Size: 263841 Color: 4

Bin 768: 5 of cap free
Amount of items: 2
Items: 
Size: 591824 Color: 0
Size: 408172 Color: 2

Bin 769: 5 of cap free
Amount of items: 3
Items: 
Size: 622076 Color: 0
Size: 189954 Color: 3
Size: 187966 Color: 1

Bin 770: 5 of cap free
Amount of items: 3
Items: 
Size: 749394 Color: 2
Size: 127166 Color: 3
Size: 123436 Color: 0

Bin 771: 5 of cap free
Amount of items: 3
Items: 
Size: 615058 Color: 2
Size: 192704 Color: 4
Size: 192234 Color: 4

Bin 772: 5 of cap free
Amount of items: 2
Items: 
Size: 703507 Color: 3
Size: 296489 Color: 0

Bin 773: 5 of cap free
Amount of items: 2
Items: 
Size: 797596 Color: 1
Size: 202400 Color: 2

Bin 774: 5 of cap free
Amount of items: 3
Items: 
Size: 679372 Color: 2
Size: 160549 Color: 3
Size: 160075 Color: 4

Bin 775: 5 of cap free
Amount of items: 3
Items: 
Size: 713529 Color: 3
Size: 150484 Color: 0
Size: 135983 Color: 3

Bin 776: 5 of cap free
Amount of items: 2
Items: 
Size: 748708 Color: 2
Size: 251288 Color: 3

Bin 777: 5 of cap free
Amount of items: 3
Items: 
Size: 370711 Color: 2
Size: 317076 Color: 1
Size: 312209 Color: 1

Bin 778: 5 of cap free
Amount of items: 2
Items: 
Size: 786558 Color: 2
Size: 213438 Color: 0

Bin 779: 5 of cap free
Amount of items: 2
Items: 
Size: 794093 Color: 4
Size: 205903 Color: 3

Bin 780: 5 of cap free
Amount of items: 3
Items: 
Size: 674145 Color: 2
Size: 163323 Color: 3
Size: 162528 Color: 4

Bin 781: 5 of cap free
Amount of items: 3
Items: 
Size: 589724 Color: 2
Size: 205366 Color: 1
Size: 204906 Color: 1

Bin 782: 5 of cap free
Amount of items: 3
Items: 
Size: 731178 Color: 2
Size: 135312 Color: 0
Size: 133506 Color: 0

Bin 783: 5 of cap free
Amount of items: 3
Items: 
Size: 586944 Color: 3
Size: 206558 Color: 1
Size: 206494 Color: 2

Bin 784: 5 of cap free
Amount of items: 2
Items: 
Size: 647064 Color: 3
Size: 352932 Color: 1

Bin 785: 5 of cap free
Amount of items: 2
Items: 
Size: 636033 Color: 0
Size: 363963 Color: 1

Bin 786: 5 of cap free
Amount of items: 3
Items: 
Size: 618959 Color: 0
Size: 190679 Color: 2
Size: 190358 Color: 3

Bin 787: 5 of cap free
Amount of items: 3
Items: 
Size: 508480 Color: 0
Size: 251703 Color: 4
Size: 239813 Color: 3

Bin 788: 5 of cap free
Amount of items: 2
Items: 
Size: 668968 Color: 2
Size: 331028 Color: 4

Bin 789: 5 of cap free
Amount of items: 3
Items: 
Size: 730532 Color: 0
Size: 136001 Color: 1
Size: 133463 Color: 3

Bin 790: 5 of cap free
Amount of items: 2
Items: 
Size: 572712 Color: 1
Size: 427284 Color: 3

Bin 791: 5 of cap free
Amount of items: 2
Items: 
Size: 574867 Color: 3
Size: 425129 Color: 0

Bin 792: 5 of cap free
Amount of items: 3
Items: 
Size: 634854 Color: 4
Size: 184020 Color: 2
Size: 181122 Color: 1

Bin 793: 5 of cap free
Amount of items: 3
Items: 
Size: 713726 Color: 4
Size: 150507 Color: 4
Size: 135763 Color: 3

Bin 794: 5 of cap free
Amount of items: 2
Items: 
Size: 685006 Color: 4
Size: 314990 Color: 1

Bin 795: 5 of cap free
Amount of items: 3
Items: 
Size: 505798 Color: 0
Size: 311398 Color: 2
Size: 182800 Color: 0

Bin 796: 5 of cap free
Amount of items: 3
Items: 
Size: 369388 Color: 1
Size: 353618 Color: 4
Size: 276990 Color: 2

Bin 797: 5 of cap free
Amount of items: 3
Items: 
Size: 663738 Color: 3
Size: 169132 Color: 2
Size: 167126 Color: 4

Bin 798: 5 of cap free
Amount of items: 3
Items: 
Size: 660725 Color: 4
Size: 170668 Color: 1
Size: 168603 Color: 3

Bin 799: 5 of cap free
Amount of items: 3
Items: 
Size: 373476 Color: 1
Size: 318772 Color: 2
Size: 307748 Color: 0

Bin 800: 5 of cap free
Amount of items: 2
Items: 
Size: 523026 Color: 2
Size: 476970 Color: 4

Bin 801: 5 of cap free
Amount of items: 2
Items: 
Size: 678363 Color: 1
Size: 321633 Color: 0

Bin 802: 5 of cap free
Amount of items: 2
Items: 
Size: 563954 Color: 3
Size: 436042 Color: 4

Bin 803: 5 of cap free
Amount of items: 2
Items: 
Size: 757483 Color: 4
Size: 242513 Color: 3

Bin 804: 5 of cap free
Amount of items: 2
Items: 
Size: 725561 Color: 0
Size: 274435 Color: 3

Bin 805: 5 of cap free
Amount of items: 3
Items: 
Size: 402239 Color: 4
Size: 312525 Color: 4
Size: 285232 Color: 1

Bin 806: 6 of cap free
Amount of items: 3
Items: 
Size: 377250 Color: 1
Size: 316367 Color: 2
Size: 306378 Color: 2

Bin 807: 6 of cap free
Amount of items: 3
Items: 
Size: 398475 Color: 4
Size: 303135 Color: 0
Size: 298385 Color: 0

Bin 808: 6 of cap free
Amount of items: 2
Items: 
Size: 520932 Color: 4
Size: 479063 Color: 1

Bin 809: 6 of cap free
Amount of items: 2
Items: 
Size: 558440 Color: 3
Size: 441555 Color: 4

Bin 810: 6 of cap free
Amount of items: 2
Items: 
Size: 566871 Color: 1
Size: 433124 Color: 2

Bin 811: 6 of cap free
Amount of items: 2
Items: 
Size: 567363 Color: 2
Size: 432632 Color: 4

Bin 812: 6 of cap free
Amount of items: 2
Items: 
Size: 577128 Color: 4
Size: 422867 Color: 3

Bin 813: 6 of cap free
Amount of items: 2
Items: 
Size: 582362 Color: 4
Size: 417633 Color: 3

Bin 814: 6 of cap free
Amount of items: 2
Items: 
Size: 585126 Color: 1
Size: 414869 Color: 2

Bin 815: 6 of cap free
Amount of items: 2
Items: 
Size: 607278 Color: 2
Size: 392717 Color: 0

Bin 816: 6 of cap free
Amount of items: 2
Items: 
Size: 609281 Color: 3
Size: 390714 Color: 4

Bin 817: 6 of cap free
Amount of items: 3
Items: 
Size: 624354 Color: 0
Size: 187859 Color: 4
Size: 187782 Color: 1

Bin 818: 6 of cap free
Amount of items: 2
Items: 
Size: 626896 Color: 0
Size: 373099 Color: 4

Bin 819: 6 of cap free
Amount of items: 2
Items: 
Size: 642005 Color: 0
Size: 357990 Color: 1

Bin 820: 6 of cap free
Amount of items: 2
Items: 
Size: 644234 Color: 2
Size: 355761 Color: 4

Bin 821: 6 of cap free
Amount of items: 3
Items: 
Size: 645094 Color: 4
Size: 177517 Color: 4
Size: 177384 Color: 2

Bin 822: 6 of cap free
Amount of items: 2
Items: 
Size: 646540 Color: 4
Size: 353455 Color: 0

Bin 823: 6 of cap free
Amount of items: 2
Items: 
Size: 647072 Color: 3
Size: 352923 Color: 0

Bin 824: 6 of cap free
Amount of items: 2
Items: 
Size: 657974 Color: 2
Size: 342021 Color: 3

Bin 825: 6 of cap free
Amount of items: 2
Items: 
Size: 660136 Color: 2
Size: 339859 Color: 1

Bin 826: 6 of cap free
Amount of items: 2
Items: 
Size: 671265 Color: 4
Size: 328730 Color: 3

Bin 827: 6 of cap free
Amount of items: 2
Items: 
Size: 672619 Color: 2
Size: 327376 Color: 0

Bin 828: 6 of cap free
Amount of items: 2
Items: 
Size: 674846 Color: 4
Size: 325149 Color: 0

Bin 829: 6 of cap free
Amount of items: 3
Items: 
Size: 680775 Color: 4
Size: 159941 Color: 1
Size: 159279 Color: 0

Bin 830: 6 of cap free
Amount of items: 3
Items: 
Size: 682840 Color: 4
Size: 158746 Color: 1
Size: 158409 Color: 0

Bin 831: 6 of cap free
Amount of items: 2
Items: 
Size: 704466 Color: 0
Size: 295529 Color: 3

Bin 832: 6 of cap free
Amount of items: 2
Items: 
Size: 721797 Color: 2
Size: 278198 Color: 3

Bin 833: 6 of cap free
Amount of items: 3
Items: 
Size: 731689 Color: 2
Size: 135820 Color: 0
Size: 132486 Color: 3

Bin 834: 6 of cap free
Amount of items: 2
Items: 
Size: 734386 Color: 3
Size: 265609 Color: 4

Bin 835: 6 of cap free
Amount of items: 2
Items: 
Size: 744128 Color: 2
Size: 255867 Color: 4

Bin 836: 6 of cap free
Amount of items: 2
Items: 
Size: 755968 Color: 0
Size: 244027 Color: 2

Bin 837: 6 of cap free
Amount of items: 2
Items: 
Size: 758881 Color: 0
Size: 241114 Color: 4

Bin 838: 6 of cap free
Amount of items: 2
Items: 
Size: 762465 Color: 3
Size: 237530 Color: 2

Bin 839: 6 of cap free
Amount of items: 2
Items: 
Size: 764779 Color: 1
Size: 235216 Color: 3

Bin 840: 6 of cap free
Amount of items: 2
Items: 
Size: 790144 Color: 3
Size: 209851 Color: 1

Bin 841: 6 of cap free
Amount of items: 2
Items: 
Size: 797964 Color: 2
Size: 202031 Color: 0

Bin 842: 6 of cap free
Amount of items: 2
Items: 
Size: 799514 Color: 0
Size: 200481 Color: 2

Bin 843: 6 of cap free
Amount of items: 3
Items: 
Size: 764382 Color: 0
Size: 118098 Color: 3
Size: 117515 Color: 1

Bin 844: 6 of cap free
Amount of items: 3
Items: 
Size: 351856 Color: 3
Size: 340757 Color: 0
Size: 307382 Color: 1

Bin 845: 6 of cap free
Amount of items: 3
Items: 
Size: 667877 Color: 3
Size: 168858 Color: 3
Size: 163260 Color: 1

Bin 846: 6 of cap free
Amount of items: 3
Items: 
Size: 652895 Color: 1
Size: 174246 Color: 3
Size: 172854 Color: 0

Bin 847: 6 of cap free
Amount of items: 2
Items: 
Size: 779678 Color: 3
Size: 220317 Color: 4

Bin 848: 6 of cap free
Amount of items: 3
Items: 
Size: 400793 Color: 4
Size: 305211 Color: 0
Size: 293991 Color: 0

Bin 849: 6 of cap free
Amount of items: 2
Items: 
Size: 528025 Color: 0
Size: 471970 Color: 1

Bin 850: 6 of cap free
Amount of items: 2
Items: 
Size: 504621 Color: 3
Size: 495374 Color: 4

Bin 851: 6 of cap free
Amount of items: 3
Items: 
Size: 665883 Color: 0
Size: 167263 Color: 2
Size: 166849 Color: 1

Bin 852: 6 of cap free
Amount of items: 2
Items: 
Size: 579592 Color: 0
Size: 420403 Color: 4

Bin 853: 6 of cap free
Amount of items: 2
Items: 
Size: 549724 Color: 2
Size: 450271 Color: 4

Bin 854: 6 of cap free
Amount of items: 3
Items: 
Size: 607730 Color: 0
Size: 196919 Color: 2
Size: 195346 Color: 2

Bin 855: 6 of cap free
Amount of items: 3
Items: 
Size: 628803 Color: 0
Size: 185918 Color: 3
Size: 185274 Color: 0

Bin 856: 6 of cap free
Amount of items: 3
Items: 
Size: 636081 Color: 3
Size: 182078 Color: 0
Size: 181836 Color: 0

Bin 857: 6 of cap free
Amount of items: 3
Items: 
Size: 613810 Color: 1
Size: 193810 Color: 2
Size: 192375 Color: 0

Bin 858: 6 of cap free
Amount of items: 2
Items: 
Size: 696477 Color: 3
Size: 303518 Color: 1

Bin 859: 6 of cap free
Amount of items: 3
Items: 
Size: 736478 Color: 4
Size: 133279 Color: 1
Size: 130238 Color: 1

Bin 860: 6 of cap free
Amount of items: 3
Items: 
Size: 783884 Color: 3
Size: 108184 Color: 1
Size: 107927 Color: 3

Bin 861: 6 of cap free
Amount of items: 2
Items: 
Size: 655093 Color: 4
Size: 344902 Color: 0

Bin 862: 6 of cap free
Amount of items: 3
Items: 
Size: 368893 Color: 0
Size: 366565 Color: 2
Size: 264537 Color: 2

Bin 863: 6 of cap free
Amount of items: 3
Items: 
Size: 729430 Color: 3
Size: 145781 Color: 2
Size: 124784 Color: 1

Bin 864: 6 of cap free
Amount of items: 3
Items: 
Size: 611369 Color: 4
Size: 194329 Color: 1
Size: 194297 Color: 0

Bin 865: 6 of cap free
Amount of items: 3
Items: 
Size: 610663 Color: 0
Size: 195033 Color: 0
Size: 194299 Color: 1

Bin 866: 6 of cap free
Amount of items: 3
Items: 
Size: 661193 Color: 1
Size: 169908 Color: 2
Size: 168894 Color: 1

Bin 867: 6 of cap free
Amount of items: 3
Items: 
Size: 717475 Color: 0
Size: 141762 Color: 1
Size: 140758 Color: 3

Bin 868: 6 of cap free
Amount of items: 3
Items: 
Size: 381180 Color: 0
Size: 343638 Color: 3
Size: 275177 Color: 4

Bin 869: 6 of cap free
Amount of items: 3
Items: 
Size: 680839 Color: 1
Size: 160582 Color: 4
Size: 158574 Color: 0

Bin 870: 6 of cap free
Amount of items: 3
Items: 
Size: 380494 Color: 0
Size: 349618 Color: 4
Size: 269883 Color: 1

Bin 871: 6 of cap free
Amount of items: 3
Items: 
Size: 779348 Color: 1
Size: 111769 Color: 2
Size: 108878 Color: 2

Bin 872: 6 of cap free
Amount of items: 2
Items: 
Size: 796293 Color: 1
Size: 203702 Color: 2

Bin 873: 6 of cap free
Amount of items: 2
Items: 
Size: 739351 Color: 1
Size: 260644 Color: 3

Bin 874: 6 of cap free
Amount of items: 2
Items: 
Size: 506957 Color: 3
Size: 493038 Color: 0

Bin 875: 6 of cap free
Amount of items: 2
Items: 
Size: 640218 Color: 4
Size: 359777 Color: 2

Bin 876: 6 of cap free
Amount of items: 2
Items: 
Size: 780451 Color: 1
Size: 219544 Color: 4

Bin 877: 6 of cap free
Amount of items: 3
Items: 
Size: 553559 Color: 0
Size: 223914 Color: 1
Size: 222522 Color: 3

Bin 878: 6 of cap free
Amount of items: 2
Items: 
Size: 685963 Color: 3
Size: 314032 Color: 4

Bin 879: 6 of cap free
Amount of items: 4
Items: 
Size: 421552 Color: 1
Size: 238719 Color: 3
Size: 235301 Color: 3
Size: 104423 Color: 0

Bin 880: 6 of cap free
Amount of items: 2
Items: 
Size: 501501 Color: 4
Size: 498494 Color: 1

Bin 881: 6 of cap free
Amount of items: 2
Items: 
Size: 738045 Color: 0
Size: 261950 Color: 2

Bin 882: 6 of cap free
Amount of items: 2
Items: 
Size: 525023 Color: 0
Size: 474972 Color: 2

Bin 883: 6 of cap free
Amount of items: 3
Items: 
Size: 348647 Color: 0
Size: 325892 Color: 2
Size: 325456 Color: 1

Bin 884: 6 of cap free
Amount of items: 3
Items: 
Size: 363284 Color: 2
Size: 318386 Color: 4
Size: 318325 Color: 3

Bin 885: 6 of cap free
Amount of items: 3
Items: 
Size: 500689 Color: 4
Size: 369112 Color: 0
Size: 130194 Color: 2

Bin 886: 7 of cap free
Amount of items: 2
Items: 
Size: 795239 Color: 4
Size: 204755 Color: 3

Bin 887: 7 of cap free
Amount of items: 2
Items: 
Size: 510892 Color: 4
Size: 489102 Color: 3

Bin 888: 7 of cap free
Amount of items: 2
Items: 
Size: 518288 Color: 3
Size: 481706 Color: 2

Bin 889: 7 of cap free
Amount of items: 2
Items: 
Size: 521737 Color: 0
Size: 478257 Color: 1

Bin 890: 7 of cap free
Amount of items: 2
Items: 
Size: 526000 Color: 1
Size: 473994 Color: 0

Bin 891: 7 of cap free
Amount of items: 2
Items: 
Size: 528250 Color: 4
Size: 471744 Color: 1

Bin 892: 7 of cap free
Amount of items: 2
Items: 
Size: 539930 Color: 1
Size: 460064 Color: 3

Bin 893: 7 of cap free
Amount of items: 2
Items: 
Size: 552280 Color: 0
Size: 447714 Color: 3

Bin 894: 7 of cap free
Amount of items: 2
Items: 
Size: 566506 Color: 3
Size: 433488 Color: 2

Bin 895: 7 of cap free
Amount of items: 2
Items: 
Size: 568580 Color: 4
Size: 431414 Color: 3

Bin 896: 7 of cap free
Amount of items: 2
Items: 
Size: 574339 Color: 3
Size: 425655 Color: 0

Bin 897: 7 of cap free
Amount of items: 2
Items: 
Size: 577371 Color: 4
Size: 422623 Color: 1

Bin 898: 7 of cap free
Amount of items: 2
Items: 
Size: 581544 Color: 2
Size: 418450 Color: 4

Bin 899: 7 of cap free
Amount of items: 2
Items: 
Size: 585799 Color: 1
Size: 414195 Color: 0

Bin 900: 7 of cap free
Amount of items: 2
Items: 
Size: 586403 Color: 2
Size: 413591 Color: 3

Bin 901: 7 of cap free
Amount of items: 2
Items: 
Size: 620595 Color: 2
Size: 379399 Color: 3

Bin 902: 7 of cap free
Amount of items: 2
Items: 
Size: 636332 Color: 1
Size: 363662 Color: 3

Bin 903: 7 of cap free
Amount of items: 3
Items: 
Size: 648117 Color: 3
Size: 176436 Color: 2
Size: 175441 Color: 2

Bin 904: 7 of cap free
Amount of items: 3
Items: 
Size: 648845 Color: 1
Size: 176029 Color: 3
Size: 175120 Color: 2

Bin 905: 7 of cap free
Amount of items: 2
Items: 
Size: 653796 Color: 1
Size: 346198 Color: 2

Bin 906: 7 of cap free
Amount of items: 2
Items: 
Size: 655976 Color: 4
Size: 344018 Color: 0

Bin 907: 7 of cap free
Amount of items: 3
Items: 
Size: 673895 Color: 0
Size: 163635 Color: 3
Size: 162464 Color: 2

Bin 908: 7 of cap free
Amount of items: 2
Items: 
Size: 673995 Color: 2
Size: 325999 Color: 4

Bin 909: 7 of cap free
Amount of items: 2
Items: 
Size: 685972 Color: 1
Size: 314022 Color: 2

Bin 910: 7 of cap free
Amount of items: 3
Items: 
Size: 686449 Color: 4
Size: 156915 Color: 4
Size: 156630 Color: 0

Bin 911: 7 of cap free
Amount of items: 2
Items: 
Size: 688558 Color: 1
Size: 311436 Color: 3

Bin 912: 7 of cap free
Amount of items: 2
Items: 
Size: 698672 Color: 1
Size: 301322 Color: 0

Bin 913: 7 of cap free
Amount of items: 3
Items: 
Size: 700683 Color: 0
Size: 149685 Color: 1
Size: 149626 Color: 1

Bin 914: 7 of cap free
Amount of items: 3
Items: 
Size: 708609 Color: 3
Size: 145897 Color: 0
Size: 145488 Color: 2

Bin 915: 7 of cap free
Amount of items: 2
Items: 
Size: 718510 Color: 1
Size: 281484 Color: 0

Bin 916: 7 of cap free
Amount of items: 3
Items: 
Size: 722657 Color: 2
Size: 138927 Color: 0
Size: 138410 Color: 2

Bin 917: 7 of cap free
Amount of items: 2
Items: 
Size: 727759 Color: 1
Size: 272235 Color: 3

Bin 918: 7 of cap free
Amount of items: 2
Items: 
Size: 752233 Color: 3
Size: 247761 Color: 0

Bin 919: 7 of cap free
Amount of items: 2
Items: 
Size: 765147 Color: 3
Size: 234847 Color: 2

Bin 920: 7 of cap free
Amount of items: 2
Items: 
Size: 791910 Color: 4
Size: 208084 Color: 0

Bin 921: 7 of cap free
Amount of items: 2
Items: 
Size: 795425 Color: 2
Size: 204569 Color: 4

Bin 922: 7 of cap free
Amount of items: 3
Items: 
Size: 716525 Color: 1
Size: 141904 Color: 1
Size: 141565 Color: 4

Bin 923: 7 of cap free
Amount of items: 3
Items: 
Size: 643764 Color: 0
Size: 178121 Color: 3
Size: 178109 Color: 2

Bin 924: 7 of cap free
Amount of items: 2
Items: 
Size: 671360 Color: 0
Size: 328634 Color: 3

Bin 925: 7 of cap free
Amount of items: 3
Items: 
Size: 350475 Color: 4
Size: 325167 Color: 1
Size: 324352 Color: 4

Bin 926: 7 of cap free
Amount of items: 3
Items: 
Size: 664183 Color: 4
Size: 168538 Color: 3
Size: 167273 Color: 0

Bin 927: 7 of cap free
Amount of items: 3
Items: 
Size: 755773 Color: 4
Size: 124883 Color: 1
Size: 119338 Color: 4

Bin 928: 7 of cap free
Amount of items: 3
Items: 
Size: 745556 Color: 3
Size: 127659 Color: 0
Size: 126779 Color: 4

Bin 929: 7 of cap free
Amount of items: 3
Items: 
Size: 703048 Color: 1
Size: 148557 Color: 4
Size: 148389 Color: 2

Bin 930: 7 of cap free
Amount of items: 3
Items: 
Size: 715850 Color: 3
Size: 142534 Color: 3
Size: 141610 Color: 0

Bin 931: 7 of cap free
Amount of items: 3
Items: 
Size: 396826 Color: 2
Size: 338281 Color: 2
Size: 264887 Color: 1

Bin 932: 7 of cap free
Amount of items: 2
Items: 
Size: 612923 Color: 2
Size: 387071 Color: 0

Bin 933: 7 of cap free
Amount of items: 3
Items: 
Size: 605327 Color: 0
Size: 197783 Color: 3
Size: 196884 Color: 0

Bin 934: 7 of cap free
Amount of items: 3
Items: 
Size: 672630 Color: 1
Size: 163976 Color: 4
Size: 163388 Color: 2

Bin 935: 7 of cap free
Amount of items: 2
Items: 
Size: 520978 Color: 0
Size: 479016 Color: 3

Bin 936: 7 of cap free
Amount of items: 3
Items: 
Size: 523714 Color: 0
Size: 239774 Color: 0
Size: 236506 Color: 2

Bin 937: 7 of cap free
Amount of items: 2
Items: 
Size: 500606 Color: 1
Size: 499388 Color: 4

Bin 938: 7 of cap free
Amount of items: 2
Items: 
Size: 639840 Color: 3
Size: 360154 Color: 4

Bin 939: 7 of cap free
Amount of items: 3
Items: 
Size: 605410 Color: 4
Size: 199017 Color: 2
Size: 195567 Color: 2

Bin 940: 7 of cap free
Amount of items: 2
Items: 
Size: 676526 Color: 0
Size: 323468 Color: 3

Bin 941: 7 of cap free
Amount of items: 3
Items: 
Size: 744081 Color: 4
Size: 128563 Color: 3
Size: 127350 Color: 4

Bin 942: 7 of cap free
Amount of items: 3
Items: 
Size: 399548 Color: 3
Size: 322038 Color: 1
Size: 278408 Color: 4

Bin 943: 7 of cap free
Amount of items: 3
Items: 
Size: 561448 Color: 0
Size: 219864 Color: 3
Size: 218682 Color: 4

Bin 944: 7 of cap free
Amount of items: 3
Items: 
Size: 645057 Color: 2
Size: 185591 Color: 3
Size: 169346 Color: 4

Bin 945: 7 of cap free
Amount of items: 3
Items: 
Size: 648308 Color: 1
Size: 175947 Color: 3
Size: 175739 Color: 0

Bin 946: 7 of cap free
Amount of items: 2
Items: 
Size: 646588 Color: 0
Size: 353406 Color: 4

Bin 947: 7 of cap free
Amount of items: 2
Items: 
Size: 629434 Color: 4
Size: 370560 Color: 3

Bin 948: 7 of cap free
Amount of items: 2
Items: 
Size: 545234 Color: 4
Size: 454760 Color: 2

Bin 949: 7 of cap free
Amount of items: 2
Items: 
Size: 666529 Color: 4
Size: 333465 Color: 1

Bin 950: 7 of cap free
Amount of items: 2
Items: 
Size: 664667 Color: 3
Size: 335327 Color: 1

Bin 951: 7 of cap free
Amount of items: 2
Items: 
Size: 647170 Color: 1
Size: 352824 Color: 2

Bin 952: 7 of cap free
Amount of items: 3
Items: 
Size: 509469 Color: 1
Size: 307614 Color: 1
Size: 182911 Color: 2

Bin 953: 7 of cap free
Amount of items: 3
Items: 
Size: 356185 Color: 1
Size: 340756 Color: 4
Size: 303053 Color: 4

Bin 954: 7 of cap free
Amount of items: 3
Items: 
Size: 440674 Color: 0
Size: 283851 Color: 0
Size: 275469 Color: 4

Bin 955: 7 of cap free
Amount of items: 4
Items: 
Size: 265707 Color: 1
Size: 264253 Color: 2
Size: 256322 Color: 1
Size: 213712 Color: 3

Bin 956: 8 of cap free
Amount of items: 3
Items: 
Size: 648297 Color: 3
Size: 175912 Color: 4
Size: 175784 Color: 0

Bin 957: 8 of cap free
Amount of items: 2
Items: 
Size: 531028 Color: 1
Size: 468965 Color: 2

Bin 958: 8 of cap free
Amount of items: 2
Items: 
Size: 543576 Color: 1
Size: 456417 Color: 2

Bin 959: 8 of cap free
Amount of items: 2
Items: 
Size: 554040 Color: 0
Size: 445953 Color: 1

Bin 960: 8 of cap free
Amount of items: 2
Items: 
Size: 554727 Color: 1
Size: 445266 Color: 0

Bin 961: 8 of cap free
Amount of items: 2
Items: 
Size: 564326 Color: 1
Size: 435667 Color: 4

Bin 962: 8 of cap free
Amount of items: 2
Items: 
Size: 607746 Color: 1
Size: 392247 Color: 4

Bin 963: 8 of cap free
Amount of items: 2
Items: 
Size: 641118 Color: 0
Size: 358875 Color: 4

Bin 964: 8 of cap free
Amount of items: 3
Items: 
Size: 652380 Color: 3
Size: 174345 Color: 2
Size: 173268 Color: 1

Bin 965: 8 of cap free
Amount of items: 3
Items: 
Size: 666138 Color: 0
Size: 167872 Color: 2
Size: 165983 Color: 2

Bin 966: 8 of cap free
Amount of items: 2
Items: 
Size: 688218 Color: 0
Size: 311775 Color: 2

Bin 967: 8 of cap free
Amount of items: 2
Items: 
Size: 689453 Color: 1
Size: 310540 Color: 4

Bin 968: 8 of cap free
Amount of items: 2
Items: 
Size: 691236 Color: 2
Size: 308757 Color: 4

Bin 969: 8 of cap free
Amount of items: 3
Items: 
Size: 693456 Color: 0
Size: 153502 Color: 2
Size: 153035 Color: 2

Bin 970: 8 of cap free
Amount of items: 2
Items: 
Size: 706621 Color: 0
Size: 293372 Color: 3

Bin 971: 8 of cap free
Amount of items: 2
Items: 
Size: 711041 Color: 4
Size: 288952 Color: 3

Bin 972: 8 of cap free
Amount of items: 3
Items: 
Size: 712040 Color: 0
Size: 144169 Color: 2
Size: 143784 Color: 3

Bin 973: 8 of cap free
Amount of items: 2
Items: 
Size: 716571 Color: 3
Size: 283422 Color: 1

Bin 974: 8 of cap free
Amount of items: 2
Items: 
Size: 722502 Color: 2
Size: 277491 Color: 0

Bin 975: 8 of cap free
Amount of items: 3
Items: 
Size: 729946 Color: 1
Size: 135043 Color: 4
Size: 135004 Color: 2

Bin 976: 8 of cap free
Amount of items: 2
Items: 
Size: 730751 Color: 1
Size: 269242 Color: 3

Bin 977: 8 of cap free
Amount of items: 2
Items: 
Size: 750461 Color: 4
Size: 249532 Color: 3

Bin 978: 8 of cap free
Amount of items: 2
Items: 
Size: 786344 Color: 4
Size: 213649 Color: 2

Bin 979: 8 of cap free
Amount of items: 2
Items: 
Size: 733581 Color: 0
Size: 266412 Color: 3

Bin 980: 8 of cap free
Amount of items: 3
Items: 
Size: 651904 Color: 2
Size: 175266 Color: 1
Size: 172823 Color: 1

Bin 981: 8 of cap free
Amount of items: 3
Items: 
Size: 350714 Color: 0
Size: 339797 Color: 4
Size: 309482 Color: 3

Bin 982: 8 of cap free
Amount of items: 2
Items: 
Size: 752338 Color: 1
Size: 247655 Color: 3

Bin 983: 8 of cap free
Amount of items: 3
Items: 
Size: 619687 Color: 3
Size: 190306 Color: 1
Size: 190000 Color: 1

Bin 984: 8 of cap free
Amount of items: 3
Items: 
Size: 384438 Color: 2
Size: 316894 Color: 0
Size: 298661 Color: 0

Bin 985: 8 of cap free
Amount of items: 2
Items: 
Size: 794207 Color: 4
Size: 205786 Color: 3

Bin 986: 8 of cap free
Amount of items: 2
Items: 
Size: 797619 Color: 0
Size: 202374 Color: 3

Bin 987: 8 of cap free
Amount of items: 2
Items: 
Size: 513666 Color: 1
Size: 486327 Color: 0

Bin 988: 8 of cap free
Amount of items: 3
Items: 
Size: 593263 Color: 4
Size: 203403 Color: 0
Size: 203327 Color: 4

Bin 989: 8 of cap free
Amount of items: 2
Items: 
Size: 769168 Color: 2
Size: 230825 Color: 4

Bin 990: 8 of cap free
Amount of items: 2
Items: 
Size: 689934 Color: 1
Size: 310059 Color: 0

Bin 991: 8 of cap free
Amount of items: 3
Items: 
Size: 627409 Color: 0
Size: 186381 Color: 0
Size: 186203 Color: 1

Bin 992: 8 of cap free
Amount of items: 2
Items: 
Size: 751798 Color: 2
Size: 248195 Color: 4

Bin 993: 8 of cap free
Amount of items: 3
Items: 
Size: 585228 Color: 3
Size: 207647 Color: 2
Size: 207118 Color: 3

Bin 994: 8 of cap free
Amount of items: 2
Items: 
Size: 504027 Color: 4
Size: 495966 Color: 1

Bin 995: 8 of cap free
Amount of items: 2
Items: 
Size: 724012 Color: 1
Size: 275981 Color: 2

Bin 996: 8 of cap free
Amount of items: 3
Items: 
Size: 688870 Color: 2
Size: 157573 Color: 4
Size: 153550 Color: 3

Bin 997: 8 of cap free
Amount of items: 2
Items: 
Size: 611272 Color: 2
Size: 388721 Color: 0

Bin 998: 8 of cap free
Amount of items: 3
Items: 
Size: 422208 Color: 3
Size: 312600 Color: 2
Size: 265185 Color: 3

Bin 999: 9 of cap free
Amount of items: 2
Items: 
Size: 732832 Color: 2
Size: 267160 Color: 3

Bin 1000: 9 of cap free
Amount of items: 3
Items: 
Size: 608781 Color: 0
Size: 195761 Color: 2
Size: 195450 Color: 1

Bin 1001: 9 of cap free
Amount of items: 3
Items: 
Size: 353285 Color: 4
Size: 343438 Color: 2
Size: 303269 Color: 1

Bin 1002: 9 of cap free
Amount of items: 2
Items: 
Size: 536477 Color: 3
Size: 463515 Color: 4

Bin 1003: 9 of cap free
Amount of items: 2
Items: 
Size: 540132 Color: 0
Size: 459860 Color: 3

Bin 1004: 9 of cap free
Amount of items: 2
Items: 
Size: 548807 Color: 1
Size: 451185 Color: 4

Bin 1005: 9 of cap free
Amount of items: 2
Items: 
Size: 549625 Color: 3
Size: 450367 Color: 1

Bin 1006: 9 of cap free
Amount of items: 2
Items: 
Size: 564720 Color: 1
Size: 435272 Color: 4

Bin 1007: 9 of cap free
Amount of items: 2
Items: 
Size: 568331 Color: 0
Size: 431661 Color: 3

Bin 1008: 9 of cap free
Amount of items: 2
Items: 
Size: 602627 Color: 2
Size: 397365 Color: 3

Bin 1009: 9 of cap free
Amount of items: 3
Items: 
Size: 614812 Color: 2
Size: 193065 Color: 3
Size: 192115 Color: 0

Bin 1010: 9 of cap free
Amount of items: 2
Items: 
Size: 614795 Color: 0
Size: 385197 Color: 3

Bin 1011: 9 of cap free
Amount of items: 3
Items: 
Size: 617112 Color: 2
Size: 191480 Color: 0
Size: 191400 Color: 0

Bin 1012: 9 of cap free
Amount of items: 2
Items: 
Size: 619721 Color: 2
Size: 380271 Color: 3

Bin 1013: 9 of cap free
Amount of items: 3
Items: 
Size: 629178 Color: 3
Size: 185581 Color: 2
Size: 185233 Color: 4

Bin 1014: 9 of cap free
Amount of items: 3
Items: 
Size: 643270 Color: 4
Size: 178844 Color: 3
Size: 177878 Color: 1

Bin 1015: 9 of cap free
Amount of items: 2
Items: 
Size: 646654 Color: 2
Size: 353338 Color: 4

Bin 1016: 9 of cap free
Amount of items: 2
Items: 
Size: 692640 Color: 3
Size: 307352 Color: 2

Bin 1017: 9 of cap free
Amount of items: 2
Items: 
Size: 703357 Color: 1
Size: 296635 Color: 2

Bin 1018: 9 of cap free
Amount of items: 2
Items: 
Size: 711547 Color: 4
Size: 288445 Color: 2

Bin 1019: 9 of cap free
Amount of items: 2
Items: 
Size: 714344 Color: 0
Size: 285648 Color: 3

Bin 1020: 9 of cap free
Amount of items: 2
Items: 
Size: 725302 Color: 0
Size: 274690 Color: 3

Bin 1021: 9 of cap free
Amount of items: 2
Items: 
Size: 727472 Color: 2
Size: 272520 Color: 3

Bin 1022: 9 of cap free
Amount of items: 2
Items: 
Size: 729672 Color: 1
Size: 270320 Color: 2

Bin 1023: 9 of cap free
Amount of items: 3
Items: 
Size: 747757 Color: 1
Size: 126328 Color: 2
Size: 125907 Color: 0

Bin 1024: 9 of cap free
Amount of items: 2
Items: 
Size: 749657 Color: 2
Size: 250335 Color: 3

Bin 1025: 9 of cap free
Amount of items: 2
Items: 
Size: 755174 Color: 3
Size: 244818 Color: 0

Bin 1026: 9 of cap free
Amount of items: 3
Items: 
Size: 634527 Color: 2
Size: 182808 Color: 2
Size: 182657 Color: 1

Bin 1027: 9 of cap free
Amount of items: 2
Items: 
Size: 651616 Color: 2
Size: 348376 Color: 1

Bin 1028: 9 of cap free
Amount of items: 3
Items: 
Size: 615855 Color: 2
Size: 192674 Color: 3
Size: 191463 Color: 1

Bin 1029: 9 of cap free
Amount of items: 2
Items: 
Size: 788238 Color: 3
Size: 211754 Color: 4

Bin 1030: 9 of cap free
Amount of items: 2
Items: 
Size: 627886 Color: 3
Size: 372106 Color: 1

Bin 1031: 9 of cap free
Amount of items: 2
Items: 
Size: 644360 Color: 4
Size: 355632 Color: 1

Bin 1032: 9 of cap free
Amount of items: 2
Items: 
Size: 635789 Color: 2
Size: 364203 Color: 4

Bin 1033: 9 of cap free
Amount of items: 2
Items: 
Size: 766289 Color: 1
Size: 233703 Color: 0

Bin 1034: 9 of cap free
Amount of items: 3
Items: 
Size: 716336 Color: 1
Size: 142296 Color: 1
Size: 141360 Color: 0

Bin 1035: 9 of cap free
Amount of items: 3
Items: 
Size: 653325 Color: 3
Size: 174052 Color: 2
Size: 172615 Color: 3

Bin 1036: 9 of cap free
Amount of items: 2
Items: 
Size: 721968 Color: 0
Size: 278024 Color: 3

Bin 1037: 9 of cap free
Amount of items: 3
Items: 
Size: 641234 Color: 2
Size: 179508 Color: 0
Size: 179250 Color: 4

Bin 1038: 9 of cap free
Amount of items: 3
Items: 
Size: 715280 Color: 1
Size: 142480 Color: 4
Size: 142232 Color: 1

Bin 1039: 9 of cap free
Amount of items: 2
Items: 
Size: 682101 Color: 2
Size: 317891 Color: 4

Bin 1040: 9 of cap free
Amount of items: 3
Items: 
Size: 631260 Color: 4
Size: 184378 Color: 2
Size: 184354 Color: 4

Bin 1041: 9 of cap free
Amount of items: 3
Items: 
Size: 727035 Color: 1
Size: 136868 Color: 4
Size: 136089 Color: 1

Bin 1042: 9 of cap free
Amount of items: 2
Items: 
Size: 736173 Color: 3
Size: 263819 Color: 4

Bin 1043: 9 of cap free
Amount of items: 2
Items: 
Size: 502802 Color: 0
Size: 497190 Color: 1

Bin 1044: 9 of cap free
Amount of items: 3
Items: 
Size: 707877 Color: 0
Size: 146553 Color: 0
Size: 145562 Color: 2

Bin 1045: 9 of cap free
Amount of items: 2
Items: 
Size: 755729 Color: 3
Size: 244263 Color: 0

Bin 1046: 9 of cap free
Amount of items: 2
Items: 
Size: 623010 Color: 1
Size: 376982 Color: 2

Bin 1047: 9 of cap free
Amount of items: 2
Items: 
Size: 570444 Color: 3
Size: 429548 Color: 0

Bin 1048: 9 of cap free
Amount of items: 2
Items: 
Size: 574486 Color: 1
Size: 425506 Color: 3

Bin 1049: 9 of cap free
Amount of items: 2
Items: 
Size: 515084 Color: 1
Size: 484908 Color: 0

Bin 1050: 9 of cap free
Amount of items: 2
Items: 
Size: 605912 Color: 4
Size: 394080 Color: 2

Bin 1051: 9 of cap free
Amount of items: 2
Items: 
Size: 791545 Color: 2
Size: 208447 Color: 0

Bin 1052: 9 of cap free
Amount of items: 3
Items: 
Size: 494920 Color: 4
Size: 353195 Color: 0
Size: 151877 Color: 0

Bin 1053: 10 of cap free
Amount of items: 3
Items: 
Size: 380853 Color: 1
Size: 366551 Color: 1
Size: 252587 Color: 3

Bin 1054: 10 of cap free
Amount of items: 2
Items: 
Size: 506422 Color: 2
Size: 493569 Color: 1

Bin 1055: 10 of cap free
Amount of items: 2
Items: 
Size: 525732 Color: 3
Size: 474259 Color: 2

Bin 1056: 10 of cap free
Amount of items: 2
Items: 
Size: 549451 Color: 3
Size: 450540 Color: 0

Bin 1057: 10 of cap free
Amount of items: 2
Items: 
Size: 559860 Color: 3
Size: 440131 Color: 2

Bin 1058: 10 of cap free
Amount of items: 2
Items: 
Size: 565480 Color: 3
Size: 434511 Color: 1

Bin 1059: 10 of cap free
Amount of items: 2
Items: 
Size: 572174 Color: 1
Size: 427817 Color: 3

Bin 1060: 10 of cap free
Amount of items: 2
Items: 
Size: 581307 Color: 2
Size: 418684 Color: 4

Bin 1061: 10 of cap free
Amount of items: 2
Items: 
Size: 581707 Color: 0
Size: 418284 Color: 1

Bin 1062: 10 of cap free
Amount of items: 2
Items: 
Size: 589206 Color: 2
Size: 410785 Color: 4

Bin 1063: 10 of cap free
Amount of items: 3
Items: 
Size: 621037 Color: 2
Size: 190496 Color: 4
Size: 188458 Color: 1

Bin 1064: 10 of cap free
Amount of items: 2
Items: 
Size: 622050 Color: 3
Size: 377941 Color: 1

Bin 1065: 10 of cap free
Amount of items: 2
Items: 
Size: 626974 Color: 3
Size: 373017 Color: 0

Bin 1066: 10 of cap free
Amount of items: 2
Items: 
Size: 633898 Color: 0
Size: 366093 Color: 1

Bin 1067: 10 of cap free
Amount of items: 2
Items: 
Size: 638530 Color: 1
Size: 361461 Color: 4

Bin 1068: 10 of cap free
Amount of items: 2
Items: 
Size: 648198 Color: 4
Size: 351793 Color: 1

Bin 1069: 10 of cap free
Amount of items: 2
Items: 
Size: 658666 Color: 1
Size: 341325 Color: 3

Bin 1070: 10 of cap free
Amount of items: 2
Items: 
Size: 681763 Color: 3
Size: 318228 Color: 1

Bin 1071: 10 of cap free
Amount of items: 2
Items: 
Size: 690397 Color: 2
Size: 309594 Color: 0

Bin 1072: 10 of cap free
Amount of items: 2
Items: 
Size: 706817 Color: 0
Size: 293174 Color: 1

Bin 1073: 10 of cap free
Amount of items: 3
Items: 
Size: 711294 Color: 0
Size: 144362 Color: 2
Size: 144335 Color: 2

Bin 1074: 10 of cap free
Amount of items: 2
Items: 
Size: 732811 Color: 3
Size: 267180 Color: 2

Bin 1075: 10 of cap free
Amount of items: 2
Items: 
Size: 747278 Color: 3
Size: 252713 Color: 4

Bin 1076: 10 of cap free
Amount of items: 2
Items: 
Size: 773235 Color: 3
Size: 226756 Color: 2

Bin 1077: 10 of cap free
Amount of items: 2
Items: 
Size: 788581 Color: 1
Size: 211410 Color: 3

Bin 1078: 10 of cap free
Amount of items: 2
Items: 
Size: 561463 Color: 4
Size: 438528 Color: 2

Bin 1079: 10 of cap free
Amount of items: 3
Items: 
Size: 694989 Color: 3
Size: 164626 Color: 1
Size: 140376 Color: 2

Bin 1080: 10 of cap free
Amount of items: 3
Items: 
Size: 645884 Color: 0
Size: 177223 Color: 0
Size: 176884 Color: 2

Bin 1081: 10 of cap free
Amount of items: 3
Items: 
Size: 626435 Color: 0
Size: 186887 Color: 1
Size: 186669 Color: 2

Bin 1082: 10 of cap free
Amount of items: 2
Items: 
Size: 584016 Color: 2
Size: 415975 Color: 3

Bin 1083: 10 of cap free
Amount of items: 3
Items: 
Size: 679304 Color: 2
Size: 161847 Color: 1
Size: 158840 Color: 1

Bin 1084: 10 of cap free
Amount of items: 2
Items: 
Size: 536657 Color: 2
Size: 463334 Color: 3

Bin 1085: 10 of cap free
Amount of items: 2
Items: 
Size: 633628 Color: 1
Size: 366363 Color: 3

Bin 1086: 10 of cap free
Amount of items: 2
Items: 
Size: 568743 Color: 1
Size: 431248 Color: 0

Bin 1087: 10 of cap free
Amount of items: 3
Items: 
Size: 665790 Color: 0
Size: 167489 Color: 0
Size: 166712 Color: 2

Bin 1088: 10 of cap free
Amount of items: 3
Items: 
Size: 699667 Color: 4
Size: 150482 Color: 0
Size: 149842 Color: 2

Bin 1089: 10 of cap free
Amount of items: 2
Items: 
Size: 667284 Color: 0
Size: 332707 Color: 2

Bin 1090: 10 of cap free
Amount of items: 2
Items: 
Size: 645417 Color: 0
Size: 354574 Color: 1

Bin 1091: 10 of cap free
Amount of items: 2
Items: 
Size: 725971 Color: 3
Size: 274020 Color: 2

Bin 1092: 10 of cap free
Amount of items: 3
Items: 
Size: 635158 Color: 2
Size: 184293 Color: 0
Size: 180540 Color: 3

Bin 1093: 10 of cap free
Amount of items: 3
Items: 
Size: 337270 Color: 0
Size: 335838 Color: 0
Size: 326883 Color: 2

Bin 1094: 10 of cap free
Amount of items: 2
Items: 
Size: 564144 Color: 0
Size: 435847 Color: 1

Bin 1095: 10 of cap free
Amount of items: 3
Items: 
Size: 658352 Color: 0
Size: 193182 Color: 3
Size: 148457 Color: 4

Bin 1096: 10 of cap free
Amount of items: 2
Items: 
Size: 721957 Color: 4
Size: 278034 Color: 0

Bin 1097: 10 of cap free
Amount of items: 2
Items: 
Size: 783550 Color: 1
Size: 216441 Color: 3

Bin 1098: 10 of cap free
Amount of items: 2
Items: 
Size: 728606 Color: 0
Size: 271385 Color: 1

Bin 1099: 10 of cap free
Amount of items: 2
Items: 
Size: 561118 Color: 2
Size: 438873 Color: 4

Bin 1100: 10 of cap free
Amount of items: 3
Items: 
Size: 375948 Color: 3
Size: 342532 Color: 0
Size: 281511 Color: 0

Bin 1101: 10 of cap free
Amount of items: 2
Items: 
Size: 767216 Color: 1
Size: 232775 Color: 2

Bin 1102: 10 of cap free
Amount of items: 3
Items: 
Size: 743188 Color: 0
Size: 129780 Color: 4
Size: 127023 Color: 2

Bin 1103: 10 of cap free
Amount of items: 3
Items: 
Size: 375657 Color: 4
Size: 340078 Color: 0
Size: 284256 Color: 1

Bin 1104: 10 of cap free
Amount of items: 3
Items: 
Size: 758012 Color: 2
Size: 122135 Color: 1
Size: 119844 Color: 3

Bin 1105: 10 of cap free
Amount of items: 2
Items: 
Size: 626028 Color: 3
Size: 373963 Color: 1

Bin 1106: 10 of cap free
Amount of items: 2
Items: 
Size: 783814 Color: 2
Size: 216177 Color: 0

Bin 1107: 10 of cap free
Amount of items: 3
Items: 
Size: 509242 Color: 3
Size: 251147 Color: 3
Size: 239602 Color: 2

Bin 1108: 11 of cap free
Amount of items: 2
Items: 
Size: 699027 Color: 3
Size: 300963 Color: 0

Bin 1109: 11 of cap free
Amount of items: 2
Items: 
Size: 519167 Color: 3
Size: 480823 Color: 1

Bin 1110: 11 of cap free
Amount of items: 2
Items: 
Size: 520286 Color: 1
Size: 479704 Color: 0

Bin 1111: 11 of cap free
Amount of items: 2
Items: 
Size: 532555 Color: 0
Size: 467435 Color: 1

Bin 1112: 11 of cap free
Amount of items: 2
Items: 
Size: 548383 Color: 2
Size: 451607 Color: 1

Bin 1113: 11 of cap free
Amount of items: 2
Items: 
Size: 554981 Color: 0
Size: 445009 Color: 1

Bin 1114: 11 of cap free
Amount of items: 2
Items: 
Size: 583791 Color: 0
Size: 416199 Color: 4

Bin 1115: 11 of cap free
Amount of items: 2
Items: 
Size: 589627 Color: 4
Size: 410363 Color: 0

Bin 1116: 11 of cap free
Amount of items: 2
Items: 
Size: 595433 Color: 4
Size: 404557 Color: 2

Bin 1117: 11 of cap free
Amount of items: 2
Items: 
Size: 647121 Color: 1
Size: 352869 Color: 0

Bin 1118: 11 of cap free
Amount of items: 2
Items: 
Size: 649714 Color: 0
Size: 350276 Color: 4

Bin 1119: 11 of cap free
Amount of items: 3
Items: 
Size: 663670 Color: 3
Size: 168387 Color: 3
Size: 167933 Color: 4

Bin 1120: 11 of cap free
Amount of items: 2
Items: 
Size: 690657 Color: 4
Size: 309333 Color: 2

Bin 1121: 11 of cap free
Amount of items: 2
Items: 
Size: 706903 Color: 0
Size: 293087 Color: 3

Bin 1122: 11 of cap free
Amount of items: 2
Items: 
Size: 709482 Color: 2
Size: 290508 Color: 1

Bin 1123: 11 of cap free
Amount of items: 2
Items: 
Size: 735515 Color: 2
Size: 264475 Color: 4

Bin 1124: 11 of cap free
Amount of items: 3
Items: 
Size: 740165 Color: 4
Size: 130713 Color: 1
Size: 129112 Color: 0

Bin 1125: 11 of cap free
Amount of items: 2
Items: 
Size: 752840 Color: 1
Size: 247150 Color: 0

Bin 1126: 11 of cap free
Amount of items: 2
Items: 
Size: 691310 Color: 4
Size: 308680 Color: 3

Bin 1127: 11 of cap free
Amount of items: 3
Items: 
Size: 731947 Color: 3
Size: 134603 Color: 0
Size: 133440 Color: 3

Bin 1128: 11 of cap free
Amount of items: 2
Items: 
Size: 643643 Color: 4
Size: 356347 Color: 3

Bin 1129: 11 of cap free
Amount of items: 3
Items: 
Size: 673864 Color: 2
Size: 165153 Color: 4
Size: 160973 Color: 1

Bin 1130: 11 of cap free
Amount of items: 3
Items: 
Size: 640277 Color: 1
Size: 183122 Color: 2
Size: 176591 Color: 2

Bin 1131: 11 of cap free
Amount of items: 3
Items: 
Size: 585059 Color: 3
Size: 207794 Color: 4
Size: 207137 Color: 3

Bin 1132: 11 of cap free
Amount of items: 2
Items: 
Size: 780047 Color: 0
Size: 219943 Color: 1

Bin 1133: 11 of cap free
Amount of items: 2
Items: 
Size: 776525 Color: 1
Size: 223465 Color: 0

Bin 1134: 11 of cap free
Amount of items: 2
Items: 
Size: 525467 Color: 2
Size: 474523 Color: 4

Bin 1135: 11 of cap free
Amount of items: 2
Items: 
Size: 793171 Color: 2
Size: 206819 Color: 3

Bin 1136: 11 of cap free
Amount of items: 3
Items: 
Size: 733306 Color: 3
Size: 148981 Color: 0
Size: 117703 Color: 0

Bin 1137: 11 of cap free
Amount of items: 3
Items: 
Size: 532738 Color: 2
Size: 234111 Color: 1
Size: 233141 Color: 4

Bin 1138: 11 of cap free
Amount of items: 3
Items: 
Size: 667073 Color: 4
Size: 167452 Color: 2
Size: 165465 Color: 0

Bin 1139: 11 of cap free
Amount of items: 3
Items: 
Size: 376928 Color: 1
Size: 366758 Color: 2
Size: 256304 Color: 4

Bin 1140: 11 of cap free
Amount of items: 3
Items: 
Size: 608428 Color: 3
Size: 195802 Color: 1
Size: 195760 Color: 3

Bin 1141: 11 of cap free
Amount of items: 2
Items: 
Size: 579312 Color: 3
Size: 420678 Color: 4

Bin 1142: 11 of cap free
Amount of items: 3
Items: 
Size: 643221 Color: 2
Size: 186850 Color: 3
Size: 169919 Color: 0

Bin 1143: 11 of cap free
Amount of items: 3
Items: 
Size: 673773 Color: 2
Size: 163574 Color: 2
Size: 162643 Color: 1

Bin 1144: 11 of cap free
Amount of items: 2
Items: 
Size: 694394 Color: 2
Size: 305596 Color: 0

Bin 1145: 11 of cap free
Amount of items: 2
Items: 
Size: 608356 Color: 0
Size: 391634 Color: 1

Bin 1146: 11 of cap free
Amount of items: 2
Items: 
Size: 608568 Color: 0
Size: 391422 Color: 4

Bin 1147: 11 of cap free
Amount of items: 2
Items: 
Size: 558501 Color: 2
Size: 441489 Color: 4

Bin 1148: 11 of cap free
Amount of items: 2
Items: 
Size: 709612 Color: 2
Size: 290378 Color: 0

Bin 1149: 11 of cap free
Amount of items: 2
Items: 
Size: 679226 Color: 4
Size: 320764 Color: 3

Bin 1150: 11 of cap free
Amount of items: 2
Items: 
Size: 521605 Color: 4
Size: 478385 Color: 3

Bin 1151: 11 of cap free
Amount of items: 2
Items: 
Size: 677369 Color: 2
Size: 322621 Color: 1

Bin 1152: 11 of cap free
Amount of items: 2
Items: 
Size: 748494 Color: 3
Size: 251496 Color: 1

Bin 1153: 11 of cap free
Amount of items: 3
Items: 
Size: 405886 Color: 0
Size: 325396 Color: 1
Size: 268708 Color: 1

Bin 1154: 11 of cap free
Amount of items: 4
Items: 
Size: 308884 Color: 4
Size: 303914 Color: 2
Size: 265260 Color: 2
Size: 121932 Color: 1

Bin 1155: 11 of cap free
Amount of items: 4
Items: 
Size: 286578 Color: 2
Size: 284515 Color: 4
Size: 258015 Color: 3
Size: 170882 Color: 2

Bin 1156: 11 of cap free
Amount of items: 4
Items: 
Size: 251819 Color: 4
Size: 251206 Color: 0
Size: 250400 Color: 3
Size: 246565 Color: 3

Bin 1157: 12 of cap free
Amount of items: 3
Items: 
Size: 398520 Color: 1
Size: 333112 Color: 2
Size: 268357 Color: 2

Bin 1158: 12 of cap free
Amount of items: 2
Items: 
Size: 524127 Color: 1
Size: 475862 Color: 4

Bin 1159: 12 of cap free
Amount of items: 2
Items: 
Size: 532132 Color: 3
Size: 467857 Color: 4

Bin 1160: 12 of cap free
Amount of items: 2
Items: 
Size: 543641 Color: 0
Size: 456348 Color: 2

Bin 1161: 12 of cap free
Amount of items: 2
Items: 
Size: 544435 Color: 0
Size: 455554 Color: 2

Bin 1162: 12 of cap free
Amount of items: 2
Items: 
Size: 597884 Color: 0
Size: 402105 Color: 1

Bin 1163: 12 of cap free
Amount of items: 2
Items: 
Size: 603507 Color: 0
Size: 396482 Color: 3

Bin 1164: 12 of cap free
Amount of items: 3
Items: 
Size: 604979 Color: 0
Size: 197593 Color: 3
Size: 197417 Color: 2

Bin 1165: 12 of cap free
Amount of items: 3
Items: 
Size: 609745 Color: 1
Size: 195149 Color: 4
Size: 195095 Color: 0

Bin 1166: 12 of cap free
Amount of items: 2
Items: 
Size: 631710 Color: 0
Size: 368279 Color: 3

Bin 1167: 12 of cap free
Amount of items: 2
Items: 
Size: 632518 Color: 0
Size: 367471 Color: 2

Bin 1168: 12 of cap free
Amount of items: 3
Items: 
Size: 642659 Color: 3
Size: 179129 Color: 0
Size: 178201 Color: 1

Bin 1169: 12 of cap free
Amount of items: 2
Items: 
Size: 663027 Color: 4
Size: 336962 Color: 1

Bin 1170: 12 of cap free
Amount of items: 2
Items: 
Size: 690016 Color: 3
Size: 309973 Color: 1

Bin 1171: 12 of cap free
Amount of items: 3
Items: 
Size: 691019 Color: 2
Size: 154798 Color: 0
Size: 154172 Color: 4

Bin 1172: 12 of cap free
Amount of items: 2
Items: 
Size: 695083 Color: 1
Size: 304906 Color: 4

Bin 1173: 12 of cap free
Amount of items: 2
Items: 
Size: 700951 Color: 4
Size: 299038 Color: 2

Bin 1174: 12 of cap free
Amount of items: 2
Items: 
Size: 702196 Color: 4
Size: 297793 Color: 3

Bin 1175: 12 of cap free
Amount of items: 2
Items: 
Size: 709287 Color: 3
Size: 290702 Color: 2

Bin 1176: 12 of cap free
Amount of items: 2
Items: 
Size: 719755 Color: 1
Size: 280234 Color: 0

Bin 1177: 12 of cap free
Amount of items: 2
Items: 
Size: 720035 Color: 0
Size: 279954 Color: 4

Bin 1178: 12 of cap free
Amount of items: 2
Items: 
Size: 740857 Color: 2
Size: 259132 Color: 4

Bin 1179: 12 of cap free
Amount of items: 2
Items: 
Size: 788988 Color: 4
Size: 211001 Color: 3

Bin 1180: 12 of cap free
Amount of items: 2
Items: 
Size: 738536 Color: 3
Size: 261453 Color: 2

Bin 1181: 12 of cap free
Amount of items: 2
Items: 
Size: 728642 Color: 4
Size: 271347 Color: 2

Bin 1182: 12 of cap free
Amount of items: 2
Items: 
Size: 774353 Color: 3
Size: 225636 Color: 1

Bin 1183: 12 of cap free
Amount of items: 2
Items: 
Size: 720849 Color: 3
Size: 279140 Color: 0

Bin 1184: 12 of cap free
Amount of items: 3
Items: 
Size: 572050 Color: 3
Size: 214058 Color: 3
Size: 213881 Color: 2

Bin 1185: 12 of cap free
Amount of items: 3
Items: 
Size: 628544 Color: 2
Size: 186072 Color: 2
Size: 185373 Color: 4

Bin 1186: 12 of cap free
Amount of items: 3
Items: 
Size: 374975 Color: 4
Size: 349406 Color: 3
Size: 275608 Color: 4

Bin 1187: 12 of cap free
Amount of items: 3
Items: 
Size: 680155 Color: 3
Size: 160022 Color: 2
Size: 159812 Color: 2

Bin 1188: 12 of cap free
Amount of items: 3
Items: 
Size: 638646 Color: 1
Size: 194152 Color: 3
Size: 167191 Color: 3

Bin 1189: 12 of cap free
Amount of items: 2
Items: 
Size: 632490 Color: 4
Size: 367499 Color: 2

Bin 1190: 12 of cap free
Amount of items: 3
Items: 
Size: 379733 Color: 0
Size: 317207 Color: 0
Size: 303049 Color: 3

Bin 1191: 12 of cap free
Amount of items: 3
Items: 
Size: 526686 Color: 0
Size: 236825 Color: 3
Size: 236478 Color: 3

Bin 1192: 12 of cap free
Amount of items: 2
Items: 
Size: 501964 Color: 3
Size: 498025 Color: 2

Bin 1193: 12 of cap free
Amount of items: 2
Items: 
Size: 775607 Color: 3
Size: 224382 Color: 2

Bin 1194: 12 of cap free
Amount of items: 3
Items: 
Size: 671058 Color: 2
Size: 164695 Color: 1
Size: 164236 Color: 4

Bin 1195: 12 of cap free
Amount of items: 3
Items: 
Size: 629383 Color: 4
Size: 185844 Color: 2
Size: 184762 Color: 4

Bin 1196: 12 of cap free
Amount of items: 2
Items: 
Size: 786502 Color: 2
Size: 213487 Color: 1

Bin 1197: 12 of cap free
Amount of items: 2
Items: 
Size: 666686 Color: 2
Size: 333303 Color: 1

Bin 1198: 12 of cap free
Amount of items: 3
Items: 
Size: 754144 Color: 0
Size: 124484 Color: 0
Size: 121361 Color: 4

Bin 1199: 12 of cap free
Amount of items: 2
Items: 
Size: 690229 Color: 0
Size: 309760 Color: 1

Bin 1200: 12 of cap free
Amount of items: 3
Items: 
Size: 633835 Color: 0
Size: 184514 Color: 4
Size: 181640 Color: 3

Bin 1201: 12 of cap free
Amount of items: 2
Items: 
Size: 541030 Color: 2
Size: 458959 Color: 3

Bin 1202: 12 of cap free
Amount of items: 2
Items: 
Size: 740121 Color: 2
Size: 259868 Color: 3

Bin 1203: 12 of cap free
Amount of items: 2
Items: 
Size: 668725 Color: 1
Size: 331264 Color: 0

Bin 1204: 12 of cap free
Amount of items: 2
Items: 
Size: 656816 Color: 1
Size: 343173 Color: 2

Bin 1205: 12 of cap free
Amount of items: 2
Items: 
Size: 661158 Color: 1
Size: 338831 Color: 0

Bin 1206: 12 of cap free
Amount of items: 2
Items: 
Size: 585908 Color: 4
Size: 414081 Color: 3

Bin 1207: 12 of cap free
Amount of items: 2
Items: 
Size: 632555 Color: 4
Size: 367434 Color: 0

Bin 1208: 12 of cap free
Amount of items: 2
Items: 
Size: 579874 Color: 3
Size: 420115 Color: 2

Bin 1209: 12 of cap free
Amount of items: 3
Items: 
Size: 338233 Color: 3
Size: 336092 Color: 0
Size: 325664 Color: 1

Bin 1210: 12 of cap free
Amount of items: 3
Items: 
Size: 403987 Color: 2
Size: 331680 Color: 2
Size: 264322 Color: 3

Bin 1211: 12 of cap free
Amount of items: 4
Items: 
Size: 265992 Color: 1
Size: 262617 Color: 2
Size: 251736 Color: 3
Size: 219644 Color: 3

Bin 1212: 13 of cap free
Amount of items: 2
Items: 
Size: 539509 Color: 0
Size: 460479 Color: 1

Bin 1213: 13 of cap free
Amount of items: 2
Items: 
Size: 565714 Color: 4
Size: 434274 Color: 1

Bin 1214: 13 of cap free
Amount of items: 2
Items: 
Size: 567840 Color: 0
Size: 432148 Color: 3

Bin 1215: 13 of cap free
Amount of items: 2
Items: 
Size: 590432 Color: 1
Size: 409556 Color: 2

Bin 1216: 13 of cap free
Amount of items: 2
Items: 
Size: 622546 Color: 0
Size: 377442 Color: 3

Bin 1217: 13 of cap free
Amount of items: 2
Items: 
Size: 654970 Color: 0
Size: 345018 Color: 2

Bin 1218: 13 of cap free
Amount of items: 2
Items: 
Size: 656716 Color: 0
Size: 343272 Color: 2

Bin 1219: 13 of cap free
Amount of items: 2
Items: 
Size: 658059 Color: 1
Size: 341929 Color: 4

Bin 1220: 13 of cap free
Amount of items: 2
Items: 
Size: 659618 Color: 2
Size: 340370 Color: 1

Bin 1221: 13 of cap free
Amount of items: 3
Items: 
Size: 663543 Color: 0
Size: 168265 Color: 2
Size: 168180 Color: 4

Bin 1222: 13 of cap free
Amount of items: 2
Items: 
Size: 668485 Color: 2
Size: 331503 Color: 0

Bin 1223: 13 of cap free
Amount of items: 2
Items: 
Size: 673916 Color: 0
Size: 326072 Color: 1

Bin 1224: 13 of cap free
Amount of items: 2
Items: 
Size: 678828 Color: 4
Size: 321160 Color: 3

Bin 1225: 13 of cap free
Amount of items: 2
Items: 
Size: 686466 Color: 4
Size: 313522 Color: 3

Bin 1226: 13 of cap free
Amount of items: 2
Items: 
Size: 695539 Color: 0
Size: 304449 Color: 4

Bin 1227: 13 of cap free
Amount of items: 2
Items: 
Size: 734554 Color: 0
Size: 265434 Color: 3

Bin 1228: 13 of cap free
Amount of items: 2
Items: 
Size: 738096 Color: 1
Size: 261892 Color: 2

Bin 1229: 13 of cap free
Amount of items: 2
Items: 
Size: 740813 Color: 0
Size: 259175 Color: 3

Bin 1230: 13 of cap free
Amount of items: 2
Items: 
Size: 789805 Color: 4
Size: 210183 Color: 2

Bin 1231: 13 of cap free
Amount of items: 3
Items: 
Size: 754855 Color: 1
Size: 124229 Color: 1
Size: 120904 Color: 3

Bin 1232: 13 of cap free
Amount of items: 2
Items: 
Size: 660256 Color: 0
Size: 339732 Color: 3

Bin 1233: 13 of cap free
Amount of items: 2
Items: 
Size: 535375 Color: 2
Size: 464613 Color: 1

Bin 1234: 13 of cap free
Amount of items: 2
Items: 
Size: 757117 Color: 1
Size: 242871 Color: 0

Bin 1235: 13 of cap free
Amount of items: 2
Items: 
Size: 568835 Color: 3
Size: 431153 Color: 4

Bin 1236: 13 of cap free
Amount of items: 2
Items: 
Size: 533838 Color: 2
Size: 466150 Color: 3

Bin 1237: 13 of cap free
Amount of items: 3
Items: 
Size: 696437 Color: 4
Size: 152049 Color: 0
Size: 151502 Color: 4

Bin 1238: 13 of cap free
Amount of items: 3
Items: 
Size: 692989 Color: 2
Size: 154883 Color: 3
Size: 152116 Color: 3

Bin 1239: 13 of cap free
Amount of items: 3
Items: 
Size: 745027 Color: 3
Size: 128796 Color: 1
Size: 126165 Color: 0

Bin 1240: 13 of cap free
Amount of items: 2
Items: 
Size: 646129 Color: 3
Size: 353859 Color: 1

Bin 1241: 13 of cap free
Amount of items: 2
Items: 
Size: 726253 Color: 4
Size: 273735 Color: 2

Bin 1242: 13 of cap free
Amount of items: 2
Items: 
Size: 602804 Color: 1
Size: 397184 Color: 0

Bin 1243: 13 of cap free
Amount of items: 2
Items: 
Size: 701019 Color: 3
Size: 298969 Color: 4

Bin 1244: 13 of cap free
Amount of items: 2
Items: 
Size: 722396 Color: 4
Size: 277592 Color: 0

Bin 1245: 13 of cap free
Amount of items: 2
Items: 
Size: 733535 Color: 3
Size: 266453 Color: 1

Bin 1246: 13 of cap free
Amount of items: 2
Items: 
Size: 603632 Color: 4
Size: 396356 Color: 0

Bin 1247: 13 of cap free
Amount of items: 2
Items: 
Size: 566629 Color: 3
Size: 433359 Color: 4

Bin 1248: 13 of cap free
Amount of items: 3
Items: 
Size: 375609 Color: 1
Size: 337140 Color: 0
Size: 287239 Color: 2

Bin 1249: 13 of cap free
Amount of items: 3
Items: 
Size: 400298 Color: 1
Size: 325215 Color: 2
Size: 274475 Color: 3

Bin 1250: 13 of cap free
Amount of items: 3
Items: 
Size: 501482 Color: 2
Size: 372465 Color: 1
Size: 126041 Color: 2

Bin 1251: 13 of cap free
Amount of items: 4
Items: 
Size: 291956 Color: 0
Size: 277679 Color: 4
Size: 262654 Color: 0
Size: 167699 Color: 1

Bin 1252: 13 of cap free
Amount of items: 4
Items: 
Size: 266710 Color: 4
Size: 258059 Color: 0
Size: 253233 Color: 2
Size: 221986 Color: 2

Bin 1253: 13 of cap free
Amount of items: 4
Items: 
Size: 265706 Color: 1
Size: 258294 Color: 2
Size: 251552 Color: 4
Size: 224436 Color: 1

Bin 1254: 14 of cap free
Amount of items: 3
Items: 
Size: 586007 Color: 4
Size: 207019 Color: 3
Size: 206961 Color: 3

Bin 1255: 14 of cap free
Amount of items: 2
Items: 
Size: 521935 Color: 4
Size: 478052 Color: 3

Bin 1256: 14 of cap free
Amount of items: 2
Items: 
Size: 525847 Color: 1
Size: 474140 Color: 2

Bin 1257: 14 of cap free
Amount of items: 2
Items: 
Size: 527114 Color: 3
Size: 472873 Color: 0

Bin 1258: 14 of cap free
Amount of items: 2
Items: 
Size: 539696 Color: 2
Size: 460291 Color: 4

Bin 1259: 14 of cap free
Amount of items: 2
Items: 
Size: 543381 Color: 3
Size: 456606 Color: 2

Bin 1260: 14 of cap free
Amount of items: 2
Items: 
Size: 553749 Color: 0
Size: 446238 Color: 1

Bin 1261: 14 of cap free
Amount of items: 3
Items: 
Size: 607221 Color: 3
Size: 196834 Color: 1
Size: 195932 Color: 1

Bin 1262: 14 of cap free
Amount of items: 3
Items: 
Size: 617923 Color: 3
Size: 191065 Color: 2
Size: 190999 Color: 4

Bin 1263: 14 of cap free
Amount of items: 2
Items: 
Size: 628730 Color: 3
Size: 371257 Color: 1

Bin 1264: 14 of cap free
Amount of items: 2
Items: 
Size: 654287 Color: 0
Size: 345700 Color: 4

Bin 1265: 14 of cap free
Amount of items: 2
Items: 
Size: 658806 Color: 1
Size: 341181 Color: 3

Bin 1266: 14 of cap free
Amount of items: 3
Items: 
Size: 660708 Color: 0
Size: 170075 Color: 2
Size: 169204 Color: 1

Bin 1267: 14 of cap free
Amount of items: 3
Items: 
Size: 686887 Color: 0
Size: 156826 Color: 3
Size: 156274 Color: 3

Bin 1268: 14 of cap free
Amount of items: 3
Items: 
Size: 692309 Color: 0
Size: 153994 Color: 1
Size: 153684 Color: 2

Bin 1269: 14 of cap free
Amount of items: 2
Items: 
Size: 704699 Color: 3
Size: 295288 Color: 2

Bin 1270: 14 of cap free
Amount of items: 2
Items: 
Size: 707490 Color: 1
Size: 292497 Color: 2

Bin 1271: 14 of cap free
Amount of items: 2
Items: 
Size: 749892 Color: 2
Size: 250095 Color: 4

Bin 1272: 14 of cap free
Amount of items: 3
Items: 
Size: 718280 Color: 3
Size: 141132 Color: 2
Size: 140575 Color: 3

Bin 1273: 14 of cap free
Amount of items: 2
Items: 
Size: 745713 Color: 4
Size: 254274 Color: 1

Bin 1274: 14 of cap free
Amount of items: 2
Items: 
Size: 525657 Color: 3
Size: 474330 Color: 4

Bin 1275: 14 of cap free
Amount of items: 3
Items: 
Size: 617296 Color: 2
Size: 191795 Color: 4
Size: 190896 Color: 0

Bin 1276: 14 of cap free
Amount of items: 3
Items: 
Size: 534913 Color: 4
Size: 233183 Color: 4
Size: 231891 Color: 0

Bin 1277: 14 of cap free
Amount of items: 2
Items: 
Size: 557995 Color: 2
Size: 441992 Color: 3

Bin 1278: 14 of cap free
Amount of items: 2
Items: 
Size: 688737 Color: 0
Size: 311250 Color: 1

Bin 1279: 14 of cap free
Amount of items: 2
Items: 
Size: 538232 Color: 1
Size: 461755 Color: 2

Bin 1280: 14 of cap free
Amount of items: 2
Items: 
Size: 771019 Color: 0
Size: 228968 Color: 2

Bin 1281: 14 of cap free
Amount of items: 2
Items: 
Size: 657108 Color: 2
Size: 342879 Color: 0

Bin 1282: 14 of cap free
Amount of items: 3
Items: 
Size: 575493 Color: 1
Size: 212420 Color: 2
Size: 212074 Color: 1

Bin 1283: 14 of cap free
Amount of items: 3
Items: 
Size: 376950 Color: 1
Size: 358852 Color: 2
Size: 264185 Color: 0

Bin 1284: 14 of cap free
Amount of items: 3
Items: 
Size: 641545 Color: 0
Size: 179234 Color: 1
Size: 179208 Color: 4

Bin 1285: 14 of cap free
Amount of items: 2
Items: 
Size: 549669 Color: 1
Size: 450318 Color: 2

Bin 1286: 14 of cap free
Amount of items: 2
Items: 
Size: 730042 Color: 3
Size: 269945 Color: 4

Bin 1287: 14 of cap free
Amount of items: 2
Items: 
Size: 537225 Color: 3
Size: 462762 Color: 4

Bin 1288: 14 of cap free
Amount of items: 2
Items: 
Size: 635352 Color: 4
Size: 364635 Color: 2

Bin 1289: 14 of cap free
Amount of items: 2
Items: 
Size: 704061 Color: 4
Size: 295926 Color: 0

Bin 1290: 14 of cap free
Amount of items: 2
Items: 
Size: 616363 Color: 0
Size: 383624 Color: 2

Bin 1291: 14 of cap free
Amount of items: 2
Items: 
Size: 716686 Color: 4
Size: 283301 Color: 0

Bin 1292: 14 of cap free
Amount of items: 2
Items: 
Size: 530616 Color: 1
Size: 469371 Color: 2

Bin 1293: 14 of cap free
Amount of items: 3
Items: 
Size: 507361 Color: 3
Size: 253186 Color: 0
Size: 239440 Color: 2

Bin 1294: 14 of cap free
Amount of items: 3
Items: 
Size: 556208 Color: 1
Size: 221890 Color: 2
Size: 221889 Color: 1

Bin 1295: 14 of cap free
Amount of items: 2
Items: 
Size: 695176 Color: 3
Size: 304811 Color: 4

Bin 1296: 14 of cap free
Amount of items: 2
Items: 
Size: 757117 Color: 1
Size: 242870 Color: 0

Bin 1297: 14 of cap free
Amount of items: 3
Items: 
Size: 628663 Color: 3
Size: 186488 Color: 2
Size: 184836 Color: 1

Bin 1298: 14 of cap free
Amount of items: 3
Items: 
Size: 571497 Color: 3
Size: 214364 Color: 2
Size: 214126 Color: 3

Bin 1299: 14 of cap free
Amount of items: 2
Items: 
Size: 563809 Color: 3
Size: 436178 Color: 1

Bin 1300: 14 of cap free
Amount of items: 2
Items: 
Size: 557085 Color: 2
Size: 442902 Color: 0

Bin 1301: 14 of cap free
Amount of items: 2
Items: 
Size: 672413 Color: 4
Size: 327574 Color: 3

Bin 1302: 14 of cap free
Amount of items: 2
Items: 
Size: 610357 Color: 0
Size: 389630 Color: 1

Bin 1303: 14 of cap free
Amount of items: 2
Items: 
Size: 782436 Color: 0
Size: 217551 Color: 3

Bin 1304: 15 of cap free
Amount of items: 3
Items: 
Size: 612800 Color: 2
Size: 194325 Color: 3
Size: 192861 Color: 1

Bin 1305: 15 of cap free
Amount of items: 3
Items: 
Size: 395290 Color: 4
Size: 315766 Color: 2
Size: 288930 Color: 2

Bin 1306: 15 of cap free
Amount of items: 3
Items: 
Size: 432289 Color: 3
Size: 299520 Color: 4
Size: 268177 Color: 1

Bin 1307: 15 of cap free
Amount of items: 2
Items: 
Size: 509798 Color: 3
Size: 490188 Color: 4

Bin 1308: 15 of cap free
Amount of items: 2
Items: 
Size: 533328 Color: 1
Size: 466658 Color: 0

Bin 1309: 15 of cap free
Amount of items: 2
Items: 
Size: 592210 Color: 1
Size: 407776 Color: 0

Bin 1310: 15 of cap free
Amount of items: 2
Items: 
Size: 602967 Color: 4
Size: 397019 Color: 3

Bin 1311: 15 of cap free
Amount of items: 2
Items: 
Size: 606520 Color: 3
Size: 393466 Color: 1

Bin 1312: 15 of cap free
Amount of items: 2
Items: 
Size: 626241 Color: 1
Size: 373745 Color: 0

Bin 1313: 15 of cap free
Amount of items: 2
Items: 
Size: 632798 Color: 1
Size: 367188 Color: 0

Bin 1314: 15 of cap free
Amount of items: 2
Items: 
Size: 645035 Color: 1
Size: 354951 Color: 0

Bin 1315: 15 of cap free
Amount of items: 2
Items: 
Size: 649818 Color: 1
Size: 350168 Color: 0

Bin 1316: 15 of cap free
Amount of items: 2
Items: 
Size: 650468 Color: 1
Size: 349518 Color: 0

Bin 1317: 15 of cap free
Amount of items: 2
Items: 
Size: 671261 Color: 1
Size: 328725 Color: 4

Bin 1318: 15 of cap free
Amount of items: 2
Items: 
Size: 708378 Color: 2
Size: 291608 Color: 3

Bin 1319: 15 of cap free
Amount of items: 2
Items: 
Size: 724214 Color: 4
Size: 275772 Color: 0

Bin 1320: 15 of cap free
Amount of items: 2
Items: 
Size: 736637 Color: 0
Size: 263349 Color: 2

Bin 1321: 15 of cap free
Amount of items: 2
Items: 
Size: 764937 Color: 1
Size: 235049 Color: 3

Bin 1322: 15 of cap free
Amount of items: 2
Items: 
Size: 774108 Color: 4
Size: 225878 Color: 3

Bin 1323: 15 of cap free
Amount of items: 2
Items: 
Size: 777751 Color: 2
Size: 222235 Color: 3

Bin 1324: 15 of cap free
Amount of items: 2
Items: 
Size: 795632 Color: 2
Size: 204354 Color: 0

Bin 1325: 15 of cap free
Amount of items: 2
Items: 
Size: 625387 Color: 3
Size: 374599 Color: 2

Bin 1326: 15 of cap free
Amount of items: 2
Items: 
Size: 698405 Color: 4
Size: 301581 Color: 0

Bin 1327: 15 of cap free
Amount of items: 2
Items: 
Size: 631154 Color: 4
Size: 368832 Color: 1

Bin 1328: 15 of cap free
Amount of items: 2
Items: 
Size: 533536 Color: 3
Size: 466450 Color: 1

Bin 1329: 15 of cap free
Amount of items: 2
Items: 
Size: 678292 Color: 2
Size: 321694 Color: 0

Bin 1330: 15 of cap free
Amount of items: 3
Items: 
Size: 763672 Color: 1
Size: 118532 Color: 3
Size: 117782 Color: 0

Bin 1331: 15 of cap free
Amount of items: 3
Items: 
Size: 634419 Color: 1
Size: 182858 Color: 4
Size: 182709 Color: 0

Bin 1332: 15 of cap free
Amount of items: 3
Items: 
Size: 385110 Color: 1
Size: 326117 Color: 2
Size: 288759 Color: 2

Bin 1333: 15 of cap free
Amount of items: 3
Items: 
Size: 399350 Color: 3
Size: 332147 Color: 2
Size: 268489 Color: 1

Bin 1334: 15 of cap free
Amount of items: 2
Items: 
Size: 774618 Color: 2
Size: 225368 Color: 3

Bin 1335: 15 of cap free
Amount of items: 2
Items: 
Size: 654946 Color: 0
Size: 345040 Color: 4

Bin 1336: 15 of cap free
Amount of items: 3
Items: 
Size: 340471 Color: 4
Size: 329985 Color: 2
Size: 329530 Color: 1

Bin 1337: 15 of cap free
Amount of items: 2
Items: 
Size: 532676 Color: 1
Size: 467310 Color: 0

Bin 1338: 15 of cap free
Amount of items: 3
Items: 
Size: 600855 Color: 4
Size: 199587 Color: 3
Size: 199544 Color: 1

Bin 1339: 15 of cap free
Amount of items: 3
Items: 
Size: 343044 Color: 3
Size: 337533 Color: 3
Size: 319409 Color: 1

Bin 1340: 15 of cap free
Amount of items: 2
Items: 
Size: 539493 Color: 0
Size: 460493 Color: 3

Bin 1341: 15 of cap free
Amount of items: 2
Items: 
Size: 756750 Color: 0
Size: 243236 Color: 1

Bin 1342: 15 of cap free
Amount of items: 3
Items: 
Size: 522606 Color: 0
Size: 240527 Color: 0
Size: 236853 Color: 3

Bin 1343: 15 of cap free
Amount of items: 2
Items: 
Size: 791406 Color: 1
Size: 208580 Color: 4

Bin 1344: 15 of cap free
Amount of items: 2
Items: 
Size: 549122 Color: 3
Size: 450864 Color: 2

Bin 1345: 15 of cap free
Amount of items: 3
Items: 
Size: 594162 Color: 1
Size: 203354 Color: 2
Size: 202470 Color: 1

Bin 1346: 15 of cap free
Amount of items: 2
Items: 
Size: 789199 Color: 3
Size: 210787 Color: 2

Bin 1347: 15 of cap free
Amount of items: 2
Items: 
Size: 506837 Color: 2
Size: 493149 Color: 4

Bin 1348: 16 of cap free
Amount of items: 3
Items: 
Size: 604254 Color: 0
Size: 197956 Color: 2
Size: 197775 Color: 3

Bin 1349: 16 of cap free
Amount of items: 3
Items: 
Size: 609059 Color: 0
Size: 195734 Color: 3
Size: 195192 Color: 4

Bin 1350: 16 of cap free
Amount of items: 3
Items: 
Size: 649023 Color: 3
Size: 175765 Color: 1
Size: 175197 Color: 3

Bin 1351: 16 of cap free
Amount of items: 2
Items: 
Size: 511404 Color: 4
Size: 488581 Color: 1

Bin 1352: 16 of cap free
Amount of items: 2
Items: 
Size: 529157 Color: 0
Size: 470828 Color: 2

Bin 1353: 16 of cap free
Amount of items: 2
Items: 
Size: 540959 Color: 1
Size: 459026 Color: 2

Bin 1354: 16 of cap free
Amount of items: 2
Items: 
Size: 559178 Color: 0
Size: 440807 Color: 3

Bin 1355: 16 of cap free
Amount of items: 2
Items: 
Size: 568189 Color: 1
Size: 431796 Color: 4

Bin 1356: 16 of cap free
Amount of items: 2
Items: 
Size: 573564 Color: 0
Size: 426421 Color: 4

Bin 1357: 16 of cap free
Amount of items: 2
Items: 
Size: 591199 Color: 0
Size: 408786 Color: 1

Bin 1358: 16 of cap free
Amount of items: 2
Items: 
Size: 603575 Color: 2
Size: 396410 Color: 0

Bin 1359: 16 of cap free
Amount of items: 3
Items: 
Size: 607992 Color: 1
Size: 196279 Color: 0
Size: 195714 Color: 3

Bin 1360: 16 of cap free
Amount of items: 2
Items: 
Size: 621543 Color: 3
Size: 378442 Color: 2

Bin 1361: 16 of cap free
Amount of items: 2
Items: 
Size: 622705 Color: 0
Size: 377280 Color: 4

Bin 1362: 16 of cap free
Amount of items: 2
Items: 
Size: 625046 Color: 0
Size: 374939 Color: 4

Bin 1363: 16 of cap free
Amount of items: 3
Items: 
Size: 664381 Color: 0
Size: 168101 Color: 3
Size: 167503 Color: 2

Bin 1364: 16 of cap free
Amount of items: 2
Items: 
Size: 665643 Color: 2
Size: 334342 Color: 1

Bin 1365: 16 of cap free
Amount of items: 2
Items: 
Size: 686795 Color: 4
Size: 313190 Color: 1

Bin 1366: 16 of cap free
Amount of items: 2
Items: 
Size: 688666 Color: 3
Size: 311319 Color: 1

Bin 1367: 16 of cap free
Amount of items: 2
Items: 
Size: 697956 Color: 1
Size: 302029 Color: 4

Bin 1368: 16 of cap free
Amount of items: 2
Items: 
Size: 751343 Color: 1
Size: 248642 Color: 4

Bin 1369: 16 of cap free
Amount of items: 2
Items: 
Size: 753408 Color: 1
Size: 246577 Color: 3

Bin 1370: 16 of cap free
Amount of items: 2
Items: 
Size: 762013 Color: 4
Size: 237972 Color: 0

Bin 1371: 16 of cap free
Amount of items: 2
Items: 
Size: 790395 Color: 4
Size: 209590 Color: 3

Bin 1372: 16 of cap free
Amount of items: 2
Items: 
Size: 655381 Color: 0
Size: 344604 Color: 1

Bin 1373: 16 of cap free
Amount of items: 3
Items: 
Size: 593430 Color: 2
Size: 207987 Color: 4
Size: 198568 Color: 2

Bin 1374: 16 of cap free
Amount of items: 3
Items: 
Size: 671307 Color: 3
Size: 164847 Color: 2
Size: 163831 Color: 4

Bin 1375: 16 of cap free
Amount of items: 3
Items: 
Size: 586255 Color: 3
Size: 206929 Color: 3
Size: 206801 Color: 2

Bin 1376: 16 of cap free
Amount of items: 3
Items: 
Size: 371317 Color: 3
Size: 340803 Color: 0
Size: 287865 Color: 4

Bin 1377: 16 of cap free
Amount of items: 3
Items: 
Size: 722790 Color: 1
Size: 139850 Color: 1
Size: 137345 Color: 0

Bin 1378: 16 of cap free
Amount of items: 3
Items: 
Size: 708421 Color: 3
Size: 148105 Color: 0
Size: 143459 Color: 3

Bin 1379: 16 of cap free
Amount of items: 3
Items: 
Size: 350678 Color: 4
Size: 324735 Color: 0
Size: 324572 Color: 4

Bin 1380: 16 of cap free
Amount of items: 2
Items: 
Size: 750354 Color: 2
Size: 249631 Color: 0

Bin 1381: 16 of cap free
Amount of items: 2
Items: 
Size: 775278 Color: 0
Size: 224707 Color: 4

Bin 1382: 16 of cap free
Amount of items: 2
Items: 
Size: 745003 Color: 1
Size: 254982 Color: 4

Bin 1383: 16 of cap free
Amount of items: 2
Items: 
Size: 577332 Color: 4
Size: 422653 Color: 2

Bin 1384: 16 of cap free
Amount of items: 2
Items: 
Size: 504601 Color: 1
Size: 495384 Color: 2

Bin 1385: 17 of cap free
Amount of items: 3
Items: 
Size: 636285 Color: 4
Size: 182059 Color: 3
Size: 181640 Color: 3

Bin 1386: 17 of cap free
Amount of items: 3
Items: 
Size: 621981 Color: 1
Size: 190198 Color: 1
Size: 187805 Color: 4

Bin 1387: 17 of cap free
Amount of items: 3
Items: 
Size: 400193 Color: 3
Size: 317722 Color: 0
Size: 282069 Color: 0

Bin 1388: 17 of cap free
Amount of items: 2
Items: 
Size: 530652 Color: 3
Size: 469332 Color: 0

Bin 1389: 17 of cap free
Amount of items: 2
Items: 
Size: 531452 Color: 1
Size: 468532 Color: 2

Bin 1390: 17 of cap free
Amount of items: 2
Items: 
Size: 535456 Color: 2
Size: 464528 Color: 0

Bin 1391: 17 of cap free
Amount of items: 3
Items: 
Size: 555509 Color: 4
Size: 222289 Color: 4
Size: 222186 Color: 3

Bin 1392: 17 of cap free
Amount of items: 2
Items: 
Size: 565311 Color: 2
Size: 434673 Color: 1

Bin 1393: 17 of cap free
Amount of items: 2
Items: 
Size: 582894 Color: 0
Size: 417090 Color: 1

Bin 1394: 17 of cap free
Amount of items: 2
Items: 
Size: 591010 Color: 0
Size: 408974 Color: 3

Bin 1395: 17 of cap free
Amount of items: 2
Items: 
Size: 595028 Color: 1
Size: 404956 Color: 4

Bin 1396: 17 of cap free
Amount of items: 2
Items: 
Size: 611918 Color: 4
Size: 388066 Color: 3

Bin 1397: 17 of cap free
Amount of items: 2
Items: 
Size: 632176 Color: 4
Size: 367808 Color: 0

Bin 1398: 17 of cap free
Amount of items: 2
Items: 
Size: 663614 Color: 0
Size: 336370 Color: 2

Bin 1399: 17 of cap free
Amount of items: 2
Items: 
Size: 684401 Color: 2
Size: 315583 Color: 4

Bin 1400: 17 of cap free
Amount of items: 2
Items: 
Size: 695737 Color: 1
Size: 304247 Color: 3

Bin 1401: 17 of cap free
Amount of items: 2
Items: 
Size: 713886 Color: 1
Size: 286098 Color: 3

Bin 1402: 17 of cap free
Amount of items: 3
Items: 
Size: 721473 Color: 4
Size: 139325 Color: 4
Size: 139186 Color: 0

Bin 1403: 17 of cap free
Amount of items: 2
Items: 
Size: 722594 Color: 3
Size: 277390 Color: 2

Bin 1404: 17 of cap free
Amount of items: 2
Items: 
Size: 722906 Color: 3
Size: 277078 Color: 4

Bin 1405: 17 of cap free
Amount of items: 2
Items: 
Size: 755909 Color: 3
Size: 244075 Color: 0

Bin 1406: 17 of cap free
Amount of items: 2
Items: 
Size: 783719 Color: 2
Size: 216265 Color: 0

Bin 1407: 17 of cap free
Amount of items: 3
Items: 
Size: 677972 Color: 0
Size: 161086 Color: 4
Size: 160926 Color: 2

Bin 1408: 17 of cap free
Amount of items: 2
Items: 
Size: 778610 Color: 4
Size: 221374 Color: 0

Bin 1409: 17 of cap free
Amount of items: 2
Items: 
Size: 506128 Color: 4
Size: 493856 Color: 2

Bin 1410: 17 of cap free
Amount of items: 2
Items: 
Size: 749110 Color: 2
Size: 250874 Color: 3

Bin 1411: 17 of cap free
Amount of items: 3
Items: 
Size: 697130 Color: 3
Size: 151573 Color: 4
Size: 151281 Color: 3

Bin 1412: 17 of cap free
Amount of items: 3
Items: 
Size: 620923 Color: 0
Size: 189898 Color: 1
Size: 189163 Color: 1

Bin 1413: 17 of cap free
Amount of items: 2
Items: 
Size: 579036 Color: 2
Size: 420948 Color: 0

Bin 1414: 17 of cap free
Amount of items: 3
Items: 
Size: 693656 Color: 4
Size: 153353 Color: 1
Size: 152975 Color: 3

Bin 1415: 17 of cap free
Amount of items: 3
Items: 
Size: 673692 Color: 3
Size: 163461 Color: 1
Size: 162831 Color: 0

Bin 1416: 17 of cap free
Amount of items: 2
Items: 
Size: 547431 Color: 2
Size: 452553 Color: 4

Bin 1417: 17 of cap free
Amount of items: 2
Items: 
Size: 639580 Color: 2
Size: 360404 Color: 0

Bin 1418: 17 of cap free
Amount of items: 3
Items: 
Size: 665139 Color: 1
Size: 167948 Color: 3
Size: 166897 Color: 1

Bin 1419: 17 of cap free
Amount of items: 3
Items: 
Size: 646350 Color: 0
Size: 177001 Color: 2
Size: 176633 Color: 1

Bin 1420: 17 of cap free
Amount of items: 2
Items: 
Size: 771380 Color: 4
Size: 228604 Color: 1

Bin 1421: 17 of cap free
Amount of items: 2
Items: 
Size: 602714 Color: 4
Size: 397270 Color: 2

Bin 1422: 17 of cap free
Amount of items: 2
Items: 
Size: 630347 Color: 2
Size: 369637 Color: 1

Bin 1423: 17 of cap free
Amount of items: 2
Items: 
Size: 636831 Color: 1
Size: 363153 Color: 3

Bin 1424: 17 of cap free
Amount of items: 2
Items: 
Size: 653113 Color: 3
Size: 346871 Color: 1

Bin 1425: 17 of cap free
Amount of items: 2
Items: 
Size: 577075 Color: 2
Size: 422909 Color: 0

Bin 1426: 17 of cap free
Amount of items: 2
Items: 
Size: 691920 Color: 2
Size: 308064 Color: 1

Bin 1427: 17 of cap free
Amount of items: 2
Items: 
Size: 622480 Color: 1
Size: 377504 Color: 3

Bin 1428: 17 of cap free
Amount of items: 2
Items: 
Size: 596349 Color: 0
Size: 403635 Color: 4

Bin 1429: 17 of cap free
Amount of items: 2
Items: 
Size: 789181 Color: 2
Size: 210803 Color: 3

Bin 1430: 17 of cap free
Amount of items: 2
Items: 
Size: 565587 Color: 3
Size: 434397 Color: 1

Bin 1431: 17 of cap free
Amount of items: 4
Items: 
Size: 277390 Color: 1
Size: 269719 Color: 2
Size: 264253 Color: 4
Size: 188622 Color: 4

Bin 1432: 18 of cap free
Amount of items: 3
Items: 
Size: 730232 Color: 4
Size: 151134 Color: 4
Size: 118617 Color: 2

Bin 1433: 18 of cap free
Amount of items: 2
Items: 
Size: 611762 Color: 2
Size: 388221 Color: 4

Bin 1434: 18 of cap free
Amount of items: 2
Items: 
Size: 514575 Color: 3
Size: 485408 Color: 0

Bin 1435: 18 of cap free
Amount of items: 2
Items: 
Size: 518867 Color: 3
Size: 481116 Color: 2

Bin 1436: 18 of cap free
Amount of items: 2
Items: 
Size: 526460 Color: 1
Size: 473523 Color: 3

Bin 1437: 18 of cap free
Amount of items: 2
Items: 
Size: 526640 Color: 3
Size: 473343 Color: 2

Bin 1438: 18 of cap free
Amount of items: 2
Items: 
Size: 532760 Color: 3
Size: 467223 Color: 4

Bin 1439: 18 of cap free
Amount of items: 2
Items: 
Size: 580835 Color: 1
Size: 419148 Color: 2

Bin 1440: 18 of cap free
Amount of items: 2
Items: 
Size: 594719 Color: 0
Size: 405264 Color: 2

Bin 1441: 18 of cap free
Amount of items: 2
Items: 
Size: 599770 Color: 4
Size: 400213 Color: 2

Bin 1442: 18 of cap free
Amount of items: 3
Items: 
Size: 612993 Color: 2
Size: 193964 Color: 3
Size: 193026 Color: 3

Bin 1443: 18 of cap free
Amount of items: 2
Items: 
Size: 620264 Color: 0
Size: 379719 Color: 3

Bin 1444: 18 of cap free
Amount of items: 3
Items: 
Size: 662265 Color: 3
Size: 169053 Color: 1
Size: 168665 Color: 0

Bin 1445: 18 of cap free
Amount of items: 2
Items: 
Size: 667596 Color: 0
Size: 332387 Color: 3

Bin 1446: 18 of cap free
Amount of items: 2
Items: 
Size: 689733 Color: 2
Size: 310250 Color: 4

Bin 1447: 18 of cap free
Amount of items: 2
Items: 
Size: 742160 Color: 3
Size: 257823 Color: 4

Bin 1448: 18 of cap free
Amount of items: 2
Items: 
Size: 763158 Color: 4
Size: 236825 Color: 0

Bin 1449: 18 of cap free
Amount of items: 2
Items: 
Size: 768278 Color: 4
Size: 231705 Color: 3

Bin 1450: 18 of cap free
Amount of items: 2
Items: 
Size: 780908 Color: 2
Size: 219075 Color: 4

Bin 1451: 18 of cap free
Amount of items: 3
Items: 
Size: 643073 Color: 1
Size: 178905 Color: 2
Size: 178005 Color: 4

Bin 1452: 18 of cap free
Amount of items: 3
Items: 
Size: 717201 Color: 2
Size: 141884 Color: 2
Size: 140898 Color: 0

Bin 1453: 18 of cap free
Amount of items: 2
Items: 
Size: 612054 Color: 2
Size: 387929 Color: 4

Bin 1454: 18 of cap free
Amount of items: 2
Items: 
Size: 714024 Color: 0
Size: 285959 Color: 3

Bin 1455: 18 of cap free
Amount of items: 2
Items: 
Size: 756666 Color: 3
Size: 243317 Color: 0

Bin 1456: 18 of cap free
Amount of items: 3
Items: 
Size: 517181 Color: 1
Size: 242106 Color: 2
Size: 240696 Color: 2

Bin 1457: 18 of cap free
Amount of items: 3
Items: 
Size: 657045 Color: 0
Size: 171490 Color: 0
Size: 171448 Color: 3

Bin 1458: 18 of cap free
Amount of items: 2
Items: 
Size: 568303 Color: 4
Size: 431680 Color: 3

Bin 1459: 18 of cap free
Amount of items: 3
Items: 
Size: 657396 Color: 4
Size: 171327 Color: 4
Size: 171260 Color: 3

Bin 1460: 18 of cap free
Amount of items: 2
Items: 
Size: 612610 Color: 4
Size: 387373 Color: 2

Bin 1461: 18 of cap free
Amount of items: 2
Items: 
Size: 646416 Color: 4
Size: 353567 Color: 1

Bin 1462: 18 of cap free
Amount of items: 2
Items: 
Size: 634645 Color: 1
Size: 365338 Color: 3

Bin 1463: 18 of cap free
Amount of items: 3
Items: 
Size: 356343 Color: 0
Size: 325109 Color: 1
Size: 318531 Color: 4

Bin 1464: 18 of cap free
Amount of items: 2
Items: 
Size: 511311 Color: 2
Size: 488672 Color: 4

Bin 1465: 19 of cap free
Amount of items: 2
Items: 
Size: 509507 Color: 2
Size: 490475 Color: 1

Bin 1466: 19 of cap free
Amount of items: 2
Items: 
Size: 514664 Color: 0
Size: 485318 Color: 2

Bin 1467: 19 of cap free
Amount of items: 2
Items: 
Size: 520077 Color: 4
Size: 479905 Color: 2

Bin 1468: 19 of cap free
Amount of items: 2
Items: 
Size: 568160 Color: 3
Size: 431822 Color: 2

Bin 1469: 19 of cap free
Amount of items: 2
Items: 
Size: 635091 Color: 0
Size: 364891 Color: 2

Bin 1470: 19 of cap free
Amount of items: 2
Items: 
Size: 635271 Color: 0
Size: 364711 Color: 2

Bin 1471: 19 of cap free
Amount of items: 2
Items: 
Size: 635594 Color: 4
Size: 364388 Color: 0

Bin 1472: 19 of cap free
Amount of items: 2
Items: 
Size: 650208 Color: 4
Size: 349774 Color: 0

Bin 1473: 19 of cap free
Amount of items: 2
Items: 
Size: 672099 Color: 1
Size: 327883 Color: 0

Bin 1474: 19 of cap free
Amount of items: 2
Items: 
Size: 760989 Color: 2
Size: 238993 Color: 4

Bin 1475: 19 of cap free
Amount of items: 2
Items: 
Size: 797630 Color: 0
Size: 202352 Color: 2

Bin 1476: 19 of cap free
Amount of items: 2
Items: 
Size: 699535 Color: 2
Size: 300447 Color: 3

Bin 1477: 19 of cap free
Amount of items: 2
Items: 
Size: 685636 Color: 3
Size: 314346 Color: 0

Bin 1478: 19 of cap free
Amount of items: 2
Items: 
Size: 605943 Color: 2
Size: 394039 Color: 0

Bin 1479: 19 of cap free
Amount of items: 2
Items: 
Size: 548609 Color: 3
Size: 451373 Color: 1

Bin 1480: 19 of cap free
Amount of items: 2
Items: 
Size: 682550 Color: 2
Size: 317432 Color: 4

Bin 1481: 19 of cap free
Amount of items: 2
Items: 
Size: 770827 Color: 0
Size: 229155 Color: 4

Bin 1482: 19 of cap free
Amount of items: 3
Items: 
Size: 374592 Color: 3
Size: 350310 Color: 4
Size: 275080 Color: 0

Bin 1483: 19 of cap free
Amount of items: 2
Items: 
Size: 591862 Color: 2
Size: 408120 Color: 4

Bin 1484: 19 of cap free
Amount of items: 2
Items: 
Size: 675031 Color: 4
Size: 324951 Color: 0

Bin 1485: 19 of cap free
Amount of items: 2
Items: 
Size: 664991 Color: 0
Size: 334991 Color: 3

Bin 1486: 19 of cap free
Amount of items: 2
Items: 
Size: 663494 Color: 3
Size: 336488 Color: 2

Bin 1487: 19 of cap free
Amount of items: 2
Items: 
Size: 618676 Color: 1
Size: 381306 Color: 3

Bin 1488: 19 of cap free
Amount of items: 2
Items: 
Size: 700363 Color: 1
Size: 299619 Color: 4

Bin 1489: 19 of cap free
Amount of items: 2
Items: 
Size: 574277 Color: 2
Size: 425705 Color: 0

Bin 1490: 19 of cap free
Amount of items: 3
Items: 
Size: 519533 Color: 2
Size: 240480 Color: 4
Size: 239969 Color: 4

Bin 1491: 19 of cap free
Amount of items: 2
Items: 
Size: 652725 Color: 0
Size: 347257 Color: 4

Bin 1492: 19 of cap free
Amount of items: 3
Items: 
Size: 505613 Color: 3
Size: 305969 Color: 0
Size: 188400 Color: 2

Bin 1493: 20 of cap free
Amount of items: 2
Items: 
Size: 755707 Color: 1
Size: 244274 Color: 2

Bin 1494: 20 of cap free
Amount of items: 3
Items: 
Size: 676441 Color: 4
Size: 161932 Color: 1
Size: 161608 Color: 4

Bin 1495: 20 of cap free
Amount of items: 3
Items: 
Size: 338374 Color: 0
Size: 335699 Color: 1
Size: 325908 Color: 2

Bin 1496: 20 of cap free
Amount of items: 2
Items: 
Size: 507385 Color: 2
Size: 492596 Color: 0

Bin 1497: 20 of cap free
Amount of items: 2
Items: 
Size: 501554 Color: 0
Size: 498427 Color: 2

Bin 1498: 20 of cap free
Amount of items: 2
Items: 
Size: 529765 Color: 4
Size: 470216 Color: 0

Bin 1499: 20 of cap free
Amount of items: 2
Items: 
Size: 541648 Color: 1
Size: 458333 Color: 0

Bin 1500: 20 of cap free
Amount of items: 2
Items: 
Size: 546516 Color: 0
Size: 453465 Color: 3

Bin 1501: 20 of cap free
Amount of items: 2
Items: 
Size: 559968 Color: 3
Size: 440013 Color: 2

Bin 1502: 20 of cap free
Amount of items: 2
Items: 
Size: 573132 Color: 2
Size: 426849 Color: 1

Bin 1503: 20 of cap free
Amount of items: 2
Items: 
Size: 575498 Color: 2
Size: 424483 Color: 1

Bin 1504: 20 of cap free
Amount of items: 2
Items: 
Size: 581299 Color: 4
Size: 418682 Color: 1

Bin 1505: 20 of cap free
Amount of items: 2
Items: 
Size: 587013 Color: 4
Size: 412968 Color: 0

Bin 1506: 20 of cap free
Amount of items: 2
Items: 
Size: 593050 Color: 3
Size: 406931 Color: 2

Bin 1507: 20 of cap free
Amount of items: 2
Items: 
Size: 596911 Color: 2
Size: 403070 Color: 1

Bin 1508: 20 of cap free
Amount of items: 3
Items: 
Size: 610781 Color: 0
Size: 195858 Color: 4
Size: 193342 Color: 4

Bin 1509: 20 of cap free
Amount of items: 2
Items: 
Size: 615937 Color: 3
Size: 384044 Color: 4

Bin 1510: 20 of cap free
Amount of items: 2
Items: 
Size: 617717 Color: 0
Size: 382264 Color: 1

Bin 1511: 20 of cap free
Amount of items: 3
Items: 
Size: 657455 Color: 0
Size: 171354 Color: 1
Size: 171172 Color: 1

Bin 1512: 20 of cap free
Amount of items: 2
Items: 
Size: 675306 Color: 1
Size: 324675 Color: 4

Bin 1513: 20 of cap free
Amount of items: 2
Items: 
Size: 707011 Color: 2
Size: 292970 Color: 4

Bin 1514: 20 of cap free
Amount of items: 2
Items: 
Size: 708181 Color: 1
Size: 291800 Color: 3

Bin 1515: 20 of cap free
Amount of items: 2
Items: 
Size: 758191 Color: 3
Size: 241790 Color: 4

Bin 1516: 20 of cap free
Amount of items: 2
Items: 
Size: 782570 Color: 2
Size: 217411 Color: 3

Bin 1517: 20 of cap free
Amount of items: 2
Items: 
Size: 788486 Color: 0
Size: 211495 Color: 1

Bin 1518: 20 of cap free
Amount of items: 3
Items: 
Size: 697837 Color: 0
Size: 151358 Color: 0
Size: 150786 Color: 3

Bin 1519: 20 of cap free
Amount of items: 2
Items: 
Size: 670188 Color: 0
Size: 329793 Color: 3

Bin 1520: 20 of cap free
Amount of items: 3
Items: 
Size: 639963 Color: 1
Size: 180040 Color: 3
Size: 179978 Color: 3

Bin 1521: 20 of cap free
Amount of items: 2
Items: 
Size: 658482 Color: 2
Size: 341499 Color: 0

Bin 1522: 20 of cap free
Amount of items: 2
Items: 
Size: 728529 Color: 0
Size: 271452 Color: 3

Bin 1523: 20 of cap free
Amount of items: 2
Items: 
Size: 613328 Color: 2
Size: 386653 Color: 3

Bin 1524: 20 of cap free
Amount of items: 3
Items: 
Size: 572914 Color: 2
Size: 213543 Color: 2
Size: 213524 Color: 0

Bin 1525: 20 of cap free
Amount of items: 3
Items: 
Size: 643243 Color: 3
Size: 202284 Color: 2
Size: 154454 Color: 3

Bin 1526: 20 of cap free
Amount of items: 2
Items: 
Size: 639163 Color: 1
Size: 360818 Color: 3

Bin 1527: 20 of cap free
Amount of items: 3
Items: 
Size: 651614 Color: 4
Size: 174686 Color: 0
Size: 173681 Color: 1

Bin 1528: 20 of cap free
Amount of items: 2
Items: 
Size: 525083 Color: 0
Size: 474898 Color: 3

Bin 1529: 20 of cap free
Amount of items: 2
Items: 
Size: 687891 Color: 1
Size: 312090 Color: 0

Bin 1530: 20 of cap free
Amount of items: 2
Items: 
Size: 605071 Color: 0
Size: 394910 Color: 4

Bin 1531: 20 of cap free
Amount of items: 2
Items: 
Size: 651172 Color: 1
Size: 348809 Color: 4

Bin 1532: 20 of cap free
Amount of items: 3
Items: 
Size: 503494 Color: 2
Size: 375523 Color: 0
Size: 120964 Color: 4

Bin 1533: 20 of cap free
Amount of items: 4
Items: 
Size: 258057 Color: 4
Size: 254458 Color: 2
Size: 251475 Color: 3
Size: 235991 Color: 0

Bin 1534: 21 of cap free
Amount of items: 3
Items: 
Size: 622784 Color: 2
Size: 189633 Color: 1
Size: 187563 Color: 1

Bin 1535: 21 of cap free
Amount of items: 2
Items: 
Size: 638640 Color: 2
Size: 361340 Color: 4

Bin 1536: 21 of cap free
Amount of items: 2
Items: 
Size: 507632 Color: 2
Size: 492348 Color: 0

Bin 1537: 21 of cap free
Amount of items: 2
Items: 
Size: 512987 Color: 3
Size: 486993 Color: 0

Bin 1538: 21 of cap free
Amount of items: 2
Items: 
Size: 518855 Color: 4
Size: 481125 Color: 3

Bin 1539: 21 of cap free
Amount of items: 2
Items: 
Size: 537837 Color: 4
Size: 462143 Color: 1

Bin 1540: 21 of cap free
Amount of items: 2
Items: 
Size: 563920 Color: 3
Size: 436060 Color: 4

Bin 1541: 21 of cap free
Amount of items: 2
Items: 
Size: 565615 Color: 0
Size: 434365 Color: 3

Bin 1542: 21 of cap free
Amount of items: 2
Items: 
Size: 568442 Color: 0
Size: 431538 Color: 3

Bin 1543: 21 of cap free
Amount of items: 2
Items: 
Size: 576219 Color: 0
Size: 423761 Color: 1

Bin 1544: 21 of cap free
Amount of items: 2
Items: 
Size: 580733 Color: 4
Size: 419247 Color: 0

Bin 1545: 21 of cap free
Amount of items: 2
Items: 
Size: 682778 Color: 1
Size: 317202 Color: 4

Bin 1546: 21 of cap free
Amount of items: 3
Items: 
Size: 685651 Color: 0
Size: 157755 Color: 2
Size: 156574 Color: 2

Bin 1547: 21 of cap free
Amount of items: 2
Items: 
Size: 693448 Color: 3
Size: 306532 Color: 4

Bin 1548: 21 of cap free
Amount of items: 2
Items: 
Size: 696823 Color: 2
Size: 303157 Color: 0

Bin 1549: 21 of cap free
Amount of items: 2
Items: 
Size: 708516 Color: 3
Size: 291464 Color: 1

Bin 1550: 21 of cap free
Amount of items: 3
Items: 
Size: 773789 Color: 2
Size: 114980 Color: 3
Size: 111211 Color: 4

Bin 1551: 21 of cap free
Amount of items: 2
Items: 
Size: 690930 Color: 1
Size: 309050 Color: 4

Bin 1552: 21 of cap free
Amount of items: 2
Items: 
Size: 719646 Color: 1
Size: 280334 Color: 0

Bin 1553: 21 of cap free
Amount of items: 2
Items: 
Size: 639346 Color: 0
Size: 360634 Color: 3

Bin 1554: 21 of cap free
Amount of items: 2
Items: 
Size: 581954 Color: 4
Size: 418026 Color: 3

Bin 1555: 21 of cap free
Amount of items: 3
Items: 
Size: 715628 Color: 2
Size: 146643 Color: 0
Size: 137709 Color: 3

Bin 1556: 21 of cap free
Amount of items: 2
Items: 
Size: 792077 Color: 4
Size: 207903 Color: 1

Bin 1557: 21 of cap free
Amount of items: 2
Items: 
Size: 752900 Color: 3
Size: 247080 Color: 1

Bin 1558: 21 of cap free
Amount of items: 3
Items: 
Size: 641182 Color: 2
Size: 179415 Color: 3
Size: 179383 Color: 3

Bin 1559: 21 of cap free
Amount of items: 3
Items: 
Size: 505114 Color: 1
Size: 254571 Color: 0
Size: 240295 Color: 4

Bin 1560: 21 of cap free
Amount of items: 2
Items: 
Size: 662944 Color: 2
Size: 337036 Color: 4

Bin 1561: 21 of cap free
Amount of items: 3
Items: 
Size: 682581 Color: 4
Size: 158927 Color: 3
Size: 158472 Color: 0

Bin 1562: 21 of cap free
Amount of items: 2
Items: 
Size: 517765 Color: 0
Size: 482215 Color: 4

Bin 1563: 21 of cap free
Amount of items: 2
Items: 
Size: 660658 Color: 0
Size: 339322 Color: 1

Bin 1564: 21 of cap free
Amount of items: 2
Items: 
Size: 637799 Color: 0
Size: 362181 Color: 3

Bin 1565: 22 of cap free
Amount of items: 3
Items: 
Size: 707869 Color: 0
Size: 146495 Color: 2
Size: 145615 Color: 2

Bin 1566: 22 of cap free
Amount of items: 3
Items: 
Size: 617122 Color: 0
Size: 192336 Color: 1
Size: 190521 Color: 1

Bin 1567: 22 of cap free
Amount of items: 2
Items: 
Size: 513750 Color: 3
Size: 486229 Color: 2

Bin 1568: 22 of cap free
Amount of items: 2
Items: 
Size: 532220 Color: 3
Size: 467759 Color: 4

Bin 1569: 22 of cap free
Amount of items: 2
Items: 
Size: 542116 Color: 0
Size: 457863 Color: 2

Bin 1570: 22 of cap free
Amount of items: 2
Items: 
Size: 543607 Color: 0
Size: 456372 Color: 3

Bin 1571: 22 of cap free
Amount of items: 2
Items: 
Size: 552863 Color: 3
Size: 447116 Color: 1

Bin 1572: 22 of cap free
Amount of items: 2
Items: 
Size: 559172 Color: 3
Size: 440807 Color: 2

Bin 1573: 22 of cap free
Amount of items: 2
Items: 
Size: 569059 Color: 3
Size: 430920 Color: 4

Bin 1574: 22 of cap free
Amount of items: 2
Items: 
Size: 571264 Color: 2
Size: 428715 Color: 3

Bin 1575: 22 of cap free
Amount of items: 2
Items: 
Size: 579801 Color: 4
Size: 420178 Color: 1

Bin 1576: 22 of cap free
Amount of items: 2
Items: 
Size: 586077 Color: 4
Size: 413902 Color: 1

Bin 1577: 22 of cap free
Amount of items: 2
Items: 
Size: 609993 Color: 1
Size: 389986 Color: 0

Bin 1578: 22 of cap free
Amount of items: 2
Items: 
Size: 670283 Color: 3
Size: 329696 Color: 1

Bin 1579: 22 of cap free
Amount of items: 3
Items: 
Size: 689972 Color: 2
Size: 155702 Color: 0
Size: 154305 Color: 3

Bin 1580: 22 of cap free
Amount of items: 2
Items: 
Size: 690457 Color: 3
Size: 309522 Color: 2

Bin 1581: 22 of cap free
Amount of items: 3
Items: 
Size: 733777 Color: 0
Size: 135191 Color: 1
Size: 131011 Color: 4

Bin 1582: 22 of cap free
Amount of items: 2
Items: 
Size: 738587 Color: 4
Size: 261392 Color: 0

Bin 1583: 22 of cap free
Amount of items: 3
Items: 
Size: 750537 Color: 4
Size: 125867 Color: 3
Size: 123575 Color: 2

Bin 1584: 22 of cap free
Amount of items: 2
Items: 
Size: 759923 Color: 2
Size: 240056 Color: 1

Bin 1585: 22 of cap free
Amount of items: 2
Items: 
Size: 766746 Color: 1
Size: 233233 Color: 0

Bin 1586: 22 of cap free
Amount of items: 2
Items: 
Size: 796953 Color: 2
Size: 203026 Color: 0

Bin 1587: 22 of cap free
Amount of items: 3
Items: 
Size: 693395 Color: 3
Size: 174399 Color: 3
Size: 132185 Color: 4

Bin 1588: 22 of cap free
Amount of items: 2
Items: 
Size: 753653 Color: 2
Size: 246326 Color: 3

Bin 1589: 22 of cap free
Amount of items: 3
Items: 
Size: 674076 Color: 0
Size: 162956 Color: 2
Size: 162947 Color: 3

Bin 1590: 22 of cap free
Amount of items: 2
Items: 
Size: 729243 Color: 0
Size: 270736 Color: 1

Bin 1591: 22 of cap free
Amount of items: 3
Items: 
Size: 630676 Color: 4
Size: 185429 Color: 1
Size: 183874 Color: 3

Bin 1592: 22 of cap free
Amount of items: 3
Items: 
Size: 731524 Color: 2
Size: 137303 Color: 1
Size: 131152 Color: 0

Bin 1593: 22 of cap free
Amount of items: 2
Items: 
Size: 758130 Color: 3
Size: 241849 Color: 1

Bin 1594: 22 of cap free
Amount of items: 2
Items: 
Size: 779080 Color: 0
Size: 220899 Color: 2

Bin 1595: 22 of cap free
Amount of items: 2
Items: 
Size: 558556 Color: 4
Size: 441423 Color: 1

Bin 1596: 22 of cap free
Amount of items: 3
Items: 
Size: 653836 Color: 4
Size: 207095 Color: 3
Size: 139048 Color: 4

Bin 1597: 22 of cap free
Amount of items: 2
Items: 
Size: 536632 Color: 2
Size: 463347 Color: 1

Bin 1598: 22 of cap free
Amount of items: 2
Items: 
Size: 641868 Color: 1
Size: 358111 Color: 3

Bin 1599: 22 of cap free
Amount of items: 2
Items: 
Size: 623587 Color: 1
Size: 376392 Color: 0

Bin 1600: 22 of cap free
Amount of items: 2
Items: 
Size: 695000 Color: 4
Size: 304979 Color: 2

Bin 1601: 22 of cap free
Amount of items: 2
Items: 
Size: 770912 Color: 0
Size: 229067 Color: 1

Bin 1602: 22 of cap free
Amount of items: 2
Items: 
Size: 705582 Color: 4
Size: 294397 Color: 2

Bin 1603: 22 of cap free
Amount of items: 3
Items: 
Size: 409723 Color: 0
Size: 318273 Color: 2
Size: 271983 Color: 3

Bin 1604: 23 of cap free
Amount of items: 3
Items: 
Size: 769033 Color: 4
Size: 117371 Color: 1
Size: 113574 Color: 1

Bin 1605: 23 of cap free
Amount of items: 3
Items: 
Size: 636565 Color: 1
Size: 181929 Color: 3
Size: 181484 Color: 1

Bin 1606: 23 of cap free
Amount of items: 2
Items: 
Size: 535783 Color: 1
Size: 464195 Color: 4

Bin 1607: 23 of cap free
Amount of items: 2
Items: 
Size: 558333 Color: 3
Size: 441645 Color: 1

Bin 1608: 23 of cap free
Amount of items: 2
Items: 
Size: 565398 Color: 1
Size: 434580 Color: 2

Bin 1609: 23 of cap free
Amount of items: 2
Items: 
Size: 586779 Color: 0
Size: 413199 Color: 4

Bin 1610: 23 of cap free
Amount of items: 2
Items: 
Size: 587985 Color: 3
Size: 411993 Color: 1

Bin 1611: 23 of cap free
Amount of items: 2
Items: 
Size: 600389 Color: 4
Size: 399589 Color: 3

Bin 1612: 23 of cap free
Amount of items: 2
Items: 
Size: 607131 Color: 1
Size: 392847 Color: 0

Bin 1613: 23 of cap free
Amount of items: 2
Items: 
Size: 627747 Color: 3
Size: 372231 Color: 0

Bin 1614: 23 of cap free
Amount of items: 2
Items: 
Size: 639684 Color: 1
Size: 360294 Color: 4

Bin 1615: 23 of cap free
Amount of items: 3
Items: 
Size: 685083 Color: 0
Size: 157993 Color: 3
Size: 156902 Color: 1

Bin 1616: 23 of cap free
Amount of items: 2
Items: 
Size: 685047 Color: 2
Size: 314931 Color: 1

Bin 1617: 23 of cap free
Amount of items: 2
Items: 
Size: 686149 Color: 4
Size: 313829 Color: 2

Bin 1618: 23 of cap free
Amount of items: 2
Items: 
Size: 723807 Color: 0
Size: 276171 Color: 4

Bin 1619: 23 of cap free
Amount of items: 2
Items: 
Size: 728451 Color: 0
Size: 271527 Color: 3

Bin 1620: 23 of cap free
Amount of items: 2
Items: 
Size: 755763 Color: 4
Size: 244215 Color: 0

Bin 1621: 23 of cap free
Amount of items: 2
Items: 
Size: 765138 Color: 3
Size: 234840 Color: 0

Bin 1622: 23 of cap free
Amount of items: 2
Items: 
Size: 769155 Color: 4
Size: 230823 Color: 2

Bin 1623: 23 of cap free
Amount of items: 2
Items: 
Size: 675705 Color: 0
Size: 324273 Color: 4

Bin 1624: 23 of cap free
Amount of items: 3
Items: 
Size: 748100 Color: 1
Size: 131801 Color: 4
Size: 120077 Color: 1

Bin 1625: 23 of cap free
Amount of items: 2
Items: 
Size: 779821 Color: 0
Size: 220157 Color: 1

Bin 1626: 23 of cap free
Amount of items: 2
Items: 
Size: 701096 Color: 3
Size: 298882 Color: 0

Bin 1627: 23 of cap free
Amount of items: 2
Items: 
Size: 545306 Color: 3
Size: 454672 Color: 1

Bin 1628: 23 of cap free
Amount of items: 2
Items: 
Size: 608822 Color: 1
Size: 391156 Color: 0

Bin 1629: 23 of cap free
Amount of items: 3
Items: 
Size: 689909 Color: 4
Size: 201051 Color: 2
Size: 109018 Color: 4

Bin 1630: 23 of cap free
Amount of items: 2
Items: 
Size: 774247 Color: 1
Size: 225731 Color: 0

Bin 1631: 23 of cap free
Amount of items: 2
Items: 
Size: 554384 Color: 2
Size: 445594 Color: 1

Bin 1632: 24 of cap free
Amount of items: 3
Items: 
Size: 614331 Color: 2
Size: 193280 Color: 1
Size: 192366 Color: 2

Bin 1633: 24 of cap free
Amount of items: 2
Items: 
Size: 500333 Color: 3
Size: 499644 Color: 4

Bin 1634: 24 of cap free
Amount of items: 2
Items: 
Size: 515713 Color: 3
Size: 484264 Color: 0

Bin 1635: 24 of cap free
Amount of items: 2
Items: 
Size: 530678 Color: 2
Size: 469299 Color: 1

Bin 1636: 24 of cap free
Amount of items: 2
Items: 
Size: 557865 Color: 2
Size: 442112 Color: 3

Bin 1637: 24 of cap free
Amount of items: 2
Items: 
Size: 579157 Color: 3
Size: 420820 Color: 1

Bin 1638: 24 of cap free
Amount of items: 2
Items: 
Size: 590322 Color: 2
Size: 409655 Color: 0

Bin 1639: 24 of cap free
Amount of items: 2
Items: 
Size: 593153 Color: 2
Size: 406824 Color: 3

Bin 1640: 24 of cap free
Amount of items: 2
Items: 
Size: 609548 Color: 1
Size: 390429 Color: 2

Bin 1641: 24 of cap free
Amount of items: 3
Items: 
Size: 611993 Color: 2
Size: 194875 Color: 1
Size: 193109 Color: 0

Bin 1642: 24 of cap free
Amount of items: 2
Items: 
Size: 638706 Color: 1
Size: 361271 Color: 0

Bin 1643: 24 of cap free
Amount of items: 2
Items: 
Size: 663725 Color: 2
Size: 336252 Color: 0

Bin 1644: 24 of cap free
Amount of items: 2
Items: 
Size: 670041 Color: 2
Size: 329936 Color: 4

Bin 1645: 24 of cap free
Amount of items: 2
Items: 
Size: 680455 Color: 2
Size: 319522 Color: 1

Bin 1646: 24 of cap free
Amount of items: 2
Items: 
Size: 680789 Color: 1
Size: 319188 Color: 2

Bin 1647: 24 of cap free
Amount of items: 3
Items: 
Size: 700446 Color: 0
Size: 150134 Color: 4
Size: 149397 Color: 2

Bin 1648: 24 of cap free
Amount of items: 2
Items: 
Size: 698554 Color: 2
Size: 301423 Color: 3

Bin 1649: 24 of cap free
Amount of items: 2
Items: 
Size: 777858 Color: 1
Size: 222119 Color: 4

Bin 1650: 24 of cap free
Amount of items: 2
Items: 
Size: 562785 Color: 1
Size: 437192 Color: 4

Bin 1651: 24 of cap free
Amount of items: 2
Items: 
Size: 534574 Color: 3
Size: 465403 Color: 2

Bin 1652: 24 of cap free
Amount of items: 2
Items: 
Size: 673949 Color: 0
Size: 326028 Color: 4

Bin 1653: 24 of cap free
Amount of items: 2
Items: 
Size: 641295 Color: 2
Size: 358682 Color: 4

Bin 1654: 24 of cap free
Amount of items: 3
Items: 
Size: 377477 Color: 3
Size: 339366 Color: 0
Size: 283134 Color: 1

Bin 1655: 24 of cap free
Amount of items: 3
Items: 
Size: 656606 Color: 0
Size: 173003 Color: 4
Size: 170368 Color: 0

Bin 1656: 24 of cap free
Amount of items: 2
Items: 
Size: 709078 Color: 3
Size: 290899 Color: 2

Bin 1657: 24 of cap free
Amount of items: 2
Items: 
Size: 572439 Color: 4
Size: 427538 Color: 0

Bin 1658: 24 of cap free
Amount of items: 3
Items: 
Size: 630707 Color: 4
Size: 185339 Color: 0
Size: 183931 Color: 1

Bin 1659: 24 of cap free
Amount of items: 2
Items: 
Size: 540863 Color: 1
Size: 459114 Color: 0

Bin 1660: 24 of cap free
Amount of items: 2
Items: 
Size: 695762 Color: 0
Size: 304215 Color: 1

Bin 1661: 24 of cap free
Amount of items: 2
Items: 
Size: 620373 Color: 0
Size: 379604 Color: 1

Bin 1662: 24 of cap free
Amount of items: 2
Items: 
Size: 585012 Color: 1
Size: 414965 Color: 3

Bin 1663: 24 of cap free
Amount of items: 2
Items: 
Size: 581447 Color: 3
Size: 418530 Color: 1

Bin 1664: 24 of cap free
Amount of items: 2
Items: 
Size: 702531 Color: 1
Size: 297446 Color: 4

Bin 1665: 24 of cap free
Amount of items: 2
Items: 
Size: 742632 Color: 3
Size: 257345 Color: 2

Bin 1666: 25 of cap free
Amount of items: 3
Items: 
Size: 706403 Color: 1
Size: 153858 Color: 0
Size: 139715 Color: 0

Bin 1667: 25 of cap free
Amount of items: 3
Items: 
Size: 636216 Color: 2
Size: 181898 Color: 1
Size: 181862 Color: 1

Bin 1668: 25 of cap free
Amount of items: 3
Items: 
Size: 704663 Color: 2
Size: 151015 Color: 1
Size: 144298 Color: 4

Bin 1669: 25 of cap free
Amount of items: 2
Items: 
Size: 528426 Color: 2
Size: 471550 Color: 0

Bin 1670: 25 of cap free
Amount of items: 2
Items: 
Size: 553629 Color: 0
Size: 446347 Color: 2

Bin 1671: 25 of cap free
Amount of items: 3
Items: 
Size: 581476 Color: 0
Size: 211516 Color: 3
Size: 206984 Color: 3

Bin 1672: 25 of cap free
Amount of items: 2
Items: 
Size: 595344 Color: 4
Size: 404632 Color: 3

Bin 1673: 25 of cap free
Amount of items: 2
Items: 
Size: 640277 Color: 1
Size: 359699 Color: 4

Bin 1674: 25 of cap free
Amount of items: 2
Items: 
Size: 641590 Color: 2
Size: 358386 Color: 1

Bin 1675: 25 of cap free
Amount of items: 2
Items: 
Size: 656419 Color: 4
Size: 343557 Color: 2

Bin 1676: 25 of cap free
Amount of items: 2
Items: 
Size: 683127 Color: 2
Size: 316849 Color: 4

Bin 1677: 25 of cap free
Amount of items: 2
Items: 
Size: 706973 Color: 3
Size: 293003 Color: 2

Bin 1678: 25 of cap free
Amount of items: 2
Items: 
Size: 710071 Color: 0
Size: 289905 Color: 1

Bin 1679: 25 of cap free
Amount of items: 2
Items: 
Size: 734969 Color: 1
Size: 265007 Color: 4

Bin 1680: 25 of cap free
Amount of items: 2
Items: 
Size: 747170 Color: 0
Size: 252806 Color: 2

Bin 1681: 25 of cap free
Amount of items: 2
Items: 
Size: 764648 Color: 2
Size: 235328 Color: 4

Bin 1682: 25 of cap free
Amount of items: 2
Items: 
Size: 770616 Color: 0
Size: 229360 Color: 2

Bin 1683: 25 of cap free
Amount of items: 2
Items: 
Size: 695324 Color: 2
Size: 304652 Color: 0

Bin 1684: 25 of cap free
Amount of items: 2
Items: 
Size: 535052 Color: 3
Size: 464924 Color: 0

Bin 1685: 25 of cap free
Amount of items: 3
Items: 
Size: 639285 Color: 0
Size: 180579 Color: 3
Size: 180112 Color: 4

Bin 1686: 25 of cap free
Amount of items: 2
Items: 
Size: 528711 Color: 2
Size: 471265 Color: 4

Bin 1687: 25 of cap free
Amount of items: 3
Items: 
Size: 708975 Color: 3
Size: 146769 Color: 0
Size: 144232 Color: 4

Bin 1688: 25 of cap free
Amount of items: 2
Items: 
Size: 692010 Color: 0
Size: 307966 Color: 4

Bin 1689: 25 of cap free
Amount of items: 2
Items: 
Size: 763196 Color: 0
Size: 236780 Color: 1

Bin 1690: 25 of cap free
Amount of items: 2
Items: 
Size: 539962 Color: 1
Size: 460014 Color: 4

Bin 1691: 25 of cap free
Amount of items: 2
Items: 
Size: 740685 Color: 3
Size: 259291 Color: 0

Bin 1692: 25 of cap free
Amount of items: 2
Items: 
Size: 669499 Color: 2
Size: 330477 Color: 1

Bin 1693: 25 of cap free
Amount of items: 2
Items: 
Size: 573868 Color: 4
Size: 426108 Color: 3

Bin 1694: 26 of cap free
Amount of items: 2
Items: 
Size: 535370 Color: 0
Size: 464605 Color: 4

Bin 1695: 26 of cap free
Amount of items: 3
Items: 
Size: 756538 Color: 3
Size: 124348 Color: 3
Size: 119089 Color: 2

Bin 1696: 26 of cap free
Amount of items: 2
Items: 
Size: 536292 Color: 0
Size: 463683 Color: 2

Bin 1697: 26 of cap free
Amount of items: 2
Items: 
Size: 541789 Color: 1
Size: 458186 Color: 0

Bin 1698: 26 of cap free
Amount of items: 2
Items: 
Size: 545182 Color: 3
Size: 454793 Color: 0

Bin 1699: 26 of cap free
Amount of items: 2
Items: 
Size: 545342 Color: 3
Size: 454633 Color: 0

Bin 1700: 26 of cap free
Amount of items: 2
Items: 
Size: 553451 Color: 3
Size: 446524 Color: 2

Bin 1701: 26 of cap free
Amount of items: 2
Items: 
Size: 558873 Color: 3
Size: 441102 Color: 0

Bin 1702: 26 of cap free
Amount of items: 2
Items: 
Size: 595660 Color: 3
Size: 404315 Color: 4

Bin 1703: 26 of cap free
Amount of items: 2
Items: 
Size: 598137 Color: 1
Size: 401838 Color: 3

Bin 1704: 26 of cap free
Amount of items: 3
Items: 
Size: 614768 Color: 0
Size: 193079 Color: 2
Size: 192128 Color: 1

Bin 1705: 26 of cap free
Amount of items: 2
Items: 
Size: 621829 Color: 4
Size: 378146 Color: 0

Bin 1706: 26 of cap free
Amount of items: 2
Items: 
Size: 650625 Color: 3
Size: 349350 Color: 2

Bin 1707: 26 of cap free
Amount of items: 3
Items: 
Size: 706106 Color: 2
Size: 146937 Color: 0
Size: 146932 Color: 1

Bin 1708: 26 of cap free
Amount of items: 2
Items: 
Size: 715058 Color: 4
Size: 284917 Color: 1

Bin 1709: 26 of cap free
Amount of items: 2
Items: 
Size: 769687 Color: 4
Size: 230288 Color: 0

Bin 1710: 26 of cap free
Amount of items: 2
Items: 
Size: 784293 Color: 1
Size: 215682 Color: 0

Bin 1711: 26 of cap free
Amount of items: 3
Items: 
Size: 639739 Color: 0
Size: 180394 Color: 3
Size: 179842 Color: 1

Bin 1712: 26 of cap free
Amount of items: 2
Items: 
Size: 606976 Color: 0
Size: 392999 Color: 1

Bin 1713: 26 of cap free
Amount of items: 3
Items: 
Size: 633959 Color: 3
Size: 183275 Color: 4
Size: 182741 Color: 2

Bin 1714: 26 of cap free
Amount of items: 2
Items: 
Size: 543411 Color: 1
Size: 456564 Color: 0

Bin 1715: 26 of cap free
Amount of items: 3
Items: 
Size: 710427 Color: 4
Size: 145044 Color: 0
Size: 144504 Color: 4

Bin 1716: 26 of cap free
Amount of items: 3
Items: 
Size: 638809 Color: 2
Size: 181667 Color: 2
Size: 179499 Color: 1

Bin 1717: 26 of cap free
Amount of items: 2
Items: 
Size: 512238 Color: 1
Size: 487737 Color: 4

Bin 1718: 26 of cap free
Amount of items: 3
Items: 
Size: 353213 Color: 0
Size: 334172 Color: 0
Size: 312590 Color: 1

Bin 1719: 26 of cap free
Amount of items: 2
Items: 
Size: 772930 Color: 2
Size: 227045 Color: 3

Bin 1720: 26 of cap free
Amount of items: 2
Items: 
Size: 577633 Color: 4
Size: 422342 Color: 0

Bin 1721: 26 of cap free
Amount of items: 2
Items: 
Size: 595833 Color: 4
Size: 404142 Color: 2

Bin 1722: 26 of cap free
Amount of items: 2
Items: 
Size: 650843 Color: 4
Size: 349132 Color: 1

Bin 1723: 27 of cap free
Amount of items: 2
Items: 
Size: 745544 Color: 1
Size: 254430 Color: 4

Bin 1724: 27 of cap free
Amount of items: 3
Items: 
Size: 779674 Color: 2
Size: 110781 Color: 3
Size: 109519 Color: 2

Bin 1725: 27 of cap free
Amount of items: 2
Items: 
Size: 649507 Color: 0
Size: 350467 Color: 1

Bin 1726: 27 of cap free
Amount of items: 2
Items: 
Size: 513393 Color: 3
Size: 486581 Color: 4

Bin 1727: 27 of cap free
Amount of items: 2
Items: 
Size: 520723 Color: 2
Size: 479251 Color: 4

Bin 1728: 27 of cap free
Amount of items: 2
Items: 
Size: 527361 Color: 0
Size: 472613 Color: 1

Bin 1729: 27 of cap free
Amount of items: 2
Items: 
Size: 566791 Color: 3
Size: 433183 Color: 0

Bin 1730: 27 of cap free
Amount of items: 2
Items: 
Size: 578581 Color: 1
Size: 421393 Color: 3

Bin 1731: 27 of cap free
Amount of items: 3
Items: 
Size: 622637 Color: 2
Size: 189017 Color: 1
Size: 188320 Color: 1

Bin 1732: 27 of cap free
Amount of items: 2
Items: 
Size: 681542 Color: 2
Size: 318432 Color: 3

Bin 1733: 27 of cap free
Amount of items: 3
Items: 
Size: 690595 Color: 0
Size: 154714 Color: 3
Size: 154665 Color: 4

Bin 1734: 27 of cap free
Amount of items: 3
Items: 
Size: 700675 Color: 0
Size: 149656 Color: 1
Size: 149643 Color: 2

Bin 1735: 27 of cap free
Amount of items: 2
Items: 
Size: 713031 Color: 2
Size: 286943 Color: 1

Bin 1736: 27 of cap free
Amount of items: 2
Items: 
Size: 716186 Color: 4
Size: 283788 Color: 3

Bin 1737: 27 of cap free
Amount of items: 2
Items: 
Size: 717832 Color: 4
Size: 282142 Color: 3

Bin 1738: 27 of cap free
Amount of items: 2
Items: 
Size: 727548 Color: 3
Size: 272426 Color: 2

Bin 1739: 27 of cap free
Amount of items: 2
Items: 
Size: 727864 Color: 0
Size: 272110 Color: 2

Bin 1740: 27 of cap free
Amount of items: 2
Items: 
Size: 731027 Color: 4
Size: 268947 Color: 2

Bin 1741: 27 of cap free
Amount of items: 2
Items: 
Size: 784590 Color: 4
Size: 215384 Color: 3

Bin 1742: 27 of cap free
Amount of items: 2
Items: 
Size: 793267 Color: 3
Size: 206707 Color: 4

Bin 1743: 27 of cap free
Amount of items: 2
Items: 
Size: 671133 Color: 1
Size: 328841 Color: 0

Bin 1744: 27 of cap free
Amount of items: 2
Items: 
Size: 741880 Color: 4
Size: 258094 Color: 1

Bin 1745: 27 of cap free
Amount of items: 2
Items: 
Size: 575535 Color: 4
Size: 424439 Color: 0

Bin 1746: 27 of cap free
Amount of items: 2
Items: 
Size: 718095 Color: 1
Size: 281879 Color: 0

Bin 1747: 27 of cap free
Amount of items: 2
Items: 
Size: 600349 Color: 1
Size: 399625 Color: 0

Bin 1748: 27 of cap free
Amount of items: 3
Items: 
Size: 758215 Color: 4
Size: 122147 Color: 1
Size: 119612 Color: 4

Bin 1749: 27 of cap free
Amount of items: 2
Items: 
Size: 644080 Color: 3
Size: 355894 Color: 2

Bin 1750: 27 of cap free
Amount of items: 2
Items: 
Size: 627408 Color: 1
Size: 372566 Color: 0

Bin 1751: 27 of cap free
Amount of items: 2
Items: 
Size: 611041 Color: 2
Size: 388933 Color: 0

Bin 1752: 27 of cap free
Amount of items: 2
Items: 
Size: 567206 Color: 4
Size: 432768 Color: 3

Bin 1753: 28 of cap free
Amount of items: 2
Items: 
Size: 504057 Color: 4
Size: 495916 Color: 3

Bin 1754: 28 of cap free
Amount of items: 3
Items: 
Size: 378908 Color: 2
Size: 338105 Color: 0
Size: 282960 Color: 0

Bin 1755: 28 of cap free
Amount of items: 2
Items: 
Size: 502572 Color: 3
Size: 497401 Color: 4

Bin 1756: 28 of cap free
Amount of items: 2
Items: 
Size: 512169 Color: 3
Size: 487804 Color: 2

Bin 1757: 28 of cap free
Amount of items: 2
Items: 
Size: 518502 Color: 1
Size: 481471 Color: 0

Bin 1758: 28 of cap free
Amount of items: 2
Items: 
Size: 520326 Color: 4
Size: 479647 Color: 2

Bin 1759: 28 of cap free
Amount of items: 2
Items: 
Size: 546480 Color: 2
Size: 453493 Color: 3

Bin 1760: 28 of cap free
Amount of items: 2
Items: 
Size: 611707 Color: 4
Size: 388266 Color: 2

Bin 1761: 28 of cap free
Amount of items: 2
Items: 
Size: 630497 Color: 1
Size: 369476 Color: 0

Bin 1762: 28 of cap free
Amount of items: 2
Items: 
Size: 677998 Color: 2
Size: 321975 Color: 4

Bin 1763: 28 of cap free
Amount of items: 2
Items: 
Size: 716490 Color: 1
Size: 283483 Color: 2

Bin 1764: 28 of cap free
Amount of items: 2
Items: 
Size: 784694 Color: 3
Size: 215279 Color: 0

Bin 1765: 28 of cap free
Amount of items: 2
Items: 
Size: 788627 Color: 1
Size: 211346 Color: 2

Bin 1766: 28 of cap free
Amount of items: 3
Items: 
Size: 737750 Color: 1
Size: 132557 Color: 0
Size: 129666 Color: 2

Bin 1767: 28 of cap free
Amount of items: 2
Items: 
Size: 515778 Color: 4
Size: 484195 Color: 1

Bin 1768: 28 of cap free
Amount of items: 2
Items: 
Size: 525077 Color: 1
Size: 474896 Color: 3

Bin 1769: 28 of cap free
Amount of items: 2
Items: 
Size: 753907 Color: 0
Size: 246066 Color: 1

Bin 1770: 28 of cap free
Amount of items: 2
Items: 
Size: 769953 Color: 2
Size: 230020 Color: 3

Bin 1771: 28 of cap free
Amount of items: 3
Items: 
Size: 630037 Color: 0
Size: 185706 Color: 2
Size: 184230 Color: 2

Bin 1772: 28 of cap free
Amount of items: 2
Items: 
Size: 557729 Color: 4
Size: 442244 Color: 3

Bin 1773: 28 of cap free
Amount of items: 2
Items: 
Size: 563199 Color: 1
Size: 436774 Color: 4

Bin 1774: 28 of cap free
Amount of items: 2
Items: 
Size: 612764 Color: 3
Size: 387209 Color: 2

Bin 1775: 28 of cap free
Amount of items: 2
Items: 
Size: 674256 Color: 1
Size: 325717 Color: 4

Bin 1776: 28 of cap free
Amount of items: 3
Items: 
Size: 376456 Color: 1
Size: 366874 Color: 1
Size: 256643 Color: 2

Bin 1777: 28 of cap free
Amount of items: 2
Items: 
Size: 530826 Color: 1
Size: 469147 Color: 2

Bin 1778: 28 of cap free
Amount of items: 2
Items: 
Size: 570980 Color: 1
Size: 428993 Color: 4

Bin 1779: 28 of cap free
Amount of items: 2
Items: 
Size: 792133 Color: 1
Size: 207840 Color: 4

Bin 1780: 28 of cap free
Amount of items: 2
Items: 
Size: 582524 Color: 4
Size: 417449 Color: 0

Bin 1781: 28 of cap free
Amount of items: 3
Items: 
Size: 367738 Color: 3
Size: 317672 Color: 2
Size: 314563 Color: 2

Bin 1782: 28 of cap free
Amount of items: 2
Items: 
Size: 709435 Color: 0
Size: 290538 Color: 3

Bin 1783: 28 of cap free
Amount of items: 3
Items: 
Size: 346310 Color: 2
Size: 344316 Color: 2
Size: 309347 Color: 3

Bin 1784: 29 of cap free
Amount of items: 3
Items: 
Size: 677961 Color: 0
Size: 161013 Color: 4
Size: 160998 Color: 1

Bin 1785: 29 of cap free
Amount of items: 2
Items: 
Size: 765877 Color: 3
Size: 234095 Color: 4

Bin 1786: 29 of cap free
Amount of items: 2
Items: 
Size: 643066 Color: 0
Size: 356906 Color: 3

Bin 1787: 29 of cap free
Amount of items: 3
Items: 
Size: 602068 Color: 4
Size: 199472 Color: 0
Size: 198432 Color: 4

Bin 1788: 29 of cap free
Amount of items: 3
Items: 
Size: 383974 Color: 4
Size: 350197 Color: 0
Size: 265801 Color: 0

Bin 1789: 29 of cap free
Amount of items: 2
Items: 
Size: 516877 Color: 0
Size: 483095 Color: 2

Bin 1790: 29 of cap free
Amount of items: 2
Items: 
Size: 524923 Color: 2
Size: 475049 Color: 1

Bin 1791: 29 of cap free
Amount of items: 2
Items: 
Size: 587618 Color: 1
Size: 412354 Color: 4

Bin 1792: 29 of cap free
Amount of items: 2
Items: 
Size: 593633 Color: 4
Size: 406339 Color: 3

Bin 1793: 29 of cap free
Amount of items: 2
Items: 
Size: 633784 Color: 2
Size: 366188 Color: 1

Bin 1794: 29 of cap free
Amount of items: 2
Items: 
Size: 680071 Color: 2
Size: 319901 Color: 4

Bin 1795: 29 of cap free
Amount of items: 2
Items: 
Size: 683704 Color: 0
Size: 316268 Color: 3

Bin 1796: 29 of cap free
Amount of items: 2
Items: 
Size: 723168 Color: 3
Size: 276804 Color: 1

Bin 1797: 29 of cap free
Amount of items: 2
Items: 
Size: 764287 Color: 4
Size: 235685 Color: 0

Bin 1798: 29 of cap free
Amount of items: 2
Items: 
Size: 766476 Color: 3
Size: 233496 Color: 4

Bin 1799: 29 of cap free
Amount of items: 2
Items: 
Size: 768563 Color: 0
Size: 231409 Color: 4

Bin 1800: 29 of cap free
Amount of items: 3
Items: 
Size: 618236 Color: 3
Size: 191179 Color: 2
Size: 190557 Color: 3

Bin 1801: 29 of cap free
Amount of items: 2
Items: 
Size: 704233 Color: 0
Size: 295739 Color: 4

Bin 1802: 29 of cap free
Amount of items: 2
Items: 
Size: 598700 Color: 2
Size: 401272 Color: 4

Bin 1803: 29 of cap free
Amount of items: 2
Items: 
Size: 568324 Color: 3
Size: 431648 Color: 0

Bin 1804: 29 of cap free
Amount of items: 2
Items: 
Size: 653386 Color: 2
Size: 346586 Color: 0

Bin 1805: 29 of cap free
Amount of items: 3
Items: 
Size: 687994 Color: 0
Size: 156214 Color: 0
Size: 155764 Color: 4

Bin 1806: 29 of cap free
Amount of items: 2
Items: 
Size: 638446 Color: 4
Size: 361526 Color: 1

Bin 1807: 29 of cap free
Amount of items: 3
Items: 
Size: 372768 Color: 1
Size: 317481 Color: 2
Size: 309723 Color: 4

Bin 1808: 29 of cap free
Amount of items: 2
Items: 
Size: 752112 Color: 1
Size: 247860 Color: 3

Bin 1809: 29 of cap free
Amount of items: 2
Items: 
Size: 700194 Color: 0
Size: 299778 Color: 2

Bin 1810: 29 of cap free
Amount of items: 2
Items: 
Size: 598586 Color: 1
Size: 401386 Color: 3

Bin 1811: 29 of cap free
Amount of items: 2
Items: 
Size: 615073 Color: 1
Size: 384899 Color: 2

Bin 1812: 29 of cap free
Amount of items: 3
Items: 
Size: 339201 Color: 4
Size: 339172 Color: 1
Size: 321599 Color: 0

Bin 1813: 30 of cap free
Amount of items: 2
Items: 
Size: 584010 Color: 4
Size: 415961 Color: 1

Bin 1814: 30 of cap free
Amount of items: 2
Items: 
Size: 543844 Color: 3
Size: 456127 Color: 4

Bin 1815: 30 of cap free
Amount of items: 2
Items: 
Size: 546208 Color: 4
Size: 453763 Color: 0

Bin 1816: 30 of cap free
Amount of items: 2
Items: 
Size: 576094 Color: 3
Size: 423877 Color: 1

Bin 1817: 30 of cap free
Amount of items: 2
Items: 
Size: 587865 Color: 2
Size: 412106 Color: 4

Bin 1818: 30 of cap free
Amount of items: 2
Items: 
Size: 602001 Color: 3
Size: 397970 Color: 2

Bin 1819: 30 of cap free
Amount of items: 2
Items: 
Size: 679787 Color: 4
Size: 320184 Color: 3

Bin 1820: 30 of cap free
Amount of items: 2
Items: 
Size: 681489 Color: 3
Size: 318482 Color: 4

Bin 1821: 30 of cap free
Amount of items: 2
Items: 
Size: 699517 Color: 3
Size: 300454 Color: 2

Bin 1822: 30 of cap free
Amount of items: 2
Items: 
Size: 761682 Color: 0
Size: 238289 Color: 2

Bin 1823: 30 of cap free
Amount of items: 2
Items: 
Size: 778186 Color: 1
Size: 221785 Color: 2

Bin 1824: 30 of cap free
Amount of items: 2
Items: 
Size: 574643 Color: 2
Size: 425328 Color: 0

Bin 1825: 30 of cap free
Amount of items: 3
Items: 
Size: 726405 Color: 0
Size: 137386 Color: 0
Size: 136180 Color: 1

Bin 1826: 30 of cap free
Amount of items: 3
Items: 
Size: 609240 Color: 2
Size: 195386 Color: 4
Size: 195345 Color: 3

Bin 1827: 30 of cap free
Amount of items: 2
Items: 
Size: 712077 Color: 2
Size: 287894 Color: 0

Bin 1828: 30 of cap free
Amount of items: 3
Items: 
Size: 374657 Color: 2
Size: 350561 Color: 4
Size: 274753 Color: 4

Bin 1829: 30 of cap free
Amount of items: 3
Items: 
Size: 655110 Color: 1
Size: 175689 Color: 3
Size: 169172 Color: 2

Bin 1830: 30 of cap free
Amount of items: 2
Items: 
Size: 598410 Color: 1
Size: 401561 Color: 3

Bin 1831: 30 of cap free
Amount of items: 2
Items: 
Size: 749041 Color: 2
Size: 250930 Color: 1

Bin 1832: 30 of cap free
Amount of items: 2
Items: 
Size: 676603 Color: 2
Size: 323368 Color: 4

Bin 1833: 31 of cap free
Amount of items: 2
Items: 
Size: 574059 Color: 0
Size: 425911 Color: 3

Bin 1834: 31 of cap free
Amount of items: 2
Items: 
Size: 514061 Color: 1
Size: 485909 Color: 4

Bin 1835: 31 of cap free
Amount of items: 2
Items: 
Size: 536339 Color: 4
Size: 463631 Color: 3

Bin 1836: 31 of cap free
Amount of items: 2
Items: 
Size: 547799 Color: 0
Size: 452171 Color: 1

Bin 1837: 31 of cap free
Amount of items: 2
Items: 
Size: 560573 Color: 4
Size: 439397 Color: 0

Bin 1838: 31 of cap free
Amount of items: 2
Items: 
Size: 603443 Color: 4
Size: 396527 Color: 2

Bin 1839: 31 of cap free
Amount of items: 2
Items: 
Size: 637009 Color: 4
Size: 362961 Color: 3

Bin 1840: 31 of cap free
Amount of items: 3
Items: 
Size: 642040 Color: 3
Size: 179091 Color: 4
Size: 178839 Color: 2

Bin 1841: 31 of cap free
Amount of items: 2
Items: 
Size: 702285 Color: 0
Size: 297685 Color: 3

Bin 1842: 31 of cap free
Amount of items: 2
Items: 
Size: 707075 Color: 2
Size: 292895 Color: 1

Bin 1843: 31 of cap free
Amount of items: 2
Items: 
Size: 742227 Color: 4
Size: 257743 Color: 1

Bin 1844: 31 of cap free
Amount of items: 2
Items: 
Size: 756598 Color: 2
Size: 243372 Color: 0

Bin 1845: 31 of cap free
Amount of items: 2
Items: 
Size: 785313 Color: 4
Size: 214657 Color: 0

Bin 1846: 31 of cap free
Amount of items: 2
Items: 
Size: 701345 Color: 2
Size: 298625 Color: 0

Bin 1847: 31 of cap free
Amount of items: 2
Items: 
Size: 753940 Color: 1
Size: 246030 Color: 2

Bin 1848: 31 of cap free
Amount of items: 2
Items: 
Size: 625732 Color: 1
Size: 374238 Color: 0

Bin 1849: 31 of cap free
Amount of items: 2
Items: 
Size: 713839 Color: 0
Size: 286131 Color: 1

Bin 1850: 31 of cap free
Amount of items: 2
Items: 
Size: 539185 Color: 4
Size: 460785 Color: 3

Bin 1851: 31 of cap free
Amount of items: 2
Items: 
Size: 654694 Color: 1
Size: 345276 Color: 0

Bin 1852: 31 of cap free
Amount of items: 3
Items: 
Size: 401624 Color: 4
Size: 344118 Color: 2
Size: 254228 Color: 1

Bin 1853: 31 of cap free
Amount of items: 2
Items: 
Size: 521047 Color: 4
Size: 478923 Color: 3

Bin 1854: 31 of cap free
Amount of items: 3
Items: 
Size: 613501 Color: 0
Size: 194028 Color: 2
Size: 192441 Color: 0

Bin 1855: 31 of cap free
Amount of items: 2
Items: 
Size: 522646 Color: 0
Size: 477324 Color: 2

Bin 1856: 31 of cap free
Amount of items: 2
Items: 
Size: 520231 Color: 3
Size: 479739 Color: 4

Bin 1857: 31 of cap free
Amount of items: 2
Items: 
Size: 776622 Color: 2
Size: 223348 Color: 1

Bin 1858: 31 of cap free
Amount of items: 2
Items: 
Size: 512511 Color: 2
Size: 487459 Color: 1

Bin 1859: 32 of cap free
Amount of items: 3
Items: 
Size: 755478 Color: 4
Size: 123533 Color: 2
Size: 120958 Color: 3

Bin 1860: 32 of cap free
Amount of items: 2
Items: 
Size: 504018 Color: 1
Size: 495951 Color: 4

Bin 1861: 32 of cap free
Amount of items: 2
Items: 
Size: 545179 Color: 1
Size: 454790 Color: 0

Bin 1862: 32 of cap free
Amount of items: 2
Items: 
Size: 562305 Color: 4
Size: 437664 Color: 2

Bin 1863: 32 of cap free
Amount of items: 2
Items: 
Size: 563275 Color: 2
Size: 436694 Color: 1

Bin 1864: 32 of cap free
Amount of items: 2
Items: 
Size: 595552 Color: 1
Size: 404417 Color: 2

Bin 1865: 32 of cap free
Amount of items: 2
Items: 
Size: 642503 Color: 2
Size: 357466 Color: 1

Bin 1866: 32 of cap free
Amount of items: 2
Items: 
Size: 645315 Color: 4
Size: 354654 Color: 0

Bin 1867: 32 of cap free
Amount of items: 2
Items: 
Size: 660958 Color: 3
Size: 339011 Color: 2

Bin 1868: 32 of cap free
Amount of items: 2
Items: 
Size: 675490 Color: 0
Size: 324479 Color: 1

Bin 1869: 32 of cap free
Amount of items: 2
Items: 
Size: 677230 Color: 1
Size: 322739 Color: 3

Bin 1870: 32 of cap free
Amount of items: 2
Items: 
Size: 678921 Color: 0
Size: 321048 Color: 4

Bin 1871: 32 of cap free
Amount of items: 2
Items: 
Size: 694744 Color: 4
Size: 305225 Color: 0

Bin 1872: 32 of cap free
Amount of items: 2
Items: 
Size: 751143 Color: 4
Size: 248826 Color: 2

Bin 1873: 32 of cap free
Amount of items: 2
Items: 
Size: 761788 Color: 2
Size: 238181 Color: 1

Bin 1874: 32 of cap free
Amount of items: 2
Items: 
Size: 778027 Color: 3
Size: 221942 Color: 2

Bin 1875: 32 of cap free
Amount of items: 2
Items: 
Size: 661671 Color: 4
Size: 338298 Color: 0

Bin 1876: 32 of cap free
Amount of items: 2
Items: 
Size: 589746 Color: 3
Size: 410223 Color: 1

Bin 1877: 32 of cap free
Amount of items: 2
Items: 
Size: 726556 Color: 0
Size: 273413 Color: 2

Bin 1878: 32 of cap free
Amount of items: 2
Items: 
Size: 715321 Color: 0
Size: 284648 Color: 3

Bin 1879: 32 of cap free
Amount of items: 2
Items: 
Size: 529645 Color: 4
Size: 470324 Color: 0

Bin 1880: 32 of cap free
Amount of items: 2
Items: 
Size: 787455 Color: 3
Size: 212514 Color: 1

Bin 1881: 32 of cap free
Amount of items: 2
Items: 
Size: 634679 Color: 1
Size: 365290 Color: 4

Bin 1882: 32 of cap free
Amount of items: 2
Items: 
Size: 706250 Color: 3
Size: 293719 Color: 1

Bin 1883: 32 of cap free
Amount of items: 2
Items: 
Size: 604290 Color: 0
Size: 395679 Color: 3

Bin 1884: 32 of cap free
Amount of items: 2
Items: 
Size: 705581 Color: 0
Size: 294388 Color: 4

Bin 1885: 32 of cap free
Amount of items: 2
Items: 
Size: 588138 Color: 4
Size: 411831 Color: 0

Bin 1886: 32 of cap free
Amount of items: 2
Items: 
Size: 760729 Color: 2
Size: 239240 Color: 4

Bin 1887: 33 of cap free
Amount of items: 3
Items: 
Size: 746170 Color: 4
Size: 126944 Color: 2
Size: 126854 Color: 0

Bin 1888: 33 of cap free
Amount of items: 2
Items: 
Size: 743649 Color: 0
Size: 256319 Color: 2

Bin 1889: 33 of cap free
Amount of items: 2
Items: 
Size: 545848 Color: 3
Size: 454120 Color: 4

Bin 1890: 33 of cap free
Amount of items: 2
Items: 
Size: 627114 Color: 3
Size: 372854 Color: 1

Bin 1891: 33 of cap free
Amount of items: 2
Items: 
Size: 633361 Color: 1
Size: 366607 Color: 4

Bin 1892: 33 of cap free
Amount of items: 2
Items: 
Size: 638266 Color: 3
Size: 361702 Color: 1

Bin 1893: 33 of cap free
Amount of items: 2
Items: 
Size: 639227 Color: 4
Size: 360741 Color: 0

Bin 1894: 33 of cap free
Amount of items: 2
Items: 
Size: 708344 Color: 3
Size: 291624 Color: 4

Bin 1895: 33 of cap free
Amount of items: 2
Items: 
Size: 724308 Color: 0
Size: 275660 Color: 3

Bin 1896: 33 of cap free
Amount of items: 2
Items: 
Size: 739293 Color: 3
Size: 260675 Color: 4

Bin 1897: 33 of cap free
Amount of items: 2
Items: 
Size: 793804 Color: 2
Size: 206164 Color: 3

Bin 1898: 33 of cap free
Amount of items: 2
Items: 
Size: 538013 Color: 0
Size: 461955 Color: 4

Bin 1899: 33 of cap free
Amount of items: 3
Items: 
Size: 699222 Color: 3
Size: 150693 Color: 3
Size: 150053 Color: 2

Bin 1900: 33 of cap free
Amount of items: 2
Items: 
Size: 504828 Color: 3
Size: 495140 Color: 0

Bin 1901: 33 of cap free
Amount of items: 3
Items: 
Size: 675344 Color: 3
Size: 162527 Color: 1
Size: 162097 Color: 0

Bin 1902: 33 of cap free
Amount of items: 3
Items: 
Size: 352930 Color: 2
Size: 352346 Color: 1
Size: 294692 Color: 1

Bin 1903: 33 of cap free
Amount of items: 2
Items: 
Size: 683272 Color: 3
Size: 316696 Color: 4

Bin 1904: 33 of cap free
Amount of items: 2
Items: 
Size: 713208 Color: 0
Size: 286760 Color: 4

Bin 1905: 33 of cap free
Amount of items: 2
Items: 
Size: 728063 Color: 0
Size: 271905 Color: 3

Bin 1906: 33 of cap free
Amount of items: 2
Items: 
Size: 645256 Color: 3
Size: 354712 Color: 2

Bin 1907: 33 of cap free
Amount of items: 2
Items: 
Size: 645363 Color: 4
Size: 354605 Color: 3

Bin 1908: 33 of cap free
Amount of items: 2
Items: 
Size: 752903 Color: 1
Size: 247065 Color: 4

Bin 1909: 33 of cap free
Amount of items: 2
Items: 
Size: 530641 Color: 4
Size: 469327 Color: 2

Bin 1910: 33 of cap free
Amount of items: 2
Items: 
Size: 579568 Color: 3
Size: 420400 Color: 0

Bin 1911: 33 of cap free
Amount of items: 2
Items: 
Size: 714093 Color: 0
Size: 285875 Color: 1

Bin 1912: 33 of cap free
Amount of items: 2
Items: 
Size: 612905 Color: 4
Size: 387063 Color: 2

Bin 1913: 33 of cap free
Amount of items: 2
Items: 
Size: 642308 Color: 2
Size: 357660 Color: 0

Bin 1914: 33 of cap free
Amount of items: 2
Items: 
Size: 633482 Color: 3
Size: 366486 Color: 4

Bin 1915: 34 of cap free
Amount of items: 2
Items: 
Size: 541283 Color: 3
Size: 458684 Color: 0

Bin 1916: 34 of cap free
Amount of items: 2
Items: 
Size: 568390 Color: 4
Size: 431577 Color: 0

Bin 1917: 34 of cap free
Amount of items: 2
Items: 
Size: 570686 Color: 3
Size: 429281 Color: 1

Bin 1918: 34 of cap free
Amount of items: 2
Items: 
Size: 571259 Color: 1
Size: 428708 Color: 0

Bin 1919: 34 of cap free
Amount of items: 2
Items: 
Size: 597182 Color: 2
Size: 402785 Color: 1

Bin 1920: 34 of cap free
Amount of items: 2
Items: 
Size: 611417 Color: 2
Size: 388550 Color: 3

Bin 1921: 34 of cap free
Amount of items: 2
Items: 
Size: 649979 Color: 4
Size: 349988 Color: 2

Bin 1922: 34 of cap free
Amount of items: 2
Items: 
Size: 687338 Color: 3
Size: 312629 Color: 4

Bin 1923: 34 of cap free
Amount of items: 2
Items: 
Size: 710269 Color: 4
Size: 289698 Color: 3

Bin 1924: 34 of cap free
Amount of items: 3
Items: 
Size: 714839 Color: 4
Size: 142754 Color: 3
Size: 142374 Color: 3

Bin 1925: 34 of cap free
Amount of items: 2
Items: 
Size: 759151 Color: 3
Size: 240816 Color: 0

Bin 1926: 34 of cap free
Amount of items: 2
Items: 
Size: 763236 Color: 2
Size: 236731 Color: 3

Bin 1927: 34 of cap free
Amount of items: 3
Items: 
Size: 362304 Color: 3
Size: 319303 Color: 4
Size: 318360 Color: 2

Bin 1928: 34 of cap free
Amount of items: 2
Items: 
Size: 720401 Color: 2
Size: 279566 Color: 1

Bin 1929: 34 of cap free
Amount of items: 2
Items: 
Size: 606361 Color: 1
Size: 393606 Color: 4

Bin 1930: 34 of cap free
Amount of items: 2
Items: 
Size: 709234 Color: 4
Size: 290733 Color: 2

Bin 1931: 34 of cap free
Amount of items: 2
Items: 
Size: 529687 Color: 0
Size: 470280 Color: 1

Bin 1932: 34 of cap free
Amount of items: 2
Items: 
Size: 654347 Color: 1
Size: 345620 Color: 3

Bin 1933: 34 of cap free
Amount of items: 2
Items: 
Size: 749486 Color: 0
Size: 250481 Color: 2

Bin 1934: 35 of cap free
Amount of items: 3
Items: 
Size: 617811 Color: 0
Size: 191267 Color: 4
Size: 190888 Color: 3

Bin 1935: 35 of cap free
Amount of items: 2
Items: 
Size: 548291 Color: 2
Size: 451675 Color: 1

Bin 1936: 35 of cap free
Amount of items: 2
Items: 
Size: 571423 Color: 1
Size: 428543 Color: 4

Bin 1937: 35 of cap free
Amount of items: 2
Items: 
Size: 587856 Color: 1
Size: 412110 Color: 2

Bin 1938: 35 of cap free
Amount of items: 2
Items: 
Size: 594443 Color: 4
Size: 405523 Color: 3

Bin 1939: 35 of cap free
Amount of items: 2
Items: 
Size: 613403 Color: 1
Size: 386563 Color: 0

Bin 1940: 35 of cap free
Amount of items: 2
Items: 
Size: 619228 Color: 1
Size: 380738 Color: 3

Bin 1941: 35 of cap free
Amount of items: 2
Items: 
Size: 619359 Color: 0
Size: 380607 Color: 4

Bin 1942: 35 of cap free
Amount of items: 2
Items: 
Size: 630217 Color: 1
Size: 369749 Color: 0

Bin 1943: 35 of cap free
Amount of items: 2
Items: 
Size: 665877 Color: 1
Size: 334089 Color: 3

Bin 1944: 35 of cap free
Amount of items: 2
Items: 
Size: 683000 Color: 2
Size: 316966 Color: 3

Bin 1945: 35 of cap free
Amount of items: 2
Items: 
Size: 687594 Color: 3
Size: 312372 Color: 4

Bin 1946: 35 of cap free
Amount of items: 2
Items: 
Size: 720768 Color: 2
Size: 279198 Color: 3

Bin 1947: 35 of cap free
Amount of items: 2
Items: 
Size: 752311 Color: 2
Size: 247655 Color: 1

Bin 1948: 35 of cap free
Amount of items: 2
Items: 
Size: 767156 Color: 2
Size: 232810 Color: 0

Bin 1949: 35 of cap free
Amount of items: 3
Items: 
Size: 664364 Color: 4
Size: 170317 Color: 0
Size: 165285 Color: 3

Bin 1950: 35 of cap free
Amount of items: 2
Items: 
Size: 527056 Color: 0
Size: 472910 Color: 4

Bin 1951: 35 of cap free
Amount of items: 2
Items: 
Size: 707354 Color: 1
Size: 292612 Color: 3

Bin 1952: 35 of cap free
Amount of items: 2
Items: 
Size: 606661 Color: 2
Size: 393305 Color: 3

Bin 1953: 35 of cap free
Amount of items: 3
Items: 
Size: 768972 Color: 4
Size: 115950 Color: 3
Size: 115044 Color: 1

Bin 1954: 35 of cap free
Amount of items: 2
Items: 
Size: 789723 Color: 1
Size: 210243 Color: 3

Bin 1955: 35 of cap free
Amount of items: 3
Items: 
Size: 506503 Color: 0
Size: 358755 Color: 0
Size: 134708 Color: 1

Bin 1956: 35 of cap free
Amount of items: 2
Items: 
Size: 602611 Color: 1
Size: 397355 Color: 3

Bin 1957: 35 of cap free
Amount of items: 2
Items: 
Size: 770990 Color: 1
Size: 228976 Color: 0

Bin 1958: 35 of cap free
Amount of items: 2
Items: 
Size: 545132 Color: 4
Size: 454834 Color: 1

Bin 1959: 36 of cap free
Amount of items: 3
Items: 
Size: 714783 Color: 1
Size: 143804 Color: 4
Size: 141378 Color: 0

Bin 1960: 36 of cap free
Amount of items: 2
Items: 
Size: 545216 Color: 0
Size: 454749 Color: 1

Bin 1961: 36 of cap free
Amount of items: 2
Items: 
Size: 671338 Color: 4
Size: 328627 Color: 3

Bin 1962: 36 of cap free
Amount of items: 2
Items: 
Size: 523621 Color: 4
Size: 476344 Color: 2

Bin 1963: 36 of cap free
Amount of items: 2
Items: 
Size: 595591 Color: 4
Size: 404374 Color: 3

Bin 1964: 36 of cap free
Amount of items: 2
Items: 
Size: 601066 Color: 1
Size: 398899 Color: 4

Bin 1965: 36 of cap free
Amount of items: 2
Items: 
Size: 609327 Color: 1
Size: 390638 Color: 2

Bin 1966: 36 of cap free
Amount of items: 2
Items: 
Size: 699114 Color: 1
Size: 300851 Color: 3

Bin 1967: 36 of cap free
Amount of items: 2
Items: 
Size: 736814 Color: 0
Size: 263151 Color: 2

Bin 1968: 36 of cap free
Amount of items: 2
Items: 
Size: 739106 Color: 2
Size: 260859 Color: 4

Bin 1969: 36 of cap free
Amount of items: 2
Items: 
Size: 504720 Color: 2
Size: 495245 Color: 4

Bin 1970: 36 of cap free
Amount of items: 2
Items: 
Size: 593715 Color: 4
Size: 406250 Color: 0

Bin 1971: 36 of cap free
Amount of items: 2
Items: 
Size: 586138 Color: 2
Size: 413827 Color: 1

Bin 1972: 36 of cap free
Amount of items: 3
Items: 
Size: 689080 Color: 3
Size: 155830 Color: 2
Size: 155055 Color: 2

Bin 1973: 36 of cap free
Amount of items: 2
Items: 
Size: 621892 Color: 3
Size: 378073 Color: 1

Bin 1974: 36 of cap free
Amount of items: 2
Items: 
Size: 586838 Color: 1
Size: 413127 Color: 0

Bin 1975: 36 of cap free
Amount of items: 2
Items: 
Size: 632698 Color: 0
Size: 367267 Color: 3

Bin 1976: 36 of cap free
Amount of items: 2
Items: 
Size: 738253 Color: 3
Size: 261712 Color: 0

Bin 1977: 36 of cap free
Amount of items: 2
Items: 
Size: 699946 Color: 4
Size: 300019 Color: 2

Bin 1978: 36 of cap free
Amount of items: 2
Items: 
Size: 747801 Color: 0
Size: 252164 Color: 1

Bin 1979: 37 of cap free
Amount of items: 3
Items: 
Size: 594214 Color: 2
Size: 211599 Color: 4
Size: 194151 Color: 0

Bin 1980: 37 of cap free
Amount of items: 2
Items: 
Size: 509230 Color: 1
Size: 490734 Color: 4

Bin 1981: 37 of cap free
Amount of items: 2
Items: 
Size: 591756 Color: 0
Size: 408208 Color: 4

Bin 1982: 37 of cap free
Amount of items: 2
Items: 
Size: 517962 Color: 2
Size: 482002 Color: 1

Bin 1983: 37 of cap free
Amount of items: 2
Items: 
Size: 525169 Color: 1
Size: 474795 Color: 2

Bin 1984: 37 of cap free
Amount of items: 2
Items: 
Size: 546915 Color: 4
Size: 453049 Color: 1

Bin 1985: 37 of cap free
Amount of items: 2
Items: 
Size: 561330 Color: 4
Size: 438634 Color: 1

Bin 1986: 37 of cap free
Amount of items: 2
Items: 
Size: 562308 Color: 1
Size: 437656 Color: 4

Bin 1987: 37 of cap free
Amount of items: 2
Items: 
Size: 578832 Color: 1
Size: 421132 Color: 2

Bin 1988: 37 of cap free
Amount of items: 3
Items: 
Size: 651672 Color: 0
Size: 177603 Color: 4
Size: 170689 Color: 1

Bin 1989: 37 of cap free
Amount of items: 2
Items: 
Size: 678806 Color: 0
Size: 321158 Color: 3

Bin 1990: 37 of cap free
Amount of items: 2
Items: 
Size: 594546 Color: 1
Size: 405418 Color: 4

Bin 1991: 37 of cap free
Amount of items: 2
Items: 
Size: 770596 Color: 3
Size: 229368 Color: 0

Bin 1992: 37 of cap free
Amount of items: 2
Items: 
Size: 609147 Color: 0
Size: 390817 Color: 3

Bin 1993: 38 of cap free
Amount of items: 3
Items: 
Size: 666354 Color: 4
Size: 166880 Color: 1
Size: 166729 Color: 3

Bin 1994: 38 of cap free
Amount of items: 3
Items: 
Size: 623116 Color: 2
Size: 188906 Color: 2
Size: 187941 Color: 4

Bin 1995: 38 of cap free
Amount of items: 2
Items: 
Size: 735415 Color: 4
Size: 264548 Color: 3

Bin 1996: 38 of cap free
Amount of items: 2
Items: 
Size: 592986 Color: 0
Size: 406977 Color: 2

Bin 1997: 38 of cap free
Amount of items: 3
Items: 
Size: 602150 Color: 0
Size: 199110 Color: 3
Size: 198703 Color: 4

Bin 1998: 38 of cap free
Amount of items: 2
Items: 
Size: 624952 Color: 1
Size: 375011 Color: 4

Bin 1999: 38 of cap free
Amount of items: 2
Items: 
Size: 659382 Color: 1
Size: 340581 Color: 4

Bin 2000: 38 of cap free
Amount of items: 2
Items: 
Size: 671790 Color: 3
Size: 328173 Color: 4

Bin 2001: 38 of cap free
Amount of items: 3
Items: 
Size: 685237 Color: 0
Size: 157531 Color: 3
Size: 157195 Color: 3

Bin 2002: 38 of cap free
Amount of items: 2
Items: 
Size: 686749 Color: 4
Size: 313214 Color: 2

Bin 2003: 38 of cap free
Amount of items: 3
Items: 
Size: 689589 Color: 0
Size: 155389 Color: 3
Size: 154985 Color: 3

Bin 2004: 38 of cap free
Amount of items: 2
Items: 
Size: 696341 Color: 1
Size: 303622 Color: 4

Bin 2005: 38 of cap free
Amount of items: 2
Items: 
Size: 766903 Color: 1
Size: 233060 Color: 2

Bin 2006: 38 of cap free
Amount of items: 2
Items: 
Size: 799480 Color: 1
Size: 200483 Color: 0

Bin 2007: 38 of cap free
Amount of items: 2
Items: 
Size: 638022 Color: 3
Size: 361941 Color: 2

Bin 2008: 38 of cap free
Amount of items: 3
Items: 
Size: 632359 Color: 0
Size: 183832 Color: 3
Size: 183772 Color: 1

Bin 2009: 38 of cap free
Amount of items: 2
Items: 
Size: 697604 Color: 2
Size: 302359 Color: 0

Bin 2010: 38 of cap free
Amount of items: 2
Items: 
Size: 742496 Color: 3
Size: 257467 Color: 1

Bin 2011: 38 of cap free
Amount of items: 2
Items: 
Size: 538519 Color: 1
Size: 461444 Color: 4

Bin 2012: 38 of cap free
Amount of items: 2
Items: 
Size: 728636 Color: 2
Size: 271327 Color: 0

Bin 2013: 38 of cap free
Amount of items: 2
Items: 
Size: 664501 Color: 1
Size: 335462 Color: 0

Bin 2014: 38 of cap free
Amount of items: 2
Items: 
Size: 569354 Color: 2
Size: 430609 Color: 1

Bin 2015: 38 of cap free
Amount of items: 2
Items: 
Size: 774340 Color: 0
Size: 225623 Color: 2

Bin 2016: 38 of cap free
Amount of items: 2
Items: 
Size: 503161 Color: 4
Size: 496802 Color: 2

Bin 2017: 39 of cap free
Amount of items: 3
Items: 
Size: 663725 Color: 1
Size: 168442 Color: 1
Size: 167795 Color: 2

Bin 2018: 39 of cap free
Amount of items: 2
Items: 
Size: 668256 Color: 4
Size: 331706 Color: 1

Bin 2019: 39 of cap free
Amount of items: 2
Items: 
Size: 521522 Color: 3
Size: 478440 Color: 2

Bin 2020: 39 of cap free
Amount of items: 2
Items: 
Size: 523483 Color: 0
Size: 476479 Color: 3

Bin 2021: 39 of cap free
Amount of items: 2
Items: 
Size: 545081 Color: 1
Size: 454881 Color: 3

Bin 2022: 39 of cap free
Amount of items: 2
Items: 
Size: 555568 Color: 4
Size: 444394 Color: 2

Bin 2023: 39 of cap free
Amount of items: 2
Items: 
Size: 593187 Color: 0
Size: 406775 Color: 4

Bin 2024: 39 of cap free
Amount of items: 2
Items: 
Size: 627662 Color: 3
Size: 372300 Color: 2

Bin 2025: 39 of cap free
Amount of items: 2
Items: 
Size: 644540 Color: 1
Size: 355422 Color: 2

Bin 2026: 39 of cap free
Amount of items: 2
Items: 
Size: 604600 Color: 4
Size: 395362 Color: 0

Bin 2027: 39 of cap free
Amount of items: 2
Items: 
Size: 733139 Color: 3
Size: 266823 Color: 1

Bin 2028: 39 of cap free
Amount of items: 2
Items: 
Size: 619749 Color: 2
Size: 380213 Color: 4

Bin 2029: 39 of cap free
Amount of items: 2
Items: 
Size: 729584 Color: 2
Size: 270378 Color: 0

Bin 2030: 39 of cap free
Amount of items: 3
Items: 
Size: 713126 Color: 2
Size: 143551 Color: 3
Size: 143285 Color: 1

Bin 2031: 39 of cap free
Amount of items: 3
Items: 
Size: 709339 Color: 3
Size: 146036 Color: 0
Size: 144587 Color: 0

Bin 2032: 39 of cap free
Amount of items: 2
Items: 
Size: 519678 Color: 0
Size: 480284 Color: 3

Bin 2033: 39 of cap free
Amount of items: 2
Items: 
Size: 644864 Color: 2
Size: 355098 Color: 0

Bin 2034: 39 of cap free
Amount of items: 2
Items: 
Size: 564995 Color: 3
Size: 434967 Color: 2

Bin 2035: 39 of cap free
Amount of items: 2
Items: 
Size: 651080 Color: 4
Size: 348882 Color: 0

Bin 2036: 39 of cap free
Amount of items: 2
Items: 
Size: 692533 Color: 4
Size: 307429 Color: 2

Bin 2037: 40 of cap free
Amount of items: 2
Items: 
Size: 511987 Color: 1
Size: 487974 Color: 3

Bin 2038: 40 of cap free
Amount of items: 2
Items: 
Size: 551892 Color: 4
Size: 448069 Color: 1

Bin 2039: 40 of cap free
Amount of items: 2
Items: 
Size: 571761 Color: 4
Size: 428200 Color: 2

Bin 2040: 40 of cap free
Amount of items: 2
Items: 
Size: 602650 Color: 3
Size: 397311 Color: 2

Bin 2041: 40 of cap free
Amount of items: 2
Items: 
Size: 619226 Color: 1
Size: 380735 Color: 2

Bin 2042: 40 of cap free
Amount of items: 2
Items: 
Size: 721210 Color: 2
Size: 278751 Color: 1

Bin 2043: 40 of cap free
Amount of items: 2
Items: 
Size: 738126 Color: 3
Size: 261835 Color: 0

Bin 2044: 40 of cap free
Amount of items: 2
Items: 
Size: 761997 Color: 2
Size: 237964 Color: 0

Bin 2045: 40 of cap free
Amount of items: 2
Items: 
Size: 797771 Color: 0
Size: 202190 Color: 1

Bin 2046: 40 of cap free
Amount of items: 2
Items: 
Size: 776804 Color: 2
Size: 223157 Color: 1

Bin 2047: 40 of cap free
Amount of items: 2
Items: 
Size: 797498 Color: 3
Size: 202463 Color: 0

Bin 2048: 40 of cap free
Amount of items: 2
Items: 
Size: 714078 Color: 1
Size: 285883 Color: 0

Bin 2049: 40 of cap free
Amount of items: 3
Items: 
Size: 563051 Color: 4
Size: 219247 Color: 0
Size: 217663 Color: 3

Bin 2050: 40 of cap free
Amount of items: 2
Items: 
Size: 748200 Color: 0
Size: 251761 Color: 3

Bin 2051: 40 of cap free
Amount of items: 2
Items: 
Size: 533889 Color: 1
Size: 466072 Color: 2

Bin 2052: 40 of cap free
Amount of items: 2
Items: 
Size: 583964 Color: 1
Size: 415997 Color: 3

Bin 2053: 41 of cap free
Amount of items: 2
Items: 
Size: 511843 Color: 0
Size: 488117 Color: 1

Bin 2054: 41 of cap free
Amount of items: 2
Items: 
Size: 518983 Color: 2
Size: 480977 Color: 0

Bin 2055: 41 of cap free
Amount of items: 2
Items: 
Size: 525519 Color: 4
Size: 474441 Color: 3

Bin 2056: 41 of cap free
Amount of items: 2
Items: 
Size: 604476 Color: 4
Size: 395484 Color: 0

Bin 2057: 41 of cap free
Amount of items: 3
Items: 
Size: 616578 Color: 2
Size: 192026 Color: 4
Size: 191356 Color: 0

Bin 2058: 41 of cap free
Amount of items: 2
Items: 
Size: 642485 Color: 3
Size: 357475 Color: 2

Bin 2059: 41 of cap free
Amount of items: 2
Items: 
Size: 648015 Color: 2
Size: 351945 Color: 1

Bin 2060: 41 of cap free
Amount of items: 2
Items: 
Size: 648654 Color: 4
Size: 351306 Color: 2

Bin 2061: 41 of cap free
Amount of items: 2
Items: 
Size: 659761 Color: 3
Size: 340199 Color: 2

Bin 2062: 41 of cap free
Amount of items: 2
Items: 
Size: 660326 Color: 3
Size: 339634 Color: 1

Bin 2063: 41 of cap free
Amount of items: 2
Items: 
Size: 672048 Color: 1
Size: 327912 Color: 4

Bin 2064: 41 of cap free
Amount of items: 2
Items: 
Size: 700244 Color: 3
Size: 299716 Color: 1

Bin 2065: 41 of cap free
Amount of items: 2
Items: 
Size: 729838 Color: 1
Size: 270122 Color: 4

Bin 2066: 41 of cap free
Amount of items: 2
Items: 
Size: 796051 Color: 4
Size: 203909 Color: 0

Bin 2067: 41 of cap free
Amount of items: 2
Items: 
Size: 521034 Color: 0
Size: 478926 Color: 4

Bin 2068: 41 of cap free
Amount of items: 2
Items: 
Size: 745869 Color: 2
Size: 254091 Color: 3

Bin 2069: 41 of cap free
Amount of items: 2
Items: 
Size: 524203 Color: 3
Size: 475757 Color: 4

Bin 2070: 42 of cap free
Amount of items: 3
Items: 
Size: 733713 Color: 2
Size: 134577 Color: 0
Size: 131669 Color: 3

Bin 2071: 42 of cap free
Amount of items: 3
Items: 
Size: 355804 Color: 2
Size: 342370 Color: 0
Size: 301785 Color: 0

Bin 2072: 42 of cap free
Amount of items: 2
Items: 
Size: 502911 Color: 0
Size: 497048 Color: 4

Bin 2073: 42 of cap free
Amount of items: 2
Items: 
Size: 505893 Color: 0
Size: 494066 Color: 2

Bin 2074: 42 of cap free
Amount of items: 2
Items: 
Size: 514720 Color: 2
Size: 485239 Color: 4

Bin 2075: 42 of cap free
Amount of items: 2
Items: 
Size: 525827 Color: 3
Size: 474132 Color: 1

Bin 2076: 42 of cap free
Amount of items: 2
Items: 
Size: 544718 Color: 2
Size: 455241 Color: 0

Bin 2077: 42 of cap free
Amount of items: 2
Items: 
Size: 547035 Color: 4
Size: 452924 Color: 3

Bin 2078: 42 of cap free
Amount of items: 2
Items: 
Size: 567907 Color: 3
Size: 432052 Color: 4

Bin 2079: 42 of cap free
Amount of items: 2
Items: 
Size: 600131 Color: 2
Size: 399828 Color: 1

Bin 2080: 42 of cap free
Amount of items: 3
Items: 
Size: 613710 Color: 3
Size: 193678 Color: 2
Size: 192571 Color: 1

Bin 2081: 42 of cap free
Amount of items: 3
Items: 
Size: 655494 Color: 3
Size: 172481 Color: 0
Size: 171984 Color: 1

Bin 2082: 42 of cap free
Amount of items: 2
Items: 
Size: 700613 Color: 4
Size: 299346 Color: 1

Bin 2083: 42 of cap free
Amount of items: 2
Items: 
Size: 705915 Color: 1
Size: 294044 Color: 2

Bin 2084: 42 of cap free
Amount of items: 2
Items: 
Size: 745137 Color: 2
Size: 254822 Color: 0

Bin 2085: 42 of cap free
Amount of items: 2
Items: 
Size: 791248 Color: 2
Size: 208711 Color: 0

Bin 2086: 42 of cap free
Amount of items: 3
Items: 
Size: 760443 Color: 3
Size: 123291 Color: 2
Size: 116225 Color: 0

Bin 2087: 42 of cap free
Amount of items: 2
Items: 
Size: 580556 Color: 0
Size: 419403 Color: 1

Bin 2088: 42 of cap free
Amount of items: 2
Items: 
Size: 540039 Color: 1
Size: 459920 Color: 4

Bin 2089: 42 of cap free
Amount of items: 2
Items: 
Size: 586703 Color: 1
Size: 413256 Color: 2

Bin 2090: 42 of cap free
Amount of items: 2
Items: 
Size: 598084 Color: 4
Size: 401875 Color: 2

Bin 2091: 42 of cap free
Amount of items: 2
Items: 
Size: 713922 Color: 1
Size: 286037 Color: 0

Bin 2092: 42 of cap free
Amount of items: 2
Items: 
Size: 761040 Color: 3
Size: 238919 Color: 2

Bin 2093: 43 of cap free
Amount of items: 2
Items: 
Size: 787596 Color: 1
Size: 212362 Color: 2

Bin 2094: 43 of cap free
Amount of items: 2
Items: 
Size: 702526 Color: 2
Size: 297432 Color: 1

Bin 2095: 43 of cap free
Amount of items: 3
Items: 
Size: 711869 Color: 0
Size: 144288 Color: 2
Size: 143801 Color: 4

Bin 2096: 43 of cap free
Amount of items: 2
Items: 
Size: 520030 Color: 4
Size: 479928 Color: 2

Bin 2097: 43 of cap free
Amount of items: 2
Items: 
Size: 526714 Color: 1
Size: 473244 Color: 2

Bin 2098: 43 of cap free
Amount of items: 2
Items: 
Size: 556510 Color: 3
Size: 443448 Color: 4

Bin 2099: 43 of cap free
Amount of items: 2
Items: 
Size: 592250 Color: 3
Size: 407708 Color: 4

Bin 2100: 43 of cap free
Amount of items: 2
Items: 
Size: 619560 Color: 2
Size: 380398 Color: 0

Bin 2101: 43 of cap free
Amount of items: 2
Items: 
Size: 624781 Color: 0
Size: 375177 Color: 3

Bin 2102: 43 of cap free
Amount of items: 2
Items: 
Size: 679043 Color: 4
Size: 320915 Color: 1

Bin 2103: 43 of cap free
Amount of items: 2
Items: 
Size: 679137 Color: 1
Size: 320821 Color: 4

Bin 2104: 43 of cap free
Amount of items: 2
Items: 
Size: 687877 Color: 3
Size: 312081 Color: 1

Bin 2105: 43 of cap free
Amount of items: 2
Items: 
Size: 703971 Color: 1
Size: 295987 Color: 3

Bin 2106: 43 of cap free
Amount of items: 2
Items: 
Size: 718944 Color: 1
Size: 281014 Color: 2

Bin 2107: 43 of cap free
Amount of items: 2
Items: 
Size: 781656 Color: 3
Size: 218302 Color: 1

Bin 2108: 43 of cap free
Amount of items: 3
Items: 
Size: 635849 Color: 2
Size: 182374 Color: 1
Size: 181735 Color: 1

Bin 2109: 43 of cap free
Amount of items: 2
Items: 
Size: 730568 Color: 4
Size: 269390 Color: 1

Bin 2110: 43 of cap free
Amount of items: 2
Items: 
Size: 634186 Color: 1
Size: 365772 Color: 0

Bin 2111: 43 of cap free
Amount of items: 2
Items: 
Size: 597639 Color: 3
Size: 402319 Color: 2

Bin 2112: 43 of cap free
Amount of items: 2
Items: 
Size: 539680 Color: 4
Size: 460278 Color: 1

Bin 2113: 44 of cap free
Amount of items: 2
Items: 
Size: 596522 Color: 3
Size: 403435 Color: 1

Bin 2114: 44 of cap free
Amount of items: 2
Items: 
Size: 757870 Color: 4
Size: 242087 Color: 0

Bin 2115: 44 of cap free
Amount of items: 2
Items: 
Size: 661662 Color: 2
Size: 338295 Color: 3

Bin 2116: 44 of cap free
Amount of items: 2
Items: 
Size: 518302 Color: 1
Size: 481655 Color: 2

Bin 2117: 44 of cap free
Amount of items: 2
Items: 
Size: 594591 Color: 4
Size: 405366 Color: 1

Bin 2118: 44 of cap free
Amount of items: 2
Items: 
Size: 693177 Color: 3
Size: 306780 Color: 2

Bin 2119: 44 of cap free
Amount of items: 2
Items: 
Size: 760541 Color: 0
Size: 239416 Color: 1

Bin 2120: 44 of cap free
Amount of items: 2
Items: 
Size: 779023 Color: 3
Size: 220934 Color: 2

Bin 2121: 44 of cap free
Amount of items: 2
Items: 
Size: 527000 Color: 2
Size: 472957 Color: 0

Bin 2122: 44 of cap free
Amount of items: 2
Items: 
Size: 777420 Color: 2
Size: 222537 Color: 4

Bin 2123: 44 of cap free
Amount of items: 2
Items: 
Size: 659449 Color: 0
Size: 340508 Color: 4

Bin 2124: 44 of cap free
Amount of items: 2
Items: 
Size: 541078 Color: 3
Size: 458879 Color: 2

Bin 2125: 44 of cap free
Amount of items: 2
Items: 
Size: 523533 Color: 1
Size: 476424 Color: 4

Bin 2126: 44 of cap free
Amount of items: 2
Items: 
Size: 659324 Color: 1
Size: 340633 Color: 0

Bin 2127: 44 of cap free
Amount of items: 2
Items: 
Size: 503231 Color: 3
Size: 496726 Color: 0

Bin 2128: 44 of cap free
Amount of items: 2
Items: 
Size: 607477 Color: 4
Size: 392480 Color: 0

Bin 2129: 45 of cap free
Amount of items: 3
Items: 
Size: 654078 Color: 1
Size: 173219 Color: 1
Size: 172659 Color: 3

Bin 2130: 45 of cap free
Amount of items: 3
Items: 
Size: 682003 Color: 3
Size: 159851 Color: 1
Size: 158102 Color: 1

Bin 2131: 45 of cap free
Amount of items: 3
Items: 
Size: 661709 Color: 0
Size: 169744 Color: 2
Size: 168503 Color: 1

Bin 2132: 45 of cap free
Amount of items: 2
Items: 
Size: 672412 Color: 3
Size: 327544 Color: 2

Bin 2133: 45 of cap free
Amount of items: 2
Items: 
Size: 513908 Color: 4
Size: 486048 Color: 0

Bin 2134: 45 of cap free
Amount of items: 2
Items: 
Size: 578092 Color: 3
Size: 421864 Color: 2

Bin 2135: 45 of cap free
Amount of items: 2
Items: 
Size: 600554 Color: 4
Size: 399402 Color: 1

Bin 2136: 45 of cap free
Amount of items: 2
Items: 
Size: 641926 Color: 4
Size: 358030 Color: 3

Bin 2137: 45 of cap free
Amount of items: 2
Items: 
Size: 668824 Color: 1
Size: 331132 Color: 0

Bin 2138: 45 of cap free
Amount of items: 2
Items: 
Size: 702835 Color: 2
Size: 297121 Color: 1

Bin 2139: 45 of cap free
Amount of items: 2
Items: 
Size: 723059 Color: 3
Size: 276897 Color: 4

Bin 2140: 45 of cap free
Amount of items: 2
Items: 
Size: 667716 Color: 0
Size: 332240 Color: 2

Bin 2141: 45 of cap free
Amount of items: 2
Items: 
Size: 577369 Color: 4
Size: 422587 Color: 3

Bin 2142: 45 of cap free
Amount of items: 3
Items: 
Size: 603665 Color: 4
Size: 198832 Color: 1
Size: 197459 Color: 2

Bin 2143: 45 of cap free
Amount of items: 2
Items: 
Size: 703519 Color: 3
Size: 296437 Color: 0

Bin 2144: 45 of cap free
Amount of items: 2
Items: 
Size: 556108 Color: 3
Size: 443848 Color: 4

Bin 2145: 45 of cap free
Amount of items: 2
Items: 
Size: 518063 Color: 0
Size: 481893 Color: 1

Bin 2146: 45 of cap free
Amount of items: 2
Items: 
Size: 531964 Color: 1
Size: 467992 Color: 0

Bin 2147: 45 of cap free
Amount of items: 2
Items: 
Size: 502322 Color: 1
Size: 497634 Color: 0

Bin 2148: 45 of cap free
Amount of items: 2
Items: 
Size: 749876 Color: 4
Size: 250080 Color: 0

Bin 2149: 45 of cap free
Amount of items: 2
Items: 
Size: 761370 Color: 0
Size: 238586 Color: 4

Bin 2150: 46 of cap free
Amount of items: 2
Items: 
Size: 501179 Color: 2
Size: 498776 Color: 0

Bin 2151: 46 of cap free
Amount of items: 2
Items: 
Size: 518164 Color: 1
Size: 481791 Color: 2

Bin 2152: 46 of cap free
Amount of items: 2
Items: 
Size: 533170 Color: 4
Size: 466785 Color: 0

Bin 2153: 46 of cap free
Amount of items: 2
Items: 
Size: 587162 Color: 3
Size: 412793 Color: 2

Bin 2154: 46 of cap free
Amount of items: 2
Items: 
Size: 630857 Color: 0
Size: 369098 Color: 3

Bin 2155: 46 of cap free
Amount of items: 2
Items: 
Size: 652760 Color: 1
Size: 347195 Color: 2

Bin 2156: 46 of cap free
Amount of items: 2
Items: 
Size: 713660 Color: 1
Size: 286295 Color: 4

Bin 2157: 46 of cap free
Amount of items: 2
Items: 
Size: 731750 Color: 3
Size: 268205 Color: 4

Bin 2158: 46 of cap free
Amount of items: 2
Items: 
Size: 767691 Color: 4
Size: 232264 Color: 3

Bin 2159: 46 of cap free
Amount of items: 2
Items: 
Size: 789442 Color: 2
Size: 210513 Color: 0

Bin 2160: 46 of cap free
Amount of items: 2
Items: 
Size: 548111 Color: 4
Size: 451844 Color: 2

Bin 2161: 46 of cap free
Amount of items: 3
Items: 
Size: 621493 Color: 4
Size: 190369 Color: 4
Size: 188093 Color: 0

Bin 2162: 46 of cap free
Amount of items: 2
Items: 
Size: 685632 Color: 0
Size: 314323 Color: 4

Bin 2163: 46 of cap free
Amount of items: 2
Items: 
Size: 756857 Color: 3
Size: 243098 Color: 1

Bin 2164: 46 of cap free
Amount of items: 4
Items: 
Size: 256718 Color: 0
Size: 254168 Color: 4
Size: 253744 Color: 2
Size: 235325 Color: 3

Bin 2165: 47 of cap free
Amount of items: 3
Items: 
Size: 706718 Color: 3
Size: 146878 Color: 4
Size: 146358 Color: 0

Bin 2166: 47 of cap free
Amount of items: 2
Items: 
Size: 725487 Color: 4
Size: 274467 Color: 3

Bin 2167: 47 of cap free
Amount of items: 2
Items: 
Size: 777440 Color: 4
Size: 222514 Color: 3

Bin 2168: 47 of cap free
Amount of items: 2
Items: 
Size: 539620 Color: 1
Size: 460334 Color: 0

Bin 2169: 47 of cap free
Amount of items: 2
Items: 
Size: 600716 Color: 4
Size: 399238 Color: 3

Bin 2170: 47 of cap free
Amount of items: 2
Items: 
Size: 611109 Color: 4
Size: 388845 Color: 1

Bin 2171: 47 of cap free
Amount of items: 2
Items: 
Size: 640379 Color: 1
Size: 359575 Color: 2

Bin 2172: 47 of cap free
Amount of items: 2
Items: 
Size: 652718 Color: 4
Size: 347236 Color: 1

Bin 2173: 47 of cap free
Amount of items: 2
Items: 
Size: 687633 Color: 1
Size: 312321 Color: 0

Bin 2174: 47 of cap free
Amount of items: 2
Items: 
Size: 705503 Color: 3
Size: 294451 Color: 2

Bin 2175: 47 of cap free
Amount of items: 2
Items: 
Size: 732048 Color: 3
Size: 267906 Color: 4

Bin 2176: 47 of cap free
Amount of items: 2
Items: 
Size: 794733 Color: 3
Size: 205221 Color: 0

Bin 2177: 47 of cap free
Amount of items: 2
Items: 
Size: 515277 Color: 3
Size: 484677 Color: 4

Bin 2178: 47 of cap free
Amount of items: 2
Items: 
Size: 790847 Color: 3
Size: 209107 Color: 1

Bin 2179: 47 of cap free
Amount of items: 2
Items: 
Size: 797561 Color: 3
Size: 202393 Color: 4

Bin 2180: 47 of cap free
Amount of items: 2
Items: 
Size: 656913 Color: 1
Size: 343041 Color: 4

Bin 2181: 47 of cap free
Amount of items: 2
Items: 
Size: 639999 Color: 1
Size: 359955 Color: 0

Bin 2182: 47 of cap free
Amount of items: 2
Items: 
Size: 780430 Color: 3
Size: 219524 Color: 1

Bin 2183: 47 of cap free
Amount of items: 2
Items: 
Size: 615695 Color: 4
Size: 384259 Color: 3

Bin 2184: 47 of cap free
Amount of items: 2
Items: 
Size: 506629 Color: 2
Size: 493325 Color: 3

Bin 2185: 48 of cap free
Amount of items: 2
Items: 
Size: 621437 Color: 2
Size: 378516 Color: 0

Bin 2186: 48 of cap free
Amount of items: 2
Items: 
Size: 501658 Color: 4
Size: 498295 Color: 0

Bin 2187: 48 of cap free
Amount of items: 3
Items: 
Size: 528520 Color: 0
Size: 236564 Color: 0
Size: 234869 Color: 3

Bin 2188: 48 of cap free
Amount of items: 2
Items: 
Size: 558027 Color: 3
Size: 441926 Color: 4

Bin 2189: 48 of cap free
Amount of items: 2
Items: 
Size: 580015 Color: 1
Size: 419938 Color: 2

Bin 2190: 48 of cap free
Amount of items: 2
Items: 
Size: 602493 Color: 1
Size: 397460 Color: 4

Bin 2191: 48 of cap free
Amount of items: 2
Items: 
Size: 643919 Color: 0
Size: 356034 Color: 2

Bin 2192: 48 of cap free
Amount of items: 2
Items: 
Size: 727118 Color: 2
Size: 272835 Color: 3

Bin 2193: 48 of cap free
Amount of items: 2
Items: 
Size: 737654 Color: 0
Size: 262299 Color: 3

Bin 2194: 48 of cap free
Amount of items: 2
Items: 
Size: 796680 Color: 4
Size: 203273 Color: 0

Bin 2195: 48 of cap free
Amount of items: 2
Items: 
Size: 576446 Color: 4
Size: 423507 Color: 3

Bin 2196: 48 of cap free
Amount of items: 3
Items: 
Size: 526548 Color: 3
Size: 237112 Color: 2
Size: 236293 Color: 3

Bin 2197: 48 of cap free
Amount of items: 2
Items: 
Size: 544266 Color: 2
Size: 455687 Color: 3

Bin 2198: 48 of cap free
Amount of items: 2
Items: 
Size: 737858 Color: 2
Size: 262095 Color: 1

Bin 2199: 48 of cap free
Amount of items: 2
Items: 
Size: 659655 Color: 1
Size: 340298 Color: 2

Bin 2200: 48 of cap free
Amount of items: 3
Items: 
Size: 743355 Color: 1
Size: 128323 Color: 3
Size: 128275 Color: 1

Bin 2201: 48 of cap free
Amount of items: 2
Items: 
Size: 726968 Color: 4
Size: 272985 Color: 3

Bin 2202: 48 of cap free
Amount of items: 3
Items: 
Size: 420924 Color: 0
Size: 416283 Color: 0
Size: 162746 Color: 4

Bin 2203: 48 of cap free
Amount of items: 2
Items: 
Size: 629401 Color: 0
Size: 370552 Color: 4

Bin 2204: 49 of cap free
Amount of items: 2
Items: 
Size: 551138 Color: 3
Size: 448814 Color: 1

Bin 2205: 49 of cap free
Amount of items: 2
Items: 
Size: 568633 Color: 1
Size: 431319 Color: 2

Bin 2206: 49 of cap free
Amount of items: 2
Items: 
Size: 570567 Color: 4
Size: 429385 Color: 0

Bin 2207: 49 of cap free
Amount of items: 2
Items: 
Size: 629154 Color: 4
Size: 370798 Color: 1

Bin 2208: 49 of cap free
Amount of items: 2
Items: 
Size: 735153 Color: 0
Size: 264799 Color: 2

Bin 2209: 49 of cap free
Amount of items: 2
Items: 
Size: 762572 Color: 4
Size: 237380 Color: 2

Bin 2210: 49 of cap free
Amount of items: 2
Items: 
Size: 788514 Color: 1
Size: 211438 Color: 0

Bin 2211: 49 of cap free
Amount of items: 2
Items: 
Size: 795019 Color: 2
Size: 204933 Color: 0

Bin 2212: 49 of cap free
Amount of items: 2
Items: 
Size: 626052 Color: 3
Size: 373900 Color: 2

Bin 2213: 49 of cap free
Amount of items: 2
Items: 
Size: 611119 Color: 0
Size: 388833 Color: 4

Bin 2214: 49 of cap free
Amount of items: 2
Items: 
Size: 508326 Color: 2
Size: 491626 Color: 4

Bin 2215: 49 of cap free
Amount of items: 2
Items: 
Size: 714143 Color: 0
Size: 285809 Color: 2

Bin 2216: 49 of cap free
Amount of items: 2
Items: 
Size: 776511 Color: 4
Size: 223441 Color: 2

Bin 2217: 50 of cap free
Amount of items: 2
Items: 
Size: 748703 Color: 0
Size: 251248 Color: 1

Bin 2218: 50 of cap free
Amount of items: 2
Items: 
Size: 695587 Color: 4
Size: 304364 Color: 2

Bin 2219: 50 of cap free
Amount of items: 3
Items: 
Size: 691311 Color: 3
Size: 154401 Color: 0
Size: 154239 Color: 1

Bin 2220: 50 of cap free
Amount of items: 2
Items: 
Size: 539492 Color: 1
Size: 460459 Color: 4

Bin 2221: 50 of cap free
Amount of items: 2
Items: 
Size: 507713 Color: 1
Size: 492238 Color: 2

Bin 2222: 50 of cap free
Amount of items: 2
Items: 
Size: 527935 Color: 4
Size: 472016 Color: 1

Bin 2223: 50 of cap free
Amount of items: 2
Items: 
Size: 583567 Color: 2
Size: 416384 Color: 4

Bin 2224: 50 of cap free
Amount of items: 2
Items: 
Size: 591679 Color: 0
Size: 408272 Color: 2

Bin 2225: 50 of cap free
Amount of items: 2
Items: 
Size: 606816 Color: 4
Size: 393135 Color: 1

Bin 2226: 50 of cap free
Amount of items: 2
Items: 
Size: 629137 Color: 0
Size: 370814 Color: 4

Bin 2227: 50 of cap free
Amount of items: 2
Items: 
Size: 642673 Color: 4
Size: 357278 Color: 0

Bin 2228: 50 of cap free
Amount of items: 2
Items: 
Size: 709786 Color: 0
Size: 290165 Color: 2

Bin 2229: 50 of cap free
Amount of items: 2
Items: 
Size: 567298 Color: 4
Size: 432653 Color: 3

Bin 2230: 50 of cap free
Amount of items: 2
Items: 
Size: 754516 Color: 2
Size: 245435 Color: 3

Bin 2231: 51 of cap free
Amount of items: 2
Items: 
Size: 539169 Color: 1
Size: 460781 Color: 0

Bin 2232: 51 of cap free
Amount of items: 2
Items: 
Size: 574268 Color: 2
Size: 425682 Color: 1

Bin 2233: 51 of cap free
Amount of items: 2
Items: 
Size: 708646 Color: 0
Size: 291304 Color: 2

Bin 2234: 51 of cap free
Amount of items: 2
Items: 
Size: 745025 Color: 0
Size: 254925 Color: 1

Bin 2235: 51 of cap free
Amount of items: 2
Items: 
Size: 536670 Color: 0
Size: 463280 Color: 4

Bin 2236: 51 of cap free
Amount of items: 2
Items: 
Size: 567052 Color: 4
Size: 432898 Color: 3

Bin 2237: 51 of cap free
Amount of items: 2
Items: 
Size: 628582 Color: 1
Size: 371368 Color: 3

Bin 2238: 51 of cap free
Amount of items: 2
Items: 
Size: 647723 Color: 4
Size: 352227 Color: 0

Bin 2239: 51 of cap free
Amount of items: 2
Items: 
Size: 649558 Color: 0
Size: 350392 Color: 4

Bin 2240: 51 of cap free
Amount of items: 2
Items: 
Size: 528007 Color: 2
Size: 471943 Color: 3

Bin 2241: 51 of cap free
Amount of items: 2
Items: 
Size: 619959 Color: 1
Size: 379991 Color: 2

Bin 2242: 51 of cap free
Amount of items: 2
Items: 
Size: 707423 Color: 4
Size: 292527 Color: 3

Bin 2243: 51 of cap free
Amount of items: 2
Items: 
Size: 759559 Color: 0
Size: 240391 Color: 1

Bin 2244: 51 of cap free
Amount of items: 2
Items: 
Size: 514300 Color: 2
Size: 485650 Color: 4

Bin 2245: 51 of cap free
Amount of items: 2
Items: 
Size: 736471 Color: 4
Size: 263479 Color: 1

Bin 2246: 51 of cap free
Amount of items: 2
Items: 
Size: 652502 Color: 4
Size: 347448 Color: 3

Bin 2247: 52 of cap free
Amount of items: 3
Items: 
Size: 684237 Color: 0
Size: 158617 Color: 4
Size: 157095 Color: 3

Bin 2248: 52 of cap free
Amount of items: 2
Items: 
Size: 575041 Color: 1
Size: 424908 Color: 3

Bin 2249: 52 of cap free
Amount of items: 2
Items: 
Size: 510858 Color: 2
Size: 489091 Color: 3

Bin 2250: 52 of cap free
Amount of items: 2
Items: 
Size: 560776 Color: 2
Size: 439173 Color: 1

Bin 2251: 52 of cap free
Amount of items: 2
Items: 
Size: 586292 Color: 3
Size: 413657 Color: 0

Bin 2252: 52 of cap free
Amount of items: 2
Items: 
Size: 587850 Color: 3
Size: 412099 Color: 2

Bin 2253: 52 of cap free
Amount of items: 2
Items: 
Size: 594930 Color: 1
Size: 405019 Color: 3

Bin 2254: 52 of cap free
Amount of items: 2
Items: 
Size: 595128 Color: 0
Size: 404821 Color: 1

Bin 2255: 52 of cap free
Amount of items: 2
Items: 
Size: 669556 Color: 4
Size: 330393 Color: 2

Bin 2256: 52 of cap free
Amount of items: 2
Items: 
Size: 680899 Color: 1
Size: 319050 Color: 3

Bin 2257: 52 of cap free
Amount of items: 2
Items: 
Size: 702686 Color: 2
Size: 297263 Color: 4

Bin 2258: 52 of cap free
Amount of items: 2
Items: 
Size: 726442 Color: 4
Size: 273507 Color: 2

Bin 2259: 52 of cap free
Amount of items: 2
Items: 
Size: 627035 Color: 2
Size: 372914 Color: 1

Bin 2260: 52 of cap free
Amount of items: 2
Items: 
Size: 778948 Color: 3
Size: 221001 Color: 1

Bin 2261: 52 of cap free
Amount of items: 2
Items: 
Size: 556017 Color: 0
Size: 443932 Color: 4

Bin 2262: 53 of cap free
Amount of items: 2
Items: 
Size: 506334 Color: 3
Size: 493614 Color: 1

Bin 2263: 53 of cap free
Amount of items: 2
Items: 
Size: 508820 Color: 2
Size: 491128 Color: 4

Bin 2264: 53 of cap free
Amount of items: 2
Items: 
Size: 510302 Color: 1
Size: 489646 Color: 2

Bin 2265: 53 of cap free
Amount of items: 2
Items: 
Size: 526161 Color: 4
Size: 473787 Color: 3

Bin 2266: 53 of cap free
Amount of items: 2
Items: 
Size: 535919 Color: 3
Size: 464029 Color: 0

Bin 2267: 53 of cap free
Amount of items: 2
Items: 
Size: 558560 Color: 1
Size: 441388 Color: 3

Bin 2268: 53 of cap free
Amount of items: 2
Items: 
Size: 583814 Color: 1
Size: 416134 Color: 2

Bin 2269: 53 of cap free
Amount of items: 2
Items: 
Size: 584769 Color: 1
Size: 415179 Color: 4

Bin 2270: 53 of cap free
Amount of items: 2
Items: 
Size: 621083 Color: 3
Size: 378865 Color: 0

Bin 2271: 53 of cap free
Amount of items: 2
Items: 
Size: 630303 Color: 4
Size: 369645 Color: 2

Bin 2272: 53 of cap free
Amount of items: 2
Items: 
Size: 636889 Color: 4
Size: 363059 Color: 3

Bin 2273: 53 of cap free
Amount of items: 2
Items: 
Size: 712433 Color: 2
Size: 287515 Color: 4

Bin 2274: 53 of cap free
Amount of items: 2
Items: 
Size: 765229 Color: 2
Size: 234719 Color: 1

Bin 2275: 53 of cap free
Amount of items: 2
Items: 
Size: 775141 Color: 0
Size: 224807 Color: 4

Bin 2276: 53 of cap free
Amount of items: 2
Items: 
Size: 535117 Color: 1
Size: 464831 Color: 4

Bin 2277: 53 of cap free
Amount of items: 2
Items: 
Size: 513050 Color: 4
Size: 486898 Color: 1

Bin 2278: 53 of cap free
Amount of items: 2
Items: 
Size: 613214 Color: 4
Size: 386734 Color: 3

Bin 2279: 53 of cap free
Amount of items: 2
Items: 
Size: 721098 Color: 3
Size: 278850 Color: 1

Bin 2280: 53 of cap free
Amount of items: 2
Items: 
Size: 666864 Color: 2
Size: 333084 Color: 0

Bin 2281: 54 of cap free
Amount of items: 3
Items: 
Size: 660144 Color: 2
Size: 170604 Color: 2
Size: 169199 Color: 4

Bin 2282: 54 of cap free
Amount of items: 2
Items: 
Size: 520324 Color: 3
Size: 479623 Color: 4

Bin 2283: 54 of cap free
Amount of items: 2
Items: 
Size: 599517 Color: 4
Size: 400430 Color: 3

Bin 2284: 54 of cap free
Amount of items: 2
Items: 
Size: 604526 Color: 4
Size: 395421 Color: 3

Bin 2285: 54 of cap free
Amount of items: 2
Items: 
Size: 666673 Color: 2
Size: 333274 Color: 3

Bin 2286: 54 of cap free
Amount of items: 2
Items: 
Size: 669738 Color: 0
Size: 330209 Color: 3

Bin 2287: 54 of cap free
Amount of items: 2
Items: 
Size: 693953 Color: 2
Size: 305994 Color: 0

Bin 2288: 54 of cap free
Amount of items: 2
Items: 
Size: 734415 Color: 3
Size: 265532 Color: 1

Bin 2289: 54 of cap free
Amount of items: 2
Items: 
Size: 560018 Color: 3
Size: 439929 Color: 0

Bin 2290: 54 of cap free
Amount of items: 2
Items: 
Size: 733131 Color: 4
Size: 266816 Color: 1

Bin 2291: 54 of cap free
Amount of items: 2
Items: 
Size: 542325 Color: 1
Size: 457622 Color: 4

Bin 2292: 54 of cap free
Amount of items: 2
Items: 
Size: 678984 Color: 0
Size: 320963 Color: 1

Bin 2293: 54 of cap free
Amount of items: 2
Items: 
Size: 559146 Color: 2
Size: 440801 Color: 3

Bin 2294: 54 of cap free
Amount of items: 2
Items: 
Size: 769802 Color: 4
Size: 230145 Color: 1

Bin 2295: 54 of cap free
Amount of items: 2
Items: 
Size: 631141 Color: 0
Size: 368806 Color: 1

Bin 2296: 55 of cap free
Amount of items: 2
Items: 
Size: 568062 Color: 1
Size: 431884 Color: 0

Bin 2297: 55 of cap free
Amount of items: 2
Items: 
Size: 595714 Color: 3
Size: 404232 Color: 1

Bin 2298: 55 of cap free
Amount of items: 2
Items: 
Size: 595756 Color: 1
Size: 404190 Color: 2

Bin 2299: 55 of cap free
Amount of items: 2
Items: 
Size: 690591 Color: 3
Size: 309355 Color: 4

Bin 2300: 55 of cap free
Amount of items: 2
Items: 
Size: 759904 Color: 1
Size: 240042 Color: 0

Bin 2301: 55 of cap free
Amount of items: 3
Items: 
Size: 575922 Color: 1
Size: 212260 Color: 1
Size: 211764 Color: 3

Bin 2302: 55 of cap free
Amount of items: 2
Items: 
Size: 537301 Color: 2
Size: 462645 Color: 4

Bin 2303: 55 of cap free
Amount of items: 2
Items: 
Size: 625914 Color: 2
Size: 374032 Color: 0

Bin 2304: 55 of cap free
Amount of items: 2
Items: 
Size: 704029 Color: 3
Size: 295917 Color: 0

Bin 2305: 55 of cap free
Amount of items: 2
Items: 
Size: 559261 Color: 3
Size: 440685 Color: 0

Bin 2306: 55 of cap free
Amount of items: 3
Items: 
Size: 667073 Color: 2
Size: 167536 Color: 2
Size: 165337 Color: 4

Bin 2307: 55 of cap free
Amount of items: 2
Items: 
Size: 794652 Color: 0
Size: 205294 Color: 4

Bin 2308: 56 of cap free
Amount of items: 2
Items: 
Size: 695929 Color: 0
Size: 304016 Color: 1

Bin 2309: 56 of cap free
Amount of items: 2
Items: 
Size: 631515 Color: 2
Size: 368430 Color: 4

Bin 2310: 56 of cap free
Amount of items: 2
Items: 
Size: 512938 Color: 0
Size: 487007 Color: 4

Bin 2311: 56 of cap free
Amount of items: 2
Items: 
Size: 528829 Color: 0
Size: 471116 Color: 3

Bin 2312: 56 of cap free
Amount of items: 2
Items: 
Size: 550758 Color: 1
Size: 449187 Color: 4

Bin 2313: 56 of cap free
Amount of items: 2
Items: 
Size: 585938 Color: 0
Size: 414007 Color: 2

Bin 2314: 56 of cap free
Amount of items: 2
Items: 
Size: 623498 Color: 3
Size: 376447 Color: 4

Bin 2315: 56 of cap free
Amount of items: 2
Items: 
Size: 719531 Color: 1
Size: 280414 Color: 2

Bin 2316: 56 of cap free
Amount of items: 2
Items: 
Size: 751944 Color: 4
Size: 248001 Color: 0

Bin 2317: 56 of cap free
Amount of items: 2
Items: 
Size: 787179 Color: 3
Size: 212766 Color: 0

Bin 2318: 56 of cap free
Amount of items: 2
Items: 
Size: 789496 Color: 3
Size: 210449 Color: 4

Bin 2319: 56 of cap free
Amount of items: 2
Items: 
Size: 705256 Color: 3
Size: 294689 Color: 0

Bin 2320: 56 of cap free
Amount of items: 2
Items: 
Size: 770592 Color: 3
Size: 229353 Color: 0

Bin 2321: 56 of cap free
Amount of items: 2
Items: 
Size: 729420 Color: 4
Size: 270525 Color: 1

Bin 2322: 56 of cap free
Amount of items: 2
Items: 
Size: 633030 Color: 0
Size: 366915 Color: 2

Bin 2323: 56 of cap free
Amount of items: 2
Items: 
Size: 570267 Color: 3
Size: 429678 Color: 4

Bin 2324: 56 of cap free
Amount of items: 2
Items: 
Size: 499998 Color: 3
Size: 499947 Color: 1

Bin 2325: 57 of cap free
Amount of items: 2
Items: 
Size: 673287 Color: 4
Size: 326657 Color: 3

Bin 2326: 57 of cap free
Amount of items: 3
Items: 
Size: 603381 Color: 3
Size: 199391 Color: 0
Size: 197172 Color: 2

Bin 2327: 57 of cap free
Amount of items: 2
Items: 
Size: 643758 Color: 1
Size: 356186 Color: 4

Bin 2328: 57 of cap free
Amount of items: 2
Items: 
Size: 501332 Color: 0
Size: 498612 Color: 1

Bin 2329: 57 of cap free
Amount of items: 2
Items: 
Size: 554899 Color: 0
Size: 445045 Color: 2

Bin 2330: 57 of cap free
Amount of items: 2
Items: 
Size: 561192 Color: 2
Size: 438752 Color: 1

Bin 2331: 57 of cap free
Amount of items: 2
Items: 
Size: 568755 Color: 2
Size: 431189 Color: 0

Bin 2332: 57 of cap free
Amount of items: 2
Items: 
Size: 586328 Color: 0
Size: 413616 Color: 2

Bin 2333: 57 of cap free
Amount of items: 2
Items: 
Size: 588570 Color: 2
Size: 411374 Color: 4

Bin 2334: 57 of cap free
Amount of items: 2
Items: 
Size: 615489 Color: 3
Size: 384455 Color: 1

Bin 2335: 57 of cap free
Amount of items: 3
Items: 
Size: 658725 Color: 4
Size: 170811 Color: 2
Size: 170408 Color: 2

Bin 2336: 57 of cap free
Amount of items: 2
Items: 
Size: 616841 Color: 3
Size: 383103 Color: 4

Bin 2337: 57 of cap free
Amount of items: 2
Items: 
Size: 793469 Color: 2
Size: 206475 Color: 4

Bin 2338: 57 of cap free
Amount of items: 2
Items: 
Size: 608323 Color: 1
Size: 391621 Color: 0

Bin 2339: 57 of cap free
Amount of items: 2
Items: 
Size: 522872 Color: 3
Size: 477072 Color: 1

Bin 2340: 57 of cap free
Amount of items: 2
Items: 
Size: 579025 Color: 2
Size: 420919 Color: 3

Bin 2341: 57 of cap free
Amount of items: 2
Items: 
Size: 566081 Color: 4
Size: 433863 Color: 0

Bin 2342: 57 of cap free
Amount of items: 2
Items: 
Size: 748011 Color: 3
Size: 251933 Color: 1

Bin 2343: 58 of cap free
Amount of items: 2
Items: 
Size: 657087 Color: 3
Size: 342856 Color: 0

Bin 2344: 58 of cap free
Amount of items: 2
Items: 
Size: 725767 Color: 2
Size: 274176 Color: 0

Bin 2345: 58 of cap free
Amount of items: 2
Items: 
Size: 512710 Color: 4
Size: 487233 Color: 0

Bin 2346: 58 of cap free
Amount of items: 2
Items: 
Size: 562709 Color: 1
Size: 437234 Color: 0

Bin 2347: 58 of cap free
Amount of items: 2
Items: 
Size: 616330 Color: 1
Size: 383613 Color: 4

Bin 2348: 58 of cap free
Amount of items: 2
Items: 
Size: 659253 Color: 0
Size: 340690 Color: 1

Bin 2349: 58 of cap free
Amount of items: 2
Items: 
Size: 712675 Color: 2
Size: 287268 Color: 0

Bin 2350: 58 of cap free
Amount of items: 2
Items: 
Size: 737246 Color: 2
Size: 262697 Color: 0

Bin 2351: 58 of cap free
Amount of items: 2
Items: 
Size: 764959 Color: 3
Size: 234984 Color: 2

Bin 2352: 58 of cap free
Amount of items: 2
Items: 
Size: 736798 Color: 3
Size: 263145 Color: 0

Bin 2353: 58 of cap free
Amount of items: 2
Items: 
Size: 668500 Color: 4
Size: 331443 Color: 0

Bin 2354: 58 of cap free
Amount of items: 2
Items: 
Size: 755363 Color: 2
Size: 244580 Color: 1

Bin 2355: 58 of cap free
Amount of items: 2
Items: 
Size: 623610 Color: 0
Size: 376333 Color: 3

Bin 2356: 58 of cap free
Amount of items: 3
Items: 
Size: 588904 Color: 3
Size: 206036 Color: 4
Size: 205003 Color: 0

Bin 2357: 59 of cap free
Amount of items: 2
Items: 
Size: 660706 Color: 0
Size: 339236 Color: 2

Bin 2358: 59 of cap free
Amount of items: 2
Items: 
Size: 542580 Color: 3
Size: 457362 Color: 0

Bin 2359: 59 of cap free
Amount of items: 2
Items: 
Size: 606251 Color: 3
Size: 393691 Color: 1

Bin 2360: 59 of cap free
Amount of items: 2
Items: 
Size: 645117 Color: 2
Size: 354825 Color: 1

Bin 2361: 59 of cap free
Amount of items: 2
Items: 
Size: 656983 Color: 1
Size: 342959 Color: 2

Bin 2362: 59 of cap free
Amount of items: 2
Items: 
Size: 684541 Color: 3
Size: 315401 Color: 2

Bin 2363: 59 of cap free
Amount of items: 2
Items: 
Size: 704514 Color: 2
Size: 295428 Color: 1

Bin 2364: 59 of cap free
Amount of items: 2
Items: 
Size: 709714 Color: 0
Size: 290228 Color: 3

Bin 2365: 59 of cap free
Amount of items: 2
Items: 
Size: 755961 Color: 3
Size: 243981 Color: 0

Bin 2366: 59 of cap free
Amount of items: 2
Items: 
Size: 505153 Color: 4
Size: 494789 Color: 3

Bin 2367: 59 of cap free
Amount of items: 2
Items: 
Size: 549922 Color: 4
Size: 450020 Color: 3

Bin 2368: 59 of cap free
Amount of items: 2
Items: 
Size: 796448 Color: 4
Size: 203494 Color: 0

Bin 2369: 59 of cap free
Amount of items: 2
Items: 
Size: 675591 Color: 4
Size: 324351 Color: 0

Bin 2370: 59 of cap free
Amount of items: 2
Items: 
Size: 506937 Color: 1
Size: 493005 Color: 0

Bin 2371: 60 of cap free
Amount of items: 2
Items: 
Size: 605179 Color: 3
Size: 394762 Color: 2

Bin 2372: 60 of cap free
Amount of items: 2
Items: 
Size: 637863 Color: 1
Size: 362078 Color: 3

Bin 2373: 60 of cap free
Amount of items: 2
Items: 
Size: 692234 Color: 4
Size: 307707 Color: 1

Bin 2374: 60 of cap free
Amount of items: 2
Items: 
Size: 698868 Color: 2
Size: 301073 Color: 1

Bin 2375: 60 of cap free
Amount of items: 2
Items: 
Size: 700179 Color: 2
Size: 299762 Color: 3

Bin 2376: 60 of cap free
Amount of items: 2
Items: 
Size: 716153 Color: 0
Size: 283788 Color: 1

Bin 2377: 60 of cap free
Amount of items: 2
Items: 
Size: 716545 Color: 2
Size: 283396 Color: 1

Bin 2378: 60 of cap free
Amount of items: 2
Items: 
Size: 788943 Color: 0
Size: 210998 Color: 2

Bin 2379: 60 of cap free
Amount of items: 2
Items: 
Size: 733860 Color: 3
Size: 266081 Color: 1

Bin 2380: 60 of cap free
Amount of items: 2
Items: 
Size: 666261 Color: 1
Size: 333680 Color: 3

Bin 2381: 60 of cap free
Amount of items: 2
Items: 
Size: 635156 Color: 4
Size: 364785 Color: 1

Bin 2382: 60 of cap free
Amount of items: 3
Items: 
Size: 692972 Color: 1
Size: 154916 Color: 1
Size: 152053 Color: 4

Bin 2383: 60 of cap free
Amount of items: 2
Items: 
Size: 719155 Color: 2
Size: 280786 Color: 0

Bin 2384: 60 of cap free
Amount of items: 2
Items: 
Size: 522759 Color: 3
Size: 477182 Color: 0

Bin 2385: 60 of cap free
Amount of items: 2
Items: 
Size: 593927 Color: 3
Size: 406014 Color: 0

Bin 2386: 60 of cap free
Amount of items: 2
Items: 
Size: 534165 Color: 3
Size: 465776 Color: 2

Bin 2387: 60 of cap free
Amount of items: 2
Items: 
Size: 750867 Color: 2
Size: 249074 Color: 4

Bin 2388: 60 of cap free
Amount of items: 3
Items: 
Size: 528909 Color: 0
Size: 268097 Color: 0
Size: 202935 Color: 1

Bin 2389: 61 of cap free
Amount of items: 2
Items: 
Size: 536624 Color: 1
Size: 463316 Color: 0

Bin 2390: 61 of cap free
Amount of items: 2
Items: 
Size: 773459 Color: 1
Size: 226481 Color: 2

Bin 2391: 61 of cap free
Amount of items: 2
Items: 
Size: 763926 Color: 3
Size: 236014 Color: 2

Bin 2392: 61 of cap free
Amount of items: 2
Items: 
Size: 514043 Color: 2
Size: 485897 Color: 1

Bin 2393: 61 of cap free
Amount of items: 2
Items: 
Size: 547864 Color: 0
Size: 452076 Color: 4

Bin 2394: 61 of cap free
Amount of items: 2
Items: 
Size: 568043 Color: 4
Size: 431897 Color: 1

Bin 2395: 61 of cap free
Amount of items: 2
Items: 
Size: 573365 Color: 4
Size: 426575 Color: 3

Bin 2396: 61 of cap free
Amount of items: 2
Items: 
Size: 577821 Color: 0
Size: 422119 Color: 2

Bin 2397: 61 of cap free
Amount of items: 2
Items: 
Size: 581095 Color: 2
Size: 418845 Color: 1

Bin 2398: 61 of cap free
Amount of items: 2
Items: 
Size: 591946 Color: 3
Size: 407994 Color: 2

Bin 2399: 61 of cap free
Amount of items: 2
Items: 
Size: 618506 Color: 4
Size: 381434 Color: 0

Bin 2400: 61 of cap free
Amount of items: 2
Items: 
Size: 732736 Color: 2
Size: 267204 Color: 3

Bin 2401: 61 of cap free
Amount of items: 2
Items: 
Size: 782711 Color: 2
Size: 217229 Color: 3

Bin 2402: 61 of cap free
Amount of items: 2
Items: 
Size: 527546 Color: 4
Size: 472394 Color: 3

Bin 2403: 61 of cap free
Amount of items: 2
Items: 
Size: 711473 Color: 2
Size: 288467 Color: 0

Bin 2404: 61 of cap free
Amount of items: 2
Items: 
Size: 604216 Color: 3
Size: 395724 Color: 1

Bin 2405: 61 of cap free
Amount of items: 3
Items: 
Size: 652247 Color: 3
Size: 174774 Color: 3
Size: 172919 Color: 1

Bin 2406: 61 of cap free
Amount of items: 2
Items: 
Size: 673508 Color: 2
Size: 326432 Color: 3

Bin 2407: 61 of cap free
Amount of items: 2
Items: 
Size: 506103 Color: 3
Size: 493837 Color: 2

Bin 2408: 61 of cap free
Amount of items: 2
Items: 
Size: 673595 Color: 4
Size: 326345 Color: 2

Bin 2409: 62 of cap free
Amount of items: 2
Items: 
Size: 523903 Color: 0
Size: 476036 Color: 4

Bin 2410: 62 of cap free
Amount of items: 2
Items: 
Size: 530448 Color: 3
Size: 469491 Color: 4

Bin 2411: 62 of cap free
Amount of items: 2
Items: 
Size: 545013 Color: 2
Size: 454926 Color: 0

Bin 2412: 62 of cap free
Amount of items: 2
Items: 
Size: 545342 Color: 0
Size: 454597 Color: 2

Bin 2413: 62 of cap free
Amount of items: 2
Items: 
Size: 614511 Color: 3
Size: 385428 Color: 0

Bin 2414: 62 of cap free
Amount of items: 2
Items: 
Size: 663526 Color: 0
Size: 336413 Color: 2

Bin 2415: 62 of cap free
Amount of items: 2
Items: 
Size: 670878 Color: 2
Size: 329061 Color: 3

Bin 2416: 62 of cap free
Amount of items: 2
Items: 
Size: 775305 Color: 2
Size: 224634 Color: 0

Bin 2417: 62 of cap free
Amount of items: 2
Items: 
Size: 701395 Color: 1
Size: 298544 Color: 0

Bin 2418: 62 of cap free
Amount of items: 2
Items: 
Size: 681065 Color: 0
Size: 318874 Color: 1

Bin 2419: 62 of cap free
Amount of items: 2
Items: 
Size: 754380 Color: 0
Size: 245559 Color: 4

Bin 2420: 62 of cap free
Amount of items: 2
Items: 
Size: 550572 Color: 0
Size: 449367 Color: 4

Bin 2421: 62 of cap free
Amount of items: 2
Items: 
Size: 785194 Color: 0
Size: 214745 Color: 4

Bin 2422: 63 of cap free
Amount of items: 2
Items: 
Size: 596516 Color: 0
Size: 403422 Color: 1

Bin 2423: 63 of cap free
Amount of items: 2
Items: 
Size: 500676 Color: 0
Size: 499262 Color: 4

Bin 2424: 63 of cap free
Amount of items: 2
Items: 
Size: 552010 Color: 3
Size: 447928 Color: 0

Bin 2425: 63 of cap free
Amount of items: 2
Items: 
Size: 565133 Color: 1
Size: 434805 Color: 4

Bin 2426: 63 of cap free
Amount of items: 2
Items: 
Size: 570540 Color: 2
Size: 429398 Color: 4

Bin 2427: 63 of cap free
Amount of items: 2
Items: 
Size: 590781 Color: 3
Size: 409157 Color: 2

Bin 2428: 63 of cap free
Amount of items: 2
Items: 
Size: 648994 Color: 0
Size: 350944 Color: 1

Bin 2429: 63 of cap free
Amount of items: 2
Items: 
Size: 661964 Color: 3
Size: 337974 Color: 4

Bin 2430: 63 of cap free
Amount of items: 2
Items: 
Size: 697031 Color: 3
Size: 302907 Color: 4

Bin 2431: 63 of cap free
Amount of items: 2
Items: 
Size: 711861 Color: 2
Size: 288077 Color: 3

Bin 2432: 63 of cap free
Amount of items: 2
Items: 
Size: 727277 Color: 0
Size: 272661 Color: 4

Bin 2433: 63 of cap free
Amount of items: 3
Items: 
Size: 747168 Color: 3
Size: 126991 Color: 1
Size: 125779 Color: 2

Bin 2434: 63 of cap free
Amount of items: 2
Items: 
Size: 780200 Color: 4
Size: 219738 Color: 1

Bin 2435: 63 of cap free
Amount of items: 2
Items: 
Size: 629276 Color: 0
Size: 370662 Color: 3

Bin 2436: 63 of cap free
Amount of items: 2
Items: 
Size: 568301 Color: 3
Size: 431637 Color: 0

Bin 2437: 63 of cap free
Amount of items: 2
Items: 
Size: 571484 Color: 0
Size: 428454 Color: 2

Bin 2438: 63 of cap free
Amount of items: 2
Items: 
Size: 785402 Color: 1
Size: 214536 Color: 0

Bin 2439: 63 of cap free
Amount of items: 2
Items: 
Size: 750594 Color: 2
Size: 249344 Color: 1

Bin 2440: 64 of cap free
Amount of items: 2
Items: 
Size: 503289 Color: 4
Size: 496648 Color: 3

Bin 2441: 64 of cap free
Amount of items: 2
Items: 
Size: 504633 Color: 3
Size: 495304 Color: 1

Bin 2442: 64 of cap free
Amount of items: 2
Items: 
Size: 533156 Color: 1
Size: 466781 Color: 2

Bin 2443: 64 of cap free
Amount of items: 2
Items: 
Size: 544329 Color: 1
Size: 455608 Color: 3

Bin 2444: 64 of cap free
Amount of items: 2
Items: 
Size: 744898 Color: 4
Size: 255039 Color: 2

Bin 2445: 64 of cap free
Amount of items: 2
Items: 
Size: 778032 Color: 2
Size: 221905 Color: 4

Bin 2446: 64 of cap free
Amount of items: 2
Items: 
Size: 783108 Color: 1
Size: 216829 Color: 0

Bin 2447: 64 of cap free
Amount of items: 2
Items: 
Size: 797920 Color: 2
Size: 202017 Color: 0

Bin 2448: 64 of cap free
Amount of items: 2
Items: 
Size: 578920 Color: 3
Size: 421017 Color: 2

Bin 2449: 64 of cap free
Amount of items: 2
Items: 
Size: 795157 Color: 4
Size: 204780 Color: 0

Bin 2450: 64 of cap free
Amount of items: 3
Items: 
Size: 605392 Color: 3
Size: 197457 Color: 1
Size: 197088 Color: 0

Bin 2451: 64 of cap free
Amount of items: 2
Items: 
Size: 750135 Color: 2
Size: 249802 Color: 1

Bin 2452: 64 of cap free
Amount of items: 2
Items: 
Size: 558643 Color: 2
Size: 441294 Color: 0

Bin 2453: 64 of cap free
Amount of items: 2
Items: 
Size: 679692 Color: 2
Size: 320245 Color: 3

Bin 2454: 64 of cap free
Amount of items: 2
Items: 
Size: 655357 Color: 4
Size: 344580 Color: 2

Bin 2455: 65 of cap free
Amount of items: 2
Items: 
Size: 524185 Color: 0
Size: 475751 Color: 2

Bin 2456: 65 of cap free
Amount of items: 2
Items: 
Size: 522313 Color: 2
Size: 477623 Color: 3

Bin 2457: 65 of cap free
Amount of items: 2
Items: 
Size: 549034 Color: 4
Size: 450902 Color: 3

Bin 2458: 65 of cap free
Amount of items: 2
Items: 
Size: 584898 Color: 4
Size: 415038 Color: 0

Bin 2459: 65 of cap free
Amount of items: 2
Items: 
Size: 738402 Color: 0
Size: 261534 Color: 2

Bin 2460: 65 of cap free
Amount of items: 2
Items: 
Size: 792250 Color: 4
Size: 207686 Color: 2

Bin 2461: 65 of cap free
Amount of items: 2
Items: 
Size: 562404 Color: 0
Size: 437532 Color: 4

Bin 2462: 65 of cap free
Amount of items: 2
Items: 
Size: 655686 Color: 1
Size: 344250 Color: 2

Bin 2463: 65 of cap free
Amount of items: 2
Items: 
Size: 570888 Color: 0
Size: 429048 Color: 2

Bin 2464: 65 of cap free
Amount of items: 2
Items: 
Size: 771229 Color: 2
Size: 228707 Color: 0

Bin 2465: 65 of cap free
Amount of items: 2
Items: 
Size: 689104 Color: 2
Size: 310832 Color: 0

Bin 2466: 65 of cap free
Amount of items: 2
Items: 
Size: 585264 Color: 3
Size: 414672 Color: 0

Bin 2467: 65 of cap free
Amount of items: 2
Items: 
Size: 612074 Color: 4
Size: 387862 Color: 0

Bin 2468: 66 of cap free
Amount of items: 2
Items: 
Size: 761092 Color: 0
Size: 238843 Color: 1

Bin 2469: 66 of cap free
Amount of items: 2
Items: 
Size: 666229 Color: 0
Size: 333706 Color: 1

Bin 2470: 66 of cap free
Amount of items: 2
Items: 
Size: 503490 Color: 3
Size: 496445 Color: 4

Bin 2471: 66 of cap free
Amount of items: 2
Items: 
Size: 603201 Color: 1
Size: 396734 Color: 0

Bin 2472: 66 of cap free
Amount of items: 2
Items: 
Size: 795475 Color: 4
Size: 204460 Color: 0

Bin 2473: 66 of cap free
Amount of items: 2
Items: 
Size: 507351 Color: 2
Size: 492584 Color: 4

Bin 2474: 66 of cap free
Amount of items: 2
Items: 
Size: 591024 Color: 3
Size: 408911 Color: 4

Bin 2475: 66 of cap free
Amount of items: 2
Items: 
Size: 653363 Color: 4
Size: 346572 Color: 0

Bin 2476: 67 of cap free
Amount of items: 2
Items: 
Size: 594066 Color: 4
Size: 405868 Color: 0

Bin 2477: 67 of cap free
Amount of items: 2
Items: 
Size: 757937 Color: 4
Size: 241997 Color: 3

Bin 2478: 67 of cap free
Amount of items: 2
Items: 
Size: 516157 Color: 0
Size: 483777 Color: 3

Bin 2479: 67 of cap free
Amount of items: 2
Items: 
Size: 582223 Color: 1
Size: 417711 Color: 4

Bin 2480: 67 of cap free
Amount of items: 3
Items: 
Size: 625446 Color: 1
Size: 187372 Color: 4
Size: 187116 Color: 0

Bin 2481: 67 of cap free
Amount of items: 2
Items: 
Size: 627333 Color: 3
Size: 372601 Color: 4

Bin 2482: 67 of cap free
Amount of items: 2
Items: 
Size: 656453 Color: 1
Size: 343481 Color: 0

Bin 2483: 67 of cap free
Amount of items: 2
Items: 
Size: 739391 Color: 0
Size: 260543 Color: 4

Bin 2484: 67 of cap free
Amount of items: 2
Items: 
Size: 530028 Color: 3
Size: 469906 Color: 0

Bin 2485: 67 of cap free
Amount of items: 2
Items: 
Size: 756142 Color: 1
Size: 243792 Color: 2

Bin 2486: 67 of cap free
Amount of items: 2
Items: 
Size: 616540 Color: 4
Size: 383394 Color: 2

Bin 2487: 67 of cap free
Amount of items: 2
Items: 
Size: 677681 Color: 2
Size: 322253 Color: 3

Bin 2488: 67 of cap free
Amount of items: 2
Items: 
Size: 685829 Color: 4
Size: 314105 Color: 3

Bin 2489: 67 of cap free
Amount of items: 2
Items: 
Size: 750523 Color: 1
Size: 249411 Color: 3

Bin 2490: 68 of cap free
Amount of items: 2
Items: 
Size: 794819 Color: 0
Size: 205114 Color: 4

Bin 2491: 68 of cap free
Amount of items: 2
Items: 
Size: 636984 Color: 4
Size: 362949 Color: 2

Bin 2492: 68 of cap free
Amount of items: 3
Items: 
Size: 585381 Color: 2
Size: 207380 Color: 2
Size: 207172 Color: 3

Bin 2493: 68 of cap free
Amount of items: 2
Items: 
Size: 526541 Color: 0
Size: 473392 Color: 1

Bin 2494: 68 of cap free
Amount of items: 2
Items: 
Size: 542175 Color: 2
Size: 457758 Color: 0

Bin 2495: 68 of cap free
Amount of items: 2
Items: 
Size: 564277 Color: 3
Size: 435656 Color: 0

Bin 2496: 68 of cap free
Amount of items: 2
Items: 
Size: 567384 Color: 2
Size: 432549 Color: 3

Bin 2497: 68 of cap free
Amount of items: 2
Items: 
Size: 662160 Color: 4
Size: 337773 Color: 1

Bin 2498: 68 of cap free
Amount of items: 2
Items: 
Size: 704093 Color: 4
Size: 295840 Color: 2

Bin 2499: 68 of cap free
Amount of items: 2
Items: 
Size: 737936 Color: 4
Size: 261997 Color: 1

Bin 2500: 68 of cap free
Amount of items: 2
Items: 
Size: 747903 Color: 4
Size: 252030 Color: 3

Bin 2501: 68 of cap free
Amount of items: 2
Items: 
Size: 758096 Color: 2
Size: 241837 Color: 0

Bin 2502: 68 of cap free
Amount of items: 2
Items: 
Size: 764454 Color: 2
Size: 235479 Color: 4

Bin 2503: 68 of cap free
Amount of items: 2
Items: 
Size: 728901 Color: 4
Size: 271032 Color: 2

Bin 2504: 68 of cap free
Amount of items: 2
Items: 
Size: 755579 Color: 1
Size: 244354 Color: 2

Bin 2505: 68 of cap free
Amount of items: 2
Items: 
Size: 590675 Color: 3
Size: 409258 Color: 1

Bin 2506: 68 of cap free
Amount of items: 2
Items: 
Size: 797482 Color: 3
Size: 202451 Color: 2

Bin 2507: 69 of cap free
Amount of items: 3
Items: 
Size: 751488 Color: 0
Size: 128094 Color: 3
Size: 120350 Color: 0

Bin 2508: 69 of cap free
Amount of items: 2
Items: 
Size: 761218 Color: 4
Size: 238714 Color: 0

Bin 2509: 69 of cap free
Amount of items: 2
Items: 
Size: 619954 Color: 1
Size: 379978 Color: 0

Bin 2510: 69 of cap free
Amount of items: 2
Items: 
Size: 724163 Color: 1
Size: 275769 Color: 2

Bin 2511: 69 of cap free
Amount of items: 2
Items: 
Size: 691722 Color: 1
Size: 308210 Color: 4

Bin 2512: 69 of cap free
Amount of items: 2
Items: 
Size: 747652 Color: 2
Size: 252280 Color: 4

Bin 2513: 69 of cap free
Amount of items: 2
Items: 
Size: 748555 Color: 4
Size: 251377 Color: 3

Bin 2514: 69 of cap free
Amount of items: 2
Items: 
Size: 729580 Color: 2
Size: 270352 Color: 0

Bin 2515: 69 of cap free
Amount of items: 2
Items: 
Size: 639780 Color: 0
Size: 360152 Color: 4

Bin 2516: 69 of cap free
Amount of items: 3
Items: 
Size: 651428 Color: 3
Size: 240253 Color: 0
Size: 108251 Color: 3

Bin 2517: 69 of cap free
Amount of items: 2
Items: 
Size: 610474 Color: 4
Size: 389458 Color: 0

Bin 2518: 70 of cap free
Amount of items: 3
Items: 
Size: 685210 Color: 0
Size: 157628 Color: 2
Size: 157093 Color: 2

Bin 2519: 70 of cap free
Amount of items: 2
Items: 
Size: 503587 Color: 4
Size: 496344 Color: 0

Bin 2520: 70 of cap free
Amount of items: 2
Items: 
Size: 541914 Color: 0
Size: 458017 Color: 3

Bin 2521: 70 of cap free
Amount of items: 2
Items: 
Size: 552122 Color: 2
Size: 447809 Color: 0

Bin 2522: 70 of cap free
Amount of items: 2
Items: 
Size: 556127 Color: 4
Size: 443804 Color: 1

Bin 2523: 70 of cap free
Amount of items: 2
Items: 
Size: 682214 Color: 1
Size: 317717 Color: 3

Bin 2524: 70 of cap free
Amount of items: 3
Items: 
Size: 746462 Color: 3
Size: 127166 Color: 1
Size: 126303 Color: 3

Bin 2525: 70 of cap free
Amount of items: 2
Items: 
Size: 541165 Color: 2
Size: 458766 Color: 0

Bin 2526: 70 of cap free
Amount of items: 2
Items: 
Size: 580534 Color: 2
Size: 419397 Color: 4

Bin 2527: 70 of cap free
Amount of items: 2
Items: 
Size: 720496 Color: 3
Size: 279435 Color: 4

Bin 2528: 71 of cap free
Amount of items: 2
Items: 
Size: 560549 Color: 2
Size: 439381 Color: 1

Bin 2529: 71 of cap free
Amount of items: 2
Items: 
Size: 563448 Color: 3
Size: 436482 Color: 1

Bin 2530: 71 of cap free
Amount of items: 2
Items: 
Size: 707396 Color: 0
Size: 292534 Color: 4

Bin 2531: 71 of cap free
Amount of items: 2
Items: 
Size: 740189 Color: 0
Size: 259741 Color: 2

Bin 2532: 71 of cap free
Amount of items: 2
Items: 
Size: 774680 Color: 0
Size: 225250 Color: 3

Bin 2533: 71 of cap free
Amount of items: 2
Items: 
Size: 541632 Color: 2
Size: 458298 Color: 0

Bin 2534: 71 of cap free
Amount of items: 2
Items: 
Size: 646679 Color: 4
Size: 353251 Color: 3

Bin 2535: 71 of cap free
Amount of items: 2
Items: 
Size: 728133 Color: 2
Size: 271797 Color: 3

Bin 2536: 71 of cap free
Amount of items: 2
Items: 
Size: 648735 Color: 4
Size: 351195 Color: 2

Bin 2537: 71 of cap free
Amount of items: 2
Items: 
Size: 744354 Color: 3
Size: 255576 Color: 0

Bin 2538: 72 of cap free
Amount of items: 2
Items: 
Size: 604945 Color: 1
Size: 394984 Color: 0

Bin 2539: 72 of cap free
Amount of items: 2
Items: 
Size: 570725 Color: 4
Size: 429204 Color: 3

Bin 2540: 72 of cap free
Amount of items: 2
Items: 
Size: 615840 Color: 3
Size: 384089 Color: 4

Bin 2541: 72 of cap free
Amount of items: 3
Items: 
Size: 637787 Color: 3
Size: 181238 Color: 0
Size: 180904 Color: 3

Bin 2542: 72 of cap free
Amount of items: 2
Items: 
Size: 687043 Color: 1
Size: 312886 Color: 4

Bin 2543: 72 of cap free
Amount of items: 2
Items: 
Size: 709613 Color: 0
Size: 290316 Color: 4

Bin 2544: 72 of cap free
Amount of items: 2
Items: 
Size: 790659 Color: 0
Size: 209270 Color: 1

Bin 2545: 72 of cap free
Amount of items: 3
Items: 
Size: 733225 Color: 4
Size: 135650 Color: 2
Size: 131054 Color: 4

Bin 2546: 72 of cap free
Amount of items: 2
Items: 
Size: 544070 Color: 3
Size: 455859 Color: 2

Bin 2547: 72 of cap free
Amount of items: 2
Items: 
Size: 523525 Color: 2
Size: 476404 Color: 4

Bin 2548: 72 of cap free
Amount of items: 2
Items: 
Size: 633067 Color: 2
Size: 366862 Color: 4

Bin 2549: 73 of cap free
Amount of items: 2
Items: 
Size: 623841 Color: 0
Size: 376087 Color: 2

Bin 2550: 73 of cap free
Amount of items: 2
Items: 
Size: 581049 Color: 0
Size: 418879 Color: 2

Bin 2551: 73 of cap free
Amount of items: 2
Items: 
Size: 719149 Color: 0
Size: 280779 Color: 1

Bin 2552: 73 of cap free
Amount of items: 2
Items: 
Size: 768251 Color: 3
Size: 231677 Color: 4

Bin 2553: 73 of cap free
Amount of items: 2
Items: 
Size: 785065 Color: 0
Size: 214863 Color: 3

Bin 2554: 73 of cap free
Amount of items: 2
Items: 
Size: 799991 Color: 0
Size: 199937 Color: 4

Bin 2555: 73 of cap free
Amount of items: 2
Items: 
Size: 612330 Color: 3
Size: 387598 Color: 1

Bin 2556: 73 of cap free
Amount of items: 2
Items: 
Size: 644401 Color: 1
Size: 355527 Color: 3

Bin 2557: 73 of cap free
Amount of items: 2
Items: 
Size: 645703 Color: 1
Size: 354225 Color: 3

Bin 2558: 73 of cap free
Amount of items: 2
Items: 
Size: 796120 Color: 1
Size: 203808 Color: 4

Bin 2559: 73 of cap free
Amount of items: 2
Items: 
Size: 712034 Color: 0
Size: 287894 Color: 2

Bin 2560: 73 of cap free
Amount of items: 2
Items: 
Size: 778838 Color: 4
Size: 221090 Color: 0

Bin 2561: 73 of cap free
Amount of items: 3
Items: 
Size: 559263 Color: 0
Size: 220389 Color: 2
Size: 220276 Color: 4

Bin 2562: 73 of cap free
Amount of items: 2
Items: 
Size: 655373 Color: 2
Size: 344555 Color: 4

Bin 2563: 73 of cap free
Amount of items: 2
Items: 
Size: 634949 Color: 2
Size: 364979 Color: 1

Bin 2564: 74 of cap free
Amount of items: 2
Items: 
Size: 554534 Color: 3
Size: 445393 Color: 2

Bin 2565: 74 of cap free
Amount of items: 2
Items: 
Size: 689802 Color: 1
Size: 310125 Color: 2

Bin 2566: 74 of cap free
Amount of items: 2
Items: 
Size: 732195 Color: 1
Size: 267732 Color: 4

Bin 2567: 74 of cap free
Amount of items: 3
Items: 
Size: 752820 Color: 4
Size: 124331 Color: 0
Size: 122776 Color: 2

Bin 2568: 74 of cap free
Amount of items: 2
Items: 
Size: 729709 Color: 3
Size: 270218 Color: 2

Bin 2569: 74 of cap free
Amount of items: 2
Items: 
Size: 635936 Color: 4
Size: 363991 Color: 2

Bin 2570: 74 of cap free
Amount of items: 2
Items: 
Size: 702518 Color: 0
Size: 297409 Color: 4

Bin 2571: 74 of cap free
Amount of items: 2
Items: 
Size: 544905 Color: 0
Size: 455022 Color: 4

Bin 2572: 74 of cap free
Amount of items: 2
Items: 
Size: 500669 Color: 3
Size: 499258 Color: 1

Bin 2573: 75 of cap free
Amount of items: 2
Items: 
Size: 557683 Color: 0
Size: 442243 Color: 2

Bin 2574: 75 of cap free
Amount of items: 2
Items: 
Size: 747021 Color: 2
Size: 252905 Color: 1

Bin 2575: 75 of cap free
Amount of items: 2
Items: 
Size: 779651 Color: 3
Size: 220275 Color: 4

Bin 2576: 75 of cap free
Amount of items: 2
Items: 
Size: 573852 Color: 3
Size: 426074 Color: 4

Bin 2577: 75 of cap free
Amount of items: 2
Items: 
Size: 646498 Color: 1
Size: 353428 Color: 0

Bin 2578: 75 of cap free
Amount of items: 2
Items: 
Size: 690377 Color: 3
Size: 309549 Color: 1

Bin 2579: 75 of cap free
Amount of items: 2
Items: 
Size: 707509 Color: 1
Size: 292417 Color: 2

Bin 2580: 75 of cap free
Amount of items: 2
Items: 
Size: 738731 Color: 3
Size: 261195 Color: 4

Bin 2581: 75 of cap free
Amount of items: 2
Items: 
Size: 791599 Color: 3
Size: 208327 Color: 2

Bin 2582: 75 of cap free
Amount of items: 2
Items: 
Size: 619209 Color: 0
Size: 380717 Color: 2

Bin 2583: 75 of cap free
Amount of items: 2
Items: 
Size: 513048 Color: 4
Size: 486878 Color: 1

Bin 2584: 75 of cap free
Amount of items: 2
Items: 
Size: 515022 Color: 3
Size: 484904 Color: 4

Bin 2585: 75 of cap free
Amount of items: 2
Items: 
Size: 541364 Color: 2
Size: 458562 Color: 3

Bin 2586: 75 of cap free
Amount of items: 2
Items: 
Size: 582783 Color: 0
Size: 417143 Color: 3

Bin 2587: 75 of cap free
Amount of items: 2
Items: 
Size: 606963 Color: 0
Size: 392963 Color: 2

Bin 2588: 75 of cap free
Amount of items: 2
Items: 
Size: 669549 Color: 1
Size: 330377 Color: 0

Bin 2589: 75 of cap free
Amount of items: 2
Items: 
Size: 510189 Color: 4
Size: 489737 Color: 3

Bin 2590: 75 of cap free
Amount of items: 2
Items: 
Size: 597615 Color: 2
Size: 402311 Color: 0

Bin 2591: 75 of cap free
Amount of items: 2
Items: 
Size: 504051 Color: 1
Size: 495875 Color: 2

Bin 2592: 75 of cap free
Amount of items: 3
Items: 
Size: 509897 Color: 2
Size: 362285 Color: 0
Size: 127744 Color: 2

Bin 2593: 76 of cap free
Amount of items: 2
Items: 
Size: 517502 Color: 0
Size: 482423 Color: 4

Bin 2594: 76 of cap free
Amount of items: 2
Items: 
Size: 523665 Color: 4
Size: 476260 Color: 1

Bin 2595: 76 of cap free
Amount of items: 2
Items: 
Size: 555341 Color: 2
Size: 444584 Color: 3

Bin 2596: 76 of cap free
Amount of items: 2
Items: 
Size: 589537 Color: 2
Size: 410388 Color: 4

Bin 2597: 76 of cap free
Amount of items: 2
Items: 
Size: 665504 Color: 1
Size: 334421 Color: 0

Bin 2598: 76 of cap free
Amount of items: 2
Items: 
Size: 672324 Color: 3
Size: 327601 Color: 1

Bin 2599: 76 of cap free
Amount of items: 2
Items: 
Size: 705066 Color: 3
Size: 294859 Color: 2

Bin 2600: 76 of cap free
Amount of items: 2
Items: 
Size: 720119 Color: 4
Size: 279806 Color: 2

Bin 2601: 76 of cap free
Amount of items: 2
Items: 
Size: 730298 Color: 3
Size: 269627 Color: 0

Bin 2602: 76 of cap free
Amount of items: 2
Items: 
Size: 742751 Color: 4
Size: 257174 Color: 0

Bin 2603: 76 of cap free
Amount of items: 3
Items: 
Size: 647079 Color: 4
Size: 176707 Color: 1
Size: 176139 Color: 0

Bin 2604: 76 of cap free
Amount of items: 2
Items: 
Size: 683950 Color: 3
Size: 315975 Color: 2

Bin 2605: 76 of cap free
Amount of items: 2
Items: 
Size: 566278 Color: 2
Size: 433647 Color: 4

Bin 2606: 76 of cap free
Amount of items: 2
Items: 
Size: 587060 Color: 2
Size: 412865 Color: 3

Bin 2607: 76 of cap free
Amount of items: 2
Items: 
Size: 648263 Color: 1
Size: 351662 Color: 4

Bin 2608: 76 of cap free
Amount of items: 2
Items: 
Size: 668822 Color: 3
Size: 331103 Color: 0

Bin 2609: 76 of cap free
Amount of items: 2
Items: 
Size: 726532 Color: 3
Size: 273393 Color: 0

Bin 2610: 77 of cap free
Amount of items: 2
Items: 
Size: 699377 Color: 0
Size: 300547 Color: 2

Bin 2611: 77 of cap free
Amount of items: 2
Items: 
Size: 535105 Color: 3
Size: 464819 Color: 0

Bin 2612: 77 of cap free
Amount of items: 2
Items: 
Size: 509996 Color: 0
Size: 489928 Color: 3

Bin 2613: 77 of cap free
Amount of items: 2
Items: 
Size: 549363 Color: 1
Size: 450561 Color: 2

Bin 2614: 77 of cap free
Amount of items: 2
Items: 
Size: 560405 Color: 4
Size: 439519 Color: 1

Bin 2615: 77 of cap free
Amount of items: 2
Items: 
Size: 602852 Color: 3
Size: 397072 Color: 2

Bin 2616: 77 of cap free
Amount of items: 2
Items: 
Size: 734872 Color: 3
Size: 265052 Color: 0

Bin 2617: 77 of cap free
Amount of items: 2
Items: 
Size: 583902 Color: 3
Size: 416022 Color: 1

Bin 2618: 77 of cap free
Amount of items: 2
Items: 
Size: 669286 Color: 2
Size: 330638 Color: 1

Bin 2619: 77 of cap free
Amount of items: 2
Items: 
Size: 693056 Color: 3
Size: 306868 Color: 0

Bin 2620: 78 of cap free
Amount of items: 3
Items: 
Size: 733482 Color: 4
Size: 133800 Color: 3
Size: 132641 Color: 4

Bin 2621: 78 of cap free
Amount of items: 2
Items: 
Size: 748001 Color: 1
Size: 251922 Color: 4

Bin 2622: 78 of cap free
Amount of items: 2
Items: 
Size: 702046 Color: 1
Size: 297877 Color: 4

Bin 2623: 78 of cap free
Amount of items: 2
Items: 
Size: 791746 Color: 1
Size: 208177 Color: 0

Bin 2624: 78 of cap free
Amount of items: 2
Items: 
Size: 544215 Color: 1
Size: 455708 Color: 2

Bin 2625: 78 of cap free
Amount of items: 2
Items: 
Size: 576306 Color: 1
Size: 423617 Color: 4

Bin 2626: 78 of cap free
Amount of items: 2
Items: 
Size: 629239 Color: 1
Size: 370684 Color: 0

Bin 2627: 78 of cap free
Amount of items: 2
Items: 
Size: 639502 Color: 0
Size: 360421 Color: 2

Bin 2628: 78 of cap free
Amount of items: 2
Items: 
Size: 697496 Color: 1
Size: 302427 Color: 4

Bin 2629: 78 of cap free
Amount of items: 2
Items: 
Size: 716158 Color: 1
Size: 283765 Color: 3

Bin 2630: 78 of cap free
Amount of items: 2
Items: 
Size: 592590 Color: 1
Size: 407333 Color: 2

Bin 2631: 78 of cap free
Amount of items: 2
Items: 
Size: 738930 Color: 1
Size: 260993 Color: 2

Bin 2632: 78 of cap free
Amount of items: 2
Items: 
Size: 608181 Color: 2
Size: 391742 Color: 0

Bin 2633: 78 of cap free
Amount of items: 2
Items: 
Size: 529794 Color: 2
Size: 470129 Color: 1

Bin 2634: 79 of cap free
Amount of items: 2
Items: 
Size: 623961 Color: 0
Size: 375961 Color: 1

Bin 2635: 79 of cap free
Amount of items: 2
Items: 
Size: 664489 Color: 1
Size: 335433 Color: 0

Bin 2636: 79 of cap free
Amount of items: 2
Items: 
Size: 621203 Color: 0
Size: 378719 Color: 3

Bin 2637: 79 of cap free
Amount of items: 2
Items: 
Size: 669719 Color: 2
Size: 330203 Color: 4

Bin 2638: 79 of cap free
Amount of items: 2
Items: 
Size: 669954 Color: 4
Size: 329968 Color: 0

Bin 2639: 79 of cap free
Amount of items: 2
Items: 
Size: 681491 Color: 0
Size: 318431 Color: 3

Bin 2640: 79 of cap free
Amount of items: 2
Items: 
Size: 712812 Color: 4
Size: 287110 Color: 1

Bin 2641: 79 of cap free
Amount of items: 2
Items: 
Size: 723044 Color: 1
Size: 276878 Color: 3

Bin 2642: 79 of cap free
Amount of items: 2
Items: 
Size: 765837 Color: 3
Size: 234085 Color: 4

Bin 2643: 79 of cap free
Amount of items: 2
Items: 
Size: 769427 Color: 4
Size: 230495 Color: 3

Bin 2644: 79 of cap free
Amount of items: 2
Items: 
Size: 562744 Color: 0
Size: 437178 Color: 4

Bin 2645: 79 of cap free
Amount of items: 2
Items: 
Size: 504204 Color: 1
Size: 495718 Color: 4

Bin 2646: 80 of cap free
Amount of items: 3
Items: 
Size: 628455 Color: 1
Size: 186050 Color: 4
Size: 185416 Color: 3

Bin 2647: 80 of cap free
Amount of items: 2
Items: 
Size: 558997 Color: 4
Size: 440924 Color: 3

Bin 2648: 80 of cap free
Amount of items: 2
Items: 
Size: 512933 Color: 0
Size: 486988 Color: 3

Bin 2649: 80 of cap free
Amount of items: 2
Items: 
Size: 524920 Color: 2
Size: 475001 Color: 4

Bin 2650: 80 of cap free
Amount of items: 2
Items: 
Size: 632921 Color: 1
Size: 367000 Color: 2

Bin 2651: 80 of cap free
Amount of items: 2
Items: 
Size: 649530 Color: 1
Size: 350391 Color: 4

Bin 2652: 80 of cap free
Amount of items: 2
Items: 
Size: 628684 Color: 3
Size: 371237 Color: 2

Bin 2653: 81 of cap free
Amount of items: 2
Items: 
Size: 619081 Color: 3
Size: 380839 Color: 4

Bin 2654: 81 of cap free
Amount of items: 2
Items: 
Size: 624996 Color: 3
Size: 374924 Color: 1

Bin 2655: 81 of cap free
Amount of items: 2
Items: 
Size: 505346 Color: 2
Size: 494574 Color: 0

Bin 2656: 81 of cap free
Amount of items: 2
Items: 
Size: 794158 Color: 2
Size: 205762 Color: 4

Bin 2657: 81 of cap free
Amount of items: 2
Items: 
Size: 695242 Color: 4
Size: 304678 Color: 0

Bin 2658: 82 of cap free
Amount of items: 2
Items: 
Size: 666225 Color: 3
Size: 333694 Color: 1

Bin 2659: 82 of cap free
Amount of items: 2
Items: 
Size: 657073 Color: 4
Size: 342846 Color: 2

Bin 2660: 82 of cap free
Amount of items: 2
Items: 
Size: 662509 Color: 3
Size: 337410 Color: 1

Bin 2661: 82 of cap free
Amount of items: 2
Items: 
Size: 678107 Color: 2
Size: 321812 Color: 1

Bin 2662: 82 of cap free
Amount of items: 2
Items: 
Size: 685112 Color: 4
Size: 314807 Color: 3

Bin 2663: 82 of cap free
Amount of items: 2
Items: 
Size: 759886 Color: 4
Size: 240033 Color: 1

Bin 2664: 82 of cap free
Amount of items: 2
Items: 
Size: 785149 Color: 1
Size: 214770 Color: 0

Bin 2665: 82 of cap free
Amount of items: 2
Items: 
Size: 644177 Color: 2
Size: 355742 Color: 3

Bin 2666: 82 of cap free
Amount of items: 2
Items: 
Size: 509226 Color: 3
Size: 490693 Color: 2

Bin 2667: 82 of cap free
Amount of items: 2
Items: 
Size: 527535 Color: 1
Size: 472384 Color: 2

Bin 2668: 83 of cap free
Amount of items: 2
Items: 
Size: 681047 Color: 4
Size: 318871 Color: 1

Bin 2669: 83 of cap free
Amount of items: 2
Items: 
Size: 758973 Color: 0
Size: 240945 Color: 1

Bin 2670: 83 of cap free
Amount of items: 2
Items: 
Size: 535280 Color: 1
Size: 464638 Color: 4

Bin 2671: 83 of cap free
Amount of items: 2
Items: 
Size: 542329 Color: 4
Size: 457589 Color: 1

Bin 2672: 83 of cap free
Amount of items: 2
Items: 
Size: 553236 Color: 0
Size: 446682 Color: 2

Bin 2673: 83 of cap free
Amount of items: 2
Items: 
Size: 790942 Color: 4
Size: 208976 Color: 3

Bin 2674: 83 of cap free
Amount of items: 2
Items: 
Size: 713464 Color: 1
Size: 286454 Color: 0

Bin 2675: 84 of cap free
Amount of items: 2
Items: 
Size: 528504 Color: 0
Size: 471413 Color: 2

Bin 2676: 84 of cap free
Amount of items: 2
Items: 
Size: 700336 Color: 3
Size: 299581 Color: 4

Bin 2677: 84 of cap free
Amount of items: 2
Items: 
Size: 782120 Color: 0
Size: 217797 Color: 3

Bin 2678: 84 of cap free
Amount of items: 2
Items: 
Size: 658341 Color: 0
Size: 341576 Color: 1

Bin 2679: 84 of cap free
Amount of items: 2
Items: 
Size: 521698 Color: 0
Size: 478219 Color: 3

Bin 2680: 85 of cap free
Amount of items: 2
Items: 
Size: 656890 Color: 3
Size: 343026 Color: 4

Bin 2681: 85 of cap free
Amount of items: 2
Items: 
Size: 523388 Color: 1
Size: 476528 Color: 2

Bin 2682: 85 of cap free
Amount of items: 2
Items: 
Size: 591424 Color: 2
Size: 408492 Color: 4

Bin 2683: 85 of cap free
Amount of items: 2
Items: 
Size: 597003 Color: 2
Size: 402913 Color: 1

Bin 2684: 85 of cap free
Amount of items: 2
Items: 
Size: 721473 Color: 3
Size: 278443 Color: 2

Bin 2685: 85 of cap free
Amount of items: 3
Items: 
Size: 616917 Color: 1
Size: 191523 Color: 4
Size: 191476 Color: 1

Bin 2686: 85 of cap free
Amount of items: 2
Items: 
Size: 739096 Color: 3
Size: 260820 Color: 2

Bin 2687: 86 of cap free
Amount of items: 2
Items: 
Size: 733383 Color: 4
Size: 266532 Color: 1

Bin 2688: 86 of cap free
Amount of items: 2
Items: 
Size: 688690 Color: 0
Size: 311225 Color: 1

Bin 2689: 86 of cap free
Amount of items: 2
Items: 
Size: 517716 Color: 4
Size: 482199 Color: 2

Bin 2690: 86 of cap free
Amount of items: 2
Items: 
Size: 534751 Color: 2
Size: 465164 Color: 3

Bin 2691: 86 of cap free
Amount of items: 2
Items: 
Size: 688297 Color: 4
Size: 311618 Color: 2

Bin 2692: 86 of cap free
Amount of items: 2
Items: 
Size: 768143 Color: 0
Size: 231772 Color: 1

Bin 2693: 86 of cap free
Amount of items: 2
Items: 
Size: 768251 Color: 3
Size: 231664 Color: 4

Bin 2694: 86 of cap free
Amount of items: 2
Items: 
Size: 784978 Color: 4
Size: 214937 Color: 2

Bin 2695: 86 of cap free
Amount of items: 2
Items: 
Size: 531962 Color: 1
Size: 467953 Color: 3

Bin 2696: 86 of cap free
Amount of items: 2
Items: 
Size: 733038 Color: 4
Size: 266877 Color: 1

Bin 2697: 86 of cap free
Amount of items: 2
Items: 
Size: 513507 Color: 4
Size: 486408 Color: 2

Bin 2698: 87 of cap free
Amount of items: 2
Items: 
Size: 574691 Color: 3
Size: 425223 Color: 4

Bin 2699: 87 of cap free
Amount of items: 2
Items: 
Size: 534540 Color: 3
Size: 465374 Color: 0

Bin 2700: 87 of cap free
Amount of items: 2
Items: 
Size: 683993 Color: 2
Size: 315921 Color: 0

Bin 2701: 87 of cap free
Amount of items: 2
Items: 
Size: 591930 Color: 3
Size: 407984 Color: 1

Bin 2702: 87 of cap free
Amount of items: 2
Items: 
Size: 607376 Color: 4
Size: 392538 Color: 1

Bin 2703: 87 of cap free
Amount of items: 2
Items: 
Size: 642096 Color: 2
Size: 357818 Color: 0

Bin 2704: 87 of cap free
Amount of items: 2
Items: 
Size: 678561 Color: 2
Size: 321353 Color: 4

Bin 2705: 87 of cap free
Amount of items: 2
Items: 
Size: 730138 Color: 0
Size: 269776 Color: 2

Bin 2706: 87 of cap free
Amount of items: 2
Items: 
Size: 799439 Color: 0
Size: 200475 Color: 4

Bin 2707: 87 of cap free
Amount of items: 2
Items: 
Size: 799327 Color: 0
Size: 200587 Color: 4

Bin 2708: 87 of cap free
Amount of items: 2
Items: 
Size: 615834 Color: 4
Size: 384080 Color: 3

Bin 2709: 87 of cap free
Amount of items: 2
Items: 
Size: 693629 Color: 1
Size: 306285 Color: 2

Bin 2710: 87 of cap free
Amount of items: 2
Items: 
Size: 689568 Color: 2
Size: 310346 Color: 1

Bin 2711: 88 of cap free
Amount of items: 3
Items: 
Size: 604042 Color: 0
Size: 198417 Color: 4
Size: 197454 Color: 1

Bin 2712: 88 of cap free
Amount of items: 2
Items: 
Size: 504710 Color: 0
Size: 495203 Color: 3

Bin 2713: 88 of cap free
Amount of items: 2
Items: 
Size: 782931 Color: 1
Size: 216982 Color: 4

Bin 2714: 88 of cap free
Amount of items: 2
Items: 
Size: 517297 Color: 1
Size: 482616 Color: 3

Bin 2715: 88 of cap free
Amount of items: 2
Items: 
Size: 650536 Color: 3
Size: 349377 Color: 4

Bin 2716: 88 of cap free
Amount of items: 2
Items: 
Size: 726526 Color: 3
Size: 273387 Color: 4

Bin 2717: 88 of cap free
Amount of items: 2
Items: 
Size: 732024 Color: 3
Size: 267889 Color: 1

Bin 2718: 88 of cap free
Amount of items: 2
Items: 
Size: 757286 Color: 2
Size: 242627 Color: 0

Bin 2719: 88 of cap free
Amount of items: 2
Items: 
Size: 789797 Color: 3
Size: 210116 Color: 4

Bin 2720: 88 of cap free
Amount of items: 2
Items: 
Size: 530825 Color: 2
Size: 469088 Color: 1

Bin 2721: 88 of cap free
Amount of items: 2
Items: 
Size: 792111 Color: 0
Size: 207802 Color: 3

Bin 2722: 89 of cap free
Amount of items: 2
Items: 
Size: 763453 Color: 1
Size: 236459 Color: 2

Bin 2723: 89 of cap free
Amount of items: 2
Items: 
Size: 700319 Color: 4
Size: 299593 Color: 0

Bin 2724: 89 of cap free
Amount of items: 2
Items: 
Size: 548567 Color: 2
Size: 451345 Color: 0

Bin 2725: 89 of cap free
Amount of items: 2
Items: 
Size: 546066 Color: 0
Size: 453846 Color: 3

Bin 2726: 89 of cap free
Amount of items: 2
Items: 
Size: 709477 Color: 2
Size: 290435 Color: 4

Bin 2727: 89 of cap free
Amount of items: 2
Items: 
Size: 749971 Color: 1
Size: 249941 Color: 3

Bin 2728: 89 of cap free
Amount of items: 2
Items: 
Size: 775045 Color: 3
Size: 224867 Color: 0

Bin 2729: 89 of cap free
Amount of items: 2
Items: 
Size: 636340 Color: 1
Size: 363572 Color: 0

Bin 2730: 89 of cap free
Amount of items: 2
Items: 
Size: 568035 Color: 2
Size: 431877 Color: 0

Bin 2731: 89 of cap free
Amount of items: 2
Items: 
Size: 790665 Color: 1
Size: 209247 Color: 2

Bin 2732: 89 of cap free
Amount of items: 2
Items: 
Size: 777392 Color: 0
Size: 222520 Color: 4

Bin 2733: 89 of cap free
Amount of items: 2
Items: 
Size: 693160 Color: 0
Size: 306752 Color: 1

Bin 2734: 89 of cap free
Amount of items: 2
Items: 
Size: 579238 Color: 3
Size: 420674 Color: 4

Bin 2735: 89 of cap free
Amount of items: 2
Items: 
Size: 637518 Color: 2
Size: 362394 Color: 3

Bin 2736: 89 of cap free
Amount of items: 2
Items: 
Size: 567519 Color: 0
Size: 432393 Color: 2

Bin 2737: 90 of cap free
Amount of items: 2
Items: 
Size: 766152 Color: 4
Size: 233759 Color: 3

Bin 2738: 90 of cap free
Amount of items: 2
Items: 
Size: 598658 Color: 3
Size: 401253 Color: 0

Bin 2739: 90 of cap free
Amount of items: 2
Items: 
Size: 504820 Color: 4
Size: 495091 Color: 1

Bin 2740: 90 of cap free
Amount of items: 2
Items: 
Size: 622752 Color: 3
Size: 377159 Color: 2

Bin 2741: 90 of cap free
Amount of items: 2
Items: 
Size: 687656 Color: 0
Size: 312255 Color: 3

Bin 2742: 90 of cap free
Amount of items: 3
Items: 
Size: 618805 Color: 3
Size: 190553 Color: 1
Size: 190553 Color: 0

Bin 2743: 90 of cap free
Amount of items: 2
Items: 
Size: 638891 Color: 1
Size: 361020 Color: 2

Bin 2744: 90 of cap free
Amount of items: 2
Items: 
Size: 674932 Color: 2
Size: 324979 Color: 1

Bin 2745: 90 of cap free
Amount of items: 2
Items: 
Size: 574824 Color: 0
Size: 425087 Color: 4

Bin 2746: 90 of cap free
Amount of items: 2
Items: 
Size: 778278 Color: 2
Size: 221633 Color: 1

Bin 2747: 90 of cap free
Amount of items: 2
Items: 
Size: 594299 Color: 1
Size: 405612 Color: 4

Bin 2748: 91 of cap free
Amount of items: 2
Items: 
Size: 500771 Color: 3
Size: 499139 Color: 4

Bin 2749: 91 of cap free
Amount of items: 2
Items: 
Size: 734933 Color: 0
Size: 264977 Color: 4

Bin 2750: 91 of cap free
Amount of items: 3
Items: 
Size: 606622 Color: 2
Size: 196691 Color: 4
Size: 196597 Color: 2

Bin 2751: 91 of cap free
Amount of items: 2
Items: 
Size: 795843 Color: 2
Size: 204067 Color: 0

Bin 2752: 92 of cap free
Amount of items: 2
Items: 
Size: 501952 Color: 1
Size: 497957 Color: 0

Bin 2753: 92 of cap free
Amount of items: 2
Items: 
Size: 539913 Color: 2
Size: 459996 Color: 0

Bin 2754: 92 of cap free
Amount of items: 2
Items: 
Size: 594057 Color: 1
Size: 405852 Color: 0

Bin 2755: 92 of cap free
Amount of items: 2
Items: 
Size: 605533 Color: 4
Size: 394376 Color: 1

Bin 2756: 92 of cap free
Amount of items: 2
Items: 
Size: 614155 Color: 4
Size: 385754 Color: 2

Bin 2757: 92 of cap free
Amount of items: 2
Items: 
Size: 702378 Color: 0
Size: 297531 Color: 3

Bin 2758: 92 of cap free
Amount of items: 2
Items: 
Size: 704789 Color: 1
Size: 295120 Color: 0

Bin 2759: 92 of cap free
Amount of items: 2
Items: 
Size: 741951 Color: 3
Size: 257958 Color: 2

Bin 2760: 92 of cap free
Amount of items: 2
Items: 
Size: 742058 Color: 3
Size: 257851 Color: 0

Bin 2761: 92 of cap free
Amount of items: 2
Items: 
Size: 761678 Color: 3
Size: 238231 Color: 2

Bin 2762: 92 of cap free
Amount of items: 2
Items: 
Size: 724320 Color: 3
Size: 275589 Color: 2

Bin 2763: 92 of cap free
Amount of items: 2
Items: 
Size: 596813 Color: 2
Size: 403096 Color: 4

Bin 2764: 92 of cap free
Amount of items: 2
Items: 
Size: 794552 Color: 1
Size: 205357 Color: 2

Bin 2765: 92 of cap free
Amount of items: 2
Items: 
Size: 515240 Color: 3
Size: 484669 Color: 0

Bin 2766: 93 of cap free
Amount of items: 2
Items: 
Size: 504172 Color: 0
Size: 495736 Color: 1

Bin 2767: 93 of cap free
Amount of items: 2
Items: 
Size: 522079 Color: 2
Size: 477829 Color: 3

Bin 2768: 93 of cap free
Amount of items: 2
Items: 
Size: 586416 Color: 0
Size: 413492 Color: 4

Bin 2769: 93 of cap free
Amount of items: 2
Items: 
Size: 714420 Color: 4
Size: 285488 Color: 2

Bin 2770: 93 of cap free
Amount of items: 2
Items: 
Size: 719044 Color: 4
Size: 280864 Color: 2

Bin 2771: 93 of cap free
Amount of items: 2
Items: 
Size: 769231 Color: 3
Size: 230677 Color: 0

Bin 2772: 93 of cap free
Amount of items: 2
Items: 
Size: 791236 Color: 0
Size: 208672 Color: 1

Bin 2773: 93 of cap free
Amount of items: 2
Items: 
Size: 729707 Color: 3
Size: 270201 Color: 2

Bin 2774: 93 of cap free
Amount of items: 2
Items: 
Size: 537867 Color: 2
Size: 462041 Color: 3

Bin 2775: 94 of cap free
Amount of items: 2
Items: 
Size: 585401 Color: 0
Size: 414506 Color: 2

Bin 2776: 94 of cap free
Amount of items: 2
Items: 
Size: 752801 Color: 3
Size: 247106 Color: 4

Bin 2777: 94 of cap free
Amount of items: 2
Items: 
Size: 635197 Color: 1
Size: 364710 Color: 2

Bin 2778: 94 of cap free
Amount of items: 2
Items: 
Size: 578908 Color: 3
Size: 420999 Color: 1

Bin 2779: 94 of cap free
Amount of items: 2
Items: 
Size: 536889 Color: 1
Size: 463018 Color: 4

Bin 2780: 95 of cap free
Amount of items: 2
Items: 
Size: 796884 Color: 4
Size: 203022 Color: 0

Bin 2781: 95 of cap free
Amount of items: 2
Items: 
Size: 505668 Color: 3
Size: 494238 Color: 2

Bin 2782: 95 of cap free
Amount of items: 2
Items: 
Size: 523263 Color: 2
Size: 476643 Color: 0

Bin 2783: 95 of cap free
Amount of items: 2
Items: 
Size: 761677 Color: 3
Size: 238229 Color: 0

Bin 2784: 95 of cap free
Amount of items: 2
Items: 
Size: 676264 Color: 2
Size: 323642 Color: 1

Bin 2785: 95 of cap free
Amount of items: 2
Items: 
Size: 720706 Color: 4
Size: 279200 Color: 2

Bin 2786: 96 of cap free
Amount of items: 2
Items: 
Size: 556123 Color: 4
Size: 443782 Color: 1

Bin 2787: 96 of cap free
Amount of items: 2
Items: 
Size: 584581 Color: 0
Size: 415324 Color: 4

Bin 2788: 96 of cap free
Amount of items: 2
Items: 
Size: 680054 Color: 2
Size: 319851 Color: 0

Bin 2789: 96 of cap free
Amount of items: 2
Items: 
Size: 663155 Color: 4
Size: 336750 Color: 1

Bin 2790: 96 of cap free
Amount of items: 2
Items: 
Size: 697246 Color: 0
Size: 302659 Color: 1

Bin 2791: 96 of cap free
Amount of items: 2
Items: 
Size: 679574 Color: 4
Size: 320331 Color: 1

Bin 2792: 96 of cap free
Amount of items: 2
Items: 
Size: 739890 Color: 2
Size: 260015 Color: 1

Bin 2793: 97 of cap free
Amount of items: 2
Items: 
Size: 507769 Color: 2
Size: 492135 Color: 1

Bin 2794: 97 of cap free
Amount of items: 2
Items: 
Size: 513738 Color: 3
Size: 486166 Color: 2

Bin 2795: 97 of cap free
Amount of items: 2
Items: 
Size: 589981 Color: 2
Size: 409923 Color: 4

Bin 2796: 97 of cap free
Amount of items: 2
Items: 
Size: 618188 Color: 2
Size: 381716 Color: 3

Bin 2797: 97 of cap free
Amount of items: 2
Items: 
Size: 775700 Color: 4
Size: 224204 Color: 0

Bin 2798: 97 of cap free
Amount of items: 2
Items: 
Size: 788911 Color: 4
Size: 210993 Color: 2

Bin 2799: 97 of cap free
Amount of items: 2
Items: 
Size: 791877 Color: 1
Size: 208027 Color: 3

Bin 2800: 97 of cap free
Amount of items: 2
Items: 
Size: 658084 Color: 2
Size: 341820 Color: 0

Bin 2801: 98 of cap free
Amount of items: 2
Items: 
Size: 641576 Color: 0
Size: 358327 Color: 4

Bin 2802: 98 of cap free
Amount of items: 2
Items: 
Size: 793139 Color: 4
Size: 206764 Color: 1

Bin 2803: 98 of cap free
Amount of items: 2
Items: 
Size: 511807 Color: 4
Size: 488096 Color: 2

Bin 2804: 98 of cap free
Amount of items: 2
Items: 
Size: 515340 Color: 3
Size: 484563 Color: 0

Bin 2805: 98 of cap free
Amount of items: 2
Items: 
Size: 520012 Color: 3
Size: 479891 Color: 1

Bin 2806: 98 of cap free
Amount of items: 3
Items: 
Size: 530085 Color: 0
Size: 235883 Color: 0
Size: 233935 Color: 4

Bin 2807: 98 of cap free
Amount of items: 2
Items: 
Size: 670682 Color: 4
Size: 329221 Color: 1

Bin 2808: 98 of cap free
Amount of items: 2
Items: 
Size: 656760 Color: 0
Size: 343143 Color: 4

Bin 2809: 98 of cap free
Amount of items: 2
Items: 
Size: 723449 Color: 4
Size: 276454 Color: 2

Bin 2810: 99 of cap free
Amount of items: 3
Items: 
Size: 713508 Color: 0
Size: 143265 Color: 4
Size: 143129 Color: 1

Bin 2811: 99 of cap free
Amount of items: 2
Items: 
Size: 528898 Color: 4
Size: 471004 Color: 1

Bin 2812: 99 of cap free
Amount of items: 2
Items: 
Size: 510672 Color: 2
Size: 489230 Color: 3

Bin 2813: 99 of cap free
Amount of items: 2
Items: 
Size: 552420 Color: 1
Size: 447482 Color: 3

Bin 2814: 99 of cap free
Amount of items: 2
Items: 
Size: 709949 Color: 0
Size: 289953 Color: 1

Bin 2815: 99 of cap free
Amount of items: 3
Items: 
Size: 617435 Color: 4
Size: 191335 Color: 2
Size: 191132 Color: 4

Bin 2816: 99 of cap free
Amount of items: 2
Items: 
Size: 567333 Color: 3
Size: 432569 Color: 2

Bin 2817: 99 of cap free
Amount of items: 2
Items: 
Size: 652891 Color: 2
Size: 347011 Color: 3

Bin 2818: 99 of cap free
Amount of items: 2
Items: 
Size: 733576 Color: 0
Size: 266326 Color: 3

Bin 2819: 99 of cap free
Amount of items: 2
Items: 
Size: 725938 Color: 2
Size: 273964 Color: 1

Bin 2820: 99 of cap free
Amount of items: 2
Items: 
Size: 721060 Color: 2
Size: 278842 Color: 1

Bin 2821: 100 of cap free
Amount of items: 2
Items: 
Size: 797471 Color: 4
Size: 202430 Color: 3

Bin 2822: 100 of cap free
Amount of items: 2
Items: 
Size: 667691 Color: 4
Size: 332210 Color: 3

Bin 2823: 100 of cap free
Amount of items: 2
Items: 
Size: 502655 Color: 4
Size: 497246 Color: 0

Bin 2824: 100 of cap free
Amount of items: 2
Items: 
Size: 766491 Color: 4
Size: 233410 Color: 0

Bin 2825: 100 of cap free
Amount of items: 2
Items: 
Size: 525911 Color: 0
Size: 473990 Color: 2

Bin 2826: 101 of cap free
Amount of items: 3
Items: 
Size: 622277 Color: 2
Size: 199222 Color: 3
Size: 178401 Color: 4

Bin 2827: 101 of cap free
Amount of items: 2
Items: 
Size: 512229 Color: 0
Size: 487671 Color: 2

Bin 2828: 101 of cap free
Amount of items: 2
Items: 
Size: 678244 Color: 3
Size: 321656 Color: 4

Bin 2829: 101 of cap free
Amount of items: 2
Items: 
Size: 555146 Color: 3
Size: 444754 Color: 4

Bin 2830: 101 of cap free
Amount of items: 2
Items: 
Size: 625129 Color: 3
Size: 374771 Color: 2

Bin 2831: 101 of cap free
Amount of items: 2
Items: 
Size: 658724 Color: 2
Size: 341176 Color: 0

Bin 2832: 101 of cap free
Amount of items: 2
Items: 
Size: 789606 Color: 1
Size: 210294 Color: 0

Bin 2833: 101 of cap free
Amount of items: 2
Items: 
Size: 615299 Color: 3
Size: 384601 Color: 1

Bin 2834: 101 of cap free
Amount of items: 2
Items: 
Size: 663260 Color: 2
Size: 336640 Color: 0

Bin 2835: 101 of cap free
Amount of items: 2
Items: 
Size: 613038 Color: 1
Size: 386862 Color: 2

Bin 2836: 102 of cap free
Amount of items: 3
Items: 
Size: 717286 Color: 0
Size: 141437 Color: 2
Size: 141176 Color: 0

Bin 2837: 102 of cap free
Amount of items: 2
Items: 
Size: 654078 Color: 1
Size: 345821 Color: 3

Bin 2838: 102 of cap free
Amount of items: 2
Items: 
Size: 534861 Color: 1
Size: 465038 Color: 2

Bin 2839: 102 of cap free
Amount of items: 2
Items: 
Size: 604792 Color: 2
Size: 395107 Color: 1

Bin 2840: 102 of cap free
Amount of items: 2
Items: 
Size: 768151 Color: 1
Size: 231748 Color: 3

Bin 2841: 102 of cap free
Amount of items: 2
Items: 
Size: 616808 Color: 0
Size: 383091 Color: 1

Bin 2842: 102 of cap free
Amount of items: 2
Items: 
Size: 793608 Color: 2
Size: 206291 Color: 3

Bin 2843: 102 of cap free
Amount of items: 2
Items: 
Size: 701772 Color: 1
Size: 298127 Color: 3

Bin 2844: 103 of cap free
Amount of items: 2
Items: 
Size: 789317 Color: 1
Size: 210581 Color: 0

Bin 2845: 103 of cap free
Amount of items: 2
Items: 
Size: 763477 Color: 2
Size: 236421 Color: 4

Bin 2846: 103 of cap free
Amount of items: 2
Items: 
Size: 667041 Color: 2
Size: 332857 Color: 1

Bin 2847: 103 of cap free
Amount of items: 2
Items: 
Size: 660863 Color: 2
Size: 339035 Color: 3

Bin 2848: 103 of cap free
Amount of items: 2
Items: 
Size: 672200 Color: 3
Size: 327698 Color: 1

Bin 2849: 103 of cap free
Amount of items: 2
Items: 
Size: 761861 Color: 4
Size: 238037 Color: 2

Bin 2850: 103 of cap free
Amount of items: 3
Items: 
Size: 622897 Color: 4
Size: 189397 Color: 2
Size: 187604 Color: 1

Bin 2851: 103 of cap free
Amount of items: 2
Items: 
Size: 620795 Color: 2
Size: 379103 Color: 4

Bin 2852: 104 of cap free
Amount of items: 3
Items: 
Size: 748876 Color: 3
Size: 145214 Color: 3
Size: 105807 Color: 0

Bin 2853: 104 of cap free
Amount of items: 2
Items: 
Size: 744198 Color: 2
Size: 255699 Color: 0

Bin 2854: 104 of cap free
Amount of items: 2
Items: 
Size: 552931 Color: 3
Size: 446966 Color: 0

Bin 2855: 104 of cap free
Amount of items: 2
Items: 
Size: 571108 Color: 4
Size: 428789 Color: 1

Bin 2856: 104 of cap free
Amount of items: 2
Items: 
Size: 572685 Color: 0
Size: 427212 Color: 3

Bin 2857: 104 of cap free
Amount of items: 2
Items: 
Size: 540584 Color: 4
Size: 459313 Color: 2

Bin 2858: 105 of cap free
Amount of items: 2
Items: 
Size: 540804 Color: 1
Size: 459092 Color: 0

Bin 2859: 105 of cap free
Amount of items: 2
Items: 
Size: 756964 Color: 2
Size: 242932 Color: 4

Bin 2860: 105 of cap free
Amount of items: 2
Items: 
Size: 519287 Color: 3
Size: 480609 Color: 2

Bin 2861: 105 of cap free
Amount of items: 2
Items: 
Size: 661006 Color: 2
Size: 338890 Color: 4

Bin 2862: 105 of cap free
Amount of items: 2
Items: 
Size: 788733 Color: 1
Size: 211163 Color: 3

Bin 2863: 105 of cap free
Amount of items: 2
Items: 
Size: 564077 Color: 4
Size: 435819 Color: 1

Bin 2864: 105 of cap free
Amount of items: 2
Items: 
Size: 763013 Color: 1
Size: 236883 Color: 0

Bin 2865: 105 of cap free
Amount of items: 2
Items: 
Size: 739382 Color: 0
Size: 260514 Color: 2

Bin 2866: 106 of cap free
Amount of items: 2
Items: 
Size: 749037 Color: 2
Size: 250858 Color: 3

Bin 2867: 106 of cap free
Amount of items: 2
Items: 
Size: 556205 Color: 1
Size: 443690 Color: 2

Bin 2868: 106 of cap free
Amount of items: 2
Items: 
Size: 577126 Color: 1
Size: 422769 Color: 2

Bin 2869: 106 of cap free
Amount of items: 2
Items: 
Size: 622752 Color: 4
Size: 377143 Color: 1

Bin 2870: 106 of cap free
Amount of items: 2
Items: 
Size: 638147 Color: 2
Size: 361748 Color: 3

Bin 2871: 106 of cap free
Amount of items: 2
Items: 
Size: 647804 Color: 4
Size: 352091 Color: 2

Bin 2872: 106 of cap free
Amount of items: 2
Items: 
Size: 679021 Color: 1
Size: 320874 Color: 2

Bin 2873: 106 of cap free
Amount of items: 2
Items: 
Size: 754584 Color: 3
Size: 245311 Color: 0

Bin 2874: 106 of cap free
Amount of items: 2
Items: 
Size: 525011 Color: 1
Size: 474884 Color: 0

Bin 2875: 106 of cap free
Amount of items: 2
Items: 
Size: 516275 Color: 3
Size: 483620 Color: 1

Bin 2876: 106 of cap free
Amount of items: 2
Items: 
Size: 761311 Color: 1
Size: 238584 Color: 2

Bin 2877: 106 of cap free
Amount of items: 2
Items: 
Size: 729557 Color: 4
Size: 270338 Color: 1

Bin 2878: 107 of cap free
Amount of items: 2
Items: 
Size: 708458 Color: 1
Size: 291436 Color: 3

Bin 2879: 107 of cap free
Amount of items: 3
Items: 
Size: 689167 Color: 0
Size: 156182 Color: 2
Size: 154545 Color: 3

Bin 2880: 107 of cap free
Amount of items: 2
Items: 
Size: 795152 Color: 1
Size: 204742 Color: 2

Bin 2881: 107 of cap free
Amount of items: 2
Items: 
Size: 527286 Color: 4
Size: 472608 Color: 2

Bin 2882: 107 of cap free
Amount of items: 2
Items: 
Size: 566777 Color: 1
Size: 433117 Color: 2

Bin 2883: 107 of cap free
Amount of items: 2
Items: 
Size: 648180 Color: 0
Size: 351714 Color: 1

Bin 2884: 107 of cap free
Amount of items: 2
Items: 
Size: 671093 Color: 3
Size: 328801 Color: 1

Bin 2885: 107 of cap free
Amount of items: 2
Items: 
Size: 542674 Color: 1
Size: 457220 Color: 3

Bin 2886: 107 of cap free
Amount of items: 2
Items: 
Size: 682182 Color: 0
Size: 317712 Color: 1

Bin 2887: 108 of cap free
Amount of items: 2
Items: 
Size: 718354 Color: 1
Size: 281539 Color: 3

Bin 2888: 108 of cap free
Amount of items: 2
Items: 
Size: 707722 Color: 0
Size: 292171 Color: 1

Bin 2889: 109 of cap free
Amount of items: 2
Items: 
Size: 661127 Color: 3
Size: 338765 Color: 0

Bin 2890: 109 of cap free
Amount of items: 2
Items: 
Size: 594790 Color: 0
Size: 405102 Color: 2

Bin 2891: 109 of cap free
Amount of items: 2
Items: 
Size: 734853 Color: 2
Size: 265039 Color: 0

Bin 2892: 109 of cap free
Amount of items: 2
Items: 
Size: 610272 Color: 4
Size: 389620 Color: 0

Bin 2893: 110 of cap free
Amount of items: 2
Items: 
Size: 757107 Color: 4
Size: 242784 Color: 1

Bin 2894: 110 of cap free
Amount of items: 2
Items: 
Size: 671443 Color: 2
Size: 328448 Color: 4

Bin 2895: 110 of cap free
Amount of items: 2
Items: 
Size: 564861 Color: 1
Size: 435030 Color: 0

Bin 2896: 110 of cap free
Amount of items: 2
Items: 
Size: 641782 Color: 3
Size: 358109 Color: 4

Bin 2897: 110 of cap free
Amount of items: 2
Items: 
Size: 782112 Color: 4
Size: 217779 Color: 1

Bin 2898: 110 of cap free
Amount of items: 2
Items: 
Size: 774893 Color: 3
Size: 224998 Color: 1

Bin 2899: 111 of cap free
Amount of items: 2
Items: 
Size: 645985 Color: 3
Size: 353905 Color: 1

Bin 2900: 111 of cap free
Amount of items: 2
Items: 
Size: 633562 Color: 0
Size: 366328 Color: 4

Bin 2901: 111 of cap free
Amount of items: 2
Items: 
Size: 520388 Color: 1
Size: 479502 Color: 3

Bin 2902: 111 of cap free
Amount of items: 2
Items: 
Size: 575077 Color: 3
Size: 424813 Color: 2

Bin 2903: 111 of cap free
Amount of items: 2
Items: 
Size: 655230 Color: 2
Size: 344660 Color: 0

Bin 2904: 111 of cap free
Amount of items: 2
Items: 
Size: 684530 Color: 1
Size: 315360 Color: 0

Bin 2905: 111 of cap free
Amount of items: 2
Items: 
Size: 737933 Color: 3
Size: 261957 Color: 4

Bin 2906: 111 of cap free
Amount of items: 2
Items: 
Size: 699575 Color: 4
Size: 300315 Color: 0

Bin 2907: 111 of cap free
Amount of items: 2
Items: 
Size: 539471 Color: 4
Size: 460419 Color: 0

Bin 2908: 111 of cap free
Amount of items: 2
Items: 
Size: 699513 Color: 2
Size: 300377 Color: 4

Bin 2909: 112 of cap free
Amount of items: 2
Items: 
Size: 647210 Color: 0
Size: 352679 Color: 2

Bin 2910: 112 of cap free
Amount of items: 2
Items: 
Size: 534515 Color: 0
Size: 465374 Color: 3

Bin 2911: 112 of cap free
Amount of items: 2
Items: 
Size: 790650 Color: 2
Size: 209239 Color: 1

Bin 2912: 113 of cap free
Amount of items: 2
Items: 
Size: 644681 Color: 4
Size: 355207 Color: 2

Bin 2913: 113 of cap free
Amount of items: 2
Items: 
Size: 741432 Color: 0
Size: 258456 Color: 4

Bin 2914: 113 of cap free
Amount of items: 2
Items: 
Size: 736018 Color: 3
Size: 263870 Color: 4

Bin 2915: 113 of cap free
Amount of items: 2
Items: 
Size: 533772 Color: 0
Size: 466116 Color: 4

Bin 2916: 113 of cap free
Amount of items: 2
Items: 
Size: 672026 Color: 2
Size: 327862 Color: 1

Bin 2917: 113 of cap free
Amount of items: 2
Items: 
Size: 678658 Color: 1
Size: 321230 Color: 2

Bin 2918: 113 of cap free
Amount of items: 2
Items: 
Size: 567015 Color: 0
Size: 432873 Color: 2

Bin 2919: 113 of cap free
Amount of items: 2
Items: 
Size: 578639 Color: 0
Size: 421249 Color: 4

Bin 2920: 113 of cap free
Amount of items: 2
Items: 
Size: 605276 Color: 1
Size: 394612 Color: 0

Bin 2921: 113 of cap free
Amount of items: 2
Items: 
Size: 506291 Color: 1
Size: 493597 Color: 3

Bin 2922: 113 of cap free
Amount of items: 2
Items: 
Size: 646307 Color: 0
Size: 353581 Color: 4

Bin 2923: 113 of cap free
Amount of items: 2
Items: 
Size: 657043 Color: 1
Size: 342845 Color: 4

Bin 2924: 113 of cap free
Amount of items: 2
Items: 
Size: 593684 Color: 4
Size: 406204 Color: 0

Bin 2925: 113 of cap free
Amount of items: 2
Items: 
Size: 549518 Color: 1
Size: 450370 Color: 0

Bin 2926: 114 of cap free
Amount of items: 2
Items: 
Size: 717645 Color: 3
Size: 282242 Color: 0

Bin 2927: 114 of cap free
Amount of items: 2
Items: 
Size: 595357 Color: 3
Size: 404530 Color: 0

Bin 2928: 114 of cap free
Amount of items: 2
Items: 
Size: 648599 Color: 1
Size: 351288 Color: 2

Bin 2929: 114 of cap free
Amount of items: 2
Items: 
Size: 663570 Color: 2
Size: 336317 Color: 4

Bin 2930: 114 of cap free
Amount of items: 2
Items: 
Size: 571873 Color: 3
Size: 428014 Color: 1

Bin 2931: 115 of cap free
Amount of items: 2
Items: 
Size: 776080 Color: 1
Size: 223806 Color: 4

Bin 2932: 115 of cap free
Amount of items: 2
Items: 
Size: 747783 Color: 0
Size: 252103 Color: 4

Bin 2933: 115 of cap free
Amount of items: 2
Items: 
Size: 502888 Color: 4
Size: 496998 Color: 0

Bin 2934: 115 of cap free
Amount of items: 2
Items: 
Size: 738728 Color: 4
Size: 261158 Color: 3

Bin 2935: 115 of cap free
Amount of items: 2
Items: 
Size: 773679 Color: 4
Size: 226207 Color: 2

Bin 2936: 115 of cap free
Amount of items: 2
Items: 
Size: 722446 Color: 1
Size: 277440 Color: 0

Bin 2937: 115 of cap free
Amount of items: 2
Items: 
Size: 672411 Color: 3
Size: 327475 Color: 1

Bin 2938: 115 of cap free
Amount of items: 2
Items: 
Size: 612315 Color: 4
Size: 387571 Color: 0

Bin 2939: 115 of cap free
Amount of items: 2
Items: 
Size: 563146 Color: 2
Size: 436740 Color: 0

Bin 2940: 115 of cap free
Amount of items: 2
Items: 
Size: 767523 Color: 3
Size: 232363 Color: 0

Bin 2941: 116 of cap free
Amount of items: 2
Items: 
Size: 560222 Color: 1
Size: 439663 Color: 4

Bin 2942: 116 of cap free
Amount of items: 2
Items: 
Size: 620364 Color: 1
Size: 379521 Color: 3

Bin 2943: 116 of cap free
Amount of items: 2
Items: 
Size: 736923 Color: 0
Size: 262962 Color: 3

Bin 2944: 116 of cap free
Amount of items: 2
Items: 
Size: 555303 Color: 1
Size: 444582 Color: 2

Bin 2945: 116 of cap free
Amount of items: 2
Items: 
Size: 771981 Color: 0
Size: 227904 Color: 2

Bin 2946: 117 of cap free
Amount of items: 2
Items: 
Size: 790822 Color: 3
Size: 209062 Color: 4

Bin 2947: 117 of cap free
Amount of items: 2
Items: 
Size: 582560 Color: 3
Size: 417324 Color: 2

Bin 2948: 117 of cap free
Amount of items: 2
Items: 
Size: 590568 Color: 1
Size: 409316 Color: 4

Bin 2949: 117 of cap free
Amount of items: 2
Items: 
Size: 633235 Color: 2
Size: 366649 Color: 1

Bin 2950: 117 of cap free
Amount of items: 2
Items: 
Size: 653482 Color: 2
Size: 346402 Color: 4

Bin 2951: 117 of cap free
Amount of items: 2
Items: 
Size: 710567 Color: 4
Size: 289317 Color: 3

Bin 2952: 117 of cap free
Amount of items: 2
Items: 
Size: 763182 Color: 4
Size: 236702 Color: 1

Bin 2953: 117 of cap free
Amount of items: 2
Items: 
Size: 667477 Color: 3
Size: 332407 Color: 0

Bin 2954: 118 of cap free
Amount of items: 3
Items: 
Size: 741138 Color: 3
Size: 130158 Color: 3
Size: 128587 Color: 1

Bin 2955: 118 of cap free
Amount of items: 2
Items: 
Size: 783421 Color: 0
Size: 216462 Color: 2

Bin 2956: 118 of cap free
Amount of items: 2
Items: 
Size: 500997 Color: 3
Size: 498886 Color: 2

Bin 2957: 118 of cap free
Amount of items: 2
Items: 
Size: 550286 Color: 3
Size: 449597 Color: 2

Bin 2958: 118 of cap free
Amount of items: 2
Items: 
Size: 697041 Color: 4
Size: 302842 Color: 3

Bin 2959: 118 of cap free
Amount of items: 2
Items: 
Size: 549168 Color: 1
Size: 450715 Color: 3

Bin 2960: 118 of cap free
Amount of items: 2
Items: 
Size: 637972 Color: 0
Size: 361911 Color: 2

Bin 2961: 119 of cap free
Amount of items: 2
Items: 
Size: 663137 Color: 3
Size: 336745 Color: 0

Bin 2962: 119 of cap free
Amount of items: 2
Items: 
Size: 550892 Color: 1
Size: 448990 Color: 4

Bin 2963: 119 of cap free
Amount of items: 2
Items: 
Size: 576149 Color: 2
Size: 423733 Color: 0

Bin 2964: 119 of cap free
Amount of items: 2
Items: 
Size: 631494 Color: 4
Size: 368388 Color: 1

Bin 2965: 119 of cap free
Amount of items: 2
Items: 
Size: 706193 Color: 1
Size: 293689 Color: 4

Bin 2966: 119 of cap free
Amount of items: 2
Items: 
Size: 552937 Color: 0
Size: 446945 Color: 2

Bin 2967: 119 of cap free
Amount of items: 2
Items: 
Size: 504799 Color: 0
Size: 495083 Color: 4

Bin 2968: 119 of cap free
Amount of items: 2
Items: 
Size: 627015 Color: 2
Size: 372867 Color: 3

Bin 2969: 119 of cap free
Amount of items: 2
Items: 
Size: 557719 Color: 2
Size: 442163 Color: 3

Bin 2970: 119 of cap free
Amount of items: 2
Items: 
Size: 784434 Color: 2
Size: 215448 Color: 1

Bin 2971: 119 of cap free
Amount of items: 2
Items: 
Size: 600793 Color: 4
Size: 399089 Color: 1

Bin 2972: 120 of cap free
Amount of items: 2
Items: 
Size: 715916 Color: 4
Size: 283965 Color: 0

Bin 2973: 120 of cap free
Amount of items: 2
Items: 
Size: 683218 Color: 1
Size: 316663 Color: 3

Bin 2974: 120 of cap free
Amount of items: 2
Items: 
Size: 781077 Color: 2
Size: 218804 Color: 3

Bin 2975: 120 of cap free
Amount of items: 2
Items: 
Size: 562383 Color: 1
Size: 437498 Color: 0

Bin 2976: 121 of cap free
Amount of items: 3
Items: 
Size: 611051 Color: 0
Size: 194956 Color: 4
Size: 193873 Color: 4

Bin 2977: 121 of cap free
Amount of items: 2
Items: 
Size: 629901 Color: 2
Size: 369979 Color: 3

Bin 2978: 121 of cap free
Amount of items: 2
Items: 
Size: 632136 Color: 3
Size: 367744 Color: 0

Bin 2979: 121 of cap free
Amount of items: 2
Items: 
Size: 645309 Color: 3
Size: 354571 Color: 1

Bin 2980: 121 of cap free
Amount of items: 2
Items: 
Size: 743321 Color: 2
Size: 256559 Color: 0

Bin 2981: 121 of cap free
Amount of items: 2
Items: 
Size: 705205 Color: 1
Size: 294675 Color: 0

Bin 2982: 122 of cap free
Amount of items: 2
Items: 
Size: 533164 Color: 2
Size: 466715 Color: 0

Bin 2983: 122 of cap free
Amount of items: 2
Items: 
Size: 552555 Color: 2
Size: 447324 Color: 4

Bin 2984: 122 of cap free
Amount of items: 2
Items: 
Size: 598460 Color: 3
Size: 401419 Color: 2

Bin 2985: 122 of cap free
Amount of items: 2
Items: 
Size: 755907 Color: 4
Size: 243972 Color: 1

Bin 2986: 122 of cap free
Amount of items: 2
Items: 
Size: 589518 Color: 4
Size: 410361 Color: 0

Bin 2987: 122 of cap free
Amount of items: 2
Items: 
Size: 660251 Color: 2
Size: 339628 Color: 3

Bin 2988: 122 of cap free
Amount of items: 2
Items: 
Size: 752074 Color: 4
Size: 247805 Color: 0

Bin 2989: 123 of cap free
Amount of items: 3
Items: 
Size: 676412 Color: 3
Size: 162057 Color: 0
Size: 161409 Color: 1

Bin 2990: 123 of cap free
Amount of items: 2
Items: 
Size: 765810 Color: 2
Size: 234068 Color: 4

Bin 2991: 123 of cap free
Amount of items: 2
Items: 
Size: 697756 Color: 1
Size: 302122 Color: 0

Bin 2992: 123 of cap free
Amount of items: 2
Items: 
Size: 734631 Color: 4
Size: 265247 Color: 0

Bin 2993: 123 of cap free
Amount of items: 2
Items: 
Size: 572015 Color: 4
Size: 427863 Color: 0

Bin 2994: 123 of cap free
Amount of items: 2
Items: 
Size: 585928 Color: 4
Size: 413950 Color: 2

Bin 2995: 123 of cap free
Amount of items: 2
Items: 
Size: 592857 Color: 1
Size: 407021 Color: 0

Bin 2996: 123 of cap free
Amount of items: 2
Items: 
Size: 686259 Color: 2
Size: 313619 Color: 1

Bin 2997: 123 of cap free
Amount of items: 2
Items: 
Size: 783124 Color: 0
Size: 216754 Color: 3

Bin 2998: 124 of cap free
Amount of items: 2
Items: 
Size: 757264 Color: 0
Size: 242613 Color: 2

Bin 2999: 124 of cap free
Amount of items: 2
Items: 
Size: 540208 Color: 0
Size: 459669 Color: 1

Bin 3000: 124 of cap free
Amount of items: 2
Items: 
Size: 626191 Color: 1
Size: 373686 Color: 2

Bin 3001: 124 of cap free
Amount of items: 2
Items: 
Size: 740554 Color: 4
Size: 259323 Color: 0

Bin 3002: 124 of cap free
Amount of items: 2
Items: 
Size: 737584 Color: 2
Size: 262293 Color: 4

Bin 3003: 124 of cap free
Amount of items: 2
Items: 
Size: 567864 Color: 4
Size: 432013 Color: 1

Bin 3004: 125 of cap free
Amount of items: 2
Items: 
Size: 741557 Color: 2
Size: 258319 Color: 0

Bin 3005: 125 of cap free
Amount of items: 2
Items: 
Size: 569340 Color: 2
Size: 430536 Color: 4

Bin 3006: 125 of cap free
Amount of items: 2
Items: 
Size: 753568 Color: 2
Size: 246308 Color: 1

Bin 3007: 125 of cap free
Amount of items: 2
Items: 
Size: 528334 Color: 3
Size: 471542 Color: 1

Bin 3008: 126 of cap free
Amount of items: 2
Items: 
Size: 784827 Color: 4
Size: 215048 Color: 3

Bin 3009: 126 of cap free
Amount of items: 2
Items: 
Size: 554830 Color: 3
Size: 445045 Color: 1

Bin 3010: 126 of cap free
Amount of items: 2
Items: 
Size: 606767 Color: 4
Size: 393108 Color: 1

Bin 3011: 126 of cap free
Amount of items: 2
Items: 
Size: 686072 Color: 4
Size: 313803 Color: 1

Bin 3012: 127 of cap free
Amount of items: 2
Items: 
Size: 509084 Color: 2
Size: 490790 Color: 4

Bin 3013: 127 of cap free
Amount of items: 2
Items: 
Size: 510442 Color: 3
Size: 489432 Color: 4

Bin 3014: 127 of cap free
Amount of items: 2
Items: 
Size: 512713 Color: 0
Size: 487161 Color: 2

Bin 3015: 127 of cap free
Amount of items: 2
Items: 
Size: 513040 Color: 2
Size: 486834 Color: 3

Bin 3016: 127 of cap free
Amount of items: 2
Items: 
Size: 669977 Color: 0
Size: 329897 Color: 4

Bin 3017: 127 of cap free
Amount of items: 2
Items: 
Size: 649978 Color: 2
Size: 349896 Color: 0

Bin 3018: 127 of cap free
Amount of items: 2
Items: 
Size: 586242 Color: 2
Size: 413632 Color: 3

Bin 3019: 127 of cap free
Amount of items: 2
Items: 
Size: 645671 Color: 4
Size: 354203 Color: 0

Bin 3020: 128 of cap free
Amount of items: 2
Items: 
Size: 680752 Color: 1
Size: 319121 Color: 2

Bin 3021: 128 of cap free
Amount of items: 2
Items: 
Size: 504968 Color: 2
Size: 494905 Color: 3

Bin 3022: 128 of cap free
Amount of items: 2
Items: 
Size: 591674 Color: 2
Size: 408199 Color: 0

Bin 3023: 128 of cap free
Amount of items: 2
Items: 
Size: 531829 Color: 3
Size: 468044 Color: 2

Bin 3024: 128 of cap free
Amount of items: 2
Items: 
Size: 675327 Color: 2
Size: 324546 Color: 3

Bin 3025: 128 of cap free
Amount of items: 2
Items: 
Size: 557974 Color: 1
Size: 441899 Color: 3

Bin 3026: 128 of cap free
Amount of items: 2
Items: 
Size: 519752 Color: 1
Size: 480121 Color: 4

Bin 3027: 129 of cap free
Amount of items: 2
Items: 
Size: 615807 Color: 1
Size: 384065 Color: 3

Bin 3028: 129 of cap free
Amount of items: 2
Items: 
Size: 671053 Color: 1
Size: 328819 Color: 4

Bin 3029: 129 of cap free
Amount of items: 2
Items: 
Size: 524420 Color: 0
Size: 475452 Color: 2

Bin 3030: 129 of cap free
Amount of items: 2
Items: 
Size: 571413 Color: 2
Size: 428459 Color: 3

Bin 3031: 129 of cap free
Amount of items: 2
Items: 
Size: 598638 Color: 2
Size: 401234 Color: 1

Bin 3032: 129 of cap free
Amount of items: 2
Items: 
Size: 742845 Color: 4
Size: 257027 Color: 3

Bin 3033: 130 of cap free
Amount of items: 2
Items: 
Size: 799968 Color: 3
Size: 199903 Color: 1

Bin 3034: 131 of cap free
Amount of items: 2
Items: 
Size: 536731 Color: 2
Size: 463139 Color: 0

Bin 3035: 131 of cap free
Amount of items: 2
Items: 
Size: 533623 Color: 0
Size: 466247 Color: 3

Bin 3036: 132 of cap free
Amount of items: 2
Items: 
Size: 779619 Color: 1
Size: 220250 Color: 2

Bin 3037: 132 of cap free
Amount of items: 2
Items: 
Size: 603748 Color: 3
Size: 396121 Color: 4

Bin 3038: 132 of cap free
Amount of items: 2
Items: 
Size: 718031 Color: 4
Size: 281838 Color: 0

Bin 3039: 132 of cap free
Amount of items: 2
Items: 
Size: 512200 Color: 4
Size: 487669 Color: 0

Bin 3040: 133 of cap free
Amount of items: 2
Items: 
Size: 570057 Color: 3
Size: 429811 Color: 0

Bin 3041: 133 of cap free
Amount of items: 2
Items: 
Size: 617410 Color: 0
Size: 382458 Color: 1

Bin 3042: 133 of cap free
Amount of items: 2
Items: 
Size: 520565 Color: 4
Size: 479303 Color: 0

Bin 3043: 133 of cap free
Amount of items: 2
Items: 
Size: 770455 Color: 1
Size: 229413 Color: 3

Bin 3044: 134 of cap free
Amount of items: 2
Items: 
Size: 635935 Color: 0
Size: 363932 Color: 2

Bin 3045: 134 of cap free
Amount of items: 2
Items: 
Size: 786363 Color: 2
Size: 213504 Color: 0

Bin 3046: 134 of cap free
Amount of items: 2
Items: 
Size: 556697 Color: 4
Size: 443170 Color: 1

Bin 3047: 134 of cap free
Amount of items: 2
Items: 
Size: 598975 Color: 2
Size: 400892 Color: 1

Bin 3048: 134 of cap free
Amount of items: 2
Items: 
Size: 613763 Color: 0
Size: 386104 Color: 3

Bin 3049: 134 of cap free
Amount of items: 2
Items: 
Size: 619516 Color: 2
Size: 380351 Color: 0

Bin 3050: 134 of cap free
Amount of items: 2
Items: 
Size: 522303 Color: 0
Size: 477564 Color: 1

Bin 3051: 134 of cap free
Amount of items: 2
Items: 
Size: 716869 Color: 1
Size: 282998 Color: 0

Bin 3052: 135 of cap free
Amount of items: 2
Items: 
Size: 799945 Color: 1
Size: 199921 Color: 3

Bin 3053: 135 of cap free
Amount of items: 2
Items: 
Size: 521451 Color: 1
Size: 478415 Color: 4

Bin 3054: 135 of cap free
Amount of items: 2
Items: 
Size: 527282 Color: 1
Size: 472584 Color: 3

Bin 3055: 135 of cap free
Amount of items: 2
Items: 
Size: 532306 Color: 1
Size: 467560 Color: 0

Bin 3056: 135 of cap free
Amount of items: 2
Items: 
Size: 736920 Color: 4
Size: 262946 Color: 3

Bin 3057: 135 of cap free
Amount of items: 2
Items: 
Size: 636315 Color: 1
Size: 363551 Color: 0

Bin 3058: 136 of cap free
Amount of items: 2
Items: 
Size: 602407 Color: 4
Size: 397458 Color: 3

Bin 3059: 136 of cap free
Amount of items: 2
Items: 
Size: 526283 Color: 0
Size: 473582 Color: 1

Bin 3060: 137 of cap free
Amount of items: 2
Items: 
Size: 525362 Color: 4
Size: 474502 Color: 1

Bin 3061: 137 of cap free
Amount of items: 2
Items: 
Size: 708448 Color: 2
Size: 291416 Color: 3

Bin 3062: 137 of cap free
Amount of items: 2
Items: 
Size: 526701 Color: 1
Size: 473163 Color: 4

Bin 3063: 137 of cap free
Amount of items: 2
Items: 
Size: 689765 Color: 4
Size: 310099 Color: 3

Bin 3064: 137 of cap free
Amount of items: 2
Items: 
Size: 564061 Color: 4
Size: 435803 Color: 3

Bin 3065: 138 of cap free
Amount of items: 2
Items: 
Size: 565221 Color: 0
Size: 434642 Color: 1

Bin 3066: 138 of cap free
Amount of items: 2
Items: 
Size: 740285 Color: 4
Size: 259578 Color: 1

Bin 3067: 138 of cap free
Amount of items: 2
Items: 
Size: 786884 Color: 4
Size: 212979 Color: 2

Bin 3068: 138 of cap free
Amount of items: 2
Items: 
Size: 775302 Color: 2
Size: 224561 Color: 1

Bin 3069: 138 of cap free
Amount of items: 2
Items: 
Size: 676865 Color: 0
Size: 322998 Color: 4

Bin 3070: 138 of cap free
Amount of items: 2
Items: 
Size: 541025 Color: 0
Size: 458838 Color: 3

Bin 3071: 138 of cap free
Amount of items: 2
Items: 
Size: 773320 Color: 3
Size: 226543 Color: 1

Bin 3072: 139 of cap free
Amount of items: 2
Items: 
Size: 690995 Color: 3
Size: 308867 Color: 1

Bin 3073: 139 of cap free
Amount of items: 2
Items: 
Size: 529771 Color: 2
Size: 470091 Color: 4

Bin 3074: 139 of cap free
Amount of items: 2
Items: 
Size: 549150 Color: 3
Size: 450712 Color: 1

Bin 3075: 139 of cap free
Amount of items: 2
Items: 
Size: 581024 Color: 4
Size: 418838 Color: 2

Bin 3076: 140 of cap free
Amount of items: 2
Items: 
Size: 776654 Color: 1
Size: 223207 Color: 0

Bin 3077: 140 of cap free
Amount of items: 2
Items: 
Size: 625271 Color: 0
Size: 374590 Color: 3

Bin 3078: 140 of cap free
Amount of items: 2
Items: 
Size: 698727 Color: 2
Size: 301134 Color: 1

Bin 3079: 140 of cap free
Amount of items: 2
Items: 
Size: 788308 Color: 0
Size: 211553 Color: 4

Bin 3080: 140 of cap free
Amount of items: 2
Items: 
Size: 580864 Color: 3
Size: 418997 Color: 0

Bin 3081: 141 of cap free
Amount of items: 2
Items: 
Size: 610261 Color: 3
Size: 389599 Color: 4

Bin 3082: 141 of cap free
Amount of items: 2
Items: 
Size: 655617 Color: 2
Size: 344243 Color: 0

Bin 3083: 141 of cap free
Amount of items: 2
Items: 
Size: 659713 Color: 1
Size: 340147 Color: 0

Bin 3084: 142 of cap free
Amount of items: 2
Items: 
Size: 749013 Color: 1
Size: 250846 Color: 3

Bin 3085: 142 of cap free
Amount of items: 2
Items: 
Size: 651595 Color: 4
Size: 348264 Color: 0

Bin 3086: 142 of cap free
Amount of items: 2
Items: 
Size: 724413 Color: 0
Size: 275446 Color: 1

Bin 3087: 142 of cap free
Amount of items: 2
Items: 
Size: 745590 Color: 4
Size: 254269 Color: 3

Bin 3088: 142 of cap free
Amount of items: 2
Items: 
Size: 502458 Color: 0
Size: 497401 Color: 3

Bin 3089: 143 of cap free
Amount of items: 2
Items: 
Size: 685997 Color: 1
Size: 313861 Color: 4

Bin 3090: 143 of cap free
Amount of items: 2
Items: 
Size: 658078 Color: 0
Size: 341780 Color: 1

Bin 3091: 143 of cap free
Amount of items: 2
Items: 
Size: 633719 Color: 0
Size: 366139 Color: 1

Bin 3092: 144 of cap free
Amount of items: 2
Items: 
Size: 549612 Color: 0
Size: 450245 Color: 3

Bin 3093: 144 of cap free
Amount of items: 2
Items: 
Size: 581680 Color: 1
Size: 418177 Color: 2

Bin 3094: 144 of cap free
Amount of items: 2
Items: 
Size: 647481 Color: 1
Size: 352376 Color: 0

Bin 3095: 144 of cap free
Amount of items: 2
Items: 
Size: 729541 Color: 1
Size: 270316 Color: 2

Bin 3096: 144 of cap free
Amount of items: 2
Items: 
Size: 691656 Color: 2
Size: 308201 Color: 0

Bin 3097: 144 of cap free
Amount of items: 2
Items: 
Size: 591670 Color: 0
Size: 408187 Color: 3

Bin 3098: 145 of cap free
Amount of items: 2
Items: 
Size: 768352 Color: 0
Size: 231504 Color: 2

Bin 3099: 145 of cap free
Amount of items: 2
Items: 
Size: 745939 Color: 4
Size: 253917 Color: 0

Bin 3100: 146 of cap free
Amount of items: 2
Items: 
Size: 719622 Color: 4
Size: 280233 Color: 3

Bin 3101: 146 of cap free
Amount of items: 2
Items: 
Size: 524510 Color: 2
Size: 475345 Color: 1

Bin 3102: 146 of cap free
Amount of items: 2
Items: 
Size: 694564 Color: 1
Size: 305291 Color: 0

Bin 3103: 146 of cap free
Amount of items: 2
Items: 
Size: 627874 Color: 4
Size: 371981 Color: 0

Bin 3104: 146 of cap free
Amount of items: 2
Items: 
Size: 776290 Color: 2
Size: 223565 Color: 1

Bin 3105: 147 of cap free
Amount of items: 2
Items: 
Size: 548932 Color: 3
Size: 450922 Color: 4

Bin 3106: 147 of cap free
Amount of items: 2
Items: 
Size: 739350 Color: 3
Size: 260504 Color: 4

Bin 3107: 147 of cap free
Amount of items: 2
Items: 
Size: 506071 Color: 2
Size: 493783 Color: 1

Bin 3108: 148 of cap free
Amount of items: 2
Items: 
Size: 597349 Color: 2
Size: 402504 Color: 0

Bin 3109: 148 of cap free
Amount of items: 2
Items: 
Size: 755121 Color: 0
Size: 244732 Color: 1

Bin 3110: 148 of cap free
Amount of items: 2
Items: 
Size: 777558 Color: 4
Size: 222295 Color: 3

Bin 3111: 149 of cap free
Amount of items: 2
Items: 
Size: 694349 Color: 4
Size: 305503 Color: 3

Bin 3112: 149 of cap free
Amount of items: 2
Items: 
Size: 603372 Color: 1
Size: 396480 Color: 2

Bin 3113: 149 of cap free
Amount of items: 2
Items: 
Size: 645668 Color: 4
Size: 354184 Color: 3

Bin 3114: 149 of cap free
Amount of items: 2
Items: 
Size: 609836 Color: 0
Size: 390016 Color: 4

Bin 3115: 150 of cap free
Amount of items: 2
Items: 
Size: 559876 Color: 1
Size: 439975 Color: 3

Bin 3116: 150 of cap free
Amount of items: 2
Items: 
Size: 524149 Color: 0
Size: 475702 Color: 1

Bin 3117: 150 of cap free
Amount of items: 2
Items: 
Size: 720472 Color: 1
Size: 279379 Color: 2

Bin 3118: 151 of cap free
Amount of items: 2
Items: 
Size: 695860 Color: 2
Size: 303990 Color: 1

Bin 3119: 151 of cap free
Amount of items: 2
Items: 
Size: 529236 Color: 0
Size: 470614 Color: 2

Bin 3120: 151 of cap free
Amount of items: 2
Items: 
Size: 687478 Color: 3
Size: 312372 Color: 2

Bin 3121: 151 of cap free
Amount of items: 2
Items: 
Size: 760018 Color: 4
Size: 239832 Color: 3

Bin 3122: 151 of cap free
Amount of items: 2
Items: 
Size: 604930 Color: 3
Size: 394920 Color: 0

Bin 3123: 151 of cap free
Amount of items: 2
Items: 
Size: 518745 Color: 2
Size: 481105 Color: 4

Bin 3124: 151 of cap free
Amount of items: 2
Items: 
Size: 577119 Color: 4
Size: 422731 Color: 2

Bin 3125: 152 of cap free
Amount of items: 2
Items: 
Size: 714583 Color: 1
Size: 285266 Color: 0

Bin 3126: 152 of cap free
Amount of items: 2
Items: 
Size: 730657 Color: 1
Size: 269192 Color: 3

Bin 3127: 152 of cap free
Amount of items: 2
Items: 
Size: 693151 Color: 3
Size: 306698 Color: 1

Bin 3128: 153 of cap free
Amount of items: 2
Items: 
Size: 569965 Color: 4
Size: 429883 Color: 3

Bin 3129: 153 of cap free
Amount of items: 2
Items: 
Size: 525124 Color: 1
Size: 474724 Color: 2

Bin 3130: 153 of cap free
Amount of items: 2
Items: 
Size: 751252 Color: 4
Size: 248596 Color: 0

Bin 3131: 153 of cap free
Amount of items: 2
Items: 
Size: 720294 Color: 2
Size: 279554 Color: 4

Bin 3132: 153 of cap free
Amount of items: 2
Items: 
Size: 518405 Color: 2
Size: 481443 Color: 4

Bin 3133: 153 of cap free
Amount of items: 2
Items: 
Size: 749392 Color: 3
Size: 250456 Color: 4

Bin 3134: 153 of cap free
Amount of items: 2
Items: 
Size: 605907 Color: 4
Size: 393941 Color: 1

Bin 3135: 154 of cap free
Amount of items: 2
Items: 
Size: 514015 Color: 3
Size: 485832 Color: 1

Bin 3136: 154 of cap free
Amount of items: 2
Items: 
Size: 515556 Color: 0
Size: 484291 Color: 1

Bin 3137: 154 of cap free
Amount of items: 2
Items: 
Size: 704790 Color: 0
Size: 295057 Color: 1

Bin 3138: 154 of cap free
Amount of items: 2
Items: 
Size: 644348 Color: 2
Size: 355499 Color: 3

Bin 3139: 155 of cap free
Amount of items: 2
Items: 
Size: 513702 Color: 3
Size: 486144 Color: 2

Bin 3140: 155 of cap free
Amount of items: 2
Items: 
Size: 552089 Color: 1
Size: 447757 Color: 2

Bin 3141: 155 of cap free
Amount of items: 2
Items: 
Size: 598965 Color: 2
Size: 400881 Color: 0

Bin 3142: 155 of cap free
Amount of items: 2
Items: 
Size: 769419 Color: 3
Size: 230427 Color: 1

Bin 3143: 156 of cap free
Amount of items: 2
Items: 
Size: 702840 Color: 1
Size: 297005 Color: 3

Bin 3144: 156 of cap free
Amount of items: 2
Items: 
Size: 526615 Color: 0
Size: 473230 Color: 1

Bin 3145: 156 of cap free
Amount of items: 2
Items: 
Size: 654768 Color: 0
Size: 345077 Color: 3

Bin 3146: 156 of cap free
Amount of items: 2
Items: 
Size: 758528 Color: 4
Size: 241317 Color: 0

Bin 3147: 157 of cap free
Amount of items: 2
Items: 
Size: 526944 Color: 0
Size: 472900 Color: 2

Bin 3148: 157 of cap free
Amount of items: 2
Items: 
Size: 688251 Color: 4
Size: 311593 Color: 3

Bin 3149: 157 of cap free
Amount of items: 2
Items: 
Size: 717233 Color: 0
Size: 282611 Color: 3

Bin 3150: 158 of cap free
Amount of items: 2
Items: 
Size: 783398 Color: 0
Size: 216445 Color: 1

Bin 3151: 158 of cap free
Amount of items: 2
Items: 
Size: 534481 Color: 1
Size: 465362 Color: 4

Bin 3152: 158 of cap free
Amount of items: 2
Items: 
Size: 693152 Color: 1
Size: 306691 Color: 4

Bin 3153: 158 of cap free
Amount of items: 2
Items: 
Size: 711140 Color: 2
Size: 288703 Color: 0

Bin 3154: 158 of cap free
Amount of items: 2
Items: 
Size: 667993 Color: 4
Size: 331850 Color: 3

Bin 3155: 158 of cap free
Amount of items: 2
Items: 
Size: 575919 Color: 0
Size: 423924 Color: 4

Bin 3156: 158 of cap free
Amount of items: 2
Items: 
Size: 670226 Color: 2
Size: 329617 Color: 1

Bin 3157: 159 of cap free
Amount of items: 2
Items: 
Size: 748991 Color: 3
Size: 250851 Color: 1

Bin 3158: 159 of cap free
Amount of items: 2
Items: 
Size: 522014 Color: 0
Size: 477828 Color: 1

Bin 3159: 159 of cap free
Amount of items: 2
Items: 
Size: 543585 Color: 2
Size: 456257 Color: 0

Bin 3160: 159 of cap free
Amount of items: 2
Items: 
Size: 624767 Color: 3
Size: 375075 Color: 2

Bin 3161: 160 of cap free
Amount of items: 2
Items: 
Size: 647987 Color: 0
Size: 351854 Color: 2

Bin 3162: 160 of cap free
Amount of items: 2
Items: 
Size: 644347 Color: 0
Size: 355494 Color: 2

Bin 3163: 160 of cap free
Amount of items: 2
Items: 
Size: 653094 Color: 1
Size: 346747 Color: 4

Bin 3164: 160 of cap free
Amount of items: 2
Items: 
Size: 705397 Color: 3
Size: 294444 Color: 2

Bin 3165: 160 of cap free
Amount of items: 2
Items: 
Size: 651590 Color: 2
Size: 348251 Color: 0

Bin 3166: 160 of cap free
Amount of items: 2
Items: 
Size: 572550 Color: 2
Size: 427291 Color: 0

Bin 3167: 160 of cap free
Amount of items: 2
Items: 
Size: 570717 Color: 1
Size: 429124 Color: 0

Bin 3168: 161 of cap free
Amount of items: 2
Items: 
Size: 544033 Color: 1
Size: 455807 Color: 0

Bin 3169: 161 of cap free
Amount of items: 2
Items: 
Size: 688684 Color: 2
Size: 311156 Color: 4

Bin 3170: 161 of cap free
Amount of items: 2
Items: 
Size: 585603 Color: 2
Size: 414237 Color: 3

Bin 3171: 161 of cap free
Amount of items: 2
Items: 
Size: 588970 Color: 0
Size: 410870 Color: 2

Bin 3172: 161 of cap free
Amount of items: 2
Items: 
Size: 766104 Color: 1
Size: 233736 Color: 4

Bin 3173: 161 of cap free
Amount of items: 2
Items: 
Size: 657890 Color: 1
Size: 341950 Color: 2

Bin 3174: 161 of cap free
Amount of items: 2
Items: 
Size: 753860 Color: 0
Size: 245980 Color: 1

Bin 3175: 162 of cap free
Amount of items: 2
Items: 
Size: 735666 Color: 3
Size: 264173 Color: 0

Bin 3176: 162 of cap free
Amount of items: 2
Items: 
Size: 612279 Color: 4
Size: 387560 Color: 0

Bin 3177: 162 of cap free
Amount of items: 2
Items: 
Size: 563440 Color: 4
Size: 436399 Color: 1

Bin 3178: 162 of cap free
Amount of items: 2
Items: 
Size: 569118 Color: 4
Size: 430721 Color: 0

Bin 3179: 163 of cap free
Amount of items: 2
Items: 
Size: 722765 Color: 2
Size: 277073 Color: 4

Bin 3180: 163 of cap free
Amount of items: 2
Items: 
Size: 702479 Color: 0
Size: 297359 Color: 2

Bin 3181: 163 of cap free
Amount of items: 2
Items: 
Size: 548604 Color: 4
Size: 451234 Color: 2

Bin 3182: 164 of cap free
Amount of items: 2
Items: 
Size: 782257 Color: 2
Size: 217580 Color: 1

Bin 3183: 164 of cap free
Amount of items: 2
Items: 
Size: 577349 Color: 4
Size: 422488 Color: 3

Bin 3184: 164 of cap free
Amount of items: 2
Items: 
Size: 519956 Color: 4
Size: 479881 Color: 2

Bin 3185: 164 of cap free
Amount of items: 2
Items: 
Size: 665216 Color: 1
Size: 334621 Color: 2

Bin 3186: 164 of cap free
Amount of items: 2
Items: 
Size: 687688 Color: 3
Size: 312149 Color: 0

Bin 3187: 164 of cap free
Amount of items: 2
Items: 
Size: 769192 Color: 3
Size: 230645 Color: 4

Bin 3188: 165 of cap free
Amount of items: 2
Items: 
Size: 616260 Color: 3
Size: 383576 Color: 2

Bin 3189: 165 of cap free
Amount of items: 2
Items: 
Size: 599339 Color: 3
Size: 400497 Color: 4

Bin 3190: 165 of cap free
Amount of items: 2
Items: 
Size: 690322 Color: 3
Size: 309514 Color: 4

Bin 3191: 165 of cap free
Amount of items: 2
Items: 
Size: 769890 Color: 4
Size: 229946 Color: 2

Bin 3192: 165 of cap free
Amount of items: 2
Items: 
Size: 557205 Color: 0
Size: 442631 Color: 4

Bin 3193: 165 of cap free
Amount of items: 2
Items: 
Size: 625246 Color: 2
Size: 374590 Color: 4

Bin 3194: 166 of cap free
Amount of items: 2
Items: 
Size: 546029 Color: 3
Size: 453806 Color: 2

Bin 3195: 166 of cap free
Amount of items: 2
Items: 
Size: 718665 Color: 3
Size: 281170 Color: 4

Bin 3196: 167 of cap free
Amount of items: 2
Items: 
Size: 764345 Color: 0
Size: 235489 Color: 2

Bin 3197: 167 of cap free
Amount of items: 2
Items: 
Size: 675753 Color: 1
Size: 324081 Color: 2

Bin 3198: 167 of cap free
Amount of items: 2
Items: 
Size: 556394 Color: 0
Size: 443440 Color: 4

Bin 3199: 167 of cap free
Amount of items: 2
Items: 
Size: 695512 Color: 4
Size: 304322 Color: 1

Bin 3200: 167 of cap free
Amount of items: 2
Items: 
Size: 665467 Color: 4
Size: 334367 Color: 0

Bin 3201: 167 of cap free
Amount of items: 2
Items: 
Size: 756092 Color: 0
Size: 243742 Color: 1

Bin 3202: 167 of cap free
Amount of items: 2
Items: 
Size: 582638 Color: 2
Size: 417196 Color: 0

Bin 3203: 168 of cap free
Amount of items: 2
Items: 
Size: 608456 Color: 2
Size: 391377 Color: 1

Bin 3204: 168 of cap free
Amount of items: 2
Items: 
Size: 634350 Color: 2
Size: 365483 Color: 0

Bin 3205: 168 of cap free
Amount of items: 2
Items: 
Size: 565747 Color: 1
Size: 434086 Color: 0

Bin 3206: 170 of cap free
Amount of items: 2
Items: 
Size: 543325 Color: 2
Size: 456506 Color: 3

Bin 3207: 170 of cap free
Amount of items: 2
Items: 
Size: 532971 Color: 4
Size: 466860 Color: 3

Bin 3208: 170 of cap free
Amount of items: 2
Items: 
Size: 560372 Color: 4
Size: 439459 Color: 2

Bin 3209: 170 of cap free
Amount of items: 2
Items: 
Size: 646829 Color: 4
Size: 353002 Color: 0

Bin 3210: 170 of cap free
Amount of items: 2
Items: 
Size: 693717 Color: 1
Size: 306114 Color: 3

Bin 3211: 170 of cap free
Amount of items: 2
Items: 
Size: 726749 Color: 2
Size: 273082 Color: 1

Bin 3212: 170 of cap free
Amount of items: 2
Items: 
Size: 744618 Color: 0
Size: 255213 Color: 3

Bin 3213: 171 of cap free
Amount of items: 2
Items: 
Size: 586656 Color: 0
Size: 413174 Color: 3

Bin 3214: 171 of cap free
Amount of items: 2
Items: 
Size: 538470 Color: 0
Size: 461360 Color: 2

Bin 3215: 171 of cap free
Amount of items: 2
Items: 
Size: 627957 Color: 0
Size: 371873 Color: 3

Bin 3216: 171 of cap free
Amount of items: 2
Items: 
Size: 542108 Color: 2
Size: 457722 Color: 1

Bin 3217: 172 of cap free
Amount of items: 2
Items: 
Size: 736141 Color: 3
Size: 263688 Color: 1

Bin 3218: 172 of cap free
Amount of items: 2
Items: 
Size: 797831 Color: 2
Size: 201998 Color: 0

Bin 3219: 172 of cap free
Amount of items: 2
Items: 
Size: 550248 Color: 3
Size: 449581 Color: 1

Bin 3220: 172 of cap free
Amount of items: 2
Items: 
Size: 557011 Color: 1
Size: 442818 Color: 3

Bin 3221: 173 of cap free
Amount of items: 2
Items: 
Size: 526977 Color: 2
Size: 472851 Color: 3

Bin 3222: 173 of cap free
Amount of items: 2
Items: 
Size: 641848 Color: 4
Size: 357980 Color: 1

Bin 3223: 173 of cap free
Amount of items: 2
Items: 
Size: 773848 Color: 2
Size: 225980 Color: 3

Bin 3224: 173 of cap free
Amount of items: 2
Items: 
Size: 538668 Color: 2
Size: 461160 Color: 0

Bin 3225: 174 of cap free
Amount of items: 2
Items: 
Size: 537281 Color: 0
Size: 462546 Color: 2

Bin 3226: 174 of cap free
Amount of items: 2
Items: 
Size: 794517 Color: 2
Size: 205310 Color: 0

Bin 3227: 174 of cap free
Amount of items: 2
Items: 
Size: 637183 Color: 4
Size: 362644 Color: 3

Bin 3228: 175 of cap free
Amount of items: 2
Items: 
Size: 542318 Color: 3
Size: 457508 Color: 2

Bin 3229: 175 of cap free
Amount of items: 2
Items: 
Size: 765477 Color: 0
Size: 234349 Color: 4

Bin 3230: 175 of cap free
Amount of items: 2
Items: 
Size: 581861 Color: 0
Size: 417965 Color: 4

Bin 3231: 176 of cap free
Amount of items: 2
Items: 
Size: 647303 Color: 2
Size: 352522 Color: 3

Bin 3232: 176 of cap free
Amount of items: 2
Items: 
Size: 609612 Color: 1
Size: 390213 Color: 3

Bin 3233: 176 of cap free
Amount of items: 2
Items: 
Size: 631847 Color: 4
Size: 367978 Color: 1

Bin 3234: 176 of cap free
Amount of items: 2
Items: 
Size: 657554 Color: 3
Size: 342271 Color: 4

Bin 3235: 176 of cap free
Amount of items: 2
Items: 
Size: 523143 Color: 0
Size: 476682 Color: 2

Bin 3236: 176 of cap free
Amount of items: 2
Items: 
Size: 772786 Color: 1
Size: 227039 Color: 3

Bin 3237: 177 of cap free
Amount of items: 2
Items: 
Size: 752012 Color: 1
Size: 247812 Color: 4

Bin 3238: 177 of cap free
Amount of items: 2
Items: 
Size: 532898 Color: 3
Size: 466926 Color: 4

Bin 3239: 177 of cap free
Amount of items: 2
Items: 
Size: 685083 Color: 4
Size: 314741 Color: 0

Bin 3240: 177 of cap free
Amount of items: 2
Items: 
Size: 549143 Color: 2
Size: 450681 Color: 3

Bin 3241: 177 of cap free
Amount of items: 2
Items: 
Size: 517152 Color: 4
Size: 482672 Color: 1

Bin 3242: 178 of cap free
Amount of items: 2
Items: 
Size: 506062 Color: 3
Size: 493761 Color: 1

Bin 3243: 178 of cap free
Amount of items: 2
Items: 
Size: 593375 Color: 1
Size: 406448 Color: 2

Bin 3244: 178 of cap free
Amount of items: 2
Items: 
Size: 753076 Color: 4
Size: 246747 Color: 3

Bin 3245: 178 of cap free
Amount of items: 2
Items: 
Size: 780340 Color: 3
Size: 219483 Color: 0

Bin 3246: 178 of cap free
Amount of items: 2
Items: 
Size: 729123 Color: 1
Size: 270700 Color: 3

Bin 3247: 178 of cap free
Amount of items: 2
Items: 
Size: 595689 Color: 3
Size: 404134 Color: 2

Bin 3248: 179 of cap free
Amount of items: 2
Items: 
Size: 772989 Color: 3
Size: 226833 Color: 1

Bin 3249: 179 of cap free
Amount of items: 2
Items: 
Size: 616628 Color: 2
Size: 383194 Color: 1

Bin 3250: 179 of cap free
Amount of items: 2
Items: 
Size: 704982 Color: 1
Size: 294840 Color: 4

Bin 3251: 179 of cap free
Amount of items: 2
Items: 
Size: 719850 Color: 4
Size: 279972 Color: 2

Bin 3252: 180 of cap free
Amount of items: 2
Items: 
Size: 513312 Color: 2
Size: 486509 Color: 3

Bin 3253: 181 of cap free
Amount of items: 2
Items: 
Size: 553364 Color: 2
Size: 446456 Color: 0

Bin 3254: 181 of cap free
Amount of items: 2
Items: 
Size: 713434 Color: 4
Size: 286386 Color: 2

Bin 3255: 181 of cap free
Amount of items: 2
Items: 
Size: 542565 Color: 3
Size: 457255 Color: 1

Bin 3256: 182 of cap free
Amount of items: 2
Items: 
Size: 704085 Color: 3
Size: 295734 Color: 2

Bin 3257: 182 of cap free
Amount of items: 2
Items: 
Size: 610414 Color: 4
Size: 389405 Color: 0

Bin 3258: 182 of cap free
Amount of items: 2
Items: 
Size: 580231 Color: 4
Size: 419588 Color: 3

Bin 3259: 182 of cap free
Amount of items: 2
Items: 
Size: 671979 Color: 1
Size: 327840 Color: 4

Bin 3260: 182 of cap free
Amount of items: 2
Items: 
Size: 720693 Color: 3
Size: 279126 Color: 4

Bin 3261: 182 of cap free
Amount of items: 2
Items: 
Size: 555298 Color: 1
Size: 444521 Color: 3

Bin 3262: 183 of cap free
Amount of items: 2
Items: 
Size: 781108 Color: 3
Size: 218710 Color: 4

Bin 3263: 183 of cap free
Amount of items: 3
Items: 
Size: 651219 Color: 2
Size: 174962 Color: 0
Size: 173637 Color: 1

Bin 3264: 183 of cap free
Amount of items: 2
Items: 
Size: 767672 Color: 2
Size: 232146 Color: 3

Bin 3265: 183 of cap free
Amount of items: 2
Items: 
Size: 530560 Color: 1
Size: 469258 Color: 2

Bin 3266: 184 of cap free
Amount of items: 2
Items: 
Size: 671614 Color: 4
Size: 328203 Color: 1

Bin 3267: 184 of cap free
Amount of items: 2
Items: 
Size: 706736 Color: 1
Size: 293081 Color: 3

Bin 3268: 184 of cap free
Amount of items: 2
Items: 
Size: 719487 Color: 3
Size: 280330 Color: 1

Bin 3269: 184 of cap free
Amount of items: 2
Items: 
Size: 503668 Color: 3
Size: 496149 Color: 1

Bin 3270: 185 of cap free
Amount of items: 2
Items: 
Size: 591366 Color: 4
Size: 408450 Color: 2

Bin 3271: 185 of cap free
Amount of items: 2
Items: 
Size: 666544 Color: 1
Size: 333272 Color: 3

Bin 3272: 185 of cap free
Amount of items: 2
Items: 
Size: 627617 Color: 2
Size: 372199 Color: 0

Bin 3273: 186 of cap free
Amount of items: 2
Items: 
Size: 717982 Color: 4
Size: 281833 Color: 2

Bin 3274: 186 of cap free
Amount of items: 2
Items: 
Size: 577612 Color: 4
Size: 422203 Color: 3

Bin 3275: 186 of cap free
Amount of items: 2
Items: 
Size: 581178 Color: 3
Size: 418637 Color: 2

Bin 3276: 186 of cap free
Amount of items: 2
Items: 
Size: 721944 Color: 3
Size: 277871 Color: 2

Bin 3277: 186 of cap free
Amount of items: 2
Items: 
Size: 751218 Color: 3
Size: 248597 Color: 4

Bin 3278: 186 of cap free
Amount of items: 2
Items: 
Size: 588519 Color: 1
Size: 411296 Color: 4

Bin 3279: 186 of cap free
Amount of items: 2
Items: 
Size: 538294 Color: 2
Size: 461521 Color: 1

Bin 3280: 186 of cap free
Amount of items: 2
Items: 
Size: 532446 Color: 2
Size: 467369 Color: 3

Bin 3281: 187 of cap free
Amount of items: 2
Items: 
Size: 572285 Color: 4
Size: 427529 Color: 3

Bin 3282: 187 of cap free
Amount of items: 2
Items: 
Size: 508301 Color: 1
Size: 491513 Color: 0

Bin 3283: 187 of cap free
Amount of items: 2
Items: 
Size: 611626 Color: 4
Size: 388188 Color: 1

Bin 3284: 187 of cap free
Amount of items: 2
Items: 
Size: 636278 Color: 0
Size: 363536 Color: 4

Bin 3285: 187 of cap free
Amount of items: 2
Items: 
Size: 753879 Color: 1
Size: 245935 Color: 2

Bin 3286: 187 of cap free
Amount of items: 2
Items: 
Size: 574208 Color: 2
Size: 425606 Color: 1

Bin 3287: 188 of cap free
Amount of items: 2
Items: 
Size: 666220 Color: 2
Size: 333593 Color: 3

Bin 3288: 188 of cap free
Amount of items: 2
Items: 
Size: 624988 Color: 4
Size: 374825 Color: 3

Bin 3289: 188 of cap free
Amount of items: 2
Items: 
Size: 765535 Color: 4
Size: 234278 Color: 2

Bin 3290: 189 of cap free
Amount of items: 2
Items: 
Size: 549878 Color: 4
Size: 449934 Color: 2

Bin 3291: 189 of cap free
Amount of items: 2
Items: 
Size: 626150 Color: 3
Size: 373662 Color: 0

Bin 3292: 189 of cap free
Amount of items: 2
Items: 
Size: 601839 Color: 1
Size: 397973 Color: 3

Bin 3293: 189 of cap free
Amount of items: 2
Items: 
Size: 766472 Color: 4
Size: 233340 Color: 0

Bin 3294: 189 of cap free
Amount of items: 2
Items: 
Size: 767677 Color: 3
Size: 232135 Color: 2

Bin 3295: 190 of cap free
Amount of items: 2
Items: 
Size: 764531 Color: 0
Size: 235280 Color: 4

Bin 3296: 190 of cap free
Amount of items: 2
Items: 
Size: 534912 Color: 0
Size: 464899 Color: 1

Bin 3297: 190 of cap free
Amount of items: 2
Items: 
Size: 579182 Color: 0
Size: 420629 Color: 3

Bin 3298: 190 of cap free
Amount of items: 3
Items: 
Size: 621884 Color: 2
Size: 189156 Color: 4
Size: 188771 Color: 1

Bin 3299: 190 of cap free
Amount of items: 2
Items: 
Size: 620321 Color: 2
Size: 379490 Color: 0

Bin 3300: 190 of cap free
Amount of items: 2
Items: 
Size: 570475 Color: 2
Size: 429336 Color: 0

Bin 3301: 190 of cap free
Amount of items: 2
Items: 
Size: 639727 Color: 4
Size: 360084 Color: 3

Bin 3302: 190 of cap free
Amount of items: 2
Items: 
Size: 561967 Color: 1
Size: 437844 Color: 0

Bin 3303: 191 of cap free
Amount of items: 2
Items: 
Size: 799932 Color: 0
Size: 199878 Color: 3

Bin 3304: 191 of cap free
Amount of items: 2
Items: 
Size: 691419 Color: 1
Size: 308391 Color: 2

Bin 3305: 191 of cap free
Amount of items: 2
Items: 
Size: 791167 Color: 0
Size: 208643 Color: 4

Bin 3306: 191 of cap free
Amount of items: 2
Items: 
Size: 729116 Color: 1
Size: 270694 Color: 0

Bin 3307: 191 of cap free
Amount of items: 2
Items: 
Size: 578068 Color: 2
Size: 421742 Color: 4

Bin 3308: 192 of cap free
Amount of items: 2
Items: 
Size: 524478 Color: 2
Size: 475331 Color: 0

Bin 3309: 192 of cap free
Amount of items: 2
Items: 
Size: 768557 Color: 1
Size: 231252 Color: 3

Bin 3310: 192 of cap free
Amount of items: 2
Items: 
Size: 751718 Color: 1
Size: 248091 Color: 0

Bin 3311: 193 of cap free
Amount of items: 2
Items: 
Size: 555288 Color: 0
Size: 444520 Color: 2

Bin 3312: 193 of cap free
Amount of items: 2
Items: 
Size: 651217 Color: 2
Size: 348591 Color: 1

Bin 3313: 193 of cap free
Amount of items: 2
Items: 
Size: 596609 Color: 2
Size: 403199 Color: 4

Bin 3314: 193 of cap free
Amount of items: 2
Items: 
Size: 678364 Color: 0
Size: 321444 Color: 1

Bin 3315: 193 of cap free
Amount of items: 2
Items: 
Size: 507014 Color: 0
Size: 492794 Color: 2

Bin 3316: 194 of cap free
Amount of items: 2
Items: 
Size: 772527 Color: 2
Size: 227280 Color: 4

Bin 3317: 195 of cap free
Amount of items: 2
Items: 
Size: 726214 Color: 3
Size: 273592 Color: 0

Bin 3318: 195 of cap free
Amount of items: 2
Items: 
Size: 513196 Color: 4
Size: 486610 Color: 2

Bin 3319: 195 of cap free
Amount of items: 2
Items: 
Size: 642269 Color: 0
Size: 357537 Color: 2

Bin 3320: 195 of cap free
Amount of items: 2
Items: 
Size: 703732 Color: 2
Size: 296074 Color: 1

Bin 3321: 195 of cap free
Amount of items: 2
Items: 
Size: 631627 Color: 4
Size: 368179 Color: 1

Bin 3322: 196 of cap free
Amount of items: 2
Items: 
Size: 701748 Color: 4
Size: 298057 Color: 0

Bin 3323: 197 of cap free
Amount of items: 2
Items: 
Size: 510628 Color: 0
Size: 489176 Color: 3

Bin 3324: 197 of cap free
Amount of items: 2
Items: 
Size: 696396 Color: 4
Size: 303408 Color: 1

Bin 3325: 197 of cap free
Amount of items: 2
Items: 
Size: 717968 Color: 2
Size: 281836 Color: 1

Bin 3326: 197 of cap free
Amount of items: 2
Items: 
Size: 546998 Color: 1
Size: 452806 Color: 2

Bin 3327: 197 of cap free
Amount of items: 2
Items: 
Size: 621665 Color: 4
Size: 378139 Color: 3

Bin 3328: 197 of cap free
Amount of items: 2
Items: 
Size: 672675 Color: 2
Size: 327129 Color: 0

Bin 3329: 198 of cap free
Amount of items: 2
Items: 
Size: 641157 Color: 3
Size: 358646 Color: 2

Bin 3330: 198 of cap free
Amount of items: 2
Items: 
Size: 658269 Color: 3
Size: 341534 Color: 0

Bin 3331: 198 of cap free
Amount of items: 2
Items: 
Size: 652836 Color: 1
Size: 346967 Color: 2

Bin 3332: 199 of cap free
Amount of items: 2
Items: 
Size: 652831 Color: 2
Size: 346971 Color: 4

Bin 3333: 199 of cap free
Amount of items: 2
Items: 
Size: 654044 Color: 2
Size: 345758 Color: 0

Bin 3334: 199 of cap free
Amount of items: 2
Items: 
Size: 535555 Color: 4
Size: 464247 Color: 2

Bin 3335: 199 of cap free
Amount of items: 2
Items: 
Size: 622538 Color: 1
Size: 377264 Color: 4

Bin 3336: 200 of cap free
Amount of items: 2
Items: 
Size: 624547 Color: 3
Size: 375254 Color: 0

Bin 3337: 200 of cap free
Amount of items: 2
Items: 
Size: 668209 Color: 3
Size: 331592 Color: 1

Bin 3338: 200 of cap free
Amount of items: 2
Items: 
Size: 677114 Color: 0
Size: 322687 Color: 4

Bin 3339: 201 of cap free
Amount of items: 2
Items: 
Size: 527485 Color: 0
Size: 472315 Color: 1

Bin 3340: 201 of cap free
Amount of items: 2
Items: 
Size: 618451 Color: 4
Size: 381349 Color: 0

Bin 3341: 202 of cap free
Amount of items: 2
Items: 
Size: 578616 Color: 1
Size: 421183 Color: 3

Bin 3342: 202 of cap free
Amount of items: 2
Items: 
Size: 665438 Color: 4
Size: 334361 Color: 3

Bin 3343: 202 of cap free
Amount of items: 2
Items: 
Size: 512161 Color: 4
Size: 487638 Color: 2

Bin 3344: 202 of cap free
Amount of items: 2
Items: 
Size: 643297 Color: 1
Size: 356502 Color: 3

Bin 3345: 202 of cap free
Amount of items: 2
Items: 
Size: 700763 Color: 4
Size: 299036 Color: 2

Bin 3346: 202 of cap free
Amount of items: 2
Items: 
Size: 703277 Color: 4
Size: 296522 Color: 1

Bin 3347: 202 of cap free
Amount of items: 2
Items: 
Size: 742713 Color: 2
Size: 257086 Color: 4

Bin 3348: 203 of cap free
Amount of items: 2
Items: 
Size: 750099 Color: 4
Size: 249699 Color: 3

Bin 3349: 203 of cap free
Amount of items: 2
Items: 
Size: 775022 Color: 3
Size: 224776 Color: 1

Bin 3350: 203 of cap free
Amount of items: 2
Items: 
Size: 511218 Color: 3
Size: 488580 Color: 2

Bin 3351: 203 of cap free
Amount of items: 2
Items: 
Size: 532162 Color: 4
Size: 467636 Color: 1

Bin 3352: 203 of cap free
Amount of items: 2
Items: 
Size: 598949 Color: 2
Size: 400849 Color: 3

Bin 3353: 203 of cap free
Amount of items: 2
Items: 
Size: 711106 Color: 3
Size: 288692 Color: 0

Bin 3354: 204 of cap free
Amount of items: 2
Items: 
Size: 552930 Color: 3
Size: 446867 Color: 1

Bin 3355: 204 of cap free
Amount of items: 2
Items: 
Size: 513967 Color: 1
Size: 485830 Color: 3

Bin 3356: 204 of cap free
Amount of items: 2
Items: 
Size: 545769 Color: 1
Size: 454028 Color: 4

Bin 3357: 206 of cap free
Amount of items: 2
Items: 
Size: 639194 Color: 2
Size: 360601 Color: 4

Bin 3358: 206 of cap free
Amount of items: 2
Items: 
Size: 649940 Color: 4
Size: 349855 Color: 2

Bin 3359: 206 of cap free
Amount of items: 2
Items: 
Size: 530803 Color: 4
Size: 468992 Color: 2

Bin 3360: 206 of cap free
Amount of items: 2
Items: 
Size: 638366 Color: 0
Size: 361429 Color: 1

Bin 3361: 207 of cap free
Amount of items: 2
Items: 
Size: 700760 Color: 4
Size: 299034 Color: 2

Bin 3362: 208 of cap free
Amount of items: 2
Items: 
Size: 562670 Color: 3
Size: 437123 Color: 0

Bin 3363: 208 of cap free
Amount of items: 2
Items: 
Size: 715416 Color: 4
Size: 284377 Color: 1

Bin 3364: 208 of cap free
Amount of items: 2
Items: 
Size: 708209 Color: 4
Size: 291584 Color: 2

Bin 3365: 208 of cap free
Amount of items: 2
Items: 
Size: 560924 Color: 1
Size: 438869 Color: 3

Bin 3366: 208 of cap free
Amount of items: 2
Items: 
Size: 703575 Color: 1
Size: 296218 Color: 2

Bin 3367: 209 of cap free
Amount of items: 2
Items: 
Size: 614157 Color: 1
Size: 385635 Color: 4

Bin 3368: 209 of cap free
Amount of items: 2
Items: 
Size: 634035 Color: 1
Size: 365757 Color: 3

Bin 3369: 210 of cap free
Amount of items: 2
Items: 
Size: 758249 Color: 2
Size: 241542 Color: 0

Bin 3370: 210 of cap free
Amount of items: 2
Items: 
Size: 774876 Color: 4
Size: 224915 Color: 3

Bin 3371: 211 of cap free
Amount of items: 2
Items: 
Size: 702001 Color: 1
Size: 297789 Color: 3

Bin 3372: 211 of cap free
Amount of items: 2
Items: 
Size: 728631 Color: 3
Size: 271159 Color: 2

Bin 3373: 211 of cap free
Amount of items: 2
Items: 
Size: 543544 Color: 2
Size: 456246 Color: 0

Bin 3374: 212 of cap free
Amount of items: 2
Items: 
Size: 784749 Color: 0
Size: 215040 Color: 1

Bin 3375: 212 of cap free
Amount of items: 2
Items: 
Size: 773258 Color: 3
Size: 226531 Color: 1

Bin 3376: 212 of cap free
Amount of items: 2
Items: 
Size: 607721 Color: 1
Size: 392068 Color: 3

Bin 3377: 212 of cap free
Amount of items: 2
Items: 
Size: 539859 Color: 4
Size: 459930 Color: 1

Bin 3378: 213 of cap free
Amount of items: 2
Items: 
Size: 527742 Color: 3
Size: 472046 Color: 4

Bin 3379: 213 of cap free
Amount of items: 2
Items: 
Size: 740156 Color: 0
Size: 259632 Color: 4

Bin 3380: 213 of cap free
Amount of items: 2
Items: 
Size: 580607 Color: 3
Size: 419181 Color: 1

Bin 3381: 214 of cap free
Amount of items: 2
Items: 
Size: 777914 Color: 0
Size: 221873 Color: 1

Bin 3382: 214 of cap free
Amount of items: 2
Items: 
Size: 608582 Color: 1
Size: 391205 Color: 3

Bin 3383: 214 of cap free
Amount of items: 2
Items: 
Size: 617393 Color: 3
Size: 382394 Color: 2

Bin 3384: 215 of cap free
Amount of items: 2
Items: 
Size: 604182 Color: 2
Size: 395604 Color: 4

Bin 3385: 215 of cap free
Amount of items: 2
Items: 
Size: 761523 Color: 0
Size: 238263 Color: 3

Bin 3386: 215 of cap free
Amount of items: 2
Items: 
Size: 676828 Color: 2
Size: 322958 Color: 0

Bin 3387: 217 of cap free
Amount of items: 2
Items: 
Size: 645241 Color: 3
Size: 354543 Color: 1

Bin 3388: 217 of cap free
Amount of items: 2
Items: 
Size: 547377 Color: 4
Size: 452407 Color: 3

Bin 3389: 217 of cap free
Amount of items: 2
Items: 
Size: 626965 Color: 2
Size: 372819 Color: 1

Bin 3390: 217 of cap free
Amount of items: 2
Items: 
Size: 546695 Color: 3
Size: 453089 Color: 4

Bin 3391: 217 of cap free
Amount of items: 2
Items: 
Size: 590947 Color: 0
Size: 408837 Color: 1

Bin 3392: 218 of cap free
Amount of items: 2
Items: 
Size: 734133 Color: 4
Size: 265650 Color: 1

Bin 3393: 218 of cap free
Amount of items: 2
Items: 
Size: 724644 Color: 2
Size: 275139 Color: 4

Bin 3394: 218 of cap free
Amount of items: 2
Items: 
Size: 518928 Color: 4
Size: 480855 Color: 3

Bin 3395: 218 of cap free
Amount of items: 2
Items: 
Size: 566273 Color: 2
Size: 433510 Color: 1

Bin 3396: 219 of cap free
Amount of items: 2
Items: 
Size: 780104 Color: 0
Size: 219678 Color: 1

Bin 3397: 219 of cap free
Amount of items: 2
Items: 
Size: 630294 Color: 4
Size: 369488 Color: 1

Bin 3398: 219 of cap free
Amount of items: 2
Items: 
Size: 716046 Color: 0
Size: 283736 Color: 1

Bin 3399: 220 of cap free
Amount of items: 2
Items: 
Size: 508770 Color: 1
Size: 491011 Color: 2

Bin 3400: 220 of cap free
Amount of items: 2
Items: 
Size: 789370 Color: 0
Size: 210411 Color: 4

Bin 3401: 220 of cap free
Amount of items: 2
Items: 
Size: 707645 Color: 0
Size: 292136 Color: 2

Bin 3402: 221 of cap free
Amount of items: 2
Items: 
Size: 760630 Color: 2
Size: 239150 Color: 4

Bin 3403: 221 of cap free
Amount of items: 2
Items: 
Size: 690916 Color: 1
Size: 308864 Color: 0

Bin 3404: 222 of cap free
Amount of items: 2
Items: 
Size: 530025 Color: 0
Size: 469754 Color: 3

Bin 3405: 223 of cap free
Amount of items: 2
Items: 
Size: 545229 Color: 4
Size: 454549 Color: 0

Bin 3406: 223 of cap free
Amount of items: 2
Items: 
Size: 778947 Color: 0
Size: 220831 Color: 1

Bin 3407: 223 of cap free
Amount of items: 2
Items: 
Size: 725709 Color: 3
Size: 274069 Color: 0

Bin 3408: 223 of cap free
Amount of items: 2
Items: 
Size: 510594 Color: 3
Size: 489184 Color: 0

Bin 3409: 224 of cap free
Amount of items: 3
Items: 
Size: 639626 Color: 3
Size: 180076 Color: 2
Size: 180075 Color: 0

Bin 3410: 224 of cap free
Amount of items: 2
Items: 
Size: 692724 Color: 1
Size: 307053 Color: 2

Bin 3411: 224 of cap free
Amount of items: 2
Items: 
Size: 744583 Color: 3
Size: 255194 Color: 4

Bin 3412: 224 of cap free
Amount of items: 2
Items: 
Size: 645966 Color: 0
Size: 353811 Color: 1

Bin 3413: 224 of cap free
Amount of items: 2
Items: 
Size: 682122 Color: 0
Size: 317655 Color: 1

Bin 3414: 225 of cap free
Amount of items: 2
Items: 
Size: 514888 Color: 2
Size: 484888 Color: 3

Bin 3415: 225 of cap free
Amount of items: 2
Items: 
Size: 667146 Color: 0
Size: 332630 Color: 3

Bin 3416: 225 of cap free
Amount of items: 2
Items: 
Size: 677543 Color: 3
Size: 322233 Color: 4

Bin 3417: 226 of cap free
Amount of items: 2
Items: 
Size: 794027 Color: 2
Size: 205748 Color: 1

Bin 3418: 226 of cap free
Amount of items: 2
Items: 
Size: 597340 Color: 2
Size: 402435 Color: 3

Bin 3419: 226 of cap free
Amount of items: 2
Items: 
Size: 564840 Color: 1
Size: 434935 Color: 3

Bin 3420: 227 of cap free
Amount of items: 2
Items: 
Size: 633472 Color: 0
Size: 366302 Color: 2

Bin 3421: 227 of cap free
Amount of items: 2
Items: 
Size: 538225 Color: 4
Size: 461549 Color: 2

Bin 3422: 227 of cap free
Amount of items: 2
Items: 
Size: 654393 Color: 1
Size: 345381 Color: 4

Bin 3423: 227 of cap free
Amount of items: 2
Items: 
Size: 698127 Color: 4
Size: 301647 Color: 1

Bin 3424: 227 of cap free
Amount of items: 2
Items: 
Size: 550258 Color: 1
Size: 449516 Color: 0

Bin 3425: 228 of cap free
Amount of items: 2
Items: 
Size: 775606 Color: 4
Size: 224167 Color: 0

Bin 3426: 228 of cap free
Amount of items: 2
Items: 
Size: 659633 Color: 1
Size: 340140 Color: 3

Bin 3427: 228 of cap free
Amount of items: 2
Items: 
Size: 531745 Color: 3
Size: 468028 Color: 1

Bin 3428: 228 of cap free
Amount of items: 2
Items: 
Size: 614734 Color: 1
Size: 385039 Color: 4

Bin 3429: 229 of cap free
Amount of items: 2
Items: 
Size: 547352 Color: 0
Size: 452420 Color: 4

Bin 3430: 229 of cap free
Amount of items: 2
Items: 
Size: 641763 Color: 0
Size: 358009 Color: 4

Bin 3431: 229 of cap free
Amount of items: 2
Items: 
Size: 553658 Color: 4
Size: 446114 Color: 2

Bin 3432: 230 of cap free
Amount of items: 2
Items: 
Size: 696885 Color: 0
Size: 302886 Color: 4

Bin 3433: 231 of cap free
Amount of items: 2
Items: 
Size: 677517 Color: 1
Size: 322253 Color: 3

Bin 3434: 232 of cap free
Amount of items: 2
Items: 
Size: 693677 Color: 2
Size: 306092 Color: 0

Bin 3435: 233 of cap free
Amount of items: 2
Items: 
Size: 714937 Color: 0
Size: 284831 Color: 3

Bin 3436: 233 of cap free
Amount of items: 2
Items: 
Size: 595011 Color: 1
Size: 404757 Color: 4

Bin 3437: 233 of cap free
Amount of items: 2
Items: 
Size: 537269 Color: 2
Size: 462499 Color: 3

Bin 3438: 233 of cap free
Amount of items: 2
Items: 
Size: 638762 Color: 2
Size: 361006 Color: 3

Bin 3439: 234 of cap free
Amount of items: 2
Items: 
Size: 666560 Color: 3
Size: 333207 Color: 0

Bin 3440: 234 of cap free
Amount of items: 2
Items: 
Size: 575053 Color: 4
Size: 424714 Color: 1

Bin 3441: 234 of cap free
Amount of items: 2
Items: 
Size: 521357 Color: 4
Size: 478410 Color: 2

Bin 3442: 234 of cap free
Amount of items: 2
Items: 
Size: 596106 Color: 3
Size: 403661 Color: 1

Bin 3443: 235 of cap free
Amount of items: 2
Items: 
Size: 566927 Color: 3
Size: 432839 Color: 1

Bin 3444: 235 of cap free
Amount of items: 2
Items: 
Size: 590509 Color: 1
Size: 409257 Color: 2

Bin 3445: 236 of cap free
Amount of items: 2
Items: 
Size: 729462 Color: 1
Size: 270303 Color: 3

Bin 3446: 236 of cap free
Amount of items: 2
Items: 
Size: 628768 Color: 2
Size: 370997 Color: 4

Bin 3447: 236 of cap free
Amount of items: 2
Items: 
Size: 768837 Color: 2
Size: 230928 Color: 1

Bin 3448: 237 of cap free
Amount of items: 2
Items: 
Size: 537810 Color: 1
Size: 461954 Color: 2

Bin 3449: 237 of cap free
Amount of items: 2
Items: 
Size: 588890 Color: 3
Size: 410874 Color: 0

Bin 3450: 238 of cap free
Amount of items: 2
Items: 
Size: 532286 Color: 1
Size: 467477 Color: 2

Bin 3451: 238 of cap free
Amount of items: 2
Items: 
Size: 512605 Color: 1
Size: 487158 Color: 2

Bin 3452: 240 of cap free
Amount of items: 2
Items: 
Size: 636549 Color: 1
Size: 363212 Color: 4

Bin 3453: 241 of cap free
Amount of items: 2
Items: 
Size: 756520 Color: 2
Size: 243240 Color: 0

Bin 3454: 241 of cap free
Amount of items: 2
Items: 
Size: 563074 Color: 3
Size: 436686 Color: 4

Bin 3455: 241 of cap free
Amount of items: 2
Items: 
Size: 659712 Color: 0
Size: 340048 Color: 1

Bin 3456: 241 of cap free
Amount of items: 2
Items: 
Size: 679382 Color: 1
Size: 320378 Color: 4

Bin 3457: 242 of cap free
Amount of items: 2
Items: 
Size: 788271 Color: 2
Size: 211488 Color: 1

Bin 3458: 242 of cap free
Amount of items: 2
Items: 
Size: 685544 Color: 3
Size: 314215 Color: 4

Bin 3459: 242 of cap free
Amount of items: 2
Items: 
Size: 718626 Color: 4
Size: 281133 Color: 1

Bin 3460: 244 of cap free
Amount of items: 2
Items: 
Size: 567245 Color: 1
Size: 432512 Color: 0

Bin 3461: 244 of cap free
Amount of items: 2
Items: 
Size: 763625 Color: 3
Size: 236132 Color: 2

Bin 3462: 245 of cap free
Amount of items: 2
Items: 
Size: 790523 Color: 0
Size: 209233 Color: 2

Bin 3463: 246 of cap free
Amount of items: 2
Items: 
Size: 565174 Color: 4
Size: 434581 Color: 1

Bin 3464: 249 of cap free
Amount of items: 2
Items: 
Size: 720226 Color: 2
Size: 279526 Color: 1

Bin 3465: 249 of cap free
Amount of items: 2
Items: 
Size: 526948 Color: 2
Size: 472804 Color: 3

Bin 3466: 249 of cap free
Amount of items: 2
Items: 
Size: 627930 Color: 0
Size: 371822 Color: 1

Bin 3467: 249 of cap free
Amount of items: 2
Items: 
Size: 745853 Color: 3
Size: 253899 Color: 1

Bin 3468: 250 of cap free
Amount of items: 2
Items: 
Size: 522599 Color: 0
Size: 477152 Color: 1

Bin 3469: 250 of cap free
Amount of items: 2
Items: 
Size: 656159 Color: 0
Size: 343592 Color: 4

Bin 3470: 250 of cap free
Amount of items: 2
Items: 
Size: 701960 Color: 3
Size: 297791 Color: 1

Bin 3471: 250 of cap free
Amount of items: 2
Items: 
Size: 712642 Color: 4
Size: 287109 Color: 1

Bin 3472: 251 of cap free
Amount of items: 2
Items: 
Size: 559219 Color: 4
Size: 440531 Color: 2

Bin 3473: 251 of cap free
Amount of items: 2
Items: 
Size: 546010 Color: 3
Size: 453740 Color: 2

Bin 3474: 251 of cap free
Amount of items: 2
Items: 
Size: 630663 Color: 3
Size: 369087 Color: 4

Bin 3475: 251 of cap free
Amount of items: 2
Items: 
Size: 744179 Color: 4
Size: 255571 Color: 2

Bin 3476: 252 of cap free
Amount of items: 2
Items: 
Size: 661147 Color: 0
Size: 338602 Color: 3

Bin 3477: 252 of cap free
Amount of items: 2
Items: 
Size: 764354 Color: 2
Size: 235395 Color: 0

Bin 3478: 252 of cap free
Amount of items: 2
Items: 
Size: 686233 Color: 1
Size: 313516 Color: 4

Bin 3479: 253 of cap free
Amount of items: 2
Items: 
Size: 535518 Color: 1
Size: 464230 Color: 2

Bin 3480: 253 of cap free
Amount of items: 2
Items: 
Size: 673682 Color: 0
Size: 326066 Color: 2

Bin 3481: 253 of cap free
Amount of items: 2
Items: 
Size: 562650 Color: 2
Size: 437098 Color: 4

Bin 3482: 253 of cap free
Amount of items: 2
Items: 
Size: 507255 Color: 2
Size: 492493 Color: 1

Bin 3483: 254 of cap free
Amount of items: 2
Items: 
Size: 561461 Color: 3
Size: 438286 Color: 1

Bin 3484: 255 of cap free
Amount of items: 2
Items: 
Size: 654689 Color: 1
Size: 345057 Color: 3

Bin 3485: 255 of cap free
Amount of items: 2
Items: 
Size: 638740 Color: 4
Size: 361006 Color: 2

Bin 3486: 256 of cap free
Amount of items: 2
Items: 
Size: 644330 Color: 3
Size: 355415 Color: 4

Bin 3487: 256 of cap free
Amount of items: 2
Items: 
Size: 557969 Color: 4
Size: 441776 Color: 0

Bin 3488: 257 of cap free
Amount of items: 2
Items: 
Size: 681190 Color: 0
Size: 318554 Color: 2

Bin 3489: 257 of cap free
Amount of items: 2
Items: 
Size: 538205 Color: 0
Size: 461539 Color: 2

Bin 3490: 257 of cap free
Amount of items: 2
Items: 
Size: 711928 Color: 3
Size: 287816 Color: 1

Bin 3491: 257 of cap free
Amount of items: 2
Items: 
Size: 761483 Color: 4
Size: 238261 Color: 3

Bin 3492: 258 of cap free
Amount of items: 2
Items: 
Size: 734049 Color: 1
Size: 265694 Color: 4

Bin 3493: 258 of cap free
Amount of items: 2
Items: 
Size: 541496 Color: 3
Size: 458247 Color: 1

Bin 3494: 259 of cap free
Amount of items: 2
Items: 
Size: 712652 Color: 1
Size: 287090 Color: 0

Bin 3495: 260 of cap free
Amount of items: 2
Items: 
Size: 532828 Color: 3
Size: 466913 Color: 4

Bin 3496: 260 of cap free
Amount of items: 2
Items: 
Size: 746402 Color: 0
Size: 253339 Color: 2

Bin 3497: 261 of cap free
Amount of items: 2
Items: 
Size: 649971 Color: 2
Size: 349769 Color: 1

Bin 3498: 261 of cap free
Amount of items: 2
Items: 
Size: 677521 Color: 3
Size: 322219 Color: 4

Bin 3499: 262 of cap free
Amount of items: 2
Items: 
Size: 567966 Color: 1
Size: 431773 Color: 0

Bin 3500: 262 of cap free
Amount of items: 2
Items: 
Size: 758480 Color: 4
Size: 241259 Color: 2

Bin 3501: 263 of cap free
Amount of items: 2
Items: 
Size: 499976 Color: 3
Size: 499762 Color: 4

Bin 3502: 263 of cap free
Amount of items: 2
Items: 
Size: 553984 Color: 1
Size: 445754 Color: 2

Bin 3503: 263 of cap free
Amount of items: 2
Items: 
Size: 668729 Color: 0
Size: 331009 Color: 1

Bin 3504: 264 of cap free
Amount of items: 2
Items: 
Size: 506031 Color: 3
Size: 493706 Color: 1

Bin 3505: 265 of cap free
Amount of items: 2
Items: 
Size: 514238 Color: 3
Size: 485498 Color: 0

Bin 3506: 265 of cap free
Amount of items: 2
Items: 
Size: 508248 Color: 2
Size: 491488 Color: 0

Bin 3507: 265 of cap free
Amount of items: 2
Items: 
Size: 527457 Color: 3
Size: 472279 Color: 2

Bin 3508: 265 of cap free
Amount of items: 2
Items: 
Size: 631805 Color: 1
Size: 367931 Color: 4

Bin 3509: 266 of cap free
Amount of items: 2
Items: 
Size: 772194 Color: 4
Size: 227541 Color: 3

Bin 3510: 266 of cap free
Amount of items: 2
Items: 
Size: 696436 Color: 0
Size: 303299 Color: 4

Bin 3511: 266 of cap free
Amount of items: 2
Items: 
Size: 521329 Color: 4
Size: 478406 Color: 3

Bin 3512: 266 of cap free
Amount of items: 2
Items: 
Size: 780103 Color: 1
Size: 219632 Color: 3

Bin 3513: 266 of cap free
Amount of items: 2
Items: 
Size: 683943 Color: 1
Size: 315792 Color: 2

Bin 3514: 267 of cap free
Amount of items: 2
Items: 
Size: 662137 Color: 1
Size: 337597 Color: 0

Bin 3515: 267 of cap free
Amount of items: 2
Items: 
Size: 533493 Color: 0
Size: 466241 Color: 1

Bin 3516: 267 of cap free
Amount of items: 2
Items: 
Size: 791585 Color: 3
Size: 208149 Color: 0

Bin 3517: 267 of cap free
Amount of items: 2
Items: 
Size: 751985 Color: 0
Size: 247749 Color: 2

Bin 3518: 267 of cap free
Amount of items: 2
Items: 
Size: 613302 Color: 0
Size: 386432 Color: 1

Bin 3519: 268 of cap free
Amount of items: 2
Items: 
Size: 626119 Color: 1
Size: 373614 Color: 2

Bin 3520: 269 of cap free
Amount of items: 2
Items: 
Size: 718619 Color: 4
Size: 281113 Color: 3

Bin 3521: 269 of cap free
Amount of items: 2
Items: 
Size: 673208 Color: 1
Size: 326524 Color: 3

Bin 3522: 270 of cap free
Amount of items: 2
Items: 
Size: 569958 Color: 3
Size: 429773 Color: 0

Bin 3523: 272 of cap free
Amount of items: 2
Items: 
Size: 658265 Color: 3
Size: 341464 Color: 1

Bin 3524: 272 of cap free
Amount of items: 2
Items: 
Size: 799631 Color: 2
Size: 200098 Color: 4

Bin 3525: 272 of cap free
Amount of items: 2
Items: 
Size: 676104 Color: 1
Size: 323625 Color: 2

Bin 3526: 273 of cap free
Amount of items: 2
Items: 
Size: 505168 Color: 3
Size: 494560 Color: 1

Bin 3527: 274 of cap free
Amount of items: 2
Items: 
Size: 630661 Color: 0
Size: 369066 Color: 4

Bin 3528: 274 of cap free
Amount of items: 2
Items: 
Size: 594986 Color: 1
Size: 404741 Color: 4

Bin 3529: 275 of cap free
Amount of items: 2
Items: 
Size: 672598 Color: 3
Size: 327128 Color: 0

Bin 3530: 275 of cap free
Amount of items: 2
Items: 
Size: 548601 Color: 4
Size: 451125 Color: 2

Bin 3531: 277 of cap free
Amount of items: 2
Items: 
Size: 631049 Color: 3
Size: 368675 Color: 0

Bin 3532: 277 of cap free
Amount of items: 2
Items: 
Size: 755851 Color: 2
Size: 243873 Color: 0

Bin 3533: 277 of cap free
Amount of items: 2
Items: 
Size: 591574 Color: 2
Size: 408150 Color: 3

Bin 3534: 279 of cap free
Amount of items: 2
Items: 
Size: 558517 Color: 3
Size: 441205 Color: 2

Bin 3535: 279 of cap free
Amount of items: 2
Items: 
Size: 532152 Color: 4
Size: 467570 Color: 1

Bin 3536: 279 of cap free
Amount of items: 2
Items: 
Size: 551193 Color: 4
Size: 448529 Color: 2

Bin 3537: 279 of cap free
Amount of items: 2
Items: 
Size: 591919 Color: 0
Size: 407803 Color: 1

Bin 3538: 279 of cap free
Amount of items: 2
Items: 
Size: 601191 Color: 1
Size: 398531 Color: 2

Bin 3539: 279 of cap free
Amount of items: 2
Items: 
Size: 503574 Color: 4
Size: 496148 Color: 2

Bin 3540: 280 of cap free
Amount of items: 2
Items: 
Size: 737799 Color: 1
Size: 261922 Color: 4

Bin 3541: 280 of cap free
Amount of items: 2
Items: 
Size: 519925 Color: 4
Size: 479796 Color: 2

Bin 3542: 281 of cap free
Amount of items: 2
Items: 
Size: 572219 Color: 3
Size: 427501 Color: 2

Bin 3543: 281 of cap free
Amount of items: 2
Items: 
Size: 685016 Color: 3
Size: 314704 Color: 4

Bin 3544: 281 of cap free
Amount of items: 2
Items: 
Size: 767018 Color: 0
Size: 232702 Color: 1

Bin 3545: 282 of cap free
Amount of items: 2
Items: 
Size: 724300 Color: 4
Size: 275419 Color: 3

Bin 3546: 282 of cap free
Amount of items: 2
Items: 
Size: 723307 Color: 4
Size: 276412 Color: 1

Bin 3547: 282 of cap free
Amount of items: 2
Items: 
Size: 517531 Color: 4
Size: 482188 Color: 1

Bin 3548: 282 of cap free
Amount of items: 2
Items: 
Size: 606907 Color: 0
Size: 392812 Color: 1

Bin 3549: 283 of cap free
Amount of items: 2
Items: 
Size: 602412 Color: 3
Size: 397306 Color: 0

Bin 3550: 283 of cap free
Amount of items: 2
Items: 
Size: 640442 Color: 4
Size: 359276 Color: 0

Bin 3551: 284 of cap free
Amount of items: 2
Items: 
Size: 620287 Color: 2
Size: 379430 Color: 4

Bin 3552: 284 of cap free
Amount of items: 2
Items: 
Size: 509835 Color: 0
Size: 489882 Color: 1

Bin 3553: 284 of cap free
Amount of items: 2
Items: 
Size: 560354 Color: 1
Size: 439363 Color: 3

Bin 3554: 285 of cap free
Amount of items: 2
Items: 
Size: 728580 Color: 4
Size: 271136 Color: 0

Bin 3555: 285 of cap free
Amount of items: 2
Items: 
Size: 612578 Color: 3
Size: 387138 Color: 1

Bin 3556: 285 of cap free
Amount of items: 2
Items: 
Size: 545224 Color: 1
Size: 454492 Color: 3

Bin 3557: 286 of cap free
Amount of items: 2
Items: 
Size: 711455 Color: 3
Size: 288260 Color: 2

Bin 3558: 287 of cap free
Amount of items: 2
Items: 
Size: 763890 Color: 4
Size: 235824 Color: 2

Bin 3559: 287 of cap free
Amount of items: 2
Items: 
Size: 781610 Color: 0
Size: 218104 Color: 4

Bin 3560: 287 of cap free
Amount of items: 2
Items: 
Size: 559796 Color: 3
Size: 439918 Color: 1

Bin 3561: 287 of cap free
Amount of items: 2
Items: 
Size: 530804 Color: 2
Size: 468910 Color: 1

Bin 3562: 288 of cap free
Amount of items: 2
Items: 
Size: 689136 Color: 4
Size: 310577 Color: 2

Bin 3563: 289 of cap free
Amount of items: 2
Items: 
Size: 783329 Color: 1
Size: 216383 Color: 4

Bin 3564: 289 of cap free
Amount of items: 2
Items: 
Size: 528315 Color: 4
Size: 471397 Color: 2

Bin 3565: 289 of cap free
Amount of items: 2
Items: 
Size: 586965 Color: 4
Size: 412747 Color: 2

Bin 3566: 289 of cap free
Amount of items: 2
Items: 
Size: 543524 Color: 1
Size: 456188 Color: 0

Bin 3567: 289 of cap free
Amount of items: 2
Items: 
Size: 607664 Color: 2
Size: 392048 Color: 3

Bin 3568: 290 of cap free
Amount of items: 2
Items: 
Size: 654408 Color: 4
Size: 345303 Color: 1

Bin 3569: 292 of cap free
Amount of items: 2
Items: 
Size: 799564 Color: 1
Size: 200145 Color: 2

Bin 3570: 292 of cap free
Amount of items: 2
Items: 
Size: 642255 Color: 1
Size: 357454 Color: 4

Bin 3571: 292 of cap free
Amount of items: 2
Items: 
Size: 509404 Color: 1
Size: 490305 Color: 0

Bin 3572: 294 of cap free
Amount of items: 2
Items: 
Size: 776561 Color: 4
Size: 223146 Color: 0

Bin 3573: 295 of cap free
Amount of items: 2
Items: 
Size: 790522 Color: 3
Size: 209184 Color: 4

Bin 3574: 295 of cap free
Amount of items: 2
Items: 
Size: 579662 Color: 2
Size: 420044 Color: 1

Bin 3575: 296 of cap free
Amount of items: 2
Items: 
Size: 663531 Color: 2
Size: 336174 Color: 3

Bin 3576: 297 of cap free
Amount of items: 2
Items: 
Size: 540497 Color: 4
Size: 459207 Color: 1

Bin 3577: 297 of cap free
Amount of items: 2
Items: 
Size: 581563 Color: 1
Size: 418141 Color: 4

Bin 3578: 298 of cap free
Amount of items: 2
Items: 
Size: 508194 Color: 0
Size: 491509 Color: 2

Bin 3579: 298 of cap free
Amount of items: 2
Items: 
Size: 688265 Color: 3
Size: 311438 Color: 0

Bin 3580: 298 of cap free
Amount of items: 2
Items: 
Size: 722425 Color: 1
Size: 277278 Color: 3

Bin 3581: 300 of cap free
Amount of items: 2
Items: 
Size: 689763 Color: 3
Size: 309938 Color: 4

Bin 3582: 301 of cap free
Amount of items: 2
Items: 
Size: 731193 Color: 2
Size: 268507 Color: 1

Bin 3583: 301 of cap free
Amount of items: 2
Items: 
Size: 557622 Color: 3
Size: 442078 Color: 2

Bin 3584: 302 of cap free
Amount of items: 2
Items: 
Size: 537810 Color: 0
Size: 461889 Color: 2

Bin 3585: 302 of cap free
Amount of items: 2
Items: 
Size: 506633 Color: 3
Size: 493066 Color: 0

Bin 3586: 303 of cap free
Amount of items: 2
Items: 
Size: 661516 Color: 3
Size: 338182 Color: 0

Bin 3587: 304 of cap free
Amount of items: 2
Items: 
Size: 697588 Color: 2
Size: 302109 Color: 3

Bin 3588: 305 of cap free
Amount of items: 2
Items: 
Size: 642655 Color: 4
Size: 357041 Color: 1

Bin 3589: 307 of cap free
Amount of items: 2
Items: 
Size: 518918 Color: 1
Size: 480776 Color: 3

Bin 3590: 307 of cap free
Amount of items: 2
Items: 
Size: 554491 Color: 3
Size: 445203 Color: 2

Bin 3591: 308 of cap free
Amount of items: 2
Items: 
Size: 778779 Color: 2
Size: 220914 Color: 0

Bin 3592: 308 of cap free
Amount of items: 2
Items: 
Size: 618394 Color: 4
Size: 381299 Color: 1

Bin 3593: 309 of cap free
Amount of items: 2
Items: 
Size: 627822 Color: 1
Size: 371870 Color: 0

Bin 3594: 309 of cap free
Amount of items: 2
Items: 
Size: 769876 Color: 0
Size: 229816 Color: 1

Bin 3595: 309 of cap free
Amount of items: 2
Items: 
Size: 559164 Color: 3
Size: 440528 Color: 4

Bin 3596: 309 of cap free
Amount of items: 2
Items: 
Size: 673194 Color: 4
Size: 326498 Color: 1

Bin 3597: 312 of cap free
Amount of items: 2
Items: 
Size: 633703 Color: 0
Size: 365986 Color: 3

Bin 3598: 312 of cap free
Amount of items: 2
Items: 
Size: 502667 Color: 2
Size: 497022 Color: 4

Bin 3599: 312 of cap free
Amount of items: 2
Items: 
Size: 509386 Color: 4
Size: 490303 Color: 0

Bin 3600: 313 of cap free
Amount of items: 2
Items: 
Size: 574371 Color: 1
Size: 425317 Color: 4

Bin 3601: 315 of cap free
Amount of items: 2
Items: 
Size: 512543 Color: 4
Size: 487143 Color: 1

Bin 3602: 315 of cap free
Amount of items: 2
Items: 
Size: 525791 Color: 3
Size: 473895 Color: 0

Bin 3603: 315 of cap free
Amount of items: 2
Items: 
Size: 586522 Color: 0
Size: 413164 Color: 2

Bin 3604: 315 of cap free
Amount of items: 2
Items: 
Size: 696299 Color: 4
Size: 303387 Color: 2

Bin 3605: 315 of cap free
Amount of items: 2
Items: 
Size: 526905 Color: 2
Size: 472781 Color: 1

Bin 3606: 315 of cap free
Amount of items: 2
Items: 
Size: 521977 Color: 3
Size: 477709 Color: 1

Bin 3607: 316 of cap free
Amount of items: 2
Items: 
Size: 588822 Color: 0
Size: 410863 Color: 1

Bin 3608: 316 of cap free
Amount of items: 2
Items: 
Size: 703951 Color: 2
Size: 295734 Color: 4

Bin 3609: 316 of cap free
Amount of items: 2
Items: 
Size: 508994 Color: 4
Size: 490691 Color: 3

Bin 3610: 317 of cap free
Amount of items: 2
Items: 
Size: 677471 Color: 4
Size: 322213 Color: 3

Bin 3611: 318 of cap free
Amount of items: 2
Items: 
Size: 726738 Color: 2
Size: 272945 Color: 4

Bin 3612: 318 of cap free
Amount of items: 2
Items: 
Size: 645179 Color: 4
Size: 354504 Color: 3

Bin 3613: 318 of cap free
Amount of items: 2
Items: 
Size: 777920 Color: 1
Size: 221763 Color: 0

Bin 3614: 319 of cap free
Amount of items: 2
Items: 
Size: 740454 Color: 4
Size: 259228 Color: 0

Bin 3615: 319 of cap free
Amount of items: 2
Items: 
Size: 514823 Color: 4
Size: 484859 Color: 2

Bin 3616: 319 of cap free
Amount of items: 2
Items: 
Size: 744525 Color: 0
Size: 255157 Color: 1

Bin 3617: 320 of cap free
Amount of items: 2
Items: 
Size: 664748 Color: 4
Size: 334933 Color: 2

Bin 3618: 320 of cap free
Amount of items: 2
Items: 
Size: 698969 Color: 4
Size: 300712 Color: 2

Bin 3619: 320 of cap free
Amount of items: 2
Items: 
Size: 617338 Color: 1
Size: 382343 Color: 0

Bin 3620: 321 of cap free
Amount of items: 2
Items: 
Size: 576395 Color: 1
Size: 423285 Color: 4

Bin 3621: 322 of cap free
Amount of items: 2
Items: 
Size: 613590 Color: 4
Size: 386089 Color: 1

Bin 3622: 323 of cap free
Amount of items: 2
Items: 
Size: 586579 Color: 2
Size: 413099 Color: 1

Bin 3623: 323 of cap free
Amount of items: 2
Items: 
Size: 706542 Color: 3
Size: 293136 Color: 1

Bin 3624: 323 of cap free
Amount of items: 2
Items: 
Size: 622864 Color: 3
Size: 376814 Color: 1

Bin 3625: 323 of cap free
Amount of items: 2
Items: 
Size: 572201 Color: 2
Size: 427477 Color: 0

Bin 3626: 323 of cap free
Amount of items: 2
Items: 
Size: 750092 Color: 0
Size: 249586 Color: 2

Bin 3627: 323 of cap free
Amount of items: 2
Items: 
Size: 516168 Color: 3
Size: 483510 Color: 4

Bin 3628: 323 of cap free
Amount of items: 2
Items: 
Size: 721363 Color: 3
Size: 278315 Color: 1

Bin 3629: 324 of cap free
Amount of items: 2
Items: 
Size: 628406 Color: 0
Size: 371271 Color: 3

Bin 3630: 324 of cap free
Amount of items: 2
Items: 
Size: 516547 Color: 3
Size: 483130 Color: 2

Bin 3631: 324 of cap free
Amount of items: 2
Items: 
Size: 751132 Color: 2
Size: 248545 Color: 0

Bin 3632: 325 of cap free
Amount of items: 2
Items: 
Size: 741482 Color: 4
Size: 258194 Color: 1

Bin 3633: 326 of cap free
Amount of items: 2
Items: 
Size: 546594 Color: 0
Size: 453081 Color: 4

Bin 3634: 326 of cap free
Amount of items: 2
Items: 
Size: 644646 Color: 3
Size: 355029 Color: 2

Bin 3635: 327 of cap free
Amount of items: 2
Items: 
Size: 731067 Color: 1
Size: 268607 Color: 4

Bin 3636: 327 of cap free
Amount of items: 2
Items: 
Size: 791527 Color: 0
Size: 208147 Color: 4

Bin 3637: 327 of cap free
Amount of items: 2
Items: 
Size: 563311 Color: 4
Size: 436363 Color: 1

Bin 3638: 327 of cap free
Amount of items: 2
Items: 
Size: 743114 Color: 3
Size: 256560 Color: 2

Bin 3639: 327 of cap free
Amount of items: 2
Items: 
Size: 788882 Color: 2
Size: 210792 Color: 3

Bin 3640: 328 of cap free
Amount of items: 2
Items: 
Size: 641041 Color: 4
Size: 358632 Color: 2

Bin 3641: 331 of cap free
Amount of items: 2
Items: 
Size: 766940 Color: 2
Size: 232730 Color: 0

Bin 3642: 331 of cap free
Amount of items: 2
Items: 
Size: 792343 Color: 3
Size: 207327 Color: 1

Bin 3643: 331 of cap free
Amount of items: 2
Items: 
Size: 766452 Color: 0
Size: 233218 Color: 3

Bin 3644: 331 of cap free
Amount of items: 2
Items: 
Size: 601156 Color: 3
Size: 398514 Color: 4

Bin 3645: 332 of cap free
Amount of items: 2
Items: 
Size: 525004 Color: 4
Size: 474665 Color: 2

Bin 3646: 333 of cap free
Amount of items: 2
Items: 
Size: 525791 Color: 3
Size: 473877 Color: 2

Bin 3647: 334 of cap free
Amount of items: 2
Items: 
Size: 692083 Color: 0
Size: 307584 Color: 1

Bin 3648: 334 of cap free
Amount of items: 2
Items: 
Size: 542106 Color: 4
Size: 457561 Color: 3

Bin 3649: 335 of cap free
Amount of items: 2
Items: 
Size: 678559 Color: 1
Size: 321107 Color: 3

Bin 3650: 335 of cap free
Amount of items: 2
Items: 
Size: 623736 Color: 1
Size: 375930 Color: 0

Bin 3651: 335 of cap free
Amount of items: 2
Items: 
Size: 716311 Color: 0
Size: 283355 Color: 4

Bin 3652: 335 of cap free
Amount of items: 2
Items: 
Size: 525771 Color: 4
Size: 473895 Color: 3

Bin 3653: 336 of cap free
Amount of items: 2
Items: 
Size: 634255 Color: 3
Size: 365410 Color: 1

Bin 3654: 336 of cap free
Amount of items: 2
Items: 
Size: 673660 Color: 0
Size: 326005 Color: 2

Bin 3655: 337 of cap free
Amount of items: 2
Items: 
Size: 782048 Color: 3
Size: 217616 Color: 2

Bin 3656: 337 of cap free
Amount of items: 2
Items: 
Size: 722413 Color: 1
Size: 277251 Color: 0

Bin 3657: 339 of cap free
Amount of items: 2
Items: 
Size: 520961 Color: 4
Size: 478701 Color: 2

Bin 3658: 341 of cap free
Amount of items: 2
Items: 
Size: 667066 Color: 1
Size: 332594 Color: 4

Bin 3659: 341 of cap free
Amount of items: 2
Items: 
Size: 708255 Color: 2
Size: 291405 Color: 3

Bin 3660: 342 of cap free
Amount of items: 2
Items: 
Size: 599515 Color: 4
Size: 400144 Color: 0

Bin 3661: 342 of cap free
Amount of items: 2
Items: 
Size: 621931 Color: 0
Size: 377728 Color: 2

Bin 3662: 343 of cap free
Amount of items: 2
Items: 
Size: 694199 Color: 2
Size: 305459 Color: 4

Bin 3663: 343 of cap free
Amount of items: 2
Items: 
Size: 753859 Color: 0
Size: 245799 Color: 4

Bin 3664: 343 of cap free
Amount of items: 2
Items: 
Size: 528681 Color: 4
Size: 470977 Color: 2

Bin 3665: 344 of cap free
Amount of items: 2
Items: 
Size: 690795 Color: 0
Size: 308862 Color: 1

Bin 3666: 345 of cap free
Amount of items: 2
Items: 
Size: 576445 Color: 4
Size: 423211 Color: 0

Bin 3667: 345 of cap free
Amount of items: 2
Items: 
Size: 519275 Color: 3
Size: 480381 Color: 1

Bin 3668: 345 of cap free
Amount of items: 2
Items: 
Size: 698946 Color: 0
Size: 300710 Color: 3

Bin 3669: 346 of cap free
Amount of items: 2
Items: 
Size: 567236 Color: 4
Size: 432419 Color: 0

Bin 3670: 346 of cap free
Amount of items: 2
Items: 
Size: 535476 Color: 4
Size: 464179 Color: 1

Bin 3671: 347 of cap free
Amount of items: 2
Items: 
Size: 662135 Color: 2
Size: 337519 Color: 4

Bin 3672: 347 of cap free
Amount of items: 2
Items: 
Size: 536616 Color: 3
Size: 463038 Color: 1

Bin 3673: 348 of cap free
Amount of items: 2
Items: 
Size: 590512 Color: 0
Size: 409141 Color: 1

Bin 3674: 348 of cap free
Amount of items: 2
Items: 
Size: 598866 Color: 2
Size: 400787 Color: 3

Bin 3675: 348 of cap free
Amount of items: 2
Items: 
Size: 680637 Color: 4
Size: 319016 Color: 0

Bin 3676: 349 of cap free
Amount of items: 2
Items: 
Size: 625643 Color: 1
Size: 374009 Color: 3

Bin 3677: 349 of cap free
Amount of items: 2
Items: 
Size: 668653 Color: 2
Size: 330999 Color: 3

Bin 3678: 349 of cap free
Amount of items: 2
Items: 
Size: 762329 Color: 4
Size: 237323 Color: 2

Bin 3679: 351 of cap free
Amount of items: 2
Items: 
Size: 508208 Color: 2
Size: 491442 Color: 0

Bin 3680: 353 of cap free
Amount of items: 2
Items: 
Size: 669540 Color: 2
Size: 330108 Color: 3

Bin 3681: 353 of cap free
Amount of items: 2
Items: 
Size: 559208 Color: 4
Size: 440440 Color: 2

Bin 3682: 354 of cap free
Amount of items: 2
Items: 
Size: 531723 Color: 3
Size: 467924 Color: 4

Bin 3683: 356 of cap free
Amount of items: 2
Items: 
Size: 570465 Color: 3
Size: 429180 Color: 1

Bin 3684: 356 of cap free
Amount of items: 2
Items: 
Size: 565884 Color: 0
Size: 433761 Color: 4

Bin 3685: 357 of cap free
Amount of items: 2
Items: 
Size: 738865 Color: 0
Size: 260779 Color: 3

Bin 3686: 357 of cap free
Amount of items: 2
Items: 
Size: 534833 Color: 3
Size: 464811 Color: 1

Bin 3687: 357 of cap free
Amount of items: 2
Items: 
Size: 619203 Color: 3
Size: 380441 Color: 2

Bin 3688: 358 of cap free
Amount of items: 2
Items: 
Size: 575035 Color: 2
Size: 424608 Color: 3

Bin 3689: 360 of cap free
Amount of items: 2
Items: 
Size: 716295 Color: 2
Size: 283346 Color: 1

Bin 3690: 360 of cap free
Amount of items: 2
Items: 
Size: 705991 Color: 0
Size: 293650 Color: 2

Bin 3691: 360 of cap free
Amount of items: 2
Items: 
Size: 499892 Color: 3
Size: 499749 Color: 2

Bin 3692: 361 of cap free
Amount of items: 2
Items: 
Size: 786734 Color: 1
Size: 212906 Color: 2

Bin 3693: 361 of cap free
Amount of items: 2
Items: 
Size: 668652 Color: 2
Size: 330988 Color: 1

Bin 3694: 363 of cap free
Amount of items: 2
Items: 
Size: 545979 Color: 4
Size: 453659 Color: 3

Bin 3695: 364 of cap free
Amount of items: 2
Items: 
Size: 728000 Color: 3
Size: 271637 Color: 2

Bin 3696: 364 of cap free
Amount of items: 2
Items: 
Size: 736747 Color: 3
Size: 262890 Color: 2

Bin 3697: 365 of cap free
Amount of items: 2
Items: 
Size: 678910 Color: 0
Size: 320726 Color: 4

Bin 3698: 366 of cap free
Amount of items: 2
Items: 
Size: 514802 Color: 2
Size: 484833 Color: 0

Bin 3699: 367 of cap free
Amount of items: 2
Items: 
Size: 639043 Color: 4
Size: 360591 Color: 3

Bin 3700: 367 of cap free
Amount of items: 2
Items: 
Size: 702737 Color: 4
Size: 296897 Color: 2

Bin 3701: 368 of cap free
Amount of items: 2
Items: 
Size: 521022 Color: 1
Size: 478611 Color: 4

Bin 3702: 369 of cap free
Amount of items: 2
Items: 
Size: 782525 Color: 0
Size: 217107 Color: 4

Bin 3703: 370 of cap free
Amount of items: 2
Items: 
Size: 644646 Color: 2
Size: 354985 Color: 0

Bin 3704: 371 of cap free
Amount of items: 2
Items: 
Size: 783833 Color: 2
Size: 215797 Color: 0

Bin 3705: 377 of cap free
Amount of items: 2
Items: 
Size: 623785 Color: 0
Size: 375839 Color: 2

Bin 3706: 377 of cap free
Amount of items: 2
Items: 
Size: 643134 Color: 0
Size: 356490 Color: 3

Bin 3707: 377 of cap free
Amount of items: 2
Items: 
Size: 614045 Color: 1
Size: 385579 Color: 4

Bin 3708: 379 of cap free
Amount of items: 2
Items: 
Size: 597225 Color: 4
Size: 402397 Color: 3

Bin 3709: 379 of cap free
Amount of items: 2
Items: 
Size: 784612 Color: 3
Size: 215010 Color: 4

Bin 3710: 381 of cap free
Amount of items: 2
Items: 
Size: 655786 Color: 2
Size: 343834 Color: 0

Bin 3711: 382 of cap free
Amount of items: 2
Items: 
Size: 762920 Color: 3
Size: 236699 Color: 4

Bin 3712: 382 of cap free
Amount of items: 2
Items: 
Size: 746244 Color: 2
Size: 253375 Color: 0

Bin 3713: 382 of cap free
Amount of items: 2
Items: 
Size: 711858 Color: 0
Size: 287761 Color: 3

Bin 3714: 384 of cap free
Amount of items: 2
Items: 
Size: 524394 Color: 3
Size: 475223 Color: 4

Bin 3715: 385 of cap free
Amount of items: 2
Items: 
Size: 586905 Color: 4
Size: 412711 Color: 0

Bin 3716: 385 of cap free
Amount of items: 2
Items: 
Size: 582447 Color: 1
Size: 417169 Color: 0

Bin 3717: 385 of cap free
Amount of items: 2
Items: 
Size: 798444 Color: 2
Size: 201172 Color: 4

Bin 3718: 386 of cap free
Amount of items: 2
Items: 
Size: 630634 Color: 1
Size: 368981 Color: 0

Bin 3719: 388 of cap free
Amount of items: 2
Items: 
Size: 595638 Color: 3
Size: 403975 Color: 4

Bin 3720: 389 of cap free
Amount of items: 2
Items: 
Size: 642649 Color: 4
Size: 356963 Color: 0

Bin 3721: 389 of cap free
Amount of items: 2
Items: 
Size: 779507 Color: 4
Size: 220105 Color: 1

Bin 3722: 391 of cap free
Amount of items: 3
Items: 
Size: 575022 Color: 3
Size: 212337 Color: 0
Size: 212251 Color: 3

Bin 3723: 391 of cap free
Amount of items: 2
Items: 
Size: 587423 Color: 0
Size: 412187 Color: 3

Bin 3724: 393 of cap free
Amount of items: 2
Items: 
Size: 590897 Color: 3
Size: 408711 Color: 1

Bin 3725: 393 of cap free
Amount of items: 2
Items: 
Size: 682651 Color: 2
Size: 316957 Color: 3

Bin 3726: 393 of cap free
Amount of items: 2
Items: 
Size: 692093 Color: 1
Size: 307515 Color: 0

Bin 3727: 395 of cap free
Amount of items: 2
Items: 
Size: 555996 Color: 0
Size: 443610 Color: 4

Bin 3728: 395 of cap free
Amount of items: 2
Items: 
Size: 715871 Color: 2
Size: 283735 Color: 4

Bin 3729: 396 of cap free
Amount of items: 2
Items: 
Size: 615178 Color: 3
Size: 384427 Color: 4

Bin 3730: 396 of cap free
Amount of items: 2
Items: 
Size: 572218 Color: 1
Size: 427387 Color: 2

Bin 3731: 396 of cap free
Amount of items: 2
Items: 
Size: 658649 Color: 4
Size: 340956 Color: 0

Bin 3732: 397 of cap free
Amount of items: 2
Items: 
Size: 703948 Color: 2
Size: 295656 Color: 1

Bin 3733: 397 of cap free
Amount of items: 2
Items: 
Size: 516964 Color: 0
Size: 482640 Color: 1

Bin 3734: 397 of cap free
Amount of items: 2
Items: 
Size: 651990 Color: 2
Size: 347614 Color: 3

Bin 3735: 398 of cap free
Amount of items: 2
Items: 
Size: 552740 Color: 0
Size: 446863 Color: 1

Bin 3736: 398 of cap free
Amount of items: 2
Items: 
Size: 755359 Color: 4
Size: 244244 Color: 1

Bin 3737: 399 of cap free
Amount of items: 2
Items: 
Size: 595493 Color: 4
Size: 404109 Color: 3

Bin 3738: 399 of cap free
Amount of items: 2
Items: 
Size: 667412 Color: 4
Size: 332190 Color: 1

Bin 3739: 403 of cap free
Amount of items: 2
Items: 
Size: 674826 Color: 0
Size: 324772 Color: 1

Bin 3740: 403 of cap free
Amount of items: 2
Items: 
Size: 620204 Color: 1
Size: 379394 Color: 4

Bin 3741: 403 of cap free
Amount of items: 2
Items: 
Size: 546605 Color: 4
Size: 452993 Color: 1

Bin 3742: 404 of cap free
Amount of items: 2
Items: 
Size: 529705 Color: 1
Size: 469892 Color: 0

Bin 3743: 405 of cap free
Amount of items: 2
Items: 
Size: 618311 Color: 3
Size: 381285 Color: 2

Bin 3744: 405 of cap free
Amount of items: 2
Items: 
Size: 689023 Color: 0
Size: 310573 Color: 2

Bin 3745: 405 of cap free
Amount of items: 2
Items: 
Size: 727524 Color: 1
Size: 272072 Color: 0

Bin 3746: 406 of cap free
Amount of items: 2
Items: 
Size: 691207 Color: 4
Size: 308388 Color: 2

Bin 3747: 407 of cap free
Amount of items: 2
Items: 
Size: 672558 Color: 1
Size: 327036 Color: 2

Bin 3748: 407 of cap free
Amount of items: 2
Items: 
Size: 671975 Color: 2
Size: 327619 Color: 3

Bin 3749: 408 of cap free
Amount of items: 2
Items: 
Size: 509371 Color: 3
Size: 490222 Color: 4

Bin 3750: 408 of cap free
Amount of items: 2
Items: 
Size: 712604 Color: 0
Size: 286989 Color: 3

Bin 3751: 409 of cap free
Amount of items: 2
Items: 
Size: 739214 Color: 3
Size: 260378 Color: 2

Bin 3752: 410 of cap free
Amount of items: 2
Items: 
Size: 769813 Color: 1
Size: 229778 Color: 0

Bin 3753: 411 of cap free
Amount of items: 2
Items: 
Size: 753710 Color: 2
Size: 245880 Color: 0

Bin 3754: 412 of cap free
Amount of items: 2
Items: 
Size: 579607 Color: 0
Size: 419982 Color: 1

Bin 3755: 412 of cap free
Amount of items: 2
Items: 
Size: 616045 Color: 4
Size: 383544 Color: 3

Bin 3756: 413 of cap free
Amount of items: 2
Items: 
Size: 588811 Color: 3
Size: 410777 Color: 0

Bin 3757: 413 of cap free
Amount of items: 2
Items: 
Size: 602699 Color: 0
Size: 396889 Color: 4

Bin 3758: 415 of cap free
Amount of items: 2
Items: 
Size: 624561 Color: 0
Size: 375025 Color: 1

Bin 3759: 416 of cap free
Amount of items: 2
Items: 
Size: 724235 Color: 4
Size: 275350 Color: 2

Bin 3760: 416 of cap free
Amount of items: 2
Items: 
Size: 504345 Color: 3
Size: 495240 Color: 0

Bin 3761: 417 of cap free
Amount of items: 2
Items: 
Size: 657842 Color: 4
Size: 341742 Color: 1

Bin 3762: 419 of cap free
Amount of items: 2
Items: 
Size: 794333 Color: 4
Size: 205249 Color: 3

Bin 3763: 421 of cap free
Amount of items: 2
Items: 
Size: 564256 Color: 0
Size: 435324 Color: 3

Bin 3764: 422 of cap free
Amount of items: 2
Items: 
Size: 545977 Color: 2
Size: 453602 Color: 4

Bin 3765: 422 of cap free
Amount of items: 2
Items: 
Size: 550237 Color: 1
Size: 449342 Color: 0

Bin 3766: 424 of cap free
Amount of items: 2
Items: 
Size: 701952 Color: 4
Size: 297625 Color: 1

Bin 3767: 424 of cap free
Amount of items: 2
Items: 
Size: 598803 Color: 4
Size: 400774 Color: 0

Bin 3768: 425 of cap free
Amount of items: 2
Items: 
Size: 651531 Color: 1
Size: 348045 Color: 2

Bin 3769: 426 of cap free
Amount of items: 2
Items: 
Size: 575917 Color: 4
Size: 423658 Color: 1

Bin 3770: 426 of cap free
Amount of items: 2
Items: 
Size: 636072 Color: 4
Size: 363503 Color: 0

Bin 3771: 428 of cap free
Amount of items: 2
Items: 
Size: 516081 Color: 2
Size: 483492 Color: 3

Bin 3772: 429 of cap free
Amount of items: 2
Items: 
Size: 710267 Color: 1
Size: 289305 Color: 3

Bin 3773: 430 of cap free
Amount of items: 2
Items: 
Size: 608384 Color: 1
Size: 391187 Color: 2

Bin 3774: 431 of cap free
Amount of items: 2
Items: 
Size: 657317 Color: 3
Size: 342253 Color: 4

Bin 3775: 431 of cap free
Amount of items: 2
Items: 
Size: 696288 Color: 4
Size: 303282 Color: 1

Bin 3776: 432 of cap free
Amount of items: 2
Items: 
Size: 522963 Color: 4
Size: 476606 Color: 1

Bin 3777: 433 of cap free
Amount of items: 2
Items: 
Size: 519462 Color: 1
Size: 480106 Color: 3

Bin 3778: 434 of cap free
Amount of items: 3
Items: 
Size: 707513 Color: 2
Size: 150419 Color: 0
Size: 141635 Color: 3

Bin 3779: 435 of cap free
Amount of items: 2
Items: 
Size: 600025 Color: 2
Size: 399541 Color: 4

Bin 3780: 435 of cap free
Amount of items: 2
Items: 
Size: 558429 Color: 0
Size: 441137 Color: 4

Bin 3781: 435 of cap free
Amount of items: 2
Items: 
Size: 753375 Color: 1
Size: 246191 Color: 2

Bin 3782: 435 of cap free
Amount of items: 2
Items: 
Size: 500803 Color: 4
Size: 498763 Color: 1

Bin 3783: 437 of cap free
Amount of items: 2
Items: 
Size: 648451 Color: 1
Size: 351113 Color: 0

Bin 3784: 437 of cap free
Amount of items: 2
Items: 
Size: 594190 Color: 3
Size: 405374 Color: 4

Bin 3785: 437 of cap free
Amount of items: 2
Items: 
Size: 780979 Color: 4
Size: 218585 Color: 3

Bin 3786: 438 of cap free
Amount of items: 2
Items: 
Size: 685520 Color: 2
Size: 314043 Color: 0

Bin 3787: 440 of cap free
Amount of items: 2
Items: 
Size: 602699 Color: 2
Size: 396862 Color: 1

Bin 3788: 441 of cap free
Amount of items: 2
Items: 
Size: 533320 Color: 0
Size: 466240 Color: 4

Bin 3789: 442 of cap free
Amount of items: 2
Items: 
Size: 709407 Color: 1
Size: 290152 Color: 2

Bin 3790: 443 of cap free
Amount of items: 2
Items: 
Size: 558408 Color: 2
Size: 441150 Color: 0

Bin 3791: 443 of cap free
Amount of items: 2
Items: 
Size: 782462 Color: 0
Size: 217096 Color: 3

Bin 3792: 444 of cap free
Amount of items: 2
Items: 
Size: 537162 Color: 0
Size: 462395 Color: 4

Bin 3793: 444 of cap free
Amount of items: 2
Items: 
Size: 503049 Color: 4
Size: 496508 Color: 3

Bin 3794: 445 of cap free
Amount of items: 2
Items: 
Size: 586532 Color: 2
Size: 413024 Color: 4

Bin 3795: 448 of cap free
Amount of items: 2
Items: 
Size: 741383 Color: 3
Size: 258170 Color: 1

Bin 3796: 450 of cap free
Amount of items: 2
Items: 
Size: 723204 Color: 3
Size: 276347 Color: 2

Bin 3797: 450 of cap free
Amount of items: 2
Items: 
Size: 559130 Color: 4
Size: 440421 Color: 0

Bin 3798: 451 of cap free
Amount of items: 2
Items: 
Size: 730407 Color: 0
Size: 269143 Color: 2

Bin 3799: 453 of cap free
Amount of items: 2
Items: 
Size: 554543 Color: 2
Size: 445005 Color: 1

Bin 3800: 457 of cap free
Amount of items: 2
Items: 
Size: 721814 Color: 0
Size: 277730 Color: 1

Bin 3801: 459 of cap free
Amount of items: 2
Items: 
Size: 719743 Color: 3
Size: 279799 Color: 1

Bin 3802: 461 of cap free
Amount of items: 2
Items: 
Size: 539136 Color: 2
Size: 460404 Color: 3

Bin 3803: 462 of cap free
Amount of items: 2
Items: 
Size: 668630 Color: 1
Size: 330909 Color: 3

Bin 3804: 462 of cap free
Amount of items: 2
Items: 
Size: 680541 Color: 3
Size: 318998 Color: 1

Bin 3805: 462 of cap free
Amount of items: 2
Items: 
Size: 769750 Color: 0
Size: 229789 Color: 1

Bin 3806: 463 of cap free
Amount of items: 2
Items: 
Size: 536619 Color: 1
Size: 462919 Color: 2

Bin 3807: 464 of cap free
Amount of items: 2
Items: 
Size: 759741 Color: 0
Size: 239796 Color: 3

Bin 3808: 467 of cap free
Amount of items: 2
Items: 
Size: 774804 Color: 3
Size: 224730 Color: 2

Bin 3809: 468 of cap free
Amount of items: 2
Items: 
Size: 717736 Color: 0
Size: 281797 Color: 3

Bin 3810: 468 of cap free
Amount of items: 2
Items: 
Size: 585058 Color: 2
Size: 414475 Color: 3

Bin 3811: 471 of cap free
Amount of items: 2
Items: 
Size: 612568 Color: 3
Size: 386962 Color: 1

Bin 3812: 472 of cap free
Amount of items: 2
Items: 
Size: 540474 Color: 4
Size: 459055 Color: 1

Bin 3813: 477 of cap free
Amount of items: 2
Items: 
Size: 683099 Color: 0
Size: 316425 Color: 2

Bin 3814: 478 of cap free
Amount of items: 2
Items: 
Size: 659586 Color: 2
Size: 339937 Color: 3

Bin 3815: 480 of cap free
Amount of items: 2
Items: 
Size: 590862 Color: 0
Size: 408659 Color: 1

Bin 3816: 480 of cap free
Amount of items: 2
Items: 
Size: 515332 Color: 0
Size: 484189 Color: 2

Bin 3817: 483 of cap free
Amount of items: 2
Items: 
Size: 692009 Color: 1
Size: 307509 Color: 4

Bin 3818: 484 of cap free
Amount of items: 2
Items: 
Size: 616578 Color: 2
Size: 382939 Color: 4

Bin 3819: 485 of cap free
Amount of items: 2
Items: 
Size: 539118 Color: 0
Size: 460398 Color: 4

Bin 3820: 486 of cap free
Amount of items: 2
Items: 
Size: 707479 Color: 0
Size: 292036 Color: 1

Bin 3821: 486 of cap free
Amount of items: 2
Items: 
Size: 531652 Color: 1
Size: 467863 Color: 3

Bin 3822: 487 of cap free
Amount of items: 2
Items: 
Size: 512390 Color: 4
Size: 487124 Color: 2

Bin 3823: 490 of cap free
Amount of items: 2
Items: 
Size: 516004 Color: 3
Size: 483507 Color: 2

Bin 3824: 490 of cap free
Amount of items: 2
Items: 
Size: 547134 Color: 2
Size: 452377 Color: 3

Bin 3825: 491 of cap free
Amount of items: 2
Items: 
Size: 524316 Color: 2
Size: 475194 Color: 1

Bin 3826: 491 of cap free
Amount of items: 2
Items: 
Size: 620172 Color: 1
Size: 379338 Color: 2

Bin 3827: 491 of cap free
Amount of items: 2
Items: 
Size: 700565 Color: 1
Size: 298945 Color: 3

Bin 3828: 492 of cap free
Amount of items: 2
Items: 
Size: 756669 Color: 0
Size: 242840 Color: 4

Bin 3829: 492 of cap free
Amount of items: 2
Items: 
Size: 751122 Color: 1
Size: 248387 Color: 4

Bin 3830: 493 of cap free
Amount of items: 2
Items: 
Size: 744055 Color: 2
Size: 255453 Color: 0

Bin 3831: 493 of cap free
Amount of items: 2
Items: 
Size: 649893 Color: 2
Size: 349615 Color: 3

Bin 3832: 495 of cap free
Amount of items: 2
Items: 
Size: 559087 Color: 4
Size: 440419 Color: 0

Bin 3833: 496 of cap free
Amount of items: 2
Items: 
Size: 735863 Color: 0
Size: 263642 Color: 4

Bin 3834: 496 of cap free
Amount of items: 2
Items: 
Size: 766948 Color: 0
Size: 232557 Color: 2

Bin 3835: 497 of cap free
Amount of items: 2
Items: 
Size: 732753 Color: 3
Size: 266751 Color: 4

Bin 3836: 497 of cap free
Amount of items: 2
Items: 
Size: 582464 Color: 0
Size: 417040 Color: 4

Bin 3837: 500 of cap free
Amount of items: 2
Items: 
Size: 761475 Color: 3
Size: 238026 Color: 4

Bin 3838: 501 of cap free
Amount of items: 2
Items: 
Size: 536602 Color: 1
Size: 462898 Color: 2

Bin 3839: 501 of cap free
Amount of items: 2
Items: 
Size: 601751 Color: 2
Size: 397749 Color: 1

Bin 3840: 501 of cap free
Amount of items: 2
Items: 
Size: 685978 Color: 2
Size: 313522 Color: 1

Bin 3841: 503 of cap free
Amount of items: 2
Items: 
Size: 556084 Color: 4
Size: 443414 Color: 3

Bin 3842: 504 of cap free
Amount of items: 2
Items: 
Size: 708182 Color: 3
Size: 291315 Color: 0

Bin 3843: 506 of cap free
Amount of items: 2
Items: 
Size: 688922 Color: 3
Size: 310573 Color: 1

Bin 3844: 507 of cap free
Amount of items: 2
Items: 
Size: 765528 Color: 4
Size: 233966 Color: 2

Bin 3845: 508 of cap free
Amount of items: 2
Items: 
Size: 528302 Color: 0
Size: 471191 Color: 4

Bin 3846: 508 of cap free
Amount of items: 2
Items: 
Size: 610165 Color: 4
Size: 389328 Color: 2

Bin 3847: 516 of cap free
Amount of items: 2
Items: 
Size: 672017 Color: 4
Size: 327468 Color: 2

Bin 3848: 520 of cap free
Amount of items: 2
Items: 
Size: 783903 Color: 0
Size: 215578 Color: 1

Bin 3849: 520 of cap free
Amount of items: 2
Items: 
Size: 582425 Color: 4
Size: 417056 Color: 0

Bin 3850: 521 of cap free
Amount of items: 2
Items: 
Size: 580643 Color: 1
Size: 418837 Color: 2

Bin 3851: 523 of cap free
Amount of items: 2
Items: 
Size: 733411 Color: 1
Size: 266067 Color: 2

Bin 3852: 528 of cap free
Amount of items: 2
Items: 
Size: 766925 Color: 2
Size: 232548 Color: 1

Bin 3853: 528 of cap free
Amount of items: 2
Items: 
Size: 681841 Color: 4
Size: 317632 Color: 1

Bin 3854: 529 of cap free
Amount of items: 2
Items: 
Size: 591374 Color: 2
Size: 408098 Color: 1

Bin 3855: 531 of cap free
Amount of items: 2
Items: 
Size: 499736 Color: 0
Size: 499734 Color: 3

Bin 3856: 533 of cap free
Amount of items: 2
Items: 
Size: 683682 Color: 0
Size: 315786 Color: 2

Bin 3857: 534 of cap free
Amount of items: 2
Items: 
Size: 516072 Color: 2
Size: 483395 Color: 3

Bin 3858: 536 of cap free
Amount of items: 2
Items: 
Size: 620165 Color: 0
Size: 379300 Color: 3

Bin 3859: 537 of cap free
Amount of items: 2
Items: 
Size: 632824 Color: 3
Size: 366640 Color: 1

Bin 3860: 538 of cap free
Amount of items: 2
Items: 
Size: 697461 Color: 3
Size: 302002 Color: 1

Bin 3861: 542 of cap free
Amount of items: 2
Items: 
Size: 567824 Color: 2
Size: 431635 Color: 4

Bin 3862: 543 of cap free
Amount of items: 2
Items: 
Size: 579606 Color: 1
Size: 419852 Color: 3

Bin 3863: 543 of cap free
Amount of items: 2
Items: 
Size: 529625 Color: 1
Size: 469833 Color: 0

Bin 3864: 544 of cap free
Amount of items: 2
Items: 
Size: 713782 Color: 3
Size: 285675 Color: 1

Bin 3865: 544 of cap free
Amount of items: 2
Items: 
Size: 662098 Color: 2
Size: 337359 Color: 3

Bin 3866: 545 of cap free
Amount of items: 2
Items: 
Size: 681158 Color: 3
Size: 318298 Color: 1

Bin 3867: 553 of cap free
Amount of items: 2
Items: 
Size: 523106 Color: 1
Size: 476342 Color: 2

Bin 3868: 554 of cap free
Amount of items: 2
Items: 
Size: 691307 Color: 2
Size: 308140 Color: 4

Bin 3869: 557 of cap free
Amount of items: 2
Items: 
Size: 569817 Color: 1
Size: 429627 Color: 2

Bin 3870: 561 of cap free
Amount of items: 2
Items: 
Size: 588710 Color: 0
Size: 410730 Color: 1

Bin 3871: 563 of cap free
Amount of items: 2
Items: 
Size: 540695 Color: 3
Size: 458743 Color: 4

Bin 3872: 564 of cap free
Amount of items: 2
Items: 
Size: 705873 Color: 1
Size: 293564 Color: 4

Bin 3873: 567 of cap free
Amount of items: 2
Items: 
Size: 704777 Color: 0
Size: 294657 Color: 1

Bin 3874: 569 of cap free
Amount of items: 2
Items: 
Size: 503298 Color: 3
Size: 496134 Color: 4

Bin 3875: 570 of cap free
Amount of items: 2
Items: 
Size: 634861 Color: 2
Size: 364570 Color: 3

Bin 3876: 571 of cap free
Amount of items: 2
Items: 
Size: 661088 Color: 4
Size: 338342 Color: 3

Bin 3877: 572 of cap free
Amount of items: 2
Items: 
Size: 508074 Color: 0
Size: 491355 Color: 4

Bin 3878: 572 of cap free
Amount of items: 2
Items: 
Size: 671207 Color: 2
Size: 328222 Color: 4

Bin 3879: 573 of cap free
Amount of items: 2
Items: 
Size: 620140 Color: 3
Size: 379288 Color: 4

Bin 3880: 573 of cap free
Amount of items: 2
Items: 
Size: 599339 Color: 2
Size: 400089 Color: 4

Bin 3881: 574 of cap free
Amount of items: 2
Items: 
Size: 610734 Color: 2
Size: 388693 Color: 1

Bin 3882: 575 of cap free
Amount of items: 2
Items: 
Size: 544963 Color: 4
Size: 454463 Color: 2

Bin 3883: 578 of cap free
Amount of items: 2
Items: 
Size: 519156 Color: 3
Size: 480267 Color: 1

Bin 3884: 579 of cap free
Amount of items: 2
Items: 
Size: 788020 Color: 3
Size: 211402 Color: 0

Bin 3885: 580 of cap free
Amount of items: 2
Items: 
Size: 732724 Color: 1
Size: 266697 Color: 2

Bin 3886: 581 of cap free
Amount of items: 2
Items: 
Size: 642998 Color: 0
Size: 356422 Color: 4

Bin 3887: 583 of cap free
Amount of items: 2
Items: 
Size: 794353 Color: 3
Size: 205065 Color: 4

Bin 3888: 583 of cap free
Amount of items: 2
Items: 
Size: 533314 Color: 3
Size: 466104 Color: 1

Bin 3889: 584 of cap free
Amount of items: 2
Items: 
Size: 513171 Color: 4
Size: 486246 Color: 3

Bin 3890: 586 of cap free
Amount of items: 2
Items: 
Size: 703819 Color: 1
Size: 295596 Color: 0

Bin 3891: 587 of cap free
Amount of items: 2
Items: 
Size: 610722 Color: 3
Size: 388692 Color: 2

Bin 3892: 588 of cap free
Amount of items: 2
Items: 
Size: 601125 Color: 1
Size: 398288 Color: 3

Bin 3893: 590 of cap free
Amount of items: 2
Items: 
Size: 513803 Color: 4
Size: 485608 Color: 3

Bin 3894: 593 of cap free
Amount of items: 2
Items: 
Size: 512928 Color: 0
Size: 486480 Color: 4

Bin 3895: 594 of cap free
Amount of items: 2
Items: 
Size: 772637 Color: 4
Size: 226770 Color: 2

Bin 3896: 595 of cap free
Amount of items: 2
Items: 
Size: 558273 Color: 1
Size: 441133 Color: 4

Bin 3897: 595 of cap free
Amount of items: 2
Items: 
Size: 704768 Color: 3
Size: 294638 Color: 4

Bin 3898: 596 of cap free
Amount of items: 2
Items: 
Size: 516025 Color: 2
Size: 483380 Color: 3

Bin 3899: 596 of cap free
Amount of items: 2
Items: 
Size: 629039 Color: 3
Size: 370366 Color: 0

Bin 3900: 599 of cap free
Amount of items: 2
Items: 
Size: 665903 Color: 3
Size: 333499 Color: 2

Bin 3901: 603 of cap free
Amount of items: 2
Items: 
Size: 744475 Color: 0
Size: 254923 Color: 1

Bin 3902: 611 of cap free
Amount of items: 2
Items: 
Size: 733358 Color: 3
Size: 266032 Color: 0

Bin 3903: 612 of cap free
Amount of items: 2
Items: 
Size: 508054 Color: 1
Size: 491335 Color: 0

Bin 3904: 616 of cap free
Amount of items: 2
Items: 
Size: 556033 Color: 4
Size: 443352 Color: 1

Bin 3905: 617 of cap free
Amount of items: 2
Items: 
Size: 524891 Color: 1
Size: 474493 Color: 2

Bin 3906: 617 of cap free
Amount of items: 2
Items: 
Size: 662036 Color: 2
Size: 337348 Color: 4

Bin 3907: 619 of cap free
Amount of items: 2
Items: 
Size: 751703 Color: 3
Size: 247679 Color: 2

Bin 3908: 619 of cap free
Amount of items: 2
Items: 
Size: 711841 Color: 4
Size: 287541 Color: 2

Bin 3909: 621 of cap free
Amount of items: 2
Items: 
Size: 619169 Color: 1
Size: 380211 Color: 3

Bin 3910: 622 of cap free
Amount of items: 2
Items: 
Size: 560153 Color: 1
Size: 439226 Color: 4

Bin 3911: 624 of cap free
Amount of items: 2
Items: 
Size: 542048 Color: 0
Size: 457329 Color: 3

Bin 3912: 624 of cap free
Amount of items: 2
Items: 
Size: 581513 Color: 4
Size: 417864 Color: 2

Bin 3913: 625 of cap free
Amount of items: 2
Items: 
Size: 624486 Color: 2
Size: 374890 Color: 4

Bin 3914: 626 of cap free
Amount of items: 2
Items: 
Size: 617263 Color: 4
Size: 382112 Color: 3

Bin 3915: 628 of cap free
Amount of items: 2
Items: 
Size: 691195 Color: 1
Size: 308178 Color: 2

Bin 3916: 629 of cap free
Amount of items: 2
Items: 
Size: 728399 Color: 3
Size: 270973 Color: 0

Bin 3917: 630 of cap free
Amount of items: 2
Items: 
Size: 668477 Color: 4
Size: 330894 Color: 3

Bin 3918: 631 of cap free
Amount of items: 2
Items: 
Size: 623579 Color: 1
Size: 375791 Color: 0

Bin 3919: 633 of cap free
Amount of items: 2
Items: 
Size: 520977 Color: 0
Size: 478391 Color: 4

Bin 3920: 634 of cap free
Amount of items: 2
Items: 
Size: 643065 Color: 2
Size: 356302 Color: 0

Bin 3921: 637 of cap free
Amount of items: 2
Items: 
Size: 663436 Color: 2
Size: 335928 Color: 4

Bin 3922: 640 of cap free
Amount of items: 2
Items: 
Size: 500879 Color: 1
Size: 498482 Color: 4

Bin 3923: 641 of cap free
Amount of items: 2
Items: 
Size: 537006 Color: 2
Size: 462354 Color: 0

Bin 3924: 642 of cap free
Amount of items: 2
Items: 
Size: 679324 Color: 4
Size: 320035 Color: 0

Bin 3925: 643 of cap free
Amount of items: 2
Items: 
Size: 579527 Color: 2
Size: 419831 Color: 1

Bin 3926: 646 of cap free
Amount of items: 2
Items: 
Size: 569742 Color: 0
Size: 429613 Color: 1

Bin 3927: 649 of cap free
Amount of items: 2
Items: 
Size: 551764 Color: 0
Size: 447588 Color: 3

Bin 3928: 650 of cap free
Amount of items: 2
Items: 
Size: 616429 Color: 3
Size: 382922 Color: 1

Bin 3929: 653 of cap free
Amount of items: 2
Items: 
Size: 538966 Color: 1
Size: 460382 Color: 0

Bin 3930: 655 of cap free
Amount of items: 2
Items: 
Size: 779348 Color: 2
Size: 219998 Color: 4

Bin 3931: 655 of cap free
Amount of items: 2
Items: 
Size: 699845 Color: 1
Size: 299501 Color: 2

Bin 3932: 656 of cap free
Amount of items: 2
Items: 
Size: 736708 Color: 2
Size: 262637 Color: 3

Bin 3933: 663 of cap free
Amount of items: 2
Items: 
Size: 639015 Color: 0
Size: 360323 Color: 3

Bin 3934: 671 of cap free
Amount of items: 2
Items: 
Size: 712598 Color: 2
Size: 286732 Color: 4

Bin 3935: 671 of cap free
Amount of items: 2
Items: 
Size: 534744 Color: 3
Size: 464586 Color: 4

Bin 3936: 671 of cap free
Amount of items: 2
Items: 
Size: 707308 Color: 4
Size: 292022 Color: 3

Bin 3937: 674 of cap free
Amount of items: 2
Items: 
Size: 504787 Color: 3
Size: 494540 Color: 1

Bin 3938: 674 of cap free
Amount of items: 2
Items: 
Size: 585094 Color: 3
Size: 414233 Color: 1

Bin 3939: 679 of cap free
Amount of items: 2
Items: 
Size: 776462 Color: 3
Size: 222860 Color: 2

Bin 3940: 679 of cap free
Amount of items: 2
Items: 
Size: 640117 Color: 2
Size: 359205 Color: 1

Bin 3941: 679 of cap free
Amount of items: 2
Items: 
Size: 512927 Color: 2
Size: 486395 Color: 4

Bin 3942: 679 of cap free
Amount of items: 2
Items: 
Size: 502472 Color: 3
Size: 496850 Color: 4

Bin 3943: 683 of cap free
Amount of items: 2
Items: 
Size: 573934 Color: 4
Size: 425384 Color: 1

Bin 3944: 686 of cap free
Amount of items: 2
Items: 
Size: 796096 Color: 0
Size: 203219 Color: 4

Bin 3945: 686 of cap free
Amount of items: 2
Items: 
Size: 524825 Color: 4
Size: 474490 Color: 2

Bin 3946: 688 of cap free
Amount of items: 2
Items: 
Size: 783654 Color: 1
Size: 215659 Color: 0

Bin 3947: 691 of cap free
Amount of items: 2
Items: 
Size: 524123 Color: 1
Size: 475187 Color: 3

Bin 3948: 692 of cap free
Amount of items: 2
Items: 
Size: 518679 Color: 4
Size: 480630 Color: 3

Bin 3949: 694 of cap free
Amount of items: 2
Items: 
Size: 576130 Color: 1
Size: 423177 Color: 3

Bin 3950: 697 of cap free
Amount of items: 2
Items: 
Size: 549435 Color: 2
Size: 449869 Color: 0

Bin 3951: 700 of cap free
Amount of items: 2
Items: 
Size: 548421 Color: 1
Size: 450880 Color: 2

Bin 3952: 701 of cap free
Amount of items: 2
Items: 
Size: 717802 Color: 3
Size: 281498 Color: 1

Bin 3953: 703 of cap free
Amount of items: 2
Items: 
Size: 657770 Color: 0
Size: 341528 Color: 3

Bin 3954: 704 of cap free
Amount of items: 2
Items: 
Size: 759634 Color: 4
Size: 239663 Color: 2

Bin 3955: 706 of cap free
Amount of items: 2
Items: 
Size: 711795 Color: 2
Size: 287500 Color: 3

Bin 3956: 707 of cap free
Amount of items: 2
Items: 
Size: 555907 Color: 0
Size: 443387 Color: 4

Bin 3957: 709 of cap free
Amount of items: 2
Items: 
Size: 755322 Color: 0
Size: 243970 Color: 2

Bin 3958: 710 of cap free
Amount of items: 2
Items: 
Size: 665219 Color: 2
Size: 334072 Color: 3

Bin 3959: 711 of cap free
Amount of items: 2
Items: 
Size: 676636 Color: 1
Size: 322654 Color: 3

Bin 3960: 711 of cap free
Amount of items: 2
Items: 
Size: 613977 Color: 0
Size: 385313 Color: 2

Bin 3961: 716 of cap free
Amount of items: 2
Items: 
Size: 504387 Color: 0
Size: 494898 Color: 3

Bin 3962: 717 of cap free
Amount of items: 2
Items: 
Size: 558267 Color: 2
Size: 441017 Color: 4

Bin 3963: 717 of cap free
Amount of items: 2
Items: 
Size: 582292 Color: 3
Size: 416992 Color: 2

Bin 3964: 719 of cap free
Amount of items: 2
Items: 
Size: 649891 Color: 1
Size: 349391 Color: 3

Bin 3965: 722 of cap free
Amount of items: 2
Items: 
Size: 795713 Color: 3
Size: 203566 Color: 0

Bin 3966: 724 of cap free
Amount of items: 2
Items: 
Size: 516018 Color: 4
Size: 483259 Color: 3

Bin 3967: 724 of cap free
Amount of items: 2
Items: 
Size: 751661 Color: 0
Size: 247616 Color: 2

Bin 3968: 726 of cap free
Amount of items: 2
Items: 
Size: 588553 Color: 4
Size: 410722 Color: 3

Bin 3969: 726 of cap free
Amount of items: 2
Items: 
Size: 543255 Color: 1
Size: 456020 Color: 2

Bin 3970: 730 of cap free
Amount of items: 2
Items: 
Size: 563015 Color: 1
Size: 436256 Color: 3

Bin 3971: 730 of cap free
Amount of items: 2
Items: 
Size: 630650 Color: 0
Size: 368621 Color: 2

Bin 3972: 732 of cap free
Amount of items: 2
Items: 
Size: 745373 Color: 2
Size: 253896 Color: 3

Bin 3973: 734 of cap free
Amount of items: 2
Items: 
Size: 608268 Color: 3
Size: 390999 Color: 2

Bin 3974: 734 of cap free
Amount of items: 2
Items: 
Size: 711767 Color: 0
Size: 287500 Color: 2

Bin 3975: 735 of cap free
Amount of items: 2
Items: 
Size: 552848 Color: 1
Size: 446418 Color: 2

Bin 3976: 737 of cap free
Amount of items: 2
Items: 
Size: 673118 Color: 2
Size: 326146 Color: 0

Bin 3977: 741 of cap free
Amount of items: 2
Items: 
Size: 728376 Color: 3
Size: 270884 Color: 1

Bin 3978: 742 of cap free
Amount of items: 2
Items: 
Size: 768902 Color: 1
Size: 230357 Color: 0

Bin 3979: 742 of cap free
Amount of items: 2
Items: 
Size: 550870 Color: 1
Size: 448389 Color: 3

Bin 3980: 744 of cap free
Amount of items: 2
Items: 
Size: 786508 Color: 1
Size: 212749 Color: 3

Bin 3981: 744 of cap free
Amount of items: 2
Items: 
Size: 751647 Color: 0
Size: 247610 Color: 1

Bin 3982: 745 of cap free
Amount of items: 2
Items: 
Size: 721779 Color: 3
Size: 277477 Color: 1

Bin 3983: 745 of cap free
Amount of items: 2
Items: 
Size: 518662 Color: 2
Size: 480594 Color: 3

Bin 3984: 745 of cap free
Amount of items: 2
Items: 
Size: 580428 Color: 0
Size: 418828 Color: 4

Bin 3985: 746 of cap free
Amount of items: 2
Items: 
Size: 519059 Color: 3
Size: 480196 Color: 1

Bin 3986: 756 of cap free
Amount of items: 2
Items: 
Size: 557507 Color: 0
Size: 441738 Color: 2

Bin 3987: 757 of cap free
Amount of items: 2
Items: 
Size: 544886 Color: 4
Size: 454358 Color: 3

Bin 3988: 757 of cap free
Amount of items: 2
Items: 
Size: 791460 Color: 1
Size: 207784 Color: 4

Bin 3989: 766 of cap free
Amount of items: 2
Items: 
Size: 761367 Color: 2
Size: 237868 Color: 4

Bin 3990: 772 of cap free
Amount of items: 2
Items: 
Size: 665744 Color: 3
Size: 333485 Color: 2

Bin 3991: 777 of cap free
Amount of items: 2
Items: 
Size: 529971 Color: 0
Size: 469253 Color: 4

Bin 3992: 777 of cap free
Amount of items: 2
Items: 
Size: 587336 Color: 0
Size: 411888 Color: 2

Bin 3993: 777 of cap free
Amount of items: 2
Items: 
Size: 621818 Color: 3
Size: 377406 Color: 1

Bin 3994: 786 of cap free
Amount of items: 2
Items: 
Size: 542042 Color: 3
Size: 457173 Color: 0

Bin 3995: 789 of cap free
Amount of items: 2
Items: 
Size: 540548 Color: 1
Size: 458664 Color: 3

Bin 3996: 792 of cap free
Amount of items: 2
Items: 
Size: 790502 Color: 4
Size: 208707 Color: 0

Bin 3997: 792 of cap free
Amount of items: 2
Items: 
Size: 606829 Color: 1
Size: 392380 Color: 3

Bin 3998: 792 of cap free
Amount of items: 2
Items: 
Size: 560196 Color: 4
Size: 439013 Color: 1

Bin 3999: 804 of cap free
Amount of items: 2
Items: 
Size: 718657 Color: 2
Size: 280540 Color: 4

Bin 4000: 805 of cap free
Amount of items: 2
Items: 
Size: 712547 Color: 1
Size: 286649 Color: 3

Bin 4001: 809 of cap free
Amount of items: 2
Items: 
Size: 619944 Color: 1
Size: 379248 Color: 2

Bin 4002: 810 of cap free
Amount of items: 2
Items: 
Size: 538955 Color: 0
Size: 460236 Color: 3

Bin 4003: 811 of cap free
Amount of items: 2
Items: 
Size: 520869 Color: 3
Size: 478321 Color: 2

Bin 4004: 814 of cap free
Amount of items: 2
Items: 
Size: 513698 Color: 3
Size: 485489 Color: 2

Bin 4005: 815 of cap free
Amount of items: 2
Items: 
Size: 701951 Color: 1
Size: 297235 Color: 4

Bin 4006: 816 of cap free
Amount of items: 2
Items: 
Size: 520801 Color: 4
Size: 478384 Color: 3

Bin 4007: 831 of cap free
Amount of items: 2
Items: 
Size: 724166 Color: 4
Size: 275004 Color: 1

Bin 4008: 839 of cap free
Amount of items: 2
Items: 
Size: 667036 Color: 3
Size: 332126 Color: 4

Bin 4009: 840 of cap free
Amount of items: 2
Items: 
Size: 500749 Color: 2
Size: 498412 Color: 4

Bin 4010: 841 of cap free
Amount of items: 2
Items: 
Size: 732481 Color: 1
Size: 266679 Color: 2

Bin 4011: 843 of cap free
Amount of items: 2
Items: 
Size: 740163 Color: 4
Size: 258995 Color: 1

Bin 4012: 843 of cap free
Amount of items: 2
Items: 
Size: 619941 Color: 0
Size: 379217 Color: 4

Bin 4013: 843 of cap free
Amount of items: 2
Items: 
Size: 533266 Color: 1
Size: 465892 Color: 2

Bin 4014: 849 of cap free
Amount of items: 2
Items: 
Size: 500764 Color: 4
Size: 498388 Color: 0

Bin 4015: 857 of cap free
Amount of items: 2
Items: 
Size: 519048 Color: 3
Size: 480096 Color: 0

Bin 4016: 864 of cap free
Amount of items: 2
Items: 
Size: 661931 Color: 3
Size: 337206 Color: 0

Bin 4017: 865 of cap free
Amount of items: 2
Items: 
Size: 617195 Color: 4
Size: 381941 Color: 1

Bin 4018: 865 of cap free
Amount of items: 2
Items: 
Size: 608167 Color: 0
Size: 390969 Color: 2

Bin 4019: 867 of cap free
Amount of items: 2
Items: 
Size: 555800 Color: 2
Size: 443334 Color: 4

Bin 4020: 867 of cap free
Amount of items: 2
Items: 
Size: 572146 Color: 3
Size: 426988 Color: 4

Bin 4021: 872 of cap free
Amount of items: 2
Items: 
Size: 679132 Color: 4
Size: 319997 Color: 0

Bin 4022: 874 of cap free
Amount of items: 2
Items: 
Size: 606765 Color: 2
Size: 392362 Color: 3

Bin 4023: 876 of cap free
Amount of items: 2
Items: 
Size: 694224 Color: 1
Size: 304901 Color: 2

Bin 4024: 879 of cap free
Amount of items: 2
Items: 
Size: 783826 Color: 0
Size: 215296 Color: 3

Bin 4025: 880 of cap free
Amount of items: 2
Items: 
Size: 574782 Color: 4
Size: 424339 Color: 1

Bin 4026: 886 of cap free
Amount of items: 2
Items: 
Size: 562885 Color: 4
Size: 436230 Color: 3

Bin 4027: 892 of cap free
Amount of items: 2
Items: 
Size: 779179 Color: 3
Size: 219930 Color: 2

Bin 4028: 897 of cap free
Amount of items: 2
Items: 
Size: 634800 Color: 0
Size: 364304 Color: 1

Bin 4029: 897 of cap free
Amount of items: 2
Items: 
Size: 612041 Color: 4
Size: 387063 Color: 3

Bin 4030: 898 of cap free
Amount of items: 2
Items: 
Size: 625619 Color: 0
Size: 373484 Color: 2

Bin 4031: 907 of cap free
Amount of items: 2
Items: 
Size: 724147 Color: 3
Size: 274947 Color: 2

Bin 4032: 911 of cap free
Amount of items: 2
Items: 
Size: 668209 Color: 3
Size: 330881 Color: 1

Bin 4033: 920 of cap free
Amount of items: 2
Items: 
Size: 777311 Color: 2
Size: 221770 Color: 1

Bin 4034: 921 of cap free
Amount of items: 2
Items: 
Size: 522503 Color: 2
Size: 476577 Color: 1

Bin 4035: 925 of cap free
Amount of items: 2
Items: 
Size: 715835 Color: 1
Size: 283241 Color: 2

Bin 4036: 929 of cap free
Amount of items: 2
Items: 
Size: 707139 Color: 4
Size: 291933 Color: 3

Bin 4037: 935 of cap free
Amount of items: 2
Items: 
Size: 704738 Color: 2
Size: 294328 Color: 3

Bin 4038: 936 of cap free
Amount of items: 2
Items: 
Size: 703703 Color: 2
Size: 295362 Color: 0

Bin 4039: 938 of cap free
Amount of items: 2
Items: 
Size: 525580 Color: 4
Size: 473483 Color: 3

Bin 4040: 941 of cap free
Amount of items: 2
Items: 
Size: 755322 Color: 2
Size: 243738 Color: 1

Bin 4041: 941 of cap free
Amount of items: 2
Items: 
Size: 684507 Color: 4
Size: 314553 Color: 2

Bin 4042: 942 of cap free
Amount of items: 2
Items: 
Size: 799537 Color: 4
Size: 199522 Color: 2

Bin 4043: 945 of cap free
Amount of items: 2
Items: 
Size: 761328 Color: 2
Size: 237728 Color: 4

Bin 4044: 950 of cap free
Amount of items: 2
Items: 
Size: 534479 Color: 3
Size: 464572 Color: 2

Bin 4045: 951 of cap free
Amount of items: 2
Items: 
Size: 726490 Color: 2
Size: 272560 Color: 0

Bin 4046: 953 of cap free
Amount of items: 2
Items: 
Size: 681812 Color: 3
Size: 317236 Color: 2

Bin 4047: 953 of cap free
Amount of items: 2
Items: 
Size: 584947 Color: 0
Size: 414101 Color: 1

Bin 4048: 956 of cap free
Amount of items: 2
Items: 
Size: 588498 Color: 4
Size: 410547 Color: 0

Bin 4049: 968 of cap free
Amount of items: 2
Items: 
Size: 543046 Color: 2
Size: 455987 Color: 3

Bin 4050: 970 of cap free
Amount of items: 2
Items: 
Size: 667001 Color: 0
Size: 332030 Color: 1

Bin 4051: 972 of cap free
Amount of items: 2
Items: 
Size: 541952 Color: 3
Size: 457077 Color: 1

Bin 4052: 981 of cap free
Amount of items: 2
Items: 
Size: 721349 Color: 4
Size: 277671 Color: 3

Bin 4053: 982 of cap free
Amount of items: 2
Items: 
Size: 500644 Color: 3
Size: 498375 Color: 1

Bin 4054: 985 of cap free
Amount of items: 2
Items: 
Size: 580380 Color: 3
Size: 418636 Color: 0

Bin 4055: 986 of cap free
Amount of items: 2
Items: 
Size: 588404 Color: 3
Size: 410611 Color: 4

Bin 4056: 990 of cap free
Amount of items: 2
Items: 
Size: 755087 Color: 0
Size: 243924 Color: 2

Bin 4057: 995 of cap free
Amount of items: 2
Items: 
Size: 631459 Color: 2
Size: 367547 Color: 0

Bin 4058: 995 of cap free
Amount of items: 2
Items: 
Size: 774474 Color: 1
Size: 224532 Color: 0

Bin 4059: 996 of cap free
Amount of items: 2
Items: 
Size: 715813 Color: 3
Size: 283192 Color: 4

Bin 4060: 997 of cap free
Amount of items: 2
Items: 
Size: 520753 Color: 1
Size: 478251 Color: 0

Bin 4061: 998 of cap free
Amount of items: 2
Items: 
Size: 738531 Color: 1
Size: 260472 Color: 3

Bin 4062: 998 of cap free
Amount of items: 2
Items: 
Size: 602284 Color: 1
Size: 396719 Color: 0

Bin 4063: 999 of cap free
Amount of items: 2
Items: 
Size: 608113 Color: 4
Size: 390889 Color: 1

Bin 4064: 1000 of cap free
Amount of items: 2
Items: 
Size: 704724 Color: 2
Size: 294277 Color: 4

Bin 4065: 1002 of cap free
Amount of items: 2
Items: 
Size: 759522 Color: 1
Size: 239477 Color: 2

Bin 4066: 1004 of cap free
Amount of items: 2
Items: 
Size: 724121 Color: 4
Size: 274876 Color: 0

Bin 4067: 1009 of cap free
Amount of items: 2
Items: 
Size: 531154 Color: 3
Size: 467838 Color: 4

Bin 4068: 1011 of cap free
Amount of items: 2
Items: 
Size: 613570 Color: 4
Size: 385420 Color: 0

Bin 4069: 1015 of cap free
Amount of items: 2
Items: 
Size: 764201 Color: 4
Size: 234785 Color: 2

Bin 4070: 1023 of cap free
Amount of items: 2
Items: 
Size: 736416 Color: 2
Size: 262562 Color: 1

Bin 4071: 1025 of cap free
Amount of items: 2
Items: 
Size: 651211 Color: 1
Size: 347765 Color: 2

Bin 4072: 1026 of cap free
Amount of items: 2
Items: 
Size: 772545 Color: 4
Size: 226430 Color: 0

Bin 4073: 1027 of cap free
Amount of items: 2
Items: 
Size: 621437 Color: 1
Size: 377537 Color: 3

Bin 4074: 1030 of cap free
Amount of items: 2
Items: 
Size: 694053 Color: 2
Size: 304918 Color: 1

Bin 4075: 1034 of cap free
Amount of items: 2
Items: 
Size: 701737 Color: 2
Size: 297230 Color: 4

Bin 4076: 1034 of cap free
Amount of items: 2
Items: 
Size: 645526 Color: 0
Size: 353441 Color: 1

Bin 4077: 1039 of cap free
Amount of items: 2
Items: 
Size: 770418 Color: 0
Size: 228544 Color: 4

Bin 4078: 1047 of cap free
Amount of items: 2
Items: 
Size: 594985 Color: 0
Size: 403969 Color: 1

Bin 4079: 1050 of cap free
Amount of items: 2
Items: 
Size: 538727 Color: 0
Size: 460224 Color: 2

Bin 4080: 1059 of cap free
Amount of items: 2
Items: 
Size: 685506 Color: 4
Size: 313436 Color: 0

Bin 4081: 1061 of cap free
Amount of items: 2
Items: 
Size: 673060 Color: 1
Size: 325880 Color: 2

Bin 4082: 1063 of cap free
Amount of items: 2
Items: 
Size: 560134 Color: 0
Size: 438804 Color: 2

Bin 4083: 1066 of cap free
Amount of items: 2
Items: 
Size: 550564 Color: 0
Size: 448371 Color: 1

Bin 4084: 1067 of cap free
Amount of items: 2
Items: 
Size: 598255 Color: 0
Size: 400679 Color: 2

Bin 4085: 1071 of cap free
Amount of items: 2
Items: 
Size: 625548 Color: 3
Size: 373382 Color: 2

Bin 4086: 1072 of cap free
Amount of items: 2
Items: 
Size: 676387 Color: 3
Size: 322542 Color: 1

Bin 4087: 1080 of cap free
Amount of items: 2
Items: 
Size: 642644 Color: 1
Size: 356277 Color: 0

Bin 4088: 1084 of cap free
Amount of items: 2
Items: 
Size: 500553 Color: 0
Size: 498364 Color: 1

Bin 4089: 1091 of cap free
Amount of items: 2
Items: 
Size: 579105 Color: 3
Size: 419805 Color: 4

Bin 4090: 1098 of cap free
Amount of items: 2
Items: 
Size: 532952 Color: 4
Size: 465951 Color: 1

Bin 4091: 1100 of cap free
Amount of items: 2
Items: 
Size: 555737 Color: 1
Size: 443164 Color: 2

Bin 4092: 1102 of cap free
Amount of items: 2
Items: 
Size: 635776 Color: 0
Size: 363123 Color: 1

Bin 4093: 1114 of cap free
Amount of items: 2
Items: 
Size: 536575 Color: 4
Size: 462312 Color: 2

Bin 4094: 1117 of cap free
Amount of items: 2
Items: 
Size: 717465 Color: 3
Size: 281419 Color: 2

Bin 4095: 1122 of cap free
Amount of items: 2
Items: 
Size: 541815 Color: 0
Size: 457064 Color: 4

Bin 4096: 1122 of cap free
Amount of items: 2
Items: 
Size: 608021 Color: 0
Size: 390858 Color: 2

Bin 4097: 1126 of cap free
Amount of items: 2
Items: 
Size: 543183 Color: 4
Size: 455692 Color: 2

Bin 4098: 1131 of cap free
Amount of items: 2
Items: 
Size: 681805 Color: 0
Size: 317065 Color: 2

Bin 4099: 1143 of cap free
Amount of items: 2
Items: 
Size: 555744 Color: 2
Size: 443114 Color: 1

Bin 4100: 1146 of cap free
Amount of items: 2
Items: 
Size: 543176 Color: 3
Size: 455679 Color: 0

Bin 4101: 1150 of cap free
Amount of items: 2
Items: 
Size: 586231 Color: 1
Size: 412620 Color: 0

Bin 4102: 1156 of cap free
Amount of items: 2
Items: 
Size: 762218 Color: 4
Size: 236627 Color: 2

Bin 4103: 1168 of cap free
Amount of items: 2
Items: 
Size: 606457 Color: 3
Size: 392376 Color: 4

Bin 4104: 1176 of cap free
Amount of items: 2
Items: 
Size: 572034 Color: 0
Size: 426791 Color: 2

Bin 4105: 1177 of cap free
Amount of items: 2
Items: 
Size: 515831 Color: 2
Size: 482993 Color: 0

Bin 4106: 1180 of cap free
Amount of items: 2
Items: 
Size: 698654 Color: 4
Size: 300167 Color: 1

Bin 4107: 1181 of cap free
Amount of items: 2
Items: 
Size: 582297 Color: 2
Size: 416523 Color: 3

Bin 4108: 1187 of cap free
Amount of items: 2
Items: 
Size: 612020 Color: 4
Size: 386794 Color: 2

Bin 4109: 1194 of cap free
Amount of items: 2
Items: 
Size: 743958 Color: 4
Size: 254849 Color: 2

Bin 4110: 1196 of cap free
Amount of items: 2
Items: 
Size: 647714 Color: 3
Size: 351091 Color: 2

Bin 4111: 1199 of cap free
Amount of items: 2
Items: 
Size: 606761 Color: 4
Size: 392041 Color: 3

Bin 4112: 1203 of cap free
Amount of items: 2
Items: 
Size: 625417 Color: 3
Size: 373381 Color: 0

Bin 4113: 1204 of cap free
Amount of items: 2
Items: 
Size: 774063 Color: 0
Size: 224734 Color: 3

Bin 4114: 1215 of cap free
Amount of items: 2
Items: 
Size: 704632 Color: 0
Size: 294154 Color: 4

Bin 4115: 1216 of cap free
Amount of items: 2
Items: 
Size: 541738 Color: 1
Size: 457047 Color: 0

Bin 4116: 1218 of cap free
Amount of items: 2
Items: 
Size: 630267 Color: 3
Size: 368516 Color: 2

Bin 4117: 1227 of cap free
Amount of items: 2
Items: 
Size: 538600 Color: 2
Size: 460174 Color: 3

Bin 4118: 1228 of cap free
Amount of items: 2
Items: 
Size: 770295 Color: 4
Size: 228478 Color: 3

Bin 4119: 1228 of cap free
Amount of items: 2
Items: 
Size: 704626 Color: 0
Size: 294147 Color: 3

Bin 4120: 1229 of cap free
Amount of items: 2
Items: 
Size: 536483 Color: 4
Size: 462289 Color: 2

Bin 4121: 1232 of cap free
Amount of items: 2
Items: 
Size: 571998 Color: 2
Size: 426771 Color: 3

Bin 4122: 1270 of cap free
Amount of items: 2
Items: 
Size: 550735 Color: 2
Size: 447996 Color: 0

Bin 4123: 1271 of cap free
Amount of items: 2
Items: 
Size: 732722 Color: 2
Size: 266008 Color: 4

Bin 4124: 1274 of cap free
Amount of items: 2
Items: 
Size: 651196 Color: 1
Size: 347531 Color: 4

Bin 4125: 1284 of cap free
Amount of items: 2
Items: 
Size: 518619 Color: 1
Size: 480098 Color: 3

Bin 4126: 1288 of cap free
Amount of items: 2
Items: 
Size: 619883 Color: 4
Size: 378830 Color: 3

Bin 4127: 1288 of cap free
Amount of items: 2
Items: 
Size: 630134 Color: 0
Size: 368579 Color: 3

Bin 4128: 1289 of cap free
Amount of items: 2
Items: 
Size: 531366 Color: 4
Size: 467346 Color: 2

Bin 4129: 1290 of cap free
Amount of items: 2
Items: 
Size: 645626 Color: 1
Size: 353085 Color: 4

Bin 4130: 1292 of cap free
Amount of items: 2
Items: 
Size: 525649 Color: 3
Size: 473060 Color: 2

Bin 4131: 1305 of cap free
Amount of items: 2
Items: 
Size: 500424 Color: 1
Size: 498272 Color: 4

Bin 4132: 1311 of cap free
Amount of items: 2
Items: 
Size: 562608 Color: 1
Size: 436082 Color: 2

Bin 4133: 1312 of cap free
Amount of items: 2
Items: 
Size: 689482 Color: 3
Size: 309207 Color: 4

Bin 4134: 1312 of cap free
Amount of items: 2
Items: 
Size: 754984 Color: 2
Size: 243705 Color: 1

Bin 4135: 1314 of cap free
Amount of items: 2
Items: 
Size: 728393 Color: 1
Size: 270294 Color: 3

Bin 4136: 1323 of cap free
Amount of items: 2
Items: 
Size: 751085 Color: 4
Size: 247593 Color: 0

Bin 4137: 1326 of cap free
Amount of items: 2
Items: 
Size: 642595 Color: 4
Size: 356080 Color: 0

Bin 4138: 1330 of cap free
Amount of items: 2
Items: 
Size: 607971 Color: 2
Size: 390700 Color: 0

Bin 4139: 1346 of cap free
Amount of items: 2
Items: 
Size: 653713 Color: 2
Size: 344942 Color: 3

Bin 4140: 1350 of cap free
Amount of items: 2
Items: 
Size: 709404 Color: 1
Size: 289247 Color: 3

Bin 4141: 1370 of cap free
Amount of items: 2
Items: 
Size: 735283 Color: 0
Size: 263348 Color: 2

Bin 4142: 1381 of cap free
Amount of items: 2
Items: 
Size: 743909 Color: 4
Size: 254711 Color: 2

Bin 4143: 1382 of cap free
Amount of items: 2
Items: 
Size: 651135 Color: 2
Size: 347484 Color: 4

Bin 4144: 1384 of cap free
Amount of items: 2
Items: 
Size: 569696 Color: 3
Size: 428921 Color: 2

Bin 4145: 1390 of cap free
Amount of items: 2
Items: 
Size: 738529 Color: 0
Size: 260082 Color: 2

Bin 4146: 1393 of cap free
Amount of items: 2
Items: 
Size: 602209 Color: 1
Size: 396399 Color: 3

Bin 4147: 1396 of cap free
Amount of items: 2
Items: 
Size: 699834 Color: 1
Size: 298771 Color: 0

Bin 4148: 1397 of cap free
Amount of items: 2
Items: 
Size: 588273 Color: 2
Size: 410331 Color: 4

Bin 4149: 1412 of cap free
Amount of items: 2
Items: 
Size: 774122 Color: 3
Size: 224467 Color: 1

Bin 4150: 1430 of cap free
Amount of items: 2
Items: 
Size: 610140 Color: 3
Size: 388431 Color: 4

Bin 4151: 1431 of cap free
Amount of items: 2
Items: 
Size: 726192 Color: 2
Size: 272378 Color: 1

Bin 4152: 1431 of cap free
Amount of items: 2
Items: 
Size: 590463 Color: 3
Size: 408107 Color: 2

Bin 4153: 1453 of cap free
Amount of items: 2
Items: 
Size: 781608 Color: 3
Size: 216940 Color: 0

Bin 4154: 1453 of cap free
Amount of items: 2
Items: 
Size: 691186 Color: 2
Size: 307362 Color: 3

Bin 4155: 1461 of cap free
Amount of items: 2
Items: 
Size: 588220 Color: 0
Size: 410320 Color: 4

Bin 4156: 1462 of cap free
Amount of items: 2
Items: 
Size: 542960 Color: 4
Size: 455579 Color: 1

Bin 4157: 1466 of cap free
Amount of items: 2
Items: 
Size: 760718 Color: 4
Size: 237817 Color: 2

Bin 4158: 1467 of cap free
Amount of items: 2
Items: 
Size: 761173 Color: 2
Size: 237361 Color: 4

Bin 4159: 1472 of cap free
Amount of items: 2
Items: 
Size: 685488 Color: 3
Size: 313041 Color: 1

Bin 4160: 1473 of cap free
Amount of items: 2
Items: 
Size: 638621 Color: 4
Size: 359907 Color: 1

Bin 4161: 1480 of cap free
Amount of items: 2
Items: 
Size: 500376 Color: 2
Size: 498145 Color: 0

Bin 4162: 1485 of cap free
Amount of items: 2
Items: 
Size: 626913 Color: 0
Size: 371603 Color: 4

Bin 4163: 1498 of cap free
Amount of items: 2
Items: 
Size: 530769 Color: 3
Size: 467734 Color: 4

Bin 4164: 1507 of cap free
Amount of items: 2
Items: 
Size: 754888 Color: 4
Size: 243606 Color: 1

Bin 4165: 1516 of cap free
Amount of items: 2
Items: 
Size: 698637 Color: 4
Size: 299848 Color: 1

Bin 4166: 1525 of cap free
Amount of items: 2
Items: 
Size: 508767 Color: 1
Size: 489709 Color: 2

Bin 4167: 1526 of cap free
Amount of items: 2
Items: 
Size: 726123 Color: 0
Size: 272352 Color: 1

Bin 4168: 1531 of cap free
Amount of items: 2
Items: 
Size: 783582 Color: 4
Size: 214888 Color: 0

Bin 4169: 1536 of cap free
Amount of items: 2
Items: 
Size: 750941 Color: 3
Size: 247524 Color: 1

Bin 4170: 1539 of cap free
Amount of items: 2
Items: 
Size: 653643 Color: 0
Size: 344819 Color: 3

Bin 4171: 1546 of cap free
Amount of items: 2
Items: 
Size: 653676 Color: 2
Size: 344779 Color: 0

Bin 4172: 1547 of cap free
Amount of items: 2
Items: 
Size: 651047 Color: 2
Size: 347407 Color: 1

Bin 4173: 1547 of cap free
Amount of items: 2
Items: 
Size: 670989 Color: 3
Size: 327465 Color: 2

Bin 4174: 1563 of cap free
Amount of items: 2
Items: 
Size: 726111 Color: 0
Size: 272327 Color: 1

Bin 4175: 1567 of cap free
Amount of items: 2
Items: 
Size: 754837 Color: 4
Size: 243597 Color: 2

Bin 4176: 1574 of cap free
Amount of items: 2
Items: 
Size: 557473 Color: 1
Size: 440954 Color: 4

Bin 4177: 1589 of cap free
Amount of items: 2
Items: 
Size: 750882 Color: 4
Size: 247530 Color: 3

Bin 4178: 1600 of cap free
Amount of items: 2
Items: 
Size: 635563 Color: 1
Size: 362838 Color: 0

Bin 4179: 1606 of cap free
Amount of items: 2
Items: 
Size: 651034 Color: 3
Size: 347361 Color: 4

Bin 4180: 1609 of cap free
Amount of items: 2
Items: 
Size: 621614 Color: 3
Size: 376778 Color: 0

Bin 4181: 1612 of cap free
Amount of items: 2
Items: 
Size: 534476 Color: 0
Size: 463913 Color: 2

Bin 4182: 1644 of cap free
Amount of items: 2
Items: 
Size: 754827 Color: 2
Size: 243530 Color: 1

Bin 4183: 1659 of cap free
Amount of items: 2
Items: 
Size: 711630 Color: 2
Size: 286712 Color: 1

Bin 4184: 1663 of cap free
Amount of items: 2
Items: 
Size: 678343 Color: 4
Size: 319995 Color: 3

Bin 4185: 1667 of cap free
Amount of items: 2
Items: 
Size: 665070 Color: 0
Size: 333264 Color: 3

Bin 4186: 1669 of cap free
Amount of items: 2
Items: 
Size: 594744 Color: 3
Size: 403588 Color: 1

Bin 4187: 1679 of cap free
Amount of items: 2
Items: 
Size: 769813 Color: 1
Size: 228509 Color: 4

Bin 4188: 1679 of cap free
Amount of items: 2
Items: 
Size: 525542 Color: 3
Size: 472780 Color: 1

Bin 4189: 1680 of cap free
Amount of items: 2
Items: 
Size: 766836 Color: 4
Size: 231485 Color: 2

Bin 4190: 1692 of cap free
Amount of items: 2
Items: 
Size: 586056 Color: 2
Size: 412253 Color: 0

Bin 4191: 1698 of cap free
Amount of items: 2
Items: 
Size: 750787 Color: 2
Size: 247516 Color: 1

Bin 4192: 1699 of cap free
Amount of items: 2
Items: 
Size: 698605 Color: 4
Size: 299697 Color: 1

Bin 4193: 1704 of cap free
Amount of items: 2
Items: 
Size: 709092 Color: 2
Size: 289205 Color: 1

Bin 4194: 1705 of cap free
Amount of items: 2
Items: 
Size: 732708 Color: 2
Size: 265588 Color: 3

Bin 4195: 1729 of cap free
Amount of items: 2
Items: 
Size: 676187 Color: 2
Size: 322085 Color: 3

Bin 4196: 1730 of cap free
Amount of items: 2
Items: 
Size: 799163 Color: 2
Size: 199108 Color: 0

Bin 4197: 1731 of cap free
Amount of items: 2
Items: 
Size: 766326 Color: 0
Size: 231944 Color: 4

Bin 4198: 1737 of cap free
Amount of items: 2
Items: 
Size: 569674 Color: 4
Size: 428590 Color: 2

Bin 4199: 1738 of cap free
Amount of items: 2
Items: 
Size: 738306 Color: 2
Size: 259957 Color: 4

Bin 4200: 1739 of cap free
Amount of items: 2
Items: 
Size: 773761 Color: 2
Size: 224501 Color: 3

Bin 4201: 1757 of cap free
Amount of items: 2
Items: 
Size: 594487 Color: 4
Size: 403757 Color: 3

Bin 4202: 1760 of cap free
Amount of items: 2
Items: 
Size: 534474 Color: 1
Size: 463767 Color: 4

Bin 4203: 1792 of cap free
Amount of items: 2
Items: 
Size: 651016 Color: 3
Size: 347193 Color: 4

Bin 4204: 1793 of cap free
Amount of items: 2
Items: 
Size: 555277 Color: 3
Size: 442931 Color: 1

Bin 4205: 1797 of cap free
Amount of items: 2
Items: 
Size: 586043 Color: 2
Size: 412161 Color: 0

Bin 4206: 1803 of cap free
Amount of items: 2
Items: 
Size: 693508 Color: 2
Size: 304690 Color: 4

Bin 4207: 1823 of cap free
Amount of items: 2
Items: 
Size: 650998 Color: 0
Size: 347180 Color: 4

Bin 4208: 1838 of cap free
Amount of items: 2
Items: 
Size: 738227 Color: 4
Size: 259936 Color: 1

Bin 4209: 1868 of cap free
Amount of items: 2
Items: 
Size: 709062 Color: 4
Size: 289071 Color: 1

Bin 4210: 1896 of cap free
Amount of items: 2
Items: 
Size: 750778 Color: 0
Size: 247327 Color: 1

Bin 4211: 1905 of cap free
Amount of items: 2
Items: 
Size: 594377 Color: 4
Size: 403719 Color: 3

Bin 4212: 1917 of cap free
Amount of items: 2
Items: 
Size: 598100 Color: 2
Size: 399984 Color: 4

Bin 4213: 1926 of cap free
Amount of items: 2
Items: 
Size: 773616 Color: 0
Size: 224459 Color: 4

Bin 4214: 1939 of cap free
Amount of items: 2
Items: 
Size: 538167 Color: 2
Size: 459895 Color: 3

Bin 4215: 1944 of cap free
Amount of items: 2
Items: 
Size: 769583 Color: 0
Size: 228474 Color: 1

Bin 4216: 1951 of cap free
Amount of items: 2
Items: 
Size: 580213 Color: 1
Size: 417837 Color: 2

Bin 4217: 1956 of cap free
Amount of items: 2
Items: 
Size: 685475 Color: 2
Size: 312570 Color: 0

Bin 4218: 1956 of cap free
Amount of items: 2
Items: 
Size: 699362 Color: 1
Size: 298683 Color: 2

Bin 4219: 1964 of cap free
Amount of items: 2
Items: 
Size: 621382 Color: 3
Size: 376655 Color: 2

Bin 4220: 1966 of cap free
Amount of items: 2
Items: 
Size: 717414 Color: 1
Size: 280621 Color: 2

Bin 4221: 1985 of cap free
Amount of items: 2
Items: 
Size: 708839 Color: 3
Size: 289177 Color: 4

Bin 4222: 1990 of cap free
Amount of items: 2
Items: 
Size: 555120 Color: 2
Size: 442891 Color: 1

Bin 4223: 1999 of cap free
Amount of items: 2
Items: 
Size: 518230 Color: 1
Size: 479772 Color: 3

Bin 4224: 2015 of cap free
Amount of items: 2
Items: 
Size: 594283 Color: 4
Size: 403703 Color: 3

Bin 4225: 2016 of cap free
Amount of items: 2
Items: 
Size: 518283 Color: 3
Size: 479702 Color: 0

Bin 4226: 2018 of cap free
Amount of items: 2
Items: 
Size: 609815 Color: 3
Size: 388168 Color: 0

Bin 4227: 2021 of cap free
Amount of items: 2
Items: 
Size: 598238 Color: 0
Size: 399742 Color: 2

Bin 4228: 2033 of cap free
Amount of items: 2
Items: 
Size: 783327 Color: 0
Size: 214641 Color: 4

Bin 4229: 2038 of cap free
Amount of items: 2
Items: 
Size: 616038 Color: 1
Size: 381925 Color: 3

Bin 4230: 2046 of cap free
Amount of items: 2
Items: 
Size: 601552 Color: 3
Size: 396403 Color: 4

Bin 4231: 2086 of cap free
Amount of items: 2
Items: 
Size: 538127 Color: 1
Size: 459788 Color: 0

Bin 4232: 2089 of cap free
Amount of items: 2
Items: 
Size: 678222 Color: 3
Size: 319690 Color: 1

Bin 4233: 2092 of cap free
Amount of items: 2
Items: 
Size: 621364 Color: 4
Size: 376545 Color: 2

Bin 4234: 2109 of cap free
Amount of items: 2
Items: 
Size: 557446 Color: 3
Size: 440446 Color: 4

Bin 4235: 2116 of cap free
Amount of items: 2
Items: 
Size: 769758 Color: 1
Size: 228127 Color: 4

Bin 4236: 2117 of cap free
Amount of items: 2
Items: 
Size: 590279 Color: 3
Size: 407605 Color: 0

Bin 4237: 2146 of cap free
Amount of items: 2
Items: 
Size: 512388 Color: 0
Size: 485467 Color: 1

Bin 4238: 2153 of cap free
Amount of items: 2
Items: 
Size: 653449 Color: 0
Size: 344399 Color: 2

Bin 4239: 2156 of cap free
Amount of items: 2
Items: 
Size: 732390 Color: 1
Size: 265455 Color: 2

Bin 4240: 2160 of cap free
Amount of items: 2
Items: 
Size: 569484 Color: 3
Size: 428357 Color: 0

Bin 4241: 2169 of cap free
Amount of items: 2
Items: 
Size: 515739 Color: 2
Size: 482093 Color: 4

Bin 4242: 2192 of cap free
Amount of items: 2
Items: 
Size: 598083 Color: 3
Size: 399726 Color: 2

Bin 4243: 2195 of cap free
Amount of items: 2
Items: 
Size: 725646 Color: 0
Size: 272160 Color: 1

Bin 4244: 2206 of cap free
Amount of items: 2
Items: 
Size: 799012 Color: 4
Size: 198783 Color: 0

Bin 4245: 2224 of cap free
Amount of items: 2
Items: 
Size: 585853 Color: 1
Size: 411924 Color: 0

Bin 4246: 2225 of cap free
Amount of items: 2
Items: 
Size: 750493 Color: 2
Size: 247283 Color: 1

Bin 4247: 2254 of cap free
Amount of items: 2
Items: 
Size: 723039 Color: 4
Size: 274708 Color: 3

Bin 4248: 2261 of cap free
Amount of items: 2
Items: 
Size: 670756 Color: 1
Size: 326984 Color: 4

Bin 4249: 2262 of cap free
Amount of items: 2
Items: 
Size: 675672 Color: 0
Size: 322067 Color: 3

Bin 4250: 2270 of cap free
Amount of items: 2
Items: 
Size: 783091 Color: 2
Size: 214640 Color: 3

Bin 4251: 2270 of cap free
Amount of items: 2
Items: 
Size: 664722 Color: 0
Size: 333009 Color: 4

Bin 4252: 2274 of cap free
Amount of items: 2
Items: 
Size: 590404 Color: 2
Size: 407323 Color: 3

Bin 4253: 2292 of cap free
Amount of items: 2
Items: 
Size: 555145 Color: 1
Size: 442564 Color: 0

Bin 4254: 2317 of cap free
Amount of items: 2
Items: 
Size: 594157 Color: 4
Size: 403527 Color: 2

Bin 4255: 2334 of cap free
Amount of items: 2
Items: 
Size: 638584 Color: 0
Size: 359083 Color: 3

Bin 4256: 2352 of cap free
Amount of items: 2
Items: 
Size: 499704 Color: 4
Size: 497945 Color: 3

Bin 4257: 2360 of cap free
Amount of items: 2
Items: 
Size: 524863 Color: 2
Size: 472778 Color: 0

Bin 4258: 2364 of cap free
Amount of items: 2
Items: 
Size: 569459 Color: 4
Size: 428178 Color: 3

Bin 4259: 2368 of cap free
Amount of items: 2
Items: 
Size: 653182 Color: 4
Size: 344451 Color: 0

Bin 4260: 2371 of cap free
Amount of items: 2
Items: 
Size: 798959 Color: 2
Size: 198671 Color: 1

Bin 4261: 2371 of cap free
Amount of items: 2
Items: 
Size: 703596 Color: 2
Size: 294034 Color: 1

Bin 4262: 2379 of cap free
Amount of items: 2
Items: 
Size: 512369 Color: 0
Size: 485253 Color: 2

Bin 4263: 2396 of cap free
Amount of items: 2
Items: 
Size: 609510 Color: 2
Size: 388095 Color: 0

Bin 4264: 2404 of cap free
Amount of items: 2
Items: 
Size: 675689 Color: 3
Size: 321908 Color: 0

Bin 4265: 2407 of cap free
Amount of items: 2
Items: 
Size: 634789 Color: 0
Size: 362805 Color: 4

Bin 4266: 2412 of cap free
Amount of items: 2
Items: 
Size: 670627 Color: 2
Size: 326962 Color: 0

Bin 4267: 2422 of cap free
Amount of items: 2
Items: 
Size: 754798 Color: 3
Size: 242781 Color: 1

Bin 4268: 2435 of cap free
Amount of items: 2
Items: 
Size: 703535 Color: 0
Size: 294031 Color: 1

Bin 4269: 2464 of cap free
Amount of items: 2
Items: 
Size: 754759 Color: 4
Size: 242778 Color: 3

Bin 4270: 2479 of cap free
Amount of items: 2
Items: 
Size: 507982 Color: 1
Size: 489540 Color: 3

Bin 4271: 2482 of cap free
Amount of items: 2
Items: 
Size: 518020 Color: 1
Size: 479499 Color: 2

Bin 4272: 2494 of cap free
Amount of items: 2
Items: 
Size: 590318 Color: 0
Size: 407189 Color: 3

Bin 4273: 2512 of cap free
Amount of items: 2
Items: 
Size: 641526 Color: 1
Size: 355963 Color: 0

Bin 4274: 2569 of cap free
Amount of items: 2
Items: 
Size: 783069 Color: 4
Size: 214363 Color: 2

Bin 4275: 2605 of cap free
Amount of items: 2
Items: 
Size: 590204 Color: 3
Size: 407192 Color: 4

Bin 4276: 2611 of cap free
Amount of items: 2
Items: 
Size: 638521 Color: 1
Size: 358869 Color: 4

Bin 4277: 2648 of cap free
Amount of items: 2
Items: 
Size: 634763 Color: 0
Size: 362590 Color: 3

Bin 4278: 2655 of cap free
Amount of items: 2
Items: 
Size: 594151 Color: 3
Size: 403195 Color: 0

Bin 4279: 2656 of cap free
Amount of items: 2
Items: 
Size: 670400 Color: 2
Size: 326945 Color: 0

Bin 4280: 2662 of cap free
Amount of items: 2
Items: 
Size: 754744 Color: 4
Size: 242595 Color: 3

Bin 4281: 2672 of cap free
Amount of items: 2
Items: 
Size: 717105 Color: 0
Size: 280224 Color: 3

Bin 4282: 2707 of cap free
Amount of items: 2
Items: 
Size: 606618 Color: 4
Size: 390676 Color: 0

Bin 4283: 2729 of cap free
Amount of items: 2
Items: 
Size: 597984 Color: 3
Size: 399288 Color: 0

Bin 4284: 2735 of cap free
Amount of items: 2
Items: 
Size: 507538 Color: 0
Size: 489728 Color: 1

Bin 4285: 2745 of cap free
Amount of items: 2
Items: 
Size: 578758 Color: 3
Size: 418498 Color: 1

Bin 4286: 2746 of cap free
Amount of items: 2
Items: 
Size: 754692 Color: 4
Size: 242563 Color: 0

Bin 4287: 2770 of cap free
Amount of items: 2
Items: 
Size: 600947 Color: 4
Size: 396284 Color: 2

Bin 4288: 2859 of cap free
Amount of items: 2
Items: 
Size: 768821 Color: 0
Size: 228321 Color: 1

Bin 4289: 2876 of cap free
Amount of items: 2
Items: 
Size: 621418 Color: 2
Size: 375707 Color: 4

Bin 4290: 2916 of cap free
Amount of items: 2
Items: 
Size: 589963 Color: 3
Size: 407122 Color: 1

Bin 4291: 2954 of cap free
Amount of items: 2
Items: 
Size: 638497 Color: 1
Size: 358550 Color: 4

Bin 4292: 2955 of cap free
Amount of items: 2
Items: 
Size: 593942 Color: 0
Size: 403104 Color: 2

Bin 4293: 2996 of cap free
Amount of items: 2
Items: 
Size: 606472 Color: 4
Size: 390533 Color: 3

Bin 4294: 3001 of cap free
Amount of items: 2
Items: 
Size: 754507 Color: 4
Size: 242493 Color: 0

Bin 4295: 3019 of cap free
Amount of items: 2
Items: 
Size: 634760 Color: 4
Size: 362222 Color: 3

Bin 4296: 3037 of cap free
Amount of items: 2
Items: 
Size: 743948 Color: 2
Size: 253016 Color: 3

Bin 4297: 3045 of cap free
Amount of items: 2
Items: 
Size: 507239 Color: 0
Size: 489717 Color: 1

Bin 4298: 3072 of cap free
Amount of items: 2
Items: 
Size: 765022 Color: 2
Size: 231907 Color: 4

Bin 4299: 3100 of cap free
Amount of items: 2
Items: 
Size: 529626 Color: 0
Size: 467275 Color: 3

Bin 4300: 3117 of cap free
Amount of items: 2
Items: 
Size: 600629 Color: 0
Size: 396255 Color: 4

Bin 4301: 3138 of cap free
Amount of items: 2
Items: 
Size: 692704 Color: 0
Size: 304159 Color: 1

Bin 4302: 3142 of cap free
Amount of items: 2
Items: 
Size: 768799 Color: 0
Size: 228060 Color: 3

Bin 4303: 3153 of cap free
Amount of items: 2
Items: 
Size: 738216 Color: 1
Size: 258632 Color: 0

Bin 4304: 3175 of cap free
Amount of items: 2
Items: 
Size: 754491 Color: 0
Size: 242335 Color: 3

Bin 4305: 3182 of cap free
Amount of items: 2
Items: 
Size: 507214 Color: 0
Size: 489605 Color: 1

Bin 4306: 3292 of cap free
Amount of items: 2
Items: 
Size: 523968 Color: 4
Size: 472741 Color: 3

Bin 4307: 3305 of cap free
Amount of items: 2
Items: 
Size: 660782 Color: 4
Size: 335914 Color: 3

Bin 4308: 3306 of cap free
Amount of items: 2
Items: 
Size: 523964 Color: 4
Size: 472731 Color: 1

Bin 4309: 3396 of cap free
Amount of items: 2
Items: 
Size: 754327 Color: 4
Size: 242278 Color: 0

Bin 4310: 3428 of cap free
Amount of items: 2
Items: 
Size: 798393 Color: 2
Size: 198180 Color: 1

Bin 4311: 3453 of cap free
Amount of items: 2
Items: 
Size: 593884 Color: 0
Size: 402664 Color: 4

Bin 4312: 3502 of cap free
Amount of items: 2
Items: 
Size: 637940 Color: 4
Size: 358559 Color: 0

Bin 4313: 3503 of cap free
Amount of items: 2
Items: 
Size: 664470 Color: 0
Size: 332028 Color: 1

Bin 4314: 3572 of cap free
Amount of items: 2
Items: 
Size: 637925 Color: 3
Size: 358504 Color: 0

Bin 4315: 3575 of cap free
Amount of items: 2
Items: 
Size: 703473 Color: 1
Size: 292953 Color: 2

Bin 4316: 3580 of cap free
Amount of items: 2
Items: 
Size: 529547 Color: 0
Size: 466874 Color: 4

Bin 4317: 3628 of cap free
Amount of items: 2
Items: 
Size: 703422 Color: 1
Size: 292951 Color: 4

Bin 4318: 3630 of cap free
Amount of items: 2
Items: 
Size: 609532 Color: 0
Size: 386839 Color: 4

Bin 4319: 3648 of cap free
Amount of items: 2
Items: 
Size: 529493 Color: 3
Size: 466860 Color: 4

Bin 4320: 3656 of cap free
Amount of items: 2
Items: 
Size: 798391 Color: 0
Size: 197954 Color: 4

Bin 4321: 3729 of cap free
Amount of items: 2
Items: 
Size: 772168 Color: 1
Size: 224104 Color: 3

Bin 4322: 3732 of cap free
Amount of items: 2
Items: 
Size: 615150 Color: 1
Size: 381119 Color: 3

Bin 4323: 3766 of cap free
Amount of items: 2
Items: 
Size: 615141 Color: 3
Size: 381094 Color: 0

Bin 4324: 3822 of cap free
Amount of items: 2
Items: 
Size: 637861 Color: 3
Size: 358318 Color: 1

Bin 4325: 3841 of cap free
Amount of items: 2
Items: 
Size: 628776 Color: 4
Size: 367384 Color: 1

Bin 4326: 3850 of cap free
Amount of items: 2
Items: 
Size: 593855 Color: 2
Size: 402296 Color: 3

Bin 4327: 3886 of cap free
Amount of items: 2
Items: 
Size: 772099 Color: 4
Size: 224016 Color: 0

Bin 4328: 3911 of cap free
Amount of items: 2
Items: 
Size: 593810 Color: 1
Size: 402280 Color: 3

Bin 4329: 3932 of cap free
Amount of items: 2
Items: 
Size: 772099 Color: 4
Size: 223970 Color: 0

Bin 4330: 3989 of cap free
Amount of items: 2
Items: 
Size: 670620 Color: 0
Size: 325392 Color: 2

Bin 4331: 3991 of cap free
Amount of items: 2
Items: 
Size: 738174 Color: 2
Size: 257836 Color: 3

Bin 4332: 4012 of cap free
Amount of items: 2
Items: 
Size: 530258 Color: 4
Size: 465731 Color: 0

Bin 4333: 4025 of cap free
Amount of items: 2
Items: 
Size: 725459 Color: 3
Size: 270517 Color: 1

Bin 4334: 4057 of cap free
Amount of items: 2
Items: 
Size: 585836 Color: 1
Size: 410108 Color: 0

Bin 4335: 4072 of cap free
Amount of items: 2
Items: 
Size: 771976 Color: 3
Size: 223953 Color: 2

Bin 4336: 4129 of cap free
Amount of items: 2
Items: 
Size: 497946 Color: 4
Size: 497926 Color: 3

Bin 4337: 4143 of cap free
Amount of items: 2
Items: 
Size: 536244 Color: 2
Size: 459614 Color: 4

Bin 4338: 4148 of cap free
Amount of items: 2
Items: 
Size: 593564 Color: 0
Size: 402289 Color: 1

Bin 4339: 4179 of cap free
Amount of items: 2
Items: 
Size: 569323 Color: 2
Size: 426499 Color: 3

Bin 4340: 4179 of cap free
Amount of items: 2
Items: 
Size: 772077 Color: 2
Size: 223745 Color: 4

Bin 4341: 4209 of cap free
Amount of items: 2
Items: 
Size: 579416 Color: 0
Size: 416376 Color: 3

Bin 4342: 4234 of cap free
Amount of items: 2
Items: 
Size: 497891 Color: 0
Size: 497876 Color: 2

Bin 4343: 4240 of cap free
Amount of items: 2
Items: 
Size: 593479 Color: 2
Size: 402282 Color: 1

Bin 4344: 4262 of cap free
Amount of items: 2
Items: 
Size: 738122 Color: 3
Size: 257617 Color: 1

Bin 4345: 4298 of cap free
Amount of items: 2
Items: 
Size: 497856 Color: 4
Size: 497847 Color: 0

Bin 4346: 4322 of cap free
Amount of items: 2
Items: 
Size: 593422 Color: 2
Size: 402257 Color: 4

Bin 4347: 4421 of cap free
Amount of items: 2
Items: 
Size: 753341 Color: 3
Size: 242239 Color: 1

Bin 4348: 4426 of cap free
Amount of items: 2
Items: 
Size: 675590 Color: 0
Size: 319985 Color: 3

Bin 4349: 4462 of cap free
Amount of items: 2
Items: 
Size: 497800 Color: 3
Size: 497739 Color: 1

Bin 4350: 4465 of cap free
Amount of items: 2
Items: 
Size: 568822 Color: 1
Size: 426714 Color: 2

Bin 4351: 4481 of cap free
Amount of items: 2
Items: 
Size: 593282 Color: 4
Size: 402238 Color: 3

Bin 4352: 4488 of cap free
Amount of items: 2
Items: 
Size: 670158 Color: 4
Size: 325355 Color: 0

Bin 4353: 4496 of cap free
Amount of items: 2
Items: 
Size: 753315 Color: 1
Size: 242190 Color: 0

Bin 4354: 4515 of cap free
Amount of items: 2
Items: 
Size: 771964 Color: 2
Size: 223522 Color: 3

Bin 4355: 4521 of cap free
Amount of items: 2
Items: 
Size: 725445 Color: 3
Size: 270035 Color: 1

Bin 4356: 4523 of cap free
Amount of items: 2
Items: 
Size: 593281 Color: 4
Size: 402197 Color: 2

Bin 4357: 4530 of cap free
Amount of items: 2
Items: 
Size: 642619 Color: 0
Size: 352852 Color: 4

Bin 4358: 4544 of cap free
Amount of items: 2
Items: 
Size: 780832 Color: 3
Size: 214625 Color: 4

Bin 4359: 4555 of cap free
Amount of items: 2
Items: 
Size: 497734 Color: 4
Size: 497712 Color: 2

Bin 4360: 4674 of cap free
Amount of items: 2
Items: 
Size: 555001 Color: 2
Size: 440326 Color: 0

Bin 4361: 4709 of cap free
Amount of items: 2
Items: 
Size: 554899 Color: 1
Size: 440393 Color: 2

Bin 4362: 4725 of cap free
Amount of items: 2
Items: 
Size: 522728 Color: 1
Size: 472548 Color: 2

Bin 4363: 4735 of cap free
Amount of items: 2
Items: 
Size: 715715 Color: 1
Size: 279551 Color: 2

Bin 4364: 4748 of cap free
Amount of items: 2
Items: 
Size: 497696 Color: 3
Size: 497557 Color: 4

Bin 4365: 4769 of cap free
Amount of items: 2
Items: 
Size: 737581 Color: 4
Size: 257651 Color: 3

Bin 4366: 4789 of cap free
Amount of items: 2
Items: 
Size: 725369 Color: 3
Size: 269843 Color: 0

Bin 4367: 4845 of cap free
Amount of items: 2
Items: 
Size: 752960 Color: 0
Size: 242196 Color: 1

Bin 4368: 4856 of cap free
Amount of items: 2
Items: 
Size: 554864 Color: 1
Size: 440281 Color: 4

Bin 4369: 4861 of cap free
Amount of items: 2
Items: 
Size: 584827 Color: 2
Size: 410313 Color: 1

Bin 4370: 4884 of cap free
Amount of items: 2
Items: 
Size: 701470 Color: 4
Size: 293647 Color: 1

Bin 4371: 4887 of cap free
Amount of items: 2
Items: 
Size: 752978 Color: 1
Size: 242136 Color: 4

Bin 4372: 4907 of cap free
Amount of items: 2
Items: 
Size: 568857 Color: 2
Size: 426237 Color: 0

Bin 4373: 4982 of cap free
Amount of items: 2
Items: 
Size: 529368 Color: 2
Size: 465651 Color: 0

Bin 4374: 5025 of cap free
Amount of items: 2
Items: 
Size: 522701 Color: 1
Size: 472275 Color: 0

Bin 4375: 5096 of cap free
Amount of items: 2
Items: 
Size: 780785 Color: 4
Size: 214120 Color: 2

Bin 4376: 5123 of cap free
Amount of items: 2
Items: 
Size: 568719 Color: 1
Size: 426159 Color: 0

Bin 4377: 5212 of cap free
Amount of items: 2
Items: 
Size: 771954 Color: 3
Size: 222835 Color: 1

Bin 4378: 5231 of cap free
Amount of items: 2
Items: 
Size: 715229 Color: 4
Size: 279541 Color: 2

Bin 4379: 5260 of cap free
Amount of items: 2
Items: 
Size: 554493 Color: 2
Size: 440248 Color: 3

Bin 4380: 5302 of cap free
Amount of items: 2
Items: 
Size: 703331 Color: 1
Size: 291368 Color: 3

Bin 4381: 5312 of cap free
Amount of items: 2
Items: 
Size: 752756 Color: 4
Size: 241933 Color: 3

Bin 4382: 5344 of cap free
Amount of items: 2
Items: 
Size: 674809 Color: 1
Size: 319848 Color: 3

Bin 4383: 5410 of cap free
Amount of items: 2
Items: 
Size: 568706 Color: 3
Size: 425885 Color: 4

Bin 4384: 5419 of cap free
Amount of items: 2
Items: 
Size: 669288 Color: 1
Size: 325294 Color: 2

Bin 4385: 5467 of cap free
Amount of items: 2
Items: 
Size: 758093 Color: 3
Size: 236441 Color: 2

Bin 4386: 5473 of cap free
Amount of items: 2
Items: 
Size: 522395 Color: 1
Size: 472133 Color: 3

Bin 4387: 5543 of cap free
Amount of items: 2
Items: 
Size: 780418 Color: 0
Size: 214040 Color: 3

Bin 4388: 5612 of cap free
Amount of items: 2
Items: 
Size: 522473 Color: 3
Size: 471916 Color: 2

Bin 4389: 5626 of cap free
Amount of items: 2
Items: 
Size: 497557 Color: 2
Size: 496818 Color: 4

Bin 4390: 5708 of cap free
Amount of items: 2
Items: 
Size: 714917 Color: 3
Size: 279376 Color: 1

Bin 4391: 5757 of cap free
Amount of items: 2
Items: 
Size: 737780 Color: 3
Size: 256464 Color: 1

Bin 4392: 5931 of cap free
Amount of items: 2
Items: 
Size: 522298 Color: 3
Size: 471772 Color: 1

Bin 4393: 6122 of cap free
Amount of items: 2
Items: 
Size: 568549 Color: 4
Size: 425330 Color: 2

Bin 4394: 6167 of cap free
Amount of items: 2
Items: 
Size: 714480 Color: 2
Size: 279354 Color: 3

Bin 4395: 6194 of cap free
Amount of items: 2
Items: 
Size: 669273 Color: 4
Size: 324534 Color: 1

Bin 4396: 6199 of cap free
Amount of items: 2
Items: 
Size: 780079 Color: 0
Size: 213723 Color: 2

Bin 4397: 6306 of cap free
Amount of items: 2
Items: 
Size: 650993 Color: 0
Size: 342702 Color: 2

Bin 4398: 6400 of cap free
Amount of items: 2
Items: 
Size: 497548 Color: 2
Size: 496053 Color: 4

Bin 4399: 6719 of cap free
Amount of items: 2
Items: 
Size: 752522 Color: 1
Size: 240760 Color: 3

Bin 4400: 6827 of cap free
Amount of items: 2
Items: 
Size: 554454 Color: 1
Size: 438720 Color: 0

Bin 4401: 6854 of cap free
Amount of items: 2
Items: 
Size: 714009 Color: 0
Size: 279138 Color: 3

Bin 4402: 7010 of cap free
Amount of items: 2
Items: 
Size: 554360 Color: 0
Size: 438631 Color: 4

Bin 4403: 7056 of cap free
Amount of items: 2
Items: 
Size: 621345 Color: 4
Size: 371600 Color: 0

Bin 4404: 7143 of cap free
Amount of items: 2
Items: 
Size: 568537 Color: 3
Size: 424321 Color: 1

Bin 4405: 7256 of cap free
Amount of items: 2
Items: 
Size: 606208 Color: 2
Size: 386537 Color: 0

Bin 4406: 7373 of cap free
Amount of items: 2
Items: 
Size: 554364 Color: 1
Size: 438264 Color: 0

Bin 4407: 7415 of cap free
Amount of items: 2
Items: 
Size: 701382 Color: 4
Size: 291204 Color: 1

Bin 4408: 7447 of cap free
Amount of items: 2
Items: 
Size: 554325 Color: 3
Size: 438229 Color: 1

Bin 4409: 7472 of cap free
Amount of items: 2
Items: 
Size: 621302 Color: 3
Size: 371227 Color: 2

Bin 4410: 7660 of cap free
Amount of items: 2
Items: 
Size: 650934 Color: 4
Size: 341407 Color: 1

Bin 4411: 7691 of cap free
Amount of items: 2
Items: 
Size: 650895 Color: 2
Size: 341415 Color: 4

Bin 4412: 7870 of cap free
Amount of items: 2
Items: 
Size: 567862 Color: 4
Size: 424269 Color: 0

Bin 4413: 7940 of cap free
Amount of items: 2
Items: 
Size: 496050 Color: 4
Size: 496011 Color: 3

Bin 4414: 7955 of cap free
Amount of items: 2
Items: 
Size: 768538 Color: 1
Size: 223508 Color: 3

Bin 4415: 8071 of cap free
Amount of items: 2
Items: 
Size: 567663 Color: 3
Size: 424267 Color: 0

Bin 4416: 8207 of cap free
Amount of items: 2
Items: 
Size: 553592 Color: 0
Size: 438202 Color: 2

Bin 4417: 8374 of cap free
Amount of items: 2
Items: 
Size: 512148 Color: 2
Size: 479479 Color: 4

Bin 4418: 8397 of cap free
Amount of items: 2
Items: 
Size: 584655 Color: 4
Size: 406949 Color: 3

Bin 4419: 8460 of cap free
Amount of items: 2
Items: 
Size: 713875 Color: 1
Size: 277666 Color: 3

Bin 4420: 8467 of cap free
Amount of items: 2
Items: 
Size: 512088 Color: 2
Size: 479446 Color: 3

Bin 4421: 8534 of cap free
Amount of items: 2
Items: 
Size: 768529 Color: 0
Size: 222938 Color: 3

Bin 4422: 8650 of cap free
Amount of items: 2
Items: 
Size: 650663 Color: 3
Size: 340688 Color: 0

Bin 4423: 8656 of cap free
Amount of items: 2
Items: 
Size: 584458 Color: 3
Size: 406887 Color: 2

Bin 4424: 8731 of cap free
Amount of items: 2
Items: 
Size: 798364 Color: 1
Size: 192906 Color: 3

Bin 4425: 8760 of cap free
Amount of items: 2
Items: 
Size: 768465 Color: 2
Size: 222776 Color: 3

Bin 4426: 8859 of cap free
Amount of items: 2
Items: 
Size: 606186 Color: 0
Size: 384956 Color: 2

Bin 4427: 9003 of cap free
Amount of items: 2
Items: 
Size: 606137 Color: 1
Size: 384861 Color: 3

Bin 4428: 9341 of cap free
Amount of items: 2
Items: 
Size: 552494 Color: 3
Size: 438166 Color: 0

Bin 4429: 9459 of cap free
Amount of items: 2
Items: 
Size: 552725 Color: 0
Size: 437817 Color: 2

Bin 4430: 9933 of cap free
Amount of items: 2
Items: 
Size: 720937 Color: 3
Size: 269131 Color: 0

Bin 4431: 10139 of cap free
Amount of items: 2
Items: 
Size: 732164 Color: 1
Size: 257698 Color: 4

Bin 4432: 10381 of cap free
Amount of items: 2
Items: 
Size: 698518 Color: 4
Size: 291102 Color: 3

Bin 4433: 10566 of cap free
Amount of items: 2
Items: 
Size: 731914 Color: 1
Size: 257521 Color: 3

Bin 4434: 10657 of cap free
Amount of items: 2
Items: 
Size: 737735 Color: 3
Size: 251609 Color: 1

Bin 4435: 11253 of cap free
Amount of items: 2
Items: 
Size: 529163 Color: 2
Size: 459585 Color: 3

Bin 4436: 11284 of cap free
Amount of items: 2
Items: 
Size: 529132 Color: 3
Size: 459585 Color: 4

Bin 4437: 11800 of cap free
Amount of items: 2
Items: 
Size: 550451 Color: 0
Size: 437750 Color: 3

Bin 4438: 12053 of cap free
Amount of items: 2
Items: 
Size: 550236 Color: 1
Size: 437712 Color: 2

Bin 4439: 12198 of cap free
Amount of items: 2
Items: 
Size: 550195 Color: 3
Size: 437608 Color: 1

Bin 4440: 12336 of cap free
Amount of items: 2
Items: 
Size: 766113 Color: 4
Size: 221552 Color: 3

Bin 4441: 12799 of cap free
Amount of items: 2
Items: 
Size: 668203 Color: 1
Size: 318999 Color: 3

Bin 4442: 12894 of cap free
Amount of items: 2
Items: 
Size: 606125 Color: 4
Size: 380982 Color: 0

Bin 4443: 12998 of cap free
Amount of items: 2
Items: 
Size: 698395 Color: 0
Size: 288608 Color: 3

Bin 4444: 13390 of cap free
Amount of items: 2
Items: 
Size: 584391 Color: 3
Size: 402220 Color: 4

Bin 4445: 13521 of cap free
Amount of items: 2
Items: 
Size: 584321 Color: 1
Size: 402159 Color: 2

Bin 4446: 13654 of cap free
Amount of items: 2
Items: 
Size: 798318 Color: 1
Size: 188029 Color: 3

Bin 4447: 13701 of cap free
Amount of items: 2
Items: 
Size: 634738 Color: 0
Size: 351562 Color: 3

Bin 4448: 13712 of cap free
Amount of items: 2
Items: 
Size: 584184 Color: 0
Size: 402105 Color: 3

Bin 4449: 14001 of cap free
Amount of items: 2
Items: 
Size: 548553 Color: 2
Size: 437447 Color: 3

Bin 4450: 14092 of cap free
Amount of items: 2
Items: 
Size: 548516 Color: 2
Size: 437393 Color: 1

Bin 4451: 14132 of cap free
Amount of items: 2
Items: 
Size: 697197 Color: 3
Size: 288672 Color: 4

Bin 4452: 14320 of cap free
Amount of items: 2
Items: 
Size: 548255 Color: 4
Size: 437426 Color: 2

Bin 4453: 14391 of cap free
Amount of items: 2
Items: 
Size: 548198 Color: 0
Size: 437412 Color: 2

Bin 4454: 15340 of cap free
Amount of items: 2
Items: 
Size: 548485 Color: 2
Size: 436176 Color: 1

Bin 4455: 15472 of cap free
Amount of items: 2
Items: 
Size: 633440 Color: 1
Size: 351089 Color: 4

Bin 4456: 15876 of cap free
Amount of items: 2
Items: 
Size: 548160 Color: 1
Size: 435965 Color: 4

Bin 4457: 16026 of cap free
Amount of items: 2
Items: 
Size: 548106 Color: 0
Size: 435869 Color: 4

Bin 4458: 16116 of cap free
Amount of items: 2
Items: 
Size: 548092 Color: 1
Size: 435793 Color: 3

Bin 4459: 16699 of cap free
Amount of items: 2
Items: 
Size: 547968 Color: 4
Size: 435334 Color: 0

Bin 4460: 16941 of cap free
Amount of items: 2
Items: 
Size: 547856 Color: 4
Size: 435204 Color: 2

Bin 4461: 17226 of cap free
Amount of items: 2
Items: 
Size: 547851 Color: 1
Size: 434924 Color: 2

Bin 4462: 17776 of cap free
Amount of items: 2
Items: 
Size: 547746 Color: 4
Size: 434479 Color: 1

Bin 4463: 17843 of cap free
Amount of items: 2
Items: 
Size: 547687 Color: 3
Size: 434471 Color: 1

Bin 4464: 17850 of cap free
Amount of items: 2
Items: 
Size: 547724 Color: 1
Size: 434427 Color: 3

Bin 4465: 17937 of cap free
Amount of items: 2
Items: 
Size: 547682 Color: 3
Size: 434382 Color: 0

Bin 4466: 17957 of cap free
Amount of items: 2
Items: 
Size: 713768 Color: 0
Size: 268276 Color: 1

Bin 4467: 18051 of cap free
Amount of items: 2
Items: 
Size: 522379 Color: 1
Size: 459571 Color: 4

Bin 4468: 18057 of cap free
Amount of items: 2
Items: 
Size: 547630 Color: 4
Size: 434314 Color: 2

Bin 4469: 18486 of cap free
Amount of items: 2
Items: 
Size: 547220 Color: 3
Size: 434295 Color: 2

Bin 4470: 18568 of cap free
Amount of items: 2
Items: 
Size: 521939 Color: 3
Size: 459494 Color: 4

Bin 4471: 19312 of cap free
Amount of items: 2
Items: 
Size: 546567 Color: 3
Size: 434122 Color: 1

Bin 4472: 19478 of cap free
Amount of items: 2
Items: 
Size: 546562 Color: 1
Size: 433961 Color: 0

Bin 4473: 19789 of cap free
Amount of items: 2
Items: 
Size: 546461 Color: 1
Size: 433751 Color: 3

Bin 4474: 20158 of cap free
Amount of items: 2
Items: 
Size: 546463 Color: 3
Size: 433380 Color: 1

Bin 4475: 20224 of cap free
Amount of items: 2
Items: 
Size: 584220 Color: 3
Size: 395557 Color: 0

Bin 4476: 20475 of cap free
Amount of items: 2
Items: 
Size: 584166 Color: 1
Size: 395360 Color: 3

Bin 4477: 20642 of cap free
Amount of items: 2
Items: 
Size: 520704 Color: 3
Size: 458655 Color: 4

Bin 4478: 20819 of cap free
Amount of items: 2
Items: 
Size: 545825 Color: 4
Size: 433357 Color: 0

Bin 4479: 20858 of cap free
Amount of items: 2
Items: 
Size: 545812 Color: 4
Size: 433331 Color: 2

Bin 4480: 20987 of cap free
Amount of items: 2
Items: 
Size: 584163 Color: 1
Size: 394851 Color: 3

Bin 4481: 21029 of cap free
Amount of items: 2
Items: 
Size: 545646 Color: 0
Size: 433326 Color: 3

Bin 4482: 21889 of cap free
Amount of items: 2
Items: 
Size: 544867 Color: 4
Size: 433245 Color: 3

Bin 4483: 22097 of cap free
Amount of items: 2
Items: 
Size: 544802 Color: 4
Size: 433102 Color: 3

Bin 4484: 22135 of cap free
Amount of items: 2
Items: 
Size: 798252 Color: 2
Size: 179614 Color: 3

Bin 4485: 22452 of cap free
Amount of items: 2
Items: 
Size: 660597 Color: 4
Size: 316952 Color: 1

Bin 4486: 23823 of cap free
Amount of items: 2
Items: 
Size: 606064 Color: 2
Size: 370114 Color: 4

Bin 4487: 27575 of cap free
Amount of items: 2
Items: 
Size: 605174 Color: 1
Size: 367252 Color: 4

Bin 4488: 29665 of cap free
Amount of items: 2
Items: 
Size: 798282 Color: 3
Size: 172054 Color: 1

Bin 4489: 31666 of cap free
Amount of items: 2
Items: 
Size: 711395 Color: 4
Size: 256940 Color: 3

Bin 4490: 32979 of cap free
Amount of items: 2
Items: 
Size: 511588 Color: 1
Size: 455434 Color: 3

Bin 4491: 33303 of cap free
Amount of items: 2
Items: 
Size: 511560 Color: 3
Size: 455138 Color: 4

Bin 4492: 33519 of cap free
Amount of items: 2
Items: 
Size: 511464 Color: 2
Size: 455018 Color: 4

Bin 4493: 39871 of cap free
Amount of items: 2
Items: 
Size: 798080 Color: 2
Size: 162050 Color: 3

Bin 4494: 48607 of cap free
Amount of items: 2
Items: 
Size: 584161 Color: 1
Size: 367233 Color: 2

Bin 4495: 50288 of cap free
Amount of items: 2
Items: 
Size: 697162 Color: 0
Size: 252551 Color: 3

Bin 4496: 52339 of cap free
Amount of items: 2
Items: 
Size: 697008 Color: 4
Size: 250654 Color: 2

Bin 4497: 55906 of cap free
Amount of items: 2
Items: 
Size: 511098 Color: 0
Size: 432997 Color: 3

Bin 4498: 60402 of cap free
Amount of items: 2
Items: 
Size: 688153 Color: 2
Size: 251446 Color: 4

Bin 4499: 64390 of cap free
Amount of items: 2
Items: 
Size: 511422 Color: 3
Size: 424189 Color: 4

Bin 4500: 66471 of cap free
Amount of items: 2
Items: 
Size: 511050 Color: 3
Size: 422480 Color: 4

Bin 4501: 66523 of cap free
Amount of items: 2
Items: 
Size: 511036 Color: 2
Size: 422442 Color: 4

Bin 4502: 66635 of cap free
Amount of items: 2
Items: 
Size: 510927 Color: 3
Size: 422439 Color: 4

Bin 4503: 66783 of cap free
Amount of items: 2
Items: 
Size: 510818 Color: 3
Size: 422400 Color: 1

Bin 4504: 70427 of cap free
Amount of items: 2
Items: 
Size: 507303 Color: 1
Size: 422271 Color: 4

Bin 4505: 71216 of cap free
Amount of items: 2
Items: 
Size: 506588 Color: 3
Size: 422197 Color: 0

Bin 4506: 71366 of cap free
Amount of items: 2
Items: 
Size: 506454 Color: 0
Size: 422181 Color: 1

Bin 4507: 71413 of cap free
Amount of items: 2
Items: 
Size: 506496 Color: 1
Size: 422092 Color: 2

Bin 4508: 71838 of cap free
Amount of items: 2
Items: 
Size: 506203 Color: 1
Size: 421960 Color: 0

Bin 4509: 72444 of cap free
Amount of items: 2
Items: 
Size: 505958 Color: 2
Size: 421599 Color: 0

Bin 4510: 74121 of cap free
Amount of items: 2
Items: 
Size: 504331 Color: 2
Size: 421549 Color: 0

Bin 4511: 75472 of cap free
Amount of items: 2
Items: 
Size: 798076 Color: 0
Size: 126453 Color: 3

Bin 4512: 201754 of cap free
Amount of items: 1
Items: 
Size: 798247 Color: 3

Bin 4513: 201948 of cap free
Amount of items: 1
Items: 
Size: 798053 Color: 3

Bin 4514: 202533 of cap free
Amount of items: 1
Items: 
Size: 797468 Color: 3

Bin 4515: 202633 of cap free
Amount of items: 1
Items: 
Size: 797368 Color: 1

Bin 4516: 202786 of cap free
Amount of items: 1
Items: 
Size: 797215 Color: 0

Bin 4517: 202846 of cap free
Amount of items: 1
Items: 
Size: 797155 Color: 1

Bin 4518: 203052 of cap free
Amount of items: 1
Items: 
Size: 796949 Color: 0

Bin 4519: 204207 of cap free
Amount of items: 1
Items: 
Size: 795794 Color: 0

Bin 4520: 204540 of cap free
Amount of items: 1
Items: 
Size: 795461 Color: 1

Bin 4521: 204687 of cap free
Amount of items: 1
Items: 
Size: 795314 Color: 2

Bin 4522: 204916 of cap free
Amount of items: 1
Items: 
Size: 795085 Color: 2

Bin 4523: 206241 of cap free
Amount of items: 1
Items: 
Size: 793760 Color: 3

Bin 4524: 206287 of cap free
Amount of items: 1
Items: 
Size: 793714 Color: 3

Bin 4525: 206323 of cap free
Amount of items: 1
Items: 
Size: 793678 Color: 3

Bin 4526: 206450 of cap free
Amount of items: 1
Items: 
Size: 793551 Color: 3

Bin 4527: 206589 of cap free
Amount of items: 1
Items: 
Size: 793412 Color: 4

Bin 4528: 206750 of cap free
Amount of items: 1
Items: 
Size: 793251 Color: 2

Bin 4529: 207283 of cap free
Amount of items: 1
Items: 
Size: 792718 Color: 1

Bin 4530: 207470 of cap free
Amount of items: 1
Items: 
Size: 792531 Color: 1

Bin 4531: 207776 of cap free
Amount of items: 1
Items: 
Size: 792225 Color: 2

Bin 4532: 209570 of cap free
Amount of items: 1
Items: 
Size: 790431 Color: 3

Bin 4533: 209656 of cap free
Amount of items: 1
Items: 
Size: 790345 Color: 1

Bin 4534: 209664 of cap free
Amount of items: 1
Items: 
Size: 790337 Color: 0

Bin 4535: 209684 of cap free
Amount of items: 1
Items: 
Size: 790317 Color: 0

Bin 4536: 209813 of cap free
Amount of items: 1
Items: 
Size: 790188 Color: 1

Bin 4537: 209843 of cap free
Amount of items: 1
Items: 
Size: 790158 Color: 3

Bin 4538: 212105 of cap free
Amount of items: 1
Items: 
Size: 787896 Color: 3

Bin 4539: 212690 of cap free
Amount of items: 1
Items: 
Size: 787311 Color: 4

Bin 4540: 213669 of cap free
Amount of items: 1
Items: 
Size: 786332 Color: 1

Bin 4541: 219987 of cap free
Amount of items: 1
Items: 
Size: 780014 Color: 2

Bin 4542: 221373 of cap free
Amount of items: 1
Items: 
Size: 778628 Color: 1

Bin 4543: 221413 of cap free
Amount of items: 1
Items: 
Size: 778588 Color: 2

Total size: 4532441801
Total free space: 10562742

