Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 1
Size: 3064 Color: 0
Size: 368 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 1
Size: 2444 Color: 2
Size: 608 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 0
Size: 2496 Color: 2
Size: 184 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 2
Size: 1852 Color: 0
Size: 232 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5292 Color: 1
Size: 1716 Color: 3
Size: 352 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5320 Color: 2
Size: 1836 Color: 0
Size: 204 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 3
Size: 1608 Color: 1
Size: 304 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 1
Size: 1696 Color: 2
Size: 152 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 0
Size: 1536 Color: 3
Size: 276 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 0
Size: 1488 Color: 4
Size: 288 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 2
Size: 1578 Color: 3
Size: 122 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 4
Size: 1374 Color: 2
Size: 288 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 0
Size: 1348 Color: 2
Size: 264 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 1
Size: 1272 Color: 0
Size: 320 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 4
Size: 964 Color: 0
Size: 536 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 2
Size: 1244 Color: 4
Size: 240 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 3
Size: 991 Color: 0
Size: 472 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 1
Size: 1026 Color: 0
Size: 384 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6007 Color: 2
Size: 1081 Color: 0
Size: 272 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6085 Color: 1
Size: 1117 Color: 0
Size: 158 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 4
Size: 1102 Color: 0
Size: 128 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 1
Size: 940 Color: 0
Size: 272 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 0
Size: 760 Color: 4
Size: 400 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 3
Size: 968 Color: 1
Size: 208 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6218 Color: 2
Size: 942 Color: 0
Size: 200 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 4
Size: 612 Color: 3
Size: 520 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6275 Color: 0
Size: 705 Color: 1
Size: 380 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 0
Size: 699 Color: 2
Size: 378 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6382 Color: 0
Size: 642 Color: 1
Size: 336 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 4
Size: 719 Color: 1
Size: 240 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6419 Color: 2
Size: 799 Color: 0
Size: 142 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 4
Size: 780 Color: 2
Size: 156 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6459 Color: 3
Size: 869 Color: 0
Size: 32 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 2
Size: 731 Color: 2
Size: 146 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6486 Color: 1
Size: 730 Color: 0
Size: 144 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6488 Color: 3
Size: 816 Color: 2
Size: 56 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6515 Color: 2
Size: 691 Color: 3
Size: 154 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 0
Size: 665 Color: 2
Size: 140 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 2
Size: 528 Color: 4
Size: 242 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6616 Color: 0
Size: 524 Color: 3
Size: 220 Color: 2

Bin 41: 1 of cap free
Amount of items: 9
Items: 
Size: 3681 Color: 3
Size: 608 Color: 1
Size: 538 Color: 1
Size: 492 Color: 1
Size: 480 Color: 4
Size: 478 Color: 3
Size: 468 Color: 2
Size: 328 Color: 1
Size: 286 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4139 Color: 3
Size: 3060 Color: 3
Size: 160 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4546 Color: 0
Size: 2685 Color: 2
Size: 128 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 4844 Color: 4
Size: 2367 Color: 4
Size: 148 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5470 Color: 3
Size: 1701 Color: 2
Size: 188 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5767 Color: 0
Size: 1460 Color: 2
Size: 132 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5934 Color: 2
Size: 1221 Color: 3
Size: 204 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 3
Size: 1136 Color: 0
Size: 186 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 3
Size: 1124 Color: 1
Size: 110 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 0
Size: 971 Color: 1
Size: 256 Color: 4

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 3
Size: 504 Color: 0
Size: 420 Color: 1

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 4
Size: 612 Color: 4
Size: 216 Color: 2

Bin 53: 2 of cap free
Amount of items: 7
Items: 
Size: 3683 Color: 4
Size: 742 Color: 3
Size: 695 Color: 4
Size: 654 Color: 4
Size: 608 Color: 2
Size: 560 Color: 0
Size: 416 Color: 2

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4608 Color: 0
Size: 2568 Color: 4
Size: 182 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4872 Color: 3
Size: 1423 Color: 3
Size: 1063 Color: 0

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 0
Size: 1850 Color: 2
Size: 368 Color: 3

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5337 Color: 1
Size: 1713 Color: 3
Size: 308 Color: 2

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6349 Color: 4
Size: 801 Color: 4
Size: 208 Color: 0

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6470 Color: 3
Size: 888 Color: 2

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 4489 Color: 2
Size: 2644 Color: 3
Size: 224 Color: 0

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5085 Color: 4
Size: 2088 Color: 3
Size: 184 Color: 0

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 1
Size: 2103 Color: 3
Size: 112 Color: 0

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 2
Size: 1897 Color: 0
Size: 296 Color: 2

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 1
Size: 1331 Color: 0
Size: 312 Color: 1

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6021 Color: 2
Size: 1336 Color: 1

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6403 Color: 2
Size: 954 Color: 1

Bin 67: 4 of cap free
Amount of items: 5
Items: 
Size: 3682 Color: 3
Size: 1622 Color: 3
Size: 1192 Color: 0
Size: 444 Color: 2
Size: 416 Color: 4

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 2
Size: 2712 Color: 2
Size: 304 Color: 3

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 4
Size: 2482 Color: 2
Size: 192 Color: 0

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 5305 Color: 2
Size: 1883 Color: 3
Size: 168 Color: 1

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 1
Size: 512 Color: 0
Size: 504 Color: 2

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6372 Color: 3
Size: 984 Color: 2

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6540 Color: 1
Size: 816 Color: 4

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4505 Color: 0
Size: 2238 Color: 4
Size: 612 Color: 2

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 4521 Color: 2
Size: 2834 Color: 4

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 5308 Color: 4
Size: 1909 Color: 1
Size: 138 Color: 4

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 5653 Color: 0
Size: 1544 Color: 0
Size: 158 Color: 2

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 2
Size: 856 Color: 3

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 1
Size: 792 Color: 4

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 0
Size: 2642 Color: 4
Size: 432 Color: 4

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 1
Size: 2346 Color: 0
Size: 684 Color: 3

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 4
Size: 2117 Color: 1
Size: 148 Color: 3

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 5501 Color: 0
Size: 1724 Color: 3
Size: 128 Color: 0

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 6146 Color: 4
Size: 1207 Color: 3

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 4
Size: 830 Color: 1

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 4386 Color: 3
Size: 2828 Color: 1
Size: 138 Color: 0

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 2
Size: 866 Color: 0
Size: 32 Color: 2

Bin 88: 9 of cap free
Amount of items: 3
Items: 
Size: 4121 Color: 3
Size: 3066 Color: 1
Size: 164 Color: 4

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 6173 Color: 4
Size: 1178 Color: 1

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 5913 Color: 2
Size: 1437 Color: 3

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 6020 Color: 2
Size: 1329 Color: 3

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 6578 Color: 4
Size: 771 Color: 3

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 5607 Color: 0
Size: 1740 Color: 3

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 6157 Color: 1
Size: 1190 Color: 4

Bin 95: 13 of cap free
Amount of items: 3
Items: 
Size: 6456 Color: 1
Size: 843 Color: 4
Size: 48 Color: 1

Bin 96: 14 of cap free
Amount of items: 25
Items: 
Size: 422 Color: 0
Size: 376 Color: 0
Size: 376 Color: 0
Size: 360 Color: 2
Size: 344 Color: 3
Size: 344 Color: 0
Size: 342 Color: 0
Size: 338 Color: 3
Size: 336 Color: 3
Size: 336 Color: 2
Size: 304 Color: 3
Size: 292 Color: 4
Size: 292 Color: 4
Size: 284 Color: 4
Size: 272 Color: 1
Size: 264 Color: 3
Size: 264 Color: 2
Size: 264 Color: 0
Size: 240 Color: 2
Size: 240 Color: 2
Size: 236 Color: 2
Size: 224 Color: 1
Size: 212 Color: 0
Size: 192 Color: 3
Size: 192 Color: 1

Bin 97: 14 of cap free
Amount of items: 3
Items: 
Size: 4123 Color: 0
Size: 2975 Color: 0
Size: 248 Color: 2

Bin 98: 15 of cap free
Amount of items: 3
Items: 
Size: 4820 Color: 1
Size: 2381 Color: 0
Size: 144 Color: 4

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 6428 Color: 1
Size: 917 Color: 2

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 5848 Color: 3
Size: 1496 Color: 2

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 4794 Color: 2
Size: 2548 Color: 4

Bin 102: 18 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 4
Size: 2100 Color: 2
Size: 160 Color: 0

Bin 103: 19 of cap free
Amount of items: 2
Items: 
Size: 4837 Color: 2
Size: 2504 Color: 1

Bin 104: 19 of cap free
Amount of items: 3
Items: 
Size: 5032 Color: 4
Size: 1551 Color: 0
Size: 758 Color: 4

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 3
Size: 1704 Color: 1

Bin 106: 19 of cap free
Amount of items: 2
Items: 
Size: 5880 Color: 1
Size: 1461 Color: 4

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 3962 Color: 1
Size: 3378 Color: 4

Bin 108: 21 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 2
Size: 1080 Color: 4

Bin 109: 25 of cap free
Amount of items: 3
Items: 
Size: 3688 Color: 4
Size: 2699 Color: 4
Size: 948 Color: 1

Bin 110: 25 of cap free
Amount of items: 2
Items: 
Size: 5101 Color: 4
Size: 2234 Color: 3

Bin 111: 28 of cap free
Amount of items: 5
Items: 
Size: 3684 Color: 4
Size: 1320 Color: 3
Size: 935 Color: 0
Size: 919 Color: 0
Size: 474 Color: 2

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 0
Size: 1036 Color: 4

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 5944 Color: 4
Size: 1386 Color: 3

Bin 114: 34 of cap free
Amount of items: 2
Items: 
Size: 6197 Color: 2
Size: 1129 Color: 1

Bin 115: 35 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 2
Size: 2532 Color: 3
Size: 1103 Color: 0

Bin 116: 36 of cap free
Amount of items: 2
Items: 
Size: 6072 Color: 0
Size: 1252 Color: 3

Bin 117: 39 of cap free
Amount of items: 3
Items: 
Size: 3692 Color: 0
Size: 3065 Color: 0
Size: 564 Color: 2

Bin 118: 40 of cap free
Amount of items: 2
Items: 
Size: 5418 Color: 1
Size: 1902 Color: 4

Bin 119: 41 of cap free
Amount of items: 2
Items: 
Size: 5071 Color: 1
Size: 2248 Color: 4

Bin 120: 41 of cap free
Amount of items: 4
Items: 
Size: 6366 Color: 1
Size: 905 Color: 4
Size: 32 Color: 1
Size: 16 Color: 4

Bin 121: 52 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 4
Size: 2872 Color: 1

Bin 122: 56 of cap free
Amount of items: 2
Items: 
Size: 4552 Color: 3
Size: 2752 Color: 2

Bin 123: 60 of cap free
Amount of items: 2
Items: 
Size: 5176 Color: 2
Size: 2124 Color: 1

Bin 124: 62 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 1
Size: 2393 Color: 2
Size: 785 Color: 4

Bin 125: 75 of cap free
Amount of items: 3
Items: 
Size: 4821 Color: 1
Size: 1832 Color: 2
Size: 632 Color: 2

Bin 126: 79 of cap free
Amount of items: 2
Items: 
Size: 5765 Color: 3
Size: 1516 Color: 1

Bin 127: 90 of cap free
Amount of items: 2
Items: 
Size: 6239 Color: 4
Size: 1031 Color: 2

Bin 128: 95 of cap free
Amount of items: 2
Items: 
Size: 5321 Color: 2
Size: 1944 Color: 4

Bin 129: 97 of cap free
Amount of items: 2
Items: 
Size: 4196 Color: 1
Size: 3067 Color: 2

Bin 130: 97 of cap free
Amount of items: 2
Items: 
Size: 5576 Color: 3
Size: 1687 Color: 1

Bin 131: 98 of cap free
Amount of items: 2
Items: 
Size: 4194 Color: 4
Size: 3068 Color: 1

Bin 132: 98 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 1
Size: 1028 Color: 2

Bin 133: 5522 of cap free
Amount of items: 11
Items: 
Size: 222 Color: 2
Size: 196 Color: 4
Size: 192 Color: 2
Size: 192 Color: 2
Size: 180 Color: 3
Size: 176 Color: 1
Size: 176 Color: 1
Size: 144 Color: 3
Size: 136 Color: 0
Size: 112 Color: 4
Size: 112 Color: 3

Total size: 971520
Total free space: 7360

