Capicity Bin: 7696
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3860 Color: 17
Size: 3204 Color: 4
Size: 632 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 7
Size: 3164 Color: 9
Size: 640 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4806 Color: 4
Size: 2410 Color: 17
Size: 480 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5275 Color: 4
Size: 2019 Color: 18
Size: 402 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5259 Color: 9
Size: 2031 Color: 18
Size: 406 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5291 Color: 17
Size: 2005 Color: 3
Size: 400 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5328 Color: 14
Size: 2132 Color: 18
Size: 236 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 14
Size: 1317 Color: 17
Size: 901 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 17
Size: 2104 Color: 2
Size: 112 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 18
Size: 1850 Color: 7
Size: 132 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 19
Size: 1788 Color: 9
Size: 168 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 4
Size: 963 Color: 15
Size: 921 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6093 Color: 3
Size: 1231 Color: 12
Size: 372 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6117 Color: 14
Size: 1379 Color: 6
Size: 200 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 7
Size: 1302 Color: 3
Size: 256 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 5
Size: 1337 Color: 7
Size: 198 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 12
Size: 1293 Color: 6
Size: 182 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 3
Size: 954 Color: 6
Size: 472 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 14
Size: 1156 Color: 1
Size: 224 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 9
Size: 848 Color: 5
Size: 552 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6300 Color: 6
Size: 1164 Color: 5
Size: 232 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6345 Color: 14
Size: 997 Color: 6
Size: 354 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 13
Size: 1044 Color: 5
Size: 254 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 12
Size: 784 Color: 14
Size: 488 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 9
Size: 734 Color: 11
Size: 480 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6494 Color: 2
Size: 1002 Color: 3
Size: 200 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6536 Color: 14
Size: 832 Color: 15
Size: 328 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 3
Size: 844 Color: 19
Size: 304 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 14
Size: 932 Color: 7
Size: 184 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6564 Color: 2
Size: 652 Color: 3
Size: 480 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 13
Size: 640 Color: 2
Size: 478 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 17
Size: 822 Color: 11
Size: 232 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 1
Size: 924 Color: 3
Size: 96 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 18
Size: 676 Color: 0
Size: 328 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 8
Size: 710 Color: 12
Size: 276 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 5
Size: 882 Color: 12
Size: 88 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 8
Size: 624 Color: 10
Size: 288 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6792 Color: 8
Size: 480 Color: 14
Size: 424 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 1
Size: 854 Color: 7
Size: 24 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 18
Size: 674 Color: 10
Size: 176 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 6
Size: 700 Color: 18
Size: 136 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 7
Size: 650 Color: 19
Size: 154 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 0
Size: 644 Color: 3
Size: 136 Color: 14

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 4803 Color: 16
Size: 2748 Color: 8
Size: 144 Color: 8

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 15
Size: 2604 Color: 11
Size: 264 Color: 16

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5176 Color: 19
Size: 2391 Color: 19
Size: 128 Color: 16

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5252 Color: 8
Size: 2075 Color: 14
Size: 368 Color: 9

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 14
Size: 2011 Color: 19
Size: 216 Color: 17

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 15
Size: 1751 Color: 8
Size: 482 Color: 0

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 5847 Color: 7
Size: 1848 Color: 12

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 12
Size: 1273 Color: 10
Size: 292 Color: 16

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 6212 Color: 15
Size: 1483 Color: 16

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6395 Color: 6
Size: 1300 Color: 3

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 6517 Color: 1
Size: 1178 Color: 7

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 7
Size: 964 Color: 14
Size: 86 Color: 3

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 7
Size: 1021 Color: 2

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6767 Color: 14
Size: 760 Color: 18
Size: 168 Color: 3

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 14
Size: 640 Color: 13
Size: 272 Color: 12

Bin 59: 2 of cap free
Amount of items: 5
Items: 
Size: 3854 Color: 3
Size: 1288 Color: 10
Size: 1064 Color: 18
Size: 968 Color: 5
Size: 520 Color: 9

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 3852 Color: 0
Size: 3202 Color: 15
Size: 640 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 4370 Color: 3
Size: 3172 Color: 8
Size: 152 Color: 18

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 14
Size: 2414 Color: 6
Size: 140 Color: 18

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 19
Size: 1600 Color: 0
Size: 156 Color: 3

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 5944 Color: 9
Size: 1462 Color: 14
Size: 288 Color: 11

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 6169 Color: 10
Size: 1281 Color: 14
Size: 244 Color: 7

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 4
Size: 827 Color: 19

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 2
Size: 2636 Color: 14
Size: 202 Color: 4

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5283 Color: 15
Size: 2102 Color: 2
Size: 308 Color: 19

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 5917 Color: 16
Size: 1776 Color: 2

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 5922 Color: 17
Size: 1771 Color: 11

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 6152 Color: 1
Size: 1541 Color: 16

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 6918 Color: 8
Size: 775 Color: 18

Bin 73: 4 of cap free
Amount of items: 5
Items: 
Size: 3864 Color: 3
Size: 1322 Color: 17
Size: 1322 Color: 16
Size: 632 Color: 12
Size: 552 Color: 11

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 4376 Color: 10
Size: 3188 Color: 11
Size: 128 Color: 13

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 6120 Color: 19
Size: 1572 Color: 2

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 2
Size: 1306 Color: 17

Bin 77: 4 of cap free
Amount of items: 4
Items: 
Size: 6552 Color: 18
Size: 1048 Color: 0
Size: 60 Color: 9
Size: 32 Color: 18

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 10
Size: 948 Color: 15

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 9
Size: 934 Color: 13

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 4780 Color: 5
Size: 2773 Color: 0
Size: 138 Color: 14

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 4807 Color: 7
Size: 2624 Color: 16
Size: 260 Color: 4

Bin 82: 5 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 14
Size: 2369 Color: 6
Size: 144 Color: 11

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 5231 Color: 9
Size: 2460 Color: 15

Bin 84: 5 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 16
Size: 1466 Color: 9
Size: 48 Color: 12

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 6501 Color: 11
Size: 1190 Color: 5

Bin 86: 5 of cap free
Amount of items: 3
Items: 
Size: 6593 Color: 19
Size: 1082 Color: 12
Size: 16 Color: 14

Bin 87: 5 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 8
Size: 1074 Color: 19

Bin 88: 5 of cap free
Amount of items: 2
Items: 
Size: 6825 Color: 4
Size: 866 Color: 17

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 1
Size: 785 Color: 6
Size: 16 Color: 2

Bin 90: 6 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 10
Size: 2411 Color: 18
Size: 691 Color: 1

Bin 91: 6 of cap free
Amount of items: 2
Items: 
Size: 6036 Color: 12
Size: 1654 Color: 11

Bin 92: 7 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 0
Size: 1127 Color: 7

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 3851 Color: 7
Size: 3205 Color: 3
Size: 632 Color: 0

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 10
Size: 1244 Color: 8

Bin 95: 8 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 18
Size: 1014 Color: 17
Size: 16 Color: 0

Bin 96: 8 of cap free
Amount of items: 2
Items: 
Size: 6705 Color: 15
Size: 983 Color: 6

Bin 97: 10 of cap free
Amount of items: 3
Items: 
Size: 5207 Color: 5
Size: 1385 Color: 6
Size: 1094 Color: 14

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 5571 Color: 2
Size: 2114 Color: 3

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 6924 Color: 2
Size: 761 Color: 18

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 5
Size: 808 Color: 13

Bin 101: 13 of cap free
Amount of items: 11
Items: 
Size: 3849 Color: 7
Size: 544 Color: 6
Size: 520 Color: 7
Size: 520 Color: 6
Size: 420 Color: 10
Size: 416 Color: 6
Size: 416 Color: 3
Size: 414 Color: 12
Size: 312 Color: 2
Size: 144 Color: 12
Size: 128 Color: 4

Bin 102: 14 of cap free
Amount of items: 27
Items: 
Size: 410 Color: 16
Size: 402 Color: 6
Size: 400 Color: 17
Size: 368 Color: 5
Size: 360 Color: 13
Size: 352 Color: 11
Size: 350 Color: 12
Size: 344 Color: 18
Size: 320 Color: 12
Size: 296 Color: 18
Size: 292 Color: 19
Size: 288 Color: 14
Size: 288 Color: 2
Size: 262 Color: 2
Size: 260 Color: 0
Size: 256 Color: 13
Size: 256 Color: 1
Size: 254 Color: 9
Size: 240 Color: 19
Size: 240 Color: 19
Size: 224 Color: 10
Size: 224 Color: 6
Size: 216 Color: 8
Size: 216 Color: 3
Size: 196 Color: 4
Size: 184 Color: 4
Size: 184 Color: 3

Bin 103: 14 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 19
Size: 1860 Color: 12
Size: 266 Color: 14

Bin 104: 14 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 4
Size: 1636 Color: 13
Size: 320 Color: 14

Bin 105: 15 of cap free
Amount of items: 2
Items: 
Size: 4816 Color: 7
Size: 2865 Color: 0

Bin 106: 15 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 1
Size: 1085 Color: 9

Bin 107: 16 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 7
Size: 2620 Color: 17
Size: 1184 Color: 11

Bin 108: 18 of cap free
Amount of items: 2
Items: 
Size: 5623 Color: 12
Size: 2055 Color: 10

Bin 109: 18 of cap free
Amount of items: 2
Items: 
Size: 6114 Color: 9
Size: 1564 Color: 15

Bin 110: 19 of cap free
Amount of items: 2
Items: 
Size: 6035 Color: 18
Size: 1642 Color: 2

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 4572 Color: 15
Size: 3104 Color: 9

Bin 112: 21 of cap free
Amount of items: 2
Items: 
Size: 5946 Color: 15
Size: 1729 Color: 9

Bin 113: 22 of cap free
Amount of items: 2
Items: 
Size: 6286 Color: 2
Size: 1388 Color: 10

Bin 114: 26 of cap free
Amount of items: 2
Items: 
Size: 6110 Color: 17
Size: 1560 Color: 5

Bin 115: 30 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 4
Size: 3204 Color: 12
Size: 554 Color: 18

Bin 116: 37 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 9
Size: 2771 Color: 11
Size: 140 Color: 3

Bin 117: 47 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 11
Size: 1176 Color: 10

Bin 118: 56 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 12
Size: 1640 Color: 1
Size: 172 Color: 14

Bin 119: 57 of cap free
Amount of items: 2
Items: 
Size: 5595 Color: 9
Size: 2044 Color: 6

Bin 120: 69 of cap free
Amount of items: 2
Items: 
Size: 6145 Color: 18
Size: 1482 Color: 16

Bin 121: 92 of cap free
Amount of items: 2
Items: 
Size: 6140 Color: 1
Size: 1464 Color: 8

Bin 122: 94 of cap free
Amount of items: 7
Items: 
Size: 3850 Color: 7
Size: 852 Color: 12
Size: 782 Color: 19
Size: 684 Color: 15
Size: 624 Color: 5
Size: 552 Color: 9
Size: 258 Color: 4

Bin 123: 98 of cap free
Amount of items: 2
Items: 
Size: 5162 Color: 17
Size: 2436 Color: 19

Bin 124: 98 of cap free
Amount of items: 2
Items: 
Size: 5736 Color: 5
Size: 1862 Color: 1

Bin 125: 108 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 16
Size: 2408 Color: 14
Size: 640 Color: 3

Bin 126: 112 of cap free
Amount of items: 2
Items: 
Size: 4808 Color: 12
Size: 2776 Color: 1

Bin 127: 115 of cap free
Amount of items: 2
Items: 
Size: 4374 Color: 2
Size: 3207 Color: 11

Bin 128: 117 of cap free
Amount of items: 2
Items: 
Size: 4373 Color: 12
Size: 3206 Color: 0

Bin 129: 119 of cap free
Amount of items: 2
Items: 
Size: 4369 Color: 4
Size: 3208 Color: 3

Bin 130: 120 of cap free
Amount of items: 2
Items: 
Size: 4556 Color: 14
Size: 3020 Color: 16

Bin 131: 120 of cap free
Amount of items: 2
Items: 
Size: 4802 Color: 5
Size: 2774 Color: 14

Bin 132: 122 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 11
Size: 2770 Color: 1
Size: 368 Color: 4

Bin 133: 5618 of cap free
Amount of items: 12
Items: 
Size: 208 Color: 16
Size: 208 Color: 10
Size: 208 Color: 5
Size: 184 Color: 18
Size: 178 Color: 4
Size: 176 Color: 2
Size: 172 Color: 9
Size: 164 Color: 17
Size: 164 Color: 14
Size: 160 Color: 14
Size: 128 Color: 10
Size: 128 Color: 8

Total size: 1015872
Total free space: 7696

