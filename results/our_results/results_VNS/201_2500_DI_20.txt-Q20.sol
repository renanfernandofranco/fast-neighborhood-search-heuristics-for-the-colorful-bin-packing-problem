Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 0
Size: 511 Color: 15
Size: 338 Color: 3
Size: 200 Color: 9
Size: 176 Color: 7

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 9
Size: 1018 Color: 15
Size: 84 Color: 0
Size: 60 Color: 18
Size: 56 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 12
Size: 731 Color: 6
Size: 52 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1835 Color: 18
Size: 519 Color: 9
Size: 102 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 7
Size: 484 Color: 13
Size: 88 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 3
Size: 374 Color: 2
Size: 100 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 14
Size: 378 Color: 9
Size: 80 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2052 Color: 16
Size: 308 Color: 1
Size: 96 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 17
Size: 272 Color: 7
Size: 130 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2092 Color: 14
Size: 272 Color: 11
Size: 92 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 6
Size: 200 Color: 18
Size: 132 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2170 Color: 2
Size: 206 Color: 19
Size: 80 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 1
Size: 204 Color: 6
Size: 72 Color: 15

Bin 14: 1 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 14
Size: 430 Color: 7
Size: 414 Color: 10
Size: 294 Color: 6
Size: 88 Color: 1

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 4
Size: 919 Color: 2

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 11
Size: 814 Color: 11
Size: 88 Color: 14

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 7
Size: 761 Color: 3
Size: 136 Color: 19

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 14
Size: 661 Color: 2
Size: 72 Color: 5

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1708 Color: 5
Size: 747 Color: 6

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 8
Size: 690 Color: 0

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1907 Color: 7
Size: 548 Color: 19

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 9
Size: 409 Color: 4
Size: 112 Color: 16

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 7
Size: 438 Color: 5
Size: 16 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2004 Color: 19
Size: 451 Color: 1

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2074 Color: 5
Size: 381 Color: 3

Bin 26: 2 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 0
Size: 1020 Color: 8
Size: 150 Color: 3
Size: 48 Color: 5

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 0
Size: 750 Color: 7
Size: 74 Color: 10

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 6
Size: 571 Color: 14
Size: 40 Color: 12

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 6
Size: 504 Color: 14
Size: 56 Color: 15

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 6
Size: 340 Color: 17
Size: 8 Color: 13

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 15
Size: 220 Color: 0
Size: 144 Color: 13

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 9
Size: 236 Color: 0
Size: 8 Color: 19

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1444 Color: 14
Size: 969 Color: 8
Size: 40 Color: 16

Bin 34: 3 of cap free
Amount of items: 4
Items: 
Size: 1545 Color: 1
Size: 844 Color: 5
Size: 32 Color: 4
Size: 32 Color: 3

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1901 Color: 19
Size: 380 Color: 12
Size: 172 Color: 6

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1915 Color: 7
Size: 538 Color: 16

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1923 Color: 13
Size: 530 Color: 5

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1948 Color: 7
Size: 505 Color: 13

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1967 Color: 3
Size: 486 Color: 4

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 18
Size: 891 Color: 14
Size: 40 Color: 14

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 12
Size: 724 Color: 10
Size: 40 Color: 12

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 3
Size: 306 Color: 6
Size: 204 Color: 7

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1804 Color: 6
Size: 647 Color: 12

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1874 Color: 17
Size: 577 Color: 18

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 3
Size: 445 Color: 15

Bin 46: 6 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 12
Size: 463 Color: 1
Size: 459 Color: 16
Size: 150 Color: 7
Size: 148 Color: 3

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 6
Size: 840 Color: 2
Size: 168 Color: 13

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 3
Size: 665 Color: 10
Size: 104 Color: 18

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 1
Size: 753 Color: 7
Size: 32 Color: 14

Bin 50: 6 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 16
Size: 284 Color: 8
Size: 8 Color: 3

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 2196 Color: 13
Size: 254 Color: 18

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 10
Size: 406 Color: 16
Size: 384 Color: 16

Bin 53: 8 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 8
Size: 618 Color: 5
Size: 32 Color: 10

Bin 54: 13 of cap free
Amount of items: 3
Items: 
Size: 1851 Color: 16
Size: 576 Color: 4
Size: 16 Color: 12

Bin 55: 14 of cap free
Amount of items: 2
Items: 
Size: 1814 Color: 16
Size: 628 Color: 18

Bin 56: 17 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 10
Size: 614 Color: 0
Size: 52 Color: 14

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1410 Color: 6
Size: 1028 Color: 1

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1590 Color: 9
Size: 846 Color: 6

Bin 59: 21 of cap free
Amount of items: 2
Items: 
Size: 1561 Color: 14
Size: 874 Color: 10

Bin 60: 23 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 2
Size: 845 Color: 18

Bin 61: 29 of cap free
Amount of items: 2
Items: 
Size: 1443 Color: 2
Size: 984 Color: 9

Bin 62: 36 of cap free
Amount of items: 16
Items: 
Size: 428 Color: 8
Size: 250 Color: 7
Size: 204 Color: 4
Size: 168 Color: 17
Size: 168 Color: 16
Size: 148 Color: 1
Size: 128 Color: 2
Size: 120 Color: 10
Size: 120 Color: 3
Size: 114 Color: 4
Size: 104 Color: 12
Size: 102 Color: 3
Size: 96 Color: 15
Size: 96 Color: 9
Size: 90 Color: 16
Size: 84 Color: 0

Bin 63: 37 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 5
Size: 1023 Color: 18

Bin 64: 44 of cap free
Amount of items: 2
Items: 
Size: 1391 Color: 6
Size: 1021 Color: 4

Bin 65: 45 of cap free
Amount of items: 2
Items: 
Size: 1389 Color: 13
Size: 1022 Color: 6

Bin 66: 2014 of cap free
Amount of items: 7
Items: 
Size: 90 Color: 19
Size: 64 Color: 12
Size: 64 Color: 10
Size: 64 Color: 0
Size: 60 Color: 13
Size: 52 Color: 2
Size: 48 Color: 14

Total size: 159640
Total free space: 2456

