Capicity Bin: 2032
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1079 Color: 2
Size: 795 Color: 1
Size: 94 Color: 0
Size: 64 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1130 Color: 2
Size: 846 Color: 0
Size: 56 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 2
Size: 758 Color: 1
Size: 52 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 2
Size: 657 Color: 0
Size: 94 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 1
Size: 611 Color: 4
Size: 38 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 2
Size: 518 Color: 2
Size: 36 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 4
Size: 461 Color: 2
Size: 90 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1501 Color: 1
Size: 363 Color: 2
Size: 168 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 443 Color: 2
Size: 50 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 1
Size: 418 Color: 4
Size: 36 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 3
Size: 371 Color: 4
Size: 74 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1630 Color: 2
Size: 276 Color: 3
Size: 126 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 2
Size: 255 Color: 3
Size: 50 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 0
Size: 201 Color: 2
Size: 100 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1744 Color: 1
Size: 230 Color: 2
Size: 58 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 0
Size: 228 Color: 4
Size: 46 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1759 Color: 4
Size: 229 Color: 4
Size: 44 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 206 Color: 2
Size: 44 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 1
Size: 206 Color: 2
Size: 36 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 2
Size: 209 Color: 3
Size: 40 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 2
Size: 158 Color: 1
Size: 64 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 0
Size: 186 Color: 2
Size: 40 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1825 Color: 3
Size: 191 Color: 2
Size: 16 Color: 4

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 1017 Color: 2
Size: 673 Color: 2
Size: 293 Color: 3
Size: 48 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 0
Size: 553 Color: 1
Size: 78 Color: 2

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 4
Size: 541 Color: 4
Size: 76 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 2
Size: 369 Color: 3
Size: 44 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 2
Size: 262 Color: 3
Size: 116 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 4
Size: 321 Color: 2
Size: 52 Color: 3

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1702 Color: 2
Size: 291 Color: 4
Size: 38 Color: 3

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 3
Size: 193 Color: 3
Size: 130 Color: 2

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1791 Color: 0
Size: 240 Color: 4

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 1
Size: 150 Color: 3
Size: 80 Color: 2

Bin 34: 1 of cap free
Amount of items: 5
Items: 
Size: 1805 Color: 3
Size: 190 Color: 4
Size: 24 Color: 0
Size: 8 Color: 1
Size: 4 Color: 2

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 1821 Color: 3
Size: 210 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 2
Size: 662 Color: 3
Size: 88 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 0
Size: 177 Color: 4
Size: 168 Color: 2

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 2
Size: 843 Color: 3
Size: 60 Color: 0

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 2
Size: 754 Color: 4
Size: 108 Color: 4

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 3
Size: 680 Color: 0
Size: 48 Color: 4

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 3
Size: 472 Color: 2
Size: 314 Color: 1

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1265 Color: 2
Size: 721 Color: 3
Size: 42 Color: 3

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 4
Size: 641 Color: 0
Size: 52 Color: 2

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 4
Size: 316 Color: 1
Size: 62 Color: 2

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 4
Size: 317 Color: 1

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1563 Color: 2
Size: 462 Color: 0

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 3
Size: 382 Color: 0

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1786 Color: 1
Size: 239 Color: 4

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1797 Color: 4
Size: 228 Color: 0

Bin 50: 9 of cap free
Amount of items: 17
Items: 
Size: 390 Color: 3
Size: 278 Color: 2
Size: 197 Color: 3
Size: 148 Color: 1
Size: 144 Color: 2
Size: 128 Color: 4
Size: 120 Color: 4
Size: 92 Color: 0
Size: 82 Color: 3
Size: 78 Color: 1
Size: 78 Color: 1
Size: 72 Color: 2
Size: 56 Color: 0
Size: 40 Color: 3
Size: 40 Color: 3
Size: 40 Color: 0
Size: 40 Color: 0

Bin 51: 9 of cap free
Amount of items: 2
Items: 
Size: 1441 Color: 4
Size: 582 Color: 1

Bin 52: 10 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 1
Size: 937 Color: 3
Size: 64 Color: 0

Bin 53: 10 of cap free
Amount of items: 2
Items: 
Size: 1771 Color: 3
Size: 251 Color: 0

Bin 54: 11 of cap free
Amount of items: 3
Items: 
Size: 1127 Color: 4
Size: 846 Color: 1
Size: 48 Color: 4

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 1
Size: 391 Color: 3

Bin 56: 12 of cap free
Amount of items: 2
Items: 
Size: 1682 Color: 0
Size: 338 Color: 3

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1747 Color: 3
Size: 271 Color: 0

Bin 58: 16 of cap free
Amount of items: 2
Items: 
Size: 1457 Color: 0
Size: 559 Color: 1

Bin 59: 16 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 3
Size: 294 Color: 0

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1534 Color: 1
Size: 481 Color: 0

Bin 61: 21 of cap free
Amount of items: 3
Items: 
Size: 1018 Color: 4
Size: 820 Color: 2
Size: 173 Color: 4

Bin 62: 21 of cap free
Amount of items: 2
Items: 
Size: 1591 Color: 4
Size: 420 Color: 3

Bin 63: 23 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 2
Size: 411 Color: 0
Size: 219 Color: 3

Bin 64: 26 of cap free
Amount of items: 2
Items: 
Size: 1681 Color: 1
Size: 325 Color: 0

Bin 65: 32 of cap free
Amount of items: 2
Items: 
Size: 1245 Color: 1
Size: 755 Color: 3

Bin 66: 1696 of cap free
Amount of items: 7
Items: 
Size: 72 Color: 2
Size: 62 Color: 4
Size: 60 Color: 1
Size: 38 Color: 1
Size: 36 Color: 0
Size: 34 Color: 3
Size: 34 Color: 0

Total size: 132080
Total free space: 2032

