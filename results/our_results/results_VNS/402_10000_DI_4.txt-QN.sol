Capicity Bin: 7696
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5207 Color: 306
Size: 2011 Color: 231
Size: 478 Color: 111

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 322
Size: 1462 Color: 207
Size: 520 Color: 120

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 324
Size: 1788 Color: 225
Size: 172 Color: 33

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 325
Size: 1636 Color: 217
Size: 320 Color: 86

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 326
Size: 1860 Color: 228
Size: 24 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 331
Size: 1482 Color: 210
Size: 276 Color: 75

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 337
Size: 1176 Color: 187
Size: 410 Color: 104

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 338
Size: 1322 Color: 201
Size: 260 Color: 70

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 342
Size: 1302 Color: 198
Size: 256 Color: 65

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6140 Color: 343
Size: 1184 Color: 189
Size: 372 Color: 98

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6145 Color: 344
Size: 997 Color: 173
Size: 554 Color: 125

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 346
Size: 844 Color: 156
Size: 691 Color: 142

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6169 Color: 347
Size: 1273 Color: 193
Size: 254 Color: 64

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 348
Size: 1231 Color: 191
Size: 288 Color: 79

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 351
Size: 1082 Color: 181
Size: 344 Color: 90

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 352
Size: 1322 Color: 202
Size: 88 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 360
Size: 1064 Color: 179
Size: 208 Color: 50

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 362
Size: 1085 Color: 182
Size: 138 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 363
Size: 954 Color: 168
Size: 260 Color: 69

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6494 Color: 364
Size: 882 Color: 161
Size: 320 Color: 87

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 368
Size: 964 Color: 170
Size: 184 Color: 42

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6552 Color: 369
Size: 784 Color: 150
Size: 360 Color: 94

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 370
Size: 652 Color: 138
Size: 482 Color: 116

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 372
Size: 852 Color: 158
Size: 266 Color: 73

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 373
Size: 932 Color: 165
Size: 184 Color: 39

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6593 Color: 374
Size: 963 Color: 169
Size: 140 Color: 20

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 379
Size: 632 Color: 129
Size: 406 Color: 103

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 389
Size: 785 Color: 151
Size: 128 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 390
Size: 760 Color: 146
Size: 152 Color: 25

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 392
Size: 624 Color: 127
Size: 254 Color: 63

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 397
Size: 676 Color: 140
Size: 144 Color: 24

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 398
Size: 624 Color: 126
Size: 182 Color: 38

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 399
Size: 640 Color: 134
Size: 164 Color: 29

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 400
Size: 640 Color: 135
Size: 140 Color: 21

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 402
Size: 640 Color: 133
Size: 132 Color: 16

Bin 36: 1 of cap free
Amount of items: 7
Items: 
Size: 3852 Color: 275
Size: 782 Color: 149
Size: 761 Color: 147
Size: 734 Color: 145
Size: 710 Color: 144
Size: 640 Color: 132
Size: 216 Color: 51

Bin 37: 1 of cap free
Amount of items: 5
Items: 
Size: 4540 Color: 288
Size: 1464 Color: 208
Size: 1317 Color: 200
Size: 202 Color: 47
Size: 172 Color: 34

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 5259 Color: 309
Size: 2436 Color: 247

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 5847 Color: 328
Size: 1848 Color: 226

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 5944 Color: 332
Size: 1751 Color: 222

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6212 Color: 349
Size: 1483 Color: 211

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 355
Size: 1379 Color: 204

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6395 Color: 358
Size: 1300 Color: 197

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6517 Color: 366
Size: 1178 Color: 188

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 380
Size: 1021 Color: 176

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 383
Size: 822 Color: 153
Size: 168 Color: 32

Bin 47: 2 of cap free
Amount of items: 5
Items: 
Size: 3908 Color: 281
Size: 3202 Color: 265
Size: 232 Color: 58
Size: 176 Color: 36
Size: 176 Color: 35

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 5283 Color: 311
Size: 2411 Color: 245

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 316
Size: 2104 Color: 238
Size: 112 Color: 10

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 323
Size: 1640 Color: 218
Size: 328 Color: 88

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6130 Color: 341
Size: 1564 Color: 214

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 382
Size: 1002 Color: 174

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 385
Size: 968 Color: 171

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 394
Size: 848 Color: 157

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 396
Size: 827 Color: 154

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 5917 Color: 329
Size: 1776 Color: 224

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 5922 Color: 330
Size: 1771 Color: 223

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 6035 Color: 334
Size: 1642 Color: 219
Size: 16 Color: 2

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6093 Color: 336
Size: 1600 Color: 216

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 6117 Color: 339
Size: 1560 Color: 213
Size: 16 Color: 1

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6152 Color: 345
Size: 1541 Color: 212

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 378
Size: 1048 Color: 178

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 384
Size: 983 Color: 172

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6792 Color: 391
Size: 901 Color: 162

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6918 Color: 401
Size: 775 Color: 148

Bin 66: 4 of cap free
Amount of items: 5
Items: 
Size: 3876 Color: 279
Size: 3172 Color: 263
Size: 264 Color: 72
Size: 196 Color: 43
Size: 184 Color: 41

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 314
Size: 2102 Color: 237
Size: 128 Color: 13

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 6120 Color: 340
Size: 1572 Color: 215

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 357
Size: 1306 Color: 199

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6536 Color: 367
Size: 1156 Color: 185

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 386
Size: 948 Color: 167

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 387
Size: 934 Color: 166

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 395
Size: 832 Color: 155

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 5231 Color: 307
Size: 2460 Color: 248

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5946 Color: 333
Size: 1729 Color: 221
Size: 16 Color: 0

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6398 Color: 359
Size: 1293 Color: 196

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6501 Color: 365
Size: 1190 Color: 190

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6564 Color: 371
Size: 1127 Color: 184

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 376
Size: 1074 Color: 180

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 6767 Color: 388
Size: 924 Color: 164

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 6825 Color: 393
Size: 866 Color: 160

Bin 82: 6 of cap free
Amount of items: 9
Items: 
Size: 3850 Color: 273
Size: 632 Color: 130
Size: 632 Color: 128
Size: 552 Color: 123
Size: 552 Color: 122
Size: 544 Color: 121
Size: 480 Color: 115
Size: 224 Color: 55
Size: 224 Color: 54

Bin 83: 6 of cap free
Amount of items: 4
Items: 
Size: 4588 Color: 291
Size: 2770 Color: 254
Size: 168 Color: 31
Size: 164 Color: 30

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 317
Size: 2114 Color: 239
Size: 96 Color: 9

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 327
Size: 1862 Color: 229

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 6036 Color: 335
Size: 1654 Color: 220

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 375
Size: 1094 Color: 183

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 381
Size: 1014 Color: 175

Bin 89: 7 of cap free
Amount of items: 2
Items: 
Size: 5275 Color: 310
Size: 2414 Color: 246

Bin 90: 8 of cap free
Amount of items: 5
Items: 
Size: 3864 Color: 278
Size: 3164 Color: 262
Size: 262 Color: 71
Size: 200 Color: 45
Size: 198 Color: 44

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 6300 Color: 354
Size: 1388 Color: 206

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 361
Size: 1244 Color: 192

Bin 93: 9 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 303
Size: 2005 Color: 230
Size: 520 Color: 119

Bin 94: 9 of cap free
Amount of items: 3
Items: 
Size: 5595 Color: 320
Size: 2044 Color: 234
Size: 48 Color: 5

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 6221 Color: 350
Size: 1466 Color: 209

Bin 96: 10 of cap free
Amount of items: 5
Items: 
Size: 3892 Color: 280
Size: 3188 Color: 264
Size: 244 Color: 62
Size: 184 Color: 40
Size: 178 Color: 37

Bin 97: 10 of cap free
Amount of items: 3
Items: 
Size: 5571 Color: 319
Size: 2055 Color: 235
Size: 60 Color: 6

Bin 98: 10 of cap free
Amount of items: 3
Items: 
Size: 5623 Color: 321
Size: 2031 Color: 233
Size: 32 Color: 4

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 377
Size: 1044 Color: 177

Bin 100: 12 of cap free
Amount of items: 3
Items: 
Size: 4780 Color: 293
Size: 2748 Color: 253
Size: 156 Color: 27

Bin 101: 12 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 302
Size: 2408 Color: 243
Size: 136 Color: 17

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 5291 Color: 312
Size: 2391 Color: 242

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 6345 Color: 356
Size: 1337 Color: 203

Bin 104: 15 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 353
Size: 1385 Color: 205

Bin 105: 17 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 292
Size: 2771 Color: 255
Size: 160 Color: 28

Bin 106: 20 of cap free
Amount of items: 2
Items: 
Size: 4572 Color: 290
Size: 3104 Color: 261

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 5176 Color: 304
Size: 1850 Color: 227
Size: 650 Color: 137

Bin 108: 21 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 305
Size: 2369 Color: 241
Size: 128 Color: 15

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 4807 Color: 297
Size: 2865 Color: 259

Bin 110: 25 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 315
Size: 2075 Color: 236
Size: 128 Color: 11

Bin 111: 34 of cap free
Amount of items: 2
Items: 
Size: 5252 Color: 308
Size: 2410 Color: 244

Bin 112: 35 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 318
Size: 2019 Color: 232
Size: 86 Color: 7

Bin 113: 43 of cap free
Amount of items: 11
Items: 
Size: 3849 Color: 272
Size: 520 Color: 118
Size: 488 Color: 117
Size: 480 Color: 114
Size: 480 Color: 113
Size: 480 Color: 112
Size: 424 Color: 109
Size: 240 Color: 60
Size: 236 Color: 59
Size: 232 Color: 57
Size: 224 Color: 56

Bin 114: 48 of cap free
Amount of items: 7
Items: 
Size: 3854 Color: 276
Size: 1164 Color: 186
Size: 854 Color: 159
Size: 808 Color: 152
Size: 552 Color: 124
Size: 208 Color: 49
Size: 208 Color: 48

Bin 115: 52 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 287
Size: 3208 Color: 271

Bin 116: 69 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 301
Size: 2636 Color: 252
Size: 136 Color: 18

Bin 117: 71 of cap free
Amount of items: 8
Items: 
Size: 3851 Color: 274
Size: 700 Color: 143
Size: 684 Color: 141
Size: 674 Color: 139
Size: 644 Color: 136
Size: 640 Color: 131
Size: 216 Color: 53
Size: 216 Color: 52

Bin 118: 101 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 300
Size: 2624 Color: 251
Size: 144 Color: 22

Bin 119: 108 of cap free
Amount of items: 3
Items: 
Size: 5328 Color: 313
Size: 2132 Color: 240
Size: 128 Color: 14

Bin 120: 113 of cap free
Amount of items: 2
Items: 
Size: 4376 Color: 286
Size: 3207 Color: 270

Bin 121: 114 of cap free
Amount of items: 2
Items: 
Size: 4806 Color: 296
Size: 2776 Color: 258

Bin 122: 116 of cap free
Amount of items: 2
Items: 
Size: 4374 Color: 285
Size: 3206 Color: 269

Bin 123: 116 of cap free
Amount of items: 3
Items: 
Size: 4816 Color: 299
Size: 2620 Color: 250
Size: 144 Color: 23

Bin 124: 118 of cap free
Amount of items: 2
Items: 
Size: 4373 Color: 284
Size: 3205 Color: 268

Bin 125: 119 of cap free
Amount of items: 2
Items: 
Size: 4803 Color: 295
Size: 2774 Color: 257

Bin 126: 120 of cap free
Amount of items: 22
Items: 
Size: 472 Color: 110
Size: 420 Color: 108
Size: 416 Color: 107
Size: 416 Color: 106
Size: 414 Color: 105
Size: 402 Color: 102
Size: 402 Color: 101
Size: 400 Color: 100
Size: 400 Color: 99
Size: 368 Color: 97
Size: 368 Color: 96
Size: 368 Color: 95
Size: 292 Color: 81
Size: 292 Color: 80
Size: 288 Color: 78
Size: 288 Color: 77
Size: 288 Color: 76
Size: 272 Color: 74
Size: 258 Color: 68
Size: 256 Color: 67
Size: 256 Color: 66
Size: 240 Color: 61

Bin 127: 120 of cap free
Amount of items: 2
Items: 
Size: 4556 Color: 289
Size: 3020 Color: 260

Bin 128: 121 of cap free
Amount of items: 2
Items: 
Size: 4802 Color: 294
Size: 2773 Color: 256

Bin 129: 122 of cap free
Amount of items: 2
Items: 
Size: 4370 Color: 283
Size: 3204 Color: 266

Bin 130: 123 of cap free
Amount of items: 2
Items: 
Size: 4369 Color: 282
Size: 3204 Color: 267

Bin 131: 130 of cap free
Amount of items: 3
Items: 
Size: 4808 Color: 298
Size: 2604 Color: 249
Size: 154 Color: 26

Bin 132: 146 of cap free
Amount of items: 5
Items: 
Size: 3860 Color: 277
Size: 1288 Color: 195
Size: 1281 Color: 194
Size: 921 Color: 163
Size: 200 Color: 46

Bin 133: 5092 of cap free
Amount of items: 8
Items: 
Size: 354 Color: 93
Size: 352 Color: 92
Size: 350 Color: 91
Size: 328 Color: 89
Size: 312 Color: 85
Size: 308 Color: 84
Size: 304 Color: 83
Size: 296 Color: 82

Total size: 1015872
Total free space: 7696

