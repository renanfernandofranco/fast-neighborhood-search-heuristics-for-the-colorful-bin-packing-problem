Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1540 Color: 152
Size: 892 Color: 127
Size: 40 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 162
Size: 662 Color: 113
Size: 100 Color: 42

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 163
Size: 462 Color: 96
Size: 276 Color: 76

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 168
Size: 546 Color: 105
Size: 108 Color: 44

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 169
Size: 382 Color: 87
Size: 262 Color: 73

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1871 Color: 172
Size: 501 Color: 100
Size: 100 Color: 40

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1877 Color: 173
Size: 491 Color: 98
Size: 104 Color: 43

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 174
Size: 538 Color: 103
Size: 52 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1996 Color: 183
Size: 272 Color: 74
Size: 204 Color: 66

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 184
Size: 385 Color: 89
Size: 76 Color: 28

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 185
Size: 381 Color: 86
Size: 74 Color: 26

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 186
Size: 302 Color: 78
Size: 152 Color: 57

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2076 Color: 191
Size: 300 Color: 77
Size: 96 Color: 38

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2077 Color: 192
Size: 331 Color: 80
Size: 64 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 194
Size: 272 Color: 75
Size: 84 Color: 31

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 197
Size: 200 Color: 64
Size: 114 Color: 47

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 198
Size: 204 Color: 67
Size: 86 Color: 33

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 199
Size: 204 Color: 68
Size: 64 Color: 20

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 200
Size: 176 Color: 62
Size: 84 Color: 32

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2218 Color: 201
Size: 156 Color: 58
Size: 98 Color: 39

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 154
Size: 881 Color: 126
Size: 36 Color: 7

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 164
Size: 667 Color: 114
Size: 56 Color: 15

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1948 Color: 178
Size: 427 Color: 91
Size: 96 Color: 37

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 181
Size: 497 Color: 99

Bin 25: 2 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 139
Size: 540 Color: 104
Size: 384 Color: 88
Size: 228 Color: 70
Size: 80 Color: 29

Bin 26: 2 of cap free
Amount of items: 5
Items: 
Size: 1254 Color: 144
Size: 1028 Color: 134
Size: 68 Color: 21
Size: 64 Color: 18
Size: 56 Color: 16

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 145
Size: 1018 Color: 131
Size: 48 Color: 13

Bin 28: 2 of cap free
Amount of items: 5
Items: 
Size: 1651 Color: 156
Size: 681 Color: 116
Size: 74 Color: 27
Size: 32 Color: 6
Size: 32 Color: 5

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 161
Size: 766 Color: 119

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1785 Color: 166
Size: 685 Color: 118

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 171
Size: 604 Color: 111
Size: 8 Color: 2

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2036 Color: 189
Size: 434 Color: 93

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2138 Color: 195
Size: 332 Color: 81

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 2148 Color: 196
Size: 322 Color: 79

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 165
Size: 684 Color: 117
Size: 8 Color: 1

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 2025 Color: 187
Size: 444 Color: 94

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 2102 Color: 193
Size: 367 Color: 84

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1241 Color: 141
Size: 1227 Color: 137

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 160
Size: 787 Color: 122
Size: 8 Color: 0

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1850 Color: 170
Size: 618 Color: 112

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1892 Color: 176
Size: 576 Color: 109

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 177
Size: 546 Color: 106

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1953 Color: 179
Size: 514 Color: 102

Bin 44: 6 of cap free
Amount of items: 7
Items: 
Size: 1237 Color: 138
Size: 373 Color: 85
Size: 342 Color: 82
Size: 242 Color: 72
Size: 100 Color: 41
Size: 88 Color: 34
Size: 84 Color: 30

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 167
Size: 673 Color: 115

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1885 Color: 175
Size: 581 Color: 110

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 1982 Color: 182
Size: 484 Color: 97

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 2033 Color: 188
Size: 433 Color: 92

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 190
Size: 404 Color: 90

Bin 50: 7 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 180
Size: 504 Color: 101

Bin 51: 8 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 155
Size: 462 Color: 95
Size: 364 Color: 83

Bin 52: 11 of cap free
Amount of items: 4
Items: 
Size: 1529 Color: 151
Size: 840 Color: 124
Size: 48 Color: 11
Size: 44 Color: 10

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 159
Size: 780 Color: 121
Size: 16 Color: 3

Bin 54: 14 of cap free
Amount of items: 2
Items: 
Size: 1458 Color: 148
Size: 1000 Color: 130

Bin 55: 21 of cap free
Amount of items: 4
Items: 
Size: 1239 Color: 140
Size: 573 Color: 108
Size: 567 Color: 107
Size: 72 Color: 25

Bin 56: 21 of cap free
Amount of items: 2
Items: 
Size: 1521 Color: 150
Size: 930 Color: 129

Bin 57: 21 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 158
Size: 770 Color: 120
Size: 24 Color: 4

Bin 58: 25 of cap free
Amount of items: 2
Items: 
Size: 1417 Color: 147
Size: 1030 Color: 136

Bin 59: 27 of cap free
Amount of items: 2
Items: 
Size: 1652 Color: 157
Size: 793 Color: 123

Bin 60: 28 of cap free
Amount of items: 2
Items: 
Size: 1415 Color: 146
Size: 1029 Color: 135

Bin 61: 35 of cap free
Amount of items: 3
Items: 
Size: 1466 Color: 149
Size: 923 Color: 128
Size: 48 Color: 12

Bin 62: 38 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 153
Size: 842 Color: 125
Size: 40 Color: 8

Bin 63: 50 of cap free
Amount of items: 16
Items: 
Size: 236 Color: 71
Size: 214 Color: 69
Size: 204 Color: 65
Size: 200 Color: 63
Size: 176 Color: 61
Size: 164 Color: 60
Size: 158 Color: 59
Size: 152 Color: 56
Size: 136 Color: 55
Size: 132 Color: 50
Size: 120 Color: 49
Size: 120 Color: 48
Size: 114 Color: 46
Size: 112 Color: 45
Size: 96 Color: 36
Size: 88 Color: 35

Bin 64: 62 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 142
Size: 1022 Color: 132
Size: 72 Color: 24
Size: 72 Color: 23

Bin 65: 63 of cap free
Amount of items: 4
Items: 
Size: 1246 Color: 143
Size: 1027 Color: 133
Size: 72 Color: 22
Size: 64 Color: 19

Bin 66: 1936 of cap free
Amount of items: 4
Items: 
Size: 136 Color: 54
Size: 134 Color: 53
Size: 134 Color: 52
Size: 132 Color: 51

Total size: 160680
Total free space: 2472

