Capicity Bin: 2052
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 13
Size: 593 Color: 18
Size: 44 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 3
Size: 583 Color: 14
Size: 46 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 8
Size: 514 Color: 19
Size: 100 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 17
Size: 425 Color: 14
Size: 84 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 6
Size: 385 Color: 14
Size: 42 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 18
Size: 262 Color: 11
Size: 112 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 4
Size: 316 Color: 8
Size: 50 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 12
Size: 231 Color: 15
Size: 84 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 9
Size: 225 Color: 9
Size: 60 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 13
Size: 148 Color: 0
Size: 132 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 18
Size: 229 Color: 12
Size: 32 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 3
Size: 178 Color: 8
Size: 80 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 8
Size: 116 Color: 17
Size: 94 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 19
Size: 128 Color: 19
Size: 78 Color: 17

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 13
Size: 854 Color: 1
Size: 86 Color: 17

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1138 Color: 4
Size: 855 Color: 6
Size: 58 Color: 8

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1267 Color: 18
Size: 692 Color: 17
Size: 92 Color: 7

Bin 18: 1 of cap free
Amount of items: 4
Items: 
Size: 1359 Color: 16
Size: 478 Color: 14
Size: 168 Color: 2
Size: 46 Color: 9

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 1485 Color: 0
Size: 402 Color: 10
Size: 148 Color: 8
Size: 16 Color: 19

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 13
Size: 331 Color: 8
Size: 174 Color: 6

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 14
Size: 481 Color: 5

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 12
Size: 391 Color: 8
Size: 62 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 357 Color: 8
Size: 56 Color: 3

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 15
Size: 282 Color: 9
Size: 68 Color: 18

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 14
Size: 342 Color: 5

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 13
Size: 257 Color: 5
Size: 76 Color: 8

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 10
Size: 263 Color: 3
Size: 46 Color: 8

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 13
Size: 152 Color: 11
Size: 70 Color: 18

Bin 29: 2 of cap free
Amount of items: 4
Items: 
Size: 1031 Color: 10
Size: 751 Color: 2
Size: 228 Color: 6
Size: 40 Color: 10

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1153 Color: 19
Size: 845 Color: 6
Size: 52 Color: 17

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1353 Color: 8
Size: 649 Color: 16
Size: 48 Color: 3

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 11
Size: 525 Color: 13
Size: 32 Color: 18

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1583 Color: 3
Size: 467 Color: 0

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 16
Size: 346 Color: 13

Bin 35: 3 of cap free
Amount of items: 4
Items: 
Size: 1257 Color: 13
Size: 473 Color: 12
Size: 251 Color: 8
Size: 68 Color: 14

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 7
Size: 655 Color: 8
Size: 24 Color: 13

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1478 Color: 18
Size: 571 Color: 5

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 4
Size: 382 Color: 18
Size: 76 Color: 7

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 17
Size: 431 Color: 5

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 10
Size: 287 Color: 13
Size: 36 Color: 9

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1775 Color: 10
Size: 274 Color: 15

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 6
Size: 266 Color: 13

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 1
Size: 314 Color: 19

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 13
Size: 828 Color: 10
Size: 56 Color: 2

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1306 Color: 3
Size: 741 Color: 1

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1477 Color: 10
Size: 570 Color: 14

Bin 47: 5 of cap free
Amount of items: 4
Items: 
Size: 1799 Color: 13
Size: 240 Color: 11
Size: 4 Color: 12
Size: 4 Color: 7

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1288 Color: 2
Size: 686 Color: 7
Size: 72 Color: 6

Bin 49: 6 of cap free
Amount of items: 4
Items: 
Size: 1375 Color: 12
Size: 579 Color: 4
Size: 68 Color: 2
Size: 24 Color: 19

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 5
Size: 389 Color: 9

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 1753 Color: 11
Size: 293 Color: 7

Bin 52: 7 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 16
Size: 472 Color: 7
Size: 271 Color: 10
Size: 72 Color: 14

Bin 53: 7 of cap free
Amount of items: 2
Items: 
Size: 1617 Color: 6
Size: 428 Color: 19

Bin 54: 9 of cap free
Amount of items: 6
Items: 
Size: 1027 Color: 11
Size: 422 Color: 13
Size: 228 Color: 3
Size: 218 Color: 1
Size: 96 Color: 9
Size: 52 Color: 2

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1420 Color: 18
Size: 622 Color: 14

Bin 56: 10 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 10
Size: 313 Color: 7

Bin 57: 11 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 19
Size: 897 Color: 18
Size: 114 Color: 8

Bin 58: 13 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 4
Size: 362 Color: 11

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 1275 Color: 3
Size: 762 Color: 16

Bin 60: 15 of cap free
Amount of items: 2
Items: 
Size: 1506 Color: 17
Size: 531 Color: 1

Bin 61: 15 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 19
Size: 502 Color: 18

Bin 62: 15 of cap free
Amount of items: 2
Items: 
Size: 1745 Color: 6
Size: 292 Color: 19

Bin 63: 15 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 10
Size: 239 Color: 14

Bin 64: 22 of cap free
Amount of items: 17
Items: 
Size: 219 Color: 8
Size: 214 Color: 6
Size: 187 Color: 8
Size: 170 Color: 8
Size: 136 Color: 2
Size: 130 Color: 2
Size: 124 Color: 7
Size: 124 Color: 1
Size: 114 Color: 6
Size: 106 Color: 19
Size: 104 Color: 11
Size: 94 Color: 12
Size: 84 Color: 1
Size: 72 Color: 12
Size: 52 Color: 13
Size: 52 Color: 9
Size: 48 Color: 9

Bin 65: 22 of cap free
Amount of items: 2
Items: 
Size: 1367 Color: 16
Size: 663 Color: 7

Bin 66: 1768 of cap free
Amount of items: 6
Items: 
Size: 64 Color: 0
Size: 52 Color: 19
Size: 48 Color: 12
Size: 40 Color: 17
Size: 40 Color: 9
Size: 40 Color: 3

Total size: 133380
Total free space: 2052

