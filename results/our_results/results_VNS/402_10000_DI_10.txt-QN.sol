Capicity Bin: 7568
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 324
Size: 1478 Color: 217
Size: 292 Color: 89

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5990 Color: 330
Size: 1318 Color: 211
Size: 260 Color: 83

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 331
Size: 1032 Color: 187
Size: 540 Color: 125

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 332
Size: 1492 Color: 219
Size: 72 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6040 Color: 334
Size: 1164 Color: 202
Size: 364 Color: 102

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 340
Size: 1156 Color: 199
Size: 240 Color: 75

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6174 Color: 341
Size: 764 Color: 156
Size: 630 Color: 136

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6179 Color: 342
Size: 1159 Color: 200
Size: 230 Color: 69

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6187 Color: 343
Size: 957 Color: 178
Size: 424 Color: 111

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 344
Size: 1316 Color: 210
Size: 64 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6191 Color: 345
Size: 1149 Color: 196
Size: 228 Color: 68

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6192 Color: 346
Size: 1152 Color: 198
Size: 224 Color: 67

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6324 Color: 350
Size: 1044 Color: 190
Size: 200 Color: 56

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6329 Color: 352
Size: 1033 Color: 188
Size: 206 Color: 62

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6338 Color: 353
Size: 830 Color: 165
Size: 400 Color: 106

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 354
Size: 1028 Color: 186
Size: 200 Color: 58

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 358
Size: 1022 Color: 183
Size: 120 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 359
Size: 951 Color: 176
Size: 190 Color: 54

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 361
Size: 954 Color: 177
Size: 156 Color: 37

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 362
Size: 724 Color: 151
Size: 384 Color: 105

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 363
Size: 1046 Color: 191
Size: 48 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 365
Size: 828 Color: 163
Size: 240 Color: 76

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6501 Color: 366
Size: 727 Color: 152
Size: 340 Color: 98

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6503 Color: 367
Size: 829 Color: 164
Size: 236 Color: 74

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 372
Size: 528 Color: 122
Size: 468 Color: 118

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6573 Color: 373
Size: 831 Color: 166
Size: 164 Color: 41

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 374
Size: 738 Color: 153
Size: 256 Color: 81

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 376
Size: 886 Color: 169
Size: 100 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 378
Size: 703 Color: 148
Size: 244 Color: 77

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 379
Size: 628 Color: 134
Size: 318 Color: 93

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6638 Color: 381
Size: 674 Color: 143
Size: 256 Color: 79

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 383
Size: 749 Color: 154
Size: 156 Color: 38

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 386
Size: 706 Color: 149
Size: 176 Color: 51

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 388
Size: 540 Color: 126
Size: 328 Color: 96

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 389
Size: 721 Color: 150
Size: 144 Color: 30

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 390
Size: 682 Color: 146
Size: 164 Color: 42

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6757 Color: 394
Size: 663 Color: 140
Size: 148 Color: 31

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 395
Size: 673 Color: 142
Size: 134 Color: 23

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 396
Size: 654 Color: 139
Size: 152 Color: 35

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 397
Size: 628 Color: 135
Size: 176 Color: 50

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 398
Size: 544 Color: 127
Size: 254 Color: 78

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 400
Size: 634 Color: 137
Size: 148 Color: 32

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6810 Color: 402
Size: 608 Color: 131
Size: 150 Color: 33

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5083 Color: 297
Size: 2324 Color: 248
Size: 160 Color: 39

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 327
Size: 1023 Color: 184
Size: 677 Color: 145

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5875 Color: 328
Size: 1144 Color: 195
Size: 548 Color: 129

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6022 Color: 333
Size: 1409 Color: 212
Size: 136 Color: 24

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 349
Size: 666 Color: 141
Size: 600 Color: 130

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6325 Color: 351
Size: 1026 Color: 185
Size: 216 Color: 65

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6561 Color: 370
Size: 1006 Color: 182

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 371
Size: 1004 Color: 181

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 380
Size: 944 Color: 174

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 387
Size: 870 Color: 168

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6754 Color: 393
Size: 812 Color: 161

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6788 Color: 401
Size: 778 Color: 157

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 315
Size: 1809 Color: 233
Size: 128 Color: 15

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 317
Size: 1794 Color: 232
Size: 128 Color: 13

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 318
Size: 1442 Color: 215
Size: 464 Color: 117

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6743 Color: 392
Size: 822 Color: 162

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 309
Size: 2006 Color: 239
Size: 132 Color: 21

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6145 Color: 339
Size: 1419 Color: 214

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 348
Size: 1290 Color: 207

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6482 Color: 364
Size: 1082 Color: 193

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6773 Color: 399
Size: 791 Color: 160

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6506 Color: 368
Size: 1057 Color: 192

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 369
Size: 1037 Color: 189

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 375
Size: 988 Color: 180

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6671 Color: 385
Size: 892 Color: 172

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 6114 Color: 337
Size: 1448 Color: 216

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 6372 Color: 357
Size: 1190 Color: 204

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 6725 Color: 391
Size: 836 Color: 167

Bin 72: 8 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 298
Size: 1308 Color: 209
Size: 1162 Color: 201

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 325
Size: 1706 Color: 227
Size: 48 Color: 0

Bin 74: 9 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 300
Size: 2308 Color: 246
Size: 156 Color: 36

Bin 75: 9 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 308
Size: 2026 Color: 240
Size: 134 Color: 22

Bin 76: 9 of cap free
Amount of items: 2
Items: 
Size: 6049 Color: 336
Size: 1510 Color: 221

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 382
Size: 907 Color: 173

Bin 78: 9 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 384
Size: 891 Color: 171

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 5138 Color: 301
Size: 2268 Color: 245
Size: 152 Color: 34

Bin 80: 10 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 316
Size: 1793 Color: 231
Size: 128 Color: 14

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 377
Size: 962 Color: 179

Bin 82: 11 of cap free
Amount of items: 4
Items: 
Size: 5758 Color: 321
Size: 1591 Color: 223
Size: 112 Color: 9
Size: 96 Color: 7

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 6431 Color: 360
Size: 1126 Color: 194

Bin 84: 13 of cap free
Amount of items: 4
Items: 
Size: 5726 Color: 320
Size: 1585 Color: 222
Size: 124 Color: 12
Size: 120 Color: 10

Bin 85: 13 of cap free
Amount of items: 2
Items: 
Size: 6341 Color: 355
Size: 1214 Color: 205

Bin 86: 14 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 310
Size: 1964 Color: 238
Size: 132 Color: 20

Bin 87: 15 of cap free
Amount of items: 2
Items: 
Size: 6142 Color: 338
Size: 1411 Color: 213

Bin 88: 17 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 305
Size: 2071 Color: 243
Size: 140 Color: 26

Bin 89: 19 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 356
Size: 1187 Color: 203

Bin 90: 20 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 288
Size: 2660 Color: 255
Size: 192 Color: 55

Bin 91: 22 of cap free
Amount of items: 2
Items: 
Size: 5838 Color: 326
Size: 1708 Color: 228

Bin 92: 23 of cap free
Amount of items: 2
Items: 
Size: 4729 Color: 291
Size: 2816 Color: 263

Bin 93: 23 of cap free
Amount of items: 4
Items: 
Size: 4852 Color: 296
Size: 2369 Color: 252
Size: 164 Color: 43
Size: 160 Color: 40

Bin 94: 25 of cap free
Amount of items: 2
Items: 
Size: 6041 Color: 335
Size: 1502 Color: 220

Bin 95: 25 of cap free
Amount of items: 2
Items: 
Size: 6270 Color: 347
Size: 1273 Color: 206

Bin 96: 31 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 314
Size: 1823 Color: 235
Size: 128 Color: 16

Bin 97: 32 of cap free
Amount of items: 2
Items: 
Size: 5162 Color: 302
Size: 2374 Color: 253

Bin 98: 35 of cap free
Amount of items: 2
Items: 
Size: 5879 Color: 329
Size: 1654 Color: 226

Bin 99: 36 of cap free
Amount of items: 11
Items: 
Size: 1151 Color: 197
Size: 949 Color: 175
Size: 889 Color: 170
Size: 790 Color: 159
Size: 789 Color: 158
Size: 755 Color: 155
Size: 689 Color: 147
Size: 544 Color: 128
Size: 336 Color: 97
Size: 320 Color: 95
Size: 320 Color: 94

Bin 100: 39 of cap free
Amount of items: 4
Items: 
Size: 5180 Color: 303
Size: 2061 Color: 241
Size: 144 Color: 29
Size: 144 Color: 28

Bin 101: 39 of cap free
Amount of items: 2
Items: 
Size: 5381 Color: 306
Size: 2148 Color: 244

Bin 102: 41 of cap free
Amount of items: 2
Items: 
Size: 5667 Color: 319
Size: 1860 Color: 236

Bin 103: 44 of cap free
Amount of items: 4
Items: 
Size: 3789 Color: 276
Size: 3151 Color: 265
Size: 296 Color: 90
Size: 288 Color: 88

Bin 104: 46 of cap free
Amount of items: 2
Items: 
Size: 3786 Color: 274
Size: 3736 Color: 272

Bin 105: 51 of cap free
Amount of items: 4
Items: 
Size: 3794 Color: 277
Size: 3153 Color: 266
Size: 288 Color: 87
Size: 282 Color: 86

Bin 106: 55 of cap free
Amount of items: 3
Items: 
Size: 4722 Color: 289
Size: 1484 Color: 218
Size: 1307 Color: 208

Bin 107: 56 of cap free
Amount of items: 4
Items: 
Size: 3796 Color: 278
Size: 3154 Color: 267
Size: 282 Color: 85
Size: 280 Color: 84

Bin 108: 56 of cap free
Amount of items: 4
Items: 
Size: 5796 Color: 323
Size: 1620 Color: 225
Size: 48 Color: 3
Size: 48 Color: 1

Bin 109: 59 of cap free
Amount of items: 4
Items: 
Size: 4380 Color: 287
Size: 2725 Color: 261
Size: 204 Color: 59
Size: 200 Color: 57

Bin 110: 65 of cap free
Amount of items: 4
Items: 
Size: 4804 Color: 295
Size: 2367 Color: 251
Size: 168 Color: 45
Size: 164 Color: 44

Bin 111: 69 of cap free
Amount of items: 2
Items: 
Size: 5091 Color: 299
Size: 2408 Color: 254

Bin 112: 75 of cap free
Amount of items: 4
Items: 
Size: 4301 Color: 282
Size: 2700 Color: 256
Size: 256 Color: 80
Size: 236 Color: 73

Bin 113: 81 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 322
Size: 1611 Color: 224
Size: 96 Color: 6

Bin 114: 82 of cap free
Amount of items: 4
Items: 
Size: 4322 Color: 284
Size: 2710 Color: 258
Size: 230 Color: 70
Size: 224 Color: 66

Bin 115: 83 of cap free
Amount of items: 4
Items: 
Size: 4788 Color: 294
Size: 2357 Color: 250
Size: 172 Color: 47
Size: 168 Color: 46

Bin 116: 83 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 313
Size: 1815 Color: 234
Size: 132 Color: 17

Bin 117: 92 of cap free
Amount of items: 4
Items: 
Size: 4306 Color: 283
Size: 2706 Color: 257
Size: 232 Color: 72
Size: 232 Color: 71

Bin 118: 92 of cap free
Amount of items: 4
Items: 
Size: 4782 Color: 293
Size: 2342 Color: 249
Size: 176 Color: 49
Size: 176 Color: 48

Bin 119: 94 of cap free
Amount of items: 4
Items: 
Size: 4330 Color: 285
Size: 2722 Color: 259
Size: 212 Color: 64
Size: 210 Color: 63

Bin 120: 95 of cap free
Amount of items: 2
Items: 
Size: 4727 Color: 290
Size: 2746 Color: 262

Bin 121: 95 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 307
Size: 1942 Color: 237
Size: 140 Color: 25

Bin 122: 96 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 279
Size: 3304 Color: 270
Size: 260 Color: 82

Bin 123: 103 of cap free
Amount of items: 4
Items: 
Size: 4332 Color: 286
Size: 2723 Color: 260
Size: 206 Color: 61
Size: 204 Color: 60

Bin 124: 107 of cap free
Amount of items: 2
Items: 
Size: 3785 Color: 273
Size: 3676 Color: 271

Bin 125: 111 of cap free
Amount of items: 2
Items: 
Size: 4299 Color: 281
Size: 3158 Color: 269

Bin 126: 112 of cap free
Amount of items: 4
Items: 
Size: 3788 Color: 275
Size: 3052 Color: 264
Size: 316 Color: 92
Size: 300 Color: 91

Bin 127: 112 of cap free
Amount of items: 4
Items: 
Size: 4758 Color: 292
Size: 2322 Color: 247
Size: 188 Color: 53
Size: 188 Color: 52

Bin 128: 126 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 312
Size: 1786 Color: 230
Size: 132 Color: 18

Bin 129: 128 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 311
Size: 1786 Color: 229
Size: 132 Color: 19

Bin 130: 138 of cap free
Amount of items: 2
Items: 
Size: 4274 Color: 280
Size: 3156 Color: 268

Bin 131: 138 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 304
Size: 2066 Color: 242
Size: 144 Color: 27

Bin 132: 302 of cap free
Amount of items: 15
Items: 
Size: 676 Color: 144
Size: 652 Color: 138
Size: 624 Color: 133
Size: 616 Color: 132
Size: 536 Color: 124
Size: 528 Color: 123
Size: 472 Color: 121
Size: 472 Color: 120
Size: 472 Color: 119
Size: 404 Color: 107
Size: 368 Color: 104
Size: 368 Color: 103
Size: 362 Color: 101
Size: 360 Color: 100
Size: 356 Color: 99

Bin 133: 4042 of cap free
Amount of items: 8
Items: 
Size: 464 Color: 116
Size: 464 Color: 115
Size: 456 Color: 114
Size: 456 Color: 113
Size: 448 Color: 112
Size: 414 Color: 110
Size: 412 Color: 109
Size: 412 Color: 108

Total size: 998976
Total free space: 7568

