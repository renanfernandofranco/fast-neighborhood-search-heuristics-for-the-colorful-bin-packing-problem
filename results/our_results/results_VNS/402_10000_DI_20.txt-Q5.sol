Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3921 Color: 0
Size: 3253 Color: 1
Size: 650 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4494 Color: 2
Size: 3124 Color: 1
Size: 206 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 3
Size: 2904 Color: 0
Size: 364 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 2
Size: 2778 Color: 1
Size: 258 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 1
Size: 2487 Color: 0
Size: 166 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5195 Color: 0
Size: 2191 Color: 1
Size: 438 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5219 Color: 2
Size: 2457 Color: 0
Size: 148 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 1
Size: 2246 Color: 0
Size: 144 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 2
Size: 1644 Color: 4
Size: 448 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5823 Color: 0
Size: 1669 Color: 0
Size: 332 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5954 Color: 4
Size: 1562 Color: 0
Size: 308 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 1
Size: 1574 Color: 0
Size: 216 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6111 Color: 0
Size: 1429 Color: 4
Size: 284 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 4
Size: 1478 Color: 0
Size: 100 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6273 Color: 2
Size: 943 Color: 0
Size: 608 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6293 Color: 3
Size: 1277 Color: 2
Size: 254 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 4
Size: 1270 Color: 1
Size: 252 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 1
Size: 1268 Color: 3
Size: 208 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 0
Size: 1196 Color: 2
Size: 232 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 4
Size: 1284 Color: 3
Size: 104 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6455 Color: 2
Size: 1141 Color: 0
Size: 228 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 2
Size: 1086 Color: 3
Size: 216 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6529 Color: 2
Size: 957 Color: 1
Size: 338 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 4
Size: 820 Color: 2
Size: 456 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 0
Size: 742 Color: 2
Size: 440 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 4
Size: 1056 Color: 2
Size: 108 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6677 Color: 3
Size: 945 Color: 4
Size: 202 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 2
Size: 724 Color: 4
Size: 328 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 2
Size: 857 Color: 2
Size: 170 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 1
Size: 648 Color: 4
Size: 396 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 4
Size: 935 Color: 2
Size: 64 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 2
Size: 676 Color: 3
Size: 294 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6874 Color: 2
Size: 702 Color: 3
Size: 248 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6914 Color: 3
Size: 476 Color: 3
Size: 434 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 2
Size: 682 Color: 1
Size: 146 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 2
Size: 616 Color: 4
Size: 188 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7024 Color: 2
Size: 552 Color: 1
Size: 248 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 3
Size: 484 Color: 1
Size: 354 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7026 Color: 1
Size: 446 Color: 2
Size: 352 Color: 4

Bin 40: 1 of cap free
Amount of items: 5
Items: 
Size: 3913 Color: 0
Size: 1834 Color: 1
Size: 972 Color: 4
Size: 724 Color: 2
Size: 380 Color: 4

Bin 41: 1 of cap free
Amount of items: 5
Items: 
Size: 3914 Color: 2
Size: 2079 Color: 2
Size: 1122 Color: 4
Size: 556 Color: 1
Size: 152 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 4
Size: 3257 Color: 0
Size: 148 Color: 2

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4841 Color: 4
Size: 2782 Color: 2
Size: 200 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 4
Size: 2433 Color: 0
Size: 488 Color: 3

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 0
Size: 2363 Color: 3
Size: 128 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 0
Size: 2099 Color: 3
Size: 176 Color: 2

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 5523 Color: 3
Size: 2300 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5619 Color: 2
Size: 2084 Color: 2
Size: 120 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5969 Color: 4
Size: 1546 Color: 0
Size: 308 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 0
Size: 880 Color: 2
Size: 342 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 0
Size: 1138 Color: 0
Size: 68 Color: 2

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 1
Size: 869 Color: 4
Size: 296 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6693 Color: 0
Size: 934 Color: 1
Size: 196 Color: 4

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6727 Color: 4
Size: 960 Color: 1
Size: 136 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6741 Color: 4
Size: 648 Color: 0
Size: 434 Color: 3

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6829 Color: 2
Size: 666 Color: 4
Size: 328 Color: 4

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 0
Size: 3440 Color: 1
Size: 214 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 3
Size: 2795 Color: 2
Size: 172 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 2
Size: 2231 Color: 4
Size: 376 Color: 0

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5307 Color: 4
Size: 2171 Color: 1
Size: 344 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 0
Size: 2211 Color: 4
Size: 260 Color: 2

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 0
Size: 1906 Color: 3
Size: 216 Color: 2

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 4
Size: 1900 Color: 0
Size: 224 Color: 2

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 0
Size: 1436 Color: 3
Size: 190 Color: 2

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 6186 Color: 1
Size: 986 Color: 4
Size: 650 Color: 3

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 6557 Color: 1
Size: 1081 Color: 0
Size: 184 Color: 2

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 3
Size: 1068 Color: 1
Size: 48 Color: 4

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 0
Size: 2781 Color: 1
Size: 556 Color: 3

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 2
Size: 2075 Color: 3
Size: 572 Color: 1

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 5699 Color: 0
Size: 1994 Color: 3
Size: 128 Color: 4

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 0
Size: 1021 Color: 2
Size: 552 Color: 1

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 3
Size: 2194 Color: 1
Size: 558 Color: 4

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 2
Size: 2433 Color: 2
Size: 240 Color: 4

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 6783 Color: 1
Size: 1037 Color: 3

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 0
Size: 890 Color: 4

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 4385 Color: 0
Size: 3258 Color: 1
Size: 176 Color: 0

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 1471 Color: 0
Size: 496 Color: 4

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6309 Color: 2
Size: 1510 Color: 1

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 4470 Color: 0
Size: 3116 Color: 2
Size: 232 Color: 1

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6366 Color: 1
Size: 1452 Color: 4

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 3
Size: 974 Color: 1

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 1
Size: 2210 Color: 0
Size: 308 Color: 2

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 4
Size: 1715 Color: 0
Size: 564 Color: 3

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 6421 Color: 2
Size: 746 Color: 0
Size: 650 Color: 3

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 3917 Color: 0
Size: 3251 Color: 3
Size: 648 Color: 1

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 4
Size: 2513 Color: 0
Size: 180 Color: 4

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 5291 Color: 0
Size: 2159 Color: 2
Size: 366 Color: 4

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6292 Color: 1
Size: 1524 Color: 2

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 4
Size: 878 Color: 3

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 3
Size: 764 Color: 1
Size: 88 Color: 1

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 4
Size: 810 Color: 3

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 1
Size: 1057 Color: 4

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 4
Size: 2838 Color: 2
Size: 504 Color: 0

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 4879 Color: 0
Size: 2798 Color: 1
Size: 136 Color: 4

Bin 95: 11 of cap free
Amount of items: 3
Items: 
Size: 4966 Color: 4
Size: 2175 Color: 0
Size: 672 Color: 2

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 3
Size: 1839 Color: 4
Size: 762 Color: 0

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 1
Size: 1065 Color: 2

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 0
Size: 1208 Color: 3

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 1
Size: 903 Color: 0

Bin 100: 15 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 1
Size: 3260 Color: 4
Size: 624 Color: 2

Bin 101: 15 of cap free
Amount of items: 2
Items: 
Size: 5482 Color: 1
Size: 2327 Color: 4

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 6059 Color: 3
Size: 1748 Color: 2

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 4
Size: 2475 Color: 2

Bin 104: 18 of cap free
Amount of items: 2
Items: 
Size: 5626 Color: 1
Size: 2180 Color: 3

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 4950 Color: 4
Size: 2854 Color: 1

Bin 106: 20 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 3
Size: 1020 Color: 4

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 6255 Color: 1
Size: 1547 Color: 4

Bin 108: 22 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 3
Size: 1318 Color: 1

Bin 109: 23 of cap free
Amount of items: 2
Items: 
Size: 6794 Color: 2
Size: 1007 Color: 1

Bin 110: 24 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 1
Size: 1171 Color: 3
Size: 60 Color: 1

Bin 111: 25 of cap free
Amount of items: 2
Items: 
Size: 6581 Color: 2
Size: 1218 Color: 1

Bin 112: 26 of cap free
Amount of items: 3
Items: 
Size: 6206 Color: 2
Size: 1480 Color: 4
Size: 112 Color: 0

Bin 113: 32 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 1
Size: 2867 Color: 2
Size: 833 Color: 3

Bin 114: 36 of cap free
Amount of items: 2
Items: 
Size: 3916 Color: 4
Size: 3872 Color: 0

Bin 115: 49 of cap free
Amount of items: 2
Items: 
Size: 6482 Color: 2
Size: 1293 Color: 4

Bin 116: 50 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 2
Size: 3048 Color: 4
Size: 304 Color: 4

Bin 117: 53 of cap free
Amount of items: 2
Items: 
Size: 6462 Color: 1
Size: 1309 Color: 4

Bin 118: 54 of cap free
Amount of items: 2
Items: 
Size: 5996 Color: 3
Size: 1774 Color: 2

Bin 119: 56 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 1
Size: 2438 Color: 2

Bin 120: 57 of cap free
Amount of items: 2
Items: 
Size: 5235 Color: 4
Size: 2532 Color: 1

Bin 121: 58 of cap free
Amount of items: 3
Items: 
Size: 3918 Color: 2
Size: 2724 Color: 3
Size: 1124 Color: 4

Bin 122: 59 of cap free
Amount of items: 2
Items: 
Size: 5846 Color: 3
Size: 1919 Color: 2

Bin 123: 61 of cap free
Amount of items: 2
Items: 
Size: 5767 Color: 4
Size: 1996 Color: 1

Bin 124: 66 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 3
Size: 1650 Color: 1

Bin 125: 75 of cap free
Amount of items: 2
Items: 
Size: 4487 Color: 0
Size: 3262 Color: 4

Bin 126: 75 of cap free
Amount of items: 2
Items: 
Size: 5243 Color: 3
Size: 2506 Color: 2

Bin 127: 77 of cap free
Amount of items: 2
Items: 
Size: 4486 Color: 0
Size: 3261 Color: 1

Bin 128: 83 of cap free
Amount of items: 2
Items: 
Size: 5970 Color: 1
Size: 1771 Color: 2

Bin 129: 90 of cap free
Amount of items: 25
Items: 
Size: 494 Color: 3
Size: 442 Color: 1
Size: 432 Color: 2
Size: 432 Color: 1
Size: 430 Color: 3
Size: 418 Color: 0
Size: 414 Color: 3
Size: 414 Color: 1
Size: 408 Color: 0
Size: 382 Color: 2
Size: 376 Color: 4
Size: 352 Color: 1
Size: 280 Color: 4
Size: 260 Color: 0
Size: 252 Color: 0
Size: 240 Color: 4
Size: 220 Color: 1
Size: 210 Color: 4
Size: 200 Color: 4
Size: 192 Color: 4
Size: 192 Color: 0
Size: 190 Color: 1
Size: 176 Color: 2
Size: 168 Color: 2
Size: 160 Color: 4

Bin 130: 91 of cap free
Amount of items: 2
Items: 
Size: 5335 Color: 2
Size: 2398 Color: 3

Bin 131: 96 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 3
Size: 1494 Color: 1

Bin 132: 107 of cap free
Amount of items: 9
Items: 
Size: 1263 Color: 2
Size: 1236 Color: 2
Size: 900 Color: 4
Size: 900 Color: 3
Size: 876 Color: 4
Size: 794 Color: 3
Size: 648 Color: 3
Size: 556 Color: 0
Size: 544 Color: 1

Bin 133: 5968 of cap free
Amount of items: 13
Items: 
Size: 160 Color: 4
Size: 160 Color: 2
Size: 156 Color: 3
Size: 152 Color: 1
Size: 152 Color: 0
Size: 152 Color: 0
Size: 148 Color: 1
Size: 148 Color: 0
Size: 136 Color: 2
Size: 132 Color: 3
Size: 128 Color: 3
Size: 120 Color: 2
Size: 112 Color: 1

Total size: 1032768
Total free space: 7824

