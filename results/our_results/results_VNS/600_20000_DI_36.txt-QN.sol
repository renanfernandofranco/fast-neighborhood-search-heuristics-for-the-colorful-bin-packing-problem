Capicity Bin: 16304
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9130 Color: 421
Size: 6782 Color: 394
Size: 392 Color: 50

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10769 Color: 446
Size: 5259 Color: 371
Size: 276 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11785 Color: 466
Size: 3767 Color: 339
Size: 752 Color: 141

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12211 Color: 478
Size: 3411 Color: 325
Size: 682 Color: 131

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12243 Color: 481
Size: 2399 Color: 274
Size: 1662 Color: 225

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 483
Size: 3608 Color: 334
Size: 368 Color: 43

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12427 Color: 487
Size: 2957 Color: 307
Size: 920 Color: 165

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 489
Size: 3180 Color: 315
Size: 648 Color: 123

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12556 Color: 492
Size: 2724 Color: 292
Size: 1024 Color: 174

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12727 Color: 496
Size: 2953 Color: 306
Size: 624 Color: 116

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 505
Size: 3176 Color: 314
Size: 192 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12950 Color: 506
Size: 2798 Color: 299
Size: 556 Color: 101

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 507
Size: 2796 Color: 298
Size: 552 Color: 100

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 508
Size: 1718 Color: 229
Size: 1620 Color: 221

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 511
Size: 1862 Color: 237
Size: 1442 Color: 210

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 517
Size: 2362 Color: 269
Size: 800 Color: 148

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 519
Size: 2570 Color: 285
Size: 512 Color: 90

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 520
Size: 2408 Color: 275
Size: 656 Color: 124

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 522
Size: 2512 Color: 280
Size: 516 Color: 91

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 523
Size: 2521 Color: 281
Size: 504 Color: 88

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 529
Size: 2138 Color: 254
Size: 744 Color: 139

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 530
Size: 2173 Color: 256
Size: 704 Color: 134

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13432 Color: 531
Size: 2568 Color: 284
Size: 304 Color: 24

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13450 Color: 533
Size: 2264 Color: 262
Size: 590 Color: 110

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13467 Color: 535
Size: 2153 Color: 255
Size: 684 Color: 132

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13470 Color: 536
Size: 2328 Color: 268
Size: 506 Color: 89

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13583 Color: 541
Size: 2269 Color: 263
Size: 452 Color: 71

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 543
Size: 2190 Color: 257
Size: 504 Color: 87

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13672 Color: 546
Size: 2042 Color: 249
Size: 590 Color: 111

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 547
Size: 1848 Color: 235
Size: 780 Color: 147

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 548
Size: 2196 Color: 258
Size: 430 Color: 61

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 551
Size: 1858 Color: 236
Size: 704 Color: 135

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 554
Size: 1928 Color: 241
Size: 580 Color: 108

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 556
Size: 1528 Color: 217
Size: 922 Color: 166

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 561
Size: 1876 Color: 239
Size: 478 Color: 80

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 565
Size: 1296 Color: 194
Size: 948 Color: 169

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14068 Color: 566
Size: 1484 Color: 212
Size: 752 Color: 142

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14074 Color: 567
Size: 1606 Color: 220
Size: 624 Color: 117

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 572
Size: 1604 Color: 219
Size: 480 Color: 81

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14298 Color: 574
Size: 1674 Color: 227
Size: 332 Color: 36

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14314 Color: 577
Size: 1054 Color: 178
Size: 936 Color: 168

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 580
Size: 1640 Color: 222
Size: 320 Color: 27

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14378 Color: 582
Size: 1190 Color: 189
Size: 736 Color: 138

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 584
Size: 1506 Color: 214
Size: 368 Color: 46

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 586
Size: 1356 Color: 201
Size: 472 Color: 79

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 587
Size: 1344 Color: 195
Size: 472 Color: 77

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14490 Color: 588
Size: 1382 Color: 205
Size: 432 Color: 63

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 589
Size: 1422 Color: 208
Size: 384 Color: 48

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 592
Size: 928 Color: 167
Size: 824 Color: 153

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 593
Size: 1352 Color: 198
Size: 384 Color: 49

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 594
Size: 1050 Color: 177
Size: 680 Color: 130

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 595
Size: 1356 Color: 202
Size: 352 Color: 40

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14598 Color: 596
Size: 1066 Color: 181
Size: 640 Color: 121

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 597
Size: 1136 Color: 183
Size: 556 Color: 102

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14648 Color: 598
Size: 1184 Color: 188
Size: 472 Color: 78

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14650 Color: 599
Size: 1354 Color: 200
Size: 300 Color: 23

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14668 Color: 600
Size: 1352 Color: 196
Size: 284 Color: 17

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 477
Size: 3774 Color: 340
Size: 332 Color: 35

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12227 Color: 480
Size: 3384 Color: 322
Size: 692 Color: 133

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 484
Size: 3604 Color: 333
Size: 368 Color: 45

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12359 Color: 485
Size: 2760 Color: 294
Size: 1184 Color: 187

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 12582 Color: 494
Size: 3721 Color: 337

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 497
Size: 3118 Color: 311
Size: 428 Color: 60

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12781 Color: 499
Size: 2902 Color: 302
Size: 620 Color: 115

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 512
Size: 3166 Color: 313
Size: 108 Color: 2

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 514
Size: 3231 Color: 318

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13201 Color: 518
Size: 3102 Color: 310

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 521
Size: 1676 Color: 228
Size: 1364 Color: 204

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 528
Size: 2460 Color: 278
Size: 432 Color: 62

Bin 70: 2 of cap free
Amount of items: 5
Items: 
Size: 8181 Color: 414
Size: 6781 Color: 393
Size: 482 Color: 82
Size: 434 Color: 64
Size: 424 Color: 59

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 456
Size: 3592 Color: 331
Size: 1358 Color: 203

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 11618 Color: 461
Size: 4684 Color: 361

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11746 Color: 463
Size: 3596 Color: 332
Size: 960 Color: 170

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 479
Size: 4004 Color: 345
Size: 80 Color: 0

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12396 Color: 486
Size: 3906 Color: 344

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12982 Color: 510
Size: 3320 Color: 321

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 562
Size: 2314 Color: 266

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14434 Color: 585
Size: 1868 Color: 238

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 11698 Color: 462
Size: 4603 Color: 358

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 12459 Color: 488
Size: 3842 Color: 343

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 12566 Color: 493
Size: 3735 Color: 338

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 12902 Color: 504
Size: 3399 Color: 323

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13721 Color: 550
Size: 2580 Color: 286

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 557
Size: 2444 Color: 277

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13930 Color: 560
Size: 2371 Color: 271

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 564
Size: 2291 Color: 265

Bin 87: 4 of cap free
Amount of items: 5
Items: 
Size: 8172 Color: 413
Size: 6780 Color: 392
Size: 472 Color: 76
Size: 440 Color: 66
Size: 436 Color: 65

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13530 Color: 539
Size: 2770 Color: 295

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 575
Size: 2000 Color: 246

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14516 Color: 590
Size: 1784 Color: 234

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 10781 Color: 447
Size: 5246 Color: 370
Size: 272 Color: 13

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 468
Size: 4284 Color: 355
Size: 176 Color: 5

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 475
Size: 4147 Color: 350

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 12876 Color: 503
Size: 3423 Color: 326

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 13103 Color: 516
Size: 3196 Color: 316

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 14078 Color: 568
Size: 2221 Color: 260

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 570
Size: 2131 Color: 253

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 14238 Color: 573
Size: 2061 Color: 250

Bin 99: 6 of cap free
Amount of items: 9
Items: 
Size: 8154 Color: 406
Size: 1352 Color: 199
Size: 1352 Color: 197
Size: 1272 Color: 193
Size: 1232 Color: 192
Size: 1224 Color: 191
Size: 736 Color: 137
Size: 488 Color: 84
Size: 488 Color: 83

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 448
Size: 3124 Color: 312
Size: 2020 Color: 247

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 453
Size: 4754 Color: 362
Size: 272 Color: 11

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 538
Size: 2782 Color: 297

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13567 Color: 540
Size: 2731 Color: 293

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13774 Color: 553
Size: 2524 Color: 282

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 14306 Color: 576
Size: 1992 Color: 245

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14316 Color: 578
Size: 1982 Color: 244

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 591
Size: 1766 Color: 232

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 12792 Color: 500
Size: 3505 Color: 329

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 10428 Color: 439
Size: 5572 Color: 377
Size: 296 Color: 22

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 465
Size: 3090 Color: 309
Size: 1428 Color: 209

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 473
Size: 4124 Color: 348
Size: 96 Color: 1

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12822 Color: 502
Size: 3474 Color: 328

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13036 Color: 513
Size: 3260 Color: 319

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13436 Color: 532
Size: 2860 Color: 301

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 563
Size: 2288 Color: 264

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14364 Color: 581
Size: 1932 Color: 242

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 452
Size: 2669 Color: 290
Size: 2382 Color: 272

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 13641 Color: 544
Size: 2654 Color: 289

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 13657 Color: 545
Size: 2638 Color: 288

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 555
Size: 2462 Color: 279

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 13884 Color: 558
Size: 2411 Color: 276

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 474
Size: 4156 Color: 351

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 490
Size: 3802 Color: 342

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 14332 Color: 579
Size: 1962 Color: 243

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 583
Size: 1914 Color: 240

Bin 126: 11 of cap free
Amount of items: 5
Items: 
Size: 8170 Color: 412
Size: 6771 Color: 391
Size: 464 Color: 75
Size: 446 Color: 68
Size: 442 Color: 67

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 476
Size: 4109 Color: 347

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 13312 Color: 524
Size: 2981 Color: 308

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 526
Size: 2937 Color: 305

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 559
Size: 2365 Color: 270

Bin 131: 12 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 450
Size: 4856 Color: 365
Size: 272 Color: 12

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 454
Size: 3406 Color: 324
Size: 1562 Color: 218

Bin 133: 12 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 464
Size: 4290 Color: 356
Size: 240 Color: 8

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 12072 Color: 472
Size: 4220 Color: 353

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 482
Size: 4044 Color: 346

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 491
Size: 3786 Color: 341

Bin 137: 12 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 537
Size: 2780 Color: 296

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 525
Size: 2936 Color: 304

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 534
Size: 2838 Color: 300

Bin 140: 15 of cap free
Amount of items: 7
Items: 
Size: 8157 Color: 408
Size: 1722 Color: 230
Size: 1666 Color: 226
Size: 1660 Color: 224
Size: 1644 Color: 223
Size: 976 Color: 172
Size: 464 Color: 73

Bin 141: 15 of cap free
Amount of items: 3
Items: 
Size: 9979 Color: 432
Size: 5982 Color: 385
Size: 328 Color: 32

Bin 142: 15 of cap free
Amount of items: 2
Items: 
Size: 12761 Color: 498
Size: 3528 Color: 330

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 11452 Color: 459
Size: 4836 Color: 364

Bin 144: 16 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 469
Size: 4248 Color: 354
Size: 160 Color: 4

Bin 145: 16 of cap free
Amount of items: 2
Items: 
Size: 13592 Color: 542
Size: 2696 Color: 291

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 14088 Color: 569
Size: 2200 Color: 259

Bin 147: 16 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 571
Size: 2110 Color: 252

Bin 148: 18 of cap free
Amount of items: 3
Items: 
Size: 10692 Color: 445
Size: 5314 Color: 374
Size: 280 Color: 15

Bin 149: 18 of cap free
Amount of items: 2
Items: 
Size: 11158 Color: 449
Size: 5128 Color: 367

Bin 150: 18 of cap free
Amount of items: 2
Items: 
Size: 11992 Color: 470
Size: 4294 Color: 357

Bin 151: 18 of cap free
Amount of items: 2
Items: 
Size: 12598 Color: 495
Size: 3688 Color: 336

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 13080 Color: 515
Size: 3205 Color: 317

Bin 153: 20 of cap free
Amount of items: 2
Items: 
Size: 9196 Color: 424
Size: 7088 Color: 401

Bin 154: 20 of cap free
Amount of items: 3
Items: 
Size: 9860 Color: 429
Size: 6080 Color: 386
Size: 344 Color: 38

Bin 155: 20 of cap free
Amount of items: 3
Items: 
Size: 10011 Color: 435
Size: 5953 Color: 384
Size: 320 Color: 29

Bin 156: 20 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 438
Size: 5552 Color: 376
Size: 308 Color: 25

Bin 157: 20 of cap free
Amount of items: 2
Items: 
Size: 13697 Color: 549
Size: 2587 Color: 287

Bin 158: 20 of cap free
Amount of items: 2
Items: 
Size: 13749 Color: 552
Size: 2535 Color: 283

Bin 159: 21 of cap free
Amount of items: 2
Items: 
Size: 12972 Color: 509
Size: 3311 Color: 320

Bin 160: 22 of cap free
Amount of items: 3
Items: 
Size: 11329 Color: 455
Size: 4681 Color: 360
Size: 272 Color: 10

Bin 161: 28 of cap free
Amount of items: 4
Items: 
Size: 10508 Color: 441
Size: 5192 Color: 368
Size: 288 Color: 21
Size: 288 Color: 20

Bin 162: 28 of cap free
Amount of items: 2
Items: 
Size: 11372 Color: 457
Size: 4904 Color: 366

Bin 163: 29 of cap free
Amount of items: 3
Items: 
Size: 10687 Color: 444
Size: 5308 Color: 373
Size: 280 Color: 16

Bin 164: 29 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 527
Size: 2903 Color: 303

Bin 165: 30 of cap free
Amount of items: 2
Items: 
Size: 9193 Color: 423
Size: 7081 Color: 400

Bin 166: 35 of cap free
Amount of items: 6
Items: 
Size: 8162 Color: 409
Size: 2092 Color: 251
Size: 2041 Color: 248
Size: 1774 Color: 233
Size: 1740 Color: 231
Size: 460 Color: 72

Bin 167: 35 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 451
Size: 3633 Color: 335
Size: 1412 Color: 207

Bin 168: 35 of cap free
Amount of items: 2
Items: 
Size: 12821 Color: 501
Size: 3448 Color: 327

Bin 169: 44 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 471
Size: 4136 Color: 349
Size: 128 Color: 3

Bin 170: 46 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 434
Size: 5928 Color: 383
Size: 320 Color: 30

Bin 171: 46 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 436
Size: 5850 Color: 380
Size: 320 Color: 28

Bin 172: 46 of cap free
Amount of items: 2
Items: 
Size: 11500 Color: 460
Size: 4758 Color: 363

Bin 173: 53 of cap free
Amount of items: 3
Items: 
Size: 11823 Color: 467
Size: 4200 Color: 352
Size: 228 Color: 7

Bin 174: 54 of cap free
Amount of items: 3
Items: 
Size: 9995 Color: 433
Size: 5927 Color: 382
Size: 328 Color: 31

Bin 175: 60 of cap free
Amount of items: 3
Items: 
Size: 11375 Color: 458
Size: 4613 Color: 359
Size: 256 Color: 9

Bin 176: 61 of cap free
Amount of items: 4
Items: 
Size: 8636 Color: 418
Size: 6793 Color: 396
Size: 408 Color: 54
Size: 406 Color: 53

Bin 177: 67 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 443
Size: 5271 Color: 372
Size: 288 Color: 18

Bin 178: 76 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 427
Size: 6396 Color: 389
Size: 368 Color: 41

Bin 179: 87 of cap free
Amount of items: 2
Items: 
Size: 8169 Color: 411
Size: 8048 Color: 404

Bin 180: 112 of cap free
Amount of items: 3
Items: 
Size: 9688 Color: 428
Size: 6152 Color: 388
Size: 352 Color: 39

Bin 181: 112 of cap free
Amount of items: 3
Items: 
Size: 9940 Color: 431
Size: 5924 Color: 381
Size: 328 Color: 33

Bin 182: 112 of cap free
Amount of items: 2
Items: 
Size: 10488 Color: 440
Size: 5704 Color: 379

Bin 183: 116 of cap free
Amount of items: 3
Items: 
Size: 10168 Color: 437
Size: 5700 Color: 378
Size: 320 Color: 26

Bin 184: 132 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 420
Size: 6796 Color: 398
Size: 392 Color: 51

Bin 185: 162 of cap free
Amount of items: 3
Items: 
Size: 9286 Color: 426
Size: 6488 Color: 390
Size: 368 Color: 42

Bin 186: 170 of cap free
Amount of items: 3
Items: 
Size: 8940 Color: 419
Size: 6794 Color: 397
Size: 400 Color: 52

Bin 187: 172 of cap free
Amount of items: 4
Items: 
Size: 8520 Color: 417
Size: 6786 Color: 395
Size: 416 Color: 56
Size: 410 Color: 55

Bin 188: 173 of cap free
Amount of items: 3
Items: 
Size: 10598 Color: 442
Size: 5245 Color: 369
Size: 288 Color: 19

Bin 189: 174 of cap free
Amount of items: 4
Items: 
Size: 9930 Color: 430
Size: 5528 Color: 375
Size: 344 Color: 37
Size: 328 Color: 34

Bin 190: 208 of cap free
Amount of items: 4
Items: 
Size: 9208 Color: 425
Size: 6140 Color: 387
Size: 380 Color: 47
Size: 368 Color: 44

Bin 191: 232 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 416
Size: 7452 Color: 403
Size: 420 Color: 57

Bin 192: 271 of cap free
Amount of items: 2
Items: 
Size: 9161 Color: 422
Size: 6872 Color: 399

Bin 193: 274 of cap free
Amount of items: 6
Items: 
Size: 8168 Color: 410
Size: 2396 Color: 273
Size: 2324 Color: 267
Size: 2246 Color: 261
Size: 448 Color: 70
Size: 448 Color: 69

Bin 194: 292 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 415
Size: 7400 Color: 402
Size: 424 Color: 58

Bin 195: 320 of cap free
Amount of items: 23
Items: 
Size: 832 Color: 156
Size: 832 Color: 155
Size: 828 Color: 154
Size: 820 Color: 152
Size: 816 Color: 151
Size: 808 Color: 150
Size: 808 Color: 149
Size: 768 Color: 146
Size: 764 Color: 145
Size: 756 Color: 144
Size: 756 Color: 143
Size: 746 Color: 140
Size: 632 Color: 118
Size: 620 Color: 114
Size: 616 Color: 113
Size: 596 Color: 112
Size: 586 Color: 109
Size: 580 Color: 107
Size: 576 Color: 106
Size: 568 Color: 105
Size: 564 Color: 104
Size: 560 Color: 103
Size: 552 Color: 99

Bin 196: 323 of cap free
Amount of items: 9
Items: 
Size: 8153 Color: 405
Size: 1192 Color: 190
Size: 1168 Color: 186
Size: 1168 Color: 185
Size: 1168 Color: 184
Size: 1088 Color: 182
Size: 1060 Color: 180
Size: 496 Color: 86
Size: 488 Color: 85

Bin 197: 324 of cap free
Amount of items: 7
Items: 
Size: 8156 Color: 407
Size: 1524 Color: 216
Size: 1512 Color: 215
Size: 1492 Color: 213
Size: 1448 Color: 211
Size: 1384 Color: 206
Size: 464 Color: 74

Bin 198: 536 of cap free
Amount of items: 20
Items: 
Size: 1056 Color: 179
Size: 1048 Color: 176
Size: 1048 Color: 175
Size: 1008 Color: 173
Size: 960 Color: 171
Size: 896 Color: 164
Size: 872 Color: 163
Size: 872 Color: 162
Size: 856 Color: 161
Size: 856 Color: 160
Size: 856 Color: 159
Size: 840 Color: 158
Size: 832 Color: 157
Size: 552 Color: 98
Size: 544 Color: 97
Size: 544 Color: 96
Size: 544 Color: 95
Size: 532 Color: 94
Size: 528 Color: 93
Size: 524 Color: 92

Bin 199: 10334 of cap free
Amount of items: 9
Items: 
Size: 712 Color: 136
Size: 678 Color: 129
Size: 672 Color: 128
Size: 672 Color: 127
Size: 664 Color: 126
Size: 662 Color: 125
Size: 646 Color: 122
Size: 632 Color: 120
Size: 632 Color: 119

Total size: 3228192
Total free space: 16304

