Capicity Bin: 16160
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11568 Color: 462
Size: 4296 Color: 354
Size: 296 Color: 24

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11596 Color: 464
Size: 4276 Color: 353
Size: 288 Color: 22

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 468
Size: 2493 Color: 291
Size: 1964 Color: 256

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11708 Color: 470
Size: 3632 Color: 332
Size: 820 Color: 151

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12233 Color: 483
Size: 3355 Color: 327
Size: 572 Color: 113

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 484
Size: 3212 Color: 318
Size: 704 Color: 136

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12272 Color: 485
Size: 3332 Color: 325
Size: 556 Color: 112

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12541 Color: 490
Size: 3017 Color: 313
Size: 602 Color: 119

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 495
Size: 2776 Color: 305
Size: 752 Color: 147

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12880 Color: 500
Size: 2826 Color: 307
Size: 454 Color: 84

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 504
Size: 2768 Color: 304
Size: 428 Color: 76

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 505
Size: 2936 Color: 309
Size: 240 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13048 Color: 507
Size: 1968 Color: 257
Size: 1144 Color: 182

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13098 Color: 508
Size: 2554 Color: 296
Size: 508 Color: 101

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13104 Color: 509
Size: 3024 Color: 314
Size: 32 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13168 Color: 512
Size: 2008 Color: 259
Size: 984 Color: 169

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 517
Size: 2394 Color: 282
Size: 498 Color: 99

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 518
Size: 1456 Color: 210
Size: 1432 Color: 209

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 519
Size: 2496 Color: 292
Size: 376 Color: 59

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 520
Size: 1798 Color: 241
Size: 1072 Color: 174

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 521
Size: 1904 Color: 250
Size: 960 Color: 165

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13338 Color: 522
Size: 2408 Color: 283
Size: 414 Color: 70

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13403 Color: 524
Size: 2299 Color: 278
Size: 458 Color: 85

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13408 Color: 525
Size: 2408 Color: 284
Size: 344 Color: 45

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 528
Size: 1956 Color: 255
Size: 744 Color: 145

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13463 Color: 529
Size: 2249 Color: 274
Size: 448 Color: 83

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13468 Color: 530
Size: 2244 Color: 273
Size: 448 Color: 82

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13506 Color: 532
Size: 1550 Color: 216
Size: 1104 Color: 179

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13634 Color: 535
Size: 1374 Color: 205
Size: 1152 Color: 183

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13768 Color: 544
Size: 1912 Color: 251
Size: 480 Color: 95

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13770 Color: 545
Size: 1630 Color: 221
Size: 760 Color: 148

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 547
Size: 1624 Color: 220
Size: 728 Color: 138

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 549
Size: 1604 Color: 219
Size: 736 Color: 141

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13827 Color: 550
Size: 1945 Color: 253
Size: 388 Color: 66

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 556
Size: 1733 Color: 232
Size: 476 Color: 89

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13954 Color: 557
Size: 1842 Color: 246
Size: 364 Color: 56

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 561
Size: 1788 Color: 238
Size: 352 Color: 49

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 563
Size: 1680 Color: 226
Size: 448 Color: 81

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14093 Color: 568
Size: 1723 Color: 231
Size: 344 Color: 46

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14096 Color: 569
Size: 1080 Color: 175
Size: 984 Color: 168

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14098 Color: 570
Size: 1534 Color: 215
Size: 528 Color: 105

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 572
Size: 1484 Color: 214
Size: 552 Color: 111

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14154 Color: 573
Size: 1346 Color: 201
Size: 660 Color: 132

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 575
Size: 1384 Color: 206
Size: 608 Color: 122

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14172 Color: 576
Size: 1160 Color: 184
Size: 828 Color: 152

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14173 Color: 577
Size: 1603 Color: 218
Size: 384 Color: 64

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 578
Size: 1564 Color: 217
Size: 418 Color: 74

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 580
Size: 1720 Color: 228
Size: 224 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14233 Color: 581
Size: 1799 Color: 242
Size: 128 Color: 5

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 582
Size: 1192 Color: 188
Size: 732 Color: 139

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 586
Size: 1360 Color: 202
Size: 498 Color: 100

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14446 Color: 591
Size: 1430 Color: 208
Size: 284 Color: 19

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14448 Color: 592
Size: 1056 Color: 173
Size: 656 Color: 129

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 593
Size: 1428 Color: 207
Size: 280 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 596
Size: 1168 Color: 186
Size: 478 Color: 90

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 597
Size: 1164 Color: 185
Size: 480 Color: 91

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14524 Color: 598
Size: 1312 Color: 191
Size: 324 Color: 38

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14526 Color: 599
Size: 1090 Color: 178
Size: 544 Color: 108

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14544 Color: 600
Size: 1184 Color: 187
Size: 432 Color: 77

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11704 Color: 469
Size: 3691 Color: 335
Size: 764 Color: 149

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12557 Color: 492
Size: 3352 Color: 326
Size: 250 Color: 9

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 12911 Color: 503
Size: 3248 Color: 320

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13165 Color: 511
Size: 2162 Color: 269
Size: 832 Color: 153

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13169 Color: 513
Size: 2622 Color: 299
Size: 368 Color: 58

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 13647 Color: 537
Size: 2512 Color: 294

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14021 Color: 562
Size: 1792 Color: 240
Size: 346 Color: 47

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 583
Size: 1922 Color: 252

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14270 Color: 584
Size: 1889 Color: 248

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 10922 Color: 449
Size: 3444 Color: 331
Size: 1792 Color: 239

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 452
Size: 3658 Color: 333
Size: 1484 Color: 213

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11774 Color: 473
Size: 4112 Color: 348
Size: 272 Color: 15

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12545 Color: 491
Size: 1891 Color: 249
Size: 1722 Color: 229

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 494
Size: 3066 Color: 315
Size: 488 Color: 96

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 523
Size: 2778 Color: 306
Size: 28 Color: 0

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13675 Color: 539
Size: 2483 Color: 290

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 551
Size: 2304 Color: 279

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 559
Size: 2170 Color: 270

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14206 Color: 579
Size: 1952 Color: 254

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14382 Color: 588
Size: 1776 Color: 237

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14504 Color: 595
Size: 1654 Color: 222

Bin 81: 3 of cap free
Amount of items: 4
Items: 
Size: 9622 Color: 431
Size: 5829 Color: 384
Size: 354 Color: 53
Size: 352 Color: 52

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 11676 Color: 467
Size: 4197 Color: 351
Size: 284 Color: 20

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 493
Size: 1841 Color: 245
Size: 1756 Color: 234

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14033 Color: 564
Size: 2124 Color: 267

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 496
Size: 3120 Color: 316
Size: 368 Color: 57

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 13204 Color: 515
Size: 2952 Color: 310

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 13488 Color: 531
Size: 2668 Color: 300

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 536
Size: 2516 Color: 295

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13812 Color: 548
Size: 2344 Color: 280

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 571
Size: 2052 Color: 260

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 14388 Color: 589
Size: 1768 Color: 235

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 590
Size: 1744 Color: 233

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 594
Size: 1700 Color: 227

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 454
Size: 4771 Color: 360
Size: 304 Color: 27

Bin 95: 5 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 498
Size: 3293 Color: 323
Size: 32 Color: 1

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 13018 Color: 506
Size: 3137 Color: 317

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 13431 Color: 527
Size: 2724 Color: 302

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 552
Size: 2275 Color: 277

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 566
Size: 2095 Color: 264

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 11566 Color: 461
Size: 3384 Color: 329
Size: 1204 Color: 189

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 499
Size: 3314 Color: 324

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13738 Color: 543
Size: 2416 Color: 286

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 14160 Color: 574
Size: 1994 Color: 258

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 585
Size: 1862 Color: 247

Bin 105: 7 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 479
Size: 3715 Color: 337
Size: 252 Color: 10

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 13181 Color: 514
Size: 2972 Color: 311

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 587
Size: 1773 Color: 236

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 487
Size: 3844 Color: 345

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 12432 Color: 488
Size: 3720 Color: 338

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 13968 Color: 558
Size: 2184 Color: 271

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 14006 Color: 560
Size: 2146 Color: 268

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 14081 Color: 567
Size: 2071 Color: 262

Bin 113: 9 of cap free
Amount of items: 2
Items: 
Size: 12883 Color: 501
Size: 3268 Color: 321

Bin 114: 9 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 510
Size: 3003 Color: 312

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 10467 Color: 446
Size: 5363 Color: 370
Size: 320 Color: 33

Bin 116: 10 of cap free
Amount of items: 3
Items: 
Size: 11731 Color: 471
Size: 4147 Color: 349
Size: 272 Color: 17

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 13936 Color: 555
Size: 2214 Color: 272

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 11185 Color: 458
Size: 4964 Color: 367

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 13893 Color: 554
Size: 2256 Color: 276

Bin 120: 12 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 451
Size: 4840 Color: 361
Size: 320 Color: 29

Bin 121: 12 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 478
Size: 3740 Color: 339
Size: 256 Color: 11

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 13232 Color: 516
Size: 2916 Color: 308

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 13651 Color: 538
Size: 2497 Color: 293

Bin 124: 12 of cap free
Amount of items: 2
Items: 
Size: 13700 Color: 541
Size: 2448 Color: 288

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 12482 Color: 489
Size: 3665 Color: 334

Bin 126: 13 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 526
Size: 2731 Color: 303

Bin 127: 13 of cap free
Amount of items: 2
Items: 
Size: 13679 Color: 540
Size: 2468 Color: 289

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 13570 Color: 534
Size: 2576 Color: 297

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 565
Size: 2106 Color: 266

Bin 130: 16 of cap free
Amount of items: 2
Items: 
Size: 11168 Color: 457
Size: 4976 Color: 368

Bin 131: 16 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 533
Size: 2600 Color: 298

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 542
Size: 2412 Color: 285

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 13790 Color: 546
Size: 2354 Color: 281

Bin 134: 19 of cap free
Amount of items: 3
Items: 
Size: 10144 Color: 435
Size: 3288 Color: 322
Size: 2709 Color: 301

Bin 135: 19 of cap free
Amount of items: 3
Items: 
Size: 10360 Color: 444
Size: 5461 Color: 376
Size: 320 Color: 36

Bin 136: 19 of cap free
Amount of items: 2
Items: 
Size: 11129 Color: 456
Size: 5012 Color: 369

Bin 137: 19 of cap free
Amount of items: 3
Items: 
Size: 11660 Color: 466
Size: 4193 Color: 350
Size: 288 Color: 21

Bin 138: 20 of cap free
Amount of items: 2
Items: 
Size: 11824 Color: 474
Size: 4316 Color: 355

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 12900 Color: 502
Size: 3240 Color: 319

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 13888 Color: 553
Size: 2252 Color: 275

Bin 141: 21 of cap free
Amount of items: 3
Items: 
Size: 12209 Color: 481
Size: 3830 Color: 343
Size: 100 Color: 4

Bin 142: 22 of cap free
Amount of items: 2
Items: 
Size: 12778 Color: 497
Size: 3360 Color: 328

Bin 143: 23 of cap free
Amount of items: 4
Items: 
Size: 9609 Color: 430
Size: 5812 Color: 383
Size: 360 Color: 55
Size: 356 Color: 54

Bin 144: 23 of cap free
Amount of items: 3
Items: 
Size: 11763 Color: 472
Size: 4102 Color: 347
Size: 272 Color: 16

Bin 145: 24 of cap free
Amount of items: 7
Items: 
Size: 8088 Color: 409
Size: 2104 Color: 265
Size: 2091 Color: 263
Size: 2069 Color: 261
Size: 752 Color: 146
Size: 520 Color: 104
Size: 512 Color: 103

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 465
Size: 4496 Color: 358

Bin 147: 24 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 486
Size: 3856 Color: 346

Bin 148: 26 of cap free
Amount of items: 5
Items: 
Size: 8092 Color: 410
Size: 3418 Color: 330
Size: 2440 Color: 287
Size: 1672 Color: 225
Size: 512 Color: 102

Bin 149: 26 of cap free
Amount of items: 3
Items: 
Size: 8986 Color: 419
Size: 6730 Color: 398
Size: 418 Color: 73

Bin 150: 28 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 423
Size: 6528 Color: 390
Size: 416 Color: 72

Bin 151: 30 of cap free
Amount of items: 2
Items: 
Size: 10148 Color: 436
Size: 5982 Color: 386

Bin 152: 32 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 477
Size: 3768 Color: 341
Size: 256 Color: 12

Bin 153: 32 of cap free
Amount of items: 3
Items: 
Size: 12196 Color: 480
Size: 3804 Color: 342
Size: 128 Color: 6

Bin 154: 36 of cap free
Amount of items: 3
Items: 
Size: 10948 Color: 450
Size: 4856 Color: 362
Size: 320 Color: 30

Bin 155: 38 of cap free
Amount of items: 9
Items: 
Size: 8082 Color: 405
Size: 1328 Color: 195
Size: 1328 Color: 194
Size: 1328 Color: 193
Size: 1328 Color: 192
Size: 1000 Color: 172
Size: 576 Color: 116
Size: 576 Color: 115
Size: 576 Color: 114

Bin 156: 40 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 443
Size: 5456 Color: 375
Size: 320 Color: 37

Bin 157: 40 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 463
Size: 4248 Color: 352
Size: 296 Color: 23

Bin 158: 44 of cap free
Amount of items: 3
Items: 
Size: 10260 Color: 442
Size: 5528 Color: 378
Size: 328 Color: 39

Bin 159: 48 of cap free
Amount of items: 4
Items: 
Size: 9560 Color: 429
Size: 5784 Color: 382
Size: 384 Color: 61
Size: 384 Color: 60

Bin 160: 49 of cap free
Amount of items: 3
Items: 
Size: 9725 Color: 434
Size: 6034 Color: 388
Size: 352 Color: 48

Bin 161: 60 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 482
Size: 3832 Color: 344
Size: 52 Color: 3

Bin 162: 68 of cap free
Amount of items: 3
Items: 
Size: 8944 Color: 418
Size: 6728 Color: 397
Size: 420 Color: 75

Bin 163: 70 of cap free
Amount of items: 3
Items: 
Size: 8922 Color: 417
Size: 6728 Color: 396
Size: 440 Color: 78

Bin 164: 70 of cap free
Amount of items: 3
Items: 
Size: 10250 Color: 441
Size: 5512 Color: 377
Size: 328 Color: 40

Bin 165: 70 of cap free
Amount of items: 3
Items: 
Size: 12062 Color: 476
Size: 3756 Color: 340
Size: 272 Color: 13

Bin 166: 71 of cap free
Amount of items: 3
Items: 
Size: 11036 Color: 453
Size: 4745 Color: 359
Size: 308 Color: 28

Bin 167: 73 of cap free
Amount of items: 2
Items: 
Size: 11125 Color: 455
Size: 4962 Color: 366

Bin 168: 84 of cap free
Amount of items: 4
Items: 
Size: 9528 Color: 428
Size: 5780 Color: 381
Size: 384 Color: 63
Size: 384 Color: 62

Bin 169: 102 of cap free
Amount of items: 3
Items: 
Size: 9674 Color: 433
Size: 6032 Color: 387
Size: 352 Color: 50

Bin 170: 124 of cap free
Amount of items: 3
Items: 
Size: 10768 Color: 448
Size: 4948 Color: 365
Size: 320 Color: 31

Bin 171: 148 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 475
Size: 3712 Color: 336
Size: 272 Color: 14

Bin 172: 152 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 440
Size: 5450 Color: 374
Size: 330 Color: 41

Bin 173: 160 of cap free
Amount of items: 3
Items: 
Size: 10224 Color: 439
Size: 5440 Color: 373
Size: 336 Color: 42

Bin 174: 161 of cap free
Amount of items: 4
Items: 
Size: 10435 Color: 445
Size: 4924 Color: 363
Size: 320 Color: 35
Size: 320 Color: 34

Bin 175: 164 of cap free
Amount of items: 20
Items: 
Size: 992 Color: 171
Size: 984 Color: 170
Size: 976 Color: 167
Size: 960 Color: 166
Size: 960 Color: 164
Size: 954 Color: 163
Size: 948 Color: 162
Size: 896 Color: 161
Size: 872 Color: 160
Size: 864 Color: 159
Size: 856 Color: 158
Size: 658 Color: 131
Size: 656 Color: 130
Size: 648 Color: 128
Size: 640 Color: 127
Size: 640 Color: 126
Size: 640 Color: 125
Size: 632 Color: 124
Size: 612 Color: 123
Size: 608 Color: 121

Bin 176: 189 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 427
Size: 6287 Color: 389
Size: 384 Color: 65

Bin 177: 198 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 438
Size: 5414 Color: 372
Size: 336 Color: 43

Bin 178: 200 of cap free
Amount of items: 7
Items: 
Size: 8085 Color: 407
Size: 1657 Color: 223
Size: 1482 Color: 212
Size: 1460 Color: 211
Size: 1372 Color: 204
Size: 1364 Color: 203
Size: 540 Color: 107

Bin 179: 200 of cap free
Amount of items: 2
Items: 
Size: 9224 Color: 424
Size: 6736 Color: 402

Bin 180: 208 of cap free
Amount of items: 3
Items: 
Size: 10206 Color: 437
Size: 5406 Color: 371
Size: 340 Color: 44

Bin 181: 220 of cap free
Amount of items: 3
Items: 
Size: 10694 Color: 447
Size: 4926 Color: 364
Size: 320 Color: 32

Bin 182: 242 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 460
Size: 4366 Color: 357
Size: 304 Color: 25

Bin 183: 259 of cap free
Amount of items: 2
Items: 
Size: 9167 Color: 422
Size: 6734 Color: 401

Bin 184: 264 of cap free
Amount of items: 2
Items: 
Size: 9163 Color: 421
Size: 6733 Color: 400

Bin 185: 266 of cap free
Amount of items: 8
Items: 
Size: 8084 Color: 406
Size: 1344 Color: 200
Size: 1344 Color: 199
Size: 1344 Color: 198
Size: 1344 Color: 197
Size: 1344 Color: 196
Size: 546 Color: 110
Size: 544 Color: 109

Bin 186: 270 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 459
Size: 4348 Color: 356
Size: 304 Color: 26

Bin 187: 281 of cap free
Amount of items: 2
Items: 
Size: 9148 Color: 420
Size: 6731 Color: 399

Bin 188: 316 of cap free
Amount of items: 3
Items: 
Size: 9648 Color: 432
Size: 5844 Color: 385
Size: 352 Color: 51

Bin 189: 332 of cap free
Amount of items: 2
Items: 
Size: 8104 Color: 411
Size: 7724 Color: 403

Bin 190: 340 of cap free
Amount of items: 4
Items: 
Size: 8200 Color: 416
Size: 6724 Color: 395
Size: 448 Color: 80
Size: 448 Color: 79

Bin 191: 340 of cap free
Amount of items: 4
Items: 
Size: 9268 Color: 426
Size: 5748 Color: 380
Size: 408 Color: 68
Size: 396 Color: 67

Bin 192: 356 of cap free
Amount of items: 4
Items: 
Size: 9252 Color: 425
Size: 5724 Color: 379
Size: 416 Color: 71
Size: 412 Color: 69

Bin 193: 368 of cap free
Amount of items: 4
Items: 
Size: 8168 Color: 415
Size: 6696 Color: 394
Size: 464 Color: 87
Size: 464 Color: 86

Bin 194: 380 of cap free
Amount of items: 4
Items: 
Size: 8148 Color: 414
Size: 6684 Color: 393
Size: 480 Color: 92
Size: 468 Color: 88

Bin 195: 400 of cap free
Amount of items: 4
Items: 
Size: 8136 Color: 413
Size: 6664 Color: 392
Size: 480 Color: 94
Size: 480 Color: 93

Bin 196: 408 of cap free
Amount of items: 4
Items: 
Size: 8112 Color: 412
Size: 6648 Color: 391
Size: 496 Color: 98
Size: 496 Color: 97

Bin 197: 504 of cap free
Amount of items: 6
Items: 
Size: 8086 Color: 408
Size: 1840 Color: 244
Size: 1812 Color: 243
Size: 1722 Color: 230
Size: 1660 Color: 224
Size: 536 Color: 106

Bin 198: 535 of cap free
Amount of items: 9
Items: 
Size: 8081 Color: 404
Size: 1312 Color: 190
Size: 1136 Color: 181
Size: 1128 Color: 180
Size: 1088 Color: 177
Size: 1088 Color: 176
Size: 608 Color: 120
Size: 600 Color: 118
Size: 584 Color: 117

Bin 199: 6278 of cap free
Amount of items: 13
Items: 
Size: 848 Color: 157
Size: 848 Color: 156
Size: 838 Color: 155
Size: 838 Color: 154
Size: 800 Color: 150
Size: 744 Color: 144
Size: 742 Color: 143
Size: 738 Color: 142
Size: 736 Color: 140
Size: 710 Color: 137
Size: 688 Color: 135
Size: 680 Color: 134
Size: 672 Color: 133

Total size: 3199680
Total free space: 16160

