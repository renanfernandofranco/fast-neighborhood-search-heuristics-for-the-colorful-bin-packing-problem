Capicity Bin: 1001
Lower Bound: 44

Bins used: 44
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 1
Size: 488 Color: 4

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 4
Size: 455 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 584 Color: 0
Size: 235 Color: 1
Size: 182 Color: 2

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 3
Size: 363 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 1
Size: 162 Color: 3
Size: 148 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 3
Size: 192 Color: 4
Size: 122 Color: 1

Bin 7: 1 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 308 Color: 4
Size: 272 Color: 3

Bin 8: 1 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 2
Size: 487 Color: 3

Bin 9: 1 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 476 Color: 2

Bin 10: 1 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 468 Color: 2

Bin 11: 1 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 3
Size: 212 Color: 1
Size: 184 Color: 3

Bin 12: 1 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 3
Size: 156 Color: 1
Size: 138 Color: 3

Bin 13: 1 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 202 Color: 4

Bin 14: 2 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 1
Size: 159 Color: 2
Size: 141 Color: 0

Bin 15: 3 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 2
Size: 149 Color: 4
Size: 139 Color: 1

Bin 16: 3 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 240 Color: 4

Bin 17: 3 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 0
Size: 117 Color: 0
Size: 114 Color: 1

Bin 18: 4 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 3
Size: 467 Color: 0

Bin 19: 5 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 348 Color: 4

Bin 20: 7 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 1
Size: 171 Color: 0
Size: 136 Color: 4

Bin 21: 8 of cap free
Amount of items: 3
Items: 
Size: 507 Color: 0
Size: 272 Color: 3
Size: 214 Color: 1

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 3
Size: 274 Color: 2

Bin 23: 8 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 1
Size: 225 Color: 2

Bin 24: 12 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 4
Size: 315 Color: 2
Size: 238 Color: 1

Bin 25: 14 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 2
Size: 252 Color: 0

Bin 26: 15 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 222 Color: 0

Bin 27: 16 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 442 Color: 0

Bin 28: 17 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 250 Color: 3

Bin 29: 18 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 0
Size: 336 Color: 1

Bin 30: 19 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 359 Color: 4

Bin 31: 20 of cap free
Amount of items: 2
Items: 
Size: 496 Color: 4
Size: 485 Color: 1

Bin 32: 20 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 4
Size: 355 Color: 1

Bin 33: 20 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 2
Size: 197 Color: 4

Bin 34: 22 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 316 Color: 0

Bin 35: 24 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 2
Size: 323 Color: 1
Size: 311 Color: 1

Bin 36: 26 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 435 Color: 0

Bin 37: 27 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 363 Color: 0

Bin 38: 54 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 1
Size: 194 Color: 0

Bin 39: 72 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 3
Size: 346 Color: 2

Bin 40: 73 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 193 Color: 0

Bin 41: 88 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 3
Size: 386 Color: 1

Bin 42: 90 of cap free
Amount of items: 2
Items: 
Size: 472 Color: 0
Size: 439 Color: 4

Bin 43: 93 of cap free
Amount of items: 3
Items: 
Size: 332 Color: 2
Size: 293 Color: 4
Size: 283 Color: 3

Bin 44: 99 of cap free
Amount of items: 2
Items: 
Size: 477 Color: 1
Size: 425 Color: 2

Total size: 43147
Total free space: 897

