Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 272 Color: 0
Size: 254 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 3
Size: 252 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 274 Color: 2
Size: 327 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 277 Color: 2
Size: 273 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 278 Color: 1
Size: 258 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 334 Color: 3
Size: 251 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 367 Color: 1
Size: 266 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 262 Color: 0
Size: 254 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 303 Color: 3
Size: 282 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 365 Color: 2
Size: 269 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 2
Size: 361 Color: 0
Size: 250 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 309 Color: 0
Size: 279 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 2
Size: 257 Color: 4
Size: 251 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 281 Color: 3
Size: 259 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 336 Color: 3
Size: 264 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 282 Color: 2
Size: 270 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 336 Color: 3
Size: 311 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 340 Color: 4
Size: 300 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 311 Color: 3
Size: 260 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 270 Color: 0
Size: 260 Color: 3

Total size: 20000
Total free space: 0

