Capicity Bin: 6528
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3709 Color: 2
Size: 2279 Color: 0
Size: 540 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3771 Color: 1
Size: 2295 Color: 3
Size: 462 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 2
Size: 2212 Color: 0
Size: 112 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4622 Color: 2
Size: 1570 Color: 3
Size: 336 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 2
Size: 1828 Color: 1
Size: 64 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 3
Size: 1564 Color: 0
Size: 216 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 2
Size: 860 Color: 3
Size: 800 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 0
Size: 1275 Color: 3
Size: 158 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5119 Color: 3
Size: 1175 Color: 2
Size: 234 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 2
Size: 1151 Color: 0
Size: 254 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5143 Color: 1
Size: 931 Color: 0
Size: 454 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 2
Size: 761 Color: 0
Size: 619 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 3
Size: 1139 Color: 4
Size: 226 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 4
Size: 1131 Color: 2
Size: 226 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 2
Size: 1155 Color: 1
Size: 190 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 3
Size: 1254 Color: 0
Size: 44 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 1156 Color: 4
Size: 88 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 3
Size: 933 Color: 2
Size: 296 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5366 Color: 4
Size: 754 Color: 0
Size: 408 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 2
Size: 948 Color: 0
Size: 184 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 2
Size: 700 Color: 0
Size: 424 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5405 Color: 3
Size: 937 Color: 0
Size: 186 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 2
Size: 575 Color: 3
Size: 540 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 1
Size: 772 Color: 1
Size: 298 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 2
Size: 867 Color: 4
Size: 172 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 4
Size: 610 Color: 0
Size: 392 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 1
Size: 811 Color: 4
Size: 162 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5579 Color: 0
Size: 873 Color: 3
Size: 76 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5599 Color: 0
Size: 775 Color: 3
Size: 154 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 0
Size: 791 Color: 2
Size: 122 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5626 Color: 2
Size: 542 Color: 0
Size: 360 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 2
Size: 566 Color: 0
Size: 312 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 3
Size: 615 Color: 2
Size: 254 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5683 Color: 3
Size: 705 Color: 2
Size: 140 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 0
Size: 564 Color: 2
Size: 272 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5712 Color: 4
Size: 436 Color: 1
Size: 380 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 2
Size: 540 Color: 0
Size: 248 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5777 Color: 0
Size: 627 Color: 2
Size: 124 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 2
Size: 643 Color: 3
Size: 128 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5814 Color: 0
Size: 458 Color: 4
Size: 256 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5791 Color: 4
Size: 649 Color: 0
Size: 88 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 0
Size: 448 Color: 3
Size: 230 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5854 Color: 2
Size: 454 Color: 0
Size: 220 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 0
Size: 2711 Color: 2
Size: 542 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 3831 Color: 0
Size: 2530 Color: 4
Size: 166 Color: 2

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 3823 Color: 4
Size: 2380 Color: 0
Size: 324 Color: 2

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 3827 Color: 1
Size: 2232 Color: 0
Size: 468 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 3860 Color: 1
Size: 2315 Color: 2
Size: 352 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 4
Size: 2249 Color: 0
Size: 312 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 0
Size: 2228 Color: 2
Size: 128 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 4237 Color: 2
Size: 2138 Color: 0
Size: 152 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 3
Size: 1683 Color: 0
Size: 504 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 4
Size: 1842 Color: 2
Size: 230 Color: 0

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 4509 Color: 0
Size: 1822 Color: 2
Size: 196 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 4671 Color: 1
Size: 1692 Color: 2
Size: 164 Color: 0

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 4737 Color: 4
Size: 1622 Color: 2
Size: 168 Color: 0

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 4995 Color: 0
Size: 1484 Color: 1
Size: 48 Color: 2

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 5046 Color: 0
Size: 941 Color: 2
Size: 540 Color: 4

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 5099 Color: 0
Size: 970 Color: 1
Size: 458 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 4
Size: 1284 Color: 1
Size: 96 Color: 3

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 5139 Color: 0
Size: 1388 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 5232 Color: 3
Size: 1159 Color: 4
Size: 136 Color: 0

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 0
Size: 818 Color: 2
Size: 328 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 5531 Color: 4
Size: 756 Color: 0
Size: 240 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 5815 Color: 0
Size: 536 Color: 3
Size: 176 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 3
Size: 464 Color: 0
Size: 224 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 4
Size: 680 Color: 1
Size: 4 Color: 1

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 3269 Color: 2
Size: 2251 Color: 4
Size: 1006 Color: 1

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 4
Size: 2714 Color: 0
Size: 352 Color: 2

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 0
Size: 2702 Color: 0
Size: 148 Color: 3

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 3795 Color: 2
Size: 2259 Color: 0
Size: 472 Color: 3

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 1
Size: 2198 Color: 0
Size: 78 Color: 4

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 4971 Color: 1
Size: 1191 Color: 0
Size: 364 Color: 2

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 1
Size: 1398 Color: 4
Size: 140 Color: 0

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 1
Size: 1378 Color: 4
Size: 122 Color: 0

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 3819 Color: 3
Size: 2706 Color: 1

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 4226 Color: 3
Size: 2299 Color: 1

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 4582 Color: 2
Size: 1407 Color: 4
Size: 536 Color: 2

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 1549 Color: 0
Size: 316 Color: 0

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 0
Size: 1082 Color: 2
Size: 440 Color: 1

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 5500 Color: 3
Size: 1025 Color: 2

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 5694 Color: 3
Size: 831 Color: 4

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 5730 Color: 2
Size: 795 Color: 4

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 3799 Color: 0
Size: 2275 Color: 2
Size: 450 Color: 4

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 0
Size: 1238 Color: 3
Size: 408 Color: 2

Bin 86: 5 of cap free
Amount of items: 4
Items: 
Size: 3266 Color: 0
Size: 2319 Color: 1
Size: 794 Color: 3
Size: 144 Color: 4

Bin 87: 5 of cap free
Amount of items: 3
Items: 
Size: 3722 Color: 3
Size: 2351 Color: 0
Size: 450 Color: 4

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 3747 Color: 2
Size: 2214 Color: 0
Size: 562 Color: 4

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 1279 Color: 2
Size: 88 Color: 1

Bin 90: 6 of cap free
Amount of items: 3
Items: 
Size: 3277 Color: 3
Size: 2255 Color: 2
Size: 990 Color: 1

Bin 91: 6 of cap free
Amount of items: 2
Items: 
Size: 5401 Color: 1
Size: 1121 Color: 3

Bin 92: 6 of cap free
Amount of items: 2
Items: 
Size: 5628 Color: 2
Size: 894 Color: 4

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 1
Size: 734 Color: 3
Size: 24 Color: 0

Bin 94: 7 of cap free
Amount of items: 3
Items: 
Size: 4967 Color: 0
Size: 1218 Color: 1
Size: 336 Color: 2

Bin 95: 8 of cap free
Amount of items: 29
Items: 
Size: 312 Color: 3
Size: 308 Color: 0
Size: 304 Color: 1
Size: 296 Color: 2
Size: 276 Color: 3
Size: 276 Color: 0
Size: 272 Color: 4
Size: 258 Color: 2
Size: 244 Color: 3
Size: 238 Color: 2
Size: 238 Color: 1
Size: 234 Color: 4
Size: 230 Color: 1
Size: 226 Color: 4
Size: 224 Color: 3
Size: 224 Color: 0
Size: 208 Color: 3
Size: 204 Color: 1
Size: 200 Color: 3
Size: 192 Color: 3
Size: 192 Color: 0
Size: 186 Color: 2
Size: 186 Color: 0
Size: 184 Color: 2
Size: 184 Color: 2
Size: 176 Color: 1
Size: 176 Color: 0
Size: 136 Color: 3
Size: 136 Color: 2

Bin 96: 8 of cap free
Amount of items: 3
Items: 
Size: 3290 Color: 4
Size: 2326 Color: 2
Size: 904 Color: 1

Bin 97: 8 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 1
Size: 1580 Color: 2
Size: 618 Color: 4

Bin 98: 8 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 2
Size: 698 Color: 1
Size: 24 Color: 3

Bin 99: 9 of cap free
Amount of items: 2
Items: 
Size: 4929 Color: 1
Size: 1590 Color: 0

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 5027 Color: 1
Size: 1491 Color: 4

Bin 101: 11 of cap free
Amount of items: 2
Items: 
Size: 5322 Color: 1
Size: 1195 Color: 4

Bin 102: 13 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 4
Size: 1749 Color: 1
Size: 120 Color: 2

Bin 103: 13 of cap free
Amount of items: 2
Items: 
Size: 5575 Color: 2
Size: 940 Color: 4

Bin 104: 15 of cap free
Amount of items: 2
Items: 
Size: 5342 Color: 2
Size: 1171 Color: 4

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 0
Size: 660 Color: 1

Bin 106: 17 of cap free
Amount of items: 2
Items: 
Size: 5409 Color: 1
Size: 1102 Color: 3

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 5670 Color: 1
Size: 838 Color: 3

Bin 108: 21 of cap free
Amount of items: 3
Items: 
Size: 4346 Color: 1
Size: 1517 Color: 0
Size: 644 Color: 4

Bin 109: 21 of cap free
Amount of items: 2
Items: 
Size: 5070 Color: 1
Size: 1437 Color: 2

Bin 110: 21 of cap free
Amount of items: 2
Items: 
Size: 5550 Color: 1
Size: 957 Color: 2

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 3282 Color: 3
Size: 3224 Color: 1

Bin 112: 22 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 1
Size: 1652 Color: 4

Bin 113: 23 of cap free
Amount of items: 2
Items: 
Size: 4741 Color: 3
Size: 1764 Color: 0

Bin 114: 23 of cap free
Amount of items: 2
Items: 
Size: 5206 Color: 2
Size: 1299 Color: 3

Bin 115: 23 of cap free
Amount of items: 2
Items: 
Size: 5787 Color: 3
Size: 718 Color: 2

Bin 116: 26 of cap free
Amount of items: 2
Items: 
Size: 4548 Color: 3
Size: 1954 Color: 4

Bin 117: 26 of cap free
Amount of items: 2
Items: 
Size: 5604 Color: 1
Size: 898 Color: 2

Bin 118: 29 of cap free
Amount of items: 2
Items: 
Size: 3775 Color: 3
Size: 2724 Color: 4

Bin 119: 30 of cap free
Amount of items: 2
Items: 
Size: 5454 Color: 3
Size: 1044 Color: 1

Bin 120: 32 of cap free
Amount of items: 2
Items: 
Size: 5276 Color: 3
Size: 1220 Color: 1

Bin 121: 36 of cap free
Amount of items: 2
Items: 
Size: 4060 Color: 3
Size: 2432 Color: 2

Bin 122: 36 of cap free
Amount of items: 2
Items: 
Size: 4999 Color: 4
Size: 1493 Color: 1

Bin 123: 36 of cap free
Amount of items: 2
Items: 
Size: 5167 Color: 3
Size: 1325 Color: 4

Bin 124: 56 of cap free
Amount of items: 2
Items: 
Size: 4412 Color: 4
Size: 2060 Color: 3

Bin 125: 60 of cap free
Amount of items: 2
Items: 
Size: 3751 Color: 2
Size: 2717 Color: 3

Bin 126: 63 of cap free
Amount of items: 2
Items: 
Size: 4500 Color: 2
Size: 1965 Color: 3

Bin 127: 68 of cap free
Amount of items: 2
Items: 
Size: 3738 Color: 3
Size: 2722 Color: 2

Bin 128: 70 of cap free
Amount of items: 2
Items: 
Size: 3894 Color: 4
Size: 2564 Color: 2

Bin 129: 72 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 2
Size: 2721 Color: 0
Size: 462 Color: 3

Bin 130: 83 of cap free
Amount of items: 3
Items: 
Size: 3265 Color: 0
Size: 2856 Color: 0
Size: 324 Color: 3

Bin 131: 97 of cap free
Amount of items: 8
Items: 
Size: 1911 Color: 1
Size: 1148 Color: 2
Size: 1135 Color: 1
Size: 571 Color: 1
Size: 542 Color: 0
Size: 440 Color: 2
Size: 360 Color: 0
Size: 324 Color: 0

Bin 132: 97 of cap free
Amount of items: 3
Items: 
Size: 3268 Color: 0
Size: 2713 Color: 2
Size: 450 Color: 3

Bin 133: 5156 of cap free
Amount of items: 11
Items: 
Size: 160 Color: 1
Size: 152 Color: 1
Size: 144 Color: 1
Size: 120 Color: 4
Size: 120 Color: 2
Size: 114 Color: 4
Size: 114 Color: 2
Size: 112 Color: 4
Size: 112 Color: 3
Size: 112 Color: 2
Size: 112 Color: 0

Total size: 861696
Total free space: 6528

