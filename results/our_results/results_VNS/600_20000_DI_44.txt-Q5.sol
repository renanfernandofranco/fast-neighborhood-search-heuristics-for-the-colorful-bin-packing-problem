Capicity Bin: 15632
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8834 Color: 1
Size: 5666 Color: 2
Size: 1132 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 2
Size: 5276 Color: 4
Size: 1048 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 4
Size: 5036 Color: 1
Size: 1064 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9756 Color: 1
Size: 5256 Color: 0
Size: 620 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 2
Size: 4730 Color: 0
Size: 944 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10319 Color: 2
Size: 5001 Color: 1
Size: 312 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 3
Size: 4584 Color: 4
Size: 608 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10658 Color: 4
Size: 4582 Color: 4
Size: 392 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11174 Color: 0
Size: 2348 Color: 1
Size: 2110 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 2
Size: 4014 Color: 3
Size: 356 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11400 Color: 0
Size: 3642 Color: 2
Size: 590 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11364 Color: 1
Size: 3812 Color: 0
Size: 456 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 3
Size: 2314 Color: 0
Size: 1703 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 0
Size: 3156 Color: 4
Size: 402 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12037 Color: 4
Size: 2997 Color: 4
Size: 598 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12181 Color: 0
Size: 2659 Color: 1
Size: 792 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 3
Size: 1920 Color: 0
Size: 1468 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12442 Color: 3
Size: 2856 Color: 0
Size: 334 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12507 Color: 1
Size: 2605 Color: 0
Size: 520 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 0
Size: 2192 Color: 2
Size: 828 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12772 Color: 0
Size: 1944 Color: 4
Size: 916 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 0
Size: 2070 Color: 4
Size: 460 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 1
Size: 2148 Color: 3
Size: 424 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 3
Size: 2360 Color: 4
Size: 180 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 2
Size: 2426 Color: 1
Size: 112 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 2
Size: 1758 Color: 1
Size: 756 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 0
Size: 1702 Color: 4
Size: 740 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 2
Size: 1302 Color: 0
Size: 1132 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 2
Size: 1324 Color: 4
Size: 1016 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13294 Color: 2
Size: 1880 Color: 1
Size: 458 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 4
Size: 1932 Color: 0
Size: 384 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 0
Size: 1852 Color: 0
Size: 414 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13430 Color: 0
Size: 1782 Color: 4
Size: 420 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 2
Size: 1859 Color: 3
Size: 362 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 1
Size: 1316 Color: 0
Size: 864 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 4
Size: 1590 Color: 2
Size: 530 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13526 Color: 1
Size: 1630 Color: 4
Size: 476 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 0
Size: 1678 Color: 4
Size: 412 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13622 Color: 4
Size: 1538 Color: 2
Size: 472 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 4
Size: 1517 Color: 2
Size: 488 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 1
Size: 1368 Color: 4
Size: 668 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 4
Size: 1146 Color: 3
Size: 780 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 2
Size: 1661 Color: 4
Size: 332 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13708 Color: 4
Size: 1560 Color: 2
Size: 364 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 4
Size: 1441 Color: 2
Size: 432 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 4
Size: 1136 Color: 3
Size: 732 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 4
Size: 1520 Color: 2
Size: 280 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 2
Size: 1524 Color: 4
Size: 304 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 4
Size: 1138 Color: 1
Size: 624 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 4
Size: 1470 Color: 2
Size: 268 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13876 Color: 2
Size: 1336 Color: 1
Size: 420 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13980 Color: 4
Size: 1300 Color: 3
Size: 352 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 4
Size: 1010 Color: 1
Size: 608 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 4
Size: 1052 Color: 3
Size: 560 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 1
Size: 1000 Color: 2
Size: 640 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 1
Size: 1120 Color: 4
Size: 472 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1300 Color: 0
Size: 272 Color: 4

Bin 58: 1 of cap free
Amount of items: 9
Items: 
Size: 7817 Color: 1
Size: 1348 Color: 0
Size: 1296 Color: 3
Size: 1296 Color: 3
Size: 1296 Color: 3
Size: 912 Color: 2
Size: 912 Color: 2
Size: 398 Color: 2
Size: 356 Color: 0

Bin 59: 1 of cap free
Amount of items: 5
Items: 
Size: 7826 Color: 4
Size: 4648 Color: 0
Size: 2291 Color: 1
Size: 532 Color: 2
Size: 334 Color: 4

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 8807 Color: 2
Size: 6504 Color: 2
Size: 320 Color: 3

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 0
Size: 5699 Color: 4
Size: 416 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10258 Color: 3
Size: 4479 Color: 0
Size: 894 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10307 Color: 3
Size: 5020 Color: 0
Size: 304 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11259 Color: 0
Size: 4076 Color: 1
Size: 296 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 3
Size: 3417 Color: 4
Size: 336 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 0
Size: 3130 Color: 4
Size: 416 Color: 1

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12201 Color: 4
Size: 2966 Color: 0
Size: 464 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12418 Color: 0
Size: 2877 Color: 4
Size: 336 Color: 2

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12778 Color: 0
Size: 1969 Color: 2
Size: 884 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12895 Color: 0
Size: 1872 Color: 4
Size: 864 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 0
Size: 1604 Color: 1
Size: 1048 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13147 Color: 0
Size: 2152 Color: 3
Size: 332 Color: 3

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13219 Color: 3
Size: 1700 Color: 4
Size: 712 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13339 Color: 3
Size: 1820 Color: 4
Size: 472 Color: 2

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13495 Color: 1
Size: 1608 Color: 2
Size: 528 Color: 4

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 8818 Color: 0
Size: 6508 Color: 4
Size: 304 Color: 3

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 9631 Color: 0
Size: 3208 Color: 0
Size: 2791 Color: 1

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 4
Size: 5282 Color: 2
Size: 292 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10114 Color: 1
Size: 5100 Color: 0
Size: 416 Color: 4

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 2
Size: 4328 Color: 3
Size: 242 Color: 4

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 0
Size: 4328 Color: 3
Size: 208 Color: 2

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 4
Size: 3702 Color: 1

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 1
Size: 2992 Color: 0
Size: 364 Color: 4

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 4
Size: 1838 Color: 4
Size: 1232 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 0
Size: 2011 Color: 4
Size: 736 Color: 2

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 4
Size: 2388 Color: 0
Size: 304 Color: 3

Bin 87: 3 of cap free
Amount of items: 5
Items: 
Size: 7821 Color: 3
Size: 5682 Color: 4
Size: 1606 Color: 4
Size: 280 Color: 3
Size: 240 Color: 0

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 8747 Color: 1
Size: 6506 Color: 2
Size: 376 Color: 0

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 9298 Color: 4
Size: 5739 Color: 4
Size: 592 Color: 1

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 9482 Color: 0
Size: 5011 Color: 1
Size: 1136 Color: 3

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 9571 Color: 4
Size: 5770 Color: 2
Size: 288 Color: 2

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 2
Size: 5689 Color: 2
Size: 344 Color: 0

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 10831 Color: 0
Size: 3782 Color: 4
Size: 1016 Color: 4

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 11190 Color: 1
Size: 4439 Color: 2

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 11738 Color: 0
Size: 3571 Color: 4
Size: 320 Color: 3

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 11911 Color: 3
Size: 3718 Color: 1

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 1
Size: 3349 Color: 2

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 12649 Color: 2
Size: 1956 Color: 1
Size: 1024 Color: 0

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12703 Color: 4
Size: 2662 Color: 2
Size: 264 Color: 0

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 8795 Color: 3
Size: 6513 Color: 2
Size: 320 Color: 0

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 0
Size: 3564 Color: 2
Size: 280 Color: 4

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 3
Size: 3528 Color: 0
Size: 256 Color: 2

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 2
Size: 2038 Color: 0

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 3
Size: 2091 Color: 0
Size: 728 Color: 1

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13446 Color: 4
Size: 2181 Color: 1

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 1
Size: 2019 Color: 0

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 7834 Color: 3
Size: 7236 Color: 4
Size: 556 Color: 1

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 0
Size: 5688 Color: 1
Size: 1040 Color: 2

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 0
Size: 2600 Color: 2
Size: 1032 Color: 4

Bin 110: 7 of cap free
Amount of items: 7
Items: 
Size: 7832 Color: 0
Size: 2281 Color: 1
Size: 1450 Color: 1
Size: 1448 Color: 2
Size: 1334 Color: 3
Size: 976 Color: 3
Size: 304 Color: 4

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 0
Size: 4001 Color: 3
Size: 560 Color: 4

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 11349 Color: 4
Size: 4276 Color: 2

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 13243 Color: 4
Size: 2382 Color: 3

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 0
Size: 5612 Color: 3
Size: 400 Color: 3

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 1
Size: 3588 Color: 0
Size: 944 Color: 1

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 2
Size: 2124 Color: 1

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13813 Color: 2
Size: 1811 Color: 0

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 12821 Color: 3
Size: 2802 Color: 1

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 13271 Color: 4
Size: 2352 Color: 3

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 4
Size: 5626 Color: 3
Size: 576 Color: 1

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 11598 Color: 3
Size: 4024 Color: 1

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13726 Color: 1
Size: 1896 Color: 0

Bin 123: 10 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 3
Size: 1679 Color: 1
Size: 40 Color: 4

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 2
Size: 6511 Color: 1
Size: 302 Color: 0

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 12520 Color: 1
Size: 3101 Color: 3

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 13950 Color: 1
Size: 1671 Color: 2

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 10818 Color: 2
Size: 3034 Color: 4
Size: 1768 Color: 1

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 11852 Color: 3
Size: 3768 Color: 4

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 3
Size: 2668 Color: 4

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 3
Size: 2161 Color: 1

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 1
Size: 1724 Color: 3

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 3
Size: 2071 Color: 1

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13589 Color: 0
Size: 2030 Color: 1

Bin 134: 14 of cap free
Amount of items: 3
Items: 
Size: 8882 Color: 1
Size: 5126 Color: 4
Size: 1610 Color: 3

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 11973 Color: 1
Size: 3645 Color: 3

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 1
Size: 2372 Color: 4
Size: 96 Color: 0

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 2
Size: 1740 Color: 0

Bin 138: 16 of cap free
Amount of items: 4
Items: 
Size: 7818 Color: 1
Size: 5051 Color: 0
Size: 2343 Color: 2
Size: 404 Color: 1

Bin 139: 16 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 4
Size: 4900 Color: 3
Size: 1380 Color: 2

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 2
Size: 2828 Color: 4

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 4
Size: 2072 Color: 1

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 14052 Color: 0
Size: 1564 Color: 1

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 12658 Color: 3
Size: 2957 Color: 1

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 3
Size: 3362 Color: 1

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 12820 Color: 3
Size: 2794 Color: 1

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 2
Size: 2246 Color: 3

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 13403 Color: 3
Size: 2211 Color: 2

Bin 148: 19 of cap free
Amount of items: 2
Items: 
Size: 13211 Color: 3
Size: 2402 Color: 1

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 0
Size: 1911 Color: 1

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 1
Size: 2548 Color: 3

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 13125 Color: 2
Size: 2487 Color: 4

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 13494 Color: 2
Size: 2118 Color: 3

Bin 153: 20 of cap free
Amount of items: 2
Items: 
Size: 13790 Color: 1
Size: 1822 Color: 3

Bin 154: 22 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 3
Size: 1982 Color: 2
Size: 64 Color: 1

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 13619 Color: 1
Size: 1991 Color: 0

Bin 156: 22 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 1
Size: 1890 Color: 2

Bin 157: 24 of cap free
Amount of items: 2
Items: 
Size: 13360 Color: 4
Size: 2248 Color: 3

Bin 158: 25 of cap free
Amount of items: 2
Items: 
Size: 12443 Color: 3
Size: 3164 Color: 1

Bin 159: 28 of cap free
Amount of items: 2
Items: 
Size: 13916 Color: 3
Size: 1688 Color: 2

Bin 160: 29 of cap free
Amount of items: 3
Items: 
Size: 10941 Color: 4
Size: 3662 Color: 3
Size: 1000 Color: 0

Bin 161: 30 of cap free
Amount of items: 3
Items: 
Size: 8172 Color: 4
Size: 6502 Color: 1
Size: 928 Color: 0

Bin 162: 31 of cap free
Amount of items: 2
Items: 
Size: 13160 Color: 1
Size: 2441 Color: 4

Bin 163: 32 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 4
Size: 5180 Color: 1

Bin 164: 32 of cap free
Amount of items: 2
Items: 
Size: 11689 Color: 3
Size: 3911 Color: 4

Bin 165: 33 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 2
Size: 2295 Color: 3

Bin 166: 37 of cap free
Amount of items: 2
Items: 
Size: 14034 Color: 2
Size: 1561 Color: 3

Bin 167: 40 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 4
Size: 6648 Color: 0
Size: 740 Color: 3

Bin 168: 40 of cap free
Amount of items: 2
Items: 
Size: 10508 Color: 3
Size: 5084 Color: 2

Bin 169: 42 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 4
Size: 3096 Color: 3
Size: 124 Color: 0

Bin 170: 43 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 3
Size: 1781 Color: 1

Bin 171: 45 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 1
Size: 1742 Color: 2

Bin 172: 46 of cap free
Amount of items: 2
Items: 
Size: 11238 Color: 1
Size: 4348 Color: 2

Bin 173: 47 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 4
Size: 3005 Color: 3

Bin 174: 47 of cap free
Amount of items: 2
Items: 
Size: 13039 Color: 2
Size: 2546 Color: 1

Bin 175: 48 of cap free
Amount of items: 2
Items: 
Size: 10136 Color: 2
Size: 5448 Color: 3

Bin 176: 49 of cap free
Amount of items: 2
Items: 
Size: 10259 Color: 1
Size: 5324 Color: 2

Bin 177: 51 of cap free
Amount of items: 4
Items: 
Size: 8072 Color: 0
Size: 5746 Color: 2
Size: 1491 Color: 3
Size: 272 Color: 1

Bin 178: 52 of cap free
Amount of items: 2
Items: 
Size: 12858 Color: 3
Size: 2722 Color: 2

Bin 179: 60 of cap free
Amount of items: 2
Items: 
Size: 12285 Color: 3
Size: 3287 Color: 4

Bin 180: 62 of cap free
Amount of items: 2
Items: 
Size: 8738 Color: 3
Size: 6832 Color: 2

Bin 181: 66 of cap free
Amount of items: 2
Items: 
Size: 9324 Color: 0
Size: 6242 Color: 4

Bin 182: 71 of cap free
Amount of items: 2
Items: 
Size: 12879 Color: 2
Size: 2682 Color: 3

Bin 183: 80 of cap free
Amount of items: 7
Items: 
Size: 7828 Color: 0
Size: 1436 Color: 4
Size: 1402 Color: 4
Size: 1350 Color: 0
Size: 1300 Color: 3
Size: 1148 Color: 2
Size: 1088 Color: 1

Bin 184: 80 of cap free
Amount of items: 2
Items: 
Size: 8900 Color: 1
Size: 6652 Color: 2

Bin 185: 80 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 1
Size: 3912 Color: 3

Bin 186: 84 of cap free
Amount of items: 2
Items: 
Size: 7820 Color: 2
Size: 7728 Color: 3

Bin 187: 90 of cap free
Amount of items: 2
Items: 
Size: 12722 Color: 4
Size: 2820 Color: 2

Bin 188: 91 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 1
Size: 4429 Color: 2

Bin 189: 118 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 4
Size: 2936 Color: 2

Bin 190: 128 of cap free
Amount of items: 19
Items: 
Size: 1296 Color: 1
Size: 1002 Color: 1
Size: 1000 Color: 1
Size: 964 Color: 1
Size: 886 Color: 0
Size: 864 Color: 1
Size: 848 Color: 3
Size: 808 Color: 1
Size: 800 Color: 4
Size: 800 Color: 3
Size: 800 Color: 0
Size: 760 Color: 1
Size: 728 Color: 3
Size: 712 Color: 1
Size: 704 Color: 2
Size: 656 Color: 0
Size: 648 Color: 2
Size: 624 Color: 2
Size: 604 Color: 0

Bin 191: 152 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 4
Size: 5260 Color: 3
Size: 1124 Color: 3

Bin 192: 154 of cap free
Amount of items: 2
Items: 
Size: 11332 Color: 2
Size: 4146 Color: 3

Bin 193: 170 of cap free
Amount of items: 2
Items: 
Size: 12216 Color: 2
Size: 3246 Color: 4

Bin 194: 192 of cap free
Amount of items: 2
Items: 
Size: 9244 Color: 3
Size: 6196 Color: 0

Bin 195: 204 of cap free
Amount of items: 2
Items: 
Size: 8914 Color: 4
Size: 6514 Color: 1

Bin 196: 205 of cap free
Amount of items: 2
Items: 
Size: 9619 Color: 4
Size: 5808 Color: 1

Bin 197: 210 of cap free
Amount of items: 34
Items: 
Size: 672 Color: 4
Size: 624 Color: 2
Size: 574 Color: 3
Size: 560 Color: 3
Size: 556 Color: 2
Size: 540 Color: 1
Size: 512 Color: 0
Size: 508 Color: 0
Size: 504 Color: 3
Size: 496 Color: 1
Size: 484 Color: 4
Size: 468 Color: 2
Size: 464 Color: 4
Size: 464 Color: 2
Size: 464 Color: 2
Size: 464 Color: 0
Size: 458 Color: 0
Size: 448 Color: 3
Size: 442 Color: 3
Size: 432 Color: 2
Size: 426 Color: 4
Size: 416 Color: 3
Size: 404 Color: 1
Size: 402 Color: 2
Size: 384 Color: 3
Size: 384 Color: 2
Size: 382 Color: 4
Size: 370 Color: 2
Size: 368 Color: 4
Size: 360 Color: 1
Size: 356 Color: 0
Size: 352 Color: 4
Size: 344 Color: 0
Size: 340 Color: 0

Bin 198: 222 of cap free
Amount of items: 2
Items: 
Size: 10808 Color: 1
Size: 4602 Color: 4

Bin 199: 11488 of cap free
Amount of items: 14
Items: 
Size: 352 Color: 1
Size: 348 Color: 4
Size: 348 Color: 3
Size: 340 Color: 1
Size: 316 Color: 4
Size: 288 Color: 3
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 272 Color: 1
Size: 264 Color: 3
Size: 256 Color: 3
Size: 256 Color: 2
Size: 240 Color: 2

Total size: 3095136
Total free space: 15632

