Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 348 Color: 1
Size: 254 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 310 Color: 0
Size: 302 Color: 1
Size: 389 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 356 Color: 1
Size: 264 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 284 Color: 1
Size: 266 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 356 Color: 1
Size: 279 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 351 Color: 1
Size: 294 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 280 Color: 1
Size: 253 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 350 Color: 1
Size: 277 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 315 Color: 1
Size: 264 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 356 Color: 1
Size: 273 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 289 Color: 1
Size: 284 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 330 Color: 1
Size: 256 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 321 Color: 1
Size: 308 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 0
Size: 250 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 294 Color: 1
Size: 252 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 285 Color: 1
Size: 277 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 321 Color: 1
Size: 315 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 273 Color: 1
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 320 Color: 1
Size: 281 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 303 Color: 1
Size: 282 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 323 Color: 1
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 319 Color: 1
Size: 251 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 277 Color: 1
Size: 253 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 306 Color: 1
Size: 265 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 345 Color: 1
Size: 255 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 337 Color: 1
Size: 278 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 321 Color: 1
Size: 273 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 276 Color: 1
Size: 256 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 267 Color: 1
Size: 265 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 327 Color: 1
Size: 290 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 325 Color: 1
Size: 291 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 329 Color: 1
Size: 259 Color: 0

Total size: 34034
Total free space: 0

