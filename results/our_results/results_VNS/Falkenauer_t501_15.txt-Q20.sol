Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 9
Size: 297 Color: 11
Size: 266 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 324 Color: 16
Size: 271 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 11
Size: 357 Color: 18
Size: 254 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 7
Size: 321 Color: 6
Size: 252 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 14
Size: 302 Color: 17
Size: 258 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 265 Color: 4
Size: 263 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 342 Color: 12
Size: 273 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 359 Color: 19
Size: 271 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 368 Color: 13
Size: 263 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 6
Size: 254 Color: 19
Size: 250 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 14
Size: 323 Color: 10
Size: 270 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 289 Color: 11
Size: 260 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 265 Color: 16
Size: 252 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 8
Size: 270 Color: 3
Size: 250 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 19
Size: 332 Color: 5
Size: 282 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 15
Size: 346 Color: 11
Size: 272 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 15
Size: 269 Color: 7
Size: 250 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 14
Size: 356 Color: 4
Size: 275 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 18
Size: 272 Color: 5
Size: 262 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 19
Size: 332 Color: 1
Size: 271 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 17
Size: 279 Color: 16
Size: 276 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 15
Size: 296 Color: 8
Size: 273 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 10
Size: 291 Color: 11
Size: 261 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 10
Size: 323 Color: 3
Size: 252 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 13
Size: 293 Color: 6
Size: 269 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7
Size: 309 Color: 5
Size: 296 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 310 Color: 11
Size: 286 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 14
Size: 309 Color: 4
Size: 251 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 16
Size: 289 Color: 4
Size: 257 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 6
Size: 269 Color: 0
Size: 255 Color: 15

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 294 Color: 0
Size: 258 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 275 Color: 3
Size: 252 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 15
Size: 349 Color: 17
Size: 291 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 5
Size: 251 Color: 11
Size: 250 Color: 5

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 256 Color: 0
Size: 250 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 16
Size: 259 Color: 15
Size: 254 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 17
Size: 310 Color: 18
Size: 301 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 348 Color: 3
Size: 258 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 340 Color: 8
Size: 290 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 267 Color: 17
Size: 267 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 14
Size: 309 Color: 6
Size: 278 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 10
Size: 294 Color: 11
Size: 253 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 16
Size: 329 Color: 11
Size: 295 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 16
Size: 289 Color: 18
Size: 289 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 357 Color: 5
Size: 256 Color: 7

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 8
Size: 278 Color: 5
Size: 259 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 9
Size: 357 Color: 18
Size: 274 Color: 8

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 6
Size: 356 Color: 6
Size: 275 Color: 11

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 364 Color: 1
Size: 263 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 9
Size: 348 Color: 11
Size: 259 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 292 Color: 10
Size: 286 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 7
Size: 281 Color: 3
Size: 270 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 9
Size: 336 Color: 8
Size: 264 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 276 Color: 6
Size: 250 Color: 19

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 14
Size: 280 Color: 2
Size: 259 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 18
Size: 277 Color: 3
Size: 259 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 12
Size: 288 Color: 10
Size: 257 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 19
Size: 289 Color: 0
Size: 251 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 290 Color: 4
Size: 270 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 7
Size: 298 Color: 3
Size: 278 Color: 17

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 19
Size: 270 Color: 18
Size: 252 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 313 Color: 16
Size: 308 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 17
Size: 268 Color: 13
Size: 257 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 15
Size: 275 Color: 6
Size: 274 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7
Size: 329 Color: 5
Size: 279 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 18
Size: 325 Color: 0
Size: 276 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 18
Size: 289 Color: 5
Size: 267 Color: 19

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 5
Size: 358 Color: 2
Size: 280 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 5
Size: 303 Color: 15
Size: 257 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 298 Color: 15
Size: 265 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 16
Size: 320 Color: 16
Size: 272 Color: 9

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 331 Color: 2
Size: 284 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 291 Color: 17
Size: 253 Color: 16

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 15
Size: 255 Color: 9
Size: 253 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 10
Size: 323 Color: 1
Size: 290 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 16
Size: 346 Color: 8
Size: 290 Color: 8

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 367 Color: 18
Size: 250 Color: 5

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 305 Color: 19
Size: 300 Color: 15

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 14
Size: 265 Color: 7
Size: 255 Color: 5

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9
Size: 274 Color: 14
Size: 258 Color: 18

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 340 Color: 1
Size: 279 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 6
Size: 300 Color: 7
Size: 282 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 19
Size: 267 Color: 11
Size: 255 Color: 13

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 253 Color: 0
Size: 251 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 368 Color: 19
Size: 251 Color: 3

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 1
Size: 326 Color: 16
Size: 325 Color: 15

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 11
Size: 316 Color: 2
Size: 287 Color: 12

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 255 Color: 19
Size: 254 Color: 6

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 6
Size: 309 Color: 14
Size: 295 Color: 13

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 318 Color: 16
Size: 318 Color: 12

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 303 Color: 18
Size: 290 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 7
Size: 277 Color: 4
Size: 261 Color: 9

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 17
Size: 268 Color: 8
Size: 266 Color: 6

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 13
Size: 337 Color: 6
Size: 271 Color: 13

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 293 Color: 17
Size: 250 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 19
Size: 309 Color: 10
Size: 264 Color: 14

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 287 Color: 6
Size: 271 Color: 17

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 8
Size: 315 Color: 11
Size: 294 Color: 17

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 357 Color: 0
Size: 263 Color: 19

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 13
Size: 312 Color: 7
Size: 278 Color: 17

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 372 Color: 19
Size: 252 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 274 Color: 10
Size: 261 Color: 2

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 365 Color: 13
Size: 263 Color: 14

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 327 Color: 6
Size: 286 Color: 19

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 355 Color: 17
Size: 271 Color: 19

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 11
Size: 330 Color: 14
Size: 306 Color: 17

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 11
Size: 296 Color: 13
Size: 257 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 18
Size: 252 Color: 18
Size: 250 Color: 17

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 8
Size: 285 Color: 19
Size: 259 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 278 Color: 16
Size: 251 Color: 15

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 329 Color: 15
Size: 275 Color: 8

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 273 Color: 3
Size: 269 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 318 Color: 16
Size: 279 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 13
Size: 330 Color: 2
Size: 292 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7
Size: 350 Color: 9
Size: 257 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 13
Size: 269 Color: 19
Size: 256 Color: 14

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 339 Color: 14
Size: 296 Color: 15

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 7
Size: 279 Color: 11
Size: 269 Color: 8

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 344 Color: 3
Size: 264 Color: 6

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6
Size: 358 Color: 8
Size: 279 Color: 10

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 0
Size: 330 Color: 12
Size: 294 Color: 11

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 12
Size: 361 Color: 5
Size: 268 Color: 18

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 16
Size: 273 Color: 1
Size: 252 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 14
Size: 276 Color: 12
Size: 253 Color: 19

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 7
Size: 283 Color: 2
Size: 263 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 17
Size: 347 Color: 2
Size: 306 Color: 11

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 324 Color: 0
Size: 288 Color: 11

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 6
Size: 251 Color: 0
Size: 250 Color: 5

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 335 Color: 19
Size: 301 Color: 9

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 14
Size: 351 Color: 5
Size: 275 Color: 13

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7
Size: 351 Color: 17
Size: 271 Color: 14

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 314 Color: 2
Size: 280 Color: 17

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 337 Color: 19
Size: 287 Color: 15

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 274 Color: 0
Size: 271 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 286 Color: 10
Size: 253 Color: 8

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 322 Color: 11
Size: 252 Color: 9

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 12
Size: 312 Color: 15
Size: 256 Color: 6

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 12
Size: 343 Color: 6
Size: 250 Color: 11

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 315 Color: 12
Size: 251 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 296 Color: 0
Size: 286 Color: 15

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 15
Size: 284 Color: 2
Size: 278 Color: 16

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 12
Size: 269 Color: 1
Size: 263 Color: 13

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 3
Size: 268 Color: 15
Size: 260 Color: 2

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 10
Size: 319 Color: 0
Size: 271 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 11
Size: 287 Color: 15
Size: 272 Color: 19

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 17
Size: 318 Color: 8
Size: 294 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 358 Color: 3
Size: 257 Color: 12

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 269 Color: 9
Size: 258 Color: 17

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 16
Size: 283 Color: 6
Size: 250 Color: 19

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 14
Size: 286 Color: 5
Size: 255 Color: 18

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 319 Color: 12
Size: 254 Color: 10

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 325 Color: 12
Size: 287 Color: 5

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 15
Size: 340 Color: 4
Size: 309 Color: 14

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 307 Color: 12
Size: 261 Color: 11

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 10
Size: 349 Color: 18
Size: 272 Color: 16

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 325 Color: 13
Size: 298 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 287 Color: 4
Size: 254 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 15
Size: 268 Color: 1
Size: 252 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 302 Color: 11
Size: 291 Color: 8

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 19
Size: 372 Color: 8
Size: 252 Color: 13

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 14
Size: 262 Color: 0
Size: 257 Color: 6

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 347 Color: 10
Size: 278 Color: 13

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 10
Size: 369 Color: 0
Size: 253 Color: 13

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 293 Color: 7
Size: 263 Color: 19

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 17
Size: 321 Color: 12
Size: 254 Color: 14

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 15
Size: 266 Color: 13
Size: 257 Color: 13

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 256 Color: 3
Size: 252 Color: 11

Total size: 167000
Total free space: 0

