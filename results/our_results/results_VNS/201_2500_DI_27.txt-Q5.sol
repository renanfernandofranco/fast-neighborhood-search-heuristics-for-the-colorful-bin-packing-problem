Capicity Bin: 1940
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1058 Color: 3
Size: 806 Color: 2
Size: 76 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 1216 Color: 2
Size: 724 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 2
Size: 466 Color: 1
Size: 60 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 2
Size: 333 Color: 1
Size: 104 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 0
Size: 327 Color: 2
Size: 72 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 4
Size: 346 Color: 1
Size: 52 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 233 Color: 3
Size: 136 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 2
Size: 241 Color: 1
Size: 60 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1648 Color: 4
Size: 258 Color: 1
Size: 34 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 2
Size: 227 Color: 4
Size: 52 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1677 Color: 1
Size: 221 Color: 4
Size: 42 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 0
Size: 191 Color: 3
Size: 56 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 1
Size: 177 Color: 2
Size: 42 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 3
Size: 181 Color: 2
Size: 36 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 3
Size: 171 Color: 4
Size: 40 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 2
Size: 170 Color: 2
Size: 28 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 3
Size: 122 Color: 2
Size: 80 Color: 4

Bin 18: 1 of cap free
Amount of items: 6
Items: 
Size: 971 Color: 3
Size: 402 Color: 2
Size: 216 Color: 4
Size: 216 Color: 2
Size: 90 Color: 1
Size: 44 Color: 0

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1293 Color: 3
Size: 646 Color: 4

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 1
Size: 521 Color: 1
Size: 64 Color: 4

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 1
Size: 448 Color: 0
Size: 40 Color: 2

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 309 Color: 1
Size: 60 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 3
Size: 270 Color: 2
Size: 74 Color: 1

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1653 Color: 3
Size: 286 Color: 2

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1726 Color: 2
Size: 213 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 0
Size: 683 Color: 4
Size: 56 Color: 3

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 2
Size: 656 Color: 0
Size: 64 Color: 3

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 0
Size: 569 Color: 1
Size: 52 Color: 4

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 4
Size: 541 Color: 0
Size: 96 Color: 4

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 0
Size: 447 Color: 2
Size: 166 Color: 4

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1340 Color: 2
Size: 550 Color: 1
Size: 48 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 4
Size: 404 Color: 3
Size: 72 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 0
Size: 365 Color: 3
Size: 40 Color: 4

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 4
Size: 245 Color: 1
Size: 24 Color: 3

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 2
Size: 207 Color: 4
Size: 46 Color: 1

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1098 Color: 2
Size: 783 Color: 3
Size: 56 Color: 0

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 4
Size: 409 Color: 3
Size: 106 Color: 0

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 3
Size: 403 Color: 2
Size: 32 Color: 2

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 3
Size: 303 Color: 2
Size: 16 Color: 2

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 3
Size: 251 Color: 0
Size: 8 Color: 2

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1715 Color: 2
Size: 222 Color: 3

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1095 Color: 0
Size: 809 Color: 1
Size: 32 Color: 3

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 4
Size: 611 Color: 3
Size: 66 Color: 0

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 2
Size: 300 Color: 1
Size: 34 Color: 4

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 3
Size: 191 Color: 4
Size: 8 Color: 4

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 4
Size: 705 Color: 0
Size: 64 Color: 3

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 4
Size: 626 Color: 2
Size: 102 Color: 3

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1397 Color: 1
Size: 490 Color: 3
Size: 48 Color: 0

Bin 49: 5 of cap free
Amount of items: 2
Items: 
Size: 1601 Color: 0
Size: 334 Color: 2

Bin 50: 6 of cap free
Amount of items: 3
Items: 
Size: 974 Color: 2
Size: 838 Color: 3
Size: 122 Color: 0

Bin 51: 6 of cap free
Amount of items: 3
Items: 
Size: 1215 Color: 1
Size: 619 Color: 2
Size: 100 Color: 0

Bin 52: 6 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 3
Size: 244 Color: 4

Bin 53: 8 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 4
Size: 371 Color: 0
Size: 66 Color: 4

Bin 54: 10 of cap free
Amount of items: 2
Items: 
Size: 1647 Color: 4
Size: 283 Color: 2

Bin 55: 12 of cap free
Amount of items: 3
Items: 
Size: 973 Color: 1
Size: 513 Color: 0
Size: 442 Color: 1

Bin 56: 12 of cap free
Amount of items: 2
Items: 
Size: 1121 Color: 1
Size: 807 Color: 2

Bin 57: 13 of cap free
Amount of items: 2
Items: 
Size: 1282 Color: 4
Size: 645 Color: 1

Bin 58: 14 of cap free
Amount of items: 16
Items: 
Size: 210 Color: 0
Size: 206 Color: 0
Size: 160 Color: 4
Size: 160 Color: 1
Size: 160 Color: 1
Size: 140 Color: 3
Size: 128 Color: 3
Size: 112 Color: 0
Size: 108 Color: 1
Size: 102 Color: 2
Size: 96 Color: 1
Size: 84 Color: 4
Size: 80 Color: 3
Size: 80 Color: 2
Size: 52 Color: 4
Size: 48 Color: 3

Bin 59: 17 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 1
Size: 738 Color: 2
Size: 88 Color: 0

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1634 Color: 1
Size: 289 Color: 4

Bin 61: 20 of cap free
Amount of items: 2
Items: 
Size: 1333 Color: 4
Size: 587 Color: 1

Bin 62: 22 of cap free
Amount of items: 2
Items: 
Size: 1577 Color: 3
Size: 341 Color: 2

Bin 63: 25 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 1
Size: 366 Color: 2

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 1
Size: 507 Color: 2

Bin 65: 30 of cap free
Amount of items: 2
Items: 
Size: 1457 Color: 1
Size: 453 Color: 4

Bin 66: 1612 of cap free
Amount of items: 7
Items: 
Size: 72 Color: 0
Size: 50 Color: 1
Size: 46 Color: 0
Size: 44 Color: 3
Size: 44 Color: 2
Size: 40 Color: 3
Size: 32 Color: 4

Total size: 126100
Total free space: 1940

