Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5100 Color: 0
Size: 2664 Color: 2
Size: 316 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 2
Size: 2450 Color: 0
Size: 476 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5161 Color: 2
Size: 2433 Color: 4
Size: 486 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5186 Color: 0
Size: 2414 Color: 2
Size: 480 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5483 Color: 4
Size: 2387 Color: 2
Size: 210 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 0
Size: 2108 Color: 2
Size: 136 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5959 Color: 0
Size: 1769 Color: 1
Size: 352 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 3
Size: 1800 Color: 3
Size: 224 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6092 Color: 3
Size: 1660 Color: 4
Size: 328 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 2
Size: 1812 Color: 2
Size: 144 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 2
Size: 1604 Color: 3
Size: 168 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 4
Size: 1365 Color: 2
Size: 304 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 0
Size: 1484 Color: 1
Size: 172 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 3
Size: 1386 Color: 4
Size: 240 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 1
Size: 1300 Color: 3
Size: 352 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 4
Size: 1436 Color: 1
Size: 174 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 3
Size: 1404 Color: 2
Size: 152 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 2
Size: 881 Color: 4
Size: 664 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 3
Size: 1142 Color: 0
Size: 272 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 0
Size: 1130 Color: 2
Size: 272 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6696 Color: 0
Size: 1192 Color: 3
Size: 192 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 2
Size: 886 Color: 2
Size: 484 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 3
Size: 896 Color: 1
Size: 412 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 3
Size: 1065 Color: 2
Size: 212 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 2
Size: 839 Color: 1
Size: 416 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 3
Size: 888 Color: 1
Size: 368 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6894 Color: 0
Size: 702 Color: 3
Size: 484 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 728 Color: 3
Size: 400 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 3
Size: 770 Color: 0
Size: 306 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 1
Size: 980 Color: 3
Size: 88 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7023 Color: 2
Size: 881 Color: 2
Size: 176 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7088 Color: 3
Size: 820 Color: 4
Size: 172 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 3
Size: 840 Color: 0
Size: 132 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7158 Color: 3
Size: 634 Color: 1
Size: 288 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 3
Size: 572 Color: 2
Size: 320 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 3
Size: 604 Color: 1
Size: 266 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 3
Size: 544 Color: 4
Size: 282 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7272 Color: 3
Size: 624 Color: 2
Size: 184 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 1
Size: 656 Color: 4
Size: 186 Color: 3

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 2
Size: 3771 Color: 3
Size: 208 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5057 Color: 3
Size: 2792 Color: 2
Size: 230 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5113 Color: 3
Size: 2408 Color: 4
Size: 558 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5735 Color: 1
Size: 2200 Color: 2
Size: 144 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 4
Size: 1727 Color: 0
Size: 196 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6218 Color: 3
Size: 1733 Color: 1
Size: 128 Color: 2

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 0
Size: 1307 Color: 2
Size: 408 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 3
Size: 1436 Color: 1
Size: 264 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 3
Size: 1248 Color: 2
Size: 388 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6583 Color: 3
Size: 1272 Color: 0
Size: 224 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 3
Size: 1147 Color: 0
Size: 264 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 2
Size: 990 Color: 3
Size: 384 Color: 1

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 0
Size: 867 Color: 4
Size: 424 Color: 3

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6878 Color: 3
Size: 1201 Color: 0

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 1
Size: 862 Color: 3
Size: 176 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 4
Size: 3362 Color: 4
Size: 664 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 3
Size: 3366 Color: 3
Size: 656 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5934 Color: 4
Size: 1856 Color: 2
Size: 288 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6239 Color: 2
Size: 1535 Color: 0
Size: 304 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6910 Color: 0
Size: 900 Color: 1
Size: 268 Color: 3

Bin 60: 3 of cap free
Amount of items: 7
Items: 
Size: 4041 Color: 3
Size: 878 Color: 1
Size: 850 Color: 2
Size: 748 Color: 3
Size: 672 Color: 4
Size: 672 Color: 0
Size: 216 Color: 0

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 1
Size: 2801 Color: 3
Size: 152 Color: 2

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 5556 Color: 0
Size: 2521 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5928 Color: 3
Size: 1941 Color: 1
Size: 208 Color: 2

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6365 Color: 1
Size: 1432 Color: 3
Size: 280 Color: 0

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6681 Color: 4
Size: 1396 Color: 1

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6747 Color: 4
Size: 1330 Color: 2

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6965 Color: 0
Size: 1112 Color: 4

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 7016 Color: 3
Size: 1061 Color: 1

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 1
Size: 931 Color: 0
Size: 128 Color: 2

Bin 70: 4 of cap free
Amount of items: 3
Items: 
Size: 4676 Color: 2
Size: 2736 Color: 3
Size: 664 Color: 2

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 4
Size: 3144 Color: 2
Size: 188 Color: 4

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 3
Size: 2444 Color: 3
Size: 560 Color: 0

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 2792 Color: 0
Size: 128 Color: 2

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 5170 Color: 3
Size: 2426 Color: 4
Size: 480 Color: 2

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 3
Size: 2468 Color: 0
Size: 160 Color: 4

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 1
Size: 1680 Color: 0

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 4
Size: 1379 Color: 0

Bin 78: 5 of cap free
Amount of items: 5
Items: 
Size: 4043 Color: 2
Size: 1342 Color: 3
Size: 1275 Color: 0
Size: 1249 Color: 3
Size: 166 Color: 3

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 2
Size: 949 Color: 0
Size: 80 Color: 0

Bin 80: 5 of cap free
Amount of items: 4
Items: 
Size: 7075 Color: 2
Size: 952 Color: 1
Size: 32 Color: 0
Size: 16 Color: 0

Bin 81: 6 of cap free
Amount of items: 4
Items: 
Size: 5594 Color: 2
Size: 1419 Color: 3
Size: 933 Color: 1
Size: 128 Color: 0

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 7080 Color: 0
Size: 978 Color: 1
Size: 16 Color: 4

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 2
Size: 2165 Color: 0
Size: 140 Color: 3

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 2
Size: 1043 Color: 0

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 2
Size: 1521 Color: 0

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 7025 Color: 3
Size: 1047 Color: 4

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 7224 Color: 2
Size: 848 Color: 0

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 5217 Color: 4
Size: 2854 Color: 2

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 5600 Color: 0
Size: 1591 Color: 3
Size: 880 Color: 2

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 4721 Color: 3
Size: 3348 Color: 0

Bin 91: 11 of cap free
Amount of items: 3
Items: 
Size: 5751 Color: 0
Size: 1928 Color: 1
Size: 390 Color: 3

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 6909 Color: 4
Size: 1160 Color: 2

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 7062 Color: 2
Size: 1006 Color: 4

Bin 94: 13 of cap free
Amount of items: 3
Items: 
Size: 4267 Color: 1
Size: 2442 Color: 3
Size: 1358 Color: 2

Bin 95: 13 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 1
Size: 3332 Color: 2
Size: 280 Color: 3

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 4642 Color: 3
Size: 3021 Color: 4
Size: 404 Color: 1

Bin 97: 13 of cap free
Amount of items: 3
Items: 
Size: 5624 Color: 2
Size: 1955 Color: 0
Size: 488 Color: 3

Bin 98: 14 of cap free
Amount of items: 4
Items: 
Size: 4046 Color: 2
Size: 3364 Color: 0
Size: 528 Color: 1
Size: 128 Color: 0

Bin 99: 14 of cap free
Amount of items: 3
Items: 
Size: 4658 Color: 0
Size: 3264 Color: 0
Size: 144 Color: 1

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 4
Size: 2056 Color: 1

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 6376 Color: 1
Size: 1688 Color: 4

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 6001 Color: 1
Size: 2062 Color: 3

Bin 103: 17 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 1
Size: 1431 Color: 2
Size: 64 Color: 2

Bin 104: 18 of cap free
Amount of items: 3
Items: 
Size: 4888 Color: 4
Size: 2866 Color: 2
Size: 308 Color: 4

Bin 105: 19 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 3
Size: 2484 Color: 0

Bin 106: 20 of cap free
Amount of items: 8
Items: 
Size: 4042 Color: 0
Size: 726 Color: 3
Size: 690 Color: 4
Size: 680 Color: 2
Size: 672 Color: 2
Size: 502 Color: 3
Size: 494 Color: 1
Size: 254 Color: 0

Bin 107: 20 of cap free
Amount of items: 2
Items: 
Size: 5192 Color: 1
Size: 2868 Color: 3

Bin 108: 21 of cap free
Amount of items: 3
Items: 
Size: 6943 Color: 2
Size: 1084 Color: 4
Size: 32 Color: 0

Bin 109: 25 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 2
Size: 1182 Color: 0
Size: 64 Color: 2

Bin 110: 27 of cap free
Amount of items: 2
Items: 
Size: 6961 Color: 2
Size: 1092 Color: 1

Bin 111: 28 of cap free
Amount of items: 3
Items: 
Size: 4312 Color: 3
Size: 3324 Color: 4
Size: 416 Color: 2

Bin 112: 30 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 4
Size: 1324 Color: 0

Bin 113: 32 of cap free
Amount of items: 24
Items: 
Size: 672 Color: 2
Size: 496 Color: 2
Size: 488 Color: 4
Size: 480 Color: 1
Size: 432 Color: 4
Size: 432 Color: 1
Size: 368 Color: 2
Size: 356 Color: 1
Size: 352 Color: 4
Size: 346 Color: 0
Size: 336 Color: 4
Size: 304 Color: 1
Size: 304 Color: 0
Size: 284 Color: 2
Size: 278 Color: 2
Size: 274 Color: 4
Size: 268 Color: 3
Size: 256 Color: 2
Size: 256 Color: 1
Size: 248 Color: 2
Size: 238 Color: 0
Size: 208 Color: 1
Size: 196 Color: 0
Size: 176 Color: 0

Bin 114: 34 of cap free
Amount of items: 2
Items: 
Size: 6492 Color: 0
Size: 1554 Color: 1

Bin 115: 35 of cap free
Amount of items: 2
Items: 
Size: 6255 Color: 3
Size: 1790 Color: 1

Bin 116: 36 of cap free
Amount of items: 2
Items: 
Size: 6168 Color: 0
Size: 1876 Color: 3

Bin 117: 39 of cap free
Amount of items: 2
Items: 
Size: 6840 Color: 0
Size: 1201 Color: 2

Bin 118: 43 of cap free
Amount of items: 2
Items: 
Size: 5950 Color: 1
Size: 2087 Color: 0

Bin 119: 45 of cap free
Amount of items: 2
Items: 
Size: 6427 Color: 0
Size: 1608 Color: 4

Bin 120: 52 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 2
Size: 2074 Color: 1
Size: 344 Color: 3

Bin 121: 52 of cap free
Amount of items: 2
Items: 
Size: 6486 Color: 4
Size: 1542 Color: 1

Bin 122: 55 of cap free
Amount of items: 2
Items: 
Size: 6173 Color: 2
Size: 1852 Color: 0

Bin 123: 60 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 2
Size: 3368 Color: 1
Size: 568 Color: 4

Bin 124: 68 of cap free
Amount of items: 2
Items: 
Size: 4833 Color: 4
Size: 3179 Color: 3

Bin 125: 68 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 3
Size: 1778 Color: 0

Bin 126: 69 of cap free
Amount of items: 2
Items: 
Size: 4644 Color: 4
Size: 3367 Color: 0

Bin 127: 77 of cap free
Amount of items: 2
Items: 
Size: 5160 Color: 3
Size: 2843 Color: 4

Bin 128: 80 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 1
Size: 3364 Color: 0
Size: 568 Color: 2

Bin 129: 83 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 1
Size: 2473 Color: 3

Bin 130: 88 of cap free
Amount of items: 2
Items: 
Size: 5860 Color: 0
Size: 2132 Color: 4

Bin 131: 92 of cap free
Amount of items: 4
Items: 
Size: 4044 Color: 1
Size: 1400 Color: 3
Size: 1391 Color: 1
Size: 1153 Color: 2

Bin 132: 94 of cap free
Amount of items: 2
Items: 
Size: 5142 Color: 0
Size: 2844 Color: 3

Bin 133: 6310 of cap free
Amount of items: 9
Items: 
Size: 232 Color: 4
Size: 228 Color: 4
Size: 228 Color: 2
Size: 216 Color: 3
Size: 198 Color: 1
Size: 176 Color: 3
Size: 176 Color: 1
Size: 172 Color: 0
Size: 144 Color: 0

Total size: 1066560
Total free space: 8080

