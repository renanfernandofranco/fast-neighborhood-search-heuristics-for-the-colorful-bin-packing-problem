Capicity Bin: 19712
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 9861 Color: 4
Size: 2441 Color: 2
Size: 2188 Color: 1
Size: 2084 Color: 3
Size: 1892 Color: 3
Size: 692 Color: 1
Size: 554 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 3
Size: 8640 Color: 3
Size: 368 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 4
Size: 7284 Color: 1
Size: 1216 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 2
Size: 7076 Color: 4
Size: 1408 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13072 Color: 4
Size: 6168 Color: 3
Size: 472 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13179 Color: 0
Size: 5445 Color: 1
Size: 1088 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13872 Color: 3
Size: 5384 Color: 1
Size: 456 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 3
Size: 4412 Color: 0
Size: 328 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15280 Color: 4
Size: 4080 Color: 1
Size: 352 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15614 Color: 1
Size: 3418 Color: 3
Size: 680 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15688 Color: 2
Size: 3144 Color: 1
Size: 880 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15752 Color: 0
Size: 3470 Color: 1
Size: 490 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15845 Color: 1
Size: 3165 Color: 2
Size: 702 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15875 Color: 4
Size: 2781 Color: 0
Size: 1056 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 16008 Color: 1
Size: 2288 Color: 2
Size: 1416 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16030 Color: 3
Size: 2004 Color: 3
Size: 1678 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16144 Color: 0
Size: 3400 Color: 3
Size: 168 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16192 Color: 4
Size: 3368 Color: 0
Size: 152 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16328 Color: 1
Size: 3058 Color: 0
Size: 326 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16466 Color: 3
Size: 2802 Color: 2
Size: 444 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16496 Color: 4
Size: 2824 Color: 3
Size: 392 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16548 Color: 4
Size: 2644 Color: 1
Size: 520 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16600 Color: 1
Size: 2776 Color: 0
Size: 336 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16591 Color: 0
Size: 2457 Color: 1
Size: 664 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16731 Color: 1
Size: 1891 Color: 3
Size: 1090 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16764 Color: 3
Size: 1808 Color: 1
Size: 1140 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 3
Size: 1640 Color: 1
Size: 1296 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16784 Color: 0
Size: 2432 Color: 4
Size: 496 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16829 Color: 2
Size: 2403 Color: 3
Size: 480 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16966 Color: 0
Size: 2226 Color: 1
Size: 520 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 17092 Color: 1
Size: 2000 Color: 3
Size: 620 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 17240 Color: 1
Size: 1832 Color: 3
Size: 640 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 17246 Color: 1
Size: 2058 Color: 3
Size: 408 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 17222 Color: 3
Size: 1642 Color: 2
Size: 848 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 17329 Color: 3
Size: 1935 Color: 2
Size: 448 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 17368 Color: 3
Size: 2168 Color: 0
Size: 176 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17313 Color: 4
Size: 2041 Color: 3
Size: 358 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17444 Color: 3
Size: 1308 Color: 2
Size: 960 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17350 Color: 2
Size: 2308 Color: 0
Size: 54 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17488 Color: 3
Size: 1872 Color: 2
Size: 352 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17494 Color: 3
Size: 1440 Color: 2
Size: 778 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17505 Color: 3
Size: 1795 Color: 4
Size: 412 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17528 Color: 4
Size: 1472 Color: 3
Size: 712 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17598 Color: 4
Size: 1762 Color: 2
Size: 352 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 4
Size: 1224 Color: 3
Size: 872 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17682 Color: 3
Size: 1672 Color: 4
Size: 358 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17656 Color: 1
Size: 1424 Color: 0
Size: 632 Color: 3

Bin 48: 1 of cap free
Amount of items: 7
Items: 
Size: 9860 Color: 1
Size: 1880 Color: 4
Size: 1775 Color: 1
Size: 1748 Color: 2
Size: 1720 Color: 1
Size: 1640 Color: 2
Size: 1088 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 11119 Color: 2
Size: 7984 Color: 3
Size: 608 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 0
Size: 6285 Color: 3
Size: 1098 Color: 1

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 12854 Color: 2
Size: 6223 Color: 1
Size: 634 Color: 2

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 13887 Color: 1
Size: 5336 Color: 3
Size: 488 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 14477 Color: 0
Size: 4450 Color: 2
Size: 784 Color: 1

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 1
Size: 4335 Color: 2
Size: 696 Color: 3

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 14890 Color: 0
Size: 4301 Color: 4
Size: 520 Color: 1

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 15851 Color: 2
Size: 3604 Color: 1
Size: 256 Color: 4

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 16292 Color: 0
Size: 3195 Color: 1
Size: 224 Color: 0

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 16350 Color: 1
Size: 1716 Color: 4
Size: 1645 Color: 4

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 16417 Color: 1
Size: 3070 Color: 0
Size: 224 Color: 4

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 16793 Color: 4
Size: 2406 Color: 1
Size: 512 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 17027 Color: 1
Size: 2072 Color: 3
Size: 612 Color: 4

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 17106 Color: 1
Size: 2093 Color: 3
Size: 512 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 17109 Color: 1
Size: 1632 Color: 2
Size: 970 Color: 3

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 17067 Color: 3
Size: 1776 Color: 1
Size: 868 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 17265 Color: 0
Size: 2008 Color: 4
Size: 438 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 17277 Color: 4
Size: 1844 Color: 1
Size: 590 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 17445 Color: 3
Size: 1850 Color: 4
Size: 416 Color: 2

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 17559 Color: 0
Size: 1694 Color: 3
Size: 458 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 17611 Color: 0
Size: 1768 Color: 3
Size: 332 Color: 2

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 17739 Color: 0
Size: 1004 Color: 3
Size: 968 Color: 1

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11056 Color: 1
Size: 7550 Color: 2
Size: 1104 Color: 4

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 2
Size: 8213 Color: 0
Size: 336 Color: 1

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 12205 Color: 1
Size: 7161 Color: 3
Size: 344 Color: 1

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13164 Color: 0
Size: 6546 Color: 2

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 4
Size: 5444 Color: 4
Size: 584 Color: 1

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 14386 Color: 1
Size: 4464 Color: 4
Size: 860 Color: 4

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 0
Size: 4362 Color: 1
Size: 866 Color: 3

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 15086 Color: 0
Size: 4248 Color: 1
Size: 376 Color: 0

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 15378 Color: 4
Size: 4332 Color: 3

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 15792 Color: 2
Size: 3438 Color: 1
Size: 480 Color: 2

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 15879 Color: 4
Size: 3223 Color: 1
Size: 608 Color: 3

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 15915 Color: 0
Size: 3179 Color: 4
Size: 616 Color: 1

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 15988 Color: 2
Size: 3478 Color: 3
Size: 244 Color: 1

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 16963 Color: 3
Size: 2747 Color: 4

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 17176 Color: 4
Size: 2174 Color: 4
Size: 360 Color: 1

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 17478 Color: 3
Size: 1344 Color: 1
Size: 888 Color: 2

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 17702 Color: 2
Size: 1632 Color: 3
Size: 376 Color: 0

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 1
Size: 7096 Color: 4
Size: 368 Color: 0

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 0
Size: 4855 Color: 3
Size: 480 Color: 1

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 0
Size: 3901 Color: 1
Size: 568 Color: 4

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 15396 Color: 0
Size: 3857 Color: 1
Size: 456 Color: 4

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 15606 Color: 1
Size: 3219 Color: 3
Size: 884 Color: 0

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 16377 Color: 0
Size: 3332 Color: 4

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 16765 Color: 0
Size: 2944 Color: 4

Bin 95: 3 of cap free
Amount of items: 2
Items: 
Size: 17249 Color: 4
Size: 2460 Color: 2

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 0
Size: 7080 Color: 1
Size: 1432 Color: 4

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 12048 Color: 3
Size: 7084 Color: 4
Size: 576 Color: 3

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 6720 Color: 3
Size: 644 Color: 0

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 12990 Color: 3
Size: 6158 Color: 1
Size: 560 Color: 4

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 13115 Color: 0
Size: 6257 Color: 1
Size: 336 Color: 0

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 3
Size: 4880 Color: 2
Size: 408 Color: 2

Bin 102: 4 of cap free
Amount of items: 4
Items: 
Size: 17552 Color: 4
Size: 1960 Color: 2
Size: 132 Color: 0
Size: 64 Color: 3

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 17655 Color: 1
Size: 2053 Color: 2

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 11922 Color: 0
Size: 7161 Color: 4
Size: 624 Color: 2

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 4
Size: 6224 Color: 0
Size: 320 Color: 1

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 1
Size: 5459 Color: 2
Size: 928 Color: 3

Bin 107: 5 of cap free
Amount of items: 3
Items: 
Size: 14960 Color: 3
Size: 4363 Color: 4
Size: 384 Color: 3

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 16786 Color: 4
Size: 2921 Color: 0

Bin 109: 6 of cap free
Amount of items: 18
Items: 
Size: 1416 Color: 3
Size: 1408 Color: 0
Size: 1250 Color: 0
Size: 1248 Color: 3
Size: 1244 Color: 4
Size: 1216 Color: 1
Size: 1216 Color: 0
Size: 1184 Color: 3
Size: 1120 Color: 4
Size: 1120 Color: 2
Size: 1100 Color: 4
Size: 1088 Color: 2
Size: 1072 Color: 0
Size: 1056 Color: 0
Size: 1000 Color: 4
Size: 864 Color: 4
Size: 736 Color: 1
Size: 368 Color: 4

Bin 110: 6 of cap free
Amount of items: 8
Items: 
Size: 9858 Color: 3
Size: 1768 Color: 3
Size: 1640 Color: 2
Size: 1632 Color: 2
Size: 1632 Color: 2
Size: 1408 Color: 4
Size: 1248 Color: 4
Size: 520 Color: 1

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 4
Size: 8212 Color: 1
Size: 832 Color: 4

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 10702 Color: 3
Size: 8204 Color: 2
Size: 800 Color: 1

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 16468 Color: 4
Size: 3238 Color: 2

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 17273 Color: 2
Size: 2433 Color: 4

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 15529 Color: 1
Size: 3280 Color: 0
Size: 896 Color: 4

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 15542 Color: 1
Size: 2448 Color: 4
Size: 1715 Color: 3

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 17201 Color: 3
Size: 2504 Color: 4

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 10654 Color: 2
Size: 7542 Color: 0
Size: 1508 Color: 2

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 3
Size: 6416 Color: 1
Size: 964 Color: 3

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 15176 Color: 2
Size: 4528 Color: 3

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 15568 Color: 3
Size: 3696 Color: 1
Size: 440 Color: 4

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 16712 Color: 2
Size: 2992 Color: 0

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 14546 Color: 3
Size: 4821 Color: 1
Size: 336 Color: 3

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 17081 Color: 4
Size: 2622 Color: 2

Bin 125: 10 of cap free
Amount of items: 3
Items: 
Size: 15101 Color: 1
Size: 2568 Color: 0
Size: 2033 Color: 3

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 16392 Color: 2
Size: 3310 Color: 3

Bin 127: 11 of cap free
Amount of items: 3
Items: 
Size: 15085 Color: 1
Size: 3108 Color: 1
Size: 1508 Color: 3

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 17508 Color: 0
Size: 2193 Color: 1

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 17530 Color: 4
Size: 2171 Color: 1

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 14892 Color: 0
Size: 4808 Color: 2

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 15716 Color: 2
Size: 3984 Color: 4

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 1
Size: 2291 Color: 4
Size: 104 Color: 3

Bin 133: 15 of cap free
Amount of items: 5
Items: 
Size: 9862 Color: 0
Size: 3199 Color: 2
Size: 2485 Color: 1
Size: 2329 Color: 3
Size: 1822 Color: 4

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 0
Size: 2720 Color: 2

Bin 135: 17 of cap free
Amount of items: 2
Items: 
Size: 16585 Color: 3
Size: 3110 Color: 2

Bin 136: 18 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 3
Size: 6140 Color: 0
Size: 692 Color: 4

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 16207 Color: 2
Size: 3487 Color: 3

Bin 138: 19 of cap free
Amount of items: 3
Items: 
Size: 13109 Color: 3
Size: 6152 Color: 2
Size: 432 Color: 2

Bin 139: 19 of cap free
Amount of items: 2
Items: 
Size: 17660 Color: 2
Size: 2033 Color: 0

Bin 140: 22 of cap free
Amount of items: 3
Items: 
Size: 14384 Color: 1
Size: 4442 Color: 2
Size: 864 Color: 0

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 17720 Color: 0
Size: 1970 Color: 1

Bin 142: 23 of cap free
Amount of items: 3
Items: 
Size: 13690 Color: 0
Size: 5503 Color: 4
Size: 496 Color: 0

Bin 143: 23 of cap free
Amount of items: 2
Items: 
Size: 15489 Color: 0
Size: 4200 Color: 2

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 16167 Color: 0
Size: 3521 Color: 3

Bin 145: 26 of cap free
Amount of items: 2
Items: 
Size: 17592 Color: 4
Size: 2094 Color: 2

Bin 146: 27 of cap free
Amount of items: 2
Items: 
Size: 17620 Color: 2
Size: 2065 Color: 4

Bin 147: 28 of cap free
Amount of items: 2
Items: 
Size: 14988 Color: 2
Size: 4696 Color: 4

Bin 148: 29 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 1
Size: 5499 Color: 3
Size: 928 Color: 0

Bin 149: 29 of cap free
Amount of items: 2
Items: 
Size: 15899 Color: 1
Size: 3784 Color: 3

Bin 150: 32 of cap free
Amount of items: 2
Items: 
Size: 14288 Color: 0
Size: 5392 Color: 3

Bin 151: 32 of cap free
Amount of items: 2
Items: 
Size: 15944 Color: 2
Size: 3736 Color: 3

Bin 152: 34 of cap free
Amount of items: 2
Items: 
Size: 16826 Color: 4
Size: 2852 Color: 0

Bin 153: 36 of cap free
Amount of items: 3
Items: 
Size: 15982 Color: 2
Size: 3422 Color: 0
Size: 272 Color: 2

Bin 154: 36 of cap free
Amount of items: 2
Items: 
Size: 17220 Color: 2
Size: 2456 Color: 0

Bin 155: 38 of cap free
Amount of items: 2
Items: 
Size: 13180 Color: 2
Size: 6494 Color: 0

Bin 156: 38 of cap free
Amount of items: 2
Items: 
Size: 16968 Color: 4
Size: 2706 Color: 0

Bin 157: 39 of cap free
Amount of items: 2
Items: 
Size: 15776 Color: 0
Size: 3897 Color: 3

Bin 158: 43 of cap free
Amount of items: 2
Items: 
Size: 17464 Color: 1
Size: 2205 Color: 4

Bin 159: 44 of cap free
Amount of items: 2
Items: 
Size: 16648 Color: 0
Size: 3020 Color: 3

Bin 160: 46 of cap free
Amount of items: 2
Items: 
Size: 16570 Color: 0
Size: 3096 Color: 3

Bin 161: 46 of cap free
Amount of items: 2
Items: 
Size: 17608 Color: 2
Size: 2058 Color: 1

Bin 162: 48 of cap free
Amount of items: 3
Items: 
Size: 15550 Color: 2
Size: 3858 Color: 3
Size: 256 Color: 0

Bin 163: 50 of cap free
Amount of items: 2
Items: 
Size: 15640 Color: 3
Size: 4022 Color: 2

Bin 164: 52 of cap free
Amount of items: 2
Items: 
Size: 16046 Color: 4
Size: 3614 Color: 0

Bin 165: 53 of cap free
Amount of items: 2
Items: 
Size: 11121 Color: 0
Size: 8538 Color: 4

Bin 166: 53 of cap free
Amount of items: 2
Items: 
Size: 15816 Color: 3
Size: 3843 Color: 2

Bin 167: 54 of cap free
Amount of items: 3
Items: 
Size: 15590 Color: 4
Size: 3940 Color: 3
Size: 128 Color: 4

Bin 168: 60 of cap free
Amount of items: 3
Items: 
Size: 9876 Color: 3
Size: 8208 Color: 1
Size: 1568 Color: 2

Bin 169: 60 of cap free
Amount of items: 2
Items: 
Size: 16948 Color: 0
Size: 2704 Color: 3

Bin 170: 62 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 1
Size: 2955 Color: 4
Size: 2607 Color: 0

Bin 171: 66 of cap free
Amount of items: 2
Items: 
Size: 16342 Color: 0
Size: 3304 Color: 3

Bin 172: 67 of cap free
Amount of items: 2
Items: 
Size: 13927 Color: 3
Size: 5718 Color: 2

Bin 173: 68 of cap free
Amount of items: 2
Items: 
Size: 13264 Color: 2
Size: 6380 Color: 3

Bin 174: 70 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 4
Size: 7370 Color: 0

Bin 175: 70 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 4
Size: 5602 Color: 0

Bin 176: 70 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 4
Size: 5026 Color: 3

Bin 177: 70 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 0
Size: 2600 Color: 4

Bin 178: 80 of cap free
Amount of items: 2
Items: 
Size: 9872 Color: 4
Size: 9760 Color: 0

Bin 179: 85 of cap free
Amount of items: 3
Items: 
Size: 10160 Color: 3
Size: 8211 Color: 4
Size: 1256 Color: 1

Bin 180: 93 of cap free
Amount of items: 2
Items: 
Size: 15031 Color: 1
Size: 4588 Color: 4

Bin 181: 94 of cap free
Amount of items: 2
Items: 
Size: 11858 Color: 0
Size: 7760 Color: 2

Bin 182: 94 of cap free
Amount of items: 2
Items: 
Size: 17328 Color: 3
Size: 2290 Color: 4

Bin 183: 95 of cap free
Amount of items: 2
Items: 
Size: 17321 Color: 0
Size: 2296 Color: 1

Bin 184: 96 of cap free
Amount of items: 2
Items: 
Size: 10870 Color: 4
Size: 8746 Color: 3

Bin 185: 97 of cap free
Amount of items: 2
Items: 
Size: 17316 Color: 4
Size: 2299 Color: 0

Bin 186: 106 of cap free
Amount of items: 3
Items: 
Size: 15990 Color: 3
Size: 3480 Color: 0
Size: 136 Color: 3

Bin 187: 120 of cap free
Amount of items: 3
Items: 
Size: 9864 Color: 3
Size: 7127 Color: 3
Size: 2601 Color: 1

Bin 188: 139 of cap free
Amount of items: 2
Items: 
Size: 14551 Color: 3
Size: 5022 Color: 2

Bin 189: 148 of cap free
Amount of items: 2
Items: 
Size: 12348 Color: 3
Size: 7216 Color: 4

Bin 190: 165 of cap free
Amount of items: 2
Items: 
Size: 15037 Color: 4
Size: 4510 Color: 2

Bin 191: 188 of cap free
Amount of items: 32
Items: 
Size: 780 Color: 2
Size: 770 Color: 0
Size: 768 Color: 4
Size: 768 Color: 2
Size: 768 Color: 0
Size: 752 Color: 0
Size: 736 Color: 0
Size: 720 Color: 3
Size: 692 Color: 4
Size: 684 Color: 2
Size: 684 Color: 0
Size: 672 Color: 2
Size: 656 Color: 4
Size: 656 Color: 2
Size: 656 Color: 0
Size: 642 Color: 3
Size: 638 Color: 4
Size: 638 Color: 1
Size: 576 Color: 3
Size: 560 Color: 1
Size: 548 Color: 1
Size: 544 Color: 4
Size: 540 Color: 2
Size: 496 Color: 1
Size: 488 Color: 3
Size: 486 Color: 3
Size: 484 Color: 4
Size: 480 Color: 2
Size: 448 Color: 1
Size: 432 Color: 3
Size: 410 Color: 1
Size: 352 Color: 1

Bin 192: 225 of cap free
Amount of items: 3
Items: 
Size: 9857 Color: 2
Size: 8200 Color: 2
Size: 1430 Color: 0

Bin 193: 245 of cap free
Amount of items: 2
Items: 
Size: 12171 Color: 4
Size: 7296 Color: 3

Bin 194: 272 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 0
Size: 8216 Color: 4

Bin 195: 290 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 1
Size: 7100 Color: 3
Size: 2442 Color: 0

Bin 196: 290 of cap free
Amount of items: 2
Items: 
Size: 11208 Color: 3
Size: 8214 Color: 0

Bin 197: 300 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 4
Size: 5552 Color: 2

Bin 198: 317 of cap free
Amount of items: 2
Items: 
Size: 14511 Color: 2
Size: 4884 Color: 3

Bin 199: 14018 of cap free
Amount of items: 15
Items: 
Size: 432 Color: 2
Size: 418 Color: 2
Size: 406 Color: 4
Size: 406 Color: 2
Size: 400 Color: 3
Size: 400 Color: 2
Size: 396 Color: 3
Size: 392 Color: 0
Size: 384 Color: 3
Size: 360 Color: 0
Size: 352 Color: 1
Size: 352 Color: 0
Size: 342 Color: 1
Size: 334 Color: 1
Size: 320 Color: 4

Total size: 3902976
Total free space: 19712

