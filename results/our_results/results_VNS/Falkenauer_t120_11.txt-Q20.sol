Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 5
Size: 349 Color: 13
Size: 258 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 255 Color: 15
Size: 252 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 13
Size: 256 Color: 1
Size: 253 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 9
Size: 350 Color: 4
Size: 265 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 263 Color: 9
Size: 288 Color: 11

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 18
Size: 275 Color: 13
Size: 262 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 17
Size: 343 Color: 12
Size: 257 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 347 Color: 16
Size: 258 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8
Size: 308 Color: 5
Size: 292 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 326 Color: 1
Size: 300 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 17
Size: 361 Color: 15
Size: 262 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 266 Color: 8
Size: 269 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 15
Size: 296 Color: 13
Size: 261 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 7
Size: 256 Color: 3
Size: 253 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 17
Size: 320 Color: 9
Size: 308 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 3
Size: 288 Color: 7
Size: 277 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 10
Size: 291 Color: 19
Size: 266 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 15
Size: 264 Color: 10
Size: 251 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 354 Color: 7
Size: 281 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 301 Color: 8
Size: 275 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 7
Size: 332 Color: 8
Size: 296 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 0
Size: 251 Color: 18
Size: 250 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 268 Color: 5
Size: 267 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 11
Size: 313 Color: 5
Size: 275 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 13
Size: 256 Color: 9
Size: 251 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 310 Color: 17
Size: 291 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 7
Size: 264 Color: 5
Size: 264 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 14
Size: 310 Color: 9
Size: 268 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 288 Color: 0
Size: 262 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 7
Size: 305 Color: 12
Size: 287 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 337 Color: 2
Size: 280 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 273 Color: 8
Size: 271 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 260 Color: 6
Size: 252 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 325 Color: 5
Size: 297 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 295 Color: 12
Size: 276 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 17
Size: 344 Color: 9
Size: 256 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 12
Size: 266 Color: 0
Size: 251 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 18
Size: 338 Color: 0
Size: 261 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 7
Size: 329 Color: 3
Size: 311 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 355 Color: 14
Size: 268 Color: 13

Total size: 40000
Total free space: 0

