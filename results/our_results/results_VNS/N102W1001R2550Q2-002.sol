Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 288 Color: 0
Size: 275 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 289 Color: 0
Size: 339 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 275 Color: 0
Size: 273 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 271 Color: 0
Size: 266 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 293 Color: 1
Size: 282 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 313 Color: 1
Size: 274 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 297 Color: 1
Size: 252 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 0
Size: 253 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 355 Color: 1
Size: 272 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 339 Color: 1
Size: 277 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 0
Size: 341 Color: 1
Size: 309 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 354 Color: 0
Size: 272 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 319 Color: 1
Size: 312 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 341 Color: 1
Size: 305 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 361 Color: 0
Size: 278 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 297 Color: 1
Size: 285 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 320 Color: 1
Size: 267 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 282 Color: 1
Size: 261 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 1
Size: 331 Color: 0
Size: 329 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 367 Color: 0
Size: 254 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 289 Color: 1
Size: 269 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 305 Color: 1
Size: 288 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 312 Color: 1
Size: 310 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 349 Color: 0
Size: 275 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 317 Color: 0
Size: 255 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 311 Color: 0
Size: 308 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 286 Color: 1
Size: 267 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 354 Color: 1
Size: 255 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 278 Color: 1
Size: 274 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 323 Color: 1
Size: 290 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 340 Color: 1
Size: 279 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 0
Size: 250 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 278 Color: 1
Size: 277 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 320 Color: 1
Size: 317 Color: 1

Total size: 34034
Total free space: 0

