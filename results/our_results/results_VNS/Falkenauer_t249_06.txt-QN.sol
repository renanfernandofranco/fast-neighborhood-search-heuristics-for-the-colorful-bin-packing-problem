Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 191
Size: 352 Color: 160
Size: 252 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 233
Size: 269 Color: 70
Size: 256 Color: 34

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 184
Size: 308 Color: 122
Size: 303 Color: 119

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 236
Size: 264 Color: 60
Size: 257 Color: 40

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 189
Size: 316 Color: 134
Size: 289 Color: 100

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 231
Size: 283 Color: 89
Size: 250 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 178
Size: 366 Color: 168
Size: 254 Color: 25

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 225
Size: 293 Color: 104
Size: 250 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 174
Size: 364 Color: 167
Size: 264 Color: 63

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 234
Size: 263 Color: 57
Size: 259 Color: 48

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 207
Size: 321 Color: 137
Size: 252 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 226
Size: 282 Color: 88
Size: 261 Color: 53

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 148
Size: 284 Color: 91
Size: 376 Color: 177

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 196
Size: 342 Color: 150
Size: 250 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 238
Size: 270 Color: 73
Size: 250 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 12
Size: 250 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 213
Size: 298 Color: 112
Size: 263 Color: 56

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 193
Size: 313 Color: 130
Size: 284 Color: 90

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 185
Size: 313 Color: 128
Size: 295 Color: 110

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 245
Size: 254 Color: 26
Size: 251 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 232
Size: 269 Color: 69
Size: 263 Color: 58

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 214
Size: 297 Color: 111
Size: 263 Color: 59

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 220
Size: 294 Color: 107
Size: 258 Color: 46

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 164
Size: 352 Color: 157
Size: 291 Color: 103

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 161
Size: 352 Color: 158
Size: 294 Color: 108

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 244
Size: 255 Color: 32
Size: 251 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 203
Size: 314 Color: 131
Size: 271 Color: 75

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 190
Size: 352 Color: 159
Size: 253 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 223
Size: 290 Color: 101
Size: 257 Color: 41

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 240
Size: 255 Color: 30
Size: 254 Color: 27

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 182
Size: 314 Color: 132
Size: 300 Color: 115

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 201
Size: 331 Color: 141
Size: 255 Color: 29

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 187
Size: 351 Color: 156
Size: 256 Color: 35

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 230
Size: 277 Color: 81
Size: 257 Color: 39

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 241
Size: 257 Color: 42
Size: 251 Color: 14

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 221
Size: 274 Color: 78
Size: 274 Color: 77

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 165
Size: 325 Color: 140
Size: 313 Color: 129

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 194
Size: 335 Color: 145
Size: 259 Color: 50

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 243
Size: 255 Color: 31
Size: 251 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 154
Size: 350 Color: 153
Size: 300 Color: 114

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 215
Size: 298 Color: 113
Size: 260 Color: 51

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 219
Size: 300 Color: 117
Size: 252 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 206
Size: 293 Color: 106
Size: 284 Color: 93

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 152
Size: 346 Color: 151
Size: 308 Color: 123

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 197
Size: 332 Color: 143
Size: 258 Color: 44

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 217
Size: 278 Color: 83
Size: 278 Color: 82

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 237
Size: 265 Color: 64
Size: 256 Color: 37

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 175
Size: 370 Color: 172
Size: 256 Color: 38

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 181
Size: 366 Color: 169
Size: 250 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 210
Size: 309 Color: 124
Size: 259 Color: 47

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 202
Size: 302 Color: 118
Size: 284 Color: 92

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 173
Size: 363 Color: 166
Size: 266 Color: 67

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 204
Size: 305 Color: 120
Size: 280 Color: 85

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 228
Size: 289 Color: 97
Size: 250 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 212
Size: 282 Color: 87
Size: 282 Color: 86

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 208
Size: 310 Color: 126
Size: 262 Color: 54

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 247
Size: 253 Color: 20
Size: 250 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 171
Size: 341 Color: 149
Size: 290 Color: 102

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 229
Size: 273 Color: 76
Size: 262 Color: 55

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 211
Size: 312 Color: 127
Size: 256 Color: 33

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 246
Size: 253 Color: 22
Size: 251 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 235
Size: 266 Color: 66
Size: 255 Color: 28

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 170
Size: 357 Color: 163
Size: 274 Color: 79

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 205
Size: 321 Color: 139
Size: 259 Color: 49

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 199
Size: 321 Color: 138
Size: 266 Color: 65

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 183
Size: 356 Color: 162
Size: 257 Color: 43

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 200
Size: 316 Color: 133
Size: 270 Color: 72

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 198
Size: 310 Color: 125
Size: 278 Color: 84

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 192
Size: 339 Color: 147
Size: 261 Color: 52

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 195
Size: 318 Color: 136
Size: 275 Color: 80

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 188
Size: 306 Color: 121
Size: 300 Color: 116

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 186
Size: 351 Color: 155
Size: 256 Color: 36

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 180
Size: 332 Color: 142
Size: 285 Color: 94

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 179
Size: 335 Color: 144
Size: 285 Color: 95

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 176
Size: 336 Color: 146
Size: 289 Color: 99

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 209
Size: 317 Color: 135
Size: 254 Color: 24

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 216
Size: 293 Color: 105
Size: 264 Color: 61

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 218
Size: 289 Color: 96
Size: 264 Color: 62

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 222
Size: 295 Color: 109
Size: 252 Color: 16

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 227
Size: 271 Color: 74
Size: 269 Color: 71

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 224
Size: 289 Color: 98
Size: 258 Color: 45

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 239
Size: 268 Color: 68
Size: 250 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 242
Size: 254 Color: 23
Size: 253 Color: 21

Total size: 83000
Total free space: 0

