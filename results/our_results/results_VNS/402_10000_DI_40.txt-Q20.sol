Capicity Bin: 7888
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 30
Items: 
Size: 384 Color: 8
Size: 376 Color: 9
Size: 356 Color: 2
Size: 352 Color: 7
Size: 342 Color: 16
Size: 336 Color: 18
Size: 336 Color: 12
Size: 326 Color: 8
Size: 320 Color: 15
Size: 320 Color: 7
Size: 300 Color: 12
Size: 298 Color: 16
Size: 284 Color: 2
Size: 270 Color: 11
Size: 244 Color: 10
Size: 240 Color: 14
Size: 240 Color: 1
Size: 232 Color: 11
Size: 232 Color: 2
Size: 224 Color: 1
Size: 220 Color: 13
Size: 220 Color: 5
Size: 216 Color: 11
Size: 200 Color: 4
Size: 196 Color: 18
Size: 192 Color: 18
Size: 172 Color: 19
Size: 172 Color: 13
Size: 152 Color: 5
Size: 136 Color: 6

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 3946 Color: 10
Size: 1359 Color: 8
Size: 1351 Color: 10
Size: 1064 Color: 18
Size: 168 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4060 Color: 5
Size: 3652 Color: 6
Size: 176 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4502 Color: 9
Size: 2822 Color: 6
Size: 564 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 1
Size: 2122 Color: 7
Size: 420 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 4
Size: 2124 Color: 8
Size: 240 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 14
Size: 2124 Color: 8
Size: 184 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 2
Size: 2022 Color: 7
Size: 212 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5865 Color: 3
Size: 1819 Color: 10
Size: 204 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6014 Color: 13
Size: 1068 Color: 6
Size: 806 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 8
Size: 1466 Color: 6
Size: 292 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 19
Size: 1374 Color: 3
Size: 264 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 17
Size: 989 Color: 8
Size: 640 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 15
Size: 1348 Color: 3
Size: 200 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 19
Size: 1235 Color: 4
Size: 246 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 12
Size: 1106 Color: 17
Size: 372 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 8
Size: 1234 Color: 4
Size: 228 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 7
Size: 974 Color: 1
Size: 408 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 6
Size: 682 Color: 3
Size: 652 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6570 Color: 14
Size: 662 Color: 17
Size: 656 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 1
Size: 956 Color: 8
Size: 272 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6708 Color: 2
Size: 764 Color: 10
Size: 416 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 11
Size: 878 Color: 12
Size: 288 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 19
Size: 870 Color: 17
Size: 252 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 0
Size: 988 Color: 7
Size: 104 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 19
Size: 724 Color: 2
Size: 328 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 19
Size: 722 Color: 13
Size: 312 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 14
Size: 754 Color: 17
Size: 216 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 8
Size: 580 Color: 2
Size: 328 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6986 Color: 17
Size: 724 Color: 16
Size: 178 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 4
Size: 702 Color: 15
Size: 144 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 12
Size: 532 Color: 1
Size: 306 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 15
Size: 552 Color: 17
Size: 284 Color: 4

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 4
Size: 3645 Color: 7
Size: 276 Color: 4

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 9
Size: 2347 Color: 6
Size: 200 Color: 2

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 6
Size: 1884 Color: 5
Size: 80 Color: 2

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5963 Color: 8
Size: 1114 Color: 5
Size: 810 Color: 4

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 12
Size: 1639 Color: 16
Size: 132 Color: 19

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6187 Color: 0
Size: 1700 Color: 16

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6267 Color: 19
Size: 1620 Color: 2

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 4
Size: 1537 Color: 11
Size: 58 Color: 14

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6304 Color: 18
Size: 1423 Color: 16
Size: 160 Color: 9

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 8
Size: 1366 Color: 9
Size: 148 Color: 14

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 2
Size: 1069 Color: 17
Size: 270 Color: 8

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6555 Color: 9
Size: 1332 Color: 19

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 1
Size: 1325 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 18
Size: 1188 Color: 13
Size: 132 Color: 3

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 17
Size: 1139 Color: 15

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6838 Color: 6
Size: 1049 Color: 15

Bin 50: 2 of cap free
Amount of items: 19
Items: 
Size: 564 Color: 4
Size: 512 Color: 9
Size: 512 Color: 3
Size: 496 Color: 10
Size: 480 Color: 19
Size: 476 Color: 8
Size: 468 Color: 6
Size: 448 Color: 11
Size: 444 Color: 14
Size: 424 Color: 17
Size: 424 Color: 0
Size: 392 Color: 16
Size: 384 Color: 17
Size: 384 Color: 10
Size: 370 Color: 7
Size: 352 Color: 1
Size: 300 Color: 5
Size: 264 Color: 13
Size: 192 Color: 15

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 3974 Color: 12
Size: 3912 Color: 14

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 6
Size: 2804 Color: 15
Size: 572 Color: 13

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 15
Size: 2059 Color: 5
Size: 168 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 3
Size: 1605 Color: 18
Size: 236 Color: 14

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6390 Color: 10
Size: 1496 Color: 19

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 14
Size: 1434 Color: 9

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 4
Size: 1018 Color: 13

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 0
Size: 858 Color: 8

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 2
Size: 796 Color: 4

Bin 60: 3 of cap free
Amount of items: 4
Items: 
Size: 3958 Color: 14
Size: 3037 Color: 11
Size: 706 Color: 12
Size: 184 Color: 4

Bin 61: 3 of cap free
Amount of items: 4
Items: 
Size: 3949 Color: 10
Size: 3238 Color: 7
Size: 472 Color: 18
Size: 226 Color: 16

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6523 Color: 17
Size: 1154 Color: 1
Size: 208 Color: 14

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 14
Size: 1102 Color: 19
Size: 128 Color: 18

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 9
Size: 1023 Color: 16

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 3
Size: 2788 Color: 1
Size: 564 Color: 8

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 12
Size: 916 Color: 8

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 7022 Color: 16
Size: 862 Color: 4

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 3
Size: 2127 Color: 13
Size: 280 Color: 17

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6439 Color: 1
Size: 1444 Color: 9

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 6661 Color: 3
Size: 1222 Color: 7

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 6
Size: 1209 Color: 17

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 5073 Color: 10
Size: 2561 Color: 4
Size: 248 Color: 11

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 2
Size: 1124 Color: 7

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 6922 Color: 3
Size: 960 Color: 10

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 13
Size: 942 Color: 0

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 4611 Color: 11
Size: 3270 Color: 12

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 4815 Color: 16
Size: 2878 Color: 15
Size: 188 Color: 8

Bin 78: 7 of cap free
Amount of items: 3
Items: 
Size: 6164 Color: 17
Size: 1165 Color: 9
Size: 552 Color: 8

Bin 79: 8 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 2
Size: 2076 Color: 1
Size: 400 Color: 19

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 13
Size: 2028 Color: 19

Bin 81: 9 of cap free
Amount of items: 6
Items: 
Size: 3948 Color: 17
Size: 1111 Color: 13
Size: 1054 Color: 12
Size: 700 Color: 1
Size: 656 Color: 19
Size: 410 Color: 14

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 4755 Color: 18
Size: 3124 Color: 17

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 6670 Color: 1
Size: 1208 Color: 7

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6703 Color: 1
Size: 1175 Color: 15

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 4948 Color: 5
Size: 2928 Color: 1

Bin 86: 12 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 5
Size: 1862 Color: 7
Size: 666 Color: 17

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 5611 Color: 14
Size: 2265 Color: 6

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 6626 Color: 16
Size: 1250 Color: 9

Bin 89: 13 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 8
Size: 1972 Color: 11
Size: 176 Color: 1

Bin 90: 13 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 6
Size: 1263 Color: 13

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 5775 Color: 14
Size: 2099 Color: 0

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 0
Size: 1028 Color: 6

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 7098 Color: 16
Size: 776 Color: 14

Bin 94: 15 of cap free
Amount of items: 3
Items: 
Size: 4143 Color: 8
Size: 3278 Color: 18
Size: 452 Color: 11

Bin 95: 16 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 0
Size: 1484 Color: 5
Size: 488 Color: 14

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 5948 Color: 16
Size: 1924 Color: 11

Bin 97: 16 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 16
Size: 852 Color: 2

Bin 98: 17 of cap free
Amount of items: 2
Items: 
Size: 5419 Color: 12
Size: 2452 Color: 13

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 5628 Color: 18
Size: 2242 Color: 14

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 6210 Color: 19
Size: 1660 Color: 17

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 6468 Color: 2
Size: 1402 Color: 13

Bin 102: 19 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 8
Size: 2562 Color: 0
Size: 136 Color: 18

Bin 103: 19 of cap free
Amount of items: 2
Items: 
Size: 6735 Color: 4
Size: 1134 Color: 13

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 6181 Color: 11
Size: 1687 Color: 19

Bin 105: 22 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 14
Size: 2838 Color: 6

Bin 106: 23 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 1
Size: 2907 Color: 2
Size: 144 Color: 17

Bin 107: 23 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 11
Size: 1503 Color: 0

Bin 108: 24 of cap free
Amount of items: 3
Items: 
Size: 6170 Color: 9
Size: 1646 Color: 10
Size: 48 Color: 5

Bin 109: 27 of cap free
Amount of items: 8
Items: 
Size: 3945 Color: 17
Size: 656 Color: 16
Size: 652 Color: 9
Size: 624 Color: 12
Size: 624 Color: 9
Size: 616 Color: 5
Size: 522 Color: 7
Size: 222 Color: 18

Bin 110: 27 of cap free
Amount of items: 2
Items: 
Size: 5337 Color: 0
Size: 2524 Color: 17

Bin 111: 27 of cap free
Amount of items: 2
Items: 
Size: 6299 Color: 13
Size: 1562 Color: 6

Bin 112: 29 of cap free
Amount of items: 4
Items: 
Size: 3950 Color: 19
Size: 1506 Color: 11
Size: 1465 Color: 8
Size: 938 Color: 9

Bin 113: 29 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 4
Size: 1204 Color: 1
Size: 80 Color: 19

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 5026 Color: 1
Size: 2830 Color: 14

Bin 115: 34 of cap free
Amount of items: 2
Items: 
Size: 5466 Color: 6
Size: 2388 Color: 10

Bin 116: 36 of cap free
Amount of items: 2
Items: 
Size: 6607 Color: 16
Size: 1245 Color: 18

Bin 117: 38 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 13
Size: 884 Color: 0

Bin 118: 39 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 19
Size: 1859 Color: 1
Size: 76 Color: 11

Bin 119: 41 of cap free
Amount of items: 2
Items: 
Size: 5774 Color: 13
Size: 2073 Color: 14

Bin 120: 41 of cap free
Amount of items: 2
Items: 
Size: 6085 Color: 7
Size: 1762 Color: 16

Bin 121: 43 of cap free
Amount of items: 2
Items: 
Size: 5459 Color: 9
Size: 2386 Color: 5

Bin 122: 44 of cap free
Amount of items: 2
Items: 
Size: 4438 Color: 7
Size: 3406 Color: 13

Bin 123: 44 of cap free
Amount of items: 2
Items: 
Size: 7074 Color: 4
Size: 770 Color: 3

Bin 124: 45 of cap free
Amount of items: 2
Items: 
Size: 6082 Color: 6
Size: 1761 Color: 19

Bin 125: 50 of cap free
Amount of items: 2
Items: 
Size: 4548 Color: 19
Size: 3290 Color: 17

Bin 126: 51 of cap free
Amount of items: 2
Items: 
Size: 4868 Color: 7
Size: 2969 Color: 13

Bin 127: 56 of cap free
Amount of items: 3
Items: 
Size: 4401 Color: 13
Size: 3283 Color: 3
Size: 148 Color: 8

Bin 128: 68 of cap free
Amount of items: 3
Items: 
Size: 4148 Color: 14
Size: 3464 Color: 17
Size: 208 Color: 12

Bin 129: 72 of cap free
Amount of items: 2
Items: 
Size: 4695 Color: 0
Size: 3121 Color: 18

Bin 130: 75 of cap free
Amount of items: 2
Items: 
Size: 5202 Color: 13
Size: 2611 Color: 0

Bin 131: 108 of cap free
Amount of items: 2
Items: 
Size: 4494 Color: 4
Size: 3286 Color: 6

Bin 132: 118 of cap free
Amount of items: 2
Items: 
Size: 4486 Color: 1
Size: 3284 Color: 17

Bin 133: 6132 of cap free
Amount of items: 12
Items: 
Size: 172 Color: 1
Size: 168 Color: 12
Size: 160 Color: 18
Size: 152 Color: 19
Size: 152 Color: 14
Size: 144 Color: 19
Size: 144 Color: 15
Size: 140 Color: 13
Size: 140 Color: 7
Size: 136 Color: 5
Size: 128 Color: 17
Size: 120 Color: 8

Total size: 1041216
Total free space: 7888

