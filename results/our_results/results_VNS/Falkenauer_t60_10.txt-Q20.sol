Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 320 Color: 6
Size: 257 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 7
Size: 322 Color: 10
Size: 250 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 0
Size: 301 Color: 0
Size: 280 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 278 Color: 18
Size: 274 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 351 Color: 19
Size: 251 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 14
Size: 335 Color: 7
Size: 299 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 12
Size: 265 Color: 19
Size: 257 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 5
Size: 256 Color: 10
Size: 253 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 14
Size: 271 Color: 11
Size: 257 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 347 Color: 13
Size: 250 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 6
Size: 323 Color: 4
Size: 277 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 12
Size: 318 Color: 13
Size: 265 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 6
Size: 292 Color: 15
Size: 284 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 270 Color: 6
Size: 266 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 5
Size: 291 Color: 3
Size: 268 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 10
Size: 310 Color: 19
Size: 251 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 3
Size: 355 Color: 13
Size: 285 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 14
Size: 332 Color: 13
Size: 280 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 7
Size: 357 Color: 19
Size: 260 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 10
Size: 294 Color: 5
Size: 266 Color: 4

Total size: 20000
Total free space: 0

