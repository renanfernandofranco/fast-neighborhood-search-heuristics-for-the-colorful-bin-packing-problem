Capicity Bin: 2356
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 826 Color: 1
Size: 613 Color: 0
Size: 381 Color: 0
Size: 188 Color: 1
Size: 158 Color: 1
Size: 114 Color: 1
Size: 76 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1181 Color: 1
Size: 981 Color: 1
Size: 194 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 1
Size: 858 Color: 1
Size: 168 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 1
Size: 691 Color: 0
Size: 138 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 1
Size: 499 Color: 0
Size: 178 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 1
Size: 474 Color: 0
Size: 100 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 1
Size: 475 Color: 0
Size: 78 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 0
Size: 333 Color: 1
Size: 76 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 1
Size: 235 Color: 1
Size: 124 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2010 Color: 1
Size: 202 Color: 0
Size: 144 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 1
Size: 241 Color: 1
Size: 98 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 0
Size: 301 Color: 1
Size: 54 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2031 Color: 1
Size: 283 Color: 1
Size: 42 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 1
Size: 226 Color: 1
Size: 60 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2071 Color: 1
Size: 229 Color: 1
Size: 56 Color: 0

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 2080 Color: 1
Size: 214 Color: 0
Size: 32 Color: 0
Size: 30 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 1
Size: 199 Color: 0
Size: 58 Color: 1

Bin 18: 1 of cap free
Amount of items: 5
Items: 
Size: 1179 Color: 1
Size: 876 Color: 0
Size: 192 Color: 1
Size: 68 Color: 1
Size: 40 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 0
Size: 930 Color: 1
Size: 144 Color: 1

Bin 20: 1 of cap free
Amount of items: 4
Items: 
Size: 1811 Color: 0
Size: 482 Color: 1
Size: 32 Color: 1
Size: 30 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 1
Size: 350 Color: 0
Size: 162 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1957 Color: 1
Size: 318 Color: 1
Size: 80 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 0
Size: 341 Color: 1
Size: 36 Color: 1

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 1377 Color: 0
Size: 977 Color: 1

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 1567 Color: 0
Size: 787 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 1
Size: 667 Color: 0
Size: 108 Color: 1

Bin 27: 2 of cap free
Amount of items: 4
Items: 
Size: 1694 Color: 0
Size: 320 Color: 1
Size: 290 Color: 1
Size: 50 Color: 0

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 0
Size: 554 Color: 1
Size: 58 Color: 0

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 0
Size: 547 Color: 1

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 1
Size: 429 Color: 0
Size: 52 Color: 0

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2083 Color: 0
Size: 271 Color: 1

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 0
Size: 236 Color: 1

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 0
Size: 1032 Color: 1
Size: 136 Color: 0

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1189 Color: 1
Size: 1164 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 1
Size: 817 Color: 1
Size: 66 Color: 0

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 649 Color: 0
Size: 46 Color: 1

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 0
Size: 495 Color: 1
Size: 44 Color: 0

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1899 Color: 1
Size: 454 Color: 0

Bin 39: 3 of cap free
Amount of items: 4
Items: 
Size: 2102 Color: 1
Size: 215 Color: 0
Size: 32 Color: 0
Size: 4 Color: 1

Bin 40: 4 of cap free
Amount of items: 17
Items: 
Size: 360 Color: 0
Size: 194 Color: 0
Size: 186 Color: 0
Size: 184 Color: 0
Size: 164 Color: 0
Size: 156 Color: 0
Size: 136 Color: 1
Size: 134 Color: 0
Size: 128 Color: 1
Size: 122 Color: 1
Size: 112 Color: 1
Size: 108 Color: 1
Size: 90 Color: 1
Size: 90 Color: 1
Size: 88 Color: 1
Size: 62 Color: 1
Size: 38 Color: 0

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 1
Size: 742 Color: 0
Size: 92 Color: 0

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 0
Size: 702 Color: 1
Size: 56 Color: 0

Bin 43: 6 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 0
Size: 259 Color: 1
Size: 12 Color: 0

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 2119 Color: 1
Size: 231 Color: 0

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1890 Color: 0
Size: 459 Color: 1

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1366 Color: 0
Size: 982 Color: 1

Bin 47: 8 of cap free
Amount of items: 2
Items: 
Size: 1397 Color: 0
Size: 951 Color: 1

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 0
Size: 262 Color: 1

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1681 Color: 0
Size: 666 Color: 1

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1413 Color: 0
Size: 933 Color: 1

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 2103 Color: 0
Size: 242 Color: 1

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 1
Size: 801 Color: 0

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 2047 Color: 1
Size: 297 Color: 0

Bin 54: 14 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 1
Size: 981 Color: 0
Size: 44 Color: 0

Bin 55: 14 of cap free
Amount of items: 3
Items: 
Size: 1878 Color: 0
Size: 448 Color: 1
Size: 16 Color: 1

Bin 56: 15 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 1053 Color: 0
Size: 46 Color: 1

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 1827 Color: 0
Size: 514 Color: 1

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1938 Color: 0
Size: 403 Color: 1

Bin 59: 17 of cap free
Amount of items: 4
Items: 
Size: 1182 Color: 0
Size: 679 Color: 1
Size: 390 Color: 1
Size: 88 Color: 0

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 0
Size: 638 Color: 1

Bin 61: 21 of cap free
Amount of items: 2
Items: 
Size: 1880 Color: 0
Size: 455 Color: 1

Bin 62: 22 of cap free
Amount of items: 2
Items: 
Size: 2042 Color: 1
Size: 292 Color: 0

Bin 63: 26 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 1
Size: 897 Color: 0
Size: 196 Color: 0

Bin 64: 26 of cap free
Amount of items: 2
Items: 
Size: 2075 Color: 0
Size: 255 Color: 1

Bin 65: 28 of cap free
Amount of items: 2
Items: 
Size: 1763 Color: 1
Size: 565 Color: 0

Bin 66: 1972 of cap free
Amount of items: 6
Items: 
Size: 112 Color: 0
Size: 84 Color: 0
Size: 68 Color: 1
Size: 44 Color: 0
Size: 40 Color: 1
Size: 36 Color: 1

Total size: 153140
Total free space: 2356

