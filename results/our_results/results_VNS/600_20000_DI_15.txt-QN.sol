Capicity Bin: 16384
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9364 Color: 423
Size: 5852 Color: 383
Size: 488 Color: 91
Size: 344 Color: 44
Size: 336 Color: 43

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 460
Size: 3821 Color: 343
Size: 768 Color: 150

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11873 Color: 465
Size: 3761 Color: 340
Size: 750 Color: 144

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 467
Size: 3908 Color: 347
Size: 582 Color: 113

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12220 Color: 475
Size: 3924 Color: 350
Size: 240 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 478
Size: 3500 Color: 330
Size: 608 Color: 119

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 479
Size: 3112 Color: 316
Size: 976 Color: 171

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 481
Size: 2516 Color: 286
Size: 1482 Color: 211

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 497
Size: 3256 Color: 320
Size: 288 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 504
Size: 2576 Color: 290
Size: 748 Color: 142

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13184 Color: 507
Size: 1624 Color: 223
Size: 1576 Color: 218

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13193 Color: 508
Size: 2763 Color: 298
Size: 428 Color: 74

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 509
Size: 1752 Color: 235
Size: 1434 Color: 207

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 515
Size: 2974 Color: 307
Size: 90 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 519
Size: 1782 Color: 239
Size: 1176 Color: 188

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13434 Color: 520
Size: 2462 Color: 284
Size: 488 Color: 92

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13470 Color: 522
Size: 2418 Color: 280
Size: 496 Color: 95

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13478 Color: 523
Size: 2352 Color: 277
Size: 554 Color: 109

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13486 Color: 524
Size: 2422 Color: 281
Size: 476 Color: 87

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13667 Color: 530
Size: 2265 Color: 272
Size: 452 Color: 83

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 533
Size: 1770 Color: 237
Size: 912 Color: 166

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 540
Size: 2140 Color: 263
Size: 424 Color: 73

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 541
Size: 1690 Color: 228
Size: 864 Color: 161

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13884 Color: 543
Size: 2056 Color: 258
Size: 444 Color: 80

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 545
Size: 2248 Color: 270
Size: 208 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13979 Color: 548
Size: 2005 Color: 254
Size: 400 Color: 65

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14006 Color: 550
Size: 1970 Color: 251
Size: 408 Color: 68

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 559
Size: 1832 Color: 240
Size: 384 Color: 60

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 561
Size: 1740 Color: 233
Size: 444 Color: 78

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14281 Color: 567
Size: 1671 Color: 227
Size: 432 Color: 75

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14291 Color: 568
Size: 1745 Color: 234
Size: 348 Color: 46

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 569
Size: 1052 Color: 180
Size: 1036 Color: 177

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 574
Size: 1582 Color: 220
Size: 448 Color: 82

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 577
Size: 1280 Color: 193
Size: 680 Color: 132

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14468 Color: 580
Size: 1604 Color: 222
Size: 312 Color: 30

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 581
Size: 1476 Color: 209
Size: 424 Color: 71

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14490 Color: 582
Size: 1364 Color: 199
Size: 530 Color: 104

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 583
Size: 1578 Color: 219
Size: 312 Color: 32

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14554 Color: 586
Size: 1526 Color: 215
Size: 304 Color: 28

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14558 Color: 587
Size: 1480 Color: 210
Size: 346 Color: 45

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 588
Size: 1448 Color: 208
Size: 368 Color: 54

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 590
Size: 1024 Color: 176
Size: 744 Color: 138

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14620 Color: 591
Size: 1548 Color: 217
Size: 216 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14660 Color: 593
Size: 1404 Color: 203
Size: 320 Color: 35

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 595
Size: 1382 Color: 202
Size: 328 Color: 38

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 597
Size: 1168 Color: 187
Size: 520 Color: 100

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14708 Color: 598
Size: 1180 Color: 190
Size: 496 Color: 94

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14724 Color: 599
Size: 1176 Color: 189
Size: 484 Color: 89

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14730 Color: 600
Size: 1162 Color: 186
Size: 492 Color: 93

Bin 50: 1 of cap free
Amount of items: 9
Items: 
Size: 8195 Color: 407
Size: 1364 Color: 200
Size: 1360 Color: 198
Size: 1360 Color: 196
Size: 1288 Color: 194
Size: 1264 Color: 192
Size: 752 Color: 145
Size: 400 Color: 67
Size: 400 Color: 66

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 11801 Color: 462
Size: 3806 Color: 342
Size: 776 Color: 151

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 11881 Color: 466
Size: 3798 Color: 341
Size: 704 Color: 136

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 476
Size: 3742 Color: 338
Size: 396 Color: 64

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 477
Size: 3727 Color: 334
Size: 396 Color: 63

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12375 Color: 480
Size: 3912 Color: 348
Size: 96 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12681 Color: 490
Size: 3334 Color: 322
Size: 368 Color: 53

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 491
Size: 3451 Color: 328
Size: 224 Color: 9

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 493
Size: 2851 Color: 303
Size: 746 Color: 141

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12802 Color: 495
Size: 3097 Color: 314
Size: 484 Color: 90

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12957 Color: 500
Size: 2986 Color: 308
Size: 440 Color: 77

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13146 Color: 506
Size: 2857 Color: 304
Size: 380 Color: 58

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13296 Color: 514
Size: 3087 Color: 313

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13521 Color: 525
Size: 2862 Color: 305

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13619 Color: 529
Size: 2764 Color: 299

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 546
Size: 2451 Color: 283

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14261 Color: 565
Size: 1360 Color: 197
Size: 762 Color: 148

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 12774 Color: 492
Size: 3608 Color: 332

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 12794 Color: 494
Size: 2260 Color: 271
Size: 1328 Color: 195

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 518
Size: 3010 Color: 311

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 532
Size: 2694 Color: 296

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 551
Size: 2360 Color: 278

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 14262 Color: 566
Size: 2120 Color: 261

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 594
Size: 1716 Color: 231

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 461
Size: 3825 Color: 344
Size: 756 Color: 146

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 482
Size: 3439 Color: 326
Size: 544 Color: 106

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 12628 Color: 486
Size: 3753 Color: 339

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 12669 Color: 488
Size: 3104 Color: 315
Size: 608 Color: 120

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 12677 Color: 489
Size: 3704 Color: 333

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 12965 Color: 501
Size: 3416 Color: 325

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 13257 Color: 512
Size: 3124 Color: 317

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 14151 Color: 558
Size: 2230 Color: 268

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 592
Size: 1733 Color: 232

Bin 83: 4 of cap free
Amount of items: 2
Items: 
Size: 11748 Color: 459
Size: 4632 Color: 362

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 499
Size: 3444 Color: 327

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 527
Size: 2820 Color: 302

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 14177 Color: 560
Size: 2203 Color: 266

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 573
Size: 2036 Color: 256

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 14429 Color: 578
Size: 1951 Color: 250

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 585
Size: 1848 Color: 242

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14676 Color: 596
Size: 1704 Color: 230

Bin 91: 5 of cap free
Amount of items: 5
Items: 
Size: 10696 Color: 437
Size: 2517 Color: 287
Size: 1998 Color: 253
Size: 864 Color: 160
Size: 304 Color: 26

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 13718 Color: 535
Size: 2661 Color: 295

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 13811 Color: 539
Size: 2568 Color: 289

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 13899 Color: 544
Size: 2480 Color: 285

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 422
Size: 6712 Color: 394
Size: 350 Color: 47

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 13032 Color: 503
Size: 3346 Color: 324

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 13225 Color: 511
Size: 3153 Color: 319

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 547
Size: 2430 Color: 282

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 14305 Color: 571
Size: 2073 Color: 260

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 14504 Color: 584
Size: 1874 Color: 245

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 10154 Color: 431
Size: 5911 Color: 386
Size: 312 Color: 33

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 11128 Color: 447
Size: 5249 Color: 376

Bin 103: 7 of cap free
Amount of items: 4
Items: 
Size: 12072 Color: 473
Size: 4171 Color: 351
Size: 70 Color: 1
Size: 64 Color: 0

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 487
Size: 3733 Color: 336

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 549
Size: 2387 Color: 279

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 553
Size: 2305 Color: 275

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 14606 Color: 589
Size: 1771 Color: 238

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 11301 Color: 453
Size: 4791 Color: 367
Size: 284 Color: 16

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 528
Size: 2772 Color: 300

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 556
Size: 2238 Color: 269

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 14246 Color: 564
Size: 2130 Color: 262

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 14332 Color: 572
Size: 2044 Color: 257

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 14434 Color: 579
Size: 1942 Color: 249

Bin 114: 9 of cap free
Amount of items: 3
Items: 
Size: 11206 Color: 449
Size: 3741 Color: 337
Size: 1428 Color: 206

Bin 115: 10 of cap free
Amount of items: 7
Items: 
Size: 8200 Color: 409
Size: 1640 Color: 225
Size: 1626 Color: 224
Size: 1588 Color: 221
Size: 1528 Color: 216
Size: 1408 Color: 204
Size: 384 Color: 59

Bin 116: 10 of cap free
Amount of items: 2
Items: 
Size: 13741 Color: 536
Size: 2633 Color: 293

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 14084 Color: 554
Size: 2290 Color: 274

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 575
Size: 2016 Color: 255

Bin 119: 11 of cap free
Amount of items: 3
Items: 
Size: 10087 Color: 430
Size: 5966 Color: 387
Size: 320 Color: 34

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 537
Size: 2607 Color: 292

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 14202 Color: 562
Size: 2171 Color: 265

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 11676 Color: 456
Size: 4696 Color: 365

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 13848 Color: 542
Size: 2524 Color: 288

Bin 124: 13 of cap free
Amount of items: 2
Items: 
Size: 14103 Color: 555
Size: 2268 Color: 273

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 14300 Color: 570
Size: 2071 Color: 259

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 531
Size: 2702 Color: 297

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 557
Size: 2222 Color: 267

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 14388 Color: 576
Size: 1982 Color: 252

Bin 129: 15 of cap free
Amount of items: 2
Items: 
Size: 14045 Color: 552
Size: 2324 Color: 276

Bin 130: 16 of cap free
Amount of items: 7
Items: 
Size: 8202 Color: 410
Size: 1841 Color: 241
Size: 1753 Color: 236
Size: 1694 Color: 229
Size: 1668 Color: 226
Size: 834 Color: 155
Size: 376 Color: 57

Bin 131: 16 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 436
Size: 5768 Color: 382
Size: 304 Color: 27

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 11818 Color: 463
Size: 4550 Color: 361

Bin 133: 17 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 471
Size: 4310 Color: 355
Size: 144 Color: 5

Bin 134: 17 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 517
Size: 3002 Color: 310

Bin 135: 18 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 496
Size: 3548 Color: 331

Bin 136: 18 of cap free
Amount of items: 2
Items: 
Size: 14221 Color: 563
Size: 2145 Color: 264

Bin 137: 20 of cap free
Amount of items: 3
Items: 
Size: 11700 Color: 458
Size: 4392 Color: 358
Size: 272 Color: 12

Bin 138: 20 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 483
Size: 3924 Color: 349

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 505
Size: 3288 Color: 321

Bin 140: 21 of cap free
Amount of items: 2
Items: 
Size: 13779 Color: 538
Size: 2584 Color: 291

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 13445 Color: 521
Size: 2917 Color: 306

Bin 142: 23 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 498
Size: 3476 Color: 329

Bin 143: 24 of cap free
Amount of items: 3
Items: 
Size: 9544 Color: 426
Size: 6480 Color: 393
Size: 336 Color: 40

Bin 144: 24 of cap free
Amount of items: 3
Items: 
Size: 11830 Color: 464
Size: 4290 Color: 353
Size: 240 Color: 11

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 534
Size: 2650 Color: 294

Bin 146: 25 of cap free
Amount of items: 3
Items: 
Size: 10701 Color: 438
Size: 3730 Color: 335
Size: 1928 Color: 248

Bin 147: 25 of cap free
Amount of items: 2
Items: 
Size: 13551 Color: 526
Size: 2808 Color: 301

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 13364 Color: 516
Size: 2994 Color: 309

Bin 149: 27 of cap free
Amount of items: 2
Items: 
Size: 11905 Color: 469
Size: 4452 Color: 360

Bin 150: 28 of cap free
Amount of items: 3
Items: 
Size: 11910 Color: 470
Size: 4302 Color: 354
Size: 144 Color: 6

Bin 151: 28 of cap free
Amount of items: 2
Items: 
Size: 12488 Color: 484
Size: 3868 Color: 346

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 513
Size: 3068 Color: 312

Bin 153: 31 of cap free
Amount of items: 2
Items: 
Size: 12521 Color: 485
Size: 3832 Color: 345

Bin 154: 32 of cap free
Amount of items: 3
Items: 
Size: 9225 Color: 418
Size: 6775 Color: 395
Size: 352 Color: 48

Bin 155: 34 of cap free
Amount of items: 2
Items: 
Size: 13206 Color: 510
Size: 3144 Color: 318

Bin 156: 36 of cap free
Amount of items: 3
Items: 
Size: 10781 Color: 441
Size: 5271 Color: 377
Size: 296 Color: 24

Bin 157: 36 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 446
Size: 5000 Color: 369
Size: 288 Color: 20

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 11222 Color: 451
Size: 5124 Color: 374

Bin 159: 39 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 502
Size: 3341 Color: 323

Bin 160: 41 of cap free
Amount of items: 11
Items: 
Size: 8193 Color: 405
Size: 1048 Color: 179
Size: 1040 Color: 178
Size: 1016 Color: 175
Size: 1016 Color: 174
Size: 1008 Color: 173
Size: 1008 Color: 172
Size: 688 Color: 135
Size: 448 Color: 81
Size: 444 Color: 79
Size: 434 Color: 76

Bin 161: 42 of cap free
Amount of items: 3
Items: 
Size: 11144 Color: 448
Size: 4914 Color: 368
Size: 284 Color: 18

Bin 162: 42 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 472
Size: 4318 Color: 356
Size: 80 Color: 2

Bin 163: 43 of cap free
Amount of items: 2
Items: 
Size: 11897 Color: 468
Size: 4444 Color: 359

Bin 164: 50 of cap free
Amount of items: 3
Items: 
Size: 10292 Color: 435
Size: 5738 Color: 381
Size: 304 Color: 29

Bin 165: 52 of cap free
Amount of items: 2
Items: 
Size: 11044 Color: 445
Size: 5288 Color: 378

Bin 166: 52 of cap free
Amount of items: 3
Items: 
Size: 11684 Color: 457
Size: 4376 Color: 357
Size: 272 Color: 13

Bin 167: 54 of cap free
Amount of items: 3
Items: 
Size: 11381 Color: 455
Size: 4677 Color: 364
Size: 272 Color: 14

Bin 168: 55 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 474
Size: 4177 Color: 352

Bin 169: 58 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 442
Size: 5194 Color: 375
Size: 292 Color: 23

Bin 170: 60 of cap free
Amount of items: 3
Items: 
Size: 11373 Color: 454
Size: 4671 Color: 363
Size: 280 Color: 15

Bin 171: 62 of cap free
Amount of items: 3
Items: 
Size: 9145 Color: 416
Size: 6825 Color: 398
Size: 352 Color: 49

Bin 172: 62 of cap free
Amount of items: 3
Items: 
Size: 10942 Color: 444
Size: 5092 Color: 372
Size: 288 Color: 21

Bin 173: 63 of cap free
Amount of items: 2
Items: 
Size: 9217 Color: 417
Size: 7104 Color: 402

Bin 174: 68 of cap free
Amount of items: 2
Items: 
Size: 11214 Color: 450
Size: 5102 Color: 373

Bin 175: 72 of cap free
Amount of items: 8
Items: 
Size: 8196 Color: 408
Size: 1522 Color: 214
Size: 1516 Color: 213
Size: 1508 Color: 212
Size: 1426 Color: 205
Size: 1364 Color: 201
Size: 392 Color: 62
Size: 388 Color: 61

Bin 176: 84 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 425
Size: 6462 Color: 392
Size: 336 Color: 41

Bin 177: 85 of cap free
Amount of items: 2
Items: 
Size: 10266 Color: 433
Size: 6033 Color: 389

Bin 178: 86 of cap free
Amount of items: 10
Items: 
Size: 8194 Color: 406
Size: 1206 Color: 191
Size: 1152 Color: 185
Size: 1144 Color: 184
Size: 1136 Color: 183
Size: 1124 Color: 182
Size: 1088 Color: 181
Size: 424 Color: 72
Size: 416 Color: 70
Size: 414 Color: 69

Bin 179: 86 of cap free
Amount of items: 3
Items: 
Size: 10926 Color: 443
Size: 5084 Color: 371
Size: 288 Color: 22

Bin 180: 91 of cap free
Amount of items: 3
Items: 
Size: 10061 Color: 429
Size: 5908 Color: 385
Size: 324 Color: 36

Bin 181: 92 of cap free
Amount of items: 4
Items: 
Size: 8760 Color: 415
Size: 6824 Color: 397
Size: 356 Color: 51
Size: 352 Color: 50

Bin 182: 92 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 434
Size: 5704 Color: 380
Size: 312 Color: 31

Bin 183: 96 of cap free
Amount of items: 2
Items: 
Size: 10760 Color: 439
Size: 5528 Color: 379

Bin 184: 100 of cap free
Amount of items: 2
Items: 
Size: 8204 Color: 411
Size: 8080 Color: 404

Bin 185: 104 of cap free
Amount of items: 3
Items: 
Size: 9768 Color: 427
Size: 6176 Color: 390
Size: 336 Color: 39

Bin 186: 108 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 428
Size: 5892 Color: 384
Size: 328 Color: 37

Bin 187: 114 of cap free
Amount of items: 6
Items: 
Size: 8344 Color: 412
Size: 1924 Color: 247
Size: 1901 Color: 246
Size: 1868 Color: 244
Size: 1861 Color: 243
Size: 372 Color: 56

Bin 188: 125 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 452
Size: 4737 Color: 366
Size: 284 Color: 17

Bin 189: 140 of cap free
Amount of items: 21
Items: 
Size: 946 Color: 170
Size: 934 Color: 169
Size: 932 Color: 168
Size: 928 Color: 167
Size: 908 Color: 165
Size: 888 Color: 164
Size: 880 Color: 163
Size: 880 Color: 162
Size: 860 Color: 159
Size: 860 Color: 158
Size: 860 Color: 157
Size: 856 Color: 156
Size: 832 Color: 154
Size: 784 Color: 153
Size: 776 Color: 152
Size: 764 Color: 149
Size: 496 Color: 96
Size: 480 Color: 88
Size: 464 Color: 86
Size: 460 Color: 85
Size: 456 Color: 84

Bin 190: 143 of cap free
Amount of items: 2
Items: 
Size: 10236 Color: 432
Size: 6005 Color: 388

Bin 191: 192 of cap free
Amount of items: 4
Items: 
Size: 8634 Color: 414
Size: 6820 Color: 396
Size: 372 Color: 55
Size: 366 Color: 52

Bin 192: 224 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 424
Size: 6360 Color: 391
Size: 336 Color: 42

Bin 193: 227 of cap free
Amount of items: 3
Items: 
Size: 10773 Color: 440
Size: 5080 Color: 370
Size: 304 Color: 25

Bin 194: 238 of cap free
Amount of items: 2
Items: 
Size: 8624 Color: 413
Size: 7522 Color: 403

Bin 195: 250 of cap free
Amount of items: 26
Items: 
Size: 760 Color: 147
Size: 750 Color: 143
Size: 746 Color: 140
Size: 744 Color: 139
Size: 736 Color: 137
Size: 688 Color: 134
Size: 688 Color: 133
Size: 672 Color: 131
Size: 672 Color: 130
Size: 668 Color: 129
Size: 664 Color: 128
Size: 660 Color: 127
Size: 656 Color: 126
Size: 592 Color: 114
Size: 570 Color: 112
Size: 568 Color: 111
Size: 560 Color: 110
Size: 552 Color: 108
Size: 544 Color: 107
Size: 536 Color: 105
Size: 528 Color: 103
Size: 528 Color: 102
Size: 526 Color: 101
Size: 512 Color: 99
Size: 512 Color: 98
Size: 502 Color: 97

Bin 196: 256 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 421
Size: 6828 Color: 401

Bin 197: 263 of cap free
Amount of items: 2
Items: 
Size: 9294 Color: 420
Size: 6827 Color: 400

Bin 198: 265 of cap free
Amount of items: 2
Items: 
Size: 9293 Color: 419
Size: 6826 Color: 399

Bin 199: 10866 of cap free
Amount of items: 9
Items: 
Size: 640 Color: 125
Size: 640 Color: 124
Size: 618 Color: 123
Size: 616 Color: 122
Size: 616 Color: 121
Size: 600 Color: 118
Size: 596 Color: 117
Size: 596 Color: 116
Size: 596 Color: 115

Total size: 3244032
Total free space: 16384

