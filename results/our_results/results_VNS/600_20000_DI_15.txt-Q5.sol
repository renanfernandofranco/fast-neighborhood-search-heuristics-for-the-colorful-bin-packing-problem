Capicity Bin: 16384
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8200 Color: 0
Size: 2290 Color: 3
Size: 2265 Color: 4
Size: 2203 Color: 0
Size: 1426 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9364 Color: 1
Size: 5852 Color: 2
Size: 1168 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10154 Color: 1
Size: 5704 Color: 3
Size: 526 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 1
Size: 4444 Color: 3
Size: 1180 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 2
Size: 5000 Color: 3
Size: 544 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11214 Color: 0
Size: 4290 Color: 3
Size: 880 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 1
Size: 3821 Color: 4
Size: 768 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11910 Color: 3
Size: 3806 Color: 1
Size: 668 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11881 Color: 0
Size: 3753 Color: 3
Size: 750 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 0
Size: 3832 Color: 3
Size: 400 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12220 Color: 4
Size: 3730 Color: 2
Size: 434 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12386 Color: 3
Size: 3444 Color: 2
Size: 554 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 0
Size: 3868 Color: 0
Size: 240 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 2
Size: 3500 Color: 3
Size: 396 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 2
Size: 3416 Color: 3
Size: 324 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12677 Color: 1
Size: 3341 Color: 3
Size: 366 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12818 Color: 2
Size: 2974 Color: 2
Size: 592 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 3
Size: 2702 Color: 0
Size: 746 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 1
Size: 2516 Color: 2
Size: 864 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 4
Size: 2808 Color: 3
Size: 288 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 3
Size: 2324 Color: 4
Size: 688 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 2
Size: 2568 Color: 3
Size: 452 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 3
Size: 2430 Color: 0
Size: 528 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13434 Color: 3
Size: 2422 Color: 0
Size: 528 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 1
Size: 2036 Color: 1
Size: 744 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 3
Size: 2260 Color: 0
Size: 456 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 3
Size: 2238 Color: 1
Size: 444 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13718 Color: 1
Size: 1690 Color: 4
Size: 976 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13766 Color: 1
Size: 1868 Color: 0
Size: 750 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 0
Size: 1901 Color: 4
Size: 672 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 3
Size: 1924 Color: 0
Size: 640 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 0
Size: 2140 Color: 3
Size: 396 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13899 Color: 1
Size: 1733 Color: 3
Size: 752 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13948 Color: 1
Size: 1576 Color: 2
Size: 860 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13979 Color: 2
Size: 2005 Color: 3
Size: 400 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14006 Color: 2
Size: 1998 Color: 3
Size: 380 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14045 Color: 2
Size: 1951 Color: 3
Size: 388 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 3
Size: 1548 Color: 2
Size: 764 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14103 Color: 1
Size: 1841 Color: 1
Size: 440 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14138 Color: 0
Size: 1694 Color: 4
Size: 552 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14151 Color: 0
Size: 1861 Color: 3
Size: 372 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1328 Color: 1
Size: 888 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 1
Size: 1448 Color: 3
Size: 736 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14246 Color: 4
Size: 1626 Color: 0
Size: 512 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14261 Color: 0
Size: 1771 Color: 3
Size: 352 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14281 Color: 2
Size: 1753 Color: 3
Size: 350 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14300 Color: 2
Size: 1152 Color: 3
Size: 932 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 1
Size: 1668 Color: 4
Size: 384 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 2
Size: 1624 Color: 2
Size: 416 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 1
Size: 1582 Color: 2
Size: 444 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14434 Color: 3
Size: 1382 Color: 2
Size: 568 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14468 Color: 3
Size: 1052 Color: 2
Size: 864 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 2
Size: 1604 Color: 0
Size: 296 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 0
Size: 1144 Color: 1
Size: 746 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 0
Size: 1360 Color: 3
Size: 520 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 3
Size: 1088 Color: 1
Size: 680 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14620 Color: 3
Size: 1476 Color: 2
Size: 288 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14558 Color: 2
Size: 1522 Color: 1
Size: 304 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14648 Color: 3
Size: 1048 Color: 0
Size: 688 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14660 Color: 3
Size: 1124 Color: 1
Size: 600 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 3
Size: 1364 Color: 4
Size: 346 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 3
Size: 1280 Color: 1
Size: 408 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 1
Size: 1264 Color: 3
Size: 444 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14708 Color: 2
Size: 1016 Color: 3
Size: 660 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14730 Color: 2
Size: 1162 Color: 4
Size: 492 Color: 3

Bin 66: 1 of cap free
Amount of items: 7
Items: 
Size: 8194 Color: 2
Size: 2248 Color: 0
Size: 1942 Color: 3
Size: 1740 Color: 4
Size: 1671 Color: 1
Size: 304 Color: 2
Size: 284 Color: 4

Bin 67: 1 of cap free
Amount of items: 5
Items: 
Size: 8204 Color: 4
Size: 4737 Color: 1
Size: 2524 Color: 1
Size: 582 Color: 2
Size: 336 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 10061 Color: 4
Size: 5908 Color: 0
Size: 414 Color: 4

Bin 69: 1 of cap free
Amount of items: 4
Items: 
Size: 10701 Color: 0
Size: 5194 Color: 2
Size: 272 Color: 3
Size: 216 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 3
Size: 4791 Color: 0
Size: 464 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 3
Size: 3727 Color: 0
Size: 762 Color: 0

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12802 Color: 4
Size: 3097 Color: 3
Size: 484 Color: 4

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 1
Size: 2763 Color: 3
Size: 560 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13146 Color: 2
Size: 2917 Color: 3
Size: 320 Color: 1

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13206 Color: 1
Size: 2607 Color: 3
Size: 570 Color: 4

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13257 Color: 1
Size: 2694 Color: 3
Size: 432 Color: 0

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 13296 Color: 0
Size: 3087 Color: 4

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13445 Color: 4
Size: 2650 Color: 4
Size: 288 Color: 3

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13521 Color: 1
Size: 2222 Color: 0
Size: 640 Color: 3

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13551 Color: 0
Size: 2056 Color: 0
Size: 776 Color: 3

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 13619 Color: 0
Size: 2764 Color: 1

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13667 Color: 4
Size: 2044 Color: 4
Size: 672 Color: 3

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 14221 Color: 3
Size: 1770 Color: 1
Size: 392 Color: 1

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 14291 Color: 3
Size: 1428 Color: 1
Size: 664 Color: 2

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 14429 Color: 4
Size: 1206 Color: 3
Size: 748 Color: 1

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 0
Size: 6480 Color: 0
Size: 400 Color: 3

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 4
Size: 5738 Color: 3
Size: 368 Color: 4

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 11381 Color: 0
Size: 3825 Color: 3
Size: 1176 Color: 2

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 11676 Color: 0
Size: 3346 Color: 3
Size: 1360 Color: 1

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 4
Size: 4310 Color: 3
Size: 272 Color: 0

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11818 Color: 4
Size: 3908 Color: 3
Size: 656 Color: 0

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 3
Size: 4177 Color: 1
Size: 292 Color: 1

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 3
Size: 3798 Color: 2
Size: 512 Color: 4

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 3
Size: 3124 Color: 0
Size: 484 Color: 2

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 4
Size: 2576 Color: 0
Size: 608 Color: 3

Bin 96: 2 of cap free
Amount of items: 3
Items: 
Size: 13478 Color: 1
Size: 2480 Color: 1
Size: 424 Color: 3

Bin 97: 2 of cap free
Amount of items: 3
Items: 
Size: 13741 Color: 3
Size: 2305 Color: 2
Size: 336 Color: 4

Bin 98: 2 of cap free
Amount of items: 3
Items: 
Size: 13932 Color: 0
Size: 2360 Color: 4
Size: 90 Color: 4

Bin 99: 2 of cap free
Amount of items: 2
Items: 
Size: 14262 Color: 1
Size: 2120 Color: 0

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 9225 Color: 4
Size: 6820 Color: 4
Size: 336 Color: 3

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 9294 Color: 4
Size: 6775 Color: 3
Size: 312 Color: 2

Bin 102: 3 of cap free
Amount of items: 3
Items: 
Size: 10773 Color: 2
Size: 5288 Color: 1
Size: 320 Color: 3

Bin 103: 3 of cap free
Amount of items: 3
Items: 
Size: 11373 Color: 3
Size: 4696 Color: 4
Size: 312 Color: 2

Bin 104: 3 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 3
Size: 3256 Color: 1
Size: 880 Color: 0

Bin 105: 3 of cap free
Amount of items: 3
Items: 
Size: 12521 Color: 2
Size: 3104 Color: 3
Size: 756 Color: 4

Bin 106: 3 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 4
Size: 2851 Color: 3
Size: 744 Color: 0

Bin 107: 3 of cap free
Amount of items: 3
Items: 
Size: 12957 Color: 3
Size: 3068 Color: 2
Size: 356 Color: 1

Bin 108: 3 of cap free
Amount of items: 3
Items: 
Size: 12965 Color: 0
Size: 2820 Color: 4
Size: 596 Color: 3

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 0
Size: 6712 Color: 3
Size: 352 Color: 1

Bin 110: 4 of cap free
Amount of items: 3
Items: 
Size: 10087 Color: 1
Size: 6005 Color: 3
Size: 288 Color: 1

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 11830 Color: 0
Size: 4550 Color: 1

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 11905 Color: 0
Size: 4171 Color: 3
Size: 304 Color: 2

Bin 113: 4 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 2
Size: 3912 Color: 0
Size: 208 Color: 2

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 12375 Color: 1
Size: 3733 Color: 3
Size: 272 Color: 1

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 4
Size: 3742 Color: 0
Size: 240 Color: 3

Bin 116: 4 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 4
Size: 1848 Color: 2

Bin 117: 5 of cap free
Amount of items: 3
Items: 
Size: 11222 Color: 0
Size: 2986 Color: 1
Size: 2171 Color: 3

Bin 118: 5 of cap free
Amount of items: 3
Items: 
Size: 14554 Color: 1
Size: 1745 Color: 4
Size: 80 Color: 4

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 1
Size: 2230 Color: 0

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 14305 Color: 2
Size: 2073 Color: 4

Bin 121: 7 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 2
Size: 2387 Color: 1

Bin 122: 7 of cap free
Amount of items: 3
Items: 
Size: 14177 Color: 0
Size: 2130 Color: 1
Size: 70 Color: 2

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 3
Size: 5768 Color: 1
Size: 312 Color: 2

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 3
Size: 5084 Color: 4
Size: 596 Color: 1

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 1
Size: 5092 Color: 2
Size: 224 Color: 3

Bin 126: 9 of cap free
Amount of items: 7
Items: 
Size: 8195 Color: 1
Size: 1578 Color: 0
Size: 1528 Color: 0
Size: 1526 Color: 3
Size: 1516 Color: 2
Size: 1024 Color: 4
Size: 1008 Color: 2

Bin 127: 9 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 2
Size: 3439 Color: 4
Size: 96 Color: 4

Bin 128: 9 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 1
Size: 3010 Color: 0

Bin 129: 10 of cap free
Amount of items: 3
Items: 
Size: 10942 Color: 4
Size: 5080 Color: 3
Size: 352 Color: 2

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 11144 Color: 4
Size: 4318 Color: 3
Size: 912 Color: 1

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 4
Size: 2352 Color: 1

Bin 132: 11 of cap free
Amount of items: 3
Items: 
Size: 9293 Color: 3
Size: 6462 Color: 1
Size: 618 Color: 1

Bin 133: 11 of cap free
Amount of items: 2
Items: 
Size: 12669 Color: 2
Size: 3704 Color: 1

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 9544 Color: 4
Size: 6828 Color: 0

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 11700 Color: 0
Size: 4671 Color: 4

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 9768 Color: 3
Size: 4632 Color: 0
Size: 1970 Color: 4

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 14354 Color: 1
Size: 2016 Color: 4

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 14388 Color: 2
Size: 1982 Color: 1

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 4
Size: 1704 Color: 0

Bin 140: 15 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 0
Size: 6825 Color: 4
Size: 784 Color: 2

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 12628 Color: 0
Size: 3741 Color: 4

Bin 142: 15 of cap free
Amount of items: 3
Items: 
Size: 13193 Color: 0
Size: 3112 Color: 4
Size: 64 Color: 4

Bin 143: 15 of cap free
Amount of items: 2
Items: 
Size: 13225 Color: 2
Size: 3144 Color: 4

Bin 144: 16 of cap free
Amount of items: 3
Items: 
Size: 8344 Color: 1
Size: 7522 Color: 3
Size: 502 Color: 4

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 4
Size: 2071 Color: 2

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 13032 Color: 4
Size: 3334 Color: 0

Bin 147: 19 of cap free
Amount of items: 7
Items: 
Size: 8193 Color: 2
Size: 1508 Color: 0
Size: 1482 Color: 1
Size: 1480 Color: 3
Size: 1434 Color: 3
Size: 1408 Color: 0
Size: 860 Color: 0

Bin 148: 20 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 0
Size: 3924 Color: 1
Size: 144 Color: 3

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 2
Size: 3924 Color: 4

Bin 150: 20 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 4
Size: 3288 Color: 2

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 14490 Color: 4
Size: 1874 Color: 1

Bin 152: 20 of cap free
Amount of items: 2
Items: 
Size: 14724 Color: 2
Size: 1640 Color: 0

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 13779 Color: 0
Size: 2584 Color: 1

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 11238 Color: 4
Size: 5124 Color: 1

Bin 155: 23 of cap free
Amount of items: 2
Items: 
Size: 11684 Color: 4
Size: 4677 Color: 0

Bin 156: 26 of cap free
Amount of items: 3
Items: 
Size: 8634 Color: 0
Size: 6360 Color: 1
Size: 1364 Color: 3

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 14606 Color: 1
Size: 1752 Color: 0

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 14084 Color: 1
Size: 2268 Color: 2

Bin 159: 32 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 2
Size: 1928 Color: 1

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 1
Size: 1782 Color: 2

Bin 161: 35 of cap free
Amount of items: 2
Items: 
Size: 11897 Color: 0
Size: 4452 Color: 1

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 13688 Color: 2
Size: 2661 Color: 4

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 4
Size: 2862 Color: 0

Bin 164: 37 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 1
Size: 2517 Color: 0

Bin 165: 37 of cap free
Amount of items: 2
Items: 
Size: 14202 Color: 2
Size: 2145 Color: 0

Bin 166: 38 of cap free
Amount of items: 2
Items: 
Size: 13884 Color: 1
Size: 2462 Color: 0

Bin 167: 38 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 0
Size: 2418 Color: 3

Bin 168: 41 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 4
Size: 2633 Color: 2

Bin 169: 42 of cap free
Amount of items: 2
Items: 
Size: 12794 Color: 4
Size: 3548 Color: 1

Bin 170: 45 of cap free
Amount of items: 3
Items: 
Size: 9145 Color: 2
Size: 6826 Color: 4
Size: 368 Color: 3

Bin 171: 47 of cap free
Amount of items: 2
Items: 
Size: 13184 Color: 2
Size: 3153 Color: 3

Bin 172: 48 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 2
Size: 3451 Color: 0

Bin 173: 52 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 4
Size: 2772 Color: 0

Bin 174: 54 of cap free
Amount of items: 3
Items: 
Size: 11748 Color: 3
Size: 2994 Color: 0
Size: 1588 Color: 4

Bin 175: 56 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 4
Size: 3476 Color: 2
Size: 144 Color: 0

Bin 176: 57 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 0
Size: 2857 Color: 1

Bin 177: 59 of cap free
Amount of items: 2
Items: 
Size: 10292 Color: 2
Size: 6033 Color: 0

Bin 178: 62 of cap free
Amount of items: 2
Items: 
Size: 13320 Color: 0
Size: 3002 Color: 2

Bin 179: 63 of cap free
Amount of items: 2
Items: 
Size: 9217 Color: 4
Size: 7104 Color: 1

Bin 180: 69 of cap free
Amount of items: 2
Items: 
Size: 11044 Color: 1
Size: 5271 Color: 4

Bin 181: 75 of cap free
Amount of items: 2
Items: 
Size: 10781 Color: 4
Size: 5528 Color: 0

Bin 182: 76 of cap free
Amount of items: 2
Items: 
Size: 11206 Color: 0
Size: 5102 Color: 2

Bin 183: 95 of cap free
Amount of items: 2
Items: 
Size: 12681 Color: 3
Size: 3608 Color: 4

Bin 184: 96 of cap free
Amount of items: 2
Items: 
Size: 9464 Color: 4
Size: 6824 Color: 2

Bin 185: 108 of cap free
Amount of items: 2
Items: 
Size: 8196 Color: 3
Size: 8080 Color: 2

Bin 186: 119 of cap free
Amount of items: 2
Items: 
Size: 11873 Color: 4
Size: 4392 Color: 1

Bin 187: 136 of cap free
Amount of items: 15
Items: 
Size: 1404 Color: 4
Size: 1364 Color: 3
Size: 1360 Color: 1
Size: 1288 Color: 2
Size: 1176 Color: 4
Size: 1136 Color: 0
Size: 1040 Color: 1
Size: 1036 Color: 3
Size: 1016 Color: 1
Size: 1008 Color: 1
Size: 946 Color: 3
Size: 934 Color: 0
Size: 908 Color: 2
Size: 856 Color: 2
Size: 776 Color: 2

Bin 188: 138 of cap free
Amount of items: 4
Items: 
Size: 8202 Color: 2
Size: 3761 Color: 3
Size: 2451 Color: 0
Size: 1832 Color: 4

Bin 189: 138 of cap free
Amount of items: 2
Items: 
Size: 11944 Color: 1
Size: 4302 Color: 4

Bin 190: 152 of cap free
Amount of items: 3
Items: 
Size: 8624 Color: 2
Size: 5892 Color: 1
Size: 1716 Color: 3

Bin 191: 152 of cap free
Amount of items: 2
Items: 
Size: 10056 Color: 3
Size: 6176 Color: 4

Bin 192: 152 of cap free
Amount of items: 2
Items: 
Size: 10266 Color: 1
Size: 5966 Color: 2

Bin 193: 168 of cap free
Amount of items: 29
Items: 
Size: 928 Color: 3
Size: 860 Color: 0
Size: 834 Color: 3
Size: 832 Color: 4
Size: 760 Color: 3
Size: 704 Color: 1
Size: 688 Color: 0
Size: 616 Color: 3
Size: 616 Color: 3
Size: 608 Color: 1
Size: 596 Color: 0
Size: 544 Color: 2
Size: 536 Color: 1
Size: 530 Color: 4
Size: 496 Color: 4
Size: 496 Color: 2
Size: 496 Color: 0
Size: 488 Color: 4
Size: 488 Color: 1
Size: 480 Color: 3
Size: 476 Color: 0
Size: 460 Color: 0
Size: 448 Color: 2
Size: 424 Color: 2
Size: 384 Color: 2
Size: 376 Color: 2
Size: 372 Color: 2
Size: 344 Color: 3
Size: 336 Color: 3

Bin 194: 169 of cap free
Amount of items: 2
Items: 
Size: 11301 Color: 2
Size: 4914 Color: 1

Bin 195: 207 of cap free
Amount of items: 2
Items: 
Size: 11801 Color: 0
Size: 4376 Color: 1

Bin 196: 209 of cap free
Amount of items: 2
Items: 
Size: 10926 Color: 1
Size: 5249 Color: 4

Bin 197: 237 of cap free
Amount of items: 2
Items: 
Size: 10236 Color: 1
Size: 5911 Color: 2

Bin 198: 257 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 1
Size: 6827 Color: 4

Bin 199: 11976 of cap free
Amount of items: 13
Items: 
Size: 448 Color: 1
Size: 428 Color: 4
Size: 424 Color: 0
Size: 348 Color: 4
Size: 336 Color: 3
Size: 328 Color: 4
Size: 328 Color: 3
Size: 312 Color: 0
Size: 304 Color: 3
Size: 304 Color: 2
Size: 284 Color: 2
Size: 284 Color: 1
Size: 280 Color: 2

Total size: 3244032
Total free space: 16384

