Capicity Bin: 16336
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 460
Size: 4276 Color: 354
Size: 260 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 472
Size: 2564 Color: 298
Size: 1352 Color: 198

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12558 Color: 480
Size: 2168 Color: 274
Size: 1610 Color: 227

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12855 Color: 485
Size: 2781 Color: 312
Size: 700 Color: 141

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 486
Size: 1898 Color: 252
Size: 1580 Color: 223

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 487
Size: 1782 Color: 242
Size: 1682 Color: 234

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 490
Size: 2810 Color: 314
Size: 554 Color: 118

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13001 Color: 491
Size: 2399 Color: 289
Size: 936 Color: 171

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 493
Size: 2738 Color: 308
Size: 560 Color: 119

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 499
Size: 1716 Color: 236
Size: 1460 Color: 211

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 502
Size: 2648 Color: 302
Size: 432 Color: 85

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13268 Color: 504
Size: 2660 Color: 303
Size: 408 Color: 74

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13277 Color: 505
Size: 2711 Color: 306
Size: 348 Color: 49

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 506
Size: 1672 Color: 231
Size: 1380 Color: 204

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 508
Size: 1834 Color: 246
Size: 1168 Color: 187

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13396 Color: 512
Size: 2364 Color: 285
Size: 576 Color: 124

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13442 Color: 514
Size: 2414 Color: 290
Size: 480 Color: 99

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13479 Color: 517
Size: 2381 Color: 287
Size: 476 Color: 96

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 518
Size: 1528 Color: 218
Size: 1328 Color: 193

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13630 Color: 524
Size: 2158 Color: 273
Size: 548 Color: 116

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 528
Size: 1848 Color: 247
Size: 752 Color: 151

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 533
Size: 1448 Color: 209
Size: 1072 Color: 179

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13824 Color: 534
Size: 2288 Color: 280
Size: 224 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13850 Color: 538
Size: 1696 Color: 235
Size: 790 Color: 157

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13977 Color: 543
Size: 1905 Color: 254
Size: 454 Color: 90

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 548
Size: 1908 Color: 255
Size: 376 Color: 66

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 552
Size: 1600 Color: 225
Size: 636 Color: 131

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 553
Size: 1414 Color: 206
Size: 812 Color: 158

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 554
Size: 1681 Color: 233
Size: 540 Color: 112

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 555
Size: 1352 Color: 199
Size: 864 Color: 163

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14138 Color: 556
Size: 1546 Color: 219
Size: 652 Color: 138

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14227 Color: 561
Size: 1749 Color: 239
Size: 360 Color: 55

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 567
Size: 1581 Color: 224
Size: 436 Color: 88

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 568
Size: 1416 Color: 207
Size: 600 Color: 128

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 569
Size: 1372 Color: 203
Size: 642 Color: 134

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 575
Size: 1468 Color: 212
Size: 464 Color: 94

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14410 Color: 577
Size: 1078 Color: 180
Size: 848 Color: 160

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14439 Color: 579
Size: 1473 Color: 213
Size: 424 Color: 80

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14444 Color: 580
Size: 1562 Color: 222
Size: 330 Color: 42

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 581
Size: 1557 Color: 221
Size: 310 Color: 32

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14511 Color: 584
Size: 1521 Color: 217
Size: 304 Color: 29

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 585
Size: 1360 Color: 201
Size: 462 Color: 92

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14561 Color: 588
Size: 1481 Color: 214
Size: 294 Color: 25

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 589
Size: 1494 Color: 216
Size: 280 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14588 Color: 592
Size: 1152 Color: 185
Size: 596 Color: 126

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 594
Size: 1264 Color: 191
Size: 472 Color: 95

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 595
Size: 1386 Color: 205
Size: 324 Color: 41

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14642 Color: 596
Size: 1344 Color: 196
Size: 350 Color: 51

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 597
Size: 1008 Color: 177
Size: 654 Color: 139

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14680 Color: 598
Size: 1160 Color: 186
Size: 496 Color: 102

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14692 Color: 600
Size: 1352 Color: 197
Size: 292 Color: 24

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 9181 Color: 419
Size: 6802 Color: 396
Size: 352 Color: 52

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 12481 Color: 478
Size: 3542 Color: 338
Size: 312 Color: 33

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 492
Size: 3302 Color: 334

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 501
Size: 2392 Color: 288
Size: 714 Color: 145

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13258 Color: 503
Size: 2077 Color: 266
Size: 1000 Color: 176

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 13389 Color: 511
Size: 2074 Color: 265
Size: 872 Color: 165

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 515
Size: 2888 Color: 318

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 516
Size: 2876 Color: 317

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13727 Color: 527
Size: 2258 Color: 277
Size: 350 Color: 50

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13839 Color: 536
Size: 1868 Color: 250
Size: 628 Color: 130

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 537
Size: 2490 Color: 294

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 14039 Color: 547
Size: 2296 Color: 281

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 558
Size: 2149 Color: 272

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 14239 Color: 563
Size: 2096 Color: 269

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 586
Size: 1815 Color: 245

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 599
Size: 1651 Color: 230

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 9234 Color: 421
Size: 6764 Color: 393
Size: 336 Color: 46

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 464
Size: 4374 Color: 359

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 12436 Color: 475
Size: 3898 Color: 346

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 488
Size: 3442 Color: 335

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 489
Size: 2901 Color: 319
Size: 480 Color: 98

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13081 Color: 496
Size: 3253 Color: 331

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 507
Size: 3018 Color: 323

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 539
Size: 2452 Color: 293

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13962 Color: 542
Size: 2372 Color: 286

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 546
Size: 2310 Color: 282

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14159 Color: 557
Size: 2175 Color: 275

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14202 Color: 560
Size: 2132 Color: 271

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 576
Size: 1928 Color: 257

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14476 Color: 582
Size: 1858 Color: 249

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 476
Size: 3533 Color: 337
Size: 344 Color: 48

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13782 Color: 532
Size: 2551 Color: 297

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14058 Color: 549
Size: 2275 Color: 279

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 564
Size: 2081 Color: 267

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 573
Size: 1955 Color: 259

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 14482 Color: 583
Size: 1851 Color: 248

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 445
Size: 5016 Color: 367
Size: 288 Color: 19

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 11128 Color: 449
Size: 5204 Color: 371

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13104 Color: 498
Size: 3228 Color: 328

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 535
Size: 2504 Color: 295

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 12374 Color: 471
Size: 3957 Color: 348

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 551
Size: 2248 Color: 276

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 14391 Color: 574
Size: 1940 Color: 258

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 578
Size: 1915 Color: 256

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 14580 Color: 591
Size: 1751 Color: 240

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 11090 Color: 447
Size: 5240 Color: 372

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 12606 Color: 481
Size: 3724 Color: 343

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 559
Size: 2130 Color: 270

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 14284 Color: 566
Size: 2046 Color: 264

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 14546 Color: 587
Size: 1784 Color: 243

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 14590 Color: 593
Size: 1740 Color: 238

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 11978 Color: 465
Size: 4351 Color: 358

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 13508 Color: 520
Size: 2821 Color: 316

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 526
Size: 2673 Color: 304

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 13897 Color: 541
Size: 2432 Color: 292

Bin 107: 8 of cap free
Amount of items: 5
Items: 
Size: 8196 Color: 412
Size: 6728 Color: 392
Size: 660 Color: 140
Size: 374 Color: 63
Size: 370 Color: 62

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 525
Size: 2689 Color: 305

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 531
Size: 2548 Color: 296

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 14569 Color: 590
Size: 1759 Color: 241

Bin 111: 9 of cap free
Amount of items: 2
Items: 
Size: 13759 Color: 530
Size: 2568 Color: 300

Bin 112: 9 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 540
Size: 2431 Color: 291

Bin 113: 9 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 545
Size: 2315 Color: 283

Bin 114: 9 of cap free
Amount of items: 2
Items: 
Size: 14235 Color: 562
Size: 2092 Color: 268

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 9202 Color: 420
Size: 6788 Color: 395
Size: 336 Color: 47

Bin 116: 10 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 461
Size: 2804 Color: 313
Size: 1678 Color: 232

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 12753 Color: 483
Size: 3573 Color: 339

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 500
Size: 3150 Color: 325

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 570
Size: 2000 Color: 262

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 571
Size: 1982 Color: 261

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 463
Size: 4185 Color: 353
Size: 256 Color: 10

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 13085 Color: 497
Size: 3240 Color: 329

Bin 123: 13 of cap free
Amount of items: 3
Items: 
Size: 12129 Color: 470
Size: 4062 Color: 352
Size: 132 Color: 4

Bin 124: 13 of cap free
Amount of items: 2
Items: 
Size: 13336 Color: 509
Size: 2987 Color: 322

Bin 125: 13 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 550
Size: 2261 Color: 278

Bin 126: 14 of cap free
Amount of items: 9
Items: 
Size: 8170 Color: 406
Size: 1344 Color: 195
Size: 1344 Color: 194
Size: 1308 Color: 192
Size: 1216 Color: 190
Size: 1192 Color: 189
Size: 964 Color: 172
Size: 392 Color: 72
Size: 392 Color: 71

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 13054 Color: 495
Size: 3268 Color: 333

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 572
Size: 1967 Color: 260

Bin 129: 15 of cap free
Amount of items: 5
Items: 
Size: 8180 Color: 410
Size: 3187 Color: 326
Size: 3120 Color: 324
Size: 1458 Color: 210
Size: 376 Color: 64

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 13419 Color: 513
Size: 2902 Color: 320

Bin 131: 16 of cap free
Amount of items: 5
Items: 
Size: 8212 Color: 413
Size: 6772 Color: 394
Size: 600 Color: 127
Size: 368 Color: 61
Size: 368 Color: 60

Bin 132: 16 of cap free
Amount of items: 3
Items: 
Size: 9634 Color: 426
Size: 6354 Color: 388
Size: 332 Color: 43

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 13607 Color: 523
Size: 2713 Color: 307

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 13991 Color: 544
Size: 2329 Color: 284

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 14280 Color: 565
Size: 2040 Color: 263

Bin 136: 17 of cap free
Amount of items: 2
Items: 
Size: 13566 Color: 522
Size: 2753 Color: 311

Bin 137: 19 of cap free
Amount of items: 2
Items: 
Size: 11164 Color: 450
Size: 5153 Color: 370

Bin 138: 20 of cap free
Amount of items: 2
Items: 
Size: 13750 Color: 529
Size: 2566 Color: 299

Bin 139: 21 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 484
Size: 3507 Color: 336

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 442
Size: 5586 Color: 378

Bin 141: 23 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 455
Size: 3213 Color: 327
Size: 1556 Color: 220

Bin 142: 23 of cap free
Amount of items: 2
Items: 
Size: 12422 Color: 473
Size: 3891 Color: 345

Bin 143: 25 of cap free
Amount of items: 2
Items: 
Size: 13049 Color: 494
Size: 3262 Color: 332

Bin 144: 26 of cap free
Amount of items: 2
Items: 
Size: 12718 Color: 482
Size: 3592 Color: 340

Bin 145: 27 of cap free
Amount of items: 2
Items: 
Size: 13559 Color: 521
Size: 2750 Color: 310

Bin 146: 28 of cap free
Amount of items: 7
Items: 
Size: 8172 Color: 408
Size: 1794 Color: 244
Size: 1720 Color: 237
Size: 1634 Color: 229
Size: 1612 Color: 228
Size: 992 Color: 175
Size: 384 Color: 68

Bin 147: 29 of cap free
Amount of items: 2
Items: 
Size: 13492 Color: 519
Size: 2815 Color: 315

Bin 148: 33 of cap free
Amount of items: 3
Items: 
Size: 9895 Color: 433
Size: 6104 Color: 386
Size: 304 Color: 30

Bin 149: 34 of cap free
Amount of items: 2
Items: 
Size: 13350 Color: 510
Size: 2952 Color: 321

Bin 150: 35 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 431
Size: 6265 Color: 387
Size: 316 Color: 34

Bin 151: 39 of cap free
Amount of items: 2
Items: 
Size: 12513 Color: 479
Size: 3784 Color: 344

Bin 152: 40 of cap free
Amount of items: 2
Items: 
Size: 11868 Color: 462
Size: 4428 Color: 360

Bin 153: 46 of cap free
Amount of items: 3
Items: 
Size: 12090 Color: 468
Size: 4008 Color: 350
Size: 192 Color: 6

Bin 154: 49 of cap free
Amount of items: 4
Items: 
Size: 10328 Color: 437
Size: 5369 Color: 373
Size: 296 Color: 27
Size: 294 Color: 26

Bin 155: 51 of cap free
Amount of items: 2
Items: 
Size: 11462 Color: 454
Size: 4823 Color: 365

Bin 156: 51 of cap free
Amount of items: 3
Items: 
Size: 11669 Color: 459
Size: 4344 Color: 357
Size: 272 Color: 12

Bin 157: 56 of cap free
Amount of items: 2
Items: 
Size: 10718 Color: 441
Size: 5562 Color: 377

Bin 158: 57 of cap free
Amount of items: 3
Items: 
Size: 11315 Color: 453
Size: 4680 Color: 363
Size: 284 Color: 16

Bin 159: 64 of cap free
Amount of items: 4
Items: 
Size: 12468 Color: 477
Size: 3716 Color: 342
Size: 48 Color: 1
Size: 40 Color: 0

Bin 160: 65 of cap free
Amount of items: 3
Items: 
Size: 11117 Color: 448
Size: 3252 Color: 330
Size: 1902 Color: 253

Bin 161: 65 of cap free
Amount of items: 2
Items: 
Size: 11589 Color: 456
Size: 4682 Color: 364

Bin 162: 71 of cap free
Amount of items: 3
Items: 
Size: 12097 Color: 469
Size: 4008 Color: 351
Size: 160 Color: 5

Bin 163: 72 of cap free
Amount of items: 2
Items: 
Size: 8184 Color: 411
Size: 8080 Color: 404

Bin 164: 73 of cap free
Amount of items: 2
Items: 
Size: 10300 Color: 436
Size: 5963 Color: 384

Bin 165: 79 of cap free
Amount of items: 4
Items: 
Size: 12433 Color: 474
Size: 3634 Color: 341
Size: 110 Color: 3
Size: 80 Color: 2

Bin 166: 82 of cap free
Amount of items: 2
Items: 
Size: 10703 Color: 440
Size: 5551 Color: 376

Bin 167: 86 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 458
Size: 4316 Color: 356
Size: 272 Color: 13

Bin 168: 97 of cap free
Amount of items: 3
Items: 
Size: 9867 Color: 432
Size: 6064 Color: 385
Size: 308 Color: 31

Bin 169: 108 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 434
Size: 5868 Color: 382
Size: 304 Color: 28

Bin 170: 108 of cap free
Amount of items: 3
Items: 
Size: 10549 Color: 439
Size: 5391 Color: 374
Size: 288 Color: 22

Bin 171: 116 of cap free
Amount of items: 2
Items: 
Size: 10274 Color: 435
Size: 5946 Color: 383

Bin 172: 121 of cap free
Amount of items: 2
Items: 
Size: 9666 Color: 427
Size: 6549 Color: 391

Bin 173: 123 of cap free
Amount of items: 3
Items: 
Size: 11637 Color: 457
Size: 4300 Color: 355
Size: 276 Color: 14

Bin 174: 123 of cap free
Amount of items: 3
Items: 
Size: 12049 Color: 467
Size: 3964 Color: 349
Size: 200 Color: 7

Bin 175: 136 of cap free
Amount of items: 6
Items: 
Size: 8174 Color: 409
Size: 2741 Color: 309
Size: 2648 Color: 301
Size: 1879 Color: 251
Size: 382 Color: 67
Size: 376 Color: 65

Bin 176: 138 of cap free
Amount of items: 3
Items: 
Size: 10364 Color: 438
Size: 5546 Color: 375
Size: 288 Color: 23

Bin 177: 139 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 466
Size: 3917 Color: 347
Size: 240 Color: 9

Bin 178: 141 of cap free
Amount of items: 3
Items: 
Size: 9471 Color: 425
Size: 6392 Color: 390
Size: 332 Color: 44

Bin 179: 148 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 417
Size: 7152 Color: 401
Size: 356 Color: 54

Bin 180: 157 of cap free
Amount of items: 8
Items: 
Size: 8171 Color: 407
Size: 1606 Color: 226
Size: 1482 Color: 215
Size: 1426 Color: 208
Size: 1360 Color: 202
Size: 1360 Color: 200
Size: 390 Color: 70
Size: 384 Color: 69

Bin 181: 162 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 418
Size: 6806 Color: 398
Size: 352 Color: 53

Bin 182: 164 of cap free
Amount of items: 3
Items: 
Size: 8264 Color: 415
Size: 7540 Color: 403
Size: 368 Color: 58

Bin 183: 166 of cap free
Amount of items: 4
Items: 
Size: 9698 Color: 430
Size: 5832 Color: 381
Size: 320 Color: 36
Size: 320 Color: 35

Bin 184: 172 of cap free
Amount of items: 2
Items: 
Size: 9356 Color: 423
Size: 6808 Color: 400

Bin 185: 194 of cap free
Amount of items: 4
Items: 
Size: 9682 Color: 429
Size: 5820 Color: 380
Size: 320 Color: 38
Size: 320 Color: 37

Bin 186: 198 of cap free
Amount of items: 2
Items: 
Size: 11084 Color: 446
Size: 5054 Color: 369

Bin 187: 204 of cap free
Amount of items: 3
Items: 
Size: 10808 Color: 444
Size: 5036 Color: 368
Size: 288 Color: 20

Bin 188: 206 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 424
Size: 6374 Color: 389
Size: 336 Color: 45

Bin 189: 215 of cap free
Amount of items: 3
Items: 
Size: 11212 Color: 452
Size: 4625 Color: 362
Size: 284 Color: 17

Bin 190: 229 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 414
Size: 7511 Color: 402
Size: 368 Color: 59

Bin 191: 229 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 422
Size: 6807 Color: 399

Bin 192: 252 of cap free
Amount of items: 3
Items: 
Size: 11180 Color: 451
Size: 4616 Color: 361
Size: 288 Color: 18

Bin 193: 257 of cap free
Amount of items: 4
Items: 
Size: 9675 Color: 428
Size: 5764 Color: 379
Size: 320 Color: 40
Size: 320 Color: 39

Bin 194: 271 of cap free
Amount of items: 10
Items: 
Size: 8169 Color: 405
Size: 1188 Color: 188
Size: 1116 Color: 184
Size: 1110 Color: 183
Size: 1108 Color: 182
Size: 1108 Color: 181
Size: 1040 Color: 178
Size: 414 Color: 76
Size: 412 Color: 75
Size: 400 Color: 73

Bin 195: 281 of cap free
Amount of items: 3
Items: 
Size: 10787 Color: 443
Size: 4980 Color: 366
Size: 288 Color: 21

Bin 196: 327 of cap free
Amount of items: 4
Items: 
Size: 8479 Color: 416
Size: 6804 Color: 397
Size: 364 Color: 57
Size: 362 Color: 56

Bin 197: 394 of cap free
Amount of items: 21
Items: 
Size: 992 Color: 174
Size: 992 Color: 173
Size: 928 Color: 170
Size: 924 Color: 169
Size: 912 Color: 168
Size: 896 Color: 167
Size: 880 Color: 166
Size: 868 Color: 164
Size: 856 Color: 162
Size: 856 Color: 161
Size: 836 Color: 159
Size: 784 Color: 156
Size: 784 Color: 155
Size: 782 Color: 154
Size: 776 Color: 153
Size: 776 Color: 152
Size: 428 Color: 82
Size: 424 Color: 81
Size: 416 Color: 79
Size: 416 Color: 78
Size: 416 Color: 77

Bin 198: 484 of cap free
Amount of items: 27
Items: 
Size: 752 Color: 150
Size: 744 Color: 149
Size: 736 Color: 148
Size: 728 Color: 147
Size: 724 Color: 146
Size: 706 Color: 144
Size: 704 Color: 143
Size: 704 Color: 142
Size: 650 Color: 137
Size: 648 Color: 136
Size: 648 Color: 135
Size: 640 Color: 133
Size: 640 Color: 132
Size: 624 Color: 129
Size: 580 Color: 125
Size: 576 Color: 123
Size: 496 Color: 103
Size: 488 Color: 101
Size: 486 Color: 100
Size: 478 Color: 97
Size: 464 Color: 93
Size: 460 Color: 91
Size: 448 Color: 89
Size: 434 Color: 87
Size: 434 Color: 86
Size: 432 Color: 84
Size: 428 Color: 83

Bin 199: 8340 of cap free
Amount of items: 15
Items: 
Size: 576 Color: 122
Size: 568 Color: 121
Size: 562 Color: 120
Size: 550 Color: 117
Size: 546 Color: 115
Size: 544 Color: 114
Size: 542 Color: 113
Size: 528 Color: 111
Size: 528 Color: 110
Size: 512 Color: 109
Size: 512 Color: 108
Size: 512 Color: 107
Size: 508 Color: 106
Size: 504 Color: 105
Size: 504 Color: 104

Total size: 3234528
Total free space: 16336

