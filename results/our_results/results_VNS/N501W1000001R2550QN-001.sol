Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 394305 Color: 394
Size: 305280 Color: 229
Size: 300416 Color: 215

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 417794 Color: 416
Size: 298576 Color: 210
Size: 283631 Color: 166

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 389550 Color: 387
Size: 331921 Color: 287
Size: 278530 Color: 148

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 468077 Color: 473
Size: 279810 Color: 151
Size: 252114 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 496256 Color: 500
Size: 252188 Color: 14
Size: 251557 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 358591 Color: 346
Size: 327000 Color: 273
Size: 314410 Color: 245

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 439142 Color: 439
Size: 298311 Color: 207
Size: 262548 Color: 70

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 436882 Color: 437
Size: 295069 Color: 196
Size: 268050 Color: 97

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 435867 Color: 436
Size: 293701 Color: 191
Size: 270433 Color: 110

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 479496 Color: 483
Size: 269459 Color: 103
Size: 251046 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 428945 Color: 427
Size: 309079 Color: 239
Size: 261977 Color: 64

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 472989 Color: 475
Size: 272455 Color: 121
Size: 254557 Color: 26

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 431761 Color: 431
Size: 303668 Color: 225
Size: 264572 Color: 81

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 406814 Color: 407
Size: 316053 Color: 249
Size: 277134 Color: 144

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 390967 Color: 389
Size: 344779 Color: 319
Size: 264255 Color: 80

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 347365 Color: 323
Size: 332471 Color: 288
Size: 320165 Color: 260

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 439495 Color: 442
Size: 302802 Color: 222
Size: 257704 Color: 44

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 351207 Color: 331
Size: 350457 Color: 329
Size: 298337 Color: 208

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 448208 Color: 453
Size: 280900 Color: 155
Size: 270893 Color: 113

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 396421 Color: 399
Size: 328594 Color: 276
Size: 274986 Color: 133

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 369347 Color: 365
Size: 327784 Color: 275
Size: 302870 Color: 223

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 445810 Color: 451
Size: 284434 Color: 169
Size: 269757 Color: 105

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 379144 Color: 375
Size: 335113 Color: 295
Size: 285744 Color: 172

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 422912 Color: 419
Size: 304033 Color: 226
Size: 273056 Color: 123

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 439832 Color: 443
Size: 300800 Color: 216
Size: 259369 Color: 53

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 423325 Color: 420
Size: 301346 Color: 217
Size: 275330 Color: 136

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 461034 Color: 466
Size: 286869 Color: 173
Size: 252098 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 375681 Color: 370
Size: 365794 Color: 358
Size: 258526 Color: 48

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 387182 Color: 386
Size: 315573 Color: 247
Size: 297246 Color: 205

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 348534 Color: 326
Size: 340979 Color: 312
Size: 310488 Color: 240

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 378620 Color: 374
Size: 358514 Color: 344
Size: 262867 Color: 72

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 473953 Color: 477
Size: 270807 Color: 112
Size: 255241 Color: 33

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 363573 Color: 354
Size: 358541 Color: 345
Size: 277887 Color: 146

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 394207 Color: 392
Size: 329073 Color: 279
Size: 276721 Color: 143

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 381676 Color: 378
Size: 360462 Color: 351
Size: 257863 Color: 46

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 450611 Color: 456
Size: 279008 Color: 149
Size: 270382 Color: 109

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 449264 Color: 455
Size: 282770 Color: 160
Size: 267967 Color: 95

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 432415 Color: 432
Size: 296605 Color: 202
Size: 270981 Color: 114

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 423698 Color: 421
Size: 315044 Color: 246
Size: 261259 Color: 60

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 414877 Color: 414
Size: 305459 Color: 230
Size: 279665 Color: 150

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 478143 Color: 482
Size: 261293 Color: 61
Size: 260565 Color: 55

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 493071 Color: 497
Size: 254314 Color: 24
Size: 252616 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 435481 Color: 434
Size: 299340 Color: 212
Size: 265180 Color: 84

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 340465 Color: 308
Size: 337253 Color: 302
Size: 322283 Color: 267

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 386601 Color: 385
Size: 330494 Color: 285
Size: 282906 Color: 161

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 437925 Color: 438
Size: 294056 Color: 193
Size: 268020 Color: 96

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 427454 Color: 426
Size: 317396 Color: 252
Size: 255151 Color: 30

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 480026 Color: 485
Size: 269112 Color: 101
Size: 250863 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 414340 Color: 411
Size: 332774 Color: 289
Size: 252887 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 368475 Color: 362
Size: 355517 Color: 338
Size: 276009 Color: 140

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 382747 Color: 379
Size: 360302 Color: 350
Size: 256952 Color: 40

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 494132 Color: 498
Size: 253978 Color: 22
Size: 251891 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 419085 Color: 418
Size: 309064 Color: 238
Size: 271852 Color: 119

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 359818 Color: 348
Size: 340682 Color: 310
Size: 299501 Color: 214

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 455663 Color: 462
Size: 281704 Color: 157
Size: 262634 Color: 71

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 452089 Color: 460
Size: 277572 Color: 145
Size: 270340 Color: 108

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 377679 Color: 373
Size: 338745 Color: 304
Size: 283577 Color: 165

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 360749 Color: 352
Size: 334179 Color: 292
Size: 305073 Color: 228

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 400750 Color: 403
Size: 333709 Color: 291
Size: 265542 Color: 88

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 368542 Color: 363
Size: 355011 Color: 337
Size: 276448 Color: 142

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 439411 Color: 441
Size: 298343 Color: 209
Size: 262247 Color: 67

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 370870 Color: 367
Size: 318365 Color: 255
Size: 310766 Color: 241

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 473305 Color: 476
Size: 272276 Color: 120
Size: 254420 Color: 25

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 424434 Color: 423
Size: 310792 Color: 242
Size: 264775 Color: 83

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 455837 Color: 463
Size: 282981 Color: 162
Size: 261183 Color: 57

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 353986 Color: 335
Size: 330297 Color: 283
Size: 315718 Color: 248

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 340768 Color: 311
Size: 329883 Color: 282
Size: 329350 Color: 281

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 440229 Color: 444
Size: 301623 Color: 219
Size: 258149 Color: 47

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 435744 Color: 435
Size: 295672 Color: 198
Size: 268585 Color: 98

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 354738 Color: 336
Size: 328639 Color: 277
Size: 316624 Color: 250

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 345214 Color: 321
Size: 337106 Color: 300
Size: 317681 Color: 253

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 429147 Color: 428
Size: 319906 Color: 259
Size: 250948 Color: 5

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 348023 Color: 325
Size: 333351 Color: 290
Size: 318627 Color: 256

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 441952 Color: 445
Size: 305491 Color: 231
Size: 252558 Color: 16

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 353761 Color: 334
Size: 326381 Color: 272
Size: 319859 Color: 258

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 452738 Color: 461
Size: 296129 Color: 200
Size: 251134 Color: 7

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 374123 Color: 369
Size: 356147 Color: 339
Size: 269731 Color: 104

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 444296 Color: 450
Size: 280538 Color: 153
Size: 275167 Color: 134

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 431138 Color: 430
Size: 297123 Color: 204
Size: 271740 Color: 118

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 490029 Color: 495
Size: 255184 Color: 31
Size: 254788 Color: 29

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 394320 Color: 395
Size: 324863 Color: 269
Size: 280818 Color: 154

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 484575 Color: 492
Size: 263701 Color: 78
Size: 251725 Color: 10

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 364225 Color: 356
Size: 344044 Color: 318
Size: 291732 Color: 186

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 417176 Color: 415
Size: 317328 Color: 251
Size: 265497 Color: 87

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 483351 Color: 489
Size: 263292 Color: 77
Size: 253358 Color: 19

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 495956 Color: 499
Size: 253729 Color: 21
Size: 250316 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 370184 Color: 366
Size: 366936 Color: 361
Size: 262881 Color: 73

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 396076 Color: 398
Size: 308445 Color: 236
Size: 295480 Color: 197

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 368678 Color: 364
Size: 341639 Color: 313
Size: 289684 Color: 181

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 474510 Color: 478
Size: 269013 Color: 100
Size: 256478 Color: 38

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 366808 Color: 360
Size: 359048 Color: 347
Size: 274145 Color: 131

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 476758 Color: 480
Size: 264616 Color: 82
Size: 258627 Color: 50

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 442804 Color: 448
Size: 283912 Color: 168
Size: 273285 Color: 124

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 457570 Color: 464
Size: 283343 Color: 164
Size: 259088 Color: 51

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 464495 Color: 471
Size: 274138 Color: 130
Size: 261368 Color: 62

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 463877 Color: 470
Size: 281404 Color: 156
Size: 254720 Color: 28

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 414461 Color: 413
Size: 311897 Color: 243
Size: 273643 Color: 128

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 409236 Color: 408
Size: 320942 Color: 262
Size: 269823 Color: 106

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 429928 Color: 429
Size: 304583 Color: 227
Size: 265490 Color: 86

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 461639 Color: 468
Size: 269427 Color: 102
Size: 268935 Color: 99

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 442465 Color: 446
Size: 295062 Color: 195
Size: 262474 Color: 69

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 480144 Color: 486
Size: 267348 Color: 93
Size: 252509 Color: 15

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 443644 Color: 449
Size: 299367 Color: 213
Size: 256990 Color: 41

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 394609 Color: 396
Size: 334881 Color: 293
Size: 270511 Color: 111

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 363583 Color: 355
Size: 337188 Color: 301
Size: 299230 Color: 211

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 470247 Color: 474
Size: 276264 Color: 141
Size: 253490 Color: 20

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 411320 Color: 409
Size: 295879 Color: 199
Size: 292802 Color: 189

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 398328 Color: 400
Size: 336003 Color: 298
Size: 265670 Color: 90

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 349393 Color: 328
Size: 342884 Color: 315
Size: 307724 Color: 234

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 386450 Color: 384
Size: 321642 Color: 266
Size: 291909 Color: 187

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 381337 Color: 377
Size: 345657 Color: 322
Size: 273007 Color: 122

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 358085 Color: 343
Size: 345094 Color: 320
Size: 296822 Color: 203

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 481052 Color: 488
Size: 262227 Color: 66
Size: 256722 Color: 39

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 404582 Color: 406
Size: 306463 Color: 233
Size: 288956 Color: 179

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 362754 Color: 353
Size: 328816 Color: 278
Size: 308431 Color: 235

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 342217 Color: 314
Size: 340062 Color: 306
Size: 317722 Color: 254

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 451025 Color: 458
Size: 293406 Color: 190
Size: 255570 Color: 34

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 385352 Color: 383
Size: 329228 Color: 280
Size: 285421 Color: 171

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 398914 Color: 401
Size: 318923 Color: 257
Size: 282164 Color: 159

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 486572 Color: 494
Size: 263049 Color: 75
Size: 250380 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 491352 Color: 496
Size: 257266 Color: 42
Size: 251383 Color: 8

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 439343 Color: 440
Size: 289169 Color: 180
Size: 271489 Color: 116

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 426824 Color: 425
Size: 288434 Color: 177
Size: 284743 Color: 170

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 394224 Color: 393
Size: 335573 Color: 296
Size: 270204 Color: 107

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 451351 Color: 459
Size: 290875 Color: 184
Size: 257775 Color: 45

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 348759 Color: 327
Size: 347884 Color: 324
Size: 303358 Color: 224

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 340681 Color: 309
Size: 338318 Color: 303
Size: 321002 Color: 263

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 479718 Color: 484
Size: 265653 Color: 89
Size: 254630 Color: 27

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 458798 Color: 465
Size: 279994 Color: 152
Size: 261209 Color: 58

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 414435 Color: 412
Size: 302417 Color: 220
Size: 283149 Color: 163

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 447471 Color: 452
Size: 296439 Color: 201
Size: 256091 Color: 35

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 483538 Color: 490
Size: 261227 Color: 59
Size: 255236 Color: 32

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 390208 Color: 388
Size: 336319 Color: 299
Size: 273474 Color: 126

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 461479 Color: 467
Size: 275491 Color: 137
Size: 263031 Color: 74

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 391234 Color: 390
Size: 330310 Color: 284
Size: 278457 Color: 147

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 350819 Color: 330
Size: 340331 Color: 307
Size: 308851 Color: 237

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 366287 Color: 359
Size: 360206 Color: 349
Size: 273508 Color: 127

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 358079 Color: 342
Size: 321159 Color: 264
Size: 320763 Color: 261

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 426115 Color: 424
Size: 306153 Color: 232
Size: 267733 Color: 94

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 395770 Color: 397
Size: 302609 Color: 221
Size: 301622 Color: 218

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 432676 Color: 433
Size: 291468 Color: 185
Size: 275857 Color: 139

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 414206 Color: 410
Size: 298207 Color: 206
Size: 287588 Color: 176

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 383798 Color: 382
Size: 357605 Color: 341
Size: 258598 Color: 49

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 418645 Color: 417
Size: 294264 Color: 194
Size: 287092 Color: 174

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 484622 Color: 493
Size: 259103 Color: 52
Size: 256276 Color: 37

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 364504 Color: 357
Size: 353400 Color: 332
Size: 282097 Color: 158

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 424195 Color: 422
Size: 325231 Color: 270
Size: 250575 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 403316 Color: 404
Size: 321479 Color: 265
Size: 275206 Color: 135

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 483551 Color: 491
Size: 262248 Color: 68
Size: 254202 Color: 23

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 442522 Color: 447
Size: 283793 Color: 167
Size: 273686 Color: 129

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 377575 Color: 372
Size: 334955 Color: 294
Size: 287471 Color: 175

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 462782 Color: 469
Size: 273294 Color: 125
Size: 263925 Color: 79

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 403646 Color: 405
Size: 325347 Color: 271
Size: 271008 Color: 115

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 399167 Color: 402
Size: 335616 Color: 297
Size: 265218 Color: 85

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 383258 Color: 380
Size: 323004 Color: 268
Size: 293739 Color: 192

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 379904 Color: 376
Size: 353726 Color: 333
Size: 266371 Color: 92

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 448672 Color: 454
Size: 290609 Color: 183
Size: 260720 Color: 56

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 450972 Color: 457
Size: 292791 Color: 188
Size: 256238 Color: 36

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 343639 Color: 317
Size: 343510 Color: 316
Size: 312852 Color: 244

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 475712 Color: 479
Size: 274176 Color: 132
Size: 250113 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 377064 Color: 371
Size: 357100 Color: 340
Size: 265837 Color: 91

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 466998 Color: 472
Size: 271501 Color: 117
Size: 261502 Color: 63

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 393546 Color: 391
Size: 330598 Color: 286
Size: 275857 Color: 138

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 371143 Color: 368
Size: 339044 Color: 305
Size: 289814 Color: 182

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 480250 Color: 487
Size: 262048 Color: 65
Size: 257703 Color: 43

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 476930 Color: 481
Size: 263272 Color: 76
Size: 259799 Color: 54

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 383409 Color: 381
Size: 327763 Color: 274
Size: 288829 Color: 178

Total size: 167000167
Total free space: 0

