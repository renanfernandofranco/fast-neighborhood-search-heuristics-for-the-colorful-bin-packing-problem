Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7672 Color: 409
Size: 2087 Color: 263
Size: 2042 Color: 258
Size: 1897 Color: 245
Size: 714 Color: 140
Size: 460 Color: 92
Size: 456 Color: 91

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11009 Color: 469
Size: 3601 Color: 332
Size: 718 Color: 143

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 472
Size: 3592 Color: 331
Size: 704 Color: 135

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 486
Size: 3928 Color: 343
Size: 112 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11549 Color: 488
Size: 3401 Color: 321
Size: 378 Color: 64

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 489
Size: 3146 Color: 313
Size: 628 Color: 126

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 490
Size: 1961 Color: 252
Size: 1797 Color: 240

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11660 Color: 493
Size: 3060 Color: 309
Size: 608 Color: 123

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11837 Color: 500
Size: 2957 Color: 307
Size: 534 Color: 107

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11966 Color: 504
Size: 2684 Color: 295
Size: 678 Color: 132

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 507
Size: 2936 Color: 305
Size: 336 Color: 51

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12124 Color: 510
Size: 2676 Color: 293
Size: 528 Color: 105

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12133 Color: 511
Size: 2363 Color: 281
Size: 832 Color: 161

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12269 Color: 514
Size: 2377 Color: 283
Size: 682 Color: 133

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 517
Size: 2530 Color: 286
Size: 504 Color: 100

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12303 Color: 518
Size: 1921 Color: 247
Size: 1104 Color: 188

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12453 Color: 521
Size: 2251 Color: 272
Size: 624 Color: 125

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12477 Color: 522
Size: 2521 Color: 285
Size: 330 Color: 49

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12484 Color: 523
Size: 2026 Color: 257
Size: 818 Color: 156

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 526
Size: 2584 Color: 289
Size: 192 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12562 Color: 527
Size: 2534 Color: 287
Size: 232 Color: 20

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 529
Size: 2306 Color: 276
Size: 444 Color: 85

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 531
Size: 2308 Color: 277
Size: 408 Color: 76

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 532
Size: 1688 Color: 232
Size: 1024 Color: 183

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12667 Color: 535
Size: 1951 Color: 250
Size: 710 Color: 139

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12683 Color: 537
Size: 2205 Color: 268
Size: 440 Color: 82

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12809 Color: 540
Size: 1655 Color: 227
Size: 864 Color: 162

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12825 Color: 541
Size: 1831 Color: 241
Size: 672 Color: 130

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12856 Color: 542
Size: 2264 Color: 273
Size: 208 Color: 15

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 543
Size: 2058 Color: 261
Size: 408 Color: 77

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 544
Size: 2056 Color: 260
Size: 400 Color: 70

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 545
Size: 1924 Color: 248
Size: 528 Color: 104

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12882 Color: 546
Size: 1620 Color: 223
Size: 826 Color: 160

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12898 Color: 547
Size: 1766 Color: 235
Size: 664 Color: 129

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 550
Size: 2044 Color: 259
Size: 312 Color: 39

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 557
Size: 1834 Color: 243
Size: 364 Color: 63

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 559
Size: 1832 Color: 242
Size: 352 Color: 56

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 560
Size: 1621 Color: 224
Size: 544 Color: 109

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13186 Color: 562
Size: 1784 Color: 238
Size: 358 Color: 59

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 563
Size: 1772 Color: 236
Size: 364 Color: 62

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13202 Color: 564
Size: 1406 Color: 209
Size: 720 Color: 144

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 565
Size: 1448 Color: 210
Size: 676 Color: 131

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 570
Size: 1526 Color: 217
Size: 480 Color: 98

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 571
Size: 1672 Color: 229
Size: 332 Color: 50

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 573
Size: 1232 Color: 194
Size: 760 Color: 148

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13338 Color: 574
Size: 1674 Color: 230
Size: 316 Color: 41

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13340 Color: 575
Size: 1484 Color: 214
Size: 504 Color: 101

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 577
Size: 1072 Color: 184
Size: 904 Color: 167

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 581
Size: 1492 Color: 215
Size: 448 Color: 88

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13439 Color: 583
Size: 1575 Color: 221
Size: 314 Color: 40

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13498 Color: 586
Size: 1126 Color: 192
Size: 704 Color: 134

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 588
Size: 1476 Color: 213
Size: 288 Color: 33

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 589
Size: 1594 Color: 222
Size: 164 Color: 8

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 591
Size: 1454 Color: 211
Size: 288 Color: 32

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 592
Size: 904 Color: 168
Size: 816 Color: 154

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 593
Size: 1286 Color: 204
Size: 400 Color: 71

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 596
Size: 1272 Color: 200
Size: 356 Color: 58

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 597
Size: 1100 Color: 187
Size: 508 Color: 102

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 598
Size: 1120 Color: 190
Size: 472 Color: 94

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13786 Color: 600
Size: 960 Color: 177
Size: 582 Color: 118

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 8710 Color: 422
Size: 3389 Color: 320
Size: 3228 Color: 316

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11269 Color: 485
Size: 3586 Color: 330
Size: 472 Color: 95

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11656 Color: 492
Size: 2911 Color: 303
Size: 760 Color: 147

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 497
Size: 3134 Color: 312
Size: 388 Color: 67

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11829 Color: 499
Size: 2234 Color: 271
Size: 1264 Color: 197

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11962 Color: 503
Size: 2917 Color: 304
Size: 448 Color: 87

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12021 Color: 505
Size: 2802 Color: 300
Size: 504 Color: 99

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 12045 Color: 506
Size: 3282 Color: 317

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 509
Size: 2806 Color: 301
Size: 404 Color: 74

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12290 Color: 516
Size: 2219 Color: 270
Size: 818 Color: 157

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 520
Size: 2663 Color: 292
Size: 224 Color: 17

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 12650 Color: 534
Size: 2677 Color: 294

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 13371 Color: 579
Size: 1956 Color: 251

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 13383 Color: 580
Size: 1944 Color: 249

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13455 Color: 584
Size: 1248 Color: 195
Size: 624 Color: 124

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 590
Size: 1751 Color: 234

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 8726 Color: 423
Size: 6184 Color: 392
Size: 416 Color: 78

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 471
Size: 3524 Color: 324
Size: 776 Color: 151

Bin 79: 2 of cap free
Amount of items: 4
Items: 
Size: 11082 Color: 477
Size: 3900 Color: 341
Size: 176 Color: 10
Size: 168 Color: 9

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 11245 Color: 483
Size: 4081 Color: 345

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11249 Color: 484
Size: 3541 Color: 325
Size: 536 Color: 108

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 12954 Color: 549
Size: 2372 Color: 282

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 556
Size: 2214 Color: 269

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 9325 Color: 430
Size: 5608 Color: 385
Size: 392 Color: 68

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 11683 Color: 494
Size: 3642 Color: 334

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 13133 Color: 558
Size: 2192 Color: 267

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13229 Color: 567
Size: 2096 Color: 264

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 568
Size: 2072 Color: 262

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 569
Size: 2021 Color: 256

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13343 Color: 576
Size: 1982 Color: 254

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 599
Size: 1561 Color: 220

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 427
Size: 6308 Color: 394

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 450
Size: 4880 Color: 371
Size: 308 Color: 38

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 10621 Color: 460
Size: 3581 Color: 329
Size: 1122 Color: 191

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 13327 Color: 572
Size: 1997 Color: 255

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 431
Size: 5476 Color: 380
Size: 384 Color: 66

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 487
Size: 3863 Color: 338

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 11781 Color: 496
Size: 3542 Color: 326

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 11940 Color: 501
Size: 3383 Color: 319

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 515
Size: 3051 Color: 308

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 13692 Color: 595
Size: 1631 Color: 225

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 478
Size: 4086 Color: 347
Size: 152 Color: 7

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 480
Size: 4121 Color: 350
Size: 96 Color: 4

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 536
Size: 2648 Color: 291

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13418 Color: 582
Size: 1904 Color: 246

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 585
Size: 1848 Color: 244

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 587
Size: 1774 Color: 237

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13646 Color: 594
Size: 1676 Color: 231

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 470
Size: 4311 Color: 354

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 11100 Color: 479
Size: 4093 Color: 348
Size: 128 Color: 6

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 12108 Color: 508
Size: 3213 Color: 315

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 12493 Color: 524
Size: 2828 Color: 302

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 528
Size: 2757 Color: 298

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 13053 Color: 555
Size: 2268 Color: 274

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 8600 Color: 419
Size: 6720 Color: 402

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 578
Size: 1964 Color: 253

Bin 117: 9 of cap free
Amount of items: 3
Items: 
Size: 9929 Color: 446
Size: 5064 Color: 375
Size: 326 Color: 45

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 525
Size: 2783 Color: 299

Bin 119: 10 of cap free
Amount of items: 4
Items: 
Size: 9884 Color: 443
Size: 4742 Color: 367
Size: 348 Color: 53
Size: 344 Color: 52

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 11950 Color: 502
Size: 3368 Color: 318

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12381 Color: 519
Size: 2937 Color: 306

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 533
Size: 2691 Color: 296

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12720 Color: 538
Size: 2598 Color: 290

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13173 Color: 561
Size: 2145 Color: 266

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 498
Size: 3501 Color: 323

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 551
Size: 2344 Color: 280

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 12989 Color: 553
Size: 2328 Color: 279

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 9788 Color: 440
Size: 5528 Color: 383

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 10632 Color: 461
Size: 4684 Color: 365

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 10732 Color: 464
Size: 4328 Color: 355
Size: 256 Color: 25

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 552
Size: 2328 Color: 278

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 10958 Color: 468
Size: 4101 Color: 349
Size: 256 Color: 21

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13023 Color: 554
Size: 2292 Color: 275

Bin 134: 14 of cap free
Amount of items: 3
Items: 
Size: 9472 Color: 432
Size: 5458 Color: 379
Size: 384 Color: 65

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 11711 Color: 495
Size: 3602 Color: 333

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 566
Size: 2101 Color: 265

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 11576 Color: 491
Size: 3736 Color: 335

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 539
Size: 2551 Color: 288

Bin 139: 17 of cap free
Amount of items: 2
Items: 
Size: 12903 Color: 548
Size: 2408 Color: 284

Bin 140: 19 of cap free
Amount of items: 7
Items: 
Size: 7667 Color: 407
Size: 1560 Color: 219
Size: 1546 Color: 218
Size: 1496 Color: 216
Size: 1466 Color: 212
Size: 1100 Color: 186
Size: 474 Color: 96

Bin 141: 19 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 458
Size: 4604 Color: 363
Size: 272 Color: 29

Bin 142: 20 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 530
Size: 2728 Color: 297

Bin 143: 21 of cap free
Amount of items: 2
Items: 
Size: 9654 Color: 437
Size: 5653 Color: 388

Bin 144: 24 of cap free
Amount of items: 4
Items: 
Size: 11189 Color: 481
Size: 3923 Color: 342
Size: 96 Color: 3
Size: 96 Color: 2

Bin 145: 25 of cap free
Amount of items: 4
Items: 
Size: 11057 Color: 474
Size: 3836 Color: 337
Size: 210 Color: 16
Size: 200 Color: 14

Bin 146: 25 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 512
Size: 3151 Color: 314

Bin 147: 26 of cap free
Amount of items: 4
Items: 
Size: 9868 Color: 442
Size: 4730 Color: 366
Size: 352 Color: 55
Size: 352 Color: 54

Bin 148: 26 of cap free
Amount of items: 3
Items: 
Size: 10093 Color: 449
Size: 4889 Color: 372
Size: 320 Color: 42

Bin 149: 27 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 420
Size: 6284 Color: 393
Size: 416 Color: 79

Bin 150: 27 of cap free
Amount of items: 4
Items: 
Size: 11033 Color: 473
Size: 3820 Color: 336
Size: 224 Color: 19
Size: 224 Color: 18

Bin 151: 28 of cap free
Amount of items: 2
Items: 
Size: 7700 Color: 412
Size: 7600 Color: 404

Bin 152: 28 of cap free
Amount of items: 3
Items: 
Size: 9560 Color: 433
Size: 5380 Color: 378
Size: 360 Color: 61

Bin 153: 28 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 438
Size: 5232 Color: 377
Size: 360 Color: 60

Bin 154: 28 of cap free
Amount of items: 4
Items: 
Size: 9900 Color: 444
Size: 4744 Color: 368
Size: 328 Color: 48
Size: 328 Color: 47

Bin 155: 28 of cap free
Amount of items: 3
Items: 
Size: 10457 Color: 459
Size: 4571 Color: 362
Size: 272 Color: 28

Bin 156: 32 of cap free
Amount of items: 2
Items: 
Size: 10361 Color: 451
Size: 4935 Color: 373

Bin 157: 32 of cap free
Amount of items: 2
Items: 
Size: 12232 Color: 513
Size: 3064 Color: 310

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 9594 Color: 434
Size: 5696 Color: 389

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 9804 Color: 441
Size: 5132 Color: 376
Size: 352 Color: 57

Bin 160: 40 of cap free
Amount of items: 4
Items: 
Size: 10668 Color: 463
Size: 4084 Color: 346
Size: 272 Color: 27
Size: 264 Color: 26

Bin 161: 44 of cap free
Amount of items: 4
Items: 
Size: 11233 Color: 482
Size: 3939 Color: 344
Size: 72 Color: 1
Size: 40 Color: 0

Bin 162: 45 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 448
Size: 5003 Color: 374
Size: 320 Color: 43

Bin 163: 55 of cap free
Amount of items: 2
Items: 
Size: 9640 Color: 436
Size: 5633 Color: 387

Bin 164: 56 of cap free
Amount of items: 2
Items: 
Size: 10652 Color: 462
Size: 4620 Color: 364

Bin 165: 64 of cap free
Amount of items: 3
Items: 
Size: 8804 Color: 425
Size: 6056 Color: 391
Size: 404 Color: 75

Bin 166: 66 of cap free
Amount of items: 3
Items: 
Size: 10426 Color: 457
Size: 4556 Color: 361
Size: 280 Color: 30

Bin 167: 70 of cap free
Amount of items: 2
Items: 
Size: 9740 Color: 439
Size: 5518 Color: 382

Bin 168: 73 of cap free
Amount of items: 3
Items: 
Size: 9256 Color: 429
Size: 5607 Color: 384
Size: 392 Color: 69

Bin 169: 75 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 467
Size: 4141 Color: 353
Size: 256 Color: 22

Bin 170: 77 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 435
Size: 5613 Color: 386

Bin 171: 84 of cap free
Amount of items: 4
Items: 
Size: 7674 Color: 410
Size: 3561 Color: 328
Size: 3553 Color: 327
Size: 456 Color: 90

Bin 172: 91 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 421
Size: 3413 Color: 322
Size: 3128 Color: 311

Bin 173: 91 of cap free
Amount of items: 3
Items: 
Size: 10417 Color: 456
Size: 4540 Color: 360
Size: 280 Color: 31

Bin 174: 103 of cap free
Amount of items: 3
Items: 
Size: 10409 Color: 455
Size: 4524 Color: 359
Size: 292 Color: 34

Bin 175: 131 of cap free
Amount of items: 3
Items: 
Size: 10385 Color: 454
Size: 4516 Color: 358
Size: 296 Color: 35

Bin 176: 132 of cap free
Amount of items: 3
Items: 
Size: 8876 Color: 426
Size: 5916 Color: 390
Size: 404 Color: 73

Bin 177: 147 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 453
Size: 4501 Color: 357
Size: 304 Color: 36

Bin 178: 154 of cap free
Amount of items: 2
Items: 
Size: 8782 Color: 424
Size: 6392 Color: 401

Bin 179: 170 of cap free
Amount of items: 20
Items: 
Size: 944 Color: 172
Size: 936 Color: 171
Size: 920 Color: 170
Size: 920 Color: 169
Size: 904 Color: 166
Size: 898 Color: 165
Size: 896 Color: 164
Size: 880 Color: 163
Size: 824 Color: 159
Size: 822 Color: 158
Size: 816 Color: 155
Size: 640 Color: 128
Size: 628 Color: 127
Size: 608 Color: 122
Size: 608 Color: 121
Size: 590 Color: 120
Size: 586 Color: 119
Size: 582 Color: 117
Size: 580 Color: 116
Size: 576 Color: 115

Bin 180: 170 of cap free
Amount of items: 3
Items: 
Size: 10366 Color: 452
Size: 4488 Color: 356
Size: 304 Color: 37

Bin 181: 170 of cap free
Amount of items: 3
Items: 
Size: 10764 Color: 466
Size: 4138 Color: 352
Size: 256 Color: 23

Bin 182: 173 of cap free
Amount of items: 4
Items: 
Size: 7912 Color: 415
Size: 6385 Color: 397
Size: 440 Color: 81
Size: 418 Color: 80

Bin 183: 179 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 476
Size: 3884 Color: 340
Size: 184 Color: 11

Bin 184: 187 of cap free
Amount of items: 3
Items: 
Size: 11065 Color: 475
Size: 3884 Color: 339
Size: 192 Color: 12

Bin 185: 188 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 465
Size: 4136 Color: 351
Size: 256 Color: 24

Bin 186: 243 of cap free
Amount of items: 3
Items: 
Size: 9953 Color: 447
Size: 4808 Color: 370
Size: 324 Color: 44

Bin 187: 250 of cap free
Amount of items: 3
Items: 
Size: 9172 Color: 428
Size: 5502 Color: 381
Size: 404 Color: 72

Bin 188: 267 of cap free
Amount of items: 8
Items: 
Size: 7665 Color: 405
Size: 1276 Color: 202
Size: 1276 Color: 201
Size: 1272 Color: 199
Size: 1272 Color: 198
Size: 1256 Color: 196
Size: 532 Color: 106
Size: 512 Color: 103

Bin 189: 268 of cap free
Amount of items: 4
Items: 
Size: 7796 Color: 414
Size: 6382 Color: 396
Size: 442 Color: 84
Size: 440 Color: 83

Bin 190: 294 of cap free
Amount of items: 4
Items: 
Size: 7764 Color: 413
Size: 6372 Color: 395
Size: 450 Color: 89
Size: 448 Color: 86

Bin 191: 302 of cap free
Amount of items: 3
Items: 
Size: 9916 Color: 445
Size: 4782 Color: 369
Size: 328 Color: 46

Bin 192: 320 of cap free
Amount of items: 2
Items: 
Size: 7684 Color: 411
Size: 7324 Color: 403

Bin 193: 347 of cap free
Amount of items: 2
Items: 
Size: 8593 Color: 418
Size: 6388 Color: 400

Bin 194: 372 of cap free
Amount of items: 2
Items: 
Size: 8569 Color: 417
Size: 6387 Color: 399

Bin 195: 375 of cap free
Amount of items: 6
Items: 
Size: 7668 Color: 408
Size: 1786 Color: 239
Size: 1731 Color: 233
Size: 1660 Color: 228
Size: 1644 Color: 226
Size: 464 Color: 93

Bin 196: 397 of cap free
Amount of items: 2
Items: 
Size: 8545 Color: 416
Size: 6386 Color: 398

Bin 197: 424 of cap free
Amount of items: 7
Items: 
Size: 7666 Color: 406
Size: 1402 Color: 208
Size: 1364 Color: 207
Size: 1364 Color: 206
Size: 1352 Color: 205
Size: 1276 Color: 203
Size: 480 Color: 97

Bin 198: 444 of cap free
Amount of items: 17
Items: 
Size: 1130 Color: 193
Size: 1120 Color: 189
Size: 1088 Color: 185
Size: 1008 Color: 182
Size: 1008 Color: 181
Size: 1000 Color: 180
Size: 976 Color: 179
Size: 976 Color: 178
Size: 952 Color: 176
Size: 948 Color: 175
Size: 944 Color: 174
Size: 944 Color: 173
Size: 560 Color: 114
Size: 560 Color: 113
Size: 560 Color: 112
Size: 560 Color: 111
Size: 550 Color: 110

Bin 199: 7170 of cap free
Amount of items: 11
Items: 
Size: 814 Color: 153
Size: 784 Color: 152
Size: 776 Color: 150
Size: 768 Color: 149
Size: 736 Color: 146
Size: 728 Color: 145
Size: 716 Color: 142
Size: 716 Color: 141
Size: 710 Color: 138
Size: 706 Color: 137
Size: 704 Color: 136

Total size: 3034944
Total free space: 15328

