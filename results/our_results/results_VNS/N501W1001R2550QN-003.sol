Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 444
Size: 303 Color: 214
Size: 261 Color: 68

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 498
Size: 252 Color: 19
Size: 251 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 374
Size: 357 Color: 331
Size: 264 Color: 87

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 403
Size: 334 Color: 283
Size: 261 Color: 70

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 387
Size: 328 Color: 271
Size: 281 Color: 158

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 424
Size: 327 Color: 269
Size: 255 Color: 42

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 479
Size: 266 Color: 103
Size: 264 Color: 88

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 491
Size: 259 Color: 59
Size: 255 Color: 44

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 411
Size: 335 Color: 289
Size: 255 Color: 48

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 490
Size: 265 Color: 93
Size: 250 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 458
Size: 301 Color: 211
Size: 253 Color: 31

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 370
Size: 342 Color: 303
Size: 281 Color: 160

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 446
Size: 285 Color: 175
Size: 278 Color: 146

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 323
Size: 337 Color: 293
Size: 312 Color: 235

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 493
Size: 254 Color: 38
Size: 253 Color: 37

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 428
Size: 318 Color: 247
Size: 260 Color: 66

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 252 Color: 25
Size: 251 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 426
Size: 317 Color: 242
Size: 263 Color: 84

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 364
Size: 372 Color: 357
Size: 253 Color: 36

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 425
Size: 322 Color: 253
Size: 259 Color: 61

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 396
Size: 339 Color: 296
Size: 263 Color: 77

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 346
Size: 319 Color: 248
Size: 316 Color: 241

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 473
Size: 275 Color: 128
Size: 264 Color: 91

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 406
Size: 330 Color: 276
Size: 263 Color: 78

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 485
Size: 269 Color: 113
Size: 252 Color: 23

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 436
Size: 313 Color: 238
Size: 263 Color: 82

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 494
Size: 256 Color: 50
Size: 250 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 361
Size: 334 Color: 284
Size: 294 Color: 197

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 359
Size: 324 Color: 259
Size: 304 Color: 216

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 489
Size: 266 Color: 104
Size: 251 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 363
Size: 329 Color: 273
Size: 296 Color: 202

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 409
Size: 330 Color: 278
Size: 262 Color: 72

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 344
Size: 340 Color: 297
Size: 299 Color: 207

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 382
Size: 320 Color: 251
Size: 294 Color: 198

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 423
Size: 329 Color: 274
Size: 253 Color: 34

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 384
Size: 358 Color: 335
Size: 253 Color: 35

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 472
Size: 286 Color: 176
Size: 253 Color: 32

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 474
Size: 271 Color: 118
Size: 267 Color: 105

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 401
Size: 319 Color: 249
Size: 279 Color: 149

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 321
Size: 350 Color: 318
Size: 300 Color: 210

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 496
Size: 255 Color: 45
Size: 250 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 385
Size: 331 Color: 281
Size: 280 Color: 152

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 366
Size: 353 Color: 325
Size: 271 Color: 119

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 477
Size: 274 Color: 123
Size: 258 Color: 56

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 412
Size: 334 Color: 288
Size: 255 Color: 43

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 435
Size: 326 Color: 266
Size: 250 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 453
Size: 294 Color: 196
Size: 265 Color: 94

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 410
Size: 307 Color: 222
Size: 284 Color: 173

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 333
Size: 355 Color: 330
Size: 289 Color: 186

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 340
Size: 345 Color: 309
Size: 296 Color: 204

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 404
Size: 312 Color: 233
Size: 282 Color: 162

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 337
Size: 350 Color: 319
Size: 291 Color: 190

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 391
Size: 326 Color: 265
Size: 281 Color: 159

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 455
Size: 295 Color: 199
Size: 263 Color: 80

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 354
Size: 367 Color: 347
Size: 263 Color: 83

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 381
Size: 336 Color: 291
Size: 278 Color: 145

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 317
Size: 344 Color: 306
Size: 308 Color: 224

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 402
Size: 334 Color: 285
Size: 262 Color: 73

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 487
Size: 269 Color: 111
Size: 251 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 365
Size: 353 Color: 324
Size: 272 Color: 120

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 379
Size: 329 Color: 272
Size: 288 Color: 184

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 484
Size: 270 Color: 114
Size: 252 Color: 24

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 431
Size: 302 Color: 212
Size: 275 Color: 127

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 414
Size: 337 Color: 294
Size: 252 Color: 20

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 440
Size: 299 Color: 206
Size: 269 Color: 112

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 467
Size: 287 Color: 180
Size: 255 Color: 49

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 476
Size: 268 Color: 110
Size: 267 Color: 107

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 459
Size: 295 Color: 201
Size: 258 Color: 55

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 495
Size: 253 Color: 30
Size: 252 Color: 16

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 362
Size: 341 Color: 300
Size: 286 Color: 178

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 352
Size: 354 Color: 328
Size: 278 Color: 141

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 420
Size: 325 Color: 263
Size: 260 Color: 63

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 397
Size: 338 Color: 295
Size: 263 Color: 85

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 373
Size: 360 Color: 338
Size: 261 Color: 71

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 452
Size: 292 Color: 194
Size: 267 Color: 106

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 415
Size: 333 Color: 282
Size: 256 Color: 51

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 500
Size: 251 Color: 14
Size: 251 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 450
Size: 295 Color: 200
Size: 265 Color: 98

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 465
Size: 279 Color: 148
Size: 264 Color: 90

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 434
Size: 323 Color: 257
Size: 253 Color: 29

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 341
Size: 353 Color: 326
Size: 287 Color: 181

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 399
Size: 323 Color: 256
Size: 278 Color: 144

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 388
Size: 325 Color: 264
Size: 284 Color: 171

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 456
Size: 282 Color: 166
Size: 275 Color: 130

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 380
Size: 317 Color: 243
Size: 299 Color: 209

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 448
Size: 289 Color: 189
Size: 273 Color: 122

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 310
Size: 330 Color: 277
Size: 326 Color: 267

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 471
Size: 276 Color: 133
Size: 263 Color: 81

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 416
Size: 336 Color: 292
Size: 252 Color: 27

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 429
Size: 327 Color: 268
Size: 251 Color: 11

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 492
Size: 256 Color: 52
Size: 252 Color: 22

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 378
Size: 312 Color: 232
Size: 306 Color: 220

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 358
Size: 344 Color: 305
Size: 284 Color: 174

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 470
Size: 277 Color: 138
Size: 264 Color: 89

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 483
Size: 272 Color: 121
Size: 250 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 480
Size: 278 Color: 140
Size: 250 Color: 6

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 447
Size: 284 Color: 172
Size: 278 Color: 142

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 356
Size: 347 Color: 315
Size: 282 Color: 167

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 468
Size: 276 Color: 136
Size: 266 Color: 102

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 418
Size: 307 Color: 223
Size: 279 Color: 150

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 375
Size: 341 Color: 298
Size: 280 Color: 153

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 372
Size: 357 Color: 332
Size: 265 Color: 96

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 408
Size: 334 Color: 287
Size: 258 Color: 57

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 445
Size: 308 Color: 225
Size: 255 Color: 46

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 482
Size: 265 Color: 97
Size: 260 Color: 62

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 437
Size: 299 Color: 208
Size: 276 Color: 134

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 466
Size: 280 Color: 155
Size: 262 Color: 74

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 439
Size: 309 Color: 226
Size: 266 Color: 101

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 398
Size: 344 Color: 304
Size: 257 Color: 53

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 451
Size: 282 Color: 163
Size: 278 Color: 143

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 432
Size: 316 Color: 240
Size: 261 Color: 69

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 405
Size: 323 Color: 254
Size: 270 Color: 116

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 353
Size: 368 Color: 348
Size: 263 Color: 79

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 367
Size: 347 Color: 312
Size: 277 Color: 139

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 438
Size: 292 Color: 193
Size: 283 Color: 168

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 349
Size: 320 Color: 252
Size: 313 Color: 239

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 311
Size: 331 Color: 280
Size: 324 Color: 258

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 427
Size: 330 Color: 279
Size: 250 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 339
Size: 360 Color: 336
Size: 281 Color: 156

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 371
Size: 344 Color: 308
Size: 279 Color: 147

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 497
Size: 252 Color: 28
Size: 252 Color: 21

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 441
Size: 289 Color: 188
Size: 276 Color: 131

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 463
Size: 280 Color: 154
Size: 265 Color: 99

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 478
Size: 281 Color: 161
Size: 250 Color: 5

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 430
Size: 313 Color: 237
Size: 264 Color: 92

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 334
Size: 334 Color: 286
Size: 310 Color: 228

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 475
Size: 283 Color: 170
Size: 254 Color: 39

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 377
Size: 368 Color: 351
Size: 251 Color: 9

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 314
Size: 347 Color: 313
Size: 307 Color: 221

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 417
Size: 312 Color: 234
Size: 274 Color: 126

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 421
Size: 296 Color: 203
Size: 288 Color: 185

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 460
Size: 293 Color: 195
Size: 260 Color: 65

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 413
Size: 312 Color: 236
Size: 277 Color: 137

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 355
Size: 323 Color: 255
Size: 306 Color: 219

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 486
Size: 263 Color: 86
Size: 258 Color: 58

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 469
Size: 276 Color: 135
Size: 265 Color: 95

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 488
Size: 260 Color: 67
Size: 258 Color: 54

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 457
Size: 302 Color: 213
Size: 252 Color: 17

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 422
Size: 329 Color: 275
Size: 254 Color: 41

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 449
Size: 309 Color: 227
Size: 252 Color: 18

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 481
Size: 274 Color: 124
Size: 253 Color: 33

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 462
Size: 287 Color: 183
Size: 259 Color: 60

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 443
Size: 305 Color: 217
Size: 260 Color: 64

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 376
Size: 368 Color: 350
Size: 252 Color: 26

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 461
Size: 287 Color: 179
Size: 265 Color: 100

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 442
Size: 311 Color: 229
Size: 254 Color: 40

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 329
Size: 354 Color: 327
Size: 292 Color: 192

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 394
Size: 342 Color: 301
Size: 262 Color: 75

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 343
Size: 348 Color: 316
Size: 291 Color: 191

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 454
Size: 282 Color: 165
Size: 276 Color: 132

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 360
Size: 317 Color: 245
Size: 311 Color: 231

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 392
Size: 317 Color: 244
Size: 289 Color: 187

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 369
Size: 341 Color: 299
Size: 282 Color: 164

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 395
Size: 328 Color: 270
Size: 275 Color: 129

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 342
Size: 342 Color: 302
Size: 298 Color: 205

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 386
Size: 324 Color: 261
Size: 287 Color: 182

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 400
Size: 336 Color: 290
Size: 262 Color: 76

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 383
Size: 344 Color: 307
Size: 268 Color: 109

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 433
Size: 306 Color: 218
Size: 270 Color: 115

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 464
Size: 274 Color: 125
Size: 270 Color: 117

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 389
Size: 324 Color: 260
Size: 283 Color: 169

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 390
Size: 352 Color: 322
Size: 255 Color: 47

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 345
Size: 350 Color: 320
Size: 286 Color: 177

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 407
Size: 311 Color: 230
Size: 281 Color: 157

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 419
Size: 318 Color: 246
Size: 267 Color: 108

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 368
Size: 320 Color: 250
Size: 303 Color: 215

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 325 Color: 262
Size: 280 Color: 151
Size: 396 Color: 393

Total size: 167167
Total free space: 0

