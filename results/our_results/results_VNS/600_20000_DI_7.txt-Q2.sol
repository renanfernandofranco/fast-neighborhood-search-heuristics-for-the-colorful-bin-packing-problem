Capicity Bin: 15616
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 26
Items: 
Size: 746 Color: 0
Size: 744 Color: 0
Size: 696 Color: 0
Size: 688 Color: 0
Size: 680 Color: 1
Size: 674 Color: 1
Size: 672 Color: 1
Size: 672 Color: 1
Size: 664 Color: 0
Size: 628 Color: 0
Size: 624 Color: 1
Size: 612 Color: 0
Size: 600 Color: 0
Size: 600 Color: 0
Size: 576 Color: 1
Size: 576 Color: 0
Size: 576 Color: 0
Size: 576 Color: 0
Size: 552 Color: 1
Size: 544 Color: 0
Size: 532 Color: 1
Size: 528 Color: 1
Size: 496 Color: 1
Size: 472 Color: 1
Size: 464 Color: 1
Size: 424 Color: 1

Bin 2: 0 of cap free
Amount of items: 12
Items: 
Size: 7810 Color: 1
Size: 840 Color: 0
Size: 832 Color: 1
Size: 824 Color: 0
Size: 816 Color: 0
Size: 808 Color: 1
Size: 808 Color: 0
Size: 758 Color: 0
Size: 720 Color: 1
Size: 704 Color: 1
Size: 432 Color: 1
Size: 264 Color: 0

Bin 3: 0 of cap free
Amount of items: 9
Items: 
Size: 7812 Color: 1
Size: 1120 Color: 0
Size: 1072 Color: 1
Size: 1056 Color: 0
Size: 1024 Color: 1
Size: 1022 Color: 0
Size: 968 Color: 0
Size: 918 Color: 0
Size: 624 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8104 Color: 0
Size: 6500 Color: 1
Size: 1012 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8400 Color: 1
Size: 6880 Color: 0
Size: 336 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8892 Color: 1
Size: 6504 Color: 0
Size: 220 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8884 Color: 0
Size: 6412 Color: 0
Size: 320 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11135 Color: 0
Size: 3735 Color: 1
Size: 746 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11147 Color: 0
Size: 3065 Color: 0
Size: 1404 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11131 Color: 1
Size: 4157 Color: 1
Size: 328 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 1
Size: 3940 Color: 0
Size: 416 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 1
Size: 2661 Color: 0
Size: 530 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12441 Color: 0
Size: 2647 Color: 0
Size: 528 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12810 Color: 0
Size: 2598 Color: 0
Size: 208 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12827 Color: 0
Size: 2325 Color: 1
Size: 464 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 1
Size: 1764 Color: 1
Size: 820 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13068 Color: 1
Size: 1364 Color: 0
Size: 1184 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13095 Color: 0
Size: 1513 Color: 0
Size: 1008 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 1
Size: 1528 Color: 0
Size: 976 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 0
Size: 2024 Color: 0
Size: 400 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 0
Size: 1458 Color: 0
Size: 824 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 0
Size: 1358 Color: 0
Size: 784 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13508 Color: 1
Size: 1422 Color: 0
Size: 686 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13566 Color: 1
Size: 1708 Color: 1
Size: 342 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 0
Size: 1342 Color: 1
Size: 614 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13674 Color: 1
Size: 1522 Color: 0
Size: 420 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13710 Color: 0
Size: 1390 Color: 0
Size: 516 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13682 Color: 1
Size: 1320 Color: 1
Size: 614 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 1
Size: 1296 Color: 0
Size: 530 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 0
Size: 1467 Color: 0
Size: 304 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 0
Size: 1296 Color: 0
Size: 440 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 0
Size: 1330 Color: 1
Size: 376 Color: 0

Bin 33: 1 of cap free
Amount of items: 7
Items: 
Size: 7809 Color: 0
Size: 1478 Color: 1
Size: 1448 Color: 1
Size: 1420 Color: 0
Size: 1384 Color: 0
Size: 1324 Color: 1
Size: 752 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 10167 Color: 1
Size: 5048 Color: 0
Size: 400 Color: 0

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 10548 Color: 0
Size: 5067 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 11567 Color: 0
Size: 3784 Color: 1
Size: 264 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 11859 Color: 0
Size: 3180 Color: 0
Size: 576 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 1
Size: 3372 Color: 1
Size: 304 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 0
Size: 2467 Color: 1
Size: 404 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 12811 Color: 0
Size: 2310 Color: 1
Size: 494 Color: 1

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 12967 Color: 0
Size: 2648 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 13079 Color: 0
Size: 2060 Color: 0
Size: 476 Color: 1

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 13203 Color: 1
Size: 2076 Color: 0
Size: 336 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 13270 Color: 1
Size: 2065 Color: 0
Size: 280 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 13325 Color: 1
Size: 1802 Color: 1
Size: 488 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 1
Size: 1525 Color: 0
Size: 492 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 13719 Color: 0
Size: 1072 Color: 1
Size: 824 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 1
Size: 1008 Color: 1
Size: 830 Color: 0

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 13787 Color: 0
Size: 1828 Color: 1

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 13801 Color: 0
Size: 1814 Color: 1

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 7816 Color: 0
Size: 6502 Color: 1
Size: 1296 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 1
Size: 5142 Color: 0
Size: 160 Color: 1

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 10754 Color: 1
Size: 4860 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 10708 Color: 0
Size: 4394 Color: 0
Size: 512 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 0
Size: 4054 Color: 0
Size: 480 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 11834 Color: 1
Size: 3154 Color: 0
Size: 626 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 12420 Color: 0
Size: 2442 Color: 1
Size: 752 Color: 0

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 12724 Color: 1
Size: 2890 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 1
Size: 2088 Color: 0
Size: 424 Color: 1

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 1
Size: 2342 Color: 0

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 1
Size: 1682 Color: 0

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 1
Size: 1584 Color: 0
Size: 80 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 0
Size: 3725 Color: 1
Size: 376 Color: 0

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 0
Size: 2477 Color: 1
Size: 344 Color: 1

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 13017 Color: 0
Size: 2596 Color: 1

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 13132 Color: 1
Size: 2481 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 13517 Color: 0
Size: 1184 Color: 0
Size: 912 Color: 1

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 1
Size: 1603 Color: 0

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 8718 Color: 0
Size: 6622 Color: 1
Size: 272 Color: 1

Bin 70: 4 of cap free
Amount of items: 6
Items: 
Size: 9128 Color: 0
Size: 1592 Color: 1
Size: 1532 Color: 1
Size: 1528 Color: 0
Size: 1484 Color: 0
Size: 348 Color: 1

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 11572 Color: 1
Size: 4040 Color: 0

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 11927 Color: 1
Size: 3365 Color: 0
Size: 320 Color: 0

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 12289 Color: 0
Size: 3131 Color: 0
Size: 192 Color: 1

Bin 74: 4 of cap free
Amount of items: 4
Items: 
Size: 13336 Color: 1
Size: 2090 Color: 0
Size: 140 Color: 1
Size: 46 Color: 0

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 13444 Color: 1
Size: 2168 Color: 0

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 1
Size: 1622 Color: 0

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 1
Size: 1590 Color: 0

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 10644 Color: 1
Size: 4595 Color: 1
Size: 372 Color: 0

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 11923 Color: 0
Size: 3688 Color: 1

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 1
Size: 2124 Color: 0

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 1
Size: 1775 Color: 0

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 8782 Color: 1
Size: 6508 Color: 1
Size: 320 Color: 0

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 9820 Color: 0
Size: 5404 Color: 1
Size: 386 Color: 0

Bin 84: 7 of cap free
Amount of items: 10
Items: 
Size: 7811 Color: 1
Size: 992 Color: 1
Size: 968 Color: 0
Size: 960 Color: 0
Size: 908 Color: 0
Size: 880 Color: 1
Size: 880 Color: 0
Size: 876 Color: 1
Size: 840 Color: 1
Size: 494 Color: 0

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 8719 Color: 0
Size: 6506 Color: 1
Size: 384 Color: 0

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 9473 Color: 0
Size: 5352 Color: 1
Size: 784 Color: 0

Bin 87: 7 of cap free
Amount of items: 3
Items: 
Size: 11563 Color: 0
Size: 3770 Color: 1
Size: 276 Color: 1

Bin 88: 7 of cap free
Amount of items: 3
Items: 
Size: 12429 Color: 0
Size: 1784 Color: 1
Size: 1396 Color: 1

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 0
Size: 2021 Color: 0
Size: 1148 Color: 1

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 0
Size: 2167 Color: 1

Bin 91: 7 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 1
Size: 1621 Color: 0

Bin 92: 7 of cap free
Amount of items: 2
Items: 
Size: 13960 Color: 0
Size: 1649 Color: 1

Bin 93: 7 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 0
Size: 1581 Color: 1

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 9576 Color: 1
Size: 6032 Color: 0

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 11067 Color: 0
Size: 4541 Color: 1

Bin 96: 8 of cap free
Amount of items: 2
Items: 
Size: 11380 Color: 1
Size: 4228 Color: 0

Bin 97: 8 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 1
Size: 1751 Color: 0

Bin 98: 9 of cap free
Amount of items: 3
Items: 
Size: 9537 Color: 1
Size: 5604 Color: 1
Size: 466 Color: 0

Bin 99: 9 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 0
Size: 1893 Color: 1
Size: 1300 Color: 1

Bin 100: 9 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 0
Size: 2209 Color: 1

Bin 101: 10 of cap free
Amount of items: 3
Items: 
Size: 9140 Color: 0
Size: 6146 Color: 1
Size: 320 Color: 0

Bin 102: 10 of cap free
Amount of items: 3
Items: 
Size: 10006 Color: 1
Size: 5328 Color: 0
Size: 272 Color: 1

Bin 103: 10 of cap free
Amount of items: 2
Items: 
Size: 12990 Color: 1
Size: 2616 Color: 0

Bin 104: 11 of cap free
Amount of items: 3
Items: 
Size: 9496 Color: 1
Size: 5749 Color: 1
Size: 360 Color: 0

Bin 105: 11 of cap free
Amount of items: 2
Items: 
Size: 13693 Color: 1
Size: 1912 Color: 0

Bin 106: 12 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 0
Size: 4836 Color: 1
Size: 140 Color: 0

Bin 107: 12 of cap free
Amount of items: 3
Items: 
Size: 11948 Color: 1
Size: 3496 Color: 0
Size: 160 Color: 0

Bin 108: 12 of cap free
Amount of items: 2
Items: 
Size: 13916 Color: 0
Size: 1688 Color: 1

Bin 109: 13 of cap free
Amount of items: 3
Items: 
Size: 10103 Color: 1
Size: 5112 Color: 0
Size: 388 Color: 0

Bin 110: 13 of cap free
Amount of items: 2
Items: 
Size: 12159 Color: 1
Size: 3444 Color: 0

Bin 111: 13 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 1
Size: 2412 Color: 0

Bin 112: 14 of cap free
Amount of items: 3
Items: 
Size: 8717 Color: 0
Size: 6505 Color: 1
Size: 380 Color: 0

Bin 113: 14 of cap free
Amount of items: 3
Items: 
Size: 9450 Color: 1
Size: 5744 Color: 1
Size: 408 Color: 0

Bin 114: 14 of cap free
Amount of items: 4
Items: 
Size: 10564 Color: 0
Size: 1892 Color: 1
Size: 1612 Color: 1
Size: 1534 Color: 0

Bin 115: 14 of cap free
Amount of items: 2
Items: 
Size: 11994 Color: 1
Size: 3608 Color: 0

Bin 116: 15 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 1
Size: 1561 Color: 0

Bin 117: 16 of cap free
Amount of items: 2
Items: 
Size: 11964 Color: 0
Size: 3636 Color: 1

Bin 118: 18 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 1
Size: 2408 Color: 0

Bin 119: 18 of cap free
Amount of items: 3
Items: 
Size: 13926 Color: 1
Size: 1556 Color: 0
Size: 116 Color: 1

Bin 120: 19 of cap free
Amount of items: 2
Items: 
Size: 12645 Color: 0
Size: 2952 Color: 1

Bin 121: 19 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 1
Size: 1958 Color: 0

Bin 122: 20 of cap free
Amount of items: 2
Items: 
Size: 11432 Color: 0
Size: 4164 Color: 1

Bin 123: 20 of cap free
Amount of items: 2
Items: 
Size: 12574 Color: 1
Size: 3022 Color: 0

Bin 124: 21 of cap free
Amount of items: 2
Items: 
Size: 11492 Color: 1
Size: 4103 Color: 0

Bin 125: 21 of cap free
Amount of items: 2
Items: 
Size: 13297 Color: 1
Size: 2298 Color: 0

Bin 126: 21 of cap free
Amount of items: 2
Items: 
Size: 13728 Color: 0
Size: 1867 Color: 1

Bin 127: 22 of cap free
Amount of items: 2
Items: 
Size: 13454 Color: 0
Size: 2140 Color: 1

Bin 128: 22 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 0
Size: 2022 Color: 1

Bin 129: 22 of cap free
Amount of items: 2
Items: 
Size: 13846 Color: 1
Size: 1748 Color: 0

Bin 130: 22 of cap free
Amount of items: 2
Items: 
Size: 13958 Color: 0
Size: 1636 Color: 1

Bin 131: 23 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 0
Size: 2670 Color: 0
Size: 2339 Color: 1

Bin 132: 23 of cap free
Amount of items: 2
Items: 
Size: 13559 Color: 1
Size: 2034 Color: 0

Bin 133: 24 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 0
Size: 1812 Color: 1

Bin 134: 25 of cap free
Amount of items: 2
Items: 
Size: 11499 Color: 0
Size: 4092 Color: 1

Bin 135: 26 of cap free
Amount of items: 2
Items: 
Size: 13052 Color: 0
Size: 2538 Color: 1

Bin 136: 28 of cap free
Amount of items: 2
Items: 
Size: 13684 Color: 1
Size: 1904 Color: 0

Bin 137: 29 of cap free
Amount of items: 2
Items: 
Size: 12508 Color: 0
Size: 3079 Color: 1

Bin 138: 30 of cap free
Amount of items: 2
Items: 
Size: 12686 Color: 1
Size: 2900 Color: 0

Bin 139: 31 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 1
Size: 1715 Color: 0

Bin 140: 32 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 0
Size: 4616 Color: 1
Size: 176 Color: 1

Bin 141: 32 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 0
Size: 1806 Color: 1

Bin 142: 33 of cap free
Amount of items: 2
Items: 
Size: 13912 Color: 0
Size: 1671 Color: 1

Bin 143: 35 of cap free
Amount of items: 2
Items: 
Size: 12150 Color: 1
Size: 3431 Color: 0

Bin 144: 35 of cap free
Amount of items: 2
Items: 
Size: 13377 Color: 1
Size: 2204 Color: 0

Bin 145: 35 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 1
Size: 2101 Color: 0

Bin 146: 36 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 1
Size: 6520 Color: 1
Size: 1148 Color: 0

Bin 147: 36 of cap free
Amount of items: 3
Items: 
Size: 11579 Color: 0
Size: 3791 Color: 1
Size: 210 Color: 1

Bin 148: 36 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 1
Size: 3780 Color: 0

Bin 149: 37 of cap free
Amount of items: 2
Items: 
Size: 13451 Color: 0
Size: 2128 Color: 1

Bin 150: 38 of cap free
Amount of items: 2
Items: 
Size: 12044 Color: 1
Size: 3534 Color: 0

Bin 151: 38 of cap free
Amount of items: 2
Items: 
Size: 12796 Color: 1
Size: 2782 Color: 0

Bin 152: 45 of cap free
Amount of items: 2
Items: 
Size: 13611 Color: 0
Size: 1960 Color: 1

Bin 153: 46 of cap free
Amount of items: 2
Items: 
Size: 12502 Color: 1
Size: 3068 Color: 0

Bin 154: 46 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 1
Size: 1850 Color: 0

Bin 155: 52 of cap free
Amount of items: 2
Items: 
Size: 11352 Color: 0
Size: 4212 Color: 1

Bin 156: 53 of cap free
Amount of items: 2
Items: 
Size: 12488 Color: 0
Size: 3075 Color: 1

Bin 157: 54 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 0
Size: 1614 Color: 1

Bin 158: 55 of cap free
Amount of items: 2
Items: 
Size: 13178 Color: 1
Size: 2383 Color: 0

Bin 159: 55 of cap free
Amount of items: 2
Items: 
Size: 13756 Color: 0
Size: 1805 Color: 1

Bin 160: 61 of cap free
Amount of items: 2
Items: 
Size: 9804 Color: 1
Size: 5751 Color: 0

Bin 161: 64 of cap free
Amount of items: 2
Items: 
Size: 12028 Color: 1
Size: 3524 Color: 0

Bin 162: 64 of cap free
Amount of items: 2
Items: 
Size: 12140 Color: 0
Size: 3412 Color: 1

Bin 163: 64 of cap free
Amount of items: 2
Items: 
Size: 13196 Color: 0
Size: 2356 Color: 1

Bin 164: 65 of cap free
Amount of items: 2
Items: 
Size: 12343 Color: 1
Size: 3208 Color: 0

Bin 165: 68 of cap free
Amount of items: 2
Items: 
Size: 7820 Color: 1
Size: 7728 Color: 0

Bin 166: 72 of cap free
Amount of items: 2
Items: 
Size: 11396 Color: 1
Size: 4148 Color: 0

Bin 167: 72 of cap free
Amount of items: 2
Items: 
Size: 13524 Color: 0
Size: 2020 Color: 1

Bin 168: 75 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 0
Size: 1933 Color: 1

Bin 169: 78 of cap free
Amount of items: 2
Items: 
Size: 13348 Color: 1
Size: 2190 Color: 0

Bin 170: 79 of cap free
Amount of items: 2
Items: 
Size: 10693 Color: 1
Size: 4844 Color: 0

Bin 171: 84 of cap free
Amount of items: 2
Items: 
Size: 12480 Color: 0
Size: 3052 Color: 1

Bin 172: 86 of cap free
Amount of items: 2
Items: 
Size: 12862 Color: 0
Size: 2668 Color: 1

Bin 173: 89 of cap free
Amount of items: 2
Items: 
Size: 11614 Color: 0
Size: 3913 Color: 1

Bin 174: 90 of cap free
Amount of items: 2
Items: 
Size: 13428 Color: 1
Size: 2098 Color: 0

Bin 175: 93 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 1
Size: 3739 Color: 0

Bin 176: 94 of cap free
Amount of items: 2
Items: 
Size: 12641 Color: 0
Size: 2881 Color: 1

Bin 177: 96 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 1
Size: 2863 Color: 0

Bin 178: 98 of cap free
Amount of items: 2
Items: 
Size: 9250 Color: 1
Size: 6268 Color: 0

Bin 179: 98 of cap free
Amount of items: 2
Items: 
Size: 11094 Color: 1
Size: 4424 Color: 0

Bin 180: 98 of cap free
Amount of items: 2
Items: 
Size: 13607 Color: 0
Size: 1911 Color: 1

Bin 181: 99 of cap free
Amount of items: 3
Items: 
Size: 10629 Color: 1
Size: 3338 Color: 1
Size: 1550 Color: 0

Bin 182: 106 of cap free
Amount of items: 2
Items: 
Size: 11378 Color: 1
Size: 4132 Color: 0

Bin 183: 106 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 1
Size: 1710 Color: 0

Bin 184: 108 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 1
Size: 2360 Color: 0

Bin 185: 112 of cap free
Amount of items: 2
Items: 
Size: 10088 Color: 1
Size: 5416 Color: 0

Bin 186: 113 of cap free
Amount of items: 2
Items: 
Size: 12846 Color: 0
Size: 2657 Color: 1

Bin 187: 116 of cap free
Amount of items: 2
Items: 
Size: 11300 Color: 0
Size: 4200 Color: 1

Bin 188: 129 of cap free
Amount of items: 2
Items: 
Size: 8980 Color: 1
Size: 6507 Color: 0

Bin 189: 130 of cap free
Amount of items: 2
Items: 
Size: 9788 Color: 1
Size: 5698 Color: 0

Bin 190: 130 of cap free
Amount of items: 2
Items: 
Size: 12757 Color: 1
Size: 2729 Color: 0

Bin 191: 136 of cap free
Amount of items: 7
Items: 
Size: 7818 Color: 1
Size: 1382 Color: 0
Size: 1300 Color: 0
Size: 1300 Color: 0
Size: 1296 Color: 1
Size: 1248 Color: 1
Size: 1136 Color: 1

Bin 192: 142 of cap free
Amount of items: 2
Items: 
Size: 12282 Color: 1
Size: 3192 Color: 0

Bin 193: 144 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 1
Size: 6264 Color: 0

Bin 194: 145 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 0
Size: 2491 Color: 1

Bin 195: 149 of cap free
Amount of items: 2
Items: 
Size: 10346 Color: 0
Size: 5121 Color: 1

Bin 196: 153 of cap free
Amount of items: 2
Items: 
Size: 12088 Color: 0
Size: 3375 Color: 1

Bin 197: 154 of cap free
Amount of items: 2
Items: 
Size: 10660 Color: 1
Size: 4802 Color: 0

Bin 198: 252 of cap free
Amount of items: 39
Items: 
Size: 536 Color: 0
Size: 528 Color: 0
Size: 512 Color: 0
Size: 504 Color: 0
Size: 464 Color: 1
Size: 464 Color: 1
Size: 464 Color: 1
Size: 464 Color: 0
Size: 460 Color: 1
Size: 456 Color: 1
Size: 456 Color: 0
Size: 436 Color: 0
Size: 432 Color: 0
Size: 416 Color: 1
Size: 416 Color: 1
Size: 408 Color: 1
Size: 404 Color: 1
Size: 404 Color: 0
Size: 368 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 360 Color: 1
Size: 360 Color: 0
Size: 360 Color: 0
Size: 360 Color: 0
Size: 354 Color: 0
Size: 352 Color: 0
Size: 348 Color: 0
Size: 344 Color: 1
Size: 340 Color: 0
Size: 336 Color: 0
Size: 334 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 316 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1

Bin 199: 9700 of cap free
Amount of items: 21
Items: 
Size: 316 Color: 0
Size: 304 Color: 1
Size: 304 Color: 0
Size: 302 Color: 1
Size: 296 Color: 1
Size: 292 Color: 1
Size: 292 Color: 0
Size: 288 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 284 Color: 1
Size: 280 Color: 0
Size: 276 Color: 0
Size: 276 Color: 0
Size: 272 Color: 1
Size: 268 Color: 1
Size: 264 Color: 1
Size: 264 Color: 1
Size: 256 Color: 0
Size: 218 Color: 0

Total size: 3091968
Total free space: 15616

