Capicity Bin: 2404
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 152
Size: 890 Color: 127
Size: 44 Color: 11

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1658 Color: 158
Size: 702 Color: 117
Size: 24 Color: 6
Size: 20 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 163
Size: 456 Color: 99
Size: 210 Color: 70

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 167
Size: 497 Color: 105
Size: 106 Color: 45

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 169
Size: 403 Color: 96
Size: 192 Color: 65

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 171
Size: 517 Color: 107
Size: 58 Color: 20

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 172
Size: 521 Color: 108
Size: 50 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 179
Size: 406 Color: 97
Size: 44 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1975 Color: 181
Size: 359 Color: 92
Size: 70 Color: 29

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 182
Size: 294 Color: 79
Size: 128 Color: 52

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 186
Size: 295 Color: 80
Size: 82 Color: 34

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 191
Size: 253 Color: 74
Size: 92 Color: 37

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 195
Size: 170 Color: 62
Size: 140 Color: 55

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2113 Color: 197
Size: 275 Color: 77
Size: 16 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 198
Size: 196 Color: 66
Size: 94 Color: 38

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 199
Size: 200 Color: 68
Size: 86 Color: 35

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 201
Size: 188 Color: 64
Size: 62 Color: 24

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 154
Size: 814 Color: 123
Size: 40 Color: 10

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 164
Size: 622 Color: 116
Size: 20 Color: 5

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 165
Size: 622 Color: 115

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 166
Size: 502 Color: 106
Size: 108 Color: 46

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 168
Size: 601 Color: 112

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1845 Color: 173
Size: 558 Color: 110

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 180
Size: 242 Color: 73
Size: 198 Color: 67

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 187
Size: 364 Color: 93

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 190
Size: 349 Color: 89

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 2063 Color: 192
Size: 340 Color: 88

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 2134 Color: 200
Size: 269 Color: 76

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 151
Size: 915 Color: 130
Size: 48 Color: 13

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 155
Size: 805 Color: 122
Size: 40 Color: 9

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 161
Size: 707 Color: 118
Size: 12 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 162
Size: 458 Color: 100
Size: 226 Color: 71

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1909 Color: 175
Size: 493 Color: 104

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1921 Color: 177
Size: 481 Color: 103

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 185
Size: 379 Color: 95

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 2047 Color: 188
Size: 354 Color: 91

Bin 37: 4 of cap free
Amount of items: 9
Items: 
Size: 615 Color: 113
Size: 322 Color: 85
Size: 311 Color: 83
Size: 296 Color: 81
Size: 282 Color: 78
Size: 262 Color: 75
Size: 168 Color: 61
Size: 72 Color: 31
Size: 72 Color: 30

Bin 38: 4 of cap free
Amount of items: 5
Items: 
Size: 1209 Color: 142
Size: 971 Color: 132
Size: 88 Color: 36
Size: 68 Color: 27
Size: 64 Color: 26

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1813 Color: 170
Size: 587 Color: 111

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1987 Color: 183
Size: 413 Color: 98

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 2051 Color: 189
Size: 349 Color: 90

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 2101 Color: 196
Size: 299 Color: 82

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 145
Size: 1001 Color: 134
Size: 60 Color: 21

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 178
Size: 467 Color: 101

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 2066 Color: 193
Size: 333 Color: 87

Bin 46: 6 of cap free
Amount of items: 4
Items: 
Size: 1602 Color: 157
Size: 730 Color: 121
Size: 36 Color: 8
Size: 30 Color: 7

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 2083 Color: 194
Size: 315 Color: 84

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 156
Size: 831 Color: 124

Bin 49: 7 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 160
Size: 718 Color: 120
Size: 12 Color: 1

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 148
Size: 946 Color: 131
Size: 56 Color: 17

Bin 51: 9 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 174
Size: 537 Color: 109

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1918 Color: 176
Size: 477 Color: 102

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 2017 Color: 184
Size: 378 Color: 94

Bin 54: 10 of cap free
Amount of items: 2
Items: 
Size: 1206 Color: 141
Size: 1188 Color: 138

Bin 55: 12 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 150
Size: 914 Color: 129
Size: 48 Color: 14

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 159
Size: 713 Color: 119
Size: 16 Color: 2

Bin 57: 15 of cap free
Amount of items: 5
Items: 
Size: 1203 Color: 139
Size: 619 Color: 114
Size: 323 Color: 86
Size: 176 Color: 63
Size: 68 Color: 28

Bin 58: 16 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 153
Size: 842 Color: 125

Bin 59: 27 of cap free
Amount of items: 2
Items: 
Size: 1375 Color: 146
Size: 1002 Color: 135

Bin 60: 30 of cap free
Amount of items: 18
Items: 
Size: 242 Color: 72
Size: 204 Color: 69
Size: 166 Color: 60
Size: 160 Color: 59
Size: 160 Color: 58
Size: 144 Color: 57
Size: 142 Color: 56
Size: 140 Color: 54
Size: 136 Color: 53
Size: 124 Color: 51
Size: 104 Color: 44
Size: 102 Color: 43
Size: 100 Color: 42
Size: 98 Color: 41
Size: 98 Color: 40
Size: 94 Color: 39
Size: 80 Color: 33
Size: 80 Color: 32

Bin 61: 30 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 144
Size: 1044 Color: 136
Size: 60 Color: 22

Bin 62: 36 of cap free
Amount of items: 4
Items: 
Size: 1241 Color: 143
Size: 1001 Color: 133
Size: 64 Color: 25
Size: 62 Color: 23

Bin 63: 41 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 149
Size: 904 Color: 128
Size: 52 Color: 16

Bin 64: 52 of cap free
Amount of items: 4
Items: 
Size: 1379 Color: 147
Size: 859 Color: 126
Size: 58 Color: 19
Size: 56 Color: 18

Bin 65: 64 of cap free
Amount of items: 2
Items: 
Size: 1205 Color: 140
Size: 1135 Color: 137

Bin 66: 1930 of cap free
Amount of items: 4
Items: 
Size: 122 Color: 50
Size: 122 Color: 49
Size: 120 Color: 48
Size: 110 Color: 47

Total size: 156260
Total free space: 2404

