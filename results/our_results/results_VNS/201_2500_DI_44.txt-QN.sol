Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 153
Size: 678 Color: 124
Size: 28 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 163
Size: 499 Color: 116
Size: 78 Color: 41

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 167
Size: 334 Color: 97
Size: 144 Color: 64

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 168
Size: 399 Color: 104
Size: 78 Color: 42

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 170
Size: 391 Color: 102
Size: 78 Color: 43

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 171
Size: 417 Color: 107
Size: 48 Color: 23

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 173
Size: 402 Color: 105
Size: 36 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 177
Size: 315 Color: 94
Size: 62 Color: 30

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 180
Size: 295 Color: 91
Size: 62 Color: 31

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 181
Size: 311 Color: 93
Size: 42 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 182
Size: 220 Color: 80
Size: 130 Color: 57

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 186
Size: 243 Color: 85
Size: 48 Color: 25

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 189
Size: 217 Color: 78
Size: 56 Color: 28

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 190
Size: 228 Color: 81
Size: 32 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 191
Size: 201 Color: 75
Size: 58 Color: 29

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 193
Size: 213 Color: 76
Size: 42 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 194
Size: 214 Color: 77
Size: 40 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 195
Size: 193 Color: 72
Size: 46 Color: 20

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 196
Size: 197 Color: 74
Size: 38 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 199
Size: 191 Color: 71
Size: 36 Color: 6

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 200
Size: 138 Color: 60
Size: 72 Color: 36

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 201
Size: 173 Color: 69
Size: 34 Color: 5

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 150
Size: 717 Color: 130
Size: 38 Color: 10

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1429 Color: 158
Size: 606 Color: 122

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 162
Size: 481 Color: 111
Size: 96 Color: 48

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1563 Color: 169
Size: 472 Color: 110

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 179
Size: 361 Color: 99

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 185
Size: 242 Color: 84
Size: 64 Color: 33

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1746 Color: 187
Size: 289 Color: 89

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 192
Size: 257 Color: 87

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 198
Size: 229 Color: 83

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1443 Color: 160
Size: 591 Color: 121

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1539 Color: 165
Size: 495 Color: 115

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1752 Color: 188
Size: 278 Color: 88
Size: 4 Color: 1

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 155
Size: 507 Color: 117
Size: 136 Color: 59

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 175
Size: 395 Color: 103

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 183
Size: 334 Color: 98

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 184
Size: 323 Color: 96
Size: 4 Color: 0

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1805 Color: 197
Size: 228 Color: 82

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 151
Size: 691 Color: 126
Size: 38 Color: 8

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 156
Size: 316 Color: 95
Size: 311 Color: 92

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 166
Size: 482 Color: 112

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1649 Color: 176
Size: 382 Color: 101

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1439 Color: 159
Size: 590 Color: 120

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1663 Color: 178
Size: 366 Color: 100

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1327 Color: 152
Size: 701 Color: 127

Bin 47: 8 of cap free
Amount of items: 2
Items: 
Size: 1447 Color: 161
Size: 581 Color: 119

Bin 48: 8 of cap free
Amount of items: 2
Items: 
Size: 1578 Color: 172
Size: 450 Color: 109

Bin 49: 9 of cap free
Amount of items: 7
Items: 
Size: 1019 Color: 138
Size: 294 Color: 90
Size: 244 Color: 86
Size: 218 Color: 79
Size: 100 Color: 53
Size: 76 Color: 39
Size: 76 Color: 38

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1603 Color: 174
Size: 424 Color: 108

Bin 51: 11 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 157
Size: 493 Color: 114
Size: 118 Color: 56

Bin 52: 14 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 149
Size: 756 Color: 134
Size: 40 Color: 11

Bin 53: 16 of cap free
Amount of items: 2
Items: 
Size: 1171 Color: 142
Size: 849 Color: 137

Bin 54: 16 of cap free
Amount of items: 2
Items: 
Size: 1498 Color: 164
Size: 522 Color: 118

Bin 55: 27 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 154
Size: 650 Color: 123
Size: 20 Color: 2

Bin 56: 33 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 148
Size: 754 Color: 133
Size: 40 Color: 12

Bin 57: 38 of cap free
Amount of items: 4
Items: 
Size: 1193 Color: 146
Size: 721 Color: 131
Size: 44 Color: 19
Size: 40 Color: 15

Bin 58: 38 of cap free
Amount of items: 4
Items: 
Size: 1197 Color: 147
Size: 721 Color: 132
Size: 40 Color: 14
Size: 40 Color: 13

Bin 59: 40 of cap free
Amount of items: 4
Items: 
Size: 1022 Color: 140
Size: 846 Color: 135
Size: 64 Color: 35
Size: 64 Color: 34

Bin 60: 45 of cap free
Amount of items: 4
Items: 
Size: 1021 Color: 139
Size: 491 Color: 113
Size: 407 Color: 106
Size: 72 Color: 37

Bin 61: 46 of cap free
Amount of items: 4
Items: 
Size: 1181 Color: 145
Size: 713 Color: 129
Size: 48 Color: 22
Size: 48 Color: 21

Bin 62: 48 of cap free
Amount of items: 4
Items: 
Size: 1177 Color: 144
Size: 713 Color: 128
Size: 50 Color: 26
Size: 48 Color: 24

Bin 63: 55 of cap free
Amount of items: 2
Items: 
Size: 1134 Color: 141
Size: 847 Color: 136

Bin 64: 63 of cap free
Amount of items: 4
Items: 
Size: 1173 Color: 143
Size: 684 Color: 125
Size: 64 Color: 32
Size: 52 Color: 27

Bin 65: 64 of cap free
Amount of items: 15
Items: 
Size: 194 Color: 73
Size: 178 Color: 70
Size: 168 Color: 68
Size: 168 Color: 67
Size: 168 Color: 66
Size: 148 Color: 65
Size: 142 Color: 63
Size: 142 Color: 62
Size: 142 Color: 61
Size: 98 Color: 49
Size: 96 Color: 47
Size: 88 Color: 46
Size: 84 Color: 45
Size: 80 Color: 44
Size: 76 Color: 40

Bin 66: 1376 of cap free
Amount of items: 6
Items: 
Size: 132 Color: 58
Size: 116 Color: 55
Size: 116 Color: 54
Size: 100 Color: 52
Size: 98 Color: 51
Size: 98 Color: 50

Total size: 132340
Total free space: 2036

