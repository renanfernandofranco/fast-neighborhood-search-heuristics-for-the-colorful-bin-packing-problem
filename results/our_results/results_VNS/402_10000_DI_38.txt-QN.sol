Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4452 Color: 281
Size: 3528 Color: 271
Size: 156 Color: 26

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6089 Color: 317
Size: 1228 Color: 196
Size: 819 Color: 157

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 318
Size: 1700 Color: 223
Size: 336 Color: 93

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 319
Size: 1842 Color: 228
Size: 148 Color: 20

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 326
Size: 1638 Color: 221
Size: 182 Color: 42

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6544 Color: 335
Size: 1168 Color: 191
Size: 424 Color: 106

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 337
Size: 1324 Color: 203
Size: 256 Color: 73

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 340
Size: 1000 Color: 176
Size: 536 Color: 119

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 343
Size: 1108 Color: 188
Size: 360 Color: 98

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 347
Size: 946 Color: 172
Size: 488 Color: 114

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6841 Color: 355
Size: 783 Color: 151
Size: 512 Color: 118

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 361
Size: 1012 Color: 179
Size: 200 Color: 52

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 362
Size: 874 Color: 166
Size: 328 Color: 91

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7001 Color: 366
Size: 755 Color: 148
Size: 380 Color: 101

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 372
Size: 696 Color: 138
Size: 350 Color: 96

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7091 Color: 373
Size: 871 Color: 165
Size: 174 Color: 39

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 374
Size: 718 Color: 142
Size: 320 Color: 87

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7106 Color: 375
Size: 546 Color: 122
Size: 484 Color: 113

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7139 Color: 378
Size: 721 Color: 143
Size: 276 Color: 78

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7155 Color: 380
Size: 831 Color: 159
Size: 150 Color: 21

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 381
Size: 708 Color: 140
Size: 272 Color: 77

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 382
Size: 798 Color: 155
Size: 172 Color: 37

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 385
Size: 788 Color: 153
Size: 152 Color: 24

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7197 Color: 386
Size: 771 Color: 150
Size: 168 Color: 35

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7249 Color: 391
Size: 859 Color: 162
Size: 28 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 392
Size: 576 Color: 124
Size: 308 Color: 85

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 393
Size: 738 Color: 145
Size: 144 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7278 Color: 395
Size: 702 Color: 139
Size: 156 Color: 27

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7280 Color: 396
Size: 568 Color: 123
Size: 288 Color: 81

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 397
Size: 711 Color: 141
Size: 142 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 398
Size: 672 Color: 131
Size: 172 Color: 38

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7310 Color: 400
Size: 608 Color: 128
Size: 218 Color: 60

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7321 Color: 402
Size: 681 Color: 136
Size: 134 Color: 9

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 5324 Color: 298
Size: 2811 Color: 254

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 5613 Color: 305
Size: 2522 Color: 249

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 5894 Color: 312
Size: 2241 Color: 239

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6241 Color: 323
Size: 1101 Color: 186
Size: 793 Color: 154

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 332
Size: 1662 Color: 222

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6838 Color: 354
Size: 1001 Color: 177
Size: 296 Color: 84

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 360
Size: 1221 Color: 195

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6937 Color: 363
Size: 1198 Color: 194

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 7171 Color: 383
Size: 964 Color: 174

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 7243 Color: 390
Size: 892 Color: 167

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 7315 Color: 401
Size: 820 Color: 158

Bin 45: 2 of cap free
Amount of items: 5
Items: 
Size: 4116 Color: 279
Size: 3356 Color: 264
Size: 342 Color: 95
Size: 160 Color: 31
Size: 160 Color: 30

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 327
Size: 1544 Color: 215
Size: 242 Color: 68

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 6394 Color: 329
Size: 1740 Color: 226

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 338
Size: 1572 Color: 217

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6733 Color: 349
Size: 1401 Color: 209

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6877 Color: 358
Size: 1257 Color: 199

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 7057 Color: 370
Size: 1077 Color: 183

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 7231 Color: 389
Size: 903 Color: 168

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 346
Size: 1441 Color: 210

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 6845 Color: 356
Size: 1288 Color: 200

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 7131 Color: 377
Size: 1002 Color: 178

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 7211 Color: 387
Size: 922 Color: 170

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 7271 Color: 394
Size: 862 Color: 163

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 7294 Color: 399
Size: 839 Color: 161

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 5447 Color: 300
Size: 2685 Color: 251

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 6818 Color: 353
Size: 1314 Color: 202

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 368
Size: 1102 Color: 187

Bin 62: 5 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 283
Size: 3382 Color: 266
Size: 152 Color: 23

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 5847 Color: 311
Size: 2284 Color: 241

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 6953 Color: 364
Size: 1178 Color: 193

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 7010 Color: 367
Size: 1121 Color: 189

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7218 Color: 388
Size: 913 Color: 169

Bin 67: 6 of cap free
Amount of items: 3
Items: 
Size: 5114 Color: 292
Size: 2876 Color: 255
Size: 140 Color: 14

Bin 68: 7 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 330
Size: 1707 Color: 224
Size: 24 Color: 0

Bin 69: 7 of cap free
Amount of items: 2
Items: 
Size: 6507 Color: 334
Size: 1622 Color: 220

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 371
Size: 1061 Color: 182

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 7107 Color: 376
Size: 1022 Color: 180

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 7182 Color: 384
Size: 947 Color: 173

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 5226 Color: 296
Size: 2902 Color: 257

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 304
Size: 2414 Color: 245
Size: 112 Color: 7

Bin 75: 8 of cap free
Amount of items: 3
Items: 
Size: 5708 Color: 308
Size: 2348 Color: 243
Size: 72 Color: 4

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 6407 Color: 331
Size: 1721 Color: 225

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 336
Size: 1581 Color: 218

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 350
Size: 1364 Color: 206

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 6980 Color: 365
Size: 1148 Color: 190

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 6073 Color: 316
Size: 2054 Color: 234

Bin 81: 9 of cap free
Amount of items: 2
Items: 
Size: 6673 Color: 345
Size: 1454 Color: 212

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 379
Size: 987 Color: 175

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 6281 Color: 325
Size: 1844 Color: 229

Bin 84: 11 of cap free
Amount of items: 2
Items: 
Size: 6578 Color: 339
Size: 1547 Color: 216

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 6892 Color: 359
Size: 1232 Color: 197

Bin 86: 13 of cap free
Amount of items: 3
Items: 
Size: 4915 Color: 289
Size: 3064 Color: 261
Size: 144 Color: 18

Bin 87: 13 of cap free
Amount of items: 2
Items: 
Size: 7041 Color: 369
Size: 1082 Color: 185

Bin 88: 14 of cap free
Amount of items: 2
Items: 
Size: 6174 Color: 320
Size: 1948 Color: 233

Bin 89: 15 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 341
Size: 1492 Color: 214

Bin 90: 15 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 348
Size: 1395 Color: 208

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 6367 Color: 328
Size: 1753 Color: 227

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 342
Size: 1475 Color: 213

Bin 93: 17 of cap free
Amount of items: 7
Items: 
Size: 4071 Color: 275
Size: 836 Color: 160
Size: 805 Color: 156
Size: 785 Color: 152
Size: 766 Color: 149
Size: 690 Color: 137
Size: 166 Color: 33

Bin 94: 17 of cap free
Amount of items: 2
Items: 
Size: 6669 Color: 344
Size: 1450 Color: 211

Bin 95: 17 of cap free
Amount of items: 2
Items: 
Size: 6817 Color: 352
Size: 1302 Color: 201

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 333
Size: 1618 Color: 219

Bin 97: 19 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 357
Size: 1252 Color: 198

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 6033 Color: 315
Size: 2083 Color: 235

Bin 99: 20 of cap free
Amount of items: 2
Items: 
Size: 6791 Color: 351
Size: 1325 Color: 204

Bin 100: 22 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 306
Size: 2375 Color: 244
Size: 102 Color: 6

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 324
Size: 1870 Color: 230

Bin 102: 23 of cap free
Amount of items: 9
Items: 
Size: 4069 Color: 273
Size: 676 Color: 133
Size: 676 Color: 132
Size: 672 Color: 130
Size: 664 Color: 129
Size: 588 Color: 127
Size: 416 Color: 105
Size: 176 Color: 41
Size: 176 Color: 40

Bin 103: 23 of cap free
Amount of items: 2
Items: 
Size: 6198 Color: 322
Size: 1915 Color: 232

Bin 104: 24 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 299
Size: 2580 Color: 250
Size: 136 Color: 11

Bin 105: 24 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 309
Size: 2236 Color: 238
Size: 72 Color: 3

Bin 106: 29 of cap free
Amount of items: 2
Items: 
Size: 5839 Color: 310
Size: 2268 Color: 240

Bin 107: 30 of cap free
Amount of items: 5
Items: 
Size: 4076 Color: 276
Size: 1171 Color: 192
Size: 1051 Color: 181
Size: 942 Color: 171
Size: 866 Color: 164

Bin 108: 33 of cap free
Amount of items: 3
Items: 
Size: 5177 Color: 295
Size: 2790 Color: 253
Size: 136 Color: 13

Bin 109: 34 of cap free
Amount of items: 3
Items: 
Size: 5242 Color: 297
Size: 2724 Color: 252
Size: 136 Color: 12

Bin 110: 37 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 282
Size: 3388 Color: 267
Size: 154 Color: 25

Bin 111: 37 of cap free
Amount of items: 2
Items: 
Size: 6190 Color: 321
Size: 1909 Color: 231

Bin 112: 42 of cap free
Amount of items: 2
Items: 
Size: 4086 Color: 278
Size: 4008 Color: 272

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 314
Size: 2122 Color: 236
Size: 40 Color: 2

Bin 114: 45 of cap free
Amount of items: 2
Items: 
Size: 5590 Color: 303
Size: 2501 Color: 248

Bin 115: 45 of cap free
Amount of items: 2
Items: 
Size: 5926 Color: 313
Size: 2165 Color: 237

Bin 116: 46 of cap free
Amount of items: 30
Items: 
Size: 382 Color: 102
Size: 372 Color: 100
Size: 368 Color: 99
Size: 358 Color: 97
Size: 340 Color: 94
Size: 336 Color: 92
Size: 328 Color: 90
Size: 324 Color: 89
Size: 324 Color: 88
Size: 314 Color: 86
Size: 294 Color: 83
Size: 288 Color: 82
Size: 288 Color: 80
Size: 280 Color: 79
Size: 270 Color: 76
Size: 264 Color: 75
Size: 260 Color: 74
Size: 220 Color: 61
Size: 216 Color: 59
Size: 216 Color: 58
Size: 216 Color: 57
Size: 214 Color: 56
Size: 214 Color: 55
Size: 210 Color: 54
Size: 208 Color: 53
Size: 200 Color: 51
Size: 200 Color: 50
Size: 198 Color: 49
Size: 196 Color: 48
Size: 192 Color: 47

Bin 117: 48 of cap free
Amount of items: 2
Items: 
Size: 5137 Color: 294
Size: 2951 Color: 259

Bin 118: 54 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 291
Size: 2898 Color: 256
Size: 140 Color: 15

Bin 119: 58 of cap free
Amount of items: 3
Items: 
Size: 5674 Color: 307
Size: 2314 Color: 242
Size: 90 Color: 5

Bin 120: 60 of cap free
Amount of items: 3
Items: 
Size: 5481 Color: 302
Size: 2467 Color: 247
Size: 128 Color: 8

Bin 121: 63 of cap free
Amount of items: 2
Items: 
Size: 4997 Color: 290
Size: 3076 Color: 262

Bin 122: 68 of cap free
Amount of items: 3
Items: 
Size: 4692 Color: 287
Size: 3224 Color: 263
Size: 152 Color: 22

Bin 123: 69 of cap free
Amount of items: 5
Items: 
Size: 4078 Color: 277
Size: 1387 Color: 207
Size: 1359 Color: 205
Size: 1081 Color: 184
Size: 162 Color: 32

Bin 124: 70 of cap free
Amount of items: 4
Items: 
Size: 4368 Color: 280
Size: 3378 Color: 265
Size: 160 Color: 29
Size: 160 Color: 28

Bin 125: 83 of cap free
Amount of items: 2
Items: 
Size: 4662 Color: 286
Size: 3391 Color: 270

Bin 126: 88 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 293
Size: 2918 Color: 258

Bin 127: 91 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 288
Size: 3033 Color: 260
Size: 144 Color: 19

Bin 128: 92 of cap free
Amount of items: 2
Items: 
Size: 4654 Color: 285
Size: 3390 Color: 269

Bin 129: 109 of cap free
Amount of items: 2
Items: 
Size: 4638 Color: 284
Size: 3389 Color: 268

Bin 130: 114 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 301
Size: 2426 Color: 246
Size: 136 Color: 10

Bin 131: 130 of cap free
Amount of items: 19
Items: 
Size: 580 Color: 126
Size: 580 Color: 125
Size: 544 Color: 121
Size: 544 Color: 120
Size: 500 Color: 117
Size: 498 Color: 116
Size: 492 Color: 115
Size: 480 Color: 112
Size: 464 Color: 111
Size: 456 Color: 110
Size: 448 Color: 109
Size: 440 Color: 108
Size: 440 Color: 107
Size: 408 Color: 104
Size: 384 Color: 103
Size: 192 Color: 46
Size: 188 Color: 45
Size: 184 Color: 44
Size: 184 Color: 43

Bin 132: 165 of cap free
Amount of items: 8
Items: 
Size: 4070 Color: 274
Size: 745 Color: 147
Size: 740 Color: 146
Size: 728 Color: 144
Size: 676 Color: 135
Size: 676 Color: 134
Size: 170 Color: 36
Size: 166 Color: 34

Bin 133: 5746 of cap free
Amount of items: 10
Items: 
Size: 256 Color: 72
Size: 250 Color: 71
Size: 248 Color: 70
Size: 248 Color: 69
Size: 240 Color: 67
Size: 236 Color: 66
Size: 232 Color: 65
Size: 232 Color: 64
Size: 224 Color: 63
Size: 224 Color: 62

Total size: 1073952
Total free space: 8136

