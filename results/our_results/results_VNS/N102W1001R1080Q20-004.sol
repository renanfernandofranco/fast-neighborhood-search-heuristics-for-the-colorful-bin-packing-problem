Capicity Bin: 1001
Lower Bound: 48

Bins used: 50
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 19
Size: 473 Color: 0

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 11
Size: 472 Color: 7

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 14
Size: 439 Color: 8

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 2
Size: 244 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 775 Color: 13
Size: 117 Color: 19
Size: 109 Color: 3

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 9
Size: 450 Color: 16

Bin 7: 1 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 332 Color: 7

Bin 8: 1 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 15
Size: 154 Color: 10
Size: 133 Color: 4

Bin 9: 1 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 0
Size: 289 Color: 7

Bin 10: 2 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 8
Size: 429 Color: 17

Bin 11: 2 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 11
Size: 416 Color: 6

Bin 12: 2 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 9
Size: 278 Color: 18

Bin 13: 2 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 7
Size: 106 Color: 18
Size: 101 Color: 1

Bin 14: 3 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 412 Color: 13

Bin 15: 4 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 19
Size: 457 Color: 16

Bin 16: 4 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 3
Size: 436 Color: 17

Bin 17: 4 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 7
Size: 494 Color: 0

Bin 18: 4 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 16
Size: 422 Color: 1

Bin 19: 5 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 7
Size: 488 Color: 0

Bin 20: 6 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 14
Size: 421 Color: 10

Bin 21: 7 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 17
Size: 239 Color: 8

Bin 22: 8 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 2
Size: 250 Color: 3

Bin 23: 8 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 2
Size: 124 Color: 1
Size: 115 Color: 14

Bin 24: 8 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 9
Size: 230 Color: 14

Bin 25: 8 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 7
Size: 209 Color: 17

Bin 26: 11 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 8
Size: 490 Color: 4

Bin 27: 12 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 4
Size: 295 Color: 10

Bin 28: 13 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 13
Size: 318 Color: 9

Bin 29: 13 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 10
Size: 213 Color: 1

Bin 30: 15 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 10
Size: 385 Color: 6

Bin 31: 15 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 0
Size: 334 Color: 9

Bin 32: 18 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 0
Size: 391 Color: 16

Bin 33: 31 of cap free
Amount of items: 2
Items: 
Size: 492 Color: 15
Size: 478 Color: 19

Bin 34: 31 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 16
Size: 313 Color: 2

Bin 35: 37 of cap free
Amount of items: 3
Items: 
Size: 647 Color: 4
Size: 160 Color: 8
Size: 157 Color: 2

Bin 36: 40 of cap free
Amount of items: 2
Items: 
Size: 491 Color: 17
Size: 470 Color: 15

Bin 37: 43 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 310 Color: 13

Bin 38: 44 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 10
Size: 204 Color: 4

Bin 39: 49 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 1
Size: 198 Color: 0

Bin 40: 57 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 10
Size: 203 Color: 19

Bin 41: 59 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 0
Size: 463 Color: 11

Bin 42: 63 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 5
Size: 202 Color: 10

Bin 43: 68 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 202 Color: 12

Bin 44: 69 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 13
Size: 146 Color: 8
Size: 145 Color: 2

Bin 45: 73 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 19
Size: 198 Color: 4

Bin 46: 113 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 14
Size: 176 Color: 15

Bin 47: 372 of cap free
Amount of items: 1
Items: 
Size: 629 Color: 15

Bin 48: 374 of cap free
Amount of items: 1
Items: 
Size: 627 Color: 3

Bin 49: 462 of cap free
Amount of items: 1
Items: 
Size: 539 Color: 7

Bin 50: 485 of cap free
Amount of items: 1
Items: 
Size: 516 Color: 18

Total size: 47402
Total free space: 2648

