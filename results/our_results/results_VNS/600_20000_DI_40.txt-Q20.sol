Capicity Bin: 15600
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 23
Items: 
Size: 876 Color: 10
Size: 864 Color: 19
Size: 864 Color: 19
Size: 832 Color: 3
Size: 800 Color: 3
Size: 740 Color: 17
Size: 728 Color: 0
Size: 724 Color: 18
Size: 712 Color: 15
Size: 710 Color: 7
Size: 704 Color: 8
Size: 704 Color: 1
Size: 672 Color: 16
Size: 668 Color: 11
Size: 656 Color: 16
Size: 640 Color: 18
Size: 640 Color: 8
Size: 616 Color: 3
Size: 560 Color: 7
Size: 534 Color: 6
Size: 528 Color: 10
Size: 496 Color: 12
Size: 332 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7813 Color: 11
Size: 6491 Color: 13
Size: 1296 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8828 Color: 4
Size: 6472 Color: 11
Size: 300 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8876 Color: 1
Size: 6004 Color: 0
Size: 720 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9080 Color: 7
Size: 5644 Color: 0
Size: 876 Color: 5

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 9320 Color: 0
Size: 2531 Color: 5
Size: 2273 Color: 12
Size: 1124 Color: 13
Size: 352 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9594 Color: 8
Size: 5578 Color: 16
Size: 428 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9684 Color: 10
Size: 5656 Color: 2
Size: 260 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10218 Color: 0
Size: 4810 Color: 6
Size: 572 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10088 Color: 5
Size: 5236 Color: 17
Size: 276 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 8
Size: 4892 Color: 16
Size: 380 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10822 Color: 17
Size: 2480 Color: 18
Size: 2298 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 12
Size: 4168 Color: 1
Size: 592 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10860 Color: 19
Size: 4402 Color: 16
Size: 338 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 5
Size: 3266 Color: 7
Size: 652 Color: 11

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11724 Color: 12
Size: 3564 Color: 6
Size: 312 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11754 Color: 13
Size: 3378 Color: 2
Size: 468 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 18
Size: 3236 Color: 4
Size: 404 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 15
Size: 2892 Color: 11
Size: 688 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12120 Color: 4
Size: 2948 Color: 3
Size: 532 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12132 Color: 18
Size: 3156 Color: 7
Size: 312 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12347 Color: 13
Size: 2695 Color: 10
Size: 558 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12391 Color: 5
Size: 1795 Color: 10
Size: 1414 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12454 Color: 5
Size: 2602 Color: 7
Size: 544 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 4
Size: 2260 Color: 12
Size: 848 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 5
Size: 2874 Color: 7
Size: 248 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 19
Size: 2596 Color: 11
Size: 400 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12622 Color: 0
Size: 2482 Color: 12
Size: 496 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 9
Size: 1466 Color: 12
Size: 1390 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12746 Color: 3
Size: 2568 Color: 16
Size: 286 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 16
Size: 2204 Color: 10
Size: 640 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 10
Size: 1608 Color: 2
Size: 1120 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 12
Size: 2348 Color: 18
Size: 268 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 12
Size: 1882 Color: 6
Size: 604 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 3
Size: 1804 Color: 1
Size: 592 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13226 Color: 0
Size: 1982 Color: 6
Size: 392 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 6
Size: 1460 Color: 4
Size: 856 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13293 Color: 14
Size: 1513 Color: 18
Size: 794 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 11
Size: 1635 Color: 12
Size: 538 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 5
Size: 1860 Color: 16
Size: 304 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13486 Color: 2
Size: 1644 Color: 2
Size: 470 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 16
Size: 1668 Color: 17
Size: 360 Color: 8

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13575 Color: 16
Size: 1619 Color: 7
Size: 406 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13654 Color: 0
Size: 1506 Color: 4
Size: 440 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 16
Size: 1604 Color: 15
Size: 392 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13606 Color: 12
Size: 1016 Color: 3
Size: 978 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 0
Size: 1298 Color: 16
Size: 624 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13659 Color: 12
Size: 1419 Color: 18
Size: 522 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13667 Color: 3
Size: 1605 Color: 7
Size: 328 Color: 11

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13737 Color: 0
Size: 1439 Color: 6
Size: 424 Color: 10

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 7
Size: 1128 Color: 18
Size: 784 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13726 Color: 8
Size: 1350 Color: 15
Size: 524 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13785 Color: 10
Size: 1459 Color: 10
Size: 356 Color: 13

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13793 Color: 16
Size: 1507 Color: 0
Size: 300 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 14
Size: 1324 Color: 15
Size: 440 Color: 10

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13842 Color: 9
Size: 1476 Color: 9
Size: 282 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 17
Size: 976 Color: 0
Size: 776 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 11
Size: 1368 Color: 19
Size: 326 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 4
Size: 1412 Color: 2
Size: 280 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13982 Color: 4
Size: 1296 Color: 8
Size: 322 Color: 17

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 11
Size: 1304 Color: 17
Size: 292 Color: 0

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 14
Size: 5723 Color: 0
Size: 680 Color: 19

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10396 Color: 19
Size: 4895 Color: 19
Size: 308 Color: 10

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 10600 Color: 2
Size: 4999 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11226 Color: 8
Size: 3885 Color: 10
Size: 488 Color: 10

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11339 Color: 17
Size: 3956 Color: 15
Size: 304 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 0
Size: 2280 Color: 19
Size: 342 Color: 17

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12997 Color: 14
Size: 1562 Color: 0
Size: 1040 Color: 5

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13068 Color: 11
Size: 2209 Color: 5
Size: 322 Color: 0

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 13154 Color: 13
Size: 2445 Color: 15

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13319 Color: 14
Size: 1298 Color: 8
Size: 982 Color: 0

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 13455 Color: 7
Size: 2144 Color: 15

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 1
Size: 1960 Color: 11

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 18
Size: 1532 Color: 1
Size: 344 Color: 9

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 8726 Color: 8
Size: 6872 Color: 19

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 9830 Color: 9
Size: 5448 Color: 14
Size: 320 Color: 16

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10268 Color: 19
Size: 4874 Color: 5
Size: 456 Color: 19

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10926 Color: 19
Size: 4340 Color: 17
Size: 332 Color: 0

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 11154 Color: 16
Size: 4444 Color: 10

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 5
Size: 3898 Color: 8
Size: 456 Color: 17

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 11892 Color: 16
Size: 3706 Color: 12

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12023 Color: 11
Size: 2687 Color: 15
Size: 888 Color: 0

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 2
Size: 2748 Color: 5
Size: 542 Color: 16

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12407 Color: 15
Size: 2679 Color: 10
Size: 512 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12496 Color: 1
Size: 2962 Color: 16
Size: 140 Color: 7

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 14
Size: 1732 Color: 0
Size: 352 Color: 15

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 13524 Color: 4
Size: 2074 Color: 7

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 13675 Color: 5
Size: 1923 Color: 19

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 13794 Color: 1
Size: 1804 Color: 2

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 13976 Color: 16
Size: 1622 Color: 6

Bin 91: 3 of cap free
Amount of items: 9
Items: 
Size: 7809 Color: 3
Size: 1280 Color: 1
Size: 1144 Color: 13
Size: 1144 Color: 4
Size: 1072 Color: 4
Size: 912 Color: 13
Size: 896 Color: 10
Size: 752 Color: 12
Size: 588 Color: 19

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 8840 Color: 0
Size: 6493 Color: 10
Size: 264 Color: 18

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 10322 Color: 0
Size: 4891 Color: 11
Size: 384 Color: 11

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 11636 Color: 5
Size: 2961 Color: 7
Size: 1000 Color: 2

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 11759 Color: 1
Size: 3574 Color: 11
Size: 264 Color: 5

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 12360 Color: 5
Size: 2661 Color: 13
Size: 576 Color: 0

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 12410 Color: 13
Size: 3187 Color: 17

Bin 98: 3 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 2
Size: 2171 Color: 6

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 8733 Color: 0
Size: 6501 Color: 4
Size: 362 Color: 16

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 9707 Color: 16
Size: 4911 Color: 18
Size: 978 Color: 4

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 10756 Color: 5
Size: 4840 Color: 9

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 10835 Color: 11
Size: 4389 Color: 9
Size: 372 Color: 9

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 17
Size: 2372 Color: 1

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 13934 Color: 3
Size: 1662 Color: 6

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 9490 Color: 1
Size: 4889 Color: 11
Size: 1216 Color: 9

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 9501 Color: 10
Size: 5640 Color: 18
Size: 454 Color: 6

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 12387 Color: 7
Size: 3208 Color: 1

Bin 108: 6 of cap free
Amount of items: 7
Items: 
Size: 7804 Color: 6
Size: 1602 Color: 9
Size: 1528 Color: 11
Size: 1464 Color: 17
Size: 1434 Color: 2
Size: 1298 Color: 8
Size: 464 Color: 3

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 0
Size: 2568 Color: 13

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 5
Size: 2146 Color: 1

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 13875 Color: 8
Size: 1719 Color: 1

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 13899 Color: 17
Size: 1695 Color: 16

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 13
Size: 1670 Color: 11

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 12779 Color: 7
Size: 2814 Color: 19

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 9
Size: 1553 Color: 17

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 8700 Color: 5
Size: 6500 Color: 0
Size: 392 Color: 17

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13550 Color: 10
Size: 2042 Color: 2

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 4
Size: 1710 Color: 19

Bin 119: 10 of cap free
Amount of items: 3
Items: 
Size: 11706 Color: 12
Size: 3604 Color: 16
Size: 280 Color: 0

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13598 Color: 8
Size: 1992 Color: 19

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13768 Color: 8
Size: 1822 Color: 7

Bin 122: 11 of cap free
Amount of items: 3
Items: 
Size: 12873 Color: 15
Size: 2500 Color: 10
Size: 216 Color: 5

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 13405 Color: 12
Size: 2184 Color: 1

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 13821 Color: 13
Size: 1688 Color: 19
Size: 80 Color: 8

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 9727 Color: 1
Size: 5621 Color: 0
Size: 240 Color: 10

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 12037 Color: 1
Size: 3551 Color: 10

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 12926 Color: 4
Size: 2662 Color: 2

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 14018 Color: 1
Size: 1546 Color: 12
Size: 24 Color: 13

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 17
Size: 2799 Color: 3

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 13053 Color: 12
Size: 2534 Color: 13

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 13
Size: 2448 Color: 11

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 4
Size: 2351 Color: 1

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 8722 Color: 16
Size: 6864 Color: 6

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 11332 Color: 15
Size: 4254 Color: 6

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 4
Size: 2009 Color: 6

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 13684 Color: 18
Size: 1901 Color: 12

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 14020 Color: 13
Size: 1565 Color: 10

Bin 138: 16 of cap free
Amount of items: 3
Items: 
Size: 7812 Color: 4
Size: 6492 Color: 0
Size: 1280 Color: 3

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 6
Size: 3048 Color: 3

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 12672 Color: 13
Size: 2912 Color: 18

Bin 141: 19 of cap free
Amount of items: 3
Items: 
Size: 8855 Color: 9
Size: 6502 Color: 2
Size: 224 Color: 14

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 10498 Color: 5
Size: 5083 Color: 7

Bin 143: 19 of cap free
Amount of items: 3
Items: 
Size: 11253 Color: 3
Size: 3944 Color: 18
Size: 384 Color: 0

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 12600 Color: 8
Size: 2981 Color: 11

Bin 145: 20 of cap free
Amount of items: 3
Items: 
Size: 12050 Color: 14
Size: 3402 Color: 12
Size: 128 Color: 1

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 12226 Color: 12
Size: 3354 Color: 13

Bin 147: 21 of cap free
Amount of items: 3
Items: 
Size: 10335 Color: 4
Size: 4932 Color: 2
Size: 312 Color: 0

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 12956 Color: 14
Size: 2622 Color: 1

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 7
Size: 1814 Color: 3

Bin 150: 24 of cap free
Amount of items: 3
Items: 
Size: 11777 Color: 11
Size: 3623 Color: 0
Size: 176 Color: 2

Bin 151: 26 of cap free
Amount of items: 2
Items: 
Size: 13539 Color: 13
Size: 2035 Color: 4

Bin 152: 27 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 14
Size: 2382 Color: 3

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 12371 Color: 17
Size: 3201 Color: 18

Bin 154: 29 of cap free
Amount of items: 2
Items: 
Size: 11284 Color: 11
Size: 4287 Color: 17

Bin 155: 29 of cap free
Amount of items: 2
Items: 
Size: 12667 Color: 18
Size: 2904 Color: 10

Bin 156: 29 of cap free
Amount of items: 2
Items: 
Size: 12951 Color: 6
Size: 2620 Color: 12

Bin 157: 29 of cap free
Amount of items: 2
Items: 
Size: 13567 Color: 5
Size: 2004 Color: 11

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 12154 Color: 19
Size: 3416 Color: 8

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 2
Size: 2123 Color: 19

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 11752 Color: 4
Size: 3816 Color: 12

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 8
Size: 1932 Color: 11

Bin 162: 39 of cap free
Amount of items: 3
Items: 
Size: 11443 Color: 4
Size: 3646 Color: 9
Size: 472 Color: 0

Bin 163: 40 of cap free
Amount of items: 2
Items: 
Size: 13444 Color: 8
Size: 2116 Color: 7

Bin 164: 41 of cap free
Amount of items: 2
Items: 
Size: 9735 Color: 8
Size: 5824 Color: 17

Bin 165: 41 of cap free
Amount of items: 2
Items: 
Size: 13346 Color: 2
Size: 2213 Color: 1

Bin 166: 42 of cap free
Amount of items: 2
Items: 
Size: 12846 Color: 9
Size: 2712 Color: 15

Bin 167: 42 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 12
Size: 2302 Color: 4

Bin 168: 43 of cap free
Amount of items: 2
Items: 
Size: 13746 Color: 3
Size: 1811 Color: 5

Bin 169: 44 of cap free
Amount of items: 2
Items: 
Size: 9800 Color: 5
Size: 5756 Color: 13

Bin 170: 44 of cap free
Amount of items: 2
Items: 
Size: 11512 Color: 6
Size: 4044 Color: 7

Bin 171: 46 of cap free
Amount of items: 2
Items: 
Size: 11578 Color: 1
Size: 3976 Color: 4

Bin 172: 47 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 14
Size: 2711 Color: 16

Bin 173: 48 of cap free
Amount of items: 2
Items: 
Size: 12460 Color: 8
Size: 3092 Color: 1

Bin 174: 49 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 13
Size: 3308 Color: 7

Bin 175: 55 of cap free
Amount of items: 6
Items: 
Size: 7806 Color: 19
Size: 1800 Color: 17
Size: 1789 Color: 9
Size: 1755 Color: 0
Size: 1611 Color: 18
Size: 784 Color: 13

Bin 176: 59 of cap free
Amount of items: 2
Items: 
Size: 12367 Color: 9
Size: 3174 Color: 15

Bin 177: 60 of cap free
Amount of items: 2
Items: 
Size: 13036 Color: 4
Size: 2504 Color: 17

Bin 178: 60 of cap free
Amount of items: 2
Items: 
Size: 13851 Color: 14
Size: 1689 Color: 15

Bin 179: 61 of cap free
Amount of items: 2
Items: 
Size: 10939 Color: 5
Size: 4600 Color: 15

Bin 180: 66 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 18
Size: 2971 Color: 9

Bin 181: 67 of cap free
Amount of items: 2
Items: 
Size: 12068 Color: 6
Size: 3465 Color: 18

Bin 182: 74 of cap free
Amount of items: 2
Items: 
Size: 13159 Color: 16
Size: 2367 Color: 9

Bin 183: 88 of cap free
Amount of items: 2
Items: 
Size: 7816 Color: 9
Size: 7696 Color: 19

Bin 184: 111 of cap free
Amount of items: 3
Items: 
Size: 7848 Color: 5
Size: 6497 Color: 2
Size: 1144 Color: 15

Bin 185: 111 of cap free
Amount of items: 2
Items: 
Size: 11518 Color: 7
Size: 3971 Color: 12

Bin 186: 113 of cap free
Amount of items: 7
Items: 
Size: 7801 Color: 4
Size: 1332 Color: 11
Size: 1322 Color: 11
Size: 1296 Color: 15
Size: 1296 Color: 7
Size: 1296 Color: 1
Size: 1144 Color: 8

Bin 187: 114 of cap free
Amount of items: 3
Items: 
Size: 8296 Color: 17
Size: 6498 Color: 13
Size: 692 Color: 10

Bin 188: 121 of cap free
Amount of items: 2
Items: 
Size: 9603 Color: 17
Size: 5876 Color: 8

Bin 189: 134 of cap free
Amount of items: 2
Items: 
Size: 9732 Color: 6
Size: 5734 Color: 16

Bin 190: 137 of cap free
Amount of items: 2
Items: 
Size: 10457 Color: 17
Size: 5006 Color: 9

Bin 191: 139 of cap free
Amount of items: 2
Items: 
Size: 9731 Color: 4
Size: 5730 Color: 7

Bin 192: 154 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 15
Size: 4174 Color: 7

Bin 193: 154 of cap free
Amount of items: 2
Items: 
Size: 11794 Color: 16
Size: 3652 Color: 6

Bin 194: 157 of cap free
Amount of items: 5
Items: 
Size: 7802 Color: 10
Size: 1972 Color: 7
Size: 1891 Color: 10
Size: 1890 Color: 6
Size: 1888 Color: 19

Bin 195: 160 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 19
Size: 4408 Color: 11

Bin 196: 170 of cap free
Amount of items: 35
Items: 
Size: 636 Color: 11
Size: 632 Color: 4
Size: 596 Color: 9
Size: 584 Color: 19
Size: 576 Color: 9
Size: 532 Color: 1
Size: 528 Color: 11
Size: 528 Color: 2
Size: 520 Color: 16
Size: 520 Color: 7
Size: 508 Color: 7
Size: 506 Color: 4
Size: 496 Color: 18
Size: 496 Color: 1
Size: 480 Color: 15
Size: 472 Color: 8
Size: 448 Color: 6
Size: 432 Color: 11
Size: 432 Color: 6
Size: 424 Color: 9
Size: 416 Color: 14
Size: 412 Color: 0
Size: 384 Color: 15
Size: 384 Color: 14
Size: 384 Color: 1
Size: 358 Color: 3
Size: 340 Color: 9
Size: 336 Color: 10
Size: 324 Color: 12
Size: 320 Color: 10
Size: 310 Color: 6
Size: 304 Color: 5
Size: 288 Color: 12
Size: 268 Color: 5
Size: 256 Color: 5

Bin 197: 188 of cap free
Amount of items: 2
Items: 
Size: 9324 Color: 5
Size: 6088 Color: 14

Bin 198: 221 of cap free
Amount of items: 9
Items: 
Size: 7805 Color: 3
Size: 1008 Color: 8
Size: 998 Color: 2
Size: 984 Color: 15
Size: 976 Color: 0
Size: 960 Color: 5
Size: 960 Color: 5
Size: 912 Color: 19
Size: 776 Color: 7

Bin 199: 11268 of cap free
Amount of items: 15
Items: 
Size: 336 Color: 17
Size: 320 Color: 17
Size: 320 Color: 15
Size: 304 Color: 10
Size: 304 Color: 10
Size: 304 Color: 3
Size: 302 Color: 7
Size: 290 Color: 6
Size: 288 Color: 16
Size: 288 Color: 14
Size: 284 Color: 11
Size: 256 Color: 12
Size: 256 Color: 12
Size: 256 Color: 5
Size: 224 Color: 16

Total size: 3088800
Total free space: 15600

