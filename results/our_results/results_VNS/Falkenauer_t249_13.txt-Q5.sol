Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 2
Size: 356 Color: 0
Size: 251 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 292 Color: 0
Size: 253 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 325 Color: 2
Size: 315 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 302 Color: 2
Size: 266 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 286 Color: 1
Size: 273 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 258 Color: 0
Size: 254 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 255 Color: 2
Size: 252 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 286 Color: 4
Size: 263 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 308 Color: 2
Size: 255 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 348 Color: 3
Size: 272 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 337 Color: 2
Size: 259 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 278 Color: 0
Size: 266 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 321 Color: 0
Size: 265 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 276 Color: 2
Size: 265 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 258 Color: 1
Size: 253 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 0
Size: 318 Color: 4
Size: 276 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 305 Color: 4
Size: 254 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 263 Color: 0
Size: 256 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 293 Color: 4
Size: 291 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 0
Size: 250 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 323 Color: 2
Size: 310 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 266 Color: 4
Size: 262 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 361 Color: 0
Size: 253 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 292 Color: 1
Size: 290 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 266 Color: 0
Size: 252 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 279 Color: 4
Size: 264 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 358 Color: 1
Size: 270 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 363 Color: 0
Size: 252 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 263 Color: 0
Size: 257 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 293 Color: 2
Size: 273 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 357 Color: 4
Size: 261 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 312 Color: 1
Size: 284 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 371 Color: 4
Size: 252 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 363 Color: 0
Size: 258 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 330 Color: 2
Size: 290 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 289 Color: 4
Size: 274 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 302 Color: 4
Size: 262 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 315 Color: 2
Size: 304 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 288 Color: 3
Size: 256 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 3
Size: 335 Color: 3
Size: 266 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 257 Color: 0
Size: 256 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 281 Color: 4
Size: 254 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 2
Size: 281 Color: 3
Size: 252 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 257 Color: 0
Size: 256 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 2
Size: 345 Color: 4
Size: 262 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 310 Color: 1
Size: 282 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 334 Color: 4
Size: 251 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 280 Color: 0
Size: 250 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 310 Color: 2
Size: 293 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 324 Color: 3
Size: 263 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 302 Color: 2
Size: 286 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 331 Color: 4
Size: 267 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 352 Color: 0
Size: 266 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 354 Color: 3
Size: 259 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 3
Size: 252 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 2
Size: 250 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 320 Color: 1
Size: 280 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 271 Color: 0
Size: 262 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 310 Color: 3
Size: 256 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 356 Color: 4
Size: 260 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 2
Size: 350 Color: 3
Size: 290 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 355 Color: 3
Size: 277 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 322 Color: 3
Size: 291 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 318 Color: 1
Size: 272 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 290 Color: 3
Size: 261 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 265 Color: 4
Size: 251 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 260 Color: 4
Size: 250 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 338 Color: 4
Size: 256 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 291 Color: 2
Size: 250 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 328 Color: 4
Size: 279 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 2
Size: 256 Color: 0
Size: 252 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 3
Size: 271 Color: 1
Size: 253 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 353 Color: 2
Size: 252 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 306 Color: 4
Size: 295 Color: 3

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 258 Color: 2
Size: 250 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 263 Color: 2
Size: 251 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 267 Color: 3
Size: 254 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 296 Color: 2
Size: 257 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 340 Color: 3
Size: 250 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 326 Color: 1
Size: 302 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 282 Color: 1
Size: 260 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 329 Color: 0
Size: 294 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 299 Color: 0
Size: 262 Color: 0

Total size: 83000
Total free space: 0

