Capicity Bin: 1000001
Lower Bound: 227

Bins used: 230
Amount of Colors: 2

Bin 1: 4 of cap free
Amount of items: 2
Items: 
Size: 521707 Color: 1
Size: 478290 Color: 0

Bin 2: 14 of cap free
Amount of items: 3
Items: 
Size: 717872 Color: 0
Size: 141840 Color: 1
Size: 140275 Color: 0

Bin 3: 24 of cap free
Amount of items: 3
Items: 
Size: 676508 Color: 0
Size: 180444 Color: 1
Size: 143025 Color: 0

Bin 4: 28 of cap free
Amount of items: 2
Items: 
Size: 757722 Color: 1
Size: 242251 Color: 0

Bin 5: 30 of cap free
Amount of items: 3
Items: 
Size: 738869 Color: 0
Size: 140931 Color: 0
Size: 120171 Color: 1

Bin 6: 51 of cap free
Amount of items: 2
Items: 
Size: 746520 Color: 1
Size: 253430 Color: 0

Bin 7: 58 of cap free
Amount of items: 2
Items: 
Size: 738903 Color: 1
Size: 261040 Color: 0

Bin 8: 81 of cap free
Amount of items: 2
Items: 
Size: 634867 Color: 0
Size: 365053 Color: 1

Bin 9: 89 of cap free
Amount of items: 2
Items: 
Size: 673866 Color: 0
Size: 326046 Color: 1

Bin 10: 99 of cap free
Amount of items: 3
Items: 
Size: 751558 Color: 0
Size: 124192 Color: 0
Size: 124152 Color: 1

Bin 11: 104 of cap free
Amount of items: 2
Items: 
Size: 705608 Color: 1
Size: 294289 Color: 0

Bin 12: 114 of cap free
Amount of items: 2
Items: 
Size: 601825 Color: 0
Size: 398062 Color: 1

Bin 13: 119 of cap free
Amount of items: 2
Items: 
Size: 763392 Color: 0
Size: 236490 Color: 1

Bin 14: 121 of cap free
Amount of items: 2
Items: 
Size: 661378 Color: 0
Size: 338502 Color: 1

Bin 15: 137 of cap free
Amount of items: 2
Items: 
Size: 524851 Color: 1
Size: 475013 Color: 0

Bin 16: 138 of cap free
Amount of items: 2
Items: 
Size: 509821 Color: 0
Size: 490042 Color: 1

Bin 17: 138 of cap free
Amount of items: 2
Items: 
Size: 603395 Color: 1
Size: 396468 Color: 0

Bin 18: 142 of cap free
Amount of items: 3
Items: 
Size: 599460 Color: 0
Size: 263681 Color: 1
Size: 136718 Color: 0

Bin 19: 154 of cap free
Amount of items: 2
Items: 
Size: 696904 Color: 1
Size: 302943 Color: 0

Bin 20: 169 of cap free
Amount of items: 2
Items: 
Size: 704909 Color: 1
Size: 294923 Color: 0

Bin 21: 181 of cap free
Amount of items: 2
Items: 
Size: 588734 Color: 0
Size: 411086 Color: 1

Bin 22: 194 of cap free
Amount of items: 3
Items: 
Size: 677016 Color: 1
Size: 181499 Color: 1
Size: 141292 Color: 0

Bin 23: 216 of cap free
Amount of items: 3
Items: 
Size: 713897 Color: 0
Size: 146681 Color: 0
Size: 139207 Color: 1

Bin 24: 230 of cap free
Amount of items: 2
Items: 
Size: 794457 Color: 0
Size: 205314 Color: 1

Bin 25: 233 of cap free
Amount of items: 3
Items: 
Size: 412891 Color: 0
Size: 293440 Color: 0
Size: 293437 Color: 1

Bin 26: 250 of cap free
Amount of items: 3
Items: 
Size: 653938 Color: 1
Size: 214697 Color: 0
Size: 131116 Color: 0

Bin 27: 254 of cap free
Amount of items: 2
Items: 
Size: 500313 Color: 0
Size: 499434 Color: 1

Bin 28: 258 of cap free
Amount of items: 2
Items: 
Size: 747600 Color: 0
Size: 252143 Color: 1

Bin 29: 259 of cap free
Amount of items: 2
Items: 
Size: 579889 Color: 1
Size: 419853 Color: 0

Bin 30: 279 of cap free
Amount of items: 3
Items: 
Size: 738555 Color: 1
Size: 132660 Color: 0
Size: 128507 Color: 1

Bin 31: 286 of cap free
Amount of items: 2
Items: 
Size: 674944 Color: 0
Size: 324771 Color: 1

Bin 32: 288 of cap free
Amount of items: 2
Items: 
Size: 727689 Color: 0
Size: 272024 Color: 1

Bin 33: 291 of cap free
Amount of items: 2
Items: 
Size: 644827 Color: 0
Size: 354883 Color: 1

Bin 34: 316 of cap free
Amount of items: 3
Items: 
Size: 648721 Color: 0
Size: 185282 Color: 1
Size: 165682 Color: 1

Bin 35: 354 of cap free
Amount of items: 2
Items: 
Size: 567212 Color: 0
Size: 432435 Color: 1

Bin 36: 394 of cap free
Amount of items: 2
Items: 
Size: 612465 Color: 0
Size: 387142 Color: 1

Bin 37: 400 of cap free
Amount of items: 3
Items: 
Size: 675946 Color: 0
Size: 164530 Color: 1
Size: 159125 Color: 0

Bin 38: 404 of cap free
Amount of items: 2
Items: 
Size: 729917 Color: 0
Size: 269680 Color: 1

Bin 39: 406 of cap free
Amount of items: 2
Items: 
Size: 787552 Color: 1
Size: 212043 Color: 0

Bin 40: 412 of cap free
Amount of items: 2
Items: 
Size: 569135 Color: 0
Size: 430454 Color: 1

Bin 41: 412 of cap free
Amount of items: 3
Items: 
Size: 730441 Color: 0
Size: 137193 Color: 1
Size: 131955 Color: 0

Bin 42: 413 of cap free
Amount of items: 2
Items: 
Size: 568694 Color: 1
Size: 430894 Color: 0

Bin 43: 415 of cap free
Amount of items: 2
Items: 
Size: 618194 Color: 0
Size: 381392 Color: 1

Bin 44: 431 of cap free
Amount of items: 2
Items: 
Size: 793763 Color: 1
Size: 205807 Color: 0

Bin 45: 472 of cap free
Amount of items: 2
Items: 
Size: 552275 Color: 0
Size: 447254 Color: 1

Bin 46: 484 of cap free
Amount of items: 2
Items: 
Size: 723868 Color: 0
Size: 275649 Color: 1

Bin 47: 535 of cap free
Amount of items: 2
Items: 
Size: 646632 Color: 1
Size: 352834 Color: 0

Bin 48: 552 of cap free
Amount of items: 2
Items: 
Size: 517259 Color: 1
Size: 482190 Color: 0

Bin 49: 563 of cap free
Amount of items: 3
Items: 
Size: 768788 Color: 1
Size: 117709 Color: 0
Size: 112941 Color: 1

Bin 50: 566 of cap free
Amount of items: 2
Items: 
Size: 503865 Color: 0
Size: 495570 Color: 1

Bin 51: 570 of cap free
Amount of items: 2
Items: 
Size: 600822 Color: 1
Size: 398609 Color: 0

Bin 52: 624 of cap free
Amount of items: 3
Items: 
Size: 716645 Color: 1
Size: 145946 Color: 0
Size: 136786 Color: 1

Bin 53: 627 of cap free
Amount of items: 2
Items: 
Size: 557327 Color: 0
Size: 442047 Color: 1

Bin 54: 642 of cap free
Amount of items: 2
Items: 
Size: 741626 Color: 0
Size: 257733 Color: 1

Bin 55: 644 of cap free
Amount of items: 2
Items: 
Size: 539466 Color: 0
Size: 459891 Color: 1

Bin 56: 649 of cap free
Amount of items: 2
Items: 
Size: 518646 Color: 0
Size: 480706 Color: 1

Bin 57: 668 of cap free
Amount of items: 2
Items: 
Size: 522706 Color: 0
Size: 476627 Color: 1

Bin 58: 699 of cap free
Amount of items: 3
Items: 
Size: 581742 Color: 0
Size: 263499 Color: 0
Size: 154061 Color: 1

Bin 59: 723 of cap free
Amount of items: 2
Items: 
Size: 760282 Color: 0
Size: 238996 Color: 1

Bin 60: 777 of cap free
Amount of items: 2
Items: 
Size: 674618 Color: 1
Size: 324606 Color: 0

Bin 61: 795 of cap free
Amount of items: 2
Items: 
Size: 713037 Color: 0
Size: 286169 Color: 1

Bin 62: 833 of cap free
Amount of items: 3
Items: 
Size: 592126 Color: 0
Size: 265610 Color: 0
Size: 141432 Color: 1

Bin 63: 866 of cap free
Amount of items: 2
Items: 
Size: 788818 Color: 1
Size: 210317 Color: 0

Bin 64: 929 of cap free
Amount of items: 2
Items: 
Size: 528180 Color: 1
Size: 470892 Color: 0

Bin 65: 960 of cap free
Amount of items: 2
Items: 
Size: 705346 Color: 1
Size: 293695 Color: 0

Bin 66: 964 of cap free
Amount of items: 2
Items: 
Size: 589387 Color: 0
Size: 409650 Color: 1

Bin 67: 992 of cap free
Amount of items: 3
Items: 
Size: 741151 Color: 1
Size: 134550 Color: 0
Size: 123308 Color: 0

Bin 68: 1023 of cap free
Amount of items: 2
Items: 
Size: 658323 Color: 1
Size: 340655 Color: 0

Bin 69: 1024 of cap free
Amount of items: 2
Items: 
Size: 562159 Color: 1
Size: 436818 Color: 0

Bin 70: 1057 of cap free
Amount of items: 2
Items: 
Size: 750566 Color: 1
Size: 248378 Color: 0

Bin 71: 1058 of cap free
Amount of items: 2
Items: 
Size: 554048 Color: 0
Size: 444895 Color: 1

Bin 72: 1070 of cap free
Amount of items: 2
Items: 
Size: 773654 Color: 0
Size: 225277 Color: 1

Bin 73: 1137 of cap free
Amount of items: 2
Items: 
Size: 665004 Color: 1
Size: 333860 Color: 0

Bin 74: 1150 of cap free
Amount of items: 2
Items: 
Size: 661871 Color: 0
Size: 336980 Color: 1

Bin 75: 1153 of cap free
Amount of items: 2
Items: 
Size: 614536 Color: 1
Size: 384312 Color: 0

Bin 76: 1159 of cap free
Amount of items: 2
Items: 
Size: 692531 Color: 1
Size: 306311 Color: 0

Bin 77: 1206 of cap free
Amount of items: 2
Items: 
Size: 711982 Color: 1
Size: 286813 Color: 0

Bin 78: 1222 of cap free
Amount of items: 2
Items: 
Size: 770479 Color: 1
Size: 228300 Color: 0

Bin 79: 1256 of cap free
Amount of items: 2
Items: 
Size: 502797 Color: 1
Size: 495948 Color: 0

Bin 80: 1261 of cap free
Amount of items: 3
Items: 
Size: 573877 Color: 0
Size: 268439 Color: 0
Size: 156424 Color: 1

Bin 81: 1272 of cap free
Amount of items: 2
Items: 
Size: 572950 Color: 1
Size: 425779 Color: 0

Bin 82: 1281 of cap free
Amount of items: 2
Items: 
Size: 749742 Color: 0
Size: 248978 Color: 1

Bin 83: 1315 of cap free
Amount of items: 2
Items: 
Size: 655440 Color: 0
Size: 343246 Color: 1

Bin 84: 1330 of cap free
Amount of items: 2
Items: 
Size: 512750 Color: 1
Size: 485921 Color: 0

Bin 85: 1349 of cap free
Amount of items: 2
Items: 
Size: 727920 Color: 1
Size: 270732 Color: 0

Bin 86: 1353 of cap free
Amount of items: 2
Items: 
Size: 794604 Color: 1
Size: 204044 Color: 0

Bin 87: 1383 of cap free
Amount of items: 2
Items: 
Size: 744010 Color: 1
Size: 254608 Color: 0

Bin 88: 1419 of cap free
Amount of items: 2
Items: 
Size: 534493 Color: 0
Size: 464089 Color: 1

Bin 89: 1455 of cap free
Amount of items: 2
Items: 
Size: 668734 Color: 1
Size: 329812 Color: 0

Bin 90: 1495 of cap free
Amount of items: 2
Items: 
Size: 701716 Color: 0
Size: 296790 Color: 1

Bin 91: 1513 of cap free
Amount of items: 2
Items: 
Size: 757462 Color: 0
Size: 241026 Color: 1

Bin 92: 1528 of cap free
Amount of items: 3
Items: 
Size: 782177 Color: 0
Size: 109514 Color: 0
Size: 106782 Color: 1

Bin 93: 1581 of cap free
Amount of items: 2
Items: 
Size: 510559 Color: 0
Size: 487861 Color: 1

Bin 94: 1668 of cap free
Amount of items: 3
Items: 
Size: 770571 Color: 0
Size: 115972 Color: 1
Size: 111790 Color: 0

Bin 95: 1672 of cap free
Amount of items: 2
Items: 
Size: 544765 Color: 0
Size: 453564 Color: 1

Bin 96: 1690 of cap free
Amount of items: 2
Items: 
Size: 711182 Color: 0
Size: 287129 Color: 1

Bin 97: 1729 of cap free
Amount of items: 2
Items: 
Size: 762818 Color: 0
Size: 235454 Color: 1

Bin 98: 1730 of cap free
Amount of items: 3
Items: 
Size: 756396 Color: 0
Size: 126690 Color: 1
Size: 115185 Color: 1

Bin 99: 1739 of cap free
Amount of items: 2
Items: 
Size: 534980 Color: 1
Size: 463282 Color: 0

Bin 100: 1771 of cap free
Amount of items: 2
Items: 
Size: 700164 Color: 1
Size: 298066 Color: 0

Bin 101: 1801 of cap free
Amount of items: 2
Items: 
Size: 522257 Color: 0
Size: 475943 Color: 1

Bin 102: 1802 of cap free
Amount of items: 2
Items: 
Size: 569445 Color: 1
Size: 428754 Color: 0

Bin 103: 1812 of cap free
Amount of items: 2
Items: 
Size: 639933 Color: 0
Size: 358256 Color: 1

Bin 104: 1843 of cap free
Amount of items: 2
Items: 
Size: 687555 Color: 0
Size: 310603 Color: 1

Bin 105: 1856 of cap free
Amount of items: 2
Items: 
Size: 657636 Color: 1
Size: 340509 Color: 0

Bin 106: 1876 of cap free
Amount of items: 2
Items: 
Size: 732407 Color: 0
Size: 265718 Color: 1

Bin 107: 1892 of cap free
Amount of items: 2
Items: 
Size: 547085 Color: 0
Size: 451024 Color: 1

Bin 108: 1921 of cap free
Amount of items: 2
Items: 
Size: 781576 Color: 0
Size: 216504 Color: 1

Bin 109: 2081 of cap free
Amount of items: 2
Items: 
Size: 546080 Color: 1
Size: 451840 Color: 0

Bin 110: 2110 of cap free
Amount of items: 2
Items: 
Size: 660365 Color: 1
Size: 337526 Color: 0

Bin 111: 2139 of cap free
Amount of items: 2
Items: 
Size: 596539 Color: 1
Size: 401323 Color: 0

Bin 112: 2248 of cap free
Amount of items: 2
Items: 
Size: 628557 Color: 0
Size: 369196 Color: 1

Bin 113: 2272 of cap free
Amount of items: 2
Items: 
Size: 513688 Color: 0
Size: 484041 Color: 1

Bin 114: 2327 of cap free
Amount of items: 2
Items: 
Size: 728648 Color: 0
Size: 269026 Color: 1

Bin 115: 2373 of cap free
Amount of items: 2
Items: 
Size: 608524 Color: 0
Size: 389104 Color: 1

Bin 116: 2414 of cap free
Amount of items: 3
Items: 
Size: 760079 Color: 1
Size: 119018 Color: 0
Size: 118490 Color: 0

Bin 117: 2472 of cap free
Amount of items: 2
Items: 
Size: 612317 Color: 0
Size: 385212 Color: 1

Bin 118: 2475 of cap free
Amount of items: 3
Items: 
Size: 635509 Color: 0
Size: 192263 Color: 1
Size: 169754 Color: 1

Bin 119: 2584 of cap free
Amount of items: 2
Items: 
Size: 782509 Color: 1
Size: 214908 Color: 0

Bin 120: 2666 of cap free
Amount of items: 2
Items: 
Size: 623640 Color: 1
Size: 373695 Color: 0

Bin 121: 2675 of cap free
Amount of items: 3
Items: 
Size: 647621 Color: 0
Size: 186350 Color: 1
Size: 163355 Color: 0

Bin 122: 2824 of cap free
Amount of items: 2
Items: 
Size: 680059 Color: 1
Size: 317118 Color: 0

Bin 123: 2961 of cap free
Amount of items: 2
Items: 
Size: 505669 Color: 1
Size: 491371 Color: 0

Bin 124: 3015 of cap free
Amount of items: 2
Items: 
Size: 587913 Color: 0
Size: 409073 Color: 1

Bin 125: 3121 of cap free
Amount of items: 2
Items: 
Size: 798788 Color: 1
Size: 198092 Color: 0

Bin 126: 3141 of cap free
Amount of items: 3
Items: 
Size: 580666 Color: 1
Size: 265001 Color: 0
Size: 151193 Color: 1

Bin 127: 3310 of cap free
Amount of items: 3
Items: 
Size: 738235 Color: 1
Size: 133003 Color: 0
Size: 125453 Color: 1

Bin 128: 3345 of cap free
Amount of items: 2
Items: 
Size: 616655 Color: 0
Size: 380001 Color: 1

Bin 129: 3364 of cap free
Amount of items: 2
Items: 
Size: 651371 Color: 0
Size: 345266 Color: 1

Bin 130: 3366 of cap free
Amount of items: 2
Items: 
Size: 760134 Color: 1
Size: 236501 Color: 0

Bin 131: 3375 of cap free
Amount of items: 3
Items: 
Size: 541566 Color: 0
Size: 295956 Color: 0
Size: 159104 Color: 1

Bin 132: 3426 of cap free
Amount of items: 2
Items: 
Size: 611513 Color: 0
Size: 385062 Color: 1

Bin 133: 3484 of cap free
Amount of items: 2
Items: 
Size: 503025 Color: 0
Size: 493492 Color: 1

Bin 134: 3489 of cap free
Amount of items: 2
Items: 
Size: 716186 Color: 1
Size: 280326 Color: 0

Bin 135: 3527 of cap free
Amount of items: 2
Items: 
Size: 754861 Color: 1
Size: 241613 Color: 0

Bin 136: 3690 of cap free
Amount of items: 2
Items: 
Size: 589939 Color: 1
Size: 406372 Color: 0

Bin 137: 3801 of cap free
Amount of items: 2
Items: 
Size: 539053 Color: 1
Size: 457147 Color: 0

Bin 138: 4112 of cap free
Amount of items: 2
Items: 
Size: 596964 Color: 0
Size: 398925 Color: 1

Bin 139: 4120 of cap free
Amount of items: 2
Items: 
Size: 679721 Color: 0
Size: 316160 Color: 1

Bin 140: 4130 of cap free
Amount of items: 2
Items: 
Size: 665339 Color: 0
Size: 330532 Color: 1

Bin 141: 4178 of cap free
Amount of items: 2
Items: 
Size: 591714 Color: 0
Size: 404109 Color: 1

Bin 142: 4201 of cap free
Amount of items: 2
Items: 
Size: 607718 Color: 1
Size: 388082 Color: 0

Bin 143: 4272 of cap free
Amount of items: 2
Items: 
Size: 589814 Color: 1
Size: 405915 Color: 0

Bin 144: 4386 of cap free
Amount of items: 2
Items: 
Size: 580548 Color: 1
Size: 415067 Color: 0

Bin 145: 4628 of cap free
Amount of items: 2
Items: 
Size: 532574 Color: 0
Size: 462799 Color: 1

Bin 146: 4648 of cap free
Amount of items: 2
Items: 
Size: 524700 Color: 0
Size: 470653 Color: 1

Bin 147: 4665 of cap free
Amount of items: 2
Items: 
Size: 510676 Color: 1
Size: 484660 Color: 0

Bin 148: 4784 of cap free
Amount of items: 2
Items: 
Size: 743679 Color: 0
Size: 251538 Color: 1

Bin 149: 4848 of cap free
Amount of items: 3
Items: 
Size: 624325 Color: 0
Size: 192190 Color: 1
Size: 178638 Color: 0

Bin 150: 4870 of cap free
Amount of items: 2
Items: 
Size: 792098 Color: 1
Size: 203033 Color: 0

Bin 151: 4999 of cap free
Amount of items: 2
Items: 
Size: 550441 Color: 0
Size: 444561 Color: 1

Bin 152: 5060 of cap free
Amount of items: 2
Items: 
Size: 628490 Color: 1
Size: 366451 Color: 0

Bin 153: 5120 of cap free
Amount of items: 2
Items: 
Size: 647197 Color: 1
Size: 347684 Color: 0

Bin 154: 5172 of cap free
Amount of items: 2
Items: 
Size: 517835 Color: 1
Size: 476994 Color: 0

Bin 155: 5229 of cap free
Amount of items: 2
Items: 
Size: 683106 Color: 1
Size: 311666 Color: 0

Bin 156: 5453 of cap free
Amount of items: 2
Items: 
Size: 742466 Color: 1
Size: 252082 Color: 0

Bin 157: 5691 of cap free
Amount of items: 2
Items: 
Size: 754063 Color: 1
Size: 240247 Color: 0

Bin 158: 5871 of cap free
Amount of items: 2
Items: 
Size: 776409 Color: 1
Size: 217721 Color: 0

Bin 159: 5963 of cap free
Amount of items: 2
Items: 
Size: 692275 Color: 1
Size: 301763 Color: 0

Bin 160: 6059 of cap free
Amount of items: 2
Items: 
Size: 753875 Color: 1
Size: 240067 Color: 0

Bin 161: 6585 of cap free
Amount of items: 2
Items: 
Size: 542437 Color: 0
Size: 450979 Color: 1

Bin 162: 6692 of cap free
Amount of items: 2
Items: 
Size: 627205 Color: 1
Size: 366104 Color: 0

Bin 163: 7486 of cap free
Amount of items: 2
Items: 
Size: 663900 Color: 0
Size: 328615 Color: 1

Bin 164: 7716 of cap free
Amount of items: 2
Items: 
Size: 668306 Color: 1
Size: 323979 Color: 0

Bin 165: 7964 of cap free
Amount of items: 2
Items: 
Size: 566961 Color: 1
Size: 425076 Color: 0

Bin 166: 8310 of cap free
Amount of items: 3
Items: 
Size: 568925 Color: 1
Size: 279642 Color: 0
Size: 143124 Color: 0

Bin 167: 8436 of cap free
Amount of items: 2
Items: 
Size: 605324 Color: 1
Size: 386241 Color: 0

Bin 168: 8999 of cap free
Amount of items: 2
Items: 
Size: 522026 Color: 0
Size: 468976 Color: 1

Bin 169: 9411 of cap free
Amount of items: 2
Items: 
Size: 605845 Color: 0
Size: 384745 Color: 1

Bin 170: 9444 of cap free
Amount of items: 3
Items: 
Size: 639627 Color: 1
Size: 184649 Color: 0
Size: 166281 Color: 0

Bin 171: 9671 of cap free
Amount of items: 3
Items: 
Size: 581445 Color: 0
Size: 262995 Color: 1
Size: 145890 Color: 0

Bin 172: 10043 of cap free
Amount of items: 2
Items: 
Size: 626036 Color: 0
Size: 363922 Color: 1

Bin 173: 10312 of cap free
Amount of items: 2
Items: 
Size: 605106 Color: 0
Size: 384583 Color: 1

Bin 174: 10602 of cap free
Amount of items: 2
Items: 
Size: 759614 Color: 0
Size: 229785 Color: 1

Bin 175: 10638 of cap free
Amount of items: 2
Items: 
Size: 532421 Color: 1
Size: 456942 Color: 0

Bin 176: 11320 of cap free
Amount of items: 2
Items: 
Size: 743295 Color: 0
Size: 245386 Color: 1

Bin 177: 12065 of cap free
Amount of items: 2
Items: 
Size: 640609 Color: 1
Size: 347327 Color: 0

Bin 178: 12114 of cap free
Amount of items: 2
Items: 
Size: 564555 Color: 1
Size: 423332 Color: 0

Bin 179: 12158 of cap free
Amount of items: 2
Items: 
Size: 753214 Color: 1
Size: 234629 Color: 0

Bin 180: 12924 of cap free
Amount of items: 2
Items: 
Size: 790398 Color: 1
Size: 196679 Color: 0

Bin 181: 12969 of cap free
Amount of items: 2
Items: 
Size: 787524 Color: 0
Size: 199508 Color: 1

Bin 182: 13066 of cap free
Amount of items: 2
Items: 
Size: 621454 Color: 1
Size: 365481 Color: 0

Bin 183: 13212 of cap free
Amount of items: 2
Items: 
Size: 588810 Color: 1
Size: 397979 Color: 0

Bin 184: 13409 of cap free
Amount of items: 2
Items: 
Size: 542174 Color: 0
Size: 444418 Color: 1

Bin 185: 13420 of cap free
Amount of items: 2
Items: 
Size: 721752 Color: 0
Size: 264829 Color: 1

Bin 186: 13859 of cap free
Amount of items: 2
Items: 
Size: 563195 Color: 1
Size: 422947 Color: 0

Bin 187: 14377 of cap free
Amount of items: 2
Items: 
Size: 653596 Color: 1
Size: 332028 Color: 0

Bin 188: 17389 of cap free
Amount of items: 2
Items: 
Size: 561792 Color: 1
Size: 420820 Color: 0

Bin 189: 18210 of cap free
Amount of items: 2
Items: 
Size: 790310 Color: 1
Size: 191481 Color: 0

Bin 190: 18370 of cap free
Amount of items: 2
Items: 
Size: 783843 Color: 0
Size: 197788 Color: 1

Bin 191: 19270 of cap free
Amount of items: 2
Items: 
Size: 560537 Color: 1
Size: 420194 Color: 0

Bin 192: 19623 of cap free
Amount of items: 2
Items: 
Size: 616608 Color: 1
Size: 363770 Color: 0

Bin 193: 20149 of cap free
Amount of items: 2
Items: 
Size: 581385 Color: 0
Size: 398467 Color: 1

Bin 194: 20313 of cap free
Amount of items: 2
Items: 
Size: 622987 Color: 0
Size: 356701 Color: 1

Bin 195: 20544 of cap free
Amount of items: 2
Items: 
Size: 531907 Color: 1
Size: 447550 Color: 0

Bin 196: 20954 of cap free
Amount of items: 2
Items: 
Size: 581009 Color: 0
Size: 398038 Color: 1

Bin 197: 21226 of cap free
Amount of items: 2
Items: 
Size: 537213 Color: 0
Size: 441562 Color: 1

Bin 198: 22602 of cap free
Amount of items: 2
Items: 
Size: 580881 Color: 0
Size: 396518 Color: 1

Bin 199: 22737 of cap free
Amount of items: 3
Items: 
Size: 613921 Color: 1
Size: 184220 Color: 0
Size: 179123 Color: 1

Bin 200: 23300 of cap free
Amount of items: 2
Items: 
Size: 668250 Color: 1
Size: 308451 Color: 0

Bin 201: 24738 of cap free
Amount of items: 2
Items: 
Size: 788263 Color: 1
Size: 187000 Color: 0

Bin 202: 25566 of cap free
Amount of items: 2
Items: 
Size: 709942 Color: 0
Size: 264493 Color: 1

Bin 203: 25827 of cap free
Amount of items: 2
Items: 
Size: 560082 Color: 1
Size: 414092 Color: 0

Bin 204: 27131 of cap free
Amount of items: 2
Items: 
Size: 531750 Color: 0
Size: 441120 Color: 1

Bin 205: 27544 of cap free
Amount of items: 2
Items: 
Size: 558995 Color: 1
Size: 413462 Color: 0

Bin 206: 28417 of cap free
Amount of items: 2
Items: 
Size: 737211 Color: 1
Size: 234373 Color: 0

Bin 207: 29478 of cap free
Amount of items: 2
Items: 
Size: 785758 Color: 1
Size: 184765 Color: 0

Bin 208: 31152 of cap free
Amount of items: 2
Items: 
Size: 523818 Color: 1
Size: 445031 Color: 0

Bin 209: 43695 of cap free
Amount of items: 2
Items: 
Size: 518099 Color: 0
Size: 438207 Color: 1

Bin 210: 46942 of cap free
Amount of items: 2
Items: 
Size: 515994 Color: 0
Size: 437065 Color: 1

Bin 211: 53733 of cap free
Amount of items: 2
Items: 
Size: 509362 Color: 0
Size: 436906 Color: 1

Bin 212: 63673 of cap free
Amount of items: 2
Items: 
Size: 523201 Color: 1
Size: 413127 Color: 0

Bin 213: 66000 of cap free
Amount of items: 2
Items: 
Size: 500896 Color: 0
Size: 433105 Color: 1

Bin 214: 68144 of cap free
Amount of items: 2
Items: 
Size: 738641 Color: 0
Size: 193216 Color: 1

Bin 215: 68962 of cap free
Amount of items: 2
Items: 
Size: 500814 Color: 0
Size: 430225 Color: 1

Bin 216: 70226 of cap free
Amount of items: 3
Items: 
Size: 603757 Color: 1
Size: 163244 Color: 0
Size: 162774 Color: 0

Bin 217: 74071 of cap free
Amount of items: 3
Items: 
Size: 602619 Color: 1
Size: 175454 Color: 1
Size: 147857 Color: 0

Bin 218: 85663 of cap free
Amount of items: 3
Items: 
Size: 577973 Color: 0
Size: 174658 Color: 1
Size: 161707 Color: 1

Bin 219: 91023 of cap free
Amount of items: 3
Items: 
Size: 304274 Color: 1
Size: 303941 Color: 1
Size: 300763 Color: 0

Bin 220: 93105 of cap free
Amount of items: 3
Items: 
Size: 304633 Color: 1
Size: 301353 Color: 0
Size: 300910 Color: 0

Bin 221: 98254 of cap free
Amount of items: 2
Items: 
Size: 577199 Color: 0
Size: 324548 Color: 1

Bin 222: 103237 of cap free
Amount of items: 2
Items: 
Size: 573664 Color: 0
Size: 323100 Color: 1

Bin 223: 108835 of cap free
Amount of items: 3
Items: 
Size: 300552 Color: 1
Size: 299939 Color: 1
Size: 290675 Color: 0

Bin 224: 111059 of cap free
Amount of items: 2
Items: 
Size: 572893 Color: 0
Size: 316049 Color: 1

Bin 225: 112150 of cap free
Amount of items: 3
Items: 
Size: 302237 Color: 1
Size: 294888 Color: 0
Size: 290726 Color: 0

Bin 226: 112974 of cap free
Amount of items: 2
Items: 
Size: 572667 Color: 0
Size: 314360 Color: 1

Bin 227: 120425 of cap free
Amount of items: 2
Items: 
Size: 569793 Color: 0
Size: 309783 Color: 1

Bin 228: 132671 of cap free
Amount of items: 3
Items: 
Size: 299423 Color: 1
Size: 288879 Color: 0
Size: 279028 Color: 0

Bin 229: 191894 of cap free
Amount of items: 2
Items: 
Size: 500757 Color: 0
Size: 307350 Color: 1

Bin 230: 427747 of cap free
Amount of items: 2
Items: 
Size: 298527 Color: 1
Size: 273727 Color: 0

Total size: 226602095
Total free space: 3398135

