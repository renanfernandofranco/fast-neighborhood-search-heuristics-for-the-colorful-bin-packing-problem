Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 30
Items: 
Size: 518 Color: 2
Size: 496 Color: 3
Size: 408 Color: 3
Size: 364 Color: 2
Size: 336 Color: 3
Size: 304 Color: 4
Size: 294 Color: 0
Size: 292 Color: 4
Size: 288 Color: 2
Size: 280 Color: 3
Size: 280 Color: 3
Size: 280 Color: 0
Size: 272 Color: 3
Size: 272 Color: 0
Size: 270 Color: 1
Size: 268 Color: 3
Size: 248 Color: 0
Size: 248 Color: 0
Size: 240 Color: 4
Size: 240 Color: 2
Size: 232 Color: 1
Size: 228 Color: 3
Size: 220 Color: 0
Size: 212 Color: 1
Size: 200 Color: 2
Size: 200 Color: 1
Size: 182 Color: 1
Size: 164 Color: 1
Size: 160 Color: 4
Size: 140 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 3
Size: 3052 Color: 0
Size: 168 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5028 Color: 2
Size: 2596 Color: 0
Size: 512 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 1
Size: 2286 Color: 0
Size: 778 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 4
Size: 2684 Color: 0
Size: 208 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 3
Size: 2287 Color: 0
Size: 456 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 0
Size: 2262 Color: 4
Size: 452 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5446 Color: 0
Size: 2242 Color: 4
Size: 448 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 2
Size: 2292 Color: 3
Size: 176 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 3
Size: 1930 Color: 0
Size: 384 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 0
Size: 1932 Color: 4
Size: 384 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 0
Size: 1660 Color: 2
Size: 328 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 3
Size: 1474 Color: 0
Size: 360 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 2
Size: 1520 Color: 0
Size: 204 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 0
Size: 1300 Color: 2
Size: 320 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 3
Size: 1188 Color: 2
Size: 448 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 2
Size: 1331 Color: 0
Size: 288 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 4
Size: 1218 Color: 0
Size: 340 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 4
Size: 1364 Color: 4
Size: 128 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 0
Size: 1212 Color: 2
Size: 208 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6722 Color: 4
Size: 774 Color: 0
Size: 640 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 862 Color: 3
Size: 452 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 4
Size: 760 Color: 2
Size: 516 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 0
Size: 1001 Color: 3
Size: 200 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 4
Size: 612 Color: 1
Size: 608 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7003 Color: 0
Size: 911 Color: 2
Size: 222 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 0
Size: 932 Color: 1
Size: 184 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 1
Size: 1002 Color: 0
Size: 120 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7035 Color: 2
Size: 809 Color: 1
Size: 292 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7052 Color: 4
Size: 844 Color: 3
Size: 240 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 0
Size: 588 Color: 4
Size: 456 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7101 Color: 0
Size: 779 Color: 4
Size: 256 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7106 Color: 3
Size: 702 Color: 4
Size: 328 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7132 Color: 4
Size: 788 Color: 0
Size: 216 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 3
Size: 590 Color: 2
Size: 404 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7195 Color: 0
Size: 791 Color: 1
Size: 150 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 0
Size: 520 Color: 1
Size: 406 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 1
Size: 698 Color: 4
Size: 232 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7221 Color: 3
Size: 759 Color: 4
Size: 156 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 2
Size: 751 Color: 0
Size: 150 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 0
Size: 520 Color: 1
Size: 322 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 3
Size: 612 Color: 2
Size: 248 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 1
Size: 676 Color: 4
Size: 158 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5701 Color: 4
Size: 1986 Color: 0
Size: 448 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 0
Size: 1651 Color: 4
Size: 104 Color: 1

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 4
Size: 1615 Color: 2

Bin 47: 1 of cap free
Amount of items: 4
Items: 
Size: 6550 Color: 3
Size: 1465 Color: 2
Size: 80 Color: 0
Size: 40 Color: 2

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6647 Color: 0
Size: 1008 Color: 2
Size: 480 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6689 Color: 0
Size: 1182 Color: 3
Size: 264 Color: 2

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 2
Size: 1284 Color: 0
Size: 88 Color: 2

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 0
Size: 1098 Color: 4
Size: 200 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6927 Color: 3
Size: 876 Color: 0
Size: 332 Color: 1

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 7018 Color: 1
Size: 1117 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7227 Color: 2
Size: 644 Color: 0
Size: 264 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5385 Color: 2
Size: 2589 Color: 3
Size: 160 Color: 0

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 5687 Color: 1
Size: 1302 Color: 0
Size: 1145 Color: 4

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 3
Size: 1964 Color: 1
Size: 388 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5949 Color: 2
Size: 2041 Color: 3
Size: 144 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 2
Size: 1468 Color: 0
Size: 264 Color: 1

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 3
Size: 1530 Color: 2

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 7196 Color: 4
Size: 938 Color: 3

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 2
Size: 3204 Color: 2
Size: 332 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 0
Size: 1023 Color: 1
Size: 1020 Color: 4

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 1
Size: 1823 Color: 0
Size: 176 Color: 1

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 6157 Color: 0
Size: 1068 Color: 1
Size: 908 Color: 3

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 6207 Color: 4
Size: 1075 Color: 2
Size: 851 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 1
Size: 1722 Color: 4
Size: 40 Color: 2

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 1
Size: 1120 Color: 0
Size: 216 Color: 1

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 3
Size: 1207 Color: 1

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 7167 Color: 3
Size: 934 Color: 1
Size: 32 Color: 2

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 3382 Color: 0
Size: 458 Color: 3

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4462 Color: 4
Size: 3062 Color: 1
Size: 608 Color: 2

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5023 Color: 0
Size: 2957 Color: 0
Size: 152 Color: 3

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 6110 Color: 1
Size: 2022 Color: 4

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 6199 Color: 4
Size: 1781 Color: 2
Size: 152 Color: 2

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6781 Color: 3
Size: 1351 Color: 1

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 4070 Color: 4
Size: 3389 Color: 2
Size: 672 Color: 1

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 2
Size: 3390 Color: 3
Size: 152 Color: 0

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 4998 Color: 0
Size: 2951 Color: 2
Size: 182 Color: 3

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 6094 Color: 3
Size: 1628 Color: 3
Size: 408 Color: 2

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 4
Size: 1670 Color: 3

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 3
Size: 830 Color: 2

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 4
Size: 1702 Color: 1
Size: 48 Color: 4

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 3
Size: 1027 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 6684 Color: 1
Size: 1444 Color: 3

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 6806 Color: 4
Size: 1322 Color: 3

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 6909 Color: 4
Size: 1219 Color: 3

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 7209 Color: 3
Size: 919 Color: 2

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 1
Size: 1197 Color: 3

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 7075 Color: 3
Size: 1052 Color: 1

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 2
Size: 1010 Color: 3

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 7264 Color: 3
Size: 863 Color: 4

Bin 93: 11 of cap free
Amount of items: 12
Items: 
Size: 2037 Color: 4
Size: 772 Color: 3
Size: 724 Color: 4
Size: 676 Color: 4
Size: 676 Color: 3
Size: 676 Color: 0
Size: 656 Color: 3
Size: 536 Color: 4
Size: 524 Color: 2
Size: 392 Color: 0
Size: 272 Color: 1
Size: 184 Color: 0

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 6853 Color: 1
Size: 1272 Color: 3

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 3
Size: 3222 Color: 0
Size: 456 Color: 1

Bin 96: 13 of cap free
Amount of items: 4
Items: 
Size: 4069 Color: 4
Size: 2260 Color: 1
Size: 1006 Color: 3
Size: 788 Color: 4

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 7013 Color: 1
Size: 1110 Color: 4

Bin 98: 15 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 1
Size: 885 Color: 4

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 5220 Color: 1
Size: 2900 Color: 3

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 4
Size: 1244 Color: 3

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 4
Size: 945 Color: 3

Bin 102: 18 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 1
Size: 3388 Color: 3
Size: 644 Color: 2

Bin 103: 21 of cap free
Amount of items: 4
Items: 
Size: 6509 Color: 4
Size: 1446 Color: 1
Size: 80 Color: 3
Size: 80 Color: 0

Bin 104: 22 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 3
Size: 1846 Color: 1

Bin 105: 22 of cap free
Amount of items: 2
Items: 
Size: 7043 Color: 2
Size: 1071 Color: 3

Bin 106: 23 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 4
Size: 1357 Color: 2

Bin 107: 25 of cap free
Amount of items: 3
Items: 
Size: 6142 Color: 0
Size: 1241 Color: 1
Size: 728 Color: 2

Bin 108: 27 of cap free
Amount of items: 2
Items: 
Size: 5031 Color: 1
Size: 3078 Color: 3

Bin 109: 30 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 1
Size: 1950 Color: 4
Size: 48 Color: 0

Bin 110: 30 of cap free
Amount of items: 2
Items: 
Size: 6702 Color: 3
Size: 1404 Color: 1

Bin 111: 30 of cap free
Amount of items: 2
Items: 
Size: 7232 Color: 1
Size: 874 Color: 3

Bin 112: 31 of cap free
Amount of items: 3
Items: 
Size: 4982 Color: 4
Size: 2595 Color: 0
Size: 528 Color: 3

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 5693 Color: 2
Size: 2412 Color: 1

Bin 114: 36 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 1
Size: 1471 Color: 2

Bin 115: 41 of cap free
Amount of items: 2
Items: 
Size: 4071 Color: 0
Size: 4024 Color: 2

Bin 116: 45 of cap free
Amount of items: 2
Items: 
Size: 5798 Color: 3
Size: 2293 Color: 4

Bin 117: 52 of cap free
Amount of items: 3
Items: 
Size: 4270 Color: 4
Size: 3238 Color: 1
Size: 576 Color: 2

Bin 118: 57 of cap free
Amount of items: 3
Items: 
Size: 4254 Color: 4
Size: 3040 Color: 0
Size: 785 Color: 3

Bin 119: 62 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 2
Size: 2646 Color: 1

Bin 120: 64 of cap free
Amount of items: 4
Items: 
Size: 4076 Color: 1
Size: 2031 Color: 3
Size: 1609 Color: 0
Size: 356 Color: 4

Bin 121: 72 of cap free
Amount of items: 2
Items: 
Size: 4294 Color: 3
Size: 3770 Color: 0

Bin 122: 74 of cap free
Amount of items: 2
Items: 
Size: 4486 Color: 4
Size: 3576 Color: 1

Bin 123: 74 of cap free
Amount of items: 2
Items: 
Size: 6370 Color: 0
Size: 1692 Color: 2

Bin 124: 77 of cap free
Amount of items: 2
Items: 
Size: 5999 Color: 4
Size: 2060 Color: 1

Bin 125: 78 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 3
Size: 2278 Color: 4

Bin 126: 82 of cap free
Amount of items: 2
Items: 
Size: 4476 Color: 4
Size: 3578 Color: 2

Bin 127: 85 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 3
Size: 3391 Color: 4

Bin 128: 100 of cap free
Amount of items: 2
Items: 
Size: 5406 Color: 1
Size: 2630 Color: 3

Bin 129: 106 of cap free
Amount of items: 2
Items: 
Size: 5394 Color: 4
Size: 2636 Color: 1

Bin 130: 130 of cap free
Amount of items: 2
Items: 
Size: 5388 Color: 2
Size: 2618 Color: 4

Bin 131: 132 of cap free
Amount of items: 2
Items: 
Size: 4962 Color: 0
Size: 3042 Color: 4

Bin 132: 134 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 2
Size: 2568 Color: 1
Size: 1356 Color: 3

Bin 133: 6010 of cap free
Amount of items: 13
Items: 
Size: 200 Color: 0
Size: 188 Color: 4
Size: 184 Color: 4
Size: 182 Color: 3
Size: 176 Color: 0
Size: 172 Color: 2
Size: 168 Color: 2
Size: 168 Color: 0
Size: 144 Color: 4
Size: 136 Color: 2
Size: 136 Color: 1
Size: 136 Color: 1
Size: 136 Color: 1

Total size: 1073952
Total free space: 8136

