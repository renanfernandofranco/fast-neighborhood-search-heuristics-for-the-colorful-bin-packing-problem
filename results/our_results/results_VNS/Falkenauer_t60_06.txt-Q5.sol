Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 337 Color: 1
Size: 269 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 250 Color: 1
Size: 252 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 4
Size: 262 Color: 1
Size: 253 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 0
Size: 300 Color: 1
Size: 250 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 310 Color: 2
Size: 287 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 355 Color: 2
Size: 265 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 339 Color: 4
Size: 292 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 365 Color: 1
Size: 261 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 322 Color: 4
Size: 304 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 0
Size: 297 Color: 1
Size: 276 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 344 Color: 0
Size: 281 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 3
Size: 321 Color: 4
Size: 274 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 322 Color: 2
Size: 278 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 276 Color: 0
Size: 273 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 279 Color: 4
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 276 Color: 1
Size: 275 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 328 Color: 4
Size: 284 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 317 Color: 0
Size: 259 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 3
Size: 284 Color: 3
Size: 252 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 357 Color: 2
Size: 275 Color: 3

Total size: 20000
Total free space: 0

