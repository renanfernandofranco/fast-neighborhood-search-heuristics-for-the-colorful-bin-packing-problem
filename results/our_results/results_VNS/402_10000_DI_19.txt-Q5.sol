Capicity Bin: 7896
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4986 Color: 3
Size: 2426 Color: 0
Size: 484 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 4
Size: 2124 Color: 0
Size: 344 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5573 Color: 0
Size: 1937 Color: 3
Size: 386 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5942 Color: 4
Size: 1748 Color: 2
Size: 206 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 1
Size: 1716 Color: 0
Size: 136 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 3
Size: 1541 Color: 4
Size: 306 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 2
Size: 1356 Color: 0
Size: 438 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6178 Color: 0
Size: 1434 Color: 4
Size: 284 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 0
Size: 1251 Color: 2
Size: 432 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 0
Size: 1330 Color: 1
Size: 264 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 0
Size: 1468 Color: 1
Size: 72 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6397 Color: 0
Size: 891 Color: 1
Size: 608 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 3
Size: 1252 Color: 2
Size: 248 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 4
Size: 1314 Color: 0
Size: 108 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 4
Size: 1122 Color: 3
Size: 224 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 2
Size: 841 Color: 3
Size: 504 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 1101 Color: 0
Size: 220 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6608 Color: 0
Size: 1080 Color: 4
Size: 208 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 2
Size: 1076 Color: 4
Size: 208 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 0
Size: 973 Color: 4
Size: 314 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 1
Size: 1073 Color: 0
Size: 166 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 0
Size: 1011 Color: 2
Size: 236 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 2
Size: 898 Color: 4
Size: 324 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 1
Size: 1124 Color: 0
Size: 88 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 1
Size: 1022 Color: 0
Size: 160 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 0
Size: 799 Color: 1
Size: 368 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6778 Color: 0
Size: 934 Color: 3
Size: 184 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 1
Size: 907 Color: 0
Size: 180 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6828 Color: 2
Size: 862 Color: 4
Size: 206 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6865 Color: 0
Size: 807 Color: 3
Size: 224 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 4
Size: 852 Color: 2
Size: 178 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 3
Size: 892 Color: 0
Size: 136 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6902 Color: 3
Size: 722 Color: 0
Size: 272 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 3
Size: 750 Color: 0
Size: 212 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 1
Size: 492 Color: 0
Size: 434 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 4
Size: 742 Color: 0
Size: 174 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 0
Size: 666 Color: 3
Size: 152 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 4
Size: 496 Color: 0
Size: 342 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 0
Size: 574 Color: 2
Size: 232 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 3
Size: 656 Color: 2
Size: 142 Color: 0

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4869 Color: 1
Size: 2878 Color: 0
Size: 148 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4939 Color: 2
Size: 2580 Color: 2
Size: 376 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5180 Color: 2
Size: 2523 Color: 0
Size: 192 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 2
Size: 2167 Color: 2
Size: 260 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5589 Color: 3
Size: 1866 Color: 1
Size: 440 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 4
Size: 1527 Color: 3
Size: 564 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5849 Color: 0
Size: 1630 Color: 2
Size: 416 Color: 4

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 2
Size: 1191 Color: 0
Size: 830 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 4
Size: 1686 Color: 0
Size: 200 Color: 1

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6033 Color: 2
Size: 1862 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 4
Size: 1707 Color: 2
Size: 104 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6247 Color: 1
Size: 1224 Color: 0
Size: 424 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6259 Color: 1
Size: 1012 Color: 0
Size: 624 Color: 2

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 2
Size: 1403 Color: 3
Size: 176 Color: 0

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 6322 Color: 3
Size: 1573 Color: 1

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6351 Color: 2
Size: 976 Color: 4
Size: 568 Color: 0

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6467 Color: 0
Size: 1082 Color: 3
Size: 346 Color: 4

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 6483 Color: 2
Size: 1180 Color: 0
Size: 232 Color: 1

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 6745 Color: 3
Size: 656 Color: 0
Size: 494 Color: 4

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 6761 Color: 2
Size: 1134 Color: 3

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 6849 Color: 0
Size: 656 Color: 4
Size: 390 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 6905 Color: 4
Size: 802 Color: 3
Size: 188 Color: 0

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 6939 Color: 2
Size: 572 Color: 2
Size: 384 Color: 0

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 3
Size: 827 Color: 2

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 4
Size: 3284 Color: 3
Size: 164 Color: 3

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 4
Size: 3044 Color: 0
Size: 214 Color: 3

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 1
Size: 2205 Color: 0
Size: 408 Color: 2

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 3
Size: 2406 Color: 0
Size: 132 Color: 4

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 5658 Color: 4
Size: 1289 Color: 0
Size: 947 Color: 2

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 4
Size: 2028 Color: 0
Size: 200 Color: 1

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 0
Size: 1923 Color: 4
Size: 154 Color: 1

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 5833 Color: 0
Size: 1721 Color: 1
Size: 340 Color: 4

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 6538 Color: 0
Size: 764 Color: 3
Size: 592 Color: 4

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 6822 Color: 3
Size: 1072 Color: 4

Bin 75: 3 of cap free
Amount of items: 9
Items: 
Size: 3949 Color: 4
Size: 632 Color: 4
Size: 536 Color: 2
Size: 512 Color: 0
Size: 504 Color: 4
Size: 480 Color: 3
Size: 448 Color: 4
Size: 416 Color: 2
Size: 416 Color: 1

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 3
Size: 2873 Color: 0
Size: 216 Color: 3

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 4872 Color: 4
Size: 2893 Color: 0
Size: 128 Color: 2

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 4927 Color: 3
Size: 2822 Color: 3
Size: 144 Color: 0

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 5265 Color: 1
Size: 2628 Color: 3

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 5297 Color: 0
Size: 2444 Color: 4
Size: 152 Color: 1

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 0
Size: 2193 Color: 2
Size: 336 Color: 2

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 6827 Color: 1
Size: 1026 Color: 3
Size: 40 Color: 0

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 2
Size: 1033 Color: 1

Bin 84: 5 of cap free
Amount of items: 3
Items: 
Size: 4918 Color: 1
Size: 2861 Color: 3
Size: 112 Color: 2

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 5793 Color: 3
Size: 2098 Color: 4

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 4
Size: 861 Color: 2

Bin 87: 6 of cap free
Amount of items: 3
Items: 
Size: 5549 Color: 2
Size: 2181 Color: 4
Size: 160 Color: 0

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 6929 Color: 4
Size: 961 Color: 2

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 0
Size: 2060 Color: 1
Size: 578 Color: 1

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 3
Size: 1165 Color: 1

Bin 91: 7 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 1
Size: 949 Color: 4

Bin 92: 8 of cap free
Amount of items: 3
Items: 
Size: 4465 Color: 2
Size: 3291 Color: 0
Size: 132 Color: 4

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 4
Size: 2936 Color: 1
Size: 204 Color: 3

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 5966 Color: 4
Size: 1922 Color: 2

Bin 95: 9 of cap free
Amount of items: 3
Items: 
Size: 4425 Color: 2
Size: 3286 Color: 0
Size: 176 Color: 3

Bin 96: 10 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 4
Size: 3290 Color: 4
Size: 520 Color: 0

Bin 97: 10 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 3
Size: 3472 Color: 3
Size: 170 Color: 2

Bin 98: 11 of cap free
Amount of items: 3
Items: 
Size: 4449 Color: 0
Size: 2576 Color: 4
Size: 860 Color: 2

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 3
Size: 1283 Color: 2

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 6621 Color: 1
Size: 1262 Color: 3

Bin 101: 14 of cap free
Amount of items: 5
Items: 
Size: 3956 Color: 0
Size: 1516 Color: 4
Size: 980 Color: 2
Size: 774 Color: 3
Size: 656 Color: 0

Bin 102: 16 of cap free
Amount of items: 6
Items: 
Size: 3954 Color: 0
Size: 1498 Color: 0
Size: 692 Color: 1
Size: 682 Color: 2
Size: 674 Color: 2
Size: 380 Color: 3

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 1
Size: 2116 Color: 3

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 1
Size: 1324 Color: 4

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 6839 Color: 3
Size: 1041 Color: 1

Bin 106: 17 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 3
Size: 1414 Color: 2

Bin 107: 17 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 4
Size: 1133 Color: 2
Size: 80 Color: 2

Bin 108: 17 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 4
Size: 873 Color: 2

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 6124 Color: 1
Size: 1753 Color: 3

Bin 110: 21 of cap free
Amount of items: 2
Items: 
Size: 6889 Color: 2
Size: 986 Color: 1

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 3
Size: 1375 Color: 2

Bin 112: 26 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 3
Size: 1484 Color: 1

Bin 113: 31 of cap free
Amount of items: 3
Items: 
Size: 4481 Color: 4
Size: 3188 Color: 3
Size: 196 Color: 0

Bin 114: 31 of cap free
Amount of items: 2
Items: 
Size: 6312 Color: 1
Size: 1553 Color: 3

Bin 115: 32 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 3
Size: 2988 Color: 4
Size: 560 Color: 0

Bin 116: 32 of cap free
Amount of items: 2
Items: 
Size: 5382 Color: 3
Size: 2482 Color: 1

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 6685 Color: 2
Size: 1179 Color: 4

Bin 118: 34 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 2
Size: 2268 Color: 1

Bin 119: 35 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 1
Size: 2475 Color: 0
Size: 868 Color: 3

Bin 120: 39 of cap free
Amount of items: 2
Items: 
Size: 5010 Color: 3
Size: 2847 Color: 4

Bin 121: 46 of cap free
Amount of items: 5
Items: 
Size: 3950 Color: 1
Size: 1488 Color: 0
Size: 1186 Color: 0
Size: 656 Color: 3
Size: 570 Color: 2

Bin 122: 47 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 3
Size: 1365 Color: 4

Bin 123: 51 of cap free
Amount of items: 2
Items: 
Size: 6065 Color: 2
Size: 1780 Color: 4

Bin 124: 60 of cap free
Amount of items: 2
Items: 
Size: 6202 Color: 2
Size: 1634 Color: 1

Bin 125: 70 of cap free
Amount of items: 3
Items: 
Size: 3951 Color: 3
Size: 2142 Color: 3
Size: 1733 Color: 1

Bin 126: 72 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 3
Size: 1548 Color: 1

Bin 127: 77 of cap free
Amount of items: 3
Items: 
Size: 3904 Color: 2
Size: 2724 Color: 2
Size: 1191 Color: 0

Bin 128: 95 of cap free
Amount of items: 2
Items: 
Size: 5844 Color: 4
Size: 1957 Color: 1

Bin 129: 97 of cap free
Amount of items: 2
Items: 
Size: 4510 Color: 4
Size: 3289 Color: 1

Bin 130: 101 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 2
Size: 2465 Color: 3

Bin 131: 106 of cap free
Amount of items: 2
Items: 
Size: 4972 Color: 3
Size: 2818 Color: 2

Bin 132: 134 of cap free
Amount of items: 25
Items: 
Size: 480 Color: 1
Size: 448 Color: 0
Size: 440 Color: 3
Size: 400 Color: 2
Size: 372 Color: 4
Size: 352 Color: 3
Size: 350 Color: 2
Size: 336 Color: 2
Size: 310 Color: 1
Size: 304 Color: 2
Size: 304 Color: 0
Size: 296 Color: 2
Size: 296 Color: 2
Size: 296 Color: 0
Size: 288 Color: 1
Size: 280 Color: 3
Size: 280 Color: 2
Size: 274 Color: 1
Size: 264 Color: 2
Size: 256 Color: 0
Size: 256 Color: 0
Size: 248 Color: 0
Size: 238 Color: 4
Size: 200 Color: 1
Size: 194 Color: 4

Bin 133: 6250 of cap free
Amount of items: 9
Items: 
Size: 248 Color: 0
Size: 234 Color: 0
Size: 190 Color: 2
Size: 168 Color: 3
Size: 168 Color: 1
Size: 168 Color: 1
Size: 164 Color: 3
Size: 158 Color: 3
Size: 148 Color: 1

Total size: 1042272
Total free space: 7896

