Capicity Bin: 6240
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3128 Color: 2
Size: 2421 Color: 0
Size: 691 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3394 Color: 2
Size: 2604 Color: 1
Size: 242 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3620 Color: 3
Size: 2104 Color: 0
Size: 516 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3920 Color: 4
Size: 1804 Color: 0
Size: 516 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 4
Size: 1960 Color: 3
Size: 112 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4323 Color: 0
Size: 1839 Color: 1
Size: 78 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 4
Size: 1544 Color: 3
Size: 304 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4390 Color: 0
Size: 1662 Color: 3
Size: 188 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 1440 Color: 0
Size: 140 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 0
Size: 960 Color: 1
Size: 518 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4811 Color: 1
Size: 1191 Color: 1
Size: 238 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 1
Size: 1032 Color: 3
Size: 288 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 2
Size: 1106 Color: 0
Size: 162 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5016 Color: 2
Size: 1060 Color: 0
Size: 164 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 2
Size: 982 Color: 1
Size: 196 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 1
Size: 811 Color: 0
Size: 376 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 0
Size: 810 Color: 0
Size: 304 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 4
Size: 699 Color: 0
Size: 384 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 3
Size: 900 Color: 0
Size: 176 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 3
Size: 729 Color: 4
Size: 328 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5190 Color: 4
Size: 888 Color: 1
Size: 162 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 3
Size: 934 Color: 1
Size: 114 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5254 Color: 0
Size: 848 Color: 0
Size: 138 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5294 Color: 0
Size: 710 Color: 3
Size: 236 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5300 Color: 2
Size: 764 Color: 0
Size: 176 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 0
Size: 784 Color: 1
Size: 124 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 0
Size: 580 Color: 2
Size: 324 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5358 Color: 0
Size: 762 Color: 1
Size: 120 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5416 Color: 3
Size: 648 Color: 1
Size: 176 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 4
Size: 564 Color: 2
Size: 254 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5436 Color: 3
Size: 440 Color: 2
Size: 364 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 1
Size: 676 Color: 3
Size: 104 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5486 Color: 1
Size: 432 Color: 4
Size: 322 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 3
Size: 472 Color: 2
Size: 250 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5544 Color: 3
Size: 578 Color: 1
Size: 118 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 4
Size: 504 Color: 0
Size: 188 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 1
Size: 482 Color: 0
Size: 156 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 4
Size: 582 Color: 2
Size: 72 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5608 Color: 4
Size: 418 Color: 1
Size: 214 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 3353 Color: 0
Size: 2586 Color: 0
Size: 300 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 0
Size: 1529 Color: 1
Size: 584 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4307 Color: 4
Size: 1740 Color: 0
Size: 192 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 4
Size: 1255 Color: 1
Size: 336 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 4735 Color: 4
Size: 1284 Color: 1
Size: 220 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 4795 Color: 1
Size: 1162 Color: 0
Size: 282 Color: 2

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4947 Color: 2
Size: 1208 Color: 4
Size: 84 Color: 0

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 5112 Color: 2
Size: 1127 Color: 4

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5229 Color: 4
Size: 930 Color: 3
Size: 80 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 2
Size: 840 Color: 3
Size: 32 Color: 3

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 5403 Color: 0
Size: 652 Color: 1
Size: 184 Color: 2

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 5442 Color: 3
Size: 797 Color: 4

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 3970 Color: 0
Size: 1492 Color: 2
Size: 776 Color: 4

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 4456 Color: 0
Size: 1654 Color: 3
Size: 128 Color: 2

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4518 Color: 1
Size: 1496 Color: 2
Size: 224 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4843 Color: 1
Size: 1219 Color: 3
Size: 176 Color: 0

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4930 Color: 1
Size: 1148 Color: 1
Size: 160 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 4
Size: 712 Color: 3
Size: 416 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5384 Color: 0
Size: 822 Color: 1
Size: 32 Color: 3

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 5550 Color: 4
Size: 688 Color: 3

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 5572 Color: 0
Size: 666 Color: 2

Bin 61: 3 of cap free
Amount of items: 9
Items: 
Size: 3121 Color: 0
Size: 516 Color: 4
Size: 512 Color: 4
Size: 480 Color: 0
Size: 416 Color: 0
Size: 344 Color: 2
Size: 336 Color: 2
Size: 296 Color: 1
Size: 216 Color: 1

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 3337 Color: 1
Size: 2648 Color: 2
Size: 252 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 3662 Color: 2
Size: 2407 Color: 1
Size: 168 Color: 2

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 3
Size: 2408 Color: 0
Size: 104 Color: 2

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 4
Size: 2097 Color: 2
Size: 64 Color: 4

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 4714 Color: 3
Size: 1327 Color: 1
Size: 196 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 3
Size: 1259 Color: 0
Size: 64 Color: 2

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 4846 Color: 4
Size: 1391 Color: 2

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 4983 Color: 1
Size: 1254 Color: 3

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 5326 Color: 2
Size: 911 Color: 1

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 3980 Color: 1
Size: 2112 Color: 0
Size: 144 Color: 3

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4156 Color: 1
Size: 1976 Color: 4
Size: 104 Color: 0

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 2
Size: 1417 Color: 0
Size: 72 Color: 2

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 5222 Color: 4
Size: 1014 Color: 3

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 5312 Color: 4
Size: 924 Color: 3

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 4
Size: 2083 Color: 2
Size: 256 Color: 1

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 5103 Color: 2
Size: 1084 Color: 4
Size: 48 Color: 0

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 5299 Color: 1
Size: 936 Color: 2

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 3148 Color: 3
Size: 2604 Color: 4
Size: 482 Color: 1

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 5140 Color: 2
Size: 1094 Color: 3

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 5604 Color: 4
Size: 630 Color: 2

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 4035 Color: 1
Size: 1966 Color: 0
Size: 232 Color: 2

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 0
Size: 1725 Color: 4
Size: 250 Color: 3

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 4731 Color: 2
Size: 1370 Color: 4
Size: 132 Color: 2

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 4715 Color: 4
Size: 1518 Color: 2

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 2
Size: 1049 Color: 4
Size: 276 Color: 4

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 5390 Color: 2
Size: 843 Color: 3

Bin 88: 7 of cap free
Amount of items: 3
Items: 
Size: 5419 Color: 1
Size: 790 Color: 3
Size: 24 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 4407 Color: 2
Size: 1825 Color: 3

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 4
Size: 1762 Color: 2
Size: 48 Color: 4

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 4541 Color: 3
Size: 1611 Color: 1
Size: 80 Color: 0

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 4700 Color: 4
Size: 1532 Color: 3

Bin 93: 8 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 0
Size: 949 Color: 2
Size: 32 Color: 2

Bin 94: 9 of cap free
Amount of items: 2
Items: 
Size: 4951 Color: 1
Size: 1280 Color: 2

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 5240 Color: 1
Size: 991 Color: 4

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 4792 Color: 3
Size: 1438 Color: 4

Bin 97: 11 of cap free
Amount of items: 4
Items: 
Size: 4040 Color: 1
Size: 1324 Color: 3
Size: 785 Color: 3
Size: 80 Color: 4

Bin 98: 13 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 4
Size: 1336 Color: 1
Size: 112 Color: 3

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 5026 Color: 4
Size: 1201 Color: 2

Bin 100: 14 of cap free
Amount of items: 3
Items: 
Size: 3146 Color: 3
Size: 2594 Color: 1
Size: 486 Color: 2

Bin 101: 14 of cap free
Amount of items: 3
Items: 
Size: 3741 Color: 0
Size: 2417 Color: 3
Size: 68 Color: 4

Bin 102: 15 of cap free
Amount of items: 3
Items: 
Size: 3132 Color: 4
Size: 2212 Color: 0
Size: 881 Color: 4

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 5267 Color: 1
Size: 958 Color: 3

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 4940 Color: 3
Size: 1284 Color: 1

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 5321 Color: 1
Size: 903 Color: 3

Bin 106: 17 of cap free
Amount of items: 3
Items: 
Size: 4573 Color: 4
Size: 1274 Color: 3
Size: 376 Color: 0

Bin 107: 17 of cap free
Amount of items: 2
Items: 
Size: 5435 Color: 3
Size: 788 Color: 2

Bin 108: 18 of cap free
Amount of items: 3
Items: 
Size: 3720 Color: 4
Size: 2374 Color: 0
Size: 128 Color: 3

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 4598 Color: 1
Size: 1623 Color: 3

Bin 110: 19 of cap free
Amount of items: 2
Items: 
Size: 4868 Color: 4
Size: 1353 Color: 2

Bin 111: 22 of cap free
Amount of items: 2
Items: 
Size: 5480 Color: 4
Size: 738 Color: 3

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 4220 Color: 2
Size: 1996 Color: 4

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 4532 Color: 2
Size: 1684 Color: 1

Bin 114: 25 of cap free
Amount of items: 3
Items: 
Size: 3321 Color: 2
Size: 2528 Color: 3
Size: 366 Color: 3

Bin 115: 26 of cap free
Amount of items: 2
Items: 
Size: 5232 Color: 4
Size: 982 Color: 0

Bin 116: 29 of cap free
Amount of items: 3
Items: 
Size: 3130 Color: 4
Size: 2601 Color: 0
Size: 480 Color: 4

Bin 117: 30 of cap free
Amount of items: 2
Items: 
Size: 3352 Color: 3
Size: 2858 Color: 2

Bin 118: 34 of cap free
Amount of items: 7
Items: 
Size: 3122 Color: 2
Size: 602 Color: 3
Size: 600 Color: 4
Size: 534 Color: 4
Size: 532 Color: 3
Size: 512 Color: 0
Size: 304 Color: 1

Bin 119: 34 of cap free
Amount of items: 2
Items: 
Size: 5094 Color: 1
Size: 1112 Color: 2

Bin 120: 36 of cap free
Amount of items: 2
Items: 
Size: 4320 Color: 1
Size: 1884 Color: 4

Bin 121: 39 of cap free
Amount of items: 5
Items: 
Size: 3124 Color: 1
Size: 952 Color: 0
Size: 942 Color: 4
Size: 767 Color: 2
Size: 416 Color: 0

Bin 122: 39 of cap free
Amount of items: 2
Items: 
Size: 4051 Color: 2
Size: 2150 Color: 0

Bin 123: 40 of cap free
Amount of items: 27
Items: 
Size: 464 Color: 3
Size: 360 Color: 3
Size: 352 Color: 3
Size: 284 Color: 2
Size: 272 Color: 0
Size: 256 Color: 2
Size: 256 Color: 2
Size: 240 Color: 4
Size: 232 Color: 4
Size: 224 Color: 3
Size: 224 Color: 3
Size: 224 Color: 3
Size: 224 Color: 2
Size: 216 Color: 4
Size: 208 Color: 3
Size: 208 Color: 1
Size: 208 Color: 0
Size: 200 Color: 3
Size: 196 Color: 3
Size: 188 Color: 0
Size: 188 Color: 0
Size: 180 Color: 1
Size: 176 Color: 1
Size: 160 Color: 1
Size: 156 Color: 2
Size: 152 Color: 1
Size: 152 Color: 1

Bin 124: 40 of cap free
Amount of items: 2
Items: 
Size: 4012 Color: 0
Size: 2188 Color: 4

Bin 125: 52 of cap free
Amount of items: 2
Items: 
Size: 4452 Color: 0
Size: 1736 Color: 4

Bin 126: 53 of cap free
Amount of items: 2
Items: 
Size: 4293 Color: 2
Size: 1894 Color: 1

Bin 127: 56 of cap free
Amount of items: 2
Items: 
Size: 4094 Color: 2
Size: 2090 Color: 1

Bin 128: 72 of cap free
Amount of items: 3
Items: 
Size: 3138 Color: 3
Size: 2602 Color: 2
Size: 428 Color: 1

Bin 129: 72 of cap free
Amount of items: 2
Items: 
Size: 3588 Color: 0
Size: 2580 Color: 3

Bin 130: 73 of cap free
Amount of items: 2
Items: 
Size: 3734 Color: 2
Size: 2433 Color: 1

Bin 131: 77 of cap free
Amount of items: 4
Items: 
Size: 3125 Color: 3
Size: 1271 Color: 1
Size: 1079 Color: 2
Size: 688 Color: 4

Bin 132: 88 of cap free
Amount of items: 2
Items: 
Size: 3341 Color: 3
Size: 2811 Color: 0

Bin 133: 4808 of cap free
Amount of items: 10
Items: 
Size: 192 Color: 4
Size: 180 Color: 0
Size: 152 Color: 0
Size: 144 Color: 4
Size: 144 Color: 4
Size: 144 Color: 2
Size: 140 Color: 1
Size: 112 Color: 2
Size: 112 Color: 2
Size: 112 Color: 1

Total size: 823680
Total free space: 6240

