Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 247
Size: 255 Color: 23
Size: 250 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 184
Size: 336 Color: 143
Size: 280 Color: 85

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 208
Size: 306 Color: 123
Size: 268 Color: 69

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 182
Size: 372 Color: 179
Size: 254 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 223
Size: 299 Color: 112
Size: 251 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 212
Size: 287 Color: 97
Size: 281 Color: 87

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 241
Size: 260 Color: 46
Size: 251 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 232
Size: 270 Color: 70
Size: 250 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 200
Size: 310 Color: 128
Size: 283 Color: 91

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 245
Size: 258 Color: 35
Size: 251 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 168
Size: 360 Color: 165
Size: 277 Color: 78

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 221
Size: 301 Color: 114
Size: 251 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 150
Size: 293 Color: 104
Size: 363 Color: 169

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 163
Size: 357 Color: 161
Size: 285 Color: 92

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 224
Size: 278 Color: 81
Size: 265 Color: 60

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 244
Size: 258 Color: 39
Size: 251 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 167
Size: 362 Color: 166
Size: 276 Color: 75

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 199
Size: 330 Color: 139
Size: 263 Color: 54

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 189
Size: 307 Color: 125
Size: 305 Color: 119

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 202
Size: 332 Color: 141
Size: 258 Color: 34

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 209
Size: 303 Color: 116
Size: 267 Color: 62

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 242
Size: 259 Color: 42
Size: 251 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 225
Size: 279 Color: 83
Size: 263 Color: 55

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 235
Size: 263 Color: 53
Size: 253 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 160
Size: 355 Color: 157
Size: 289 Color: 100

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 192
Size: 277 Color: 79
Size: 329 Color: 138

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 170
Size: 342 Color: 148
Size: 293 Color: 105

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 191
Size: 352 Color: 154
Size: 257 Color: 33

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 236
Size: 258 Color: 38
Size: 258 Color: 37

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 9
Size: 250 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 164
Size: 352 Color: 153
Size: 288 Color: 99

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 190
Size: 306 Color: 124
Size: 303 Color: 117

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 175
Size: 367 Color: 172
Size: 264 Color: 58

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 178
Size: 329 Color: 137
Size: 300 Color: 113

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 214
Size: 297 Color: 110
Size: 267 Color: 65

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 194
Size: 317 Color: 131
Size: 287 Color: 96

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 177
Size: 367 Color: 173
Size: 262 Color: 50

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 240
Size: 257 Color: 32
Size: 255 Color: 22

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 226
Size: 288 Color: 98
Size: 253 Color: 19

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 216
Size: 285 Color: 93
Size: 276 Color: 77

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 219
Size: 294 Color: 107
Size: 264 Color: 56

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 217
Size: 281 Color: 86
Size: 279 Color: 84

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 205
Size: 319 Color: 132
Size: 259 Color: 41

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 243
Size: 256 Color: 27
Size: 254 Color: 20

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 246
Size: 256 Color: 28
Size: 252 Color: 15

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 210
Size: 303 Color: 118
Size: 267 Color: 64

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 204
Size: 330 Color: 140
Size: 256 Color: 29

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 222
Size: 283 Color: 90
Size: 268 Color: 66

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 234
Size: 259 Color: 44
Size: 258 Color: 36

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 213
Size: 293 Color: 106
Size: 272 Color: 73

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 162
Size: 356 Color: 159
Size: 287 Color: 95

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 220
Size: 290 Color: 101
Size: 265 Color: 59

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 218
Size: 282 Color: 89
Size: 278 Color: 82

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 233
Size: 262 Color: 48
Size: 255 Color: 24

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 197
Size: 337 Color: 144
Size: 262 Color: 51

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 239
Size: 263 Color: 52
Size: 250 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 228
Size: 278 Color: 80
Size: 256 Color: 25

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 183
Size: 323 Color: 136
Size: 296 Color: 109

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 149
Size: 342 Color: 147
Size: 315 Color: 130

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 230
Size: 272 Color: 72
Size: 259 Color: 43

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 237
Size: 264 Color: 57
Size: 250 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 231
Size: 273 Color: 74
Size: 251 Color: 10

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 207
Size: 306 Color: 122
Size: 268 Color: 67

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 188
Size: 309 Color: 126
Size: 305 Color: 120

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 227
Size: 281 Color: 88
Size: 253 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 193
Size: 313 Color: 129
Size: 292 Color: 102

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 215
Size: 297 Color: 111
Size: 266 Color: 61

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 196
Size: 351 Color: 151
Size: 250 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 238
Size: 257 Color: 31
Size: 257 Color: 30

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 180
Size: 352 Color: 155
Size: 276 Color: 76

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 206
Size: 320 Color: 133
Size: 256 Color: 26

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 181
Size: 340 Color: 146
Size: 286 Color: 94

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 176
Size: 368 Color: 174
Size: 262 Color: 49

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 198
Size: 338 Color: 145
Size: 260 Color: 45

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 211
Size: 302 Color: 115
Size: 267 Color: 63

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 187
Size: 365 Color: 171
Size: 251 Color: 14

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 203
Size: 321 Color: 134
Size: 268 Color: 68

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 186
Size: 310 Color: 127
Size: 306 Color: 121

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 185
Size: 322 Color: 135
Size: 294 Color: 108

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 229
Size: 271 Color: 71
Size: 260 Color: 47

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 195
Size: 351 Color: 152
Size: 253 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 201
Size: 334 Color: 142
Size: 258 Color: 40

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 158
Size: 353 Color: 156
Size: 292 Color: 103

Total size: 83000
Total free space: 0

