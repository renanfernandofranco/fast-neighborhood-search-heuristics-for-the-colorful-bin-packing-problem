Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 216
Size: 276 Color: 81
Size: 286 Color: 95

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 234
Size: 269 Color: 63
Size: 255 Color: 23

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 238
Size: 264 Color: 51
Size: 257 Color: 34

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 254 Color: 21
Size: 496 Color: 246
Size: 250 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 164
Size: 336 Color: 150
Size: 304 Color: 120

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 188
Size: 312 Color: 126
Size: 299 Color: 113

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 187
Size: 347 Color: 155
Size: 267 Color: 55

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 245
Size: 253 Color: 17
Size: 252 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 202
Size: 294 Color: 106
Size: 291 Color: 101

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 243
Size: 257 Color: 32
Size: 254 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 224
Size: 277 Color: 84
Size: 265 Color: 52

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 172
Size: 369 Color: 170
Size: 257 Color: 35

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 244
Size: 256 Color: 28
Size: 252 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 206
Size: 324 Color: 138
Size: 259 Color: 40

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 230
Size: 273 Color: 74
Size: 258 Color: 37

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 181
Size: 323 Color: 136
Size: 295 Color: 107

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 192
Size: 319 Color: 133
Size: 289 Color: 98

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 226
Size: 269 Color: 61
Size: 268 Color: 60

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 237
Size: 268 Color: 58
Size: 255 Color: 22

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 235
Size: 268 Color: 57
Size: 256 Color: 31

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 239
Size: 270 Color: 67
Size: 250 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 209
Size: 304 Color: 119
Size: 274 Color: 77

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 228
Size: 285 Color: 94
Size: 250 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 194
Size: 347 Color: 153
Size: 252 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 189
Size: 356 Color: 162
Size: 254 Color: 20

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 201
Size: 323 Color: 137
Size: 263 Color: 50

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 225
Size: 276 Color: 82
Size: 265 Color: 53

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 232
Size: 270 Color: 66
Size: 255 Color: 25

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 176
Size: 356 Color: 160
Size: 266 Color: 54

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 175
Size: 362 Color: 167
Size: 261 Color: 45

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 163
Size: 348 Color: 156
Size: 295 Color: 108

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 213
Size: 310 Color: 124
Size: 257 Color: 36

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 152
Size: 334 Color: 146
Size: 325 Color: 140

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 242
Size: 262 Color: 47
Size: 250 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 185
Size: 347 Color: 154
Size: 269 Color: 64

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 169
Size: 353 Color: 158
Size: 279 Color: 88

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 12
Size: 250 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 179
Size: 362 Color: 166
Size: 256 Color: 29

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 191
Size: 313 Color: 129
Size: 296 Color: 109

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 217
Size: 297 Color: 110
Size: 259 Color: 41

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 218
Size: 294 Color: 105
Size: 260 Color: 42

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 240
Size: 259 Color: 39
Size: 257 Color: 33

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 177
Size: 321 Color: 134
Size: 300 Color: 114

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 236
Size: 274 Color: 75
Size: 250 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 223
Size: 273 Color: 73
Size: 271 Color: 68

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 204
Size: 334 Color: 147
Size: 250 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 168
Size: 338 Color: 151
Size: 299 Color: 112

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 200
Size: 326 Color: 142
Size: 261 Color: 43

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 183
Size: 315 Color: 131
Size: 302 Color: 117

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 241
Size: 262 Color: 48
Size: 251 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 207
Size: 326 Color: 141
Size: 256 Color: 30

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 221
Size: 278 Color: 87
Size: 271 Color: 69

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 229
Size: 278 Color: 86
Size: 255 Color: 26

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 211
Size: 307 Color: 121
Size: 262 Color: 49

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 205
Size: 334 Color: 145
Size: 250 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 215
Size: 287 Color: 96
Size: 275 Color: 78

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 210
Size: 298 Color: 111
Size: 271 Color: 70

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 231
Size: 277 Color: 83
Size: 250 Color: 6

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 186
Size: 323 Color: 135
Size: 292 Color: 103

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 233
Size: 273 Color: 72
Size: 252 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 182
Size: 356 Color: 161
Size: 261 Color: 44

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 247
Size: 254 Color: 19
Size: 250 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 212
Size: 309 Color: 123
Size: 259 Color: 38

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 214
Size: 291 Color: 102
Size: 275 Color: 79

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 178
Size: 330 Color: 144
Size: 290 Color: 99

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 219
Size: 281 Color: 90
Size: 272 Color: 71

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 174
Size: 374 Color: 173
Size: 250 Color: 5

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 203
Size: 309 Color: 122
Size: 276 Color: 80

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 193
Size: 329 Color: 143
Size: 274 Color: 76

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 184
Size: 313 Color: 128
Size: 303 Color: 118

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 190
Size: 310 Color: 125
Size: 300 Color: 115

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 222
Size: 280 Color: 89
Size: 269 Color: 62

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 195
Size: 312 Color: 127
Size: 283 Color: 93

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 208
Size: 313 Color: 130
Size: 267 Color: 56

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 196
Size: 301 Color: 116
Size: 294 Color: 104

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 227
Size: 281 Color: 91
Size: 256 Color: 27

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 220
Size: 289 Color: 97
Size: 262 Color: 46

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 165
Size: 349 Color: 157
Size: 291 Color: 100

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 198
Size: 324 Color: 139
Size: 268 Color: 59

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 199
Size: 335 Color: 148
Size: 255 Color: 24

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 180
Size: 335 Color: 149
Size: 283 Color: 92

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 197
Size: 316 Color: 132
Size: 278 Color: 85

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 171
Size: 356 Color: 159
Size: 270 Color: 65

Total size: 83000
Total free space: 0

