Capicity Bin: 2428
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 164
Size: 635 Color: 109
Size: 126 Color: 46

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 167
Size: 670 Color: 115
Size: 60 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 171
Size: 523 Color: 102
Size: 104 Color: 39

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1834 Color: 174
Size: 498 Color: 99
Size: 96 Color: 35

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 175
Size: 526 Color: 103
Size: 64 Color: 20

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1885 Color: 176
Size: 391 Color: 93
Size: 152 Color: 55

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 178
Size: 494 Color: 98
Size: 24 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 180
Size: 382 Color: 91
Size: 90 Color: 32

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 181
Size: 309 Color: 80
Size: 160 Color: 60

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 182
Size: 338 Color: 83
Size: 124 Color: 44

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 185
Size: 378 Color: 89
Size: 72 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 186
Size: 375 Color: 88
Size: 74 Color: 25

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 187
Size: 369 Color: 86
Size: 76 Color: 26

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 189
Size: 342 Color: 84
Size: 64 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 192
Size: 307 Color: 79
Size: 60 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 193
Size: 222 Color: 71
Size: 144 Color: 53

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 195
Size: 208 Color: 70
Size: 126 Color: 47

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 196
Size: 273 Color: 75
Size: 54 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2105 Color: 198
Size: 271 Color: 74
Size: 52 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 199
Size: 202 Color: 69
Size: 92 Color: 33

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2162 Color: 201
Size: 190 Color: 65
Size: 76 Color: 27

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 161
Size: 769 Color: 118

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 162
Size: 638 Color: 110
Size: 126 Color: 48

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 170
Size: 629 Color: 107

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 177
Size: 530 Color: 104
Size: 8 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 184
Size: 453 Color: 97

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 2083 Color: 194
Size: 344 Color: 85

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 2102 Color: 197
Size: 325 Color: 82

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2135 Color: 200
Size: 292 Color: 77

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 165
Size: 659 Color: 113
Size: 96 Color: 34

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1454 Color: 149
Size: 971 Color: 132

Bin 32: 3 of cap free
Amount of items: 4
Items: 
Size: 1586 Color: 157
Size: 771 Color: 119
Size: 36 Color: 5
Size: 32 Color: 4

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 169
Size: 631 Color: 108

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 179
Size: 382 Color: 90
Size: 88 Color: 31

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 190
Size: 395 Color: 94
Size: 4 Color: 0

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 191
Size: 386 Color: 92

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 152
Size: 655 Color: 112
Size: 274 Color: 76

Bin 38: 6 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 160
Size: 779 Color: 121

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1970 Color: 183
Size: 451 Color: 96

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1987 Color: 188
Size: 434 Color: 95

Bin 41: 9 of cap free
Amount of items: 2
Items: 
Size: 1503 Color: 154
Size: 916 Color: 126

Bin 42: 10 of cap free
Amount of items: 2
Items: 
Size: 1481 Color: 151
Size: 937 Color: 127

Bin 43: 10 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 156
Size: 836 Color: 124
Size: 40 Color: 6

Bin 44: 10 of cap free
Amount of items: 2
Items: 
Size: 1779 Color: 168
Size: 639 Color: 111

Bin 45: 11 of cap free
Amount of items: 2
Items: 
Size: 1626 Color: 158
Size: 791 Color: 122

Bin 46: 11 of cap free
Amount of items: 2
Items: 
Size: 1675 Color: 166
Size: 742 Color: 117

Bin 47: 13 of cap free
Amount of items: 2
Items: 
Size: 1219 Color: 141
Size: 1196 Color: 138

Bin 48: 14 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 150
Size: 955 Color: 131

Bin 49: 14 of cap free
Amount of items: 2
Items: 
Size: 1639 Color: 159
Size: 775 Color: 120

Bin 50: 15 of cap free
Amount of items: 2
Items: 
Size: 1803 Color: 173
Size: 610 Color: 106

Bin 51: 17 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 172
Size: 609 Color: 105

Bin 52: 21 of cap free
Amount of items: 9
Items: 
Size: 521 Color: 100
Size: 371 Color: 87
Size: 310 Color: 81
Size: 306 Color: 78
Size: 246 Color: 73
Size: 245 Color: 72
Size: 200 Color: 68
Size: 104 Color: 37
Size: 104 Color: 36

Bin 53: 23 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 148
Size: 1011 Color: 135
Size: 44 Color: 9

Bin 54: 23 of cap free
Amount of items: 4
Items: 
Size: 1507 Color: 155
Size: 814 Color: 123
Size: 44 Color: 8
Size: 40 Color: 7

Bin 55: 27 of cap free
Amount of items: 2
Items: 
Size: 1499 Color: 153
Size: 902 Color: 125

Bin 56: 28 of cap free
Amount of items: 4
Items: 
Size: 1290 Color: 146
Size: 1010 Color: 134
Size: 52 Color: 12
Size: 48 Color: 11

Bin 57: 28 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 163
Size: 702 Color: 116
Size: 32 Color: 3

Bin 58: 29 of cap free
Amount of items: 3
Items: 
Size: 1215 Color: 139
Size: 662 Color: 114
Size: 522 Color: 101

Bin 59: 29 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 147
Size: 1060 Color: 136
Size: 48 Color: 10

Bin 60: 40 of cap free
Amount of items: 16
Items: 
Size: 194 Color: 67
Size: 194 Color: 66
Size: 190 Color: 64
Size: 188 Color: 63
Size: 188 Color: 62
Size: 176 Color: 61
Size: 156 Color: 59
Size: 154 Color: 58
Size: 154 Color: 57
Size: 130 Color: 49
Size: 124 Color: 45
Size: 120 Color: 43
Size: 108 Color: 42
Size: 104 Color: 41
Size: 104 Color: 40
Size: 104 Color: 38

Bin 61: 42 of cap free
Amount of items: 4
Items: 
Size: 1287 Color: 145
Size: 975 Color: 133
Size: 64 Color: 18
Size: 60 Color: 16

Bin 62: 50 of cap free
Amount of items: 4
Items: 
Size: 1283 Color: 144
Size: 951 Color: 130
Size: 72 Color: 22
Size: 72 Color: 21

Bin 63: 58 of cap free
Amount of items: 4
Items: 
Size: 1259 Color: 142
Size: 949 Color: 128
Size: 84 Color: 30
Size: 78 Color: 29

Bin 64: 61 of cap free
Amount of items: 2
Items: 
Size: 1218 Color: 140
Size: 1149 Color: 137

Bin 65: 63 of cap free
Amount of items: 4
Items: 
Size: 1263 Color: 143
Size: 950 Color: 129
Size: 78 Color: 28
Size: 74 Color: 24

Bin 66: 1720 of cap free
Amount of items: 5
Items: 
Size: 154 Color: 56
Size: 152 Color: 54
Size: 140 Color: 52
Size: 132 Color: 51
Size: 130 Color: 50

Total size: 157820
Total free space: 2428

