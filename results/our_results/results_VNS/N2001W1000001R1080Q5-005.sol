Capicity Bin: 1000001
Lower Bound: 875

Bins used: 880
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 734982 Color: 2
Size: 135179 Color: 0
Size: 129840 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 736722 Color: 2
Size: 143250 Color: 4
Size: 120029 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 697391 Color: 2
Size: 177584 Color: 1
Size: 125026 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 368326 Color: 4
Size: 327046 Color: 1
Size: 304629 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 719408 Color: 0
Size: 142204 Color: 4
Size: 138389 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 736457 Color: 2
Size: 148423 Color: 4
Size: 115121 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 745506 Color: 2
Size: 154110 Color: 4
Size: 100385 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 450722 Color: 2
Size: 445386 Color: 2
Size: 103893 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 654271 Color: 0
Size: 193850 Color: 3
Size: 151880 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 684266 Color: 3
Size: 181052 Color: 1
Size: 134683 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 786062 Color: 2
Size: 112204 Color: 1
Size: 101735 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 671377 Color: 2
Size: 191625 Color: 4
Size: 136999 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 476374 Color: 0
Size: 389301 Color: 1
Size: 134326 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 525721 Color: 1
Size: 248596 Color: 2
Size: 225684 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 724761 Color: 2
Size: 152462 Color: 0
Size: 122778 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 686936 Color: 1
Size: 194491 Color: 4
Size: 118574 Color: 1

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 672422 Color: 4
Size: 114618 Color: 0
Size: 107958 Color: 2
Size: 105003 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 503102 Color: 1
Size: 274764 Color: 3
Size: 222135 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 709273 Color: 2
Size: 151500 Color: 0
Size: 139228 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 733071 Color: 4
Size: 155066 Color: 4
Size: 111864 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 593320 Color: 3
Size: 300182 Color: 4
Size: 106499 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 348368 Color: 2
Size: 336135 Color: 0
Size: 315498 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 666166 Color: 1
Size: 216971 Color: 1
Size: 116864 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 709007 Color: 2
Size: 152288 Color: 3
Size: 138706 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 670541 Color: 4
Size: 229210 Color: 0
Size: 100250 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 720800 Color: 4
Size: 172954 Color: 0
Size: 106247 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 629450 Color: 4
Size: 229426 Color: 3
Size: 141125 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 656229 Color: 3
Size: 183372 Color: 2
Size: 160400 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 501679 Color: 3
Size: 250271 Color: 2
Size: 248051 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 426554 Color: 2
Size: 421032 Color: 0
Size: 152415 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 755533 Color: 1
Size: 124696 Color: 3
Size: 119772 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 474135 Color: 4
Size: 401393 Color: 3
Size: 124473 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 489506 Color: 3
Size: 342973 Color: 0
Size: 167522 Color: 1

Bin 34: 0 of cap free
Amount of items: 4
Items: 
Size: 369795 Color: 0
Size: 271284 Color: 0
Size: 246713 Color: 2
Size: 112209 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 376857 Color: 3
Size: 361251 Color: 3
Size: 261893 Color: 2

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 550669 Color: 2
Size: 449332 Color: 3

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 703163 Color: 1
Size: 296838 Color: 4

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 729785 Color: 2
Size: 135891 Color: 0
Size: 134324 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 744967 Color: 2
Size: 128928 Color: 4
Size: 126105 Color: 3

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 666172 Color: 2
Size: 221959 Color: 0
Size: 111869 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 772735 Color: 0
Size: 123049 Color: 3
Size: 104216 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 731460 Color: 4
Size: 162373 Color: 0
Size: 106167 Color: 4

Bin 43: 1 of cap free
Amount of items: 4
Items: 
Size: 678410 Color: 4
Size: 110890 Color: 1
Size: 109508 Color: 0
Size: 101192 Color: 2

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 725249 Color: 4
Size: 139508 Color: 1
Size: 135243 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 748213 Color: 2
Size: 136617 Color: 4
Size: 115170 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 748461 Color: 4
Size: 131496 Color: 0
Size: 120043 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 369884 Color: 0
Size: 315194 Color: 2
Size: 314922 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 775295 Color: 1
Size: 118305 Color: 0
Size: 106400 Color: 4

Bin 49: 1 of cap free
Amount of items: 4
Items: 
Size: 480518 Color: 3
Size: 261714 Color: 0
Size: 137230 Color: 4
Size: 120538 Color: 2

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 338325 Color: 4
Size: 332064 Color: 3
Size: 329611 Color: 4

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 607517 Color: 4
Size: 392482 Color: 1

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 621677 Color: 2
Size: 195259 Color: 0
Size: 183063 Color: 4

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 711719 Color: 2
Size: 160809 Color: 3
Size: 127471 Color: 0

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 702803 Color: 0
Size: 157753 Color: 0
Size: 139443 Color: 2

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 735657 Color: 0
Size: 142146 Color: 2
Size: 122196 Color: 3

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 699444 Color: 0
Size: 163194 Color: 4
Size: 137361 Color: 2

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 596294 Color: 4
Size: 216123 Color: 2
Size: 187582 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 594176 Color: 2
Size: 218274 Color: 1
Size: 187549 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 377574 Color: 4
Size: 315126 Color: 3
Size: 307299 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 498050 Color: 0
Size: 383426 Color: 2
Size: 118523 Color: 1

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 663034 Color: 1
Size: 336965 Color: 2

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 722437 Color: 0
Size: 149064 Color: 2
Size: 128498 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 401456 Color: 0
Size: 300044 Color: 4
Size: 298499 Color: 1

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 352942 Color: 1
Size: 327020 Color: 0
Size: 320037 Color: 3

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 691115 Color: 3
Size: 194844 Color: 0
Size: 114040 Color: 1

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 747072 Color: 4
Size: 141963 Color: 0
Size: 110964 Color: 2

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 766541 Color: 4
Size: 124058 Color: 1
Size: 109400 Color: 0

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 448530 Color: 1
Size: 438945 Color: 1
Size: 112524 Color: 4

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 411750 Color: 2
Size: 380900 Color: 1
Size: 207349 Color: 2

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 629328 Color: 3
Size: 370670 Color: 4

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 640033 Color: 2
Size: 180027 Color: 3
Size: 179938 Color: 0

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 641648 Color: 2
Size: 180849 Color: 4
Size: 177501 Color: 1

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 689214 Color: 2
Size: 156552 Color: 1
Size: 154232 Color: 3

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 743979 Color: 2
Size: 150762 Color: 0
Size: 105257 Color: 4

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 723991 Color: 2
Size: 142587 Color: 3
Size: 133420 Color: 2

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 725778 Color: 2
Size: 145918 Color: 4
Size: 128302 Color: 0

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 606640 Color: 2
Size: 206554 Color: 4
Size: 186804 Color: 0

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 638123 Color: 1
Size: 186946 Color: 0
Size: 174929 Color: 4

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 744438 Color: 4
Size: 138529 Color: 2
Size: 117031 Color: 3

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 435538 Color: 1
Size: 430942 Color: 2
Size: 133518 Color: 3

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 649438 Color: 0
Size: 249286 Color: 0
Size: 101274 Color: 3

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 568170 Color: 3
Size: 217106 Color: 2
Size: 214722 Color: 1

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 703755 Color: 3
Size: 151152 Color: 2
Size: 145091 Color: 2

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 599828 Color: 0
Size: 216082 Color: 4
Size: 184088 Color: 0

Bin 85: 3 of cap free
Amount of items: 4
Items: 
Size: 495944 Color: 4
Size: 249673 Color: 3
Size: 131636 Color: 3
Size: 122745 Color: 1

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 676413 Color: 3
Size: 209865 Color: 2
Size: 113720 Color: 1

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 714996 Color: 0
Size: 156030 Color: 3
Size: 128972 Color: 2

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 453143 Color: 2
Size: 421043 Color: 1
Size: 125812 Color: 4

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 495747 Color: 2
Size: 255410 Color: 1
Size: 248840 Color: 3

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 509038 Color: 1
Size: 490959 Color: 3

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 551540 Color: 2
Size: 448457 Color: 0

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 676167 Color: 3
Size: 323830 Color: 4

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 694182 Color: 1
Size: 167042 Color: 3
Size: 138773 Color: 0

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 672970 Color: 2
Size: 192168 Color: 0
Size: 134859 Color: 0

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 452741 Color: 2
Size: 275706 Color: 2
Size: 271550 Color: 3

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 679993 Color: 4
Size: 210089 Color: 4
Size: 109915 Color: 0

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 593167 Color: 4
Size: 218361 Color: 1
Size: 188469 Color: 4

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 604363 Color: 4
Size: 214770 Color: 2
Size: 180864 Color: 1

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 437592 Color: 2
Size: 286472 Color: 4
Size: 275933 Color: 0

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 642100 Color: 2
Size: 181053 Color: 0
Size: 176843 Color: 2

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 464926 Color: 2
Size: 432753 Color: 0
Size: 102317 Color: 3

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 653187 Color: 2
Size: 175077 Color: 3
Size: 171732 Color: 1

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 584668 Color: 3
Size: 217767 Color: 2
Size: 197561 Color: 4

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 514029 Color: 4
Size: 485966 Color: 2

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 619529 Color: 2
Size: 194830 Color: 1
Size: 185636 Color: 4

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 670876 Color: 1
Size: 329119 Color: 0

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 668541 Color: 1
Size: 167120 Color: 4
Size: 164334 Color: 0

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 661756 Color: 2
Size: 170194 Color: 4
Size: 168045 Color: 3

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 632780 Color: 3
Size: 260201 Color: 0
Size: 107014 Color: 2

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 767664 Color: 4
Size: 123027 Color: 4
Size: 109303 Color: 0

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 585934 Color: 1
Size: 219582 Color: 0
Size: 194478 Color: 4

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 373501 Color: 1
Size: 370370 Color: 0
Size: 256123 Color: 3

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 556761 Color: 3
Size: 443232 Color: 1

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 562754 Color: 3
Size: 437239 Color: 2

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 643521 Color: 2
Size: 356472 Color: 1

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 645799 Color: 2
Size: 179166 Color: 0
Size: 175028 Color: 3

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 683483 Color: 0
Size: 316510 Color: 1

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 700092 Color: 0
Size: 299901 Color: 2

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 766033 Color: 1
Size: 123017 Color: 2
Size: 110943 Color: 3

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 715150 Color: 4
Size: 157549 Color: 3
Size: 127294 Color: 1

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 448617 Color: 4
Size: 282358 Color: 1
Size: 269018 Color: 1

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 662812 Color: 4
Size: 337181 Color: 1

Bin 123: 8 of cap free
Amount of items: 3
Items: 
Size: 756033 Color: 0
Size: 132614 Color: 4
Size: 111346 Color: 4

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 605250 Color: 2
Size: 205170 Color: 0
Size: 189572 Color: 1

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 688800 Color: 3
Size: 311192 Color: 0

Bin 126: 9 of cap free
Amount of items: 3
Items: 
Size: 791029 Color: 3
Size: 105536 Color: 3
Size: 103427 Color: 4

Bin 127: 9 of cap free
Amount of items: 3
Items: 
Size: 643574 Color: 4
Size: 178264 Color: 1
Size: 178154 Color: 2

Bin 128: 9 of cap free
Amount of items: 3
Items: 
Size: 373353 Color: 4
Size: 316182 Color: 3
Size: 310457 Color: 0

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 550459 Color: 1
Size: 449532 Color: 2

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 601050 Color: 2
Size: 211965 Color: 1
Size: 186976 Color: 4

Bin 131: 10 of cap free
Amount of items: 3
Items: 
Size: 750594 Color: 0
Size: 132989 Color: 1
Size: 116408 Color: 4

Bin 132: 10 of cap free
Amount of items: 3
Items: 
Size: 429350 Color: 4
Size: 288336 Color: 0
Size: 282305 Color: 1

Bin 133: 10 of cap free
Amount of items: 2
Items: 
Size: 720395 Color: 0
Size: 279596 Color: 2

Bin 134: 11 of cap free
Amount of items: 3
Items: 
Size: 757719 Color: 2
Size: 134845 Color: 1
Size: 107426 Color: 4

Bin 135: 11 of cap free
Amount of items: 3
Items: 
Size: 381384 Color: 2
Size: 313492 Color: 2
Size: 305114 Color: 4

Bin 136: 11 of cap free
Amount of items: 3
Items: 
Size: 485857 Color: 1
Size: 387652 Color: 0
Size: 126481 Color: 3

Bin 137: 11 of cap free
Amount of items: 2
Items: 
Size: 612253 Color: 4
Size: 387737 Color: 2

Bin 138: 11 of cap free
Amount of items: 3
Items: 
Size: 377367 Color: 3
Size: 345444 Color: 0
Size: 277179 Color: 0

Bin 139: 12 of cap free
Amount of items: 2
Items: 
Size: 705752 Color: 3
Size: 294237 Color: 4

Bin 140: 13 of cap free
Amount of items: 3
Items: 
Size: 612688 Color: 2
Size: 206452 Color: 3
Size: 180848 Color: 3

Bin 141: 13 of cap free
Amount of items: 2
Items: 
Size: 616199 Color: 3
Size: 383789 Color: 1

Bin 142: 13 of cap free
Amount of items: 3
Items: 
Size: 680113 Color: 2
Size: 163950 Color: 0
Size: 155925 Color: 0

Bin 143: 13 of cap free
Amount of items: 3
Items: 
Size: 742519 Color: 4
Size: 146482 Color: 3
Size: 110987 Color: 1

Bin 144: 13 of cap free
Amount of items: 3
Items: 
Size: 421077 Color: 0
Size: 302073 Color: 2
Size: 276838 Color: 1

Bin 145: 13 of cap free
Amount of items: 3
Items: 
Size: 656382 Color: 0
Size: 172733 Color: 2
Size: 170873 Color: 1

Bin 146: 13 of cap free
Amount of items: 3
Items: 
Size: 338770 Color: 1
Size: 331133 Color: 4
Size: 330085 Color: 4

Bin 147: 14 of cap free
Amount of items: 3
Items: 
Size: 461102 Color: 1
Size: 271810 Color: 4
Size: 267075 Color: 4

Bin 148: 14 of cap free
Amount of items: 3
Items: 
Size: 733680 Color: 1
Size: 141637 Color: 2
Size: 124670 Color: 4

Bin 149: 14 of cap free
Amount of items: 3
Items: 
Size: 684489 Color: 2
Size: 171112 Color: 0
Size: 144386 Color: 0

Bin 150: 14 of cap free
Amount of items: 3
Items: 
Size: 596176 Color: 4
Size: 215149 Color: 4
Size: 188662 Color: 1

Bin 151: 14 of cap free
Amount of items: 3
Items: 
Size: 759270 Color: 4
Size: 126760 Color: 2
Size: 113957 Color: 4

Bin 152: 15 of cap free
Amount of items: 2
Items: 
Size: 655311 Color: 0
Size: 344675 Color: 1

Bin 153: 15 of cap free
Amount of items: 3
Items: 
Size: 729470 Color: 3
Size: 137084 Color: 0
Size: 133432 Color: 3

Bin 154: 15 of cap free
Amount of items: 3
Items: 
Size: 703212 Color: 4
Size: 183671 Color: 4
Size: 113103 Color: 1

Bin 155: 15 of cap free
Amount of items: 4
Items: 
Size: 407063 Color: 3
Size: 292973 Color: 0
Size: 192674 Color: 4
Size: 107276 Color: 1

Bin 156: 15 of cap free
Amount of items: 3
Items: 
Size: 390047 Color: 2
Size: 352755 Color: 4
Size: 257184 Color: 0

Bin 157: 16 of cap free
Amount of items: 3
Items: 
Size: 451947 Color: 1
Size: 286699 Color: 4
Size: 261339 Color: 0

Bin 158: 16 of cap free
Amount of items: 2
Items: 
Size: 770542 Color: 2
Size: 229443 Color: 4

Bin 159: 17 of cap free
Amount of items: 3
Items: 
Size: 719884 Color: 1
Size: 141598 Color: 1
Size: 138502 Color: 2

Bin 160: 17 of cap free
Amount of items: 2
Items: 
Size: 525389 Color: 2
Size: 474595 Color: 3

Bin 161: 17 of cap free
Amount of items: 2
Items: 
Size: 710499 Color: 3
Size: 289485 Color: 0

Bin 162: 17 of cap free
Amount of items: 3
Items: 
Size: 367620 Color: 1
Size: 334591 Color: 3
Size: 297773 Color: 4

Bin 163: 18 of cap free
Amount of items: 3
Items: 
Size: 771081 Color: 3
Size: 115997 Color: 3
Size: 112905 Color: 0

Bin 164: 18 of cap free
Amount of items: 3
Items: 
Size: 728346 Color: 1
Size: 138962 Color: 3
Size: 132675 Color: 4

Bin 165: 18 of cap free
Amount of items: 3
Items: 
Size: 406239 Color: 1
Size: 386645 Color: 1
Size: 207099 Color: 2

Bin 166: 19 of cap free
Amount of items: 2
Items: 
Size: 574448 Color: 1
Size: 425534 Color: 4

Bin 167: 19 of cap free
Amount of items: 3
Items: 
Size: 602325 Color: 2
Size: 213571 Color: 1
Size: 184086 Color: 1

Bin 168: 19 of cap free
Amount of items: 3
Items: 
Size: 593822 Color: 1
Size: 216889 Color: 0
Size: 189271 Color: 1

Bin 169: 20 of cap free
Amount of items: 2
Items: 
Size: 773325 Color: 1
Size: 226656 Color: 4

Bin 170: 20 of cap free
Amount of items: 3
Items: 
Size: 702518 Color: 4
Size: 152169 Color: 3
Size: 145294 Color: 2

Bin 171: 20 of cap free
Amount of items: 2
Items: 
Size: 537543 Color: 4
Size: 462438 Color: 1

Bin 172: 20 of cap free
Amount of items: 3
Items: 
Size: 726680 Color: 0
Size: 142729 Color: 3
Size: 130572 Color: 0

Bin 173: 20 of cap free
Amount of items: 3
Items: 
Size: 453769 Color: 2
Size: 383002 Color: 3
Size: 163210 Color: 4

Bin 174: 21 of cap free
Amount of items: 3
Items: 
Size: 620905 Color: 2
Size: 192535 Color: 3
Size: 186540 Color: 3

Bin 175: 21 of cap free
Amount of items: 3
Items: 
Size: 488580 Color: 3
Size: 258073 Color: 1
Size: 253327 Color: 0

Bin 176: 21 of cap free
Amount of items: 3
Items: 
Size: 708031 Color: 3
Size: 179046 Color: 3
Size: 112903 Color: 4

Bin 177: 21 of cap free
Amount of items: 2
Items: 
Size: 542473 Color: 1
Size: 457507 Color: 3

Bin 178: 21 of cap free
Amount of items: 2
Items: 
Size: 561480 Color: 3
Size: 438500 Color: 1

Bin 179: 22 of cap free
Amount of items: 2
Items: 
Size: 605297 Color: 3
Size: 394682 Color: 1

Bin 180: 22 of cap free
Amount of items: 2
Items: 
Size: 661621 Color: 4
Size: 338358 Color: 1

Bin 181: 23 of cap free
Amount of items: 2
Items: 
Size: 563728 Color: 1
Size: 436250 Color: 4

Bin 182: 23 of cap free
Amount of items: 2
Items: 
Size: 620086 Color: 2
Size: 379892 Color: 0

Bin 183: 23 of cap free
Amount of items: 2
Items: 
Size: 574547 Color: 4
Size: 425431 Color: 3

Bin 184: 23 of cap free
Amount of items: 3
Items: 
Size: 427672 Color: 3
Size: 377426 Color: 3
Size: 194880 Color: 0

Bin 185: 24 of cap free
Amount of items: 3
Items: 
Size: 668325 Color: 2
Size: 166553 Color: 3
Size: 165099 Color: 0

Bin 186: 24 of cap free
Amount of items: 2
Items: 
Size: 722641 Color: 3
Size: 277336 Color: 0

Bin 187: 24 of cap free
Amount of items: 3
Items: 
Size: 449202 Color: 2
Size: 448636 Color: 0
Size: 102139 Color: 1

Bin 188: 24 of cap free
Amount of items: 3
Items: 
Size: 370445 Color: 0
Size: 316171 Color: 2
Size: 313361 Color: 3

Bin 189: 25 of cap free
Amount of items: 3
Items: 
Size: 352151 Color: 2
Size: 334740 Color: 1
Size: 313085 Color: 3

Bin 190: 26 of cap free
Amount of items: 3
Items: 
Size: 616298 Color: 2
Size: 193707 Color: 1
Size: 189970 Color: 3

Bin 191: 26 of cap free
Amount of items: 2
Items: 
Size: 755642 Color: 1
Size: 244333 Color: 4

Bin 192: 26 of cap free
Amount of items: 2
Items: 
Size: 625467 Color: 3
Size: 374508 Color: 4

Bin 193: 26 of cap free
Amount of items: 3
Items: 
Size: 581358 Color: 2
Size: 217782 Color: 2
Size: 200835 Color: 0

Bin 194: 26 of cap free
Amount of items: 2
Items: 
Size: 772878 Color: 3
Size: 227097 Color: 2

Bin 195: 26 of cap free
Amount of items: 3
Items: 
Size: 373482 Color: 1
Size: 369377 Color: 4
Size: 257116 Color: 0

Bin 196: 27 of cap free
Amount of items: 3
Items: 
Size: 444092 Color: 1
Size: 401758 Color: 4
Size: 154124 Color: 1

Bin 197: 28 of cap free
Amount of items: 3
Items: 
Size: 374217 Color: 1
Size: 315352 Color: 1
Size: 310404 Color: 3

Bin 198: 28 of cap free
Amount of items: 3
Items: 
Size: 713472 Color: 2
Size: 154967 Color: 0
Size: 131534 Color: 2

Bin 199: 29 of cap free
Amount of items: 2
Items: 
Size: 699807 Color: 4
Size: 300165 Color: 3

Bin 200: 29 of cap free
Amount of items: 2
Items: 
Size: 714664 Color: 3
Size: 285308 Color: 0

Bin 201: 30 of cap free
Amount of items: 3
Items: 
Size: 341783 Color: 0
Size: 330227 Color: 4
Size: 327961 Color: 4

Bin 202: 30 of cap free
Amount of items: 3
Items: 
Size: 429424 Color: 4
Size: 298279 Color: 0
Size: 272268 Color: 2

Bin 203: 30 of cap free
Amount of items: 2
Items: 
Size: 671798 Color: 1
Size: 328173 Color: 3

Bin 204: 30 of cap free
Amount of items: 3
Items: 
Size: 675650 Color: 4
Size: 203557 Color: 3
Size: 120764 Color: 4

Bin 205: 31 of cap free
Amount of items: 3
Items: 
Size: 638722 Color: 2
Size: 214666 Color: 2
Size: 146582 Color: 0

Bin 206: 31 of cap free
Amount of items: 3
Items: 
Size: 341487 Color: 3
Size: 336295 Color: 2
Size: 322188 Color: 4

Bin 207: 32 of cap free
Amount of items: 3
Items: 
Size: 663595 Color: 3
Size: 169514 Color: 4
Size: 166860 Color: 3

Bin 208: 32 of cap free
Amount of items: 3
Items: 
Size: 604655 Color: 1
Size: 212097 Color: 3
Size: 183217 Color: 0

Bin 209: 33 of cap free
Amount of items: 2
Items: 
Size: 551663 Color: 1
Size: 448305 Color: 2

Bin 210: 33 of cap free
Amount of items: 3
Items: 
Size: 600461 Color: 1
Size: 211413 Color: 0
Size: 188094 Color: 1

Bin 211: 33 of cap free
Amount of items: 3
Items: 
Size: 594051 Color: 1
Size: 216748 Color: 3
Size: 189169 Color: 4

Bin 212: 34 of cap free
Amount of items: 3
Items: 
Size: 700894 Color: 2
Size: 152940 Color: 3
Size: 146133 Color: 0

Bin 213: 34 of cap free
Amount of items: 3
Items: 
Size: 730840 Color: 2
Size: 149196 Color: 1
Size: 119931 Color: 1

Bin 214: 35 of cap free
Amount of items: 2
Items: 
Size: 586630 Color: 2
Size: 413336 Color: 3

Bin 215: 37 of cap free
Amount of items: 2
Items: 
Size: 680480 Color: 1
Size: 319484 Color: 2

Bin 216: 38 of cap free
Amount of items: 3
Items: 
Size: 372176 Color: 4
Size: 320300 Color: 4
Size: 307487 Color: 0

Bin 217: 38 of cap free
Amount of items: 3
Items: 
Size: 351411 Color: 4
Size: 333637 Color: 0
Size: 314915 Color: 3

Bin 218: 38 of cap free
Amount of items: 3
Items: 
Size: 426849 Color: 2
Size: 301995 Color: 1
Size: 271119 Color: 1

Bin 219: 38 of cap free
Amount of items: 2
Items: 
Size: 573829 Color: 3
Size: 426134 Color: 0

Bin 220: 39 of cap free
Amount of items: 2
Items: 
Size: 681069 Color: 4
Size: 318893 Color: 3

Bin 221: 40 of cap free
Amount of items: 2
Items: 
Size: 577351 Color: 1
Size: 422610 Color: 0

Bin 222: 40 of cap free
Amount of items: 2
Items: 
Size: 639463 Color: 1
Size: 360498 Color: 0

Bin 223: 40 of cap free
Amount of items: 2
Items: 
Size: 602973 Color: 0
Size: 396988 Color: 2

Bin 224: 40 of cap free
Amount of items: 3
Items: 
Size: 724157 Color: 1
Size: 144856 Color: 1
Size: 130948 Color: 0

Bin 225: 40 of cap free
Amount of items: 2
Items: 
Size: 549565 Color: 0
Size: 450396 Color: 1

Bin 226: 41 of cap free
Amount of items: 3
Items: 
Size: 452486 Color: 0
Size: 322241 Color: 1
Size: 225233 Color: 0

Bin 227: 41 of cap free
Amount of items: 3
Items: 
Size: 414060 Color: 2
Size: 298193 Color: 3
Size: 287707 Color: 0

Bin 228: 42 of cap free
Amount of items: 3
Items: 
Size: 447061 Color: 0
Size: 282174 Color: 1
Size: 270724 Color: 2

Bin 229: 42 of cap free
Amount of items: 2
Items: 
Size: 795885 Color: 3
Size: 204074 Color: 4

Bin 230: 43 of cap free
Amount of items: 3
Items: 
Size: 650762 Color: 0
Size: 175417 Color: 3
Size: 173779 Color: 0

Bin 231: 43 of cap free
Amount of items: 2
Items: 
Size: 707927 Color: 1
Size: 292031 Color: 0

Bin 232: 43 of cap free
Amount of items: 3
Items: 
Size: 607171 Color: 1
Size: 211709 Color: 2
Size: 181078 Color: 3

Bin 233: 43 of cap free
Amount of items: 2
Items: 
Size: 618008 Color: 3
Size: 381950 Color: 0

Bin 234: 43 of cap free
Amount of items: 3
Items: 
Size: 386381 Color: 2
Size: 344149 Color: 3
Size: 269428 Color: 2

Bin 235: 44 of cap free
Amount of items: 3
Items: 
Size: 410202 Color: 1
Size: 374148 Color: 4
Size: 215607 Color: 3

Bin 236: 45 of cap free
Amount of items: 3
Items: 
Size: 681314 Color: 2
Size: 159371 Color: 3
Size: 159271 Color: 4

Bin 237: 45 of cap free
Amount of items: 3
Items: 
Size: 579813 Color: 1
Size: 225785 Color: 2
Size: 194358 Color: 2

Bin 238: 45 of cap free
Amount of items: 3
Items: 
Size: 427559 Color: 2
Size: 300878 Color: 4
Size: 271519 Color: 2

Bin 239: 45 of cap free
Amount of items: 3
Items: 
Size: 426720 Color: 0
Size: 382872 Color: 4
Size: 190364 Color: 3

Bin 240: 47 of cap free
Amount of items: 3
Items: 
Size: 607544 Color: 1
Size: 210852 Color: 4
Size: 181558 Color: 2

Bin 241: 48 of cap free
Amount of items: 2
Items: 
Size: 632406 Color: 3
Size: 367547 Color: 2

Bin 242: 48 of cap free
Amount of items: 2
Items: 
Size: 723551 Color: 4
Size: 276402 Color: 1

Bin 243: 48 of cap free
Amount of items: 2
Items: 
Size: 740232 Color: 3
Size: 259721 Color: 2

Bin 244: 49 of cap free
Amount of items: 2
Items: 
Size: 708109 Color: 0
Size: 291843 Color: 1

Bin 245: 50 of cap free
Amount of items: 2
Items: 
Size: 607951 Color: 4
Size: 392000 Color: 1

Bin 246: 52 of cap free
Amount of items: 2
Items: 
Size: 593263 Color: 3
Size: 406686 Color: 4

Bin 247: 52 of cap free
Amount of items: 2
Items: 
Size: 747379 Color: 0
Size: 252570 Color: 4

Bin 248: 52 of cap free
Amount of items: 3
Items: 
Size: 369894 Color: 2
Size: 316311 Color: 1
Size: 313744 Color: 0

Bin 249: 52 of cap free
Amount of items: 2
Items: 
Size: 727547 Color: 0
Size: 272402 Color: 2

Bin 250: 52 of cap free
Amount of items: 2
Items: 
Size: 531973 Color: 2
Size: 467976 Color: 4

Bin 251: 53 of cap free
Amount of items: 3
Items: 
Size: 753959 Color: 1
Size: 139290 Color: 3
Size: 106699 Color: 3

Bin 252: 54 of cap free
Amount of items: 2
Items: 
Size: 715083 Color: 0
Size: 284864 Color: 4

Bin 253: 54 of cap free
Amount of items: 3
Items: 
Size: 449463 Color: 1
Size: 402014 Color: 3
Size: 148470 Color: 0

Bin 254: 55 of cap free
Amount of items: 3
Items: 
Size: 622547 Color: 2
Size: 190499 Color: 0
Size: 186900 Color: 2

Bin 255: 55 of cap free
Amount of items: 2
Items: 
Size: 515399 Color: 4
Size: 484547 Color: 0

Bin 256: 55 of cap free
Amount of items: 2
Items: 
Size: 655562 Color: 2
Size: 344384 Color: 4

Bin 257: 55 of cap free
Amount of items: 3
Items: 
Size: 378802 Color: 1
Size: 310626 Color: 0
Size: 310518 Color: 2

Bin 258: 55 of cap free
Amount of items: 2
Items: 
Size: 609248 Color: 2
Size: 390698 Color: 3

Bin 259: 56 of cap free
Amount of items: 2
Items: 
Size: 577049 Color: 0
Size: 422896 Color: 1

Bin 260: 56 of cap free
Amount of items: 2
Items: 
Size: 768677 Color: 3
Size: 231268 Color: 2

Bin 261: 57 of cap free
Amount of items: 3
Items: 
Size: 633887 Color: 4
Size: 186115 Color: 4
Size: 179942 Color: 1

Bin 262: 58 of cap free
Amount of items: 3
Items: 
Size: 726053 Color: 2
Size: 146561 Color: 0
Size: 127329 Color: 3

Bin 263: 58 of cap free
Amount of items: 2
Items: 
Size: 622084 Color: 1
Size: 377859 Color: 3

Bin 264: 59 of cap free
Amount of items: 3
Items: 
Size: 607386 Color: 4
Size: 206738 Color: 3
Size: 185818 Color: 4

Bin 265: 59 of cap free
Amount of items: 2
Items: 
Size: 777790 Color: 4
Size: 222152 Color: 1

Bin 266: 59 of cap free
Amount of items: 3
Items: 
Size: 339657 Color: 4
Size: 332641 Color: 4
Size: 327644 Color: 2

Bin 267: 59 of cap free
Amount of items: 2
Items: 
Size: 724478 Color: 2
Size: 275464 Color: 0

Bin 268: 60 of cap free
Amount of items: 2
Items: 
Size: 683065 Color: 3
Size: 316876 Color: 0

Bin 269: 62 of cap free
Amount of items: 2
Items: 
Size: 576968 Color: 4
Size: 422971 Color: 2

Bin 270: 62 of cap free
Amount of items: 2
Items: 
Size: 632255 Color: 3
Size: 367684 Color: 0

Bin 271: 63 of cap free
Amount of items: 3
Items: 
Size: 376799 Color: 2
Size: 334270 Color: 1
Size: 288869 Color: 2

Bin 272: 64 of cap free
Amount of items: 2
Items: 
Size: 662414 Color: 3
Size: 337523 Color: 2

Bin 273: 65 of cap free
Amount of items: 2
Items: 
Size: 513678 Color: 0
Size: 486258 Color: 2

Bin 274: 65 of cap free
Amount of items: 2
Items: 
Size: 561199 Color: 3
Size: 438737 Color: 1

Bin 275: 65 of cap free
Amount of items: 2
Items: 
Size: 628210 Color: 4
Size: 371726 Color: 1

Bin 276: 66 of cap free
Amount of items: 2
Items: 
Size: 610096 Color: 1
Size: 389839 Color: 3

Bin 277: 66 of cap free
Amount of items: 3
Items: 
Size: 637738 Color: 1
Size: 184067 Color: 3
Size: 178130 Color: 3

Bin 278: 67 of cap free
Amount of items: 2
Items: 
Size: 752211 Color: 4
Size: 247723 Color: 3

Bin 279: 67 of cap free
Amount of items: 3
Items: 
Size: 447827 Color: 1
Size: 405072 Color: 3
Size: 147035 Color: 2

Bin 280: 68 of cap free
Amount of items: 2
Items: 
Size: 728742 Color: 4
Size: 271191 Color: 1

Bin 281: 68 of cap free
Amount of items: 2
Items: 
Size: 700434 Color: 2
Size: 299499 Color: 4

Bin 282: 68 of cap free
Amount of items: 3
Items: 
Size: 459719 Color: 1
Size: 273536 Color: 0
Size: 266678 Color: 1

Bin 283: 69 of cap free
Amount of items: 3
Items: 
Size: 487181 Color: 2
Size: 258557 Color: 4
Size: 254194 Color: 2

Bin 284: 70 of cap free
Amount of items: 2
Items: 
Size: 533308 Color: 2
Size: 466623 Color: 3

Bin 285: 70 of cap free
Amount of items: 3
Items: 
Size: 670461 Color: 1
Size: 176918 Color: 4
Size: 152552 Color: 0

Bin 286: 70 of cap free
Amount of items: 3
Items: 
Size: 639856 Color: 1
Size: 181767 Color: 0
Size: 178308 Color: 0

Bin 287: 70 of cap free
Amount of items: 3
Items: 
Size: 661767 Color: 3
Size: 169152 Color: 1
Size: 169012 Color: 1

Bin 288: 70 of cap free
Amount of items: 2
Items: 
Size: 501060 Color: 3
Size: 498871 Color: 0

Bin 289: 71 of cap free
Amount of items: 2
Items: 
Size: 659942 Color: 4
Size: 339988 Color: 3

Bin 290: 71 of cap free
Amount of items: 2
Items: 
Size: 653691 Color: 1
Size: 346239 Color: 2

Bin 291: 72 of cap free
Amount of items: 2
Items: 
Size: 606278 Color: 3
Size: 393651 Color: 4

Bin 292: 72 of cap free
Amount of items: 2
Items: 
Size: 589231 Color: 2
Size: 410698 Color: 3

Bin 293: 73 of cap free
Amount of items: 2
Items: 
Size: 657969 Color: 2
Size: 341959 Color: 0

Bin 294: 73 of cap free
Amount of items: 2
Items: 
Size: 754250 Color: 1
Size: 245678 Color: 3

Bin 295: 74 of cap free
Amount of items: 2
Items: 
Size: 672550 Color: 3
Size: 327377 Color: 4

Bin 296: 75 of cap free
Amount of items: 3
Items: 
Size: 649818 Color: 0
Size: 175710 Color: 2
Size: 174398 Color: 4

Bin 297: 75 of cap free
Amount of items: 2
Items: 
Size: 691848 Color: 1
Size: 308078 Color: 3

Bin 298: 76 of cap free
Amount of items: 2
Items: 
Size: 532979 Color: 0
Size: 466946 Color: 4

Bin 299: 76 of cap free
Amount of items: 2
Items: 
Size: 659264 Color: 2
Size: 340661 Color: 3

Bin 300: 76 of cap free
Amount of items: 3
Items: 
Size: 724773 Color: 1
Size: 144036 Color: 4
Size: 131116 Color: 3

Bin 301: 76 of cap free
Amount of items: 3
Items: 
Size: 378415 Color: 2
Size: 359719 Color: 3
Size: 261791 Color: 1

Bin 302: 77 of cap free
Amount of items: 3
Items: 
Size: 599558 Color: 0
Size: 212678 Color: 4
Size: 187688 Color: 0

Bin 303: 78 of cap free
Amount of items: 2
Items: 
Size: 501760 Color: 4
Size: 498163 Color: 2

Bin 304: 78 of cap free
Amount of items: 2
Items: 
Size: 594620 Color: 2
Size: 405303 Color: 1

Bin 305: 78 of cap free
Amount of items: 2
Items: 
Size: 636584 Color: 3
Size: 363339 Color: 1

Bin 306: 80 of cap free
Amount of items: 2
Items: 
Size: 774978 Color: 3
Size: 224943 Color: 2

Bin 307: 81 of cap free
Amount of items: 2
Items: 
Size: 734870 Color: 0
Size: 265050 Color: 4

Bin 308: 81 of cap free
Amount of items: 3
Items: 
Size: 343539 Color: 4
Size: 328748 Color: 1
Size: 327633 Color: 4

Bin 309: 82 of cap free
Amount of items: 2
Items: 
Size: 509720 Color: 2
Size: 490199 Color: 3

Bin 310: 82 of cap free
Amount of items: 2
Items: 
Size: 768536 Color: 1
Size: 231383 Color: 4

Bin 311: 82 of cap free
Amount of items: 2
Items: 
Size: 755052 Color: 1
Size: 244867 Color: 3

Bin 312: 82 of cap free
Amount of items: 2
Items: 
Size: 648311 Color: 1
Size: 351608 Color: 3

Bin 313: 82 of cap free
Amount of items: 2
Items: 
Size: 560469 Color: 3
Size: 439450 Color: 1

Bin 314: 83 of cap free
Amount of items: 2
Items: 
Size: 639729 Color: 0
Size: 360189 Color: 3

Bin 315: 83 of cap free
Amount of items: 3
Items: 
Size: 570432 Color: 2
Size: 217729 Color: 4
Size: 211757 Color: 1

Bin 316: 83 of cap free
Amount of items: 3
Items: 
Size: 594030 Color: 4
Size: 214948 Color: 0
Size: 190940 Color: 3

Bin 317: 84 of cap free
Amount of items: 2
Items: 
Size: 539818 Color: 3
Size: 460099 Color: 4

Bin 318: 84 of cap free
Amount of items: 2
Items: 
Size: 676727 Color: 3
Size: 323190 Color: 2

Bin 319: 85 of cap free
Amount of items: 2
Items: 
Size: 591075 Color: 3
Size: 408841 Color: 4

Bin 320: 85 of cap free
Amount of items: 2
Items: 
Size: 503398 Color: 0
Size: 496518 Color: 2

Bin 321: 86 of cap free
Amount of items: 2
Items: 
Size: 583364 Color: 4
Size: 416551 Color: 2

Bin 322: 87 of cap free
Amount of items: 2
Items: 
Size: 568776 Color: 0
Size: 431138 Color: 3

Bin 323: 87 of cap free
Amount of items: 2
Items: 
Size: 598638 Color: 3
Size: 401276 Color: 4

Bin 324: 87 of cap free
Amount of items: 2
Items: 
Size: 669949 Color: 2
Size: 329965 Color: 4

Bin 325: 88 of cap free
Amount of items: 3
Items: 
Size: 614247 Color: 3
Size: 204274 Color: 2
Size: 181392 Color: 1

Bin 326: 89 of cap free
Amount of items: 3
Items: 
Size: 447356 Color: 3
Size: 276450 Color: 4
Size: 276106 Color: 4

Bin 327: 90 of cap free
Amount of items: 2
Items: 
Size: 726671 Color: 1
Size: 273240 Color: 0

Bin 328: 91 of cap free
Amount of items: 2
Items: 
Size: 582024 Color: 2
Size: 417886 Color: 4

Bin 329: 93 of cap free
Amount of items: 3
Items: 
Size: 662143 Color: 4
Size: 175084 Color: 2
Size: 162681 Color: 1

Bin 330: 94 of cap free
Amount of items: 2
Items: 
Size: 563665 Color: 3
Size: 436242 Color: 0

Bin 331: 94 of cap free
Amount of items: 2
Items: 
Size: 612130 Color: 4
Size: 387777 Color: 2

Bin 332: 94 of cap free
Amount of items: 2
Items: 
Size: 645597 Color: 0
Size: 354310 Color: 4

Bin 333: 94 of cap free
Amount of items: 2
Items: 
Size: 566613 Color: 1
Size: 433294 Color: 0

Bin 334: 95 of cap free
Amount of items: 2
Items: 
Size: 637814 Color: 0
Size: 362092 Color: 4

Bin 335: 95 of cap free
Amount of items: 2
Items: 
Size: 668091 Color: 2
Size: 331815 Color: 0

Bin 336: 96 of cap free
Amount of items: 2
Items: 
Size: 528022 Color: 2
Size: 471883 Color: 3

Bin 337: 96 of cap free
Amount of items: 2
Items: 
Size: 577136 Color: 4
Size: 422769 Color: 2

Bin 338: 97 of cap free
Amount of items: 2
Items: 
Size: 506410 Color: 4
Size: 493494 Color: 1

Bin 339: 97 of cap free
Amount of items: 2
Items: 
Size: 567256 Color: 3
Size: 432648 Color: 1

Bin 340: 98 of cap free
Amount of items: 2
Items: 
Size: 702921 Color: 4
Size: 296982 Color: 2

Bin 341: 99 of cap free
Amount of items: 2
Items: 
Size: 742470 Color: 1
Size: 257432 Color: 4

Bin 342: 99 of cap free
Amount of items: 2
Items: 
Size: 783447 Color: 1
Size: 216455 Color: 0

Bin 343: 99 of cap free
Amount of items: 2
Items: 
Size: 636276 Color: 2
Size: 363626 Color: 0

Bin 344: 99 of cap free
Amount of items: 2
Items: 
Size: 570238 Color: 2
Size: 429664 Color: 4

Bin 345: 100 of cap free
Amount of items: 2
Items: 
Size: 535840 Color: 1
Size: 464061 Color: 2

Bin 346: 101 of cap free
Amount of items: 2
Items: 
Size: 669614 Color: 4
Size: 330286 Color: 1

Bin 347: 104 of cap free
Amount of items: 3
Items: 
Size: 656736 Color: 1
Size: 172407 Color: 2
Size: 170754 Color: 3

Bin 348: 105 of cap free
Amount of items: 2
Items: 
Size: 676045 Color: 4
Size: 323851 Color: 0

Bin 349: 105 of cap free
Amount of items: 2
Items: 
Size: 587133 Color: 3
Size: 412763 Color: 2

Bin 350: 107 of cap free
Amount of items: 3
Items: 
Size: 447349 Color: 2
Size: 391563 Color: 1
Size: 160982 Color: 1

Bin 351: 108 of cap free
Amount of items: 2
Items: 
Size: 769777 Color: 4
Size: 230116 Color: 0

Bin 352: 109 of cap free
Amount of items: 2
Items: 
Size: 570479 Color: 1
Size: 429413 Color: 2

Bin 353: 110 of cap free
Amount of items: 2
Items: 
Size: 522288 Color: 1
Size: 477603 Color: 0

Bin 354: 110 of cap free
Amount of items: 2
Items: 
Size: 523181 Color: 1
Size: 476710 Color: 3

Bin 355: 111 of cap free
Amount of items: 2
Items: 
Size: 514352 Color: 1
Size: 485538 Color: 2

Bin 356: 111 of cap free
Amount of items: 2
Items: 
Size: 627648 Color: 0
Size: 372242 Color: 4

Bin 357: 113 of cap free
Amount of items: 2
Items: 
Size: 727504 Color: 0
Size: 272384 Color: 4

Bin 358: 113 of cap free
Amount of items: 3
Items: 
Size: 377402 Color: 3
Size: 314602 Color: 1
Size: 307884 Color: 4

Bin 359: 114 of cap free
Amount of items: 2
Items: 
Size: 794468 Color: 1
Size: 205419 Color: 2

Bin 360: 115 of cap free
Amount of items: 2
Items: 
Size: 765134 Color: 2
Size: 234752 Color: 4

Bin 361: 116 of cap free
Amount of items: 3
Items: 
Size: 457232 Color: 1
Size: 276592 Color: 1
Size: 266061 Color: 2

Bin 362: 116 of cap free
Amount of items: 3
Items: 
Size: 433139 Color: 1
Size: 384714 Color: 3
Size: 182032 Color: 3

Bin 363: 118 of cap free
Amount of items: 3
Items: 
Size: 451206 Color: 2
Size: 287126 Color: 3
Size: 261551 Color: 2

Bin 364: 118 of cap free
Amount of items: 2
Items: 
Size: 523376 Color: 1
Size: 476507 Color: 4

Bin 365: 118 of cap free
Amount of items: 2
Items: 
Size: 628968 Color: 3
Size: 370915 Color: 1

Bin 366: 119 of cap free
Amount of items: 2
Items: 
Size: 557785 Color: 0
Size: 442097 Color: 3

Bin 367: 119 of cap free
Amount of items: 2
Items: 
Size: 569957 Color: 4
Size: 429925 Color: 0

Bin 368: 119 of cap free
Amount of items: 2
Items: 
Size: 756219 Color: 0
Size: 243663 Color: 3

Bin 369: 121 of cap free
Amount of items: 2
Items: 
Size: 776928 Color: 3
Size: 222952 Color: 4

Bin 370: 122 of cap free
Amount of items: 2
Items: 
Size: 595390 Color: 3
Size: 404489 Color: 1

Bin 371: 122 of cap free
Amount of items: 3
Items: 
Size: 701838 Color: 2
Size: 156095 Color: 1
Size: 141946 Color: 0

Bin 372: 122 of cap free
Amount of items: 2
Items: 
Size: 687579 Color: 1
Size: 312300 Color: 0

Bin 373: 123 of cap free
Amount of items: 2
Items: 
Size: 790131 Color: 0
Size: 209747 Color: 2

Bin 374: 123 of cap free
Amount of items: 3
Items: 
Size: 446416 Color: 4
Size: 402623 Color: 4
Size: 150839 Color: 2

Bin 375: 124 of cap free
Amount of items: 2
Items: 
Size: 685680 Color: 3
Size: 314197 Color: 1

Bin 376: 126 of cap free
Amount of items: 2
Items: 
Size: 566882 Color: 4
Size: 432993 Color: 1

Bin 377: 126 of cap free
Amount of items: 2
Items: 
Size: 724456 Color: 1
Size: 275419 Color: 2

Bin 378: 127 of cap free
Amount of items: 2
Items: 
Size: 530353 Color: 2
Size: 469521 Color: 3

Bin 379: 127 of cap free
Amount of items: 2
Items: 
Size: 748354 Color: 2
Size: 251520 Color: 4

Bin 380: 129 of cap free
Amount of items: 2
Items: 
Size: 526783 Color: 2
Size: 473089 Color: 0

Bin 381: 133 of cap free
Amount of items: 2
Items: 
Size: 683364 Color: 1
Size: 316504 Color: 4

Bin 382: 134 of cap free
Amount of items: 2
Items: 
Size: 590528 Color: 2
Size: 409339 Color: 4

Bin 383: 136 of cap free
Amount of items: 2
Items: 
Size: 515733 Color: 1
Size: 484132 Color: 0

Bin 384: 137 of cap free
Amount of items: 2
Items: 
Size: 500209 Color: 2
Size: 499655 Color: 3

Bin 385: 137 of cap free
Amount of items: 2
Items: 
Size: 759364 Color: 4
Size: 240500 Color: 0

Bin 386: 138 of cap free
Amount of items: 2
Items: 
Size: 634754 Color: 0
Size: 365109 Color: 4

Bin 387: 138 of cap free
Amount of items: 2
Items: 
Size: 754683 Color: 1
Size: 245180 Color: 2

Bin 388: 141 of cap free
Amount of items: 2
Items: 
Size: 695933 Color: 4
Size: 303927 Color: 1

Bin 389: 141 of cap free
Amount of items: 3
Items: 
Size: 432848 Color: 0
Size: 285498 Color: 0
Size: 281514 Color: 1

Bin 390: 142 of cap free
Amount of items: 2
Items: 
Size: 681805 Color: 0
Size: 318054 Color: 1

Bin 391: 142 of cap free
Amount of items: 2
Items: 
Size: 539043 Color: 4
Size: 460816 Color: 1

Bin 392: 142 of cap free
Amount of items: 2
Items: 
Size: 520900 Color: 4
Size: 478959 Color: 1

Bin 393: 143 of cap free
Amount of items: 2
Items: 
Size: 719567 Color: 2
Size: 280291 Color: 3

Bin 394: 143 of cap free
Amount of items: 2
Items: 
Size: 577570 Color: 0
Size: 422288 Color: 3

Bin 395: 144 of cap free
Amount of items: 2
Items: 
Size: 598906 Color: 4
Size: 400951 Color: 1

Bin 396: 145 of cap free
Amount of items: 2
Items: 
Size: 749319 Color: 4
Size: 250537 Color: 1

Bin 397: 146 of cap free
Amount of items: 2
Items: 
Size: 644487 Color: 4
Size: 355368 Color: 3

Bin 398: 147 of cap free
Amount of items: 2
Items: 
Size: 567383 Color: 2
Size: 432471 Color: 0

Bin 399: 147 of cap free
Amount of items: 2
Items: 
Size: 789932 Color: 1
Size: 209922 Color: 0

Bin 400: 147 of cap free
Amount of items: 2
Items: 
Size: 758459 Color: 4
Size: 241395 Color: 1

Bin 401: 148 of cap free
Amount of items: 2
Items: 
Size: 542427 Color: 2
Size: 457426 Color: 1

Bin 402: 148 of cap free
Amount of items: 2
Items: 
Size: 780845 Color: 4
Size: 219008 Color: 2

Bin 403: 149 of cap free
Amount of items: 2
Items: 
Size: 569517 Color: 3
Size: 430335 Color: 0

Bin 404: 149 of cap free
Amount of items: 3
Items: 
Size: 401764 Color: 1
Size: 368877 Color: 3
Size: 229211 Color: 1

Bin 405: 150 of cap free
Amount of items: 3
Items: 
Size: 710566 Color: 0
Size: 161172 Color: 2
Size: 128113 Color: 3

Bin 406: 150 of cap free
Amount of items: 2
Items: 
Size: 713253 Color: 4
Size: 286598 Color: 2

Bin 407: 150 of cap free
Amount of items: 3
Items: 
Size: 753931 Color: 4
Size: 123803 Color: 0
Size: 122117 Color: 1

Bin 408: 150 of cap free
Amount of items: 2
Items: 
Size: 606077 Color: 0
Size: 393774 Color: 4

Bin 409: 151 of cap free
Amount of items: 2
Items: 
Size: 560818 Color: 0
Size: 439032 Color: 4

Bin 410: 153 of cap free
Amount of items: 2
Items: 
Size: 568211 Color: 1
Size: 431637 Color: 3

Bin 411: 154 of cap free
Amount of items: 3
Items: 
Size: 535415 Color: 4
Size: 234103 Color: 4
Size: 230329 Color: 0

Bin 412: 154 of cap free
Amount of items: 3
Items: 
Size: 496452 Color: 0
Size: 256406 Color: 4
Size: 246989 Color: 4

Bin 413: 155 of cap free
Amount of items: 2
Items: 
Size: 565223 Color: 4
Size: 434623 Color: 2

Bin 414: 156 of cap free
Amount of items: 3
Items: 
Size: 620626 Color: 0
Size: 191679 Color: 0
Size: 187540 Color: 2

Bin 415: 156 of cap free
Amount of items: 2
Items: 
Size: 653288 Color: 0
Size: 346557 Color: 1

Bin 416: 156 of cap free
Amount of items: 2
Items: 
Size: 543979 Color: 0
Size: 455866 Color: 3

Bin 417: 158 of cap free
Amount of items: 2
Items: 
Size: 677871 Color: 0
Size: 321972 Color: 4

Bin 418: 158 of cap free
Amount of items: 2
Items: 
Size: 706430 Color: 4
Size: 293413 Color: 2

Bin 419: 158 of cap free
Amount of items: 2
Items: 
Size: 758884 Color: 0
Size: 240959 Color: 1

Bin 420: 160 of cap free
Amount of items: 2
Items: 
Size: 728516 Color: 0
Size: 271325 Color: 4

Bin 421: 161 of cap free
Amount of items: 2
Items: 
Size: 751544 Color: 1
Size: 248296 Color: 3

Bin 422: 162 of cap free
Amount of items: 2
Items: 
Size: 722017 Color: 0
Size: 277822 Color: 3

Bin 423: 164 of cap free
Amount of items: 2
Items: 
Size: 799220 Color: 0
Size: 200617 Color: 1

Bin 424: 164 of cap free
Amount of items: 3
Items: 
Size: 354081 Color: 0
Size: 325281 Color: 2
Size: 320475 Color: 4

Bin 425: 164 of cap free
Amount of items: 3
Items: 
Size: 540979 Color: 4
Size: 341703 Color: 4
Size: 117155 Color: 1

Bin 426: 167 of cap free
Amount of items: 3
Items: 
Size: 629960 Color: 4
Size: 193566 Color: 2
Size: 176308 Color: 0

Bin 427: 167 of cap free
Amount of items: 2
Items: 
Size: 772801 Color: 1
Size: 227033 Color: 4

Bin 428: 168 of cap free
Amount of items: 2
Items: 
Size: 680381 Color: 4
Size: 319452 Color: 2

Bin 429: 169 of cap free
Amount of items: 2
Items: 
Size: 679798 Color: 4
Size: 320034 Color: 0

Bin 430: 173 of cap free
Amount of items: 2
Items: 
Size: 520455 Color: 1
Size: 479373 Color: 0

Bin 431: 175 of cap free
Amount of items: 2
Items: 
Size: 506048 Color: 3
Size: 493778 Color: 4

Bin 432: 176 of cap free
Amount of items: 2
Items: 
Size: 593815 Color: 0
Size: 406010 Color: 1

Bin 433: 177 of cap free
Amount of items: 2
Items: 
Size: 519023 Color: 4
Size: 480801 Color: 1

Bin 434: 181 of cap free
Amount of items: 2
Items: 
Size: 517149 Color: 1
Size: 482671 Color: 3

Bin 435: 181 of cap free
Amount of items: 2
Items: 
Size: 767133 Color: 0
Size: 232687 Color: 2

Bin 436: 184 of cap free
Amount of items: 2
Items: 
Size: 590516 Color: 3
Size: 409301 Color: 0

Bin 437: 184 of cap free
Amount of items: 2
Items: 
Size: 649227 Color: 0
Size: 350590 Color: 3

Bin 438: 186 of cap free
Amount of items: 2
Items: 
Size: 512147 Color: 1
Size: 487668 Color: 3

Bin 439: 186 of cap free
Amount of items: 2
Items: 
Size: 526922 Color: 4
Size: 472893 Color: 3

Bin 440: 186 of cap free
Amount of items: 2
Items: 
Size: 680871 Color: 1
Size: 318944 Color: 4

Bin 441: 186 of cap free
Amount of items: 2
Items: 
Size: 757413 Color: 4
Size: 242402 Color: 0

Bin 442: 189 of cap free
Amount of items: 2
Items: 
Size: 619210 Color: 3
Size: 380602 Color: 2

Bin 443: 190 of cap free
Amount of items: 2
Items: 
Size: 593549 Color: 1
Size: 406262 Color: 4

Bin 444: 193 of cap free
Amount of items: 2
Items: 
Size: 764605 Color: 0
Size: 235203 Color: 3

Bin 445: 195 of cap free
Amount of items: 3
Items: 
Size: 375716 Color: 2
Size: 355047 Color: 4
Size: 269043 Color: 2

Bin 446: 198 of cap free
Amount of items: 2
Items: 
Size: 550639 Color: 0
Size: 449164 Color: 4

Bin 447: 201 of cap free
Amount of items: 2
Items: 
Size: 526307 Color: 1
Size: 473493 Color: 4

Bin 448: 202 of cap free
Amount of items: 2
Items: 
Size: 771582 Color: 1
Size: 228217 Color: 2

Bin 449: 203 of cap free
Amount of items: 2
Items: 
Size: 568199 Color: 0
Size: 431599 Color: 4

Bin 450: 203 of cap free
Amount of items: 3
Items: 
Size: 458633 Color: 1
Size: 281878 Color: 0
Size: 259287 Color: 1

Bin 451: 203 of cap free
Amount of items: 2
Items: 
Size: 546061 Color: 3
Size: 453737 Color: 0

Bin 452: 209 of cap free
Amount of items: 3
Items: 
Size: 668004 Color: 1
Size: 166079 Color: 0
Size: 165709 Color: 1

Bin 453: 209 of cap free
Amount of items: 2
Items: 
Size: 539554 Color: 0
Size: 460238 Color: 1

Bin 454: 209 of cap free
Amount of items: 3
Items: 
Size: 377763 Color: 4
Size: 315124 Color: 2
Size: 306905 Color: 0

Bin 455: 209 of cap free
Amount of items: 2
Items: 
Size: 661450 Color: 2
Size: 338342 Color: 3

Bin 456: 211 of cap free
Amount of items: 2
Items: 
Size: 794382 Color: 4
Size: 205408 Color: 0

Bin 457: 211 of cap free
Amount of items: 2
Items: 
Size: 700423 Color: 1
Size: 299367 Color: 3

Bin 458: 212 of cap free
Amount of items: 2
Items: 
Size: 615246 Color: 3
Size: 384543 Color: 0

Bin 459: 213 of cap free
Amount of items: 2
Items: 
Size: 588128 Color: 1
Size: 411660 Color: 2

Bin 460: 214 of cap free
Amount of items: 3
Items: 
Size: 378616 Color: 2
Size: 330772 Color: 1
Size: 290399 Color: 1

Bin 461: 215 of cap free
Amount of items: 2
Items: 
Size: 752739 Color: 4
Size: 247047 Color: 1

Bin 462: 215 of cap free
Amount of items: 2
Items: 
Size: 653760 Color: 2
Size: 346026 Color: 0

Bin 463: 221 of cap free
Amount of items: 2
Items: 
Size: 511214 Color: 4
Size: 488566 Color: 2

Bin 464: 221 of cap free
Amount of items: 2
Items: 
Size: 667506 Color: 0
Size: 332274 Color: 4

Bin 465: 221 of cap free
Amount of items: 2
Items: 
Size: 691288 Color: 1
Size: 308492 Color: 3

Bin 466: 221 of cap free
Amount of items: 2
Items: 
Size: 667143 Color: 1
Size: 332637 Color: 3

Bin 467: 223 of cap free
Amount of items: 2
Items: 
Size: 529104 Color: 0
Size: 470674 Color: 4

Bin 468: 223 of cap free
Amount of items: 2
Items: 
Size: 680160 Color: 3
Size: 319618 Color: 4

Bin 469: 224 of cap free
Amount of items: 2
Items: 
Size: 546871 Color: 2
Size: 452906 Color: 4

Bin 470: 224 of cap free
Amount of items: 2
Items: 
Size: 681265 Color: 3
Size: 318512 Color: 2

Bin 471: 225 of cap free
Amount of items: 2
Items: 
Size: 561064 Color: 0
Size: 438712 Color: 2

Bin 472: 225 of cap free
Amount of items: 3
Items: 
Size: 626930 Color: 1
Size: 194123 Color: 2
Size: 178723 Color: 3

Bin 473: 232 of cap free
Amount of items: 2
Items: 
Size: 534832 Color: 0
Size: 464937 Color: 4

Bin 474: 232 of cap free
Amount of items: 2
Items: 
Size: 786826 Color: 4
Size: 212943 Color: 0

Bin 475: 233 of cap free
Amount of items: 2
Items: 
Size: 778370 Color: 1
Size: 221398 Color: 2

Bin 476: 234 of cap free
Amount of items: 2
Items: 
Size: 590498 Color: 4
Size: 409269 Color: 1

Bin 477: 236 of cap free
Amount of items: 2
Items: 
Size: 715809 Color: 4
Size: 283956 Color: 2

Bin 478: 240 of cap free
Amount of items: 2
Items: 
Size: 607928 Color: 0
Size: 391833 Color: 2

Bin 479: 241 of cap free
Amount of items: 2
Items: 
Size: 763807 Color: 0
Size: 235953 Color: 1

Bin 480: 242 of cap free
Amount of items: 2
Items: 
Size: 637413 Color: 0
Size: 362346 Color: 1

Bin 481: 243 of cap free
Amount of items: 2
Items: 
Size: 510607 Color: 2
Size: 489151 Color: 3

Bin 482: 243 of cap free
Amount of items: 2
Items: 
Size: 622684 Color: 0
Size: 377074 Color: 2

Bin 483: 244 of cap free
Amount of items: 2
Items: 
Size: 624334 Color: 4
Size: 375423 Color: 2

Bin 484: 246 of cap free
Amount of items: 2
Items: 
Size: 509940 Color: 0
Size: 489815 Color: 2

Bin 485: 252 of cap free
Amount of items: 3
Items: 
Size: 720339 Color: 1
Size: 145225 Color: 4
Size: 134185 Color: 0

Bin 486: 256 of cap free
Amount of items: 2
Items: 
Size: 787680 Color: 1
Size: 212065 Color: 2

Bin 487: 257 of cap free
Amount of items: 2
Items: 
Size: 757984 Color: 1
Size: 241760 Color: 0

Bin 488: 262 of cap free
Amount of items: 2
Items: 
Size: 524685 Color: 3
Size: 475054 Color: 4

Bin 489: 265 of cap free
Amount of items: 2
Items: 
Size: 590913 Color: 1
Size: 408823 Color: 3

Bin 490: 269 of cap free
Amount of items: 2
Items: 
Size: 700995 Color: 3
Size: 298737 Color: 1

Bin 491: 270 of cap free
Amount of items: 2
Items: 
Size: 516108 Color: 2
Size: 483623 Color: 4

Bin 492: 276 of cap free
Amount of items: 2
Items: 
Size: 758426 Color: 3
Size: 241299 Color: 0

Bin 493: 276 of cap free
Amount of items: 2
Items: 
Size: 786795 Color: 1
Size: 212930 Color: 2

Bin 494: 277 of cap free
Amount of items: 3
Items: 
Size: 581903 Color: 0
Size: 214140 Color: 2
Size: 203681 Color: 3

Bin 495: 283 of cap free
Amount of items: 3
Items: 
Size: 613519 Color: 3
Size: 217620 Color: 0
Size: 168579 Color: 4

Bin 496: 283 of cap free
Amount of items: 2
Items: 
Size: 736704 Color: 3
Size: 263014 Color: 0

Bin 497: 284 of cap free
Amount of items: 2
Items: 
Size: 694515 Color: 1
Size: 305202 Color: 2

Bin 498: 285 of cap free
Amount of items: 2
Items: 
Size: 527644 Color: 4
Size: 472072 Color: 0

Bin 499: 286 of cap free
Amount of items: 3
Items: 
Size: 601646 Color: 1
Size: 211594 Color: 0
Size: 186475 Color: 1

Bin 500: 287 of cap free
Amount of items: 3
Items: 
Size: 752190 Color: 0
Size: 132658 Color: 0
Size: 114866 Color: 4

Bin 501: 288 of cap free
Amount of items: 2
Items: 
Size: 635358 Color: 4
Size: 364355 Color: 0

Bin 502: 289 of cap free
Amount of items: 2
Items: 
Size: 777571 Color: 3
Size: 222141 Color: 4

Bin 503: 290 of cap free
Amount of items: 2
Items: 
Size: 617356 Color: 3
Size: 382355 Color: 2

Bin 504: 293 of cap free
Amount of items: 2
Items: 
Size: 571308 Color: 4
Size: 428400 Color: 2

Bin 505: 294 of cap free
Amount of items: 2
Items: 
Size: 652891 Color: 2
Size: 346816 Color: 0

Bin 506: 296 of cap free
Amount of items: 2
Items: 
Size: 564326 Color: 0
Size: 435379 Color: 2

Bin 507: 296 of cap free
Amount of items: 2
Items: 
Size: 533166 Color: 3
Size: 466539 Color: 1

Bin 508: 297 of cap free
Amount of items: 3
Items: 
Size: 374327 Color: 3
Size: 318472 Color: 0
Size: 306905 Color: 4

Bin 509: 300 of cap free
Amount of items: 2
Items: 
Size: 559135 Color: 1
Size: 440566 Color: 0

Bin 510: 301 of cap free
Amount of items: 2
Items: 
Size: 649950 Color: 1
Size: 349750 Color: 3

Bin 511: 304 of cap free
Amount of items: 2
Items: 
Size: 534427 Color: 0
Size: 465270 Color: 1

Bin 512: 305 of cap free
Amount of items: 3
Items: 
Size: 487139 Color: 2
Size: 365908 Color: 4
Size: 146649 Color: 0

Bin 513: 306 of cap free
Amount of items: 2
Items: 
Size: 518976 Color: 1
Size: 480719 Color: 4

Bin 514: 307 of cap free
Amount of items: 2
Items: 
Size: 583024 Color: 4
Size: 416670 Color: 0

Bin 515: 308 of cap free
Amount of items: 2
Items: 
Size: 656167 Color: 3
Size: 343526 Color: 0

Bin 516: 310 of cap free
Amount of items: 2
Items: 
Size: 759853 Color: 3
Size: 239838 Color: 0

Bin 517: 314 of cap free
Amount of items: 2
Items: 
Size: 776399 Color: 2
Size: 223288 Color: 1

Bin 518: 314 of cap free
Amount of items: 2
Items: 
Size: 563338 Color: 2
Size: 436349 Color: 3

Bin 519: 318 of cap free
Amount of items: 2
Items: 
Size: 590085 Color: 2
Size: 409598 Color: 0

Bin 520: 319 of cap free
Amount of items: 3
Items: 
Size: 378221 Color: 0
Size: 334026 Color: 4
Size: 287435 Color: 3

Bin 521: 320 of cap free
Amount of items: 2
Items: 
Size: 626474 Color: 0
Size: 373207 Color: 1

Bin 522: 322 of cap free
Amount of items: 2
Items: 
Size: 581009 Color: 4
Size: 418670 Color: 3

Bin 523: 323 of cap free
Amount of items: 2
Items: 
Size: 559283 Color: 0
Size: 440395 Color: 3

Bin 524: 325 of cap free
Amount of items: 2
Items: 
Size: 509542 Color: 2
Size: 490134 Color: 3

Bin 525: 329 of cap free
Amount of items: 2
Items: 
Size: 535977 Color: 1
Size: 463695 Color: 2

Bin 526: 329 of cap free
Amount of items: 3
Items: 
Size: 433734 Color: 3
Size: 392851 Color: 4
Size: 173087 Color: 1

Bin 527: 330 of cap free
Amount of items: 2
Items: 
Size: 528249 Color: 1
Size: 471422 Color: 3

Bin 528: 330 of cap free
Amount of items: 2
Items: 
Size: 752180 Color: 0
Size: 247491 Color: 4

Bin 529: 332 of cap free
Amount of items: 2
Items: 
Size: 718146 Color: 1
Size: 281523 Color: 2

Bin 530: 332 of cap free
Amount of items: 2
Items: 
Size: 548321 Color: 2
Size: 451348 Color: 4

Bin 531: 336 of cap free
Amount of items: 2
Items: 
Size: 677122 Color: 4
Size: 322543 Color: 2

Bin 532: 337 of cap free
Amount of items: 2
Items: 
Size: 787276 Color: 4
Size: 212388 Color: 3

Bin 533: 337 of cap free
Amount of items: 2
Items: 
Size: 599529 Color: 2
Size: 400135 Color: 0

Bin 534: 345 of cap free
Amount of items: 2
Items: 
Size: 507510 Color: 1
Size: 492146 Color: 2

Bin 535: 345 of cap free
Amount of items: 2
Items: 
Size: 778680 Color: 0
Size: 220976 Color: 2

Bin 536: 347 of cap free
Amount of items: 2
Items: 
Size: 653662 Color: 4
Size: 345992 Color: 1

Bin 537: 350 of cap free
Amount of items: 2
Items: 
Size: 720934 Color: 0
Size: 278717 Color: 1

Bin 538: 350 of cap free
Amount of items: 2
Items: 
Size: 500080 Color: 0
Size: 499571 Color: 4

Bin 539: 354 of cap free
Amount of items: 2
Items: 
Size: 589805 Color: 3
Size: 409842 Color: 2

Bin 540: 354 of cap free
Amount of items: 2
Items: 
Size: 727634 Color: 4
Size: 272013 Color: 0

Bin 541: 355 of cap free
Amount of items: 2
Items: 
Size: 670682 Color: 4
Size: 328964 Color: 1

Bin 542: 357 of cap free
Amount of items: 3
Items: 
Size: 352293 Color: 0
Size: 325172 Color: 4
Size: 322179 Color: 3

Bin 543: 359 of cap free
Amount of items: 2
Items: 
Size: 538029 Color: 0
Size: 461613 Color: 4

Bin 544: 363 of cap free
Amount of items: 2
Items: 
Size: 789509 Color: 0
Size: 210129 Color: 2

Bin 545: 365 of cap free
Amount of items: 2
Items: 
Size: 744803 Color: 0
Size: 254833 Color: 4

Bin 546: 369 of cap free
Amount of items: 2
Items: 
Size: 703895 Color: 3
Size: 295737 Color: 1

Bin 547: 373 of cap free
Amount of items: 2
Items: 
Size: 504754 Color: 2
Size: 494874 Color: 4

Bin 548: 374 of cap free
Amount of items: 3
Items: 
Size: 462624 Color: 1
Size: 301957 Color: 3
Size: 235046 Color: 2

Bin 549: 376 of cap free
Amount of items: 2
Items: 
Size: 623017 Color: 3
Size: 376608 Color: 0

Bin 550: 380 of cap free
Amount of items: 2
Items: 
Size: 659881 Color: 0
Size: 339740 Color: 4

Bin 551: 383 of cap free
Amount of items: 2
Items: 
Size: 759457 Color: 0
Size: 240161 Color: 2

Bin 552: 384 of cap free
Amount of items: 2
Items: 
Size: 559736 Color: 3
Size: 439881 Color: 2

Bin 553: 384 of cap free
Amount of items: 2
Items: 
Size: 600831 Color: 4
Size: 398786 Color: 3

Bin 554: 385 of cap free
Amount of items: 2
Items: 
Size: 511727 Color: 2
Size: 487889 Color: 3

Bin 555: 386 of cap free
Amount of items: 2
Items: 
Size: 506618 Color: 1
Size: 492997 Color: 2

Bin 556: 388 of cap free
Amount of items: 2
Items: 
Size: 653076 Color: 0
Size: 346537 Color: 4

Bin 557: 393 of cap free
Amount of items: 2
Items: 
Size: 649546 Color: 1
Size: 350062 Color: 2

Bin 558: 400 of cap free
Amount of items: 2
Items: 
Size: 601645 Color: 2
Size: 397956 Color: 4

Bin 559: 401 of cap free
Amount of items: 2
Items: 
Size: 585962 Color: 3
Size: 413638 Color: 2

Bin 560: 403 of cap free
Amount of items: 2
Items: 
Size: 744166 Color: 1
Size: 255432 Color: 0

Bin 561: 403 of cap free
Amount of items: 3
Items: 
Size: 607543 Color: 2
Size: 207742 Color: 4
Size: 184313 Color: 2

Bin 562: 403 of cap free
Amount of items: 2
Items: 
Size: 551122 Color: 1
Size: 448476 Color: 0

Bin 563: 408 of cap free
Amount of items: 2
Items: 
Size: 565880 Color: 0
Size: 433713 Color: 1

Bin 564: 415 of cap free
Amount of items: 2
Items: 
Size: 798345 Color: 4
Size: 201241 Color: 0

Bin 565: 419 of cap free
Amount of items: 2
Items: 
Size: 521533 Color: 4
Size: 478049 Color: 0

Bin 566: 419 of cap free
Amount of items: 2
Items: 
Size: 543296 Color: 0
Size: 456286 Color: 2

Bin 567: 419 of cap free
Amount of items: 2
Items: 
Size: 657203 Color: 4
Size: 342379 Color: 2

Bin 568: 421 of cap free
Amount of items: 2
Items: 
Size: 592547 Color: 3
Size: 407033 Color: 1

Bin 569: 424 of cap free
Amount of items: 2
Items: 
Size: 501614 Color: 0
Size: 497963 Color: 2

Bin 570: 428 of cap free
Amount of items: 2
Items: 
Size: 747350 Color: 3
Size: 252223 Color: 2

Bin 571: 432 of cap free
Amount of items: 2
Items: 
Size: 654461 Color: 1
Size: 345108 Color: 3

Bin 572: 436 of cap free
Amount of items: 2
Items: 
Size: 625964 Color: 4
Size: 373601 Color: 3

Bin 573: 438 of cap free
Amount of items: 2
Items: 
Size: 557283 Color: 2
Size: 442280 Color: 4

Bin 574: 444 of cap free
Amount of items: 2
Items: 
Size: 562047 Color: 1
Size: 437510 Color: 3

Bin 575: 445 of cap free
Amount of items: 2
Items: 
Size: 640748 Color: 0
Size: 358808 Color: 4

Bin 576: 449 of cap free
Amount of items: 2
Items: 
Size: 624440 Color: 2
Size: 375112 Color: 0

Bin 577: 451 of cap free
Amount of items: 2
Items: 
Size: 731884 Color: 0
Size: 267666 Color: 3

Bin 578: 456 of cap free
Amount of items: 2
Items: 
Size: 522266 Color: 1
Size: 477279 Color: 0

Bin 579: 459 of cap free
Amount of items: 2
Items: 
Size: 665552 Color: 2
Size: 333990 Color: 4

Bin 580: 460 of cap free
Amount of items: 2
Items: 
Size: 515419 Color: 0
Size: 484122 Color: 2

Bin 581: 462 of cap free
Amount of items: 2
Items: 
Size: 702036 Color: 1
Size: 297503 Color: 2

Bin 582: 462 of cap free
Amount of items: 2
Items: 
Size: 625266 Color: 2
Size: 374273 Color: 1

Bin 583: 463 of cap free
Amount of items: 2
Items: 
Size: 506583 Color: 0
Size: 492955 Color: 1

Bin 584: 467 of cap free
Amount of items: 2
Items: 
Size: 574639 Color: 3
Size: 424895 Color: 4

Bin 585: 477 of cap free
Amount of items: 2
Items: 
Size: 534885 Color: 4
Size: 464639 Color: 0

Bin 586: 480 of cap free
Amount of items: 2
Items: 
Size: 760207 Color: 0
Size: 239314 Color: 2

Bin 587: 484 of cap free
Amount of items: 2
Items: 
Size: 673116 Color: 0
Size: 326401 Color: 1

Bin 588: 485 of cap free
Amount of items: 2
Items: 
Size: 575681 Color: 1
Size: 423835 Color: 4

Bin 589: 487 of cap free
Amount of items: 2
Items: 
Size: 594334 Color: 3
Size: 405180 Color: 1

Bin 590: 488 of cap free
Amount of items: 2
Items: 
Size: 682825 Color: 2
Size: 316688 Color: 0

Bin 591: 491 of cap free
Amount of items: 2
Items: 
Size: 797164 Color: 0
Size: 202346 Color: 3

Bin 592: 494 of cap free
Amount of items: 2
Items: 
Size: 531348 Color: 0
Size: 468159 Color: 1

Bin 593: 504 of cap free
Amount of items: 2
Items: 
Size: 557575 Color: 1
Size: 441922 Color: 2

Bin 594: 511 of cap free
Amount of items: 2
Items: 
Size: 644058 Color: 0
Size: 355432 Color: 4

Bin 595: 520 of cap free
Amount of items: 2
Items: 
Size: 637912 Color: 4
Size: 361569 Color: 2

Bin 596: 524 of cap free
Amount of items: 3
Items: 
Size: 674185 Color: 0
Size: 173076 Color: 2
Size: 152216 Color: 2

Bin 597: 524 of cap free
Amount of items: 2
Items: 
Size: 695038 Color: 1
Size: 304439 Color: 3

Bin 598: 527 of cap free
Amount of items: 2
Items: 
Size: 643351 Color: 3
Size: 356123 Color: 2

Bin 599: 529 of cap free
Amount of items: 3
Items: 
Size: 452736 Color: 0
Size: 276936 Color: 3
Size: 269800 Color: 0

Bin 600: 539 of cap free
Amount of items: 2
Items: 
Size: 661944 Color: 0
Size: 337518 Color: 3

Bin 601: 546 of cap free
Amount of items: 2
Items: 
Size: 646336 Color: 4
Size: 353119 Color: 1

Bin 602: 546 of cap free
Amount of items: 2
Items: 
Size: 773171 Color: 1
Size: 226284 Color: 4

Bin 603: 547 of cap free
Amount of items: 2
Items: 
Size: 602623 Color: 4
Size: 396831 Color: 1

Bin 604: 549 of cap free
Amount of items: 2
Items: 
Size: 616616 Color: 0
Size: 382836 Color: 4

Bin 605: 557 of cap free
Amount of items: 3
Items: 
Size: 685907 Color: 3
Size: 164196 Color: 4
Size: 149341 Color: 4

Bin 606: 564 of cap free
Amount of items: 3
Items: 
Size: 724958 Color: 0
Size: 153789 Color: 0
Size: 120690 Color: 2

Bin 607: 568 of cap free
Amount of items: 2
Items: 
Size: 602519 Color: 1
Size: 396914 Color: 0

Bin 608: 575 of cap free
Amount of items: 2
Items: 
Size: 645484 Color: 4
Size: 353942 Color: 3

Bin 609: 576 of cap free
Amount of items: 2
Items: 
Size: 776128 Color: 1
Size: 223297 Color: 2

Bin 610: 576 of cap free
Amount of items: 2
Items: 
Size: 706139 Color: 0
Size: 293286 Color: 4

Bin 611: 576 of cap free
Amount of items: 2
Items: 
Size: 530335 Color: 1
Size: 469090 Color: 3

Bin 612: 577 of cap free
Amount of items: 2
Items: 
Size: 546716 Color: 1
Size: 452708 Color: 0

Bin 613: 597 of cap free
Amount of items: 2
Items: 
Size: 577940 Color: 0
Size: 421464 Color: 1

Bin 614: 606 of cap free
Amount of items: 2
Items: 
Size: 557600 Color: 4
Size: 441795 Color: 1

Bin 615: 610 of cap free
Amount of items: 2
Items: 
Size: 732244 Color: 3
Size: 267147 Color: 4

Bin 616: 612 of cap free
Amount of items: 2
Items: 
Size: 508435 Color: 0
Size: 490954 Color: 2

Bin 617: 613 of cap free
Amount of items: 2
Items: 
Size: 741498 Color: 3
Size: 257890 Color: 4

Bin 618: 613 of cap free
Amount of items: 2
Items: 
Size: 713003 Color: 4
Size: 286385 Color: 2

Bin 619: 615 of cap free
Amount of items: 2
Items: 
Size: 724361 Color: 2
Size: 275025 Color: 0

Bin 620: 616 of cap free
Amount of items: 2
Items: 
Size: 555393 Color: 2
Size: 443992 Color: 4

Bin 621: 626 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 1
Size: 228151 Color: 0

Bin 622: 633 of cap free
Amount of items: 2
Items: 
Size: 777539 Color: 3
Size: 221829 Color: 4

Bin 623: 637 of cap free
Amount of items: 2
Items: 
Size: 781125 Color: 1
Size: 218239 Color: 4

Bin 624: 638 of cap free
Amount of items: 2
Items: 
Size: 503097 Color: 0
Size: 496266 Color: 1

Bin 625: 652 of cap free
Amount of items: 2
Items: 
Size: 501890 Color: 2
Size: 497459 Color: 4

Bin 626: 659 of cap free
Amount of items: 2
Items: 
Size: 705674 Color: 3
Size: 293668 Color: 0

Bin 627: 662 of cap free
Amount of items: 2
Items: 
Size: 650698 Color: 3
Size: 348641 Color: 2

Bin 628: 663 of cap free
Amount of items: 2
Items: 
Size: 571172 Color: 1
Size: 428166 Color: 2

Bin 629: 670 of cap free
Amount of items: 2
Items: 
Size: 543982 Color: 3
Size: 455349 Color: 4

Bin 630: 670 of cap free
Amount of items: 2
Items: 
Size: 770396 Color: 1
Size: 228935 Color: 3

Bin 631: 678 of cap free
Amount of items: 2
Items: 
Size: 513357 Color: 2
Size: 485966 Color: 1

Bin 632: 687 of cap free
Amount of items: 2
Items: 
Size: 551855 Color: 1
Size: 447459 Color: 3

Bin 633: 688 of cap free
Amount of items: 2
Items: 
Size: 633045 Color: 3
Size: 366268 Color: 2

Bin 634: 691 of cap free
Amount of items: 2
Items: 
Size: 705671 Color: 3
Size: 293639 Color: 0

Bin 635: 696 of cap free
Amount of items: 2
Items: 
Size: 520791 Color: 0
Size: 478514 Color: 1

Bin 636: 702 of cap free
Amount of items: 2
Items: 
Size: 699022 Color: 4
Size: 300277 Color: 2

Bin 637: 706 of cap free
Amount of items: 2
Items: 
Size: 791620 Color: 3
Size: 207675 Color: 1

Bin 638: 710 of cap free
Amount of items: 2
Items: 
Size: 563052 Color: 4
Size: 436239 Color: 3

Bin 639: 717 of cap free
Amount of items: 2
Items: 
Size: 715526 Color: 2
Size: 283758 Color: 0

Bin 640: 728 of cap free
Amount of items: 2
Items: 
Size: 516038 Color: 1
Size: 483235 Color: 3

Bin 641: 729 of cap free
Amount of items: 2
Items: 
Size: 798310 Color: 3
Size: 200962 Color: 2

Bin 642: 735 of cap free
Amount of items: 2
Items: 
Size: 678973 Color: 2
Size: 320293 Color: 1

Bin 643: 739 of cap free
Amount of items: 2
Items: 
Size: 654368 Color: 4
Size: 344894 Color: 3

Bin 644: 740 of cap free
Amount of items: 2
Items: 
Size: 519950 Color: 2
Size: 479311 Color: 3

Bin 645: 741 of cap free
Amount of items: 2
Items: 
Size: 555081 Color: 0
Size: 444179 Color: 2

Bin 646: 742 of cap free
Amount of items: 2
Items: 
Size: 794039 Color: 4
Size: 205220 Color: 0

Bin 647: 746 of cap free
Amount of items: 3
Items: 
Size: 720141 Color: 3
Size: 148280 Color: 2
Size: 130834 Color: 2

Bin 648: 758 of cap free
Amount of items: 2
Items: 
Size: 596174 Color: 2
Size: 403069 Color: 3

Bin 649: 761 of cap free
Amount of items: 2
Items: 
Size: 763671 Color: 1
Size: 235569 Color: 2

Bin 650: 767 of cap free
Amount of items: 2
Items: 
Size: 574486 Color: 2
Size: 424748 Color: 4

Bin 651: 769 of cap free
Amount of items: 2
Items: 
Size: 677090 Color: 4
Size: 322142 Color: 3

Bin 652: 775 of cap free
Amount of items: 2
Items: 
Size: 532696 Color: 4
Size: 466530 Color: 3

Bin 653: 795 of cap free
Amount of items: 2
Items: 
Size: 506404 Color: 3
Size: 492802 Color: 2

Bin 654: 798 of cap free
Amount of items: 2
Items: 
Size: 726701 Color: 0
Size: 272502 Color: 4

Bin 655: 805 of cap free
Amount of items: 2
Items: 
Size: 614860 Color: 3
Size: 384336 Color: 4

Bin 656: 816 of cap free
Amount of items: 2
Items: 
Size: 531140 Color: 0
Size: 468045 Color: 2

Bin 657: 819 of cap free
Amount of items: 2
Items: 
Size: 779802 Color: 4
Size: 219380 Color: 2

Bin 658: 821 of cap free
Amount of items: 2
Items: 
Size: 553454 Color: 3
Size: 445726 Color: 1

Bin 659: 822 of cap free
Amount of items: 2
Items: 
Size: 647782 Color: 1
Size: 351397 Color: 2

Bin 660: 835 of cap free
Amount of items: 2
Items: 
Size: 571863 Color: 2
Size: 427303 Color: 3

Bin 661: 840 of cap free
Amount of items: 2
Items: 
Size: 781049 Color: 4
Size: 218112 Color: 3

Bin 662: 852 of cap free
Amount of items: 2
Items: 
Size: 762209 Color: 3
Size: 236940 Color: 2

Bin 663: 855 of cap free
Amount of items: 2
Items: 
Size: 660062 Color: 2
Size: 339084 Color: 0

Bin 664: 862 of cap free
Amount of items: 2
Items: 
Size: 693351 Color: 0
Size: 305788 Color: 1

Bin 665: 865 of cap free
Amount of items: 2
Items: 
Size: 719196 Color: 2
Size: 279940 Color: 4

Bin 666: 882 of cap free
Amount of items: 2
Items: 
Size: 759611 Color: 2
Size: 239508 Color: 0

Bin 667: 884 of cap free
Amount of items: 2
Items: 
Size: 512467 Color: 4
Size: 486650 Color: 2

Bin 668: 892 of cap free
Amount of items: 2
Items: 
Size: 700084 Color: 0
Size: 299025 Color: 2

Bin 669: 894 of cap free
Amount of items: 2
Items: 
Size: 505428 Color: 4
Size: 493679 Color: 3

Bin 670: 903 of cap free
Amount of items: 2
Items: 
Size: 680701 Color: 4
Size: 318397 Color: 3

Bin 671: 917 of cap free
Amount of items: 2
Items: 
Size: 528712 Color: 4
Size: 470372 Color: 2

Bin 672: 919 of cap free
Amount of items: 2
Items: 
Size: 575184 Color: 4
Size: 423898 Color: 2

Bin 673: 926 of cap free
Amount of items: 2
Items: 
Size: 673881 Color: 4
Size: 325194 Color: 1

Bin 674: 948 of cap free
Amount of items: 2
Items: 
Size: 662564 Color: 2
Size: 336489 Color: 4

Bin 675: 962 of cap free
Amount of items: 2
Items: 
Size: 698784 Color: 1
Size: 300255 Color: 3

Bin 676: 966 of cap free
Amount of items: 2
Items: 
Size: 748212 Color: 4
Size: 250823 Color: 1

Bin 677: 982 of cap free
Amount of items: 2
Items: 
Size: 577570 Color: 3
Size: 421449 Color: 4

Bin 678: 984 of cap free
Amount of items: 2
Items: 
Size: 708845 Color: 4
Size: 290172 Color: 1

Bin 679: 987 of cap free
Amount of items: 2
Items: 
Size: 555251 Color: 2
Size: 443763 Color: 3

Bin 680: 992 of cap free
Amount of items: 2
Items: 
Size: 627871 Color: 4
Size: 371138 Color: 2

Bin 681: 1008 of cap free
Amount of items: 2
Items: 
Size: 791419 Color: 4
Size: 207574 Color: 0

Bin 682: 1032 of cap free
Amount of items: 2
Items: 
Size: 652530 Color: 3
Size: 346439 Color: 2

Bin 683: 1033 of cap free
Amount of items: 2
Items: 
Size: 635186 Color: 1
Size: 363782 Color: 3

Bin 684: 1035 of cap free
Amount of items: 2
Items: 
Size: 523968 Color: 4
Size: 474998 Color: 1

Bin 685: 1037 of cap free
Amount of items: 2
Items: 
Size: 759894 Color: 0
Size: 239070 Color: 4

Bin 686: 1038 of cap free
Amount of items: 2
Items: 
Size: 600379 Color: 4
Size: 398584 Color: 0

Bin 687: 1051 of cap free
Amount of items: 2
Items: 
Size: 508302 Color: 2
Size: 490648 Color: 1

Bin 688: 1058 of cap free
Amount of items: 2
Items: 
Size: 670446 Color: 3
Size: 328497 Color: 0

Bin 689: 1065 of cap free
Amount of items: 2
Items: 
Size: 686282 Color: 0
Size: 312654 Color: 3

Bin 690: 1071 of cap free
Amount of items: 2
Items: 
Size: 567532 Color: 4
Size: 431398 Color: 3

Bin 691: 1091 of cap free
Amount of items: 2
Items: 
Size: 755968 Color: 3
Size: 242942 Color: 4

Bin 692: 1093 of cap free
Amount of items: 2
Items: 
Size: 526021 Color: 4
Size: 472887 Color: 2

Bin 693: 1095 of cap free
Amount of items: 2
Items: 
Size: 500764 Color: 2
Size: 498142 Color: 0

Bin 694: 1106 of cap free
Amount of items: 2
Items: 
Size: 592365 Color: 0
Size: 406530 Color: 3

Bin 695: 1120 of cap free
Amount of items: 2
Items: 
Size: 778256 Color: 1
Size: 220625 Color: 2

Bin 696: 1145 of cap free
Amount of items: 2
Items: 
Size: 725172 Color: 2
Size: 273684 Color: 0

Bin 697: 1148 of cap free
Amount of items: 2
Items: 
Size: 553146 Color: 0
Size: 445707 Color: 4

Bin 698: 1150 of cap free
Amount of items: 2
Items: 
Size: 600369 Color: 0
Size: 398482 Color: 4

Bin 699: 1167 of cap free
Amount of items: 2
Items: 
Size: 784720 Color: 2
Size: 214114 Color: 4

Bin 700: 1167 of cap free
Amount of items: 2
Items: 
Size: 549093 Color: 1
Size: 449741 Color: 3

Bin 701: 1189 of cap free
Amount of items: 2
Items: 
Size: 709958 Color: 2
Size: 288854 Color: 1

Bin 702: 1192 of cap free
Amount of items: 2
Items: 
Size: 562818 Color: 3
Size: 435991 Color: 1

Bin 703: 1221 of cap free
Amount of items: 2
Items: 
Size: 572151 Color: 4
Size: 426629 Color: 2

Bin 704: 1245 of cap free
Amount of items: 2
Items: 
Size: 640464 Color: 1
Size: 358292 Color: 3

Bin 705: 1277 of cap free
Amount of items: 2
Items: 
Size: 715183 Color: 2
Size: 283541 Color: 0

Bin 706: 1280 of cap free
Amount of items: 2
Items: 
Size: 654242 Color: 3
Size: 344479 Color: 1

Bin 707: 1283 of cap free
Amount of items: 2
Items: 
Size: 733129 Color: 0
Size: 265589 Color: 4

Bin 708: 1285 of cap free
Amount of items: 2
Items: 
Size: 506402 Color: 4
Size: 492314 Color: 1

Bin 709: 1286 of cap free
Amount of items: 2
Items: 
Size: 569398 Color: 1
Size: 429317 Color: 2

Bin 710: 1293 of cap free
Amount of items: 2
Items: 
Size: 668626 Color: 4
Size: 330082 Color: 2

Bin 711: 1301 of cap free
Amount of items: 2
Items: 
Size: 557533 Color: 4
Size: 441167 Color: 2

Bin 712: 1302 of cap free
Amount of items: 2
Items: 
Size: 654229 Color: 2
Size: 344470 Color: 0

Bin 713: 1317 of cap free
Amount of items: 2
Items: 
Size: 735761 Color: 4
Size: 262923 Color: 1

Bin 714: 1331 of cap free
Amount of items: 2
Items: 
Size: 611077 Color: 4
Size: 387593 Color: 1

Bin 715: 1336 of cap free
Amount of items: 2
Items: 
Size: 676912 Color: 4
Size: 321753 Color: 0

Bin 716: 1348 of cap free
Amount of items: 2
Items: 
Size: 771130 Color: 2
Size: 227523 Color: 3

Bin 717: 1362 of cap free
Amount of items: 2
Items: 
Size: 763599 Color: 4
Size: 235040 Color: 3

Bin 718: 1380 of cap free
Amount of items: 2
Items: 
Size: 638602 Color: 3
Size: 360019 Color: 0

Bin 719: 1384 of cap free
Amount of items: 2
Items: 
Size: 585228 Color: 4
Size: 413389 Color: 2

Bin 720: 1440 of cap free
Amount of items: 2
Items: 
Size: 673715 Color: 0
Size: 324846 Color: 3

Bin 721: 1459 of cap free
Amount of items: 2
Items: 
Size: 698607 Color: 1
Size: 299935 Color: 4

Bin 722: 1465 of cap free
Amount of items: 2
Items: 
Size: 555011 Color: 2
Size: 443525 Color: 1

Bin 723: 1480 of cap free
Amount of items: 2
Items: 
Size: 619461 Color: 0
Size: 379060 Color: 4

Bin 724: 1487 of cap free
Amount of items: 2
Items: 
Size: 552956 Color: 0
Size: 445558 Color: 1

Bin 725: 1490 of cap free
Amount of items: 2
Items: 
Size: 538908 Color: 2
Size: 459603 Color: 3

Bin 726: 1499 of cap free
Amount of items: 2
Items: 
Size: 756386 Color: 4
Size: 242116 Color: 3

Bin 727: 1504 of cap free
Amount of items: 2
Items: 
Size: 523779 Color: 4
Size: 474718 Color: 1

Bin 728: 1529 of cap free
Amount of items: 2
Items: 
Size: 534279 Color: 3
Size: 464193 Color: 4

Bin 729: 1532 of cap free
Amount of items: 2
Items: 
Size: 607475 Color: 3
Size: 390994 Color: 0

Bin 730: 1554 of cap free
Amount of items: 2
Items: 
Size: 643903 Color: 1
Size: 354544 Color: 0

Bin 731: 1570 of cap free
Amount of items: 2
Items: 
Size: 571164 Color: 0
Size: 427267 Color: 4

Bin 732: 1581 of cap free
Amount of items: 2
Items: 
Size: 534237 Color: 1
Size: 464183 Color: 2

Bin 733: 1582 of cap free
Amount of items: 2
Items: 
Size: 523763 Color: 4
Size: 474656 Color: 0

Bin 734: 1608 of cap free
Amount of items: 2
Items: 
Size: 748051 Color: 1
Size: 250342 Color: 4

Bin 735: 1622 of cap free
Amount of items: 2
Items: 
Size: 709873 Color: 0
Size: 288506 Color: 3

Bin 736: 1627 of cap free
Amount of items: 2
Items: 
Size: 703817 Color: 4
Size: 294557 Color: 2

Bin 737: 1651 of cap free
Amount of items: 2
Items: 
Size: 714870 Color: 4
Size: 283480 Color: 1

Bin 738: 1658 of cap free
Amount of items: 2
Items: 
Size: 784506 Color: 1
Size: 213837 Color: 2

Bin 739: 1677 of cap free
Amount of items: 2
Items: 
Size: 556626 Color: 3
Size: 441698 Color: 4

Bin 740: 1679 of cap free
Amount of items: 2
Items: 
Size: 761472 Color: 3
Size: 236850 Color: 4

Bin 741: 1745 of cap free
Amount of items: 3
Items: 
Size: 678443 Color: 4
Size: 168549 Color: 3
Size: 151264 Color: 0

Bin 742: 1759 of cap free
Amount of items: 2
Items: 
Size: 534170 Color: 4
Size: 464072 Color: 1

Bin 743: 1760 of cap free
Amount of items: 2
Items: 
Size: 748003 Color: 4
Size: 250238 Color: 1

Bin 744: 1783 of cap free
Amount of items: 2
Items: 
Size: 574332 Color: 2
Size: 423886 Color: 1

Bin 745: 1799 of cap free
Amount of items: 2
Items: 
Size: 543601 Color: 2
Size: 454601 Color: 0

Bin 746: 1805 of cap free
Amount of items: 2
Items: 
Size: 502104 Color: 1
Size: 496092 Color: 2

Bin 747: 1811 of cap free
Amount of items: 2
Items: 
Size: 646924 Color: 1
Size: 351266 Color: 0

Bin 748: 1817 of cap free
Amount of items: 2
Items: 
Size: 616167 Color: 2
Size: 382017 Color: 1

Bin 749: 1824 of cap free
Amount of items: 2
Items: 
Size: 627671 Color: 4
Size: 370506 Color: 0

Bin 750: 1827 of cap free
Amount of items: 2
Items: 
Size: 661710 Color: 0
Size: 336464 Color: 2

Bin 751: 1828 of cap free
Amount of items: 2
Items: 
Size: 696598 Color: 3
Size: 301575 Color: 2

Bin 752: 1854 of cap free
Amount of items: 2
Items: 
Size: 634728 Color: 4
Size: 363419 Color: 2

Bin 753: 1895 of cap free
Amount of items: 2
Items: 
Size: 538575 Color: 2
Size: 459531 Color: 0

Bin 754: 2011 of cap free
Amount of items: 2
Items: 
Size: 690267 Color: 0
Size: 307723 Color: 3

Bin 755: 2017 of cap free
Amount of items: 2
Items: 
Size: 607340 Color: 4
Size: 390644 Color: 3

Bin 756: 2026 of cap free
Amount of items: 2
Items: 
Size: 543072 Color: 1
Size: 454903 Color: 2

Bin 757: 2036 of cap free
Amount of items: 2
Items: 
Size: 580912 Color: 4
Size: 417053 Color: 2

Bin 758: 2038 of cap free
Amount of items: 2
Items: 
Size: 624836 Color: 0
Size: 373127 Color: 3

Bin 759: 2038 of cap free
Amount of items: 2
Items: 
Size: 600264 Color: 3
Size: 397699 Color: 4

Bin 760: 2040 of cap free
Amount of items: 2
Items: 
Size: 618909 Color: 1
Size: 379052 Color: 3

Bin 761: 2053 of cap free
Amount of items: 2
Items: 
Size: 751263 Color: 4
Size: 246685 Color: 0

Bin 762: 2073 of cap free
Amount of items: 2
Items: 
Size: 799898 Color: 3
Size: 198030 Color: 1

Bin 763: 2092 of cap free
Amount of items: 2
Items: 
Size: 510887 Color: 2
Size: 487022 Color: 3

Bin 764: 2129 of cap free
Amount of items: 2
Items: 
Size: 735398 Color: 4
Size: 262474 Color: 1

Bin 765: 2141 of cap free
Amount of items: 2
Items: 
Size: 759240 Color: 3
Size: 238620 Color: 2

Bin 766: 2209 of cap free
Amount of items: 2
Items: 
Size: 519891 Color: 2
Size: 477901 Color: 3

Bin 767: 2256 of cap free
Amount of items: 2
Items: 
Size: 687491 Color: 3
Size: 310254 Color: 1

Bin 768: 2262 of cap free
Amount of items: 2
Items: 
Size: 534215 Color: 1
Size: 463524 Color: 0

Bin 769: 2275 of cap free
Amount of items: 2
Items: 
Size: 584366 Color: 1
Size: 413360 Color: 2

Bin 770: 2387 of cap free
Amount of items: 2
Items: 
Size: 765335 Color: 1
Size: 232279 Color: 2

Bin 771: 2478 of cap free
Amount of items: 2
Items: 
Size: 580045 Color: 3
Size: 417478 Color: 4

Bin 772: 2560 of cap free
Amount of items: 2
Items: 
Size: 642839 Color: 4
Size: 354602 Color: 1

Bin 773: 2568 of cap free
Amount of items: 2
Items: 
Size: 543014 Color: 2
Size: 454419 Color: 4

Bin 774: 2673 of cap free
Amount of items: 2
Items: 
Size: 519574 Color: 2
Size: 477754 Color: 0

Bin 775: 2681 of cap free
Amount of items: 2
Items: 
Size: 618748 Color: 0
Size: 378572 Color: 4

Bin 776: 2691 of cap free
Amount of items: 2
Items: 
Size: 533696 Color: 2
Size: 463614 Color: 1

Bin 777: 2720 of cap free
Amount of items: 2
Items: 
Size: 790996 Color: 4
Size: 206285 Color: 0

Bin 778: 2773 of cap free
Amount of items: 2
Items: 
Size: 649148 Color: 0
Size: 348080 Color: 4

Bin 779: 2786 of cap free
Amount of items: 2
Items: 
Size: 562318 Color: 3
Size: 434897 Color: 1

Bin 780: 2801 of cap free
Amount of items: 2
Items: 
Size: 790690 Color: 2
Size: 206510 Color: 4

Bin 781: 2837 of cap free
Amount of items: 2
Items: 
Size: 765566 Color: 3
Size: 231598 Color: 1

Bin 782: 2841 of cap free
Amount of items: 2
Items: 
Size: 799691 Color: 0
Size: 197469 Color: 1

Bin 783: 3027 of cap free
Amount of items: 2
Items: 
Size: 599300 Color: 3
Size: 397674 Color: 2

Bin 784: 3042 of cap free
Amount of items: 2
Items: 
Size: 599378 Color: 2
Size: 397581 Color: 0

Bin 785: 3067 of cap free
Amount of items: 2
Items: 
Size: 739235 Color: 3
Size: 257699 Color: 0

Bin 786: 3117 of cap free
Amount of items: 2
Items: 
Size: 779356 Color: 2
Size: 217528 Color: 3

Bin 787: 3166 of cap free
Amount of items: 2
Items: 
Size: 533596 Color: 1
Size: 463239 Color: 0

Bin 788: 3191 of cap free
Amount of items: 2
Items: 
Size: 649029 Color: 4
Size: 347781 Color: 1

Bin 789: 3255 of cap free
Amount of items: 2
Items: 
Size: 742422 Color: 4
Size: 254324 Color: 1

Bin 790: 3432 of cap free
Amount of items: 2
Items: 
Size: 672830 Color: 2
Size: 323739 Color: 4

Bin 791: 3458 of cap free
Amount of items: 2
Items: 
Size: 499198 Color: 2
Size: 497345 Color: 4

Bin 792: 3470 of cap free
Amount of items: 2
Items: 
Size: 538513 Color: 1
Size: 458018 Color: 2

Bin 793: 3524 of cap free
Amount of items: 2
Items: 
Size: 747343 Color: 0
Size: 249134 Color: 2

Bin 794: 3570 of cap free
Amount of items: 2
Items: 
Size: 561999 Color: 3
Size: 434432 Color: 2

Bin 795: 3643 of cap free
Amount of items: 2
Items: 
Size: 591429 Color: 4
Size: 404929 Color: 2

Bin 796: 3688 of cap free
Amount of items: 2
Items: 
Size: 579940 Color: 1
Size: 416373 Color: 0

Bin 797: 3722 of cap free
Amount of items: 2
Items: 
Size: 583274 Color: 0
Size: 413005 Color: 3

Bin 798: 3751 of cap free
Amount of items: 2
Items: 
Size: 782714 Color: 1
Size: 213536 Color: 2

Bin 799: 3781 of cap free
Amount of items: 2
Items: 
Size: 523501 Color: 1
Size: 472719 Color: 3

Bin 800: 3839 of cap free
Amount of items: 2
Items: 
Size: 606471 Color: 4
Size: 389691 Color: 3

Bin 801: 3934 of cap free
Amount of items: 2
Items: 
Size: 648933 Color: 4
Size: 347134 Color: 3

Bin 802: 3971 of cap free
Amount of items: 2
Items: 
Size: 569437 Color: 0
Size: 426593 Color: 1

Bin 803: 4022 of cap free
Amount of items: 2
Items: 
Size: 714871 Color: 1
Size: 281108 Color: 2

Bin 804: 4064 of cap free
Amount of items: 2
Items: 
Size: 708581 Color: 0
Size: 287356 Color: 2

Bin 805: 4256 of cap free
Amount of items: 2
Items: 
Size: 591269 Color: 4
Size: 404476 Color: 0

Bin 806: 4310 of cap free
Amount of items: 2
Items: 
Size: 682435 Color: 3
Size: 313256 Color: 0

Bin 807: 4369 of cap free
Amount of items: 2
Items: 
Size: 541990 Color: 2
Size: 453642 Color: 4

Bin 808: 4400 of cap free
Amount of items: 2
Items: 
Size: 632613 Color: 3
Size: 362988 Color: 4

Bin 809: 4425 of cap free
Amount of items: 2
Items: 
Size: 758869 Color: 0
Size: 236707 Color: 2

Bin 810: 4541 of cap free
Amount of items: 2
Items: 
Size: 733790 Color: 4
Size: 261670 Color: 1

Bin 811: 4823 of cap free
Amount of items: 2
Items: 
Size: 758391 Color: 2
Size: 236787 Color: 0

Bin 812: 4868 of cap free
Amount of items: 2
Items: 
Size: 574192 Color: 1
Size: 420941 Color: 3

Bin 813: 4983 of cap free
Amount of items: 2
Items: 
Size: 740413 Color: 0
Size: 254605 Color: 4

Bin 814: 5030 of cap free
Amount of items: 2
Items: 
Size: 574027 Color: 3
Size: 420944 Color: 2

Bin 815: 5366 of cap free
Amount of items: 2
Items: 
Size: 693225 Color: 3
Size: 301410 Color: 1

Bin 816: 5419 of cap free
Amount of items: 2
Items: 
Size: 797133 Color: 2
Size: 197449 Color: 0

Bin 817: 5439 of cap free
Amount of items: 2
Items: 
Size: 537531 Color: 1
Size: 457031 Color: 2

Bin 818: 5493 of cap free
Amount of items: 2
Items: 
Size: 549190 Color: 3
Size: 445318 Color: 0

Bin 819: 5508 of cap free
Amount of items: 2
Items: 
Size: 797159 Color: 4
Size: 197334 Color: 2

Bin 820: 5560 of cap free
Amount of items: 2
Items: 
Size: 797073 Color: 2
Size: 197368 Color: 0

Bin 821: 5768 of cap free
Amount of items: 2
Items: 
Size: 530685 Color: 3
Size: 463548 Color: 1

Bin 822: 5922 of cap free
Amount of items: 2
Items: 
Size: 708275 Color: 3
Size: 285804 Color: 1

Bin 823: 6164 of cap free
Amount of items: 2
Items: 
Size: 757137 Color: 3
Size: 236700 Color: 2

Bin 824: 6257 of cap free
Amount of items: 2
Items: 
Size: 554735 Color: 4
Size: 439009 Color: 0

Bin 825: 6265 of cap free
Amount of items: 2
Items: 
Size: 604070 Color: 3
Size: 389666 Color: 1

Bin 826: 6530 of cap free
Amount of items: 2
Items: 
Size: 707623 Color: 0
Size: 285848 Color: 3

Bin 827: 6606 of cap free
Amount of items: 2
Items: 
Size: 729275 Color: 2
Size: 264120 Color: 4

Bin 828: 6724 of cap free
Amount of items: 2
Items: 
Size: 624334 Color: 0
Size: 368943 Color: 1

Bin 829: 6850 of cap free
Amount of items: 2
Items: 
Size: 649010 Color: 1
Size: 344141 Color: 4

Bin 830: 6925 of cap free
Amount of items: 2
Items: 
Size: 681735 Color: 2
Size: 311341 Color: 3

Bin 831: 6945 of cap free
Amount of items: 2
Items: 
Size: 604043 Color: 3
Size: 389013 Color: 0

Bin 832: 7453 of cap free
Amount of items: 2
Items: 
Size: 632665 Color: 1
Size: 359883 Color: 3

Bin 833: 7595 of cap free
Amount of items: 2
Items: 
Size: 579430 Color: 4
Size: 412976 Color: 1

Bin 834: 7954 of cap free
Amount of items: 2
Items: 
Size: 632239 Color: 1
Size: 359808 Color: 2

Bin 835: 8054 of cap free
Amount of items: 2
Items: 
Size: 738864 Color: 2
Size: 253083 Color: 3

Bin 836: 8275 of cap free
Amount of items: 2
Items: 
Size: 589590 Color: 2
Size: 402136 Color: 4

Bin 837: 8489 of cap free
Amount of items: 2
Items: 
Size: 698583 Color: 1
Size: 292929 Color: 0

Bin 838: 9056 of cap free
Amount of items: 2
Items: 
Size: 537443 Color: 3
Size: 453502 Color: 2

Bin 839: 9506 of cap free
Amount of items: 2
Items: 
Size: 536997 Color: 4
Size: 453498 Color: 0

Bin 840: 9617 of cap free
Amount of items: 2
Items: 
Size: 495689 Color: 2
Size: 494695 Color: 0

Bin 841: 10981 of cap free
Amount of items: 2
Items: 
Size: 494727 Color: 2
Size: 494293 Color: 4

Bin 842: 11010 of cap free
Amount of items: 2
Items: 
Size: 525948 Color: 3
Size: 463043 Color: 1

Bin 843: 14080 of cap free
Amount of items: 2
Items: 
Size: 738563 Color: 0
Size: 247358 Color: 2

Bin 844: 14409 of cap free
Amount of items: 2
Items: 
Size: 642610 Color: 0
Size: 342982 Color: 4

Bin 845: 14732 of cap free
Amount of items: 2
Items: 
Size: 573889 Color: 4
Size: 411380 Color: 1

Bin 846: 14799 of cap free
Amount of items: 2
Items: 
Size: 796996 Color: 4
Size: 188206 Color: 3

Bin 847: 15615 of cap free
Amount of items: 2
Items: 
Size: 494724 Color: 2
Size: 489662 Color: 0

Bin 848: 16125 of cap free
Amount of items: 2
Items: 
Size: 730989 Color: 4
Size: 252887 Color: 0

Bin 849: 16244 of cap free
Amount of items: 2
Items: 
Size: 602425 Color: 3
Size: 381332 Color: 2

Bin 850: 19584 of cap free
Amount of items: 2
Items: 
Size: 528685 Color: 1
Size: 451732 Color: 4

Bin 851: 20587 of cap free
Amount of items: 2
Items: 
Size: 795882 Color: 0
Size: 183532 Color: 4

Bin 852: 24754 of cap free
Amount of items: 2
Items: 
Size: 489516 Color: 3
Size: 485731 Color: 4

Bin 853: 29712 of cap free
Amount of items: 2
Items: 
Size: 573988 Color: 1
Size: 396301 Color: 3

Bin 854: 33310 of cap free
Amount of items: 2
Items: 
Size: 630958 Color: 1
Size: 335733 Color: 3

Bin 855: 35612 of cap free
Amount of items: 2
Items: 
Size: 483008 Color: 0
Size: 481381 Color: 1

Bin 856: 38061 of cap free
Amount of items: 2
Items: 
Size: 481326 Color: 3
Size: 480614 Color: 2

Bin 857: 43359 of cap free
Amount of items: 2
Items: 
Size: 795752 Color: 0
Size: 160890 Color: 2

Bin 858: 83424 of cap free
Amount of items: 2
Items: 
Size: 573675 Color: 3
Size: 342902 Color: 2

Bin 859: 88287 of cap free
Amount of items: 2
Items: 
Size: 480321 Color: 2
Size: 431393 Color: 4

Bin 860: 91410 of cap free
Amount of items: 2
Items: 
Size: 672062 Color: 2
Size: 236529 Color: 1

Bin 861: 99637 of cap free
Amount of items: 2
Items: 
Size: 795070 Color: 2
Size: 105294 Color: 4

Bin 862: 204981 of cap free
Amount of items: 1
Items: 
Size: 795020 Color: 3

Bin 863: 209166 of cap free
Amount of items: 1
Items: 
Size: 790835 Color: 4

Bin 864: 209677 of cap free
Amount of items: 1
Items: 
Size: 790324 Color: 2

Bin 865: 209880 of cap free
Amount of items: 1
Items: 
Size: 790121 Color: 2

Bin 866: 211165 of cap free
Amount of items: 1
Items: 
Size: 788836 Color: 3

Bin 867: 211269 of cap free
Amount of items: 1
Items: 
Size: 788732 Color: 3

Bin 868: 215974 of cap free
Amount of items: 1
Items: 
Size: 784027 Color: 2

Bin 869: 217394 of cap free
Amount of items: 1
Items: 
Size: 782607 Color: 3

Bin 870: 222359 of cap free
Amount of items: 1
Items: 
Size: 777642 Color: 4

Bin 871: 222494 of cap free
Amount of items: 1
Items: 
Size: 777507 Color: 3

Bin 872: 223935 of cap free
Amount of items: 1
Items: 
Size: 776066 Color: 1

Bin 873: 227155 of cap free
Amount of items: 1
Items: 
Size: 772846 Color: 4

Bin 874: 227770 of cap free
Amount of items: 1
Items: 
Size: 772231 Color: 3

Bin 875: 227819 of cap free
Amount of items: 1
Items: 
Size: 772182 Color: 3

Bin 876: 230591 of cap free
Amount of items: 1
Items: 
Size: 769410 Color: 2

Bin 877: 230696 of cap free
Amount of items: 1
Items: 
Size: 769305 Color: 1

Bin 878: 244238 of cap free
Amount of items: 1
Items: 
Size: 755763 Color: 1

Bin 879: 244387 of cap free
Amount of items: 1
Items: 
Size: 755614 Color: 1

Bin 880: 246078 of cap free
Amount of items: 1
Items: 
Size: 753923 Color: 1

Total size: 874417442
Total free space: 5583438

