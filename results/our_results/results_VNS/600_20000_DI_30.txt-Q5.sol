Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9380 Color: 4
Size: 5748 Color: 4
Size: 1160 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 3
Size: 5828 Color: 4
Size: 1160 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9412 Color: 0
Size: 5708 Color: 4
Size: 1168 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 4
Size: 5832 Color: 4
Size: 704 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9798 Color: 4
Size: 6050 Color: 1
Size: 440 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 2
Size: 5292 Color: 4
Size: 640 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10372 Color: 3
Size: 5640 Color: 2
Size: 276 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10406 Color: 2
Size: 5474 Color: 4
Size: 408 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11314 Color: 4
Size: 4166 Color: 2
Size: 808 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11452 Color: 0
Size: 2617 Color: 2
Size: 2219 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11346 Color: 3
Size: 4604 Color: 2
Size: 338 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3880 Color: 2
Size: 496 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 1
Size: 3656 Color: 2
Size: 612 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 1
Size: 2746 Color: 0
Size: 576 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13040 Color: 4
Size: 2916 Color: 2
Size: 332 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 2
Size: 2824 Color: 3
Size: 340 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 1
Size: 1896 Color: 0
Size: 1172 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 2
Size: 2224 Color: 4
Size: 828 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 3
Size: 2482 Color: 2
Size: 504 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 0
Size: 2424 Color: 2
Size: 504 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 3
Size: 2728 Color: 2
Size: 176 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13410 Color: 4
Size: 2334 Color: 2
Size: 544 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 3
Size: 2312 Color: 0
Size: 496 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 3
Size: 1940 Color: 0
Size: 832 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13558 Color: 1
Size: 2170 Color: 1
Size: 560 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 2
Size: 2060 Color: 3
Size: 632 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 2
Size: 2200 Color: 4
Size: 432 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13732 Color: 2
Size: 1356 Color: 0
Size: 1200 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 2
Size: 2416 Color: 1
Size: 120 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 0
Size: 2124 Color: 2
Size: 416 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13810 Color: 2
Size: 1704 Color: 1
Size: 774 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 2
Size: 1804 Color: 4
Size: 664 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 3
Size: 1964 Color: 2
Size: 488 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13866 Color: 2
Size: 1826 Color: 0
Size: 596 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 3
Size: 1980 Color: 4
Size: 392 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13922 Color: 1
Size: 1986 Color: 2
Size: 380 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13931 Color: 0
Size: 1965 Color: 3
Size: 392 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 2
Size: 1796 Color: 1
Size: 528 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 4
Size: 1606 Color: 2
Size: 696 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14034 Color: 3
Size: 1532 Color: 2
Size: 722 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14083 Color: 3
Size: 1845 Color: 2
Size: 360 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 0
Size: 1498 Color: 3
Size: 686 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 0
Size: 1414 Color: 2
Size: 760 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14165 Color: 2
Size: 1675 Color: 4
Size: 448 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14162 Color: 1
Size: 1440 Color: 2
Size: 686 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 2
Size: 1692 Color: 3
Size: 336 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 3
Size: 1820 Color: 2
Size: 220 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 2
Size: 1084 Color: 1
Size: 882 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14279 Color: 0
Size: 1533 Color: 2
Size: 476 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 1
Size: 1464 Color: 2
Size: 480 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 1
Size: 1352 Color: 2
Size: 580 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14364 Color: 3
Size: 1280 Color: 2
Size: 644 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 2
Size: 1178 Color: 0
Size: 680 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 2
Size: 1356 Color: 3
Size: 480 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14449 Color: 0
Size: 1443 Color: 3
Size: 396 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14469 Color: 4
Size: 1517 Color: 2
Size: 302 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14501 Color: 4
Size: 1491 Color: 4
Size: 296 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14594 Color: 3
Size: 1386 Color: 0
Size: 308 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 864 Color: 2
Size: 812 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 1
Size: 1356 Color: 2
Size: 306 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 3
Size: 1144 Color: 2
Size: 492 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 9444 Color: 2
Size: 4808 Color: 2
Size: 2035 Color: 1

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10993 Color: 3
Size: 4926 Color: 4
Size: 368 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11458 Color: 3
Size: 4413 Color: 4
Size: 416 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 3
Size: 3866 Color: 3
Size: 304 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12161 Color: 0
Size: 3646 Color: 2
Size: 480 Color: 3

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12402 Color: 2
Size: 3433 Color: 4
Size: 452 Color: 3

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 0
Size: 3411 Color: 3
Size: 384 Color: 4

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 4
Size: 2411 Color: 4
Size: 1080 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 3
Size: 2120 Color: 1
Size: 520 Color: 2

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14287 Color: 0
Size: 2000 Color: 1

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 3
Size: 1396 Color: 4
Size: 334 Color: 2

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 14592 Color: 4
Size: 1695 Color: 0

Bin 74: 2 of cap free
Amount of items: 5
Items: 
Size: 8154 Color: 4
Size: 4042 Color: 2
Size: 1974 Color: 1
Size: 1404 Color: 3
Size: 712 Color: 4

Bin 75: 2 of cap free
Amount of items: 5
Items: 
Size: 8148 Color: 3
Size: 5438 Color: 2
Size: 1608 Color: 4
Size: 728 Color: 1
Size: 364 Color: 0

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 3
Size: 6000 Color: 1
Size: 520 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 2
Size: 4964 Color: 0
Size: 432 Color: 3

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11256 Color: 1
Size: 4122 Color: 0
Size: 908 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 2
Size: 4328 Color: 0
Size: 516 Color: 3

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 0
Size: 4044 Color: 3
Size: 560 Color: 2

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 12334 Color: 4
Size: 3432 Color: 2
Size: 520 Color: 3

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 1
Size: 2152 Color: 2
Size: 1549 Color: 3

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12607 Color: 0
Size: 2695 Color: 4
Size: 984 Color: 3

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12994 Color: 0
Size: 2508 Color: 4
Size: 784 Color: 1

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 1
Size: 3242 Color: 2
Size: 140 Color: 3

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 0
Size: 3180 Color: 1
Size: 96 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13082 Color: 3
Size: 2044 Color: 0
Size: 1160 Color: 1

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 13346 Color: 3
Size: 2940 Color: 1

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 13890 Color: 4
Size: 2396 Color: 1

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11436 Color: 1
Size: 4529 Color: 4
Size: 320 Color: 4

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 2
Size: 2888 Color: 0
Size: 234 Color: 4

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 13551 Color: 2
Size: 2734 Color: 4

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 0
Size: 2051 Color: 4

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 4
Size: 5000 Color: 0
Size: 512 Color: 2

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 10835 Color: 3
Size: 5161 Color: 4
Size: 288 Color: 1

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 2
Size: 4036 Color: 0
Size: 1152 Color: 2

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 12698 Color: 3
Size: 3048 Color: 0
Size: 538 Color: 4

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 2
Size: 2644 Color: 4
Size: 624 Color: 0

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 14362 Color: 4
Size: 1922 Color: 3

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 3
Size: 1684 Color: 1

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 4
Size: 2201 Color: 3

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 10520 Color: 2
Size: 5410 Color: 0
Size: 352 Color: 1

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 10438 Color: 3
Size: 5844 Color: 4

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 3
Size: 2674 Color: 2
Size: 1604 Color: 1

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 0
Size: 2548 Color: 4
Size: 1624 Color: 1

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 2
Size: 3560 Color: 0
Size: 288 Color: 1

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 13590 Color: 4
Size: 2564 Color: 0
Size: 128 Color: 4

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 0
Size: 2402 Color: 1

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 14440 Color: 3
Size: 1842 Color: 4

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 4
Size: 1714 Color: 1
Size: 32 Color: 4

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 0
Size: 6764 Color: 2
Size: 264 Color: 1

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 9396 Color: 3
Size: 5901 Color: 4
Size: 984 Color: 0

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 13847 Color: 3
Size: 2434 Color: 4

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 3
Size: 4202 Color: 4

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 12764 Color: 2
Size: 3144 Color: 0
Size: 372 Color: 1

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13314 Color: 4
Size: 2966 Color: 2

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 14194 Color: 4
Size: 2086 Color: 3

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 0
Size: 1628 Color: 1
Size: 8 Color: 3

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 12797 Color: 1
Size: 3482 Color: 3

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 8152 Color: 3
Size: 6782 Color: 2
Size: 1344 Color: 1

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 10095 Color: 2
Size: 5863 Color: 0
Size: 320 Color: 3

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 10865 Color: 4
Size: 4545 Color: 0
Size: 868 Color: 1

Bin 123: 10 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 0
Size: 3510 Color: 2
Size: 584 Color: 3

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 2
Size: 3138 Color: 3

Bin 125: 11 of cap free
Amount of items: 9
Items: 
Size: 8145 Color: 3
Size: 1352 Color: 4
Size: 1344 Color: 4
Size: 1312 Color: 4
Size: 1208 Color: 3
Size: 1008 Color: 1
Size: 804 Color: 0
Size: 800 Color: 0
Size: 304 Color: 1

Bin 126: 11 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 0
Size: 3441 Color: 4
Size: 360 Color: 3

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 13829 Color: 3
Size: 2448 Color: 1

Bin 128: 11 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1669 Color: 2
Size: 76 Color: 0

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 8180 Color: 3
Size: 8096 Color: 2

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 1
Size: 5844 Color: 4
Size: 360 Color: 4

Bin 131: 12 of cap free
Amount of items: 4
Items: 
Size: 11410 Color: 1
Size: 2414 Color: 3
Size: 1612 Color: 3
Size: 840 Color: 4

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 12730 Color: 2
Size: 2022 Color: 0
Size: 1524 Color: 1

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 1
Size: 2344 Color: 0

Bin 134: 13 of cap free
Amount of items: 6
Items: 
Size: 8146 Color: 3
Size: 2628 Color: 2
Size: 2497 Color: 2
Size: 1372 Color: 4
Size: 1352 Color: 0
Size: 280 Color: 1

Bin 135: 14 of cap free
Amount of items: 19
Items: 
Size: 1208 Color: 3
Size: 1144 Color: 4
Size: 1144 Color: 3
Size: 1088 Color: 3
Size: 1032 Color: 3
Size: 984 Color: 1
Size: 960 Color: 1
Size: 920 Color: 4
Size: 912 Color: 3
Size: 912 Color: 3
Size: 902 Color: 3
Size: 896 Color: 4
Size: 800 Color: 1
Size: 768 Color: 0
Size: 720 Color: 1
Size: 592 Color: 0
Size: 544 Color: 0
Size: 424 Color: 0
Size: 324 Color: 2

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 2
Size: 3879 Color: 2
Size: 672 Color: 1

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 14460 Color: 0
Size: 1814 Color: 1

Bin 138: 15 of cap free
Amount of items: 3
Items: 
Size: 9209 Color: 4
Size: 6600 Color: 0
Size: 464 Color: 3

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 13627 Color: 1
Size: 2646 Color: 0

Bin 140: 17 of cap free
Amount of items: 3
Items: 
Size: 10909 Color: 1
Size: 5010 Color: 3
Size: 352 Color: 3

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 1
Size: 2066 Color: 0
Size: 96 Color: 4

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 14268 Color: 0
Size: 2002 Color: 4

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 13055 Color: 3
Size: 3214 Color: 2

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 2244 Color: 0

Bin 145: 23 of cap free
Amount of items: 4
Items: 
Size: 10756 Color: 2
Size: 3469 Color: 0
Size: 1544 Color: 3
Size: 496 Color: 1

Bin 146: 23 of cap free
Amount of items: 2
Items: 
Size: 11914 Color: 4
Size: 4351 Color: 0

Bin 147: 23 of cap free
Amount of items: 2
Items: 
Size: 14494 Color: 0
Size: 1771 Color: 4

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 12197 Color: 4
Size: 4066 Color: 0

Bin 149: 25 of cap free
Amount of items: 2
Items: 
Size: 12979 Color: 2
Size: 3284 Color: 4

Bin 150: 25 of cap free
Amount of items: 2
Items: 
Size: 14431 Color: 1
Size: 1832 Color: 3

Bin 151: 26 of cap free
Amount of items: 3
Items: 
Size: 9034 Color: 0
Size: 5812 Color: 0
Size: 1416 Color: 1

Bin 152: 26 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 1
Size: 2122 Color: 3

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 14620 Color: 1
Size: 1642 Color: 0

Bin 154: 28 of cap free
Amount of items: 3
Items: 
Size: 8992 Color: 0
Size: 6788 Color: 2
Size: 480 Color: 1

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 13490 Color: 3
Size: 2770 Color: 4

Bin 156: 29 of cap free
Amount of items: 2
Items: 
Size: 13654 Color: 3
Size: 2605 Color: 1

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 4
Size: 2454 Color: 3

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 14376 Color: 0
Size: 1882 Color: 4

Bin 159: 31 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 3
Size: 3477 Color: 1

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 10806 Color: 4
Size: 5448 Color: 1

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 3
Size: 2568 Color: 4

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 3
Size: 2856 Color: 2

Bin 163: 36 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 0
Size: 5055 Color: 1
Size: 160 Color: 0

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 0
Size: 4612 Color: 1

Bin 165: 37 of cap free
Amount of items: 2
Items: 
Size: 14255 Color: 0
Size: 1996 Color: 3

Bin 166: 42 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 0
Size: 2759 Color: 2

Bin 167: 43 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 1
Size: 3069 Color: 4

Bin 168: 44 of cap free
Amount of items: 3
Items: 
Size: 12520 Color: 0
Size: 3564 Color: 4
Size: 160 Color: 0

Bin 169: 49 of cap free
Amount of items: 3
Items: 
Size: 12634 Color: 2
Size: 2911 Color: 1
Size: 694 Color: 0

Bin 170: 50 of cap free
Amount of items: 2
Items: 
Size: 13960 Color: 0
Size: 2278 Color: 1

Bin 171: 52 of cap free
Amount of items: 2
Items: 
Size: 10504 Color: 4
Size: 5732 Color: 3

Bin 172: 54 of cap free
Amount of items: 2
Items: 
Size: 13114 Color: 1
Size: 3120 Color: 3

Bin 173: 56 of cap free
Amount of items: 2
Items: 
Size: 13742 Color: 4
Size: 2490 Color: 0

Bin 174: 59 of cap free
Amount of items: 2
Items: 
Size: 10133 Color: 0
Size: 6096 Color: 1

Bin 175: 63 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 3
Size: 4521 Color: 2
Size: 1364 Color: 0

Bin 176: 64 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 4
Size: 4200 Color: 0

Bin 177: 68 of cap free
Amount of items: 2
Items: 
Size: 11650 Color: 2
Size: 4570 Color: 4

Bin 178: 68 of cap free
Amount of items: 2
Items: 
Size: 12648 Color: 2
Size: 3572 Color: 3

Bin 179: 70 of cap free
Amount of items: 2
Items: 
Size: 12920 Color: 1
Size: 3298 Color: 2

Bin 180: 70 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 4
Size: 2994 Color: 1

Bin 181: 72 of cap free
Amount of items: 2
Items: 
Size: 13900 Color: 4
Size: 2316 Color: 0

Bin 182: 73 of cap free
Amount of items: 2
Items: 
Size: 12169 Color: 0
Size: 4046 Color: 2

Bin 183: 76 of cap free
Amount of items: 2
Items: 
Size: 12745 Color: 2
Size: 3467 Color: 4

Bin 184: 80 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 1
Size: 2924 Color: 2

Bin 185: 88 of cap free
Amount of items: 2
Items: 
Size: 11069 Color: 2
Size: 5131 Color: 1

Bin 186: 88 of cap free
Amount of items: 2
Items: 
Size: 14124 Color: 1
Size: 2076 Color: 4

Bin 187: 90 of cap free
Amount of items: 2
Items: 
Size: 9030 Color: 4
Size: 7168 Color: 3

Bin 188: 94 of cap free
Amount of items: 2
Items: 
Size: 11246 Color: 2
Size: 4948 Color: 4

Bin 189: 98 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 0
Size: 6046 Color: 4
Size: 840 Color: 1

Bin 190: 101 of cap free
Amount of items: 3
Items: 
Size: 8376 Color: 3
Size: 6787 Color: 0
Size: 1024 Color: 1

Bin 191: 146 of cap free
Amount of items: 2
Items: 
Size: 10378 Color: 3
Size: 5764 Color: 1

Bin 192: 151 of cap free
Amount of items: 2
Items: 
Size: 11635 Color: 0
Size: 4502 Color: 1

Bin 193: 190 of cap free
Amount of items: 3
Items: 
Size: 8147 Color: 3
Size: 4146 Color: 2
Size: 3805 Color: 2

Bin 194: 200 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 2
Size: 6772 Color: 3

Bin 195: 202 of cap free
Amount of items: 3
Items: 
Size: 8164 Color: 2
Size: 6786 Color: 0
Size: 1136 Color: 1

Bin 196: 212 of cap free
Amount of items: 2
Items: 
Size: 9284 Color: 1
Size: 6792 Color: 3

Bin 197: 227 of cap free
Amount of items: 2
Items: 
Size: 9276 Color: 1
Size: 6785 Color: 3

Bin 198: 250 of cap free
Amount of items: 32
Items: 
Size: 880 Color: 3
Size: 820 Color: 2
Size: 772 Color: 4
Size: 704 Color: 1
Size: 700 Color: 1
Size: 656 Color: 1
Size: 624 Color: 3
Size: 592 Color: 4
Size: 584 Color: 1
Size: 576 Color: 2
Size: 552 Color: 1
Size: 550 Color: 3
Size: 548 Color: 1
Size: 532 Color: 1
Size: 464 Color: 2
Size: 456 Color: 2
Size: 442 Color: 4
Size: 412 Color: 0
Size: 408 Color: 4
Size: 408 Color: 4
Size: 408 Color: 1
Size: 408 Color: 0
Size: 406 Color: 1
Size: 400 Color: 2
Size: 392 Color: 4
Size: 392 Color: 4
Size: 392 Color: 2
Size: 336 Color: 0
Size: 320 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 296 Color: 0

Bin 199: 11844 of cap free
Amount of items: 15
Items: 
Size: 360 Color: 4
Size: 352 Color: 2
Size: 336 Color: 3
Size: 324 Color: 4
Size: 320 Color: 1
Size: 304 Color: 3
Size: 304 Color: 2
Size: 272 Color: 4
Size: 272 Color: 3
Size: 272 Color: 2
Size: 272 Color: 2
Size: 272 Color: 0
Size: 272 Color: 0
Size: 256 Color: 3
Size: 256 Color: 0

Total size: 3225024
Total free space: 16288

