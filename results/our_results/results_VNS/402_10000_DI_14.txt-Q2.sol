Capicity Bin: 8344
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 27
Items: 
Size: 400 Color: 0
Size: 398 Color: 0
Size: 396 Color: 0
Size: 384 Color: 0
Size: 380 Color: 0
Size: 362 Color: 1
Size: 360 Color: 0
Size: 358 Color: 0
Size: 356 Color: 0
Size: 320 Color: 1
Size: 320 Color: 0
Size: 318 Color: 1
Size: 316 Color: 1
Size: 300 Color: 0
Size: 292 Color: 1
Size: 282 Color: 1
Size: 280 Color: 1
Size: 280 Color: 0
Size: 272 Color: 1
Size: 272 Color: 0
Size: 264 Color: 1
Size: 260 Color: 1
Size: 260 Color: 1
Size: 256 Color: 0
Size: 238 Color: 1
Size: 236 Color: 1
Size: 184 Color: 0

Bin 2: 0 of cap free
Amount of items: 20
Items: 
Size: 520 Color: 0
Size: 512 Color: 0
Size: 498 Color: 1
Size: 496 Color: 0
Size: 466 Color: 0
Size: 462 Color: 0
Size: 454 Color: 1
Size: 452 Color: 0
Size: 452 Color: 0
Size: 448 Color: 0
Size: 424 Color: 1
Size: 406 Color: 0
Size: 402 Color: 1
Size: 400 Color: 1
Size: 388 Color: 1
Size: 388 Color: 1
Size: 368 Color: 1
Size: 364 Color: 1
Size: 252 Color: 0
Size: 192 Color: 1

Bin 3: 0 of cap free
Amount of items: 9
Items: 
Size: 4174 Color: 1
Size: 692 Color: 0
Size: 692 Color: 0
Size: 664 Color: 0
Size: 592 Color: 1
Size: 584 Color: 0
Size: 516 Color: 1
Size: 222 Color: 0
Size: 208 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4201 Color: 1
Size: 3453 Color: 1
Size: 690 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4850 Color: 1
Size: 2914 Color: 1
Size: 580 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5056 Color: 1
Size: 2744 Color: 0
Size: 544 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 1
Size: 2364 Color: 1
Size: 472 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5881 Color: 1
Size: 1633 Color: 0
Size: 830 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 1
Size: 1826 Color: 0
Size: 522 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 1
Size: 1612 Color: 0
Size: 96 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 0
Size: 1371 Color: 1
Size: 330 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 1
Size: 1402 Color: 1
Size: 280 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6826 Color: 1
Size: 1182 Color: 0
Size: 336 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 1
Size: 1208 Color: 1
Size: 244 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 1
Size: 1153 Color: 0
Size: 230 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 1
Size: 536 Color: 0
Size: 458 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7375 Color: 0
Size: 769 Color: 0
Size: 200 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 1
Size: 764 Color: 0
Size: 152 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 5181 Color: 1
Size: 2962 Color: 0
Size: 200 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 0
Size: 2500 Color: 1
Size: 274 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 1
Size: 1601 Color: 0
Size: 978 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 1
Size: 2267 Color: 1
Size: 144 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 5921 Color: 0
Size: 2242 Color: 0
Size: 180 Color: 1

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 6010 Color: 0
Size: 2333 Color: 1

Bin 25: 1 of cap free
Amount of items: 4
Items: 
Size: 6154 Color: 0
Size: 2001 Color: 1
Size: 148 Color: 1
Size: 40 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 0
Size: 1806 Color: 0
Size: 136 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 0
Size: 1574 Color: 0
Size: 208 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 0
Size: 1406 Color: 1
Size: 120 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 0
Size: 925 Color: 0
Size: 588 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6857 Color: 0
Size: 1306 Color: 1
Size: 180 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 1
Size: 986 Color: 0
Size: 372 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 1
Size: 1121 Color: 0
Size: 226 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 1
Size: 688 Color: 0
Size: 512 Color: 1

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 7314 Color: 1
Size: 1029 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 7305 Color: 0
Size: 802 Color: 1
Size: 236 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 1
Size: 3462 Color: 1
Size: 84 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 1
Size: 3006 Color: 0
Size: 268 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 0
Size: 2906 Color: 1
Size: 312 Color: 1

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 5226 Color: 1
Size: 2964 Color: 0
Size: 152 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 5945 Color: 1
Size: 1987 Color: 0
Size: 410 Color: 1

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 0
Size: 2012 Color: 0
Size: 240 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 0
Size: 1964 Color: 1
Size: 254 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 1
Size: 2021 Color: 0
Size: 160 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 6361 Color: 1
Size: 1821 Color: 0
Size: 160 Color: 1

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 7041 Color: 1
Size: 1301 Color: 0

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 4177 Color: 0
Size: 4020 Color: 0
Size: 144 Color: 1

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 4774 Color: 0
Size: 3403 Color: 1
Size: 164 Color: 0

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 4794 Color: 0
Size: 2967 Color: 1
Size: 580 Color: 0

Bin 49: 3 of cap free
Amount of items: 2
Items: 
Size: 6185 Color: 1
Size: 2156 Color: 0

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 0
Size: 1133 Color: 0
Size: 804 Color: 1

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 1
Size: 1032 Color: 1
Size: 526 Color: 0

Bin 52: 3 of cap free
Amount of items: 2
Items: 
Size: 7025 Color: 0
Size: 1316 Color: 1

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 0
Size: 3470 Color: 1
Size: 680 Color: 0

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 5585 Color: 0
Size: 2603 Color: 1
Size: 152 Color: 0

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 6426 Color: 0
Size: 1914 Color: 1

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 6458 Color: 0
Size: 1882 Color: 1

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 6873 Color: 1
Size: 1467 Color: 0

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 6921 Color: 1
Size: 1419 Color: 0

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 6950 Color: 1
Size: 1390 Color: 0

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 0
Size: 1062 Color: 1
Size: 128 Color: 0

Bin 61: 5 of cap free
Amount of items: 5
Items: 
Size: 4173 Color: 0
Size: 2281 Color: 1
Size: 1587 Color: 0
Size: 246 Color: 1
Size: 52 Color: 0

Bin 62: 5 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 0
Size: 3673 Color: 0
Size: 326 Color: 1

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 4890 Color: 0
Size: 3001 Color: 0
Size: 448 Color: 1

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 5545 Color: 1
Size: 2582 Color: 0
Size: 212 Color: 1

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 7177 Color: 1
Size: 1162 Color: 0

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7263 Color: 1
Size: 1076 Color: 0

Bin 67: 6 of cap free
Amount of items: 3
Items: 
Size: 5221 Color: 0
Size: 2981 Color: 1
Size: 136 Color: 0

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7494 Color: 0
Size: 844 Color: 1

Bin 69: 7 of cap free
Amount of items: 2
Items: 
Size: 6284 Color: 1
Size: 2053 Color: 0

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 6716 Color: 0
Size: 1621 Color: 1

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 7250 Color: 1
Size: 1087 Color: 0

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 7074 Color: 0
Size: 1262 Color: 1

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 7235 Color: 0
Size: 1101 Color: 1

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 7285 Color: 0
Size: 1051 Color: 1

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 7388 Color: 0
Size: 947 Color: 1

Bin 76: 9 of cap free
Amount of items: 2
Items: 
Size: 7434 Color: 0
Size: 901 Color: 1

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 7468 Color: 1
Size: 867 Color: 0

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 4198 Color: 0
Size: 4136 Color: 1

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 6050 Color: 1
Size: 1962 Color: 1
Size: 322 Color: 0

Bin 80: 10 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 1
Size: 1284 Color: 0

Bin 81: 10 of cap free
Amount of items: 4
Items: 
Size: 7382 Color: 0
Size: 862 Color: 1
Size: 64 Color: 1
Size: 26 Color: 0

Bin 82: 11 of cap free
Amount of items: 2
Items: 
Size: 7332 Color: 1
Size: 1001 Color: 0

Bin 83: 12 of cap free
Amount of items: 2
Items: 
Size: 4668 Color: 0
Size: 3664 Color: 1

Bin 84: 12 of cap free
Amount of items: 3
Items: 
Size: 6644 Color: 0
Size: 1584 Color: 1
Size: 104 Color: 0

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 6664 Color: 1
Size: 1668 Color: 0

Bin 86: 12 of cap free
Amount of items: 2
Items: 
Size: 7274 Color: 1
Size: 1058 Color: 0

Bin 87: 13 of cap free
Amount of items: 2
Items: 
Size: 6385 Color: 0
Size: 1946 Color: 1

Bin 88: 13 of cap free
Amount of items: 3
Items: 
Size: 7466 Color: 1
Size: 809 Color: 0
Size: 56 Color: 0

Bin 89: 17 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 0
Size: 3340 Color: 1
Size: 218 Color: 1

Bin 90: 17 of cap free
Amount of items: 3
Items: 
Size: 6412 Color: 0
Size: 1227 Color: 1
Size: 688 Color: 1

Bin 91: 17 of cap free
Amount of items: 2
Items: 
Size: 6707 Color: 0
Size: 1620 Color: 1

Bin 92: 17 of cap free
Amount of items: 2
Items: 
Size: 7345 Color: 0
Size: 982 Color: 1

Bin 93: 18 of cap free
Amount of items: 3
Items: 
Size: 4261 Color: 1
Size: 3473 Color: 1
Size: 592 Color: 0

Bin 94: 18 of cap free
Amount of items: 2
Items: 
Size: 5348 Color: 1
Size: 2978 Color: 0

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 7085 Color: 0
Size: 1241 Color: 1

Bin 96: 19 of cap free
Amount of items: 3
Items: 
Size: 4785 Color: 1
Size: 3068 Color: 0
Size: 472 Color: 0

Bin 97: 21 of cap free
Amount of items: 2
Items: 
Size: 7111 Color: 1
Size: 1212 Color: 0

Bin 98: 22 of cap free
Amount of items: 6
Items: 
Size: 4182 Color: 1
Size: 1036 Color: 0
Size: 924 Color: 0
Size: 734 Color: 0
Size: 732 Color: 1
Size: 714 Color: 1

Bin 99: 24 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 1
Size: 3468 Color: 1
Size: 664 Color: 0

Bin 100: 24 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 1
Size: 1653 Color: 0
Size: 762 Color: 0

Bin 101: 26 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 1
Size: 1082 Color: 0

Bin 102: 27 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 0
Size: 1545 Color: 1

Bin 103: 28 of cap free
Amount of items: 2
Items: 
Size: 4858 Color: 1
Size: 3458 Color: 0

Bin 104: 28 of cap free
Amount of items: 2
Items: 
Size: 7044 Color: 0
Size: 1272 Color: 1

Bin 105: 29 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 0
Size: 1141 Color: 1

Bin 106: 35 of cap free
Amount of items: 2
Items: 
Size: 6585 Color: 1
Size: 1724 Color: 0

Bin 107: 36 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 0
Size: 1650 Color: 1

Bin 108: 37 of cap free
Amount of items: 2
Items: 
Size: 5994 Color: 0
Size: 2313 Color: 1

Bin 109: 38 of cap free
Amount of items: 2
Items: 
Size: 7423 Color: 0
Size: 883 Color: 1

Bin 110: 43 of cap free
Amount of items: 2
Items: 
Size: 5205 Color: 1
Size: 3096 Color: 0

Bin 111: 44 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 0
Size: 1128 Color: 1

Bin 112: 49 of cap free
Amount of items: 2
Items: 
Size: 7108 Color: 1
Size: 1187 Color: 0

Bin 113: 51 of cap free
Amount of items: 2
Items: 
Size: 5609 Color: 0
Size: 2684 Color: 1

Bin 114: 51 of cap free
Amount of items: 2
Items: 
Size: 6699 Color: 0
Size: 1594 Color: 1

Bin 115: 52 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 1
Size: 2114 Color: 0

Bin 116: 53 of cap free
Amount of items: 2
Items: 
Size: 5654 Color: 1
Size: 2637 Color: 0

Bin 117: 53 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 1
Size: 1365 Color: 0

Bin 118: 64 of cap free
Amount of items: 7
Items: 
Size: 4180 Color: 1
Size: 914 Color: 0
Size: 694 Color: 0
Size: 692 Color: 0
Size: 608 Color: 1
Size: 598 Color: 1
Size: 594 Color: 1

Bin 119: 67 of cap free
Amount of items: 2
Items: 
Size: 6425 Color: 1
Size: 1852 Color: 0

Bin 120: 70 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 0
Size: 1420 Color: 1

Bin 121: 70 of cap free
Amount of items: 2
Items: 
Size: 7001 Color: 1
Size: 1273 Color: 0

Bin 122: 73 of cap free
Amount of items: 2
Items: 
Size: 5970 Color: 1
Size: 2301 Color: 0

Bin 123: 84 of cap free
Amount of items: 2
Items: 
Size: 6896 Color: 1
Size: 1364 Color: 0

Bin 124: 90 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 0
Size: 3004 Color: 1

Bin 125: 90 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 0
Size: 1084 Color: 1

Bin 126: 93 of cap free
Amount of items: 2
Items: 
Size: 6450 Color: 0
Size: 1801 Color: 1

Bin 127: 97 of cap free
Amount of items: 2
Items: 
Size: 5630 Color: 1
Size: 2617 Color: 0

Bin 128: 110 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 0
Size: 2033 Color: 1

Bin 129: 116 of cap free
Amount of items: 2
Items: 
Size: 6441 Color: 0
Size: 1787 Color: 1

Bin 130: 117 of cap free
Amount of items: 2
Items: 
Size: 5625 Color: 1
Size: 2602 Color: 0

Bin 131: 121 of cap free
Amount of items: 2
Items: 
Size: 4745 Color: 0
Size: 3478 Color: 1

Bin 132: 121 of cap free
Amount of items: 2
Items: 
Size: 5961 Color: 1
Size: 2262 Color: 0

Bin 133: 5720 of cap free
Amount of items: 14
Items: 
Size: 240 Color: 0
Size: 232 Color: 0
Size: 216 Color: 0
Size: 216 Color: 0
Size: 204 Color: 1
Size: 192 Color: 1
Size: 184 Color: 1
Size: 184 Color: 1
Size: 176 Color: 0
Size: 172 Color: 1
Size: 168 Color: 0
Size: 168 Color: 0
Size: 144 Color: 1
Size: 128 Color: 1

Total size: 1101408
Total free space: 8344

