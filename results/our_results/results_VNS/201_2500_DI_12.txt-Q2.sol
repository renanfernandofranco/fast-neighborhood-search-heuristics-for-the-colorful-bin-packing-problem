Capicity Bin: 1864
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 941 Color: 1
Size: 771 Color: 0
Size: 152 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 0
Size: 694 Color: 0
Size: 52 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1155 Color: 0
Size: 673 Color: 0
Size: 36 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1297 Color: 0
Size: 517 Color: 0
Size: 50 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 1
Size: 442 Color: 1
Size: 84 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1441 Color: 1
Size: 353 Color: 1
Size: 70 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 0
Size: 358 Color: 1
Size: 68 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1454 Color: 1
Size: 374 Color: 1
Size: 36 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 1
Size: 208 Color: 0
Size: 154 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1513 Color: 1
Size: 283 Color: 1
Size: 68 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 237 Color: 0
Size: 84 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 0
Size: 230 Color: 0
Size: 64 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 0
Size: 163 Color: 0
Size: 96 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 0
Size: 220 Color: 1
Size: 30 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 0
Size: 181 Color: 1
Size: 34 Color: 0

Bin 16: 1 of cap free
Amount of items: 4
Items: 
Size: 934 Color: 0
Size: 667 Color: 0
Size: 182 Color: 1
Size: 80 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1190 Color: 0
Size: 621 Color: 1
Size: 52 Color: 1

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1449 Color: 1
Size: 414 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 1
Size: 241 Color: 0
Size: 72 Color: 0

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1561 Color: 0
Size: 302 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 1
Size: 170 Color: 0
Size: 54 Color: 0

Bin 22: 2 of cap free
Amount of items: 23
Items: 
Size: 136 Color: 0
Size: 134 Color: 1
Size: 132 Color: 1
Size: 112 Color: 1
Size: 108 Color: 0
Size: 100 Color: 0
Size: 96 Color: 0
Size: 92 Color: 1
Size: 92 Color: 1
Size: 82 Color: 1
Size: 80 Color: 1
Size: 76 Color: 1
Size: 68 Color: 0
Size: 64 Color: 1
Size: 60 Color: 1
Size: 60 Color: 1
Size: 58 Color: 1
Size: 58 Color: 0
Size: 56 Color: 0
Size: 52 Color: 0
Size: 50 Color: 0
Size: 48 Color: 0
Size: 48 Color: 0

Bin 23: 2 of cap free
Amount of items: 7
Items: 
Size: 933 Color: 0
Size: 282 Color: 1
Size: 269 Color: 1
Size: 154 Color: 1
Size: 152 Color: 0
Size: 40 Color: 1
Size: 32 Color: 0

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 0
Size: 467 Color: 1
Size: 40 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 0
Size: 421 Color: 1
Size: 40 Color: 0

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1475 Color: 0
Size: 387 Color: 1

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1553 Color: 1
Size: 309 Color: 0

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1569 Color: 1
Size: 293 Color: 0

Bin 29: 2 of cap free
Amount of items: 4
Items: 
Size: 1669 Color: 1
Size: 181 Color: 0
Size: 8 Color: 0
Size: 4 Color: 1

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 1
Size: 189 Color: 0

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1279 Color: 1
Size: 514 Color: 0
Size: 68 Color: 1

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 1
Size: 211 Color: 0

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 0
Size: 210 Color: 1
Size: 4 Color: 1

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 0
Size: 199 Color: 1

Bin 35: 4 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 0
Size: 425 Color: 0
Size: 272 Color: 1

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1530 Color: 0
Size: 330 Color: 1

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 0
Size: 325 Color: 1

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1613 Color: 0
Size: 247 Color: 1

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1655 Color: 0
Size: 197 Color: 1
Size: 8 Color: 1

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1237 Color: 0
Size: 622 Color: 1

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 1370 Color: 0
Size: 489 Color: 1

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1597 Color: 0
Size: 262 Color: 1

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 1
Size: 261 Color: 0

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 1
Size: 201 Color: 0

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 0
Size: 773 Color: 1
Size: 50 Color: 1

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 1
Size: 208 Color: 0
Size: 124 Color: 0

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1604 Color: 1
Size: 253 Color: 0

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1635 Color: 1
Size: 222 Color: 0

Bin 49: 10 of cap free
Amount of items: 2
Items: 
Size: 1305 Color: 0
Size: 549 Color: 1

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 0
Size: 384 Color: 1

Bin 51: 12 of cap free
Amount of items: 3
Items: 
Size: 1172 Color: 0
Size: 562 Color: 0
Size: 118 Color: 1

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 246 Color: 0
Size: 16 Color: 1

Bin 53: 13 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 0
Size: 223 Color: 1
Size: 22 Color: 1

Bin 54: 14 of cap free
Amount of items: 3
Items: 
Size: 937 Color: 0
Size: 752 Color: 0
Size: 161 Color: 1

Bin 55: 14 of cap free
Amount of items: 2
Items: 
Size: 1418 Color: 1
Size: 432 Color: 0

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1575 Color: 1
Size: 275 Color: 0

Bin 57: 16 of cap free
Amount of items: 3
Items: 
Size: 1288 Color: 0
Size: 342 Color: 0
Size: 218 Color: 1

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1497 Color: 1
Size: 347 Color: 0

Bin 59: 21 of cap free
Amount of items: 2
Items: 
Size: 1065 Color: 0
Size: 778 Color: 1

Bin 60: 23 of cap free
Amount of items: 2
Items: 
Size: 1250 Color: 0
Size: 591 Color: 1

Bin 61: 29 of cap free
Amount of items: 2
Items: 
Size: 1207 Color: 1
Size: 628 Color: 0

Bin 62: 29 of cap free
Amount of items: 2
Items: 
Size: 1361 Color: 0
Size: 474 Color: 1

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1057 Color: 0
Size: 777 Color: 1

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 0
Size: 288 Color: 1

Bin 65: 35 of cap free
Amount of items: 2
Items: 
Size: 1298 Color: 0
Size: 531 Color: 1

Bin 66: 1420 of cap free
Amount of items: 11
Items: 
Size: 48 Color: 1
Size: 48 Color: 1
Size: 44 Color: 1
Size: 44 Color: 0
Size: 44 Color: 0
Size: 40 Color: 1
Size: 40 Color: 1
Size: 40 Color: 0
Size: 32 Color: 0
Size: 32 Color: 0
Size: 32 Color: 0

Total size: 121160
Total free space: 1864

