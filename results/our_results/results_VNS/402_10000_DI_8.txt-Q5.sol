Capicity Bin: 7760
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 3890 Color: 1
Size: 3222 Color: 3
Size: 496 Color: 4
Size: 152 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 3885 Color: 2
Size: 1876 Color: 3
Size: 1731 Color: 1
Size: 136 Color: 4
Size: 132 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 2
Size: 3224 Color: 3
Size: 640 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4328 Color: 2
Size: 3228 Color: 3
Size: 204 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 2
Size: 2776 Color: 3
Size: 208 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5297 Color: 3
Size: 2053 Color: 0
Size: 410 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5758 Color: 4
Size: 1726 Color: 3
Size: 276 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5832 Color: 0
Size: 1370 Color: 0
Size: 558 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 1
Size: 1596 Color: 4
Size: 312 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 1
Size: 1608 Color: 3
Size: 252 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6040 Color: 1
Size: 1556 Color: 0
Size: 164 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 2
Size: 1372 Color: 0
Size: 272 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 2
Size: 1060 Color: 0
Size: 544 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6215 Color: 2
Size: 989 Color: 1
Size: 556 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 3
Size: 1448 Color: 0
Size: 112 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 2
Size: 1284 Color: 4
Size: 256 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6249 Color: 1
Size: 1261 Color: 0
Size: 250 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 1
Size: 1302 Color: 0
Size: 188 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6383 Color: 2
Size: 733 Color: 2
Size: 644 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6344 Color: 3
Size: 968 Color: 0
Size: 448 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 0
Size: 702 Color: 1
Size: 484 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6593 Color: 1
Size: 971 Color: 0
Size: 196 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 3
Size: 646 Color: 4
Size: 484 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6646 Color: 1
Size: 756 Color: 4
Size: 358 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 1
Size: 928 Color: 0
Size: 172 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 0
Size: 881 Color: 1
Size: 192 Color: 3

Bin 27: 0 of cap free
Amount of items: 4
Items: 
Size: 6672 Color: 2
Size: 1008 Color: 4
Size: 64 Color: 1
Size: 16 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 1
Size: 904 Color: 4
Size: 176 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 1
Size: 690 Color: 0
Size: 376 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6730 Color: 2
Size: 874 Color: 0
Size: 156 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 2
Size: 640 Color: 4
Size: 376 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 1
Size: 556 Color: 0
Size: 442 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 1
Size: 816 Color: 0
Size: 160 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 4
Size: 771 Color: 0
Size: 152 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6851 Color: 0
Size: 759 Color: 1
Size: 150 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 0
Size: 731 Color: 4
Size: 144 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 2
Size: 640 Color: 0
Size: 248 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 0
Size: 560 Color: 4
Size: 282 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 0
Size: 678 Color: 3
Size: 132 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 4
Size: 400 Color: 0
Size: 392 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 2
Size: 552 Color: 0
Size: 224 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 3
Size: 2324 Color: 4
Size: 951 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4845 Color: 3
Size: 2774 Color: 4
Size: 140 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 4
Size: 1134 Color: 2
Size: 895 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5998 Color: 2
Size: 1593 Color: 0
Size: 168 Color: 4

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 5962 Color: 3
Size: 1797 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 0
Size: 1143 Color: 3
Size: 246 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6389 Color: 1
Size: 1192 Color: 2
Size: 178 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 0
Size: 1132 Color: 4
Size: 140 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6490 Color: 2
Size: 785 Color: 0
Size: 484 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 3
Size: 930 Color: 1
Size: 320 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6597 Color: 0
Size: 834 Color: 1
Size: 328 Color: 4

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6775 Color: 0
Size: 664 Color: 3
Size: 320 Color: 4

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 1
Size: 915 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6955 Color: 1
Size: 644 Color: 2
Size: 160 Color: 0

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6967 Color: 0
Size: 556 Color: 1
Size: 236 Color: 2

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 3
Size: 2802 Color: 1
Size: 480 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5064 Color: 4
Size: 2422 Color: 3
Size: 272 Color: 2

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 4
Size: 2308 Color: 1
Size: 248 Color: 0

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5214 Color: 3
Size: 2248 Color: 2
Size: 296 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 0
Size: 1882 Color: 3
Size: 376 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5502 Color: 4
Size: 1616 Color: 1
Size: 640 Color: 2

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 0
Size: 1502 Color: 0
Size: 664 Color: 2

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 6057 Color: 0
Size: 1701 Color: 2

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6902 Color: 4
Size: 856 Color: 1

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 3882 Color: 2
Size: 3231 Color: 2
Size: 644 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 3
Size: 2797 Color: 0
Size: 558 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 4838 Color: 0
Size: 2791 Color: 3
Size: 128 Color: 2

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5721 Color: 0
Size: 1572 Color: 0
Size: 464 Color: 2

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 5921 Color: 4
Size: 1612 Color: 0
Size: 224 Color: 1

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 6714 Color: 3
Size: 1043 Color: 2

Bin 72: 4 of cap free
Amount of items: 26
Items: 
Size: 718 Color: 0
Size: 662 Color: 0
Size: 448 Color: 2
Size: 424 Color: 2
Size: 424 Color: 2
Size: 352 Color: 4
Size: 304 Color: 4
Size: 304 Color: 4
Size: 276 Color: 1
Size: 272 Color: 4
Size: 258 Color: 4
Size: 256 Color: 4
Size: 256 Color: 1
Size: 256 Color: 0
Size: 240 Color: 3
Size: 228 Color: 1
Size: 228 Color: 1
Size: 228 Color: 1
Size: 228 Color: 1
Size: 224 Color: 2
Size: 212 Color: 0
Size: 208 Color: 0
Size: 192 Color: 3
Size: 192 Color: 0
Size: 184 Color: 2
Size: 182 Color: 2

Bin 73: 4 of cap free
Amount of items: 4
Items: 
Size: 3892 Color: 3
Size: 2732 Color: 0
Size: 924 Color: 4
Size: 208 Color: 1

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 4870 Color: 3
Size: 2740 Color: 0
Size: 146 Color: 1

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 3
Size: 2438 Color: 1
Size: 322 Color: 1

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 5360 Color: 3
Size: 2014 Color: 0
Size: 382 Color: 1

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 5516 Color: 2
Size: 2056 Color: 1
Size: 184 Color: 3

Bin 78: 4 of cap free
Amount of items: 4
Items: 
Size: 6663 Color: 3
Size: 1061 Color: 4
Size: 16 Color: 3
Size: 16 Color: 0

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 3
Size: 822 Color: 4

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 4
Size: 1289 Color: 2
Size: 64 Color: 4

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 4411 Color: 2
Size: 2799 Color: 3
Size: 544 Color: 0

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 5774 Color: 1
Size: 1980 Color: 3

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 1
Size: 764 Color: 4
Size: 24 Color: 2

Bin 84: 7 of cap free
Amount of items: 5
Items: 
Size: 3884 Color: 4
Size: 1658 Color: 1
Size: 1387 Color: 2
Size: 456 Color: 0
Size: 368 Color: 3

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 4423 Color: 2
Size: 3136 Color: 2
Size: 194 Color: 0

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 4
Size: 1200 Color: 3
Size: 288 Color: 2

Bin 87: 7 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 2
Size: 1146 Color: 1
Size: 32 Color: 3

Bin 88: 8 of cap free
Amount of items: 6
Items: 
Size: 3881 Color: 4
Size: 1162 Color: 0
Size: 864 Color: 3
Size: 862 Color: 3
Size: 671 Color: 1
Size: 312 Color: 2

Bin 89: 8 of cap free
Amount of items: 4
Items: 
Size: 3898 Color: 4
Size: 3226 Color: 0
Size: 494 Color: 2
Size: 134 Color: 2

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 3
Size: 1964 Color: 0
Size: 744 Color: 3

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 3
Size: 1597 Color: 2
Size: 338 Color: 0

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 4
Size: 2421 Color: 4
Size: 650 Color: 2

Bin 93: 9 of cap free
Amount of items: 3
Items: 
Size: 5742 Color: 3
Size: 1913 Color: 4
Size: 96 Color: 1

Bin 94: 9 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 2
Size: 1247 Color: 3
Size: 118 Color: 1

Bin 95: 9 of cap free
Amount of items: 2
Items: 
Size: 6703 Color: 3
Size: 1048 Color: 1

Bin 96: 9 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 1
Size: 942 Color: 2
Size: 50 Color: 1

Bin 97: 9 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 4
Size: 973 Color: 1

Bin 98: 9 of cap free
Amount of items: 2
Items: 
Size: 6819 Color: 3
Size: 932 Color: 1

Bin 99: 9 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 3
Size: 769 Color: 1

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 6688 Color: 3
Size: 1062 Color: 2

Bin 101: 10 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 4
Size: 890 Color: 3

Bin 102: 11 of cap free
Amount of items: 3
Items: 
Size: 5097 Color: 2
Size: 2488 Color: 3
Size: 164 Color: 0

Bin 103: 11 of cap free
Amount of items: 2
Items: 
Size: 6600 Color: 4
Size: 1149 Color: 3

Bin 104: 12 of cap free
Amount of items: 3
Items: 
Size: 4980 Color: 4
Size: 2608 Color: 0
Size: 160 Color: 2

Bin 105: 15 of cap free
Amount of items: 3
Items: 
Size: 5465 Color: 0
Size: 2120 Color: 3
Size: 160 Color: 2

Bin 106: 15 of cap free
Amount of items: 2
Items: 
Size: 5929 Color: 2
Size: 1816 Color: 3

Bin 107: 15 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 2
Size: 864 Color: 4

Bin 108: 17 of cap free
Amount of items: 2
Items: 
Size: 6322 Color: 3
Size: 1421 Color: 4

Bin 109: 20 of cap free
Amount of items: 3
Items: 
Size: 6097 Color: 4
Size: 1579 Color: 3
Size: 64 Color: 3

Bin 110: 20 of cap free
Amount of items: 2
Items: 
Size: 6202 Color: 3
Size: 1538 Color: 4

Bin 111: 21 of cap free
Amount of items: 2
Items: 
Size: 6118 Color: 2
Size: 1621 Color: 1

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 3
Size: 2268 Color: 2

Bin 113: 25 of cap free
Amount of items: 3
Items: 
Size: 4407 Color: 4
Size: 2872 Color: 1
Size: 456 Color: 0

Bin 114: 26 of cap free
Amount of items: 2
Items: 
Size: 6492 Color: 3
Size: 1242 Color: 2

Bin 115: 26 of cap free
Amount of items: 2
Items: 
Size: 6644 Color: 3
Size: 1090 Color: 0

Bin 116: 32 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 3
Size: 2324 Color: 4

Bin 117: 33 of cap free
Amount of items: 2
Items: 
Size: 5605 Color: 4
Size: 2122 Color: 3

Bin 118: 38 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 4
Size: 1894 Color: 1

Bin 119: 38 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 3
Size: 1202 Color: 2

Bin 120: 41 of cap free
Amount of items: 2
Items: 
Size: 5905 Color: 1
Size: 1814 Color: 3

Bin 121: 46 of cap free
Amount of items: 2
Items: 
Size: 4855 Color: 2
Size: 2859 Color: 0

Bin 122: 46 of cap free
Amount of items: 2
Items: 
Size: 5304 Color: 4
Size: 2410 Color: 2

Bin 123: 49 of cap free
Amount of items: 2
Items: 
Size: 5490 Color: 3
Size: 2221 Color: 1

Bin 124: 50 of cap free
Amount of items: 2
Items: 
Size: 5576 Color: 0
Size: 2134 Color: 4

Bin 125: 52 of cap free
Amount of items: 2
Items: 
Size: 6404 Color: 4
Size: 1304 Color: 2

Bin 126: 71 of cap free
Amount of items: 2
Items: 
Size: 5897 Color: 2
Size: 1792 Color: 3

Bin 127: 83 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 4
Size: 1884 Color: 0
Size: 821 Color: 1

Bin 128: 92 of cap free
Amount of items: 2
Items: 
Size: 4434 Color: 1
Size: 3234 Color: 2

Bin 129: 94 of cap free
Amount of items: 2
Items: 
Size: 4880 Color: 1
Size: 2786 Color: 2

Bin 130: 109 of cap free
Amount of items: 2
Items: 
Size: 4418 Color: 0
Size: 3233 Color: 1

Bin 131: 121 of cap free
Amount of items: 2
Items: 
Size: 4403 Color: 1
Size: 3236 Color: 3

Bin 132: 125 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 1
Size: 2781 Color: 2

Bin 133: 6124 of cap free
Amount of items: 10
Items: 
Size: 224 Color: 1
Size: 176 Color: 4
Size: 176 Color: 2
Size: 176 Color: 1
Size: 176 Color: 0
Size: 160 Color: 3
Size: 144 Color: 2
Size: 144 Color: 0
Size: 132 Color: 3
Size: 128 Color: 3

Total size: 1024320
Total free space: 7760

