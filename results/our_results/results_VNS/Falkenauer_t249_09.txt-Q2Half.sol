Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 275 Color: 1
Size: 274 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 324 Color: 1
Size: 297 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 312 Color: 1
Size: 264 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 350 Color: 1
Size: 269 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 1
Size: 260 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 267 Color: 1
Size: 255 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 326 Color: 1
Size: 260 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 251 Color: 0
Size: 255 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 274 Color: 1
Size: 256 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 319 Color: 1
Size: 287 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 1
Size: 252 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 289 Color: 0
Size: 275 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 302 Color: 1
Size: 302 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 295 Color: 1
Size: 268 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 299 Color: 1
Size: 255 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 305 Color: 1
Size: 257 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 356 Color: 1
Size: 269 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 332 Color: 1
Size: 296 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 364 Color: 1
Size: 270 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 292 Color: 1
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 273 Color: 1
Size: 267 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 291 Color: 1
Size: 260 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 275 Color: 1
Size: 255 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 349 Color: 1
Size: 263 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 275 Color: 1
Size: 262 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 353 Color: 1
Size: 271 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 369 Color: 1
Size: 262 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 271 Color: 1
Size: 252 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 309 Color: 1
Size: 274 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 315 Color: 1
Size: 258 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 361 Color: 1
Size: 262 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 308 Color: 1
Size: 252 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 312 Color: 1
Size: 252 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 323 Color: 0
Size: 297 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 327 Color: 1
Size: 258 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 288 Color: 1
Size: 287 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 295 Color: 1
Size: 294 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 329 Color: 1
Size: 273 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 365 Color: 1
Size: 270 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 328 Color: 1
Size: 295 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 312 Color: 1
Size: 264 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 302 Color: 1
Size: 264 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 268 Color: 1
Size: 262 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 289 Color: 1
Size: 256 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 339 Color: 1
Size: 292 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 291 Color: 1
Size: 251 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 294 Color: 0
Size: 289 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 259 Color: 1
Size: 250 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 345 Color: 1
Size: 258 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 298 Color: 1
Size: 267 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 283 Color: 0
Size: 273 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 268 Color: 1
Size: 266 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 301 Color: 1
Size: 273 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 257 Color: 1
Size: 255 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 261 Color: 0
Size: 259 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 303 Color: 1
Size: 286 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 279 Color: 1
Size: 252 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 1
Size: 250 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 333 Color: 1
Size: 285 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 260 Color: 1
Size: 253 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 335 Color: 1
Size: 281 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 314 Color: 1
Size: 275 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 282 Color: 0
Size: 356 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 303 Color: 1
Size: 251 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 357 Color: 1
Size: 257 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 280 Color: 1
Size: 263 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 280 Color: 1
Size: 261 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 303 Color: 1
Size: 259 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 340 Color: 1
Size: 305 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 307 Color: 1
Size: 274 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 271 Color: 1
Size: 269 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 291 Color: 1
Size: 269 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1
Size: 340 Color: 1
Size: 317 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 282 Color: 0
Size: 280 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 270 Color: 1
Size: 256 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 352 Color: 1
Size: 270 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 338 Color: 1
Size: 274 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 337 Color: 1
Size: 263 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 341 Color: 1
Size: 289 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 329 Color: 1
Size: 307 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 318 Color: 1
Size: 255 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 266 Color: 1
Size: 252 Color: 0

Total size: 83000
Total free space: 0

