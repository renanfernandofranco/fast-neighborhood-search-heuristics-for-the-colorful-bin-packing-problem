Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 4070 Color: 6
Size: 1221 Color: 2
Size: 859 Color: 16
Size: 793 Color: 18
Size: 755 Color: 16
Size: 272 Color: 1
Size: 166 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4368 Color: 7
Size: 3528 Color: 0
Size: 240 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 11
Size: 3389 Color: 0
Size: 150 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5460 Color: 19
Size: 2348 Color: 18
Size: 328 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5481 Color: 8
Size: 2501 Color: 6
Size: 154 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5674 Color: 10
Size: 2122 Color: 1
Size: 340 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 2
Size: 1314 Color: 12
Size: 676 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 13
Size: 987 Color: 14
Size: 676 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 17
Size: 1228 Color: 7
Size: 408 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 11
Size: 788 Color: 5
Size: 456 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6980 Color: 17
Size: 664 Color: 18
Size: 492 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7091 Color: 19
Size: 721 Color: 14
Size: 324 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 18
Size: 546 Color: 16
Size: 500 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7139 Color: 0
Size: 783 Color: 6
Size: 214 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7131 Color: 16
Size: 785 Color: 2
Size: 220 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 1
Size: 672 Color: 4
Size: 324 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7155 Color: 1
Size: 771 Color: 17
Size: 210 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 17
Size: 740 Color: 14
Size: 214 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 11
Size: 568 Color: 10
Size: 372 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7218 Color: 1
Size: 676 Color: 18
Size: 242 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 14
Size: 708 Color: 2
Size: 176 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7243 Color: 18
Size: 745 Color: 2
Size: 148 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 2
Size: 738 Color: 13
Size: 144 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 15
Size: 711 Color: 15
Size: 142 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 7
Size: 588 Color: 13
Size: 256 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7321 Color: 14
Size: 681 Color: 7
Size: 134 Color: 8

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5177 Color: 6
Size: 2790 Color: 12
Size: 168 Color: 7

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 5324 Color: 3
Size: 2811 Color: 2

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 5613 Color: 11
Size: 2522 Color: 18

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 4
Size: 2083 Color: 18
Size: 248 Color: 6

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 5894 Color: 9
Size: 2241 Color: 14

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6241 Color: 5
Size: 1662 Color: 11
Size: 232 Color: 17

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 0
Size: 1492 Color: 14
Size: 136 Color: 11

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 14
Size: 1395 Color: 15
Size: 140 Color: 5

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 18
Size: 1324 Color: 0
Size: 182 Color: 16

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6673 Color: 2
Size: 766 Color: 1
Size: 696 Color: 16

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 14
Size: 1148 Color: 18
Size: 170 Color: 4

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6937 Color: 7
Size: 1198 Color: 3

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 7171 Color: 8
Size: 964 Color: 2

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 7315 Color: 3
Size: 820 Color: 9

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 17
Size: 2898 Color: 8
Size: 368 Color: 14

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 6089 Color: 0
Size: 1909 Color: 13
Size: 136 Color: 2

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 16
Size: 1618 Color: 5
Size: 200 Color: 12

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 6394 Color: 14
Size: 1740 Color: 11

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 15
Size: 1108 Color: 10
Size: 448 Color: 11

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 6733 Color: 13
Size: 1401 Color: 8

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 6818 Color: 15
Size: 892 Color: 11
Size: 424 Color: 8

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6877 Color: 9
Size: 1257 Color: 5

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 6914 Color: 5
Size: 1002 Color: 0
Size: 218 Color: 18

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 7001 Color: 14
Size: 805 Color: 8
Size: 328 Color: 11

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 7010 Color: 16
Size: 1022 Color: 0
Size: 102 Color: 19

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 7231 Color: 17
Size: 903 Color: 9

Bin 53: 3 of cap free
Amount of items: 4
Items: 
Size: 4078 Color: 4
Size: 3033 Color: 3
Size: 866 Color: 15
Size: 156 Color: 17

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 5137 Color: 13
Size: 2580 Color: 2
Size: 416 Color: 5

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 0
Size: 1081 Color: 3
Size: 350 Color: 1

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 6838 Color: 10
Size: 1121 Color: 7
Size: 174 Color: 0

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 6845 Color: 15
Size: 1288 Color: 7

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 7211 Color: 5
Size: 922 Color: 8

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 7271 Color: 10
Size: 862 Color: 7

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 7278 Color: 3
Size: 831 Color: 16
Size: 24 Color: 9

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 7294 Color: 10
Size: 839 Color: 15

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 5447 Color: 11
Size: 2685 Color: 12

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 16
Size: 1622 Color: 8
Size: 162 Color: 18

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 14
Size: 1168 Color: 13
Size: 40 Color: 12

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 17
Size: 1102 Color: 16

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 5590 Color: 8
Size: 2375 Color: 3
Size: 166 Color: 15

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 5847 Color: 4
Size: 2284 Color: 14

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6953 Color: 19
Size: 1178 Color: 2

Bin 69: 6 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 7
Size: 946 Color: 4
Size: 28 Color: 8

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 13
Size: 3356 Color: 3
Size: 216 Color: 1

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 16
Size: 1061 Color: 9

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 7310 Color: 5
Size: 819 Color: 6

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 4452 Color: 12
Size: 3382 Color: 0
Size: 294 Color: 7

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 4638 Color: 2
Size: 3378 Color: 11
Size: 112 Color: 4

Bin 75: 8 of cap free
Amount of items: 3
Items: 
Size: 5226 Color: 0
Size: 2414 Color: 18
Size: 488 Color: 7

Bin 76: 8 of cap free
Amount of items: 3
Items: 
Size: 5708 Color: 11
Size: 2236 Color: 18
Size: 184 Color: 15

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 6407 Color: 12
Size: 1721 Color: 6

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6547 Color: 5
Size: 1581 Color: 17

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 18
Size: 1572 Color: 12

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 2
Size: 1364 Color: 17

Bin 81: 9 of cap free
Amount of items: 3
Items: 
Size: 6174 Color: 10
Size: 1753 Color: 18
Size: 200 Color: 15

Bin 82: 11 of cap free
Amount of items: 2
Items: 
Size: 6281 Color: 9
Size: 1844 Color: 5

Bin 83: 12 of cap free
Amount of items: 2
Items: 
Size: 4116 Color: 0
Size: 4008 Color: 3

Bin 84: 13 of cap free
Amount of items: 2
Items: 
Size: 6669 Color: 3
Size: 1454 Color: 1

Bin 85: 13 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 16
Size: 1359 Color: 18
Size: 72 Color: 14

Bin 86: 13 of cap free
Amount of items: 2
Items: 
Size: 7041 Color: 14
Size: 1082 Color: 19

Bin 87: 13 of cap free
Amount of items: 2
Items: 
Size: 7249 Color: 6
Size: 874 Color: 11

Bin 88: 14 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 18
Size: 2054 Color: 5
Size: 136 Color: 11

Bin 89: 14 of cap free
Amount of items: 3
Items: 
Size: 6190 Color: 13
Size: 1842 Color: 1
Size: 90 Color: 0

Bin 90: 16 of cap free
Amount of items: 18
Items: 
Size: 672 Color: 9
Size: 580 Color: 9
Size: 576 Color: 7
Size: 544 Color: 14
Size: 544 Color: 13
Size: 536 Color: 7
Size: 512 Color: 0
Size: 498 Color: 12
Size: 484 Color: 7
Size: 480 Color: 12
Size: 464 Color: 15
Size: 440 Color: 15
Size: 440 Color: 11
Size: 320 Color: 2
Size: 296 Color: 5
Size: 276 Color: 1
Size: 260 Color: 10
Size: 198 Color: 10

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 5044 Color: 18
Size: 3076 Color: 4

Bin 92: 16 of cap free
Amount of items: 2
Items: 
Size: 5396 Color: 9
Size: 2724 Color: 14

Bin 93: 16 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 9
Size: 1948 Color: 12
Size: 72 Color: 15

Bin 94: 16 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 6
Size: 1475 Color: 19

Bin 95: 17 of cap free
Amount of items: 2
Items: 
Size: 7107 Color: 5
Size: 1012 Color: 15

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 5242 Color: 16
Size: 2876 Color: 6

Bin 97: 18 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 9
Size: 1450 Color: 13

Bin 98: 19 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 1
Size: 1700 Color: 17
Size: 384 Color: 6

Bin 99: 19 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 4
Size: 1252 Color: 14

Bin 100: 20 of cap free
Amount of items: 2
Items: 
Size: 6791 Color: 19
Size: 1325 Color: 11

Bin 101: 20 of cap free
Amount of items: 2
Items: 
Size: 7280 Color: 16
Size: 836 Color: 15

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 7
Size: 1870 Color: 14

Bin 103: 23 of cap free
Amount of items: 2
Items: 
Size: 6198 Color: 16
Size: 1915 Color: 6

Bin 104: 23 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 15
Size: 1387 Color: 9

Bin 105: 23 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 18
Size: 947 Color: 15

Bin 106: 26 of cap free
Amount of items: 2
Items: 
Size: 7197 Color: 2
Size: 913 Color: 4

Bin 107: 28 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 4
Size: 3224 Color: 19
Size: 798 Color: 10

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 7057 Color: 17
Size: 1051 Color: 7

Bin 109: 29 of cap free
Amount of items: 2
Items: 
Size: 5839 Color: 12
Size: 2268 Color: 1

Bin 110: 29 of cap free
Amount of items: 2
Items: 
Size: 7106 Color: 16
Size: 1001 Color: 15

Bin 111: 30 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 0
Size: 1544 Color: 19

Bin 112: 31 of cap free
Amount of items: 2
Items: 
Size: 6398 Color: 3
Size: 1707 Color: 0

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 16
Size: 1171 Color: 19

Bin 114: 36 of cap free
Amount of items: 3
Items: 
Size: 4915 Color: 12
Size: 2314 Color: 11
Size: 871 Color: 15

Bin 115: 38 of cap free
Amount of items: 2
Items: 
Size: 7098 Color: 17
Size: 1000 Color: 13

Bin 116: 42 of cap free
Amount of items: 3
Items: 
Size: 6073 Color: 13
Size: 1441 Color: 7
Size: 580 Color: 1

Bin 117: 43 of cap free
Amount of items: 4
Items: 
Size: 4076 Color: 13
Size: 1638 Color: 3
Size: 1302 Color: 15
Size: 1077 Color: 17

Bin 118: 45 of cap free
Amount of items: 8
Items: 
Size: 4069 Color: 19
Size: 728 Color: 13
Size: 718 Color: 14
Size: 702 Color: 6
Size: 690 Color: 7
Size: 676 Color: 8
Size: 336 Color: 5
Size: 172 Color: 9

Bin 119: 45 of cap free
Amount of items: 2
Items: 
Size: 5926 Color: 0
Size: 2165 Color: 19

Bin 120: 45 of cap free
Amount of items: 2
Items: 
Size: 6544 Color: 5
Size: 1547 Color: 9

Bin 121: 53 of cap free
Amount of items: 2
Items: 
Size: 4692 Color: 11
Size: 3391 Color: 4

Bin 122: 55 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 9
Size: 2951 Color: 1

Bin 123: 59 of cap free
Amount of items: 3
Items: 
Size: 4071 Color: 1
Size: 3064 Color: 4
Size: 942 Color: 5

Bin 124: 60 of cap free
Amount of items: 3
Items: 
Size: 6367 Color: 7
Size: 1101 Color: 19
Size: 608 Color: 1

Bin 125: 63 of cap free
Amount of items: 2
Items: 
Size: 6841 Color: 1
Size: 1232 Color: 17

Bin 126: 67 of cap free
Amount of items: 2
Items: 
Size: 5602 Color: 8
Size: 2467 Color: 10

Bin 127: 73 of cap free
Amount of items: 2
Items: 
Size: 5637 Color: 6
Size: 2426 Color: 18

Bin 128: 77 of cap free
Amount of items: 3
Items: 
Size: 4997 Color: 17
Size: 2902 Color: 16
Size: 160 Color: 0

Bin 129: 86 of cap free
Amount of items: 2
Items: 
Size: 4662 Color: 8
Size: 3388 Color: 5

Bin 130: 88 of cap free
Amount of items: 31
Items: 
Size: 382 Color: 0
Size: 380 Color: 19
Size: 360 Color: 6
Size: 358 Color: 15
Size: 342 Color: 13
Size: 336 Color: 17
Size: 314 Color: 9
Size: 308 Color: 19
Size: 288 Color: 19
Size: 288 Color: 6
Size: 288 Color: 0
Size: 280 Color: 11
Size: 270 Color: 11
Size: 264 Color: 1
Size: 256 Color: 2
Size: 250 Color: 10
Size: 248 Color: 5
Size: 236 Color: 2
Size: 232 Color: 6
Size: 224 Color: 12
Size: 224 Color: 3
Size: 216 Color: 15
Size: 216 Color: 3
Size: 208 Color: 7
Size: 200 Color: 11
Size: 196 Color: 5
Size: 192 Color: 18
Size: 192 Color: 10
Size: 188 Color: 18
Size: 160 Color: 1
Size: 152 Color: 17

Bin 131: 92 of cap free
Amount of items: 2
Items: 
Size: 4654 Color: 6
Size: 3390 Color: 8

Bin 132: 104 of cap free
Amount of items: 2
Items: 
Size: 5114 Color: 4
Size: 2918 Color: 14

Bin 133: 6132 of cap free
Amount of items: 13
Items: 
Size: 184 Color: 8
Size: 176 Color: 5
Size: 172 Color: 2
Size: 160 Color: 12
Size: 160 Color: 10
Size: 156 Color: 1
Size: 152 Color: 18
Size: 152 Color: 4
Size: 144 Color: 16
Size: 144 Color: 14
Size: 140 Color: 13
Size: 136 Color: 9
Size: 128 Color: 17

Total size: 1073952
Total free space: 8136

