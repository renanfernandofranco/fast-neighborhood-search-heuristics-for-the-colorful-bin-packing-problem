Capicity Bin: 7360
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4120 Color: 286
Size: 3064 Color: 266
Size: 176 Color: 39

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 4608 Color: 298
Size: 2752 Color: 257

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 309
Size: 1784 Color: 225
Size: 612 Color: 135

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 327
Size: 1726 Color: 224
Size: 156 Color: 31

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5584 Color: 330
Size: 1592 Color: 217
Size: 184 Color: 40

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 331
Size: 1668 Color: 220
Size: 100 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 336
Size: 1588 Color: 216
Size: 74 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 337
Size: 1474 Color: 209
Size: 172 Color: 37

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5844 Color: 343
Size: 770 Color: 152
Size: 746 Color: 151

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 346
Size: 1276 Color: 198
Size: 188 Color: 44

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 347
Size: 1206 Color: 190
Size: 252 Color: 69

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5997 Color: 352
Size: 945 Color: 170
Size: 418 Color: 109

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 353
Size: 1324 Color: 202
Size: 32 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 355
Size: 744 Color: 150
Size: 560 Color: 127

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 360
Size: 723 Color: 145
Size: 512 Color: 121

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 364
Size: 816 Color: 160
Size: 376 Color: 100

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 365
Size: 984 Color: 174
Size: 192 Color: 46

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6226 Color: 367
Size: 646 Color: 138
Size: 488 Color: 119

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 369
Size: 958 Color: 173
Size: 160 Color: 33

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 372
Size: 675 Color: 140
Size: 384 Color: 101

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6387 Color: 378
Size: 781 Color: 154
Size: 192 Color: 47

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6388 Color: 379
Size: 608 Color: 132
Size: 364 Color: 98

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6434 Color: 384
Size: 702 Color: 142
Size: 224 Color: 59

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6440 Color: 385
Size: 776 Color: 153
Size: 144 Color: 26

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6466 Color: 387
Size: 608 Color: 133
Size: 286 Color: 81

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6471 Color: 388
Size: 741 Color: 149
Size: 148 Color: 29

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 389
Size: 660 Color: 139
Size: 228 Color: 61

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 392
Size: 464 Color: 116
Size: 392 Color: 102

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 393
Size: 714 Color: 143
Size: 140 Color: 22

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 395
Size: 630 Color: 137
Size: 208 Color: 54

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6536 Color: 396
Size: 456 Color: 115
Size: 368 Color: 99

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 400
Size: 618 Color: 136
Size: 156 Color: 32

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 401
Size: 550 Color: 126
Size: 204 Color: 49

Bin 34: 1 of cap free
Amount of items: 7
Items: 
Size: 3684 Color: 275
Size: 811 Color: 157
Size: 728 Color: 147
Size: 728 Color: 146
Size: 716 Color: 144
Size: 484 Color: 118
Size: 208 Color: 52

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 304
Size: 2409 Color: 245
Size: 128 Color: 15

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 318
Size: 2087 Color: 234
Size: 48 Color: 5

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 322
Size: 1821 Color: 226
Size: 148 Color: 30

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5437 Color: 324
Size: 1586 Color: 215
Size: 336 Color: 94

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5492 Color: 328
Size: 1603 Color: 218
Size: 264 Color: 73

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 5528 Color: 329
Size: 1831 Color: 227

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5635 Color: 332
Size: 1404 Color: 206
Size: 320 Color: 90

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 342
Size: 1244 Color: 194
Size: 272 Color: 75

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 354
Size: 1096 Color: 183
Size: 254 Color: 70

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6068 Color: 356
Size: 1291 Color: 200

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 381
Size: 951 Color: 172

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6418 Color: 382
Size: 941 Color: 169

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 398
Size: 808 Color: 156

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6622 Color: 402
Size: 737 Color: 148

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 4578 Color: 296
Size: 2644 Color: 252
Size: 136 Color: 19

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 302
Size: 2568 Color: 250
Size: 128 Color: 17

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 4940 Color: 308
Size: 2294 Color: 242
Size: 124 Color: 13

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5460 Color: 325
Size: 1898 Color: 229

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6102 Color: 359
Size: 1256 Color: 195

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6221 Color: 366
Size: 1137 Color: 187

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6280 Color: 371
Size: 1078 Color: 182

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6493 Color: 391
Size: 865 Color: 163

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6508 Color: 394
Size: 850 Color: 162

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 399
Size: 786 Color: 155

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 291
Size: 2933 Color: 263
Size: 144 Color: 24

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5724 Color: 338
Size: 1617 Color: 219
Size: 16 Color: 1

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6307 Color: 373
Size: 1050 Color: 180

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6423 Color: 383
Size: 934 Color: 168

Bin 63: 4 of cap free
Amount of items: 23
Items: 
Size: 416 Color: 108
Size: 416 Color: 107
Size: 416 Color: 106
Size: 400 Color: 105
Size: 396 Color: 104
Size: 394 Color: 103
Size: 364 Color: 97
Size: 352 Color: 96
Size: 344 Color: 95
Size: 336 Color: 93
Size: 328 Color: 92
Size: 322 Color: 91
Size: 304 Color: 88
Size: 272 Color: 78
Size: 272 Color: 77
Size: 272 Color: 76
Size: 272 Color: 74
Size: 256 Color: 72
Size: 256 Color: 71
Size: 248 Color: 68
Size: 240 Color: 67
Size: 240 Color: 66
Size: 240 Color: 65

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 5048 Color: 312
Size: 2308 Color: 243

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 5336 Color: 320
Size: 2020 Color: 233

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 5876 Color: 345
Size: 1480 Color: 210

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 362
Size: 1224 Color: 193

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 363
Size: 1208 Color: 191

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6477 Color: 390
Size: 879 Color: 165

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6540 Color: 397
Size: 816 Color: 159

Bin 71: 5 of cap free
Amount of items: 9
Items: 
Size: 3683 Color: 274
Size: 684 Color: 141
Size: 608 Color: 134
Size: 608 Color: 131
Size: 574 Color: 130
Size: 566 Color: 129
Size: 212 Color: 56
Size: 212 Color: 55
Size: 208 Color: 53

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 5827 Color: 341
Size: 1528 Color: 212

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6324 Color: 375
Size: 1031 Color: 179

Bin 74: 6 of cap free
Amount of items: 4
Items: 
Size: 4194 Color: 290
Size: 2872 Color: 261
Size: 144 Color: 27
Size: 144 Color: 25

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 5165 Color: 316
Size: 2101 Color: 235
Size: 88 Color: 7

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 333
Size: 1688 Color: 222
Size: 16 Color: 0

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 335
Size: 1670 Color: 221

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 350
Size: 1374 Color: 204

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6086 Color: 358
Size: 1268 Color: 197

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 368
Size: 1127 Color: 185

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 376
Size: 1012 Color: 177

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6354 Color: 377
Size: 1000 Color: 176

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 6450 Color: 386
Size: 904 Color: 166

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 5914 Color: 348
Size: 1439 Color: 208

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 5928 Color: 349
Size: 1425 Color: 207

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 4841 Color: 306
Size: 2511 Color: 249

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 4966 Color: 310
Size: 2262 Color: 240
Size: 124 Color: 12

Bin 88: 8 of cap free
Amount of items: 4
Items: 
Size: 5290 Color: 319
Size: 1998 Color: 232
Size: 32 Color: 3
Size: 32 Color: 2

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 5864 Color: 344
Size: 1488 Color: 211

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 370
Size: 1108 Color: 184

Bin 91: 9 of cap free
Amount of items: 4
Items: 
Size: 4188 Color: 288
Size: 2833 Color: 259
Size: 168 Color: 35
Size: 162 Color: 34

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 5987 Color: 351
Size: 1364 Color: 203

Bin 93: 11 of cap free
Amount of items: 2
Items: 
Size: 5421 Color: 323
Size: 1928 Color: 230

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 5813 Color: 340
Size: 1536 Color: 213

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6070 Color: 357
Size: 1279 Color: 199

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 6131 Color: 361
Size: 1218 Color: 192

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 6403 Color: 380
Size: 946 Color: 171

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 5464 Color: 326
Size: 1884 Color: 228

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 5784 Color: 339
Size: 1564 Color: 214

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 6323 Color: 374
Size: 1025 Color: 178

Bin 101: 13 of cap free
Amount of items: 2
Items: 
Size: 5651 Color: 334
Size: 1696 Color: 223

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 5175 Color: 317
Size: 2168 Color: 239

Bin 103: 20 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 295
Size: 2652 Color: 253
Size: 136 Color: 20

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 4628 Color: 300
Size: 2712 Color: 256

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 311
Size: 2344 Color: 244

Bin 106: 24 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 315
Size: 2132 Color: 238
Size: 96 Color: 8

Bin 107: 24 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 321
Size: 1972 Color: 231

Bin 108: 29 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 303
Size: 2427 Color: 246
Size: 128 Color: 16

Bin 109: 30 of cap free
Amount of items: 6
Items: 
Size: 3688 Color: 276
Size: 932 Color: 167
Size: 868 Color: 164
Size: 826 Color: 161
Size: 812 Color: 158
Size: 204 Color: 51

Bin 110: 34 of cap free
Amount of items: 2
Items: 
Size: 4449 Color: 292
Size: 2877 Color: 262

Bin 111: 34 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 314
Size: 2118 Color: 237
Size: 120 Color: 10

Bin 112: 35 of cap free
Amount of items: 4
Items: 
Size: 4180 Color: 287
Size: 2803 Color: 258
Size: 174 Color: 38
Size: 168 Color: 36

Bin 113: 39 of cap free
Amount of items: 2
Items: 
Size: 4825 Color: 305
Size: 2496 Color: 248

Bin 114: 41 of cap free
Amount of items: 3
Items: 
Size: 5086 Color: 313
Size: 2113 Color: 236
Size: 120 Color: 11

Bin 115: 56 of cap free
Amount of items: 9
Items: 
Size: 3682 Color: 273
Size: 560 Color: 128
Size: 528 Color: 125
Size: 528 Color: 124
Size: 528 Color: 123
Size: 528 Color: 122
Size: 502 Color: 120
Size: 224 Color: 58
Size: 224 Color: 57

Bin 116: 56 of cap free
Amount of items: 2
Items: 
Size: 4610 Color: 299
Size: 2694 Color: 255

Bin 117: 58 of cap free
Amount of items: 3
Items: 
Size: 4596 Color: 297
Size: 1386 Color: 205
Size: 1320 Color: 201

Bin 118: 59 of cap free
Amount of items: 11
Items: 
Size: 3681 Color: 272
Size: 480 Color: 117
Size: 456 Color: 114
Size: 452 Color: 113
Size: 448 Color: 112
Size: 422 Color: 111
Size: 420 Color: 110
Size: 240 Color: 64
Size: 240 Color: 63
Size: 236 Color: 62
Size: 226 Color: 60

Bin 119: 62 of cap free
Amount of items: 5
Items: 
Size: 3692 Color: 278
Size: 1265 Color: 196
Size: 1156 Color: 189
Size: 985 Color: 175
Size: 200 Color: 48

Bin 120: 75 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 294
Size: 2678 Color: 254
Size: 136 Color: 21

Bin 121: 86 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 301
Size: 2494 Color: 247
Size: 134 Color: 18

Bin 122: 89 of cap free
Amount of items: 2
Items: 
Size: 3928 Color: 281
Size: 3343 Color: 271

Bin 123: 91 of cap free
Amount of items: 3
Items: 
Size: 4857 Color: 307
Size: 2284 Color: 241
Size: 128 Color: 14

Bin 124: 111 of cap free
Amount of items: 3
Items: 
Size: 3997 Color: 285
Size: 3068 Color: 268
Size: 184 Color: 41

Bin 125: 123 of cap free
Amount of items: 5
Items: 
Size: 3690 Color: 277
Size: 1145 Color: 188
Size: 1136 Color: 186
Size: 1062 Color: 181
Size: 204 Color: 50

Bin 126: 123 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 293
Size: 2642 Color: 251
Size: 140 Color: 23

Bin 127: 144 of cap free
Amount of items: 2
Items: 
Size: 3925 Color: 280
Size: 3291 Color: 270

Bin 128: 145 of cap free
Amount of items: 3
Items: 
Size: 3961 Color: 284
Size: 3066 Color: 267
Size: 188 Color: 42

Bin 129: 155 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 289
Size: 2869 Color: 260
Size: 146 Color: 28

Bin 130: 165 of cap free
Amount of items: 3
Items: 
Size: 3945 Color: 283
Size: 3062 Color: 265
Size: 188 Color: 43

Bin 131: 167 of cap free
Amount of items: 3
Items: 
Size: 3941 Color: 282
Size: 3060 Color: 264
Size: 192 Color: 45

Bin 132: 190 of cap free
Amount of items: 2
Items: 
Size: 3909 Color: 279
Size: 3261 Color: 269

Bin 133: 4708 of cap free
Amount of items: 9
Items: 
Size: 312 Color: 89
Size: 304 Color: 87
Size: 304 Color: 86
Size: 300 Color: 85
Size: 296 Color: 84
Size: 288 Color: 83
Size: 288 Color: 82
Size: 284 Color: 80
Size: 276 Color: 79

Total size: 971520
Total free space: 7360

