Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 24
Items: 
Size: 160 Color: 60
Size: 148 Color: 57
Size: 144 Color: 56
Size: 144 Color: 54
Size: 124 Color: 49
Size: 120 Color: 48
Size: 120 Color: 47
Size: 112 Color: 46
Size: 108 Color: 45
Size: 106 Color: 44
Size: 106 Color: 43
Size: 104 Color: 41
Size: 92 Color: 39
Size: 90 Color: 38
Size: 88 Color: 32
Size: 88 Color: 31
Size: 88 Color: 30
Size: 80 Color: 29
Size: 80 Color: 28
Size: 76 Color: 27
Size: 76 Color: 26
Size: 74 Color: 24
Size: 64 Color: 21
Size: 64 Color: 20

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 142
Size: 1018 Color: 133
Size: 104 Color: 42
Size: 48 Color: 13
Size: 48 Color: 12

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1696 Color: 158
Size: 568 Color: 111
Size: 176 Color: 63
Size: 16 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 169
Size: 580 Color: 112
Size: 24 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 170
Size: 436 Color: 94
Size: 128 Color: 52

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 172
Size: 384 Color: 88
Size: 164 Color: 61

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1924 Color: 176
Size: 524 Color: 105
Size: 8 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 185
Size: 280 Color: 78
Size: 148 Color: 58

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 186
Size: 260 Color: 75
Size: 152 Color: 59

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 188
Size: 331 Color: 84
Size: 64 Color: 22

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 190
Size: 228 Color: 71
Size: 128 Color: 53

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 193
Size: 242 Color: 73
Size: 96 Color: 40

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 282 Color: 79
Size: 28 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 195
Size: 232 Color: 72
Size: 76 Color: 25

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 198
Size: 226 Color: 70
Size: 44 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 199
Size: 204 Color: 68
Size: 64 Color: 23

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 200
Size: 262 Color: 76
Size: 2 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 157
Size: 618 Color: 113
Size: 144 Color: 55

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1913 Color: 175
Size: 542 Color: 109

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 2002 Color: 183
Size: 453 Color: 98

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 197
Size: 289 Color: 80

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 2202 Color: 201
Size: 253 Color: 74

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 1978 Color: 179
Size: 476 Color: 103

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 181
Size: 460 Color: 101

Bin 25: 2 of cap free
Amount of items: 2
Items: 
Size: 2005 Color: 184
Size: 449 Color: 97

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 2050 Color: 187
Size: 404 Color: 93

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 191
Size: 348 Color: 86

Bin 28: 3 of cap free
Amount of items: 2
Items: 
Size: 1697 Color: 159
Size: 756 Color: 123

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 182
Size: 444 Color: 96
Size: 8 Color: 2

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 2111 Color: 192
Size: 342 Color: 85

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 196
Size: 300 Color: 81

Bin 32: 4 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 160
Size: 751 Color: 122

Bin 33: 4 of cap free
Amount of items: 2
Items: 
Size: 1815 Color: 167
Size: 637 Color: 117

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 178
Size: 512 Color: 104

Bin 35: 5 of cap free
Amount of items: 7
Items: 
Size: 1230 Color: 139
Size: 381 Color: 87
Size: 330 Color: 82
Size: 272 Color: 77
Size: 126 Color: 51
Size: 56 Color: 17
Size: 56 Color: 16

Bin 36: 5 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 189
Size: 389 Color: 90

Bin 37: 6 of cap free
Amount of items: 2
Items: 
Size: 1925 Color: 177
Size: 525 Color: 106

Bin 38: 7 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 146
Size: 1023 Color: 137

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 174
Size: 539 Color: 108

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1991 Color: 180
Size: 458 Color: 100

Bin 41: 8 of cap free
Amount of items: 2
Items: 
Size: 1530 Color: 149
Size: 918 Color: 131

Bin 42: 8 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 156
Size: 774 Color: 124

Bin 43: 8 of cap free
Amount of items: 2
Items: 
Size: 1718 Color: 162
Size: 730 Color: 121

Bin 44: 8 of cap free
Amount of items: 2
Items: 
Size: 1764 Color: 163
Size: 684 Color: 119

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 164
Size: 654 Color: 118

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1894 Color: 171
Size: 554 Color: 110

Bin 47: 9 of cap free
Amount of items: 9
Items: 
Size: 1229 Color: 138
Size: 204 Color: 67
Size: 204 Color: 66
Size: 200 Color: 65
Size: 200 Color: 64
Size: 172 Color: 62
Size: 126 Color: 50
Size: 56 Color: 19
Size: 56 Color: 18

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1827 Color: 168
Size: 620 Color: 114

Bin 49: 11 of cap free
Amount of items: 2
Items: 
Size: 1423 Color: 145
Size: 1022 Color: 136

Bin 50: 11 of cap free
Amount of items: 2
Items: 
Size: 1716 Color: 161
Size: 729 Color: 120

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1557 Color: 152
Size: 887 Color: 129

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 155
Size: 402 Color: 92
Size: 398 Color: 91

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1811 Color: 166
Size: 633 Color: 116

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1909 Color: 173
Size: 535 Color: 107

Bin 55: 13 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 154
Size: 840 Color: 126
Size: 20 Color: 4

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 144
Size: 1021 Color: 135

Bin 57: 14 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 153
Size: 828 Color: 125
Size: 32 Color: 7

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 165
Size: 631 Color: 115

Bin 59: 16 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 140
Size: 443 Color: 95
Size: 386 Color: 89
Size: 330 Color: 83
Size: 50 Color: 15

Bin 60: 19 of cap free
Amount of items: 5
Items: 
Size: 1236 Color: 141
Size: 470 Color: 102
Size: 457 Color: 99
Size: 226 Color: 69
Size: 48 Color: 14

Bin 61: 19 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 150
Size: 861 Color: 127
Size: 40 Color: 8

Bin 62: 24 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 143
Size: 1020 Color: 134
Size: 48 Color: 11

Bin 63: 24 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 148
Size: 916 Color: 130
Size: 48 Color: 10

Bin 64: 34 of cap free
Amount of items: 2
Items: 
Size: 1556 Color: 151
Size: 866 Color: 128

Bin 65: 38 of cap free
Amount of items: 2
Items: 
Size: 1442 Color: 147
Size: 976 Color: 132

Bin 66: 2014 of cap free
Amount of items: 5
Items: 
Size: 90 Color: 37
Size: 88 Color: 36
Size: 88 Color: 35
Size: 88 Color: 34
Size: 88 Color: 33

Total size: 159640
Total free space: 2456

