Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 3
Size: 253 Color: 0
Size: 250 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 326 Color: 2
Size: 313 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 293 Color: 0
Size: 254 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 314 Color: 1
Size: 266 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 264 Color: 4
Size: 260 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 275 Color: 0
Size: 270 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 359 Color: 0
Size: 255 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 330 Color: 3
Size: 253 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 316 Color: 3
Size: 272 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 276 Color: 0
Size: 268 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 2
Size: 352 Color: 0
Size: 292 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 340 Color: 1
Size: 280 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 4
Size: 305 Color: 4
Size: 251 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 2
Size: 329 Color: 2
Size: 326 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 268 Color: 4
Size: 265 Color: 0
Size: 467 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 274 Color: 2
Size: 253 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 294 Color: 1
Size: 257 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 4
Size: 253 Color: 3
Size: 252 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 317 Color: 2
Size: 280 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 342 Color: 0
Size: 255 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 307 Color: 2
Size: 296 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 311 Color: 1
Size: 265 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 320 Color: 2
Size: 275 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 4
Size: 316 Color: 3
Size: 261 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 295 Color: 2
Size: 282 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 294 Color: 4
Size: 257 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 342 Color: 2
Size: 252 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 279 Color: 0
Size: 255 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 307 Color: 4
Size: 271 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 305 Color: 1
Size: 299 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 321 Color: 2
Size: 278 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 324 Color: 2
Size: 281 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 255 Color: 4
Size: 252 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 281 Color: 3
Size: 255 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 346 Color: 2
Size: 255 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 275 Color: 4
Size: 274 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 0
Size: 320 Color: 1
Size: 318 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 257 Color: 0
Size: 255 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 324 Color: 0
Size: 262 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 358 Color: 1
Size: 264 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 274 Color: 3
Size: 260 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 2
Size: 259 Color: 1
Size: 258 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 294 Color: 4
Size: 251 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 305 Color: 3
Size: 289 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 256 Color: 0
Size: 255 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 3
Size: 353 Color: 0
Size: 283 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 309 Color: 1
Size: 280 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 300 Color: 1
Size: 290 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 261 Color: 0
Size: 257 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 325 Color: 1
Size: 251 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 269 Color: 2
Size: 255 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 280 Color: 3
Size: 278 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 294 Color: 1
Size: 273 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 3
Size: 331 Color: 1
Size: 256 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 3
Size: 356 Color: 3
Size: 285 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 271 Color: 1
Size: 257 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 4
Size: 302 Color: 0
Size: 270 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 274 Color: 0
Size: 264 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 334 Color: 2
Size: 256 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 332 Color: 2
Size: 294 Color: 4
Size: 374 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 275 Color: 4
Size: 252 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 314 Color: 3
Size: 260 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 362 Color: 4
Size: 256 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 282 Color: 0
Size: 281 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 303 Color: 3
Size: 264 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 2
Size: 285 Color: 1
Size: 283 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 272 Color: 4
Size: 269 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 338 Color: 0
Size: 280 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 274 Color: 0
Size: 265 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 353 Color: 0
Size: 255 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 319 Color: 2
Size: 261 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 358 Color: 2
Size: 270 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 318 Color: 1
Size: 278 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 2
Size: 289 Color: 3
Size: 261 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 316 Color: 4
Size: 306 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 2
Size: 340 Color: 1
Size: 266 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 296 Color: 3
Size: 290 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 284 Color: 4
Size: 265 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 360 Color: 0
Size: 256 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 261 Color: 4
Size: 253 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 4
Size: 273 Color: 0
Size: 253 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 291 Color: 0
Size: 255 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 356 Color: 4
Size: 253 Color: 4

Total size: 83000
Total free space: 0

