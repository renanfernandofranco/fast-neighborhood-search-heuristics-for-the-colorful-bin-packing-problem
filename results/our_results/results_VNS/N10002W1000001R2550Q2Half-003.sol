Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 436282 Color: 1
Size: 308806 Color: 1
Size: 254913 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 485242 Color: 1
Size: 262827 Color: 1
Size: 251932 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 411037 Color: 1
Size: 306575 Color: 1
Size: 282389 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 394171 Color: 1
Size: 316402 Color: 1
Size: 289428 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 377574 Color: 1
Size: 346370 Color: 1
Size: 276057 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 356879 Color: 1
Size: 356737 Color: 1
Size: 286385 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 482989 Color: 1
Size: 261857 Color: 1
Size: 255155 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 379443 Color: 1
Size: 315057 Color: 0
Size: 305501 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 406573 Color: 1
Size: 304691 Color: 0
Size: 288737 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 437934 Color: 1
Size: 305069 Color: 1
Size: 256998 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 443021 Color: 1
Size: 301285 Color: 1
Size: 255695 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 399545 Color: 1
Size: 317783 Color: 1
Size: 282673 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 366338 Color: 1
Size: 331205 Color: 1
Size: 302458 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 470208 Color: 1
Size: 275784 Color: 1
Size: 254009 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 397717 Color: 1
Size: 328128 Color: 1
Size: 274156 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 415143 Color: 1
Size: 317044 Color: 1
Size: 267814 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 399515 Color: 1
Size: 326620 Color: 1
Size: 273866 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 425633 Color: 1
Size: 294443 Color: 1
Size: 279925 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 380669 Color: 1
Size: 338849 Color: 1
Size: 280483 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 472566 Color: 1
Size: 263868 Color: 1
Size: 263567 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 436917 Color: 1
Size: 304097 Color: 1
Size: 258987 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 493176 Color: 1
Size: 255243 Color: 1
Size: 251582 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 435638 Color: 1
Size: 305300 Color: 1
Size: 259063 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 394912 Color: 1
Size: 351957 Color: 1
Size: 253132 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 414129 Color: 1
Size: 299475 Color: 0
Size: 286397 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 496885 Color: 1
Size: 252768 Color: 1
Size: 250348 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 438680 Color: 1
Size: 307541 Color: 1
Size: 253780 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 469862 Color: 1
Size: 272804 Color: 1
Size: 257335 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 426559 Color: 1
Size: 306421 Color: 1
Size: 267021 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 363947 Color: 1
Size: 327372 Color: 1
Size: 308682 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 368804 Color: 1
Size: 338999 Color: 1
Size: 292198 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 468066 Color: 1
Size: 281057 Color: 1
Size: 250878 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 359730 Color: 1
Size: 359232 Color: 1
Size: 281039 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 495896 Color: 1
Size: 252877 Color: 1
Size: 251228 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 379423 Color: 1
Size: 351326 Color: 1
Size: 269252 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 378372 Color: 1
Size: 314980 Color: 1
Size: 306649 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 390956 Color: 1
Size: 332666 Color: 1
Size: 276379 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 498878 Color: 1
Size: 250792 Color: 1
Size: 250331 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 362377 Color: 1
Size: 348292 Color: 1
Size: 289332 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 449347 Color: 1
Size: 291017 Color: 1
Size: 259637 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 496200 Color: 1
Size: 252678 Color: 1
Size: 251123 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 416929 Color: 1
Size: 300063 Color: 0
Size: 283009 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 486207 Color: 1
Size: 261267 Color: 1
Size: 252527 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 497903 Color: 1
Size: 251846 Color: 1
Size: 250252 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 390239 Color: 1
Size: 314998 Color: 1
Size: 294764 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 399570 Color: 1
Size: 337063 Color: 1
Size: 263368 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 455870 Color: 1
Size: 290481 Color: 1
Size: 253650 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 394090 Color: 1
Size: 304941 Color: 0
Size: 300970 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 451015 Color: 1
Size: 287272 Color: 1
Size: 261714 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 382470 Color: 1
Size: 319213 Color: 1
Size: 298318 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 358653 Color: 1
Size: 338780 Color: 1
Size: 302568 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 351921 Color: 1
Size: 340074 Color: 1
Size: 308006 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 452531 Color: 1
Size: 276127 Color: 1
Size: 271343 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 370766 Color: 1
Size: 330351 Color: 1
Size: 298884 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 446881 Color: 1
Size: 287813 Color: 1
Size: 265307 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 392854 Color: 1
Size: 350453 Color: 1
Size: 256694 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 358167 Color: 1
Size: 353933 Color: 1
Size: 287901 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 483450 Color: 1
Size: 265617 Color: 1
Size: 250934 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 365272 Color: 1
Size: 323527 Color: 1
Size: 311202 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 426230 Color: 1
Size: 305055 Color: 1
Size: 268716 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 491063 Color: 1
Size: 256499 Color: 1
Size: 252439 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 434289 Color: 1
Size: 293055 Color: 1
Size: 272657 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 433668 Color: 1
Size: 312958 Color: 1
Size: 253375 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 491937 Color: 1
Size: 256870 Color: 1
Size: 251194 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 428069 Color: 1
Size: 304365 Color: 1
Size: 267567 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 476492 Color: 1
Size: 270394 Color: 1
Size: 253115 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 359813 Color: 1
Size: 325962 Color: 1
Size: 314226 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 374491 Color: 1
Size: 349765 Color: 1
Size: 275745 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 495159 Color: 1
Size: 253839 Color: 1
Size: 251003 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 368981 Color: 1
Size: 336663 Color: 1
Size: 294357 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 490944 Color: 1
Size: 259022 Color: 1
Size: 250035 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 497925 Color: 1
Size: 251933 Color: 1
Size: 250143 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 418668 Color: 1
Size: 321343 Color: 1
Size: 259990 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 485956 Color: 1
Size: 257303 Color: 1
Size: 256742 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 354803 Color: 1
Size: 330656 Color: 1
Size: 314542 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 410861 Color: 1
Size: 328176 Color: 1
Size: 260964 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 444422 Color: 1
Size: 299993 Color: 1
Size: 255586 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 450353 Color: 1
Size: 293089 Color: 1
Size: 256559 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 455505 Color: 1
Size: 280309 Color: 1
Size: 264187 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 377743 Color: 1
Size: 316067 Color: 1
Size: 306191 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 490470 Color: 1
Size: 257790 Color: 1
Size: 251741 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 370262 Color: 1
Size: 348157 Color: 1
Size: 281582 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 379203 Color: 1
Size: 348620 Color: 1
Size: 272178 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 395259 Color: 1
Size: 336727 Color: 1
Size: 268015 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 481786 Color: 1
Size: 262234 Color: 1
Size: 255981 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 405345 Color: 1
Size: 324911 Color: 1
Size: 269745 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 371503 Color: 1
Size: 331605 Color: 1
Size: 296893 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 483460 Color: 1
Size: 263256 Color: 1
Size: 253285 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 441927 Color: 1
Size: 290004 Color: 1
Size: 268070 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 449467 Color: 1
Size: 298189 Color: 1
Size: 252345 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 475969 Color: 1
Size: 270492 Color: 1
Size: 253540 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 398324 Color: 1
Size: 344079 Color: 1
Size: 257598 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 440884 Color: 1
Size: 299850 Color: 1
Size: 259267 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 403164 Color: 1
Size: 321002 Color: 1
Size: 275835 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 492006 Color: 1
Size: 256795 Color: 1
Size: 251200 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 348565 Color: 1
Size: 326312 Color: 0
Size: 325124 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 435291 Color: 1
Size: 310511 Color: 1
Size: 254199 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 372263 Color: 1
Size: 347860 Color: 1
Size: 279878 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 473458 Color: 1
Size: 266493 Color: 0
Size: 260050 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 411915 Color: 1
Size: 299946 Color: 1
Size: 288140 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 392784 Color: 1
Size: 331625 Color: 1
Size: 275592 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 383680 Color: 1
Size: 354445 Color: 1
Size: 261876 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 393513 Color: 1
Size: 325001 Color: 1
Size: 281487 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 366926 Color: 1
Size: 342878 Color: 1
Size: 290197 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 408232 Color: 1
Size: 302722 Color: 1
Size: 289047 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 409579 Color: 1
Size: 318292 Color: 1
Size: 272130 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 406543 Color: 1
Size: 326116 Color: 1
Size: 267342 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 484355 Color: 1
Size: 262421 Color: 1
Size: 253225 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 410909 Color: 1
Size: 326672 Color: 1
Size: 262420 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 462834 Color: 1
Size: 284483 Color: 1
Size: 252684 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 469164 Color: 1
Size: 268137 Color: 1
Size: 262700 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 402977 Color: 1
Size: 310525 Color: 1
Size: 286499 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 466706 Color: 1
Size: 282995 Color: 1
Size: 250300 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 482288 Color: 1
Size: 263742 Color: 1
Size: 253971 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 401568 Color: 1
Size: 329921 Color: 1
Size: 268512 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 472164 Color: 1
Size: 269841 Color: 1
Size: 257996 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 358857 Color: 1
Size: 322147 Color: 1
Size: 318997 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 386937 Color: 1
Size: 333704 Color: 1
Size: 279360 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 430298 Color: 1
Size: 312674 Color: 1
Size: 257029 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 474908 Color: 1
Size: 266165 Color: 1
Size: 258928 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 440837 Color: 1
Size: 291416 Color: 1
Size: 267748 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 483045 Color: 1
Size: 264572 Color: 1
Size: 252384 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 467413 Color: 1
Size: 274428 Color: 1
Size: 258160 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 474786 Color: 1
Size: 270477 Color: 1
Size: 254738 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 462437 Color: 1
Size: 284217 Color: 1
Size: 253347 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 462751 Color: 1
Size: 277778 Color: 1
Size: 259472 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 395720 Color: 1
Size: 316779 Color: 1
Size: 287502 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 392276 Color: 1
Size: 336106 Color: 1
Size: 271619 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 379894 Color: 1
Size: 324615 Color: 1
Size: 295492 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 489225 Color: 1
Size: 258316 Color: 1
Size: 252460 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 378426 Color: 1
Size: 318796 Color: 1
Size: 302779 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 398825 Color: 1
Size: 322977 Color: 1
Size: 278199 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 391921 Color: 1
Size: 332108 Color: 1
Size: 275972 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 496046 Color: 1
Size: 253435 Color: 1
Size: 250520 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 483405 Color: 1
Size: 265743 Color: 1
Size: 250853 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 392166 Color: 1
Size: 350632 Color: 1
Size: 257203 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 447895 Color: 1
Size: 300364 Color: 1
Size: 251742 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 499148 Color: 1
Size: 250724 Color: 1
Size: 250129 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 442402 Color: 1
Size: 299701 Color: 1
Size: 257898 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 459815 Color: 1
Size: 287992 Color: 1
Size: 252194 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 446384 Color: 1
Size: 293556 Color: 1
Size: 260061 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 445302 Color: 1
Size: 301203 Color: 1
Size: 253496 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 351118 Color: 1
Size: 345229 Color: 1
Size: 303654 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 452701 Color: 1
Size: 285252 Color: 1
Size: 262048 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 382001 Color: 1
Size: 336618 Color: 1
Size: 281382 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 375063 Color: 1
Size: 338163 Color: 1
Size: 286775 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 425352 Color: 1
Size: 305590 Color: 1
Size: 269059 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 370907 Color: 1
Size: 350582 Color: 1
Size: 278512 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 365851 Color: 1
Size: 322231 Color: 1
Size: 311919 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 380510 Color: 1
Size: 353764 Color: 1
Size: 265727 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 392085 Color: 1
Size: 337986 Color: 1
Size: 269930 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 430333 Color: 1
Size: 311392 Color: 1
Size: 258276 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 422124 Color: 1
Size: 301710 Color: 1
Size: 276167 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 454646 Color: 1
Size: 279295 Color: 1
Size: 266060 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 444964 Color: 1
Size: 295935 Color: 1
Size: 259102 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 456833 Color: 1
Size: 289439 Color: 1
Size: 253729 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 428061 Color: 1
Size: 290654 Color: 0
Size: 281286 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 427143 Color: 1
Size: 315846 Color: 1
Size: 257012 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 420275 Color: 1
Size: 320510 Color: 0
Size: 259216 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 479907 Color: 1
Size: 261128 Color: 1
Size: 258966 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 480943 Color: 1
Size: 261302 Color: 1
Size: 257756 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 355904 Color: 1
Size: 351215 Color: 1
Size: 292882 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 370431 Color: 1
Size: 357365 Color: 1
Size: 272205 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 487075 Color: 1
Size: 260159 Color: 0
Size: 252767 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 440711 Color: 1
Size: 301932 Color: 1
Size: 257358 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 488721 Color: 1
Size: 259373 Color: 1
Size: 251907 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 381468 Color: 1
Size: 342015 Color: 1
Size: 276518 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 476957 Color: 1
Size: 272236 Color: 1
Size: 250808 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 369249 Color: 1
Size: 346504 Color: 1
Size: 284248 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 414354 Color: 1
Size: 309948 Color: 1
Size: 275699 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 404275 Color: 1
Size: 328376 Color: 1
Size: 267350 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 390985 Color: 1
Size: 325916 Color: 1
Size: 283100 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 437120 Color: 1
Size: 307547 Color: 1
Size: 255334 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 393623 Color: 1
Size: 326763 Color: 1
Size: 279615 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 431587 Color: 1
Size: 309031 Color: 1
Size: 259383 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 466048 Color: 1
Size: 283926 Color: 1
Size: 250027 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 381087 Color: 1
Size: 340312 Color: 1
Size: 278602 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 443690 Color: 1
Size: 286683 Color: 1
Size: 269628 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 363775 Color: 1
Size: 340059 Color: 1
Size: 296167 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 407330 Color: 1
Size: 334332 Color: 1
Size: 258339 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 410225 Color: 1
Size: 334221 Color: 1
Size: 255555 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 395287 Color: 1
Size: 340501 Color: 1
Size: 264213 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 475907 Color: 1
Size: 271517 Color: 1
Size: 252577 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 456982 Color: 1
Size: 287471 Color: 1
Size: 255548 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 476729 Color: 1
Size: 272511 Color: 1
Size: 250761 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 376322 Color: 1
Size: 321571 Color: 0
Size: 302108 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 349355 Color: 1
Size: 332703 Color: 1
Size: 317943 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 364893 Color: 1
Size: 318538 Color: 0
Size: 316570 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 431023 Color: 1
Size: 297689 Color: 1
Size: 271289 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 379688 Color: 1
Size: 334450 Color: 1
Size: 285863 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 416984 Color: 1
Size: 299498 Color: 1
Size: 283519 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 470057 Color: 1
Size: 272010 Color: 1
Size: 257934 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 359369 Color: 1
Size: 344124 Color: 1
Size: 296508 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 493681 Color: 1
Size: 253926 Color: 1
Size: 252394 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 401985 Color: 1
Size: 345747 Color: 1
Size: 252269 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 435480 Color: 1
Size: 288565 Color: 1
Size: 275956 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 367497 Color: 1
Size: 326173 Color: 0
Size: 306331 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 424142 Color: 1
Size: 313860 Color: 1
Size: 261999 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 480490 Color: 1
Size: 268319 Color: 1
Size: 251192 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 354023 Color: 1
Size: 348730 Color: 1
Size: 297248 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 354841 Color: 1
Size: 333105 Color: 1
Size: 312055 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 380119 Color: 1
Size: 351302 Color: 1
Size: 268580 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 461869 Color: 1
Size: 279117 Color: 1
Size: 259015 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 485854 Color: 1
Size: 258828 Color: 1
Size: 255319 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 473543 Color: 1
Size: 273567 Color: 1
Size: 252891 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 480373 Color: 1
Size: 267282 Color: 1
Size: 252346 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 499545 Color: 1
Size: 250249 Color: 1
Size: 250207 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 387726 Color: 1
Size: 327853 Color: 1
Size: 284422 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 397187 Color: 1
Size: 315553 Color: 1
Size: 287261 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 378186 Color: 1
Size: 334433 Color: 1
Size: 287382 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 379586 Color: 1
Size: 322427 Color: 0
Size: 297988 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 469382 Color: 1
Size: 277584 Color: 1
Size: 253035 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 392185 Color: 1
Size: 355139 Color: 1
Size: 252677 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 476611 Color: 1
Size: 270626 Color: 1
Size: 252764 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 434914 Color: 1
Size: 303629 Color: 1
Size: 261458 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 408360 Color: 1
Size: 315288 Color: 1
Size: 276353 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 439975 Color: 1
Size: 290094 Color: 1
Size: 269932 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 448893 Color: 1
Size: 293790 Color: 1
Size: 257318 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 392325 Color: 1
Size: 334456 Color: 1
Size: 273220 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 455520 Color: 1
Size: 272340 Color: 0
Size: 272141 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 475597 Color: 1
Size: 267796 Color: 1
Size: 256608 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 373417 Color: 1
Size: 344558 Color: 1
Size: 282026 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 373854 Color: 1
Size: 322471 Color: 1
Size: 303676 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 416852 Color: 1
Size: 306645 Color: 1
Size: 276504 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 438044 Color: 1
Size: 281164 Color: 1
Size: 280793 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 431220 Color: 1
Size: 306764 Color: 1
Size: 262017 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 472736 Color: 1
Size: 267941 Color: 1
Size: 259324 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 393695 Color: 1
Size: 353758 Color: 1
Size: 252548 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 388237 Color: 1
Size: 316561 Color: 1
Size: 295203 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 428376 Color: 1
Size: 316764 Color: 1
Size: 254861 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 414442 Color: 1
Size: 313197 Color: 1
Size: 272362 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 396624 Color: 1
Size: 319251 Color: 1
Size: 284126 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 467926 Color: 1
Size: 273385 Color: 1
Size: 258690 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 448693 Color: 1
Size: 289487 Color: 1
Size: 261821 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 421819 Color: 1
Size: 314217 Color: 1
Size: 263965 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 367067 Color: 1
Size: 320302 Color: 0
Size: 312632 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 465559 Color: 1
Size: 278320 Color: 1
Size: 256122 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 376543 Color: 1
Size: 333865 Color: 1
Size: 289593 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 418425 Color: 1
Size: 319102 Color: 1
Size: 262474 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 385396 Color: 1
Size: 338452 Color: 1
Size: 276153 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 346950 Color: 1
Size: 341329 Color: 1
Size: 311722 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 405946 Color: 1
Size: 333083 Color: 1
Size: 260972 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 465837 Color: 1
Size: 274187 Color: 0
Size: 259977 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 374061 Color: 1
Size: 345390 Color: 1
Size: 280550 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 355244 Color: 1
Size: 348557 Color: 1
Size: 296200 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 489747 Color: 1
Size: 258266 Color: 1
Size: 251988 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 429278 Color: 1
Size: 309586 Color: 1
Size: 261137 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 376906 Color: 1
Size: 324796 Color: 1
Size: 298299 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 434605 Color: 1
Size: 287235 Color: 1
Size: 278161 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 371052 Color: 1
Size: 339988 Color: 1
Size: 288961 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 443001 Color: 1
Size: 305891 Color: 1
Size: 251109 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 445284 Color: 1
Size: 290994 Color: 1
Size: 263723 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 432569 Color: 1
Size: 304043 Color: 1
Size: 263389 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 424861 Color: 1
Size: 320743 Color: 1
Size: 254397 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 387528 Color: 1
Size: 340272 Color: 1
Size: 272201 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 388443 Color: 1
Size: 315952 Color: 1
Size: 295606 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 367707 Color: 1
Size: 329323 Color: 1
Size: 302971 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 365962 Color: 1
Size: 331244 Color: 1
Size: 302795 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 356568 Color: 1
Size: 326253 Color: 1
Size: 317180 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 434282 Color: 1
Size: 302246 Color: 1
Size: 263473 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 493686 Color: 1
Size: 254457 Color: 1
Size: 251858 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 496906 Color: 1
Size: 252694 Color: 1
Size: 250401 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 395281 Color: 1
Size: 338887 Color: 1
Size: 265833 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 475426 Color: 1
Size: 273443 Color: 1
Size: 251132 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 358639 Color: 1
Size: 344840 Color: 1
Size: 296522 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 490920 Color: 1
Size: 257040 Color: 1
Size: 252041 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 379142 Color: 1
Size: 346057 Color: 1
Size: 274802 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 404618 Color: 1
Size: 300571 Color: 1
Size: 294812 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 395880 Color: 1
Size: 328284 Color: 1
Size: 275837 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 427811 Color: 1
Size: 315453 Color: 1
Size: 256737 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 401080 Color: 1
Size: 324952 Color: 1
Size: 273969 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 461894 Color: 1
Size: 284726 Color: 1
Size: 253381 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 398067 Color: 1
Size: 345775 Color: 1
Size: 256159 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 432877 Color: 1
Size: 294108 Color: 1
Size: 273016 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 488707 Color: 1
Size: 256500 Color: 1
Size: 254794 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 379830 Color: 1
Size: 340850 Color: 1
Size: 279321 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 362341 Color: 1
Size: 330962 Color: 1
Size: 306698 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 400271 Color: 1
Size: 319286 Color: 1
Size: 280444 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 495172 Color: 1
Size: 254393 Color: 1
Size: 250436 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 475845 Color: 1
Size: 273968 Color: 1
Size: 250188 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 497017 Color: 1
Size: 251920 Color: 1
Size: 251064 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 391066 Color: 1
Size: 329113 Color: 1
Size: 279822 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 416741 Color: 1
Size: 323442 Color: 1
Size: 259818 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 376006 Color: 1
Size: 328370 Color: 1
Size: 295625 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 387962 Color: 1
Size: 335173 Color: 1
Size: 276866 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 495127 Color: 1
Size: 254547 Color: 1
Size: 250327 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 380669 Color: 1
Size: 324405 Color: 1
Size: 294927 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 407308 Color: 1
Size: 339262 Color: 1
Size: 253431 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 386860 Color: 1
Size: 309606 Color: 1
Size: 303535 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 445499 Color: 1
Size: 296569 Color: 1
Size: 257933 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 496640 Color: 1
Size: 252384 Color: 1
Size: 250977 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 463937 Color: 1
Size: 280907 Color: 1
Size: 255157 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 378601 Color: 1
Size: 337940 Color: 1
Size: 283460 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 373164 Color: 1
Size: 329217 Color: 1
Size: 297620 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 368007 Color: 1
Size: 354365 Color: 1
Size: 277629 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 393912 Color: 1
Size: 327385 Color: 1
Size: 278704 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 461224 Color: 1
Size: 286886 Color: 1
Size: 251891 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 403711 Color: 1
Size: 331067 Color: 1
Size: 265223 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 385344 Color: 1
Size: 354369 Color: 1
Size: 260288 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 472016 Color: 1
Size: 265876 Color: 1
Size: 262109 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 394922 Color: 1
Size: 337595 Color: 1
Size: 267484 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 492738 Color: 1
Size: 256998 Color: 1
Size: 250265 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 494643 Color: 1
Size: 253375 Color: 1
Size: 251983 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 387847 Color: 1
Size: 325610 Color: 1
Size: 286544 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 380403 Color: 1
Size: 311322 Color: 0
Size: 308276 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 465335 Color: 1
Size: 278030 Color: 1
Size: 256636 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 395086 Color: 1
Size: 309726 Color: 1
Size: 295189 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 374035 Color: 1
Size: 329073 Color: 1
Size: 296893 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 454059 Color: 1
Size: 291479 Color: 1
Size: 254463 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 465847 Color: 1
Size: 282640 Color: 1
Size: 251514 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 358138 Color: 1
Size: 328134 Color: 1
Size: 313729 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 455394 Color: 1
Size: 277615 Color: 1
Size: 266992 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 398720 Color: 1
Size: 329427 Color: 1
Size: 271854 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 414498 Color: 1
Size: 316119 Color: 1
Size: 269384 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 446865 Color: 1
Size: 276914 Color: 1
Size: 276222 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 391380 Color: 1
Size: 312431 Color: 1
Size: 296190 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 495827 Color: 1
Size: 253293 Color: 1
Size: 250881 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 429682 Color: 1
Size: 316778 Color: 1
Size: 253541 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 384756 Color: 1
Size: 330282 Color: 1
Size: 284963 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 442381 Color: 1
Size: 307008 Color: 1
Size: 250612 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 372023 Color: 1
Size: 348444 Color: 1
Size: 279534 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 411723 Color: 1
Size: 336158 Color: 1
Size: 252120 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 425723 Color: 1
Size: 298283 Color: 0
Size: 275995 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 448376 Color: 1
Size: 294869 Color: 1
Size: 256756 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 391129 Color: 1
Size: 330223 Color: 1
Size: 278649 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 404003 Color: 1
Size: 317371 Color: 1
Size: 278627 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 455016 Color: 1
Size: 285981 Color: 1
Size: 259004 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 432224 Color: 1
Size: 302761 Color: 1
Size: 265016 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 477209 Color: 1
Size: 272255 Color: 1
Size: 250537 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 434510 Color: 1
Size: 295404 Color: 1
Size: 270087 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 408180 Color: 1
Size: 316866 Color: 0
Size: 274955 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 490818 Color: 1
Size: 258574 Color: 1
Size: 250609 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 414410 Color: 1
Size: 319367 Color: 1
Size: 266224 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 379850 Color: 1
Size: 342394 Color: 1
Size: 277757 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 434804 Color: 1
Size: 283551 Color: 0
Size: 281646 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 394975 Color: 1
Size: 321800 Color: 1
Size: 283226 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 360031 Color: 1
Size: 358416 Color: 1
Size: 281554 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 480690 Color: 1
Size: 266406 Color: 1
Size: 252905 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 384246 Color: 1
Size: 326410 Color: 1
Size: 289345 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 408890 Color: 1
Size: 299671 Color: 1
Size: 291440 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 377230 Color: 1
Size: 352705 Color: 1
Size: 270066 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 430129 Color: 1
Size: 301060 Color: 1
Size: 268812 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 407537 Color: 1
Size: 316015 Color: 1
Size: 276449 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 377641 Color: 1
Size: 346071 Color: 1
Size: 276289 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 475932 Color: 1
Size: 269506 Color: 1
Size: 254563 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 472126 Color: 1
Size: 269922 Color: 1
Size: 257953 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 413650 Color: 1
Size: 326445 Color: 1
Size: 259906 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 375930 Color: 1
Size: 361518 Color: 1
Size: 262553 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 411180 Color: 1
Size: 330830 Color: 1
Size: 257991 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 407584 Color: 1
Size: 328285 Color: 1
Size: 264132 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 409026 Color: 1
Size: 319595 Color: 1
Size: 271380 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 398547 Color: 1
Size: 302725 Color: 1
Size: 298729 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 420622 Color: 1
Size: 326280 Color: 1
Size: 253099 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 448649 Color: 1
Size: 293888 Color: 1
Size: 257464 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 470263 Color: 1
Size: 266172 Color: 1
Size: 263566 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 414099 Color: 1
Size: 324650 Color: 1
Size: 261252 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 464104 Color: 1
Size: 281111 Color: 1
Size: 254786 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 401479 Color: 1
Size: 306364 Color: 1
Size: 292158 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 360642 Color: 1
Size: 345898 Color: 1
Size: 293461 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 358656 Color: 1
Size: 354926 Color: 1
Size: 286419 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 468811 Color: 1
Size: 273064 Color: 1
Size: 258126 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 440464 Color: 1
Size: 291833 Color: 1
Size: 267704 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 365040 Color: 1
Size: 344817 Color: 1
Size: 290144 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 496814 Color: 1
Size: 251739 Color: 1
Size: 251448 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 472876 Color: 1
Size: 264921 Color: 0
Size: 262204 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 493605 Color: 1
Size: 255283 Color: 1
Size: 251113 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 374573 Color: 1
Size: 345753 Color: 1
Size: 279675 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 442661 Color: 1
Size: 291525 Color: 1
Size: 265815 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 397254 Color: 1
Size: 351245 Color: 1
Size: 251502 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 467513 Color: 1
Size: 279120 Color: 1
Size: 253368 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 462312 Color: 1
Size: 270814 Color: 1
Size: 266875 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 441837 Color: 1
Size: 300440 Color: 1
Size: 257724 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 351018 Color: 1
Size: 340584 Color: 1
Size: 308399 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 364209 Color: 1
Size: 333100 Color: 1
Size: 302692 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 393627 Color: 1
Size: 355317 Color: 1
Size: 251057 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 452793 Color: 1
Size: 280276 Color: 0
Size: 266932 Color: 1

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 432523 Color: 1
Size: 308164 Color: 1
Size: 259314 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 410051 Color: 1
Size: 297041 Color: 0
Size: 292909 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 475084 Color: 1
Size: 273549 Color: 1
Size: 251368 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 433854 Color: 1
Size: 303089 Color: 1
Size: 263058 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 397718 Color: 1
Size: 329949 Color: 1
Size: 272334 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 430937 Color: 1
Size: 299862 Color: 1
Size: 269202 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 386802 Color: 1
Size: 337197 Color: 1
Size: 276002 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 373882 Color: 1
Size: 329125 Color: 1
Size: 296994 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 491880 Color: 1
Size: 256334 Color: 1
Size: 251787 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 372494 Color: 1
Size: 342944 Color: 1
Size: 284563 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 443287 Color: 1
Size: 290503 Color: 1
Size: 266211 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 465231 Color: 1
Size: 268733 Color: 1
Size: 266037 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 417094 Color: 1
Size: 324137 Color: 1
Size: 258770 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 408538 Color: 1
Size: 295778 Color: 0
Size: 295685 Color: 1

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 468678 Color: 1
Size: 274270 Color: 0
Size: 257053 Color: 1

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 427582 Color: 1
Size: 309171 Color: 1
Size: 263248 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 381505 Color: 1
Size: 327066 Color: 1
Size: 291430 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 363381 Color: 1
Size: 323929 Color: 1
Size: 312691 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 366657 Color: 1
Size: 360539 Color: 1
Size: 272805 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 454142 Color: 1
Size: 291416 Color: 1
Size: 254443 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 442745 Color: 1
Size: 300107 Color: 1
Size: 257149 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 370993 Color: 1
Size: 370078 Color: 1
Size: 258930 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 374649 Color: 1
Size: 327952 Color: 0
Size: 297400 Color: 1

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 385318 Color: 1
Size: 310108 Color: 1
Size: 304575 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 400968 Color: 1
Size: 308629 Color: 1
Size: 290404 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 462042 Color: 1
Size: 282188 Color: 1
Size: 255771 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 448357 Color: 1
Size: 298183 Color: 1
Size: 253461 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 412780 Color: 1
Size: 315495 Color: 1
Size: 271726 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 472729 Color: 1
Size: 272897 Color: 1
Size: 254375 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 458583 Color: 1
Size: 280016 Color: 1
Size: 261402 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 433944 Color: 1
Size: 305012 Color: 1
Size: 261045 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 382612 Color: 1
Size: 349359 Color: 1
Size: 268030 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 350882 Color: 1
Size: 347152 Color: 1
Size: 301967 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 409293 Color: 1
Size: 333894 Color: 1
Size: 256814 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 386596 Color: 1
Size: 324955 Color: 1
Size: 288450 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 480078 Color: 1
Size: 264315 Color: 1
Size: 255608 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 390115 Color: 1
Size: 322055 Color: 1
Size: 287831 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 374712 Color: 1
Size: 366408 Color: 1
Size: 258881 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 373940 Color: 1
Size: 346946 Color: 1
Size: 279115 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 368656 Color: 1
Size: 346038 Color: 1
Size: 285307 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 380594 Color: 1
Size: 347943 Color: 1
Size: 271464 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 499766 Color: 1
Size: 250148 Color: 1
Size: 250087 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 379101 Color: 1
Size: 311779 Color: 1
Size: 309121 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 384775 Color: 1
Size: 345816 Color: 1
Size: 269410 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 429492 Color: 1
Size: 301312 Color: 1
Size: 269197 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 422305 Color: 1
Size: 309290 Color: 1
Size: 268406 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 476308 Color: 1
Size: 270123 Color: 1
Size: 253570 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 481765 Color: 1
Size: 267649 Color: 1
Size: 250587 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 452841 Color: 1
Size: 292028 Color: 1
Size: 255132 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 419014 Color: 1
Size: 290665 Color: 1
Size: 290322 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 361462 Color: 1
Size: 333283 Color: 1
Size: 305256 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 477961 Color: 1
Size: 270803 Color: 1
Size: 251237 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 427639 Color: 1
Size: 298358 Color: 1
Size: 274004 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 408207 Color: 1
Size: 313754 Color: 1
Size: 278040 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 365223 Color: 1
Size: 333927 Color: 1
Size: 300851 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 379656 Color: 1
Size: 360797 Color: 1
Size: 259548 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 434531 Color: 1
Size: 310110 Color: 1
Size: 255360 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 357010 Color: 1
Size: 327970 Color: 1
Size: 315021 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 496170 Color: 1
Size: 252929 Color: 1
Size: 250902 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 405925 Color: 1
Size: 314854 Color: 1
Size: 279222 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 466487 Color: 1
Size: 281050 Color: 1
Size: 252464 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 401876 Color: 1
Size: 344975 Color: 1
Size: 253150 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 375339 Color: 1
Size: 351511 Color: 1
Size: 273151 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 439542 Color: 1
Size: 284504 Color: 0
Size: 275955 Color: 1

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 350413 Color: 1
Size: 349729 Color: 1
Size: 299859 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 452286 Color: 1
Size: 282036 Color: 0
Size: 265679 Color: 1

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 371764 Color: 1
Size: 352432 Color: 1
Size: 275805 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 380994 Color: 1
Size: 336206 Color: 1
Size: 282801 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 475901 Color: 1
Size: 268980 Color: 1
Size: 255120 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 444479 Color: 1
Size: 279514 Color: 0
Size: 276008 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 474921 Color: 1
Size: 266608 Color: 1
Size: 258472 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 453256 Color: 1
Size: 274893 Color: 0
Size: 271852 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 431931 Color: 1
Size: 312943 Color: 1
Size: 255127 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 366670 Color: 1
Size: 322087 Color: 0
Size: 311244 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 443503 Color: 1
Size: 299574 Color: 1
Size: 256924 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 400884 Color: 1
Size: 338847 Color: 1
Size: 260270 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 494347 Color: 1
Size: 254217 Color: 1
Size: 251437 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 477655 Color: 1
Size: 268482 Color: 1
Size: 253864 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 388347 Color: 1
Size: 345488 Color: 1
Size: 266166 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 362282 Color: 1
Size: 342048 Color: 1
Size: 295671 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 397152 Color: 1
Size: 322010 Color: 1
Size: 280839 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 489785 Color: 1
Size: 257920 Color: 1
Size: 252296 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 350798 Color: 1
Size: 338922 Color: 1
Size: 310281 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 367752 Color: 1
Size: 365892 Color: 1
Size: 266357 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 476239 Color: 1
Size: 271229 Color: 1
Size: 252533 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 477541 Color: 1
Size: 265402 Color: 1
Size: 257058 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 459356 Color: 1
Size: 281086 Color: 1
Size: 259559 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 467592 Color: 1
Size: 270396 Color: 1
Size: 262013 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 426434 Color: 1
Size: 317511 Color: 1
Size: 256056 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 361231 Color: 1
Size: 341889 Color: 1
Size: 296881 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 462732 Color: 1
Size: 279970 Color: 1
Size: 257299 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 481253 Color: 1
Size: 259627 Color: 1
Size: 259121 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 461109 Color: 1
Size: 271456 Color: 1
Size: 267436 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 386813 Color: 1
Size: 322799 Color: 1
Size: 290389 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 375412 Color: 1
Size: 313071 Color: 1
Size: 311518 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 407247 Color: 1
Size: 316594 Color: 1
Size: 276160 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 483547 Color: 1
Size: 264635 Color: 1
Size: 251819 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 434427 Color: 1
Size: 304877 Color: 1
Size: 260697 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 441529 Color: 1
Size: 280111 Color: 1
Size: 278361 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 390653 Color: 1
Size: 335503 Color: 1
Size: 273845 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 422129 Color: 1
Size: 326610 Color: 1
Size: 251262 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 364878 Color: 1
Size: 350888 Color: 1
Size: 284235 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 384826 Color: 1
Size: 333115 Color: 1
Size: 282060 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 403099 Color: 1
Size: 301352 Color: 1
Size: 295550 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 423235 Color: 1
Size: 306781 Color: 1
Size: 269985 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 397632 Color: 1
Size: 308353 Color: 1
Size: 294016 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 409732 Color: 1
Size: 329621 Color: 1
Size: 260648 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 364747 Color: 1
Size: 340541 Color: 1
Size: 294713 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 409203 Color: 1
Size: 334016 Color: 1
Size: 256782 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 356508 Color: 1
Size: 339307 Color: 1
Size: 304186 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 392163 Color: 1
Size: 343195 Color: 1
Size: 264643 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 452192 Color: 1
Size: 282634 Color: 1
Size: 265175 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 424530 Color: 1
Size: 313538 Color: 1
Size: 261933 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 450783 Color: 1
Size: 298334 Color: 1
Size: 250884 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 363831 Color: 1
Size: 328844 Color: 0
Size: 307326 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 408828 Color: 1
Size: 302424 Color: 1
Size: 288749 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 364895 Color: 1
Size: 338835 Color: 1
Size: 296271 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 423166 Color: 1
Size: 319179 Color: 1
Size: 257656 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 494518 Color: 1
Size: 254846 Color: 1
Size: 250637 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 450300 Color: 1
Size: 290906 Color: 1
Size: 258795 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 413826 Color: 1
Size: 302026 Color: 1
Size: 284149 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 416785 Color: 1
Size: 316891 Color: 1
Size: 266325 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 451529 Color: 1
Size: 291718 Color: 1
Size: 256754 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 434771 Color: 1
Size: 308913 Color: 1
Size: 256317 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 483243 Color: 1
Size: 262542 Color: 1
Size: 254216 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 403779 Color: 1
Size: 321941 Color: 1
Size: 274281 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 450409 Color: 1
Size: 287370 Color: 1
Size: 262222 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 401615 Color: 1
Size: 312471 Color: 1
Size: 285915 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 431965 Color: 1
Size: 296084 Color: 1
Size: 271952 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 417375 Color: 1
Size: 295364 Color: 1
Size: 287262 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 416024 Color: 1
Size: 316344 Color: 1
Size: 267633 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 428101 Color: 1
Size: 308826 Color: 1
Size: 263074 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 372712 Color: 1
Size: 362008 Color: 1
Size: 265281 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 466940 Color: 1
Size: 268303 Color: 0
Size: 264758 Color: 1

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 484249 Color: 1
Size: 262010 Color: 1
Size: 253742 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 478613 Color: 1
Size: 269477 Color: 1
Size: 251911 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 359875 Color: 1
Size: 338346 Color: 1
Size: 301780 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 452801 Color: 1
Size: 288581 Color: 1
Size: 258619 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 491818 Color: 1
Size: 256193 Color: 1
Size: 251990 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 399267 Color: 1
Size: 334656 Color: 1
Size: 266078 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 438519 Color: 1
Size: 298684 Color: 1
Size: 262798 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 355829 Color: 1
Size: 354076 Color: 1
Size: 290096 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 471629 Color: 1
Size: 265232 Color: 1
Size: 263140 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 408379 Color: 1
Size: 308328 Color: 1
Size: 283294 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 379677 Color: 1
Size: 325166 Color: 1
Size: 295158 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 426432 Color: 1
Size: 310516 Color: 1
Size: 263053 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 391381 Color: 1
Size: 321456 Color: 1
Size: 287164 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 391504 Color: 1
Size: 333501 Color: 1
Size: 274996 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 387588 Color: 1
Size: 317222 Color: 0
Size: 295191 Color: 1

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 402852 Color: 1
Size: 310284 Color: 1
Size: 286865 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 381194 Color: 1
Size: 352180 Color: 1
Size: 266627 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 479902 Color: 1
Size: 266439 Color: 1
Size: 253660 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 385735 Color: 1
Size: 350050 Color: 1
Size: 264216 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 452981 Color: 1
Size: 292822 Color: 1
Size: 254198 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 453462 Color: 1
Size: 290394 Color: 1
Size: 256145 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 498406 Color: 1
Size: 250912 Color: 0
Size: 250683 Color: 1

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 430382 Color: 1
Size: 313184 Color: 1
Size: 256435 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 406783 Color: 1
Size: 318398 Color: 1
Size: 274820 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 447817 Color: 1
Size: 299284 Color: 1
Size: 252900 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 410582 Color: 1
Size: 335445 Color: 1
Size: 253974 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 449663 Color: 1
Size: 279789 Color: 1
Size: 270549 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 472205 Color: 1
Size: 268292 Color: 0
Size: 259504 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 497475 Color: 1
Size: 252442 Color: 1
Size: 250084 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 415135 Color: 1
Size: 308531 Color: 1
Size: 276335 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 388779 Color: 1
Size: 318112 Color: 1
Size: 293110 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 442425 Color: 1
Size: 284936 Color: 1
Size: 272640 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 433778 Color: 1
Size: 287599 Color: 0
Size: 278624 Color: 1

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 428901 Color: 1
Size: 296287 Color: 1
Size: 274813 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 373027 Color: 1
Size: 359713 Color: 1
Size: 267261 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 475386 Color: 1
Size: 267980 Color: 1
Size: 256635 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 423731 Color: 1
Size: 320441 Color: 1
Size: 255829 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 462198 Color: 1
Size: 274243 Color: 1
Size: 263560 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 362022 Color: 1
Size: 339271 Color: 1
Size: 298708 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 412285 Color: 1
Size: 331979 Color: 1
Size: 255737 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 466679 Color: 1
Size: 277407 Color: 1
Size: 255915 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 426837 Color: 1
Size: 290238 Color: 0
Size: 282926 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 496787 Color: 1
Size: 252538 Color: 1
Size: 250676 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 415038 Color: 1
Size: 304947 Color: 0
Size: 280016 Color: 1

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 425031 Color: 1
Size: 323163 Color: 1
Size: 251807 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 445411 Color: 1
Size: 296340 Color: 1
Size: 258250 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 450861 Color: 1
Size: 275200 Color: 1
Size: 273940 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 390579 Color: 1
Size: 341305 Color: 1
Size: 268117 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 415289 Color: 1
Size: 302892 Color: 1
Size: 281820 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 367219 Color: 1
Size: 352264 Color: 1
Size: 280518 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 371259 Color: 1
Size: 335240 Color: 1
Size: 293502 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 474211 Color: 1
Size: 274653 Color: 1
Size: 251137 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 480086 Color: 1
Size: 264067 Color: 1
Size: 255848 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 370731 Color: 1
Size: 315427 Color: 1
Size: 313843 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 360174 Color: 1
Size: 358015 Color: 1
Size: 281812 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 474735 Color: 1
Size: 268640 Color: 1
Size: 256626 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 493152 Color: 1
Size: 254723 Color: 0
Size: 252126 Color: 1

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 359770 Color: 1
Size: 345964 Color: 1
Size: 294267 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 446703 Color: 1
Size: 295667 Color: 1
Size: 257631 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 364725 Color: 1
Size: 322193 Color: 0
Size: 313083 Color: 1

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 404091 Color: 1
Size: 320136 Color: 1
Size: 275774 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 361359 Color: 1
Size: 354339 Color: 1
Size: 284303 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 429574 Color: 1
Size: 312641 Color: 1
Size: 257786 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 408629 Color: 1
Size: 299996 Color: 1
Size: 291376 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 357894 Color: 1
Size: 331793 Color: 1
Size: 310314 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 437428 Color: 1
Size: 288273 Color: 1
Size: 274300 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 352748 Color: 1
Size: 349820 Color: 1
Size: 297433 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 435023 Color: 1
Size: 300792 Color: 1
Size: 264186 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 418765 Color: 1
Size: 308452 Color: 1
Size: 272784 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 364144 Color: 1
Size: 328336 Color: 1
Size: 307521 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 372544 Color: 1
Size: 324432 Color: 1
Size: 303025 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 385714 Color: 1
Size: 324330 Color: 1
Size: 289957 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 421823 Color: 1
Size: 324376 Color: 1
Size: 253802 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 437589 Color: 1
Size: 302979 Color: 1
Size: 259433 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 360157 Color: 1
Size: 357111 Color: 1
Size: 282733 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 448639 Color: 1
Size: 295153 Color: 1
Size: 256209 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 363796 Color: 1
Size: 343896 Color: 1
Size: 292309 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 460095 Color: 1
Size: 284351 Color: 1
Size: 255555 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 361386 Color: 1
Size: 336074 Color: 1
Size: 302541 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 413201 Color: 1
Size: 306857 Color: 0
Size: 279943 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 377073 Color: 1
Size: 345999 Color: 1
Size: 276929 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 382825 Color: 1
Size: 317152 Color: 1
Size: 300024 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 365726 Color: 1
Size: 362096 Color: 1
Size: 272179 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 496735 Color: 1
Size: 252937 Color: 1
Size: 250329 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 405601 Color: 1
Size: 332944 Color: 1
Size: 261456 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 371494 Color: 1
Size: 342288 Color: 1
Size: 286219 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 487332 Color: 1
Size: 256439 Color: 0
Size: 256230 Color: 1

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 369486 Color: 1
Size: 324402 Color: 1
Size: 306113 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 406364 Color: 1
Size: 329142 Color: 1
Size: 264495 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 455128 Color: 1
Size: 288155 Color: 1
Size: 256718 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 401378 Color: 1
Size: 322785 Color: 0
Size: 275838 Color: 1

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 359716 Color: 1
Size: 352320 Color: 1
Size: 287965 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 366040 Color: 1
Size: 335233 Color: 1
Size: 298728 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 356080 Color: 1
Size: 336118 Color: 1
Size: 307803 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 477166 Color: 1
Size: 270259 Color: 1
Size: 252576 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 411580 Color: 1
Size: 320913 Color: 1
Size: 267508 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 432900 Color: 1
Size: 284122 Color: 0
Size: 282979 Color: 1

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 414148 Color: 1
Size: 318610 Color: 1
Size: 267243 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 346318 Color: 1
Size: 338847 Color: 1
Size: 314836 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 494837 Color: 1
Size: 254634 Color: 1
Size: 250530 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 464169 Color: 1
Size: 268141 Color: 0
Size: 267691 Color: 1

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 399162 Color: 1
Size: 330148 Color: 1
Size: 270691 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 393581 Color: 1
Size: 349149 Color: 1
Size: 257271 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 431741 Color: 1
Size: 294884 Color: 1
Size: 273376 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 348427 Color: 1
Size: 338329 Color: 1
Size: 313245 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 388537 Color: 1
Size: 327425 Color: 1
Size: 284039 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 398339 Color: 1
Size: 339710 Color: 1
Size: 261952 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 433629 Color: 1
Size: 299143 Color: 1
Size: 267229 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 375599 Color: 1
Size: 350266 Color: 1
Size: 274136 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 358135 Color: 1
Size: 323922 Color: 0
Size: 317944 Color: 1

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 450311 Color: 1
Size: 294113 Color: 1
Size: 255577 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 393321 Color: 1
Size: 340667 Color: 1
Size: 266013 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 348548 Color: 1
Size: 334320 Color: 1
Size: 317133 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 481770 Color: 1
Size: 265486 Color: 1
Size: 252745 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 419870 Color: 1
Size: 309664 Color: 0
Size: 270467 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 395086 Color: 1
Size: 306172 Color: 1
Size: 298743 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 462973 Color: 1
Size: 282899 Color: 1
Size: 254129 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 461490 Color: 1
Size: 280253 Color: 1
Size: 258258 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 415293 Color: 1
Size: 318067 Color: 1
Size: 266641 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 367538 Color: 1
Size: 356318 Color: 1
Size: 276145 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 497434 Color: 1
Size: 252371 Color: 1
Size: 250196 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 456779 Color: 1
Size: 281535 Color: 1
Size: 261687 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 409069 Color: 1
Size: 335027 Color: 1
Size: 255905 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 410056 Color: 1
Size: 310370 Color: 1
Size: 279575 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 398169 Color: 1
Size: 343908 Color: 1
Size: 257924 Color: 0

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 382866 Color: 1
Size: 311465 Color: 1
Size: 305670 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 489489 Color: 1
Size: 257578 Color: 1
Size: 252934 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 349276 Color: 1
Size: 340598 Color: 1
Size: 310127 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 383637 Color: 1
Size: 339827 Color: 1
Size: 276537 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 491825 Color: 1
Size: 257753 Color: 1
Size: 250423 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 382738 Color: 1
Size: 336499 Color: 1
Size: 280764 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 432394 Color: 1
Size: 312218 Color: 1
Size: 255389 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 467143 Color: 1
Size: 282713 Color: 1
Size: 250145 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 415672 Color: 1
Size: 323851 Color: 1
Size: 260478 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 480798 Color: 1
Size: 266235 Color: 1
Size: 252968 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 378229 Color: 1
Size: 350743 Color: 1
Size: 271029 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 496248 Color: 1
Size: 252855 Color: 1
Size: 250898 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 377074 Color: 1
Size: 351313 Color: 1
Size: 271614 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 379186 Color: 1
Size: 357916 Color: 1
Size: 262899 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 494485 Color: 1
Size: 255172 Color: 1
Size: 250344 Color: 0

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 365922 Color: 1
Size: 336893 Color: 1
Size: 297186 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 446617 Color: 1
Size: 292781 Color: 1
Size: 260603 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 499084 Color: 1
Size: 250547 Color: 1
Size: 250370 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 491582 Color: 1
Size: 257239 Color: 1
Size: 251180 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 487719 Color: 1
Size: 257273 Color: 1
Size: 255009 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 442822 Color: 1
Size: 301792 Color: 1
Size: 255387 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 499114 Color: 1
Size: 250606 Color: 1
Size: 250281 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 377786 Color: 1
Size: 334549 Color: 1
Size: 287666 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 488187 Color: 1
Size: 260457 Color: 1
Size: 251357 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 460198 Color: 1
Size: 288436 Color: 1
Size: 251367 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 386833 Color: 1
Size: 325481 Color: 1
Size: 287687 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 473584 Color: 1
Size: 273808 Color: 1
Size: 252609 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 469063 Color: 1
Size: 266091 Color: 1
Size: 264847 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 442447 Color: 1
Size: 293584 Color: 1
Size: 263970 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 482866 Color: 1
Size: 259692 Color: 0
Size: 257443 Color: 1

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 383982 Color: 1
Size: 355909 Color: 1
Size: 260110 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 369335 Color: 1
Size: 363299 Color: 1
Size: 267367 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 398604 Color: 1
Size: 303075 Color: 1
Size: 298322 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 360076 Color: 1
Size: 348847 Color: 1
Size: 291078 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 352955 Color: 1
Size: 330180 Color: 1
Size: 316866 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 360891 Color: 1
Size: 325593 Color: 1
Size: 313517 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 388348 Color: 1
Size: 332731 Color: 1
Size: 278922 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 443497 Color: 1
Size: 293526 Color: 1
Size: 262978 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 474116 Color: 1
Size: 273640 Color: 1
Size: 252245 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 488599 Color: 1
Size: 257769 Color: 1
Size: 253633 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 474690 Color: 1
Size: 272598 Color: 1
Size: 252713 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 499091 Color: 1
Size: 250895 Color: 1
Size: 250015 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 400147 Color: 1
Size: 346469 Color: 1
Size: 253385 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 407099 Color: 1
Size: 326802 Color: 1
Size: 266100 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 480417 Color: 1
Size: 266638 Color: 1
Size: 252946 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 400775 Color: 1
Size: 313555 Color: 0
Size: 285671 Color: 1

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 364431 Color: 1
Size: 331750 Color: 1
Size: 303820 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 431142 Color: 1
Size: 307841 Color: 1
Size: 261018 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 411195 Color: 1
Size: 319156 Color: 1
Size: 269650 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 378234 Color: 1
Size: 327862 Color: 1
Size: 293905 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 406108 Color: 1
Size: 313766 Color: 1
Size: 280127 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 380476 Color: 1
Size: 362413 Color: 1
Size: 257112 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 396981 Color: 1
Size: 352256 Color: 1
Size: 250764 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 387708 Color: 1
Size: 336624 Color: 1
Size: 275669 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 498714 Color: 1
Size: 251083 Color: 1
Size: 250204 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 497526 Color: 1
Size: 251656 Color: 1
Size: 250819 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 438402 Color: 1
Size: 310636 Color: 1
Size: 250963 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 424651 Color: 1
Size: 296291 Color: 1
Size: 279059 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 396133 Color: 1
Size: 326373 Color: 1
Size: 277495 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 466834 Color: 1
Size: 278840 Color: 1
Size: 254327 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 493917 Color: 1
Size: 254000 Color: 1
Size: 252084 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 350565 Color: 1
Size: 343869 Color: 1
Size: 305567 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 431812 Color: 1
Size: 313750 Color: 1
Size: 254439 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 419796 Color: 1
Size: 307497 Color: 1
Size: 272708 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 497838 Color: 1
Size: 251806 Color: 1
Size: 250357 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 377423 Color: 1
Size: 334581 Color: 1
Size: 287997 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 386896 Color: 1
Size: 338484 Color: 1
Size: 274621 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 368023 Color: 1
Size: 354962 Color: 1
Size: 277016 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 381088 Color: 1
Size: 320943 Color: 1
Size: 297970 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 397734 Color: 1
Size: 330766 Color: 1
Size: 271501 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 407373 Color: 1
Size: 328394 Color: 1
Size: 264234 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 371740 Color: 1
Size: 340109 Color: 1
Size: 288152 Color: 0

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 360673 Color: 1
Size: 342325 Color: 1
Size: 297003 Color: 0

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 371907 Color: 1
Size: 350905 Color: 1
Size: 277189 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 373480 Color: 1
Size: 371427 Color: 1
Size: 255094 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 369852 Color: 1
Size: 334328 Color: 1
Size: 295821 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 439272 Color: 1
Size: 288776 Color: 1
Size: 271953 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 452912 Color: 1
Size: 296149 Color: 1
Size: 250940 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 369858 Color: 1
Size: 343324 Color: 1
Size: 286819 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 409502 Color: 1
Size: 318600 Color: 1
Size: 271899 Color: 0

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 448425 Color: 1
Size: 297625 Color: 1
Size: 253951 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 360972 Color: 1
Size: 360382 Color: 1
Size: 278647 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 373942 Color: 1
Size: 360749 Color: 1
Size: 265310 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 367387 Color: 1
Size: 338727 Color: 1
Size: 293887 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 446224 Color: 1
Size: 295071 Color: 1
Size: 258706 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 454542 Color: 1
Size: 281573 Color: 1
Size: 263886 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 402935 Color: 1
Size: 345794 Color: 1
Size: 251272 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 389777 Color: 1
Size: 324561 Color: 1
Size: 285663 Color: 0

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 424494 Color: 1
Size: 298563 Color: 1
Size: 276944 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 387518 Color: 1
Size: 351177 Color: 1
Size: 261306 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 369981 Color: 1
Size: 331440 Color: 1
Size: 298580 Color: 0

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 496074 Color: 1
Size: 253332 Color: 1
Size: 250595 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 477873 Color: 1
Size: 264148 Color: 0
Size: 257980 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 448717 Color: 1
Size: 299926 Color: 1
Size: 251358 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 408404 Color: 1
Size: 326842 Color: 1
Size: 264755 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 387843 Color: 1
Size: 346422 Color: 1
Size: 265736 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 358279 Color: 1
Size: 325560 Color: 1
Size: 316162 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 432447 Color: 1
Size: 307666 Color: 1
Size: 259888 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 413668 Color: 1
Size: 310863 Color: 1
Size: 275470 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 361839 Color: 1
Size: 320354 Color: 1
Size: 317808 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 424724 Color: 1
Size: 311203 Color: 1
Size: 264074 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 409643 Color: 1
Size: 298781 Color: 1
Size: 291577 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 489551 Color: 1
Size: 258490 Color: 1
Size: 251960 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 385252 Color: 1
Size: 328847 Color: 1
Size: 285902 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 364163 Color: 1
Size: 319839 Color: 0
Size: 315999 Color: 1

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 367210 Color: 1
Size: 338433 Color: 1
Size: 294358 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 370208 Color: 1
Size: 343954 Color: 1
Size: 285839 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 472888 Color: 1
Size: 273071 Color: 1
Size: 254042 Color: 0

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 420983 Color: 1
Size: 318567 Color: 1
Size: 260451 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 361293 Color: 1
Size: 336724 Color: 1
Size: 301984 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 411664 Color: 1
Size: 317275 Color: 1
Size: 271062 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 350158 Color: 1
Size: 341878 Color: 1
Size: 307965 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 471538 Color: 1
Size: 272688 Color: 1
Size: 255775 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 369312 Color: 1
Size: 345998 Color: 1
Size: 284691 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 447827 Color: 1
Size: 297339 Color: 1
Size: 254835 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 490099 Color: 1
Size: 259683 Color: 1
Size: 250219 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 431310 Color: 1
Size: 313817 Color: 1
Size: 254874 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 483829 Color: 1
Size: 264347 Color: 1
Size: 251825 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 473745 Color: 1
Size: 272717 Color: 1
Size: 253539 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 444915 Color: 1
Size: 293711 Color: 1
Size: 261375 Color: 0

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 412808 Color: 1
Size: 332302 Color: 1
Size: 254891 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 457149 Color: 1
Size: 291070 Color: 1
Size: 251782 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 485629 Color: 1
Size: 261123 Color: 1
Size: 253249 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 427740 Color: 1
Size: 290920 Color: 0
Size: 281341 Color: 1

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 444874 Color: 1
Size: 294735 Color: 1
Size: 260392 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 368304 Color: 1
Size: 348535 Color: 1
Size: 283162 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 358867 Color: 1
Size: 355484 Color: 1
Size: 285650 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 355826 Color: 1
Size: 329697 Color: 1
Size: 314478 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 362536 Color: 1
Size: 322319 Color: 1
Size: 315146 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 444015 Color: 1
Size: 300225 Color: 1
Size: 255761 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 477308 Color: 1
Size: 268427 Color: 1
Size: 254266 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 465427 Color: 1
Size: 272805 Color: 1
Size: 261769 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 373896 Color: 1
Size: 352601 Color: 1
Size: 273504 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 388350 Color: 1
Size: 310141 Color: 1
Size: 301510 Color: 0

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 375606 Color: 1
Size: 347379 Color: 1
Size: 277016 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 387015 Color: 1
Size: 321381 Color: 0
Size: 291605 Color: 1

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 497284 Color: 1
Size: 252087 Color: 1
Size: 250630 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 404150 Color: 1
Size: 320587 Color: 1
Size: 275264 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 448696 Color: 1
Size: 281346 Color: 1
Size: 269959 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 482734 Color: 1
Size: 263473 Color: 1
Size: 253794 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 428643 Color: 1
Size: 312675 Color: 1
Size: 258683 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 461480 Color: 1
Size: 273330 Color: 1
Size: 265191 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 413640 Color: 1
Size: 308485 Color: 1
Size: 277876 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 424822 Color: 1
Size: 315144 Color: 1
Size: 260035 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 409535 Color: 1
Size: 309946 Color: 0
Size: 280520 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 362159 Color: 1
Size: 332200 Color: 1
Size: 305642 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 376535 Color: 1
Size: 360149 Color: 1
Size: 263317 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 443100 Color: 1
Size: 280194 Color: 0
Size: 276707 Color: 1

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 495192 Color: 1
Size: 253960 Color: 1
Size: 250849 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 448181 Color: 1
Size: 289302 Color: 1
Size: 262518 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 417853 Color: 1
Size: 327500 Color: 1
Size: 254648 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 391875 Color: 1
Size: 322361 Color: 1
Size: 285765 Color: 0

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 384149 Color: 1
Size: 335241 Color: 1
Size: 280611 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 491343 Color: 1
Size: 257643 Color: 1
Size: 251015 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 363036 Color: 1
Size: 358392 Color: 1
Size: 278573 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 428498 Color: 1
Size: 316381 Color: 1
Size: 255122 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 382082 Color: 1
Size: 329982 Color: 0
Size: 287937 Color: 1

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 424660 Color: 1
Size: 317969 Color: 1
Size: 257372 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 349425 Color: 1
Size: 336331 Color: 1
Size: 314245 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 354428 Color: 1
Size: 332617 Color: 1
Size: 312956 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 435429 Color: 1
Size: 295519 Color: 1
Size: 269053 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 407059 Color: 1
Size: 329942 Color: 1
Size: 263000 Color: 0

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 365276 Color: 1
Size: 360559 Color: 1
Size: 274166 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 387345 Color: 1
Size: 334488 Color: 1
Size: 278168 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 395315 Color: 1
Size: 321658 Color: 1
Size: 283028 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 385841 Color: 1
Size: 313187 Color: 1
Size: 300973 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 370905 Color: 1
Size: 335610 Color: 1
Size: 293486 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 412575 Color: 1
Size: 323818 Color: 1
Size: 263608 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 371054 Color: 1
Size: 359185 Color: 1
Size: 269762 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 394353 Color: 1
Size: 332983 Color: 1
Size: 272665 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 385613 Color: 1
Size: 340744 Color: 1
Size: 273644 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 372692 Color: 1
Size: 328125 Color: 1
Size: 299184 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 417790 Color: 1
Size: 321105 Color: 1
Size: 261106 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 362063 Color: 1
Size: 360679 Color: 1
Size: 277259 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 382365 Color: 1
Size: 335230 Color: 1
Size: 282406 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 487833 Color: 1
Size: 259400 Color: 1
Size: 252768 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 417029 Color: 1
Size: 309983 Color: 1
Size: 272989 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 375822 Color: 1
Size: 371129 Color: 1
Size: 253050 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 390486 Color: 1
Size: 308997 Color: 1
Size: 300518 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 478045 Color: 1
Size: 268188 Color: 1
Size: 253768 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 475492 Color: 1
Size: 271121 Color: 1
Size: 253388 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 465060 Color: 1
Size: 278305 Color: 1
Size: 256636 Color: 0

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 474021 Color: 1
Size: 270024 Color: 1
Size: 255956 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 419214 Color: 1
Size: 314701 Color: 1
Size: 266086 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 442890 Color: 1
Size: 298077 Color: 1
Size: 259034 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 386439 Color: 1
Size: 319001 Color: 1
Size: 294561 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 407797 Color: 1
Size: 324932 Color: 1
Size: 267272 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 360217 Color: 1
Size: 355091 Color: 1
Size: 284693 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 435817 Color: 1
Size: 305653 Color: 1
Size: 258531 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 461103 Color: 1
Size: 282594 Color: 1
Size: 256304 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 383165 Color: 1
Size: 337541 Color: 1
Size: 279295 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 403048 Color: 1
Size: 333034 Color: 1
Size: 263919 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 488736 Color: 1
Size: 260955 Color: 1
Size: 250310 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 377442 Color: 1
Size: 338037 Color: 1
Size: 284522 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 355278 Color: 1
Size: 334867 Color: 1
Size: 309856 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 465804 Color: 1
Size: 280511 Color: 1
Size: 253686 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 354275 Color: 1
Size: 339513 Color: 1
Size: 306213 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 428465 Color: 1
Size: 307691 Color: 1
Size: 263845 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 499792 Color: 1
Size: 250170 Color: 1
Size: 250039 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 369657 Color: 1
Size: 353784 Color: 1
Size: 276560 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 343190 Color: 1
Size: 336693 Color: 1
Size: 320118 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 477997 Color: 1
Size: 270520 Color: 1
Size: 251484 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 425241 Color: 1
Size: 303902 Color: 0
Size: 270858 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 454150 Color: 1
Size: 285192 Color: 1
Size: 260659 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 454800 Color: 1
Size: 291207 Color: 1
Size: 253994 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 355717 Color: 1
Size: 355043 Color: 1
Size: 289241 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 386065 Color: 1
Size: 358887 Color: 1
Size: 255049 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 380477 Color: 1
Size: 353509 Color: 1
Size: 266015 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 497429 Color: 1
Size: 252427 Color: 1
Size: 250145 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 454394 Color: 1
Size: 280711 Color: 1
Size: 264896 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 444731 Color: 1
Size: 285359 Color: 0
Size: 269911 Color: 1

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 434952 Color: 1
Size: 287814 Color: 1
Size: 277235 Color: 0

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 380539 Color: 1
Size: 353593 Color: 1
Size: 265869 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 462336 Color: 1
Size: 276493 Color: 1
Size: 261172 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 436716 Color: 1
Size: 310465 Color: 1
Size: 252820 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 390478 Color: 1
Size: 340587 Color: 1
Size: 268936 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 480794 Color: 1
Size: 260467 Color: 1
Size: 258740 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 349993 Color: 1
Size: 344708 Color: 1
Size: 305300 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 474586 Color: 1
Size: 266474 Color: 1
Size: 258941 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 436658 Color: 1
Size: 312547 Color: 1
Size: 250796 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 428811 Color: 1
Size: 305033 Color: 1
Size: 266157 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 370620 Color: 1
Size: 340572 Color: 1
Size: 288809 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 468173 Color: 1
Size: 277235 Color: 1
Size: 254593 Color: 0

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 420923 Color: 1
Size: 303836 Color: 1
Size: 275242 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 433258 Color: 1
Size: 285144 Color: 1
Size: 281599 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 435050 Color: 1
Size: 303275 Color: 1
Size: 261676 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 375428 Color: 1
Size: 346212 Color: 1
Size: 278361 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 360496 Color: 1
Size: 339076 Color: 1
Size: 300429 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 391722 Color: 1
Size: 312970 Color: 1
Size: 295309 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 456290 Color: 1
Size: 284567 Color: 1
Size: 259144 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 418409 Color: 1
Size: 304406 Color: 1
Size: 277186 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 454014 Color: 1
Size: 295135 Color: 0
Size: 250852 Color: 1

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 437770 Color: 1
Size: 305929 Color: 1
Size: 256302 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 361103 Color: 1
Size: 351867 Color: 1
Size: 287031 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 411683 Color: 1
Size: 318918 Color: 1
Size: 269400 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 429413 Color: 1
Size: 300003 Color: 1
Size: 270585 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 479497 Color: 1
Size: 267991 Color: 1
Size: 252513 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 456216 Color: 1
Size: 287025 Color: 1
Size: 256760 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 406826 Color: 1
Size: 338935 Color: 1
Size: 254240 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 366210 Color: 1
Size: 335173 Color: 1
Size: 298618 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 432252 Color: 1
Size: 289962 Color: 1
Size: 277787 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 388963 Color: 1
Size: 342505 Color: 1
Size: 268533 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 420541 Color: 1
Size: 315544 Color: 1
Size: 263916 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 373430 Color: 1
Size: 335663 Color: 1
Size: 290908 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 368325 Color: 1
Size: 320681 Color: 0
Size: 310995 Color: 1

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 382281 Color: 1
Size: 318557 Color: 1
Size: 299163 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 425808 Color: 1
Size: 302479 Color: 1
Size: 271714 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 369292 Color: 1
Size: 358958 Color: 1
Size: 271751 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 460576 Color: 1
Size: 277371 Color: 1
Size: 262054 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 434199 Color: 1
Size: 289013 Color: 0
Size: 276789 Color: 1

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 422831 Color: 1
Size: 325295 Color: 1
Size: 251875 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 465312 Color: 1
Size: 267533 Color: 1
Size: 267156 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 403772 Color: 1
Size: 301741 Color: 1
Size: 294488 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 388211 Color: 1
Size: 310541 Color: 1
Size: 301249 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 492939 Color: 1
Size: 255761 Color: 1
Size: 251301 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 423489 Color: 1
Size: 317950 Color: 1
Size: 258562 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 413810 Color: 1
Size: 315633 Color: 1
Size: 270558 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 464694 Color: 1
Size: 271983 Color: 1
Size: 263324 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 374834 Color: 1
Size: 337158 Color: 1
Size: 288009 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 451052 Color: 1
Size: 278133 Color: 0
Size: 270816 Color: 1

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 412713 Color: 1
Size: 316862 Color: 1
Size: 270426 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 423013 Color: 1
Size: 293824 Color: 1
Size: 283164 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 391229 Color: 1
Size: 339154 Color: 1
Size: 269618 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 473130 Color: 1
Size: 271548 Color: 1
Size: 255323 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 365171 Color: 1
Size: 334132 Color: 1
Size: 300698 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 397914 Color: 1
Size: 310885 Color: 0
Size: 291202 Color: 1

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 371458 Color: 1
Size: 345198 Color: 1
Size: 283345 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 483337 Color: 1
Size: 265999 Color: 1
Size: 250665 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 398189 Color: 1
Size: 310205 Color: 0
Size: 291607 Color: 1

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 366270 Color: 1
Size: 349045 Color: 1
Size: 284686 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 391402 Color: 1
Size: 304443 Color: 0
Size: 304156 Color: 1

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 472686 Color: 1
Size: 274793 Color: 1
Size: 252522 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 447331 Color: 1
Size: 300082 Color: 1
Size: 252588 Color: 0

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 486398 Color: 1
Size: 260194 Color: 1
Size: 253409 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 417870 Color: 1
Size: 308019 Color: 1
Size: 274112 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 383908 Color: 1
Size: 355574 Color: 1
Size: 260519 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 379744 Color: 1
Size: 369341 Color: 1
Size: 250916 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 364961 Color: 1
Size: 324685 Color: 0
Size: 310355 Color: 1

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 429462 Color: 1
Size: 300459 Color: 1
Size: 270080 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 487741 Color: 1
Size: 256486 Color: 1
Size: 255774 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 412567 Color: 1
Size: 297476 Color: 0
Size: 289958 Color: 1

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 467006 Color: 1
Size: 272591 Color: 0
Size: 260404 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 494466 Color: 1
Size: 254334 Color: 1
Size: 251201 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 445789 Color: 1
Size: 298017 Color: 1
Size: 256195 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 388526 Color: 1
Size: 313761 Color: 1
Size: 297714 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 353218 Color: 1
Size: 342441 Color: 1
Size: 304342 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 455581 Color: 1
Size: 272458 Color: 1
Size: 271962 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 350850 Color: 1
Size: 342013 Color: 1
Size: 307138 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 386557 Color: 1
Size: 357038 Color: 1
Size: 256406 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 499798 Color: 1
Size: 250155 Color: 1
Size: 250048 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 378724 Color: 1
Size: 357489 Color: 1
Size: 263788 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 477298 Color: 1
Size: 262961 Color: 0
Size: 259742 Color: 1

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 402085 Color: 1
Size: 331941 Color: 1
Size: 265975 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 356437 Color: 1
Size: 327345 Color: 1
Size: 316219 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 423122 Color: 1
Size: 303552 Color: 1
Size: 273327 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 458513 Color: 1
Size: 284299 Color: 1
Size: 257189 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 388807 Color: 1
Size: 339558 Color: 1
Size: 271636 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 405818 Color: 1
Size: 332295 Color: 1
Size: 261888 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 466247 Color: 1
Size: 281812 Color: 1
Size: 251942 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 354605 Color: 1
Size: 331802 Color: 1
Size: 313594 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 471227 Color: 1
Size: 275759 Color: 1
Size: 253015 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 388732 Color: 1
Size: 318876 Color: 0
Size: 292393 Color: 1

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 493272 Color: 1
Size: 255988 Color: 1
Size: 250741 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 372453 Color: 1
Size: 352986 Color: 1
Size: 274562 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 460338 Color: 1
Size: 286117 Color: 1
Size: 253546 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 421440 Color: 1
Size: 321369 Color: 1
Size: 257192 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 343467 Color: 1
Size: 331608 Color: 1
Size: 324926 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 424852 Color: 1
Size: 304674 Color: 1
Size: 270475 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 350160 Color: 1
Size: 325677 Color: 1
Size: 324164 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 437630 Color: 1
Size: 307258 Color: 1
Size: 255113 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 474315 Color: 1
Size: 272659 Color: 1
Size: 253027 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 419308 Color: 1
Size: 312454 Color: 1
Size: 268239 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 390344 Color: 1
Size: 312220 Color: 1
Size: 297437 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 483575 Color: 1
Size: 264240 Color: 1
Size: 252186 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 391654 Color: 1
Size: 345185 Color: 1
Size: 263162 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 415316 Color: 1
Size: 315394 Color: 1
Size: 269291 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 469119 Color: 1
Size: 278876 Color: 1
Size: 252006 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 354757 Color: 1
Size: 337477 Color: 1
Size: 307767 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 453111 Color: 1
Size: 288685 Color: 1
Size: 258205 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 461514 Color: 1
Size: 275058 Color: 1
Size: 263429 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 432537 Color: 1
Size: 308428 Color: 1
Size: 259036 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 348025 Color: 1
Size: 340691 Color: 1
Size: 311285 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 484704 Color: 1
Size: 261772 Color: 1
Size: 253525 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 437203 Color: 1
Size: 309344 Color: 1
Size: 253454 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 474850 Color: 1
Size: 268647 Color: 1
Size: 256504 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 356712 Color: 1
Size: 335053 Color: 1
Size: 308236 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 433751 Color: 1
Size: 307452 Color: 1
Size: 258798 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 386111 Color: 1
Size: 314392 Color: 1
Size: 299498 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 395163 Color: 1
Size: 338459 Color: 1
Size: 266379 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 448822 Color: 1
Size: 296042 Color: 1
Size: 255137 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 356360 Color: 1
Size: 352959 Color: 1
Size: 290682 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 368546 Color: 1
Size: 352115 Color: 1
Size: 279340 Color: 0

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 390960 Color: 1
Size: 315813 Color: 0
Size: 293228 Color: 1

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 459719 Color: 1
Size: 284076 Color: 1
Size: 256206 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 400119 Color: 1
Size: 340874 Color: 1
Size: 259008 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 395702 Color: 1
Size: 328295 Color: 1
Size: 276004 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 381530 Color: 1
Size: 315244 Color: 0
Size: 303227 Color: 1

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 441116 Color: 1
Size: 304345 Color: 1
Size: 254540 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 373042 Color: 1
Size: 350422 Color: 1
Size: 276537 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 360009 Color: 1
Size: 336925 Color: 1
Size: 303067 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 498631 Color: 1
Size: 250860 Color: 1
Size: 250510 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 490855 Color: 1
Size: 254919 Color: 1
Size: 254227 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 377430 Color: 1
Size: 317240 Color: 1
Size: 305331 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 428362 Color: 1
Size: 302325 Color: 1
Size: 269314 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 431481 Color: 1
Size: 305880 Color: 1
Size: 262640 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 466194 Color: 1
Size: 272930 Color: 1
Size: 260877 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 369464 Color: 1
Size: 326361 Color: 0
Size: 304176 Color: 1

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 378998 Color: 1
Size: 358767 Color: 1
Size: 262236 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 497558 Color: 1
Size: 252173 Color: 1
Size: 250270 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 414812 Color: 1
Size: 326835 Color: 1
Size: 258354 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 361220 Color: 1
Size: 319847 Color: 1
Size: 318934 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 362443 Color: 1
Size: 327658 Color: 1
Size: 309900 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 379207 Color: 1
Size: 344515 Color: 1
Size: 276279 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 383580 Color: 1
Size: 328570 Color: 1
Size: 287851 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 383899 Color: 1
Size: 326149 Color: 1
Size: 289953 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 360111 Color: 1
Size: 359148 Color: 1
Size: 280742 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 435132 Color: 1
Size: 293725 Color: 1
Size: 271144 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 411748 Color: 1
Size: 316389 Color: 1
Size: 271864 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 357182 Color: 1
Size: 332912 Color: 1
Size: 309907 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 426171 Color: 1
Size: 302206 Color: 1
Size: 271624 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 438484 Color: 1
Size: 304941 Color: 1
Size: 256576 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 386652 Color: 1
Size: 357561 Color: 1
Size: 255788 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 442291 Color: 1
Size: 295818 Color: 1
Size: 261892 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 427643 Color: 1
Size: 318962 Color: 1
Size: 253396 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 356114 Color: 1
Size: 328389 Color: 0
Size: 315498 Color: 1

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 396513 Color: 1
Size: 308380 Color: 1
Size: 295108 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 352823 Color: 1
Size: 347804 Color: 1
Size: 299374 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 378025 Color: 1
Size: 344040 Color: 1
Size: 277936 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 402192 Color: 1
Size: 299963 Color: 1
Size: 297846 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 363387 Color: 1
Size: 327857 Color: 1
Size: 308757 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 363729 Color: 1
Size: 321185 Color: 1
Size: 315087 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 354142 Color: 1
Size: 349714 Color: 1
Size: 296145 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 358986 Color: 1
Size: 350321 Color: 1
Size: 290694 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 359510 Color: 1
Size: 354665 Color: 1
Size: 285826 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 364881 Color: 1
Size: 336959 Color: 1
Size: 298161 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 380980 Color: 1
Size: 322918 Color: 1
Size: 296103 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 361831 Color: 1
Size: 361347 Color: 1
Size: 276823 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 375227 Color: 1
Size: 346610 Color: 1
Size: 278164 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 425703 Color: 1
Size: 313143 Color: 1
Size: 261155 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 368242 Color: 1
Size: 360686 Color: 1
Size: 271073 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 370793 Color: 1
Size: 364749 Color: 1
Size: 264459 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 361158 Color: 1
Size: 341586 Color: 1
Size: 297257 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 388519 Color: 1
Size: 310068 Color: 0
Size: 301414 Color: 1

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 411195 Color: 1
Size: 306262 Color: 0
Size: 282544 Color: 1

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 433677 Color: 1
Size: 292474 Color: 1
Size: 273850 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 438565 Color: 1
Size: 298735 Color: 1
Size: 262701 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 360555 Color: 1
Size: 347220 Color: 1
Size: 292226 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 362089 Color: 1
Size: 346123 Color: 1
Size: 291789 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 356945 Color: 1
Size: 343025 Color: 1
Size: 300031 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 362686 Color: 1
Size: 336121 Color: 1
Size: 301194 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 355263 Color: 1
Size: 327379 Color: 0
Size: 317359 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 360255 Color: 1
Size: 343835 Color: 1
Size: 295911 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 361681 Color: 1
Size: 323538 Color: 1
Size: 314782 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 358244 Color: 1
Size: 348857 Color: 1
Size: 292900 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 350696 Color: 1
Size: 344055 Color: 1
Size: 305250 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 361007 Color: 1
Size: 351331 Color: 1
Size: 287663 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 350686 Color: 1
Size: 344473 Color: 1
Size: 304842 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 379007 Color: 1
Size: 328231 Color: 0
Size: 292763 Color: 1

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 360971 Color: 1
Size: 322592 Color: 0
Size: 316438 Color: 1

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 383502 Color: 1
Size: 338425 Color: 1
Size: 278074 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 349673 Color: 1
Size: 345649 Color: 1
Size: 304679 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 366225 Color: 1
Size: 358892 Color: 1
Size: 274884 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 359934 Color: 1
Size: 338465 Color: 1
Size: 301602 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 360056 Color: 1
Size: 322455 Color: 1
Size: 317490 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 413270 Color: 1
Size: 315234 Color: 1
Size: 271497 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 360114 Color: 1
Size: 325302 Color: 1
Size: 314585 Color: 0

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 364152 Color: 1
Size: 358879 Color: 1
Size: 276970 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 352102 Color: 1
Size: 344452 Color: 1
Size: 303447 Color: 0

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 374386 Color: 1
Size: 367981 Color: 1
Size: 257634 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 370123 Color: 1
Size: 362443 Color: 1
Size: 267435 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 359554 Color: 1
Size: 330190 Color: 1
Size: 310257 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 360331 Color: 1
Size: 326612 Color: 1
Size: 313058 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 368697 Color: 1
Size: 356877 Color: 1
Size: 274427 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 359747 Color: 1
Size: 327854 Color: 1
Size: 312400 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 496541 Color: 1
Size: 252694 Color: 1
Size: 250766 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 390649 Color: 1
Size: 347154 Color: 1
Size: 262198 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 353697 Color: 1
Size: 352769 Color: 1
Size: 293535 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 367469 Color: 1
Size: 337767 Color: 1
Size: 294765 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 359353 Color: 1
Size: 322332 Color: 1
Size: 318316 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 385629 Color: 1
Size: 358354 Color: 1
Size: 256018 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 381592 Color: 1
Size: 327699 Color: 1
Size: 290710 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 358744 Color: 1
Size: 324803 Color: 1
Size: 316454 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 358794 Color: 1
Size: 334483 Color: 1
Size: 306724 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 359621 Color: 1
Size: 358515 Color: 1
Size: 281865 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 358152 Color: 1
Size: 333888 Color: 1
Size: 307961 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 359222 Color: 1
Size: 358696 Color: 1
Size: 282083 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 359270 Color: 1
Size: 333355 Color: 1
Size: 307376 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 362572 Color: 1
Size: 324030 Color: 1
Size: 313399 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 349230 Color: 1
Size: 345335 Color: 1
Size: 305436 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 357741 Color: 1
Size: 324331 Color: 1
Size: 317929 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 358074 Color: 1
Size: 328803 Color: 1
Size: 313124 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 356192 Color: 1
Size: 335569 Color: 1
Size: 308240 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 351929 Color: 1
Size: 332643 Color: 1
Size: 315429 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 356587 Color: 1
Size: 325584 Color: 1
Size: 317830 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 357995 Color: 1
Size: 357828 Color: 1
Size: 284178 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 356997 Color: 1
Size: 326140 Color: 0
Size: 316864 Color: 1

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 382684 Color: 1
Size: 357540 Color: 1
Size: 259777 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 355731 Color: 1
Size: 337989 Color: 1
Size: 306281 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 353528 Color: 1
Size: 348268 Color: 1
Size: 298205 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 356958 Color: 1
Size: 323056 Color: 0
Size: 319987 Color: 1

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 357162 Color: 1
Size: 337304 Color: 1
Size: 305535 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 355118 Color: 1
Size: 353060 Color: 1
Size: 291823 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 357798 Color: 1
Size: 331152 Color: 1
Size: 311051 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 369679 Color: 1
Size: 359135 Color: 1
Size: 271187 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 369272 Color: 1
Size: 356951 Color: 1
Size: 273778 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 354348 Color: 1
Size: 346313 Color: 1
Size: 299340 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 382346 Color: 1
Size: 309649 Color: 1
Size: 308006 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 440268 Color: 1
Size: 284846 Color: 0
Size: 274887 Color: 1

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 406376 Color: 1
Size: 325571 Color: 1
Size: 268054 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 380821 Color: 1
Size: 356401 Color: 1
Size: 262779 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 352089 Color: 1
Size: 338214 Color: 1
Size: 309698 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 404811 Color: 1
Size: 344792 Color: 1
Size: 250398 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 357673 Color: 1
Size: 343336 Color: 1
Size: 298992 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 351365 Color: 1
Size: 341335 Color: 1
Size: 307301 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 382759 Color: 1
Size: 343308 Color: 1
Size: 273934 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 349115 Color: 1
Size: 329150 Color: 1
Size: 321736 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 348966 Color: 1
Size: 336147 Color: 1
Size: 314888 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 352920 Color: 1
Size: 349162 Color: 1
Size: 297919 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 354007 Color: 1
Size: 347966 Color: 1
Size: 298028 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 355643 Color: 1
Size: 336605 Color: 1
Size: 307753 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 394667 Color: 1
Size: 333686 Color: 1
Size: 271648 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 356361 Color: 1
Size: 335435 Color: 1
Size: 308205 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 368177 Color: 1
Size: 347803 Color: 1
Size: 284021 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 359241 Color: 1
Size: 329868 Color: 1
Size: 310892 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 355583 Color: 1
Size: 328840 Color: 1
Size: 315578 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 355020 Color: 1
Size: 323217 Color: 0
Size: 321764 Color: 1

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 362931 Color: 1
Size: 329394 Color: 1
Size: 307676 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 388526 Color: 1
Size: 323596 Color: 0
Size: 287879 Color: 1

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 354813 Color: 1
Size: 325442 Color: 1
Size: 319746 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 354645 Color: 1
Size: 334553 Color: 1
Size: 310803 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 350591 Color: 1
Size: 350538 Color: 1
Size: 298872 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 426290 Color: 1
Size: 299524 Color: 1
Size: 274187 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 354042 Color: 1
Size: 330250 Color: 1
Size: 315709 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 353039 Color: 1
Size: 347141 Color: 1
Size: 299821 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 384370 Color: 1
Size: 350429 Color: 1
Size: 265202 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 352630 Color: 1
Size: 346695 Color: 1
Size: 300676 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 352215 Color: 1
Size: 331800 Color: 1
Size: 315986 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 369899 Color: 1
Size: 341619 Color: 1
Size: 288483 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 393359 Color: 1
Size: 355690 Color: 1
Size: 250952 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 358129 Color: 1
Size: 337902 Color: 1
Size: 303970 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 354512 Color: 1
Size: 346824 Color: 1
Size: 298665 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 405399 Color: 1
Size: 302947 Color: 0
Size: 291655 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 350880 Color: 1
Size: 348587 Color: 1
Size: 300534 Color: 0

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 347971 Color: 1
Size: 337442 Color: 1
Size: 314588 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 373657 Color: 1
Size: 319642 Color: 1
Size: 306702 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 360703 Color: 1
Size: 350943 Color: 1
Size: 288355 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 361186 Color: 1
Size: 349676 Color: 1
Size: 289139 Color: 0

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 361666 Color: 1
Size: 329419 Color: 0
Size: 308916 Color: 1

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 361768 Color: 1
Size: 331316 Color: 0
Size: 306917 Color: 1

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 361798 Color: 1
Size: 341808 Color: 1
Size: 296395 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 361846 Color: 1
Size: 350731 Color: 1
Size: 287424 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 362193 Color: 1
Size: 337146 Color: 1
Size: 300662 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 362427 Color: 1
Size: 340721 Color: 1
Size: 296853 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 362447 Color: 1
Size: 326675 Color: 0
Size: 310879 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 362483 Color: 1
Size: 346977 Color: 1
Size: 290541 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 362553 Color: 1
Size: 347643 Color: 1
Size: 289805 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 362733 Color: 1
Size: 327682 Color: 1
Size: 309586 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 362750 Color: 1
Size: 353480 Color: 1
Size: 283771 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 362812 Color: 1
Size: 347440 Color: 1
Size: 289749 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 362892 Color: 1
Size: 335667 Color: 1
Size: 301442 Color: 0

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 362915 Color: 1
Size: 321597 Color: 1
Size: 315489 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 363071 Color: 1
Size: 341050 Color: 1
Size: 295880 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 363182 Color: 1
Size: 334234 Color: 1
Size: 302585 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 363188 Color: 1
Size: 351905 Color: 1
Size: 284908 Color: 0

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 363265 Color: 1
Size: 336736 Color: 1
Size: 300000 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 363421 Color: 1
Size: 349099 Color: 1
Size: 287481 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 363451 Color: 1
Size: 344989 Color: 1
Size: 291561 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 363463 Color: 1
Size: 338557 Color: 1
Size: 297981 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 1
Size: 330273 Color: 1
Size: 306246 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 363681 Color: 1
Size: 338325 Color: 1
Size: 297995 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 363701 Color: 1
Size: 318535 Color: 1
Size: 317765 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 363731 Color: 1
Size: 333107 Color: 1
Size: 303163 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 363796 Color: 1
Size: 350069 Color: 1
Size: 286136 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 363859 Color: 1
Size: 329563 Color: 1
Size: 306579 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 363880 Color: 1
Size: 344308 Color: 1
Size: 291813 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 363931 Color: 1
Size: 342036 Color: 1
Size: 294034 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 363942 Color: 1
Size: 349299 Color: 1
Size: 286760 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 363976 Color: 1
Size: 334931 Color: 1
Size: 301094 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 363985 Color: 1
Size: 343410 Color: 1
Size: 292606 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 364000 Color: 1
Size: 346582 Color: 1
Size: 289419 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 364116 Color: 1
Size: 328264 Color: 0
Size: 307621 Color: 1

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 364312 Color: 1
Size: 338112 Color: 1
Size: 297577 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 364354 Color: 1
Size: 352369 Color: 1
Size: 283278 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 364390 Color: 1
Size: 344944 Color: 1
Size: 290667 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 364439 Color: 1
Size: 322676 Color: 1
Size: 312886 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 364484 Color: 1
Size: 326750 Color: 1
Size: 308767 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 364501 Color: 1
Size: 345265 Color: 1
Size: 290235 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 364545 Color: 1
Size: 323996 Color: 1
Size: 311460 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 364554 Color: 1
Size: 349457 Color: 1
Size: 285990 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 364681 Color: 1
Size: 341746 Color: 1
Size: 293574 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 361118 Color: 1
Size: 349397 Color: 1
Size: 289486 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 365008 Color: 1
Size: 341843 Color: 1
Size: 293150 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 365035 Color: 1
Size: 325772 Color: 1
Size: 309194 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 365043 Color: 1
Size: 344210 Color: 1
Size: 290748 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 365070 Color: 1
Size: 317912 Color: 1
Size: 317019 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 365330 Color: 1
Size: 348894 Color: 1
Size: 285777 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 365354 Color: 1
Size: 341793 Color: 1
Size: 292854 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 365439 Color: 1
Size: 339597 Color: 1
Size: 294965 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 365467 Color: 1
Size: 338119 Color: 1
Size: 296415 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 365624 Color: 1
Size: 325144 Color: 0
Size: 309233 Color: 1

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 365645 Color: 1
Size: 331825 Color: 1
Size: 302531 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 365688 Color: 1
Size: 343114 Color: 1
Size: 291199 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 365904 Color: 1
Size: 348904 Color: 1
Size: 285193 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 365969 Color: 1
Size: 351799 Color: 1
Size: 282233 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 366155 Color: 1
Size: 352546 Color: 1
Size: 281300 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 366237 Color: 1
Size: 324179 Color: 0
Size: 309585 Color: 1

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 366255 Color: 1
Size: 334865 Color: 1
Size: 298881 Color: 0

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 366265 Color: 1
Size: 325856 Color: 0
Size: 307880 Color: 1

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 366336 Color: 1
Size: 352783 Color: 1
Size: 280882 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 366434 Color: 1
Size: 330767 Color: 1
Size: 302800 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 366456 Color: 1
Size: 342364 Color: 1
Size: 291181 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 366497 Color: 1
Size: 327432 Color: 1
Size: 306072 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 366526 Color: 1
Size: 330337 Color: 1
Size: 303138 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 366733 Color: 1
Size: 353928 Color: 1
Size: 279340 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 366838 Color: 1
Size: 353256 Color: 1
Size: 279907 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 366895 Color: 1
Size: 344129 Color: 1
Size: 288977 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 366897 Color: 1
Size: 346236 Color: 1
Size: 286868 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 366897 Color: 1
Size: 322302 Color: 1
Size: 310802 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 366936 Color: 1
Size: 346075 Color: 1
Size: 286990 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 367284 Color: 1
Size: 340807 Color: 1
Size: 291910 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 367319 Color: 1
Size: 366729 Color: 1
Size: 265953 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 367341 Color: 1
Size: 337680 Color: 1
Size: 294980 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 367414 Color: 1
Size: 334004 Color: 1
Size: 298583 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 367603 Color: 1
Size: 348654 Color: 1
Size: 283744 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 367609 Color: 1
Size: 340649 Color: 1
Size: 291743 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 367622 Color: 1
Size: 341400 Color: 1
Size: 290979 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 367669 Color: 1
Size: 345199 Color: 1
Size: 287133 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 367875 Color: 1
Size: 339138 Color: 1
Size: 292988 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 368263 Color: 1
Size: 316592 Color: 1
Size: 315146 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 368439 Color: 1
Size: 367020 Color: 1
Size: 264542 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 368418 Color: 1
Size: 324912 Color: 1
Size: 306671 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 368742 Color: 1
Size: 333019 Color: 1
Size: 298240 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 368818 Color: 1
Size: 332054 Color: 1
Size: 299129 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 368844 Color: 1
Size: 340731 Color: 1
Size: 290426 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 368852 Color: 1
Size: 318139 Color: 1
Size: 313010 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 368963 Color: 1
Size: 346850 Color: 1
Size: 284188 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 369006 Color: 1
Size: 334543 Color: 1
Size: 296452 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 369058 Color: 1
Size: 351956 Color: 1
Size: 278987 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 369071 Color: 1
Size: 339702 Color: 1
Size: 291228 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 369144 Color: 1
Size: 356236 Color: 1
Size: 274621 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 369165 Color: 1
Size: 320473 Color: 1
Size: 310363 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 369201 Color: 1
Size: 360704 Color: 1
Size: 270096 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 369216 Color: 1
Size: 357445 Color: 1
Size: 273340 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 369226 Color: 1
Size: 337226 Color: 1
Size: 293549 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 369229 Color: 1
Size: 328117 Color: 1
Size: 302655 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 369240 Color: 1
Size: 349911 Color: 1
Size: 280850 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 369291 Color: 1
Size: 365429 Color: 1
Size: 265281 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 369309 Color: 1
Size: 329284 Color: 1
Size: 301408 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 369445 Color: 1
Size: 362198 Color: 1
Size: 268358 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 369459 Color: 1
Size: 316091 Color: 1
Size: 314451 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 369481 Color: 1
Size: 346308 Color: 1
Size: 284212 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 369519 Color: 1
Size: 323712 Color: 1
Size: 306770 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 369520 Color: 1
Size: 332315 Color: 1
Size: 298166 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 369678 Color: 1
Size: 342378 Color: 1
Size: 287945 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 369713 Color: 1
Size: 346630 Color: 1
Size: 283658 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 369781 Color: 1
Size: 330541 Color: 0
Size: 299679 Color: 1

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 369834 Color: 1
Size: 357205 Color: 1
Size: 272962 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 369880 Color: 1
Size: 366887 Color: 1
Size: 263234 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 369908 Color: 1
Size: 356133 Color: 1
Size: 273960 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 369910 Color: 1
Size: 329194 Color: 1
Size: 300897 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 369917 Color: 1
Size: 341337 Color: 1
Size: 288747 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 369921 Color: 1
Size: 325963 Color: 1
Size: 304117 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 369922 Color: 1
Size: 330036 Color: 1
Size: 300043 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 369941 Color: 1
Size: 354060 Color: 1
Size: 276000 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 369950 Color: 1
Size: 332317 Color: 1
Size: 297734 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 370117 Color: 1
Size: 349788 Color: 1
Size: 280096 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 370140 Color: 1
Size: 328604 Color: 1
Size: 301257 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 370140 Color: 1
Size: 354070 Color: 1
Size: 275791 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 370246 Color: 1
Size: 346837 Color: 1
Size: 282918 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 370356 Color: 1
Size: 333311 Color: 1
Size: 296334 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 370395 Color: 1
Size: 318396 Color: 1
Size: 311210 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 370412 Color: 1
Size: 361650 Color: 1
Size: 267939 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 370523 Color: 1
Size: 338617 Color: 1
Size: 290861 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 370537 Color: 1
Size: 319746 Color: 0
Size: 309718 Color: 1

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 370537 Color: 1
Size: 333933 Color: 1
Size: 295531 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 370550 Color: 1
Size: 315196 Color: 0
Size: 314255 Color: 1

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 370594 Color: 1
Size: 327023 Color: 0
Size: 302384 Color: 1

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 370638 Color: 1
Size: 333646 Color: 1
Size: 295717 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 370674 Color: 1
Size: 336294 Color: 1
Size: 293033 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 370688 Color: 1
Size: 339517 Color: 1
Size: 289796 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 370882 Color: 1
Size: 346471 Color: 1
Size: 282648 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 370893 Color: 1
Size: 335921 Color: 1
Size: 293187 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 371030 Color: 1
Size: 314916 Color: 0
Size: 314055 Color: 1

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 371040 Color: 1
Size: 318492 Color: 1
Size: 310469 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 371057 Color: 1
Size: 346689 Color: 1
Size: 282255 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 371064 Color: 1
Size: 348528 Color: 1
Size: 280409 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 371096 Color: 1
Size: 326630 Color: 1
Size: 302275 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 371099 Color: 1
Size: 329243 Color: 0
Size: 299659 Color: 1

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 371107 Color: 1
Size: 368278 Color: 1
Size: 260616 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 371178 Color: 1
Size: 340106 Color: 1
Size: 288717 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 371279 Color: 1
Size: 352499 Color: 1
Size: 276223 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 371311 Color: 1
Size: 329483 Color: 1
Size: 299207 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 371474 Color: 1
Size: 329901 Color: 1
Size: 298626 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 371493 Color: 1
Size: 344693 Color: 1
Size: 283815 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 371592 Color: 1
Size: 337730 Color: 1
Size: 290679 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 371631 Color: 1
Size: 336222 Color: 1
Size: 292148 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 371659 Color: 1
Size: 331539 Color: 1
Size: 296803 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 371688 Color: 1
Size: 344499 Color: 1
Size: 283814 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 371692 Color: 1
Size: 356068 Color: 1
Size: 272241 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 371786 Color: 1
Size: 347823 Color: 1
Size: 280392 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 371875 Color: 1
Size: 353382 Color: 1
Size: 274744 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 371911 Color: 1
Size: 342973 Color: 1
Size: 285117 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 371924 Color: 1
Size: 359679 Color: 1
Size: 268398 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 371940 Color: 1
Size: 331348 Color: 1
Size: 296713 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 372032 Color: 1
Size: 325853 Color: 1
Size: 302116 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 372044 Color: 1
Size: 327895 Color: 1
Size: 300062 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 372067 Color: 1
Size: 327042 Color: 1
Size: 300892 Color: 0

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 372096 Color: 1
Size: 345130 Color: 1
Size: 282775 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 372127 Color: 1
Size: 319765 Color: 0
Size: 308109 Color: 1

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 372141 Color: 1
Size: 327223 Color: 1
Size: 300637 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 372194 Color: 1
Size: 335169 Color: 1
Size: 292638 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 372266 Color: 1
Size: 357664 Color: 1
Size: 270071 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 372339 Color: 1
Size: 341082 Color: 1
Size: 286580 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 372361 Color: 1
Size: 346958 Color: 1
Size: 280682 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 372396 Color: 1
Size: 346196 Color: 1
Size: 281409 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 372453 Color: 1
Size: 325979 Color: 0
Size: 301569 Color: 1

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 372478 Color: 1
Size: 358899 Color: 1
Size: 268624 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 372514 Color: 1
Size: 325192 Color: 1
Size: 302295 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 372543 Color: 1
Size: 331713 Color: 1
Size: 295745 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 372601 Color: 1
Size: 344116 Color: 1
Size: 283284 Color: 0

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 372691 Color: 1
Size: 337435 Color: 1
Size: 289875 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 372705 Color: 1
Size: 340624 Color: 1
Size: 286672 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 372736 Color: 1
Size: 340012 Color: 1
Size: 287253 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 372784 Color: 1
Size: 363632 Color: 1
Size: 263585 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 372905 Color: 1
Size: 322585 Color: 1
Size: 304511 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 372972 Color: 1
Size: 353666 Color: 1
Size: 273363 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 372988 Color: 1
Size: 333957 Color: 1
Size: 293056 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 373010 Color: 1
Size: 327655 Color: 1
Size: 299336 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 373021 Color: 1
Size: 325800 Color: 1
Size: 301180 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 373050 Color: 1
Size: 329330 Color: 1
Size: 297621 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 373087 Color: 1
Size: 353291 Color: 1
Size: 273623 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 373278 Color: 1
Size: 329824 Color: 1
Size: 296899 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 373287 Color: 1
Size: 326945 Color: 1
Size: 299769 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 373317 Color: 1
Size: 358653 Color: 1
Size: 268031 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 373337 Color: 1
Size: 337793 Color: 1
Size: 288871 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 373549 Color: 1
Size: 337468 Color: 1
Size: 288984 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 373638 Color: 1
Size: 338359 Color: 1
Size: 288004 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 373674 Color: 1
Size: 343806 Color: 1
Size: 282521 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 373680 Color: 1
Size: 350883 Color: 1
Size: 275438 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 373681 Color: 1
Size: 353940 Color: 1
Size: 272380 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 373722 Color: 1
Size: 366271 Color: 1
Size: 260008 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 373759 Color: 1
Size: 315459 Color: 1
Size: 310783 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 373759 Color: 1
Size: 340247 Color: 1
Size: 285995 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 373764 Color: 1
Size: 327627 Color: 1
Size: 298610 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 373803 Color: 1
Size: 367498 Color: 1
Size: 258700 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 373811 Color: 1
Size: 332944 Color: 1
Size: 293246 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 373813 Color: 1
Size: 340636 Color: 1
Size: 285552 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 373831 Color: 1
Size: 349583 Color: 1
Size: 276587 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 373881 Color: 1
Size: 362993 Color: 1
Size: 263127 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 373950 Color: 1
Size: 339572 Color: 1
Size: 286479 Color: 0

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 373992 Color: 1
Size: 329678 Color: 1
Size: 296331 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 350221 Color: 1
Size: 349684 Color: 1
Size: 300096 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 374065 Color: 1
Size: 338453 Color: 1
Size: 287483 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 374089 Color: 1
Size: 331907 Color: 1
Size: 294005 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 374101 Color: 1
Size: 340354 Color: 1
Size: 285546 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 374148 Color: 1
Size: 328289 Color: 1
Size: 297564 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 374155 Color: 1
Size: 325294 Color: 1
Size: 300552 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 374219 Color: 1
Size: 333912 Color: 1
Size: 291870 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 374284 Color: 1
Size: 325063 Color: 1
Size: 300654 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 374359 Color: 1
Size: 351016 Color: 1
Size: 274626 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 374374 Color: 1
Size: 329579 Color: 1
Size: 296048 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 374381 Color: 1
Size: 345539 Color: 1
Size: 280081 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 374505 Color: 1
Size: 317877 Color: 1
Size: 307619 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 374538 Color: 1
Size: 338185 Color: 1
Size: 287278 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 374776 Color: 1
Size: 355937 Color: 1
Size: 269288 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 374812 Color: 1
Size: 336511 Color: 1
Size: 288678 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 374832 Color: 1
Size: 343095 Color: 1
Size: 282074 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 374899 Color: 1
Size: 326801 Color: 1
Size: 298301 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 374937 Color: 1
Size: 341881 Color: 1
Size: 283183 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 374944 Color: 1
Size: 337582 Color: 1
Size: 287475 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 374963 Color: 1
Size: 314960 Color: 1
Size: 310078 Color: 0

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 374970 Color: 1
Size: 336146 Color: 1
Size: 288885 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 374976 Color: 1
Size: 327826 Color: 1
Size: 297199 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 375057 Color: 1
Size: 336713 Color: 1
Size: 288231 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 375076 Color: 1
Size: 339679 Color: 1
Size: 285246 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 375094 Color: 1
Size: 341982 Color: 1
Size: 282925 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 375139 Color: 1
Size: 371601 Color: 1
Size: 253261 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 375225 Color: 1
Size: 314587 Color: 0
Size: 310189 Color: 1

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 375234 Color: 1
Size: 353096 Color: 1
Size: 271671 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 375249 Color: 1
Size: 313630 Color: 0
Size: 311122 Color: 1

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 375258 Color: 1
Size: 337274 Color: 1
Size: 287469 Color: 0

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 375287 Color: 1
Size: 359310 Color: 1
Size: 265404 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 375389 Color: 1
Size: 362987 Color: 1
Size: 261625 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 375436 Color: 1
Size: 330815 Color: 1
Size: 293750 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 375510 Color: 1
Size: 342293 Color: 1
Size: 282198 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 375551 Color: 1
Size: 324517 Color: 1
Size: 299933 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 375634 Color: 1
Size: 315634 Color: 0
Size: 308733 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 375699 Color: 1
Size: 321443 Color: 1
Size: 302859 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 375705 Color: 1
Size: 318955 Color: 0
Size: 305341 Color: 1

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 375748 Color: 1
Size: 332984 Color: 1
Size: 291269 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 375778 Color: 1
Size: 358576 Color: 1
Size: 265647 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 375845 Color: 1
Size: 346448 Color: 1
Size: 277708 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 375864 Color: 1
Size: 332909 Color: 1
Size: 291228 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 375918 Color: 1
Size: 367821 Color: 1
Size: 256262 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 375931 Color: 1
Size: 337082 Color: 1
Size: 286988 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 375991 Color: 1
Size: 340690 Color: 1
Size: 283320 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 376069 Color: 1
Size: 314220 Color: 0
Size: 309712 Color: 1

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 376121 Color: 1
Size: 358969 Color: 1
Size: 264911 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 376149 Color: 1
Size: 339934 Color: 1
Size: 283918 Color: 0

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 376155 Color: 1
Size: 347739 Color: 1
Size: 276107 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 376250 Color: 1
Size: 318944 Color: 1
Size: 304807 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 376383 Color: 1
Size: 354017 Color: 1
Size: 269601 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 376547 Color: 1
Size: 331424 Color: 1
Size: 292030 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 376548 Color: 1
Size: 343184 Color: 1
Size: 280269 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 376644 Color: 1
Size: 314913 Color: 1
Size: 308444 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 376718 Color: 1
Size: 364223 Color: 1
Size: 259060 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 376826 Color: 1
Size: 341944 Color: 1
Size: 281231 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 376861 Color: 1
Size: 343699 Color: 1
Size: 279441 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 376864 Color: 1
Size: 319153 Color: 1
Size: 303984 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 376893 Color: 1
Size: 347670 Color: 1
Size: 275438 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 376914 Color: 1
Size: 339195 Color: 1
Size: 283892 Color: 0

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 376950 Color: 1
Size: 341999 Color: 1
Size: 281052 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 377021 Color: 1
Size: 361042 Color: 1
Size: 261938 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 346296 Color: 1
Size: 345468 Color: 1
Size: 308237 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 377060 Color: 1
Size: 337313 Color: 1
Size: 285628 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 377063 Color: 1
Size: 330894 Color: 1
Size: 292044 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 377412 Color: 1
Size: 335207 Color: 1
Size: 287382 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 377439 Color: 1
Size: 340417 Color: 1
Size: 282145 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 377473 Color: 1
Size: 334730 Color: 1
Size: 287798 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 377516 Color: 1
Size: 312518 Color: 1
Size: 309967 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 377547 Color: 1
Size: 332038 Color: 1
Size: 290416 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 377621 Color: 1
Size: 313697 Color: 1
Size: 308683 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 377792 Color: 1
Size: 319382 Color: 1
Size: 302827 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 377807 Color: 1
Size: 320957 Color: 1
Size: 301237 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 377881 Color: 1
Size: 343585 Color: 1
Size: 278535 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 377942 Color: 1
Size: 313375 Color: 1
Size: 308684 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 377961 Color: 1
Size: 338468 Color: 1
Size: 283572 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 377988 Color: 1
Size: 341706 Color: 1
Size: 280307 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 378010 Color: 1
Size: 349759 Color: 1
Size: 272232 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 378095 Color: 1
Size: 312118 Color: 0
Size: 309788 Color: 1

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 378097 Color: 1
Size: 312001 Color: 0
Size: 309903 Color: 1

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 378309 Color: 1
Size: 320248 Color: 1
Size: 301444 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 378350 Color: 1
Size: 343163 Color: 1
Size: 278488 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 378369 Color: 1
Size: 337105 Color: 1
Size: 284527 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 378427 Color: 1
Size: 349047 Color: 1
Size: 272527 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 378519 Color: 1
Size: 320243 Color: 0
Size: 301239 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 378565 Color: 1
Size: 324358 Color: 1
Size: 297078 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 378584 Color: 1
Size: 347146 Color: 1
Size: 274271 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 378602 Color: 1
Size: 333124 Color: 1
Size: 288275 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 378648 Color: 1
Size: 344179 Color: 1
Size: 277174 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 378652 Color: 1
Size: 315346 Color: 1
Size: 306003 Color: 0

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 357495 Color: 1
Size: 339080 Color: 1
Size: 303426 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 378769 Color: 1
Size: 358921 Color: 1
Size: 262311 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 378969 Color: 1
Size: 346100 Color: 1
Size: 274932 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 378972 Color: 1
Size: 341239 Color: 1
Size: 279790 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 379038 Color: 1
Size: 359727 Color: 1
Size: 261236 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 379051 Color: 1
Size: 350433 Color: 1
Size: 270517 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 379115 Color: 1
Size: 344115 Color: 1
Size: 276771 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 379128 Color: 1
Size: 337340 Color: 1
Size: 283533 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 379166 Color: 1
Size: 337618 Color: 1
Size: 283217 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 379172 Color: 1
Size: 320141 Color: 1
Size: 300688 Color: 0

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 379226 Color: 1
Size: 314565 Color: 0
Size: 306210 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 379254 Color: 1
Size: 335647 Color: 1
Size: 285100 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 379412 Color: 1
Size: 343129 Color: 1
Size: 277460 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 379427 Color: 1
Size: 330715 Color: 1
Size: 289859 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 379504 Color: 1
Size: 332999 Color: 1
Size: 287498 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 379558 Color: 1
Size: 333902 Color: 1
Size: 286541 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 379621 Color: 1
Size: 361533 Color: 1
Size: 258847 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 379682 Color: 1
Size: 341791 Color: 1
Size: 278528 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 379705 Color: 1
Size: 332847 Color: 1
Size: 287449 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 379718 Color: 1
Size: 345725 Color: 1
Size: 274558 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 379748 Color: 1
Size: 328548 Color: 1
Size: 291705 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 379803 Color: 1
Size: 318210 Color: 1
Size: 301988 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 379819 Color: 1
Size: 337856 Color: 1
Size: 282326 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 379837 Color: 1
Size: 341079 Color: 1
Size: 279085 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 379845 Color: 1
Size: 316120 Color: 1
Size: 304036 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 379889 Color: 1
Size: 344045 Color: 1
Size: 276067 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 380152 Color: 1
Size: 337900 Color: 1
Size: 281949 Color: 0

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 380178 Color: 1
Size: 322668 Color: 1
Size: 297155 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 380253 Color: 1
Size: 314257 Color: 1
Size: 305491 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 380266 Color: 1
Size: 354715 Color: 1
Size: 265020 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 380271 Color: 1
Size: 322844 Color: 1
Size: 296886 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 380274 Color: 1
Size: 313119 Color: 0
Size: 306608 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 380358 Color: 1
Size: 323960 Color: 1
Size: 295683 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 380359 Color: 1
Size: 335137 Color: 1
Size: 284505 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 380362 Color: 1
Size: 341300 Color: 1
Size: 278339 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 380362 Color: 1
Size: 315622 Color: 0
Size: 304017 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 380364 Color: 1
Size: 334839 Color: 1
Size: 284798 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 380364 Color: 1
Size: 354915 Color: 1
Size: 264722 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 380370 Color: 1
Size: 314257 Color: 1
Size: 305374 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 380373 Color: 1
Size: 322823 Color: 0
Size: 296805 Color: 1

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 380389 Color: 1
Size: 361031 Color: 1
Size: 258581 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 380474 Color: 1
Size: 339388 Color: 1
Size: 280139 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 380640 Color: 1
Size: 323349 Color: 1
Size: 296012 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 380643 Color: 1
Size: 340853 Color: 1
Size: 278505 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 380665 Color: 1
Size: 363200 Color: 1
Size: 256136 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 380669 Color: 1
Size: 326876 Color: 1
Size: 292456 Color: 0

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 380711 Color: 1
Size: 358143 Color: 1
Size: 261147 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 380727 Color: 1
Size: 333174 Color: 1
Size: 286100 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 380764 Color: 1
Size: 353939 Color: 1
Size: 265298 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 380778 Color: 1
Size: 348138 Color: 1
Size: 271085 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 380837 Color: 1
Size: 338314 Color: 1
Size: 280850 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 380842 Color: 1
Size: 337150 Color: 1
Size: 282009 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 380883 Color: 1
Size: 333798 Color: 1
Size: 285320 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 380999 Color: 1
Size: 347500 Color: 1
Size: 271502 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 381056 Color: 1
Size: 330093 Color: 1
Size: 288852 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 381085 Color: 1
Size: 352971 Color: 1
Size: 265945 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 381209 Color: 1
Size: 315760 Color: 1
Size: 303032 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 381253 Color: 1
Size: 333281 Color: 1
Size: 285467 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 381439 Color: 1
Size: 336367 Color: 1
Size: 282195 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 381454 Color: 1
Size: 339490 Color: 1
Size: 279057 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 381463 Color: 1
Size: 349297 Color: 1
Size: 269241 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 381479 Color: 1
Size: 316591 Color: 1
Size: 301931 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 381479 Color: 1
Size: 317399 Color: 1
Size: 301123 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 381618 Color: 1
Size: 320778 Color: 1
Size: 297605 Color: 0

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 381678 Color: 1
Size: 334755 Color: 1
Size: 283568 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 381718 Color: 1
Size: 335920 Color: 1
Size: 282363 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 381728 Color: 1
Size: 345980 Color: 1
Size: 272293 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 381839 Color: 1
Size: 355035 Color: 1
Size: 263127 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 381844 Color: 1
Size: 325687 Color: 1
Size: 292470 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 381851 Color: 1
Size: 346550 Color: 1
Size: 271600 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 382003 Color: 1
Size: 331030 Color: 1
Size: 286968 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 382017 Color: 1
Size: 334608 Color: 1
Size: 283376 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 382053 Color: 1
Size: 324897 Color: 1
Size: 293051 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 382209 Color: 1
Size: 312288 Color: 1
Size: 305504 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 382261 Color: 1
Size: 312258 Color: 0
Size: 305482 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 382304 Color: 1
Size: 330070 Color: 1
Size: 287627 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 382322 Color: 1
Size: 356532 Color: 1
Size: 261147 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 382384 Color: 1
Size: 337290 Color: 1
Size: 280327 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 382396 Color: 1
Size: 362614 Color: 1
Size: 254991 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 382485 Color: 1
Size: 345370 Color: 1
Size: 272146 Color: 0

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 382539 Color: 1
Size: 331023 Color: 1
Size: 286439 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 382592 Color: 1
Size: 333060 Color: 1
Size: 284349 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 382617 Color: 1
Size: 327249 Color: 0
Size: 290135 Color: 1

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 382638 Color: 1
Size: 335981 Color: 1
Size: 281382 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 382690 Color: 1
Size: 317639 Color: 1
Size: 299672 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 382702 Color: 1
Size: 314797 Color: 1
Size: 302502 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 382800 Color: 1
Size: 331803 Color: 1
Size: 285398 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 382851 Color: 1
Size: 330088 Color: 1
Size: 287062 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 382862 Color: 1
Size: 327771 Color: 1
Size: 289368 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 382926 Color: 1
Size: 330738 Color: 1
Size: 286337 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 383007 Color: 1
Size: 332492 Color: 1
Size: 284502 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 383013 Color: 1
Size: 341996 Color: 1
Size: 274992 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 383102 Color: 1
Size: 325459 Color: 1
Size: 291440 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 376216 Color: 1
Size: 357252 Color: 1
Size: 266533 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 383223 Color: 1
Size: 341510 Color: 1
Size: 275268 Color: 0

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 383224 Color: 1
Size: 353395 Color: 1
Size: 263382 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 383340 Color: 1
Size: 308501 Color: 0
Size: 308160 Color: 1

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 383345 Color: 1
Size: 327278 Color: 1
Size: 289378 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 383473 Color: 1
Size: 330275 Color: 1
Size: 286253 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 383566 Color: 1
Size: 354293 Color: 1
Size: 262142 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 383592 Color: 1
Size: 356052 Color: 1
Size: 260357 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 383605 Color: 1
Size: 330078 Color: 1
Size: 286318 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 383666 Color: 1
Size: 360586 Color: 1
Size: 255749 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 383776 Color: 1
Size: 350070 Color: 1
Size: 266155 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 383863 Color: 1
Size: 327752 Color: 1
Size: 288386 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 383903 Color: 1
Size: 340323 Color: 1
Size: 275775 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 383914 Color: 1
Size: 317337 Color: 1
Size: 298750 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 383946 Color: 1
Size: 339697 Color: 1
Size: 276358 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 384031 Color: 1
Size: 320559 Color: 1
Size: 295411 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 384094 Color: 1
Size: 315941 Color: 1
Size: 299966 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 384154 Color: 1
Size: 340072 Color: 1
Size: 275775 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 384203 Color: 1
Size: 316542 Color: 1
Size: 299256 Color: 0

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 384235 Color: 1
Size: 332391 Color: 1
Size: 283375 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 384261 Color: 1
Size: 318172 Color: 1
Size: 297568 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 384283 Color: 1
Size: 319926 Color: 1
Size: 295792 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 384307 Color: 1
Size: 345354 Color: 1
Size: 270340 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 384319 Color: 1
Size: 348440 Color: 1
Size: 267242 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 384347 Color: 1
Size: 335877 Color: 1
Size: 279777 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 384387 Color: 1
Size: 347109 Color: 1
Size: 268505 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 384645 Color: 1
Size: 342733 Color: 1
Size: 272623 Color: 0

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 384744 Color: 1
Size: 342411 Color: 1
Size: 272846 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 384794 Color: 1
Size: 346582 Color: 1
Size: 268625 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 384923 Color: 1
Size: 322459 Color: 1
Size: 292619 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 385025 Color: 1
Size: 320420 Color: 1
Size: 294556 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 385067 Color: 1
Size: 337049 Color: 1
Size: 277885 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 385083 Color: 1
Size: 312544 Color: 1
Size: 302374 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 385170 Color: 1
Size: 334893 Color: 1
Size: 279938 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 385193 Color: 1
Size: 321990 Color: 1
Size: 292818 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 385206 Color: 1
Size: 321461 Color: 1
Size: 293334 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 385268 Color: 1
Size: 311050 Color: 1
Size: 303683 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 385430 Color: 1
Size: 357721 Color: 1
Size: 256850 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 385455 Color: 1
Size: 317142 Color: 1
Size: 297404 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 385461 Color: 1
Size: 308037 Color: 1
Size: 306503 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 385494 Color: 1
Size: 330746 Color: 1
Size: 283761 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 385670 Color: 1
Size: 333260 Color: 1
Size: 281071 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 385712 Color: 1
Size: 337283 Color: 1
Size: 277006 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 385734 Color: 1
Size: 346181 Color: 1
Size: 268086 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 385739 Color: 1
Size: 314251 Color: 0
Size: 300011 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 385753 Color: 1
Size: 350588 Color: 1
Size: 263660 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 385822 Color: 1
Size: 347185 Color: 1
Size: 266994 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 385824 Color: 1
Size: 312816 Color: 1
Size: 301361 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 386000 Color: 1
Size: 344955 Color: 1
Size: 269046 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 366453 Color: 1
Size: 331075 Color: 1
Size: 302473 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 386074 Color: 1
Size: 337808 Color: 1
Size: 276119 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 386155 Color: 1
Size: 319884 Color: 1
Size: 293962 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 386156 Color: 1
Size: 325003 Color: 1
Size: 288842 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 386189 Color: 1
Size: 331465 Color: 1
Size: 282347 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 386289 Color: 1
Size: 347369 Color: 1
Size: 266343 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 386591 Color: 1
Size: 337179 Color: 1
Size: 276231 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 386623 Color: 1
Size: 337806 Color: 1
Size: 275572 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 386642 Color: 1
Size: 327056 Color: 1
Size: 286303 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 386647 Color: 1
Size: 320077 Color: 1
Size: 293277 Color: 0

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 386669 Color: 1
Size: 324276 Color: 1
Size: 289056 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 386752 Color: 1
Size: 321680 Color: 1
Size: 291569 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 386755 Color: 1
Size: 336101 Color: 1
Size: 277145 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 386829 Color: 1
Size: 314267 Color: 1
Size: 298905 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 386846 Color: 1
Size: 327907 Color: 1
Size: 285248 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 386855 Color: 1
Size: 348900 Color: 1
Size: 264246 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 386869 Color: 1
Size: 334530 Color: 1
Size: 278602 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 386877 Color: 1
Size: 334030 Color: 1
Size: 279094 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 386918 Color: 1
Size: 317137 Color: 1
Size: 295946 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 386974 Color: 1
Size: 334635 Color: 1
Size: 278392 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 387004 Color: 1
Size: 334004 Color: 1
Size: 278993 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 387053 Color: 1
Size: 330750 Color: 1
Size: 282198 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 387347 Color: 1
Size: 354775 Color: 1
Size: 257879 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 387413 Color: 1
Size: 313079 Color: 1
Size: 299509 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 387542 Color: 1
Size: 334933 Color: 1
Size: 277526 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 387597 Color: 1
Size: 330322 Color: 1
Size: 282082 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 387644 Color: 1
Size: 350352 Color: 1
Size: 262005 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 387652 Color: 1
Size: 306499 Color: 1
Size: 305850 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 387661 Color: 1
Size: 330678 Color: 1
Size: 281662 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 387686 Color: 1
Size: 308312 Color: 0
Size: 304003 Color: 1

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 387776 Color: 1
Size: 308349 Color: 0
Size: 303876 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 387836 Color: 1
Size: 334416 Color: 1
Size: 277749 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 387867 Color: 1
Size: 310010 Color: 1
Size: 302124 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 387888 Color: 1
Size: 354929 Color: 1
Size: 257184 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 387902 Color: 1
Size: 326008 Color: 1
Size: 286091 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 388043 Color: 1
Size: 356976 Color: 1
Size: 254982 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 388262 Color: 1
Size: 331744 Color: 1
Size: 279995 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 388381 Color: 1
Size: 312205 Color: 1
Size: 299415 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 388412 Color: 1
Size: 318883 Color: 1
Size: 292706 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 388416 Color: 1
Size: 342555 Color: 1
Size: 269030 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 388443 Color: 1
Size: 314241 Color: 1
Size: 297317 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 388549 Color: 1
Size: 336086 Color: 1
Size: 275366 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 388572 Color: 1
Size: 341692 Color: 1
Size: 269737 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 388577 Color: 1
Size: 332638 Color: 1
Size: 278786 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 388627 Color: 1
Size: 307499 Color: 0
Size: 303875 Color: 1

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 388641 Color: 1
Size: 330382 Color: 1
Size: 280978 Color: 0

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 388740 Color: 1
Size: 340667 Color: 1
Size: 270594 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 388781 Color: 1
Size: 331778 Color: 1
Size: 279442 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 388814 Color: 1
Size: 308670 Color: 1
Size: 302517 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 388845 Color: 1
Size: 335316 Color: 1
Size: 275840 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 388848 Color: 1
Size: 307297 Color: 0
Size: 303856 Color: 1

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 388957 Color: 1
Size: 325931 Color: 1
Size: 285113 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 388965 Color: 1
Size: 329193 Color: 1
Size: 281843 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 389017 Color: 1
Size: 348554 Color: 1
Size: 262430 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 389029 Color: 1
Size: 334339 Color: 1
Size: 276633 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 389052 Color: 1
Size: 347155 Color: 1
Size: 263794 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 389078 Color: 1
Size: 329341 Color: 1
Size: 281582 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 389078 Color: 1
Size: 347205 Color: 1
Size: 263718 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 389180 Color: 1
Size: 317217 Color: 0
Size: 293604 Color: 1

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 389236 Color: 1
Size: 350435 Color: 1
Size: 260330 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 389319 Color: 1
Size: 325086 Color: 1
Size: 285596 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 389354 Color: 1
Size: 342850 Color: 1
Size: 267797 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 389388 Color: 1
Size: 340851 Color: 1
Size: 269762 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 389409 Color: 1
Size: 332025 Color: 1
Size: 278567 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 389425 Color: 1
Size: 355835 Color: 1
Size: 254741 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 389507 Color: 1
Size: 344847 Color: 1
Size: 265647 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 389532 Color: 1
Size: 324564 Color: 1
Size: 285905 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 389548 Color: 1
Size: 320692 Color: 1
Size: 289761 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 389627 Color: 1
Size: 345534 Color: 1
Size: 264840 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 389677 Color: 1
Size: 326570 Color: 1
Size: 283754 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 389768 Color: 1
Size: 323861 Color: 1
Size: 286372 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 389857 Color: 1
Size: 330683 Color: 1
Size: 279461 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 389956 Color: 1
Size: 308598 Color: 0
Size: 301447 Color: 1

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 390056 Color: 1
Size: 337561 Color: 1
Size: 272384 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 390080 Color: 1
Size: 337716 Color: 1
Size: 272205 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 390152 Color: 1
Size: 356077 Color: 1
Size: 253772 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 390410 Color: 1
Size: 339167 Color: 1
Size: 270424 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 390522 Color: 1
Size: 313987 Color: 1
Size: 295492 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 390655 Color: 1
Size: 344380 Color: 1
Size: 264966 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 390696 Color: 1
Size: 309555 Color: 1
Size: 299750 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 390701 Color: 1
Size: 342035 Color: 1
Size: 267265 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 390702 Color: 1
Size: 341594 Color: 1
Size: 267705 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 390711 Color: 1
Size: 335228 Color: 1
Size: 274062 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 390761 Color: 1
Size: 346418 Color: 1
Size: 262822 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 390797 Color: 1
Size: 335494 Color: 1
Size: 273710 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 390900 Color: 1
Size: 327942 Color: 0
Size: 281159 Color: 1

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 390931 Color: 1
Size: 331234 Color: 1
Size: 277836 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 391092 Color: 1
Size: 310335 Color: 0
Size: 298574 Color: 1

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 391094 Color: 1
Size: 318466 Color: 1
Size: 290441 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 391143 Color: 1
Size: 318035 Color: 1
Size: 290823 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 391184 Color: 1
Size: 349109 Color: 1
Size: 259708 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 391211 Color: 1
Size: 318497 Color: 1
Size: 290293 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 391262 Color: 1
Size: 328999 Color: 1
Size: 279740 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 391368 Color: 1
Size: 335494 Color: 1
Size: 273139 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 391420 Color: 1
Size: 352245 Color: 1
Size: 256336 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 391482 Color: 1
Size: 310553 Color: 1
Size: 297966 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 391507 Color: 1
Size: 321338 Color: 1
Size: 287156 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 391533 Color: 1
Size: 306770 Color: 1
Size: 301698 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 391606 Color: 1
Size: 346915 Color: 1
Size: 261480 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 391679 Color: 1
Size: 341846 Color: 1
Size: 266476 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 391724 Color: 1
Size: 310100 Color: 1
Size: 298177 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 391753 Color: 1
Size: 326797 Color: 1
Size: 281451 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 391759 Color: 1
Size: 314922 Color: 0
Size: 293320 Color: 1

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 391797 Color: 1
Size: 335744 Color: 1
Size: 272460 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 391833 Color: 1
Size: 328741 Color: 1
Size: 279427 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 391929 Color: 1
Size: 336650 Color: 1
Size: 271422 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 392097 Color: 1
Size: 312621 Color: 0
Size: 295283 Color: 1

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 392140 Color: 1
Size: 334692 Color: 1
Size: 273169 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 1
Size: 332288 Color: 1
Size: 275559 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 357550 Color: 1
Size: 332892 Color: 1
Size: 309559 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 392407 Color: 1
Size: 339513 Color: 1
Size: 268081 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 392418 Color: 1
Size: 312208 Color: 1
Size: 295375 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 392513 Color: 1
Size: 333857 Color: 1
Size: 273631 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 392524 Color: 1
Size: 314704 Color: 1
Size: 292773 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 392547 Color: 1
Size: 336405 Color: 1
Size: 271049 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 392815 Color: 1
Size: 338972 Color: 1
Size: 268214 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 392867 Color: 1
Size: 344787 Color: 1
Size: 262347 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 392936 Color: 1
Size: 332336 Color: 1
Size: 274729 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 392951 Color: 1
Size: 305613 Color: 1
Size: 301437 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 393006 Color: 1
Size: 329103 Color: 1
Size: 277892 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 393034 Color: 1
Size: 344234 Color: 1
Size: 262733 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 393042 Color: 1
Size: 338204 Color: 1
Size: 268755 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 393197 Color: 1
Size: 322116 Color: 1
Size: 284688 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 393251 Color: 1
Size: 332412 Color: 1
Size: 274338 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 393279 Color: 1
Size: 324292 Color: 1
Size: 282430 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 393410 Color: 1
Size: 349513 Color: 1
Size: 257078 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 393504 Color: 1
Size: 313338 Color: 1
Size: 293159 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 393509 Color: 1
Size: 332184 Color: 1
Size: 274308 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 393637 Color: 1
Size: 327536 Color: 1
Size: 278828 Color: 0

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 393660 Color: 1
Size: 341360 Color: 1
Size: 264981 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 393685 Color: 1
Size: 333338 Color: 1
Size: 272978 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 393771 Color: 1
Size: 320104 Color: 1
Size: 286126 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 393838 Color: 1
Size: 322789 Color: 1
Size: 283374 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 393903 Color: 1
Size: 329886 Color: 1
Size: 276212 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 393947 Color: 1
Size: 329171 Color: 1
Size: 276883 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 393952 Color: 1
Size: 351404 Color: 1
Size: 254645 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 393971 Color: 1
Size: 350484 Color: 1
Size: 255546 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 394004 Color: 1
Size: 327946 Color: 1
Size: 278051 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 394036 Color: 1
Size: 338447 Color: 1
Size: 267518 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 394139 Color: 1
Size: 334903 Color: 1
Size: 270959 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 394165 Color: 1
Size: 332990 Color: 1
Size: 272846 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 394183 Color: 1
Size: 312616 Color: 1
Size: 293202 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 394235 Color: 1
Size: 317219 Color: 1
Size: 288547 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 394241 Color: 1
Size: 321314 Color: 0
Size: 284446 Color: 1

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 394284 Color: 1
Size: 330365 Color: 1
Size: 275352 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 394346 Color: 1
Size: 331780 Color: 1
Size: 273875 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 394394 Color: 1
Size: 345304 Color: 1
Size: 260303 Color: 0

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 394553 Color: 1
Size: 331278 Color: 1
Size: 274170 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 394585 Color: 1
Size: 327827 Color: 1
Size: 277589 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 394608 Color: 1
Size: 326598 Color: 1
Size: 278795 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 394740 Color: 1
Size: 327213 Color: 1
Size: 278048 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 394782 Color: 1
Size: 354346 Color: 1
Size: 250873 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 394952 Color: 1
Size: 345486 Color: 1
Size: 259563 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 395015 Color: 1
Size: 320401 Color: 0
Size: 284585 Color: 1

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 395200 Color: 1
Size: 337089 Color: 1
Size: 267712 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 395223 Color: 1
Size: 314351 Color: 1
Size: 290427 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 395560 Color: 1
Size: 333421 Color: 1
Size: 271020 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 395568 Color: 1
Size: 324522 Color: 1
Size: 279911 Color: 0

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 395576 Color: 1
Size: 344259 Color: 1
Size: 260166 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 395598 Color: 1
Size: 337536 Color: 1
Size: 266867 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 395613 Color: 1
Size: 331576 Color: 1
Size: 272812 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 395634 Color: 1
Size: 303463 Color: 1
Size: 300904 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 395720 Color: 1
Size: 329901 Color: 1
Size: 274380 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 395758 Color: 1
Size: 309482 Color: 1
Size: 294761 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 395854 Color: 1
Size: 347709 Color: 1
Size: 256438 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 395858 Color: 1
Size: 316415 Color: 1
Size: 287728 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 396006 Color: 1
Size: 315188 Color: 1
Size: 288807 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 396026 Color: 1
Size: 338357 Color: 1
Size: 265618 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 396143 Color: 1
Size: 339659 Color: 1
Size: 264199 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 396214 Color: 1
Size: 316208 Color: 1
Size: 287579 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 396392 Color: 1
Size: 322693 Color: 1
Size: 280916 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 396410 Color: 1
Size: 304573 Color: 0
Size: 299018 Color: 1

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 396669 Color: 1
Size: 339698 Color: 1
Size: 263634 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 396694 Color: 1
Size: 337015 Color: 1
Size: 266292 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 396732 Color: 1
Size: 346466 Color: 1
Size: 256803 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 396740 Color: 1
Size: 340823 Color: 1
Size: 262438 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 396794 Color: 1
Size: 337708 Color: 1
Size: 265499 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 396891 Color: 1
Size: 306237 Color: 1
Size: 296873 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 396927 Color: 1
Size: 346812 Color: 1
Size: 256262 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 396982 Color: 1
Size: 333381 Color: 1
Size: 269638 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 397000 Color: 1
Size: 336877 Color: 1
Size: 266124 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 397065 Color: 1
Size: 333198 Color: 1
Size: 269738 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 397101 Color: 1
Size: 340886 Color: 1
Size: 262014 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 397144 Color: 1
Size: 345047 Color: 1
Size: 257810 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 397145 Color: 1
Size: 333662 Color: 1
Size: 269194 Color: 0

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 397225 Color: 1
Size: 321097 Color: 1
Size: 281679 Color: 0

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 397330 Color: 1
Size: 316512 Color: 1
Size: 286159 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 397334 Color: 1
Size: 331471 Color: 1
Size: 271196 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 397368 Color: 1
Size: 321821 Color: 1
Size: 280812 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 397502 Color: 1
Size: 332101 Color: 1
Size: 270398 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 397508 Color: 1
Size: 308973 Color: 0
Size: 293520 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 397605 Color: 1
Size: 327743 Color: 1
Size: 274653 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 397656 Color: 1
Size: 331453 Color: 1
Size: 270892 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 397675 Color: 1
Size: 312709 Color: 1
Size: 289617 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 397715 Color: 1
Size: 319929 Color: 1
Size: 282357 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 397773 Color: 1
Size: 321864 Color: 1
Size: 280364 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 397910 Color: 1
Size: 327510 Color: 1
Size: 274581 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 397925 Color: 1
Size: 309564 Color: 0
Size: 292512 Color: 1

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 398041 Color: 1
Size: 325215 Color: 1
Size: 276745 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 398051 Color: 1
Size: 315818 Color: 1
Size: 286132 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 398054 Color: 1
Size: 315222 Color: 1
Size: 286725 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 362195 Color: 1
Size: 356417 Color: 1
Size: 281389 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 398370 Color: 1
Size: 339412 Color: 1
Size: 262219 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 398364 Color: 1
Size: 316638 Color: 1
Size: 284999 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 398459 Color: 1
Size: 333155 Color: 1
Size: 268387 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 398641 Color: 1
Size: 319724 Color: 1
Size: 281636 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 398691 Color: 1
Size: 326180 Color: 1
Size: 275130 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 398704 Color: 1
Size: 326457 Color: 1
Size: 274840 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 398743 Color: 1
Size: 340286 Color: 1
Size: 260972 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 398786 Color: 1
Size: 331035 Color: 1
Size: 270180 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 398848 Color: 1
Size: 313393 Color: 0
Size: 287760 Color: 1

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 398952 Color: 1
Size: 332636 Color: 1
Size: 268413 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 398962 Color: 1
Size: 311454 Color: 1
Size: 289585 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 399104 Color: 1
Size: 317122 Color: 1
Size: 283775 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 399120 Color: 1
Size: 344049 Color: 1
Size: 256832 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 399166 Color: 1
Size: 333668 Color: 1
Size: 267167 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 399186 Color: 1
Size: 320367 Color: 0
Size: 280448 Color: 1

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 399198 Color: 1
Size: 325657 Color: 1
Size: 275146 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 399215 Color: 1
Size: 331816 Color: 1
Size: 268970 Color: 0

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 399297 Color: 1
Size: 331921 Color: 1
Size: 268783 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 399326 Color: 1
Size: 344280 Color: 1
Size: 256395 Color: 0

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 399358 Color: 1
Size: 327139 Color: 1
Size: 273504 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 399380 Color: 1
Size: 331064 Color: 1
Size: 269557 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 399383 Color: 1
Size: 306907 Color: 1
Size: 293711 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 399513 Color: 1
Size: 346362 Color: 1
Size: 254126 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 399560 Color: 1
Size: 319240 Color: 1
Size: 281201 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 399633 Color: 1
Size: 329465 Color: 1
Size: 270903 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 399710 Color: 1
Size: 305310 Color: 1
Size: 294981 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 399746 Color: 1
Size: 337061 Color: 1
Size: 263194 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 399801 Color: 1
Size: 326981 Color: 1
Size: 273219 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 400009 Color: 1
Size: 329841 Color: 1
Size: 270151 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 400015 Color: 1
Size: 324649 Color: 1
Size: 275337 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 400023 Color: 1
Size: 319969 Color: 1
Size: 280009 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 400049 Color: 1
Size: 307687 Color: 0
Size: 292265 Color: 1

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 400109 Color: 1
Size: 305481 Color: 1
Size: 294411 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 400300 Color: 1
Size: 306973 Color: 0
Size: 292728 Color: 1

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 400304 Color: 1
Size: 341764 Color: 1
Size: 257933 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 400307 Color: 1
Size: 322849 Color: 1
Size: 276845 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 400321 Color: 1
Size: 316062 Color: 1
Size: 283618 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 400380 Color: 1
Size: 320329 Color: 1
Size: 279292 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 400383 Color: 1
Size: 329882 Color: 1
Size: 269736 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 400529 Color: 1
Size: 327340 Color: 1
Size: 272132 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 400626 Color: 1
Size: 316234 Color: 1
Size: 283141 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 400743 Color: 1
Size: 332620 Color: 1
Size: 266638 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 400790 Color: 1
Size: 327562 Color: 1
Size: 271649 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 400850 Color: 1
Size: 345589 Color: 1
Size: 253562 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 400893 Color: 1
Size: 319851 Color: 1
Size: 279257 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 400934 Color: 1
Size: 334214 Color: 1
Size: 264853 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 400948 Color: 1
Size: 319511 Color: 1
Size: 279542 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 400974 Color: 1
Size: 342076 Color: 1
Size: 256951 Color: 0

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 401118 Color: 1
Size: 318963 Color: 1
Size: 279920 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 401130 Color: 1
Size: 335264 Color: 1
Size: 263607 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 401202 Color: 1
Size: 314249 Color: 1
Size: 284550 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 401215 Color: 1
Size: 331068 Color: 1
Size: 267718 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 401258 Color: 1
Size: 322802 Color: 1
Size: 275941 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 401265 Color: 1
Size: 306066 Color: 1
Size: 292670 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 357543 Color: 1
Size: 334892 Color: 1
Size: 307566 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 401270 Color: 1
Size: 305835 Color: 1
Size: 292896 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 401307 Color: 1
Size: 305587 Color: 1
Size: 293107 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 401333 Color: 1
Size: 321569 Color: 1
Size: 277099 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 401358 Color: 1
Size: 313938 Color: 1
Size: 284705 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 401433 Color: 1
Size: 336091 Color: 1
Size: 262477 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 401436 Color: 1
Size: 333564 Color: 1
Size: 265001 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 401480 Color: 1
Size: 300337 Color: 0
Size: 298184 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 401617 Color: 1
Size: 341777 Color: 1
Size: 256607 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 401705 Color: 1
Size: 316614 Color: 1
Size: 281682 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 401768 Color: 1
Size: 332271 Color: 1
Size: 265962 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 401795 Color: 1
Size: 317507 Color: 1
Size: 280699 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 401896 Color: 1
Size: 340126 Color: 1
Size: 257979 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 402002 Color: 1
Size: 320153 Color: 1
Size: 277846 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 402033 Color: 1
Size: 333914 Color: 1
Size: 264054 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 402107 Color: 1
Size: 305177 Color: 1
Size: 292717 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 402117 Color: 1
Size: 321129 Color: 1
Size: 276755 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 402169 Color: 1
Size: 321517 Color: 1
Size: 276315 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 402241 Color: 1
Size: 337163 Color: 1
Size: 260597 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 402350 Color: 1
Size: 328682 Color: 1
Size: 268969 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 402443 Color: 1
Size: 317276 Color: 1
Size: 280282 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 402666 Color: 1
Size: 331565 Color: 1
Size: 265770 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 402702 Color: 1
Size: 340923 Color: 1
Size: 256376 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 402789 Color: 1
Size: 321223 Color: 1
Size: 275989 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 402864 Color: 1
Size: 344388 Color: 1
Size: 252749 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 402864 Color: 1
Size: 314014 Color: 1
Size: 283123 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 402930 Color: 1
Size: 314556 Color: 1
Size: 282515 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 402935 Color: 1
Size: 341104 Color: 1
Size: 255962 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 402955 Color: 1
Size: 337767 Color: 1
Size: 259279 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 403292 Color: 1
Size: 342852 Color: 1
Size: 253857 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 403456 Color: 1
Size: 330094 Color: 1
Size: 266451 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 403477 Color: 1
Size: 323730 Color: 1
Size: 272794 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 403679 Color: 1
Size: 341352 Color: 1
Size: 254970 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 403758 Color: 1
Size: 320618 Color: 1
Size: 275625 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 403778 Color: 1
Size: 330337 Color: 1
Size: 265886 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 403799 Color: 1
Size: 334747 Color: 1
Size: 261455 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 403842 Color: 1
Size: 302917 Color: 0
Size: 293242 Color: 1

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 403891 Color: 1
Size: 299536 Color: 1
Size: 296574 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 404211 Color: 1
Size: 298641 Color: 1
Size: 297149 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 404274 Color: 1
Size: 332886 Color: 1
Size: 262841 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 404276 Color: 1
Size: 327849 Color: 1
Size: 267876 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 404284 Color: 1
Size: 334636 Color: 1
Size: 261081 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 404412 Color: 1
Size: 340579 Color: 1
Size: 255010 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 404484 Color: 1
Size: 332226 Color: 1
Size: 263291 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 404489 Color: 1
Size: 330467 Color: 1
Size: 265045 Color: 0

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 404514 Color: 1
Size: 300578 Color: 0
Size: 294909 Color: 1

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 404560 Color: 1
Size: 333230 Color: 1
Size: 262211 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 404586 Color: 1
Size: 322702 Color: 1
Size: 272713 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 404787 Color: 1
Size: 313557 Color: 1
Size: 281657 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 404885 Color: 1
Size: 308950 Color: 1
Size: 286166 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 405038 Color: 1
Size: 300156 Color: 1
Size: 294807 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 405080 Color: 1
Size: 314811 Color: 1
Size: 280110 Color: 0

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 405196 Color: 1
Size: 325709 Color: 1
Size: 269096 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 405285 Color: 1
Size: 327994 Color: 1
Size: 266722 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 405327 Color: 1
Size: 333267 Color: 1
Size: 261407 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 405486 Color: 1
Size: 302129 Color: 0
Size: 292386 Color: 1

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 405523 Color: 1
Size: 312054 Color: 1
Size: 282424 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 405560 Color: 1
Size: 302969 Color: 1
Size: 291472 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 405624 Color: 1
Size: 309910 Color: 1
Size: 284467 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 405793 Color: 1
Size: 313876 Color: 1
Size: 280332 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 405880 Color: 1
Size: 315544 Color: 1
Size: 278577 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 357675 Color: 1
Size: 333025 Color: 1
Size: 309301 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 406088 Color: 1
Size: 324012 Color: 1
Size: 269901 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 406121 Color: 1
Size: 324508 Color: 1
Size: 269372 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 406185 Color: 1
Size: 330808 Color: 1
Size: 263008 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 406256 Color: 1
Size: 319063 Color: 1
Size: 274682 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 406308 Color: 1
Size: 321753 Color: 1
Size: 271940 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 406352 Color: 1
Size: 308984 Color: 1
Size: 284665 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 406362 Color: 1
Size: 323590 Color: 1
Size: 270049 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 406437 Color: 1
Size: 324779 Color: 1
Size: 268785 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 406457 Color: 1
Size: 324782 Color: 1
Size: 268762 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 406505 Color: 1
Size: 320447 Color: 1
Size: 273049 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 406524 Color: 1
Size: 341205 Color: 1
Size: 252272 Color: 0

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 406713 Color: 1
Size: 297304 Color: 0
Size: 295984 Color: 1

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 406800 Color: 1
Size: 330648 Color: 1
Size: 262553 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 406842 Color: 1
Size: 342082 Color: 1
Size: 251077 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 406919 Color: 1
Size: 342834 Color: 1
Size: 250248 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 407011 Color: 1
Size: 331387 Color: 1
Size: 261603 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 347796 Color: 1
Size: 326403 Color: 1
Size: 325802 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 407302 Color: 1
Size: 326373 Color: 1
Size: 266326 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 407354 Color: 1
Size: 296679 Color: 0
Size: 295968 Color: 1

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 407380 Color: 1
Size: 335193 Color: 1
Size: 257428 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 407452 Color: 1
Size: 340828 Color: 1
Size: 251721 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 407482 Color: 1
Size: 308677 Color: 1
Size: 283842 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 407590 Color: 1
Size: 319142 Color: 1
Size: 273269 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 407599 Color: 1
Size: 332438 Color: 1
Size: 259964 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 407645 Color: 1
Size: 305700 Color: 1
Size: 286656 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 407701 Color: 1
Size: 315721 Color: 1
Size: 276579 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 407753 Color: 1
Size: 301409 Color: 1
Size: 290839 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 407907 Color: 1
Size: 333269 Color: 1
Size: 258825 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 408037 Color: 1
Size: 318205 Color: 1
Size: 273759 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 408040 Color: 1
Size: 318312 Color: 1
Size: 273649 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 408040 Color: 1
Size: 332193 Color: 1
Size: 259768 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 408064 Color: 1
Size: 313215 Color: 1
Size: 278722 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 408127 Color: 1
Size: 310943 Color: 1
Size: 280931 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 408131 Color: 1
Size: 326760 Color: 1
Size: 265110 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 408167 Color: 1
Size: 306198 Color: 1
Size: 285636 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 408168 Color: 1
Size: 327602 Color: 1
Size: 264231 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 408258 Color: 1
Size: 309792 Color: 1
Size: 281951 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 408399 Color: 1
Size: 338879 Color: 1
Size: 252723 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 408457 Color: 1
Size: 321710 Color: 1
Size: 269834 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 408463 Color: 1
Size: 334690 Color: 1
Size: 256848 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 408552 Color: 1
Size: 330260 Color: 1
Size: 261189 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 408641 Color: 1
Size: 318766 Color: 1
Size: 272594 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 408675 Color: 1
Size: 318170 Color: 1
Size: 273156 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 408693 Color: 1
Size: 315325 Color: 1
Size: 275983 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 408782 Color: 1
Size: 334607 Color: 1
Size: 256612 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 408809 Color: 1
Size: 325051 Color: 1
Size: 266141 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 408875 Color: 1
Size: 326490 Color: 1
Size: 264636 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 409016 Color: 1
Size: 328272 Color: 1
Size: 262713 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 409028 Color: 1
Size: 310748 Color: 1
Size: 280225 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 409059 Color: 1
Size: 336589 Color: 1
Size: 254353 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 409300 Color: 1
Size: 330232 Color: 1
Size: 260469 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 409385 Color: 1
Size: 300778 Color: 1
Size: 289838 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 409546 Color: 1
Size: 317992 Color: 1
Size: 272463 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 409740 Color: 1
Size: 312209 Color: 1
Size: 278052 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 409779 Color: 1
Size: 327748 Color: 1
Size: 262474 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 409796 Color: 1
Size: 327767 Color: 1
Size: 262438 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 409809 Color: 1
Size: 318583 Color: 1
Size: 271609 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 409904 Color: 1
Size: 305175 Color: 0
Size: 284922 Color: 1

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 409931 Color: 1
Size: 330374 Color: 1
Size: 259696 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 409946 Color: 1
Size: 306039 Color: 1
Size: 284016 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 409962 Color: 1
Size: 311182 Color: 1
Size: 278857 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 410067 Color: 1
Size: 318269 Color: 1
Size: 271665 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 410094 Color: 1
Size: 320330 Color: 1
Size: 269577 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 410143 Color: 1
Size: 297196 Color: 1
Size: 292662 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 410179 Color: 1
Size: 328723 Color: 1
Size: 261099 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 410263 Color: 1
Size: 325251 Color: 1
Size: 264487 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 410312 Color: 1
Size: 301353 Color: 0
Size: 288336 Color: 1

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 410346 Color: 1
Size: 308082 Color: 1
Size: 281573 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 410370 Color: 1
Size: 322626 Color: 1
Size: 267005 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 410418 Color: 1
Size: 313437 Color: 1
Size: 276146 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 410563 Color: 1
Size: 301038 Color: 1
Size: 288400 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 410564 Color: 1
Size: 328994 Color: 1
Size: 260443 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 410580 Color: 1
Size: 301941 Color: 1
Size: 287480 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 410612 Color: 1
Size: 332799 Color: 1
Size: 256590 Color: 0

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 410667 Color: 1
Size: 309262 Color: 1
Size: 280072 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 410693 Color: 1
Size: 331806 Color: 1
Size: 257502 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 410695 Color: 1
Size: 300694 Color: 1
Size: 288612 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 410717 Color: 1
Size: 312771 Color: 1
Size: 276513 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 410864 Color: 1
Size: 334137 Color: 1
Size: 255000 Color: 0

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 410933 Color: 1
Size: 319141 Color: 1
Size: 269927 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 410950 Color: 1
Size: 309030 Color: 1
Size: 280021 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 411205 Color: 1
Size: 313389 Color: 1
Size: 275407 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 411427 Color: 1
Size: 321911 Color: 1
Size: 266663 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 411528 Color: 1
Size: 327688 Color: 1
Size: 260785 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 411548 Color: 1
Size: 302995 Color: 1
Size: 285458 Color: 0

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 411841 Color: 1
Size: 328079 Color: 1
Size: 260081 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 411882 Color: 1
Size: 310106 Color: 1
Size: 278013 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 411934 Color: 1
Size: 304101 Color: 0
Size: 283966 Color: 1

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 411973 Color: 1
Size: 324344 Color: 1
Size: 263684 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 412004 Color: 1
Size: 319392 Color: 1
Size: 268605 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 412053 Color: 1
Size: 316477 Color: 1
Size: 271471 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 412180 Color: 1
Size: 331524 Color: 1
Size: 256297 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 412502 Color: 1
Size: 306503 Color: 1
Size: 280996 Color: 0

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 383247 Color: 1
Size: 355985 Color: 1
Size: 260769 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 412582 Color: 1
Size: 319320 Color: 1
Size: 268099 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 412624 Color: 1
Size: 332187 Color: 1
Size: 255190 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 412642 Color: 1
Size: 324374 Color: 1
Size: 262985 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 412709 Color: 1
Size: 332586 Color: 1
Size: 254706 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 412803 Color: 1
Size: 326368 Color: 1
Size: 260830 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 412834 Color: 1
Size: 309455 Color: 1
Size: 277712 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 412873 Color: 1
Size: 316617 Color: 1
Size: 270511 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 412888 Color: 1
Size: 325206 Color: 1
Size: 261907 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 412982 Color: 1
Size: 304102 Color: 1
Size: 282917 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 413054 Color: 1
Size: 328677 Color: 1
Size: 258270 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 413061 Color: 1
Size: 315574 Color: 1
Size: 271366 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 413078 Color: 1
Size: 319947 Color: 1
Size: 266976 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 413204 Color: 1
Size: 314683 Color: 1
Size: 272114 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 413208 Color: 1
Size: 324907 Color: 1
Size: 261886 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 413277 Color: 1
Size: 312304 Color: 1
Size: 274420 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 413536 Color: 1
Size: 320418 Color: 1
Size: 266047 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 413546 Color: 1
Size: 315433 Color: 1
Size: 271022 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 413729 Color: 1
Size: 309104 Color: 1
Size: 277168 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 413743 Color: 1
Size: 332833 Color: 1
Size: 253425 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 413817 Color: 1
Size: 328101 Color: 1
Size: 258083 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 413835 Color: 1
Size: 316144 Color: 1
Size: 270022 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 413891 Color: 1
Size: 299893 Color: 1
Size: 286217 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 413913 Color: 1
Size: 297950 Color: 0
Size: 288138 Color: 1

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 413998 Color: 1
Size: 331844 Color: 1
Size: 254159 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 414028 Color: 1
Size: 301505 Color: 0
Size: 284468 Color: 1

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 414051 Color: 1
Size: 306031 Color: 1
Size: 279919 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 414058 Color: 1
Size: 326373 Color: 1
Size: 259570 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 414140 Color: 1
Size: 321865 Color: 1
Size: 263996 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 414180 Color: 1
Size: 293133 Color: 0
Size: 292688 Color: 1

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 414404 Color: 1
Size: 309961 Color: 1
Size: 275636 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 414540 Color: 1
Size: 330532 Color: 1
Size: 254929 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 414570 Color: 1
Size: 319548 Color: 1
Size: 265883 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 414574 Color: 1
Size: 312454 Color: 0
Size: 272973 Color: 1

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 414637 Color: 1
Size: 297516 Color: 1
Size: 287848 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 414790 Color: 1
Size: 328649 Color: 1
Size: 256562 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 414803 Color: 1
Size: 312900 Color: 1
Size: 272298 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 414844 Color: 1
Size: 334062 Color: 1
Size: 251095 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 414867 Color: 1
Size: 317679 Color: 1
Size: 267455 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 414921 Color: 1
Size: 295064 Color: 1
Size: 290016 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 415046 Color: 1
Size: 308725 Color: 1
Size: 276230 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 415098 Color: 1
Size: 312255 Color: 1
Size: 272648 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 415219 Color: 1
Size: 318564 Color: 1
Size: 266218 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 415237 Color: 1
Size: 296755 Color: 1
Size: 288009 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 415253 Color: 1
Size: 324546 Color: 1
Size: 260202 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 415324 Color: 1
Size: 310962 Color: 1
Size: 273715 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 415423 Color: 1
Size: 299531 Color: 1
Size: 285047 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 415493 Color: 1
Size: 313914 Color: 1
Size: 270594 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 404782 Color: 1
Size: 337683 Color: 1
Size: 257536 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 415538 Color: 1
Size: 332185 Color: 1
Size: 252278 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 415539 Color: 1
Size: 328839 Color: 1
Size: 255623 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 415543 Color: 1
Size: 314007 Color: 1
Size: 270451 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 415613 Color: 1
Size: 316790 Color: 1
Size: 267598 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 415640 Color: 1
Size: 308260 Color: 1
Size: 276101 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 415739 Color: 1
Size: 295612 Color: 0
Size: 288650 Color: 1

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 435672 Color: 1
Size: 296554 Color: 1
Size: 267775 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 415881 Color: 1
Size: 319545 Color: 1
Size: 264575 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 415909 Color: 1
Size: 333746 Color: 1
Size: 250346 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 416041 Color: 1
Size: 315520 Color: 1
Size: 268440 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 416070 Color: 1
Size: 302486 Color: 1
Size: 281445 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 416126 Color: 1
Size: 296108 Color: 0
Size: 287767 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 416176 Color: 1
Size: 306790 Color: 1
Size: 277035 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 416194 Color: 1
Size: 312829 Color: 1
Size: 270978 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 416311 Color: 1
Size: 332726 Color: 1
Size: 250964 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 416312 Color: 1
Size: 323399 Color: 1
Size: 260290 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 416466 Color: 1
Size: 306253 Color: 1
Size: 277282 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 416470 Color: 1
Size: 316670 Color: 1
Size: 266861 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 416481 Color: 1
Size: 310861 Color: 1
Size: 272659 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 416603 Color: 1
Size: 316894 Color: 1
Size: 266504 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 416763 Color: 1
Size: 294893 Color: 1
Size: 288345 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 1
Size: 316302 Color: 1
Size: 266884 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 416867 Color: 1
Size: 314709 Color: 1
Size: 268425 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 416907 Color: 1
Size: 311718 Color: 1
Size: 271376 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 416908 Color: 1
Size: 304399 Color: 1
Size: 278694 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 416931 Color: 1
Size: 320194 Color: 1
Size: 262876 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 416954 Color: 1
Size: 314782 Color: 1
Size: 268265 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 416992 Color: 1
Size: 305486 Color: 0
Size: 277523 Color: 1

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 417039 Color: 1
Size: 295635 Color: 1
Size: 287327 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 417065 Color: 1
Size: 316647 Color: 1
Size: 266289 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 417066 Color: 1
Size: 301429 Color: 1
Size: 281506 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 417304 Color: 1
Size: 316487 Color: 1
Size: 266210 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 417353 Color: 1
Size: 321664 Color: 1
Size: 260984 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 417625 Color: 1
Size: 321139 Color: 1
Size: 261237 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 417628 Color: 1
Size: 323387 Color: 1
Size: 258986 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 417671 Color: 1
Size: 302068 Color: 1
Size: 280262 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 417713 Color: 1
Size: 320344 Color: 1
Size: 261944 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 417875 Color: 1
Size: 324070 Color: 1
Size: 258056 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 417936 Color: 1
Size: 311529 Color: 1
Size: 270536 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 417943 Color: 1
Size: 307301 Color: 1
Size: 274757 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 418050 Color: 1
Size: 291434 Color: 0
Size: 290517 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 418156 Color: 1
Size: 322846 Color: 1
Size: 258999 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 418197 Color: 1
Size: 309561 Color: 1
Size: 272243 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 418319 Color: 1
Size: 313827 Color: 1
Size: 267855 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 418415 Color: 1
Size: 328685 Color: 1
Size: 252901 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 418598 Color: 1
Size: 310377 Color: 1
Size: 271026 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 418863 Color: 1
Size: 295117 Color: 0
Size: 286021 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 418907 Color: 1
Size: 305263 Color: 1
Size: 275831 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 418924 Color: 1
Size: 322208 Color: 1
Size: 258869 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 419029 Color: 1
Size: 329853 Color: 1
Size: 251119 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 419093 Color: 1
Size: 322812 Color: 1
Size: 258096 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 419132 Color: 1
Size: 323600 Color: 1
Size: 257269 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 419195 Color: 1
Size: 325037 Color: 1
Size: 255769 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 419210 Color: 1
Size: 312815 Color: 1
Size: 267976 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 419231 Color: 1
Size: 327926 Color: 1
Size: 252844 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 419262 Color: 1
Size: 309348 Color: 1
Size: 271391 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 419279 Color: 1
Size: 314719 Color: 1
Size: 266003 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 419360 Color: 1
Size: 310602 Color: 1
Size: 270039 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 419458 Color: 1
Size: 311708 Color: 1
Size: 268835 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 419486 Color: 1
Size: 306156 Color: 1
Size: 274359 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 419498 Color: 1
Size: 312104 Color: 1
Size: 268399 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 419645 Color: 1
Size: 299582 Color: 1
Size: 280774 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 419709 Color: 1
Size: 325351 Color: 1
Size: 254941 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 419730 Color: 1
Size: 300771 Color: 1
Size: 279500 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 419752 Color: 1
Size: 292263 Color: 1
Size: 287986 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 419765 Color: 1
Size: 317383 Color: 1
Size: 262853 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 419876 Color: 1
Size: 324351 Color: 1
Size: 255774 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 419897 Color: 1
Size: 314423 Color: 1
Size: 265681 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 419952 Color: 1
Size: 325299 Color: 1
Size: 254750 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 419964 Color: 1
Size: 305685 Color: 1
Size: 274352 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 419976 Color: 1
Size: 316739 Color: 1
Size: 263286 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 419982 Color: 1
Size: 293727 Color: 1
Size: 286292 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 420312 Color: 1
Size: 311038 Color: 1
Size: 268651 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 420512 Color: 1
Size: 324456 Color: 1
Size: 255033 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 420554 Color: 1
Size: 299740 Color: 1
Size: 279707 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 420767 Color: 1
Size: 297850 Color: 1
Size: 281384 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 420801 Color: 1
Size: 322853 Color: 1
Size: 256347 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 420866 Color: 1
Size: 309637 Color: 1
Size: 269498 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 420907 Color: 1
Size: 312858 Color: 1
Size: 266236 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 420917 Color: 1
Size: 309649 Color: 1
Size: 269435 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 421017 Color: 1
Size: 319228 Color: 1
Size: 259756 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 421028 Color: 1
Size: 295741 Color: 1
Size: 283232 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 421124 Color: 1
Size: 303387 Color: 1
Size: 275490 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 421338 Color: 1
Size: 303149 Color: 1
Size: 275514 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 421351 Color: 1
Size: 290430 Color: 0
Size: 288220 Color: 1

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 421361 Color: 1
Size: 307633 Color: 1
Size: 271007 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 421377 Color: 1
Size: 314448 Color: 1
Size: 264176 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 421424 Color: 1
Size: 314263 Color: 1
Size: 264314 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 421431 Color: 1
Size: 311140 Color: 1
Size: 267430 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 421459 Color: 1
Size: 295697 Color: 1
Size: 282845 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 421476 Color: 1
Size: 305742 Color: 1
Size: 272783 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 421776 Color: 1
Size: 321331 Color: 1
Size: 256894 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 421801 Color: 1
Size: 326776 Color: 1
Size: 251424 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 421862 Color: 1
Size: 322083 Color: 1
Size: 256056 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 421886 Color: 1
Size: 310916 Color: 1
Size: 267199 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 421894 Color: 1
Size: 290518 Color: 0
Size: 287589 Color: 1

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 421973 Color: 1
Size: 291401 Color: 1
Size: 286627 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 422082 Color: 1
Size: 290155 Color: 1
Size: 287764 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 422082 Color: 1
Size: 321154 Color: 1
Size: 256765 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 422102 Color: 1
Size: 311043 Color: 1
Size: 266856 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 422126 Color: 1
Size: 296705 Color: 0
Size: 281170 Color: 1

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 422144 Color: 1
Size: 300625 Color: 1
Size: 277232 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 422166 Color: 1
Size: 309494 Color: 1
Size: 268341 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 422170 Color: 1
Size: 308737 Color: 1
Size: 269094 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 422205 Color: 1
Size: 295492 Color: 1
Size: 282304 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 422215 Color: 1
Size: 313031 Color: 1
Size: 264755 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 422250 Color: 1
Size: 323070 Color: 1
Size: 254681 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 422283 Color: 1
Size: 310568 Color: 1
Size: 267150 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 422416 Color: 1
Size: 313124 Color: 1
Size: 264461 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 422614 Color: 1
Size: 312443 Color: 1
Size: 264944 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 422642 Color: 1
Size: 323302 Color: 1
Size: 254057 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 422704 Color: 1
Size: 309923 Color: 1
Size: 267374 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 422729 Color: 1
Size: 290483 Color: 1
Size: 286789 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 422820 Color: 1
Size: 321505 Color: 1
Size: 255676 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 422830 Color: 1
Size: 305397 Color: 1
Size: 271774 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 422879 Color: 1
Size: 321580 Color: 1
Size: 255542 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 423011 Color: 1
Size: 310959 Color: 1
Size: 266031 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 423118 Color: 1
Size: 316402 Color: 1
Size: 260481 Color: 0

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 423213 Color: 1
Size: 316543 Color: 1
Size: 260245 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 423298 Color: 1
Size: 325819 Color: 1
Size: 250884 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 423359 Color: 1
Size: 324893 Color: 1
Size: 251749 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 423385 Color: 1
Size: 317940 Color: 1
Size: 258676 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 423432 Color: 1
Size: 292766 Color: 1
Size: 283803 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 423697 Color: 1
Size: 314763 Color: 1
Size: 261541 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 423734 Color: 1
Size: 315418 Color: 1
Size: 260849 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 423739 Color: 1
Size: 325996 Color: 1
Size: 250266 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 423750 Color: 1
Size: 293283 Color: 1
Size: 282968 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 423783 Color: 1
Size: 297259 Color: 0
Size: 278959 Color: 1

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 423789 Color: 1
Size: 321384 Color: 1
Size: 254828 Color: 0

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 423891 Color: 1
Size: 315304 Color: 1
Size: 260806 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 423997 Color: 1
Size: 311341 Color: 1
Size: 264663 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 424023 Color: 1
Size: 323713 Color: 1
Size: 252265 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 424085 Color: 1
Size: 320694 Color: 1
Size: 255222 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 424163 Color: 1
Size: 306829 Color: 1
Size: 269009 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 424249 Color: 1
Size: 306713 Color: 1
Size: 269039 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 424347 Color: 1
Size: 315910 Color: 1
Size: 259744 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 424359 Color: 1
Size: 321483 Color: 1
Size: 254159 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 424483 Color: 1
Size: 314952 Color: 1
Size: 260566 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 424555 Color: 1
Size: 294059 Color: 1
Size: 281387 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 424671 Color: 1
Size: 306206 Color: 1
Size: 269124 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 424686 Color: 1
Size: 323350 Color: 1
Size: 251965 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 424716 Color: 1
Size: 323067 Color: 1
Size: 252218 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 424723 Color: 1
Size: 303512 Color: 1
Size: 271766 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 424732 Color: 1
Size: 315349 Color: 1
Size: 259920 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 424867 Color: 1
Size: 318723 Color: 1
Size: 256411 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 425061 Color: 1
Size: 316399 Color: 1
Size: 258541 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 425062 Color: 1
Size: 315796 Color: 1
Size: 259143 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 425181 Color: 1
Size: 307911 Color: 1
Size: 266909 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 346405 Color: 1
Size: 340359 Color: 1
Size: 313237 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 425357 Color: 1
Size: 314155 Color: 1
Size: 260489 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 425411 Color: 1
Size: 317186 Color: 1
Size: 257404 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 425427 Color: 1
Size: 293947 Color: 0
Size: 280627 Color: 1

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 425502 Color: 1
Size: 307059 Color: 1
Size: 267440 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 425532 Color: 1
Size: 294104 Color: 1
Size: 280365 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 425559 Color: 1
Size: 306049 Color: 1
Size: 268393 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 425636 Color: 1
Size: 322966 Color: 1
Size: 251399 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 425744 Color: 1
Size: 308539 Color: 1
Size: 265718 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 425932 Color: 1
Size: 295961 Color: 1
Size: 278108 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 425968 Color: 1
Size: 297942 Color: 1
Size: 276091 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 425997 Color: 1
Size: 307607 Color: 1
Size: 266397 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 426072 Color: 1
Size: 297154 Color: 1
Size: 276775 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 426234 Color: 1
Size: 306056 Color: 1
Size: 267711 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 426279 Color: 1
Size: 316583 Color: 1
Size: 257139 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 426292 Color: 1
Size: 302924 Color: 1
Size: 270785 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 426389 Color: 1
Size: 317334 Color: 1
Size: 256278 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 426393 Color: 1
Size: 295628 Color: 1
Size: 277980 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 426503 Color: 1
Size: 311072 Color: 1
Size: 262426 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 426566 Color: 1
Size: 319800 Color: 1
Size: 253635 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 426827 Color: 1
Size: 313748 Color: 1
Size: 259426 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 427006 Color: 1
Size: 297352 Color: 1
Size: 275643 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 427128 Color: 1
Size: 303768 Color: 1
Size: 269105 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 427165 Color: 1
Size: 293023 Color: 1
Size: 279813 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 427185 Color: 1
Size: 306529 Color: 1
Size: 266287 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 427199 Color: 1
Size: 291377 Color: 1
Size: 281425 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 427414 Color: 1
Size: 288901 Color: 0
Size: 283686 Color: 1

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 427518 Color: 1
Size: 305934 Color: 1
Size: 266549 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 427533 Color: 1
Size: 313647 Color: 1
Size: 258821 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 427542 Color: 1
Size: 307389 Color: 1
Size: 265070 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 427654 Color: 1
Size: 317582 Color: 1
Size: 254765 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 427711 Color: 1
Size: 306824 Color: 0
Size: 265466 Color: 1

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 427874 Color: 1
Size: 310117 Color: 1
Size: 262010 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 428003 Color: 1
Size: 309983 Color: 1
Size: 262015 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 428099 Color: 1
Size: 303542 Color: 1
Size: 268360 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 428177 Color: 1
Size: 310561 Color: 1
Size: 261263 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 428227 Color: 1
Size: 311455 Color: 1
Size: 260319 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 428281 Color: 1
Size: 292205 Color: 1
Size: 279515 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 428328 Color: 1
Size: 315484 Color: 1
Size: 256189 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 428493 Color: 1
Size: 316707 Color: 1
Size: 254801 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 428675 Color: 1
Size: 313515 Color: 1
Size: 257811 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 428684 Color: 1
Size: 306015 Color: 1
Size: 265302 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 428709 Color: 1
Size: 311145 Color: 1
Size: 260147 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 428829 Color: 1
Size: 315687 Color: 1
Size: 255485 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 428852 Color: 1
Size: 286385 Color: 1
Size: 284764 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 428852 Color: 1
Size: 306929 Color: 1
Size: 264220 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 428928 Color: 1
Size: 298294 Color: 1
Size: 272779 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 429030 Color: 1
Size: 315582 Color: 1
Size: 255389 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 429072 Color: 1
Size: 314152 Color: 1
Size: 256777 Color: 0

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 429088 Color: 1
Size: 286855 Color: 0
Size: 284058 Color: 1

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 429115 Color: 1
Size: 313617 Color: 1
Size: 257269 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 429178 Color: 1
Size: 289097 Color: 1
Size: 281726 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 429336 Color: 1
Size: 305560 Color: 1
Size: 265105 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 429337 Color: 1
Size: 309076 Color: 1
Size: 261588 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 429359 Color: 1
Size: 319673 Color: 1
Size: 250969 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 429469 Color: 1
Size: 313812 Color: 1
Size: 256720 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 429652 Color: 1
Size: 286095 Color: 1
Size: 284254 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 429677 Color: 1
Size: 309717 Color: 1
Size: 260607 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 429743 Color: 1
Size: 313272 Color: 1
Size: 256986 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 430131 Color: 1
Size: 297075 Color: 1
Size: 272795 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 430278 Color: 1
Size: 299256 Color: 1
Size: 270467 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 430317 Color: 1
Size: 302221 Color: 1
Size: 267463 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 430488 Color: 1
Size: 314094 Color: 1
Size: 255419 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 430508 Color: 1
Size: 318775 Color: 1
Size: 250718 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 430519 Color: 1
Size: 305245 Color: 1
Size: 264237 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 430637 Color: 1
Size: 308993 Color: 1
Size: 260371 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 430647 Color: 1
Size: 310666 Color: 1
Size: 258688 Color: 0

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 430758 Color: 1
Size: 305422 Color: 1
Size: 263821 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 430841 Color: 1
Size: 310038 Color: 1
Size: 259122 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 430895 Color: 1
Size: 303721 Color: 1
Size: 265385 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 430932 Color: 1
Size: 306493 Color: 1
Size: 262576 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 430935 Color: 1
Size: 297732 Color: 1
Size: 271334 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 431019 Color: 1
Size: 308478 Color: 1
Size: 260504 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 431155 Color: 1
Size: 303883 Color: 1
Size: 264963 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 431277 Color: 1
Size: 307420 Color: 1
Size: 261304 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 431330 Color: 1
Size: 316180 Color: 1
Size: 252491 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 431376 Color: 1
Size: 292884 Color: 1
Size: 275741 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 431384 Color: 1
Size: 306704 Color: 1
Size: 261913 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 431506 Color: 1
Size: 311598 Color: 1
Size: 256897 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 431550 Color: 1
Size: 309621 Color: 1
Size: 258830 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 431580 Color: 1
Size: 291545 Color: 0
Size: 276876 Color: 1

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 431752 Color: 1
Size: 302713 Color: 1
Size: 265536 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 431861 Color: 1
Size: 314033 Color: 1
Size: 254107 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 431882 Color: 1
Size: 305153 Color: 1
Size: 262966 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 431950 Color: 1
Size: 312986 Color: 1
Size: 255065 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 432045 Color: 1
Size: 315375 Color: 1
Size: 252581 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 432107 Color: 1
Size: 294128 Color: 1
Size: 273766 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 432286 Color: 1
Size: 295537 Color: 1
Size: 272178 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 432382 Color: 1
Size: 308174 Color: 1
Size: 259445 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 433293 Color: 1
Size: 311279 Color: 1
Size: 255429 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 432415 Color: 1
Size: 296961 Color: 1
Size: 270625 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 432532 Color: 1
Size: 310640 Color: 1
Size: 256829 Color: 0

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 432553 Color: 1
Size: 309065 Color: 1
Size: 258383 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 432628 Color: 1
Size: 304296 Color: 1
Size: 263077 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 432678 Color: 1
Size: 285093 Color: 1
Size: 282230 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 432740 Color: 1
Size: 302236 Color: 1
Size: 265025 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 432740 Color: 1
Size: 290019 Color: 0
Size: 277242 Color: 1

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 432761 Color: 1
Size: 315726 Color: 1
Size: 251514 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 433004 Color: 1
Size: 307191 Color: 1
Size: 259806 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 433048 Color: 1
Size: 303618 Color: 1
Size: 263335 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 433181 Color: 1
Size: 312401 Color: 1
Size: 254419 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 433204 Color: 1
Size: 294954 Color: 1
Size: 271843 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 433258 Color: 1
Size: 309345 Color: 1
Size: 257398 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 433311 Color: 1
Size: 306802 Color: 1
Size: 259888 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 433429 Color: 1
Size: 306180 Color: 1
Size: 260392 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 433527 Color: 1
Size: 301207 Color: 1
Size: 265267 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 433708 Color: 1
Size: 300260 Color: 1
Size: 266033 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 433800 Color: 1
Size: 300987 Color: 1
Size: 265214 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 433841 Color: 1
Size: 304741 Color: 1
Size: 261419 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 433856 Color: 1
Size: 313413 Color: 1
Size: 252732 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 434030 Color: 1
Size: 301267 Color: 1
Size: 264704 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 434031 Color: 1
Size: 302598 Color: 1
Size: 263372 Color: 0

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 434039 Color: 1
Size: 308759 Color: 1
Size: 257203 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 434053 Color: 1
Size: 295347 Color: 1
Size: 270601 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 434063 Color: 1
Size: 286805 Color: 1
Size: 279133 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 434091 Color: 1
Size: 286096 Color: 1
Size: 279814 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 434198 Color: 1
Size: 312458 Color: 1
Size: 253345 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 434272 Color: 1
Size: 296329 Color: 1
Size: 269400 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 434397 Color: 1
Size: 310420 Color: 1
Size: 255184 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 434459 Color: 1
Size: 308127 Color: 1
Size: 257415 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 434618 Color: 1
Size: 304833 Color: 1
Size: 260550 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 434624 Color: 1
Size: 313890 Color: 1
Size: 251487 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 434684 Color: 1
Size: 298910 Color: 1
Size: 266407 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 434838 Color: 1
Size: 298830 Color: 1
Size: 266333 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 434901 Color: 1
Size: 309333 Color: 1
Size: 255767 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 435004 Color: 1
Size: 296496 Color: 1
Size: 268501 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 435053 Color: 1
Size: 288002 Color: 1
Size: 276946 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 435091 Color: 1
Size: 304364 Color: 1
Size: 260546 Color: 0

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 435102 Color: 1
Size: 285245 Color: 1
Size: 279654 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 435207 Color: 1
Size: 308717 Color: 1
Size: 256077 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 435274 Color: 1
Size: 303613 Color: 1
Size: 261114 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 435409 Color: 1
Size: 298235 Color: 1
Size: 266357 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 435448 Color: 1
Size: 309665 Color: 1
Size: 254888 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 435693 Color: 1
Size: 310478 Color: 1
Size: 253830 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 435843 Color: 1
Size: 306725 Color: 1
Size: 257433 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 435929 Color: 1
Size: 289921 Color: 1
Size: 274151 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 436033 Color: 1
Size: 309764 Color: 1
Size: 254204 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 436139 Color: 1
Size: 295084 Color: 1
Size: 268778 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 436171 Color: 1
Size: 310221 Color: 1
Size: 253609 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 436186 Color: 1
Size: 309627 Color: 1
Size: 254188 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 436216 Color: 1
Size: 308071 Color: 1
Size: 255714 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 436225 Color: 1
Size: 300514 Color: 1
Size: 263262 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 436246 Color: 1
Size: 285199 Color: 0
Size: 278556 Color: 1

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 436348 Color: 1
Size: 293011 Color: 1
Size: 270642 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 436433 Color: 1
Size: 306568 Color: 1
Size: 257000 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 436433 Color: 1
Size: 290941 Color: 1
Size: 272627 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 436464 Color: 1
Size: 306778 Color: 1
Size: 256759 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 436527 Color: 1
Size: 290112 Color: 1
Size: 273362 Color: 0

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 436603 Color: 1
Size: 292431 Color: 1
Size: 270967 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 436789 Color: 1
Size: 295082 Color: 1
Size: 268130 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 436833 Color: 1
Size: 293822 Color: 1
Size: 269346 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 436870 Color: 1
Size: 283658 Color: 1
Size: 279473 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 436944 Color: 1
Size: 297172 Color: 1
Size: 265885 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 437135 Color: 1
Size: 295881 Color: 1
Size: 266985 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 437203 Color: 1
Size: 288973 Color: 1
Size: 273825 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 437254 Color: 1
Size: 298454 Color: 1
Size: 264293 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 437273 Color: 1
Size: 283655 Color: 0
Size: 279073 Color: 1

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 437434 Color: 1
Size: 304251 Color: 1
Size: 258316 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 437537 Color: 1
Size: 287665 Color: 1
Size: 274799 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 437635 Color: 1
Size: 306512 Color: 1
Size: 255854 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 437646 Color: 1
Size: 299036 Color: 1
Size: 263319 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 437907 Color: 1
Size: 297129 Color: 1
Size: 264965 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 437925 Color: 1
Size: 295029 Color: 1
Size: 267047 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 438058 Color: 1
Size: 302242 Color: 1
Size: 259701 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 438127 Color: 1
Size: 301945 Color: 1
Size: 259929 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 438249 Color: 1
Size: 304620 Color: 1
Size: 257132 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 438374 Color: 1
Size: 297861 Color: 1
Size: 263766 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 438414 Color: 1
Size: 306348 Color: 1
Size: 255239 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 438418 Color: 1
Size: 294376 Color: 1
Size: 267207 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 438446 Color: 1
Size: 282121 Color: 0
Size: 279434 Color: 1

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 438530 Color: 1
Size: 308935 Color: 1
Size: 252536 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 438614 Color: 1
Size: 300667 Color: 1
Size: 260720 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 438635 Color: 1
Size: 310342 Color: 1
Size: 251024 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 438670 Color: 1
Size: 295167 Color: 1
Size: 266164 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 438729 Color: 1
Size: 301934 Color: 1
Size: 259338 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 438744 Color: 1
Size: 305862 Color: 1
Size: 255395 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 438757 Color: 1
Size: 301660 Color: 1
Size: 259584 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 438967 Color: 1
Size: 303210 Color: 1
Size: 257824 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 439111 Color: 1
Size: 299717 Color: 1
Size: 261173 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 439156 Color: 1
Size: 285330 Color: 1
Size: 275515 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 439157 Color: 1
Size: 287241 Color: 1
Size: 273603 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 439412 Color: 1
Size: 305797 Color: 1
Size: 254792 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 439698 Color: 1
Size: 309096 Color: 1
Size: 251207 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 439737 Color: 1
Size: 305084 Color: 1
Size: 255180 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 439755 Color: 1
Size: 300229 Color: 1
Size: 260017 Color: 0

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 439771 Color: 1
Size: 299969 Color: 1
Size: 260261 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 439938 Color: 1
Size: 296874 Color: 1
Size: 263189 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 439953 Color: 1
Size: 303229 Color: 1
Size: 256819 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 439975 Color: 1
Size: 308960 Color: 1
Size: 251066 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 440026 Color: 1
Size: 286715 Color: 1
Size: 273260 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 440361 Color: 1
Size: 304333 Color: 1
Size: 255307 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 440530 Color: 1
Size: 302100 Color: 1
Size: 257371 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 440578 Color: 1
Size: 299774 Color: 1
Size: 259649 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 440652 Color: 1
Size: 302191 Color: 1
Size: 257158 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 440657 Color: 1
Size: 309282 Color: 1
Size: 250062 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 440669 Color: 1
Size: 308282 Color: 1
Size: 251050 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 440697 Color: 1
Size: 302165 Color: 1
Size: 257139 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 440712 Color: 1
Size: 285708 Color: 1
Size: 273581 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 440715 Color: 1
Size: 297453 Color: 1
Size: 261833 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 440721 Color: 1
Size: 285291 Color: 1
Size: 273989 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 440731 Color: 1
Size: 289358 Color: 1
Size: 269912 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 440784 Color: 1
Size: 281553 Color: 0
Size: 277664 Color: 1

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 440870 Color: 1
Size: 293258 Color: 1
Size: 265873 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 441321 Color: 1
Size: 281768 Color: 1
Size: 276912 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 441431 Color: 1
Size: 286411 Color: 0
Size: 272159 Color: 1

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 441435 Color: 1
Size: 302863 Color: 1
Size: 255703 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 491486 Color: 1
Size: 256965 Color: 1
Size: 251550 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 441577 Color: 1
Size: 295599 Color: 1
Size: 262825 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 441626 Color: 1
Size: 304920 Color: 1
Size: 253455 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 441688 Color: 1
Size: 294956 Color: 0
Size: 263357 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 441784 Color: 1
Size: 305317 Color: 1
Size: 252900 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 441813 Color: 1
Size: 294820 Color: 1
Size: 263368 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 441867 Color: 1
Size: 301357 Color: 1
Size: 256777 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 441884 Color: 1
Size: 284935 Color: 1
Size: 273182 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 441894 Color: 1
Size: 303661 Color: 1
Size: 254446 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 441906 Color: 1
Size: 280378 Color: 1
Size: 277717 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 442143 Color: 1
Size: 301679 Color: 1
Size: 256179 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 442158 Color: 1
Size: 297709 Color: 1
Size: 260134 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 442245 Color: 1
Size: 289491 Color: 1
Size: 268265 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 442392 Color: 1
Size: 287500 Color: 1
Size: 270109 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 442423 Color: 1
Size: 297560 Color: 1
Size: 260018 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 442578 Color: 1
Size: 304324 Color: 1
Size: 253099 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 442756 Color: 1
Size: 298281 Color: 1
Size: 258964 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 442823 Color: 1
Size: 298355 Color: 1
Size: 258823 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 442823 Color: 1
Size: 288868 Color: 1
Size: 268310 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 442843 Color: 1
Size: 300949 Color: 1
Size: 256209 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 442868 Color: 1
Size: 293723 Color: 1
Size: 263410 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 442874 Color: 1
Size: 296950 Color: 1
Size: 260177 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 442889 Color: 1
Size: 281189 Color: 1
Size: 275923 Color: 0

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 442907 Color: 1
Size: 299232 Color: 1
Size: 257862 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 442968 Color: 1
Size: 301628 Color: 1
Size: 255405 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 443009 Color: 1
Size: 305400 Color: 1
Size: 251592 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 443023 Color: 1
Size: 298014 Color: 1
Size: 258964 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 443076 Color: 1
Size: 283725 Color: 1
Size: 273200 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 443182 Color: 1
Size: 294469 Color: 1
Size: 262350 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 443204 Color: 1
Size: 305308 Color: 1
Size: 251489 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 443253 Color: 1
Size: 293525 Color: 1
Size: 263223 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 443378 Color: 1
Size: 302618 Color: 1
Size: 254005 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 443455 Color: 1
Size: 282104 Color: 1
Size: 274442 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 443478 Color: 1
Size: 296293 Color: 1
Size: 260230 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 443489 Color: 1
Size: 299584 Color: 1
Size: 256928 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 443608 Color: 1
Size: 281952 Color: 1
Size: 274441 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 443830 Color: 1
Size: 299502 Color: 1
Size: 256669 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 443926 Color: 1
Size: 298745 Color: 1
Size: 257330 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 444093 Color: 1
Size: 290707 Color: 1
Size: 265201 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 444161 Color: 1
Size: 297897 Color: 1
Size: 257943 Color: 0

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 444175 Color: 1
Size: 291035 Color: 1
Size: 264791 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 444177 Color: 1
Size: 296217 Color: 1
Size: 259607 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 444199 Color: 1
Size: 297389 Color: 1
Size: 258413 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 444233 Color: 1
Size: 280282 Color: 1
Size: 275486 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 444394 Color: 1
Size: 282816 Color: 0
Size: 272791 Color: 1

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 444460 Color: 1
Size: 296977 Color: 1
Size: 258564 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 444560 Color: 1
Size: 292862 Color: 1
Size: 262579 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 444567 Color: 1
Size: 304809 Color: 1
Size: 250625 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 444590 Color: 1
Size: 287804 Color: 1
Size: 267607 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 444591 Color: 1
Size: 283865 Color: 0
Size: 271545 Color: 1

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 444649 Color: 1
Size: 287788 Color: 1
Size: 267564 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 444742 Color: 1
Size: 298389 Color: 1
Size: 256870 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 444863 Color: 1
Size: 299422 Color: 1
Size: 255716 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 444955 Color: 1
Size: 304407 Color: 1
Size: 250639 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 445002 Color: 1
Size: 302727 Color: 1
Size: 252272 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 445031 Color: 1
Size: 285621 Color: 1
Size: 269349 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 445052 Color: 1
Size: 293254 Color: 1
Size: 261695 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 445088 Color: 1
Size: 293599 Color: 1
Size: 261314 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 445228 Color: 1
Size: 292802 Color: 1
Size: 261971 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 445365 Color: 1
Size: 302195 Color: 1
Size: 252441 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 445455 Color: 1
Size: 285123 Color: 1
Size: 269423 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 445551 Color: 1
Size: 293383 Color: 1
Size: 261067 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 445838 Color: 1
Size: 294149 Color: 1
Size: 260014 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 445972 Color: 1
Size: 299560 Color: 1
Size: 254469 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 446088 Color: 1
Size: 280234 Color: 0
Size: 273679 Color: 1

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 446139 Color: 1
Size: 298492 Color: 1
Size: 255370 Color: 0

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 446320 Color: 1
Size: 300863 Color: 1
Size: 252818 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 446489 Color: 1
Size: 299331 Color: 1
Size: 254181 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 446547 Color: 1
Size: 294921 Color: 1
Size: 258533 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 446563 Color: 1
Size: 300232 Color: 1
Size: 253206 Color: 0

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 446580 Color: 1
Size: 286300 Color: 1
Size: 267121 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 446635 Color: 1
Size: 300108 Color: 1
Size: 253258 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 446792 Color: 1
Size: 286043 Color: 1
Size: 267166 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 447015 Color: 1
Size: 296807 Color: 1
Size: 256179 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 447162 Color: 1
Size: 279401 Color: 1
Size: 273438 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 447350 Color: 1
Size: 289564 Color: 1
Size: 263087 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 447377 Color: 1
Size: 299798 Color: 1
Size: 252826 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447396 Color: 1
Size: 294334 Color: 1
Size: 258271 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447573 Color: 1
Size: 297289 Color: 1
Size: 255139 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 447598 Color: 1
Size: 289164 Color: 1
Size: 263239 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447644 Color: 1
Size: 279756 Color: 1
Size: 272601 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 447689 Color: 1
Size: 300756 Color: 1
Size: 251556 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 447750 Color: 1
Size: 289593 Color: 1
Size: 262658 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 447767 Color: 1
Size: 300821 Color: 1
Size: 251413 Color: 0

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 447835 Color: 1
Size: 298831 Color: 1
Size: 253335 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448058 Color: 1
Size: 293785 Color: 1
Size: 258158 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 448402 Color: 1
Size: 299782 Color: 1
Size: 251817 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448587 Color: 1
Size: 287946 Color: 1
Size: 263468 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448587 Color: 1
Size: 289301 Color: 1
Size: 262113 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448617 Color: 1
Size: 299328 Color: 1
Size: 252056 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 448643 Color: 1
Size: 297102 Color: 1
Size: 254256 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 448794 Color: 1
Size: 285887 Color: 1
Size: 265320 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 448933 Color: 1
Size: 296071 Color: 1
Size: 254997 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 449190 Color: 1
Size: 284544 Color: 0
Size: 266267 Color: 1

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 449214 Color: 1
Size: 292568 Color: 1
Size: 258219 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 449378 Color: 1
Size: 295921 Color: 1
Size: 254702 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 449453 Color: 1
Size: 298411 Color: 1
Size: 252137 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 449454 Color: 1
Size: 276240 Color: 0
Size: 274307 Color: 1

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 449491 Color: 1
Size: 296450 Color: 1
Size: 254060 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 449527 Color: 1
Size: 292525 Color: 1
Size: 257949 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 449536 Color: 1
Size: 289058 Color: 1
Size: 261407 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 449822 Color: 1
Size: 299543 Color: 1
Size: 250636 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 449839 Color: 1
Size: 299654 Color: 1
Size: 250508 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 449853 Color: 1
Size: 293878 Color: 1
Size: 256270 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 449883 Color: 1
Size: 287924 Color: 1
Size: 262194 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 449926 Color: 1
Size: 281461 Color: 1
Size: 268614 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 450455 Color: 1
Size: 288393 Color: 1
Size: 261153 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 450470 Color: 1
Size: 285657 Color: 1
Size: 263874 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 450526 Color: 1
Size: 281952 Color: 1
Size: 267523 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 450540 Color: 1
Size: 286671 Color: 1
Size: 262790 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 450809 Color: 1
Size: 290441 Color: 1
Size: 258751 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 450839 Color: 1
Size: 289128 Color: 1
Size: 260034 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 450954 Color: 1
Size: 292770 Color: 1
Size: 256277 Color: 0

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 450973 Color: 1
Size: 296810 Color: 1
Size: 252218 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 451086 Color: 1
Size: 282487 Color: 1
Size: 266428 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 451166 Color: 1
Size: 296419 Color: 1
Size: 252416 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 451189 Color: 1
Size: 291229 Color: 1
Size: 257583 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 451356 Color: 1
Size: 290595 Color: 1
Size: 258050 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 451442 Color: 1
Size: 277491 Color: 1
Size: 271068 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 451495 Color: 1
Size: 288376 Color: 1
Size: 260130 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 451693 Color: 1
Size: 286915 Color: 1
Size: 261393 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 451742 Color: 1
Size: 296838 Color: 1
Size: 251421 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 451763 Color: 1
Size: 290132 Color: 1
Size: 258106 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 451886 Color: 1
Size: 294689 Color: 1
Size: 253426 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 451904 Color: 1
Size: 280858 Color: 1
Size: 267239 Color: 0

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 451971 Color: 1
Size: 281777 Color: 1
Size: 266253 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 451990 Color: 1
Size: 288145 Color: 1
Size: 259866 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 452127 Color: 1
Size: 282780 Color: 1
Size: 265094 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 452135 Color: 1
Size: 296194 Color: 1
Size: 251672 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 452158 Color: 1
Size: 291304 Color: 1
Size: 256539 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 452202 Color: 1
Size: 291147 Color: 1
Size: 256652 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 452214 Color: 1
Size: 293648 Color: 1
Size: 254139 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 452236 Color: 1
Size: 279161 Color: 1
Size: 268604 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 452276 Color: 1
Size: 293634 Color: 1
Size: 254091 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 452308 Color: 1
Size: 282522 Color: 1
Size: 265171 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 452557 Color: 1
Size: 275010 Color: 1
Size: 272434 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 452631 Color: 1
Size: 287576 Color: 1
Size: 259794 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 452707 Color: 1
Size: 287065 Color: 1
Size: 260229 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 452723 Color: 1
Size: 280379 Color: 1
Size: 266899 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 452723 Color: 1
Size: 292321 Color: 1
Size: 254957 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 453017 Color: 1
Size: 288327 Color: 1
Size: 258657 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 453044 Color: 1
Size: 295178 Color: 1
Size: 251779 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 453209 Color: 1
Size: 283830 Color: 1
Size: 262962 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 453229 Color: 1
Size: 283062 Color: 1
Size: 263710 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 453253 Color: 1
Size: 293212 Color: 1
Size: 253536 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 453476 Color: 1
Size: 292630 Color: 1
Size: 253895 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 453480 Color: 1
Size: 291522 Color: 1
Size: 254999 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 453535 Color: 1
Size: 286579 Color: 1
Size: 259887 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 453548 Color: 1
Size: 294024 Color: 1
Size: 252429 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 453617 Color: 1
Size: 278214 Color: 1
Size: 268170 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 453801 Color: 1
Size: 291563 Color: 1
Size: 254637 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 453966 Color: 1
Size: 282104 Color: 1
Size: 263931 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 454076 Color: 1
Size: 277785 Color: 1
Size: 268140 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 454153 Color: 1
Size: 291964 Color: 1
Size: 253884 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 454163 Color: 1
Size: 283903 Color: 1
Size: 261935 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 454294 Color: 1
Size: 282344 Color: 1
Size: 263363 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 454307 Color: 1
Size: 293147 Color: 1
Size: 252547 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 454325 Color: 1
Size: 282609 Color: 1
Size: 263067 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 454364 Color: 1
Size: 275775 Color: 1
Size: 269862 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 454378 Color: 1
Size: 283173 Color: 1
Size: 262450 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 454437 Color: 1
Size: 282323 Color: 1
Size: 263241 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 454704 Color: 1
Size: 279741 Color: 1
Size: 265556 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 454798 Color: 1
Size: 288136 Color: 1
Size: 257067 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 454920 Color: 1
Size: 292606 Color: 1
Size: 252475 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 454939 Color: 1
Size: 294039 Color: 1
Size: 251023 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 455010 Color: 1
Size: 293863 Color: 1
Size: 251128 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 455096 Color: 1
Size: 285159 Color: 1
Size: 259746 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 455186 Color: 1
Size: 286101 Color: 1
Size: 258714 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 455227 Color: 1
Size: 282719 Color: 1
Size: 262055 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 455284 Color: 1
Size: 280050 Color: 1
Size: 264667 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 455366 Color: 1
Size: 282040 Color: 1
Size: 262595 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 455710 Color: 1
Size: 293861 Color: 1
Size: 250430 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 455717 Color: 1
Size: 284940 Color: 1
Size: 259344 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 455756 Color: 1
Size: 288882 Color: 1
Size: 255363 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 455783 Color: 1
Size: 276913 Color: 1
Size: 267305 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 455834 Color: 1
Size: 291060 Color: 1
Size: 253107 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 455902 Color: 1
Size: 290074 Color: 1
Size: 254025 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 456011 Color: 1
Size: 281176 Color: 1
Size: 262814 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 456029 Color: 1
Size: 291225 Color: 1
Size: 252747 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 456372 Color: 1
Size: 288717 Color: 1
Size: 254912 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 456520 Color: 1
Size: 284647 Color: 1
Size: 258834 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 456676 Color: 1
Size: 287517 Color: 1
Size: 255808 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 456827 Color: 1
Size: 275214 Color: 1
Size: 267960 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 457137 Color: 1
Size: 281178 Color: 1
Size: 261686 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 457183 Color: 1
Size: 276108 Color: 1
Size: 266710 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 457241 Color: 1
Size: 281026 Color: 1
Size: 261734 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 457345 Color: 1
Size: 280364 Color: 1
Size: 262292 Color: 0

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 457361 Color: 1
Size: 288720 Color: 1
Size: 253920 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 457440 Color: 1
Size: 280811 Color: 1
Size: 261750 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 457446 Color: 1
Size: 275740 Color: 1
Size: 266815 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 457518 Color: 1
Size: 274724 Color: 0
Size: 267759 Color: 1

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 457570 Color: 1
Size: 290038 Color: 1
Size: 252393 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 457613 Color: 1
Size: 289391 Color: 1
Size: 252997 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 457678 Color: 1
Size: 274971 Color: 1
Size: 267352 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 457687 Color: 1
Size: 290157 Color: 1
Size: 252157 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 457709 Color: 1
Size: 274019 Color: 0
Size: 268273 Color: 1

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 457856 Color: 1
Size: 276740 Color: 1
Size: 265405 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 457933 Color: 1
Size: 282545 Color: 1
Size: 259523 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 458078 Color: 1
Size: 277590 Color: 1
Size: 264333 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 458148 Color: 1
Size: 276348 Color: 0
Size: 265505 Color: 1

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 458154 Color: 1
Size: 290096 Color: 1
Size: 251751 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 458262 Color: 1
Size: 280638 Color: 1
Size: 261101 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 458312 Color: 1
Size: 285021 Color: 1
Size: 256668 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 458356 Color: 1
Size: 288738 Color: 1
Size: 252907 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 458386 Color: 1
Size: 289519 Color: 1
Size: 252096 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 458393 Color: 1
Size: 278280 Color: 1
Size: 263328 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 458411 Color: 1
Size: 287592 Color: 1
Size: 253998 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 458483 Color: 1
Size: 281370 Color: 1
Size: 260148 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 458566 Color: 1
Size: 281582 Color: 1
Size: 259853 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 458616 Color: 1
Size: 287446 Color: 1
Size: 253939 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 458859 Color: 1
Size: 290282 Color: 1
Size: 250860 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 458912 Color: 1
Size: 284161 Color: 0
Size: 256928 Color: 1

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 459071 Color: 1
Size: 274956 Color: 1
Size: 265974 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 459075 Color: 1
Size: 288236 Color: 1
Size: 252690 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 459381 Color: 1
Size: 275945 Color: 1
Size: 264675 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 459435 Color: 1
Size: 282517 Color: 1
Size: 258049 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 459512 Color: 1
Size: 277816 Color: 1
Size: 262673 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 459728 Color: 1
Size: 286410 Color: 1
Size: 253863 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 459740 Color: 1
Size: 288150 Color: 1
Size: 252111 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 459796 Color: 1
Size: 283920 Color: 1
Size: 256285 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 460281 Color: 1
Size: 279292 Color: 1
Size: 260428 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 460419 Color: 1
Size: 282945 Color: 1
Size: 256637 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 460465 Color: 1
Size: 282786 Color: 1
Size: 256750 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 460515 Color: 1
Size: 272369 Color: 0
Size: 267117 Color: 1

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 460890 Color: 1
Size: 288054 Color: 1
Size: 251057 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 460921 Color: 1
Size: 279993 Color: 1
Size: 259087 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 460996 Color: 1
Size: 280438 Color: 1
Size: 258567 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 461056 Color: 1
Size: 283361 Color: 1
Size: 255584 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 461130 Color: 1
Size: 285649 Color: 1
Size: 253222 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 461180 Color: 1
Size: 275049 Color: 1
Size: 263772 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 461238 Color: 1
Size: 276932 Color: 1
Size: 261831 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 461296 Color: 1
Size: 273719 Color: 1
Size: 264986 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 461349 Color: 1
Size: 274575 Color: 1
Size: 264077 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 461349 Color: 1
Size: 283425 Color: 1
Size: 255227 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 461515 Color: 1
Size: 284910 Color: 1
Size: 253576 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 461597 Color: 1
Size: 285903 Color: 1
Size: 252501 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 461645 Color: 1
Size: 284016 Color: 1
Size: 254340 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 457439 Color: 1
Size: 289735 Color: 1
Size: 252827 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 461831 Color: 1
Size: 287866 Color: 1
Size: 250304 Color: 0

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 461880 Color: 1
Size: 287600 Color: 1
Size: 250521 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 401578 Color: 1
Size: 334407 Color: 1
Size: 264016 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 461938 Color: 1
Size: 282695 Color: 1
Size: 255368 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 461970 Color: 1
Size: 271792 Color: 0
Size: 266239 Color: 1

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 462155 Color: 1
Size: 276035 Color: 0
Size: 261811 Color: 1

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 462355 Color: 1
Size: 275315 Color: 1
Size: 262331 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 462358 Color: 1
Size: 280233 Color: 1
Size: 257410 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 462383 Color: 1
Size: 285351 Color: 1
Size: 252267 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 462409 Color: 1
Size: 275788 Color: 1
Size: 261804 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 462419 Color: 1
Size: 283136 Color: 1
Size: 254446 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 462462 Color: 1
Size: 281008 Color: 1
Size: 256531 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 462492 Color: 1
Size: 282496 Color: 1
Size: 255013 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 462637 Color: 1
Size: 283087 Color: 1
Size: 254277 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 462711 Color: 1
Size: 278626 Color: 1
Size: 258664 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 462718 Color: 1
Size: 286601 Color: 1
Size: 250682 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 462910 Color: 1
Size: 283279 Color: 1
Size: 253812 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 462995 Color: 1
Size: 279646 Color: 1
Size: 257360 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 463093 Color: 1
Size: 282552 Color: 1
Size: 254356 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 463232 Color: 1
Size: 286191 Color: 1
Size: 250578 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 463330 Color: 1
Size: 284984 Color: 1
Size: 251687 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 463442 Color: 1
Size: 277951 Color: 1
Size: 258608 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 463482 Color: 1
Size: 281988 Color: 1
Size: 254531 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 463514 Color: 1
Size: 284696 Color: 1
Size: 251791 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 463553 Color: 1
Size: 277925 Color: 1
Size: 258523 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 463559 Color: 1
Size: 275820 Color: 1
Size: 260622 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 463619 Color: 1
Size: 275552 Color: 1
Size: 260830 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 463701 Color: 1
Size: 277249 Color: 1
Size: 259051 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 463905 Color: 1
Size: 283944 Color: 1
Size: 252152 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 463952 Color: 1
Size: 269457 Color: 1
Size: 266592 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 464150 Color: 1
Size: 274561 Color: 1
Size: 261290 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 464225 Color: 1
Size: 284192 Color: 1
Size: 251584 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 464361 Color: 1
Size: 283524 Color: 1
Size: 252116 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 464399 Color: 1
Size: 282239 Color: 1
Size: 253363 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 464442 Color: 1
Size: 281147 Color: 1
Size: 254412 Color: 0

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 464503 Color: 1
Size: 275845 Color: 1
Size: 259653 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 464610 Color: 1
Size: 278055 Color: 1
Size: 257336 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 465584 Color: 1
Size: 282625 Color: 1
Size: 251792 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 465598 Color: 1
Size: 279077 Color: 1
Size: 255326 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 465653 Color: 1
Size: 277326 Color: 1
Size: 257022 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 465895 Color: 1
Size: 273846 Color: 1
Size: 260260 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 465969 Color: 1
Size: 269084 Color: 0
Size: 264948 Color: 1

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 465976 Color: 1
Size: 278115 Color: 1
Size: 255910 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 466239 Color: 1
Size: 282096 Color: 1
Size: 251666 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 466264 Color: 1
Size: 274779 Color: 1
Size: 258958 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 466348 Color: 1
Size: 277774 Color: 1
Size: 255879 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 466577 Color: 1
Size: 274800 Color: 1
Size: 258624 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 466662 Color: 1
Size: 280909 Color: 1
Size: 252430 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 466784 Color: 1
Size: 279627 Color: 1
Size: 253590 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 466916 Color: 1
Size: 274610 Color: 1
Size: 258475 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 466954 Color: 1
Size: 282431 Color: 1
Size: 250616 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 467173 Color: 1
Size: 282363 Color: 1
Size: 250465 Color: 0

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 467430 Color: 1
Size: 275315 Color: 1
Size: 257256 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 467499 Color: 1
Size: 269102 Color: 1
Size: 263400 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 467988 Color: 1
Size: 277228 Color: 1
Size: 254785 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 468118 Color: 1
Size: 268910 Color: 1
Size: 262973 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 468231 Color: 1
Size: 273686 Color: 1
Size: 258084 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 468238 Color: 1
Size: 267001 Color: 1
Size: 264762 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 468349 Color: 1
Size: 281454 Color: 1
Size: 250198 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 468494 Color: 1
Size: 266723 Color: 1
Size: 264784 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 468541 Color: 1
Size: 275139 Color: 1
Size: 256321 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 468805 Color: 1
Size: 281149 Color: 1
Size: 250047 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 468911 Color: 1
Size: 276651 Color: 1
Size: 254439 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 469068 Color: 1
Size: 280077 Color: 1
Size: 250856 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 469322 Color: 1
Size: 277941 Color: 1
Size: 252738 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 469339 Color: 1
Size: 270754 Color: 1
Size: 259908 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 469398 Color: 1
Size: 269282 Color: 1
Size: 261321 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 469398 Color: 1
Size: 276338 Color: 1
Size: 254265 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 469406 Color: 1
Size: 276184 Color: 1
Size: 254411 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 469481 Color: 1
Size: 277221 Color: 1
Size: 253299 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 469675 Color: 1
Size: 277547 Color: 1
Size: 252779 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 469735 Color: 1
Size: 276125 Color: 1
Size: 254141 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 469795 Color: 1
Size: 275821 Color: 1
Size: 254385 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 469854 Color: 1
Size: 279397 Color: 1
Size: 250750 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 469870 Color: 1
Size: 279547 Color: 1
Size: 250584 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 469894 Color: 1
Size: 275930 Color: 1
Size: 254177 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 469905 Color: 1
Size: 276374 Color: 1
Size: 253722 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 469966 Color: 1
Size: 279993 Color: 1
Size: 250042 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 469975 Color: 1
Size: 274956 Color: 1
Size: 255070 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 470072 Color: 1
Size: 274430 Color: 1
Size: 255499 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 470092 Color: 1
Size: 270984 Color: 1
Size: 258925 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 470130 Color: 1
Size: 277920 Color: 1
Size: 251951 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 470149 Color: 1
Size: 266844 Color: 1
Size: 263008 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 470251 Color: 1
Size: 271138 Color: 1
Size: 258612 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 470271 Color: 1
Size: 276401 Color: 1
Size: 253329 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 470429 Color: 1
Size: 275452 Color: 1
Size: 254120 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 470453 Color: 1
Size: 272877 Color: 1
Size: 256671 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 470487 Color: 1
Size: 269007 Color: 1
Size: 260507 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 470505 Color: 1
Size: 266268 Color: 1
Size: 263228 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 470690 Color: 1
Size: 272224 Color: 1
Size: 257087 Color: 0

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 470836 Color: 1
Size: 267526 Color: 1
Size: 261639 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 471097 Color: 1
Size: 268373 Color: 1
Size: 260531 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 471112 Color: 1
Size: 270480 Color: 1
Size: 258409 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 471153 Color: 1
Size: 274826 Color: 1
Size: 254022 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 471548 Color: 1
Size: 275496 Color: 1
Size: 252957 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 471971 Color: 1
Size: 271050 Color: 1
Size: 256980 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 472012 Color: 1
Size: 265576 Color: 1
Size: 262413 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 472071 Color: 1
Size: 273323 Color: 1
Size: 254607 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 472271 Color: 1
Size: 268030 Color: 0
Size: 259700 Color: 1

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 472327 Color: 1
Size: 270753 Color: 1
Size: 256921 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 472383 Color: 1
Size: 272574 Color: 1
Size: 255044 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 472582 Color: 1
Size: 272058 Color: 1
Size: 255361 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 472651 Color: 1
Size: 269739 Color: 1
Size: 257611 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 472697 Color: 1
Size: 266775 Color: 1
Size: 260529 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 472733 Color: 1
Size: 276938 Color: 1
Size: 250330 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 472795 Color: 1
Size: 270270 Color: 1
Size: 256936 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 472913 Color: 1
Size: 269867 Color: 1
Size: 257221 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 472934 Color: 1
Size: 269442 Color: 1
Size: 257625 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 473019 Color: 1
Size: 269916 Color: 1
Size: 257066 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 473049 Color: 1
Size: 275841 Color: 1
Size: 251111 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 473123 Color: 1
Size: 267194 Color: 1
Size: 259684 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 473288 Color: 1
Size: 269360 Color: 0
Size: 257353 Color: 1

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 473464 Color: 1
Size: 263901 Color: 1
Size: 262636 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 473640 Color: 1
Size: 275810 Color: 1
Size: 250551 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 473691 Color: 1
Size: 276078 Color: 1
Size: 250232 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 473854 Color: 1
Size: 270147 Color: 1
Size: 256000 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 474008 Color: 1
Size: 275686 Color: 1
Size: 250307 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 474081 Color: 1
Size: 274478 Color: 1
Size: 251442 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 474259 Color: 1
Size: 270390 Color: 1
Size: 255352 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 474356 Color: 1
Size: 272699 Color: 1
Size: 252946 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 474388 Color: 1
Size: 269306 Color: 1
Size: 256307 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 474466 Color: 1
Size: 274785 Color: 1
Size: 250750 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 474556 Color: 1
Size: 266107 Color: 1
Size: 259338 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 474632 Color: 1
Size: 265035 Color: 1
Size: 260334 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 474990 Color: 1
Size: 263637 Color: 0
Size: 261374 Color: 1

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 475082 Color: 1
Size: 273378 Color: 1
Size: 251541 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 475268 Color: 1
Size: 262649 Color: 0
Size: 262084 Color: 1

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 475417 Color: 1
Size: 267538 Color: 1
Size: 257046 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 475595 Color: 1
Size: 273774 Color: 1
Size: 250632 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 475622 Color: 1
Size: 272928 Color: 1
Size: 251451 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 475734 Color: 1
Size: 267852 Color: 1
Size: 256415 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 475772 Color: 1
Size: 268596 Color: 1
Size: 255633 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 475998 Color: 1
Size: 272420 Color: 1
Size: 251583 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 476011 Color: 1
Size: 268072 Color: 1
Size: 255918 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 476018 Color: 1
Size: 263029 Color: 1
Size: 260954 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 476545 Color: 1
Size: 267672 Color: 1
Size: 255784 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 476561 Color: 1
Size: 264570 Color: 1
Size: 258870 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 476624 Color: 1
Size: 272603 Color: 1
Size: 250774 Color: 0

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 476820 Color: 1
Size: 268995 Color: 1
Size: 254186 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 476910 Color: 1
Size: 270051 Color: 1
Size: 253040 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 476939 Color: 1
Size: 268929 Color: 1
Size: 254133 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 476972 Color: 1
Size: 270879 Color: 1
Size: 252150 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 477128 Color: 1
Size: 269976 Color: 1
Size: 252897 Color: 0

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 477222 Color: 1
Size: 268963 Color: 1
Size: 253816 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 477350 Color: 1
Size: 266672 Color: 1
Size: 255979 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 477411 Color: 1
Size: 263100 Color: 0
Size: 259490 Color: 1

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 477428 Color: 1
Size: 265126 Color: 1
Size: 257447 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 477608 Color: 1
Size: 263121 Color: 1
Size: 259272 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 475130 Color: 1
Size: 270966 Color: 1
Size: 253905 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 477983 Color: 1
Size: 264761 Color: 1
Size: 257257 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 478001 Color: 1
Size: 269961 Color: 1
Size: 252039 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 478135 Color: 1
Size: 270022 Color: 1
Size: 251844 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 478199 Color: 1
Size: 265609 Color: 1
Size: 256193 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 478202 Color: 1
Size: 265940 Color: 1
Size: 255859 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 478319 Color: 1
Size: 268080 Color: 1
Size: 253602 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 478533 Color: 1
Size: 271124 Color: 1
Size: 250344 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 478684 Color: 1
Size: 266321 Color: 1
Size: 254996 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 478831 Color: 1
Size: 263144 Color: 1
Size: 258026 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 478990 Color: 1
Size: 266883 Color: 1
Size: 254128 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 479239 Color: 1
Size: 260652 Color: 0
Size: 260110 Color: 1

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 479252 Color: 1
Size: 269554 Color: 1
Size: 251195 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 479367 Color: 1
Size: 260712 Color: 0
Size: 259922 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 479401 Color: 1
Size: 260857 Color: 0
Size: 259743 Color: 1

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 479404 Color: 1
Size: 263653 Color: 1
Size: 256944 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 479586 Color: 1
Size: 267723 Color: 1
Size: 252692 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 479591 Color: 1
Size: 268563 Color: 1
Size: 251847 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 479798 Color: 1
Size: 264275 Color: 1
Size: 255928 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 479866 Color: 1
Size: 267314 Color: 1
Size: 252821 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 480104 Color: 1
Size: 263278 Color: 1
Size: 256619 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 480219 Color: 1
Size: 264473 Color: 1
Size: 255309 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 480544 Color: 1
Size: 261295 Color: 0
Size: 258162 Color: 1

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 480766 Color: 1
Size: 260427 Color: 1
Size: 258808 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 480779 Color: 1
Size: 263825 Color: 1
Size: 255397 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 480785 Color: 1
Size: 262566 Color: 1
Size: 256650 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 481012 Color: 1
Size: 265070 Color: 1
Size: 253919 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 481034 Color: 1
Size: 263598 Color: 1
Size: 255369 Color: 0

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 481157 Color: 1
Size: 267549 Color: 1
Size: 251295 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 481270 Color: 1
Size: 262126 Color: 0
Size: 256605 Color: 1

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 481358 Color: 1
Size: 267189 Color: 1
Size: 251454 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 481369 Color: 1
Size: 260514 Color: 1
Size: 258118 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 481509 Color: 1
Size: 265909 Color: 1
Size: 252583 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 481526 Color: 1
Size: 259803 Color: 1
Size: 258672 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 481728 Color: 1
Size: 260768 Color: 1
Size: 257505 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 481759 Color: 1
Size: 261649 Color: 1
Size: 256593 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 481780 Color: 1
Size: 261979 Color: 0
Size: 256242 Color: 1

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 481880 Color: 1
Size: 261615 Color: 1
Size: 256506 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 481915 Color: 1
Size: 259145 Color: 0
Size: 258941 Color: 1

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 481978 Color: 1
Size: 265033 Color: 1
Size: 252990 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 482428 Color: 1
Size: 263065 Color: 1
Size: 254508 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 482433 Color: 1
Size: 261859 Color: 0
Size: 255709 Color: 1

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 482512 Color: 1
Size: 262290 Color: 1
Size: 255199 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 482791 Color: 1
Size: 261395 Color: 0
Size: 255815 Color: 1

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 482806 Color: 1
Size: 262152 Color: 1
Size: 255043 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 482949 Color: 1
Size: 264403 Color: 1
Size: 252649 Color: 0

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 482973 Color: 1
Size: 266356 Color: 1
Size: 250672 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 482995 Color: 1
Size: 265713 Color: 1
Size: 251293 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 483021 Color: 1
Size: 266970 Color: 1
Size: 250010 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 483036 Color: 1
Size: 264373 Color: 1
Size: 252592 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 483123 Color: 1
Size: 259783 Color: 0
Size: 257095 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 483306 Color: 1
Size: 265797 Color: 1
Size: 250898 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 483449 Color: 1
Size: 264987 Color: 1
Size: 251565 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 483475 Color: 1
Size: 263586 Color: 1
Size: 252940 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 483499 Color: 1
Size: 261221 Color: 1
Size: 255281 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 483515 Color: 1
Size: 261616 Color: 1
Size: 254870 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 483558 Color: 1
Size: 265416 Color: 1
Size: 251027 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 483632 Color: 1
Size: 265564 Color: 1
Size: 250805 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 483770 Color: 1
Size: 263892 Color: 1
Size: 252339 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 483823 Color: 1
Size: 263768 Color: 1
Size: 252410 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 484252 Color: 1
Size: 259238 Color: 1
Size: 256511 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 484264 Color: 1
Size: 258274 Color: 1
Size: 257463 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 484337 Color: 1
Size: 265276 Color: 1
Size: 250388 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 484399 Color: 1
Size: 262524 Color: 1
Size: 253078 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 484407 Color: 1
Size: 262726 Color: 1
Size: 252868 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 484573 Color: 1
Size: 263220 Color: 1
Size: 252208 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 484576 Color: 1
Size: 261866 Color: 1
Size: 253559 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 484581 Color: 1
Size: 263626 Color: 1
Size: 251794 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 485173 Color: 1
Size: 262844 Color: 1
Size: 251984 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 485425 Color: 1
Size: 263252 Color: 1
Size: 251324 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 485478 Color: 1
Size: 262468 Color: 1
Size: 252055 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 485526 Color: 1
Size: 257925 Color: 0
Size: 256550 Color: 1

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 485608 Color: 1
Size: 257755 Color: 1
Size: 256638 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 485624 Color: 1
Size: 259930 Color: 1
Size: 254447 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 485664 Color: 1
Size: 261574 Color: 1
Size: 252763 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 485853 Color: 1
Size: 262081 Color: 1
Size: 252067 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 485863 Color: 1
Size: 258686 Color: 1
Size: 255452 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 485921 Color: 1
Size: 262358 Color: 1
Size: 251722 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 485948 Color: 1
Size: 258523 Color: 1
Size: 255530 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 485970 Color: 1
Size: 259236 Color: 1
Size: 254795 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 486334 Color: 1
Size: 262393 Color: 1
Size: 251274 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 486406 Color: 1
Size: 262044 Color: 1
Size: 251551 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 486431 Color: 1
Size: 261589 Color: 1
Size: 251981 Color: 0

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 486539 Color: 1
Size: 258412 Color: 1
Size: 255050 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 486648 Color: 1
Size: 260346 Color: 1
Size: 253007 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 486712 Color: 1
Size: 263010 Color: 1
Size: 250279 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 487041 Color: 1
Size: 257924 Color: 1
Size: 255036 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 487142 Color: 1
Size: 257296 Color: 1
Size: 255563 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 487260 Color: 1
Size: 261430 Color: 1
Size: 251311 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 487273 Color: 1
Size: 262240 Color: 1
Size: 250488 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 487312 Color: 1
Size: 259813 Color: 1
Size: 252876 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 487376 Color: 1
Size: 259728 Color: 1
Size: 252897 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 487422 Color: 1
Size: 260113 Color: 1
Size: 252466 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 487540 Color: 1
Size: 262087 Color: 1
Size: 250374 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 487742 Color: 1
Size: 257448 Color: 1
Size: 254811 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 487775 Color: 1
Size: 261556 Color: 1
Size: 250670 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 487797 Color: 1
Size: 259949 Color: 1
Size: 252255 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 487847 Color: 1
Size: 256936 Color: 1
Size: 255218 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 488039 Color: 1
Size: 258813 Color: 1
Size: 253149 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 488101 Color: 1
Size: 261789 Color: 1
Size: 250111 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 488191 Color: 1
Size: 259044 Color: 1
Size: 252766 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 488412 Color: 1
Size: 259322 Color: 1
Size: 252267 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 488528 Color: 1
Size: 260600 Color: 1
Size: 250873 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 488593 Color: 1
Size: 256361 Color: 0
Size: 255047 Color: 1

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 488597 Color: 1
Size: 259019 Color: 1
Size: 252385 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 488654 Color: 1
Size: 259039 Color: 1
Size: 252308 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 488657 Color: 1
Size: 259411 Color: 1
Size: 251933 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 488807 Color: 1
Size: 260218 Color: 1
Size: 250976 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 488834 Color: 1
Size: 258380 Color: 1
Size: 252787 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 488846 Color: 1
Size: 260274 Color: 1
Size: 250881 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 488911 Color: 1
Size: 258741 Color: 1
Size: 252349 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 488917 Color: 1
Size: 256683 Color: 1
Size: 254401 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 489246 Color: 1
Size: 256298 Color: 1
Size: 254457 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 489270 Color: 1
Size: 259561 Color: 1
Size: 251170 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 489367 Color: 1
Size: 259948 Color: 1
Size: 250686 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 489527 Color: 1
Size: 259088 Color: 0
Size: 251386 Color: 1

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 489574 Color: 1
Size: 258572 Color: 1
Size: 251855 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 489644 Color: 1
Size: 255844 Color: 1
Size: 254513 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 489956 Color: 1
Size: 257189 Color: 1
Size: 252856 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 490567 Color: 1
Size: 256535 Color: 1
Size: 252899 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 490634 Color: 1
Size: 255609 Color: 1
Size: 253758 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 490717 Color: 1
Size: 258745 Color: 1
Size: 250539 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 490937 Color: 1
Size: 256125 Color: 1
Size: 252939 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 490942 Color: 1
Size: 258235 Color: 1
Size: 250824 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 491008 Color: 1
Size: 258977 Color: 1
Size: 250016 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 491064 Color: 1
Size: 254595 Color: 0
Size: 254342 Color: 1

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 491229 Color: 1
Size: 255119 Color: 1
Size: 253653 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 491292 Color: 1
Size: 256382 Color: 1
Size: 252327 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 491415 Color: 1
Size: 255007 Color: 1
Size: 253579 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 491763 Color: 1
Size: 257431 Color: 1
Size: 250807 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 491938 Color: 1
Size: 257002 Color: 1
Size: 251061 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 491987 Color: 1
Size: 257900 Color: 1
Size: 250114 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 492146 Color: 1
Size: 254464 Color: 0
Size: 253391 Color: 1

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 492154 Color: 1
Size: 257535 Color: 1
Size: 250312 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 492198 Color: 1
Size: 256792 Color: 1
Size: 251011 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 492218 Color: 1
Size: 254828 Color: 0
Size: 252955 Color: 1

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 492418 Color: 1
Size: 256842 Color: 1
Size: 250741 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 492524 Color: 1
Size: 255092 Color: 1
Size: 252385 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 492565 Color: 1
Size: 256108 Color: 1
Size: 251328 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 492656 Color: 1
Size: 257273 Color: 1
Size: 250072 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 492692 Color: 1
Size: 254143 Color: 1
Size: 253166 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 492712 Color: 1
Size: 256587 Color: 1
Size: 250702 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 492774 Color: 1
Size: 254017 Color: 1
Size: 253210 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 492777 Color: 1
Size: 254681 Color: 1
Size: 252543 Color: 0

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 492874 Color: 1
Size: 257049 Color: 1
Size: 250078 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 492919 Color: 1
Size: 255578 Color: 1
Size: 251504 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 493061 Color: 1
Size: 253923 Color: 1
Size: 253017 Color: 0

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 493070 Color: 1
Size: 255494 Color: 1
Size: 251437 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 493074 Color: 1
Size: 253544 Color: 1
Size: 253383 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 493150 Color: 1
Size: 255603 Color: 1
Size: 251248 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 493353 Color: 1
Size: 253694 Color: 1
Size: 252954 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 493450 Color: 1
Size: 253925 Color: 0
Size: 252626 Color: 1

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 493779 Color: 1
Size: 253924 Color: 1
Size: 252298 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 493817 Color: 1
Size: 254081 Color: 1
Size: 252103 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 493841 Color: 1
Size: 255382 Color: 1
Size: 250778 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 493925 Color: 1
Size: 254001 Color: 1
Size: 252075 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 493932 Color: 1
Size: 255588 Color: 1
Size: 250481 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 494187 Color: 1
Size: 255695 Color: 1
Size: 250119 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 494454 Color: 1
Size: 252776 Color: 0
Size: 252771 Color: 1

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 494553 Color: 1
Size: 254075 Color: 1
Size: 251373 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 494553 Color: 1
Size: 253772 Color: 1
Size: 251676 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 494607 Color: 1
Size: 254524 Color: 1
Size: 250870 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 494785 Color: 1
Size: 254960 Color: 1
Size: 250256 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 494982 Color: 1
Size: 254294 Color: 1
Size: 250725 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 495037 Color: 1
Size: 253453 Color: 1
Size: 251511 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 495061 Color: 1
Size: 253072 Color: 1
Size: 251868 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 495217 Color: 1
Size: 253356 Color: 1
Size: 251428 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 495517 Color: 1
Size: 253298 Color: 1
Size: 251186 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 495700 Color: 1
Size: 253822 Color: 1
Size: 250479 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 495805 Color: 1
Size: 252322 Color: 1
Size: 251874 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 495821 Color: 1
Size: 252704 Color: 1
Size: 251476 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 495915 Color: 1
Size: 252488 Color: 1
Size: 251598 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 496330 Color: 1
Size: 253313 Color: 1
Size: 250358 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 496407 Color: 1
Size: 253214 Color: 1
Size: 250380 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 496448 Color: 1
Size: 253345 Color: 1
Size: 250208 Color: 0

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 496464 Color: 1
Size: 253485 Color: 1
Size: 250052 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 496631 Color: 1
Size: 252807 Color: 1
Size: 250563 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 496809 Color: 1
Size: 252581 Color: 1
Size: 250611 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 496840 Color: 1
Size: 251974 Color: 0
Size: 251187 Color: 1

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 496890 Color: 1
Size: 252496 Color: 1
Size: 250615 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 496908 Color: 1
Size: 252245 Color: 1
Size: 250848 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 496937 Color: 1
Size: 252637 Color: 1
Size: 250427 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 496956 Color: 1
Size: 252979 Color: 1
Size: 250066 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 496963 Color: 1
Size: 252813 Color: 1
Size: 250225 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 421293 Color: 1
Size: 327829 Color: 1
Size: 250879 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 497055 Color: 1
Size: 251974 Color: 1
Size: 250972 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 497058 Color: 1
Size: 252254 Color: 1
Size: 250689 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 497187 Color: 1
Size: 252503 Color: 1
Size: 250311 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 497292 Color: 1
Size: 251389 Color: 1
Size: 251320 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 497536 Color: 1
Size: 252090 Color: 1
Size: 250375 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 497893 Color: 1
Size: 252028 Color: 1
Size: 250080 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 498118 Color: 1
Size: 251177 Color: 1
Size: 250706 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 498136 Color: 1
Size: 251609 Color: 1
Size: 250256 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 498405 Color: 1
Size: 250840 Color: 1
Size: 250756 Color: 0

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 498533 Color: 1
Size: 251071 Color: 1
Size: 250397 Color: 0

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 498598 Color: 1
Size: 251206 Color: 1
Size: 250197 Color: 0

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 498786 Color: 1
Size: 250756 Color: 1
Size: 250459 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 499158 Color: 1
Size: 250786 Color: 1
Size: 250057 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 499171 Color: 1
Size: 250760 Color: 1
Size: 250070 Color: 0

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 499173 Color: 1
Size: 250556 Color: 1
Size: 250272 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 499278 Color: 1
Size: 250572 Color: 1
Size: 250151 Color: 0

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 499446 Color: 1
Size: 250343 Color: 1
Size: 250212 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 499474 Color: 1
Size: 250468 Color: 1
Size: 250059 Color: 0

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 499505 Color: 1
Size: 250419 Color: 1
Size: 250077 Color: 0

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 499687 Color: 1
Size: 250217 Color: 1
Size: 250097 Color: 0

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 499697 Color: 1
Size: 250199 Color: 1
Size: 250105 Color: 0

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 499699 Color: 1
Size: 250240 Color: 1
Size: 250062 Color: 0

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 499809 Color: 1
Size: 250125 Color: 1
Size: 250067 Color: 0

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 362643 Color: 1
Size: 358017 Color: 1
Size: 279340 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 497117 Color: 1
Size: 252128 Color: 1
Size: 250755 Color: 0

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 408463 Color: 1
Size: 295895 Color: 1
Size: 295642 Color: 0

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 355483 Color: 1
Size: 348167 Color: 1
Size: 296350 Color: 0

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 390517 Color: 1
Size: 339075 Color: 1
Size: 270408 Color: 0

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 362072 Color: 1
Size: 325387 Color: 1
Size: 312541 Color: 0

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 356000 Color: 1
Size: 338292 Color: 1
Size: 305708 Color: 0

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 388487 Color: 1
Size: 333795 Color: 1
Size: 277718 Color: 0

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 354234 Color: 1
Size: 348933 Color: 1
Size: 296833 Color: 0

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 417001 Color: 1
Size: 292829 Color: 0
Size: 290170 Color: 1

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 358259 Color: 1
Size: 335549 Color: 1
Size: 306192 Color: 0

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 347286 Color: 1
Size: 346228 Color: 1
Size: 306486 Color: 0

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 355575 Color: 1
Size: 346941 Color: 1
Size: 297484 Color: 0

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 440711 Color: 1
Size: 285993 Color: 1
Size: 273296 Color: 0

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 393043 Color: 1
Size: 314432 Color: 1
Size: 292525 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 353294 Color: 1
Size: 347290 Color: 1
Size: 299416 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 398752 Color: 1
Size: 303087 Color: 1
Size: 298161 Color: 0

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 360057 Color: 1
Size: 344661 Color: 1
Size: 295282 Color: 0

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 364666 Color: 1
Size: 333647 Color: 1
Size: 301687 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 361846 Color: 1
Size: 358998 Color: 1
Size: 279156 Color: 0

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 368085 Color: 1
Size: 361809 Color: 1
Size: 270106 Color: 0

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 420690 Color: 1
Size: 318348 Color: 1
Size: 260962 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 456620 Color: 1
Size: 288601 Color: 1
Size: 254779 Color: 0

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 370298 Color: 1
Size: 331061 Color: 1
Size: 298641 Color: 0

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 409958 Color: 1
Size: 303846 Color: 0
Size: 286196 Color: 1

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 363371 Color: 1
Size: 353908 Color: 1
Size: 282721 Color: 0

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 443080 Color: 1
Size: 285063 Color: 0
Size: 271857 Color: 1

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 369872 Color: 1
Size: 360371 Color: 1
Size: 269757 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 365938 Color: 1
Size: 357625 Color: 1
Size: 276437 Color: 0

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 359408 Color: 1
Size: 356504 Color: 1
Size: 284088 Color: 0

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 377026 Color: 1
Size: 356835 Color: 1
Size: 266139 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 359752 Color: 1
Size: 357090 Color: 1
Size: 283158 Color: 0

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 429163 Color: 1
Size: 291654 Color: 1
Size: 279183 Color: 0

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 408022 Color: 1
Size: 326550 Color: 1
Size: 265428 Color: 0

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 403406 Color: 1
Size: 312850 Color: 0
Size: 283744 Color: 1

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 374853 Color: 1
Size: 324687 Color: 1
Size: 300460 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 403981 Color: 1
Size: 325991 Color: 0
Size: 270028 Color: 1

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 379719 Color: 1
Size: 348714 Color: 1
Size: 271567 Color: 0

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 355968 Color: 1
Size: 345555 Color: 1
Size: 298477 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 389206 Color: 1
Size: 309404 Color: 1
Size: 301390 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 401241 Color: 1
Size: 321647 Color: 0
Size: 277112 Color: 1

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 354332 Color: 1
Size: 348219 Color: 1
Size: 297449 Color: 0

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 346263 Color: 1
Size: 335364 Color: 1
Size: 318373 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 368662 Color: 1
Size: 364190 Color: 1
Size: 267148 Color: 0

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 460750 Color: 1
Size: 279411 Color: 0
Size: 259839 Color: 1

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 357351 Color: 1
Size: 334003 Color: 1
Size: 308646 Color: 0

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 409203 Color: 1
Size: 325670 Color: 1
Size: 265127 Color: 0

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 364514 Color: 1
Size: 332350 Color: 1
Size: 303136 Color: 0

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 421708 Color: 1
Size: 299140 Color: 1
Size: 279152 Color: 0

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 365372 Color: 1
Size: 322133 Color: 0
Size: 312495 Color: 1

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 386569 Color: 1
Size: 360350 Color: 1
Size: 253081 Color: 0

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 392792 Color: 1
Size: 303788 Color: 0
Size: 303420 Color: 1

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 377954 Color: 1
Size: 319233 Color: 1
Size: 302813 Color: 0

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 498516 Color: 1
Size: 250830 Color: 1
Size: 250654 Color: 0

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 349618 Color: 1
Size: 346447 Color: 1
Size: 303935 Color: 0

Bin 3022: 1 of cap free
Amount of items: 3
Items: 
Size: 374253 Color: 1
Size: 319684 Color: 1
Size: 306063 Color: 0

Bin 3023: 1 of cap free
Amount of items: 3
Items: 
Size: 357115 Color: 1
Size: 341395 Color: 1
Size: 301490 Color: 0

Bin 3024: 1 of cap free
Amount of items: 3
Items: 
Size: 360593 Color: 1
Size: 342174 Color: 1
Size: 297233 Color: 0

Bin 3025: 1 of cap free
Amount of items: 3
Items: 
Size: 364462 Color: 1
Size: 351177 Color: 1
Size: 284361 Color: 0

Bin 3026: 1 of cap free
Amount of items: 3
Items: 
Size: 361110 Color: 1
Size: 341082 Color: 1
Size: 297808 Color: 0

Bin 3027: 1 of cap free
Amount of items: 3
Items: 
Size: 361452 Color: 1
Size: 341260 Color: 1
Size: 297288 Color: 0

Bin 3028: 1 of cap free
Amount of items: 3
Items: 
Size: 361985 Color: 1
Size: 320378 Color: 1
Size: 317637 Color: 0

Bin 3029: 1 of cap free
Amount of items: 3
Items: 
Size: 362411 Color: 1
Size: 351732 Color: 1
Size: 285857 Color: 0

Bin 3030: 1 of cap free
Amount of items: 3
Items: 
Size: 363282 Color: 1
Size: 334879 Color: 1
Size: 301839 Color: 0

Bin 3031: 1 of cap free
Amount of items: 3
Items: 
Size: 364158 Color: 1
Size: 324363 Color: 1
Size: 311479 Color: 0

Bin 3032: 1 of cap free
Amount of items: 3
Items: 
Size: 365427 Color: 1
Size: 323843 Color: 1
Size: 310730 Color: 0

Bin 3033: 1 of cap free
Amount of items: 3
Items: 
Size: 365443 Color: 1
Size: 350214 Color: 1
Size: 284343 Color: 0

Bin 3034: 1 of cap free
Amount of items: 3
Items: 
Size: 498024 Color: 1
Size: 251691 Color: 1
Size: 250285 Color: 0

Bin 3035: 1 of cap free
Amount of items: 3
Items: 
Size: 359263 Color: 1
Size: 331167 Color: 0
Size: 309570 Color: 1

Bin 3036: 1 of cap free
Amount of items: 3
Items: 
Size: 383825 Color: 1
Size: 357779 Color: 1
Size: 258396 Color: 0

Bin 3037: 1 of cap free
Amount of items: 3
Items: 
Size: 354988 Color: 1
Size: 343692 Color: 1
Size: 301320 Color: 0

Bin 3038: 1 of cap free
Amount of items: 3
Items: 
Size: 354954 Color: 1
Size: 353941 Color: 1
Size: 291105 Color: 0

Bin 3039: 1 of cap free
Amount of items: 3
Items: 
Size: 410517 Color: 1
Size: 316440 Color: 0
Size: 273043 Color: 1

Bin 3040: 1 of cap free
Amount of items: 3
Items: 
Size: 352860 Color: 1
Size: 333013 Color: 1
Size: 314127 Color: 0

Bin 3041: 1 of cap free
Amount of items: 3
Items: 
Size: 390260 Color: 1
Size: 349035 Color: 1
Size: 260705 Color: 0

Bin 3042: 1 of cap free
Amount of items: 3
Items: 
Size: 407323 Color: 1
Size: 322196 Color: 1
Size: 270481 Color: 0

Bin 3043: 1 of cap free
Amount of items: 3
Items: 
Size: 425470 Color: 1
Size: 309952 Color: 0
Size: 264578 Color: 1

Bin 3044: 1 of cap free
Amount of items: 3
Items: 
Size: 353941 Color: 1
Size: 350120 Color: 1
Size: 295939 Color: 0

Bin 3045: 1 of cap free
Amount of items: 3
Items: 
Size: 348139 Color: 1
Size: 338608 Color: 1
Size: 313253 Color: 0

Bin 3046: 1 of cap free
Amount of items: 3
Items: 
Size: 353426 Color: 1
Size: 347735 Color: 1
Size: 298839 Color: 0

Bin 3047: 1 of cap free
Amount of items: 3
Items: 
Size: 437751 Color: 1
Size: 305505 Color: 1
Size: 256744 Color: 0

Bin 3048: 1 of cap free
Amount of items: 3
Items: 
Size: 383934 Color: 1
Size: 345128 Color: 1
Size: 270938 Color: 0

Bin 3049: 1 of cap free
Amount of items: 3
Items: 
Size: 487668 Color: 1
Size: 261381 Color: 1
Size: 250951 Color: 0

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 394101 Color: 1
Size: 345112 Color: 1
Size: 260786 Color: 0

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 394051 Color: 1
Size: 354782 Color: 1
Size: 251166 Color: 0

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 461611 Color: 1
Size: 272789 Color: 0
Size: 265599 Color: 1

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 386949 Color: 1
Size: 344075 Color: 1
Size: 268975 Color: 0

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 390867 Color: 1
Size: 355982 Color: 1
Size: 253150 Color: 0

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 346899 Color: 1
Size: 345721 Color: 1
Size: 307379 Color: 0

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 360074 Color: 1
Size: 331132 Color: 1
Size: 308793 Color: 0

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 354489 Color: 1
Size: 350141 Color: 1
Size: 295369 Color: 0

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 363143 Color: 1
Size: 343042 Color: 1
Size: 293814 Color: 0

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 438990 Color: 1
Size: 300140 Color: 1
Size: 260869 Color: 0

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 368471 Color: 1
Size: 360853 Color: 1
Size: 270675 Color: 0

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 354600 Color: 1
Size: 337171 Color: 1
Size: 308228 Color: 0

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 428979 Color: 1
Size: 297878 Color: 1
Size: 273142 Color: 0

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 374008 Color: 1
Size: 321530 Color: 1
Size: 304461 Color: 0

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 378043 Color: 1
Size: 315742 Color: 0
Size: 306214 Color: 1

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 405099 Color: 1
Size: 331896 Color: 1
Size: 263004 Color: 0

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 499390 Color: 1
Size: 250512 Color: 1
Size: 250097 Color: 0

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 392457 Color: 1
Size: 354060 Color: 1
Size: 253482 Color: 0

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 360867 Color: 1
Size: 351081 Color: 1
Size: 288051 Color: 0

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 355847 Color: 1
Size: 354097 Color: 1
Size: 290055 Color: 0

Bin 3070: 2 of cap free
Amount of items: 3
Items: 
Size: 349864 Color: 1
Size: 349047 Color: 1
Size: 301088 Color: 0

Bin 3071: 2 of cap free
Amount of items: 3
Items: 
Size: 362591 Color: 1
Size: 330096 Color: 1
Size: 307312 Color: 0

Bin 3072: 2 of cap free
Amount of items: 3
Items: 
Size: 343762 Color: 1
Size: 338060 Color: 1
Size: 318177 Color: 0

Bin 3073: 2 of cap free
Amount of items: 3
Items: 
Size: 359578 Color: 1
Size: 336283 Color: 1
Size: 304138 Color: 0

Bin 3074: 2 of cap free
Amount of items: 3
Items: 
Size: 351005 Color: 1
Size: 347940 Color: 1
Size: 301054 Color: 0

Bin 3075: 2 of cap free
Amount of items: 3
Items: 
Size: 377618 Color: 1
Size: 360440 Color: 1
Size: 261941 Color: 0

Bin 3076: 2 of cap free
Amount of items: 3
Items: 
Size: 356521 Color: 1
Size: 350811 Color: 1
Size: 292667 Color: 0

Bin 3077: 2 of cap free
Amount of items: 3
Items: 
Size: 366727 Color: 1
Size: 362633 Color: 1
Size: 270639 Color: 0

Bin 3078: 2 of cap free
Amount of items: 3
Items: 
Size: 359722 Color: 1
Size: 346282 Color: 1
Size: 293995 Color: 0

Bin 3079: 2 of cap free
Amount of items: 3
Items: 
Size: 351668 Color: 1
Size: 341161 Color: 1
Size: 307170 Color: 0

Bin 3080: 2 of cap free
Amount of items: 3
Items: 
Size: 356907 Color: 1
Size: 346952 Color: 1
Size: 296140 Color: 0

Bin 3081: 2 of cap free
Amount of items: 3
Items: 
Size: 399068 Color: 1
Size: 318532 Color: 0
Size: 282399 Color: 1

Bin 3082: 2 of cap free
Amount of items: 3
Items: 
Size: 413570 Color: 1
Size: 319031 Color: 1
Size: 267398 Color: 0

Bin 3083: 2 of cap free
Amount of items: 3
Items: 
Size: 388914 Color: 1
Size: 306111 Color: 0
Size: 304974 Color: 1

Bin 3084: 2 of cap free
Amount of items: 3
Items: 
Size: 391310 Color: 1
Size: 324100 Color: 1
Size: 284589 Color: 0

Bin 3085: 2 of cap free
Amount of items: 3
Items: 
Size: 364141 Color: 1
Size: 332606 Color: 1
Size: 303252 Color: 0

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 361134 Color: 1
Size: 320033 Color: 0
Size: 318831 Color: 1

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 375149 Color: 1
Size: 358695 Color: 1
Size: 266154 Color: 0

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 405231 Color: 1
Size: 332213 Color: 1
Size: 262554 Color: 0

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 375539 Color: 1
Size: 360966 Color: 1
Size: 263493 Color: 0

Bin 3090: 3 of cap free
Amount of items: 3
Items: 
Size: 359813 Color: 1
Size: 334871 Color: 1
Size: 305314 Color: 0

Bin 3091: 3 of cap free
Amount of items: 3
Items: 
Size: 392709 Color: 1
Size: 314490 Color: 1
Size: 292799 Color: 0

Bin 3092: 3 of cap free
Amount of items: 3
Items: 
Size: 347868 Color: 1
Size: 347595 Color: 1
Size: 304535 Color: 0

Bin 3093: 3 of cap free
Amount of items: 3
Items: 
Size: 412136 Color: 1
Size: 334896 Color: 1
Size: 252966 Color: 0

Bin 3094: 3 of cap free
Amount of items: 3
Items: 
Size: 447915 Color: 1
Size: 301307 Color: 1
Size: 250776 Color: 0

Bin 3095: 3 of cap free
Amount of items: 3
Items: 
Size: 370711 Color: 1
Size: 322829 Color: 1
Size: 306458 Color: 0

Bin 3096: 3 of cap free
Amount of items: 3
Items: 
Size: 413989 Color: 1
Size: 313810 Color: 1
Size: 272199 Color: 0

Bin 3097: 3 of cap free
Amount of items: 3
Items: 
Size: 432091 Color: 1
Size: 310159 Color: 1
Size: 257748 Color: 0

Bin 3098: 3 of cap free
Amount of items: 3
Items: 
Size: 399265 Color: 1
Size: 331307 Color: 1
Size: 269426 Color: 0

Bin 3099: 3 of cap free
Amount of items: 3
Items: 
Size: 410242 Color: 1
Size: 311193 Color: 0
Size: 278563 Color: 1

Bin 3100: 3 of cap free
Amount of items: 3
Items: 
Size: 390900 Color: 1
Size: 318300 Color: 1
Size: 290798 Color: 0

Bin 3101: 3 of cap free
Amount of items: 3
Items: 
Size: 353016 Color: 1
Size: 345877 Color: 1
Size: 301105 Color: 0

Bin 3102: 3 of cap free
Amount of items: 3
Items: 
Size: 354560 Color: 1
Size: 342946 Color: 1
Size: 302492 Color: 0

Bin 3103: 3 of cap free
Amount of items: 3
Items: 
Size: 342899 Color: 1
Size: 334052 Color: 1
Size: 323047 Color: 0

Bin 3104: 3 of cap free
Amount of items: 3
Items: 
Size: 390421 Color: 1
Size: 346483 Color: 1
Size: 263094 Color: 0

Bin 3105: 3 of cap free
Amount of items: 3
Items: 
Size: 368239 Color: 1
Size: 334006 Color: 1
Size: 297753 Color: 0

Bin 3106: 3 of cap free
Amount of items: 3
Items: 
Size: 361665 Color: 1
Size: 359661 Color: 1
Size: 278672 Color: 0

Bin 3107: 3 of cap free
Amount of items: 3
Items: 
Size: 357008 Color: 1
Size: 352069 Color: 1
Size: 290921 Color: 0

Bin 3108: 3 of cap free
Amount of items: 3
Items: 
Size: 373522 Color: 1
Size: 360036 Color: 1
Size: 266440 Color: 0

Bin 3109: 3 of cap free
Amount of items: 3
Items: 
Size: 364900 Color: 1
Size: 320403 Color: 1
Size: 314695 Color: 0

Bin 3110: 3 of cap free
Amount of items: 3
Items: 
Size: 351723 Color: 1
Size: 343884 Color: 1
Size: 304391 Color: 0

Bin 3111: 3 of cap free
Amount of items: 3
Items: 
Size: 358047 Color: 1
Size: 344668 Color: 1
Size: 297283 Color: 0

Bin 3112: 3 of cap free
Amount of items: 3
Items: 
Size: 365470 Color: 1
Size: 357041 Color: 1
Size: 277487 Color: 0

Bin 3113: 3 of cap free
Amount of items: 3
Items: 
Size: 357985 Color: 1
Size: 323454 Color: 0
Size: 318559 Color: 1

Bin 3114: 3 of cap free
Amount of items: 3
Items: 
Size: 345102 Color: 1
Size: 331337 Color: 1
Size: 323559 Color: 0

Bin 3115: 3 of cap free
Amount of items: 3
Items: 
Size: 415996 Color: 1
Size: 308125 Color: 1
Size: 275877 Color: 0

Bin 3116: 3 of cap free
Amount of items: 3
Items: 
Size: 441899 Color: 1
Size: 294542 Color: 1
Size: 263557 Color: 0

Bin 3117: 4 of cap free
Amount of items: 3
Items: 
Size: 411171 Color: 1
Size: 299357 Color: 1
Size: 289469 Color: 0

Bin 3118: 4 of cap free
Amount of items: 3
Items: 
Size: 391840 Color: 1
Size: 345292 Color: 1
Size: 262865 Color: 0

Bin 3119: 4 of cap free
Amount of items: 3
Items: 
Size: 409101 Color: 1
Size: 307912 Color: 1
Size: 282984 Color: 0

Bin 3120: 4 of cap free
Amount of items: 3
Items: 
Size: 482528 Color: 1
Size: 263672 Color: 1
Size: 253797 Color: 0

Bin 3121: 4 of cap free
Amount of items: 3
Items: 
Size: 369599 Color: 1
Size: 350146 Color: 1
Size: 280252 Color: 0

Bin 3122: 4 of cap free
Amount of items: 3
Items: 
Size: 372775 Color: 1
Size: 340186 Color: 1
Size: 287036 Color: 0

Bin 3123: 4 of cap free
Amount of items: 3
Items: 
Size: 361116 Color: 1
Size: 320891 Color: 0
Size: 317990 Color: 1

Bin 3124: 4 of cap free
Amount of items: 3
Items: 
Size: 396408 Color: 1
Size: 331457 Color: 1
Size: 272132 Color: 0

Bin 3125: 4 of cap free
Amount of items: 3
Items: 
Size: 379887 Color: 1
Size: 358908 Color: 1
Size: 261202 Color: 0

Bin 3126: 4 of cap free
Amount of items: 3
Items: 
Size: 354429 Color: 1
Size: 348792 Color: 1
Size: 296776 Color: 0

Bin 3127: 4 of cap free
Amount of items: 3
Items: 
Size: 391543 Color: 1
Size: 336575 Color: 1
Size: 271879 Color: 0

Bin 3128: 4 of cap free
Amount of items: 3
Items: 
Size: 386398 Color: 1
Size: 336961 Color: 1
Size: 276638 Color: 0

Bin 3129: 5 of cap free
Amount of items: 3
Items: 
Size: 381831 Color: 1
Size: 323741 Color: 1
Size: 294424 Color: 0

Bin 3130: 5 of cap free
Amount of items: 3
Items: 
Size: 426336 Color: 1
Size: 323471 Color: 1
Size: 250189 Color: 0

Bin 3131: 5 of cap free
Amount of items: 3
Items: 
Size: 376185 Color: 1
Size: 348263 Color: 1
Size: 275548 Color: 0

Bin 3132: 5 of cap free
Amount of items: 3
Items: 
Size: 383069 Color: 1
Size: 361705 Color: 1
Size: 255222 Color: 0

Bin 3133: 5 of cap free
Amount of items: 3
Items: 
Size: 355332 Color: 1
Size: 345768 Color: 1
Size: 298896 Color: 0

Bin 3134: 5 of cap free
Amount of items: 3
Items: 
Size: 441472 Color: 1
Size: 303443 Color: 1
Size: 255081 Color: 0

Bin 3135: 5 of cap free
Amount of items: 3
Items: 
Size: 393658 Color: 1
Size: 347693 Color: 1
Size: 258645 Color: 0

Bin 3136: 5 of cap free
Amount of items: 3
Items: 
Size: 358255 Color: 1
Size: 353084 Color: 1
Size: 288657 Color: 0

Bin 3137: 5 of cap free
Amount of items: 3
Items: 
Size: 363239 Color: 1
Size: 362769 Color: 1
Size: 273988 Color: 0

Bin 3138: 5 of cap free
Amount of items: 3
Items: 
Size: 347245 Color: 1
Size: 346668 Color: 1
Size: 306083 Color: 0

Bin 3139: 5 of cap free
Amount of items: 3
Items: 
Size: 375057 Color: 1
Size: 349681 Color: 1
Size: 275258 Color: 0

Bin 3140: 5 of cap free
Amount of items: 3
Items: 
Size: 365647 Color: 1
Size: 318760 Color: 0
Size: 315589 Color: 1

Bin 3141: 5 of cap free
Amount of items: 3
Items: 
Size: 359728 Color: 1
Size: 358469 Color: 1
Size: 281799 Color: 0

Bin 3142: 5 of cap free
Amount of items: 3
Items: 
Size: 369549 Color: 1
Size: 332362 Color: 1
Size: 298085 Color: 0

Bin 3143: 5 of cap free
Amount of items: 3
Items: 
Size: 372838 Color: 1
Size: 358424 Color: 1
Size: 268734 Color: 0

Bin 3144: 5 of cap free
Amount of items: 3
Items: 
Size: 427799 Color: 1
Size: 290071 Color: 0
Size: 282126 Color: 1

Bin 3145: 6 of cap free
Amount of items: 3
Items: 
Size: 381395 Color: 1
Size: 337365 Color: 1
Size: 281235 Color: 0

Bin 3146: 6 of cap free
Amount of items: 3
Items: 
Size: 387293 Color: 1
Size: 320395 Color: 0
Size: 292307 Color: 1

Bin 3147: 6 of cap free
Amount of items: 3
Items: 
Size: 391385 Color: 1
Size: 319065 Color: 1
Size: 289545 Color: 0

Bin 3148: 6 of cap free
Amount of items: 3
Items: 
Size: 363040 Color: 1
Size: 322367 Color: 1
Size: 314588 Color: 0

Bin 3149: 6 of cap free
Amount of items: 3
Items: 
Size: 367829 Color: 1
Size: 322143 Color: 1
Size: 310023 Color: 0

Bin 3150: 6 of cap free
Amount of items: 3
Items: 
Size: 366921 Color: 1
Size: 339014 Color: 1
Size: 294060 Color: 0

Bin 3151: 6 of cap free
Amount of items: 3
Items: 
Size: 351486 Color: 1
Size: 347275 Color: 1
Size: 301234 Color: 0

Bin 3152: 6 of cap free
Amount of items: 3
Items: 
Size: 372228 Color: 1
Size: 315477 Color: 0
Size: 312290 Color: 1

Bin 3153: 6 of cap free
Amount of items: 3
Items: 
Size: 407734 Color: 1
Size: 341783 Color: 1
Size: 250478 Color: 0

Bin 3154: 6 of cap free
Amount of items: 3
Items: 
Size: 358926 Color: 1
Size: 336565 Color: 1
Size: 304504 Color: 0

Bin 3155: 6 of cap free
Amount of items: 3
Items: 
Size: 421085 Color: 1
Size: 316773 Color: 1
Size: 262137 Color: 0

Bin 3156: 6 of cap free
Amount of items: 3
Items: 
Size: 347588 Color: 1
Size: 330964 Color: 1
Size: 321443 Color: 0

Bin 3157: 6 of cap free
Amount of items: 3
Items: 
Size: 429429 Color: 1
Size: 316792 Color: 1
Size: 253774 Color: 0

Bin 3158: 7 of cap free
Amount of items: 3
Items: 
Size: 366671 Color: 1
Size: 328899 Color: 1
Size: 304424 Color: 0

Bin 3159: 7 of cap free
Amount of items: 3
Items: 
Size: 420599 Color: 1
Size: 310131 Color: 1
Size: 269264 Color: 0

Bin 3160: 7 of cap free
Amount of items: 3
Items: 
Size: 375134 Color: 1
Size: 334352 Color: 1
Size: 290508 Color: 0

Bin 3161: 7 of cap free
Amount of items: 3
Items: 
Size: 440538 Color: 1
Size: 282263 Color: 1
Size: 277193 Color: 0

Bin 3162: 7 of cap free
Amount of items: 3
Items: 
Size: 354330 Color: 1
Size: 323110 Color: 1
Size: 322554 Color: 0

Bin 3163: 7 of cap free
Amount of items: 3
Items: 
Size: 368345 Color: 1
Size: 343347 Color: 1
Size: 288302 Color: 0

Bin 3164: 7 of cap free
Amount of items: 3
Items: 
Size: 410989 Color: 1
Size: 328094 Color: 1
Size: 260911 Color: 0

Bin 3165: 7 of cap free
Amount of items: 3
Items: 
Size: 404088 Color: 1
Size: 308681 Color: 0
Size: 287225 Color: 1

Bin 3166: 7 of cap free
Amount of items: 3
Items: 
Size: 348866 Color: 1
Size: 344850 Color: 1
Size: 306278 Color: 0

Bin 3167: 7 of cap free
Amount of items: 3
Items: 
Size: 356483 Color: 1
Size: 355156 Color: 1
Size: 288355 Color: 0

Bin 3168: 7 of cap free
Amount of items: 3
Items: 
Size: 361333 Color: 1
Size: 340993 Color: 1
Size: 297668 Color: 0

Bin 3169: 7 of cap free
Amount of items: 3
Items: 
Size: 397081 Color: 1
Size: 348712 Color: 1
Size: 254201 Color: 0

Bin 3170: 7 of cap free
Amount of items: 3
Items: 
Size: 378434 Color: 1
Size: 356870 Color: 1
Size: 264690 Color: 0

Bin 3171: 7 of cap free
Amount of items: 3
Items: 
Size: 392915 Color: 1
Size: 347227 Color: 1
Size: 259852 Color: 0

Bin 3172: 8 of cap free
Amount of items: 3
Items: 
Size: 421431 Color: 1
Size: 313926 Color: 1
Size: 264636 Color: 0

Bin 3173: 8 of cap free
Amount of items: 3
Items: 
Size: 350069 Color: 1
Size: 339627 Color: 1
Size: 310297 Color: 0

Bin 3174: 8 of cap free
Amount of items: 3
Items: 
Size: 364440 Color: 1
Size: 339485 Color: 1
Size: 296068 Color: 0

Bin 3175: 8 of cap free
Amount of items: 3
Items: 
Size: 400496 Color: 1
Size: 308933 Color: 0
Size: 290564 Color: 1

Bin 3176: 9 of cap free
Amount of items: 3
Items: 
Size: 385825 Color: 1
Size: 330403 Color: 1
Size: 283764 Color: 0

Bin 3177: 9 of cap free
Amount of items: 3
Items: 
Size: 373160 Color: 1
Size: 326723 Color: 1
Size: 300109 Color: 0

Bin 3178: 9 of cap free
Amount of items: 3
Items: 
Size: 407567 Color: 1
Size: 341119 Color: 1
Size: 251306 Color: 0

Bin 3179: 9 of cap free
Amount of items: 3
Items: 
Size: 359114 Color: 1
Size: 330207 Color: 1
Size: 310671 Color: 0

Bin 3180: 9 of cap free
Amount of items: 3
Items: 
Size: 433506 Color: 1
Size: 285218 Color: 0
Size: 281268 Color: 1

Bin 3181: 9 of cap free
Amount of items: 3
Items: 
Size: 422450 Color: 1
Size: 301703 Color: 1
Size: 275839 Color: 0

Bin 3182: 9 of cap free
Amount of items: 3
Items: 
Size: 375436 Color: 1
Size: 342812 Color: 1
Size: 281744 Color: 0

Bin 3183: 9 of cap free
Amount of items: 3
Items: 
Size: 399825 Color: 1
Size: 348932 Color: 1
Size: 251235 Color: 0

Bin 3184: 9 of cap free
Amount of items: 3
Items: 
Size: 432148 Color: 1
Size: 309892 Color: 1
Size: 257952 Color: 0

Bin 3185: 9 of cap free
Amount of items: 3
Items: 
Size: 379156 Color: 1
Size: 340029 Color: 1
Size: 280807 Color: 0

Bin 3186: 9 of cap free
Amount of items: 3
Items: 
Size: 360035 Color: 1
Size: 356885 Color: 1
Size: 283072 Color: 0

Bin 3187: 9 of cap free
Amount of items: 3
Items: 
Size: 415530 Color: 1
Size: 297912 Color: 1
Size: 286550 Color: 0

Bin 3188: 9 of cap free
Amount of items: 3
Items: 
Size: 409149 Color: 1
Size: 321628 Color: 1
Size: 269215 Color: 0

Bin 3189: 9 of cap free
Amount of items: 3
Items: 
Size: 340438 Color: 1
Size: 330671 Color: 1
Size: 328883 Color: 0

Bin 3190: 9 of cap free
Amount of items: 3
Items: 
Size: 464241 Color: 1
Size: 273562 Color: 1
Size: 262189 Color: 0

Bin 3191: 10 of cap free
Amount of items: 3
Items: 
Size: 431606 Color: 1
Size: 313444 Color: 1
Size: 254941 Color: 0

Bin 3192: 10 of cap free
Amount of items: 3
Items: 
Size: 413565 Color: 1
Size: 301175 Color: 0
Size: 285251 Color: 1

Bin 3193: 10 of cap free
Amount of items: 3
Items: 
Size: 394520 Color: 1
Size: 338470 Color: 1
Size: 267001 Color: 0

Bin 3194: 10 of cap free
Amount of items: 3
Items: 
Size: 407611 Color: 1
Size: 320809 Color: 1
Size: 271571 Color: 0

Bin 3195: 10 of cap free
Amount of items: 3
Items: 
Size: 378232 Color: 1
Size: 364983 Color: 1
Size: 256776 Color: 0

Bin 3196: 10 of cap free
Amount of items: 3
Items: 
Size: 392766 Color: 1
Size: 317373 Color: 1
Size: 289852 Color: 0

Bin 3197: 10 of cap free
Amount of items: 3
Items: 
Size: 416318 Color: 1
Size: 307851 Color: 1
Size: 275822 Color: 0

Bin 3198: 11 of cap free
Amount of items: 3
Items: 
Size: 412187 Color: 1
Size: 335531 Color: 1
Size: 252272 Color: 0

Bin 3199: 11 of cap free
Amount of items: 3
Items: 
Size: 389053 Color: 1
Size: 356073 Color: 1
Size: 254864 Color: 0

Bin 3200: 11 of cap free
Amount of items: 3
Items: 
Size: 360545 Color: 1
Size: 348399 Color: 1
Size: 291046 Color: 0

Bin 3201: 12 of cap free
Amount of items: 3
Items: 
Size: 461826 Color: 1
Size: 282106 Color: 1
Size: 256057 Color: 0

Bin 3202: 12 of cap free
Amount of items: 3
Items: 
Size: 446755 Color: 1
Size: 300052 Color: 1
Size: 253182 Color: 0

Bin 3203: 12 of cap free
Amount of items: 3
Items: 
Size: 350596 Color: 1
Size: 327663 Color: 1
Size: 321730 Color: 0

Bin 3204: 12 of cap free
Amount of items: 3
Items: 
Size: 372245 Color: 1
Size: 341887 Color: 1
Size: 285857 Color: 0

Bin 3205: 13 of cap free
Amount of items: 3
Items: 
Size: 419608 Color: 1
Size: 305536 Color: 1
Size: 274844 Color: 0

Bin 3206: 13 of cap free
Amount of items: 3
Items: 
Size: 393012 Color: 1
Size: 325320 Color: 1
Size: 281656 Color: 0

Bin 3207: 13 of cap free
Amount of items: 3
Items: 
Size: 433854 Color: 1
Size: 293759 Color: 1
Size: 272375 Color: 0

Bin 3208: 13 of cap free
Amount of items: 3
Items: 
Size: 388198 Color: 1
Size: 309625 Color: 0
Size: 302165 Color: 1

Bin 3209: 14 of cap free
Amount of items: 3
Items: 
Size: 389298 Color: 1
Size: 343497 Color: 1
Size: 267192 Color: 0

Bin 3210: 15 of cap free
Amount of items: 3
Items: 
Size: 390755 Color: 1
Size: 315734 Color: 0
Size: 293497 Color: 1

Bin 3211: 15 of cap free
Amount of items: 3
Items: 
Size: 435640 Color: 1
Size: 307549 Color: 1
Size: 256797 Color: 0

Bin 3212: 15 of cap free
Amount of items: 3
Items: 
Size: 378716 Color: 1
Size: 311994 Color: 0
Size: 309276 Color: 1

Bin 3213: 15 of cap free
Amount of items: 3
Items: 
Size: 357905 Color: 1
Size: 337330 Color: 1
Size: 304751 Color: 0

Bin 3214: 15 of cap free
Amount of items: 3
Items: 
Size: 362586 Color: 1
Size: 358105 Color: 1
Size: 279295 Color: 0

Bin 3215: 15 of cap free
Amount of items: 3
Items: 
Size: 419014 Color: 1
Size: 312978 Color: 1
Size: 267994 Color: 0

Bin 3216: 16 of cap free
Amount of items: 3
Items: 
Size: 353175 Color: 1
Size: 328959 Color: 1
Size: 317851 Color: 0

Bin 3217: 16 of cap free
Amount of items: 3
Items: 
Size: 405317 Color: 1
Size: 307862 Color: 1
Size: 286806 Color: 0

Bin 3218: 16 of cap free
Amount of items: 3
Items: 
Size: 408486 Color: 1
Size: 300409 Color: 1
Size: 291090 Color: 0

Bin 3219: 17 of cap free
Amount of items: 3
Items: 
Size: 346926 Color: 1
Size: 339478 Color: 1
Size: 313580 Color: 0

Bin 3220: 17 of cap free
Amount of items: 3
Items: 
Size: 360499 Color: 1
Size: 321784 Color: 0
Size: 317701 Color: 1

Bin 3221: 17 of cap free
Amount of items: 3
Items: 
Size: 385680 Color: 1
Size: 348173 Color: 1
Size: 266131 Color: 0

Bin 3222: 17 of cap free
Amount of items: 3
Items: 
Size: 354480 Color: 1
Size: 335239 Color: 1
Size: 310265 Color: 0

Bin 3223: 17 of cap free
Amount of items: 3
Items: 
Size: 396485 Color: 1
Size: 350462 Color: 1
Size: 253037 Color: 0

Bin 3224: 17 of cap free
Amount of items: 3
Items: 
Size: 412567 Color: 1
Size: 331041 Color: 1
Size: 256376 Color: 0

Bin 3225: 18 of cap free
Amount of items: 3
Items: 
Size: 474568 Color: 1
Size: 263412 Color: 1
Size: 262003 Color: 0

Bin 3226: 18 of cap free
Amount of items: 3
Items: 
Size: 388934 Color: 1
Size: 324758 Color: 0
Size: 286291 Color: 1

Bin 3227: 18 of cap free
Amount of items: 3
Items: 
Size: 348824 Color: 1
Size: 347652 Color: 1
Size: 303507 Color: 0

Bin 3228: 19 of cap free
Amount of items: 3
Items: 
Size: 379741 Color: 1
Size: 346852 Color: 1
Size: 273389 Color: 0

Bin 3229: 19 of cap free
Amount of items: 3
Items: 
Size: 379052 Color: 1
Size: 364841 Color: 1
Size: 256089 Color: 0

Bin 3230: 20 of cap free
Amount of items: 3
Items: 
Size: 391989 Color: 1
Size: 333050 Color: 1
Size: 274942 Color: 0

Bin 3231: 20 of cap free
Amount of items: 3
Items: 
Size: 382590 Color: 1
Size: 354345 Color: 1
Size: 263046 Color: 0

Bin 3232: 21 of cap free
Amount of items: 3
Items: 
Size: 427657 Color: 1
Size: 309705 Color: 1
Size: 262618 Color: 0

Bin 3233: 21 of cap free
Amount of items: 3
Items: 
Size: 386936 Color: 1
Size: 342355 Color: 1
Size: 270689 Color: 0

Bin 3234: 21 of cap free
Amount of items: 3
Items: 
Size: 368252 Color: 1
Size: 348392 Color: 1
Size: 283336 Color: 0

Bin 3235: 21 of cap free
Amount of items: 3
Items: 
Size: 388990 Color: 1
Size: 306684 Color: 0
Size: 304306 Color: 1

Bin 3236: 21 of cap free
Amount of items: 3
Items: 
Size: 393540 Color: 1
Size: 346050 Color: 1
Size: 260390 Color: 0

Bin 3237: 22 of cap free
Amount of items: 3
Items: 
Size: 442709 Color: 1
Size: 297783 Color: 1
Size: 259487 Color: 0

Bin 3238: 22 of cap free
Amount of items: 3
Items: 
Size: 382173 Color: 1
Size: 318551 Color: 1
Size: 299255 Color: 0

Bin 3239: 22 of cap free
Amount of items: 3
Items: 
Size: 385612 Color: 1
Size: 317060 Color: 1
Size: 297307 Color: 0

Bin 3240: 23 of cap free
Amount of items: 3
Items: 
Size: 403045 Color: 1
Size: 342489 Color: 1
Size: 254444 Color: 0

Bin 3241: 24 of cap free
Amount of items: 3
Items: 
Size: 369737 Color: 1
Size: 358678 Color: 1
Size: 271562 Color: 0

Bin 3242: 24 of cap free
Amount of items: 3
Items: 
Size: 407411 Color: 1
Size: 327111 Color: 1
Size: 265455 Color: 0

Bin 3243: 24 of cap free
Amount of items: 3
Items: 
Size: 427108 Color: 1
Size: 293310 Color: 1
Size: 279559 Color: 0

Bin 3244: 25 of cap free
Amount of items: 3
Items: 
Size: 373471 Color: 1
Size: 363967 Color: 1
Size: 262538 Color: 0

Bin 3245: 26 of cap free
Amount of items: 3
Items: 
Size: 461536 Color: 1
Size: 272496 Color: 0
Size: 265943 Color: 1

Bin 3246: 26 of cap free
Amount of items: 3
Items: 
Size: 349063 Color: 1
Size: 328213 Color: 1
Size: 322699 Color: 0

Bin 3247: 27 of cap free
Amount of items: 3
Items: 
Size: 348217 Color: 1
Size: 345922 Color: 1
Size: 305835 Color: 0

Bin 3248: 28 of cap free
Amount of items: 3
Items: 
Size: 375116 Color: 1
Size: 360390 Color: 1
Size: 264467 Color: 0

Bin 3249: 29 of cap free
Amount of items: 3
Items: 
Size: 379416 Color: 1
Size: 363359 Color: 1
Size: 257197 Color: 0

Bin 3250: 29 of cap free
Amount of items: 3
Items: 
Size: 452495 Color: 1
Size: 290711 Color: 1
Size: 256766 Color: 0

Bin 3251: 29 of cap free
Amount of items: 3
Items: 
Size: 407456 Color: 1
Size: 306031 Color: 1
Size: 286485 Color: 0

Bin 3252: 29 of cap free
Amount of items: 3
Items: 
Size: 381275 Color: 1
Size: 327620 Color: 1
Size: 291077 Color: 0

Bin 3253: 30 of cap free
Amount of items: 3
Items: 
Size: 372767 Color: 1
Size: 331117 Color: 1
Size: 296087 Color: 0

Bin 3254: 31 of cap free
Amount of items: 3
Items: 
Size: 454657 Color: 1
Size: 284781 Color: 1
Size: 260532 Color: 0

Bin 3255: 31 of cap free
Amount of items: 3
Items: 
Size: 351847 Color: 1
Size: 344828 Color: 1
Size: 303295 Color: 0

Bin 3256: 34 of cap free
Amount of items: 3
Items: 
Size: 361703 Color: 1
Size: 335978 Color: 1
Size: 302286 Color: 0

Bin 3257: 35 of cap free
Amount of items: 3
Items: 
Size: 410573 Color: 1
Size: 339075 Color: 1
Size: 250318 Color: 0

Bin 3258: 35 of cap free
Amount of items: 3
Items: 
Size: 414091 Color: 1
Size: 315538 Color: 1
Size: 270337 Color: 0

Bin 3259: 35 of cap free
Amount of items: 3
Items: 
Size: 365442 Color: 1
Size: 359283 Color: 1
Size: 275241 Color: 0

Bin 3260: 36 of cap free
Amount of items: 3
Items: 
Size: 427286 Color: 1
Size: 298199 Color: 1
Size: 274480 Color: 0

Bin 3261: 36 of cap free
Amount of items: 3
Items: 
Size: 404471 Color: 1
Size: 329230 Color: 1
Size: 266264 Color: 0

Bin 3262: 37 of cap free
Amount of items: 3
Items: 
Size: 455080 Color: 1
Size: 284679 Color: 1
Size: 260205 Color: 0

Bin 3263: 38 of cap free
Amount of items: 3
Items: 
Size: 368815 Color: 1
Size: 365789 Color: 1
Size: 265359 Color: 0

Bin 3264: 38 of cap free
Amount of items: 3
Items: 
Size: 393727 Color: 1
Size: 304958 Color: 1
Size: 301278 Color: 0

Bin 3265: 38 of cap free
Amount of items: 3
Items: 
Size: 455924 Color: 1
Size: 276564 Color: 0
Size: 267475 Color: 1

Bin 3266: 38 of cap free
Amount of items: 3
Items: 
Size: 407551 Color: 1
Size: 303436 Color: 0
Size: 288976 Color: 1

Bin 3267: 39 of cap free
Amount of items: 3
Items: 
Size: 390442 Color: 1
Size: 335743 Color: 1
Size: 273777 Color: 0

Bin 3268: 42 of cap free
Amount of items: 3
Items: 
Size: 423626 Color: 1
Size: 294211 Color: 1
Size: 282122 Color: 0

Bin 3269: 42 of cap free
Amount of items: 3
Items: 
Size: 372349 Color: 1
Size: 354573 Color: 1
Size: 273037 Color: 0

Bin 3270: 42 of cap free
Amount of items: 3
Items: 
Size: 408798 Color: 1
Size: 340632 Color: 1
Size: 250529 Color: 0

Bin 3271: 42 of cap free
Amount of items: 3
Items: 
Size: 376954 Color: 1
Size: 319971 Color: 1
Size: 303034 Color: 0

Bin 3272: 44 of cap free
Amount of items: 3
Items: 
Size: 398451 Color: 1
Size: 329752 Color: 1
Size: 271754 Color: 0

Bin 3273: 44 of cap free
Amount of items: 3
Items: 
Size: 346520 Color: 1
Size: 332941 Color: 1
Size: 320496 Color: 0

Bin 3274: 44 of cap free
Amount of items: 3
Items: 
Size: 402431 Color: 1
Size: 309863 Color: 1
Size: 287663 Color: 0

Bin 3275: 45 of cap free
Amount of items: 3
Items: 
Size: 367903 Color: 1
Size: 350623 Color: 1
Size: 281430 Color: 0

Bin 3276: 45 of cap free
Amount of items: 3
Items: 
Size: 389401 Color: 1
Size: 354649 Color: 1
Size: 255906 Color: 0

Bin 3277: 48 of cap free
Amount of items: 3
Items: 
Size: 421216 Color: 1
Size: 297658 Color: 1
Size: 281079 Color: 0

Bin 3278: 49 of cap free
Amount of items: 3
Items: 
Size: 394770 Color: 1
Size: 310341 Color: 0
Size: 294841 Color: 1

Bin 3279: 49 of cap free
Amount of items: 3
Items: 
Size: 338367 Color: 1
Size: 337555 Color: 1
Size: 324030 Color: 0

Bin 3280: 49 of cap free
Amount of items: 3
Items: 
Size: 468890 Color: 1
Size: 275218 Color: 1
Size: 255844 Color: 0

Bin 3281: 52 of cap free
Amount of items: 3
Items: 
Size: 436244 Color: 1
Size: 293922 Color: 1
Size: 269783 Color: 0

Bin 3282: 53 of cap free
Amount of items: 3
Items: 
Size: 391469 Color: 1
Size: 308402 Color: 0
Size: 300077 Color: 1

Bin 3283: 53 of cap free
Amount of items: 3
Items: 
Size: 428740 Color: 1
Size: 302564 Color: 1
Size: 268644 Color: 0

Bin 3284: 54 of cap free
Amount of items: 3
Items: 
Size: 422205 Color: 1
Size: 324990 Color: 1
Size: 252752 Color: 0

Bin 3285: 57 of cap free
Amount of items: 3
Items: 
Size: 357165 Color: 1
Size: 327267 Color: 1
Size: 315512 Color: 0

Bin 3286: 60 of cap free
Amount of items: 3
Items: 
Size: 382410 Color: 1
Size: 314900 Color: 1
Size: 302631 Color: 0

Bin 3287: 67 of cap free
Amount of items: 3
Items: 
Size: 413545 Color: 1
Size: 316763 Color: 1
Size: 269626 Color: 0

Bin 3288: 68 of cap free
Amount of items: 3
Items: 
Size: 376030 Color: 1
Size: 316551 Color: 1
Size: 307352 Color: 0

Bin 3289: 71 of cap free
Amount of items: 3
Items: 
Size: 451879 Color: 1
Size: 277264 Color: 1
Size: 270787 Color: 0

Bin 3290: 71 of cap free
Amount of items: 3
Items: 
Size: 384758 Color: 1
Size: 322199 Color: 0
Size: 292973 Color: 1

Bin 3291: 74 of cap free
Amount of items: 3
Items: 
Size: 407219 Color: 1
Size: 302057 Color: 1
Size: 290651 Color: 0

Bin 3292: 74 of cap free
Amount of items: 3
Items: 
Size: 365010 Color: 1
Size: 324754 Color: 0
Size: 310163 Color: 1

Bin 3293: 78 of cap free
Amount of items: 3
Items: 
Size: 437238 Color: 1
Size: 282403 Color: 0
Size: 280282 Color: 1

Bin 3294: 81 of cap free
Amount of items: 3
Items: 
Size: 434778 Color: 1
Size: 307595 Color: 1
Size: 257547 Color: 0

Bin 3295: 81 of cap free
Amount of items: 3
Items: 
Size: 402114 Color: 1
Size: 346956 Color: 1
Size: 250850 Color: 0

Bin 3296: 82 of cap free
Amount of items: 3
Items: 
Size: 415872 Color: 1
Size: 303654 Color: 1
Size: 280393 Color: 0

Bin 3297: 85 of cap free
Amount of items: 3
Items: 
Size: 396085 Color: 1
Size: 305635 Color: 1
Size: 298196 Color: 0

Bin 3298: 97 of cap free
Amount of items: 3
Items: 
Size: 426388 Color: 1
Size: 322422 Color: 1
Size: 251094 Color: 0

Bin 3299: 97 of cap free
Amount of items: 3
Items: 
Size: 400818 Color: 1
Size: 307485 Color: 1
Size: 291601 Color: 0

Bin 3300: 100 of cap free
Amount of items: 3
Items: 
Size: 366126 Color: 1
Size: 357967 Color: 1
Size: 275808 Color: 0

Bin 3301: 105 of cap free
Amount of items: 3
Items: 
Size: 442504 Color: 1
Size: 299965 Color: 1
Size: 257427 Color: 0

Bin 3302: 123 of cap free
Amount of items: 3
Items: 
Size: 399413 Color: 1
Size: 304300 Color: 1
Size: 296165 Color: 0

Bin 3303: 131 of cap free
Amount of items: 3
Items: 
Size: 406442 Color: 1
Size: 296987 Color: 0
Size: 296441 Color: 1

Bin 3304: 138 of cap free
Amount of items: 3
Items: 
Size: 424864 Color: 1
Size: 314254 Color: 1
Size: 260745 Color: 0

Bin 3305: 153 of cap free
Amount of items: 3
Items: 
Size: 413658 Color: 1
Size: 327451 Color: 1
Size: 258739 Color: 0

Bin 3306: 159 of cap free
Amount of items: 3
Items: 
Size: 472450 Color: 1
Size: 265883 Color: 0
Size: 261509 Color: 1

Bin 3307: 200 of cap free
Amount of items: 3
Items: 
Size: 421929 Color: 1
Size: 312797 Color: 1
Size: 265075 Color: 0

Bin 3308: 218 of cap free
Amount of items: 3
Items: 
Size: 477899 Color: 1
Size: 268453 Color: 1
Size: 253431 Color: 0

Bin 3309: 232 of cap free
Amount of items: 3
Items: 
Size: 374123 Color: 1
Size: 313471 Color: 0
Size: 312175 Color: 1

Bin 3310: 256 of cap free
Amount of items: 3
Items: 
Size: 448170 Color: 1
Size: 285085 Color: 1
Size: 266490 Color: 0

Bin 3311: 263 of cap free
Amount of items: 3
Items: 
Size: 386223 Color: 1
Size: 322622 Color: 1
Size: 290893 Color: 0

Bin 3312: 266 of cap free
Amount of items: 3
Items: 
Size: 363348 Color: 1
Size: 325327 Color: 0
Size: 311060 Color: 1

Bin 3313: 289 of cap free
Amount of items: 3
Items: 
Size: 360774 Color: 1
Size: 331038 Color: 1
Size: 307900 Color: 0

Bin 3314: 294 of cap free
Amount of items: 3
Items: 
Size: 369885 Color: 1
Size: 345287 Color: 1
Size: 284535 Color: 0

Bin 3315: 302 of cap free
Amount of items: 3
Items: 
Size: 407043 Color: 1
Size: 320194 Color: 1
Size: 272462 Color: 0

Bin 3316: 343 of cap free
Amount of items: 3
Items: 
Size: 354967 Color: 1
Size: 330455 Color: 1
Size: 314236 Color: 0

Bin 3317: 349 of cap free
Amount of items: 3
Items: 
Size: 382127 Color: 1
Size: 323766 Color: 1
Size: 293759 Color: 0

Bin 3318: 364 of cap free
Amount of items: 3
Items: 
Size: 361885 Color: 1
Size: 347684 Color: 1
Size: 290068 Color: 0

Bin 3319: 413 of cap free
Amount of items: 3
Items: 
Size: 401368 Color: 1
Size: 318843 Color: 1
Size: 279377 Color: 0

Bin 3320: 472 of cap free
Amount of items: 3
Items: 
Size: 421102 Color: 1
Size: 291417 Color: 1
Size: 287010 Color: 0

Bin 3321: 511 of cap free
Amount of items: 3
Items: 
Size: 407022 Color: 1
Size: 337787 Color: 1
Size: 254681 Color: 0

Bin 3322: 630 of cap free
Amount of items: 3
Items: 
Size: 401267 Color: 1
Size: 309781 Color: 1
Size: 288323 Color: 0

Bin 3323: 667 of cap free
Amount of items: 3
Items: 
Size: 423587 Color: 1
Size: 305996 Color: 1
Size: 269751 Color: 0

Bin 3324: 709 of cap free
Amount of items: 3
Items: 
Size: 391089 Color: 1
Size: 321875 Color: 0
Size: 286328 Color: 1

Bin 3325: 724 of cap free
Amount of items: 3
Items: 
Size: 373057 Color: 1
Size: 325717 Color: 1
Size: 300503 Color: 0

Bin 3326: 779 of cap free
Amount of items: 3
Items: 
Size: 389960 Color: 1
Size: 310716 Color: 1
Size: 298546 Color: 0

Bin 3327: 907 of cap free
Amount of items: 3
Items: 
Size: 376916 Color: 1
Size: 316888 Color: 1
Size: 305290 Color: 0

Bin 3328: 1444 of cap free
Amount of items: 3
Items: 
Size: 358311 Color: 1
Size: 354793 Color: 1
Size: 285453 Color: 0

Bin 3329: 1473 of cap free
Amount of items: 3
Items: 
Size: 461631 Color: 1
Size: 282419 Color: 1
Size: 254478 Color: 0

Bin 3330: 3071 of cap free
Amount of items: 3
Items: 
Size: 338497 Color: 1
Size: 331082 Color: 0
Size: 327351 Color: 1

Bin 3331: 3727 of cap free
Amount of items: 3
Items: 
Size: 389076 Color: 1
Size: 318136 Color: 0
Size: 289062 Color: 1

Bin 3332: 74911 of cap free
Amount of items: 3
Items: 
Size: 331796 Color: 1
Size: 310604 Color: 1
Size: 282690 Color: 0

Bin 3333: 141596 of cap free
Amount of items: 3
Items: 
Size: 308423 Color: 1
Size: 279466 Color: 1
Size: 270516 Color: 0

Bin 3334: 249210 of cap free
Amount of items: 2
Items: 
Size: 498749 Color: 1
Size: 252042 Color: 0

Bin 3335: 510179 of cap free
Amount of items: 1
Items: 
Size: 489822 Color: 1

Total size: 3334003334
Total free space: 1000001

