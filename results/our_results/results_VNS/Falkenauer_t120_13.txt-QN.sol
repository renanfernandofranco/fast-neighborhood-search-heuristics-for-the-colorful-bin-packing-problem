Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 104
Size: 323 Color: 64
Size: 254 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 100
Size: 324 Color: 65
Size: 263 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 98
Size: 329 Color: 67
Size: 264 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 96
Size: 341 Color: 71
Size: 255 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 97
Size: 321 Color: 61
Size: 273 Color: 32

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 89
Size: 314 Color: 57
Size: 302 Color: 52

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 106
Size: 305 Color: 53
Size: 255 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 82
Size: 356 Color: 78
Size: 274 Color: 34

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 111
Size: 290 Color: 44
Size: 251 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 119
Size: 259 Color: 12
Size: 250 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 85
Size: 334 Color: 69
Size: 291 Color: 46

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 110
Size: 287 Color: 42
Size: 255 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 90
Size: 355 Color: 74
Size: 261 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 105
Size: 309 Color: 55
Size: 265 Color: 20

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 108
Size: 297 Color: 49
Size: 255 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 93
Size: 313 Color: 56
Size: 290 Color: 45

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 112
Size: 273 Color: 31
Size: 268 Color: 27

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 86
Size: 375 Color: 84
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 118
Size: 267 Color: 24
Size: 256 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 101
Size: 256 Color: 10
Size: 330 Color: 68

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 81
Size: 359 Color: 79
Size: 280 Color: 39

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 91
Size: 360 Color: 80
Size: 253 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 94
Size: 318 Color: 60
Size: 283 Color: 41

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 113
Size: 273 Color: 33
Size: 266 Color: 23

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 114
Size: 275 Color: 35
Size: 261 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 77
Size: 329 Color: 66
Size: 315 Color: 58

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 117
Size: 269 Color: 28
Size: 258 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 103
Size: 305 Color: 54
Size: 278 Color: 37

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 92
Size: 337 Color: 70
Size: 272 Color: 30

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 109
Size: 283 Color: 40
Size: 263 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 95
Size: 322 Color: 62
Size: 278 Color: 38

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 107
Size: 291 Color: 47
Size: 265 Color: 21

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 87
Size: 323 Color: 63
Size: 299 Color: 51

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 102
Size: 317 Color: 59
Size: 267 Color: 26

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 115
Size: 270 Color: 29
Size: 263 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 83
Size: 350 Color: 72
Size: 278 Color: 36

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 99
Size: 297 Color: 50
Size: 295 Color: 48

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 88
Size: 356 Color: 76
Size: 266 Color: 22

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 75
Size: 354 Color: 73
Size: 290 Color: 43

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 116
Size: 267 Color: 25
Size: 261 Color: 14

Total size: 40000
Total free space: 0

