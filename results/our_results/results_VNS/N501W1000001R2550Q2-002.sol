Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 361140 Color: 1
Size: 353230 Color: 0
Size: 285631 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462807 Color: 0
Size: 284427 Color: 1
Size: 252767 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 405789 Color: 0
Size: 325664 Color: 1
Size: 268548 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 363452 Color: 0
Size: 323858 Color: 1
Size: 312691 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 374510 Color: 1
Size: 374322 Color: 1
Size: 251169 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 456309 Color: 1
Size: 283186 Color: 0
Size: 260506 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 420222 Color: 0
Size: 307869 Color: 0
Size: 271910 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 364528 Color: 0
Size: 354296 Color: 1
Size: 281177 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 479951 Color: 1
Size: 260549 Color: 0
Size: 259501 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 426613 Color: 0
Size: 321885 Color: 1
Size: 251503 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 412550 Color: 1
Size: 293780 Color: 1
Size: 293671 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378153 Color: 1
Size: 313799 Color: 0
Size: 308049 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 411619 Color: 0
Size: 334309 Color: 1
Size: 254073 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 471112 Color: 0
Size: 271698 Color: 0
Size: 257191 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 479565 Color: 0
Size: 260226 Color: 1
Size: 260210 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 372256 Color: 0
Size: 339418 Color: 0
Size: 288327 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 349425 Color: 0
Size: 335287 Color: 1
Size: 315289 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 445551 Color: 0
Size: 287541 Color: 1
Size: 266909 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 420883 Color: 0
Size: 314545 Color: 1
Size: 264573 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 431431 Color: 1
Size: 307042 Color: 0
Size: 261528 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 396115 Color: 0
Size: 317848 Color: 0
Size: 286038 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 379755 Color: 1
Size: 352829 Color: 0
Size: 267417 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 410672 Color: 1
Size: 330995 Color: 0
Size: 258334 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 418429 Color: 1
Size: 301751 Color: 0
Size: 279821 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 367373 Color: 1
Size: 339588 Color: 0
Size: 293040 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 375726 Color: 1
Size: 344705 Color: 0
Size: 279570 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 396258 Color: 0
Size: 327066 Color: 1
Size: 276677 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 465729 Color: 0
Size: 283941 Color: 1
Size: 250331 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 401236 Color: 0
Size: 327192 Color: 0
Size: 271573 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 464856 Color: 1
Size: 274991 Color: 0
Size: 260154 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 403006 Color: 1
Size: 304238 Color: 0
Size: 292757 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 386666 Color: 1
Size: 309762 Color: 0
Size: 303573 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 404131 Color: 0
Size: 308404 Color: 1
Size: 287466 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 361032 Color: 0
Size: 329134 Color: 1
Size: 309835 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 429909 Color: 0
Size: 288450 Color: 1
Size: 281642 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 464298 Color: 1
Size: 282869 Color: 0
Size: 252834 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 479040 Color: 1
Size: 263591 Color: 1
Size: 257370 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 397927 Color: 1
Size: 338839 Color: 1
Size: 263235 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 472060 Color: 0
Size: 270019 Color: 1
Size: 257922 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 413414 Color: 1
Size: 297370 Color: 0
Size: 289217 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 387132 Color: 1
Size: 356997 Color: 0
Size: 255872 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 494307 Color: 0
Size: 254781 Color: 1
Size: 250913 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 379360 Color: 1
Size: 328134 Color: 0
Size: 292507 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 384181 Color: 1
Size: 325697 Color: 0
Size: 290123 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 372214 Color: 0
Size: 330165 Color: 0
Size: 297622 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 454395 Color: 0
Size: 279900 Color: 1
Size: 265706 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 397356 Color: 0
Size: 314942 Color: 1
Size: 287703 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 406233 Color: 0
Size: 300522 Color: 1
Size: 293246 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 363696 Color: 1
Size: 345385 Color: 1
Size: 290920 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 370267 Color: 0
Size: 333680 Color: 0
Size: 296054 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 401797 Color: 0
Size: 316823 Color: 1
Size: 281381 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 482948 Color: 0
Size: 265957 Color: 1
Size: 251096 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 481329 Color: 1
Size: 264979 Color: 0
Size: 253693 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 410087 Color: 0
Size: 306339 Color: 1
Size: 283575 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 393742 Color: 1
Size: 306236 Color: 0
Size: 300023 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 413609 Color: 1
Size: 302285 Color: 1
Size: 284107 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 487467 Color: 1
Size: 259767 Color: 0
Size: 252767 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 383899 Color: 0
Size: 308977 Color: 1
Size: 307125 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 390295 Color: 1
Size: 349641 Color: 1
Size: 260065 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 424810 Color: 0
Size: 300155 Color: 1
Size: 275036 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 318659 Color: 1
Size: 343846 Color: 0
Size: 337496 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 413512 Color: 0
Size: 322771 Color: 1
Size: 263718 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 440375 Color: 1
Size: 290958 Color: 1
Size: 268668 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 429762 Color: 1
Size: 320191 Color: 0
Size: 250048 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 409540 Color: 1
Size: 332347 Color: 0
Size: 258114 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 440008 Color: 0
Size: 301028 Color: 0
Size: 258965 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 476087 Color: 0
Size: 264727 Color: 1
Size: 259187 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 386884 Color: 0
Size: 334716 Color: 0
Size: 278401 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 493092 Color: 1
Size: 255753 Color: 0
Size: 251156 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 476236 Color: 1
Size: 267278 Color: 0
Size: 256487 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 346341 Color: 1
Size: 328997 Color: 0
Size: 324663 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 403511 Color: 0
Size: 337985 Color: 1
Size: 258505 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 412943 Color: 0
Size: 299317 Color: 1
Size: 287741 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 392462 Color: 0
Size: 326198 Color: 1
Size: 281341 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 491913 Color: 1
Size: 256199 Color: 0
Size: 251889 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 380532 Color: 1
Size: 329965 Color: 0
Size: 289504 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 485874 Color: 1
Size: 261678 Color: 0
Size: 252449 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 377062 Color: 0
Size: 338147 Color: 1
Size: 284792 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 365359 Color: 0
Size: 318865 Color: 1
Size: 315777 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 438865 Color: 1
Size: 290534 Color: 0
Size: 270602 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 468329 Color: 0
Size: 266940 Color: 1
Size: 264732 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 491179 Color: 0
Size: 255578 Color: 1
Size: 253244 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 483925 Color: 1
Size: 258886 Color: 0
Size: 257190 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 395368 Color: 1
Size: 317027 Color: 1
Size: 287606 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 448010 Color: 1
Size: 287039 Color: 0
Size: 264952 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 418656 Color: 1
Size: 321244 Color: 0
Size: 260101 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 414014 Color: 0
Size: 315694 Color: 1
Size: 270293 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 384122 Color: 0
Size: 354877 Color: 0
Size: 261002 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 456037 Color: 0
Size: 282127 Color: 1
Size: 261837 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 429312 Color: 0
Size: 287194 Color: 1
Size: 283495 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 448659 Color: 1
Size: 293677 Color: 0
Size: 257665 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 407286 Color: 0
Size: 341805 Color: 1
Size: 250910 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 465807 Color: 1
Size: 272634 Color: 0
Size: 261560 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 390605 Color: 0
Size: 307726 Color: 1
Size: 301670 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 456873 Color: 1
Size: 293011 Color: 0
Size: 250117 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 344783 Color: 0
Size: 341717 Color: 1
Size: 313501 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 489573 Color: 1
Size: 258484 Color: 1
Size: 251944 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 458507 Color: 0
Size: 278932 Color: 1
Size: 262562 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 363657 Color: 1
Size: 345249 Color: 0
Size: 291095 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 453063 Color: 0
Size: 296227 Color: 1
Size: 250711 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 419167 Color: 1
Size: 315708 Color: 0
Size: 265126 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 450267 Color: 0
Size: 292711 Color: 1
Size: 257023 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 478877 Color: 0
Size: 261903 Color: 1
Size: 259221 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 396670 Color: 1
Size: 328977 Color: 0
Size: 274354 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 486405 Color: 1
Size: 262734 Color: 0
Size: 250862 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 488388 Color: 1
Size: 258156 Color: 1
Size: 253457 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 393254 Color: 0
Size: 343636 Color: 1
Size: 263111 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 480264 Color: 1
Size: 269042 Color: 1
Size: 250695 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 488665 Color: 0
Size: 256089 Color: 1
Size: 255247 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 397455 Color: 0
Size: 331296 Color: 1
Size: 271250 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 409170 Color: 1
Size: 336733 Color: 1
Size: 254098 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 393645 Color: 1
Size: 326858 Color: 0
Size: 279498 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 406508 Color: 1
Size: 341418 Color: 0
Size: 252075 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 364250 Color: 1
Size: 327163 Color: 0
Size: 308588 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 499259 Color: 1
Size: 250693 Color: 0
Size: 250049 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 383207 Color: 0
Size: 329826 Color: 1
Size: 286968 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 432079 Color: 1
Size: 304834 Color: 0
Size: 263088 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 453771 Color: 0
Size: 274074 Color: 0
Size: 272156 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 387201 Color: 0
Size: 350459 Color: 0
Size: 262341 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 489730 Color: 1
Size: 257088 Color: 0
Size: 253183 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 463006 Color: 1
Size: 281291 Color: 1
Size: 255704 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 430952 Color: 1
Size: 313017 Color: 0
Size: 256032 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 356797 Color: 0
Size: 356659 Color: 1
Size: 286545 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 471592 Color: 1
Size: 269555 Color: 0
Size: 258854 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 435146 Color: 1
Size: 296222 Color: 1
Size: 268633 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 407869 Color: 0
Size: 299688 Color: 0
Size: 292444 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 459676 Color: 0
Size: 273219 Color: 1
Size: 267106 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 426493 Color: 1
Size: 315577 Color: 0
Size: 257931 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 466813 Color: 0
Size: 281295 Color: 1
Size: 251893 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 483353 Color: 1
Size: 264723 Color: 0
Size: 251925 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 430185 Color: 0
Size: 293472 Color: 1
Size: 276344 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 458261 Color: 1
Size: 290013 Color: 1
Size: 251727 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 457691 Color: 0
Size: 284162 Color: 0
Size: 258148 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 394182 Color: 0
Size: 325049 Color: 0
Size: 280770 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 392811 Color: 1
Size: 322953 Color: 0
Size: 284237 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 342418 Color: 0
Size: 333427 Color: 0
Size: 324156 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 393424 Color: 0
Size: 350367 Color: 1
Size: 256210 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 400859 Color: 1
Size: 333585 Color: 0
Size: 265557 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 422412 Color: 1
Size: 315983 Color: 0
Size: 261606 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 400357 Color: 0
Size: 318417 Color: 1
Size: 281227 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 362355 Color: 0
Size: 320544 Color: 1
Size: 317102 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 478878 Color: 0
Size: 267763 Color: 1
Size: 253360 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 422520 Color: 1
Size: 291488 Color: 1
Size: 285993 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 413788 Color: 0
Size: 296696 Color: 1
Size: 289517 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 444858 Color: 0
Size: 301083 Color: 0
Size: 254060 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 491997 Color: 0
Size: 255035 Color: 1
Size: 252969 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 407290 Color: 0
Size: 318774 Color: 1
Size: 273937 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 467906 Color: 1
Size: 268812 Color: 0
Size: 263283 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 416553 Color: 0
Size: 316133 Color: 1
Size: 267315 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 408098 Color: 0
Size: 340417 Color: 1
Size: 251486 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 399547 Color: 0
Size: 342095 Color: 0
Size: 258359 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 428985 Color: 1
Size: 307539 Color: 0
Size: 263477 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 371856 Color: 1
Size: 351460 Color: 1
Size: 276685 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 445291 Color: 0
Size: 299769 Color: 1
Size: 254941 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 385810 Color: 0
Size: 332547 Color: 1
Size: 281644 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 440838 Color: 1
Size: 303810 Color: 0
Size: 255353 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 382550 Color: 1
Size: 360358 Color: 0
Size: 257093 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 366478 Color: 0
Size: 352850 Color: 0
Size: 280673 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 407341 Color: 0
Size: 330783 Color: 1
Size: 261877 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 350704 Color: 0
Size: 330357 Color: 1
Size: 318940 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 476131 Color: 0
Size: 263550 Color: 1
Size: 260320 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 352852 Color: 1
Size: 332177 Color: 0
Size: 314972 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 347119 Color: 0
Size: 329888 Color: 0
Size: 322994 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 402920 Color: 0
Size: 311271 Color: 1
Size: 285810 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 384364 Color: 0
Size: 326497 Color: 1
Size: 289140 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 381924 Color: 1
Size: 311951 Color: 1
Size: 306126 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 386028 Color: 0
Size: 324765 Color: 1
Size: 289208 Color: 1

Total size: 167000167
Total free space: 0

