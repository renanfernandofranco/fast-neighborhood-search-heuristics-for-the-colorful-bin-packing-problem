Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 331 Color: 2
Size: 253 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 340 Color: 0
Size: 256 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 251 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 4
Size: 280 Color: 0
Size: 276 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 3
Size: 328 Color: 1
Size: 309 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 306 Color: 2
Size: 289 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 336 Color: 3
Size: 311 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 2
Size: 298 Color: 3
Size: 259 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 313 Color: 3
Size: 282 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 4
Size: 316 Color: 2
Size: 251 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 329 Color: 3
Size: 269 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 319 Color: 3
Size: 267 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 365 Color: 3
Size: 262 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 287 Color: 4
Size: 260 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 258 Color: 3
Size: 256 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 350 Color: 1
Size: 260 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 3
Size: 257 Color: 4
Size: 254 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 258 Color: 3
Size: 256 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 309 Color: 4
Size: 273 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 309 Color: 2
Size: 305 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 254 Color: 1
Size: 250 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 271 Color: 0
Size: 259 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 356 Color: 1
Size: 271 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 289 Color: 3
Size: 261 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 294 Color: 0
Size: 261 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 2
Size: 329 Color: 4
Size: 302 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 290 Color: 4
Size: 289 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 3
Size: 336 Color: 2
Size: 252 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 302 Color: 4
Size: 280 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 332 Color: 0
Size: 284 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 358 Color: 3
Size: 254 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 283 Color: 1
Size: 275 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 2
Size: 357 Color: 2
Size: 261 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 250 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 350 Color: 2
Size: 254 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 313 Color: 4
Size: 257 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 4
Size: 266 Color: 1
Size: 264 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 253 Color: 2
Size: 251 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 343 Color: 4
Size: 273 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 265 Color: 2
Size: 251 Color: 1

Total size: 40000
Total free space: 0

