Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9380 Color: 424
Size: 5640 Color: 378
Size: 528 Color: 105
Size: 372 Color: 59
Size: 368 Color: 58

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 9396 Color: 425
Size: 5708 Color: 379
Size: 464 Color: 86
Size: 360 Color: 56
Size: 360 Color: 54

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 9412 Color: 426
Size: 5732 Color: 380
Size: 432 Color: 80
Size: 360 Color: 53
Size: 352 Color: 52

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 469
Size: 3564 Color: 336
Size: 720 Color: 141

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 477
Size: 3560 Color: 335
Size: 544 Color: 109

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12520 Color: 484
Size: 3432 Color: 327
Size: 336 Color: 46

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12730 Color: 490
Size: 2746 Color: 304
Size: 812 Color: 153

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12745 Color: 491
Size: 2911 Color: 310
Size: 632 Color: 126

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 496
Size: 2224 Color: 272
Size: 1160 Color: 185

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 497
Size: 1964 Color: 248
Size: 1404 Color: 207

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 498
Size: 2916 Color: 311
Size: 406 Color: 69

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13010 Color: 501
Size: 1882 Color: 244
Size: 1396 Color: 206

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 502
Size: 2120 Color: 264
Size: 1152 Color: 183

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13114 Color: 506
Size: 1996 Color: 253
Size: 1178 Color: 189

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 510
Size: 2824 Color: 307
Size: 288 Color: 24

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13220 Color: 511
Size: 2060 Color: 260
Size: 1008 Color: 173

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 512
Size: 2568 Color: 294
Size: 496 Color: 95

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 515
Size: 2490 Color: 289
Size: 496 Color: 97

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 519
Size: 2424 Color: 284
Size: 480 Color: 91

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13480 Color: 522
Size: 1980 Color: 251
Size: 828 Color: 155

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13487 Color: 523
Size: 2497 Color: 290
Size: 304 Color: 32

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 524
Size: 2414 Color: 282
Size: 384 Color: 61

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13686 Color: 534
Size: 2278 Color: 274
Size: 324 Color: 42

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 536
Size: 2066 Color: 261
Size: 480 Color: 92

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 537
Size: 1372 Color: 204
Size: 1168 Color: 187

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 538
Size: 1896 Color: 245
Size: 640 Color: 127

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13810 Color: 540
Size: 1974 Color: 250
Size: 504 Color: 99

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 541
Size: 1628 Color: 226
Size: 840 Color: 158

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 548
Size: 1524 Color: 216
Size: 864 Color: 159

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 549
Size: 2076 Color: 262
Size: 296 Color: 27

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13922 Color: 550
Size: 1986 Color: 252
Size: 380 Color: 60

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13931 Color: 551
Size: 1549 Color: 220
Size: 808 Color: 152

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 555
Size: 1606 Color: 222
Size: 696 Color: 136

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 556
Size: 1352 Color: 198
Size: 912 Color: 167

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14104 Color: 560
Size: 1704 Color: 233
Size: 480 Color: 90

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 561
Size: 1604 Color: 221
Size: 576 Color: 115

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 564
Size: 1344 Color: 196
Size: 804 Color: 151

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 567
Size: 2086 Color: 263
Size: 8 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14255 Color: 570
Size: 1669 Color: 228
Size: 364 Color: 57

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 572
Size: 1356 Color: 202
Size: 664 Color: 130

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 577
Size: 1160 Color: 186
Size: 772 Color: 146

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14362 Color: 578
Size: 1624 Color: 225
Size: 302 Color: 28

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14376 Color: 580
Size: 1608 Color: 223
Size: 304 Color: 30

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 581
Size: 1498 Color: 214
Size: 360 Color: 55

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14431 Color: 582
Size: 1517 Color: 215
Size: 340 Color: 49

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14449 Color: 584
Size: 1533 Color: 218
Size: 306 Color: 35

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 588
Size: 1386 Color: 205
Size: 408 Color: 71

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14501 Color: 589
Size: 1491 Color: 213
Size: 296 Color: 26

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 590
Size: 1144 Color: 182
Size: 612 Color: 123

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 591
Size: 1532 Color: 217
Size: 220 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 592
Size: 1443 Color: 211
Size: 288 Color: 25

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14620 Color: 597
Size: 1364 Color: 203
Size: 304 Color: 33

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14626 Color: 598
Size: 902 Color: 164
Size: 760 Color: 144

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 600
Size: 1172 Color: 188
Size: 464 Color: 87

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12334 Color: 479
Size: 3433 Color: 328
Size: 520 Color: 104

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 481
Size: 3069 Color: 317
Size: 784 Color: 148

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 488
Size: 2759 Color: 305
Size: 880 Color: 161

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 531
Size: 2564 Color: 293
Size: 76 Color: 2

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 14165 Color: 566
Size: 2122 Color: 265

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 14287 Color: 574
Size: 2000 Color: 254

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 14322 Color: 575
Size: 1965 Color: 249

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 14592 Color: 593
Size: 1695 Color: 232

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 596
Size: 1675 Color: 229

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 10835 Color: 445
Size: 5131 Color: 371
Size: 320 Color: 38

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 11410 Color: 457
Size: 4604 Color: 362
Size: 272 Color: 21

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 473
Size: 4036 Color: 344
Size: 140 Color: 7

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 12585 Color: 485
Size: 3467 Color: 330
Size: 234 Color: 12

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 13346 Color: 517
Size: 2940 Color: 313

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 525
Size: 2770 Color: 306

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13558 Color: 527
Size: 2728 Color: 302

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 539
Size: 2482 Color: 288

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 13890 Color: 547
Size: 2396 Color: 279

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 565
Size: 2124 Color: 266

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 14364 Color: 579
Size: 1922 Color: 246

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 14460 Color: 586
Size: 1826 Color: 240

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 14594 Color: 594
Size: 1692 Color: 231

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 14644 Color: 599
Size: 1642 Color: 227

Bin 78: 3 of cap free
Amount of items: 7
Items: 
Size: 8152 Color: 409
Size: 1771 Color: 235
Size: 1714 Color: 234
Size: 1612 Color: 224
Size: 1544 Color: 219
Size: 1084 Color: 177
Size: 408 Color: 70

Bin 79: 3 of cap free
Amount of items: 7
Items: 
Size: 8164 Color: 411
Size: 2411 Color: 281
Size: 2334 Color: 277
Size: 2044 Color: 258
Size: 544 Color: 108
Size: 396 Color: 67
Size: 392 Color: 66

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 483
Size: 3441 Color: 329
Size: 352 Color: 50

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 520
Size: 2888 Color: 309

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 13551 Color: 526
Size: 2734 Color: 303

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 528
Size: 2695 Color: 301

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 568
Size: 2051 Color: 259

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 14440 Color: 583
Size: 1845 Color: 243

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 459
Size: 4570 Color: 361
Size: 272 Color: 19

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 472
Size: 4046 Color: 347
Size: 160 Color: 8

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 508
Size: 3144 Color: 320

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 513
Size: 3048 Color: 316

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13360 Color: 518
Size: 2924 Color: 312

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 533
Size: 2628 Color: 297

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 543
Size: 2448 Color: 286

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 559
Size: 2201 Color: 270

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14114 Color: 562
Size: 2170 Color: 268

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 576
Size: 1940 Color: 247

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 585
Size: 1832 Color: 241

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 595
Size: 1684 Color: 230

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 12117 Color: 474
Size: 4166 Color: 351

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 13163 Color: 509
Size: 3120 Color: 318

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 13829 Color: 542
Size: 2454 Color: 287

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 569
Size: 2035 Color: 257

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 14469 Color: 587
Size: 1814 Color: 238

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 10438 Color: 439
Size: 5844 Color: 386

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 12402 Color: 480
Size: 3880 Color: 343

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13040 Color: 503
Size: 3242 Color: 323

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13866 Color: 545
Size: 2416 Color: 283

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 546
Size: 2402 Color: 280

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 558
Size: 2200 Color: 269

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 14260 Color: 571
Size: 2022 Color: 256

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 10133 Color: 433
Size: 5828 Color: 384
Size: 320 Color: 40

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 12476 Color: 482
Size: 3805 Color: 340

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 493
Size: 3469 Color: 331
Size: 32 Color: 1

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 13847 Color: 544
Size: 2434 Color: 285

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 14279 Color: 573
Size: 2002 Color: 255

Bin 115: 8 of cap free
Amount of items: 21
Items: 
Size: 984 Color: 172
Size: 984 Color: 171
Size: 984 Color: 170
Size: 960 Color: 169
Size: 920 Color: 168
Size: 912 Color: 166
Size: 908 Color: 165
Size: 896 Color: 163
Size: 882 Color: 162
Size: 868 Color: 160
Size: 840 Color: 157
Size: 832 Color: 156
Size: 820 Color: 154
Size: 800 Color: 150
Size: 800 Color: 149
Size: 592 Color: 121
Size: 480 Color: 89
Size: 476 Color: 88
Size: 452 Color: 84
Size: 448 Color: 83
Size: 442 Color: 82

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 10806 Color: 444
Size: 5474 Color: 377

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 12634 Color: 487
Size: 3646 Color: 338

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13314 Color: 516
Size: 2966 Color: 314

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 535
Size: 2548 Color: 292

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13964 Color: 554
Size: 2316 Color: 276

Bin 121: 9 of cap free
Amount of items: 9
Items: 
Size: 8147 Color: 407
Size: 1356 Color: 200
Size: 1352 Color: 199
Size: 1352 Color: 197
Size: 1344 Color: 195
Size: 1312 Color: 194
Size: 596 Color: 122
Size: 412 Color: 75
Size: 408 Color: 74

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 10378 Color: 437
Size: 5901 Color: 389

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 12797 Color: 495
Size: 3482 Color: 333

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 12994 Color: 500
Size: 3284 Color: 324

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 514
Size: 2994 Color: 315

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14034 Color: 557
Size: 2244 Color: 273

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 12979 Color: 499
Size: 3298 Color: 325

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 8180 Color: 412
Size: 8096 Color: 404

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 9284 Color: 420
Size: 6600 Color: 394
Size: 392 Color: 62

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 552
Size: 2344 Color: 278

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 14124 Color: 563
Size: 2152 Color: 267

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 10865 Color: 446
Size: 5410 Color: 374

Bin 133: 13 of cap free
Amount of items: 3
Items: 
Size: 11458 Color: 461
Size: 4545 Color: 360
Size: 272 Color: 17

Bin 134: 13 of cap free
Amount of items: 4
Items: 
Size: 12161 Color: 475
Size: 3866 Color: 341
Size: 128 Color: 6
Size: 120 Color: 5

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 492
Size: 3510 Color: 334

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 12796 Color: 494
Size: 3477 Color: 332

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 13627 Color: 530
Size: 2646 Color: 299

Bin 138: 16 of cap free
Amount of items: 7
Items: 
Size: 8154 Color: 410
Size: 1842 Color: 242
Size: 1820 Color: 239
Size: 1804 Color: 237
Size: 1796 Color: 236
Size: 456 Color: 85
Size: 400 Color: 68

Bin 139: 16 of cap free
Amount of items: 3
Items: 
Size: 10909 Color: 448
Size: 5055 Color: 370
Size: 308 Color: 36

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 13960 Color: 553
Size: 2312 Color: 275

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 13654 Color: 532
Size: 2617 Color: 296

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 489
Size: 3572 Color: 337

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 13596 Color: 529
Size: 2674 Color: 300

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 10406 Color: 438
Size: 5863 Color: 388

Bin 145: 19 of cap free
Amount of items: 2
Items: 
Size: 13055 Color: 504
Size: 3214 Color: 322

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 10504 Color: 440
Size: 5764 Color: 382

Bin 147: 21 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 450
Size: 4926 Color: 365
Size: 304 Color: 31

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 11256 Color: 454
Size: 5010 Color: 369

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 13410 Color: 521
Size: 2856 Color: 308

Bin 150: 23 of cap free
Amount of items: 2
Items: 
Size: 11914 Color: 468
Size: 4351 Color: 355

Bin 151: 25 of cap free
Amount of items: 2
Items: 
Size: 12197 Color: 478
Size: 4066 Color: 348

Bin 152: 25 of cap free
Amount of items: 2
Items: 
Size: 12607 Color: 486
Size: 3656 Color: 339

Bin 153: 26 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 505
Size: 3180 Color: 321

Bin 154: 26 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 507
Size: 3138 Color: 319

Bin 155: 27 of cap free
Amount of items: 3
Items: 
Size: 10993 Color: 449
Size: 4964 Color: 367
Size: 304 Color: 34

Bin 156: 29 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 434
Size: 3411 Color: 326
Size: 2508 Color: 291

Bin 157: 35 of cap free
Amount of items: 3
Items: 
Size: 11452 Color: 460
Size: 4529 Color: 359
Size: 272 Color: 18

Bin 158: 41 of cap free
Amount of items: 2
Items: 
Size: 11635 Color: 462
Size: 4612 Color: 363

Bin 159: 42 of cap free
Amount of items: 2
Items: 
Size: 11246 Color: 453
Size: 5000 Color: 368

Bin 160: 48 of cap free
Amount of items: 2
Items: 
Size: 11912 Color: 467
Size: 4328 Color: 354

Bin 161: 48 of cap free
Amount of items: 4
Items: 
Size: 12169 Color: 476
Size: 3879 Color: 342
Size: 96 Color: 4
Size: 96 Color: 3

Bin 162: 50 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 470
Size: 4042 Color: 345
Size: 176 Color: 10

Bin 163: 52 of cap free
Amount of items: 5
Items: 
Size: 8376 Color: 413
Size: 2644 Color: 298
Size: 2605 Color: 295
Size: 2219 Color: 271
Size: 392 Color: 65

Bin 164: 57 of cap free
Amount of items: 3
Items: 
Size: 10095 Color: 432
Size: 5812 Color: 383
Size: 324 Color: 41

Bin 165: 58 of cap free
Amount of items: 2
Items: 
Size: 11069 Color: 451
Size: 5161 Color: 372

Bin 166: 59 of cap free
Amount of items: 3
Items: 
Size: 11436 Color: 458
Size: 4521 Color: 358
Size: 272 Color: 20

Bin 167: 60 of cap free
Amount of items: 3
Items: 
Size: 9798 Color: 430
Size: 6096 Color: 393
Size: 334 Color: 44

Bin 168: 60 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 471
Size: 4044 Color: 346
Size: 160 Color: 9

Bin 169: 68 of cap free
Amount of items: 2
Items: 
Size: 10772 Color: 443
Size: 5448 Color: 376

Bin 170: 72 of cap free
Amount of items: 2
Items: 
Size: 10372 Color: 436
Size: 5844 Color: 387

Bin 171: 80 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 452
Size: 4808 Color: 364
Size: 304 Color: 29

Bin 172: 90 of cap free
Amount of items: 3
Items: 
Size: 9034 Color: 416
Size: 6772 Color: 396
Size: 392 Color: 63

Bin 173: 94 of cap free
Amount of items: 2
Items: 
Size: 10756 Color: 442
Size: 5438 Color: 375

Bin 174: 100 of cap free
Amount of items: 2
Items: 
Size: 10356 Color: 435
Size: 5832 Color: 385

Bin 175: 102 of cap free
Amount of items: 3
Items: 
Size: 9030 Color: 415
Size: 6764 Color: 395
Size: 392 Color: 64

Bin 176: 107 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 466
Size: 4202 Color: 353
Size: 256 Color: 13

Bin 177: 110 of cap free
Amount of items: 9
Items: 
Size: 8146 Color: 406
Size: 1280 Color: 193
Size: 1208 Color: 192
Size: 1208 Color: 191
Size: 1200 Color: 190
Size: 1160 Color: 184
Size: 1144 Color: 181
Size: 416 Color: 77
Size: 416 Color: 76

Bin 178: 128 of cap free
Amount of items: 2
Items: 
Size: 8992 Color: 414
Size: 7168 Color: 403

Bin 179: 130 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 447
Size: 4948 Color: 366
Size: 320 Color: 37

Bin 180: 136 of cap free
Amount of items: 3
Items: 
Size: 9766 Color: 429
Size: 6050 Color: 392
Size: 336 Color: 45

Bin 181: 136 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 431
Size: 5748 Color: 381
Size: 332 Color: 43

Bin 182: 150 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 465
Size: 4200 Color: 352
Size: 256 Color: 14

Bin 183: 154 of cap free
Amount of items: 4
Items: 
Size: 9444 Color: 427
Size: 6000 Color: 390
Size: 352 Color: 51
Size: 338 Color: 48

Bin 184: 154 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 428
Size: 6046 Color: 391
Size: 336 Color: 47

Bin 185: 156 of cap free
Amount of items: 3
Items: 
Size: 10520 Color: 441
Size: 5292 Color: 373
Size: 320 Color: 39

Bin 186: 164 of cap free
Amount of items: 3
Items: 
Size: 11346 Color: 456
Size: 4502 Color: 357
Size: 276 Color: 22

Bin 187: 180 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 423
Size: 6792 Color: 402

Bin 188: 196 of cap free
Amount of items: 2
Items: 
Size: 9304 Color: 422
Size: 6788 Color: 401

Bin 189: 201 of cap free
Amount of items: 2
Items: 
Size: 9300 Color: 421
Size: 6787 Color: 400

Bin 190: 226 of cap free
Amount of items: 2
Items: 
Size: 9276 Color: 419
Size: 6786 Color: 399

Bin 191: 228 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 464
Size: 4146 Color: 350
Size: 264 Color: 15

Bin 192: 234 of cap free
Amount of items: 8
Items: 
Size: 8148 Color: 408
Size: 1464 Color: 212
Size: 1440 Color: 210
Size: 1416 Color: 209
Size: 1414 Color: 208
Size: 1356 Color: 201
Size: 408 Color: 73
Size: 408 Color: 72

Bin 193: 250 of cap free
Amount of items: 2
Items: 
Size: 9253 Color: 418
Size: 6785 Color: 398

Bin 194: 254 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 463
Size: 4122 Color: 349
Size: 272 Color: 16

Bin 195: 281 of cap free
Amount of items: 3
Items: 
Size: 11314 Color: 455
Size: 4413 Color: 356
Size: 280 Color: 23

Bin 196: 290 of cap free
Amount of items: 26
Items: 
Size: 774 Color: 147
Size: 768 Color: 145
Size: 728 Color: 143
Size: 722 Color: 142
Size: 712 Color: 140
Size: 704 Color: 139
Size: 704 Color: 138
Size: 700 Color: 137
Size: 694 Color: 135
Size: 686 Color: 134
Size: 686 Color: 133
Size: 680 Color: 132
Size: 672 Color: 131
Size: 552 Color: 112
Size: 550 Color: 111
Size: 548 Color: 110
Size: 538 Color: 107
Size: 532 Color: 106
Size: 520 Color: 103
Size: 520 Color: 102
Size: 516 Color: 101
Size: 512 Color: 100
Size: 504 Color: 98
Size: 496 Color: 96
Size: 492 Color: 94
Size: 488 Color: 93

Bin 197: 297 of cap free
Amount of items: 2
Items: 
Size: 9209 Color: 417
Size: 6782 Color: 397

Bin 198: 343 of cap free
Amount of items: 10
Items: 
Size: 8145 Color: 405
Size: 1144 Color: 180
Size: 1136 Color: 179
Size: 1088 Color: 178
Size: 1080 Color: 176
Size: 1032 Color: 175
Size: 1024 Color: 174
Size: 440 Color: 81
Size: 432 Color: 79
Size: 424 Color: 78

Bin 199: 9704 of cap free
Amount of items: 11
Items: 
Size: 656 Color: 129
Size: 644 Color: 128
Size: 624 Color: 125
Size: 624 Color: 124
Size: 592 Color: 120
Size: 584 Color: 119
Size: 584 Color: 118
Size: 580 Color: 117
Size: 576 Color: 116
Size: 560 Color: 114
Size: 560 Color: 113

Total size: 3225024
Total free space: 16288

