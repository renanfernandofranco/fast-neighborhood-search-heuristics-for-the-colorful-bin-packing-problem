Capicity Bin: 15200
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7640 Color: 3
Size: 7208 Color: 4
Size: 352 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 0
Size: 4648 Color: 4
Size: 336 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10410 Color: 2
Size: 3466 Color: 1
Size: 1324 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 4
Size: 4074 Color: 0
Size: 692 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 0
Size: 3598 Color: 2
Size: 576 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10994 Color: 3
Size: 3506 Color: 0
Size: 700 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 2
Size: 3854 Color: 4
Size: 312 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11047 Color: 4
Size: 3461 Color: 3
Size: 692 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11044 Color: 0
Size: 3468 Color: 1
Size: 688 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11276 Color: 0
Size: 3482 Color: 1
Size: 442 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 2
Size: 3356 Color: 0
Size: 520 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 0
Size: 2844 Color: 1
Size: 576 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 3
Size: 2837 Color: 2
Size: 566 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 1
Size: 2918 Color: 0
Size: 290 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 4
Size: 2132 Color: 2
Size: 960 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12160 Color: 2
Size: 2580 Color: 0
Size: 460 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12170 Color: 1
Size: 1954 Color: 0
Size: 1076 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 2568 Color: 4
Size: 448 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 0
Size: 2226 Color: 1
Size: 720 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 4
Size: 2520 Color: 1
Size: 400 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12372 Color: 3
Size: 2196 Color: 1
Size: 632 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 0
Size: 2314 Color: 1
Size: 488 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12426 Color: 1
Size: 2436 Color: 0
Size: 338 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 1
Size: 2492 Color: 0
Size: 280 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 0
Size: 1448 Color: 3
Size: 1264 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12521 Color: 0
Size: 2535 Color: 1
Size: 144 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12528 Color: 0
Size: 1800 Color: 4
Size: 872 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12530 Color: 3
Size: 2206 Color: 2
Size: 464 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 2
Size: 2364 Color: 1
Size: 264 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12605 Color: 0
Size: 1971 Color: 4
Size: 624 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 3
Size: 1880 Color: 3
Size: 672 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 0
Size: 1764 Color: 1
Size: 644 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 4
Size: 1610 Color: 0
Size: 792 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12828 Color: 2
Size: 1388 Color: 3
Size: 984 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 0
Size: 1838 Color: 3
Size: 504 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12911 Color: 0
Size: 1569 Color: 2
Size: 720 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 2
Size: 1888 Color: 3
Size: 392 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 2
Size: 1900 Color: 2
Size: 348 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12998 Color: 1
Size: 1486 Color: 0
Size: 716 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 2
Size: 1532 Color: 4
Size: 648 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13091 Color: 0
Size: 1461 Color: 4
Size: 648 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13084 Color: 4
Size: 1912 Color: 0
Size: 204 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13139 Color: 3
Size: 1719 Color: 0
Size: 342 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13183 Color: 0
Size: 1759 Color: 1
Size: 258 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13236 Color: 3
Size: 1644 Color: 0
Size: 320 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13319 Color: 0
Size: 1481 Color: 2
Size: 400 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 1460 Color: 1
Size: 424 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 2
Size: 1048 Color: 0
Size: 816 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 0
Size: 946 Color: 2
Size: 880 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 4
Size: 1232 Color: 0
Size: 580 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13425 Color: 1
Size: 1443 Color: 3
Size: 332 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 0
Size: 1340 Color: 4
Size: 408 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13462 Color: 2
Size: 1434 Color: 2
Size: 304 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 0
Size: 1464 Color: 2
Size: 272 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13482 Color: 4
Size: 1406 Color: 0
Size: 312 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13557 Color: 0
Size: 1371 Color: 4
Size: 272 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 4
Size: 944 Color: 0
Size: 716 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13594 Color: 3
Size: 1302 Color: 1
Size: 304 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 0
Size: 1284 Color: 4
Size: 320 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 0
Size: 976 Color: 4
Size: 564 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13629 Color: 3
Size: 1311 Color: 2
Size: 260 Color: 0

Bin 62: 1 of cap free
Amount of items: 4
Items: 
Size: 7603 Color: 1
Size: 4168 Color: 0
Size: 2458 Color: 2
Size: 970 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 9377 Color: 3
Size: 4306 Color: 0
Size: 1516 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 9445 Color: 1
Size: 4922 Color: 0
Size: 832 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 9529 Color: 4
Size: 5320 Color: 0
Size: 350 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 10034 Color: 1
Size: 2852 Color: 4
Size: 2313 Color: 4

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 10283 Color: 1
Size: 4916 Color: 2

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 10418 Color: 3
Size: 4781 Color: 4

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 10647 Color: 3
Size: 3986 Color: 0
Size: 566 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 10667 Color: 1
Size: 4244 Color: 2
Size: 288 Color: 4

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 10873 Color: 0
Size: 3466 Color: 2
Size: 860 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 10890 Color: 3
Size: 3885 Color: 0
Size: 424 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 11325 Color: 1
Size: 3422 Color: 3
Size: 452 Color: 0

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 11496 Color: 0
Size: 2181 Color: 3
Size: 1522 Color: 3

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 11538 Color: 2
Size: 2563 Color: 0
Size: 1098 Color: 2

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 11974 Color: 1
Size: 2841 Color: 0
Size: 384 Color: 3

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 12177 Color: 4
Size: 2882 Color: 2
Size: 140 Color: 4

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 2
Size: 2266 Color: 0
Size: 368 Color: 1

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 12692 Color: 3
Size: 2507 Color: 4

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 12986 Color: 3
Size: 2213 Color: 2

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 1
Size: 1428 Color: 2
Size: 608 Color: 0

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 4
Size: 1497 Color: 0
Size: 514 Color: 3

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 13651 Color: 1
Size: 1104 Color: 4
Size: 444 Color: 0

Bin 84: 2 of cap free
Amount of items: 7
Items: 
Size: 7604 Color: 2
Size: 1598 Color: 4
Size: 1494 Color: 4
Size: 1342 Color: 2
Size: 1112 Color: 1
Size: 1064 Color: 1
Size: 984 Color: 4

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 8526 Color: 0
Size: 6672 Color: 1

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 9472 Color: 1
Size: 5566 Color: 3
Size: 160 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 3
Size: 2864 Color: 4
Size: 2028 Color: 1

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 4
Size: 3974 Color: 1
Size: 848 Color: 0

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 10986 Color: 0
Size: 3820 Color: 1
Size: 392 Color: 3

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 11098 Color: 1
Size: 3684 Color: 0
Size: 416 Color: 1

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11471 Color: 3
Size: 3231 Color: 0
Size: 496 Color: 2

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 11702 Color: 0
Size: 2440 Color: 2
Size: 1056 Color: 1

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 4
Size: 2968 Color: 4
Size: 434 Color: 0

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 12734 Color: 0
Size: 2064 Color: 1
Size: 400 Color: 2

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 3
Size: 2008 Color: 1

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 13514 Color: 4
Size: 1684 Color: 1

Bin 97: 2 of cap free
Amount of items: 2
Items: 
Size: 13638 Color: 4
Size: 1560 Color: 1

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 2
Size: 5501 Color: 4
Size: 532 Color: 1

Bin 99: 3 of cap free
Amount of items: 2
Items: 
Size: 9640 Color: 1
Size: 5557 Color: 4

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 10205 Color: 0
Size: 4784 Color: 1
Size: 208 Color: 0

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 0
Size: 3096 Color: 2
Size: 380 Color: 3

Bin 102: 3 of cap free
Amount of items: 2
Items: 
Size: 12545 Color: 4
Size: 2652 Color: 3

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 8532 Color: 1
Size: 6312 Color: 2
Size: 352 Color: 3

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 9294 Color: 0
Size: 4926 Color: 0
Size: 976 Color: 2

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 9290 Color: 2
Size: 5564 Color: 1
Size: 342 Color: 3

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 4
Size: 4904 Color: 0
Size: 984 Color: 4

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 10042 Color: 1
Size: 4394 Color: 4
Size: 760 Color: 3

Bin 108: 4 of cap free
Amount of items: 2
Items: 
Size: 10882 Color: 4
Size: 4314 Color: 2

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 12109 Color: 0
Size: 2823 Color: 3
Size: 264 Color: 4

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 1
Size: 1940 Color: 4

Bin 111: 4 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 2
Size: 1566 Color: 3
Size: 20 Color: 3

Bin 112: 5 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 0
Size: 6334 Color: 2
Size: 260 Color: 3

Bin 113: 5 of cap free
Amount of items: 3
Items: 
Size: 11701 Color: 0
Size: 2126 Color: 3
Size: 1368 Color: 2

Bin 114: 5 of cap free
Amount of items: 2
Items: 
Size: 12585 Color: 2
Size: 2610 Color: 0

Bin 115: 6 of cap free
Amount of items: 21
Items: 
Size: 1096 Color: 2
Size: 1016 Color: 3
Size: 944 Color: 4
Size: 876 Color: 4
Size: 860 Color: 2
Size: 800 Color: 3
Size: 796 Color: 2
Size: 796 Color: 2
Size: 796 Color: 1
Size: 736 Color: 2
Size: 728 Color: 2
Size: 704 Color: 2
Size: 704 Color: 0
Size: 700 Color: 1
Size: 692 Color: 0
Size: 680 Color: 1
Size: 640 Color: 1
Size: 528 Color: 0
Size: 512 Color: 0
Size: 296 Color: 4
Size: 294 Color: 0

Bin 116: 6 of cap free
Amount of items: 2
Items: 
Size: 11112 Color: 4
Size: 4082 Color: 2

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 1
Size: 3602 Color: 3
Size: 160 Color: 4

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 13448 Color: 4
Size: 1681 Color: 2
Size: 64 Color: 0

Bin 119: 8 of cap free
Amount of items: 5
Items: 
Size: 7606 Color: 3
Size: 3474 Color: 4
Size: 1846 Color: 0
Size: 1266 Color: 0
Size: 1000 Color: 1

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 9465 Color: 1
Size: 5727 Color: 3

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 1
Size: 4386 Color: 3
Size: 876 Color: 2

Bin 122: 8 of cap free
Amount of items: 3
Items: 
Size: 10780 Color: 0
Size: 4024 Color: 0
Size: 388 Color: 4

Bin 123: 8 of cap free
Amount of items: 2
Items: 
Size: 12876 Color: 1
Size: 2316 Color: 3

Bin 124: 8 of cap free
Amount of items: 2
Items: 
Size: 13029 Color: 1
Size: 2163 Color: 3

Bin 125: 8 of cap free
Amount of items: 2
Items: 
Size: 13620 Color: 2
Size: 1572 Color: 1

Bin 126: 9 of cap free
Amount of items: 2
Items: 
Size: 13492 Color: 3
Size: 1699 Color: 2

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 10904 Color: 4
Size: 4285 Color: 3

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 12837 Color: 2
Size: 2352 Color: 3

Bin 129: 11 of cap free
Amount of items: 3
Items: 
Size: 13445 Color: 1
Size: 1672 Color: 2
Size: 72 Color: 0

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 0
Size: 4380 Color: 3
Size: 912 Color: 2

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 12924 Color: 1
Size: 2264 Color: 2

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 13208 Color: 2
Size: 1980 Color: 3

Bin 133: 13 of cap free
Amount of items: 3
Items: 
Size: 10044 Color: 4
Size: 4727 Color: 0
Size: 416 Color: 1

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 13094 Color: 1
Size: 2092 Color: 4

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 12284 Color: 3
Size: 2901 Color: 2

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 0
Size: 6331 Color: 4
Size: 320 Color: 1

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 2
Size: 2136 Color: 3

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 13364 Color: 1
Size: 1820 Color: 2

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 13469 Color: 3
Size: 1715 Color: 2

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 1
Size: 1624 Color: 3

Bin 141: 17 of cap free
Amount of items: 3
Items: 
Size: 7804 Color: 2
Size: 6523 Color: 2
Size: 856 Color: 4

Bin 142: 18 of cap free
Amount of items: 3
Items: 
Size: 9292 Color: 3
Size: 5562 Color: 2
Size: 328 Color: 1

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 3
Size: 2643 Color: 1
Size: 112 Color: 2

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 12070 Color: 4
Size: 3109 Color: 2

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 4
Size: 1909 Color: 2

Bin 146: 23 of cap free
Amount of items: 2
Items: 
Size: 9513 Color: 4
Size: 5664 Color: 1

Bin 147: 24 of cap free
Amount of items: 2
Items: 
Size: 9948 Color: 2
Size: 5228 Color: 3

Bin 148: 24 of cap free
Amount of items: 2
Items: 
Size: 12650 Color: 0
Size: 2526 Color: 4

Bin 149: 24 of cap free
Amount of items: 2
Items: 
Size: 13418 Color: 1
Size: 1758 Color: 4

Bin 150: 25 of cap free
Amount of items: 3
Items: 
Size: 9068 Color: 2
Size: 5487 Color: 3
Size: 620 Color: 0

Bin 151: 26 of cap free
Amount of items: 2
Items: 
Size: 11180 Color: 4
Size: 3994 Color: 1

Bin 152: 26 of cap free
Amount of items: 2
Items: 
Size: 12120 Color: 1
Size: 3054 Color: 4

Bin 153: 27 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 3
Size: 4163 Color: 2

Bin 154: 27 of cap free
Amount of items: 2
Items: 
Size: 12193 Color: 2
Size: 2980 Color: 4

Bin 155: 27 of cap free
Amount of items: 2
Items: 
Size: 12493 Color: 4
Size: 2680 Color: 1

Bin 156: 28 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 4
Size: 5340 Color: 1
Size: 812 Color: 0

Bin 157: 30 of cap free
Amount of items: 4
Items: 
Size: 7608 Color: 0
Size: 3594 Color: 4
Size: 3592 Color: 4
Size: 376 Color: 3

Bin 158: 30 of cap free
Amount of items: 3
Items: 
Size: 8617 Color: 2
Size: 6329 Color: 4
Size: 224 Color: 0

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 10620 Color: 3
Size: 4550 Color: 0

Bin 160: 30 of cap free
Amount of items: 2
Items: 
Size: 11530 Color: 4
Size: 3640 Color: 1

Bin 161: 30 of cap free
Amount of items: 2
Items: 
Size: 11656 Color: 1
Size: 3514 Color: 4

Bin 162: 32 of cap free
Amount of items: 8
Items: 
Size: 7601 Color: 2
Size: 1291 Color: 3
Size: 1264 Color: 3
Size: 1264 Color: 0
Size: 1264 Color: 0
Size: 1248 Color: 4
Size: 952 Color: 1
Size: 284 Color: 2

Bin 163: 33 of cap free
Amount of items: 2
Items: 
Size: 10314 Color: 2
Size: 4853 Color: 4

Bin 164: 34 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 1
Size: 4924 Color: 4
Size: 304 Color: 0

Bin 165: 35 of cap free
Amount of items: 3
Items: 
Size: 11793 Color: 1
Size: 3276 Color: 4
Size: 96 Color: 3

Bin 166: 35 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 2
Size: 2521 Color: 4

Bin 167: 36 of cap free
Amount of items: 3
Items: 
Size: 8522 Color: 4
Size: 6330 Color: 2
Size: 312 Color: 4

Bin 168: 36 of cap free
Amount of items: 2
Items: 
Size: 12020 Color: 4
Size: 3144 Color: 3

Bin 169: 38 of cap free
Amount of items: 3
Items: 
Size: 7612 Color: 2
Size: 5260 Color: 1
Size: 2290 Color: 0

Bin 170: 42 of cap free
Amount of items: 2
Items: 
Size: 11742 Color: 3
Size: 3416 Color: 1

Bin 171: 45 of cap free
Amount of items: 3
Items: 
Size: 13449 Color: 2
Size: 1678 Color: 3
Size: 28 Color: 2

Bin 172: 50 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 2
Size: 3522 Color: 1

Bin 173: 52 of cap free
Amount of items: 3
Items: 
Size: 10978 Color: 2
Size: 4002 Color: 4
Size: 168 Color: 2

Bin 174: 54 of cap free
Amount of items: 2
Items: 
Size: 12890 Color: 1
Size: 2256 Color: 4

Bin 175: 55 of cap free
Amount of items: 3
Items: 
Size: 11813 Color: 4
Size: 3236 Color: 2
Size: 96 Color: 4

Bin 176: 55 of cap free
Amount of items: 2
Items: 
Size: 13143 Color: 1
Size: 2002 Color: 2

Bin 177: 56 of cap free
Amount of items: 2
Items: 
Size: 10108 Color: 0
Size: 5036 Color: 3

Bin 178: 57 of cap free
Amount of items: 2
Items: 
Size: 7607 Color: 4
Size: 7536 Color: 0

Bin 179: 57 of cap free
Amount of items: 2
Items: 
Size: 10402 Color: 3
Size: 4741 Color: 4

Bin 180: 58 of cap free
Amount of items: 2
Items: 
Size: 10026 Color: 3
Size: 5116 Color: 4

Bin 181: 60 of cap free
Amount of items: 2
Items: 
Size: 10716 Color: 0
Size: 4424 Color: 3

Bin 182: 60 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 2
Size: 4300 Color: 1

Bin 183: 61 of cap free
Amount of items: 2
Items: 
Size: 12445 Color: 1
Size: 2694 Color: 4

Bin 184: 64 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 3
Size: 5816 Color: 1

Bin 185: 67 of cap free
Amount of items: 2
Items: 
Size: 13322 Color: 3
Size: 1811 Color: 1

Bin 186: 69 of cap free
Amount of items: 2
Items: 
Size: 12554 Color: 1
Size: 2577 Color: 3

Bin 187: 71 of cap free
Amount of items: 2
Items: 
Size: 12212 Color: 4
Size: 2917 Color: 2

Bin 188: 72 of cap free
Amount of items: 2
Items: 
Size: 8796 Color: 0
Size: 6332 Color: 3

Bin 189: 75 of cap free
Amount of items: 2
Items: 
Size: 12482 Color: 4
Size: 2643 Color: 1

Bin 190: 90 of cap free
Amount of items: 9
Items: 
Size: 7602 Color: 3
Size: 1216 Color: 4
Size: 1152 Color: 3
Size: 1112 Color: 4
Size: 1110 Color: 2
Size: 954 Color: 0
Size: 832 Color: 1
Size: 812 Color: 1
Size: 320 Color: 2

Bin 191: 90 of cap free
Amount of items: 3
Items: 
Size: 8824 Color: 3
Size: 4228 Color: 0
Size: 2058 Color: 2

Bin 192: 90 of cap free
Amount of items: 2
Items: 
Size: 12772 Color: 0
Size: 2338 Color: 3

Bin 193: 91 of cap free
Amount of items: 2
Items: 
Size: 11050 Color: 3
Size: 4059 Color: 2

Bin 194: 102 of cap free
Amount of items: 31
Items: 
Size: 700 Color: 4
Size: 692 Color: 2
Size: 664 Color: 3
Size: 608 Color: 3
Size: 608 Color: 3
Size: 592 Color: 4
Size: 582 Color: 1
Size: 578 Color: 2
Size: 568 Color: 1
Size: 560 Color: 4
Size: 528 Color: 4
Size: 512 Color: 4
Size: 502 Color: 2
Size: 500 Color: 2
Size: 496 Color: 2
Size: 494 Color: 4
Size: 480 Color: 3
Size: 462 Color: 0
Size: 456 Color: 1
Size: 440 Color: 1
Size: 432 Color: 3
Size: 432 Color: 3
Size: 416 Color: 1
Size: 384 Color: 0
Size: 368 Color: 0
Size: 368 Color: 0
Size: 364 Color: 1
Size: 360 Color: 0
Size: 360 Color: 0
Size: 296 Color: 3
Size: 296 Color: 1

Bin 195: 108 of cap free
Amount of items: 2
Items: 
Size: 8232 Color: 3
Size: 6860 Color: 4

Bin 196: 128 of cap free
Amount of items: 2
Items: 
Size: 11042 Color: 4
Size: 4030 Color: 1

Bin 197: 144 of cap free
Amount of items: 2
Items: 
Size: 8892 Color: 1
Size: 6164 Color: 4

Bin 198: 159 of cap free
Amount of items: 2
Items: 
Size: 11434 Color: 3
Size: 3607 Color: 1

Bin 199: 11814 of cap free
Amount of items: 11
Items: 
Size: 480 Color: 2
Size: 464 Color: 4
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 280 Color: 3
Size: 272 Color: 0
Size: 258 Color: 2
Size: 256 Color: 3
Size: 256 Color: 1
Size: 256 Color: 0

Total size: 3009600
Total free space: 15200

