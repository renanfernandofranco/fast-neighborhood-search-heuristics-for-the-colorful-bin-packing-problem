Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 56
Size: 255 Color: 8
Size: 253 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 54
Size: 278 Color: 19
Size: 252 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 40
Size: 370 Color: 39
Size: 258 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 55
Size: 266 Color: 13
Size: 253 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 59
Size: 253 Color: 5
Size: 252 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 58
Size: 256 Color: 9
Size: 251 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 46
Size: 352 Color: 37
Size: 256 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 43
Size: 331 Color: 33
Size: 284 Color: 24

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 57
Size: 257 Color: 11
Size: 251 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 47
Size: 327 Color: 32
Size: 278 Color: 20

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 41
Size: 369 Color: 38
Size: 253 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 45
Size: 313 Color: 30
Size: 296 Color: 27

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 49
Size: 307 Color: 28
Size: 295 Color: 26

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 50
Size: 313 Color: 29
Size: 288 Color: 25

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 42
Size: 352 Color: 36
Size: 267 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 52
Size: 283 Color: 22
Size: 270 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 51
Size: 323 Color: 31
Size: 268 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 44
Size: 331 Color: 34
Size: 280 Color: 21

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 48
Size: 336 Color: 35
Size: 268 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 53
Size: 284 Color: 23
Size: 266 Color: 14

Total size: 20000
Total free space: 0

