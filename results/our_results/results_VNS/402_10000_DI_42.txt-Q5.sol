Capicity Bin: 6624
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 25
Items: 
Size: 440 Color: 0
Size: 392 Color: 0
Size: 380 Color: 3
Size: 376 Color: 0
Size: 332 Color: 0
Size: 328 Color: 2
Size: 306 Color: 3
Size: 296 Color: 3
Size: 292 Color: 1
Size: 272 Color: 3
Size: 272 Color: 0
Size: 256 Color: 4
Size: 254 Color: 2
Size: 252 Color: 1
Size: 234 Color: 4
Size: 232 Color: 4
Size: 224 Color: 1
Size: 216 Color: 3
Size: 214 Color: 4
Size: 196 Color: 0
Size: 194 Color: 3
Size: 180 Color: 4
Size: 176 Color: 3
Size: 160 Color: 2
Size: 150 Color: 4

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 3264 Color: 2
Size: 550 Color: 0
Size: 482 Color: 1
Size: 456 Color: 1
Size: 456 Color: 0
Size: 440 Color: 1
Size: 424 Color: 3
Size: 384 Color: 3
Size: 168 Color: 4

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 3321 Color: 2
Size: 2085 Color: 2
Size: 674 Color: 1
Size: 336 Color: 2
Size: 208 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 3314 Color: 3
Size: 2220 Color: 0
Size: 722 Color: 4
Size: 200 Color: 2
Size: 168 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3322 Color: 4
Size: 2482 Color: 1
Size: 820 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3348 Color: 3
Size: 2732 Color: 2
Size: 544 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 3476 Color: 2
Size: 2393 Color: 1
Size: 755 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 3650 Color: 4
Size: 2762 Color: 2
Size: 212 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3844 Color: 1
Size: 2480 Color: 2
Size: 300 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3940 Color: 2
Size: 2508 Color: 0
Size: 176 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4391 Color: 1
Size: 1644 Color: 2
Size: 589 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 3
Size: 1868 Color: 0
Size: 104 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4811 Color: 2
Size: 1569 Color: 4
Size: 244 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5045 Color: 2
Size: 1159 Color: 3
Size: 420 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5107 Color: 2
Size: 1145 Color: 1
Size: 372 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5138 Color: 2
Size: 1222 Color: 3
Size: 264 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 2
Size: 1242 Color: 0
Size: 220 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 2
Size: 1028 Color: 4
Size: 240 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5370 Color: 3
Size: 1122 Color: 2
Size: 132 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 2
Size: 974 Color: 3
Size: 192 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5471 Color: 4
Size: 961 Color: 0
Size: 192 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 1
Size: 962 Color: 3
Size: 132 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5552 Color: 4
Size: 920 Color: 2
Size: 152 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5553 Color: 1
Size: 893 Color: 2
Size: 178 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5580 Color: 0
Size: 800 Color: 4
Size: 244 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5602 Color: 1
Size: 876 Color: 2
Size: 146 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 1
Size: 904 Color: 2
Size: 114 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5669 Color: 2
Size: 643 Color: 1
Size: 312 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 3
Size: 738 Color: 2
Size: 240 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5665 Color: 3
Size: 673 Color: 4
Size: 286 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5684 Color: 1
Size: 742 Color: 2
Size: 198 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5709 Color: 2
Size: 811 Color: 1
Size: 104 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5702 Color: 0
Size: 782 Color: 2
Size: 140 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5738 Color: 1
Size: 548 Color: 1
Size: 338 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 4
Size: 642 Color: 0
Size: 226 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5759 Color: 4
Size: 721 Color: 2
Size: 144 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5765 Color: 1
Size: 717 Color: 1
Size: 142 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 4
Size: 818 Color: 1
Size: 26 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5801 Color: 3
Size: 687 Color: 2
Size: 136 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5826 Color: 2
Size: 478 Color: 1
Size: 320 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 644 Color: 3
Size: 128 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5851 Color: 2
Size: 581 Color: 0
Size: 192 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 0
Size: 613 Color: 2
Size: 124 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5898 Color: 3
Size: 582 Color: 1
Size: 144 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 0
Size: 548 Color: 2
Size: 170 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5924 Color: 1
Size: 416 Color: 2
Size: 284 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5930 Color: 4
Size: 550 Color: 2
Size: 144 Color: 4

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 3325 Color: 0
Size: 3138 Color: 2
Size: 160 Color: 4

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 3866 Color: 0
Size: 2757 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 4123 Color: 3
Size: 2280 Color: 1
Size: 220 Color: 3

Bin 51: 1 of cap free
Amount of items: 5
Items: 
Size: 4564 Color: 2
Size: 858 Color: 0
Size: 857 Color: 0
Size: 248 Color: 3
Size: 96 Color: 4

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 4907 Color: 2
Size: 1564 Color: 3
Size: 152 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 4
Size: 1204 Color: 2
Size: 168 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 5397 Color: 2
Size: 706 Color: 1
Size: 520 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 5431 Color: 2
Size: 988 Color: 3
Size: 204 Color: 0

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 5444 Color: 3
Size: 1179 Color: 0

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 5577 Color: 4
Size: 854 Color: 2
Size: 192 Color: 3

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 5739 Color: 1
Size: 548 Color: 0
Size: 336 Color: 2

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 5742 Color: 0
Size: 881 Color: 3

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 4
Size: 666 Color: 2
Size: 140 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 5919 Color: 1
Size: 476 Color: 2
Size: 228 Color: 3

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 5935 Color: 1
Size: 504 Color: 4
Size: 184 Color: 2

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 0
Size: 2753 Color: 1
Size: 116 Color: 3

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 3765 Color: 0
Size: 2761 Color: 4
Size: 96 Color: 2

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 4093 Color: 2
Size: 2417 Color: 4
Size: 112 Color: 4

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 2
Size: 2244 Color: 0
Size: 112 Color: 3

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 2
Size: 1690 Color: 3
Size: 184 Color: 1

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 4
Size: 1356 Color: 2
Size: 78 Color: 3

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 2
Size: 1224 Color: 4
Size: 122 Color: 1

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 5239 Color: 4
Size: 1383 Color: 3

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 5282 Color: 4
Size: 1196 Color: 2
Size: 144 Color: 4

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 5338 Color: 2
Size: 924 Color: 0
Size: 360 Color: 1

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 5515 Color: 4
Size: 971 Color: 3
Size: 136 Color: 2

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 4
Size: 1106 Color: 0

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 5834 Color: 2
Size: 788 Color: 4

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 0
Size: 739 Color: 3
Size: 40 Color: 0

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 5914 Color: 0
Size: 708 Color: 1

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 3725 Color: 1
Size: 2504 Color: 3
Size: 392 Color: 2

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 4935 Color: 2
Size: 1428 Color: 0
Size: 258 Color: 0

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 2
Size: 1409 Color: 3
Size: 208 Color: 1

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 2
Size: 645 Color: 0
Size: 580 Color: 1

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 1
Size: 1250 Color: 4

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 5459 Color: 4
Size: 1074 Color: 4
Size: 88 Color: 2

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 1
Size: 977 Color: 0

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 2
Size: 1883 Color: 4
Size: 116 Color: 1

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 5597 Color: 4
Size: 1023 Color: 1

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 0
Size: 914 Color: 3
Size: 40 Color: 2

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 5818 Color: 0
Size: 802 Color: 4

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 4062 Color: 3
Size: 2383 Color: 4
Size: 174 Color: 2

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 0
Size: 2111 Color: 2
Size: 280 Color: 3

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 4814 Color: 3
Size: 1671 Color: 2
Size: 134 Color: 0

Bin 92: 5 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 1
Size: 1535 Color: 4
Size: 168 Color: 2

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 5076 Color: 1
Size: 1431 Color: 3
Size: 112 Color: 2

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 5160 Color: 0
Size: 1299 Color: 2
Size: 160 Color: 1

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 4365 Color: 3
Size: 1861 Color: 2
Size: 392 Color: 3

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 5932 Color: 0
Size: 686 Color: 3

Bin 97: 7 of cap free
Amount of items: 7
Items: 
Size: 3316 Color: 2
Size: 662 Color: 1
Size: 602 Color: 1
Size: 594 Color: 2
Size: 588 Color: 4
Size: 575 Color: 0
Size: 280 Color: 4

Bin 98: 7 of cap free
Amount of items: 3
Items: 
Size: 3313 Color: 3
Size: 2754 Color: 2
Size: 550 Color: 4

Bin 99: 7 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 4
Size: 1478 Color: 3
Size: 72 Color: 0

Bin 100: 7 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 1
Size: 902 Color: 0

Bin 101: 7 of cap free
Amount of items: 2
Items: 
Size: 5816 Color: 4
Size: 801 Color: 1

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 5858 Color: 4
Size: 759 Color: 3

Bin 103: 8 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 3
Size: 2004 Color: 0
Size: 72 Color: 1

Bin 104: 9 of cap free
Amount of items: 2
Items: 
Size: 5569 Color: 4
Size: 1046 Color: 0

Bin 105: 9 of cap free
Amount of items: 2
Items: 
Size: 5690 Color: 0
Size: 925 Color: 3

Bin 106: 10 of cap free
Amount of items: 3
Items: 
Size: 4338 Color: 2
Size: 2182 Color: 4
Size: 94 Color: 1

Bin 107: 11 of cap free
Amount of items: 2
Items: 
Size: 5762 Color: 0
Size: 851 Color: 3

Bin 108: 11 of cap free
Amount of items: 2
Items: 
Size: 5889 Color: 1
Size: 724 Color: 0

Bin 109: 15 of cap free
Amount of items: 3
Items: 
Size: 3317 Color: 2
Size: 2804 Color: 2
Size: 488 Color: 0

Bin 110: 16 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 2
Size: 1940 Color: 1
Size: 70 Color: 0

Bin 111: 17 of cap free
Amount of items: 2
Items: 
Size: 5927 Color: 0
Size: 680 Color: 1

Bin 112: 18 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 3
Size: 1275 Color: 0

Bin 113: 19 of cap free
Amount of items: 2
Items: 
Size: 5095 Color: 1
Size: 1510 Color: 3

Bin 114: 20 of cap free
Amount of items: 2
Items: 
Size: 4698 Color: 1
Size: 1906 Color: 4

Bin 115: 20 of cap free
Amount of items: 2
Items: 
Size: 5778 Color: 3
Size: 826 Color: 0

Bin 116: 21 of cap free
Amount of items: 2
Items: 
Size: 4910 Color: 3
Size: 1693 Color: 0

Bin 117: 22 of cap free
Amount of items: 2
Items: 
Size: 4300 Color: 4
Size: 2302 Color: 3

Bin 118: 24 of cap free
Amount of items: 2
Items: 
Size: 5605 Color: 3
Size: 995 Color: 4

Bin 119: 28 of cap free
Amount of items: 2
Items: 
Size: 4458 Color: 0
Size: 2138 Color: 4

Bin 120: 28 of cap free
Amount of items: 2
Items: 
Size: 5304 Color: 1
Size: 1292 Color: 3

Bin 121: 30 of cap free
Amount of items: 2
Items: 
Size: 5470 Color: 1
Size: 1124 Color: 3

Bin 122: 32 of cap free
Amount of items: 2
Items: 
Size: 3964 Color: 3
Size: 2628 Color: 1

Bin 123: 33 of cap free
Amount of items: 2
Items: 
Size: 5546 Color: 4
Size: 1045 Color: 3

Bin 124: 35 of cap free
Amount of items: 2
Items: 
Size: 4783 Color: 4
Size: 1806 Color: 1

Bin 125: 35 of cap free
Amount of items: 2
Items: 
Size: 5211 Color: 3
Size: 1378 Color: 1

Bin 126: 44 of cap free
Amount of items: 2
Items: 
Size: 4974 Color: 1
Size: 1606 Color: 4

Bin 127: 46 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 1
Size: 1724 Color: 3

Bin 128: 51 of cap free
Amount of items: 3
Items: 
Size: 3330 Color: 3
Size: 2751 Color: 3
Size: 492 Color: 0

Bin 129: 61 of cap free
Amount of items: 2
Items: 
Size: 5298 Color: 0
Size: 1265 Color: 1

Bin 130: 68 of cap free
Amount of items: 2
Items: 
Size: 5126 Color: 3
Size: 1430 Color: 4

Bin 131: 75 of cap free
Amount of items: 3
Items: 
Size: 4593 Color: 2
Size: 1079 Color: 4
Size: 877 Color: 4

Bin 132: 104 of cap free
Amount of items: 2
Items: 
Size: 3632 Color: 4
Size: 2888 Color: 1

Bin 133: 5538 of cap free
Amount of items: 8
Items: 
Size: 158 Color: 0
Size: 156 Color: 3
Size: 156 Color: 1
Size: 128 Color: 4
Size: 128 Color: 2
Size: 128 Color: 2
Size: 116 Color: 1
Size: 116 Color: 0

Total size: 874368
Total free space: 6624

