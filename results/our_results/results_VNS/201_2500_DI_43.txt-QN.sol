Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 22
Items: 
Size: 204 Color: 67
Size: 160 Color: 59
Size: 156 Color: 58
Size: 142 Color: 55
Size: 132 Color: 54
Size: 128 Color: 52
Size: 126 Color: 51
Size: 120 Color: 50
Size: 112 Color: 49
Size: 112 Color: 48
Size: 108 Color: 47
Size: 108 Color: 46
Size: 92 Color: 39
Size: 90 Color: 38
Size: 88 Color: 37
Size: 88 Color: 36
Size: 88 Color: 35
Size: 88 Color: 34
Size: 88 Color: 33
Size: 76 Color: 31
Size: 76 Color: 30
Size: 74 Color: 29

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 141
Size: 508 Color: 103
Size: 470 Color: 102
Size: 184 Color: 64
Size: 60 Color: 20

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 163
Size: 620 Color: 114
Size: 56 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 169
Size: 564 Color: 110
Size: 40 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 170
Size: 554 Color: 108
Size: 8 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 175
Size: 369 Color: 87
Size: 168 Color: 61

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1926 Color: 176
Size: 386 Color: 95
Size: 144 Color: 56

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 180
Size: 382 Color: 92
Size: 72 Color: 24

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 182
Size: 383 Color: 93
Size: 62 Color: 21

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 185
Size: 260 Color: 74
Size: 168 Color: 60

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 186
Size: 370 Color: 88
Size: 8 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 191
Size: 204 Color: 68
Size: 152 Color: 57

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 192
Size: 324 Color: 84
Size: 16 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 194
Size: 261 Color: 75
Size: 50 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 198
Size: 176 Color: 63
Size: 92 Color: 40

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 201
Size: 200 Color: 65
Size: 52 Color: 17

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 154
Size: 828 Color: 125
Size: 24 Color: 7

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 159
Size: 756 Color: 122

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 160
Size: 620 Color: 113
Size: 128 Color: 53

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 165
Size: 653 Color: 116
Size: 8 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 167
Size: 568 Color: 111
Size: 80 Color: 32

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 2148 Color: 196
Size: 307 Color: 82

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 2190 Color: 199
Size: 265 Color: 77

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 155
Size: 610 Color: 112
Size: 200 Color: 66

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 172
Size: 538 Color: 105
Size: 8 Color: 2

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 178
Size: 460 Color: 100

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 2012 Color: 183
Size: 442 Color: 97

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 2082 Color: 188
Size: 372 Color: 90

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2090 Color: 190
Size: 364 Color: 86

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 200
Size: 262 Color: 76

Bin 31: 3 of cap free
Amount of items: 9
Items: 
Size: 1229 Color: 138
Size: 244 Color: 72
Size: 232 Color: 71
Size: 222 Color: 70
Size: 212 Color: 69
Size: 96 Color: 42
Size: 74 Color: 28
Size: 72 Color: 27
Size: 72 Color: 26

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1799 Color: 166
Size: 654 Color: 117

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 168
Size: 631 Color: 115
Size: 12 Color: 4

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1911 Color: 174
Size: 542 Color: 107

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 1429 Color: 146
Size: 1023 Color: 136

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 153
Size: 857 Color: 128

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 157
Size: 778 Color: 123

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1897 Color: 171
Size: 555 Color: 109

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 177
Size: 512 Color: 104

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1997 Color: 179
Size: 455 Color: 99

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 2003 Color: 181
Size: 449 Color: 98

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 2081 Color: 187
Size: 371 Color: 89

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 2139 Color: 193
Size: 313 Color: 83

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 195
Size: 306 Color: 81

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 197
Size: 280 Color: 79

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 161
Size: 719 Color: 120
Size: 16 Color: 6

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 173
Size: 541 Color: 106

Bin 48: 5 of cap free
Amount of items: 2
Items: 
Size: 2015 Color: 184
Size: 436 Color: 96

Bin 49: 5 of cap free
Amount of items: 2
Items: 
Size: 2089 Color: 189
Size: 362 Color: 85

Bin 50: 6 of cap free
Amount of items: 4
Items: 
Size: 1334 Color: 143
Size: 1020 Color: 133
Size: 48 Color: 15
Size: 48 Color: 14

Bin 51: 6 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 152
Size: 862 Color: 129
Size: 32 Color: 8

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 158
Size: 753 Color: 121

Bin 53: 8 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 145
Size: 1022 Color: 135

Bin 54: 8 of cap free
Amount of items: 2
Items: 
Size: 1510 Color: 149
Size: 938 Color: 131

Bin 55: 8 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 156
Size: 790 Color: 124

Bin 56: 8 of cap free
Amount of items: 2
Items: 
Size: 1764 Color: 162
Size: 684 Color: 119

Bin 57: 8 of cap free
Amount of items: 2
Items: 
Size: 1782 Color: 164
Size: 666 Color: 118

Bin 58: 10 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 140
Size: 467 Color: 101
Size: 384 Color: 94
Size: 300 Color: 80
Size: 64 Color: 22

Bin 59: 23 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 144
Size: 1021 Color: 134
Size: 48 Color: 13

Bin 60: 24 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 148
Size: 916 Color: 130
Size: 48 Color: 12

Bin 61: 25 of cap free
Amount of items: 7
Items: 
Size: 1230 Color: 139
Size: 379 Color: 91
Size: 272 Color: 78
Size: 244 Color: 73
Size: 170 Color: 62
Size: 72 Color: 25
Size: 64 Color: 23

Bin 62: 29 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 151
Size: 851 Color: 127
Size: 40 Color: 9

Bin 63: 43 of cap free
Amount of items: 2
Items: 
Size: 1437 Color: 147
Size: 976 Color: 132

Bin 64: 46 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 142
Size: 1114 Color: 137
Size: 60 Color: 19

Bin 65: 46 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 150
Size: 840 Color: 126
Size: 44 Color: 11

Bin 66: 2054 of cap free
Amount of items: 4
Items: 
Size: 104 Color: 45
Size: 102 Color: 44
Size: 100 Color: 43
Size: 96 Color: 41

Total size: 159640
Total free space: 2456

