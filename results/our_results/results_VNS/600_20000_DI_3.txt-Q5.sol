Capicity Bin: 16752
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8388 Color: 3
Size: 6972 Color: 4
Size: 1392 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 1
Size: 6072 Color: 0
Size: 1200 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 3
Size: 6640 Color: 2
Size: 536 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 0
Size: 6028 Color: 4
Size: 844 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10378 Color: 3
Size: 6090 Color: 3
Size: 284 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10572 Color: 1
Size: 5156 Color: 1
Size: 1024 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 0
Size: 5160 Color: 4
Size: 288 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 0
Size: 4552 Color: 3
Size: 480 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 3
Size: 3318 Color: 1
Size: 1394 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12318 Color: 4
Size: 3698 Color: 2
Size: 736 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12459 Color: 2
Size: 2274 Color: 3
Size: 2019 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12636 Color: 4
Size: 3284 Color: 1
Size: 832 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 4
Size: 3484 Color: 0
Size: 456 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12888 Color: 0
Size: 3416 Color: 3
Size: 448 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12945 Color: 1
Size: 3173 Color: 0
Size: 634 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 1
Size: 3224 Color: 3
Size: 304 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13554 Color: 3
Size: 2600 Color: 1
Size: 598 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 3
Size: 2028 Color: 1
Size: 1032 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13817 Color: 0
Size: 2447 Color: 3
Size: 488 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13947 Color: 3
Size: 1859 Color: 1
Size: 946 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 0
Size: 2299 Color: 3
Size: 458 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14007 Color: 4
Size: 2121 Color: 0
Size: 624 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 0
Size: 2336 Color: 3
Size: 344 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14187 Color: 4
Size: 2203 Color: 3
Size: 362 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 0
Size: 1922 Color: 3
Size: 632 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14234 Color: 3
Size: 1470 Color: 1
Size: 1048 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 2
Size: 1702 Color: 3
Size: 840 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 3
Size: 2102 Color: 4
Size: 390 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 3
Size: 1978 Color: 1
Size: 452 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 4
Size: 2026 Color: 3
Size: 402 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14370 Color: 2
Size: 1312 Color: 3
Size: 1070 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 4
Size: 1979 Color: 1
Size: 394 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14415 Color: 3
Size: 1959 Color: 2
Size: 378 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 4
Size: 1592 Color: 3
Size: 736 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14468 Color: 4
Size: 1804 Color: 3
Size: 480 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 3
Size: 1974 Color: 0
Size: 256 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 3
Size: 1880 Color: 4
Size: 320 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 3
Size: 1826 Color: 4
Size: 364 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 0
Size: 1670 Color: 3
Size: 568 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 3
Size: 1819 Color: 2
Size: 362 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 1
Size: 1908 Color: 3
Size: 312 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14591 Color: 3
Size: 1801 Color: 0
Size: 360 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14671 Color: 3
Size: 1735 Color: 2
Size: 346 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 3
Size: 1592 Color: 1
Size: 462 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 4
Size: 1804 Color: 3
Size: 296 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 3
Size: 1498 Color: 0
Size: 554 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 0
Size: 1700 Color: 0
Size: 368 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 0
Size: 1200 Color: 1
Size: 836 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14722 Color: 0
Size: 1694 Color: 3
Size: 336 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14846 Color: 3
Size: 1442 Color: 2
Size: 464 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14796 Color: 2
Size: 1392 Color: 0
Size: 564 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14856 Color: 0
Size: 1280 Color: 3
Size: 616 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14860 Color: 1
Size: 1392 Color: 3
Size: 500 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14974 Color: 3
Size: 1482 Color: 0
Size: 296 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14952 Color: 0
Size: 1448 Color: 2
Size: 352 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 0
Size: 1360 Color: 0
Size: 420 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14988 Color: 3
Size: 1196 Color: 4
Size: 568 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 15026 Color: 1
Size: 1198 Color: 3
Size: 528 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 4
Size: 1392 Color: 0
Size: 312 Color: 3

Bin 60: 1 of cap free
Amount of items: 7
Items: 
Size: 8377 Color: 4
Size: 1662 Color: 1
Size: 1642 Color: 1
Size: 1512 Color: 1
Size: 1394 Color: 3
Size: 1392 Color: 2
Size: 772 Color: 4

Bin 61: 1 of cap free
Amount of items: 7
Items: 
Size: 8381 Color: 1
Size: 1882 Color: 1
Size: 1594 Color: 0
Size: 1562 Color: 0
Size: 1476 Color: 4
Size: 976 Color: 2
Size: 880 Color: 0

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 9353 Color: 1
Size: 6982 Color: 0
Size: 416 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10329 Color: 3
Size: 5206 Color: 1
Size: 1216 Color: 1

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 1
Size: 5244 Color: 0
Size: 432 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11083 Color: 0
Size: 5164 Color: 2
Size: 504 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11734 Color: 1
Size: 4725 Color: 0
Size: 292 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 0
Size: 4209 Color: 4
Size: 240 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 1
Size: 3863 Color: 0
Size: 320 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12953 Color: 1
Size: 3302 Color: 2
Size: 496 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 4
Size: 2903 Color: 3
Size: 540 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 1
Size: 2997 Color: 3
Size: 430 Color: 1

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13421 Color: 0
Size: 2662 Color: 3
Size: 668 Color: 4

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 1
Size: 2084 Color: 4
Size: 944 Color: 3

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13762 Color: 4
Size: 2601 Color: 3
Size: 388 Color: 1

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13919 Color: 3
Size: 2300 Color: 1
Size: 532 Color: 2

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 4
Size: 2289 Color: 1
Size: 532 Color: 3

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 1
Size: 2348 Color: 3
Size: 452 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 2
Size: 2335 Color: 3
Size: 384 Color: 4

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 1
Size: 2525 Color: 4

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 14503 Color: 0
Size: 2248 Color: 2

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 9442 Color: 3
Size: 6968 Color: 0
Size: 340 Color: 3

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 10330 Color: 0
Size: 6036 Color: 2
Size: 384 Color: 1

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 2
Size: 4738 Color: 0
Size: 584 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12726 Color: 2
Size: 3736 Color: 3
Size: 288 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 2
Size: 3608 Color: 0
Size: 344 Color: 3

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 3
Size: 2262 Color: 1
Size: 1024 Color: 1

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 2
Size: 2556 Color: 3
Size: 656 Color: 2

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 3
Size: 2120 Color: 1
Size: 1068 Color: 2

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 0
Size: 2510 Color: 1
Size: 680 Color: 2

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 0
Size: 2666 Color: 1
Size: 328 Color: 3

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 14002 Color: 2
Size: 2500 Color: 3
Size: 248 Color: 1

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 4
Size: 2472 Color: 3
Size: 236 Color: 0

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 0
Size: 2130 Color: 2
Size: 372 Color: 3

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 14898 Color: 2
Size: 1852 Color: 1

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 15002 Color: 2
Size: 1452 Color: 1
Size: 296 Color: 3

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 0
Size: 6973 Color: 2
Size: 260 Color: 1

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 11480 Color: 2
Size: 4981 Color: 0
Size: 288 Color: 4

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 11705 Color: 3
Size: 4516 Color: 0
Size: 528 Color: 1

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 3
Size: 4408 Color: 1
Size: 224 Color: 4

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 1
Size: 4207 Color: 4
Size: 288 Color: 0

Bin 101: 3 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 1
Size: 3579 Color: 0
Size: 396 Color: 2

Bin 102: 3 of cap free
Amount of items: 2
Items: 
Size: 14854 Color: 1
Size: 1895 Color: 2

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 3
Size: 5353 Color: 2
Size: 962 Color: 0

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 10556 Color: 1
Size: 5528 Color: 2
Size: 664 Color: 0

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 2
Size: 5196 Color: 3
Size: 584 Color: 1

Bin 106: 4 of cap free
Amount of items: 3
Items: 
Size: 11686 Color: 2
Size: 4614 Color: 3
Size: 448 Color: 0

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 2
Size: 3710 Color: 3
Size: 370 Color: 3

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 13269 Color: 3
Size: 3201 Color: 1
Size: 278 Color: 2

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 3
Size: 1688 Color: 0
Size: 1636 Color: 0

Bin 110: 4 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 0
Size: 2808 Color: 4

Bin 111: 4 of cap free
Amount of items: 2
Items: 
Size: 13971 Color: 1
Size: 2777 Color: 2

Bin 112: 4 of cap free
Amount of items: 2
Items: 
Size: 14026 Color: 0
Size: 2722 Color: 1

Bin 113: 4 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 2
Size: 2122 Color: 4

Bin 114: 5 of cap free
Amount of items: 3
Items: 
Size: 8800 Color: 2
Size: 6971 Color: 0
Size: 976 Color: 1

Bin 115: 5 of cap free
Amount of items: 3
Items: 
Size: 13038 Color: 1
Size: 3485 Color: 3
Size: 224 Color: 4

Bin 116: 5 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 3
Size: 2319 Color: 2
Size: 1040 Color: 4

Bin 117: 5 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 0
Size: 2159 Color: 4

Bin 118: 5 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 4
Size: 1965 Color: 2

Bin 119: 6 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 0
Size: 2876 Color: 2

Bin 120: 6 of cap free
Amount of items: 2
Items: 
Size: 14990 Color: 0
Size: 1756 Color: 4

Bin 121: 6 of cap free
Amount of items: 2
Items: 
Size: 15032 Color: 4
Size: 1714 Color: 1

Bin 122: 7 of cap free
Amount of items: 3
Items: 
Size: 11070 Color: 0
Size: 4731 Color: 3
Size: 944 Color: 2

Bin 123: 7 of cap free
Amount of items: 3
Items: 
Size: 11218 Color: 2
Size: 5267 Color: 4
Size: 260 Color: 4

Bin 124: 7 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 4
Size: 4182 Color: 3

Bin 125: 8 of cap free
Amount of items: 19
Items: 
Size: 1184 Color: 3
Size: 1152 Color: 2
Size: 1136 Color: 3
Size: 1032 Color: 2
Size: 960 Color: 4
Size: 960 Color: 0
Size: 920 Color: 4
Size: 896 Color: 4
Size: 896 Color: 2
Size: 864 Color: 0
Size: 840 Color: 3
Size: 792 Color: 1
Size: 784 Color: 4
Size: 784 Color: 2
Size: 772 Color: 4
Size: 748 Color: 3
Size: 704 Color: 0
Size: 660 Color: 2
Size: 660 Color: 0

Bin 126: 8 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 1
Size: 7328 Color: 2
Size: 432 Color: 1

Bin 127: 8 of cap free
Amount of items: 3
Items: 
Size: 9446 Color: 2
Size: 6978 Color: 3
Size: 320 Color: 1

Bin 128: 8 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 3
Size: 6980 Color: 1
Size: 240 Color: 1

Bin 129: 8 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 3
Size: 3167 Color: 0
Size: 1462 Color: 2

Bin 130: 8 of cap free
Amount of items: 3
Items: 
Size: 12516 Color: 0
Size: 3660 Color: 4
Size: 568 Color: 3

Bin 131: 8 of cap free
Amount of items: 3
Items: 
Size: 12572 Color: 1
Size: 3436 Color: 3
Size: 736 Color: 0

Bin 132: 8 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 3
Size: 3980 Color: 2

Bin 133: 8 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 4
Size: 3532 Color: 2

Bin 134: 8 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 1
Size: 2265 Color: 0

Bin 135: 9 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 4
Size: 2051 Color: 1
Size: 28 Color: 0

Bin 136: 10 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 4
Size: 3358 Color: 0

Bin 137: 10 of cap free
Amount of items: 2
Items: 
Size: 14403 Color: 2
Size: 2339 Color: 4

Bin 138: 10 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 4
Size: 2244 Color: 0

Bin 139: 10 of cap free
Amount of items: 2
Items: 
Size: 14710 Color: 1
Size: 2032 Color: 2

Bin 140: 11 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 0
Size: 5992 Color: 3
Size: 1949 Color: 3
Size: 408 Color: 2

Bin 141: 11 of cap free
Amount of items: 2
Items: 
Size: 12571 Color: 4
Size: 4170 Color: 2

Bin 142: 12 of cap free
Amount of items: 3
Items: 
Size: 10506 Color: 4
Size: 5810 Color: 2
Size: 424 Color: 0

Bin 143: 12 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 3
Size: 4520 Color: 0
Size: 176 Color: 1

Bin 144: 12 of cap free
Amount of items: 2
Items: 
Size: 13996 Color: 0
Size: 2744 Color: 4

Bin 145: 12 of cap free
Amount of items: 2
Items: 
Size: 14386 Color: 0
Size: 2354 Color: 4

Bin 146: 12 of cap free
Amount of items: 2
Items: 
Size: 14878 Color: 4
Size: 1862 Color: 2

Bin 147: 14 of cap free
Amount of items: 3
Items: 
Size: 11599 Color: 3
Size: 4819 Color: 4
Size: 320 Color: 2

Bin 148: 14 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 0
Size: 3098 Color: 4

Bin 149: 14 of cap free
Amount of items: 2
Items: 
Size: 14450 Color: 1
Size: 2288 Color: 0

Bin 150: 16 of cap free
Amount of items: 2
Items: 
Size: 15012 Color: 2
Size: 1724 Color: 1

Bin 151: 18 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 2
Size: 2934 Color: 1

Bin 152: 19 of cap free
Amount of items: 2
Items: 
Size: 13525 Color: 4
Size: 3208 Color: 0

Bin 153: 19 of cap free
Amount of items: 2
Items: 
Size: 14331 Color: 4
Size: 2402 Color: 2

Bin 154: 20 of cap free
Amount of items: 2
Items: 
Size: 14958 Color: 1
Size: 1774 Color: 2

Bin 155: 21 of cap free
Amount of items: 3
Items: 
Size: 9559 Color: 2
Size: 6888 Color: 3
Size: 284 Color: 0

Bin 156: 21 of cap free
Amount of items: 2
Items: 
Size: 10971 Color: 3
Size: 5760 Color: 2

Bin 157: 22 of cap free
Amount of items: 31
Items: 
Size: 740 Color: 1
Size: 714 Color: 4
Size: 704 Color: 2
Size: 698 Color: 2
Size: 696 Color: 4
Size: 696 Color: 3
Size: 688 Color: 1
Size: 672 Color: 3
Size: 652 Color: 3
Size: 640 Color: 2
Size: 580 Color: 1
Size: 568 Color: 4
Size: 560 Color: 4
Size: 544 Color: 0
Size: 518 Color: 4
Size: 512 Color: 4
Size: 512 Color: 4
Size: 504 Color: 0
Size: 496 Color: 2
Size: 468 Color: 3
Size: 466 Color: 3
Size: 466 Color: 1
Size: 464 Color: 3
Size: 456 Color: 2
Size: 424 Color: 0
Size: 424 Color: 0
Size: 416 Color: 0
Size: 408 Color: 2
Size: 392 Color: 0
Size: 380 Color: 0
Size: 272 Color: 3

Bin 158: 22 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 4
Size: 3496 Color: 0

Bin 159: 22 of cap free
Amount of items: 2
Items: 
Size: 14744 Color: 2
Size: 1986 Color: 1

Bin 160: 24 of cap free
Amount of items: 8
Items: 
Size: 8378 Color: 2
Size: 1580 Color: 1
Size: 1394 Color: 3
Size: 1392 Color: 0
Size: 1200 Color: 3
Size: 1136 Color: 4
Size: 1088 Color: 0
Size: 560 Color: 2

Bin 161: 25 of cap free
Amount of items: 3
Items: 
Size: 14523 Color: 2
Size: 2136 Color: 4
Size: 68 Color: 2

Bin 162: 26 of cap free
Amount of items: 5
Items: 
Size: 8382 Color: 2
Size: 2858 Color: 3
Size: 2682 Color: 1
Size: 1752 Color: 1
Size: 1052 Color: 0

Bin 163: 26 of cap free
Amount of items: 2
Items: 
Size: 10632 Color: 3
Size: 6094 Color: 4

Bin 164: 26 of cap free
Amount of items: 2
Items: 
Size: 14035 Color: 0
Size: 2691 Color: 1

Bin 165: 27 of cap free
Amount of items: 4
Items: 
Size: 8380 Color: 0
Size: 5981 Color: 3
Size: 1944 Color: 1
Size: 420 Color: 2

Bin 166: 27 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 0
Size: 3261 Color: 3
Size: 1484 Color: 1

Bin 167: 28 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 4
Size: 4444 Color: 3

Bin 168: 28 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 1
Size: 2664 Color: 4

Bin 169: 28 of cap free
Amount of items: 2
Items: 
Size: 14575 Color: 2
Size: 2149 Color: 1

Bin 170: 30 of cap free
Amount of items: 4
Items: 
Size: 8385 Color: 2
Size: 5995 Color: 4
Size: 2106 Color: 4
Size: 236 Color: 1

Bin 171: 33 of cap free
Amount of items: 2
Items: 
Size: 9575 Color: 3
Size: 7144 Color: 4

Bin 172: 34 of cap free
Amount of items: 2
Items: 
Size: 12790 Color: 3
Size: 3928 Color: 2

Bin 173: 34 of cap free
Amount of items: 2
Items: 
Size: 14163 Color: 1
Size: 2555 Color: 0

Bin 174: 35 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 1
Size: 6981 Color: 0
Size: 1232 Color: 2

Bin 175: 36 of cap free
Amount of items: 2
Items: 
Size: 10460 Color: 2
Size: 6256 Color: 4

Bin 176: 38 of cap free
Amount of items: 2
Items: 
Size: 14207 Color: 1
Size: 2507 Color: 0

Bin 177: 39 of cap free
Amount of items: 2
Items: 
Size: 13327 Color: 1
Size: 3386 Color: 0

Bin 178: 40 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 2
Size: 8256 Color: 4

Bin 179: 48 of cap free
Amount of items: 2
Items: 
Size: 12839 Color: 2
Size: 3865 Color: 4

Bin 180: 50 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 1
Size: 3750 Color: 3

Bin 181: 51 of cap free
Amount of items: 4
Items: 
Size: 13633 Color: 4
Size: 2860 Color: 1
Size: 112 Color: 3
Size: 96 Color: 2

Bin 182: 51 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 0
Size: 2956 Color: 4

Bin 183: 52 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 0
Size: 2804 Color: 1

Bin 184: 53 of cap free
Amount of items: 3
Items: 
Size: 11703 Color: 3
Size: 4824 Color: 4
Size: 172 Color: 3

Bin 185: 58 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 2
Size: 5354 Color: 4

Bin 186: 58 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 1
Size: 2494 Color: 2

Bin 187: 60 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 2
Size: 2399 Color: 0

Bin 188: 61 of cap free
Amount of items: 2
Items: 
Size: 10524 Color: 2
Size: 6167 Color: 1

Bin 189: 75 of cap free
Amount of items: 2
Items: 
Size: 14285 Color: 0
Size: 2392 Color: 4

Bin 190: 86 of cap free
Amount of items: 2
Items: 
Size: 13490 Color: 1
Size: 3176 Color: 0

Bin 191: 90 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 3
Size: 4222 Color: 4

Bin 192: 98 of cap free
Amount of items: 2
Items: 
Size: 11750 Color: 2
Size: 4904 Color: 4

Bin 193: 102 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 1
Size: 3324 Color: 4

Bin 194: 104 of cap free
Amount of items: 2
Items: 
Size: 13157 Color: 0
Size: 3491 Color: 1

Bin 195: 128 of cap free
Amount of items: 2
Items: 
Size: 10136 Color: 4
Size: 6488 Color: 3

Bin 196: 144 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 1
Size: 5736 Color: 0

Bin 197: 164 of cap free
Amount of items: 2
Items: 
Size: 12664 Color: 3
Size: 3924 Color: 4

Bin 198: 170 of cap free
Amount of items: 3
Items: 
Size: 8389 Color: 3
Size: 6977 Color: 3
Size: 1216 Color: 0

Bin 199: 13832 of cap free
Amount of items: 8
Items: 
Size: 404 Color: 1
Size: 400 Color: 4
Size: 376 Color: 0
Size: 368 Color: 1
Size: 360 Color: 2
Size: 340 Color: 0
Size: 336 Color: 4
Size: 336 Color: 2

Total size: 3316896
Total free space: 16752

