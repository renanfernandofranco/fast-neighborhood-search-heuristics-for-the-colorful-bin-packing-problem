Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 5
Size: 312 Color: 12
Size: 275 Color: 10

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 15
Size: 340 Color: 5
Size: 267 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 16
Size: 321 Color: 2
Size: 257 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 359 Color: 8
Size: 270 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 13
Size: 302 Color: 1
Size: 281 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 10
Size: 277 Color: 14
Size: 261 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 19
Size: 323 Color: 12
Size: 271 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 19
Size: 289 Color: 12
Size: 252 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 11
Size: 264 Color: 9
Size: 261 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 18
Size: 359 Color: 16
Size: 251 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8
Size: 299 Color: 15
Size: 261 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 12
Size: 350 Color: 4
Size: 286 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 258 Color: 4
Size: 252 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 6
Size: 296 Color: 6
Size: 262 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 323 Color: 2
Size: 251 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 322 Color: 19
Size: 271 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 16
Size: 298 Color: 11
Size: 271 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 7
Size: 296 Color: 2
Size: 276 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 6
Size: 324 Color: 7
Size: 280 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 12
Size: 278 Color: 13
Size: 260 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 283 Color: 6
Size: 269 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 15
Size: 302 Color: 6
Size: 274 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 16
Size: 264 Color: 4
Size: 261 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 7
Size: 276 Color: 6
Size: 264 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 7
Size: 330 Color: 9
Size: 329 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 5
Size: 313 Color: 1
Size: 274 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 307 Color: 19
Size: 269 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 14
Size: 277 Color: 3
Size: 256 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 286 Color: 5
Size: 263 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 279 Color: 2
Size: 266 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 281 Color: 10
Size: 258 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 299 Color: 19
Size: 258 Color: 5

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 358 Color: 3
Size: 282 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 274 Color: 16
Size: 257 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 9
Size: 256 Color: 16
Size: 254 Color: 14

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 325 Color: 4
Size: 258 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 19
Size: 353 Color: 6
Size: 258 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 315 Color: 2
Size: 314 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 11
Size: 291 Color: 3
Size: 290 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 8
Size: 332 Color: 4
Size: 272 Color: 14

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 339 Color: 12
Size: 250 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 10
Size: 299 Color: 19
Size: 258 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 305 Color: 12
Size: 255 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 19
Size: 336 Color: 6
Size: 278 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 278 Color: 1
Size: 257 Color: 12

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 0
Size: 285 Color: 17
Size: 264 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 300 Color: 18
Size: 281 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 7
Size: 266 Color: 8
Size: 257 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 8
Size: 351 Color: 18
Size: 283 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 15
Size: 327 Color: 9
Size: 287 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 319 Color: 19
Size: 284 Color: 10

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 287 Color: 18
Size: 266 Color: 6

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 15
Size: 280 Color: 13
Size: 265 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 15
Size: 289 Color: 12
Size: 260 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 353 Color: 4
Size: 255 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 256 Color: 7
Size: 251 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 13
Size: 296 Color: 15
Size: 255 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 10
Size: 312 Color: 5
Size: 284 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 19
Size: 320 Color: 13
Size: 273 Color: 9

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 291 Color: 7
Size: 267 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 14
Size: 321 Color: 10
Size: 275 Color: 14

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 16
Size: 289 Color: 17
Size: 271 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 290 Color: 18
Size: 252 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 319 Color: 12
Size: 257 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 10
Size: 309 Color: 18
Size: 306 Color: 6

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 17
Size: 296 Color: 6
Size: 261 Color: 13

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 6
Size: 259 Color: 1
Size: 254 Color: 6

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 297 Color: 4
Size: 296 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 19
Size: 305 Color: 0
Size: 258 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 11
Size: 359 Color: 0
Size: 282 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8
Size: 286 Color: 5
Size: 279 Color: 13

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 352 Color: 16
Size: 286 Color: 13

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 354 Color: 13
Size: 261 Color: 11

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 10
Size: 360 Color: 5
Size: 280 Color: 13

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 18
Size: 360 Color: 10
Size: 280 Color: 11

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 0
Size: 352 Color: 11
Size: 292 Color: 8

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 5
Size: 369 Color: 15
Size: 261 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 5
Size: 326 Color: 17
Size: 321 Color: 6

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 323 Color: 9
Size: 307 Color: 11

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 15
Size: 357 Color: 13
Size: 283 Color: 11

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 0
Size: 298 Color: 17
Size: 367 Color: 15

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7
Size: 329 Color: 13
Size: 298 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 357 Color: 4
Size: 278 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 17
Size: 355 Color: 4
Size: 271 Color: 5

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 18
Size: 314 Color: 0
Size: 312 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7
Size: 351 Color: 13
Size: 271 Color: 7

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 345 Color: 17
Size: 272 Color: 6

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 8
Size: 361 Color: 13
Size: 253 Color: 19

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 13
Size: 331 Color: 6
Size: 285 Color: 18

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 11
Size: 331 Color: 14
Size: 282 Color: 6

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 6
Size: 331 Color: 2
Size: 278 Color: 19

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 10
Size: 353 Color: 4
Size: 255 Color: 15

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 319 Color: 9
Size: 286 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 6
Size: 321 Color: 13
Size: 281 Color: 12

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 350 Color: 10
Size: 252 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 10
Size: 329 Color: 0
Size: 270 Color: 7

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 5
Size: 320 Color: 17
Size: 276 Color: 4

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 334 Color: 14
Size: 261 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 307 Color: 14
Size: 286 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 6
Size: 307 Color: 16
Size: 286 Color: 19

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 19
Size: 303 Color: 1
Size: 286 Color: 12

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 11
Size: 337 Color: 13
Size: 252 Color: 10

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 316 Color: 11
Size: 270 Color: 18

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8
Size: 318 Color: 13
Size: 267 Color: 16

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 13
Size: 294 Color: 2
Size: 284 Color: 10

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 300 Color: 0
Size: 277 Color: 9

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 6
Size: 312 Color: 9
Size: 264 Color: 14

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 16
Size: 310 Color: 13
Size: 266 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 291 Color: 13
Size: 285 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 297 Color: 13
Size: 277 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 5
Size: 298 Color: 10
Size: 276 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 296 Color: 4
Size: 288 Color: 12

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 9
Size: 303 Color: 2
Size: 269 Color: 8

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 316 Color: 11
Size: 252 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 293 Color: 3
Size: 274 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 7
Size: 302 Color: 7
Size: 264 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 10
Size: 285 Color: 14
Size: 281 Color: 8

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 301 Color: 18
Size: 264 Color: 6

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 3
Size: 293 Color: 15
Size: 272 Color: 16

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 9
Size: 301 Color: 13
Size: 267 Color: 19

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 14
Size: 303 Color: 15
Size: 253 Color: 15

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 19
Size: 284 Color: 10
Size: 271 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 302 Color: 17
Size: 251 Color: 15

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 6
Size: 279 Color: 0
Size: 272 Color: 5

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 17
Size: 327 Color: 19
Size: 326 Color: 17

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 17
Size: 284 Color: 1
Size: 264 Color: 17

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 296 Color: 13
Size: 251 Color: 19

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 12
Size: 280 Color: 9
Size: 267 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 295 Color: 16
Size: 250 Color: 8

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 8
Size: 279 Color: 14
Size: 265 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 19
Size: 288 Color: 1
Size: 255 Color: 13

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 19
Size: 272 Color: 6
Size: 267 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 8
Size: 278 Color: 19
Size: 261 Color: 12

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 18
Size: 282 Color: 8
Size: 255 Color: 8

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 16
Size: 282 Color: 9
Size: 254 Color: 16

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 18
Size: 279 Color: 6
Size: 255 Color: 17

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 11
Size: 279 Color: 19
Size: 252 Color: 5

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 276 Color: 18
Size: 257 Color: 9

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 19
Size: 269 Color: 1
Size: 263 Color: 12

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 16
Size: 270 Color: 8
Size: 258 Color: 15

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 19
Size: 272 Color: 17
Size: 254 Color: 6

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 0
Size: 264 Color: 2
Size: 262 Color: 12

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 2
Size: 273 Color: 11
Size: 251 Color: 12

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 11
Size: 270 Color: 16
Size: 255 Color: 11

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 3
Size: 263 Color: 7
Size: 262 Color: 12

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 273 Color: 13
Size: 251 Color: 15

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9
Size: 271 Color: 9
Size: 253 Color: 7

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 13
Size: 262 Color: 16
Size: 257 Color: 18

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 267 Color: 16
Size: 251 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 15
Size: 261 Color: 10
Size: 257 Color: 17

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 13
Size: 268 Color: 2
Size: 250 Color: 8

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 15
Size: 264 Color: 1
Size: 253 Color: 8

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 261 Color: 19
Size: 254 Color: 16

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 264 Color: 10
Size: 251 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 13
Size: 262 Color: 2
Size: 253 Color: 12

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 15
Size: 262 Color: 18
Size: 252 Color: 12

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 5
Size: 260 Color: 12
Size: 254 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 8
Size: 260 Color: 12
Size: 252 Color: 7

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 260 Color: 10
Size: 252 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 10
Size: 258 Color: 17
Size: 254 Color: 11

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 5
Size: 262 Color: 6
Size: 250 Color: 9

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 6
Size: 257 Color: 10
Size: 252 Color: 13

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 254 Color: 4
Size: 252 Color: 16

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 19
Size: 254 Color: 9
Size: 252 Color: 13

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 9
Size: 254 Color: 2
Size: 251 Color: 8

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 6
Size: 253 Color: 2
Size: 251 Color: 16

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 13
Size: 252 Color: 17
Size: 250 Color: 19

Total size: 167000
Total free space: 0

