Capicity Bin: 6528
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 3266 Color: 1
Size: 844 Color: 4
Size: 820 Color: 4
Size: 720 Color: 4
Size: 590 Color: 3
Size: 144 Color: 3
Size: 144 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3284 Color: 3
Size: 2708 Color: 1
Size: 536 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3282 Color: 0
Size: 2706 Color: 4
Size: 540 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 2
Size: 2366 Color: 0
Size: 196 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3994 Color: 0
Size: 2114 Color: 1
Size: 420 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4026 Color: 0
Size: 1490 Color: 3
Size: 1012 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4164 Color: 4
Size: 2120 Color: 1
Size: 244 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4232 Color: 2
Size: 2172 Color: 0
Size: 124 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 0
Size: 2120 Color: 1
Size: 100 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4404 Color: 4
Size: 1928 Color: 3
Size: 196 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4584 Color: 4
Size: 1576 Color: 2
Size: 368 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4604 Color: 3
Size: 1564 Color: 0
Size: 360 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 3
Size: 1768 Color: 3
Size: 112 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4649 Color: 4
Size: 1411 Color: 1
Size: 468 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 1
Size: 1504 Color: 4
Size: 248 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 1
Size: 1415 Color: 2
Size: 282 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 3
Size: 1382 Color: 4
Size: 204 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5009 Color: 0
Size: 1103 Color: 4
Size: 416 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 0
Size: 1268 Color: 2
Size: 248 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5050 Color: 4
Size: 1196 Color: 0
Size: 282 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 1
Size: 1240 Color: 4
Size: 176 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5130 Color: 4
Size: 1234 Color: 1
Size: 164 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 3
Size: 988 Color: 4
Size: 336 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5310 Color: 3
Size: 906 Color: 3
Size: 312 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5342 Color: 3
Size: 730 Color: 0
Size: 456 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5368 Color: 3
Size: 760 Color: 4
Size: 400 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 3
Size: 580 Color: 2
Size: 504 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5464 Color: 4
Size: 748 Color: 3
Size: 316 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 4
Size: 728 Color: 2
Size: 252 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 1
Size: 542 Color: 2
Size: 456 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 2
Size: 814 Color: 4
Size: 160 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 3
Size: 686 Color: 4
Size: 236 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5636 Color: 2
Size: 540 Color: 1
Size: 352 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 0
Size: 794 Color: 4
Size: 80 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 3
Size: 644 Color: 4
Size: 224 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 3
Size: 728 Color: 4
Size: 112 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5722 Color: 3
Size: 542 Color: 4
Size: 264 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5738 Color: 3
Size: 582 Color: 4
Size: 208 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 4
Size: 620 Color: 1
Size: 152 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 0
Size: 634 Color: 4
Size: 124 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 2
Size: 536 Color: 4
Size: 180 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5832 Color: 1
Size: 568 Color: 4
Size: 128 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 3
Size: 542 Color: 3
Size: 152 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5848 Color: 2
Size: 496 Color: 1
Size: 184 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 0
Size: 2051 Color: 2
Size: 136 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 2
Size: 1784 Color: 0
Size: 120 Color: 4

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 2
Size: 1385 Color: 1
Size: 240 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5035 Color: 0
Size: 1380 Color: 3
Size: 112 Color: 4

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5039 Color: 4
Size: 1192 Color: 0
Size: 296 Color: 2

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 1
Size: 1018 Color: 4
Size: 352 Color: 4

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 5183 Color: 3
Size: 1344 Color: 2

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 5205 Color: 4
Size: 1322 Color: 3

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 5303 Color: 4
Size: 984 Color: 2
Size: 240 Color: 1

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 3
Size: 1021 Color: 4
Size: 64 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 3658 Color: 0
Size: 2668 Color: 2
Size: 200 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 1
Size: 2724 Color: 2
Size: 112 Color: 0

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 3812 Color: 2
Size: 2714 Color: 4

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 0
Size: 1772 Color: 3
Size: 480 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 4369 Color: 0
Size: 1881 Color: 4
Size: 276 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 4478 Color: 2
Size: 1828 Color: 4
Size: 220 Color: 1

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 4870 Color: 4
Size: 1232 Color: 1
Size: 424 Color: 0

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 0
Size: 1228 Color: 4
Size: 120 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 2
Size: 1108 Color: 1
Size: 220 Color: 4

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 1
Size: 1008 Color: 3
Size: 272 Color: 3

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 5252 Color: 2
Size: 1194 Color: 1
Size: 80 Color: 4

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 5316 Color: 0
Size: 1126 Color: 4
Size: 84 Color: 0

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 4
Size: 724 Color: 3
Size: 192 Color: 0

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 5788 Color: 1
Size: 738 Color: 2

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 3332 Color: 1
Size: 2721 Color: 4
Size: 472 Color: 1

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 4096 Color: 3
Size: 2285 Color: 2
Size: 144 Color: 0

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 3
Size: 2138 Color: 1
Size: 116 Color: 1

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 4512 Color: 0
Size: 1797 Color: 0
Size: 216 Color: 1

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 4710 Color: 3
Size: 1567 Color: 2
Size: 248 Color: 0

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 1
Size: 1241 Color: 1
Size: 212 Color: 0

Bin 75: 3 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 2
Size: 1241 Color: 4

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 2
Size: 1121 Color: 0
Size: 52 Color: 2

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 5414 Color: 3
Size: 1110 Color: 0

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 5690 Color: 0
Size: 834 Color: 1

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 0
Size: 702 Color: 2

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 4678 Color: 1
Size: 1589 Color: 2
Size: 256 Color: 0

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 3624 Color: 1
Size: 2722 Color: 2
Size: 176 Color: 1

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 4373 Color: 1
Size: 2149 Color: 4

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 5592 Color: 1
Size: 930 Color: 3

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 4405 Color: 3
Size: 1972 Color: 4
Size: 144 Color: 1

Bin 85: 8 of cap free
Amount of items: 4
Items: 
Size: 3273 Color: 4
Size: 2711 Color: 1
Size: 408 Color: 0
Size: 128 Color: 2

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 2
Size: 2268 Color: 1
Size: 128 Color: 0

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 3
Size: 968 Color: 3
Size: 902 Color: 0

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 5450 Color: 2
Size: 1070 Color: 3

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 3787 Color: 0
Size: 2568 Color: 1
Size: 164 Color: 3

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 5098 Color: 2
Size: 1420 Color: 4

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 5720 Color: 1
Size: 766 Color: 3
Size: 32 Color: 2

Bin 92: 10 of cap free
Amount of items: 4
Items: 
Size: 5806 Color: 0
Size: 644 Color: 1
Size: 36 Color: 1
Size: 32 Color: 4

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 3464 Color: 3
Size: 2713 Color: 0
Size: 340 Color: 4

Bin 94: 12 of cap free
Amount of items: 4
Items: 
Size: 3277 Color: 0
Size: 1801 Color: 0
Size: 1080 Color: 3
Size: 358 Color: 4

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 0
Size: 2640 Color: 2
Size: 112 Color: 1

Bin 96: 12 of cap free
Amount of items: 3
Items: 
Size: 3992 Color: 1
Size: 2394 Color: 4
Size: 130 Color: 2

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 0
Size: 1304 Color: 3
Size: 48 Color: 4

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 1
Size: 668 Color: 3
Size: 12 Color: 4

Bin 99: 14 of cap free
Amount of items: 3
Items: 
Size: 3272 Color: 2
Size: 2702 Color: 1
Size: 540 Color: 2

Bin 100: 14 of cap free
Amount of items: 2
Items: 
Size: 4306 Color: 2
Size: 2208 Color: 1

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 4510 Color: 0
Size: 2004 Color: 3

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 4660 Color: 2
Size: 1854 Color: 4

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 3
Size: 990 Color: 1

Bin 104: 15 of cap free
Amount of items: 7
Items: 
Size: 3265 Color: 0
Size: 720 Color: 4
Size: 604 Color: 2
Size: 584 Color: 3
Size: 536 Color: 1
Size: 528 Color: 2
Size: 276 Color: 0

Bin 105: 15 of cap free
Amount of items: 3
Items: 
Size: 4867 Color: 2
Size: 1566 Color: 4
Size: 80 Color: 4

Bin 106: 16 of cap free
Amount of items: 2
Items: 
Size: 5048 Color: 1
Size: 1464 Color: 3

Bin 107: 16 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 2
Size: 1044 Color: 0

Bin 108: 16 of cap free
Amount of items: 2
Items: 
Size: 5624 Color: 0
Size: 888 Color: 2

Bin 109: 18 of cap free
Amount of items: 2
Items: 
Size: 4424 Color: 1
Size: 2086 Color: 3

Bin 110: 18 of cap free
Amount of items: 2
Items: 
Size: 4828 Color: 4
Size: 1682 Color: 1

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 4200 Color: 4
Size: 2308 Color: 3

Bin 112: 24 of cap free
Amount of items: 3
Items: 
Size: 3300 Color: 4
Size: 2728 Color: 0
Size: 476 Color: 2

Bin 113: 26 of cap free
Amount of items: 2
Items: 
Size: 5710 Color: 4
Size: 792 Color: 1

Bin 114: 26 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 1
Size: 770 Color: 3

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 5240 Color: 2
Size: 1258 Color: 4

Bin 116: 30 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 3
Size: 734 Color: 2

Bin 117: 34 of cap free
Amount of items: 2
Items: 
Size: 4968 Color: 4
Size: 1526 Color: 1

Bin 118: 35 of cap free
Amount of items: 2
Items: 
Size: 4069 Color: 1
Size: 2424 Color: 3

Bin 119: 39 of cap free
Amount of items: 2
Items: 
Size: 4835 Color: 4
Size: 1654 Color: 0

Bin 120: 39 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 3
Size: 1429 Color: 1

Bin 121: 40 of cap free
Amount of items: 2
Items: 
Size: 5348 Color: 4
Size: 1140 Color: 2

Bin 122: 42 of cap free
Amount of items: 5
Items: 
Size: 3268 Color: 4
Size: 1518 Color: 4
Size: 712 Color: 0
Size: 540 Color: 3
Size: 448 Color: 2

Bin 123: 42 of cap free
Amount of items: 2
Items: 
Size: 4135 Color: 4
Size: 2351 Color: 2

Bin 124: 42 of cap free
Amount of items: 2
Items: 
Size: 5578 Color: 2
Size: 908 Color: 1

Bin 125: 46 of cap free
Amount of items: 2
Items: 
Size: 5656 Color: 2
Size: 826 Color: 3

Bin 126: 48 of cap free
Amount of items: 2
Items: 
Size: 4876 Color: 4
Size: 1604 Color: 2

Bin 127: 76 of cap free
Amount of items: 4
Items: 
Size: 3269 Color: 0
Size: 1771 Color: 3
Size: 884 Color: 2
Size: 528 Color: 4

Bin 128: 76 of cap free
Amount of items: 2
Items: 
Size: 4742 Color: 3
Size: 1710 Color: 2

Bin 129: 86 of cap free
Amount of items: 24
Items: 
Size: 376 Color: 4
Size: 372 Color: 3
Size: 368 Color: 0
Size: 358 Color: 3
Size: 336 Color: 0
Size: 320 Color: 4
Size: 312 Color: 0
Size: 304 Color: 4
Size: 304 Color: 0
Size: 300 Color: 3
Size: 288 Color: 2
Size: 280 Color: 1
Size: 240 Color: 3
Size: 240 Color: 2
Size: 224 Color: 4
Size: 224 Color: 4
Size: 224 Color: 3
Size: 224 Color: 3
Size: 220 Color: 3
Size: 208 Color: 1
Size: 192 Color: 1
Size: 192 Color: 0
Size: 176 Color: 1
Size: 160 Color: 1

Bin 130: 89 of cap free
Amount of items: 3
Items: 
Size: 3290 Color: 1
Size: 1882 Color: 0
Size: 1267 Color: 1

Bin 131: 95 of cap free
Amount of items: 2
Items: 
Size: 3709 Color: 4
Size: 2724 Color: 2

Bin 132: 121 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 1
Size: 2717 Color: 4
Size: 416 Color: 0

Bin 133: 4992 of cap free
Amount of items: 9
Items: 
Size: 200 Color: 2
Size: 200 Color: 2
Size: 192 Color: 2
Size: 176 Color: 0
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 0
Size: 144 Color: 4
Size: 144 Color: 3

Total size: 861696
Total free space: 6528

