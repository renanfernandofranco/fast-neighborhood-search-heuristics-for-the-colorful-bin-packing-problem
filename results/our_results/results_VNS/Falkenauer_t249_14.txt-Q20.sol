Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 17
Size: 355 Color: 11
Size: 254 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 354 Color: 9
Size: 283 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 289 Color: 17
Size: 287 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 325 Color: 17
Size: 260 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 6
Size: 274 Color: 13
Size: 273 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 7
Size: 343 Color: 17
Size: 302 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 6
Size: 297 Color: 10
Size: 273 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 6
Size: 262 Color: 8
Size: 251 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 339 Color: 0
Size: 285 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 7
Size: 306 Color: 4
Size: 275 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 310 Color: 15
Size: 264 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 294 Color: 1
Size: 284 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 13
Size: 280 Color: 13
Size: 251 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 285 Color: 5
Size: 267 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 314 Color: 10
Size: 272 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 335 Color: 19
Size: 293 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 13
Size: 314 Color: 14
Size: 305 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 301 Color: 16
Size: 274 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 332 Color: 17
Size: 262 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 12
Size: 311 Color: 16
Size: 279 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 8
Size: 361 Color: 4
Size: 260 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 290 Color: 10
Size: 268 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 303 Color: 3
Size: 282 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 15
Size: 288 Color: 12
Size: 282 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 352 Color: 3
Size: 251 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 5
Size: 339 Color: 17
Size: 311 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 13
Size: 284 Color: 0
Size: 260 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 6
Size: 259 Color: 19
Size: 256 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 13
Size: 280 Color: 18
Size: 260 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 14
Size: 308 Color: 5
Size: 255 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 358 Color: 6
Size: 259 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 321 Color: 8
Size: 300 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 17
Size: 302 Color: 1
Size: 275 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 18
Size: 276 Color: 19
Size: 275 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 352 Color: 6
Size: 266 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 318 Color: 7
Size: 304 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 331 Color: 9
Size: 266 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 9
Size: 347 Color: 17
Size: 269 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 6
Size: 297 Color: 8
Size: 254 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 12
Size: 350 Color: 6
Size: 299 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 260 Color: 10
Size: 257 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 13
Size: 250 Color: 18
Size: 278 Color: 5

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 9
Size: 327 Color: 6
Size: 266 Color: 8

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 339 Color: 6
Size: 283 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 10
Size: 281 Color: 0
Size: 255 Color: 9

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 15
Size: 332 Color: 4
Size: 271 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 8
Size: 358 Color: 3
Size: 263 Color: 7

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 16
Size: 253 Color: 15
Size: 252 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 16
Size: 329 Color: 16
Size: 329 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 10
Size: 361 Color: 9
Size: 261 Color: 15

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 13
Size: 293 Color: 9
Size: 275 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 324 Color: 16
Size: 309 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 350 Color: 7
Size: 257 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 293 Color: 12
Size: 279 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 14
Size: 365 Color: 11
Size: 260 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 254 Color: 4
Size: 251 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 18
Size: 366 Color: 11
Size: 259 Color: 12

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 6
Size: 345 Color: 0
Size: 299 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 2
Size: 309 Color: 13
Size: 264 Color: 10

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 17
Size: 344 Color: 9
Size: 276 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 10
Size: 291 Color: 9
Size: 264 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 13
Size: 280 Color: 0
Size: 257 Color: 15

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 12
Size: 326 Color: 15
Size: 287 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 252 Color: 12
Size: 250 Color: 17

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 8
Size: 269 Color: 4
Size: 267 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 305 Color: 15
Size: 272 Color: 7

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 18
Size: 263 Color: 9
Size: 258 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 12
Size: 328 Color: 13
Size: 281 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 16
Size: 308 Color: 16
Size: 252 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 7
Size: 303 Color: 5
Size: 264 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 17
Size: 260 Color: 2
Size: 256 Color: 8

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 356 Color: 7
Size: 270 Color: 9

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 314 Color: 12
Size: 263 Color: 18

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 6
Size: 326 Color: 7
Size: 257 Color: 19

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 358 Color: 8
Size: 266 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 349 Color: 13
Size: 279 Color: 18

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 19
Size: 259 Color: 14
Size: 257 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 330 Color: 3
Size: 295 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 256 Color: 9
Size: 251 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 17
Size: 268 Color: 12
Size: 256 Color: 10

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 15
Size: 308 Color: 9
Size: 290 Color: 16

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 18
Size: 360 Color: 8
Size: 256 Color: 10

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 12
Size: 327 Color: 3
Size: 260 Color: 2

Total size: 83000
Total free space: 0

