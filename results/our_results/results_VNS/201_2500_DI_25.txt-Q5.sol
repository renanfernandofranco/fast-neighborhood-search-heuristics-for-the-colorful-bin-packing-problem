Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1250 Color: 0
Size: 1102 Color: 1
Size: 112 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 2
Size: 942 Color: 2
Size: 184 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1543 Color: 1
Size: 871 Color: 2
Size: 50 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 870 Color: 4
Size: 40 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 3
Size: 810 Color: 0
Size: 52 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 3
Size: 669 Color: 1
Size: 132 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 1
Size: 394 Color: 4
Size: 176 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 1
Size: 383 Color: 0
Size: 76 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 4
Size: 338 Color: 0
Size: 92 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 4
Size: 374 Color: 1
Size: 28 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2045 Color: 1
Size: 387 Color: 0
Size: 32 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 2
Size: 301 Color: 4
Size: 96 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2087 Color: 3
Size: 331 Color: 0
Size: 46 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 4
Size: 304 Color: 2
Size: 54 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2143 Color: 0
Size: 273 Color: 1
Size: 48 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2150 Color: 3
Size: 172 Color: 0
Size: 142 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 4
Size: 250 Color: 0
Size: 48 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 0
Size: 204 Color: 1
Size: 50 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 4
Size: 214 Color: 0
Size: 56 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2215 Color: 1
Size: 209 Color: 0
Size: 40 Color: 4

Bin 21: 1 of cap free
Amount of items: 4
Items: 
Size: 1235 Color: 1
Size: 1080 Color: 0
Size: 116 Color: 4
Size: 32 Color: 0

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 4
Size: 1062 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 3
Size: 578 Color: 0
Size: 128 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1905 Color: 0
Size: 542 Color: 1
Size: 16 Color: 4

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 3
Size: 502 Color: 2

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1966 Color: 1
Size: 425 Color: 4
Size: 72 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 2
Size: 442 Color: 0
Size: 52 Color: 4

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2024 Color: 2
Size: 235 Color: 2
Size: 204 Color: 0

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2050 Color: 1
Size: 413 Color: 4

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 3
Size: 269 Color: 0
Size: 72 Color: 4

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 2201 Color: 3
Size: 262 Color: 4

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 0
Size: 840 Color: 0
Size: 128 Color: 4

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 0
Size: 662 Color: 3
Size: 82 Color: 1

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 1
Size: 622 Color: 3
Size: 22 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 1
Size: 368 Color: 4
Size: 76 Color: 3

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 2055 Color: 3
Size: 267 Color: 0
Size: 140 Color: 2

Bin 37: 3 of cap free
Amount of items: 4
Items: 
Size: 1234 Color: 3
Size: 885 Color: 1
Size: 274 Color: 4
Size: 68 Color: 4

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 1
Size: 1014 Color: 0
Size: 44 Color: 3

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 3
Size: 591 Color: 0
Size: 96 Color: 1

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 2
Size: 346 Color: 0
Size: 160 Color: 4

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 1
Size: 467 Color: 3

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 2121 Color: 4
Size: 340 Color: 1

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 3
Size: 541 Color: 0
Size: 82 Color: 4

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 2098 Color: 4
Size: 362 Color: 2

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 3
Size: 481 Color: 4
Size: 44 Color: 0

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 2135 Color: 2
Size: 302 Color: 4
Size: 22 Color: 4

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 2
Size: 306 Color: 4

Bin 48: 6 of cap free
Amount of items: 5
Items: 
Size: 1025 Color: 1
Size: 771 Color: 3
Size: 286 Color: 1
Size: 200 Color: 1
Size: 176 Color: 0

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2183 Color: 4
Size: 275 Color: 3

Bin 50: 6 of cap free
Amount of items: 3
Items: 
Size: 2193 Color: 1
Size: 261 Color: 3
Size: 4 Color: 0

Bin 51: 7 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 1
Size: 887 Color: 2
Size: 148 Color: 3

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1866 Color: 0
Size: 523 Color: 1
Size: 68 Color: 3

Bin 53: 8 of cap free
Amount of items: 3
Items: 
Size: 1887 Color: 3
Size: 421 Color: 2
Size: 148 Color: 0

Bin 54: 8 of cap free
Amount of items: 2
Items: 
Size: 2169 Color: 4
Size: 287 Color: 1

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1607 Color: 0
Size: 621 Color: 3
Size: 226 Color: 1

Bin 56: 10 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 0
Size: 418 Color: 1
Size: 221 Color: 4

Bin 57: 10 of cap free
Amount of items: 2
Items: 
Size: 2103 Color: 0
Size: 351 Color: 1

Bin 58: 11 of cap free
Amount of items: 2
Items: 
Size: 1233 Color: 4
Size: 1220 Color: 3

Bin 59: 11 of cap free
Amount of items: 2
Items: 
Size: 1541 Color: 0
Size: 912 Color: 3

Bin 60: 11 of cap free
Amount of items: 2
Items: 
Size: 2138 Color: 3
Size: 315 Color: 2

Bin 61: 14 of cap free
Amount of items: 2
Items: 
Size: 1972 Color: 0
Size: 478 Color: 1

Bin 62: 15 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 3
Size: 1027 Color: 4
Size: 68 Color: 2

Bin 63: 21 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 2
Size: 722 Color: 3

Bin 64: 25 of cap free
Amount of items: 18
Items: 
Size: 715 Color: 3
Size: 168 Color: 0
Size: 152 Color: 1
Size: 140 Color: 0
Size: 124 Color: 0
Size: 122 Color: 3
Size: 116 Color: 4
Size: 108 Color: 4
Size: 104 Color: 4
Size: 104 Color: 4
Size: 92 Color: 2
Size: 88 Color: 3
Size: 84 Color: 0
Size: 80 Color: 4
Size: 66 Color: 4
Size: 64 Color: 1
Size: 56 Color: 2
Size: 56 Color: 2

Bin 65: 28 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 3
Size: 762 Color: 0

Bin 66: 2188 of cap free
Amount of items: 5
Items: 
Size: 62 Color: 0
Size: 60 Color: 3
Size: 60 Color: 3
Size: 52 Color: 2
Size: 42 Color: 1

Total size: 160160
Total free space: 2464

