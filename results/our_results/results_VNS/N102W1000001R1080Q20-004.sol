Capicity Bin: 1000001
Lower Bound: 46

Bins used: 46
Amount of Colors: 20

Bin 1: 42 of cap free
Amount of items: 3
Items: 
Size: 456556 Color: 6
Size: 399849 Color: 14
Size: 143554 Color: 6

Bin 2: 75 of cap free
Amount of items: 4
Items: 
Size: 440413 Color: 2
Size: 217620 Color: 19
Size: 181521 Color: 4
Size: 160372 Color: 15

Bin 3: 118 of cap free
Amount of items: 2
Items: 
Size: 525374 Color: 19
Size: 474509 Color: 10

Bin 4: 146 of cap free
Amount of items: 3
Items: 
Size: 461861 Color: 15
Size: 432527 Color: 14
Size: 105467 Color: 0

Bin 5: 209 of cap free
Amount of items: 3
Items: 
Size: 608635 Color: 5
Size: 247208 Color: 3
Size: 143949 Color: 4

Bin 6: 221 of cap free
Amount of items: 3
Items: 
Size: 694309 Color: 10
Size: 187764 Color: 14
Size: 117707 Color: 5

Bin 7: 236 of cap free
Amount of items: 2
Items: 
Size: 599351 Color: 9
Size: 400414 Color: 15

Bin 8: 254 of cap free
Amount of items: 3
Items: 
Size: 413896 Color: 15
Size: 397545 Color: 19
Size: 188306 Color: 3

Bin 9: 365 of cap free
Amount of items: 2
Items: 
Size: 600214 Color: 8
Size: 399422 Color: 12

Bin 10: 401 of cap free
Amount of items: 3
Items: 
Size: 492382 Color: 13
Size: 306693 Color: 15
Size: 200525 Color: 11

Bin 11: 489 of cap free
Amount of items: 2
Items: 
Size: 669915 Color: 0
Size: 329597 Color: 19

Bin 12: 745 of cap free
Amount of items: 2
Items: 
Size: 651058 Color: 15
Size: 348198 Color: 9

Bin 13: 826 of cap free
Amount of items: 2
Items: 
Size: 660202 Color: 3
Size: 338973 Color: 4

Bin 14: 940 of cap free
Amount of items: 3
Items: 
Size: 755901 Color: 12
Size: 128046 Color: 3
Size: 115114 Color: 0

Bin 15: 1556 of cap free
Amount of items: 2
Items: 
Size: 715148 Color: 9
Size: 283297 Color: 0

Bin 16: 1662 of cap free
Amount of items: 2
Items: 
Size: 650143 Color: 18
Size: 348196 Color: 12

Bin 17: 1669 of cap free
Amount of items: 2
Items: 
Size: 514518 Color: 13
Size: 483814 Color: 11

Bin 18: 1738 of cap free
Amount of items: 3
Items: 
Size: 733178 Color: 18
Size: 147537 Color: 11
Size: 117548 Color: 4

Bin 19: 2159 of cap free
Amount of items: 2
Items: 
Size: 632300 Color: 10
Size: 365542 Color: 17

Bin 20: 2501 of cap free
Amount of items: 2
Items: 
Size: 627435 Color: 13
Size: 370065 Color: 18

Bin 21: 2547 of cap free
Amount of items: 2
Items: 
Size: 760332 Color: 13
Size: 237122 Color: 3

Bin 22: 2725 of cap free
Amount of items: 2
Items: 
Size: 740506 Color: 19
Size: 256770 Color: 2

Bin 23: 3254 of cap free
Amount of items: 2
Items: 
Size: 714303 Color: 8
Size: 282444 Color: 4

Bin 24: 3517 of cap free
Amount of items: 2
Items: 
Size: 755137 Color: 11
Size: 241347 Color: 10

Bin 25: 3751 of cap free
Amount of items: 2
Items: 
Size: 679323 Color: 1
Size: 316927 Color: 12

Bin 26: 4061 of cap free
Amount of items: 2
Items: 
Size: 706063 Color: 8
Size: 289877 Color: 14

Bin 27: 4321 of cap free
Amount of items: 2
Items: 
Size: 500203 Color: 15
Size: 495477 Color: 14

Bin 28: 4511 of cap free
Amount of items: 2
Items: 
Size: 587111 Color: 6
Size: 408379 Color: 1

Bin 29: 5154 of cap free
Amount of items: 2
Items: 
Size: 772470 Color: 9
Size: 222377 Color: 11

Bin 30: 6273 of cap free
Amount of items: 2
Items: 
Size: 642342 Color: 0
Size: 351386 Color: 6

Bin 31: 7147 of cap free
Amount of items: 2
Items: 
Size: 506407 Color: 15
Size: 486447 Color: 7

Bin 32: 7700 of cap free
Amount of items: 2
Items: 
Size: 568944 Color: 3
Size: 423357 Color: 12

Bin 33: 7967 of cap free
Amount of items: 2
Items: 
Size: 769990 Color: 5
Size: 222044 Color: 9

Bin 34: 8201 of cap free
Amount of items: 2
Items: 
Size: 705822 Color: 2
Size: 285978 Color: 12

Bin 35: 8327 of cap free
Amount of items: 2
Items: 
Size: 534690 Color: 14
Size: 456984 Color: 3

Bin 36: 8476 of cap free
Amount of items: 2
Items: 
Size: 577197 Color: 10
Size: 414328 Color: 7

Bin 37: 11500 of cap free
Amount of items: 2
Items: 
Size: 653456 Color: 13
Size: 335045 Color: 18

Bin 38: 11728 of cap free
Amount of items: 2
Items: 
Size: 778115 Color: 4
Size: 210158 Color: 6

Bin 39: 18607 of cap free
Amount of items: 2
Items: 
Size: 620552 Color: 12
Size: 360842 Color: 19

Bin 40: 28121 of cap free
Amount of items: 2
Items: 
Size: 765459 Color: 9
Size: 206421 Color: 3

Bin 41: 28834 of cap free
Amount of items: 2
Items: 
Size: 577798 Color: 7
Size: 393369 Color: 5

Bin 42: 31841 of cap free
Amount of items: 2
Items: 
Size: 486165 Color: 7
Size: 481995 Color: 17

Bin 43: 40110 of cap free
Amount of items: 2
Items: 
Size: 607998 Color: 0
Size: 351893 Color: 3

Bin 44: 45965 of cap free
Amount of items: 2
Items: 
Size: 758917 Color: 17
Size: 195119 Color: 15

Bin 45: 75333 of cap free
Amount of items: 2
Items: 
Size: 620264 Color: 17
Size: 304404 Color: 6

Bin 46: 111800 of cap free
Amount of items: 2
Items: 
Size: 449435 Color: 8
Size: 438766 Color: 13

Total size: 45491923
Total free space: 508123

