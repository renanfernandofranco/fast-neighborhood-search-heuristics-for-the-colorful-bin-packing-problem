Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 3
Size: 309 Color: 1
Size: 273 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 19
Size: 341 Color: 2
Size: 269 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 16
Size: 331 Color: 1
Size: 251 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 5
Size: 309 Color: 8
Size: 261 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 19
Size: 296 Color: 3
Size: 254 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 6
Size: 333 Color: 17
Size: 315 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 15
Size: 253 Color: 17
Size: 250 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 12
Size: 268 Color: 5
Size: 253 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 273 Color: 18
Size: 267 Color: 16

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 6
Size: 309 Color: 13
Size: 296 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 6
Size: 259 Color: 4
Size: 257 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 276 Color: 8
Size: 257 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 13
Size: 256 Color: 11
Size: 253 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 3
Size: 337 Color: 3
Size: 258 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 12
Size: 291 Color: 5
Size: 275 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 5
Size: 340 Color: 0
Size: 279 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 15
Size: 321 Color: 4
Size: 272 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 6
Size: 255 Color: 2
Size: 253 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 2
Size: 276 Color: 6
Size: 256 Color: 17

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 12
Size: 282 Color: 18
Size: 276 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 269 Color: 16
Size: 305 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 7
Size: 251 Color: 5
Size: 250 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 298 Color: 11
Size: 292 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 6
Size: 254 Color: 15
Size: 250 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 14
Size: 367 Color: 7
Size: 259 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 16
Size: 330 Color: 10
Size: 271 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 9
Size: 271 Color: 15
Size: 260 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 319 Color: 10
Size: 265 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 333 Color: 18
Size: 309 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7
Size: 326 Color: 5
Size: 288 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 277 Color: 3
Size: 251 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 350 Color: 10
Size: 281 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 6
Size: 260 Color: 9
Size: 254 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 271 Color: 0
Size: 253 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 345 Color: 15
Size: 275 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 5
Size: 330 Color: 8
Size: 291 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 309 Color: 18
Size: 301 Color: 14
Size: 390 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 320 Color: 17
Size: 300 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 364 Color: 11
Size: 265 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 274 Color: 1
Size: 270 Color: 3

Total size: 40000
Total free space: 0

