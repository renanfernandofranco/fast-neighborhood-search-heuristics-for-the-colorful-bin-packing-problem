Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1493 Color: 150
Size: 601 Color: 109
Size: 378 Color: 86

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 163
Size: 499 Color: 100
Size: 242 Color: 74

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 165
Size: 634 Color: 113
Size: 92 Color: 34

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 167
Size: 462 Color: 95
Size: 204 Color: 68

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 168
Size: 606 Color: 110
Size: 54 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1876 Color: 172
Size: 556 Color: 106
Size: 40 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 176
Size: 382 Color: 87
Size: 172 Color: 61

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 178
Size: 418 Color: 91
Size: 122 Color: 49

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 182
Size: 384 Color: 88
Size: 108 Color: 43

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 183
Size: 409 Color: 89
Size: 80 Color: 27

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1986 Color: 184
Size: 478 Color: 98
Size: 8 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 189
Size: 308 Color: 81
Size: 96 Color: 37

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 191
Size: 220 Color: 69
Size: 144 Color: 55

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2122 Color: 192
Size: 314 Color: 82
Size: 36 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 193
Size: 228 Color: 70
Size: 104 Color: 41

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 194
Size: 282 Color: 78
Size: 44 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 196
Size: 234 Color: 72
Size: 44 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 199
Size: 192 Color: 63
Size: 76 Color: 25

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2210 Color: 200
Size: 174 Color: 62
Size: 88 Color: 31

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 201
Size: 200 Color: 64
Size: 60 Color: 18

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1594 Color: 155
Size: 877 Color: 127

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 174
Size: 504 Color: 102
Size: 70 Color: 21

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1971 Color: 180
Size: 500 Color: 101

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2028 Color: 187
Size: 443 Color: 92

Bin 25: 2 of cap free
Amount of items: 5
Items: 
Size: 1239 Color: 140
Size: 522 Color: 103
Size: 469 Color: 97
Size: 152 Color: 57
Size: 88 Color: 32

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 146
Size: 988 Color: 129
Size: 64 Color: 19

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 162
Size: 756 Color: 120

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1941 Color: 179
Size: 529 Color: 104

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2018 Color: 185
Size: 452 Color: 94

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2058 Color: 188
Size: 412 Color: 90

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2098 Color: 190
Size: 372 Color: 85

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2186 Color: 195
Size: 284 Color: 79

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2198 Color: 198
Size: 272 Color: 77

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1553 Color: 152
Size: 860 Color: 126
Size: 56 Color: 17

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 156
Size: 802 Color: 122
Size: 44 Color: 10

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1911 Color: 175
Size: 558 Color: 107

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1926 Color: 177
Size: 543 Color: 105

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 186
Size: 447 Color: 93

Bin 39: 4 of cap free
Amount of items: 5
Items: 
Size: 1673 Color: 158
Size: 667 Color: 115
Size: 48 Color: 14
Size: 40 Color: 8
Size: 40 Color: 7

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 181
Size: 494 Color: 99

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 2196 Color: 197
Size: 272 Color: 76

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 143
Size: 1140 Color: 137
Size: 72 Color: 24

Bin 43: 6 of cap free
Amount of items: 4
Items: 
Size: 1292 Color: 144
Size: 1030 Color: 135
Size: 72 Color: 23
Size: 72 Color: 22

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 145
Size: 993 Color: 130
Size: 64 Color: 20

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1444 Color: 148
Size: 1022 Color: 133

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1732 Color: 164
Size: 734 Color: 119

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1839 Color: 170
Size: 619 Color: 111
Size: 8 Color: 0

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1846 Color: 171
Size: 620 Color: 112

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 173
Size: 576 Color: 108
Size: 8 Color: 1

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1821 Color: 169
Size: 642 Color: 114

Bin 51: 10 of cap free
Amount of items: 3
Items: 
Size: 1572 Color: 154
Size: 846 Color: 125
Size: 44 Color: 12

Bin 52: 12 of cap free
Amount of items: 2
Items: 
Size: 1751 Color: 166
Size: 709 Color: 118

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1458 Color: 149
Size: 1000 Color: 131

Bin 54: 16 of cap free
Amount of items: 5
Items: 
Size: 1238 Color: 139
Size: 466 Color: 96
Size: 370 Color: 84
Size: 294 Color: 80
Size: 88 Color: 33

Bin 55: 17 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 157
Size: 817 Color: 123

Bin 56: 18 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 153
Size: 840 Color: 124
Size: 56 Color: 16

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 147
Size: 1031 Color: 136

Bin 58: 22 of cap free
Amount of items: 4
Items: 
Size: 1706 Color: 161
Size: 700 Color: 117
Size: 28 Color: 4
Size: 16 Color: 3

Bin 59: 25 of cap free
Amount of items: 7
Items: 
Size: 1237 Color: 138
Size: 340 Color: 83
Size: 242 Color: 75
Size: 236 Color: 73
Size: 204 Color: 67
Size: 96 Color: 36
Size: 92 Color: 35

Bin 60: 29 of cap free
Amount of items: 2
Items: 
Size: 1676 Color: 159
Size: 767 Color: 121

Bin 61: 37 of cap free
Amount of items: 4
Items: 
Size: 1246 Color: 142
Size: 1029 Color: 134
Size: 80 Color: 28
Size: 80 Color: 26

Bin 62: 38 of cap free
Amount of items: 2
Items: 
Size: 1552 Color: 151
Size: 882 Color: 128

Bin 63: 38 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 160
Size: 698 Color: 116
Size: 32 Color: 5

Bin 64: 42 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 141
Size: 1018 Color: 132
Size: 88 Color: 30
Size: 80 Color: 29

Bin 65: 44 of cap free
Amount of items: 17
Items: 
Size: 230 Color: 71
Size: 204 Color: 66
Size: 204 Color: 65
Size: 168 Color: 60
Size: 168 Color: 59
Size: 162 Color: 58
Size: 144 Color: 56
Size: 140 Color: 54
Size: 136 Color: 53
Size: 132 Color: 52
Size: 120 Color: 46
Size: 112 Color: 45
Size: 108 Color: 44
Size: 104 Color: 42
Size: 104 Color: 40
Size: 96 Color: 39
Size: 96 Color: 38

Bin 66: 1984 of cap free
Amount of items: 4
Items: 
Size: 124 Color: 51
Size: 124 Color: 50
Size: 120 Color: 48
Size: 120 Color: 47

Total size: 160680
Total free space: 2472

