Capicity Bin: 8224
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4568 Color: 15
Size: 3240 Color: 4
Size: 416 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 10
Size: 3420 Color: 18
Size: 232 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 0
Size: 2625 Color: 2
Size: 524 Color: 12

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 5168 Color: 12
Size: 3056 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 12
Size: 2046 Color: 14
Size: 488 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 5
Size: 2106 Color: 6
Size: 420 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 15
Size: 2114 Color: 5
Size: 340 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 17
Size: 2044 Color: 13
Size: 184 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 17
Size: 1570 Color: 14
Size: 608 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 17
Size: 1928 Color: 12
Size: 224 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 6
Size: 1927 Color: 0
Size: 124 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 14
Size: 1774 Color: 2
Size: 344 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6165 Color: 19
Size: 1753 Color: 16
Size: 306 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 13
Size: 1447 Color: 14
Size: 596 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6299 Color: 14
Size: 1605 Color: 14
Size: 320 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 16
Size: 1528 Color: 14
Size: 384 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6472 Color: 14
Size: 1608 Color: 8
Size: 144 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6632 Color: 14
Size: 1000 Color: 16
Size: 592 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6631 Color: 6
Size: 1185 Color: 19
Size: 408 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 8
Size: 1324 Color: 0
Size: 264 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 3
Size: 1128 Color: 10
Size: 428 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6725 Color: 5
Size: 901 Color: 2
Size: 598 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 2
Size: 1022 Color: 19
Size: 464 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 7
Size: 912 Color: 12
Size: 500 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6872 Color: 8
Size: 932 Color: 5
Size: 420 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6875 Color: 5
Size: 931 Color: 15
Size: 418 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 14
Size: 902 Color: 19
Size: 344 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 18
Size: 964 Color: 17
Size: 288 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6977 Color: 13
Size: 999 Color: 2
Size: 248 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7002 Color: 6
Size: 696 Color: 18
Size: 526 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 6
Size: 1027 Color: 14
Size: 170 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7107 Color: 8
Size: 925 Color: 9
Size: 192 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 15
Size: 888 Color: 11
Size: 228 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7112 Color: 11
Size: 936 Color: 19
Size: 176 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 8
Size: 861 Color: 18
Size: 236 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 1
Size: 910 Color: 9
Size: 180 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7135 Color: 8
Size: 909 Color: 14
Size: 180 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7168 Color: 19
Size: 680 Color: 3
Size: 376 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7176 Color: 15
Size: 868 Color: 19
Size: 180 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7207 Color: 5
Size: 849 Color: 4
Size: 168 Color: 15

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 15
Size: 776 Color: 6
Size: 198 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 17
Size: 684 Color: 4
Size: 286 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7342 Color: 1
Size: 598 Color: 15
Size: 284 Color: 5

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7400 Color: 14
Size: 680 Color: 7
Size: 144 Color: 19

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 4625 Color: 12
Size: 3422 Color: 3
Size: 176 Color: 16

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4641 Color: 19
Size: 2331 Color: 12
Size: 1251 Color: 14

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 4654 Color: 7
Size: 2993 Color: 4
Size: 576 Color: 16

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 16
Size: 2987 Color: 1
Size: 288 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5059 Color: 4
Size: 3044 Color: 7
Size: 120 Color: 6

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 5967 Color: 0
Size: 2088 Color: 13
Size: 168 Color: 3

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 19
Size: 1881 Color: 3

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 1
Size: 712 Color: 6
Size: 526 Color: 14

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 11
Size: 1131 Color: 13

Bin 54: 2 of cap free
Amount of items: 5
Items: 
Size: 4116 Color: 7
Size: 1800 Color: 8
Size: 1174 Color: 13
Size: 684 Color: 8
Size: 448 Color: 0

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 6
Size: 2146 Color: 19
Size: 296 Color: 2

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6132 Color: 4
Size: 1904 Color: 1
Size: 186 Color: 14

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 15
Size: 1818 Color: 9
Size: 112 Color: 19

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6776 Color: 19
Size: 1446 Color: 5

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 4
Size: 1125 Color: 14
Size: 280 Color: 0

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6818 Color: 19
Size: 1404 Color: 15

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 7188 Color: 10
Size: 1034 Color: 16

Bin 62: 2 of cap free
Amount of items: 4
Items: 
Size: 7384 Color: 8
Size: 814 Color: 10
Size: 16 Color: 9
Size: 8 Color: 1

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 11
Size: 2888 Color: 2
Size: 266 Color: 10

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 6
Size: 2085 Color: 12
Size: 400 Color: 15

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 5961 Color: 13
Size: 2260 Color: 8

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 9
Size: 1887 Color: 12

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 6540 Color: 1
Size: 1429 Color: 2
Size: 252 Color: 8

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 6786 Color: 3
Size: 1435 Color: 19

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 11
Size: 2902 Color: 9
Size: 206 Color: 4

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 5274 Color: 3
Size: 2946 Color: 8

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 5429 Color: 13
Size: 2631 Color: 10
Size: 160 Color: 10

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 5706 Color: 2
Size: 2514 Color: 16

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 13
Size: 1748 Color: 0
Size: 80 Color: 15

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 9
Size: 1280 Color: 10
Size: 512 Color: 18

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 11
Size: 1416 Color: 12

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 7193 Color: 0
Size: 1026 Color: 13

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 14
Size: 915 Color: 4

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 4633 Color: 14
Size: 3425 Color: 9
Size: 160 Color: 6

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 5258 Color: 16
Size: 2600 Color: 16
Size: 360 Color: 6

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 6
Size: 2466 Color: 9
Size: 492 Color: 4

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 5650 Color: 10
Size: 2568 Color: 16

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 1
Size: 1266 Color: 4

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 7032 Color: 11
Size: 1186 Color: 2

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 18
Size: 918 Color: 13

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 18
Size: 880 Color: 16

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6717 Color: 17
Size: 1500 Color: 2

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 4746 Color: 14
Size: 2978 Color: 11
Size: 492 Color: 17

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 6511 Color: 2
Size: 1257 Color: 10
Size: 448 Color: 16

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 7076 Color: 1
Size: 1140 Color: 8

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 16
Size: 1012 Color: 11

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 10
Size: 1712 Color: 9

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 7150 Color: 5
Size: 1064 Color: 17

Bin 93: 12 of cap free
Amount of items: 4
Items: 
Size: 4120 Color: 7
Size: 2337 Color: 6
Size: 1175 Color: 5
Size: 580 Color: 3

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 5210 Color: 5
Size: 3001 Color: 15

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 7126 Color: 6
Size: 1084 Color: 1

Bin 96: 15 of cap free
Amount of items: 2
Items: 
Size: 6349 Color: 16
Size: 1860 Color: 6

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 13
Size: 2406 Color: 10
Size: 1042 Color: 14

Bin 98: 19 of cap free
Amount of items: 2
Items: 
Size: 5723 Color: 9
Size: 2482 Color: 12

Bin 99: 20 of cap free
Amount of items: 6
Items: 
Size: 4114 Color: 4
Size: 948 Color: 12
Size: 928 Color: 11
Size: 898 Color: 16
Size: 772 Color: 6
Size: 544 Color: 10

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 19
Size: 1335 Color: 18

Bin 101: 23 of cap free
Amount of items: 2
Items: 
Size: 6993 Color: 16
Size: 1208 Color: 7

Bin 102: 27 of cap free
Amount of items: 8
Items: 
Size: 4115 Color: 8
Size: 742 Color: 1
Size: 738 Color: 10
Size: 708 Color: 19
Size: 672 Color: 11
Size: 652 Color: 3
Size: 320 Color: 0
Size: 250 Color: 2

Bin 103: 28 of cap free
Amount of items: 4
Items: 
Size: 4122 Color: 5
Size: 2462 Color: 13
Size: 1300 Color: 14
Size: 312 Color: 15

Bin 104: 28 of cap free
Amount of items: 2
Items: 
Size: 4738 Color: 10
Size: 3458 Color: 17

Bin 105: 28 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 7
Size: 1202 Color: 15

Bin 106: 30 of cap free
Amount of items: 2
Items: 
Size: 6582 Color: 10
Size: 1612 Color: 16

Bin 107: 31 of cap free
Amount of items: 2
Items: 
Size: 6490 Color: 7
Size: 1703 Color: 12

Bin 108: 33 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 6
Size: 2091 Color: 19
Size: 588 Color: 4

Bin 109: 33 of cap free
Amount of items: 2
Items: 
Size: 5715 Color: 16
Size: 2476 Color: 12

Bin 110: 34 of cap free
Amount of items: 2
Items: 
Size: 7380 Color: 6
Size: 810 Color: 10

Bin 111: 35 of cap free
Amount of items: 2
Items: 
Size: 5421 Color: 11
Size: 2768 Color: 10

Bin 112: 35 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 6
Size: 1329 Color: 16

Bin 113: 37 of cap free
Amount of items: 2
Items: 
Size: 7143 Color: 4
Size: 1044 Color: 18

Bin 114: 38 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 1
Size: 1563 Color: 0

Bin 115: 41 of cap free
Amount of items: 2
Items: 
Size: 7142 Color: 19
Size: 1041 Color: 1

Bin 116: 48 of cap free
Amount of items: 2
Items: 
Size: 5912 Color: 15
Size: 2264 Color: 19

Bin 117: 50 of cap free
Amount of items: 2
Items: 
Size: 6932 Color: 10
Size: 1242 Color: 9

Bin 118: 52 of cap free
Amount of items: 34
Items: 
Size: 368 Color: 18
Size: 352 Color: 6
Size: 312 Color: 18
Size: 304 Color: 16
Size: 304 Color: 15
Size: 304 Color: 14
Size: 288 Color: 2
Size: 272 Color: 5
Size: 264 Color: 19
Size: 256 Color: 18
Size: 256 Color: 16
Size: 256 Color: 4
Size: 244 Color: 0
Size: 240 Color: 16
Size: 236 Color: 10
Size: 236 Color: 1
Size: 232 Color: 17
Size: 232 Color: 7
Size: 226 Color: 8
Size: 224 Color: 19
Size: 224 Color: 14
Size: 208 Color: 19
Size: 208 Color: 17
Size: 208 Color: 0
Size: 206 Color: 6
Size: 204 Color: 18
Size: 204 Color: 12
Size: 204 Color: 2
Size: 192 Color: 4
Size: 184 Color: 15
Size: 184 Color: 11
Size: 184 Color: 11
Size: 180 Color: 9
Size: 176 Color: 2

Bin 119: 52 of cap free
Amount of items: 2
Items: 
Size: 5266 Color: 1
Size: 2906 Color: 19

Bin 120: 52 of cap free
Amount of items: 2
Items: 
Size: 6802 Color: 1
Size: 1370 Color: 0

Bin 121: 54 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 19
Size: 1464 Color: 12

Bin 122: 58 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 19
Size: 1180 Color: 11

Bin 123: 60 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 13
Size: 2914 Color: 1

Bin 124: 61 of cap free
Amount of items: 10
Items: 
Size: 4113 Color: 18
Size: 580 Color: 17
Size: 576 Color: 1
Size: 492 Color: 8
Size: 488 Color: 4
Size: 466 Color: 15
Size: 416 Color: 4
Size: 400 Color: 1
Size: 376 Color: 6
Size: 256 Color: 0

Bin 125: 62 of cap free
Amount of items: 2
Items: 
Size: 4730 Color: 7
Size: 3432 Color: 3

Bin 126: 69 of cap free
Amount of items: 2
Items: 
Size: 5516 Color: 19
Size: 2639 Color: 3

Bin 127: 70 of cap free
Amount of items: 2
Items: 
Size: 5680 Color: 5
Size: 2474 Color: 11

Bin 128: 73 of cap free
Amount of items: 2
Items: 
Size: 6489 Color: 13
Size: 1662 Color: 2

Bin 129: 76 of cap free
Amount of items: 2
Items: 
Size: 7115 Color: 18
Size: 1033 Color: 9

Bin 130: 92 of cap free
Amount of items: 2
Items: 
Size: 5400 Color: 12
Size: 2732 Color: 1

Bin 131: 107 of cap free
Amount of items: 2
Items: 
Size: 4690 Color: 5
Size: 3427 Color: 12

Bin 132: 112 of cap free
Amount of items: 2
Items: 
Size: 4124 Color: 19
Size: 3988 Color: 5

Bin 133: 6274 of cap free
Amount of items: 12
Items: 
Size: 204 Color: 6
Size: 200 Color: 0
Size: 182 Color: 15
Size: 180 Color: 13
Size: 176 Color: 4
Size: 160 Color: 3
Size: 160 Color: 2
Size: 152 Color: 9
Size: 144 Color: 13
Size: 136 Color: 14
Size: 128 Color: 15
Size: 128 Color: 11

Total size: 1085568
Total free space: 8224

