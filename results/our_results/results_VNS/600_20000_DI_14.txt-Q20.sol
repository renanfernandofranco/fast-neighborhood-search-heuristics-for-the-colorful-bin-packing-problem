Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 7672 Color: 13
Size: 2058 Color: 1
Size: 1546 Color: 16
Size: 1492 Color: 17
Size: 1484 Color: 18
Size: 628 Color: 3
Size: 448 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 3
Size: 6184 Color: 7
Size: 448 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9900 Color: 17
Size: 5132 Color: 14
Size: 296 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10093 Color: 12
Size: 4571 Color: 17
Size: 664 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10668 Color: 15
Size: 3900 Color: 3
Size: 760 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 18
Size: 3820 Color: 9
Size: 760 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10764 Color: 1
Size: 4084 Color: 3
Size: 480 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 12
Size: 3884 Color: 0
Size: 418 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11057 Color: 12
Size: 3923 Color: 0
Size: 348 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11065 Color: 8
Size: 3553 Color: 7
Size: 710 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 1
Size: 3836 Color: 13
Size: 408 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 11
Size: 3060 Color: 17
Size: 714 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11962 Color: 14
Size: 3134 Color: 7
Size: 232 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 15
Size: 2936 Color: 18
Size: 336 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 13
Size: 2268 Color: 0
Size: 952 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 10
Size: 2251 Color: 13
Size: 960 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12124 Color: 0
Size: 2676 Color: 19
Size: 528 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 8
Size: 2264 Color: 9
Size: 832 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12133 Color: 19
Size: 2663 Color: 5
Size: 532 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12152 Color: 4
Size: 3064 Color: 7
Size: 112 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12290 Color: 17
Size: 2530 Color: 12
Size: 508 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12381 Color: 15
Size: 2219 Color: 15
Size: 728 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 17
Size: 2306 Color: 6
Size: 582 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 3
Size: 1784 Color: 4
Size: 1008 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 10
Size: 2056 Color: 1
Size: 720 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 17
Size: 2308 Color: 16
Size: 408 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 10
Size: 2584 Color: 9
Size: 128 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12650 Color: 8
Size: 1402 Color: 10
Size: 1276 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12674 Color: 18
Size: 2026 Color: 5
Size: 628 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 3
Size: 1786 Color: 11
Size: 822 Color: 15

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12825 Color: 8
Size: 1921 Color: 17
Size: 582 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12809 Color: 9
Size: 2205 Color: 5
Size: 314 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12856 Color: 0
Size: 1766 Color: 5
Size: 706 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 8
Size: 1848 Color: 17
Size: 608 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12898 Color: 9
Size: 1454 Color: 6
Size: 976 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12903 Color: 0
Size: 1897 Color: 4
Size: 528 Color: 14

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 8
Size: 2044 Color: 10
Size: 312 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13023 Color: 13
Size: 1831 Color: 18
Size: 474 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 15
Size: 1126 Color: 18
Size: 1072 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 0
Size: 1264 Color: 4
Size: 920 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 13
Size: 1621 Color: 0
Size: 544 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 14
Size: 1620 Color: 5
Size: 504 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 15
Size: 1672 Color: 16
Size: 444 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 13
Size: 1100 Color: 19
Size: 904 Color: 12

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13327 Color: 17
Size: 1561 Color: 13
Size: 440 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 16
Size: 1352 Color: 4
Size: 624 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13356 Color: 4
Size: 1594 Color: 5
Size: 378 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 10
Size: 1526 Color: 13
Size: 328 Color: 16

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 2
Size: 1364 Color: 9
Size: 388 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 19
Size: 1272 Color: 5
Size: 356 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 16
Size: 1256 Color: 14
Size: 352 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13736 Color: 6
Size: 1232 Color: 13
Size: 360 Color: 5

Bin 53: 1 of cap free
Amount of items: 9
Items: 
Size: 7667 Color: 13
Size: 1276 Color: 4
Size: 1272 Color: 18
Size: 1120 Color: 19
Size: 1120 Color: 5
Size: 1104 Color: 12
Size: 784 Color: 1
Size: 704 Color: 3
Size: 280 Color: 4

Bin 54: 1 of cap free
Amount of items: 5
Items: 
Size: 7665 Color: 18
Size: 3524 Color: 2
Size: 1956 Color: 19
Size: 1466 Color: 6
Size: 716 Color: 7

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 8593 Color: 16
Size: 6382 Color: 8
Size: 352 Color: 17

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 9
Size: 5696 Color: 8
Size: 168 Color: 5

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 9953 Color: 12
Size: 4556 Color: 7
Size: 818 Color: 10

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 11189 Color: 0
Size: 4138 Color: 9

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11249 Color: 13
Size: 3542 Color: 2
Size: 536 Color: 5

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 17
Size: 3581 Color: 8
Size: 176 Color: 19

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11781 Color: 15
Size: 3146 Color: 19
Size: 400 Color: 11

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11950 Color: 10
Size: 2917 Color: 10
Size: 460 Color: 16

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11966 Color: 13
Size: 3151 Color: 9
Size: 210 Color: 6

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 12045 Color: 19
Size: 3282 Color: 8

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12683 Color: 2
Size: 2292 Color: 5
Size: 352 Color: 16

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 3
Size: 1832 Color: 8
Size: 442 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 5
Size: 1774 Color: 13
Size: 324 Color: 8

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 19
Size: 1575 Color: 13
Size: 416 Color: 7

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13371 Color: 17
Size: 1406 Color: 19
Size: 550 Color: 13

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 13383 Color: 7
Size: 1944 Color: 8

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13439 Color: 17
Size: 1024 Color: 5
Size: 864 Color: 1

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13455 Color: 2
Size: 1248 Color: 17
Size: 624 Color: 19

Bin 73: 2 of cap free
Amount of items: 21
Items: 
Size: 944 Color: 13
Size: 936 Color: 4
Size: 904 Color: 16
Size: 898 Color: 6
Size: 896 Color: 4
Size: 880 Color: 16
Size: 826 Color: 17
Size: 818 Color: 19
Size: 816 Color: 17
Size: 816 Color: 12
Size: 814 Color: 13
Size: 776 Color: 10
Size: 776 Color: 8
Size: 768 Color: 1
Size: 736 Color: 2
Size: 608 Color: 1
Size: 504 Color: 3
Size: 472 Color: 11
Size: 450 Color: 0
Size: 400 Color: 11
Size: 288 Color: 3

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 8876 Color: 8
Size: 5502 Color: 7
Size: 948 Color: 14

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 9594 Color: 11
Size: 5476 Color: 0
Size: 256 Color: 4

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 4
Size: 5458 Color: 12

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 8
Size: 4604 Color: 13
Size: 586 Color: 15

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 10426 Color: 15
Size: 4516 Color: 5
Size: 384 Color: 9

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11009 Color: 13
Size: 4093 Color: 3
Size: 224 Color: 12

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11245 Color: 9
Size: 3601 Color: 6
Size: 480 Color: 18

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 14
Size: 3213 Color: 6
Size: 308 Color: 0

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 6
Size: 2648 Color: 8
Size: 384 Color: 15

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 12954 Color: 1
Size: 2372 Color: 14

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 7
Size: 2214 Color: 13

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 16
Size: 1644 Color: 7
Size: 96 Color: 13

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 11
Size: 6284 Color: 13
Size: 440 Color: 8

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 10361 Color: 5
Size: 4620 Color: 18
Size: 344 Color: 1

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 10409 Color: 2
Size: 4524 Color: 13
Size: 392 Color: 3

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 10417 Color: 2
Size: 4744 Color: 18
Size: 164 Color: 8

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 10632 Color: 14
Size: 4501 Color: 13
Size: 192 Color: 1

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 10732 Color: 11
Size: 4081 Color: 0
Size: 512 Color: 14

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 19
Size: 4101 Color: 17
Size: 192 Color: 17

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 3
Size: 3389 Color: 9
Size: 360 Color: 6

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 11683 Color: 5
Size: 3642 Color: 11

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 5
Size: 2377 Color: 1
Size: 72 Color: 11

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 6
Size: 1448 Color: 11
Size: 704 Color: 8

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 19
Size: 2072 Color: 3

Bin 98: 3 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 1
Size: 2021 Color: 0

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 9929 Color: 3
Size: 3561 Color: 15
Size: 1834 Color: 4

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 10652 Color: 6
Size: 4488 Color: 13
Size: 184 Color: 14

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 12
Size: 1560 Color: 14

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 10958 Color: 4
Size: 3541 Color: 2
Size: 824 Color: 8

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 11460 Color: 2
Size: 3863 Color: 9

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 11940 Color: 11
Size: 3383 Color: 8

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 14
Size: 3051 Color: 4

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13692 Color: 10
Size: 1631 Color: 9

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 9804 Color: 9
Size: 5518 Color: 17

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 10433 Color: 19
Size: 4889 Color: 13

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 12269 Color: 9
Size: 2957 Color: 13
Size: 96 Color: 9

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 6
Size: 1982 Color: 4

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 13418 Color: 2
Size: 1904 Color: 19

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 13646 Color: 4
Size: 1676 Color: 8

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 13786 Color: 15
Size: 1496 Color: 4
Size: 40 Color: 5

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 18
Size: 4311 Color: 4

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 11033 Color: 6
Size: 4136 Color: 10
Size: 152 Color: 6

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 12493 Color: 7
Size: 2828 Color: 10

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 19
Size: 2757 Color: 13

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 9
Size: 2363 Color: 17
Size: 96 Color: 7

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 13570 Color: 4
Size: 1751 Color: 1

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 8600 Color: 3
Size: 6720 Color: 15

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 10385 Color: 6
Size: 4935 Color: 11

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 15
Size: 1772 Color: 0

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 5
Size: 5003 Color: 13
Size: 608 Color: 5

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 11233 Color: 6
Size: 4086 Color: 2

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 13322 Color: 0
Size: 1997 Color: 17

Bin 126: 10 of cap free
Amount of items: 7
Items: 
Size: 7668 Color: 15
Size: 1476 Color: 5
Size: 1364 Color: 4
Size: 1286 Color: 8
Size: 1276 Color: 17
Size: 1272 Color: 19
Size: 976 Color: 14

Bin 127: 10 of cap free
Amount of items: 3
Items: 
Size: 9916 Color: 18
Size: 4730 Color: 14
Size: 672 Color: 5

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 11082 Color: 3
Size: 3884 Color: 5
Size: 352 Color: 18

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 15
Size: 2691 Color: 10

Bin 130: 11 of cap free
Amount of items: 3
Items: 
Size: 11269 Color: 5
Size: 3128 Color: 8
Size: 920 Color: 0

Bin 131: 11 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 16
Size: 3501 Color: 17

Bin 132: 11 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 15
Size: 2344 Color: 18

Bin 133: 11 of cap free
Amount of items: 2
Items: 
Size: 12989 Color: 16
Size: 2328 Color: 9

Bin 134: 12 of cap free
Amount of items: 3
Items: 
Size: 10366 Color: 11
Size: 4742 Color: 13
Size: 208 Color: 8

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 15
Size: 2328 Color: 11

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 8
Size: 1674 Color: 2

Bin 137: 15 of cap free
Amount of items: 2
Items: 
Size: 11711 Color: 7
Size: 3602 Color: 1

Bin 138: 16 of cap free
Amount of items: 3
Items: 
Size: 7764 Color: 0
Size: 7324 Color: 16
Size: 224 Color: 10

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 9256 Color: 17
Size: 6056 Color: 11

Bin 140: 16 of cap free
Amount of items: 3
Items: 
Size: 9560 Color: 3
Size: 5528 Color: 4
Size: 224 Color: 8

Bin 141: 16 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 5
Size: 1924 Color: 11

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 14
Size: 2551 Color: 6

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 16
Size: 2728 Color: 13

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 9654 Color: 11
Size: 5653 Color: 0

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 13343 Color: 4
Size: 1964 Color: 17

Bin 146: 23 of cap free
Amount of items: 2
Items: 
Size: 10621 Color: 3
Size: 4684 Color: 16

Bin 147: 23 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 3
Size: 3928 Color: 6
Size: 272 Color: 8

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 13202 Color: 15
Size: 2101 Color: 19

Bin 149: 28 of cap free
Amount of items: 2
Items: 
Size: 7700 Color: 6
Size: 7600 Color: 16

Bin 150: 28 of cap free
Amount of items: 3
Items: 
Size: 9740 Color: 15
Size: 3368 Color: 18
Size: 2192 Color: 8

Bin 151: 29 of cap free
Amount of items: 2
Items: 
Size: 13338 Color: 18
Size: 1961 Color: 1

Bin 152: 30 of cap free
Amount of items: 3
Items: 
Size: 7796 Color: 8
Size: 6372 Color: 12
Size: 1130 Color: 0

Bin 153: 32 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 7
Size: 1688 Color: 15

Bin 154: 33 of cap free
Amount of items: 2
Items: 
Size: 13498 Color: 12
Size: 1797 Color: 11

Bin 155: 33 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 5
Size: 1731 Color: 14

Bin 156: 34 of cap free
Amount of items: 4
Items: 
Size: 7674 Color: 1
Size: 4782 Color: 11
Size: 2534 Color: 14
Size: 304 Color: 4

Bin 157: 35 of cap free
Amount of items: 3
Items: 
Size: 8545 Color: 7
Size: 6308 Color: 13
Size: 440 Color: 6

Bin 158: 36 of cap free
Amount of items: 3
Items: 
Size: 8710 Color: 6
Size: 4540 Color: 8
Size: 2042 Color: 16

Bin 159: 38 of cap free
Amount of items: 2
Items: 
Size: 12484 Color: 18
Size: 2806 Color: 2

Bin 160: 38 of cap free
Amount of items: 2
Items: 
Size: 12882 Color: 10
Size: 2408 Color: 13

Bin 161: 40 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 7
Size: 2096 Color: 4

Bin 162: 43 of cap free
Amount of items: 2
Items: 
Size: 11549 Color: 6
Size: 3736 Color: 4

Bin 163: 45 of cap free
Amount of items: 3
Items: 
Size: 9172 Color: 17
Size: 5607 Color: 19
Size: 504 Color: 6

Bin 164: 48 of cap free
Amount of items: 3
Items: 
Size: 9472 Color: 8
Size: 5608 Color: 4
Size: 200 Color: 18

Bin 165: 49 of cap free
Amount of items: 2
Items: 
Size: 12477 Color: 19
Size: 2802 Color: 10

Bin 166: 50 of cap free
Amount of items: 2
Items: 
Size: 13133 Color: 16
Size: 2145 Color: 2

Bin 167: 55 of cap free
Amount of items: 2
Items: 
Size: 9640 Color: 1
Size: 5633 Color: 5

Bin 168: 55 of cap free
Amount of items: 2
Items: 
Size: 13186 Color: 12
Size: 2087 Color: 7

Bin 169: 57 of cap free
Amount of items: 3
Items: 
Size: 8569 Color: 5
Size: 6386 Color: 14
Size: 316 Color: 4

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 10457 Color: 9
Size: 4808 Color: 11

Bin 171: 63 of cap free
Amount of items: 2
Items: 
Size: 12667 Color: 14
Size: 2598 Color: 9

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 9884 Color: 12
Size: 5380 Color: 11

Bin 173: 66 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 14
Size: 2684 Color: 7

Bin 174: 72 of cap free
Amount of items: 2
Items: 
Size: 10376 Color: 15
Size: 4880 Color: 3

Bin 175: 76 of cap free
Amount of items: 2
Items: 
Size: 11660 Color: 16
Size: 3592 Color: 0

Bin 176: 77 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 15
Size: 5613 Color: 1

Bin 177: 78 of cap free
Amount of items: 2
Items: 
Size: 11837 Color: 14
Size: 3413 Color: 1

Bin 178: 79 of cap free
Amount of items: 2
Items: 
Size: 12021 Color: 17
Size: 3228 Color: 1

Bin 179: 86 of cap free
Amount of items: 2
Items: 
Size: 11656 Color: 1
Size: 3586 Color: 0

Bin 180: 87 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 8
Size: 6385 Color: 2
Size: 944 Color: 18

Bin 181: 87 of cap free
Amount of items: 2
Items: 
Size: 9325 Color: 13
Size: 5916 Color: 18

Bin 182: 87 of cap free
Amount of items: 2
Items: 
Size: 11100 Color: 6
Size: 4141 Color: 14

Bin 183: 88 of cap free
Amount of items: 2
Items: 
Size: 12303 Color: 6
Size: 2937 Color: 9

Bin 184: 89 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 10
Size: 2677 Color: 7

Bin 185: 92 of cap free
Amount of items: 2
Items: 
Size: 12453 Color: 8
Size: 2783 Color: 19

Bin 186: 98 of cap free
Amount of items: 2
Items: 
Size: 11829 Color: 9
Size: 3401 Color: 14

Bin 187: 101 of cap free
Amount of items: 2
Items: 
Size: 11288 Color: 16
Size: 3939 Color: 1

Bin 188: 108 of cap free
Amount of items: 31
Items: 
Size: 718 Color: 19
Size: 716 Color: 8
Size: 710 Color: 4
Size: 682 Color: 12
Size: 676 Color: 16
Size: 640 Color: 14
Size: 590 Color: 6
Size: 580 Color: 7
Size: 576 Color: 18
Size: 560 Color: 15
Size: 560 Color: 9
Size: 560 Color: 6
Size: 560 Color: 5
Size: 534 Color: 16
Size: 472 Color: 3
Size: 464 Color: 15
Size: 456 Color: 17
Size: 456 Color: 3
Size: 448 Color: 7
Size: 416 Color: 6
Size: 404 Color: 19
Size: 404 Color: 18
Size: 404 Color: 18
Size: 404 Color: 12
Size: 392 Color: 0
Size: 330 Color: 11
Size: 328 Color: 1
Size: 320 Color: 1
Size: 292 Color: 7
Size: 288 Color: 11
Size: 280 Color: 0

Bin 189: 108 of cap free
Amount of items: 3
Items: 
Size: 9788 Color: 17
Size: 2911 Color: 9
Size: 2521 Color: 5

Bin 190: 126 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 10
Size: 5064 Color: 17
Size: 1122 Color: 16

Bin 191: 126 of cap free
Amount of items: 2
Items: 
Size: 11081 Color: 3
Size: 4121 Color: 15

Bin 192: 132 of cap free
Amount of items: 2
Items: 
Size: 8804 Color: 5
Size: 6392 Color: 12

Bin 193: 136 of cap free
Amount of items: 2
Items: 
Size: 9960 Color: 9
Size: 5232 Color: 6

Bin 194: 144 of cap free
Amount of items: 5
Items: 
Size: 7684 Color: 15
Size: 2234 Color: 11
Size: 1951 Color: 18
Size: 1660 Color: 14
Size: 1655 Color: 14

Bin 195: 144 of cap free
Amount of items: 2
Items: 
Size: 10856 Color: 8
Size: 4328 Color: 7

Bin 196: 158 of cap free
Amount of items: 2
Items: 
Size: 8782 Color: 0
Size: 6388 Color: 2

Bin 197: 215 of cap free
Amount of items: 2
Items: 
Size: 8726 Color: 5
Size: 6387 Color: 18

Bin 198: 236 of cap free
Amount of items: 9
Items: 
Size: 7666 Color: 16
Size: 1100 Color: 4
Size: 1088 Color: 15
Size: 1008 Color: 5
Size: 1000 Color: 7
Size: 944 Color: 14
Size: 904 Color: 6
Size: 704 Color: 0
Size: 678 Color: 11

Bin 199: 10800 of cap free
Amount of items: 15
Items: 
Size: 364 Color: 15
Size: 364 Color: 15
Size: 358 Color: 13
Size: 332 Color: 9
Size: 328 Color: 10
Size: 326 Color: 19
Size: 320 Color: 5
Size: 304 Color: 1
Size: 272 Color: 19
Size: 272 Color: 0
Size: 264 Color: 11
Size: 256 Color: 17
Size: 256 Color: 7
Size: 256 Color: 6
Size: 256 Color: 3

Total size: 3034944
Total free space: 15328

