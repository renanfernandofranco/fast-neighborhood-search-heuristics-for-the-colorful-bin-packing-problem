Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 313 Color: 0
Size: 298 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 305 Color: 0
Size: 280 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 355 Color: 1
Size: 251 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 255 Color: 1
Size: 254 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 334 Color: 1
Size: 291 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 277 Color: 1
Size: 258 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 253 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 268 Color: 1
Size: 258 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 284 Color: 0
Size: 281 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 250 Color: 0
Size: 256 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 268 Color: 1
Size: 252 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 288 Color: 1
Size: 250 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 280 Color: 0
Size: 306 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 272 Color: 1
Size: 259 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 333 Color: 1
Size: 259 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 303 Color: 1
Size: 264 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 335 Color: 1
Size: 272 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 358 Color: 1
Size: 279 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 318 Color: 1
Size: 289 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 349 Color: 1
Size: 278 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 275 Color: 1
Size: 254 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 315 Color: 1
Size: 275 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 298 Color: 1
Size: 255 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 309 Color: 1
Size: 271 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 312 Color: 1
Size: 275 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 351 Color: 1
Size: 260 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 290 Color: 1
Size: 257 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 355 Color: 1
Size: 282 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 307 Color: 1
Size: 270 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 343 Color: 1
Size: 299 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 260 Color: 1
Size: 258 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 282 Color: 1
Size: 261 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 282 Color: 1
Size: 256 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 329 Color: 1
Size: 269 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 303 Color: 1
Size: 273 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 332 Color: 1
Size: 257 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 281 Color: 1
Size: 257 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 340 Color: 1
Size: 289 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 332 Color: 1
Size: 285 Color: 0

Total size: 40000
Total free space: 0

