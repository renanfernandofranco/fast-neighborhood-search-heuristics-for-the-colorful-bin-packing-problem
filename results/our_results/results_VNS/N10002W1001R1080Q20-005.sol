Capicity Bin: 1001
Lower Bound: 4534

Bins used: 4537
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 6
Size: 288 Color: 13

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 13
Size: 265 Color: 12

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 15
Size: 312 Color: 5

Bin 4: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 17
Size: 217 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 6
Size: 285 Color: 17
Size: 112 Color: 11

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 19
Size: 265 Color: 5

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 19
Size: 242 Color: 15

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 15
Size: 271 Color: 11

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 17
Size: 243 Color: 5

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 16
Size: 200 Color: 6

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 15
Size: 309 Color: 12

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 14
Size: 269 Color: 8

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 4
Size: 251 Color: 19

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 17
Size: 243 Color: 8

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 18
Size: 225 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 14
Size: 289 Color: 9
Size: 234 Color: 17

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 11
Size: 437 Color: 7

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 17
Size: 384 Color: 8

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 15
Size: 424 Color: 19

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 16
Size: 473 Color: 14

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 8

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 5
Size: 345 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 19
Size: 176 Color: 12
Size: 123 Color: 12

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 18
Size: 315 Color: 8

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 4
Size: 205 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 5
Size: 323 Color: 10
Size: 161 Color: 7

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 425 Color: 5

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 14
Size: 192 Color: 17
Size: 175 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 18
Size: 120 Color: 4
Size: 116 Color: 17

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 18
Size: 230 Color: 5

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 17
Size: 282 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 7
Size: 190 Color: 15
Size: 107 Color: 12

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 480 Color: 14

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 19
Size: 200 Color: 10

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 7
Size: 451 Color: 9

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 14
Size: 390 Color: 10

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 13
Size: 213 Color: 14

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 18
Size: 128 Color: 15
Size: 127 Color: 14

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 318 Color: 10

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 336 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 1
Size: 159 Color: 15
Size: 143 Color: 12

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 17
Size: 202 Color: 19

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 14
Size: 438 Color: 16

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 10
Size: 227 Color: 14

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 387 Color: 1

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 3
Size: 326 Color: 8

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 4
Size: 464 Color: 14

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 5
Size: 383 Color: 4

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 18
Size: 341 Color: 11

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 6
Size: 320 Color: 11

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 10
Size: 211 Color: 4

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 14
Size: 207 Color: 9

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 371 Color: 6

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 19
Size: 331 Color: 5

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 14
Size: 306 Color: 16

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 6
Size: 426 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 4
Size: 192 Color: 14
Size: 123 Color: 12

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 12
Size: 409 Color: 8

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 11
Size: 251 Color: 2

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 1
Size: 339 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 4
Size: 344 Color: 9
Size: 312 Color: 17

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 345 Color: 13

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 7
Size: 472 Color: 6

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 9
Size: 347 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 18
Size: 172 Color: 18
Size: 118 Color: 11

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 19
Size: 404 Color: 15

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 18
Size: 203 Color: 14

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 3
Size: 321 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 16
Size: 245 Color: 11
Size: 154 Color: 8

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 12
Size: 408 Color: 15

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 6
Size: 362 Color: 8

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 6
Size: 304 Color: 19

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 13
Size: 438 Color: 1

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 15
Size: 263 Color: 5

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 17
Size: 224 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 14
Size: 226 Color: 18
Size: 171 Color: 3

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 6
Size: 372 Color: 3

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 17
Size: 231 Color: 2

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 5

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 8
Size: 368 Color: 0

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 4
Size: 274 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 5
Size: 361 Color: 9
Size: 146 Color: 14

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 19
Size: 263 Color: 12

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 13
Size: 390 Color: 11

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 11
Size: 216 Color: 4

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 16
Size: 236 Color: 11

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 15
Size: 482 Color: 1

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 3

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 14
Size: 473 Color: 0

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 7
Size: 417 Color: 11

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 6
Size: 471 Color: 8

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 4
Size: 411 Color: 18

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 1
Size: 366 Color: 16

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 10
Size: 357 Color: 14

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 11
Size: 411 Color: 3

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 7
Size: 344 Color: 2

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 3
Size: 452 Color: 12

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 6
Size: 278 Color: 17

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 6
Size: 235 Color: 13

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 12
Size: 436 Color: 13

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 7
Size: 277 Color: 14

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 7
Size: 370 Color: 13

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 17
Size: 459 Color: 10

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 5
Size: 334 Color: 19

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 17
Size: 209 Color: 4

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 17
Size: 388 Color: 10

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 8
Size: 426 Color: 17

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 11
Size: 401 Color: 18

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 19
Size: 356 Color: 12

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 4

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 283 Color: 13

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 18
Size: 389 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 415 Color: 8
Size: 127 Color: 3

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 9
Size: 467 Color: 1

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 9
Size: 233 Color: 12

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 16
Size: 248 Color: 10

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 18
Size: 232 Color: 5

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 4
Size: 325 Color: 0
Size: 184 Color: 11

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 258 Color: 18

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 14
Size: 341 Color: 15

Bin 121: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 19
Size: 471 Color: 2

Bin 122: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 7
Size: 271 Color: 4

Bin 123: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 467 Color: 4

Bin 124: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 6
Size: 397 Color: 8

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 5
Size: 448 Color: 9

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 16
Size: 488 Color: 2

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 4

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 16
Size: 428 Color: 7

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 14
Size: 175 Color: 12
Size: 171 Color: 15

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 323 Color: 15
Size: 193 Color: 19

Bin 131: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 19
Size: 247 Color: 17

Bin 132: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 12
Size: 216 Color: 10

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 16
Size: 457 Color: 11

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 5
Size: 412 Color: 14

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 18
Size: 361 Color: 11

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 16
Size: 243 Color: 8
Size: 208 Color: 1

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 18
Size: 447 Color: 8

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 12
Size: 200 Color: 2

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 2
Size: 427 Color: 19

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 15
Size: 366 Color: 9

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 8
Size: 390 Color: 13

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 15

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 2
Size: 186 Color: 14
Size: 129 Color: 14

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 18
Size: 198 Color: 19
Size: 198 Color: 13

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 11
Size: 345 Color: 4
Size: 280 Color: 4

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 6
Size: 349 Color: 12

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 8
Size: 428 Color: 18

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 14
Size: 245 Color: 4
Size: 198 Color: 18

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 8
Size: 495 Color: 14

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 18
Size: 364 Color: 1

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 19
Size: 462 Color: 16

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 8
Size: 417 Color: 0

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 1

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 13
Size: 251 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 19
Size: 126 Color: 19
Size: 115 Color: 4

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 6
Size: 207 Color: 4

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 14
Size: 216 Color: 17

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 8
Size: 487 Color: 2

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 19
Size: 238 Color: 8
Size: 157 Color: 19

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 8
Size: 360 Color: 4
Size: 281 Color: 11

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 18
Size: 374 Color: 14

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 11
Size: 403 Color: 18

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 12

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 2
Size: 234 Color: 7

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 6

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 11
Size: 288 Color: 14

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 11
Size: 173 Color: 5
Size: 146 Color: 5

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 10
Size: 398 Color: 14
Size: 107 Color: 0

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 19
Size: 364 Color: 15

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 7

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 13
Size: 322 Color: 10

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 4
Size: 273 Color: 18

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 19
Size: 266 Color: 2

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 7
Size: 276 Color: 15

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 5
Size: 212 Color: 2

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 1
Size: 298 Color: 7

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 6

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 231 Color: 2

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 16
Size: 362 Color: 8

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 7
Size: 331 Color: 16

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 1
Size: 136 Color: 7
Size: 132 Color: 6

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 6

Bin 183: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 4
Size: 441 Color: 6

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 16
Size: 111 Color: 10
Size: 103 Color: 2

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 17
Size: 350 Color: 2

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 11
Size: 300 Color: 12

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 4
Size: 492 Color: 11

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 3

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 12
Size: 318 Color: 16

Bin 190: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 2
Size: 359 Color: 18

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 403 Color: 9

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 9
Size: 210 Color: 13

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 9
Size: 401 Color: 4

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 9
Size: 481 Color: 12

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 18
Size: 220 Color: 19

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 15
Size: 400 Color: 16

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 5
Size: 274 Color: 16

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 2
Size: 307 Color: 15

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 11
Size: 366 Color: 19

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 11
Size: 336 Color: 14

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 12
Size: 446 Color: 9

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 10
Size: 349 Color: 11

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 15
Size: 226 Color: 7

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 14

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 15
Size: 455 Color: 13

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 9
Size: 350 Color: 5

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 5
Size: 333 Color: 17

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 14
Size: 460 Color: 4

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 6
Size: 345 Color: 2

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 5
Size: 360 Color: 15

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 19
Size: 157 Color: 14
Size: 122 Color: 5

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 15
Size: 244 Color: 2

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 17
Size: 252 Color: 9

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 14
Size: 212 Color: 6

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 18
Size: 301 Color: 12

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 11

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 5
Size: 272 Color: 9

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 5
Size: 110 Color: 14
Size: 104 Color: 2

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 8
Size: 416 Color: 18

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 12
Size: 323 Color: 18

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 11
Size: 204 Color: 16

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 2

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 8
Size: 327 Color: 5

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 16
Size: 427 Color: 6

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 1
Size: 286 Color: 15

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 10
Size: 172 Color: 13
Size: 153 Color: 12

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 7
Size: 340 Color: 19

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 4
Size: 170 Color: 11
Size: 161 Color: 1

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 18

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 3
Size: 467 Color: 12

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 10
Size: 405 Color: 6

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 2
Size: 328 Color: 17

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 17
Size: 440 Color: 5

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 17
Size: 401 Color: 15
Size: 138 Color: 8

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 337 Color: 17

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 6
Size: 385 Color: 16
Size: 138 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 5
Size: 271 Color: 9
Size: 178 Color: 9

Bin 238: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 5
Size: 265 Color: 17

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 16
Size: 355 Color: 0

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 12
Size: 466 Color: 11

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 19

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 12
Size: 404 Color: 14

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 14
Size: 223 Color: 18

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 6

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 9
Size: 335 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 11
Size: 357 Color: 4
Size: 175 Color: 8

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 6
Size: 298 Color: 8

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 4
Size: 417 Color: 11

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 12
Size: 182 Color: 11
Size: 181 Color: 7

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 8
Size: 262 Color: 6

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 17
Size: 226 Color: 15

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 15
Size: 288 Color: 7

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 8
Size: 334 Color: 4

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 4
Size: 207 Color: 13
Size: 163 Color: 8

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 11
Size: 365 Color: 1

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 12
Size: 325 Color: 14

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 18
Size: 333 Color: 0

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 8
Size: 235 Color: 12

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 18
Size: 225 Color: 9

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 13
Size: 188 Color: 14
Size: 164 Color: 7

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 14
Size: 141 Color: 10
Size: 122 Color: 7

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 440 Color: 3

Bin 263: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 3
Size: 294 Color: 1

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 13

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 5
Size: 415 Color: 6

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 15
Size: 256 Color: 5

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 17
Size: 121 Color: 17
Size: 114 Color: 13

Bin 268: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 15
Size: 306 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 10
Size: 311 Color: 16
Size: 177 Color: 13

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 11
Size: 256 Color: 10

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 5
Size: 159 Color: 5
Size: 157 Color: 15

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 303 Color: 5

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 18
Size: 416 Color: 8

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 6
Size: 384 Color: 2
Size: 158 Color: 9

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 14
Size: 393 Color: 7

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 418 Color: 9
Size: 130 Color: 3

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 2
Size: 250 Color: 12
Size: 147 Color: 7

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 9
Size: 247 Color: 7

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 15
Size: 357 Color: 6

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 3
Size: 195 Color: 13
Size: 184 Color: 10

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 5
Size: 234 Color: 18
Size: 150 Color: 15

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 4
Size: 150 Color: 13
Size: 116 Color: 1

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 7
Size: 215 Color: 6

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 12
Size: 442 Color: 13

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 19
Size: 249 Color: 0

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 17
Size: 290 Color: 0

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 10
Size: 403 Color: 18

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 206 Color: 10

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 7
Size: 171 Color: 14
Size: 171 Color: 11

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 4
Size: 487 Color: 12

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 7
Size: 413 Color: 15

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 10
Size: 440 Color: 3

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 7
Size: 253 Color: 3

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 16
Size: 403 Color: 5

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 10
Size: 498 Color: 5

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 4
Size: 139 Color: 7
Size: 121 Color: 14

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 10
Size: 327 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 4
Size: 343 Color: 13
Size: 183 Color: 10

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 14
Size: 401 Color: 1

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 14
Size: 413 Color: 9

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 7
Size: 351 Color: 3
Size: 155 Color: 2

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 4
Size: 302 Color: 18

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 18
Size: 405 Color: 17

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 0
Size: 262 Color: 9

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 9
Size: 437 Color: 5

Bin 306: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 11
Size: 442 Color: 5

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 7
Size: 393 Color: 3

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 6
Size: 274 Color: 7

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 8
Size: 251 Color: 2

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 5
Size: 486 Color: 7

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 10
Size: 319 Color: 9

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 3
Size: 491 Color: 11

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 19
Size: 281 Color: 6

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 12
Size: 318 Color: 5

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 3
Size: 299 Color: 12

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 17
Size: 207 Color: 15

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 17
Size: 376 Color: 13

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 18
Size: 468 Color: 4

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 19
Size: 230 Color: 7

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 10
Size: 487 Color: 3

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 17
Size: 400 Color: 14

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 16
Size: 235 Color: 7

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 10
Size: 308 Color: 17

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 17
Size: 279 Color: 3

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 13
Size: 376 Color: 3

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 3
Size: 285 Color: 5
Size: 194 Color: 12

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 8
Size: 474 Color: 16

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 17
Size: 171 Color: 13
Size: 149 Color: 1

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 8
Size: 341 Color: 4

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 9
Size: 236 Color: 3

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 2
Size: 344 Color: 19

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 3
Size: 280 Color: 14

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 3
Size: 357 Color: 5

Bin 334: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 8
Size: 497 Color: 7

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 16
Size: 236 Color: 3

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 0
Size: 196 Color: 8
Size: 121 Color: 19

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 12
Size: 232 Color: 11

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 19
Size: 171 Color: 17
Size: 165 Color: 15

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 4
Size: 446 Color: 17

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 13
Size: 425 Color: 3
Size: 122 Color: 17

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 9
Size: 201 Color: 4

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 11

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 9
Size: 359 Color: 17

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 10
Size: 227 Color: 17

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 15
Size: 464 Color: 8

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 14
Size: 182 Color: 9
Size: 134 Color: 18

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 15
Size: 300 Color: 16

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 11
Size: 368 Color: 17

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 14
Size: 145 Color: 5
Size: 143 Color: 2

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 12
Size: 163 Color: 0
Size: 154 Color: 17

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 15
Size: 400 Color: 1

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 15
Size: 270 Color: 2

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 3
Size: 185 Color: 13
Size: 145 Color: 16

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 14
Size: 294 Color: 3
Size: 145 Color: 5

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 7
Size: 351 Color: 8

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 11

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 15
Size: 220 Color: 9

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 17
Size: 235 Color: 13

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 5
Size: 390 Color: 18

Bin 360: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 14
Size: 235 Color: 5

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 10
Size: 341 Color: 1
Size: 160 Color: 3

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 12
Size: 202 Color: 2

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 12
Size: 371 Color: 6

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 13
Size: 398 Color: 16

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 8
Size: 343 Color: 14

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 5
Size: 248 Color: 1

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 11
Size: 368 Color: 7

Bin 368: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 16
Size: 431 Color: 13

Bin 369: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 16
Size: 393 Color: 3

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 355 Color: 0
Size: 193 Color: 9

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 0
Size: 154 Color: 1
Size: 110 Color: 2

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 17
Size: 202 Color: 6

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 9
Size: 147 Color: 8
Size: 106 Color: 2

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 3
Size: 220 Color: 8

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 13
Size: 240 Color: 0

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 14
Size: 409 Color: 7

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 14
Size: 365 Color: 7

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 16
Size: 340 Color: 6

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 3
Size: 241 Color: 2

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 14
Size: 277 Color: 0

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 2
Size: 364 Color: 15

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 3
Size: 448 Color: 2

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 13
Size: 106 Color: 7
Size: 105 Color: 0

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 18
Size: 208 Color: 8

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 4
Size: 431 Color: 7

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 2
Size: 389 Color: 5

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 8
Size: 291 Color: 17

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 11

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 18
Size: 280 Color: 14

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 10
Size: 247 Color: 8

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 12
Size: 256 Color: 1

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 19
Size: 469 Color: 14

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 14
Size: 111 Color: 3
Size: 104 Color: 8

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 15
Size: 480 Color: 1

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 0
Size: 309 Color: 14

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 14
Size: 255 Color: 10

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 283 Color: 13

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 19
Size: 259 Color: 10

Bin 399: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 14
Size: 490 Color: 9

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 8
Size: 292 Color: 11

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 7
Size: 362 Color: 4

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 2
Size: 454 Color: 8

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 14
Size: 409 Color: 11

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 13
Size: 457 Color: 2

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 2
Size: 215 Color: 5

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 14
Size: 224 Color: 16
Size: 189 Color: 0

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 17
Size: 465 Color: 13

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 12
Size: 473 Color: 0

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 16
Size: 442 Color: 10

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 18
Size: 481 Color: 1

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 7
Size: 453 Color: 6

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 15

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 18
Size: 477 Color: 1

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 16
Size: 432 Color: 18

Bin 415: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 208 Color: 16

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 11
Size: 455 Color: 18

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 18
Size: 276 Color: 1
Size: 121 Color: 19

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 19
Size: 164 Color: 16
Size: 114 Color: 0

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 2
Size: 272 Color: 6

Bin 420: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 16
Size: 278 Color: 7

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 17
Size: 465 Color: 13

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 17
Size: 345 Color: 8

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 8
Size: 251 Color: 16

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 9
Size: 433 Color: 0

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 18
Size: 340 Color: 8

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 14
Size: 384 Color: 15

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 16

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 8
Size: 417 Color: 17

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 11
Size: 247 Color: 19
Size: 142 Color: 7

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 14
Size: 415 Color: 8

Bin 431: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 0
Size: 207 Color: 4

Bin 432: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 14
Size: 308 Color: 9

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 1
Size: 262 Color: 10

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 4
Size: 152 Color: 13
Size: 152 Color: 4

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 6
Size: 434 Color: 12

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 11
Size: 402 Color: 12

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 1
Size: 218 Color: 5

Bin 438: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 15
Size: 285 Color: 18

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 4
Size: 303 Color: 0

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 2
Size: 264 Color: 6

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 10
Size: 359 Color: 14
Size: 151 Color: 6

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 12
Size: 321 Color: 6
Size: 189 Color: 5

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 7
Size: 411 Color: 11

Bin 444: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 17
Size: 386 Color: 1

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 333 Color: 12

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 9
Size: 419 Color: 14
Size: 162 Color: 8

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 17
Size: 105 Color: 5
Size: 100 Color: 1

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 418 Color: 8

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 17
Size: 209 Color: 2

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 9
Size: 224 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 2
Size: 147 Color: 6
Size: 143 Color: 18

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 442 Color: 18

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 19

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 13
Size: 260 Color: 17
Size: 183 Color: 1

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 13

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 6
Size: 274 Color: 18

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 9
Size: 225 Color: 10

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 14
Size: 389 Color: 11

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 19
Size: 440 Color: 8

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 10
Size: 155 Color: 2
Size: 123 Color: 14

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 9
Size: 308 Color: 8

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 7
Size: 278 Color: 10
Size: 174 Color: 10

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 14
Size: 310 Color: 5

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 7
Size: 273 Color: 11

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 12
Size: 204 Color: 14

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 9
Size: 396 Color: 12

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 19
Size: 417 Color: 16

Bin 468: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 5
Size: 278 Color: 4

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 13
Size: 429 Color: 18

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 6
Size: 173 Color: 6
Size: 119 Color: 8

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 19
Size: 464 Color: 14

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 13
Size: 433 Color: 16

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 11
Size: 317 Color: 6

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 12
Size: 289 Color: 19

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 14
Size: 427 Color: 12

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 16
Size: 443 Color: 5

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 15
Size: 352 Color: 1

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 14
Size: 449 Color: 13

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 7
Size: 232 Color: 6

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 10
Size: 329 Color: 5

Bin 481: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 10
Size: 490 Color: 15

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 7
Size: 411 Color: 19

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 15
Size: 284 Color: 11

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 14
Size: 286 Color: 16

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 15
Size: 370 Color: 13

Bin 486: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 8
Size: 230 Color: 12

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 5

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 4
Size: 312 Color: 1

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 18
Size: 492 Color: 19

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 16
Size: 349 Color: 18

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 9
Size: 178 Color: 15
Size: 178 Color: 2

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 11
Size: 285 Color: 15

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 3
Size: 199 Color: 9
Size: 113 Color: 10

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 17

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 3
Size: 285 Color: 10

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 14
Size: 318 Color: 13

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 6
Size: 471 Color: 4

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 4

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 16
Size: 446 Color: 12

Bin 500: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 10
Size: 342 Color: 15

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 226 Color: 2

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 19
Size: 304 Color: 2

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 12
Size: 360 Color: 8

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 7

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 11
Size: 364 Color: 16

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 8
Size: 432 Color: 18

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 15
Size: 389 Color: 5

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 7
Size: 221 Color: 16

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 9
Size: 418 Color: 7

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 10
Size: 374 Color: 6

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 12
Size: 484 Color: 9

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 0
Size: 157 Color: 7
Size: 155 Color: 9

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 16
Size: 447 Color: 13

Bin 514: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 9
Size: 222 Color: 4

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 6
Size: 372 Color: 0

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 4
Size: 208 Color: 12

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 6
Size: 334 Color: 1
Size: 332 Color: 13

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 15
Size: 454 Color: 8

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 5
Size: 283 Color: 8

Bin 520: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 437 Color: 19

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 16
Size: 251 Color: 14

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 0
Size: 189 Color: 14
Size: 161 Color: 10

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 438 Color: 9
Size: 110 Color: 12

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 12
Size: 211 Color: 15

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 19
Size: 470 Color: 1

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 8
Size: 407 Color: 1

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 8
Size: 376 Color: 0
Size: 112 Color: 3

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 19
Size: 398 Color: 2
Size: 125 Color: 4

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 10
Size: 452 Color: 14

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 18
Size: 312 Color: 2

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 11
Size: 337 Color: 12

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 8
Size: 458 Color: 11

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 3
Size: 349 Color: 13

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 336 Color: 9

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 16
Size: 245 Color: 1
Size: 152 Color: 16

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 10
Size: 351 Color: 14

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 3
Size: 340 Color: 19

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 9
Size: 447 Color: 14

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 5
Size: 289 Color: 12

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 6
Size: 409 Color: 0

Bin 541: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 13

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 5
Size: 385 Color: 16
Size: 122 Color: 3

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 10
Size: 414 Color: 11

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 11
Size: 452 Color: 13

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 13
Size: 492 Color: 1

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 17
Size: 255 Color: 19

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 19
Size: 233 Color: 6

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 17
Size: 415 Color: 14
Size: 166 Color: 3

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 11
Size: 171 Color: 12
Size: 160 Color: 14

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 9
Size: 221 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 13
Size: 229 Color: 18
Size: 152 Color: 1

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 1
Size: 332 Color: 11
Size: 118 Color: 16

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 12
Size: 208 Color: 15

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 10
Size: 231 Color: 15

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 441 Color: 18

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 15

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 2
Size: 329 Color: 8

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 9

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 14
Size: 404 Color: 0

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 12
Size: 440 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 6
Size: 260 Color: 6
Size: 198 Color: 19

Bin 562: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 264 Color: 14

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 6
Size: 494 Color: 19

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 8
Size: 400 Color: 18

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 11
Size: 466 Color: 7

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 11
Size: 325 Color: 3

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 5
Size: 307 Color: 7

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 10
Size: 477 Color: 7

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 5
Size: 279 Color: 3

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 19

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 9
Size: 415 Color: 11

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 8
Size: 367 Color: 5

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 2
Size: 442 Color: 11

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 15
Size: 469 Color: 2

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 11
Size: 233 Color: 8

Bin 576: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 11
Size: 276 Color: 8

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 6
Size: 257 Color: 0

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 15
Size: 404 Color: 5

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 17
Size: 376 Color: 16

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 2
Size: 398 Color: 7

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 10
Size: 475 Color: 1

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 2
Size: 235 Color: 10

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 11
Size: 460 Color: 16

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 11
Size: 427 Color: 12

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 10
Size: 334 Color: 19

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 12
Size: 394 Color: 10
Size: 129 Color: 0

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 5
Size: 404 Color: 17

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 18
Size: 226 Color: 4

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 305 Color: 4

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 9
Size: 369 Color: 15

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 11
Size: 147 Color: 2
Size: 108 Color: 1

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 15

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 17
Size: 496 Color: 10

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 11
Size: 394 Color: 3
Size: 111 Color: 14

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 608 Color: 2
Size: 197 Color: 2
Size: 196 Color: 17

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 1
Size: 322 Color: 6
Size: 174 Color: 8

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 19
Size: 279 Color: 5

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 16

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 2
Size: 227 Color: 1

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 8
Size: 332 Color: 19
Size: 177 Color: 9

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 363 Color: 15

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 9
Size: 278 Color: 4
Size: 196 Color: 4

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 19
Size: 346 Color: 10

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 14
Size: 147 Color: 15
Size: 147 Color: 8

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 8
Size: 396 Color: 10

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 16
Size: 318 Color: 15

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 493 Color: 0

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 6
Size: 202 Color: 10

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 5
Size: 337 Color: 8

Bin 610: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 8
Size: 243 Color: 14

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 577 Color: 7
Size: 240 Color: 18
Size: 184 Color: 13

Bin 612: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 6
Size: 321 Color: 9

Bin 613: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 6
Size: 390 Color: 14

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 10
Size: 247 Color: 19

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 16
Size: 360 Color: 0

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 6
Size: 253 Color: 10

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 11
Size: 431 Color: 16

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 6
Size: 419 Color: 2

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 9
Size: 420 Color: 8

Bin 620: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 5
Size: 445 Color: 16

Bin 621: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 3
Size: 420 Color: 9

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 13
Size: 415 Color: 12

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 4
Size: 248 Color: 1

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 18
Size: 326 Color: 13

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 8

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 16
Size: 164 Color: 13
Size: 104 Color: 18

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 6
Size: 329 Color: 0

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 16
Size: 224 Color: 12

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 7
Size: 276 Color: 6

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 14
Size: 243 Color: 13
Size: 198 Color: 15

Bin 631: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 14
Size: 282 Color: 16

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 2
Size: 494 Color: 13

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 13
Size: 451 Color: 7

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 322 Color: 17

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 541 Color: 0
Size: 278 Color: 18
Size: 182 Color: 16

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 17
Size: 277 Color: 11

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 15
Size: 304 Color: 3
Size: 299 Color: 2

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 12
Size: 245 Color: 9
Size: 234 Color: 10

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 3
Size: 218 Color: 11

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 732 Color: 9
Size: 148 Color: 16
Size: 121 Color: 3

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 16
Size: 357 Color: 6

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 9
Size: 269 Color: 15

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 14
Size: 318 Color: 4

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 12
Size: 492 Color: 5

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 2
Size: 213 Color: 3

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 17
Size: 193 Color: 5
Size: 113 Color: 6

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 5
Size: 234 Color: 6
Size: 161 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 14
Size: 276 Color: 10
Size: 182 Color: 13

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 11
Size: 433 Color: 8

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 17
Size: 234 Color: 18

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 4

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 15
Size: 327 Color: 18

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 11
Size: 483 Color: 8

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 1
Size: 214 Color: 6

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 4
Size: 296 Color: 12

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 6
Size: 287 Color: 17

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 4
Size: 412 Color: 2

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 12
Size: 480 Color: 15

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 11
Size: 432 Color: 4

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 19
Size: 372 Color: 3

Bin 661: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 2
Size: 338 Color: 6

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 19
Size: 344 Color: 13

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 7
Size: 347 Color: 4
Size: 197 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 13
Size: 203 Color: 10
Size: 194 Color: 11

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 17
Size: 307 Color: 1

Bin 666: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 17
Size: 452 Color: 11

Bin 667: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 15

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 2
Size: 380 Color: 3

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 6
Size: 407 Color: 7

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 17
Size: 458 Color: 1

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 19

Bin 672: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 17
Size: 485 Color: 19

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 19
Size: 322 Color: 8

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 8
Size: 451 Color: 7

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 15
Size: 369 Color: 4

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 9
Size: 280 Color: 19
Size: 133 Color: 9

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 8

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 8
Size: 326 Color: 13

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 6
Size: 382 Color: 9

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 15
Size: 316 Color: 2

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 19
Size: 475 Color: 17

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 243 Color: 14

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 7

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 18
Size: 408 Color: 1

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 8
Size: 330 Color: 6

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 18
Size: 310 Color: 1

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 3
Size: 362 Color: 8

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 1

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 493 Color: 8

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 2
Size: 234 Color: 5

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 15
Size: 414 Color: 7

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 13
Size: 333 Color: 7

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 14
Size: 357 Color: 17

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 17
Size: 388 Color: 2

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 7
Size: 425 Color: 16

Bin 696: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 9
Size: 229 Color: 13

Bin 697: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 5
Size: 253 Color: 16

Bin 698: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 12
Size: 348 Color: 13

Bin 699: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 14
Size: 309 Color: 15

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 1
Size: 245 Color: 6
Size: 167 Color: 15

Bin 701: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 8
Size: 487 Color: 11

Bin 702: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 10
Size: 237 Color: 5

Bin 703: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 15
Size: 423 Color: 5

Bin 704: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 9
Size: 277 Color: 10

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 14
Size: 126 Color: 13
Size: 123 Color: 12

Bin 706: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 8
Size: 447 Color: 19

Bin 707: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 14
Size: 473 Color: 17

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 3
Size: 335 Color: 14
Size: 172 Color: 4

Bin 709: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 10
Size: 242 Color: 16

Bin 710: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 15
Size: 432 Color: 3

Bin 711: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 19
Size: 376 Color: 3

Bin 712: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 17
Size: 453 Color: 5

Bin 713: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 13
Size: 347 Color: 10

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 2
Size: 229 Color: 2
Size: 149 Color: 1

Bin 715: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 14

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 0
Size: 245 Color: 15
Size: 194 Color: 7

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 7
Size: 185 Color: 17
Size: 146 Color: 3

Bin 718: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 12
Size: 415 Color: 15

Bin 719: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 13
Size: 349 Color: 15

Bin 720: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 10
Size: 304 Color: 1

Bin 721: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 9
Size: 342 Color: 19

Bin 722: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 387 Color: 16

Bin 723: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 9
Size: 312 Color: 5

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 5
Size: 294 Color: 3
Size: 207 Color: 5

Bin 725: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 17
Size: 391 Color: 11

Bin 726: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 13
Size: 327 Color: 2

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 13
Size: 148 Color: 0
Size: 100 Color: 12

Bin 728: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 10
Size: 290 Color: 6

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 5
Size: 114 Color: 6
Size: 100 Color: 19

Bin 730: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 16
Size: 468 Color: 9

Bin 731: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 1

Bin 732: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 0
Size: 275 Color: 15

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 15
Size: 216 Color: 1
Size: 120 Color: 7

Bin 734: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 16
Size: 225 Color: 9

Bin 735: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 387 Color: 5

Bin 736: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 8
Size: 203 Color: 13

Bin 737: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 4
Size: 288 Color: 5

Bin 738: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 16
Size: 497 Color: 12

Bin 739: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 18
Size: 241 Color: 5

Bin 740: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 13
Size: 388 Color: 14

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 12
Size: 418 Color: 11
Size: 136 Color: 11

Bin 742: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 9

Bin 743: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 18
Size: 298 Color: 5

Bin 744: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 18

Bin 745: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 19
Size: 439 Color: 18

Bin 746: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 371 Color: 2

Bin 747: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 15
Size: 338 Color: 7

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 502 Color: 17
Size: 385 Color: 15
Size: 114 Color: 4

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 384 Color: 5
Size: 179 Color: 8

Bin 750: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 9
Size: 237 Color: 3

Bin 751: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 13
Size: 385 Color: 14

Bin 752: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 10
Size: 369 Color: 14

Bin 753: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 6
Size: 471 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 680 Color: 15
Size: 161 Color: 9
Size: 160 Color: 9

Bin 755: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 5
Size: 212 Color: 19

Bin 756: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 8
Size: 430 Color: 4

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 376 Color: 7
Size: 172 Color: 11

Bin 758: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 14
Size: 314 Color: 8

Bin 759: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 17
Size: 410 Color: 9

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 3
Size: 199 Color: 4
Size: 181 Color: 12

Bin 761: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 17
Size: 244 Color: 0

Bin 762: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 3
Size: 464 Color: 15

Bin 763: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 1
Size: 477 Color: 5

Bin 764: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 16
Size: 285 Color: 9

Bin 765: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 474 Color: 17

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 18
Size: 135 Color: 17
Size: 108 Color: 11

Bin 767: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 16
Size: 234 Color: 10

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 7
Size: 194 Color: 12
Size: 190 Color: 10

Bin 769: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 7
Size: 214 Color: 4

Bin 770: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 394 Color: 11

Bin 771: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 9
Size: 219 Color: 18

Bin 772: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 3
Size: 300 Color: 2

Bin 773: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 8
Size: 439 Color: 11

Bin 774: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 19
Size: 481 Color: 6

Bin 775: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 14
Size: 228 Color: 8

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 15
Size: 193 Color: 10
Size: 160 Color: 7

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 5
Size: 386 Color: 0
Size: 179 Color: 11

Bin 778: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 14
Size: 452 Color: 8

Bin 779: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 6
Size: 225 Color: 5

Bin 780: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 12
Size: 232 Color: 7

Bin 781: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 16
Size: 306 Color: 3

Bin 782: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 8
Size: 336 Color: 10

Bin 783: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 17

Bin 784: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 5
Size: 352 Color: 2

Bin 785: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 11
Size: 351 Color: 1

Bin 786: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 5

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 18
Size: 177 Color: 12
Size: 176 Color: 1

Bin 788: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 12
Size: 416 Color: 18

Bin 789: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 5
Size: 446 Color: 6

Bin 790: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 14
Size: 477 Color: 4

Bin 791: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 16
Size: 469 Color: 6

Bin 792: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 6
Size: 386 Color: 16

Bin 793: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 6
Size: 481 Color: 2

Bin 794: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 17
Size: 405 Color: 2

Bin 795: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 12
Size: 258 Color: 16

Bin 796: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 0
Size: 290 Color: 14

Bin 797: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 19
Size: 242 Color: 6

Bin 798: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 5
Size: 496 Color: 13

Bin 799: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 9
Size: 295 Color: 7

Bin 800: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 8
Size: 211 Color: 6

Bin 801: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 19
Size: 376 Color: 18

Bin 802: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 7
Size: 491 Color: 14

Bin 803: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 5
Size: 211 Color: 18

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 16
Size: 131 Color: 12
Size: 102 Color: 7

Bin 805: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 3
Size: 257 Color: 13

Bin 806: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 8
Size: 417 Color: 18

Bin 807: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 3
Size: 317 Color: 9

Bin 808: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 2
Size: 232 Color: 7

Bin 809: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 13
Size: 299 Color: 17

Bin 810: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 11
Size: 466 Color: 1

Bin 811: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 0
Size: 269 Color: 10

Bin 812: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 0
Size: 449 Color: 3

Bin 813: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 449 Color: 2

Bin 814: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 10
Size: 211 Color: 2

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 537 Color: 2
Size: 272 Color: 10
Size: 192 Color: 7

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 13
Size: 223 Color: 11
Size: 174 Color: 16

Bin 817: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 12
Size: 357 Color: 4

Bin 818: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 19
Size: 390 Color: 14

Bin 819: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 2
Size: 314 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 15
Size: 398 Color: 12
Size: 109 Color: 10

Bin 821: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 5
Size: 317 Color: 2

Bin 822: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 2
Size: 337 Color: 12

Bin 823: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 11
Size: 425 Color: 2

Bin 824: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 13
Size: 315 Color: 3

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 4
Size: 180 Color: 8
Size: 151 Color: 14

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 0
Size: 201 Color: 14
Size: 130 Color: 1

Bin 827: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 5
Size: 394 Color: 18

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 13
Size: 272 Color: 5
Size: 195 Color: 13

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 4
Size: 103 Color: 7
Size: 102 Color: 1

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 14
Size: 281 Color: 4
Size: 125 Color: 12

Bin 831: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 208 Color: 2

Bin 832: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 6
Size: 339 Color: 17

Bin 833: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 19
Size: 224 Color: 5

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 3
Size: 197 Color: 17
Size: 138 Color: 11

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 9
Size: 357 Color: 4
Size: 120 Color: 13

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 11
Size: 369 Color: 14
Size: 137 Color: 13

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 8
Size: 135 Color: 18
Size: 135 Color: 9

Bin 838: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 2

Bin 839: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 3
Size: 408 Color: 13

Bin 840: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 19
Size: 378 Color: 7

Bin 841: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 13
Size: 227 Color: 12

Bin 842: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 2
Size: 500 Color: 11

Bin 843: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 8
Size: 339 Color: 4

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 1
Size: 192 Color: 18
Size: 105 Color: 12

Bin 845: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 10
Size: 228 Color: 16

Bin 846: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 10
Size: 447 Color: 13

Bin 847: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 12
Size: 452 Color: 17

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 405 Color: 14
Size: 174 Color: 4

Bin 849: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 12
Size: 296 Color: 17

Bin 850: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 5
Size: 241 Color: 0

Bin 851: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 5
Size: 323 Color: 12

Bin 852: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 498 Color: 18

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 18
Size: 145 Color: 5
Size: 142 Color: 7

Bin 854: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 18
Size: 410 Color: 9

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 16
Size: 191 Color: 6
Size: 182 Color: 16

Bin 856: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 10
Size: 374 Color: 1

Bin 857: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 16
Size: 213 Color: 3

Bin 858: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 11
Size: 217 Color: 18

Bin 859: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 8
Size: 498 Color: 17

Bin 860: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 18
Size: 359 Color: 6

Bin 861: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 15
Size: 246 Color: 17

Bin 862: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 16
Size: 287 Color: 3

Bin 863: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 237 Color: 14

Bin 864: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 7
Size: 476 Color: 15

Bin 865: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 8
Size: 492 Color: 19

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 2
Size: 250 Color: 6
Size: 178 Color: 1

Bin 867: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 15
Size: 420 Color: 14

Bin 868: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 240 Color: 7

Bin 869: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 5
Size: 491 Color: 0

Bin 870: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 15
Size: 391 Color: 18

Bin 871: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 13
Size: 459 Color: 12

Bin 872: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 13
Size: 253 Color: 5

Bin 873: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 13
Size: 358 Color: 5

Bin 874: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 12
Size: 443 Color: 9

Bin 875: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 14
Size: 212 Color: 10

Bin 876: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 0
Size: 437 Color: 12

Bin 877: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 17
Size: 200 Color: 15

Bin 878: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 5
Size: 306 Color: 11

Bin 879: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 14
Size: 481 Color: 3

Bin 880: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 8
Size: 335 Color: 14

Bin 881: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 5
Size: 211 Color: 9

Bin 882: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 13
Size: 387 Color: 9

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 11
Size: 171 Color: 13
Size: 171 Color: 11

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 10
Size: 261 Color: 6
Size: 188 Color: 5

Bin 885: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 13
Size: 212 Color: 12

Bin 886: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 13
Size: 271 Color: 4

Bin 887: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 9

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 7
Size: 386 Color: 12
Size: 177 Color: 17

Bin 889: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 8
Size: 463 Color: 17

Bin 890: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 17
Size: 281 Color: 7

Bin 891: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 18

Bin 892: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 11
Size: 353 Color: 1

Bin 893: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 9
Size: 383 Color: 18

Bin 894: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 16
Size: 264 Color: 12

Bin 895: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 3
Size: 256 Color: 7

Bin 896: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 7
Size: 457 Color: 6

Bin 897: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 17
Size: 419 Color: 11

Bin 898: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 2
Size: 481 Color: 14

Bin 899: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 19
Size: 493 Color: 5

Bin 900: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 7
Size: 211 Color: 12

Bin 901: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 6
Size: 343 Color: 1

Bin 902: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 10
Size: 258 Color: 6

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 502 Color: 4
Size: 394 Color: 1
Size: 105 Color: 6

Bin 904: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 12
Size: 327 Color: 11

Bin 905: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 13
Size: 255 Color: 16

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 417 Color: 11
Size: 136 Color: 10

Bin 907: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 10
Size: 312 Color: 15

Bin 908: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 5
Size: 219 Color: 4

Bin 909: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 18
Size: 316 Color: 8

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 17
Size: 104 Color: 4
Size: 102 Color: 6

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 7
Size: 198 Color: 10
Size: 198 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 15
Size: 199 Color: 6
Size: 160 Color: 16

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 16
Size: 398 Color: 7
Size: 183 Color: 12

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 6
Size: 157 Color: 19
Size: 157 Color: 2

Bin 915: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 9

Bin 916: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 17
Size: 416 Color: 0

Bin 917: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 10
Size: 371 Color: 13

Bin 918: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 18
Size: 377 Color: 17

Bin 919: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 11
Size: 288 Color: 1

Bin 920: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 0
Size: 430 Color: 19

Bin 921: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 9
Size: 422 Color: 10

Bin 922: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 14
Size: 490 Color: 1

Bin 923: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 3
Size: 295 Color: 7

Bin 924: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 4
Size: 375 Color: 13

Bin 925: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 19
Size: 321 Color: 7

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 2
Size: 195 Color: 0
Size: 184 Color: 14

Bin 927: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 11
Size: 370 Color: 19

Bin 928: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 16
Size: 415 Color: 1

Bin 929: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 12

Bin 930: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 5
Size: 462 Color: 13

Bin 931: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 16

Bin 932: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 12
Size: 452 Color: 8

Bin 933: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 8
Size: 381 Color: 12

Bin 934: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 12
Size: 453 Color: 3

Bin 935: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 5
Size: 211 Color: 11

Bin 936: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 1
Size: 360 Color: 19

Bin 937: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 13
Size: 292 Color: 0

Bin 938: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 14
Size: 378 Color: 5

Bin 939: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 8

Bin 940: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 5
Size: 391 Color: 6

Bin 941: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 6
Size: 250 Color: 8

Bin 942: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 7
Size: 394 Color: 16

Bin 943: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 9
Size: 499 Color: 13

Bin 944: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 18
Size: 274 Color: 14

Bin 945: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 14
Size: 472 Color: 5

Bin 946: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 5
Size: 395 Color: 11

Bin 947: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 19
Size: 457 Color: 8

Bin 948: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 14
Size: 297 Color: 12

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 9
Size: 145 Color: 13
Size: 122 Color: 13

Bin 950: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 3
Size: 401 Color: 13

Bin 951: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 12
Size: 476 Color: 5

Bin 952: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 8
Size: 435 Color: 14

Bin 953: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 19
Size: 365 Color: 0

Bin 954: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 8
Size: 420 Color: 9

Bin 955: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 5
Size: 397 Color: 16

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 1
Size: 172 Color: 18
Size: 130 Color: 5

Bin 957: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 7
Size: 359 Color: 12

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 5
Size: 143 Color: 8
Size: 106 Color: 5

Bin 959: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 456 Color: 14

Bin 960: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 14
Size: 256 Color: 12

Bin 961: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 17
Size: 258 Color: 19

Bin 962: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 3
Size: 373 Color: 11

Bin 963: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 2
Size: 442 Color: 3

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 13
Size: 180 Color: 12
Size: 163 Color: 12

Bin 965: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 5
Size: 488 Color: 12

Bin 966: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 6

Bin 967: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 9
Size: 372 Color: 5

Bin 968: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 328 Color: 16

Bin 969: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 5
Size: 303 Color: 18

Bin 970: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 17
Size: 334 Color: 4

Bin 971: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 9
Size: 378 Color: 3

Bin 972: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 10
Size: 378 Color: 5

Bin 973: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 10
Size: 355 Color: 13

Bin 974: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 250 Color: 6

Bin 975: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 2
Size: 401 Color: 6

Bin 976: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 14
Size: 499 Color: 9

Bin 977: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 12
Size: 349 Color: 2

Bin 978: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 19
Size: 332 Color: 7

Bin 979: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 3
Size: 421 Color: 18

Bin 980: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 5
Size: 215 Color: 2

Bin 981: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 2
Size: 248 Color: 4

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 18
Size: 186 Color: 16
Size: 130 Color: 6

Bin 983: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 8
Size: 384 Color: 0

Bin 984: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 13
Size: 276 Color: 19

Bin 985: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 10
Size: 343 Color: 16

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 15
Size: 185 Color: 1
Size: 130 Color: 2

Bin 987: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 11
Size: 256 Color: 6

Bin 988: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 15
Size: 391 Color: 6

Bin 989: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 10
Size: 368 Color: 19

Bin 990: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 16

Bin 991: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 9
Size: 206 Color: 19

Bin 992: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 7
Size: 206 Color: 12

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 537 Color: 11
Size: 286 Color: 10
Size: 178 Color: 9

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 5
Size: 289 Color: 5
Size: 169 Color: 15

Bin 995: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 18
Size: 354 Color: 3

Bin 996: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 2
Size: 441 Color: 18

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 19
Size: 134 Color: 17
Size: 109 Color: 15

Bin 998: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 6
Size: 454 Color: 4

Bin 999: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 17
Size: 449 Color: 6

Bin 1000: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 13
Size: 436 Color: 16

Bin 1001: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 4
Size: 448 Color: 5

Bin 1002: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 7
Size: 309 Color: 14

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 19
Size: 175 Color: 3
Size: 154 Color: 18

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 2
Size: 141 Color: 9
Size: 115 Color: 15

Bin 1005: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 9
Size: 317 Color: 11

Bin 1006: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 239 Color: 4

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 19
Size: 172 Color: 7
Size: 147 Color: 9

Bin 1008: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 12
Size: 468 Color: 1

Bin 1009: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 6
Size: 206 Color: 16

Bin 1010: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 10

Bin 1011: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 11
Size: 354 Color: 9

Bin 1012: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 9
Size: 403 Color: 3

Bin 1013: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 17
Size: 212 Color: 3

Bin 1014: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 13
Size: 343 Color: 3

Bin 1015: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 5
Size: 477 Color: 19

Bin 1016: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 8
Size: 462 Color: 14

Bin 1017: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 8

Bin 1018: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 208 Color: 7

Bin 1019: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 16
Size: 389 Color: 7

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 18
Size: 174 Color: 13
Size: 128 Color: 17

Bin 1021: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 12
Size: 458 Color: 2

Bin 1022: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 2
Size: 365 Color: 1

Bin 1023: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 12
Size: 363 Color: 19

Bin 1024: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 8
Size: 270 Color: 10

Bin 1025: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 12
Size: 316 Color: 10

Bin 1026: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 16
Size: 493 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 418 Color: 14
Size: 112 Color: 0

Bin 1028: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 6
Size: 226 Color: 14

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 644 Color: 2
Size: 180 Color: 16
Size: 177 Color: 4

Bin 1030: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 8

Bin 1031: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 17
Size: 340 Color: 5

Bin 1032: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 1

Bin 1033: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 9
Size: 410 Color: 6

Bin 1034: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 10
Size: 474 Color: 5

Bin 1035: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 5
Size: 376 Color: 3

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 3
Size: 173 Color: 1
Size: 144 Color: 15

Bin 1037: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 487 Color: 8

Bin 1038: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 466 Color: 11

Bin 1039: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 19
Size: 487 Color: 11

Bin 1040: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 14
Size: 361 Color: 2

Bin 1041: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 13
Size: 388 Color: 12

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 0
Size: 282 Color: 11
Size: 184 Color: 11

Bin 1043: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 13
Size: 278 Color: 9

Bin 1044: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 5

Bin 1045: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 9
Size: 482 Color: 6

Bin 1046: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 215 Color: 9

Bin 1047: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 2
Size: 383 Color: 15

Bin 1048: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 475 Color: 9

Bin 1049: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 7
Size: 211 Color: 5

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 382 Color: 9
Size: 166 Color: 2

Bin 1051: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 6
Size: 218 Color: 3

Bin 1052: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 13
Size: 395 Color: 9

Bin 1053: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 4
Size: 222 Color: 1

Bin 1054: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 2
Size: 446 Color: 8

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 7
Size: 159 Color: 1
Size: 158 Color: 13

Bin 1056: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 3
Size: 306 Color: 17

Bin 1057: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 17

Bin 1058: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 17

Bin 1059: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 6
Size: 273 Color: 17

Bin 1060: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 13

Bin 1061: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 3
Size: 461 Color: 7

Bin 1062: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 14
Size: 442 Color: 16

Bin 1063: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 1
Size: 401 Color: 2

Bin 1064: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 10
Size: 422 Color: 15

Bin 1065: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 10
Size: 274 Color: 17

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 13
Size: 128 Color: 6
Size: 105 Color: 9

Bin 1067: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 8
Size: 237 Color: 4

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 8
Size: 389 Color: 9
Size: 197 Color: 4

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 17
Size: 177 Color: 1
Size: 156 Color: 19

Bin 1070: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 268 Color: 8

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 19
Size: 189 Color: 0
Size: 141 Color: 18

Bin 1072: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 15
Size: 388 Color: 4

Bin 1073: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 16
Size: 426 Color: 8

Bin 1074: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 10
Size: 316 Color: 6

Bin 1075: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 14
Size: 483 Color: 4

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 16
Size: 199 Color: 19
Size: 143 Color: 1

Bin 1077: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 19
Size: 456 Color: 15

Bin 1078: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 4
Size: 337 Color: 3

Bin 1079: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 11
Size: 213 Color: 12

Bin 1080: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 14
Size: 228 Color: 10

Bin 1081: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 308 Color: 4

Bin 1082: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 13
Size: 343 Color: 19

Bin 1083: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 9
Size: 399 Color: 16

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 6
Size: 357 Color: 2
Size: 286 Color: 8

Bin 1085: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 1
Size: 436 Color: 3

Bin 1086: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 16
Size: 398 Color: 12

Bin 1087: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 19
Size: 204 Color: 2

Bin 1088: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 389 Color: 13

Bin 1089: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 9
Size: 361 Color: 7

Bin 1090: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 15
Size: 236 Color: 12

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 12
Size: 167 Color: 11
Size: 115 Color: 3

Bin 1092: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 0
Size: 354 Color: 12

Bin 1093: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 15
Size: 456 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 6
Size: 300 Color: 15
Size: 128 Color: 15

Bin 1095: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 13
Size: 417 Color: 7

Bin 1096: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 4
Size: 426 Color: 0

Bin 1097: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 18
Size: 286 Color: 9

Bin 1098: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 17
Size: 297 Color: 6

Bin 1099: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 12
Size: 337 Color: 18

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 9
Size: 199 Color: 16
Size: 181 Color: 19

Bin 1101: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 18
Size: 492 Color: 5

Bin 1102: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 18
Size: 210 Color: 1

Bin 1103: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 18
Size: 488 Color: 8

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 14
Size: 166 Color: 19
Size: 147 Color: 16

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 12
Size: 407 Color: 6
Size: 122 Color: 18

Bin 1106: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 6
Size: 440 Color: 2

Bin 1107: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 11
Size: 419 Color: 17

Bin 1108: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 6
Size: 238 Color: 0

Bin 1109: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 4
Size: 220 Color: 17

Bin 1110: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 6
Size: 357 Color: 12

Bin 1111: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 13
Size: 275 Color: 6

Bin 1112: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 16
Size: 400 Color: 6

Bin 1113: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 16
Size: 409 Color: 11

Bin 1114: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 344 Color: 8

Bin 1115: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 0
Size: 472 Color: 17

Bin 1116: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 7
Size: 395 Color: 19

Bin 1117: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 16
Size: 220 Color: 18

Bin 1118: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 6
Size: 368 Color: 5

Bin 1119: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 5
Size: 430 Color: 18

Bin 1120: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 19
Size: 496 Color: 3

Bin 1121: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 12
Size: 395 Color: 17

Bin 1122: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 14
Size: 258 Color: 2

Bin 1123: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 12
Size: 221 Color: 13

Bin 1124: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 6
Size: 342 Color: 17

Bin 1125: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 15
Size: 432 Color: 11

Bin 1126: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 7
Size: 430 Color: 4

Bin 1127: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 7
Size: 430 Color: 9

Bin 1128: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 11

Bin 1129: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 9
Size: 395 Color: 3

Bin 1130: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 16
Size: 456 Color: 9

Bin 1131: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 12
Size: 331 Color: 11

Bin 1132: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 16
Size: 258 Color: 10

Bin 1133: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 17
Size: 450 Color: 13

Bin 1134: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 11

Bin 1135: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 12
Size: 361 Color: 8

Bin 1136: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 3
Size: 204 Color: 12

Bin 1137: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 14
Size: 384 Color: 6

Bin 1138: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 283 Color: 13

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 15
Size: 148 Color: 17
Size: 124 Color: 7

Bin 1140: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 7
Size: 331 Color: 2

Bin 1141: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 3
Size: 364 Color: 18

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 13
Size: 180 Color: 8
Size: 149 Color: 6

Bin 1143: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 5
Size: 338 Color: 2

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 12
Size: 423 Color: 2
Size: 107 Color: 6

Bin 1145: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 18
Size: 389 Color: 9

Bin 1146: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 10
Size: 281 Color: 13

Bin 1147: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 16
Size: 485 Color: 17

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 15
Size: 157 Color: 13
Size: 156 Color: 4

Bin 1149: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 18
Size: 279 Color: 11

Bin 1150: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 7
Size: 275 Color: 1

Bin 1151: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 11
Size: 286 Color: 15

Bin 1152: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 3
Size: 253 Color: 18

Bin 1153: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 8
Size: 480 Color: 7

Bin 1154: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 17
Size: 347 Color: 6

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 18
Size: 195 Color: 10
Size: 194 Color: 8

Bin 1156: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 211 Color: 3

Bin 1157: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 405 Color: 5

Bin 1158: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 10
Size: 437 Color: 9

Bin 1159: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 16
Size: 218 Color: 17

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 3
Size: 350 Color: 12
Size: 197 Color: 10

Bin 1161: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 11
Size: 418 Color: 6

Bin 1162: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 5
Size: 371 Color: 6

Bin 1163: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 16
Size: 382 Color: 0

Bin 1164: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 2
Size: 303 Color: 14

Bin 1165: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 16
Size: 486 Color: 14

Bin 1166: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 7
Size: 232 Color: 12

Bin 1167: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 2
Size: 313 Color: 18

Bin 1168: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 18
Size: 314 Color: 4

Bin 1169: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 11
Size: 393 Color: 14

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 13
Size: 165 Color: 3
Size: 149 Color: 5

Bin 1171: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 2
Size: 287 Color: 19

Bin 1172: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 8
Size: 248 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 14
Size: 196 Color: 3
Size: 177 Color: 16

Bin 1174: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 12
Size: 226 Color: 10

Bin 1175: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 0
Size: 423 Color: 19

Bin 1176: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 12
Size: 274 Color: 14

Bin 1177: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 11
Size: 332 Color: 14

Bin 1178: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 5
Size: 387 Color: 8

Bin 1179: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 5
Size: 384 Color: 10

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 9
Size: 170 Color: 9
Size: 160 Color: 10

Bin 1181: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 2
Size: 470 Color: 17

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 11
Size: 177 Color: 12
Size: 148 Color: 17

Bin 1183: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 3

Bin 1184: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 6
Size: 455 Color: 1

Bin 1185: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 2
Size: 489 Color: 13

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 10
Size: 138 Color: 18
Size: 110 Color: 5

Bin 1187: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 9
Size: 428 Color: 16

Bin 1188: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 8

Bin 1189: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 245 Color: 13

Bin 1190: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 1
Size: 383 Color: 5

Bin 1191: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 18
Size: 232 Color: 9

Bin 1192: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 230 Color: 3

Bin 1193: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 3
Size: 441 Color: 11

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 10
Size: 216 Color: 11
Size: 157 Color: 10

Bin 1195: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 16
Size: 320 Color: 17

Bin 1196: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 8
Size: 244 Color: 4

Bin 1197: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 5
Size: 478 Color: 16

Bin 1198: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 5
Size: 379 Color: 18

Bin 1199: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 10
Size: 305 Color: 16

Bin 1200: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 238 Color: 14

Bin 1201: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 12
Size: 352 Color: 14

Bin 1202: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 16
Size: 381 Color: 13

Bin 1203: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 2
Size: 295 Color: 17

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 7
Size: 133 Color: 18
Size: 113 Color: 18

Bin 1205: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 4
Size: 223 Color: 8

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 10
Size: 166 Color: 19
Size: 129 Color: 2

Bin 1207: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 19
Size: 407 Color: 4

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 1
Size: 183 Color: 8
Size: 113 Color: 9

Bin 1209: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 9
Size: 401 Color: 17

Bin 1210: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 9
Size: 439 Color: 17

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 13
Size: 246 Color: 5
Size: 203 Color: 6

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 5
Size: 175 Color: 17
Size: 175 Color: 11

Bin 1213: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 17
Size: 385 Color: 3

Bin 1214: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 9
Size: 276 Color: 7

Bin 1215: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 7
Size: 467 Color: 16

Bin 1216: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 18
Size: 205 Color: 13

Bin 1217: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 16
Size: 416 Color: 11

Bin 1218: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 18
Size: 243 Color: 16

Bin 1219: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 3

Bin 1220: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 2
Size: 240 Color: 16

Bin 1221: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 18
Size: 449 Color: 2

Bin 1222: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 17
Size: 483 Color: 14

Bin 1223: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 11
Size: 407 Color: 9

Bin 1224: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 13
Size: 231 Color: 11

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 13
Size: 349 Color: 9
Size: 116 Color: 2

Bin 1226: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 0
Size: 467 Color: 14

Bin 1227: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 15
Size: 265 Color: 10

Bin 1228: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 0
Size: 491 Color: 8

Bin 1229: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 6
Size: 408 Color: 2

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 11
Size: 195 Color: 3
Size: 149 Color: 0

Bin 1231: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 8
Size: 473 Color: 6

Bin 1232: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 11
Size: 438 Color: 4

Bin 1233: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 9
Size: 340 Color: 12

Bin 1234: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 14
Size: 309 Color: 7

Bin 1235: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 13
Size: 474 Color: 11

Bin 1236: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 12

Bin 1237: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 13
Size: 499 Color: 10

Bin 1238: 0 of cap free
Amount of items: 4
Items: 
Size: 604 Color: 12
Size: 158 Color: 5
Size: 137 Color: 10
Size: 102 Color: 2

Bin 1239: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 366 Color: 6

Bin 1240: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 15
Size: 477 Color: 5

Bin 1241: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 7
Size: 410 Color: 14

Bin 1242: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 12
Size: 495 Color: 3

Bin 1243: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 8

Bin 1244: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 17
Size: 295 Color: 14

Bin 1245: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 16
Size: 477 Color: 11

Bin 1246: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 13
Size: 392 Color: 3

Bin 1247: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 11
Size: 499 Color: 6

Bin 1248: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 13
Size: 254 Color: 18

Bin 1249: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 17
Size: 486 Color: 4

Bin 1250: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 16
Size: 268 Color: 19

Bin 1251: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 16
Size: 464 Color: 18

Bin 1252: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 19
Size: 449 Color: 13

Bin 1253: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 5
Size: 390 Color: 19

Bin 1254: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 12
Size: 422 Color: 5

Bin 1255: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 12
Size: 451 Color: 5

Bin 1256: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 13
Size: 251 Color: 9

Bin 1257: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 1
Size: 434 Color: 12

Bin 1258: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 18
Size: 300 Color: 1

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 7
Size: 191 Color: 17
Size: 128 Color: 1

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 15
Size: 197 Color: 13
Size: 181 Color: 19

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 729 Color: 2
Size: 154 Color: 5
Size: 118 Color: 5

Bin 1262: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 9
Size: 373 Color: 19

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 2
Size: 193 Color: 9
Size: 179 Color: 12

Bin 1264: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 4
Size: 397 Color: 10

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 18
Size: 169 Color: 10
Size: 169 Color: 6

Bin 1266: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 13
Size: 325 Color: 10

Bin 1267: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 19
Size: 429 Color: 18

Bin 1268: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 18
Size: 343 Color: 7

Bin 1269: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 10
Size: 235 Color: 5

Bin 1270: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 16
Size: 209 Color: 10

Bin 1271: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 14
Size: 354 Color: 4

Bin 1272: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 7
Size: 320 Color: 19

Bin 1273: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 8
Size: 358 Color: 14

Bin 1274: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 0
Size: 209 Color: 16

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 3
Size: 196 Color: 11
Size: 196 Color: 6

Bin 1276: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 5
Size: 251 Color: 17

Bin 1277: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 10
Size: 457 Color: 19

Bin 1278: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 17
Size: 370 Color: 19

Bin 1279: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 8
Size: 337 Color: 3

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 12
Size: 187 Color: 19
Size: 187 Color: 16

Bin 1281: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 16
Size: 416 Color: 18

Bin 1282: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 11
Size: 326 Color: 6

Bin 1283: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 2
Size: 443 Color: 14

Bin 1284: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 13
Size: 437 Color: 11

Bin 1285: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 7
Size: 439 Color: 14

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 0
Size: 289 Color: 1
Size: 172 Color: 12

Bin 1287: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 16
Size: 294 Color: 17

Bin 1288: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 11
Size: 419 Color: 16

Bin 1289: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 11
Size: 410 Color: 15

Bin 1290: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 319 Color: 16

Bin 1291: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 18
Size: 399 Color: 5

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 4
Size: 179 Color: 2
Size: 177 Color: 10

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 19
Size: 169 Color: 14
Size: 145 Color: 3

Bin 1294: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 13
Size: 346 Color: 14

Bin 1295: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 13
Size: 472 Color: 15

Bin 1296: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 16
Size: 293 Color: 4

Bin 1297: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 371 Color: 13

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 10
Size: 394 Color: 5
Size: 190 Color: 11

Bin 1299: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 18
Size: 496 Color: 17

Bin 1300: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 378 Color: 6

Bin 1301: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 7
Size: 223 Color: 19

Bin 1302: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 3
Size: 432 Color: 17

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 14
Size: 181 Color: 7
Size: 161 Color: 13

Bin 1304: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 14
Size: 222 Color: 16

Bin 1305: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 325 Color: 10

Bin 1306: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 4
Size: 398 Color: 11

Bin 1307: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 5
Size: 410 Color: 17

Bin 1308: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 11
Size: 343 Color: 8

Bin 1309: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 2
Size: 409 Color: 15

Bin 1310: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 2
Size: 296 Color: 13

Bin 1311: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 1
Size: 392 Color: 9

Bin 1312: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 16
Size: 353 Color: 1

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 5
Size: 136 Color: 14
Size: 113 Color: 4

Bin 1314: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 2
Size: 425 Color: 19

Bin 1315: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 15
Size: 268 Color: 9

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 18
Size: 129 Color: 9
Size: 107 Color: 3

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 5
Size: 198 Color: 13
Size: 175 Color: 16

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 18
Size: 419 Color: 8
Size: 163 Color: 16

Bin 1319: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 18

Bin 1320: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 11

Bin 1321: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 19
Size: 304 Color: 3

Bin 1322: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 10
Size: 460 Color: 11

Bin 1323: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 11
Size: 484 Color: 7

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 701 Color: 6
Size: 151 Color: 3
Size: 149 Color: 19

Bin 1325: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 6
Size: 279 Color: 3

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 418 Color: 8
Size: 160 Color: 14

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 16
Size: 184 Color: 3
Size: 130 Color: 5

Bin 1328: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 9
Size: 438 Color: 10

Bin 1329: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 9
Size: 374 Color: 10

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 7
Size: 159 Color: 7
Size: 155 Color: 10

Bin 1331: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 2
Size: 256 Color: 17

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 7
Size: 141 Color: 19
Size: 140 Color: 8

Bin 1333: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 11
Size: 365 Color: 7

Bin 1334: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 11
Size: 301 Color: 9

Bin 1335: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 15
Size: 329 Color: 1

Bin 1336: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 16
Size: 295 Color: 0

Bin 1337: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 15
Size: 252 Color: 17

Bin 1338: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 2
Size: 419 Color: 15

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 13
Size: 345 Color: 19
Size: 176 Color: 3

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 14
Size: 301 Color: 15
Size: 260 Color: 18

Bin 1341: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 13
Size: 412 Color: 0

Bin 1342: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 5
Size: 433 Color: 12

Bin 1343: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 5
Size: 367 Color: 19

Bin 1344: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 3
Size: 274 Color: 0

Bin 1345: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 13
Size: 477 Color: 9

Bin 1346: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 4
Size: 345 Color: 13

Bin 1347: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 9
Size: 396 Color: 11

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 3
Size: 321 Color: 16
Size: 149 Color: 12

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 789 Color: 15
Size: 110 Color: 2
Size: 102 Color: 15

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 2
Size: 154 Color: 9
Size: 102 Color: 16

Bin 1351: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 15
Size: 230 Color: 5

Bin 1352: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 19
Size: 449 Color: 2

Bin 1353: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 10
Size: 365 Color: 14

Bin 1354: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 5
Size: 393 Color: 18

Bin 1355: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 10
Size: 325 Color: 5

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 14
Size: 180 Color: 18
Size: 118 Color: 15

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 14
Size: 128 Color: 17
Size: 128 Color: 11

Bin 1358: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 18

Bin 1359: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 9
Size: 346 Color: 11

Bin 1360: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 11

Bin 1361: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 6
Size: 309 Color: 8

Bin 1362: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 14
Size: 279 Color: 1

Bin 1363: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 486 Color: 19

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 4
Size: 172 Color: 1
Size: 158 Color: 15

Bin 1365: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 19
Size: 477 Color: 18

Bin 1366: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 3
Size: 212 Color: 11

Bin 1367: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 5
Size: 381 Color: 11

Bin 1368: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 13
Size: 437 Color: 1

Bin 1369: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 15
Size: 396 Color: 4

Bin 1370: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 16
Size: 467 Color: 8

Bin 1371: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 19
Size: 265 Color: 15

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 13
Size: 193 Color: 17
Size: 189 Color: 2

Bin 1373: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 16
Size: 227 Color: 7

Bin 1374: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 15
Size: 231 Color: 18

Bin 1375: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 18
Size: 359 Color: 1

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 16
Size: 178 Color: 9
Size: 153 Color: 19

Bin 1377: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 8
Size: 267 Color: 18

Bin 1378: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 11
Size: 388 Color: 13

Bin 1379: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 16
Size: 481 Color: 19

Bin 1380: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 14
Size: 219 Color: 19

Bin 1381: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 7
Size: 215 Color: 5

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 17
Size: 177 Color: 11
Size: 156 Color: 1

Bin 1383: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 10
Size: 303 Color: 2

Bin 1384: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 17
Size: 273 Color: 3

Bin 1385: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 8
Size: 313 Color: 1

Bin 1386: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 5
Size: 216 Color: 11

Bin 1387: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 19
Size: 443 Color: 10

Bin 1388: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 9
Size: 201 Color: 13

Bin 1389: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 2
Size: 238 Color: 0

Bin 1390: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 4

Bin 1391: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 3
Size: 263 Color: 15

Bin 1392: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 11
Size: 499 Color: 12

Bin 1393: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 14
Size: 455 Color: 8

Bin 1394: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 9
Size: 352 Color: 2

Bin 1395: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 8
Size: 387 Color: 15

Bin 1396: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 11
Size: 412 Color: 2

Bin 1397: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 2
Size: 233 Color: 15

Bin 1398: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 273 Color: 8

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 9
Size: 408 Color: 10
Size: 155 Color: 8

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 5
Size: 192 Color: 15
Size: 186 Color: 4

Bin 1401: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 5
Size: 445 Color: 15

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 1
Size: 292 Color: 1
Size: 147 Color: 6

Bin 1403: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 11
Size: 385 Color: 5

Bin 1404: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 16
Size: 433 Color: 17

Bin 1405: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 8
Size: 296 Color: 17

Bin 1406: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 8

Bin 1407: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 7
Size: 409 Color: 17

Bin 1408: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 2
Size: 433 Color: 10

Bin 1409: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 6
Size: 372 Color: 13

Bin 1410: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 0
Size: 356 Color: 19

Bin 1411: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 8
Size: 335 Color: 14

Bin 1412: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 3
Size: 216 Color: 4

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 7
Size: 110 Color: 9
Size: 109 Color: 8

Bin 1414: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 6
Size: 402 Color: 15

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 16
Size: 289 Color: 8
Size: 139 Color: 1

Bin 1416: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 17
Size: 224 Color: 11

Bin 1417: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 17

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 13
Size: 171 Color: 9
Size: 158 Color: 6

Bin 1419: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 6
Size: 462 Color: 12

Bin 1420: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 5
Size: 262 Color: 1

Bin 1421: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 0
Size: 206 Color: 5

Bin 1422: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 18
Size: 305 Color: 14

Bin 1423: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 10
Size: 465 Color: 18

Bin 1424: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 10
Size: 295 Color: 11

Bin 1425: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 282 Color: 9

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 8
Size: 161 Color: 19
Size: 156 Color: 1

Bin 1427: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 6
Size: 436 Color: 12

Bin 1428: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 7
Size: 418 Color: 6

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 8
Size: 173 Color: 19
Size: 173 Color: 7

Bin 1430: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 17
Size: 251 Color: 9

Bin 1431: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 6
Size: 440 Color: 9

Bin 1432: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 15
Size: 479 Color: 1

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 0
Size: 165 Color: 5
Size: 148 Color: 3

Bin 1434: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 11
Size: 243 Color: 13

Bin 1435: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 12
Size: 371 Color: 9

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 10
Size: 138 Color: 6
Size: 138 Color: 1

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 2
Size: 192 Color: 19
Size: 125 Color: 1

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 9
Size: 135 Color: 15
Size: 132 Color: 3

Bin 1439: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 16
Size: 259 Color: 13

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 11
Size: 326 Color: 11
Size: 132 Color: 7

Bin 1441: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 10
Size: 338 Color: 16

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 0
Size: 242 Color: 1
Size: 155 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 16
Size: 420 Color: 13
Size: 128 Color: 13

Bin 1444: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 4
Size: 354 Color: 5

Bin 1445: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 9
Size: 497 Color: 5

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 12
Size: 326 Color: 6
Size: 199 Color: 12

Bin 1447: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 17
Size: 475 Color: 11

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 11
Size: 332 Color: 5
Size: 186 Color: 5

Bin 1449: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 13
Size: 414 Color: 2

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 17
Size: 256 Color: 9
Size: 127 Color: 6

Bin 1451: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 8
Size: 302 Color: 7

Bin 1452: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 17
Size: 363 Color: 13

Bin 1453: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 11
Size: 471 Color: 8

Bin 1454: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 17
Size: 213 Color: 12

Bin 1455: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 11
Size: 353 Color: 5

Bin 1456: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 9
Size: 341 Color: 13

Bin 1457: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 18
Size: 285 Color: 8

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 13
Size: 188 Color: 4
Size: 159 Color: 7

Bin 1459: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 11
Size: 211 Color: 16

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 7
Size: 191 Color: 9
Size: 123 Color: 14

Bin 1461: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 9
Size: 483 Color: 17

Bin 1462: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 4
Size: 401 Color: 16

Bin 1463: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 17
Size: 471 Color: 9

Bin 1464: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 10
Size: 323 Color: 9

Bin 1465: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 12
Size: 287 Color: 3

Bin 1466: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 8
Size: 303 Color: 1

Bin 1467: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 7
Size: 393 Color: 16

Bin 1468: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 19
Size: 248 Color: 1

Bin 1469: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 15
Size: 482 Color: 13

Bin 1470: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 4
Size: 297 Color: 18

Bin 1471: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 3
Size: 397 Color: 7

Bin 1472: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 17
Size: 414 Color: 19

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 16
Size: 134 Color: 10
Size: 134 Color: 5

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 18
Size: 169 Color: 0
Size: 160 Color: 1

Bin 1475: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 7
Size: 252 Color: 15

Bin 1476: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 9
Size: 440 Color: 17

Bin 1477: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 19
Size: 264 Color: 9

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 403 Color: 14
Size: 143 Color: 10

Bin 1479: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 4
Size: 330 Color: 17

Bin 1480: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 9
Size: 241 Color: 12

Bin 1481: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 2
Size: 461 Color: 7

Bin 1482: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 13

Bin 1483: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 6

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 11
Size: 157 Color: 15
Size: 156 Color: 8

Bin 1485: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 404 Color: 18

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 128 Color: 5
Size: 127 Color: 0

Bin 1487: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 15
Size: 403 Color: 6

Bin 1488: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 13
Size: 359 Color: 4

Bin 1489: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 9
Size: 225 Color: 16

Bin 1490: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 7
Size: 369 Color: 13

Bin 1491: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 5
Size: 455 Color: 16

Bin 1492: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 12
Size: 288 Color: 18

Bin 1493: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 4
Size: 477 Color: 16

Bin 1494: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 12

Bin 1495: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 17
Size: 213 Color: 13

Bin 1496: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 7
Size: 219 Color: 12

Bin 1497: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 9
Size: 424 Color: 16

Bin 1498: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 205 Color: 0

Bin 1499: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 10
Size: 312 Color: 1

Bin 1500: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 16
Size: 258 Color: 0

Bin 1501: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 3
Size: 480 Color: 6

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 12
Size: 242 Color: 11
Size: 186 Color: 7

Bin 1503: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 7
Size: 414 Color: 18

Bin 1504: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 9
Size: 392 Color: 2

Bin 1505: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 5
Size: 482 Color: 15

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 385 Color: 10
Size: 118 Color: 16

Bin 1507: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 1
Size: 426 Color: 16

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 15
Size: 198 Color: 12
Size: 198 Color: 4

Bin 1509: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 3
Size: 215 Color: 18

Bin 1510: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 14
Size: 352 Color: 17

Bin 1511: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 11
Size: 379 Color: 6

Bin 1512: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 9
Size: 304 Color: 11

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 18
Size: 183 Color: 10
Size: 110 Color: 17

Bin 1514: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 13
Size: 490 Color: 1

Bin 1515: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 4
Size: 290 Color: 17

Bin 1516: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 14
Size: 382 Color: 19

Bin 1517: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 12
Size: 267 Color: 13

Bin 1518: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 10
Size: 204 Color: 8

Bin 1519: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 13
Size: 257 Color: 1

Bin 1520: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 15
Size: 332 Color: 19

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 0
Size: 113 Color: 5
Size: 108 Color: 7

Bin 1522: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 15

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 17
Size: 395 Color: 17
Size: 184 Color: 2

Bin 1524: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 13
Size: 244 Color: 16

Bin 1525: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 12
Size: 306 Color: 10

Bin 1526: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 16
Size: 280 Color: 18

Bin 1527: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 4
Size: 369 Color: 7

Bin 1528: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 4
Size: 286 Color: 7

Bin 1529: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 6
Size: 358 Color: 15

Bin 1530: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 10
Size: 461 Color: 14

Bin 1531: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 7
Size: 257 Color: 0

Bin 1532: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 14
Size: 463 Color: 15

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 18
Size: 322 Color: 7
Size: 121 Color: 6

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 17
Size: 128 Color: 12
Size: 120 Color: 5

Bin 1535: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 9
Size: 358 Color: 18

Bin 1536: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 8
Size: 337 Color: 19

Bin 1537: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 19
Size: 209 Color: 7

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 6
Size: 174 Color: 2
Size: 172 Color: 12

Bin 1539: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 13

Bin 1540: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 7
Size: 417 Color: 17

Bin 1541: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 19
Size: 306 Color: 15

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 644 Color: 12
Size: 191 Color: 3
Size: 166 Color: 7

Bin 1543: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 9
Size: 298 Color: 3

Bin 1544: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 479 Color: 11

Bin 1545: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 15

Bin 1546: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 8
Size: 220 Color: 11

Bin 1547: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 307 Color: 13

Bin 1548: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 18
Size: 260 Color: 14

Bin 1549: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 8
Size: 230 Color: 7

Bin 1550: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 9
Size: 330 Color: 5

Bin 1551: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 10
Size: 488 Color: 4

Bin 1552: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 10
Size: 277 Color: 9

Bin 1553: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 7
Size: 230 Color: 4

Bin 1554: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 10
Size: 326 Color: 5

Bin 1555: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 8
Size: 425 Color: 9

Bin 1556: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 17
Size: 257 Color: 12

Bin 1557: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 17
Size: 426 Color: 10

Bin 1558: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 11
Size: 403 Color: 16

Bin 1559: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 3
Size: 430 Color: 17

Bin 1560: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 5
Size: 480 Color: 6

Bin 1561: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 12
Size: 281 Color: 16

Bin 1562: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 16
Size: 440 Color: 15

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 18
Size: 334 Color: 1
Size: 187 Color: 9

Bin 1564: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 2
Size: 220 Color: 15

Bin 1565: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 8
Size: 249 Color: 12

Bin 1566: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 13
Size: 322 Color: 6

Bin 1567: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 6

Bin 1568: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 18
Size: 352 Color: 14

Bin 1569: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 13
Size: 434 Color: 2

Bin 1570: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 5
Size: 432 Color: 19

Bin 1571: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 12
Size: 336 Color: 9

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 8
Size: 188 Color: 7
Size: 187 Color: 18

Bin 1573: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 19
Size: 363 Color: 18

Bin 1574: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 19
Size: 354 Color: 16

Bin 1575: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 19
Size: 275 Color: 3

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 4
Size: 204 Color: 3
Size: 105 Color: 13

Bin 1577: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 8
Size: 338 Color: 11

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 1
Size: 125 Color: 3
Size: 119 Color: 6

Bin 1579: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 12
Size: 347 Color: 6

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 6
Size: 420 Color: 15
Size: 149 Color: 6

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 6
Size: 150 Color: 14
Size: 147 Color: 9

Bin 1582: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 8
Size: 392 Color: 10

Bin 1583: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 15
Size: 272 Color: 16

Bin 1584: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 13
Size: 241 Color: 5

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 6
Size: 163 Color: 5
Size: 104 Color: 10

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 384 Color: 7
Size: 164 Color: 2

Bin 1587: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 7
Size: 223 Color: 6

Bin 1588: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 11
Size: 206 Color: 6

Bin 1589: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 2
Size: 309 Color: 15

Bin 1590: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 12
Size: 322 Color: 4

Bin 1591: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 16
Size: 464 Color: 11

Bin 1592: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 19
Size: 272 Color: 6

Bin 1593: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 13
Size: 487 Color: 1

Bin 1594: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 6
Size: 427 Color: 2

Bin 1595: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 18
Size: 463 Color: 13

Bin 1596: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 19
Size: 447 Color: 14

Bin 1597: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 19

Bin 1598: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 9
Size: 230 Color: 4

Bin 1599: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 7
Size: 200 Color: 12

Bin 1600: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 17
Size: 209 Color: 1

Bin 1601: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 19
Size: 241 Color: 16

Bin 1602: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 12
Size: 417 Color: 16

Bin 1603: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 18
Size: 385 Color: 10

Bin 1604: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 8
Size: 476 Color: 17

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 13
Size: 415 Color: 2
Size: 127 Color: 8

Bin 1606: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 18
Size: 209 Color: 12

Bin 1607: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 0
Size: 351 Color: 4

Bin 1608: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 17
Size: 225 Color: 11

Bin 1609: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 19
Size: 300 Color: 0

Bin 1610: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 466 Color: 8

Bin 1611: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 15
Size: 321 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 14
Size: 419 Color: 1
Size: 162 Color: 6

Bin 1613: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 3
Size: 406 Color: 8

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 16
Size: 198 Color: 17
Size: 135 Color: 19

Bin 1615: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 2
Size: 456 Color: 12

Bin 1616: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 19

Bin 1617: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 14
Size: 475 Color: 19

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 0
Size: 169 Color: 17
Size: 153 Color: 7

Bin 1619: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 0
Size: 458 Color: 1

Bin 1620: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 7
Size: 489 Color: 18

Bin 1621: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 18
Size: 252 Color: 9

Bin 1622: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 16
Size: 357 Color: 17

Bin 1623: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 18
Size: 222 Color: 15

Bin 1624: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 12
Size: 472 Color: 15

Bin 1625: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 5
Size: 409 Color: 19

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 14
Size: 159 Color: 12
Size: 119 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 9
Size: 114 Color: 18
Size: 101 Color: 2

Bin 1628: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 18
Size: 325 Color: 10

Bin 1629: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 5
Size: 345 Color: 13

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 403 Color: 11
Size: 178 Color: 12

Bin 1631: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 7
Size: 473 Color: 18

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 16
Size: 141 Color: 13
Size: 141 Color: 9

Bin 1633: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 9
Size: 218 Color: 11

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 17
Size: 167 Color: 10
Size: 121 Color: 19

Bin 1635: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 16
Size: 379 Color: 0

Bin 1636: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 14
Size: 206 Color: 0

Bin 1637: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 3

Bin 1638: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 5
Size: 213 Color: 3

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 6
Size: 281 Color: 9
Size: 168 Color: 2

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 17
Size: 117 Color: 4
Size: 116 Color: 10

Bin 1641: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 10
Size: 348 Color: 2

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 14
Size: 120 Color: 12
Size: 112 Color: 7

Bin 1643: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 10
Size: 432 Color: 8

Bin 1644: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 10
Size: 209 Color: 15

Bin 1645: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 13
Size: 319 Color: 3

Bin 1646: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 6
Size: 318 Color: 3

Bin 1647: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 16
Size: 427 Color: 10

Bin 1648: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 231 Color: 13

Bin 1649: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 19
Size: 336 Color: 14

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 1
Size: 186 Color: 18
Size: 164 Color: 17

Bin 1651: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 12
Size: 354 Color: 4

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 12
Size: 137 Color: 4
Size: 111 Color: 19

Bin 1653: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 13
Size: 231 Color: 9

Bin 1654: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 17
Size: 295 Color: 5

Bin 1655: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 12
Size: 399 Color: 2

Bin 1656: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 6
Size: 347 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 12
Size: 296 Color: 3
Size: 143 Color: 8

Bin 1658: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 17
Size: 392 Color: 10

Bin 1659: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 11
Size: 207 Color: 12

Bin 1660: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 13
Size: 368 Color: 7

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 8
Size: 180 Color: 3
Size: 166 Color: 5

Bin 1662: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 16
Size: 298 Color: 10

Bin 1663: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 10
Size: 484 Color: 12

Bin 1664: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 15
Size: 288 Color: 13

Bin 1665: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 19

Bin 1666: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 10
Size: 457 Color: 15

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 438 Color: 10
Size: 115 Color: 15

Bin 1668: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 3
Size: 428 Color: 5

Bin 1669: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 2

Bin 1670: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 19
Size: 427 Color: 8

Bin 1671: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 10

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 16
Size: 161 Color: 5
Size: 126 Color: 12

Bin 1673: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 16
Size: 330 Color: 3

Bin 1674: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 7

Bin 1675: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 14
Size: 490 Color: 13

Bin 1676: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 16
Size: 409 Color: 1

Bin 1677: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 12
Size: 210 Color: 8

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 0
Size: 151 Color: 7
Size: 136 Color: 15

Bin 1679: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 14

Bin 1680: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 5
Size: 362 Color: 16

Bin 1681: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 237 Color: 9

Bin 1682: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 12
Size: 411 Color: 16

Bin 1683: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 19
Size: 317 Color: 5

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 12
Size: 313 Color: 16
Size: 121 Color: 12

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 9
Size: 346 Color: 16
Size: 105 Color: 11

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 352 Color: 8
Size: 171 Color: 3

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 1
Size: 185 Color: 6
Size: 173 Color: 17

Bin 1688: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 0
Size: 460 Color: 17

Bin 1689: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 3
Size: 450 Color: 19

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 16
Size: 194 Color: 4
Size: 189 Color: 13

Bin 1691: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 12
Size: 267 Color: 6

Bin 1692: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 19
Size: 497 Color: 13

Bin 1693: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 8
Size: 211 Color: 16

Bin 1694: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 19
Size: 434 Color: 8

Bin 1695: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 8
Size: 470 Color: 18

Bin 1696: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 12
Size: 239 Color: 9

Bin 1697: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 17
Size: 487 Color: 8

Bin 1698: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 12
Size: 281 Color: 9

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 10
Size: 138 Color: 10
Size: 108 Color: 7

Bin 1700: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 18
Size: 465 Color: 4

Bin 1701: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 12
Size: 385 Color: 0

Bin 1702: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 17
Size: 422 Color: 5

Bin 1703: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 14
Size: 394 Color: 5

Bin 1704: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 11
Size: 329 Color: 18

Bin 1705: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 4
Size: 404 Color: 14

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 14
Size: 104 Color: 12
Size: 102 Color: 15

Bin 1707: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 222 Color: 17

Bin 1708: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 6
Size: 465 Color: 13

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 712 Color: 14
Size: 174 Color: 1
Size: 115 Color: 5

Bin 1710: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 2
Size: 204 Color: 9

Bin 1711: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 8
Size: 425 Color: 5

Bin 1712: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 17
Size: 218 Color: 13

Bin 1713: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 0
Size: 407 Color: 1

Bin 1714: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 12
Size: 207 Color: 13

Bin 1715: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 4
Size: 358 Color: 7

Bin 1716: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 18
Size: 271 Color: 6

Bin 1717: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 1
Size: 435 Color: 5

Bin 1718: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 19
Size: 431 Color: 12

Bin 1719: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 6
Size: 329 Color: 3

Bin 1720: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 13
Size: 235 Color: 19

Bin 1721: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 14
Size: 346 Color: 8

Bin 1722: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 19
Size: 282 Color: 11

Bin 1723: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 1
Size: 273 Color: 5

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 14
Size: 342 Color: 12
Size: 134 Color: 4

Bin 1725: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 9
Size: 220 Color: 14

Bin 1726: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 15
Size: 296 Color: 16

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 6
Size: 162 Color: 14
Size: 161 Color: 18

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 15
Size: 186 Color: 2
Size: 182 Color: 7

Bin 1729: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 14
Size: 381 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 8
Size: 112 Color: 5
Size: 102 Color: 3

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 6
Size: 193 Color: 16
Size: 180 Color: 13

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 9
Size: 107 Color: 15
Size: 107 Color: 5

Bin 1733: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 281 Color: 7

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 263 Color: 12
Size: 260 Color: 17

Bin 1735: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 495 Color: 12

Bin 1736: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 7

Bin 1737: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 10
Size: 357 Color: 3

Bin 1738: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 14
Size: 350 Color: 1

Bin 1739: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 284 Color: 17

Bin 1740: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 13
Size: 258 Color: 14

Bin 1741: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 4
Size: 347 Color: 10

Bin 1742: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 18
Size: 424 Color: 3

Bin 1743: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 2
Size: 309 Color: 10

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 3
Size: 300 Color: 12
Size: 151 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 7
Size: 370 Color: 19
Size: 187 Color: 15

Bin 1746: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 11
Size: 297 Color: 2

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 15
Size: 166 Color: 8
Size: 102 Color: 12

Bin 1748: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 6
Size: 444 Color: 5

Bin 1749: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 3
Size: 473 Color: 16

Bin 1750: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 6
Size: 355 Color: 2

Bin 1751: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 7
Size: 456 Color: 2

Bin 1752: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 19
Size: 260 Color: 8

Bin 1753: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 12
Size: 415 Color: 15

Bin 1754: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 10
Size: 217 Color: 17

Bin 1755: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 18

Bin 1756: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 14
Size: 259 Color: 9

Bin 1757: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 295 Color: 15

Bin 1758: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 485 Color: 12

Bin 1759: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 482 Color: 14

Bin 1760: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 16
Size: 420 Color: 12

Bin 1761: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 13
Size: 272 Color: 8

Bin 1762: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 15
Size: 431 Color: 4

Bin 1763: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 1
Size: 275 Color: 15

Bin 1764: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 18
Size: 391 Color: 12

Bin 1765: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 14
Size: 403 Color: 4

Bin 1766: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 17
Size: 340 Color: 15

Bin 1767: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 9
Size: 219 Color: 16

Bin 1768: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 13
Size: 327 Color: 3

Bin 1769: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 7
Size: 236 Color: 18

Bin 1770: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 8
Size: 285 Color: 12

Bin 1771: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 4
Size: 342 Color: 15

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 9
Size: 234 Color: 0
Size: 196 Color: 1

Bin 1773: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 18
Size: 237 Color: 8

Bin 1774: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 9
Size: 256 Color: 18

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 9
Size: 416 Color: 4
Size: 153 Color: 8

Bin 1776: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 9

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 6
Size: 305 Color: 15
Size: 145 Color: 3

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 8
Size: 104 Color: 5
Size: 101 Color: 15

Bin 1779: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 8
Size: 258 Color: 7

Bin 1780: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 5
Size: 231 Color: 9

Bin 1781: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 16
Size: 203 Color: 19

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 7
Size: 321 Color: 3
Size: 113 Color: 12

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 16
Size: 418 Color: 7
Size: 139 Color: 3

Bin 1784: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 16
Size: 393 Color: 14

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 8
Size: 305 Color: 9
Size: 192 Color: 7

Bin 1786: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 4
Size: 465 Color: 7

Bin 1787: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 19
Size: 425 Color: 11

Bin 1788: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 0
Size: 399 Color: 11

Bin 1789: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 15
Size: 328 Color: 12

Bin 1790: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 16
Size: 473 Color: 15

Bin 1791: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 8
Size: 500 Color: 14

Bin 1792: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 0
Size: 412 Color: 3

Bin 1793: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 6
Size: 395 Color: 8

Bin 1794: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 17
Size: 380 Color: 3

Bin 1795: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 3
Size: 468 Color: 18

Bin 1796: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 5
Size: 306 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 1
Size: 276 Color: 6
Size: 116 Color: 16

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 406 Color: 12
Size: 153 Color: 6

Bin 1799: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 18
Size: 447 Color: 16

Bin 1800: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 486 Color: 7

Bin 1801: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 0
Size: 287 Color: 4

Bin 1802: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 15
Size: 267 Color: 9

Bin 1803: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 15
Size: 484 Color: 10

Bin 1804: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 2
Size: 400 Color: 3

Bin 1805: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 0
Size: 310 Color: 14

Bin 1806: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 8
Size: 439 Color: 13

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 15
Size: 408 Color: 8
Size: 161 Color: 14

Bin 1808: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 7
Size: 281 Color: 1

Bin 1809: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 16
Size: 294 Color: 2

Bin 1810: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 9
Size: 306 Color: 11

Bin 1811: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 10
Size: 461 Color: 12

Bin 1812: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 10
Size: 473 Color: 19

Bin 1813: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 9
Size: 427 Color: 16

Bin 1814: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 13
Size: 257 Color: 16

Bin 1815: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 18
Size: 200 Color: 3

Bin 1816: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 16
Size: 308 Color: 18

Bin 1817: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 16

Bin 1818: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 4
Size: 346 Color: 5

Bin 1819: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 5
Size: 339 Color: 8

Bin 1820: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 13
Size: 298 Color: 6

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 15
Size: 143 Color: 13
Size: 143 Color: 11

Bin 1822: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 3
Size: 214 Color: 1

Bin 1823: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 7
Size: 446 Color: 14

Bin 1824: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 12
Size: 388 Color: 8

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 596 Color: 19
Size: 271 Color: 0
Size: 134 Color: 10

Bin 1826: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 7
Size: 417 Color: 19

Bin 1827: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 13
Size: 217 Color: 12

Bin 1828: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 19
Size: 372 Color: 13

Bin 1829: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 5
Size: 336 Color: 2

Bin 1830: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 5
Size: 370 Color: 11

Bin 1831: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 5
Size: 210 Color: 7

Bin 1832: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 10
Size: 458 Color: 6

Bin 1833: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 11
Size: 338 Color: 8

Bin 1834: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 9
Size: 460 Color: 10

Bin 1835: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 5
Size: 444 Color: 14

Bin 1836: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 13
Size: 297 Color: 10

Bin 1837: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 19
Size: 250 Color: 1

Bin 1838: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 12
Size: 330 Color: 17

Bin 1839: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 19
Size: 210 Color: 0

Bin 1840: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 13

Bin 1841: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 3
Size: 206 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 564 Color: 12
Size: 326 Color: 3
Size: 111 Color: 12

Bin 1843: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 8
Size: 496 Color: 7

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 2
Size: 132 Color: 2
Size: 117 Color: 5

Bin 1845: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 7
Size: 339 Color: 10

Bin 1846: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 6
Size: 288 Color: 4

Bin 1847: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 6
Size: 406 Color: 13

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 14
Size: 145 Color: 13
Size: 123 Color: 15

Bin 1849: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 0
Size: 299 Color: 6

Bin 1850: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 12
Size: 365 Color: 9

Bin 1851: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 12
Size: 267 Color: 9

Bin 1852: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 7

Bin 1853: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 9
Size: 366 Color: 3

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 18
Size: 438 Color: 7
Size: 110 Color: 5

Bin 1855: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 4
Size: 445 Color: 14

Bin 1856: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 8
Size: 404 Color: 17

Bin 1857: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 18
Size: 331 Color: 3

Bin 1858: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 0
Size: 481 Color: 8

Bin 1859: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 5
Size: 230 Color: 0

Bin 1860: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 4
Size: 277 Color: 2

Bin 1861: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 9
Size: 338 Color: 5

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 515 Color: 7
Size: 289 Color: 11
Size: 197 Color: 11

Bin 1863: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 16
Size: 492 Color: 13

Bin 1864: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 10
Size: 408 Color: 1

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6
Size: 320 Color: 17
Size: 320 Color: 16

Bin 1866: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 13
Size: 386 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 2
Size: 152 Color: 6
Size: 150 Color: 15

Bin 1868: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 11
Size: 276 Color: 16

Bin 1869: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 11
Size: 404 Color: 16

Bin 1870: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 12
Size: 332 Color: 1

Bin 1871: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 7
Size: 241 Color: 14

Bin 1872: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 13
Size: 254 Color: 7

Bin 1873: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 7
Size: 275 Color: 8

Bin 1874: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 6
Size: 492 Color: 15

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 2
Size: 252 Color: 5
Size: 136 Color: 3

Bin 1876: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 5
Size: 423 Color: 4

Bin 1877: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 4
Size: 226 Color: 6

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 15
Size: 196 Color: 14
Size: 184 Color: 8

Bin 1879: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 5
Size: 495 Color: 18

Bin 1880: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 222 Color: 16

Bin 1881: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 10
Size: 256 Color: 3

Bin 1882: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 13
Size: 464 Color: 6

Bin 1883: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 11
Size: 432 Color: 5

Bin 1884: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 0
Size: 201 Color: 18

Bin 1885: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 387 Color: 18

Bin 1886: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 12
Size: 420 Color: 15

Bin 1887: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 12
Size: 201 Color: 18

Bin 1888: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 12
Size: 221 Color: 7

Bin 1889: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 2
Size: 386 Color: 12

Bin 1890: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 6
Size: 407 Color: 4

Bin 1891: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 16
Size: 246 Color: 5

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 19
Size: 280 Color: 13
Size: 159 Color: 4

Bin 1893: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 4

Bin 1894: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 8
Size: 310 Color: 18

Bin 1895: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 11
Size: 384 Color: 18

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 7
Size: 158 Color: 17
Size: 157 Color: 2

Bin 1897: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 19

Bin 1898: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 10
Size: 397 Color: 19

Bin 1899: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 6
Size: 457 Color: 1

Bin 1900: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 3
Size: 258 Color: 17

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 1
Size: 171 Color: 13
Size: 132 Color: 11

Bin 1902: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 10

Bin 1903: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 11
Size: 307 Color: 2

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 18
Size: 178 Color: 2
Size: 149 Color: 13

Bin 1905: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 8
Size: 480 Color: 0

Bin 1906: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 7
Size: 346 Color: 12

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 17
Size: 182 Color: 15
Size: 137 Color: 13

Bin 1908: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 448 Color: 1

Bin 1909: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 14
Size: 412 Color: 16

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 701 Color: 6
Size: 156 Color: 11
Size: 144 Color: 3

Bin 1911: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 15
Size: 224 Color: 9

Bin 1912: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 6
Size: 371 Color: 9

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 10
Size: 116 Color: 15
Size: 106 Color: 10

Bin 1914: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 11
Size: 211 Color: 14

Bin 1915: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 1
Size: 457 Color: 16

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 4
Size: 174 Color: 5
Size: 172 Color: 0

Bin 1917: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 13
Size: 422 Color: 1

Bin 1918: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 4
Size: 265 Color: 15

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 17
Size: 146 Color: 2
Size: 137 Color: 15

Bin 1920: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 7
Size: 387 Color: 4

Bin 1921: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 7
Size: 284 Color: 14

Bin 1922: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 8
Size: 432 Color: 2

Bin 1923: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 5
Size: 221 Color: 1

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 16
Size: 129 Color: 14
Size: 119 Color: 19

Bin 1925: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 5
Size: 465 Color: 18

Bin 1926: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 13
Size: 401 Color: 14

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 19
Size: 398 Color: 10
Size: 144 Color: 3

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 11
Size: 385 Color: 10
Size: 122 Color: 5

Bin 1929: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 16
Size: 212 Color: 6

Bin 1930: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 0
Size: 298 Color: 12

Bin 1931: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 17
Size: 441 Color: 4

Bin 1932: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 10
Size: 316 Color: 16

Bin 1933: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 18
Size: 397 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 408 Color: 0
Size: 175 Color: 5

Bin 1935: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 12
Size: 302 Color: 15

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 12
Size: 181 Color: 3
Size: 117 Color: 1

Bin 1937: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 5

Bin 1938: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 4
Size: 457 Color: 7

Bin 1939: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 19
Size: 437 Color: 15

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 15
Size: 352 Color: 6
Size: 186 Color: 1

Bin 1941: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 3
Size: 221 Color: 0

Bin 1942: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 9
Size: 232 Color: 8

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 7
Size: 164 Color: 7
Size: 151 Color: 14

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 2
Size: 155 Color: 14
Size: 153 Color: 12

Bin 1945: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 4
Size: 313 Color: 13

Bin 1946: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 5
Size: 399 Color: 18

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 347 Color: 12
Size: 182 Color: 2

Bin 1948: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 8
Size: 398 Color: 7

Bin 1949: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 3
Size: 398 Color: 5

Bin 1950: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 3
Size: 228 Color: 14

Bin 1951: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 12
Size: 295 Color: 15

Bin 1952: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 4
Size: 466 Color: 2

Bin 1953: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 14
Size: 401 Color: 2

Bin 1954: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 4
Size: 397 Color: 15

Bin 1955: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 15

Bin 1956: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 9
Size: 409 Color: 11

Bin 1957: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 14
Size: 295 Color: 11

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 4
Size: 226 Color: 0
Size: 180 Color: 2

Bin 1959: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 15
Size: 442 Color: 18

Bin 1960: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 9
Size: 421 Color: 19

Bin 1961: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 14
Size: 373 Color: 1

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 15
Size: 351 Color: 17
Size: 234 Color: 7

Bin 1963: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 8
Size: 369 Color: 13

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 8
Size: 174 Color: 8
Size: 113 Color: 18

Bin 1965: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 17
Size: 331 Color: 7

Bin 1966: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 16
Size: 326 Color: 15

Bin 1967: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 6
Size: 404 Color: 3

Bin 1968: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 17

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 4
Size: 261 Color: 15
Size: 188 Color: 4

Bin 1970: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 0
Size: 469 Color: 13

Bin 1971: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 7
Size: 453 Color: 9

Bin 1972: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 4
Size: 450 Color: 7

Bin 1973: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 2

Bin 1974: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 2
Size: 431 Color: 3

Bin 1975: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 6
Size: 480 Color: 12

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 2
Size: 105 Color: 13
Size: 104 Color: 15

Bin 1977: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 7
Size: 356 Color: 4

Bin 1978: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 8
Size: 414 Color: 15

Bin 1979: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 12
Size: 457 Color: 14

Bin 1980: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 5
Size: 307 Color: 1

Bin 1981: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 18
Size: 463 Color: 17

Bin 1982: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 12
Size: 216 Color: 5

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 7
Size: 147 Color: 7
Size: 119 Color: 10

Bin 1984: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 2
Size: 410 Color: 7

Bin 1985: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 5
Size: 219 Color: 8

Bin 1986: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 1
Size: 228 Color: 14

Bin 1987: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 19
Size: 370 Color: 10

Bin 1988: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 4
Size: 293 Color: 11

Bin 1989: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 10
Size: 244 Color: 11

Bin 1990: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 14
Size: 303 Color: 6

Bin 1991: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 15
Size: 334 Color: 14

Bin 1992: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 14
Size: 424 Color: 2

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 360 Color: 15
Size: 280 Color: 15

Bin 1994: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 11
Size: 451 Color: 3

Bin 1995: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 8
Size: 330 Color: 10

Bin 1996: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 11
Size: 482 Color: 9

Bin 1997: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 8
Size: 218 Color: 18

Bin 1998: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 16
Size: 362 Color: 10

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 14
Size: 126 Color: 1
Size: 115 Color: 19

Bin 2000: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 17
Size: 480 Color: 3

Bin 2001: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 3
Size: 451 Color: 2

Bin 2002: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 14

Bin 2003: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 11
Size: 416 Color: 2

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 0
Size: 245 Color: 13
Size: 135 Color: 7

Bin 2005: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 13
Size: 426 Color: 2

Bin 2006: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 17

Bin 2007: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 14
Size: 239 Color: 17

Bin 2008: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 19
Size: 394 Color: 14

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 15
Size: 382 Color: 0
Size: 121 Color: 16

Bin 2010: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 12
Size: 345 Color: 15

Bin 2011: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 4
Size: 289 Color: 8

Bin 2012: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 307 Color: 2

Bin 2013: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 13
Size: 257 Color: 7

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 400 Color: 13
Size: 178 Color: 3

Bin 2015: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 15
Size: 228 Color: 9

Bin 2016: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 10
Size: 316 Color: 2

Bin 2017: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 14
Size: 276 Color: 7

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 10
Size: 139 Color: 8
Size: 128 Color: 12

Bin 2019: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 8
Size: 231 Color: 4

Bin 2020: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 6
Size: 302 Color: 4

Bin 2021: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 7
Size: 269 Color: 17

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 8
Size: 318 Color: 6
Size: 149 Color: 1

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 2
Size: 192 Color: 2
Size: 188 Color: 16

Bin 2024: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 15
Size: 319 Color: 10

Bin 2025: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 17
Size: 217 Color: 13

Bin 2026: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 11
Size: 271 Color: 18

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 2
Size: 192 Color: 11
Size: 181 Color: 17

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 6
Size: 208 Color: 13
Size: 174 Color: 18

Bin 2029: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 7
Size: 349 Color: 17

Bin 2030: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 19
Size: 300 Color: 0

Bin 2031: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 16
Size: 364 Color: 17

Bin 2032: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 7
Size: 450 Color: 15

Bin 2033: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 257 Color: 14

Bin 2034: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 6
Size: 324 Color: 15

Bin 2035: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 8
Size: 208 Color: 17

Bin 2036: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 264 Color: 1

Bin 2037: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 13
Size: 435 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 12
Size: 273 Color: 12
Size: 148 Color: 1

Bin 2039: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 0
Size: 315 Color: 9

Bin 2040: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 16
Size: 215 Color: 11

Bin 2041: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 15
Size: 466 Color: 4

Bin 2042: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 19
Size: 420 Color: 2

Bin 2043: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 17
Size: 237 Color: 7

Bin 2044: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 15
Size: 386 Color: 16

Bin 2045: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 4
Size: 223 Color: 16

Bin 2046: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 10
Size: 391 Color: 16

Bin 2047: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 7
Size: 275 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 13
Size: 180 Color: 16
Size: 139 Color: 4

Bin 2049: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 12
Size: 458 Color: 16

Bin 2050: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 12
Size: 339 Color: 16

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 12
Size: 312 Color: 19
Size: 197 Color: 4

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 12
Size: 143 Color: 1
Size: 135 Color: 8

Bin 2053: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 8
Size: 427 Color: 19

Bin 2054: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 7
Size: 431 Color: 16

Bin 2055: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 0
Size: 202 Color: 1

Bin 2056: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 5
Size: 427 Color: 7

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 0
Size: 196 Color: 0
Size: 179 Color: 13

Bin 2058: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 18
Size: 468 Color: 15

Bin 2059: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 8
Size: 233 Color: 13

Bin 2060: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 0
Size: 451 Color: 3

Bin 2061: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 16
Size: 385 Color: 10

Bin 2062: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 17
Size: 282 Color: 14

Bin 2063: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 13
Size: 426 Color: 19

Bin 2064: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 16
Size: 302 Color: 4

Bin 2065: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 15
Size: 385 Color: 2

Bin 2066: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 15
Size: 269 Color: 3

Bin 2067: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 19
Size: 201 Color: 12

Bin 2068: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 15
Size: 456 Color: 11

Bin 2069: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 2
Size: 268 Color: 6

Bin 2070: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 2
Size: 455 Color: 15

Bin 2071: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 18
Size: 277 Color: 4

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 17
Size: 191 Color: 2
Size: 189 Color: 17

Bin 2073: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 4
Size: 434 Color: 8

Bin 2074: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 349 Color: 16

Bin 2075: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 12
Size: 251 Color: 1

Bin 2076: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 6
Size: 298 Color: 1

Bin 2077: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 15
Size: 394 Color: 2

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 19
Size: 183 Color: 3
Size: 150 Color: 12

Bin 2079: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 16
Size: 374 Color: 9

Bin 2080: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 5
Size: 294 Color: 0

Bin 2081: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 9
Size: 491 Color: 18

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 9
Size: 229 Color: 7
Size: 126 Color: 9

Bin 2083: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 13
Size: 328 Color: 16

Bin 2084: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 3
Size: 389 Color: 0

Bin 2085: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 18
Size: 231 Color: 4

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 4
Size: 245 Color: 17
Size: 147 Color: 5

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 0
Size: 114 Color: 13
Size: 101 Color: 17

Bin 2088: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 19
Size: 482 Color: 10

Bin 2089: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 9
Size: 203 Color: 3

Bin 2090: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 13
Size: 250 Color: 4

Bin 2091: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 17
Size: 283 Color: 6

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 6
Size: 172 Color: 14
Size: 115 Color: 5

Bin 2093: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 6
Size: 326 Color: 4

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 12
Size: 161 Color: 9
Size: 154 Color: 2

Bin 2095: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 10
Size: 410 Color: 15

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 8
Size: 321 Color: 3
Size: 200 Color: 13

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 14
Size: 181 Color: 19
Size: 132 Color: 14

Bin 2098: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 398 Color: 16

Bin 2099: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 15
Size: 286 Color: 12

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 18
Size: 104 Color: 5
Size: 102 Color: 15

Bin 2101: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 17
Size: 431 Color: 15

Bin 2102: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 16
Size: 280 Color: 9

Bin 2103: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 9
Size: 413 Color: 13

Bin 2104: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 17
Size: 248 Color: 4

Bin 2105: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 6
Size: 269 Color: 2

Bin 2106: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 17
Size: 404 Color: 11

Bin 2107: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 2
Size: 259 Color: 4

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 19
Size: 121 Color: 19
Size: 101 Color: 11

Bin 2109: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 14
Size: 297 Color: 18

Bin 2110: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 11
Size: 206 Color: 7

Bin 2111: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 15
Size: 203 Color: 17

Bin 2112: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 257 Color: 14

Bin 2113: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 15
Size: 210 Color: 18

Bin 2114: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 10
Size: 348 Color: 16

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 3
Size: 178 Color: 11
Size: 136 Color: 4

Bin 2116: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 7
Size: 265 Color: 10

Bin 2117: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 6
Size: 376 Color: 5

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 2
Size: 195 Color: 9
Size: 173 Color: 4

Bin 2119: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 17
Size: 445 Color: 14

Bin 2120: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 15
Size: 375 Color: 18

Bin 2121: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 12
Size: 480 Color: 10

Bin 2122: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 5
Size: 477 Color: 1

Bin 2123: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 12
Size: 241 Color: 1

Bin 2124: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 7
Size: 201 Color: 10

Bin 2125: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 19
Size: 465 Color: 1

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 14
Size: 170 Color: 3
Size: 113 Color: 17

Bin 2127: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 8
Size: 369 Color: 18

Bin 2128: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 7
Size: 425 Color: 12

Bin 2129: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 5
Size: 216 Color: 3

Bin 2130: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 1
Size: 483 Color: 5

Bin 2131: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 8
Size: 289 Color: 12

Bin 2132: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 6
Size: 375 Color: 2

Bin 2133: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 18
Size: 448 Color: 12

Bin 2134: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 14
Size: 412 Color: 17

Bin 2135: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 9
Size: 220 Color: 19

Bin 2136: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 14
Size: 444 Color: 11

Bin 2137: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 230 Color: 17

Bin 2138: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 12
Size: 441 Color: 13

Bin 2139: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 13
Size: 378 Color: 12

Bin 2140: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 19
Size: 407 Color: 11

Bin 2141: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 10
Size: 241 Color: 14

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 8
Size: 169 Color: 7
Size: 114 Color: 10

Bin 2143: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 14
Size: 321 Color: 17

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 3
Size: 104 Color: 1
Size: 101 Color: 2

Bin 2145: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 8
Size: 481 Color: 0

Bin 2146: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 7
Size: 415 Color: 19

Bin 2147: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 19
Size: 328 Color: 10

Bin 2148: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 12

Bin 2149: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 13
Size: 418 Color: 2

Bin 2150: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 7
Size: 228 Color: 5

Bin 2151: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 18
Size: 229 Color: 12

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 18
Size: 168 Color: 18
Size: 167 Color: 3

Bin 2153: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 10
Size: 269 Color: 18

Bin 2154: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 18
Size: 337 Color: 10

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 15
Size: 181 Color: 5
Size: 178 Color: 12

Bin 2156: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 8

Bin 2157: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 17
Size: 291 Color: 10

Bin 2158: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 14

Bin 2159: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 9
Size: 361 Color: 17

Bin 2160: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 18
Size: 418 Color: 9

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 13
Size: 158 Color: 5
Size: 130 Color: 11

Bin 2162: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 12
Size: 456 Color: 0

Bin 2163: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 15
Size: 431 Color: 7

Bin 2164: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 15
Size: 476 Color: 12

Bin 2165: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 1
Size: 458 Color: 18

Bin 2166: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 1
Size: 407 Color: 0

Bin 2167: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 14
Size: 246 Color: 9

Bin 2168: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 18
Size: 435 Color: 3

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 4
Size: 148 Color: 17
Size: 119 Color: 15

Bin 2170: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 10
Size: 416 Color: 13

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 8
Size: 147 Color: 4
Size: 121 Color: 12

Bin 2172: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 19
Size: 217 Color: 18

Bin 2173: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 19
Size: 390 Color: 5

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 16
Size: 194 Color: 9
Size: 108 Color: 6

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 17
Size: 103 Color: 14
Size: 102 Color: 0

Bin 2176: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 16
Size: 205 Color: 19

Bin 2177: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 8
Size: 400 Color: 14

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 5
Size: 322 Color: 2
Size: 321 Color: 13

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 13
Size: 231 Color: 9
Size: 115 Color: 16

Bin 2180: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 7
Size: 291 Color: 17

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 2
Size: 323 Color: 3
Size: 144 Color: 7

Bin 2182: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 1
Size: 325 Color: 11

Bin 2183: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 1
Size: 476 Color: 12

Bin 2184: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 14
Size: 480 Color: 5

Bin 2185: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 7
Size: 250 Color: 0

Bin 2186: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 7
Size: 422 Color: 16

Bin 2187: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 5
Size: 444 Color: 13

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 14
Size: 139 Color: 11
Size: 129 Color: 1

Bin 2189: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 15
Size: 313 Color: 2

Bin 2190: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 12
Size: 277 Color: 10

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 2
Size: 157 Color: 15
Size: 134 Color: 9

Bin 2192: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 3
Size: 342 Color: 12

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 12
Size: 390 Color: 3
Size: 182 Color: 1

Bin 2194: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 18
Size: 377 Color: 9

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 19
Size: 375 Color: 1
Size: 148 Color: 7

Bin 2196: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 12
Size: 263 Color: 0

Bin 2197: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 466 Color: 15

Bin 2198: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 18
Size: 363 Color: 6

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 5
Size: 308 Color: 0
Size: 260 Color: 9

Bin 2200: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 371 Color: 14

Bin 2201: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 6
Size: 378 Color: 12

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 6
Size: 321 Color: 5
Size: 146 Color: 13

Bin 2203: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 8
Size: 500 Color: 19

Bin 2204: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 5
Size: 421 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 7
Size: 226 Color: 11
Size: 164 Color: 19

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 11
Size: 178 Color: 16
Size: 124 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 18
Size: 131 Color: 7
Size: 125 Color: 1

Bin 2208: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 439 Color: 1

Bin 2209: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 17

Bin 2210: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 14
Size: 292 Color: 17

Bin 2211: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 11
Size: 391 Color: 3

Bin 2212: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 17
Size: 468 Color: 11

Bin 2213: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 15
Size: 457 Color: 12

Bin 2214: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 3
Size: 341 Color: 4

Bin 2215: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 6
Size: 304 Color: 19

Bin 2216: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 10
Size: 263 Color: 5

Bin 2217: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 371 Color: 19

Bin 2218: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 8
Size: 328 Color: 9

Bin 2219: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 16
Size: 353 Color: 17

Bin 2220: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 8
Size: 473 Color: 4

Bin 2221: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 18
Size: 421 Color: 11

Bin 2222: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 6
Size: 472 Color: 4

Bin 2223: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 16
Size: 463 Color: 6

Bin 2224: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 10
Size: 414 Color: 16

Bin 2225: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 5
Size: 265 Color: 17

Bin 2226: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 4
Size: 489 Color: 13

Bin 2227: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 11
Size: 380 Color: 6

Bin 2228: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 15
Size: 205 Color: 0

Bin 2229: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 18
Size: 271 Color: 9

Bin 2230: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 8
Size: 409 Color: 3

Bin 2231: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 16
Size: 467 Color: 3

Bin 2232: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 8
Size: 262 Color: 13

Bin 2233: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 16
Size: 473 Color: 12

Bin 2234: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 18
Size: 289 Color: 9

Bin 2235: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 18
Size: 449 Color: 12

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 15
Size: 398 Color: 6
Size: 134 Color: 5

Bin 2237: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 16
Size: 462 Color: 19

Bin 2238: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 11
Size: 254 Color: 9

Bin 2239: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 5
Size: 440 Color: 6

Bin 2240: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 17
Size: 343 Color: 6

Bin 2241: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 3
Size: 382 Color: 14

Bin 2242: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 2
Size: 350 Color: 4

Bin 2243: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 18
Size: 249 Color: 9

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 14
Size: 177 Color: 15
Size: 116 Color: 15

Bin 2245: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 16
Size: 337 Color: 13

Bin 2246: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 17
Size: 247 Color: 15

Bin 2247: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 14
Size: 232 Color: 2

Bin 2248: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 16
Size: 229 Color: 12

Bin 2249: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 11
Size: 451 Color: 8

Bin 2250: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 14
Size: 490 Color: 5

Bin 2251: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 0
Size: 341 Color: 4

Bin 2252: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 19
Size: 247 Color: 4

Bin 2253: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 2
Size: 231 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 150 Color: 2
Size: 105 Color: 13

Bin 2255: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 1

Bin 2256: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 6

Bin 2257: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 19
Size: 239 Color: 15

Bin 2258: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 7
Size: 236 Color: 16

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 18
Size: 172 Color: 6
Size: 137 Color: 12

Bin 2260: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 14
Size: 438 Color: 8

Bin 2261: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 6
Size: 230 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 18
Size: 338 Color: 6
Size: 322 Color: 13

Bin 2263: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 3
Size: 348 Color: 4

Bin 2264: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 4
Size: 494 Color: 16

Bin 2265: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 17
Size: 429 Color: 6

Bin 2266: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 18
Size: 298 Color: 11

Bin 2267: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 14
Size: 429 Color: 9

Bin 2268: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 3
Size: 200 Color: 10

Bin 2269: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 2
Size: 255 Color: 8

Bin 2270: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 390 Color: 8

Bin 2271: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 14
Size: 256 Color: 17

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 13
Size: 260 Color: 12
Size: 168 Color: 8

Bin 2273: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 12
Size: 497 Color: 9

Bin 2274: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 11
Size: 268 Color: 16

Bin 2275: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 10
Size: 208 Color: 3

Bin 2276: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 8

Bin 2277: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 14
Size: 375 Color: 7

Bin 2278: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 12
Size: 495 Color: 1

Bin 2279: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 19

Bin 2280: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 7
Size: 295 Color: 4

Bin 2281: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 18
Size: 424 Color: 2

Bin 2282: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 280 Color: 8

Bin 2283: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 11
Size: 258 Color: 7

Bin 2284: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 0
Size: 262 Color: 7

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 17
Size: 184 Color: 5
Size: 175 Color: 2

Bin 2286: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 10
Size: 434 Color: 13

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 2
Size: 190 Color: 18
Size: 183 Color: 19

Bin 2288: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 1
Size: 416 Color: 3

Bin 2289: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 18
Size: 396 Color: 19

Bin 2290: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 464 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 8
Size: 196 Color: 16
Size: 152 Color: 5

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 1
Size: 176 Color: 6
Size: 130 Color: 1

Bin 2293: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 19

Bin 2294: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 11
Size: 356 Color: 15

Bin 2295: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 14
Size: 386 Color: 17

Bin 2296: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 12
Size: 417 Color: 8

Bin 2297: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 3
Size: 471 Color: 17

Bin 2298: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 11
Size: 209 Color: 14

Bin 2299: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 8
Size: 244 Color: 18

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 11
Size: 156 Color: 2
Size: 131 Color: 6

Bin 2301: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 6
Size: 446 Color: 16

Bin 2302: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 1
Size: 316 Color: 13

Bin 2303: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 8
Size: 201 Color: 13

Bin 2304: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 331 Color: 8

Bin 2305: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 16
Size: 441 Color: 7

Bin 2306: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 16
Size: 383 Color: 11

Bin 2307: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 18
Size: 211 Color: 6

Bin 2308: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 7
Size: 368 Color: 19

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 11
Size: 197 Color: 10
Size: 143 Color: 3

Bin 2310: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 384 Color: 10

Bin 2311: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 12
Size: 447 Color: 5

Bin 2312: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 14
Size: 314 Color: 2

Bin 2313: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 11
Size: 390 Color: 7

Bin 2314: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 19
Size: 310 Color: 6

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 0
Size: 245 Color: 3
Size: 189 Color: 5

Bin 2316: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 7
Size: 339 Color: 8

Bin 2317: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 13
Size: 450 Color: 19

Bin 2318: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 15
Size: 386 Color: 10

Bin 2319: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 0
Size: 488 Color: 8

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 13
Size: 180 Color: 12
Size: 180 Color: 7

Bin 2321: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 16
Size: 386 Color: 4

Bin 2322: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 0
Size: 343 Color: 2

Bin 2323: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 8
Size: 367 Color: 1

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 19
Size: 176 Color: 15
Size: 110 Color: 3

Bin 2325: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 11
Size: 247 Color: 12

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 4
Size: 199 Color: 6
Size: 153 Color: 13

Bin 2327: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 5
Size: 311 Color: 6

Bin 2328: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 14
Size: 379 Color: 4

Bin 2329: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 0
Size: 247 Color: 14

Bin 2330: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 9
Size: 401 Color: 3

Bin 2331: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 4
Size: 336 Color: 10

Bin 2332: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 7
Size: 264 Color: 11

Bin 2333: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 18
Size: 403 Color: 15

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 1
Size: 245 Color: 19
Size: 127 Color: 1

Bin 2335: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 3
Size: 419 Color: 17

Bin 2336: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 16
Size: 312 Color: 13

Bin 2337: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 10
Size: 482 Color: 0

Bin 2338: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 1
Size: 232 Color: 12

Bin 2339: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 6
Size: 392 Color: 16

Bin 2340: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 4
Size: 207 Color: 12

Bin 2341: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 11
Size: 343 Color: 5

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 14
Size: 173 Color: 17
Size: 173 Color: 15

Bin 2343: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 410 Color: 16

Bin 2344: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 8
Size: 233 Color: 9

Bin 2345: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 16
Size: 291 Color: 7

Bin 2346: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 17
Size: 351 Color: 2

Bin 2347: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 13

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 273 Color: 17
Size: 260 Color: 4

Bin 2349: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 402 Color: 1

Bin 2350: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 18
Size: 441 Color: 9

Bin 2351: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 272 Color: 14

Bin 2352: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 5
Size: 312 Color: 12

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 537 Color: 14
Size: 351 Color: 6
Size: 113 Color: 6

Bin 2354: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 13
Size: 482 Color: 9

Bin 2355: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 11
Size: 284 Color: 17

Bin 2356: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 6
Size: 326 Color: 11

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 12
Size: 125 Color: 4
Size: 116 Color: 3

Bin 2358: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 19
Size: 220 Color: 4

Bin 2359: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 13
Size: 482 Color: 3

Bin 2360: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 11
Size: 376 Color: 1

Bin 2361: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 17
Size: 204 Color: 18

Bin 2362: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 11
Size: 421 Color: 13

Bin 2363: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 11
Size: 483 Color: 13

Bin 2364: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 15
Size: 262 Color: 11

Bin 2365: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 8
Size: 326 Color: 18

Bin 2366: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 4

Bin 2367: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 16
Size: 300 Color: 6

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 17
Size: 285 Color: 10
Size: 189 Color: 19

Bin 2369: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 11
Size: 424 Color: 13

Bin 2370: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 7
Size: 263 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 0
Size: 157 Color: 13
Size: 136 Color: 7

Bin 2372: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 16
Size: 254 Color: 8

Bin 2373: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 9
Size: 207 Color: 7

Bin 2374: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 9
Size: 458 Color: 5

Bin 2375: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 19
Size: 401 Color: 18

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 10
Size: 192 Color: 13
Size: 186 Color: 14

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 7
Size: 408 Color: 14
Size: 140 Color: 3

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 11
Size: 169 Color: 18
Size: 145 Color: 16

Bin 2379: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 18
Size: 232 Color: 1

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 13
Size: 173 Color: 9
Size: 163 Color: 13

Bin 2381: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 18
Size: 380 Color: 17

Bin 2382: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 3
Size: 315 Color: 1

Bin 2383: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 8
Size: 263 Color: 7

Bin 2384: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 0
Size: 323 Color: 8

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 5
Size: 174 Color: 13
Size: 103 Color: 10

Bin 2386: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 7
Size: 275 Color: 18

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 2
Size: 164 Color: 11
Size: 139 Color: 5

Bin 2388: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 3
Size: 470 Color: 11

Bin 2389: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 7
Size: 456 Color: 15

Bin 2390: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 19
Size: 454 Color: 1

Bin 2391: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 12
Size: 439 Color: 5

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 19
Size: 351 Color: 8
Size: 296 Color: 1

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 0
Size: 144 Color: 9
Size: 103 Color: 16

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 661 Color: 16
Size: 198 Color: 19
Size: 142 Color: 4

Bin 2395: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 19
Size: 292 Color: 15

Bin 2396: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 2

Bin 2397: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 6
Size: 341 Color: 9

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 18
Size: 147 Color: 19
Size: 117 Color: 12

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 339 Color: 6
Size: 193 Color: 1

Bin 2400: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 6
Size: 413 Color: 15

Bin 2401: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 16
Size: 496 Color: 8

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 3
Size: 252 Color: 10
Size: 121 Color: 11

Bin 2403: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 7
Size: 375 Color: 0

Bin 2404: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 3
Size: 249 Color: 16

Bin 2405: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 15
Size: 227 Color: 1

Bin 2406: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 12
Size: 413 Color: 4

Bin 2407: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 3

Bin 2408: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 6
Size: 297 Color: 7

Bin 2409: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 14
Size: 373 Color: 12

Bin 2410: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 1

Bin 2411: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 403 Color: 4

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 9
Size: 106 Color: 15
Size: 104 Color: 5

Bin 2413: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 9
Size: 205 Color: 16

Bin 2414: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 10
Size: 453 Color: 18

Bin 2415: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 19
Size: 453 Color: 4

Bin 2416: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 13
Size: 348 Color: 10

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 2
Size: 184 Color: 8
Size: 133 Color: 7

Bin 2418: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 11
Size: 480 Color: 13

Bin 2419: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 19
Size: 287 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 574 Color: 13
Size: 256 Color: 0
Size: 171 Color: 0

Bin 2421: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 269 Color: 6

Bin 2422: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 11
Size: 305 Color: 9

Bin 2423: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 4

Bin 2424: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 17
Size: 486 Color: 3

Bin 2425: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 18
Size: 366 Color: 17

Bin 2426: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 12
Size: 265 Color: 7

Bin 2427: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 3
Size: 240 Color: 13

Bin 2428: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 14
Size: 420 Color: 15

Bin 2429: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 17
Size: 242 Color: 8

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 592 Color: 19
Size: 271 Color: 11
Size: 138 Color: 19

Bin 2431: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 1
Size: 218 Color: 4

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 12
Size: 196 Color: 8
Size: 184 Color: 17

Bin 2433: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 10
Size: 469 Color: 0

Bin 2434: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 8
Size: 393 Color: 18

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 587 Color: 19
Size: 207 Color: 16
Size: 207 Color: 8

Bin 2436: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 10
Size: 400 Color: 17

Bin 2437: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 8
Size: 204 Color: 10

Bin 2438: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 19
Size: 314 Color: 5

Bin 2439: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 16
Size: 458 Color: 6

Bin 2440: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 7
Size: 259 Color: 10

Bin 2441: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 16
Size: 406 Color: 11

Bin 2442: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 2
Size: 424 Color: 8

Bin 2443: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 9
Size: 351 Color: 1

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 577 Color: 16
Size: 235 Color: 19
Size: 189 Color: 13

Bin 2445: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 14
Size: 422 Color: 3

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 9
Size: 181 Color: 9
Size: 155 Color: 15

Bin 2447: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 468 Color: 6

Bin 2448: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 7
Size: 446 Color: 15

Bin 2449: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 10
Size: 318 Color: 15

Bin 2450: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 10
Size: 447 Color: 6

Bin 2451: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 9
Size: 397 Color: 2

Bin 2452: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 14
Size: 377 Color: 0

Bin 2453: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 19
Size: 231 Color: 5

Bin 2454: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 19
Size: 452 Color: 10

Bin 2455: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 13
Size: 283 Color: 18

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 10
Size: 276 Color: 2
Size: 137 Color: 9

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 18
Size: 105 Color: 4
Size: 100 Color: 18

Bin 2458: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 4
Size: 399 Color: 7

Bin 2459: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 0

Bin 2460: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 10
Size: 278 Color: 7

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 2
Size: 172 Color: 11
Size: 171 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 10
Size: 152 Color: 16
Size: 116 Color: 12

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 2
Size: 199 Color: 17
Size: 116 Color: 15

Bin 2464: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 8
Size: 311 Color: 12

Bin 2465: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 6
Size: 284 Color: 12

Bin 2466: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 2
Size: 311 Color: 8

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 1
Size: 188 Color: 13
Size: 167 Color: 7

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 6
Size: 180 Color: 11
Size: 139 Color: 4

Bin 2469: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 6
Size: 456 Color: 3

Bin 2470: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 13
Size: 377 Color: 6

Bin 2471: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 14
Size: 207 Color: 6

Bin 2472: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 11
Size: 213 Color: 19

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 10
Size: 172 Color: 9
Size: 144 Color: 10

Bin 2474: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 2
Size: 464 Color: 19

Bin 2475: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 9
Size: 227 Color: 2

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 18
Size: 129 Color: 19
Size: 107 Color: 5

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 3
Size: 245 Color: 14
Size: 167 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 2
Size: 147 Color: 15
Size: 140 Color: 2

Bin 2479: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 0
Size: 366 Color: 15

Bin 2480: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 7
Size: 237 Color: 19

Bin 2481: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 19
Size: 223 Color: 15

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 16
Size: 191 Color: 15
Size: 188 Color: 11

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 6
Size: 229 Color: 7
Size: 115 Color: 14

Bin 2484: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 10
Size: 388 Color: 1

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 18
Size: 151 Color: 16
Size: 103 Color: 3

Bin 2486: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 18
Size: 416 Color: 5

Bin 2487: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 7
Size: 450 Color: 4

Bin 2488: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 15
Size: 470 Color: 5

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8
Size: 386 Color: 6
Size: 195 Color: 4

Bin 2490: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 3
Size: 218 Color: 5

Bin 2491: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 11
Size: 282 Color: 19

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 8
Size: 168 Color: 1
Size: 127 Color: 18

Bin 2493: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 12
Size: 272 Color: 1

Bin 2494: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 9
Size: 302 Color: 8

Bin 2495: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 16
Size: 212 Color: 5

Bin 2496: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 6
Size: 327 Color: 19

Bin 2497: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 6
Size: 306 Color: 16

Bin 2498: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 9
Size: 264 Color: 17

Bin 2499: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 14
Size: 427 Color: 15

Bin 2500: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 7
Size: 404 Color: 17

Bin 2501: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 14

Bin 2502: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 15
Size: 362 Color: 9

Bin 2503: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 5
Size: 454 Color: 11

Bin 2504: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 10
Size: 246 Color: 4

Bin 2505: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 18
Size: 316 Color: 13

Bin 2506: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 2
Size: 476 Color: 19

Bin 2507: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 16
Size: 391 Color: 14

Bin 2508: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 8
Size: 228 Color: 4

Bin 2509: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 6
Size: 209 Color: 4

Bin 2510: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 13
Size: 402 Color: 10

Bin 2511: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 19
Size: 255 Color: 5

Bin 2512: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 8
Size: 429 Color: 15

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 340 Color: 0
Size: 170 Color: 18

Bin 2514: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 6
Size: 328 Color: 1

Bin 2515: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 14
Size: 467 Color: 8

Bin 2516: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 5
Size: 213 Color: 15

Bin 2517: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 13
Size: 308 Color: 7

Bin 2518: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 8
Size: 385 Color: 16

Bin 2519: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 12
Size: 228 Color: 13

Bin 2520: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 10
Size: 497 Color: 17

Bin 2521: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 15
Size: 339 Color: 9

Bin 2522: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 10
Size: 396 Color: 8

Bin 2523: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 18
Size: 370 Color: 14

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 13
Size: 174 Color: 9
Size: 113 Color: 1

Bin 2525: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 13
Size: 270 Color: 2

Bin 2526: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 9
Size: 336 Color: 7

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 2
Size: 153 Color: 1
Size: 129 Color: 9

Bin 2528: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 6
Size: 392 Color: 1

Bin 2529: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 6
Size: 233 Color: 13

Bin 2530: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 9
Size: 237 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 10
Size: 276 Color: 13
Size: 191 Color: 12

Bin 2532: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 2
Size: 304 Color: 15

Bin 2533: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 13
Size: 324 Color: 9

Bin 2534: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 17
Size: 471 Color: 10

Bin 2535: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 8
Size: 435 Color: 6

Bin 2536: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 448 Color: 1

Bin 2537: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 16
Size: 357 Color: 5

Bin 2538: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 5

Bin 2539: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 9
Size: 413 Color: 5

Bin 2540: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 12

Bin 2541: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 4
Size: 211 Color: 19

Bin 2542: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 5
Size: 473 Color: 19

Bin 2543: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 11
Size: 436 Color: 12

Bin 2544: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 8
Size: 308 Color: 14

Bin 2545: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 282 Color: 14

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 19
Size: 133 Color: 16
Size: 133 Color: 13

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 2
Size: 109 Color: 16
Size: 105 Color: 3

Bin 2548: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 11
Size: 232 Color: 16

Bin 2549: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 11
Size: 210 Color: 15

Bin 2550: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 4
Size: 258 Color: 11

Bin 2551: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 11
Size: 350 Color: 1

Bin 2552: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 18
Size: 367 Color: 2

Bin 2553: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 18
Size: 282 Color: 11

Bin 2554: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 13
Size: 313 Color: 4

Bin 2555: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 13
Size: 358 Color: 1

Bin 2556: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 9

Bin 2557: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 14
Size: 362 Color: 9

Bin 2558: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 11
Size: 237 Color: 6

Bin 2559: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 14
Size: 275 Color: 10

Bin 2560: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 7

Bin 2561: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 6
Size: 288 Color: 18

Bin 2562: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 18
Size: 459 Color: 14

Bin 2563: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 3
Size: 224 Color: 16

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 12
Size: 178 Color: 2
Size: 113 Color: 15

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 13
Size: 299 Color: 5
Size: 162 Color: 16

Bin 2566: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 19
Size: 201 Color: 16

Bin 2567: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 5
Size: 334 Color: 17

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 15
Size: 445 Color: 15
Size: 108 Color: 8

Bin 2569: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 0

Bin 2570: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 14
Size: 316 Color: 11

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 8
Size: 177 Color: 2
Size: 110 Color: 3

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 17
Size: 299 Color: 9
Size: 114 Color: 15

Bin 2573: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 18
Size: 441 Color: 0

Bin 2574: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 7
Size: 444 Color: 6

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 0
Size: 173 Color: 0
Size: 169 Color: 15

Bin 2576: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 5
Size: 398 Color: 17

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 4
Size: 203 Color: 12
Size: 163 Color: 6

Bin 2578: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 11
Size: 283 Color: 19

Bin 2579: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 16
Size: 294 Color: 17

Bin 2580: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 0
Size: 432 Color: 3

Bin 2581: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 6
Size: 360 Color: 17

Bin 2582: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 12
Size: 277 Color: 19

Bin 2583: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 493 Color: 7

Bin 2584: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 16
Size: 305 Color: 17

Bin 2585: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 8
Size: 275 Color: 0

Bin 2586: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 19
Size: 380 Color: 7

Bin 2587: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 5
Size: 213 Color: 8

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 15
Size: 408 Color: 5
Size: 118 Color: 16

Bin 2589: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 17
Size: 389 Color: 7

Bin 2590: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 8
Size: 219 Color: 2

Bin 2591: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 16
Size: 456 Color: 5

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 14
Size: 348 Color: 2
Size: 126 Color: 12

Bin 2593: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 5
Size: 297 Color: 6

Bin 2594: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 283 Color: 3

Bin 2595: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 14
Size: 461 Color: 7

Bin 2596: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 464 Color: 13

Bin 2597: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 7
Size: 459 Color: 16

Bin 2598: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 0
Size: 209 Color: 4

Bin 2599: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 11
Size: 234 Color: 15

Bin 2600: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 352 Color: 5

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 9
Size: 186 Color: 16
Size: 176 Color: 19

Bin 2602: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 12
Size: 435 Color: 17

Bin 2603: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 14
Size: 429 Color: 0

Bin 2604: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 12
Size: 254 Color: 3

Bin 2605: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 2
Size: 380 Color: 9

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 6
Size: 289 Color: 14
Size: 152 Color: 2

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 8
Size: 358 Color: 0
Size: 139 Color: 7

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 16
Size: 119 Color: 6
Size: 108 Color: 5

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 14
Size: 137 Color: 7
Size: 121 Color: 5

Bin 2610: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 6
Size: 378 Color: 1

Bin 2611: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 8
Size: 239 Color: 15

Bin 2612: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 11
Size: 458 Color: 16

Bin 2613: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 13
Size: 264 Color: 8

Bin 2614: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 16

Bin 2615: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 9
Size: 475 Color: 4

Bin 2616: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 17

Bin 2617: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 15
Size: 476 Color: 3

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 1
Size: 161 Color: 5
Size: 134 Color: 9

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 16
Size: 217 Color: 17
Size: 149 Color: 11

Bin 2620: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 4
Size: 436 Color: 14

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 15
Size: 193 Color: 1
Size: 138 Color: 10

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 390 Color: 1
Size: 143 Color: 13

Bin 2623: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 14
Size: 399 Color: 8

Bin 2624: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 3
Size: 323 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 15
Size: 161 Color: 9
Size: 123 Color: 1

Bin 2626: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 4
Size: 314 Color: 6

Bin 2627: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 9
Size: 433 Color: 0

Bin 2628: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 14
Size: 393 Color: 9

Bin 2629: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 3
Size: 238 Color: 7

Bin 2630: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 18
Size: 306 Color: 14

Bin 2631: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 2
Size: 380 Color: 15

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 6
Size: 338 Color: 19
Size: 245 Color: 7

Bin 2633: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 10
Size: 384 Color: 2

Bin 2634: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 13
Size: 358 Color: 2

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 596 Color: 8
Size: 209 Color: 1
Size: 196 Color: 6

Bin 2636: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 4
Size: 204 Color: 9

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 7
Size: 261 Color: 9
Size: 160 Color: 11

Bin 2638: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 16
Size: 283 Color: 5

Bin 2639: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 7
Size: 254 Color: 13

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 16
Size: 340 Color: 19
Size: 121 Color: 17

Bin 2641: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 8
Size: 363 Color: 15

Bin 2642: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 19
Size: 276 Color: 8

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 12
Size: 401 Color: 1
Size: 124 Color: 19

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 311 Color: 17
Size: 199 Color: 17

Bin 2645: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 1

Bin 2646: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 2
Size: 330 Color: 10

Bin 2647: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 12
Size: 444 Color: 15

Bin 2648: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 19
Size: 374 Color: 16

Bin 2649: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 2
Size: 472 Color: 13

Bin 2650: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 494 Color: 9

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 13
Size: 172 Color: 16
Size: 132 Color: 5

Bin 2652: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 5
Size: 448 Color: 3

Bin 2653: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 13
Size: 412 Color: 0

Bin 2654: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 14
Size: 414 Color: 10

Bin 2655: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 6
Size: 301 Color: 19

Bin 2656: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 303 Color: 5

Bin 2657: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 14

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 4
Size: 442 Color: 16
Size: 104 Color: 8

Bin 2659: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 387 Color: 17

Bin 2660: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 1
Size: 227 Color: 16

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 9
Size: 263 Color: 9
Size: 139 Color: 3

Bin 2662: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 19
Size: 416 Color: 6

Bin 2663: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 5
Size: 425 Color: 4

Bin 2664: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 10
Size: 383 Color: 6

Bin 2665: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 15
Size: 363 Color: 14

Bin 2666: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 15
Size: 448 Color: 7

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 0
Size: 353 Color: 18
Size: 121 Color: 13

Bin 2668: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 4
Size: 466 Color: 2

Bin 2669: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 1
Size: 322 Color: 12

Bin 2670: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 14
Size: 383 Color: 5

Bin 2671: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 2
Size: 232 Color: 3

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 10
Size: 161 Color: 3
Size: 136 Color: 6

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 763 Color: 13
Size: 126 Color: 2
Size: 112 Color: 15

Bin 2674: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 18

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 0
Size: 152 Color: 8
Size: 126 Color: 4

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 2
Size: 129 Color: 15
Size: 129 Color: 1

Bin 2677: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 4
Size: 242 Color: 6

Bin 2678: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 0

Bin 2679: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 1
Size: 354 Color: 15

Bin 2680: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 11
Size: 200 Color: 9

Bin 2681: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 369 Color: 8

Bin 2682: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 13
Size: 438 Color: 9

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 0
Size: 310 Color: 13
Size: 187 Color: 6

Bin 2684: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 6
Size: 368 Color: 5

Bin 2685: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 6
Size: 462 Color: 4

Bin 2686: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 18
Size: 262 Color: 19

Bin 2687: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 8
Size: 448 Color: 3

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 394 Color: 9
Size: 192 Color: 4

Bin 2689: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 474 Color: 12

Bin 2690: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 0
Size: 431 Color: 13

Bin 2691: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 259 Color: 11

Bin 2692: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 402 Color: 11

Bin 2693: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 11
Size: 378 Color: 10

Bin 2694: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 11
Size: 404 Color: 1

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 10
Size: 130 Color: 16
Size: 130 Color: 2

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 376 Color: 18
Size: 172 Color: 5

Bin 2697: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 18
Size: 424 Color: 10

Bin 2698: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 13
Size: 409 Color: 10

Bin 2699: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 9
Size: 449 Color: 0

Bin 2700: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 14
Size: 223 Color: 8

Bin 2701: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 2
Size: 362 Color: 5

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 14
Size: 245 Color: 17
Size: 154 Color: 15

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 12
Size: 130 Color: 18
Size: 110 Color: 16

Bin 2704: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 9
Size: 409 Color: 17

Bin 2705: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 5
Size: 366 Color: 12

Bin 2706: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 18
Size: 369 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 18
Size: 150 Color: 11
Size: 148 Color: 17

Bin 2708: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 11
Size: 266 Color: 13

Bin 2709: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 0
Size: 433 Color: 14

Bin 2710: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 8
Size: 423 Color: 3

Bin 2711: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 9
Size: 284 Color: 16

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 13
Size: 131 Color: 2
Size: 127 Color: 1

Bin 2713: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 4
Size: 453 Color: 0

Bin 2714: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 13
Size: 432 Color: 12

Bin 2715: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 0
Size: 373 Color: 9

Bin 2716: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 7
Size: 329 Color: 15

Bin 2717: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 3
Size: 458 Color: 1

Bin 2718: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 18
Size: 272 Color: 10

Bin 2719: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 4
Size: 322 Color: 19

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 17
Size: 358 Color: 7
Size: 276 Color: 12

Bin 2721: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 9
Size: 274 Color: 18

Bin 2722: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 3
Size: 450 Color: 0

Bin 2723: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 19
Size: 202 Color: 3

Bin 2724: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 13

Bin 2725: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 18
Size: 290 Color: 9

Bin 2726: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 14
Size: 401 Color: 5

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 18
Size: 186 Color: 7
Size: 141 Color: 17

Bin 2728: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 19
Size: 315 Color: 11

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 5
Size: 182 Color: 7
Size: 134 Color: 9

Bin 2730: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 11

Bin 2731: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 15
Size: 376 Color: 11

Bin 2732: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 11
Size: 445 Color: 18

Bin 2733: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 18
Size: 290 Color: 3

Bin 2734: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 6
Size: 275 Color: 10

Bin 2735: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 15
Size: 213 Color: 7

Bin 2736: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 12
Size: 402 Color: 13

Bin 2737: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 19
Size: 322 Color: 9

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 18
Size: 172 Color: 1
Size: 144 Color: 3

Bin 2739: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 17
Size: 267 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 17
Size: 296 Color: 13
Size: 100 Color: 12

Bin 2741: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 13
Size: 480 Color: 5

Bin 2742: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 6
Size: 365 Color: 10

Bin 2743: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 15
Size: 242 Color: 16

Bin 2744: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 14
Size: 270 Color: 13

Bin 2745: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 5
Size: 493 Color: 15

Bin 2746: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 18
Size: 415 Color: 14

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 15
Size: 262 Color: 14
Size: 217 Color: 8

Bin 2748: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 2
Size: 483 Color: 11

Bin 2749: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 2
Size: 251 Color: 11

Bin 2750: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 6
Size: 219 Color: 13

Bin 2751: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 16
Size: 423 Color: 10

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8
Size: 415 Color: 16
Size: 161 Color: 17

Bin 2753: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 476 Color: 5

Bin 2754: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 16
Size: 484 Color: 19

Bin 2755: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 16
Size: 472 Color: 14

Bin 2756: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 8
Size: 425 Color: 7

Bin 2757: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 4
Size: 452 Color: 18

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 10
Size: 121 Color: 0
Size: 101 Color: 14

Bin 2759: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 3
Size: 247 Color: 13

Bin 2760: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 11
Size: 210 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 647 Color: 2
Size: 177 Color: 17
Size: 177 Color: 1

Bin 2762: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 11
Size: 341 Color: 8

Bin 2763: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 14
Size: 375 Color: 16

Bin 2764: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 11
Size: 500 Color: 15

Bin 2765: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 18
Size: 277 Color: 6

Bin 2766: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 7
Size: 308 Color: 13

Bin 2767: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 18
Size: 243 Color: 8

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 17
Size: 333 Color: 2
Size: 128 Color: 17

Bin 2769: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 2
Size: 418 Color: 13

Bin 2770: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 17
Size: 462 Color: 14

Bin 2771: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 3
Size: 295 Color: 8

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 1
Size: 187 Color: 10
Size: 169 Color: 18

Bin 2773: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 12
Size: 495 Color: 19

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 13
Size: 239 Color: 1
Size: 195 Color: 12

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 19
Size: 118 Color: 13
Size: 115 Color: 2

Bin 2776: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 2
Size: 274 Color: 12

Bin 2777: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 5
Size: 411 Color: 17

Bin 2778: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 8
Size: 463 Color: 0

Bin 2779: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 11
Size: 486 Color: 12

Bin 2780: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 7
Size: 337 Color: 16

Bin 2781: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 8
Size: 360 Color: 13

Bin 2782: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 14
Size: 281 Color: 13

Bin 2783: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 10

Bin 2784: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 8
Size: 413 Color: 0

Bin 2785: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 18
Size: 223 Color: 5

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 18
Size: 123 Color: 10
Size: 117 Color: 16

Bin 2787: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 9

Bin 2788: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 10
Size: 413 Color: 4

Bin 2789: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 13
Size: 339 Color: 9

Bin 2790: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 8
Size: 469 Color: 1

Bin 2791: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 2
Size: 449 Color: 7

Bin 2792: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 19
Size: 232 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 574 Color: 0
Size: 250 Color: 0
Size: 177 Color: 16

Bin 2794: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 11
Size: 329 Color: 12

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 17
Size: 334 Color: 3
Size: 118 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 14
Size: 140 Color: 10
Size: 127 Color: 18

Bin 2797: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 9
Size: 297 Color: 2

Bin 2798: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 16
Size: 233 Color: 10

Bin 2799: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 1
Size: 244 Color: 3

Bin 2800: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 17
Size: 433 Color: 15

Bin 2801: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 7
Size: 375 Color: 14

Bin 2802: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 18
Size: 410 Color: 10

Bin 2803: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 7
Size: 398 Color: 18

Bin 2804: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 281 Color: 1

Bin 2805: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 10
Size: 226 Color: 5

Bin 2806: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 16
Size: 490 Color: 8

Bin 2807: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 14
Size: 434 Color: 4

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 19
Size: 238 Color: 2
Size: 188 Color: 7

Bin 2809: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 9
Size: 334 Color: 15

Bin 2810: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 1
Size: 277 Color: 4

Bin 2811: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 5
Size: 230 Color: 10

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 15
Size: 361 Color: 4
Size: 264 Color: 3

Bin 2813: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 16
Size: 435 Color: 8

Bin 2814: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 3
Size: 409 Color: 15

Bin 2815: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 17
Size: 490 Color: 4

Bin 2816: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 0
Size: 379 Color: 9

Bin 2817: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 19
Size: 421 Color: 3

Bin 2818: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 15
Size: 434 Color: 3

Bin 2819: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 5
Size: 417 Color: 2

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 7
Size: 184 Color: 14
Size: 172 Color: 2

Bin 2821: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 14
Size: 362 Color: 4

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 18
Size: 237 Color: 19
Size: 198 Color: 9

Bin 2823: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 2

Bin 2824: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 10

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 9
Size: 360 Color: 15
Size: 158 Color: 16

Bin 2826: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 4
Size: 453 Color: 10

Bin 2827: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 12
Size: 350 Color: 18

Bin 2828: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 4
Size: 324 Color: 11

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9
Size: 403 Color: 12
Size: 156 Color: 18

Bin 2830: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 6
Size: 385 Color: 1

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 10
Size: 250 Color: 4
Size: 162 Color: 11

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 13
Size: 143 Color: 17
Size: 143 Color: 13

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 2
Size: 188 Color: 10
Size: 164 Color: 11

Bin 2834: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 14
Size: 269 Color: 5

Bin 2835: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 19
Size: 475 Color: 13

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 6
Size: 148 Color: 14
Size: 147 Color: 7

Bin 2837: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 3
Size: 395 Color: 0

Bin 2838: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 16
Size: 467 Color: 2

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 2
Size: 216 Color: 11
Size: 176 Color: 7

Bin 2840: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 10
Size: 389 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 13
Size: 332 Color: 12
Size: 171 Color: 19

Bin 2842: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 5
Size: 215 Color: 15

Bin 2843: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 10
Size: 499 Color: 12

Bin 2844: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 288 Color: 12

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 14
Size: 122 Color: 17
Size: 118 Color: 19

Bin 2846: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 18
Size: 353 Color: 13

Bin 2847: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 19
Size: 462 Color: 18

Bin 2848: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 18
Size: 290 Color: 6

Bin 2849: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 16
Size: 200 Color: 14

Bin 2850: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 12
Size: 386 Color: 3

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 16
Size: 191 Color: 14
Size: 189 Color: 1

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 356 Color: 11
Size: 182 Color: 6

Bin 2853: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 3
Size: 222 Color: 10

Bin 2854: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 429 Color: 14

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 11
Size: 242 Color: 15
Size: 199 Color: 8

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 14
Size: 154 Color: 18
Size: 110 Color: 1

Bin 2857: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 10
Size: 491 Color: 13

Bin 2858: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 13
Size: 277 Color: 0

Bin 2859: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 8
Size: 318 Color: 18

Bin 2860: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 15
Size: 207 Color: 4

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 4
Size: 220 Color: 4
Size: 158 Color: 16

Bin 2862: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 13
Size: 474 Color: 12

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 11
Size: 176 Color: 6
Size: 131 Color: 8

Bin 2864: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 278 Color: 19

Bin 2865: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 18
Size: 334 Color: 11

Bin 2866: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 11
Size: 475 Color: 0

Bin 2867: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 7
Size: 470 Color: 19

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 5
Size: 158 Color: 2
Size: 151 Color: 6

Bin 2869: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 6
Size: 388 Color: 1

Bin 2870: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 4
Size: 262 Color: 14

Bin 2871: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 11
Size: 358 Color: 2

Bin 2872: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 221 Color: 3

Bin 2873: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 445 Color: 19

Bin 2874: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 4
Size: 249 Color: 8

Bin 2875: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 2
Size: 210 Color: 0

Bin 2876: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 4
Size: 246 Color: 19

Bin 2877: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 1
Size: 219 Color: 12

Bin 2878: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 5
Size: 261 Color: 16

Bin 2879: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 12
Size: 261 Color: 18

Bin 2880: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 9
Size: 291 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 2
Size: 361 Color: 15
Size: 181 Color: 15

Bin 2882: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 8

Bin 2883: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 4
Size: 423 Color: 5

Bin 2884: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 7
Size: 314 Color: 5

Bin 2885: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 13
Size: 442 Color: 19

Bin 2886: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 15
Size: 270 Color: 0

Bin 2887: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 7
Size: 269 Color: 16

Bin 2888: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 18

Bin 2889: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 3
Size: 482 Color: 6

Bin 2890: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 16
Size: 443 Color: 3

Bin 2891: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 3
Size: 381 Color: 4

Bin 2892: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 10
Size: 255 Color: 8

Bin 2893: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 2
Size: 327 Color: 18

Bin 2894: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 18
Size: 359 Color: 19

Bin 2895: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 17
Size: 482 Color: 2

Bin 2896: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 12
Size: 291 Color: 17

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 277 Color: 5
Size: 271 Color: 13

Bin 2898: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 15
Size: 383 Color: 13

Bin 2899: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 11
Size: 216 Color: 5

Bin 2900: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 13
Size: 240 Color: 15

Bin 2901: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 9
Size: 363 Color: 16

Bin 2902: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 3
Size: 273 Color: 4

Bin 2903: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 8
Size: 208 Color: 19

Bin 2904: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 12
Size: 435 Color: 4

Bin 2905: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 7
Size: 331 Color: 19

Bin 2906: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 2
Size: 269 Color: 10

Bin 2907: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 0
Size: 334 Color: 5

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 5
Size: 408 Color: 0
Size: 161 Color: 4

Bin 2909: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 12
Size: 399 Color: 17

Bin 2910: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 6
Size: 355 Color: 0

Bin 2911: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 14
Size: 400 Color: 16

Bin 2912: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 4
Size: 295 Color: 2

Bin 2913: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 261 Color: 8

Bin 2914: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 18
Size: 487 Color: 12

Bin 2915: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 9
Size: 225 Color: 13

Bin 2916: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 15
Size: 401 Color: 4

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 370 Color: 11
Size: 178 Color: 7

Bin 2918: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 17
Size: 489 Color: 12

Bin 2919: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 3

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 17
Size: 367 Color: 10
Size: 192 Color: 18

Bin 2921: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 14
Size: 260 Color: 15

Bin 2922: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 5
Size: 220 Color: 3

Bin 2923: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 14
Size: 265 Color: 19

Bin 2924: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 10
Size: 460 Color: 7

Bin 2925: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 5
Size: 324 Color: 14

Bin 2926: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 18
Size: 466 Color: 15

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 7
Size: 417 Color: 11
Size: 106 Color: 15

Bin 2928: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 468 Color: 13

Bin 2929: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 13
Size: 355 Color: 19

Bin 2930: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 5

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 10
Size: 422 Color: 12
Size: 123 Color: 9

Bin 2932: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 6
Size: 437 Color: 3

Bin 2933: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 0

Bin 2934: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 16
Size: 346 Color: 5

Bin 2935: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 7

Bin 2936: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 5
Size: 475 Color: 6

Bin 2937: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 0
Size: 333 Color: 3

Bin 2938: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 2
Size: 491 Color: 3

Bin 2939: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 11
Size: 238 Color: 9

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 13
Size: 382 Color: 12
Size: 197 Color: 10

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 347 Color: 10
Size: 294 Color: 1

Bin 2942: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 203 Color: 6

Bin 2943: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 9
Size: 213 Color: 15

Bin 2944: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 8
Size: 330 Color: 5

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 2
Size: 191 Color: 18
Size: 161 Color: 7

Bin 2946: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 17
Size: 408 Color: 16

Bin 2947: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 10
Size: 474 Color: 17

Bin 2948: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 13
Size: 480 Color: 19

Bin 2949: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 5
Size: 271 Color: 4

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 10
Size: 326 Color: 0
Size: 197 Color: 9

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 13
Size: 237 Color: 18
Size: 197 Color: 8

Bin 2952: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 12
Size: 251 Color: 14

Bin 2953: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 17
Size: 341 Color: 2

Bin 2954: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 10
Size: 402 Color: 14

Bin 2955: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 9
Size: 395 Color: 12

Bin 2956: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 14
Size: 429 Color: 1

Bin 2957: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 15
Size: 462 Color: 8

Bin 2958: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 17
Size: 213 Color: 19

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 261 Color: 4
Size: 242 Color: 15

Bin 2960: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 9
Size: 377 Color: 18

Bin 2961: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 9
Size: 351 Color: 0

Bin 2962: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 15
Size: 249 Color: 18

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 19
Size: 149 Color: 14
Size: 148 Color: 16

Bin 2964: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 7
Size: 445 Color: 5

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 13
Size: 177 Color: 16
Size: 116 Color: 16

Bin 2966: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 6
Size: 400 Color: 2

Bin 2967: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 9
Size: 293 Color: 17

Bin 2968: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 18
Size: 259 Color: 2

Bin 2969: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 10
Size: 380 Color: 18

Bin 2970: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 6
Size: 336 Color: 15

Bin 2971: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 11
Size: 287 Color: 16

Bin 2972: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 18
Size: 430 Color: 6

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 11
Size: 132 Color: 7
Size: 108 Color: 11

Bin 2974: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 12
Size: 248 Color: 10

Bin 2975: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 15
Size: 244 Color: 3

Bin 2976: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 19
Size: 397 Color: 0

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 12
Size: 265 Color: 3
Size: 170 Color: 13

Bin 2978: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 19
Size: 267 Color: 7

Bin 2979: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 12
Size: 457 Color: 4

Bin 2980: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 5
Size: 450 Color: 13

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 2
Size: 182 Color: 2
Size: 111 Color: 13

Bin 2982: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 15
Size: 458 Color: 6

Bin 2983: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 18
Size: 324 Color: 19

Bin 2984: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 9
Size: 323 Color: 0

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 12
Size: 182 Color: 3
Size: 100 Color: 16

Bin 2986: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 15
Size: 498 Color: 3

Bin 2987: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 18
Size: 466 Color: 9

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 730 Color: 8
Size: 145 Color: 0
Size: 126 Color: 13

Bin 2989: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 0
Size: 355 Color: 2

Bin 2990: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 10
Size: 246 Color: 11

Bin 2991: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 5
Size: 431 Color: 12

Bin 2992: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 13
Size: 406 Color: 15

Bin 2993: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 17
Size: 260 Color: 6

Bin 2994: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 9
Size: 402 Color: 12

Bin 2995: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 11
Size: 277 Color: 15

Bin 2996: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 15
Size: 222 Color: 5

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 3
Size: 133 Color: 11
Size: 133 Color: 5

Bin 2998: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 16
Size: 219 Color: 18

Bin 2999: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 10
Size: 203 Color: 19

Bin 3000: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 8
Size: 347 Color: 16

Bin 3001: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 7
Size: 377 Color: 3

Bin 3002: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 15
Size: 201 Color: 14

Bin 3003: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 18
Size: 471 Color: 4

Bin 3004: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 8
Size: 430 Color: 13

Bin 3005: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 394 Color: 16

Bin 3006: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 16
Size: 337 Color: 8

Bin 3007: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 15
Size: 270 Color: 8

Bin 3008: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 7

Bin 3009: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 4
Size: 227 Color: 7

Bin 3010: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 10
Size: 255 Color: 18

Bin 3011: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 12
Size: 257 Color: 14

Bin 3012: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 14
Size: 273 Color: 9

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 16
Size: 151 Color: 3
Size: 141 Color: 1

Bin 3014: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 5
Size: 242 Color: 0

Bin 3015: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 16
Size: 260 Color: 13

Bin 3016: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 17
Size: 273 Color: 11

Bin 3017: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 3
Size: 231 Color: 15

Bin 3018: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 16
Size: 407 Color: 2

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 9
Size: 133 Color: 19
Size: 108 Color: 7

Bin 3020: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 6
Size: 241 Color: 4

Bin 3021: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 9
Size: 425 Color: 0

Bin 3022: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 13
Size: 367 Color: 12

Bin 3023: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 14
Size: 271 Color: 8

Bin 3024: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 4
Size: 305 Color: 6

Bin 3025: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 10

Bin 3026: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 8
Size: 263 Color: 14

Bin 3027: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 4
Size: 301 Color: 10

Bin 3028: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 7
Size: 469 Color: 11

Bin 3029: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 18
Size: 385 Color: 3

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 1
Size: 197 Color: 7
Size: 195 Color: 3

Bin 3031: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 3
Size: 202 Color: 19

Bin 3032: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 14

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 2
Size: 295 Color: 17
Size: 179 Color: 8

Bin 3034: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 2
Size: 341 Color: 13

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 14
Size: 126 Color: 5
Size: 101 Color: 13

Bin 3036: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 1
Size: 426 Color: 15

Bin 3037: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 19
Size: 458 Color: 10

Bin 3038: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 7
Size: 359 Color: 3

Bin 3039: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 9
Size: 499 Color: 3

Bin 3040: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 14
Size: 253 Color: 2

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 647 Color: 17
Size: 178 Color: 10
Size: 176 Color: 18

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 2
Size: 419 Color: 7
Size: 159 Color: 3

Bin 3043: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 5
Size: 357 Color: 13

Bin 3044: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 17
Size: 406 Color: 16

Bin 3045: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 5
Size: 499 Color: 4

Bin 3046: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 13
Size: 450 Color: 0

Bin 3047: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 17
Size: 473 Color: 12

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 13
Size: 271 Color: 6
Size: 195 Color: 19

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 416 Color: 15
Size: 122 Color: 7

Bin 3050: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 13
Size: 443 Color: 10

Bin 3051: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 11
Size: 284 Color: 2

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 18
Size: 134 Color: 13
Size: 130 Color: 5

Bin 3053: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 15
Size: 349 Color: 6

Bin 3054: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 8

Bin 3055: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 15
Size: 313 Color: 10

Bin 3056: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 7
Size: 222 Color: 12

Bin 3057: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 9
Size: 313 Color: 8

Bin 3058: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 276 Color: 13

Bin 3059: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 402 Color: 18

Bin 3060: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 13
Size: 305 Color: 16

Bin 3061: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 2
Size: 270 Color: 13

Bin 3062: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 9
Size: 434 Color: 2

Bin 3063: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 7

Bin 3064: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 4
Size: 390 Color: 11

Bin 3065: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 6
Size: 221 Color: 2

Bin 3066: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 14
Size: 289 Color: 12

Bin 3067: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 8
Size: 383 Color: 3

Bin 3068: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 0
Size: 374 Color: 5

Bin 3069: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 7
Size: 271 Color: 10

Bin 3070: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 17
Size: 393 Color: 3

Bin 3071: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 3
Size: 212 Color: 6

Bin 3072: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 9
Size: 242 Color: 8

Bin 3073: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 9
Size: 363 Color: 3

Bin 3074: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 18
Size: 389 Color: 15

Bin 3075: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 15
Size: 490 Color: 4

Bin 3076: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 15
Size: 309 Color: 13

Bin 3077: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 15
Size: 419 Color: 1

Bin 3078: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 1
Size: 459 Color: 16

Bin 3079: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 16
Size: 312 Color: 8

Bin 3080: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 8
Size: 364 Color: 6

Bin 3081: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 13
Size: 345 Color: 4

Bin 3082: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 11
Size: 411 Color: 7

Bin 3083: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 11
Size: 312 Color: 8

Bin 3084: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 7
Size: 256 Color: 3

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 13
Size: 292 Color: 16
Size: 121 Color: 14

Bin 3086: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 14
Size: 285 Color: 3

Bin 3087: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 14
Size: 218 Color: 9

Bin 3088: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 3
Size: 326 Color: 6

Bin 3089: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 8
Size: 314 Color: 15

Bin 3090: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 15
Size: 212 Color: 4

Bin 3091: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 16
Size: 317 Color: 6

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 15
Size: 334 Color: 3
Size: 117 Color: 10

Bin 3093: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 11
Size: 308 Color: 18

Bin 3094: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 7
Size: 321 Color: 0

Bin 3095: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 8
Size: 365 Color: 19

Bin 3096: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 215 Color: 8

Bin 3097: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 14
Size: 306 Color: 6

Bin 3098: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 4
Size: 432 Color: 5

Bin 3099: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 2
Size: 218 Color: 16

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 8
Size: 289 Color: 9
Size: 195 Color: 7

Bin 3101: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 15
Size: 254 Color: 14

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 11
Size: 196 Color: 0
Size: 149 Color: 11

Bin 3103: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 16
Size: 251 Color: 18

Bin 3104: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 1

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 19
Size: 229 Color: 5
Size: 184 Color: 19

Bin 3106: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 12
Size: 386 Color: 4

Bin 3107: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 16
Size: 383 Color: 19

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 19
Size: 167 Color: 19
Size: 160 Color: 13

Bin 3109: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 7
Size: 458 Color: 15

Bin 3110: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 7
Size: 384 Color: 17

Bin 3111: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 6
Size: 320 Color: 10

Bin 3112: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 9

Bin 3113: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 9
Size: 331 Color: 12

Bin 3114: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 371 Color: 18

Bin 3115: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 223 Color: 9

Bin 3116: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 15
Size: 275 Color: 10

Bin 3117: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 5
Size: 253 Color: 10

Bin 3118: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 16
Size: 264 Color: 1

Bin 3119: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 12
Size: 235 Color: 5

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8
Size: 344 Color: 10
Size: 238 Color: 14

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 8
Size: 203 Color: 18
Size: 178 Color: 16

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 4
Size: 422 Color: 19
Size: 156 Color: 14

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 574 Color: 5
Size: 245 Color: 18
Size: 182 Color: 15

Bin 3124: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 9
Size: 260 Color: 8

Bin 3125: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 12
Size: 475 Color: 19

Bin 3126: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 8
Size: 300 Color: 18

Bin 3127: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 13
Size: 451 Color: 10

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 19
Size: 360 Color: 11
Size: 159 Color: 1

Bin 3129: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 5
Size: 236 Color: 14

Bin 3130: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 13
Size: 223 Color: 6

Bin 3131: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 14
Size: 450 Color: 19

Bin 3132: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 18
Size: 354 Color: 10

Bin 3133: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 13
Size: 384 Color: 12

Bin 3134: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 16
Size: 425 Color: 1

Bin 3135: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 220 Color: 16

Bin 3136: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 19
Size: 335 Color: 0

Bin 3137: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 18
Size: 476 Color: 4

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 14
Size: 354 Color: 10
Size: 289 Color: 2

Bin 3139: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 2
Size: 306 Color: 1

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 13
Size: 272 Color: 14
Size: 180 Color: 4

Bin 3141: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 18

Bin 3142: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 13
Size: 375 Color: 0

Bin 3143: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 19
Size: 450 Color: 11

Bin 3144: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 11
Size: 350 Color: 10

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 4
Size: 348 Color: 5
Size: 126 Color: 10

Bin 3146: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 3
Size: 411 Color: 5

Bin 3147: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 9
Size: 269 Color: 14

Bin 3148: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 0
Size: 399 Color: 1

Bin 3149: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 16
Size: 331 Color: 14

Bin 3150: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 19
Size: 371 Color: 7

Bin 3151: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 9
Size: 407 Color: 10

Bin 3152: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 19
Size: 456 Color: 2

Bin 3153: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 2
Size: 473 Color: 13

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 7
Size: 194 Color: 0
Size: 159 Color: 13

Bin 3155: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 18
Size: 215 Color: 8

Bin 3156: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 351 Color: 18

Bin 3157: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 10
Size: 484 Color: 14

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 14
Size: 191 Color: 8
Size: 187 Color: 6

Bin 3159: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 4
Size: 333 Color: 18

Bin 3160: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 12
Size: 396 Color: 14

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 11
Size: 146 Color: 7
Size: 100 Color: 7

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 7
Size: 321 Color: 15
Size: 176 Color: 19

Bin 3163: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 11
Size: 465 Color: 3

Bin 3164: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 10

Bin 3165: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 4
Size: 280 Color: 19

Bin 3166: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 18
Size: 333 Color: 1

Bin 3167: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 9
Size: 277 Color: 17

Bin 3168: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 11
Size: 471 Color: 9

Bin 3169: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 10
Size: 164 Color: 0
Size: 127 Color: 16

Bin 3170: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 2
Size: 480 Color: 9

Bin 3171: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 11
Size: 461 Color: 0

Bin 3172: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 0
Size: 424 Color: 13

Bin 3173: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 19
Size: 290 Color: 5

Bin 3174: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 5
Size: 199 Color: 16
Size: 153 Color: 16

Bin 3175: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 18
Size: 414 Color: 15
Size: 118 Color: 6

Bin 3176: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 4
Size: 301 Color: 11

Bin 3177: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 9
Size: 131 Color: 10
Size: 113 Color: 2

Bin 3178: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 16
Size: 446 Color: 11

Bin 3179: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 4
Size: 200 Color: 18

Bin 3180: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 16
Size: 234 Color: 11

Bin 3181: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 11
Size: 386 Color: 7

Bin 3182: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 14
Size: 453 Color: 11

Bin 3183: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 11
Size: 419 Color: 4
Size: 129 Color: 10

Bin 3184: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 10
Size: 333 Color: 6

Bin 3185: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 351 Color: 8

Bin 3186: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 0
Size: 331 Color: 4

Bin 3187: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 8
Size: 329 Color: 16

Bin 3188: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 13
Size: 306 Color: 19

Bin 3189: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 18
Size: 309 Color: 1

Bin 3190: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 12
Size: 108 Color: 17
Size: 106 Color: 18

Bin 3191: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 6
Size: 373 Color: 6
Size: 130 Color: 12

Bin 3192: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 5
Size: 294 Color: 4

Bin 3193: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 10
Size: 182 Color: 6
Size: 173 Color: 1

Bin 3194: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 403 Color: 1

Bin 3195: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 6
Size: 129 Color: 3
Size: 108 Color: 6

Bin 3196: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 1

Bin 3197: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 12
Size: 259 Color: 15

Bin 3198: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 11
Size: 423 Color: 8

Bin 3199: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 9

Bin 3200: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 7
Size: 385 Color: 12

Bin 3201: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 0
Size: 372 Color: 8

Bin 3202: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 5
Size: 457 Color: 4

Bin 3203: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 14
Size: 348 Color: 10

Bin 3204: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 13
Size: 229 Color: 16

Bin 3205: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 19
Size: 313 Color: 12

Bin 3206: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 11
Size: 333 Color: 6

Bin 3207: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 19
Size: 310 Color: 10

Bin 3208: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 3
Size: 359 Color: 16

Bin 3209: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 12
Size: 481 Color: 8

Bin 3210: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 18
Size: 103 Color: 6
Size: 102 Color: 10

Bin 3211: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 1
Size: 301 Color: 0
Size: 189 Color: 6

Bin 3212: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 11
Size: 460 Color: 9

Bin 3213: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 5
Size: 255 Color: 12

Bin 3214: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 19
Size: 248 Color: 12

Bin 3215: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 5
Size: 414 Color: 9

Bin 3216: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 4
Size: 359 Color: 0

Bin 3217: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 12
Size: 268 Color: 8

Bin 3218: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 9
Size: 208 Color: 2

Bin 3219: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 13
Size: 246 Color: 2

Bin 3220: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 16
Size: 439 Color: 10

Bin 3221: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 9
Size: 334 Color: 5
Size: 133 Color: 0

Bin 3222: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 0
Size: 347 Color: 15

Bin 3223: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 17
Size: 207 Color: 12

Bin 3224: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 15
Size: 258 Color: 13

Bin 3225: 0 of cap free
Amount of items: 3
Items: 
Size: 512 Color: 6
Size: 294 Color: 15
Size: 195 Color: 15

Bin 3226: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 3
Size: 153 Color: 12
Size: 153 Color: 6

Bin 3227: 0 of cap free
Amount of items: 3
Items: 
Size: 505 Color: 19
Size: 323 Color: 19
Size: 173 Color: 3

Bin 3228: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 6
Size: 399 Color: 18

Bin 3229: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 1
Size: 248 Color: 4

Bin 3230: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 12

Bin 3231: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 7
Size: 339 Color: 9

Bin 3232: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 300 Color: 15

Bin 3233: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 13
Size: 482 Color: 18

Bin 3234: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 19
Size: 231 Color: 10

Bin 3235: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 18
Size: 486 Color: 13

Bin 3236: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 1
Size: 263 Color: 19

Bin 3237: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 11
Size: 414 Color: 3

Bin 3238: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 350 Color: 15

Bin 3239: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 6
Size: 135 Color: 0
Size: 123 Color: 10

Bin 3240: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 18
Size: 315 Color: 7

Bin 3241: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 4

Bin 3242: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 9
Size: 282 Color: 16

Bin 3243: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 11
Size: 384 Color: 10
Size: 197 Color: 6

Bin 3244: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 3
Size: 376 Color: 2

Bin 3245: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 6
Size: 377 Color: 14

Bin 3246: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 5
Size: 357 Color: 18
Size: 190 Color: 14

Bin 3247: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 16
Size: 273 Color: 0

Bin 3248: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 1
Size: 446 Color: 8

Bin 3249: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 334 Color: 12
Size: 169 Color: 19

Bin 3250: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 5
Size: 235 Color: 15

Bin 3251: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 10
Size: 398 Color: 6

Bin 3252: 0 of cap free
Amount of items: 3
Items: 
Size: 537 Color: 14
Size: 323 Color: 3
Size: 141 Color: 1

Bin 3253: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 11
Size: 492 Color: 3

Bin 3254: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 14
Size: 317 Color: 7

Bin 3255: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 13
Size: 363 Color: 6

Bin 3256: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 2
Size: 226 Color: 17

Bin 3257: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 487 Color: 10

Bin 3258: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 1
Size: 334 Color: 13
Size: 100 Color: 16

Bin 3259: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 10
Size: 164 Color: 4
Size: 163 Color: 19

Bin 3260: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 8
Size: 311 Color: 1

Bin 3261: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 16
Size: 377 Color: 1

Bin 3262: 0 of cap free
Amount of items: 3
Items: 
Size: 630 Color: 18
Size: 189 Color: 10
Size: 182 Color: 17

Bin 3263: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 17
Size: 374 Color: 4

Bin 3264: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 6
Size: 303 Color: 10

Bin 3265: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 15
Size: 314 Color: 8

Bin 3266: 0 of cap free
Amount of items: 3
Items: 
Size: 549 Color: 15
Size: 276 Color: 5
Size: 176 Color: 2

Bin 3267: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 7
Size: 285 Color: 4

Bin 3268: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 3
Size: 244 Color: 12

Bin 3269: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 3
Size: 350 Color: 9

Bin 3270: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 13
Size: 404 Color: 16

Bin 3271: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 5
Size: 337 Color: 1

Bin 3272: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 8

Bin 3273: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 17
Size: 326 Color: 13

Bin 3274: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 6
Size: 400 Color: 4

Bin 3275: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 3
Size: 273 Color: 9

Bin 3276: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 17
Size: 365 Color: 9

Bin 3277: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 371 Color: 10
Size: 190 Color: 16

Bin 3278: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 19
Size: 368 Color: 14

Bin 3279: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 19
Size: 254 Color: 6

Bin 3280: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 16
Size: 497 Color: 6

Bin 3281: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 18
Size: 318 Color: 9

Bin 3282: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 13
Size: 449 Color: 10

Bin 3283: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 466 Color: 1

Bin 3284: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 1
Size: 165 Color: 18
Size: 115 Color: 5

Bin 3285: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 3
Size: 184 Color: 6
Size: 172 Color: 0

Bin 3286: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 4
Size: 178 Color: 18
Size: 142 Color: 16

Bin 3287: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 16
Size: 143 Color: 8
Size: 124 Color: 16

Bin 3288: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 8
Size: 247 Color: 2

Bin 3289: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 1
Size: 228 Color: 2

Bin 3290: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 17
Size: 175 Color: 19
Size: 124 Color: 18

Bin 3291: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 2
Size: 321 Color: 3
Size: 320 Color: 11

Bin 3292: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 13
Size: 400 Color: 7

Bin 3293: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 13
Size: 330 Color: 15

Bin 3294: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 13
Size: 269 Color: 5

Bin 3295: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 18
Size: 493 Color: 0

Bin 3296: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 12
Size: 350 Color: 16

Bin 3297: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 3
Size: 242 Color: 2

Bin 3298: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 8
Size: 146 Color: 14
Size: 134 Color: 17

Bin 3299: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 14
Size: 373 Color: 15

Bin 3300: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 15
Size: 189 Color: 11
Size: 136 Color: 0

Bin 3301: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 8
Size: 237 Color: 12
Size: 191 Color: 6

Bin 3302: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 10
Size: 334 Color: 8

Bin 3303: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 448 Color: 12

Bin 3304: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 284 Color: 18

Bin 3305: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 13
Size: 424 Color: 4

Bin 3306: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 7
Size: 455 Color: 11

Bin 3307: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 12
Size: 247 Color: 18

Bin 3308: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 7
Size: 280 Color: 5

Bin 3309: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 4
Size: 470 Color: 7

Bin 3310: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 19
Size: 135 Color: 12
Size: 119 Color: 11

Bin 3311: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 12
Size: 273 Color: 15

Bin 3312: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 19
Size: 351 Color: 3
Size: 110 Color: 6

Bin 3313: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 2
Size: 254 Color: 15
Size: 252 Color: 14

Bin 3314: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 6
Size: 123 Color: 5
Size: 118 Color: 15

Bin 3315: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 3
Size: 493 Color: 12

Bin 3316: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 9
Size: 288 Color: 4

Bin 3317: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 9
Size: 258 Color: 4

Bin 3318: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 424 Color: 4

Bin 3319: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 2
Size: 468 Color: 12

Bin 3320: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 10
Size: 385 Color: 8

Bin 3321: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 18
Size: 285 Color: 5
Size: 141 Color: 10

Bin 3322: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 12
Size: 392 Color: 1

Bin 3323: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 17
Size: 281 Color: 19
Size: 261 Color: 7

Bin 3324: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 19
Size: 322 Color: 0

Bin 3325: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 13
Size: 322 Color: 9

Bin 3326: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 14
Size: 293 Color: 13

Bin 3327: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 289 Color: 9

Bin 3328: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 8
Size: 312 Color: 17

Bin 3329: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 16
Size: 406 Color: 10

Bin 3330: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 5
Size: 379 Color: 13

Bin 3331: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 2
Size: 211 Color: 15

Bin 3332: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 14
Size: 484 Color: 19

Bin 3333: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 0
Size: 127 Color: 18
Size: 109 Color: 14

Bin 3334: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 7
Size: 381 Color: 8

Bin 3335: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 7
Size: 283 Color: 18

Bin 3336: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 19
Size: 291 Color: 15

Bin 3337: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 10
Size: 274 Color: 14

Bin 3338: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 15
Size: 303 Color: 4

Bin 3339: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 9

Bin 3340: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 6
Size: 229 Color: 2

Bin 3341: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 14
Size: 432 Color: 3

Bin 3342: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 8
Size: 298 Color: 5

Bin 3343: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 13
Size: 283 Color: 2

Bin 3344: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 6

Bin 3345: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 347 Color: 8
Size: 160 Color: 10

Bin 3346: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 16
Size: 219 Color: 10

Bin 3347: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 5
Size: 438 Color: 18

Bin 3348: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 6
Size: 389 Color: 13

Bin 3349: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 19
Size: 360 Color: 1
Size: 137 Color: 11

Bin 3350: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 17
Size: 395 Color: 6

Bin 3351: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 10
Size: 430 Color: 13

Bin 3352: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 7
Size: 280 Color: 4

Bin 3353: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 10

Bin 3354: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 2
Size: 289 Color: 18

Bin 3355: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 2
Size: 127 Color: 7
Size: 105 Color: 3

Bin 3356: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 15
Size: 252 Color: 6

Bin 3357: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 11
Size: 456 Color: 17

Bin 3358: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 17
Size: 287 Color: 13
Size: 245 Color: 14

Bin 3359: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 8
Size: 365 Color: 13

Bin 3360: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 14
Size: 427 Color: 7

Bin 3361: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 2
Size: 345 Color: 0

Bin 3362: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 3
Size: 169 Color: 9
Size: 148 Color: 17

Bin 3363: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 13
Size: 222 Color: 7

Bin 3364: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 14
Size: 132 Color: 3
Size: 125 Color: 13

Bin 3365: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 3
Size: 302 Color: 2

Bin 3366: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 17
Size: 202 Color: 13

Bin 3367: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 17
Size: 357 Color: 1

Bin 3368: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 16
Size: 259 Color: 9

Bin 3369: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 4
Size: 278 Color: 16

Bin 3370: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 10
Size: 471 Color: 2

Bin 3371: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 16
Size: 114 Color: 3
Size: 101 Color: 5

Bin 3372: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 12
Size: 319 Color: 3

Bin 3373: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 7
Size: 412 Color: 4

Bin 3374: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 2
Size: 318 Color: 17

Bin 3375: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 397 Color: 5
Size: 171 Color: 14

Bin 3376: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 4
Size: 419 Color: 15

Bin 3377: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 4
Size: 298 Color: 5

Bin 3378: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 10
Size: 327 Color: 11

Bin 3379: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 16
Size: 459 Color: 19

Bin 3380: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 16
Size: 271 Color: 14

Bin 3381: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 361 Color: 1
Size: 169 Color: 13

Bin 3382: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 7
Size: 260 Color: 6

Bin 3383: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 18
Size: 340 Color: 14
Size: 144 Color: 15

Bin 3384: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 6
Size: 252 Color: 14

Bin 3385: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 2
Size: 377 Color: 18

Bin 3386: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 13
Size: 204 Color: 10
Size: 108 Color: 14

Bin 3387: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 17
Size: 498 Color: 12

Bin 3388: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 10
Size: 155 Color: 3
Size: 147 Color: 16

Bin 3389: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 18
Size: 306 Color: 11
Size: 106 Color: 0

Bin 3390: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 3
Size: 291 Color: 17

Bin 3391: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 18
Size: 194 Color: 10
Size: 171 Color: 4

Bin 3392: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 9
Size: 193 Color: 2
Size: 157 Color: 18

Bin 3393: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 1
Size: 242 Color: 12
Size: 155 Color: 18

Bin 3394: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 312 Color: 15

Bin 3395: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 437 Color: 15

Bin 3396: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 0
Size: 409 Color: 12

Bin 3397: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 5
Size: 290 Color: 2
Size: 116 Color: 14

Bin 3398: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 5
Size: 245 Color: 2
Size: 117 Color: 6

Bin 3399: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 13
Size: 237 Color: 9

Bin 3400: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 19
Size: 150 Color: 18
Size: 149 Color: 6

Bin 3401: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 12
Size: 238 Color: 2

Bin 3402: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 5

Bin 3403: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 6
Size: 251 Color: 11

Bin 3404: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 5
Size: 267 Color: 0

Bin 3405: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 13
Size: 291 Color: 4

Bin 3406: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 5
Size: 316 Color: 18

Bin 3407: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 13
Size: 264 Color: 8

Bin 3408: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 8
Size: 339 Color: 14

Bin 3409: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 10
Size: 321 Color: 13

Bin 3410: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 7
Size: 475 Color: 1

Bin 3411: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 17
Size: 108 Color: 11
Size: 106 Color: 0

Bin 3412: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 8
Size: 298 Color: 6

Bin 3413: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 6
Size: 416 Color: 9

Bin 3414: 0 of cap free
Amount of items: 4
Items: 
Size: 445 Color: 11
Size: 229 Color: 19
Size: 196 Color: 8
Size: 131 Color: 0

Bin 3415: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 7
Size: 428 Color: 6

Bin 3416: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 9
Size: 249 Color: 11

Bin 3417: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 10
Size: 466 Color: 0

Bin 3418: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 16

Bin 3419: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 18
Size: 335 Color: 13

Bin 3420: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 5
Size: 109 Color: 17
Size: 101 Color: 6

Bin 3421: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 392 Color: 17

Bin 3422: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 14
Size: 371 Color: 10

Bin 3423: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 3
Size: 285 Color: 6
Size: 245 Color: 10

Bin 3424: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 5
Size: 115 Color: 16
Size: 107 Color: 8

Bin 3425: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 6
Size: 431 Color: 17

Bin 3426: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 19
Size: 279 Color: 3

Bin 3427: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 3
Size: 232 Color: 6

Bin 3428: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 17
Size: 343 Color: 18

Bin 3429: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 17
Size: 291 Color: 16

Bin 3430: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 9
Size: 302 Color: 2

Bin 3431: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 16
Size: 383 Color: 10

Bin 3432: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 0
Size: 436 Color: 4

Bin 3433: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 14
Size: 231 Color: 2

Bin 3434: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 13

Bin 3435: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 16
Size: 462 Color: 14

Bin 3436: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 1
Size: 310 Color: 12

Bin 3437: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 1
Size: 275 Color: 0

Bin 3438: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 15
Size: 481 Color: 3

Bin 3439: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 0
Size: 235 Color: 17

Bin 3440: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9
Size: 395 Color: 1
Size: 125 Color: 19

Bin 3441: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 7
Size: 372 Color: 14

Bin 3442: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 13
Size: 270 Color: 10

Bin 3443: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 14
Size: 236 Color: 11

Bin 3444: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 9
Size: 284 Color: 1

Bin 3445: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 15
Size: 416 Color: 0

Bin 3446: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 7
Size: 248 Color: 12

Bin 3447: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 17
Size: 271 Color: 7

Bin 3448: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 15
Size: 118 Color: 15
Size: 109 Color: 4

Bin 3449: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 0
Size: 105 Color: 0
Size: 100 Color: 7

Bin 3450: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 11
Size: 366 Color: 2

Bin 3451: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 15
Size: 444 Color: 8

Bin 3452: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 3
Size: 339 Color: 6

Bin 3453: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 13
Size: 267 Color: 2

Bin 3454: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 338 Color: 0

Bin 3455: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 12
Size: 216 Color: 15
Size: 113 Color: 14

Bin 3456: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 11
Size: 241 Color: 3

Bin 3457: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 8
Size: 448 Color: 10

Bin 3458: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 5
Size: 391 Color: 1
Size: 195 Color: 14

Bin 3459: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 17
Size: 283 Color: 1

Bin 3460: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 13
Size: 500 Color: 1

Bin 3461: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 13
Size: 192 Color: 4
Size: 163 Color: 11

Bin 3462: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 16

Bin 3463: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 7
Size: 257 Color: 16

Bin 3464: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 1
Size: 323 Color: 3

Bin 3465: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 11
Size: 288 Color: 15

Bin 3466: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 5
Size: 338 Color: 2

Bin 3467: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 17
Size: 398 Color: 17
Size: 184 Color: 2

Bin 3468: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 11
Size: 301 Color: 7

Bin 3469: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 17
Size: 137 Color: 10
Size: 102 Color: 13

Bin 3470: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 15
Size: 242 Color: 3
Size: 155 Color: 6

Bin 3471: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 18
Size: 340 Color: 16

Bin 3472: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 2
Size: 436 Color: 12
Size: 127 Color: 0

Bin 3473: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 3
Size: 436 Color: 9

Bin 3474: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 10
Size: 424 Color: 8

Bin 3475: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 0
Size: 310 Color: 9

Bin 3476: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 16
Size: 383 Color: 12

Bin 3477: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 14
Size: 233 Color: 3

Bin 3478: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 15
Size: 493 Color: 8

Bin 3479: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 19
Size: 194 Color: 6
Size: 125 Color: 12

Bin 3480: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 17
Size: 145 Color: 9
Size: 139 Color: 17

Bin 3481: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 19
Size: 220 Color: 12

Bin 3482: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 6
Size: 411 Color: 5

Bin 3483: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 8
Size: 366 Color: 9

Bin 3484: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 16
Size: 433 Color: 17

Bin 3485: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 9
Size: 406 Color: 2

Bin 3486: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 9
Size: 287 Color: 16

Bin 3487: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 4
Size: 250 Color: 6

Bin 3488: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 5
Size: 212 Color: 13

Bin 3489: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 10
Size: 480 Color: 13

Bin 3490: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 10
Size: 440 Color: 17

Bin 3491: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 15
Size: 131 Color: 13
Size: 120 Color: 13

Bin 3492: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 18
Size: 328 Color: 16

Bin 3493: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 6
Size: 383 Color: 17

Bin 3494: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 6
Size: 270 Color: 5

Bin 3495: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 13
Size: 413 Color: 14

Bin 3496: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 19
Size: 443 Color: 1

Bin 3497: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 18
Size: 459 Color: 13

Bin 3498: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 9
Size: 168 Color: 14
Size: 168 Color: 2

Bin 3499: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 15

Bin 3500: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 1
Size: 337 Color: 19
Size: 151 Color: 9

Bin 3501: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 18
Size: 346 Color: 7

Bin 3502: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 18
Size: 183 Color: 0
Size: 176 Color: 10

Bin 3503: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 16
Size: 451 Color: 8

Bin 3504: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 11
Size: 310 Color: 10

Bin 3505: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 5
Size: 335 Color: 13

Bin 3506: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 17
Size: 369 Color: 3

Bin 3507: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 9
Size: 462 Color: 5

Bin 3508: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 3
Size: 369 Color: 1

Bin 3509: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 18
Size: 199 Color: 14
Size: 198 Color: 5

Bin 3510: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 12
Size: 105 Color: 2
Size: 105 Color: 2

Bin 3511: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 14
Size: 273 Color: 15
Size: 178 Color: 9

Bin 3512: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 5
Size: 249 Color: 9

Bin 3513: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 282 Color: 0

Bin 3514: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 3
Size: 270 Color: 12

Bin 3515: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 5
Size: 223 Color: 14

Bin 3516: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 5
Size: 494 Color: 15

Bin 3517: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 11
Size: 196 Color: 16
Size: 121 Color: 12

Bin 3518: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 5
Size: 192 Color: 9
Size: 121 Color: 12

Bin 3519: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 11
Size: 329 Color: 14

Bin 3520: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 17
Size: 374 Color: 19

Bin 3521: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 19
Size: 339 Color: 0
Size: 122 Color: 12

Bin 3522: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 8
Size: 412 Color: 19

Bin 3523: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 5
Size: 429 Color: 4

Bin 3524: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 12

Bin 3525: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 376 Color: 12
Size: 150 Color: 17

Bin 3526: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 417 Color: 11

Bin 3527: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 7
Size: 429 Color: 15

Bin 3528: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 10
Size: 391 Color: 17
Size: 135 Color: 4

Bin 3529: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 11
Size: 129 Color: 0
Size: 126 Color: 19

Bin 3530: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 6
Size: 432 Color: 7

Bin 3531: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 3
Size: 129 Color: 5
Size: 111 Color: 2

Bin 3532: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 7
Size: 317 Color: 9

Bin 3533: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 12
Size: 208 Color: 19

Bin 3534: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 8
Size: 361 Color: 11
Size: 185 Color: 7

Bin 3535: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 7
Size: 292 Color: 11

Bin 3536: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 17
Size: 335 Color: 13

Bin 3537: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 3
Size: 264 Color: 15
Size: 185 Color: 0

Bin 3538: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 382 Color: 19

Bin 3539: 0 of cap free
Amount of items: 3
Items: 
Size: 659 Color: 19
Size: 234 Color: 10
Size: 108 Color: 13

Bin 3540: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 13
Size: 461 Color: 17

Bin 3541: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 0
Size: 351 Color: 13
Size: 114 Color: 12

Bin 3542: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 266 Color: 16

Bin 3543: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 19
Size: 411 Color: 18

Bin 3544: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 10
Size: 441 Color: 0

Bin 3545: 0 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 7
Size: 398 Color: 9

Bin 3546: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 13
Size: 466 Color: 2

Bin 3547: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 6
Size: 467 Color: 17

Bin 3548: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 337 Color: 10

Bin 3549: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 19
Size: 332 Color: 17

Bin 3550: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 9

Bin 3551: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 8
Size: 232 Color: 17

Bin 3552: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 5
Size: 421 Color: 9

Bin 3553: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 6
Size: 292 Color: 11

Bin 3554: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 13
Size: 222 Color: 3

Bin 3555: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 8
Size: 332 Color: 1

Bin 3556: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 4
Size: 217 Color: 16

Bin 3557: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 486 Color: 3

Bin 3558: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 19
Size: 312 Color: 10

Bin 3559: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 8
Size: 439 Color: 16

Bin 3560: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 7

Bin 3561: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 5

Bin 3562: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 387 Color: 6

Bin 3563: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 18
Size: 298 Color: 19
Size: 181 Color: 14

Bin 3564: 0 of cap free
Amount of items: 3
Items: 
Size: 791 Color: 5
Size: 106 Color: 2
Size: 104 Color: 3

Bin 3565: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 1
Size: 421 Color: 4

Bin 3566: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 9
Size: 419 Color: 2

Bin 3567: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 11
Size: 344 Color: 0

Bin 3568: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 7
Size: 294 Color: 15

Bin 3569: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 17
Size: 487 Color: 8

Bin 3570: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 0
Size: 293 Color: 14

Bin 3571: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 17
Size: 276 Color: 5
Size: 137 Color: 0

Bin 3572: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 6
Size: 286 Color: 14

Bin 3573: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 18
Size: 202 Color: 4

Bin 3574: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 16
Size: 494 Color: 19

Bin 3575: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 15
Size: 444 Color: 11

Bin 3576: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 15
Size: 370 Color: 5

Bin 3577: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 7
Size: 419 Color: 12
Size: 140 Color: 8

Bin 3578: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 10
Size: 498 Color: 13

Bin 3579: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 10
Size: 224 Color: 16
Size: 166 Color: 15

Bin 3580: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 16
Size: 235 Color: 0

Bin 3581: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 15
Size: 472 Color: 4

Bin 3582: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 13
Size: 438 Color: 18

Bin 3583: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 13
Size: 299 Color: 4

Bin 3584: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 13
Size: 364 Color: 12

Bin 3585: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 324 Color: 12

Bin 3586: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 1
Size: 196 Color: 11
Size: 154 Color: 17

Bin 3587: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 7
Size: 274 Color: 19

Bin 3588: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 12
Size: 300 Color: 13

Bin 3589: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 7
Size: 435 Color: 11

Bin 3590: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 1
Size: 188 Color: 0
Size: 141 Color: 15

Bin 3591: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 19
Size: 169 Color: 0
Size: 118 Color: 12

Bin 3592: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 19
Size: 464 Color: 17

Bin 3593: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 5
Size: 208 Color: 15
Size: 188 Color: 4

Bin 3594: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 5
Size: 394 Color: 4

Bin 3595: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 5
Size: 252 Color: 3
Size: 191 Color: 7

Bin 3596: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 2
Size: 332 Color: 4
Size: 332 Color: 3

Bin 3597: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 4
Size: 386 Color: 15

Bin 3598: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 16
Size: 425 Color: 0
Size: 117 Color: 7

Bin 3599: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 18
Size: 402 Color: 7

Bin 3600: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 9
Size: 266 Color: 11

Bin 3601: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 18
Size: 473 Color: 11

Bin 3602: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 7
Size: 421 Color: 14

Bin 3603: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 14
Size: 315 Color: 11

Bin 3604: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 9
Size: 338 Color: 17

Bin 3605: 0 of cap free
Amount of items: 3
Items: 
Size: 543 Color: 11
Size: 250 Color: 3
Size: 208 Color: 15

Bin 3606: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 282 Color: 16

Bin 3607: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 18
Size: 210 Color: 13

Bin 3608: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 13
Size: 203 Color: 7

Bin 3609: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 13
Size: 200 Color: 7

Bin 3610: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 10
Size: 243 Color: 9

Bin 3611: 0 of cap free
Amount of items: 3
Items: 
Size: 540 Color: 12
Size: 361 Color: 14
Size: 100 Color: 17

Bin 3612: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 14
Size: 204 Color: 6

Bin 3613: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 19
Size: 232 Color: 11

Bin 3614: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 19
Size: 404 Color: 10

Bin 3615: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 7
Size: 124 Color: 15
Size: 116 Color: 16

Bin 3616: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 19
Size: 293 Color: 9

Bin 3617: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 5
Size: 263 Color: 4

Bin 3618: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 5
Size: 364 Color: 3

Bin 3619: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 15
Size: 389 Color: 8

Bin 3620: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 1
Size: 265 Color: 11

Bin 3621: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 17
Size: 494 Color: 9

Bin 3622: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 12
Size: 396 Color: 17

Bin 3623: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 13
Size: 391 Color: 3

Bin 3624: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 9
Size: 477 Color: 10

Bin 3625: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 2
Size: 394 Color: 14

Bin 3626: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 6
Size: 310 Color: 15

Bin 3627: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 18
Size: 365 Color: 9

Bin 3628: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 17
Size: 367 Color: 7

Bin 3629: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 16
Size: 224 Color: 18

Bin 3630: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 19
Size: 285 Color: 3

Bin 3631: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 5
Size: 250 Color: 10

Bin 3632: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 5
Size: 421 Color: 6

Bin 3633: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 1
Size: 149 Color: 5
Size: 107 Color: 1

Bin 3634: 0 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 13
Size: 407 Color: 17

Bin 3635: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 10
Size: 443 Color: 3

Bin 3636: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 6
Size: 340 Color: 18

Bin 3637: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 2
Size: 228 Color: 14

Bin 3638: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 18
Size: 410 Color: 8

Bin 3639: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 4
Size: 266 Color: 1

Bin 3640: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 12
Size: 350 Color: 4

Bin 3641: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 7
Size: 263 Color: 17

Bin 3642: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 9
Size: 430 Color: 8

Bin 3643: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 6
Size: 422 Color: 16
Size: 156 Color: 4

Bin 3644: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 19
Size: 316 Color: 17

Bin 3645: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 10
Size: 139 Color: 6
Size: 101 Color: 13

Bin 3646: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 15
Size: 425 Color: 14

Bin 3647: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 11
Size: 235 Color: 3

Bin 3648: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 14
Size: 422 Color: 19

Bin 3649: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 10
Size: 434 Color: 13

Bin 3650: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 13
Size: 349 Color: 6

Bin 3651: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 11
Size: 160 Color: 14
Size: 153 Color: 13

Bin 3652: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 0
Size: 296 Color: 12

Bin 3653: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 7
Size: 307 Color: 16

Bin 3654: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 19
Size: 426 Color: 10

Bin 3655: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 7
Size: 229 Color: 1

Bin 3656: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 7
Size: 274 Color: 3

Bin 3657: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 15
Size: 229 Color: 9

Bin 3658: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 17
Size: 500 Color: 15

Bin 3659: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 9
Size: 454 Color: 2

Bin 3660: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 9
Size: 361 Color: 10

Bin 3661: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 19
Size: 324 Color: 16

Bin 3662: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 410 Color: 1

Bin 3663: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 14
Size: 148 Color: 18
Size: 118 Color: 4

Bin 3664: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 13
Size: 368 Color: 19

Bin 3665: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 1
Size: 242 Color: 14

Bin 3666: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 18
Size: 328 Color: 13

Bin 3667: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 9
Size: 208 Color: 17

Bin 3668: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 12
Size: 195 Color: 5
Size: 102 Color: 13

Bin 3669: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 9
Size: 281 Color: 5

Bin 3670: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 19
Size: 173 Color: 17
Size: 173 Color: 16

Bin 3671: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 19
Size: 127 Color: 10
Size: 101 Color: 0

Bin 3672: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 2
Size: 325 Color: 12

Bin 3673: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 15
Size: 153 Color: 5
Size: 152 Color: 14

Bin 3674: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 10
Size: 272 Color: 6

Bin 3675: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 14
Size: 242 Color: 18
Size: 137 Color: 14

Bin 3676: 0 of cap free
Amount of items: 3
Items: 
Size: 536 Color: 13
Size: 324 Color: 10
Size: 141 Color: 19

Bin 3677: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 4
Size: 339 Color: 2
Size: 323 Color: 3

Bin 3678: 0 of cap free
Amount of items: 3
Items: 
Size: 760 Color: 4
Size: 134 Color: 16
Size: 107 Color: 9

Bin 3679: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 11

Bin 3680: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 2
Size: 339 Color: 5

Bin 3681: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 18
Size: 198 Color: 18
Size: 184 Color: 11

Bin 3682: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 17
Size: 470 Color: 4

Bin 3683: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 1
Size: 409 Color: 19

Bin 3684: 0 of cap free
Amount of items: 3
Items: 
Size: 527 Color: 2
Size: 282 Color: 17
Size: 192 Color: 16

Bin 3685: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 3
Size: 307 Color: 8

Bin 3686: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 10
Size: 337 Color: 14

Bin 3687: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 19
Size: 386 Color: 9

Bin 3688: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 12
Size: 130 Color: 8
Size: 106 Color: 19

Bin 3689: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 16
Size: 318 Color: 5

Bin 3690: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 7
Size: 445 Color: 1

Bin 3691: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 13
Size: 410 Color: 3

Bin 3692: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 6
Size: 135 Color: 16
Size: 111 Color: 4

Bin 3693: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 228 Color: 8

Bin 3694: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 13
Size: 435 Color: 9

Bin 3695: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 14
Size: 146 Color: 13
Size: 145 Color: 13

Bin 3696: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 12
Size: 189 Color: 16
Size: 180 Color: 4

Bin 3697: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 3
Size: 140 Color: 6
Size: 134 Color: 8

Bin 3698: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 7
Size: 347 Color: 14
Size: 261 Color: 4

Bin 3699: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 6
Size: 261 Color: 7

Bin 3700: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 499 Color: 1

Bin 3701: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 11
Size: 395 Color: 13

Bin 3702: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 11
Size: 113 Color: 4
Size: 101 Color: 10

Bin 3703: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 10
Size: 280 Color: 9

Bin 3704: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 5
Size: 335 Color: 9

Bin 3705: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 17
Size: 302 Color: 13

Bin 3706: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 8
Size: 279 Color: 2

Bin 3707: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 5
Size: 371 Color: 1

Bin 3708: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 16
Size: 374 Color: 15

Bin 3709: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 16
Size: 227 Color: 2

Bin 3710: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 8
Size: 428 Color: 13

Bin 3711: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 11
Size: 352 Color: 5

Bin 3712: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 12
Size: 468 Color: 19

Bin 3713: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 16
Size: 230 Color: 17

Bin 3714: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 5
Size: 134 Color: 19
Size: 134 Color: 15

Bin 3715: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 303 Color: 7

Bin 3716: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 16
Size: 315 Color: 13

Bin 3717: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 2
Size: 351 Color: 3

Bin 3718: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 8
Size: 342 Color: 14

Bin 3719: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 7
Size: 491 Color: 13

Bin 3720: 0 of cap free
Amount of items: 3
Items: 
Size: 690 Color: 8
Size: 184 Color: 10
Size: 127 Color: 10

Bin 3721: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 19
Size: 380 Color: 11

Bin 3722: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 323 Color: 9
Size: 245 Color: 2

Bin 3723: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 291 Color: 4

Bin 3724: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 18
Size: 441 Color: 13

Bin 3725: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 10
Size: 390 Color: 19

Bin 3726: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 279 Color: 12

Bin 3727: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 5
Size: 318 Color: 12

Bin 3728: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 17
Size: 261 Color: 19
Size: 119 Color: 0

Bin 3729: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 12
Size: 469 Color: 1

Bin 3730: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 11
Size: 354 Color: 19

Bin 3731: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 378 Color: 14

Bin 3732: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 15
Size: 371 Color: 0

Bin 3733: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 6
Size: 436 Color: 14

Bin 3734: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 4
Size: 163 Color: 19
Size: 152 Color: 0

Bin 3735: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 19
Size: 283 Color: 7

Bin 3736: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 10
Size: 416 Color: 13

Bin 3737: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 0
Size: 279 Color: 6

Bin 3738: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 11
Size: 226 Color: 9

Bin 3739: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 13
Size: 445 Color: 8

Bin 3740: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 5
Size: 190 Color: 13
Size: 190 Color: 10

Bin 3741: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 3
Size: 490 Color: 12

Bin 3742: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 18
Size: 281 Color: 2

Bin 3743: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 0
Size: 455 Color: 10

Bin 3744: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 15
Size: 452 Color: 9

Bin 3745: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 6
Size: 257 Color: 4

Bin 3746: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 4
Size: 344 Color: 5

Bin 3747: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 8
Size: 272 Color: 3

Bin 3748: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 16
Size: 367 Color: 5

Bin 3749: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 5
Size: 224 Color: 0

Bin 3750: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 19
Size: 363 Color: 11

Bin 3751: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 0
Size: 176 Color: 0
Size: 139 Color: 15

Bin 3752: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 18
Size: 338 Color: 10

Bin 3753: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 19
Size: 413 Color: 18

Bin 3754: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 339 Color: 15

Bin 3755: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 10
Size: 223 Color: 14

Bin 3756: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 19
Size: 333 Color: 16

Bin 3757: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 0
Size: 132 Color: 6
Size: 123 Color: 11

Bin 3758: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 16

Bin 3759: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 4
Size: 461 Color: 15

Bin 3760: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 15
Size: 201 Color: 10

Bin 3761: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 16
Size: 373 Color: 18

Bin 3762: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 12
Size: 321 Color: 13
Size: 163 Color: 17

Bin 3763: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 16
Size: 232 Color: 2
Size: 181 Color: 2

Bin 3764: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 7
Size: 405 Color: 10

Bin 3765: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 1
Size: 490 Color: 0

Bin 3766: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 12
Size: 424 Color: 16

Bin 3767: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 1
Size: 171 Color: 17
Size: 136 Color: 3

Bin 3768: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 11
Size: 471 Color: 4

Bin 3769: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 12
Size: 295 Color: 14

Bin 3770: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 10
Size: 468 Color: 7

Bin 3771: 0 of cap free
Amount of items: 3
Items: 
Size: 623 Color: 6
Size: 203 Color: 4
Size: 175 Color: 12

Bin 3772: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 11
Size: 324 Color: 7

Bin 3773: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 12
Size: 304 Color: 1

Bin 3774: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 13
Size: 421 Color: 18

Bin 3775: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 4
Size: 160 Color: 9
Size: 159 Color: 16

Bin 3776: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 8
Size: 326 Color: 11

Bin 3777: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 5
Size: 433 Color: 8

Bin 3778: 0 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 5
Size: 187 Color: 15
Size: 187 Color: 14

Bin 3779: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 17
Size: 298 Color: 8

Bin 3780: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 19

Bin 3781: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 19
Size: 204 Color: 19
Size: 176 Color: 11

Bin 3782: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 14
Size: 458 Color: 6

Bin 3783: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 15

Bin 3784: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 19

Bin 3785: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 2
Size: 373 Color: 4

Bin 3786: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 6
Size: 411 Color: 15

Bin 3787: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 10
Size: 360 Color: 11

Bin 3788: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 18
Size: 348 Color: 1

Bin 3789: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 19
Size: 377 Color: 11

Bin 3790: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 1
Size: 348 Color: 4

Bin 3791: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 2
Size: 173 Color: 10
Size: 115 Color: 7

Bin 3792: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 14
Size: 224 Color: 10

Bin 3793: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 12
Size: 348 Color: 14

Bin 3794: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 10
Size: 479 Color: 16

Bin 3795: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 17
Size: 219 Color: 2

Bin 3796: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 15
Size: 296 Color: 19

Bin 3797: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 418 Color: 15
Size: 114 Color: 6

Bin 3798: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 8
Size: 281 Color: 13

Bin 3799: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 13
Size: 302 Color: 15

Bin 3800: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 203 Color: 4

Bin 3801: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 4
Size: 357 Color: 3

Bin 3802: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 5
Size: 492 Color: 11

Bin 3803: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 19
Size: 145 Color: 14
Size: 145 Color: 8

Bin 3804: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 10
Size: 441 Color: 12

Bin 3805: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 19
Size: 356 Color: 4

Bin 3806: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 5
Size: 283 Color: 3

Bin 3807: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 16
Size: 233 Color: 18

Bin 3808: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 6
Size: 459 Color: 19

Bin 3809: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 9
Size: 404 Color: 6

Bin 3810: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 9
Size: 484 Color: 12

Bin 3811: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 13
Size: 496 Color: 15

Bin 3812: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 2
Size: 193 Color: 14
Size: 132 Color: 19

Bin 3813: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 428 Color: 18

Bin 3814: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 387 Color: 18

Bin 3815: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 14
Size: 144 Color: 19
Size: 135 Color: 17

Bin 3816: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 1
Size: 261 Color: 19

Bin 3817: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 5
Size: 352 Color: 1

Bin 3818: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 7
Size: 284 Color: 0

Bin 3819: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 10
Size: 235 Color: 18
Size: 132 Color: 6

Bin 3820: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 17
Size: 470 Color: 14

Bin 3821: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 12
Size: 188 Color: 0
Size: 142 Color: 13

Bin 3822: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 3
Size: 239 Color: 12

Bin 3823: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 15
Size: 405 Color: 3

Bin 3824: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 11
Size: 483 Color: 13

Bin 3825: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 14
Size: 491 Color: 18

Bin 3826: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 14
Size: 329 Color: 0

Bin 3827: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 442 Color: 13

Bin 3828: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 6
Size: 292 Color: 0

Bin 3829: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 17
Size: 409 Color: 0

Bin 3830: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 8
Size: 366 Color: 3

Bin 3831: 0 of cap free
Amount of items: 3
Items: 
Size: 580 Color: 10
Size: 263 Color: 19
Size: 158 Color: 18

Bin 3832: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 15
Size: 473 Color: 5

Bin 3833: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 7
Size: 410 Color: 10

Bin 3834: 0 of cap free
Amount of items: 3
Items: 
Size: 502 Color: 15
Size: 394 Color: 5
Size: 105 Color: 16

Bin 3835: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 14
Size: 290 Color: 0

Bin 3836: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 2
Size: 185 Color: 3
Size: 184 Color: 2

Bin 3837: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 10
Size: 402 Color: 12

Bin 3838: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 19
Size: 270 Color: 7

Bin 3839: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 15
Size: 228 Color: 13

Bin 3840: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 14
Size: 326 Color: 10

Bin 3841: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 10
Size: 205 Color: 11

Bin 3842: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 2
Size: 253 Color: 7

Bin 3843: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 431 Color: 6

Bin 3844: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 9
Size: 330 Color: 16

Bin 3845: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 2
Size: 275 Color: 19

Bin 3846: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 16
Size: 385 Color: 13

Bin 3847: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 18
Size: 411 Color: 4

Bin 3848: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 7
Size: 178 Color: 12
Size: 110 Color: 17

Bin 3849: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 12
Size: 416 Color: 16

Bin 3850: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 1
Size: 344 Color: 12

Bin 3851: 0 of cap free
Amount of items: 3
Items: 
Size: 648 Color: 9
Size: 190 Color: 4
Size: 163 Color: 3

Bin 3852: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 3
Size: 392 Color: 6

Bin 3853: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 9
Size: 342 Color: 12

Bin 3854: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 2
Size: 253 Color: 3

Bin 3855: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 2
Size: 198 Color: 6
Size: 164 Color: 9

Bin 3856: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 15
Size: 192 Color: 8
Size: 143 Color: 1

Bin 3857: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 8
Size: 269 Color: 15

Bin 3858: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 0
Size: 218 Color: 2

Bin 3859: 0 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 7
Size: 338 Color: 14

Bin 3860: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 4
Size: 481 Color: 9

Bin 3861: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 16
Size: 279 Color: 11

Bin 3862: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 11
Size: 170 Color: 11
Size: 121 Color: 5

Bin 3863: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 16
Size: 446 Color: 5

Bin 3864: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 9
Size: 295 Color: 11

Bin 3865: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 19
Size: 448 Color: 4

Bin 3866: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 16
Size: 490 Color: 13

Bin 3867: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 12
Size: 234 Color: 8

Bin 3868: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 12
Size: 279 Color: 8

Bin 3869: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 13
Size: 204 Color: 4

Bin 3870: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 17
Size: 159 Color: 4
Size: 156 Color: 19

Bin 3871: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 7
Size: 282 Color: 17

Bin 3872: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 366 Color: 1

Bin 3873: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 336 Color: 16

Bin 3874: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 1
Size: 232 Color: 9

Bin 3875: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 3
Size: 351 Color: 8

Bin 3876: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 17
Size: 325 Color: 7
Size: 208 Color: 0

Bin 3877: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 17
Size: 218 Color: 19

Bin 3878: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 9
Size: 150 Color: 13
Size: 143 Color: 11

Bin 3879: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 5
Size: 309 Color: 6

Bin 3880: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 5
Size: 441 Color: 16

Bin 3881: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 4
Size: 286 Color: 19

Bin 3882: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 3
Size: 262 Color: 1

Bin 3883: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 16
Size: 476 Color: 2

Bin 3884: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 2
Size: 165 Color: 9
Size: 128 Color: 7

Bin 3885: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 10
Size: 365 Color: 17

Bin 3886: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 10
Size: 268 Color: 2

Bin 3887: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 16
Size: 361 Color: 2

Bin 3888: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 9
Size: 251 Color: 15

Bin 3889: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 16
Size: 382 Color: 14

Bin 3890: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 15
Size: 319 Color: 1

Bin 3891: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 3
Size: 199 Color: 17
Size: 136 Color: 14

Bin 3892: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 6
Size: 489 Color: 15

Bin 3893: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 2
Size: 421 Color: 1

Bin 3894: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 8
Size: 317 Color: 1

Bin 3895: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 17
Size: 295 Color: 12

Bin 3896: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 16
Size: 128 Color: 9
Size: 120 Color: 2

Bin 3897: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 6
Size: 323 Color: 17
Size: 174 Color: 11

Bin 3898: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 9
Size: 221 Color: 16

Bin 3899: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 8
Size: 364 Color: 4

Bin 3900: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 19
Size: 147 Color: 6
Size: 137 Color: 2

Bin 3901: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 16
Size: 323 Color: 5
Size: 174 Color: 9

Bin 3902: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 13
Size: 447 Color: 11

Bin 3903: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 12
Size: 450 Color: 16

Bin 3904: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 6
Size: 367 Color: 18

Bin 3905: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 18
Size: 198 Color: 15
Size: 158 Color: 17

Bin 3906: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 3
Size: 260 Color: 16
Size: 144 Color: 18

Bin 3907: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 10
Size: 344 Color: 6

Bin 3908: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 14
Size: 264 Color: 19

Bin 3909: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 13
Size: 185 Color: 3
Size: 185 Color: 1

Bin 3910: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 10
Size: 396 Color: 0

Bin 3911: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 15
Size: 354 Color: 18

Bin 3912: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 8
Size: 264 Color: 4
Size: 140 Color: 13

Bin 3913: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 13
Size: 320 Color: 19

Bin 3914: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 14
Size: 308 Color: 10

Bin 3915: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 6

Bin 3916: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 11
Size: 479 Color: 4

Bin 3917: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 397 Color: 17

Bin 3918: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 9
Size: 356 Color: 6

Bin 3919: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 3
Size: 489 Color: 6

Bin 3920: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 7
Size: 444 Color: 0

Bin 3921: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 7
Size: 296 Color: 16

Bin 3922: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 11
Size: 193 Color: 3
Size: 124 Color: 2

Bin 3923: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 5
Size: 278 Color: 13

Bin 3924: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 11
Size: 276 Color: 10

Bin 3925: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 18
Size: 107 Color: 12
Size: 107 Color: 3

Bin 3926: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 10
Size: 447 Color: 18

Bin 3927: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 8
Size: 499 Color: 1

Bin 3928: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 18
Size: 431 Color: 9

Bin 3929: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 7
Size: 400 Color: 4

Bin 3930: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 16
Size: 318 Color: 3

Bin 3931: 0 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 15
Size: 244 Color: 4

Bin 3932: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 0

Bin 3933: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 18
Size: 206 Color: 3

Bin 3934: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 4
Size: 473 Color: 5

Bin 3935: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 8
Size: 172 Color: 9
Size: 136 Color: 4

Bin 3936: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 17
Size: 204 Color: 14

Bin 3937: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 18
Size: 463 Color: 6

Bin 3938: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 18
Size: 419 Color: 4

Bin 3939: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 2
Size: 204 Color: 1

Bin 3940: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 7
Size: 231 Color: 18

Bin 3941: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 7
Size: 402 Color: 14

Bin 3942: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 7
Size: 204 Color: 13

Bin 3943: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 3
Size: 145 Color: 0
Size: 108 Color: 1

Bin 3944: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 9
Size: 366 Color: 4

Bin 3945: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 4
Size: 487 Color: 16

Bin 3946: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 7
Size: 333 Color: 11

Bin 3947: 0 of cap free
Amount of items: 3
Items: 
Size: 517 Color: 8
Size: 310 Color: 14
Size: 174 Color: 1

Bin 3948: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 11
Size: 376 Color: 4

Bin 3949: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 5
Size: 346 Color: 16

Bin 3950: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 18
Size: 227 Color: 9

Bin 3951: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 384 Color: 15

Bin 3952: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 8
Size: 327 Color: 11

Bin 3953: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 6
Size: 325 Color: 14

Bin 3954: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 8
Size: 202 Color: 0

Bin 3955: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 0
Size: 217 Color: 17

Bin 3956: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 12
Size: 139 Color: 8
Size: 109 Color: 1

Bin 3957: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 13
Size: 363 Color: 17

Bin 3958: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 13
Size: 369 Color: 3

Bin 3959: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 15
Size: 313 Color: 0

Bin 3960: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 15
Size: 316 Color: 17

Bin 3961: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 6
Size: 494 Color: 11

Bin 3962: 0 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 3
Size: 148 Color: 2
Size: 139 Color: 15

Bin 3963: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 3
Size: 479 Color: 18

Bin 3964: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 7
Size: 305 Color: 0

Bin 3965: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 1
Size: 287 Color: 15

Bin 3966: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 10
Size: 328 Color: 18

Bin 3967: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 6
Size: 348 Color: 7

Bin 3968: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 382 Color: 6

Bin 3969: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 8
Size: 345 Color: 12

Bin 3970: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 4
Size: 414 Color: 16

Bin 3971: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 7
Size: 269 Color: 9

Bin 3972: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 12
Size: 225 Color: 8

Bin 3973: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 7
Size: 316 Color: 19

Bin 3974: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 18
Size: 269 Color: 11

Bin 3975: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 16
Size: 368 Color: 4

Bin 3976: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 8
Size: 315 Color: 1

Bin 3977: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 1
Size: 149 Color: 7
Size: 142 Color: 16

Bin 3978: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 18
Size: 217 Color: 4

Bin 3979: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 17

Bin 3980: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 18
Size: 225 Color: 7

Bin 3981: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 19
Size: 268 Color: 10

Bin 3982: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 2
Size: 371 Color: 19

Bin 3983: 0 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 5
Size: 115 Color: 17
Size: 103 Color: 13

Bin 3984: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 6
Size: 271 Color: 1
Size: 160 Color: 16

Bin 3985: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 6
Size: 389 Color: 0

Bin 3986: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 5
Size: 219 Color: 7

Bin 3987: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 13
Size: 171 Color: 11
Size: 166 Color: 15

Bin 3988: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 14
Size: 396 Color: 9

Bin 3989: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 9
Size: 374 Color: 5

Bin 3990: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 18

Bin 3991: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 18
Size: 312 Color: 3
Size: 127 Color: 9

Bin 3992: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 209 Color: 18

Bin 3993: 0 of cap free
Amount of items: 3
Items: 
Size: 789 Color: 10
Size: 107 Color: 17
Size: 105 Color: 10

Bin 3994: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 9
Size: 255 Color: 15

Bin 3995: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 2
Size: 492 Color: 16

Bin 3996: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 3
Size: 230 Color: 7

Bin 3997: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 6
Size: 169 Color: 15
Size: 147 Color: 0

Bin 3998: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 2
Size: 355 Color: 19

Bin 3999: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 6
Size: 248 Color: 8

Bin 4000: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 13
Size: 313 Color: 19

Bin 4001: 0 of cap free
Amount of items: 3
Items: 
Size: 605 Color: 3
Size: 242 Color: 2
Size: 154 Color: 17

Bin 4002: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 351 Color: 15
Size: 242 Color: 1

Bin 4003: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 18
Size: 247 Color: 15

Bin 4004: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 2
Size: 242 Color: 7
Size: 139 Color: 3

Bin 4005: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 6
Size: 204 Color: 11

Bin 4006: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 5
Size: 461 Color: 4

Bin 4007: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 3
Size: 314 Color: 15

Bin 4008: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 13

Bin 4009: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 2
Size: 252 Color: 16

Bin 4010: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 14
Size: 329 Color: 13

Bin 4011: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 15

Bin 4012: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 19
Size: 392 Color: 1

Bin 4013: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 8
Size: 340 Color: 16

Bin 4014: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 18
Size: 240 Color: 3

Bin 4015: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 11
Size: 115 Color: 9
Size: 107 Color: 6

Bin 4016: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 16
Size: 323 Color: 14

Bin 4017: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 5
Size: 440 Color: 8

Bin 4018: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 6
Size: 351 Color: 17

Bin 4019: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 11
Size: 345 Color: 19
Size: 306 Color: 12

Bin 4020: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 17
Size: 462 Color: 6

Bin 4021: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 17
Size: 243 Color: 12

Bin 4022: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 6
Size: 297 Color: 4

Bin 4023: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 12
Size: 157 Color: 7
Size: 155 Color: 10

Bin 4024: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 15
Size: 456 Color: 2

Bin 4025: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 18
Size: 486 Color: 3

Bin 4026: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 5
Size: 405 Color: 11

Bin 4027: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 1
Size: 376 Color: 19

Bin 4028: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 9
Size: 262 Color: 17
Size: 133 Color: 16

Bin 4029: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 13
Size: 369 Color: 11

Bin 4030: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 13
Size: 206 Color: 5

Bin 4031: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 5
Size: 263 Color: 15

Bin 4032: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 5
Size: 444 Color: 17

Bin 4033: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 18
Size: 433 Color: 13

Bin 4034: 0 of cap free
Amount of items: 3
Items: 
Size: 776 Color: 17
Size: 117 Color: 19
Size: 108 Color: 3

Bin 4035: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 12
Size: 259 Color: 0

Bin 4036: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 12
Size: 417 Color: 2

Bin 4037: 0 of cap free
Amount of items: 3
Items: 
Size: 748 Color: 11
Size: 145 Color: 14
Size: 108 Color: 19

Bin 4038: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 9
Size: 484 Color: 1

Bin 4039: 0 of cap free
Amount of items: 3
Items: 
Size: 513 Color: 14
Size: 338 Color: 7
Size: 150 Color: 18

Bin 4040: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 8
Size: 461 Color: 15

Bin 4041: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 13
Size: 406 Color: 15

Bin 4042: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 8
Size: 236 Color: 4

Bin 4043: 0 of cap free
Amount of items: 3
Items: 
Size: 504 Color: 11
Size: 301 Color: 5
Size: 196 Color: 14

Bin 4044: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 5
Size: 186 Color: 2
Size: 111 Color: 5

Bin 4045: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 1
Size: 455 Color: 15

Bin 4046: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 9
Size: 308 Color: 10

Bin 4047: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 10
Size: 394 Color: 9

Bin 4048: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 7
Size: 364 Color: 0

Bin 4049: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 19
Size: 353 Color: 10

Bin 4050: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 14
Size: 169 Color: 8
Size: 113 Color: 18

Bin 4051: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 1
Size: 430 Color: 6

Bin 4052: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 238 Color: 14

Bin 4053: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 7
Size: 250 Color: 19

Bin 4054: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 6
Size: 456 Color: 9

Bin 4055: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 11
Size: 453 Color: 9

Bin 4056: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 0

Bin 4057: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 15
Size: 231 Color: 4

Bin 4058: 0 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 17
Size: 195 Color: 10
Size: 167 Color: 15

Bin 4059: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 8
Size: 150 Color: 11
Size: 149 Color: 6

Bin 4060: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 5
Size: 127 Color: 12
Size: 119 Color: 19

Bin 4061: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 1
Size: 330 Color: 7

Bin 4062: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 8
Size: 466 Color: 17

Bin 4063: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 12
Size: 357 Color: 10

Bin 4064: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 281 Color: 1

Bin 4065: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 4
Size: 212 Color: 7

Bin 4066: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 14
Size: 440 Color: 2

Bin 4067: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 387 Color: 15

Bin 4068: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 18
Size: 212 Color: 17

Bin 4069: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 6
Size: 175 Color: 2
Size: 148 Color: 3

Bin 4070: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 13
Size: 434 Color: 3

Bin 4071: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 10
Size: 306 Color: 12

Bin 4072: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 3
Size: 324 Color: 10

Bin 4073: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 5
Size: 173 Color: 14
Size: 154 Color: 5

Bin 4074: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 5
Size: 251 Color: 12

Bin 4075: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 273 Color: 9
Size: 273 Color: 2

Bin 4076: 0 of cap free
Amount of items: 3
Items: 
Size: 587 Color: 13
Size: 245 Color: 0
Size: 169 Color: 18

Bin 4077: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 19
Size: 349 Color: 0
Size: 183 Color: 9

Bin 4078: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 19
Size: 184 Color: 2
Size: 136 Color: 12

Bin 4079: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 15
Size: 256 Color: 14

Bin 4080: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 413 Color: 4

Bin 4081: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 16
Size: 245 Color: 0

Bin 4082: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 17
Size: 397 Color: 12

Bin 4083: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 13
Size: 361 Color: 8
Size: 217 Color: 7

Bin 4084: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 12
Size: 226 Color: 18

Bin 4085: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 330 Color: 19

Bin 4086: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 9
Size: 455 Color: 6

Bin 4087: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 13
Size: 441 Color: 4

Bin 4088: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 4
Size: 243 Color: 12

Bin 4089: 0 of cap free
Amount of items: 3
Items: 
Size: 708 Color: 2
Size: 166 Color: 2
Size: 127 Color: 19

Bin 4090: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 12
Size: 266 Color: 6

Bin 4091: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 10
Size: 342 Color: 0

Bin 4092: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 6
Size: 491 Color: 11

Bin 4093: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 1
Size: 335 Color: 8

Bin 4094: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 11
Size: 398 Color: 19
Size: 172 Color: 3

Bin 4095: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 7

Bin 4096: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 14
Size: 454 Color: 8

Bin 4097: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 11
Size: 398 Color: 3
Size: 183 Color: 12

Bin 4098: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 17
Size: 332 Color: 10

Bin 4099: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 18
Size: 427 Color: 4

Bin 4100: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 9
Size: 215 Color: 18

Bin 4101: 0 of cap free
Amount of items: 3
Items: 
Size: 558 Color: 6
Size: 239 Color: 0
Size: 204 Color: 18

Bin 4102: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 9
Size: 446 Color: 4

Bin 4103: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 7
Size: 157 Color: 2
Size: 141 Color: 13

Bin 4104: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 13
Size: 452 Color: 5

Bin 4105: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 10
Size: 128 Color: 10
Size: 127 Color: 18

Bin 4106: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 11
Size: 389 Color: 14

Bin 4107: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 2
Size: 299 Color: 7

Bin 4108: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 17
Size: 249 Color: 13

Bin 4109: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 15
Size: 238 Color: 5

Bin 4110: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 10
Size: 187 Color: 10
Size: 186 Color: 19

Bin 4111: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 3
Size: 134 Color: 13
Size: 132 Color: 17

Bin 4112: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 6
Size: 467 Color: 17

Bin 4113: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 11
Size: 307 Color: 9

Bin 4114: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 0
Size: 292 Color: 16

Bin 4115: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 17
Size: 421 Color: 14

Bin 4116: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 16
Size: 394 Color: 1

Bin 4117: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 15
Size: 234 Color: 4
Size: 139 Color: 10

Bin 4118: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 368 Color: 12

Bin 4119: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 11
Size: 489 Color: 1

Bin 4120: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 7
Size: 353 Color: 13

Bin 4121: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 297 Color: 19
Size: 281 Color: 15

Bin 4122: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 2
Size: 479 Color: 7

Bin 4123: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 10
Size: 178 Color: 11
Size: 141 Color: 3

Bin 4124: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 10
Size: 286 Color: 18

Bin 4125: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 2
Size: 441 Color: 11

Bin 4126: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 16
Size: 493 Color: 2

Bin 4127: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 11
Size: 439 Color: 7

Bin 4128: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 13
Size: 496 Color: 6

Bin 4129: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 4
Size: 397 Color: 19

Bin 4130: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 10
Size: 251 Color: 2

Bin 4131: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 15

Bin 4132: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 16
Size: 160 Color: 14
Size: 160 Color: 5

Bin 4133: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 17
Size: 342 Color: 10

Bin 4134: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 0

Bin 4135: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 6
Size: 287 Color: 3

Bin 4136: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 8
Size: 411 Color: 19

Bin 4137: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 422 Color: 1

Bin 4138: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 12
Size: 213 Color: 8

Bin 4139: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 17

Bin 4140: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 18

Bin 4141: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 16
Size: 350 Color: 2

Bin 4142: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 11
Size: 231 Color: 9

Bin 4143: 0 of cap free
Amount of items: 4
Items: 
Size: 567 Color: 4
Size: 178 Color: 13
Size: 151 Color: 1
Size: 105 Color: 17

Bin 4144: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 13
Size: 454 Color: 3

Bin 4145: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 9
Size: 485 Color: 18

Bin 4146: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 7
Size: 376 Color: 18

Bin 4147: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 1
Size: 216 Color: 13

Bin 4148: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 19
Size: 251 Color: 13

Bin 4149: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 10
Size: 308 Color: 4

Bin 4150: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 5
Size: 359 Color: 6

Bin 4151: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 10
Size: 275 Color: 2

Bin 4152: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 6
Size: 282 Color: 13

Bin 4153: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 13
Size: 325 Color: 1

Bin 4154: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 2
Size: 196 Color: 9
Size: 169 Color: 10

Bin 4155: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 9
Size: 456 Color: 6

Bin 4156: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 8
Size: 352 Color: 1

Bin 4157: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 18
Size: 300 Color: 10

Bin 4158: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 9
Size: 307 Color: 10

Bin 4159: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 11
Size: 388 Color: 2

Bin 4160: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 17
Size: 408 Color: 11
Size: 117 Color: 0

Bin 4161: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 6
Size: 462 Color: 7

Bin 4162: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 9
Size: 418 Color: 18

Bin 4163: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 14
Size: 169 Color: 13
Size: 162 Color: 3

Bin 4164: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 16
Size: 141 Color: 17
Size: 116 Color: 0

Bin 4165: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 297 Color: 11

Bin 4166: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 16
Size: 285 Color: 6

Bin 4167: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 19
Size: 337 Color: 0
Size: 326 Color: 15

Bin 4168: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 17
Size: 152 Color: 3
Size: 146 Color: 6

Bin 4169: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 3
Size: 122 Color: 14
Size: 118 Color: 5

Bin 4170: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 12
Size: 341 Color: 4

Bin 4171: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 19
Size: 301 Color: 11

Bin 4172: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 260 Color: 5

Bin 4173: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 17
Size: 153 Color: 18
Size: 153 Color: 4

Bin 4174: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 8
Size: 272 Color: 8
Size: 140 Color: 16

Bin 4175: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 16
Size: 401 Color: 1

Bin 4176: 0 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 2
Size: 185 Color: 5
Size: 111 Color: 15

Bin 4177: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 6
Size: 431 Color: 16

Bin 4178: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 11
Size: 345 Color: 2
Size: 311 Color: 11

Bin 4179: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 5

Bin 4180: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 9
Size: 247 Color: 13

Bin 4181: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 12
Size: 331 Color: 0

Bin 4182: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 14
Size: 215 Color: 10

Bin 4183: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 9
Size: 229 Color: 0

Bin 4184: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 6
Size: 286 Color: 1

Bin 4185: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 5
Size: 341 Color: 2

Bin 4186: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 0
Size: 194 Color: 14
Size: 114 Color: 2

Bin 4187: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 16
Size: 469 Color: 5

Bin 4188: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 13
Size: 262 Color: 19

Bin 4189: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 18
Size: 472 Color: 6

Bin 4190: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 5
Size: 435 Color: 1

Bin 4191: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 3
Size: 460 Color: 0

Bin 4192: 0 of cap free
Amount of items: 3
Items: 
Size: 514 Color: 14
Size: 385 Color: 9
Size: 102 Color: 8

Bin 4193: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 12
Size: 309 Color: 18

Bin 4194: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 13
Size: 238 Color: 17
Size: 141 Color: 17

Bin 4195: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 10
Size: 294 Color: 9
Size: 285 Color: 17

Bin 4196: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 17
Size: 169 Color: 10
Size: 168 Color: 19

Bin 4197: 0 of cap free
Amount of items: 3
Items: 
Size: 624 Color: 6
Size: 191 Color: 8
Size: 186 Color: 9

Bin 4198: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 10
Size: 347 Color: 18
Size: 182 Color: 4

Bin 4199: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 7
Size: 445 Color: 6

Bin 4200: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 1
Size: 300 Color: 6

Bin 4201: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 6
Size: 359 Color: 3

Bin 4202: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 8
Size: 452 Color: 11

Bin 4203: 0 of cap free
Amount of items: 3
Items: 
Size: 646 Color: 9
Size: 234 Color: 8
Size: 121 Color: 8

Bin 4204: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 12
Size: 332 Color: 3
Size: 250 Color: 3

Bin 4205: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 2
Size: 395 Color: 1

Bin 4206: 0 of cap free
Amount of items: 3
Items: 
Size: 795 Color: 16
Size: 105 Color: 4
Size: 101 Color: 15

Bin 4207: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 13
Size: 460 Color: 19

Bin 4208: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 3
Size: 485 Color: 17

Bin 4209: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 10
Size: 398 Color: 3
Size: 148 Color: 10

Bin 4210: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 7
Size: 419 Color: 13
Size: 137 Color: 11

Bin 4211: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 8
Size: 391 Color: 18
Size: 135 Color: 9

Bin 4212: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 7
Size: 271 Color: 4

Bin 4213: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 16
Size: 325 Color: 2

Bin 4214: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 13
Size: 335 Color: 6

Bin 4215: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 2

Bin 4216: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 14
Size: 402 Color: 7

Bin 4217: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 203 Color: 7

Bin 4218: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 425 Color: 1
Size: 136 Color: 7

Bin 4219: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 200 Color: 13

Bin 4220: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 18
Size: 388 Color: 19

Bin 4221: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 14
Size: 377 Color: 5

Bin 4222: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 8
Size: 341 Color: 16

Bin 4223: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 9
Size: 303 Color: 7

Bin 4224: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 252 Color: 12

Bin 4225: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 13
Size: 368 Color: 14

Bin 4226: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 6
Size: 208 Color: 12
Size: 158 Color: 7

Bin 4227: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 16
Size: 361 Color: 9
Size: 279 Color: 19

Bin 4228: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 9
Size: 234 Color: 10
Size: 122 Color: 6

Bin 4229: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 0
Size: 197 Color: 18
Size: 118 Color: 8

Bin 4230: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 16
Size: 386 Color: 13

Bin 4231: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 6
Size: 417 Color: 4

Bin 4232: 0 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 10
Size: 255 Color: 14

Bin 4233: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 3
Size: 371 Color: 14

Bin 4234: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 2
Size: 342 Color: 19

Bin 4235: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 6
Size: 241 Color: 3

Bin 4236: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 10
Size: 367 Color: 16
Size: 140 Color: 15

Bin 4237: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 10
Size: 369 Color: 2

Bin 4238: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 16
Size: 429 Color: 7

Bin 4239: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 14
Size: 339 Color: 17

Bin 4240: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 4
Size: 273 Color: 3

Bin 4241: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 6
Size: 453 Color: 0

Bin 4242: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 15
Size: 486 Color: 3

Bin 4243: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 15
Size: 349 Color: 14
Size: 130 Color: 14

Bin 4244: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 6
Size: 360 Color: 0

Bin 4245: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 19
Size: 472 Color: 2

Bin 4246: 0 of cap free
Amount of items: 3
Items: 
Size: 551 Color: 12
Size: 261 Color: 18
Size: 189 Color: 12

Bin 4247: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 11
Size: 370 Color: 16

Bin 4248: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 9
Size: 336 Color: 19

Bin 4249: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 222 Color: 9

Bin 4250: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 10
Size: 317 Color: 15

Bin 4251: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 5
Size: 416 Color: 18

Bin 4252: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 17
Size: 224 Color: 6

Bin 4253: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 7
Size: 287 Color: 3

Bin 4254: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 6
Size: 478 Color: 5

Bin 4255: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 9
Size: 475 Color: 0

Bin 4256: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 18
Size: 299 Color: 15

Bin 4257: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 15
Size: 461 Color: 16

Bin 4258: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 16
Size: 480 Color: 10

Bin 4259: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 15
Size: 246 Color: 7

Bin 4260: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 3
Size: 332 Color: 17
Size: 271 Color: 16

Bin 4261: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 18
Size: 285 Color: 8

Bin 4262: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 6
Size: 485 Color: 14

Bin 4263: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 11
Size: 399 Color: 4

Bin 4264: 0 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 7
Size: 243 Color: 14

Bin 4265: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 18
Size: 357 Color: 7

Bin 4266: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 5
Size: 427 Color: 12

Bin 4267: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 5
Size: 140 Color: 6
Size: 108 Color: 18

Bin 4268: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 9
Size: 229 Color: 17
Size: 162 Color: 10

Bin 4269: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 19
Size: 425 Color: 16
Size: 122 Color: 4

Bin 4270: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 13
Size: 399 Color: 9

Bin 4271: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 5
Size: 500 Color: 17

Bin 4272: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 3
Size: 362 Color: 8

Bin 4273: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 12
Size: 183 Color: 18
Size: 134 Color: 18

Bin 4274: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9
Size: 398 Color: 4
Size: 158 Color: 3

Bin 4275: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 13
Size: 421 Color: 12

Bin 4276: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 371 Color: 2
Size: 161 Color: 4

Bin 4277: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 5
Size: 211 Color: 13

Bin 4278: 0 of cap free
Amount of items: 3
Items: 
Size: 645 Color: 5
Size: 190 Color: 11
Size: 166 Color: 8

Bin 4279: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 14
Size: 179 Color: 17
Size: 126 Color: 11

Bin 4280: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 10
Size: 336 Color: 8

Bin 4281: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 14
Size: 310 Color: 0
Size: 300 Color: 11

Bin 4282: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 344 Color: 12

Bin 4283: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 13
Size: 338 Color: 9
Size: 139 Color: 4

Bin 4284: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 7
Size: 218 Color: 8

Bin 4285: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 371 Color: 7

Bin 4286: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 13
Size: 379 Color: 18

Bin 4287: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 9
Size: 259 Color: 4

Bin 4288: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 6
Size: 352 Color: 0
Size: 196 Color: 18

Bin 4289: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 8
Size: 271 Color: 4

Bin 4290: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 3
Size: 208 Color: 0

Bin 4291: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 15
Size: 421 Color: 1

Bin 4292: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 9
Size: 271 Color: 12
Size: 164 Color: 6

Bin 4293: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 2
Size: 262 Color: 16

Bin 4294: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 19
Size: 234 Color: 12
Size: 134 Color: 10

Bin 4295: 0 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 5
Size: 182 Color: 5
Size: 176 Color: 12

Bin 4296: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 19
Size: 362 Color: 3

Bin 4297: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 6
Size: 132 Color: 19
Size: 101 Color: 3

Bin 4298: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 0
Size: 396 Color: 19

Bin 4299: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 19
Size: 290 Color: 15

Bin 4300: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 1
Size: 495 Color: 16

Bin 4301: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 10
Size: 395 Color: 0

Bin 4302: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 19
Size: 375 Color: 6

Bin 4303: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 13
Size: 129 Color: 8
Size: 104 Color: 3

Bin 4304: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 19
Size: 448 Color: 3

Bin 4305: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 15
Size: 328 Color: 13
Size: 149 Color: 6

Bin 4306: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 8
Size: 395 Color: 19

Bin 4307: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 8
Size: 151 Color: 19
Size: 128 Color: 5

Bin 4308: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 17
Size: 223 Color: 15

Bin 4309: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 9
Size: 290 Color: 12

Bin 4310: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 17
Size: 215 Color: 2

Bin 4311: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 19
Size: 458 Color: 5

Bin 4312: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 16
Size: 233 Color: 17

Bin 4313: 0 of cap free
Amount of items: 3
Items: 
Size: 588 Color: 15
Size: 220 Color: 4
Size: 193 Color: 1

Bin 4314: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 3
Size: 442 Color: 12

Bin 4315: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 17
Size: 213 Color: 10

Bin 4316: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 0
Size: 461 Color: 3

Bin 4317: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 8
Size: 292 Color: 11

Bin 4318: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 18
Size: 113 Color: 17
Size: 101 Color: 12

Bin 4319: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 2
Size: 253 Color: 6

Bin 4320: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 16
Size: 356 Color: 17

Bin 4321: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 9
Size: 376 Color: 13

Bin 4322: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 4
Size: 245 Color: 0
Size: 152 Color: 3

Bin 4323: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 14
Size: 444 Color: 12

Bin 4324: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 6
Size: 311 Color: 4

Bin 4325: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 13
Size: 154 Color: 19
Size: 101 Color: 4

Bin 4326: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 10
Size: 246 Color: 15

Bin 4327: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 18
Size: 297 Color: 0

Bin 4328: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 12
Size: 490 Color: 16

Bin 4329: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 7
Size: 403 Color: 17

Bin 4330: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 10
Size: 203 Color: 12
Size: 133 Color: 1

Bin 4331: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 4
Size: 415 Color: 13

Bin 4332: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 5
Size: 324 Color: 8

Bin 4333: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 14
Size: 444 Color: 1

Bin 4334: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 6
Size: 347 Color: 2

Bin 4335: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 19
Size: 475 Color: 10

Bin 4336: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 19
Size: 341 Color: 5

Bin 4337: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 15
Size: 423 Color: 5
Size: 115 Color: 0

Bin 4338: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 3
Size: 292 Color: 10

Bin 4339: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 19

Bin 4340: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 19
Size: 250 Color: 0
Size: 142 Color: 13

Bin 4341: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 2
Size: 413 Color: 11

Bin 4342: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 7
Size: 445 Color: 18

Bin 4343: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 11
Size: 125 Color: 14
Size: 115 Color: 7

Bin 4344: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 0
Size: 332 Color: 0
Size: 184 Color: 16

Bin 4345: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 2
Size: 115 Color: 15
Size: 107 Color: 16

Bin 4346: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 11
Size: 233 Color: 8

Bin 4347: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 13
Size: 247 Color: 14

Bin 4348: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 13
Size: 457 Color: 9

Bin 4349: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 3
Size: 319 Color: 19

Bin 4350: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 8
Size: 269 Color: 18

Bin 4351: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 16
Size: 356 Color: 19

Bin 4352: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 7
Size: 460 Color: 15

Bin 4353: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 8
Size: 261 Color: 5
Size: 118 Color: 8

Bin 4354: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 17
Size: 303 Color: 7

Bin 4355: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 16
Size: 354 Color: 17

Bin 4356: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 15
Size: 305 Color: 11

Bin 4357: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 14
Size: 246 Color: 11

Bin 4358: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 0
Size: 299 Color: 19

Bin 4359: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 18
Size: 421 Color: 11

Bin 4360: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 16
Size: 173 Color: 3
Size: 173 Color: 2

Bin 4361: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 4
Size: 252 Color: 12

Bin 4362: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 4
Size: 300 Color: 5

Bin 4363: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 18
Size: 479 Color: 9

Bin 4364: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 6
Size: 201 Color: 8

Bin 4365: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 13
Size: 327 Color: 11
Size: 323 Color: 4

Bin 4366: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 4
Size: 435 Color: 15

Bin 4367: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 12
Size: 259 Color: 10

Bin 4368: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 1
Size: 309 Color: 11

Bin 4369: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 17
Size: 417 Color: 11

Bin 4370: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 13
Size: 323 Color: 3

Bin 4371: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 5
Size: 275 Color: 15

Bin 4372: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 13
Size: 326 Color: 0

Bin 4373: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 12
Size: 128 Color: 1
Size: 116 Color: 10

Bin 4374: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 6
Size: 380 Color: 4

Bin 4375: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 5
Size: 452 Color: 8

Bin 4376: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 13
Size: 173 Color: 10
Size: 141 Color: 4

Bin 4377: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 321 Color: 12

Bin 4378: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 3
Size: 394 Color: 15

Bin 4379: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 2

Bin 4380: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 8
Size: 464 Color: 10

Bin 4381: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 14
Size: 401 Color: 16

Bin 4382: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 5
Size: 465 Color: 17

Bin 4383: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 18
Size: 267 Color: 4

Bin 4384: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 4
Size: 285 Color: 5

Bin 4385: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 3
Size: 175 Color: 11
Size: 129 Color: 15

Bin 4386: 0 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 17
Size: 364 Color: 9

Bin 4387: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 5
Size: 245 Color: 0
Size: 183 Color: 14

Bin 4388: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 9
Size: 346 Color: 11

Bin 4389: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 237 Color: 7

Bin 4390: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 4
Size: 195 Color: 15
Size: 185 Color: 12

Bin 4391: 0 of cap free
Amount of items: 3
Items: 
Size: 773 Color: 1
Size: 122 Color: 7
Size: 106 Color: 7

Bin 4392: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 19
Size: 390 Color: 15

Bin 4393: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 14
Size: 360 Color: 6
Size: 281 Color: 10

Bin 4394: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 12
Size: 261 Color: 9

Bin 4395: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 11
Size: 328 Color: 13

Bin 4396: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 15
Size: 430 Color: 9

Bin 4397: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 5
Size: 404 Color: 11

Bin 4398: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 352 Color: 14

Bin 4399: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 19

Bin 4400: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 14
Size: 442 Color: 16

Bin 4401: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 14
Size: 400 Color: 1

Bin 4402: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 11
Size: 500 Color: 10

Bin 4403: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 10
Size: 238 Color: 18
Size: 164 Color: 15

Bin 4404: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 14

Bin 4405: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 18
Size: 133 Color: 8
Size: 125 Color: 12

Bin 4406: 0 of cap free
Amount of items: 3
Items: 
Size: 619 Color: 17
Size: 191 Color: 12
Size: 191 Color: 7

Bin 4407: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 19
Size: 235 Color: 12
Size: 235 Color: 2

Bin 4408: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 13
Size: 489 Color: 14

Bin 4409: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 12
Size: 472 Color: 15

Bin 4410: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 18
Size: 461 Color: 6

Bin 4411: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 17
Size: 308 Color: 15

Bin 4412: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 16
Size: 359 Color: 14

Bin 4413: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 14
Size: 385 Color: 16
Size: 122 Color: 14

Bin 4414: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 16
Size: 251 Color: 1

Bin 4415: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 18
Size: 166 Color: 8
Size: 163 Color: 9

Bin 4416: 0 of cap free
Amount of items: 3
Items: 
Size: 621 Color: 4
Size: 190 Color: 16
Size: 190 Color: 9

Bin 4417: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 7
Size: 110 Color: 9
Size: 105 Color: 0

Bin 4418: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 12
Size: 273 Color: 4

Bin 4419: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 6
Size: 495 Color: 2

Bin 4420: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 4
Size: 356 Color: 18

Bin 4421: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 19
Size: 352 Color: 8

Bin 4422: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 18
Size: 422 Color: 5

Bin 4423: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 17
Size: 371 Color: 3

Bin 4424: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 12
Size: 361 Color: 8

Bin 4425: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 2
Size: 469 Color: 19

Bin 4426: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 10
Size: 454 Color: 15

Bin 4427: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 17
Size: 489 Color: 16

Bin 4428: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 285 Color: 18

Bin 4429: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 0
Size: 251 Color: 5
Size: 177 Color: 5

Bin 4430: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 18
Size: 479 Color: 15

Bin 4431: 0 of cap free
Amount of items: 3
Items: 
Size: 604 Color: 5
Size: 235 Color: 16
Size: 162 Color: 17

Bin 4432: 0 of cap free
Amount of items: 3
Items: 
Size: 612 Color: 5
Size: 195 Color: 15
Size: 194 Color: 2

Bin 4433: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 8
Size: 238 Color: 1

Bin 4434: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 10
Size: 470 Color: 13

Bin 4435: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 11
Size: 333 Color: 18

Bin 4436: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 6
Size: 456 Color: 5

Bin 4437: 0 of cap free
Amount of items: 3
Items: 
Size: 633 Color: 7
Size: 192 Color: 16
Size: 176 Color: 11

Bin 4438: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 18
Size: 228 Color: 0

Bin 4439: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 15
Size: 350 Color: 4

Bin 4440: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 9
Size: 416 Color: 3

Bin 4441: 0 of cap free
Amount of items: 3
Items: 
Size: 779 Color: 14
Size: 119 Color: 1
Size: 103 Color: 19

Bin 4442: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 5
Size: 446 Color: 11

Bin 4443: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 17
Size: 418 Color: 1

Bin 4444: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 9
Size: 321 Color: 8
Size: 172 Color: 1

Bin 4445: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 13
Size: 153 Color: 10
Size: 153 Color: 1

Bin 4446: 0 of cap free
Amount of items: 3
Items: 
Size: 573 Color: 14
Size: 229 Color: 13
Size: 199 Color: 8

Bin 4447: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 2
Size: 392 Color: 10

Bin 4448: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 5

Bin 4449: 0 of cap free
Amount of items: 3
Items: 
Size: 682 Color: 18
Size: 160 Color: 16
Size: 159 Color: 10

Bin 4450: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 10
Size: 310 Color: 4
Size: 157 Color: 18

Bin 4451: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 5
Size: 262 Color: 6

Bin 4452: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 0
Size: 375 Color: 2

Bin 4453: 0 of cap free
Amount of items: 3
Items: 
Size: 665 Color: 6
Size: 181 Color: 11
Size: 155 Color: 19

Bin 4454: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 17
Size: 374 Color: 12

Bin 4455: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 10
Size: 493 Color: 18

Bin 4456: 1 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 13
Size: 438 Color: 4

Bin 4457: 1 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 19
Size: 500 Color: 4

Bin 4458: 1 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 4
Size: 408 Color: 1

Bin 4459: 1 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 16
Size: 469 Color: 17

Bin 4460: 1 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 14
Size: 459 Color: 17

Bin 4461: 1 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 0
Size: 459 Color: 2

Bin 4462: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 4
Size: 442 Color: 15

Bin 4463: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 4
Size: 498 Color: 10

Bin 4464: 1 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 16
Size: 165 Color: 17
Size: 110 Color: 0

Bin 4465: 1 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 4
Size: 307 Color: 9

Bin 4466: 1 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 8
Size: 391 Color: 12

Bin 4467: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 14
Size: 498 Color: 10

Bin 4468: 1 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 10
Size: 213 Color: 4

Bin 4469: 1 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 10
Size: 476 Color: 16

Bin 4470: 1 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 485 Color: 13

Bin 4471: 1 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 14
Size: 459 Color: 3

Bin 4472: 1 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 7
Size: 213 Color: 5

Bin 4473: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 18
Size: 492 Color: 7

Bin 4474: 1 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 19
Size: 478 Color: 16

Bin 4475: 1 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 7
Size: 481 Color: 3

Bin 4476: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 10
Size: 498 Color: 2

Bin 4477: 1 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 5
Size: 473 Color: 4

Bin 4478: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 8
Size: 492 Color: 2

Bin 4479: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 16
Size: 492 Color: 8

Bin 4480: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 442 Color: 10

Bin 4481: 1 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 7
Size: 459 Color: 18

Bin 4482: 1 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 6
Size: 500 Color: 4

Bin 4483: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 3
Size: 361 Color: 1

Bin 4484: 1 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 10
Size: 498 Color: 11

Bin 4485: 1 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 18
Size: 500 Color: 5

Bin 4486: 1 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 17
Size: 245 Color: 10

Bin 4487: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 3
Size: 442 Color: 16

Bin 4488: 1 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 6
Size: 351 Color: 7

Bin 4489: 1 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 4
Size: 448 Color: 3

Bin 4490: 1 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 7
Size: 492 Color: 17

Bin 4491: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 14
Size: 442 Color: 6

Bin 4492: 2 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 10
Size: 472 Color: 0

Bin 4493: 2 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 12
Size: 485 Color: 3

Bin 4494: 2 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 16
Size: 208 Color: 19

Bin 4495: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 16
Size: 385 Color: 8

Bin 4496: 2 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 12
Size: 463 Color: 0

Bin 4497: 2 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 0
Size: 459 Color: 13

Bin 4498: 2 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 3
Size: 323 Color: 6

Bin 4499: 2 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 17
Size: 468 Color: 5

Bin 4500: 2 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 10
Size: 403 Color: 2

Bin 4501: 3 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 471 Color: 0

Bin 4502: 3 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 15
Size: 207 Color: 18

Bin 4503: 3 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 18
Size: 402 Color: 15

Bin 4504: 3 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 13
Size: 485 Color: 14

Bin 4505: 3 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 1
Size: 399 Color: 15

Bin 4506: 3 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 12
Size: 199 Color: 10

Bin 4507: 3 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 14
Size: 436 Color: 6

Bin 4508: 4 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 1
Size: 445 Color: 5

Bin 4509: 5 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 11
Size: 453 Color: 5

Bin 4510: 5 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 12
Size: 483 Color: 6

Bin 4511: 5 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 434 Color: 17

Bin 4512: 5 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 9
Size: 197 Color: 8

Bin 4513: 5 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 17
Size: 197 Color: 6

Bin 4514: 8 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 1
Size: 459 Color: 7

Bin 4515: 8 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 420 Color: 14

Bin 4516: 10 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 5
Size: 192 Color: 2

Bin 4517: 12 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 1
Size: 210 Color: 4

Bin 4518: 16 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 13
Size: 186 Color: 14

Bin 4519: 26 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 1
Size: 179 Color: 11

Bin 4520: 29 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 3
Size: 384 Color: 5

Bin 4521: 35 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 11
Size: 170 Color: 14

Bin 4522: 37 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 11
Size: 168 Color: 4

Bin 4523: 57 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 14
Size: 148 Color: 4

Bin 4524: 99 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 128 Color: 3

Bin 4525: 128 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 17
Size: 185 Color: 13

Bin 4526: 205 of cap free
Amount of items: 1
Items: 
Size: 796 Color: 11

Bin 4527: 206 of cap free
Amount of items: 1
Items: 
Size: 795 Color: 7

Bin 4528: 206 of cap free
Amount of items: 1
Items: 
Size: 795 Color: 12

Bin 4529: 206 of cap free
Amount of items: 1
Items: 
Size: 795 Color: 1

Bin 4530: 210 of cap free
Amount of items: 1
Items: 
Size: 791 Color: 0

Bin 4531: 210 of cap free
Amount of items: 1
Items: 
Size: 791 Color: 11

Bin 4532: 214 of cap free
Amount of items: 1
Items: 
Size: 787 Color: 14

Bin 4533: 214 of cap free
Amount of items: 1
Items: 
Size: 787 Color: 9

Bin 4534: 215 of cap free
Amount of items: 1
Items: 
Size: 786 Color: 7

Bin 4535: 222 of cap free
Amount of items: 1
Items: 
Size: 779 Color: 13

Bin 4536: 222 of cap free
Amount of items: 1
Items: 
Size: 779 Color: 6

Bin 4537: 222 of cap free
Amount of items: 1
Items: 
Size: 779 Color: 4

Total size: 4538416
Total free space: 3121

