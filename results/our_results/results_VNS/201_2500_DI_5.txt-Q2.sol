Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 0
Size: 631 Color: 0
Size: 128 Color: 1

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1716 Color: 0
Size: 684 Color: 1
Size: 48 Color: 1
Size: 8 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 280 Color: 0
Size: 148 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 0
Size: 330 Color: 1
Size: 8 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 0
Size: 232 Color: 1
Size: 76 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 0
Size: 204 Color: 0
Size: 64 Color: 1

Bin 7: 1 of cap free
Amount of items: 9
Items: 
Size: 1229 Color: 0
Size: 226 Color: 0
Size: 200 Color: 0
Size: 200 Color: 0
Size: 152 Color: 1
Size: 148 Color: 1
Size: 144 Color: 1
Size: 108 Color: 0
Size: 48 Color: 1

Bin 8: 1 of cap free
Amount of items: 3
Items: 
Size: 1924 Color: 1
Size: 443 Color: 0
Size: 88 Color: 0

Bin 9: 1 of cap free
Amount of items: 4
Items: 
Size: 2001 Color: 0
Size: 384 Color: 1
Size: 50 Color: 1
Size: 20 Color: 0

Bin 10: 1 of cap free
Amount of items: 2
Items: 
Size: 2002 Color: 0
Size: 453 Color: 1

Bin 11: 1 of cap free
Amount of items: 2
Items: 
Size: 2202 Color: 1
Size: 253 Color: 0

Bin 12: 2 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 0
Size: 916 Color: 0
Size: 96 Color: 1

Bin 13: 2 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 1
Size: 918 Color: 0

Bin 14: 2 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 730 Color: 0
Size: 80 Color: 1

Bin 15: 2 of cap free
Amount of items: 4
Items: 
Size: 1908 Color: 0
Size: 470 Color: 1
Size: 44 Color: 0
Size: 32 Color: 1

Bin 16: 2 of cap free
Amount of items: 2
Items: 
Size: 1978 Color: 0
Size: 476 Color: 1

Bin 17: 2 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 0
Size: 460 Color: 1

Bin 18: 2 of cap free
Amount of items: 2
Items: 
Size: 2050 Color: 1
Size: 404 Color: 0

Bin 19: 2 of cap free
Amount of items: 2
Items: 
Size: 2106 Color: 0
Size: 348 Color: 1

Bin 20: 2 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 0
Size: 260 Color: 1
Size: 2 Color: 1

Bin 21: 3 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 398 Color: 0
Size: 381 Color: 1

Bin 22: 3 of cap free
Amount of items: 2
Items: 
Size: 2111 Color: 1
Size: 342 Color: 0

Bin 23: 3 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 1
Size: 300 Color: 0

Bin 24: 4 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 0
Size: 976 Color: 0
Size: 112 Color: 1

Bin 25: 4 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 1
Size: 756 Color: 0

Bin 26: 4 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 1
Size: 751 Color: 0

Bin 27: 4 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 1
Size: 568 Color: 0
Size: 120 Color: 0

Bin 28: 4 of cap free
Amount of items: 2
Items: 
Size: 1815 Color: 1
Size: 637 Color: 0

Bin 29: 4 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 0
Size: 542 Color: 1

Bin 30: 4 of cap free
Amount of items: 2
Items: 
Size: 1913 Color: 1
Size: 539 Color: 0

Bin 31: 4 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 1
Size: 512 Color: 0

Bin 32: 5 of cap free
Amount of items: 4
Items: 
Size: 1468 Color: 0
Size: 887 Color: 1
Size: 56 Color: 0
Size: 40 Color: 1

Bin 33: 5 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 0
Size: 840 Color: 1
Size: 28 Color: 0

Bin 34: 5 of cap free
Amount of items: 2
Items: 
Size: 2062 Color: 0
Size: 389 Color: 1

Bin 35: 5 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 289 Color: 0
Size: 16 Color: 1

Bin 36: 6 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 1
Size: 449 Color: 0
Size: 436 Color: 0
Size: 228 Color: 1
Size: 106 Color: 1

Bin 37: 7 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 0
Size: 1023 Color: 1

Bin 38: 7 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 0
Size: 828 Color: 1
Size: 64 Color: 0

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1925 Color: 1
Size: 524 Color: 0

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 2005 Color: 0
Size: 444 Color: 1

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1991 Color: 1
Size: 458 Color: 0

Bin 42: 8 of cap free
Amount of items: 23
Items: 
Size: 172 Color: 0
Size: 160 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0
Size: 128 Color: 0
Size: 126 Color: 1
Size: 126 Color: 0
Size: 124 Color: 1
Size: 120 Color: 1
Size: 106 Color: 1
Size: 104 Color: 0
Size: 104 Color: 0
Size: 92 Color: 1
Size: 90 Color: 1
Size: 88 Color: 1
Size: 88 Color: 1
Size: 88 Color: 1
Size: 88 Color: 1
Size: 88 Color: 0
Size: 80 Color: 0
Size: 76 Color: 0
Size: 64 Color: 1
Size: 48 Color: 0

Bin 43: 8 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 0
Size: 654 Color: 1

Bin 44: 8 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 0
Size: 282 Color: 1

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 2186 Color: 1
Size: 262 Color: 0

Bin 46: 9 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 0
Size: 861 Color: 1
Size: 56 Color: 0

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1718 Color: 0
Size: 729 Color: 1

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 2061 Color: 1
Size: 386 Color: 0

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 1
Size: 866 Color: 1
Size: 24 Color: 0

Bin 50: 10 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 1
Size: 774 Color: 1
Size: 90 Color: 0

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1892 Color: 0
Size: 554 Color: 1

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 2044 Color: 0
Size: 402 Color: 1

Bin 53: 11 of cap free
Amount of items: 2
Items: 
Size: 1827 Color: 1
Size: 618 Color: 0

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1423 Color: 0
Size: 1021 Color: 1

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1811 Color: 0
Size: 633 Color: 1

Bin 56: 17 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 0
Size: 1018 Color: 1

Bin 57: 20 of cap free
Amount of items: 6
Items: 
Size: 1230 Color: 0
Size: 330 Color: 0
Size: 242 Color: 1
Size: 226 Color: 0
Size: 204 Color: 1
Size: 204 Color: 1

Bin 58: 22 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1022 Color: 0
Size: 176 Color: 1

Bin 59: 22 of cap free
Amount of items: 2
Items: 
Size: 1909 Color: 0
Size: 525 Color: 1

Bin 60: 24 of cap free
Amount of items: 2
Items: 
Size: 1852 Color: 1
Size: 580 Color: 0

Bin 61: 25 of cap free
Amount of items: 2
Items: 
Size: 2100 Color: 1
Size: 331 Color: 0

Bin 62: 26 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 0
Size: 620 Color: 1

Bin 63: 27 of cap free
Amount of items: 2
Items: 
Size: 1894 Color: 1
Size: 535 Color: 0

Bin 64: 34 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 1020 Color: 0
Size: 164 Color: 1

Bin 65: 34 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 0
Size: 457 Color: 1
Size: 272 Color: 0

Bin 66: 1946 of cap free
Amount of items: 8
Items: 
Size: 88 Color: 1
Size: 76 Color: 0
Size: 74 Color: 0
Size: 64 Color: 1
Size: 56 Color: 1
Size: 56 Color: 1
Size: 48 Color: 0
Size: 48 Color: 0

Total size: 159640
Total free space: 2456

