Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 366 Color: 13
Size: 263 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 269 Color: 9
Size: 252 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 13
Size: 266 Color: 8
Size: 259 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 12
Size: 259 Color: 3
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 17
Size: 312 Color: 16
Size: 305 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 16
Size: 317 Color: 18
Size: 294 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 11
Size: 295 Color: 2
Size: 290 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 263 Color: 7
Size: 257 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 316 Color: 5
Size: 257 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 8
Size: 351 Color: 2
Size: 298 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 253 Color: 3
Size: 251 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 17
Size: 256 Color: 10
Size: 252 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 17
Size: 293 Color: 14
Size: 263 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 10
Size: 302 Color: 4
Size: 255 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 10
Size: 278 Color: 8
Size: 270 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 17
Size: 284 Color: 8
Size: 251 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9
Size: 275 Color: 7
Size: 257 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 255 Color: 1
Size: 250 Color: 19

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 13
Size: 266 Color: 7
Size: 255 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 18
Size: 314 Color: 10
Size: 293 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 15
Size: 325 Color: 10
Size: 260 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 9
Size: 284 Color: 7
Size: 277 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 19
Size: 273 Color: 11
Size: 261 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 8
Size: 289 Color: 13
Size: 264 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 321 Color: 13
Size: 269 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 8
Size: 362 Color: 3
Size: 274 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 17
Size: 297 Color: 2
Size: 289 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 13
Size: 313 Color: 14
Size: 300 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 10
Size: 335 Color: 8
Size: 308 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 12
Size: 310 Color: 1
Size: 278 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 6
Size: 291 Color: 18
Size: 256 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 19
Size: 350 Color: 3
Size: 255 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 10
Size: 309 Color: 12
Size: 263 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 7
Size: 370 Color: 16
Size: 250 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 300 Color: 1
Size: 293 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 9
Size: 331 Color: 10
Size: 303 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 13
Size: 285 Color: 4
Size: 283 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 17
Size: 321 Color: 19
Size: 259 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 17
Size: 289 Color: 0
Size: 254 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 354 Color: 15
Size: 250 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 318 Color: 5
Size: 310 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 7
Size: 339 Color: 13
Size: 253 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 14
Size: 285 Color: 14
Size: 262 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 11
Size: 251 Color: 9
Size: 250 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 332 Color: 18
Size: 254 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 342 Color: 2
Size: 274 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 300 Color: 7
Size: 294 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 284 Color: 15
Size: 284 Color: 9
Size: 432 Color: 6

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 13
Size: 356 Color: 15
Size: 264 Color: 6

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 18
Size: 321 Color: 9
Size: 266 Color: 7

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 2
Size: 352 Color: 12
Size: 256 Color: 19

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 15
Size: 357 Color: 18
Size: 280 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 346 Color: 18
Size: 251 Color: 19

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 12
Size: 261 Color: 4
Size: 257 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 335 Color: 6
Size: 313 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 9
Size: 290 Color: 11
Size: 262 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 14
Size: 256 Color: 11
Size: 250 Color: 5

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 4
Size: 268 Color: 19
Size: 265 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 300 Color: 13
Size: 271 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 12
Size: 271 Color: 0
Size: 250 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 308 Color: 14
Size: 250 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 369 Color: 1
Size: 256 Color: 13

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 4
Size: 256 Color: 19
Size: 251 Color: 6

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 6
Size: 316 Color: 2
Size: 270 Color: 6

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 14
Size: 289 Color: 12
Size: 254 Color: 6

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 11
Size: 352 Color: 11
Size: 274 Color: 8

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 5
Size: 252 Color: 2
Size: 251 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 4
Size: 253 Color: 0
Size: 253 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 295 Color: 12
Size: 252 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 336 Color: 10
Size: 269 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 13
Size: 313 Color: 5
Size: 264 Color: 19

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 4
Size: 264 Color: 0
Size: 258 Color: 14

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 13
Size: 350 Color: 6
Size: 257 Color: 19

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 19
Size: 306 Color: 15
Size: 258 Color: 13

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 5
Size: 282 Color: 16
Size: 278 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 5
Size: 341 Color: 0
Size: 259 Color: 14

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 15
Size: 282 Color: 8
Size: 258 Color: 6

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 18
Size: 332 Color: 4
Size: 282 Color: 13

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 5
Size: 352 Color: 2
Size: 254 Color: 13

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 19
Size: 289 Color: 15
Size: 250 Color: 14

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 369 Color: 2
Size: 255 Color: 13

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 5
Size: 340 Color: 5
Size: 314 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 7
Size: 298 Color: 6
Size: 254 Color: 16

Total size: 83000
Total free space: 0

