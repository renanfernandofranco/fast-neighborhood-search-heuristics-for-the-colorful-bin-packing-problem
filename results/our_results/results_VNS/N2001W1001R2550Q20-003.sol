Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 18
Size: 331 Color: 17
Size: 269 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 12
Size: 311 Color: 18
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 328 Color: 5
Size: 284 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 315 Color: 2
Size: 258 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 10
Size: 253 Color: 11
Size: 251 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 314 Color: 5
Size: 289 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 289 Color: 12
Size: 263 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 12
Size: 306 Color: 18
Size: 283 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 15
Size: 324 Color: 1
Size: 286 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 356 Color: 13
Size: 258 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 10
Size: 297 Color: 19
Size: 284 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 11
Size: 319 Color: 0
Size: 298 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 306 Color: 14
Size: 291 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 13
Size: 330 Color: 2
Size: 294 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 338 Color: 0
Size: 305 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 350 Color: 19
Size: 251 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 11
Size: 308 Color: 3
Size: 254 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 357 Color: 12
Size: 255 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 5
Size: 324 Color: 3
Size: 263 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 17
Size: 269 Color: 15
Size: 255 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 17
Size: 294 Color: 15
Size: 250 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 286 Color: 6
Size: 271 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 333 Color: 12
Size: 270 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 8
Size: 255 Color: 6
Size: 253 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 276 Color: 16
Size: 264 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 6
Size: 328 Color: 2
Size: 304 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 16
Size: 284 Color: 4
Size: 261 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 14
Size: 266 Color: 18
Size: 265 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 14
Size: 360 Color: 5
Size: 261 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 7
Size: 340 Color: 4
Size: 296 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6
Size: 326 Color: 13
Size: 311 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 351 Color: 16
Size: 255 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 370 Color: 13
Size: 253 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 7
Size: 318 Color: 8
Size: 281 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 3
Size: 284 Color: 4
Size: 250 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 2
Size: 285 Color: 19
Size: 268 Color: 9

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 286 Color: 17
Size: 269 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 355 Color: 17
Size: 259 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 13
Size: 277 Color: 11
Size: 273 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 4
Size: 279 Color: 4
Size: 273 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 283 Color: 12
Size: 263 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 287 Color: 1
Size: 256 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 4
Size: 340 Color: 19
Size: 286 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 16
Size: 282 Color: 19
Size: 250 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 19
Size: 363 Color: 16
Size: 259 Color: 7

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8
Size: 305 Color: 0
Size: 260 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 17
Size: 330 Color: 8
Size: 310 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 5
Size: 290 Color: 4
Size: 282 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 309 Color: 9
Size: 418 Color: 8
Size: 274 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 7
Size: 299 Color: 11
Size: 275 Color: 8

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 13
Size: 314 Color: 3
Size: 288 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 11
Size: 301 Color: 16
Size: 294 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 11
Size: 289 Color: 3
Size: 264 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 352 Color: 16
Size: 276 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 5
Size: 351 Color: 17
Size: 283 Color: 12

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 3
Size: 329 Color: 3
Size: 285 Color: 12

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 19
Size: 329 Color: 18
Size: 261 Color: 16

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 363 Color: 18
Size: 257 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 19
Size: 334 Color: 0
Size: 250 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 330 Color: 1
Size: 274 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 18
Size: 332 Color: 19
Size: 318 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 8
Size: 254 Color: 5
Size: 252 Color: 11

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 15
Size: 306 Color: 6
Size: 295 Color: 17

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 9
Size: 331 Color: 18
Size: 279 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 18
Size: 354 Color: 17
Size: 255 Color: 7

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 12
Size: 355 Color: 6
Size: 263 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 11
Size: 285 Color: 17
Size: 258 Color: 18

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 252 Color: 0
Size: 250 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 16
Size: 277 Color: 7
Size: 255 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 12
Size: 355 Color: 12
Size: 252 Color: 10

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 18
Size: 267 Color: 15
Size: 254 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 0
Size: 333 Color: 10
Size: 309 Color: 18

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 14
Size: 250 Color: 18
Size: 250 Color: 8

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 273 Color: 10
Size: 256 Color: 19

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 8
Size: 355 Color: 3
Size: 268 Color: 15

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 10
Size: 279 Color: 7
Size: 257 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 336 Color: 12
Size: 285 Color: 17

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 19
Size: 325 Color: 3
Size: 271 Color: 14

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 5
Size: 290 Color: 6
Size: 271 Color: 7

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 9
Size: 323 Color: 5
Size: 255 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 361 Color: 2
Size: 269 Color: 6

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 16
Size: 328 Color: 6
Size: 320 Color: 6

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 6
Size: 317 Color: 18
Size: 290 Color: 11

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 12
Size: 294 Color: 19
Size: 270 Color: 14

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 14
Size: 284 Color: 7
Size: 250 Color: 5

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 15
Size: 252 Color: 14

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 7
Size: 309 Color: 0
Size: 279 Color: 12

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 8
Size: 265 Color: 11
Size: 255 Color: 5

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 296 Color: 13
Size: 294 Color: 18

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 17
Size: 262 Color: 3
Size: 252 Color: 7

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 12
Size: 283 Color: 19
Size: 265 Color: 12

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 319 Color: 17
Size: 277 Color: 12

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 13
Size: 290 Color: 6
Size: 269 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 348 Color: 16
Size: 255 Color: 14

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 6
Size: 280 Color: 9
Size: 269 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 18
Size: 336 Color: 0
Size: 324 Color: 17

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 18
Size: 344 Color: 19
Size: 274 Color: 19

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 291 Color: 2
Size: 255 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 337 Color: 6
Size: 254 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 273 Color: 13
Size: 271 Color: 5

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 317 Color: 15
Size: 304 Color: 16

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 272 Color: 13
Size: 264 Color: 19

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 307 Color: 12
Size: 262 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 8
Size: 325 Color: 3
Size: 280 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 9
Size: 308 Color: 0
Size: 280 Color: 8

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 10
Size: 289 Color: 18
Size: 270 Color: 6

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 288 Color: 13
Size: 254 Color: 7

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 335 Color: 0
Size: 263 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 261 Color: 0
Size: 250 Color: 9

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 19
Size: 342 Color: 2
Size: 305 Color: 9

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 17
Size: 265 Color: 2
Size: 263 Color: 7

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 12
Size: 362 Color: 19
Size: 250 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 11
Size: 254 Color: 3
Size: 250 Color: 18

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 13
Size: 319 Color: 16
Size: 295 Color: 7

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 302 Color: 12
Size: 293 Color: 5

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 16
Size: 267 Color: 2
Size: 262 Color: 7

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 19
Size: 345 Color: 2
Size: 261 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 323 Color: 18
Size: 283 Color: 14

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 325 Color: 11
Size: 276 Color: 9

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 6
Size: 345 Color: 4
Size: 250 Color: 6

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 14
Size: 267 Color: 11
Size: 260 Color: 9

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 8
Size: 355 Color: 0
Size: 284 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 303 Color: 18
Size: 292 Color: 17

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 271 Color: 14
Size: 259 Color: 17

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 7
Size: 292 Color: 4
Size: 258 Color: 11

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 13
Size: 297 Color: 3
Size: 293 Color: 2

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 359 Color: 11
Size: 264 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 321 Color: 6
Size: 275 Color: 9

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 354 Color: 19
Size: 252 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 9
Size: 319 Color: 8
Size: 299 Color: 14

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 367 Color: 5
Size: 254 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 19
Size: 372 Color: 13
Size: 257 Color: 12

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 362 Color: 18
Size: 255 Color: 17

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 11
Size: 325 Color: 12
Size: 299 Color: 7

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 16
Size: 304 Color: 19
Size: 281 Color: 10

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 5
Size: 328 Color: 19
Size: 287 Color: 17

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 320 Color: 6
Size: 308 Color: 16

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 7
Size: 291 Color: 7
Size: 255 Color: 5

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 0
Size: 352 Color: 17
Size: 287 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 321 Color: 0
Size: 283 Color: 15

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 12
Size: 343 Color: 14
Size: 313 Color: 16

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 314 Color: 2
Size: 283 Color: 5

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 273 Color: 17
Size: 264 Color: 12

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 6
Size: 338 Color: 17
Size: 290 Color: 5

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 14
Size: 328 Color: 3
Size: 309 Color: 11

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 15
Size: 321 Color: 15
Size: 257 Color: 6

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 15
Size: 335 Color: 7
Size: 331 Color: 8

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 353 Color: 9
Size: 283 Color: 7

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 14
Size: 323 Color: 16
Size: 298 Color: 6

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 327 Color: 15
Size: 279 Color: 9

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 11
Size: 357 Color: 9
Size: 272 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 305 Color: 6
Size: 290 Color: 8

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9
Size: 289 Color: 12
Size: 256 Color: 11

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 262 Color: 15
Size: 256 Color: 16

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 8
Size: 301 Color: 1
Size: 279 Color: 11

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 12
Size: 345 Color: 5
Size: 299 Color: 18

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 17
Size: 296 Color: 4
Size: 262 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 5
Size: 345 Color: 16
Size: 284 Color: 3

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 12
Size: 290 Color: 5
Size: 258 Color: 12

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 325 Color: 12
Size: 296 Color: 8

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 12
Size: 296 Color: 13
Size: 296 Color: 12

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 15
Size: 255 Color: 4
Size: 254 Color: 13

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 14
Size: 278 Color: 2
Size: 275 Color: 18

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 8
Size: 300 Color: 2
Size: 253 Color: 14

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 19
Size: 340 Color: 10
Size: 309 Color: 13

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 346 Color: 9
Size: 256 Color: 16

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 333 Color: 8
Size: 259 Color: 18

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 343 Color: 13
Size: 303 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 5
Size: 284 Color: 7
Size: 279 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 16
Size: 257 Color: 12
Size: 253 Color: 3

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 19
Size: 286 Color: 3
Size: 270 Color: 19

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 10
Size: 324 Color: 16
Size: 272 Color: 4

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 15
Size: 341 Color: 1
Size: 307 Color: 2

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 7
Size: 311 Color: 13
Size: 269 Color: 10

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 13
Size: 288 Color: 19
Size: 257 Color: 12

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 320 Color: 7
Size: 270 Color: 7

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 18
Size: 330 Color: 18
Size: 327 Color: 15

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 18
Size: 318 Color: 6
Size: 307 Color: 12

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 16
Size: 325 Color: 18
Size: 314 Color: 15

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 328 Color: 11
Size: 280 Color: 12

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 339 Color: 14
Size: 265 Color: 7

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 347 Color: 14
Size: 291 Color: 16

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 5
Size: 305 Color: 3
Size: 301 Color: 6

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 273 Color: 12
Size: 252 Color: 16

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 19
Size: 268 Color: 2
Size: 257 Color: 13

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 17
Size: 311 Color: 5
Size: 262 Color: 13

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 9
Size: 311 Color: 3
Size: 250 Color: 11

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 19
Size: 256 Color: 2
Size: 250 Color: 18

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 11
Size: 311 Color: 11
Size: 263 Color: 7

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 5
Size: 343 Color: 7
Size: 256 Color: 7

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 13
Size: 331 Color: 3
Size: 295 Color: 19

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 294 Color: 7
Size: 255 Color: 9

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 13
Size: 296 Color: 12
Size: 273 Color: 13

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 5
Size: 341 Color: 14
Size: 255 Color: 6

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 19
Size: 309 Color: 18
Size: 263 Color: 5

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 8
Size: 273 Color: 9
Size: 250 Color: 18

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 12
Size: 334 Color: 12
Size: 273 Color: 4

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 11
Size: 296 Color: 10
Size: 255 Color: 19

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 4
Size: 345 Color: 0
Size: 261 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 342 Color: 14
Size: 260 Color: 12

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 326 Color: 15
Size: 313 Color: 18

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 18
Size: 358 Color: 16
Size: 283 Color: 19

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 12
Size: 323 Color: 9
Size: 296 Color: 8

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 349 Color: 16
Size: 255 Color: 14

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 13
Size: 261 Color: 1
Size: 258 Color: 2

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 291 Color: 17
Size: 263 Color: 5

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 305 Color: 0
Size: 286 Color: 4

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 282 Color: 19
Size: 269 Color: 16

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 16
Size: 302 Color: 11
Size: 285 Color: 16

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 368 Color: 0
Size: 263 Color: 15

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 9
Size: 319 Color: 19
Size: 289 Color: 13

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 13
Size: 264 Color: 8
Size: 261 Color: 14

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 19
Size: 300 Color: 19
Size: 253 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 10
Size: 272 Color: 8
Size: 250 Color: 10

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 16
Size: 315 Color: 7
Size: 293 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 6
Size: 308 Color: 5
Size: 296 Color: 3

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 7
Size: 301 Color: 18
Size: 261 Color: 5

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 17
Size: 342 Color: 0
Size: 290 Color: 3

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 347 Color: 5
Size: 277 Color: 6

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 14
Size: 312 Color: 3
Size: 279 Color: 13

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 16
Size: 284 Color: 12
Size: 263 Color: 10

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 368 Color: 11
Size: 260 Color: 5

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 305 Color: 10
Size: 305 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 253 Color: 7
Size: 252 Color: 5

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 7
Size: 265 Color: 9
Size: 253 Color: 17

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 327 Color: 17
Size: 270 Color: 13

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 5
Size: 279 Color: 14
Size: 276 Color: 18

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 10
Size: 341 Color: 12
Size: 310 Color: 3

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 16
Size: 338 Color: 12
Size: 260 Color: 6

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 324 Color: 12
Size: 301 Color: 11

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 9
Size: 289 Color: 15
Size: 269 Color: 18

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7
Size: 343 Color: 17
Size: 274 Color: 13

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 7
Size: 267 Color: 2
Size: 256 Color: 3

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7
Size: 365 Color: 16
Size: 250 Color: 7

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 9
Size: 328 Color: 18
Size: 253 Color: 18

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 6
Size: 252 Color: 6
Size: 251 Color: 17

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 312 Color: 2
Size: 306 Color: 3

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 12
Size: 280 Color: 15
Size: 280 Color: 9

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 18
Size: 293 Color: 16
Size: 284 Color: 17

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 16
Size: 320 Color: 7
Size: 296 Color: 19

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 16
Size: 273 Color: 16
Size: 254 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 6
Size: 316 Color: 15
Size: 261 Color: 17

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 13
Size: 283 Color: 5
Size: 268 Color: 18

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 17
Size: 316 Color: 0
Size: 312 Color: 2

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 5
Size: 264 Color: 16
Size: 261 Color: 10

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 295 Color: 19
Size: 282 Color: 11

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 342 Color: 7
Size: 269 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 253 Color: 11
Size: 250 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 15
Size: 308 Color: 13
Size: 305 Color: 2

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 9
Size: 300 Color: 1
Size: 272 Color: 13

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 255 Color: 13
Size: 252 Color: 7

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7
Size: 352 Color: 1
Size: 255 Color: 19

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 5
Size: 306 Color: 1
Size: 254 Color: 10

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 336 Color: 10
Size: 296 Color: 5

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 16
Size: 365 Color: 2
Size: 268 Color: 9

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 300 Color: 10
Size: 274 Color: 19

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 290 Color: 4
Size: 279 Color: 5

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8
Size: 338 Color: 19
Size: 254 Color: 8

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 289 Color: 12
Size: 257 Color: 9

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 17
Size: 251 Color: 17
Size: 250 Color: 15

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 323 Color: 3
Size: 262 Color: 2

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 11
Size: 296 Color: 10
Size: 262 Color: 17

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 13
Size: 337 Color: 4
Size: 320 Color: 7

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 17
Size: 350 Color: 4
Size: 275 Color: 12

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 305 Color: 5
Size: 282 Color: 15

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 4
Size: 341 Color: 3
Size: 315 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 323 Color: 0
Size: 291 Color: 5

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 270 Color: 1
Size: 264 Color: 12

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 314 Color: 13
Size: 306 Color: 9

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 12
Size: 299 Color: 10
Size: 251 Color: 4

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 8
Size: 291 Color: 16
Size: 282 Color: 6

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 3
Size: 331 Color: 8
Size: 272 Color: 10

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 365 Color: 17
Size: 264 Color: 8

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 6
Size: 331 Color: 2
Size: 323 Color: 11

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9
Size: 280 Color: 9
Size: 250 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 288 Color: 7
Size: 266 Color: 6

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 17
Size: 330 Color: 3
Size: 293 Color: 19

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 17
Size: 311 Color: 8
Size: 283 Color: 8

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 6
Size: 326 Color: 9
Size: 282 Color: 6

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 16
Size: 345 Color: 6
Size: 310 Color: 4

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 336 Color: 5
Size: 272 Color: 10

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 11
Size: 262 Color: 15
Size: 253 Color: 7

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 288 Color: 16
Size: 261 Color: 1

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 13
Size: 255 Color: 3
Size: 253 Color: 6

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 3
Size: 295 Color: 8
Size: 262 Color: 2

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 9
Size: 330 Color: 6
Size: 274 Color: 10

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 265 Color: 5
Size: 253 Color: 10

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 14
Size: 285 Color: 4
Size: 267 Color: 4

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 270 Color: 6
Size: 251 Color: 5

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 13
Size: 291 Color: 8
Size: 285 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8
Size: 289 Color: 19
Size: 277 Color: 9

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 14
Size: 260 Color: 5
Size: 256 Color: 8

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 4
Size: 363 Color: 18
Size: 259 Color: 3

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 258 Color: 6
Size: 253 Color: 14

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 322 Color: 5
Size: 275 Color: 17

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 19
Size: 256 Color: 6
Size: 252 Color: 13

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 12
Size: 268 Color: 16
Size: 251 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 10
Size: 334 Color: 0
Size: 264 Color: 16

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8
Size: 304 Color: 18
Size: 258 Color: 10

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 341 Color: 5
Size: 259 Color: 11

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 5
Size: 343 Color: 3
Size: 256 Color: 11

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 19
Size: 311 Color: 13
Size: 250 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 14
Size: 310 Color: 7
Size: 274 Color: 16

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 13
Size: 306 Color: 0
Size: 300 Color: 13

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 15
Size: 372 Color: 1
Size: 251 Color: 7

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 18
Size: 254 Color: 6
Size: 253 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 338 Color: 1
Size: 310 Color: 2

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 7
Size: 275 Color: 17
Size: 261 Color: 5

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 4
Size: 272 Color: 16
Size: 268 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 265 Color: 17
Size: 252 Color: 5

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 8
Size: 330 Color: 9
Size: 281 Color: 9

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 16
Size: 274 Color: 1
Size: 260 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 16
Size: 319 Color: 11
Size: 257 Color: 12

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 18
Size: 326 Color: 19
Size: 259 Color: 4

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 16
Size: 315 Color: 13
Size: 294 Color: 9

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 19
Size: 291 Color: 19
Size: 283 Color: 6

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 350 Color: 8
Size: 296 Color: 4

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 355 Color: 15
Size: 251 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 4
Size: 320 Color: 6
Size: 265 Color: 3

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 6
Size: 315 Color: 16
Size: 260 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 11
Size: 349 Color: 5
Size: 290 Color: 9

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 18
Size: 320 Color: 10
Size: 272 Color: 18

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 322 Color: 7
Size: 253 Color: 11

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 15
Size: 302 Color: 14
Size: 294 Color: 2

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 5
Size: 253 Color: 17
Size: 253 Color: 17

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 326 Color: 17
Size: 281 Color: 12

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 281 Color: 6
Size: 267 Color: 7

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 7
Size: 331 Color: 7
Size: 312 Color: 3

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 16
Size: 319 Color: 4
Size: 254 Color: 19

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 19
Size: 309 Color: 8
Size: 264 Color: 9

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 5
Size: 296 Color: 15
Size: 269 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 19
Size: 306 Color: 11
Size: 289 Color: 18

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 18
Size: 347 Color: 3
Size: 285 Color: 18

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 14
Size: 313 Color: 8
Size: 255 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 14
Size: 269 Color: 16
Size: 264 Color: 10

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 337 Color: 11
Size: 272 Color: 8

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 7
Size: 349 Color: 14
Size: 300 Color: 17

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 8
Size: 261 Color: 9
Size: 257 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 15
Size: 330 Color: 18
Size: 319 Color: 14

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 10
Size: 271 Color: 7
Size: 251 Color: 3

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 0
Size: 276 Color: 1
Size: 269 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 2
Size: 277 Color: 19
Size: 265 Color: 4

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 333 Color: 12
Size: 280 Color: 11

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 19
Size: 264 Color: 4
Size: 262 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 269 Color: 7
Size: 251 Color: 18

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 11
Size: 326 Color: 19
Size: 307 Color: 16

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 14
Size: 252 Color: 17
Size: 251 Color: 11

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 317 Color: 9
Size: 276 Color: 1

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 12
Size: 287 Color: 14
Size: 259 Color: 1

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 348 Color: 0
Size: 281 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 13
Size: 369 Color: 18
Size: 252 Color: 6

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 16
Size: 339 Color: 11
Size: 253 Color: 4

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 6
Size: 270 Color: 2
Size: 253 Color: 8

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 18
Size: 366 Color: 1
Size: 258 Color: 10

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 17
Size: 271 Color: 7
Size: 269 Color: 6

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 290 Color: 4
Size: 289 Color: 17

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 266 Color: 2
Size: 251 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 12
Size: 333 Color: 13
Size: 282 Color: 5

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 338 Color: 2
Size: 294 Color: 1

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 6
Size: 310 Color: 1
Size: 309 Color: 16

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 17
Size: 305 Color: 14
Size: 303 Color: 3

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 328 Color: 3
Size: 263 Color: 13

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 18
Size: 276 Color: 4
Size: 273 Color: 13

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 7
Size: 283 Color: 8
Size: 266 Color: 2

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 18
Size: 272 Color: 15
Size: 267 Color: 16

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 290 Color: 3
Size: 275 Color: 9

Bin 367: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 3
Size: 250 Color: 15
Size: 250 Color: 11
Size: 250 Color: 2

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 6
Size: 328 Color: 4
Size: 327 Color: 13

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 10
Size: 286 Color: 11
Size: 274 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 18
Size: 297 Color: 0
Size: 270 Color: 19

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 19
Size: 318 Color: 19
Size: 255 Color: 10

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 11
Size: 349 Color: 18
Size: 265 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 315 Color: 6
Size: 288 Color: 12

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 12
Size: 258 Color: 9

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 340 Color: 7
Size: 280 Color: 5

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 329 Color: 19
Size: 258 Color: 10

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 8
Size: 331 Color: 4
Size: 318 Color: 2

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 14
Size: 315 Color: 16
Size: 298 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 19
Size: 282 Color: 5
Size: 275 Color: 16

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 327 Color: 14
Size: 260 Color: 12

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 14
Size: 279 Color: 8
Size: 255 Color: 6

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 10
Size: 337 Color: 18
Size: 290 Color: 8

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 7
Size: 329 Color: 5
Size: 250 Color: 19

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 15
Size: 257 Color: 5
Size: 256 Color: 10

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 8
Size: 335 Color: 15
Size: 300 Color: 2

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 2
Size: 281 Color: 14
Size: 250 Color: 11

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 10
Size: 283 Color: 8
Size: 270 Color: 16

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8
Size: 287 Color: 9
Size: 282 Color: 5

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 4
Size: 298 Color: 10
Size: 256 Color: 16

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 2
Size: 312 Color: 9
Size: 271 Color: 18

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 17
Size: 349 Color: 10
Size: 284 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 9
Size: 368 Color: 6
Size: 257 Color: 10

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 8
Size: 331 Color: 11
Size: 328 Color: 13

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 2
Size: 300 Color: 9
Size: 280 Color: 18

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 11
Size: 307 Color: 16
Size: 288 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 358 Color: 3
Size: 257 Color: 4

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 297 Color: 2
Size: 278 Color: 6

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 369 Color: 7
Size: 250 Color: 8

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 17
Size: 341 Color: 6
Size: 296 Color: 11

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 335 Color: 16
Size: 257 Color: 12

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 6
Size: 352 Color: 19
Size: 274 Color: 17

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 310 Color: 6
Size: 253 Color: 18

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 4
Size: 335 Color: 3
Size: 331 Color: 10

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 317 Color: 14
Size: 286 Color: 4

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 16
Size: 338 Color: 11
Size: 303 Color: 9

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 19
Size: 317 Color: 0
Size: 276 Color: 15

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 337 Color: 15
Size: 299 Color: 7

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8
Size: 307 Color: 1
Size: 281 Color: 17

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 12
Size: 309 Color: 0
Size: 296 Color: 6

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 15
Size: 335 Color: 16
Size: 311 Color: 10

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 265 Color: 15
Size: 251 Color: 8

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 317 Color: 11
Size: 296 Color: 9

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 0
Size: 296 Color: 3
Size: 260 Color: 17

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 6
Size: 354 Color: 7
Size: 272 Color: 6

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 339 Color: 2
Size: 266 Color: 19

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 19
Size: 293 Color: 18
Size: 260 Color: 9

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 17
Size: 301 Color: 17
Size: 268 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 2
Size: 306 Color: 1
Size: 252 Color: 7

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 263 Color: 8
Size: 258 Color: 18

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 317 Color: 4
Size: 308 Color: 12

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 6
Size: 296 Color: 15
Size: 263 Color: 4

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8
Size: 288 Color: 2
Size: 278 Color: 7

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 11
Size: 286 Color: 11
Size: 258 Color: 2

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 5
Size: 338 Color: 13
Size: 255 Color: 8

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 5
Size: 352 Color: 7
Size: 293 Color: 12

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 283 Color: 0
Size: 273 Color: 18

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 8
Size: 251 Color: 2
Size: 250 Color: 9

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 331 Color: 11
Size: 266 Color: 1

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 4
Size: 332 Color: 3
Size: 312 Color: 7

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 15
Size: 301 Color: 14
Size: 290 Color: 5

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 17
Size: 354 Color: 3
Size: 251 Color: 12

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 345 Color: 19
Size: 264 Color: 7

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 17
Size: 289 Color: 2
Size: 256 Color: 16

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 17
Size: 326 Color: 14
Size: 264 Color: 10

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 349 Color: 18
Size: 276 Color: 14

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 10
Size: 328 Color: 19
Size: 273 Color: 2

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 15
Size: 317 Color: 7
Size: 264 Color: 19

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 331 Color: 7
Size: 280 Color: 15

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 339 Color: 7
Size: 267 Color: 5

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 365 Color: 17
Size: 258 Color: 8

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 318 Color: 17
Size: 302 Color: 6

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 297 Color: 11
Size: 292 Color: 16

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 289 Color: 15
Size: 252 Color: 5

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 17
Size: 331 Color: 2
Size: 329 Color: 19

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 329 Color: 1
Size: 290 Color: 1

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 9
Size: 333 Color: 8
Size: 302 Color: 4

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 16
Size: 268 Color: 15
Size: 255 Color: 8

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 13
Size: 359 Color: 18
Size: 278 Color: 13

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 18
Size: 342 Color: 18
Size: 313 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 7
Size: 334 Color: 9
Size: 275 Color: 11

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 18
Size: 350 Color: 0
Size: 283 Color: 3

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 5
Size: 333 Color: 17
Size: 327 Color: 4

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 10
Size: 324 Color: 5
Size: 283 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 17
Size: 300 Color: 7
Size: 288 Color: 4

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 283 Color: 6
Size: 264 Color: 12

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 19
Size: 272 Color: 11
Size: 264 Color: 3

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 324 Color: 0
Size: 265 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 19
Size: 277 Color: 10
Size: 261 Color: 1

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 11
Size: 334 Color: 14
Size: 324 Color: 12

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 13
Size: 306 Color: 5
Size: 274 Color: 2

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 345 Color: 15
Size: 276 Color: 19

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 11
Size: 269 Color: 15
Size: 260 Color: 6

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 19
Size: 279 Color: 13
Size: 266 Color: 18

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 13
Size: 269 Color: 14
Size: 263 Color: 19

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 338 Color: 9
Size: 292 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 14
Size: 272 Color: 15
Size: 260 Color: 15

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 272 Color: 2
Size: 260 Color: 6

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 15
Size: 346 Color: 12
Size: 289 Color: 16

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 15
Size: 359 Color: 10
Size: 278 Color: 19

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 12
Size: 319 Color: 18
Size: 304 Color: 1

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 362 Color: 18
Size: 251 Color: 7

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1
Size: 332 Color: 5
Size: 323 Color: 7

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 15
Size: 355 Color: 13
Size: 281 Color: 2

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 296 Color: 16
Size: 260 Color: 17

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 9
Size: 325 Color: 8
Size: 319 Color: 7

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 15
Size: 295 Color: 8
Size: 267 Color: 7

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 13
Size: 353 Color: 13
Size: 253 Color: 7

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 10
Size: 296 Color: 0
Size: 273 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 12
Size: 312 Color: 16
Size: 256 Color: 8

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 272 Color: 4
Size: 254 Color: 18

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 5
Size: 358 Color: 7
Size: 270 Color: 14

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 301 Color: 1
Size: 253 Color: 12

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 12
Size: 333 Color: 9
Size: 329 Color: 17

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 4
Size: 308 Color: 11
Size: 306 Color: 18

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 7
Size: 337 Color: 9
Size: 307 Color: 18

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 18
Size: 349 Color: 6
Size: 289 Color: 4

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 259 Color: 5
Size: 252 Color: 4

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 19
Size: 285 Color: 3
Size: 252 Color: 13

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 3
Size: 293 Color: 19
Size: 258 Color: 9

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 313 Color: 12
Size: 312 Color: 3

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 5
Size: 337 Color: 4
Size: 263 Color: 17

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 14
Size: 289 Color: 18
Size: 250 Color: 10

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 9
Size: 251 Color: 18
Size: 250 Color: 17

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 12
Size: 310 Color: 16
Size: 297 Color: 18

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 10
Size: 329 Color: 12
Size: 272 Color: 6

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 11
Size: 341 Color: 2
Size: 252 Color: 9

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 9
Size: 360 Color: 5
Size: 259 Color: 16

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 16
Size: 303 Color: 19
Size: 251 Color: 4

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 19
Size: 300 Color: 4
Size: 252 Color: 6

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 342 Color: 2
Size: 285 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 18
Size: 331 Color: 19
Size: 255 Color: 10

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 19
Size: 352 Color: 7
Size: 297 Color: 9

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 257 Color: 11
Size: 251 Color: 7

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 18
Size: 350 Color: 6
Size: 278 Color: 2

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 7
Size: 256 Color: 15
Size: 254 Color: 2

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 371 Color: 9
Size: 254 Color: 11

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 15
Size: 307 Color: 4
Size: 277 Color: 11

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 17
Size: 338 Color: 5
Size: 319 Color: 11

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 8
Size: 260 Color: 9
Size: 255 Color: 18

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 6
Size: 251 Color: 7
Size: 251 Color: 2

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 5
Size: 342 Color: 1
Size: 256 Color: 12

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 6
Size: 292 Color: 14
Size: 257 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 11
Size: 348 Color: 17
Size: 250 Color: 7

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 19
Size: 305 Color: 1
Size: 259 Color: 7

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 18
Size: 297 Color: 15
Size: 252 Color: 6

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 8
Size: 321 Color: 14
Size: 266 Color: 15

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 15
Size: 287 Color: 17
Size: 264 Color: 13

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 15
Size: 342 Color: 19
Size: 302 Color: 14

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 342 Color: 0
Size: 263 Color: 9

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 9
Size: 341 Color: 11
Size: 288 Color: 19

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 14
Size: 282 Color: 9
Size: 272 Color: 17

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 8
Size: 267 Color: 9
Size: 266 Color: 15

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 270 Color: 9
Size: 267 Color: 4

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 328 Color: 4
Size: 259 Color: 8

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 8
Size: 282 Color: 3
Size: 257 Color: 8

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 15
Size: 343 Color: 4
Size: 309 Color: 15

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 339 Color: 0
Size: 272 Color: 9

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 8
Size: 348 Color: 6
Size: 304 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 280 Color: 11
Size: 264 Color: 19

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 339 Color: 14
Size: 281 Color: 15

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 16
Size: 299 Color: 3
Size: 288 Color: 12

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 2
Size: 324 Color: 1
Size: 315 Color: 18

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 341 Color: 12
Size: 275 Color: 5

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 330 Color: 15
Size: 260 Color: 11

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 15
Size: 296 Color: 13
Size: 292 Color: 7

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 276 Color: 15
Size: 271 Color: 14

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 15
Size: 286 Color: 11
Size: 282 Color: 8

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 10
Size: 356 Color: 5
Size: 251 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 19
Size: 334 Color: 16
Size: 291 Color: 2

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 11
Size: 324 Color: 18
Size: 303 Color: 11

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 12
Size: 264 Color: 13
Size: 261 Color: 8

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 10
Size: 280 Color: 9
Size: 265 Color: 10

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 15
Size: 299 Color: 11
Size: 261 Color: 17

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 6
Size: 285 Color: 5
Size: 257 Color: 13

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 279 Color: 3
Size: 258 Color: 3

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 294 Color: 7
Size: 262 Color: 5

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 2
Size: 500 Color: 3

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 322 Color: 3
Size: 309 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 5
Size: 280 Color: 14
Size: 263 Color: 8

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 17
Size: 323 Color: 4
Size: 322 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9
Size: 284 Color: 4
Size: 252 Color: 4

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 301 Color: 3
Size: 290 Color: 15

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 305 Color: 5
Size: 255 Color: 12

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 5
Size: 326 Color: 3
Size: 317 Color: 8

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 316 Color: 4
Size: 292 Color: 6

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 367 Color: 17
Size: 263 Color: 13

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 17
Size: 339 Color: 1
Size: 264 Color: 4

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 308 Color: 6
Size: 283 Color: 15

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 15
Size: 327 Color: 6
Size: 276 Color: 2

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 19
Size: 339 Color: 0
Size: 307 Color: 4

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 8
Size: 327 Color: 16
Size: 325 Color: 10

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 290 Color: 12
Size: 253 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 15
Size: 325 Color: 5
Size: 316 Color: 8

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 272 Color: 0
Size: 263 Color: 3

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 294 Color: 6
Size: 290 Color: 13

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 5
Size: 288 Color: 12
Size: 251 Color: 15

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 18
Size: 330 Color: 4
Size: 329 Color: 13

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 331 Color: 8
Size: 256 Color: 5

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 314 Color: 3
Size: 312 Color: 3

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 314 Color: 14
Size: 261 Color: 13

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 15
Size: 349 Color: 19
Size: 281 Color: 14

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 261 Color: 4
Size: 255 Color: 9

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 355 Color: 15
Size: 252 Color: 12

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 17
Size: 281 Color: 17
Size: 271 Color: 19

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 11
Size: 329 Color: 17
Size: 300 Color: 5

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 11
Size: 312 Color: 10
Size: 254 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 11
Size: 268 Color: 18
Size: 259 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 13
Size: 302 Color: 17
Size: 258 Color: 5

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 5
Size: 321 Color: 12
Size: 269 Color: 12

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 16
Size: 346 Color: 4
Size: 251 Color: 19

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 5
Size: 306 Color: 9
Size: 302 Color: 9

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 11
Size: 338 Color: 9
Size: 278 Color: 4

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 297 Color: 8
Size: 281 Color: 7

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 314 Color: 19
Size: 252 Color: 7

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 322 Color: 4
Size: 265 Color: 1

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 15
Size: 319 Color: 6
Size: 274 Color: 4

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 9
Size: 307 Color: 1
Size: 305 Color: 19

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 5
Size: 311 Color: 4
Size: 282 Color: 3

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 12
Size: 311 Color: 17
Size: 257 Color: 9

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 6
Size: 324 Color: 3
Size: 277 Color: 14

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 12
Size: 349 Color: 5
Size: 267 Color: 2

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 312 Color: 15
Size: 258 Color: 15

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 331 Color: 8
Size: 285 Color: 7

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 299 Color: 1
Size: 257 Color: 3

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 15
Size: 265 Color: 12
Size: 253 Color: 12

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 13
Size: 346 Color: 13
Size: 268 Color: 19

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 5
Size: 267 Color: 0
Size: 266 Color: 5

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 5
Size: 295 Color: 10
Size: 269 Color: 18

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 5
Size: 364 Color: 15
Size: 252 Color: 6

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 3
Size: 334 Color: 19
Size: 278 Color: 3

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 13
Size: 327 Color: 15
Size: 289 Color: 11

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 6
Size: 332 Color: 11
Size: 267 Color: 18

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 363 Color: 15
Size: 262 Color: 19

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 3
Size: 304 Color: 9
Size: 250 Color: 4

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 16
Size: 327 Color: 1
Size: 280 Color: 5

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 329 Color: 0
Size: 309 Color: 17

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 15
Size: 262 Color: 4
Size: 253 Color: 12

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 15
Size: 293 Color: 6
Size: 258 Color: 8

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 10
Size: 368 Color: 1
Size: 251 Color: 7

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 8
Size: 334 Color: 15
Size: 324 Color: 9

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 13
Size: 257 Color: 17
Size: 256 Color: 18

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 14
Size: 329 Color: 11
Size: 259 Color: 9

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 14
Size: 325 Color: 15
Size: 292 Color: 18

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 19
Size: 311 Color: 14
Size: 292 Color: 19

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 17
Size: 275 Color: 0
Size: 267 Color: 19

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 18
Size: 294 Color: 9
Size: 259 Color: 15

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 15
Size: 269 Color: 6
Size: 261 Color: 14

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 17
Size: 251 Color: 3
Size: 250 Color: 12

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 356 Color: 15
Size: 266 Color: 17

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 19
Size: 266 Color: 19
Size: 260 Color: 6

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 356 Color: 11
Size: 253 Color: 3

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 307 Color: 1
Size: 260 Color: 5

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 319 Color: 2
Size: 294 Color: 7

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 3
Size: 317 Color: 2
Size: 258 Color: 18

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 18
Size: 336 Color: 9
Size: 253 Color: 1

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 348 Color: 5
Size: 283 Color: 1

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 293 Color: 4
Size: 251 Color: 6

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 14
Size: 252 Color: 6
Size: 250 Color: 17

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 7
Size: 285 Color: 13
Size: 279 Color: 9

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 10
Size: 305 Color: 18
Size: 250 Color: 18

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 15
Size: 319 Color: 8
Size: 318 Color: 7

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 6
Size: 369 Color: 5
Size: 256 Color: 2

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 349 Color: 7
Size: 255 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 282 Color: 1
Size: 265 Color: 3

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 19
Size: 329 Color: 0
Size: 250 Color: 12

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 14
Size: 281 Color: 10
Size: 277 Color: 8

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 10
Size: 360 Color: 1
Size: 260 Color: 8

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 9
Size: 319 Color: 9
Size: 298 Color: 17

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 5
Size: 347 Color: 1
Size: 281 Color: 8

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 3
Size: 298 Color: 6
Size: 257 Color: 14

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 13
Size: 322 Color: 3
Size: 259 Color: 4

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 16
Size: 335 Color: 14
Size: 301 Color: 14

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 11
Size: 349 Color: 16
Size: 251 Color: 10

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 2
Size: 331 Color: 2
Size: 314 Color: 19

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 14
Size: 299 Color: 6
Size: 286 Color: 10

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 347 Color: 13
Size: 290 Color: 15

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 16
Size: 304 Color: 1
Size: 262 Color: 8

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 305 Color: 8
Size: 296 Color: 14

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 16
Size: 334 Color: 4
Size: 306 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 7
Size: 344 Color: 14
Size: 291 Color: 11

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 328 Color: 11
Size: 276 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 4
Size: 284 Color: 16
Size: 257 Color: 17

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 5
Size: 332 Color: 7
Size: 257 Color: 5

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 14
Size: 336 Color: 14
Size: 297 Color: 7

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 5
Size: 321 Color: 2
Size: 253 Color: 15

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 16
Size: 304 Color: 19
Size: 260 Color: 17

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 285 Color: 11
Size: 250 Color: 2

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 10
Size: 320 Color: 13
Size: 310 Color: 16

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 251 Color: 18
Size: 251 Color: 10

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 271 Color: 17
Size: 258 Color: 18

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 11
Size: 342 Color: 18
Size: 263 Color: 2

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 13
Size: 332 Color: 8
Size: 319 Color: 15

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 13
Size: 309 Color: 6
Size: 297 Color: 10

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 5
Size: 281 Color: 11
Size: 276 Color: 4

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 17
Size: 351 Color: 14
Size: 253 Color: 12

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 9
Size: 304 Color: 14
Size: 298 Color: 8

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 0
Size: 325 Color: 16
Size: 324 Color: 0

Total size: 667667
Total free space: 0

