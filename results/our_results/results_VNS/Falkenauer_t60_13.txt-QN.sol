Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 56
Size: 261 Color: 15
Size: 251 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 53
Size: 286 Color: 23
Size: 255 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 49
Size: 296 Color: 30
Size: 260 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 288 Color: 25
Size: 278 Color: 19
Size: 434 Color: 48

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 43
Size: 323 Color: 35
Size: 296 Color: 28

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 54
Size: 262 Color: 16
Size: 258 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 52
Size: 287 Color: 24
Size: 257 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 59
Size: 254 Color: 5
Size: 251 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 58
Size: 254 Color: 6
Size: 253 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 42
Size: 340 Color: 37
Size: 291 Color: 26

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 55
Size: 259 Color: 13
Size: 256 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 57
Size: 258 Color: 11
Size: 250 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 46
Size: 299 Color: 32
Size: 280 Color: 21

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 39
Size: 350 Color: 38
Size: 297 Color: 31

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 44
Size: 361 Color: 41
Size: 253 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 50
Size: 296 Color: 29
Size: 256 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 47
Size: 293 Color: 27
Size: 278 Color: 20

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 45
Size: 317 Color: 34
Size: 264 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 40
Size: 327 Color: 36
Size: 317 Color: 33

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 51
Size: 281 Color: 22
Size: 267 Color: 18

Total size: 20000
Total free space: 0

