Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391330 Color: 78
Size: 262410 Color: 13
Size: 346261 Color: 63

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 394461 Color: 82
Size: 267749 Color: 19
Size: 337791 Color: 57

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 412145 Color: 85
Size: 298986 Color: 44
Size: 288870 Color: 40

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 250529 Color: 0
Size: 275216 Color: 27
Size: 474256 Color: 96

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 435684 Color: 88
Size: 257815 Color: 9
Size: 306502 Color: 47

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 289586 Color: 41
Size: 342407 Color: 59
Size: 368008 Color: 72

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 362940 Color: 71
Size: 349426 Color: 67
Size: 287635 Color: 39

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 391520 Color: 79
Size: 346393 Color: 64
Size: 262088 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 393316 Color: 80
Size: 270594 Color: 24
Size: 336091 Color: 56

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 356273 Color: 68
Size: 386335 Color: 74
Size: 257393 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 386783 Color: 75
Size: 326595 Color: 54
Size: 286623 Color: 37

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 406003 Color: 83
Size: 268555 Color: 20
Size: 325443 Color: 53

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 311529 Color: 51
Size: 411474 Color: 84
Size: 276998 Color: 31

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 389187 Color: 77
Size: 344604 Color: 61
Size: 266210 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 394315 Color: 81
Size: 318305 Color: 52
Size: 287381 Color: 38

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 362477 Color: 70
Size: 299517 Color: 45
Size: 338007 Color: 58

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 360672 Color: 69
Size: 346177 Color: 62
Size: 293152 Color: 42

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 386954 Color: 76
Size: 348784 Color: 66
Size: 264263 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 344445 Color: 60
Size: 348403 Color: 65
Size: 307153 Color: 48

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 377804 Color: 73
Size: 327160 Color: 55
Size: 295037 Color: 43

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 416128 Color: 86
Size: 302966 Color: 46
Size: 280907 Color: 35

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 429894 Color: 87
Size: 309602 Color: 50
Size: 260505 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 437108 Color: 89
Size: 307352 Color: 49
Size: 255541 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 446720 Color: 90
Size: 278808 Color: 34
Size: 274473 Color: 26

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 446865 Color: 91
Size: 277334 Color: 32
Size: 275802 Color: 29

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 452881 Color: 92
Size: 276731 Color: 30
Size: 270389 Color: 23

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 455759 Color: 93
Size: 275362 Color: 28
Size: 268880 Color: 21

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 462048 Color: 94
Size: 280984 Color: 36
Size: 256969 Color: 7

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 463327 Color: 95
Size: 277726 Color: 33
Size: 258948 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 476433 Color: 97
Size: 269048 Color: 22
Size: 254520 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 477237 Color: 98
Size: 271020 Color: 25
Size: 251744 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 478784 Color: 99
Size: 266167 Color: 17
Size: 255050 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 480541 Color: 100
Size: 262549 Color: 14
Size: 256911 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 484905 Color: 101
Size: 263092 Color: 15
Size: 252004 Color: 2

Total size: 34000034
Total free space: 0

