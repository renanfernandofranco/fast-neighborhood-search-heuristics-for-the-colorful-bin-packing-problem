Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11602 Color: 466
Size: 3666 Color: 338
Size: 732 Color: 146

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 467
Size: 3318 Color: 328
Size: 1058 Color: 183

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12280 Color: 486
Size: 3432 Color: 329
Size: 288 Color: 23

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 492
Size: 3524 Color: 334
Size: 36 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 495
Size: 3056 Color: 315
Size: 370 Color: 60

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12738 Color: 501
Size: 2444 Color: 285
Size: 818 Color: 161

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 502
Size: 2708 Color: 299
Size: 552 Color: 119

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 513
Size: 2722 Color: 301
Size: 312 Color: 36

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12976 Color: 514
Size: 2728 Color: 303
Size: 296 Color: 29

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 519
Size: 2450 Color: 286
Size: 490 Color: 98

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13101 Color: 522
Size: 2291 Color: 277
Size: 608 Color: 125

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 524
Size: 2530 Color: 292
Size: 312 Color: 34

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 528
Size: 1772 Color: 236
Size: 928 Color: 171

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 529
Size: 2194 Color: 270
Size: 504 Color: 102

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 531
Size: 2192 Color: 269
Size: 472 Color: 90

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 534
Size: 2400 Color: 282
Size: 232 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 536
Size: 2252 Color: 275
Size: 360 Color: 55

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13425 Color: 538
Size: 2147 Color: 264
Size: 428 Color: 77

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 541
Size: 1988 Color: 254
Size: 498 Color: 100

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13519 Color: 542
Size: 1857 Color: 243
Size: 624 Color: 129

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 546
Size: 1942 Color: 250
Size: 440 Color: 85

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 548
Size: 2011 Color: 256
Size: 342 Color: 50

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 549
Size: 1548 Color: 215
Size: 784 Color: 155

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13714 Color: 552
Size: 1766 Color: 235
Size: 520 Color: 105

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 554
Size: 1671 Color: 226
Size: 580 Color: 121

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13773 Color: 556
Size: 1853 Color: 242
Size: 374 Color: 62

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13774 Color: 557
Size: 1858 Color: 244
Size: 368 Color: 56

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 558
Size: 1807 Color: 238
Size: 416 Color: 76

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13876 Color: 563
Size: 1692 Color: 229
Size: 432 Color: 79

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13882 Color: 564
Size: 1466 Color: 212
Size: 652 Color: 135

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13909 Color: 566
Size: 1563 Color: 217
Size: 528 Color: 109

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 568
Size: 1116 Color: 187
Size: 942 Color: 175

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 569
Size: 1152 Color: 191
Size: 904 Color: 168

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13972 Color: 572
Size: 1368 Color: 204
Size: 660 Color: 136

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 575
Size: 1490 Color: 213
Size: 488 Color: 97

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 576
Size: 1000 Color: 182
Size: 976 Color: 176

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14066 Color: 578
Size: 1386 Color: 207
Size: 548 Color: 118

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 580
Size: 1612 Color: 222
Size: 312 Color: 35

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14106 Color: 582
Size: 1394 Color: 208
Size: 500 Color: 101

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14113 Color: 583
Size: 1573 Color: 219
Size: 314 Color: 37

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 584
Size: 1444 Color: 210
Size: 438 Color: 84

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 589
Size: 1512 Color: 214
Size: 288 Color: 25

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14242 Color: 591
Size: 1582 Color: 220
Size: 176 Color: 7

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 592
Size: 936 Color: 174
Size: 816 Color: 160

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 593
Size: 1332 Color: 201
Size: 400 Color: 70

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 595
Size: 1382 Color: 206
Size: 288 Color: 24

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14338 Color: 596
Size: 898 Color: 167
Size: 764 Color: 152

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 598
Size: 1302 Color: 196
Size: 340 Color: 49

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 600
Size: 1328 Color: 197
Size: 292 Color: 27

Bin 50: 1 of cap free
Amount of items: 9
Items: 
Size: 8003 Color: 407
Size: 1332 Color: 202
Size: 1328 Color: 200
Size: 1328 Color: 198
Size: 1300 Color: 195
Size: 1280 Color: 194
Size: 692 Color: 140
Size: 368 Color: 58
Size: 368 Color: 57

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 11666 Color: 469
Size: 3469 Color: 332
Size: 864 Color: 166

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 478
Size: 3921 Color: 346

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 479
Size: 3446 Color: 331
Size: 452 Color: 87

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 12187 Color: 482
Size: 2964 Color: 311
Size: 848 Color: 164

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12347 Color: 490
Size: 3112 Color: 318
Size: 540 Color: 115

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 12444 Color: 493
Size: 3555 Color: 335

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12611 Color: 496
Size: 2452 Color: 287
Size: 936 Color: 172

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 497
Size: 3045 Color: 314
Size: 322 Color: 40

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 498
Size: 1986 Color: 253
Size: 1328 Color: 199

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12834 Color: 508
Size: 2763 Color: 306
Size: 402 Color: 71

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 515
Size: 2197 Color: 271
Size: 816 Color: 159

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 518
Size: 2170 Color: 267
Size: 776 Color: 153

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13793 Color: 560
Size: 2206 Color: 273

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13945 Color: 570
Size: 2054 Color: 259

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 587
Size: 1859 Color: 245

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 464
Size: 3434 Color: 330
Size: 984 Color: 177

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 484
Size: 3752 Color: 342

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 12514 Color: 494
Size: 3484 Color: 333

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 499
Size: 3300 Color: 327

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13062 Color: 520
Size: 2936 Color: 310

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 13274 Color: 527
Size: 2724 Color: 302

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 533
Size: 2633 Color: 296

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 577
Size: 1948 Color: 251

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 579
Size: 1930 Color: 249

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 14214 Color: 590
Size: 1784 Color: 237

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 599
Size: 1638 Color: 224

Bin 77: 3 of cap free
Amount of items: 7
Items: 
Size: 8010 Color: 410
Size: 1741 Color: 234
Size: 1736 Color: 233
Size: 1720 Color: 232
Size: 1718 Color: 231
Size: 720 Color: 144
Size: 352 Color: 51

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 526
Size: 2744 Color: 304

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 567
Size: 2069 Color: 260

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 585
Size: 1877 Color: 247

Bin 81: 4 of cap free
Amount of items: 2
Items: 
Size: 11288 Color: 458
Size: 4708 Color: 364

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 11652 Color: 468
Size: 4344 Color: 358

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 472
Size: 4092 Color: 352
Size: 124 Color: 5

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 12102 Color: 480
Size: 3894 Color: 345

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 506
Size: 3208 Color: 322

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 532
Size: 2642 Color: 297

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 13370 Color: 535
Size: 2626 Color: 295

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13980 Color: 573
Size: 2016 Color: 257

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 594
Size: 1672 Color: 227

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14346 Color: 597
Size: 1650 Color: 225

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 504
Size: 3251 Color: 325

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 543
Size: 2457 Color: 288

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 562
Size: 2162 Color: 266

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 581
Size: 1912 Color: 248

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 14125 Color: 586
Size: 1870 Color: 246

Bin 96: 6 of cap free
Amount of items: 4
Items: 
Size: 10504 Color: 443
Size: 4938 Color: 367
Size: 276 Color: 22
Size: 276 Color: 21

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 12230 Color: 483
Size: 3764 Color: 343

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 489
Size: 3656 Color: 337

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 547
Size: 2370 Color: 281

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 553
Size: 2274 Color: 276

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 588
Size: 1846 Color: 241

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 10025 Color: 434
Size: 5968 Color: 385

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 12743 Color: 503
Size: 3250 Color: 324

Bin 104: 8 of cap free
Amount of items: 3
Items: 
Size: 8197 Color: 414
Size: 7459 Color: 402
Size: 336 Color: 48

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 419
Size: 6976 Color: 401

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 9988 Color: 433
Size: 5700 Color: 381
Size: 304 Color: 31

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 11828 Color: 473
Size: 4164 Color: 354

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 476
Size: 4044 Color: 350
Size: 52 Color: 1

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 12850 Color: 510
Size: 3142 Color: 320

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 525
Size: 2754 Color: 305

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 530
Size: 2684 Color: 298

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 539
Size: 2544 Color: 293

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13900 Color: 565
Size: 2092 Color: 262

Bin 114: 9 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 424
Size: 6503 Color: 391
Size: 324 Color: 41

Bin 115: 9 of cap free
Amount of items: 3
Items: 
Size: 11266 Color: 457
Size: 4497 Color: 359
Size: 228 Color: 12

Bin 116: 9 of cap free
Amount of items: 3
Items: 
Size: 11297 Color: 459
Size: 2514 Color: 291
Size: 2180 Color: 268

Bin 117: 9 of cap free
Amount of items: 4
Items: 
Size: 11839 Color: 474
Size: 3928 Color: 347
Size: 112 Color: 4
Size: 112 Color: 3

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 12168 Color: 481
Size: 3823 Color: 344

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13587 Color: 545
Size: 2403 Color: 283

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 555
Size: 2232 Color: 274

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13954 Color: 571
Size: 2036 Color: 258

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 10897 Color: 449
Size: 5092 Color: 373

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 12890 Color: 511
Size: 3099 Color: 317

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 521
Size: 2913 Color: 309

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 13492 Color: 540
Size: 2497 Color: 290

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 574
Size: 1992 Color: 255

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 440
Size: 5336 Color: 376
Size: 296 Color: 28

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 512
Size: 3084 Color: 316

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13670 Color: 550
Size: 2318 Color: 279

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 551
Size: 2302 Color: 278

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 559
Size: 2200 Color: 272

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 11534 Color: 463
Size: 4253 Color: 357
Size: 200 Color: 9

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 561
Size: 2155 Color: 265

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 452
Size: 4950 Color: 368

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 517
Size: 2970 Color: 313

Bin 136: 15 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 465
Size: 2715 Color: 300
Size: 1684 Color: 228

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 12022 Color: 477
Size: 3962 Color: 349

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 12300 Color: 488
Size: 3684 Color: 340

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 12756 Color: 505
Size: 3228 Color: 323

Bin 140: 17 of cap free
Amount of items: 11
Items: 
Size: 8001 Color: 405
Size: 1080 Color: 184
Size: 1000 Color: 181
Size: 994 Color: 180
Size: 992 Color: 179
Size: 988 Color: 178
Size: 936 Color: 173
Size: 844 Color: 163
Size: 384 Color: 65
Size: 384 Color: 64
Size: 380 Color: 63

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 423
Size: 6513 Color: 392
Size: 328 Color: 42

Bin 142: 18 of cap free
Amount of items: 3
Items: 
Size: 9545 Color: 428
Size: 6133 Color: 389
Size: 304 Color: 33

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 11330 Color: 460
Size: 4652 Color: 363

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 485
Size: 3722 Color: 341

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 500
Size: 3270 Color: 326

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 537
Size: 2584 Color: 294

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 544
Size: 2417 Color: 284

Bin 148: 21 of cap free
Amount of items: 2
Items: 
Size: 12800 Color: 507
Size: 3179 Color: 321

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 10930 Color: 450
Size: 5048 Color: 372

Bin 150: 23 of cap free
Amount of items: 3
Items: 
Size: 10392 Color: 441
Size: 5297 Color: 375
Size: 288 Color: 26

Bin 151: 23 of cap free
Amount of items: 2
Items: 
Size: 12847 Color: 509
Size: 3130 Color: 319

Bin 152: 24 of cap free
Amount of items: 2
Items: 
Size: 8040 Color: 412
Size: 7936 Color: 404

Bin 153: 24 of cap free
Amount of items: 3
Items: 
Size: 11735 Color: 471
Size: 4101 Color: 353
Size: 140 Color: 6

Bin 154: 25 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 523
Size: 2858 Color: 307

Bin 155: 26 of cap free
Amount of items: 2
Items: 
Size: 11250 Color: 456
Size: 4724 Color: 366

Bin 156: 27 of cap free
Amount of items: 2
Items: 
Size: 9645 Color: 429
Size: 6328 Color: 390

Bin 157: 27 of cap free
Amount of items: 2
Items: 
Size: 13005 Color: 516
Size: 2968 Color: 312

Bin 158: 31 of cap free
Amount of items: 7
Items: 
Size: 8008 Color: 409
Size: 1713 Color: 230
Size: 1626 Color: 223
Size: 1604 Color: 221
Size: 1570 Color: 218
Size: 1096 Color: 186
Size: 352 Color: 52

Bin 159: 35 of cap free
Amount of items: 2
Items: 
Size: 12283 Color: 487
Size: 3682 Color: 339

Bin 160: 36 of cap free
Amount of items: 2
Items: 
Size: 9986 Color: 432
Size: 5978 Color: 386

Bin 161: 36 of cap free
Amount of items: 2
Items: 
Size: 10825 Color: 448
Size: 5139 Color: 374

Bin 162: 38 of cap free
Amount of items: 2
Items: 
Size: 12348 Color: 491
Size: 3614 Color: 336

Bin 163: 42 of cap free
Amount of items: 5
Items: 
Size: 9960 Color: 430
Size: 2488 Color: 289
Size: 2356 Color: 280
Size: 850 Color: 165
Size: 304 Color: 32

Bin 164: 50 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 462
Size: 4230 Color: 356
Size: 208 Color: 10

Bin 165: 52 of cap free
Amount of items: 3
Items: 
Size: 11668 Color: 470
Size: 4088 Color: 351
Size: 192 Color: 8

Bin 166: 54 of cap free
Amount of items: 2
Items: 
Size: 9410 Color: 425
Size: 6536 Color: 393

Bin 167: 56 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 455
Size: 4584 Color: 362
Size: 264 Color: 14

Bin 168: 65 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 451
Size: 4717 Color: 365
Size: 272 Color: 17

Bin 169: 67 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 418
Size: 6665 Color: 397
Size: 332 Color: 43

Bin 170: 79 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 427
Size: 6099 Color: 388
Size: 320 Color: 38

Bin 171: 86 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 442
Size: 5494 Color: 380

Bin 172: 86 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 454
Size: 4550 Color: 361
Size: 272 Color: 15

Bin 173: 92 of cap free
Amount of items: 3
Items: 
Size: 11866 Color: 475
Size: 3946 Color: 348
Size: 96 Color: 2

Bin 174: 100 of cap free
Amount of items: 2
Items: 
Size: 10068 Color: 436
Size: 5832 Color: 384

Bin 175: 109 of cap free
Amount of items: 3
Items: 
Size: 10605 Color: 446
Size: 5014 Color: 371
Size: 272 Color: 18

Bin 176: 109 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 453
Size: 4538 Color: 360
Size: 272 Color: 16

Bin 177: 132 of cap free
Amount of items: 24
Items: 
Size: 912 Color: 170
Size: 908 Color: 169
Size: 824 Color: 162
Size: 800 Color: 158
Size: 788 Color: 157
Size: 788 Color: 156
Size: 782 Color: 154
Size: 760 Color: 151
Size: 752 Color: 150
Size: 744 Color: 149
Size: 736 Color: 148
Size: 736 Color: 147
Size: 732 Color: 145
Size: 720 Color: 143
Size: 710 Color: 142
Size: 696 Color: 141
Size: 688 Color: 139
Size: 412 Color: 74
Size: 412 Color: 73
Size: 408 Color: 72
Size: 396 Color: 69
Size: 392 Color: 68
Size: 388 Color: 67
Size: 384 Color: 66

Bin 178: 137 of cap free
Amount of items: 3
Items: 
Size: 11413 Color: 461
Size: 4226 Color: 355
Size: 224 Color: 11

Bin 179: 142 of cap free
Amount of items: 8
Items: 
Size: 8004 Color: 408
Size: 1556 Color: 216
Size: 1464 Color: 211
Size: 1404 Color: 209
Size: 1370 Color: 205
Size: 1356 Color: 203
Size: 352 Color: 54
Size: 352 Color: 53

Bin 180: 158 of cap free
Amount of items: 3
Items: 
Size: 10558 Color: 445
Size: 5012 Color: 370
Size: 272 Color: 19

Bin 181: 159 of cap free
Amount of items: 2
Items: 
Size: 8185 Color: 413
Size: 7656 Color: 403

Bin 182: 161 of cap free
Amount of items: 2
Items: 
Size: 9131 Color: 422
Size: 6708 Color: 400

Bin 183: 166 of cap free
Amount of items: 2
Items: 
Size: 10062 Color: 435
Size: 5772 Color: 383

Bin 184: 166 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 447
Size: 2906 Color: 308
Size: 2136 Color: 263

Bin 185: 170 of cap free
Amount of items: 3
Items: 
Size: 8830 Color: 417
Size: 6664 Color: 396
Size: 336 Color: 44

Bin 186: 178 of cap free
Amount of items: 3
Items: 
Size: 9432 Color: 426
Size: 6070 Color: 387
Size: 320 Color: 39

Bin 187: 179 of cap free
Amount of items: 2
Items: 
Size: 10341 Color: 439
Size: 5480 Color: 379

Bin 188: 188 of cap free
Amount of items: 3
Items: 
Size: 8814 Color: 416
Size: 6662 Color: 395
Size: 336 Color: 45

Bin 189: 205 of cap free
Amount of items: 3
Items: 
Size: 10542 Color: 444
Size: 4981 Color: 369
Size: 272 Color: 20

Bin 190: 214 of cap free
Amount of items: 3
Items: 
Size: 10078 Color: 437
Size: 5404 Color: 377
Size: 304 Color: 30

Bin 191: 242 of cap free
Amount of items: 2
Items: 
Size: 10340 Color: 438
Size: 5418 Color: 378

Bin 192: 248 of cap free
Amount of items: 2
Items: 
Size: 9084 Color: 421
Size: 6668 Color: 399

Bin 193: 258 of cap free
Amount of items: 2
Items: 
Size: 9076 Color: 420
Size: 6666 Color: 398

Bin 194: 260 of cap free
Amount of items: 4
Items: 
Size: 8408 Color: 415
Size: 6660 Color: 394
Size: 336 Color: 47
Size: 336 Color: 46

Bin 195: 280 of cap free
Amount of items: 9
Items: 
Size: 8002 Color: 406
Size: 1264 Color: 193
Size: 1192 Color: 192
Size: 1152 Color: 190
Size: 1144 Color: 189
Size: 1136 Color: 188
Size: 1088 Color: 185
Size: 372 Color: 61
Size: 370 Color: 59

Bin 196: 293 of cap free
Amount of items: 5
Items: 
Size: 8012 Color: 411
Size: 2074 Color: 261
Size: 1961 Color: 252
Size: 1844 Color: 240
Size: 1816 Color: 239

Bin 197: 303 of cap free
Amount of items: 2
Items: 
Size: 9972 Color: 431
Size: 5725 Color: 382

Bin 198: 334 of cap free
Amount of items: 29
Items: 
Size: 688 Color: 138
Size: 672 Color: 137
Size: 648 Color: 134
Size: 648 Color: 133
Size: 634 Color: 132
Size: 628 Color: 131
Size: 624 Color: 130
Size: 618 Color: 128
Size: 616 Color: 127
Size: 608 Color: 126
Size: 592 Color: 124
Size: 592 Color: 123
Size: 584 Color: 122
Size: 568 Color: 120
Size: 544 Color: 117
Size: 542 Color: 116
Size: 480 Color: 94
Size: 480 Color: 93
Size: 480 Color: 92
Size: 476 Color: 91
Size: 460 Color: 89
Size: 456 Color: 88
Size: 448 Color: 86
Size: 436 Color: 83
Size: 432 Color: 82
Size: 432 Color: 81
Size: 432 Color: 80
Size: 432 Color: 78
Size: 416 Color: 75

Bin 199: 9266 of cap free
Amount of items: 13
Items: 
Size: 540 Color: 114
Size: 536 Color: 113
Size: 536 Color: 112
Size: 528 Color: 111
Size: 528 Color: 110
Size: 528 Color: 108
Size: 524 Color: 107
Size: 524 Color: 106
Size: 512 Color: 104
Size: 512 Color: 103
Size: 496 Color: 99
Size: 488 Color: 96
Size: 482 Color: 95

Total size: 3168000
Total free space: 16000

