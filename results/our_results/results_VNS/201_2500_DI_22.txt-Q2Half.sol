Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 810 Color: 1
Size: 718 Color: 1
Size: 316 Color: 0
Size: 144 Color: 0
Size: 48 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 1
Size: 367 Color: 1
Size: 52 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 1
Size: 441 Color: 1
Size: 88 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 1
Size: 326 Color: 1
Size: 144 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 190 Color: 1
Size: 36 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 1
Size: 666 Color: 1
Size: 132 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 1408 Color: 1
Size: 468 Color: 1
Size: 124 Color: 0
Size: 36 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1066 Color: 1
Size: 820 Color: 1
Size: 106 Color: 0
Size: 44 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1019 Color: 1
Size: 989 Color: 1
Size: 28 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1393 Color: 1
Size: 531 Color: 1
Size: 112 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 1
Size: 692 Color: 1
Size: 68 Color: 0

Bin 12: 0 of cap free
Amount of items: 5
Items: 
Size: 1475 Color: 1
Size: 262 Color: 1
Size: 187 Color: 1
Size: 56 Color: 0
Size: 56 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 1
Size: 211 Color: 1
Size: 84 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1289 Color: 1
Size: 727 Color: 1
Size: 20 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 1
Size: 743 Color: 1
Size: 126 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 1
Size: 185 Color: 1
Size: 36 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 1
Size: 442 Color: 1
Size: 80 Color: 0

Bin 19: 0 of cap free
Amount of items: 5
Items: 
Size: 1021 Color: 1
Size: 490 Color: 1
Size: 389 Color: 1
Size: 72 Color: 0
Size: 64 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 1
Size: 209 Color: 1
Size: 40 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 407 Color: 1
Size: 58 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1802 Color: 1
Size: 190 Color: 1
Size: 44 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 257 Color: 1
Size: 50 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 1
Size: 254 Color: 1
Size: 56 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 1
Size: 321 Color: 1
Size: 64 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 1
Size: 362 Color: 1
Size: 168 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 282 Color: 1
Size: 108 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 1
Size: 182 Color: 1
Size: 104 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 1
Size: 227 Color: 1
Size: 44 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 178 Color: 1
Size: 44 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 230 Color: 1
Size: 124 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 1
Size: 657 Color: 1
Size: 88 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 1
Size: 298 Color: 1
Size: 76 Color: 0

Bin 34: 0 of cap free
Amount of items: 4
Items: 
Size: 1645 Color: 1
Size: 283 Color: 1
Size: 76 Color: 0
Size: 32 Color: 0

Bin 35: 0 of cap free
Amount of items: 5
Items: 
Size: 890 Color: 1
Size: 622 Color: 1
Size: 424 Color: 1
Size: 64 Color: 0
Size: 36 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 1
Size: 538 Color: 1
Size: 48 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1664 Color: 1
Size: 316 Color: 1
Size: 56 Color: 0

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 1767 Color: 1
Size: 225 Color: 1
Size: 36 Color: 0
Size: 8 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 1
Size: 242 Color: 1
Size: 96 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 1
Size: 394 Color: 1
Size: 40 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 1
Size: 463 Color: 1
Size: 92 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 1
Size: 361 Color: 1
Size: 70 Color: 0

Bin 43: 1 of cap free
Amount of items: 5
Items: 
Size: 1023 Color: 1
Size: 537 Color: 1
Size: 247 Color: 1
Size: 168 Color: 0
Size: 60 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 1
Size: 265 Color: 1
Size: 36 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 1
Size: 198 Color: 1
Size: 52 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 1
Size: 293 Color: 1
Size: 52 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 1
Size: 224 Color: 1
Size: 48 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 1
Size: 562 Color: 1
Size: 106 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 1
Size: 569 Color: 1
Size: 76 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1620 Color: 1
Size: 290 Color: 1
Size: 124 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 1
Size: 847 Color: 1
Size: 8 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 1609 Color: 1
Size: 348 Color: 1
Size: 76 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 1
Size: 721 Color: 1
Size: 140 Color: 0

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 1
Size: 327 Color: 1
Size: 20 Color: 0

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 1
Size: 525 Color: 1
Size: 104 Color: 0

Bin 56: 9 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 1
Size: 314 Color: 1
Size: 16 Color: 0

Bin 57: 9 of cap free
Amount of items: 3
Items: 
Size: 1022 Color: 1
Size: 845 Color: 1
Size: 160 Color: 0

Bin 58: 13 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 1
Size: 541 Color: 1
Size: 88 Color: 0

Bin 59: 19 of cap free
Amount of items: 3
Items: 
Size: 1355 Color: 1
Size: 438 Color: 1
Size: 224 Color: 0

Bin 60: 24 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 407 Color: 1
Size: 56 Color: 0

Bin 61: 91 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 1
Size: 623 Color: 1
Size: 32 Color: 0

Bin 62: 210 of cap free
Amount of items: 1
Items: 
Size: 1826 Color: 1

Bin 63: 218 of cap free
Amount of items: 1
Items: 
Size: 1818 Color: 1

Bin 64: 223 of cap free
Amount of items: 1
Items: 
Size: 1813 Color: 1

Bin 65: 317 of cap free
Amount of items: 1
Items: 
Size: 1719 Color: 1

Bin 66: 871 of cap free
Amount of items: 1
Items: 
Size: 1165 Color: 1

Total size: 132340
Total free space: 2036

