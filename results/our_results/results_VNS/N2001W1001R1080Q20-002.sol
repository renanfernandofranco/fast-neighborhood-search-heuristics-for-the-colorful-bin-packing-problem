Capicity Bin: 1001
Lower Bound: 896

Bins used: 898
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 11
Size: 322 Color: 14
Size: 320 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 3
Size: 315 Color: 15
Size: 315 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 8
Size: 307 Color: 4
Size: 270 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 511 Color: 9
Size: 248 Color: 11
Size: 242 Color: 13

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 3
Size: 457 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 7
Size: 247 Color: 18
Size: 199 Color: 5

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 8
Size: 443 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 559 Color: 5
Size: 248 Color: 13
Size: 194 Color: 16

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 19
Size: 432 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 570 Color: 6
Size: 232 Color: 7
Size: 199 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 6
Size: 215 Color: 18
Size: 189 Color: 2

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 14
Size: 373 Color: 1

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 6
Size: 358 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 10
Size: 175 Color: 1
Size: 155 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 13
Size: 134 Color: 3
Size: 126 Color: 11

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 2
Size: 260 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 1
Size: 134 Color: 6
Size: 129 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 12
Size: 121 Color: 12
Size: 113 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 746 Color: 6
Size: 129 Color: 10
Size: 126 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 567 Color: 4
Size: 235 Color: 15
Size: 199 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 5
Size: 328 Color: 7
Size: 322 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 18
Size: 289 Color: 5
Size: 287 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 502 Color: 1
Size: 273 Color: 12
Size: 226 Color: 8

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 17
Size: 498 Color: 3

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 11
Size: 488 Color: 0

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 14
Size: 476 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 297 Color: 0
Size: 232 Color: 14

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 4
Size: 425 Color: 10

Bin 29: 0 of cap free
Amount of items: 4
Items: 
Size: 511 Color: 11
Size: 226 Color: 9
Size: 163 Color: 7
Size: 101 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 5
Size: 201 Color: 10
Size: 166 Color: 17

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 6
Size: 323 Color: 13

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 0
Size: 289 Color: 12

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 13
Size: 276 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 11
Size: 324 Color: 19
Size: 323 Color: 18

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 9
Size: 478 Color: 2

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 9
Size: 428 Color: 0

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 16
Size: 239 Color: 8

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 11
Size: 217 Color: 6

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 441 Color: 8

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 9
Size: 442 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 698 Color: 12
Size: 193 Color: 11
Size: 110 Color: 7

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 12

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 19
Size: 298 Color: 1

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 18
Size: 218 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 322 Color: 4
Size: 320 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 520 Color: 9
Size: 287 Color: 8
Size: 194 Color: 2

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 9
Size: 478 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 6
Size: 337 Color: 18
Size: 326 Color: 4

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 18
Size: 374 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 317 Color: 1
Size: 293 Color: 9

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 496 Color: 11

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 0
Size: 397 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 321 Color: 8
Size: 273 Color: 2

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 5
Size: 237 Color: 3

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 10
Size: 334 Color: 15

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 8
Size: 449 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 512 Color: 15
Size: 288 Color: 19
Size: 201 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 17
Size: 259 Color: 13
Size: 241 Color: 14

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 12
Size: 496 Color: 17

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 448 Color: 10

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 1
Size: 447 Color: 18

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 4
Size: 339 Color: 9

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 391 Color: 3

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 18
Size: 222 Color: 11

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 13
Size: 492 Color: 15

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 9
Size: 439 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 320 Color: 15
Size: 317 Color: 19

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 13
Size: 483 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 392 Color: 13
Size: 132 Color: 19

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 396 Color: 2
Size: 198 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 8
Size: 395 Color: 0
Size: 211 Color: 17

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 354 Color: 19
Size: 224 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 6
Size: 352 Color: 12
Size: 241 Color: 14

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 2
Size: 359 Color: 10
Size: 241 Color: 19

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 352 Color: 3
Size: 253 Color: 18

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 8
Size: 408 Color: 18

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 8
Size: 351 Color: 12
Size: 259 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 333 Color: 19
Size: 271 Color: 19

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 7
Size: 336 Color: 10
Size: 274 Color: 8

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 332 Color: 12
Size: 299 Color: 10

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 0
Size: 338 Color: 19
Size: 309 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 16
Size: 322 Color: 13
Size: 320 Color: 5

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 8
Size: 332 Color: 15
Size: 318 Color: 19

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 7
Size: 321 Color: 17
Size: 321 Color: 13

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6
Size: 322 Color: 14
Size: 322 Color: 10

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 18
Size: 333 Color: 15
Size: 315 Color: 5

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 315 Color: 9
Size: 315 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 8
Size: 315 Color: 13
Size: 315 Color: 10

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 13
Size: 332 Color: 5
Size: 317 Color: 19

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 17
Size: 312 Color: 0
Size: 268 Color: 17

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 7
Size: 304 Color: 18
Size: 273 Color: 2

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 7
Size: 288 Color: 14
Size: 288 Color: 5

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 16
Size: 288 Color: 16
Size: 275 Color: 6

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 11
Size: 293 Color: 14
Size: 270 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 9
Size: 288 Color: 16
Size: 274 Color: 11

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 13

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 7
Size: 498 Color: 16

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 10
Size: 495 Color: 19

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 3
Size: 494 Color: 6

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 11
Size: 493 Color: 15

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 3
Size: 492 Color: 11

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 9
Size: 492 Color: 19

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 3
Size: 489 Color: 8

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 0
Size: 489 Color: 10

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 16
Size: 485 Color: 18

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 18
Size: 485 Color: 11

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 19
Size: 487 Color: 5

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 519 Color: 4
Size: 288 Color: 1
Size: 194 Color: 12

Bin 109: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 6
Size: 482 Color: 18

Bin 110: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 1
Size: 480 Color: 9

Bin 111: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 4
Size: 479 Color: 14

Bin 112: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 9
Size: 478 Color: 11

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 522 Color: 18
Size: 287 Color: 19
Size: 192 Color: 7

Bin 114: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 5
Size: 478 Color: 0

Bin 115: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 7
Size: 478 Color: 9

Bin 116: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 478 Color: 10

Bin 117: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 4
Size: 476 Color: 5

Bin 118: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 17
Size: 476 Color: 9

Bin 119: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 0
Size: 477 Color: 3

Bin 120: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 475 Color: 16

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 523 Color: 18
Size: 267 Color: 10
Size: 211 Color: 14

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 526 Color: 4
Size: 274 Color: 9
Size: 201 Color: 5

Bin 123: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 19
Size: 474 Color: 3

Bin 124: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 6

Bin 125: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 8
Size: 471 Color: 15

Bin 126: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 9
Size: 475 Color: 2

Bin 127: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 10
Size: 471 Color: 3

Bin 128: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 7
Size: 470 Color: 10

Bin 129: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 481 Color: 4

Bin 130: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 8
Size: 470 Color: 11

Bin 131: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 470 Color: 12

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 13
Size: 287 Color: 19
Size: 182 Color: 2

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 16
Size: 469 Color: 18

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 16
Size: 468 Color: 17

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 9
Size: 243 Color: 13
Size: 224 Color: 18

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 17
Size: 467 Color: 3

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 2
Size: 466 Color: 8

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 466 Color: 17

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 8
Size: 465 Color: 1

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 6
Size: 465 Color: 7

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 12

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 7
Size: 464 Color: 8

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 19
Size: 464 Color: 16

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 15
Size: 464 Color: 11

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 463 Color: 14

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 4
Size: 462 Color: 8

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 13
Size: 459 Color: 8

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 14
Size: 458 Color: 17

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 7
Size: 457 Color: 6

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 6
Size: 457 Color: 18

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 8
Size: 457 Color: 15

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 13
Size: 457 Color: 6

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 11
Size: 456 Color: 5

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 8
Size: 456 Color: 19

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 7
Size: 454 Color: 11

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 4
Size: 453 Color: 9

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 19
Size: 453 Color: 14

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 9
Size: 453 Color: 12

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 11
Size: 452 Color: 13

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 18
Size: 452 Color: 8

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 9
Size: 452 Color: 12

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 1
Size: 452 Color: 18

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 2
Size: 452 Color: 6

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 550 Color: 11
Size: 266 Color: 15
Size: 185 Color: 10

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 5
Size: 451 Color: 1

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 10
Size: 451 Color: 0

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 1
Size: 450 Color: 12

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 552 Color: 11
Size: 267 Color: 12
Size: 182 Color: 0

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 17
Size: 448 Color: 1

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 4
Size: 448 Color: 17

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 15
Size: 448 Color: 18

Bin 172: 0 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 18
Size: 448 Color: 5

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 2
Size: 447 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 9
Size: 225 Color: 10
Size: 222 Color: 14

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 554 Color: 13
Size: 267 Color: 13
Size: 180 Color: 2

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 9
Size: 446 Color: 11

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 19
Size: 446 Color: 11

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 2
Size: 275 Color: 7
Size: 170 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 15
Size: 235 Color: 4
Size: 210 Color: 6

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 9
Size: 275 Color: 19
Size: 170 Color: 5

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 0
Size: 275 Color: 1
Size: 170 Color: 11

Bin 182: 0 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 17
Size: 445 Color: 3

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 557 Color: 6
Size: 252 Color: 5
Size: 192 Color: 8

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 557 Color: 3
Size: 232 Color: 18
Size: 212 Color: 6

Bin 185: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 19

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 14
Size: 442 Color: 2

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 14
Size: 442 Color: 11

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 5
Size: 442 Color: 11

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 15
Size: 269 Color: 2
Size: 172 Color: 8

Bin 190: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 7
Size: 441 Color: 2

Bin 191: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 2
Size: 440 Color: 14

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 7
Size: 440 Color: 15

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 6
Size: 439 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 15
Size: 229 Color: 2
Size: 210 Color: 6

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 562 Color: 15
Size: 235 Color: 13
Size: 204 Color: 14

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 17
Size: 438 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 561 Color: 1
Size: 268 Color: 18
Size: 172 Color: 2

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 11
Size: 437 Color: 9

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 10
Size: 437 Color: 5

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 0
Size: 436 Color: 3

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 6
Size: 435 Color: 10

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 7

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 9
Size: 437 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 566 Color: 2
Size: 266 Color: 3
Size: 169 Color: 7

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 7
Size: 434 Color: 11

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 11
Size: 434 Color: 16

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 16
Size: 432 Color: 0

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 3
Size: 433 Color: 17

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 15
Size: 432 Color: 5

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 19
Size: 432 Color: 12

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 18
Size: 431 Color: 14

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 5
Size: 431 Color: 9

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 13
Size: 431 Color: 17

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 13
Size: 431 Color: 6

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 15
Size: 430 Color: 0

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 6
Size: 430 Color: 17

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 3
Size: 429 Color: 0

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 7
Size: 428 Color: 19

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 0
Size: 243 Color: 17
Size: 183 Color: 4

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 17
Size: 423 Color: 9

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 9
Size: 429 Color: 18

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 6

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 15
Size: 422 Color: 0

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 18
Size: 422 Color: 0

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 7
Size: 421 Color: 10

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 17
Size: 420 Color: 14

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 17
Size: 420 Color: 12

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 12
Size: 420 Color: 5

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 15
Size: 419 Color: 6

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 419 Color: 10

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 17
Size: 419 Color: 3

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 3
Size: 419 Color: 7

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 12
Size: 418 Color: 17

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 13
Size: 418 Color: 15

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 417 Color: 6

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 3
Size: 416 Color: 11

Bin 237: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 17
Size: 417 Color: 15

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 18
Size: 247 Color: 9
Size: 169 Color: 7

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 12
Size: 415 Color: 3

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 19
Size: 415 Color: 11

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 19
Size: 414 Color: 8

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 4
Size: 413 Color: 17

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 12
Size: 412 Color: 16

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 5
Size: 412 Color: 6

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 18
Size: 411 Color: 6

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 10
Size: 411 Color: 3

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 6
Size: 411 Color: 1

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 15
Size: 410 Color: 8

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 591 Color: 15
Size: 241 Color: 17
Size: 169 Color: 16

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 6
Size: 409 Color: 5

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 8
Size: 409 Color: 7

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 10
Size: 408 Color: 9

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 7
Size: 241 Color: 1
Size: 166 Color: 6

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 12
Size: 406 Color: 8

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 19
Size: 406 Color: 7

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 6
Size: 224 Color: 14
Size: 182 Color: 12

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 9
Size: 204 Color: 4
Size: 202 Color: 18

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 2
Size: 229 Color: 1
Size: 177 Color: 4

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 17
Size: 406 Color: 7

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 8
Size: 405 Color: 4

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 8
Size: 404 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 13
Size: 212 Color: 14
Size: 192 Color: 13

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 19
Size: 225 Color: 11
Size: 179 Color: 10

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 598 Color: 3
Size: 212 Color: 0
Size: 191 Color: 4

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 12
Size: 403 Color: 15

Bin 266: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 6
Size: 402 Color: 17

Bin 267: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 11
Size: 401 Color: 13

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 600 Color: 8
Size: 226 Color: 6
Size: 175 Color: 13

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 601 Color: 18
Size: 400 Color: 3

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 12
Size: 399 Color: 6

Bin 271: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 10
Size: 399 Color: 2

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 6
Size: 399 Color: 18

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 7
Size: 399 Color: 19

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 9
Size: 399 Color: 7

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 602 Color: 11
Size: 223 Color: 12
Size: 176 Color: 14

Bin 276: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 5
Size: 397 Color: 2

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 15
Size: 397 Color: 5

Bin 278: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 12
Size: 395 Color: 8

Bin 279: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 1
Size: 395 Color: 14

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 606 Color: 11
Size: 229 Color: 9
Size: 166 Color: 12

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 16
Size: 394 Color: 0

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 14
Size: 394 Color: 19

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 8
Size: 394 Color: 4

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 19
Size: 394 Color: 11

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 18
Size: 393 Color: 5

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 10
Size: 393 Color: 17

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 13
Size: 392 Color: 8

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 609 Color: 19
Size: 197 Color: 16
Size: 195 Color: 14

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 17
Size: 222 Color: 15
Size: 169 Color: 11

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 9
Size: 390 Color: 7

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 613 Color: 3
Size: 214 Color: 6
Size: 174 Color: 5

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 8
Size: 388 Color: 15

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 13
Size: 222 Color: 17
Size: 165 Color: 15

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 6
Size: 387 Color: 19

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 387 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 614 Color: 12
Size: 225 Color: 9
Size: 162 Color: 16

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 2
Size: 385 Color: 16

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 11
Size: 384 Color: 0

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 13
Size: 384 Color: 2

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 14
Size: 195 Color: 13
Size: 188 Color: 16

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 15
Size: 383 Color: 10

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 18
Size: 383 Color: 17

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 9
Size: 385 Color: 2

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 3
Size: 383 Color: 1

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 6
Size: 383 Color: 16

Bin 306: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 8
Size: 382 Color: 0

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 5
Size: 382 Color: 10

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 4
Size: 382 Color: 7

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 11
Size: 382 Color: 9

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 9
Size: 211 Color: 17
Size: 173 Color: 17

Bin 311: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 381 Color: 16

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 12
Size: 381 Color: 4

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 3
Size: 380 Color: 18

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 11
Size: 380 Color: 3

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 2
Size: 379 Color: 1

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 16
Size: 379 Color: 5

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 5
Size: 191 Color: 12
Size: 188 Color: 3

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 11
Size: 379 Color: 16

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 9
Size: 380 Color: 7

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 8
Size: 375 Color: 13

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 15
Size: 375 Color: 1

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 10
Size: 373 Color: 8

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 4
Size: 371 Color: 6

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 18
Size: 188 Color: 11
Size: 182 Color: 19

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 632 Color: 16
Size: 210 Color: 9
Size: 159 Color: 7

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 19
Size: 369 Color: 7

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 16
Size: 367 Color: 6

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 11
Size: 204 Color: 0
Size: 163 Color: 14

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 0
Size: 367 Color: 1

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 10
Size: 367 Color: 14

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 18
Size: 366 Color: 15

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 0
Size: 193 Color: 16
Size: 173 Color: 18

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 16
Size: 200 Color: 11
Size: 166 Color: 17

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 636 Color: 1
Size: 198 Color: 8
Size: 167 Color: 9

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 6
Size: 365 Color: 3

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 15
Size: 365 Color: 8

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 5
Size: 363 Color: 6

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 15
Size: 363 Color: 3

Bin 339: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 6
Size: 363 Color: 11

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 14
Size: 191 Color: 8
Size: 172 Color: 17

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 638 Color: 0
Size: 199 Color: 3
Size: 164 Color: 12

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 18
Size: 363 Color: 19

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 10
Size: 362 Color: 5

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 17
Size: 362 Color: 13

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 0
Size: 361 Color: 18

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 10
Size: 361 Color: 15

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 17
Size: 183 Color: 9
Size: 177 Color: 18

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 642 Color: 17
Size: 180 Color: 8
Size: 179 Color: 18

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 15
Size: 358 Color: 1

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 3
Size: 358 Color: 5

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 2
Size: 358 Color: 4

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 9
Size: 356 Color: 16

Bin 353: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 14
Size: 356 Color: 3

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 10
Size: 355 Color: 11

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 4
Size: 355 Color: 5

Bin 356: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 11
Size: 354 Color: 2

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 10
Size: 353 Color: 19

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 18
Size: 353 Color: 17

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 16
Size: 177 Color: 17
Size: 174 Color: 12

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 1
Size: 180 Color: 11
Size: 172 Color: 10

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 8
Size: 351 Color: 4

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 4

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 0
Size: 350 Color: 6

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 12
Size: 350 Color: 7

Bin 365: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 2
Size: 349 Color: 3

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 7
Size: 349 Color: 14

Bin 367: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 16
Size: 348 Color: 13

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 13
Size: 178 Color: 14
Size: 170 Color: 11

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 18
Size: 181 Color: 3
Size: 167 Color: 9

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 13
Size: 191 Color: 1
Size: 157 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 654 Color: 19
Size: 180 Color: 8
Size: 167 Color: 17

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 11
Size: 348 Color: 18

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 1
Size: 177 Color: 8
Size: 171 Color: 10

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 15
Size: 347 Color: 10

Bin 375: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 10
Size: 347 Color: 15

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 12
Size: 347 Color: 6

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 3
Size: 346 Color: 12

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 6
Size: 346 Color: 10

Bin 379: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 19
Size: 346 Color: 10

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 6
Size: 346 Color: 18

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 15
Size: 178 Color: 13
Size: 168 Color: 11

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 18
Size: 189 Color: 1
Size: 157 Color: 4

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 12
Size: 345 Color: 15

Bin 384: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 15
Size: 345 Color: 12

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 14
Size: 183 Color: 9
Size: 161 Color: 7

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 7
Size: 175 Color: 13
Size: 169 Color: 7

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 656 Color: 1
Size: 175 Color: 11
Size: 170 Color: 13

Bin 388: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 12
Size: 344 Color: 15

Bin 389: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 7
Size: 344 Color: 8

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 2
Size: 344 Color: 3

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 8
Size: 343 Color: 16

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 8
Size: 182 Color: 15
Size: 161 Color: 11

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 658 Color: 19
Size: 188 Color: 1
Size: 155 Color: 4

Bin 394: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 7
Size: 342 Color: 14

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 15
Size: 342 Color: 13

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 13
Size: 342 Color: 4

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 0
Size: 342 Color: 3

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 16
Size: 340 Color: 19

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 3
Size: 186 Color: 8
Size: 155 Color: 2

Bin 400: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 10
Size: 339 Color: 14

Bin 401: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 5
Size: 339 Color: 13

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 3
Size: 182 Color: 8
Size: 155 Color: 13

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 5
Size: 335 Color: 10

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 9
Size: 335 Color: 16

Bin 405: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 15
Size: 334 Color: 16

Bin 406: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 18
Size: 334 Color: 4

Bin 407: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 3
Size: 336 Color: 15

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 19
Size: 333 Color: 17

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 18
Size: 333 Color: 4

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 9
Size: 333 Color: 5

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 19
Size: 333 Color: 0

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 18

Bin 413: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 16
Size: 331 Color: 19

Bin 414: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 4
Size: 331 Color: 15

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 6
Size: 179 Color: 3
Size: 152 Color: 4

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 9
Size: 330 Color: 8

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 10
Size: 329 Color: 4

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 12
Size: 329 Color: 19

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 329 Color: 2

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 673 Color: 18
Size: 176 Color: 1
Size: 152 Color: 9

Bin 421: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 9
Size: 328 Color: 0

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 7
Size: 328 Color: 8

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 6
Size: 168 Color: 15
Size: 158 Color: 17

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 12
Size: 166 Color: 18
Size: 160 Color: 12

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 1
Size: 171 Color: 3
Size: 154 Color: 5

Bin 426: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 12
Size: 325 Color: 2

Bin 427: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 18
Size: 324 Color: 14

Bin 428: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 2
Size: 324 Color: 8

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 6
Size: 324 Color: 19

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 19
Size: 324 Color: 18

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 2
Size: 171 Color: 12
Size: 153 Color: 2

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 6
Size: 172 Color: 3
Size: 150 Color: 4

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 10
Size: 321 Color: 12

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 19

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 10
Size: 320 Color: 14

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 10
Size: 319 Color: 2

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 12
Size: 319 Color: 14

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 683 Color: 11
Size: 162 Color: 3
Size: 156 Color: 0

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 19
Size: 318 Color: 1

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 19
Size: 318 Color: 8

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 7
Size: 316 Color: 19

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 18
Size: 316 Color: 13

Bin 443: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 19
Size: 316 Color: 2

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 685 Color: 3
Size: 166 Color: 1
Size: 150 Color: 6

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 686 Color: 9
Size: 315 Color: 17

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 10
Size: 314 Color: 13

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 7
Size: 314 Color: 4

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 16
Size: 314 Color: 7

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 18
Size: 313 Color: 11

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 4
Size: 160 Color: 13
Size: 153 Color: 9

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 11
Size: 162 Color: 17
Size: 151 Color: 4

Bin 452: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 2
Size: 312 Color: 5

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 18
Size: 310 Color: 7

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 9
Size: 157 Color: 11
Size: 153 Color: 5

Bin 455: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 2
Size: 310 Color: 8

Bin 456: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 4
Size: 310 Color: 1

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 14
Size: 159 Color: 8
Size: 150 Color: 6

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 14
Size: 308 Color: 17

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 9
Size: 307 Color: 14

Bin 460: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 17
Size: 307 Color: 10

Bin 461: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 17
Size: 307 Color: 6

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 17
Size: 306 Color: 19

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 306 Color: 19

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 19
Size: 306 Color: 4

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 696 Color: 5
Size: 156 Color: 1
Size: 149 Color: 7

Bin 466: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 15
Size: 305 Color: 0

Bin 467: 0 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 17
Size: 305 Color: 18

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 8
Size: 157 Color: 8
Size: 151 Color: 9

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 14
Size: 304 Color: 12

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 16
Size: 304 Color: 12

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 11
Size: 303 Color: 18

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 6
Size: 302 Color: 12

Bin 473: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 302 Color: 9

Bin 474: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 3
Size: 301 Color: 16

Bin 475: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 17
Size: 301 Color: 12

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 5
Size: 301 Color: 0

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 1
Size: 300 Color: 14

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 6
Size: 300 Color: 19

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 701 Color: 3
Size: 153 Color: 14
Size: 147 Color: 15

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 701 Color: 8
Size: 150 Color: 18
Size: 150 Color: 18

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 702 Color: 5
Size: 152 Color: 14
Size: 147 Color: 1

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 19
Size: 299 Color: 13

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 11
Size: 299 Color: 18

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 4
Size: 299 Color: 5

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 10
Size: 298 Color: 6

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 12
Size: 152 Color: 0
Size: 146 Color: 10

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 3
Size: 151 Color: 19
Size: 146 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 2
Size: 153 Color: 8
Size: 144 Color: 6

Bin 489: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 3
Size: 296 Color: 11

Bin 490: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 11
Size: 296 Color: 19

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 2
Size: 296 Color: 8

Bin 492: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 5
Size: 295 Color: 15

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 15
Size: 294 Color: 7

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 7
Size: 294 Color: 16

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 16
Size: 294 Color: 10

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 708 Color: 19
Size: 293 Color: 0

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 10
Size: 292 Color: 6

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 10
Size: 292 Color: 9

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 9
Size: 147 Color: 18
Size: 144 Color: 14

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 710 Color: 10
Size: 147 Color: 19
Size: 144 Color: 18

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 14
Size: 291 Color: 16

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 4
Size: 291 Color: 1

Bin 503: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 1
Size: 290 Color: 9

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 16
Size: 290 Color: 17

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 711 Color: 16
Size: 290 Color: 12

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 2
Size: 145 Color: 15
Size: 145 Color: 11

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 15
Size: 146 Color: 10
Size: 144 Color: 19

Bin 508: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 14
Size: 288 Color: 0

Bin 509: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 19
Size: 288 Color: 16

Bin 510: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 2
Size: 287 Color: 14

Bin 511: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 3
Size: 287 Color: 10

Bin 512: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 6
Size: 286 Color: 2

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 286 Color: 11

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 2
Size: 143 Color: 5
Size: 143 Color: 0

Bin 515: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 8
Size: 285 Color: 13

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 13
Size: 285 Color: 16

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 4
Size: 284 Color: 15

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 284 Color: 18

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 7
Size: 283 Color: 11

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 8
Size: 142 Color: 6
Size: 141 Color: 5

Bin 521: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 11
Size: 283 Color: 17

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 7
Size: 141 Color: 11
Size: 141 Color: 2

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 720 Color: 1
Size: 141 Color: 14
Size: 140 Color: 4

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 8
Size: 280 Color: 4

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 12
Size: 281 Color: 10

Bin 526: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 9
Size: 280 Color: 19

Bin 527: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 13
Size: 280 Color: 4

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 721 Color: 9
Size: 141 Color: 5
Size: 139 Color: 5

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 10
Size: 140 Color: 4
Size: 139 Color: 8

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 6
Size: 279 Color: 4

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 9
Size: 279 Color: 5

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 10
Size: 279 Color: 14

Bin 533: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 16
Size: 278 Color: 9

Bin 534: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 17
Size: 278 Color: 9

Bin 535: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 10
Size: 276 Color: 3

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 2
Size: 276 Color: 16

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 13
Size: 275 Color: 6

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 9
Size: 275 Color: 12

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 10
Size: 273 Color: 6

Bin 540: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 11
Size: 273 Color: 9

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 6
Size: 139 Color: 14
Size: 134 Color: 2

Bin 542: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 0
Size: 272 Color: 8

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 5
Size: 272 Color: 13

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 8
Size: 273 Color: 16

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 4
Size: 271 Color: 19

Bin 546: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 7
Size: 271 Color: 19

Bin 547: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 19
Size: 271 Color: 13

Bin 548: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 12
Size: 271 Color: 10

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 12
Size: 270 Color: 16

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 16
Size: 270 Color: 17

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 12
Size: 270 Color: 9

Bin 552: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 6
Size: 269 Color: 14

Bin 553: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 18
Size: 268 Color: 6

Bin 554: 0 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 267 Color: 9

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 12
Size: 266 Color: 0

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 19
Size: 266 Color: 11

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 7
Size: 266 Color: 8

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 3
Size: 265 Color: 2

Bin 559: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 10
Size: 265 Color: 3

Bin 560: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 5
Size: 265 Color: 10

Bin 561: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 12

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 18
Size: 132 Color: 2
Size: 132 Color: 2

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 13
Size: 262 Color: 5

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 14
Size: 262 Color: 15

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 5
Size: 262 Color: 14

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 19
Size: 262 Color: 5

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 18
Size: 138 Color: 11
Size: 123 Color: 3

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 12
Size: 260 Color: 5

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 17
Size: 259 Color: 18

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 742 Color: 3
Size: 131 Color: 6
Size: 128 Color: 15

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 19
Size: 132 Color: 12
Size: 126 Color: 12

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 19
Size: 137 Color: 0
Size: 121 Color: 1

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 258 Color: 9

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 257 Color: 6

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 17
Size: 257 Color: 6

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 9
Size: 134 Color: 17
Size: 123 Color: 18

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 17
Size: 141 Color: 12
Size: 115 Color: 11

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 745 Color: 8
Size: 138 Color: 19
Size: 118 Color: 4

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 256 Color: 15

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 15
Size: 133 Color: 6
Size: 121 Color: 6

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 4
Size: 131 Color: 9
Size: 123 Color: 13

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 0
Size: 253 Color: 11

Bin 583: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 6
Size: 253 Color: 2

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 8
Size: 252 Color: 11

Bin 585: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 18
Size: 252 Color: 11

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 10
Size: 128 Color: 5
Size: 124 Color: 1

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 11
Size: 137 Color: 17
Size: 115 Color: 1

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 8
Size: 132 Color: 13
Size: 119 Color: 3

Bin 589: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 13
Size: 251 Color: 2

Bin 590: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 3
Size: 251 Color: 6

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 12
Size: 138 Color: 16
Size: 113 Color: 1

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 19
Size: 250 Color: 8

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 12
Size: 249 Color: 0

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 10
Size: 249 Color: 18

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 13
Size: 249 Color: 16

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 7
Size: 130 Color: 19
Size: 118 Color: 14

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 11
Size: 129 Color: 2
Size: 119 Color: 14

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 19
Size: 133 Color: 7
Size: 114 Color: 3

Bin 599: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 15
Size: 247 Color: 17

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 18
Size: 247 Color: 10

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 8
Size: 245 Color: 2

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 6
Size: 246 Color: 11

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 2
Size: 246 Color: 14

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 15
Size: 131 Color: 4
Size: 114 Color: 5

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 19
Size: 131 Color: 2
Size: 112 Color: 2

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 7
Size: 242 Color: 9

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 6
Size: 241 Color: 5

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 13
Size: 123 Color: 17
Size: 117 Color: 5

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 0
Size: 124 Color: 10
Size: 116 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 14
Size: 121 Color: 10
Size: 119 Color: 7

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 10
Size: 124 Color: 2
Size: 116 Color: 4

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 762 Color: 8
Size: 126 Color: 14
Size: 113 Color: 1

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 764 Color: 5
Size: 122 Color: 2
Size: 115 Color: 14

Bin 614: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 11
Size: 237 Color: 9

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 237 Color: 4

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 7
Size: 236 Color: 4

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 16
Size: 236 Color: 6

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 6
Size: 236 Color: 16

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 12
Size: 121 Color: 9
Size: 114 Color: 14

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 766 Color: 19
Size: 120 Color: 4
Size: 115 Color: 9

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 1
Size: 122 Color: 8
Size: 112 Color: 11

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 12
Size: 234 Color: 9

Bin 623: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 5
Size: 234 Color: 15

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 13
Size: 120 Color: 15
Size: 113 Color: 12

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 14
Size: 118 Color: 12
Size: 114 Color: 5

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 770 Color: 9
Size: 121 Color: 15
Size: 110 Color: 7

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 6
Size: 231 Color: 16

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 19
Size: 231 Color: 18

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 13
Size: 230 Color: 17

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 15
Size: 117 Color: 14
Size: 113 Color: 9

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 6
Size: 118 Color: 14
Size: 111 Color: 0

Bin 632: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 16
Size: 229 Color: 19

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 10
Size: 229 Color: 9

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 17
Size: 228 Color: 10

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 18
Size: 228 Color: 12

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 9
Size: 227 Color: 0

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 13
Size: 227 Color: 4

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 776 Color: 12
Size: 116 Color: 15
Size: 109 Color: 10

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 5
Size: 225 Color: 8

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 2
Size: 224 Color: 1

Bin 641: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 13
Size: 224 Color: 4

Bin 642: 0 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 14
Size: 223 Color: 15

Bin 643: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 7
Size: 221 Color: 18

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 6
Size: 220 Color: 5

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 2
Size: 219 Color: 6

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 4
Size: 219 Color: 5

Bin 647: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 4
Size: 219 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 13
Size: 111 Color: 5
Size: 107 Color: 11

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 19
Size: 111 Color: 2
Size: 107 Color: 11

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 8
Size: 117 Color: 19
Size: 101 Color: 11

Bin 651: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 11
Size: 218 Color: 4

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 784 Color: 3
Size: 116 Color: 15
Size: 101 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 784 Color: 11
Size: 116 Color: 10
Size: 101 Color: 15

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 12
Size: 108 Color: 8
Size: 108 Color: 4

Bin 655: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 6
Size: 215 Color: 14

Bin 656: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 9
Size: 214 Color: 15

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 15
Size: 214 Color: 12

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 3
Size: 108 Color: 9
Size: 106 Color: 9

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 3
Size: 108 Color: 11
Size: 106 Color: 19

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 17
Size: 113 Color: 14
Size: 101 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 6
Size: 109 Color: 13
Size: 104 Color: 4

Bin 662: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 16
Size: 213 Color: 10

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 19
Size: 213 Color: 7

Bin 664: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 12
Size: 213 Color: 4

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 789 Color: 13
Size: 111 Color: 10
Size: 101 Color: 13

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 7
Size: 110 Color: 8
Size: 101 Color: 3

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 0
Size: 111 Color: 6
Size: 100 Color: 12

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 5
Size: 211 Color: 2

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 16
Size: 210 Color: 5

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 210 Color: 1

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 0
Size: 106 Color: 5
Size: 103 Color: 7

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 13
Size: 106 Color: 16
Size: 103 Color: 9

Bin 673: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 12
Size: 209 Color: 6

Bin 674: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 9
Size: 209 Color: 4

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 0
Size: 209 Color: 19

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 14
Size: 208 Color: 18

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 10
Size: 208 Color: 16

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 18
Size: 105 Color: 10
Size: 103 Color: 10

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 1
Size: 104 Color: 14
Size: 104 Color: 10

Bin 680: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 11
Size: 208 Color: 6

Bin 681: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 17
Size: 207 Color: 18

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 19
Size: 207 Color: 2

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 15
Size: 206 Color: 19

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 18
Size: 205 Color: 8

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 796 Color: 4
Size: 105 Color: 6
Size: 100 Color: 19

Bin 686: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 204 Color: 18

Bin 687: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 16
Size: 203 Color: 17

Bin 688: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 3
Size: 203 Color: 11

Bin 689: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 203 Color: 2

Bin 690: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 4
Size: 203 Color: 7

Bin 691: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 11
Size: 201 Color: 7

Bin 692: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 3
Size: 201 Color: 16

Bin 693: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 9
Size: 201 Color: 17

Bin 694: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 15
Size: 200 Color: 18

Bin 695: 0 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 0
Size: 200 Color: 12

Bin 696: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 15
Size: 488 Color: 5

Bin 697: 1 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 3
Size: 486 Color: 9

Bin 698: 1 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 13
Size: 486 Color: 2

Bin 699: 1 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 11
Size: 479 Color: 13

Bin 700: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 15
Size: 474 Color: 18

Bin 701: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 12
Size: 474 Color: 19

Bin 702: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 2
Size: 474 Color: 16

Bin 703: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 11
Size: 467 Color: 9

Bin 704: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 16
Size: 467 Color: 6

Bin 705: 1 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 16
Size: 467 Color: 6

Bin 706: 1 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 15
Size: 460 Color: 3

Bin 707: 1 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 1
Size: 457 Color: 2

Bin 708: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 14
Size: 450 Color: 3

Bin 709: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 4
Size: 450 Color: 19

Bin 710: 1 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 5
Size: 450 Color: 17

Bin 711: 1 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 6
Size: 443 Color: 5

Bin 712: 1 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 2
Size: 436 Color: 7

Bin 713: 1 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 9
Size: 436 Color: 12

Bin 714: 1 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 16
Size: 429 Color: 7

Bin 715: 1 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 1
Size: 429 Color: 13

Bin 716: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 14
Size: 425 Color: 9

Bin 717: 1 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 17
Size: 420 Color: 6

Bin 718: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 18
Size: 418 Color: 7

Bin 719: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 418 Color: 16

Bin 720: 1 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 8
Size: 416 Color: 17

Bin 721: 1 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 13
Size: 411 Color: 19

Bin 722: 1 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 336 Color: 3
Size: 172 Color: 11

Bin 723: 1 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 17
Size: 396 Color: 9

Bin 724: 1 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 5
Size: 392 Color: 16

Bin 725: 1 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 6
Size: 389 Color: 16

Bin 726: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 4
Size: 386 Color: 1

Bin 727: 1 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 18
Size: 379 Color: 2

Bin 728: 1 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 7
Size: 377 Color: 11

Bin 729: 1 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 377 Color: 8

Bin 730: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 5
Size: 372 Color: 8

Bin 731: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 10
Size: 372 Color: 18

Bin 732: 1 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 13
Size: 365 Color: 2

Bin 733: 1 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 3
Size: 365 Color: 7

Bin 734: 1 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 18
Size: 346 Color: 6

Bin 735: 1 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 2
Size: 339 Color: 0

Bin 736: 1 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 19
Size: 334 Color: 12

Bin 737: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 17
Size: 326 Color: 4

Bin 738: 1 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 17
Size: 326 Color: 2

Bin 739: 1 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 10
Size: 184 Color: 7
Size: 150 Color: 5

Bin 740: 1 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 15
Size: 312 Color: 13

Bin 741: 1 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 14
Size: 312 Color: 8

Bin 742: 1 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 15
Size: 304 Color: 18

Bin 743: 1 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 7
Size: 293 Color: 6

Bin 744: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 6
Size: 290 Color: 1

Bin 745: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 17
Size: 290 Color: 8

Bin 746: 1 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 2
Size: 280 Color: 3

Bin 747: 1 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 10
Size: 280 Color: 13

Bin 748: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 11
Size: 264 Color: 12

Bin 749: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 4
Size: 264 Color: 16

Bin 750: 1 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 9
Size: 264 Color: 19

Bin 751: 1 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 11
Size: 253 Color: 12

Bin 752: 1 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 9
Size: 253 Color: 2

Bin 753: 1 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 2
Size: 250 Color: 15

Bin 754: 1 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 17
Size: 250 Color: 12

Bin 755: 1 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 3
Size: 236 Color: 19

Bin 756: 1 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 11
Size: 248 Color: 10

Bin 757: 1 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 13
Size: 248 Color: 10

Bin 758: 1 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 14
Size: 233 Color: 17

Bin 759: 1 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 17
Size: 229 Color: 6

Bin 760: 1 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 18
Size: 223 Color: 8

Bin 761: 1 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 15
Size: 216 Color: 10

Bin 762: 1 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 0
Size: 213 Color: 4

Bin 763: 1 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 4
Size: 208 Color: 9

Bin 764: 1 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 15
Size: 206 Color: 4

Bin 765: 1 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 1
Size: 242 Color: 17

Bin 766: 1 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 16
Size: 312 Color: 4

Bin 767: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 5
Size: 400 Color: 3

Bin 768: 1 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 2
Size: 362 Color: 19

Bin 769: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 17
Size: 425 Color: 0

Bin 770: 2 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 18
Size: 497 Color: 0

Bin 771: 2 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 2
Size: 456 Color: 16

Bin 772: 2 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 357 Color: 9

Bin 773: 2 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 0
Size: 338 Color: 7

Bin 774: 2 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 13
Size: 175 Color: 6
Size: 137 Color: 16

Bin 775: 2 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 10
Size: 270 Color: 6

Bin 776: 2 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 6
Size: 259 Color: 4

Bin 777: 2 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 14
Size: 252 Color: 16

Bin 778: 2 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 17
Size: 236 Color: 4

Bin 779: 2 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 2
Size: 497 Color: 4

Bin 780: 2 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 15
Size: 497 Color: 0

Bin 781: 2 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 19
Size: 463 Color: 1

Bin 782: 2 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 8
Size: 460 Color: 14

Bin 783: 2 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 13
Size: 460 Color: 5

Bin 784: 2 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 8
Size: 460 Color: 14

Bin 785: 2 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 14
Size: 456 Color: 9

Bin 786: 2 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 1
Size: 456 Color: 4

Bin 787: 2 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 7
Size: 456 Color: 5

Bin 788: 2 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 6
Size: 453 Color: 4

Bin 789: 2 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 10
Size: 449 Color: 14

Bin 790: 2 of cap free
Amount of items: 2
Items: 
Size: 589 Color: 11
Size: 410 Color: 19

Bin 791: 2 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 384 Color: 18

Bin 792: 2 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 384 Color: 7

Bin 793: 2 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 6
Size: 376 Color: 18

Bin 794: 2 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 10
Size: 348 Color: 16

Bin 795: 2 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 17
Size: 341 Color: 2

Bin 796: 2 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 19
Size: 280 Color: 6

Bin 797: 2 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 17
Size: 280 Color: 4

Bin 798: 2 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 1
Size: 277 Color: 2

Bin 799: 2 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 1
Size: 229 Color: 8

Bin 800: 2 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 7
Size: 215 Color: 17

Bin 801: 2 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 13
Size: 205 Color: 9

Bin 802: 2 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 12
Size: 205 Color: 9

Bin 803: 2 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 8
Size: 205 Color: 2

Bin 804: 2 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 0
Size: 205 Color: 13

Bin 805: 2 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 7
Size: 449 Color: 4

Bin 806: 3 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 12
Size: 403 Color: 3

Bin 807: 3 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 10
Size: 399 Color: 8

Bin 808: 3 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 18
Size: 486 Color: 16

Bin 809: 3 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 3
Size: 478 Color: 2

Bin 810: 3 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 14
Size: 428 Color: 11

Bin 811: 3 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 17
Size: 418 Color: 5

Bin 812: 3 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 2
Size: 399 Color: 9

Bin 813: 3 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 18
Size: 364 Color: 7

Bin 814: 3 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 12
Size: 289 Color: 15

Bin 815: 3 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 10
Size: 235 Color: 19

Bin 816: 3 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 4
Size: 256 Color: 14

Bin 817: 4 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 18
Size: 440 Color: 5

Bin 818: 4 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 12
Size: 325 Color: 0

Bin 819: 4 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 486 Color: 17

Bin 820: 4 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 15
Size: 427 Color: 0

Bin 821: 4 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 15
Size: 409 Color: 10

Bin 822: 4 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 2
Size: 376 Color: 1

Bin 823: 4 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 2
Size: 364 Color: 8

Bin 824: 4 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 4
Size: 307 Color: 9

Bin 825: 4 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 14
Size: 307 Color: 7

Bin 826: 4 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 8
Size: 471 Color: 10

Bin 827: 5 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 13
Size: 256 Color: 2

Bin 828: 5 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 4
Size: 477 Color: 15

Bin 829: 5 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 8
Size: 409 Color: 12

Bin 830: 5 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 8
Size: 409 Color: 7

Bin 831: 5 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 12
Size: 338 Color: 5

Bin 832: 5 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 16
Size: 277 Color: 17

Bin 833: 5 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 9
Size: 277 Color: 2

Bin 834: 5 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 7
Size: 213 Color: 0

Bin 835: 5 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 9
Size: 427 Color: 16

Bin 836: 6 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 11
Size: 256 Color: 12

Bin 837: 6 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 10
Size: 484 Color: 12

Bin 838: 6 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 8
Size: 453 Color: 17

Bin 839: 6 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 17
Size: 452 Color: 1

Bin 840: 6 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 19
Size: 427 Color: 10

Bin 841: 6 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 408 Color: 16

Bin 842: 6 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 19
Size: 375 Color: 17

Bin 843: 6 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 3
Size: 296 Color: 4

Bin 844: 6 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 296 Color: 17

Bin 845: 6 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 5
Size: 296 Color: 12

Bin 846: 6 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 2
Size: 276 Color: 4

Bin 847: 6 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 7
Size: 408 Color: 11

Bin 848: 6 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 3
Size: 439 Color: 11

Bin 849: 7 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 5
Size: 295 Color: 15

Bin 850: 7 of cap free
Amount of items: 2
Items: 
Size: 497 Color: 15
Size: 497 Color: 3

Bin 851: 7 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 14
Size: 399 Color: 2

Bin 852: 7 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 6
Size: 374 Color: 8

Bin 853: 7 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 8
Size: 374 Color: 19

Bin 854: 7 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 15
Size: 374 Color: 3

Bin 855: 8 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 18
Size: 275 Color: 1

Bin 856: 8 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 374 Color: 12

Bin 857: 8 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 4
Size: 275 Color: 17

Bin 858: 9 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 17
Size: 293 Color: 19

Bin 859: 9 of cap free
Amount of items: 2
Items: 
Size: 496 Color: 12
Size: 496 Color: 8

Bin 860: 10 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 19
Size: 321 Color: 1

Bin 861: 10 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 14
Size: 210 Color: 19

Bin 862: 10 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 15
Size: 423 Color: 18

Bin 863: 10 of cap free
Amount of items: 2
Items: 
Size: 496 Color: 19
Size: 495 Color: 16

Bin 864: 10 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 8
Size: 396 Color: 4

Bin 865: 10 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 11
Size: 372 Color: 1

Bin 866: 10 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 484 Color: 8

Bin 867: 10 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 17
Size: 423 Color: 16

Bin 868: 10 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 484 Color: 16

Bin 869: 11 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 7
Size: 422 Color: 15

Bin 870: 11 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 8
Size: 357 Color: 3

Bin 871: 12 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 14
Size: 370 Color: 13

Bin 872: 14 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 7
Size: 448 Color: 16

Bin 873: 14 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 3
Size: 448 Color: 16

Bin 874: 16 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 6
Size: 418 Color: 1

Bin 875: 17 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 9
Size: 232 Color: 6

Bin 876: 18 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 16
Size: 351 Color: 3

Bin 877: 18 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 11
Size: 447 Color: 3

Bin 878: 22 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 5
Size: 395 Color: 10

Bin 879: 25 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 9
Size: 392 Color: 3

Bin 880: 25 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 319 Color: 12

Bin 881: 27 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 18
Size: 439 Color: 4

Bin 882: 27 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 5
Size: 439 Color: 3

Bin 883: 27 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 8
Size: 227 Color: 14

Bin 884: 28 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 16
Size: 438 Color: 19

Bin 885: 30 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 16
Size: 438 Color: 19

Bin 886: 33 of cap free
Amount of items: 2
Items: 
Size: 484 Color: 18
Size: 484 Color: 3

Bin 887: 35 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 18
Size: 167 Color: 8

Bin 888: 35 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 17
Size: 483 Color: 2

Bin 889: 47 of cap free
Amount of items: 2
Items: 
Size: 477 Color: 17
Size: 477 Color: 14

Bin 890: 79 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 19
Size: 389 Color: 1

Bin 891: 92 of cap free
Amount of items: 2
Items: 
Size: 471 Color: 1
Size: 438 Color: 19

Bin 892: 207 of cap free
Amount of items: 1
Items: 
Size: 794 Color: 13

Bin 893: 220 of cap free
Amount of items: 1
Items: 
Size: 781 Color: 3

Bin 894: 225 of cap free
Amount of items: 1
Items: 
Size: 776 Color: 16

Bin 895: 263 of cap free
Amount of items: 1
Items: 
Size: 738 Color: 8

Bin 896: 272 of cap free
Amount of items: 1
Items: 
Size: 729 Color: 0

Bin 897: 284 of cap free
Amount of items: 1
Items: 
Size: 717 Color: 3

Bin 898: 303 of cap free
Amount of items: 1
Items: 
Size: 698 Color: 14

Total size: 895945
Total free space: 2953

