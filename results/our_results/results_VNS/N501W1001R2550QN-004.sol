Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 463
Size: 280 Color: 139
Size: 267 Color: 85

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 430
Size: 327 Color: 272
Size: 253 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 348
Size: 331 Color: 284
Size: 308 Color: 224

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 385
Size: 362 Color: 345
Size: 253 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 350
Size: 323 Color: 259
Size: 314 Color: 235

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 392
Size: 351 Color: 329
Size: 262 Color: 62

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 412
Size: 317 Color: 245
Size: 276 Color: 124

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 377
Size: 300 Color: 199
Size: 321 Color: 255

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 464
Size: 281 Color: 141
Size: 265 Color: 73

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 378
Size: 359 Color: 340
Size: 261 Color: 58

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 423
Size: 329 Color: 280
Size: 256 Color: 36

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 453
Size: 288 Color: 160
Size: 274 Color: 112

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 431
Size: 314 Color: 234
Size: 266 Color: 77

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 383
Size: 341 Color: 314
Size: 276 Color: 125

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 322
Size: 330 Color: 281
Size: 324 Color: 263

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 405
Size: 306 Color: 215
Size: 299 Color: 191

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 436
Size: 296 Color: 186
Size: 281 Color: 144

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 386
Size: 318 Color: 247
Size: 297 Color: 187

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 459
Size: 288 Color: 159
Size: 270 Color: 97

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 482
Size: 267 Color: 83
Size: 258 Color: 44

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 498
Size: 251 Color: 10
Size: 250 Color: 8

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 382
Size: 333 Color: 291
Size: 284 Color: 152

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 324
Size: 340 Color: 310
Size: 313 Color: 233

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 438
Size: 321 Color: 254
Size: 254 Color: 22

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 364
Size: 354 Color: 331
Size: 276 Color: 119

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 425
Size: 314 Color: 236
Size: 268 Color: 89

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 489
Size: 259 Color: 49
Size: 256 Color: 35

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 359
Size: 317 Color: 244
Size: 314 Color: 237

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 373
Size: 369 Color: 357
Size: 255 Color: 30

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 484
Size: 265 Color: 74
Size: 260 Color: 51

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 471
Size: 277 Color: 128
Size: 261 Color: 57

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 391
Size: 328 Color: 276
Size: 285 Color: 154

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 461
Size: 276 Color: 126
Size: 273 Color: 106

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 470
Size: 277 Color: 131
Size: 266 Color: 76

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 362
Size: 321 Color: 253
Size: 310 Color: 227

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 474
Size: 277 Color: 127
Size: 259 Color: 45

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 437
Size: 300 Color: 197
Size: 276 Color: 120

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 494
Size: 254 Color: 29
Size: 254 Color: 27

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 415
Size: 325 Color: 267
Size: 266 Color: 79

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 416
Size: 334 Color: 293
Size: 257 Color: 40

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 488
Size: 271 Color: 100
Size: 250 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 400
Size: 314 Color: 238
Size: 295 Color: 182

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 450
Size: 295 Color: 183
Size: 269 Color: 91

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 351
Size: 357 Color: 337
Size: 280 Color: 136

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 407
Size: 327 Color: 273
Size: 276 Color: 122

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 414
Size: 326 Color: 270
Size: 267 Color: 84

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 295
Size: 334 Color: 294
Size: 332 Color: 287

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 347
Size: 333 Color: 290
Size: 306 Color: 218

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 356
Size: 367 Color: 354
Size: 265 Color: 70

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 467
Size: 275 Color: 117
Size: 270 Color: 95

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 370
Size: 331 Color: 282
Size: 295 Color: 179

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 372
Size: 324 Color: 262
Size: 300 Color: 198

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 336
Size: 356 Color: 335
Size: 289 Color: 162

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 495
Size: 255 Color: 34
Size: 251 Color: 12

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 485
Size: 265 Color: 72
Size: 259 Color: 46

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 481
Size: 267 Color: 86
Size: 261 Color: 56

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 389
Size: 360 Color: 342
Size: 254 Color: 26

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 497
Size: 253 Color: 21
Size: 251 Color: 13

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 420
Size: 303 Color: 210
Size: 285 Color: 155

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 318
Size: 329 Color: 278
Size: 329 Color: 277

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 388
Size: 339 Color: 309
Size: 276 Color: 121

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 410
Size: 342 Color: 315
Size: 253 Color: 17

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 353
Size: 360 Color: 343
Size: 274 Color: 109

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 480
Size: 272 Color: 105
Size: 262 Color: 61

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 424
Size: 301 Color: 202
Size: 284 Color: 151

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 478
Size: 274 Color: 108
Size: 260 Color: 52

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 477
Size: 274 Color: 113
Size: 261 Color: 54

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 349
Size: 336 Color: 300
Size: 301 Color: 204

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 440
Size: 298 Color: 190
Size: 276 Color: 123

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 496
Size: 254 Color: 25
Size: 251 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 486
Size: 264 Color: 67
Size: 259 Color: 48

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 399
Size: 316 Color: 242
Size: 294 Color: 174

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 363
Size: 335 Color: 298
Size: 295 Color: 178

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 358
Size: 337 Color: 303
Size: 295 Color: 181

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 466
Size: 275 Color: 116
Size: 271 Color: 99

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 355
Size: 337 Color: 304
Size: 296 Color: 184

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 445
Size: 306 Color: 216
Size: 266 Color: 78

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 472
Size: 269 Color: 93
Size: 269 Color: 92

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 491
Size: 265 Color: 71
Size: 250 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 476
Size: 271 Color: 102
Size: 264 Color: 66

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 330
Size: 333 Color: 289
Size: 315 Color: 240

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 390
Size: 322 Color: 256
Size: 292 Color: 172

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 439
Size: 310 Color: 228
Size: 265 Color: 75

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 473
Size: 279 Color: 135
Size: 257 Color: 41

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 393
Size: 332 Color: 288
Size: 281 Color: 142

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 462
Size: 295 Color: 180
Size: 252 Color: 16

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 344
Size: 354 Color: 333
Size: 286 Color: 156

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 376
Size: 340 Color: 311
Size: 282 Color: 147

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 402
Size: 335 Color: 299
Size: 270 Color: 98

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 451
Size: 310 Color: 229
Size: 253 Color: 20

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 460
Size: 291 Color: 168
Size: 263 Color: 63

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 419
Size: 316 Color: 241
Size: 272 Color: 104

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 492
Size: 259 Color: 50
Size: 255 Color: 33

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 418
Size: 339 Color: 308
Size: 250 Color: 6

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 365
Size: 336 Color: 301
Size: 294 Color: 175

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 435
Size: 301 Color: 201
Size: 277 Color: 130

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 360
Size: 340 Color: 312
Size: 291 Color: 167

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 325
Size: 341 Color: 313
Size: 311 Color: 232

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 457
Size: 303 Color: 207
Size: 257 Color: 38

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 465
Size: 280 Color: 138
Size: 266 Color: 81

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 398
Size: 318 Color: 248
Size: 292 Color: 173

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 352
Size: 326 Color: 269
Size: 309 Color: 226

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 404
Size: 347 Color: 321
Size: 258 Color: 43

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 446
Size: 290 Color: 166
Size: 281 Color: 140

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 396
Size: 319 Color: 251
Size: 292 Color: 171

Bin 106: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 500
Size: 500 Color: 499

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 422
Size: 320 Color: 252
Size: 267 Color: 87

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 394
Size: 338 Color: 307
Size: 274 Color: 111

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 397
Size: 322 Color: 257
Size: 289 Color: 164

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 411
Size: 308 Color: 223
Size: 285 Color: 153

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 456
Size: 306 Color: 221
Size: 254 Color: 24

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 493
Size: 257 Color: 39
Size: 254 Color: 28

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 432
Size: 323 Color: 258
Size: 257 Color: 42

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 441
Size: 303 Color: 209
Size: 270 Color: 96

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 444
Size: 290 Color: 165
Size: 282 Color: 149

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 401
Size: 326 Color: 271
Size: 279 Color: 134

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 380
Size: 327 Color: 274
Size: 291 Color: 170

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 379
Size: 338 Color: 306
Size: 282 Color: 146

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 442
Size: 306 Color: 219
Size: 267 Color: 82

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 447
Size: 286 Color: 157
Size: 282 Color: 148

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 367
Size: 342 Color: 316
Size: 287 Color: 158

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 395
Size: 337 Color: 305
Size: 274 Color: 110

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 339
Size: 332 Color: 286
Size: 311 Color: 230

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 433
Size: 299 Color: 192
Size: 280 Color: 137

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 426
Size: 301 Color: 200
Size: 281 Color: 143

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 469
Size: 279 Color: 133
Size: 264 Color: 69

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 346
Size: 335 Color: 296
Size: 304 Color: 213

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 421
Size: 297 Color: 188
Size: 291 Color: 169

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 490
Size: 261 Color: 59
Size: 254 Color: 23

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 328
Size: 350 Color: 327
Size: 301 Color: 203

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 452
Size: 294 Color: 176
Size: 269 Color: 94

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 369
Size: 323 Color: 260
Size: 303 Color: 208

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 384
Size: 315 Color: 239
Size: 301 Color: 205

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 338
Size: 335 Color: 297
Size: 309 Color: 225

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 403
Size: 311 Color: 231
Size: 294 Color: 177

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 374
Size: 319 Color: 249
Size: 304 Color: 212

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 413
Size: 325 Color: 266
Size: 268 Color: 88

Bin 138: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 11
Size: 250 Color: 7
Size: 250 Color: 2
Size: 250 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 454
Size: 306 Color: 220
Size: 256 Color: 37

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 408
Size: 325 Color: 268
Size: 275 Color: 115

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 317
Size: 334 Color: 292
Size: 324 Color: 265

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 361
Size: 332 Color: 285
Size: 299 Color: 193

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 366
Size: 347 Color: 323
Size: 283 Color: 150

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 428
Size: 307 Color: 222
Size: 274 Color: 114

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 381
Size: 346 Color: 319
Size: 271 Color: 101

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 448
Size: 318 Color: 246
Size: 250 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 332
Size: 324 Color: 264
Size: 323 Color: 261

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 406
Size: 303 Color: 211
Size: 300 Color: 194

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 387
Size: 349 Color: 326
Size: 266 Color: 80

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 375
Size: 355 Color: 334
Size: 268 Color: 90

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 487
Size: 263 Color: 65
Size: 260 Color: 53

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 458
Size: 281 Color: 145
Size: 277 Color: 129

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 468
Size: 289 Color: 161
Size: 255 Color: 31

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 329 Color: 279
Size: 252 Color: 15
Size: 420 Color: 429

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 479
Size: 273 Color: 107
Size: 261 Color: 60

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 417
Size: 301 Color: 206
Size: 289 Color: 163

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 449
Size: 317 Color: 243
Size: 250 Color: 4

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 427
Size: 331 Color: 283
Size: 251 Color: 9

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 409
Size: 300 Color: 195
Size: 296 Color: 185

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 434
Size: 319 Color: 250
Size: 259 Color: 47

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 371
Size: 346 Color: 320
Size: 279 Color: 132

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 368
Size: 327 Color: 275
Size: 300 Color: 196

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 341
Size: 336 Color: 302
Size: 306 Color: 217

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 443
Size: 297 Color: 189
Size: 275 Color: 118

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 483
Size: 264 Color: 68
Size: 261 Color: 55

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 455
Size: 306 Color: 214
Size: 255 Color: 32

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 475
Size: 272 Color: 103
Size: 263 Color: 64

Total size: 167167
Total free space: 0

