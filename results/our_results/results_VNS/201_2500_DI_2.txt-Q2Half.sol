Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1283 Color: 1
Size: 631 Color: 1
Size: 96 Color: 0
Size: 30 Color: 0
Size: 8 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 1
Size: 318 Color: 1
Size: 132 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1463 Color: 1
Size: 489 Color: 1
Size: 96 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 1
Size: 733 Color: 1
Size: 146 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 202 Color: 1
Size: 108 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 281 Color: 1
Size: 108 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 1
Size: 487 Color: 1
Size: 96 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 550 Color: 1
Size: 108 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 242 Color: 1
Size: 48 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 390 Color: 1
Size: 84 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1638 Color: 1
Size: 194 Color: 1
Size: 144 Color: 0
Size: 72 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 186 Color: 1
Size: 120 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1629 Color: 1
Size: 325 Color: 1
Size: 54 Color: 0
Size: 40 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 1
Size: 849 Color: 1
Size: 168 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 1
Size: 435 Color: 1
Size: 20 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 1
Size: 246 Color: 1
Size: 120 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 275 Color: 1
Size: 100 Color: 0

Bin 18: 0 of cap free
Amount of items: 4
Items: 
Size: 1674 Color: 1
Size: 198 Color: 1
Size: 140 Color: 0
Size: 36 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 1
Size: 385 Color: 1
Size: 76 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 1
Size: 351 Color: 1
Size: 76 Color: 0

Bin 21: 0 of cap free
Amount of items: 5
Items: 
Size: 1319 Color: 1
Size: 431 Color: 1
Size: 190 Color: 1
Size: 60 Color: 0
Size: 48 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 1
Size: 903 Color: 1
Size: 120 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 1
Size: 357 Color: 1
Size: 160 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 1
Size: 682 Color: 1
Size: 68 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 1
Size: 281 Color: 1
Size: 56 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 1
Size: 424 Color: 1
Size: 208 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 1
Size: 553 Color: 1
Size: 110 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 1
Size: 263 Color: 1
Size: 52 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 1
Size: 258 Color: 1
Size: 36 Color: 0

Bin 30: 0 of cap free
Amount of items: 5
Items: 
Size: 1165 Color: 1
Size: 398 Color: 1
Size: 241 Color: 1
Size: 168 Color: 0
Size: 76 Color: 0

Bin 31: 0 of cap free
Amount of items: 5
Items: 
Size: 1086 Color: 1
Size: 472 Color: 1
Size: 378 Color: 1
Size: 64 Color: 0
Size: 48 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 1
Size: 430 Color: 1
Size: 36 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 1
Size: 802 Color: 1
Size: 220 Color: 0

Bin 34: 0 of cap free
Amount of items: 4
Items: 
Size: 1280 Color: 1
Size: 614 Color: 1
Size: 86 Color: 0
Size: 68 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 1
Size: 273 Color: 1
Size: 62 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 1
Size: 635 Color: 1
Size: 126 Color: 0

Bin 37: 0 of cap free
Amount of items: 4
Items: 
Size: 1588 Color: 1
Size: 314 Color: 1
Size: 126 Color: 0
Size: 20 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 1
Size: 265 Color: 1
Size: 2 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 1
Size: 542 Color: 1
Size: 36 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 382 Color: 1
Size: 76 Color: 0

Bin 41: 0 of cap free
Amount of items: 4
Items: 
Size: 1202 Color: 1
Size: 706 Color: 1
Size: 76 Color: 0
Size: 64 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 1
Size: 463 Color: 1
Size: 58 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 1
Size: 388 Color: 1
Size: 126 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 737 Color: 1
Size: 76 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 1
Size: 696 Color: 1
Size: 60 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 1
Size: 262 Color: 1
Size: 42 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 1
Size: 828 Color: 1
Size: 46 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 170 Color: 1
Size: 84 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 1
Size: 609 Color: 1
Size: 124 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 854 Color: 1
Size: 851 Color: 1
Size: 342 Color: 0

Bin 51: 1 of cap free
Amount of items: 5
Items: 
Size: 731 Color: 1
Size: 626 Color: 1
Size: 338 Color: 1
Size: 316 Color: 0
Size: 36 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 1
Size: 313 Color: 1
Size: 48 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 1
Size: 213 Color: 1
Size: 72 Color: 0

Bin 54: 2 of cap free
Amount of items: 5
Items: 
Size: 1027 Color: 1
Size: 446 Color: 1
Size: 429 Color: 1
Size: 88 Color: 0
Size: 56 Color: 0

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 1389 Color: 1
Size: 510 Color: 1
Size: 146 Color: 0

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 1
Size: 639 Color: 1
Size: 8 Color: 0

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 1
Size: 482 Color: 1
Size: 48 Color: 0

Bin 58: 5 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 1
Size: 551 Color: 1
Size: 54 Color: 0

Bin 59: 29 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 1
Size: 303 Color: 1
Size: 70 Color: 0

Bin 60: 222 of cap free
Amount of items: 1
Items: 
Size: 1826 Color: 1

Bin 61: 226 of cap free
Amount of items: 1
Items: 
Size: 1822 Color: 1

Bin 62: 230 of cap free
Amount of items: 1
Items: 
Size: 1818 Color: 1

Bin 63: 234 of cap free
Amount of items: 1
Items: 
Size: 1814 Color: 1

Bin 64: 238 of cap free
Amount of items: 1
Items: 
Size: 1810 Color: 1

Bin 65: 327 of cap free
Amount of items: 1
Items: 
Size: 1721 Color: 1

Bin 66: 513 of cap free
Amount of items: 1
Items: 
Size: 1535 Color: 1

Total size: 133120
Total free space: 2048

