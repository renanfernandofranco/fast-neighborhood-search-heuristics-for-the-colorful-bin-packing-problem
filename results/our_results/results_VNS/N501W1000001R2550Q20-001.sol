Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 370744 Color: 15
Size: 345974 Color: 9
Size: 283283 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 362310 Color: 17
Size: 319405 Color: 18
Size: 318286 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 384856 Color: 12
Size: 337259 Color: 1
Size: 277886 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 344189 Color: 8
Size: 340216 Color: 4
Size: 315596 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 421296 Color: 18
Size: 291617 Color: 1
Size: 287088 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 375953 Color: 9
Size: 350011 Color: 5
Size: 274037 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 461747 Color: 11
Size: 287364 Color: 6
Size: 250890 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 475943 Color: 9
Size: 272789 Color: 9
Size: 251269 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 412046 Color: 4
Size: 317398 Color: 3
Size: 270557 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373209 Color: 16
Size: 368897 Color: 6
Size: 257895 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 348019 Color: 1
Size: 344701 Color: 7
Size: 307281 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 400001 Color: 2
Size: 303604 Color: 9
Size: 296396 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 368451 Color: 16
Size: 327993 Color: 0
Size: 303557 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 411875 Color: 10
Size: 336192 Color: 6
Size: 251934 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 374833 Color: 8
Size: 318503 Color: 10
Size: 306665 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 437828 Color: 2
Size: 310908 Color: 12
Size: 251265 Color: 15

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 441826 Color: 13
Size: 290954 Color: 17
Size: 267221 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 379559 Color: 5
Size: 328965 Color: 0
Size: 291477 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 389287 Color: 13
Size: 310679 Color: 0
Size: 300035 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 396934 Color: 19
Size: 351954 Color: 18
Size: 251113 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 450068 Color: 1
Size: 289826 Color: 17
Size: 260107 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 351790 Color: 13
Size: 327479 Color: 12
Size: 320732 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 481427 Color: 4
Size: 260148 Color: 15
Size: 258426 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 382003 Color: 19
Size: 313131 Color: 10
Size: 304867 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 489823 Color: 6
Size: 258970 Color: 3
Size: 251208 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 399672 Color: 2
Size: 318846 Color: 14
Size: 281483 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 487350 Color: 19
Size: 257767 Color: 9
Size: 254884 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 399934 Color: 17
Size: 339861 Color: 5
Size: 260206 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 456319 Color: 11
Size: 287534 Color: 18
Size: 256148 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 374483 Color: 0
Size: 320975 Color: 8
Size: 304543 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 377819 Color: 11
Size: 334757 Color: 6
Size: 287425 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 370645 Color: 4
Size: 359438 Color: 4
Size: 269918 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 374981 Color: 17
Size: 358825 Color: 10
Size: 266195 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 398816 Color: 12
Size: 320775 Color: 6
Size: 280410 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 482669 Color: 10
Size: 262343 Color: 17
Size: 254989 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 349061 Color: 7
Size: 340768 Color: 14
Size: 310172 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 396481 Color: 19
Size: 304178 Color: 0
Size: 299342 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 384508 Color: 10
Size: 335688 Color: 6
Size: 279805 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 374950 Color: 1
Size: 353726 Color: 1
Size: 271325 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 486474 Color: 0
Size: 260945 Color: 11
Size: 252582 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 383663 Color: 6
Size: 324547 Color: 11
Size: 291791 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 366736 Color: 19
Size: 352612 Color: 15
Size: 280653 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 369352 Color: 17
Size: 320585 Color: 2
Size: 310064 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 390122 Color: 15
Size: 314284 Color: 1
Size: 295595 Color: 14

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 457850 Color: 9
Size: 282392 Color: 5
Size: 259759 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 474176 Color: 15
Size: 269618 Color: 6
Size: 256207 Color: 5

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 356409 Color: 5
Size: 346305 Color: 3
Size: 297287 Color: 8

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 423081 Color: 0
Size: 312400 Color: 9
Size: 264520 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 469794 Color: 12
Size: 276249 Color: 5
Size: 253958 Color: 19

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 345212 Color: 5
Size: 336298 Color: 17
Size: 318491 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 451898 Color: 4
Size: 291905 Color: 8
Size: 256198 Color: 10

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 480233 Color: 1
Size: 265104 Color: 2
Size: 254664 Color: 16

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 363407 Color: 5
Size: 332824 Color: 11
Size: 303770 Color: 18

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 369525 Color: 1
Size: 338445 Color: 11
Size: 292031 Color: 9

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 431515 Color: 12
Size: 304105 Color: 8
Size: 264381 Color: 13

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 425015 Color: 0
Size: 321701 Color: 2
Size: 253285 Color: 11

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 390418 Color: 1
Size: 312760 Color: 4
Size: 296823 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 376360 Color: 2
Size: 359461 Color: 0
Size: 264180 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 408901 Color: 0
Size: 310126 Color: 11
Size: 280974 Color: 9

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 414809 Color: 0
Size: 297950 Color: 4
Size: 287242 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 401671 Color: 4
Size: 335588 Color: 14
Size: 262742 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 475998 Color: 14
Size: 264471 Color: 10
Size: 259532 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 384712 Color: 17
Size: 328661 Color: 9
Size: 286628 Color: 19

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 434659 Color: 13
Size: 305338 Color: 2
Size: 260004 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 433304 Color: 7
Size: 294147 Color: 16
Size: 272550 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 381475 Color: 8
Size: 335334 Color: 11
Size: 283192 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 360198 Color: 11
Size: 345991 Color: 7
Size: 293812 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 392307 Color: 4
Size: 356092 Color: 3
Size: 251602 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 374336 Color: 0
Size: 331016 Color: 19
Size: 294649 Color: 6

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 406088 Color: 0
Size: 307040 Color: 4
Size: 286873 Color: 10

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 395625 Color: 9
Size: 344151 Color: 3
Size: 260225 Color: 7

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 373180 Color: 11
Size: 353181 Color: 15
Size: 273640 Color: 12

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 379308 Color: 12
Size: 343618 Color: 10
Size: 277075 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 379406 Color: 0
Size: 359740 Color: 17
Size: 260855 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 427404 Color: 7
Size: 320915 Color: 11
Size: 251682 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 409769 Color: 8
Size: 322072 Color: 16
Size: 268160 Color: 12

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 376556 Color: 0
Size: 320736 Color: 11
Size: 302709 Color: 8

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 371541 Color: 11
Size: 326857 Color: 10
Size: 301603 Color: 11

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 468383 Color: 8
Size: 274769 Color: 5
Size: 256849 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 368041 Color: 5
Size: 318076 Color: 15
Size: 313884 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 461307 Color: 1
Size: 284401 Color: 2
Size: 254293 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 369850 Color: 15
Size: 369236 Color: 1
Size: 260915 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 379943 Color: 17
Size: 332937 Color: 17
Size: 287121 Color: 6

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 356292 Color: 5
Size: 330122 Color: 18
Size: 313587 Color: 13

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 363184 Color: 17
Size: 318421 Color: 5
Size: 318396 Color: 6

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 388730 Color: 14
Size: 322155 Color: 18
Size: 289116 Color: 4

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 413331 Color: 15
Size: 309789 Color: 8
Size: 276881 Color: 11

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 353411 Color: 5
Size: 329763 Color: 12
Size: 316827 Color: 16

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 400620 Color: 1
Size: 320433 Color: 11
Size: 278948 Color: 10

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 460071 Color: 11
Size: 288989 Color: 2
Size: 250941 Color: 15

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 392088 Color: 5
Size: 326399 Color: 10
Size: 281514 Color: 19

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 421492 Color: 11
Size: 317873 Color: 7
Size: 260636 Color: 6

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 452729 Color: 5
Size: 277483 Color: 19
Size: 269789 Color: 12

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 483012 Color: 1
Size: 264953 Color: 6
Size: 252036 Color: 11

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 415005 Color: 4
Size: 316593 Color: 11
Size: 268403 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 437226 Color: 16
Size: 290475 Color: 11
Size: 272300 Color: 6

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 454895 Color: 19
Size: 277436 Color: 0
Size: 267670 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 405997 Color: 18
Size: 310081 Color: 17
Size: 283923 Color: 8

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 391115 Color: 13
Size: 308084 Color: 17
Size: 300802 Color: 6

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 342192 Color: 4
Size: 331148 Color: 11
Size: 326661 Color: 8

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 344734 Color: 10
Size: 329923 Color: 10
Size: 325344 Color: 13

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 381836 Color: 19
Size: 319349 Color: 10
Size: 298816 Color: 6

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 435488 Color: 11
Size: 288940 Color: 14
Size: 275573 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 448022 Color: 0
Size: 293455 Color: 12
Size: 258524 Color: 7

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 499746 Color: 10
Size: 250154 Color: 12
Size: 250101 Color: 11

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 410354 Color: 10
Size: 310304 Color: 5
Size: 279343 Color: 18

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 464201 Color: 1
Size: 272059 Color: 18
Size: 263741 Color: 5

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 440617 Color: 16
Size: 284270 Color: 6
Size: 275114 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 435671 Color: 0
Size: 295067 Color: 9
Size: 269263 Color: 19

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 463221 Color: 7
Size: 268779 Color: 2
Size: 268001 Color: 9

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 432253 Color: 14
Size: 301018 Color: 16
Size: 266730 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 370948 Color: 6
Size: 353496 Color: 15
Size: 275557 Color: 7

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 387368 Color: 6
Size: 320792 Color: 7
Size: 291841 Color: 15

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 370306 Color: 10
Size: 319740 Color: 15
Size: 309955 Color: 7

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 395522 Color: 6
Size: 317375 Color: 1
Size: 287104 Color: 9

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 358341 Color: 18
Size: 357315 Color: 12
Size: 284345 Color: 7

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 368258 Color: 0
Size: 327227 Color: 12
Size: 304516 Color: 14

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 402326 Color: 6
Size: 318809 Color: 4
Size: 278866 Color: 19

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 447666 Color: 10
Size: 288859 Color: 6
Size: 263476 Color: 18

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 452010 Color: 5
Size: 297856 Color: 18
Size: 250135 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 425389 Color: 18
Size: 313181 Color: 11
Size: 261431 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 361188 Color: 2
Size: 344083 Color: 9
Size: 294730 Color: 18

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 494346 Color: 14
Size: 253920 Color: 13
Size: 251735 Color: 13

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 393730 Color: 7
Size: 310239 Color: 16
Size: 296032 Color: 15

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 358200 Color: 16
Size: 341323 Color: 1
Size: 300478 Color: 14

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 466485 Color: 19
Size: 269088 Color: 2
Size: 264428 Color: 11

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 440202 Color: 19
Size: 307802 Color: 9
Size: 251997 Color: 10

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 437731 Color: 7
Size: 304133 Color: 12
Size: 258137 Color: 10

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 408229 Color: 4
Size: 315291 Color: 10
Size: 276481 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 488397 Color: 6
Size: 257345 Color: 18
Size: 254259 Color: 10

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 374933 Color: 9
Size: 366959 Color: 17
Size: 258109 Color: 19

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 420377 Color: 12
Size: 299383 Color: 7
Size: 280241 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 380279 Color: 3
Size: 327481 Color: 10
Size: 292241 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 454358 Color: 6
Size: 274363 Color: 6
Size: 271280 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 425964 Color: 3
Size: 312829 Color: 3
Size: 261208 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 388916 Color: 4
Size: 352899 Color: 10
Size: 258186 Color: 5

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 446209 Color: 18
Size: 298486 Color: 13
Size: 255306 Color: 8

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 385931 Color: 17
Size: 342895 Color: 1
Size: 271175 Color: 12

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 391497 Color: 8
Size: 356003 Color: 2
Size: 252501 Color: 16

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 385803 Color: 0
Size: 314944 Color: 2
Size: 299254 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 384540 Color: 18
Size: 344650 Color: 9
Size: 270811 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 428883 Color: 3
Size: 321112 Color: 17
Size: 250006 Color: 15

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 425183 Color: 13
Size: 303153 Color: 8
Size: 271665 Color: 15

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 383692 Color: 15
Size: 308780 Color: 10
Size: 307529 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 406485 Color: 19
Size: 339409 Color: 19
Size: 254107 Color: 12

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 476587 Color: 4
Size: 270601 Color: 2
Size: 252813 Color: 5

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 422407 Color: 2
Size: 312152 Color: 3
Size: 265442 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 381693 Color: 19
Size: 365479 Color: 6
Size: 252829 Color: 17

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 482680 Color: 6
Size: 265154 Color: 10
Size: 252167 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 493269 Color: 11
Size: 253683 Color: 13
Size: 253049 Color: 16

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 392140 Color: 17
Size: 350985 Color: 4
Size: 256876 Color: 10

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 417692 Color: 8
Size: 307431 Color: 18
Size: 274878 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 471235 Color: 14
Size: 266884 Color: 9
Size: 261882 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 489075 Color: 8
Size: 256365 Color: 0
Size: 254561 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 431927 Color: 14
Size: 313730 Color: 15
Size: 254344 Color: 8

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 405029 Color: 9
Size: 301168 Color: 11
Size: 293804 Color: 4

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 493825 Color: 8
Size: 254568 Color: 8
Size: 251608 Color: 6

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 441395 Color: 14
Size: 280907 Color: 9
Size: 277699 Color: 9

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 456232 Color: 13
Size: 283742 Color: 12
Size: 260027 Color: 10

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 403184 Color: 14
Size: 336226 Color: 0
Size: 260591 Color: 7

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 347238 Color: 6
Size: 335349 Color: 16
Size: 317414 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 376322 Color: 5
Size: 351590 Color: 8
Size: 272089 Color: 6

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 486232 Color: 1
Size: 260314 Color: 19
Size: 253455 Color: 17

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 469951 Color: 12
Size: 273952 Color: 1
Size: 256098 Color: 12

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 424622 Color: 16
Size: 323949 Color: 15
Size: 251430 Color: 15

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 431625 Color: 5
Size: 294314 Color: 13
Size: 274062 Color: 10

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 461789 Color: 1
Size: 276866 Color: 6
Size: 261346 Color: 19

Total size: 167000167
Total free space: 0

