Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 324 Color: 14
Size: 276 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 314 Color: 19
Size: 413 Color: 12
Size: 273 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 282 Color: 1
Size: 256 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 312 Color: 14
Size: 259 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 14
Size: 360 Color: 0
Size: 273 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 8
Size: 259 Color: 16
Size: 253 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 266 Color: 11
Size: 252 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 7
Size: 282 Color: 8
Size: 271 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 3
Size: 269 Color: 13
Size: 259 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 294 Color: 5
Size: 264 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 9
Size: 362 Color: 16
Size: 274 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 18
Size: 329 Color: 8
Size: 265 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 12
Size: 313 Color: 10
Size: 291 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 312 Color: 18
Size: 262 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 338 Color: 0
Size: 279 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 305 Color: 11
Size: 257 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 285 Color: 3
Size: 259 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 3
Size: 289 Color: 14
Size: 256 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 9
Size: 350 Color: 7
Size: 299 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8
Size: 293 Color: 12
Size: 270 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 18
Size: 284 Color: 17
Size: 263 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 307 Color: 10
Size: 289 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 17
Size: 277 Color: 15
Size: 260 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 10
Size: 310 Color: 2
Size: 273 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 295 Color: 5
Size: 294 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 361 Color: 13
Size: 263 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 16
Size: 259 Color: 10
Size: 256 Color: 10

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 265 Color: 13
Size: 254 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 294 Color: 13
Size: 289 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 12
Size: 305 Color: 12
Size: 260 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 9
Size: 364 Color: 10
Size: 253 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 13
Size: 332 Color: 6
Size: 267 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7
Size: 363 Color: 5
Size: 250 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 360 Color: 7
Size: 250 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 19
Size: 338 Color: 12
Size: 305 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 15
Size: 298 Color: 10
Size: 255 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 17
Size: 287 Color: 16
Size: 284 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 14
Size: 366 Color: 1
Size: 253 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 11
Size: 315 Color: 18
Size: 315 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 16
Size: 284 Color: 15
Size: 277 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 11
Size: 280 Color: 8
Size: 259 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 308 Color: 14
Size: 296 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 15
Size: 323 Color: 11
Size: 322 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 278 Color: 18
Size: 269 Color: 9

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 5
Size: 303 Color: 19
Size: 302 Color: 10

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 260 Color: 17
Size: 251 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 7
Size: 312 Color: 2
Size: 272 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 327 Color: 13
Size: 256 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 6
Size: 268 Color: 3
Size: 260 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 305 Color: 4
Size: 278 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 8
Size: 261 Color: 4
Size: 255 Color: 15

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 334 Color: 4
Size: 296 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 3
Size: 316 Color: 6
Size: 264 Color: 16

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 321 Color: 10
Size: 262 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 267 Color: 13
Size: 261 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 13
Size: 324 Color: 15
Size: 261 Color: 10

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 258 Color: 2
Size: 253 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 18
Size: 254 Color: 17
Size: 254 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 17
Size: 270 Color: 3
Size: 252 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 11
Size: 250 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 13
Size: 314 Color: 11
Size: 260 Color: 8

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 10
Size: 315 Color: 13
Size: 253 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 13
Size: 368 Color: 2
Size: 250 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 10
Size: 318 Color: 14
Size: 298 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 19
Size: 350 Color: 8
Size: 294 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 12
Size: 284 Color: 5
Size: 262 Color: 15

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 6
Size: 328 Color: 6
Size: 300 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 269 Color: 4
Size: 254 Color: 12

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 12
Size: 357 Color: 19
Size: 266 Color: 7

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 4
Size: 299 Color: 13
Size: 289 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 297 Color: 18
Size: 297 Color: 8

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 16
Size: 253 Color: 13
Size: 250 Color: 12

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 322 Color: 3
Size: 259 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 19
Size: 360 Color: 1
Size: 268 Color: 16

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 4
Size: 285 Color: 18
Size: 261 Color: 5

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 265 Color: 12
Size: 250 Color: 14

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 317 Color: 8
Size: 307 Color: 14

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 314 Color: 0
Size: 255 Color: 14

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 17
Size: 285 Color: 12
Size: 274 Color: 12

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9
Size: 278 Color: 6
Size: 250 Color: 7

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 4
Size: 324 Color: 8
Size: 273 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 5
Size: 252 Color: 9
Size: 250 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 296 Color: 6
Size: 268 Color: 8

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 14
Size: 296 Color: 11
Size: 286 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 18
Size: 350 Color: 16
Size: 270 Color: 7

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 0
Size: 272 Color: 3
Size: 254 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 11
Size: 259 Color: 6
Size: 257 Color: 12

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 16
Size: 351 Color: 5
Size: 265 Color: 10

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 278 Color: 1
Size: 256 Color: 12

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 318 Color: 15
Size: 290 Color: 8

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 17
Size: 282 Color: 6
Size: 258 Color: 19

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 9
Size: 292 Color: 12
Size: 271 Color: 3

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 16
Size: 332 Color: 1
Size: 256 Color: 16

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 13
Size: 257 Color: 9
Size: 253 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 15
Size: 306 Color: 7
Size: 257 Color: 12

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 12
Size: 284 Color: 7
Size: 267 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 18
Size: 252 Color: 19
Size: 250 Color: 13

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 6
Size: 334 Color: 17
Size: 276 Color: 14

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 356 Color: 12
Size: 275 Color: 9

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 307 Color: 9
Size: 286 Color: 5

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 288 Color: 5
Size: 260 Color: 17

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8
Size: 335 Color: 6
Size: 261 Color: 9

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 276 Color: 17
Size: 265 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 15
Size: 304 Color: 1
Size: 250 Color: 5

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 360 Color: 10
Size: 258 Color: 14

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 280 Color: 8
Size: 253 Color: 14

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 10
Size: 261 Color: 14
Size: 254 Color: 16

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 7
Size: 294 Color: 1
Size: 260 Color: 16

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 0
Size: 250 Color: 14

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 18
Size: 345 Color: 6
Size: 268 Color: 5

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9
Size: 275 Color: 11
Size: 267 Color: 9

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 12
Size: 265 Color: 16
Size: 254 Color: 17

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 0
Size: 299 Color: 17
Size: 257 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 350 Color: 8
Size: 280 Color: 13

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 0
Size: 289 Color: 2
Size: 279 Color: 9

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 10
Size: 276 Color: 8
Size: 250 Color: 8

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 19
Size: 313 Color: 13
Size: 257 Color: 13

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 13
Size: 297 Color: 7
Size: 283 Color: 16

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 6
Size: 276 Color: 2
Size: 254 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 9
Size: 357 Color: 16
Size: 263 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 7
Size: 280 Color: 10
Size: 263 Color: 16

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 9
Size: 318 Color: 2
Size: 305 Color: 4

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 11
Size: 316 Color: 13
Size: 284 Color: 6

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 6
Size: 277 Color: 13
Size: 252 Color: 17

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 7
Size: 331 Color: 4
Size: 252 Color: 15

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 16
Size: 333 Color: 14
Size: 312 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 5
Size: 306 Color: 9
Size: 292 Color: 10

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 11
Size: 366 Color: 2
Size: 252 Color: 7

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 15
Size: 331 Color: 9
Size: 252 Color: 8

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 5
Size: 261 Color: 5
Size: 253 Color: 9

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 19
Size: 331 Color: 6
Size: 291 Color: 9

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 16
Size: 352 Color: 4
Size: 274 Color: 7

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 327 Color: 16
Size: 290 Color: 17

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 368 Color: 14
Size: 251 Color: 15

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 3
Size: 310 Color: 11
Size: 261 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 320 Color: 12
Size: 288 Color: 18

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 299 Color: 14
Size: 288 Color: 15

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 15
Size: 263 Color: 12
Size: 261 Color: 16

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 3
Size: 356 Color: 18
Size: 283 Color: 18

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 333 Color: 16
Size: 256 Color: 10

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 325 Color: 8
Size: 257 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 272 Color: 3
Size: 261 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 9
Size: 280 Color: 4
Size: 252 Color: 2

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 366 Color: 2
Size: 255 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 266 Color: 4
Size: 251 Color: 9

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 340 Color: 11
Size: 290 Color: 14

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 352 Color: 0
Size: 277 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 9
Size: 303 Color: 7
Size: 257 Color: 10

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 0
Size: 335 Color: 0
Size: 312 Color: 13

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 8
Size: 330 Color: 13
Size: 291 Color: 10

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 8
Size: 288 Color: 4
Size: 264 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 6
Size: 341 Color: 13
Size: 267 Color: 19

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 326 Color: 5
Size: 279 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 8
Size: 272 Color: 18
Size: 259 Color: 14

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 312 Color: 10
Size: 266 Color: 17

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 13
Size: 300 Color: 1
Size: 250 Color: 6

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 8
Size: 332 Color: 11
Size: 295 Color: 13

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 289 Color: 8
Size: 277 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 17
Size: 312 Color: 18
Size: 257 Color: 4

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 11
Size: 309 Color: 9
Size: 305 Color: 6

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 277 Color: 6
Size: 264 Color: 11

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 19
Size: 283 Color: 6
Size: 254 Color: 14

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 17
Size: 264 Color: 15
Size: 263 Color: 7

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 7
Size: 271 Color: 5
Size: 260 Color: 6

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 16
Size: 278 Color: 8
Size: 260 Color: 7

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 18
Size: 288 Color: 6
Size: 252 Color: 14

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 333 Color: 18
Size: 280 Color: 3

Total size: 167000
Total free space: 0

