Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1426 Color: 1
Size: 620 Color: 1
Size: 176 Color: 0
Size: 144 Color: 1
Size: 90 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 1
Size: 1023 Color: 1
Size: 204 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 866 Color: 1
Size: 226 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 1022 Color: 1
Size: 976 Color: 1
Size: 330 Color: 1
Size: 88 Color: 0
Size: 40 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 1
Size: 232 Color: 1
Size: 76 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 1
Size: 774 Color: 1
Size: 126 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 1
Size: 460 Color: 1
Size: 88 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 1018 Color: 1
Size: 112 Color: 0
Size: 88 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 1
Size: 756 Color: 1
Size: 56 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 1
Size: 457 Color: 1
Size: 90 Color: 0

Bin 11: 0 of cap free
Amount of items: 7
Items: 
Size: 887 Color: 1
Size: 840 Color: 1
Size: 525 Color: 1
Size: 88 Color: 0
Size: 48 Color: 0
Size: 48 Color: 0
Size: 20 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 1
Size: 918 Color: 1
Size: 96 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 1
Size: 730 Color: 1
Size: 144 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 1
Size: 242 Color: 1
Size: 48 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 1
Size: 729 Color: 1
Size: 144 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 164 Color: 1
Size: 104 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 1
Size: 386 Color: 1
Size: 8 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 1
Size: 226 Color: 1
Size: 28 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 1
Size: 453 Color: 1
Size: 2 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 1
Size: 535 Color: 1
Size: 106 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 348 Color: 1
Size: 80 Color: 0

Bin 23: 0 of cap free
Amount of items: 5
Items: 
Size: 1423 Color: 1
Size: 398 Color: 1
Size: 389 Color: 1
Size: 172 Color: 0
Size: 74 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 1
Size: 300 Color: 1
Size: 56 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 1
Size: 330 Color: 1
Size: 148 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 1
Size: 342 Color: 1
Size: 64 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 631 Color: 1
Size: 124 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2106 Color: 1
Size: 262 Color: 1
Size: 88 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 282 Color: 1
Size: 56 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 542 Color: 1
Size: 104 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 1
Size: 580 Color: 1
Size: 160 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 1
Size: 331 Color: 1
Size: 200 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 1
Size: 436 Color: 1
Size: 128 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 1
Size: 404 Color: 1
Size: 50 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 1
Size: 524 Color: 1
Size: 80 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1020 Color: 1
Size: 200 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 618 Color: 1
Size: 120 Color: 0

Bin 38: 0 of cap free
Amount of items: 4
Items: 
Size: 1894 Color: 1
Size: 470 Color: 1
Size: 48 Color: 0
Size: 44 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 1
Size: 633 Color: 1
Size: 402 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 1
Size: 684 Color: 1
Size: 76 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 1
Size: 289 Color: 1
Size: 106 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 654 Color: 1
Size: 128 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 1924 Color: 1
Size: 384 Color: 1
Size: 148 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1991 Color: 1
Size: 260 Color: 1
Size: 204 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 381 Color: 1
Size: 280 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 1
Size: 568 Color: 1
Size: 76 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 1
Size: 539 Color: 1
Size: 152 Color: 0

Bin 48: 1 of cap free
Amount of items: 5
Items: 
Size: 1468 Color: 1
Size: 458 Color: 1
Size: 449 Color: 1
Size: 64 Color: 0
Size: 16 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 1
Size: 512 Color: 1
Size: 32 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 1
Size: 637 Color: 1
Size: 120 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 1
Size: 916 Color: 1
Size: 8 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 1
Size: 253 Color: 1
Size: 48 Color: 0

Bin 53: 3 of cap free
Amount of items: 4
Items: 
Size: 2005 Color: 1
Size: 272 Color: 1
Size: 88 Color: 0
Size: 88 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 861 Color: 1
Size: 56 Color: 0

Bin 55: 7 of cap free
Amount of items: 3
Items: 
Size: 1913 Color: 1
Size: 444 Color: 1
Size: 92 Color: 0

Bin 56: 7 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 1
Size: 828 Color: 1
Size: 64 Color: 0

Bin 57: 9 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 443 Color: 1
Size: 64 Color: 0

Bin 58: 13 of cap free
Amount of items: 5
Items: 
Size: 1230 Color: 1
Size: 751 Color: 1
Size: 228 Color: 0
Size: 126 Color: 0
Size: 108 Color: 1

Bin 59: 51 of cap free
Amount of items: 3
Items: 
Size: 1827 Color: 1
Size: 554 Color: 1
Size: 24 Color: 0

Bin 60: 264 of cap free
Amount of items: 1
Items: 
Size: 2192 Color: 1

Bin 61: 270 of cap free
Amount of items: 1
Items: 
Size: 2186 Color: 1

Bin 62: 287 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 1
Size: 476 Color: 0

Bin 63: 310 of cap free
Amount of items: 1
Items: 
Size: 2146 Color: 1

Bin 64: 345 of cap free
Amount of items: 1
Items: 
Size: 2111 Color: 1

Bin 65: 412 of cap free
Amount of items: 1
Items: 
Size: 2044 Color: 1

Bin 66: 462 of cap free
Amount of items: 1
Items: 
Size: 1994 Color: 1

Total size: 159640
Total free space: 2456

