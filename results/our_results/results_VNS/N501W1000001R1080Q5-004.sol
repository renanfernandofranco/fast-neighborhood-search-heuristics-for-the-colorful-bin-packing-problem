Capicity Bin: 1000001
Lower Bound: 229

Bins used: 231
Amount of Colors: 5

Bin 1: 8 of cap free
Amount of items: 2
Items: 
Size: 571262 Color: 4
Size: 428731 Color: 3

Bin 2: 10 of cap free
Amount of items: 2
Items: 
Size: 507484 Color: 1
Size: 492507 Color: 2

Bin 3: 16 of cap free
Amount of items: 3
Items: 
Size: 694993 Color: 2
Size: 157554 Color: 0
Size: 147438 Color: 1

Bin 4: 34 of cap free
Amount of items: 2
Items: 
Size: 698103 Color: 0
Size: 301864 Color: 2

Bin 5: 36 of cap free
Amount of items: 2
Items: 
Size: 794084 Color: 1
Size: 205881 Color: 3

Bin 6: 51 of cap free
Amount of items: 3
Items: 
Size: 638389 Color: 1
Size: 187715 Color: 4
Size: 173846 Color: 0

Bin 7: 72 of cap free
Amount of items: 2
Items: 
Size: 585962 Color: 1
Size: 413967 Color: 0

Bin 8: 90 of cap free
Amount of items: 3
Items: 
Size: 568684 Color: 4
Size: 257747 Color: 1
Size: 173480 Color: 2

Bin 9: 114 of cap free
Amount of items: 2
Items: 
Size: 719850 Color: 0
Size: 280037 Color: 4

Bin 10: 118 of cap free
Amount of items: 3
Items: 
Size: 565380 Color: 3
Size: 256323 Color: 1
Size: 178180 Color: 3

Bin 11: 118 of cap free
Amount of items: 2
Items: 
Size: 781389 Color: 4
Size: 218494 Color: 2

Bin 12: 141 of cap free
Amount of items: 2
Items: 
Size: 576440 Color: 1
Size: 423420 Color: 3

Bin 13: 147 of cap free
Amount of items: 2
Items: 
Size: 709712 Color: 1
Size: 290142 Color: 4

Bin 14: 163 of cap free
Amount of items: 3
Items: 
Size: 672256 Color: 1
Size: 170392 Color: 4
Size: 157190 Color: 3

Bin 15: 167 of cap free
Amount of items: 2
Items: 
Size: 519972 Color: 2
Size: 479862 Color: 1

Bin 16: 173 of cap free
Amount of items: 2
Items: 
Size: 676054 Color: 2
Size: 323774 Color: 3

Bin 17: 197 of cap free
Amount of items: 2
Items: 
Size: 582267 Color: 0
Size: 417537 Color: 4

Bin 18: 200 of cap free
Amount of items: 2
Items: 
Size: 649992 Color: 2
Size: 349809 Color: 4

Bin 19: 206 of cap free
Amount of items: 2
Items: 
Size: 614690 Color: 2
Size: 385105 Color: 0

Bin 20: 209 of cap free
Amount of items: 3
Items: 
Size: 630341 Color: 0
Size: 195249 Color: 1
Size: 174202 Color: 2

Bin 21: 209 of cap free
Amount of items: 2
Items: 
Size: 768992 Color: 3
Size: 230800 Color: 4

Bin 22: 252 of cap free
Amount of items: 2
Items: 
Size: 662129 Color: 3
Size: 337620 Color: 0

Bin 23: 256 of cap free
Amount of items: 2
Items: 
Size: 636185 Color: 2
Size: 363560 Color: 0

Bin 24: 262 of cap free
Amount of items: 2
Items: 
Size: 664991 Color: 3
Size: 334748 Color: 2

Bin 25: 293 of cap free
Amount of items: 2
Items: 
Size: 647026 Color: 3
Size: 352682 Color: 1

Bin 26: 310 of cap free
Amount of items: 2
Items: 
Size: 604923 Color: 4
Size: 394768 Color: 3

Bin 27: 319 of cap free
Amount of items: 2
Items: 
Size: 693915 Color: 2
Size: 305767 Color: 0

Bin 28: 340 of cap free
Amount of items: 2
Items: 
Size: 648125 Color: 0
Size: 351536 Color: 3

Bin 29: 341 of cap free
Amount of items: 2
Items: 
Size: 575747 Color: 3
Size: 423913 Color: 4

Bin 30: 341 of cap free
Amount of items: 3
Items: 
Size: 782077 Color: 0
Size: 116018 Color: 4
Size: 101565 Color: 1

Bin 31: 350 of cap free
Amount of items: 3
Items: 
Size: 696208 Color: 1
Size: 152419 Color: 0
Size: 151024 Color: 4

Bin 32: 355 of cap free
Amount of items: 2
Items: 
Size: 768940 Color: 1
Size: 230706 Color: 2

Bin 33: 366 of cap free
Amount of items: 3
Items: 
Size: 686103 Color: 0
Size: 162932 Color: 0
Size: 150600 Color: 1

Bin 34: 380 of cap free
Amount of items: 3
Items: 
Size: 705750 Color: 4
Size: 148820 Color: 2
Size: 145051 Color: 4

Bin 35: 383 of cap free
Amount of items: 2
Items: 
Size: 679506 Color: 0
Size: 320112 Color: 4

Bin 36: 403 of cap free
Amount of items: 3
Items: 
Size: 644336 Color: 1
Size: 185238 Color: 1
Size: 170024 Color: 4

Bin 37: 403 of cap free
Amount of items: 3
Items: 
Size: 681995 Color: 0
Size: 159235 Color: 4
Size: 158368 Color: 1

Bin 38: 419 of cap free
Amount of items: 2
Items: 
Size: 721630 Color: 3
Size: 277952 Color: 2

Bin 39: 429 of cap free
Amount of items: 3
Items: 
Size: 715836 Color: 3
Size: 142301 Color: 1
Size: 141435 Color: 4

Bin 40: 431 of cap free
Amount of items: 2
Items: 
Size: 572978 Color: 1
Size: 426592 Color: 4

Bin 41: 435 of cap free
Amount of items: 2
Items: 
Size: 776611 Color: 3
Size: 222955 Color: 0

Bin 42: 438 of cap free
Amount of items: 3
Items: 
Size: 736706 Color: 3
Size: 132198 Color: 1
Size: 130659 Color: 3

Bin 43: 449 of cap free
Amount of items: 2
Items: 
Size: 684891 Color: 3
Size: 314661 Color: 4

Bin 44: 450 of cap free
Amount of items: 2
Items: 
Size: 508742 Color: 2
Size: 490809 Color: 4

Bin 45: 507 of cap free
Amount of items: 3
Items: 
Size: 723064 Color: 3
Size: 139735 Color: 2
Size: 136695 Color: 3

Bin 46: 521 of cap free
Amount of items: 2
Items: 
Size: 692537 Color: 0
Size: 306943 Color: 3

Bin 47: 528 of cap free
Amount of items: 2
Items: 
Size: 771793 Color: 1
Size: 227680 Color: 0

Bin 48: 533 of cap free
Amount of items: 2
Items: 
Size: 791802 Color: 0
Size: 207666 Color: 3

Bin 49: 563 of cap free
Amount of items: 3
Items: 
Size: 621001 Color: 0
Size: 203437 Color: 1
Size: 175000 Color: 0

Bin 50: 573 of cap free
Amount of items: 2
Items: 
Size: 588166 Color: 2
Size: 411262 Color: 1

Bin 51: 578 of cap free
Amount of items: 2
Items: 
Size: 773897 Color: 0
Size: 225526 Color: 3

Bin 52: 599 of cap free
Amount of items: 2
Items: 
Size: 700626 Color: 0
Size: 298776 Color: 4

Bin 53: 599 of cap free
Amount of items: 2
Items: 
Size: 772231 Color: 4
Size: 227171 Color: 1

Bin 54: 634 of cap free
Amount of items: 2
Items: 
Size: 570213 Color: 4
Size: 429154 Color: 2

Bin 55: 662 of cap free
Amount of items: 2
Items: 
Size: 549258 Color: 2
Size: 450081 Color: 4

Bin 56: 682 of cap free
Amount of items: 2
Items: 
Size: 593121 Color: 0
Size: 406198 Color: 4

Bin 57: 691 of cap free
Amount of items: 2
Items: 
Size: 775895 Color: 0
Size: 223415 Color: 4

Bin 58: 700 of cap free
Amount of items: 2
Items: 
Size: 714554 Color: 4
Size: 284747 Color: 0

Bin 59: 704 of cap free
Amount of items: 2
Items: 
Size: 553068 Color: 1
Size: 446229 Color: 2

Bin 60: 707 of cap free
Amount of items: 2
Items: 
Size: 788097 Color: 0
Size: 211197 Color: 2

Bin 61: 719 of cap free
Amount of items: 3
Items: 
Size: 354772 Color: 3
Size: 327076 Color: 0
Size: 317434 Color: 2

Bin 62: 722 of cap free
Amount of items: 2
Items: 
Size: 549104 Color: 3
Size: 450175 Color: 2

Bin 63: 743 of cap free
Amount of items: 3
Items: 
Size: 649080 Color: 0
Size: 184113 Color: 1
Size: 166065 Color: 4

Bin 64: 753 of cap free
Amount of items: 2
Items: 
Size: 670995 Color: 4
Size: 328253 Color: 0

Bin 65: 754 of cap free
Amount of items: 2
Items: 
Size: 591765 Color: 1
Size: 407482 Color: 4

Bin 66: 772 of cap free
Amount of items: 3
Items: 
Size: 672871 Color: 4
Size: 171861 Color: 1
Size: 154497 Color: 3

Bin 67: 796 of cap free
Amount of items: 2
Items: 
Size: 690656 Color: 1
Size: 308549 Color: 3

Bin 68: 803 of cap free
Amount of items: 2
Items: 
Size: 652497 Color: 1
Size: 346701 Color: 4

Bin 69: 819 of cap free
Amount of items: 2
Items: 
Size: 543808 Color: 4
Size: 455374 Color: 3

Bin 70: 837 of cap free
Amount of items: 2
Items: 
Size: 530034 Color: 4
Size: 469130 Color: 2

Bin 71: 848 of cap free
Amount of items: 2
Items: 
Size: 776983 Color: 3
Size: 222170 Color: 1

Bin 72: 854 of cap free
Amount of items: 3
Items: 
Size: 710026 Color: 0
Size: 146074 Color: 4
Size: 143047 Color: 4

Bin 73: 879 of cap free
Amount of items: 2
Items: 
Size: 715331 Color: 0
Size: 283791 Color: 3

Bin 74: 960 of cap free
Amount of items: 2
Items: 
Size: 606019 Color: 3
Size: 393022 Color: 1

Bin 75: 977 of cap free
Amount of items: 2
Items: 
Size: 630430 Color: 3
Size: 368594 Color: 0

Bin 76: 1024 of cap free
Amount of items: 2
Items: 
Size: 643711 Color: 4
Size: 355266 Color: 2

Bin 77: 1074 of cap free
Amount of items: 2
Items: 
Size: 700187 Color: 1
Size: 298740 Color: 0

Bin 78: 1077 of cap free
Amount of items: 2
Items: 
Size: 764869 Color: 3
Size: 234055 Color: 4

Bin 79: 1087 of cap free
Amount of items: 3
Items: 
Size: 680790 Color: 3
Size: 162181 Color: 3
Size: 155943 Color: 1

Bin 80: 1115 of cap free
Amount of items: 2
Items: 
Size: 515093 Color: 4
Size: 483793 Color: 2

Bin 81: 1152 of cap free
Amount of items: 3
Items: 
Size: 436801 Color: 3
Size: 301164 Color: 4
Size: 260884 Color: 1

Bin 82: 1228 of cap free
Amount of items: 2
Items: 
Size: 582838 Color: 1
Size: 415935 Color: 2

Bin 83: 1230 of cap free
Amount of items: 2
Items: 
Size: 519699 Color: 0
Size: 479072 Color: 1

Bin 84: 1276 of cap free
Amount of items: 2
Items: 
Size: 532991 Color: 0
Size: 465734 Color: 4

Bin 85: 1297 of cap free
Amount of items: 2
Items: 
Size: 777904 Color: 0
Size: 220800 Color: 3

Bin 86: 1298 of cap free
Amount of items: 2
Items: 
Size: 653724 Color: 3
Size: 344979 Color: 0

Bin 87: 1298 of cap free
Amount of items: 2
Items: 
Size: 773044 Color: 1
Size: 225659 Color: 4

Bin 88: 1317 of cap free
Amount of items: 2
Items: 
Size: 638271 Color: 0
Size: 360413 Color: 1

Bin 89: 1325 of cap free
Amount of items: 2
Items: 
Size: 542305 Color: 2
Size: 456371 Color: 3

Bin 90: 1391 of cap free
Amount of items: 2
Items: 
Size: 663887 Color: 4
Size: 334723 Color: 3

Bin 91: 1422 of cap free
Amount of items: 3
Items: 
Size: 756027 Color: 2
Size: 124467 Color: 2
Size: 118085 Color: 3

Bin 92: 1453 of cap free
Amount of items: 3
Items: 
Size: 759887 Color: 1
Size: 124534 Color: 0
Size: 114127 Color: 2

Bin 93: 1461 of cap free
Amount of items: 2
Items: 
Size: 752470 Color: 2
Size: 246070 Color: 3

Bin 94: 1467 of cap free
Amount of items: 2
Items: 
Size: 571074 Color: 0
Size: 427460 Color: 4

Bin 95: 1467 of cap free
Amount of items: 2
Items: 
Size: 594891 Color: 3
Size: 403643 Color: 1

Bin 96: 1509 of cap free
Amount of items: 3
Items: 
Size: 610792 Color: 1
Size: 194291 Color: 4
Size: 193409 Color: 2

Bin 97: 1517 of cap free
Amount of items: 2
Items: 
Size: 659837 Color: 2
Size: 338647 Color: 1

Bin 98: 1524 of cap free
Amount of items: 2
Items: 
Size: 730545 Color: 1
Size: 267932 Color: 4

Bin 99: 1545 of cap free
Amount of items: 2
Items: 
Size: 730454 Color: 2
Size: 268002 Color: 0

Bin 100: 1550 of cap free
Amount of items: 2
Items: 
Size: 646100 Color: 4
Size: 352351 Color: 3

Bin 101: 1578 of cap free
Amount of items: 3
Items: 
Size: 553797 Color: 4
Size: 266682 Color: 1
Size: 177944 Color: 0

Bin 102: 1589 of cap free
Amount of items: 2
Items: 
Size: 556861 Color: 1
Size: 441551 Color: 3

Bin 103: 1626 of cap free
Amount of items: 2
Items: 
Size: 510545 Color: 1
Size: 487830 Color: 2

Bin 104: 1652 of cap free
Amount of items: 2
Items: 
Size: 568431 Color: 1
Size: 429918 Color: 3

Bin 105: 1659 of cap free
Amount of items: 2
Items: 
Size: 584157 Color: 0
Size: 414185 Color: 1

Bin 106: 1681 of cap free
Amount of items: 3
Items: 
Size: 645128 Color: 3
Size: 187650 Color: 1
Size: 165542 Color: 0

Bin 107: 1686 of cap free
Amount of items: 2
Items: 
Size: 659539 Color: 3
Size: 338776 Color: 0

Bin 108: 1762 of cap free
Amount of items: 2
Items: 
Size: 730441 Color: 4
Size: 267798 Color: 3

Bin 109: 1770 of cap free
Amount of items: 2
Items: 
Size: 552875 Color: 1
Size: 445356 Color: 2

Bin 110: 1854 of cap free
Amount of items: 3
Items: 
Size: 620843 Color: 2
Size: 197496 Color: 1
Size: 179808 Color: 2

Bin 111: 1858 of cap free
Amount of items: 2
Items: 
Size: 665715 Color: 4
Size: 332428 Color: 2

Bin 112: 1916 of cap free
Amount of items: 2
Items: 
Size: 677327 Color: 0
Size: 320758 Color: 2

Bin 113: 1934 of cap free
Amount of items: 2
Items: 
Size: 522078 Color: 1
Size: 475989 Color: 3

Bin 114: 1986 of cap free
Amount of items: 2
Items: 
Size: 577971 Color: 4
Size: 420044 Color: 3

Bin 115: 2077 of cap free
Amount of items: 2
Items: 
Size: 531719 Color: 3
Size: 466205 Color: 0

Bin 116: 2102 of cap free
Amount of items: 2
Items: 
Size: 735897 Color: 0
Size: 262002 Color: 3

Bin 117: 2177 of cap free
Amount of items: 3
Items: 
Size: 758456 Color: 0
Size: 119836 Color: 1
Size: 119532 Color: 3

Bin 118: 2221 of cap free
Amount of items: 2
Items: 
Size: 779661 Color: 4
Size: 218119 Color: 1

Bin 119: 2236 of cap free
Amount of items: 2
Items: 
Size: 540375 Color: 4
Size: 457390 Color: 1

Bin 120: 2309 of cap free
Amount of items: 2
Items: 
Size: 620563 Color: 4
Size: 377129 Color: 2

Bin 121: 2330 of cap free
Amount of items: 2
Items: 
Size: 757398 Color: 2
Size: 240273 Color: 4

Bin 122: 2339 of cap free
Amount of items: 2
Items: 
Size: 720085 Color: 3
Size: 277577 Color: 2

Bin 123: 2400 of cap free
Amount of items: 2
Items: 
Size: 632603 Color: 2
Size: 364998 Color: 1

Bin 124: 2423 of cap free
Amount of items: 2
Items: 
Size: 768500 Color: 4
Size: 229078 Color: 1

Bin 125: 2441 of cap free
Amount of items: 2
Items: 
Size: 521111 Color: 0
Size: 476449 Color: 2

Bin 126: 2473 of cap free
Amount of items: 2
Items: 
Size: 510000 Color: 3
Size: 487528 Color: 1

Bin 127: 2538 of cap free
Amount of items: 2
Items: 
Size: 540285 Color: 0
Size: 457178 Color: 2

Bin 128: 2592 of cap free
Amount of items: 2
Items: 
Size: 716511 Color: 4
Size: 280898 Color: 2

Bin 129: 2630 of cap free
Amount of items: 2
Items: 
Size: 500934 Color: 1
Size: 496437 Color: 4

Bin 130: 2642 of cap free
Amount of items: 3
Items: 
Size: 785732 Color: 3
Size: 105819 Color: 4
Size: 105808 Color: 1

Bin 131: 2671 of cap free
Amount of items: 2
Items: 
Size: 616014 Color: 1
Size: 381316 Color: 0

Bin 132: 2689 of cap free
Amount of items: 2
Items: 
Size: 590559 Color: 1
Size: 406753 Color: 4

Bin 133: 2813 of cap free
Amount of items: 2
Items: 
Size: 712202 Color: 4
Size: 284986 Color: 1

Bin 134: 2871 of cap free
Amount of items: 3
Items: 
Size: 599609 Color: 1
Size: 205821 Color: 0
Size: 191700 Color: 2

Bin 135: 2918 of cap free
Amount of items: 3
Items: 
Size: 596271 Color: 3
Size: 226079 Color: 1
Size: 174733 Color: 0

Bin 136: 2954 of cap free
Amount of items: 3
Items: 
Size: 645083 Color: 2
Size: 187602 Color: 1
Size: 164362 Color: 3

Bin 137: 3033 of cap free
Amount of items: 2
Items: 
Size: 748754 Color: 3
Size: 248214 Color: 0

Bin 138: 3078 of cap free
Amount of items: 2
Items: 
Size: 524140 Color: 2
Size: 472783 Color: 3

Bin 139: 3116 of cap free
Amount of items: 3
Items: 
Size: 354166 Color: 3
Size: 330713 Color: 4
Size: 312006 Color: 4

Bin 140: 3151 of cap free
Amount of items: 2
Items: 
Size: 627249 Color: 0
Size: 369601 Color: 3

Bin 141: 3197 of cap free
Amount of items: 2
Items: 
Size: 595779 Color: 0
Size: 401025 Color: 4

Bin 142: 3199 of cap free
Amount of items: 2
Items: 
Size: 509732 Color: 4
Size: 487070 Color: 0

Bin 143: 3301 of cap free
Amount of items: 2
Items: 
Size: 678454 Color: 3
Size: 318246 Color: 1

Bin 144: 3367 of cap free
Amount of items: 2
Items: 
Size: 772281 Color: 2
Size: 224353 Color: 3

Bin 145: 3400 of cap free
Amount of items: 2
Items: 
Size: 779806 Color: 0
Size: 216795 Color: 2

Bin 146: 3520 of cap free
Amount of items: 2
Items: 
Size: 548184 Color: 2
Size: 448297 Color: 0

Bin 147: 3646 of cap free
Amount of items: 2
Items: 
Size: 594306 Color: 2
Size: 402049 Color: 0

Bin 148: 3668 of cap free
Amount of items: 2
Items: 
Size: 521037 Color: 4
Size: 475296 Color: 2

Bin 149: 3844 of cap free
Amount of items: 3
Items: 
Size: 780438 Color: 3
Size: 112698 Color: 1
Size: 103021 Color: 2

Bin 150: 3968 of cap free
Amount of items: 2
Items: 
Size: 720094 Color: 2
Size: 275939 Color: 3

Bin 151: 4091 of cap free
Amount of items: 2
Items: 
Size: 797250 Color: 3
Size: 198660 Color: 2

Bin 152: 4240 of cap free
Amount of items: 2
Items: 
Size: 752941 Color: 4
Size: 242820 Color: 1

Bin 153: 4296 of cap free
Amount of items: 2
Items: 
Size: 574661 Color: 1
Size: 421044 Color: 4

Bin 154: 4308 of cap free
Amount of items: 2
Items: 
Size: 740604 Color: 2
Size: 255089 Color: 3

Bin 155: 4368 of cap free
Amount of items: 2
Items: 
Size: 720445 Color: 1
Size: 275188 Color: 0

Bin 156: 4460 of cap free
Amount of items: 2
Items: 
Size: 632944 Color: 4
Size: 362597 Color: 2

Bin 157: 4771 of cap free
Amount of items: 2
Items: 
Size: 719831 Color: 4
Size: 275399 Color: 0

Bin 158: 4794 of cap free
Amount of items: 2
Items: 
Size: 526288 Color: 0
Size: 468919 Color: 1

Bin 159: 4837 of cap free
Amount of items: 2
Items: 
Size: 757387 Color: 3
Size: 237777 Color: 4

Bin 160: 5141 of cap free
Amount of items: 2
Items: 
Size: 532194 Color: 3
Size: 462666 Color: 1

Bin 161: 5310 of cap free
Amount of items: 2
Items: 
Size: 667017 Color: 0
Size: 327674 Color: 1

Bin 162: 5461 of cap free
Amount of items: 2
Items: 
Size: 616639 Color: 0
Size: 377901 Color: 4

Bin 163: 5555 of cap free
Amount of items: 2
Items: 
Size: 616653 Color: 2
Size: 377793 Color: 1

Bin 164: 5584 of cap free
Amount of items: 2
Items: 
Size: 594287 Color: 2
Size: 400130 Color: 4

Bin 165: 5858 of cap free
Amount of items: 2
Items: 
Size: 543173 Color: 4
Size: 450970 Color: 2

Bin 166: 5858 of cap free
Amount of items: 2
Items: 
Size: 663285 Color: 3
Size: 330858 Color: 4

Bin 167: 5977 of cap free
Amount of items: 2
Items: 
Size: 696653 Color: 4
Size: 297371 Color: 2

Bin 168: 6315 of cap free
Amount of items: 2
Items: 
Size: 579853 Color: 2
Size: 413833 Color: 1

Bin 169: 6388 of cap free
Amount of items: 2
Items: 
Size: 602710 Color: 4
Size: 390903 Color: 3

Bin 170: 6661 of cap free
Amount of items: 2
Items: 
Size: 573852 Color: 3
Size: 419488 Color: 4

Bin 171: 6783 of cap free
Amount of items: 2
Items: 
Size: 603170 Color: 0
Size: 390048 Color: 1

Bin 172: 6875 of cap free
Amount of items: 2
Items: 
Size: 739731 Color: 1
Size: 253395 Color: 2

Bin 173: 6952 of cap free
Amount of items: 2
Items: 
Size: 739464 Color: 3
Size: 253585 Color: 0

Bin 174: 6975 of cap free
Amount of items: 2
Items: 
Size: 759512 Color: 1
Size: 233514 Color: 0

Bin 175: 7019 of cap free
Amount of items: 2
Items: 
Size: 555750 Color: 1
Size: 437232 Color: 3

Bin 176: 7041 of cap free
Amount of items: 2
Items: 
Size: 674162 Color: 0
Size: 318798 Color: 4

Bin 177: 7234 of cap free
Amount of items: 2
Items: 
Size: 545725 Color: 2
Size: 447042 Color: 4

Bin 178: 7300 of cap free
Amount of items: 2
Items: 
Size: 758508 Color: 4
Size: 234193 Color: 3

Bin 179: 7748 of cap free
Amount of items: 2
Items: 
Size: 519838 Color: 3
Size: 472415 Color: 2

Bin 180: 7784 of cap free
Amount of items: 2
Items: 
Size: 759360 Color: 1
Size: 232857 Color: 0

Bin 181: 7828 of cap free
Amount of items: 2
Items: 
Size: 653642 Color: 3
Size: 338531 Color: 2

Bin 182: 7877 of cap free
Amount of items: 2
Items: 
Size: 539458 Color: 3
Size: 452666 Color: 1

Bin 183: 8198 of cap free
Amount of items: 2
Items: 
Size: 687624 Color: 4
Size: 304179 Color: 0

Bin 184: 8311 of cap free
Amount of items: 2
Items: 
Size: 738650 Color: 1
Size: 253040 Color: 3

Bin 185: 8324 of cap free
Amount of items: 2
Items: 
Size: 749876 Color: 0
Size: 241801 Color: 1

Bin 186: 8435 of cap free
Amount of items: 2
Items: 
Size: 769309 Color: 3
Size: 222257 Color: 4

Bin 187: 8799 of cap free
Amount of items: 2
Items: 
Size: 795584 Color: 1
Size: 195618 Color: 2

Bin 188: 8834 of cap free
Amount of items: 2
Items: 
Size: 504981 Color: 0
Size: 486186 Color: 1

Bin 189: 9374 of cap free
Amount of items: 2
Items: 
Size: 573242 Color: 0
Size: 417385 Color: 4

Bin 190: 9944 of cap free
Amount of items: 2
Items: 
Size: 590747 Color: 3
Size: 399310 Color: 4

Bin 191: 10525 of cap free
Amount of items: 2
Items: 
Size: 756295 Color: 0
Size: 233181 Color: 2

Bin 192: 10634 of cap free
Amount of items: 2
Items: 
Size: 562332 Color: 1
Size: 427035 Color: 2

Bin 193: 10657 of cap free
Amount of items: 2
Items: 
Size: 719207 Color: 1
Size: 270137 Color: 0

Bin 194: 11297 of cap free
Amount of items: 3
Items: 
Size: 594102 Color: 4
Size: 205385 Color: 0
Size: 189217 Color: 2

Bin 195: 11719 of cap free
Amount of items: 2
Items: 
Size: 611942 Color: 2
Size: 376340 Color: 3

Bin 196: 11968 of cap free
Amount of items: 2
Items: 
Size: 704803 Color: 3
Size: 283230 Color: 1

Bin 197: 12304 of cap free
Amount of items: 2
Items: 
Size: 771128 Color: 1
Size: 216569 Color: 0

Bin 198: 12730 of cap free
Amount of items: 2
Items: 
Size: 717082 Color: 2
Size: 270189 Color: 4

Bin 199: 12897 of cap free
Amount of items: 2
Items: 
Size: 717153 Color: 1
Size: 269951 Color: 0

Bin 200: 13003 of cap free
Amount of items: 2
Items: 
Size: 747565 Color: 4
Size: 239433 Color: 1

Bin 201: 14087 of cap free
Amount of items: 2
Items: 
Size: 504984 Color: 2
Size: 480930 Color: 3

Bin 202: 15339 of cap free
Amount of items: 2
Items: 
Size: 611686 Color: 3
Size: 372976 Color: 4

Bin 203: 15491 of cap free
Amount of items: 2
Items: 
Size: 503486 Color: 3
Size: 481024 Color: 0

Bin 204: 18891 of cap free
Amount of items: 2
Items: 
Size: 500753 Color: 1
Size: 480357 Color: 0

Bin 205: 19091 of cap free
Amount of items: 2
Items: 
Size: 588161 Color: 2
Size: 392749 Color: 4

Bin 206: 19299 of cap free
Amount of items: 3
Items: 
Size: 590974 Color: 4
Size: 205081 Color: 2
Size: 184647 Color: 0

Bin 207: 20226 of cap free
Amount of items: 2
Items: 
Size: 587665 Color: 2
Size: 392110 Color: 4

Bin 208: 28663 of cap free
Amount of items: 2
Items: 
Size: 674798 Color: 4
Size: 296540 Color: 1

Bin 209: 28860 of cap free
Amount of items: 2
Items: 
Size: 531376 Color: 2
Size: 439765 Color: 1

Bin 210: 29193 of cap free
Amount of items: 3
Items: 
Size: 352997 Color: 4
Size: 349201 Color: 0
Size: 268610 Color: 1

Bin 211: 36334 of cap free
Amount of items: 2
Items: 
Size: 591452 Color: 3
Size: 372215 Color: 1

Bin 212: 37132 of cap free
Amount of items: 2
Items: 
Size: 748402 Color: 1
Size: 214467 Color: 2

Bin 213: 40559 of cap free
Amount of items: 2
Items: 
Size: 499517 Color: 2
Size: 459925 Color: 4

Bin 214: 42299 of cap free
Amount of items: 2
Items: 
Size: 590020 Color: 2
Size: 367682 Color: 1

Bin 215: 42388 of cap free
Amount of items: 3
Items: 
Size: 349144 Color: 0
Size: 343076 Color: 3
Size: 265393 Color: 1

Bin 216: 48231 of cap free
Amount of items: 2
Items: 
Size: 737571 Color: 1
Size: 214199 Color: 2

Bin 217: 49940 of cap free
Amount of items: 2
Items: 
Size: 736754 Color: 1
Size: 213307 Color: 2

Bin 218: 50498 of cap free
Amount of items: 2
Items: 
Size: 582811 Color: 3
Size: 366692 Color: 4

Bin 219: 55048 of cap free
Amount of items: 2
Items: 
Size: 734742 Color: 1
Size: 210211 Color: 4

Bin 220: 58468 of cap free
Amount of items: 2
Items: 
Size: 664275 Color: 4
Size: 277258 Color: 1

Bin 221: 59104 of cap free
Amount of items: 2
Items: 
Size: 478671 Color: 1
Size: 462226 Color: 0

Bin 222: 60034 of cap free
Amount of items: 2
Items: 
Size: 730399 Color: 1
Size: 209568 Color: 0

Bin 223: 63449 of cap free
Amount of items: 2
Items: 
Size: 575487 Color: 4
Size: 361065 Color: 0

Bin 224: 66306 of cap free
Amount of items: 2
Items: 
Size: 663189 Color: 2
Size: 270506 Color: 1

Bin 225: 70767 of cap free
Amount of items: 2
Items: 
Size: 570719 Color: 3
Size: 358515 Color: 1

Bin 226: 74384 of cap free
Amount of items: 2
Items: 
Size: 567708 Color: 3
Size: 357909 Color: 0

Bin 227: 78401 of cap free
Amount of items: 2
Items: 
Size: 567252 Color: 0
Size: 354348 Color: 1

Bin 228: 87512 of cap free
Amount of items: 2
Items: 
Size: 703635 Color: 1
Size: 208854 Color: 0

Bin 229: 104089 of cap free
Amount of items: 2
Items: 
Size: 566514 Color: 3
Size: 329398 Color: 2

Bin 230: 180727 of cap free
Amount of items: 2
Items: 
Size: 464242 Color: 2
Size: 355032 Color: 0

Bin 231: 210291 of cap free
Amount of items: 2
Items: 
Size: 433123 Color: 3
Size: 356587 Color: 4

Total size: 228702598
Total free space: 2297633

