Capicity Bin: 7520
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 280
Size: 3366 Color: 271
Size: 164 Color: 38

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5324 Color: 306
Size: 2076 Color: 243
Size: 120 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 322
Size: 1446 Color: 220
Size: 288 Color: 85

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 323
Size: 1117 Color: 193
Size: 604 Color: 131

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5803 Color: 324
Size: 1093 Color: 191
Size: 624 Color: 133

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5975 Color: 331
Size: 1365 Color: 213
Size: 180 Color: 52

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6024 Color: 333
Size: 1318 Color: 211
Size: 178 Color: 48

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 341
Size: 1204 Color: 201
Size: 200 Color: 58

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 343
Size: 1132 Color: 196
Size: 216 Color: 63

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 345
Size: 1121 Color: 194
Size: 222 Color: 67

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6205 Color: 347
Size: 1097 Color: 192
Size: 218 Color: 64

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6261 Color: 352
Size: 1121 Color: 195
Size: 138 Color: 25

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 356
Size: 892 Color: 173
Size: 286 Color: 84

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 359
Size: 1001 Color: 185
Size: 162 Color: 37

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 361
Size: 868 Color: 166
Size: 222 Color: 66

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 362
Size: 891 Color: 172
Size: 194 Color: 56

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 363
Size: 902 Color: 176
Size: 180 Color: 49

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6441 Color: 364
Size: 901 Color: 175
Size: 178 Color: 47

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 367
Size: 642 Color: 137
Size: 416 Color: 111

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 371
Size: 540 Color: 126
Size: 496 Color: 123

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 372
Size: 690 Color: 145
Size: 304 Color: 91

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 377
Size: 718 Color: 150
Size: 244 Color: 73

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 378
Size: 751 Color: 155
Size: 200 Color: 59

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 379
Size: 600 Color: 130
Size: 344 Color: 96

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6577 Color: 380
Size: 813 Color: 162
Size: 130 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 386
Size: 695 Color: 146
Size: 172 Color: 42

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 387
Size: 721 Color: 151
Size: 144 Color: 30

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 388
Size: 684 Color: 144
Size: 174 Color: 43

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 389
Size: 660 Color: 140
Size: 192 Color: 54

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6683 Color: 391
Size: 699 Color: 147
Size: 138 Color: 24

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 394
Size: 432 Color: 113
Size: 388 Color: 104

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6715 Color: 395
Size: 671 Color: 142
Size: 134 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6719 Color: 396
Size: 669 Color: 141
Size: 132 Color: 17

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6747 Color: 400
Size: 677 Color: 143
Size: 96 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 401
Size: 626 Color: 136
Size: 144 Color: 28

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 402
Size: 588 Color: 129
Size: 176 Color: 46

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 320
Size: 1605 Color: 228
Size: 152 Color: 33

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 338
Size: 1435 Color: 219

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6090 Color: 339
Size: 1429 Color: 216

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 344
Size: 1204 Color: 202
Size: 140 Color: 26

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6350 Color: 357
Size: 873 Color: 169
Size: 296 Color: 89

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6353 Color: 358
Size: 1166 Color: 198

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6548 Color: 376
Size: 971 Color: 181

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 397
Size: 793 Color: 160

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6732 Color: 398
Size: 787 Color: 158

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6739 Color: 399
Size: 780 Color: 157

Bin 47: 2 of cap free
Amount of items: 23
Items: 
Size: 432 Color: 112
Size: 416 Color: 110
Size: 410 Color: 109
Size: 410 Color: 108
Size: 408 Color: 107
Size: 408 Color: 106
Size: 408 Color: 105
Size: 384 Color: 103
Size: 376 Color: 102
Size: 360 Color: 101
Size: 360 Color: 100
Size: 360 Color: 99
Size: 352 Color: 97
Size: 272 Color: 80
Size: 256 Color: 77
Size: 256 Color: 76
Size: 246 Color: 75
Size: 246 Color: 74
Size: 240 Color: 72
Size: 236 Color: 71
Size: 232 Color: 70
Size: 232 Color: 69
Size: 218 Color: 65

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 5807 Color: 325
Size: 1431 Color: 217
Size: 280 Color: 81

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 330
Size: 1284 Color: 208
Size: 260 Color: 78

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6098 Color: 340
Size: 1420 Color: 215

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6122 Color: 342
Size: 1396 Color: 214

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6242 Color: 350
Size: 1276 Color: 207

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 366
Size: 1066 Color: 190

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6545 Color: 375
Size: 973 Color: 182

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6621 Color: 383
Size: 897 Color: 174

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 385
Size: 882 Color: 171

Bin 57: 3 of cap free
Amount of items: 7
Items: 
Size: 3765 Color: 276
Size: 1229 Color: 203
Size: 964 Color: 180
Size: 905 Color: 177
Size: 304 Color: 90
Size: 176 Color: 45
Size: 174 Color: 44

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 5754 Color: 319
Size: 1603 Color: 227
Size: 160 Color: 36

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 6043 Color: 335
Size: 1474 Color: 223

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 369
Size: 1044 Color: 187

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6477 Color: 370
Size: 1040 Color: 186

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6687 Color: 392
Size: 830 Color: 164

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 5506 Color: 311
Size: 2010 Color: 239

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 329
Size: 1183 Color: 199
Size: 360 Color: 98

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6250 Color: 351
Size: 1266 Color: 206

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 368
Size: 1051 Color: 188

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6534 Color: 374
Size: 982 Color: 184

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 6694 Color: 393
Size: 822 Color: 163

Bin 69: 5 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 299
Size: 2351 Color: 250
Size: 128 Color: 14

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 6047 Color: 336
Size: 1468 Color: 222

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 382
Size: 910 Color: 178

Bin 72: 7 of cap free
Amount of items: 7
Items: 
Size: 3762 Color: 274
Size: 758 Color: 156
Size: 742 Color: 154
Size: 740 Color: 153
Size: 723 Color: 152
Size: 608 Color: 132
Size: 180 Color: 51

Bin 73: 7 of cap free
Amount of items: 7
Items: 
Size: 3764 Color: 275
Size: 871 Color: 168
Size: 870 Color: 167
Size: 812 Color: 161
Size: 792 Color: 159
Size: 224 Color: 68
Size: 180 Color: 50

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 321
Size: 1748 Color: 231

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 6670 Color: 390
Size: 842 Color: 165

Bin 76: 9 of cap free
Amount of items: 3
Items: 
Size: 5597 Color: 315
Size: 1878 Color: 236
Size: 36 Color: 1

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 354
Size: 1235 Color: 205

Bin 78: 9 of cap free
Amount of items: 2
Items: 
Size: 6533 Color: 373
Size: 978 Color: 183

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 6630 Color: 384
Size: 881 Color: 170

Bin 80: 10 of cap free
Amount of items: 2
Items: 
Size: 6590 Color: 381
Size: 920 Color: 179

Bin 81: 12 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 312
Size: 1498 Color: 224
Size: 496 Color: 122

Bin 82: 12 of cap free
Amount of items: 2
Items: 
Size: 6076 Color: 337
Size: 1432 Color: 218

Bin 83: 12 of cap free
Amount of items: 2
Items: 
Size: 6364 Color: 360
Size: 1144 Color: 197

Bin 84: 13 of cap free
Amount of items: 2
Items: 
Size: 6445 Color: 365
Size: 1062 Color: 189

Bin 85: 14 of cap free
Amount of items: 2
Items: 
Size: 5852 Color: 327
Size: 1654 Color: 229

Bin 86: 15 of cap free
Amount of items: 2
Items: 
Size: 6039 Color: 334
Size: 1466 Color: 221

Bin 87: 15 of cap free
Amount of items: 2
Items: 
Size: 6181 Color: 346
Size: 1324 Color: 212

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 6319 Color: 355
Size: 1186 Color: 200

Bin 89: 16 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 313
Size: 1902 Color: 237
Size: 64 Color: 3

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 332
Size: 1524 Color: 225

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 6213 Color: 349
Size: 1291 Color: 210

Bin 92: 18 of cap free
Amount of items: 3
Items: 
Size: 5353 Color: 308
Size: 2057 Color: 242
Size: 92 Color: 5

Bin 93: 18 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 326
Size: 1682 Color: 230

Bin 94: 19 of cap free
Amount of items: 2
Items: 
Size: 5692 Color: 317
Size: 1809 Color: 234

Bin 95: 20 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 307
Size: 2053 Color: 241
Size: 96 Color: 7

Bin 96: 21 of cap free
Amount of items: 2
Items: 
Size: 4699 Color: 292
Size: 2800 Color: 259

Bin 97: 21 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 316
Size: 1807 Color: 233
Size: 32 Color: 0

Bin 98: 21 of cap free
Amount of items: 2
Items: 
Size: 6209 Color: 348
Size: 1290 Color: 209

Bin 99: 21 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 353
Size: 1231 Color: 204

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 5398 Color: 309
Size: 2100 Color: 245

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 5942 Color: 328
Size: 1556 Color: 226

Bin 102: 23 of cap free
Amount of items: 3
Items: 
Size: 5020 Color: 298
Size: 2349 Color: 249
Size: 128 Color: 15

Bin 103: 24 of cap free
Amount of items: 2
Items: 
Size: 5726 Color: 318
Size: 1770 Color: 232

Bin 104: 30 of cap free
Amount of items: 8
Items: 
Size: 3761 Color: 273
Size: 716 Color: 149
Size: 710 Color: 148
Size: 651 Color: 139
Size: 644 Color: 138
Size: 624 Color: 135
Size: 192 Color: 55
Size: 192 Color: 53

Bin 105: 33 of cap free
Amount of items: 3
Items: 
Size: 4644 Color: 290
Size: 2707 Color: 257
Size: 136 Color: 23

Bin 106: 33 of cap free
Amount of items: 3
Items: 
Size: 5595 Color: 314
Size: 1836 Color: 235
Size: 56 Color: 2

Bin 107: 34 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 303
Size: 2170 Color: 247
Size: 128 Color: 11

Bin 108: 36 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 297
Size: 2344 Color: 248
Size: 136 Color: 19

Bin 109: 37 of cap free
Amount of items: 4
Items: 
Size: 5061 Color: 302
Size: 2166 Color: 246
Size: 128 Color: 13
Size: 128 Color: 12

Bin 110: 40 of cap free
Amount of items: 2
Items: 
Size: 4538 Color: 289
Size: 2942 Color: 263

Bin 111: 42 of cap free
Amount of items: 2
Items: 
Size: 3766 Color: 277
Size: 3712 Color: 272

Bin 112: 56 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 310
Size: 1948 Color: 238
Size: 88 Color: 4

Bin 113: 59 of cap free
Amount of items: 4
Items: 
Size: 4703 Color: 293
Size: 2486 Color: 253
Size: 136 Color: 22
Size: 136 Color: 21

Bin 114: 59 of cap free
Amount of items: 2
Items: 
Size: 5057 Color: 301
Size: 2404 Color: 252

Bin 115: 64 of cap free
Amount of items: 17
Items: 
Size: 624 Color: 134
Size: 572 Color: 128
Size: 568 Color: 127
Size: 540 Color: 125
Size: 520 Color: 124
Size: 496 Color: 121
Size: 496 Color: 120
Size: 472 Color: 119
Size: 472 Color: 118
Size: 470 Color: 117
Size: 470 Color: 116
Size: 468 Color: 115
Size: 464 Color: 114
Size: 212 Color: 62
Size: 208 Color: 61
Size: 208 Color: 60
Size: 196 Color: 57

Bin 116: 68 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 294
Size: 2604 Color: 256
Size: 136 Color: 20

Bin 117: 73 of cap free
Amount of items: 3
Items: 
Size: 4273 Color: 285
Size: 3030 Color: 265
Size: 144 Color: 29

Bin 118: 74 of cap free
Amount of items: 3
Items: 
Size: 5242 Color: 305
Size: 2084 Color: 244
Size: 120 Color: 9

Bin 119: 88 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 283
Size: 3134 Color: 269
Size: 148 Color: 32

Bin 120: 89 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 284
Size: 3012 Color: 264
Size: 148 Color: 31

Bin 121: 106 of cap free
Amount of items: 2
Items: 
Size: 4922 Color: 296
Size: 2492 Color: 255

Bin 122: 107 of cap free
Amount of items: 3
Items: 
Size: 5238 Color: 304
Size: 2051 Color: 240
Size: 124 Color: 10

Bin 123: 112 of cap free
Amount of items: 2
Items: 
Size: 4534 Color: 288
Size: 2874 Color: 262

Bin 124: 112 of cap free
Amount of items: 2
Items: 
Size: 4918 Color: 295
Size: 2490 Color: 254

Bin 125: 112 of cap free
Amount of items: 2
Items: 
Size: 5053 Color: 300
Size: 2355 Color: 251

Bin 126: 116 of cap free
Amount of items: 3
Items: 
Size: 4396 Color: 286
Size: 2868 Color: 260
Size: 140 Color: 27

Bin 127: 116 of cap free
Amount of items: 2
Items: 
Size: 4695 Color: 291
Size: 2709 Color: 258

Bin 128: 118 of cap free
Amount of items: 2
Items: 
Size: 4532 Color: 287
Size: 2870 Color: 261

Bin 129: 147 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 282
Size: 3133 Color: 268
Size: 156 Color: 34

Bin 130: 152 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 281
Size: 3132 Color: 267
Size: 158 Color: 35

Bin 131: 160 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 279
Size: 3288 Color: 270
Size: 164 Color: 39

Bin 132: 171 of cap free
Amount of items: 4
Items: 
Size: 3886 Color: 278
Size: 3131 Color: 266
Size: 168 Color: 41
Size: 164 Color: 40

Bin 133: 4506 of cap free
Amount of items: 10
Items: 
Size: 332 Color: 95
Size: 328 Color: 94
Size: 320 Color: 93
Size: 320 Color: 92
Size: 292 Color: 88
Size: 292 Color: 87
Size: 288 Color: 86
Size: 286 Color: 83
Size: 284 Color: 82
Size: 272 Color: 79

Total size: 992640
Total free space: 7520

