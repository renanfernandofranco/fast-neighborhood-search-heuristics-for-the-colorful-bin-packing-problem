Capicity Bin: 16128
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9104 Color: 422
Size: 6648 Color: 391
Size: 376 Color: 60

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 452
Size: 5136 Color: 363
Size: 288 Color: 26

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 467
Size: 3062 Color: 308
Size: 1842 Color: 239

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12075 Color: 482
Size: 2781 Color: 298
Size: 1272 Color: 190

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 483
Size: 3600 Color: 327
Size: 446 Color: 80

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 484
Size: 3370 Color: 320
Size: 672 Color: 132

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12240 Color: 488
Size: 3504 Color: 324
Size: 384 Color: 62

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 489
Size: 3244 Color: 315
Size: 640 Color: 126

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12442 Color: 493
Size: 2358 Color: 273
Size: 1328 Color: 195

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 497
Size: 3560 Color: 326
Size: 32 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 499
Size: 2604 Color: 288
Size: 880 Color: 160

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12652 Color: 500
Size: 2058 Color: 257
Size: 1418 Color: 204

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12661 Color: 501
Size: 2391 Color: 278
Size: 1076 Color: 172

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 507
Size: 1912 Color: 246
Size: 1312 Color: 192

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12944 Color: 508
Size: 1844 Color: 240
Size: 1340 Color: 198

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 512
Size: 2620 Color: 290
Size: 520 Color: 101

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 514
Size: 1788 Color: 234
Size: 1336 Color: 196

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 515
Size: 2596 Color: 287
Size: 512 Color: 99

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 520
Size: 2896 Color: 303
Size: 32 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 524
Size: 2344 Color: 272
Size: 528 Color: 106

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 526
Size: 2372 Color: 276
Size: 472 Color: 88

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13291 Color: 527
Size: 2427 Color: 280
Size: 410 Color: 71

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 528
Size: 2380 Color: 277
Size: 448 Color: 83

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13540 Color: 537
Size: 1432 Color: 207
Size: 1156 Color: 182

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13580 Color: 539
Size: 1688 Color: 225
Size: 860 Color: 159

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13597 Color: 542
Size: 2051 Color: 255
Size: 480 Color: 91

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 543
Size: 2056 Color: 256
Size: 464 Color: 86

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13659 Color: 545
Size: 2059 Color: 258
Size: 410 Color: 70

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13703 Color: 549
Size: 1881 Color: 244
Size: 544 Color: 107

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13708 Color: 550
Size: 1936 Color: 248
Size: 484 Color: 93

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13802 Color: 554
Size: 1942 Color: 249
Size: 384 Color: 63

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13840 Color: 555
Size: 1264 Color: 188
Size: 1024 Color: 168

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13848 Color: 556
Size: 1336 Color: 197
Size: 944 Color: 163

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13912 Color: 562
Size: 1738 Color: 229
Size: 478 Color: 90

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 563
Size: 1782 Color: 233
Size: 428 Color: 77

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 564
Size: 1484 Color: 211
Size: 720 Color: 138

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13976 Color: 565
Size: 1328 Color: 193
Size: 824 Color: 151

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14046 Color: 570
Size: 1328 Color: 194
Size: 754 Color: 144

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14048 Color: 571
Size: 1800 Color: 237
Size: 280 Color: 20

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14051 Color: 572
Size: 1731 Color: 228
Size: 346 Color: 45

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14116 Color: 576
Size: 1084 Color: 174
Size: 928 Color: 162

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14160 Color: 578
Size: 1760 Color: 230
Size: 208 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14165 Color: 579
Size: 1637 Color: 222
Size: 326 Color: 39

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 580
Size: 1690 Color: 226
Size: 244 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 581
Size: 1524 Color: 213
Size: 408 Color: 69

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 583
Size: 1616 Color: 221
Size: 288 Color: 25

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 587
Size: 1372 Color: 202
Size: 464 Color: 84

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 588
Size: 1512 Color: 212
Size: 320 Color: 36

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14300 Color: 589
Size: 1532 Color: 215
Size: 296 Color: 28

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14350 Color: 590
Size: 1374 Color: 203
Size: 404 Color: 68

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14354 Color: 591
Size: 1482 Color: 210
Size: 292 Color: 27

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14414 Color: 594
Size: 1340 Color: 199
Size: 374 Color: 59

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 595
Size: 1340 Color: 200
Size: 368 Color: 57

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 596
Size: 1152 Color: 181
Size: 552 Color: 108

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14482 Color: 598
Size: 1342 Color: 201
Size: 304 Color: 29

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 599
Size: 1228 Color: 187
Size: 416 Color: 72

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 600
Size: 1168 Color: 185
Size: 472 Color: 89

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11077 Color: 460
Size: 4770 Color: 357
Size: 280 Color: 19

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 11192 Color: 465
Size: 3779 Color: 335
Size: 1156 Color: 184

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 11864 Color: 479
Size: 4211 Color: 345
Size: 52 Color: 2

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12087 Color: 485
Size: 4040 Color: 339

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 511
Size: 2891 Color: 302
Size: 256 Color: 14

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13247 Color: 523
Size: 1792 Color: 236
Size: 1088 Color: 175

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 536
Size: 2611 Color: 289

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13595 Color: 541
Size: 1684 Color: 224
Size: 848 Color: 158

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13873 Color: 558
Size: 1838 Color: 238
Size: 416 Color: 73

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 13909 Color: 561
Size: 2218 Color: 265

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14095 Color: 573
Size: 2032 Color: 254

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 575
Size: 1592 Color: 218
Size: 420 Color: 74

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 593
Size: 1767 Color: 232

Bin 71: 2 of cap free
Amount of items: 5
Items: 
Size: 9224 Color: 427
Size: 5742 Color: 376
Size: 424 Color: 76
Size: 368 Color: 58
Size: 368 Color: 56

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 9580 Color: 434
Size: 6194 Color: 385
Size: 352 Color: 48

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 11944 Color: 480
Size: 4182 Color: 344

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 12392 Color: 492
Size: 3734 Color: 332

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 12747 Color: 504
Size: 3379 Color: 322

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13174 Color: 519
Size: 2952 Color: 306

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 14206 Color: 582
Size: 1920 Color: 247

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 586
Size: 1848 Color: 241

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 9622 Color: 435
Size: 6151 Color: 384
Size: 352 Color: 47

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 12037 Color: 481
Size: 4088 Color: 340

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 506
Size: 3334 Color: 317

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 12997 Color: 513
Size: 3128 Color: 311

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13217 Color: 522
Size: 2908 Color: 305

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 574
Size: 2021 Color: 253

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 14430 Color: 597
Size: 1695 Color: 227

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 9174 Color: 424
Size: 5422 Color: 370
Size: 1528 Color: 214

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 10388 Color: 443
Size: 5736 Color: 375

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 10394 Color: 444
Size: 5410 Color: 369
Size: 320 Color: 35

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 10429 Color: 447
Size: 5387 Color: 368
Size: 308 Color: 33

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 10452 Color: 448
Size: 5368 Color: 366
Size: 304 Color: 32

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13584 Color: 540
Size: 2540 Color: 285

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 546
Size: 2462 Color: 283

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 584
Size: 1892 Color: 245

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 11595 Color: 470
Size: 4528 Color: 353

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 12968 Color: 510
Size: 3155 Color: 313

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 13451 Color: 533
Size: 2672 Color: 295

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 552
Size: 2365 Color: 275

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 9182 Color: 425
Size: 5460 Color: 372
Size: 1480 Color: 208

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 11046 Color: 456
Size: 5076 Color: 362

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 11220 Color: 466
Size: 3730 Color: 331
Size: 1172 Color: 186

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 11756 Color: 476
Size: 4238 Color: 347
Size: 128 Color: 5

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 13122 Color: 518
Size: 3000 Color: 307

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13320 Color: 530
Size: 2802 Color: 299

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13795 Color: 553
Size: 2327 Color: 271

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13874 Color: 559
Size: 2248 Color: 268

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14011 Color: 569
Size: 2111 Color: 261

Bin 107: 7 of cap free
Amount of items: 3
Items: 
Size: 11034 Color: 455
Size: 3236 Color: 314
Size: 1851 Color: 242

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 13047 Color: 517
Size: 3074 Color: 310

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 529
Size: 2819 Color: 300

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 14356 Color: 592
Size: 1765 Color: 231

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 10671 Color: 451
Size: 5145 Color: 364
Size: 304 Color: 30

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 486
Size: 4016 Color: 338

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 532
Size: 2696 Color: 296

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 548
Size: 2448 Color: 282

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13712 Color: 551
Size: 2408 Color: 279

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14142 Color: 577
Size: 1978 Color: 251

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 13259 Color: 525
Size: 2860 Color: 301

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 13888 Color: 560
Size: 2231 Color: 267

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 14256 Color: 585
Size: 1863 Color: 243

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 457
Size: 4782 Color: 358
Size: 288 Color: 22

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12458 Color: 496
Size: 3660 Color: 330

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 535
Size: 2648 Color: 292

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 567
Size: 2128 Color: 263

Bin 124: 11 of cap free
Amount of items: 7
Items: 
Size: 8069 Color: 407
Size: 1648 Color: 223
Size: 1602 Color: 219
Size: 1584 Color: 217
Size: 1542 Color: 216
Size: 1148 Color: 180
Size: 524 Color: 105

Bin 125: 11 of cap free
Amount of items: 3
Items: 
Size: 11824 Color: 478
Size: 4221 Color: 346
Size: 72 Color: 3

Bin 126: 12 of cap free
Amount of items: 4
Items: 
Size: 9638 Color: 436
Size: 5782 Color: 378
Size: 352 Color: 46
Size: 344 Color: 44

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 11740 Color: 475
Size: 4248 Color: 349
Size: 128 Color: 6

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 12742 Color: 503
Size: 3374 Color: 321

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 557
Size: 2256 Color: 269

Bin 130: 13 of cap free
Amount of items: 3
Items: 
Size: 11062 Color: 458
Size: 3741 Color: 333
Size: 1312 Color: 191

Bin 131: 13 of cap free
Amount of items: 3
Items: 
Size: 11134 Color: 462
Size: 3369 Color: 319
Size: 1612 Color: 220

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 13546 Color: 538
Size: 2569 Color: 286

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 14009 Color: 568
Size: 2106 Color: 260

Bin 134: 14 of cap free
Amount of items: 3
Items: 
Size: 11110 Color: 461
Size: 4732 Color: 355
Size: 272 Color: 18

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12962 Color: 509
Size: 3152 Color: 312

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 9190 Color: 426
Size: 5440 Color: 371
Size: 1482 Color: 209

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 12368 Color: 491
Size: 3744 Color: 334

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 12600 Color: 498
Size: 3512 Color: 325

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 566
Size: 2124 Color: 262

Bin 140: 18 of cap free
Amount of items: 3
Items: 
Size: 10406 Color: 446
Size: 5384 Color: 367
Size: 320 Color: 34

Bin 141: 19 of cap free
Amount of items: 2
Items: 
Size: 13384 Color: 531
Size: 2725 Color: 297

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 9256 Color: 430
Size: 6496 Color: 390
Size: 356 Color: 53

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 11156 Color: 464
Size: 4680 Color: 354
Size: 272 Color: 17

Bin 144: 20 of cap free
Amount of items: 2
Items: 
Size: 13208 Color: 521
Size: 2900 Color: 304

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 490
Size: 3855 Color: 336

Bin 146: 21 of cap free
Amount of items: 2
Items: 
Size: 13667 Color: 547
Size: 2440 Color: 281

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 13610 Color: 544
Size: 2496 Color: 284

Bin 148: 23 of cap free
Amount of items: 2
Items: 
Size: 13463 Color: 534
Size: 2642 Color: 291

Bin 149: 24 of cap free
Amount of items: 9
Items: 
Size: 8068 Color: 406
Size: 1430 Color: 206
Size: 1428 Color: 205
Size: 1272 Color: 189
Size: 1136 Color: 179
Size: 1096 Color: 177
Size: 562 Color: 111
Size: 556 Color: 110
Size: 556 Color: 109

Bin 150: 24 of cap free
Amount of items: 3
Items: 
Size: 11654 Color: 474
Size: 4302 Color: 350
Size: 148 Color: 7

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 13035 Color: 516
Size: 3069 Color: 309

Bin 152: 28 of cap free
Amount of items: 3
Items: 
Size: 11065 Color: 459
Size: 4751 Color: 356
Size: 284 Color: 21

Bin 153: 29 of cap free
Amount of items: 2
Items: 
Size: 12688 Color: 502
Size: 3411 Color: 323

Bin 154: 30 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 505
Size: 3328 Color: 316

Bin 155: 32 of cap free
Amount of items: 2
Items: 
Size: 12168 Color: 487
Size: 3928 Color: 337

Bin 156: 35 of cap free
Amount of items: 4
Items: 
Size: 11639 Color: 472
Size: 4148 Color: 342
Size: 156 Color: 10
Size: 150 Color: 9

Bin 157: 35 of cap free
Amount of items: 2
Items: 
Size: 12449 Color: 495
Size: 3644 Color: 329

Bin 158: 41 of cap free
Amount of items: 5
Items: 
Size: 8080 Color: 410
Size: 2671 Color: 294
Size: 2670 Color: 293
Size: 2154 Color: 264
Size: 512 Color: 98

Bin 159: 42 of cap free
Amount of items: 3
Items: 
Size: 9968 Color: 441
Size: 5798 Color: 380
Size: 320 Color: 38

Bin 160: 44 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 433
Size: 6200 Color: 386
Size: 352 Color: 49

Bin 161: 45 of cap free
Amount of items: 2
Items: 
Size: 12447 Color: 494
Size: 3636 Color: 328

Bin 162: 51 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 449
Size: 3368 Color: 318
Size: 2221 Color: 266

Bin 163: 55 of cap free
Amount of items: 3
Items: 
Size: 9955 Color: 440
Size: 5790 Color: 379
Size: 328 Color: 40

Bin 164: 55 of cap free
Amount of items: 2
Items: 
Size: 11136 Color: 463
Size: 4937 Color: 361

Bin 165: 66 of cap free
Amount of items: 3
Items: 
Size: 11772 Color: 477
Size: 4162 Color: 343
Size: 128 Color: 4

Bin 166: 68 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 431
Size: 6388 Color: 389
Size: 352 Color: 52

Bin 167: 84 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 473
Size: 4246 Color: 348
Size: 148 Color: 8

Bin 168: 99 of cap free
Amount of items: 7
Items: 
Size: 8072 Color: 408
Size: 2020 Color: 252
Size: 1945 Color: 250
Size: 1792 Color: 235
Size: 1156 Color: 183
Size: 524 Color: 104
Size: 520 Color: 103

Bin 169: 100 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 416
Size: 7420 Color: 403
Size: 440 Color: 78

Bin 170: 108 of cap free
Amount of items: 3
Items: 
Size: 11312 Color: 469
Size: 4436 Color: 352
Size: 272 Color: 15

Bin 171: 117 of cap free
Amount of items: 4
Items: 
Size: 8468 Color: 417
Size: 6721 Color: 397
Size: 422 Color: 75
Size: 400 Color: 67

Bin 172: 130 of cap free
Amount of items: 4
Items: 
Size: 8484 Color: 418
Size: 6722 Color: 398
Size: 400 Color: 66
Size: 392 Color: 65

Bin 173: 132 of cap free
Amount of items: 3
Items: 
Size: 10520 Color: 450
Size: 5172 Color: 365
Size: 304 Color: 31

Bin 174: 137 of cap free
Amount of items: 3
Items: 
Size: 10879 Color: 454
Size: 4824 Color: 360
Size: 288 Color: 23

Bin 175: 140 of cap free
Amount of items: 4
Items: 
Size: 9516 Color: 432
Size: 5768 Color: 377
Size: 352 Color: 51
Size: 352 Color: 50

Bin 176: 150 of cap free
Amount of items: 3
Items: 
Size: 9246 Color: 429
Size: 6372 Color: 388
Size: 360 Color: 54

Bin 177: 158 of cap free
Amount of items: 3
Items: 
Size: 9238 Color: 428
Size: 6364 Color: 387
Size: 368 Color: 55

Bin 178: 164 of cap free
Amount of items: 3
Items: 
Size: 10144 Color: 442
Size: 5500 Color: 373
Size: 320 Color: 37

Bin 179: 167 of cap free
Amount of items: 11
Items: 
Size: 8065 Color: 404
Size: 1028 Color: 169
Size: 992 Color: 167
Size: 952 Color: 166
Size: 952 Color: 165
Size: 948 Color: 164
Size: 608 Color: 120
Size: 608 Color: 119
Size: 608 Color: 118
Size: 608 Color: 117
Size: 592 Color: 116

Bin 180: 173 of cap free
Amount of items: 3
Items: 
Size: 8500 Color: 419
Size: 7067 Color: 402
Size: 388 Color: 64

Bin 181: 179 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 468
Size: 4389 Color: 351
Size: 272 Color: 16

Bin 182: 208 of cap free
Amount of items: 2
Items: 
Size: 10404 Color: 445
Size: 5516 Color: 374

Bin 183: 216 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 439
Size: 5872 Color: 383
Size: 336 Color: 41

Bin 184: 220 of cap free
Amount of items: 3
Items: 
Size: 10812 Color: 453
Size: 4808 Color: 359
Size: 288 Color: 24

Bin 185: 223 of cap free
Amount of items: 3
Items: 
Size: 11607 Color: 471
Size: 4092 Color: 341
Size: 206 Color: 11

Bin 186: 252 of cap free
Amount of items: 6
Items: 
Size: 8074 Color: 409
Size: 2364 Color: 274
Size: 2296 Color: 270
Size: 2102 Color: 259
Size: 520 Color: 102
Size: 520 Color: 100

Bin 187: 259 of cap free
Amount of items: 3
Items: 
Size: 9672 Color: 438
Size: 5861 Color: 382
Size: 336 Color: 42

Bin 188: 271 of cap free
Amount of items: 3
Items: 
Size: 8749 Color: 420
Size: 6724 Color: 399
Size: 384 Color: 61

Bin 189: 278 of cap free
Amount of items: 2
Items: 
Size: 9114 Color: 423
Size: 6736 Color: 401

Bin 190: 279 of cap free
Amount of items: 3
Items: 
Size: 9665 Color: 437
Size: 5846 Color: 381
Size: 338 Color: 43

Bin 191: 305 of cap free
Amount of items: 2
Items: 
Size: 9095 Color: 421
Size: 6728 Color: 400

Bin 192: 318 of cap free
Amount of items: 10
Items: 
Size: 8066 Color: 405
Size: 1136 Color: 178
Size: 1096 Color: 176
Size: 1080 Color: 173
Size: 1072 Color: 171
Size: 1056 Color: 170
Size: 576 Color: 115
Size: 576 Color: 114
Size: 576 Color: 113
Size: 576 Color: 112

Bin 193: 344 of cap free
Amount of items: 4
Items: 
Size: 8084 Color: 412
Size: 6696 Color: 393
Size: 512 Color: 95
Size: 492 Color: 94

Bin 194: 358 of cap free
Amount of items: 4
Items: 
Size: 8082 Color: 411
Size: 6664 Color: 392
Size: 512 Color: 97
Size: 512 Color: 96

Bin 195: 386 of cap free
Amount of items: 4
Items: 
Size: 8136 Color: 415
Size: 6714 Color: 396
Size: 448 Color: 81
Size: 444 Color: 79

Bin 196: 388 of cap free
Amount of items: 4
Items: 
Size: 8090 Color: 413
Size: 6702 Color: 394
Size: 480 Color: 92
Size: 468 Color: 87

Bin 197: 406 of cap free
Amount of items: 4
Items: 
Size: 8104 Color: 414
Size: 6706 Color: 395
Size: 464 Color: 85
Size: 448 Color: 82

Bin 198: 532 of cap free
Amount of items: 21
Items: 
Size: 896 Color: 161
Size: 844 Color: 157
Size: 842 Color: 156
Size: 840 Color: 155
Size: 836 Color: 154
Size: 832 Color: 153
Size: 832 Color: 152
Size: 816 Color: 150
Size: 816 Color: 149
Size: 816 Color: 148
Size: 800 Color: 147
Size: 672 Color: 131
Size: 672 Color: 130
Size: 666 Color: 129
Size: 664 Color: 128
Size: 656 Color: 127
Size: 640 Color: 125
Size: 624 Color: 124
Size: 612 Color: 123
Size: 612 Color: 122
Size: 608 Color: 121

Bin 199: 7422 of cap free
Amount of items: 12
Items: 
Size: 800 Color: 146
Size: 764 Color: 145
Size: 748 Color: 143
Size: 744 Color: 142
Size: 744 Color: 141
Size: 728 Color: 140
Size: 728 Color: 139
Size: 704 Color: 137
Size: 704 Color: 136
Size: 688 Color: 135
Size: 680 Color: 134
Size: 674 Color: 133

Total size: 3193344
Total free space: 16128

