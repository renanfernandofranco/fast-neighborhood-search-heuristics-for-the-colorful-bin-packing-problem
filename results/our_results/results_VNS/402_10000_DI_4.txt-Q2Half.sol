Capicity Bin: 7696
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 1
Size: 2748 Color: 1
Size: 360 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 1
Size: 624 Color: 1
Size: 328 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 1
Size: 1176 Color: 1
Size: 964 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3860 Color: 1
Size: 3204 Color: 1
Size: 632 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 1
Size: 782 Color: 1
Size: 272 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 1
Size: 734 Color: 1
Size: 400 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 6386 Color: 1
Size: 854 Color: 1
Size: 304 Color: 0
Size: 152 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5922 Color: 1
Size: 1302 Color: 1
Size: 472 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6169 Color: 1
Size: 1273 Color: 1
Size: 254 Color: 0

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 5176 Color: 1
Size: 1640 Color: 1
Size: 480 Color: 0
Size: 400 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 1
Size: 676 Color: 1
Size: 128 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4808 Color: 1
Size: 2776 Color: 1
Size: 112 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6145 Color: 1
Size: 1127 Color: 1
Size: 424 Color: 0

Bin 14: 0 of cap free
Amount of items: 6
Items: 
Size: 3854 Color: 1
Size: 2044 Color: 1
Size: 1094 Color: 1
Size: 368 Color: 0
Size: 208 Color: 0
Size: 128 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4806 Color: 1
Size: 2410 Color: 1
Size: 480 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 1
Size: 644 Color: 1
Size: 176 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 1
Size: 784 Color: 1
Size: 128 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4803 Color: 1
Size: 2411 Color: 1
Size: 482 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 1
Size: 827 Color: 1
Size: 164 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 1860 Color: 1
Size: 368 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5328 Color: 1
Size: 1848 Color: 1
Size: 520 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4816 Color: 1
Size: 2624 Color: 1
Size: 256 Color: 0

Bin 23: 0 of cap free
Amount of items: 5
Items: 
Size: 4556 Color: 1
Size: 2114 Color: 1
Size: 674 Color: 1
Size: 208 Color: 0
Size: 144 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 1
Size: 852 Color: 1
Size: 168 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6846 Color: 1
Size: 710 Color: 1
Size: 140 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4373 Color: 1
Size: 2771 Color: 1
Size: 552 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 1
Size: 652 Color: 1
Size: 184 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 3850 Color: 1
Size: 3206 Color: 1
Size: 640 Color: 0

Bin 29: 0 of cap free
Amount of items: 5
Items: 
Size: 5462 Color: 1
Size: 1048 Color: 1
Size: 882 Color: 1
Size: 176 Color: 0
Size: 128 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 1
Size: 1074 Color: 1
Size: 198 Color: 0

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 5283 Color: 1
Size: 1751 Color: 1
Size: 350 Color: 0
Size: 312 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6120 Color: 1
Size: 1288 Color: 1
Size: 288 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 1
Size: 1164 Color: 1
Size: 216 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 1
Size: 1044 Color: 1
Size: 254 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6036 Color: 1
Size: 1636 Color: 1
Size: 24 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4376 Color: 1
Size: 3188 Color: 1
Size: 132 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5275 Color: 1
Size: 2019 Color: 1
Size: 402 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6212 Color: 1
Size: 1244 Color: 1
Size: 240 Color: 0

Bin 39: 0 of cap free
Amount of items: 4
Items: 
Size: 5595 Color: 1
Size: 1541 Color: 1
Size: 352 Color: 0
Size: 208 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 1
Size: 1462 Color: 1
Size: 296 Color: 0

Bin 41: 0 of cap free
Amount of items: 5
Items: 
Size: 3172 Color: 1
Size: 2011 Color: 1
Size: 2005 Color: 1
Size: 276 Color: 0
Size: 232 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 1
Size: 1190 Color: 1
Size: 236 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 1
Size: 691 Color: 1
Size: 488 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1322 Color: 1
Size: 264 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 1
Size: 1642 Color: 1
Size: 328 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5231 Color: 1
Size: 2055 Color: 1
Size: 410 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5944 Color: 1
Size: 1464 Color: 1
Size: 288 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 1
Size: 3020 Color: 1
Size: 136 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 1
Size: 544 Color: 1
Size: 262 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 1
Size: 2102 Color: 1
Size: 416 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 1
Size: 1300 Color: 1
Size: 258 Color: 0

Bin 52: 0 of cap free
Amount of items: 5
Items: 
Size: 4374 Color: 1
Size: 3104 Color: 1
Size: 154 Color: 0
Size: 48 Color: 1
Size: 16 Color: 0

Bin 53: 0 of cap free
Amount of items: 5
Items: 
Size: 5291 Color: 1
Size: 997 Color: 1
Size: 844 Color: 1
Size: 308 Color: 0
Size: 256 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 1
Size: 700 Color: 1
Size: 416 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 4370 Color: 1
Size: 2774 Color: 1
Size: 552 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5736 Color: 1
Size: 1776 Color: 1
Size: 184 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6792 Color: 1
Size: 760 Color: 1
Size: 144 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 3851 Color: 1
Size: 3205 Color: 1
Size: 640 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5828 Color: 1
Size: 1466 Color: 1
Size: 402 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5480 Color: 1
Size: 1862 Color: 1
Size: 354 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6710 Color: 1
Size: 848 Color: 1
Size: 138 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6130 Color: 1
Size: 1306 Color: 1
Size: 260 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 1
Size: 924 Color: 1
Size: 224 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 3864 Color: 1
Size: 3208 Color: 1
Size: 624 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5478 Color: 1
Size: 2132 Color: 1
Size: 86 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 3849 Color: 1
Size: 3207 Color: 1
Size: 640 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5207 Color: 1
Size: 2075 Color: 1
Size: 414 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 6674 Color: 1
Size: 822 Color: 1
Size: 200 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6767 Color: 1
Size: 761 Color: 1
Size: 168 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 1
Size: 2391 Color: 1
Size: 478 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6093 Color: 1
Size: 1379 Color: 1
Size: 224 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 3852 Color: 1
Size: 3204 Color: 1
Size: 640 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 1
Size: 1184 Color: 1
Size: 216 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 1
Size: 901 Color: 1
Size: 178 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5623 Color: 1
Size: 1729 Color: 1
Size: 344 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 1
Size: 1064 Color: 1
Size: 480 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 6300 Color: 1
Size: 1156 Color: 1
Size: 240 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 1
Size: 832 Color: 1
Size: 172 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5847 Color: 1
Size: 983 Color: 1
Size: 866 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 1
Size: 1337 Color: 1
Size: 182 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 1
Size: 1014 Color: 1
Size: 200 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 4807 Color: 1
Size: 2369 Color: 1
Size: 520 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6345 Color: 1
Size: 1085 Color: 1
Size: 266 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 1
Size: 934 Color: 1
Size: 184 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 1
Size: 650 Color: 1
Size: 288 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 1
Size: 1560 Color: 0
Size: 1281 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 6552 Color: 1
Size: 948 Color: 1
Size: 196 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 1
Size: 1231 Color: 1
Size: 244 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6286 Color: 1
Size: 1178 Color: 1
Size: 232 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 6501 Color: 1
Size: 775 Color: 1
Size: 420 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 6444 Color: 1
Size: 932 Color: 1
Size: 320 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 1
Size: 3164 Color: 1
Size: 640 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6140 Color: 1
Size: 1002 Color: 1
Size: 554 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 6564 Color: 1
Size: 968 Color: 1
Size: 164 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 1
Size: 963 Color: 1
Size: 136 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 1
Size: 1082 Color: 1
Size: 140 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 1
Size: 1021 Color: 1
Size: 16 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 1
Size: 2031 Color: 1
Size: 1788 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 1
Size: 1771 Color: 1
Size: 184 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 1
Size: 1293 Color: 1
Size: 288 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6035 Color: 1
Size: 1564 Color: 1
Size: 96 Color: 0

Bin 102: 2 of cap free
Amount of items: 3
Items: 
Size: 4780 Color: 1
Size: 2770 Color: 1
Size: 144 Color: 0

Bin 103: 2 of cap free
Amount of items: 5
Items: 
Size: 3908 Color: 1
Size: 1654 Color: 1
Size: 1388 Color: 1
Size: 520 Color: 0
Size: 224 Color: 0

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 1
Size: 1317 Color: 1
Size: 216 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 1
Size: 808 Color: 1
Size: 160 Color: 0

Bin 106: 3 of cap free
Amount of items: 3
Items: 
Size: 4572 Color: 1
Size: 2865 Color: 1
Size: 256 Color: 0

Bin 107: 4 of cap free
Amount of items: 3
Items: 
Size: 5252 Color: 1
Size: 2408 Color: 1
Size: 32 Color: 0

Bin 108: 4 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 1
Size: 1850 Color: 1
Size: 128 Color: 0

Bin 109: 4 of cap free
Amount of items: 3
Items: 
Size: 6536 Color: 1
Size: 954 Color: 1
Size: 202 Color: 0

Bin 110: 4 of cap free
Amount of items: 3
Items: 
Size: 5917 Color: 1
Size: 1483 Color: 1
Size: 292 Color: 0

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 1
Size: 785 Color: 1
Size: 260 Color: 0

Bin 112: 7 of cap free
Amount of items: 3
Items: 
Size: 5259 Color: 1
Size: 2414 Color: 1
Size: 16 Color: 0

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 6395 Color: 1
Size: 921 Color: 1
Size: 372 Color: 0

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 2620 Color: 1
Size: 320 Color: 0

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 1
Size: 1322 Color: 1
Size: 552 Color: 0

Bin 116: 11 of cap free
Amount of items: 3
Items: 
Size: 5571 Color: 1
Size: 1482 Color: 1
Size: 632 Color: 0

Bin 117: 13 of cap free
Amount of items: 3
Items: 
Size: 6593 Color: 1
Size: 684 Color: 1
Size: 406 Color: 0

Bin 118: 14 of cap free
Amount of items: 3
Items: 
Size: 5162 Color: 1
Size: 2460 Color: 1
Size: 60 Color: 0

Bin 119: 22 of cap free
Amount of items: 3
Items: 
Size: 6117 Color: 1
Size: 1385 Color: 1
Size: 172 Color: 0

Bin 120: 52 of cap free
Amount of items: 4
Items: 
Size: 4436 Color: 1
Size: 2436 Color: 1
Size: 480 Color: 0
Size: 292 Color: 0

Bin 121: 59 of cap free
Amount of items: 3
Items: 
Size: 4369 Color: 1
Size: 2636 Color: 1
Size: 632 Color: 0

Bin 122: 62 of cap free
Amount of items: 3
Items: 
Size: 5946 Color: 1
Size: 1600 Color: 1
Size: 88 Color: 0

Bin 123: 84 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 1
Size: 2104 Color: 1
Size: 368 Color: 0

Bin 124: 134 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 1
Size: 2604 Color: 1
Size: 156 Color: 0

Bin 125: 149 of cap free
Amount of items: 3
Items: 
Size: 3202 Color: 1
Size: 2773 Color: 1
Size: 1572 Color: 0

Bin 126: 772 of cap free
Amount of items: 1
Items: 
Size: 6924 Color: 1

Bin 127: 778 of cap free
Amount of items: 1
Items: 
Size: 6918 Color: 1

Bin 128: 780 of cap free
Amount of items: 1
Items: 
Size: 6916 Color: 1

Bin 129: 829 of cap free
Amount of items: 1
Items: 
Size: 6867 Color: 1

Bin 130: 871 of cap free
Amount of items: 1
Items: 
Size: 6825 Color: 1

Bin 131: 878 of cap free
Amount of items: 1
Items: 
Size: 6818 Color: 1

Bin 132: 913 of cap free
Amount of items: 1
Items: 
Size: 6783 Color: 1

Bin 133: 1202 of cap free
Amount of items: 1
Items: 
Size: 6494 Color: 1

Total size: 1015872
Total free space: 7696

