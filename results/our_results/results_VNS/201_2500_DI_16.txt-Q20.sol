Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 9
Size: 1029 Color: 1
Size: 204 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 13
Size: 1022 Color: 9
Size: 204 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 19
Size: 1018 Color: 19
Size: 200 Color: 16

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1404 Color: 12
Size: 567 Color: 14
Size: 433 Color: 12
Size: 68 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 8
Size: 667 Color: 3
Size: 132 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 662 Color: 11
Size: 100 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 5
Size: 684 Color: 8
Size: 40 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1785 Color: 4
Size: 573 Color: 17
Size: 114 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 12
Size: 618 Color: 9
Size: 36 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 1
Size: 540 Color: 4
Size: 104 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 9
Size: 546 Color: 12
Size: 76 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 7
Size: 546 Color: 15
Size: 44 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1885 Color: 19
Size: 491 Color: 13
Size: 96 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 11
Size: 484 Color: 10
Size: 96 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 12
Size: 364 Color: 10
Size: 134 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 8
Size: 404 Color: 5
Size: 86 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 0
Size: 373 Color: 13
Size: 88 Color: 19

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 13
Size: 367 Color: 5
Size: 88 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 13
Size: 382 Color: 14
Size: 72 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 19
Size: 322 Color: 3
Size: 114 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 12
Size: 262 Color: 0
Size: 108 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 5
Size: 204 Color: 6
Size: 120 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 18
Size: 214 Color: 2
Size: 100 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 4
Size: 204 Color: 16
Size: 64 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 19
Size: 164 Color: 12
Size: 96 Color: 17

Bin 26: 1 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 3
Size: 1027 Color: 1
Size: 136 Color: 18
Size: 64 Color: 10

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 19
Size: 681 Color: 16
Size: 152 Color: 17

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 1
Size: 787 Color: 7
Size: 32 Color: 19

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 17
Size: 501 Color: 0
Size: 236 Color: 12

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1877 Color: 11
Size: 538 Color: 5
Size: 56 Color: 7

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1953 Color: 12
Size: 462 Color: 6
Size: 56 Color: 9

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 12
Size: 462 Color: 1
Size: 48 Color: 19

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 11
Size: 497 Color: 18
Size: 52 Color: 15

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 3
Size: 892 Color: 15
Size: 24 Color: 11

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 5
Size: 766 Color: 3

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1996 Color: 4
Size: 434 Color: 12
Size: 40 Color: 7

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 18
Size: 272 Color: 15
Size: 136 Color: 12

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 2138 Color: 3
Size: 332 Color: 5

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 12
Size: 780 Color: 15
Size: 272 Color: 9

Bin 40: 3 of cap free
Amount of items: 4
Items: 
Size: 1552 Color: 3
Size: 504 Color: 12
Size: 381 Color: 6
Size: 32 Color: 7

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 10
Size: 770 Color: 4
Size: 48 Color: 12

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 19
Size: 576 Color: 15
Size: 228 Color: 3

Bin 43: 3 of cap free
Amount of items: 2
Items: 
Size: 2025 Color: 19
Size: 444 Color: 6

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 18
Size: 1030 Color: 12
Size: 200 Color: 4

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1241 Color: 6
Size: 1227 Color: 0

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 1871 Color: 10
Size: 581 Color: 18
Size: 16 Color: 15

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 2218 Color: 2
Size: 242 Color: 6
Size: 8 Color: 16

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1466 Color: 16
Size: 1000 Color: 18

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 8
Size: 673 Color: 15

Bin 50: 6 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 18
Size: 342 Color: 6
Size: 8 Color: 1

Bin 51: 6 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 18
Size: 276 Color: 16
Size: 8 Color: 3

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 12
Size: 930 Color: 12
Size: 120 Color: 5

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1540 Color: 11
Size: 923 Color: 5

Bin 54: 10 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 10
Size: 685 Color: 2

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 19
Size: 604 Color: 2

Bin 56: 10 of cap free
Amount of items: 2
Items: 
Size: 1948 Color: 16
Size: 514 Color: 6

Bin 57: 10 of cap free
Amount of items: 2
Items: 
Size: 2077 Color: 0
Size: 385 Color: 18

Bin 58: 11 of cap free
Amount of items: 17
Items: 
Size: 331 Color: 2
Size: 302 Color: 4
Size: 300 Color: 14
Size: 176 Color: 14
Size: 156 Color: 3
Size: 152 Color: 16
Size: 134 Color: 4
Size: 132 Color: 7
Size: 112 Color: 18
Size: 100 Color: 8
Size: 98 Color: 3
Size: 84 Color: 19
Size: 84 Color: 17
Size: 80 Color: 7
Size: 74 Color: 18
Size: 74 Color: 13
Size: 72 Color: 1

Bin 59: 12 of cap free
Amount of items: 2
Items: 
Size: 2033 Color: 18
Size: 427 Color: 13

Bin 60: 12 of cap free
Amount of items: 2
Items: 
Size: 2076 Color: 1
Size: 384 Color: 19

Bin 61: 14 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 1
Size: 881 Color: 14
Size: 48 Color: 16

Bin 62: 16 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 18
Size: 840 Color: 4
Size: 158 Color: 16

Bin 63: 22 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 19
Size: 793 Color: 18

Bin 64: 25 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 19
Size: 842 Color: 9
Size: 84 Color: 12

Bin 65: 31 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 10
Size: 1028 Color: 17
Size: 176 Color: 19

Bin 66: 2200 of cap free
Amount of items: 4
Items: 
Size: 72 Color: 16
Size: 72 Color: 10
Size: 64 Color: 15
Size: 64 Color: 13

Total size: 160680
Total free space: 2472

