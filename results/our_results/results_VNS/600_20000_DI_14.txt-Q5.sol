Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 7674 Color: 0
Size: 6382 Color: 2
Size: 1272 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 2
Size: 6284 Color: 3
Size: 348 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9560 Color: 2
Size: 5476 Color: 1
Size: 292 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9804 Color: 2
Size: 4604 Color: 2
Size: 920 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 2
Size: 5132 Color: 4
Size: 328 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 4
Size: 4782 Color: 3
Size: 586 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10093 Color: 1
Size: 5003 Color: 3
Size: 232 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 4
Size: 4744 Color: 1
Size: 448 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10366 Color: 2
Size: 4136 Color: 2
Size: 826 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 1
Size: 4571 Color: 2
Size: 324 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10457 Color: 3
Size: 4311 Color: 1
Size: 560 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10732 Color: 4
Size: 3820 Color: 3
Size: 776 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11009 Color: 4
Size: 3601 Color: 2
Size: 718 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11105 Color: 4
Size: 3401 Color: 1
Size: 822 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11656 Color: 3
Size: 3064 Color: 4
Size: 608 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11683 Color: 1
Size: 3389 Color: 4
Size: 256 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11962 Color: 1
Size: 2684 Color: 1
Size: 682 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 2
Size: 2828 Color: 1
Size: 444 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12133 Color: 4
Size: 2251 Color: 2
Size: 944 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12290 Color: 1
Size: 2534 Color: 2
Size: 504 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12484 Color: 1
Size: 2264 Color: 2
Size: 580 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 2
Size: 1672 Color: 1
Size: 1120 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 0
Size: 1492 Color: 4
Size: 1272 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 4
Size: 2308 Color: 4
Size: 442 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 3
Size: 1644 Color: 1
Size: 1072 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12627 Color: 0
Size: 1797 Color: 2
Size: 904 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12674 Color: 0
Size: 2214 Color: 1
Size: 440 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 2
Size: 1784 Color: 1
Size: 824 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12809 Color: 1
Size: 1655 Color: 3
Size: 864 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12825 Color: 1
Size: 1921 Color: 2
Size: 582 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 3
Size: 2328 Color: 2
Size: 128 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 3
Size: 1364 Color: 2
Size: 1088 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12898 Color: 0
Size: 2026 Color: 3
Size: 404 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 3
Size: 2268 Color: 2
Size: 72 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 1
Size: 1964 Color: 3
Size: 392 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13023 Color: 3
Size: 1897 Color: 1
Size: 408 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 0
Size: 1272 Color: 2
Size: 944 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13130 Color: 4
Size: 1484 Color: 1
Size: 714 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13186 Color: 3
Size: 1786 Color: 1
Size: 356 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 2
Size: 1766 Color: 1
Size: 418 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 4
Size: 1832 Color: 3
Size: 304 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13202 Color: 3
Size: 1774 Color: 1
Size: 352 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13212 Color: 1
Size: 1772 Color: 3
Size: 344 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13304 Color: 3
Size: 1660 Color: 0
Size: 364 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 2
Size: 1526 Color: 2
Size: 480 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 4
Size: 1264 Color: 3
Size: 728 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13343 Color: 3
Size: 1621 Color: 2
Size: 364 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13356 Color: 3
Size: 1364 Color: 4
Size: 608 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13371 Color: 1
Size: 1631 Color: 2
Size: 326 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13498 Color: 3
Size: 1126 Color: 1
Size: 704 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13455 Color: 0
Size: 1561 Color: 1
Size: 312 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13548 Color: 4
Size: 1276 Color: 0
Size: 504 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13570 Color: 1
Size: 1454 Color: 3
Size: 304 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 3
Size: 936 Color: 2
Size: 784 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 3
Size: 1008 Color: 4
Size: 628 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 4
Size: 1276 Color: 0
Size: 352 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13786 Color: 3
Size: 960 Color: 4
Size: 582 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 4
Size: 1276 Color: 0
Size: 288 Color: 3

Bin 59: 1 of cap free
Amount of items: 7
Items: 
Size: 7666 Color: 2
Size: 2219 Color: 2
Size: 2044 Color: 3
Size: 1402 Color: 0
Size: 952 Color: 4
Size: 716 Color: 4
Size: 328 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 0
Size: 5607 Color: 4
Size: 704 Color: 3

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 10385 Color: 2
Size: 4742 Color: 0
Size: 200 Color: 3

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10764 Color: 2
Size: 3923 Color: 3
Size: 640 Color: 2

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10856 Color: 4
Size: 4093 Color: 1
Size: 378 Color: 3

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11033 Color: 4
Size: 4086 Color: 1
Size: 208 Color: 0

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11082 Color: 4
Size: 3541 Color: 3
Size: 704 Color: 2

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11269 Color: 3
Size: 3586 Color: 1
Size: 472 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 2
Size: 3863 Color: 2
Size: 176 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11460 Color: 1
Size: 3553 Color: 0
Size: 314 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 1
Size: 2306 Color: 2
Size: 904 Color: 3

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12650 Color: 2
Size: 2205 Color: 4
Size: 472 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12989 Color: 4
Size: 2058 Color: 3
Size: 280 Color: 4

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13133 Color: 2
Size: 2042 Color: 3
Size: 152 Color: 0

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13163 Color: 0
Size: 1620 Color: 3
Size: 544 Color: 1

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13327 Color: 4
Size: 1024 Color: 3
Size: 976 Color: 0

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 13383 Color: 2
Size: 1944 Color: 0

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 13439 Color: 3
Size: 1560 Color: 0
Size: 328 Color: 0

Bin 77: 2 of cap free
Amount of items: 5
Items: 
Size: 7684 Color: 2
Size: 4328 Color: 1
Size: 1546 Color: 3
Size: 1448 Color: 4
Size: 320 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 8710 Color: 1
Size: 6056 Color: 0
Size: 560 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 8876 Color: 3
Size: 5502 Color: 2
Size: 948 Color: 0

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 9594 Color: 1
Size: 3884 Color: 3
Size: 1848 Color: 0

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 9953 Color: 0
Size: 3228 Color: 0
Size: 2145 Color: 3

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 10409 Color: 1
Size: 4501 Color: 3
Size: 416 Color: 0

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11233 Color: 0
Size: 3561 Color: 3
Size: 532 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11781 Color: 3
Size: 2917 Color: 1
Size: 628 Color: 4

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 11805 Color: 2
Size: 3213 Color: 3
Size: 308 Color: 0

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 3
Size: 2936 Color: 1
Size: 96 Color: 4

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 0
Size: 2676 Color: 2
Size: 210 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 12453 Color: 2
Size: 2521 Color: 1
Size: 352 Color: 0

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 12954 Color: 1
Size: 2372 Color: 2

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 2
Size: 4121 Color: 1
Size: 456 Color: 2

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 11065 Color: 2
Size: 3900 Color: 1
Size: 360 Color: 0

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 2
Size: 3524 Color: 1
Size: 720 Color: 0

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 11549 Color: 2
Size: 3592 Color: 4
Size: 184 Color: 1

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 0
Size: 2937 Color: 2
Size: 818 Color: 3

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 2
Size: 3581 Color: 2
Size: 168 Color: 3

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 11837 Color: 4
Size: 3128 Color: 0
Size: 360 Color: 3

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 12045 Color: 3
Size: 2806 Color: 1
Size: 474 Color: 4

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 12269 Color: 1
Size: 2056 Color: 3
Size: 1000 Color: 1

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 1
Size: 2072 Color: 3
Size: 760 Color: 3

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 8600 Color: 2
Size: 6308 Color: 3
Size: 416 Color: 0

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 8782 Color: 2
Size: 6184 Color: 0
Size: 358 Color: 1

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 9654 Color: 2
Size: 4540 Color: 0
Size: 1130 Color: 3

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 9884 Color: 1
Size: 4730 Color: 0
Size: 710 Color: 3

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 11100 Color: 1
Size: 3928 Color: 2
Size: 296 Color: 4

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 10632 Color: 4
Size: 4141 Color: 2
Size: 550 Color: 1

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 11245 Color: 4
Size: 3542 Color: 0
Size: 536 Color: 3

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 11940 Color: 2
Size: 3383 Color: 4

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 2
Size: 3051 Color: 4

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 8601 Color: 1
Size: 6385 Color: 4
Size: 336 Color: 1

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 9640 Color: 4
Size: 5458 Color: 4
Size: 224 Color: 0

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 9900 Color: 1
Size: 4524 Color: 2
Size: 898 Color: 0

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 0
Size: 3836 Color: 2
Size: 460 Color: 1

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 12381 Color: 4
Size: 2677 Color: 1
Size: 264 Color: 3

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 13340 Color: 2
Size: 1982 Color: 1

Bin 115: 6 of cap free
Amount of items: 2
Items: 
Size: 13418 Color: 1
Size: 1904 Color: 0

Bin 116: 6 of cap free
Amount of items: 2
Items: 
Size: 13646 Color: 1
Size: 1676 Color: 0

Bin 117: 7 of cap free
Amount of items: 5
Items: 
Size: 7672 Color: 0
Size: 3884 Color: 0
Size: 2783 Color: 1
Size: 534 Color: 3
Size: 448 Color: 3

Bin 118: 7 of cap free
Amount of items: 2
Items: 
Size: 9708 Color: 2
Size: 5613 Color: 3

Bin 119: 7 of cap free
Amount of items: 3
Items: 
Size: 12021 Color: 1
Size: 1834 Color: 0
Size: 1466 Color: 3

Bin 120: 7 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 2
Size: 1997 Color: 0

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 11032 Color: 2
Size: 2192 Color: 3
Size: 2096 Color: 3

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 3
Size: 2757 Color: 0

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 4
Size: 1951 Color: 0
Size: 164 Color: 0

Bin 124: 10 of cap free
Amount of items: 3
Items: 
Size: 11084 Color: 2
Size: 4138 Color: 0
Size: 96 Color: 0

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 11950 Color: 1
Size: 3368 Color: 2

Bin 126: 11 of cap free
Amount of items: 3
Items: 
Size: 9325 Color: 4
Size: 4516 Color: 3
Size: 1476 Color: 4

Bin 127: 11 of cap free
Amount of items: 3
Items: 
Size: 9472 Color: 1
Size: 5653 Color: 3
Size: 192 Color: 0

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 11816 Color: 3
Size: 3501 Color: 4

Bin 129: 11 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 2
Size: 2344 Color: 1

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 9788 Color: 2
Size: 5064 Color: 1
Size: 464 Color: 0

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 13229 Color: 0
Size: 2087 Color: 1

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 1
Size: 1674 Color: 2

Bin 133: 13 of cap free
Amount of items: 3
Items: 
Size: 11010 Color: 3
Size: 4081 Color: 4
Size: 224 Color: 2

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 12667 Color: 0
Size: 2648 Color: 2

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 3
Size: 1751 Color: 4

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 9172 Color: 2
Size: 5518 Color: 4
Size: 624 Color: 3

Bin 137: 14 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 4
Size: 1688 Color: 2
Size: 40 Color: 1

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 1
Size: 1594 Color: 0

Bin 139: 15 of cap free
Amount of items: 7
Items: 
Size: 7665 Color: 0
Size: 1406 Color: 2
Size: 1352 Color: 0
Size: 1286 Color: 2
Size: 1256 Color: 3
Size: 1248 Color: 3
Size: 1100 Color: 1

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 11711 Color: 1
Size: 3602 Color: 0

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 13352 Color: 1
Size: 1961 Color: 0

Bin 142: 16 of cap free
Amount of items: 18
Items: 
Size: 1104 Color: 3
Size: 1100 Color: 1
Size: 1008 Color: 3
Size: 976 Color: 1
Size: 944 Color: 0
Size: 920 Color: 0
Size: 904 Color: 4
Size: 896 Color: 3
Size: 880 Color: 3
Size: 818 Color: 2
Size: 816 Color: 4
Size: 816 Color: 4
Size: 814 Color: 4
Size: 776 Color: 4
Size: 768 Color: 2
Size: 736 Color: 1
Size: 716 Color: 1
Size: 320 Color: 2

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 1
Size: 1924 Color: 0

Bin 144: 17 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 3
Size: 4488 Color: 4
Size: 2911 Color: 2

Bin 145: 17 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 2
Size: 2551 Color: 0

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 12903 Color: 0
Size: 2408 Color: 4

Bin 147: 17 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 4
Size: 1575 Color: 2

Bin 148: 18 of cap free
Amount of items: 3
Items: 
Size: 8726 Color: 3
Size: 6392 Color: 4
Size: 192 Color: 0

Bin 149: 18 of cap free
Amount of items: 4
Items: 
Size: 10958 Color: 3
Size: 2584 Color: 4
Size: 1496 Color: 4
Size: 272 Color: 1

Bin 150: 19 of cap free
Amount of items: 2
Items: 
Size: 9929 Color: 4
Size: 5380 Color: 3

Bin 151: 20 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 2
Size: 2728 Color: 4

Bin 152: 21 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 3
Size: 2691 Color: 4

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 1
Size: 1731 Color: 4

Bin 154: 22 of cap free
Amount of items: 2
Items: 
Size: 10417 Color: 3
Size: 4889 Color: 4

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 10426 Color: 4
Size: 4880 Color: 3

Bin 156: 22 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 0
Size: 2530 Color: 3
Size: 224 Color: 2

Bin 157: 23 of cap free
Amount of items: 2
Items: 
Size: 10621 Color: 0
Size: 4684 Color: 4

Bin 158: 23 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 0
Size: 1831 Color: 1

Bin 159: 24 of cap free
Amount of items: 3
Items: 
Size: 9256 Color: 1
Size: 5608 Color: 4
Size: 440 Color: 0

Bin 160: 25 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 0
Size: 3151 Color: 2

Bin 161: 26 of cap free
Amount of items: 2
Items: 
Size: 11660 Color: 1
Size: 3642 Color: 2

Bin 162: 26 of cap free
Amount of items: 3
Items: 
Size: 12862 Color: 3
Size: 2328 Color: 0
Size: 112 Color: 4

Bin 163: 31 of cap free
Amount of items: 3
Items: 
Size: 8593 Color: 1
Size: 6372 Color: 4
Size: 332 Color: 0

Bin 164: 32 of cap free
Amount of items: 2
Items: 
Size: 10361 Color: 1
Size: 4935 Color: 2

Bin 165: 34 of cap free
Amount of items: 2
Items: 
Size: 13338 Color: 4
Size: 1956 Color: 0

Bin 166: 35 of cap free
Amount of items: 3
Items: 
Size: 8545 Color: 1
Size: 5916 Color: 4
Size: 832 Color: 4

Bin 167: 36 of cap free
Amount of items: 6
Items: 
Size: 7667 Color: 0
Size: 2663 Color: 0
Size: 2292 Color: 3
Size: 1232 Color: 4
Size: 1122 Color: 4
Size: 316 Color: 4

Bin 168: 36 of cap free
Amount of items: 2
Items: 
Size: 12232 Color: 4
Size: 3060 Color: 0

Bin 169: 38 of cap free
Amount of items: 2
Items: 
Size: 11554 Color: 4
Size: 3736 Color: 2

Bin 170: 41 of cap free
Amount of items: 2
Items: 
Size: 13053 Color: 2
Size: 2234 Color: 1

Bin 171: 44 of cap free
Amount of items: 3
Items: 
Size: 11249 Color: 1
Size: 3939 Color: 0
Size: 96 Color: 2

Bin 172: 47 of cap free
Amount of items: 2
Items: 
Size: 12683 Color: 2
Size: 2598 Color: 3

Bin 173: 49 of cap free
Amount of items: 2
Items: 
Size: 12477 Color: 4
Size: 2802 Color: 3

Bin 174: 52 of cap free
Amount of items: 3
Items: 
Size: 7796 Color: 2
Size: 6720 Color: 1
Size: 760 Color: 0

Bin 175: 54 of cap free
Amount of items: 2
Items: 
Size: 13173 Color: 2
Size: 2101 Color: 4

Bin 176: 54 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 4
Size: 2021 Color: 2

Bin 177: 55 of cap free
Amount of items: 2
Items: 
Size: 11189 Color: 3
Size: 4084 Color: 4

Bin 178: 56 of cap free
Amount of items: 2
Items: 
Size: 10652 Color: 4
Size: 4620 Color: 3

Bin 179: 57 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 1
Size: 5633 Color: 4

Bin 180: 58 of cap free
Amount of items: 2
Items: 
Size: 12124 Color: 0
Size: 3146 Color: 2

Bin 181: 60 of cap free
Amount of items: 2
Items: 
Size: 7668 Color: 1
Size: 7600 Color: 0

Bin 182: 60 of cap free
Amount of items: 2
Items: 
Size: 9740 Color: 1
Size: 5528 Color: 3

Bin 183: 68 of cap free
Amount of items: 2
Items: 
Size: 12303 Color: 4
Size: 2957 Color: 3

Bin 184: 80 of cap free
Amount of items: 2
Items: 
Size: 11966 Color: 4
Size: 3282 Color: 0

Bin 185: 83 of cap free
Amount of items: 2
Items: 
Size: 12882 Color: 0
Size: 2363 Color: 4

Bin 186: 86 of cap free
Amount of items: 2
Items: 
Size: 11829 Color: 1
Size: 3413 Color: 2

Bin 187: 86 of cap free
Amount of items: 2
Items: 
Size: 12108 Color: 3
Size: 3134 Color: 4

Bin 188: 92 of cap free
Amount of items: 3
Items: 
Size: 8569 Color: 0
Size: 6387 Color: 2
Size: 280 Color: 3

Bin 189: 95 of cap free
Amount of items: 2
Items: 
Size: 12856 Color: 4
Size: 2377 Color: 0

Bin 190: 104 of cap free
Amount of items: 2
Items: 
Size: 10668 Color: 3
Size: 4556 Color: 4

Bin 191: 122 of cap free
Amount of items: 3
Items: 
Size: 7700 Color: 0
Size: 6386 Color: 1
Size: 1120 Color: 3

Bin 192: 136 of cap free
Amount of items: 2
Items: 
Size: 8804 Color: 4
Size: 6388 Color: 2

Bin 193: 144 of cap free
Amount of items: 2
Items: 
Size: 10376 Color: 0
Size: 4808 Color: 1

Bin 194: 169 of cap free
Amount of items: 2
Items: 
Size: 9463 Color: 4
Size: 5696 Color: 1

Bin 195: 170 of cap free
Amount of items: 2
Items: 
Size: 11057 Color: 4
Size: 4101 Color: 3

Bin 196: 180 of cap free
Amount of items: 2
Items: 
Size: 9916 Color: 1
Size: 5232 Color: 4

Bin 197: 190 of cap free
Amount of items: 29
Items: 
Size: 710 Color: 3
Size: 706 Color: 4
Size: 678 Color: 4
Size: 676 Color: 2
Size: 672 Color: 3
Size: 664 Color: 3
Size: 624 Color: 1
Size: 608 Color: 1
Size: 590 Color: 2
Size: 576 Color: 2
Size: 560 Color: 3
Size: 560 Color: 2
Size: 528 Color: 4
Size: 528 Color: 0
Size: 512 Color: 0
Size: 508 Color: 0
Size: 504 Color: 0
Size: 480 Color: 2
Size: 456 Color: 1
Size: 450 Color: 4
Size: 448 Color: 1
Size: 440 Color: 4
Size: 408 Color: 0
Size: 404 Color: 3
Size: 404 Color: 1
Size: 400 Color: 4
Size: 388 Color: 1
Size: 384 Color: 1
Size: 272 Color: 3

Bin 198: 240 of cap free
Amount of items: 2
Items: 
Size: 7764 Color: 1
Size: 7324 Color: 3

Bin 199: 11482 of cap free
Amount of items: 12
Items: 
Size: 404 Color: 0
Size: 400 Color: 0
Size: 392 Color: 0
Size: 384 Color: 1
Size: 352 Color: 2
Size: 330 Color: 1
Size: 288 Color: 4
Size: 272 Color: 4
Size: 256 Color: 4
Size: 256 Color: 3
Size: 256 Color: 3
Size: 256 Color: 3

Total size: 3034944
Total free space: 15328

