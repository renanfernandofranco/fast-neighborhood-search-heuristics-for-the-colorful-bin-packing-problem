Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 18
Size: 254 Color: 6
Size: 251 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 10
Size: 308 Color: 4
Size: 251 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 9
Size: 333 Color: 18
Size: 256 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 5
Size: 357 Color: 0
Size: 262 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 12
Size: 324 Color: 1
Size: 274 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 2
Size: 280 Color: 2
Size: 259 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 13
Size: 357 Color: 14
Size: 273 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 14
Size: 264 Color: 6
Size: 256 Color: 17

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 12
Size: 359 Color: 4
Size: 250 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 16
Size: 255 Color: 5
Size: 252 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 264 Color: 1
Size: 252 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 15
Size: 294 Color: 16
Size: 268 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 368 Color: 3
Size: 262 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 18
Size: 298 Color: 13
Size: 288 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 15
Size: 342 Color: 8
Size: 256 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 19
Size: 261 Color: 16
Size: 256 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 337 Color: 13
Size: 292 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 19
Size: 368 Color: 12
Size: 260 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 9
Size: 325 Color: 3
Size: 274 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 303 Color: 10
Size: 291 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 17
Size: 256 Color: 1
Size: 254 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 12
Size: 357 Color: 1
Size: 272 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 14
Size: 289 Color: 4
Size: 280 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 313 Color: 11
Size: 257 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8
Size: 300 Color: 3
Size: 299 Color: 13

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 2
Size: 367 Color: 13
Size: 250 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 6
Size: 353 Color: 5
Size: 283 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 16
Size: 308 Color: 6
Size: 299 Color: 15

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 278 Color: 2
Size: 258 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 7
Size: 312 Color: 7
Size: 272 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 270 Color: 10
Size: 263 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 2
Size: 360 Color: 15
Size: 261 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 8
Size: 361 Color: 11
Size: 274 Color: 9

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 357 Color: 2
Size: 262 Color: 5

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 6
Size: 257 Color: 12
Size: 252 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 345 Color: 10
Size: 266 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 7
Size: 358 Color: 0
Size: 264 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 15
Size: 251 Color: 18
Size: 251 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 5
Size: 305 Color: 16
Size: 270 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 17
Size: 302 Color: 1
Size: 266 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 306 Color: 17
Size: 253 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 18
Size: 327 Color: 11
Size: 255 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 10
Size: 273 Color: 4
Size: 250 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 14
Size: 326 Color: 11
Size: 284 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 11
Size: 278 Color: 12
Size: 251 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 323 Color: 17
Size: 305 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 19
Size: 283 Color: 19
Size: 250 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 16
Size: 325 Color: 6
Size: 273 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 3
Size: 260 Color: 1
Size: 251 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 9
Size: 276 Color: 6
Size: 261 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 9
Size: 341 Color: 6
Size: 276 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 335 Color: 17
Size: 278 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 11
Size: 264 Color: 6
Size: 257 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 367 Color: 16
Size: 252 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 337 Color: 16
Size: 297 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 8
Size: 354 Color: 16
Size: 277 Color: 19

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 0
Size: 318 Color: 14
Size: 317 Color: 8

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 7
Size: 265 Color: 15
Size: 258 Color: 7

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 14
Size: 266 Color: 6
Size: 261 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 13
Size: 327 Color: 16
Size: 295 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 307 Color: 1
Size: 270 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 359 Color: 7
Size: 271 Color: 19

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 13
Size: 279 Color: 16
Size: 262 Color: 5

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 4
Size: 262 Color: 3
Size: 252 Color: 13

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 19
Size: 323 Color: 6
Size: 303 Color: 18

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 10
Size: 333 Color: 2
Size: 301 Color: 9

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 10
Size: 282 Color: 16
Size: 250 Color: 6

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 4
Size: 277 Color: 1
Size: 268 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 270 Color: 13
Size: 265 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 10
Size: 320 Color: 7
Size: 274 Color: 8

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 275 Color: 13
Size: 256 Color: 9

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 18
Size: 330 Color: 5
Size: 310 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 312 Color: 19
Size: 275 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 6
Size: 294 Color: 14
Size: 256 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8
Size: 308 Color: 18
Size: 255 Color: 19

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 14
Size: 265 Color: 2
Size: 262 Color: 16

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 269 Color: 3
Size: 251 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 351 Color: 7
Size: 266 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 9
Size: 351 Color: 7
Size: 298 Color: 19

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 283 Color: 4
Size: 256 Color: 9

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 11
Size: 298 Color: 3
Size: 268 Color: 7

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 19
Size: 253 Color: 15
Size: 252 Color: 18

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8
Size: 290 Color: 11
Size: 268 Color: 5

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 19
Size: 318 Color: 5
Size: 266 Color: 17

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 4
Size: 291 Color: 5
Size: 250 Color: 13

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 322 Color: 3
Size: 254 Color: 6

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 6
Size: 252 Color: 10
Size: 250 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 19
Size: 298 Color: 8
Size: 285 Color: 13

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 12
Size: 262 Color: 0
Size: 251 Color: 15

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 283 Color: 1
Size: 257 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 369 Color: 7
Size: 262 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 9
Size: 315 Color: 8
Size: 260 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 302 Color: 7
Size: 285 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 12
Size: 296 Color: 19
Size: 267 Color: 13

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 14
Size: 308 Color: 9
Size: 255 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 11
Size: 372 Color: 3
Size: 256 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 290 Color: 9
Size: 285 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 265 Color: 2
Size: 254 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 16
Size: 318 Color: 8
Size: 296 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 13
Size: 347 Color: 7
Size: 262 Color: 6

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 19
Size: 298 Color: 4
Size: 285 Color: 9

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 12
Size: 301 Color: 18
Size: 257 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 19
Size: 300 Color: 4
Size: 288 Color: 10

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 12
Size: 371 Color: 14
Size: 251 Color: 14

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 310 Color: 1
Size: 309 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 317 Color: 1
Size: 297 Color: 11

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 337 Color: 11
Size: 259 Color: 19

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 9
Size: 306 Color: 7
Size: 258 Color: 7

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 14
Size: 286 Color: 1
Size: 276 Color: 14

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 18
Size: 351 Color: 7
Size: 276 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 5
Size: 286 Color: 2
Size: 250 Color: 15

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 315 Color: 8
Size: 289 Color: 6

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 278 Color: 19
Size: 266 Color: 13

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 5
Size: 297 Color: 0
Size: 261 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 10
Size: 296 Color: 0
Size: 269 Color: 6

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 359 Color: 16
Size: 267 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 13
Size: 262 Color: 14
Size: 284 Color: 8

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 19
Size: 317 Color: 4
Size: 251 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 11
Size: 357 Color: 1
Size: 271 Color: 15

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 272 Color: 2
Size: 270 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 10
Size: 353 Color: 18
Size: 285 Color: 11

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 264 Color: 7
Size: 261 Color: 5

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9
Size: 256 Color: 16
Size: 252 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 327 Color: 5
Size: 277 Color: 2

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 268 Color: 5
Size: 268 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 286 Color: 14
Size: 269 Color: 17

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 10
Size: 317 Color: 11
Size: 271 Color: 9

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 9
Size: 332 Color: 18
Size: 299 Color: 9

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 12
Size: 324 Color: 19
Size: 286 Color: 16

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 12
Size: 311 Color: 14
Size: 294 Color: 19

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 8
Size: 360 Color: 0
Size: 256 Color: 12

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 12
Size: 277 Color: 14
Size: 251 Color: 10

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 5
Size: 280 Color: 10
Size: 276 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 266 Color: 6
Size: 254 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 9
Size: 359 Color: 2
Size: 270 Color: 11

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 15
Size: 289 Color: 17
Size: 256 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 283 Color: 8
Size: 259 Color: 18

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 16
Size: 355 Color: 2
Size: 256 Color: 14

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 9
Size: 276 Color: 3
Size: 259 Color: 17

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 362 Color: 13
Size: 262 Color: 4

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 10
Size: 259 Color: 2
Size: 251 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 364 Color: 7
Size: 260 Color: 14

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 18
Size: 251 Color: 6
Size: 250 Color: 15

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 292 Color: 19
Size: 277 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 4
Size: 274 Color: 4
Size: 270 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 269 Color: 18
Size: 267 Color: 13

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 3
Size: 298 Color: 4
Size: 283 Color: 2

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7
Size: 323 Color: 13
Size: 292 Color: 5

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 5
Size: 353 Color: 0
Size: 276 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 11
Size: 302 Color: 4
Size: 262 Color: 17

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 10
Size: 268 Color: 6
Size: 257 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 3
Size: 338 Color: 13
Size: 319 Color: 19

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 3
Size: 350 Color: 13
Size: 257 Color: 15

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 9
Size: 351 Color: 19
Size: 271 Color: 8

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 5
Size: 281 Color: 16
Size: 257 Color: 5

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 301 Color: 19
Size: 290 Color: 18

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 11
Size: 324 Color: 4
Size: 267 Color: 13

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 8
Size: 318 Color: 14
Size: 252 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 11
Size: 335 Color: 13
Size: 258 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 10
Size: 331 Color: 14
Size: 290 Color: 10

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 10
Size: 306 Color: 9
Size: 288 Color: 6

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 9
Size: 328 Color: 10
Size: 258 Color: 18

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 0
Size: 250 Color: 14

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 12
Size: 339 Color: 3
Size: 307 Color: 2

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 265 Color: 5
Size: 252 Color: 2

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 4
Size: 305 Color: 7
Size: 267 Color: 8

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 315 Color: 16
Size: 310 Color: 4

Total size: 167000
Total free space: 0

