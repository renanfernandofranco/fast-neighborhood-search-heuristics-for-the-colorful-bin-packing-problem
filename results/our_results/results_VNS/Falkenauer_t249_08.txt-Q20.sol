Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 14
Size: 258 Color: 18
Size: 254 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 277 Color: 5
Size: 275 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 14
Size: 334 Color: 7
Size: 264 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 19
Size: 349 Color: 4
Size: 250 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 12
Size: 263 Color: 7
Size: 256 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 19
Size: 351 Color: 1
Size: 260 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 12
Size: 262 Color: 4
Size: 256 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 6
Size: 285 Color: 15
Size: 270 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 9
Size: 261 Color: 9
Size: 252 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 14
Size: 305 Color: 2
Size: 257 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 18
Size: 360 Color: 19
Size: 260 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 8
Size: 289 Color: 10
Size: 269 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 13
Size: 319 Color: 0
Size: 275 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 295 Color: 3
Size: 263 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 12
Size: 350 Color: 13
Size: 259 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 13
Size: 286 Color: 13
Size: 260 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 11
Size: 267 Color: 10
Size: 251 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 266 Color: 6
Size: 377 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 15
Size: 270 Color: 5
Size: 255 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 11
Size: 260 Color: 8
Size: 257 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 334 Color: 15
Size: 262 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 6
Size: 306 Color: 10
Size: 298 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 275 Color: 4
Size: 261 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 8
Size: 317 Color: 5
Size: 288 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 12
Size: 325 Color: 8
Size: 320 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 270 Color: 17
Size: 250 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 9
Size: 278 Color: 7
Size: 263 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 10
Size: 334 Color: 13
Size: 320 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 317 Color: 2
Size: 278 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 346 Color: 10
Size: 291 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 14
Size: 300 Color: 8
Size: 251 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 363 Color: 10
Size: 254 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 325 Color: 0
Size: 274 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 6
Size: 252 Color: 5
Size: 250 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 19
Size: 279 Color: 16
Size: 270 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 267 Color: 12
Size: 264 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 6
Size: 313 Color: 12
Size: 309 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 287 Color: 8
Size: 263 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 11
Size: 340 Color: 0
Size: 264 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 13
Size: 255 Color: 13
Size: 252 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 5
Size: 345 Color: 11
Size: 270 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 7
Size: 271 Color: 4
Size: 250 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 17
Size: 283 Color: 7
Size: 268 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 10
Size: 343 Color: 3
Size: 275 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 15
Size: 371 Color: 4
Size: 258 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 16
Size: 320 Color: 19
Size: 278 Color: 14

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 326 Color: 17
Size: 263 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 18
Size: 275 Color: 4
Size: 266 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 13
Size: 334 Color: 12
Size: 268 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 331 Color: 6
Size: 286 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 18
Size: 252 Color: 6
Size: 250 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 266 Color: 3
Size: 258 Color: 17

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 9
Size: 338 Color: 15
Size: 276 Color: 10

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 18
Size: 257 Color: 8
Size: 255 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 313 Color: 4
Size: 308 Color: 15

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 8
Size: 275 Color: 18
Size: 259 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 19
Size: 359 Color: 6
Size: 276 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 358 Color: 18
Size: 254 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 13
Size: 319 Color: 14
Size: 279 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 6
Size: 366 Color: 7
Size: 252 Color: 14

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 10
Size: 283 Color: 9
Size: 339 Color: 17

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 6
Size: 267 Color: 3
Size: 265 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 324 Color: 3
Size: 307 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 335 Color: 7
Size: 261 Color: 4

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 11
Size: 273 Color: 19
Size: 262 Color: 12

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 314 Color: 15
Size: 308 Color: 9

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 347 Color: 8
Size: 250 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9
Size: 281 Color: 2
Size: 264 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 6
Size: 348 Color: 13
Size: 301 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 17
Size: 295 Color: 5
Size: 269 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 17
Size: 267 Color: 12
Size: 250 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 12
Size: 255 Color: 8
Size: 252 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 5
Size: 361 Color: 17
Size: 276 Color: 19

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8
Size: 312 Color: 8
Size: 253 Color: 19

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 6
Size: 284 Color: 0
Size: 269 Color: 17

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 271 Color: 11
Size: 250 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 300 Color: 2
Size: 293 Color: 18

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 9
Size: 317 Color: 4
Size: 254 Color: 10

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 259 Color: 14
Size: 251 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 15
Size: 362 Color: 5
Size: 271 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8
Size: 317 Color: 10
Size: 275 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 9
Size: 286 Color: 8
Size: 278 Color: 13

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 336 Color: 17
Size: 269 Color: 8

Total size: 83000
Total free space: 0

