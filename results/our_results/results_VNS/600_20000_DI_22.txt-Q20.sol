Capicity Bin: 16416
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 8240 Color: 14
Size: 6832 Color: 0
Size: 1048 Color: 2
Size: 296 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 18
Size: 6836 Color: 14
Size: 1076 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10164 Color: 14
Size: 5852 Color: 11
Size: 400 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11209 Color: 17
Size: 4341 Color: 4
Size: 866 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12036 Color: 5
Size: 3768 Color: 19
Size: 612 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12291 Color: 10
Size: 3429 Color: 4
Size: 696 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 8
Size: 3432 Color: 4
Size: 672 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 9
Size: 3044 Color: 13
Size: 696 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 6
Size: 2348 Color: 7
Size: 1360 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 2
Size: 3096 Color: 18
Size: 608 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12722 Color: 16
Size: 2982 Color: 16
Size: 712 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12848 Color: 17
Size: 3092 Color: 6
Size: 476 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12978 Color: 10
Size: 3002 Color: 11
Size: 436 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 1
Size: 2990 Color: 6
Size: 384 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13144 Color: 2
Size: 2584 Color: 2
Size: 688 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 8
Size: 2988 Color: 11
Size: 278 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 4
Size: 2692 Color: 18
Size: 536 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13332 Color: 13
Size: 2584 Color: 16
Size: 500 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 4
Size: 2582 Color: 19
Size: 370 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13488 Color: 2
Size: 2448 Color: 0
Size: 480 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 13
Size: 2572 Color: 10
Size: 294 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 7
Size: 2390 Color: 4
Size: 418 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13628 Color: 8
Size: 2704 Color: 7
Size: 84 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 12
Size: 1456 Color: 5
Size: 1216 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13750 Color: 19
Size: 1620 Color: 3
Size: 1046 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 14
Size: 2324 Color: 3
Size: 314 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 1
Size: 2164 Color: 9
Size: 424 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 9
Size: 2360 Color: 8
Size: 192 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13890 Color: 1
Size: 1364 Color: 5
Size: 1162 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 4
Size: 2040 Color: 5
Size: 352 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13929 Color: 9
Size: 2073 Color: 16
Size: 414 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13936 Color: 7
Size: 2004 Color: 11
Size: 476 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 6
Size: 2053 Color: 6
Size: 410 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 4
Size: 1420 Color: 14
Size: 832 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 7
Size: 2051 Color: 18
Size: 408 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13960 Color: 5
Size: 2008 Color: 9
Size: 448 Color: 8

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14193 Color: 4
Size: 1537 Color: 7
Size: 686 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14012 Color: 10
Size: 1380 Color: 8
Size: 1024 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14132 Color: 8
Size: 1184 Color: 15
Size: 1100 Color: 5

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 4
Size: 1796 Color: 12
Size: 426 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14241 Color: 16
Size: 1771 Color: 19
Size: 404 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 0
Size: 1364 Color: 6
Size: 784 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 4
Size: 1564 Color: 9
Size: 560 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 13
Size: 1908 Color: 12
Size: 32 Color: 14

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14500 Color: 15
Size: 1436 Color: 9
Size: 480 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14520 Color: 17
Size: 1360 Color: 4
Size: 536 Color: 11

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14548 Color: 3
Size: 1336 Color: 14
Size: 532 Color: 7

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 11
Size: 1482 Color: 15
Size: 322 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 6
Size: 1152 Color: 3
Size: 626 Color: 17

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14700 Color: 9
Size: 1358 Color: 10
Size: 358 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 14
Size: 988 Color: 1
Size: 752 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14704 Color: 6
Size: 1344 Color: 5
Size: 368 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 5
Size: 1192 Color: 9
Size: 508 Color: 12

Bin 54: 1 of cap free
Amount of items: 5
Items: 
Size: 8216 Color: 19
Size: 3334 Color: 10
Size: 2336 Color: 6
Size: 1793 Color: 7
Size: 736 Color: 16

Bin 55: 1 of cap free
Amount of items: 5
Items: 
Size: 9244 Color: 4
Size: 3856 Color: 0
Size: 2691 Color: 7
Size: 320 Color: 0
Size: 304 Color: 18

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 11404 Color: 9
Size: 5011 Color: 2

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12626 Color: 3
Size: 3021 Color: 12
Size: 768 Color: 8

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13189 Color: 3
Size: 2542 Color: 13
Size: 684 Color: 17

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13302 Color: 7
Size: 2361 Color: 14
Size: 752 Color: 14

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13905 Color: 4
Size: 1582 Color: 12
Size: 928 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 15
Size: 2342 Color: 6
Size: 152 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13925 Color: 6
Size: 2026 Color: 0
Size: 464 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 18
Size: 2077 Color: 6
Size: 352 Color: 17

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 14569 Color: 0
Size: 1488 Color: 4
Size: 358 Color: 15

Bin 65: 2 of cap free
Amount of items: 7
Items: 
Size: 8213 Color: 5
Size: 1592 Color: 0
Size: 1585 Color: 10
Size: 1561 Color: 19
Size: 1462 Color: 14
Size: 1401 Color: 8
Size: 600 Color: 14

Bin 66: 2 of cap free
Amount of items: 5
Items: 
Size: 8218 Color: 3
Size: 3468 Color: 17
Size: 3424 Color: 6
Size: 676 Color: 2
Size: 628 Color: 17

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 9378 Color: 13
Size: 6444 Color: 13
Size: 592 Color: 19

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 7
Size: 4792 Color: 9
Size: 160 Color: 8

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11802 Color: 15
Size: 3031 Color: 14
Size: 1581 Color: 15

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 12100 Color: 15
Size: 3770 Color: 10
Size: 544 Color: 14

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 9
Size: 3822 Color: 2

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 14
Size: 3386 Color: 12
Size: 288 Color: 8

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12842 Color: 12
Size: 3572 Color: 6

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13252 Color: 9
Size: 3162 Color: 2

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13366 Color: 17
Size: 3048 Color: 11

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13606 Color: 12
Size: 2528 Color: 16
Size: 280 Color: 13

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 13857 Color: 12
Size: 1517 Color: 15
Size: 1040 Color: 19

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 14394 Color: 13
Size: 1644 Color: 9
Size: 376 Color: 1

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 9455 Color: 19
Size: 6600 Color: 4
Size: 358 Color: 6

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 12791 Color: 5
Size: 3026 Color: 13
Size: 596 Color: 6

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 4
Size: 3213 Color: 2

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 4
Size: 2661 Color: 2
Size: 468 Color: 11

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 14257 Color: 0
Size: 1772 Color: 5
Size: 384 Color: 9

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 14289 Color: 13
Size: 1800 Color: 4
Size: 324 Color: 14

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 14573 Color: 18
Size: 1840 Color: 13

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 14640 Color: 8
Size: 1773 Color: 7

Bin 87: 4 of cap free
Amount of items: 5
Items: 
Size: 8226 Color: 11
Size: 6826 Color: 8
Size: 608 Color: 10
Size: 376 Color: 9
Size: 376 Color: 1

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 19
Size: 6096 Color: 0
Size: 362 Color: 8

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 10936 Color: 1
Size: 5164 Color: 8
Size: 312 Color: 9

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 12
Size: 4508 Color: 4
Size: 680 Color: 17

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 11572 Color: 11
Size: 4568 Color: 4
Size: 272 Color: 17

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 12
Size: 3764 Color: 14
Size: 736 Color: 16

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 12228 Color: 12
Size: 2387 Color: 18
Size: 1797 Color: 8

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 2
Size: 3480 Color: 19
Size: 320 Color: 10

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 12
Size: 3652 Color: 16

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 13604 Color: 4
Size: 1480 Color: 15
Size: 1328 Color: 17

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 7
Size: 2612 Color: 10

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 9
Size: 2520 Color: 6

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 14440 Color: 3
Size: 1920 Color: 4
Size: 52 Color: 7

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 8265 Color: 8
Size: 6834 Color: 9
Size: 1312 Color: 7

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 9439 Color: 2
Size: 6624 Color: 3
Size: 348 Color: 18

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 13
Size: 5815 Color: 8
Size: 300 Color: 4

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 11221 Color: 10
Size: 4790 Color: 18
Size: 400 Color: 19

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 6
Size: 3911 Color: 8
Size: 420 Color: 16

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 9104 Color: 15
Size: 7306 Color: 11

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 9314 Color: 17
Size: 6840 Color: 11
Size: 256 Color: 8

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 12190 Color: 10
Size: 3492 Color: 12
Size: 728 Color: 19

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 0
Size: 3012 Color: 11

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 14314 Color: 12
Size: 2096 Color: 15

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 14458 Color: 2
Size: 1824 Color: 11
Size: 128 Color: 12

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14597 Color: 12
Size: 1813 Color: 5

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 14609 Color: 9
Size: 1801 Color: 3

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 6
Size: 1744 Color: 1

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 13553 Color: 11
Size: 2728 Color: 4
Size: 128 Color: 7

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 11360 Color: 4
Size: 5048 Color: 9

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 12464 Color: 3
Size: 3944 Color: 0

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 1
Size: 3572 Color: 19

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 7
Size: 2992 Color: 9

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 13632 Color: 11
Size: 2776 Color: 5

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 14224 Color: 14
Size: 2184 Color: 16

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 0
Size: 2056 Color: 7

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 17
Size: 1824 Color: 1

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 9
Size: 6793 Color: 13
Size: 1366 Color: 3

Bin 124: 9 of cap free
Amount of items: 3
Items: 
Size: 10228 Color: 17
Size: 5765 Color: 11
Size: 414 Color: 7

Bin 125: 9 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 0
Size: 1853 Color: 11
Size: 32 Color: 5

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 11430 Color: 16
Size: 4976 Color: 14

Bin 127: 10 of cap free
Amount of items: 3
Items: 
Size: 11830 Color: 9
Size: 3468 Color: 16
Size: 1108 Color: 17

Bin 128: 10 of cap free
Amount of items: 3
Items: 
Size: 11879 Color: 13
Size: 3439 Color: 10
Size: 1088 Color: 9

Bin 129: 10 of cap free
Amount of items: 2
Items: 
Size: 12248 Color: 18
Size: 4158 Color: 12

Bin 130: 10 of cap free
Amount of items: 3
Items: 
Size: 12818 Color: 0
Size: 3524 Color: 3
Size: 64 Color: 18

Bin 131: 10 of cap free
Amount of items: 3
Items: 
Size: 14261 Color: 7
Size: 2081 Color: 10
Size: 64 Color: 12

Bin 132: 11 of cap free
Amount of items: 3
Items: 
Size: 9416 Color: 4
Size: 6557 Color: 19
Size: 432 Color: 13

Bin 133: 11 of cap free
Amount of items: 3
Items: 
Size: 10916 Color: 9
Size: 4969 Color: 13
Size: 520 Color: 8

Bin 134: 11 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 5
Size: 2093 Color: 15

Bin 135: 11 of cap free
Amount of items: 2
Items: 
Size: 14521 Color: 10
Size: 1884 Color: 0

Bin 136: 12 of cap free
Amount of items: 3
Items: 
Size: 12196 Color: 9
Size: 3472 Color: 0
Size: 736 Color: 13

Bin 137: 13 of cap free
Amount of items: 3
Items: 
Size: 11891 Color: 19
Size: 3344 Color: 15
Size: 1168 Color: 4

Bin 138: 13 of cap free
Amount of items: 2
Items: 
Size: 14545 Color: 5
Size: 1858 Color: 12

Bin 139: 14 of cap free
Amount of items: 3
Items: 
Size: 11504 Color: 7
Size: 4738 Color: 5
Size: 160 Color: 8

Bin 140: 14 of cap free
Amount of items: 2
Items: 
Size: 12272 Color: 3
Size: 4130 Color: 17

Bin 141: 14 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 7
Size: 1754 Color: 8

Bin 142: 14 of cap free
Amount of items: 3
Items: 
Size: 14764 Color: 18
Size: 1634 Color: 10
Size: 4 Color: 4

Bin 143: 15 of cap free
Amount of items: 3
Items: 
Size: 8393 Color: 4
Size: 7688 Color: 18
Size: 320 Color: 4

Bin 144: 15 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 2
Size: 2108 Color: 10

Bin 145: 16 of cap free
Amount of items: 7
Items: 
Size: 8212 Color: 4
Size: 1541 Color: 19
Size: 1528 Color: 5
Size: 1507 Color: 17
Size: 1452 Color: 7
Size: 1360 Color: 9
Size: 800 Color: 2

Bin 146: 16 of cap free
Amount of items: 3
Items: 
Size: 10100 Color: 1
Size: 5536 Color: 17
Size: 764 Color: 9

Bin 147: 16 of cap free
Amount of items: 3
Items: 
Size: 10448 Color: 3
Size: 5488 Color: 1
Size: 464 Color: 19

Bin 148: 17 of cap free
Amount of items: 3
Items: 
Size: 14473 Color: 0
Size: 1898 Color: 15
Size: 28 Color: 10

Bin 149: 18 of cap free
Amount of items: 2
Items: 
Size: 11012 Color: 10
Size: 5386 Color: 1

Bin 150: 18 of cap free
Amount of items: 2
Items: 
Size: 12354 Color: 6
Size: 4044 Color: 15

Bin 151: 18 of cap free
Amount of items: 2
Items: 
Size: 14142 Color: 15
Size: 2256 Color: 9

Bin 152: 18 of cap free
Amount of items: 2
Items: 
Size: 14265 Color: 14
Size: 2133 Color: 5

Bin 153: 19 of cap free
Amount of items: 12
Items: 
Size: 8209 Color: 1
Size: 912 Color: 19
Size: 900 Color: 7
Size: 896 Color: 16
Size: 896 Color: 4
Size: 872 Color: 1
Size: 864 Color: 12
Size: 864 Color: 4
Size: 664 Color: 18
Size: 616 Color: 2
Size: 352 Color: 11
Size: 352 Color: 10

Bin 154: 19 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 8
Size: 7161 Color: 17

Bin 155: 19 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 3
Size: 2814 Color: 17

Bin 156: 20 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 1
Size: 5436 Color: 12

Bin 157: 20 of cap free
Amount of items: 3
Items: 
Size: 12303 Color: 12
Size: 2472 Color: 4
Size: 1621 Color: 13

Bin 158: 21 of cap free
Amount of items: 2
Items: 
Size: 13223 Color: 17
Size: 3172 Color: 2

Bin 159: 22 of cap free
Amount of items: 3
Items: 
Size: 10680 Color: 17
Size: 5506 Color: 15
Size: 208 Color: 0

Bin 160: 22 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 1
Size: 2722 Color: 6

Bin 161: 23 of cap free
Amount of items: 2
Items: 
Size: 14737 Color: 8
Size: 1656 Color: 2

Bin 162: 24 of cap free
Amount of items: 2
Items: 
Size: 9483 Color: 7
Size: 6909 Color: 13

Bin 163: 25 of cap free
Amount of items: 2
Items: 
Size: 14537 Color: 19
Size: 1854 Color: 6

Bin 164: 26 of cap free
Amount of items: 3
Items: 
Size: 10455 Color: 8
Size: 4331 Color: 11
Size: 1604 Color: 13

Bin 165: 26 of cap free
Amount of items: 2
Items: 
Size: 10734 Color: 2
Size: 5656 Color: 8

Bin 166: 26 of cap free
Amount of items: 2
Items: 
Size: 12786 Color: 9
Size: 3604 Color: 16

Bin 167: 29 of cap free
Amount of items: 3
Items: 
Size: 13199 Color: 3
Size: 3124 Color: 10
Size: 64 Color: 4

Bin 168: 30 of cap free
Amount of items: 4
Items: 
Size: 8220 Color: 4
Size: 3846 Color: 0
Size: 3776 Color: 9
Size: 544 Color: 12

Bin 169: 30 of cap free
Amount of items: 3
Items: 
Size: 11894 Color: 11
Size: 4328 Color: 8
Size: 164 Color: 3

Bin 170: 31 of cap free
Amount of items: 2
Items: 
Size: 10405 Color: 3
Size: 5980 Color: 2

Bin 171: 32 of cap free
Amount of items: 3
Items: 
Size: 10812 Color: 11
Size: 5268 Color: 16
Size: 304 Color: 19

Bin 172: 32 of cap free
Amount of items: 2
Items: 
Size: 11824 Color: 13
Size: 4560 Color: 14

Bin 173: 33 of cap free
Amount of items: 4
Items: 
Size: 8442 Color: 7
Size: 6837 Color: 16
Size: 632 Color: 10
Size: 472 Color: 17

Bin 174: 34 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 17
Size: 3286 Color: 12

Bin 175: 38 of cap free
Amount of items: 2
Items: 
Size: 14176 Color: 9
Size: 2202 Color: 19

Bin 176: 40 of cap free
Amount of items: 2
Items: 
Size: 11170 Color: 7
Size: 5206 Color: 11

Bin 177: 44 of cap free
Amount of items: 2
Items: 
Size: 12164 Color: 0
Size: 4208 Color: 13

Bin 178: 44 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 9
Size: 4112 Color: 5

Bin 179: 46 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 11
Size: 2106 Color: 0

Bin 180: 48 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 18
Size: 3100 Color: 17
Size: 464 Color: 9

Bin 181: 52 of cap free
Amount of items: 2
Items: 
Size: 11688 Color: 9
Size: 4676 Color: 0

Bin 182: 66 of cap free
Amount of items: 2
Items: 
Size: 9663 Color: 19
Size: 6687 Color: 13

Bin 183: 66 of cap free
Amount of items: 3
Items: 
Size: 10670 Color: 6
Size: 3082 Color: 7
Size: 2598 Color: 15

Bin 184: 69 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 1
Size: 3929 Color: 7

Bin 185: 99 of cap free
Amount of items: 2
Items: 
Size: 12781 Color: 15
Size: 3536 Color: 0

Bin 186: 100 of cap free
Amount of items: 3
Items: 
Size: 10170 Color: 2
Size: 5866 Color: 5
Size: 280 Color: 19

Bin 187: 104 of cap free
Amount of items: 2
Items: 
Size: 12132 Color: 6
Size: 4180 Color: 19

Bin 188: 122 of cap free
Amount of items: 2
Items: 
Size: 12772 Color: 11
Size: 3522 Color: 14

Bin 189: 126 of cap free
Amount of items: 30
Items: 
Size: 828 Color: 9
Size: 824 Color: 15
Size: 800 Color: 1
Size: 712 Color: 3
Size: 688 Color: 8
Size: 672 Color: 0
Size: 616 Color: 18
Size: 608 Color: 16
Size: 608 Color: 6
Size: 604 Color: 19
Size: 604 Color: 17
Size: 604 Color: 7
Size: 596 Color: 7
Size: 592 Color: 19
Size: 576 Color: 6
Size: 544 Color: 13
Size: 544 Color: 8
Size: 516 Color: 13
Size: 512 Color: 18
Size: 512 Color: 5
Size: 480 Color: 2
Size: 416 Color: 15
Size: 416 Color: 11
Size: 416 Color: 9
Size: 414 Color: 7
Size: 354 Color: 2
Size: 320 Color: 10
Size: 320 Color: 10
Size: 306 Color: 14
Size: 288 Color: 17

Bin 190: 128 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 7
Size: 5848 Color: 9
Size: 600 Color: 4

Bin 191: 138 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 11
Size: 6602 Color: 3
Size: 368 Color: 0

Bin 192: 142 of cap free
Amount of items: 2
Items: 
Size: 11900 Color: 1
Size: 4374 Color: 13

Bin 193: 160 of cap free
Amount of items: 2
Items: 
Size: 10360 Color: 0
Size: 5896 Color: 14

Bin 194: 176 of cap free
Amount of items: 2
Items: 
Size: 9594 Color: 13
Size: 6646 Color: 2

Bin 195: 188 of cap free
Amount of items: 3
Items: 
Size: 9810 Color: 3
Size: 5714 Color: 0
Size: 704 Color: 13

Bin 196: 188 of cap free
Amount of items: 2
Items: 
Size: 10304 Color: 8
Size: 5924 Color: 17

Bin 197: 198 of cap free
Amount of items: 9
Items: 
Size: 8210 Color: 15
Size: 1172 Color: 16
Size: 1008 Color: 18
Size: 1000 Color: 4
Size: 992 Color: 14
Size: 992 Color: 12
Size: 956 Color: 13
Size: 944 Color: 14
Size: 944 Color: 0

Bin 198: 220 of cap free
Amount of items: 2
Items: 
Size: 9352 Color: 14
Size: 6844 Color: 18

Bin 199: 12594 of cap free
Amount of items: 12
Items: 
Size: 400 Color: 16
Size: 384 Color: 5
Size: 352 Color: 0
Size: 312 Color: 1
Size: 310 Color: 11
Size: 306 Color: 12
Size: 304 Color: 10
Size: 302 Color: 15
Size: 288 Color: 18
Size: 288 Color: 17
Size: 288 Color: 14
Size: 288 Color: 2

Total size: 3250368
Total free space: 16416

