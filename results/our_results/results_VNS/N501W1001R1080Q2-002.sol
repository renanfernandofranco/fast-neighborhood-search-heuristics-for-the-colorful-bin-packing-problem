Capicity Bin: 1001
Lower Bound: 235

Bins used: 240
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 1

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 0
Size: 467 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 353 Color: 1
Size: 153 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 355 Color: 1
Size: 147 Color: 0

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 0

Bin 6: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 1
Size: 499 Color: 0

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 0
Size: 449 Color: 1

Bin 8: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 439 Color: 1

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 435 Color: 1

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 0

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 0
Size: 430 Color: 1

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 462 Color: 0

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 0
Size: 428 Color: 1

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 1
Size: 427 Color: 0

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 0
Size: 391 Color: 1

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 1
Size: 413 Color: 0

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 0
Size: 383 Color: 1

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 382 Color: 1

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 0
Size: 379 Color: 1

Bin 20: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 362 Color: 1

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 0
Size: 361 Color: 1

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 1
Size: 372 Color: 0

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 0

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 367 Color: 0

Bin 25: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 1
Size: 355 Color: 0

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 353 Color: 0

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 0
Size: 342 Color: 1

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 339 Color: 1

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 339 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 0
Size: 161 Color: 1
Size: 159 Color: 1

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 0
Size: 320 Color: 1

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 0

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 0
Size: 317 Color: 1

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 318 Color: 0

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 1
Size: 308 Color: 0

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 304 Color: 0

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 303 Color: 1

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 716 Color: 0
Size: 285 Color: 1

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 1
Size: 280 Color: 0

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 1
Size: 278 Color: 0

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 1

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 741 Color: 1
Size: 132 Color: 1
Size: 128 Color: 0

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 1
Size: 254 Color: 0

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 753 Color: 0
Size: 142 Color: 1
Size: 106 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 755 Color: 1
Size: 133 Color: 1
Size: 113 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 0
Size: 126 Color: 1
Size: 119 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 756 Color: 1
Size: 131 Color: 1
Size: 114 Color: 0

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 1
Size: 240 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 0
Size: 236 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 0
Size: 125 Color: 1
Size: 108 Color: 0

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 0
Size: 227 Color: 1

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 226 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 1
Size: 210 Color: 0

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 209 Color: 0

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 0
Size: 208 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 208 Color: 0

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 1
Size: 207 Color: 0

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 481 Color: 1

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 0
Size: 460 Color: 1

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 443 Color: 1

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 428 Color: 1

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 1
Size: 453 Color: 0

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 421 Color: 1

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 0
Size: 404 Color: 1

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 1
Size: 429 Color: 0

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 401 Color: 1

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 427 Color: 0

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 0
Size: 395 Color: 1

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 410 Color: 0

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 0
Size: 383 Color: 1

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 381 Color: 1

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 0
Size: 368 Color: 1

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 1
Size: 352 Color: 0

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 323 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 0
Size: 163 Color: 1
Size: 159 Color: 0

Bin 78: 1 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 0
Size: 317 Color: 1

Bin 79: 1 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 302 Color: 0

Bin 80: 1 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 296 Color: 1

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 714 Color: 1
Size: 149 Color: 1
Size: 137 Color: 0

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 279 Color: 1

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 271 Color: 0

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 239 Color: 1

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 771 Color: 0
Size: 128 Color: 1
Size: 101 Color: 1

Bin 86: 1 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 0
Size: 228 Color: 1

Bin 87: 1 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 1
Size: 220 Color: 0

Bin 88: 1 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 1
Size: 210 Color: 0

Bin 89: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 1
Size: 199 Color: 0

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 0
Size: 361 Color: 1
Size: 164 Color: 0

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 362 Color: 1
Size: 148 Color: 0

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 358 Color: 1
Size: 148 Color: 0

Bin 93: 2 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 466 Color: 1

Bin 94: 2 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 463 Color: 1

Bin 95: 2 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 1
Size: 482 Color: 0

Bin 96: 2 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 0
Size: 439 Color: 1

Bin 97: 2 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 467 Color: 0

Bin 98: 2 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 1
Size: 446 Color: 0

Bin 99: 2 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 1
Size: 416 Color: 0

Bin 100: 2 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 412 Color: 0

Bin 101: 2 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 1
Size: 395 Color: 0

Bin 102: 2 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 1
Size: 390 Color: 0

Bin 103: 2 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 385 Color: 0

Bin 104: 2 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 380 Color: 0

Bin 105: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 1
Size: 358 Color: 0

Bin 106: 2 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 342 Color: 1

Bin 107: 2 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 1
Size: 336 Color: 0

Bin 108: 2 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 332 Color: 0

Bin 109: 2 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 1
Size: 307 Color: 0

Bin 110: 2 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 1
Size: 299 Color: 0

Bin 111: 2 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 0
Size: 286 Color: 1

Bin 112: 2 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 279 Color: 1

Bin 113: 2 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 279 Color: 1

Bin 114: 2 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 1
Size: 268 Color: 0

Bin 115: 2 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 255 Color: 1

Bin 116: 2 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 0
Size: 115 Color: 1
Size: 101 Color: 0

Bin 117: 2 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 1
Size: 213 Color: 0

Bin 118: 3 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 395 Color: 1
Size: 172 Color: 0

Bin 119: 3 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 479 Color: 1

Bin 120: 3 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 0
Size: 455 Color: 1

Bin 121: 3 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 435 Color: 1

Bin 122: 3 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 466 Color: 0

Bin 123: 3 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 0
Size: 410 Color: 1

Bin 124: 3 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 1
Size: 432 Color: 0

Bin 125: 3 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 1
Size: 165 Color: 0
Size: 161 Color: 1

Bin 126: 3 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 325 Color: 0

Bin 127: 3 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 1
Size: 315 Color: 0

Bin 128: 3 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 308 Color: 1

Bin 129: 3 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 1
Size: 306 Color: 0

Bin 130: 3 of cap free
Amount of items: 3
Items: 
Size: 747 Color: 0
Size: 128 Color: 0
Size: 123 Color: 1

Bin 131: 3 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 247 Color: 1

Bin 132: 3 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 227 Color: 0

Bin 133: 4 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 0
Size: 434 Color: 1

Bin 134: 4 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 1
Size: 388 Color: 0

Bin 135: 4 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 1
Size: 357 Color: 0

Bin 136: 4 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 0
Size: 325 Color: 1

Bin 137: 4 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 0
Size: 141 Color: 1
Size: 139 Color: 0

Bin 138: 4 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 1
Size: 277 Color: 0

Bin 139: 4 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 0
Size: 277 Color: 1

Bin 140: 4 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 1
Size: 271 Color: 0

Bin 141: 5 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 470 Color: 1

Bin 142: 5 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 1
Size: 451 Color: 0

Bin 143: 5 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 1
Size: 426 Color: 0

Bin 144: 5 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 409 Color: 0

Bin 145: 5 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 1
Size: 400 Color: 0

Bin 146: 5 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 0
Size: 174 Color: 0
Size: 148 Color: 1

Bin 147: 5 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 1
Size: 319 Color: 0

Bin 148: 5 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 1
Size: 290 Color: 0

Bin 149: 5 of cap free
Amount of items: 3
Items: 
Size: 754 Color: 1
Size: 142 Color: 1
Size: 100 Color: 0

Bin 150: 5 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 1
Size: 218 Color: 0

Bin 151: 5 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 217 Color: 1

Bin 152: 5 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 205 Color: 1

Bin 153: 6 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 477 Color: 1

Bin 154: 6 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 479 Color: 0

Bin 155: 6 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 380 Color: 1

Bin 156: 6 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 376 Color: 0

Bin 157: 6 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 0
Size: 324 Color: 1

Bin 158: 6 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 0
Size: 244 Color: 1

Bin 159: 6 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 0
Size: 204 Color: 1

Bin 160: 7 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 1
Size: 466 Color: 0

Bin 161: 7 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 1
Size: 416 Color: 0

Bin 162: 7 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 1
Size: 385 Color: 0

Bin 163: 7 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 364 Color: 1

Bin 164: 7 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 375 Color: 0

Bin 165: 7 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 357 Color: 0

Bin 166: 7 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 275 Color: 1

Bin 167: 7 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 234 Color: 1

Bin 168: 8 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 0
Size: 487 Color: 1

Bin 169: 8 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 488 Color: 0

Bin 170: 8 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 304 Color: 0

Bin 171: 8 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 244 Color: 1

Bin 172: 8 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 1
Size: 236 Color: 0

Bin 173: 8 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 0
Size: 104 Color: 1
Size: 103 Color: 1

Bin 174: 9 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 445 Color: 1

Bin 175: 9 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 384 Color: 0

Bin 176: 9 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 356 Color: 0

Bin 177: 9 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 331 Color: 0

Bin 178: 9 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 323 Color: 1

Bin 179: 9 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 1
Size: 167 Color: 1
Size: 154 Color: 0

Bin 180: 9 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 274 Color: 1

Bin 181: 10 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 1
Size: 477 Color: 0

Bin 182: 10 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 319 Color: 0

Bin 183: 10 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 295 Color: 1

Bin 184: 10 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 273 Color: 1

Bin 185: 10 of cap free
Amount of items: 2
Items: 
Size: 760 Color: 0
Size: 231 Color: 1

Bin 186: 10 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 216 Color: 1

Bin 187: 10 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 0
Size: 201 Color: 1

Bin 188: 11 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 0
Size: 487 Color: 1

Bin 189: 11 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 1
Size: 426 Color: 0

Bin 190: 11 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 377 Color: 1

Bin 191: 11 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 271 Color: 0

Bin 192: 12 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 432 Color: 1

Bin 193: 12 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 242 Color: 1

Bin 194: 12 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 212 Color: 0

Bin 195: 13 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 376 Color: 1

Bin 196: 13 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 270 Color: 1

Bin 197: 14 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 235 Color: 0

Bin 198: 15 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 1
Size: 209 Color: 0

Bin 199: 15 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 197 Color: 1

Bin 200: 16 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 354 Color: 0

Bin 201: 17 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 0
Size: 484 Color: 1

Bin 202: 20 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 463 Color: 1

Bin 203: 20 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 1
Size: 263 Color: 0

Bin 204: 21 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 263 Color: 0

Bin 205: 23 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 383 Color: 0

Bin 206: 23 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 1
Size: 226 Color: 0

Bin 207: 25 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 1
Size: 226 Color: 0

Bin 208: 26 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 0
Size: 363 Color: 1

Bin 209: 26 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 231 Color: 1

Bin 210: 30 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 1
Size: 415 Color: 0

Bin 211: 32 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 374 Color: 0

Bin 212: 33 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 1
Size: 297 Color: 0

Bin 213: 35 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 373 Color: 0

Bin 214: 45 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 0
Size: 293 Color: 1

Bin 215: 45 of cap free
Amount of items: 2
Items: 
Size: 663 Color: 0
Size: 293 Color: 1

Bin 216: 49 of cap free
Amount of items: 2
Items: 
Size: 497 Color: 0
Size: 455 Color: 1

Bin 217: 54 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 202 Color: 0

Bin 218: 60 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 197 Color: 0

Bin 219: 66 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 192 Color: 1

Bin 220: 70 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 191 Color: 1

Bin 221: 73 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 185 Color: 0

Bin 222: 78 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 186 Color: 1

Bin 223: 81 of cap free
Amount of items: 2
Items: 
Size: 475 Color: 0
Size: 445 Color: 1

Bin 224: 84 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 1
Size: 176 Color: 0

Bin 225: 90 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 174 Color: 1

Bin 226: 91 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 173 Color: 1

Bin 227: 101 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 167 Color: 1

Bin 228: 112 of cap free
Amount of items: 2
Items: 
Size: 464 Color: 0
Size: 425 Color: 1

Bin 229: 118 of cap free
Amount of items: 2
Items: 
Size: 459 Color: 0
Size: 424 Color: 1

Bin 230: 123 of cap free
Amount of items: 2
Items: 
Size: 458 Color: 0
Size: 420 Color: 1

Bin 231: 133 of cap free
Amount of items: 2
Items: 
Size: 449 Color: 0
Size: 419 Color: 1

Bin 232: 184 of cap free
Amount of items: 2
Items: 
Size: 409 Color: 0
Size: 408 Color: 1

Bin 233: 230 of cap free
Amount of items: 2
Items: 
Size: 400 Color: 1
Size: 371 Color: 0

Bin 234: 253 of cap free
Amount of items: 2
Items: 
Size: 399 Color: 1
Size: 349 Color: 0

Bin 235: 261 of cap free
Amount of items: 2
Items: 
Size: 393 Color: 1
Size: 347 Color: 0

Bin 236: 264 of cap free
Amount of items: 2
Items: 
Size: 393 Color: 1
Size: 344 Color: 0

Bin 237: 350 of cap free
Amount of items: 1
Items: 
Size: 651 Color: 0

Bin 238: 350 of cap free
Amount of items: 1
Items: 
Size: 651 Color: 0

Bin 239: 352 of cap free
Amount of items: 1
Items: 
Size: 649 Color: 0

Bin 240: 378 of cap free
Amount of items: 2
Items: 
Size: 361 Color: 1
Size: 262 Color: 0

Total size: 235146
Total free space: 5094

