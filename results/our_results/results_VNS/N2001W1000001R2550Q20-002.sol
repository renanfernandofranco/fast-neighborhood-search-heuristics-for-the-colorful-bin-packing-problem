Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 355866 Color: 4
Size: 338725 Color: 15
Size: 305410 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 357802 Color: 11
Size: 344428 Color: 10
Size: 297771 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 360374 Color: 0
Size: 348576 Color: 6
Size: 291051 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 366362 Color: 7
Size: 350617 Color: 6
Size: 283022 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 369329 Color: 9
Size: 316014 Color: 0
Size: 314658 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 370805 Color: 7
Size: 341626 Color: 9
Size: 287570 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 371099 Color: 1
Size: 339512 Color: 18
Size: 289390 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 371255 Color: 16
Size: 347379 Color: 12
Size: 281367 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 371830 Color: 13
Size: 346148 Color: 16
Size: 282023 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 371837 Color: 7
Size: 356988 Color: 16
Size: 271176 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 371880 Color: 11
Size: 367675 Color: 13
Size: 260446 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 372675 Color: 19
Size: 321701 Color: 14
Size: 305625 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 373340 Color: 2
Size: 317234 Color: 0
Size: 309427 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 373455 Color: 0
Size: 349082 Color: 14
Size: 277464 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 373621 Color: 19
Size: 342047 Color: 8
Size: 284333 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 373889 Color: 3
Size: 369403 Color: 11
Size: 256709 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 374110 Color: 19
Size: 360705 Color: 3
Size: 265186 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 374551 Color: 14
Size: 345251 Color: 0
Size: 280199 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 375185 Color: 18
Size: 341776 Color: 3
Size: 283040 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 376251 Color: 17
Size: 350568 Color: 11
Size: 273182 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 376535 Color: 8
Size: 318540 Color: 0
Size: 304926 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 376741 Color: 16
Size: 339080 Color: 16
Size: 284180 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 377214 Color: 0
Size: 333036 Color: 10
Size: 289751 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 377982 Color: 15
Size: 360531 Color: 5
Size: 261488 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 379225 Color: 11
Size: 323876 Color: 7
Size: 296900 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 379359 Color: 10
Size: 370415 Color: 15
Size: 250227 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 379425 Color: 0
Size: 365703 Color: 7
Size: 254873 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 380343 Color: 1
Size: 316771 Color: 2
Size: 302887 Color: 9

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 380357 Color: 14
Size: 350084 Color: 11
Size: 269560 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 380396 Color: 2
Size: 316969 Color: 19
Size: 302636 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 381213 Color: 2
Size: 313897 Color: 18
Size: 304891 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 380466 Color: 10
Size: 351573 Color: 11
Size: 267962 Color: 7

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 381611 Color: 6
Size: 335709 Color: 18
Size: 282681 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 381727 Color: 15
Size: 329080 Color: 9
Size: 289194 Color: 16

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 381773 Color: 18
Size: 318379 Color: 13
Size: 299849 Color: 17

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 381833 Color: 5
Size: 353332 Color: 4
Size: 264836 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 382046 Color: 18
Size: 320223 Color: 10
Size: 297732 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 382259 Color: 4
Size: 324205 Color: 13
Size: 293537 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 382302 Color: 18
Size: 341489 Color: 10
Size: 276210 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 382407 Color: 15
Size: 360391 Color: 16
Size: 257203 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 382674 Color: 3
Size: 327969 Color: 18
Size: 289358 Color: 18

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 383587 Color: 16
Size: 354381 Color: 7
Size: 262033 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 383746 Color: 8
Size: 350814 Color: 5
Size: 265441 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 384906 Color: 6
Size: 362480 Color: 5
Size: 252615 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 384933 Color: 14
Size: 312783 Color: 19
Size: 302285 Color: 13

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 385126 Color: 17
Size: 351131 Color: 9
Size: 263744 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 385379 Color: 4
Size: 333983 Color: 11
Size: 280639 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 385878 Color: 0
Size: 351775 Color: 12
Size: 262348 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 386335 Color: 4
Size: 325385 Color: 17
Size: 288281 Color: 9

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 386338 Color: 1
Size: 361560 Color: 7
Size: 252103 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 16
Size: 332056 Color: 6
Size: 281118 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 386935 Color: 6
Size: 352571 Color: 14
Size: 260495 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 387184 Color: 1
Size: 313874 Color: 19
Size: 298943 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 387984 Color: 3
Size: 356011 Color: 8
Size: 256006 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 388046 Color: 19
Size: 315541 Color: 6
Size: 296414 Color: 16

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 388278 Color: 17
Size: 352986 Color: 15
Size: 258737 Color: 6

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 388884 Color: 3
Size: 351206 Color: 12
Size: 259911 Color: 5

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 389078 Color: 17
Size: 319719 Color: 0
Size: 291204 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 389185 Color: 2
Size: 305805 Color: 11
Size: 305011 Color: 6

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 389704 Color: 15
Size: 349318 Color: 2
Size: 260979 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 390044 Color: 10
Size: 359756 Color: 2
Size: 250201 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 389894 Color: 12
Size: 317007 Color: 2
Size: 293100 Color: 5

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 390036 Color: 3
Size: 334284 Color: 14
Size: 275681 Color: 8

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 390062 Color: 12
Size: 344787 Color: 17
Size: 265152 Color: 16

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 390114 Color: 17
Size: 311597 Color: 8
Size: 298290 Color: 8

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 390439 Color: 1
Size: 344735 Color: 18
Size: 264827 Color: 9

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 390592 Color: 7
Size: 347911 Color: 16
Size: 261498 Color: 7

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 390953 Color: 11
Size: 339372 Color: 7
Size: 269676 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 391259 Color: 8
Size: 355644 Color: 18
Size: 253098 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 391709 Color: 15
Size: 306959 Color: 19
Size: 301333 Color: 10

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 392289 Color: 17
Size: 338142 Color: 18
Size: 269570 Color: 5

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 392817 Color: 6
Size: 318200 Color: 19
Size: 288984 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 393553 Color: 17
Size: 329744 Color: 0
Size: 276704 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 393827 Color: 1
Size: 326633 Color: 17
Size: 279541 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 394557 Color: 7
Size: 309695 Color: 17
Size: 295749 Color: 16

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 394910 Color: 18
Size: 322362 Color: 3
Size: 282729 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 394965 Color: 14
Size: 316973 Color: 4
Size: 288063 Color: 13

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 395275 Color: 6
Size: 348506 Color: 18
Size: 256220 Color: 10

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 395289 Color: 16
Size: 303567 Color: 17
Size: 301145 Color: 2

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 395353 Color: 1
Size: 338676 Color: 13
Size: 265972 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 395865 Color: 9
Size: 340375 Color: 15
Size: 263761 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 395987 Color: 6
Size: 346943 Color: 19
Size: 257071 Color: 17

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 396234 Color: 12
Size: 316080 Color: 1
Size: 287687 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 396359 Color: 8
Size: 334445 Color: 7
Size: 269197 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 396528 Color: 7
Size: 320769 Color: 14
Size: 282704 Color: 12

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 396976 Color: 9
Size: 340034 Color: 14
Size: 262991 Color: 3

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 397210 Color: 7
Size: 339979 Color: 7
Size: 262812 Color: 12

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 398520 Color: 0
Size: 331855 Color: 3
Size: 269626 Color: 9

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 399056 Color: 12
Size: 325702 Color: 11
Size: 275243 Color: 19

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 399255 Color: 2
Size: 325228 Color: 0
Size: 275518 Color: 6

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 399830 Color: 16
Size: 310488 Color: 15
Size: 289683 Color: 6

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 400501 Color: 2
Size: 304123 Color: 17
Size: 295377 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 400831 Color: 14
Size: 338458 Color: 18
Size: 260712 Color: 11

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 401169 Color: 1
Size: 325625 Color: 16
Size: 273207 Color: 5

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 401334 Color: 12
Size: 348544 Color: 17
Size: 250123 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 401605 Color: 14
Size: 300005 Color: 18
Size: 298391 Color: 11

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 401862 Color: 12
Size: 310797 Color: 9
Size: 287342 Color: 8

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 402305 Color: 3
Size: 342391 Color: 11
Size: 255305 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 402476 Color: 3
Size: 327451 Color: 7
Size: 270074 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 402558 Color: 1
Size: 300869 Color: 18
Size: 296574 Color: 9

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 404048 Color: 10
Size: 310948 Color: 17
Size: 285005 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 402622 Color: 0
Size: 299835 Color: 13
Size: 297544 Color: 19

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 404323 Color: 8
Size: 315019 Color: 13
Size: 280659 Color: 9

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 405118 Color: 7
Size: 340754 Color: 11
Size: 254129 Color: 11

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 405226 Color: 12
Size: 326301 Color: 15
Size: 268474 Color: 10

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 405311 Color: 14
Size: 316578 Color: 9
Size: 278112 Color: 15

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 405720 Color: 5
Size: 319815 Color: 0
Size: 274466 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 406010 Color: 0
Size: 315780 Color: 17
Size: 278211 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 406122 Color: 18
Size: 324817 Color: 12
Size: 269062 Color: 12

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 406796 Color: 5
Size: 338949 Color: 3
Size: 254256 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 407283 Color: 12
Size: 297429 Color: 9
Size: 295289 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 407313 Color: 4
Size: 312105 Color: 7
Size: 280583 Color: 14

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 407690 Color: 16
Size: 334479 Color: 5
Size: 257832 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 408089 Color: 10
Size: 297251 Color: 1
Size: 294661 Color: 16

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 408715 Color: 13
Size: 331894 Color: 17
Size: 259392 Color: 17

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 408809 Color: 17
Size: 332485 Color: 17
Size: 258707 Color: 13

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 408986 Color: 11
Size: 331336 Color: 6
Size: 259679 Color: 5

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 409432 Color: 16
Size: 309864 Color: 15
Size: 280705 Color: 15

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 409610 Color: 10
Size: 327689 Color: 8
Size: 262702 Color: 15

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 409632 Color: 3
Size: 333746 Color: 11
Size: 256623 Color: 6

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 409688 Color: 5
Size: 309745 Color: 18
Size: 280568 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 409732 Color: 8
Size: 305025 Color: 17
Size: 285244 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 409974 Color: 18
Size: 317035 Color: 9
Size: 272992 Color: 8

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 410174 Color: 14
Size: 308454 Color: 7
Size: 281373 Color: 16

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 412182 Color: 10
Size: 334800 Color: 15
Size: 253019 Color: 8

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 411171 Color: 6
Size: 314041 Color: 11
Size: 274789 Color: 17

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 411976 Color: 19
Size: 309061 Color: 18
Size: 278964 Color: 17

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 412769 Color: 10
Size: 334152 Color: 1
Size: 253080 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 412966 Color: 6
Size: 320335 Color: 10
Size: 266700 Color: 16

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 413052 Color: 2
Size: 333281 Color: 14
Size: 253668 Color: 19

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 413141 Color: 9
Size: 332731 Color: 11
Size: 254129 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 414085 Color: 13
Size: 304902 Color: 4
Size: 281014 Color: 16

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 414783 Color: 18
Size: 301210 Color: 1
Size: 284008 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 414976 Color: 1
Size: 303366 Color: 16
Size: 281659 Color: 6

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 415164 Color: 3
Size: 319715 Color: 18
Size: 265122 Color: 11

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 415428 Color: 15
Size: 308935 Color: 14
Size: 275638 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 415643 Color: 13
Size: 319695 Color: 1
Size: 264663 Color: 17

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 415878 Color: 12
Size: 327872 Color: 16
Size: 256251 Color: 19

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 415936 Color: 9
Size: 322083 Color: 9
Size: 261982 Color: 14

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 415945 Color: 6
Size: 325156 Color: 2
Size: 258900 Color: 7

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 416197 Color: 17
Size: 319269 Color: 18
Size: 264535 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 416425 Color: 2
Size: 322423 Color: 12
Size: 261153 Color: 11

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 416699 Color: 18
Size: 321320 Color: 2
Size: 261982 Color: 19

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 416753 Color: 14
Size: 303933 Color: 11
Size: 279315 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 416777 Color: 9
Size: 316801 Color: 15
Size: 266423 Color: 13

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 416944 Color: 19
Size: 309236 Color: 18
Size: 273821 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 417181 Color: 1
Size: 311021 Color: 14
Size: 271799 Color: 19

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 417272 Color: 16
Size: 310259 Color: 9
Size: 272470 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 417341 Color: 0
Size: 313827 Color: 7
Size: 268833 Color: 8

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 417652 Color: 12
Size: 312643 Color: 1
Size: 269706 Color: 10

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 417725 Color: 9
Size: 304019 Color: 3
Size: 278257 Color: 13

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 418535 Color: 13
Size: 314918 Color: 11
Size: 266548 Color: 12

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 418683 Color: 17
Size: 324022 Color: 4
Size: 257296 Color: 15

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 419154 Color: 9
Size: 328224 Color: 7
Size: 252623 Color: 13

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 419292 Color: 19
Size: 306921 Color: 15
Size: 273788 Color: 2

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 419399 Color: 14
Size: 308264 Color: 13
Size: 272338 Color: 11

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 419464 Color: 13
Size: 298902 Color: 11
Size: 281635 Color: 16

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 420122 Color: 12
Size: 327273 Color: 4
Size: 252606 Color: 12

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 421098 Color: 10
Size: 327794 Color: 18
Size: 251109 Color: 15

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 420422 Color: 12
Size: 323238 Color: 0
Size: 256341 Color: 16

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 420696 Color: 0
Size: 319091 Color: 16
Size: 260214 Color: 8

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 420725 Color: 12
Size: 292704 Color: 1
Size: 286572 Color: 9

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 421551 Color: 0
Size: 318207 Color: 18
Size: 260243 Color: 12

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 421617 Color: 0
Size: 292964 Color: 12
Size: 285420 Color: 16

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 421914 Color: 6
Size: 302193 Color: 14
Size: 275894 Color: 15

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 422041 Color: 1
Size: 320539 Color: 18
Size: 257421 Color: 18

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 422372 Color: 16
Size: 299969 Color: 11
Size: 277660 Color: 8

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 422393 Color: 8
Size: 308183 Color: 11
Size: 269425 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 422603 Color: 4
Size: 322287 Color: 0
Size: 255111 Color: 17

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 423387 Color: 4
Size: 307565 Color: 0
Size: 269049 Color: 5

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 423742 Color: 11
Size: 324512 Color: 8
Size: 251747 Color: 13

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 423829 Color: 5
Size: 304217 Color: 16
Size: 271955 Color: 19

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 424452 Color: 3
Size: 323239 Color: 2
Size: 252310 Color: 15

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 424787 Color: 5
Size: 306732 Color: 13
Size: 268482 Color: 14

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 425089 Color: 0
Size: 302285 Color: 1
Size: 272627 Color: 17

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 425310 Color: 12
Size: 310573 Color: 16
Size: 264118 Color: 15

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 425333 Color: 19
Size: 322808 Color: 1
Size: 251860 Color: 7

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 425339 Color: 2
Size: 294115 Color: 6
Size: 280547 Color: 16

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 425382 Color: 3
Size: 315287 Color: 14
Size: 259332 Color: 9

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 425614 Color: 9
Size: 304897 Color: 17
Size: 269490 Color: 16

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 426721 Color: 10
Size: 313610 Color: 17
Size: 259670 Color: 6

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 425962 Color: 18
Size: 301100 Color: 11
Size: 272939 Color: 18

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 426762 Color: 10
Size: 308202 Color: 17
Size: 265037 Color: 7

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 426851 Color: 19
Size: 299811 Color: 2
Size: 273339 Color: 6

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 426966 Color: 1
Size: 294306 Color: 0
Size: 278729 Color: 11

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 427022 Color: 4
Size: 320744 Color: 9
Size: 252235 Color: 12

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 427055 Color: 6
Size: 322210 Color: 13
Size: 250736 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 427194 Color: 7
Size: 308125 Color: 9
Size: 264682 Color: 7

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 427541 Color: 0
Size: 314648 Color: 4
Size: 257812 Color: 12

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 427607 Color: 9
Size: 290941 Color: 12
Size: 281453 Color: 12

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 428037 Color: 12
Size: 311270 Color: 8
Size: 260694 Color: 11

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 428144 Color: 12
Size: 292885 Color: 2
Size: 278972 Color: 18

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 428585 Color: 10
Size: 303018 Color: 12
Size: 268398 Color: 11

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 428381 Color: 4
Size: 288579 Color: 14
Size: 283041 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 428515 Color: 3
Size: 305497 Color: 17
Size: 265989 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 428860 Color: 14
Size: 320413 Color: 0
Size: 250728 Color: 12

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 430230 Color: 13
Size: 289305 Color: 4
Size: 280466 Color: 16

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 430255 Color: 6
Size: 298914 Color: 7
Size: 270832 Color: 5

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 431088 Color: 16
Size: 309782 Color: 5
Size: 259131 Color: 13

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 431415 Color: 4
Size: 296116 Color: 13
Size: 272470 Color: 9

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 431860 Color: 10
Size: 303407 Color: 17
Size: 264734 Color: 18

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 431927 Color: 1
Size: 298258 Color: 13
Size: 269816 Color: 5

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 432019 Color: 13
Size: 286732 Color: 6
Size: 281250 Color: 17

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 432420 Color: 1
Size: 289815 Color: 4
Size: 277766 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 433372 Color: 11
Size: 315165 Color: 13
Size: 251464 Color: 14

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 433548 Color: 2
Size: 288817 Color: 18
Size: 277636 Color: 6

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 434835 Color: 10
Size: 288450 Color: 2
Size: 276716 Color: 8

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 433575 Color: 12
Size: 283374 Color: 17
Size: 283052 Color: 14

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 433742 Color: 12
Size: 299812 Color: 11
Size: 266447 Color: 13

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 435071 Color: 10
Size: 309223 Color: 19
Size: 255707 Color: 17

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 434647 Color: 19
Size: 282845 Color: 1
Size: 282509 Color: 17

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 434661 Color: 15
Size: 287586 Color: 13
Size: 277754 Color: 17

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 434872 Color: 16
Size: 291440 Color: 0
Size: 273689 Color: 19

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 435200 Color: 10
Size: 292574 Color: 14
Size: 272227 Color: 15

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 434939 Color: 17
Size: 306652 Color: 14
Size: 258410 Color: 18

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 434982 Color: 0
Size: 291348 Color: 1
Size: 273671 Color: 19

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 435035 Color: 13
Size: 290927 Color: 5
Size: 274039 Color: 2

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 435046 Color: 17
Size: 296012 Color: 19
Size: 268943 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 435792 Color: 10
Size: 309128 Color: 19
Size: 255081 Color: 8

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 435414 Color: 5
Size: 282356 Color: 8
Size: 282231 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 436165 Color: 3
Size: 305282 Color: 13
Size: 258554 Color: 5

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 436898 Color: 10
Size: 299346 Color: 2
Size: 263757 Color: 7

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 436815 Color: 8
Size: 312872 Color: 17
Size: 250314 Color: 4

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 437079 Color: 18
Size: 296182 Color: 2
Size: 266740 Color: 17

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 437151 Color: 14
Size: 284748 Color: 6
Size: 278102 Color: 6

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 437204 Color: 17
Size: 282129 Color: 2
Size: 280668 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 437421 Color: 6
Size: 291142 Color: 0
Size: 271438 Color: 16

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 437764 Color: 8
Size: 310239 Color: 17
Size: 251998 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 438519 Color: 17
Size: 298261 Color: 17
Size: 263221 Color: 8

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 437953 Color: 18
Size: 285698 Color: 13
Size: 276350 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 438851 Color: 4
Size: 297932 Color: 2
Size: 263218 Color: 3

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 438957 Color: 13
Size: 306191 Color: 19
Size: 254853 Color: 2

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 438981 Color: 5
Size: 289133 Color: 4
Size: 271887 Color: 15

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 439019 Color: 18
Size: 300741 Color: 9
Size: 260241 Color: 10

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 439135 Color: 5
Size: 309450 Color: 8
Size: 251416 Color: 5

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 439433 Color: 11
Size: 282506 Color: 19
Size: 278062 Color: 12

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 439537 Color: 4
Size: 283582 Color: 7
Size: 276882 Color: 12

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 439547 Color: 6
Size: 284883 Color: 14
Size: 275571 Color: 12

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 439671 Color: 5
Size: 283474 Color: 3
Size: 276856 Color: 12

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 440702 Color: 16
Size: 297381 Color: 11
Size: 261918 Color: 14

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 440814 Color: 13
Size: 283134 Color: 12
Size: 276053 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 440916 Color: 3
Size: 307783 Color: 9
Size: 251302 Color: 13

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 440950 Color: 0
Size: 293972 Color: 7
Size: 265079 Color: 10

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 441132 Color: 11
Size: 297262 Color: 8
Size: 261607 Color: 16

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 441984 Color: 17
Size: 306557 Color: 9
Size: 251460 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 441683 Color: 3
Size: 295299 Color: 0
Size: 263019 Color: 7

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 441738 Color: 19
Size: 306740 Color: 12
Size: 251523 Color: 13

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 442068 Color: 11
Size: 292846 Color: 6
Size: 265087 Color: 4

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 442172 Color: 11
Size: 302291 Color: 8
Size: 255538 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 442389 Color: 7
Size: 300342 Color: 11
Size: 257270 Color: 5

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 442413 Color: 11
Size: 304537 Color: 1
Size: 253051 Color: 5

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 442426 Color: 12
Size: 297643 Color: 12
Size: 259932 Color: 15

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 442438 Color: 6
Size: 287248 Color: 16
Size: 270315 Color: 2

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 443323 Color: 17
Size: 297397 Color: 4
Size: 259281 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 442934 Color: 4
Size: 297778 Color: 13
Size: 259289 Color: 12

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 443268 Color: 13
Size: 293428 Color: 2
Size: 263305 Color: 16

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 443341 Color: 11
Size: 292056 Color: 0
Size: 264604 Color: 15

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 443611 Color: 6
Size: 286634 Color: 9
Size: 269756 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 443872 Color: 14
Size: 293503 Color: 3
Size: 262626 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 443914 Color: 14
Size: 303630 Color: 2
Size: 252457 Color: 18

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 444013 Color: 8
Size: 293823 Color: 15
Size: 262165 Color: 19

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 444063 Color: 17
Size: 302660 Color: 19
Size: 253278 Color: 12

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 444312 Color: 2
Size: 304871 Color: 0
Size: 250818 Color: 15

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 444314 Color: 12
Size: 292155 Color: 5
Size: 263532 Color: 8

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 444972 Color: 19
Size: 285016 Color: 7
Size: 270013 Color: 12

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 445251 Color: 14
Size: 299073 Color: 14
Size: 255677 Color: 3

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 445947 Color: 12
Size: 298385 Color: 7
Size: 255669 Color: 5

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 446964 Color: 17
Size: 292149 Color: 6
Size: 260888 Color: 18

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 446514 Color: 5
Size: 295754 Color: 12
Size: 257733 Color: 8

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 446698 Color: 16
Size: 284017 Color: 14
Size: 269286 Color: 5

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 447793 Color: 10
Size: 287390 Color: 14
Size: 264818 Color: 15

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 448563 Color: 7
Size: 300106 Color: 8
Size: 251332 Color: 11

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 448794 Color: 2
Size: 297901 Color: 0
Size: 253306 Color: 13

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 448944 Color: 6
Size: 277114 Color: 10
Size: 273943 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 448949 Color: 5
Size: 297956 Color: 15
Size: 253096 Color: 3

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 449725 Color: 7
Size: 288230 Color: 6
Size: 262046 Color: 6

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 450005 Color: 6
Size: 299209 Color: 13
Size: 250787 Color: 9

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 450009 Color: 6
Size: 286778 Color: 9
Size: 263214 Color: 2

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 450219 Color: 12
Size: 289905 Color: 8
Size: 259877 Color: 8

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 450362 Color: 17
Size: 296901 Color: 11
Size: 252738 Color: 15

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 450489 Color: 6
Size: 287406 Color: 2
Size: 262106 Color: 5

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 450719 Color: 10
Size: 295286 Color: 9
Size: 253996 Color: 6

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 451608 Color: 2
Size: 292601 Color: 6
Size: 255792 Color: 11

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 451668 Color: 5
Size: 275229 Color: 11
Size: 273104 Color: 19

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 451712 Color: 10
Size: 281762 Color: 19
Size: 266527 Color: 5

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 452358 Color: 12
Size: 278020 Color: 2
Size: 269623 Color: 13

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 452883 Color: 12
Size: 286098 Color: 5
Size: 261020 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 452946 Color: 9
Size: 290728 Color: 16
Size: 256327 Color: 8

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 453085 Color: 7
Size: 288412 Color: 7
Size: 258504 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 453325 Color: 5
Size: 285609 Color: 17
Size: 261067 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 453639 Color: 9
Size: 281364 Color: 5
Size: 264998 Color: 4

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 453839 Color: 16
Size: 294734 Color: 14
Size: 251428 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 453891 Color: 2
Size: 290786 Color: 0
Size: 255324 Color: 7

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 454099 Color: 3
Size: 288144 Color: 5
Size: 257758 Color: 18

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 454243 Color: 9
Size: 273729 Color: 18
Size: 272029 Color: 9

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 454272 Color: 4
Size: 293340 Color: 16
Size: 252389 Color: 10

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 454410 Color: 13
Size: 275147 Color: 16
Size: 270444 Color: 12

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 454734 Color: 15
Size: 281861 Color: 7
Size: 263406 Color: 10

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 454756 Color: 11
Size: 285233 Color: 7
Size: 260012 Color: 8

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 455200 Color: 10
Size: 294354 Color: 3
Size: 250447 Color: 9

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 455335 Color: 19
Size: 291737 Color: 12
Size: 252929 Color: 10

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 455343 Color: 6
Size: 286804 Color: 15
Size: 257854 Color: 17

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 456310 Color: 13
Size: 286789 Color: 7
Size: 256902 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 456311 Color: 11
Size: 283869 Color: 9
Size: 259821 Color: 9

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 456785 Color: 8
Size: 273081 Color: 9
Size: 270135 Color: 11

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 456980 Color: 5
Size: 288490 Color: 18
Size: 254531 Color: 14

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 457084 Color: 17
Size: 276703 Color: 3
Size: 266214 Color: 18

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 457058 Color: 1
Size: 285037 Color: 12
Size: 257906 Color: 9

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 457065 Color: 18
Size: 271781 Color: 18
Size: 271155 Color: 13

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 457557 Color: 1
Size: 277230 Color: 10
Size: 265214 Color: 13

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 457886 Color: 8
Size: 291948 Color: 2
Size: 250167 Color: 14

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 458555 Color: 14
Size: 275536 Color: 17
Size: 265910 Color: 2

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 458860 Color: 15
Size: 287889 Color: 14
Size: 253252 Color: 5

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 459185 Color: 1
Size: 279007 Color: 0
Size: 261809 Color: 14

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 459499 Color: 19
Size: 274242 Color: 0
Size: 266260 Color: 4

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 459639 Color: 11
Size: 283044 Color: 9
Size: 257318 Color: 3

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 460222 Color: 3
Size: 289655 Color: 9
Size: 250124 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 460264 Color: 18
Size: 289358 Color: 14
Size: 250379 Color: 5

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 460280 Color: 11
Size: 273117 Color: 14
Size: 266604 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 461410 Color: 10
Size: 280851 Color: 13
Size: 257740 Color: 2

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 461471 Color: 14
Size: 280939 Color: 12
Size: 257591 Color: 17

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 461784 Color: 15
Size: 286174 Color: 9
Size: 252043 Color: 10

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 461862 Color: 6
Size: 272024 Color: 1
Size: 266115 Color: 19

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 461886 Color: 14
Size: 279455 Color: 4
Size: 258660 Color: 6

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 462068 Color: 13
Size: 283042 Color: 0
Size: 254891 Color: 18

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 462576 Color: 18
Size: 270503 Color: 10
Size: 266922 Color: 7

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 462770 Color: 14
Size: 276419 Color: 13
Size: 260812 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 462876 Color: 4
Size: 275768 Color: 16
Size: 261357 Color: 2

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 463052 Color: 3
Size: 281946 Color: 2
Size: 255003 Color: 18

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 463842 Color: 2
Size: 281609 Color: 11
Size: 254550 Color: 3

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 463863 Color: 18
Size: 271993 Color: 8
Size: 264145 Color: 16

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 464607 Color: 1
Size: 277743 Color: 9
Size: 257651 Color: 5

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 464929 Color: 9
Size: 268757 Color: 14
Size: 266315 Color: 1

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 465761 Color: 17
Size: 272650 Color: 9
Size: 261590 Color: 14

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 465171 Color: 1
Size: 282501 Color: 12
Size: 252329 Color: 14

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 466855 Color: 12
Size: 274022 Color: 16
Size: 259124 Color: 4

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 466866 Color: 9
Size: 277458 Color: 12
Size: 255677 Color: 19

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 467285 Color: 3
Size: 266813 Color: 9
Size: 265903 Color: 14

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 467338 Color: 3
Size: 273064 Color: 13
Size: 259599 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 467455 Color: 15
Size: 270923 Color: 7
Size: 261623 Color: 11

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 467992 Color: 4
Size: 273590 Color: 0
Size: 258419 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 469617 Color: 17
Size: 274777 Color: 13
Size: 255607 Color: 10

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 469258 Color: 4
Size: 271351 Color: 2
Size: 259392 Color: 13

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 470057 Color: 17
Size: 266927 Color: 7
Size: 263017 Color: 9

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 470135 Color: 10
Size: 277869 Color: 12
Size: 251997 Color: 2

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 470589 Color: 13
Size: 273492 Color: 12
Size: 255920 Color: 15

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 470616 Color: 1
Size: 275949 Color: 12
Size: 253436 Color: 16

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 470982 Color: 2
Size: 277726 Color: 10
Size: 251293 Color: 10

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 471323 Color: 3
Size: 275468 Color: 0
Size: 253210 Color: 7

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 471504 Color: 10
Size: 269730 Color: 2
Size: 258767 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 472310 Color: 3
Size: 274641 Color: 12
Size: 253050 Color: 16

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 472415 Color: 5
Size: 275749 Color: 1
Size: 251837 Color: 2

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 473619 Color: 11
Size: 266241 Color: 7
Size: 260141 Color: 13

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 474354 Color: 17
Size: 272011 Color: 19
Size: 253636 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 473895 Color: 9
Size: 267698 Color: 5
Size: 258408 Color: 4

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 473924 Color: 6
Size: 272465 Color: 15
Size: 253612 Color: 4

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 474483 Color: 15
Size: 273879 Color: 9
Size: 251639 Color: 16

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 474555 Color: 2
Size: 274677 Color: 8
Size: 250769 Color: 17

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 474729 Color: 0
Size: 274855 Color: 9
Size: 250417 Color: 2

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 474970 Color: 10
Size: 274577 Color: 10
Size: 250454 Color: 19

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 475333 Color: 14
Size: 267225 Color: 19
Size: 257443 Color: 15

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 475370 Color: 4
Size: 268279 Color: 12
Size: 256352 Color: 11

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 475630 Color: 18
Size: 267147 Color: 7
Size: 257224 Color: 17

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 475699 Color: 19
Size: 267352 Color: 8
Size: 256950 Color: 18

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 476657 Color: 16
Size: 269788 Color: 7
Size: 253556 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 476673 Color: 2
Size: 263725 Color: 8
Size: 259603 Color: 8

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 476835 Color: 3
Size: 266723 Color: 10
Size: 256443 Color: 12

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 477640 Color: 15
Size: 265780 Color: 10
Size: 256581 Color: 12

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 477989 Color: 16
Size: 261371 Color: 6
Size: 260641 Color: 1

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 478153 Color: 18
Size: 263468 Color: 14
Size: 258380 Color: 7

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 478903 Color: 11
Size: 264223 Color: 2
Size: 256875 Color: 9

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 479584 Color: 15
Size: 270181 Color: 18
Size: 250236 Color: 11

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 479964 Color: 4
Size: 265515 Color: 19
Size: 254522 Color: 8

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 480395 Color: 9
Size: 263410 Color: 10
Size: 256196 Color: 19

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 480754 Color: 2
Size: 262831 Color: 2
Size: 256416 Color: 3

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 481086 Color: 10
Size: 266424 Color: 7
Size: 252491 Color: 17

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 481431 Color: 6
Size: 261932 Color: 14
Size: 256638 Color: 12

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 481452 Color: 3
Size: 268068 Color: 16
Size: 250481 Color: 19

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 482345 Color: 9
Size: 264722 Color: 18
Size: 252934 Color: 18

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 482390 Color: 12
Size: 267087 Color: 5
Size: 250524 Color: 7

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 482669 Color: 3
Size: 260037 Color: 17
Size: 257295 Color: 7

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 483117 Color: 2
Size: 258645 Color: 1
Size: 258239 Color: 12

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 483551 Color: 2
Size: 259442 Color: 3
Size: 257008 Color: 10

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 483775 Color: 6
Size: 266153 Color: 16
Size: 250073 Color: 19

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 484095 Color: 7
Size: 263438 Color: 16
Size: 252468 Color: 4

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 484501 Color: 19
Size: 262015 Color: 3
Size: 253485 Color: 14

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 485355 Color: 11
Size: 261874 Color: 11
Size: 252772 Color: 10

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 485501 Color: 6
Size: 263572 Color: 19
Size: 250928 Color: 16

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 485811 Color: 10
Size: 262008 Color: 18
Size: 252182 Color: 8

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 485914 Color: 2
Size: 260405 Color: 1
Size: 253682 Color: 16

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 486662 Color: 4
Size: 261627 Color: 0
Size: 251712 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 487160 Color: 17
Size: 258682 Color: 11
Size: 254159 Color: 5

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 486892 Color: 15
Size: 259530 Color: 10
Size: 253579 Color: 7

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 487085 Color: 15
Size: 258959 Color: 6
Size: 253957 Color: 18

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 487114 Color: 13
Size: 256498 Color: 4
Size: 256389 Color: 19

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 487122 Color: 11
Size: 259488 Color: 6
Size: 253391 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 488022 Color: 9
Size: 260429 Color: 11
Size: 251550 Color: 18

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 488526 Color: 4
Size: 258188 Color: 4
Size: 253287 Color: 15

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 488686 Color: 3
Size: 258650 Color: 12
Size: 252665 Color: 12

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 490511 Color: 17
Size: 259292 Color: 0
Size: 250198 Color: 14

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 489612 Color: 2
Size: 257217 Color: 18
Size: 253172 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 489633 Color: 0
Size: 260229 Color: 9
Size: 250139 Color: 18

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 490647 Color: 17
Size: 255530 Color: 8
Size: 253824 Color: 18

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 491683 Color: 5
Size: 257174 Color: 7
Size: 251144 Color: 16

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 491686 Color: 0
Size: 256571 Color: 15
Size: 251744 Color: 16

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 491892 Color: 16
Size: 256175 Color: 11
Size: 251934 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 492553 Color: 15
Size: 256619 Color: 12
Size: 250829 Color: 4

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 492915 Color: 19
Size: 253705 Color: 18
Size: 253381 Color: 17

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 493397 Color: 0
Size: 255703 Color: 4
Size: 250901 Color: 18

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 493541 Color: 4
Size: 256013 Color: 6
Size: 250447 Color: 15

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 493626 Color: 4
Size: 255425 Color: 3
Size: 250950 Color: 3

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 493713 Color: 4
Size: 254986 Color: 14
Size: 251302 Color: 16

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 493871 Color: 3
Size: 253424 Color: 8
Size: 252706 Color: 17

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 493986 Color: 13
Size: 254324 Color: 3
Size: 251691 Color: 4

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 494283 Color: 15
Size: 254937 Color: 10
Size: 250781 Color: 6

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 494536 Color: 4
Size: 253496 Color: 0
Size: 251969 Color: 7

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 494576 Color: 2
Size: 254155 Color: 1
Size: 251270 Color: 16

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 494735 Color: 17
Size: 253318 Color: 4
Size: 251948 Color: 13

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 495590 Color: 15
Size: 252501 Color: 15
Size: 251910 Color: 19

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 496277 Color: 15
Size: 253708 Color: 1
Size: 250016 Color: 16

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 496690 Color: 8
Size: 253194 Color: 3
Size: 250117 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 496794 Color: 14
Size: 251783 Color: 17
Size: 251424 Color: 13

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 497046 Color: 12
Size: 252485 Color: 9
Size: 250470 Color: 5

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 367644 Color: 7
Size: 348062 Color: 0
Size: 284294 Color: 5

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 367946 Color: 8
Size: 330170 Color: 15
Size: 301884 Color: 3

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 369511 Color: 15
Size: 329238 Color: 17
Size: 301251 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 369953 Color: 2
Size: 344430 Color: 1
Size: 285617 Color: 1

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 370726 Color: 6
Size: 334318 Color: 5
Size: 294956 Color: 2

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 374318 Color: 19
Size: 352986 Color: 4
Size: 272696 Color: 2

Bin 430: 1 of cap free
Amount of items: 3
Items: 
Size: 375201 Color: 9
Size: 363704 Color: 10
Size: 261095 Color: 16

Bin 431: 1 of cap free
Amount of items: 3
Items: 
Size: 376295 Color: 12
Size: 319635 Color: 6
Size: 304070 Color: 15

Bin 432: 1 of cap free
Amount of items: 3
Items: 
Size: 377794 Color: 13
Size: 351328 Color: 17
Size: 270878 Color: 13

Bin 433: 1 of cap free
Amount of items: 3
Items: 
Size: 380192 Color: 4
Size: 355450 Color: 14
Size: 264358 Color: 4

Bin 434: 1 of cap free
Amount of items: 3
Items: 
Size: 380674 Color: 5
Size: 316104 Color: 1
Size: 303222 Color: 17

Bin 435: 1 of cap free
Amount of items: 3
Items: 
Size: 381150 Color: 11
Size: 323326 Color: 8
Size: 295524 Color: 4

Bin 436: 1 of cap free
Amount of items: 3
Items: 
Size: 382982 Color: 2
Size: 331094 Color: 0
Size: 285924 Color: 8

Bin 437: 1 of cap free
Amount of items: 3
Items: 
Size: 383841 Color: 3
Size: 348925 Color: 8
Size: 267234 Color: 15

Bin 438: 1 of cap free
Amount of items: 3
Items: 
Size: 384991 Color: 9
Size: 322329 Color: 12
Size: 292680 Color: 10

Bin 439: 1 of cap free
Amount of items: 3
Items: 
Size: 385703 Color: 4
Size: 336831 Color: 7
Size: 277466 Color: 14

Bin 440: 1 of cap free
Amount of items: 3
Items: 
Size: 387033 Color: 2
Size: 357924 Color: 4
Size: 255043 Color: 3

Bin 441: 1 of cap free
Amount of items: 3
Items: 
Size: 387393 Color: 15
Size: 359289 Color: 10
Size: 253318 Color: 11

Bin 442: 1 of cap free
Amount of items: 3
Items: 
Size: 388418 Color: 14
Size: 307885 Color: 7
Size: 303697 Color: 10

Bin 443: 1 of cap free
Amount of items: 3
Items: 
Size: 388501 Color: 7
Size: 351873 Color: 12
Size: 259626 Color: 5

Bin 444: 1 of cap free
Amount of items: 3
Items: 
Size: 388594 Color: 12
Size: 326239 Color: 16
Size: 285167 Color: 19

Bin 445: 1 of cap free
Amount of items: 3
Items: 
Size: 389000 Color: 16
Size: 321372 Color: 11
Size: 289628 Color: 0

Bin 446: 1 of cap free
Amount of items: 3
Items: 
Size: 392815 Color: 16
Size: 306359 Color: 18
Size: 300826 Color: 8

Bin 447: 1 of cap free
Amount of items: 3
Items: 
Size: 393425 Color: 11
Size: 338702 Color: 14
Size: 267873 Color: 14

Bin 448: 1 of cap free
Amount of items: 3
Items: 
Size: 393893 Color: 19
Size: 333230 Color: 3
Size: 272877 Color: 1

Bin 449: 1 of cap free
Amount of items: 3
Items: 
Size: 396339 Color: 16
Size: 337115 Color: 16
Size: 266546 Color: 5

Bin 450: 1 of cap free
Amount of items: 3
Items: 
Size: 397336 Color: 1
Size: 320615 Color: 5
Size: 282049 Color: 15

Bin 451: 1 of cap free
Amount of items: 3
Items: 
Size: 402087 Color: 2
Size: 300915 Color: 0
Size: 296998 Color: 9

Bin 452: 1 of cap free
Amount of items: 3
Items: 
Size: 402559 Color: 17
Size: 310823 Color: 12
Size: 286618 Color: 5

Bin 453: 1 of cap free
Amount of items: 3
Items: 
Size: 404654 Color: 13
Size: 318223 Color: 19
Size: 277123 Color: 17

Bin 454: 1 of cap free
Amount of items: 3
Items: 
Size: 405903 Color: 18
Size: 315666 Color: 17
Size: 278431 Color: 5

Bin 455: 1 of cap free
Amount of items: 3
Items: 
Size: 406576 Color: 10
Size: 332092 Color: 0
Size: 261332 Color: 2

Bin 456: 1 of cap free
Amount of items: 3
Items: 
Size: 406067 Color: 18
Size: 313943 Color: 4
Size: 279990 Color: 9

Bin 457: 1 of cap free
Amount of items: 3
Items: 
Size: 407392 Color: 19
Size: 339706 Color: 0
Size: 252902 Color: 7

Bin 458: 1 of cap free
Amount of items: 3
Items: 
Size: 407945 Color: 2
Size: 324514 Color: 0
Size: 267541 Color: 5

Bin 459: 1 of cap free
Amount of items: 3
Items: 
Size: 410522 Color: 19
Size: 304242 Color: 17
Size: 285236 Color: 3

Bin 460: 1 of cap free
Amount of items: 3
Items: 
Size: 411241 Color: 17
Size: 338272 Color: 16
Size: 250487 Color: 16

Bin 461: 1 of cap free
Amount of items: 3
Items: 
Size: 412484 Color: 8
Size: 311711 Color: 8
Size: 275805 Color: 11

Bin 462: 1 of cap free
Amount of items: 3
Items: 
Size: 413596 Color: 9
Size: 308283 Color: 14
Size: 278121 Color: 7

Bin 463: 1 of cap free
Amount of items: 3
Items: 
Size: 416985 Color: 10
Size: 307765 Color: 1
Size: 275250 Color: 11

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 418251 Color: 16
Size: 319937 Color: 8
Size: 261812 Color: 2

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 422708 Color: 6
Size: 307038 Color: 5
Size: 270254 Color: 0

Bin 466: 1 of cap free
Amount of items: 3
Items: 
Size: 424744 Color: 7
Size: 308901 Color: 11
Size: 266355 Color: 10

Bin 467: 1 of cap free
Amount of items: 3
Items: 
Size: 425774 Color: 14
Size: 300429 Color: 6
Size: 273797 Color: 3

Bin 468: 1 of cap free
Amount of items: 3
Items: 
Size: 429279 Color: 15
Size: 315216 Color: 4
Size: 255505 Color: 10

Bin 469: 1 of cap free
Amount of items: 3
Items: 
Size: 429848 Color: 13
Size: 312561 Color: 6
Size: 257591 Color: 6

Bin 470: 1 of cap free
Amount of items: 3
Items: 
Size: 431469 Color: 2
Size: 288039 Color: 11
Size: 280492 Color: 13

Bin 471: 1 of cap free
Amount of items: 3
Items: 
Size: 433360 Color: 19
Size: 293031 Color: 17
Size: 273609 Color: 4

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 436354 Color: 13
Size: 282978 Color: 6
Size: 280668 Color: 9

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 437414 Color: 7
Size: 299776 Color: 0
Size: 262810 Color: 17

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 438746 Color: 15
Size: 286407 Color: 4
Size: 274847 Color: 17

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 439365 Color: 1
Size: 299980 Color: 3
Size: 260655 Color: 17

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 442103 Color: 15
Size: 293021 Color: 13
Size: 264876 Color: 17

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 443480 Color: 8
Size: 292506 Color: 5
Size: 264014 Color: 17

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 453676 Color: 19
Size: 275836 Color: 2
Size: 270488 Color: 7

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 453988 Color: 8
Size: 280988 Color: 17
Size: 265024 Color: 10

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 469424 Color: 14
Size: 276144 Color: 3
Size: 254432 Color: 3

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 489657 Color: 3
Size: 258817 Color: 18
Size: 251526 Color: 6

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 491635 Color: 12
Size: 254368 Color: 15
Size: 253997 Color: 17

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 494965 Color: 6
Size: 254379 Color: 1
Size: 250656 Color: 9

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 365959 Color: 18
Size: 332484 Color: 15
Size: 301556 Color: 11

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 367665 Color: 10
Size: 331322 Color: 8
Size: 301012 Color: 8

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 370517 Color: 9
Size: 323432 Color: 13
Size: 306050 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 370570 Color: 9
Size: 340664 Color: 11
Size: 288765 Color: 5

Bin 488: 2 of cap free
Amount of items: 3
Items: 
Size: 377146 Color: 9
Size: 351514 Color: 3
Size: 271339 Color: 10

Bin 489: 2 of cap free
Amount of items: 3
Items: 
Size: 378836 Color: 9
Size: 356445 Color: 6
Size: 264718 Color: 16

Bin 490: 2 of cap free
Amount of items: 3
Items: 
Size: 382941 Color: 8
Size: 353231 Color: 7
Size: 263827 Color: 16

Bin 491: 2 of cap free
Amount of items: 3
Items: 
Size: 383167 Color: 7
Size: 357429 Color: 14
Size: 259403 Color: 3

Bin 492: 2 of cap free
Amount of items: 3
Items: 
Size: 384788 Color: 1
Size: 352613 Color: 5
Size: 262598 Color: 15

Bin 493: 2 of cap free
Amount of items: 3
Items: 
Size: 388360 Color: 7
Size: 354742 Color: 3
Size: 256897 Color: 8

Bin 494: 2 of cap free
Amount of items: 3
Items: 
Size: 389259 Color: 4
Size: 331397 Color: 5
Size: 279343 Color: 0

Bin 495: 2 of cap free
Amount of items: 3
Items: 
Size: 390431 Color: 15
Size: 349045 Color: 0
Size: 260523 Color: 11

Bin 496: 2 of cap free
Amount of items: 3
Items: 
Size: 392039 Color: 0
Size: 316047 Color: 12
Size: 291913 Color: 5

Bin 497: 2 of cap free
Amount of items: 3
Items: 
Size: 395217 Color: 17
Size: 351084 Color: 16
Size: 253698 Color: 16

Bin 498: 2 of cap free
Amount of items: 3
Items: 
Size: 396860 Color: 12
Size: 343908 Color: 15
Size: 259231 Color: 10

Bin 499: 2 of cap free
Amount of items: 3
Items: 
Size: 398051 Color: 14
Size: 317981 Color: 13
Size: 283967 Color: 4

Bin 500: 2 of cap free
Amount of items: 3
Items: 
Size: 402227 Color: 5
Size: 327487 Color: 14
Size: 270285 Color: 8

Bin 501: 2 of cap free
Amount of items: 3
Items: 
Size: 402267 Color: 8
Size: 346455 Color: 13
Size: 251277 Color: 16

Bin 502: 2 of cap free
Amount of items: 3
Items: 
Size: 402987 Color: 15
Size: 320591 Color: 5
Size: 276421 Color: 16

Bin 503: 2 of cap free
Amount of items: 3
Items: 
Size: 411620 Color: 4
Size: 300354 Color: 17
Size: 288025 Color: 9

Bin 504: 2 of cap free
Amount of items: 3
Items: 
Size: 412029 Color: 13
Size: 318903 Color: 13
Size: 269067 Color: 7

Bin 505: 2 of cap free
Amount of items: 3
Items: 
Size: 427115 Color: 17
Size: 305783 Color: 19
Size: 267101 Color: 10

Bin 506: 2 of cap free
Amount of items: 3
Items: 
Size: 449290 Color: 11
Size: 296803 Color: 17
Size: 253906 Color: 5

Bin 507: 2 of cap free
Amount of items: 3
Items: 
Size: 471206 Color: 14
Size: 265133 Color: 17
Size: 263660 Color: 4

Bin 508: 3 of cap free
Amount of items: 3
Items: 
Size: 361336 Color: 17
Size: 320022 Color: 16
Size: 318640 Color: 7

Bin 509: 3 of cap free
Amount of items: 3
Items: 
Size: 361503 Color: 7
Size: 340264 Color: 17
Size: 298231 Color: 7

Bin 510: 3 of cap free
Amount of items: 3
Items: 
Size: 361658 Color: 2
Size: 343503 Color: 6
Size: 294837 Color: 16

Bin 511: 3 of cap free
Amount of items: 3
Items: 
Size: 367455 Color: 6
Size: 359333 Color: 15
Size: 273210 Color: 16

Bin 512: 3 of cap free
Amount of items: 3
Items: 
Size: 368956 Color: 6
Size: 324941 Color: 11
Size: 306101 Color: 11

Bin 513: 3 of cap free
Amount of items: 3
Items: 
Size: 370162 Color: 18
Size: 324776 Color: 5
Size: 305060 Color: 14

Bin 514: 3 of cap free
Amount of items: 3
Items: 
Size: 370658 Color: 19
Size: 340199 Color: 14
Size: 289141 Color: 5

Bin 515: 3 of cap free
Amount of items: 3
Items: 
Size: 375277 Color: 3
Size: 315130 Color: 15
Size: 309591 Color: 19

Bin 516: 3 of cap free
Amount of items: 3
Items: 
Size: 376052 Color: 16
Size: 370693 Color: 1
Size: 253253 Color: 17

Bin 517: 3 of cap free
Amount of items: 3
Items: 
Size: 384655 Color: 19
Size: 363890 Color: 17
Size: 251453 Color: 1

Bin 518: 3 of cap free
Amount of items: 3
Items: 
Size: 385422 Color: 12
Size: 316146 Color: 13
Size: 298430 Color: 18

Bin 519: 3 of cap free
Amount of items: 3
Items: 
Size: 396048 Color: 2
Size: 305067 Color: 3
Size: 298883 Color: 10

Bin 520: 3 of cap free
Amount of items: 3
Items: 
Size: 400198 Color: 15
Size: 332353 Color: 17
Size: 267447 Color: 9

Bin 521: 3 of cap free
Amount of items: 3
Items: 
Size: 400427 Color: 19
Size: 302646 Color: 6
Size: 296925 Color: 6

Bin 522: 3 of cap free
Amount of items: 3
Items: 
Size: 404802 Color: 15
Size: 317195 Color: 11
Size: 278001 Color: 6

Bin 523: 3 of cap free
Amount of items: 3
Items: 
Size: 415573 Color: 6
Size: 308558 Color: 19
Size: 275867 Color: 10

Bin 524: 3 of cap free
Amount of items: 3
Items: 
Size: 416278 Color: 5
Size: 320490 Color: 10
Size: 263230 Color: 0

Bin 525: 3 of cap free
Amount of items: 3
Items: 
Size: 421304 Color: 9
Size: 301068 Color: 9
Size: 277626 Color: 10

Bin 526: 3 of cap free
Amount of items: 3
Items: 
Size: 448164 Color: 2
Size: 297972 Color: 17
Size: 253862 Color: 9

Bin 527: 3 of cap free
Amount of items: 3
Items: 
Size: 489553 Color: 0
Size: 256560 Color: 15
Size: 253885 Color: 8

Bin 528: 4 of cap free
Amount of items: 3
Items: 
Size: 363787 Color: 7
Size: 327851 Color: 15
Size: 308359 Color: 15

Bin 529: 4 of cap free
Amount of items: 3
Items: 
Size: 368541 Color: 4
Size: 326182 Color: 0
Size: 305274 Color: 18

Bin 530: 4 of cap free
Amount of items: 3
Items: 
Size: 370079 Color: 14
Size: 348668 Color: 3
Size: 281250 Color: 7

Bin 531: 4 of cap free
Amount of items: 3
Items: 
Size: 371778 Color: 17
Size: 322415 Color: 2
Size: 305804 Color: 13

Bin 532: 4 of cap free
Amount of items: 3
Items: 
Size: 373242 Color: 2
Size: 354148 Color: 14
Size: 272607 Color: 4

Bin 533: 4 of cap free
Amount of items: 3
Items: 
Size: 374470 Color: 10
Size: 315675 Color: 13
Size: 309852 Color: 3

Bin 534: 4 of cap free
Amount of items: 3
Items: 
Size: 377294 Color: 10
Size: 330567 Color: 17
Size: 292136 Color: 1

Bin 535: 4 of cap free
Amount of items: 3
Items: 
Size: 400051 Color: 1
Size: 326178 Color: 10
Size: 273768 Color: 12

Bin 536: 4 of cap free
Amount of items: 3
Items: 
Size: 414339 Color: 9
Size: 313238 Color: 7
Size: 272420 Color: 10

Bin 537: 4 of cap free
Amount of items: 3
Items: 
Size: 439673 Color: 1
Size: 283805 Color: 17
Size: 276519 Color: 4

Bin 538: 4 of cap free
Amount of items: 3
Items: 
Size: 462552 Color: 7
Size: 286949 Color: 17
Size: 250496 Color: 1

Bin 539: 5 of cap free
Amount of items: 3
Items: 
Size: 361009 Color: 12
Size: 331727 Color: 17
Size: 307260 Color: 15

Bin 540: 5 of cap free
Amount of items: 3
Items: 
Size: 364753 Color: 8
Size: 333184 Color: 0
Size: 302059 Color: 9

Bin 541: 5 of cap free
Amount of items: 3
Items: 
Size: 370263 Color: 2
Size: 349959 Color: 7
Size: 279774 Color: 2

Bin 542: 5 of cap free
Amount of items: 3
Items: 
Size: 374609 Color: 11
Size: 348813 Color: 6
Size: 276574 Color: 0

Bin 543: 5 of cap free
Amount of items: 3
Items: 
Size: 376423 Color: 13
Size: 350463 Color: 13
Size: 273110 Color: 16

Bin 544: 5 of cap free
Amount of items: 3
Items: 
Size: 378160 Color: 4
Size: 319086 Color: 12
Size: 302750 Color: 14

Bin 545: 5 of cap free
Amount of items: 3
Items: 
Size: 383555 Color: 6
Size: 314032 Color: 9
Size: 302409 Color: 18

Bin 546: 5 of cap free
Amount of items: 3
Items: 
Size: 386398 Color: 4
Size: 315315 Color: 11
Size: 298283 Color: 0

Bin 547: 5 of cap free
Amount of items: 3
Items: 
Size: 403962 Color: 1
Size: 343747 Color: 10
Size: 252287 Color: 16

Bin 548: 5 of cap free
Amount of items: 3
Items: 
Size: 422708 Color: 13
Size: 314848 Color: 10
Size: 262440 Color: 2

Bin 549: 5 of cap free
Amount of items: 3
Items: 
Size: 459667 Color: 0
Size: 272845 Color: 3
Size: 267484 Color: 17

Bin 550: 5 of cap free
Amount of items: 3
Items: 
Size: 466529 Color: 10
Size: 283036 Color: 17
Size: 250431 Color: 18

Bin 551: 6 of cap free
Amount of items: 3
Items: 
Size: 364141 Color: 3
Size: 318839 Color: 4
Size: 317015 Color: 11

Bin 552: 6 of cap free
Amount of items: 3
Items: 
Size: 370484 Color: 1
Size: 346144 Color: 3
Size: 283367 Color: 15

Bin 553: 6 of cap free
Amount of items: 3
Items: 
Size: 370785 Color: 19
Size: 353107 Color: 3
Size: 276103 Color: 14

Bin 554: 6 of cap free
Amount of items: 3
Items: 
Size: 372135 Color: 7
Size: 346940 Color: 7
Size: 280920 Color: 15

Bin 555: 6 of cap free
Amount of items: 3
Items: 
Size: 377350 Color: 7
Size: 360282 Color: 9
Size: 262363 Color: 3

Bin 556: 6 of cap free
Amount of items: 3
Items: 
Size: 380487 Color: 2
Size: 331855 Color: 19
Size: 287653 Color: 3

Bin 557: 6 of cap free
Amount of items: 3
Items: 
Size: 390179 Color: 4
Size: 323334 Color: 11
Size: 286482 Color: 10

Bin 558: 6 of cap free
Amount of items: 3
Items: 
Size: 400901 Color: 0
Size: 348066 Color: 0
Size: 251028 Color: 10

Bin 559: 6 of cap free
Amount of items: 3
Items: 
Size: 407307 Color: 6
Size: 338471 Color: 10
Size: 254217 Color: 9

Bin 560: 7 of cap free
Amount of items: 3
Items: 
Size: 372181 Color: 18
Size: 369406 Color: 9
Size: 258407 Color: 11

Bin 561: 7 of cap free
Amount of items: 3
Items: 
Size: 372955 Color: 13
Size: 340698 Color: 14
Size: 286341 Color: 1

Bin 562: 7 of cap free
Amount of items: 3
Items: 
Size: 379829 Color: 6
Size: 335634 Color: 18
Size: 284531 Color: 6

Bin 563: 7 of cap free
Amount of items: 3
Items: 
Size: 381512 Color: 18
Size: 325519 Color: 18
Size: 292963 Color: 2

Bin 564: 7 of cap free
Amount of items: 3
Items: 
Size: 383466 Color: 7
Size: 327818 Color: 10
Size: 288710 Color: 3

Bin 565: 7 of cap free
Amount of items: 3
Items: 
Size: 384484 Color: 16
Size: 358529 Color: 10
Size: 256981 Color: 11

Bin 566: 7 of cap free
Amount of items: 3
Items: 
Size: 451981 Color: 14
Size: 276430 Color: 16
Size: 271583 Color: 17

Bin 567: 7 of cap free
Amount of items: 3
Items: 
Size: 454558 Color: 13
Size: 281270 Color: 1
Size: 264166 Color: 17

Bin 568: 7 of cap free
Amount of items: 3
Items: 
Size: 479069 Color: 9
Size: 269328 Color: 3
Size: 251597 Color: 17

Bin 569: 8 of cap free
Amount of items: 3
Items: 
Size: 360651 Color: 8
Size: 339987 Color: 4
Size: 299355 Color: 8

Bin 570: 8 of cap free
Amount of items: 3
Items: 
Size: 364180 Color: 16
Size: 358196 Color: 9
Size: 277617 Color: 8

Bin 571: 8 of cap free
Amount of items: 3
Items: 
Size: 370180 Color: 10
Size: 365812 Color: 8
Size: 264001 Color: 16

Bin 572: 8 of cap free
Amount of items: 3
Items: 
Size: 372101 Color: 13
Size: 332685 Color: 14
Size: 295207 Color: 11

Bin 573: 8 of cap free
Amount of items: 3
Items: 
Size: 372234 Color: 8
Size: 369443 Color: 17
Size: 258316 Color: 16

Bin 574: 8 of cap free
Amount of items: 3
Items: 
Size: 374922 Color: 2
Size: 322611 Color: 7
Size: 302460 Color: 17

Bin 575: 8 of cap free
Amount of items: 3
Items: 
Size: 386445 Color: 18
Size: 328204 Color: 5
Size: 285344 Color: 10

Bin 576: 8 of cap free
Amount of items: 3
Items: 
Size: 389071 Color: 9
Size: 307224 Color: 15
Size: 303698 Color: 10

Bin 577: 8 of cap free
Amount of items: 3
Items: 
Size: 398293 Color: 13
Size: 345112 Color: 18
Size: 256588 Color: 10

Bin 578: 8 of cap free
Amount of items: 3
Items: 
Size: 477241 Color: 15
Size: 271271 Color: 7
Size: 251481 Color: 17

Bin 579: 9 of cap free
Amount of items: 3
Items: 
Size: 363232 Color: 10
Size: 361255 Color: 17
Size: 275505 Color: 19

Bin 580: 9 of cap free
Amount of items: 3
Items: 
Size: 366550 Color: 16
Size: 323094 Color: 3
Size: 310348 Color: 18

Bin 581: 9 of cap free
Amount of items: 3
Items: 
Size: 370334 Color: 0
Size: 367875 Color: 8
Size: 261783 Color: 18

Bin 582: 9 of cap free
Amount of items: 3
Items: 
Size: 401961 Color: 13
Size: 341441 Color: 8
Size: 256590 Color: 10

Bin 583: 10 of cap free
Amount of items: 3
Items: 
Size: 382435 Color: 4
Size: 309312 Color: 10
Size: 308244 Color: 9

Bin 584: 10 of cap free
Amount of items: 3
Items: 
Size: 385803 Color: 1
Size: 320414 Color: 10
Size: 293774 Color: 15

Bin 585: 11 of cap free
Amount of items: 3
Items: 
Size: 371742 Color: 4
Size: 323398 Color: 2
Size: 304850 Color: 3

Bin 586: 12 of cap free
Amount of items: 3
Items: 
Size: 359701 Color: 2
Size: 350322 Color: 16
Size: 289966 Color: 19

Bin 587: 12 of cap free
Amount of items: 3
Items: 
Size: 369560 Color: 6
Size: 369523 Color: 5
Size: 260906 Color: 16

Bin 588: 12 of cap free
Amount of items: 3
Items: 
Size: 379176 Color: 9
Size: 359545 Color: 10
Size: 261268 Color: 3

Bin 589: 12 of cap free
Amount of items: 3
Items: 
Size: 393232 Color: 1
Size: 305262 Color: 10
Size: 301495 Color: 15

Bin 590: 13 of cap free
Amount of items: 3
Items: 
Size: 362661 Color: 1
Size: 362306 Color: 8
Size: 275021 Color: 9

Bin 591: 13 of cap free
Amount of items: 3
Items: 
Size: 365076 Color: 2
Size: 334795 Color: 2
Size: 300117 Color: 13

Bin 592: 13 of cap free
Amount of items: 3
Items: 
Size: 369964 Color: 3
Size: 334912 Color: 1
Size: 295112 Color: 8

Bin 593: 13 of cap free
Amount of items: 3
Items: 
Size: 374229 Color: 2
Size: 339514 Color: 10
Size: 286245 Color: 7

Bin 594: 14 of cap free
Amount of items: 3
Items: 
Size: 368615 Color: 13
Size: 361723 Color: 17
Size: 269649 Color: 2

Bin 595: 15 of cap free
Amount of items: 3
Items: 
Size: 354808 Color: 18
Size: 351619 Color: 5
Size: 293559 Color: 0

Bin 596: 15 of cap free
Amount of items: 3
Items: 
Size: 367219 Color: 2
Size: 328969 Color: 19
Size: 303798 Color: 10

Bin 597: 15 of cap free
Amount of items: 3
Items: 
Size: 368593 Color: 7
Size: 322724 Color: 8
Size: 308669 Color: 10

Bin 598: 15 of cap free
Amount of items: 3
Items: 
Size: 374561 Color: 10
Size: 336243 Color: 2
Size: 289182 Color: 16

Bin 599: 15 of cap free
Amount of items: 3
Items: 
Size: 418811 Color: 7
Size: 299646 Color: 10
Size: 281529 Color: 17

Bin 600: 15 of cap free
Amount of items: 3
Items: 
Size: 485312 Color: 4
Size: 258995 Color: 17
Size: 255679 Color: 0

Bin 601: 16 of cap free
Amount of items: 3
Items: 
Size: 359360 Color: 16
Size: 342333 Color: 13
Size: 298292 Color: 4

Bin 602: 16 of cap free
Amount of items: 3
Items: 
Size: 394548 Color: 18
Size: 332403 Color: 17
Size: 273034 Color: 10

Bin 603: 16 of cap free
Amount of items: 3
Items: 
Size: 422188 Color: 0
Size: 322303 Color: 10
Size: 255494 Color: 7

Bin 604: 17 of cap free
Amount of items: 3
Items: 
Size: 363906 Color: 18
Size: 356076 Color: 3
Size: 280002 Color: 6

Bin 605: 17 of cap free
Amount of items: 3
Items: 
Size: 369539 Color: 3
Size: 337293 Color: 3
Size: 293152 Color: 8

Bin 606: 20 of cap free
Amount of items: 3
Items: 
Size: 362717 Color: 7
Size: 358508 Color: 7
Size: 278756 Color: 11

Bin 607: 21 of cap free
Amount of items: 3
Items: 
Size: 359486 Color: 12
Size: 340108 Color: 9
Size: 300386 Color: 3

Bin 608: 22 of cap free
Amount of items: 3
Items: 
Size: 354399 Color: 10
Size: 348649 Color: 15
Size: 296931 Color: 17

Bin 609: 22 of cap free
Amount of items: 3
Items: 
Size: 374306 Color: 12
Size: 332927 Color: 5
Size: 292746 Color: 15

Bin 610: 22 of cap free
Amount of items: 3
Items: 
Size: 383506 Color: 19
Size: 309778 Color: 2
Size: 306695 Color: 7

Bin 611: 25 of cap free
Amount of items: 3
Items: 
Size: 359286 Color: 17
Size: 333996 Color: 2
Size: 306694 Color: 14

Bin 612: 25 of cap free
Amount of items: 3
Items: 
Size: 365660 Color: 10
Size: 341826 Color: 9
Size: 292490 Color: 18

Bin 613: 27 of cap free
Amount of items: 3
Items: 
Size: 359996 Color: 1
Size: 331311 Color: 5
Size: 308667 Color: 4

Bin 614: 27 of cap free
Amount of items: 3
Items: 
Size: 373603 Color: 5
Size: 330409 Color: 7
Size: 295962 Color: 8

Bin 615: 28 of cap free
Amount of items: 3
Items: 
Size: 380164 Color: 16
Size: 329466 Color: 3
Size: 290343 Color: 7

Bin 616: 30 of cap free
Amount of items: 3
Items: 
Size: 372819 Color: 5
Size: 323442 Color: 10
Size: 303710 Color: 7

Bin 617: 32 of cap free
Amount of items: 3
Items: 
Size: 357143 Color: 3
Size: 353164 Color: 19
Size: 289662 Color: 11

Bin 618: 34 of cap free
Amount of items: 3
Items: 
Size: 381894 Color: 10
Size: 323327 Color: 6
Size: 294746 Color: 2

Bin 619: 35 of cap free
Amount of items: 3
Items: 
Size: 367899 Color: 5
Size: 326265 Color: 10
Size: 305802 Color: 11

Bin 620: 36 of cap free
Amount of items: 3
Items: 
Size: 360374 Color: 10
Size: 327212 Color: 2
Size: 312379 Color: 11

Bin 621: 37 of cap free
Amount of items: 3
Items: 
Size: 369545 Color: 19
Size: 361519 Color: 18
Size: 268900 Color: 6

Bin 622: 38 of cap free
Amount of items: 3
Items: 
Size: 371969 Color: 7
Size: 359172 Color: 5
Size: 268822 Color: 9

Bin 623: 39 of cap free
Amount of items: 3
Items: 
Size: 349820 Color: 13
Size: 349418 Color: 14
Size: 300724 Color: 2

Bin 624: 39 of cap free
Amount of items: 3
Items: 
Size: 376713 Color: 2
Size: 349002 Color: 15
Size: 274247 Color: 3

Bin 625: 48 of cap free
Amount of items: 3
Items: 
Size: 369134 Color: 10
Size: 344348 Color: 1
Size: 286471 Color: 11

Bin 626: 50 of cap free
Amount of items: 3
Items: 
Size: 371279 Color: 19
Size: 359917 Color: 17
Size: 268755 Color: 8

Bin 627: 51 of cap free
Amount of items: 3
Items: 
Size: 358911 Color: 8
Size: 320865 Color: 7
Size: 320174 Color: 17

Bin 628: 52 of cap free
Amount of items: 3
Items: 
Size: 360251 Color: 0
Size: 322161 Color: 3
Size: 317537 Color: 16

Bin 629: 65 of cap free
Amount of items: 3
Items: 
Size: 334750 Color: 9
Size: 334238 Color: 10
Size: 330948 Color: 6

Bin 630: 66 of cap free
Amount of items: 3
Items: 
Size: 359160 Color: 0
Size: 351051 Color: 1
Size: 289724 Color: 10

Bin 631: 72 of cap free
Amount of items: 3
Items: 
Size: 344536 Color: 11
Size: 329678 Color: 4
Size: 325715 Color: 14

Bin 632: 78 of cap free
Amount of items: 3
Items: 
Size: 352560 Color: 2
Size: 333106 Color: 19
Size: 314257 Color: 10

Bin 633: 79 of cap free
Amount of items: 3
Items: 
Size: 357801 Color: 8
Size: 340178 Color: 5
Size: 301943 Color: 10

Bin 634: 82 of cap free
Amount of items: 3
Items: 
Size: 361352 Color: 5
Size: 344092 Color: 9
Size: 294475 Color: 10

Bin 635: 107 of cap free
Amount of items: 3
Items: 
Size: 361651 Color: 13
Size: 327441 Color: 18
Size: 310802 Color: 5

Bin 636: 108 of cap free
Amount of items: 3
Items: 
Size: 357829 Color: 11
Size: 324143 Color: 3
Size: 317921 Color: 14

Bin 637: 115 of cap free
Amount of items: 3
Items: 
Size: 347917 Color: 6
Size: 331740 Color: 9
Size: 320229 Color: 10

Bin 638: 116 of cap free
Amount of items: 3
Items: 
Size: 357368 Color: 18
Size: 331883 Color: 18
Size: 310634 Color: 12

Bin 639: 116 of cap free
Amount of items: 3
Items: 
Size: 358891 Color: 17
Size: 330137 Color: 13
Size: 310857 Color: 9

Bin 640: 123 of cap free
Amount of items: 3
Items: 
Size: 355915 Color: 0
Size: 323928 Color: 9
Size: 320035 Color: 1

Bin 641: 136 of cap free
Amount of items: 3
Items: 
Size: 366359 Color: 3
Size: 346938 Color: 17
Size: 286568 Color: 2

Bin 642: 145 of cap free
Amount of items: 3
Items: 
Size: 355343 Color: 13
Size: 333217 Color: 3
Size: 311296 Color: 14

Bin 643: 173 of cap free
Amount of items: 3
Items: 
Size: 352293 Color: 6
Size: 336238 Color: 17
Size: 311297 Color: 13

Bin 644: 206 of cap free
Amount of items: 3
Items: 
Size: 357767 Color: 11
Size: 352337 Color: 14
Size: 289691 Color: 12

Bin 645: 225 of cap free
Amount of items: 3
Items: 
Size: 364557 Color: 17
Size: 340972 Color: 8
Size: 294247 Color: 5

Bin 646: 287 of cap free
Amount of items: 3
Items: 
Size: 359147 Color: 7
Size: 333783 Color: 5
Size: 306784 Color: 12

Bin 647: 288 of cap free
Amount of items: 3
Items: 
Size: 357872 Color: 12
Size: 325897 Color: 2
Size: 315944 Color: 1

Bin 648: 302 of cap free
Amount of items: 3
Items: 
Size: 363929 Color: 5
Size: 355087 Color: 17
Size: 280683 Color: 3

Bin 649: 365 of cap free
Amount of items: 3
Items: 
Size: 351988 Color: 10
Size: 331650 Color: 1
Size: 315998 Color: 5

Bin 650: 797 of cap free
Amount of items: 3
Items: 
Size: 348937 Color: 4
Size: 327203 Color: 11
Size: 323064 Color: 18

Bin 651: 1240 of cap free
Amount of items: 3
Items: 
Size: 354964 Color: 11
Size: 338387 Color: 1
Size: 305410 Color: 7

Bin 652: 1448 of cap free
Amount of items: 2
Items: 
Size: 499369 Color: 14
Size: 499184 Color: 19

Bin 653: 2219 of cap free
Amount of items: 3
Items: 
Size: 334093 Color: 5
Size: 332670 Color: 3
Size: 331019 Color: 0

Bin 654: 2490 of cap free
Amount of items: 3
Items: 
Size: 351822 Color: 9
Size: 327905 Color: 11
Size: 317784 Color: 14

Bin 655: 2614 of cap free
Amount of items: 3
Items: 
Size: 351892 Color: 15
Size: 327904 Color: 9
Size: 317591 Color: 13

Bin 656: 3616 of cap free
Amount of items: 2
Items: 
Size: 498951 Color: 1
Size: 497434 Color: 0

Bin 657: 3965 of cap free
Amount of items: 3
Items: 
Size: 352557 Color: 3
Size: 342403 Color: 1
Size: 301076 Color: 6

Bin 658: 10010 of cap free
Amount of items: 3
Items: 
Size: 351889 Color: 8
Size: 351638 Color: 18
Size: 286464 Color: 6

Bin 659: 15745 of cap free
Amount of items: 3
Items: 
Size: 349552 Color: 19
Size: 349184 Color: 1
Size: 285520 Color: 4

Bin 660: 28196 of cap free
Amount of items: 3
Items: 
Size: 349163 Color: 18
Size: 343215 Color: 5
Size: 279427 Color: 1

Bin 661: 33968 of cap free
Amount of items: 3
Items: 
Size: 348385 Color: 14
Size: 348010 Color: 14
Size: 269638 Color: 16

Bin 662: 48615 of cap free
Amount of items: 3
Items: 
Size: 344501 Color: 13
Size: 343934 Color: 3
Size: 262951 Color: 0

Bin 663: 51778 of cap free
Amount of items: 3
Items: 
Size: 345218 Color: 13
Size: 344310 Color: 11
Size: 258695 Color: 6

Bin 664: 65930 of cap free
Amount of items: 3
Items: 
Size: 342712 Color: 7
Size: 335903 Color: 8
Size: 255456 Color: 2

Bin 665: 66273 of cap free
Amount of items: 3
Items: 
Size: 340914 Color: 2
Size: 337352 Color: 5
Size: 255462 Color: 16

Bin 666: 72808 of cap free
Amount of items: 3
Items: 
Size: 337967 Color: 9
Size: 337579 Color: 19
Size: 251647 Color: 15

Bin 667: 84347 of cap free
Amount of items: 3
Items: 
Size: 330648 Color: 8
Size: 330611 Color: 12
Size: 254395 Color: 7

Bin 668: 499106 of cap free
Amount of items: 2
Items: 
Size: 250581 Color: 15
Size: 250314 Color: 17

Total size: 667000667
Total free space: 1000001

