Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 117
Size: 255 Color: 15
Size: 250 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 113
Size: 263 Color: 26
Size: 263 Color: 25

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 103
Size: 288 Color: 47
Size: 273 Color: 38

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 106
Size: 295 Color: 51
Size: 260 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 94
Size: 329 Color: 65
Size: 266 Color: 27

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 105
Size: 299 Color: 55
Size: 256 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 101
Size: 320 Color: 63
Size: 250 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 81
Size: 350 Color: 70
Size: 287 Color: 46

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 108
Size: 298 Color: 54
Size: 252 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 100
Size: 298 Color: 52
Size: 282 Color: 43

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 89
Size: 366 Color: 87
Size: 262 Color: 23

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 109
Size: 267 Color: 29
Size: 267 Color: 28

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 86
Size: 362 Color: 80
Size: 272 Color: 36

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 110
Size: 276 Color: 42
Size: 254 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 96
Size: 315 Color: 62
Size: 275 Color: 40

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 111
Size: 269 Color: 31
Size: 259 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 114
Size: 269 Color: 32
Size: 253 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 75
Size: 350 Color: 71
Size: 294 Color: 50

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 116
Size: 262 Color: 24
Size: 253 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 118
Size: 252 Color: 4
Size: 251 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 88
Size: 356 Color: 76
Size: 274 Color: 39

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 119
Size: 252 Color: 5
Size: 251 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 104
Size: 302 Color: 57
Size: 254 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 99
Size: 313 Color: 60
Size: 268 Color: 30

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 93
Size: 325 Color: 64
Size: 275 Color: 41

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 95
Size: 303 Color: 58
Size: 290 Color: 48

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 79
Size: 357 Color: 78
Size: 282 Color: 44

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 77
Size: 351 Color: 72
Size: 292 Color: 49

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 92
Size: 350 Color: 69
Size: 253 Color: 11

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 112
Size: 273 Color: 37
Size: 254 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 74
Size: 347 Color: 68
Size: 298 Color: 53

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 107
Size: 283 Color: 45
Size: 271 Color: 33

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 97
Size: 336 Color: 67
Size: 252 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 85
Size: 363 Color: 82
Size: 271 Color: 34

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 98
Size: 314 Color: 61
Size: 272 Color: 35

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 115
Size: 261 Color: 22
Size: 259 Color: 20

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 102
Size: 307 Color: 59
Size: 259 Color: 18

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 91
Size: 352 Color: 73
Size: 253 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 90
Size: 366 Color: 83
Size: 258 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 84
Size: 333 Color: 66
Size: 301 Color: 56

Total size: 40000
Total free space: 0

