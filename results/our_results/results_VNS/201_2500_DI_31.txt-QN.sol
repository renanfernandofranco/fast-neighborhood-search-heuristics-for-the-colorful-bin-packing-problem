Capicity Bin: 2492
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1879 Color: 168
Size: 453 Color: 101
Size: 160 Color: 59

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 175
Size: 282 Color: 80
Size: 204 Color: 67

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 177
Size: 355 Color: 92
Size: 120 Color: 48

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2066 Color: 181
Size: 346 Color: 90
Size: 80 Color: 30

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2067 Color: 182
Size: 273 Color: 78
Size: 152 Color: 57

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2117 Color: 187
Size: 285 Color: 81
Size: 90 Color: 34

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2121 Color: 188
Size: 311 Color: 85
Size: 60 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2126 Color: 189
Size: 358 Color: 93
Size: 8 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2149 Color: 190
Size: 287 Color: 82
Size: 56 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 192
Size: 226 Color: 72
Size: 108 Color: 41

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2173 Color: 194
Size: 267 Color: 76
Size: 52 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2175 Color: 195
Size: 313 Color: 86
Size: 4 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 196
Size: 300 Color: 83
Size: 2 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2197 Color: 197
Size: 247 Color: 73
Size: 48 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2199 Color: 198
Size: 271 Color: 77
Size: 22 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2222 Color: 200
Size: 218 Color: 71
Size: 52 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2234 Color: 201
Size: 206 Color: 68
Size: 52 Color: 15

Bin 18: 1 of cap free
Amount of items: 9
Items: 
Size: 515 Color: 106
Size: 511 Color: 105
Size: 387 Color: 94
Size: 345 Color: 89
Size: 265 Color: 75
Size: 144 Color: 53
Size: 112 Color: 43
Size: 108 Color: 42
Size: 104 Color: 39

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1846 Color: 165
Size: 583 Color: 110
Size: 62 Color: 20

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1946 Color: 171
Size: 505 Color: 104
Size: 40 Color: 7

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1949 Color: 172
Size: 542 Color: 108

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 173
Size: 530 Color: 107

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2000 Color: 174
Size: 443 Color: 99
Size: 48 Color: 10

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 176
Size: 352 Color: 91
Size: 132 Color: 51

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 2018 Color: 178
Size: 397 Color: 95
Size: 76 Color: 28

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 2109 Color: 186
Size: 254 Color: 74
Size: 128 Color: 50

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 2217 Color: 199
Size: 274 Color: 79

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 155
Size: 852 Color: 124
Size: 44 Color: 9

Bin 29: 3 of cap free
Amount of items: 5
Items: 
Size: 1249 Color: 140
Size: 932 Color: 127
Size: 104 Color: 40
Size: 102 Color: 38
Size: 102 Color: 37

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1875 Color: 167
Size: 614 Color: 112

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 2151 Color: 191
Size: 338 Color: 88

Bin 32: 4 of cap free
Amount of items: 2
Items: 
Size: 1554 Color: 151
Size: 934 Color: 128

Bin 33: 4 of cap free
Amount of items: 4
Items: 
Size: 1574 Color: 154
Size: 806 Color: 123
Size: 56 Color: 16
Size: 52 Color: 12

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 160
Size: 750 Color: 118

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 166
Size: 630 Color: 113

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 2090 Color: 185
Size: 398 Color: 96

Bin 37: 5 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 144
Size: 1133 Color: 137
Size: 76 Color: 27

Bin 38: 5 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 149
Size: 994 Color: 130
Size: 64 Color: 22

Bin 39: 5 of cap free
Amount of items: 2
Items: 
Size: 2021 Color: 179
Size: 466 Color: 103

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 2029 Color: 180
Size: 458 Color: 102

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 193
Size: 321 Color: 87

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 163
Size: 669 Color: 117
Size: 24 Color: 4

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 2079 Color: 184
Size: 406 Color: 98

Bin 44: 8 of cap free
Amount of items: 4
Items: 
Size: 1758 Color: 161
Size: 650 Color: 114
Size: 40 Color: 8
Size: 36 Color: 6

Bin 45: 8 of cap free
Amount of items: 2
Items: 
Size: 1834 Color: 164
Size: 650 Color: 115

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 170
Size: 550 Color: 109

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 158
Size: 769 Color: 120

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 2078 Color: 183
Size: 405 Color: 97

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 162
Size: 667 Color: 116
Size: 24 Color: 5

Bin 50: 12 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 147
Size: 1038 Color: 135
Size: 68 Color: 24

Bin 51: 13 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 150
Size: 887 Color: 125
Size: 62 Color: 21

Bin 52: 13 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 159
Size: 447 Color: 100
Size: 306 Color: 84

Bin 53: 17 of cap free
Amount of items: 2
Items: 
Size: 1247 Color: 139
Size: 1228 Color: 138

Bin 54: 17 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 157
Size: 782 Color: 121

Bin 55: 20 of cap free
Amount of items: 2
Items: 
Size: 1887 Color: 169
Size: 585 Color: 111

Bin 56: 25 of cap free
Amount of items: 3
Items: 
Size: 1362 Color: 146
Size: 1037 Color: 134
Size: 68 Color: 25

Bin 57: 32 of cap free
Amount of items: 4
Items: 
Size: 1253 Color: 142
Size: 1031 Color: 132
Size: 88 Color: 33
Size: 88 Color: 32

Bin 58: 32 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 145
Size: 1088 Color: 136
Size: 70 Color: 26

Bin 59: 32 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 152
Size: 889 Color: 126

Bin 60: 34 of cap free
Amount of items: 2
Items: 
Size: 1691 Color: 156
Size: 767 Color: 119

Bin 61: 36 of cap free
Amount of items: 4
Items: 
Size: 1250 Color: 141
Size: 1014 Color: 131
Size: 100 Color: 36
Size: 92 Color: 35

Bin 62: 44 of cap free
Amount of items: 4
Items: 
Size: 1257 Color: 143
Size: 1033 Color: 133
Size: 80 Color: 31
Size: 78 Color: 29

Bin 63: 55 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 148
Size: 942 Color: 129
Size: 68 Color: 23

Bin 64: 57 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 153
Size: 802 Color: 122
Size: 60 Color: 18

Bin 65: 62 of cap free
Amount of items: 15
Items: 
Size: 212 Color: 70
Size: 206 Color: 69
Size: 204 Color: 66
Size: 200 Color: 65
Size: 196 Color: 64
Size: 188 Color: 63
Size: 184 Color: 62
Size: 176 Color: 61
Size: 148 Color: 54
Size: 132 Color: 52
Size: 124 Color: 49
Size: 116 Color: 47
Size: 116 Color: 46
Size: 116 Color: 45
Size: 112 Color: 44

Bin 66: 1860 of cap free
Amount of items: 4
Items: 
Size: 176 Color: 60
Size: 156 Color: 58
Size: 152 Color: 56
Size: 148 Color: 55

Total size: 161980
Total free space: 2492

