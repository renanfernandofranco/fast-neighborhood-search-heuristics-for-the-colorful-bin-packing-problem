Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 0
Size: 250 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 3
Size: 356 Color: 1
Size: 286 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 352 Color: 1
Size: 276 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 2
Size: 303 Color: 2
Size: 260 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 292 Color: 2
Size: 268 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 342 Color: 0
Size: 287 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 260 Color: 3
Size: 250 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 352 Color: 4
Size: 262 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 329 Color: 4
Size: 303 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 293 Color: 3
Size: 281 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 343 Color: 3
Size: 258 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 317 Color: 4
Size: 287 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 267 Color: 2
Size: 257 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 363 Color: 0
Size: 253 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 257 Color: 4
Size: 253 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 276 Color: 1
Size: 258 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 0
Size: 329 Color: 0
Size: 309 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 289 Color: 0
Size: 271 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 360 Color: 0
Size: 256 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 360 Color: 2
Size: 256 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 305 Color: 3
Size: 288 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 285 Color: 4
Size: 279 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 3
Size: 351 Color: 2
Size: 293 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 259 Color: 2
Size: 253 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 302 Color: 2
Size: 263 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 0
Size: 294 Color: 3
Size: 256 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 313 Color: 0
Size: 263 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 259 Color: 3
Size: 255 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 4
Size: 306 Color: 3
Size: 283 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 292 Color: 4
Size: 277 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 263 Color: 1
Size: 254 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 4
Size: 332 Color: 0
Size: 267 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 334 Color: 0
Size: 294 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 265 Color: 4
Size: 251 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 4
Size: 278 Color: 4
Size: 253 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 323 Color: 3
Size: 251 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 2
Size: 262 Color: 3
Size: 255 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 310 Color: 1
Size: 306 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 300 Color: 2
Size: 268 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 322 Color: 4
Size: 264 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 263 Color: 4
Size: 250 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 4
Size: 288 Color: 0
Size: 267 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 4
Size: 257 Color: 2
Size: 254 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 355 Color: 1
Size: 250 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 259 Color: 1
Size: 250 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 338 Color: 4
Size: 297 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 276 Color: 2
Size: 265 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 337 Color: 2
Size: 272 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 3
Size: 342 Color: 0
Size: 301 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 258 Color: 3
Size: 258 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 353 Color: 4
Size: 266 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 296 Color: 4
Size: 256 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 4
Size: 278 Color: 0
Size: 264 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 0
Size: 351 Color: 1
Size: 297 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 262 Color: 3
Size: 258 Color: 1
Size: 480 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 3
Size: 367 Color: 3
Size: 264 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 305 Color: 4
Size: 299 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 281 Color: 2
Size: 262 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 2
Size: 330 Color: 0
Size: 282 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 258 Color: 3
Size: 256 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 306 Color: 1
Size: 272 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 362 Color: 3
Size: 273 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 355 Color: 2
Size: 251 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 320 Color: 4
Size: 306 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 344 Color: 2
Size: 285 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 330 Color: 4
Size: 279 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 367 Color: 3
Size: 259 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 293 Color: 2
Size: 268 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 290 Color: 4
Size: 268 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 280 Color: 4
Size: 251 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 310 Color: 1
Size: 260 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 321 Color: 0
Size: 277 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 336 Color: 3
Size: 307 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 281 Color: 3
Size: 270 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 257 Color: 2
Size: 251 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 303 Color: 0
Size: 287 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 0
Size: 340 Color: 0
Size: 252 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 283 Color: 1
Size: 251 Color: 3

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 363 Color: 0
Size: 267 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 2
Size: 319 Color: 1
Size: 251 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 315 Color: 0
Size: 278 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 3
Size: 258 Color: 0
Size: 251 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 251 Color: 1
Size: 250 Color: 1

Total size: 83000
Total free space: 0

