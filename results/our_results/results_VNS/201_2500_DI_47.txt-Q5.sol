Capicity Bin: 1976
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1075 Color: 0
Size: 823 Color: 3
Size: 78 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 2
Size: 542 Color: 4
Size: 60 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 4
Size: 531 Color: 2
Size: 44 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 408 Color: 0
Size: 106 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 4
Size: 391 Color: 3
Size: 78 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 3
Size: 402 Color: 3
Size: 32 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 0
Size: 362 Color: 2
Size: 52 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 4
Size: 319 Color: 0
Size: 62 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1635 Color: 1
Size: 317 Color: 4
Size: 24 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 3
Size: 266 Color: 2
Size: 52 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 0
Size: 259 Color: 4
Size: 50 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 0
Size: 262 Color: 4
Size: 32 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 3
Size: 241 Color: 1
Size: 46 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1705 Color: 2
Size: 221 Color: 1
Size: 50 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1700 Color: 0
Size: 140 Color: 2
Size: 136 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 0
Size: 204 Color: 2
Size: 46 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 2
Size: 150 Color: 3
Size: 48 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1774 Color: 3
Size: 108 Color: 1
Size: 94 Color: 2

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 991 Color: 0
Size: 751 Color: 2
Size: 233 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 2
Size: 718 Color: 4
Size: 60 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1341 Color: 4
Size: 550 Color: 2
Size: 84 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 0
Size: 502 Color: 2
Size: 78 Color: 4

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 2
Size: 389 Color: 1
Size: 164 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 1
Size: 502 Color: 2
Size: 56 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 0
Size: 456 Color: 4
Size: 16 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 3
Size: 246 Color: 3
Size: 56 Color: 2

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1721 Color: 4
Size: 170 Color: 2
Size: 84 Color: 3

Bin 28: 2 of cap free
Amount of items: 5
Items: 
Size: 990 Color: 2
Size: 462 Color: 2
Size: 304 Color: 0
Size: 110 Color: 4
Size: 108 Color: 4

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1275 Color: 2
Size: 651 Color: 4
Size: 48 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 2
Size: 537 Color: 2
Size: 104 Color: 3

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1511 Color: 1
Size: 399 Color: 1
Size: 64 Color: 2

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 1
Size: 227 Color: 1
Size: 144 Color: 2

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1611 Color: 4
Size: 323 Color: 0
Size: 40 Color: 2

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 1
Size: 166 Color: 4
Size: 158 Color: 2

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 4
Size: 253 Color: 1
Size: 8 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 4
Size: 236 Color: 0

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1158 Color: 2
Size: 729 Color: 0
Size: 86 Color: 3

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 1
Size: 585 Color: 4
Size: 62 Color: 2

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 2
Size: 430 Color: 0

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 4
Size: 307 Color: 1

Bin 41: 4 of cap free
Amount of items: 4
Items: 
Size: 1313 Color: 1
Size: 306 Color: 3
Size: 305 Color: 0
Size: 48 Color: 4

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 2
Size: 334 Color: 0

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1189 Color: 0
Size: 682 Color: 0
Size: 100 Color: 3

Bin 44: 5 of cap free
Amount of items: 4
Items: 
Size: 1318 Color: 1
Size: 481 Color: 4
Size: 130 Color: 3
Size: 42 Color: 3

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 0
Size: 427 Color: 4
Size: 20 Color: 0

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1610 Color: 1
Size: 361 Color: 0

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1697 Color: 1
Size: 274 Color: 4

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 0
Size: 311 Color: 1
Size: 32 Color: 0

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1589 Color: 0
Size: 380 Color: 1

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 3
Size: 899 Color: 4
Size: 80 Color: 3

Bin 51: 9 of cap free
Amount of items: 3
Items: 
Size: 1103 Color: 3
Size: 822 Color: 0
Size: 42 Color: 0

Bin 52: 9 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 0
Size: 657 Color: 2
Size: 48 Color: 3

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 4
Size: 553 Color: 0

Bin 54: 9 of cap free
Amount of items: 2
Items: 
Size: 1718 Color: 2
Size: 249 Color: 1

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1368 Color: 3
Size: 598 Color: 4

Bin 56: 10 of cap free
Amount of items: 2
Items: 
Size: 1499 Color: 0
Size: 467 Color: 2

Bin 57: 12 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 4
Size: 346 Color: 0
Size: 40 Color: 2

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1679 Color: 3
Size: 282 Color: 1

Bin 59: 16 of cap free
Amount of items: 3
Items: 
Size: 1054 Color: 4
Size: 866 Color: 2
Size: 40 Color: 3

Bin 60: 16 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 2
Size: 800 Color: 2
Size: 42 Color: 3

Bin 61: 17 of cap free
Amount of items: 14
Items: 
Size: 218 Color: 3
Size: 216 Color: 0
Size: 213 Color: 4
Size: 210 Color: 2
Size: 206 Color: 2
Size: 164 Color: 1
Size: 116 Color: 2
Size: 116 Color: 2
Size: 96 Color: 2
Size: 92 Color: 4
Size: 92 Color: 3
Size: 76 Color: 3
Size: 72 Color: 3
Size: 72 Color: 0

Bin 62: 23 of cap free
Amount of items: 3
Items: 
Size: 1494 Color: 0
Size: 395 Color: 3
Size: 64 Color: 2

Bin 63: 26 of cap free
Amount of items: 2
Items: 
Size: 1465 Color: 0
Size: 485 Color: 4

Bin 64: 30 of cap free
Amount of items: 3
Items: 
Size: 1023 Color: 3
Size: 795 Color: 2
Size: 128 Color: 1

Bin 65: 32 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 0
Size: 668 Color: 3
Size: 40 Color: 2

Bin 66: 1640 of cap free
Amount of items: 5
Items: 
Size: 72 Color: 3
Size: 68 Color: 1
Size: 68 Color: 1
Size: 68 Color: 0
Size: 60 Color: 4

Total size: 128440
Total free space: 1976

