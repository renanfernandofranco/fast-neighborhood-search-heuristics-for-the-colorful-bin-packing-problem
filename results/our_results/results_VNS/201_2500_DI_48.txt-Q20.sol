Capicity Bin: 1972
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 997 Color: 14
Size: 813 Color: 10
Size: 162 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 16
Size: 671 Color: 13
Size: 134 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 14
Size: 551 Color: 0
Size: 108 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1423 Color: 4
Size: 459 Color: 16
Size: 90 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1435 Color: 19
Size: 451 Color: 6
Size: 86 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1507 Color: 3
Size: 355 Color: 6
Size: 110 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 4
Size: 379 Color: 14
Size: 68 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 4
Size: 389 Color: 2
Size: 36 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 4
Size: 350 Color: 16
Size: 68 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 0
Size: 422 Color: 16
Size: 36 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 4
Size: 349 Color: 3
Size: 42 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 15
Size: 221 Color: 11
Size: 164 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1631 Color: 17
Size: 249 Color: 10
Size: 92 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 12
Size: 283 Color: 6
Size: 56 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 11
Size: 250 Color: 12
Size: 48 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 10
Size: 171 Color: 19
Size: 116 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 10
Size: 227 Color: 11
Size: 52 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1717 Color: 19
Size: 211 Color: 10
Size: 44 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 8
Size: 184 Color: 18
Size: 70 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1747 Color: 4
Size: 181 Color: 19
Size: 44 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1756 Color: 12
Size: 174 Color: 4
Size: 42 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 19
Size: 162 Color: 10
Size: 44 Color: 15

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 990 Color: 11
Size: 821 Color: 5
Size: 160 Color: 15

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1125 Color: 19
Size: 814 Color: 13
Size: 32 Color: 9

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 6
Size: 710 Color: 6
Size: 90 Color: 5

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 19
Size: 669 Color: 19
Size: 32 Color: 17

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1305 Color: 2
Size: 586 Color: 4
Size: 80 Color: 9

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 19
Size: 553 Color: 11
Size: 68 Color: 3

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1431 Color: 15
Size: 456 Color: 14
Size: 84 Color: 15

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 10
Size: 162 Color: 2
Size: 140 Color: 3

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1676 Color: 10
Size: 295 Color: 8

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 0
Size: 262 Color: 9

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 1730 Color: 0
Size: 241 Color: 4

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 1769 Color: 14
Size: 202 Color: 0

Bin 35: 2 of cap free
Amount of items: 18
Items: 
Size: 248 Color: 19
Size: 220 Color: 4
Size: 189 Color: 13
Size: 175 Color: 2
Size: 134 Color: 7
Size: 132 Color: 9
Size: 110 Color: 4
Size: 100 Color: 1
Size: 96 Color: 4
Size: 90 Color: 5
Size: 76 Color: 19
Size: 76 Color: 3
Size: 64 Color: 2
Size: 56 Color: 11
Size: 56 Color: 3
Size: 52 Color: 3
Size: 48 Color: 11
Size: 48 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1240 Color: 16
Size: 666 Color: 11
Size: 64 Color: 7

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 4
Size: 589 Color: 19
Size: 72 Color: 12

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 3
Size: 466 Color: 12
Size: 140 Color: 19

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 16
Size: 522 Color: 11
Size: 34 Color: 12

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 19
Size: 455 Color: 11
Size: 64 Color: 14

Bin 41: 2 of cap free
Amount of items: 4
Items: 
Size: 1725 Color: 10
Size: 233 Color: 13
Size: 8 Color: 9
Size: 4 Color: 15

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 1753 Color: 0
Size: 213 Color: 4
Size: 4 Color: 10

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 1763 Color: 17
Size: 207 Color: 16

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 1427 Color: 2
Size: 322 Color: 11
Size: 220 Color: 17

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 2
Size: 286 Color: 16
Size: 8 Color: 10

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 1755 Color: 18
Size: 214 Color: 12

Bin 47: 4 of cap free
Amount of items: 2
Items: 
Size: 1586 Color: 17
Size: 382 Color: 15

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 8
Size: 267 Color: 4

Bin 49: 5 of cap free
Amount of items: 4
Items: 
Size: 987 Color: 14
Size: 435 Color: 14
Size: 429 Color: 6
Size: 116 Color: 9

Bin 50: 7 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 11
Size: 707 Color: 0
Size: 32 Color: 2

Bin 51: 9 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 11
Size: 408 Color: 6

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 15
Size: 305 Color: 18

Bin 53: 10 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 3
Size: 321 Color: 19
Size: 16 Color: 6

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 18
Size: 491 Color: 0

Bin 55: 12 of cap free
Amount of items: 3
Items: 
Size: 989 Color: 16
Size: 811 Color: 12
Size: 160 Color: 11

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1163 Color: 14
Size: 796 Color: 8

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 1630 Color: 19
Size: 327 Color: 15

Bin 58: 15 of cap free
Amount of items: 2
Items: 
Size: 1653 Color: 8
Size: 304 Color: 3

Bin 59: 18 of cap free
Amount of items: 4
Items: 
Size: 993 Color: 12
Size: 817 Color: 10
Size: 80 Color: 3
Size: 64 Color: 4

Bin 60: 25 of cap free
Amount of items: 3
Items: 
Size: 998 Color: 16
Size: 664 Color: 3
Size: 285 Color: 17

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1459 Color: 3
Size: 485 Color: 18

Bin 62: 29 of cap free
Amount of items: 2
Items: 
Size: 1122 Color: 6
Size: 821 Color: 7

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1267 Color: 4
Size: 675 Color: 6

Bin 64: 30 of cap free
Amount of items: 2
Items: 
Size: 1385 Color: 17
Size: 557 Color: 5

Bin 65: 33 of cap free
Amount of items: 2
Items: 
Size: 1001 Color: 4
Size: 938 Color: 18

Bin 66: 1626 of cap free
Amount of items: 8
Items: 
Size: 52 Color: 0
Size: 46 Color: 11
Size: 46 Color: 6
Size: 42 Color: 1
Size: 40 Color: 16
Size: 40 Color: 15
Size: 40 Color: 10
Size: 40 Color: 7

Total size: 128180
Total free space: 1972

