Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 269 Color: 0
Size: 323 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 271 Color: 1
Size: 251 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 344 Color: 1
Size: 304 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 262 Color: 1
Size: 252 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 292 Color: 1
Size: 274 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 336 Color: 1
Size: 277 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 251 Color: 1
Size: 251 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 288 Color: 1
Size: 250 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 1
Size: 252 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 326 Color: 1
Size: 280 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 284 Color: 1
Size: 284 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 256 Color: 1
Size: 254 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 362 Color: 1
Size: 270 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 304 Color: 1
Size: 262 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 346 Color: 1
Size: 297 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 344 Color: 1
Size: 269 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 324 Color: 1
Size: 276 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 296 Color: 1
Size: 276 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 340 Color: 1
Size: 278 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 262 Color: 1
Size: 256 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 356 Color: 1
Size: 282 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 314 Color: 1
Size: 259 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 305 Color: 1
Size: 281 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 329 Color: 1
Size: 263 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 356 Color: 1
Size: 250 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 327 Color: 1
Size: 276 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 336 Color: 1
Size: 289 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 347 Color: 1
Size: 292 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 300 Color: 1
Size: 270 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 293 Color: 1
Size: 288 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 353 Color: 1
Size: 281 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 338 Color: 1
Size: 252 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 330 Color: 1
Size: 275 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 261 Color: 1
Size: 259 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 386 Color: 1
Size: 250 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 345 Color: 1
Size: 285 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 272 Color: 1
Size: 250 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 314 Color: 1
Size: 295 Color: 0

Total size: 40000
Total free space: 0

