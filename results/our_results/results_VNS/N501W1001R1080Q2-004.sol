Capicity Bin: 1001
Lower Bound: 227

Bins used: 230
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 569 Color: 0
Size: 270 Color: 1
Size: 162 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 620 Color: 0
Size: 224 Color: 0
Size: 157 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 143 Color: 0
Size: 140 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 0
Size: 320 Color: 1
Size: 194 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 546 Color: 0
Size: 271 Color: 1
Size: 184 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 601 Color: 0
Size: 224 Color: 0
Size: 176 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 711 Color: 1
Size: 170 Color: 1
Size: 120 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 0
Size: 144 Color: 1
Size: 142 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 634 Color: 0
Size: 227 Color: 1
Size: 140 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 535 Color: 1
Size: 251 Color: 0
Size: 215 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 0
Size: 174 Color: 1
Size: 151 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 557 Color: 0
Size: 269 Color: 1
Size: 175 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 590 Color: 0
Size: 221 Color: 0
Size: 190 Color: 1

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 0
Size: 311 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 1
Size: 155 Color: 0
Size: 124 Color: 0

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 1
Size: 494 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 347 Color: 0
Size: 294 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 328 Color: 1
Size: 253 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 316 Color: 1
Size: 273 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 531 Color: 1
Size: 353 Color: 0
Size: 117 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 532 Color: 0
Size: 369 Color: 0
Size: 100 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 525 Color: 0
Size: 354 Color: 1
Size: 122 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 524 Color: 1
Size: 346 Color: 0
Size: 131 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 530 Color: 1
Size: 340 Color: 1
Size: 131 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 516 Color: 1
Size: 335 Color: 0
Size: 150 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 0
Size: 342 Color: 1
Size: 104 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 507 Color: 1
Size: 289 Color: 0
Size: 205 Color: 0

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 1
Size: 466 Color: 0

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 1
Size: 468 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 319 Color: 1
Size: 212 Color: 0

Bin 31: 0 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 1
Size: 474 Color: 0

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 486 Color: 0

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 0
Size: 484 Color: 1

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 483 Color: 1

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 0
Size: 481 Color: 1

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 0
Size: 478 Color: 1

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 1

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 470 Color: 1

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 462 Color: 1

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 0
Size: 461 Color: 1

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 1
Size: 451 Color: 0

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 1

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 443 Color: 1

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 1
Size: 440 Color: 0

Bin 45: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 439 Color: 0

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 0
Size: 437 Color: 1

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 1
Size: 422 Color: 0

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 417 Color: 1

Bin 49: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 1

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 1
Size: 406 Color: 0

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 1
Size: 403 Color: 0

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 0
Size: 403 Color: 1

Bin 53: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 0
Size: 395 Color: 1

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 394 Color: 0

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 0

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 1
Size: 393 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 611 Color: 1
Size: 224 Color: 0
Size: 166 Color: 0

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 0
Size: 387 Color: 1

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 0
Size: 386 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 0
Size: 194 Color: 0
Size: 189 Color: 1

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 1
Size: 382 Color: 0

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 1
Size: 379 Color: 0

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 0
Size: 380 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 635 Color: 0
Size: 186 Color: 1
Size: 180 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 0
Size: 352 Color: 1

Bin 67: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 1
Size: 344 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 655 Color: 0
Size: 177 Color: 0
Size: 169 Color: 1

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 344 Color: 1

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 342 Color: 0

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 1
Size: 340 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 0
Size: 332 Color: 1

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 1

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 1
Size: 317 Color: 0

Bin 75: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 1
Size: 316 Color: 0

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 0
Size: 314 Color: 1

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 1
Size: 312 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 699 Color: 1
Size: 171 Color: 1
Size: 131 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 703 Color: 0
Size: 162 Color: 0
Size: 136 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 1

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 296 Color: 0

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 284 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 718 Color: 1
Size: 153 Color: 0
Size: 130 Color: 0

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 0
Size: 284 Color: 1

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 1
Size: 282 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 0
Size: 143 Color: 1
Size: 130 Color: 0

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 0
Size: 270 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 0
Size: 134 Color: 1
Size: 133 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 735 Color: 0
Size: 135 Color: 1
Size: 131 Color: 0

Bin 90: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 259 Color: 1

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 258 Color: 0

Bin 92: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 0
Size: 256 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 1
Size: 134 Color: 0
Size: 118 Color: 0

Bin 94: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 1
Size: 246 Color: 0

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 1

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 1
Size: 242 Color: 0

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 0
Size: 240 Color: 1

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 1
Size: 228 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 772 Color: 0
Size: 126 Color: 1
Size: 103 Color: 0

Bin 100: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 220 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 0
Size: 112 Color: 1
Size: 107 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 784 Color: 0
Size: 116 Color: 1
Size: 101 Color: 0

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 0
Size: 216 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 0
Size: 102 Color: 1
Size: 101 Color: 0

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 464 Color: 0

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 1
Size: 474 Color: 0

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 0
Size: 495 Color: 1

Bin 108: 1 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 0
Size: 465 Color: 1

Bin 109: 1 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 463 Color: 1

Bin 110: 1 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 0
Size: 458 Color: 1

Bin 111: 1 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 441 Color: 0

Bin 112: 1 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 387 Color: 1

Bin 113: 1 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 1
Size: 380 Color: 0

Bin 114: 1 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 1
Size: 377 Color: 0

Bin 115: 1 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 1
Size: 366 Color: 0

Bin 116: 1 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 1
Size: 362 Color: 0

Bin 117: 1 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 1
Size: 359 Color: 0

Bin 118: 1 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 343 Color: 1

Bin 119: 1 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 335 Color: 0

Bin 120: 1 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 0
Size: 175 Color: 0
Size: 161 Color: 1

Bin 121: 1 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 318 Color: 0

Bin 122: 1 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 310 Color: 0

Bin 123: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 0
Size: 299 Color: 1

Bin 124: 1 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 0
Size: 290 Color: 1

Bin 125: 1 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 0
Size: 281 Color: 1

Bin 126: 1 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 279 Color: 1

Bin 127: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 276 Color: 1

Bin 128: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 276 Color: 1

Bin 129: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 1
Size: 276 Color: 0

Bin 130: 1 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 1
Size: 274 Color: 0

Bin 131: 1 of cap free
Amount of items: 2
Items: 
Size: 728 Color: 0
Size: 272 Color: 1

Bin 132: 1 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 271 Color: 0

Bin 133: 1 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 271 Color: 0

Bin 134: 1 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 0
Size: 266 Color: 1

Bin 135: 1 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 256 Color: 1

Bin 136: 1 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 1
Size: 256 Color: 0

Bin 137: 1 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 251 Color: 1

Bin 138: 1 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 0
Size: 234 Color: 1

Bin 139: 1 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 0
Size: 231 Color: 1

Bin 140: 1 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 0
Size: 225 Color: 1

Bin 141: 1 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 1
Size: 219 Color: 0

Bin 142: 1 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 1
Size: 200 Color: 0

Bin 143: 1 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 0
Size: 442 Color: 1

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 1
Size: 347 Color: 0
Size: 304 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 0
Size: 349 Color: 1
Size: 151 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 345 Color: 1
Size: 157 Color: 0

Bin 147: 2 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 1
Size: 467 Color: 0

Bin 148: 2 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 0
Size: 495 Color: 1

Bin 149: 2 of cap free
Amount of items: 3
Items: 
Size: 582 Color: 1
Size: 218 Color: 0
Size: 199 Color: 0

Bin 150: 2 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 1
Size: 233 Color: 0
Size: 169 Color: 0

Bin 151: 2 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 0
Size: 394 Color: 1

Bin 152: 2 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 0
Size: 374 Color: 1

Bin 153: 2 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 1
Size: 327 Color: 0

Bin 154: 2 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 0
Size: 281 Color: 1

Bin 155: 2 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 266 Color: 0

Bin 156: 2 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 1
Size: 215 Color: 0

Bin 157: 2 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 0
Size: 213 Color: 1

Bin 158: 2 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 0
Size: 408 Color: 1

Bin 159: 2 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 1
Size: 204 Color: 0

Bin 160: 3 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 1
Size: 476 Color: 0

Bin 161: 3 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 1
Size: 459 Color: 0

Bin 162: 3 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 1
Size: 482 Color: 0

Bin 163: 3 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 1
Size: 454 Color: 0

Bin 164: 3 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 425 Color: 0

Bin 165: 3 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 0
Size: 423 Color: 1

Bin 166: 3 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 0
Size: 399 Color: 1

Bin 167: 3 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 1
Size: 385 Color: 0

Bin 168: 3 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 300 Color: 0

Bin 169: 3 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 1
Size: 285 Color: 0

Bin 170: 3 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 0
Size: 265 Color: 1

Bin 171: 3 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 1
Size: 261 Color: 0

Bin 172: 3 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 0
Size: 250 Color: 1

Bin 173: 3 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 247 Color: 0

Bin 174: 3 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 1
Size: 242 Color: 0

Bin 175: 3 of cap free
Amount of items: 2
Items: 
Size: 778 Color: 0
Size: 220 Color: 1

Bin 176: 4 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 339 Color: 0

Bin 177: 4 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 0
Size: 276 Color: 1

Bin 178: 4 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 0
Size: 255 Color: 1

Bin 179: 4 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 1
Size: 207 Color: 0

Bin 180: 4 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 199 Color: 1

Bin 181: 5 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 0
Size: 317 Color: 1

Bin 182: 5 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 310 Color: 1
Size: 198 Color: 0

Bin 183: 5 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 0
Size: 493 Color: 1

Bin 184: 5 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 0
Size: 416 Color: 1

Bin 185: 5 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 394 Color: 0

Bin 186: 6 of cap free
Amount of items: 2
Items: 
Size: 568 Color: 0
Size: 427 Color: 1

Bin 187: 6 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 0
Size: 398 Color: 1

Bin 188: 6 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 0
Size: 197 Color: 1

Bin 189: 7 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 0
Size: 492 Color: 1

Bin 190: 7 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 437 Color: 1

Bin 191: 7 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 385 Color: 1

Bin 192: 8 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 337 Color: 0

Bin 193: 9 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 433 Color: 0

Bin 194: 9 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 384 Color: 1

Bin 195: 10 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 432 Color: 0

Bin 196: 10 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 287 Color: 1

Bin 197: 10 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 1
Size: 255 Color: 0

Bin 198: 10 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 0
Size: 457 Color: 1

Bin 199: 13 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 0
Size: 455 Color: 1

Bin 200: 13 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 1
Size: 376 Color: 0

Bin 201: 13 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 0
Size: 358 Color: 1

Bin 202: 14 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 236 Color: 0

Bin 203: 14 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 247 Color: 1

Bin 204: 17 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 1
Size: 373 Color: 0

Bin 205: 21 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 0
Size: 449 Color: 1

Bin 206: 23 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 0
Size: 196 Color: 1

Bin 207: 28 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 0
Size: 276 Color: 1

Bin 208: 28 of cap free
Amount of items: 2
Items: 
Size: 566 Color: 0
Size: 407 Color: 1

Bin 209: 28 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 0
Size: 354 Color: 1

Bin 210: 29 of cap free
Amount of items: 2
Items: 
Size: 696 Color: 0
Size: 276 Color: 1

Bin 211: 37 of cap free
Amount of items: 2
Items: 
Size: 543 Color: 1
Size: 421 Color: 0

Bin 212: 38 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 0
Size: 191 Color: 1

Bin 213: 39 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 0
Size: 406 Color: 1

Bin 214: 40 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 0
Size: 405 Color: 1

Bin 215: 42 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 264 Color: 1

Bin 216: 48 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 0
Size: 185 Color: 1

Bin 217: 56 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 352 Color: 0

Bin 218: 67 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 0
Size: 405 Color: 1

Bin 219: 74 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 0
Size: 398 Color: 1

Bin 220: 81 of cap free
Amount of items: 2
Items: 
Size: 757 Color: 0
Size: 163 Color: 1

Bin 221: 121 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 0
Size: 354 Color: 1

Bin 222: 132 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 354 Color: 1

Bin 223: 150 of cap free
Amount of items: 2
Items: 
Size: 498 Color: 0
Size: 353 Color: 1

Bin 224: 217 of cap free
Amount of items: 1
Items: 
Size: 784 Color: 1

Bin 225: 230 of cap free
Amount of items: 1
Items: 
Size: 771 Color: 1

Bin 226: 232 of cap free
Amount of items: 1
Items: 
Size: 769 Color: 1

Bin 227: 261 of cap free
Amount of items: 1
Items: 
Size: 740 Color: 0

Bin 228: 283 of cap free
Amount of items: 1
Items: 
Size: 718 Color: 1

Bin 229: 306 of cap free
Amount of items: 1
Items: 
Size: 695 Color: 0

Bin 230: 335 of cap free
Amount of items: 1
Items: 
Size: 666 Color: 0

Total size: 226934
Total free space: 3296

