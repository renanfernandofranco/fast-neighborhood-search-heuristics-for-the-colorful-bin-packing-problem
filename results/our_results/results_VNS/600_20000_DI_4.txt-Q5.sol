Capicity Bin: 16432
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 19
Items: 
Size: 1368 Color: 0
Size: 1144 Color: 1
Size: 1136 Color: 1
Size: 1052 Color: 1
Size: 1036 Color: 4
Size: 984 Color: 4
Size: 984 Color: 1
Size: 908 Color: 4
Size: 864 Color: 3
Size: 848 Color: 3
Size: 848 Color: 3
Size: 816 Color: 2
Size: 800 Color: 2
Size: 736 Color: 0
Size: 668 Color: 2
Size: 656 Color: 0
Size: 608 Color: 2
Size: 544 Color: 1
Size: 432 Color: 2

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8236 Color: 1
Size: 3607 Color: 2
Size: 2597 Color: 3
Size: 1344 Color: 0
Size: 648 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8232 Color: 0
Size: 6840 Color: 3
Size: 1360 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9253 Color: 0
Size: 5983 Color: 3
Size: 1196 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10210 Color: 2
Size: 5182 Color: 0
Size: 1040 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11000 Color: 4
Size: 5144 Color: 4
Size: 288 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 1
Size: 3724 Color: 2
Size: 604 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 0
Size: 3582 Color: 4
Size: 414 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 2
Size: 3748 Color: 4
Size: 184 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 2
Size: 3586 Color: 1
Size: 298 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 4
Size: 3222 Color: 1
Size: 272 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 4
Size: 2512 Color: 2
Size: 908 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13073 Color: 2
Size: 2801 Color: 1
Size: 558 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 0
Size: 2734 Color: 2
Size: 544 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 2
Size: 2568 Color: 1
Size: 496 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 2
Size: 2510 Color: 1
Size: 518 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13420 Color: 1
Size: 2556 Color: 2
Size: 456 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 1
Size: 1992 Color: 4
Size: 976 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13503 Color: 1
Size: 2501 Color: 0
Size: 428 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13487 Color: 0
Size: 2781 Color: 0
Size: 164 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 2
Size: 2532 Color: 4
Size: 256 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13699 Color: 2
Size: 2317 Color: 0
Size: 416 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 4
Size: 2524 Color: 3
Size: 208 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13734 Color: 4
Size: 1498 Color: 3
Size: 1200 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13826 Color: 2
Size: 2216 Color: 4
Size: 390 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 4
Size: 2284 Color: 3
Size: 312 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13861 Color: 3
Size: 2303 Color: 1
Size: 268 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 2174 Color: 1
Size: 330 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 4
Size: 1876 Color: 3
Size: 568 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 2088 Color: 0
Size: 288 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 4
Size: 2043 Color: 1
Size: 316 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 4
Size: 1572 Color: 1
Size: 720 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14221 Color: 4
Size: 1843 Color: 3
Size: 368 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 3
Size: 1964 Color: 0
Size: 300 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 2
Size: 1528 Color: 2
Size: 716 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14267 Color: 4
Size: 1653 Color: 1
Size: 512 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 1
Size: 1368 Color: 0
Size: 800 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 4
Size: 1642 Color: 0
Size: 400 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14408 Color: 0
Size: 1024 Color: 4
Size: 1000 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 0
Size: 1484 Color: 4
Size: 496 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 3
Size: 1070 Color: 0
Size: 908 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 2
Size: 1448 Color: 2
Size: 528 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14485 Color: 1
Size: 1623 Color: 0
Size: 324 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14500 Color: 3
Size: 1656 Color: 4
Size: 276 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 0
Size: 1570 Color: 1
Size: 344 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 1
Size: 1360 Color: 4
Size: 504 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14605 Color: 4
Size: 1491 Color: 1
Size: 336 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 2
Size: 1442 Color: 0
Size: 378 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14622 Color: 1
Size: 976 Color: 3
Size: 834 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 3
Size: 1056 Color: 4
Size: 738 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14654 Color: 1
Size: 1502 Color: 2
Size: 276 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 0
Size: 1468 Color: 1
Size: 288 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 4
Size: 1214 Color: 3
Size: 516 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14762 Color: 2
Size: 1374 Color: 1
Size: 296 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14786 Color: 1
Size: 1198 Color: 2
Size: 448 Color: 4

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10982 Color: 4
Size: 3252 Color: 0
Size: 2197 Color: 3

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 11972 Color: 4
Size: 4131 Color: 2
Size: 328 Color: 1

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 0
Size: 2884 Color: 0
Size: 1054 Color: 4

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 3
Size: 3283 Color: 0
Size: 644 Color: 1

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 3
Size: 2813 Color: 3
Size: 656 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 4
Size: 3027 Color: 2
Size: 376 Color: 0

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13113 Color: 2
Size: 2922 Color: 1
Size: 396 Color: 4

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13317 Color: 1
Size: 2746 Color: 4
Size: 368 Color: 2

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13517 Color: 1
Size: 1922 Color: 0
Size: 992 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 0
Size: 2098 Color: 4
Size: 488 Color: 2

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 0
Size: 2075 Color: 1
Size: 438 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 1
Size: 1382 Color: 0
Size: 1024 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 14385 Color: 1
Size: 1702 Color: 4
Size: 344 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 0
Size: 1524 Color: 4
Size: 500 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 14663 Color: 4
Size: 1056 Color: 3
Size: 712 Color: 2

Bin 71: 2 of cap free
Amount of items: 7
Items: 
Size: 8220 Color: 0
Size: 2037 Color: 4
Size: 2005 Color: 0
Size: 1280 Color: 4
Size: 1032 Color: 2
Size: 992 Color: 2
Size: 864 Color: 1

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10974 Color: 0
Size: 5012 Color: 4
Size: 444 Color: 4

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11622 Color: 3
Size: 4056 Color: 3
Size: 752 Color: 4

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 4
Size: 4022 Color: 3
Size: 192 Color: 1

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 3
Size: 3528 Color: 1
Size: 336 Color: 2

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 12856 Color: 0
Size: 1890 Color: 3
Size: 1684 Color: 0

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 1
Size: 3244 Color: 2
Size: 240 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 2
Size: 1614 Color: 0
Size: 1394 Color: 0

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 1
Size: 1650 Color: 1
Size: 704 Color: 4

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 3
Size: 1668 Color: 4
Size: 452 Color: 2

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 10821 Color: 0
Size: 5288 Color: 4
Size: 320 Color: 2

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 3
Size: 4177 Color: 2
Size: 352 Color: 1

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 1
Size: 3733 Color: 0
Size: 496 Color: 2

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 12419 Color: 4
Size: 4010 Color: 3

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 12803 Color: 4
Size: 3218 Color: 2
Size: 408 Color: 1

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 14582 Color: 3
Size: 1741 Color: 0
Size: 106 Color: 2

Bin 87: 4 of cap free
Amount of items: 7
Items: 
Size: 8219 Color: 3
Size: 1982 Color: 0
Size: 1598 Color: 3
Size: 1511 Color: 4
Size: 1510 Color: 1
Size: 950 Color: 2
Size: 658 Color: 2

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 9306 Color: 2
Size: 5938 Color: 4
Size: 1184 Color: 0

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 2
Size: 6808 Color: 0
Size: 304 Color: 4

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 2
Size: 6612 Color: 3
Size: 480 Color: 0

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 3
Size: 6008 Color: 4

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 11610 Color: 1
Size: 4530 Color: 3
Size: 288 Color: 2

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 1
Size: 3721 Color: 2
Size: 850 Color: 0

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 4
Size: 2488 Color: 3

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 3
Size: 2016 Color: 0

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14630 Color: 0
Size: 1798 Color: 2

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 11325 Color: 2
Size: 4550 Color: 1
Size: 552 Color: 0

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 11421 Color: 3
Size: 4542 Color: 0
Size: 464 Color: 1

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 3
Size: 2431 Color: 3
Size: 340 Color: 2

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 0
Size: 1689 Color: 2
Size: 42 Color: 2

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 10516 Color: 0
Size: 5168 Color: 3
Size: 742 Color: 2

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 10484 Color: 2
Size: 5942 Color: 1

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 1
Size: 5032 Color: 4
Size: 746 Color: 4

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13138 Color: 4
Size: 3288 Color: 0

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 13470 Color: 0
Size: 2844 Color: 4
Size: 112 Color: 1

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13910 Color: 0
Size: 2516 Color: 1

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14621 Color: 2
Size: 1805 Color: 1

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14774 Color: 1
Size: 1652 Color: 0

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 9224 Color: 4
Size: 6845 Color: 3
Size: 356 Color: 1

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 10101 Color: 4
Size: 5740 Color: 0
Size: 584 Color: 1

Bin 111: 7 of cap free
Amount of items: 3
Items: 
Size: 12477 Color: 3
Size: 3528 Color: 1
Size: 420 Color: 4

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 10532 Color: 1
Size: 4932 Color: 0
Size: 960 Color: 4

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 0
Size: 3284 Color: 2
Size: 224 Color: 1

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13983 Color: 0
Size: 2441 Color: 3

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 0
Size: 2084 Color: 3

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13669 Color: 4
Size: 2754 Color: 0

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 14263 Color: 0
Size: 2160 Color: 2

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 14321 Color: 3
Size: 2102 Color: 0

Bin 119: 10 of cap free
Amount of items: 4
Items: 
Size: 10218 Color: 0
Size: 4284 Color: 1
Size: 1612 Color: 3
Size: 308 Color: 2

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 3
Size: 2888 Color: 4

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 14153 Color: 1
Size: 2269 Color: 3

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 14436 Color: 4
Size: 1986 Color: 1

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 13333 Color: 1
Size: 3088 Color: 4

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 0
Size: 2143 Color: 3

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 0
Size: 1761 Color: 1

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 4
Size: 5684 Color: 0
Size: 328 Color: 2

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 12105 Color: 2
Size: 3979 Color: 3
Size: 336 Color: 1

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 0
Size: 2778 Color: 4

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 4
Size: 2604 Color: 3

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 0
Size: 2250 Color: 2

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 14466 Color: 1
Size: 1953 Color: 2

Bin 132: 13 of cap free
Amount of items: 3
Items: 
Size: 14643 Color: 2
Size: 1748 Color: 0
Size: 28 Color: 1

Bin 133: 15 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 4
Size: 6844 Color: 0
Size: 432 Color: 2

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 3
Size: 3345 Color: 0

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 2
Size: 2328 Color: 3

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 3
Size: 7280 Color: 1
Size: 392 Color: 2

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 4
Size: 3960 Color: 0

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 14600 Color: 1
Size: 1816 Color: 0

Bin 139: 18 of cap free
Amount of items: 3
Items: 
Size: 10725 Color: 1
Size: 4757 Color: 2
Size: 932 Color: 0

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 10005 Color: 4
Size: 6408 Color: 1

Bin 141: 19 of cap free
Amount of items: 4
Items: 
Size: 14557 Color: 3
Size: 1688 Color: 1
Size: 126 Color: 4
Size: 42 Color: 3

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 11292 Color: 4
Size: 4888 Color: 0
Size: 232 Color: 2

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 11953 Color: 2
Size: 2852 Color: 0
Size: 1607 Color: 1

Bin 144: 20 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 2
Size: 2906 Color: 1
Size: 762 Color: 0

Bin 145: 22 of cap free
Amount of items: 2
Items: 
Size: 11224 Color: 2
Size: 5186 Color: 1

Bin 146: 23 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 2
Size: 3608 Color: 4

Bin 147: 24 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 1
Size: 1770 Color: 4
Size: 64 Color: 0

Bin 148: 25 of cap free
Amount of items: 3
Items: 
Size: 9237 Color: 1
Size: 6846 Color: 3
Size: 324 Color: 0

Bin 149: 32 of cap free
Amount of items: 2
Items: 
Size: 11576 Color: 3
Size: 4824 Color: 4

Bin 150: 33 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 2
Size: 1901 Color: 3

Bin 151: 34 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 3
Size: 4260 Color: 2

Bin 152: 34 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 1
Size: 2232 Color: 0

Bin 153: 35 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 0
Size: 3025 Color: 4

Bin 154: 36 of cap free
Amount of items: 3
Items: 
Size: 11236 Color: 4
Size: 4920 Color: 1
Size: 240 Color: 0

Bin 155: 39 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 3
Size: 5997 Color: 4
Size: 784 Color: 0

Bin 156: 41 of cap free
Amount of items: 2
Items: 
Size: 13711 Color: 1
Size: 2680 Color: 3

Bin 157: 43 of cap free
Amount of items: 2
Items: 
Size: 13057 Color: 4
Size: 3332 Color: 0

Bin 158: 43 of cap free
Amount of items: 2
Items: 
Size: 13797 Color: 3
Size: 2592 Color: 4

Bin 159: 45 of cap free
Amount of items: 2
Items: 
Size: 12130 Color: 3
Size: 4257 Color: 2

Bin 160: 45 of cap free
Amount of items: 2
Items: 
Size: 12574 Color: 2
Size: 3813 Color: 0

Bin 161: 48 of cap free
Amount of items: 2
Items: 
Size: 9548 Color: 1
Size: 6836 Color: 2

Bin 162: 48 of cap free
Amount of items: 2
Items: 
Size: 14058 Color: 2
Size: 2326 Color: 0

Bin 163: 49 of cap free
Amount of items: 3
Items: 
Size: 10536 Color: 0
Size: 4663 Color: 3
Size: 1184 Color: 2

Bin 164: 51 of cap free
Amount of items: 3
Items: 
Size: 14548 Color: 3
Size: 1721 Color: 4
Size: 112 Color: 3

Bin 165: 57 of cap free
Amount of items: 6
Items: 
Size: 8217 Color: 1
Size: 2157 Color: 3
Size: 2044 Color: 0
Size: 2041 Color: 0
Size: 1368 Color: 2
Size: 548 Color: 1

Bin 166: 58 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 0
Size: 2324 Color: 1

Bin 167: 60 of cap free
Amount of items: 2
Items: 
Size: 13440 Color: 0
Size: 2932 Color: 1

Bin 168: 64 of cap free
Amount of items: 29
Items: 
Size: 824 Color: 3
Size: 800 Color: 3
Size: 744 Color: 3
Size: 720 Color: 4
Size: 688 Color: 1
Size: 664 Color: 1
Size: 648 Color: 1
Size: 640 Color: 4
Size: 640 Color: 1
Size: 640 Color: 1
Size: 604 Color: 0
Size: 580 Color: 2
Size: 580 Color: 2
Size: 580 Color: 0
Size: 576 Color: 3
Size: 568 Color: 0
Size: 562 Color: 0
Size: 560 Color: 3
Size: 504 Color: 1
Size: 484 Color: 2
Size: 480 Color: 3
Size: 480 Color: 1
Size: 464 Color: 2
Size: 460 Color: 2
Size: 430 Color: 4
Size: 392 Color: 2
Size: 384 Color: 4
Size: 352 Color: 0
Size: 320 Color: 0

Bin 169: 65 of cap free
Amount of items: 2
Items: 
Size: 13784 Color: 3
Size: 2583 Color: 4

Bin 170: 71 of cap free
Amount of items: 2
Items: 
Size: 13943 Color: 3
Size: 2418 Color: 0

Bin 171: 79 of cap free
Amount of items: 2
Items: 
Size: 12009 Color: 4
Size: 4344 Color: 2

Bin 172: 80 of cap free
Amount of items: 2
Items: 
Size: 13438 Color: 2
Size: 2914 Color: 4

Bin 173: 82 of cap free
Amount of items: 2
Items: 
Size: 8222 Color: 3
Size: 8128 Color: 1

Bin 174: 82 of cap free
Amount of items: 2
Items: 
Size: 11477 Color: 1
Size: 4873 Color: 4

Bin 175: 84 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 0
Size: 4558 Color: 4
Size: 1550 Color: 1

Bin 176: 86 of cap free
Amount of items: 2
Items: 
Size: 14398 Color: 0
Size: 1948 Color: 1

Bin 177: 87 of cap free
Amount of items: 2
Items: 
Size: 14449 Color: 1
Size: 1896 Color: 3

Bin 178: 91 of cap free
Amount of items: 3
Items: 
Size: 11688 Color: 1
Size: 2767 Color: 2
Size: 1886 Color: 3

Bin 179: 102 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 3
Size: 3014 Color: 4

Bin 180: 120 of cap free
Amount of items: 2
Items: 
Size: 12532 Color: 2
Size: 3780 Color: 4

Bin 181: 126 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 3
Size: 6842 Color: 0
Size: 1184 Color: 2

Bin 182: 128 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 4
Size: 3080 Color: 3

Bin 183: 132 of cap free
Amount of items: 2
Items: 
Size: 11336 Color: 4
Size: 4964 Color: 1

Bin 184: 132 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 2
Size: 3320 Color: 4

Bin 185: 157 of cap free
Amount of items: 2
Items: 
Size: 10998 Color: 4
Size: 5277 Color: 1

Bin 186: 160 of cap free
Amount of items: 2
Items: 
Size: 11940 Color: 3
Size: 4332 Color: 0

Bin 187: 167 of cap free
Amount of items: 2
Items: 
Size: 12968 Color: 4
Size: 3297 Color: 3

Bin 188: 178 of cap free
Amount of items: 3
Items: 
Size: 9310 Color: 1
Size: 6040 Color: 3
Size: 904 Color: 3

Bin 189: 182 of cap free
Amount of items: 2
Items: 
Size: 12930 Color: 4
Size: 3320 Color: 2

Bin 190: 184 of cap free
Amount of items: 2
Items: 
Size: 11324 Color: 4
Size: 4924 Color: 1

Bin 191: 203 of cap free
Amount of items: 2
Items: 
Size: 10966 Color: 4
Size: 5263 Color: 2

Bin 192: 215 of cap free
Amount of items: 2
Items: 
Size: 11969 Color: 0
Size: 4248 Color: 4

Bin 193: 236 of cap free
Amount of items: 2
Items: 
Size: 10264 Color: 3
Size: 5932 Color: 1

Bin 194: 238 of cap free
Amount of items: 2
Items: 
Size: 10117 Color: 1
Size: 6077 Color: 4

Bin 195: 238 of cap free
Amount of items: 2
Items: 
Size: 10837 Color: 3
Size: 5357 Color: 4

Bin 196: 256 of cap free
Amount of items: 7
Items: 
Size: 8218 Color: 0
Size: 1560 Color: 3
Size: 1542 Color: 3
Size: 1368 Color: 1
Size: 1368 Color: 0
Size: 1264 Color: 1
Size: 856 Color: 2

Bin 197: 261 of cap free
Amount of items: 2
Items: 
Size: 9324 Color: 2
Size: 6847 Color: 4

Bin 198: 264 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 3
Size: 6064 Color: 2

Bin 199: 10348 of cap free
Amount of items: 17
Items: 
Size: 448 Color: 3
Size: 448 Color: 1
Size: 416 Color: 3
Size: 408 Color: 3
Size: 384 Color: 2
Size: 376 Color: 2
Size: 370 Color: 4
Size: 368 Color: 4
Size: 360 Color: 4
Size: 350 Color: 2
Size: 328 Color: 2
Size: 320 Color: 4
Size: 308 Color: 0
Size: 304 Color: 0
Size: 300 Color: 0
Size: 300 Color: 0
Size: 296 Color: 1

Total size: 3253536
Total free space: 16432

