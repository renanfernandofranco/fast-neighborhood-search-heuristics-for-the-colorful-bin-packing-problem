Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 476
Size: 274 Color: 134
Size: 252 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 433
Size: 307 Color: 229
Size: 259 Color: 67

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 455
Size: 282 Color: 162
Size: 269 Color: 109

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 427
Size: 315 Color: 247
Size: 256 Color: 44

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 424
Size: 323 Color: 271
Size: 253 Color: 25

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 379
Size: 317 Color: 252
Size: 297 Color: 206

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 460
Size: 287 Color: 174
Size: 260 Color: 71

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 430
Size: 299 Color: 209
Size: 269 Color: 107

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 432
Size: 250 Color: 9
Size: 318 Color: 256

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 360
Size: 321 Color: 265
Size: 307 Color: 227

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 461
Size: 291 Color: 188
Size: 253 Color: 24

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 355
Size: 336 Color: 300
Size: 296 Color: 204

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 472
Size: 269 Color: 111
Size: 262 Color: 80

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 434
Size: 316 Color: 250
Size: 250 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 437
Size: 286 Color: 170
Size: 278 Color: 152

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 400
Size: 324 Color: 274
Size: 274 Color: 131

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 388
Size: 333 Color: 297
Size: 275 Color: 138

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 471
Size: 271 Color: 120
Size: 261 Color: 74

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 336
Size: 323 Color: 272
Size: 322 Color: 268

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 350
Size: 350 Color: 325
Size: 286 Color: 173

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 411
Size: 327 Color: 282
Size: 260 Color: 70

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 416
Size: 331 Color: 293
Size: 253 Color: 29

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 412
Size: 295 Color: 199
Size: 291 Color: 184

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 447
Size: 294 Color: 195
Size: 263 Color: 88

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 397
Size: 319 Color: 259
Size: 280 Color: 155

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 420
Size: 309 Color: 236
Size: 273 Color: 127

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 450
Size: 282 Color: 161
Size: 273 Color: 128

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 351
Size: 338 Color: 304
Size: 297 Color: 208

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 386
Size: 317 Color: 251
Size: 293 Color: 193

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 445
Size: 293 Color: 191
Size: 265 Color: 98

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 409
Size: 306 Color: 222
Size: 283 Color: 164

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 358
Size: 323 Color: 273
Size: 307 Color: 225

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 376
Size: 339 Color: 306
Size: 277 Color: 148

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 375
Size: 349 Color: 324
Size: 268 Color: 104

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 490
Size: 258 Color: 61
Size: 254 Color: 31

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 452
Size: 293 Color: 192
Size: 261 Color: 72

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 494
Size: 258 Color: 56
Size: 250 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 387
Size: 322 Color: 269
Size: 287 Color: 175

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 359
Size: 370 Color: 357
Size: 258 Color: 58

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 479
Size: 267 Color: 101
Size: 256 Color: 49

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 428
Size: 313 Color: 242
Size: 257 Color: 53

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 390
Size: 314 Color: 246
Size: 292 Color: 189

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 488
Size: 261 Color: 78
Size: 252 Color: 20

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 393
Size: 330 Color: 288
Size: 274 Color: 132

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 313
Size: 342 Color: 312
Size: 317 Color: 253

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 458
Size: 291 Color: 185
Size: 258 Color: 60

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 362
Size: 327 Color: 283
Size: 300 Color: 213

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 346
Size: 361 Color: 344
Size: 277 Color: 147

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 483
Size: 267 Color: 102
Size: 251 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 439
Size: 287 Color: 176
Size: 274 Color: 130

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 398
Size: 330 Color: 291
Size: 269 Color: 113

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 486
Size: 261 Color: 75
Size: 254 Color: 30

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 414
Size: 329 Color: 287
Size: 256 Color: 47

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 496
Size: 255 Color: 37
Size: 251 Color: 16

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 491
Size: 262 Color: 82
Size: 250 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 338
Size: 356 Color: 335
Size: 288 Color: 177

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 381
Size: 338 Color: 303
Size: 274 Color: 133

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 429
Size: 288 Color: 178
Size: 280 Color: 154

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 415
Size: 332 Color: 295
Size: 253 Color: 27

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 384
Size: 321 Color: 262
Size: 289 Color: 180

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 478
Size: 270 Color: 118
Size: 256 Color: 46

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 391
Size: 310 Color: 237
Size: 296 Color: 200

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 464
Size: 277 Color: 146
Size: 261 Color: 73

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 423
Size: 319 Color: 260
Size: 257 Color: 51

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 356
Size: 325 Color: 278
Size: 306 Color: 223

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 405
Size: 326 Color: 279
Size: 269 Color: 108

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 498
Size: 253 Color: 28
Size: 250 Color: 5

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 413
Size: 328 Color: 285
Size: 257 Color: 52

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 417
Size: 334 Color: 299
Size: 250 Color: 11

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 321
Size: 345 Color: 318
Size: 309 Color: 233

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 368
Size: 323 Color: 270
Size: 299 Color: 211

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 378
Size: 337 Color: 302
Size: 278 Color: 150

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 493
Size: 259 Color: 62
Size: 250 Color: 7

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 373
Size: 348 Color: 322
Size: 269 Color: 112

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 399
Size: 324 Color: 276
Size: 274 Color: 136

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 389
Size: 333 Color: 298
Size: 275 Color: 139

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 309
Size: 340 Color: 308
Size: 321 Color: 264

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 369
Size: 312 Color: 240
Size: 310 Color: 238

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 499
Size: 250 Color: 6
Size: 250 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 406
Size: 328 Color: 286
Size: 263 Color: 83

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 402
Size: 325 Color: 277
Size: 271 Color: 123

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 441
Size: 307 Color: 230
Size: 252 Color: 17

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 349
Size: 348 Color: 323
Size: 289 Color: 181

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 372
Size: 362 Color: 345
Size: 258 Color: 59

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 470
Size: 272 Color: 124
Size: 260 Color: 69

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 474
Size: 275 Color: 142
Size: 255 Color: 40

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 448
Size: 303 Color: 218
Size: 253 Color: 23

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 425
Size: 313 Color: 244
Size: 259 Color: 65

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 480
Size: 271 Color: 122
Size: 252 Color: 21

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 419
Size: 296 Color: 201
Size: 286 Color: 172

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 385
Size: 326 Color: 281
Size: 284 Color: 166

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 462
Size: 284 Color: 169
Size: 257 Color: 54

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 334
Size: 354 Color: 333
Size: 293 Color: 194

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 364
Size: 326 Color: 280
Size: 300 Color: 215

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 453
Size: 289 Color: 179
Size: 265 Color: 94

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 401
Size: 318 Color: 257
Size: 279 Color: 153

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 457
Size: 297 Color: 207
Size: 254 Color: 35

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 331
Size: 354 Color: 330
Size: 293 Color: 190

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 468
Size: 270 Color: 114
Size: 265 Color: 96

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 407
Size: 312 Color: 241
Size: 278 Color: 149

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 442
Size: 291 Color: 187
Size: 268 Color: 105

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 371
Size: 316 Color: 249
Size: 304 Color: 219

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 347
Size: 336 Color: 301
Size: 301 Color: 217

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 435
Size: 290 Color: 183
Size: 275 Color: 141

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 367
Size: 354 Color: 332
Size: 269 Color: 110

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 422
Size: 300 Color: 214
Size: 281 Color: 156

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 404
Size: 324 Color: 275
Size: 271 Color: 121

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 454
Size: 278 Color: 151
Size: 275 Color: 143

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 395
Size: 339 Color: 307
Size: 263 Color: 86

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 466
Size: 276 Color: 144
Size: 261 Color: 79

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 392
Size: 330 Color: 290
Size: 275 Color: 140

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 446
Size: 297 Color: 205
Size: 261 Color: 76

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 348
Size: 330 Color: 289
Size: 307 Color: 231

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 477
Size: 267 Color: 99
Size: 259 Color: 68

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 497
Size: 254 Color: 36
Size: 251 Color: 13

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 482
Size: 264 Color: 92
Size: 256 Color: 48

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 410
Size: 322 Color: 267
Size: 267 Color: 100

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 473
Size: 265 Color: 97
Size: 265 Color: 95

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 449
Size: 299 Color: 210
Size: 256 Color: 45

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 408
Size: 332 Color: 294
Size: 258 Color: 57

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 469
Size: 281 Color: 159
Size: 253 Color: 26

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 370
Size: 369 Color: 354
Size: 251 Color: 14

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 485
Size: 263 Color: 84
Size: 252 Color: 19

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 443
Size: 296 Color: 202
Size: 263 Color: 87

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 484
Size: 262 Color: 81
Size: 255 Color: 38

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 374
Size: 333 Color: 296
Size: 284 Color: 168

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 320
Size: 347 Color: 319
Size: 307 Color: 226

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 396
Size: 319 Color: 258
Size: 281 Color: 158

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 341
Size: 360 Color: 340
Size: 281 Color: 157

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 327
Size: 351 Color: 326
Size: 299 Color: 212

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 363
Size: 317 Color: 255
Size: 310 Color: 239

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 481
Size: 264 Color: 93
Size: 259 Color: 63

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 421
Size: 327 Color: 284
Size: 254 Color: 33

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 394
Size: 344 Color: 316
Size: 259 Color: 64

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 456
Size: 281 Color: 160
Size: 270 Color: 115

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 365
Size: 361 Color: 343
Size: 263 Color: 90

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 459
Size: 276 Color: 145
Size: 271 Color: 119

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 366
Size: 330 Color: 292
Size: 294 Color: 197

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 418
Size: 309 Color: 235
Size: 273 Color: 129

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 403
Size: 321 Color: 263
Size: 274 Color: 135

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 377
Size: 309 Color: 234
Size: 307 Color: 228

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 465
Size: 270 Color: 117
Size: 268 Color: 106

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 383
Size: 317 Color: 254
Size: 294 Color: 196

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 440
Size: 305 Color: 220
Size: 256 Color: 43

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 431
Size: 314 Color: 245
Size: 254 Color: 34

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 337
Size: 343 Color: 314
Size: 301 Color: 216

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 495
Size: 255 Color: 41
Size: 253 Color: 22

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 380
Size: 308 Color: 232
Size: 305 Color: 221

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 426
Size: 321 Color: 266
Size: 250 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 436
Size: 307 Color: 224
Size: 258 Color: 55

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 492
Size: 255 Color: 39
Size: 254 Color: 32

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 444
Size: 286 Color: 171
Size: 272 Color: 126

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 311
Size: 340 Color: 310
Size: 320 Color: 261

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 475
Size: 263 Color: 89
Size: 263 Color: 85

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 353
Size: 366 Color: 352
Size: 268 Color: 103

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 451
Size: 284 Color: 167
Size: 270 Color: 116

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 438
Size: 291 Color: 186
Size: 272 Color: 125

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 329
Size: 352 Color: 328
Size: 296 Color: 203

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 382
Size: 316 Color: 248
Size: 295 Color: 198

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 342
Size: 358 Color: 339
Size: 282 Color: 163

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 463
Size: 284 Color: 165
Size: 256 Color: 50

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 361
Size: 338 Color: 305
Size: 289 Color: 182

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 489
Size: 263 Color: 91
Size: 250 Color: 10

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 317
Size: 344 Color: 315
Size: 313 Color: 243

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 467
Size: 275 Color: 137
Size: 261 Color: 77

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 487
Size: 259 Color: 66
Size: 255 Color: 42

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 500
Size: 250 Color: 12
Size: 250 Color: 8

Total size: 167167
Total free space: 0

