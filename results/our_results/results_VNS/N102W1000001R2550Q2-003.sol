Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454427 Color: 1
Size: 279699 Color: 1
Size: 265875 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 460946 Color: 1
Size: 277599 Color: 0
Size: 261456 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 379332 Color: 0
Size: 343066 Color: 1
Size: 277603 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 358020 Color: 1
Size: 361900 Color: 1
Size: 280081 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 415318 Color: 1
Size: 330533 Color: 0
Size: 254150 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 376124 Color: 0
Size: 341453 Color: 1
Size: 282424 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 428993 Color: 1
Size: 300101 Color: 0
Size: 270907 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 424558 Color: 0
Size: 303307 Color: 0
Size: 272136 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 465260 Color: 1
Size: 282949 Color: 0
Size: 251792 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 398207 Color: 0
Size: 325408 Color: 0
Size: 276386 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 299907 Color: 0
Size: 326289 Color: 1
Size: 373805 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 315001 Color: 1
Size: 415686 Color: 0
Size: 269314 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 332296 Color: 0
Size: 292132 Color: 0
Size: 375573 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 439832 Color: 0
Size: 293194 Color: 0
Size: 266975 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 426257 Color: 1
Size: 301704 Color: 0
Size: 272040 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 383735 Color: 1
Size: 314770 Color: 1
Size: 301496 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 450761 Color: 0
Size: 293832 Color: 1
Size: 255408 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 398729 Color: 1
Size: 311423 Color: 0
Size: 289849 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 449332 Color: 0
Size: 278858 Color: 1
Size: 271811 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 409906 Color: 1
Size: 339882 Color: 0
Size: 250213 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 397744 Color: 1
Size: 346188 Color: 1
Size: 256069 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 349979 Color: 0
Size: 315759 Color: 0
Size: 334263 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 386417 Color: 1
Size: 309172 Color: 0
Size: 304412 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 402947 Color: 1
Size: 342976 Color: 0
Size: 254078 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 406989 Color: 0
Size: 305529 Color: 1
Size: 287483 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 400599 Color: 1
Size: 344676 Color: 0
Size: 254726 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 315476 Color: 1
Size: 366964 Color: 0
Size: 317561 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 358236 Color: 1
Size: 321045 Color: 1
Size: 320720 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 432303 Color: 1
Size: 316504 Color: 1
Size: 251194 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 426981 Color: 0
Size: 319381 Color: 1
Size: 253639 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 444366 Color: 0
Size: 295241 Color: 0
Size: 260394 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 438039 Color: 1
Size: 301121 Color: 0
Size: 260841 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 443970 Color: 1
Size: 283723 Color: 0
Size: 272308 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 484786 Color: 1
Size: 259408 Color: 0
Size: 255807 Color: 0

Total size: 34000034
Total free space: 0

