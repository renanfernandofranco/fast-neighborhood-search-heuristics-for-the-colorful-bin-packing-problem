Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 18
Size: 357 Color: 4
Size: 252 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 16
Size: 252 Color: 2
Size: 250 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 16
Size: 293 Color: 18
Size: 256 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 7
Size: 282 Color: 9
Size: 250 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 7
Size: 352 Color: 9
Size: 280 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 16
Size: 346 Color: 18
Size: 293 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 7
Size: 292 Color: 18
Size: 255 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 14
Size: 370 Color: 6
Size: 252 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 295 Color: 0
Size: 251 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 258 Color: 10
Size: 251 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 10
Size: 343 Color: 10
Size: 259 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9
Size: 259 Color: 8
Size: 256 Color: 18

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 354 Color: 9
Size: 263 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 2
Size: 335 Color: 0
Size: 312 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 10
Size: 284 Color: 17
Size: 254 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 8
Size: 356 Color: 7
Size: 283 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 9
Size: 341 Color: 1
Size: 278 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 10
Size: 286 Color: 10
Size: 275 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 9
Size: 363 Color: 13
Size: 269 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 15
Size: 275 Color: 17
Size: 272 Color: 10

Total size: 20000
Total free space: 0

