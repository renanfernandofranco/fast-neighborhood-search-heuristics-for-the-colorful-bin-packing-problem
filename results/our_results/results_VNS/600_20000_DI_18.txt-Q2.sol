Capicity Bin: 15760
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 41
Items: 
Size: 528 Color: 1
Size: 492 Color: 1
Size: 488 Color: 1
Size: 480 Color: 1
Size: 478 Color: 1
Size: 472 Color: 1
Size: 464 Color: 1
Size: 464 Color: 1
Size: 424 Color: 1
Size: 424 Color: 0
Size: 424 Color: 0
Size: 424 Color: 0
Size: 416 Color: 1
Size: 416 Color: 1
Size: 412 Color: 0
Size: 392 Color: 1
Size: 392 Color: 0
Size: 388 Color: 1
Size: 388 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 372 Color: 0
Size: 370 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 368 Color: 1
Size: 360 Color: 1
Size: 342 Color: 0
Size: 340 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 328 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 300 Color: 0
Size: 300 Color: 0
Size: 268 Color: 1
Size: 262 Color: 0

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 7886 Color: 0
Size: 1375 Color: 1
Size: 1346 Color: 1
Size: 1331 Color: 1
Size: 1088 Color: 0
Size: 1028 Color: 0
Size: 988 Color: 0
Size: 420 Color: 0
Size: 298 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7940 Color: 0
Size: 6524 Color: 0
Size: 1296 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7908 Color: 1
Size: 6548 Color: 0
Size: 1304 Color: 1

Bin 5: 0 of cap free
Amount of items: 5
Items: 
Size: 9208 Color: 1
Size: 4408 Color: 0
Size: 1482 Color: 0
Size: 372 Color: 0
Size: 290 Color: 1

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 9624 Color: 1
Size: 5652 Color: 0
Size: 312 Color: 1
Size: 172 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10266 Color: 0
Size: 4582 Color: 1
Size: 912 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10580 Color: 1
Size: 4916 Color: 1
Size: 264 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 0
Size: 3896 Color: 1
Size: 432 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 1
Size: 3084 Color: 1
Size: 600 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 0
Size: 2900 Color: 0
Size: 720 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12156 Color: 1
Size: 3020 Color: 1
Size: 584 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12812 Color: 0
Size: 2084 Color: 0
Size: 864 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 1
Size: 2648 Color: 0
Size: 288 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 1
Size: 2116 Color: 0
Size: 720 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13005 Color: 0
Size: 1507 Color: 1
Size: 1248 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13042 Color: 0
Size: 2266 Color: 1
Size: 452 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13138 Color: 1
Size: 1326 Color: 1
Size: 1296 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13371 Color: 1
Size: 2085 Color: 0
Size: 304 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13412 Color: 1
Size: 1200 Color: 1
Size: 1148 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 0
Size: 1624 Color: 0
Size: 640 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 0
Size: 1844 Color: 0
Size: 400 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 1
Size: 1008 Color: 0
Size: 964 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13803 Color: 0
Size: 1631 Color: 0
Size: 326 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 1
Size: 1348 Color: 0
Size: 604 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 0
Size: 1625 Color: 0
Size: 324 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 1
Size: 1444 Color: 1
Size: 448 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13858 Color: 0
Size: 1414 Color: 0
Size: 488 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13961 Color: 0
Size: 1455 Color: 0
Size: 344 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14023 Color: 0
Size: 1449 Color: 0
Size: 288 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 1
Size: 1312 Color: 0
Size: 308 Color: 1

Bin 32: 1 of cap free
Amount of items: 8
Items: 
Size: 7883 Color: 0
Size: 1312 Color: 1
Size: 1304 Color: 1
Size: 1304 Color: 1
Size: 1296 Color: 1
Size: 984 Color: 0
Size: 976 Color: 0
Size: 700 Color: 0

Bin 33: 1 of cap free
Amount of items: 5
Items: 
Size: 7884 Color: 1
Size: 4837 Color: 1
Size: 1386 Color: 0
Size: 1356 Color: 0
Size: 296 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 8871 Color: 0
Size: 6504 Color: 1
Size: 384 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 0
Size: 5151 Color: 1
Size: 504 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 0
Size: 4223 Color: 1
Size: 276 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 11470 Color: 1
Size: 3993 Color: 1
Size: 296 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 12134 Color: 0
Size: 3009 Color: 1
Size: 616 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 12909 Color: 0
Size: 1528 Color: 1
Size: 1322 Color: 0

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 13503 Color: 1
Size: 1920 Color: 0
Size: 336 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 0
Size: 1120 Color: 0
Size: 916 Color: 1

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 13805 Color: 1
Size: 1522 Color: 1
Size: 432 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 13953 Color: 0
Size: 1806 Color: 1

Bin 44: 2 of cap free
Amount of items: 11
Items: 
Size: 7882 Color: 1
Size: 948 Color: 1
Size: 928 Color: 1
Size: 924 Color: 1
Size: 844 Color: 1
Size: 816 Color: 1
Size: 768 Color: 0
Size: 760 Color: 0
Size: 712 Color: 0
Size: 688 Color: 0
Size: 488 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 9581 Color: 1
Size: 5741 Color: 1
Size: 436 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 10983 Color: 0
Size: 4775 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11441 Color: 1
Size: 3733 Color: 1
Size: 584 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 11890 Color: 1
Size: 3608 Color: 0
Size: 260 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 11912 Color: 1
Size: 3578 Color: 0
Size: 268 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 12078 Color: 1
Size: 3222 Color: 0
Size: 458 Color: 1

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 1
Size: 3294 Color: 0
Size: 280 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 12881 Color: 1
Size: 1494 Color: 0
Size: 1383 Color: 1

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 13090 Color: 1
Size: 2668 Color: 0

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 13096 Color: 1
Size: 2662 Color: 0

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 0
Size: 2470 Color: 1

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 13381 Color: 0
Size: 2377 Color: 1

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 1
Size: 2102 Color: 0

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 1
Size: 1690 Color: 0

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 8697 Color: 1
Size: 6532 Color: 0
Size: 528 Color: 0

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 10596 Color: 1
Size: 4585 Color: 0
Size: 576 Color: 0

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 10661 Color: 1
Size: 5096 Color: 0

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 1
Size: 2857 Color: 0
Size: 1120 Color: 0

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 1
Size: 2647 Color: 0
Size: 320 Color: 1

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 0
Size: 1851 Color: 1

Bin 65: 4 of cap free
Amount of items: 7
Items: 
Size: 7892 Color: 0
Size: 1448 Color: 1
Size: 1442 Color: 1
Size: 1412 Color: 1
Size: 1394 Color: 1
Size: 1176 Color: 0
Size: 992 Color: 0

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 0
Size: 6928 Color: 1
Size: 308 Color: 1

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 1
Size: 5644 Color: 0
Size: 456 Color: 0

Bin 68: 4 of cap free
Amount of items: 4
Items: 
Size: 11244 Color: 0
Size: 2118 Color: 0
Size: 1546 Color: 1
Size: 848 Color: 1

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 13594 Color: 0
Size: 2162 Color: 1

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 1
Size: 1586 Color: 0

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 9868 Color: 0
Size: 5887 Color: 1

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 11426 Color: 0
Size: 3981 Color: 1
Size: 348 Color: 0

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 12872 Color: 0
Size: 2291 Color: 1
Size: 592 Color: 0

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 13903 Color: 1
Size: 1852 Color: 0

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 14111 Color: 1
Size: 1644 Color: 0

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 0
Size: 2956 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 13058 Color: 1
Size: 2696 Color: 0

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 0
Size: 1976 Color: 1

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 0
Size: 1688 Color: 1

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 13521 Color: 0
Size: 2232 Color: 1

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 1
Size: 1631 Color: 0
Size: 184 Color: 1

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 14015 Color: 1
Size: 1738 Color: 0

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 9664 Color: 1
Size: 5808 Color: 0
Size: 280 Color: 1

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 10910 Color: 0
Size: 4842 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 12151 Color: 0
Size: 3601 Color: 1

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 0
Size: 1912 Color: 0
Size: 1140 Color: 1

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 0
Size: 2176 Color: 1

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 13885 Color: 0
Size: 1867 Color: 1

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 14098 Color: 1
Size: 1654 Color: 0

Bin 90: 9 of cap free
Amount of items: 7
Items: 
Size: 7896 Color: 0
Size: 1516 Color: 1
Size: 1484 Color: 1
Size: 1351 Color: 1
Size: 1312 Color: 0
Size: 1312 Color: 0
Size: 880 Color: 0

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 9549 Color: 0
Size: 6040 Color: 1
Size: 162 Color: 0

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 0
Size: 6562 Color: 1
Size: 396 Color: 1

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 12220 Color: 1
Size: 3530 Color: 0

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 12570 Color: 1
Size: 3180 Color: 0

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 13678 Color: 1
Size: 2072 Color: 0

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 0
Size: 1934 Color: 1

Bin 97: 10 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 1
Size: 1896 Color: 0

Bin 98: 10 of cap free
Amount of items: 2
Items: 
Size: 13982 Color: 1
Size: 1768 Color: 0

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 12333 Color: 1
Size: 3416 Color: 0

Bin 100: 11 of cap free
Amount of items: 2
Items: 
Size: 13627 Color: 0
Size: 2122 Color: 1

Bin 101: 11 of cap free
Amount of items: 3
Items: 
Size: 13707 Color: 1
Size: 1962 Color: 0
Size: 80 Color: 1

Bin 102: 11 of cap free
Amount of items: 2
Items: 
Size: 13970 Color: 1
Size: 1779 Color: 0

Bin 103: 11 of cap free
Amount of items: 2
Items: 
Size: 14101 Color: 0
Size: 1648 Color: 1

Bin 104: 12 of cap free
Amount of items: 2
Items: 
Size: 9852 Color: 1
Size: 5896 Color: 0

Bin 105: 13 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 0
Size: 2156 Color: 1

Bin 106: 13 of cap free
Amount of items: 2
Items: 
Size: 14073 Color: 1
Size: 1674 Color: 0

Bin 107: 14 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 1
Size: 3876 Color: 0
Size: 674 Color: 1

Bin 108: 14 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 1
Size: 2364 Color: 0

Bin 109: 15 of cap free
Amount of items: 2
Items: 
Size: 10259 Color: 0
Size: 5486 Color: 1

Bin 110: 16 of cap free
Amount of items: 2
Items: 
Size: 10058 Color: 1
Size: 5686 Color: 0

Bin 111: 16 of cap free
Amount of items: 2
Items: 
Size: 11420 Color: 1
Size: 4324 Color: 0

Bin 112: 16 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 0
Size: 3208 Color: 1

Bin 113: 16 of cap free
Amount of items: 2
Items: 
Size: 12814 Color: 0
Size: 2930 Color: 1

Bin 114: 16 of cap free
Amount of items: 4
Items: 
Size: 13880 Color: 0
Size: 1560 Color: 1
Size: 160 Color: 1
Size: 144 Color: 0

Bin 115: 16 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 0
Size: 1716 Color: 1

Bin 116: 18 of cap free
Amount of items: 3
Items: 
Size: 9400 Color: 1
Size: 5624 Color: 1
Size: 718 Color: 0

Bin 117: 19 of cap free
Amount of items: 2
Items: 
Size: 13283 Color: 1
Size: 2458 Color: 0

Bin 118: 19 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 1
Size: 1563 Color: 0

Bin 119: 20 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 1
Size: 2004 Color: 0

Bin 120: 21 of cap free
Amount of items: 2
Items: 
Size: 12755 Color: 1
Size: 2984 Color: 0

Bin 121: 22 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 1
Size: 4946 Color: 0

Bin 122: 22 of cap free
Amount of items: 2
Items: 
Size: 13180 Color: 0
Size: 2558 Color: 1

Bin 123: 22 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 0
Size: 2296 Color: 1

Bin 124: 22 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 0
Size: 1750 Color: 1

Bin 125: 23 of cap free
Amount of items: 2
Items: 
Size: 11709 Color: 0
Size: 4028 Color: 1

Bin 126: 24 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 1
Size: 2188 Color: 0

Bin 127: 25 of cap free
Amount of items: 2
Items: 
Size: 12585 Color: 0
Size: 3150 Color: 1

Bin 128: 25 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 0
Size: 1711 Color: 1

Bin 129: 27 of cap free
Amount of items: 2
Items: 
Size: 13228 Color: 0
Size: 2505 Color: 1

Bin 130: 27 of cap free
Amount of items: 2
Items: 
Size: 14034 Color: 1
Size: 1699 Color: 0

Bin 131: 28 of cap free
Amount of items: 2
Items: 
Size: 7924 Color: 0
Size: 7808 Color: 1

Bin 132: 28 of cap free
Amount of items: 2
Items: 
Size: 10628 Color: 0
Size: 5104 Color: 1

Bin 133: 30 of cap free
Amount of items: 3
Items: 
Size: 10146 Color: 0
Size: 4728 Color: 1
Size: 856 Color: 1

Bin 134: 30 of cap free
Amount of items: 2
Items: 
Size: 11096 Color: 1
Size: 4634 Color: 0

Bin 135: 31 of cap free
Amount of items: 2
Items: 
Size: 13879 Color: 1
Size: 1850 Color: 0

Bin 136: 32 of cap free
Amount of items: 2
Items: 
Size: 12284 Color: 0
Size: 3444 Color: 1

Bin 137: 32 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 0
Size: 1580 Color: 1

Bin 138: 33 of cap free
Amount of items: 9
Items: 
Size: 7881 Color: 0
Size: 1136 Color: 1
Size: 1136 Color: 1
Size: 1128 Color: 1
Size: 1034 Color: 1
Size: 976 Color: 0
Size: 856 Color: 0
Size: 812 Color: 0
Size: 768 Color: 0

Bin 139: 33 of cap free
Amount of items: 2
Items: 
Size: 13013 Color: 1
Size: 2714 Color: 0

Bin 140: 33 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 0
Size: 2065 Color: 1

Bin 141: 34 of cap free
Amount of items: 2
Items: 
Size: 13170 Color: 0
Size: 2556 Color: 1

Bin 142: 34 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 0
Size: 2186 Color: 1

Bin 143: 35 of cap free
Amount of items: 2
Items: 
Size: 11573 Color: 1
Size: 4152 Color: 0

Bin 144: 36 of cap free
Amount of items: 2
Items: 
Size: 13539 Color: 0
Size: 2185 Color: 1

Bin 145: 37 of cap free
Amount of items: 2
Items: 
Size: 14090 Color: 0
Size: 1633 Color: 1

Bin 146: 38 of cap free
Amount of items: 2
Items: 
Size: 11982 Color: 0
Size: 3740 Color: 1

Bin 147: 38 of cap free
Amount of items: 2
Items: 
Size: 14146 Color: 0
Size: 1576 Color: 1

Bin 148: 41 of cap free
Amount of items: 2
Items: 
Size: 8980 Color: 0
Size: 6739 Color: 1

Bin 149: 41 of cap free
Amount of items: 2
Items: 
Size: 11641 Color: 0
Size: 4078 Color: 1

Bin 150: 41 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 0
Size: 1965 Color: 1

Bin 151: 46 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 1
Size: 4042 Color: 0

Bin 152: 46 of cap free
Amount of items: 2
Items: 
Size: 14165 Color: 1
Size: 1549 Color: 0

Bin 153: 47 of cap free
Amount of items: 2
Items: 
Size: 12453 Color: 0
Size: 3260 Color: 1

Bin 154: 47 of cap free
Amount of items: 2
Items: 
Size: 12956 Color: 0
Size: 2757 Color: 1

Bin 155: 53 of cap free
Amount of items: 2
Items: 
Size: 13410 Color: 1
Size: 2297 Color: 0

Bin 156: 55 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 0
Size: 1809 Color: 1

Bin 157: 56 of cap free
Amount of items: 2
Items: 
Size: 13364 Color: 0
Size: 2340 Color: 1

Bin 158: 56 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 1
Size: 1756 Color: 0

Bin 159: 58 of cap free
Amount of items: 2
Items: 
Size: 11898 Color: 0
Size: 3804 Color: 1

Bin 160: 62 of cap free
Amount of items: 2
Items: 
Size: 13242 Color: 1
Size: 2456 Color: 0

Bin 161: 63 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 0
Size: 3191 Color: 1

Bin 162: 63 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 1
Size: 1753 Color: 0

Bin 163: 64 of cap free
Amount of items: 3
Items: 
Size: 14141 Color: 1
Size: 1501 Color: 0
Size: 54 Color: 0

Bin 164: 69 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 1
Size: 1983 Color: 0

Bin 165: 82 of cap free
Amount of items: 2
Items: 
Size: 13218 Color: 0
Size: 2460 Color: 1

Bin 166: 84 of cap free
Amount of items: 3
Items: 
Size: 7960 Color: 0
Size: 6564 Color: 0
Size: 1152 Color: 1

Bin 167: 86 of cap free
Amount of items: 2
Items: 
Size: 12060 Color: 1
Size: 3614 Color: 0

Bin 168: 94 of cap free
Amount of items: 2
Items: 
Size: 10202 Color: 0
Size: 5464 Color: 1

Bin 169: 94 of cap free
Amount of items: 2
Items: 
Size: 11933 Color: 1
Size: 3733 Color: 0

Bin 170: 95 of cap free
Amount of items: 2
Items: 
Size: 13542 Color: 1
Size: 2123 Color: 0

Bin 171: 104 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 0
Size: 4028 Color: 1

Bin 172: 108 of cap free
Amount of items: 2
Items: 
Size: 12624 Color: 1
Size: 3028 Color: 0

Bin 173: 111 of cap free
Amount of items: 2
Items: 
Size: 10472 Color: 1
Size: 5177 Color: 0

Bin 174: 118 of cap free
Amount of items: 2
Items: 
Size: 9826 Color: 1
Size: 5816 Color: 0

Bin 175: 124 of cap free
Amount of items: 2
Items: 
Size: 9954 Color: 1
Size: 5682 Color: 0

Bin 176: 126 of cap free
Amount of items: 2
Items: 
Size: 9818 Color: 1
Size: 5816 Color: 0

Bin 177: 130 of cap free
Amount of items: 2
Items: 
Size: 13403 Color: 1
Size: 2227 Color: 0

Bin 178: 134 of cap free
Amount of items: 2
Items: 
Size: 13400 Color: 1
Size: 2226 Color: 0

Bin 179: 136 of cap free
Amount of items: 2
Items: 
Size: 10870 Color: 1
Size: 4754 Color: 0

Bin 180: 136 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 0
Size: 1964 Color: 1

Bin 181: 137 of cap free
Amount of items: 2
Items: 
Size: 10285 Color: 1
Size: 5338 Color: 0

Bin 182: 137 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 0
Size: 3377 Color: 1

Bin 183: 143 of cap free
Amount of items: 2
Items: 
Size: 10693 Color: 0
Size: 4924 Color: 1

Bin 184: 146 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 0
Size: 2598 Color: 1

Bin 185: 152 of cap free
Amount of items: 2
Items: 
Size: 12584 Color: 1
Size: 3024 Color: 0

Bin 186: 171 of cap free
Amount of items: 2
Items: 
Size: 11281 Color: 0
Size: 4308 Color: 1

Bin 187: 174 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 1
Size: 3022 Color: 0

Bin 188: 178 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 1
Size: 6566 Color: 0

Bin 189: 199 of cap free
Amount of items: 2
Items: 
Size: 8996 Color: 1
Size: 6565 Color: 0

Bin 190: 200 of cap free
Amount of items: 2
Items: 
Size: 11276 Color: 0
Size: 4284 Color: 1

Bin 191: 211 of cap free
Amount of items: 2
Items: 
Size: 13213 Color: 0
Size: 2336 Color: 1

Bin 192: 212 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 1
Size: 2408 Color: 0

Bin 193: 220 of cap free
Amount of items: 2
Items: 
Size: 12216 Color: 1
Size: 3324 Color: 0

Bin 194: 220 of cap free
Amount of items: 2
Items: 
Size: 13139 Color: 1
Size: 2401 Color: 0

Bin 195: 244 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 1
Size: 2968 Color: 0

Bin 196: 254 of cap free
Amount of items: 2
Items: 
Size: 8942 Color: 1
Size: 6564 Color: 0

Bin 197: 254 of cap free
Amount of items: 2
Items: 
Size: 8938 Color: 0
Size: 6568 Color: 1

Bin 198: 258 of cap free
Amount of items: 27
Items: 
Size: 808 Color: 1
Size: 796 Color: 1
Size: 746 Color: 1
Size: 744 Color: 1
Size: 672 Color: 1
Size: 656 Color: 1
Size: 640 Color: 0
Size: 636 Color: 0
Size: 628 Color: 1
Size: 604 Color: 1
Size: 600 Color: 1
Size: 576 Color: 1
Size: 570 Color: 0
Size: 550 Color: 1
Size: 540 Color: 1
Size: 528 Color: 1
Size: 528 Color: 0
Size: 528 Color: 0
Size: 500 Color: 0
Size: 480 Color: 0
Size: 474 Color: 0
Size: 472 Color: 0
Size: 464 Color: 0
Size: 454 Color: 0
Size: 444 Color: 0
Size: 436 Color: 0
Size: 428 Color: 0

Bin 199: 8152 of cap free
Amount of items: 28
Items: 
Size: 360 Color: 1
Size: 354 Color: 1
Size: 344 Color: 1
Size: 338 Color: 1
Size: 332 Color: 1
Size: 328 Color: 1
Size: 324 Color: 1
Size: 316 Color: 1
Size: 304 Color: 1
Size: 296 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 284 Color: 0
Size: 276 Color: 1
Size: 276 Color: 1
Size: 274 Color: 0
Size: 272 Color: 1
Size: 272 Color: 0
Size: 264 Color: 1
Size: 264 Color: 0
Size: 264 Color: 0
Size: 260 Color: 0
Size: 216 Color: 0
Size: 144 Color: 0
Size: 128 Color: 0
Size: 128 Color: 0
Size: 126 Color: 0

Total size: 3120480
Total free space: 15760

