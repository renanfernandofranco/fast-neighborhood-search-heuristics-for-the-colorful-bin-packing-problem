Capicity Bin: 1996
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1162 Color: 146
Size: 778 Color: 133
Size: 56 Color: 23

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 163
Size: 451 Color: 111
Size: 88 Color: 45

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1521 Color: 168
Size: 397 Color: 104
Size: 78 Color: 41

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 169
Size: 390 Color: 102
Size: 84 Color: 43

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1525 Color: 170
Size: 351 Color: 97
Size: 120 Color: 56

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1530 Color: 171
Size: 446 Color: 109
Size: 20 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1610 Color: 179
Size: 220 Color: 78
Size: 166 Color: 68

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 180
Size: 303 Color: 90
Size: 76 Color: 38

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 183
Size: 186 Color: 71
Size: 164 Color: 67

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 186
Size: 263 Color: 86
Size: 52 Color: 21

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 190
Size: 239 Color: 81
Size: 46 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 192
Size: 198 Color: 75
Size: 76 Color: 36

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 195
Size: 197 Color: 74
Size: 38 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 197
Size: 118 Color: 55
Size: 108 Color: 53

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 198
Size: 189 Color: 72
Size: 36 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 200
Size: 178 Color: 69
Size: 40 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 201
Size: 142 Color: 63
Size: 68 Color: 32

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1338 Color: 155
Size: 615 Color: 123
Size: 42 Color: 14

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 164
Size: 325 Color: 94
Size: 211 Color: 77

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 165
Size: 449 Color: 110
Size: 84 Color: 42

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1486 Color: 166
Size: 509 Color: 115

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 167
Size: 478 Color: 113

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 177
Size: 393 Color: 103

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 181
Size: 369 Color: 100
Size: 8 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 182
Size: 354 Color: 98
Size: 8 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1714 Color: 191
Size: 281 Color: 87

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 174
Size: 410 Color: 106
Size: 10 Color: 2

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1607 Color: 178
Size: 387 Color: 101

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1654 Color: 184
Size: 340 Color: 96

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1743 Color: 193
Size: 251 Color: 84

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1774 Color: 199
Size: 220 Color: 79

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 159
Size: 610 Color: 122

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1533 Color: 172
Size: 460 Color: 112

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1751 Color: 194
Size: 242 Color: 82

Bin 35: 4 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 162
Size: 550 Color: 119
Size: 16 Color: 3

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1706 Color: 189
Size: 286 Color: 88

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1762 Color: 196
Size: 230 Color: 80

Bin 38: 5 of cap free
Amount of items: 2
Items: 
Size: 1575 Color: 175
Size: 416 Color: 107

Bin 39: 6 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 152
Size: 676 Color: 127
Size: 48 Color: 17

Bin 40: 6 of cap free
Amount of items: 2
Items: 
Size: 1682 Color: 187
Size: 308 Color: 91

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 176
Size: 401 Color: 105

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 1659 Color: 185
Size: 330 Color: 95

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1695 Color: 188
Size: 294 Color: 89

Bin 44: 10 of cap free
Amount of items: 2
Items: 
Size: 1252 Color: 149
Size: 734 Color: 131

Bin 45: 11 of cap free
Amount of items: 2
Items: 
Size: 1287 Color: 153
Size: 698 Color: 128

Bin 46: 12 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 173
Size: 426 Color: 108

Bin 47: 14 of cap free
Amount of items: 3
Items: 
Size: 1118 Color: 142
Size: 804 Color: 134
Size: 60 Color: 25

Bin 48: 18 of cap free
Amount of items: 2
Items: 
Size: 1387 Color: 160
Size: 591 Color: 121

Bin 49: 21 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 151
Size: 666 Color: 126
Size: 50 Color: 18

Bin 50: 24 of cap free
Amount of items: 2
Items: 
Size: 1141 Color: 144
Size: 831 Color: 137

Bin 51: 25 of cap free
Amount of items: 3
Items: 
Size: 1346 Color: 156
Size: 589 Color: 120
Size: 36 Color: 11

Bin 52: 26 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 148
Size: 717 Color: 130
Size: 52 Color: 20

Bin 53: 26 of cap free
Amount of items: 3
Items: 
Size: 1255 Color: 150
Size: 663 Color: 125
Size: 52 Color: 19

Bin 54: 28 of cap free
Amount of items: 5
Items: 
Size: 1002 Color: 139
Size: 322 Color: 93
Size: 318 Color: 92
Size: 262 Color: 85
Size: 64 Color: 28

Bin 55: 28 of cap free
Amount of items: 3
Items: 
Size: 1145 Color: 145
Size: 767 Color: 132
Size: 56 Color: 24

Bin 56: 29 of cap free
Amount of items: 2
Items: 
Size: 1137 Color: 143
Size: 830 Color: 136

Bin 57: 29 of cap free
Amount of items: 3
Items: 
Size: 1198 Color: 147
Size: 713 Color: 129
Size: 56 Color: 22

Bin 58: 29 of cap free
Amount of items: 4
Items: 
Size: 1380 Color: 158
Size: 515 Color: 117
Size: 36 Color: 8
Size: 36 Color: 6

Bin 59: 32 of cap free
Amount of items: 18
Items: 
Size: 164 Color: 66
Size: 152 Color: 65
Size: 144 Color: 64
Size: 142 Color: 62
Size: 136 Color: 61
Size: 132 Color: 60
Size: 132 Color: 59
Size: 122 Color: 58
Size: 122 Color: 57
Size: 116 Color: 54
Size: 88 Color: 44
Size: 78 Color: 40
Size: 78 Color: 39
Size: 76 Color: 37
Size: 72 Color: 35
Size: 72 Color: 34
Size: 70 Color: 33
Size: 68 Color: 31

Bin 60: 32 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 161
Size: 542 Color: 118
Size: 32 Color: 5

Bin 61: 34 of cap free
Amount of items: 4
Items: 
Size: 1379 Color: 157
Size: 511 Color: 116
Size: 36 Color: 10
Size: 36 Color: 9

Bin 62: 41 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 141
Size: 829 Color: 135
Size: 60 Color: 26

Bin 63: 42 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 154
Size: 619 Color: 124
Size: 44 Color: 15

Bin 64: 46 of cap free
Amount of items: 7
Items: 
Size: 999 Color: 138
Size: 246 Color: 83
Size: 205 Color: 76
Size: 190 Color: 73
Size: 182 Color: 70
Size: 64 Color: 30
Size: 64 Color: 29

Bin 65: 57 of cap free
Amount of items: 4
Items: 
Size: 1003 Color: 140
Size: 506 Color: 114
Size: 366 Color: 99
Size: 64 Color: 27

Bin 66: 1304 of cap free
Amount of items: 7
Items: 
Size: 108 Color: 52
Size: 102 Color: 51
Size: 102 Color: 50
Size: 100 Color: 49
Size: 100 Color: 48
Size: 92 Color: 47
Size: 88 Color: 46

Total size: 129740
Total free space: 1996

