Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 10
Size: 357 Color: 10
Size: 254 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 11
Size: 349 Color: 7
Size: 295 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 356 Color: 11
Size: 261 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 3
Size: 289 Color: 19
Size: 278 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 6
Size: 323 Color: 13
Size: 309 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 292 Color: 14
Size: 277 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 14
Size: 274 Color: 7
Size: 263 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 13
Size: 287 Color: 14
Size: 269 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 12
Size: 348 Color: 15
Size: 262 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 13
Size: 267 Color: 5
Size: 257 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 356 Color: 15
Size: 281 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 17
Size: 312 Color: 19
Size: 250 Color: 15

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 8
Size: 279 Color: 5
Size: 274 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 296 Color: 3
Size: 291 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 7
Size: 251 Color: 15
Size: 250 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 7
Size: 325 Color: 6
Size: 259 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 7
Size: 303 Color: 0
Size: 281 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 18
Size: 334 Color: 7
Size: 276 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 8
Size: 347 Color: 15
Size: 256 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 271 Color: 4
Size: 262 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 14
Size: 335 Color: 14
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 5
Size: 276 Color: 16
Size: 273 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 12
Size: 274 Color: 9
Size: 250 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 268 Color: 10
Size: 255 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 12
Size: 257 Color: 17
Size: 254 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 11
Size: 299 Color: 12
Size: 255 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 10
Size: 291 Color: 9
Size: 278 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 347 Color: 4
Size: 268 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 262 Color: 15
Size: 259 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 300 Color: 18
Size: 290 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 5
Size: 302 Color: 8
Size: 264 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 4
Size: 313 Color: 14
Size: 304 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 8
Size: 280 Color: 0
Size: 257 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 262 Color: 3
Size: 250 Color: 16

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 9
Size: 267 Color: 5
Size: 257 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 10
Size: 313 Color: 14
Size: 265 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 14
Size: 275 Color: 9
Size: 256 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 8
Size: 286 Color: 18
Size: 276 Color: 18

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 14
Size: 283 Color: 2
Size: 252 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 360 Color: 5
Size: 266 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 11
Size: 310 Color: 10
Size: 275 Color: 7

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 321 Color: 9
Size: 271 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 17
Size: 369 Color: 10
Size: 253 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 10
Size: 323 Color: 1
Size: 259 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 19
Size: 294 Color: 9
Size: 250 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 356 Color: 12
Size: 252 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 312 Color: 18
Size: 283 Color: 5

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 360 Color: 10
Size: 258 Color: 5

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 6
Size: 261 Color: 4
Size: 252 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 10
Size: 353 Color: 12
Size: 285 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 18
Size: 278 Color: 6
Size: 273 Color: 12

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 9
Size: 316 Color: 5
Size: 304 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 5
Size: 338 Color: 1
Size: 300 Color: 8

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 5
Size: 272 Color: 2
Size: 270 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 17
Size: 309 Color: 4
Size: 307 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 294 Color: 0
Size: 255 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 310 Color: 4
Size: 299 Color: 9

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 8
Size: 319 Color: 6
Size: 297 Color: 17

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 261 Color: 14
Size: 255 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 6
Size: 255 Color: 7
Size: 250 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 19
Size: 315 Color: 15
Size: 271 Color: 7

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 12
Size: 273 Color: 5
Size: 268 Color: 12

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 335 Color: 9
Size: 291 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7
Size: 313 Color: 14
Size: 301 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 6
Size: 269 Color: 13
Size: 256 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 10
Size: 334 Color: 14
Size: 330 Color: 13

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 16
Size: 269 Color: 4
Size: 256 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 10
Size: 326 Color: 5
Size: 269 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 19
Size: 257 Color: 7
Size: 251 Color: 6

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 298 Color: 9
Size: 270 Color: 17

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 4
Size: 254 Color: 14
Size: 250 Color: 14

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 17
Size: 326 Color: 14
Size: 295 Color: 6

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 254 Color: 2
Size: 250 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 14
Size: 268 Color: 5
Size: 259 Color: 15

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 10
Size: 341 Color: 15
Size: 277 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 14
Size: 374 Color: 10
Size: 250 Color: 9

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 334 Color: 0
Size: 289 Color: 5

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 10
Size: 270 Color: 10
Size: 250 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 347 Color: 1
Size: 252 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7
Size: 324 Color: 11
Size: 294 Color: 12

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 4
Size: 323 Color: 16
Size: 260 Color: 13

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 324 Color: 14
Size: 256 Color: 18

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 13
Size: 329 Color: 13
Size: 265 Color: 5

Total size: 83000
Total free space: 0

