Capicity Bin: 7880
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 1
Size: 774 Color: 1
Size: 332 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 7059 Color: 1
Size: 685 Color: 1
Size: 136 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6005 Color: 1
Size: 1563 Color: 1
Size: 312 Color: 0

Bin 4: 0 of cap free
Amount of items: 5
Items: 
Size: 4380 Color: 1
Size: 1711 Color: 1
Size: 717 Color: 1
Size: 552 Color: 0
Size: 520 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 1
Size: 3282 Color: 1
Size: 652 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 1
Size: 3284 Color: 1
Size: 648 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7037 Color: 1
Size: 703 Color: 1
Size: 140 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4514 Color: 1
Size: 2798 Color: 1
Size: 568 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7058 Color: 1
Size: 686 Color: 1
Size: 136 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4473 Color: 1
Size: 2841 Color: 1
Size: 566 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 1
Size: 1226 Color: 1
Size: 244 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7012 Color: 1
Size: 836 Color: 1
Size: 32 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 1
Size: 1014 Color: 1
Size: 200 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 1
Size: 1002 Color: 1
Size: 66 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 1
Size: 3274 Color: 1
Size: 652 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 1
Size: 1148 Color: 1
Size: 216 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 1
Size: 660 Color: 1
Size: 134 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 1
Size: 1121 Color: 1
Size: 224 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6992 Color: 1
Size: 744 Color: 1
Size: 144 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 1
Size: 632 Color: 1
Size: 156 Color: 0

Bin 21: 0 of cap free
Amount of items: 5
Items: 
Size: 4945 Color: 1
Size: 1260 Color: 1
Size: 1191 Color: 1
Size: 244 Color: 0
Size: 240 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4929 Color: 1
Size: 2461 Color: 1
Size: 490 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6765 Color: 1
Size: 931 Color: 1
Size: 184 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 1
Size: 960 Color: 1
Size: 196 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 1
Size: 2412 Color: 1
Size: 480 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 1
Size: 718 Color: 1
Size: 172 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6307 Color: 1
Size: 1077 Color: 1
Size: 496 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4076 Color: 1
Size: 3172 Color: 1
Size: 632 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 3943 Color: 1
Size: 3281 Color: 1
Size: 656 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5430 Color: 1
Size: 2042 Color: 1
Size: 408 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4937 Color: 1
Size: 2453 Color: 1
Size: 490 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 1
Size: 1357 Color: 1
Size: 152 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6222 Color: 1
Size: 1382 Color: 1
Size: 276 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 1
Size: 1724 Color: 1
Size: 160 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7068 Color: 1
Size: 684 Color: 1
Size: 128 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 1
Size: 2100 Color: 1
Size: 360 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 1
Size: 1284 Color: 1
Size: 224 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4489 Color: 1
Size: 2827 Color: 1
Size: 564 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7077 Color: 1
Size: 671 Color: 1
Size: 132 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6580 Color: 1
Size: 1084 Color: 1
Size: 216 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 1
Size: 1391 Color: 1
Size: 276 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6667 Color: 1
Size: 909 Color: 1
Size: 304 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 3962 Color: 1
Size: 3266 Color: 1
Size: 652 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 1
Size: 1500 Color: 1
Size: 152 Color: 0

Bin 45: 0 of cap free
Amount of items: 5
Items: 
Size: 5676 Color: 1
Size: 934 Color: 1
Size: 802 Color: 1
Size: 312 Color: 0
Size: 156 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6842 Color: 1
Size: 854 Color: 1
Size: 184 Color: 0

Bin 47: 0 of cap free
Amount of items: 5
Items: 
Size: 5758 Color: 1
Size: 820 Color: 1
Size: 742 Color: 1
Size: 408 Color: 0
Size: 152 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 1
Size: 786 Color: 1
Size: 236 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5755 Color: 1
Size: 1469 Color: 1
Size: 656 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6938 Color: 1
Size: 810 Color: 1
Size: 132 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 1
Size: 1067 Color: 1
Size: 158 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 1
Size: 996 Color: 1
Size: 192 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6050 Color: 1
Size: 1222 Color: 1
Size: 608 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 3941 Color: 1
Size: 3283 Color: 1
Size: 656 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 1
Size: 1167 Color: 1
Size: 112 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6925 Color: 1
Size: 787 Color: 1
Size: 168 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6095 Color: 1
Size: 1229 Color: 1
Size: 556 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 1
Size: 940 Color: 1
Size: 184 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 4436 Color: 1
Size: 3084 Color: 1
Size: 360 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 1
Size: 1940 Color: 1
Size: 384 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5734 Color: 1
Size: 1790 Color: 1
Size: 356 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6566 Color: 1
Size: 1098 Color: 1
Size: 216 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 7054 Color: 1
Size: 690 Color: 1
Size: 136 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5884 Color: 1
Size: 1724 Color: 1
Size: 272 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 1
Size: 882 Color: 1
Size: 176 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 1
Size: 708 Color: 1
Size: 136 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5309 Color: 1
Size: 2159 Color: 1
Size: 412 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5002 Color: 1
Size: 2402 Color: 1
Size: 476 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5390 Color: 1
Size: 2078 Color: 1
Size: 412 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 1
Size: 1140 Color: 1
Size: 428 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 1
Size: 1380 Color: 1
Size: 160 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 1
Size: 764 Color: 1
Size: 354 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4558 Color: 1
Size: 3180 Color: 1
Size: 142 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 6453 Color: 1
Size: 1287 Color: 1
Size: 140 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6006 Color: 1
Size: 1578 Color: 1
Size: 296 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6018 Color: 1
Size: 1554 Color: 1
Size: 308 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 1
Size: 2382 Color: 1
Size: 472 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 4812 Color: 1
Size: 2876 Color: 1
Size: 192 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 6871 Color: 1
Size: 841 Color: 1
Size: 168 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6860 Color: 1
Size: 852 Color: 1
Size: 168 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5374 Color: 1
Size: 2158 Color: 1
Size: 348 Color: 0

Bin 82: 0 of cap free
Amount of items: 5
Items: 
Size: 3942 Color: 1
Size: 1844 Color: 1
Size: 1692 Color: 1
Size: 344 Color: 0
Size: 58 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6394 Color: 1
Size: 1224 Color: 1
Size: 262 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 1
Size: 2444 Color: 1
Size: 480 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 6548 Color: 1
Size: 964 Color: 1
Size: 368 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 1
Size: 866 Color: 1
Size: 336 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 3970 Color: 1
Size: 3262 Color: 1
Size: 648 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5132 Color: 1
Size: 2536 Color: 1
Size: 212 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6791 Color: 1
Size: 797 Color: 1
Size: 292 Color: 0

Bin 90: 0 of cap free
Amount of items: 5
Items: 
Size: 4526 Color: 1
Size: 1668 Color: 1
Size: 1242 Color: 1
Size: 296 Color: 0
Size: 148 Color: 0

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 1
Size: 749 Color: 1
Size: 176 Color: 0

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 5615 Color: 1
Size: 2062 Color: 1
Size: 202 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 1
Size: 981 Color: 1
Size: 348 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 6418 Color: 1
Size: 1311 Color: 1
Size: 150 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 751 Color: 1
Size: 156 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 4848 Color: 1
Size: 2925 Color: 1
Size: 106 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 1
Size: 677 Color: 1
Size: 180 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 6527 Color: 1
Size: 1080 Color: 1
Size: 272 Color: 0

Bin 99: 1 of cap free
Amount of items: 5
Items: 
Size: 3282 Color: 1
Size: 2770 Color: 1
Size: 1011 Color: 1
Size: 488 Color: 0
Size: 328 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 1
Size: 2620 Color: 1
Size: 336 Color: 0

Bin 101: 1 of cap free
Amount of items: 5
Items: 
Size: 4481 Color: 1
Size: 2143 Color: 1
Size: 863 Color: 1
Size: 248 Color: 0
Size: 144 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 1
Size: 1771 Color: 1
Size: 256 Color: 0

Bin 103: 1 of cap free
Amount of items: 5
Items: 
Size: 2833 Color: 1
Size: 2806 Color: 1
Size: 1526 Color: 1
Size: 566 Color: 0
Size: 148 Color: 0

Bin 104: 2 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 954 Color: 1
Size: 312 Color: 0

Bin 105: 2 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 1
Size: 2482 Color: 1
Size: 32 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 5303 Color: 1
Size: 2447 Color: 1
Size: 128 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 1
Size: 1388 Color: 1
Size: 244 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 1
Size: 2564 Color: 1
Size: 320 Color: 0

Bin 109: 3 of cap free
Amount of items: 3
Items: 
Size: 5887 Color: 1
Size: 1770 Color: 1
Size: 220 Color: 0

Bin 110: 3 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 1
Size: 1661 Color: 1
Size: 404 Color: 0

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 1
Size: 1110 Color: 1
Size: 64 Color: 0

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 5750 Color: 1
Size: 2052 Color: 1
Size: 74 Color: 0

Bin 113: 4 of cap free
Amount of items: 3
Items: 
Size: 6900 Color: 1
Size: 560 Color: 0
Size: 416 Color: 0

Bin 114: 5 of cap free
Amount of items: 3
Items: 
Size: 6407 Color: 1
Size: 1116 Color: 1
Size: 352 Color: 0

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 5820 Color: 1
Size: 1782 Color: 1
Size: 272 Color: 0

Bin 116: 6 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 1
Size: 1362 Color: 1
Size: 428 Color: 0

Bin 117: 6 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 1
Size: 2436 Color: 1
Size: 32 Color: 0

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 6845 Color: 1
Size: 892 Color: 1
Size: 136 Color: 0

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 3896 Color: 1
Size: 3472 Color: 1
Size: 504 Color: 0

Bin 120: 12 of cap free
Amount of items: 3
Items: 
Size: 4740 Color: 1
Size: 2928 Color: 1
Size: 200 Color: 0

Bin 121: 15 of cap free
Amount of items: 3
Items: 
Size: 6983 Color: 1
Size: 504 Color: 1
Size: 378 Color: 0

Bin 122: 17 of cap free
Amount of items: 3
Items: 
Size: 6119 Color: 1
Size: 1572 Color: 1
Size: 172 Color: 0

Bin 123: 27 of cap free
Amount of items: 3
Items: 
Size: 5607 Color: 1
Size: 1806 Color: 1
Size: 440 Color: 0

Bin 124: 29 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 1
Size: 662 Color: 1
Size: 120 Color: 0

Bin 125: 63 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 1
Size: 2149 Color: 1
Size: 1480 Color: 0

Bin 126: 155 of cap free
Amount of items: 3
Items: 
Size: 5714 Color: 1
Size: 1895 Color: 1
Size: 116 Color: 0

Bin 127: 859 of cap free
Amount of items: 1
Items: 
Size: 7021 Color: 1

Bin 128: 901 of cap free
Amount of items: 1
Items: 
Size: 6979 Color: 1

Bin 129: 943 of cap free
Amount of items: 1
Items: 
Size: 6937 Color: 1

Bin 130: 958 of cap free
Amount of items: 1
Items: 
Size: 6922 Color: 1

Bin 131: 970 of cap free
Amount of items: 1
Items: 
Size: 6910 Color: 1

Bin 132: 1193 of cap free
Amount of items: 1
Items: 
Size: 6687 Color: 1

Bin 133: 1660 of cap free
Amount of items: 1
Items: 
Size: 6220 Color: 1

Total size: 1040160
Total free space: 7880

