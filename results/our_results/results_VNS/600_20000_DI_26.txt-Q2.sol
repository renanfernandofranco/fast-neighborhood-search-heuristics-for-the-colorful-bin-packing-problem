Capicity Bin: 16032
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 28
Items: 
Size: 752 Color: 1
Size: 752 Color: 1
Size: 752 Color: 1
Size: 712 Color: 1
Size: 682 Color: 1
Size: 672 Color: 1
Size: 664 Color: 1
Size: 640 Color: 1
Size: 640 Color: 0
Size: 616 Color: 1
Size: 600 Color: 1
Size: 588 Color: 1
Size: 582 Color: 1
Size: 576 Color: 0
Size: 576 Color: 0
Size: 568 Color: 0
Size: 560 Color: 1
Size: 554 Color: 0
Size: 520 Color: 0
Size: 502 Color: 0
Size: 488 Color: 0
Size: 476 Color: 0
Size: 456 Color: 0
Size: 452 Color: 0
Size: 448 Color: 0
Size: 440 Color: 0
Size: 388 Color: 1
Size: 376 Color: 0

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 8024 Color: 0
Size: 1332 Color: 0
Size: 1328 Color: 0
Size: 1280 Color: 0
Size: 1120 Color: 1
Size: 1088 Color: 1
Size: 1040 Color: 1
Size: 498 Color: 1
Size: 322 Color: 0

Bin 3: 0 of cap free
Amount of items: 7
Items: 
Size: 8028 Color: 0
Size: 1428 Color: 0
Size: 1422 Color: 1
Size: 1416 Color: 1
Size: 1412 Color: 0
Size: 1334 Color: 1
Size: 992 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 1
Size: 6676 Color: 0
Size: 458 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9480 Color: 0
Size: 6248 Color: 1
Size: 304 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9522 Color: 1
Size: 6158 Color: 1
Size: 352 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 1
Size: 4348 Color: 1
Size: 912 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10968 Color: 1
Size: 4388 Color: 1
Size: 676 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11043 Color: 0
Size: 4231 Color: 1
Size: 758 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11019 Color: 1
Size: 4179 Color: 1
Size: 834 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 0
Size: 4152 Color: 0
Size: 816 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11442 Color: 0
Size: 4232 Color: 1
Size: 358 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11475 Color: 0
Size: 4151 Color: 1
Size: 406 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12524 Color: 1
Size: 3268 Color: 0
Size: 240 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12482 Color: 0
Size: 2092 Color: 0
Size: 1458 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 0
Size: 1560 Color: 1
Size: 1152 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13356 Color: 1
Size: 2236 Color: 0
Size: 440 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13382 Color: 0
Size: 2004 Color: 0
Size: 646 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13386 Color: 0
Size: 2006 Color: 0
Size: 640 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 0
Size: 2164 Color: 0
Size: 432 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 1
Size: 2264 Color: 0
Size: 224 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13637 Color: 1
Size: 1997 Color: 0
Size: 398 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 0
Size: 1570 Color: 1
Size: 584 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 0
Size: 1528 Color: 1
Size: 612 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 1
Size: 1773 Color: 1
Size: 450 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13894 Color: 0
Size: 1604 Color: 1
Size: 534 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13964 Color: 1
Size: 1724 Color: 1
Size: 344 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14099 Color: 1
Size: 1501 Color: 0
Size: 432 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 0
Size: 964 Color: 0
Size: 960 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14288 Color: 0
Size: 1508 Color: 0
Size: 236 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 1
Size: 850 Color: 0
Size: 824 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 1
Size: 1370 Color: 1
Size: 272 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 8528 Color: 1
Size: 6673 Color: 1
Size: 830 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 8893 Color: 1
Size: 6674 Color: 0
Size: 464 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 9783 Color: 1
Size: 5780 Color: 0
Size: 468 Color: 1

Bin 36: 1 of cap free
Amount of items: 5
Items: 
Size: 10298 Color: 0
Size: 1660 Color: 1
Size: 1553 Color: 1
Size: 1332 Color: 1
Size: 1188 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 11127 Color: 1
Size: 4122 Color: 0
Size: 782 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 1
Size: 4159 Color: 1
Size: 444 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 11736 Color: 1
Size: 3989 Color: 1
Size: 306 Color: 0

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 0
Size: 4099 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 12810 Color: 1
Size: 2917 Color: 0
Size: 304 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 12823 Color: 1
Size: 2924 Color: 0
Size: 284 Color: 1

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 12852 Color: 1
Size: 2891 Color: 0
Size: 288 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 13041 Color: 0
Size: 1608 Color: 1
Size: 1382 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 13166 Color: 1
Size: 2649 Color: 0
Size: 216 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 13327 Color: 1
Size: 2210 Color: 0
Size: 494 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 13455 Color: 1
Size: 1328 Color: 1
Size: 1248 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 13521 Color: 1
Size: 1442 Color: 1
Size: 1068 Color: 0

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 13623 Color: 0
Size: 2408 Color: 1

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 13765 Color: 1
Size: 1770 Color: 1
Size: 496 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 0
Size: 1798 Color: 1
Size: 208 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 14107 Color: 1
Size: 1542 Color: 0
Size: 382 Color: 0

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 9714 Color: 0
Size: 5788 Color: 0
Size: 528 Color: 1

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 10362 Color: 0
Size: 5464 Color: 0
Size: 204 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 0
Size: 5044 Color: 0
Size: 324 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 11539 Color: 0
Size: 4219 Color: 1
Size: 272 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 11683 Color: 1
Size: 3799 Color: 0
Size: 548 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 13020 Color: 1
Size: 1556 Color: 1
Size: 1454 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 13082 Color: 1
Size: 2568 Color: 0
Size: 380 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 0
Size: 2206 Color: 1
Size: 212 Color: 0

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 0
Size: 2390 Color: 1

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 0
Size: 2264 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 13733 Color: 1
Size: 1537 Color: 1
Size: 760 Color: 0

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 14290 Color: 1
Size: 1740 Color: 0

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 14326 Color: 0
Size: 1704 Color: 1

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 1
Size: 1686 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 8021 Color: 1
Size: 6680 Color: 0
Size: 1328 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 0
Size: 5351 Color: 0
Size: 334 Color: 1

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 12185 Color: 0
Size: 3844 Color: 1

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 12357 Color: 1
Size: 3272 Color: 0
Size: 400 Color: 1

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 0
Size: 3233 Color: 0
Size: 400 Color: 1

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 13513 Color: 0
Size: 2516 Color: 1

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 14228 Color: 0
Size: 1801 Color: 1

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 14356 Color: 1
Size: 1673 Color: 0

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 9100 Color: 0
Size: 6472 Color: 1
Size: 456 Color: 0

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 10820 Color: 0
Size: 4744 Color: 1
Size: 464 Color: 0

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 11031 Color: 0
Size: 4641 Color: 0
Size: 356 Color: 1

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 11051 Color: 0
Size: 4131 Color: 1
Size: 846 Color: 0

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 0
Size: 3085 Color: 1
Size: 612 Color: 0

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 13359 Color: 1
Size: 2493 Color: 0
Size: 176 Color: 1

Bin 81: 5 of cap free
Amount of items: 3
Items: 
Size: 10245 Color: 1
Size: 5498 Color: 0
Size: 284 Color: 1

Bin 82: 5 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 0
Size: 3800 Color: 1
Size: 288 Color: 0

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 12705 Color: 1
Size: 3322 Color: 0

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 13065 Color: 0
Size: 2962 Color: 1

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 13992 Color: 0
Size: 2035 Color: 1

Bin 86: 5 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 1
Size: 1907 Color: 0

Bin 87: 6 of cap free
Amount of items: 9
Items: 
Size: 8018 Color: 1
Size: 1188 Color: 0
Size: 1156 Color: 0
Size: 1104 Color: 0
Size: 1000 Color: 1
Size: 944 Color: 1
Size: 880 Color: 0
Size: 872 Color: 0
Size: 864 Color: 1

Bin 88: 6 of cap free
Amount of items: 3
Items: 
Size: 8026 Color: 1
Size: 7120 Color: 1
Size: 880 Color: 0

Bin 89: 6 of cap free
Amount of items: 3
Items: 
Size: 12737 Color: 1
Size: 2521 Color: 0
Size: 768 Color: 1

Bin 90: 6 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 1
Size: 2082 Color: 0

Bin 91: 7 of cap free
Amount of items: 3
Items: 
Size: 8025 Color: 1
Size: 6666 Color: 1
Size: 1334 Color: 0

Bin 92: 7 of cap free
Amount of items: 3
Items: 
Size: 12046 Color: 0
Size: 3771 Color: 0
Size: 208 Color: 1

Bin 93: 7 of cap free
Amount of items: 2
Items: 
Size: 13578 Color: 1
Size: 2447 Color: 0

Bin 94: 7 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 0
Size: 1853 Color: 1

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 10923 Color: 1
Size: 5101 Color: 0

Bin 96: 8 of cap free
Amount of items: 3
Items: 
Size: 11980 Color: 1
Size: 3804 Color: 0
Size: 240 Color: 1

Bin 97: 8 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 1
Size: 2462 Color: 0

Bin 98: 9 of cap free
Amount of items: 2
Items: 
Size: 13732 Color: 1
Size: 2291 Color: 0

Bin 99: 9 of cap free
Amount of items: 2
Items: 
Size: 14360 Color: 0
Size: 1663 Color: 1

Bin 100: 10 of cap free
Amount of items: 7
Items: 
Size: 8020 Color: 1
Size: 1404 Color: 1
Size: 1400 Color: 1
Size: 1334 Color: 0
Size: 1332 Color: 1
Size: 1332 Color: 0
Size: 1200 Color: 0

Bin 101: 10 of cap free
Amount of items: 5
Items: 
Size: 10213 Color: 0
Size: 1928 Color: 0
Size: 1497 Color: 1
Size: 1456 Color: 1
Size: 928 Color: 1

Bin 102: 10 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 0
Size: 3582 Color: 1

Bin 103: 10 of cap free
Amount of items: 2
Items: 
Size: 13873 Color: 0
Size: 2149 Color: 1

Bin 104: 10 of cap free
Amount of items: 2
Items: 
Size: 13929 Color: 0
Size: 2093 Color: 1

Bin 105: 10 of cap free
Amount of items: 2
Items: 
Size: 14002 Color: 0
Size: 2020 Color: 1

Bin 106: 10 of cap free
Amount of items: 2
Items: 
Size: 14189 Color: 0
Size: 1833 Color: 1

Bin 107: 11 of cap free
Amount of items: 3
Items: 
Size: 9613 Color: 1
Size: 5904 Color: 1
Size: 504 Color: 0

Bin 108: 11 of cap free
Amount of items: 3
Items: 
Size: 12180 Color: 1
Size: 3625 Color: 0
Size: 216 Color: 1

Bin 109: 11 of cap free
Amount of items: 2
Items: 
Size: 13861 Color: 0
Size: 2160 Color: 1

Bin 110: 11 of cap free
Amount of items: 2
Items: 
Size: 14037 Color: 0
Size: 1984 Color: 1

Bin 111: 11 of cap free
Amount of items: 2
Items: 
Size: 14233 Color: 1
Size: 1788 Color: 0

Bin 112: 11 of cap free
Amount of items: 2
Items: 
Size: 14237 Color: 0
Size: 1784 Color: 1

Bin 113: 12 of cap free
Amount of items: 3
Items: 
Size: 8042 Color: 0
Size: 7786 Color: 1
Size: 192 Color: 0

Bin 114: 12 of cap free
Amount of items: 2
Items: 
Size: 12732 Color: 1
Size: 3288 Color: 0

Bin 115: 14 of cap free
Amount of items: 3
Items: 
Size: 12306 Color: 0
Size: 2101 Color: 0
Size: 1611 Color: 1

Bin 116: 14 of cap free
Amount of items: 3
Items: 
Size: 12338 Color: 1
Size: 2528 Color: 0
Size: 1152 Color: 1

Bin 117: 14 of cap free
Amount of items: 2
Items: 
Size: 12680 Color: 0
Size: 3338 Color: 1

Bin 118: 14 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 0
Size: 2602 Color: 1

Bin 119: 14 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 0
Size: 2316 Color: 1

Bin 120: 14 of cap free
Amount of items: 2
Items: 
Size: 14324 Color: 0
Size: 1694 Color: 1

Bin 121: 15 of cap free
Amount of items: 2
Items: 
Size: 11115 Color: 0
Size: 4902 Color: 1

Bin 122: 15 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 1
Size: 4089 Color: 0

Bin 123: 15 of cap free
Amount of items: 2
Items: 
Size: 13009 Color: 1
Size: 3008 Color: 0

Bin 124: 15 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 0
Size: 2184 Color: 1

Bin 125: 16 of cap free
Amount of items: 2
Items: 
Size: 12636 Color: 1
Size: 3380 Color: 0

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 0
Size: 3400 Color: 1

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 13260 Color: 1
Size: 2756 Color: 0

Bin 128: 17 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 1
Size: 3063 Color: 0

Bin 129: 17 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 0
Size: 1953 Color: 1

Bin 130: 18 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 0
Size: 2269 Color: 1

Bin 131: 20 of cap free
Amount of items: 2
Items: 
Size: 12800 Color: 0
Size: 3212 Color: 1

Bin 132: 20 of cap free
Amount of items: 2
Items: 
Size: 13689 Color: 1
Size: 2323 Color: 0

Bin 133: 20 of cap free
Amount of items: 2
Items: 
Size: 14095 Color: 0
Size: 1917 Color: 1

Bin 134: 21 of cap free
Amount of items: 2
Items: 
Size: 13283 Color: 0
Size: 2728 Color: 1

Bin 135: 22 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 0
Size: 2290 Color: 1

Bin 136: 23 of cap free
Amount of items: 2
Items: 
Size: 13780 Color: 0
Size: 2229 Color: 1

Bin 137: 23 of cap free
Amount of items: 3
Items: 
Size: 14150 Color: 1
Size: 1731 Color: 0
Size: 128 Color: 1

Bin 138: 24 of cap free
Amount of items: 3
Items: 
Size: 9618 Color: 0
Size: 5672 Color: 0
Size: 718 Color: 1

Bin 139: 24 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 0
Size: 3036 Color: 1
Size: 724 Color: 0

Bin 140: 25 of cap free
Amount of items: 2
Items: 
Size: 14111 Color: 1
Size: 1896 Color: 0

Bin 141: 26 of cap free
Amount of items: 2
Items: 
Size: 13144 Color: 0
Size: 2862 Color: 1

Bin 142: 29 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 0
Size: 6671 Color: 1
Size: 1052 Color: 0

Bin 143: 29 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 0
Size: 2055 Color: 1

Bin 144: 29 of cap free
Amount of items: 3
Items: 
Size: 14302 Color: 1
Size: 1605 Color: 0
Size: 96 Color: 1

Bin 145: 30 of cap free
Amount of items: 2
Items: 
Size: 13529 Color: 0
Size: 2473 Color: 1

Bin 146: 33 of cap free
Amount of items: 2
Items: 
Size: 10463 Color: 0
Size: 5536 Color: 1

Bin 147: 34 of cap free
Amount of items: 2
Items: 
Size: 10732 Color: 0
Size: 5266 Color: 1

Bin 148: 34 of cap free
Amount of items: 2
Items: 
Size: 14216 Color: 1
Size: 1782 Color: 0

Bin 149: 35 of cap free
Amount of items: 2
Items: 
Size: 10216 Color: 0
Size: 5781 Color: 1

Bin 150: 35 of cap free
Amount of items: 2
Items: 
Size: 11738 Color: 0
Size: 4259 Color: 1

Bin 151: 35 of cap free
Amount of items: 2
Items: 
Size: 12565 Color: 1
Size: 3432 Color: 0

Bin 152: 35 of cap free
Amount of items: 2
Items: 
Size: 13311 Color: 1
Size: 2686 Color: 0

Bin 153: 35 of cap free
Amount of items: 2
Items: 
Size: 13910 Color: 0
Size: 2087 Color: 1

Bin 154: 37 of cap free
Amount of items: 2
Items: 
Size: 11971 Color: 0
Size: 4024 Color: 1

Bin 155: 39 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 0
Size: 1615 Color: 1

Bin 156: 40 of cap free
Amount of items: 2
Items: 
Size: 12910 Color: 1
Size: 3082 Color: 0

Bin 157: 40 of cap free
Amount of items: 2
Items: 
Size: 13245 Color: 1
Size: 2747 Color: 0

Bin 158: 44 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 1
Size: 3884 Color: 0

Bin 159: 45 of cap free
Amount of items: 2
Items: 
Size: 12602 Color: 0
Size: 3385 Color: 1

Bin 160: 45 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 1
Size: 2351 Color: 0

Bin 161: 46 of cap free
Amount of items: 2
Items: 
Size: 13213 Color: 0
Size: 2773 Color: 1

Bin 162: 46 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 1
Size: 1942 Color: 0

Bin 163: 47 of cap free
Amount of items: 2
Items: 
Size: 11507 Color: 0
Size: 4478 Color: 1

Bin 164: 49 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 0
Size: 3207 Color: 1

Bin 165: 50 of cap free
Amount of items: 2
Items: 
Size: 10036 Color: 1
Size: 5946 Color: 0

Bin 166: 53 of cap free
Amount of items: 2
Items: 
Size: 12153 Color: 0
Size: 3826 Color: 1

Bin 167: 53 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 0
Size: 1811 Color: 1

Bin 168: 60 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 0
Size: 1890 Color: 1

Bin 169: 62 of cap free
Amount of items: 2
Items: 
Size: 9824 Color: 1
Size: 6146 Color: 0

Bin 170: 63 of cap free
Amount of items: 3
Items: 
Size: 9095 Color: 1
Size: 6682 Color: 0
Size: 192 Color: 1

Bin 171: 65 of cap free
Amount of items: 2
Items: 
Size: 12855 Color: 0
Size: 3112 Color: 1

Bin 172: 66 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 1
Size: 1626 Color: 0

Bin 173: 67 of cap free
Amount of items: 2
Items: 
Size: 13957 Color: 1
Size: 2008 Color: 0

Bin 174: 71 of cap free
Amount of items: 2
Items: 
Size: 13286 Color: 1
Size: 2675 Color: 0

Bin 175: 73 of cap free
Amount of items: 2
Items: 
Size: 10955 Color: 0
Size: 5004 Color: 1

Bin 176: 75 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 1
Size: 1771 Color: 0

Bin 177: 78 of cap free
Amount of items: 2
Items: 
Size: 8034 Color: 1
Size: 7920 Color: 0

Bin 178: 79 of cap free
Amount of items: 2
Items: 
Size: 13097 Color: 0
Size: 2856 Color: 1

Bin 179: 81 of cap free
Amount of items: 2
Items: 
Size: 13905 Color: 0
Size: 2046 Color: 1

Bin 180: 82 of cap free
Amount of items: 2
Items: 
Size: 10741 Color: 1
Size: 5209 Color: 0

Bin 181: 88 of cap free
Amount of items: 2
Items: 
Size: 12533 Color: 1
Size: 3411 Color: 0

Bin 182: 93 of cap free
Amount of items: 2
Items: 
Size: 9988 Color: 0
Size: 5951 Color: 1

Bin 183: 93 of cap free
Amount of items: 2
Items: 
Size: 13591 Color: 1
Size: 2348 Color: 0

Bin 184: 95 of cap free
Amount of items: 2
Items: 
Size: 14053 Color: 0
Size: 1884 Color: 1

Bin 185: 96 of cap free
Amount of items: 40
Items: 
Size: 544 Color: 1
Size: 536 Color: 1
Size: 528 Color: 1
Size: 528 Color: 1
Size: 512 Color: 1
Size: 512 Color: 1
Size: 488 Color: 1
Size: 480 Color: 1
Size: 440 Color: 1
Size: 428 Color: 0
Size: 418 Color: 1
Size: 418 Color: 0
Size: 416 Color: 0
Size: 408 Color: 0
Size: 400 Color: 0
Size: 392 Color: 1
Size: 390 Color: 0
Size: 388 Color: 1
Size: 388 Color: 0
Size: 384 Color: 1
Size: 384 Color: 1
Size: 370 Color: 0
Size: 368 Color: 0
Size: 366 Color: 1
Size: 360 Color: 0
Size: 356 Color: 0
Size: 354 Color: 0
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 0
Size: 350 Color: 1
Size: 344 Color: 1
Size: 344 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 332 Color: 0
Size: 328 Color: 0
Size: 322 Color: 1
Size: 320 Color: 0
Size: 312 Color: 0

Bin 186: 101 of cap free
Amount of items: 2
Items: 
Size: 11075 Color: 0
Size: 4856 Color: 1

Bin 187: 110 of cap free
Amount of items: 2
Items: 
Size: 14169 Color: 1
Size: 1753 Color: 0

Bin 188: 115 of cap free
Amount of items: 2
Items: 
Size: 9240 Color: 0
Size: 6677 Color: 1

Bin 189: 123 of cap free
Amount of items: 2
Items: 
Size: 11086 Color: 1
Size: 4823 Color: 0

Bin 190: 127 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 0
Size: 2009 Color: 1

Bin 191: 132 of cap free
Amount of items: 2
Items: 
Size: 11480 Color: 0
Size: 4420 Color: 1

Bin 192: 139 of cap free
Amount of items: 11
Items: 
Size: 8017 Color: 0
Size: 892 Color: 1
Size: 832 Color: 0
Size: 830 Color: 1
Size: 826 Color: 1
Size: 818 Color: 1
Size: 816 Color: 1
Size: 764 Color: 0
Size: 754 Color: 1
Size: 672 Color: 0
Size: 672 Color: 0

Bin 193: 140 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 1
Size: 6684 Color: 0

Bin 194: 145 of cap free
Amount of items: 2
Items: 
Size: 11476 Color: 0
Size: 4411 Color: 1

Bin 195: 146 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 0
Size: 6256 Color: 1
Size: 1601 Color: 0

Bin 196: 151 of cap free
Amount of items: 2
Items: 
Size: 13626 Color: 0
Size: 2255 Color: 1

Bin 197: 160 of cap free
Amount of items: 2
Items: 
Size: 13220 Color: 1
Size: 2652 Color: 0

Bin 198: 259 of cap free
Amount of items: 2
Items: 
Size: 9092 Color: 1
Size: 6681 Color: 0

Bin 199: 10898 of cap free
Amount of items: 18
Items: 
Size: 320 Color: 1
Size: 320 Color: 1
Size: 310 Color: 0
Size: 304 Color: 1
Size: 304 Color: 0
Size: 298 Color: 1
Size: 298 Color: 0
Size: 296 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 280 Color: 1
Size: 280 Color: 1
Size: 272 Color: 1
Size: 272 Color: 0
Size: 272 Color: 0
Size: 272 Color: 0
Size: 268 Color: 1
Size: 192 Color: 0

Total size: 3174336
Total free space: 16032

