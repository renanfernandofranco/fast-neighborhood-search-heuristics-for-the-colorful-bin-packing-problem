Capicity Bin: 1000001
Lower Bound: 899

Bins used: 901
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 574425 Color: 0
Size: 425576 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 640355 Color: 1
Size: 190044 Color: 0
Size: 169602 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 718625 Color: 0
Size: 281376 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 739105 Color: 1
Size: 131066 Color: 1
Size: 129830 Color: 0

Bin 5: 1 of cap free
Amount of items: 3
Items: 
Size: 456008 Color: 1
Size: 352265 Color: 0
Size: 191727 Color: 1

Bin 6: 1 of cap free
Amount of items: 2
Items: 
Size: 594829 Color: 0
Size: 405171 Color: 1

Bin 7: 1 of cap free
Amount of items: 3
Items: 
Size: 661174 Color: 1
Size: 186901 Color: 1
Size: 151925 Color: 0

Bin 8: 1 of cap free
Amount of items: 3
Items: 
Size: 737360 Color: 0
Size: 134104 Color: 1
Size: 128536 Color: 1

Bin 9: 1 of cap free
Amount of items: 2
Items: 
Size: 794491 Color: 1
Size: 205509 Color: 0

Bin 10: 2 of cap free
Amount of items: 2
Items: 
Size: 542541 Color: 0
Size: 457458 Color: 1

Bin 11: 2 of cap free
Amount of items: 3
Items: 
Size: 595547 Color: 1
Size: 215071 Color: 1
Size: 189381 Color: 0

Bin 12: 2 of cap free
Amount of items: 3
Items: 
Size: 683785 Color: 0
Size: 159532 Color: 1
Size: 156682 Color: 0

Bin 13: 2 of cap free
Amount of items: 3
Items: 
Size: 702567 Color: 1
Size: 150340 Color: 0
Size: 147092 Color: 1

Bin 14: 2 of cap free
Amount of items: 3
Items: 
Size: 724652 Color: 1
Size: 139889 Color: 0
Size: 135458 Color: 1

Bin 15: 2 of cap free
Amount of items: 2
Items: 
Size: 777056 Color: 1
Size: 222943 Color: 0

Bin 16: 3 of cap free
Amount of items: 3
Items: 
Size: 574452 Color: 0
Size: 285800 Color: 1
Size: 139746 Color: 0

Bin 17: 3 of cap free
Amount of items: 2
Items: 
Size: 622463 Color: 1
Size: 377535 Color: 0

Bin 18: 3 of cap free
Amount of items: 3
Items: 
Size: 699216 Color: 1
Size: 168963 Color: 1
Size: 131819 Color: 0

Bin 19: 3 of cap free
Amount of items: 3
Items: 
Size: 708220 Color: 0
Size: 162909 Color: 0
Size: 128869 Color: 1

Bin 20: 3 of cap free
Amount of items: 3
Items: 
Size: 739378 Color: 0
Size: 148945 Color: 0
Size: 111675 Color: 1

Bin 21: 4 of cap free
Amount of items: 3
Items: 
Size: 502315 Color: 1
Size: 349615 Color: 0
Size: 148067 Color: 1

Bin 22: 4 of cap free
Amount of items: 2
Items: 
Size: 544370 Color: 1
Size: 455627 Color: 0

Bin 23: 4 of cap free
Amount of items: 3
Items: 
Size: 698982 Color: 1
Size: 155933 Color: 0
Size: 145082 Color: 0

Bin 24: 4 of cap free
Amount of items: 3
Items: 
Size: 727727 Color: 1
Size: 136804 Color: 1
Size: 135466 Color: 0

Bin 25: 4 of cap free
Amount of items: 2
Items: 
Size: 770503 Color: 0
Size: 229494 Color: 1

Bin 26: 5 of cap free
Amount of items: 3
Items: 
Size: 638455 Color: 1
Size: 191551 Color: 0
Size: 169990 Color: 1

Bin 27: 5 of cap free
Amount of items: 3
Items: 
Size: 640532 Color: 0
Size: 182706 Color: 0
Size: 176758 Color: 1

Bin 28: 5 of cap free
Amount of items: 2
Items: 
Size: 655615 Color: 0
Size: 344381 Color: 1

Bin 29: 5 of cap free
Amount of items: 3
Items: 
Size: 662365 Color: 0
Size: 196361 Color: 1
Size: 141270 Color: 0

Bin 30: 5 of cap free
Amount of items: 3
Items: 
Size: 662721 Color: 1
Size: 169083 Color: 0
Size: 168192 Color: 1

Bin 31: 5 of cap free
Amount of items: 3
Items: 
Size: 784771 Color: 1
Size: 112786 Color: 0
Size: 102439 Color: 1

Bin 32: 6 of cap free
Amount of items: 3
Items: 
Size: 448769 Color: 1
Size: 370573 Color: 1
Size: 180653 Color: 0

Bin 33: 6 of cap free
Amount of items: 3
Items: 
Size: 570193 Color: 0
Size: 242924 Color: 0
Size: 186878 Color: 1

Bin 34: 6 of cap free
Amount of items: 3
Items: 
Size: 637454 Color: 0
Size: 226937 Color: 1
Size: 135604 Color: 0

Bin 35: 6 of cap free
Amount of items: 3
Items: 
Size: 708947 Color: 1
Size: 171108 Color: 1
Size: 119940 Color: 0

Bin 36: 6 of cap free
Amount of items: 3
Items: 
Size: 722997 Color: 0
Size: 151304 Color: 1
Size: 125694 Color: 0

Bin 37: 6 of cap free
Amount of items: 3
Items: 
Size: 724607 Color: 1
Size: 151206 Color: 0
Size: 124182 Color: 0

Bin 38: 6 of cap free
Amount of items: 3
Items: 
Size: 776642 Color: 0
Size: 113473 Color: 0
Size: 109880 Color: 1

Bin 39: 7 of cap free
Amount of items: 3
Items: 
Size: 577314 Color: 1
Size: 274873 Color: 1
Size: 147807 Color: 0

Bin 40: 7 of cap free
Amount of items: 3
Items: 
Size: 661854 Color: 0
Size: 171043 Color: 1
Size: 167097 Color: 0

Bin 41: 7 of cap free
Amount of items: 3
Items: 
Size: 723340 Color: 0
Size: 139375 Color: 0
Size: 137279 Color: 1

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 750202 Color: 0
Size: 249792 Color: 1

Bin 43: 7 of cap free
Amount of items: 3
Items: 
Size: 759847 Color: 0
Size: 132133 Color: 0
Size: 108014 Color: 1

Bin 44: 7 of cap free
Amount of items: 3
Items: 
Size: 759549 Color: 1
Size: 125955 Color: 0
Size: 114490 Color: 1

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 774874 Color: 0
Size: 117116 Color: 0
Size: 108004 Color: 1

Bin 46: 8 of cap free
Amount of items: 3
Items: 
Size: 697518 Color: 1
Size: 162618 Color: 1
Size: 139857 Color: 0

Bin 47: 8 of cap free
Amount of items: 3
Items: 
Size: 718940 Color: 1
Size: 146582 Color: 1
Size: 134471 Color: 0

Bin 48: 8 of cap free
Amount of items: 3
Items: 
Size: 724072 Color: 1
Size: 146393 Color: 1
Size: 129528 Color: 0

Bin 49: 9 of cap free
Amount of items: 3
Items: 
Size: 580961 Color: 1
Size: 272979 Color: 1
Size: 146052 Color: 0

Bin 50: 9 of cap free
Amount of items: 3
Items: 
Size: 685122 Color: 0
Size: 190293 Color: 1
Size: 124577 Color: 0

Bin 51: 9 of cap free
Amount of items: 3
Items: 
Size: 699667 Color: 1
Size: 164816 Color: 1
Size: 135509 Color: 0

Bin 52: 11 of cap free
Amount of items: 3
Items: 
Size: 582836 Color: 1
Size: 274214 Color: 1
Size: 142940 Color: 0

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 641197 Color: 0
Size: 186358 Color: 1
Size: 172435 Color: 1

Bin 54: 11 of cap free
Amount of items: 3
Items: 
Size: 660811 Color: 0
Size: 178417 Color: 1
Size: 160762 Color: 0

Bin 55: 12 of cap free
Amount of items: 3
Items: 
Size: 454556 Color: 1
Size: 380882 Color: 0
Size: 164551 Color: 1

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 493262 Color: 1
Size: 359621 Color: 0
Size: 147106 Color: 1

Bin 57: 12 of cap free
Amount of items: 2
Items: 
Size: 733890 Color: 1
Size: 266099 Color: 0

Bin 58: 12 of cap free
Amount of items: 3
Items: 
Size: 737839 Color: 0
Size: 138059 Color: 1
Size: 124091 Color: 0

Bin 59: 13 of cap free
Amount of items: 2
Items: 
Size: 507647 Color: 1
Size: 492341 Color: 0

Bin 60: 13 of cap free
Amount of items: 2
Items: 
Size: 543728 Color: 0
Size: 456260 Color: 1

Bin 61: 13 of cap free
Amount of items: 3
Items: 
Size: 641318 Color: 0
Size: 189859 Color: 1
Size: 168811 Color: 0

Bin 62: 13 of cap free
Amount of items: 3
Items: 
Size: 661249 Color: 0
Size: 184201 Color: 0
Size: 154538 Color: 1

Bin 63: 13 of cap free
Amount of items: 3
Items: 
Size: 663213 Color: 0
Size: 170767 Color: 1
Size: 166008 Color: 0

Bin 64: 13 of cap free
Amount of items: 2
Items: 
Size: 720736 Color: 0
Size: 279252 Color: 1

Bin 65: 15 of cap free
Amount of items: 2
Items: 
Size: 554070 Color: 0
Size: 445916 Color: 1

Bin 66: 15 of cap free
Amount of items: 3
Items: 
Size: 593983 Color: 0
Size: 245730 Color: 0
Size: 160273 Color: 1

Bin 67: 16 of cap free
Amount of items: 2
Items: 
Size: 599740 Color: 1
Size: 400245 Color: 0

Bin 68: 16 of cap free
Amount of items: 3
Items: 
Size: 718167 Color: 0
Size: 145953 Color: 0
Size: 135865 Color: 1

Bin 69: 17 of cap free
Amount of items: 2
Items: 
Size: 529384 Color: 1
Size: 470600 Color: 0

Bin 70: 17 of cap free
Amount of items: 2
Items: 
Size: 560595 Color: 1
Size: 439389 Color: 0

Bin 71: 17 of cap free
Amount of items: 3
Items: 
Size: 661150 Color: 1
Size: 195664 Color: 0
Size: 143170 Color: 1

Bin 72: 17 of cap free
Amount of items: 3
Items: 
Size: 699861 Color: 1
Size: 161757 Color: 0
Size: 138366 Color: 0

Bin 73: 17 of cap free
Amount of items: 2
Items: 
Size: 759408 Color: 1
Size: 240576 Color: 0

Bin 74: 17 of cap free
Amount of items: 2
Items: 
Size: 779805 Color: 0
Size: 220179 Color: 1

Bin 75: 18 of cap free
Amount of items: 2
Items: 
Size: 552311 Color: 1
Size: 447672 Color: 0

Bin 76: 19 of cap free
Amount of items: 2
Items: 
Size: 649490 Color: 0
Size: 350492 Color: 1

Bin 77: 20 of cap free
Amount of items: 3
Items: 
Size: 660379 Color: 0
Size: 171007 Color: 1
Size: 168595 Color: 1

Bin 78: 20 of cap free
Amount of items: 3
Items: 
Size: 662862 Color: 1
Size: 174577 Color: 1
Size: 162542 Color: 0

Bin 79: 20 of cap free
Amount of items: 3
Items: 
Size: 738519 Color: 0
Size: 133180 Color: 1
Size: 128282 Color: 1

Bin 80: 21 of cap free
Amount of items: 3
Items: 
Size: 577690 Color: 0
Size: 241186 Color: 0
Size: 181104 Color: 1

Bin 81: 21 of cap free
Amount of items: 3
Items: 
Size: 600292 Color: 1
Size: 220776 Color: 0
Size: 178912 Color: 1

Bin 82: 21 of cap free
Amount of items: 3
Items: 
Size: 701038 Color: 0
Size: 154984 Color: 0
Size: 143958 Color: 1

Bin 83: 21 of cap free
Amount of items: 3
Items: 
Size: 738174 Color: 0
Size: 147635 Color: 1
Size: 114171 Color: 1

Bin 84: 21 of cap free
Amount of items: 3
Items: 
Size: 762438 Color: 0
Size: 131028 Color: 0
Size: 106514 Color: 1

Bin 85: 22 of cap free
Amount of items: 2
Items: 
Size: 614580 Color: 0
Size: 385399 Color: 1

Bin 86: 22 of cap free
Amount of items: 3
Items: 
Size: 637576 Color: 1
Size: 217651 Color: 0
Size: 144752 Color: 1

Bin 87: 23 of cap free
Amount of items: 2
Items: 
Size: 541921 Color: 1
Size: 458057 Color: 0

Bin 88: 23 of cap free
Amount of items: 3
Items: 
Size: 579781 Color: 0
Size: 242949 Color: 0
Size: 177248 Color: 1

Bin 89: 23 of cap free
Amount of items: 3
Items: 
Size: 702767 Color: 0
Size: 156304 Color: 0
Size: 140907 Color: 1

Bin 90: 23 of cap free
Amount of items: 2
Items: 
Size: 739722 Color: 1
Size: 260256 Color: 0

Bin 91: 24 of cap free
Amount of items: 2
Items: 
Size: 602997 Color: 1
Size: 396980 Color: 0

Bin 92: 25 of cap free
Amount of items: 2
Items: 
Size: 603929 Color: 0
Size: 396047 Color: 1

Bin 93: 25 of cap free
Amount of items: 3
Items: 
Size: 684516 Color: 1
Size: 160678 Color: 1
Size: 154782 Color: 0

Bin 94: 25 of cap free
Amount of items: 2
Items: 
Size: 685159 Color: 1
Size: 314817 Color: 0

Bin 95: 25 of cap free
Amount of items: 2
Items: 
Size: 796089 Color: 1
Size: 203887 Color: 0

Bin 96: 26 of cap free
Amount of items: 3
Items: 
Size: 638897 Color: 1
Size: 186402 Color: 0
Size: 174676 Color: 1

Bin 97: 26 of cap free
Amount of items: 3
Items: 
Size: 728255 Color: 0
Size: 137350 Color: 1
Size: 134370 Color: 0

Bin 98: 27 of cap free
Amount of items: 3
Items: 
Size: 570665 Color: 1
Size: 285928 Color: 1
Size: 143381 Color: 0

Bin 99: 27 of cap free
Amount of items: 2
Items: 
Size: 653649 Color: 0
Size: 346325 Color: 1

Bin 100: 28 of cap free
Amount of items: 3
Items: 
Size: 638257 Color: 0
Size: 181187 Color: 1
Size: 180529 Color: 0

Bin 101: 28 of cap free
Amount of items: 2
Items: 
Size: 692029 Color: 1
Size: 307944 Color: 0

Bin 102: 28 of cap free
Amount of items: 3
Items: 
Size: 698371 Color: 1
Size: 154212 Color: 0
Size: 147390 Color: 1

Bin 103: 29 of cap free
Amount of items: 2
Items: 
Size: 584840 Color: 0
Size: 415132 Color: 1

Bin 104: 29 of cap free
Amount of items: 3
Items: 
Size: 638462 Color: 0
Size: 187752 Color: 0
Size: 173758 Color: 1

Bin 105: 29 of cap free
Amount of items: 3
Items: 
Size: 699218 Color: 1
Size: 156042 Color: 0
Size: 144712 Color: 0

Bin 106: 29 of cap free
Amount of items: 3
Items: 
Size: 702414 Color: 1
Size: 160834 Color: 1
Size: 136724 Color: 0

Bin 107: 29 of cap free
Amount of items: 3
Items: 
Size: 773352 Color: 1
Size: 117387 Color: 0
Size: 109233 Color: 1

Bin 108: 29 of cap free
Amount of items: 2
Items: 
Size: 790941 Color: 0
Size: 209031 Color: 1

Bin 109: 30 of cap free
Amount of items: 3
Items: 
Size: 593139 Color: 1
Size: 214678 Color: 1
Size: 192154 Color: 0

Bin 110: 30 of cap free
Amount of items: 3
Items: 
Size: 727506 Color: 1
Size: 138615 Color: 1
Size: 133850 Color: 0

Bin 111: 31 of cap free
Amount of items: 2
Items: 
Size: 668782 Color: 0
Size: 331188 Color: 1

Bin 112: 33 of cap free
Amount of items: 3
Items: 
Size: 738483 Color: 0
Size: 137020 Color: 0
Size: 124465 Color: 1

Bin 113: 33 of cap free
Amount of items: 2
Items: 
Size: 740857 Color: 0
Size: 259111 Color: 1

Bin 114: 34 of cap free
Amount of items: 2
Items: 
Size: 603665 Color: 0
Size: 396302 Color: 1

Bin 115: 34 of cap free
Amount of items: 3
Items: 
Size: 641475 Color: 0
Size: 193487 Color: 1
Size: 165005 Color: 1

Bin 116: 35 of cap free
Amount of items: 3
Items: 
Size: 765199 Color: 1
Size: 128249 Color: 0
Size: 106518 Color: 1

Bin 117: 36 of cap free
Amount of items: 2
Items: 
Size: 773603 Color: 0
Size: 226362 Color: 1

Bin 118: 37 of cap free
Amount of items: 2
Items: 
Size: 696794 Color: 0
Size: 303170 Color: 1

Bin 119: 37 of cap free
Amount of items: 3
Items: 
Size: 702525 Color: 1
Size: 170556 Color: 0
Size: 126883 Color: 1

Bin 120: 38 of cap free
Amount of items: 3
Items: 
Size: 570586 Color: 1
Size: 224804 Color: 1
Size: 204573 Color: 0

Bin 121: 38 of cap free
Amount of items: 2
Items: 
Size: 629404 Color: 1
Size: 370559 Color: 0

Bin 122: 38 of cap free
Amount of items: 3
Items: 
Size: 718698 Color: 0
Size: 146714 Color: 1
Size: 134551 Color: 0

Bin 123: 39 of cap free
Amount of items: 2
Items: 
Size: 585376 Color: 0
Size: 414586 Color: 1

Bin 124: 39 of cap free
Amount of items: 3
Items: 
Size: 727478 Color: 0
Size: 145436 Color: 1
Size: 127048 Color: 0

Bin 125: 39 of cap free
Amount of items: 3
Items: 
Size: 792630 Color: 0
Size: 107046 Color: 1
Size: 100286 Color: 1

Bin 126: 40 of cap free
Amount of items: 2
Items: 
Size: 763687 Color: 0
Size: 236274 Color: 1

Bin 127: 41 of cap free
Amount of items: 2
Items: 
Size: 526831 Color: 0
Size: 473129 Color: 1

Bin 128: 41 of cap free
Amount of items: 3
Items: 
Size: 557117 Color: 1
Size: 304625 Color: 0
Size: 138218 Color: 1

Bin 129: 43 of cap free
Amount of items: 3
Items: 
Size: 495471 Color: 1
Size: 319294 Color: 0
Size: 185193 Color: 0

Bin 130: 43 of cap free
Amount of items: 3
Items: 
Size: 578989 Color: 1
Size: 224905 Color: 1
Size: 196064 Color: 0

Bin 131: 43 of cap free
Amount of items: 2
Items: 
Size: 680651 Color: 0
Size: 319307 Color: 1

Bin 132: 44 of cap free
Amount of items: 3
Items: 
Size: 698189 Color: 1
Size: 183593 Color: 0
Size: 118175 Color: 0

Bin 133: 44 of cap free
Amount of items: 2
Items: 
Size: 791654 Color: 1
Size: 208303 Color: 0

Bin 134: 45 of cap free
Amount of items: 3
Items: 
Size: 593617 Color: 1
Size: 246402 Color: 0
Size: 159937 Color: 1

Bin 135: 45 of cap free
Amount of items: 2
Items: 
Size: 616171 Color: 0
Size: 383785 Color: 1

Bin 136: 46 of cap free
Amount of items: 3
Items: 
Size: 456020 Color: 1
Size: 359791 Color: 0
Size: 184144 Color: 0

Bin 137: 46 of cap free
Amount of items: 3
Items: 
Size: 760009 Color: 0
Size: 132020 Color: 0
Size: 107926 Color: 1

Bin 138: 46 of cap free
Amount of items: 3
Items: 
Size: 762929 Color: 0
Size: 124117 Color: 1
Size: 112909 Color: 1

Bin 139: 49 of cap free
Amount of items: 2
Items: 
Size: 574138 Color: 0
Size: 425814 Color: 1

Bin 140: 49 of cap free
Amount of items: 3
Items: 
Size: 700757 Color: 0
Size: 166201 Color: 1
Size: 132994 Color: 1

Bin 141: 49 of cap free
Amount of items: 2
Items: 
Size: 775424 Color: 0
Size: 224528 Color: 1

Bin 142: 50 of cap free
Amount of items: 3
Items: 
Size: 580096 Color: 0
Size: 294138 Color: 1
Size: 125717 Color: 0

Bin 143: 50 of cap free
Amount of items: 3
Items: 
Size: 788520 Color: 1
Size: 106838 Color: 1
Size: 104593 Color: 0

Bin 144: 52 of cap free
Amount of items: 2
Items: 
Size: 756782 Color: 0
Size: 243167 Color: 1

Bin 145: 52 of cap free
Amount of items: 3
Items: 
Size: 775217 Color: 0
Size: 112961 Color: 1
Size: 111771 Color: 1

Bin 146: 53 of cap free
Amount of items: 2
Items: 
Size: 625936 Color: 1
Size: 374012 Color: 0

Bin 147: 53 of cap free
Amount of items: 3
Items: 
Size: 724156 Color: 1
Size: 143486 Color: 0
Size: 132306 Color: 1

Bin 148: 54 of cap free
Amount of items: 2
Items: 
Size: 709070 Color: 1
Size: 290877 Color: 0

Bin 149: 55 of cap free
Amount of items: 2
Items: 
Size: 542456 Color: 1
Size: 457490 Color: 0

Bin 150: 55 of cap free
Amount of items: 2
Items: 
Size: 556406 Color: 0
Size: 443540 Color: 1

Bin 151: 55 of cap free
Amount of items: 2
Items: 
Size: 670655 Color: 0
Size: 329291 Color: 1

Bin 152: 56 of cap free
Amount of items: 2
Items: 
Size: 539492 Color: 1
Size: 460453 Color: 0

Bin 153: 57 of cap free
Amount of items: 3
Items: 
Size: 528132 Color: 0
Size: 305589 Color: 1
Size: 166223 Color: 0

Bin 154: 58 of cap free
Amount of items: 2
Items: 
Size: 760327 Color: 1
Size: 239616 Color: 0

Bin 155: 59 of cap free
Amount of items: 3
Items: 
Size: 552077 Color: 1
Size: 294231 Color: 1
Size: 153634 Color: 0

Bin 156: 60 of cap free
Amount of items: 2
Items: 
Size: 507802 Color: 1
Size: 492139 Color: 0

Bin 157: 60 of cap free
Amount of items: 2
Items: 
Size: 519451 Color: 1
Size: 480490 Color: 0

Bin 158: 60 of cap free
Amount of items: 2
Items: 
Size: 743361 Color: 1
Size: 256580 Color: 0

Bin 159: 61 of cap free
Amount of items: 2
Items: 
Size: 793174 Color: 1
Size: 206766 Color: 0

Bin 160: 62 of cap free
Amount of items: 2
Items: 
Size: 608395 Color: 1
Size: 391544 Color: 0

Bin 161: 62 of cap free
Amount of items: 2
Items: 
Size: 715179 Color: 1
Size: 284760 Color: 0

Bin 162: 63 of cap free
Amount of items: 3
Items: 
Size: 655734 Color: 0
Size: 178372 Color: 0
Size: 165832 Color: 1

Bin 163: 64 of cap free
Amount of items: 2
Items: 
Size: 776035 Color: 1
Size: 223902 Color: 0

Bin 164: 65 of cap free
Amount of items: 3
Items: 
Size: 640876 Color: 0
Size: 240554 Color: 0
Size: 118506 Color: 1

Bin 165: 65 of cap free
Amount of items: 2
Items: 
Size: 799921 Color: 1
Size: 200015 Color: 0

Bin 166: 67 of cap free
Amount of items: 3
Items: 
Size: 619530 Color: 0
Size: 214425 Color: 1
Size: 165979 Color: 1

Bin 167: 67 of cap free
Amount of items: 3
Items: 
Size: 655961 Color: 1
Size: 179379 Color: 0
Size: 164594 Color: 1

Bin 168: 68 of cap free
Amount of items: 3
Items: 
Size: 595836 Color: 0
Size: 244475 Color: 1
Size: 159622 Color: 0

Bin 169: 68 of cap free
Amount of items: 2
Items: 
Size: 705222 Color: 1
Size: 294711 Color: 0

Bin 170: 68 of cap free
Amount of items: 2
Items: 
Size: 758933 Color: 1
Size: 241000 Color: 0

Bin 171: 69 of cap free
Amount of items: 2
Items: 
Size: 594798 Color: 0
Size: 405134 Color: 1

Bin 172: 69 of cap free
Amount of items: 2
Items: 
Size: 602605 Color: 1
Size: 397327 Color: 0

Bin 173: 69 of cap free
Amount of items: 3
Items: 
Size: 718696 Color: 1
Size: 144241 Color: 1
Size: 136995 Color: 0

Bin 174: 69 of cap free
Amount of items: 2
Items: 
Size: 796132 Color: 1
Size: 203800 Color: 0

Bin 175: 70 of cap free
Amount of items: 3
Items: 
Size: 686304 Color: 0
Size: 168816 Color: 1
Size: 144811 Color: 1

Bin 176: 71 of cap free
Amount of items: 2
Items: 
Size: 527395 Color: 1
Size: 472535 Color: 0

Bin 177: 72 of cap free
Amount of items: 2
Items: 
Size: 506169 Color: 1
Size: 493760 Color: 0

Bin 178: 72 of cap free
Amount of items: 2
Items: 
Size: 607352 Color: 1
Size: 392577 Color: 0

Bin 179: 73 of cap free
Amount of items: 2
Items: 
Size: 564975 Color: 1
Size: 434953 Color: 0

Bin 180: 73 of cap free
Amount of items: 2
Items: 
Size: 650619 Color: 1
Size: 349309 Color: 0

Bin 181: 75 of cap free
Amount of items: 3
Items: 
Size: 640152 Color: 1
Size: 180630 Color: 1
Size: 179144 Color: 0

Bin 182: 75 of cap free
Amount of items: 2
Items: 
Size: 766611 Color: 0
Size: 233315 Color: 1

Bin 183: 77 of cap free
Amount of items: 2
Items: 
Size: 737181 Color: 0
Size: 262743 Color: 1

Bin 184: 77 of cap free
Amount of items: 2
Items: 
Size: 744439 Color: 0
Size: 255485 Color: 1

Bin 185: 81 of cap free
Amount of items: 2
Items: 
Size: 570061 Color: 0
Size: 429859 Color: 1

Bin 186: 85 of cap free
Amount of items: 2
Items: 
Size: 675892 Color: 1
Size: 324024 Color: 0

Bin 187: 86 of cap free
Amount of items: 2
Items: 
Size: 631091 Color: 0
Size: 368824 Color: 1

Bin 188: 86 of cap free
Amount of items: 2
Items: 
Size: 781424 Color: 0
Size: 218491 Color: 1

Bin 189: 87 of cap free
Amount of items: 2
Items: 
Size: 685876 Color: 1
Size: 314038 Color: 0

Bin 190: 87 of cap free
Amount of items: 2
Items: 
Size: 787911 Color: 0
Size: 212003 Color: 1

Bin 191: 89 of cap free
Amount of items: 2
Items: 
Size: 519678 Color: 0
Size: 480234 Color: 1

Bin 192: 90 of cap free
Amount of items: 2
Items: 
Size: 643251 Color: 0
Size: 356660 Color: 1

Bin 193: 90 of cap free
Amount of items: 2
Items: 
Size: 770340 Color: 0
Size: 229571 Color: 1

Bin 194: 92 of cap free
Amount of items: 2
Items: 
Size: 608764 Color: 0
Size: 391145 Color: 1

Bin 195: 92 of cap free
Amount of items: 2
Items: 
Size: 616904 Color: 1
Size: 383005 Color: 0

Bin 196: 92 of cap free
Amount of items: 3
Items: 
Size: 760323 Color: 0
Size: 121766 Color: 0
Size: 117820 Color: 1

Bin 197: 95 of cap free
Amount of items: 3
Items: 
Size: 534401 Color: 0
Size: 305418 Color: 1
Size: 160087 Color: 0

Bin 198: 95 of cap free
Amount of items: 3
Items: 
Size: 573579 Color: 0
Size: 287018 Color: 1
Size: 139309 Color: 0

Bin 199: 96 of cap free
Amount of items: 3
Items: 
Size: 760801 Color: 1
Size: 120757 Color: 0
Size: 118347 Color: 1

Bin 200: 98 of cap free
Amount of items: 2
Items: 
Size: 607928 Color: 1
Size: 391975 Color: 0

Bin 201: 99 of cap free
Amount of items: 2
Items: 
Size: 736688 Color: 0
Size: 263214 Color: 1

Bin 202: 99 of cap free
Amount of items: 2
Items: 
Size: 766997 Color: 0
Size: 232905 Color: 1

Bin 203: 100 of cap free
Amount of items: 2
Items: 
Size: 531664 Color: 1
Size: 468237 Color: 0

Bin 204: 100 of cap free
Amount of items: 2
Items: 
Size: 596441 Color: 1
Size: 403460 Color: 0

Bin 205: 102 of cap free
Amount of items: 2
Items: 
Size: 649216 Color: 1
Size: 350683 Color: 0

Bin 206: 102 of cap free
Amount of items: 3
Items: 
Size: 683822 Color: 1
Size: 163486 Color: 0
Size: 152591 Color: 1

Bin 207: 104 of cap free
Amount of items: 2
Items: 
Size: 599177 Color: 0
Size: 400720 Color: 1

Bin 208: 104 of cap free
Amount of items: 2
Items: 
Size: 686626 Color: 1
Size: 313271 Color: 0

Bin 209: 105 of cap free
Amount of items: 2
Items: 
Size: 730373 Color: 0
Size: 269523 Color: 1

Bin 210: 106 of cap free
Amount of items: 2
Items: 
Size: 619266 Color: 0
Size: 380629 Color: 1

Bin 211: 106 of cap free
Amount of items: 2
Items: 
Size: 731597 Color: 0
Size: 268298 Color: 1

Bin 212: 106 of cap free
Amount of items: 2
Items: 
Size: 782361 Color: 0
Size: 217534 Color: 1

Bin 213: 107 of cap free
Amount of items: 2
Items: 
Size: 514941 Color: 1
Size: 484953 Color: 0

Bin 214: 107 of cap free
Amount of items: 3
Items: 
Size: 759512 Color: 0
Size: 133904 Color: 1
Size: 106478 Color: 1

Bin 215: 108 of cap free
Amount of items: 2
Items: 
Size: 529111 Color: 1
Size: 470782 Color: 0

Bin 216: 108 of cap free
Amount of items: 2
Items: 
Size: 751628 Color: 0
Size: 248265 Color: 1

Bin 217: 109 of cap free
Amount of items: 2
Items: 
Size: 513393 Color: 0
Size: 486499 Color: 1

Bin 218: 110 of cap free
Amount of items: 2
Items: 
Size: 666493 Color: 0
Size: 333398 Color: 1

Bin 219: 110 of cap free
Amount of items: 3
Items: 
Size: 758970 Color: 0
Size: 128577 Color: 0
Size: 112344 Color: 1

Bin 220: 111 of cap free
Amount of items: 3
Items: 
Size: 592897 Color: 1
Size: 246316 Color: 0
Size: 160677 Color: 1

Bin 221: 111 of cap free
Amount of items: 2
Items: 
Size: 677965 Color: 0
Size: 321925 Color: 1

Bin 222: 111 of cap free
Amount of items: 2
Items: 
Size: 710048 Color: 1
Size: 289842 Color: 0

Bin 223: 111 of cap free
Amount of items: 2
Items: 
Size: 764528 Color: 0
Size: 235362 Color: 1

Bin 224: 112 of cap free
Amount of items: 2
Items: 
Size: 611160 Color: 0
Size: 388729 Color: 1

Bin 225: 112 of cap free
Amount of items: 3
Items: 
Size: 702717 Color: 0
Size: 163422 Color: 1
Size: 133750 Color: 0

Bin 226: 112 of cap free
Amount of items: 3
Items: 
Size: 789047 Color: 1
Size: 109575 Color: 0
Size: 101267 Color: 1

Bin 227: 113 of cap free
Amount of items: 2
Items: 
Size: 527743 Color: 1
Size: 472145 Color: 0

Bin 228: 114 of cap free
Amount of items: 2
Items: 
Size: 602919 Color: 0
Size: 396968 Color: 1

Bin 229: 114 of cap free
Amount of items: 2
Items: 
Size: 656323 Color: 0
Size: 343564 Color: 1

Bin 230: 115 of cap free
Amount of items: 3
Items: 
Size: 541742 Color: 0
Size: 303039 Color: 1
Size: 155105 Color: 0

Bin 231: 115 of cap free
Amount of items: 2
Items: 
Size: 773026 Color: 1
Size: 226860 Color: 0

Bin 232: 116 of cap free
Amount of items: 2
Items: 
Size: 687030 Color: 1
Size: 312855 Color: 0

Bin 233: 118 of cap free
Amount of items: 2
Items: 
Size: 565041 Color: 0
Size: 434842 Color: 1

Bin 234: 119 of cap free
Amount of items: 3
Items: 
Size: 477138 Color: 1
Size: 351473 Color: 0
Size: 171271 Color: 0

Bin 235: 119 of cap free
Amount of items: 2
Items: 
Size: 560654 Color: 0
Size: 439228 Color: 1

Bin 236: 120 of cap free
Amount of items: 2
Items: 
Size: 794619 Color: 1
Size: 205262 Color: 0

Bin 237: 121 of cap free
Amount of items: 2
Items: 
Size: 779048 Color: 1
Size: 220832 Color: 0

Bin 238: 124 of cap free
Amount of items: 3
Items: 
Size: 661936 Color: 0
Size: 170609 Color: 1
Size: 167332 Color: 1

Bin 239: 124 of cap free
Amount of items: 2
Items: 
Size: 770076 Color: 1
Size: 229801 Color: 0

Bin 240: 124 of cap free
Amount of items: 2
Items: 
Size: 787736 Color: 1
Size: 212141 Color: 0

Bin 241: 127 of cap free
Amount of items: 2
Items: 
Size: 713388 Color: 1
Size: 286486 Color: 0

Bin 242: 127 of cap free
Amount of items: 2
Items: 
Size: 772239 Color: 1
Size: 227635 Color: 0

Bin 243: 128 of cap free
Amount of items: 2
Items: 
Size: 624753 Color: 1
Size: 375120 Color: 0

Bin 244: 128 of cap free
Amount of items: 2
Items: 
Size: 716729 Color: 0
Size: 283144 Color: 1

Bin 245: 128 of cap free
Amount of items: 2
Items: 
Size: 754106 Color: 1
Size: 245767 Color: 0

Bin 246: 129 of cap free
Amount of items: 2
Items: 
Size: 577090 Color: 1
Size: 422782 Color: 0

Bin 247: 129 of cap free
Amount of items: 3
Items: 
Size: 594058 Color: 0
Size: 275803 Color: 1
Size: 130011 Color: 0

Bin 248: 129 of cap free
Amount of items: 2
Items: 
Size: 726636 Color: 0
Size: 273236 Color: 1

Bin 249: 129 of cap free
Amount of items: 2
Items: 
Size: 743209 Color: 0
Size: 256663 Color: 1

Bin 250: 131 of cap free
Amount of items: 2
Items: 
Size: 687039 Color: 0
Size: 312831 Color: 1

Bin 251: 132 of cap free
Amount of items: 2
Items: 
Size: 568307 Color: 1
Size: 431562 Color: 0

Bin 252: 132 of cap free
Amount of items: 2
Items: 
Size: 635573 Color: 0
Size: 364296 Color: 1

Bin 253: 132 of cap free
Amount of items: 2
Items: 
Size: 772329 Color: 0
Size: 227540 Color: 1

Bin 254: 133 of cap free
Amount of items: 2
Items: 
Size: 518075 Color: 0
Size: 481793 Color: 1

Bin 255: 133 of cap free
Amount of items: 3
Items: 
Size: 573682 Color: 0
Size: 274186 Color: 1
Size: 152000 Color: 1

Bin 256: 133 of cap free
Amount of items: 2
Items: 
Size: 708150 Color: 0
Size: 291718 Color: 1

Bin 257: 135 of cap free
Amount of items: 2
Items: 
Size: 699575 Color: 1
Size: 300291 Color: 0

Bin 258: 136 of cap free
Amount of items: 2
Items: 
Size: 540372 Color: 0
Size: 459493 Color: 1

Bin 259: 136 of cap free
Amount of items: 3
Items: 
Size: 781552 Color: 1
Size: 114522 Color: 0
Size: 103791 Color: 1

Bin 260: 137 of cap free
Amount of items: 2
Items: 
Size: 587537 Color: 1
Size: 412327 Color: 0

Bin 261: 138 of cap free
Amount of items: 2
Items: 
Size: 503949 Color: 0
Size: 495914 Color: 1

Bin 262: 138 of cap free
Amount of items: 3
Items: 
Size: 531990 Color: 1
Size: 305019 Color: 1
Size: 162854 Color: 0

Bin 263: 138 of cap free
Amount of items: 2
Items: 
Size: 548141 Color: 0
Size: 451722 Color: 1

Bin 264: 141 of cap free
Amount of items: 3
Items: 
Size: 478948 Color: 1
Size: 375027 Color: 0
Size: 145885 Color: 1

Bin 265: 144 of cap free
Amount of items: 2
Items: 
Size: 696082 Color: 1
Size: 303775 Color: 0

Bin 266: 146 of cap free
Amount of items: 2
Items: 
Size: 696954 Color: 1
Size: 302901 Color: 0

Bin 267: 147 of cap free
Amount of items: 2
Items: 
Size: 797006 Color: 1
Size: 202848 Color: 0

Bin 268: 148 of cap free
Amount of items: 2
Items: 
Size: 677618 Color: 0
Size: 322235 Color: 1

Bin 269: 150 of cap free
Amount of items: 2
Items: 
Size: 531497 Color: 0
Size: 468354 Color: 1

Bin 270: 152 of cap free
Amount of items: 2
Items: 
Size: 554092 Color: 1
Size: 445757 Color: 0

Bin 271: 152 of cap free
Amount of items: 2
Items: 
Size: 756890 Color: 1
Size: 242959 Color: 0

Bin 272: 153 of cap free
Amount of items: 3
Items: 
Size: 529276 Color: 1
Size: 324678 Color: 0
Size: 145894 Color: 1

Bin 273: 153 of cap free
Amount of items: 2
Items: 
Size: 675673 Color: 0
Size: 324175 Color: 1

Bin 274: 154 of cap free
Amount of items: 2
Items: 
Size: 527150 Color: 1
Size: 472697 Color: 0

Bin 275: 154 of cap free
Amount of items: 2
Items: 
Size: 591555 Color: 1
Size: 408292 Color: 0

Bin 276: 157 of cap free
Amount of items: 3
Items: 
Size: 455994 Color: 1
Size: 360959 Color: 0
Size: 182891 Color: 0

Bin 277: 157 of cap free
Amount of items: 3
Items: 
Size: 489615 Color: 1
Size: 337390 Color: 0
Size: 172839 Color: 0

Bin 278: 159 of cap free
Amount of items: 2
Items: 
Size: 543690 Color: 0
Size: 456152 Color: 1

Bin 279: 159 of cap free
Amount of items: 3
Items: 
Size: 574575 Color: 1
Size: 243323 Color: 0
Size: 181944 Color: 1

Bin 280: 160 of cap free
Amount of items: 2
Items: 
Size: 566800 Color: 1
Size: 433041 Color: 0

Bin 281: 162 of cap free
Amount of items: 3
Items: 
Size: 567333 Color: 1
Size: 246625 Color: 0
Size: 185881 Color: 0

Bin 282: 162 of cap free
Amount of items: 2
Items: 
Size: 733048 Color: 0
Size: 266791 Color: 1

Bin 283: 164 of cap free
Amount of items: 2
Items: 
Size: 600729 Color: 1
Size: 399108 Color: 0

Bin 284: 166 of cap free
Amount of items: 2
Items: 
Size: 584860 Color: 1
Size: 414975 Color: 0

Bin 285: 167 of cap free
Amount of items: 2
Items: 
Size: 699316 Color: 0
Size: 300518 Color: 1

Bin 286: 169 of cap free
Amount of items: 3
Items: 
Size: 477455 Color: 1
Size: 363728 Color: 1
Size: 158649 Color: 0

Bin 287: 171 of cap free
Amount of items: 2
Items: 
Size: 507125 Color: 1
Size: 492705 Color: 0

Bin 288: 171 of cap free
Amount of items: 2
Items: 
Size: 597063 Color: 0
Size: 402767 Color: 1

Bin 289: 171 of cap free
Amount of items: 2
Items: 
Size: 696597 Color: 0
Size: 303233 Color: 1

Bin 290: 171 of cap free
Amount of items: 2
Items: 
Size: 711785 Color: 0
Size: 288045 Color: 1

Bin 291: 171 of cap free
Amount of items: 2
Items: 
Size: 722117 Color: 0
Size: 277713 Color: 1

Bin 292: 172 of cap free
Amount of items: 2
Items: 
Size: 551316 Color: 1
Size: 448513 Color: 0

Bin 293: 173 of cap free
Amount of items: 2
Items: 
Size: 604650 Color: 0
Size: 395178 Color: 1

Bin 294: 173 of cap free
Amount of items: 2
Items: 
Size: 669729 Color: 0
Size: 330099 Color: 1

Bin 295: 174 of cap free
Amount of items: 2
Items: 
Size: 677250 Color: 1
Size: 322577 Color: 0

Bin 296: 175 of cap free
Amount of items: 2
Items: 
Size: 693078 Color: 1
Size: 306748 Color: 0

Bin 297: 182 of cap free
Amount of items: 2
Items: 
Size: 705057 Color: 0
Size: 294762 Color: 1

Bin 298: 185 of cap free
Amount of items: 2
Items: 
Size: 637504 Color: 0
Size: 362312 Color: 1

Bin 299: 185 of cap free
Amount of items: 2
Items: 
Size: 667496 Color: 0
Size: 332320 Color: 1

Bin 300: 186 of cap free
Amount of items: 2
Items: 
Size: 783113 Color: 0
Size: 216702 Color: 1

Bin 301: 192 of cap free
Amount of items: 2
Items: 
Size: 712414 Color: 0
Size: 287395 Color: 1

Bin 302: 193 of cap free
Amount of items: 2
Items: 
Size: 554887 Color: 1
Size: 444921 Color: 0

Bin 303: 195 of cap free
Amount of items: 2
Items: 
Size: 585589 Color: 0
Size: 414217 Color: 1

Bin 304: 197 of cap free
Amount of items: 2
Items: 
Size: 570151 Color: 1
Size: 429653 Color: 0

Bin 305: 197 of cap free
Amount of items: 3
Items: 
Size: 593649 Color: 0
Size: 263045 Color: 0
Size: 143110 Color: 1

Bin 306: 200 of cap free
Amount of items: 2
Items: 
Size: 550247 Color: 1
Size: 449554 Color: 0

Bin 307: 201 of cap free
Amount of items: 2
Items: 
Size: 650346 Color: 0
Size: 349454 Color: 1

Bin 308: 202 of cap free
Amount of items: 2
Items: 
Size: 517107 Color: 0
Size: 482692 Color: 1

Bin 309: 202 of cap free
Amount of items: 2
Items: 
Size: 737373 Color: 0
Size: 262426 Color: 1

Bin 310: 202 of cap free
Amount of items: 2
Items: 
Size: 742569 Color: 0
Size: 257230 Color: 1

Bin 311: 204 of cap free
Amount of items: 2
Items: 
Size: 514545 Color: 0
Size: 485252 Color: 1

Bin 312: 205 of cap free
Amount of items: 2
Items: 
Size: 765619 Color: 0
Size: 234177 Color: 1

Bin 313: 206 of cap free
Amount of items: 2
Items: 
Size: 573456 Color: 0
Size: 426339 Color: 1

Bin 314: 211 of cap free
Amount of items: 3
Items: 
Size: 788601 Color: 1
Size: 108608 Color: 0
Size: 102581 Color: 0

Bin 315: 212 of cap free
Amount of items: 3
Items: 
Size: 565896 Color: 1
Size: 305812 Color: 0
Size: 128081 Color: 0

Bin 316: 212 of cap free
Amount of items: 2
Items: 
Size: 618231 Color: 1
Size: 381558 Color: 0

Bin 317: 212 of cap free
Amount of items: 2
Items: 
Size: 689424 Color: 0
Size: 310365 Color: 1

Bin 318: 212 of cap free
Amount of items: 2
Items: 
Size: 714668 Color: 1
Size: 285121 Color: 0

Bin 319: 213 of cap free
Amount of items: 2
Items: 
Size: 570142 Color: 1
Size: 429646 Color: 0

Bin 320: 214 of cap free
Amount of items: 2
Items: 
Size: 690668 Color: 0
Size: 309119 Color: 1

Bin 321: 215 of cap free
Amount of items: 2
Items: 
Size: 785591 Color: 1
Size: 214195 Color: 0

Bin 322: 216 of cap free
Amount of items: 3
Items: 
Size: 515400 Color: 0
Size: 330372 Color: 1
Size: 154013 Color: 1

Bin 323: 216 of cap free
Amount of items: 2
Items: 
Size: 606999 Color: 0
Size: 392786 Color: 1

Bin 324: 216 of cap free
Amount of items: 2
Items: 
Size: 664956 Color: 1
Size: 334829 Color: 0

Bin 325: 217 of cap free
Amount of items: 2
Items: 
Size: 522292 Color: 1
Size: 477492 Color: 0

Bin 326: 217 of cap free
Amount of items: 2
Items: 
Size: 553099 Color: 0
Size: 446685 Color: 1

Bin 327: 217 of cap free
Amount of items: 2
Items: 
Size: 575424 Color: 0
Size: 424360 Color: 1

Bin 328: 218 of cap free
Amount of items: 3
Items: 
Size: 789572 Color: 1
Size: 108890 Color: 0
Size: 101321 Color: 1

Bin 329: 219 of cap free
Amount of items: 2
Items: 
Size: 641844 Color: 1
Size: 357938 Color: 0

Bin 330: 222 of cap free
Amount of items: 3
Items: 
Size: 567549 Color: 1
Size: 246785 Color: 0
Size: 185445 Color: 1

Bin 331: 222 of cap free
Amount of items: 3
Items: 
Size: 719924 Color: 0
Size: 162215 Color: 1
Size: 117640 Color: 1

Bin 332: 224 of cap free
Amount of items: 2
Items: 
Size: 673236 Color: 0
Size: 326541 Color: 1

Bin 333: 224 of cap free
Amount of items: 2
Items: 
Size: 730320 Color: 1
Size: 269457 Color: 0

Bin 334: 224 of cap free
Amount of items: 2
Items: 
Size: 746195 Color: 1
Size: 253582 Color: 0

Bin 335: 224 of cap free
Amount of items: 2
Items: 
Size: 788034 Color: 1
Size: 211743 Color: 0

Bin 336: 225 of cap free
Amount of items: 2
Items: 
Size: 535260 Color: 0
Size: 464516 Color: 1

Bin 337: 226 of cap free
Amount of items: 2
Items: 
Size: 651513 Color: 1
Size: 348262 Color: 0

Bin 338: 229 of cap free
Amount of items: 3
Items: 
Size: 776802 Color: 0
Size: 114772 Color: 1
Size: 108198 Color: 1

Bin 339: 231 of cap free
Amount of items: 2
Items: 
Size: 691969 Color: 1
Size: 307801 Color: 0

Bin 340: 233 of cap free
Amount of items: 2
Items: 
Size: 557225 Color: 1
Size: 442543 Color: 0

Bin 341: 233 of cap free
Amount of items: 2
Items: 
Size: 694395 Color: 1
Size: 305373 Color: 0

Bin 342: 235 of cap free
Amount of items: 2
Items: 
Size: 786506 Color: 1
Size: 213260 Color: 0

Bin 343: 236 of cap free
Amount of items: 2
Items: 
Size: 747488 Color: 0
Size: 252277 Color: 1

Bin 344: 236 of cap free
Amount of items: 3
Items: 
Size: 760986 Color: 1
Size: 120530 Color: 1
Size: 118249 Color: 0

Bin 345: 237 of cap free
Amount of items: 2
Items: 
Size: 506707 Color: 1
Size: 493057 Color: 0

Bin 346: 237 of cap free
Amount of items: 2
Items: 
Size: 708343 Color: 1
Size: 291421 Color: 0

Bin 347: 238 of cap free
Amount of items: 3
Items: 
Size: 571923 Color: 0
Size: 294753 Color: 1
Size: 133087 Color: 1

Bin 348: 241 of cap free
Amount of items: 2
Items: 
Size: 756078 Color: 1
Size: 243682 Color: 0

Bin 349: 242 of cap free
Amount of items: 2
Items: 
Size: 610404 Color: 1
Size: 389355 Color: 0

Bin 350: 242 of cap free
Amount of items: 2
Items: 
Size: 744196 Color: 0
Size: 255563 Color: 1

Bin 351: 245 of cap free
Amount of items: 2
Items: 
Size: 680939 Color: 0
Size: 318817 Color: 1

Bin 352: 247 of cap free
Amount of items: 2
Items: 
Size: 538508 Color: 0
Size: 461246 Color: 1

Bin 353: 248 of cap free
Amount of items: 2
Items: 
Size: 734772 Color: 1
Size: 264981 Color: 0

Bin 354: 249 of cap free
Amount of items: 2
Items: 
Size: 507117 Color: 1
Size: 492635 Color: 0

Bin 355: 249 of cap free
Amount of items: 2
Items: 
Size: 618395 Color: 0
Size: 381357 Color: 1

Bin 356: 250 of cap free
Amount of items: 2
Items: 
Size: 676385 Color: 1
Size: 323366 Color: 0

Bin 357: 252 of cap free
Amount of items: 2
Items: 
Size: 692132 Color: 0
Size: 307617 Color: 1

Bin 358: 252 of cap free
Amount of items: 2
Items: 
Size: 700943 Color: 1
Size: 298806 Color: 0

Bin 359: 253 of cap free
Amount of items: 2
Items: 
Size: 747952 Color: 1
Size: 251796 Color: 0

Bin 360: 254 of cap free
Amount of items: 2
Items: 
Size: 754182 Color: 0
Size: 245565 Color: 1

Bin 361: 254 of cap free
Amount of items: 2
Items: 
Size: 799770 Color: 0
Size: 199977 Color: 1

Bin 362: 255 of cap free
Amount of items: 2
Items: 
Size: 722229 Color: 1
Size: 277517 Color: 0

Bin 363: 256 of cap free
Amount of items: 2
Items: 
Size: 619847 Color: 0
Size: 379898 Color: 1

Bin 364: 264 of cap free
Amount of items: 2
Items: 
Size: 784265 Color: 1
Size: 215472 Color: 0

Bin 365: 267 of cap free
Amount of items: 3
Items: 
Size: 510112 Color: 1
Size: 324333 Color: 0
Size: 165289 Color: 1

Bin 366: 267 of cap free
Amount of items: 2
Items: 
Size: 525594 Color: 1
Size: 474140 Color: 0

Bin 367: 267 of cap free
Amount of items: 2
Items: 
Size: 795867 Color: 0
Size: 203867 Color: 1

Bin 368: 268 of cap free
Amount of items: 2
Items: 
Size: 587861 Color: 1
Size: 411872 Color: 0

Bin 369: 268 of cap free
Amount of items: 2
Items: 
Size: 632390 Color: 0
Size: 367343 Color: 1

Bin 370: 268 of cap free
Amount of items: 3
Items: 
Size: 776489 Color: 0
Size: 111997 Color: 1
Size: 111247 Color: 0

Bin 371: 272 of cap free
Amount of items: 2
Items: 
Size: 771224 Color: 1
Size: 228505 Color: 0

Bin 372: 273 of cap free
Amount of items: 2
Items: 
Size: 519862 Color: 0
Size: 479866 Color: 1

Bin 373: 275 of cap free
Amount of items: 3
Items: 
Size: 510720 Color: 1
Size: 326307 Color: 1
Size: 162699 Color: 0

Bin 374: 280 of cap free
Amount of items: 2
Items: 
Size: 598794 Color: 0
Size: 400927 Color: 1

Bin 375: 282 of cap free
Amount of items: 2
Items: 
Size: 584279 Color: 0
Size: 415440 Color: 1

Bin 376: 282 of cap free
Amount of items: 2
Items: 
Size: 603183 Color: 0
Size: 396536 Color: 1

Bin 377: 284 of cap free
Amount of items: 2
Items: 
Size: 548904 Color: 0
Size: 450813 Color: 1

Bin 378: 284 of cap free
Amount of items: 3
Items: 
Size: 583243 Color: 0
Size: 275172 Color: 1
Size: 141302 Color: 0

Bin 379: 285 of cap free
Amount of items: 2
Items: 
Size: 568277 Color: 0
Size: 431439 Color: 1

Bin 380: 287 of cap free
Amount of items: 2
Items: 
Size: 577773 Color: 0
Size: 421941 Color: 1

Bin 381: 288 of cap free
Amount of items: 2
Items: 
Size: 525873 Color: 1
Size: 473840 Color: 0

Bin 382: 288 of cap free
Amount of items: 2
Items: 
Size: 542645 Color: 0
Size: 457068 Color: 1

Bin 383: 288 of cap free
Amount of items: 2
Items: 
Size: 704959 Color: 0
Size: 294754 Color: 1

Bin 384: 290 of cap free
Amount of items: 3
Items: 
Size: 567173 Color: 1
Size: 291909 Color: 1
Size: 140629 Color: 0

Bin 385: 290 of cap free
Amount of items: 2
Items: 
Size: 635960 Color: 1
Size: 363751 Color: 0

Bin 386: 291 of cap free
Amount of items: 3
Items: 
Size: 450280 Color: 1
Size: 377799 Color: 0
Size: 171631 Color: 0

Bin 387: 291 of cap free
Amount of items: 2
Items: 
Size: 567782 Color: 1
Size: 431928 Color: 0

Bin 388: 292 of cap free
Amount of items: 2
Items: 
Size: 667187 Color: 0
Size: 332522 Color: 1

Bin 389: 292 of cap free
Amount of items: 2
Items: 
Size: 790103 Color: 0
Size: 209606 Color: 1

Bin 390: 293 of cap free
Amount of items: 2
Items: 
Size: 581006 Color: 1
Size: 418702 Color: 0

Bin 391: 294 of cap free
Amount of items: 2
Items: 
Size: 554110 Color: 0
Size: 445597 Color: 1

Bin 392: 295 of cap free
Amount of items: 3
Items: 
Size: 502791 Color: 1
Size: 327762 Color: 0
Size: 169153 Color: 0

Bin 393: 300 of cap free
Amount of items: 2
Items: 
Size: 501411 Color: 0
Size: 498290 Color: 1

Bin 394: 302 of cap free
Amount of items: 2
Items: 
Size: 531854 Color: 1
Size: 467845 Color: 0

Bin 395: 302 of cap free
Amount of items: 2
Items: 
Size: 678759 Color: 0
Size: 320940 Color: 1

Bin 396: 303 of cap free
Amount of items: 2
Items: 
Size: 533325 Color: 1
Size: 466373 Color: 0

Bin 397: 304 of cap free
Amount of items: 2
Items: 
Size: 736615 Color: 1
Size: 263082 Color: 0

Bin 398: 308 of cap free
Amount of items: 2
Items: 
Size: 715039 Color: 0
Size: 284654 Color: 1

Bin 399: 316 of cap free
Amount of items: 2
Items: 
Size: 644444 Color: 1
Size: 355241 Color: 0

Bin 400: 316 of cap free
Amount of items: 2
Items: 
Size: 648184 Color: 0
Size: 351501 Color: 1

Bin 401: 317 of cap free
Amount of items: 2
Items: 
Size: 795314 Color: 0
Size: 204370 Color: 1

Bin 402: 319 of cap free
Amount of items: 2
Items: 
Size: 768492 Color: 0
Size: 231190 Color: 1

Bin 403: 319 of cap free
Amount of items: 2
Items: 
Size: 794624 Color: 0
Size: 205058 Color: 1

Bin 404: 320 of cap free
Amount of items: 2
Items: 
Size: 602993 Color: 1
Size: 396688 Color: 0

Bin 405: 326 of cap free
Amount of items: 2
Items: 
Size: 594716 Color: 0
Size: 404959 Color: 1

Bin 406: 327 of cap free
Amount of items: 2
Items: 
Size: 523085 Color: 0
Size: 476589 Color: 1

Bin 407: 329 of cap free
Amount of items: 2
Items: 
Size: 725684 Color: 1
Size: 273988 Color: 0

Bin 408: 330 of cap free
Amount of items: 2
Items: 
Size: 680912 Color: 1
Size: 318759 Color: 0

Bin 409: 330 of cap free
Amount of items: 2
Items: 
Size: 762887 Color: 1
Size: 236784 Color: 0

Bin 410: 333 of cap free
Amount of items: 2
Items: 
Size: 517761 Color: 1
Size: 481907 Color: 0

Bin 411: 335 of cap free
Amount of items: 2
Items: 
Size: 552217 Color: 0
Size: 447449 Color: 1

Bin 412: 335 of cap free
Amount of items: 2
Items: 
Size: 695575 Color: 0
Size: 304091 Color: 1

Bin 413: 336 of cap free
Amount of items: 3
Items: 
Size: 570695 Color: 0
Size: 243294 Color: 0
Size: 185676 Color: 1

Bin 414: 336 of cap free
Amount of items: 2
Items: 
Size: 596390 Color: 0
Size: 403275 Color: 1

Bin 415: 338 of cap free
Amount of items: 3
Items: 
Size: 450389 Color: 1
Size: 363985 Color: 1
Size: 185289 Color: 0

Bin 416: 338 of cap free
Amount of items: 2
Items: 
Size: 678229 Color: 1
Size: 321434 Color: 0

Bin 417: 339 of cap free
Amount of items: 2
Items: 
Size: 569034 Color: 1
Size: 430628 Color: 0

Bin 418: 339 of cap free
Amount of items: 2
Items: 
Size: 646699 Color: 1
Size: 352963 Color: 0

Bin 419: 340 of cap free
Amount of items: 2
Items: 
Size: 759597 Color: 0
Size: 240064 Color: 1

Bin 420: 343 of cap free
Amount of items: 2
Items: 
Size: 674780 Color: 0
Size: 324878 Color: 1

Bin 421: 345 of cap free
Amount of items: 3
Items: 
Size: 478942 Color: 1
Size: 359044 Color: 0
Size: 161670 Color: 0

Bin 422: 345 of cap free
Amount of items: 2
Items: 
Size: 767811 Color: 1
Size: 231845 Color: 0

Bin 423: 347 of cap free
Amount of items: 3
Items: 
Size: 456762 Color: 1
Size: 377936 Color: 0
Size: 164956 Color: 1

Bin 424: 347 of cap free
Amount of items: 2
Items: 
Size: 720300 Color: 0
Size: 279354 Color: 1

Bin 425: 350 of cap free
Amount of items: 3
Items: 
Size: 552922 Color: 1
Size: 291580 Color: 1
Size: 155149 Color: 0

Bin 426: 350 of cap free
Amount of items: 2
Items: 
Size: 587628 Color: 0
Size: 412023 Color: 1

Bin 427: 353 of cap free
Amount of items: 2
Items: 
Size: 568152 Color: 1
Size: 431496 Color: 0

Bin 428: 353 of cap free
Amount of items: 2
Items: 
Size: 714312 Color: 1
Size: 285336 Color: 0

Bin 429: 355 of cap free
Amount of items: 2
Items: 
Size: 640959 Color: 1
Size: 358687 Color: 0

Bin 430: 356 of cap free
Amount of items: 3
Items: 
Size: 495168 Color: 1
Size: 359105 Color: 0
Size: 145372 Color: 1

Bin 431: 357 of cap free
Amount of items: 2
Items: 
Size: 537769 Color: 0
Size: 461875 Color: 1

Bin 432: 360 of cap free
Amount of items: 3
Items: 
Size: 766592 Color: 1
Size: 126112 Color: 0
Size: 106937 Color: 0

Bin 433: 362 of cap free
Amount of items: 2
Items: 
Size: 658552 Color: 1
Size: 341087 Color: 0

Bin 434: 367 of cap free
Amount of items: 2
Items: 
Size: 540353 Color: 1
Size: 459281 Color: 0

Bin 435: 367 of cap free
Amount of items: 2
Items: 
Size: 578900 Color: 1
Size: 420734 Color: 0

Bin 436: 367 of cap free
Amount of items: 2
Items: 
Size: 606510 Color: 0
Size: 393124 Color: 1

Bin 437: 374 of cap free
Amount of items: 2
Items: 
Size: 599107 Color: 0
Size: 400520 Color: 1

Bin 438: 381 of cap free
Amount of items: 2
Items: 
Size: 767459 Color: 0
Size: 232161 Color: 1

Bin 439: 382 of cap free
Amount of items: 2
Items: 
Size: 601167 Color: 1
Size: 398452 Color: 0

Bin 440: 387 of cap free
Amount of items: 2
Items: 
Size: 731617 Color: 1
Size: 267997 Color: 0

Bin 441: 389 of cap free
Amount of items: 2
Items: 
Size: 523764 Color: 0
Size: 475848 Color: 1

Bin 442: 391 of cap free
Amount of items: 2
Items: 
Size: 518884 Color: 0
Size: 480726 Color: 1

Bin 443: 391 of cap free
Amount of items: 2
Items: 
Size: 571131 Color: 1
Size: 428479 Color: 0

Bin 444: 392 of cap free
Amount of items: 2
Items: 
Size: 623694 Color: 1
Size: 375915 Color: 0

Bin 445: 393 of cap free
Amount of items: 2
Items: 
Size: 638918 Color: 0
Size: 360690 Color: 1

Bin 446: 394 of cap free
Amount of items: 2
Items: 
Size: 544667 Color: 1
Size: 454940 Color: 0

Bin 447: 396 of cap free
Amount of items: 2
Items: 
Size: 645343 Color: 0
Size: 354262 Color: 1

Bin 448: 398 of cap free
Amount of items: 2
Items: 
Size: 592329 Color: 1
Size: 407274 Color: 0

Bin 449: 401 of cap free
Amount of items: 2
Items: 
Size: 733689 Color: 1
Size: 265911 Color: 0

Bin 450: 407 of cap free
Amount of items: 2
Items: 
Size: 505521 Color: 0
Size: 494073 Color: 1

Bin 451: 414 of cap free
Amount of items: 2
Items: 
Size: 623225 Color: 1
Size: 376362 Color: 0

Bin 452: 415 of cap free
Amount of items: 2
Items: 
Size: 615708 Color: 1
Size: 383878 Color: 0

Bin 453: 415 of cap free
Amount of items: 2
Items: 
Size: 758420 Color: 0
Size: 241166 Color: 1

Bin 454: 418 of cap free
Amount of items: 2
Items: 
Size: 563456 Color: 1
Size: 436127 Color: 0

Bin 455: 418 of cap free
Amount of items: 2
Items: 
Size: 668444 Color: 1
Size: 331139 Color: 0

Bin 456: 418 of cap free
Amount of items: 2
Items: 
Size: 729586 Color: 0
Size: 269997 Color: 1

Bin 457: 424 of cap free
Amount of items: 2
Items: 
Size: 631498 Color: 0
Size: 368079 Color: 1

Bin 458: 425 of cap free
Amount of items: 2
Items: 
Size: 510786 Color: 0
Size: 488790 Color: 1

Bin 459: 425 of cap free
Amount of items: 2
Items: 
Size: 582941 Color: 0
Size: 416635 Color: 1

Bin 460: 425 of cap free
Amount of items: 2
Items: 
Size: 664283 Color: 1
Size: 335293 Color: 0

Bin 461: 426 of cap free
Amount of items: 2
Items: 
Size: 512665 Color: 1
Size: 486910 Color: 0

Bin 462: 428 of cap free
Amount of items: 3
Items: 
Size: 782528 Color: 1
Size: 112378 Color: 0
Size: 104667 Color: 0

Bin 463: 430 of cap free
Amount of items: 2
Items: 
Size: 608202 Color: 1
Size: 391369 Color: 0

Bin 464: 432 of cap free
Amount of items: 2
Items: 
Size: 617240 Color: 1
Size: 382329 Color: 0

Bin 465: 434 of cap free
Amount of items: 2
Items: 
Size: 693311 Color: 0
Size: 306256 Color: 1

Bin 466: 436 of cap free
Amount of items: 2
Items: 
Size: 542644 Color: 0
Size: 456921 Color: 1

Bin 467: 437 of cap free
Amount of items: 2
Items: 
Size: 788800 Color: 1
Size: 210764 Color: 0

Bin 468: 439 of cap free
Amount of items: 2
Items: 
Size: 721109 Color: 0
Size: 278453 Color: 1

Bin 469: 443 of cap free
Amount of items: 2
Items: 
Size: 641847 Color: 0
Size: 357711 Color: 1

Bin 470: 445 of cap free
Amount of items: 2
Items: 
Size: 544075 Color: 0
Size: 455481 Color: 1

Bin 471: 446 of cap free
Amount of items: 2
Items: 
Size: 752958 Color: 0
Size: 246597 Color: 1

Bin 472: 447 of cap free
Amount of items: 2
Items: 
Size: 780832 Color: 1
Size: 218722 Color: 0

Bin 473: 461 of cap free
Amount of items: 2
Items: 
Size: 504871 Color: 1
Size: 494669 Color: 0

Bin 474: 461 of cap free
Amount of items: 2
Items: 
Size: 731360 Color: 0
Size: 268180 Color: 1

Bin 475: 469 of cap free
Amount of items: 2
Items: 
Size: 619604 Color: 1
Size: 379928 Color: 0

Bin 476: 469 of cap free
Amount of items: 2
Items: 
Size: 772943 Color: 1
Size: 226589 Color: 0

Bin 477: 472 of cap free
Amount of items: 2
Items: 
Size: 602383 Color: 1
Size: 397146 Color: 0

Bin 478: 472 of cap free
Amount of items: 2
Items: 
Size: 687943 Color: 1
Size: 311586 Color: 0

Bin 479: 472 of cap free
Amount of items: 2
Items: 
Size: 723189 Color: 1
Size: 276340 Color: 0

Bin 480: 472 of cap free
Amount of items: 3
Items: 
Size: 792053 Color: 0
Size: 105933 Color: 0
Size: 101543 Color: 1

Bin 481: 474 of cap free
Amount of items: 2
Items: 
Size: 550373 Color: 0
Size: 449154 Color: 1

Bin 482: 481 of cap free
Amount of items: 2
Items: 
Size: 656020 Color: 0
Size: 343500 Color: 1

Bin 483: 484 of cap free
Amount of items: 2
Items: 
Size: 558325 Color: 0
Size: 441192 Color: 1

Bin 484: 489 of cap free
Amount of items: 2
Items: 
Size: 661308 Color: 0
Size: 338204 Color: 1

Bin 485: 491 of cap free
Amount of items: 2
Items: 
Size: 557586 Color: 1
Size: 441924 Color: 0

Bin 486: 494 of cap free
Amount of items: 2
Items: 
Size: 668043 Color: 0
Size: 331464 Color: 1

Bin 487: 495 of cap free
Amount of items: 2
Items: 
Size: 770038 Color: 1
Size: 229468 Color: 0

Bin 488: 499 of cap free
Amount of items: 2
Items: 
Size: 621348 Color: 1
Size: 378154 Color: 0

Bin 489: 499 of cap free
Amount of items: 2
Items: 
Size: 711686 Color: 1
Size: 287816 Color: 0

Bin 490: 505 of cap free
Amount of items: 2
Items: 
Size: 686834 Color: 1
Size: 312662 Color: 0

Bin 491: 507 of cap free
Amount of items: 2
Items: 
Size: 591601 Color: 0
Size: 407893 Color: 1

Bin 492: 507 of cap free
Amount of items: 2
Items: 
Size: 601616 Color: 1
Size: 397878 Color: 0

Bin 493: 510 of cap free
Amount of items: 3
Items: 
Size: 438845 Color: 1
Size: 374916 Color: 0
Size: 185730 Color: 0

Bin 494: 515 of cap free
Amount of items: 2
Items: 
Size: 786938 Color: 0
Size: 212548 Color: 1

Bin 495: 519 of cap free
Amount of items: 2
Items: 
Size: 700761 Color: 1
Size: 298721 Color: 0

Bin 496: 530 of cap free
Amount of items: 2
Items: 
Size: 539629 Color: 0
Size: 459842 Color: 1

Bin 497: 548 of cap free
Amount of items: 2
Items: 
Size: 537458 Color: 1
Size: 461995 Color: 0

Bin 498: 548 of cap free
Amount of items: 2
Items: 
Size: 542557 Color: 0
Size: 456896 Color: 1

Bin 499: 549 of cap free
Amount of items: 2
Items: 
Size: 554674 Color: 0
Size: 444778 Color: 1

Bin 500: 551 of cap free
Amount of items: 3
Items: 
Size: 490433 Color: 1
Size: 344337 Color: 1
Size: 164680 Color: 0

Bin 501: 555 of cap free
Amount of items: 2
Items: 
Size: 786830 Color: 1
Size: 212616 Color: 0

Bin 502: 565 of cap free
Amount of items: 2
Items: 
Size: 562740 Color: 1
Size: 436696 Color: 0

Bin 503: 573 of cap free
Amount of items: 2
Items: 
Size: 681942 Color: 0
Size: 317486 Color: 1

Bin 504: 575 of cap free
Amount of items: 2
Items: 
Size: 507919 Color: 0
Size: 491507 Color: 1

Bin 505: 575 of cap free
Amount of items: 2
Items: 
Size: 559086 Color: 1
Size: 440340 Color: 0

Bin 506: 582 of cap free
Amount of items: 2
Items: 
Size: 565984 Color: 1
Size: 433435 Color: 0

Bin 507: 583 of cap free
Amount of items: 2
Items: 
Size: 544583 Color: 0
Size: 454835 Color: 1

Bin 508: 583 of cap free
Amount of items: 2
Items: 
Size: 761773 Color: 0
Size: 237645 Color: 1

Bin 509: 587 of cap free
Amount of items: 2
Items: 
Size: 682627 Color: 1
Size: 316787 Color: 0

Bin 510: 590 of cap free
Amount of items: 2
Items: 
Size: 706708 Color: 0
Size: 292703 Color: 1

Bin 511: 591 of cap free
Amount of items: 3
Items: 
Size: 493115 Color: 1
Size: 342464 Color: 0
Size: 163831 Color: 0

Bin 512: 591 of cap free
Amount of items: 2
Items: 
Size: 505393 Color: 0
Size: 494017 Color: 1

Bin 513: 593 of cap free
Amount of items: 2
Items: 
Size: 636734 Color: 0
Size: 362674 Color: 1

Bin 514: 593 of cap free
Amount of items: 2
Items: 
Size: 645278 Color: 0
Size: 354130 Color: 1

Bin 515: 594 of cap free
Amount of items: 2
Items: 
Size: 679694 Color: 0
Size: 319713 Color: 1

Bin 516: 596 of cap free
Amount of items: 2
Items: 
Size: 733392 Color: 0
Size: 266013 Color: 1

Bin 517: 599 of cap free
Amount of items: 2
Items: 
Size: 775835 Color: 0
Size: 223567 Color: 1

Bin 518: 608 of cap free
Amount of items: 2
Items: 
Size: 505776 Color: 1
Size: 493617 Color: 0

Bin 519: 608 of cap free
Amount of items: 3
Items: 
Size: 789445 Color: 1
Size: 107088 Color: 0
Size: 102860 Color: 0

Bin 520: 611 of cap free
Amount of items: 2
Items: 
Size: 508743 Color: 0
Size: 490647 Color: 1

Bin 521: 614 of cap free
Amount of items: 2
Items: 
Size: 762167 Color: 1
Size: 237220 Color: 0

Bin 522: 615 of cap free
Amount of items: 3
Items: 
Size: 580879 Color: 1
Size: 240887 Color: 1
Size: 177620 Color: 0

Bin 523: 617 of cap free
Amount of items: 2
Items: 
Size: 548600 Color: 1
Size: 450784 Color: 0

Bin 524: 618 of cap free
Amount of items: 2
Items: 
Size: 763616 Color: 0
Size: 235767 Color: 1

Bin 525: 619 of cap free
Amount of items: 2
Items: 
Size: 559982 Color: 0
Size: 439400 Color: 1

Bin 526: 620 of cap free
Amount of items: 2
Items: 
Size: 503230 Color: 0
Size: 496151 Color: 1

Bin 527: 621 of cap free
Amount of items: 2
Items: 
Size: 654026 Color: 1
Size: 345354 Color: 0

Bin 528: 629 of cap free
Amount of items: 2
Items: 
Size: 707433 Color: 1
Size: 291939 Color: 0

Bin 529: 630 of cap free
Amount of items: 2
Items: 
Size: 521098 Color: 0
Size: 478273 Color: 1

Bin 530: 631 of cap free
Amount of items: 2
Items: 
Size: 664735 Color: 0
Size: 334635 Color: 1

Bin 531: 632 of cap free
Amount of items: 2
Items: 
Size: 613011 Color: 1
Size: 386358 Color: 0

Bin 532: 635 of cap free
Amount of items: 2
Items: 
Size: 550063 Color: 1
Size: 449303 Color: 0

Bin 533: 635 of cap free
Amount of items: 2
Items: 
Size: 713056 Color: 0
Size: 286310 Color: 1

Bin 534: 635 of cap free
Amount of items: 2
Items: 
Size: 765082 Color: 1
Size: 234284 Color: 0

Bin 535: 646 of cap free
Amount of items: 3
Items: 
Size: 776602 Color: 0
Size: 114666 Color: 1
Size: 108087 Color: 1

Bin 536: 647 of cap free
Amount of items: 3
Items: 
Size: 785486 Color: 1
Size: 113634 Color: 0
Size: 100234 Color: 0

Bin 537: 648 of cap free
Amount of items: 2
Items: 
Size: 634802 Color: 1
Size: 364551 Color: 0

Bin 538: 649 of cap free
Amount of items: 2
Items: 
Size: 628171 Color: 1
Size: 371181 Color: 0

Bin 539: 649 of cap free
Amount of items: 2
Items: 
Size: 755816 Color: 0
Size: 243536 Color: 1

Bin 540: 654 of cap free
Amount of items: 2
Items: 
Size: 646767 Color: 0
Size: 352580 Color: 1

Bin 541: 655 of cap free
Amount of items: 2
Items: 
Size: 764393 Color: 1
Size: 234953 Color: 0

Bin 542: 657 of cap free
Amount of items: 2
Items: 
Size: 509295 Color: 1
Size: 490049 Color: 0

Bin 543: 661 of cap free
Amount of items: 2
Items: 
Size: 570019 Color: 0
Size: 429321 Color: 1

Bin 544: 674 of cap free
Amount of items: 2
Items: 
Size: 536821 Color: 0
Size: 462506 Color: 1

Bin 545: 674 of cap free
Amount of items: 2
Items: 
Size: 666344 Color: 0
Size: 332983 Color: 1

Bin 546: 684 of cap free
Amount of items: 2
Items: 
Size: 590527 Color: 1
Size: 408790 Color: 0

Bin 547: 689 of cap free
Amount of items: 2
Items: 
Size: 774010 Color: 1
Size: 225302 Color: 0

Bin 548: 690 of cap free
Amount of items: 2
Items: 
Size: 755192 Color: 1
Size: 244119 Color: 0

Bin 549: 694 of cap free
Amount of items: 2
Items: 
Size: 555994 Color: 1
Size: 443313 Color: 0

Bin 550: 694 of cap free
Amount of items: 2
Items: 
Size: 700501 Color: 0
Size: 298806 Color: 1

Bin 551: 697 of cap free
Amount of items: 2
Items: 
Size: 726924 Color: 1
Size: 272380 Color: 0

Bin 552: 702 of cap free
Amount of items: 2
Items: 
Size: 607766 Color: 0
Size: 391533 Color: 1

Bin 553: 708 of cap free
Amount of items: 2
Items: 
Size: 569988 Color: 0
Size: 429305 Color: 1

Bin 554: 708 of cap free
Amount of items: 2
Items: 
Size: 648103 Color: 0
Size: 351190 Color: 1

Bin 555: 714 of cap free
Amount of items: 2
Items: 
Size: 734909 Color: 0
Size: 264378 Color: 1

Bin 556: 722 of cap free
Amount of items: 2
Items: 
Size: 741724 Color: 0
Size: 257555 Color: 1

Bin 557: 723 of cap free
Amount of items: 2
Items: 
Size: 752082 Color: 1
Size: 247196 Color: 0

Bin 558: 730 of cap free
Amount of items: 2
Items: 
Size: 791366 Color: 1
Size: 207905 Color: 0

Bin 559: 735 of cap free
Amount of items: 2
Items: 
Size: 521007 Color: 1
Size: 478259 Color: 0

Bin 560: 742 of cap free
Amount of items: 2
Items: 
Size: 698780 Color: 0
Size: 300479 Color: 1

Bin 561: 742 of cap free
Amount of items: 2
Items: 
Size: 782715 Color: 0
Size: 216544 Color: 1

Bin 562: 756 of cap free
Amount of items: 2
Items: 
Size: 499720 Color: 1
Size: 499525 Color: 0

Bin 563: 761 of cap free
Amount of items: 2
Items: 
Size: 703576 Color: 1
Size: 295664 Color: 0

Bin 564: 766 of cap free
Amount of items: 2
Items: 
Size: 614290 Color: 1
Size: 384945 Color: 0

Bin 565: 768 of cap free
Amount of items: 2
Items: 
Size: 734369 Color: 1
Size: 264864 Color: 0

Bin 566: 770 of cap free
Amount of items: 2
Items: 
Size: 740551 Color: 1
Size: 258680 Color: 0

Bin 567: 776 of cap free
Amount of items: 2
Items: 
Size: 559677 Color: 1
Size: 439548 Color: 0

Bin 568: 786 of cap free
Amount of items: 2
Items: 
Size: 565180 Color: 0
Size: 434035 Color: 1

Bin 569: 789 of cap free
Amount of items: 3
Items: 
Size: 562112 Color: 0
Size: 319637 Color: 1
Size: 117463 Color: 0

Bin 570: 789 of cap free
Amount of items: 2
Items: 
Size: 637967 Color: 1
Size: 361245 Color: 0

Bin 571: 795 of cap free
Amount of items: 2
Items: 
Size: 604254 Color: 0
Size: 394952 Color: 1

Bin 572: 798 of cap free
Amount of items: 2
Items: 
Size: 611088 Color: 1
Size: 388115 Color: 0

Bin 573: 804 of cap free
Amount of items: 2
Items: 
Size: 716250 Color: 0
Size: 282947 Color: 1

Bin 574: 805 of cap free
Amount of items: 2
Items: 
Size: 609717 Color: 0
Size: 389479 Color: 1

Bin 575: 809 of cap free
Amount of items: 2
Items: 
Size: 517353 Color: 1
Size: 481839 Color: 0

Bin 576: 832 of cap free
Amount of items: 2
Items: 
Size: 541220 Color: 1
Size: 457949 Color: 0

Bin 577: 837 of cap free
Amount of items: 2
Items: 
Size: 536792 Color: 0
Size: 462372 Color: 1

Bin 578: 837 of cap free
Amount of items: 2
Items: 
Size: 596801 Color: 0
Size: 402363 Color: 1

Bin 579: 840 of cap free
Amount of items: 3
Items: 
Size: 541275 Color: 0
Size: 319621 Color: 0
Size: 138265 Color: 1

Bin 580: 852 of cap free
Amount of items: 2
Items: 
Size: 762014 Color: 1
Size: 237135 Color: 0

Bin 581: 870 of cap free
Amount of items: 2
Items: 
Size: 658174 Color: 1
Size: 340957 Color: 0

Bin 582: 873 of cap free
Amount of items: 3
Items: 
Size: 547575 Color: 1
Size: 295077 Color: 1
Size: 156476 Color: 0

Bin 583: 874 of cap free
Amount of items: 2
Items: 
Size: 683540 Color: 1
Size: 315587 Color: 0

Bin 584: 878 of cap free
Amount of items: 2
Items: 
Size: 783185 Color: 1
Size: 215938 Color: 0

Bin 585: 879 of cap free
Amount of items: 2
Items: 
Size: 623208 Color: 0
Size: 375914 Color: 1

Bin 586: 893 of cap free
Amount of items: 2
Items: 
Size: 535691 Color: 1
Size: 463417 Color: 0

Bin 587: 893 of cap free
Amount of items: 2
Items: 
Size: 698750 Color: 0
Size: 300358 Color: 1

Bin 588: 914 of cap free
Amount of items: 2
Items: 
Size: 516116 Color: 1
Size: 482971 Color: 0

Bin 589: 927 of cap free
Amount of items: 2
Items: 
Size: 796772 Color: 1
Size: 202302 Color: 0

Bin 590: 929 of cap free
Amount of items: 2
Items: 
Size: 611770 Color: 0
Size: 387302 Color: 1

Bin 591: 938 of cap free
Amount of items: 2
Items: 
Size: 700324 Color: 0
Size: 298739 Color: 1

Bin 592: 940 of cap free
Amount of items: 2
Items: 
Size: 650977 Color: 1
Size: 348084 Color: 0

Bin 593: 941 of cap free
Amount of items: 2
Items: 
Size: 500787 Color: 0
Size: 498273 Color: 1

Bin 594: 943 of cap free
Amount of items: 2
Items: 
Size: 528566 Color: 0
Size: 470492 Color: 1

Bin 595: 945 of cap free
Amount of items: 2
Items: 
Size: 768405 Color: 0
Size: 230651 Color: 1

Bin 596: 948 of cap free
Amount of items: 2
Items: 
Size: 558215 Color: 0
Size: 440838 Color: 1

Bin 597: 949 of cap free
Amount of items: 2
Items: 
Size: 798249 Color: 0
Size: 200803 Color: 1

Bin 598: 950 of cap free
Amount of items: 2
Items: 
Size: 596789 Color: 0
Size: 402262 Color: 1

Bin 599: 950 of cap free
Amount of items: 2
Items: 
Size: 658042 Color: 0
Size: 341009 Color: 1

Bin 600: 951 of cap free
Amount of items: 3
Items: 
Size: 438493 Color: 1
Size: 383451 Color: 0
Size: 177106 Color: 1

Bin 601: 953 of cap free
Amount of items: 2
Items: 
Size: 647976 Color: 0
Size: 351072 Color: 1

Bin 602: 953 of cap free
Amount of items: 2
Items: 
Size: 650683 Color: 0
Size: 348365 Color: 1

Bin 603: 954 of cap free
Amount of items: 2
Items: 
Size: 695536 Color: 1
Size: 303511 Color: 0

Bin 604: 955 of cap free
Amount of items: 2
Items: 
Size: 578430 Color: 0
Size: 420616 Color: 1

Bin 605: 956 of cap free
Amount of items: 2
Items: 
Size: 748701 Color: 1
Size: 250344 Color: 0

Bin 606: 971 of cap free
Amount of items: 2
Items: 
Size: 767398 Color: 1
Size: 231632 Color: 0

Bin 607: 975 of cap free
Amount of items: 2
Items: 
Size: 600720 Color: 0
Size: 398306 Color: 1

Bin 608: 977 of cap free
Amount of items: 2
Items: 
Size: 585438 Color: 0
Size: 413586 Color: 1

Bin 609: 983 of cap free
Amount of items: 2
Items: 
Size: 751780 Color: 0
Size: 247238 Color: 1

Bin 610: 985 of cap free
Amount of items: 2
Items: 
Size: 617165 Color: 1
Size: 381851 Color: 0

Bin 611: 991 of cap free
Amount of items: 2
Items: 
Size: 568009 Color: 0
Size: 431001 Color: 1

Bin 612: 999 of cap free
Amount of items: 2
Items: 
Size: 655336 Color: 1
Size: 343666 Color: 0

Bin 613: 1011 of cap free
Amount of items: 2
Items: 
Size: 714817 Color: 0
Size: 284173 Color: 1

Bin 614: 1017 of cap free
Amount of items: 2
Items: 
Size: 783116 Color: 1
Size: 215868 Color: 0

Bin 615: 1026 of cap free
Amount of items: 2
Items: 
Size: 669984 Color: 0
Size: 328991 Color: 1

Bin 616: 1027 of cap free
Amount of items: 2
Items: 
Size: 717745 Color: 1
Size: 281229 Color: 0

Bin 617: 1028 of cap free
Amount of items: 2
Items: 
Size: 684743 Color: 1
Size: 314230 Color: 0

Bin 618: 1035 of cap free
Amount of items: 2
Items: 
Size: 623117 Color: 1
Size: 375849 Color: 0

Bin 619: 1038 of cap free
Amount of items: 2
Items: 
Size: 695144 Color: 0
Size: 303819 Color: 1

Bin 620: 1040 of cap free
Amount of items: 2
Items: 
Size: 544322 Color: 1
Size: 454639 Color: 0

Bin 621: 1043 of cap free
Amount of items: 2
Items: 
Size: 539740 Color: 1
Size: 459218 Color: 0

Bin 622: 1069 of cap free
Amount of items: 2
Items: 
Size: 720835 Color: 1
Size: 278097 Color: 0

Bin 623: 1070 of cap free
Amount of items: 2
Items: 
Size: 671156 Color: 1
Size: 327775 Color: 0

Bin 624: 1077 of cap free
Amount of items: 2
Items: 
Size: 531845 Color: 1
Size: 467079 Color: 0

Bin 625: 1092 of cap free
Amount of items: 2
Items: 
Size: 682769 Color: 0
Size: 316140 Color: 1

Bin 626: 1092 of cap free
Amount of items: 2
Items: 
Size: 749900 Color: 0
Size: 249009 Color: 1

Bin 627: 1093 of cap free
Amount of items: 2
Items: 
Size: 716033 Color: 0
Size: 282875 Color: 1

Bin 628: 1097 of cap free
Amount of items: 2
Items: 
Size: 517340 Color: 0
Size: 481564 Color: 1

Bin 629: 1098 of cap free
Amount of items: 2
Items: 
Size: 518574 Color: 0
Size: 480329 Color: 1

Bin 630: 1110 of cap free
Amount of items: 2
Items: 
Size: 756962 Color: 0
Size: 241929 Color: 1

Bin 631: 1114 of cap free
Amount of items: 2
Items: 
Size: 779424 Color: 1
Size: 219463 Color: 0

Bin 632: 1125 of cap free
Amount of items: 3
Items: 
Size: 758959 Color: 1
Size: 127025 Color: 1
Size: 112892 Color: 0

Bin 633: 1129 of cap free
Amount of items: 2
Items: 
Size: 789385 Color: 0
Size: 209487 Color: 1

Bin 634: 1151 of cap free
Amount of items: 2
Items: 
Size: 730976 Color: 0
Size: 267874 Color: 1

Bin 635: 1160 of cap free
Amount of items: 2
Items: 
Size: 745939 Color: 0
Size: 252902 Color: 1

Bin 636: 1162 of cap free
Amount of items: 2
Items: 
Size: 632117 Color: 0
Size: 366722 Color: 1

Bin 637: 1176 of cap free
Amount of items: 2
Items: 
Size: 773377 Color: 0
Size: 225448 Color: 1

Bin 638: 1191 of cap free
Amount of items: 2
Items: 
Size: 673908 Color: 1
Size: 324902 Color: 0

Bin 639: 1198 of cap free
Amount of items: 2
Items: 
Size: 711092 Color: 1
Size: 287711 Color: 0

Bin 640: 1205 of cap free
Amount of items: 2
Items: 
Size: 744076 Color: 1
Size: 254720 Color: 0

Bin 641: 1217 of cap free
Amount of items: 2
Items: 
Size: 523179 Color: 1
Size: 475605 Color: 0

Bin 642: 1219 of cap free
Amount of items: 2
Items: 
Size: 716030 Color: 0
Size: 282752 Color: 1

Bin 643: 1231 of cap free
Amount of items: 2
Items: 
Size: 690332 Color: 0
Size: 308438 Color: 1

Bin 644: 1236 of cap free
Amount of items: 2
Items: 
Size: 671443 Color: 0
Size: 327322 Color: 1

Bin 645: 1247 of cap free
Amount of items: 2
Items: 
Size: 665763 Color: 1
Size: 332991 Color: 0

Bin 646: 1255 of cap free
Amount of items: 3
Items: 
Size: 453635 Color: 1
Size: 381315 Color: 0
Size: 163796 Color: 0

Bin 647: 1256 of cap free
Amount of items: 2
Items: 
Size: 614076 Color: 1
Size: 384669 Color: 0

Bin 648: 1266 of cap free
Amount of items: 2
Items: 
Size: 578315 Color: 0
Size: 420420 Color: 1

Bin 649: 1268 of cap free
Amount of items: 2
Items: 
Size: 602238 Color: 1
Size: 396495 Color: 0

Bin 650: 1269 of cap free
Amount of items: 2
Items: 
Size: 620956 Color: 0
Size: 377776 Color: 1

Bin 651: 1295 of cap free
Amount of items: 2
Items: 
Size: 689426 Color: 1
Size: 309280 Color: 0

Bin 652: 1305 of cap free
Amount of items: 2
Items: 
Size: 529980 Color: 1
Size: 468716 Color: 0

Bin 653: 1315 of cap free
Amount of items: 3
Items: 
Size: 493903 Color: 1
Size: 342929 Color: 0
Size: 161854 Color: 0

Bin 654: 1320 of cap free
Amount of items: 2
Items: 
Size: 734871 Color: 0
Size: 263810 Color: 1

Bin 655: 1321 of cap free
Amount of items: 2
Items: 
Size: 676889 Color: 1
Size: 321791 Color: 0

Bin 656: 1323 of cap free
Amount of items: 2
Items: 
Size: 525040 Color: 1
Size: 473638 Color: 0

Bin 657: 1328 of cap free
Amount of items: 2
Items: 
Size: 652394 Color: 0
Size: 346279 Color: 1

Bin 658: 1336 of cap free
Amount of items: 2
Items: 
Size: 729387 Color: 0
Size: 269278 Color: 1

Bin 659: 1339 of cap free
Amount of items: 2
Items: 
Size: 791226 Color: 1
Size: 207436 Color: 0

Bin 660: 1355 of cap free
Amount of items: 2
Items: 
Size: 643044 Color: 0
Size: 355602 Color: 1

Bin 661: 1380 of cap free
Amount of items: 2
Items: 
Size: 746119 Color: 1
Size: 252502 Color: 0

Bin 662: 1381 of cap free
Amount of items: 2
Items: 
Size: 581099 Color: 0
Size: 417521 Color: 1

Bin 663: 1389 of cap free
Amount of items: 2
Items: 
Size: 759560 Color: 0
Size: 239052 Color: 1

Bin 664: 1393 of cap free
Amount of items: 2
Items: 
Size: 705703 Color: 1
Size: 292905 Color: 0

Bin 665: 1401 of cap free
Amount of items: 2
Items: 
Size: 504011 Color: 1
Size: 494589 Color: 0

Bin 666: 1404 of cap free
Amount of items: 2
Items: 
Size: 632917 Color: 1
Size: 365680 Color: 0

Bin 667: 1430 of cap free
Amount of items: 2
Items: 
Size: 616788 Color: 1
Size: 381783 Color: 0

Bin 668: 1455 of cap free
Amount of items: 2
Items: 
Size: 709848 Color: 0
Size: 288698 Color: 1

Bin 669: 1455 of cap free
Amount of items: 2
Items: 
Size: 713986 Color: 1
Size: 284560 Color: 0

Bin 670: 1461 of cap free
Amount of items: 2
Items: 
Size: 721907 Color: 0
Size: 276633 Color: 1

Bin 671: 1477 of cap free
Amount of items: 2
Items: 
Size: 584183 Color: 1
Size: 414341 Color: 0

Bin 672: 1495 of cap free
Amount of items: 2
Items: 
Size: 645711 Color: 1
Size: 352795 Color: 0

Bin 673: 1506 of cap free
Amount of items: 2
Items: 
Size: 629721 Color: 1
Size: 368774 Color: 0

Bin 674: 1508 of cap free
Amount of items: 2
Items: 
Size: 744059 Color: 0
Size: 254434 Color: 1

Bin 675: 1510 of cap free
Amount of items: 2
Items: 
Size: 546379 Color: 0
Size: 452112 Color: 1

Bin 676: 1512 of cap free
Amount of items: 2
Items: 
Size: 532448 Color: 0
Size: 466041 Color: 1

Bin 677: 1528 of cap free
Amount of items: 2
Items: 
Size: 586603 Color: 1
Size: 411870 Color: 0

Bin 678: 1529 of cap free
Amount of items: 2
Items: 
Size: 549495 Color: 1
Size: 448977 Color: 0

Bin 679: 1561 of cap free
Amount of items: 2
Items: 
Size: 699713 Color: 0
Size: 298727 Color: 1

Bin 680: 1562 of cap free
Amount of items: 2
Items: 
Size: 508468 Color: 1
Size: 489971 Color: 0

Bin 681: 1562 of cap free
Amount of items: 3
Items: 
Size: 556521 Color: 0
Size: 285225 Color: 1
Size: 156693 Color: 0

Bin 682: 1566 of cap free
Amount of items: 2
Items: 
Size: 792682 Color: 1
Size: 205753 Color: 0

Bin 683: 1571 of cap free
Amount of items: 2
Items: 
Size: 575214 Color: 0
Size: 423216 Color: 1

Bin 684: 1625 of cap free
Amount of items: 2
Items: 
Size: 692222 Color: 1
Size: 306154 Color: 0

Bin 685: 1628 of cap free
Amount of items: 2
Items: 
Size: 673575 Color: 1
Size: 324798 Color: 0

Bin 686: 1629 of cap free
Amount of items: 2
Items: 
Size: 610895 Color: 1
Size: 387477 Color: 0

Bin 687: 1632 of cap free
Amount of items: 2
Items: 
Size: 715804 Color: 1
Size: 282565 Color: 0

Bin 688: 1648 of cap free
Amount of items: 2
Items: 
Size: 691646 Color: 0
Size: 306707 Color: 1

Bin 689: 1671 of cap free
Amount of items: 2
Items: 
Size: 756734 Color: 0
Size: 241596 Color: 1

Bin 690: 1673 of cap free
Amount of items: 2
Items: 
Size: 677457 Color: 0
Size: 320871 Color: 1

Bin 691: 1675 of cap free
Amount of items: 2
Items: 
Size: 789066 Color: 0
Size: 209260 Color: 1

Bin 692: 1687 of cap free
Amount of items: 2
Items: 
Size: 686822 Color: 1
Size: 311492 Color: 0

Bin 693: 1691 of cap free
Amount of items: 2
Items: 
Size: 515548 Color: 1
Size: 482762 Color: 0

Bin 694: 1713 of cap free
Amount of items: 2
Items: 
Size: 764473 Color: 0
Size: 233815 Color: 1

Bin 695: 1717 of cap free
Amount of items: 2
Items: 
Size: 715965 Color: 0
Size: 282319 Color: 1

Bin 696: 1733 of cap free
Amount of items: 2
Items: 
Size: 525809 Color: 0
Size: 472459 Color: 1

Bin 697: 1735 of cap free
Amount of items: 2
Items: 
Size: 619469 Color: 1
Size: 378797 Color: 0

Bin 698: 1741 of cap free
Amount of items: 2
Items: 
Size: 775425 Color: 1
Size: 222835 Color: 0

Bin 699: 1743 of cap free
Amount of items: 2
Items: 
Size: 654941 Color: 1
Size: 343317 Color: 0

Bin 700: 1751 of cap free
Amount of items: 2
Items: 
Size: 594061 Color: 0
Size: 404189 Color: 1

Bin 701: 1774 of cap free
Amount of items: 2
Items: 
Size: 739424 Color: 0
Size: 258803 Color: 1

Bin 702: 1821 of cap free
Amount of items: 2
Items: 
Size: 590843 Color: 0
Size: 407337 Color: 1

Bin 703: 1831 of cap free
Amount of items: 2
Items: 
Size: 532334 Color: 0
Size: 465836 Color: 1

Bin 704: 1835 of cap free
Amount of items: 2
Items: 
Size: 513225 Color: 0
Size: 484941 Color: 1

Bin 705: 1839 of cap free
Amount of items: 2
Items: 
Size: 564352 Color: 0
Size: 433810 Color: 1

Bin 706: 1840 of cap free
Amount of items: 2
Items: 
Size: 769757 Color: 1
Size: 228404 Color: 0

Bin 707: 1841 of cap free
Amount of items: 2
Items: 
Size: 555735 Color: 0
Size: 442425 Color: 1

Bin 708: 1889 of cap free
Amount of items: 2
Items: 
Size: 710908 Color: 1
Size: 287204 Color: 0

Bin 709: 1935 of cap free
Amount of items: 2
Items: 
Size: 503799 Color: 1
Size: 494267 Color: 0

Bin 710: 1954 of cap free
Amount of items: 2
Items: 
Size: 588256 Color: 1
Size: 409791 Color: 0

Bin 711: 1958 of cap free
Amount of items: 2
Items: 
Size: 539035 Color: 0
Size: 459008 Color: 1

Bin 712: 1959 of cap free
Amount of items: 2
Items: 
Size: 605014 Color: 1
Size: 393028 Color: 0

Bin 713: 1971 of cap free
Amount of items: 2
Items: 
Size: 657735 Color: 0
Size: 340295 Color: 1

Bin 714: 1982 of cap free
Amount of items: 2
Items: 
Size: 631949 Color: 0
Size: 366070 Color: 1

Bin 715: 2009 of cap free
Amount of items: 2
Items: 
Size: 529764 Color: 1
Size: 468228 Color: 0

Bin 716: 2095 of cap free
Amount of items: 2
Items: 
Size: 590633 Color: 0
Size: 407273 Color: 1

Bin 717: 2095 of cap free
Amount of items: 2
Items: 
Size: 629650 Color: 1
Size: 368256 Color: 0

Bin 718: 2109 of cap free
Amount of items: 2
Items: 
Size: 766873 Color: 1
Size: 231019 Color: 0

Bin 719: 2122 of cap free
Amount of items: 2
Items: 
Size: 715916 Color: 0
Size: 281963 Color: 1

Bin 720: 2127 of cap free
Amount of items: 2
Items: 
Size: 726402 Color: 1
Size: 271472 Color: 0

Bin 721: 2130 of cap free
Amount of items: 2
Items: 
Size: 730752 Color: 0
Size: 267119 Color: 1

Bin 722: 2142 of cap free
Amount of items: 2
Items: 
Size: 551943 Color: 0
Size: 445916 Color: 1

Bin 723: 2152 of cap free
Amount of items: 2
Items: 
Size: 667099 Color: 1
Size: 330750 Color: 0

Bin 724: 2157 of cap free
Amount of items: 2
Items: 
Size: 747893 Color: 1
Size: 249951 Color: 0

Bin 725: 2162 of cap free
Amount of items: 2
Items: 
Size: 559000 Color: 1
Size: 438839 Color: 0

Bin 726: 2162 of cap free
Amount of items: 2
Items: 
Size: 614017 Color: 1
Size: 383822 Color: 0

Bin 727: 2183 of cap free
Amount of items: 2
Items: 
Size: 772531 Color: 1
Size: 225287 Color: 0

Bin 728: 2190 of cap free
Amount of items: 2
Items: 
Size: 717645 Color: 1
Size: 280166 Color: 0

Bin 729: 2198 of cap free
Amount of items: 3
Items: 
Size: 565889 Color: 1
Size: 313784 Color: 1
Size: 118130 Color: 0

Bin 730: 2201 of cap free
Amount of items: 2
Items: 
Size: 525363 Color: 0
Size: 472437 Color: 1

Bin 731: 2205 of cap free
Amount of items: 2
Items: 
Size: 779030 Color: 0
Size: 218766 Color: 1

Bin 732: 2210 of cap free
Amount of items: 2
Items: 
Size: 799102 Color: 1
Size: 198689 Color: 0

Bin 733: 2213 of cap free
Amount of items: 2
Items: 
Size: 654746 Color: 1
Size: 343042 Color: 0

Bin 734: 2219 of cap free
Amount of items: 2
Items: 
Size: 673996 Color: 0
Size: 323786 Color: 1

Bin 735: 2239 of cap free
Amount of items: 2
Items: 
Size: 604791 Color: 1
Size: 392971 Color: 0

Bin 736: 2293 of cap free
Amount of items: 2
Items: 
Size: 508072 Color: 1
Size: 489636 Color: 0

Bin 737: 2298 of cap free
Amount of items: 2
Items: 
Size: 709227 Color: 0
Size: 288476 Color: 1

Bin 738: 2312 of cap free
Amount of items: 2
Items: 
Size: 719883 Color: 1
Size: 277806 Color: 0

Bin 739: 2324 of cap free
Amount of items: 2
Items: 
Size: 614541 Color: 0
Size: 383136 Color: 1

Bin 740: 2328 of cap free
Amount of items: 2
Items: 
Size: 629540 Color: 1
Size: 368133 Color: 0

Bin 741: 2332 of cap free
Amount of items: 3
Items: 
Size: 570106 Color: 0
Size: 285565 Color: 1
Size: 141998 Color: 0

Bin 742: 2350 of cap free
Amount of items: 2
Items: 
Size: 610656 Color: 0
Size: 386995 Color: 1

Bin 743: 2351 of cap free
Amount of items: 2
Items: 
Size: 643199 Color: 1
Size: 354451 Color: 0

Bin 744: 2374 of cap free
Amount of items: 2
Items: 
Size: 721561 Color: 0
Size: 276066 Color: 1

Bin 745: 2378 of cap free
Amount of items: 2
Items: 
Size: 743508 Color: 1
Size: 254115 Color: 0

Bin 746: 2405 of cap free
Amount of items: 2
Items: 
Size: 619455 Color: 1
Size: 378141 Color: 0

Bin 747: 2412 of cap free
Amount of items: 2
Items: 
Size: 734212 Color: 0
Size: 263377 Color: 1

Bin 748: 2425 of cap free
Amount of items: 2
Items: 
Size: 681483 Color: 0
Size: 316093 Color: 1

Bin 749: 2436 of cap free
Amount of items: 2
Items: 
Size: 726148 Color: 1
Size: 271417 Color: 0

Bin 750: 2452 of cap free
Amount of items: 2
Items: 
Size: 713139 Color: 1
Size: 284410 Color: 0

Bin 751: 2471 of cap free
Amount of items: 2
Items: 
Size: 739541 Color: 1
Size: 257989 Color: 0

Bin 752: 2472 of cap free
Amount of items: 2
Items: 
Size: 652208 Color: 1
Size: 345321 Color: 0

Bin 753: 2490 of cap free
Amount of items: 2
Items: 
Size: 510460 Color: 0
Size: 487051 Color: 1

Bin 754: 2500 of cap free
Amount of items: 2
Items: 
Size: 756581 Color: 0
Size: 240920 Color: 1

Bin 755: 2514 of cap free
Amount of items: 2
Items: 
Size: 676224 Color: 1
Size: 321263 Color: 0

Bin 756: 2516 of cap free
Amount of items: 2
Items: 
Size: 534171 Color: 1
Size: 463314 Color: 0

Bin 757: 2580 of cap free
Amount of items: 2
Items: 
Size: 659189 Color: 1
Size: 338232 Color: 0

Bin 758: 2589 of cap free
Amount of items: 2
Items: 
Size: 798978 Color: 1
Size: 198434 Color: 0

Bin 759: 2590 of cap free
Amount of items: 2
Items: 
Size: 687485 Color: 0
Size: 309926 Color: 1

Bin 760: 2618 of cap free
Amount of items: 3
Items: 
Size: 553696 Color: 0
Size: 305271 Color: 0
Size: 138416 Color: 1

Bin 761: 2644 of cap free
Amount of items: 2
Items: 
Size: 614512 Color: 0
Size: 382845 Color: 1

Bin 762: 2682 of cap free
Amount of items: 2
Items: 
Size: 689049 Color: 1
Size: 308270 Color: 0

Bin 763: 2705 of cap free
Amount of items: 2
Items: 
Size: 604510 Color: 1
Size: 392786 Color: 0

Bin 764: 2745 of cap free
Amount of items: 2
Items: 
Size: 524739 Color: 1
Size: 472517 Color: 0

Bin 765: 2801 of cap free
Amount of items: 3
Items: 
Size: 561262 Color: 1
Size: 285370 Color: 1
Size: 150568 Color: 0

Bin 766: 2836 of cap free
Amount of items: 2
Items: 
Size: 675998 Color: 1
Size: 321167 Color: 0

Bin 767: 2838 of cap free
Amount of items: 2
Items: 
Size: 664252 Color: 0
Size: 332911 Color: 1

Bin 768: 2845 of cap free
Amount of items: 2
Items: 
Size: 626066 Color: 1
Size: 371090 Color: 0

Bin 769: 2885 of cap free
Amount of items: 2
Items: 
Size: 499214 Color: 0
Size: 497902 Color: 1

Bin 770: 2906 of cap free
Amount of items: 2
Items: 
Size: 525212 Color: 0
Size: 471883 Color: 1

Bin 771: 2914 of cap free
Amount of items: 2
Items: 
Size: 738633 Color: 0
Size: 258454 Color: 1

Bin 772: 2916 of cap free
Amount of items: 2
Items: 
Size: 528269 Color: 0
Size: 468816 Color: 1

Bin 773: 2935 of cap free
Amount of items: 2
Items: 
Size: 592314 Color: 1
Size: 404752 Color: 0

Bin 774: 2937 of cap free
Amount of items: 2
Items: 
Size: 798003 Color: 0
Size: 199061 Color: 1

Bin 775: 2943 of cap free
Amount of items: 3
Items: 
Size: 534876 Color: 0
Size: 305791 Color: 1
Size: 156391 Color: 1

Bin 776: 2995 of cap free
Amount of items: 2
Items: 
Size: 524698 Color: 1
Size: 472308 Color: 0

Bin 777: 3085 of cap free
Amount of items: 2
Items: 
Size: 605632 Color: 0
Size: 391284 Color: 1

Bin 778: 3092 of cap free
Amount of items: 2
Items: 
Size: 614124 Color: 0
Size: 382785 Color: 1

Bin 779: 3108 of cap free
Amount of items: 2
Items: 
Size: 517029 Color: 0
Size: 479864 Color: 1

Bin 780: 3137 of cap free
Amount of items: 2
Items: 
Size: 656972 Color: 0
Size: 339892 Color: 1

Bin 781: 3142 of cap free
Amount of items: 2
Items: 
Size: 610664 Color: 1
Size: 386195 Color: 0

Bin 782: 3242 of cap free
Amount of items: 2
Items: 
Size: 642882 Color: 0
Size: 353877 Color: 1

Bin 783: 3281 of cap free
Amount of items: 2
Items: 
Size: 625441 Color: 0
Size: 371279 Color: 1

Bin 784: 3301 of cap free
Amount of items: 2
Items: 
Size: 730160 Color: 1
Size: 266540 Color: 0

Bin 785: 3309 of cap free
Amount of items: 2
Items: 
Size: 564030 Color: 0
Size: 432662 Color: 1

Bin 786: 3336 of cap free
Amount of items: 2
Items: 
Size: 583767 Color: 0
Size: 412898 Color: 1

Bin 787: 3338 of cap free
Amount of items: 2
Items: 
Size: 547970 Color: 1
Size: 448693 Color: 0

Bin 788: 3348 of cap free
Amount of items: 2
Items: 
Size: 580287 Color: 0
Size: 416366 Color: 1

Bin 789: 3429 of cap free
Amount of items: 2
Items: 
Size: 663714 Color: 0
Size: 332858 Color: 1

Bin 790: 3498 of cap free
Amount of items: 2
Items: 
Size: 725649 Color: 1
Size: 270854 Color: 0

Bin 791: 3503 of cap free
Amount of items: 2
Items: 
Size: 592215 Color: 1
Size: 404283 Color: 0

Bin 792: 3515 of cap free
Amount of items: 2
Items: 
Size: 663567 Color: 1
Size: 332919 Color: 0

Bin 793: 3591 of cap free
Amount of items: 2
Items: 
Size: 613667 Color: 0
Size: 382743 Color: 1

Bin 794: 3664 of cap free
Amount of items: 2
Items: 
Size: 575857 Color: 1
Size: 420480 Color: 0

Bin 795: 3666 of cap free
Amount of items: 2
Items: 
Size: 547909 Color: 1
Size: 448426 Color: 0

Bin 796: 3686 of cap free
Amount of items: 2
Items: 
Size: 778600 Color: 0
Size: 217715 Color: 1

Bin 797: 3789 of cap free
Amount of items: 2
Items: 
Size: 625155 Color: 0
Size: 371057 Color: 1

Bin 798: 3869 of cap free
Amount of items: 2
Items: 
Size: 625604 Color: 1
Size: 370528 Color: 0

Bin 799: 3894 of cap free
Amount of items: 2
Items: 
Size: 650240 Color: 0
Size: 345867 Color: 1

Bin 800: 3900 of cap free
Amount of items: 2
Items: 
Size: 630407 Color: 0
Size: 365694 Color: 1

Bin 801: 3919 of cap free
Amount of items: 2
Items: 
Size: 537654 Color: 0
Size: 458428 Color: 1

Bin 802: 4014 of cap free
Amount of items: 2
Items: 
Size: 704705 Color: 1
Size: 291282 Color: 0

Bin 803: 4017 of cap free
Amount of items: 2
Items: 
Size: 657978 Color: 1
Size: 338006 Color: 0

Bin 804: 4034 of cap free
Amount of items: 2
Items: 
Size: 798085 Color: 1
Size: 197882 Color: 0

Bin 805: 4084 of cap free
Amount of items: 2
Items: 
Size: 516426 Color: 0
Size: 479491 Color: 1

Bin 806: 4186 of cap free
Amount of items: 2
Items: 
Size: 632698 Color: 1
Size: 363117 Color: 0

Bin 807: 4191 of cap free
Amount of items: 2
Items: 
Size: 778510 Color: 0
Size: 217300 Color: 1

Bin 808: 4297 of cap free
Amount of items: 2
Items: 
Size: 786700 Color: 0
Size: 209004 Color: 1

Bin 809: 4320 of cap free
Amount of items: 2
Items: 
Size: 749592 Color: 0
Size: 246089 Color: 1

Bin 810: 4427 of cap free
Amount of items: 2
Items: 
Size: 498105 Color: 0
Size: 497469 Color: 1

Bin 811: 4446 of cap free
Amount of items: 2
Items: 
Size: 624715 Color: 0
Size: 370840 Color: 1

Bin 812: 4465 of cap free
Amount of items: 2
Items: 
Size: 551245 Color: 0
Size: 444291 Color: 1

Bin 813: 4481 of cap free
Amount of items: 2
Items: 
Size: 575521 Color: 1
Size: 419999 Color: 0

Bin 814: 4498 of cap free
Amount of items: 2
Items: 
Size: 558871 Color: 1
Size: 436632 Color: 0

Bin 815: 4507 of cap free
Amount of items: 2
Items: 
Size: 643094 Color: 1
Size: 352400 Color: 0

Bin 816: 4601 of cap free
Amount of items: 2
Items: 
Size: 728445 Color: 0
Size: 266955 Color: 1

Bin 817: 4649 of cap free
Amount of items: 2
Items: 
Size: 724697 Color: 1
Size: 270655 Color: 0

Bin 818: 4731 of cap free
Amount of items: 2
Items: 
Size: 575424 Color: 1
Size: 419846 Color: 0

Bin 819: 4774 of cap free
Amount of items: 2
Items: 
Size: 583719 Color: 0
Size: 411508 Color: 1

Bin 820: 4908 of cap free
Amount of items: 2
Items: 
Size: 697534 Color: 0
Size: 297559 Color: 1

Bin 821: 5014 of cap free
Amount of items: 2
Items: 
Size: 680887 Color: 0
Size: 314100 Color: 1

Bin 822: 5015 of cap free
Amount of items: 2
Items: 
Size: 649465 Color: 0
Size: 345521 Color: 1

Bin 823: 5147 of cap free
Amount of items: 2
Items: 
Size: 563892 Color: 0
Size: 430962 Color: 1

Bin 824: 5157 of cap free
Amount of items: 2
Items: 
Size: 597900 Color: 0
Size: 396944 Color: 1

Bin 825: 5159 of cap free
Amount of items: 2
Items: 
Size: 778416 Color: 0
Size: 216426 Color: 1

Bin 826: 5166 of cap free
Amount of items: 2
Items: 
Size: 629529 Color: 0
Size: 365306 Color: 1

Bin 827: 5226 of cap free
Amount of items: 2
Items: 
Size: 730126 Color: 1
Size: 264649 Color: 0

Bin 828: 5279 of cap free
Amount of items: 2
Items: 
Size: 522653 Color: 1
Size: 472069 Color: 0

Bin 829: 5297 of cap free
Amount of items: 2
Items: 
Size: 497809 Color: 0
Size: 496895 Color: 1

Bin 830: 5303 of cap free
Amount of items: 2
Items: 
Size: 786599 Color: 0
Size: 208099 Color: 1

Bin 831: 5325 of cap free
Amount of items: 2
Items: 
Size: 663506 Color: 0
Size: 331170 Color: 1

Bin 832: 5587 of cap free
Amount of items: 2
Items: 
Size: 778343 Color: 0
Size: 216071 Color: 1

Bin 833: 5635 of cap free
Amount of items: 2
Items: 
Size: 771708 Color: 1
Size: 222658 Color: 0

Bin 834: 5652 of cap free
Amount of items: 2
Items: 
Size: 631914 Color: 1
Size: 362435 Color: 0

Bin 835: 5765 of cap free
Amount of items: 2
Items: 
Size: 680387 Color: 0
Size: 313849 Color: 1

Bin 836: 5774 of cap free
Amount of items: 3
Items: 
Size: 553043 Color: 0
Size: 302799 Color: 0
Size: 138385 Color: 1

Bin 837: 5782 of cap free
Amount of items: 2
Items: 
Size: 686078 Color: 1
Size: 308141 Color: 0

Bin 838: 5878 of cap free
Amount of items: 2
Items: 
Size: 563886 Color: 0
Size: 430237 Color: 1

Bin 839: 5944 of cap free
Amount of items: 2
Items: 
Size: 613633 Color: 0
Size: 380424 Color: 1

Bin 840: 6205 of cap free
Amount of items: 2
Items: 
Size: 525141 Color: 0
Size: 468655 Color: 1

Bin 841: 6257 of cap free
Amount of items: 2
Items: 
Size: 584100 Color: 1
Size: 409644 Color: 0

Bin 842: 6291 of cap free
Amount of items: 2
Items: 
Size: 663070 Color: 1
Size: 330640 Color: 0

Bin 843: 6301 of cap free
Amount of items: 2
Items: 
Size: 748825 Color: 0
Size: 244875 Color: 1

Bin 844: 6324 of cap free
Amount of items: 2
Items: 
Size: 777969 Color: 0
Size: 215708 Color: 1

Bin 845: 6332 of cap free
Amount of items: 2
Items: 
Size: 613368 Color: 0
Size: 380301 Color: 1

Bin 846: 6347 of cap free
Amount of items: 2
Items: 
Size: 574351 Color: 1
Size: 419303 Color: 0

Bin 847: 6440 of cap free
Amount of items: 2
Items: 
Size: 729890 Color: 1
Size: 263671 Color: 0

Bin 848: 6494 of cap free
Amount of items: 2
Items: 
Size: 795785 Color: 1
Size: 197722 Color: 0

Bin 849: 6502 of cap free
Amount of items: 4
Items: 
Size: 323146 Color: 1
Size: 303557 Color: 1
Size: 183906 Color: 0
Size: 182890 Color: 0

Bin 850: 6513 of cap free
Amount of items: 2
Items: 
Size: 574197 Color: 1
Size: 419291 Color: 0

Bin 851: 6537 of cap free
Amount of items: 2
Items: 
Size: 777948 Color: 0
Size: 215516 Color: 1

Bin 852: 6605 of cap free
Amount of items: 2
Items: 
Size: 709142 Color: 1
Size: 284254 Color: 0

Bin 853: 6670 of cap free
Amount of items: 2
Items: 
Size: 794359 Color: 0
Size: 198972 Color: 1

Bin 854: 6841 of cap free
Amount of items: 2
Items: 
Size: 763222 Color: 0
Size: 229938 Color: 1

Bin 855: 6844 of cap free
Amount of items: 2
Items: 
Size: 697328 Color: 0
Size: 295829 Color: 1

Bin 856: 7094 of cap free
Amount of items: 2
Items: 
Size: 777702 Color: 0
Size: 215205 Color: 1

Bin 857: 7215 of cap free
Amount of items: 2
Items: 
Size: 591962 Color: 1
Size: 400824 Color: 0

Bin 858: 7236 of cap free
Amount of items: 2
Items: 
Size: 573690 Color: 1
Size: 419075 Color: 0

Bin 859: 7254 of cap free
Amount of items: 2
Items: 
Size: 506404 Color: 1
Size: 486343 Color: 0

Bin 860: 7727 of cap free
Amount of items: 2
Items: 
Size: 520254 Color: 1
Size: 472020 Color: 0

Bin 861: 7757 of cap free
Amount of items: 2
Items: 
Size: 563436 Color: 0
Size: 428808 Color: 1

Bin 862: 7776 of cap free
Amount of items: 2
Items: 
Size: 520246 Color: 1
Size: 471979 Color: 0

Bin 863: 7899 of cap free
Amount of items: 2
Items: 
Size: 763211 Color: 0
Size: 228891 Color: 1

Bin 864: 7972 of cap free
Amount of items: 2
Items: 
Size: 595932 Color: 0
Size: 396097 Color: 1

Bin 865: 8241 of cap free
Amount of items: 2
Items: 
Size: 563386 Color: 0
Size: 428374 Color: 1

Bin 866: 8340 of cap free
Amount of items: 2
Items: 
Size: 742769 Color: 1
Size: 248892 Color: 0

Bin 867: 8408 of cap free
Amount of items: 2
Items: 
Size: 563321 Color: 0
Size: 428272 Color: 1

Bin 868: 8419 of cap free
Amount of items: 2
Items: 
Size: 769182 Color: 1
Size: 222400 Color: 0

Bin 869: 8429 of cap free
Amount of items: 2
Items: 
Size: 599335 Color: 1
Size: 392237 Color: 0

Bin 870: 8550 of cap free
Amount of items: 2
Items: 
Size: 573384 Color: 1
Size: 418067 Color: 0

Bin 871: 8564 of cap free
Amount of items: 2
Items: 
Size: 533738 Color: 1
Size: 457699 Color: 0

Bin 872: 8620 of cap free
Amount of items: 2
Items: 
Size: 646741 Color: 0
Size: 344640 Color: 1

Bin 873: 8818 of cap free
Amount of items: 2
Items: 
Size: 533695 Color: 1
Size: 457488 Color: 0

Bin 874: 8993 of cap free
Amount of items: 2
Items: 
Size: 646461 Color: 0
Size: 344547 Color: 1

Bin 875: 9014 of cap free
Amount of items: 2
Items: 
Size: 562790 Color: 0
Size: 428197 Color: 1

Bin 876: 9563 of cap free
Amount of items: 2
Items: 
Size: 745789 Color: 0
Size: 244649 Color: 1

Bin 877: 9607 of cap free
Amount of items: 2
Items: 
Size: 742020 Color: 1
Size: 248374 Color: 0

Bin 878: 9925 of cap free
Amount of items: 2
Items: 
Size: 572844 Color: 1
Size: 417232 Color: 0

Bin 879: 10879 of cap free
Amount of items: 2
Items: 
Size: 609264 Color: 0
Size: 379858 Color: 1

Bin 880: 11296 of cap free
Amount of items: 2
Items: 
Size: 503545 Color: 1
Size: 485160 Color: 0

Bin 881: 11655 of cap free
Amount of items: 2
Items: 
Size: 609189 Color: 0
Size: 379157 Color: 1

Bin 882: 11950 of cap free
Amount of items: 2
Items: 
Size: 503271 Color: 1
Size: 484780 Color: 0

Bin 883: 13078 of cap free
Amount of items: 2
Items: 
Size: 515430 Color: 1
Size: 471493 Color: 0

Bin 884: 15513 of cap free
Amount of items: 2
Items: 
Size: 609062 Color: 0
Size: 375426 Color: 1

Bin 885: 17753 of cap free
Amount of items: 2
Items: 
Size: 524381 Color: 0
Size: 457867 Color: 1

Bin 886: 19246 of cap free
Amount of items: 2
Items: 
Size: 522969 Color: 0
Size: 457786 Color: 1

Bin 887: 19412 of cap free
Amount of items: 2
Items: 
Size: 574035 Color: 0
Size: 406554 Color: 1

Bin 888: 38019 of cap free
Amount of items: 3
Items: 
Size: 551718 Color: 1
Size: 213617 Color: 1
Size: 196647 Color: 0

Bin 889: 53663 of cap free
Amount of items: 3
Items: 
Size: 544281 Color: 1
Size: 207732 Color: 1
Size: 194325 Color: 0

Bin 890: 58230 of cap free
Amount of items: 5
Items: 
Size: 194723 Color: 1
Size: 193764 Color: 1
Size: 193142 Color: 1
Size: 180305 Color: 0
Size: 179837 Color: 0

Bin 891: 61057 of cap free
Amount of items: 3
Items: 
Size: 546769 Color: 1
Size: 196517 Color: 0
Size: 195658 Color: 0

Bin 892: 65208 of cap free
Amount of items: 2
Items: 
Size: 544214 Color: 1
Size: 390579 Color: 0

Bin 893: 65252 of cap free
Amount of items: 3
Items: 
Size: 542969 Color: 1
Size: 198600 Color: 1
Size: 193180 Color: 0

Bin 894: 66136 of cap free
Amount of items: 3
Items: 
Size: 542894 Color: 1
Size: 197920 Color: 1
Size: 193051 Color: 0

Bin 895: 70113 of cap free
Amount of items: 3
Items: 
Size: 377597 Color: 0
Size: 363485 Color: 1
Size: 188806 Color: 0

Bin 896: 75376 of cap free
Amount of items: 3
Items: 
Size: 542688 Color: 1
Size: 192345 Color: 0
Size: 189592 Color: 0

Bin 897: 87229 of cap free
Amount of items: 5
Items: 
Size: 193017 Color: 1
Size: 192990 Color: 1
Size: 176750 Color: 0
Size: 175020 Color: 0
Size: 174995 Color: 0

Bin 898: 88755 of cap free
Amount of items: 3
Items: 
Size: 363404 Color: 1
Size: 350382 Color: 0
Size: 197460 Color: 1

Bin 899: 130762 of cap free
Amount of items: 3
Items: 
Size: 350280 Color: 0
Size: 330809 Color: 1
Size: 188150 Color: 0

Bin 900: 134187 of cap free
Amount of items: 5
Items: 
Size: 184665 Color: 1
Size: 184550 Color: 1
Size: 166508 Color: 1
Size: 166434 Color: 0
Size: 163657 Color: 0

Bin 901: 522304 of cap free
Amount of items: 3
Items: 
Size: 161322 Color: 0
Size: 160627 Color: 0
Size: 155748 Color: 1

Total size: 898317736
Total free space: 2683165

