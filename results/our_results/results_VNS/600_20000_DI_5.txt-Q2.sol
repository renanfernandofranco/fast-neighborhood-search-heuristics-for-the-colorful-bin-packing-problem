Capicity Bin: 15712
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 24
Items: 
Size: 804 Color: 1
Size: 800 Color: 1
Size: 796 Color: 1
Size: 768 Color: 1
Size: 760 Color: 0
Size: 736 Color: 1
Size: 728 Color: 0
Size: 720 Color: 1
Size: 720 Color: 1
Size: 692 Color: 0
Size: 688 Color: 1
Size: 668 Color: 1
Size: 640 Color: 1
Size: 640 Color: 1
Size: 632 Color: 0
Size: 610 Color: 0
Size: 608 Color: 0
Size: 592 Color: 1
Size: 560 Color: 0
Size: 548 Color: 0
Size: 538 Color: 0
Size: 532 Color: 0
Size: 516 Color: 0
Size: 416 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 7878 Color: 0
Size: 1898 Color: 1
Size: 1538 Color: 0
Size: 1514 Color: 1
Size: 1470 Color: 0
Size: 1142 Color: 0
Size: 272 Color: 1

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 7892 Color: 0
Size: 5580 Color: 1
Size: 1592 Color: 0
Size: 352 Color: 1
Size: 296 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7876 Color: 1
Size: 7308 Color: 1
Size: 528 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 0
Size: 5848 Color: 1
Size: 336 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10314 Color: 0
Size: 5070 Color: 1
Size: 328 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10360 Color: 0
Size: 5016 Color: 0
Size: 336 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10724 Color: 0
Size: 4108 Color: 0
Size: 880 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10664 Color: 1
Size: 4648 Color: 0
Size: 400 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10756 Color: 0
Size: 4472 Color: 0
Size: 484 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10896 Color: 0
Size: 4048 Color: 0
Size: 768 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10866 Color: 1
Size: 4502 Color: 0
Size: 344 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 0
Size: 3592 Color: 1
Size: 1072 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11356 Color: 0
Size: 3896 Color: 0
Size: 460 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11559 Color: 0
Size: 2691 Color: 1
Size: 1462 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11784 Color: 1
Size: 3636 Color: 0
Size: 292 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 1
Size: 3592 Color: 0
Size: 240 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 1
Size: 3148 Color: 0
Size: 624 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11908 Color: 0
Size: 3160 Color: 0
Size: 644 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12264 Color: 1
Size: 2952 Color: 1
Size: 496 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12503 Color: 1
Size: 2675 Color: 1
Size: 534 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 1
Size: 2372 Color: 0
Size: 632 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12735 Color: 1
Size: 2077 Color: 1
Size: 900 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 1
Size: 2364 Color: 0
Size: 512 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12850 Color: 1
Size: 2322 Color: 0
Size: 540 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 1
Size: 1528 Color: 0
Size: 1152 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 0
Size: 1918 Color: 1
Size: 720 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 1
Size: 1300 Color: 1
Size: 1296 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13093 Color: 0
Size: 2349 Color: 0
Size: 270 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13176 Color: 1
Size: 1480 Color: 1
Size: 1056 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13186 Color: 1
Size: 2094 Color: 0
Size: 432 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13202 Color: 0
Size: 2088 Color: 0
Size: 422 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13221 Color: 1
Size: 1931 Color: 0
Size: 560 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 0
Size: 1864 Color: 1
Size: 624 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13284 Color: 1
Size: 1822 Color: 1
Size: 606 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 0
Size: 1996 Color: 1
Size: 398 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13290 Color: 1
Size: 1936 Color: 0
Size: 486 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13317 Color: 1
Size: 1983 Color: 0
Size: 412 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 1232 Color: 0
Size: 716 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13782 Color: 1
Size: 1378 Color: 0
Size: 552 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 1
Size: 1494 Color: 1
Size: 414 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13922 Color: 0
Size: 1028 Color: 0
Size: 762 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 1
Size: 1682 Color: 0
Size: 80 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 0
Size: 1364 Color: 1
Size: 424 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14000 Color: 0
Size: 1008 Color: 1
Size: 704 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14072 Color: 1
Size: 1136 Color: 0
Size: 504 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14134 Color: 1
Size: 952 Color: 1
Size: 626 Color: 0

Bin 48: 1 of cap free
Amount of items: 11
Items: 
Size: 7857 Color: 0
Size: 1000 Color: 0
Size: 960 Color: 0
Size: 888 Color: 0
Size: 880 Color: 1
Size: 880 Color: 1
Size: 848 Color: 1
Size: 824 Color: 1
Size: 804 Color: 0
Size: 400 Color: 0
Size: 370 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 8702 Color: 1
Size: 6545 Color: 1
Size: 464 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 8849 Color: 1
Size: 6542 Color: 1
Size: 320 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 8889 Color: 1
Size: 6518 Color: 0
Size: 304 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 9709 Color: 1
Size: 5058 Color: 0
Size: 944 Color: 1

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 10298 Color: 1
Size: 5017 Color: 0
Size: 396 Color: 1

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 1
Size: 4445 Color: 0
Size: 320 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 11474 Color: 1
Size: 3981 Color: 0
Size: 256 Color: 1

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 11945 Color: 1
Size: 3602 Color: 1
Size: 164 Color: 0

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12782 Color: 0
Size: 2113 Color: 0
Size: 816 Color: 1

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 13177 Color: 0
Size: 2534 Color: 1

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 13397 Color: 1
Size: 2314 Color: 0

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 13463 Color: 1
Size: 2248 Color: 0

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 1
Size: 1875 Color: 0

Bin 62: 2 of cap free
Amount of items: 8
Items: 
Size: 7860 Color: 1
Size: 1502 Color: 1
Size: 1468 Color: 0
Size: 1456 Color: 0
Size: 1450 Color: 0
Size: 1434 Color: 1
Size: 312 Color: 0
Size: 228 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 9208 Color: 0
Size: 6198 Color: 1
Size: 304 Color: 1

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 9693 Color: 0
Size: 5733 Color: 1
Size: 284 Color: 0

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 10854 Color: 0
Size: 4216 Color: 1
Size: 640 Color: 0

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 11342 Color: 1
Size: 4072 Color: 1
Size: 296 Color: 0

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 11380 Color: 1
Size: 3974 Color: 0
Size: 356 Color: 1

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11850 Color: 1
Size: 2970 Color: 1
Size: 890 Color: 0

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 12153 Color: 1
Size: 3161 Color: 0
Size: 396 Color: 1

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 12121 Color: 0
Size: 2993 Color: 1
Size: 596 Color: 0

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 12696 Color: 1
Size: 2022 Color: 0
Size: 992 Color: 1

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 0
Size: 2586 Color: 1

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13254 Color: 0
Size: 2456 Color: 1

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 0
Size: 2386 Color: 1

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 0
Size: 2230 Color: 1

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13590 Color: 1
Size: 2120 Color: 0

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 1
Size: 2050 Color: 0

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 1
Size: 1606 Color: 0
Size: 288 Color: 0

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13994 Color: 0
Size: 1716 Color: 1

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 1
Size: 3451 Color: 0
Size: 330 Color: 0

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 0
Size: 2301 Color: 0
Size: 632 Color: 1

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 12789 Color: 0
Size: 2648 Color: 1
Size: 272 Color: 0

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 13193 Color: 0
Size: 1492 Color: 1
Size: 1024 Color: 0

Bin 84: 4 of cap free
Amount of items: 9
Items: 
Size: 7858 Color: 0
Size: 1196 Color: 0
Size: 1112 Color: 1
Size: 1112 Color: 0
Size: 1104 Color: 0
Size: 1012 Color: 1
Size: 1002 Color: 1
Size: 896 Color: 1
Size: 416 Color: 1

Bin 85: 4 of cap free
Amount of items: 7
Items: 
Size: 7870 Color: 0
Size: 1404 Color: 1
Size: 1368 Color: 1
Size: 1362 Color: 1
Size: 1308 Color: 0
Size: 1236 Color: 1
Size: 1160 Color: 0

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 8712 Color: 0
Size: 5850 Color: 1
Size: 1146 Color: 0

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 9704 Color: 0
Size: 5556 Color: 1
Size: 448 Color: 0

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 1
Size: 5564 Color: 0
Size: 304 Color: 1

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 0
Size: 4164 Color: 1
Size: 704 Color: 1

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 11410 Color: 0
Size: 4042 Color: 1
Size: 256 Color: 0

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 12340 Color: 0
Size: 3368 Color: 1

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 0
Size: 2404 Color: 1

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13414 Color: 0
Size: 2294 Color: 1

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 9988 Color: 0
Size: 5003 Color: 1
Size: 716 Color: 0

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 10935 Color: 1
Size: 4772 Color: 0

Bin 96: 6 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 1
Size: 3586 Color: 0
Size: 268 Color: 0

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 1
Size: 2008 Color: 0
Size: 1104 Color: 0

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 13333 Color: 0
Size: 2101 Color: 1
Size: 272 Color: 0

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 13530 Color: 1
Size: 2176 Color: 0

Bin 100: 7 of cap free
Amount of items: 3
Items: 
Size: 8833 Color: 1
Size: 6552 Color: 1
Size: 320 Color: 0

Bin 101: 7 of cap free
Amount of items: 2
Items: 
Size: 11573 Color: 1
Size: 4132 Color: 0

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 11929 Color: 1
Size: 2984 Color: 1
Size: 792 Color: 0

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 12483 Color: 1
Size: 3222 Color: 0

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 1
Size: 2193 Color: 0

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 10152 Color: 1
Size: 5376 Color: 1
Size: 176 Color: 0

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 12953 Color: 1
Size: 2751 Color: 0

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 12930 Color: 0
Size: 2774 Color: 1

Bin 108: 10 of cap free
Amount of items: 2
Items: 
Size: 9534 Color: 1
Size: 6168 Color: 0

Bin 109: 10 of cap free
Amount of items: 2
Items: 
Size: 10882 Color: 1
Size: 4820 Color: 0

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 0
Size: 1982 Color: 1

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 13738 Color: 0
Size: 1964 Color: 1

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 14046 Color: 0
Size: 1656 Color: 1

Bin 113: 12 of cap free
Amount of items: 2
Items: 
Size: 10788 Color: 0
Size: 4912 Color: 1

Bin 114: 12 of cap free
Amount of items: 2
Items: 
Size: 13852 Color: 0
Size: 1848 Color: 1

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 13896 Color: 1
Size: 1804 Color: 0

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 13974 Color: 1
Size: 1726 Color: 0

Bin 117: 14 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 1
Size: 4026 Color: 0

Bin 118: 14 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 0
Size: 1828 Color: 1

Bin 119: 14 of cap free
Amount of items: 2
Items: 
Size: 13956 Color: 1
Size: 1742 Color: 0

Bin 120: 14 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 0
Size: 1576 Color: 1
Size: 96 Color: 1

Bin 121: 15 of cap free
Amount of items: 2
Items: 
Size: 11876 Color: 0
Size: 3821 Color: 1

Bin 122: 16 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 0
Size: 4424 Color: 1

Bin 123: 20 of cap free
Amount of items: 4
Items: 
Size: 7864 Color: 1
Size: 5432 Color: 1
Size: 1564 Color: 0
Size: 832 Color: 0

Bin 124: 20 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 0
Size: 2130 Color: 1

Bin 125: 20 of cap free
Amount of items: 2
Items: 
Size: 14082 Color: 0
Size: 1610 Color: 1

Bin 126: 21 of cap free
Amount of items: 2
Items: 
Size: 10004 Color: 1
Size: 5687 Color: 0

Bin 127: 21 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 0
Size: 4251 Color: 1
Size: 824 Color: 1

Bin 128: 21 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 0
Size: 1997 Color: 1

Bin 129: 22 of cap free
Amount of items: 2
Items: 
Size: 14062 Color: 1
Size: 1628 Color: 0

Bin 130: 23 of cap free
Amount of items: 3
Items: 
Size: 10363 Color: 0
Size: 5150 Color: 1
Size: 176 Color: 1

Bin 131: 23 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 0
Size: 2888 Color: 1

Bin 132: 23 of cap free
Amount of items: 2
Items: 
Size: 13626 Color: 1
Size: 2063 Color: 0

Bin 133: 24 of cap free
Amount of items: 2
Items: 
Size: 12884 Color: 1
Size: 2804 Color: 0

Bin 134: 24 of cap free
Amount of items: 2
Items: 
Size: 13524 Color: 1
Size: 2164 Color: 0

Bin 135: 25 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 0
Size: 6547 Color: 1
Size: 960 Color: 0

Bin 136: 25 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 1
Size: 2427 Color: 0
Size: 912 Color: 0

Bin 137: 26 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 0
Size: 2168 Color: 1
Size: 184 Color: 0

Bin 138: 27 of cap free
Amount of items: 2
Items: 
Size: 10921 Color: 1
Size: 4764 Color: 0

Bin 139: 29 of cap free
Amount of items: 2
Items: 
Size: 12041 Color: 1
Size: 3642 Color: 0

Bin 140: 30 of cap free
Amount of items: 2
Items: 
Size: 9052 Color: 1
Size: 6630 Color: 0

Bin 141: 30 of cap free
Amount of items: 3
Items: 
Size: 9630 Color: 0
Size: 5888 Color: 1
Size: 164 Color: 1

Bin 142: 31 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 0
Size: 3612 Color: 1

Bin 143: 31 of cap free
Amount of items: 2
Items: 
Size: 13149 Color: 0
Size: 2532 Color: 1

Bin 144: 32 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 1
Size: 3704 Color: 0

Bin 145: 32 of cap free
Amount of items: 2
Items: 
Size: 12868 Color: 0
Size: 2812 Color: 1

Bin 146: 33 of cap free
Amount of items: 2
Items: 
Size: 13237 Color: 0
Size: 2442 Color: 1

Bin 147: 34 of cap free
Amount of items: 2
Items: 
Size: 7902 Color: 0
Size: 7776 Color: 1

Bin 148: 35 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 0
Size: 3141 Color: 1

Bin 149: 36 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 1
Size: 1940 Color: 0

Bin 150: 37 of cap free
Amount of items: 5
Items: 
Size: 7862 Color: 1
Size: 2684 Color: 1
Size: 2661 Color: 1
Size: 1556 Color: 0
Size: 912 Color: 0

Bin 151: 37 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 0
Size: 2287 Color: 1

Bin 152: 38 of cap free
Amount of items: 2
Items: 
Size: 13438 Color: 1
Size: 2236 Color: 0

Bin 153: 38 of cap free
Amount of items: 3
Items: 
Size: 13992 Color: 0
Size: 1566 Color: 1
Size: 116 Color: 1

Bin 154: 38 of cap free
Amount of items: 2
Items: 
Size: 14028 Color: 0
Size: 1646 Color: 1

Bin 155: 39 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 1
Size: 2117 Color: 0

Bin 156: 40 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 0
Size: 1596 Color: 1

Bin 157: 41 of cap free
Amount of items: 2
Items: 
Size: 11919 Color: 0
Size: 3752 Color: 1

Bin 158: 41 of cap free
Amount of items: 2
Items: 
Size: 12610 Color: 1
Size: 3061 Color: 0

Bin 159: 42 of cap free
Amount of items: 3
Items: 
Size: 8278 Color: 0
Size: 6832 Color: 1
Size: 560 Color: 0

Bin 160: 42 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 0
Size: 2028 Color: 1

Bin 161: 44 of cap free
Amount of items: 2
Items: 
Size: 12134 Color: 1
Size: 3534 Color: 0

Bin 162: 47 of cap free
Amount of items: 2
Items: 
Size: 10379 Color: 1
Size: 5286 Color: 0

Bin 163: 48 of cap free
Amount of items: 2
Items: 
Size: 11416 Color: 1
Size: 4248 Color: 0

Bin 164: 50 of cap free
Amount of items: 2
Items: 
Size: 8312 Color: 1
Size: 7350 Color: 0

Bin 165: 52 of cap free
Amount of items: 2
Items: 
Size: 8518 Color: 1
Size: 7142 Color: 0

Bin 166: 53 of cap free
Amount of items: 2
Items: 
Size: 11145 Color: 1
Size: 4514 Color: 0

Bin 167: 54 of cap free
Amount of items: 2
Items: 
Size: 12424 Color: 1
Size: 3234 Color: 0

Bin 168: 54 of cap free
Amount of items: 2
Items: 
Size: 12676 Color: 1
Size: 2982 Color: 0

Bin 169: 58 of cap free
Amount of items: 2
Items: 
Size: 9370 Color: 0
Size: 6284 Color: 1

Bin 170: 58 of cap free
Amount of items: 2
Items: 
Size: 13173 Color: 1
Size: 2481 Color: 0

Bin 171: 59 of cap free
Amount of items: 2
Items: 
Size: 9932 Color: 1
Size: 5721 Color: 0

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 13128 Color: 0
Size: 2520 Color: 1

Bin 173: 65 of cap free
Amount of items: 2
Items: 
Size: 12519 Color: 0
Size: 3128 Color: 1

Bin 174: 67 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 0
Size: 3461 Color: 1

Bin 175: 68 of cap free
Amount of items: 2
Items: 
Size: 9646 Color: 0
Size: 5998 Color: 1

Bin 176: 71 of cap free
Amount of items: 2
Items: 
Size: 11834 Color: 0
Size: 3807 Color: 1

Bin 177: 71 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 1
Size: 2967 Color: 0

Bin 178: 78 of cap free
Amount of items: 2
Items: 
Size: 13898 Color: 0
Size: 1736 Color: 1

Bin 179: 83 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 0
Size: 3037 Color: 1

Bin 180: 84 of cap free
Amount of items: 2
Items: 
Size: 13834 Color: 1
Size: 1794 Color: 0

Bin 181: 93 of cap free
Amount of items: 2
Items: 
Size: 12969 Color: 1
Size: 2650 Color: 0

Bin 182: 94 of cap free
Amount of items: 2
Items: 
Size: 12398 Color: 0
Size: 3220 Color: 1

Bin 183: 94 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 0
Size: 2262 Color: 1

Bin 184: 100 of cap free
Amount of items: 2
Items: 
Size: 11394 Color: 1
Size: 4218 Color: 0

Bin 185: 109 of cap free
Amount of items: 2
Items: 
Size: 12150 Color: 0
Size: 3453 Color: 1

Bin 186: 110 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 1
Size: 1770 Color: 0

Bin 187: 116 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 0
Size: 2762 Color: 1

Bin 188: 117 of cap free
Amount of items: 2
Items: 
Size: 13158 Color: 1
Size: 2437 Color: 0

Bin 189: 122 of cap free
Amount of items: 2
Items: 
Size: 12386 Color: 0
Size: 3204 Color: 1

Bin 190: 124 of cap free
Amount of items: 2
Items: 
Size: 11129 Color: 0
Size: 4459 Color: 1

Bin 191: 126 of cap free
Amount of items: 35
Items: 
Size: 598 Color: 1
Size: 592 Color: 1
Size: 576 Color: 1
Size: 552 Color: 1
Size: 516 Color: 0
Size: 504 Color: 0
Size: 504 Color: 0
Size: 496 Color: 0
Size: 488 Color: 0
Size: 480 Color: 1
Size: 476 Color: 0
Size: 472 Color: 1
Size: 472 Color: 0
Size: 464 Color: 1
Size: 458 Color: 0
Size: 456 Color: 0
Size: 456 Color: 0
Size: 432 Color: 1
Size: 422 Color: 1
Size: 418 Color: 0
Size: 416 Color: 1
Size: 408 Color: 1
Size: 400 Color: 1
Size: 400 Color: 1
Size: 392 Color: 0
Size: 392 Color: 0
Size: 384 Color: 1
Size: 384 Color: 1
Size: 380 Color: 1
Size: 376 Color: 1
Size: 376 Color: 0
Size: 374 Color: 0
Size: 368 Color: 0
Size: 360 Color: 0
Size: 344 Color: 1

Bin 192: 127 of cap free
Amount of items: 2
Items: 
Size: 12413 Color: 1
Size: 3172 Color: 0

Bin 193: 128 of cap free
Amount of items: 2
Items: 
Size: 9036 Color: 1
Size: 6548 Color: 0

Bin 194: 144 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 0
Size: 5160 Color: 1

Bin 195: 148 of cap free
Amount of items: 2
Items: 
Size: 12962 Color: 1
Size: 2602 Color: 0

Bin 196: 160 of cap free
Amount of items: 2
Items: 
Size: 9020 Color: 1
Size: 6532 Color: 0

Bin 197: 168 of cap free
Amount of items: 2
Items: 
Size: 7894 Color: 0
Size: 7650 Color: 1

Bin 198: 169 of cap free
Amount of items: 7
Items: 
Size: 7859 Color: 0
Size: 1308 Color: 1
Size: 1308 Color: 0
Size: 1304 Color: 0
Size: 1304 Color: 0
Size: 1248 Color: 1
Size: 1212 Color: 1

Bin 199: 10704 of cap free
Amount of items: 17
Items: 
Size: 360 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 320 Color: 0
Size: 312 Color: 1
Size: 312 Color: 0
Size: 304 Color: 1
Size: 304 Color: 0
Size: 300 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1
Size: 280 Color: 0
Size: 256 Color: 1
Size: 212 Color: 1
Size: 208 Color: 1
Size: 208 Color: 1

Total size: 3110976
Total free space: 15712

