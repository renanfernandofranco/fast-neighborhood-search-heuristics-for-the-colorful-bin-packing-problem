Capicity Bin: 7520
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 0
Size: 3132 Color: 9
Size: 304 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4534 Color: 15
Size: 2490 Color: 18
Size: 496 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4538 Color: 13
Size: 2486 Color: 12
Size: 496 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4918 Color: 3
Size: 2170 Color: 1
Size: 432 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5057 Color: 0
Size: 2053 Color: 0
Size: 410 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5020 Color: 15
Size: 2344 Color: 5
Size: 156 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 16
Size: 1466 Color: 10
Size: 292 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 5
Size: 1318 Color: 16
Size: 416 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 10
Size: 1121 Color: 17
Size: 600 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5803 Color: 8
Size: 1429 Color: 6
Size: 288 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 4
Size: 1446 Color: 14
Size: 222 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 10
Size: 1290 Color: 1
Size: 140 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6098 Color: 13
Size: 902 Color: 2
Size: 520 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 5
Size: 1204 Color: 19
Size: 200 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 10
Size: 873 Color: 4
Size: 472 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 7
Size: 1117 Color: 5
Size: 222 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 14
Size: 891 Color: 19
Size: 416 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6319 Color: 6
Size: 881 Color: 12
Size: 320 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 18
Size: 892 Color: 17
Size: 286 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6430 Color: 14
Size: 626 Color: 12
Size: 464 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6445 Color: 16
Size: 901 Color: 17
Size: 174 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 16
Size: 572 Color: 12
Size: 496 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6558 Color: 0
Size: 870 Color: 6
Size: 92 Color: 7

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 12
Size: 813 Color: 11
Size: 138 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6576 Color: 12
Size: 624 Color: 5
Size: 320 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 2
Size: 723 Color: 16
Size: 176 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6630 Color: 19
Size: 710 Color: 17
Size: 180 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6683 Color: 5
Size: 677 Color: 12
Size: 160 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 14
Size: 699 Color: 16
Size: 168 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 10
Size: 669 Color: 9
Size: 196 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 5
Size: 671 Color: 18
Size: 162 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 12
Size: 822 Color: 9
Size: 36 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 8
Size: 540 Color: 19
Size: 286 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 6
Size: 608 Color: 4
Size: 212 Color: 5

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 4
Size: 496 Color: 13
Size: 292 Color: 17

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4150 Color: 7
Size: 3133 Color: 2
Size: 236 Color: 0

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4703 Color: 13
Size: 2100 Color: 0
Size: 716 Color: 3

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 17
Size: 2351 Color: 14
Size: 164 Color: 5

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 0
Size: 1204 Color: 16
Size: 964 Color: 12

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5324 Color: 18
Size: 2057 Color: 15
Size: 138 Color: 5

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 3
Size: 1435 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6177 Color: 1
Size: 718 Color: 16
Size: 624 Color: 14

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6353 Color: 17
Size: 1166 Color: 11

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 7
Size: 812 Color: 14
Size: 272 Color: 19

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 9
Size: 897 Color: 11
Size: 96 Color: 14

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6533 Color: 8
Size: 742 Color: 5
Size: 244 Color: 11

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6548 Color: 4
Size: 971 Color: 9

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 11
Size: 793 Color: 18

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6739 Color: 10
Size: 644 Color: 2
Size: 136 Color: 5

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6747 Color: 3
Size: 740 Color: 0
Size: 32 Color: 16

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 19
Size: 3288 Color: 4
Size: 240 Color: 17

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 4644 Color: 11
Size: 2874 Color: 19

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5188 Color: 6
Size: 2166 Color: 3
Size: 164 Color: 16

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6122 Color: 14
Size: 1396 Color: 10

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 6350 Color: 11
Size: 792 Color: 5
Size: 376 Color: 17

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6545 Color: 2
Size: 973 Color: 8

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6636 Color: 7
Size: 882 Color: 12

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 5242 Color: 6
Size: 2051 Color: 9
Size: 224 Color: 5

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 5353 Color: 2
Size: 2076 Color: 5
Size: 88 Color: 13

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 1
Size: 1284 Color: 12
Size: 260 Color: 16

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 2
Size: 1365 Color: 12
Size: 178 Color: 5

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6043 Color: 8
Size: 1186 Color: 5
Size: 288 Color: 11

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 13
Size: 1093 Color: 16
Size: 174 Color: 5

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 5
Size: 568 Color: 14
Size: 472 Color: 9

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 18
Size: 1044 Color: 11

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 5398 Color: 18
Size: 1902 Color: 14
Size: 216 Color: 0

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 5506 Color: 16
Size: 2010 Color: 3

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 18
Size: 1770 Color: 6
Size: 208 Color: 11

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 10
Size: 1654 Color: 1
Size: 136 Color: 18

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 4
Size: 1051 Color: 9

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6534 Color: 6
Size: 982 Color: 11

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 6039 Color: 3
Size: 1420 Color: 19
Size: 56 Color: 14

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6047 Color: 6
Size: 1468 Color: 16

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 7
Size: 910 Color: 0

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 1
Size: 2084 Color: 14

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 3
Size: 1748 Color: 12

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 6670 Color: 14
Size: 842 Color: 7

Bin 78: 9 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 4
Size: 1235 Color: 18

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 6590 Color: 8
Size: 920 Color: 6

Bin 80: 11 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 16
Size: 3131 Color: 1
Size: 470 Color: 10

Bin 81: 11 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 14
Size: 2349 Color: 17
Size: 124 Color: 0

Bin 82: 12 of cap free
Amount of items: 3
Items: 
Size: 5807 Color: 1
Size: 1097 Color: 18
Size: 604 Color: 10

Bin 83: 12 of cap free
Amount of items: 2
Items: 
Size: 6242 Color: 1
Size: 1266 Color: 13

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 6364 Color: 18
Size: 1144 Color: 12

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 6750 Color: 12
Size: 758 Color: 0

Bin 86: 13 of cap free
Amount of items: 2
Items: 
Size: 6076 Color: 19
Size: 1431 Color: 10

Bin 87: 13 of cap free
Amount of items: 2
Items: 
Size: 6441 Color: 18
Size: 1066 Color: 11

Bin 88: 13 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 0
Size: 751 Color: 11

Bin 89: 14 of cap free
Amount of items: 2
Items: 
Size: 6719 Color: 7
Size: 787 Color: 10

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 2
Size: 1524 Color: 6

Bin 91: 18 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 3
Size: 1682 Color: 1

Bin 92: 18 of cap free
Amount of items: 2
Items: 
Size: 6462 Color: 5
Size: 1040 Color: 4

Bin 93: 19 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 19
Size: 2870 Color: 1
Size: 360 Color: 19

Bin 94: 19 of cap free
Amount of items: 3
Items: 
Size: 5238 Color: 14
Size: 1603 Color: 6
Size: 660 Color: 17

Bin 95: 19 of cap free
Amount of items: 2
Items: 
Size: 5692 Color: 11
Size: 1809 Color: 6

Bin 96: 20 of cap free
Amount of items: 6
Items: 
Size: 3761 Color: 2
Size: 1183 Color: 12
Size: 868 Color: 5
Size: 690 Color: 7
Size: 588 Color: 14
Size: 410 Color: 4

Bin 97: 20 of cap free
Amount of items: 2
Items: 
Size: 6209 Color: 13
Size: 1291 Color: 11

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 6438 Color: 2
Size: 1062 Color: 12

Bin 99: 21 of cap free
Amount of items: 16
Items: 
Size: 684 Color: 11
Size: 651 Color: 12
Size: 642 Color: 15
Size: 624 Color: 6
Size: 540 Color: 13
Size: 470 Color: 16
Size: 468 Color: 10
Size: 432 Color: 10
Size: 408 Color: 19
Size: 408 Color: 9
Size: 408 Color: 5
Size: 388 Color: 4
Size: 384 Color: 2
Size: 352 Color: 1
Size: 344 Color: 14
Size: 296 Color: 11

Bin 100: 21 of cap free
Amount of items: 2
Items: 
Size: 4699 Color: 14
Size: 2800 Color: 15

Bin 101: 21 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 16
Size: 1231 Color: 12

Bin 102: 22 of cap free
Amount of items: 3
Items: 
Size: 5595 Color: 5
Size: 1807 Color: 14
Size: 96 Color: 10

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 5942 Color: 0
Size: 1556 Color: 2

Bin 104: 22 of cap free
Amount of items: 2
Items: 
Size: 6024 Color: 10
Size: 1474 Color: 13

Bin 105: 22 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 8
Size: 830 Color: 9

Bin 106: 24 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 6
Size: 2604 Color: 12
Size: 180 Color: 19

Bin 107: 24 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 0
Size: 1836 Color: 2

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 6172 Color: 13
Size: 1324 Color: 10

Bin 109: 25 of cap free
Amount of items: 2
Items: 
Size: 6715 Color: 2
Size: 780 Color: 16

Bin 110: 30 of cap free
Amount of items: 2
Items: 
Size: 6261 Color: 6
Size: 1229 Color: 4

Bin 111: 31 of cap free
Amount of items: 2
Items: 
Size: 6357 Color: 7
Size: 1132 Color: 9

Bin 112: 33 of cap free
Amount of items: 3
Items: 
Size: 5754 Color: 2
Size: 1605 Color: 5
Size: 128 Color: 14

Bin 113: 35 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 2
Size: 1001 Color: 15

Bin 114: 38 of cap free
Amount of items: 2
Items: 
Size: 6577 Color: 1
Size: 905 Color: 0

Bin 115: 39 of cap free
Amount of items: 2
Items: 
Size: 6205 Color: 12
Size: 1276 Color: 11

Bin 116: 42 of cap free
Amount of items: 2
Items: 
Size: 3766 Color: 8
Size: 3712 Color: 12

Bin 117: 45 of cap free
Amount of items: 3
Items: 
Size: 3886 Color: 7
Size: 2868 Color: 18
Size: 721 Color: 7

Bin 118: 45 of cap free
Amount of items: 2
Items: 
Size: 5597 Color: 15
Size: 1878 Color: 18

Bin 119: 46 of cap free
Amount of items: 33
Items: 
Size: 360 Color: 17
Size: 360 Color: 7
Size: 360 Color: 0
Size: 332 Color: 5
Size: 328 Color: 18
Size: 304 Color: 1
Size: 284 Color: 15
Size: 280 Color: 0
Size: 272 Color: 1
Size: 256 Color: 15
Size: 246 Color: 9
Size: 246 Color: 1
Size: 232 Color: 18
Size: 232 Color: 6
Size: 218 Color: 14
Size: 218 Color: 14
Size: 208 Color: 4
Size: 200 Color: 14
Size: 194 Color: 2
Size: 192 Color: 18
Size: 192 Color: 1
Size: 192 Color: 0
Size: 180 Color: 15
Size: 180 Color: 14
Size: 178 Color: 4
Size: 176 Color: 8
Size: 172 Color: 11
Size: 164 Color: 8
Size: 158 Color: 16
Size: 152 Color: 5
Size: 144 Color: 5
Size: 136 Color: 3
Size: 128 Color: 2

Bin 120: 46 of cap free
Amount of items: 2
Items: 
Size: 4532 Color: 19
Size: 2942 Color: 12

Bin 121: 47 of cap free
Amount of items: 2
Items: 
Size: 5975 Color: 15
Size: 1498 Color: 14

Bin 122: 48 of cap free
Amount of items: 3
Items: 
Size: 3765 Color: 4
Size: 3012 Color: 17
Size: 695 Color: 8

Bin 123: 48 of cap free
Amount of items: 3
Items: 
Size: 5053 Color: 11
Size: 2355 Color: 6
Size: 64 Color: 16

Bin 124: 55 of cap free
Amount of items: 2
Items: 
Size: 5061 Color: 2
Size: 2404 Color: 19

Bin 125: 58 of cap free
Amount of items: 2
Items: 
Size: 5514 Color: 18
Size: 1948 Color: 2

Bin 126: 71 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 10
Size: 2707 Color: 18
Size: 978 Color: 0

Bin 127: 76 of cap free
Amount of items: 2
Items: 
Size: 4078 Color: 19
Size: 3366 Color: 14

Bin 128: 78 of cap free
Amount of items: 5
Items: 
Size: 3762 Color: 12
Size: 1432 Color: 19
Size: 1121 Color: 1
Size: 871 Color: 18
Size: 256 Color: 2

Bin 129: 94 of cap free
Amount of items: 2
Items: 
Size: 4396 Color: 6
Size: 3030 Color: 10

Bin 130: 106 of cap free
Amount of items: 2
Items: 
Size: 4922 Color: 13
Size: 2492 Color: 5

Bin 131: 113 of cap free
Amount of items: 2
Items: 
Size: 4273 Color: 11
Size: 3134 Color: 0

Bin 132: 116 of cap free
Amount of items: 2
Items: 
Size: 4695 Color: 16
Size: 2709 Color: 17

Bin 133: 5504 of cap free
Amount of items: 15
Items: 
Size: 148 Color: 19
Size: 148 Color: 13
Size: 144 Color: 11
Size: 144 Color: 7
Size: 140 Color: 5
Size: 136 Color: 10
Size: 136 Color: 9
Size: 134 Color: 18
Size: 132 Color: 6
Size: 130 Color: 15
Size: 128 Color: 13
Size: 128 Color: 8
Size: 128 Color: 3
Size: 120 Color: 6
Size: 120 Color: 2

Total size: 992640
Total free space: 7520

