Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 12
Size: 828 Color: 15
Size: 160 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 4
Size: 641 Color: 9
Size: 102 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 3
Size: 622 Color: 18
Size: 120 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 17
Size: 588 Color: 11
Size: 16 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 18
Size: 300 Color: 12
Size: 272 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 0
Size: 484 Color: 5
Size: 56 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 13
Size: 377 Color: 4
Size: 128 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 13
Size: 363 Color: 15
Size: 72 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 9
Size: 380 Color: 10
Size: 40 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 6
Size: 364 Color: 16
Size: 24 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 11
Size: 280 Color: 0
Size: 126 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 19
Size: 244 Color: 11
Size: 126 Color: 7

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 1
Size: 168 Color: 17
Size: 148 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 0
Size: 258 Color: 12
Size: 32 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 17
Size: 262 Color: 1
Size: 8 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 17
Size: 174 Color: 0
Size: 94 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 10
Size: 176 Color: 6
Size: 88 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 7
Size: 244 Color: 19
Size: 8 Color: 16

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 2
Size: 850 Color: 15
Size: 200 Color: 19

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 9
Size: 586 Color: 11
Size: 16 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 6
Size: 422 Color: 11
Size: 108 Color: 19

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 19
Size: 324 Color: 8
Size: 80 Color: 13

Bin 23: 2 of cap free
Amount of items: 4
Items: 
Size: 1567 Color: 8
Size: 621 Color: 1
Size: 200 Color: 17
Size: 66 Color: 12

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 16
Size: 503 Color: 18
Size: 144 Color: 4

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 3
Size: 467 Color: 6
Size: 150 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 3
Size: 580 Color: 19
Size: 32 Color: 18

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 1
Size: 452 Color: 12
Size: 88 Color: 19

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 9
Size: 378 Color: 14
Size: 56 Color: 5

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 10
Size: 292 Color: 2
Size: 16 Color: 15

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 2
Size: 282 Color: 5

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 19
Size: 232 Color: 18
Size: 20 Color: 8

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 1430 Color: 0
Size: 1023 Color: 6

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 6
Size: 863 Color: 2
Size: 54 Color: 6

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 5
Size: 862 Color: 11
Size: 40 Color: 12

Bin 35: 3 of cap free
Amount of items: 4
Items: 
Size: 1673 Color: 2
Size: 568 Color: 18
Size: 168 Color: 6
Size: 44 Color: 14

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 9
Size: 637 Color: 10
Size: 100 Color: 2

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1764 Color: 5
Size: 689 Color: 6

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 19
Size: 421 Color: 4
Size: 226 Color: 15

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 15
Size: 339 Color: 6
Size: 140 Color: 4

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 7
Size: 1018 Color: 8
Size: 204 Color: 19

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 17
Size: 976 Color: 16
Size: 112 Color: 3

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 0
Size: 756 Color: 19

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1935 Color: 16
Size: 517 Color: 0

Bin 44: 5 of cap free
Amount of items: 15
Items: 
Size: 620 Color: 10
Size: 439 Color: 17
Size: 212 Color: 1
Size: 204 Color: 19
Size: 120 Color: 18
Size: 120 Color: 3
Size: 102 Color: 8
Size: 88 Color: 16
Size: 88 Color: 10
Size: 88 Color: 9
Size: 86 Color: 16
Size: 84 Color: 11
Size: 72 Color: 17
Size: 64 Color: 5
Size: 64 Color: 2

Bin 45: 5 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 6
Size: 1020 Color: 3
Size: 88 Color: 1
Size: 64 Color: 2
Size: 48 Color: 11

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 2006 Color: 3
Size: 444 Color: 18

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 2108 Color: 16
Size: 342 Color: 4

Bin 48: 8 of cap free
Amount of items: 4
Items: 
Size: 1644 Color: 11
Size: 512 Color: 12
Size: 204 Color: 17
Size: 88 Color: 5

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 16
Size: 755 Color: 8

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 1841 Color: 11
Size: 607 Color: 16

Bin 51: 8 of cap free
Amount of items: 2
Items: 
Size: 2005 Color: 5
Size: 443 Color: 2

Bin 52: 8 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 15
Size: 330 Color: 10

Bin 53: 9 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 3
Size: 684 Color: 3
Size: 74 Color: 2

Bin 54: 10 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 15
Size: 514 Color: 10

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1931 Color: 8
Size: 513 Color: 15

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1421 Color: 5
Size: 1022 Color: 12

Bin 57: 13 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 11
Size: 714 Color: 12

Bin 58: 14 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 17
Size: 916 Color: 8
Size: 100 Color: 19

Bin 59: 14 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 15
Size: 840 Color: 19

Bin 60: 15 of cap free
Amount of items: 2
Items: 
Size: 1900 Color: 10
Size: 541 Color: 12

Bin 61: 18 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 3
Size: 468 Color: 9
Size: 384 Color: 6

Bin 62: 23 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 11
Size: 741 Color: 7
Size: 454 Color: 16

Bin 63: 23 of cap free
Amount of items: 2
Items: 
Size: 1556 Color: 14
Size: 877 Color: 7

Bin 64: 27 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 17
Size: 1021 Color: 12
Size: 172 Color: 4

Bin 65: 33 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 10
Size: 858 Color: 6
Size: 214 Color: 15
Size: 122 Color: 15

Bin 66: 2108 of cap free
Amount of items: 7
Items: 
Size: 72 Color: 19
Size: 60 Color: 15
Size: 48 Color: 2
Size: 48 Color: 1
Size: 40 Color: 8
Size: 40 Color: 7
Size: 40 Color: 5

Total size: 159640
Total free space: 2456

