Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 152
Size: 696 Color: 127
Size: 54 Color: 20

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 158
Size: 614 Color: 121
Size: 36 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 164
Size: 482 Color: 112
Size: 52 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 165
Size: 313 Color: 89
Size: 208 Color: 74

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 166
Size: 431 Color: 107
Size: 86 Color: 43

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 167
Size: 390 Color: 102
Size: 124 Color: 56

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 171
Size: 385 Color: 100
Size: 76 Color: 38

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 172
Size: 316 Color: 91
Size: 144 Color: 62

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 173
Size: 382 Color: 99
Size: 76 Color: 36

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1593 Color: 174
Size: 435 Color: 108
Size: 20 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 177
Size: 351 Color: 96
Size: 68 Color: 30

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 178
Size: 342 Color: 95
Size: 68 Color: 31

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 181
Size: 275 Color: 85
Size: 100 Color: 48

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 182
Size: 338 Color: 94
Size: 36 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 183
Size: 258 Color: 80
Size: 108 Color: 49

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 185
Size: 273 Color: 84
Size: 64 Color: 28

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 186
Size: 281 Color: 87
Size: 54 Color: 21

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 189
Size: 262 Color: 81
Size: 48 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 190
Size: 246 Color: 79
Size: 60 Color: 26

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 192
Size: 198 Color: 72
Size: 96 Color: 45

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 193
Size: 242 Color: 78
Size: 48 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 194
Size: 241 Color: 77
Size: 46 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 195
Size: 265 Color: 83
Size: 2 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 196
Size: 213 Color: 75
Size: 42 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 197
Size: 168 Color: 67
Size: 70 Color: 32

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 198
Size: 186 Color: 69
Size: 48 Color: 16

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 199
Size: 190 Color: 70
Size: 40 Color: 11

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 200
Size: 170 Color: 68
Size: 56 Color: 22

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 201
Size: 160 Color: 65
Size: 62 Color: 27

Bin 30: 1 of cap free
Amount of items: 7
Items: 
Size: 1025 Color: 138
Size: 281 Color: 86
Size: 263 Color: 82
Size: 202 Color: 73
Size: 132 Color: 60
Size: 72 Color: 34
Size: 72 Color: 33

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 142
Size: 903 Color: 137
Size: 58 Color: 24

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 155
Size: 626 Color: 122
Size: 36 Color: 10

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 1416 Color: 159
Size: 631 Color: 123

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 1659 Color: 180
Size: 388 Color: 101

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 1733 Color: 188
Size: 314 Color: 90

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 169
Size: 472 Color: 111

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 187
Size: 325 Color: 93

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1743 Color: 191
Size: 303 Color: 88

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 149
Size: 706 Color: 128
Size: 56 Color: 23

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 168
Size: 510 Color: 115

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1582 Color: 170
Size: 463 Color: 110

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1621 Color: 176
Size: 424 Color: 104

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1314 Color: 153
Size: 682 Color: 126
Size: 48 Color: 17

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 175
Size: 446 Color: 109

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1646 Color: 179
Size: 398 Color: 103

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1463 Color: 161
Size: 550 Color: 117
Size: 30 Color: 5

Bin 47: 6 of cap free
Amount of items: 5
Items: 
Size: 1027 Color: 140
Size: 430 Color: 106
Size: 429 Color: 105
Size: 96 Color: 47
Size: 60 Color: 25

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 184
Size: 357 Color: 97

Bin 49: 9 of cap free
Amount of items: 4
Items: 
Size: 1470 Color: 163
Size: 553 Color: 119
Size: 8 Color: 2
Size: 8 Color: 1

Bin 50: 12 of cap free
Amount of items: 2
Items: 
Size: 1234 Color: 147
Size: 802 Color: 132

Bin 51: 12 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 162
Size: 551 Color: 118
Size: 20 Color: 3

Bin 52: 13 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 157
Size: 609 Color: 120
Size: 36 Color: 9

Bin 53: 18 of cap free
Amount of items: 2
Items: 
Size: 1202 Color: 146
Size: 828 Color: 133

Bin 54: 20 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 151
Size: 737 Color: 131

Bin 55: 20 of cap free
Amount of items: 2
Items: 
Size: 1389 Color: 156
Size: 639 Color: 125

Bin 56: 21 of cap free
Amount of items: 2
Items: 
Size: 1173 Color: 145
Size: 854 Color: 136

Bin 57: 28 of cap free
Amount of items: 2
Items: 
Size: 1169 Color: 144
Size: 851 Color: 135

Bin 58: 28 of cap free
Amount of items: 2
Items: 
Size: 1287 Color: 150
Size: 733 Color: 130

Bin 59: 32 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 160
Size: 542 Color: 116
Size: 36 Color: 7

Bin 60: 34 of cap free
Amount of items: 2
Items: 
Size: 1165 Color: 143
Size: 849 Color: 134

Bin 61: 37 of cap free
Amount of items: 2
Items: 
Size: 1280 Color: 148
Size: 731 Color: 129

Bin 62: 41 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 141
Size: 489 Color: 114
Size: 487 Color: 113

Bin 63: 42 of cap free
Amount of items: 5
Items: 
Size: 1026 Color: 139
Size: 378 Color: 98
Size: 318 Color: 92
Size: 220 Color: 76
Size: 64 Color: 29

Bin 64: 46 of cap free
Amount of items: 3
Items: 
Size: 1319 Color: 154
Size: 635 Color: 124
Size: 48 Color: 14

Bin 65: 76 of cap free
Amount of items: 17
Items: 
Size: 194 Color: 71
Size: 168 Color: 66
Size: 146 Color: 64
Size: 146 Color: 63
Size: 140 Color: 61
Size: 126 Color: 59
Size: 126 Color: 58
Size: 126 Color: 57
Size: 120 Color: 55
Size: 120 Color: 54
Size: 88 Color: 44
Size: 84 Color: 42
Size: 84 Color: 41
Size: 76 Color: 40
Size: 76 Color: 39
Size: 76 Color: 37
Size: 76 Color: 35

Bin 66: 1506 of cap free
Amount of items: 5
Items: 
Size: 120 Color: 53
Size: 110 Color: 52
Size: 108 Color: 51
Size: 108 Color: 50
Size: 96 Color: 46

Total size: 133120
Total free space: 2048

