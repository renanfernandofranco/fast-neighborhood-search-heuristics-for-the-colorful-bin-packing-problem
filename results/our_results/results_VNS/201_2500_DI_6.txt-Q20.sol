Capicity Bin: 2020
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1011 Color: 18
Size: 831 Color: 0
Size: 142 Color: 12
Size: 36 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1013 Color: 18
Size: 841 Color: 4
Size: 166 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1155 Color: 18
Size: 721 Color: 3
Size: 144 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 12
Size: 698 Color: 7
Size: 100 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 12
Size: 586 Color: 10
Size: 48 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1403 Color: 2
Size: 453 Color: 15
Size: 164 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 15
Size: 462 Color: 19
Size: 48 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1537 Color: 16
Size: 403 Color: 14
Size: 80 Color: 19

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1585 Color: 19
Size: 363 Color: 3
Size: 72 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 15
Size: 322 Color: 11
Size: 100 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1623 Color: 9
Size: 263 Color: 1
Size: 134 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 17
Size: 301 Color: 16
Size: 58 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 18
Size: 286 Color: 19
Size: 56 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 15
Size: 269 Color: 18
Size: 52 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 1
Size: 212 Color: 8
Size: 102 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 15
Size: 222 Color: 5
Size: 52 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 19
Size: 267 Color: 18
Size: 16 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 8
Size: 142 Color: 16
Size: 120 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1772 Color: 7
Size: 164 Color: 8
Size: 84 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 17
Size: 202 Color: 15
Size: 40 Color: 7

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1186 Color: 6
Size: 833 Color: 5

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 11
Size: 666 Color: 5
Size: 36 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1362 Color: 3
Size: 589 Color: 4
Size: 68 Color: 7

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 15
Size: 451 Color: 2
Size: 168 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 7
Size: 506 Color: 18
Size: 102 Color: 16

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1466 Color: 12
Size: 509 Color: 8
Size: 44 Color: 10

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 1
Size: 329 Color: 16
Size: 20 Color: 13

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 14
Size: 837 Color: 12
Size: 156 Color: 16

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1268 Color: 19
Size: 684 Color: 7
Size: 66 Color: 12

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 8
Size: 337 Color: 15
Size: 16 Color: 9

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1731 Color: 19
Size: 287 Color: 7

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 18
Size: 224 Color: 9

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1290 Color: 15
Size: 671 Color: 0
Size: 56 Color: 8

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 15
Size: 574 Color: 10
Size: 44 Color: 13

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 19
Size: 511 Color: 15
Size: 92 Color: 11

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 19
Size: 331 Color: 6

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1705 Color: 2
Size: 312 Color: 18

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1157 Color: 0
Size: 717 Color: 7
Size: 142 Color: 2

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 18
Size: 398 Color: 12

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 16
Size: 374 Color: 5
Size: 8 Color: 6

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1786 Color: 5
Size: 230 Color: 1

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 1315 Color: 13
Size: 610 Color: 4
Size: 90 Color: 9

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 9
Size: 338 Color: 5

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1766 Color: 3
Size: 241 Color: 17
Size: 8 Color: 13

Bin 45: 6 of cap free
Amount of items: 5
Items: 
Size: 1017 Color: 0
Size: 297 Color: 10
Size: 294 Color: 18
Size: 270 Color: 2
Size: 136 Color: 0

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 1217 Color: 14
Size: 721 Color: 12
Size: 76 Color: 19

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1627 Color: 3
Size: 365 Color: 14
Size: 22 Color: 15

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1659 Color: 11
Size: 354 Color: 7

Bin 49: 8 of cap free
Amount of items: 19
Items: 
Size: 282 Color: 9
Size: 198 Color: 13
Size: 190 Color: 3
Size: 132 Color: 17
Size: 132 Color: 16
Size: 116 Color: 18
Size: 116 Color: 12
Size: 102 Color: 15
Size: 80 Color: 15
Size: 76 Color: 5
Size: 76 Color: 4
Size: 72 Color: 13
Size: 72 Color: 11
Size: 64 Color: 14
Size: 64 Color: 6
Size: 64 Color: 1
Size: 60 Color: 7
Size: 60 Color: 2
Size: 56 Color: 9

Bin 50: 9 of cap free
Amount of items: 3
Items: 
Size: 1078 Color: 12
Size: 713 Color: 2
Size: 220 Color: 18

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 1583 Color: 13
Size: 426 Color: 6

Bin 52: 12 of cap free
Amount of items: 3
Items: 
Size: 1169 Color: 3
Size: 673 Color: 15
Size: 166 Color: 13

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 9
Size: 491 Color: 16

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1165 Color: 15
Size: 842 Color: 17

Bin 55: 17 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 13
Size: 775 Color: 14
Size: 214 Color: 6

Bin 56: 17 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 2
Size: 816 Color: 9
Size: 166 Color: 7

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1161 Color: 4
Size: 841 Color: 15

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1481 Color: 7
Size: 519 Color: 5

Bin 59: 21 of cap free
Amount of items: 2
Items: 
Size: 1213 Color: 10
Size: 786 Color: 19

Bin 60: 21 of cap free
Amount of items: 2
Items: 
Size: 1535 Color: 8
Size: 464 Color: 16

Bin 61: 25 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 19
Size: 405 Color: 10
Size: 44 Color: 15

Bin 62: 26 of cap free
Amount of items: 2
Items: 
Size: 1407 Color: 4
Size: 587 Color: 18

Bin 63: 26 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 4
Size: 420 Color: 17

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1378 Color: 11
Size: 614 Color: 16

Bin 65: 28 of cap free
Amount of items: 2
Items: 
Size: 1477 Color: 14
Size: 515 Color: 6

Bin 66: 1620 of cap free
Amount of items: 8
Items: 
Size: 72 Color: 0
Size: 56 Color: 3
Size: 52 Color: 18
Size: 52 Color: 11
Size: 52 Color: 9
Size: 40 Color: 8
Size: 40 Color: 1
Size: 36 Color: 15

Total size: 131300
Total free space: 2020

