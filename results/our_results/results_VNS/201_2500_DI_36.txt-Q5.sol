Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 13
Items: 
Size: 517 Color: 0
Size: 339 Color: 0
Size: 324 Color: 2
Size: 244 Color: 2
Size: 226 Color: 1
Size: 214 Color: 1
Size: 120 Color: 4
Size: 88 Color: 4
Size: 88 Color: 4
Size: 88 Color: 0
Size: 84 Color: 2
Size: 64 Color: 4
Size: 60 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 4
Size: 1021 Color: 2
Size: 204 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 3
Size: 1018 Color: 3
Size: 200 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 0
Size: 637 Color: 0
Size: 126 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 4
Size: 689 Color: 4
Size: 54 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 3
Size: 620 Color: 0
Size: 120 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1841 Color: 2
Size: 443 Color: 4
Size: 172 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1900 Color: 0
Size: 484 Color: 1
Size: 72 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 0
Size: 468 Color: 3
Size: 72 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 1
Size: 454 Color: 0
Size: 88 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 4
Size: 439 Color: 2
Size: 66 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 0
Size: 378 Color: 1
Size: 72 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 3
Size: 363 Color: 0
Size: 88 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 4
Size: 280 Color: 0
Size: 140 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 0
Size: 380 Color: 3
Size: 8 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 4
Size: 330 Color: 3
Size: 40 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 4
Size: 292 Color: 3
Size: 56 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 4
Size: 300 Color: 3
Size: 16 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 3
Size: 262 Color: 1
Size: 48 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 4
Size: 204 Color: 1
Size: 86 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 0
Size: 212 Color: 2
Size: 56 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2186 Color: 1
Size: 148 Color: 3
Size: 122 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 2
Size: 150 Color: 2
Size: 102 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1837 Color: 0
Size: 586 Color: 1
Size: 32 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 4
Size: 503 Color: 2
Size: 100 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 1
Size: 514 Color: 4
Size: 16 Color: 4

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1931 Color: 2
Size: 364 Color: 0
Size: 160 Color: 4

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 2
Size: 384 Color: 4
Size: 20 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 1
Size: 858 Color: 2
Size: 128 Color: 2

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 0
Size: 863 Color: 3
Size: 40 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2050 Color: 0
Size: 204 Color: 4
Size: 200 Color: 1

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 272 Color: 0
Size: 64 Color: 4

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 4
Size: 282 Color: 2

Bin 34: 3 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 0
Size: 1023 Color: 1
Size: 168 Color: 4
Size: 32 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1853 Color: 0
Size: 512 Color: 2
Size: 88 Color: 1

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 4
Size: 444 Color: 3
Size: 74 Color: 0

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 0
Size: 976 Color: 1
Size: 112 Color: 2

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 0
Size: 916 Color: 2

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 0
Size: 568 Color: 2
Size: 120 Color: 1

Bin 40: 5 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 4
Size: 1020 Color: 1
Size: 102 Color: 2
Size: 100 Color: 1

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 3
Size: 862 Color: 1
Size: 168 Color: 0

Bin 42: 5 of cap free
Amount of items: 4
Items: 
Size: 1426 Color: 1
Size: 741 Color: 4
Size: 244 Color: 0
Size: 40 Color: 3

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 1
Size: 840 Color: 4
Size: 44 Color: 0

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 4
Size: 684 Color: 0
Size: 94 Color: 1

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 2
Size: 588 Color: 3
Size: 174 Color: 0

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 0
Size: 580 Color: 3
Size: 64 Color: 1

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 2021 Color: 3
Size: 422 Color: 4
Size: 8 Color: 2

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 3
Size: 452 Color: 4
Size: 24 Color: 4

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 1
Size: 258 Color: 4

Bin 50: 6 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 2
Size: 232 Color: 4
Size: 16 Color: 0

Bin 51: 7 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 2
Size: 607 Color: 4

Bin 52: 8 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 3
Size: 756 Color: 4
Size: 48 Color: 1

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1806 Color: 1
Size: 641 Color: 4

Bin 54: 11 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 4
Size: 755 Color: 0
Size: 88 Color: 1

Bin 55: 11 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 0
Size: 513 Color: 1

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 0
Size: 622 Color: 1
Size: 108 Color: 1

Bin 57: 13 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 2
Size: 621 Color: 2
Size: 126 Color: 1

Bin 58: 13 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 3
Size: 714 Color: 2

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 2020 Color: 0
Size: 421 Color: 1

Bin 60: 22 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 1
Size: 828 Color: 0
Size: 176 Color: 3

Bin 61: 23 of cap free
Amount of items: 2
Items: 
Size: 1556 Color: 3
Size: 877 Color: 2

Bin 62: 26 of cap free
Amount of items: 3
Items: 
Size: 1586 Color: 0
Size: 467 Color: 3
Size: 377 Color: 1

Bin 63: 28 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 3
Size: 850 Color: 1
Size: 342 Color: 0

Bin 64: 29 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 1
Size: 1022 Color: 2

Bin 65: 31 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 2
Size: 541 Color: 4

Bin 66: 2104 of cap free
Amount of items: 5
Items: 
Size: 144 Color: 1
Size: 80 Color: 0
Size: 48 Color: 2
Size: 40 Color: 4
Size: 40 Color: 2

Total size: 159640
Total free space: 2456

