Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5219 Color: 307
Size: 2457 Color: 245
Size: 148 Color: 21

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5698 Color: 325
Size: 1994 Color: 222
Size: 132 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5699 Color: 326
Size: 1919 Color: 221
Size: 206 Color: 51

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 331
Size: 1546 Color: 206
Size: 432 Color: 103

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5954 Color: 333
Size: 1494 Color: 203
Size: 376 Color: 92

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6186 Color: 341
Size: 972 Color: 167
Size: 666 Color: 137

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 342
Size: 1480 Color: 202
Size: 148 Color: 23

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 345
Size: 1478 Color: 201
Size: 100 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 349
Size: 1318 Color: 196
Size: 214 Color: 54

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 351
Size: 820 Color: 150
Size: 702 Color: 141

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 355
Size: 876 Color: 154
Size: 552 Color: 120

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6421 Color: 356
Size: 957 Color: 165
Size: 446 Color: 109

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 357
Size: 900 Color: 158
Size: 488 Color: 114

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6455 Color: 358
Size: 1309 Color: 195
Size: 60 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 359
Size: 1138 Color: 182
Size: 224 Color: 59

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 360
Size: 1122 Color: 180
Size: 220 Color: 58

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 361
Size: 960 Color: 166
Size: 380 Color: 94

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 362
Size: 746 Color: 145
Size: 556 Color: 122

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 366
Size: 1007 Color: 170
Size: 248 Color: 65

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 372
Size: 682 Color: 140
Size: 484 Color: 113

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 373
Size: 1056 Color: 174
Size: 108 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6706 Color: 376
Size: 934 Color: 161
Size: 184 Color: 41

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 381
Size: 676 Color: 139
Size: 376 Color: 93

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 382
Size: 878 Color: 155
Size: 166 Color: 33

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 383
Size: 869 Color: 153
Size: 172 Color: 36

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 387
Size: 935 Color: 162
Size: 64 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 390
Size: 794 Color: 148
Size: 176 Color: 39

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 392
Size: 608 Color: 127
Size: 308 Color: 79

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 398
Size: 648 Color: 130
Size: 180 Color: 40

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7006 Color: 399
Size: 616 Color: 128
Size: 202 Color: 50

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 400
Size: 556 Color: 123
Size: 248 Color: 66

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7024 Color: 401
Size: 572 Color: 126
Size: 228 Color: 60

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7026 Color: 402
Size: 544 Color: 118
Size: 254 Color: 69

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 5291 Color: 310
Size: 2532 Color: 250

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5767 Color: 329
Size: 1900 Color: 219
Size: 156 Color: 29

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 332
Size: 1771 Color: 215
Size: 200 Color: 48

Bin 37: 1 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 339
Size: 1715 Color: 213

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6111 Color: 340
Size: 1644 Color: 210
Size: 68 Color: 3

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 344
Size: 1429 Color: 197
Size: 160 Color: 30

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 380
Size: 1065 Color: 176

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 300
Size: 2075 Color: 224
Size: 624 Color: 129

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 5195 Color: 304
Size: 2475 Color: 246
Size: 152 Color: 25

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 5523 Color: 320
Size: 2211 Color: 235
Size: 88 Color: 4

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 336
Size: 1650 Color: 211
Size: 176 Color: 38

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 6206 Color: 343
Size: 1284 Color: 193
Size: 332 Color: 83

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 6248 Color: 346
Size: 1574 Color: 209

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 6529 Color: 363
Size: 1293 Color: 194

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 369
Size: 1218 Color: 187

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6741 Color: 378
Size: 1081 Color: 178

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 384
Size: 1037 Color: 173

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 396
Size: 857 Color: 152

Bin 52: 4 of cap free
Amount of items: 7
Items: 
Size: 3914 Color: 274
Size: 900 Color: 159
Size: 810 Color: 149
Size: 764 Color: 147
Size: 762 Color: 146
Size: 418 Color: 100
Size: 252 Color: 67

Bin 53: 4 of cap free
Amount of items: 5
Items: 
Size: 3925 Color: 279
Size: 3253 Color: 265
Size: 216 Color: 57
Size: 216 Color: 55
Size: 210 Color: 53

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 4966 Color: 298
Size: 2854 Color: 258

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 5243 Color: 309
Size: 2433 Color: 243
Size: 144 Color: 19

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 5626 Color: 324
Size: 2194 Color: 233

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 6273 Color: 348
Size: 1547 Color: 207

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 6557 Color: 365
Size: 1263 Color: 189

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 394
Size: 890 Color: 157

Bin 60: 5 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 302
Size: 1906 Color: 220
Size: 742 Color: 144

Bin 61: 5 of cap free
Amount of items: 3
Items: 
Size: 5235 Color: 308
Size: 2438 Color: 244
Size: 146 Color: 20

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 5823 Color: 330
Size: 1996 Color: 223

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 6309 Color: 352
Size: 1510 Color: 204

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 353
Size: 1471 Color: 200

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6874 Color: 391
Size: 945 Color: 164

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 397
Size: 833 Color: 151

Bin 67: 6 of cap free
Amount of items: 5
Items: 
Size: 3917 Color: 276
Size: 1669 Color: 212
Size: 1436 Color: 198
Size: 564 Color: 125
Size: 232 Color: 62

Bin 68: 6 of cap free
Amount of items: 3
Items: 
Size: 5335 Color: 316
Size: 2363 Color: 240
Size: 120 Color: 10

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 6366 Color: 354
Size: 1452 Color: 199

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 6548 Color: 364
Size: 1270 Color: 191

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 6677 Color: 374
Size: 1141 Color: 183

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 6797 Color: 386
Size: 1021 Color: 172

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 389
Size: 974 Color: 168

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 395
Size: 880 Color: 156

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 4950 Color: 297
Size: 2867 Color: 259

Bin 76: 7 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 321
Size: 2231 Color: 236
Size: 48 Color: 0

Bin 77: 7 of cap free
Amount of items: 2
Items: 
Size: 6255 Color: 347
Size: 1562 Color: 208

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 6293 Color: 350
Size: 1524 Color: 205

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 6581 Color: 367
Size: 1236 Color: 188

Bin 80: 7 of cap free
Amount of items: 2
Items: 
Size: 6693 Color: 375
Size: 1124 Color: 181

Bin 81: 7 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 393
Size: 903 Color: 160

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 328
Size: 2084 Color: 226

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 6748 Color: 379
Size: 1068 Color: 177

Bin 84: 9 of cap free
Amount of items: 2
Items: 
Size: 6829 Color: 388
Size: 986 Color: 169

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 6794 Color: 385
Size: 1020 Color: 171

Bin 86: 11 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 294
Size: 2798 Color: 256
Size: 160 Color: 31

Bin 87: 11 of cap free
Amount of items: 3
Items: 
Size: 4879 Color: 295
Size: 2782 Color: 254
Size: 152 Color: 28

Bin 88: 11 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 303
Size: 2487 Color: 247
Size: 152 Color: 26

Bin 89: 11 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 370
Size: 1196 Color: 185

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 371
Size: 1171 Color: 184

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 6727 Color: 377
Size: 1086 Color: 179

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 5299 Color: 311
Size: 2513 Color: 249

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 5619 Color: 323
Size: 2191 Color: 232

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 5970 Color: 335
Size: 1839 Color: 218

Bin 95: 15 of cap free
Amount of items: 2
Items: 
Size: 6601 Color: 368
Size: 1208 Color: 186

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 6034 Color: 337
Size: 1774 Color: 216

Bin 97: 17 of cap free
Amount of items: 2
Items: 
Size: 6059 Color: 338
Size: 1748 Color: 214

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 4902 Color: 296
Size: 2904 Color: 260

Bin 99: 19 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 301
Size: 2506 Color: 248
Size: 152 Color: 27

Bin 100: 20 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 281
Size: 3440 Color: 271
Size: 196 Color: 47

Bin 101: 21 of cap free
Amount of items: 2
Items: 
Size: 5969 Color: 334
Size: 1834 Color: 217

Bin 102: 25 of cap free
Amount of items: 2
Items: 
Size: 5700 Color: 327
Size: 2099 Color: 227

Bin 103: 28 of cap free
Amount of items: 3
Items: 
Size: 4841 Color: 293
Size: 2795 Color: 255
Size: 160 Color: 32

Bin 104: 28 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 306
Size: 2433 Color: 242
Size: 148 Color: 22

Bin 105: 28 of cap free
Amount of items: 3
Items: 
Size: 5482 Color: 319
Size: 2210 Color: 234
Size: 104 Color: 6

Bin 106: 30 of cap free
Amount of items: 3
Items: 
Size: 4494 Color: 290
Size: 3124 Color: 263
Size: 176 Color: 37

Bin 107: 30 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 292
Size: 2838 Color: 257
Size: 168 Color: 34

Bin 108: 30 of cap free
Amount of items: 2
Items: 
Size: 5548 Color: 322
Size: 2246 Color: 237

Bin 109: 32 of cap free
Amount of items: 2
Items: 
Size: 5068 Color: 299
Size: 2724 Color: 251

Bin 110: 33 of cap free
Amount of items: 3
Items: 
Size: 4487 Color: 289
Size: 3116 Color: 262
Size: 188 Color: 42

Bin 111: 34 of cap free
Amount of items: 2
Items: 
Size: 3918 Color: 277
Size: 3872 Color: 272

Bin 112: 34 of cap free
Amount of items: 3
Items: 
Size: 5351 Color: 317
Size: 2327 Color: 239
Size: 112 Color: 9

Bin 113: 46 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 284
Size: 2079 Color: 225
Size: 1277 Color: 192

Bin 114: 50 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 291
Size: 3048 Color: 261
Size: 170 Color: 35

Bin 115: 59 of cap free
Amount of items: 4
Items: 
Size: 5330 Color: 313
Size: 2171 Color: 229
Size: 136 Color: 16
Size: 128 Color: 14

Bin 116: 62 of cap free
Amount of items: 4
Items: 
Size: 5331 Color: 314
Size: 2175 Color: 230
Size: 128 Color: 13
Size: 128 Color: 12

Bin 117: 66 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 305
Size: 2398 Color: 241
Size: 148 Color: 24

Bin 118: 67 of cap free
Amount of items: 4
Items: 
Size: 4092 Color: 280
Size: 3257 Color: 266
Size: 208 Color: 52
Size: 200 Color: 49

Bin 119: 72 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 315
Size: 2300 Color: 238
Size: 120 Color: 11

Bin 120: 86 of cap free
Amount of items: 4
Items: 
Size: 5307 Color: 312
Size: 2159 Color: 228
Size: 136 Color: 18
Size: 136 Color: 17

Bin 121: 91 of cap free
Amount of items: 2
Items: 
Size: 4471 Color: 286
Size: 3262 Color: 270

Bin 122: 93 of cap free
Amount of items: 2
Items: 
Size: 4470 Color: 285
Size: 3261 Color: 269

Bin 123: 98 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 318
Size: 2180 Color: 231
Size: 112 Color: 8

Bin 124: 146 of cap free
Amount of items: 2
Items: 
Size: 4418 Color: 283
Size: 3260 Color: 268

Bin 125: 160 of cap free
Amount of items: 6
Items: 
Size: 3916 Color: 275
Size: 1268 Color: 190
Size: 1057 Color: 175
Size: 943 Color: 163
Size: 240 Color: 64
Size: 240 Color: 63

Bin 126: 177 of cap free
Amount of items: 4
Items: 
Size: 4486 Color: 288
Size: 2781 Color: 253
Size: 190 Color: 44
Size: 190 Color: 43

Bin 127: 178 of cap free
Amount of items: 4
Items: 
Size: 4484 Color: 287
Size: 2778 Color: 252
Size: 192 Color: 46
Size: 192 Color: 45

Bin 128: 181 of cap free
Amount of items: 2
Items: 
Size: 4385 Color: 282
Size: 3258 Color: 267

Bin 129: 204 of cap free
Amount of items: 4
Items: 
Size: 3921 Color: 278
Size: 3251 Color: 264
Size: 232 Color: 61
Size: 216 Color: 56

Bin 130: 239 of cap free
Amount of items: 7
Items: 
Size: 3913 Color: 273
Size: 724 Color: 143
Size: 724 Color: 142
Size: 672 Color: 138
Size: 650 Color: 136
Size: 650 Color: 135
Size: 252 Color: 68

Bin 131: 252 of cap free
Amount of items: 16
Items: 
Size: 650 Color: 134
Size: 648 Color: 133
Size: 648 Color: 132
Size: 648 Color: 131
Size: 558 Color: 124
Size: 556 Color: 121
Size: 552 Color: 119
Size: 504 Color: 117
Size: 496 Color: 116
Size: 494 Color: 115
Size: 476 Color: 112
Size: 284 Color: 74
Size: 280 Color: 73
Size: 260 Color: 72
Size: 260 Color: 71
Size: 258 Color: 70

Bin 132: 266 of cap free
Amount of items: 20
Items: 
Size: 456 Color: 111
Size: 448 Color: 110
Size: 442 Color: 108
Size: 440 Color: 107
Size: 438 Color: 106
Size: 434 Color: 105
Size: 434 Color: 104
Size: 432 Color: 102
Size: 430 Color: 101
Size: 414 Color: 99
Size: 344 Color: 86
Size: 342 Color: 85
Size: 338 Color: 84
Size: 328 Color: 82
Size: 328 Color: 81
Size: 308 Color: 80
Size: 308 Color: 78
Size: 304 Color: 77
Size: 296 Color: 76
Size: 294 Color: 75

Bin 133: 4436 of cap free
Amount of items: 9
Items: 
Size: 414 Color: 98
Size: 408 Color: 97
Size: 396 Color: 96
Size: 382 Color: 95
Size: 366 Color: 91
Size: 364 Color: 90
Size: 354 Color: 89
Size: 352 Color: 88
Size: 352 Color: 87

Total size: 1032768
Total free space: 7824

