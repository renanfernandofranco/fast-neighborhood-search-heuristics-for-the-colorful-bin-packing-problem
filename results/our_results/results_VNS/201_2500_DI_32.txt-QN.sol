Capicity Bin: 2356
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1182 Color: 141
Size: 858 Color: 126
Size: 158 Color: 58
Size: 80 Color: 34
Size: 78 Color: 33

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1189 Color: 143
Size: 981 Color: 133
Size: 66 Color: 28
Size: 62 Color: 27
Size: 58 Color: 24

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1594 Color: 159
Size: 702 Color: 120
Size: 30 Color: 4
Size: 30 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 163
Size: 554 Color: 111
Size: 108 Color: 43

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1803 Color: 168
Size: 495 Color: 107
Size: 58 Color: 25

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 170
Size: 381 Color: 96
Size: 164 Color: 60

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 171
Size: 454 Color: 101
Size: 88 Color: 37

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 179
Size: 262 Color: 83
Size: 156 Color: 57

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 182
Size: 318 Color: 90
Size: 60 Color: 26

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 183
Size: 235 Color: 77
Size: 124 Color: 49

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 188
Size: 214 Color: 72
Size: 100 Color: 42

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 190
Size: 242 Color: 80
Size: 44 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2075 Color: 192
Size: 241 Color: 79
Size: 40 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 193
Size: 231 Color: 76
Size: 46 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2080 Color: 194
Size: 192 Color: 66
Size: 84 Color: 35

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 196
Size: 194 Color: 68
Size: 76 Color: 31

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 198
Size: 186 Color: 64
Size: 68 Color: 29

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 199
Size: 215 Color: 73
Size: 38 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 200
Size: 188 Color: 65
Size: 50 Color: 19

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 165
Size: 613 Color: 113

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 166
Size: 448 Color: 100
Size: 144 Color: 56

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1873 Color: 174
Size: 482 Color: 106

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1880 Color: 176
Size: 475 Color: 105

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2119 Color: 201
Size: 236 Color: 78

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1377 Color: 150
Size: 933 Color: 130
Size: 44 Color: 15

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 169
Size: 547 Color: 110

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1843 Color: 173
Size: 499 Color: 108
Size: 12 Color: 1

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1899 Color: 178
Size: 455 Color: 102

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2071 Color: 191
Size: 283 Color: 85

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2083 Color: 195
Size: 271 Color: 84

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2099 Color: 197
Size: 255 Color: 81

Bin 32: 3 of cap free
Amount of items: 11
Items: 
Size: 360 Color: 95
Size: 297 Color: 88
Size: 292 Color: 87
Size: 290 Color: 86
Size: 259 Color: 82
Size: 229 Color: 75
Size: 226 Color: 74
Size: 128 Color: 50
Size: 92 Color: 40
Size: 90 Color: 39
Size: 90 Color: 38

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 148
Size: 977 Color: 132
Size: 46 Color: 17

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 158
Size: 742 Color: 121
Size: 32 Color: 5

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 160
Size: 679 Color: 118
Size: 16 Color: 2

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1878 Color: 175
Size: 474 Color: 104

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 189
Size: 301 Color: 89
Size: 4 Color: 0

Bin 38: 5 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 145
Size: 1053 Color: 137
Size: 56 Color: 22

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 147
Size: 982 Color: 135
Size: 52 Color: 20

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 2001 Color: 184
Size: 350 Color: 94

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 2010 Color: 185
Size: 341 Color: 93

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 2031 Color: 187
Size: 320 Color: 91

Bin 43: 6 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 152
Size: 897 Color: 128
Size: 40 Color: 12

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 155
Size: 787 Color: 122
Size: 36 Color: 8

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 164
Size: 649 Color: 115

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1947 Color: 180
Size: 403 Color: 98

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 2017 Color: 186
Size: 333 Color: 92

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1890 Color: 177
Size: 459 Color: 103

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1681 Color: 162
Size: 667 Color: 117

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1782 Color: 167
Size: 565 Color: 112

Bin 51: 9 of cap free
Amount of items: 2
Items: 
Size: 1957 Color: 181
Size: 390 Color: 97

Bin 52: 11 of cap free
Amount of items: 2
Items: 
Size: 1181 Color: 140
Size: 1164 Color: 138

Bin 53: 11 of cap free
Amount of items: 2
Items: 
Size: 1679 Color: 161
Size: 666 Color: 116

Bin 54: 12 of cap free
Amount of items: 2
Items: 
Size: 1518 Color: 154
Size: 826 Color: 125

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 156
Size: 801 Color: 123

Bin 56: 15 of cap free
Amount of items: 2
Items: 
Size: 1827 Color: 172
Size: 514 Color: 109

Bin 57: 16 of cap free
Amount of items: 3
Items: 
Size: 1366 Color: 149
Size: 930 Color: 129
Size: 44 Color: 16

Bin 58: 22 of cap free
Amount of items: 4
Items: 
Size: 1179 Color: 139
Size: 638 Color: 114
Size: 429 Color: 99
Size: 88 Color: 36

Bin 59: 31 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 144
Size: 1032 Color: 136
Size: 56 Color: 23

Bin 60: 33 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 153
Size: 817 Color: 124
Size: 36 Color: 9

Bin 61: 34 of cap free
Amount of items: 4
Items: 
Size: 1567 Color: 157
Size: 691 Color: 119
Size: 32 Color: 7
Size: 32 Color: 6

Bin 62: 40 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 146
Size: 981 Color: 134
Size: 54 Color: 21

Bin 63: 41 of cap free
Amount of items: 3
Items: 
Size: 1397 Color: 151
Size: 876 Color: 127
Size: 42 Color: 13

Bin 64: 63 of cap free
Amount of items: 15
Items: 
Size: 202 Color: 71
Size: 199 Color: 70
Size: 196 Color: 69
Size: 194 Color: 67
Size: 184 Color: 63
Size: 178 Color: 62
Size: 168 Color: 61
Size: 162 Color: 59
Size: 144 Color: 55
Size: 122 Color: 48
Size: 114 Color: 47
Size: 112 Color: 46
Size: 112 Color: 45
Size: 108 Color: 44
Size: 98 Color: 41

Bin 65: 76 of cap free
Amount of items: 4
Items: 
Size: 1185 Color: 142
Size: 951 Color: 131
Size: 76 Color: 32
Size: 68 Color: 30

Bin 66: 1812 of cap free
Amount of items: 4
Items: 
Size: 138 Color: 54
Size: 136 Color: 53
Size: 136 Color: 52
Size: 134 Color: 51

Total size: 153140
Total free space: 2356

