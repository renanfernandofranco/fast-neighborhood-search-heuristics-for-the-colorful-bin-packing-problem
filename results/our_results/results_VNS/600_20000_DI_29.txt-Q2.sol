Capicity Bin: 16800
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 36
Items: 
Size: 736 Color: 1
Size: 720 Color: 1
Size: 676 Color: 1
Size: 672 Color: 1
Size: 658 Color: 1
Size: 636 Color: 1
Size: 632 Color: 1
Size: 624 Color: 1
Size: 566 Color: 1
Size: 532 Color: 1
Size: 520 Color: 1
Size: 504 Color: 1
Size: 498 Color: 1
Size: 480 Color: 1
Size: 480 Color: 1
Size: 478 Color: 1
Size: 448 Color: 1
Size: 432 Color: 0
Size: 432 Color: 0
Size: 432 Color: 0
Size: 420 Color: 0
Size: 420 Color: 0
Size: 400 Color: 0
Size: 400 Color: 0
Size: 384 Color: 0
Size: 376 Color: 0
Size: 358 Color: 0
Size: 352 Color: 0
Size: 340 Color: 0
Size: 336 Color: 0
Size: 336 Color: 0
Size: 328 Color: 0
Size: 324 Color: 0
Size: 320 Color: 0
Size: 288 Color: 1
Size: 262 Color: 0

Bin 2: 0 of cap free
Amount of items: 11
Items: 
Size: 8404 Color: 1
Size: 1168 Color: 1
Size: 1120 Color: 1
Size: 1096 Color: 1
Size: 1080 Color: 1
Size: 808 Color: 0
Size: 732 Color: 0
Size: 728 Color: 0
Size: 720 Color: 0
Size: 656 Color: 0
Size: 288 Color: 1

Bin 3: 0 of cap free
Amount of items: 8
Items: 
Size: 8401 Color: 0
Size: 1533 Color: 1
Size: 1510 Color: 1
Size: 1464 Color: 1
Size: 1232 Color: 0
Size: 1116 Color: 0
Size: 1056 Color: 0
Size: 488 Color: 1

Bin 4: 0 of cap free
Amount of items: 7
Items: 
Size: 8410 Color: 1
Size: 2031 Color: 1
Size: 1628 Color: 1
Size: 1595 Color: 1
Size: 1396 Color: 0
Size: 1288 Color: 0
Size: 452 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 8413 Color: 1
Size: 6991 Color: 1
Size: 1396 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9066 Color: 1
Size: 6986 Color: 0
Size: 748 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 0
Size: 6192 Color: 1
Size: 600 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 0
Size: 5970 Color: 1
Size: 524 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 1
Size: 6220 Color: 1
Size: 240 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 0
Size: 6200 Color: 1
Size: 292 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11864 Color: 1
Size: 4216 Color: 0
Size: 720 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 0
Size: 4282 Color: 0
Size: 408 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 0
Size: 4088 Color: 0
Size: 332 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 0
Size: 2152 Color: 1
Size: 1524 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13978 Color: 0
Size: 1598 Color: 1
Size: 1224 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 0
Size: 1512 Color: 1
Size: 1110 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 0
Size: 2020 Color: 1
Size: 368 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 1
Size: 1652 Color: 0
Size: 576 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 1
Size: 1518 Color: 0
Size: 704 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 0
Size: 1548 Color: 0
Size: 366 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15030 Color: 1
Size: 1406 Color: 1
Size: 364 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15070 Color: 0
Size: 1442 Color: 0
Size: 288 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15092 Color: 1
Size: 1392 Color: 1
Size: 316 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 1
Size: 6256 Color: 1
Size: 404 Color: 0

Bin 25: 1 of cap free
Amount of items: 5
Items: 
Size: 10856 Color: 0
Size: 2154 Color: 1
Size: 2105 Color: 1
Size: 1428 Color: 1
Size: 256 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 0
Size: 3595 Color: 0
Size: 902 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 13145 Color: 1
Size: 3166 Color: 0
Size: 488 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 1
Size: 1928 Color: 0
Size: 1642 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 0
Size: 3085 Color: 1
Size: 350 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 13923 Color: 0
Size: 2244 Color: 0
Size: 632 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 14199 Color: 0
Size: 2168 Color: 1
Size: 432 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 14427 Color: 0
Size: 1592 Color: 1
Size: 780 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 14475 Color: 0
Size: 1524 Color: 1
Size: 800 Color: 0

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 14693 Color: 0
Size: 2106 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 14799 Color: 0
Size: 1392 Color: 0
Size: 608 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 9312 Color: 0
Size: 6994 Color: 1
Size: 492 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 1
Size: 7002 Color: 0
Size: 368 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 12251 Color: 0
Size: 4083 Color: 1
Size: 464 Color: 0

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 0
Size: 4076 Color: 1
Size: 468 Color: 1

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 12390 Color: 1
Size: 4408 Color: 0

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 1
Size: 3830 Color: 0
Size: 416 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 1
Size: 3172 Color: 0
Size: 436 Color: 1

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 13408 Color: 1
Size: 3390 Color: 0

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 13460 Color: 0
Size: 3338 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 1
Size: 2700 Color: 1
Size: 448 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 1
Size: 2722 Color: 0

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 14438 Color: 1
Size: 2360 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 1
Size: 1392 Color: 0
Size: 816 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 14701 Color: 0
Size: 1505 Color: 0
Size: 592 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 9368 Color: 1
Size: 6997 Color: 0
Size: 432 Color: 1

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 11764 Color: 1
Size: 4517 Color: 0
Size: 516 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 0
Size: 3601 Color: 0
Size: 352 Color: 1

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 1
Size: 2977 Color: 1
Size: 446 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 1
Size: 2841 Color: 0
Size: 296 Color: 0

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 8405 Color: 0
Size: 6993 Color: 0
Size: 1398 Color: 1

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 0
Size: 4052 Color: 1
Size: 368 Color: 1

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 12564 Color: 0
Size: 4040 Color: 1
Size: 192 Color: 0

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 13118 Color: 0
Size: 3678 Color: 1

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 0
Size: 3124 Color: 1

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 0
Size: 2968 Color: 1

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 14275 Color: 1
Size: 2521 Color: 0

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 14330 Color: 0
Size: 2466 Color: 1

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 11381 Color: 1
Size: 5414 Color: 0

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 12741 Color: 1
Size: 3750 Color: 0
Size: 304 Color: 1

Bin 65: 5 of cap free
Amount of items: 3
Items: 
Size: 14007 Color: 1
Size: 2484 Color: 0
Size: 304 Color: 1

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 11382 Color: 0
Size: 5412 Color: 1

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 12734 Color: 0
Size: 4060 Color: 1

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 13842 Color: 1
Size: 2952 Color: 0

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 1
Size: 2414 Color: 0

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 0
Size: 5141 Color: 0
Size: 512 Color: 1

Bin 71: 7 of cap free
Amount of items: 3
Items: 
Size: 12206 Color: 0
Size: 4019 Color: 1
Size: 568 Color: 1

Bin 72: 7 of cap free
Amount of items: 3
Items: 
Size: 12843 Color: 0
Size: 3598 Color: 0
Size: 352 Color: 1

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 14365 Color: 1
Size: 2428 Color: 0

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 8504 Color: 0
Size: 8288 Color: 1

Bin 75: 8 of cap free
Amount of items: 3
Items: 
Size: 11044 Color: 1
Size: 5492 Color: 0
Size: 256 Color: 1

Bin 76: 8 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 1
Size: 3658 Color: 0
Size: 336 Color: 1

Bin 77: 8 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 0
Size: 2602 Color: 1
Size: 1136 Color: 0

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 1
Size: 3068 Color: 0

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 15000 Color: 1
Size: 1792 Color: 0

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 1
Size: 2603 Color: 0

Bin 81: 9 of cap free
Amount of items: 3
Items: 
Size: 14982 Color: 1
Size: 1665 Color: 0
Size: 144 Color: 1

Bin 82: 9 of cap free
Amount of items: 3
Items: 
Size: 15114 Color: 0
Size: 1669 Color: 1
Size: 8 Color: 0

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 11074 Color: 0
Size: 5388 Color: 0
Size: 328 Color: 1

Bin 84: 10 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 2492 Color: 0
Size: 242 Color: 1

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 1
Size: 2626 Color: 0

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 14728 Color: 1
Size: 2062 Color: 0

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 14948 Color: 1
Size: 1842 Color: 0

Bin 88: 11 of cap free
Amount of items: 2
Items: 
Size: 14205 Color: 1
Size: 2584 Color: 0

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 0
Size: 2180 Color: 1
Size: 126 Color: 1

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 14845 Color: 1
Size: 1944 Color: 0

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 15009 Color: 0
Size: 1780 Color: 1

Bin 92: 12 of cap free
Amount of items: 12
Items: 
Size: 8402 Color: 1
Size: 1080 Color: 1
Size: 1072 Color: 1
Size: 1008 Color: 1
Size: 958 Color: 1
Size: 704 Color: 0
Size: 688 Color: 0
Size: 664 Color: 0
Size: 616 Color: 0
Size: 616 Color: 0
Size: 594 Color: 0
Size: 386 Color: 1

Bin 93: 12 of cap free
Amount of items: 5
Items: 
Size: 10631 Color: 1
Size: 2044 Color: 1
Size: 1478 Color: 1
Size: 1443 Color: 0
Size: 1192 Color: 0

Bin 94: 12 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 1
Size: 3160 Color: 0
Size: 568 Color: 1

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 14602 Color: 1
Size: 2186 Color: 0

Bin 96: 12 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 0
Size: 1644 Color: 1
Size: 96 Color: 0

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 12996 Color: 1
Size: 3791 Color: 0

Bin 98: 14 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 0
Size: 7440 Color: 0
Size: 920 Color: 1

Bin 99: 14 of cap free
Amount of items: 3
Items: 
Size: 11574 Color: 0
Size: 4252 Color: 1
Size: 960 Color: 1

Bin 100: 15 of cap free
Amount of items: 3
Items: 
Size: 11528 Color: 0
Size: 4281 Color: 0
Size: 976 Color: 1

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 11896 Color: 1
Size: 4888 Color: 0

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 13240 Color: 1
Size: 3544 Color: 0

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 1
Size: 2296 Color: 0

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 14674 Color: 0
Size: 2110 Color: 1

Bin 105: 18 of cap free
Amount of items: 3
Items: 
Size: 10082 Color: 1
Size: 6148 Color: 1
Size: 552 Color: 0

Bin 106: 18 of cap free
Amount of items: 2
Items: 
Size: 13677 Color: 0
Size: 3105 Color: 1

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 1
Size: 2982 Color: 0

Bin 108: 19 of cap free
Amount of items: 3
Items: 
Size: 9549 Color: 0
Size: 7000 Color: 1
Size: 232 Color: 0

Bin 109: 19 of cap free
Amount of items: 3
Items: 
Size: 13002 Color: 0
Size: 1925 Color: 0
Size: 1854 Color: 1

Bin 110: 19 of cap free
Amount of items: 2
Items: 
Size: 14696 Color: 1
Size: 2085 Color: 0

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 13481 Color: 1
Size: 3299 Color: 0

Bin 112: 20 of cap free
Amount of items: 2
Items: 
Size: 13976 Color: 1
Size: 2804 Color: 0

Bin 113: 20 of cap free
Amount of items: 2
Items: 
Size: 14649 Color: 0
Size: 2131 Color: 1

Bin 114: 22 of cap free
Amount of items: 2
Items: 
Size: 14792 Color: 0
Size: 1986 Color: 1

Bin 115: 22 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 0
Size: 1688 Color: 1
Size: 14 Color: 1

Bin 116: 24 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 1
Size: 3684 Color: 0

Bin 117: 24 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 0
Size: 2576 Color: 1

Bin 118: 24 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 0
Size: 1860 Color: 1
Size: 96 Color: 1

Bin 119: 26 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 0
Size: 5250 Color: 1
Size: 352 Color: 1

Bin 120: 26 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 1
Size: 3092 Color: 0

Bin 121: 26 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 0
Size: 2868 Color: 1

Bin 122: 26 of cap free
Amount of items: 2
Items: 
Size: 14836 Color: 0
Size: 1938 Color: 1

Bin 123: 27 of cap free
Amount of items: 2
Items: 
Size: 13293 Color: 0
Size: 3480 Color: 1

Bin 124: 27 of cap free
Amount of items: 3
Items: 
Size: 14758 Color: 1
Size: 1931 Color: 0
Size: 84 Color: 1

Bin 125: 27 of cap free
Amount of items: 2
Items: 
Size: 14980 Color: 0
Size: 1793 Color: 1

Bin 126: 28 of cap free
Amount of items: 2
Items: 
Size: 12414 Color: 1
Size: 4358 Color: 0

Bin 127: 29 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 0
Size: 6043 Color: 1

Bin 128: 29 of cap free
Amount of items: 2
Items: 
Size: 13393 Color: 0
Size: 3378 Color: 1

Bin 129: 29 of cap free
Amount of items: 2
Items: 
Size: 15069 Color: 1
Size: 1702 Color: 0

Bin 130: 30 of cap free
Amount of items: 2
Items: 
Size: 14601 Color: 1
Size: 2169 Color: 0

Bin 131: 31 of cap free
Amount of items: 2
Items: 
Size: 12979 Color: 1
Size: 3790 Color: 0

Bin 132: 31 of cap free
Amount of items: 2
Items: 
Size: 14995 Color: 1
Size: 1774 Color: 0

Bin 133: 32 of cap free
Amount of items: 2
Items: 
Size: 11704 Color: 1
Size: 5064 Color: 0

Bin 134: 32 of cap free
Amount of items: 2
Items: 
Size: 11964 Color: 0
Size: 4804 Color: 1

Bin 135: 35 of cap free
Amount of items: 2
Items: 
Size: 12247 Color: 1
Size: 4518 Color: 0

Bin 136: 37 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 0
Size: 4284 Color: 1

Bin 137: 37 of cap free
Amount of items: 2
Items: 
Size: 13775 Color: 0
Size: 2988 Color: 1

Bin 138: 39 of cap free
Amount of items: 2
Items: 
Size: 11960 Color: 0
Size: 4801 Color: 1

Bin 139: 40 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 1
Size: 2354 Color: 0

Bin 140: 42 of cap free
Amount of items: 2
Items: 
Size: 12034 Color: 0
Size: 4724 Color: 1

Bin 141: 42 of cap free
Amount of items: 2
Items: 
Size: 13226 Color: 0
Size: 3532 Color: 1

Bin 142: 45 of cap free
Amount of items: 2
Items: 
Size: 11204 Color: 1
Size: 5551 Color: 0

Bin 143: 45 of cap free
Amount of items: 2
Items: 
Size: 14356 Color: 1
Size: 2399 Color: 0

Bin 144: 47 of cap free
Amount of items: 2
Items: 
Size: 13744 Color: 1
Size: 3009 Color: 0

Bin 145: 48 of cap free
Amount of items: 2
Items: 
Size: 14508 Color: 0
Size: 2244 Color: 1

Bin 146: 50 of cap free
Amount of items: 2
Items: 
Size: 14478 Color: 1
Size: 2272 Color: 0

Bin 147: 52 of cap free
Amount of items: 9
Items: 
Size: 8408 Color: 1
Size: 1396 Color: 1
Size: 1232 Color: 1
Size: 1208 Color: 1
Size: 952 Color: 0
Size: 952 Color: 0
Size: 928 Color: 0
Size: 856 Color: 0
Size: 816 Color: 0

Bin 148: 52 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 1
Size: 3214 Color: 0

Bin 149: 52 of cap free
Amount of items: 2
Items: 
Size: 14668 Color: 1
Size: 2080 Color: 0

Bin 150: 54 of cap free
Amount of items: 3
Items: 
Size: 8418 Color: 0
Size: 7576 Color: 1
Size: 752 Color: 0

Bin 151: 54 of cap free
Amount of items: 2
Items: 
Size: 14830 Color: 0
Size: 1916 Color: 1

Bin 152: 55 of cap free
Amount of items: 2
Items: 
Size: 14806 Color: 1
Size: 1939 Color: 0

Bin 153: 60 of cap free
Amount of items: 2
Items: 
Size: 14972 Color: 0
Size: 1768 Color: 1

Bin 154: 61 of cap free
Amount of items: 2
Items: 
Size: 14115 Color: 0
Size: 2624 Color: 1

Bin 155: 62 of cap free
Amount of items: 2
Items: 
Size: 13880 Color: 1
Size: 2858 Color: 0

Bin 156: 62 of cap free
Amount of items: 2
Items: 
Size: 14904 Color: 1
Size: 1834 Color: 0

Bin 157: 64 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 0
Size: 4104 Color: 1

Bin 158: 64 of cap free
Amount of items: 2
Items: 
Size: 14804 Color: 1
Size: 1932 Color: 0

Bin 159: 64 of cap free
Amount of items: 2
Items: 
Size: 15068 Color: 0
Size: 1668 Color: 1

Bin 160: 66 of cap free
Amount of items: 2
Items: 
Size: 14472 Color: 1
Size: 2262 Color: 0

Bin 161: 68 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 0
Size: 3168 Color: 1

Bin 162: 70 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 0
Size: 2239 Color: 1

Bin 163: 74 of cap free
Amount of items: 2
Items: 
Size: 14990 Color: 1
Size: 1736 Color: 0

Bin 164: 76 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 1
Size: 7004 Color: 0

Bin 165: 76 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 0
Size: 4776 Color: 1

Bin 166: 76 of cap free
Amount of items: 2
Items: 
Size: 14108 Color: 0
Size: 2616 Color: 1

Bin 167: 78 of cap free
Amount of items: 2
Items: 
Size: 14270 Color: 0
Size: 2452 Color: 1

Bin 168: 80 of cap free
Amount of items: 2
Items: 
Size: 14887 Color: 1
Size: 1833 Color: 0

Bin 169: 82 of cap free
Amount of items: 2
Items: 
Size: 14961 Color: 0
Size: 1757 Color: 1

Bin 170: 86 of cap free
Amount of items: 2
Items: 
Size: 11042 Color: 1
Size: 5672 Color: 0

Bin 171: 94 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 0
Size: 4774 Color: 1

Bin 172: 94 of cap free
Amount of items: 2
Items: 
Size: 14656 Color: 1
Size: 2050 Color: 0

Bin 173: 98 of cap free
Amount of items: 2
Items: 
Size: 10790 Color: 1
Size: 5912 Color: 0

Bin 174: 98 of cap free
Amount of items: 2
Items: 
Size: 12728 Color: 1
Size: 3974 Color: 0

Bin 175: 112 of cap free
Amount of items: 2
Items: 
Size: 14484 Color: 0
Size: 2204 Color: 1

Bin 176: 114 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 0
Size: 7048 Color: 1

Bin 177: 116 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 0
Size: 2792 Color: 1

Bin 178: 123 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 1
Size: 7001 Color: 0
Size: 1264 Color: 1

Bin 179: 125 of cap free
Amount of items: 2
Items: 
Size: 11665 Color: 1
Size: 5010 Color: 0

Bin 180: 128 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 1
Size: 3400 Color: 0

Bin 181: 128 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 0
Size: 2440 Color: 1

Bin 182: 129 of cap free
Amount of items: 2
Items: 
Size: 14342 Color: 1
Size: 2329 Color: 0

Bin 183: 135 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 0
Size: 3300 Color: 1

Bin 184: 142 of cap free
Amount of items: 2
Items: 
Size: 10212 Color: 0
Size: 6446 Color: 1

Bin 185: 150 of cap free
Amount of items: 2
Items: 
Size: 14086 Color: 1
Size: 2564 Color: 0

Bin 186: 151 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 1
Size: 3185 Color: 0

Bin 187: 155 of cap free
Amount of items: 8
Items: 
Size: 8409 Color: 1
Size: 1398 Color: 1
Size: 1398 Color: 1
Size: 1396 Color: 1
Size: 1056 Color: 0
Size: 1028 Color: 0
Size: 1000 Color: 0
Size: 960 Color: 0

Bin 188: 170 of cap free
Amount of items: 2
Items: 
Size: 11662 Color: 1
Size: 4968 Color: 0

Bin 189: 173 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 0
Size: 2767 Color: 1

Bin 190: 176 of cap free
Amount of items: 25
Items: 
Size: 936 Color: 1
Size: 900 Color: 1
Size: 868 Color: 1
Size: 864 Color: 1
Size: 854 Color: 1
Size: 808 Color: 1
Size: 800 Color: 1
Size: 792 Color: 1
Size: 784 Color: 1
Size: 764 Color: 1
Size: 758 Color: 1
Size: 756 Color: 1
Size: 592 Color: 0
Size: 550 Color: 0
Size: 544 Color: 0
Size: 544 Color: 0
Size: 536 Color: 0
Size: 534 Color: 0
Size: 532 Color: 0
Size: 512 Color: 0
Size: 508 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 464 Color: 0
Size: 464 Color: 0

Bin 191: 176 of cap free
Amount of items: 2
Items: 
Size: 11916 Color: 1
Size: 4708 Color: 0

Bin 192: 177 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2163 Color: 1
Size: 1444 Color: 0

Bin 193: 218 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 0
Size: 3910 Color: 0
Size: 1631 Color: 1

Bin 194: 231 of cap free
Amount of items: 2
Items: 
Size: 11901 Color: 0
Size: 4668 Color: 1

Bin 195: 246 of cap free
Amount of items: 2
Items: 
Size: 10952 Color: 1
Size: 5602 Color: 0

Bin 196: 262 of cap free
Amount of items: 2
Items: 
Size: 8696 Color: 0
Size: 7842 Color: 1

Bin 197: 262 of cap free
Amount of items: 2
Items: 
Size: 9453 Color: 1
Size: 7085 Color: 0

Bin 198: 280 of cap free
Amount of items: 2
Items: 
Size: 9524 Color: 0
Size: 6996 Color: 1

Bin 199: 8996 of cap free
Amount of items: 24
Items: 
Size: 416 Color: 1
Size: 408 Color: 1
Size: 386 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 350 Color: 1
Size: 328 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 318 Color: 0
Size: 306 Color: 0
Size: 304 Color: 0
Size: 300 Color: 1
Size: 300 Color: 1
Size: 300 Color: 0
Size: 288 Color: 0
Size: 288 Color: 0
Size: 280 Color: 0
Size: 280 Color: 0
Size: 232 Color: 0
Size: 224 Color: 0

Total size: 3326400
Total free space: 16800

