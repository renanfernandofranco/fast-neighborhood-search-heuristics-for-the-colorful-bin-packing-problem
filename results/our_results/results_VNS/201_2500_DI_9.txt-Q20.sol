Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 16
Size: 882 Color: 12
Size: 176 Color: 15

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 9
Size: 920 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 9
Size: 780 Color: 0
Size: 40 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 561 Color: 8
Size: 110 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1798 Color: 2
Size: 558 Color: 18
Size: 108 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1884 Color: 19
Size: 484 Color: 17
Size: 96 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 10
Size: 502 Color: 14
Size: 44 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 8
Size: 351 Color: 3
Size: 144 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 15
Size: 246 Color: 10
Size: 204 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2020 Color: 17
Size: 308 Color: 2
Size: 136 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2054 Color: 3
Size: 314 Color: 1
Size: 96 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 16
Size: 315 Color: 14
Size: 86 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 4
Size: 252 Color: 5
Size: 122 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 9
Size: 244 Color: 10
Size: 120 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2151 Color: 13
Size: 249 Color: 15
Size: 64 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 4
Size: 236 Color: 3
Size: 48 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 0
Size: 244 Color: 18
Size: 48 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 15
Size: 226 Color: 11
Size: 56 Color: 16

Bin 19: 1 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 16
Size: 869 Color: 2
Size: 200 Color: 18
Size: 108 Color: 7
Size: 52 Color: 3

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 19
Size: 1027 Color: 17
Size: 40 Color: 18

Bin 21: 1 of cap free
Amount of items: 4
Items: 
Size: 1559 Color: 4
Size: 684 Color: 13
Size: 176 Color: 6
Size: 44 Color: 11

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 16
Size: 617 Color: 11
Size: 172 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 14
Size: 685 Color: 8
Size: 60 Color: 8

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 13
Size: 540 Color: 4
Size: 26 Color: 17

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 16
Size: 365 Color: 8
Size: 128 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 10
Size: 387 Color: 5
Size: 8 Color: 16

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2107 Color: 1
Size: 204 Color: 2
Size: 152 Color: 2

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 2164 Color: 10
Size: 299 Color: 9

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 6
Size: 261 Color: 18
Size: 8 Color: 3

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 18
Size: 882 Color: 16
Size: 48 Color: 13

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 13
Size: 785 Color: 4
Size: 136 Color: 7

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 12
Size: 771 Color: 2
Size: 48 Color: 16

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1740 Color: 8
Size: 722 Color: 1

Bin 34: 3 of cap free
Amount of items: 5
Items: 
Size: 1233 Color: 5
Size: 404 Color: 16
Size: 384 Color: 9
Size: 272 Color: 6
Size: 168 Color: 18

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 4
Size: 887 Color: 6
Size: 124 Color: 10

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1988 Color: 16
Size: 473 Color: 15

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 2170 Color: 19
Size: 291 Color: 5

Bin 38: 4 of cap free
Amount of items: 5
Items: 
Size: 1235 Color: 10
Size: 452 Color: 19
Size: 441 Color: 11
Size: 238 Color: 14
Size: 94 Color: 0

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 19
Size: 840 Color: 2
Size: 378 Color: 7

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 1
Size: 342 Color: 14

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 2188 Color: 10
Size: 272 Color: 13

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 11
Size: 1022 Color: 5
Size: 102 Color: 9

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 13
Size: 734 Color: 9

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 2001 Color: 19
Size: 458 Color: 9

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 2045 Color: 11
Size: 414 Color: 7

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 2115 Color: 13
Size: 332 Color: 5
Size: 12 Color: 4

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 9
Size: 413 Color: 14
Size: 18 Color: 17

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 2123 Color: 16
Size: 335 Color: 17

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 2167 Color: 18
Size: 290 Color: 16

Bin 50: 8 of cap free
Amount of items: 19
Items: 
Size: 372 Color: 19
Size: 200 Color: 4
Size: 188 Color: 9
Size: 176 Color: 3
Size: 172 Color: 16
Size: 152 Color: 12
Size: 148 Color: 12
Size: 136 Color: 18
Size: 120 Color: 12
Size: 100 Color: 2
Size: 88 Color: 13
Size: 88 Color: 6
Size: 82 Color: 9
Size: 80 Color: 7
Size: 72 Color: 18
Size: 72 Color: 16
Size: 72 Color: 7
Size: 72 Color: 6
Size: 66 Color: 17

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 11
Size: 758 Color: 17

Bin 52: 11 of cap free
Amount of items: 2
Items: 
Size: 1849 Color: 7
Size: 604 Color: 13

Bin 53: 11 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 0
Size: 513 Color: 7

Bin 54: 14 of cap free
Amount of items: 2
Items: 
Size: 1558 Color: 18
Size: 892 Color: 12

Bin 55: 14 of cap free
Amount of items: 2
Items: 
Size: 1820 Color: 3
Size: 630 Color: 1

Bin 56: 18 of cap free
Amount of items: 2
Items: 
Size: 1418 Color: 7
Size: 1028 Color: 15

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1657 Color: 0
Size: 789 Color: 19

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 17
Size: 846 Color: 9

Bin 59: 20 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 7
Size: 662 Color: 13
Size: 72 Color: 4

Bin 60: 23 of cap free
Amount of items: 2
Items: 
Size: 1937 Color: 19
Size: 504 Color: 17

Bin 61: 26 of cap free
Amount of items: 2
Items: 
Size: 1862 Color: 7
Size: 576 Color: 11

Bin 62: 31 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 4
Size: 1093 Color: 11
Size: 104 Color: 6

Bin 63: 32 of cap free
Amount of items: 4
Items: 
Size: 1410 Color: 6
Size: 874 Color: 5
Size: 76 Color: 16
Size: 72 Color: 11

Bin 64: 36 of cap free
Amount of items: 2
Items: 
Size: 1487 Color: 8
Size: 941 Color: 7

Bin 65: 37 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 17
Size: 1026 Color: 4

Bin 66: 2044 of cap free
Amount of items: 7
Items: 
Size: 68 Color: 19
Size: 68 Color: 8
Size: 64 Color: 17
Size: 58 Color: 5
Size: 58 Color: 1
Size: 56 Color: 11
Size: 48 Color: 4

Total size: 160160
Total free space: 2464

