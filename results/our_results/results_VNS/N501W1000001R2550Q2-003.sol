Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 350950 Color: 0
Size: 383346 Color: 0
Size: 265705 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 388897 Color: 0
Size: 325766 Color: 1
Size: 285338 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 383550 Color: 0
Size: 337302 Color: 1
Size: 279149 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 342186 Color: 0
Size: 335898 Color: 0
Size: 321917 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 481012 Color: 0
Size: 265417 Color: 1
Size: 253572 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 392923 Color: 1
Size: 351279 Color: 0
Size: 255799 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 428193 Color: 0
Size: 311816 Color: 0
Size: 259992 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 412730 Color: 0
Size: 294661 Color: 1
Size: 292610 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 351681 Color: 1
Size: 334113 Color: 0
Size: 314207 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 372274 Color: 1
Size: 351619 Color: 0
Size: 276108 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 400407 Color: 0
Size: 307337 Color: 1
Size: 292257 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 409755 Color: 1
Size: 318004 Color: 0
Size: 272242 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 377290 Color: 0
Size: 316037 Color: 1
Size: 306674 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 408104 Color: 0
Size: 323780 Color: 0
Size: 268117 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 436472 Color: 0
Size: 300893 Color: 0
Size: 262636 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 399898 Color: 1
Size: 331909 Color: 1
Size: 268194 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 445314 Color: 1
Size: 300935 Color: 0
Size: 253752 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 376978 Color: 1
Size: 348420 Color: 0
Size: 274603 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 357375 Color: 1
Size: 325409 Color: 0
Size: 317217 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 406474 Color: 1
Size: 338815 Color: 0
Size: 254712 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 462980 Color: 1
Size: 286200 Color: 0
Size: 250821 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 365635 Color: 0
Size: 331872 Color: 0
Size: 302494 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 370375 Color: 1
Size: 328886 Color: 0
Size: 300740 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 461057 Color: 0
Size: 271515 Color: 0
Size: 267429 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 358444 Color: 0
Size: 335414 Color: 1
Size: 306143 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 392132 Color: 0
Size: 340128 Color: 1
Size: 267741 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 375992 Color: 1
Size: 322547 Color: 0
Size: 301462 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 454756 Color: 1
Size: 283910 Color: 0
Size: 261335 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 359151 Color: 0
Size: 332002 Color: 1
Size: 308848 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 442356 Color: 0
Size: 304951 Color: 1
Size: 252694 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 458045 Color: 0
Size: 271512 Color: 1
Size: 270444 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 448686 Color: 1
Size: 295983 Color: 0
Size: 255332 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 434431 Color: 0
Size: 294738 Color: 1
Size: 270832 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 449785 Color: 0
Size: 290548 Color: 1
Size: 259668 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 438873 Color: 0
Size: 294842 Color: 0
Size: 266286 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 456213 Color: 0
Size: 274925 Color: 1
Size: 268863 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 423191 Color: 0
Size: 315623 Color: 1
Size: 261187 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 444277 Color: 0
Size: 292916 Color: 1
Size: 262808 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 380129 Color: 1
Size: 329690 Color: 0
Size: 290182 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 468224 Color: 0
Size: 280053 Color: 0
Size: 251724 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 385589 Color: 1
Size: 314328 Color: 0
Size: 300084 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 459388 Color: 0
Size: 279710 Color: 1
Size: 260903 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 384352 Color: 1
Size: 311265 Color: 1
Size: 304384 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 417529 Color: 0
Size: 297879 Color: 1
Size: 284593 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 400655 Color: 0
Size: 341135 Color: 1
Size: 258211 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 390457 Color: 0
Size: 357715 Color: 1
Size: 251829 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 416530 Color: 0
Size: 325864 Color: 0
Size: 257607 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 383168 Color: 1
Size: 316621 Color: 0
Size: 300212 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 395445 Color: 1
Size: 314568 Color: 1
Size: 289988 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 406486 Color: 1
Size: 296897 Color: 0
Size: 296618 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 393318 Color: 1
Size: 325802 Color: 0
Size: 280881 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 367682 Color: 1
Size: 342432 Color: 0
Size: 289887 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 441760 Color: 0
Size: 293143 Color: 0
Size: 265098 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 376424 Color: 0
Size: 361100 Color: 1
Size: 262477 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 473300 Color: 1
Size: 269803 Color: 0
Size: 256898 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 383675 Color: 0
Size: 320309 Color: 0
Size: 296017 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 368482 Color: 1
Size: 348362 Color: 0
Size: 283157 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 482841 Color: 1
Size: 264487 Color: 0
Size: 252673 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 0
Size: 344694 Color: 0
Size: 291825 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 450718 Color: 0
Size: 279467 Color: 1
Size: 269816 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 452907 Color: 1
Size: 276951 Color: 0
Size: 270143 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 405047 Color: 0
Size: 318400 Color: 1
Size: 276554 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 407915 Color: 1
Size: 328856 Color: 0
Size: 263230 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 488220 Color: 1
Size: 261722 Color: 1
Size: 250059 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 455860 Color: 0
Size: 277120 Color: 0
Size: 267021 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 377794 Color: 1
Size: 314833 Color: 0
Size: 307374 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 422781 Color: 1
Size: 297413 Color: 0
Size: 279807 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 403757 Color: 1
Size: 341440 Color: 0
Size: 254804 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 486361 Color: 1
Size: 259479 Color: 0
Size: 254161 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 391153 Color: 0
Size: 333421 Color: 1
Size: 275427 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 447016 Color: 1
Size: 302854 Color: 0
Size: 250131 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 359685 Color: 0
Size: 320283 Color: 1
Size: 320033 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 353237 Color: 0
Size: 333586 Color: 1
Size: 313178 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 464889 Color: 1
Size: 278058 Color: 0
Size: 257054 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 435884 Color: 1
Size: 312477 Color: 0
Size: 251640 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 487461 Color: 1
Size: 258720 Color: 0
Size: 253820 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 411785 Color: 1
Size: 309821 Color: 0
Size: 278395 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 472581 Color: 1
Size: 274799 Color: 0
Size: 252621 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 440525 Color: 0
Size: 299953 Color: 1
Size: 259523 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 498957 Color: 0
Size: 250831 Color: 0
Size: 250213 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 452898 Color: 1
Size: 274143 Color: 1
Size: 272960 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 426462 Color: 1
Size: 290701 Color: 0
Size: 282838 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 382320 Color: 0
Size: 357485 Color: 1
Size: 260196 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 374430 Color: 1
Size: 345193 Color: 0
Size: 280378 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 415320 Color: 0
Size: 315090 Color: 0
Size: 269591 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 477672 Color: 0
Size: 263707 Color: 1
Size: 258622 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 357854 Color: 0
Size: 335602 Color: 1
Size: 306545 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 395478 Color: 1
Size: 309963 Color: 1
Size: 294560 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 361567 Color: 0
Size: 336038 Color: 0
Size: 302396 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 483225 Color: 0
Size: 259281 Color: 1
Size: 257495 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 338642 Color: 0
Size: 332397 Color: 0
Size: 328962 Color: 1

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 472789 Color: 1
Size: 271291 Color: 1
Size: 255921 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 398738 Color: 0
Size: 303894 Color: 1
Size: 297369 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 410559 Color: 1
Size: 320597 Color: 0
Size: 268845 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 478591 Color: 1
Size: 263661 Color: 0
Size: 257749 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 348823 Color: 0
Size: 327702 Color: 1
Size: 323476 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 389305 Color: 1
Size: 340263 Color: 0
Size: 270433 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 358082 Color: 1
Size: 354081 Color: 0
Size: 287838 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 409840 Color: 1
Size: 309519 Color: 0
Size: 280642 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 380610 Color: 1
Size: 324625 Color: 1
Size: 294766 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 386118 Color: 1
Size: 314258 Color: 0
Size: 299625 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 376425 Color: 0
Size: 366268 Color: 1
Size: 257308 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 466840 Color: 0
Size: 273831 Color: 1
Size: 259330 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 399582 Color: 1
Size: 312964 Color: 0
Size: 287455 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 412498 Color: 0
Size: 312067 Color: 0
Size: 275436 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 485707 Color: 0
Size: 260827 Color: 0
Size: 253467 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 474966 Color: 0
Size: 269363 Color: 1
Size: 255672 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 448247 Color: 0
Size: 293892 Color: 1
Size: 257862 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 384700 Color: 0
Size: 332133 Color: 1
Size: 283168 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 494919 Color: 1
Size: 254517 Color: 0
Size: 250565 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 396616 Color: 0
Size: 321854 Color: 1
Size: 281531 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 425517 Color: 0
Size: 319422 Color: 1
Size: 255062 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 388398 Color: 0
Size: 343994 Color: 1
Size: 267609 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 355100 Color: 0
Size: 346914 Color: 0
Size: 297987 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 357919 Color: 0
Size: 344537 Color: 0
Size: 297545 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 395568 Color: 1
Size: 309700 Color: 0
Size: 294733 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 474406 Color: 0
Size: 264632 Color: 1
Size: 260963 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 374304 Color: 1
Size: 327806 Color: 0
Size: 297891 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 381780 Color: 1
Size: 320935 Color: 0
Size: 297286 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 487344 Color: 1
Size: 257026 Color: 0
Size: 255631 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 398367 Color: 0
Size: 326825 Color: 1
Size: 274809 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 497211 Color: 0
Size: 252449 Color: 0
Size: 250341 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 413060 Color: 1
Size: 316360 Color: 0
Size: 270581 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 356478 Color: 0
Size: 331044 Color: 0
Size: 312479 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 432982 Color: 1
Size: 294287 Color: 1
Size: 272732 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 394029 Color: 0
Size: 329203 Color: 0
Size: 276769 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 494473 Color: 1
Size: 253122 Color: 0
Size: 252406 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 436160 Color: 0
Size: 297997 Color: 1
Size: 265844 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 462601 Color: 0
Size: 282315 Color: 1
Size: 255085 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 365248 Color: 1
Size: 353115 Color: 0
Size: 281638 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 380593 Color: 1
Size: 345176 Color: 0
Size: 274232 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 361982 Color: 1
Size: 322767 Color: 1
Size: 315252 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 472490 Color: 1
Size: 274047 Color: 0
Size: 253464 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 410170 Color: 1
Size: 335956 Color: 0
Size: 253875 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 374389 Color: 1
Size: 358165 Color: 1
Size: 267447 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 479456 Color: 0
Size: 264073 Color: 1
Size: 256472 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 475838 Color: 0
Size: 265430 Color: 1
Size: 258733 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 440659 Color: 0
Size: 290172 Color: 1
Size: 269170 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 342332 Color: 1
Size: 330898 Color: 0
Size: 326771 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 459662 Color: 1
Size: 272019 Color: 0
Size: 268320 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 487001 Color: 0
Size: 256604 Color: 1
Size: 256396 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 451019 Color: 1
Size: 290563 Color: 1
Size: 258419 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 434772 Color: 0
Size: 297765 Color: 0
Size: 267464 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 361808 Color: 1
Size: 337276 Color: 0
Size: 300917 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 0
Size: 326512 Color: 0
Size: 310007 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 340723 Color: 1
Size: 333555 Color: 1
Size: 325723 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 477134 Color: 0
Size: 264827 Color: 1
Size: 258040 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 371790 Color: 0
Size: 345861 Color: 0
Size: 282350 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 468001 Color: 0
Size: 273787 Color: 0
Size: 258213 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 365019 Color: 0
Size: 346337 Color: 1
Size: 288645 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 366409 Color: 0
Size: 322194 Color: 0
Size: 311398 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 411646 Color: 1
Size: 304854 Color: 0
Size: 283501 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 417577 Color: 0
Size: 314771 Color: 0
Size: 267653 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 376375 Color: 1
Size: 312729 Color: 0
Size: 310897 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 383748 Color: 0
Size: 340364 Color: 0
Size: 275889 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 416793 Color: 1
Size: 289915 Color: 0
Size: 293293 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 386827 Color: 0
Size: 319831 Color: 1
Size: 293343 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 492611 Color: 0
Size: 255473 Color: 1
Size: 251917 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 465384 Color: 1
Size: 275581 Color: 1
Size: 259036 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 371705 Color: 1
Size: 332269 Color: 1
Size: 296027 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 400734 Color: 0
Size: 338716 Color: 1
Size: 260551 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 402302 Color: 1
Size: 326120 Color: 1
Size: 271579 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 452793 Color: 1
Size: 275745 Color: 1
Size: 271463 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 485204 Color: 0
Size: 258032 Color: 1
Size: 256765 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 365921 Color: 0
Size: 322798 Color: 1
Size: 311282 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 387018 Color: 0
Size: 327253 Color: 0
Size: 285730 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 370941 Color: 1
Size: 321555 Color: 1
Size: 307505 Color: 0

Total size: 167000167
Total free space: 0

