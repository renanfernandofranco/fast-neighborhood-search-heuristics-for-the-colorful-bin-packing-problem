Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 3
Size: 696 Color: 1
Size: 72 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 2
Size: 607 Color: 1
Size: 62 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 406 Color: 2
Size: 120 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 1
Size: 388 Color: 3
Size: 98 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 1
Size: 389 Color: 3
Size: 84 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 2
Size: 398 Color: 1
Size: 36 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 3
Size: 316 Color: 2
Size: 78 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 0
Size: 289 Color: 1
Size: 96 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 4
Size: 317 Color: 1
Size: 62 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 274 Color: 3
Size: 100 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 0
Size: 282 Color: 2
Size: 76 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 0
Size: 194 Color: 4
Size: 140 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 1
Size: 246 Color: 3
Size: 60 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 3
Size: 271 Color: 1
Size: 52 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 1
Size: 261 Color: 0
Size: 20 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 2
Size: 146 Color: 0
Size: 96 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 2
Size: 170 Color: 1
Size: 68 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 3
Size: 146 Color: 4
Size: 80 Color: 1

Bin 19: 1 of cap free
Amount of items: 12
Items: 
Size: 559 Color: 1
Size: 362 Color: 1
Size: 202 Color: 0
Size: 190 Color: 0
Size: 168 Color: 2
Size: 160 Color: 1
Size: 76 Color: 3
Size: 76 Color: 3
Size: 76 Color: 0
Size: 64 Color: 2
Size: 62 Color: 4
Size: 52 Color: 2

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 2
Size: 851 Color: 2
Size: 110 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 3
Size: 828 Color: 1
Size: 48 Color: 2

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 0
Size: 491 Color: 1
Size: 258 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 3
Size: 477 Color: 3
Size: 112 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 1
Size: 554 Color: 0
Size: 36 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 1
Size: 493 Color: 2
Size: 52 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 4
Size: 395 Color: 1
Size: 78 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 4
Size: 424 Color: 1
Size: 40 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 1
Size: 314 Color: 2
Size: 104 Color: 4

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 302 Color: 2
Size: 44 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 1
Size: 168 Color: 4
Size: 108 Color: 4

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 4
Size: 220 Color: 1
Size: 52 Color: 3

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 2
Size: 208 Color: 4
Size: 48 Color: 1

Bin 33: 2 of cap free
Amount of items: 5
Items: 
Size: 1026 Color: 0
Size: 731 Color: 1
Size: 211 Color: 3
Size: 42 Color: 1
Size: 36 Color: 3

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 0
Size: 529 Color: 2
Size: 56 Color: 2

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 0
Size: 235 Color: 1
Size: 48 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 3
Size: 239 Color: 4
Size: 12 Color: 0

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 1
Size: 706 Color: 3
Size: 140 Color: 2

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 4
Size: 391 Color: 2
Size: 72 Color: 1

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1754 Color: 4
Size: 291 Color: 2

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1814 Color: 4
Size: 231 Color: 0

Bin 41: 3 of cap free
Amount of items: 4
Items: 
Size: 1818 Color: 3
Size: 215 Color: 2
Size: 8 Color: 3
Size: 4 Color: 1

Bin 42: 4 of cap free
Amount of items: 4
Items: 
Size: 1029 Color: 3
Size: 853 Color: 4
Size: 126 Color: 3
Size: 36 Color: 2

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 1
Size: 802 Color: 4
Size: 40 Color: 3

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1426 Color: 1
Size: 472 Color: 2
Size: 146 Color: 3

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1770 Color: 3
Size: 274 Color: 0

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 4
Size: 321 Color: 3

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 1
Size: 562 Color: 0
Size: 64 Color: 3

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1625 Color: 0
Size: 414 Color: 2

Bin 49: 12 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 0
Size: 682 Color: 2
Size: 120 Color: 2

Bin 50: 16 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 0
Size: 321 Color: 2
Size: 8 Color: 0

Bin 51: 18 of cap free
Amount of items: 4
Items: 
Size: 1025 Color: 4
Size: 605 Color: 1
Size: 202 Color: 2
Size: 198 Color: 3

Bin 52: 18 of cap free
Amount of items: 3
Items: 
Size: 1163 Color: 1
Size: 735 Color: 4
Size: 132 Color: 4

Bin 53: 18 of cap free
Amount of items: 2
Items: 
Size: 1321 Color: 0
Size: 709 Color: 1

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1588 Color: 0
Size: 442 Color: 2

Bin 55: 22 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 2
Size: 353 Color: 0

Bin 56: 24 of cap free
Amount of items: 2
Items: 
Size: 1285 Color: 2
Size: 739 Color: 0

Bin 57: 25 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 1
Size: 522 Color: 3
Size: 86 Color: 3

Bin 58: 27 of cap free
Amount of items: 2
Items: 
Size: 1167 Color: 4
Size: 854 Color: 2

Bin 59: 27 of cap free
Amount of items: 2
Items: 
Size: 1523 Color: 3
Size: 498 Color: 0

Bin 60: 27 of cap free
Amount of items: 2
Items: 
Size: 1527 Color: 0
Size: 494 Color: 4

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 4
Size: 637 Color: 2

Bin 62: 30 of cap free
Amount of items: 2
Items: 
Size: 1579 Color: 0
Size: 439 Color: 4

Bin 63: 31 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 2
Size: 330 Color: 4
Size: 313 Color: 4

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1386 Color: 2
Size: 630 Color: 3

Bin 65: 32 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 3
Size: 351 Color: 4

Bin 66: 1570 of cap free
Amount of items: 9
Items: 
Size: 70 Color: 1
Size: 60 Color: 0
Size: 56 Color: 4
Size: 56 Color: 2
Size: 56 Color: 2
Size: 46 Color: 4
Size: 46 Color: 4
Size: 46 Color: 0
Size: 42 Color: 1

Total size: 133120
Total free space: 2048

