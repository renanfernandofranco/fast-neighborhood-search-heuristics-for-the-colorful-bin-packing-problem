Capicity Bin: 2400
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 0
Size: 802 Color: 4
Size: 160 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 0
Size: 605 Color: 1
Size: 120 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 2
Size: 611 Color: 3
Size: 122 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1920 Color: 0
Size: 242 Color: 2
Size: 238 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 0
Size: 401 Color: 3
Size: 78 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 3
Size: 387 Color: 0
Size: 76 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 4
Size: 338 Color: 0
Size: 64 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 1
Size: 331 Color: 0
Size: 64 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 0
Size: 287 Color: 2
Size: 52 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 3
Size: 298 Color: 1
Size: 60 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 0
Size: 168 Color: 3
Size: 142 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 4
Size: 249 Color: 2
Size: 48 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 4
Size: 192 Color: 2
Size: 98 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 1
Size: 144 Color: 0
Size: 126 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 2
Size: 198 Color: 0
Size: 48 Color: 4

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 2
Size: 1025 Color: 2
Size: 140 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 3
Size: 790 Color: 0
Size: 86 Color: 2

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 0
Size: 678 Color: 1
Size: 166 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 0
Size: 533 Color: 1
Size: 196 Color: 3

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 0
Size: 466 Color: 3
Size: 172 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 2
Size: 554 Color: 3
Size: 40 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1857 Color: 3
Size: 350 Color: 0
Size: 192 Color: 2

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 4
Size: 427 Color: 0
Size: 90 Color: 2

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1958 Color: 4
Size: 441 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 0
Size: 206 Color: 1
Size: 136 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 0
Size: 296 Color: 3
Size: 24 Color: 3

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2049 Color: 3
Size: 302 Color: 4
Size: 48 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2095 Color: 0
Size: 198 Color: 4
Size: 106 Color: 3

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2070 Color: 4
Size: 329 Color: 1

Bin 30: 2 of cap free
Amount of items: 4
Items: 
Size: 1209 Color: 0
Size: 711 Color: 4
Size: 434 Color: 3
Size: 44 Color: 4

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 4
Size: 888 Color: 3
Size: 92 Color: 3

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 2
Size: 301 Color: 0
Size: 255 Color: 4

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1881 Color: 3
Size: 453 Color: 4
Size: 64 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 0
Size: 293 Color: 4
Size: 142 Color: 3

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 1
Size: 365 Color: 0
Size: 72 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 2
Size: 252 Color: 3

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 2041 Color: 4
Size: 356 Color: 2

Bin 38: 4 of cap free
Amount of items: 5
Items: 
Size: 1205 Color: 0
Size: 863 Color: 3
Size: 132 Color: 1
Size: 100 Color: 0
Size: 96 Color: 4

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1399 Color: 4
Size: 997 Color: 2

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 1
Size: 835 Color: 4
Size: 166 Color: 3

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 2
Size: 610 Color: 0
Size: 196 Color: 2

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 0
Size: 497 Color: 4
Size: 122 Color: 4

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 4
Size: 491 Color: 1
Size: 32 Color: 3

Bin 44: 5 of cap free
Amount of items: 5
Items: 
Size: 1202 Color: 0
Size: 825 Color: 3
Size: 226 Color: 2
Size: 86 Color: 4
Size: 56 Color: 4

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 705 Color: 0
Size: 120 Color: 3

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1889 Color: 2
Size: 506 Color: 4

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 4
Size: 719 Color: 1
Size: 32 Color: 2

Bin 48: 7 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 549 Color: 4
Size: 50 Color: 0

Bin 49: 10 of cap free
Amount of items: 2
Items: 
Size: 1659 Color: 1
Size: 731 Color: 2

Bin 50: 10 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 3
Size: 460 Color: 2
Size: 16 Color: 4

Bin 51: 10 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 3
Size: 367 Color: 2
Size: 16 Color: 3

Bin 52: 11 of cap free
Amount of items: 4
Items: 
Size: 1188 Color: 3
Size: 993 Color: 2
Size: 164 Color: 2
Size: 44 Color: 1

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 619 Color: 2
Size: 32 Color: 3

Bin 54: 12 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 2
Size: 983 Color: 4
Size: 40 Color: 0

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1982 Color: 2
Size: 406 Color: 3

Bin 56: 14 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 822 Color: 0
Size: 174 Color: 2

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1547 Color: 1
Size: 839 Color: 2

Bin 58: 14 of cap free
Amount of items: 2
Items: 
Size: 2124 Color: 2
Size: 262 Color: 3

Bin 59: 15 of cap free
Amount of items: 2
Items: 
Size: 1411 Color: 0
Size: 974 Color: 3

Bin 60: 17 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 0
Size: 1002 Color: 2
Size: 160 Color: 4

Bin 61: 19 of cap free
Amount of items: 2
Items: 
Size: 1539 Color: 0
Size: 842 Color: 1

Bin 62: 26 of cap free
Amount of items: 2
Items: 
Size: 1743 Color: 1
Size: 631 Color: 2

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1813 Color: 3
Size: 559 Color: 4

Bin 64: 37 of cap free
Amount of items: 12
Items: 
Size: 433 Color: 4
Size: 382 Color: 3
Size: 332 Color: 3
Size: 283 Color: 0
Size: 269 Color: 0
Size: 146 Color: 3
Size: 112 Color: 1
Size: 108 Color: 2
Size: 84 Color: 4
Size: 84 Color: 4
Size: 72 Color: 1
Size: 58 Color: 0

Bin 65: 39 of cap free
Amount of items: 3
Items: 
Size: 1201 Color: 3
Size: 1052 Color: 1
Size: 108 Color: 0

Bin 66: 2018 of cap free
Amount of items: 6
Items: 
Size: 80 Color: 2
Size: 68 Color: 1
Size: 64 Color: 4
Size: 58 Color: 0
Size: 56 Color: 2
Size: 56 Color: 1

Total size: 156000
Total free space: 2400

