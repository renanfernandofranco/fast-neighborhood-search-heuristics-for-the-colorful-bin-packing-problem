Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 13
Items: 
Size: 9826 Color: 1
Size: 888 Color: 0
Size: 880 Color: 1
Size: 880 Color: 0
Size: 876 Color: 0
Size: 868 Color: 0
Size: 868 Color: 0
Size: 864 Color: 1
Size: 852 Color: 1
Size: 832 Color: 1
Size: 800 Color: 1
Size: 672 Color: 1
Size: 542 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12043 Color: 1
Size: 6339 Color: 1
Size: 1266 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12099 Color: 1
Size: 6333 Color: 1
Size: 1216 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 0
Size: 6720 Color: 0
Size: 604 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 0
Size: 6624 Color: 0
Size: 560 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 1
Size: 5928 Color: 1
Size: 304 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 1
Size: 5360 Color: 0
Size: 480 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 0
Size: 3476 Color: 1
Size: 2064 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 0
Size: 4536 Color: 1
Size: 896 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 1
Size: 3720 Color: 0
Size: 736 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15776 Color: 0
Size: 3168 Color: 1
Size: 704 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15828 Color: 0
Size: 3416 Color: 0
Size: 404 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15871 Color: 0
Size: 3149 Color: 1
Size: 628 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15904 Color: 1
Size: 2144 Color: 0
Size: 1600 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 16180 Color: 0
Size: 2448 Color: 1
Size: 1020 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 16400 Color: 1
Size: 1856 Color: 1
Size: 1392 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 1
Size: 2000 Color: 0
Size: 1216 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 1
Size: 1648 Color: 1
Size: 1280 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16792 Color: 0
Size: 2232 Color: 0
Size: 624 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16740 Color: 1
Size: 2140 Color: 0
Size: 768 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 17176 Color: 1
Size: 1408 Color: 1
Size: 1064 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 17424 Color: 0
Size: 1632 Color: 0
Size: 592 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 17514 Color: 0
Size: 1846 Color: 1
Size: 288 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 17480 Color: 1
Size: 1816 Color: 1
Size: 352 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 17654 Color: 0
Size: 1662 Color: 0
Size: 332 Color: 1

Bin 26: 1 of cap free
Amount of items: 5
Items: 
Size: 10695 Color: 1
Size: 3680 Color: 0
Size: 3680 Color: 0
Size: 920 Color: 1
Size: 672 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 1
Size: 8144 Color: 0
Size: 466 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 1
Size: 6291 Color: 1
Size: 1024 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 15208 Color: 0
Size: 4039 Color: 1
Size: 400 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 15602 Color: 1
Size: 2273 Color: 1
Size: 1772 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 15704 Color: 1
Size: 3433 Color: 0
Size: 510 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 17094 Color: 1
Size: 2553 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 17045 Color: 0
Size: 2602 Color: 1

Bin 34: 2 of cap free
Amount of items: 11
Items: 
Size: 9828 Color: 1
Size: 1152 Color: 1
Size: 1126 Color: 1
Size: 1112 Color: 0
Size: 1104 Color: 0
Size: 1044 Color: 0
Size: 1024 Color: 1
Size: 1024 Color: 0
Size: 1016 Color: 1
Size: 896 Color: 0
Size: 320 Color: 1

Bin 35: 2 of cap free
Amount of items: 8
Items: 
Size: 9836 Color: 1
Size: 1632 Color: 1
Size: 1620 Color: 1
Size: 1600 Color: 1
Size: 1434 Color: 0
Size: 1428 Color: 0
Size: 1408 Color: 0
Size: 688 Color: 0

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 0
Size: 6108 Color: 1

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 0
Size: 4382 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 15414 Color: 1
Size: 3808 Color: 0
Size: 424 Color: 1

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 15536 Color: 0
Size: 4110 Color: 1

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 15710 Color: 0
Size: 3280 Color: 0
Size: 656 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 16094 Color: 0
Size: 3552 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 16348 Color: 0
Size: 1858 Color: 0
Size: 1440 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 0
Size: 2962 Color: 0
Size: 200 Color: 1

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 16910 Color: 1
Size: 2736 Color: 0

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 17354 Color: 0
Size: 2292 Color: 1

Bin 46: 2 of cap free
Amount of items: 4
Items: 
Size: 17550 Color: 0
Size: 1872 Color: 1
Size: 128 Color: 0
Size: 96 Color: 1

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 11093 Color: 0
Size: 8168 Color: 0
Size: 384 Color: 1

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 13728 Color: 0
Size: 5565 Color: 1
Size: 352 Color: 1

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 0
Size: 5053 Color: 1
Size: 356 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 14803 Color: 1
Size: 4330 Color: 0
Size: 512 Color: 1

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 16387 Color: 0
Size: 2810 Color: 0
Size: 448 Color: 1

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 14584 Color: 0
Size: 4544 Color: 1
Size: 516 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 16503 Color: 0
Size: 2993 Color: 1
Size: 148 Color: 1

Bin 54: 5 of cap free
Amount of items: 7
Items: 
Size: 9840 Color: 1
Size: 2355 Color: 1
Size: 1782 Color: 0
Size: 1780 Color: 1
Size: 1750 Color: 0
Size: 1720 Color: 0
Size: 416 Color: 1

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 1
Size: 5889 Color: 1
Size: 2566 Color: 0

Bin 56: 5 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 1
Size: 4399 Color: 0
Size: 424 Color: 1

Bin 57: 5 of cap free
Amount of items: 2
Items: 
Size: 16751 Color: 0
Size: 2892 Color: 1

Bin 58: 6 of cap free
Amount of items: 35
Items: 
Size: 686 Color: 0
Size: 672 Color: 1
Size: 672 Color: 1
Size: 672 Color: 0
Size: 672 Color: 0
Size: 660 Color: 0
Size: 660 Color: 0
Size: 656 Color: 0
Size: 640 Color: 0
Size: 640 Color: 0
Size: 632 Color: 0
Size: 600 Color: 0
Size: 598 Color: 0
Size: 584 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 564 Color: 1
Size: 544 Color: 1
Size: 544 Color: 1
Size: 524 Color: 1
Size: 516 Color: 1
Size: 512 Color: 0
Size: 512 Color: 0
Size: 512 Color: 0
Size: 488 Color: 0
Size: 482 Color: 0
Size: 480 Color: 0
Size: 464 Color: 1
Size: 456 Color: 1
Size: 456 Color: 1
Size: 448 Color: 1
Size: 400 Color: 1
Size: 392 Color: 0

Bin 59: 6 of cap free
Amount of items: 3
Items: 
Size: 11744 Color: 0
Size: 7162 Color: 1
Size: 736 Color: 1

Bin 60: 6 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 1
Size: 7704 Color: 0
Size: 368 Color: 1

Bin 61: 6 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 1
Size: 7202 Color: 0

Bin 62: 6 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 0
Size: 5556 Color: 1
Size: 832 Color: 1

Bin 63: 6 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 1
Size: 4860 Color: 0
Size: 1412 Color: 0

Bin 64: 6 of cap free
Amount of items: 3
Items: 
Size: 13424 Color: 1
Size: 4960 Color: 0
Size: 1258 Color: 0

Bin 65: 6 of cap free
Amount of items: 2
Items: 
Size: 16570 Color: 1
Size: 3072 Color: 0

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 16674 Color: 0
Size: 2968 Color: 1

Bin 67: 7 of cap free
Amount of items: 3
Items: 
Size: 11122 Color: 1
Size: 8183 Color: 0
Size: 336 Color: 0

Bin 68: 7 of cap free
Amount of items: 2
Items: 
Size: 13641 Color: 1
Size: 6000 Color: 0

Bin 69: 7 of cap free
Amount of items: 2
Items: 
Size: 16585 Color: 1
Size: 3056 Color: 0

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 17226 Color: 0
Size: 2415 Color: 1

Bin 71: 8 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 0
Size: 8160 Color: 1
Size: 454 Color: 0

Bin 72: 8 of cap free
Amount of items: 3
Items: 
Size: 11058 Color: 1
Size: 8182 Color: 0
Size: 400 Color: 1

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 0
Size: 6384 Color: 1
Size: 720 Color: 1

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 15728 Color: 1
Size: 3512 Color: 1
Size: 400 Color: 0

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 16984 Color: 0
Size: 2656 Color: 1

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 17680 Color: 1
Size: 1960 Color: 0

Bin 77: 10 of cap free
Amount of items: 5
Items: 
Size: 9904 Color: 1
Size: 4016 Color: 1
Size: 2130 Color: 0
Size: 1952 Color: 0
Size: 1636 Color: 1

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 14430 Color: 1
Size: 5208 Color: 0

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 17552 Color: 0
Size: 2022 Color: 1
Size: 64 Color: 0

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 1
Size: 7131 Color: 0

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 0
Size: 3677 Color: 1

Bin 82: 11 of cap free
Amount of items: 2
Items: 
Size: 17016 Color: 0
Size: 2621 Color: 1

Bin 83: 12 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 1
Size: 7044 Color: 0

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 16608 Color: 0
Size: 3028 Color: 1

Bin 85: 12 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 1
Size: 2594 Color: 0

Bin 86: 12 of cap free
Amount of items: 2
Items: 
Size: 17244 Color: 0
Size: 2392 Color: 1

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 17396 Color: 0
Size: 2240 Color: 1

Bin 88: 13 of cap free
Amount of items: 3
Items: 
Size: 14816 Color: 1
Size: 4299 Color: 0
Size: 520 Color: 0

Bin 89: 13 of cap free
Amount of items: 2
Items: 
Size: 15119 Color: 0
Size: 4516 Color: 1

Bin 90: 14 of cap free
Amount of items: 3
Items: 
Size: 11006 Color: 1
Size: 8180 Color: 0
Size: 448 Color: 0

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 12448 Color: 0
Size: 7186 Color: 1

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 0
Size: 6082 Color: 1

Bin 93: 15 of cap free
Amount of items: 3
Items: 
Size: 14049 Color: 0
Size: 4880 Color: 1
Size: 704 Color: 1

Bin 94: 17 of cap free
Amount of items: 2
Items: 
Size: 12579 Color: 0
Size: 7052 Color: 1

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 17202 Color: 0
Size: 2428 Color: 1

Bin 96: 19 of cap free
Amount of items: 2
Items: 
Size: 12988 Color: 1
Size: 6641 Color: 0

Bin 97: 20 of cap free
Amount of items: 3
Items: 
Size: 14112 Color: 0
Size: 4620 Color: 1
Size: 896 Color: 1

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 15678 Color: 0
Size: 3950 Color: 1

Bin 99: 20 of cap free
Amount of items: 2
Items: 
Size: 16022 Color: 0
Size: 3606 Color: 1

Bin 100: 21 of cap free
Amount of items: 2
Items: 
Size: 16908 Color: 0
Size: 2719 Color: 1

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 0
Size: 5330 Color: 1

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 16344 Color: 0
Size: 3282 Color: 1

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 17638 Color: 0
Size: 1988 Color: 1

Bin 104: 23 of cap free
Amount of items: 3
Items: 
Size: 16245 Color: 0
Size: 3188 Color: 1
Size: 192 Color: 1

Bin 105: 23 of cap free
Amount of items: 2
Items: 
Size: 16921 Color: 1
Size: 2704 Color: 0

Bin 106: 24 of cap free
Amount of items: 2
Items: 
Size: 13820 Color: 0
Size: 5804 Color: 1

Bin 107: 24 of cap free
Amount of items: 2
Items: 
Size: 16864 Color: 1
Size: 2760 Color: 0

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 17582 Color: 0
Size: 2042 Color: 1

Bin 109: 26 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 1
Size: 7638 Color: 0
Size: 744 Color: 0

Bin 110: 26 of cap free
Amount of items: 2
Items: 
Size: 15898 Color: 0
Size: 3724 Color: 1

Bin 111: 27 of cap free
Amount of items: 2
Items: 
Size: 12887 Color: 0
Size: 6734 Color: 1

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 12474 Color: 1
Size: 7146 Color: 0

Bin 113: 28 of cap free
Amount of items: 2
Items: 
Size: 16246 Color: 0
Size: 3374 Color: 1

Bin 114: 28 of cap free
Amount of items: 2
Items: 
Size: 17268 Color: 1
Size: 2352 Color: 0

Bin 115: 29 of cap free
Amount of items: 14
Items: 
Size: 9825 Color: 1
Size: 864 Color: 0
Size: 858 Color: 0
Size: 832 Color: 0
Size: 806 Color: 0
Size: 788 Color: 0
Size: 784 Color: 1
Size: 734 Color: 0
Size: 704 Color: 1
Size: 704 Color: 1
Size: 704 Color: 1
Size: 704 Color: 1
Size: 704 Color: 0
Size: 608 Color: 1

Bin 116: 29 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 1
Size: 5128 Color: 0

Bin 117: 29 of cap free
Amount of items: 2
Items: 
Size: 15147 Color: 0
Size: 4472 Color: 1

Bin 118: 30 of cap free
Amount of items: 2
Items: 
Size: 16538 Color: 0
Size: 3080 Color: 1

Bin 119: 30 of cap free
Amount of items: 2
Items: 
Size: 17418 Color: 0
Size: 2200 Color: 1

Bin 120: 31 of cap free
Amount of items: 2
Items: 
Size: 13585 Color: 0
Size: 6032 Color: 1

Bin 121: 32 of cap free
Amount of items: 2
Items: 
Size: 12288 Color: 1
Size: 7328 Color: 0

Bin 122: 32 of cap free
Amount of items: 2
Items: 
Size: 13568 Color: 1
Size: 6048 Color: 0

Bin 123: 32 of cap free
Amount of items: 2
Items: 
Size: 14456 Color: 0
Size: 5160 Color: 1

Bin 124: 32 of cap free
Amount of items: 2
Items: 
Size: 15520 Color: 0
Size: 4096 Color: 1

Bin 125: 32 of cap free
Amount of items: 4
Items: 
Size: 17524 Color: 0
Size: 1862 Color: 1
Size: 128 Color: 0
Size: 102 Color: 1

Bin 126: 33 of cap free
Amount of items: 2
Items: 
Size: 11090 Color: 1
Size: 8525 Color: 0

Bin 127: 34 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 1
Size: 3126 Color: 0

Bin 128: 34 of cap free
Amount of items: 3
Items: 
Size: 17668 Color: 0
Size: 1914 Color: 1
Size: 32 Color: 1

Bin 129: 35 of cap free
Amount of items: 2
Items: 
Size: 15180 Color: 1
Size: 4433 Color: 0

Bin 130: 36 of cap free
Amount of items: 4
Items: 
Size: 17632 Color: 0
Size: 1884 Color: 1
Size: 64 Color: 0
Size: 32 Color: 1

Bin 131: 38 of cap free
Amount of items: 2
Items: 
Size: 17328 Color: 1
Size: 2282 Color: 0

Bin 132: 39 of cap free
Amount of items: 2
Items: 
Size: 14942 Color: 1
Size: 4667 Color: 0

Bin 133: 39 of cap free
Amount of items: 2
Items: 
Size: 16057 Color: 1
Size: 3552 Color: 0

Bin 134: 40 of cap free
Amount of items: 3
Items: 
Size: 16848 Color: 1
Size: 2592 Color: 0
Size: 168 Color: 0

Bin 135: 40 of cap free
Amount of items: 2
Items: 
Size: 17536 Color: 1
Size: 2072 Color: 0

Bin 136: 44 of cap free
Amount of items: 3
Items: 
Size: 15484 Color: 1
Size: 3864 Color: 0
Size: 256 Color: 0

Bin 137: 46 of cap free
Amount of items: 2
Items: 
Size: 16958 Color: 0
Size: 2644 Color: 1

Bin 138: 50 of cap free
Amount of items: 2
Items: 
Size: 16288 Color: 0
Size: 3310 Color: 1

Bin 139: 50 of cap free
Amount of items: 2
Items: 
Size: 17422 Color: 1
Size: 2176 Color: 0

Bin 140: 52 of cap free
Amount of items: 5
Items: 
Size: 9888 Color: 0
Size: 4016 Color: 1
Size: 2128 Color: 0
Size: 1886 Color: 0
Size: 1678 Color: 1

Bin 141: 52 of cap free
Amount of items: 2
Items: 
Size: 11408 Color: 0
Size: 8188 Color: 1

Bin 142: 52 of cap free
Amount of items: 2
Items: 
Size: 17312 Color: 0
Size: 2284 Color: 1

Bin 143: 52 of cap free
Amount of items: 2
Items: 
Size: 17592 Color: 1
Size: 2004 Color: 0

Bin 144: 53 of cap free
Amount of items: 2
Items: 
Size: 15237 Color: 0
Size: 4358 Color: 1

Bin 145: 56 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 0
Size: 8176 Color: 1
Size: 220 Color: 1

Bin 146: 56 of cap free
Amount of items: 2
Items: 
Size: 15832 Color: 0
Size: 3760 Color: 1

Bin 147: 60 of cap free
Amount of items: 2
Items: 
Size: 12127 Color: 0
Size: 7461 Color: 1

Bin 148: 60 of cap free
Amount of items: 2
Items: 
Size: 15560 Color: 1
Size: 4028 Color: 0

Bin 149: 61 of cap free
Amount of items: 2
Items: 
Size: 12971 Color: 0
Size: 6616 Color: 1

Bin 150: 62 of cap free
Amount of items: 2
Items: 
Size: 14114 Color: 1
Size: 5472 Color: 0

Bin 151: 62 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 0
Size: 5234 Color: 1

Bin 152: 63 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 0
Size: 7177 Color: 1

Bin 153: 64 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 0
Size: 4240 Color: 1

Bin 154: 64 of cap free
Amount of items: 2
Items: 
Size: 15392 Color: 1
Size: 4192 Color: 0

Bin 155: 68 of cap free
Amount of items: 2
Items: 
Size: 12684 Color: 1
Size: 6896 Color: 0

Bin 156: 68 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 0
Size: 6008 Color: 1

Bin 157: 69 of cap free
Amount of items: 2
Items: 
Size: 16823 Color: 0
Size: 2756 Color: 1

Bin 158: 70 of cap free
Amount of items: 3
Items: 
Size: 16020 Color: 1
Size: 3440 Color: 0
Size: 118 Color: 0

Bin 159: 72 of cap free
Amount of items: 2
Items: 
Size: 17120 Color: 0
Size: 2456 Color: 1

Bin 160: 74 of cap free
Amount of items: 2
Items: 
Size: 16956 Color: 1
Size: 2618 Color: 0

Bin 161: 82 of cap free
Amount of items: 2
Items: 
Size: 15216 Color: 0
Size: 4350 Color: 1

Bin 162: 82 of cap free
Amount of items: 2
Items: 
Size: 16278 Color: 1
Size: 3288 Color: 0

Bin 163: 84 of cap free
Amount of items: 7
Items: 
Size: 9848 Color: 0
Size: 1696 Color: 0
Size: 1632 Color: 1
Size: 1632 Color: 1
Size: 1632 Color: 1
Size: 1632 Color: 0
Size: 1492 Color: 0

Bin 164: 86 of cap free
Amount of items: 2
Items: 
Size: 12570 Color: 0
Size: 6992 Color: 1

Bin 165: 88 of cap free
Amount of items: 2
Items: 
Size: 16712 Color: 1
Size: 2848 Color: 0

Bin 166: 88 of cap free
Amount of items: 2
Items: 
Size: 17386 Color: 0
Size: 2174 Color: 1

Bin 167: 90 of cap free
Amount of items: 2
Items: 
Size: 14454 Color: 0
Size: 5104 Color: 1

Bin 168: 90 of cap free
Amount of items: 2
Items: 
Size: 15136 Color: 1
Size: 4422 Color: 0

Bin 169: 90 of cap free
Amount of items: 2
Items: 
Size: 17104 Color: 1
Size: 2454 Color: 0

Bin 170: 94 of cap free
Amount of items: 2
Items: 
Size: 15322 Color: 0
Size: 4232 Color: 1

Bin 171: 96 of cap free
Amount of items: 2
Items: 
Size: 14342 Color: 0
Size: 5210 Color: 1

Bin 172: 96 of cap free
Amount of items: 2
Items: 
Size: 16530 Color: 1
Size: 3022 Color: 0

Bin 173: 98 of cap free
Amount of items: 2
Items: 
Size: 14910 Color: 1
Size: 4640 Color: 0

Bin 174: 102 of cap free
Amount of items: 2
Items: 
Size: 13506 Color: 1
Size: 6040 Color: 0

Bin 175: 102 of cap free
Amount of items: 2
Items: 
Size: 16016 Color: 0
Size: 3530 Color: 1

Bin 176: 102 of cap free
Amount of items: 2
Items: 
Size: 17304 Color: 1
Size: 2242 Color: 0

Bin 177: 104 of cap free
Amount of items: 2
Items: 
Size: 14422 Color: 1
Size: 5122 Color: 0

Bin 178: 104 of cap free
Amount of items: 2
Items: 
Size: 14576 Color: 1
Size: 4968 Color: 0

Bin 179: 104 of cap free
Amount of items: 2
Items: 
Size: 16706 Color: 1
Size: 2838 Color: 0

Bin 180: 109 of cap free
Amount of items: 2
Items: 
Size: 17084 Color: 0
Size: 2455 Color: 1

Bin 181: 120 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 0
Size: 7808 Color: 1

Bin 182: 132 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 1
Size: 6732 Color: 0

Bin 183: 156 of cap free
Amount of items: 2
Items: 
Size: 14878 Color: 1
Size: 4614 Color: 0

Bin 184: 164 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 0
Size: 5635 Color: 1
Size: 2937 Color: 1

Bin 185: 164 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 1
Size: 5094 Color: 0

Bin 186: 164 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 0
Size: 4620 Color: 1

Bin 187: 166 of cap free
Amount of items: 2
Items: 
Size: 11280 Color: 1
Size: 8202 Color: 0

Bin 188: 174 of cap free
Amount of items: 2
Items: 
Size: 13152 Color: 1
Size: 6322 Color: 0

Bin 189: 186 of cap free
Amount of items: 8
Items: 
Size: 9832 Color: 1
Size: 1436 Color: 1
Size: 1424 Color: 1
Size: 1408 Color: 0
Size: 1376 Color: 0
Size: 1344 Color: 0
Size: 1344 Color: 0
Size: 1298 Color: 1

Bin 190: 192 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 1
Size: 8184 Color: 0

Bin 191: 196 of cap free
Amount of items: 2
Items: 
Size: 14860 Color: 1
Size: 4592 Color: 0

Bin 192: 197 of cap free
Amount of items: 2
Items: 
Size: 15529 Color: 1
Size: 3922 Color: 0

Bin 193: 210 of cap free
Amount of items: 9
Items: 
Size: 9834 Color: 0
Size: 1312 Color: 0
Size: 1200 Color: 1
Size: 1200 Color: 1
Size: 1188 Color: 0
Size: 1184 Color: 1
Size: 1184 Color: 0
Size: 1176 Color: 1
Size: 1160 Color: 0

Bin 194: 220 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 0
Size: 5954 Color: 1

Bin 195: 221 of cap free
Amount of items: 11
Items: 
Size: 9829 Color: 0
Size: 1010 Color: 0
Size: 1008 Color: 0
Size: 992 Color: 0
Size: 968 Color: 1
Size: 964 Color: 1
Size: 960 Color: 1
Size: 960 Color: 0
Size: 932 Color: 1
Size: 920 Color: 0
Size: 884 Color: 1

Bin 196: 250 of cap free
Amount of items: 2
Items: 
Size: 13496 Color: 1
Size: 5902 Color: 0

Bin 197: 252 of cap free
Amount of items: 2
Items: 
Size: 14328 Color: 0
Size: 5068 Color: 1

Bin 198: 282 of cap free
Amount of items: 2
Items: 
Size: 12350 Color: 1
Size: 7016 Color: 0

Bin 199: 11230 of cap free
Amount of items: 22
Items: 
Size: 480 Color: 0
Size: 470 Color: 0
Size: 448 Color: 1
Size: 432 Color: 1
Size: 432 Color: 1
Size: 432 Color: 0
Size: 384 Color: 1
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 380 Color: 1
Size: 376 Color: 1
Size: 368 Color: 0
Size: 368 Color: 0
Size: 352 Color: 1
Size: 352 Color: 1
Size: 352 Color: 1
Size: 348 Color: 1
Size: 332 Color: 0
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0

Total size: 3890304
Total free space: 19648

