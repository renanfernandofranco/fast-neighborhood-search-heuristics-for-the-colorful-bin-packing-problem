Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 7
Size: 267 Color: 6
Size: 260 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 5
Size: 261 Color: 0
Size: 252 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 11
Size: 356 Color: 14
Size: 281 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 13
Size: 353 Color: 0
Size: 283 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 8
Size: 276 Color: 14
Size: 257 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 17
Size: 298 Color: 19
Size: 282 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 267 Color: 1
Size: 266 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 6
Size: 295 Color: 0
Size: 279 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 15
Size: 267 Color: 14
Size: 260 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 17
Size: 277 Color: 0
Size: 266 Color: 10

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 14
Size: 301 Color: 14
Size: 300 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 6
Size: 326 Color: 1
Size: 281 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 8
Size: 324 Color: 13
Size: 253 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 301 Color: 14
Size: 280 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 276 Color: 6
Size: 268 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 339 Color: 2
Size: 283 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 5
Size: 257 Color: 13
Size: 253 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 17
Size: 365 Color: 2
Size: 251 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 288 Color: 4
Size: 278 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 357 Color: 7
Size: 250 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 17
Size: 322 Color: 18
Size: 257 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 14
Size: 252 Color: 7
Size: 250 Color: 16

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 10
Size: 280 Color: 18
Size: 277 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 17
Size: 254 Color: 4
Size: 252 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 17
Size: 270 Color: 13
Size: 262 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 362 Color: 10
Size: 268 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 7
Size: 323 Color: 8
Size: 261 Color: 8

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 17
Size: 300 Color: 17
Size: 279 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 308 Color: 2
Size: 282 Color: 17

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 8
Size: 288 Color: 1
Size: 256 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 18
Size: 300 Color: 13
Size: 253 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 7
Size: 329 Color: 11
Size: 311 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 314 Color: 1
Size: 281 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 13
Size: 352 Color: 19
Size: 295 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 6
Size: 302 Color: 3
Size: 253 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 5
Size: 273 Color: 14
Size: 251 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 9
Size: 336 Color: 3
Size: 278 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 300 Color: 10
Size: 252 Color: 7

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 9
Size: 275 Color: 7
Size: 259 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 18
Size: 345 Color: 5
Size: 301 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 11
Size: 255 Color: 19
Size: 252 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 19
Size: 305 Color: 1
Size: 291 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 323 Color: 7
Size: 250 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 265 Color: 7
Size: 255 Color: 6

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 15
Size: 334 Color: 5
Size: 278 Color: 13

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 5
Size: 304 Color: 1
Size: 269 Color: 10

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 317 Color: 11
Size: 291 Color: 9

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 336 Color: 13
Size: 260 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 17
Size: 313 Color: 4
Size: 274 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 6
Size: 316 Color: 7
Size: 271 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 19
Size: 325 Color: 12
Size: 274 Color: 13

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 8
Size: 298 Color: 0
Size: 262 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 278 Color: 11
Size: 254 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 12
Size: 265 Color: 1
Size: 261 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 5
Size: 293 Color: 9
Size: 277 Color: 8

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 19
Size: 339 Color: 0
Size: 279 Color: 5

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 298 Color: 17
Size: 287 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 3
Size: 319 Color: 19
Size: 317 Color: 12

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 295 Color: 10
Size: 265 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 18
Size: 251 Color: 8
Size: 250 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 13
Size: 349 Color: 16
Size: 263 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 18
Size: 360 Color: 11
Size: 265 Color: 14

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 304 Color: 1
Size: 288 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 305 Color: 2
Size: 298 Color: 10

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 5
Size: 272 Color: 19
Size: 258 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 16
Size: 337 Color: 11
Size: 289 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7
Size: 307 Color: 9
Size: 306 Color: 5

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 14
Size: 313 Color: 7
Size: 273 Color: 8

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 10
Size: 368 Color: 18
Size: 259 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9
Size: 271 Color: 3
Size: 262 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 258 Color: 3
Size: 256 Color: 19

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 5
Size: 352 Color: 1
Size: 250 Color: 12

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 318 Color: 12
Size: 278 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 14
Size: 250 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 286 Color: 9
Size: 285 Color: 15

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 307 Color: 18
Size: 296 Color: 12

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 16
Size: 323 Color: 6
Size: 309 Color: 9

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 274 Color: 3
Size: 262 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 15
Size: 304 Color: 5
Size: 283 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 270 Color: 7
Size: 251 Color: 13

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 9
Size: 264 Color: 2
Size: 256 Color: 11

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 5
Size: 300 Color: 7
Size: 293 Color: 16

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 2
Size: 265 Color: 1
Size: 253 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9
Size: 279 Color: 17
Size: 263 Color: 14

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 15
Size: 337 Color: 18
Size: 269 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 325 Color: 14
Size: 257 Color: 10

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 327 Color: 5
Size: 305 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 18
Size: 263 Color: 9
Size: 263 Color: 9

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 259 Color: 14
Size: 254 Color: 2

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 362 Color: 7
Size: 258 Color: 17

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 286 Color: 18
Size: 266 Color: 4

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 7
Size: 339 Color: 16
Size: 312 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 279 Color: 8
Size: 256 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 15
Size: 352 Color: 2
Size: 271 Color: 17

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 13
Size: 334 Color: 16
Size: 314 Color: 16

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 10
Size: 275 Color: 18
Size: 274 Color: 5

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 5
Size: 251 Color: 5

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 18
Size: 344 Color: 17
Size: 253 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 323 Color: 18
Size: 250 Color: 19

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 19
Size: 296 Color: 1
Size: 252 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 19
Size: 318 Color: 13
Size: 301 Color: 8

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 289 Color: 10
Size: 259 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 297 Color: 5
Size: 276 Color: 7

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 18
Size: 258 Color: 1
Size: 252 Color: 8

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 16
Size: 312 Color: 6
Size: 274 Color: 14

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 296 Color: 9
Size: 276 Color: 11

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 7
Size: 252 Color: 18
Size: 251 Color: 16

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 13
Size: 299 Color: 17
Size: 271 Color: 16

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 6
Size: 318 Color: 13
Size: 283 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 17
Size: 345 Color: 19
Size: 288 Color: 19

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 7
Size: 282 Color: 2
Size: 251 Color: 13

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 14
Size: 310 Color: 11
Size: 281 Color: 2

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 264 Color: 7
Size: 257 Color: 14

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 13
Size: 359 Color: 9
Size: 251 Color: 12

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 375 Color: 2
Size: 250 Color: 18

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 16
Size: 340 Color: 11
Size: 261 Color: 18

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 14
Size: 324 Color: 11
Size: 252 Color: 19

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 15
Size: 358 Color: 19
Size: 278 Color: 10

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 12
Size: 341 Color: 14
Size: 300 Color: 5

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 312 Color: 17
Size: 271 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 11
Size: 330 Color: 11
Size: 289 Color: 6

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 13
Size: 356 Color: 15
Size: 256 Color: 12

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 334 Color: 4
Size: 283 Color: 18

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 8
Size: 350 Color: 9
Size: 287 Color: 2

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 13
Size: 299 Color: 2
Size: 261 Color: 13

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 19
Size: 286 Color: 15
Size: 275 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 13
Size: 352 Color: 7
Size: 274 Color: 12

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 18
Size: 256 Color: 14
Size: 253 Color: 12

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 14
Size: 355 Color: 3
Size: 252 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 5
Size: 272 Color: 11
Size: 251 Color: 13

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 19
Size: 321 Color: 2
Size: 254 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 314 Color: 19
Size: 251 Color: 9

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 337 Color: 9
Size: 267 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 16
Size: 277 Color: 4
Size: 264 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 310 Color: 11
Size: 264 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 362 Color: 17
Size: 262 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 8
Size: 326 Color: 18
Size: 298 Color: 11

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 5
Size: 265 Color: 10
Size: 257 Color: 14

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 372 Color: 9
Size: 252 Color: 19

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 1
Size: 337 Color: 9
Size: 317 Color: 6

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 274 Color: 16
Size: 261 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 12
Size: 295 Color: 8
Size: 250 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 16
Size: 367 Color: 3
Size: 257 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 16
Size: 264 Color: 6
Size: 252 Color: 2

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 5
Size: 313 Color: 13
Size: 261 Color: 17

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 332 Color: 18
Size: 273 Color: 2

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 11
Size: 292 Color: 5
Size: 260 Color: 15

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 7
Size: 357 Color: 1
Size: 279 Color: 16

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 12
Size: 324 Color: 9
Size: 274 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 16
Size: 331 Color: 8
Size: 291 Color: 18

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 17
Size: 307 Color: 17
Size: 304 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 5
Size: 283 Color: 16
Size: 282 Color: 1

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 10
Size: 329 Color: 14
Size: 311 Color: 3

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 18
Size: 281 Color: 19
Size: 265 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 19
Size: 322 Color: 10
Size: 314 Color: 6

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 15
Size: 341 Color: 10
Size: 250 Color: 7

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 10
Size: 326 Color: 1
Size: 285 Color: 8

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 339 Color: 5
Size: 301 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 10
Size: 262 Color: 15
Size: 252 Color: 13

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 0
Size: 274 Color: 7
Size: 258 Color: 17

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 12
Size: 277 Color: 8
Size: 258 Color: 16

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 12
Size: 285 Color: 17
Size: 274 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 5
Size: 358 Color: 11
Size: 280 Color: 5

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 16
Size: 308 Color: 15
Size: 287 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 344 Color: 10
Size: 287 Color: 18

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 11
Size: 252 Color: 6
Size: 250 Color: 13

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 13
Size: 339 Color: 13
Size: 279 Color: 8

Total size: 167000
Total free space: 0

