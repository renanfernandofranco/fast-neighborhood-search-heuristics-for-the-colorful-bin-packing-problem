Capicity Bin: 2328
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 22
Items: 
Size: 166 Color: 0
Size: 164 Color: 0
Size: 152 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0
Size: 124 Color: 1
Size: 120 Color: 0
Size: 110 Color: 0
Size: 106 Color: 0
Size: 104 Color: 1
Size: 104 Color: 1
Size: 104 Color: 0
Size: 90 Color: 1
Size: 76 Color: 1
Size: 76 Color: 1
Size: 76 Color: 1
Size: 76 Color: 1
Size: 68 Color: 1
Size: 66 Color: 1
Size: 66 Color: 0
Size: 48 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 0
Size: 654 Color: 0
Size: 128 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 0
Size: 578 Color: 0
Size: 40 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 341 Color: 0
Size: 188 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 0
Size: 478 Color: 1
Size: 36 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 0
Size: 246 Color: 0
Size: 184 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 1
Size: 275 Color: 1
Size: 128 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 302 Color: 1
Size: 56 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 1
Size: 291 Color: 1
Size: 58 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 0
Size: 210 Color: 1
Size: 96 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 0
Size: 176 Color: 1
Size: 94 Color: 1

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 2094 Color: 1
Size: 202 Color: 0
Size: 28 Color: 1
Size: 4 Color: 0

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 1
Size: 926 Color: 0
Size: 66 Color: 1

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 1
Size: 829 Color: 0
Size: 52 Color: 1

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 0
Size: 645 Color: 0
Size: 108 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 0
Size: 533 Color: 1
Size: 128 Color: 1

Bin 17: 1 of cap free
Amount of items: 4
Items: 
Size: 1689 Color: 1
Size: 554 Color: 0
Size: 44 Color: 0
Size: 40 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 1
Size: 441 Color: 1
Size: 192 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 1
Size: 459 Color: 1
Size: 90 Color: 0

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1861 Color: 0
Size: 466 Color: 1

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1929 Color: 0
Size: 398 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 0
Size: 333 Color: 1
Size: 40 Color: 0

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 0
Size: 850 Color: 0
Size: 106 Color: 1

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 0
Size: 864 Color: 0
Size: 56 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 0
Size: 469 Color: 1
Size: 192 Color: 0

Bin 26: 2 of cap free
Amount of items: 4
Items: 
Size: 2086 Color: 1
Size: 192 Color: 0
Size: 28 Color: 1
Size: 20 Color: 0

Bin 27: 3 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 1
Size: 886 Color: 0
Size: 112 Color: 1

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 1
Size: 770 Color: 1
Size: 88 Color: 0

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 723 Color: 0
Size: 48 Color: 1

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1854 Color: 0
Size: 471 Color: 1

Bin 31: 4 of cap free
Amount of items: 3
Items: 
Size: 1864 Color: 1
Size: 262 Color: 0
Size: 198 Color: 1

Bin 32: 5 of cap free
Amount of items: 5
Items: 
Size: 1165 Color: 1
Size: 448 Color: 1
Size: 362 Color: 0
Size: 258 Color: 0
Size: 90 Color: 1

Bin 33: 5 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 1
Size: 565 Color: 0

Bin 34: 5 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 0
Size: 381 Color: 1

Bin 35: 5 of cap free
Amount of items: 2
Items: 
Size: 1999 Color: 1
Size: 324 Color: 0

Bin 36: 6 of cap free
Amount of items: 4
Items: 
Size: 1693 Color: 1
Size: 561 Color: 0
Size: 40 Color: 1
Size: 28 Color: 0

Bin 37: 6 of cap free
Amount of items: 2
Items: 
Size: 1871 Color: 0
Size: 451 Color: 1

Bin 38: 6 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 1
Size: 288 Color: 0

Bin 39: 7 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 0
Size: 947 Color: 0
Size: 156 Color: 1

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 0
Size: 631 Color: 1

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 0
Size: 538 Color: 1

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 1787 Color: 1
Size: 534 Color: 0

Bin 43: 8 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 1
Size: 462 Color: 0

Bin 44: 9 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 0
Size: 646 Color: 1

Bin 45: 10 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 0
Size: 553 Color: 1

Bin 46: 10 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 0
Size: 323 Color: 1
Size: 54 Color: 0

Bin 47: 12 of cap free
Amount of items: 2
Items: 
Size: 1779 Color: 0
Size: 537 Color: 1

Bin 48: 12 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 1
Size: 322 Color: 0

Bin 49: 13 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 802 Color: 1
Size: 52 Color: 0

Bin 50: 13 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 0
Size: 630 Color: 1

Bin 51: 13 of cap free
Amount of items: 2
Items: 
Size: 1921 Color: 1
Size: 394 Color: 0

Bin 52: 14 of cap free
Amount of items: 2
Items: 
Size: 1573 Color: 0
Size: 741 Color: 1

Bin 53: 15 of cap free
Amount of items: 2
Items: 
Size: 1922 Color: 0
Size: 391 Color: 1

Bin 54: 16 of cap free
Amount of items: 5
Items: 
Size: 1152 Color: 1
Size: 348 Color: 0
Size: 346 Color: 0
Size: 342 Color: 0
Size: 124 Color: 1

Bin 55: 16 of cap free
Amount of items: 2
Items: 
Size: 2014 Color: 1
Size: 298 Color: 0

Bin 56: 17 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 0
Size: 337 Color: 1

Bin 57: 18 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 0
Size: 531 Color: 1
Size: 128 Color: 1

Bin 58: 18 of cap free
Amount of items: 2
Items: 
Size: 1855 Color: 1
Size: 455 Color: 0

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 2078 Color: 0
Size: 226 Color: 1

Bin 60: 33 of cap free
Amount of items: 2
Items: 
Size: 1325 Color: 0
Size: 970 Color: 1

Bin 61: 34 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 1
Size: 971 Color: 0
Size: 156 Color: 1

Bin 62: 35 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 1
Size: 738 Color: 0

Bin 63: 37 of cap free
Amount of items: 2
Items: 
Size: 1322 Color: 0
Size: 969 Color: 1

Bin 64: 45 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 1
Size: 835 Color: 0
Size: 282 Color: 0

Bin 65: 46 of cap free
Amount of items: 2
Items: 
Size: 1266 Color: 0
Size: 1016 Color: 1

Bin 66: 1760 of cap free
Amount of items: 8
Items: 
Size: 92 Color: 0
Size: 92 Color: 0
Size: 88 Color: 0
Size: 64 Color: 1
Size: 64 Color: 1
Size: 64 Color: 1
Size: 56 Color: 1
Size: 48 Color: 0

Total size: 151320
Total free space: 2328

