Capicity Bin: 8400
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 4
Size: 3502 Color: 3
Size: 696 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 1
Size: 3916 Color: 0
Size: 280 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4783 Color: 2
Size: 3015 Color: 0
Size: 602 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 4
Size: 2668 Color: 0
Size: 528 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 3
Size: 2436 Color: 0
Size: 360 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5724 Color: 4
Size: 2236 Color: 0
Size: 440 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5962 Color: 4
Size: 2290 Color: 0
Size: 148 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6076 Color: 0
Size: 2034 Color: 4
Size: 290 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6100 Color: 3
Size: 1534 Color: 0
Size: 766 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 2
Size: 2022 Color: 2
Size: 182 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6290 Color: 3
Size: 1810 Color: 0
Size: 300 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6476 Color: 4
Size: 1734 Color: 4
Size: 190 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 3
Size: 1451 Color: 1
Size: 320 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 4
Size: 1532 Color: 1
Size: 150 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6800 Color: 1
Size: 1200 Color: 4
Size: 400 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 0
Size: 1301 Color: 3
Size: 260 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 3
Size: 1108 Color: 0
Size: 376 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 2
Size: 1214 Color: 0
Size: 240 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6977 Color: 0
Size: 1187 Color: 4
Size: 236 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7063 Color: 0
Size: 1061 Color: 4
Size: 276 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7073 Color: 4
Size: 943 Color: 0
Size: 384 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 0
Size: 698 Color: 4
Size: 552 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7124 Color: 1
Size: 924 Color: 0
Size: 352 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 0
Size: 812 Color: 1
Size: 344 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 0
Size: 849 Color: 4
Size: 304 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7189 Color: 3
Size: 1011 Color: 0
Size: 200 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7253 Color: 0
Size: 823 Color: 4
Size: 324 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7218 Color: 1
Size: 986 Color: 0
Size: 196 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 4
Size: 726 Color: 1
Size: 440 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7249 Color: 4
Size: 863 Color: 0
Size: 288 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7349 Color: 0
Size: 795 Color: 1
Size: 256 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 1
Size: 926 Color: 0
Size: 184 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7356 Color: 4
Size: 804 Color: 0
Size: 240 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7370 Color: 1
Size: 874 Color: 0
Size: 156 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7383 Color: 1
Size: 755 Color: 4
Size: 262 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7444 Color: 0
Size: 764 Color: 4
Size: 192 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7404 Color: 1
Size: 794 Color: 3
Size: 202 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7450 Color: 0
Size: 754 Color: 2
Size: 196 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7447 Color: 1
Size: 781 Color: 4
Size: 172 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7482 Color: 2
Size: 512 Color: 0
Size: 406 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7556 Color: 3
Size: 698 Color: 2
Size: 146 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5391 Color: 1
Size: 2788 Color: 0
Size: 220 Color: 1

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5633 Color: 4
Size: 2302 Color: 3
Size: 464 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5957 Color: 1
Size: 2284 Color: 0
Size: 158 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 1
Size: 1804 Color: 3
Size: 156 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 2
Size: 1546 Color: 3
Size: 192 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 0
Size: 957 Color: 0
Size: 696 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6823 Color: 0
Size: 1404 Color: 0
Size: 172 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 2
Size: 1213 Color: 0
Size: 304 Color: 4

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 0
Size: 1331 Color: 1
Size: 112 Color: 3

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 1
Size: 1159 Color: 4
Size: 164 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 7303 Color: 0
Size: 584 Color: 2
Size: 512 Color: 1

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 7319 Color: 0
Size: 600 Color: 3
Size: 480 Color: 3

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 7397 Color: 3
Size: 1002 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7495 Color: 3
Size: 500 Color: 4
Size: 404 Color: 0

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 7498 Color: 4
Size: 901 Color: 3

Bin 57: 1 of cap free
Amount of items: 4
Items: 
Size: 7530 Color: 4
Size: 837 Color: 1
Size: 16 Color: 4
Size: 16 Color: 0

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 4213 Color: 3
Size: 3497 Color: 3
Size: 688 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 4799 Color: 0
Size: 3027 Color: 3
Size: 572 Color: 2

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 4
Size: 2576 Color: 3
Size: 168 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 6217 Color: 0
Size: 2037 Color: 3
Size: 144 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 6455 Color: 3
Size: 1537 Color: 1
Size: 406 Color: 3

Bin 63: 2 of cap free
Amount of items: 4
Items: 
Size: 7354 Color: 4
Size: 964 Color: 1
Size: 48 Color: 3
Size: 32 Color: 0

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 3
Size: 3491 Color: 3
Size: 144 Color: 0

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 0
Size: 3022 Color: 1
Size: 416 Color: 2

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5622 Color: 4
Size: 2627 Color: 4
Size: 148 Color: 0

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 0
Size: 1979 Color: 3
Size: 176 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 6281 Color: 0
Size: 1940 Color: 3
Size: 176 Color: 4

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 4
Size: 1767 Color: 0
Size: 308 Color: 2

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 1
Size: 1635 Color: 3
Size: 200 Color: 0

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 3
Size: 3301 Color: 0
Size: 326 Color: 2

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 4
Size: 2606 Color: 1
Size: 152 Color: 3

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 0
Size: 1068 Color: 0
Size: 782 Color: 3

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 1
Size: 1344 Color: 1
Size: 308 Color: 3

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 3
Size: 930 Color: 2

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 3
Size: 1762 Color: 2
Size: 190 Color: 4

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 2
Size: 1631 Color: 1

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 7269 Color: 4
Size: 1126 Color: 3

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 7338 Color: 4
Size: 961 Color: 1
Size: 96 Color: 0

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 6221 Color: 3
Size: 2041 Color: 1
Size: 132 Color: 4

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 7479 Color: 0
Size: 915 Color: 3

Bin 82: 7 of cap free
Amount of items: 5
Items: 
Size: 4201 Color: 3
Size: 1821 Color: 0
Size: 1394 Color: 1
Size: 769 Color: 4
Size: 208 Color: 0

Bin 83: 7 of cap free
Amount of items: 5
Items: 
Size: 4205 Color: 2
Size: 2856 Color: 1
Size: 862 Color: 4
Size: 252 Color: 4
Size: 218 Color: 4

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 5751 Color: 2
Size: 2642 Color: 1

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 3
Size: 1107 Color: 0
Size: 1042 Color: 4

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 7127 Color: 3
Size: 1266 Color: 4

Bin 87: 7 of cap free
Amount of items: 3
Items: 
Size: 7492 Color: 4
Size: 877 Color: 1
Size: 24 Color: 4

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 0
Size: 1477 Color: 2
Size: 266 Color: 3

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 4
Size: 1746 Color: 2

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 1
Size: 1461 Color: 3

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 7202 Color: 2
Size: 1189 Color: 4

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 4209 Color: 2
Size: 3493 Color: 0
Size: 688 Color: 1

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 1
Size: 2932 Color: 3
Size: 184 Color: 0

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 7413 Color: 1
Size: 961 Color: 4
Size: 16 Color: 1

Bin 95: 11 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 3
Size: 2209 Color: 4
Size: 280 Color: 0

Bin 96: 13 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 0
Size: 2509 Color: 2
Size: 648 Color: 1

Bin 97: 13 of cap free
Amount of items: 3
Items: 
Size: 7463 Color: 0
Size: 876 Color: 1
Size: 48 Color: 1

Bin 98: 14 of cap free
Amount of items: 3
Items: 
Size: 4468 Color: 3
Size: 3220 Color: 0
Size: 698 Color: 3

Bin 99: 14 of cap free
Amount of items: 2
Items: 
Size: 7185 Color: 3
Size: 1201 Color: 4

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 4884 Color: 3
Size: 3501 Color: 2

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 6258 Color: 3
Size: 2126 Color: 1

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6898 Color: 0
Size: 1486 Color: 2

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 7365 Color: 1
Size: 1013 Color: 2

Bin 104: 23 of cap free
Amount of items: 2
Items: 
Size: 5249 Color: 4
Size: 3128 Color: 3

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 7252 Color: 4
Size: 1124 Color: 1

Bin 106: 24 of cap free
Amount of items: 2
Items: 
Size: 7402 Color: 4
Size: 974 Color: 1

Bin 107: 25 of cap free
Amount of items: 2
Items: 
Size: 7300 Color: 4
Size: 1075 Color: 2

Bin 108: 26 of cap free
Amount of items: 2
Items: 
Size: 6530 Color: 4
Size: 1844 Color: 1

Bin 109: 30 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 0
Size: 3648 Color: 2
Size: 460 Color: 1

Bin 110: 34 of cap free
Amount of items: 3
Items: 
Size: 4220 Color: 4
Size: 3450 Color: 1
Size: 696 Color: 0

Bin 111: 34 of cap free
Amount of items: 2
Items: 
Size: 6564 Color: 0
Size: 1802 Color: 1

Bin 112: 35 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 0
Size: 1562 Color: 4

Bin 113: 35 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 1
Size: 1315 Color: 3

Bin 114: 37 of cap free
Amount of items: 2
Items: 
Size: 6961 Color: 4
Size: 1402 Color: 3

Bin 115: 38 of cap free
Amount of items: 2
Items: 
Size: 4668 Color: 4
Size: 3694 Color: 3

Bin 116: 45 of cap free
Amount of items: 2
Items: 
Size: 7111 Color: 2
Size: 1244 Color: 4

Bin 117: 47 of cap free
Amount of items: 2
Items: 
Size: 5484 Color: 4
Size: 2869 Color: 2

Bin 118: 51 of cap free
Amount of items: 2
Items: 
Size: 6306 Color: 4
Size: 2043 Color: 3

Bin 119: 54 of cap free
Amount of items: 2
Items: 
Size: 5312 Color: 2
Size: 3034 Color: 1

Bin 120: 56 of cap free
Amount of items: 2
Items: 
Size: 5060 Color: 3
Size: 3284 Color: 1

Bin 121: 58 of cap free
Amount of items: 2
Items: 
Size: 4206 Color: 4
Size: 4136 Color: 3

Bin 122: 63 of cap free
Amount of items: 2
Items: 
Size: 6716 Color: 0
Size: 1621 Color: 1

Bin 123: 66 of cap free
Amount of items: 2
Items: 
Size: 6730 Color: 1
Size: 1604 Color: 0

Bin 124: 73 of cap free
Amount of items: 2
Items: 
Size: 6945 Color: 1
Size: 1382 Color: 2

Bin 125: 76 of cap free
Amount of items: 11
Items: 
Size: 1364 Color: 2
Size: 1204 Color: 0
Size: 1032 Color: 0
Size: 836 Color: 3
Size: 834 Color: 3
Size: 604 Color: 1
Size: 604 Color: 0
Size: 524 Color: 1
Size: 500 Color: 1
Size: 460 Color: 4
Size: 362 Color: 4

Bin 126: 79 of cap free
Amount of items: 2
Items: 
Size: 7095 Color: 3
Size: 1226 Color: 4

Bin 127: 86 of cap free
Amount of items: 2
Items: 
Size: 6230 Color: 3
Size: 2084 Color: 4

Bin 128: 90 of cap free
Amount of items: 2
Items: 
Size: 5978 Color: 3
Size: 2332 Color: 2

Bin 129: 113 of cap free
Amount of items: 2
Items: 
Size: 5969 Color: 1
Size: 2318 Color: 4

Bin 130: 118 of cap free
Amount of items: 25
Items: 
Size: 528 Color: 0
Size: 520 Color: 2
Size: 496 Color: 0
Size: 460 Color: 2
Size: 456 Color: 0
Size: 440 Color: 3
Size: 388 Color: 1
Size: 360 Color: 2
Size: 356 Color: 4
Size: 352 Color: 1
Size: 348 Color: 4
Size: 348 Color: 4
Size: 326 Color: 3
Size: 294 Color: 2
Size: 280 Color: 2
Size: 272 Color: 1
Size: 272 Color: 1
Size: 244 Color: 0
Size: 242 Color: 0
Size: 240 Color: 4
Size: 238 Color: 3
Size: 214 Color: 4
Size: 212 Color: 1
Size: 208 Color: 1
Size: 188 Color: 4

Bin 131: 138 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 3
Size: 3484 Color: 2

Bin 132: 140 of cap free
Amount of items: 2
Items: 
Size: 5953 Color: 2
Size: 2307 Color: 1

Bin 133: 6340 of cap free
Amount of items: 12
Items: 
Size: 224 Color: 0
Size: 200 Color: 0
Size: 180 Color: 4
Size: 174 Color: 3
Size: 168 Color: 2
Size: 168 Color: 1
Size: 166 Color: 0
Size: 164 Color: 4
Size: 160 Color: 1
Size: 152 Color: 4
Size: 152 Color: 3
Size: 152 Color: 1

Total size: 1108800
Total free space: 8400

