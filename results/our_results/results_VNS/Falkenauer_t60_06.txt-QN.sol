Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 53
Size: 276 Color: 15
Size: 275 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 322 Color: 32
Size: 278 Color: 18
Size: 400 Color: 48

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 47
Size: 337 Color: 34
Size: 269 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 35
Size: 273 Color: 11
Size: 388 Color: 46

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 42
Size: 365 Color: 39
Size: 261 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 43
Size: 322 Color: 31
Size: 304 Color: 27

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 50
Size: 321 Color: 30
Size: 274 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 51
Size: 317 Color: 29
Size: 259 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 58
Size: 262 Color: 8
Size: 253 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 40
Size: 357 Color: 38
Size: 275 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 59
Size: 252 Color: 3
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 57
Size: 279 Color: 19
Size: 250 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 55
Size: 284 Color: 21
Size: 265 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 45
Size: 328 Color: 33
Size: 292 Color: 24

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 44
Size: 344 Color: 36
Size: 281 Color: 20

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 54
Size: 300 Color: 26
Size: 250 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 52
Size: 297 Color: 25
Size: 276 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 41
Size: 355 Color: 37
Size: 276 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 49
Size: 310 Color: 28
Size: 287 Color: 23

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 56
Size: 284 Color: 22
Size: 252 Color: 4

Total size: 20000
Total free space: 0

