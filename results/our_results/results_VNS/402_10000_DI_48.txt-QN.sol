Capicity Bin: 9504
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4770 Color: 282
Size: 3956 Color: 265
Size: 280 Color: 59
Size: 250 Color: 50
Size: 248 Color: 49

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 5943 Color: 299
Size: 2969 Color: 243
Size: 240 Color: 45
Size: 176 Color: 23
Size: 176 Color: 22

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 323
Size: 1522 Color: 190
Size: 1048 Color: 154

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 326
Size: 1874 Color: 210
Size: 528 Color: 105

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7256 Color: 331
Size: 2136 Color: 218
Size: 112 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 333
Size: 1846 Color: 209
Size: 368 Color: 78

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7541 Color: 340
Size: 1725 Color: 202
Size: 238 Color: 42

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7572 Color: 343
Size: 1142 Color: 158
Size: 790 Color: 136

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7573 Color: 344
Size: 1611 Color: 196
Size: 320 Color: 68

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7584 Color: 345
Size: 1320 Color: 176
Size: 600 Color: 113

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 355
Size: 922 Color: 145
Size: 784 Color: 127

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7800 Color: 356
Size: 1328 Color: 178
Size: 376 Color: 82

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 357
Size: 972 Color: 149
Size: 688 Color: 122

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7863 Color: 359
Size: 1337 Color: 179
Size: 304 Color: 65

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7920 Color: 363
Size: 1456 Color: 186
Size: 128 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7933 Color: 365
Size: 1325 Color: 177
Size: 246 Color: 47

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7949 Color: 367
Size: 1231 Color: 169
Size: 324 Color: 71

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8068 Color: 373
Size: 1228 Color: 168
Size: 208 Color: 29

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 374
Size: 1188 Color: 162
Size: 244 Color: 46

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 376
Size: 1208 Color: 166
Size: 212 Color: 31

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 382
Size: 1080 Color: 156
Size: 208 Color: 28

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 383
Size: 922 Color: 146
Size: 360 Color: 76

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 388
Size: 684 Color: 120
Size: 480 Color: 98

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8344 Color: 389
Size: 640 Color: 116
Size: 520 Color: 104

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8368 Color: 391
Size: 976 Color: 150
Size: 160 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 392
Size: 810 Color: 138
Size: 316 Color: 67

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 393
Size: 914 Color: 144
Size: 180 Color: 24

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 395
Size: 802 Color: 137
Size: 260 Color: 54

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 397
Size: 784 Color: 128
Size: 236 Color: 41

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8488 Color: 398
Size: 784 Color: 129
Size: 232 Color: 40

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 399
Size: 692 Color: 123
Size: 298 Color: 64

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 402
Size: 704 Color: 126
Size: 258 Color: 53

Bin 33: 1 of cap free
Amount of items: 5
Items: 
Size: 5410 Color: 290
Size: 3411 Color: 251
Size: 232 Color: 38
Size: 226 Color: 35
Size: 224 Color: 34

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 7127 Color: 327
Size: 2376 Color: 226

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 7531 Color: 339
Size: 1444 Color: 185
Size: 528 Color: 106

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 7707 Color: 350
Size: 1612 Color: 197
Size: 184 Color: 25

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 7719 Color: 351
Size: 1528 Color: 191
Size: 256 Color: 52

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 7997 Color: 368
Size: 1222 Color: 167
Size: 284 Color: 60

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 8004 Color: 369
Size: 1499 Color: 189

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 371
Size: 1042 Color: 153
Size: 432 Color: 91

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 8134 Color: 380
Size: 1369 Color: 181

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 309
Size: 3002 Color: 246

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 322
Size: 1297 Color: 174
Size: 1296 Color: 173

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 328
Size: 2002 Color: 215
Size: 328 Color: 72

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 7558 Color: 342
Size: 1608 Color: 195
Size: 336 Color: 73

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 7766 Color: 353
Size: 1736 Color: 203

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 7928 Color: 364
Size: 1574 Color: 193

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 8042 Color: 372
Size: 1460 Color: 187

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 8516 Color: 400
Size: 986 Color: 151

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 8534 Color: 401
Size: 968 Color: 148

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 8244 Color: 384
Size: 1257 Color: 172

Bin 52: 4 of cap free
Amount of items: 4
Items: 
Size: 6870 Color: 319
Size: 2456 Color: 229
Size: 94 Color: 4
Size: 80 Color: 3

Bin 53: 4 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 324
Size: 2548 Color: 232

Bin 54: 4 of cap free
Amount of items: 2
Items: 
Size: 7780 Color: 354
Size: 1720 Color: 201

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 7864 Color: 360
Size: 1636 Color: 199

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 7878 Color: 361
Size: 1622 Color: 198

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 8078 Color: 375
Size: 1422 Color: 184

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 8100 Color: 377
Size: 1400 Color: 183

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 8190 Color: 381
Size: 1310 Color: 175

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 8248 Color: 385
Size: 1252 Color: 171

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 8254 Color: 386
Size: 1246 Color: 170

Bin 62: 5 of cap free
Amount of items: 5
Items: 
Size: 5401 Color: 289
Size: 3410 Color: 250
Size: 232 Color: 39
Size: 228 Color: 37
Size: 228 Color: 36

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 303
Size: 2979 Color: 245
Size: 164 Color: 18

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 6751 Color: 314
Size: 2604 Color: 235
Size: 144 Color: 9

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 7678 Color: 349
Size: 1821 Color: 207

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7901 Color: 362
Size: 1598 Color: 194

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 8010 Color: 370
Size: 1489 Color: 188

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7756 Color: 352
Size: 1742 Color: 204

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 7853 Color: 358
Size: 1645 Color: 200

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 8110 Color: 378
Size: 1388 Color: 182

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 8308 Color: 387
Size: 1190 Color: 163

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 8428 Color: 394
Size: 1070 Color: 155

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 8470 Color: 396
Size: 1028 Color: 152

Bin 74: 7 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 315
Size: 2606 Color: 236
Size: 128 Color: 7

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 7333 Color: 336
Size: 2164 Color: 221

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 317
Size: 2676 Color: 238

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 7548 Color: 341
Size: 1948 Color: 213

Bin 78: 9 of cap free
Amount of items: 4
Items: 
Size: 6888 Color: 320
Size: 2475 Color: 230
Size: 68 Color: 2
Size: 64 Color: 1

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 6300 Color: 302
Size: 3026 Color: 247
Size: 168 Color: 19

Bin 80: 10 of cap free
Amount of items: 2
Items: 
Size: 8346 Color: 390
Size: 1148 Color: 159

Bin 81: 13 of cap free
Amount of items: 2
Items: 
Size: 7934 Color: 366
Size: 1557 Color: 192

Bin 82: 14 of cap free
Amount of items: 2
Items: 
Size: 7228 Color: 330
Size: 2262 Color: 224

Bin 83: 14 of cap free
Amount of items: 2
Items: 
Size: 7292 Color: 334
Size: 2198 Color: 222

Bin 84: 14 of cap free
Amount of items: 2
Items: 
Size: 7590 Color: 346
Size: 1900 Color: 212

Bin 85: 14 of cap free
Amount of items: 2
Items: 
Size: 8132 Color: 379
Size: 1358 Color: 180

Bin 86: 15 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 294
Size: 3421 Color: 253
Size: 192 Color: 26

Bin 87: 16 of cap free
Amount of items: 2
Items: 
Size: 6794 Color: 316
Size: 2694 Color: 239

Bin 88: 16 of cap free
Amount of items: 2
Items: 
Size: 7608 Color: 347
Size: 1880 Color: 211

Bin 89: 18 of cap free
Amount of items: 7
Items: 
Size: 4754 Color: 274
Size: 1204 Color: 165
Size: 1200 Color: 164
Size: 1172 Color: 161
Size: 464 Color: 97
Size: 348 Color: 75
Size: 344 Color: 74

Bin 90: 18 of cap free
Amount of items: 2
Items: 
Size: 7414 Color: 337
Size: 2072 Color: 217

Bin 91: 19 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 307
Size: 2905 Color: 242
Size: 160 Color: 17

Bin 92: 19 of cap free
Amount of items: 2
Items: 
Size: 7067 Color: 325
Size: 2418 Color: 227

Bin 93: 20 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 308
Size: 2872 Color: 241
Size: 160 Color: 16

Bin 94: 20 of cap free
Amount of items: 2
Items: 
Size: 7321 Color: 335
Size: 2163 Color: 220

Bin 95: 21 of cap free
Amount of items: 2
Items: 
Size: 7672 Color: 348
Size: 1811 Color: 206

Bin 96: 22 of cap free
Amount of items: 2
Items: 
Size: 5906 Color: 295
Size: 3576 Color: 259

Bin 97: 24 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 293
Size: 3414 Color: 252
Size: 192 Color: 27

Bin 98: 25 of cap free
Amount of items: 2
Items: 
Size: 7448 Color: 338
Size: 2031 Color: 216

Bin 99: 26 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 332
Size: 2220 Color: 223

Bin 100: 27 of cap free
Amount of items: 2
Items: 
Size: 7192 Color: 329
Size: 2285 Color: 225

Bin 101: 28 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 306
Size: 2142 Color: 219
Size: 942 Color: 147

Bin 102: 40 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 305
Size: 1981 Color: 214
Size: 1098 Color: 157

Bin 103: 46 of cap free
Amount of items: 4
Items: 
Size: 4778 Color: 283
Size: 4192 Color: 271
Size: 248 Color: 48
Size: 240 Color: 44

Bin 104: 56 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 313
Size: 2628 Color: 237
Size: 156 Color: 10

Bin 105: 56 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 321
Size: 2508 Color: 231
Size: 32 Color: 0

Bin 106: 59 of cap free
Amount of items: 2
Items: 
Size: 4757 Color: 276
Size: 4688 Color: 272

Bin 107: 60 of cap free
Amount of items: 2
Items: 
Size: 5940 Color: 298
Size: 3504 Color: 258

Bin 108: 66 of cap free
Amount of items: 4
Items: 
Size: 6518 Color: 310
Size: 2600 Color: 233
Size: 160 Color: 15
Size: 160 Color: 14

Bin 109: 67 of cap free
Amount of items: 3
Items: 
Size: 6537 Color: 311
Size: 2740 Color: 240
Size: 160 Color: 13

Bin 110: 68 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 300
Size: 3192 Color: 249
Size: 172 Color: 21

Bin 111: 72 of cap free
Amount of items: 5
Items: 
Size: 4756 Color: 275
Size: 1844 Color: 208
Size: 1808 Color: 205
Size: 704 Color: 125
Size: 320 Color: 70

Bin 112: 83 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 284
Size: 3957 Color: 266
Size: 240 Color: 43

Bin 113: 86 of cap free
Amount of items: 2
Items: 
Size: 5378 Color: 288
Size: 4040 Color: 270

Bin 114: 90 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 301
Size: 2972 Color: 244
Size: 168 Color: 20

Bin 115: 100 of cap free
Amount of items: 2
Items: 
Size: 5938 Color: 297
Size: 3466 Color: 257

Bin 116: 103 of cap free
Amount of items: 2
Items: 
Size: 6373 Color: 304
Size: 3028 Color: 248

Bin 117: 109 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 318
Size: 2455 Color: 228
Size: 96 Color: 5

Bin 118: 121 of cap free
Amount of items: 2
Items: 
Size: 5931 Color: 296
Size: 3452 Color: 256

Bin 119: 137 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 312
Size: 2601 Color: 234
Size: 160 Color: 12

Bin 120: 164 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 292
Size: 3444 Color: 255
Size: 208 Color: 30

Bin 121: 168 of cap free
Amount of items: 2
Items: 
Size: 5372 Color: 287
Size: 3964 Color: 269

Bin 122: 178 of cap free
Amount of items: 4
Items: 
Size: 4760 Color: 277
Size: 3942 Color: 260
Size: 320 Color: 69
Size: 304 Color: 66

Bin 123: 178 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 286
Size: 3962 Color: 268

Bin 124: 196 of cap free
Amount of items: 14
Items: 
Size: 852 Color: 140
Size: 828 Color: 139
Size: 790 Color: 135
Size: 790 Color: 134
Size: 788 Color: 133
Size: 788 Color: 132
Size: 788 Color: 131
Size: 788 Color: 130
Size: 704 Color: 124
Size: 688 Color: 121
Size: 384 Color: 84
Size: 380 Color: 83
Size: 372 Color: 81
Size: 368 Color: 80

Bin 125: 197 of cap free
Amount of items: 2
Items: 
Size: 5346 Color: 285
Size: 3961 Color: 267

Bin 126: 203 of cap free
Amount of items: 4
Items: 
Size: 4761 Color: 278
Size: 3946 Color: 261
Size: 298 Color: 63
Size: 296 Color: 62

Bin 127: 209 of cap free
Amount of items: 4
Items: 
Size: 5413 Color: 291
Size: 3442 Color: 254
Size: 224 Color: 33
Size: 216 Color: 32

Bin 128: 213 of cap free
Amount of items: 7
Items: 
Size: 4753 Color: 273
Size: 1166 Color: 160
Size: 900 Color: 143
Size: 886 Color: 142
Size: 856 Color: 141
Size: 368 Color: 79
Size: 362 Color: 77

Bin 129: 231 of cap free
Amount of items: 4
Items: 
Size: 4762 Color: 279
Size: 3951 Color: 262
Size: 288 Color: 61
Size: 272 Color: 58

Bin 130: 247 of cap free
Amount of items: 4
Items: 
Size: 4764 Color: 280
Size: 3953 Color: 263
Size: 272 Color: 57
Size: 268 Color: 56

Bin 131: 263 of cap free
Amount of items: 4
Items: 
Size: 4765 Color: 281
Size: 3954 Color: 264
Size: 266 Color: 55
Size: 256 Color: 51

Bin 132: 386 of cap free
Amount of items: 17
Items: 
Size: 682 Color: 119
Size: 680 Color: 118
Size: 680 Color: 117
Size: 624 Color: 115
Size: 604 Color: 114
Size: 596 Color: 112
Size: 594 Color: 111
Size: 592 Color: 110
Size: 592 Color: 109
Size: 560 Color: 108
Size: 436 Color: 92
Size: 432 Color: 90
Size: 428 Color: 89
Size: 416 Color: 88
Size: 406 Color: 87
Size: 400 Color: 86
Size: 396 Color: 85

Bin 133: 4654 of cap free
Amount of items: 10
Items: 
Size: 536 Color: 107
Size: 518 Color: 103
Size: 512 Color: 102
Size: 504 Color: 101
Size: 496 Color: 100
Size: 492 Color: 99
Size: 456 Color: 96
Size: 448 Color: 95
Size: 448 Color: 94
Size: 440 Color: 93

Total size: 1254528
Total free space: 9504

