Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 3
Size: 250 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 94
Size: 332 Color: 69
Size: 268 Color: 33

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 102
Size: 300 Color: 56
Size: 265 Color: 27

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 117
Size: 256 Color: 12
Size: 251 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 88
Size: 329 Color: 68
Size: 288 Color: 47

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 99
Size: 310 Color: 61
Size: 268 Color: 34

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 109
Size: 275 Color: 40
Size: 260 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 90
Size: 349 Color: 75
Size: 258 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 84
Size: 360 Color: 79
Size: 266 Color: 29

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 82
Size: 337 Color: 70
Size: 291 Color: 50

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 118
Size: 255 Color: 9
Size: 252 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 93
Size: 347 Color: 74
Size: 253 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 113
Size: 264 Color: 26
Size: 251 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 91
Size: 313 Color: 64
Size: 292 Color: 51

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 85
Size: 280 Color: 43
Size: 343 Color: 72

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 100
Size: 308 Color: 60
Size: 268 Color: 32

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 115
Size: 257 Color: 14
Size: 252 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 96
Size: 311 Color: 63
Size: 288 Color: 48

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 89
Size: 354 Color: 77
Size: 261 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 105
Size: 288 Color: 46
Size: 263 Color: 23

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 114
Size: 256 Color: 13
Size: 256 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 87
Size: 326 Color: 67
Size: 296 Color: 53

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 95
Size: 344 Color: 73
Size: 256 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 78
Size: 350 Color: 76
Size: 295 Color: 52

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 110
Size: 271 Color: 36
Size: 264 Color: 24

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 81
Size: 338 Color: 71
Size: 297 Color: 55

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 86
Size: 361 Color: 80
Size: 262 Color: 22

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 104
Size: 281 Color: 44
Size: 276 Color: 41

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 106
Size: 277 Color: 42
Size: 273 Color: 37

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 101
Size: 310 Color: 62
Size: 261 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 97
Size: 325 Color: 66
Size: 267 Color: 31

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 107
Size: 275 Color: 39
Size: 269 Color: 35

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 111
Size: 266 Color: 28
Size: 262 Color: 20

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 116
Size: 258 Color: 15
Size: 251 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 83
Size: 320 Color: 65
Size: 308 Color: 59

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 98
Size: 301 Color: 57
Size: 287 Color: 45

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 112
Size: 264 Color: 25
Size: 253 Color: 7

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 92
Size: 305 Color: 58
Size: 296 Color: 54

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 103
Size: 291 Color: 49
Size: 266 Color: 30

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 108
Size: 275 Color: 38
Size: 262 Color: 21

Total size: 40000
Total free space: 0

