Capicity Bin: 2068
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1035 Color: 0
Size: 578 Color: 1
Size: 193 Color: 1
Size: 172 Color: 1
Size: 90 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1053 Color: 1
Size: 847 Color: 0
Size: 168 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 1300 Color: 0
Size: 768 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1445 Color: 1
Size: 521 Color: 0
Size: 102 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1550 Color: 0
Size: 480 Color: 1
Size: 38 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 1
Size: 461 Color: 1
Size: 38 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 0
Size: 438 Color: 1
Size: 40 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 1
Size: 320 Color: 0
Size: 142 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 362 Color: 0
Size: 68 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 1
Size: 351 Color: 0
Size: 68 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 0
Size: 170 Color: 1
Size: 168 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 0
Size: 274 Color: 0
Size: 52 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 0
Size: 261 Color: 1
Size: 42 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1749 Color: 1
Size: 267 Color: 0
Size: 52 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 0
Size: 261 Color: 0
Size: 34 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1817 Color: 1
Size: 187 Color: 1
Size: 64 Color: 0

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 0
Size: 853 Color: 0
Size: 68 Color: 1

Bin 18: 1 of cap free
Amount of items: 5
Items: 
Size: 1177 Color: 0
Size: 630 Color: 1
Size: 152 Color: 0
Size: 54 Color: 1
Size: 54 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 0
Size: 694 Color: 1
Size: 136 Color: 0

Bin 20: 1 of cap free
Amount of items: 5
Items: 
Size: 1425 Color: 1
Size: 329 Color: 0
Size: 181 Color: 0
Size: 76 Color: 1
Size: 56 Color: 0

Bin 21: 1 of cap free
Amount of items: 4
Items: 
Size: 1428 Color: 1
Size: 331 Color: 0
Size: 244 Color: 0
Size: 64 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 0
Size: 434 Color: 1
Size: 70 Color: 0

Bin 23: 1 of cap free
Amount of items: 4
Items: 
Size: 1609 Color: 1
Size: 402 Color: 0
Size: 32 Color: 1
Size: 24 Color: 0

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 0
Size: 357 Color: 0
Size: 56 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 0
Size: 214 Color: 1
Size: 120 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1750 Color: 0
Size: 281 Color: 1
Size: 36 Color: 1

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1776 Color: 0
Size: 291 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 0
Size: 218 Color: 0
Size: 40 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 0
Size: 841 Color: 0
Size: 40 Color: 1

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 486 Color: 1
Size: 342 Color: 1

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1373 Color: 1
Size: 693 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1453 Color: 1
Size: 513 Color: 0
Size: 100 Color: 0

Bin 33: 2 of cap free
Amount of items: 4
Items: 
Size: 1734 Color: 0
Size: 266 Color: 1
Size: 58 Color: 1
Size: 8 Color: 0

Bin 34: 2 of cap free
Amount of items: 4
Items: 
Size: 1741 Color: 0
Size: 273 Color: 1
Size: 44 Color: 1
Size: 8 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 1
Size: 354 Color: 1
Size: 106 Color: 0

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 0
Size: 346 Color: 1

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1803 Color: 1
Size: 262 Color: 0

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1037 Color: 1
Size: 889 Color: 0
Size: 138 Color: 1

Bin 39: 4 of cap free
Amount of items: 4
Items: 
Size: 1486 Color: 1
Size: 522 Color: 0
Size: 40 Color: 1
Size: 16 Color: 0

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1853 Color: 1
Size: 211 Color: 0

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 1646 Color: 0
Size: 417 Color: 1

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1641 Color: 1
Size: 421 Color: 0

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1662 Color: 0
Size: 400 Color: 1

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 1845 Color: 0
Size: 217 Color: 1

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 1
Size: 439 Color: 0

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1814 Color: 0
Size: 247 Color: 1

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1829 Color: 1
Size: 232 Color: 0

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1289 Color: 0
Size: 770 Color: 1

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 1673 Color: 0
Size: 386 Color: 1

Bin 50: 9 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 0
Size: 201 Color: 1

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1675 Color: 1
Size: 383 Color: 0

Bin 52: 10 of cap free
Amount of items: 2
Items: 
Size: 1837 Color: 1
Size: 221 Color: 0

Bin 53: 13 of cap free
Amount of items: 2
Items: 
Size: 1446 Color: 0
Size: 609 Color: 1

Bin 54: 14 of cap free
Amount of items: 2
Items: 
Size: 1517 Color: 1
Size: 537 Color: 0

Bin 55: 17 of cap free
Amount of items: 2
Items: 
Size: 1314 Color: 0
Size: 737 Color: 1

Bin 56: 26 of cap free
Amount of items: 2
Items: 
Size: 1461 Color: 0
Size: 581 Color: 1

Bin 57: 26 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 0
Size: 232 Color: 1

Bin 58: 28 of cap free
Amount of items: 2
Items: 
Size: 1612 Color: 1
Size: 428 Color: 0

Bin 59: 28 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 0
Size: 282 Color: 1

Bin 60: 29 of cap free
Amount of items: 2
Items: 
Size: 1757 Color: 1
Size: 282 Color: 0

Bin 61: 33 of cap free
Amount of items: 3
Items: 
Size: 1045 Color: 0
Size: 862 Color: 0
Size: 128 Color: 1

Bin 62: 33 of cap free
Amount of items: 2
Items: 
Size: 1339 Color: 0
Size: 696 Color: 1

Bin 63: 35 of cap free
Amount of items: 17
Items: 
Size: 507 Color: 1
Size: 178 Color: 1
Size: 146 Color: 0
Size: 114 Color: 0
Size: 112 Color: 1
Size: 102 Color: 1
Size: 100 Color: 1
Size: 96 Color: 0
Size: 84 Color: 1
Size: 84 Color: 1
Size: 82 Color: 0
Size: 76 Color: 0
Size: 76 Color: 0
Size: 76 Color: 0
Size: 68 Color: 1
Size: 68 Color: 0
Size: 64 Color: 1

Bin 64: 39 of cap free
Amount of items: 2
Items: 
Size: 1378 Color: 1
Size: 651 Color: 0

Bin 65: 45 of cap free
Amount of items: 3
Items: 
Size: 1038 Color: 0
Size: 861 Color: 0
Size: 124 Color: 1

Bin 66: 1566 of cap free
Amount of items: 10
Items: 
Size: 64 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0
Size: 50 Color: 1
Size: 50 Color: 0
Size: 48 Color: 1
Size: 48 Color: 1
Size: 48 Color: 1
Size: 48 Color: 1
Size: 42 Color: 0

Total size: 134420
Total free space: 2068

