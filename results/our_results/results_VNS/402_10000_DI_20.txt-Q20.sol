Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4168 Color: 14
Size: 3440 Color: 15
Size: 216 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 14
Size: 3258 Color: 19
Size: 148 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4422 Color: 17
Size: 2778 Color: 4
Size: 624 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4494 Color: 12
Size: 3116 Color: 8
Size: 214 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 16
Size: 3048 Color: 17
Size: 220 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4855 Color: 10
Size: 2781 Color: 18
Size: 188 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5195 Color: 11
Size: 2211 Color: 15
Size: 418 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5243 Color: 5
Size: 2433 Color: 15
Size: 148 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5291 Color: 13
Size: 2191 Color: 17
Size: 342 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5332 Color: 1
Size: 2300 Color: 4
Size: 192 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5335 Color: 17
Size: 2231 Color: 0
Size: 258 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5699 Color: 4
Size: 1771 Color: 19
Size: 354 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 16
Size: 1996 Color: 1
Size: 128 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 11
Size: 1284 Color: 19
Size: 544 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5970 Color: 1
Size: 1478 Color: 5
Size: 376 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6186 Color: 5
Size: 1510 Color: 10
Size: 128 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6196 Color: 12
Size: 1436 Color: 10
Size: 192 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6246 Color: 14
Size: 1138 Color: 14
Size: 440 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6248 Color: 7
Size: 900 Color: 6
Size: 676 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6292 Color: 1
Size: 1236 Color: 12
Size: 296 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6302 Color: 14
Size: 1218 Color: 11
Size: 304 Color: 5

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6309 Color: 3
Size: 1081 Color: 5
Size: 434 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 11
Size: 1270 Color: 11
Size: 206 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 16
Size: 972 Color: 14
Size: 456 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6421 Color: 14
Size: 1007 Color: 12
Size: 396 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 3
Size: 974 Color: 9
Size: 414 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 11
Size: 810 Color: 3
Size: 552 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 12
Size: 1056 Color: 16
Size: 284 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 2
Size: 903 Color: 1
Size: 352 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 12
Size: 943 Color: 15
Size: 280 Color: 11

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 5
Size: 682 Color: 19
Size: 484 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 10
Size: 820 Color: 17
Size: 344 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6727 Color: 15
Size: 945 Color: 5
Size: 152 Color: 10

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 6
Size: 900 Color: 12
Size: 144 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 10
Size: 869 Color: 7
Size: 172 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6784 Color: 15
Size: 564 Color: 9
Size: 476 Color: 19

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 11
Size: 876 Color: 4
Size: 104 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6829 Color: 15
Size: 935 Color: 7
Size: 60 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 11
Size: 608 Color: 13
Size: 308 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6874 Color: 16
Size: 504 Color: 8
Size: 446 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6914 Color: 10
Size: 742 Color: 9
Size: 168 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6938 Color: 11
Size: 448 Color: 4
Size: 438 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 14
Size: 648 Color: 2
Size: 180 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7006 Color: 9
Size: 648 Color: 11
Size: 170 Color: 0

Bin 45: 1 of cap free
Amount of items: 5
Items: 
Size: 4470 Color: 16
Size: 2795 Color: 18
Size: 248 Color: 17
Size: 202 Color: 13
Size: 108 Color: 17

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 10
Size: 3124 Color: 18
Size: 228 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 4879 Color: 2
Size: 2798 Color: 1
Size: 146 Color: 6

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 17
Size: 2246 Color: 11
Size: 430 Color: 13

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 3
Size: 2475 Color: 6
Size: 136 Color: 13

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 5523 Color: 7
Size: 2180 Color: 11
Size: 120 Color: 9

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 5538 Color: 9
Size: 2075 Color: 7
Size: 210 Color: 13

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 5767 Color: 17
Size: 1122 Color: 6
Size: 934 Color: 5

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 10
Size: 1715 Color: 6

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6255 Color: 1
Size: 1480 Color: 4
Size: 88 Color: 14

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6581 Color: 2
Size: 878 Color: 16
Size: 364 Color: 15

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6741 Color: 17
Size: 648 Color: 10
Size: 434 Color: 4

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6825 Color: 13
Size: 616 Color: 18
Size: 382 Color: 6

Bin 58: 2 of cap free
Amount of items: 5
Items: 
Size: 3914 Color: 15
Size: 2171 Color: 18
Size: 1429 Color: 15
Size: 156 Color: 17
Size: 152 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 4950 Color: 18
Size: 2724 Color: 0
Size: 148 Color: 16

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 0
Size: 2079 Color: 17
Size: 572 Color: 10

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5626 Color: 8
Size: 1644 Color: 13
Size: 552 Color: 5

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 4
Size: 1562 Color: 1
Size: 414 Color: 0

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 5
Size: 1774 Color: 0
Size: 196 Color: 13

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 4
Size: 1068 Color: 5
Size: 232 Color: 11

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6529 Color: 6
Size: 1293 Color: 2

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 8
Size: 746 Color: 11
Size: 328 Color: 10

Bin 67: 3 of cap free
Amount of items: 4
Items: 
Size: 3925 Color: 1
Size: 2782 Color: 11
Size: 986 Color: 3
Size: 128 Color: 16

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 3918 Color: 6
Size: 3253 Color: 8
Size: 650 Color: 12

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5174 Color: 15
Size: 2159 Color: 11
Size: 488 Color: 11

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 12
Size: 857 Color: 7

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 4966 Color: 2
Size: 2854 Color: 10

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 5307 Color: 6
Size: 2513 Color: 17

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 4
Size: 1906 Color: 1
Size: 366 Color: 2

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 11
Size: 1650 Color: 14
Size: 136 Color: 0

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 6206 Color: 10
Size: 1546 Color: 3
Size: 68 Color: 6

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6273 Color: 16
Size: 1547 Color: 11

Bin 77: 4 of cap free
Amount of items: 2
Items: 
Size: 6557 Color: 10
Size: 1263 Color: 2

Bin 78: 4 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 16
Size: 890 Color: 7

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 7026 Color: 10
Size: 794 Color: 8

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 6986 Color: 1
Size: 833 Color: 18

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6366 Color: 14
Size: 1452 Color: 16

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6677 Color: 6
Size: 1141 Color: 0

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 6797 Color: 0
Size: 1021 Color: 19

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 12
Size: 2398 Color: 0
Size: 120 Color: 2

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 1
Size: 2487 Color: 6

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 5823 Color: 6
Size: 1994 Color: 15

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6293 Color: 13
Size: 1524 Color: 19

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 6693 Color: 12
Size: 1124 Color: 2

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 1
Size: 1471 Color: 18
Size: 1277 Color: 13

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 11
Size: 2084 Color: 15

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 6548 Color: 8
Size: 1268 Color: 3

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 0
Size: 1057 Color: 6

Bin 93: 10 of cap free
Amount of items: 4
Items: 
Size: 3917 Color: 15
Size: 3251 Color: 13
Size: 338 Color: 12
Size: 308 Color: 19

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 6794 Color: 12
Size: 1020 Color: 19

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 7
Size: 960 Color: 5

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 19
Size: 2438 Color: 0
Size: 160 Color: 11

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 5619 Color: 14
Size: 2194 Color: 17

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 8
Size: 1196 Color: 17

Bin 99: 11 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 5
Size: 1171 Color: 1

Bin 100: 12 of cap free
Amount of items: 3
Items: 
Size: 6455 Color: 9
Size: 1309 Color: 2
Size: 48 Color: 19

Bin 101: 12 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 9
Size: 1208 Color: 18

Bin 102: 15 of cap free
Amount of items: 2
Items: 
Size: 5482 Color: 5
Size: 2327 Color: 16

Bin 103: 15 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 15
Size: 1037 Color: 16

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 5351 Color: 7
Size: 2457 Color: 19

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 5969 Color: 16
Size: 1839 Color: 17

Bin 106: 16 of cap free
Amount of items: 2
Items: 
Size: 6234 Color: 17
Size: 1574 Color: 13

Bin 107: 17 of cap free
Amount of items: 2
Items: 
Size: 6059 Color: 3
Size: 1748 Color: 15

Bin 108: 22 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 5
Size: 2838 Color: 0
Size: 176 Color: 16

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 6482 Color: 2
Size: 1318 Color: 9

Bin 110: 27 of cap free
Amount of items: 2
Items: 
Size: 5434 Color: 5
Size: 2363 Color: 2

Bin 111: 27 of cap free
Amount of items: 2
Items: 
Size: 5698 Color: 0
Size: 2099 Color: 16

Bin 112: 28 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 12
Size: 2210 Color: 11
Size: 1494 Color: 11

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 3921 Color: 10
Size: 3872 Color: 17

Bin 114: 32 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 3
Size: 2175 Color: 6
Size: 494 Color: 18

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 19
Size: 1086 Color: 10

Bin 116: 34 of cap free
Amount of items: 3
Items: 
Size: 4385 Color: 1
Size: 3257 Color: 12
Size: 148 Color: 3

Bin 117: 35 of cap free
Amount of items: 3
Items: 
Size: 5219 Color: 1
Size: 2506 Color: 2
Size: 64 Color: 12

Bin 118: 36 of cap free
Amount of items: 2
Items: 
Size: 5954 Color: 13
Size: 1834 Color: 19

Bin 119: 36 of cap free
Amount of items: 2
Items: 
Size: 7024 Color: 18
Size: 764 Color: 0

Bin 120: 42 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 12
Size: 762 Color: 15

Bin 121: 44 of cap free
Amount of items: 2
Items: 
Size: 6111 Color: 15
Size: 1669 Color: 16

Bin 122: 55 of cap free
Amount of items: 2
Items: 
Size: 4902 Color: 0
Size: 2867 Color: 11

Bin 123: 57 of cap free
Amount of items: 2
Items: 
Size: 5235 Color: 8
Size: 2532 Color: 19

Bin 124: 60 of cap free
Amount of items: 2
Items: 
Size: 5331 Color: 6
Size: 2433 Color: 5

Bin 125: 73 of cap free
Amount of items: 6
Items: 
Size: 3913 Color: 17
Size: 1065 Color: 11
Size: 957 Color: 8
Size: 880 Color: 19
Size: 556 Color: 1
Size: 380 Color: 3

Bin 126: 75 of cap free
Amount of items: 2
Items: 
Size: 4487 Color: 12
Size: 3262 Color: 0

Bin 127: 77 of cap free
Amount of items: 2
Items: 
Size: 4486 Color: 0
Size: 3261 Color: 11

Bin 128: 79 of cap free
Amount of items: 2
Items: 
Size: 4841 Color: 14
Size: 2904 Color: 15

Bin 129: 80 of cap free
Amount of items: 14
Items: 
Size: 724 Color: 14
Size: 724 Color: 7
Size: 702 Color: 15
Size: 672 Color: 19
Size: 666 Color: 12
Size: 650 Color: 16
Size: 650 Color: 8
Size: 648 Color: 9
Size: 558 Color: 13
Size: 496 Color: 0
Size: 432 Color: 1
Size: 376 Color: 4
Size: 294 Color: 3
Size: 152 Color: 4

Bin 130: 80 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 12
Size: 3260 Color: 4

Bin 131: 89 of cap free
Amount of items: 3
Items: 
Size: 3916 Color: 16
Size: 1919 Color: 11
Size: 1900 Color: 7

Bin 132: 92 of cap free
Amount of items: 27
Items: 
Size: 556 Color: 16
Size: 556 Color: 5
Size: 442 Color: 8
Size: 432 Color: 7
Size: 408 Color: 0
Size: 352 Color: 1
Size: 332 Color: 9
Size: 328 Color: 4
Size: 308 Color: 18
Size: 260 Color: 15
Size: 260 Color: 4
Size: 254 Color: 19
Size: 252 Color: 3
Size: 252 Color: 1
Size: 248 Color: 4
Size: 240 Color: 13
Size: 240 Color: 8
Size: 232 Color: 17
Size: 224 Color: 14
Size: 216 Color: 19
Size: 216 Color: 5
Size: 208 Color: 17
Size: 200 Color: 14
Size: 200 Color: 3
Size: 190 Color: 2
Size: 166 Color: 0
Size: 160 Color: 10

Bin 133: 6194 of cap free
Amount of items: 11
Items: 
Size: 190 Color: 9
Size: 184 Color: 2
Size: 176 Color: 19
Size: 176 Color: 7
Size: 160 Color: 14
Size: 152 Color: 10
Size: 136 Color: 0
Size: 132 Color: 1
Size: 112 Color: 5
Size: 112 Color: 3
Size: 100 Color: 16

Total size: 1032768
Total free space: 7824

