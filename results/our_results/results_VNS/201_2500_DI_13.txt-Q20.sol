Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1373 Color: 9
Size: 1021 Color: 2
Size: 62 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 6
Size: 1022 Color: 12
Size: 44 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 18
Size: 1020 Color: 7
Size: 40 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1688 Color: 14
Size: 738 Color: 12
Size: 30 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 15
Size: 684 Color: 5
Size: 40 Color: 19

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1739 Color: 7
Size: 599 Color: 17
Size: 118 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 4
Size: 572 Color: 8
Size: 102 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 19
Size: 438 Color: 18
Size: 200 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 19
Size: 526 Color: 12
Size: 104 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1883 Color: 6
Size: 479 Color: 5
Size: 94 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 14
Size: 293 Color: 3
Size: 228 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 12
Size: 436 Color: 13
Size: 80 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 5
Size: 430 Color: 8
Size: 52 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 14
Size: 370 Color: 11
Size: 8 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 3
Size: 310 Color: 1
Size: 56 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 15
Size: 270 Color: 1
Size: 32 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 11
Size: 236 Color: 5
Size: 56 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 13
Size: 200 Color: 17
Size: 76 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 5
Size: 202 Color: 18
Size: 64 Color: 19

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 18
Size: 884 Color: 17
Size: 80 Color: 19

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 1
Size: 435 Color: 8
Size: 128 Color: 10

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 4
Size: 476 Color: 10

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 0
Size: 324 Color: 10
Size: 88 Color: 8

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 2188 Color: 3
Size: 257 Color: 18
Size: 8 Color: 9
Size: 2 Color: 9

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2201 Color: 4
Size: 254 Color: 6

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 19
Size: 588 Color: 7
Size: 180 Color: 16

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1812 Color: 2
Size: 642 Color: 4

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 2015 Color: 7
Size: 439 Color: 15

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 1
Size: 386 Color: 16

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 10
Size: 694 Color: 10
Size: 88 Color: 16

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1730 Color: 5
Size: 723 Color: 2

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 2165 Color: 14
Size: 272 Color: 7
Size: 16 Color: 17

Bin 33: 4 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 7
Size: 974 Color: 9
Size: 244 Color: 3

Bin 34: 4 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 4
Size: 890 Color: 17
Size: 72 Color: 8

Bin 35: 4 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 5
Size: 772 Color: 10
Size: 144 Color: 17

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1898 Color: 14
Size: 554 Color: 5

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 18
Size: 222 Color: 5
Size: 130 Color: 6

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 17
Size: 272 Color: 4
Size: 8 Color: 7

Bin 39: 5 of cap free
Amount of items: 2
Items: 
Size: 2134 Color: 17
Size: 317 Color: 0

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 6
Size: 562 Color: 16
Size: 244 Color: 7

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 1795 Color: 9
Size: 655 Color: 12

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 18
Size: 384 Color: 12
Size: 78 Color: 10

Bin 43: 6 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 14
Size: 402 Color: 3
Size: 20 Color: 7

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 4
Size: 334 Color: 12
Size: 58 Color: 5

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 2105 Color: 6
Size: 345 Color: 4

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1843 Color: 18
Size: 606 Color: 11

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1959 Color: 13
Size: 490 Color: 8

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 0
Size: 415 Color: 6

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 2149 Color: 7
Size: 300 Color: 4

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1870 Color: 17
Size: 576 Color: 3

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 5
Size: 504 Color: 13

Bin 52: 11 of cap free
Amount of items: 22
Items: 
Size: 213 Color: 3
Size: 176 Color: 7
Size: 160 Color: 12
Size: 152 Color: 13
Size: 144 Color: 4
Size: 136 Color: 2
Size: 128 Color: 19
Size: 120 Color: 5
Size: 120 Color: 3
Size: 112 Color: 1
Size: 110 Color: 12
Size: 100 Color: 7
Size: 96 Color: 11
Size: 96 Color: 11
Size: 86 Color: 16
Size: 84 Color: 16
Size: 84 Color: 11
Size: 84 Color: 7
Size: 82 Color: 0
Size: 64 Color: 6
Size: 50 Color: 10
Size: 48 Color: 10

Bin 53: 11 of cap free
Amount of items: 4
Items: 
Size: 1233 Color: 16
Size: 984 Color: 18
Size: 160 Color: 10
Size: 68 Color: 0

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 8
Size: 511 Color: 9

Bin 55: 13 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 16
Size: 399 Color: 15
Size: 396 Color: 19
Size: 243 Color: 3
Size: 176 Color: 6

Bin 56: 13 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 1
Size: 551 Color: 11
Size: 478 Color: 17
Size: 184 Color: 9

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 2077 Color: 1
Size: 364 Color: 14

Bin 58: 20 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 11
Size: 402 Color: 16
Size: 40 Color: 5

Bin 59: 21 of cap free
Amount of items: 2
Items: 
Size: 1532 Color: 8
Size: 903 Color: 13

Bin 60: 21 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 8
Size: 805 Color: 5
Size: 56 Color: 3

Bin 61: 23 of cap free
Amount of items: 2
Items: 
Size: 1236 Color: 2
Size: 1197 Color: 12

Bin 62: 24 of cap free
Amount of items: 2
Items: 
Size: 1626 Color: 18
Size: 806 Color: 5

Bin 63: 24 of cap free
Amount of items: 2
Items: 
Size: 1828 Color: 8
Size: 604 Color: 5

Bin 64: 27 of cap free
Amount of items: 2
Items: 
Size: 1589 Color: 15
Size: 840 Color: 11

Bin 65: 32 of cap free
Amount of items: 2
Items: 
Size: 1382 Color: 9
Size: 1042 Color: 4

Bin 66: 2054 of cap free
Amount of items: 7
Items: 
Size: 80 Color: 12
Size: 64 Color: 6
Size: 64 Color: 0
Size: 56 Color: 13
Size: 48 Color: 17
Size: 48 Color: 10
Size: 42 Color: 14

Total size: 159640
Total free space: 2456

