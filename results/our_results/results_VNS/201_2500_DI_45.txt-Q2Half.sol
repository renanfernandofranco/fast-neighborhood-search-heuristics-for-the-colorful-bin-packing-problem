Capicity Bin: 2404
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1975 Color: 1
Size: 269 Color: 1
Size: 160 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 1
Size: 481 Color: 1
Size: 94 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 1
Size: 140 Color: 0
Size: 110 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1801 Color: 1
Size: 403 Color: 1
Size: 200 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 1
Size: 333 Color: 1
Size: 20 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 1
Size: 262 Color: 1
Size: 120 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1833 Color: 1
Size: 477 Color: 1
Size: 94 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 1
Size: 294 Color: 1
Size: 16 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 1
Size: 315 Color: 1
Size: 72 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 1
Size: 587 Color: 1
Size: 56 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1044 Color: 1
Size: 904 Color: 1
Size: 456 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 1
Size: 204 Color: 1
Size: 82 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 1
Size: 537 Color: 1
Size: 86 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 1
Size: 971 Color: 1
Size: 58 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 1
Size: 275 Color: 1
Size: 166 Color: 0

Bin 16: 0 of cap free
Amount of items: 5
Items: 
Size: 1209 Color: 1
Size: 730 Color: 1
Size: 323 Color: 1
Size: 98 Color: 0
Size: 44 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 558 Color: 1
Size: 188 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 1
Size: 295 Color: 1
Size: 62 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 805 Color: 1
Size: 50 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 1
Size: 299 Color: 1
Size: 196 Color: 0

Bin 21: 0 of cap free
Amount of items: 5
Items: 
Size: 1430 Color: 1
Size: 406 Color: 1
Size: 364 Color: 1
Size: 142 Color: 0
Size: 62 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 1
Size: 859 Color: 1
Size: 106 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1566 Color: 1
Size: 702 Color: 1
Size: 136 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1845 Color: 1
Size: 467 Color: 1
Size: 92 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 1
Size: 493 Color: 1
Size: 102 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 1
Size: 842 Color: 1
Size: 16 Color: 0

Bin 27: 0 of cap free
Amount of items: 5
Items: 
Size: 1802 Color: 1
Size: 242 Color: 1
Size: 210 Color: 1
Size: 80 Color: 0
Size: 70 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1918 Color: 1
Size: 378 Color: 1
Size: 108 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 458 Color: 1
Size: 88 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1987 Color: 1
Size: 349 Color: 1
Size: 68 Color: 0

Bin 31: 0 of cap free
Amount of items: 5
Items: 
Size: 1338 Color: 1
Size: 521 Color: 1
Size: 413 Color: 1
Size: 80 Color: 0
Size: 52 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1470 Color: 1
Size: 890 Color: 1
Size: 44 Color: 0

Bin 33: 0 of cap free
Amount of items: 5
Items: 
Size: 914 Color: 1
Size: 718 Color: 1
Size: 502 Color: 1
Size: 170 Color: 0
Size: 100 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 1
Size: 707 Color: 1
Size: 30 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 622 Color: 1
Size: 64 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 1
Size: 242 Color: 1
Size: 48 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 1
Size: 359 Color: 1
Size: 124 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 1
Size: 1001 Color: 1
Size: 198 Color: 0

Bin 39: 0 of cap free
Amount of items: 5
Items: 
Size: 1002 Color: 1
Size: 814 Color: 1
Size: 340 Color: 1
Size: 192 Color: 0
Size: 56 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 1
Size: 619 Color: 1
Size: 122 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 1
Size: 1001 Color: 1
Size: 24 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 2059 Color: 1
Size: 296 Color: 1
Size: 48 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1203 Color: 1
Size: 1188 Color: 1
Size: 12 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 1
Size: 282 Color: 1
Size: 58 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 601 Color: 1
Size: 64 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 1
Size: 311 Color: 1
Size: 160 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 1
Size: 140 Color: 0
Size: 128 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 1
Size: 615 Color: 1
Size: 104 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 1
Size: 322 Color: 1
Size: 98 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 1
Size: 517 Color: 1
Size: 72 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 2113 Color: 1
Size: 253 Color: 1
Size: 36 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 1206 Color: 1
Size: 1135 Color: 1
Size: 60 Color: 0

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 1394 Color: 1
Size: 831 Color: 1
Size: 176 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 1
Size: 354 Color: 1
Size: 20 Color: 0

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 1
Size: 379 Color: 1
Size: 68 Color: 0

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 1
Size: 349 Color: 1
Size: 12 Color: 0

Bin 57: 11 of cap free
Amount of items: 3
Items: 
Size: 1407 Color: 1
Size: 946 Color: 1
Size: 40 Color: 0

Bin 58: 12 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 1
Size: 622 Color: 1
Size: 168 Color: 0

Bin 59: 12 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 1
Size: 713 Color: 1
Size: 122 Color: 0

Bin 60: 17 of cap free
Amount of items: 3
Items: 
Size: 2101 Color: 1
Size: 226 Color: 1
Size: 60 Color: 0

Bin 61: 74 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 497 Color: 1
Size: 40 Color: 0

Bin 62: 75 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 1
Size: 915 Color: 1
Size: 144 Color: 0

Bin 63: 321 of cap free
Amount of items: 1
Items: 
Size: 2083 Color: 1

Bin 64: 338 of cap free
Amount of items: 1
Items: 
Size: 2066 Color: 1

Bin 65: 350 of cap free
Amount of items: 1
Items: 
Size: 2054 Color: 1

Bin 66: 1163 of cap free
Amount of items: 1
Items: 
Size: 1241 Color: 1

Total size: 156260
Total free space: 2404

