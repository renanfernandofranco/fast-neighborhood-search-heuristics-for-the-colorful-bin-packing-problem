Capicity Bin: 7472
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 3740 Color: 1
Size: 2043 Color: 0
Size: 1325 Color: 0
Size: 220 Color: 9
Size: 144 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4532 Color: 15
Size: 2776 Color: 19
Size: 164 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4576 Color: 12
Size: 2654 Color: 5
Size: 242 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4620 Color: 8
Size: 2666 Color: 12
Size: 186 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4908 Color: 7
Size: 2260 Color: 1
Size: 304 Color: 19

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 5074 Color: 6
Size: 2044 Color: 12
Size: 266 Color: 15
Size: 88 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5260 Color: 7
Size: 2014 Color: 10
Size: 198 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5265 Color: 10
Size: 1221 Color: 16
Size: 986 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 6
Size: 1876 Color: 19
Size: 128 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5515 Color: 1
Size: 986 Color: 13
Size: 971 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5575 Color: 18
Size: 1581 Color: 5
Size: 316 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5838 Color: 4
Size: 1362 Color: 12
Size: 272 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 12
Size: 1233 Color: 2
Size: 246 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6077 Color: 7
Size: 939 Color: 8
Size: 456 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6068 Color: 15
Size: 730 Color: 16
Size: 674 Color: 18

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6150 Color: 16
Size: 1102 Color: 8
Size: 220 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6155 Color: 14
Size: 951 Color: 18
Size: 366 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6166 Color: 4
Size: 1090 Color: 4
Size: 216 Color: 9

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6195 Color: 8
Size: 1065 Color: 15
Size: 212 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 1
Size: 790 Color: 3
Size: 414 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6273 Color: 13
Size: 959 Color: 11
Size: 240 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 14
Size: 850 Color: 0
Size: 348 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 10
Size: 708 Color: 12
Size: 448 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 14
Size: 620 Color: 8
Size: 544 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 8
Size: 620 Color: 12
Size: 538 Color: 14

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6323 Color: 7
Size: 855 Color: 1
Size: 294 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 15
Size: 809 Color: 3
Size: 308 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6363 Color: 3
Size: 925 Color: 11
Size: 184 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 11
Size: 804 Color: 10
Size: 240 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6447 Color: 9
Size: 855 Color: 4
Size: 170 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 6
Size: 546 Color: 16
Size: 472 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 13
Size: 803 Color: 6
Size: 160 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 13
Size: 620 Color: 18
Size: 336 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 11
Size: 736 Color: 13
Size: 150 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 3
Size: 608 Color: 9
Size: 272 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 8
Size: 644 Color: 0
Size: 216 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6628 Color: 12
Size: 658 Color: 4
Size: 186 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 4
Size: 488 Color: 10
Size: 322 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 11
Size: 542 Color: 16
Size: 244 Color: 8

Bin 40: 1 of cap free
Amount of items: 7
Items: 
Size: 3737 Color: 1
Size: 964 Color: 7
Size: 836 Color: 18
Size: 708 Color: 4
Size: 622 Color: 10
Size: 424 Color: 6
Size: 180 Color: 5

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 8
Size: 2652 Color: 6
Size: 196 Color: 17

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4639 Color: 19
Size: 2560 Color: 19
Size: 272 Color: 4

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 13
Size: 2380 Color: 15
Size: 168 Color: 12

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5241 Color: 2
Size: 1694 Color: 5
Size: 536 Color: 18

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 18
Size: 1861 Color: 11
Size: 168 Color: 13

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 12
Size: 1374 Color: 9
Size: 542 Color: 10

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 8
Size: 1538 Color: 15
Size: 190 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 18
Size: 1099 Color: 1
Size: 256 Color: 13

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 10
Size: 1339 Color: 0

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6331 Color: 17
Size: 1140 Color: 13

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6347 Color: 14
Size: 1124 Color: 18

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6463 Color: 8
Size: 870 Color: 10
Size: 138 Color: 19

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 8
Size: 821 Color: 7

Bin 54: 2 of cap free
Amount of items: 5
Items: 
Size: 3741 Color: 8
Size: 2628 Color: 3
Size: 765 Color: 6
Size: 192 Color: 7
Size: 144 Color: 10

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4981 Color: 9
Size: 2361 Color: 15
Size: 128 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 10
Size: 1556 Color: 15
Size: 198 Color: 11

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6149 Color: 13
Size: 1241 Color: 14
Size: 80 Color: 18

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6430 Color: 12
Size: 1040 Color: 8

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 19
Size: 724 Color: 17
Size: 220 Color: 10

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6542 Color: 6
Size: 928 Color: 13

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6692 Color: 3
Size: 778 Color: 2

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 4211 Color: 16
Size: 3106 Color: 9
Size: 152 Color: 13

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 4290 Color: 14
Size: 2719 Color: 8
Size: 460 Color: 1

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 5535 Color: 11
Size: 1934 Color: 0

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 5826 Color: 2
Size: 1643 Color: 16

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 2
Size: 1226 Color: 2
Size: 376 Color: 13

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 14
Size: 966 Color: 3

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 8
Size: 902 Color: 15

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 6593 Color: 13
Size: 876 Color: 11

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6598 Color: 18
Size: 871 Color: 11

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 19
Size: 2290 Color: 12
Size: 88 Color: 17

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 5612 Color: 15
Size: 1676 Color: 18
Size: 180 Color: 1

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 6009 Color: 18
Size: 931 Color: 4
Size: 528 Color: 1

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 11
Size: 3111 Color: 16
Size: 168 Color: 0

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 4420 Color: 15
Size: 2691 Color: 6
Size: 356 Color: 0

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 18
Size: 2023 Color: 19
Size: 272 Color: 1

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6476 Color: 19
Size: 991 Color: 3

Bin 78: 6 of cap free
Amount of items: 4
Items: 
Size: 4219 Color: 10
Size: 2711 Color: 12
Size: 416 Color: 17
Size: 120 Color: 6

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 4726 Color: 9
Size: 2740 Color: 5

Bin 80: 6 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 2
Size: 1841 Color: 1
Size: 344 Color: 13

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6025 Color: 9
Size: 1441 Color: 3

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 7
Size: 1207 Color: 11

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 5703 Color: 16
Size: 1762 Color: 12

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 6290 Color: 7
Size: 1175 Color: 18

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 0
Size: 1002 Color: 12
Size: 24 Color: 12

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 8
Size: 2140 Color: 11
Size: 104 Color: 17

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 6002 Color: 11
Size: 1214 Color: 15
Size: 248 Color: 17

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 1
Size: 861 Color: 12
Size: 48 Color: 19

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 4764 Color: 0
Size: 2699 Color: 15

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 19
Size: 1244 Color: 4

Bin 91: 9 of cap free
Amount of items: 2
Items: 
Size: 6390 Color: 1
Size: 1073 Color: 16

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 12
Size: 755 Color: 9

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 5727 Color: 11
Size: 1599 Color: 10
Size: 136 Color: 13

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 4710 Color: 7
Size: 2751 Color: 15

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 3780 Color: 2
Size: 3680 Color: 12

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 5985 Color: 16
Size: 1475 Color: 10

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6718 Color: 0
Size: 742 Color: 5

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 4195 Color: 1
Size: 3264 Color: 5

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 6487 Color: 16
Size: 972 Color: 7

Bin 100: 14 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 0
Size: 3084 Color: 18
Size: 290 Color: 17

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 5333 Color: 14
Size: 2125 Color: 5

Bin 102: 14 of cap free
Amount of items: 2
Items: 
Size: 5614 Color: 2
Size: 1844 Color: 6

Bin 103: 14 of cap free
Amount of items: 3
Items: 
Size: 5883 Color: 10
Size: 1103 Color: 11
Size: 472 Color: 1

Bin 104: 15 of cap free
Amount of items: 3
Items: 
Size: 5501 Color: 17
Size: 1884 Color: 15
Size: 72 Color: 16

Bin 105: 15 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 18
Size: 1011 Color: 3

Bin 106: 17 of cap free
Amount of items: 2
Items: 
Size: 5378 Color: 10
Size: 2077 Color: 19

Bin 107: 17 of cap free
Amount of items: 2
Items: 
Size: 6283 Color: 15
Size: 1172 Color: 9

Bin 108: 18 of cap free
Amount of items: 3
Items: 
Size: 6018 Color: 11
Size: 1396 Color: 14
Size: 40 Color: 4

Bin 109: 19 of cap free
Amount of items: 2
Items: 
Size: 5001 Color: 2
Size: 2452 Color: 13

Bin 110: 20 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 12
Size: 3114 Color: 13
Size: 64 Color: 10

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 6
Size: 2424 Color: 15

Bin 112: 21 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 8
Size: 1631 Color: 16

Bin 113: 24 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 4
Size: 1468 Color: 16

Bin 114: 26 of cap free
Amount of items: 25
Items: 
Size: 472 Color: 14
Size: 424 Color: 16
Size: 424 Color: 5
Size: 400 Color: 14
Size: 396 Color: 2
Size: 370 Color: 6
Size: 368 Color: 7
Size: 368 Color: 4
Size: 368 Color: 3
Size: 328 Color: 4
Size: 328 Color: 1
Size: 326 Color: 19
Size: 318 Color: 10
Size: 304 Color: 13
Size: 288 Color: 18
Size: 246 Color: 2
Size: 232 Color: 3
Size: 202 Color: 15
Size: 196 Color: 17
Size: 194 Color: 18
Size: 192 Color: 17
Size: 192 Color: 15
Size: 190 Color: 9
Size: 160 Color: 11
Size: 160 Color: 8

Bin 115: 27 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 4
Size: 3102 Color: 2
Size: 172 Color: 3

Bin 116: 28 of cap free
Amount of items: 4
Items: 
Size: 4235 Color: 11
Size: 2375 Color: 7
Size: 678 Color: 0
Size: 156 Color: 4

Bin 117: 28 of cap free
Amount of items: 3
Items: 
Size: 4243 Color: 18
Size: 3113 Color: 13
Size: 88 Color: 16

Bin 118: 30 of cap free
Amount of items: 2
Items: 
Size: 5892 Color: 15
Size: 1550 Color: 12

Bin 119: 32 of cap free
Amount of items: 3
Items: 
Size: 3754 Color: 19
Size: 2685 Color: 13
Size: 1001 Color: 1

Bin 120: 33 of cap free
Amount of items: 2
Items: 
Size: 6307 Color: 4
Size: 1132 Color: 19

Bin 121: 39 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 1
Size: 2731 Color: 13
Size: 410 Color: 4

Bin 122: 40 of cap free
Amount of items: 2
Items: 
Size: 5508 Color: 10
Size: 1924 Color: 17

Bin 123: 41 of cap free
Amount of items: 2
Items: 
Size: 5976 Color: 12
Size: 1455 Color: 2

Bin 124: 44 of cap free
Amount of items: 3
Items: 
Size: 3746 Color: 14
Size: 2302 Color: 3
Size: 1380 Color: 4

Bin 125: 45 of cap free
Amount of items: 2
Items: 
Size: 4599 Color: 2
Size: 2828 Color: 1

Bin 126: 49 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 13
Size: 2061 Color: 16

Bin 127: 53 of cap free
Amount of items: 2
Items: 
Size: 5804 Color: 14
Size: 1615 Color: 18

Bin 128: 56 of cap free
Amount of items: 2
Items: 
Size: 5021 Color: 10
Size: 2395 Color: 15

Bin 129: 57 of cap free
Amount of items: 2
Items: 
Size: 4251 Color: 16
Size: 3164 Color: 12

Bin 130: 59 of cap free
Amount of items: 2
Items: 
Size: 5630 Color: 18
Size: 1783 Color: 14

Bin 131: 70 of cap free
Amount of items: 15
Items: 
Size: 686 Color: 4
Size: 616 Color: 0
Size: 568 Color: 6
Size: 560 Color: 5
Size: 550 Color: 11
Size: 538 Color: 2
Size: 532 Color: 7
Size: 528 Color: 3
Size: 504 Color: 16
Size: 478 Color: 14
Size: 474 Color: 9
Size: 472 Color: 18
Size: 408 Color: 8
Size: 264 Color: 17
Size: 224 Color: 9

Bin 132: 120 of cap free
Amount of items: 4
Items: 
Size: 3738 Color: 14
Size: 2002 Color: 11
Size: 1324 Color: 19
Size: 288 Color: 17

Bin 133: 6070 of cap free
Amount of items: 9
Items: 
Size: 218 Color: 0
Size: 172 Color: 17
Size: 160 Color: 7
Size: 152 Color: 11
Size: 152 Color: 6
Size: 144 Color: 8
Size: 136 Color: 19
Size: 136 Color: 9
Size: 132 Color: 12

Total size: 986304
Total free space: 7472

