Capicity Bin: 7552
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 291
Size: 2781 Color: 259
Size: 150 Color: 26

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5148 Color: 302
Size: 2276 Color: 247
Size: 128 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5778 Color: 322
Size: 1642 Color: 226
Size: 132 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 325
Size: 1554 Color: 221
Size: 148 Color: 24

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5927 Color: 328
Size: 871 Color: 165
Size: 754 Color: 150

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 334
Size: 1148 Color: 194
Size: 400 Color: 107

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 336
Size: 1292 Color: 206
Size: 230 Color: 68

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 337
Size: 1174 Color: 198
Size: 332 Color: 97

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6107 Color: 340
Size: 821 Color: 160
Size: 624 Color: 133

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 345
Size: 999 Color: 175
Size: 392 Color: 105

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 346
Size: 1067 Color: 186
Size: 312 Color: 92

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6180 Color: 347
Size: 1176 Color: 199
Size: 196 Color: 49

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6321 Color: 355
Size: 999 Color: 176
Size: 232 Color: 70

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 356
Size: 1018 Color: 181
Size: 200 Color: 54

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 357
Size: 684 Color: 141
Size: 528 Color: 124

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 360
Size: 870 Color: 164
Size: 328 Color: 96

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6510 Color: 374
Size: 794 Color: 154
Size: 248 Color: 74

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6518 Color: 375
Size: 654 Color: 139
Size: 380 Color: 103

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 381
Size: 548 Color: 126
Size: 416 Color: 111

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 382
Size: 804 Color: 158
Size: 156 Color: 31

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 383
Size: 666 Color: 140
Size: 292 Color: 86

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 384
Size: 757 Color: 151
Size: 194 Color: 48

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 385
Size: 802 Color: 157
Size: 148 Color: 25

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 389
Size: 702 Color: 143
Size: 200 Color: 52

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 390
Size: 751 Color: 149
Size: 148 Color: 23

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 391
Size: 820 Color: 159
Size: 74 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 393
Size: 724 Color: 146
Size: 136 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6695 Color: 395
Size: 715 Color: 144
Size: 142 Color: 21

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 396
Size: 638 Color: 137
Size: 200 Color: 53

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 397
Size: 698 Color: 142
Size: 136 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6740 Color: 398
Size: 504 Color: 122
Size: 308 Color: 90

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 400
Size: 554 Color: 127
Size: 228 Color: 67

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 401
Size: 556 Color: 128
Size: 224 Color: 64

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6790 Color: 402
Size: 540 Color: 125
Size: 222 Color: 61

Bin 35: 1 of cap free
Amount of items: 9
Items: 
Size: 3777 Color: 273
Size: 652 Color: 138
Size: 628 Color: 136
Size: 628 Color: 135
Size: 628 Color: 134
Size: 624 Color: 132
Size: 234 Color: 71
Size: 192 Color: 47
Size: 188 Color: 46

Bin 36: 1 of cap free
Amount of items: 7
Items: 
Size: 3779 Color: 275
Size: 884 Color: 166
Size: 843 Color: 161
Size: 800 Color: 156
Size: 793 Color: 153
Size: 268 Color: 83
Size: 184 Color: 44

Bin 37: 1 of cap free
Amount of items: 7
Items: 
Size: 3780 Color: 276
Size: 1007 Color: 178
Size: 1002 Color: 177
Size: 928 Color: 170
Size: 480 Color: 119
Size: 180 Color: 43
Size: 174 Color: 42

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 298
Size: 2443 Color: 250
Size: 136 Color: 16

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5492 Color: 311
Size: 1647 Color: 227
Size: 412 Color: 108

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5675 Color: 318
Size: 1724 Color: 230
Size: 152 Color: 28

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5789 Color: 323
Size: 1652 Color: 228
Size: 110 Color: 6

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 329
Size: 1299 Color: 207
Size: 320 Color: 93

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 330
Size: 1461 Color: 217
Size: 150 Color: 27

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6137 Color: 342
Size: 1414 Color: 215

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6152 Color: 344
Size: 1399 Color: 214

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6237 Color: 352
Size: 1314 Color: 208

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 6434 Color: 367
Size: 1117 Color: 191

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6539 Color: 377
Size: 1012 Color: 180

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6541 Color: 378
Size: 1010 Color: 179

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 380
Size: 979 Color: 174

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6666 Color: 392
Size: 885 Color: 167

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 6693 Color: 394
Size: 858 Color: 162

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 5772 Color: 321
Size: 1778 Color: 232

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 5858 Color: 326
Size: 1580 Color: 224
Size: 112 Color: 7

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 5995 Color: 333
Size: 1555 Color: 222

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6345 Color: 359
Size: 1205 Color: 202

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6507 Color: 373
Size: 1043 Color: 183

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 388
Size: 905 Color: 168

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6754 Color: 399
Size: 796 Color: 155

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 4847 Color: 295
Size: 2702 Color: 257

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 314
Size: 1043 Color: 184
Size: 934 Color: 171

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 5951 Color: 331
Size: 1598 Color: 225

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 353
Size: 1281 Color: 205

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6419 Color: 366
Size: 1130 Color: 192

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 386
Size: 945 Color: 172

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6625 Color: 387
Size: 924 Color: 169

Bin 67: 4 of cap free
Amount of items: 7
Items: 
Size: 3778 Color: 274
Size: 773 Color: 152
Size: 746 Color: 148
Size: 742 Color: 147
Size: 717 Color: 145
Size: 608 Color: 131
Size: 184 Color: 45

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 341
Size: 1432 Color: 216

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6213 Color: 350
Size: 1335 Color: 210

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6230 Color: 351
Size: 1318 Color: 209

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6411 Color: 365
Size: 1137 Color: 193

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6467 Color: 371
Size: 1081 Color: 188

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 372
Size: 1048 Color: 185

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 4310 Color: 284
Size: 3237 Color: 270

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6386 Color: 363
Size: 1161 Color: 197

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 364
Size: 1151 Color: 195

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 303
Size: 2255 Color: 246
Size: 128 Color: 12

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 313
Size: 1919 Color: 237
Size: 72 Color: 2

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6198 Color: 349
Size: 1348 Color: 211

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6342 Color: 358
Size: 1204 Color: 201

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 369
Size: 1102 Color: 190

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 376
Size: 1020 Color: 182

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 5554 Color: 312
Size: 1991 Color: 239

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 5799 Color: 324
Size: 1746 Color: 231

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 6189 Color: 348
Size: 1356 Color: 212

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6301 Color: 354
Size: 1244 Color: 203

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 4364 Color: 286
Size: 3020 Color: 264
Size: 160 Color: 35

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 316
Size: 1910 Color: 236
Size: 48 Color: 1

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 5732 Color: 320
Size: 1812 Color: 234

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 5879 Color: 327
Size: 1665 Color: 229

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 6060 Color: 339
Size: 1484 Color: 219

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 5262 Color: 306
Size: 2159 Color: 245
Size: 122 Color: 8

Bin 93: 11 of cap free
Amount of items: 2
Items: 
Size: 5577 Color: 315
Size: 1964 Color: 238

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 6146 Color: 343
Size: 1395 Color: 213

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 370
Size: 1076 Color: 187

Bin 96: 11 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 379
Size: 974 Color: 173

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6443 Color: 368
Size: 1097 Color: 189

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 317
Size: 1879 Color: 235

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 5974 Color: 332
Size: 1565 Color: 223

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 6015 Color: 335
Size: 1524 Color: 220

Bin 101: 13 of cap free
Amount of items: 2
Items: 
Size: 6379 Color: 362
Size: 1160 Color: 196

Bin 102: 14 of cap free
Amount of items: 19
Items: 
Size: 600 Color: 130
Size: 560 Color: 129
Size: 518 Color: 123
Size: 500 Color: 121
Size: 488 Color: 120
Size: 476 Color: 118
Size: 456 Color: 117
Size: 450 Color: 116
Size: 448 Color: 115
Size: 430 Color: 114
Size: 424 Color: 113
Size: 416 Color: 112
Size: 414 Color: 110
Size: 336 Color: 99
Size: 208 Color: 57
Size: 208 Color: 56
Size: 208 Color: 55
Size: 200 Color: 51
Size: 198 Color: 50

Bin 103: 14 of cap free
Amount of items: 2
Items: 
Size: 5036 Color: 299
Size: 2502 Color: 252

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 6056 Color: 338
Size: 1482 Color: 218

Bin 105: 15 of cap free
Amount of items: 3
Items: 
Size: 4798 Color: 293
Size: 2597 Color: 254
Size: 142 Color: 20

Bin 106: 16 of cap free
Amount of items: 2
Items: 
Size: 6355 Color: 361
Size: 1181 Color: 200

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 4217 Color: 282
Size: 3147 Color: 268
Size: 168 Color: 37

Bin 108: 20 of cap free
Amount of items: 3
Items: 
Size: 4550 Color: 290
Size: 2828 Color: 262
Size: 154 Color: 29

Bin 109: 21 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 305
Size: 2156 Color: 244
Size: 124 Color: 9

Bin 110: 21 of cap free
Amount of items: 3
Items: 
Size: 5411 Color: 308
Size: 1258 Color: 204
Size: 862 Color: 163

Bin 111: 22 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 310
Size: 2004 Color: 240
Size: 104 Color: 4

Bin 112: 24 of cap free
Amount of items: 3
Items: 
Size: 4214 Color: 281
Size: 3146 Color: 267
Size: 168 Color: 38

Bin 113: 26 of cap free
Amount of items: 4
Items: 
Size: 5196 Color: 304
Size: 2078 Color: 241
Size: 128 Color: 11
Size: 124 Color: 10

Bin 114: 30 of cap free
Amount of items: 2
Items: 
Size: 3786 Color: 277
Size: 3736 Color: 272

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 5420 Color: 309
Size: 2100 Color: 243

Bin 116: 33 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 319
Size: 1785 Color: 233
Size: 44 Color: 0

Bin 117: 35 of cap free
Amount of items: 2
Items: 
Size: 5059 Color: 300
Size: 2458 Color: 251

Bin 118: 40 of cap free
Amount of items: 26
Items: 
Size: 412 Color: 109
Size: 398 Color: 106
Size: 382 Color: 104
Size: 374 Color: 102
Size: 356 Color: 101
Size: 352 Color: 100
Size: 336 Color: 98
Size: 328 Color: 95
Size: 324 Color: 94
Size: 312 Color: 91
Size: 296 Color: 89
Size: 296 Color: 88
Size: 292 Color: 87
Size: 280 Color: 85
Size: 256 Color: 76
Size: 252 Color: 75
Size: 248 Color: 73
Size: 240 Color: 72
Size: 232 Color: 69
Size: 226 Color: 66
Size: 226 Color: 65
Size: 224 Color: 63
Size: 224 Color: 62
Size: 220 Color: 60
Size: 218 Color: 59
Size: 208 Color: 58

Bin 119: 46 of cap free
Amount of items: 3
Items: 
Size: 4193 Color: 280
Size: 3145 Color: 266
Size: 168 Color: 39

Bin 120: 53 of cap free
Amount of items: 4
Items: 
Size: 4437 Color: 287
Size: 2742 Color: 258
Size: 160 Color: 34
Size: 160 Color: 33

Bin 121: 56 of cap free
Amount of items: 3
Items: 
Size: 4970 Color: 297
Size: 2390 Color: 249
Size: 136 Color: 18

Bin 122: 64 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 289
Size: 2808 Color: 261
Size: 156 Color: 30

Bin 123: 64 of cap free
Amount of items: 2
Items: 
Size: 4828 Color: 294
Size: 2660 Color: 256

Bin 124: 64 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 301
Size: 2298 Color: 248
Size: 128 Color: 14

Bin 125: 65 of cap free
Amount of items: 2
Items: 
Size: 4963 Color: 296
Size: 2524 Color: 253

Bin 126: 68 of cap free
Amount of items: 3
Items: 
Size: 4686 Color: 292
Size: 2654 Color: 255
Size: 144 Color: 22

Bin 127: 70 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 307
Size: 2079 Color: 242
Size: 104 Color: 5

Bin 128: 74 of cap free
Amount of items: 3
Items: 
Size: 4164 Color: 279
Size: 3142 Color: 265
Size: 172 Color: 40

Bin 129: 104 of cap free
Amount of items: 3
Items: 
Size: 4318 Color: 285
Size: 2966 Color: 263
Size: 164 Color: 36

Bin 130: 126 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 288
Size: 2782 Color: 260
Size: 158 Color: 32

Bin 131: 136 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 278
Size: 3312 Color: 271
Size: 172 Color: 41

Bin 132: 142 of cap free
Amount of items: 2
Items: 
Size: 4262 Color: 283
Size: 3148 Color: 269

Bin 133: 5706 of cap free
Amount of items: 7
Items: 
Size: 278 Color: 84
Size: 266 Color: 82
Size: 264 Color: 81
Size: 264 Color: 80
Size: 260 Color: 79
Size: 258 Color: 78
Size: 256 Color: 77

Total size: 996864
Total free space: 7552

