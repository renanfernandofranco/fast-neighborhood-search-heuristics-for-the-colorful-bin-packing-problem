Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 5
Size: 354 Color: 0
Size: 288 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 10
Size: 315 Color: 9
Size: 296 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 258 Color: 5
Size: 253 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 6
Size: 310 Color: 5
Size: 282 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 345 Color: 13
Size: 282 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 18
Size: 320 Color: 11
Size: 250 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 2
Size: 344 Color: 1
Size: 299 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 330 Color: 3
Size: 277 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 16
Size: 368 Color: 9
Size: 265 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 16
Size: 332 Color: 16
Size: 254 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7
Size: 334 Color: 4
Size: 280 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 9
Size: 335 Color: 17
Size: 286 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 11
Size: 323 Color: 17
Size: 291 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 351 Color: 3
Size: 284 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 2
Size: 285 Color: 17
Size: 275 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 7
Size: 332 Color: 1
Size: 323 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 18
Size: 290 Color: 2
Size: 261 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 11
Size: 319 Color: 13
Size: 272 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 291 Color: 5
Size: 266 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 264 Color: 7
Size: 251 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 16
Size: 334 Color: 14
Size: 295 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 343 Color: 19
Size: 280 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 12
Size: 353 Color: 6
Size: 251 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 350 Color: 4
Size: 269 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 2
Size: 352 Color: 18
Size: 294 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 19
Size: 322 Color: 18
Size: 287 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 356 Color: 12
Size: 262 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 19
Size: 347 Color: 6
Size: 295 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 15
Size: 308 Color: 3
Size: 307 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 18
Size: 279 Color: 14
Size: 265 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 5
Size: 266 Color: 9
Size: 258 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 285 Color: 12
Size: 266 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 341 Color: 13
Size: 277 Color: 18

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 11
Size: 269 Color: 10
Size: 258 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 324 Color: 2
Size: 285 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 15
Size: 361 Color: 11
Size: 268 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8
Size: 331 Color: 4
Size: 254 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 8
Size: 313 Color: 8
Size: 292 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 15
Size: 305 Color: 11
Size: 297 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 299 Color: 0
Size: 268 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 19
Size: 337 Color: 8
Size: 285 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 3
Size: 326 Color: 5
Size: 325 Color: 17

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 19
Size: 316 Color: 4
Size: 285 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 5
Size: 252 Color: 3
Size: 250 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 17
Size: 339 Color: 12
Size: 254 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 310 Color: 17
Size: 309 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 2
Size: 323 Color: 14
Size: 313 Color: 17

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 8
Size: 323 Color: 18
Size: 323 Color: 13

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 14
Size: 293 Color: 15
Size: 282 Color: 15

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 18
Size: 323 Color: 15
Size: 261 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 10
Size: 255 Color: 18
Size: 250 Color: 13

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 17
Size: 278 Color: 5
Size: 271 Color: 10

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 18
Size: 301 Color: 18
Size: 268 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 8
Size: 303 Color: 4
Size: 293 Color: 6

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 16
Size: 321 Color: 1
Size: 288 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 17
Size: 352 Color: 7
Size: 266 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 7
Size: 289 Color: 8
Size: 265 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 19
Size: 312 Color: 0
Size: 257 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 9
Size: 303 Color: 19
Size: 303 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 16
Size: 263 Color: 18
Size: 254 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 11
Size: 302 Color: 7
Size: 267 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 14
Size: 287 Color: 8
Size: 261 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 329 Color: 3
Size: 258 Color: 9

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 15
Size: 325 Color: 9
Size: 283 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 6
Size: 349 Color: 13
Size: 299 Color: 14

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 13
Size: 327 Color: 2
Size: 287 Color: 6

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 2
Size: 279 Color: 15
Size: 270 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 16
Size: 302 Color: 15
Size: 259 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 19
Size: 313 Color: 10
Size: 304 Color: 5

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 7
Size: 327 Color: 8
Size: 316 Color: 14

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 8
Size: 283 Color: 17
Size: 251 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 7
Size: 340 Color: 14
Size: 259 Color: 5

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 2
Size: 334 Color: 3
Size: 295 Color: 6

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 324 Color: 6
Size: 298 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 359 Color: 17
Size: 275 Color: 12

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 321 Color: 11
Size: 277 Color: 2

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 5
Size: 275 Color: 1
Size: 264 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 9
Size: 335 Color: 14
Size: 322 Color: 11

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 5
Size: 265 Color: 12
Size: 256 Color: 7

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 281 Color: 0
Size: 262 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 6
Size: 357 Color: 4
Size: 256 Color: 4

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 10
Size: 277 Color: 6
Size: 259 Color: 14

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 18
Size: 381 Color: 6
Size: 275 Color: 14

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 18
Size: 278 Color: 7
Size: 276 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 258 Color: 4
Size: 255 Color: 17

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 306 Color: 5
Size: 286 Color: 8

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 12
Size: 254 Color: 11
Size: 250 Color: 19

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 304 Color: 14
Size: 255 Color: 15

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 11
Size: 308 Color: 1
Size: 263 Color: 5

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 7
Size: 262 Color: 7
Size: 251 Color: 1

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 15
Size: 286 Color: 6
Size: 280 Color: 19

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 18
Size: 300 Color: 6
Size: 277 Color: 12

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 19
Size: 338 Color: 16
Size: 280 Color: 12

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 13
Size: 333 Color: 18
Size: 320 Color: 11

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 11
Size: 327 Color: 9
Size: 262 Color: 15

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 318 Color: 6
Size: 251 Color: 13

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 326 Color: 18
Size: 284 Color: 15

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 353 Color: 13
Size: 261 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 14
Size: 316 Color: 15
Size: 289 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 14
Size: 256 Color: 13
Size: 256 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 309 Color: 12
Size: 255 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 16
Size: 327 Color: 1
Size: 309 Color: 18

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 10
Size: 346 Color: 2
Size: 303 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 8
Size: 350 Color: 6
Size: 250 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 352 Color: 10
Size: 262 Color: 11

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 11
Size: 323 Color: 6
Size: 285 Color: 18

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 9
Size: 252 Color: 16
Size: 250 Color: 6

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 7
Size: 297 Color: 18
Size: 267 Color: 14

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 6
Size: 278 Color: 0
Size: 270 Color: 6

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 4
Size: 295 Color: 16
Size: 284 Color: 6

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 298 Color: 7
Size: 275 Color: 18

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 19
Size: 281 Color: 3
Size: 268 Color: 16

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 7
Size: 308 Color: 3
Size: 262 Color: 15

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 313 Color: 11
Size: 256 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 269 Color: 19
Size: 259 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 319 Color: 18
Size: 276 Color: 10

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 11
Size: 318 Color: 14
Size: 276 Color: 14

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 16
Size: 333 Color: 8
Size: 267 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 325 Color: 13
Size: 280 Color: 6

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 6
Size: 331 Color: 14
Size: 290 Color: 16

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 6
Size: 258 Color: 4
Size: 252 Color: 18

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 17
Size: 351 Color: 10
Size: 282 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 324 Color: 17
Size: 284 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 352 Color: 7
Size: 277 Color: 17

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 309 Color: 16
Size: 295 Color: 2

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 334 Color: 0
Size: 261 Color: 9

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 7
Size: 292 Color: 19
Size: 273 Color: 18

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 269 Color: 5
Size: 263 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 11
Size: 288 Color: 18
Size: 266 Color: 14

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 7
Size: 329 Color: 0
Size: 263 Color: 12

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 17
Size: 333 Color: 5
Size: 306 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 10
Size: 271 Color: 1
Size: 262 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 14
Size: 347 Color: 9
Size: 306 Color: 15

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 11
Size: 310 Color: 17
Size: 275 Color: 18

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 8
Size: 300 Color: 13
Size: 294 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 15
Size: 300 Color: 14
Size: 279 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 309 Color: 19
Size: 288 Color: 17

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 356 Color: 10
Size: 258 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 13
Size: 292 Color: 7
Size: 286 Color: 5

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 3
Size: 266 Color: 19
Size: 254 Color: 7

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 13
Size: 333 Color: 12
Size: 250 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 305 Color: 13
Size: 278 Color: 12

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 19
Size: 351 Color: 14
Size: 260 Color: 15

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 4
Size: 265 Color: 11
Size: 255 Color: 16

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 332 Color: 9
Size: 311 Color: 8

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 345 Color: 17
Size: 303 Color: 15

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 7
Size: 301 Color: 8
Size: 259 Color: 19

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 15
Size: 270 Color: 8
Size: 259 Color: 15

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 2
Size: 274 Color: 16
Size: 273 Color: 2

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 5
Size: 316 Color: 7
Size: 304 Color: 13

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 16
Size: 353 Color: 6
Size: 251 Color: 19

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 12
Size: 301 Color: 13
Size: 280 Color: 7

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 11
Size: 301 Color: 17
Size: 296 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 8
Size: 270 Color: 7
Size: 260 Color: 12

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 10
Size: 290 Color: 16
Size: 280 Color: 11

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 5
Size: 329 Color: 7
Size: 266 Color: 9

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 17
Size: 267 Color: 17
Size: 250 Color: 19

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 12
Size: 306 Color: 12
Size: 297 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 355 Color: 18
Size: 278 Color: 12

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 6
Size: 346 Color: 9
Size: 306 Color: 19

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 5
Size: 306 Color: 19
Size: 277 Color: 14

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7
Size: 327 Color: 19
Size: 280 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 306 Color: 12
Size: 299 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 19
Size: 332 Color: 3
Size: 269 Color: 5

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 10
Size: 297 Color: 5
Size: 268 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 9
Size: 278 Color: 3
Size: 252 Color: 12

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 8
Size: 262 Color: 1
Size: 257 Color: 0

Total size: 167167
Total free space: 0

