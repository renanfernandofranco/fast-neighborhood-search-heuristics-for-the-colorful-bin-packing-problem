Capicity Bin: 7744
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 8
Size: 2108 Color: 17
Size: 202 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 14
Size: 1867 Color: 4
Size: 372 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5662 Color: 8
Size: 1844 Color: 15
Size: 238 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 3
Size: 1657 Color: 7
Size: 344 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5769 Color: 8
Size: 1647 Color: 6
Size: 328 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 2
Size: 1476 Color: 15
Size: 408 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 17
Size: 1538 Color: 19
Size: 304 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6058 Color: 5
Size: 1558 Color: 13
Size: 128 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 7
Size: 968 Color: 7
Size: 516 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6285 Color: 3
Size: 806 Color: 5
Size: 653 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 13
Size: 1201 Color: 17
Size: 242 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 9
Size: 1146 Color: 1
Size: 292 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 0
Size: 1012 Color: 3
Size: 424 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 6
Size: 1177 Color: 15
Size: 234 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 11
Size: 1154 Color: 16
Size: 236 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 8
Size: 1228 Color: 4
Size: 146 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 4
Size: 1242 Color: 15
Size: 92 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 13
Size: 701 Color: 18
Size: 536 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 8
Size: 900 Color: 5
Size: 312 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 15
Size: 860 Color: 12
Size: 330 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 15
Size: 742 Color: 3
Size: 384 Color: 7

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 3
Size: 668 Color: 3
Size: 416 Color: 10

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6673 Color: 17
Size: 893 Color: 8
Size: 178 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 10
Size: 788 Color: 7
Size: 240 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6717 Color: 15
Size: 857 Color: 14
Size: 170 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 5
Size: 772 Color: 11
Size: 208 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 8
Size: 644 Color: 6
Size: 312 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 11
Size: 933 Color: 17
Size: 28 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 13
Size: 556 Color: 9
Size: 384 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6806 Color: 3
Size: 644 Color: 1
Size: 294 Color: 8

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 0
Size: 762 Color: 1
Size: 152 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 16
Size: 640 Color: 15
Size: 228 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6892 Color: 0
Size: 716 Color: 18
Size: 136 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6933 Color: 8
Size: 677 Color: 15
Size: 134 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 14
Size: 658 Color: 1
Size: 138 Color: 14

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4393 Color: 11
Size: 3222 Color: 4
Size: 128 Color: 5

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5191 Color: 13
Size: 2440 Color: 8
Size: 112 Color: 10

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 10
Size: 1675 Color: 14
Size: 536 Color: 18

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 18
Size: 1306 Color: 1
Size: 464 Color: 6

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5977 Color: 17
Size: 1566 Color: 14
Size: 200 Color: 8

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6159 Color: 13
Size: 1168 Color: 14
Size: 416 Color: 7

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6305 Color: 14
Size: 1108 Color: 8
Size: 330 Color: 16

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6309 Color: 4
Size: 842 Color: 0
Size: 592 Color: 2

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 11
Size: 1194 Color: 8
Size: 236 Color: 19

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 0
Size: 1197 Color: 13
Size: 232 Color: 19

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6377 Color: 14
Size: 1366 Color: 6

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6417 Color: 9
Size: 1086 Color: 8
Size: 240 Color: 9

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 6734 Color: 2
Size: 1009 Color: 1

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6809 Color: 18
Size: 934 Color: 9

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6835 Color: 10
Size: 748 Color: 8
Size: 160 Color: 7

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6961 Color: 10
Size: 782 Color: 2

Bin 52: 2 of cap free
Amount of items: 5
Items: 
Size: 3874 Color: 19
Size: 1661 Color: 10
Size: 1276 Color: 14
Size: 801 Color: 8
Size: 130 Color: 1

Bin 53: 2 of cap free
Amount of items: 4
Items: 
Size: 3882 Color: 3
Size: 3220 Color: 2
Size: 368 Color: 1
Size: 272 Color: 8

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4832 Color: 15
Size: 2426 Color: 7
Size: 484 Color: 1

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 18
Size: 2586 Color: 16
Size: 168 Color: 17

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 4994 Color: 5
Size: 2604 Color: 7
Size: 144 Color: 5

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 2294 Color: 3
Size: 164 Color: 11

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 0
Size: 1244 Color: 9
Size: 416 Color: 1

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6143 Color: 8
Size: 913 Color: 5
Size: 686 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6192 Color: 9
Size: 1386 Color: 8
Size: 164 Color: 2

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 6538 Color: 4
Size: 1204 Color: 15

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 17
Size: 644 Color: 5
Size: 472 Color: 8

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 6753 Color: 6
Size: 989 Color: 18

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 16
Size: 890 Color: 0

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6922 Color: 12
Size: 820 Color: 15

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 4
Size: 424 Color: 8
Size: 368 Color: 11

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 19
Size: 2778 Color: 3
Size: 132 Color: 15

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 8
Size: 2708 Color: 8
Size: 206 Color: 12

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 13
Size: 2431 Color: 18
Size: 112 Color: 19

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6420 Color: 12
Size: 1321 Color: 4

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 6562 Color: 5
Size: 1019 Color: 8
Size: 160 Color: 3

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 14
Size: 1058 Color: 16
Size: 34 Color: 12

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 17
Size: 3392 Color: 10
Size: 176 Color: 11

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 4
Size: 1217 Color: 16

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 5
Size: 2122 Color: 2
Size: 128 Color: 8

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 6
Size: 1009 Color: 8
Size: 456 Color: 14

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 14
Size: 1463 Color: 0

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 18
Size: 1122 Color: 15

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6745 Color: 5
Size: 994 Color: 9

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 12
Size: 1758 Color: 3

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6238 Color: 12
Size: 1500 Color: 10

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6258 Color: 7
Size: 1480 Color: 12

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 6678 Color: 12
Size: 1060 Color: 16

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 4397 Color: 3
Size: 3142 Color: 0
Size: 198 Color: 1

Bin 85: 7 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 2
Size: 1877 Color: 5
Size: 640 Color: 10

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 6402 Color: 3
Size: 1335 Color: 12

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 11
Size: 1202 Color: 0

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 13
Size: 917 Color: 5

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 4500 Color: 14
Size: 2724 Color: 13
Size: 512 Color: 12

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 5493 Color: 13
Size: 2131 Color: 17
Size: 112 Color: 10

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 11
Size: 776 Color: 3
Size: 2 Color: 9

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 9
Size: 1572 Color: 3
Size: 56 Color: 0

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 6858 Color: 5
Size: 876 Color: 9

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 6645 Color: 12
Size: 1056 Color: 17
Size: 32 Color: 5

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 4
Size: 1258 Color: 8
Size: 608 Color: 19

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 6701 Color: 13
Size: 1031 Color: 5

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6824 Color: 5
Size: 908 Color: 6

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 6903 Color: 11
Size: 827 Color: 10

Bin 99: 15 of cap free
Amount of items: 5
Items: 
Size: 3873 Color: 7
Size: 1162 Color: 15
Size: 1157 Color: 4
Size: 1141 Color: 10
Size: 396 Color: 9

Bin 100: 16 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 2
Size: 2002 Color: 12
Size: 88 Color: 11

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 5676 Color: 12
Size: 2052 Color: 5

Bin 102: 16 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 18
Size: 1724 Color: 3
Size: 56 Color: 4

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 5989 Color: 10
Size: 1738 Color: 9

Bin 104: 17 of cap free
Amount of items: 2
Items: 
Size: 6442 Color: 17
Size: 1285 Color: 14

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 4642 Color: 17
Size: 3084 Color: 16

Bin 106: 18 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 2
Size: 1114 Color: 1

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 5
Size: 948 Color: 11

Bin 108: 20 of cap free
Amount of items: 2
Items: 
Size: 3884 Color: 19
Size: 3840 Color: 17

Bin 109: 23 of cap free
Amount of items: 2
Items: 
Size: 6357 Color: 10
Size: 1364 Color: 12

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 6476 Color: 11
Size: 1245 Color: 16

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 6611 Color: 17
Size: 1107 Color: 1

Bin 112: 30 of cap free
Amount of items: 14
Items: 
Size: 967 Color: 12
Size: 759 Color: 19
Size: 756 Color: 5
Size: 662 Color: 16
Size: 558 Color: 14
Size: 552 Color: 7
Size: 488 Color: 18
Size: 486 Color: 9
Size: 484 Color: 15
Size: 472 Color: 17
Size: 456 Color: 13
Size: 424 Color: 0
Size: 374 Color: 10
Size: 276 Color: 1

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 6700 Color: 0
Size: 1014 Color: 19

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 5412 Color: 7
Size: 2300 Color: 18

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 6108 Color: 11
Size: 1604 Color: 7

Bin 116: 33 of cap free
Amount of items: 2
Items: 
Size: 4484 Color: 10
Size: 3227 Color: 0

Bin 117: 34 of cap free
Amount of items: 2
Items: 
Size: 5346 Color: 16
Size: 2364 Color: 11

Bin 118: 35 of cap free
Amount of items: 2
Items: 
Size: 4916 Color: 6
Size: 2793 Color: 4

Bin 119: 35 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 2
Size: 1881 Color: 13

Bin 120: 38 of cap free
Amount of items: 2
Items: 
Size: 4834 Color: 13
Size: 2872 Color: 3

Bin 121: 38 of cap free
Amount of items: 3
Items: 
Size: 5189 Color: 16
Size: 2429 Color: 10
Size: 88 Color: 1

Bin 122: 39 of cap free
Amount of items: 2
Items: 
Size: 5757 Color: 4
Size: 1948 Color: 7

Bin 123: 44 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 3
Size: 2980 Color: 8
Size: 132 Color: 1

Bin 124: 47 of cap free
Amount of items: 2
Items: 
Size: 6220 Color: 13
Size: 1477 Color: 0

Bin 125: 48 of cap free
Amount of items: 2
Items: 
Size: 4628 Color: 8
Size: 3068 Color: 14

Bin 126: 65 of cap free
Amount of items: 2
Items: 
Size: 5753 Color: 4
Size: 1926 Color: 11

Bin 127: 70 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 18
Size: 2129 Color: 2
Size: 1669 Color: 4

Bin 128: 77 of cap free
Amount of items: 3
Items: 
Size: 3875 Color: 19
Size: 2791 Color: 7
Size: 1001 Color: 18

Bin 129: 78 of cap free
Amount of items: 32
Items: 
Size: 374 Color: 0
Size: 348 Color: 2
Size: 344 Color: 12
Size: 332 Color: 17
Size: 312 Color: 9
Size: 296 Color: 16
Size: 288 Color: 0
Size: 272 Color: 1
Size: 266 Color: 12
Size: 264 Color: 0
Size: 248 Color: 8
Size: 248 Color: 7
Size: 244 Color: 4
Size: 238 Color: 11
Size: 230 Color: 1
Size: 228 Color: 1
Size: 226 Color: 7
Size: 220 Color: 10
Size: 220 Color: 5
Size: 220 Color: 4
Size: 216 Color: 9
Size: 216 Color: 0
Size: 204 Color: 19
Size: 200 Color: 5
Size: 196 Color: 16
Size: 184 Color: 18
Size: 184 Color: 4
Size: 182 Color: 8
Size: 182 Color: 4
Size: 168 Color: 2
Size: 160 Color: 10
Size: 156 Color: 11

Bin 130: 104 of cap free
Amount of items: 2
Items: 
Size: 4244 Color: 17
Size: 3396 Color: 9

Bin 131: 104 of cap free
Amount of items: 2
Items: 
Size: 4414 Color: 3
Size: 3226 Color: 2

Bin 132: 121 of cap free
Amount of items: 2
Items: 
Size: 4398 Color: 8
Size: 3225 Color: 7

Bin 133: 6106 of cap free
Amount of items: 11
Items: 
Size: 176 Color: 17
Size: 168 Color: 12
Size: 152 Color: 17
Size: 152 Color: 10
Size: 150 Color: 2
Size: 148 Color: 8
Size: 144 Color: 13
Size: 144 Color: 9
Size: 140 Color: 11
Size: 136 Color: 5
Size: 128 Color: 1

Total size: 1022208
Total free space: 7744

