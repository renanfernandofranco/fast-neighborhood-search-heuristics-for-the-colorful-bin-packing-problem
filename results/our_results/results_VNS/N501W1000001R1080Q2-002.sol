Capicity Bin: 1000001
Lower Bound: 232

Bins used: 236
Amount of Colors: 2

Bin 1: 2 of cap free
Amount of items: 3
Items: 
Size: 756761 Color: 0
Size: 140787 Color: 1
Size: 102451 Color: 1

Bin 2: 3 of cap free
Amount of items: 2
Items: 
Size: 565573 Color: 1
Size: 434425 Color: 0

Bin 3: 3 of cap free
Amount of items: 3
Items: 
Size: 538700 Color: 0
Size: 359492 Color: 1
Size: 101806 Color: 0

Bin 4: 4 of cap free
Amount of items: 3
Items: 
Size: 484577 Color: 0
Size: 392425 Color: 1
Size: 122995 Color: 1

Bin 5: 4 of cap free
Amount of items: 3
Items: 
Size: 773922 Color: 1
Size: 121668 Color: 1
Size: 104407 Color: 0

Bin 6: 5 of cap free
Amount of items: 3
Items: 
Size: 586821 Color: 0
Size: 311574 Color: 1
Size: 101601 Color: 1

Bin 7: 10 of cap free
Amount of items: 3
Items: 
Size: 700743 Color: 0
Size: 151034 Color: 1
Size: 148214 Color: 0

Bin 8: 10 of cap free
Amount of items: 3
Items: 
Size: 694440 Color: 0
Size: 196471 Color: 1
Size: 109080 Color: 0

Bin 9: 12 of cap free
Amount of items: 3
Items: 
Size: 644493 Color: 1
Size: 222749 Color: 0
Size: 132747 Color: 1

Bin 10: 12 of cap free
Amount of items: 3
Items: 
Size: 713997 Color: 1
Size: 175664 Color: 0
Size: 110328 Color: 1

Bin 11: 13 of cap free
Amount of items: 3
Items: 
Size: 707445 Color: 0
Size: 152062 Color: 1
Size: 140481 Color: 0

Bin 12: 16 of cap free
Amount of items: 3
Items: 
Size: 753792 Color: 1
Size: 125385 Color: 0
Size: 120808 Color: 0

Bin 13: 22 of cap free
Amount of items: 3
Items: 
Size: 584927 Color: 1
Size: 254359 Color: 1
Size: 160693 Color: 0

Bin 14: 28 of cap free
Amount of items: 3
Items: 
Size: 588802 Color: 0
Size: 207316 Color: 0
Size: 203855 Color: 1

Bin 15: 30 of cap free
Amount of items: 3
Items: 
Size: 567612 Color: 0
Size: 260497 Color: 1
Size: 171862 Color: 0

Bin 16: 36 of cap free
Amount of items: 3
Items: 
Size: 652117 Color: 0
Size: 230690 Color: 1
Size: 117158 Color: 1

Bin 17: 44 of cap free
Amount of items: 3
Items: 
Size: 707583 Color: 1
Size: 181771 Color: 0
Size: 110603 Color: 1

Bin 18: 46 of cap free
Amount of items: 2
Items: 
Size: 715927 Color: 1
Size: 284028 Color: 0

Bin 19: 50 of cap free
Amount of items: 3
Items: 
Size: 653448 Color: 1
Size: 197218 Color: 0
Size: 149285 Color: 0

Bin 20: 60 of cap free
Amount of items: 3
Items: 
Size: 709636 Color: 0
Size: 172628 Color: 1
Size: 117677 Color: 1

Bin 21: 65 of cap free
Amount of items: 2
Items: 
Size: 777137 Color: 0
Size: 222799 Color: 1

Bin 22: 66 of cap free
Amount of items: 3
Items: 
Size: 533278 Color: 1
Size: 277800 Color: 0
Size: 188857 Color: 0

Bin 23: 76 of cap free
Amount of items: 3
Items: 
Size: 587627 Color: 1
Size: 277417 Color: 1
Size: 134881 Color: 0

Bin 24: 80 of cap free
Amount of items: 2
Items: 
Size: 516264 Color: 1
Size: 483657 Color: 0

Bin 25: 81 of cap free
Amount of items: 2
Items: 
Size: 756448 Color: 1
Size: 243472 Color: 0

Bin 26: 85 of cap free
Amount of items: 3
Items: 
Size: 663293 Color: 0
Size: 173428 Color: 1
Size: 163195 Color: 0

Bin 27: 85 of cap free
Amount of items: 2
Items: 
Size: 738958 Color: 1
Size: 260958 Color: 0

Bin 28: 86 of cap free
Amount of items: 3
Items: 
Size: 587734 Color: 0
Size: 252637 Color: 1
Size: 159544 Color: 0

Bin 29: 86 of cap free
Amount of items: 2
Items: 
Size: 757739 Color: 1
Size: 242176 Color: 0

Bin 30: 93 of cap free
Amount of items: 3
Items: 
Size: 658234 Color: 0
Size: 181616 Color: 1
Size: 160058 Color: 0

Bin 31: 103 of cap free
Amount of items: 3
Items: 
Size: 475106 Color: 0
Size: 322557 Color: 1
Size: 202235 Color: 1

Bin 32: 105 of cap free
Amount of items: 2
Items: 
Size: 625669 Color: 0
Size: 374227 Color: 1

Bin 33: 107 of cap free
Amount of items: 3
Items: 
Size: 691402 Color: 1
Size: 157976 Color: 0
Size: 150516 Color: 0

Bin 34: 121 of cap free
Amount of items: 2
Items: 
Size: 665839 Color: 0
Size: 334041 Color: 1

Bin 35: 130 of cap free
Amount of items: 3
Items: 
Size: 650090 Color: 0
Size: 204330 Color: 1
Size: 145451 Color: 0

Bin 36: 151 of cap free
Amount of items: 3
Items: 
Size: 654296 Color: 0
Size: 205624 Color: 1
Size: 139930 Color: 1

Bin 37: 162 of cap free
Amount of items: 3
Items: 
Size: 562978 Color: 0
Size: 309374 Color: 0
Size: 127487 Color: 1

Bin 38: 168 of cap free
Amount of items: 3
Items: 
Size: 639856 Color: 0
Size: 229001 Color: 0
Size: 130976 Color: 1

Bin 39: 169 of cap free
Amount of items: 3
Items: 
Size: 703425 Color: 0
Size: 165866 Color: 0
Size: 130541 Color: 1

Bin 40: 193 of cap free
Amount of items: 2
Items: 
Size: 699481 Color: 1
Size: 300327 Color: 0

Bin 41: 196 of cap free
Amount of items: 3
Items: 
Size: 687279 Color: 1
Size: 160818 Color: 0
Size: 151708 Color: 1

Bin 42: 197 of cap free
Amount of items: 3
Items: 
Size: 670059 Color: 0
Size: 198734 Color: 1
Size: 131011 Color: 0

Bin 43: 215 of cap free
Amount of items: 2
Items: 
Size: 607040 Color: 0
Size: 392746 Color: 1

Bin 44: 239 of cap free
Amount of items: 2
Items: 
Size: 530814 Color: 1
Size: 468948 Color: 0

Bin 45: 243 of cap free
Amount of items: 2
Items: 
Size: 504224 Color: 1
Size: 495534 Color: 0

Bin 46: 252 of cap free
Amount of items: 2
Items: 
Size: 562425 Color: 1
Size: 437324 Color: 0

Bin 47: 260 of cap free
Amount of items: 3
Items: 
Size: 488094 Color: 0
Size: 282130 Color: 1
Size: 229517 Color: 1

Bin 48: 270 of cap free
Amount of items: 2
Items: 
Size: 631708 Color: 1
Size: 368023 Color: 0

Bin 49: 287 of cap free
Amount of items: 2
Items: 
Size: 736665 Color: 1
Size: 263049 Color: 0

Bin 50: 316 of cap free
Amount of items: 2
Items: 
Size: 543527 Color: 0
Size: 456158 Color: 1

Bin 51: 318 of cap free
Amount of items: 2
Items: 
Size: 653633 Color: 0
Size: 346050 Color: 1

Bin 52: 355 of cap free
Amount of items: 2
Items: 
Size: 557477 Color: 0
Size: 442169 Color: 1

Bin 53: 372 of cap free
Amount of items: 3
Items: 
Size: 597085 Color: 0
Size: 284084 Color: 1
Size: 118460 Color: 1

Bin 54: 392 of cap free
Amount of items: 2
Items: 
Size: 609898 Color: 1
Size: 389711 Color: 0

Bin 55: 395 of cap free
Amount of items: 3
Items: 
Size: 652441 Color: 0
Size: 198289 Color: 1
Size: 148876 Color: 0

Bin 56: 413 of cap free
Amount of items: 2
Items: 
Size: 504918 Color: 0
Size: 494670 Color: 1

Bin 57: 429 of cap free
Amount of items: 2
Items: 
Size: 568566 Color: 0
Size: 431006 Color: 1

Bin 58: 439 of cap free
Amount of items: 2
Items: 
Size: 797893 Color: 0
Size: 201669 Color: 1

Bin 59: 444 of cap free
Amount of items: 3
Items: 
Size: 576634 Color: 0
Size: 300859 Color: 0
Size: 122064 Color: 1

Bin 60: 446 of cap free
Amount of items: 3
Items: 
Size: 592453 Color: 0
Size: 264322 Color: 1
Size: 142780 Color: 0

Bin 61: 450 of cap free
Amount of items: 3
Items: 
Size: 698768 Color: 0
Size: 165719 Color: 0
Size: 135064 Color: 1

Bin 62: 451 of cap free
Amount of items: 3
Items: 
Size: 580047 Color: 1
Size: 244145 Color: 0
Size: 175358 Color: 0

Bin 63: 471 of cap free
Amount of items: 2
Items: 
Size: 528690 Color: 0
Size: 470840 Color: 1

Bin 64: 482 of cap free
Amount of items: 3
Items: 
Size: 531193 Color: 1
Size: 240240 Color: 0
Size: 228086 Color: 1

Bin 65: 486 of cap free
Amount of items: 2
Items: 
Size: 677071 Color: 0
Size: 322444 Color: 1

Bin 66: 489 of cap free
Amount of items: 3
Items: 
Size: 563200 Color: 0
Size: 269636 Color: 1
Size: 166676 Color: 1

Bin 67: 490 of cap free
Amount of items: 2
Items: 
Size: 513419 Color: 1
Size: 486092 Color: 0

Bin 68: 499 of cap free
Amount of items: 2
Items: 
Size: 760714 Color: 1
Size: 238788 Color: 0

Bin 69: 501 of cap free
Amount of items: 2
Items: 
Size: 583377 Color: 0
Size: 416123 Color: 1

Bin 70: 518 of cap free
Amount of items: 2
Items: 
Size: 544373 Color: 0
Size: 455110 Color: 1

Bin 71: 533 of cap free
Amount of items: 2
Items: 
Size: 777315 Color: 0
Size: 222153 Color: 1

Bin 72: 547 of cap free
Amount of items: 2
Items: 
Size: 685727 Color: 0
Size: 313727 Color: 1

Bin 73: 557 of cap free
Amount of items: 2
Items: 
Size: 618028 Color: 1
Size: 381416 Color: 0

Bin 74: 607 of cap free
Amount of items: 2
Items: 
Size: 623824 Color: 1
Size: 375570 Color: 0

Bin 75: 639 of cap free
Amount of items: 2
Items: 
Size: 532170 Color: 0
Size: 467192 Color: 1

Bin 76: 691 of cap free
Amount of items: 2
Items: 
Size: 752203 Color: 0
Size: 247107 Color: 1

Bin 77: 704 of cap free
Amount of items: 3
Items: 
Size: 540544 Color: 0
Size: 313892 Color: 0
Size: 144861 Color: 1

Bin 78: 708 of cap free
Amount of items: 2
Items: 
Size: 694570 Color: 0
Size: 304723 Color: 1

Bin 79: 726 of cap free
Amount of items: 2
Items: 
Size: 525077 Color: 1
Size: 474198 Color: 0

Bin 80: 739 of cap free
Amount of items: 2
Items: 
Size: 595899 Color: 0
Size: 403363 Color: 1

Bin 81: 748 of cap free
Amount of items: 2
Items: 
Size: 567566 Color: 0
Size: 431687 Color: 1

Bin 82: 768 of cap free
Amount of items: 2
Items: 
Size: 532486 Color: 1
Size: 466747 Color: 0

Bin 83: 786 of cap free
Amount of items: 2
Items: 
Size: 553597 Color: 1
Size: 445618 Color: 0

Bin 84: 830 of cap free
Amount of items: 2
Items: 
Size: 641084 Color: 0
Size: 358087 Color: 1

Bin 85: 838 of cap free
Amount of items: 2
Items: 
Size: 509714 Color: 0
Size: 489449 Color: 1

Bin 86: 840 of cap free
Amount of items: 2
Items: 
Size: 516046 Color: 0
Size: 483115 Color: 1

Bin 87: 859 of cap free
Amount of items: 2
Items: 
Size: 634024 Color: 1
Size: 365118 Color: 0

Bin 88: 912 of cap free
Amount of items: 2
Items: 
Size: 745235 Color: 1
Size: 253854 Color: 0

Bin 89: 916 of cap free
Amount of items: 2
Items: 
Size: 742188 Color: 1
Size: 256897 Color: 0

Bin 90: 941 of cap free
Amount of items: 2
Items: 
Size: 724082 Color: 1
Size: 274978 Color: 0

Bin 91: 952 of cap free
Amount of items: 2
Items: 
Size: 607983 Color: 0
Size: 391066 Color: 1

Bin 92: 955 of cap free
Amount of items: 2
Items: 
Size: 574295 Color: 0
Size: 424751 Color: 1

Bin 93: 969 of cap free
Amount of items: 3
Items: 
Size: 527556 Color: 1
Size: 251751 Color: 0
Size: 219725 Color: 0

Bin 94: 983 of cap free
Amount of items: 2
Items: 
Size: 499775 Color: 1
Size: 499243 Color: 0

Bin 95: 1025 of cap free
Amount of items: 2
Items: 
Size: 666266 Color: 1
Size: 332710 Color: 0

Bin 96: 1065 of cap free
Amount of items: 2
Items: 
Size: 632112 Color: 0
Size: 366824 Color: 1

Bin 97: 1096 of cap free
Amount of items: 3
Items: 
Size: 656572 Color: 0
Size: 188176 Color: 1
Size: 154157 Color: 0

Bin 98: 1100 of cap free
Amount of items: 2
Items: 
Size: 750704 Color: 1
Size: 248197 Color: 0

Bin 99: 1153 of cap free
Amount of items: 2
Items: 
Size: 673391 Color: 0
Size: 325457 Color: 1

Bin 100: 1182 of cap free
Amount of items: 2
Items: 
Size: 771163 Color: 1
Size: 227656 Color: 0

Bin 101: 1286 of cap free
Amount of items: 3
Items: 
Size: 689665 Color: 0
Size: 159233 Color: 0
Size: 149817 Color: 1

Bin 102: 1318 of cap free
Amount of items: 2
Items: 
Size: 584329 Color: 0
Size: 414354 Color: 1

Bin 103: 1344 of cap free
Amount of items: 2
Items: 
Size: 561883 Color: 1
Size: 436774 Color: 0

Bin 104: 1354 of cap free
Amount of items: 2
Items: 
Size: 692588 Color: 1
Size: 306059 Color: 0

Bin 105: 1380 of cap free
Amount of items: 2
Items: 
Size: 545358 Color: 1
Size: 453263 Color: 0

Bin 106: 1436 of cap free
Amount of items: 2
Items: 
Size: 598247 Color: 1
Size: 400318 Color: 0

Bin 107: 1487 of cap free
Amount of items: 2
Items: 
Size: 623813 Color: 1
Size: 374701 Color: 0

Bin 108: 1578 of cap free
Amount of items: 2
Items: 
Size: 671024 Color: 0
Size: 327399 Color: 1

Bin 109: 1590 of cap free
Amount of items: 2
Items: 
Size: 741907 Color: 1
Size: 256504 Color: 0

Bin 110: 1633 of cap free
Amount of items: 2
Items: 
Size: 788075 Color: 0
Size: 210293 Color: 1

Bin 111: 1709 of cap free
Amount of items: 2
Items: 
Size: 737056 Color: 0
Size: 261236 Color: 1

Bin 112: 1728 of cap free
Amount of items: 2
Items: 
Size: 783150 Color: 0
Size: 215123 Color: 1

Bin 113: 1731 of cap free
Amount of items: 2
Items: 
Size: 772955 Color: 0
Size: 225315 Color: 1

Bin 114: 1766 of cap free
Amount of items: 2
Items: 
Size: 644440 Color: 0
Size: 353795 Color: 1

Bin 115: 1817 of cap free
Amount of items: 2
Items: 
Size: 508950 Color: 0
Size: 489234 Color: 1

Bin 116: 1822 of cap free
Amount of items: 2
Items: 
Size: 622413 Color: 0
Size: 375766 Color: 1

Bin 117: 1854 of cap free
Amount of items: 2
Items: 
Size: 683824 Color: 0
Size: 314323 Color: 1

Bin 118: 1868 of cap free
Amount of items: 2
Items: 
Size: 740954 Color: 0
Size: 257179 Color: 1

Bin 119: 1923 of cap free
Amount of items: 2
Items: 
Size: 768367 Color: 1
Size: 229711 Color: 0

Bin 120: 1928 of cap free
Amount of items: 2
Items: 
Size: 775928 Color: 1
Size: 222145 Color: 0

Bin 121: 1956 of cap free
Amount of items: 2
Items: 
Size: 581479 Color: 1
Size: 416566 Color: 0

Bin 122: 2048 of cap free
Amount of items: 2
Items: 
Size: 677870 Color: 0
Size: 320083 Color: 1

Bin 123: 2095 of cap free
Amount of items: 2
Items: 
Size: 789772 Color: 1
Size: 208134 Color: 0

Bin 124: 2148 of cap free
Amount of items: 2
Items: 
Size: 673299 Color: 1
Size: 324554 Color: 0

Bin 125: 2197 of cap free
Amount of items: 2
Items: 
Size: 729935 Color: 1
Size: 267869 Color: 0

Bin 126: 2219 of cap free
Amount of items: 2
Items: 
Size: 552328 Color: 0
Size: 445454 Color: 1

Bin 127: 2254 of cap free
Amount of items: 2
Items: 
Size: 698486 Color: 1
Size: 299261 Color: 0

Bin 128: 2254 of cap free
Amount of items: 2
Items: 
Size: 606857 Color: 0
Size: 390890 Color: 1

Bin 129: 2263 of cap free
Amount of items: 2
Items: 
Size: 619202 Color: 0
Size: 378536 Color: 1

Bin 130: 2287 of cap free
Amount of items: 2
Items: 
Size: 693722 Color: 0
Size: 303992 Color: 1

Bin 131: 2352 of cap free
Amount of items: 2
Items: 
Size: 529716 Color: 0
Size: 467933 Color: 1

Bin 132: 2384 of cap free
Amount of items: 2
Items: 
Size: 664298 Color: 0
Size: 333319 Color: 1

Bin 133: 2400 of cap free
Amount of items: 2
Items: 
Size: 677511 Color: 1
Size: 320090 Color: 0

Bin 134: 2627 of cap free
Amount of items: 2
Items: 
Size: 799736 Color: 0
Size: 197638 Color: 1

Bin 135: 2644 of cap free
Amount of items: 2
Items: 
Size: 590555 Color: 1
Size: 406802 Color: 0

Bin 136: 2657 of cap free
Amount of items: 2
Items: 
Size: 753952 Color: 1
Size: 243392 Color: 0

Bin 137: 2676 of cap free
Amount of items: 2
Items: 
Size: 594789 Color: 1
Size: 402536 Color: 0

Bin 138: 2703 of cap free
Amount of items: 2
Items: 
Size: 714656 Color: 1
Size: 282642 Color: 0

Bin 139: 2716 of cap free
Amount of items: 2
Items: 
Size: 724329 Color: 0
Size: 272956 Color: 1

Bin 140: 2753 of cap free
Amount of items: 2
Items: 
Size: 633366 Color: 1
Size: 363882 Color: 0

Bin 141: 2774 of cap free
Amount of items: 2
Items: 
Size: 534023 Color: 1
Size: 463204 Color: 0

Bin 142: 2934 of cap free
Amount of items: 2
Items: 
Size: 600449 Color: 1
Size: 396618 Color: 0

Bin 143: 2980 of cap free
Amount of items: 2
Items: 
Size: 641421 Color: 1
Size: 355600 Color: 0

Bin 144: 3085 of cap free
Amount of items: 2
Items: 
Size: 606453 Color: 1
Size: 390463 Color: 0

Bin 145: 3183 of cap free
Amount of items: 2
Items: 
Size: 569537 Color: 1
Size: 427281 Color: 0

Bin 146: 3223 of cap free
Amount of items: 2
Items: 
Size: 516197 Color: 1
Size: 480581 Color: 0

Bin 147: 3365 of cap free
Amount of items: 2
Items: 
Size: 513782 Color: 0
Size: 482854 Color: 1

Bin 148: 3445 of cap free
Amount of items: 2
Items: 
Size: 682814 Color: 1
Size: 313742 Color: 0

Bin 149: 3492 of cap free
Amount of items: 2
Items: 
Size: 592195 Color: 0
Size: 404314 Color: 1

Bin 150: 3494 of cap free
Amount of items: 2
Items: 
Size: 613790 Color: 0
Size: 382717 Color: 1

Bin 151: 3558 of cap free
Amount of items: 2
Items: 
Size: 775685 Color: 1
Size: 220758 Color: 0

Bin 152: 3648 of cap free
Amount of items: 2
Items: 
Size: 669176 Color: 1
Size: 327177 Color: 0

Bin 153: 3676 of cap free
Amount of items: 2
Items: 
Size: 563489 Color: 1
Size: 432836 Color: 0

Bin 154: 3704 of cap free
Amount of items: 2
Items: 
Size: 663013 Color: 0
Size: 333284 Color: 1

Bin 155: 3781 of cap free
Amount of items: 2
Items: 
Size: 798727 Color: 0
Size: 197493 Color: 1

Bin 156: 3799 of cap free
Amount of items: 2
Items: 
Size: 569368 Color: 1
Size: 426834 Color: 0

Bin 157: 3962 of cap free
Amount of items: 2
Items: 
Size: 689117 Color: 0
Size: 306922 Color: 1

Bin 158: 4207 of cap free
Amount of items: 2
Items: 
Size: 782463 Color: 0
Size: 213331 Color: 1

Bin 159: 4227 of cap free
Amount of items: 2
Items: 
Size: 507806 Color: 0
Size: 487968 Color: 1

Bin 160: 4247 of cap free
Amount of items: 2
Items: 
Size: 513450 Color: 0
Size: 482304 Color: 1

Bin 161: 4300 of cap free
Amount of items: 2
Items: 
Size: 563323 Color: 1
Size: 432378 Color: 0

Bin 162: 4557 of cap free
Amount of items: 2
Items: 
Size: 682396 Color: 0
Size: 313048 Color: 1

Bin 163: 4891 of cap free
Amount of items: 2
Items: 
Size: 591810 Color: 0
Size: 403300 Color: 1

Bin 164: 4939 of cap free
Amount of items: 2
Items: 
Size: 748450 Color: 0
Size: 246612 Color: 1

Bin 165: 5058 of cap free
Amount of items: 2
Items: 
Size: 513300 Color: 0
Size: 481643 Color: 1

Bin 166: 5341 of cap free
Amount of items: 2
Items: 
Size: 717847 Color: 1
Size: 276813 Color: 0

Bin 167: 5380 of cap free
Amount of items: 2
Items: 
Size: 781540 Color: 0
Size: 213081 Color: 1

Bin 168: 5652 of cap free
Amount of items: 2
Items: 
Size: 549729 Color: 0
Size: 444620 Color: 1

Bin 169: 5701 of cap free
Amount of items: 2
Items: 
Size: 523869 Color: 0
Size: 470431 Color: 1

Bin 170: 5758 of cap free
Amount of items: 2
Items: 
Size: 760860 Color: 0
Size: 233383 Color: 1

Bin 171: 5866 of cap free
Amount of items: 2
Items: 
Size: 606468 Color: 0
Size: 387667 Color: 1

Bin 172: 6034 of cap free
Amount of items: 2
Items: 
Size: 740665 Color: 0
Size: 253302 Color: 1

Bin 173: 6091 of cap free
Amount of items: 2
Items: 
Size: 798285 Color: 1
Size: 195625 Color: 0

Bin 174: 6532 of cap free
Amount of items: 2
Items: 
Size: 582001 Color: 0
Size: 411468 Color: 1

Bin 175: 6848 of cap free
Amount of items: 2
Items: 
Size: 681019 Color: 0
Size: 312134 Color: 1

Bin 176: 7032 of cap free
Amount of items: 2
Items: 
Size: 642229 Color: 0
Size: 350740 Color: 1

Bin 177: 7096 of cap free
Amount of items: 2
Items: 
Size: 605588 Color: 0
Size: 387317 Color: 1

Bin 178: 7167 of cap free
Amount of items: 2
Items: 
Size: 624872 Color: 0
Size: 367962 Color: 1

Bin 179: 7756 of cap free
Amount of items: 2
Items: 
Size: 560061 Color: 1
Size: 432184 Color: 0

Bin 180: 7779 of cap free
Amount of items: 2
Items: 
Size: 661829 Color: 0
Size: 330393 Color: 1

Bin 181: 7781 of cap free
Amount of items: 2
Items: 
Size: 574084 Color: 0
Size: 418136 Color: 1

Bin 182: 7805 of cap free
Amount of items: 2
Items: 
Size: 668280 Color: 1
Size: 323916 Color: 0

Bin 183: 7948 of cap free
Amount of items: 2
Items: 
Size: 784166 Color: 1
Size: 207887 Color: 0

Bin 184: 8079 of cap free
Amount of items: 2
Items: 
Size: 629798 Color: 1
Size: 362124 Color: 0

Bin 185: 8133 of cap free
Amount of items: 2
Items: 
Size: 688467 Color: 0
Size: 303401 Color: 1

Bin 186: 8863 of cap free
Amount of items: 2
Items: 
Size: 712262 Color: 0
Size: 278876 Color: 1

Bin 187: 8871 of cap free
Amount of items: 2
Items: 
Size: 695071 Color: 1
Size: 296059 Color: 0

Bin 188: 9353 of cap free
Amount of items: 3
Items: 
Size: 704139 Color: 1
Size: 151949 Color: 0
Size: 134560 Color: 0

Bin 189: 9595 of cap free
Amount of items: 2
Items: 
Size: 542555 Color: 1
Size: 447851 Color: 0

Bin 190: 9847 of cap free
Amount of items: 2
Items: 
Size: 759211 Color: 0
Size: 230943 Color: 1

Bin 191: 10681 of cap free
Amount of items: 2
Items: 
Size: 639098 Color: 0
Size: 350222 Color: 1

Bin 192: 10771 of cap free
Amount of items: 2
Items: 
Size: 523469 Color: 0
Size: 465761 Color: 1

Bin 193: 10784 of cap free
Amount of items: 2
Items: 
Size: 496813 Color: 0
Size: 492404 Color: 1

Bin 194: 10795 of cap free
Amount of items: 2
Items: 
Size: 749681 Color: 1
Size: 239525 Color: 0

Bin 195: 11902 of cap free
Amount of items: 2
Items: 
Size: 628485 Color: 1
Size: 359614 Color: 0

Bin 196: 11989 of cap free
Amount of items: 2
Items: 
Size: 732450 Color: 1
Size: 255562 Color: 0

Bin 197: 12056 of cap free
Amount of items: 2
Items: 
Size: 667883 Color: 1
Size: 320062 Color: 0

Bin 198: 12398 of cap free
Amount of items: 2
Items: 
Size: 796123 Color: 1
Size: 191480 Color: 0

Bin 199: 12503 of cap free
Amount of items: 2
Items: 
Size: 713702 Color: 1
Size: 273796 Color: 0

Bin 200: 13327 of cap free
Amount of items: 2
Items: 
Size: 624253 Color: 0
Size: 362421 Color: 1

Bin 201: 14637 of cap free
Amount of items: 2
Items: 
Size: 713527 Color: 1
Size: 271837 Color: 0

Bin 202: 15136 of cap free
Amount of items: 2
Items: 
Size: 492596 Color: 0
Size: 492269 Color: 1

Bin 203: 15695 of cap free
Amount of items: 2
Items: 
Size: 711573 Color: 0
Size: 272733 Color: 1

Bin 204: 16133 of cap free
Amount of items: 2
Items: 
Size: 549651 Color: 0
Size: 434217 Color: 1

Bin 205: 16217 of cap free
Amount of items: 2
Items: 
Size: 518552 Color: 0
Size: 465232 Color: 1

Bin 206: 17212 of cap free
Amount of items: 2
Items: 
Size: 795768 Color: 1
Size: 187021 Color: 0

Bin 207: 18819 of cap free
Amount of items: 2
Items: 
Size: 593472 Color: 1
Size: 387710 Color: 0

Bin 208: 19363 of cap free
Amount of items: 2
Items: 
Size: 794102 Color: 1
Size: 186536 Color: 0

Bin 209: 19888 of cap free
Amount of items: 2
Items: 
Size: 598396 Color: 0
Size: 381717 Color: 1

Bin 210: 22124 of cap free
Amount of items: 2
Items: 
Size: 513173 Color: 0
Size: 464704 Color: 1

Bin 211: 23213 of cap free
Amount of items: 2
Items: 
Size: 492268 Color: 1
Size: 484520 Color: 0

Bin 212: 25290 of cap free
Amount of items: 2
Items: 
Size: 655334 Color: 0
Size: 319377 Color: 1

Bin 213: 25420 of cap free
Amount of items: 2
Items: 
Size: 512950 Color: 0
Size: 461631 Color: 1

Bin 214: 28776 of cap free
Amount of items: 2
Items: 
Size: 700344 Color: 0
Size: 270881 Color: 1

Bin 215: 30844 of cap free
Amount of items: 2
Items: 
Size: 623176 Color: 1
Size: 345981 Color: 0

Bin 216: 35261 of cap free
Amount of items: 2
Items: 
Size: 621354 Color: 1
Size: 343386 Color: 0

Bin 217: 36389 of cap free
Amount of items: 2
Items: 
Size: 539663 Color: 1
Size: 423949 Color: 0

Bin 218: 45333 of cap free
Amount of items: 2
Items: 
Size: 619135 Color: 1
Size: 335533 Color: 0

Bin 219: 86823 of cap free
Amount of items: 2
Items: 
Size: 580547 Color: 1
Size: 332631 Color: 0

Bin 220: 202144 of cap free
Amount of items: 1
Items: 
Size: 797857 Color: 0

Bin 221: 206606 of cap free
Amount of items: 1
Items: 
Size: 793395 Color: 0

Bin 222: 206618 of cap free
Amount of items: 1
Items: 
Size: 793383 Color: 1

Bin 223: 209105 of cap free
Amount of items: 1
Items: 
Size: 790896 Color: 0

Bin 224: 212303 of cap free
Amount of items: 1
Items: 
Size: 787698 Color: 0

Bin 225: 217345 of cap free
Amount of items: 1
Items: 
Size: 782656 Color: 1

Bin 226: 218859 of cap free
Amount of items: 1
Items: 
Size: 781142 Color: 1

Bin 227: 221885 of cap free
Amount of items: 1
Items: 
Size: 778116 Color: 0

Bin 228: 223790 of cap free
Amount of items: 1
Items: 
Size: 776211 Color: 0

Bin 229: 232998 of cap free
Amount of items: 1
Items: 
Size: 767003 Color: 1

Bin 230: 233315 of cap free
Amount of items: 1
Items: 
Size: 766686 Color: 1

Bin 231: 234450 of cap free
Amount of items: 1
Items: 
Size: 765551 Color: 1

Bin 232: 242541 of cap free
Amount of items: 1
Items: 
Size: 757460 Color: 0

Bin 233: 250723 of cap free
Amount of items: 1
Items: 
Size: 749278 Color: 1

Bin 234: 251042 of cap free
Amount of items: 1
Items: 
Size: 748959 Color: 1

Bin 235: 251278 of cap free
Amount of items: 1
Items: 
Size: 748723 Color: 1

Bin 236: 263774 of cap free
Amount of items: 1
Items: 
Size: 736227 Color: 0

Total size: 231095353
Total free space: 4904883

