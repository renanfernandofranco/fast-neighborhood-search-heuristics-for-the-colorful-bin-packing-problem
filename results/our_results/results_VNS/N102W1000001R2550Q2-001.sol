Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 406003 Color: 1
Size: 268555 Color: 0
Size: 325443 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 463327 Color: 1
Size: 277726 Color: 0
Size: 258948 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 437108 Color: 0
Size: 307352 Color: 1
Size: 255541 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 360672 Color: 0
Size: 346177 Color: 0
Size: 293152 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476433 Color: 0
Size: 254520 Color: 1
Size: 269048 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 286623 Color: 0
Size: 326595 Color: 1
Size: 386783 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 435684 Color: 1
Size: 306502 Color: 0
Size: 257815 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 386335 Color: 1
Size: 356273 Color: 1
Size: 257393 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 452881 Color: 0
Size: 270389 Color: 0
Size: 276731 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 446720 Color: 1
Size: 274473 Color: 0
Size: 278808 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 391520 Color: 1
Size: 346393 Color: 1
Size: 262088 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 348403 Color: 1
Size: 344445 Color: 0
Size: 307153 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 377804 Color: 0
Size: 327160 Color: 1
Size: 295037 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 416128 Color: 1
Size: 302966 Color: 0
Size: 280907 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 389187 Color: 0
Size: 266210 Color: 1
Size: 344604 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 480541 Color: 0
Size: 262549 Color: 1
Size: 256911 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 386954 Color: 1
Size: 348784 Color: 0
Size: 264263 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 276998 Color: 1
Size: 411474 Color: 0
Size: 311529 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 477237 Color: 1
Size: 271020 Color: 0
Size: 251744 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 368008 Color: 1
Size: 342407 Color: 0
Size: 289586 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 362477 Color: 0
Size: 299517 Color: 0
Size: 338007 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 393316 Color: 1
Size: 336091 Color: 1
Size: 270594 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 255050 Color: 0
Size: 478784 Color: 1
Size: 266167 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 362940 Color: 1
Size: 287635 Color: 0
Size: 349426 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 394461 Color: 1
Size: 337791 Color: 0
Size: 267749 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 394315 Color: 0
Size: 287381 Color: 1
Size: 318305 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 455759 Color: 0
Size: 268880 Color: 1
Size: 275362 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 484905 Color: 1
Size: 252004 Color: 0
Size: 263092 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 391330 Color: 0
Size: 346261 Color: 0
Size: 262410 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 412145 Color: 0
Size: 298986 Color: 1
Size: 288870 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 429894 Color: 1
Size: 309602 Color: 0
Size: 260505 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 446865 Color: 1
Size: 277334 Color: 1
Size: 275802 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 462048 Color: 1
Size: 280984 Color: 0
Size: 256969 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 474256 Color: 0
Size: 275216 Color: 1
Size: 250529 Color: 0

Total size: 34000034
Total free space: 0

