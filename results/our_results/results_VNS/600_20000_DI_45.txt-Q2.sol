Capicity Bin: 16224
Lower Bound: 198

Bins used: 200
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 10065 Color: 0
Size: 4307 Color: 0
Size: 1044 Color: 1
Size: 808 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 1
Size: 1672 Color: 1
Size: 1344 Color: 0

Bin 3: 0 of cap free
Amount of items: 9
Items: 
Size: 8116 Color: 1
Size: 1350 Color: 0
Size: 1344 Color: 0
Size: 1344 Color: 0
Size: 948 Color: 1
Size: 944 Color: 1
Size: 910 Color: 1
Size: 860 Color: 1
Size: 408 Color: 0

Bin 4: 0 of cap free
Amount of items: 8
Items: 
Size: 8120 Color: 1
Size: 2562 Color: 0
Size: 1448 Color: 1
Size: 1374 Color: 0
Size: 1136 Color: 1
Size: 1104 Color: 1
Size: 288 Color: 0
Size: 192 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7056 Color: 1
Size: 5768 Color: 0
Size: 3400 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 10546 Color: 0
Size: 4734 Color: 0
Size: 744 Color: 1
Size: 200 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 1
Size: 2416 Color: 0
Size: 264 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 14372 Color: 0
Size: 1180 Color: 0
Size: 480 Color: 1
Size: 192 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14136 Color: 1
Size: 1842 Color: 1
Size: 246 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 1
Size: 1384 Color: 1
Size: 1184 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 1
Size: 5764 Color: 1
Size: 1700 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 12746 Color: 0
Size: 2454 Color: 1
Size: 848 Color: 0
Size: 176 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12758 Color: 1
Size: 1862 Color: 1
Size: 1604 Color: 0

Bin 14: 0 of cap free
Amount of items: 11
Items: 
Size: 8113 Color: 1
Size: 1595 Color: 1
Size: 928 Color: 0
Size: 912 Color: 0
Size: 880 Color: 0
Size: 864 Color: 1
Size: 844 Color: 0
Size: 816 Color: 1
Size: 784 Color: 1
Size: 296 Color: 0
Size: 192 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 0
Size: 3084 Color: 1
Size: 264 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 0
Size: 1364 Color: 1
Size: 1096 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 0
Size: 1348 Color: 0
Size: 280 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 1
Size: 3832 Color: 0
Size: 318 Color: 1

Bin 19: 0 of cap free
Amount of items: 4
Items: 
Size: 10837 Color: 0
Size: 4899 Color: 0
Size: 392 Color: 1
Size: 96 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 1
Size: 1924 Color: 1
Size: 186 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8732 Color: 0
Size: 6748 Color: 1
Size: 744 Color: 1

Bin 22: 0 of cap free
Amount of items: 28
Items: 
Size: 794 Color: 1
Size: 706 Color: 0
Size: 704 Color: 0
Size: 696 Color: 1
Size: 688 Color: 1
Size: 640 Color: 1
Size: 632 Color: 1
Size: 624 Color: 1
Size: 608 Color: 0
Size: 600 Color: 0
Size: 584 Color: 1
Size: 584 Color: 0
Size: 576 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 576 Color: 0
Size: 552 Color: 1
Size: 544 Color: 0
Size: 540 Color: 1
Size: 540 Color: 1
Size: 536 Color: 1
Size: 532 Color: 0
Size: 524 Color: 0
Size: 516 Color: 0
Size: 496 Color: 0
Size: 488 Color: 0
Size: 464 Color: 0
Size: 328 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9908 Color: 1
Size: 5906 Color: 0
Size: 410 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 1
Size: 2078 Color: 0
Size: 1072 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13994 Color: 1
Size: 1894 Color: 0
Size: 336 Color: 0

Bin 26: 0 of cap free
Amount of items: 4
Items: 
Size: 14284 Color: 0
Size: 792 Color: 0
Size: 760 Color: 1
Size: 388 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 0
Size: 2278 Color: 1
Size: 1922 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 9396 Color: 0
Size: 5350 Color: 0
Size: 1478 Color: 1

Bin 29: 0 of cap free
Amount of items: 41
Items: 
Size: 512 Color: 1
Size: 508 Color: 1
Size: 480 Color: 1
Size: 464 Color: 0
Size: 462 Color: 1
Size: 456 Color: 0
Size: 448 Color: 0
Size: 440 Color: 0
Size: 440 Color: 0
Size: 434 Color: 0
Size: 432 Color: 1
Size: 432 Color: 0
Size: 420 Color: 0
Size: 416 Color: 0
Size: 412 Color: 1
Size: 412 Color: 0
Size: 408 Color: 1
Size: 408 Color: 1
Size: 404 Color: 1
Size: 400 Color: 0
Size: 392 Color: 1
Size: 384 Color: 1
Size: 384 Color: 1
Size: 384 Color: 0
Size: 378 Color: 0
Size: 376 Color: 0
Size: 372 Color: 1
Size: 370 Color: 1
Size: 368 Color: 1
Size: 364 Color: 0
Size: 364 Color: 0
Size: 356 Color: 0
Size: 352 Color: 0
Size: 348 Color: 1
Size: 348 Color: 0
Size: 336 Color: 1
Size: 336 Color: 1
Size: 312 Color: 1
Size: 304 Color: 1
Size: 304 Color: 1
Size: 304 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14029 Color: 1
Size: 2075 Color: 0
Size: 120 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12619 Color: 1
Size: 1893 Color: 1
Size: 1712 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12142 Color: 1
Size: 3806 Color: 1
Size: 276 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11162 Color: 0
Size: 2706 Color: 0
Size: 2356 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 0
Size: 2262 Color: 0
Size: 44 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12036 Color: 0
Size: 2118 Color: 1
Size: 2070 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 0
Size: 1620 Color: 0
Size: 312 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 10868 Color: 0
Size: 3971 Color: 1
Size: 1385 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 1
Size: 4056 Color: 0
Size: 800 Color: 1

Bin 39: 0 of cap free
Amount of items: 9
Items: 
Size: 8114 Color: 1
Size: 1344 Color: 0
Size: 1248 Color: 0
Size: 992 Color: 0
Size: 944 Color: 0
Size: 900 Color: 1
Size: 898 Color: 1
Size: 896 Color: 1
Size: 888 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 0
Size: 4136 Color: 1
Size: 624 Color: 0

Bin 41: 0 of cap free
Amount of items: 4
Items: 
Size: 13832 Color: 0
Size: 1848 Color: 0
Size: 272 Color: 1
Size: 272 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13404 Color: 1
Size: 2684 Color: 0
Size: 136 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 0
Size: 4680 Color: 1
Size: 2224 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8138 Color: 1
Size: 6742 Color: 0
Size: 1344 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 10120 Color: 0
Size: 5930 Color: 1
Size: 174 Color: 0

Bin 46: 0 of cap free
Amount of items: 14
Items: 
Size: 12344 Color: 0
Size: 320 Color: 0
Size: 320 Color: 0
Size: 304 Color: 1
Size: 304 Color: 1
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 300 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1
Size: 284 Color: 1
Size: 280 Color: 1
Size: 280 Color: 1

Bin 47: 0 of cap free
Amount of items: 4
Items: 
Size: 14536 Color: 0
Size: 1144 Color: 0
Size: 272 Color: 1
Size: 272 Color: 1

Bin 48: 0 of cap free
Amount of items: 8
Items: 
Size: 8122 Color: 1
Size: 1522 Color: 1
Size: 1515 Color: 0
Size: 1501 Color: 1
Size: 1486 Color: 0
Size: 1484 Color: 0
Size: 302 Color: 1
Size: 292 Color: 0

Bin 49: 0 of cap free
Amount of items: 4
Items: 
Size: 8132 Color: 1
Size: 6684 Color: 0
Size: 1176 Color: 1
Size: 232 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 1
Size: 6744 Color: 0
Size: 1024 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 9154 Color: 0
Size: 6746 Color: 1
Size: 324 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 0
Size: 6328 Color: 1
Size: 368 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 0
Size: 6132 Color: 1
Size: 340 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 1
Size: 5528 Color: 0
Size: 208 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 11146 Color: 1
Size: 4222 Color: 1
Size: 856 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 1
Size: 2776 Color: 1
Size: 336 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 13132 Color: 1
Size: 2580 Color: 1
Size: 512 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 0
Size: 2818 Color: 1
Size: 252 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 13170 Color: 1
Size: 2602 Color: 1
Size: 452 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 13395 Color: 0
Size: 1458 Color: 1
Size: 1371 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 0
Size: 1442 Color: 1
Size: 1248 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 13623 Color: 1
Size: 2169 Color: 1
Size: 432 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 1
Size: 2132 Color: 1
Size: 424 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 1
Size: 1988 Color: 0
Size: 488 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13906 Color: 0
Size: 1280 Color: 1
Size: 1038 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 1
Size: 1232 Color: 0
Size: 984 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 0
Size: 1548 Color: 0
Size: 672 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 1
Size: 1422 Color: 0
Size: 680 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 14215 Color: 1
Size: 1559 Color: 0
Size: 450 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 0
Size: 1348 Color: 1
Size: 688 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 14232 Color: 1
Size: 1528 Color: 0
Size: 464 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 14306 Color: 1
Size: 1348 Color: 0
Size: 570 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 14398 Color: 1
Size: 1026 Color: 1
Size: 800 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 14461 Color: 1
Size: 1471 Color: 1
Size: 292 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 1
Size: 1428 Color: 0
Size: 320 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 1
Size: 1416 Color: 0
Size: 286 Color: 1

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 9636 Color: 1
Size: 5127 Color: 0
Size: 1460 Color: 1

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 10530 Color: 1
Size: 5197 Color: 0
Size: 496 Color: 1

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 0
Size: 3533 Color: 1
Size: 256 Color: 0

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 1
Size: 2483 Color: 1
Size: 184 Color: 0

Bin 81: 1 of cap free
Amount of items: 2
Items: 
Size: 13844 Color: 0
Size: 2379 Color: 1

Bin 82: 1 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 1
Size: 2061 Color: 0

Bin 83: 1 of cap free
Amount of items: 2
Items: 
Size: 14282 Color: 1
Size: 1941 Color: 0

Bin 84: 1 of cap free
Amount of items: 2
Items: 
Size: 14392 Color: 0
Size: 1831 Color: 1

Bin 85: 1 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 1
Size: 3965 Color: 1
Size: 3120 Color: 0

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 0
Size: 2058 Color: 1
Size: 1421 Color: 0

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 9990 Color: 0
Size: 6232 Color: 1

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 0
Size: 4242 Color: 1
Size: 648 Color: 1

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 12801 Color: 0
Size: 2581 Color: 1
Size: 840 Color: 0

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 12929 Color: 1
Size: 3005 Color: 1
Size: 288 Color: 0

Bin 91: 2 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 1
Size: 2796 Color: 0

Bin 92: 2 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 0
Size: 1654 Color: 1

Bin 93: 2 of cap free
Amount of items: 5
Items: 
Size: 8130 Color: 1
Size: 3462 Color: 0
Size: 2066 Color: 1
Size: 1540 Color: 1
Size: 1024 Color: 0

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 14084 Color: 1
Size: 1622 Color: 1
Size: 516 Color: 0

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 9119 Color: 1
Size: 6754 Color: 1
Size: 348 Color: 0

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 0
Size: 4497 Color: 0
Size: 452 Color: 1

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 13510 Color: 0
Size: 2711 Color: 1

Bin 98: 3 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 1
Size: 1866 Color: 0

Bin 99: 3 of cap free
Amount of items: 2
Items: 
Size: 14499 Color: 0
Size: 1722 Color: 1

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 8648 Color: 1
Size: 7252 Color: 1
Size: 320 Color: 0

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 9989 Color: 1
Size: 5921 Color: 1
Size: 310 Color: 0

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 0
Size: 2904 Color: 1

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 0
Size: 4561 Color: 1

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 11985 Color: 0
Size: 4234 Color: 1

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 9974 Color: 0
Size: 6244 Color: 1

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 12978 Color: 1
Size: 3240 Color: 0

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 0
Size: 2232 Color: 1

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 1
Size: 2152 Color: 0

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 14542 Color: 1
Size: 1675 Color: 0

Bin 110: 9 of cap free
Amount of items: 2
Items: 
Size: 13106 Color: 0
Size: 3109 Color: 1

Bin 111: 10 of cap free
Amount of items: 3
Items: 
Size: 8157 Color: 1
Size: 7723 Color: 1
Size: 334 Color: 0

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 1
Size: 2456 Color: 0

Bin 113: 11 of cap free
Amount of items: 2
Items: 
Size: 14519 Color: 1
Size: 1694 Color: 0

Bin 114: 11 of cap free
Amount of items: 2
Items: 
Size: 11467 Color: 1
Size: 4746 Color: 0

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 10720 Color: 1
Size: 5492 Color: 0

Bin 116: 13 of cap free
Amount of items: 2
Items: 
Size: 14311 Color: 0
Size: 1900 Color: 1

Bin 117: 13 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 1
Size: 1713 Color: 0

Bin 118: 15 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 0
Size: 2212 Color: 1

Bin 119: 15 of cap free
Amount of items: 2
Items: 
Size: 14407 Color: 0
Size: 1802 Color: 1

Bin 120: 16 of cap free
Amount of items: 2
Items: 
Size: 10616 Color: 0
Size: 5592 Color: 1

Bin 121: 16 of cap free
Amount of items: 2
Items: 
Size: 12716 Color: 1
Size: 3492 Color: 0

Bin 122: 18 of cap free
Amount of items: 2
Items: 
Size: 13282 Color: 1
Size: 2924 Color: 0

Bin 123: 18 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 1
Size: 2520 Color: 0

Bin 124: 18 of cap free
Amount of items: 2
Items: 
Size: 14454 Color: 1
Size: 1752 Color: 0

Bin 125: 20 of cap free
Amount of items: 2
Items: 
Size: 12466 Color: 1
Size: 3738 Color: 0

Bin 126: 20 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 1
Size: 3122 Color: 0

Bin 127: 20 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 0
Size: 2334 Color: 1

Bin 128: 21 of cap free
Amount of items: 2
Items: 
Size: 9959 Color: 1
Size: 6244 Color: 0

Bin 129: 21 of cap free
Amount of items: 2
Items: 
Size: 14452 Color: 1
Size: 1751 Color: 0

Bin 130: 22 of cap free
Amount of items: 2
Items: 
Size: 10981 Color: 1
Size: 5221 Color: 0

Bin 131: 22 of cap free
Amount of items: 2
Items: 
Size: 14194 Color: 1
Size: 2008 Color: 0

Bin 132: 25 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 0
Size: 5891 Color: 1

Bin 133: 26 of cap free
Amount of items: 2
Items: 
Size: 14578 Color: 1
Size: 1620 Color: 0

Bin 134: 27 of cap free
Amount of items: 2
Items: 
Size: 14125 Color: 0
Size: 2072 Color: 1

Bin 135: 29 of cap free
Amount of items: 2
Items: 
Size: 14127 Color: 1
Size: 2068 Color: 0

Bin 136: 29 of cap free
Amount of items: 2
Items: 
Size: 13953 Color: 0
Size: 2242 Color: 1

Bin 137: 30 of cap free
Amount of items: 3
Items: 
Size: 13579 Color: 1
Size: 1439 Color: 0
Size: 1176 Color: 1

Bin 138: 31 of cap free
Amount of items: 2
Items: 
Size: 12429 Color: 1
Size: 3764 Color: 0

Bin 139: 33 of cap free
Amount of items: 2
Items: 
Size: 14442 Color: 1
Size: 1749 Color: 0

Bin 140: 34 of cap free
Amount of items: 2
Items: 
Size: 11057 Color: 1
Size: 5133 Color: 0

Bin 141: 35 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 1
Size: 3163 Color: 0

Bin 142: 35 of cap free
Amount of items: 2
Items: 
Size: 14581 Color: 0
Size: 1608 Color: 1

Bin 143: 40 of cap free
Amount of items: 2
Items: 
Size: 11716 Color: 0
Size: 4468 Color: 1

Bin 144: 43 of cap free
Amount of items: 2
Items: 
Size: 13753 Color: 0
Size: 2428 Color: 1

Bin 145: 44 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 1
Size: 1692 Color: 0

Bin 146: 45 of cap free
Amount of items: 2
Items: 
Size: 14391 Color: 0
Size: 1788 Color: 1

Bin 147: 46 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 0
Size: 2890 Color: 1

Bin 148: 47 of cap free
Amount of items: 2
Items: 
Size: 14320 Color: 1
Size: 1857 Color: 0

Bin 149: 47 of cap free
Amount of items: 2
Items: 
Size: 13949 Color: 0
Size: 2228 Color: 1

Bin 150: 52 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 1
Size: 2248 Color: 0

Bin 151: 52 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 1
Size: 6856 Color: 0

Bin 152: 52 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 1
Size: 3268 Color: 0

Bin 153: 52 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 1
Size: 2600 Color: 0

Bin 154: 62 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 0
Size: 4792 Color: 0
Size: 4608 Color: 1

Bin 155: 63 of cap free
Amount of items: 2
Items: 
Size: 14516 Color: 0
Size: 1645 Color: 1

Bin 156: 66 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 1
Size: 1846 Color: 0

Bin 157: 66 of cap free
Amount of items: 2
Items: 
Size: 14196 Color: 0
Size: 1962 Color: 1

Bin 158: 71 of cap free
Amount of items: 2
Items: 
Size: 10753 Color: 0
Size: 5400 Color: 1

Bin 159: 72 of cap free
Amount of items: 2
Items: 
Size: 8136 Color: 0
Size: 8016 Color: 1

Bin 160: 75 of cap free
Amount of items: 2
Items: 
Size: 13571 Color: 1
Size: 2578 Color: 0

Bin 161: 75 of cap free
Amount of items: 2
Items: 
Size: 13742 Color: 0
Size: 2407 Color: 1

Bin 162: 77 of cap free
Amount of items: 2
Items: 
Size: 13245 Color: 1
Size: 2902 Color: 0

Bin 163: 82 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 0
Size: 1762 Color: 1

Bin 164: 86 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 0
Size: 3134 Color: 1

Bin 165: 103 of cap free
Amount of items: 2
Items: 
Size: 10073 Color: 1
Size: 6048 Color: 0

Bin 166: 104 of cap free
Amount of items: 2
Items: 
Size: 13454 Color: 0
Size: 2666 Color: 1

Bin 167: 114 of cap free
Amount of items: 3
Items: 
Size: 11742 Color: 1
Size: 4084 Color: 1
Size: 284 Color: 0

Bin 168: 118 of cap free
Amount of items: 2
Items: 
Size: 13895 Color: 0
Size: 2211 Color: 1

Bin 169: 120 of cap free
Amount of items: 2
Items: 
Size: 13789 Color: 1
Size: 2315 Color: 0

Bin 170: 122 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 1
Size: 2052 Color: 1
Size: 1402 Color: 0

Bin 171: 134 of cap free
Amount of items: 2
Items: 
Size: 12532 Color: 0
Size: 3558 Color: 1

Bin 172: 158 of cap free
Amount of items: 2
Items: 
Size: 11134 Color: 1
Size: 4932 Color: 0

Bin 173: 158 of cap free
Amount of items: 2
Items: 
Size: 14018 Color: 1
Size: 2048 Color: 0

Bin 174: 174 of cap free
Amount of items: 2
Items: 
Size: 12423 Color: 1
Size: 3627 Color: 0

Bin 175: 174 of cap free
Amount of items: 2
Items: 
Size: 10920 Color: 0
Size: 5130 Color: 1

Bin 176: 219 of cap free
Amount of items: 2
Items: 
Size: 12478 Color: 0
Size: 3527 Color: 1

Bin 177: 227 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 0
Size: 6761 Color: 1

Bin 178: 244 of cap free
Amount of items: 2
Items: 
Size: 13127 Color: 1
Size: 2853 Color: 0

Bin 179: 260 of cap free
Amount of items: 2
Items: 
Size: 10070 Color: 1
Size: 5894 Color: 0

Bin 180: 288 of cap free
Amount of items: 2
Items: 
Size: 10829 Color: 0
Size: 5107 Color: 1

Bin 181: 307 of cap free
Amount of items: 2
Items: 
Size: 9157 Color: 0
Size: 6760 Color: 1

Bin 182: 320 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 0
Size: 3976 Color: 1

Bin 183: 341 of cap free
Amount of items: 2
Items: 
Size: 11459 Color: 0
Size: 4424 Color: 1

Bin 184: 404 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 0
Size: 3512 Color: 1

Bin 185: 441 of cap free
Amount of items: 2
Items: 
Size: 13752 Color: 1
Size: 2031 Color: 0

Bin 186: 625 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 0
Size: 2626 Color: 1

Bin 187: 638 of cap free
Amount of items: 2
Items: 
Size: 12184 Color: 1
Size: 3402 Color: 0

Bin 188: 666 of cap free
Amount of items: 2
Items: 
Size: 12960 Color: 0
Size: 2598 Color: 1

Bin 189: 863 of cap free
Amount of items: 2
Items: 
Size: 11993 Color: 1
Size: 3368 Color: 0

Bin 190: 1384 of cap free
Amount of items: 2
Items: 
Size: 8117 Color: 0
Size: 6723 Color: 1

Bin 191: 1661 of cap free
Amount of items: 1
Items: 
Size: 14563 Color: 1

Bin 192: 1746 of cap free
Amount of items: 1
Items: 
Size: 14478 Color: 0

Bin 193: 1801 of cap free
Amount of items: 1
Items: 
Size: 14423 Color: 1

Bin 194: 1890 of cap free
Amount of items: 1
Items: 
Size: 14334 Color: 0

Bin 195: 1916 of cap free
Amount of items: 1
Items: 
Size: 14308 Color: 0

Bin 196: 1973 of cap free
Amount of items: 1
Items: 
Size: 14251 Color: 1

Bin 197: 2490 of cap free
Amount of items: 1
Items: 
Size: 13734 Color: 1

Bin 198: 2664 of cap free
Amount of items: 1
Items: 
Size: 13560 Color: 1

Bin 199: 2730 of cap free
Amount of items: 1
Items: 
Size: 13494 Color: 1

Bin 200: 2777 of cap free
Amount of items: 1
Items: 
Size: 13447 Color: 1

Total size: 3212352
Total free space: 32448

