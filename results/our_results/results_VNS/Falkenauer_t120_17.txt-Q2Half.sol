Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 289 Color: 1
Size: 269 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 332 Color: 1
Size: 271 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 250 Color: 0
Size: 251 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 352 Color: 1
Size: 269 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 285 Color: 1
Size: 266 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 314 Color: 1
Size: 270 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 258 Color: 1
Size: 253 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 324 Color: 1
Size: 275 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 319 Color: 1
Size: 259 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 276 Color: 0
Size: 274 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 272 Color: 1
Size: 257 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 294 Color: 1
Size: 259 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 330 Color: 1
Size: 282 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 253 Color: 0
Size: 273 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 254 Color: 1
Size: 254 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 357 Color: 1
Size: 283 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 275 Color: 1
Size: 255 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 288 Color: 1
Size: 256 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 337 Color: 1
Size: 279 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 286 Color: 0
Size: 279 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 279 Color: 1
Size: 274 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 267 Color: 1
Size: 257 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 321 Color: 1
Size: 280 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 296 Color: 1
Size: 251 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 314 Color: 1
Size: 288 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 300 Color: 1
Size: 269 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 252 Color: 1
Size: 252 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 322 Color: 1
Size: 256 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 319 Color: 1
Size: 304 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 308 Color: 1
Size: 296 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 270 Color: 1
Size: 257 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 301 Color: 0
Size: 285 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 266 Color: 1
Size: 257 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 359 Color: 1
Size: 263 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 307 Color: 1
Size: 260 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 323 Color: 1
Size: 292 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 279 Color: 1
Size: 267 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 306 Color: 1
Size: 262 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 296 Color: 1
Size: 258 Color: 0

Total size: 40000
Total free space: 0

