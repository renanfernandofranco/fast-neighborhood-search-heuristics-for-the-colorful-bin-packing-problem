Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 346750 Color: 1
Size: 317235 Color: 0
Size: 336016 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 354282 Color: 1
Size: 338750 Color: 1
Size: 306969 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 358071 Color: 1
Size: 329740 Color: 1
Size: 312190 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 363470 Color: 1
Size: 353851 Color: 1
Size: 282680 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 363891 Color: 1
Size: 351575 Color: 1
Size: 284535 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 381375 Color: 1
Size: 358429 Color: 1
Size: 260197 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 385875 Color: 1
Size: 333124 Color: 1
Size: 281002 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 388393 Color: 1
Size: 343458 Color: 1
Size: 268150 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 388950 Color: 1
Size: 332827 Color: 1
Size: 278224 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392346 Color: 1
Size: 340496 Color: 1
Size: 267159 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 393695 Color: 1
Size: 325506 Color: 1
Size: 280800 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 396258 Color: 1
Size: 330858 Color: 1
Size: 272885 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 396822 Color: 1
Size: 344006 Color: 1
Size: 259173 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 399275 Color: 1
Size: 314707 Color: 1
Size: 286019 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 407507 Color: 1
Size: 319688 Color: 1
Size: 272806 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 407712 Color: 1
Size: 310446 Color: 1
Size: 281843 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 409744 Color: 1
Size: 330295 Color: 1
Size: 259962 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 411593 Color: 1
Size: 313497 Color: 1
Size: 274911 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 419340 Color: 1
Size: 319053 Color: 1
Size: 261608 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 421368 Color: 1
Size: 317104 Color: 1
Size: 261529 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 423402 Color: 1
Size: 309467 Color: 1
Size: 267132 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 424366 Color: 1
Size: 295727 Color: 1
Size: 279908 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 427322 Color: 1
Size: 293225 Color: 1
Size: 279454 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 430905 Color: 1
Size: 292948 Color: 1
Size: 276148 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 433745 Color: 1
Size: 290996 Color: 1
Size: 275260 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 435334 Color: 1
Size: 291492 Color: 1
Size: 273175 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 436272 Color: 1
Size: 295895 Color: 1
Size: 267834 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 450132 Color: 1
Size: 284687 Color: 1
Size: 265182 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 455006 Color: 1
Size: 291280 Color: 1
Size: 253715 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 468649 Color: 1
Size: 267772 Color: 1
Size: 263580 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 482371 Color: 1
Size: 266160 Color: 1
Size: 251470 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 484931 Color: 1
Size: 264967 Color: 1
Size: 250103 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 491328 Color: 1
Size: 255182 Color: 1
Size: 253491 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 495558 Color: 1
Size: 253065 Color: 1
Size: 251378 Color: 0

Total size: 34000034
Total free space: 0

