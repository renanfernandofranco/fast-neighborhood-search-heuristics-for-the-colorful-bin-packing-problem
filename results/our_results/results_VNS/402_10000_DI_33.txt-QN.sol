Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 4928 Color: 295
Size: 2896 Color: 258

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5912 Color: 323
Size: 1608 Color: 217
Size: 304 Color: 86

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6070 Color: 331
Size: 1430 Color: 204
Size: 324 Color: 91

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6084 Color: 334
Size: 1604 Color: 216
Size: 136 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 342
Size: 1272 Color: 195
Size: 292 Color: 84

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 348
Size: 1364 Color: 200
Size: 64 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 351
Size: 1180 Color: 189
Size: 216 Color: 54

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6433 Color: 352
Size: 927 Color: 163
Size: 464 Color: 113

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 355
Size: 1042 Color: 175
Size: 320 Color: 90

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 356
Size: 1157 Color: 185
Size: 190 Color: 44

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 360
Size: 1160 Color: 186
Size: 144 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 361
Size: 942 Color: 165
Size: 360 Color: 99

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 364
Size: 1084 Color: 177
Size: 152 Color: 22

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 369
Size: 801 Color: 151
Size: 352 Color: 97

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 372
Size: 660 Color: 136
Size: 472 Color: 115

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6713 Color: 375
Size: 771 Color: 149
Size: 340 Color: 95

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 378
Size: 728 Color: 143
Size: 352 Color: 98

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 379
Size: 742 Color: 146
Size: 332 Color: 93

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 383
Size: 802 Color: 152
Size: 200 Color: 51

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 386
Size: 804 Color: 153
Size: 152 Color: 21

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 388
Size: 542 Color: 122
Size: 384 Color: 104

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 390
Size: 912 Color: 161
Size: 4 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6918 Color: 391
Size: 898 Color: 160
Size: 8 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 392
Size: 730 Color: 144
Size: 160 Color: 29

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 393
Size: 732 Color: 145
Size: 144 Color: 16

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 394
Size: 650 Color: 134
Size: 224 Color: 59

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 395
Size: 650 Color: 135
Size: 222 Color: 57

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 396
Size: 596 Color: 127
Size: 272 Color: 75

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 397
Size: 698 Color: 137
Size: 158 Color: 25

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 400
Size: 648 Color: 133
Size: 188 Color: 43

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 401
Size: 640 Color: 129
Size: 194 Color: 50

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7036 Color: 402
Size: 500 Color: 119
Size: 288 Color: 82

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6072 Color: 332
Size: 1751 Color: 224

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6081 Color: 333
Size: 1462 Color: 208
Size: 280 Color: 77

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 345
Size: 1453 Color: 207
Size: 36 Color: 3

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6437 Color: 353
Size: 1096 Color: 180
Size: 290 Color: 83

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6481 Color: 357
Size: 1182 Color: 190
Size: 160 Color: 28

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6517 Color: 359
Size: 934 Color: 164
Size: 372 Color: 101

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6662 Color: 368
Size: 1161 Color: 187

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6677 Color: 370
Size: 710 Color: 139
Size: 436 Color: 110

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 384
Size: 961 Color: 169

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 5367 Color: 305
Size: 1691 Color: 222
Size: 764 Color: 148

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 5900 Color: 322
Size: 1922 Color: 232

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 6182 Color: 339
Size: 1196 Color: 191
Size: 444 Color: 111

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 344
Size: 1494 Color: 210
Size: 16 Color: 2

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 6385 Color: 347
Size: 1437 Color: 205

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 6736 Color: 377
Size: 1086 Color: 179

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 385
Size: 957 Color: 168

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 398
Size: 844 Color: 156

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6984 Color: 399
Size: 838 Color: 155

Bin 51: 3 of cap free
Amount of items: 7
Items: 
Size: 3917 Color: 275
Size: 880 Color: 158
Size: 808 Color: 154
Size: 774 Color: 150
Size: 758 Color: 147
Size: 508 Color: 120
Size: 176 Color: 38

Bin 52: 3 of cap free
Amount of items: 2
Items: 
Size: 5707 Color: 317
Size: 2114 Color: 238

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 328
Size: 1812 Color: 228

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 335
Size: 1528 Color: 211
Size: 192 Color: 49

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 374
Size: 1115 Color: 182

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 6901 Color: 389
Size: 920 Color: 162

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 3921 Color: 277
Size: 3899 Color: 271

Bin 58: 4 of cap free
Amount of items: 7
Items: 
Size: 3928 Color: 278
Size: 1308 Color: 198
Size: 1086 Color: 178
Size: 970 Color: 171
Size: 176 Color: 39
Size: 176 Color: 36
Size: 176 Color: 35

Bin 59: 4 of cap free
Amount of items: 5
Items: 
Size: 3932 Color: 280
Size: 3236 Color: 262
Size: 316 Color: 89
Size: 168 Color: 33
Size: 168 Color: 32

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 5392 Color: 306
Size: 2428 Color: 247

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 5652 Color: 314
Size: 2168 Color: 239

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 318
Size: 1572 Color: 213
Size: 466 Color: 114

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 341
Size: 1573 Color: 214

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6720 Color: 376
Size: 1100 Color: 181

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6872 Color: 387
Size: 948 Color: 167

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 310
Size: 1589 Color: 215
Size: 712 Color: 140

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6054 Color: 330
Size: 1765 Color: 225

Bin 68: 5 of cap free
Amount of items: 2
Items: 
Size: 6141 Color: 337
Size: 1678 Color: 221

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6508 Color: 358
Size: 1311 Color: 199

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 366
Size: 1201 Color: 192

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 6698 Color: 373
Size: 1121 Color: 183

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 5202 Color: 301
Size: 2616 Color: 251

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6524 Color: 362
Size: 1294 Color: 196

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 371
Size: 1138 Color: 184

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6786 Color: 381
Size: 1032 Color: 174

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 6812 Color: 382
Size: 1006 Color: 173

Bin 77: 7 of cap free
Amount of items: 2
Items: 
Size: 5797 Color: 319
Size: 2020 Color: 235

Bin 78: 8 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 294
Size: 2756 Color: 254
Size: 144 Color: 18

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 5992 Color: 327
Size: 1824 Color: 229

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 343
Size: 1542 Color: 212

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 363
Size: 1242 Color: 194

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 6600 Color: 365
Size: 1216 Color: 193

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 367
Size: 1164 Color: 188

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 380
Size: 1060 Color: 176

Bin 85: 9 of cap free
Amount of items: 3
Items: 
Size: 4956 Color: 296
Size: 2715 Color: 253
Size: 144 Color: 17

Bin 86: 9 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 350
Size: 1403 Color: 203

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 5356 Color: 304
Size: 2458 Color: 248

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 321
Size: 1928 Color: 233
Size: 40 Color: 4

Bin 89: 11 of cap free
Amount of items: 3
Items: 
Size: 3948 Color: 281
Size: 3701 Color: 270
Size: 164 Color: 31

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 4822 Color: 292
Size: 2991 Color: 260

Bin 91: 12 of cap free
Amount of items: 3
Items: 
Size: 5672 Color: 315
Size: 2060 Color: 236
Size: 80 Color: 6

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 6110 Color: 336
Size: 1702 Color: 223

Bin 93: 12 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 346
Size: 1464 Color: 209

Bin 94: 14 of cap free
Amount of items: 2
Items: 
Size: 6410 Color: 349
Size: 1400 Color: 202

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 6440 Color: 354
Size: 1370 Color: 201

Bin 96: 15 of cap free
Amount of items: 5
Items: 
Size: 3929 Color: 279
Size: 1676 Color: 220
Size: 1452 Color: 206
Size: 576 Color: 126
Size: 176 Color: 34

Bin 97: 15 of cap free
Amount of items: 2
Items: 
Size: 5453 Color: 308
Size: 2356 Color: 244

Bin 98: 17 of cap free
Amount of items: 4
Items: 
Size: 5210 Color: 302
Size: 2341 Color: 243
Size: 128 Color: 10
Size: 128 Color: 9

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 5919 Color: 324
Size: 1887 Color: 231

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 6034 Color: 329
Size: 1772 Color: 226

Bin 101: 19 of cap free
Amount of items: 3
Items: 
Size: 4775 Color: 291
Size: 2882 Color: 257
Size: 148 Color: 20

Bin 102: 20 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 340
Size: 1616 Color: 218

Bin 103: 22 of cap free
Amount of items: 2
Items: 
Size: 5940 Color: 325
Size: 1862 Color: 230

Bin 104: 22 of cap free
Amount of items: 2
Items: 
Size: 6152 Color: 338
Size: 1650 Color: 219

Bin 105: 23 of cap free
Amount of items: 4
Items: 
Size: 4237 Color: 282
Size: 3244 Color: 263
Size: 160 Color: 30
Size: 160 Color: 27

Bin 106: 24 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 293
Size: 2774 Color: 255
Size: 148 Color: 19

Bin 107: 24 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 307
Size: 2396 Color: 246

Bin 108: 24 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 309
Size: 1308 Color: 197
Size: 980 Color: 172

Bin 109: 24 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 311
Size: 2182 Color: 240
Size: 96 Color: 7

Bin 110: 32 of cap free
Amount of items: 2
Items: 
Size: 5561 Color: 312
Size: 2231 Color: 242

Bin 111: 33 of cap free
Amount of items: 2
Items: 
Size: 5814 Color: 320
Size: 1977 Color: 234

Bin 112: 36 of cap free
Amount of items: 2
Items: 
Size: 5988 Color: 326
Size: 1800 Color: 227

Bin 113: 37 of cap free
Amount of items: 3
Items: 
Size: 5149 Color: 300
Size: 2502 Color: 249
Size: 136 Color: 12

Bin 114: 38 of cap free
Amount of items: 2
Items: 
Size: 4524 Color: 288
Size: 3262 Color: 269

Bin 115: 39 of cap free
Amount of items: 2
Items: 
Size: 5700 Color: 316
Size: 2085 Color: 237

Bin 116: 44 of cap free
Amount of items: 2
Items: 
Size: 4984 Color: 297
Size: 2796 Color: 256

Bin 117: 44 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 313
Size: 2186 Color: 241

Bin 118: 46 of cap free
Amount of items: 6
Items: 
Size: 3918 Color: 276
Size: 968 Color: 170
Size: 944 Color: 166
Size: 892 Color: 159
Size: 880 Color: 157
Size: 176 Color: 37

Bin 119: 47 of cap free
Amount of items: 3
Items: 
Size: 4567 Color: 289
Size: 3058 Color: 261
Size: 152 Color: 24

Bin 120: 47 of cap free
Amount of items: 3
Items: 
Size: 5017 Color: 299
Size: 2624 Color: 252
Size: 136 Color: 13

Bin 121: 65 of cap free
Amount of items: 2
Items: 
Size: 4498 Color: 287
Size: 3261 Color: 268

Bin 122: 67 of cap free
Amount of items: 3
Items: 
Size: 4344 Color: 283
Size: 3253 Color: 264
Size: 160 Color: 26

Bin 123: 72 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 290
Size: 2904 Color: 259
Size: 152 Color: 23

Bin 124: 86 of cap free
Amount of items: 9
Items: 
Size: 3914 Color: 273
Size: 648 Color: 130
Size: 640 Color: 128
Size: 552 Color: 125
Size: 552 Color: 124
Size: 544 Color: 123
Size: 512 Color: 121
Size: 192 Color: 45
Size: 184 Color: 42

Bin 125: 88 of cap free
Amount of items: 2
Items: 
Size: 4476 Color: 286
Size: 3260 Color: 267

Bin 126: 92 of cap free
Amount of items: 8
Items: 
Size: 3916 Color: 274
Size: 728 Color: 142
Size: 724 Color: 141
Size: 700 Color: 138
Size: 648 Color: 132
Size: 648 Color: 131
Size: 184 Color: 41
Size: 184 Color: 40

Bin 127: 96 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 303
Size: 2376 Color: 245
Size: 128 Color: 8

Bin 128: 116 of cap free
Amount of items: 2
Items: 
Size: 4450 Color: 285
Size: 3258 Color: 266

Bin 129: 126 of cap free
Amount of items: 2
Items: 
Size: 4442 Color: 284
Size: 3256 Color: 265

Bin 130: 131 of cap free
Amount of items: 12
Items: 
Size: 3913 Color: 272
Size: 492 Color: 118
Size: 488 Color: 117
Size: 480 Color: 116
Size: 464 Color: 112
Size: 432 Color: 109
Size: 432 Color: 108
Size: 208 Color: 53
Size: 208 Color: 52
Size: 192 Color: 48
Size: 192 Color: 47
Size: 192 Color: 46

Bin 131: 135 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 298
Size: 2541 Color: 250
Size: 144 Color: 15

Bin 132: 194 of cap free
Amount of items: 26
Items: 
Size: 408 Color: 107
Size: 400 Color: 106
Size: 394 Color: 105
Size: 384 Color: 103
Size: 376 Color: 102
Size: 368 Color: 100
Size: 352 Color: 96
Size: 336 Color: 94
Size: 328 Color: 92
Size: 312 Color: 88
Size: 304 Color: 87
Size: 296 Color: 85
Size: 288 Color: 81
Size: 288 Color: 80
Size: 286 Color: 79
Size: 238 Color: 67
Size: 232 Color: 66
Size: 232 Color: 65
Size: 232 Color: 64
Size: 232 Color: 63
Size: 230 Color: 62
Size: 230 Color: 61
Size: 228 Color: 60
Size: 224 Color: 58
Size: 216 Color: 56
Size: 216 Color: 55

Bin 133: 5500 of cap free
Amount of items: 9
Items: 
Size: 284 Color: 78
Size: 272 Color: 76
Size: 272 Color: 74
Size: 256 Color: 73
Size: 256 Color: 72
Size: 256 Color: 71
Size: 248 Color: 70
Size: 240 Color: 69
Size: 240 Color: 68

Total size: 1032768
Total free space: 7824

