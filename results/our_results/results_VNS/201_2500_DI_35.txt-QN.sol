Capicity Bin: 2400
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 164
Size: 549 Color: 109
Size: 108 Color: 41

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 168
Size: 453 Color: 102
Size: 142 Color: 53

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 169
Size: 491 Color: 105
Size: 96 Color: 37

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1881 Color: 173
Size: 427 Color: 98
Size: 92 Color: 36

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 174
Size: 466 Color: 104
Size: 52 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 176
Size: 406 Color: 97
Size: 80 Color: 30

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1920 Color: 177
Size: 382 Color: 94
Size: 98 Color: 38

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 178
Size: 283 Color: 79
Size: 196 Color: 67

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 183
Size: 226 Color: 71
Size: 192 Color: 65

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 185
Size: 331 Color: 87
Size: 64 Color: 23

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2041 Color: 187
Size: 301 Color: 84
Size: 58 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 190
Size: 293 Color: 81
Size: 50 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 194
Size: 238 Color: 72
Size: 72 Color: 26

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 198
Size: 168 Color: 61
Size: 108 Color: 42

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 201
Size: 174 Color: 63
Size: 72 Color: 27

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 163
Size: 497 Color: 106
Size: 164 Color: 58

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 167
Size: 605 Color: 112

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1958 Color: 180
Size: 441 Color: 101

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1998 Color: 184
Size: 401 Color: 96

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 2049 Color: 189
Size: 350 Color: 90

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 2061 Color: 191
Size: 338 Color: 89

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 2070 Color: 192
Size: 329 Color: 86

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 193
Size: 160 Color: 57
Size: 160 Color: 56

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 2103 Color: 196
Size: 296 Color: 82

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2130 Color: 199
Size: 269 Color: 78

Bin 26: 2 of cap free
Amount of items: 4
Items: 
Size: 1234 Color: 144
Size: 1052 Color: 137
Size: 56 Color: 16
Size: 56 Color: 15

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 152
Size: 835 Color: 126
Size: 40 Color: 6

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 2042 Color: 188
Size: 356 Color: 91

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 200
Size: 252 Color: 75

Bin 30: 3 of cap free
Amount of items: 5
Items: 
Size: 1209 Color: 142
Size: 974 Color: 131
Size: 90 Color: 35
Size: 64 Color: 21
Size: 60 Color: 20

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1395 Color: 147
Size: 1002 Color: 135

Bin 32: 3 of cap free
Amount of items: 5
Items: 
Size: 1411 Color: 149
Size: 822 Color: 124
Size: 68 Color: 25
Size: 48 Color: 11
Size: 48 Color: 10

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 155
Size: 842 Color: 128

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 161
Size: 711 Color: 119
Size: 16 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 172
Size: 332 Color: 88
Size: 192 Color: 64

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1937 Color: 179
Size: 460 Color: 103

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1963 Color: 182
Size: 434 Color: 100

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 2095 Color: 195
Size: 302 Color: 85

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 2110 Color: 197
Size: 287 Color: 80

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 145
Size: 983 Color: 132
Size: 48 Color: 12

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1399 Color: 148
Size: 997 Color: 134

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 153
Size: 825 Color: 125
Size: 32 Color: 5

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 166
Size: 619 Color: 115

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1842 Color: 170
Size: 554 Color: 110

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1889 Color: 175
Size: 506 Color: 107

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1675 Color: 162
Size: 719 Color: 120

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 181
Size: 433 Color: 99

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 2007 Color: 186
Size: 387 Color: 95

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1205 Color: 141
Size: 1188 Color: 138

Bin 50: 7 of cap free
Amount of items: 4
Items: 
Size: 1659 Color: 159
Size: 678 Color: 117
Size: 32 Color: 3
Size: 24 Color: 2

Bin 51: 8 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 156
Size: 790 Color: 122
Size: 32 Color: 4

Bin 52: 8 of cap free
Amount of items: 2
Items: 
Size: 1590 Color: 157
Size: 802 Color: 123

Bin 53: 8 of cap free
Amount of items: 2
Items: 
Size: 1761 Color: 165
Size: 631 Color: 116

Bin 54: 10 of cap free
Amount of items: 2
Items: 
Size: 1857 Color: 171
Size: 533 Color: 108

Bin 55: 12 of cap free
Amount of items: 5
Items: 
Size: 1201 Color: 139
Size: 610 Color: 113
Size: 367 Color: 93
Size: 146 Color: 55
Size: 64 Color: 22

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 160
Size: 705 Color: 118
Size: 16 Color: 1

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1547 Color: 154
Size: 839 Color: 127

Bin 58: 17 of cap free
Amount of items: 2
Items: 
Size: 1390 Color: 146
Size: 993 Color: 133

Bin 59: 23 of cap free
Amount of items: 12
Items: 
Size: 365 Color: 92
Size: 298 Color: 83
Size: 262 Color: 77
Size: 255 Color: 76
Size: 249 Color: 74
Size: 242 Color: 73
Size: 206 Color: 70
Size: 198 Color: 69
Size: 84 Color: 31
Size: 78 Color: 29
Size: 76 Color: 28
Size: 64 Color: 24

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 158
Size: 731 Color: 121

Bin 61: 28 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 140
Size: 611 Color: 114
Size: 559 Color: 111

Bin 62: 31 of cap free
Amount of items: 4
Items: 
Size: 1418 Color: 150
Size: 863 Color: 129
Size: 44 Color: 9
Size: 44 Color: 8

Bin 63: 34 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 151
Size: 888 Color: 130
Size: 40 Color: 7

Bin 64: 40 of cap free
Amount of items: 4
Items: 
Size: 1221 Color: 143
Size: 1025 Color: 136
Size: 58 Color: 18
Size: 56 Color: 17

Bin 65: 114 of cap free
Amount of items: 17
Items: 
Size: 198 Color: 68
Size: 196 Color: 66
Size: 172 Color: 62
Size: 166 Color: 60
Size: 166 Color: 59
Size: 144 Color: 54
Size: 142 Color: 52
Size: 140 Color: 51
Size: 136 Color: 50
Size: 132 Color: 49
Size: 120 Color: 44
Size: 112 Color: 43
Size: 106 Color: 40
Size: 100 Color: 39
Size: 86 Color: 34
Size: 86 Color: 33
Size: 84 Color: 32

Bin 66: 1910 of cap free
Amount of items: 4
Items: 
Size: 126 Color: 48
Size: 122 Color: 47
Size: 122 Color: 46
Size: 120 Color: 45

Total size: 156000
Total free space: 2400

