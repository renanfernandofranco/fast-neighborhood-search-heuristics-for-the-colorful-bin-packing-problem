Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 1544 Color: 151
Size: 920 Color: 132

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 161
Size: 388 Color: 90
Size: 354 Color: 86

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1740 Color: 162
Size: 628 Color: 114
Size: 96 Color: 39

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 163
Size: 581 Color: 112
Size: 114 Color: 46

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 170
Size: 516 Color: 105
Size: 88 Color: 36

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 171
Size: 503 Color: 102
Size: 100 Color: 41

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1927 Color: 174
Size: 401 Color: 92
Size: 136 Color: 52

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 175
Size: 447 Color: 96
Size: 88 Color: 37

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1930 Color: 176
Size: 446 Color: 95
Size: 88 Color: 32

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 185
Size: 322 Color: 82
Size: 100 Color: 42

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 186
Size: 351 Color: 85
Size: 70 Color: 25

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 187
Size: 324 Color: 83
Size: 96 Color: 40

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2051 Color: 188
Size: 277 Color: 77
Size: 136 Color: 51

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 190
Size: 356 Color: 87
Size: 24 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 192
Size: 305 Color: 80
Size: 60 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 193
Size: 176 Color: 61
Size: 172 Color: 59

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 196
Size: 268 Color: 74
Size: 48 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 199
Size: 212 Color: 69
Size: 54 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 200
Size: 200 Color: 65
Size: 60 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2212 Color: 201
Size: 200 Color: 64
Size: 52 Color: 12

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 150
Size: 883 Color: 130
Size: 48 Color: 10

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 165
Size: 682 Color: 117

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1850 Color: 168
Size: 573 Color: 110
Size: 40 Color: 5

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1958 Color: 178
Size: 505 Color: 104

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 181
Size: 290 Color: 78
Size: 176 Color: 62

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 194
Size: 345 Color: 84

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2133 Color: 195
Size: 262 Color: 73
Size: 68 Color: 22

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1898 Color: 172
Size: 564 Color: 108

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1983 Color: 179
Size: 471 Color: 100
Size: 8 Color: 0

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2078 Color: 189
Size: 384 Color: 89

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2170 Color: 198
Size: 292 Color: 79

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 156
Size: 571 Color: 109
Size: 246 Color: 72

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 164
Size: 684 Color: 118

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 169
Size: 562 Color: 107
Size: 40 Color: 6

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 183
Size: 449 Color: 97
Size: 8 Color: 1

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 2025 Color: 184
Size: 428 Color: 93
Size: 8 Color: 2

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 2150 Color: 197
Size: 311 Color: 81

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 159
Size: 761 Color: 120
Size: 28 Color: 4

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1956 Color: 177
Size: 504 Color: 103

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1998 Color: 182
Size: 462 Color: 98

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 2093 Color: 191
Size: 367 Color: 88

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 160
Size: 763 Color: 121

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1796 Color: 167
Size: 663 Color: 116

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1985 Color: 180
Size: 474 Color: 101

Bin 45: 9 of cap free
Amount of items: 7
Items: 
Size: 1233 Color: 138
Size: 390 Color: 91
Size: 272 Color: 76
Size: 272 Color: 75
Size: 120 Color: 48
Size: 88 Color: 33
Size: 80 Color: 31

Bin 46: 10 of cap free
Amount of items: 2
Items: 
Size: 1908 Color: 173
Size: 546 Color: 106

Bin 47: 13 of cap free
Amount of items: 2
Items: 
Size: 1790 Color: 166
Size: 661 Color: 115

Bin 48: 15 of cap free
Amount of items: 2
Items: 
Size: 1669 Color: 158
Size: 780 Color: 122

Bin 49: 16 of cap free
Amount of items: 3
Items: 
Size: 1646 Color: 157
Size: 758 Color: 119
Size: 44 Color: 7

Bin 50: 17 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 139
Size: 468 Color: 99
Size: 443 Color: 94
Size: 222 Color: 71
Size: 80 Color: 30

Bin 51: 20 of cap free
Amount of items: 4
Items: 
Size: 1450 Color: 149
Size: 882 Color: 129
Size: 56 Color: 15
Size: 56 Color: 14

Bin 52: 20 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 155
Size: 846 Color: 125

Bin 53: 22 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 154
Size: 840 Color: 124
Size: 44 Color: 8

Bin 54: 23 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 152
Size: 892 Color: 131

Bin 55: 27 of cap free
Amount of items: 3
Items: 
Size: 1551 Color: 153
Size: 838 Color: 123
Size: 48 Color: 9

Bin 56: 30 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 145
Size: 1028 Color: 137

Bin 57: 31 of cap free
Amount of items: 3
Items: 
Size: 1235 Color: 140
Size: 622 Color: 113
Size: 576 Color: 111

Bin 58: 32 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 144
Size: 1027 Color: 136

Bin 59: 42 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 143
Size: 1026 Color: 135

Bin 60: 44 of cap free
Amount of items: 4
Items: 
Size: 1418 Color: 148
Size: 882 Color: 128
Size: 64 Color: 19
Size: 56 Color: 16

Bin 61: 45 of cap free
Amount of items: 4
Items: 
Size: 1410 Color: 147
Size: 881 Color: 127
Size: 64 Color: 21
Size: 64 Color: 20

Bin 62: 47 of cap free
Amount of items: 4
Items: 
Size: 1407 Color: 146
Size: 874 Color: 126
Size: 68 Color: 24
Size: 68 Color: 23

Bin 63: 53 of cap free
Amount of items: 4
Items: 
Size: 1242 Color: 142
Size: 1025 Color: 134
Size: 72 Color: 27
Size: 72 Color: 26

Bin 64: 58 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 141
Size: 1022 Color: 133
Size: 76 Color: 29
Size: 72 Color: 28

Bin 65: 78 of cap free
Amount of items: 16
Items: 
Size: 220 Color: 70
Size: 204 Color: 68
Size: 204 Color: 67
Size: 204 Color: 66
Size: 176 Color: 63
Size: 176 Color: 60
Size: 172 Color: 58
Size: 168 Color: 57
Size: 152 Color: 56
Size: 114 Color: 47
Size: 112 Color: 45
Size: 112 Color: 44
Size: 104 Color: 43
Size: 92 Color: 38
Size: 88 Color: 35
Size: 88 Color: 34

Bin 66: 1748 of cap free
Amount of items: 5
Items: 
Size: 152 Color: 55
Size: 152 Color: 54
Size: 148 Color: 53
Size: 132 Color: 50
Size: 132 Color: 49

Total size: 160160
Total free space: 2464

