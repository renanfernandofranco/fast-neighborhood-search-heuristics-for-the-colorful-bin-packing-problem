Capicity Bin: 14912
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 31
Items: 
Size: 594 Color: 133
Size: 580 Color: 132
Size: 576 Color: 131
Size: 576 Color: 130
Size: 576 Color: 129
Size: 546 Color: 126
Size: 544 Color: 125
Size: 530 Color: 123
Size: 528 Color: 122
Size: 528 Color: 121
Size: 524 Color: 120
Size: 524 Color: 119
Size: 516 Color: 118
Size: 512 Color: 116
Size: 504 Color: 115
Size: 472 Color: 106
Size: 448 Color: 100
Size: 448 Color: 99
Size: 440 Color: 96
Size: 440 Color: 95
Size: 430 Color: 92
Size: 424 Color: 91
Size: 420 Color: 90
Size: 416 Color: 89
Size: 416 Color: 88
Size: 416 Color: 87
Size: 404 Color: 85
Size: 400 Color: 84
Size: 400 Color: 83
Size: 392 Color: 82
Size: 388 Color: 81

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 7464 Color: 409
Size: 1240 Color: 198
Size: 1232 Color: 196
Size: 1216 Color: 194
Size: 1216 Color: 193
Size: 1152 Color: 192
Size: 720 Color: 154
Size: 336 Color: 58
Size: 336 Color: 57

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 431
Size: 5336 Color: 383
Size: 272 Color: 25

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11057 Color: 471
Size: 2472 Color: 302
Size: 1383 Color: 211

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11097 Color: 473
Size: 3517 Color: 343
Size: 298 Color: 36

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11161 Color: 477
Size: 3525 Color: 344
Size: 226 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11164 Color: 478
Size: 3372 Color: 339
Size: 376 Color: 76

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11416 Color: 483
Size: 3208 Color: 333
Size: 288 Color: 31

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11618 Color: 488
Size: 1801 Color: 254
Size: 1493 Color: 223

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11628 Color: 489
Size: 2850 Color: 316
Size: 434 Color: 94

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 499
Size: 2616 Color: 307
Size: 444 Color: 97

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 508
Size: 1888 Color: 261
Size: 944 Color: 178

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 510
Size: 2020 Color: 269
Size: 784 Color: 161

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12188 Color: 514
Size: 2212 Color: 281
Size: 512 Color: 117

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 515
Size: 2436 Color: 299
Size: 276 Color: 26

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12304 Color: 520
Size: 2256 Color: 285
Size: 352 Color: 64

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12314 Color: 522
Size: 2298 Color: 288
Size: 300 Color: 37

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12329 Color: 524
Size: 2299 Color: 289
Size: 284 Color: 30

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12365 Color: 526
Size: 2173 Color: 279
Size: 374 Color: 75

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12368 Color: 527
Size: 1448 Color: 218
Size: 1096 Color: 188

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 528
Size: 1470 Color: 221
Size: 1072 Color: 187

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12492 Color: 533
Size: 1604 Color: 233
Size: 816 Color: 167

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12580 Color: 538
Size: 1864 Color: 258
Size: 468 Color: 105

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 540
Size: 1664 Color: 240
Size: 632 Color: 140

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12660 Color: 543
Size: 1948 Color: 264
Size: 304 Color: 39

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12706 Color: 546
Size: 1514 Color: 225
Size: 692 Color: 150

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12770 Color: 550
Size: 1566 Color: 230
Size: 576 Color: 128

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12808 Color: 554
Size: 1464 Color: 220
Size: 640 Color: 144

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12848 Color: 557
Size: 1232 Color: 197
Size: 832 Color: 170

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12867 Color: 558
Size: 1769 Color: 250
Size: 276 Color: 27

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12908 Color: 561
Size: 1456 Color: 219
Size: 548 Color: 127

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12921 Color: 562
Size: 1661 Color: 238
Size: 330 Color: 55

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12930 Color: 563
Size: 1654 Color: 236
Size: 328 Color: 54

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12985 Color: 567
Size: 1607 Color: 234
Size: 320 Color: 51

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 568
Size: 1492 Color: 222
Size: 432 Color: 93

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12997 Color: 569
Size: 1597 Color: 232
Size: 318 Color: 44

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13057 Color: 572
Size: 1547 Color: 228
Size: 308 Color: 41

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13083 Color: 574
Size: 1525 Color: 227
Size: 304 Color: 38

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13098 Color: 576
Size: 1402 Color: 212
Size: 412 Color: 86

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 577
Size: 1444 Color: 217
Size: 356 Color: 66

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 584
Size: 1380 Color: 210
Size: 344 Color: 61

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13214 Color: 587
Size: 1096 Color: 191
Size: 602 Color: 136

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 589
Size: 1424 Color: 215
Size: 256 Color: 14

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 592
Size: 1056 Color: 185
Size: 596 Color: 134

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 594
Size: 976 Color: 182
Size: 648 Color: 146

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13330 Color: 596
Size: 1242 Color: 201
Size: 340 Color: 59

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13348 Color: 597
Size: 1240 Color: 200
Size: 324 Color: 52

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13354 Color: 598
Size: 1302 Color: 204
Size: 256 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 600
Size: 774 Color: 159
Size: 768 Color: 157

Bin 50: 1 of cap free
Amount of items: 7
Items: 
Size: 7496 Color: 412
Size: 1768 Color: 249
Size: 1757 Color: 247
Size: 1664 Color: 239
Size: 1522 Color: 226
Size: 384 Color: 80
Size: 320 Color: 48

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 10868 Color: 466
Size: 3933 Color: 355
Size: 110 Color: 4

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 472
Size: 3847 Color: 350

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 11121 Color: 475
Size: 3342 Color: 338
Size: 448 Color: 98

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 485
Size: 3483 Color: 342

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 11943 Color: 502
Size: 2968 Color: 323

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 12003 Color: 506
Size: 2908 Color: 319

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12086 Color: 509
Size: 2153 Color: 276
Size: 672 Color: 149

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12305 Color: 521
Size: 1374 Color: 209
Size: 1232 Color: 195

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 12457 Color: 531
Size: 2454 Color: 301

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 12553 Color: 535
Size: 2358 Color: 295

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 12599 Color: 539
Size: 2312 Color: 291

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 12751 Color: 549
Size: 2160 Color: 277

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 12803 Color: 553
Size: 2108 Color: 273

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13121 Color: 578
Size: 1780 Color: 252
Size: 10 Color: 0

Bin 65: 2 of cap free
Amount of items: 9
Items: 
Size: 7462 Color: 408
Size: 1096 Color: 190
Size: 1096 Color: 189
Size: 1072 Color: 186
Size: 1008 Color: 184
Size: 992 Color: 183
Size: 976 Color: 179
Size: 864 Color: 171
Size: 344 Color: 60

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 10516 Color: 457
Size: 4394 Color: 369

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 461
Size: 4084 Color: 359
Size: 178 Color: 8

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 503
Size: 2918 Color: 320
Size: 32 Color: 2

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 11970 Color: 504
Size: 2940 Color: 322

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 12324 Color: 523
Size: 2586 Color: 306

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 536
Size: 2348 Color: 294

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 548
Size: 2166 Color: 278

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 579
Size: 1786 Color: 253

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 583
Size: 1734 Color: 245

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 590
Size: 1676 Color: 241

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 595
Size: 1586 Color: 231

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13362 Color: 599
Size: 1548 Color: 229

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 11466 Color: 487
Size: 2907 Color: 318
Size: 536 Color: 124

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 12434 Color: 530
Size: 2475 Color: 303

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 13034 Color: 571
Size: 1875 Color: 259

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 13133 Color: 580
Size: 1776 Color: 251

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 13150 Color: 581
Size: 1759 Color: 248

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13253 Color: 591
Size: 1656 Color: 237

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 11746 Color: 495
Size: 3162 Color: 331

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 497
Size: 3124 Color: 328

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 12352 Color: 525
Size: 2556 Color: 305

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 12716 Color: 547
Size: 2192 Color: 280

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 12946 Color: 565
Size: 1962 Color: 266

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13266 Color: 593
Size: 1642 Color: 235

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 10304 Color: 455
Size: 4347 Color: 367
Size: 256 Color: 15

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 11330 Color: 480
Size: 2977 Color: 324
Size: 600 Color: 135

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 11425 Color: 484
Size: 3482 Color: 341

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 518
Size: 2664 Color: 311

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 552
Size: 2123 Color: 275

Bin 95: 6 of cap free
Amount of items: 9
Items: 
Size: 7472 Color: 410
Size: 1322 Color: 206
Size: 1308 Color: 205
Size: 1294 Color: 203
Size: 1286 Color: 202
Size: 1240 Color: 199
Size: 336 Color: 56
Size: 328 Color: 53
Size: 320 Color: 50

Bin 96: 6 of cap free
Amount of items: 3
Items: 
Size: 10738 Color: 465
Size: 4040 Color: 358
Size: 128 Color: 5

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 12481 Color: 532
Size: 2425 Color: 298

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 542
Size: 2264 Color: 286

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 13201 Color: 586
Size: 1705 Color: 244

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 588
Size: 1682 Color: 242

Bin 101: 7 of cap free
Amount of items: 2
Items: 
Size: 11297 Color: 479
Size: 3608 Color: 347

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 11664 Color: 491
Size: 3241 Color: 335

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 11692 Color: 492
Size: 3213 Color: 334

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 12568 Color: 537
Size: 2337 Color: 292

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 12680 Color: 545
Size: 2225 Color: 283

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 12977 Color: 566
Size: 1928 Color: 263

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 10224 Color: 450
Size: 4680 Color: 373

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 11023 Color: 470
Size: 3881 Color: 352

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 11368 Color: 482
Size: 3536 Color: 345

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 12008 Color: 507
Size: 2896 Color: 317

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 12158 Color: 513
Size: 2746 Color: 315

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13010 Color: 570
Size: 1894 Color: 262

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13160 Color: 582
Size: 1744 Color: 246

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13200 Color: 585
Size: 1704 Color: 243

Bin 115: 9 of cap free
Amount of items: 3
Items: 
Size: 10194 Color: 449
Size: 4453 Color: 370
Size: 256 Color: 20

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 541
Size: 2276 Color: 287

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 12663 Color: 544
Size: 2240 Color: 284

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 564
Size: 1967 Color: 267

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 10008 Color: 444
Size: 4894 Color: 378

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 494
Size: 3181 Color: 332

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 519
Size: 2642 Color: 309

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 12780 Color: 551
Size: 2122 Color: 274

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12894 Color: 560
Size: 2008 Color: 268

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 573
Size: 1842 Color: 257

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13086 Color: 575
Size: 1816 Color: 255

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 11341 Color: 481
Size: 3560 Color: 346

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 12240 Color: 517
Size: 2661 Color: 310

Bin 128: 11 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 556
Size: 2067 Color: 272

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 9360 Color: 432
Size: 5540 Color: 390

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 505
Size: 2920 Color: 321

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 11145 Color: 476
Size: 3754 Color: 349

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 12872 Color: 559
Size: 2027 Color: 270

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 11757 Color: 496
Size: 3141 Color: 329

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 12832 Color: 555
Size: 2066 Color: 271

Bin 135: 16 of cap free
Amount of items: 2
Items: 
Size: 11440 Color: 486
Size: 3456 Color: 340

Bin 136: 17 of cap free
Amount of items: 3
Items: 
Size: 10297 Color: 454
Size: 4342 Color: 366
Size: 256 Color: 16

Bin 137: 17 of cap free
Amount of items: 2
Items: 
Size: 11909 Color: 501
Size: 2986 Color: 325

Bin 138: 18 of cap free
Amount of items: 3
Items: 
Size: 10233 Color: 451
Size: 2444 Color: 300
Size: 2217 Color: 282

Bin 139: 18 of cap free
Amount of items: 3
Items: 
Size: 10257 Color: 452
Size: 4381 Color: 368
Size: 256 Color: 18

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 468
Size: 3934 Color: 356

Bin 141: 19 of cap free
Amount of items: 7
Items: 
Size: 7476 Color: 411
Size: 1512 Color: 224
Size: 1427 Color: 216
Size: 1418 Color: 214
Size: 1416 Color: 213
Size: 1324 Color: 207
Size: 320 Color: 49

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 9240 Color: 430
Size: 5372 Color: 384
Size: 280 Color: 28

Bin 143: 21 of cap free
Amount of items: 2
Items: 
Size: 12155 Color: 512
Size: 2736 Color: 314

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 12388 Color: 529
Size: 2503 Color: 304

Bin 145: 23 of cap free
Amount of items: 2
Items: 
Size: 11633 Color: 490
Size: 3256 Color: 336

Bin 146: 24 of cap free
Amount of items: 3
Items: 
Size: 10602 Color: 460
Size: 4098 Color: 362
Size: 188 Color: 9

Bin 147: 24 of cap free
Amount of items: 2
Items: 
Size: 12504 Color: 534
Size: 2384 Color: 296

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 11874 Color: 500
Size: 3013 Color: 326

Bin 149: 25 of cap free
Amount of items: 2
Items: 
Size: 12203 Color: 516
Size: 2684 Color: 312

Bin 150: 26 of cap free
Amount of items: 4
Items: 
Size: 11118 Color: 474
Size: 3668 Color: 348
Size: 68 Color: 3
Size: 32 Color: 1

Bin 151: 27 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 511
Size: 2733 Color: 313

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 10906 Color: 467
Size: 3978 Color: 357

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 11810 Color: 498
Size: 3072 Color: 327

Bin 154: 31 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 493
Size: 3161 Color: 330

Bin 155: 35 of cap free
Amount of items: 2
Items: 
Size: 10693 Color: 462
Size: 4184 Color: 364

Bin 156: 35 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 469
Size: 3861 Color: 351

Bin 157: 36 of cap free
Amount of items: 2
Items: 
Size: 9492 Color: 434
Size: 5384 Color: 385

Bin 158: 39 of cap free
Amount of items: 3
Items: 
Size: 10193 Color: 448
Size: 3312 Color: 337
Size: 1368 Color: 208

Bin 159: 40 of cap free
Amount of items: 3
Items: 
Size: 9896 Color: 441
Size: 2631 Color: 308
Size: 2345 Color: 293

Bin 160: 42 of cap free
Amount of items: 2
Items: 
Size: 9376 Color: 433
Size: 5494 Color: 389

Bin 161: 42 of cap free
Amount of items: 2
Items: 
Size: 10142 Color: 447
Size: 4728 Color: 374

Bin 162: 44 of cap free
Amount of items: 2
Items: 
Size: 9808 Color: 440
Size: 5060 Color: 380

Bin 163: 44 of cap free
Amount of items: 3
Items: 
Size: 10584 Color: 459
Size: 4092 Color: 361
Size: 192 Color: 10

Bin 164: 47 of cap free
Amount of items: 2
Items: 
Size: 9697 Color: 438
Size: 5168 Color: 382

Bin 165: 51 of cap free
Amount of items: 3
Items: 
Size: 9706 Color: 439
Size: 4883 Color: 375
Size: 272 Color: 23

Bin 166: 52 of cap free
Amount of items: 2
Items: 
Size: 8844 Color: 424
Size: 6016 Color: 393

Bin 167: 54 of cap free
Amount of items: 3
Items: 
Size: 10558 Color: 458
Size: 4088 Color: 360
Size: 212 Color: 11

Bin 168: 56 of cap free
Amount of items: 3
Items: 
Size: 10072 Color: 446
Size: 4524 Color: 372
Size: 260 Color: 21

Bin 169: 62 of cap free
Amount of items: 3
Items: 
Size: 10452 Color: 456
Size: 4152 Color: 363
Size: 246 Color: 13

Bin 170: 72 of cap free
Amount of items: 3
Items: 
Size: 8326 Color: 417
Size: 6210 Color: 397
Size: 304 Color: 40

Bin 171: 74 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 416
Size: 6204 Color: 396
Size: 312 Color: 42

Bin 172: 74 of cap free
Amount of items: 2
Items: 
Size: 9948 Color: 443
Size: 4890 Color: 377

Bin 173: 78 of cap free
Amount of items: 3
Items: 
Size: 8327 Color: 418
Size: 6211 Color: 398
Size: 296 Color: 35

Bin 174: 78 of cap free
Amount of items: 3
Items: 
Size: 8329 Color: 419
Size: 6213 Color: 399
Size: 292 Color: 34

Bin 175: 83 of cap free
Amount of items: 2
Items: 
Size: 9944 Color: 442
Size: 4885 Color: 376

Bin 176: 84 of cap free
Amount of items: 3
Items: 
Size: 7528 Color: 413
Size: 6980 Color: 403
Size: 320 Color: 47

Bin 177: 86 of cap free
Amount of items: 3
Items: 
Size: 9051 Color: 427
Size: 5487 Color: 386
Size: 288 Color: 32

Bin 178: 90 of cap free
Amount of items: 3
Items: 
Size: 9053 Color: 428
Size: 5489 Color: 387
Size: 280 Color: 29

Bin 179: 95 of cap free
Amount of items: 15
Items: 
Size: 7457 Color: 404
Size: 664 Color: 147
Size: 642 Color: 145
Size: 640 Color: 143
Size: 640 Color: 142
Size: 634 Color: 141
Size: 630 Color: 139
Size: 626 Color: 138
Size: 624 Color: 137
Size: 384 Color: 79
Size: 384 Color: 78
Size: 384 Color: 77
Size: 372 Color: 74
Size: 368 Color: 73
Size: 368 Color: 72

Bin 180: 102 of cap free
Amount of items: 2
Items: 
Size: 9046 Color: 426
Size: 5764 Color: 392

Bin 181: 103 of cap free
Amount of items: 3
Items: 
Size: 10281 Color: 453
Size: 4272 Color: 365
Size: 256 Color: 17

Bin 182: 108 of cap free
Amount of items: 4
Items: 
Size: 7996 Color: 414
Size: 6168 Color: 394
Size: 320 Color: 46
Size: 320 Color: 45

Bin 183: 112 of cap free
Amount of items: 3
Items: 
Size: 10012 Color: 445
Size: 4524 Color: 371
Size: 264 Color: 22

Bin 184: 119 of cap free
Amount of items: 2
Items: 
Size: 9657 Color: 437
Size: 5136 Color: 381

Bin 185: 123 of cap free
Amount of items: 3
Items: 
Size: 10733 Color: 464
Size: 3920 Color: 354
Size: 136 Color: 6

Bin 186: 140 of cap free
Amount of items: 3
Items: 
Size: 8272 Color: 415
Size: 6184 Color: 395
Size: 316 Color: 43

Bin 187: 155 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 463
Size: 3901 Color: 353
Size: 152 Color: 7

Bin 188: 158 of cap free
Amount of items: 2
Items: 
Size: 9042 Color: 425
Size: 5712 Color: 391

Bin 189: 160 of cap free
Amount of items: 5
Items: 
Size: 8784 Color: 423
Size: 1960 Color: 265
Size: 1884 Color: 260
Size: 1836 Color: 256
Size: 288 Color: 33

Bin 190: 168 of cap free
Amount of items: 2
Items: 
Size: 8520 Color: 422
Size: 6224 Color: 402

Bin 191: 176 of cap free
Amount of items: 13
Items: 
Size: 7458 Color: 405
Size: 778 Color: 160
Size: 770 Color: 158
Size: 768 Color: 156
Size: 728 Color: 155
Size: 704 Color: 153
Size: 702 Color: 152
Size: 696 Color: 151
Size: 672 Color: 148
Size: 368 Color: 71
Size: 368 Color: 70
Size: 364 Color: 69
Size: 360 Color: 68

Bin 192: 186 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 429
Size: 5490 Color: 388

Bin 193: 228 of cap free
Amount of items: 11
Items: 
Size: 7460 Color: 406
Size: 868 Color: 172
Size: 832 Color: 169
Size: 816 Color: 168
Size: 816 Color: 166
Size: 802 Color: 165
Size: 800 Color: 164
Size: 792 Color: 163
Size: 786 Color: 162
Size: 360 Color: 67
Size: 352 Color: 65

Bin 194: 228 of cap free
Amount of items: 2
Items: 
Size: 8468 Color: 421
Size: 6216 Color: 401

Bin 195: 233 of cap free
Amount of items: 2
Items: 
Size: 9655 Color: 436
Size: 5024 Color: 379

Bin 196: 242 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 420
Size: 6214 Color: 400

Bin 197: 270 of cap free
Amount of items: 4
Items: 
Size: 9642 Color: 435
Size: 2424 Color: 297
Size: 2304 Color: 290
Size: 272 Color: 24

Bin 198: 293 of cap free
Amount of items: 10
Items: 
Size: 7461 Color: 407
Size: 976 Color: 181
Size: 976 Color: 180
Size: 928 Color: 177
Size: 928 Color: 176
Size: 896 Color: 175
Size: 876 Color: 174
Size: 876 Color: 173
Size: 352 Color: 63
Size: 350 Color: 62

Bin 199: 9200 of cap free
Amount of items: 12
Items: 
Size: 500 Color: 114
Size: 494 Color: 113
Size: 488 Color: 112
Size: 488 Color: 111
Size: 484 Color: 110
Size: 480 Color: 109
Size: 480 Color: 108
Size: 480 Color: 107
Size: 458 Color: 104
Size: 456 Color: 103
Size: 456 Color: 102
Size: 448 Color: 101

Total size: 2952576
Total free space: 14912

