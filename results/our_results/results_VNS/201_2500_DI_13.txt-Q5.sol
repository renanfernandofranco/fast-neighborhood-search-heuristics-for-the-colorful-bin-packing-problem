Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 3
Size: 1020 Color: 3
Size: 200 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1532 Color: 0
Size: 840 Color: 2
Size: 84 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1730 Color: 2
Size: 402 Color: 3
Size: 324 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 0
Size: 606 Color: 2
Size: 118 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 2
Size: 576 Color: 0
Size: 62 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 2
Size: 554 Color: 4
Size: 76 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1828 Color: 0
Size: 572 Color: 0
Size: 56 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1870 Color: 2
Size: 504 Color: 3
Size: 82 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 2
Size: 476 Color: 3
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1935 Color: 3
Size: 345 Color: 1
Size: 176 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 3
Size: 384 Color: 1
Size: 44 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2043 Color: 0
Size: 317 Color: 3
Size: 96 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 1
Size: 244 Color: 3
Size: 144 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 3
Size: 310 Color: 2
Size: 56 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2105 Color: 3
Size: 293 Color: 4
Size: 58 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2134 Color: 3
Size: 228 Color: 4
Size: 94 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2154 Color: 2
Size: 272 Color: 3
Size: 30 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 3
Size: 184 Color: 4
Size: 100 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 4
Size: 236 Color: 2
Size: 40 Color: 3

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 0
Size: 884 Color: 4
Size: 80 Color: 3

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 1
Size: 604 Color: 2
Size: 56 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1892 Color: 2
Size: 435 Color: 1
Size: 128 Color: 4

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 2
Size: 479 Color: 3
Size: 78 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 1
Size: 436 Color: 4
Size: 40 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1988 Color: 3
Size: 399 Color: 4
Size: 68 Color: 4

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 2164 Color: 4
Size: 243 Color: 0
Size: 48 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2201 Color: 1
Size: 144 Color: 3
Size: 110 Color: 4

Bin 28: 2 of cap free
Amount of items: 4
Items: 
Size: 1574 Color: 2
Size: 562 Color: 1
Size: 270 Color: 3
Size: 48 Color: 1

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1812 Color: 1
Size: 642 Color: 3

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2058 Color: 0
Size: 396 Color: 2

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 2165 Color: 0
Size: 257 Color: 3
Size: 32 Color: 1

Bin 32: 2 of cap free
Amount of items: 4
Items: 
Size: 2190 Color: 1
Size: 254 Color: 4
Size: 8 Color: 4
Size: 2 Color: 1

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1671 Color: 3
Size: 694 Color: 2
Size: 88 Color: 4

Bin 34: 3 of cap free
Amount of items: 3
Items: 
Size: 1959 Color: 1
Size: 430 Color: 4
Size: 64 Color: 2

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 2015 Color: 4
Size: 386 Color: 3
Size: 52 Color: 0

Bin 36: 4 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 0
Size: 684 Color: 2
Size: 490 Color: 3
Size: 48 Color: 2

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 1229 Color: 2
Size: 1021 Color: 3
Size: 202 Color: 1

Bin 38: 4 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 3
Size: 438 Color: 0
Size: 72 Color: 2

Bin 39: 4 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 3
Size: 478 Color: 0

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 3
Size: 244 Color: 4
Size: 20 Color: 0

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1883 Color: 0
Size: 526 Color: 3
Size: 42 Color: 2

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 4
Size: 974 Color: 1
Size: 86 Color: 4

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1644 Color: 3
Size: 806 Color: 1

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1739 Color: 4
Size: 551 Color: 2
Size: 160 Color: 1

Bin 45: 6 of cap free
Amount of items: 4
Items: 
Size: 2100 Color: 1
Size: 334 Color: 2
Size: 8 Color: 3
Size: 8 Color: 2

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1589 Color: 2
Size: 588 Color: 0
Size: 272 Color: 3

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 1
Size: 439 Color: 4
Size: 16 Color: 4

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 1
Size: 415 Color: 4

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 2149 Color: 0
Size: 300 Color: 1

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 2
Size: 772 Color: 1
Size: 50 Color: 0

Bin 51: 8 of cap free
Amount of items: 2
Items: 
Size: 2078 Color: 2
Size: 370 Color: 1

Bin 52: 9 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 0
Size: 805 Color: 2
Size: 152 Color: 0

Bin 53: 11 of cap free
Amount of items: 3
Items: 
Size: 1382 Color: 1
Size: 903 Color: 2
Size: 160 Color: 2

Bin 54: 11 of cap free
Amount of items: 2
Items: 
Size: 1934 Color: 4
Size: 511 Color: 3

Bin 55: 12 of cap free
Amount of items: 3
Items: 
Size: 1396 Color: 4
Size: 984 Color: 3
Size: 64 Color: 3

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1843 Color: 3
Size: 599 Color: 4

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 2077 Color: 4
Size: 364 Color: 1

Bin 58: 19 of cap free
Amount of items: 16
Items: 
Size: 402 Color: 4
Size: 222 Color: 4
Size: 213 Color: 4
Size: 200 Color: 1
Size: 180 Color: 0
Size: 136 Color: 3
Size: 130 Color: 4
Size: 128 Color: 3
Size: 120 Color: 3
Size: 120 Color: 2
Size: 112 Color: 1
Size: 104 Color: 1
Size: 102 Color: 2
Size: 96 Color: 0
Size: 88 Color: 1
Size: 84 Color: 2

Bin 59: 19 of cap free
Amount of items: 2
Items: 
Size: 1782 Color: 4
Size: 655 Color: 0

Bin 60: 24 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 3
Size: 1022 Color: 1
Size: 176 Color: 2

Bin 61: 26 of cap free
Amount of items: 2
Items: 
Size: 1233 Color: 4
Size: 1197 Color: 0

Bin 62: 30 of cap free
Amount of items: 2
Items: 
Size: 1536 Color: 4
Size: 890 Color: 0

Bin 63: 30 of cap free
Amount of items: 2
Items: 
Size: 1688 Color: 4
Size: 738 Color: 0

Bin 64: 41 of cap free
Amount of items: 2
Items: 
Size: 1373 Color: 2
Size: 1042 Color: 0

Bin 65: 47 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 4
Size: 723 Color: 0

Bin 66: 2028 of cap free
Amount of items: 6
Items: 
Size: 84 Color: 3
Size: 80 Color: 4
Size: 80 Color: 3
Size: 64 Color: 4
Size: 64 Color: 1
Size: 56 Color: 2

Total size: 159640
Total free space: 2456

