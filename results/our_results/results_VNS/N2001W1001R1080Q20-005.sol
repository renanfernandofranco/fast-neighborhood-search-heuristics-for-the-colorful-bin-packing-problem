Capicity Bin: 1001
Lower Bound: 898

Bins used: 901
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 9
Size: 329 Color: 13
Size: 317 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 16
Size: 329 Color: 12
Size: 317 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 13
Size: 336 Color: 7
Size: 329 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 16
Size: 337 Color: 6
Size: 327 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 9
Size: 333 Color: 4
Size: 306 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 14
Size: 319 Color: 5
Size: 313 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 288 Color: 7
Size: 278 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 19
Size: 287 Color: 5
Size: 278 Color: 15

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 5
Size: 497 Color: 9

Bin 10: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 4
Size: 490 Color: 10

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 511 Color: 10
Size: 490 Color: 18

Bin 12: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 9
Size: 491 Color: 13

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 12
Size: 482 Color: 8

Bin 14: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 13
Size: 467 Color: 1

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 467 Color: 13

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 4
Size: 467 Color: 3

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 11
Size: 464 Color: 5

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 19
Size: 456 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 13
Size: 258 Color: 1
Size: 188 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 556 Color: 12
Size: 257 Color: 19
Size: 188 Color: 15

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 10
Size: 441 Color: 5

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 17
Size: 421 Color: 5

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 2
Size: 415 Color: 16

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 9
Size: 403 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 6
Size: 207 Color: 16
Size: 199 Color: 11

Bin 26: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 3
Size: 404 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 628 Color: 12
Size: 196 Color: 4
Size: 177 Color: 12

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 3
Size: 372 Color: 19

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 3
Size: 359 Color: 2

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 647 Color: 12
Size: 354 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 0
Size: 192 Color: 2
Size: 152 Color: 16

Bin 32: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 15
Size: 313 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 5
Size: 183 Color: 9
Size: 131 Color: 16

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 11
Size: 183 Color: 10
Size: 127 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 709 Color: 9
Size: 164 Color: 10
Size: 128 Color: 9

Bin 36: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 14
Size: 288 Color: 19

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 19
Size: 281 Color: 3

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 16
Size: 226 Color: 15

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 13
Size: 226 Color: 16

Bin 40: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 12
Size: 221 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 15
Size: 336 Color: 18
Size: 308 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 15
Size: 342 Color: 6
Size: 305 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 9
Size: 329 Color: 18
Size: 310 Color: 13

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 17
Size: 322 Color: 6
Size: 202 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 533 Color: 7
Size: 270 Color: 12
Size: 198 Color: 15

Bin 46: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 18
Size: 383 Color: 2

Bin 47: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 17
Size: 374 Color: 16

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 12
Size: 357 Color: 18

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 2
Size: 193 Color: 4
Size: 159 Color: 0

Bin 50: 0 of cap free
Amount of items: 2
Items: 
Size: 666 Color: 3
Size: 335 Color: 4

Bin 51: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 0
Size: 327 Color: 14

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 721 Color: 15
Size: 280 Color: 9

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 761 Color: 9
Size: 126 Color: 16
Size: 114 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 11
Size: 333 Color: 9
Size: 312 Color: 7

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 19
Size: 290 Color: 8
Size: 274 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 8
Size: 399 Color: 15
Size: 147 Color: 8

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 8
Size: 198 Color: 10
Size: 126 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 0
Size: 212 Color: 8

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 2
Size: 286 Color: 3
Size: 284 Color: 8

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 11
Size: 465 Color: 2

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 16
Size: 459 Color: 1

Bin 62: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 0
Size: 414 Color: 5

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 13
Size: 185 Color: 9
Size: 159 Color: 14

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 8
Size: 340 Color: 19

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 5
Size: 318 Color: 18

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 19
Size: 340 Color: 12
Size: 305 Color: 5

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 6
Size: 290 Color: 14
Size: 273 Color: 18

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 13
Size: 348 Color: 16

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 9
Size: 499 Color: 2

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 596 Color: 10
Size: 405 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 597 Color: 0
Size: 205 Color: 18
Size: 199 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 687 Color: 9
Size: 185 Color: 9
Size: 129 Color: 19

Bin 73: 0 of cap free
Amount of items: 2
Items: 
Size: 506 Color: 18
Size: 495 Color: 16

Bin 74: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 1
Size: 465 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 427 Color: 6
Size: 110 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 590 Color: 9
Size: 208 Color: 2
Size: 203 Color: 9

Bin 77: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 13
Size: 493 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 286 Color: 5
Size: 273 Color: 17

Bin 79: 0 of cap free
Amount of items: 2
Items: 
Size: 676 Color: 1
Size: 325 Color: 8

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 7
Size: 494 Color: 0

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 15
Size: 409 Color: 11

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 15
Size: 229 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 18
Size: 329 Color: 3
Size: 317 Color: 10

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 12
Size: 500 Color: 11

Bin 85: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 8
Size: 493 Color: 5

Bin 86: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 496 Color: 15

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 343 Color: 17
Size: 304 Color: 6

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 10
Size: 410 Color: 18

Bin 89: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 9
Size: 492 Color: 8

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 589 Color: 17
Size: 223 Color: 0
Size: 189 Color: 13

Bin 91: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 6
Size: 216 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 325 Color: 13
Size: 304 Color: 14

Bin 93: 0 of cap free
Amount of items: 2
Items: 
Size: 646 Color: 17
Size: 355 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 16
Size: 266 Color: 9
Size: 266 Color: 9

Bin 95: 0 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 17
Size: 492 Color: 9

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 15
Size: 487 Color: 9

Bin 97: 0 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 10
Size: 486 Color: 9

Bin 98: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 0
Size: 434 Color: 18

Bin 99: 0 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 17
Size: 333 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 433 Color: 14
Size: 104 Color: 17

Bin 101: 0 of cap free
Amount of items: 2
Items: 
Size: 561 Color: 16
Size: 440 Color: 19

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 13
Size: 477 Color: 12

Bin 103: 0 of cap free
Amount of items: 2
Items: 
Size: 524 Color: 9
Size: 477 Color: 13

Bin 104: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 7
Size: 401 Color: 5

Bin 105: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 8
Size: 468 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 4
Size: 399 Color: 0
Size: 203 Color: 4

Bin 107: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 5
Size: 442 Color: 9

Bin 108: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 5
Size: 472 Color: 8

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 432 Color: 16
Size: 135 Color: 12

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 0
Size: 405 Color: 6
Size: 184 Color: 15

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 403 Color: 14
Size: 193 Color: 19

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 7
Size: 394 Color: 11
Size: 198 Color: 9

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 14
Size: 362 Color: 13
Size: 205 Color: 6

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 16
Size: 355 Color: 2
Size: 231 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 362 Color: 11
Size: 238 Color: 16

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 6
Size: 340 Color: 13
Size: 252 Color: 16

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 2
Size: 337 Color: 13
Size: 325 Color: 15

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 17
Size: 333 Color: 16
Size: 304 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 12
Size: 322 Color: 0
Size: 315 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 12
Size: 328 Color: 1
Size: 304 Color: 13

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 319 Color: 2
Size: 312 Color: 11

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 319 Color: 4
Size: 318 Color: 19

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 11
Size: 332 Color: 18
Size: 307 Color: 7

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 328 Color: 2
Size: 319 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 305 Color: 10
Size: 302 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 15
Size: 304 Color: 1
Size: 301 Color: 13

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 18
Size: 304 Color: 0
Size: 302 Color: 7

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 305 Color: 16
Size: 300 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 19
Size: 300 Color: 6
Size: 266 Color: 7

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 17
Size: 287 Color: 16
Size: 274 Color: 15

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 17
Size: 285 Color: 16
Size: 276 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 9
Size: 286 Color: 6
Size: 275 Color: 3

Bin 133: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 16
Size: 500 Color: 13

Bin 134: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 10
Size: 498 Color: 15

Bin 135: 0 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 15
Size: 498 Color: 8

Bin 136: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 19
Size: 497 Color: 18

Bin 137: 0 of cap free
Amount of items: 2
Items: 
Size: 504 Color: 12
Size: 497 Color: 4

Bin 138: 0 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 14
Size: 494 Color: 16

Bin 139: 0 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 2
Size: 493 Color: 5

Bin 140: 0 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 19
Size: 496 Color: 2

Bin 141: 0 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 1
Size: 491 Color: 10

Bin 142: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 6
Size: 489 Color: 15

Bin 143: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 8
Size: 489 Color: 10

Bin 144: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 15
Size: 487 Color: 10

Bin 145: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 9
Size: 487 Color: 1

Bin 146: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 8
Size: 487 Color: 10

Bin 147: 0 of cap free
Amount of items: 2
Items: 
Size: 514 Color: 12
Size: 487 Color: 13

Bin 148: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 8
Size: 484 Color: 6

Bin 149: 0 of cap free
Amount of items: 2
Items: 
Size: 517 Color: 10
Size: 484 Color: 2

Bin 150: 0 of cap free
Amount of items: 2
Items: 
Size: 519 Color: 0
Size: 482 Color: 3

Bin 151: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 15
Size: 481 Color: 1

Bin 152: 0 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 5
Size: 481 Color: 18

Bin 153: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 19
Size: 480 Color: 4

Bin 154: 0 of cap free
Amount of items: 2
Items: 
Size: 521 Color: 4
Size: 480 Color: 0

Bin 155: 0 of cap free
Amount of items: 2
Items: 
Size: 522 Color: 5
Size: 479 Color: 8

Bin 156: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 17
Size: 478 Color: 3

Bin 157: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 17
Size: 478 Color: 4

Bin 158: 0 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 3
Size: 478 Color: 8

Bin 159: 0 of cap free
Amount of items: 2
Items: 
Size: 525 Color: 19
Size: 476 Color: 15

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 18
Size: 475 Color: 8

Bin 161: 0 of cap free
Amount of items: 2
Items: 
Size: 528 Color: 0
Size: 473 Color: 18

Bin 162: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 0
Size: 472 Color: 11

Bin 163: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 7
Size: 472 Color: 11

Bin 164: 0 of cap free
Amount of items: 2
Items: 
Size: 529 Color: 3
Size: 472 Color: 2

Bin 165: 0 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 13
Size: 470 Color: 1

Bin 166: 0 of cap free
Amount of items: 2
Items: 
Size: 532 Color: 18
Size: 469 Color: 19

Bin 167: 0 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 13
Size: 468 Color: 4

Bin 168: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 10
Size: 467 Color: 16

Bin 169: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 18
Size: 467 Color: 14

Bin 170: 0 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 10
Size: 467 Color: 2

Bin 171: 0 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 19
Size: 471 Color: 8

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 13
Size: 264 Color: 12
Size: 203 Color: 18

Bin 173: 0 of cap free
Amount of items: 2
Items: 
Size: 535 Color: 8
Size: 466 Color: 11

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 15
Size: 463 Color: 2

Bin 175: 0 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 12
Size: 463 Color: 7

Bin 176: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 10
Size: 462 Color: 14

Bin 177: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 17
Size: 462 Color: 10

Bin 178: 0 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 14
Size: 462 Color: 3

Bin 179: 0 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 0
Size: 460 Color: 7

Bin 180: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 13
Size: 459 Color: 4

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 7
Size: 262 Color: 9
Size: 197 Color: 16

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 16
Size: 262 Color: 19
Size: 197 Color: 16

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 542 Color: 11
Size: 265 Color: 12
Size: 194 Color: 1

Bin 184: 0 of cap free
Amount of items: 2
Items: 
Size: 542 Color: 9
Size: 459 Color: 3

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 545 Color: 17
Size: 263 Color: 10
Size: 193 Color: 6

Bin 186: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 10
Size: 456 Color: 4

Bin 187: 0 of cap free
Amount of items: 2
Items: 
Size: 545 Color: 12
Size: 456 Color: 15

Bin 188: 0 of cap free
Amount of items: 2
Items: 
Size: 546 Color: 13
Size: 455 Color: 0

Bin 189: 0 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 3
Size: 454 Color: 13

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 548 Color: 8
Size: 261 Color: 9
Size: 192 Color: 11

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 547 Color: 13
Size: 263 Color: 3
Size: 191 Color: 18

Bin 192: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 2
Size: 450 Color: 0

Bin 193: 0 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 7
Size: 452 Color: 0

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 17
Size: 451 Color: 8

Bin 195: 0 of cap free
Amount of items: 2
Items: 
Size: 550 Color: 12
Size: 451 Color: 10

Bin 196: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 5
Size: 450 Color: 10

Bin 197: 0 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 14
Size: 449 Color: 8

Bin 198: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 10
Size: 447 Color: 4

Bin 199: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 10
Size: 447 Color: 11

Bin 200: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 1
Size: 447 Color: 15

Bin 201: 0 of cap free
Amount of items: 2
Items: 
Size: 554 Color: 7
Size: 447 Color: 0

Bin 202: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 0
Size: 446 Color: 5

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 10
Size: 446 Color: 15

Bin 204: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 446 Color: 7

Bin 205: 0 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 17
Size: 446 Color: 13

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 3

Bin 207: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 0
Size: 444 Color: 16

Bin 208: 0 of cap free
Amount of items: 2
Items: 
Size: 557 Color: 10
Size: 444 Color: 12

Bin 209: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 8
Size: 443 Color: 11

Bin 210: 0 of cap free
Amount of items: 2
Items: 
Size: 558 Color: 17
Size: 443 Color: 7

Bin 211: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 18
Size: 442 Color: 5

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 442 Color: 14

Bin 213: 0 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 6
Size: 442 Color: 18

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 561 Color: 19
Size: 253 Color: 5
Size: 187 Color: 18

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 12
Size: 439 Color: 10

Bin 216: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 19
Size: 439 Color: 2

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 2
Size: 439 Color: 4

Bin 218: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 15
Size: 439 Color: 0

Bin 219: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 16
Size: 438 Color: 13

Bin 220: 0 of cap free
Amount of items: 2
Items: 
Size: 563 Color: 17
Size: 438 Color: 13

Bin 221: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 6
Size: 436 Color: 3

Bin 222: 0 of cap free
Amount of items: 2
Items: 
Size: 565 Color: 17
Size: 436 Color: 4

Bin 223: 0 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 8
Size: 434 Color: 13

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 14
Size: 432 Color: 0

Bin 225: 0 of cap free
Amount of items: 2
Items: 
Size: 569 Color: 18
Size: 432 Color: 13

Bin 226: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 6
Size: 431 Color: 7

Bin 227: 0 of cap free
Amount of items: 2
Items: 
Size: 570 Color: 10
Size: 431 Color: 14

Bin 228: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 11
Size: 429 Color: 3

Bin 229: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 9
Size: 429 Color: 19

Bin 230: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 4
Size: 429 Color: 2

Bin 231: 0 of cap free
Amount of items: 2
Items: 
Size: 574 Color: 5
Size: 427 Color: 19

Bin 232: 0 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 15
Size: 426 Color: 0

Bin 233: 0 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 2
Size: 428 Color: 7

Bin 234: 0 of cap free
Amount of items: 2
Items: 
Size: 576 Color: 12
Size: 425 Color: 19

Bin 235: 0 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 18
Size: 424 Color: 16

Bin 236: 0 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 13
Size: 423 Color: 2

Bin 237: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 11
Size: 422 Color: 19

Bin 238: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 13
Size: 422 Color: 10

Bin 239: 0 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 4
Size: 422 Color: 7

Bin 240: 0 of cap free
Amount of items: 2
Items: 
Size: 580 Color: 3
Size: 421 Color: 5

Bin 241: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 19
Size: 420 Color: 10

Bin 242: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 9
Size: 420 Color: 13

Bin 243: 0 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 9
Size: 420 Color: 16

Bin 244: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 19
Size: 419 Color: 14

Bin 245: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 10
Size: 418 Color: 8

Bin 246: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 15
Size: 417 Color: 1

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 10
Size: 417 Color: 3

Bin 248: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 11
Size: 417 Color: 15

Bin 249: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 6
Size: 416 Color: 7

Bin 250: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 7
Size: 416 Color: 17

Bin 251: 0 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 14
Size: 416 Color: 15

Bin 252: 0 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 7
Size: 415 Color: 19

Bin 253: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 8
Size: 414 Color: 1

Bin 254: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 15
Size: 413 Color: 13

Bin 255: 0 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 14
Size: 413 Color: 8

Bin 256: 0 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 19
Size: 414 Color: 14

Bin 257: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 4
Size: 411 Color: 10

Bin 258: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 4
Size: 411 Color: 16

Bin 259: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 9
Size: 410 Color: 18

Bin 260: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 12

Bin 261: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 19
Size: 411 Color: 7

Bin 262: 0 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 3
Size: 410 Color: 5

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 592 Color: 2
Size: 219 Color: 3
Size: 190 Color: 16

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 0
Size: 408 Color: 18

Bin 265: 0 of cap free
Amount of items: 2
Items: 
Size: 593 Color: 1
Size: 408 Color: 8

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 594 Color: 11
Size: 205 Color: 12
Size: 202 Color: 13

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 14
Size: 218 Color: 10
Size: 188 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 5
Size: 218 Color: 8
Size: 188 Color: 16

Bin 269: 0 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 15
Size: 406 Color: 2

Bin 270: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 11
Size: 404 Color: 16

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 598 Color: 15
Size: 205 Color: 0
Size: 198 Color: 6

Bin 272: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 3
Size: 403 Color: 10

Bin 273: 0 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 16
Size: 403 Color: 8

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 14
Size: 402 Color: 9

Bin 275: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 7
Size: 402 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 599 Color: 7
Size: 218 Color: 5
Size: 184 Color: 6

Bin 277: 0 of cap free
Amount of items: 2
Items: 
Size: 599 Color: 4
Size: 402 Color: 2

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 600 Color: 11
Size: 202 Color: 0
Size: 199 Color: 16

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 595 Color: 8
Size: 219 Color: 14
Size: 187 Color: 0

Bin 280: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 7

Bin 281: 0 of cap free
Amount of items: 2
Items: 
Size: 602 Color: 1
Size: 399 Color: 5

Bin 282: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 2
Size: 397 Color: 15

Bin 283: 0 of cap free
Amount of items: 2
Items: 
Size: 604 Color: 15
Size: 397 Color: 17

Bin 284: 0 of cap free
Amount of items: 2
Items: 
Size: 605 Color: 12
Size: 396 Color: 8

Bin 285: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 14
Size: 395 Color: 16

Bin 286: 0 of cap free
Amount of items: 2
Items: 
Size: 606 Color: 9
Size: 395 Color: 8

Bin 287: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 4
Size: 394 Color: 1

Bin 288: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 16
Size: 394 Color: 15

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 17
Size: 394 Color: 9

Bin 290: 0 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 3
Size: 393 Color: 2

Bin 291: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 12
Size: 392 Color: 1

Bin 292: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 10
Size: 392 Color: 12

Bin 293: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 4
Size: 392 Color: 9

Bin 294: 0 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 19
Size: 391 Color: 0

Bin 295: 0 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 0
Size: 390 Color: 7

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 612 Color: 5
Size: 389 Color: 11

Bin 297: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 7
Size: 388 Color: 3

Bin 298: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 13
Size: 388 Color: 4

Bin 299: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 3
Size: 387 Color: 12

Bin 300: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 12
Size: 387 Color: 0

Bin 301: 0 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 15
Size: 387 Color: 12

Bin 302: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 4

Bin 303: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 5
Size: 386 Color: 14

Bin 304: 0 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 6
Size: 385 Color: 12

Bin 305: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 7
Size: 384 Color: 18

Bin 306: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 18
Size: 384 Color: 19

Bin 307: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 14
Size: 384 Color: 3

Bin 308: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 4
Size: 384 Color: 7

Bin 309: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 7
Size: 383 Color: 19

Bin 310: 0 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 15
Size: 383 Color: 5

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 618 Color: 9
Size: 199 Color: 2
Size: 184 Color: 18

Bin 312: 0 of cap free
Amount of items: 2
Items: 
Size: 617 Color: 5
Size: 384 Color: 15

Bin 313: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 10
Size: 379 Color: 16

Bin 314: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 3
Size: 379 Color: 9

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 622 Color: 1
Size: 192 Color: 0
Size: 187 Color: 18

Bin 316: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 6
Size: 378 Color: 17

Bin 317: 0 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 10
Size: 378 Color: 13

Bin 318: 0 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 5
Size: 379 Color: 9

Bin 319: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 16
Size: 376 Color: 9

Bin 320: 0 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 376 Color: 3

Bin 321: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 16
Size: 375 Color: 15

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 8
Size: 375 Color: 18

Bin 323: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 10
Size: 375 Color: 6

Bin 324: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 5
Size: 377 Color: 18

Bin 325: 0 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 9
Size: 375 Color: 16

Bin 326: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 16
Size: 374 Color: 14

Bin 327: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 10
Size: 374 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 627 Color: 15
Size: 190 Color: 17
Size: 184 Color: 0

Bin 329: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 10
Size: 374 Color: 9

Bin 330: 0 of cap free
Amount of items: 2
Items: 
Size: 627 Color: 3
Size: 374 Color: 5

Bin 331: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 19
Size: 373 Color: 7

Bin 332: 0 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 12
Size: 373 Color: 16

Bin 333: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 12
Size: 372 Color: 2

Bin 334: 0 of cap free
Amount of items: 2
Items: 
Size: 629 Color: 18
Size: 372 Color: 12

Bin 335: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 18
Size: 371 Color: 13

Bin 336: 0 of cap free
Amount of items: 2
Items: 
Size: 630 Color: 17
Size: 371 Color: 5

Bin 337: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 18
Size: 370 Color: 8

Bin 338: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 9
Size: 370 Color: 4

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 631 Color: 7
Size: 193 Color: 5
Size: 177 Color: 16

Bin 340: 0 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 15
Size: 369 Color: 7

Bin 341: 0 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 9
Size: 368 Color: 15

Bin 342: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 11
Size: 367 Color: 13

Bin 343: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 6
Size: 366 Color: 2

Bin 344: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 17
Size: 366 Color: 6

Bin 345: 0 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 9
Size: 366 Color: 17

Bin 346: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 0
Size: 365 Color: 10

Bin 347: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 10
Size: 363 Color: 19

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 16
Size: 363 Color: 6

Bin 349: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 8
Size: 363 Color: 14

Bin 350: 0 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 9
Size: 363 Color: 4

Bin 351: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 13
Size: 362 Color: 4

Bin 352: 0 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 11
Size: 362 Color: 3

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 6
Size: 187 Color: 19
Size: 174 Color: 0

Bin 354: 0 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 1
Size: 361 Color: 18

Bin 355: 0 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 11
Size: 360 Color: 9

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 641 Color: 9
Size: 181 Color: 18
Size: 179 Color: 13

Bin 357: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 15
Size: 359 Color: 17

Bin 358: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 6

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 8
Size: 358 Color: 7

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 644 Color: 5
Size: 188 Color: 18
Size: 169 Color: 17

Bin 361: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 14
Size: 353 Color: 13

Bin 362: 0 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 11
Size: 353 Color: 19

Bin 363: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 7

Bin 364: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 10
Size: 352 Color: 8

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 649 Color: 8
Size: 190 Color: 16
Size: 162 Color: 13

Bin 366: 0 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 13
Size: 351 Color: 10

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 650 Color: 14
Size: 192 Color: 14
Size: 159 Color: 5

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 13
Size: 185 Color: 12
Size: 165 Color: 7

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 16
Size: 180 Color: 18
Size: 170 Color: 15

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 651 Color: 2
Size: 175 Color: 16
Size: 175 Color: 5

Bin 371: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 6
Size: 350 Color: 9

Bin 372: 0 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 1
Size: 350 Color: 8

Bin 373: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 15
Size: 349 Color: 1

Bin 374: 0 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 18
Size: 349 Color: 16

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 653 Color: 14
Size: 180 Color: 7
Size: 168 Color: 5

Bin 376: 0 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 17
Size: 348 Color: 8

Bin 377: 0 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 9
Size: 347 Color: 6

Bin 378: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 17
Size: 345 Color: 8

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 657 Color: 13
Size: 177 Color: 5
Size: 167 Color: 1

Bin 380: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 13
Size: 343 Color: 15

Bin 381: 0 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 19
Size: 342 Color: 13

Bin 382: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 6
Size: 341 Color: 12

Bin 383: 0 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 14
Size: 341 Color: 9

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 660 Color: 17
Size: 182 Color: 14
Size: 159 Color: 5

Bin 385: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 10
Size: 340 Color: 14

Bin 386: 0 of cap free
Amount of items: 2
Items: 
Size: 661 Color: 4
Size: 340 Color: 3

Bin 387: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 7
Size: 339 Color: 4

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 663 Color: 15
Size: 174 Color: 1
Size: 164 Color: 14

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 0
Size: 176 Color: 11
Size: 161 Color: 6

Bin 390: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 3
Size: 337 Color: 15

Bin 391: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 19
Size: 337 Color: 6

Bin 392: 0 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 1
Size: 337 Color: 0

Bin 393: 0 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 336 Color: 19

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 666 Color: 7
Size: 174 Color: 6
Size: 161 Color: 6

Bin 395: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 16
Size: 334 Color: 2

Bin 396: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 12
Size: 334 Color: 5

Bin 397: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 13
Size: 334 Color: 16

Bin 398: 0 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 9
Size: 334 Color: 3

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 1
Size: 169 Color: 17
Size: 164 Color: 5

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 668 Color: 15
Size: 186 Color: 14
Size: 147 Color: 14

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 669 Color: 17
Size: 166 Color: 5
Size: 166 Color: 1

Bin 402: 0 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 15
Size: 332 Color: 1

Bin 403: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 18
Size: 331 Color: 15

Bin 404: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 7
Size: 331 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 3
Size: 177 Color: 17
Size: 154 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 17
Size: 183 Color: 7
Size: 148 Color: 4

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 3
Size: 189 Color: 2
Size: 142 Color: 0

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 15
Size: 331 Color: 9

Bin 409: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 3
Size: 330 Color: 10

Bin 410: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 7
Size: 330 Color: 18

Bin 411: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 6
Size: 330 Color: 3

Bin 412: 0 of cap free
Amount of items: 2
Items: 
Size: 671 Color: 1
Size: 330 Color: 10

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 671 Color: 18
Size: 185 Color: 3
Size: 145 Color: 11

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 8
Size: 180 Color: 18
Size: 149 Color: 16

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 672 Color: 15
Size: 185 Color: 2
Size: 144 Color: 1

Bin 416: 0 of cap free
Amount of items: 2
Items: 
Size: 672 Color: 19
Size: 329 Color: 10

Bin 417: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 3
Size: 328 Color: 15

Bin 418: 0 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 0
Size: 328 Color: 9

Bin 419: 0 of cap free
Amount of items: 2
Items: 
Size: 674 Color: 8
Size: 327 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 674 Color: 7
Size: 185 Color: 3
Size: 142 Color: 16

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 675 Color: 17
Size: 170 Color: 2
Size: 156 Color: 4

Bin 422: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 14
Size: 326 Color: 11

Bin 423: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 8
Size: 326 Color: 6

Bin 424: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 11
Size: 326 Color: 13

Bin 425: 0 of cap free
Amount of items: 2
Items: 
Size: 675 Color: 15
Size: 326 Color: 12

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 676 Color: 16
Size: 205 Color: 3
Size: 120 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 0
Size: 170 Color: 2
Size: 154 Color: 7

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 5
Size: 168 Color: 0
Size: 156 Color: 14

Bin 429: 0 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 9
Size: 324 Color: 18

Bin 430: 0 of cap free
Amount of items: 2
Items: 
Size: 678 Color: 13
Size: 323 Color: 9

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 9
Size: 201 Color: 4
Size: 122 Color: 17

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 679 Color: 18
Size: 198 Color: 0
Size: 124 Color: 6

Bin 433: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 3
Size: 322 Color: 6

Bin 434: 0 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 2
Size: 322 Color: 12

Bin 435: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 321 Color: 6

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 7
Size: 321 Color: 10

Bin 437: 0 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 2
Size: 321 Color: 7

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 681 Color: 14
Size: 172 Color: 0
Size: 148 Color: 13

Bin 439: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 3
Size: 320 Color: 4

Bin 440: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 7
Size: 320 Color: 10

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 9
Size: 320 Color: 7

Bin 442: 0 of cap free
Amount of items: 2
Items: 
Size: 683 Color: 16
Size: 318 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 7
Size: 176 Color: 2
Size: 141 Color: 1

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 14
Size: 161 Color: 0
Size: 156 Color: 5

Bin 445: 0 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 6
Size: 317 Color: 4

Bin 446: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 14
Size: 316 Color: 1

Bin 447: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 5
Size: 316 Color: 10

Bin 448: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 7
Size: 316 Color: 13

Bin 449: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 18
Size: 316 Color: 6

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 13
Size: 195 Color: 10
Size: 120 Color: 7

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 14
Size: 184 Color: 3
Size: 131 Color: 19

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 686 Color: 19
Size: 184 Color: 2
Size: 131 Color: 18

Bin 453: 0 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 18
Size: 314 Color: 19

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 688 Color: 11
Size: 177 Color: 10
Size: 136 Color: 19

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 1
Size: 187 Color: 4
Size: 125 Color: 3

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 18
Size: 171 Color: 11
Size: 141 Color: 2

Bin 457: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 6
Size: 311 Color: 16

Bin 458: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 17
Size: 311 Color: 8

Bin 459: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 16
Size: 310 Color: 5

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 15
Size: 174 Color: 12
Size: 136 Color: 11

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 691 Color: 11
Size: 168 Color: 12
Size: 142 Color: 2

Bin 462: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 3
Size: 310 Color: 11

Bin 463: 0 of cap free
Amount of items: 2
Items: 
Size: 691 Color: 17
Size: 310 Color: 9

Bin 464: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 16
Size: 309 Color: 19

Bin 465: 0 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 19
Size: 309 Color: 12

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 13
Size: 162 Color: 6
Size: 147 Color: 16

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 692 Color: 19
Size: 180 Color: 3
Size: 129 Color: 7

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 16
Size: 173 Color: 2
Size: 135 Color: 14

Bin 469: 0 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 11
Size: 308 Color: 5

Bin 470: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 8
Size: 307 Color: 0

Bin 471: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 9
Size: 307 Color: 17

Bin 472: 0 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 7
Size: 307 Color: 19

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 12
Size: 155 Color: 0
Size: 152 Color: 12

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 695 Color: 8
Size: 165 Color: 17
Size: 141 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 697 Color: 8
Size: 179 Color: 14
Size: 125 Color: 2

Bin 476: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 12
Size: 303 Color: 10

Bin 477: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 303 Color: 5

Bin 478: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 14
Size: 303 Color: 15

Bin 479: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 0
Size: 303 Color: 10

Bin 480: 0 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 9
Size: 302 Color: 12

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 700 Color: 8
Size: 159 Color: 18
Size: 142 Color: 10

Bin 482: 0 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 15
Size: 300 Color: 16

Bin 483: 0 of cap free
Amount of items: 2
Items: 
Size: 702 Color: 15
Size: 299 Color: 4

Bin 484: 0 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 17
Size: 298 Color: 2

Bin 485: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 10
Size: 297 Color: 1

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 704 Color: 18
Size: 162 Color: 15
Size: 135 Color: 8

Bin 487: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 18

Bin 488: 0 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 0
Size: 297 Color: 19

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 5
Size: 172 Color: 12
Size: 124 Color: 12

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 11
Size: 174 Color: 10
Size: 122 Color: 17

Bin 491: 0 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 16
Size: 296 Color: 9

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 706 Color: 11
Size: 166 Color: 16
Size: 129 Color: 8

Bin 493: 0 of cap free
Amount of items: 2
Items: 
Size: 706 Color: 0
Size: 295 Color: 3

Bin 494: 0 of cap free
Amount of items: 2
Items: 
Size: 707 Color: 5
Size: 294 Color: 4

Bin 495: 0 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 18
Size: 292 Color: 7

Bin 496: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 14
Size: 291 Color: 2

Bin 497: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 12
Size: 291 Color: 19

Bin 498: 0 of cap free
Amount of items: 2
Items: 
Size: 710 Color: 11
Size: 291 Color: 0

Bin 499: 0 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 18
Size: 289 Color: 11

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 712 Color: 13
Size: 158 Color: 12
Size: 131 Color: 10

Bin 501: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 5
Size: 288 Color: 13

Bin 502: 0 of cap free
Amount of items: 2
Items: 
Size: 713 Color: 18
Size: 288 Color: 6

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 713 Color: 7
Size: 144 Color: 13
Size: 144 Color: 8

Bin 504: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 12
Size: 287 Color: 13

Bin 505: 0 of cap free
Amount of items: 2
Items: 
Size: 714 Color: 5
Size: 287 Color: 16

Bin 506: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 5
Size: 286 Color: 7

Bin 507: 0 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 18
Size: 286 Color: 2

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 6
Size: 146 Color: 0
Size: 140 Color: 15

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 715 Color: 11
Size: 160 Color: 17
Size: 126 Color: 18

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 716 Color: 9
Size: 147 Color: 8
Size: 138 Color: 11

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 4
Size: 156 Color: 14
Size: 128 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 12
Size: 149 Color: 7
Size: 135 Color: 16

Bin 513: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 15
Size: 284 Color: 3

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 15
Size: 148 Color: 14
Size: 136 Color: 11

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 717 Color: 5
Size: 155 Color: 11
Size: 129 Color: 0

Bin 516: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 18
Size: 283 Color: 11

Bin 517: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 11
Size: 283 Color: 2

Bin 518: 0 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 2
Size: 283 Color: 3

Bin 519: 0 of cap free
Amount of items: 2
Items: 
Size: 719 Color: 4
Size: 282 Color: 11

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 3
Size: 150 Color: 2
Size: 132 Color: 5

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 18
Size: 157 Color: 1
Size: 125 Color: 13

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 719 Color: 1
Size: 143 Color: 13
Size: 139 Color: 15

Bin 523: 0 of cap free
Amount of items: 2
Items: 
Size: 720 Color: 4
Size: 281 Color: 11

Bin 524: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 2
Size: 279 Color: 5

Bin 525: 0 of cap free
Amount of items: 2
Items: 
Size: 722 Color: 18
Size: 279 Color: 7

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 722 Color: 0
Size: 148 Color: 14
Size: 131 Color: 8

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 5
Size: 140 Color: 6
Size: 138 Color: 16

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 723 Color: 14
Size: 154 Color: 9
Size: 124 Color: 13

Bin 529: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 13
Size: 278 Color: 18

Bin 530: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 18
Size: 278 Color: 6

Bin 531: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 0
Size: 277 Color: 2

Bin 532: 0 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 7
Size: 277 Color: 10

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 17
Size: 144 Color: 14
Size: 132 Color: 18

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 725 Color: 8
Size: 154 Color: 11
Size: 122 Color: 10

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 726 Color: 0
Size: 154 Color: 9
Size: 121 Color: 7

Bin 536: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 4
Size: 275 Color: 8

Bin 537: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 7
Size: 275 Color: 4

Bin 538: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 8
Size: 274 Color: 10

Bin 539: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 0
Size: 275 Color: 11

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 727 Color: 15
Size: 148 Color: 18
Size: 126 Color: 18

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 10
Size: 151 Color: 13
Size: 122 Color: 5

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 728 Color: 16
Size: 151 Color: 9
Size: 122 Color: 18

Bin 543: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 4
Size: 272 Color: 7

Bin 544: 0 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 16
Size: 272 Color: 15

Bin 545: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 0
Size: 271 Color: 4

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 3
Size: 137 Color: 3
Size: 133 Color: 18

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 12
Size: 136 Color: 3
Size: 134 Color: 3

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 731 Color: 3
Size: 139 Color: 17
Size: 131 Color: 8

Bin 549: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 9
Size: 269 Color: 4

Bin 550: 0 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 7
Size: 269 Color: 6

Bin 551: 0 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 19
Size: 268 Color: 13

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 6
Size: 140 Color: 1
Size: 128 Color: 16

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 733 Color: 5
Size: 134 Color: 12
Size: 134 Color: 1

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 734 Color: 17
Size: 151 Color: 9
Size: 116 Color: 1

Bin 555: 0 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 19
Size: 266 Color: 5

Bin 556: 0 of cap free
Amount of items: 2
Items: 
Size: 736 Color: 17
Size: 265 Color: 12

Bin 557: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 6
Size: 264 Color: 15

Bin 558: 0 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 3
Size: 264 Color: 13

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 1
Size: 139 Color: 17
Size: 125 Color: 16

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 738 Color: 13
Size: 140 Color: 16
Size: 123 Color: 7

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 739 Color: 18
Size: 137 Color: 11
Size: 125 Color: 7

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 740 Color: 9
Size: 140 Color: 17
Size: 121 Color: 15

Bin 563: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 15
Size: 259 Color: 2

Bin 564: 0 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 3
Size: 259 Color: 9

Bin 565: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 9
Size: 261 Color: 17

Bin 566: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 2
Size: 258 Color: 13

Bin 567: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 11
Size: 258 Color: 14

Bin 568: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 16
Size: 258 Color: 19

Bin 569: 0 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 0
Size: 258 Color: 11

Bin 570: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 11
Size: 257 Color: 5

Bin 571: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 7
Size: 257 Color: 15

Bin 572: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 5
Size: 257 Color: 3

Bin 573: 0 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 8
Size: 257 Color: 10

Bin 574: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 15
Size: 256 Color: 10

Bin 575: 0 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 5
Size: 256 Color: 10

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 743 Color: 9
Size: 140 Color: 9
Size: 118 Color: 6

Bin 577: 0 of cap free
Amount of items: 2
Items: 
Size: 747 Color: 0
Size: 254 Color: 19

Bin 578: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 17
Size: 253 Color: 16

Bin 579: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 15
Size: 253 Color: 14

Bin 580: 0 of cap free
Amount of items: 2
Items: 
Size: 748 Color: 15
Size: 253 Color: 7

Bin 581: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 15
Size: 252 Color: 0

Bin 582: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 5
Size: 252 Color: 7

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 749 Color: 3
Size: 138 Color: 9
Size: 114 Color: 0

Bin 584: 0 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 0
Size: 252 Color: 9

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 750 Color: 11
Size: 137 Color: 10
Size: 114 Color: 1

Bin 586: 0 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 0
Size: 251 Color: 18

Bin 587: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 19
Size: 250 Color: 15

Bin 588: 0 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 2
Size: 250 Color: 14

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 18
Size: 132 Color: 8
Size: 117 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 14
Size: 133 Color: 3
Size: 116 Color: 10

Bin 591: 0 of cap free
Amount of items: 2
Items: 
Size: 752 Color: 5
Size: 249 Color: 6

Bin 592: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 16
Size: 248 Color: 8

Bin 593: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 14
Size: 248 Color: 10

Bin 594: 0 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 12
Size: 248 Color: 11

Bin 595: 0 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 10
Size: 247 Color: 4

Bin 596: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 6
Size: 246 Color: 13

Bin 597: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 6
Size: 246 Color: 9

Bin 598: 0 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 5
Size: 245 Color: 2

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 757 Color: 12
Size: 131 Color: 9
Size: 113 Color: 8

Bin 600: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 8
Size: 242 Color: 14

Bin 601: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 0
Size: 242 Color: 12

Bin 602: 0 of cap free
Amount of items: 2
Items: 
Size: 759 Color: 17
Size: 242 Color: 19

Bin 603: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 15
Size: 240 Color: 0

Bin 604: 0 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 10
Size: 240 Color: 14

Bin 605: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 14
Size: 239 Color: 12

Bin 606: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 4
Size: 239 Color: 7

Bin 607: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 7
Size: 239 Color: 17

Bin 608: 0 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 14
Size: 238 Color: 11

Bin 609: 0 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 2
Size: 237 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 10
Size: 119 Color: 3
Size: 117 Color: 6

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 10
Size: 122 Color: 5
Size: 114 Color: 1

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 2
Size: 126 Color: 4
Size: 110 Color: 8

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 10
Size: 124 Color: 2
Size: 112 Color: 1

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 765 Color: 2
Size: 120 Color: 7
Size: 116 Color: 10

Bin 615: 0 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 6
Size: 236 Color: 17

Bin 616: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 17
Size: 235 Color: 1

Bin 617: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 11
Size: 235 Color: 10

Bin 618: 0 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 19
Size: 235 Color: 4

Bin 619: 0 of cap free
Amount of items: 2
Items: 
Size: 767 Color: 6
Size: 234 Color: 11

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 767 Color: 17
Size: 125 Color: 9
Size: 109 Color: 15

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 768 Color: 18
Size: 117 Color: 11
Size: 116 Color: 16

Bin 622: 0 of cap free
Amount of items: 2
Items: 
Size: 768 Color: 3
Size: 233 Color: 9

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 769 Color: 4
Size: 120 Color: 8
Size: 112 Color: 3

Bin 624: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 14
Size: 231 Color: 19

Bin 625: 0 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 10
Size: 231 Color: 14

Bin 626: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 1
Size: 230 Color: 2

Bin 627: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 2
Size: 230 Color: 4

Bin 628: 0 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 19
Size: 230 Color: 15

Bin 629: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 1
Size: 229 Color: 15

Bin 630: 0 of cap free
Amount of items: 2
Items: 
Size: 772 Color: 4
Size: 229 Color: 12

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 1
Size: 114 Color: 13
Size: 113 Color: 3

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 774 Color: 18
Size: 119 Color: 8
Size: 108 Color: 7

Bin 633: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 0
Size: 225 Color: 5

Bin 634: 0 of cap free
Amount of items: 2
Items: 
Size: 776 Color: 15
Size: 225 Color: 16

Bin 635: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 9
Size: 224 Color: 4

Bin 636: 0 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 19
Size: 222 Color: 16

Bin 637: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 8
Size: 221 Color: 14

Bin 638: 0 of cap free
Amount of items: 2
Items: 
Size: 780 Color: 17
Size: 221 Color: 5

Bin 639: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 7
Size: 220 Color: 6

Bin 640: 0 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 11
Size: 220 Color: 19

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 781 Color: 17
Size: 116 Color: 1
Size: 104 Color: 5

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 782 Color: 17
Size: 117 Color: 16
Size: 102 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 783 Color: 18
Size: 115 Color: 9
Size: 103 Color: 4

Bin 644: 0 of cap free
Amount of items: 2
Items: 
Size: 783 Color: 4
Size: 218 Color: 0

Bin 645: 0 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 19
Size: 217 Color: 11

Bin 646: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 11
Size: 216 Color: 7

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 785 Color: 11
Size: 108 Color: 17
Size: 108 Color: 6

Bin 648: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 6
Size: 216 Color: 2

Bin 649: 0 of cap free
Amount of items: 2
Items: 
Size: 785 Color: 19
Size: 216 Color: 15

Bin 650: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 4
Size: 215 Color: 2

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 786 Color: 8
Size: 113 Color: 6
Size: 102 Color: 6

Bin 652: 0 of cap free
Amount of items: 2
Items: 
Size: 786 Color: 8
Size: 215 Color: 0

Bin 653: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 18
Size: 214 Color: 7

Bin 654: 0 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 4
Size: 214 Color: 14

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 16
Size: 110 Color: 13
Size: 104 Color: 4

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 787 Color: 7
Size: 108 Color: 16
Size: 106 Color: 6

Bin 657: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 9
Size: 213 Color: 13

Bin 658: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 13
Size: 213 Color: 11

Bin 659: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 5
Size: 213 Color: 16

Bin 660: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 19
Size: 213 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 14
Size: 113 Color: 10
Size: 100 Color: 11

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 789 Color: 6
Size: 111 Color: 9
Size: 101 Color: 0

Bin 663: 0 of cap free
Amount of items: 2
Items: 
Size: 789 Color: 11
Size: 212 Color: 5

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 19
Size: 108 Color: 10
Size: 103 Color: 7

Bin 665: 0 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 3
Size: 211 Color: 13

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 12
Size: 111 Color: 10
Size: 100 Color: 19

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 790 Color: 11
Size: 108 Color: 18
Size: 103 Color: 11

Bin 668: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 8
Size: 210 Color: 2

Bin 669: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 5
Size: 210 Color: 6

Bin 670: 0 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 19
Size: 210 Color: 0

Bin 671: 0 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 3
Size: 209 Color: 19

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 5
Size: 105 Color: 16
Size: 104 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 792 Color: 1
Size: 106 Color: 13
Size: 103 Color: 13

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 793 Color: 19
Size: 105 Color: 2
Size: 103 Color: 16

Bin 675: 0 of cap free
Amount of items: 2
Items: 
Size: 794 Color: 19
Size: 207 Color: 3

Bin 676: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 8
Size: 206 Color: 13

Bin 677: 0 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 12
Size: 206 Color: 9

Bin 678: 0 of cap free
Amount of items: 2
Items: 
Size: 796 Color: 15
Size: 205 Color: 1

Bin 679: 0 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 15
Size: 204 Color: 1

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 797 Color: 18
Size: 102 Color: 17
Size: 102 Color: 15

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 798 Color: 3
Size: 102 Color: 18
Size: 101 Color: 4

Bin 682: 0 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 9
Size: 203 Color: 2

Bin 683: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 16
Size: 201 Color: 11

Bin 684: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 10
Size: 201 Color: 16

Bin 685: 0 of cap free
Amount of items: 2
Items: 
Size: 800 Color: 5
Size: 201 Color: 19

Bin 686: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 8
Size: 482 Color: 9

Bin 687: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 19
Size: 474 Color: 11

Bin 688: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 7
Size: 474 Color: 16

Bin 689: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 5
Size: 474 Color: 0

Bin 690: 1 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 11
Size: 469 Color: 16

Bin 691: 1 of cap free
Amount of items: 3
Items: 
Size: 534 Color: 1
Size: 263 Color: 12
Size: 203 Color: 11

Bin 692: 1 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 5
Size: 456 Color: 4

Bin 693: 1 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 17
Size: 456 Color: 7

Bin 694: 1 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 18
Size: 451 Color: 6

Bin 695: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 2
Size: 447 Color: 6

Bin 696: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 447 Color: 5

Bin 697: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 8
Size: 447 Color: 9

Bin 698: 1 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 4
Size: 429 Color: 5

Bin 699: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 4
Size: 427 Color: 9

Bin 700: 1 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 11
Size: 427 Color: 3

Bin 701: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 3
Size: 425 Color: 8

Bin 702: 1 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 0
Size: 412 Color: 9

Bin 703: 1 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 8
Size: 412 Color: 10

Bin 704: 1 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 6
Size: 400 Color: 8

Bin 705: 1 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 6
Size: 390 Color: 14

Bin 706: 1 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 6
Size: 390 Color: 1

Bin 707: 1 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 14
Size: 386 Color: 18

Bin 708: 1 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 17
Size: 384 Color: 14

Bin 709: 1 of cap free
Amount of items: 2
Items: 
Size: 619 Color: 9
Size: 381 Color: 14

Bin 710: 1 of cap free
Amount of items: 2
Items: 
Size: 628 Color: 9
Size: 372 Color: 1

Bin 711: 1 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 8
Size: 369 Color: 3

Bin 712: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 15
Size: 367 Color: 0

Bin 713: 1 of cap free
Amount of items: 2
Items: 
Size: 633 Color: 15
Size: 367 Color: 11

Bin 714: 1 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 16
Size: 349 Color: 2

Bin 715: 1 of cap free
Amount of items: 2
Items: 
Size: 655 Color: 6
Size: 345 Color: 15

Bin 716: 1 of cap free
Amount of items: 2
Items: 
Size: 660 Color: 1
Size: 340 Color: 19

Bin 717: 1 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 10
Size: 338 Color: 6

Bin 718: 1 of cap free
Amount of items: 2
Items: 
Size: 664 Color: 16
Size: 336 Color: 10

Bin 719: 1 of cap free
Amount of items: 3
Items: 
Size: 664 Color: 8
Size: 189 Color: 2
Size: 147 Color: 11

Bin 720: 1 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 9
Size: 333 Color: 4

Bin 721: 1 of cap free
Amount of items: 2
Items: 
Size: 677 Color: 19
Size: 323 Color: 6

Bin 722: 1 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 6
Size: 320 Color: 11

Bin 723: 1 of cap free
Amount of items: 2
Items: 
Size: 692 Color: 18
Size: 308 Color: 17

Bin 724: 1 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 14
Size: 288 Color: 6

Bin 725: 1 of cap free
Amount of items: 2
Items: 
Size: 724 Color: 19
Size: 276 Color: 14

Bin 726: 1 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 16
Size: 270 Color: 2

Bin 727: 1 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 4
Size: 268 Color: 19

Bin 728: 1 of cap free
Amount of items: 2
Items: 
Size: 734 Color: 11
Size: 266 Color: 12

Bin 729: 1 of cap free
Amount of items: 2
Items: 
Size: 741 Color: 4
Size: 259 Color: 7

Bin 730: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 18
Size: 254 Color: 7

Bin 731: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 17
Size: 254 Color: 16

Bin 732: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 19
Size: 254 Color: 16

Bin 733: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 17
Size: 254 Color: 7

Bin 734: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 7
Size: 254 Color: 8

Bin 735: 1 of cap free
Amount of items: 2
Items: 
Size: 750 Color: 11
Size: 250 Color: 17

Bin 736: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 2
Size: 246 Color: 5

Bin 737: 1 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 5
Size: 246 Color: 13

Bin 738: 1 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 14
Size: 244 Color: 11

Bin 739: 1 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 3
Size: 244 Color: 1

Bin 740: 1 of cap free
Amount of items: 2
Items: 
Size: 756 Color: 5
Size: 244 Color: 12

Bin 741: 1 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 18
Size: 239 Color: 2

Bin 742: 1 of cap free
Amount of items: 2
Items: 
Size: 761 Color: 11
Size: 239 Color: 2

Bin 743: 1 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 16
Size: 234 Color: 3

Bin 744: 1 of cap free
Amount of items: 2
Items: 
Size: 771 Color: 0
Size: 229 Color: 6

Bin 745: 1 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 7
Size: 221 Color: 3

Bin 746: 1 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 4
Size: 219 Color: 16

Bin 747: 1 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 19
Size: 212 Color: 5

Bin 748: 1 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 7
Size: 209 Color: 4

Bin 749: 1 of cap free
Amount of items: 2
Items: 
Size: 801 Color: 11
Size: 199 Color: 6

Bin 750: 1 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 12
Size: 459 Color: 1

Bin 751: 1 of cap free
Amount of items: 2
Items: 
Size: 701 Color: 6
Size: 299 Color: 9

Bin 752: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 3
Size: 482 Color: 12

Bin 753: 1 of cap free
Amount of items: 2
Items: 
Size: 526 Color: 15
Size: 474 Color: 4

Bin 754: 1 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 13
Size: 333 Color: 10

Bin 755: 1 of cap free
Amount of items: 2
Items: 
Size: 553 Color: 6
Size: 447 Color: 2

Bin 756: 1 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 15
Size: 209 Color: 2

Bin 757: 1 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 17
Size: 410 Color: 18

Bin 758: 1 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 11
Size: 425 Color: 0

Bin 759: 1 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 9
Size: 364 Color: 10

Bin 760: 1 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 18
Size: 405 Color: 10

Bin 761: 1 of cap free
Amount of items: 2
Items: 
Size: 598 Color: 5
Size: 402 Color: 10

Bin 762: 2 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 10
Size: 466 Color: 12

Bin 763: 2 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 2
Size: 466 Color: 5

Bin 764: 2 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 15
Size: 455 Color: 16

Bin 765: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 15
Size: 358 Color: 10

Bin 766: 2 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 1
Size: 287 Color: 2

Bin 767: 2 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 12
Size: 225 Color: 9

Bin 768: 2 of cap free
Amount of items: 2
Items: 
Size: 774 Color: 7
Size: 225 Color: 2

Bin 769: 2 of cap free
Amount of items: 2
Items: 
Size: 500 Color: 2
Size: 499 Color: 12

Bin 770: 2 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 1
Size: 496 Color: 3

Bin 771: 2 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 11
Size: 496 Color: 12

Bin 772: 2 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 10
Size: 489 Color: 6

Bin 773: 2 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 5
Size: 479 Color: 18

Bin 774: 2 of cap free
Amount of items: 2
Items: 
Size: 530 Color: 10
Size: 469 Color: 3

Bin 775: 2 of cap free
Amount of items: 2
Items: 
Size: 541 Color: 2
Size: 458 Color: 9

Bin 776: 2 of cap free
Amount of items: 2
Items: 
Size: 544 Color: 17
Size: 455 Color: 0

Bin 777: 2 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 4
Size: 450 Color: 6

Bin 778: 2 of cap free
Amount of items: 2
Items: 
Size: 549 Color: 6
Size: 450 Color: 2

Bin 779: 2 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 12
Size: 443 Color: 16

Bin 780: 2 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 11
Size: 443 Color: 4

Bin 781: 2 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 19
Size: 420 Color: 14

Bin 782: 2 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 19
Size: 396 Color: 14

Bin 783: 2 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 12
Size: 396 Color: 13

Bin 784: 2 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 11
Size: 396 Color: 16

Bin 785: 2 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 13
Size: 389 Color: 5

Bin 786: 2 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 2
Size: 389 Color: 11

Bin 787: 2 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 10
Size: 389 Color: 18

Bin 788: 2 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 2
Size: 386 Color: 7

Bin 789: 2 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 10
Size: 377 Color: 2

Bin 790: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 17
Size: 358 Color: 2

Bin 791: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 11
Size: 358 Color: 18

Bin 792: 2 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 2
Size: 358 Color: 4

Bin 793: 2 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 8
Size: 345 Color: 17

Bin 794: 2 of cap free
Amount of items: 2
Items: 
Size: 670 Color: 2
Size: 329 Color: 14

Bin 795: 2 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 319 Color: 9

Bin 796: 2 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 294 Color: 14

Bin 797: 2 of cap free
Amount of items: 2
Items: 
Size: 709 Color: 3
Size: 290 Color: 18

Bin 798: 2 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 15
Size: 254 Color: 8

Bin 799: 2 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 16
Size: 241 Color: 18

Bin 800: 2 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 5
Size: 241 Color: 17

Bin 801: 2 of cap free
Amount of items: 2
Items: 
Size: 758 Color: 2
Size: 241 Color: 12

Bin 802: 2 of cap free
Amount of items: 2
Items: 
Size: 765 Color: 15
Size: 234 Color: 5

Bin 803: 2 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 0
Size: 420 Color: 5

Bin 804: 2 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 14
Size: 319 Color: 10

Bin 805: 2 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 13
Size: 489 Color: 6

Bin 806: 2 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 17
Size: 479 Color: 15

Bin 807: 2 of cap free
Amount of items: 2
Items: 
Size: 533 Color: 8
Size: 466 Color: 4

Bin 808: 2 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 4
Size: 299 Color: 8

Bin 809: 3 of cap free
Amount of items: 2
Items: 
Size: 594 Color: 8
Size: 404 Color: 17

Bin 810: 3 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 5
Size: 308 Color: 6

Bin 811: 3 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 8
Size: 395 Color: 13

Bin 812: 3 of cap free
Amount of items: 2
Items: 
Size: 510 Color: 7
Size: 488 Color: 11

Bin 813: 3 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 15
Size: 450 Color: 9

Bin 814: 3 of cap free
Amount of items: 2
Items: 
Size: 579 Color: 9
Size: 419 Color: 3

Bin 815: 3 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 10
Size: 377 Color: 3

Bin 816: 3 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 19
Size: 345 Color: 2

Bin 817: 3 of cap free
Amount of items: 2
Items: 
Size: 754 Color: 2
Size: 244 Color: 16

Bin 818: 3 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 10
Size: 225 Color: 19

Bin 819: 3 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 11
Size: 225 Color: 8

Bin 820: 3 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 11
Size: 219 Color: 16

Bin 821: 3 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 19
Size: 458 Color: 1

Bin 822: 3 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 16
Size: 219 Color: 2

Bin 823: 3 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 5
Size: 442 Color: 15

Bin 824: 3 of cap free
Amount of items: 2
Items: 
Size: 653 Color: 6
Size: 345 Color: 1

Bin 825: 3 of cap free
Amount of items: 2
Items: 
Size: 520 Color: 8
Size: 478 Color: 12

Bin 826: 4 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 2
Size: 419 Color: 14

Bin 827: 4 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 8
Size: 268 Color: 3

Bin 828: 4 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 2
Size: 257 Color: 19

Bin 829: 4 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 15
Size: 294 Color: 19

Bin 830: 4 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 17
Size: 381 Color: 3

Bin 831: 4 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 3
Size: 381 Color: 14

Bin 832: 4 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 13
Size: 381 Color: 5

Bin 833: 4 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 12
Size: 488 Color: 14

Bin 834: 4 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 7
Size: 494 Color: 19

Bin 835: 4 of cap free
Amount of items: 2
Items: 
Size: 626 Color: 6
Size: 371 Color: 19

Bin 836: 5 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 8
Size: 371 Color: 4

Bin 837: 5 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 4
Size: 371 Color: 13

Bin 838: 5 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 6
Size: 293 Color: 3

Bin 839: 5 of cap free
Amount of items: 2
Items: 
Size: 718 Color: 17
Size: 278 Color: 9

Bin 840: 6 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 7
Size: 278 Color: 1

Bin 841: 6 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 17
Size: 355 Color: 0

Bin 842: 6 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 5
Size: 455 Color: 15

Bin 843: 6 of cap free
Amount of items: 2
Items: 
Size: 790 Color: 11
Size: 205 Color: 7

Bin 844: 6 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 18
Size: 417 Color: 10

Bin 845: 6 of cap free
Amount of items: 2
Items: 
Size: 578 Color: 4
Size: 417 Color: 3

Bin 846: 6 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 3
Size: 440 Color: 18

Bin 847: 6 of cap free
Amount of items: 2
Items: 
Size: 555 Color: 11
Size: 440 Color: 3

Bin 848: 7 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 0
Size: 354 Color: 11

Bin 849: 7 of cap free
Amount of items: 2
Items: 
Size: 640 Color: 6
Size: 354 Color: 10

Bin 850: 7 of cap free
Amount of items: 2
Items: 
Size: 577 Color: 1
Size: 417 Color: 16

Bin 851: 7 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 1
Size: 454 Color: 16

Bin 852: 8 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 3
Size: 336 Color: 0

Bin 853: 8 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 11
Size: 290 Color: 13

Bin 854: 9 of cap free
Amount of items: 2
Items: 
Size: 679 Color: 8
Size: 313 Color: 14

Bin 855: 9 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 7
Size: 371 Color: 0

Bin 856: 10 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 7
Size: 488 Color: 5

Bin 857: 10 of cap free
Amount of items: 2
Items: 
Size: 552 Color: 3
Size: 439 Color: 2

Bin 858: 10 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 3
Size: 238 Color: 5

Bin 859: 10 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 3
Size: 488 Color: 18

Bin 860: 10 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 12
Size: 488 Color: 3

Bin 861: 11 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 4
Size: 400 Color: 2

Bin 862: 11 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 9
Size: 369 Color: 4

Bin 863: 13 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 7
Size: 353 Color: 11

Bin 864: 13 of cap free
Amount of items: 2
Items: 
Size: 635 Color: 2
Size: 353 Color: 15

Bin 865: 14 of cap free
Amount of items: 2
Items: 
Size: 588 Color: 0
Size: 399 Color: 4

Bin 866: 14 of cap free
Amount of items: 2
Items: 
Size: 548 Color: 8
Size: 439 Color: 13

Bin 867: 15 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 329 Color: 8

Bin 868: 16 of cap free
Amount of items: 2
Items: 
Size: 712 Color: 8
Size: 273 Color: 12

Bin 869: 23 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 14
Size: 327 Color: 7

Bin 870: 24 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 9
Size: 326 Color: 19

Bin 871: 25 of cap free
Amount of items: 2
Items: 
Size: 488 Color: 19
Size: 488 Color: 7

Bin 872: 27 of cap free
Amount of items: 2
Items: 
Size: 488 Color: 8
Size: 486 Color: 12

Bin 873: 28 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 14
Size: 435 Color: 3

Bin 874: 28 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 9
Size: 435 Color: 2

Bin 875: 29 of cap free
Amount of items: 2
Items: 
Size: 486 Color: 6
Size: 486 Color: 1

Bin 876: 30 of cap free
Amount of items: 2
Items: 
Size: 486 Color: 3
Size: 485 Color: 16

Bin 877: 31 of cap free
Amount of items: 2
Items: 
Size: 485 Color: 18
Size: 485 Color: 4

Bin 878: 32 of cap free
Amount of items: 2
Items: 
Size: 485 Color: 5
Size: 484 Color: 18

Bin 879: 33 of cap free
Amount of items: 2
Items: 
Size: 484 Color: 11
Size: 484 Color: 5

Bin 880: 34 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 3
Size: 179 Color: 8

Bin 881: 39 of cap free
Amount of items: 2
Items: 
Size: 484 Color: 13
Size: 478 Color: 4

Bin 882: 42 of cap free
Amount of items: 2
Items: 
Size: 608 Color: 0
Size: 351 Color: 13

Bin 883: 45 of cap free
Amount of items: 2
Items: 
Size: 651 Color: 8
Size: 305 Color: 5

Bin 884: 46 of cap free
Amount of items: 2
Items: 
Size: 478 Color: 4
Size: 477 Color: 16

Bin 885: 48 of cap free
Amount of items: 2
Items: 
Size: 477 Color: 18
Size: 476 Color: 17

Bin 886: 52 of cap free
Amount of items: 2
Items: 
Size: 476 Color: 9
Size: 473 Color: 1

Bin 887: 55 of cap free
Amount of items: 2
Items: 
Size: 473 Color: 16
Size: 473 Color: 0

Bin 888: 55 of cap free
Amount of items: 2
Items: 
Size: 473 Color: 8
Size: 473 Color: 7

Bin 889: 60 of cap free
Amount of items: 2
Items: 
Size: 472 Color: 2
Size: 469 Color: 5

Bin 890: 66 of cap free
Amount of items: 2
Items: 
Size: 469 Color: 2
Size: 466 Color: 7

Bin 891: 70 of cap free
Amount of items: 2
Items: 
Size: 784 Color: 3
Size: 147 Color: 6

Bin 892: 102 of cap free
Amount of items: 2
Items: 
Size: 466 Color: 11
Size: 433 Color: 8

Bin 893: 135 of cap free
Amount of items: 2
Items: 
Size: 433 Color: 19
Size: 433 Color: 8

Bin 894: 136 of cap free
Amount of items: 2
Items: 
Size: 433 Color: 19
Size: 432 Color: 17

Bin 895: 141 of cap free
Amount of items: 2
Items: 
Size: 431 Color: 19
Size: 429 Color: 6

Bin 896: 228 of cap free
Amount of items: 1
Items: 
Size: 773 Color: 13

Bin 897: 232 of cap free
Amount of items: 1
Items: 
Size: 769 Color: 11

Bin 898: 232 of cap free
Amount of items: 1
Items: 
Size: 769 Color: 4

Bin 899: 249 of cap free
Amount of items: 1
Items: 
Size: 752 Color: 1

Bin 900: 256 of cap free
Amount of items: 1
Items: 
Size: 745 Color: 5

Bin 901: 298 of cap free
Amount of items: 1
Items: 
Size: 703 Color: 15

Total size: 898422
Total free space: 3479

