Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 2
Size: 7052 Color: 3
Size: 876 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 2
Size: 6000 Color: 3
Size: 1184 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12570 Color: 0
Size: 6624 Color: 3
Size: 454 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12579 Color: 1
Size: 6333 Color: 1
Size: 736 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 3
Size: 5928 Color: 4
Size: 466 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 3
Size: 3476 Color: 0
Size: 2352 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 14112 Color: 1
Size: 5104 Color: 4
Size: 432 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 14236 Color: 1
Size: 4516 Color: 4
Size: 896 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 2
Size: 4968 Color: 3
Size: 352 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 4
Size: 4880 Color: 1
Size: 416 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14422 Color: 3
Size: 4358 Color: 2
Size: 868 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14860 Color: 0
Size: 3188 Color: 1
Size: 1600 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14864 Color: 3
Size: 4592 Color: 4
Size: 192 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14910 Color: 4
Size: 4620 Color: 3
Size: 118 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15192 Color: 0
Size: 3022 Color: 4
Size: 1434 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15208 Color: 1
Size: 4016 Color: 4
Size: 424 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15322 Color: 2
Size: 3606 Color: 4
Size: 720 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15392 Color: 4
Size: 3056 Color: 1
Size: 1200 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15484 Color: 1
Size: 4016 Color: 1
Size: 148 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 2
Size: 3760 Color: 4
Size: 368 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15536 Color: 1
Size: 3680 Color: 4
Size: 432 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15602 Color: 4
Size: 2174 Color: 0
Size: 1872 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15678 Color: 3
Size: 3310 Color: 4
Size: 660 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15776 Color: 4
Size: 3552 Color: 3
Size: 320 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15828 Color: 4
Size: 3416 Color: 3
Size: 404 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15832 Color: 0
Size: 3512 Color: 4
Size: 304 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15898 Color: 1
Size: 3374 Color: 2
Size: 376 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 15904 Color: 2
Size: 3072 Color: 1
Size: 672 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16016 Color: 0
Size: 2736 Color: 4
Size: 896 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16022 Color: 3
Size: 3028 Color: 1
Size: 598 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16344 Color: 4
Size: 2128 Color: 1
Size: 1176 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16400 Color: 4
Size: 2240 Color: 3
Size: 1008 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 16387 Color: 1
Size: 2621 Color: 4
Size: 640 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16432 Color: 0
Size: 2656 Color: 4
Size: 560 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16570 Color: 4
Size: 1780 Color: 3
Size: 1298 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16538 Color: 0
Size: 2594 Color: 4
Size: 516 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16608 Color: 2
Size: 1632 Color: 4
Size: 1408 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16674 Color: 2
Size: 2454 Color: 2
Size: 520 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16740 Color: 4
Size: 1782 Color: 3
Size: 1126 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 1
Size: 2448 Color: 0
Size: 480 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16792 Color: 1
Size: 1696 Color: 4
Size: 1160 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16848 Color: 1
Size: 2200 Color: 4
Size: 600 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16908 Color: 4
Size: 1720 Color: 1
Size: 1020 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16910 Color: 0
Size: 2004 Color: 4
Size: 734 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16956 Color: 4
Size: 1886 Color: 2
Size: 806 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16958 Color: 2
Size: 1858 Color: 4
Size: 832 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 4
Size: 2232 Color: 2
Size: 400 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17084 Color: 0
Size: 1632 Color: 4
Size: 932 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17104 Color: 2
Size: 1344 Color: 4
Size: 1200 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17176 Color: 1
Size: 2140 Color: 1
Size: 332 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17244 Color: 4
Size: 1862 Color: 1
Size: 542 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17268 Color: 4
Size: 1636 Color: 2
Size: 744 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17312 Color: 1
Size: 1312 Color: 4
Size: 1024 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17354 Color: 1
Size: 1846 Color: 4
Size: 448 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17396 Color: 2
Size: 1884 Color: 3
Size: 368 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17422 Color: 3
Size: 1266 Color: 0
Size: 960 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17480 Color: 2
Size: 1816 Color: 2
Size: 352 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17524 Color: 1
Size: 1772 Color: 4
Size: 352 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17552 Color: 2
Size: 1648 Color: 4
Size: 448 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 0
Size: 1024 Color: 4
Size: 992 Color: 2

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17654 Color: 2
Size: 1258 Color: 0
Size: 736 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17680 Color: 0
Size: 1600 Color: 2
Size: 368 Color: 4

Bin 63: 1 of cap free
Amount of items: 6
Items: 
Size: 9836 Color: 2
Size: 2756 Color: 0
Size: 2592 Color: 0
Size: 2553 Color: 4
Size: 1440 Color: 3
Size: 470 Color: 0

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 11122 Color: 1
Size: 8525 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12350 Color: 3
Size: 6641 Color: 4
Size: 656 Color: 1

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 3
Size: 6339 Color: 1
Size: 320 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 0
Size: 5889 Color: 3
Size: 220 Color: 2

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 3
Size: 5565 Color: 2
Size: 510 Color: 2

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 14816 Color: 2
Size: 4399 Color: 2
Size: 432 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 15119 Color: 4
Size: 4192 Color: 0
Size: 336 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 15237 Color: 2
Size: 3724 Color: 4
Size: 686 Color: 1

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 16245 Color: 0
Size: 2838 Color: 4
Size: 564 Color: 1

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 17045 Color: 4
Size: 1750 Color: 3
Size: 852 Color: 2

Bin 74: 2 of cap free
Amount of items: 30
Items: 
Size: 832 Color: 3
Size: 832 Color: 3
Size: 800 Color: 3
Size: 788 Color: 0
Size: 784 Color: 1
Size: 704 Color: 4
Size: 704 Color: 3
Size: 704 Color: 3
Size: 704 Color: 1
Size: 704 Color: 0
Size: 704 Color: 0
Size: 672 Color: 3
Size: 672 Color: 3
Size: 672 Color: 3
Size: 672 Color: 2
Size: 660 Color: 3
Size: 656 Color: 3
Size: 632 Color: 2
Size: 624 Color: 2
Size: 608 Color: 2
Size: 604 Color: 4
Size: 576 Color: 2
Size: 576 Color: 1
Size: 576 Color: 1
Size: 576 Color: 0
Size: 544 Color: 2
Size: 544 Color: 2
Size: 524 Color: 1
Size: 516 Color: 0
Size: 482 Color: 1

Bin 75: 2 of cap free
Amount of items: 5
Items: 
Size: 9888 Color: 2
Size: 4028 Color: 1
Size: 3950 Color: 2
Size: 1152 Color: 3
Size: 628 Color: 0

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 11006 Color: 4
Size: 8176 Color: 3
Size: 464 Color: 2

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12684 Color: 1
Size: 6082 Color: 3
Size: 880 Color: 4

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 12971 Color: 2
Size: 6291 Color: 0
Size: 384 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 2
Size: 4860 Color: 3
Size: 672 Color: 0

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 14430 Color: 2
Size: 4960 Color: 4
Size: 256 Color: 0

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 0
Size: 3168 Color: 1
Size: 2022 Color: 4

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 0
Size: 4382 Color: 3

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 15414 Color: 2
Size: 4232 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 16094 Color: 0
Size: 2968 Color: 4
Size: 584 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 17094 Color: 1
Size: 2456 Color: 3
Size: 96 Color: 0

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 11093 Color: 1
Size: 8168 Color: 4
Size: 384 Color: 0

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 12043 Color: 3
Size: 6322 Color: 1
Size: 1280 Color: 1

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12099 Color: 2
Size: 7162 Color: 3
Size: 384 Color: 2

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 2
Size: 5556 Color: 3
Size: 448 Color: 4

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 4
Size: 3677 Color: 1
Size: 1392 Color: 2

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 14803 Color: 0
Size: 4330 Color: 0
Size: 512 Color: 4

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 16246 Color: 4
Size: 2355 Color: 3
Size: 1044 Color: 3

Bin 93: 4 of cap free
Amount of items: 4
Items: 
Size: 9840 Color: 3
Size: 5094 Color: 4
Size: 4422 Color: 2
Size: 288 Color: 1

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 13585 Color: 3
Size: 5635 Color: 2
Size: 424 Color: 1

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 15147 Color: 0
Size: 3433 Color: 4
Size: 1064 Color: 1

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 17042 Color: 0
Size: 2602 Color: 3

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 17514 Color: 4
Size: 2130 Color: 0

Bin 98: 5 of cap free
Amount of items: 13
Items: 
Size: 9825 Color: 3
Size: 1112 Color: 4
Size: 1104 Color: 4
Size: 920 Color: 3
Size: 896 Color: 0
Size: 888 Color: 4
Size: 884 Color: 0
Size: 880 Color: 0
Size: 868 Color: 4
Size: 858 Color: 4
Size: 704 Color: 2
Size: 384 Color: 1
Size: 320 Color: 4

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 10695 Color: 2
Size: 8180 Color: 0
Size: 768 Color: 1

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 16706 Color: 2
Size: 2937 Color: 0

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 16751 Color: 3
Size: 2892 Color: 1

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 11058 Color: 0
Size: 8184 Color: 1
Size: 400 Color: 3

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 13496 Color: 3
Size: 5122 Color: 4
Size: 1024 Color: 1

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 17226 Color: 3
Size: 2415 Color: 2

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 0
Size: 6720 Color: 2
Size: 480 Color: 3

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 15704 Color: 2
Size: 3808 Color: 0
Size: 128 Color: 3

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 15960 Color: 2
Size: 3680 Color: 0

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 16921 Color: 3
Size: 2719 Color: 2

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 15529 Color: 3
Size: 4110 Color: 2

Bin 110: 10 of cap free
Amount of items: 3
Items: 
Size: 11408 Color: 4
Size: 7638 Color: 0
Size: 592 Color: 2

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 17638 Color: 3
Size: 2000 Color: 0

Bin 112: 11 of cap free
Amount of items: 3
Items: 
Size: 11037 Color: 4
Size: 8144 Color: 1
Size: 456 Color: 0

Bin 113: 11 of cap free
Amount of items: 3
Items: 
Size: 14049 Color: 2
Size: 4620 Color: 3
Size: 968 Color: 1

Bin 114: 11 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 1
Size: 5053 Color: 0

Bin 115: 12 of cap free
Amount of items: 9
Items: 
Size: 9826 Color: 1
Size: 1408 Color: 3
Size: 1408 Color: 0
Size: 1376 Color: 2
Size: 1344 Color: 2
Size: 1216 Color: 1
Size: 1184 Color: 4
Size: 1010 Color: 4
Size: 864 Color: 3

Bin 116: 12 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 1
Size: 7016 Color: 3
Size: 332 Color: 2

Bin 117: 12 of cap free
Amount of items: 3
Items: 
Size: 12332 Color: 2
Size: 6616 Color: 1
Size: 688 Color: 3

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 16348 Color: 3
Size: 3288 Color: 2

Bin 119: 14 of cap free
Amount of items: 2
Items: 
Size: 12448 Color: 0
Size: 7186 Color: 4

Bin 120: 14 of cap free
Amount of items: 3
Items: 
Size: 17582 Color: 0
Size: 1988 Color: 2
Size: 64 Color: 1

Bin 121: 14 of cap free
Amount of items: 3
Items: 
Size: 17592 Color: 2
Size: 1914 Color: 0
Size: 128 Color: 3

Bin 122: 15 of cap free
Amount of items: 2
Items: 
Size: 16484 Color: 2
Size: 3149 Color: 1

Bin 123: 15 of cap free
Amount of items: 3
Items: 
Size: 16503 Color: 1
Size: 2962 Color: 3
Size: 168 Color: 0

Bin 124: 15 of cap free
Amount of items: 2
Items: 
Size: 16823 Color: 1
Size: 2810 Color: 3

Bin 125: 16 of cap free
Amount of items: 2
Items: 
Size: 15710 Color: 2
Size: 3922 Color: 1

Bin 126: 16 of cap free
Amount of items: 3
Items: 
Size: 17424 Color: 0
Size: 2176 Color: 1
Size: 32 Color: 0

Bin 127: 16 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 1
Size: 2064 Color: 0
Size: 32 Color: 2

Bin 128: 17 of cap free
Amount of items: 6
Items: 
Size: 9832 Color: 3
Size: 2428 Color: 4
Size: 2282 Color: 0
Size: 2273 Color: 1
Size: 1952 Color: 4
Size: 864 Color: 3

Bin 129: 18 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 4
Size: 7202 Color: 3
Size: 1188 Color: 4

Bin 130: 18 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 2
Size: 6108 Color: 3
Size: 1016 Color: 2

Bin 131: 18 of cap free
Amount of items: 2
Items: 
Size: 13728 Color: 2
Size: 5902 Color: 4

Bin 132: 18 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 0
Size: 2566 Color: 2
Size: 200 Color: 3

Bin 133: 19 of cap free
Amount of items: 4
Items: 
Size: 9834 Color: 3
Size: 4536 Color: 0
Size: 4299 Color: 2
Size: 960 Color: 3

Bin 134: 19 of cap free
Amount of items: 3
Items: 
Size: 11090 Color: 1
Size: 8183 Color: 3
Size: 356 Color: 3

Bin 135: 20 of cap free
Amount of items: 2
Items: 
Size: 17386 Color: 3
Size: 2242 Color: 2

Bin 136: 20 of cap free
Amount of items: 2
Items: 
Size: 17668 Color: 0
Size: 1960 Color: 3

Bin 137: 21 of cap free
Amount of items: 3
Items: 
Size: 11280 Color: 4
Size: 7131 Color: 3
Size: 1216 Color: 4

Bin 138: 22 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 3
Size: 7704 Color: 1
Size: 352 Color: 2

Bin 139: 22 of cap free
Amount of items: 3
Items: 
Size: 17418 Color: 0
Size: 2144 Color: 3
Size: 64 Color: 3

Bin 140: 23 of cap free
Amount of items: 8
Items: 
Size: 9829 Color: 2
Size: 1632 Color: 1
Size: 1632 Color: 0
Size: 1620 Color: 2
Size: 1436 Color: 2
Size: 1424 Color: 3
Size: 1412 Color: 3
Size: 640 Color: 4

Bin 141: 24 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 4
Size: 5234 Color: 2

Bin 142: 26 of cap free
Amount of items: 2
Items: 
Size: 17550 Color: 4
Size: 2072 Color: 3

Bin 143: 27 of cap free
Amount of items: 2
Items: 
Size: 12887 Color: 0
Size: 6734 Color: 2

Bin 144: 28 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 0
Size: 6896 Color: 4
Size: 400 Color: 1

Bin 145: 28 of cap free
Amount of items: 2
Items: 
Size: 12474 Color: 4
Size: 7146 Color: 2

Bin 146: 28 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 4
Size: 5330 Color: 3
Size: 920 Color: 4

Bin 147: 28 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 1
Size: 4640 Color: 3
Size: 102 Color: 1

Bin 148: 28 of cap free
Amount of items: 2
Items: 
Size: 16180 Color: 2
Size: 3440 Color: 3

Bin 149: 29 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 2
Size: 5128 Color: 3

Bin 150: 30 of cap free
Amount of items: 5
Items: 
Size: 9848 Color: 1
Size: 2760 Color: 0
Size: 2704 Color: 0
Size: 2644 Color: 4
Size: 1662 Color: 3

Bin 151: 32 of cap free
Amount of items: 2
Items: 
Size: 13568 Color: 1
Size: 6048 Color: 2

Bin 152: 34 of cap free
Amount of items: 2
Items: 
Size: 14454 Color: 1
Size: 5160 Color: 0

Bin 153: 34 of cap free
Amount of items: 2
Items: 
Size: 16488 Color: 2
Size: 3126 Color: 4

Bin 154: 35 of cap free
Amount of items: 2
Items: 
Size: 15180 Color: 2
Size: 4433 Color: 1

Bin 155: 36 of cap free
Amount of items: 3
Items: 
Size: 13474 Color: 0
Size: 4096 Color: 3
Size: 2042 Color: 4

Bin 156: 36 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 2
Size: 5804 Color: 4

Bin 157: 36 of cap free
Amount of items: 2
Items: 
Size: 17328 Color: 2
Size: 2284 Color: 1

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 16530 Color: 2
Size: 3080 Color: 1

Bin 159: 39 of cap free
Amount of items: 2
Items: 
Size: 14942 Color: 2
Size: 4667 Color: 0

Bin 160: 39 of cap free
Amount of items: 2
Items: 
Size: 16057 Color: 1
Size: 3552 Color: 2

Bin 161: 40 of cap free
Amount of items: 3
Items: 
Size: 11026 Color: 1
Size: 8182 Color: 0
Size: 400 Color: 1

Bin 162: 40 of cap free
Amount of items: 2
Items: 
Size: 15136 Color: 0
Size: 4472 Color: 1

Bin 163: 46 of cap free
Amount of items: 2
Items: 
Size: 16984 Color: 1
Size: 2618 Color: 3

Bin 164: 49 of cap free
Amount of items: 2
Items: 
Size: 15560 Color: 0
Size: 4039 Color: 1

Bin 165: 52 of cap free
Amount of items: 2
Items: 
Size: 17304 Color: 4
Size: 2292 Color: 1

Bin 166: 54 of cap free
Amount of items: 2
Items: 
Size: 17202 Color: 1
Size: 2392 Color: 0

Bin 167: 56 of cap free
Amount of items: 2
Items: 
Size: 15728 Color: 3
Size: 3864 Color: 1

Bin 168: 57 of cap free
Amount of items: 2
Items: 
Size: 15871 Color: 0
Size: 3720 Color: 1

Bin 169: 60 of cap free
Amount of items: 2
Items: 
Size: 12127 Color: 0
Size: 7461 Color: 4

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 4
Size: 7177 Color: 2

Bin 171: 64 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 2
Size: 6992 Color: 4

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 13552 Color: 1
Size: 6032 Color: 4

Bin 173: 64 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 2
Size: 4240 Color: 0

Bin 174: 68 of cap free
Amount of items: 2
Items: 
Size: 12536 Color: 0
Size: 7044 Color: 2

Bin 175: 68 of cap free
Amount of items: 2
Items: 
Size: 14108 Color: 4
Size: 5472 Color: 1

Bin 176: 70 of cap free
Amount of items: 2
Items: 
Size: 16585 Color: 1
Size: 2993 Color: 2

Bin 177: 72 of cap free
Amount of items: 2
Items: 
Size: 14216 Color: 2
Size: 5360 Color: 4

Bin 178: 73 of cap free
Amount of items: 2
Items: 
Size: 17120 Color: 3
Size: 2455 Color: 0

Bin 179: 78 of cap free
Amount of items: 2
Items: 
Size: 16288 Color: 2
Size: 3282 Color: 3

Bin 180: 82 of cap free
Amount of items: 2
Items: 
Size: 15216 Color: 2
Size: 4350 Color: 1

Bin 181: 88 of cap free
Amount of items: 2
Items: 
Size: 16712 Color: 3
Size: 2848 Color: 0

Bin 182: 90 of cap free
Amount of items: 2
Items: 
Size: 16278 Color: 0
Size: 3280 Color: 3

Bin 183: 96 of cap free
Amount of items: 2
Items: 
Size: 11744 Color: 0
Size: 7808 Color: 4

Bin 184: 96 of cap free
Amount of items: 2
Items: 
Size: 14342 Color: 2
Size: 5210 Color: 4

Bin 185: 98 of cap free
Amount of items: 2
Items: 
Size: 16020 Color: 3
Size: 3530 Color: 0

Bin 186: 102 of cap free
Amount of items: 7
Items: 
Size: 9828 Color: 1
Size: 1856 Color: 4
Size: 1678 Color: 2
Size: 1632 Color: 1
Size: 1632 Color: 0
Size: 1492 Color: 0
Size: 1428 Color: 3

Bin 187: 102 of cap free
Amount of items: 2
Items: 
Size: 13506 Color: 4
Size: 6040 Color: 1

Bin 188: 112 of cap free
Amount of items: 2
Items: 
Size: 13152 Color: 0
Size: 6384 Color: 1

Bin 189: 132 of cap free
Amount of items: 3
Items: 
Size: 9904 Color: 1
Size: 5068 Color: 1
Size: 4544 Color: 0

Bin 190: 132 of cap free
Amount of items: 2
Items: 
Size: 12784 Color: 1
Size: 6732 Color: 0

Bin 191: 144 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 4
Size: 5208 Color: 1

Bin 192: 168 of cap free
Amount of items: 3
Items: 
Size: 11188 Color: 4
Size: 7328 Color: 2
Size: 964 Color: 3

Bin 193: 174 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 0
Size: 8202 Color: 4

Bin 194: 214 of cap free
Amount of items: 2
Items: 
Size: 14820 Color: 3
Size: 4614 Color: 1

Bin 195: 224 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 0
Size: 8160 Color: 4
Size: 352 Color: 3

Bin 196: 224 of cap free
Amount of items: 2
Items: 
Size: 13416 Color: 4
Size: 6008 Color: 0

Bin 197: 264 of cap free
Amount of items: 2
Items: 
Size: 11196 Color: 4
Size: 8188 Color: 2

Bin 198: 270 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 0
Size: 5954 Color: 2

Bin 199: 14416 of cap free
Amount of items: 12
Items: 
Size: 512 Color: 2
Size: 512 Color: 2
Size: 512 Color: 0
Size: 488 Color: 0
Size: 480 Color: 2
Size: 456 Color: 4
Size: 448 Color: 1
Size: 392 Color: 4
Size: 384 Color: 1
Size: 380 Color: 4
Size: 348 Color: 1
Size: 320 Color: 0

Total size: 3890304
Total free space: 19648

