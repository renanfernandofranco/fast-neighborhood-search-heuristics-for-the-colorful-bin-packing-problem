Capicity Bin: 15328
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 37
Items: 
Size: 496 Color: 1
Size: 496 Color: 0
Size: 492 Color: 1
Size: 492 Color: 1
Size: 492 Color: 1
Size: 484 Color: 0
Size: 484 Color: 0
Size: 480 Color: 0
Size: 480 Color: 0
Size: 464 Color: 1
Size: 462 Color: 1
Size: 460 Color: 1
Size: 452 Color: 1
Size: 448 Color: 0
Size: 440 Color: 1
Size: 432 Color: 0
Size: 432 Color: 0
Size: 424 Color: 1
Size: 404 Color: 1
Size: 400 Color: 1
Size: 392 Color: 1
Size: 392 Color: 1
Size: 392 Color: 0
Size: 384 Color: 1
Size: 384 Color: 0
Size: 384 Color: 0
Size: 384 Color: 0
Size: 382 Color: 1
Size: 376 Color: 1
Size: 368 Color: 0
Size: 368 Color: 0
Size: 360 Color: 0
Size: 348 Color: 0
Size: 340 Color: 1
Size: 320 Color: 0
Size: 280 Color: 1
Size: 260 Color: 0

Bin 2: 0 of cap free
Amount of items: 7
Items: 
Size: 7678 Color: 1
Size: 1444 Color: 0
Size: 1442 Color: 0
Size: 1388 Color: 0
Size: 1364 Color: 0
Size: 1128 Color: 1
Size: 884 Color: 1

Bin 3: 0 of cap free
Amount of items: 7
Items: 
Size: 7672 Color: 0
Size: 1896 Color: 0
Size: 1480 Color: 0
Size: 1390 Color: 0
Size: 1330 Color: 1
Size: 1128 Color: 1
Size: 432 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7752 Color: 0
Size: 6956 Color: 0
Size: 620 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 0
Size: 4024 Color: 0
Size: 800 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10662 Color: 0
Size: 4058 Color: 1
Size: 608 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10924 Color: 1
Size: 4164 Color: 0
Size: 240 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11412 Color: 0
Size: 3592 Color: 0
Size: 324 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 1
Size: 3007 Color: 1
Size: 408 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 1
Size: 1982 Color: 1
Size: 1072 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 0
Size: 1492 Color: 1
Size: 1482 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 2728 Color: 0
Size: 256 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 0
Size: 2163 Color: 1
Size: 600 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 1
Size: 1980 Color: 1
Size: 704 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 0
Size: 2008 Color: 0
Size: 752 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12676 Color: 0
Size: 2212 Color: 0
Size: 440 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 0
Size: 1464 Color: 0
Size: 892 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12937 Color: 1
Size: 1993 Color: 1
Size: 398 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13045 Color: 0
Size: 1903 Color: 0
Size: 380 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13398 Color: 1
Size: 1486 Color: 1
Size: 444 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 0
Size: 1272 Color: 1
Size: 690 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13476 Color: 1
Size: 1428 Color: 0
Size: 424 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13490 Color: 1
Size: 1314 Color: 0
Size: 524 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 0
Size: 1248 Color: 0
Size: 568 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 1
Size: 1276 Color: 0
Size: 416 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 0
Size: 1496 Color: 1
Size: 164 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13642 Color: 1
Size: 1406 Color: 1
Size: 280 Color: 0

Bin 28: 1 of cap free
Amount of items: 9
Items: 
Size: 7667 Color: 1
Size: 1264 Color: 0
Size: 1168 Color: 0
Size: 1080 Color: 0
Size: 1016 Color: 0
Size: 1008 Color: 1
Size: 1000 Color: 1
Size: 640 Color: 0
Size: 484 Color: 1

Bin 29: 1 of cap free
Amount of items: 6
Items: 
Size: 7676 Color: 0
Size: 1942 Color: 0
Size: 1913 Color: 0
Size: 1412 Color: 1
Size: 1352 Color: 1
Size: 1032 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 8531 Color: 1
Size: 6308 Color: 0
Size: 488 Color: 1

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 0
Size: 5065 Color: 0
Size: 254 Color: 1

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 10269 Color: 1
Size: 5058 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 11511 Color: 1
Size: 3816 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 11915 Color: 1
Size: 2844 Color: 0
Size: 568 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 11932 Color: 0
Size: 3181 Color: 1
Size: 214 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 12073 Color: 1
Size: 2214 Color: 0
Size: 1040 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 12137 Color: 1
Size: 2894 Color: 0
Size: 296 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 12256 Color: 1
Size: 1983 Color: 1
Size: 1088 Color: 0

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 12594 Color: 0
Size: 2221 Color: 1
Size: 512 Color: 0

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 13004 Color: 0
Size: 2323 Color: 1

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 13406 Color: 1
Size: 1921 Color: 0

Bin 42: 2 of cap free
Amount of items: 11
Items: 
Size: 7670 Color: 0
Size: 912 Color: 0
Size: 900 Color: 0
Size: 848 Color: 1
Size: 842 Color: 1
Size: 842 Color: 0
Size: 832 Color: 1
Size: 824 Color: 1
Size: 800 Color: 1
Size: 584 Color: 0
Size: 272 Color: 1

Bin 43: 2 of cap free
Amount of items: 9
Items: 
Size: 7668 Color: 1
Size: 1302 Color: 0
Size: 1272 Color: 0
Size: 1272 Color: 0
Size: 1056 Color: 1
Size: 1012 Color: 1
Size: 1008 Color: 1
Size: 384 Color: 1
Size: 352 Color: 0

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 9212 Color: 1
Size: 5664 Color: 1
Size: 450 Color: 0

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 9258 Color: 0
Size: 5658 Color: 1
Size: 410 Color: 0

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 1
Size: 3530 Color: 1
Size: 444 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 0
Size: 2466 Color: 1
Size: 960 Color: 0

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 12293 Color: 0
Size: 3033 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 12324 Color: 0
Size: 2442 Color: 1
Size: 560 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 12367 Color: 0
Size: 2623 Color: 1
Size: 336 Color: 0

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 13023 Color: 0
Size: 2303 Color: 1

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 13484 Color: 0
Size: 1842 Color: 1

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 8529 Color: 1
Size: 6356 Color: 0
Size: 440 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 9133 Color: 1
Size: 5662 Color: 1
Size: 530 Color: 0

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 10273 Color: 0
Size: 4446 Color: 0
Size: 606 Color: 1

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 10760 Color: 0
Size: 4565 Color: 1

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 10988 Color: 0
Size: 4145 Color: 1
Size: 192 Color: 1

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 0
Size: 3811 Color: 0
Size: 420 Color: 1

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 11179 Color: 1
Size: 3886 Color: 0
Size: 260 Color: 1

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 11594 Color: 1
Size: 3539 Color: 1
Size: 192 Color: 0

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 12181 Color: 0
Size: 3144 Color: 1

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 13033 Color: 1
Size: 2292 Color: 0

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 1
Size: 6364 Color: 1
Size: 712 Color: 0

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 8788 Color: 0
Size: 6328 Color: 1
Size: 208 Color: 0

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 9922 Color: 1
Size: 4426 Color: 0
Size: 976 Color: 1

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 12056 Color: 0
Size: 3268 Color: 1

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 0
Size: 2845 Color: 1

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 12658 Color: 1
Size: 1358 Color: 1
Size: 1308 Color: 0

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 12873 Color: 1
Size: 2451 Color: 0

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 13298 Color: 1
Size: 2026 Color: 0

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 13540 Color: 1
Size: 1784 Color: 0

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 1
Size: 1764 Color: 0

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 13702 Color: 0
Size: 1622 Color: 1

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 0
Size: 1604 Color: 1

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 13002 Color: 1
Size: 2321 Color: 0

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 0
Size: 2053 Color: 1

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 9851 Color: 1
Size: 5163 Color: 0
Size: 308 Color: 1

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 0
Size: 2486 Color: 1
Size: 1276 Color: 0

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 11721 Color: 1
Size: 2469 Color: 1
Size: 1132 Color: 0

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 12418 Color: 1
Size: 2904 Color: 0

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 12426 Color: 1
Size: 2896 Color: 0

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 12580 Color: 1
Size: 2742 Color: 0

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 0
Size: 1940 Color: 1

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 0
Size: 1718 Color: 1

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 11916 Color: 0
Size: 3405 Color: 1

Bin 86: 7 of cap free
Amount of items: 2
Items: 
Size: 12775 Color: 0
Size: 2546 Color: 1

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 0
Size: 4422 Color: 0
Size: 944 Color: 1

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 12038 Color: 1
Size: 3282 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 0
Size: 1846 Color: 1

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 10757 Color: 1
Size: 4562 Color: 0

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 11243 Color: 1
Size: 3620 Color: 0
Size: 456 Color: 0

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 11338 Color: 1
Size: 2717 Color: 0
Size: 1264 Color: 1

Bin 93: 10 of cap free
Amount of items: 10
Items: 
Size: 7666 Color: 1
Size: 1008 Color: 0
Size: 928 Color: 0
Size: 928 Color: 0
Size: 928 Color: 0
Size: 908 Color: 1
Size: 880 Color: 1
Size: 880 Color: 1
Size: 856 Color: 1
Size: 336 Color: 0

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 11390 Color: 0
Size: 2008 Color: 1
Size: 1920 Color: 0

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 1
Size: 2690 Color: 0

Bin 96: 12 of cap free
Amount of items: 26
Items: 
Size: 704 Color: 1
Size: 704 Color: 1
Size: 704 Color: 1
Size: 680 Color: 0
Size: 664 Color: 0
Size: 656 Color: 1
Size: 656 Color: 1
Size: 644 Color: 1
Size: 636 Color: 0
Size: 624 Color: 1
Size: 608 Color: 0
Size: 592 Color: 1
Size: 576 Color: 1
Size: 576 Color: 0
Size: 576 Color: 0
Size: 568 Color: 0
Size: 568 Color: 0
Size: 548 Color: 0
Size: 544 Color: 1
Size: 542 Color: 1
Size: 542 Color: 0
Size: 536 Color: 1
Size: 528 Color: 0
Size: 508 Color: 0
Size: 504 Color: 1
Size: 328 Color: 0

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 12410 Color: 0
Size: 2906 Color: 1

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 0
Size: 2028 Color: 1

Bin 99: 13 of cap free
Amount of items: 3
Items: 
Size: 11820 Color: 1
Size: 2847 Color: 0
Size: 648 Color: 0

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 1
Size: 2251 Color: 0

Bin 101: 13 of cap free
Amount of items: 2
Items: 
Size: 13186 Color: 0
Size: 2129 Color: 1

Bin 102: 14 of cap free
Amount of items: 3
Items: 
Size: 7692 Color: 1
Size: 6862 Color: 1
Size: 760 Color: 0

Bin 103: 14 of cap free
Amount of items: 3
Items: 
Size: 10148 Color: 1
Size: 3890 Color: 0
Size: 1276 Color: 1

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 12102 Color: 0
Size: 3212 Color: 1

Bin 105: 14 of cap free
Amount of items: 2
Items: 
Size: 12252 Color: 1
Size: 3062 Color: 0

Bin 106: 14 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 1
Size: 2426 Color: 0
Size: 224 Color: 1

Bin 107: 14 of cap free
Amount of items: 2
Items: 
Size: 13620 Color: 1
Size: 1694 Color: 0

Bin 108: 15 of cap free
Amount of items: 2
Items: 
Size: 10666 Color: 0
Size: 4647 Color: 1

Bin 109: 16 of cap free
Amount of items: 3
Items: 
Size: 8538 Color: 0
Size: 6378 Color: 1
Size: 396 Color: 0

Bin 110: 16 of cap free
Amount of items: 3
Items: 
Size: 9251 Color: 0
Size: 5665 Color: 1
Size: 396 Color: 0

Bin 111: 16 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 0
Size: 1548 Color: 1

Bin 112: 16 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 1
Size: 1546 Color: 0

Bin 113: 17 of cap free
Amount of items: 2
Items: 
Size: 12386 Color: 1
Size: 2925 Color: 0

Bin 114: 18 of cap free
Amount of items: 2
Items: 
Size: 8542 Color: 0
Size: 6768 Color: 1

Bin 115: 18 of cap free
Amount of items: 2
Items: 
Size: 9858 Color: 0
Size: 5452 Color: 1

Bin 116: 18 of cap free
Amount of items: 2
Items: 
Size: 13700 Color: 0
Size: 1610 Color: 1

Bin 117: 19 of cap free
Amount of items: 2
Items: 
Size: 11476 Color: 1
Size: 3833 Color: 0

Bin 118: 20 of cap free
Amount of items: 3
Items: 
Size: 9660 Color: 1
Size: 5352 Color: 0
Size: 296 Color: 1

Bin 119: 20 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 0
Size: 2096 Color: 1

Bin 120: 21 of cap free
Amount of items: 2
Items: 
Size: 11848 Color: 0
Size: 3459 Color: 1

Bin 121: 22 of cap free
Amount of items: 3
Items: 
Size: 9262 Color: 1
Size: 5692 Color: 0
Size: 352 Color: 1

Bin 122: 22 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 0
Size: 2546 Color: 1

Bin 123: 22 of cap free
Amount of items: 2
Items: 
Size: 12954 Color: 0
Size: 2352 Color: 1

Bin 124: 22 of cap free
Amount of items: 2
Items: 
Size: 13118 Color: 0
Size: 2188 Color: 1

Bin 125: 23 of cap free
Amount of items: 2
Items: 
Size: 10301 Color: 1
Size: 5004 Color: 0

Bin 126: 26 of cap free
Amount of items: 2
Items: 
Size: 8920 Color: 0
Size: 6382 Color: 1

Bin 127: 26 of cap free
Amount of items: 2
Items: 
Size: 13404 Color: 0
Size: 1898 Color: 1

Bin 128: 26 of cap free
Amount of items: 2
Items: 
Size: 13598 Color: 0
Size: 1704 Color: 1

Bin 129: 28 of cap free
Amount of items: 2
Items: 
Size: 11624 Color: 0
Size: 3676 Color: 1

Bin 130: 28 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 1
Size: 2152 Color: 0

Bin 131: 28 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 0
Size: 1534 Color: 1
Size: 96 Color: 0

Bin 132: 29 of cap free
Amount of items: 2
Items: 
Size: 12791 Color: 1
Size: 2508 Color: 0

Bin 133: 29 of cap free
Amount of items: 2
Items: 
Size: 12865 Color: 1
Size: 2434 Color: 0

Bin 134: 30 of cap free
Amount of items: 2
Items: 
Size: 10026 Color: 1
Size: 5272 Color: 0

Bin 135: 30 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 1
Size: 3640 Color: 0

Bin 136: 30 of cap free
Amount of items: 2
Items: 
Size: 13192 Color: 1
Size: 2106 Color: 0

Bin 137: 32 of cap free
Amount of items: 2
Items: 
Size: 12069 Color: 0
Size: 3227 Color: 1

Bin 138: 32 of cap free
Amount of items: 2
Items: 
Size: 12956 Color: 1
Size: 2340 Color: 0

Bin 139: 34 of cap free
Amount of items: 2
Items: 
Size: 11457 Color: 0
Size: 3837 Color: 1

Bin 140: 34 of cap free
Amount of items: 2
Items: 
Size: 12900 Color: 0
Size: 2394 Color: 1

Bin 141: 36 of cap free
Amount of items: 2
Items: 
Size: 7708 Color: 0
Size: 7584 Color: 1

Bin 142: 36 of cap free
Amount of items: 2
Items: 
Size: 10968 Color: 1
Size: 4324 Color: 0

Bin 143: 36 of cap free
Amount of items: 2
Items: 
Size: 13048 Color: 1
Size: 2244 Color: 0

Bin 144: 36 of cap free
Amount of items: 2
Items: 
Size: 13546 Color: 0
Size: 1746 Color: 1

Bin 145: 38 of cap free
Amount of items: 2
Items: 
Size: 12802 Color: 0
Size: 2488 Color: 1

Bin 146: 40 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 1
Size: 1964 Color: 0

Bin 147: 42 of cap free
Amount of items: 3
Items: 
Size: 13550 Color: 1
Size: 1640 Color: 0
Size: 96 Color: 1

Bin 148: 43 of cap free
Amount of items: 2
Items: 
Size: 12708 Color: 1
Size: 2577 Color: 0

Bin 149: 44 of cap free
Amount of items: 2
Items: 
Size: 12402 Color: 0
Size: 2882 Color: 1

Bin 150: 46 of cap free
Amount of items: 2
Items: 
Size: 13754 Color: 1
Size: 1528 Color: 0

Bin 151: 47 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 1
Size: 2047 Color: 0

Bin 152: 48 of cap free
Amount of items: 2
Items: 
Size: 13054 Color: 0
Size: 2226 Color: 1

Bin 153: 51 of cap free
Amount of items: 3
Items: 
Size: 12663 Color: 1
Size: 2422 Color: 0
Size: 192 Color: 1

Bin 154: 52 of cap free
Amount of items: 2
Items: 
Size: 10340 Color: 1
Size: 4936 Color: 0

Bin 155: 53 of cap free
Amount of items: 2
Items: 
Size: 12562 Color: 0
Size: 2713 Color: 1

Bin 156: 54 of cap free
Amount of items: 2
Items: 
Size: 11083 Color: 1
Size: 4191 Color: 0

Bin 157: 54 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 0
Size: 1602 Color: 1

Bin 158: 54 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 0
Size: 1540 Color: 1

Bin 159: 57 of cap free
Amount of items: 2
Items: 
Size: 11689 Color: 0
Size: 3582 Color: 1

Bin 160: 64 of cap free
Amount of items: 2
Items: 
Size: 12733 Color: 0
Size: 2531 Color: 1

Bin 161: 65 of cap free
Amount of items: 2
Items: 
Size: 12149 Color: 1
Size: 3114 Color: 0

Bin 162: 69 of cap free
Amount of items: 11
Items: 
Size: 7665 Color: 0
Size: 836 Color: 0
Size: 800 Color: 0
Size: 776 Color: 1
Size: 776 Color: 1
Size: 766 Color: 1
Size: 766 Color: 1
Size: 728 Color: 0
Size: 720 Color: 1
Size: 720 Color: 0
Size: 706 Color: 0

Bin 163: 72 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 1
Size: 1520 Color: 0

Bin 164: 76 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 1
Size: 2482 Color: 0

Bin 165: 79 of cap free
Amount of items: 2
Items: 
Size: 11032 Color: 0
Size: 4217 Color: 1

Bin 166: 79 of cap free
Amount of items: 2
Items: 
Size: 12389 Color: 0
Size: 2860 Color: 1

Bin 167: 80 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 0
Size: 3096 Color: 1

Bin 168: 85 of cap free
Amount of items: 2
Items: 
Size: 11030 Color: 0
Size: 4213 Color: 1

Bin 169: 88 of cap free
Amount of items: 2
Items: 
Size: 8860 Color: 0
Size: 6380 Color: 1

Bin 170: 90 of cap free
Amount of items: 2
Items: 
Size: 12674 Color: 1
Size: 2564 Color: 0

Bin 171: 92 of cap free
Amount of items: 2
Items: 
Size: 8844 Color: 1
Size: 6392 Color: 0

Bin 172: 97 of cap free
Amount of items: 2
Items: 
Size: 12949 Color: 1
Size: 2282 Color: 0

Bin 173: 106 of cap free
Amount of items: 2
Items: 
Size: 10498 Color: 1
Size: 4724 Color: 0

Bin 174: 110 of cap free
Amount of items: 2
Items: 
Size: 11034 Color: 1
Size: 4184 Color: 0

Bin 175: 112 of cap free
Amount of items: 3
Items: 
Size: 9512 Color: 0
Size: 4028 Color: 0
Size: 1676 Color: 1

Bin 176: 117 of cap free
Amount of items: 2
Items: 
Size: 8824 Color: 1
Size: 6387 Color: 0

Bin 177: 117 of cap free
Amount of items: 2
Items: 
Size: 10729 Color: 0
Size: 4482 Color: 1

Bin 178: 118 of cap free
Amount of items: 2
Items: 
Size: 12898 Color: 0
Size: 2312 Color: 1

Bin 179: 119 of cap free
Amount of items: 2
Items: 
Size: 10703 Color: 1
Size: 4506 Color: 0

Bin 180: 122 of cap free
Amount of items: 2
Items: 
Size: 12370 Color: 0
Size: 2836 Color: 1

Bin 181: 122 of cap free
Amount of items: 2
Items: 
Size: 12545 Color: 0
Size: 2661 Color: 1

Bin 182: 128 of cap free
Amount of items: 2
Items: 
Size: 9324 Color: 1
Size: 5876 Color: 0

Bin 183: 129 of cap free
Amount of items: 2
Items: 
Size: 9416 Color: 0
Size: 5783 Color: 1

Bin 184: 132 of cap free
Amount of items: 2
Items: 
Size: 11870 Color: 1
Size: 3326 Color: 0

Bin 185: 140 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 0
Size: 1820 Color: 1

Bin 186: 148 of cap free
Amount of items: 2
Items: 
Size: 10500 Color: 0
Size: 4680 Color: 1

Bin 187: 150 of cap free
Amount of items: 2
Items: 
Size: 11858 Color: 1
Size: 3320 Color: 0

Bin 188: 156 of cap free
Amount of items: 2
Items: 
Size: 12524 Color: 0
Size: 2648 Color: 1

Bin 189: 160 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 0
Size: 6152 Color: 1

Bin 190: 160 of cap free
Amount of items: 2
Items: 
Size: 10312 Color: 0
Size: 4856 Color: 1

Bin 191: 160 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 1
Size: 2232 Color: 0

Bin 192: 163 of cap free
Amount of items: 2
Items: 
Size: 10725 Color: 0
Size: 4440 Color: 1

Bin 193: 171 of cap free
Amount of items: 2
Items: 
Size: 9753 Color: 0
Size: 5404 Color: 1

Bin 194: 174 of cap free
Amount of items: 2
Items: 
Size: 12230 Color: 1
Size: 2924 Color: 0

Bin 195: 176 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 1
Size: 5432 Color: 0

Bin 196: 186 of cap free
Amount of items: 2
Items: 
Size: 13504 Color: 1
Size: 1638 Color: 0

Bin 197: 211 of cap free
Amount of items: 2
Items: 
Size: 8732 Color: 1
Size: 6385 Color: 0

Bin 198: 248 of cap free
Amount of items: 2
Items: 
Size: 10018 Color: 0
Size: 5062 Color: 1

Bin 199: 8380 of cap free
Amount of items: 24
Items: 
Size: 324 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 304 Color: 0
Size: 296 Color: 1
Size: 296 Color: 0
Size: 288 Color: 1
Size: 288 Color: 1
Size: 288 Color: 0
Size: 288 Color: 0
Size: 280 Color: 1
Size: 280 Color: 0
Size: 280 Color: 0
Size: 272 Color: 1
Size: 272 Color: 1
Size: 268 Color: 1
Size: 268 Color: 1
Size: 264 Color: 1
Size: 264 Color: 0
Size: 256 Color: 0

Total size: 3034944
Total free space: 15328

