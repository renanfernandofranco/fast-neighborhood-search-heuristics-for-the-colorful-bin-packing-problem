Capicity Bin: 2464
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1862 Color: 170
Size: 502 Color: 103
Size: 100 Color: 40

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1969 Color: 176
Size: 413 Color: 96
Size: 82 Color: 33

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 180
Size: 378 Color: 92
Size: 72 Color: 26

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 182
Size: 365 Color: 90
Size: 72 Color: 25

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2063 Color: 185
Size: 315 Color: 85
Size: 86 Color: 34

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 2100 Color: 188
Size: 244 Color: 73
Size: 120 Color: 47

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2115 Color: 190
Size: 261 Color: 77
Size: 88 Color: 36

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2167 Color: 195
Size: 249 Color: 75
Size: 48 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2180 Color: 198
Size: 236 Color: 70
Size: 48 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2182 Color: 199
Size: 172 Color: 60
Size: 110 Color: 45

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 200
Size: 188 Color: 64
Size: 88 Color: 35

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2194 Color: 201
Size: 226 Color: 69
Size: 44 Color: 8

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 152
Size: 874 Color: 126
Size: 48 Color: 12

Bin 14: 1 of cap free
Amount of items: 4
Items: 
Size: 1598 Color: 156
Size: 785 Color: 121
Size: 40 Color: 6
Size: 40 Color: 5

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1657 Color: 159
Size: 780 Color: 120
Size: 26 Color: 4

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 160
Size: 771 Color: 119
Size: 18 Color: 3

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 163
Size: 685 Color: 115
Size: 60 Color: 19

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 186
Size: 387 Color: 94
Size: 8 Color: 1

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 2164 Color: 194
Size: 299 Color: 82

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 197
Size: 291 Color: 81

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 158
Size: 504 Color: 104
Size: 314 Color: 84

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 162
Size: 684 Color: 114
Size: 68 Color: 23

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 1740 Color: 165
Size: 722 Color: 116

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 166
Size: 617 Color: 111
Size: 52 Color: 14

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 168
Size: 630 Color: 112
Size: 12 Color: 2

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 177
Size: 484 Color: 102
Size: 8 Color: 0

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 2090 Color: 187
Size: 372 Color: 91

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1544 Color: 153
Size: 869 Color: 125
Size: 48 Color: 10

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 174
Size: 272 Color: 79
Size: 252 Color: 76

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1988 Color: 178
Size: 473 Color: 101

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 2020 Color: 181
Size: 441 Color: 98

Bin 32: 4 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 167
Size: 662 Color: 113

Bin 33: 4 of cap free
Amount of items: 2
Items: 
Size: 1884 Color: 171
Size: 576 Color: 109

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 191
Size: 342 Color: 88

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 2170 Color: 196
Size: 290 Color: 80

Bin 36: 5 of cap free
Amount of items: 8
Items: 
Size: 1233 Color: 138
Size: 246 Color: 74
Size: 244 Color: 72
Size: 238 Color: 71
Size: 204 Color: 68
Size: 102 Color: 41
Size: 96 Color: 39
Size: 96 Color: 38

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 164
Size: 734 Color: 117

Bin 38: 5 of cap free
Amount of items: 2
Items: 
Size: 2001 Color: 179
Size: 458 Color: 100

Bin 39: 5 of cap free
Amount of items: 2
Items: 
Size: 2045 Color: 183
Size: 414 Color: 97

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 2151 Color: 193
Size: 308 Color: 83

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 1897 Color: 172
Size: 561 Color: 108

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1918 Color: 173
Size: 540 Color: 106

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 184
Size: 404 Color: 95

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 2107 Color: 189
Size: 351 Color: 89

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 2123 Color: 192
Size: 335 Color: 87

Bin 46: 7 of cap free
Amount of items: 5
Items: 
Size: 1235 Color: 140
Size: 558 Color: 107
Size: 452 Color: 99
Size: 136 Color: 53
Size: 76 Color: 31

Bin 47: 10 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 161
Size: 758 Color: 118

Bin 48: 11 of cap free
Amount of items: 2
Items: 
Size: 1849 Color: 169
Size: 604 Color: 110

Bin 49: 11 of cap free
Amount of items: 2
Items: 
Size: 1940 Color: 175
Size: 513 Color: 105

Bin 50: 12 of cap free
Amount of items: 2
Items: 
Size: 1532 Color: 151
Size: 920 Color: 131

Bin 51: 15 of cap free
Amount of items: 3
Items: 
Size: 1559 Color: 155
Size: 846 Color: 124
Size: 44 Color: 7

Bin 52: 17 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 149
Size: 941 Color: 132
Size: 56 Color: 16

Bin 53: 18 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 154
Size: 840 Color: 123
Size: 48 Color: 9

Bin 54: 26 of cap free
Amount of items: 16
Items: 
Size: 204 Color: 67
Size: 200 Color: 66
Size: 200 Color: 65
Size: 176 Color: 63
Size: 176 Color: 62
Size: 176 Color: 61
Size: 172 Color: 59
Size: 168 Color: 58
Size: 152 Color: 57
Size: 128 Color: 50
Size: 124 Color: 49
Size: 122 Color: 48
Size: 120 Color: 46
Size: 108 Color: 44
Size: 108 Color: 43
Size: 104 Color: 42

Bin 55: 29 of cap free
Amount of items: 3
Items: 
Size: 1487 Color: 150
Size: 892 Color: 130
Size: 56 Color: 15

Bin 56: 31 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 143
Size: 1026 Color: 134
Size: 72 Color: 27

Bin 57: 32 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 157
Size: 789 Color: 122

Bin 58: 35 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 145
Size: 1028 Color: 136

Bin 59: 41 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 144
Size: 1027 Color: 135

Bin 60: 42 of cap free
Amount of items: 4
Items: 
Size: 1406 Color: 146
Size: 882 Color: 127
Size: 68 Color: 24
Size: 66 Color: 22

Bin 61: 43 of cap free
Amount of items: 4
Items: 
Size: 1418 Color: 148
Size: 887 Color: 129
Size: 58 Color: 18
Size: 58 Color: 17

Bin 62: 44 of cap free
Amount of items: 4
Items: 
Size: 1410 Color: 147
Size: 882 Color: 128
Size: 64 Color: 21
Size: 64 Color: 20

Bin 63: 57 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 142
Size: 1093 Color: 137
Size: 72 Color: 28

Bin 64: 62 of cap free
Amount of items: 4
Items: 
Size: 1236 Color: 141
Size: 1022 Color: 133
Size: 72 Color: 30
Size: 72 Color: 29

Bin 65: 68 of cap free
Amount of items: 6
Items: 
Size: 1234 Color: 139
Size: 384 Color: 93
Size: 332 Color: 86
Size: 272 Color: 78
Size: 94 Color: 37
Size: 80 Color: 32

Bin 66: 1748 of cap free
Amount of items: 5
Items: 
Size: 152 Color: 56
Size: 148 Color: 55
Size: 144 Color: 54
Size: 136 Color: 52
Size: 136 Color: 51

Total size: 160160
Total free space: 2464

