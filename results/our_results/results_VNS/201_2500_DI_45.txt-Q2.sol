Capicity Bin: 2404
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 622 Color: 1
Size: 64 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 0
Size: 467 Color: 0
Size: 128 Color: 1

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1829 Color: 1
Size: 497 Color: 0
Size: 62 Color: 0
Size: 16 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 502 Color: 0
Size: 44 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1909 Color: 0
Size: 295 Color: 1
Size: 200 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 0
Size: 378 Color: 1
Size: 44 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2083 Color: 1
Size: 253 Color: 0
Size: 68 Color: 1

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 2154 Color: 1
Size: 226 Color: 0
Size: 12 Color: 1
Size: 12 Color: 0

Bin 9: 1 of cap free
Amount of items: 5
Items: 
Size: 1203 Color: 1
Size: 904 Color: 1
Size: 160 Color: 0
Size: 88 Color: 0
Size: 48 Color: 1

Bin 10: 1 of cap free
Amount of items: 3
Items: 
Size: 1206 Color: 1
Size: 1135 Color: 0
Size: 62 Color: 1

Bin 11: 1 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 1
Size: 713 Color: 0
Size: 311 Color: 1

Bin 12: 1 of cap free
Amount of items: 4
Items: 
Size: 1602 Color: 0
Size: 521 Color: 1
Size: 210 Color: 1
Size: 70 Color: 0

Bin 13: 1 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 1
Size: 622 Color: 0

Bin 14: 1 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 1
Size: 406 Color: 0
Size: 204 Color: 1

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 1845 Color: 0
Size: 558 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 0
Size: 242 Color: 1
Size: 198 Color: 0

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 2039 Color: 0
Size: 364 Color: 1

Bin 18: 1 of cap free
Amount of items: 4
Items: 
Size: 2051 Color: 1
Size: 262 Color: 0
Size: 50 Color: 1
Size: 40 Color: 0

Bin 19: 1 of cap free
Amount of items: 4
Items: 
Size: 2059 Color: 1
Size: 294 Color: 0
Size: 30 Color: 0
Size: 20 Color: 1

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 2054 Color: 0
Size: 349 Color: 1

Bin 21: 2 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 0
Size: 1001 Color: 1
Size: 196 Color: 0

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 1241 Color: 0
Size: 1001 Color: 1
Size: 160 Color: 0

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1375 Color: 0
Size: 859 Color: 1
Size: 168 Color: 0

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1430 Color: 1
Size: 890 Color: 0
Size: 82 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1557 Color: 1
Size: 805 Color: 1
Size: 40 Color: 0

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1801 Color: 0
Size: 601 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 0
Size: 517 Color: 1
Size: 72 Color: 1

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 1
Size: 914 Color: 0
Size: 48 Color: 0

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 0
Size: 707 Color: 0
Size: 36 Color: 1

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 493 Color: 0
Size: 170 Color: 0

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 0
Size: 323 Color: 1
Size: 124 Color: 1

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 1
Size: 379 Color: 0

Bin 33: 4 of cap free
Amount of items: 3
Items: 
Size: 1663 Color: 0
Size: 615 Color: 1
Size: 122 Color: 1

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 0
Size: 282 Color: 1

Bin 35: 5 of cap free
Amount of items: 2
Items: 
Size: 1918 Color: 1
Size: 481 Color: 0

Bin 36: 5 of cap free
Amount of items: 2
Items: 
Size: 2066 Color: 1
Size: 333 Color: 0

Bin 37: 6 of cap free
Amount of items: 22
Items: 
Size: 176 Color: 1
Size: 166 Color: 1
Size: 142 Color: 1
Size: 140 Color: 0
Size: 140 Color: 0
Size: 136 Color: 0
Size: 122 Color: 0
Size: 120 Color: 0
Size: 110 Color: 0
Size: 108 Color: 0
Size: 106 Color: 0
Size: 102 Color: 0
Size: 100 Color: 1
Size: 98 Color: 1
Size: 98 Color: 1
Size: 94 Color: 1
Size: 94 Color: 0
Size: 92 Color: 1
Size: 86 Color: 1
Size: 80 Color: 1
Size: 64 Color: 0
Size: 24 Color: 1

Bin 38: 6 of cap free
Amount of items: 2
Items: 
Size: 1921 Color: 0
Size: 477 Color: 1

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1209 Color: 1
Size: 1188 Color: 0

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 0
Size: 831 Color: 1

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1667 Color: 1
Size: 730 Color: 0

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 2101 Color: 1
Size: 296 Color: 0

Bin 43: 8 of cap free
Amount of items: 2
Items: 
Size: 2047 Color: 0
Size: 349 Color: 1

Bin 44: 11 of cap free
Amount of items: 2
Items: 
Size: 2094 Color: 0
Size: 299 Color: 1

Bin 45: 13 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 0
Size: 842 Color: 1

Bin 46: 14 of cap free
Amount of items: 2
Items: 
Size: 1932 Color: 0
Size: 458 Color: 1

Bin 47: 14 of cap free
Amount of items: 2
Items: 
Size: 1987 Color: 0
Size: 403 Color: 1

Bin 48: 15 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 1
Size: 587 Color: 0

Bin 49: 15 of cap free
Amount of items: 2
Items: 
Size: 2114 Color: 1
Size: 275 Color: 0

Bin 50: 16 of cap free
Amount of items: 2
Items: 
Size: 1975 Color: 1
Size: 413 Color: 0

Bin 51: 18 of cap free
Amount of items: 2
Items: 
Size: 2027 Color: 0
Size: 359 Color: 1

Bin 52: 19 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 0
Size: 915 Color: 1

Bin 53: 19 of cap free
Amount of items: 2
Items: 
Size: 1683 Color: 0
Size: 702 Color: 1

Bin 54: 19 of cap free
Amount of items: 2
Items: 
Size: 2063 Color: 1
Size: 322 Color: 0

Bin 55: 22 of cap free
Amount of items: 2
Items: 
Size: 1338 Color: 0
Size: 1044 Color: 1

Bin 56: 22 of cap free
Amount of items: 2
Items: 
Size: 2113 Color: 1
Size: 269 Color: 0

Bin 57: 24 of cap free
Amount of items: 2
Items: 
Size: 1761 Color: 1
Size: 619 Color: 0

Bin 58: 28 of cap free
Amount of items: 3
Items: 
Size: 1270 Color: 0
Size: 1002 Color: 1
Size: 104 Color: 1

Bin 59: 28 of cap free
Amount of items: 2
Items: 
Size: 2134 Color: 0
Size: 242 Color: 1

Bin 60: 33 of cap free
Amount of items: 2
Items: 
Size: 2017 Color: 1
Size: 354 Color: 0

Bin 61: 34 of cap free
Amount of items: 2
Items: 
Size: 1833 Color: 1
Size: 537 Color: 0

Bin 62: 39 of cap free
Amount of items: 2
Items: 
Size: 1394 Color: 1
Size: 971 Color: 0

Bin 63: 44 of cap free
Amount of items: 2
Items: 
Size: 1546 Color: 0
Size: 814 Color: 1

Bin 64: 51 of cap free
Amount of items: 7
Items: 
Size: 718 Color: 0
Size: 456 Color: 0
Size: 340 Color: 1
Size: 315 Color: 1
Size: 192 Color: 1
Size: 188 Color: 1
Size: 144 Color: 0

Bin 65: 51 of cap free
Amount of items: 2
Items: 
Size: 1407 Color: 0
Size: 946 Color: 1

Bin 66: 1748 of cap free
Amount of items: 12
Items: 
Size: 80 Color: 0
Size: 72 Color: 1
Size: 68 Color: 1
Size: 60 Color: 0
Size: 60 Color: 0
Size: 58 Color: 1
Size: 58 Color: 0
Size: 56 Color: 1
Size: 56 Color: 0
Size: 52 Color: 0
Size: 20 Color: 1
Size: 16 Color: 1

Total size: 156260
Total free space: 2404

