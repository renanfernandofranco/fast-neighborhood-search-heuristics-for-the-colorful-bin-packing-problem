Capicity Bin: 1000001
Lower Bound: 222

Bins used: 223
Amount of Colors: 20

Bin 1: 1 of cap free
Amount of items: 3
Items: 
Size: 768589 Color: 17
Size: 131404 Color: 2
Size: 100007 Color: 6

Bin 2: 1 of cap free
Amount of items: 3
Items: 
Size: 625298 Color: 19
Size: 229858 Color: 11
Size: 144844 Color: 17

Bin 3: 1 of cap free
Amount of items: 2
Items: 
Size: 721734 Color: 14
Size: 278266 Color: 6

Bin 4: 5 of cap free
Amount of items: 3
Items: 
Size: 784208 Color: 1
Size: 112066 Color: 3
Size: 103722 Color: 2

Bin 5: 6 of cap free
Amount of items: 2
Items: 
Size: 622295 Color: 17
Size: 377700 Color: 9

Bin 6: 10 of cap free
Amount of items: 3
Items: 
Size: 601799 Color: 16
Size: 264855 Color: 3
Size: 133337 Color: 5

Bin 7: 13 of cap free
Amount of items: 3
Items: 
Size: 687808 Color: 13
Size: 174020 Color: 6
Size: 138160 Color: 0

Bin 8: 15 of cap free
Amount of items: 3
Items: 
Size: 722577 Color: 7
Size: 145730 Color: 1
Size: 131679 Color: 13

Bin 9: 17 of cap free
Amount of items: 3
Items: 
Size: 596773 Color: 12
Size: 244242 Color: 3
Size: 158969 Color: 10

Bin 10: 18 of cap free
Amount of items: 3
Items: 
Size: 774923 Color: 5
Size: 113898 Color: 10
Size: 111162 Color: 10

Bin 11: 26 of cap free
Amount of items: 3
Items: 
Size: 666466 Color: 8
Size: 184741 Color: 6
Size: 148768 Color: 4

Bin 12: 28 of cap free
Amount of items: 3
Items: 
Size: 787638 Color: 3
Size: 106204 Color: 7
Size: 106131 Color: 9

Bin 13: 31 of cap free
Amount of items: 3
Items: 
Size: 696079 Color: 8
Size: 154923 Color: 10
Size: 148968 Color: 12

Bin 14: 31 of cap free
Amount of items: 3
Items: 
Size: 705744 Color: 13
Size: 147622 Color: 16
Size: 146604 Color: 14

Bin 15: 34 of cap free
Amount of items: 3
Items: 
Size: 727108 Color: 2
Size: 137213 Color: 4
Size: 135646 Color: 12

Bin 16: 36 of cap free
Amount of items: 3
Items: 
Size: 660830 Color: 12
Size: 176538 Color: 6
Size: 162597 Color: 19

Bin 17: 44 of cap free
Amount of items: 3
Items: 
Size: 709920 Color: 18
Size: 160807 Color: 17
Size: 129230 Color: 5

Bin 18: 46 of cap free
Amount of items: 3
Items: 
Size: 592645 Color: 7
Size: 252975 Color: 12
Size: 154335 Color: 11

Bin 19: 48 of cap free
Amount of items: 2
Items: 
Size: 507241 Color: 6
Size: 492712 Color: 3

Bin 20: 48 of cap free
Amount of items: 3
Items: 
Size: 486179 Color: 3
Size: 261456 Color: 0
Size: 252318 Color: 3

Bin 21: 56 of cap free
Amount of items: 2
Items: 
Size: 664459 Color: 13
Size: 335486 Color: 14

Bin 22: 57 of cap free
Amount of items: 2
Items: 
Size: 533000 Color: 6
Size: 466944 Color: 3

Bin 23: 58 of cap free
Amount of items: 2
Items: 
Size: 741364 Color: 14
Size: 258579 Color: 6

Bin 24: 58 of cap free
Amount of items: 2
Items: 
Size: 574305 Color: 3
Size: 425638 Color: 17

Bin 25: 65 of cap free
Amount of items: 2
Items: 
Size: 718243 Color: 1
Size: 281693 Color: 12

Bin 26: 65 of cap free
Amount of items: 3
Items: 
Size: 485568 Color: 18
Size: 280506 Color: 7
Size: 233862 Color: 14

Bin 27: 70 of cap free
Amount of items: 3
Items: 
Size: 709604 Color: 8
Size: 150731 Color: 7
Size: 139596 Color: 14

Bin 28: 83 of cap free
Amount of items: 3
Items: 
Size: 505464 Color: 18
Size: 261020 Color: 4
Size: 233434 Color: 4

Bin 29: 85 of cap free
Amount of items: 3
Items: 
Size: 713674 Color: 8
Size: 164577 Color: 1
Size: 121665 Color: 10

Bin 30: 91 of cap free
Amount of items: 3
Items: 
Size: 712414 Color: 5
Size: 168453 Color: 8
Size: 119043 Color: 2

Bin 31: 92 of cap free
Amount of items: 2
Items: 
Size: 705822 Color: 2
Size: 294087 Color: 1

Bin 32: 95 of cap free
Amount of items: 3
Items: 
Size: 761582 Color: 17
Size: 126179 Color: 5
Size: 112145 Color: 10

Bin 33: 96 of cap free
Amount of items: 3
Items: 
Size: 496035 Color: 8
Size: 261749 Color: 2
Size: 242121 Color: 12

Bin 34: 97 of cap free
Amount of items: 2
Items: 
Size: 743134 Color: 19
Size: 256770 Color: 8

Bin 35: 97 of cap free
Amount of items: 2
Items: 
Size: 625693 Color: 14
Size: 374211 Color: 9

Bin 36: 103 of cap free
Amount of items: 3
Items: 
Size: 687598 Color: 12
Size: 165831 Color: 8
Size: 146469 Color: 7

Bin 37: 108 of cap free
Amount of items: 2
Items: 
Size: 764897 Color: 4
Size: 234996 Color: 12

Bin 38: 109 of cap free
Amount of items: 2
Items: 
Size: 775080 Color: 8
Size: 224812 Color: 14

Bin 39: 111 of cap free
Amount of items: 4
Items: 
Size: 471371 Color: 7
Size: 298554 Color: 14
Size: 122407 Color: 9
Size: 107558 Color: 4

Bin 40: 117 of cap free
Amount of items: 3
Items: 
Size: 706316 Color: 6
Size: 156583 Color: 12
Size: 136985 Color: 1

Bin 41: 121 of cap free
Amount of items: 3
Items: 
Size: 651840 Color: 19
Size: 243203 Color: 7
Size: 104837 Color: 16

Bin 42: 121 of cap free
Amount of items: 3
Items: 
Size: 587219 Color: 7
Size: 258098 Color: 9
Size: 154563 Color: 19

Bin 43: 136 of cap free
Amount of items: 3
Items: 
Size: 672101 Color: 10
Size: 167896 Color: 17
Size: 159868 Color: 9

Bin 44: 151 of cap free
Amount of items: 3
Items: 
Size: 604196 Color: 5
Size: 241507 Color: 14
Size: 154147 Color: 14

Bin 45: 154 of cap free
Amount of items: 2
Items: 
Size: 532771 Color: 19
Size: 467076 Color: 5

Bin 46: 173 of cap free
Amount of items: 3
Items: 
Size: 555945 Color: 3
Size: 310185 Color: 1
Size: 133698 Color: 19

Bin 47: 178 of cap free
Amount of items: 2
Items: 
Size: 532562 Color: 10
Size: 467261 Color: 2

Bin 48: 180 of cap free
Amount of items: 2
Items: 
Size: 545044 Color: 13
Size: 454777 Color: 15

Bin 49: 186 of cap free
Amount of items: 3
Items: 
Size: 762378 Color: 11
Size: 123070 Color: 14
Size: 114367 Color: 5

Bin 50: 191 of cap free
Amount of items: 3
Items: 
Size: 695289 Color: 0
Size: 154894 Color: 3
Size: 149627 Color: 7

Bin 51: 192 of cap free
Amount of items: 3
Items: 
Size: 682522 Color: 16
Size: 166610 Color: 2
Size: 150677 Color: 7

Bin 52: 193 of cap free
Amount of items: 2
Items: 
Size: 782256 Color: 12
Size: 217552 Color: 15

Bin 53: 196 of cap free
Amount of items: 2
Items: 
Size: 710909 Color: 9
Size: 288896 Color: 15

Bin 54: 197 of cap free
Amount of items: 2
Items: 
Size: 690324 Color: 10
Size: 309480 Color: 16

Bin 55: 201 of cap free
Amount of items: 2
Items: 
Size: 717873 Color: 10
Size: 281927 Color: 7

Bin 56: 203 of cap free
Amount of items: 2
Items: 
Size: 631289 Color: 14
Size: 368509 Color: 10

Bin 57: 204 of cap free
Amount of items: 2
Items: 
Size: 781206 Color: 17
Size: 218591 Color: 15

Bin 58: 205 of cap free
Amount of items: 2
Items: 
Size: 570246 Color: 18
Size: 429550 Color: 8

Bin 59: 218 of cap free
Amount of items: 2
Items: 
Size: 705259 Color: 14
Size: 294524 Color: 4

Bin 60: 244 of cap free
Amount of items: 3
Items: 
Size: 639393 Color: 1
Size: 187693 Color: 8
Size: 172671 Color: 5

Bin 61: 261 of cap free
Amount of items: 3
Items: 
Size: 616655 Color: 15
Size: 240416 Color: 13
Size: 142669 Color: 0

Bin 62: 283 of cap free
Amount of items: 3
Items: 
Size: 385308 Color: 4
Size: 383604 Color: 3
Size: 230806 Color: 8

Bin 63: 286 of cap free
Amount of items: 3
Items: 
Size: 569206 Color: 12
Size: 295438 Color: 4
Size: 135071 Color: 5

Bin 64: 298 of cap free
Amount of items: 2
Items: 
Size: 503493 Color: 8
Size: 496210 Color: 18

Bin 65: 310 of cap free
Amount of items: 2
Items: 
Size: 562291 Color: 11
Size: 437400 Color: 17

Bin 66: 319 of cap free
Amount of items: 2
Items: 
Size: 674089 Color: 18
Size: 325593 Color: 2

Bin 67: 330 of cap free
Amount of items: 2
Items: 
Size: 663593 Color: 13
Size: 336078 Color: 18

Bin 68: 334 of cap free
Amount of items: 2
Items: 
Size: 610653 Color: 10
Size: 389014 Color: 2

Bin 69: 365 of cap free
Amount of items: 2
Items: 
Size: 595704 Color: 1
Size: 403932 Color: 5

Bin 70: 374 of cap free
Amount of items: 2
Items: 
Size: 778655 Color: 19
Size: 220972 Color: 17

Bin 71: 389 of cap free
Amount of items: 3
Items: 
Size: 621889 Color: 11
Size: 242345 Color: 1
Size: 135378 Color: 7

Bin 72: 390 of cap free
Amount of items: 2
Items: 
Size: 692928 Color: 14
Size: 306683 Color: 13

Bin 73: 392 of cap free
Amount of items: 2
Items: 
Size: 652469 Color: 2
Size: 347140 Color: 11

Bin 74: 393 of cap free
Amount of items: 2
Items: 
Size: 725074 Color: 0
Size: 274534 Color: 19

Bin 75: 421 of cap free
Amount of items: 2
Items: 
Size: 561313 Color: 13
Size: 438267 Color: 16

Bin 76: 425 of cap free
Amount of items: 2
Items: 
Size: 777294 Color: 14
Size: 222282 Color: 5

Bin 77: 432 of cap free
Amount of items: 2
Items: 
Size: 638107 Color: 17
Size: 361462 Color: 18

Bin 78: 442 of cap free
Amount of items: 2
Items: 
Size: 659248 Color: 6
Size: 340311 Color: 4

Bin 79: 443 of cap free
Amount of items: 2
Items: 
Size: 579284 Color: 3
Size: 420274 Color: 1

Bin 80: 451 of cap free
Amount of items: 2
Items: 
Size: 758820 Color: 16
Size: 240730 Color: 13

Bin 81: 457 of cap free
Amount of items: 2
Items: 
Size: 541829 Color: 19
Size: 457715 Color: 9

Bin 82: 463 of cap free
Amount of items: 3
Items: 
Size: 743530 Color: 1
Size: 130764 Color: 0
Size: 125244 Color: 16

Bin 83: 489 of cap free
Amount of items: 3
Items: 
Size: 469554 Color: 2
Size: 334324 Color: 5
Size: 195634 Color: 0

Bin 84: 499 of cap free
Amount of items: 3
Items: 
Size: 698926 Color: 11
Size: 174611 Color: 1
Size: 125965 Color: 8

Bin 85: 518 of cap free
Amount of items: 2
Items: 
Size: 621317 Color: 6
Size: 378166 Color: 3

Bin 86: 598 of cap free
Amount of items: 2
Items: 
Size: 581977 Color: 1
Size: 417426 Color: 5

Bin 87: 620 of cap free
Amount of items: 2
Items: 
Size: 524619 Color: 1
Size: 474762 Color: 17

Bin 88: 662 of cap free
Amount of items: 2
Items: 
Size: 514550 Color: 19
Size: 484789 Color: 1

Bin 89: 672 of cap free
Amount of items: 2
Items: 
Size: 771048 Color: 6
Size: 228281 Color: 11

Bin 90: 677 of cap free
Amount of items: 2
Items: 
Size: 537310 Color: 0
Size: 462014 Color: 14

Bin 91: 681 of cap free
Amount of items: 2
Items: 
Size: 546528 Color: 16
Size: 452792 Color: 14

Bin 92: 690 of cap free
Amount of items: 2
Items: 
Size: 576095 Color: 15
Size: 423216 Color: 5

Bin 93: 694 of cap free
Amount of items: 3
Items: 
Size: 658358 Color: 19
Size: 181273 Color: 19
Size: 159676 Color: 10

Bin 94: 705 of cap free
Amount of items: 2
Items: 
Size: 673166 Color: 0
Size: 326130 Color: 18

Bin 95: 724 of cap free
Amount of items: 3
Items: 
Size: 492060 Color: 18
Size: 263869 Color: 6
Size: 243348 Color: 19

Bin 96: 730 of cap free
Amount of items: 2
Items: 
Size: 689104 Color: 5
Size: 310167 Color: 6

Bin 97: 738 of cap free
Amount of items: 2
Items: 
Size: 605933 Color: 0
Size: 393330 Color: 1

Bin 98: 751 of cap free
Amount of items: 3
Items: 
Size: 566075 Color: 10
Size: 220877 Color: 12
Size: 212298 Color: 15

Bin 99: 755 of cap free
Amount of items: 2
Items: 
Size: 772954 Color: 8
Size: 226292 Color: 4

Bin 100: 810 of cap free
Amount of items: 2
Items: 
Size: 612248 Color: 0
Size: 386943 Color: 7

Bin 101: 843 of cap free
Amount of items: 2
Items: 
Size: 550392 Color: 17
Size: 448766 Color: 5

Bin 102: 853 of cap free
Amount of items: 3
Items: 
Size: 666436 Color: 14
Size: 185839 Color: 6
Size: 146873 Color: 0

Bin 103: 877 of cap free
Amount of items: 2
Items: 
Size: 733896 Color: 0
Size: 265228 Color: 7

Bin 104: 889 of cap free
Amount of items: 2
Items: 
Size: 658335 Color: 13
Size: 340777 Color: 6

Bin 105: 894 of cap free
Amount of items: 2
Items: 
Size: 675273 Color: 5
Size: 323834 Color: 0

Bin 106: 904 of cap free
Amount of items: 3
Items: 
Size: 789695 Color: 13
Size: 107883 Color: 18
Size: 101519 Color: 5

Bin 107: 948 of cap free
Amount of items: 2
Items: 
Size: 718720 Color: 4
Size: 280333 Color: 19

Bin 108: 955 of cap free
Amount of items: 2
Items: 
Size: 788296 Color: 6
Size: 210750 Color: 10

Bin 109: 978 of cap free
Amount of items: 2
Items: 
Size: 710763 Color: 16
Size: 288260 Color: 17

Bin 110: 984 of cap free
Amount of items: 2
Items: 
Size: 599849 Color: 7
Size: 399168 Color: 19

Bin 111: 986 of cap free
Amount of items: 3
Items: 
Size: 359961 Color: 5
Size: 319782 Color: 8
Size: 319272 Color: 4

Bin 112: 991 of cap free
Amount of items: 2
Items: 
Size: 541426 Color: 0
Size: 457584 Color: 4

Bin 113: 1058 of cap free
Amount of items: 2
Items: 
Size: 761757 Color: 17
Size: 237186 Color: 9

Bin 114: 1060 of cap free
Amount of items: 2
Items: 
Size: 627205 Color: 19
Size: 371736 Color: 1

Bin 115: 1087 of cap free
Amount of items: 2
Items: 
Size: 604054 Color: 17
Size: 394860 Color: 4

Bin 116: 1088 of cap free
Amount of items: 3
Items: 
Size: 559490 Color: 13
Size: 222595 Color: 14
Size: 216828 Color: 6

Bin 117: 1090 of cap free
Amount of items: 2
Items: 
Size: 708976 Color: 18
Size: 289935 Color: 12

Bin 118: 1148 of cap free
Amount of items: 2
Items: 
Size: 508889 Color: 18
Size: 489964 Color: 1

Bin 119: 1149 of cap free
Amount of items: 2
Items: 
Size: 521304 Color: 3
Size: 477548 Color: 0

Bin 120: 1190 of cap free
Amount of items: 3
Items: 
Size: 376424 Color: 13
Size: 351456 Color: 18
Size: 270931 Color: 15

Bin 121: 1217 of cap free
Amount of items: 2
Items: 
Size: 625796 Color: 15
Size: 372988 Color: 18

Bin 122: 1235 of cap free
Amount of items: 2
Items: 
Size: 758793 Color: 17
Size: 239973 Color: 19

Bin 123: 1235 of cap free
Amount of items: 3
Items: 
Size: 650314 Color: 15
Size: 180831 Color: 17
Size: 167621 Color: 8

Bin 124: 1277 of cap free
Amount of items: 2
Items: 
Size: 502542 Color: 2
Size: 496182 Color: 12

Bin 125: 1393 of cap free
Amount of items: 2
Items: 
Size: 744584 Color: 4
Size: 254024 Color: 13

Bin 126: 1416 of cap free
Amount of items: 2
Items: 
Size: 550008 Color: 1
Size: 448577 Color: 7

Bin 127: 1418 of cap free
Amount of items: 2
Items: 
Size: 630740 Color: 2
Size: 367843 Color: 9

Bin 128: 1458 of cap free
Amount of items: 2
Items: 
Size: 735298 Color: 9
Size: 263245 Color: 16

Bin 129: 1463 of cap free
Amount of items: 3
Items: 
Size: 683526 Color: 17
Size: 176125 Color: 18
Size: 138887 Color: 4

Bin 130: 1523 of cap free
Amount of items: 2
Items: 
Size: 795923 Color: 0
Size: 202555 Color: 8

Bin 131: 1549 of cap free
Amount of items: 2
Items: 
Size: 581835 Color: 12
Size: 416617 Color: 18

Bin 132: 1578 of cap free
Amount of items: 2
Items: 
Size: 779269 Color: 11
Size: 219154 Color: 7

Bin 133: 1591 of cap free
Amount of items: 2
Items: 
Size: 618819 Color: 13
Size: 379591 Color: 18

Bin 134: 1665 of cap free
Amount of items: 2
Items: 
Size: 770700 Color: 16
Size: 227636 Color: 1

Bin 135: 1666 of cap free
Amount of items: 2
Items: 
Size: 541101 Color: 16
Size: 457234 Color: 4

Bin 136: 1711 of cap free
Amount of items: 2
Items: 
Size: 774124 Color: 12
Size: 224166 Color: 6

Bin 137: 1852 of cap free
Amount of items: 2
Items: 
Size: 561231 Color: 13
Size: 436918 Color: 11

Bin 138: 1903 of cap free
Amount of items: 3
Items: 
Size: 674870 Color: 11
Size: 167543 Color: 4
Size: 155685 Color: 15

Bin 139: 1928 of cap free
Amount of items: 2
Items: 
Size: 602439 Color: 3
Size: 395634 Color: 17

Bin 140: 2080 of cap free
Amount of items: 2
Items: 
Size: 660408 Color: 1
Size: 337513 Color: 11

Bin 141: 2086 of cap free
Amount of items: 2
Items: 
Size: 552406 Color: 8
Size: 445509 Color: 18

Bin 142: 2245 of cap free
Amount of items: 2
Items: 
Size: 749961 Color: 14
Size: 247795 Color: 13

Bin 143: 2360 of cap free
Amount of items: 2
Items: 
Size: 536289 Color: 9
Size: 461352 Color: 1

Bin 144: 2365 of cap free
Amount of items: 2
Items: 
Size: 760596 Color: 4
Size: 237040 Color: 9

Bin 145: 2382 of cap free
Amount of items: 2
Items: 
Size: 770145 Color: 1
Size: 227474 Color: 0

Bin 146: 2524 of cap free
Amount of items: 2
Items: 
Size: 594091 Color: 16
Size: 403386 Color: 6

Bin 147: 2683 of cap free
Amount of items: 2
Items: 
Size: 519789 Color: 15
Size: 477529 Color: 4

Bin 148: 2720 of cap free
Amount of items: 2
Items: 
Size: 691372 Color: 17
Size: 305909 Color: 18

Bin 149: 2750 of cap free
Amount of items: 2
Items: 
Size: 798194 Color: 8
Size: 199057 Color: 11

Bin 150: 2759 of cap free
Amount of items: 2
Items: 
Size: 617766 Color: 7
Size: 379476 Color: 16

Bin 151: 2899 of cap free
Amount of items: 2
Items: 
Size: 739866 Color: 4
Size: 257236 Color: 12

Bin 152: 2950 of cap free
Amount of items: 2
Items: 
Size: 575112 Color: 10
Size: 421939 Color: 14

Bin 153: 2961 of cap free
Amount of items: 2
Items: 
Size: 531879 Color: 9
Size: 465161 Color: 5

Bin 154: 3060 of cap free
Amount of items: 2
Items: 
Size: 760482 Color: 6
Size: 236459 Color: 12

Bin 155: 3091 of cap free
Amount of items: 2
Items: 
Size: 578590 Color: 11
Size: 418320 Color: 8

Bin 156: 3096 of cap free
Amount of items: 2
Items: 
Size: 540977 Color: 8
Size: 455928 Color: 9

Bin 157: 3102 of cap free
Amount of items: 2
Items: 
Size: 630643 Color: 6
Size: 366256 Color: 7

Bin 158: 3148 of cap free
Amount of items: 2
Items: 
Size: 549045 Color: 8
Size: 447808 Color: 17

Bin 159: 3513 of cap free
Amount of items: 2
Items: 
Size: 504785 Color: 18
Size: 491703 Color: 17

Bin 160: 3542 of cap free
Amount of items: 2
Items: 
Size: 766632 Color: 19
Size: 229827 Color: 1

Bin 161: 3560 of cap free
Amount of items: 2
Items: 
Size: 535779 Color: 15
Size: 460662 Color: 2

Bin 162: 3872 of cap free
Amount of items: 2
Items: 
Size: 570197 Color: 10
Size: 425932 Color: 11

Bin 163: 3877 of cap free
Amount of items: 2
Items: 
Size: 739461 Color: 12
Size: 256663 Color: 19

Bin 164: 3918 of cap free
Amount of items: 2
Items: 
Size: 687354 Color: 13
Size: 308729 Color: 0

Bin 165: 4002 of cap free
Amount of items: 2
Items: 
Size: 531405 Color: 2
Size: 464594 Color: 15

Bin 166: 4110 of cap free
Amount of items: 2
Items: 
Size: 625794 Color: 11
Size: 370097 Color: 8

Bin 167: 4113 of cap free
Amount of items: 3
Items: 
Size: 702495 Color: 19
Size: 167520 Color: 14
Size: 125873 Color: 19

Bin 168: 4264 of cap free
Amount of items: 2
Items: 
Size: 743444 Color: 6
Size: 252293 Color: 10

Bin 169: 4292 of cap free
Amount of items: 2
Items: 
Size: 797980 Color: 6
Size: 197729 Color: 3

Bin 170: 4415 of cap free
Amount of items: 2
Items: 
Size: 748265 Color: 6
Size: 247321 Color: 4

Bin 171: 4509 of cap free
Amount of items: 2
Items: 
Size: 592359 Color: 7
Size: 403133 Color: 14

Bin 172: 4752 of cap free
Amount of items: 2
Items: 
Size: 713589 Color: 4
Size: 281660 Color: 7

Bin 173: 5102 of cap free
Amount of items: 2
Items: 
Size: 539718 Color: 3
Size: 455181 Color: 16

Bin 174: 5226 of cap free
Amount of items: 2
Items: 
Size: 702181 Color: 11
Size: 292594 Color: 8

Bin 175: 5238 of cap free
Amount of items: 2
Items: 
Size: 601708 Color: 12
Size: 393055 Color: 2

Bin 176: 5312 of cap free
Amount of items: 2
Items: 
Size: 592011 Color: 5
Size: 402678 Color: 8

Bin 177: 5370 of cap free
Amount of items: 2
Items: 
Size: 673068 Color: 11
Size: 321563 Color: 10

Bin 178: 5427 of cap free
Amount of items: 2
Items: 
Size: 518006 Color: 11
Size: 476568 Color: 18

Bin 179: 5857 of cap free
Amount of items: 2
Items: 
Size: 579139 Color: 8
Size: 415005 Color: 18

Bin 180: 5931 of cap free
Amount of items: 2
Items: 
Size: 548635 Color: 17
Size: 445435 Color: 8

Bin 181: 6095 of cap free
Amount of items: 2
Items: 
Size: 702152 Color: 9
Size: 291754 Color: 5

Bin 182: 6148 of cap free
Amount of items: 2
Items: 
Size: 643053 Color: 8
Size: 350800 Color: 14

Bin 183: 6635 of cap free
Amount of items: 2
Items: 
Size: 658267 Color: 16
Size: 335099 Color: 7

Bin 184: 6905 of cap free
Amount of items: 2
Items: 
Size: 501564 Color: 10
Size: 491532 Color: 16

Bin 185: 7059 of cap free
Amount of items: 2
Items: 
Size: 501416 Color: 15
Size: 491526 Color: 1

Bin 186: 7155 of cap free
Amount of items: 2
Items: 
Size: 591275 Color: 8
Size: 401571 Color: 15

Bin 187: 7949 of cap free
Amount of items: 2
Items: 
Size: 797758 Color: 17
Size: 194294 Color: 10

Bin 188: 8006 of cap free
Amount of items: 2
Items: 
Size: 641992 Color: 2
Size: 350003 Color: 14

Bin 189: 8360 of cap free
Amount of items: 2
Items: 
Size: 712175 Color: 7
Size: 279466 Color: 15

Bin 190: 8406 of cap free
Amount of items: 2
Items: 
Size: 590670 Color: 11
Size: 400925 Color: 8

Bin 191: 8628 of cap free
Amount of items: 2
Items: 
Size: 686006 Color: 14
Size: 305367 Color: 11

Bin 192: 9720 of cap free
Amount of items: 2
Items: 
Size: 498901 Color: 14
Size: 491380 Color: 15

Bin 193: 10226 of cap free
Amount of items: 2
Items: 
Size: 685488 Color: 5
Size: 304287 Color: 3

Bin 194: 10282 of cap free
Amount of items: 2
Items: 
Size: 574896 Color: 3
Size: 414823 Color: 5

Bin 195: 10606 of cap free
Amount of items: 2
Items: 
Size: 670177 Color: 10
Size: 319218 Color: 9

Bin 196: 10794 of cap free
Amount of items: 2
Items: 
Size: 697475 Color: 9
Size: 291732 Color: 18

Bin 197: 10965 of cap free
Amount of items: 2
Items: 
Size: 625490 Color: 1
Size: 363546 Color: 17

Bin 198: 11373 of cap free
Amount of items: 2
Items: 
Size: 574106 Color: 9
Size: 414522 Color: 5

Bin 199: 12060 of cap free
Amount of items: 2
Items: 
Size: 624698 Color: 3
Size: 363243 Color: 17

Bin 200: 12635 of cap free
Amount of items: 2
Items: 
Size: 795094 Color: 17
Size: 192272 Color: 0

Bin 201: 12800 of cap free
Amount of items: 2
Items: 
Size: 682965 Color: 13
Size: 304236 Color: 3

Bin 202: 13473 of cap free
Amount of items: 2
Items: 
Size: 739317 Color: 13
Size: 247211 Color: 1

Bin 203: 13646 of cap free
Amount of items: 2
Items: 
Size: 794160 Color: 16
Size: 192195 Color: 6

Bin 204: 14353 of cap free
Amount of items: 2
Items: 
Size: 586653 Color: 18
Size: 398995 Color: 15

Bin 205: 16686 of cap free
Amount of items: 2
Items: 
Size: 569673 Color: 9
Size: 413642 Color: 13

Bin 206: 21286 of cap free
Amount of items: 2
Items: 
Size: 660104 Color: 7
Size: 318611 Color: 9

Bin 207: 21922 of cap free
Amount of items: 2
Items: 
Size: 792941 Color: 16
Size: 185138 Color: 14

Bin 208: 24572 of cap free
Amount of items: 2
Items: 
Size: 487889 Color: 4
Size: 487540 Color: 6

Bin 209: 31671 of cap free
Amount of items: 2
Items: 
Size: 485975 Color: 11
Size: 482355 Color: 1

Bin 210: 38671 of cap free
Amount of items: 2
Items: 
Size: 569598 Color: 16
Size: 391732 Color: 4

Bin 211: 46693 of cap free
Amount of items: 2
Items: 
Size: 564508 Color: 10
Size: 388800 Color: 9

Bin 212: 49616 of cap free
Amount of items: 2
Items: 
Size: 724149 Color: 18
Size: 226236 Color: 0

Bin 213: 49719 of cap free
Amount of items: 2
Items: 
Size: 475793 Color: 8
Size: 474489 Color: 4

Bin 214: 55758 of cap free
Amount of items: 2
Items: 
Size: 472390 Color: 11
Size: 471853 Color: 14

Bin 215: 56609 of cap free
Amount of items: 2
Items: 
Size: 558169 Color: 9
Size: 385223 Color: 14

Bin 216: 59175 of cap free
Amount of items: 2
Items: 
Size: 471733 Color: 10
Size: 469093 Color: 9

Bin 217: 60119 of cap free
Amount of items: 2
Items: 
Size: 554771 Color: 15
Size: 385111 Color: 17

Bin 218: 71547 of cap free
Amount of items: 2
Items: 
Size: 468167 Color: 2
Size: 460287 Color: 1

Bin 219: 73984 of cap free
Amount of items: 2
Items: 
Size: 548204 Color: 0
Size: 377813 Color: 11

Bin 220: 74331 of cap free
Amount of items: 2
Items: 
Size: 791773 Color: 4
Size: 133897 Color: 11

Bin 221: 82622 of cap free
Amount of items: 2
Items: 
Size: 682900 Color: 3
Size: 234479 Color: 18

Bin 222: 107509 of cap free
Amount of items: 2
Items: 
Size: 790974 Color: 14
Size: 101518 Color: 11

Bin 223: 215061 of cap free
Amount of items: 1
Items: 
Size: 784940 Color: 15

Total size: 221381793
Total free space: 1618430

