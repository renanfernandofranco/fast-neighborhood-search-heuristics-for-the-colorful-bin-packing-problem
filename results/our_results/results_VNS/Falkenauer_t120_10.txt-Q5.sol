Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 4
Size: 292 Color: 1
Size: 287 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 3
Size: 306 Color: 2
Size: 273 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 356 Color: 1
Size: 254 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 297 Color: 0
Size: 251 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 355 Color: 0
Size: 262 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 3
Size: 298 Color: 0
Size: 266 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 346 Color: 0
Size: 269 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 4
Size: 361 Color: 3
Size: 277 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 316 Color: 1
Size: 308 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 255 Color: 2
Size: 250 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 285 Color: 4
Size: 274 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 352 Color: 4
Size: 270 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 294 Color: 4
Size: 279 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 255 Color: 4
Size: 250 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 4
Size: 324 Color: 2
Size: 268 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 4
Size: 339 Color: 1
Size: 306 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 287 Color: 1
Size: 254 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 2
Size: 345 Color: 2
Size: 287 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 257 Color: 3
Size: 251 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 346 Color: 0
Size: 269 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 276 Color: 2
Size: 253 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 312 Color: 0
Size: 281 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 305 Color: 4
Size: 305 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 292 Color: 4
Size: 282 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 4
Size: 263 Color: 1
Size: 258 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 257 Color: 2
Size: 255 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 258 Color: 3
Size: 251 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 329 Color: 1
Size: 319 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 3
Size: 342 Color: 2
Size: 251 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 330 Color: 4
Size: 315 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 4
Size: 303 Color: 1
Size: 272 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 4
Size: 320 Color: 0
Size: 265 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 300 Color: 4
Size: 298 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 0
Size: 269 Color: 1
Size: 257 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 264 Color: 4
Size: 258 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 272 Color: 4
Size: 266 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 3
Size: 297 Color: 4
Size: 265 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 301 Color: 0
Size: 257 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 339 Color: 1
Size: 284 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 297 Color: 1
Size: 282 Color: 1

Total size: 40000
Total free space: 0

