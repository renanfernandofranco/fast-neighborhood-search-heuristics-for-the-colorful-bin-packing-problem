Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 11232 Color: 1
Size: 3686 Color: 1
Size: 938 Color: 0
Size: 432 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 1
Size: 5800 Color: 1
Size: 1152 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 1
Size: 4328 Color: 1
Size: 848 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10196 Color: 1
Size: 4904 Color: 1
Size: 1188 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 1
Size: 5472 Color: 1
Size: 376 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 8164 Color: 1
Size: 7676 Color: 1
Size: 448 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8692 Color: 1
Size: 6708 Color: 1
Size: 888 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 1
Size: 7228 Color: 1
Size: 576 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 8152 Color: 1
Size: 6792 Color: 1
Size: 1344 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 8148 Color: 1
Size: 6788 Color: 1
Size: 1352 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 8146 Color: 1
Size: 6786 Color: 1
Size: 1356 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 9993 Color: 1
Size: 4623 Color: 1
Size: 1152 Color: 0
Size: 520 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8154 Color: 1
Size: 6782 Color: 1
Size: 1352 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8168 Color: 1
Size: 6776 Color: 1
Size: 1344 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8176 Color: 1
Size: 6768 Color: 1
Size: 1344 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 1
Size: 6772 Color: 1
Size: 1336 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9254 Color: 1
Size: 5862 Color: 1
Size: 1172 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9262 Color: 1
Size: 5858 Color: 1
Size: 1168 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 1
Size: 5724 Color: 1
Size: 1144 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 1
Size: 5988 Color: 1
Size: 704 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9932 Color: 1
Size: 5300 Color: 1
Size: 1056 Color: 0

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 10240 Color: 1
Size: 6048 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 10328 Color: 1
Size: 4968 Color: 1
Size: 992 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 10344 Color: 1
Size: 4968 Color: 1
Size: 976 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 10352 Color: 1
Size: 4976 Color: 1
Size: 960 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 1
Size: 4936 Color: 1
Size: 976 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 1
Size: 4528 Color: 1
Size: 1352 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 10591 Color: 1
Size: 4749 Color: 1
Size: 948 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 10836 Color: 1
Size: 4604 Color: 1
Size: 848 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 10838 Color: 1
Size: 4542 Color: 1
Size: 908 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 10864 Color: 1
Size: 4724 Color: 1
Size: 700 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11316 Color: 1
Size: 4620 Color: 1
Size: 352 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11365 Color: 1
Size: 4103 Color: 1
Size: 820 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 1
Size: 4016 Color: 1
Size: 800 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 1
Size: 3986 Color: 1
Size: 796 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11508 Color: 1
Size: 3964 Color: 1
Size: 816 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11524 Color: 1
Size: 3972 Color: 1
Size: 792 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11592 Color: 1
Size: 3928 Color: 1
Size: 768 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 1
Size: 3805 Color: 1
Size: 760 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 1
Size: 3743 Color: 1
Size: 748 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 1
Size: 3792 Color: 1
Size: 480 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12095 Color: 1
Size: 3495 Color: 1
Size: 698 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 1
Size: 3524 Color: 1
Size: 624 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 1
Size: 2910 Color: 1
Size: 1184 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 1
Size: 3416 Color: 1
Size: 672 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12242 Color: 1
Size: 3374 Color: 1
Size: 672 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 1
Size: 3324 Color: 1
Size: 656 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12422 Color: 1
Size: 3222 Color: 1
Size: 644 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 1
Size: 3220 Color: 1
Size: 640 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12432 Color: 1
Size: 3216 Color: 1
Size: 640 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12534 Color: 1
Size: 3130 Color: 1
Size: 624 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12567 Color: 1
Size: 2801 Color: 1
Size: 920 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 1
Size: 3112 Color: 1
Size: 608 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 1
Size: 3440 Color: 1
Size: 256 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 1
Size: 2644 Color: 1
Size: 944 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12725 Color: 1
Size: 2971 Color: 1
Size: 592 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12776 Color: 1
Size: 2936 Color: 1
Size: 576 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12799 Color: 1
Size: 2909 Color: 1
Size: 580 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 1
Size: 2908 Color: 1
Size: 576 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 12910 Color: 1
Size: 2818 Color: 1
Size: 560 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 1
Size: 2804 Color: 1
Size: 560 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 12929 Color: 1
Size: 2967 Color: 1
Size: 392 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2832 Color: 1
Size: 440 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 13017 Color: 1
Size: 2727 Color: 1
Size: 544 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 13018 Color: 1
Size: 2726 Color: 1
Size: 544 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 13110 Color: 1
Size: 2728 Color: 1
Size: 450 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 1
Size: 2628 Color: 1
Size: 544 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 1
Size: 2008 Color: 1
Size: 1088 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 1
Size: 2584 Color: 1
Size: 408 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 13314 Color: 1
Size: 2482 Color: 1
Size: 492 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 1
Size: 2604 Color: 1
Size: 368 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 13341 Color: 1
Size: 2457 Color: 1
Size: 490 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 1
Size: 2413 Color: 1
Size: 482 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 13394 Color: 1
Size: 2210 Color: 1
Size: 684 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 1
Size: 2411 Color: 1
Size: 480 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 1
Size: 2308 Color: 1
Size: 580 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 1
Size: 2416 Color: 1
Size: 372 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 1
Size: 2314 Color: 1
Size: 460 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 1
Size: 2324 Color: 1
Size: 440 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 1
Size: 2408 Color: 1
Size: 336 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 1
Size: 2284 Color: 1
Size: 448 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 1
Size: 2296 Color: 1
Size: 376 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 13636 Color: 1
Size: 2188 Color: 1
Size: 464 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 1
Size: 2212 Color: 1
Size: 408 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 13696 Color: 1
Size: 2108 Color: 1
Size: 484 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 1
Size: 2442 Color: 1
Size: 140 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 13712 Color: 1
Size: 2160 Color: 1
Size: 416 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 1
Size: 2050 Color: 1
Size: 518 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 1996 Color: 1
Size: 528 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 1
Size: 2190 Color: 1
Size: 280 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 13830 Color: 1
Size: 1662 Color: 1
Size: 796 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 1
Size: 2056 Color: 1
Size: 400 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 13836 Color: 1
Size: 2010 Color: 1
Size: 442 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 13864 Color: 1
Size: 2024 Color: 1
Size: 400 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 13868 Color: 1
Size: 1964 Color: 1
Size: 456 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 1930 Color: 1
Size: 480 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 1
Size: 2020 Color: 1
Size: 372 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 1
Size: 2044 Color: 1
Size: 344 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 1
Size: 1973 Color: 1
Size: 394 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 13968 Color: 1
Size: 2096 Color: 1
Size: 224 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 13978 Color: 1
Size: 1902 Color: 1
Size: 408 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 13984 Color: 1
Size: 1824 Color: 1
Size: 480 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 13986 Color: 1
Size: 1952 Color: 1
Size: 350 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 1
Size: 1630 Color: 1
Size: 648 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 14026 Color: 1
Size: 1886 Color: 1
Size: 376 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 1
Size: 1904 Color: 1
Size: 352 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 1
Size: 1816 Color: 1
Size: 412 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 14092 Color: 1
Size: 1884 Color: 1
Size: 312 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 1
Size: 1756 Color: 1
Size: 422 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 1
Size: 1824 Color: 1
Size: 344 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 1
Size: 1768 Color: 1
Size: 352 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 1
Size: 1754 Color: 1
Size: 348 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 1
Size: 1700 Color: 1
Size: 400 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 1
Size: 1742 Color: 1
Size: 344 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 14220 Color: 1
Size: 1060 Color: 0
Size: 1008 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 1
Size: 1656 Color: 1
Size: 408 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 14252 Color: 1
Size: 1860 Color: 1
Size: 176 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 14286 Color: 1
Size: 1670 Color: 1
Size: 332 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 1
Size: 1744 Color: 1
Size: 232 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 14348 Color: 1
Size: 1620 Color: 1
Size: 320 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 1
Size: 1608 Color: 1
Size: 320 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 14366 Color: 1
Size: 1602 Color: 1
Size: 320 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 1
Size: 1724 Color: 1
Size: 176 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 14418 Color: 1
Size: 1562 Color: 1
Size: 308 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 1
Size: 1564 Color: 1
Size: 304 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 14458 Color: 1
Size: 1526 Color: 1
Size: 304 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 1
Size: 1512 Color: 1
Size: 292 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 14486 Color: 1
Size: 1474 Color: 1
Size: 328 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 1
Size: 1464 Color: 1
Size: 320 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 1
Size: 1442 Color: 1
Size: 324 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 1
Size: 1456 Color: 1
Size: 296 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 14540 Color: 1
Size: 1460 Color: 1
Size: 288 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 1
Size: 1428 Color: 1
Size: 284 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 1
Size: 1426 Color: 1
Size: 284 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 1
Size: 1588 Color: 1
Size: 120 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 1
Size: 1404 Color: 1
Size: 272 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 1
Size: 1400 Color: 1
Size: 272 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 14634 Color: 1
Size: 1382 Color: 1
Size: 272 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 1
Size: 1584 Color: 1
Size: 60 Color: 0

Bin 140: 1 of cap free
Amount of items: 5
Items: 
Size: 8145 Color: 1
Size: 3612 Color: 1
Size: 3506 Color: 1
Size: 512 Color: 0
Size: 512 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 1
Size: 3421 Color: 1
Size: 784 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 12183 Color: 1
Size: 3560 Color: 1
Size: 544 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 12479 Color: 1
Size: 3392 Color: 1
Size: 416 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 13173 Color: 1
Size: 2556 Color: 1
Size: 558 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 1
Size: 2437 Color: 1
Size: 528 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 13362 Color: 1
Size: 2305 Color: 1
Size: 620 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 13601 Color: 1
Size: 2414 Color: 1
Size: 272 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 1
Size: 2261 Color: 1
Size: 400 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 1
Size: 2208 Color: 1
Size: 446 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 1
Size: 2152 Color: 1
Size: 486 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 1
Size: 2062 Color: 1
Size: 476 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 1
Size: 2222 Color: 1
Size: 256 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 1
Size: 2067 Color: 1
Size: 192 Color: 0

Bin 154: 2 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 1
Size: 2505 Color: 1
Size: 416 Color: 0

Bin 155: 2 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 1
Size: 5958 Color: 1
Size: 976 Color: 0

Bin 156: 2 of cap free
Amount of items: 3
Items: 
Size: 10070 Color: 1
Size: 5416 Color: 1
Size: 800 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 10182 Color: 1
Size: 5784 Color: 1
Size: 320 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 10741 Color: 1
Size: 4697 Color: 1
Size: 848 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 11478 Color: 1
Size: 4104 Color: 1
Size: 704 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 1
Size: 3988 Color: 1
Size: 768 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 1
Size: 4244 Color: 1
Size: 384 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 11956 Color: 1
Size: 4010 Color: 1
Size: 320 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 1
Size: 3846 Color: 1
Size: 416 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 1
Size: 2996 Color: 1
Size: 492 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 13577 Color: 1
Size: 2117 Color: 1
Size: 592 Color: 0

Bin 166: 2 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 1
Size: 1818 Color: 1
Size: 134 Color: 0

Bin 167: 2 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 1
Size: 1468 Color: 1
Size: 256 Color: 0

Bin 168: 2 of cap free
Amount of items: 5
Items: 
Size: 5309 Color: 1
Size: 5090 Color: 1
Size: 3175 Color: 1
Size: 1356 Color: 0
Size: 1356 Color: 0

Bin 169: 3 of cap free
Amount of items: 3
Items: 
Size: 13261 Color: 1
Size: 2512 Color: 1
Size: 512 Color: 0

Bin 170: 3 of cap free
Amount of items: 3
Items: 
Size: 10653 Color: 1
Size: 4584 Color: 1
Size: 1048 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 1
Size: 4177 Color: 1
Size: 912 Color: 0

Bin 172: 3 of cap free
Amount of items: 3
Items: 
Size: 13172 Color: 1
Size: 2217 Color: 1
Size: 896 Color: 0

Bin 173: 3 of cap free
Amount of items: 4
Items: 
Size: 11368 Color: 1
Size: 3101 Color: 1
Size: 1304 Color: 0
Size: 512 Color: 0

Bin 174: 3 of cap free
Amount of items: 3
Items: 
Size: 9744 Color: 1
Size: 5907 Color: 1
Size: 634 Color: 0

Bin 175: 4 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 1
Size: 3994 Color: 1
Size: 792 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 11674 Color: 1
Size: 3776 Color: 1
Size: 834 Color: 0

Bin 177: 4 of cap free
Amount of items: 3
Items: 
Size: 11792 Color: 1
Size: 3568 Color: 1
Size: 924 Color: 0

Bin 178: 4 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 1
Size: 1508 Color: 1
Size: 360 Color: 0

Bin 179: 4 of cap free
Amount of items: 3
Items: 
Size: 9919 Color: 1
Size: 5981 Color: 1
Size: 384 Color: 0

Bin 180: 5 of cap free
Amount of items: 3
Items: 
Size: 11277 Color: 1
Size: 4622 Color: 1
Size: 384 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 1
Size: 4454 Color: 1
Size: 1036 Color: 0

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 1
Size: 2650 Color: 1
Size: 720 Color: 0

Bin 183: 7 of cap free
Amount of items: 3
Items: 
Size: 9142 Color: 1
Size: 6787 Color: 1
Size: 352 Color: 0

Bin 184: 7 of cap free
Amount of items: 3
Items: 
Size: 9201 Color: 1
Size: 6064 Color: 1
Size: 1016 Color: 0

Bin 185: 7 of cap free
Amount of items: 3
Items: 
Size: 10746 Color: 1
Size: 5247 Color: 1
Size: 288 Color: 0

Bin 186: 11 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 1
Size: 6785 Color: 1
Size: 1264 Color: 0

Bin 187: 12 of cap free
Amount of items: 3
Items: 
Size: 9040 Color: 1
Size: 6532 Color: 1
Size: 704 Color: 0

Bin 188: 24 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 1
Size: 4450 Color: 1
Size: 1194 Color: 0

Bin 189: 26 of cap free
Amount of items: 3
Items: 
Size: 13313 Color: 1
Size: 2597 Color: 1
Size: 352 Color: 0

Bin 190: 33 of cap free
Amount of items: 3
Items: 
Size: 8147 Color: 1
Size: 6996 Color: 1
Size: 1112 Color: 0

Bin 191: 34 of cap free
Amount of items: 3
Items: 
Size: 11540 Color: 1
Size: 4282 Color: 1
Size: 432 Color: 0

Bin 192: 60 of cap free
Amount of items: 3
Items: 
Size: 8244 Color: 1
Size: 5456 Color: 1
Size: 2528 Color: 0

Bin 193: 65 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 1
Size: 2647 Color: 1
Size: 1180 Color: 0

Bin 194: 66 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 1
Size: 2474 Color: 1
Size: 608 Color: 0

Bin 195: 66 of cap free
Amount of items: 3
Items: 
Size: 13629 Color: 1
Size: 2241 Color: 1
Size: 352 Color: 0

Bin 196: 124 of cap free
Amount of items: 3
Items: 
Size: 10950 Color: 1
Size: 5182 Color: 1
Size: 32 Color: 0

Bin 197: 139 of cap free
Amount of items: 3
Items: 
Size: 9113 Color: 1
Size: 6332 Color: 1
Size: 704 Color: 0

Bin 198: 2464 of cap free
Amount of items: 3
Items: 
Size: 8452 Color: 1
Size: 5084 Color: 1
Size: 288 Color: 0

Bin 199: 13044 of cap free
Amount of items: 1
Items: 
Size: 3244 Color: 1

Total size: 3225024
Total free space: 16288

