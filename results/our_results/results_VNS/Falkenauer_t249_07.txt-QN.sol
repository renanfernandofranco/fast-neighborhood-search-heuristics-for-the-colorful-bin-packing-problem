Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 201
Size: 299 Color: 118
Size: 283 Color: 97

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 173
Size: 365 Color: 166
Size: 258 Color: 36

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 198
Size: 318 Color: 135
Size: 267 Color: 67

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 191
Size: 336 Color: 148
Size: 260 Color: 48

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 179
Size: 363 Color: 164
Size: 254 Color: 26

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 244
Size: 255 Color: 28
Size: 250 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 183
Size: 332 Color: 145
Size: 277 Color: 84

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 217
Size: 282 Color: 94
Size: 269 Color: 71

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 226
Size: 270 Color: 72
Size: 268 Color: 69

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 202
Size: 327 Color: 142
Size: 254 Color: 24

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 199
Size: 303 Color: 123
Size: 280 Color: 92

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 176
Size: 324 Color: 140
Size: 296 Color: 114

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 229
Size: 271 Color: 75
Size: 261 Color: 51

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 222
Size: 287 Color: 103
Size: 253 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 247
Size: 252 Color: 11
Size: 250 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 235
Size: 271 Color: 77
Size: 258 Color: 40

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 231
Size: 278 Color: 87
Size: 254 Color: 21

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 171
Size: 359 Color: 162
Size: 266 Color: 65

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 175
Size: 355 Color: 160
Size: 266 Color: 64

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 194
Size: 341 Color: 151
Size: 252 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 219
Size: 284 Color: 101
Size: 266 Color: 62

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 182
Size: 352 Color: 157
Size: 259 Color: 45

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 184
Size: 354 Color: 158
Size: 254 Color: 25

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 203
Size: 322 Color: 139
Size: 259 Color: 43

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 215
Size: 295 Color: 112
Size: 261 Color: 49

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 214
Size: 298 Color: 117
Size: 259 Color: 41

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 189
Size: 307 Color: 130
Size: 296 Color: 113

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 172
Size: 371 Color: 169
Size: 253 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 221
Size: 276 Color: 82
Size: 268 Color: 68

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 197
Size: 296 Color: 115
Size: 292 Color: 109

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 236
Size: 273 Color: 78
Size: 255 Color: 29

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 213
Size: 284 Color: 98
Size: 276 Color: 81

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 154
Size: 340 Color: 150
Size: 313 Color: 132

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 216
Size: 278 Color: 88
Size: 275 Color: 80

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 188
Size: 313 Color: 133
Size: 291 Color: 108

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 7
Size: 250 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 208
Size: 304 Color: 127
Size: 266 Color: 63

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 240
Size: 256 Color: 32
Size: 256 Color: 31

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 195
Size: 297 Color: 116
Size: 292 Color: 111

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 204
Size: 303 Color: 121
Size: 277 Color: 86

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 227
Size: 274 Color: 79
Size: 261 Color: 52

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 192
Size: 337 Color: 149
Size: 258 Color: 38

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 234
Size: 268 Color: 70
Size: 262 Color: 57

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 233
Size: 271 Color: 76
Size: 259 Color: 42

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 205
Size: 321 Color: 138
Size: 254 Color: 23

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 242
Size: 256 Color: 33
Size: 255 Color: 27

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 187
Size: 344 Color: 152
Size: 260 Color: 46

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 168
Size: 347 Color: 153
Size: 283 Color: 95

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 211
Size: 305 Color: 129
Size: 262 Color: 55

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 178
Size: 335 Color: 147
Size: 284 Color: 100

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 209
Size: 304 Color: 124
Size: 266 Color: 66

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 232
Size: 276 Color: 83
Size: 256 Color: 34

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 193
Size: 305 Color: 128
Size: 289 Color: 105

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 238
Size: 266 Color: 61
Size: 254 Color: 22

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 210
Size: 291 Color: 107
Size: 279 Color: 90

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 190
Size: 335 Color: 146
Size: 264 Color: 58

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 206
Size: 324 Color: 141
Size: 250 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 245
Size: 253 Color: 17
Size: 250 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 220
Size: 285 Color: 102
Size: 264 Color: 59

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 155
Size: 331 Color: 144
Size: 319 Color: 137

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 243
Size: 254 Color: 19
Size: 252 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 246
Size: 252 Color: 10
Size: 250 Color: 5

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 177
Size: 361 Color: 163
Size: 259 Color: 44

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 218
Size: 300 Color: 119
Size: 251 Color: 6

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 239
Size: 262 Color: 54
Size: 252 Color: 9

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 196
Size: 330 Color: 143
Size: 258 Color: 39

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 228
Size: 270 Color: 73
Size: 262 Color: 56

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 174
Size: 365 Color: 165
Size: 258 Color: 37

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 241
Size: 260 Color: 47
Size: 252 Color: 8

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 186
Size: 304 Color: 126
Size: 301 Color: 120

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 237
Size: 271 Color: 74
Size: 253 Color: 15

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 161
Size: 350 Color: 156
Size: 292 Color: 110

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 180
Size: 314 Color: 134
Size: 303 Color: 122

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 181
Size: 355 Color: 159
Size: 261 Color: 53

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 170
Size: 368 Color: 167
Size: 258 Color: 35

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 200
Size: 304 Color: 125
Size: 279 Color: 91

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 212
Size: 283 Color: 96
Size: 281 Color: 93

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 207
Size: 309 Color: 131
Size: 264 Color: 60

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 225
Size: 284 Color: 99
Size: 254 Color: 20

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 230
Size: 277 Color: 85
Size: 255 Color: 30

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 224
Size: 278 Color: 89
Size: 261 Color: 50

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 223
Size: 287 Color: 104
Size: 253 Color: 18

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 185
Size: 319 Color: 136
Size: 289 Color: 106

Total size: 83000
Total free space: 0

