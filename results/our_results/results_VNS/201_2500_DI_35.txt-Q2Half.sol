Capicity Bin: 2400
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 1
Size: 192 Color: 1
Size: 84 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 1
Size: 252 Color: 1
Size: 58 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 1
Size: 611 Color: 1
Size: 146 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 1
Size: 302 Color: 1
Size: 56 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 1
Size: 719 Color: 1
Size: 126 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 790 Color: 1
Size: 40 Color: 0

Bin 7: 0 of cap free
Amount of items: 5
Items: 
Size: 1857 Color: 1
Size: 387 Color: 1
Size: 84 Color: 0
Size: 48 Color: 0
Size: 24 Color: 0

Bin 8: 0 of cap free
Amount of items: 6
Items: 
Size: 1221 Color: 1
Size: 711 Color: 1
Size: 298 Color: 1
Size: 106 Color: 0
Size: 32 Color: 0
Size: 32 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 1
Size: 206 Color: 1
Size: 48 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 1
Size: 249 Color: 1
Size: 72 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 1
Size: 356 Color: 1
Size: 86 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 1
Size: 329 Color: 1
Size: 108 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 434 Color: 1
Size: 172 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 1
Size: 427 Color: 1
Size: 160 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 1
Size: 610 Color: 1
Size: 120 Color: 0

Bin 16: 0 of cap free
Amount of items: 5
Items: 
Size: 1539 Color: 1
Size: 367 Color: 1
Size: 296 Color: 1
Size: 112 Color: 0
Size: 86 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 1
Size: 678 Color: 1
Size: 132 Color: 0

Bin 18: 0 of cap free
Amount of items: 5
Items: 
Size: 1188 Color: 1
Size: 822 Color: 1
Size: 262 Color: 1
Size: 76 Color: 0
Size: 52 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2041 Color: 1
Size: 269 Color: 1
Size: 90 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 559 Color: 1
Size: 64 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1547 Color: 1
Size: 731 Color: 1
Size: 122 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 1
Size: 466 Color: 1
Size: 92 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 1
Size: 997 Color: 1
Size: 198 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 1
Size: 842 Color: 1
Size: 120 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 1
Size: 453 Color: 1
Size: 142 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 1
Size: 1002 Color: 1
Size: 196 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 1
Size: 255 Color: 1
Size: 140 Color: 0

Bin 28: 0 of cap free
Amount of items: 5
Items: 
Size: 1025 Color: 1
Size: 506 Color: 1
Size: 460 Color: 0
Size: 331 Color: 1
Size: 78 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 1
Size: 825 Color: 1
Size: 164 Color: 0

Bin 30: 0 of cap free
Amount of items: 5
Items: 
Size: 1201 Color: 1
Size: 705 Color: 1
Size: 382 Color: 1
Size: 64 Color: 0
Size: 48 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 1
Size: 605 Color: 1
Size: 136 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 974 Color: 1
Size: 192 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 1
Size: 839 Color: 1
Size: 196 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 1
Size: 283 Color: 1
Size: 56 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 1
Size: 497 Color: 1
Size: 160 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 1
Size: 226 Color: 1
Size: 64 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 1
Size: 554 Color: 1
Size: 108 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 1
Size: 350 Color: 1
Size: 168 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1395 Color: 1
Size: 863 Color: 1
Size: 142 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 1
Size: 888 Color: 1
Size: 122 Color: 0

Bin 41: 1 of cap free
Amount of items: 5
Items: 
Size: 1399 Color: 1
Size: 619 Color: 1
Size: 293 Color: 1
Size: 56 Color: 0
Size: 32 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 1
Size: 401 Color: 1
Size: 16 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 2007 Color: 1
Size: 332 Color: 1
Size: 60 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 1
Size: 441 Color: 1
Size: 44 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 1
Size: 238 Color: 1
Size: 58 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 1
Size: 301 Color: 1
Size: 100 Color: 0

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 1
Size: 549 Color: 1
Size: 174 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 1
Size: 835 Color: 1
Size: 40 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 1
Size: 433 Color: 1
Size: 44 Color: 0

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 1961 Color: 1
Size: 365 Color: 1
Size: 72 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 1
Size: 242 Color: 1
Size: 98 Color: 0

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 1889 Color: 1
Size: 491 Color: 1
Size: 16 Color: 0

Bin 53: 6 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 1
Size: 631 Color: 1
Size: 96 Color: 0

Bin 54: 6 of cap free
Amount of items: 3
Items: 
Size: 1920 Color: 1
Size: 406 Color: 1
Size: 68 Color: 0

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1937 Color: 1
Size: 287 Color: 1
Size: 166 Color: 0

Bin 56: 14 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 1
Size: 802 Color: 1
Size: 166 Color: 0

Bin 57: 26 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 1
Size: 533 Color: 1
Size: 80 Color: 0

Bin 58: 37 of cap free
Amount of items: 3
Items: 
Size: 1881 Color: 1
Size: 338 Color: 1
Size: 144 Color: 0

Bin 59: 48 of cap free
Amount of items: 2
Items: 
Size: 2154 Color: 1
Size: 198 Color: 0

Bin 60: 75 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 1
Size: 1052 Color: 1
Size: 64 Color: 0

Bin 61: 270 of cap free
Amount of items: 1
Items: 
Size: 2130 Color: 1

Bin 62: 305 of cap free
Amount of items: 1
Items: 
Size: 2095 Color: 1

Bin 63: 330 of cap free
Amount of items: 1
Items: 
Size: 2070 Color: 1

Bin 64: 351 of cap free
Amount of items: 1
Items: 
Size: 2049 Color: 1

Bin 65: 374 of cap free
Amount of items: 3
Items: 
Size: 993 Color: 1
Size: 983 Color: 1
Size: 50 Color: 0

Bin 66: 527 of cap free
Amount of items: 1
Items: 
Size: 1873 Color: 1

Total size: 156000
Total free space: 2400

