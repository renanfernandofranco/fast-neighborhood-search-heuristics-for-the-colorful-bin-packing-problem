Capicity Bin: 8352
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4186 Color: 11
Size: 3474 Color: 6
Size: 692 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 16
Size: 3462 Color: 0
Size: 688 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5694 Color: 10
Size: 2494 Color: 17
Size: 164 Color: 14

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 6164 Color: 10
Size: 1916 Color: 1
Size: 168 Color: 7
Size: 104 Color: 18

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 6315 Color: 9
Size: 1453 Color: 4
Size: 440 Color: 14
Size: 144 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 12
Size: 1426 Color: 5
Size: 202 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6852 Color: 5
Size: 1252 Color: 0
Size: 248 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6969 Color: 8
Size: 1153 Color: 8
Size: 230 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6982 Color: 19
Size: 1158 Color: 12
Size: 212 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 15
Size: 694 Color: 14
Size: 596 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 16
Size: 949 Color: 3
Size: 328 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7078 Color: 6
Size: 1162 Color: 14
Size: 112 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 5
Size: 788 Color: 19
Size: 424 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 6
Size: 1056 Color: 7
Size: 140 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 5
Size: 694 Color: 18
Size: 436 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 1
Size: 716 Color: 17
Size: 312 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7364 Color: 4
Size: 756 Color: 12
Size: 232 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7378 Color: 18
Size: 762 Color: 19
Size: 212 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7395 Color: 8
Size: 709 Color: 4
Size: 248 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7442 Color: 14
Size: 730 Color: 1
Size: 180 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7452 Color: 13
Size: 814 Color: 18
Size: 86 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7457 Color: 13
Size: 747 Color: 14
Size: 148 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7484 Color: 18
Size: 724 Color: 11
Size: 144 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7494 Color: 11
Size: 664 Color: 13
Size: 194 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7511 Color: 14
Size: 701 Color: 9
Size: 140 Color: 5

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 6021 Color: 16
Size: 2186 Color: 18
Size: 144 Color: 17

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6175 Color: 15
Size: 2036 Color: 1
Size: 140 Color: 18

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 6789 Color: 4
Size: 1442 Color: 1
Size: 120 Color: 15

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 17
Size: 1341 Color: 3
Size: 64 Color: 8

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6998 Color: 18
Size: 1021 Color: 11
Size: 332 Color: 7

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 4
Size: 1053 Color: 15
Size: 148 Color: 19

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 7215 Color: 8
Size: 1136 Color: 13

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 1
Size: 825 Color: 3
Size: 192 Color: 0

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 7353 Color: 16
Size: 942 Color: 19
Size: 56 Color: 7

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 7478 Color: 4
Size: 873 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 4866 Color: 17
Size: 2894 Color: 1
Size: 590 Color: 4

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 6
Size: 2906 Color: 0
Size: 576 Color: 10

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 7
Size: 2959 Color: 15

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 5730 Color: 0
Size: 2620 Color: 18

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 6594 Color: 15
Size: 1756 Color: 19

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 6802 Color: 17
Size: 1460 Color: 7
Size: 88 Color: 12

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 7238 Color: 0
Size: 828 Color: 19
Size: 284 Color: 15

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 7308 Color: 5
Size: 778 Color: 15
Size: 264 Color: 18

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 9
Size: 1012 Color: 13

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 7396 Color: 2
Size: 954 Color: 4

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 7500 Color: 2
Size: 850 Color: 9

Bin 47: 3 of cap free
Amount of items: 4
Items: 
Size: 4178 Color: 15
Size: 3481 Color: 0
Size: 536 Color: 1
Size: 154 Color: 14

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 11
Size: 2061 Color: 4
Size: 228 Color: 14

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 11
Size: 1111 Color: 19
Size: 778 Color: 17

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 6487 Color: 7
Size: 1174 Color: 2
Size: 688 Color: 11

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 8
Size: 1555 Color: 6
Size: 152 Color: 7

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 11
Size: 1030 Color: 15
Size: 72 Color: 0

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 7503 Color: 18
Size: 846 Color: 19

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 4
Size: 3476 Color: 19
Size: 692 Color: 0

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 4882 Color: 7
Size: 3466 Color: 3

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 8
Size: 3004 Color: 18
Size: 188 Color: 14

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 7270 Color: 6
Size: 1078 Color: 4

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 7286 Color: 18
Size: 1062 Color: 14

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 7427 Color: 16
Size: 921 Color: 0

Bin 60: 5 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 19
Size: 3279 Color: 11
Size: 272 Color: 14

Bin 61: 6 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 11
Size: 2690 Color: 2
Size: 338 Color: 18

Bin 62: 6 of cap free
Amount of items: 3
Items: 
Size: 6916 Color: 7
Size: 1002 Color: 18
Size: 428 Color: 1

Bin 63: 6 of cap free
Amount of items: 2
Items: 
Size: 7052 Color: 9
Size: 1294 Color: 14

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 5
Size: 1142 Color: 0

Bin 65: 7 of cap free
Amount of items: 2
Items: 
Size: 4284 Color: 16
Size: 4061 Color: 14

Bin 66: 7 of cap free
Amount of items: 3
Items: 
Size: 4637 Color: 1
Size: 3540 Color: 16
Size: 168 Color: 11

Bin 67: 7 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 9
Size: 1466 Color: 1
Size: 136 Color: 11

Bin 68: 7 of cap free
Amount of items: 3
Items: 
Size: 7510 Color: 13
Size: 833 Color: 6
Size: 2 Color: 12

Bin 69: 8 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 16
Size: 3358 Color: 3
Size: 208 Color: 12

Bin 70: 9 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 18
Size: 1164 Color: 4
Size: 296 Color: 16

Bin 71: 9 of cap free
Amount of items: 2
Items: 
Size: 7085 Color: 19
Size: 1258 Color: 0

Bin 72: 10 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 14
Size: 1662 Color: 15

Bin 73: 10 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 1
Size: 876 Color: 13

Bin 74: 11 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 1
Size: 2660 Color: 6
Size: 176 Color: 16

Bin 75: 11 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 10
Size: 1036 Color: 11

Bin 76: 12 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 5
Size: 1650 Color: 16
Size: 174 Color: 1

Bin 77: 13 of cap free
Amount of items: 3
Items: 
Size: 5212 Color: 6
Size: 2691 Color: 8
Size: 436 Color: 3

Bin 78: 14 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 17
Size: 2142 Color: 16
Size: 440 Color: 11

Bin 79: 14 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 11
Size: 1372 Color: 0

Bin 80: 14 of cap free
Amount of items: 2
Items: 
Size: 7422 Color: 1
Size: 916 Color: 8

Bin 81: 15 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 9
Size: 3431 Color: 7
Size: 144 Color: 0

Bin 82: 16 of cap free
Amount of items: 30
Items: 
Size: 412 Color: 17
Size: 400 Color: 1
Size: 388 Color: 11
Size: 376 Color: 18
Size: 362 Color: 3
Size: 360 Color: 6
Size: 348 Color: 6
Size: 336 Color: 11
Size: 332 Color: 18
Size: 320 Color: 17
Size: 310 Color: 1
Size: 296 Color: 19
Size: 292 Color: 2
Size: 284 Color: 13
Size: 268 Color: 3
Size: 260 Color: 14
Size: 256 Color: 16
Size: 244 Color: 12
Size: 240 Color: 16
Size: 232 Color: 15
Size: 232 Color: 14
Size: 224 Color: 17
Size: 224 Color: 6
Size: 216 Color: 9
Size: 216 Color: 0
Size: 204 Color: 15
Size: 200 Color: 15
Size: 184 Color: 13
Size: 168 Color: 12
Size: 152 Color: 19

Bin 83: 16 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 18
Size: 1924 Color: 3

Bin 84: 16 of cap free
Amount of items: 2
Items: 
Size: 6650 Color: 6
Size: 1686 Color: 12

Bin 85: 16 of cap free
Amount of items: 2
Items: 
Size: 7033 Color: 4
Size: 1303 Color: 5

Bin 86: 17 of cap free
Amount of items: 3
Items: 
Size: 4803 Color: 15
Size: 2994 Color: 2
Size: 538 Color: 7

Bin 87: 17 of cap free
Amount of items: 2
Items: 
Size: 6252 Color: 19
Size: 2083 Color: 3

Bin 88: 17 of cap free
Amount of items: 2
Items: 
Size: 7110 Color: 18
Size: 1225 Color: 19

Bin 89: 18 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 12
Size: 2530 Color: 0
Size: 474 Color: 13

Bin 90: 18 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 4
Size: 1828 Color: 9
Size: 166 Color: 1

Bin 91: 18 of cap free
Amount of items: 2
Items: 
Size: 7129 Color: 15
Size: 1205 Color: 6

Bin 92: 19 of cap free
Amount of items: 3
Items: 
Size: 6330 Color: 5
Size: 1815 Color: 13
Size: 188 Color: 7

Bin 93: 19 of cap free
Amount of items: 2
Items: 
Size: 7276 Color: 3
Size: 1057 Color: 1

Bin 94: 21 of cap free
Amount of items: 16
Items: 
Size: 804 Color: 4
Size: 771 Color: 9
Size: 746 Color: 2
Size: 742 Color: 4
Size: 692 Color: 8
Size: 528 Color: 1
Size: 520 Color: 5
Size: 520 Color: 2
Size: 496 Color: 7
Size: 472 Color: 8
Size: 440 Color: 0
Size: 400 Color: 12
Size: 376 Color: 6
Size: 376 Color: 3
Size: 288 Color: 13
Size: 160 Color: 14

Bin 95: 22 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 2
Size: 1882 Color: 10
Size: 106 Color: 19

Bin 96: 23 of cap free
Amount of items: 2
Items: 
Size: 7213 Color: 3
Size: 1116 Color: 11

Bin 97: 24 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 11
Size: 3656 Color: 7
Size: 332 Color: 12

Bin 98: 24 of cap free
Amount of items: 2
Items: 
Size: 5476 Color: 17
Size: 2852 Color: 7

Bin 99: 24 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 13
Size: 1580 Color: 19
Size: 144 Color: 16

Bin 100: 24 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 0
Size: 1422 Color: 12
Size: 280 Color: 11

Bin 101: 24 of cap free
Amount of items: 2
Items: 
Size: 6708 Color: 15
Size: 1620 Color: 12

Bin 102: 24 of cap free
Amount of items: 2
Items: 
Size: 6846 Color: 3
Size: 1482 Color: 14

Bin 103: 24 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 14
Size: 1364 Color: 3

Bin 104: 25 of cap free
Amount of items: 2
Items: 
Size: 7363 Color: 9
Size: 964 Color: 8

Bin 105: 27 of cap free
Amount of items: 2
Items: 
Size: 4181 Color: 2
Size: 4144 Color: 12

Bin 106: 29 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 9
Size: 3348 Color: 5
Size: 228 Color: 2

Bin 107: 30 of cap free
Amount of items: 2
Items: 
Size: 5709 Color: 7
Size: 2613 Color: 1

Bin 108: 30 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 3
Size: 1204 Color: 10

Bin 109: 32 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 15
Size: 3532 Color: 14
Size: 600 Color: 5

Bin 110: 34 of cap free
Amount of items: 2
Items: 
Size: 7458 Color: 7
Size: 860 Color: 6

Bin 111: 35 of cap free
Amount of items: 2
Items: 
Size: 6374 Color: 4
Size: 1943 Color: 6

Bin 112: 35 of cap free
Amount of items: 2
Items: 
Size: 6705 Color: 19
Size: 1612 Color: 2

Bin 113: 36 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 11
Size: 3482 Color: 9
Size: 640 Color: 13

Bin 114: 38 of cap free
Amount of items: 2
Items: 
Size: 7412 Color: 6
Size: 902 Color: 5

Bin 115: 44 of cap free
Amount of items: 2
Items: 
Size: 5460 Color: 17
Size: 2848 Color: 5

Bin 116: 44 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 17
Size: 1288 Color: 19

Bin 117: 47 of cap free
Amount of items: 2
Items: 
Size: 6824 Color: 9
Size: 1481 Color: 16

Bin 118: 52 of cap free
Amount of items: 2
Items: 
Size: 6082 Color: 12
Size: 2218 Color: 2

Bin 119: 55 of cap free
Amount of items: 2
Items: 
Size: 6094 Color: 12
Size: 2203 Color: 19

Bin 120: 55 of cap free
Amount of items: 4
Items: 
Size: 6267 Color: 3
Size: 1094 Color: 17
Size: 504 Color: 16
Size: 432 Color: 18

Bin 121: 58 of cap free
Amount of items: 3
Items: 
Size: 5786 Color: 19
Size: 2164 Color: 18
Size: 344 Color: 7

Bin 122: 63 of cap free
Amount of items: 2
Items: 
Size: 5916 Color: 17
Size: 2373 Color: 4

Bin 123: 69 of cap free
Amount of items: 2
Items: 
Size: 5879 Color: 11
Size: 2404 Color: 17

Bin 124: 78 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 9
Size: 1699 Color: 6

Bin 125: 82 of cap free
Amount of items: 2
Items: 
Size: 5362 Color: 16
Size: 2908 Color: 18

Bin 126: 84 of cap free
Amount of items: 3
Items: 
Size: 5762 Color: 6
Size: 2162 Color: 2
Size: 344 Color: 14

Bin 127: 96 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 3
Size: 3348 Color: 10
Size: 160 Color: 1

Bin 128: 100 of cap free
Amount of items: 2
Items: 
Size: 6358 Color: 19
Size: 1894 Color: 0

Bin 129: 100 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 8
Size: 1678 Color: 6

Bin 130: 108 of cap free
Amount of items: 6
Items: 
Size: 4177 Color: 3
Size: 1084 Color: 17
Size: 890 Color: 7
Size: 799 Color: 12
Size: 718 Color: 13
Size: 576 Color: 18

Bin 131: 110 of cap free
Amount of items: 3
Items: 
Size: 4185 Color: 16
Size: 3477 Color: 7
Size: 580 Color: 8

Bin 132: 125 of cap free
Amount of items: 2
Items: 
Size: 5123 Color: 16
Size: 3104 Color: 9

Bin 133: 5906 of cap free
Amount of items: 14
Items: 
Size: 216 Color: 9
Size: 210 Color: 5
Size: 208 Color: 0
Size: 200 Color: 18
Size: 184 Color: 14
Size: 174 Color: 19
Size: 168 Color: 3
Size: 160 Color: 11
Size: 160 Color: 4
Size: 160 Color: 1
Size: 158 Color: 15
Size: 152 Color: 13
Size: 148 Color: 12
Size: 148 Color: 2

Total size: 1102464
Total free space: 8352

