Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 11
Size: 373 Color: 19
Size: 263 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 6
Size: 332 Color: 6
Size: 256 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 8
Size: 280 Color: 19
Size: 275 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 319 Color: 19
Size: 252 Color: 13

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 2
Size: 359 Color: 1
Size: 261 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 369 Color: 4
Size: 262 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 18
Size: 316 Color: 12
Size: 308 Color: 13

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 6
Size: 343 Color: 13
Size: 294 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 307 Color: 6
Size: 276 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 11
Size: 319 Color: 6
Size: 304 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 15
Size: 304 Color: 3
Size: 254 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 13
Size: 289 Color: 14
Size: 258 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 5
Size: 304 Color: 7
Size: 253 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 337 Color: 13
Size: 266 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 2
Size: 263 Color: 6
Size: 254 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 14
Size: 288 Color: 4
Size: 261 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 12
Size: 273 Color: 1
Size: 259 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 17
Size: 351 Color: 17
Size: 258 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 10
Size: 298 Color: 3
Size: 276 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 6
Size: 359 Color: 8
Size: 259 Color: 9

Total size: 20000
Total free space: 0

