Capicity Bin: 16224
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 8
Items: 
Size: 8116 Color: 16
Size: 1348 Color: 8
Size: 1344 Color: 17
Size: 1344 Color: 2
Size: 1280 Color: 3
Size: 1176 Color: 11
Size: 1136 Color: 17
Size: 480 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8136 Color: 11
Size: 6744 Color: 6
Size: 1344 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 8648 Color: 12
Size: 7252 Color: 5
Size: 324 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 11
Size: 6760 Color: 5
Size: 704 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9154 Color: 3
Size: 5894 Color: 3
Size: 1176 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9138 Color: 17
Size: 6746 Color: 10
Size: 340 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 14
Size: 5107 Color: 4
Size: 280 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11134 Color: 15
Size: 4242 Color: 2
Size: 848 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 12
Size: 4680 Color: 10
Size: 272 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 1
Size: 4608 Color: 19
Size: 284 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 14
Size: 4056 Color: 11
Size: 800 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 6
Size: 4424 Color: 2
Size: 336 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 1
Size: 3976 Color: 1
Size: 174 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12801 Color: 9
Size: 2853 Color: 17
Size: 570 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 3
Size: 2924 Color: 10
Size: 296 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 13
Size: 2598 Color: 1
Size: 600 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13132 Color: 11
Size: 2580 Color: 13
Size: 512 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13282 Color: 10
Size: 2578 Color: 18
Size: 364 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13245 Color: 4
Size: 1501 Color: 16
Size: 1478 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 0
Size: 2334 Color: 3
Size: 464 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 1
Size: 2242 Color: 14
Size: 448 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 4
Size: 2072 Color: 4
Size: 608 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13623 Color: 18
Size: 1857 Color: 9
Size: 744 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 0
Size: 2356 Color: 17
Size: 120 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 8
Size: 2048 Color: 15
Size: 424 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13753 Color: 12
Size: 2169 Color: 1
Size: 302 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 19
Size: 1448 Color: 6
Size: 944 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 5
Size: 2052 Color: 8
Size: 328 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 3
Size: 2066 Color: 10
Size: 288 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 11
Size: 1924 Color: 13
Size: 376 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13949 Color: 12
Size: 1595 Color: 15
Size: 680 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14008 Color: 11
Size: 1752 Color: 13
Size: 464 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14018 Color: 8
Size: 1654 Color: 1
Size: 552 Color: 8

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 7
Size: 1350 Color: 2
Size: 760 Color: 11

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 12
Size: 1692 Color: 5
Size: 410 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14188 Color: 6
Size: 1604 Color: 14
Size: 432 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 2
Size: 1522 Color: 6
Size: 508 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14282 Color: 18
Size: 1402 Color: 10
Size: 540 Color: 10

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 13
Size: 1428 Color: 6
Size: 512 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14292 Color: 6
Size: 1528 Color: 1
Size: 404 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 4
Size: 1620 Color: 9
Size: 292 Color: 17

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14320 Color: 13
Size: 1416 Color: 3
Size: 488 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 12
Size: 1184 Color: 6
Size: 706 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14392 Color: 3
Size: 1460 Color: 16
Size: 372 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 1
Size: 1439 Color: 18
Size: 378 Color: 5

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 15
Size: 1484 Color: 17
Size: 288 Color: 13

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 6
Size: 1422 Color: 18
Size: 348 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14461 Color: 5
Size: 1371 Color: 19
Size: 392 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 17
Size: 1442 Color: 19
Size: 284 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14499 Color: 0
Size: 1421 Color: 2
Size: 304 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 4
Size: 864 Color: 1
Size: 844 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14542 Color: 8
Size: 1348 Color: 17
Size: 334 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14563 Color: 15
Size: 1385 Color: 13
Size: 276 Color: 9

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 4
Size: 1248 Color: 19
Size: 408 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 8117 Color: 17
Size: 6742 Color: 13
Size: 1364 Color: 16

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 9119 Color: 1
Size: 6748 Color: 11
Size: 356 Color: 8

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 9236 Color: 7
Size: 5891 Color: 10
Size: 1096 Color: 13

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 1
Size: 6723 Color: 12
Size: 184 Color: 9

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 10829 Color: 7
Size: 5130 Color: 14
Size: 264 Color: 11

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 10
Size: 3627 Color: 1
Size: 412 Color: 12

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 8
Size: 3527 Color: 15
Size: 388 Color: 2

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13127 Color: 19
Size: 2070 Color: 17
Size: 1026 Color: 8

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13208 Color: 17
Size: 2031 Color: 6
Size: 984 Color: 9

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13447 Color: 11
Size: 1672 Color: 16
Size: 1104 Color: 13

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 7
Size: 2211 Color: 19
Size: 456 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 8
Size: 1751 Color: 14
Size: 816 Color: 8

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 14125 Color: 3
Size: 1458 Color: 16
Size: 640 Color: 1

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 19
Size: 2061 Color: 18

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 14215 Color: 13
Size: 2008 Color: 8

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 14391 Color: 12
Size: 1038 Color: 18
Size: 794 Color: 8

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14578 Color: 0
Size: 1645 Color: 7

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 2
Size: 5350 Color: 14
Size: 384 Color: 0

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11162 Color: 4
Size: 3512 Color: 16
Size: 1548 Color: 14

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11742 Color: 17
Size: 3832 Color: 8
Size: 648 Color: 8

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 10
Size: 2562 Color: 6
Size: 784 Color: 4

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 15
Size: 2776 Color: 19
Size: 292 Color: 2

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13404 Color: 12
Size: 2818 Color: 16

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 3
Size: 2416 Color: 7
Size: 246 Color: 1

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 13994 Color: 0
Size: 2228 Color: 13

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 4
Size: 1486 Color: 1
Size: 540 Color: 7

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 6
Size: 1842 Color: 8

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 14
Size: 1700 Color: 5

Bin 83: 3 of cap free
Amount of items: 7
Items: 
Size: 8120 Color: 14
Size: 1471 Color: 3
Size: 1384 Color: 18
Size: 1374 Color: 2
Size: 1348 Color: 12
Size: 1344 Color: 7
Size: 1180 Color: 17

Bin 84: 3 of cap free
Amount of items: 5
Items: 
Size: 8130 Color: 13
Size: 5127 Color: 7
Size: 1922 Color: 10
Size: 856 Color: 9
Size: 186 Color: 10

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 9989 Color: 10
Size: 6232 Color: 4

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 11146 Color: 5
Size: 4899 Color: 9
Size: 176 Color: 11

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 0
Size: 3109 Color: 16

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 5
Size: 2315 Color: 8

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 13953 Color: 14
Size: 2068 Color: 1
Size: 200 Color: 5

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 0
Size: 2224 Color: 9

Bin 91: 3 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 10
Size: 1866 Color: 0

Bin 92: 4 of cap free
Amount of items: 5
Items: 
Size: 8113 Color: 17
Size: 4234 Color: 18
Size: 1862 Color: 11
Size: 1515 Color: 19
Size: 496 Color: 6

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 11985 Color: 9
Size: 3971 Color: 6
Size: 264 Color: 15

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 12758 Color: 16
Size: 3462 Color: 10

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 4
Size: 2904 Color: 1

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 7
Size: 2456 Color: 12

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 5
Size: 1988 Color: 7

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 14372 Color: 16
Size: 1848 Color: 9

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 11658 Color: 9
Size: 4561 Color: 14

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 12619 Color: 7
Size: 3368 Color: 5
Size: 232 Color: 16

Bin 101: 6 of cap free
Amount of items: 23
Items: 
Size: 912 Color: 11
Size: 910 Color: 6
Size: 898 Color: 13
Size: 896 Color: 18
Size: 888 Color: 17
Size: 860 Color: 9
Size: 840 Color: 4
Size: 808 Color: 2
Size: 800 Color: 8
Size: 792 Color: 4
Size: 744 Color: 3
Size: 688 Color: 16
Size: 688 Color: 5
Size: 672 Color: 13
Size: 632 Color: 16
Size: 624 Color: 11
Size: 624 Color: 10
Size: 584 Color: 8
Size: 584 Color: 7
Size: 480 Color: 0
Size: 452 Color: 6
Size: 450 Color: 0
Size: 392 Color: 12

Bin 102: 6 of cap free
Amount of items: 11
Items: 
Size: 8114 Color: 14
Size: 1044 Color: 12
Size: 1024 Color: 17
Size: 1024 Color: 10
Size: 992 Color: 9
Size: 948 Color: 17
Size: 944 Color: 9
Size: 928 Color: 2
Size: 496 Color: 0
Size: 368 Color: 4
Size: 336 Color: 18

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 9974 Color: 7
Size: 6244 Color: 1

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 10546 Color: 4
Size: 4792 Color: 6
Size: 880 Color: 12

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 12978 Color: 0
Size: 3240 Color: 13

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 10
Size: 2278 Color: 1
Size: 272 Color: 11

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 11
Size: 2232 Color: 17

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 11
Size: 2152 Color: 5

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 14596 Color: 7
Size: 1622 Color: 8

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 12
Size: 2581 Color: 7
Size: 348 Color: 10

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 9
Size: 2483 Color: 10

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 13789 Color: 2
Size: 2428 Color: 5

Bin 113: 8 of cap free
Amount of items: 4
Items: 
Size: 8138 Color: 2
Size: 6754 Color: 19
Size: 1072 Color: 9
Size: 252 Color: 12

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 12478 Color: 9
Size: 3738 Color: 0

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 19
Size: 3120 Color: 17
Size: 136 Color: 15

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 11
Size: 3134 Color: 14

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 13
Size: 2212 Color: 19

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 14084 Color: 17
Size: 2132 Color: 2

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 11993 Color: 16
Size: 4222 Color: 3

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 0
Size: 5906 Color: 1

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 12648 Color: 19
Size: 2666 Color: 16
Size: 900 Color: 14

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 14136 Color: 13
Size: 2078 Color: 10

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 9157 Color: 15
Size: 7056 Color: 18

Bin 124: 11 of cap free
Amount of items: 2
Items: 
Size: 11467 Color: 1
Size: 4746 Color: 18

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 11716 Color: 0
Size: 4497 Color: 19

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 14251 Color: 14
Size: 1962 Color: 9

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 14519 Color: 13
Size: 1694 Color: 7

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 9528 Color: 2
Size: 6684 Color: 7

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 9908 Color: 5
Size: 6048 Color: 6
Size: 256 Color: 18

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 10720 Color: 13
Size: 5492 Color: 12

Bin 131: 12 of cap free
Amount of items: 3
Items: 
Size: 10868 Color: 7
Size: 4932 Color: 0
Size: 412 Color: 6

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 5
Size: 1749 Color: 1
Size: 1559 Color: 15

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 14
Size: 2454 Color: 15

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 14311 Color: 17
Size: 1900 Color: 2

Bin 135: 13 of cap free
Amount of items: 2
Items: 
Size: 14423 Color: 4
Size: 1788 Color: 10

Bin 136: 13 of cap free
Amount of items: 2
Items: 
Size: 14536 Color: 18
Size: 1675 Color: 6

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 5
Size: 1722 Color: 7

Bin 138: 15 of cap free
Amount of items: 3
Items: 
Size: 12429 Color: 9
Size: 3492 Color: 19
Size: 288 Color: 14

Bin 139: 17 of cap free
Amount of items: 3
Items: 
Size: 12466 Color: 12
Size: 3533 Color: 1
Size: 208 Color: 8

Bin 140: 18 of cap free
Amount of items: 3
Items: 
Size: 9990 Color: 1
Size: 5764 Color: 15
Size: 452 Color: 17

Bin 141: 18 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 12
Size: 2520 Color: 9

Bin 142: 19 of cap free
Amount of items: 2
Items: 
Size: 10073 Color: 19
Size: 6132 Color: 1

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 13494 Color: 19
Size: 2711 Color: 17

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 13579 Color: 6
Size: 2626 Color: 11

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 14442 Color: 16
Size: 1762 Color: 6

Bin 146: 21 of cap free
Amount of items: 2
Items: 
Size: 9959 Color: 16
Size: 6244 Color: 4

Bin 147: 21 of cap free
Amount of items: 3
Items: 
Size: 10065 Color: 12
Size: 5768 Color: 19
Size: 370 Color: 9

Bin 148: 21 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 5
Size: 3965 Color: 2
Size: 310 Color: 14

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 10981 Color: 16
Size: 5221 Color: 14

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 14127 Color: 7
Size: 2075 Color: 8

Bin 151: 22 of cap free
Amount of items: 2
Items: 
Size: 14308 Color: 0
Size: 1894 Color: 6

Bin 152: 23 of cap free
Amount of items: 2
Items: 
Size: 14581 Color: 9
Size: 1620 Color: 15

Bin 153: 24 of cap free
Amount of items: 2
Items: 
Size: 14398 Color: 15
Size: 1802 Color: 0

Bin 154: 25 of cap free
Amount of items: 2
Items: 
Size: 14306 Color: 16
Size: 1893 Color: 18

Bin 155: 26 of cap free
Amount of items: 3
Items: 
Size: 10070 Color: 18
Size: 5592 Color: 6
Size: 536 Color: 13

Bin 156: 27 of cap free
Amount of items: 2
Items: 
Size: 12929 Color: 8
Size: 3268 Color: 19

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 13074 Color: 19
Size: 3122 Color: 16

Bin 158: 30 of cap free
Amount of items: 2
Items: 
Size: 13510 Color: 1
Size: 2684 Color: 14

Bin 159: 31 of cap free
Amount of items: 2
Items: 
Size: 11459 Color: 6
Size: 4734 Color: 13

Bin 160: 33 of cap free
Amount of items: 2
Items: 
Size: 13395 Color: 8
Size: 2796 Color: 14

Bin 161: 33 of cap free
Amount of items: 3
Items: 
Size: 14029 Color: 0
Size: 2118 Color: 15
Size: 44 Color: 12

Bin 162: 33 of cap free
Amount of items: 2
Items: 
Size: 14478 Color: 15
Size: 1713 Color: 14

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 11057 Color: 11
Size: 5133 Color: 14

Bin 164: 34 of cap free
Amount of items: 2
Items: 
Size: 13106 Color: 17
Size: 3084 Color: 0

Bin 165: 35 of cap free
Amount of items: 3
Items: 
Size: 8732 Color: 1
Size: 6761 Color: 17
Size: 696 Color: 3

Bin 166: 36 of cap free
Amount of items: 2
Items: 
Size: 14476 Color: 6
Size: 1712 Color: 16

Bin 167: 37 of cap free
Amount of items: 2
Items: 
Size: 12423 Color: 18
Size: 3764 Color: 4

Bin 168: 38 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 18
Size: 3558 Color: 4
Size: 96 Color: 10

Bin 169: 44 of cap free
Amount of items: 40
Items: 
Size: 576 Color: 18
Size: 576 Color: 13
Size: 576 Color: 10
Size: 576 Color: 4
Size: 532 Color: 15
Size: 524 Color: 2
Size: 516 Color: 14
Size: 516 Color: 1
Size: 488 Color: 9
Size: 464 Color: 15
Size: 462 Color: 13
Size: 440 Color: 15
Size: 440 Color: 6
Size: 434 Color: 17
Size: 432 Color: 9
Size: 432 Color: 2
Size: 420 Color: 14
Size: 416 Color: 13
Size: 408 Color: 8
Size: 408 Color: 6
Size: 400 Color: 1
Size: 384 Color: 16
Size: 384 Color: 2
Size: 368 Color: 7
Size: 364 Color: 7
Size: 352 Color: 0
Size: 348 Color: 16
Size: 336 Color: 11
Size: 336 Color: 4
Size: 320 Color: 12
Size: 320 Color: 6
Size: 320 Color: 4
Size: 320 Color: 1
Size: 304 Color: 19
Size: 304 Color: 19
Size: 304 Color: 14
Size: 304 Color: 12
Size: 304 Color: 5
Size: 280 Color: 0
Size: 192 Color: 10

Bin 170: 44 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 0
Size: 2262 Color: 14

Bin 171: 45 of cap free
Amount of items: 6
Items: 
Size: 8122 Color: 19
Size: 1846 Color: 0
Size: 1831 Color: 9
Size: 1608 Color: 15
Size: 1540 Color: 11
Size: 1232 Color: 18

Bin 172: 45 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 6
Size: 7723 Color: 14

Bin 173: 48 of cap free
Amount of items: 2
Items: 
Size: 9320 Color: 4
Size: 6856 Color: 8

Bin 174: 48 of cap free
Amount of items: 3
Items: 
Size: 12142 Color: 10
Size: 2890 Color: 2
Size: 1144 Color: 6

Bin 175: 49 of cap free
Amount of items: 2
Items: 
Size: 13170 Color: 9
Size: 3005 Color: 16

Bin 176: 50 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 13
Size: 2602 Color: 14

Bin 177: 51 of cap free
Amount of items: 2
Items: 
Size: 8157 Color: 7
Size: 8016 Color: 19

Bin 178: 52 of cap free
Amount of items: 2
Items: 
Size: 12036 Color: 1
Size: 4136 Color: 5

Bin 179: 53 of cap free
Amount of items: 2
Items: 
Size: 13571 Color: 17
Size: 2600 Color: 15

Bin 180: 62 of cap free
Amount of items: 3
Items: 
Size: 12716 Color: 4
Size: 2902 Color: 6
Size: 544 Color: 1

Bin 181: 63 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 6
Size: 4468 Color: 12
Size: 1941 Color: 9

Bin 182: 64 of cap free
Amount of items: 2
Items: 
Size: 13454 Color: 14
Size: 2706 Color: 19

Bin 183: 66 of cap free
Amount of items: 2
Items: 
Size: 9396 Color: 1
Size: 6762 Color: 6

Bin 184: 67 of cap free
Amount of items: 3
Items: 
Size: 12434 Color: 18
Size: 2379 Color: 11
Size: 1344 Color: 6

Bin 185: 68 of cap free
Amount of items: 3
Items: 
Size: 9636 Color: 5
Size: 6328 Color: 4
Size: 192 Color: 17

Bin 186: 71 of cap free
Amount of items: 2
Items: 
Size: 10753 Color: 19
Size: 5400 Color: 1

Bin 187: 74 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 8
Size: 3806 Color: 12

Bin 188: 75 of cap free
Amount of items: 2
Items: 
Size: 13742 Color: 7
Size: 2407 Color: 5

Bin 189: 78 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 0
Size: 3402 Color: 2

Bin 190: 78 of cap free
Amount of items: 2
Items: 
Size: 12746 Color: 2
Size: 3400 Color: 0

Bin 191: 80 of cap free
Amount of items: 2
Items: 
Size: 10616 Color: 15
Size: 5528 Color: 0

Bin 192: 81 of cap free
Amount of items: 2
Items: 
Size: 13895 Color: 19
Size: 2248 Color: 2

Bin 193: 88 of cap free
Amount of items: 2
Items: 
Size: 12973 Color: 18
Size: 3163 Color: 12

Bin 194: 107 of cap free
Amount of items: 2
Items: 
Size: 10920 Color: 19
Size: 5197 Color: 12

Bin 195: 113 of cap free
Amount of items: 3
Items: 
Size: 8132 Color: 10
Size: 5921 Color: 15
Size: 2058 Color: 3

Bin 196: 116 of cap free
Amount of items: 2
Items: 
Size: 12024 Color: 13
Size: 4084 Color: 0

Bin 197: 139 of cap free
Amount of items: 3
Items: 
Size: 10530 Color: 14
Size: 4307 Color: 7
Size: 1248 Color: 10

Bin 198: 174 of cap free
Amount of items: 2
Items: 
Size: 10120 Color: 2
Size: 5930 Color: 10

Bin 199: 12784 of cap free
Amount of items: 12
Items: 
Size: 318 Color: 1
Size: 312 Color: 11
Size: 312 Color: 3
Size: 304 Color: 16
Size: 304 Color: 6
Size: 300 Color: 19
Size: 288 Color: 12
Size: 286 Color: 14
Size: 280 Color: 0
Size: 272 Color: 9
Size: 272 Color: 5
Size: 192 Color: 10

Total size: 3212352
Total free space: 16224

