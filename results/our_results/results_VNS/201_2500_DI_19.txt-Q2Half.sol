Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 1
Size: 731 Color: 1
Size: 146 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1461 Color: 1
Size: 239 Color: 1
Size: 220 Color: 1
Size: 72 Color: 0
Size: 56 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 1
Size: 472 Color: 1
Size: 160 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 1
Size: 362 Color: 1
Size: 12 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 1
Size: 853 Color: 1
Size: 170 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1386 Color: 1
Size: 494 Color: 1
Size: 168 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 1
Size: 605 Color: 1
Size: 60 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1614 Color: 1
Size: 388 Color: 1
Size: 46 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 1
Size: 406 Color: 1
Size: 140 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1575 Color: 1
Size: 353 Color: 1
Size: 120 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1029 Color: 1
Size: 851 Color: 1
Size: 96 Color: 0
Size: 72 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 1
Size: 493 Color: 1
Size: 140 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 316 Color: 1
Size: 78 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1714 Color: 1
Size: 282 Color: 1
Size: 52 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1625 Color: 1
Size: 313 Color: 1
Size: 110 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 1
Size: 215 Color: 1
Size: 132 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 1
Size: 491 Color: 1
Size: 100 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 1
Size: 424 Color: 1
Size: 42 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 1
Size: 302 Color: 1
Size: 56 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 190 Color: 1
Size: 44 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 198 Color: 1
Size: 40 Color: 0

Bin 22: 0 of cap free
Amount of items: 5
Items: 
Size: 1298 Color: 1
Size: 414 Color: 1
Size: 202 Color: 1
Size: 98 Color: 0
Size: 36 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 1
Size: 235 Color: 1
Size: 42 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1374 Color: 1
Size: 554 Color: 1
Size: 120 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 1
Size: 607 Color: 1
Size: 62 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 1
Size: 439 Color: 1
Size: 86 Color: 0

Bin 27: 0 of cap free
Amount of items: 5
Items: 
Size: 828 Color: 1
Size: 562 Color: 1
Size: 498 Color: 1
Size: 112 Color: 0
Size: 48 Color: 0

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 1663 Color: 1
Size: 271 Color: 1
Size: 62 Color: 0
Size: 52 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 398 Color: 1
Size: 76 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 442 Color: 1
Size: 84 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 1
Size: 706 Color: 1
Size: 62 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1754 Color: 1
Size: 274 Color: 1
Size: 20 Color: 0

Bin 33: 0 of cap free
Amount of items: 4
Items: 
Size: 1579 Color: 1
Size: 351 Color: 1
Size: 78 Color: 0
Size: 40 Color: 0

Bin 34: 0 of cap free
Amount of items: 7
Items: 
Size: 696 Color: 1
Size: 559 Color: 1
Size: 321 Color: 1
Size: 246 Color: 1
Size: 80 Color: 0
Size: 76 Color: 0
Size: 70 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1703 Color: 1
Size: 289 Color: 1
Size: 56 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 1
Size: 522 Color: 1
Size: 68 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 1086 Color: 1
Size: 529 Color: 1
Size: 321 Color: 1
Size: 76 Color: 0
Size: 36 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 1
Size: 274 Color: 1
Size: 4 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 1
Size: 735 Color: 1
Size: 146 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1026 Color: 1
Size: 854 Color: 1
Size: 168 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1583 Color: 1
Size: 389 Color: 1
Size: 76 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 1
Size: 330 Color: 1
Size: 52 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 1
Size: 314 Color: 1
Size: 8 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 1
Size: 802 Color: 1
Size: 46 Color: 0

Bin 45: 1 of cap free
Amount of items: 4
Items: 
Size: 1163 Color: 1
Size: 682 Color: 1
Size: 146 Color: 0
Size: 56 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1321 Color: 1
Size: 630 Color: 1
Size: 96 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 1
Size: 709 Color: 1
Size: 104 Color: 0

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 1
Size: 637 Color: 1
Size: 208 Color: 0

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 1
Size: 261 Color: 1
Size: 64 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 1
Size: 395 Color: 1
Size: 64 Color: 0

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 1
Size: 291 Color: 1
Size: 126 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 1
Size: 231 Color: 1
Size: 48 Color: 0

Bin 53: 3 of cap free
Amount of items: 5
Items: 
Size: 1426 Color: 1
Size: 317 Color: 1
Size: 202 Color: 1
Size: 52 Color: 0
Size: 48 Color: 0

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 1
Size: 194 Color: 1
Size: 60 Color: 0

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 1
Size: 211 Color: 1
Size: 36 Color: 0

Bin 56: 8 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 1
Size: 477 Color: 1
Size: 36 Color: 0

Bin 57: 9 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 258 Color: 1
Size: 108 Color: 0

Bin 58: 16 of cap free
Amount of items: 3
Items: 
Size: 1285 Color: 1
Size: 739 Color: 1
Size: 8 Color: 0

Bin 59: 49 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 1
Size: 391 Color: 1
Size: 46 Color: 0

Bin 60: 226 of cap free
Amount of items: 1
Items: 
Size: 1822 Color: 1

Bin 61: 230 of cap free
Amount of items: 1
Items: 
Size: 1818 Color: 1

Bin 62: 242 of cap free
Amount of items: 1
Items: 
Size: 1806 Color: 1

Bin 63: 273 of cap free
Amount of items: 1
Items: 
Size: 1775 Color: 1

Bin 64: 285 of cap free
Amount of items: 1
Items: 
Size: 1763 Color: 1

Bin 65: 306 of cap free
Amount of items: 1
Items: 
Size: 1742 Color: 1

Bin 66: 379 of cap free
Amount of items: 1
Items: 
Size: 1669 Color: 1

Total size: 133120
Total free space: 2048

