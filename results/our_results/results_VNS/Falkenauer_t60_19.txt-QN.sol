Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 45
Size: 350 Color: 36
Size: 259 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 50
Size: 324 Color: 31
Size: 253 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 44
Size: 358 Color: 37
Size: 258 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 46
Size: 342 Color: 34
Size: 263 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 47
Size: 343 Color: 35
Size: 252 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 40
Size: 342 Color: 33
Size: 292 Color: 26

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 41
Size: 363 Color: 39
Size: 270 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 49
Size: 298 Color: 27
Size: 287 Color: 24

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 53
Size: 286 Color: 23
Size: 255 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 58
Size: 254 Color: 7
Size: 253 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 51
Size: 316 Color: 30
Size: 257 Color: 10

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 56
Size: 276 Color: 20
Size: 254 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 55
Size: 279 Color: 21
Size: 261 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 52
Size: 282 Color: 22
Size: 259 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 48
Size: 305 Color: 29
Size: 288 Color: 25

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 57
Size: 261 Color: 14
Size: 251 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 43
Size: 361 Color: 38
Size: 257 Color: 9

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 59
Size: 251 Color: 2
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 42
Size: 329 Color: 32
Size: 303 Color: 28

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 54
Size: 273 Color: 19
Size: 267 Color: 17

Total size: 20000
Total free space: 0

