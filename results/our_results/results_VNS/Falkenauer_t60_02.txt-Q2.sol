Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 326 Color: 1
Size: 276 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 345 Color: 1
Size: 288 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 263 Color: 1
Size: 261 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 267 Color: 1
Size: 251 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 0
Size: 305 Color: 0
Size: 266 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 328 Color: 0
Size: 271 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 267 Color: 0
Size: 254 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 281 Color: 1
Size: 260 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 308 Color: 1
Size: 262 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 350 Color: 1
Size: 260 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 0
Size: 350 Color: 0
Size: 288 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 267 Color: 1
Size: 251 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 277 Color: 1
Size: 259 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 0
Size: 250 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 354 Color: 0
Size: 268 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 352 Color: 0
Size: 279 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 0
Size: 339 Color: 0
Size: 261 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 280 Color: 0
Size: 284 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 252 Color: 0
Size: 250 Color: 1

Total size: 20000
Total free space: 0

