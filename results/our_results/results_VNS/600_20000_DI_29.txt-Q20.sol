Capicity Bin: 16800
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8408 Color: 4
Size: 3790 Color: 18
Size: 3532 Color: 11
Size: 592 Color: 13
Size: 478 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8426 Color: 12
Size: 7842 Color: 6
Size: 532 Color: 19

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9368 Color: 4
Size: 7000 Color: 7
Size: 432 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9428 Color: 8
Size: 6986 Color: 11
Size: 386 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 19
Size: 6996 Color: 9
Size: 280 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10212 Color: 4
Size: 6220 Color: 15
Size: 368 Color: 8

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11574 Color: 12
Size: 3598 Color: 0
Size: 1628 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11964 Color: 3
Size: 4088 Color: 11
Size: 748 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12034 Color: 3
Size: 3830 Color: 0
Size: 936 Color: 7

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12247 Color: 7
Size: 3595 Color: 5
Size: 958 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12632 Color: 17
Size: 3544 Color: 9
Size: 624 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 7
Size: 3390 Color: 9
Size: 566 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13002 Color: 12
Size: 2062 Color: 5
Size: 1736 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 5
Size: 3480 Color: 4
Size: 304 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13054 Color: 17
Size: 3214 Color: 16
Size: 532 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 8
Size: 3400 Color: 0
Size: 340 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 1
Size: 3172 Color: 2
Size: 536 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 3
Size: 3378 Color: 12
Size: 304 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13124 Color: 9
Size: 2152 Color: 12
Size: 1524 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13226 Color: 8
Size: 2982 Color: 1
Size: 592 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13229 Color: 7
Size: 3185 Color: 3
Size: 386 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 2
Size: 3160 Color: 19
Size: 400 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 10
Size: 3092 Color: 4
Size: 436 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13293 Color: 16
Size: 3009 Color: 1
Size: 498 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13364 Color: 15
Size: 2484 Color: 10
Size: 952 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 5
Size: 3085 Color: 7
Size: 350 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 15
Size: 2239 Color: 15
Size: 1168 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13534 Color: 6
Size: 2466 Color: 10
Size: 800 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13408 Color: 5
Size: 2576 Color: 1
Size: 816 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13481 Color: 8
Size: 2767 Color: 13
Size: 552 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13677 Color: 6
Size: 2163 Color: 13
Size: 960 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 17
Size: 2044 Color: 9
Size: 1192 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13650 Color: 14
Size: 2414 Color: 1
Size: 736 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13744 Color: 12
Size: 2624 Color: 10
Size: 432 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13860 Color: 8
Size: 2020 Color: 11
Size: 920 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 11
Size: 2440 Color: 9
Size: 480 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13978 Color: 13
Size: 1598 Color: 10
Size: 1224 Color: 14

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14108 Color: 15
Size: 2452 Color: 1
Size: 240 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 14
Size: 2169 Color: 14
Size: 516 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14164 Color: 6
Size: 1768 Color: 5
Size: 868 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 11
Size: 2168 Color: 15
Size: 432 Color: 10

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 0
Size: 1510 Color: 16
Size: 960 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14342 Color: 4
Size: 1642 Color: 5
Size: 816 Color: 16

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 11
Size: 1688 Color: 6
Size: 756 Color: 19

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14412 Color: 10
Size: 1524 Color: 4
Size: 864 Color: 9

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14472 Color: 19
Size: 2244 Color: 13
Size: 84 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14475 Color: 7
Size: 1757 Color: 3
Size: 568 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14483 Color: 16
Size: 1533 Color: 6
Size: 784 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14508 Color: 9
Size: 2050 Color: 5
Size: 242 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14572 Color: 4
Size: 1592 Color: 17
Size: 636 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 7
Size: 1854 Color: 9
Size: 368 Color: 6

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14649 Color: 17
Size: 1833 Color: 15
Size: 318 Color: 5

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14656 Color: 2
Size: 1392 Color: 6
Size: 752 Color: 11

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14701 Color: 7
Size: 1595 Color: 9
Size: 504 Color: 19

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 2
Size: 1072 Color: 6
Size: 1000 Color: 18

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14806 Color: 19
Size: 1548 Color: 19
Size: 446 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14820 Color: 12
Size: 1512 Color: 1
Size: 468 Color: 6

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 11
Size: 1652 Color: 15
Size: 262 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14887 Color: 12
Size: 1505 Color: 10
Size: 408 Color: 13

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 6
Size: 1096 Color: 0
Size: 732 Color: 10

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14980 Color: 11
Size: 1056 Color: 12
Size: 764 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 15030 Color: 6
Size: 1406 Color: 9
Size: 364 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 15000 Color: 3
Size: 1464 Color: 5
Size: 336 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 2
Size: 1665 Color: 11
Size: 126 Color: 5

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 15048 Color: 16
Size: 1232 Color: 11
Size: 520 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 15069 Color: 16
Size: 1443 Color: 10
Size: 288 Color: 13

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 15070 Color: 17
Size: 1442 Color: 0
Size: 288 Color: 10

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 15076 Color: 14
Size: 1428 Color: 6
Size: 296 Color: 0

Bin 69: 1 of cap free
Amount of items: 7
Items: 
Size: 8404 Color: 10
Size: 2080 Color: 11
Size: 1928 Color: 15
Size: 1925 Color: 1
Size: 1842 Color: 6
Size: 320 Color: 4
Size: 300 Color: 0

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 9453 Color: 18
Size: 6994 Color: 11
Size: 352 Color: 7

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 5
Size: 2977 Color: 14
Size: 358 Color: 19

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 13775 Color: 13
Size: 2792 Color: 6
Size: 232 Color: 13

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 11
Size: 2603 Color: 6
Size: 304 Color: 11

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 14205 Color: 4
Size: 2244 Color: 6
Size: 350 Color: 8

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 14427 Color: 10
Size: 1396 Color: 8
Size: 976 Color: 6

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 14668 Color: 10
Size: 2131 Color: 7

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 14693 Color: 11
Size: 2106 Color: 5

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 14961 Color: 7
Size: 1080 Color: 18
Size: 758 Color: 19

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 9
Size: 6192 Color: 9
Size: 300 Color: 3

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 11662 Color: 14
Size: 3658 Color: 10
Size: 1478 Color: 15

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 11960 Color: 16
Size: 3910 Color: 10
Size: 928 Color: 15

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12251 Color: 18
Size: 4083 Color: 11
Size: 464 Color: 9

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 12390 Color: 13
Size: 4076 Color: 10
Size: 332 Color: 5

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 17
Size: 3750 Color: 9
Size: 320 Color: 6

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 14076 Color: 16
Size: 2722 Color: 1

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 14438 Color: 10
Size: 2360 Color: 3

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 18
Size: 1916 Color: 12
Size: 404 Color: 13

Bin 88: 3 of cap free
Amount of items: 8
Items: 
Size: 8402 Color: 17
Size: 1631 Color: 4
Size: 1518 Color: 17
Size: 1398 Color: 11
Size: 1396 Color: 18
Size: 1396 Color: 7
Size: 672 Color: 13
Size: 384 Color: 19

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 8413 Color: 6
Size: 7576 Color: 14
Size: 808 Color: 12

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 8696 Color: 19
Size: 6991 Color: 14
Size: 1110 Color: 6

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 9312 Color: 16
Size: 6993 Color: 1
Size: 492 Color: 11

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 10631 Color: 1
Size: 4774 Color: 18
Size: 1392 Color: 6

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 11204 Color: 5
Size: 5141 Color: 13
Size: 452 Color: 6

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 11665 Color: 15
Size: 4724 Color: 7
Size: 408 Color: 6

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 12843 Color: 0
Size: 3338 Color: 9
Size: 616 Color: 14

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 6
Size: 2329 Color: 17
Size: 1008 Color: 0

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 14007 Color: 7
Size: 1398 Color: 15
Size: 1392 Color: 6

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 11382 Color: 15
Size: 5414 Color: 11

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 6
Size: 3300 Color: 16
Size: 306 Color: 1

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 13374 Color: 6
Size: 3166 Color: 2
Size: 256 Color: 5

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 10
Size: 3124 Color: 12

Bin 102: 4 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 12
Size: 2968 Color: 14

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 16
Size: 2564 Color: 8

Bin 104: 4 of cap free
Amount of items: 2
Items: 
Size: 14275 Color: 5
Size: 2521 Color: 0

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 9066 Color: 7
Size: 7440 Color: 0
Size: 288 Color: 6

Bin 106: 6 of cap free
Amount of items: 3
Items: 
Size: 10082 Color: 1
Size: 6200 Color: 7
Size: 512 Color: 16

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 13
Size: 3684 Color: 8
Size: 856 Color: 7

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 12734 Color: 2
Size: 4060 Color: 3

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 13842 Color: 14
Size: 2952 Color: 11

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 14178 Color: 7
Size: 2616 Color: 10

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14590 Color: 10
Size: 2204 Color: 18

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 15092 Color: 15
Size: 1702 Color: 1

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 11381 Color: 17
Size: 5412 Color: 18

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 0
Size: 4281 Color: 2
Size: 616 Color: 2

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 11901 Color: 16
Size: 4668 Color: 6
Size: 224 Color: 12

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 12741 Color: 15
Size: 4052 Color: 10

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 11
Size: 2841 Color: 6
Size: 292 Color: 16

Bin 118: 7 of cap free
Amount of items: 2
Items: 
Size: 14365 Color: 8
Size: 2428 Color: 7

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 8504 Color: 18
Size: 8288 Color: 14

Bin 120: 8 of cap free
Amount of items: 3
Items: 
Size: 11172 Color: 2
Size: 5388 Color: 7
Size: 232 Color: 13

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 10
Size: 3068 Color: 1

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 13923 Color: 13
Size: 2868 Color: 10

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 15114 Color: 17
Size: 1669 Color: 5
Size: 8 Color: 19

Bin 124: 10 of cap free
Amount of items: 3
Items: 
Size: 11042 Color: 11
Size: 5492 Color: 2
Size: 256 Color: 6

Bin 125: 10 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 6
Size: 4040 Color: 6
Size: 336 Color: 7

Bin 126: 10 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 18
Size: 3299 Color: 16
Size: 512 Color: 7

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 9
Size: 2602 Color: 14

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 14804 Color: 18
Size: 1986 Color: 5

Bin 129: 11 of cap free
Amount of items: 9
Items: 
Size: 8401 Color: 19
Size: 1398 Color: 1
Size: 1396 Color: 7
Size: 1208 Color: 18
Size: 1136 Color: 2
Size: 1120 Color: 6
Size: 1116 Color: 4
Size: 550 Color: 13
Size: 464 Color: 15

Bin 130: 11 of cap free
Amount of items: 2
Items: 
Size: 14758 Color: 11
Size: 2031 Color: 10

Bin 131: 11 of cap free
Amount of items: 2
Items: 
Size: 14845 Color: 13
Size: 1944 Color: 2

Bin 132: 12 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 7
Size: 6148 Color: 14
Size: 300 Color: 15

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 12380 Color: 2
Size: 4408 Color: 18

Bin 134: 12 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 13
Size: 2988 Color: 9

Bin 135: 12 of cap free
Amount of items: 2
Items: 
Size: 14602 Color: 10
Size: 2186 Color: 8

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 14995 Color: 14
Size: 1793 Color: 3

Bin 137: 13 of cap free
Amount of items: 2
Items: 
Size: 12996 Color: 3
Size: 3791 Color: 4

Bin 138: 13 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 4
Size: 3105 Color: 9

Bin 139: 13 of cap free
Amount of items: 2
Items: 
Size: 14491 Color: 19
Size: 2296 Color: 18

Bin 140: 14 of cap free
Amount of items: 2
Items: 
Size: 14086 Color: 8
Size: 2700 Color: 4

Bin 141: 16 of cap free
Amount of items: 3
Items: 
Size: 10952 Color: 3
Size: 3678 Color: 11
Size: 2154 Color: 14

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 14674 Color: 16
Size: 2110 Color: 17

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 14199 Color: 11
Size: 2584 Color: 13

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 14948 Color: 18
Size: 1834 Color: 14

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 14990 Color: 9
Size: 1792 Color: 0

Bin 146: 19 of cap free
Amount of items: 2
Items: 
Size: 14601 Color: 0
Size: 2180 Color: 19

Bin 147: 19 of cap free
Amount of items: 2
Items: 
Size: 14696 Color: 9
Size: 2085 Color: 15

Bin 148: 20 of cap free
Amount of items: 2
Items: 
Size: 12564 Color: 2
Size: 4216 Color: 4

Bin 149: 20 of cap free
Amount of items: 2
Items: 
Size: 13976 Color: 18
Size: 2804 Color: 5

Bin 150: 21 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 1
Size: 2399 Color: 8

Bin 151: 22 of cap free
Amount of items: 2
Items: 
Size: 11528 Color: 2
Size: 5250 Color: 4

Bin 152: 22 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 1
Size: 2626 Color: 12
Size: 96 Color: 12

Bin 153: 23 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 3
Size: 4708 Color: 7
Size: 1028 Color: 9

Bin 154: 24 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 3
Size: 4282 Color: 9
Size: 192 Color: 4

Bin 155: 25 of cap free
Amount of items: 2
Items: 
Size: 14836 Color: 2
Size: 1939 Color: 19

Bin 156: 26 of cap free
Amount of items: 2
Items: 
Size: 11764 Color: 17
Size: 5010 Color: 0

Bin 157: 28 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 17
Size: 3974 Color: 8

Bin 158: 29 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 19
Size: 6043 Color: 8

Bin 159: 29 of cap free
Amount of items: 3
Items: 
Size: 12110 Color: 0
Size: 4517 Color: 8
Size: 144 Color: 6

Bin 160: 32 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 15
Size: 7048 Color: 10

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 10856 Color: 19
Size: 5912 Color: 9

Bin 162: 32 of cap free
Amount of items: 2
Items: 
Size: 11704 Color: 11
Size: 5064 Color: 2

Bin 163: 32 of cap free
Amount of items: 2
Items: 
Size: 14830 Color: 7
Size: 1938 Color: 4

Bin 164: 34 of cap free
Amount of items: 5
Items: 
Size: 8405 Color: 0
Size: 3168 Color: 6
Size: 2105 Color: 19
Size: 1644 Color: 8
Size: 1444 Color: 12

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 9
Size: 2858 Color: 7

Bin 166: 36 of cap free
Amount of items: 2
Items: 
Size: 14904 Color: 8
Size: 1860 Color: 0

Bin 167: 37 of cap free
Amount of items: 3
Items: 
Size: 10139 Color: 18
Size: 6256 Color: 17
Size: 368 Color: 1

Bin 168: 37 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 10
Size: 4284 Color: 2

Bin 169: 38 of cap free
Amount of items: 2
Items: 
Size: 14270 Color: 0
Size: 2492 Color: 7

Bin 170: 38 of cap free
Amount of items: 2
Items: 
Size: 14982 Color: 2
Size: 1780 Color: 7

Bin 171: 40 of cap free
Amount of items: 2
Items: 
Size: 10790 Color: 10
Size: 5970 Color: 9

Bin 172: 40 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 8
Size: 2354 Color: 12

Bin 173: 40 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 17
Size: 2272 Color: 8

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 10308 Color: 6
Size: 6446 Color: 18

Bin 175: 48 of cap free
Amount of items: 2
Items: 
Size: 11864 Color: 9
Size: 4888 Color: 12

Bin 176: 48 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 1
Size: 4804 Color: 10

Bin 177: 48 of cap free
Amount of items: 3
Items: 
Size: 12552 Color: 3
Size: 4104 Color: 2
Size: 96 Color: 16

Bin 178: 50 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 11
Size: 4968 Color: 2
Size: 1774 Color: 14

Bin 179: 50 of cap free
Amount of items: 3
Items: 
Size: 15068 Color: 15
Size: 1668 Color: 9
Size: 14 Color: 6

Bin 180: 54 of cap free
Amount of items: 2
Items: 
Size: 11074 Color: 9
Size: 5672 Color: 10

Bin 181: 54 of cap free
Amount of items: 2
Items: 
Size: 13145 Color: 6
Size: 3601 Color: 4

Bin 182: 54 of cap free
Amount of items: 2
Items: 
Size: 14484 Color: 2
Size: 2262 Color: 7

Bin 183: 58 of cap free
Amount of items: 35
Items: 
Size: 658 Color: 11
Size: 656 Color: 15
Size: 632 Color: 14
Size: 632 Color: 0
Size: 608 Color: 8
Size: 600 Color: 5
Size: 576 Color: 8
Size: 568 Color: 14
Size: 544 Color: 14
Size: 544 Color: 7
Size: 534 Color: 3
Size: 524 Color: 1
Size: 508 Color: 1
Size: 488 Color: 16
Size: 488 Color: 7
Size: 480 Color: 11
Size: 480 Color: 9
Size: 480 Color: 9
Size: 448 Color: 18
Size: 448 Color: 13
Size: 432 Color: 15
Size: 432 Color: 10
Size: 420 Color: 13
Size: 420 Color: 4
Size: 416 Color: 19
Size: 416 Color: 4
Size: 400 Color: 15
Size: 384 Color: 14
Size: 384 Color: 9
Size: 384 Color: 6
Size: 376 Color: 17
Size: 366 Color: 6
Size: 352 Color: 0
Size: 336 Color: 3
Size: 328 Color: 13

Bin 184: 58 of cap free
Amount of items: 2
Items: 
Size: 11140 Color: 14
Size: 5602 Color: 3

Bin 185: 66 of cap free
Amount of items: 2
Items: 
Size: 12376 Color: 3
Size: 4358 Color: 13

Bin 186: 67 of cap free
Amount of items: 2
Items: 
Size: 11932 Color: 9
Size: 4801 Color: 18

Bin 187: 69 of cap free
Amount of items: 2
Items: 
Size: 14799 Color: 7
Size: 1932 Color: 5

Bin 188: 76 of cap free
Amount of items: 2
Items: 
Size: 12206 Color: 5
Size: 4518 Color: 19

Bin 189: 77 of cap free
Amount of items: 2
Items: 
Size: 9638 Color: 0
Size: 7085 Color: 5

Bin 190: 77 of cap free
Amount of items: 2
Items: 
Size: 14792 Color: 12
Size: 1931 Color: 1

Bin 191: 92 of cap free
Amount of items: 3
Items: 
Size: 8418 Color: 3
Size: 7002 Color: 16
Size: 1288 Color: 1

Bin 192: 108 of cap free
Amount of items: 2
Items: 
Size: 11916 Color: 12
Size: 4776 Color: 14

Bin 193: 120 of cap free
Amount of items: 3
Items: 
Size: 8409 Color: 12
Size: 4252 Color: 1
Size: 4019 Color: 11

Bin 194: 123 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 9
Size: 7001 Color: 18
Size: 1264 Color: 18

Bin 195: 161 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 15
Size: 6997 Color: 14
Size: 1232 Color: 8

Bin 196: 166 of cap free
Amount of items: 22
Items: 
Size: 1080 Color: 7
Size: 1056 Color: 5
Size: 952 Color: 7
Size: 902 Color: 11
Size: 900 Color: 4
Size: 854 Color: 8
Size: 808 Color: 9
Size: 800 Color: 17
Size: 792 Color: 8
Size: 780 Color: 12
Size: 728 Color: 3
Size: 720 Color: 15
Size: 720 Color: 5
Size: 720 Color: 4
Size: 704 Color: 19
Size: 704 Color: 17
Size: 688 Color: 3
Size: 676 Color: 7
Size: 664 Color: 0
Size: 594 Color: 1
Size: 464 Color: 13
Size: 328 Color: 1

Bin 197: 205 of cap free
Amount of items: 2
Items: 
Size: 11044 Color: 11
Size: 5551 Color: 9

Bin 198: 247 of cap free
Amount of items: 2
Items: 
Size: 9549 Color: 6
Size: 7004 Color: 5

Bin 199: 13248 of cap free
Amount of items: 11
Items: 
Size: 384 Color: 8
Size: 352 Color: 2
Size: 352 Color: 0
Size: 328 Color: 17
Size: 324 Color: 19
Size: 320 Color: 15
Size: 320 Color: 12
Size: 316 Color: 6
Size: 288 Color: 3
Size: 288 Color: 1
Size: 280 Color: 13

Total size: 3326400
Total free space: 16800

