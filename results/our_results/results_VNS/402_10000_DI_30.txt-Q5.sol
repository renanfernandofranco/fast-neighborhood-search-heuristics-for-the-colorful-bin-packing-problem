Capicity Bin: 4912
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2728 Color: 4
Size: 2042 Color: 2
Size: 142 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2938 Color: 4
Size: 1866 Color: 0
Size: 108 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3072 Color: 3
Size: 1532 Color: 3
Size: 308 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3465 Color: 4
Size: 1207 Color: 0
Size: 240 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3484 Color: 3
Size: 1212 Color: 4
Size: 216 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3599 Color: 0
Size: 1209 Color: 1
Size: 104 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 3602 Color: 0
Size: 1214 Color: 1
Size: 96 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 3634 Color: 3
Size: 1122 Color: 0
Size: 156 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3732 Color: 3
Size: 1082 Color: 0
Size: 98 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3816 Color: 3
Size: 1016 Color: 1
Size: 80 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3855 Color: 3
Size: 881 Color: 0
Size: 176 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 3880 Color: 4
Size: 910 Color: 0
Size: 122 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 3900 Color: 0
Size: 724 Color: 4
Size: 288 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 3955 Color: 1
Size: 811 Color: 0
Size: 146 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 3963 Color: 0
Size: 785 Color: 2
Size: 164 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 3990 Color: 0
Size: 728 Color: 2
Size: 194 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4035 Color: 0
Size: 657 Color: 4
Size: 220 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4051 Color: 2
Size: 731 Color: 3
Size: 130 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4056 Color: 0
Size: 680 Color: 2
Size: 176 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 4
Size: 730 Color: 3
Size: 114 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 1
Size: 684 Color: 4
Size: 144 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4100 Color: 3
Size: 440 Color: 2
Size: 372 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4126 Color: 2
Size: 530 Color: 1
Size: 256 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4122 Color: 0
Size: 662 Color: 2
Size: 128 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4194 Color: 2
Size: 542 Color: 0
Size: 176 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4225 Color: 3
Size: 567 Color: 3
Size: 120 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4236 Color: 2
Size: 564 Color: 0
Size: 112 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 4
Size: 428 Color: 2
Size: 240 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4248 Color: 4
Size: 472 Color: 2
Size: 192 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4263 Color: 1
Size: 541 Color: 0
Size: 108 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4260 Color: 2
Size: 544 Color: 4
Size: 108 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 2
Size: 464 Color: 1
Size: 156 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4279 Color: 0
Size: 529 Color: 1
Size: 104 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 2
Size: 486 Color: 0
Size: 96 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4360 Color: 0
Size: 372 Color: 2
Size: 180 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4386 Color: 3
Size: 414 Color: 4
Size: 112 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4392 Color: 2
Size: 296 Color: 3
Size: 224 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4402 Color: 4
Size: 426 Color: 2
Size: 84 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4418 Color: 3
Size: 318 Color: 0
Size: 176 Color: 2

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 2675 Color: 2
Size: 2044 Color: 2
Size: 192 Color: 4

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 2971 Color: 4
Size: 1740 Color: 0
Size: 200 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 3063 Color: 3
Size: 1656 Color: 4
Size: 192 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 3260 Color: 4
Size: 1371 Color: 1
Size: 280 Color: 2

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 3449 Color: 1
Size: 1238 Color: 1
Size: 224 Color: 3

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 3613 Color: 3
Size: 1136 Color: 2
Size: 162 Color: 1

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 3558 Color: 2
Size: 1221 Color: 4
Size: 132 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 3839 Color: 3
Size: 728 Color: 0
Size: 344 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 0
Size: 657 Color: 3
Size: 308 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 3939 Color: 3
Size: 888 Color: 1
Size: 84 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 4059 Color: 2
Size: 548 Color: 3
Size: 304 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 4141 Color: 4
Size: 558 Color: 3
Size: 212 Color: 2

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 4209 Color: 0
Size: 524 Color: 2
Size: 178 Color: 1

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 4246 Color: 1
Size: 665 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 4331 Color: 3
Size: 532 Color: 2
Size: 48 Color: 3

Bin 55: 2 of cap free
Amount of items: 7
Items: 
Size: 2458 Color: 2
Size: 564 Color: 0
Size: 490 Color: 0
Size: 436 Color: 2
Size: 408 Color: 3
Size: 322 Color: 3
Size: 232 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 3116 Color: 4
Size: 1698 Color: 0
Size: 96 Color: 1

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 3279 Color: 0
Size: 1551 Color: 0
Size: 80 Color: 2

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 3378 Color: 3
Size: 1436 Color: 0
Size: 96 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 3394 Color: 2
Size: 1160 Color: 0
Size: 356 Color: 1

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 3586 Color: 0
Size: 1164 Color: 3
Size: 160 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 4
Size: 996 Color: 3
Size: 224 Color: 0

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 3753 Color: 4
Size: 981 Color: 1
Size: 176 Color: 2

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 3958 Color: 2
Size: 952 Color: 4

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 4038 Color: 1
Size: 872 Color: 2

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 4072 Color: 3
Size: 838 Color: 2

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 4104 Color: 2
Size: 806 Color: 4

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 4326 Color: 1
Size: 584 Color: 0

Bin 68: 3 of cap free
Amount of items: 9
Items: 
Size: 2457 Color: 1
Size: 400 Color: 4
Size: 400 Color: 1
Size: 352 Color: 1
Size: 336 Color: 4
Size: 320 Color: 3
Size: 288 Color: 0
Size: 220 Color: 2
Size: 136 Color: 0

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 2820 Color: 0
Size: 1865 Color: 0
Size: 224 Color: 2

Bin 70: 3 of cap free
Amount of items: 4
Items: 
Size: 2995 Color: 0
Size: 1500 Color: 3
Size: 310 Color: 2
Size: 104 Color: 3

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 3051 Color: 3
Size: 1646 Color: 2
Size: 212 Color: 3

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 3126 Color: 3
Size: 1783 Color: 2

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 3548 Color: 1
Size: 1361 Color: 2

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 3838 Color: 1
Size: 799 Color: 0
Size: 272 Color: 2

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 4179 Color: 1
Size: 658 Color: 3
Size: 72 Color: 2

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 4198 Color: 0
Size: 711 Color: 3

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 2460 Color: 2
Size: 2040 Color: 2
Size: 408 Color: 4

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 3178 Color: 2
Size: 1562 Color: 1
Size: 168 Color: 0

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 1
Size: 1368 Color: 2
Size: 80 Color: 0

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 4216 Color: 4
Size: 692 Color: 0

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 3288 Color: 2
Size: 1619 Color: 4

Bin 82: 5 of cap free
Amount of items: 3
Items: 
Size: 3737 Color: 4
Size: 1106 Color: 0
Size: 64 Color: 1

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 4125 Color: 4
Size: 782 Color: 3

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 3262 Color: 3
Size: 876 Color: 3
Size: 768 Color: 2

Bin 85: 6 of cap free
Amount of items: 3
Items: 
Size: 3410 Color: 1
Size: 1380 Color: 0
Size: 116 Color: 4

Bin 86: 6 of cap free
Amount of items: 3
Items: 
Size: 3528 Color: 0
Size: 1130 Color: 1
Size: 248 Color: 2

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 4115 Color: 1
Size: 791 Color: 4

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 4187 Color: 4
Size: 719 Color: 3

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 2773 Color: 1
Size: 2036 Color: 4
Size: 96 Color: 4

Bin 90: 7 of cap free
Amount of items: 3
Items: 
Size: 3228 Color: 0
Size: 1541 Color: 4
Size: 136 Color: 2

Bin 91: 7 of cap free
Amount of items: 2
Items: 
Size: 3822 Color: 1
Size: 1083 Color: 4

Bin 92: 7 of cap free
Amount of items: 2
Items: 
Size: 4262 Color: 3
Size: 643 Color: 4

Bin 93: 7 of cap free
Amount of items: 3
Items: 
Size: 4278 Color: 4
Size: 611 Color: 1
Size: 16 Color: 1

Bin 94: 8 of cap free
Amount of items: 3
Items: 
Size: 2462 Color: 3
Size: 1446 Color: 0
Size: 996 Color: 4

Bin 95: 8 of cap free
Amount of items: 3
Items: 
Size: 2674 Color: 4
Size: 2150 Color: 0
Size: 80 Color: 1

Bin 96: 8 of cap free
Amount of items: 3
Items: 
Size: 2690 Color: 2
Size: 1886 Color: 1
Size: 328 Color: 0

Bin 97: 8 of cap free
Amount of items: 2
Items: 
Size: 3392 Color: 1
Size: 1512 Color: 2

Bin 98: 8 of cap free
Amount of items: 3
Items: 
Size: 3570 Color: 3
Size: 1254 Color: 2
Size: 80 Color: 2

Bin 99: 8 of cap free
Amount of items: 3
Items: 
Size: 3925 Color: 1
Size: 895 Color: 3
Size: 84 Color: 4

Bin 100: 9 of cap free
Amount of items: 2
Items: 
Size: 4233 Color: 4
Size: 670 Color: 1

Bin 101: 9 of cap free
Amount of items: 2
Items: 
Size: 4404 Color: 1
Size: 499 Color: 4

Bin 102: 10 of cap free
Amount of items: 3
Items: 
Size: 2958 Color: 0
Size: 1832 Color: 3
Size: 112 Color: 2

Bin 103: 11 of cap free
Amount of items: 3
Items: 
Size: 2476 Color: 1
Size: 2145 Color: 2
Size: 280 Color: 3

Bin 104: 11 of cap free
Amount of items: 4
Items: 
Size: 2955 Color: 0
Size: 988 Color: 2
Size: 798 Color: 4
Size: 160 Color: 2

Bin 105: 11 of cap free
Amount of items: 2
Items: 
Size: 3635 Color: 4
Size: 1266 Color: 2

Bin 106: 11 of cap free
Amount of items: 3
Items: 
Size: 4384 Color: 0
Size: 485 Color: 3
Size: 32 Color: 1

Bin 107: 11 of cap free
Amount of items: 3
Items: 
Size: 4396 Color: 0
Size: 489 Color: 4
Size: 16 Color: 1

Bin 108: 12 of cap free
Amount of items: 3
Items: 
Size: 3432 Color: 2
Size: 1404 Color: 4
Size: 64 Color: 2

Bin 109: 12 of cap free
Amount of items: 2
Items: 
Size: 3618 Color: 0
Size: 1282 Color: 2

Bin 110: 12 of cap free
Amount of items: 2
Items: 
Size: 3704 Color: 2
Size: 1196 Color: 1

Bin 111: 12 of cap free
Amount of items: 2
Items: 
Size: 4327 Color: 2
Size: 573 Color: 3

Bin 112: 13 of cap free
Amount of items: 2
Items: 
Size: 2852 Color: 3
Size: 2047 Color: 1

Bin 113: 13 of cap free
Amount of items: 2
Items: 
Size: 3148 Color: 3
Size: 1751 Color: 4

Bin 114: 13 of cap free
Amount of items: 2
Items: 
Size: 3932 Color: 2
Size: 967 Color: 1

Bin 115: 13 of cap free
Amount of items: 3
Items: 
Size: 4315 Color: 3
Size: 568 Color: 0
Size: 16 Color: 3

Bin 116: 17 of cap free
Amount of items: 3
Items: 
Size: 3784 Color: 2
Size: 1065 Color: 4
Size: 46 Color: 0

Bin 117: 18 of cap free
Amount of items: 2
Items: 
Size: 3516 Color: 4
Size: 1378 Color: 3

Bin 118: 18 of cap free
Amount of items: 2
Items: 
Size: 3974 Color: 2
Size: 920 Color: 3

Bin 119: 20 of cap free
Amount of items: 2
Items: 
Size: 3868 Color: 4
Size: 1024 Color: 1

Bin 120: 24 of cap free
Amount of items: 2
Items: 
Size: 2936 Color: 2
Size: 1952 Color: 3

Bin 121: 25 of cap free
Amount of items: 2
Items: 
Size: 4067 Color: 4
Size: 820 Color: 3

Bin 122: 31 of cap free
Amount of items: 2
Items: 
Size: 4276 Color: 4
Size: 605 Color: 0

Bin 123: 36 of cap free
Amount of items: 2
Items: 
Size: 3196 Color: 1
Size: 1680 Color: 4

Bin 124: 37 of cap free
Amount of items: 2
Items: 
Size: 4052 Color: 2
Size: 823 Color: 4

Bin 125: 42 of cap free
Amount of items: 2
Items: 
Size: 3848 Color: 2
Size: 1022 Color: 1

Bin 126: 43 of cap free
Amount of items: 2
Items: 
Size: 3971 Color: 4
Size: 898 Color: 3

Bin 127: 44 of cap free
Amount of items: 2
Items: 
Size: 3269 Color: 0
Size: 1599 Color: 4

Bin 128: 48 of cap free
Amount of items: 2
Items: 
Size: 3724 Color: 3
Size: 1140 Color: 1

Bin 129: 50 of cap free
Amount of items: 24
Items: 
Size: 320 Color: 3
Size: 272 Color: 3
Size: 272 Color: 2
Size: 272 Color: 1
Size: 256 Color: 2
Size: 252 Color: 4
Size: 252 Color: 3
Size: 242 Color: 0
Size: 240 Color: 0
Size: 232 Color: 3
Size: 232 Color: 1
Size: 206 Color: 1
Size: 192 Color: 1
Size: 176 Color: 4
Size: 176 Color: 4
Size: 160 Color: 0
Size: 158 Color: 4
Size: 158 Color: 0
Size: 156 Color: 2
Size: 142 Color: 1
Size: 136 Color: 2
Size: 128 Color: 2
Size: 128 Color: 0
Size: 104 Color: 3

Bin 130: 50 of cap free
Amount of items: 4
Items: 
Size: 2472 Color: 2
Size: 1240 Color: 4
Size: 708 Color: 0
Size: 442 Color: 3

Bin 131: 52 of cap free
Amount of items: 2
Items: 
Size: 3112 Color: 1
Size: 1748 Color: 2

Bin 132: 54 of cap free
Amount of items: 6
Items: 
Size: 2459 Color: 0
Size: 705 Color: 1
Size: 598 Color: 0
Size: 560 Color: 1
Size: 408 Color: 3
Size: 128 Color: 3

Bin 133: 3908 of cap free
Amount of items: 9
Items: 
Size: 140 Color: 4
Size: 128 Color: 4
Size: 128 Color: 0
Size: 112 Color: 0
Size: 104 Color: 3
Size: 104 Color: 3
Size: 96 Color: 3
Size: 96 Color: 2
Size: 96 Color: 2

Total size: 648384
Total free space: 4912

