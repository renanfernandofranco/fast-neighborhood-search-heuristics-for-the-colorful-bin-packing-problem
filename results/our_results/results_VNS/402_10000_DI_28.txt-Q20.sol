Capicity Bin: 8160
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4106 Color: 8
Size: 3918 Color: 1
Size: 136 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4138 Color: 9
Size: 3354 Color: 14
Size: 668 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4146 Color: 11
Size: 3346 Color: 4
Size: 668 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 4
Size: 3342 Color: 3
Size: 664 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4332 Color: 18
Size: 3548 Color: 17
Size: 280 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 14
Size: 2778 Color: 3
Size: 578 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4829 Color: 7
Size: 2777 Color: 18
Size: 554 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 6
Size: 2804 Color: 16
Size: 136 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5511 Color: 11
Size: 2431 Color: 14
Size: 218 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 1
Size: 2140 Color: 1
Size: 120 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5901 Color: 3
Size: 1497 Color: 9
Size: 762 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 16
Size: 1882 Color: 19
Size: 376 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6012 Color: 0
Size: 1414 Color: 3
Size: 734 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 17
Size: 1647 Color: 0
Size: 328 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 10
Size: 1580 Color: 1
Size: 312 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6381 Color: 0
Size: 1483 Color: 18
Size: 296 Color: 10

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 5
Size: 1426 Color: 18
Size: 284 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6560 Color: 13
Size: 1436 Color: 14
Size: 164 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 3
Size: 1333 Color: 0
Size: 266 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 14
Size: 1286 Color: 12
Size: 256 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 10
Size: 1284 Color: 19
Size: 256 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6769 Color: 0
Size: 839 Color: 12
Size: 552 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 5
Size: 1082 Color: 7
Size: 330 Color: 10

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 8
Size: 1178 Color: 18
Size: 232 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 5
Size: 700 Color: 9
Size: 606 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 0
Size: 918 Color: 13
Size: 376 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6901 Color: 18
Size: 763 Color: 18
Size: 496 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 14
Size: 684 Color: 4
Size: 552 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 15
Size: 957 Color: 0
Size: 266 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 10
Size: 858 Color: 15
Size: 312 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 2
Size: 676 Color: 0
Size: 440 Color: 15

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 13
Size: 556 Color: 2
Size: 518 Color: 12

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 2
Size: 584 Color: 16
Size: 484 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7132 Color: 15
Size: 782 Color: 1
Size: 246 Color: 10

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 2
Size: 852 Color: 10
Size: 158 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7183 Color: 5
Size: 879 Color: 14
Size: 98 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 16
Size: 738 Color: 0
Size: 232 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 18
Size: 668 Color: 3
Size: 270 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 0
Size: 678 Color: 10
Size: 230 Color: 13

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 13
Size: 764 Color: 15
Size: 146 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7282 Color: 10
Size: 576 Color: 4
Size: 302 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7303 Color: 10
Size: 715 Color: 5
Size: 142 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 5
Size: 680 Color: 4
Size: 156 Color: 0

Bin 44: 1 of cap free
Amount of items: 5
Items: 
Size: 4081 Color: 6
Size: 1161 Color: 10
Size: 1090 Color: 16
Size: 1066 Color: 0
Size: 761 Color: 6

Bin 45: 1 of cap free
Amount of items: 5
Items: 
Size: 4085 Color: 0
Size: 1552 Color: 0
Size: 1339 Color: 9
Size: 695 Color: 9
Size: 488 Color: 10

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 6
Size: 3201 Color: 16
Size: 136 Color: 19

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5121 Color: 18
Size: 2782 Color: 3
Size: 256 Color: 2

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 5626 Color: 18
Size: 2533 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 5861 Color: 19
Size: 2130 Color: 5
Size: 168 Color: 14

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6018 Color: 7
Size: 1465 Color: 18
Size: 676 Color: 16

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6145 Color: 19
Size: 2014 Color: 16

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6169 Color: 13
Size: 1774 Color: 4
Size: 216 Color: 12

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 14
Size: 1681 Color: 3
Size: 208 Color: 15

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 6466 Color: 8
Size: 1693 Color: 10

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 6581 Color: 14
Size: 1578 Color: 16

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6681 Color: 14
Size: 1144 Color: 6
Size: 334 Color: 5

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 4
Size: 926 Color: 0
Size: 480 Color: 15

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 8
Size: 921 Color: 13
Size: 42 Color: 6

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 7327 Color: 8
Size: 832 Color: 16

Bin 60: 2 of cap free
Amount of items: 16
Items: 
Size: 797 Color: 2
Size: 768 Color: 19
Size: 731 Color: 18
Size: 694 Color: 6
Size: 528 Color: 10
Size: 512 Color: 17
Size: 484 Color: 3
Size: 448 Color: 12
Size: 428 Color: 8
Size: 424 Color: 19
Size: 424 Color: 2
Size: 400 Color: 5
Size: 400 Color: 4
Size: 396 Color: 12
Size: 386 Color: 2
Size: 338 Color: 14

Bin 61: 2 of cap free
Amount of items: 4
Items: 
Size: 4082 Color: 17
Size: 3076 Color: 4
Size: 804 Color: 4
Size: 196 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 4130 Color: 7
Size: 3796 Color: 4
Size: 232 Color: 13

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 16
Size: 3362 Color: 12
Size: 320 Color: 3

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 15
Size: 1598 Color: 0
Size: 100 Color: 2

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5746 Color: 16
Size: 2209 Color: 2
Size: 202 Color: 8

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6444 Color: 12
Size: 1713 Color: 17

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 2
Size: 638 Color: 6
Size: 506 Color: 10

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 6
Size: 2924 Color: 2
Size: 144 Color: 7

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 0
Size: 2406 Color: 10
Size: 144 Color: 10

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 5596 Color: 12
Size: 2560 Color: 3

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 7055 Color: 4
Size: 1101 Color: 6

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 7294 Color: 16
Size: 862 Color: 17

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 6505 Color: 9
Size: 1582 Color: 15
Size: 68 Color: 16

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 1
Size: 1233 Color: 10
Size: 40 Color: 13

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 6988 Color: 18
Size: 1166 Color: 5

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 11
Size: 980 Color: 16

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 11
Size: 2601 Color: 16
Size: 722 Color: 0

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 7
Size: 1051 Color: 18

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 7155 Color: 1
Size: 998 Color: 16

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 11
Size: 2426 Color: 2
Size: 298 Color: 1

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 7245 Color: 8
Size: 883 Color: 16
Size: 24 Color: 3

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 7
Size: 822 Color: 11

Bin 83: 9 of cap free
Amount of items: 3
Items: 
Size: 5779 Color: 17
Size: 2284 Color: 18
Size: 88 Color: 0

Bin 84: 9 of cap free
Amount of items: 2
Items: 
Size: 6365 Color: 1
Size: 1786 Color: 10

Bin 85: 9 of cap free
Amount of items: 2
Items: 
Size: 7130 Color: 11
Size: 1021 Color: 7

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 3
Size: 1100 Color: 2

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 17
Size: 978 Color: 1

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 7340 Color: 18
Size: 810 Color: 14

Bin 89: 11 of cap free
Amount of items: 2
Items: 
Size: 6129 Color: 16
Size: 2020 Color: 5

Bin 90: 11 of cap free
Amount of items: 2
Items: 
Size: 7248 Color: 3
Size: 901 Color: 12

Bin 91: 13 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 13
Size: 2151 Color: 8
Size: 256 Color: 5

Bin 92: 14 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 12
Size: 3386 Color: 19
Size: 676 Color: 1

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 4114 Color: 1
Size: 4032 Color: 17

Bin 94: 14 of cap free
Amount of items: 3
Items: 
Size: 5041 Color: 3
Size: 2681 Color: 18
Size: 424 Color: 0

Bin 95: 14 of cap free
Amount of items: 2
Items: 
Size: 6262 Color: 8
Size: 1884 Color: 10

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 19
Size: 1180 Color: 10

Bin 97: 15 of cap free
Amount of items: 2
Items: 
Size: 6228 Color: 3
Size: 1917 Color: 5

Bin 98: 16 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 11
Size: 2894 Color: 1

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 7168 Color: 13
Size: 976 Color: 3

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 7322 Color: 11
Size: 822 Color: 8

Bin 101: 17 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 19
Size: 1661 Color: 1
Size: 448 Color: 0

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 9
Size: 1537 Color: 14

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 7081 Color: 15
Size: 1062 Color: 14

Bin 104: 17 of cap free
Amount of items: 2
Items: 
Size: 7283 Color: 12
Size: 860 Color: 7

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 6
Size: 1298 Color: 2

Bin 106: 19 of cap free
Amount of items: 2
Items: 
Size: 6529 Color: 4
Size: 1612 Color: 10

Bin 107: 19 of cap free
Amount of items: 2
Items: 
Size: 6841 Color: 8
Size: 1300 Color: 6

Bin 108: 20 of cap free
Amount of items: 2
Items: 
Size: 5100 Color: 6
Size: 3040 Color: 7

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 5245 Color: 10
Size: 2893 Color: 9

Bin 110: 22 of cap free
Amount of items: 2
Items: 
Size: 7246 Color: 14
Size: 892 Color: 9

Bin 111: 23 of cap free
Amount of items: 2
Items: 
Size: 6341 Color: 16
Size: 1796 Color: 6

Bin 112: 23 of cap free
Amount of items: 2
Items: 
Size: 7101 Color: 14
Size: 1036 Color: 9

Bin 113: 23 of cap free
Amount of items: 2
Items: 
Size: 7205 Color: 18
Size: 932 Color: 12

Bin 114: 29 of cap free
Amount of items: 2
Items: 
Size: 7249 Color: 0
Size: 882 Color: 3

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 4321 Color: 12
Size: 3809 Color: 15

Bin 116: 30 of cap free
Amount of items: 3
Items: 
Size: 4945 Color: 5
Size: 3033 Color: 0
Size: 152 Color: 12

Bin 117: 30 of cap free
Amount of items: 2
Items: 
Size: 6890 Color: 3
Size: 1240 Color: 17

Bin 118: 31 of cap free
Amount of items: 2
Items: 
Size: 6246 Color: 1
Size: 1883 Color: 2

Bin 119: 37 of cap free
Amount of items: 2
Items: 
Size: 5837 Color: 6
Size: 2286 Color: 8

Bin 120: 37 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 13
Size: 1361 Color: 10

Bin 121: 39 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 7
Size: 1517 Color: 15

Bin 122: 52 of cap free
Amount of items: 34
Items: 
Size: 384 Color: 4
Size: 382 Color: 15
Size: 376 Color: 12
Size: 356 Color: 8
Size: 352 Color: 9
Size: 352 Color: 5
Size: 342 Color: 18
Size: 316 Color: 17
Size: 316 Color: 6
Size: 280 Color: 18
Size: 280 Color: 13
Size: 248 Color: 8
Size: 232 Color: 10
Size: 216 Color: 14
Size: 212 Color: 18
Size: 212 Color: 18
Size: 208 Color: 1
Size: 200 Color: 7
Size: 192 Color: 15
Size: 192 Color: 12
Size: 190 Color: 12
Size: 190 Color: 0
Size: 184 Color: 16
Size: 184 Color: 9
Size: 184 Color: 9
Size: 184 Color: 3
Size: 178 Color: 2
Size: 176 Color: 17
Size: 176 Color: 13
Size: 176 Color: 5
Size: 168 Color: 3
Size: 166 Color: 14
Size: 160 Color: 1
Size: 144 Color: 0

Bin 123: 54 of cap free
Amount of items: 2
Items: 
Size: 4180 Color: 6
Size: 3926 Color: 2

Bin 124: 58 of cap free
Amount of items: 2
Items: 
Size: 5274 Color: 10
Size: 2828 Color: 1

Bin 125: 61 of cap free
Amount of items: 3
Items: 
Size: 4090 Color: 4
Size: 3401 Color: 5
Size: 608 Color: 9

Bin 126: 67 of cap free
Amount of items: 2
Items: 
Size: 4689 Color: 0
Size: 3404 Color: 14

Bin 127: 70 of cap free
Amount of items: 2
Items: 
Size: 6105 Color: 3
Size: 1985 Color: 15

Bin 128: 71 of cap free
Amount of items: 2
Items: 
Size: 4521 Color: 4
Size: 3568 Color: 14

Bin 129: 106 of cap free
Amount of items: 2
Items: 
Size: 4652 Color: 10
Size: 3402 Color: 3

Bin 130: 113 of cap free
Amount of items: 3
Items: 
Size: 4690 Color: 11
Size: 1937 Color: 19
Size: 1420 Color: 14

Bin 131: 127 of cap free
Amount of items: 2
Items: 
Size: 5581 Color: 11
Size: 2452 Color: 13

Bin 132: 134 of cap free
Amount of items: 3
Items: 
Size: 4098 Color: 5
Size: 3394 Color: 12
Size: 534 Color: 0

Bin 133: 6380 of cap free
Amount of items: 12
Items: 
Size: 160 Color: 9
Size: 160 Color: 6
Size: 156 Color: 5
Size: 152 Color: 17
Size: 152 Color: 14
Size: 152 Color: 2
Size: 150 Color: 10
Size: 144 Color: 1
Size: 144 Color: 0
Size: 138 Color: 16
Size: 136 Color: 13
Size: 136 Color: 8

Total size: 1077120
Total free space: 8160

