Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 305
Size: 2531 Color: 241
Size: 156 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 307
Size: 1558 Color: 203
Size: 1102 Color: 172

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5909 Color: 321
Size: 1775 Color: 216
Size: 396 Color: 95

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5999 Color: 329
Size: 1571 Color: 205
Size: 510 Color: 115

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6007 Color: 330
Size: 1729 Color: 211
Size: 344 Color: 80

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 333
Size: 1756 Color: 214
Size: 136 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6195 Color: 334
Size: 1439 Color: 197
Size: 446 Color: 106

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 336
Size: 1580 Color: 206
Size: 286 Color: 66

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 342
Size: 1369 Color: 193
Size: 356 Color: 85

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 343
Size: 1044 Color: 166
Size: 672 Color: 135

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 344
Size: 1533 Color: 200
Size: 168 Color: 21

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 345
Size: 1268 Color: 188
Size: 432 Color: 100

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6436 Color: 347
Size: 1580 Color: 207
Size: 64 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6464 Color: 349
Size: 1240 Color: 185
Size: 376 Color: 93

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 353
Size: 1169 Color: 179
Size: 356 Color: 86

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 355
Size: 1291 Color: 190
Size: 210 Color: 39

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 357
Size: 1172 Color: 180
Size: 308 Color: 71

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 358
Size: 1220 Color: 181
Size: 258 Color: 59

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 359
Size: 1222 Color: 182
Size: 248 Color: 54

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 360
Size: 1112 Color: 173
Size: 350 Color: 82

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 362
Size: 1052 Color: 168
Size: 352 Color: 83

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6713 Color: 364
Size: 1071 Color: 170
Size: 296 Color: 68

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6721 Color: 365
Size: 1127 Color: 175
Size: 232 Color: 47

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 366
Size: 684 Color: 139
Size: 672 Color: 136

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 367
Size: 1055 Color: 169
Size: 296 Color: 69

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6758 Color: 368
Size: 1004 Color: 162
Size: 318 Color: 76

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 371
Size: 1025 Color: 163
Size: 240 Color: 51

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 375
Size: 762 Color: 146
Size: 442 Color: 104

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 376
Size: 1002 Color: 161
Size: 196 Color: 34

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 377
Size: 946 Color: 156
Size: 244 Color: 53

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 380
Size: 965 Color: 159
Size: 192 Color: 33

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 385
Size: 956 Color: 158
Size: 136 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6996 Color: 386
Size: 724 Color: 143
Size: 360 Color: 88

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 387
Size: 891 Color: 153
Size: 176 Color: 27

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7038 Color: 388
Size: 754 Color: 145
Size: 288 Color: 67

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7084 Color: 391
Size: 868 Color: 151
Size: 128 Color: 7

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7094 Color: 392
Size: 586 Color: 125
Size: 400 Color: 96

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7178 Color: 395
Size: 714 Color: 142
Size: 188 Color: 31

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7220 Color: 397
Size: 636 Color: 132
Size: 224 Color: 44

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7226 Color: 398
Size: 544 Color: 121
Size: 310 Color: 72

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 399
Size: 582 Color: 124
Size: 264 Color: 60

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 402
Size: 648 Color: 133
Size: 164 Color: 20

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 5417 Color: 306
Size: 2662 Color: 248

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 319
Size: 1755 Color: 213
Size: 450 Color: 107

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5946 Color: 324
Size: 1949 Color: 225
Size: 184 Color: 28

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6171 Color: 332
Size: 1764 Color: 215
Size: 144 Color: 13

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6219 Color: 337
Size: 1436 Color: 196
Size: 424 Color: 99

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6587 Color: 356
Size: 1420 Color: 195
Size: 72 Color: 3

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6893 Color: 378
Size: 672 Color: 137
Size: 514 Color: 118

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6917 Color: 379
Size: 706 Color: 141
Size: 456 Color: 108

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 381
Size: 1141 Color: 178

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 6946 Color: 383
Size: 1133 Color: 177

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5972 Color: 326
Size: 1826 Color: 221
Size: 280 Color: 63

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6202 Color: 335
Size: 1876 Color: 223

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 361
Size: 1458 Color: 198

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 7046 Color: 390
Size: 1032 Color: 165

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7242 Color: 400
Size: 836 Color: 149

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 7260 Color: 401
Size: 818 Color: 147

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 5433 Color: 309
Size: 1782 Color: 217
Size: 862 Color: 150

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 5836 Color: 318
Size: 2241 Color: 236

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 351
Size: 1551 Color: 202

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6851 Color: 374
Size: 1226 Color: 183

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6426 Color: 346
Size: 1650 Color: 210

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6485 Color: 350
Size: 1591 Color: 208

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6531 Color: 352
Size: 1545 Color: 201

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7044 Color: 389
Size: 1031 Color: 164

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 317
Size: 2294 Color: 238

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 373
Size: 1251 Color: 187

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 394
Size: 908 Color: 154

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 7204 Color: 396
Size: 870 Color: 152

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 6262 Color: 339
Size: 1811 Color: 220

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 393
Size: 971 Color: 160

Bin 73: 8 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 328
Size: 2092 Color: 229

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 6564 Color: 354
Size: 1508 Color: 199

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 382
Size: 1132 Color: 176

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 6954 Color: 384
Size: 1118 Color: 174

Bin 77: 9 of cap free
Amount of items: 4
Items: 
Size: 5572 Color: 311
Size: 2207 Color: 232
Size: 148 Color: 15
Size: 144 Color: 14

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 5890 Color: 320
Size: 2180 Color: 231

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 6276 Color: 340
Size: 1794 Color: 219

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 338
Size: 1842 Color: 222

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 341
Size: 1735 Color: 212

Bin 82: 12 of cap free
Amount of items: 13
Items: 
Size: 942 Color: 155
Size: 822 Color: 148
Size: 732 Color: 144
Size: 702 Color: 140
Size: 684 Color: 138
Size: 672 Color: 134
Size: 636 Color: 131
Size: 636 Color: 130
Size: 632 Color: 129
Size: 632 Color: 128
Size: 344 Color: 78
Size: 320 Color: 77
Size: 314 Color: 75

Bin 83: 12 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 316
Size: 2221 Color: 235
Size: 104 Color: 5

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 6797 Color: 370
Size: 1271 Color: 189

Bin 85: 13 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 303
Size: 2577 Color: 247
Size: 160 Color: 19

Bin 86: 13 of cap free
Amount of items: 2
Items: 
Size: 6439 Color: 348
Size: 1628 Color: 209

Bin 87: 14 of cap free
Amount of items: 3
Items: 
Size: 5369 Color: 304
Size: 2537 Color: 243
Size: 160 Color: 18

Bin 88: 14 of cap free
Amount of items: 3
Items: 
Size: 5693 Color: 315
Size: 2261 Color: 237
Size: 112 Color: 6

Bin 89: 15 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 372
Size: 1245 Color: 186

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 369
Size: 1298 Color: 191

Bin 91: 19 of cap free
Amount of items: 2
Items: 
Size: 5951 Color: 325
Size: 2110 Color: 230

Bin 92: 19 of cap free
Amount of items: 2
Items: 
Size: 6679 Color: 363
Size: 1382 Color: 194

Bin 93: 20 of cap free
Amount of items: 3
Items: 
Size: 4046 Color: 277
Size: 3742 Color: 271
Size: 272 Color: 62

Bin 94: 21 of cap free
Amount of items: 5
Items: 
Size: 4042 Color: 274
Size: 1786 Color: 218
Size: 1331 Color: 192
Size: 592 Color: 127
Size: 308 Color: 70

Bin 95: 21 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 286
Size: 3276 Color: 265
Size: 226 Color: 46

Bin 96: 23 of cap free
Amount of items: 2
Items: 
Size: 5975 Color: 327
Size: 2082 Color: 228

Bin 97: 24 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 331
Size: 1924 Color: 224

Bin 98: 26 of cap free
Amount of items: 3
Items: 
Size: 5582 Color: 312
Size: 2332 Color: 239
Size: 140 Color: 12

Bin 99: 26 of cap free
Amount of items: 4
Items: 
Size: 5938 Color: 323
Size: 2012 Color: 227
Size: 56 Color: 1
Size: 48 Color: 0

Bin 100: 29 of cap free
Amount of items: 2
Items: 
Size: 4043 Color: 275
Size: 4008 Color: 272

Bin 101: 29 of cap free
Amount of items: 3
Items: 
Size: 4890 Color: 293
Size: 2957 Color: 257
Size: 204 Color: 36

Bin 102: 32 of cap free
Amount of items: 5
Items: 
Size: 4044 Color: 276
Size: 2900 Color: 252
Size: 544 Color: 122
Size: 280 Color: 65
Size: 280 Color: 64

Bin 103: 34 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 314
Size: 2220 Color: 234
Size: 136 Color: 10

Bin 104: 40 of cap free
Amount of items: 18
Items: 
Size: 590 Color: 126
Size: 580 Color: 123
Size: 544 Color: 120
Size: 528 Color: 119
Size: 512 Color: 117
Size: 512 Color: 116
Size: 506 Color: 114
Size: 504 Color: 113
Size: 504 Color: 112
Size: 388 Color: 94
Size: 376 Color: 92
Size: 368 Color: 91
Size: 364 Color: 90
Size: 364 Color: 89
Size: 356 Color: 87
Size: 354 Color: 84
Size: 346 Color: 81
Size: 344 Color: 79

Bin 105: 40 of cap free
Amount of items: 3
Items: 
Size: 5425 Color: 308
Size: 1566 Color: 204
Size: 1049 Color: 167

Bin 106: 46 of cap free
Amount of items: 3
Items: 
Size: 4266 Color: 283
Size: 3528 Color: 270
Size: 240 Color: 50

Bin 107: 49 of cap free
Amount of items: 4
Items: 
Size: 4684 Color: 289
Size: 2911 Color: 253
Size: 220 Color: 42
Size: 216 Color: 41

Bin 108: 56 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 302
Size: 2572 Color: 246
Size: 168 Color: 22

Bin 109: 58 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 310
Size: 2402 Color: 240
Size: 152 Color: 16

Bin 110: 59 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 313
Size: 2213 Color: 233
Size: 140 Color: 11

Bin 111: 63 of cap free
Amount of items: 3
Items: 
Size: 4589 Color: 288
Size: 3204 Color: 264
Size: 224 Color: 43

Bin 112: 71 of cap free
Amount of items: 3
Items: 
Size: 4581 Color: 287
Size: 3202 Color: 263
Size: 226 Color: 45

Bin 113: 71 of cap free
Amount of items: 3
Items: 
Size: 5930 Color: 322
Size: 1991 Color: 226
Size: 88 Color: 4

Bin 114: 76 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 295
Size: 3008 Color: 259

Bin 115: 87 of cap free
Amount of items: 2
Items: 
Size: 4989 Color: 294
Size: 3004 Color: 258

Bin 116: 102 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 301
Size: 2564 Color: 245
Size: 168 Color: 23

Bin 117: 115 of cap free
Amount of items: 3
Items: 
Size: 5045 Color: 300
Size: 2748 Color: 251
Size: 172 Color: 24

Bin 118: 120 of cap free
Amount of items: 3
Items: 
Size: 5044 Color: 299
Size: 2744 Color: 250
Size: 172 Color: 25

Bin 119: 121 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 285
Size: 3194 Color: 262
Size: 232 Color: 48

Bin 120: 129 of cap free
Amount of items: 6
Items: 
Size: 4041 Color: 273
Size: 1234 Color: 184
Size: 1098 Color: 171
Size: 954 Color: 157
Size: 312 Color: 74
Size: 312 Color: 73

Bin 121: 129 of cap free
Amount of items: 3
Items: 
Size: 5037 Color: 298
Size: 2738 Color: 249
Size: 176 Color: 26

Bin 122: 133 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 292
Size: 2937 Color: 256
Size: 208 Color: 37

Bin 123: 138 of cap free
Amount of items: 4
Items: 
Size: 5013 Color: 297
Size: 2557 Color: 244
Size: 188 Color: 30
Size: 184 Color: 29

Bin 124: 152 of cap free
Amount of items: 3
Items: 
Size: 4798 Color: 291
Size: 2922 Color: 255
Size: 208 Color: 38

Bin 125: 152 of cap free
Amount of items: 4
Items: 
Size: 5004 Color: 296
Size: 2532 Color: 242
Size: 200 Color: 35
Size: 192 Color: 32

Bin 126: 163 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 290
Size: 2917 Color: 254
Size: 212 Color: 40

Bin 127: 170 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 284
Size: 3186 Color: 261
Size: 240 Color: 49

Bin 128: 211 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 282
Size: 3367 Color: 269
Size: 244 Color: 52

Bin 129: 214 of cap free
Amount of items: 4
Items: 
Size: 4156 Color: 278
Size: 3182 Color: 260
Size: 272 Color: 61
Size: 256 Color: 58

Bin 130: 217 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 281
Size: 3365 Color: 268
Size: 248 Color: 55

Bin 131: 222 of cap free
Amount of items: 3
Items: 
Size: 4242 Color: 279
Size: 3362 Color: 266
Size: 254 Color: 57

Bin 132: 222 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 280
Size: 3364 Color: 267
Size: 250 Color: 56

Bin 133: 4038 of cap free
Amount of items: 9
Items: 
Size: 496 Color: 111
Size: 496 Color: 110
Size: 464 Color: 109
Size: 442 Color: 105
Size: 440 Color: 103
Size: 440 Color: 102
Size: 432 Color: 101
Size: 416 Color: 98
Size: 416 Color: 97

Total size: 1066560
Total free space: 8080

