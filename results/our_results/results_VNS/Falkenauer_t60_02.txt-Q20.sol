Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 8
Size: 281 Color: 2
Size: 260 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 9
Size: 345 Color: 3
Size: 288 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 17
Size: 267 Color: 14
Size: 251 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 11
Size: 326 Color: 13
Size: 276 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 19
Size: 256 Color: 2
Size: 250 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 0
Size: 252 Color: 12
Size: 250 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 10
Size: 308 Color: 3
Size: 262 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 268 Color: 0
Size: 354 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 305 Color: 18
Size: 266 Color: 14
Size: 429 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 352 Color: 4
Size: 279 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 16
Size: 350 Color: 16
Size: 288 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 7
Size: 252 Color: 19
Size: 250 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 13
Size: 267 Color: 18
Size: 251 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 11
Size: 339 Color: 2
Size: 261 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 12
Size: 350 Color: 8
Size: 260 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 14
Size: 267 Color: 5
Size: 254 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 3
Size: 277 Color: 5
Size: 259 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 16
Size: 263 Color: 17
Size: 261 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 6
Size: 284 Color: 6
Size: 280 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 19
Size: 328 Color: 9
Size: 271 Color: 7

Total size: 20000
Total free space: 0

