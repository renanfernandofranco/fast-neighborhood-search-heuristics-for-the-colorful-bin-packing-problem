Capicity Bin: 8344
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4174 Color: 11
Size: 1584 Color: 16
Size: 1574 Color: 5
Size: 732 Color: 10
Size: 280 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4794 Color: 8
Size: 3006 Color: 2
Size: 544 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4769 Color: 19
Size: 2967 Color: 6
Size: 608 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5068 Color: 8
Size: 2964 Color: 4
Size: 312 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5508 Color: 2
Size: 2500 Color: 13
Size: 336 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5945 Color: 0
Size: 1801 Color: 3
Size: 598 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5970 Color: 2
Size: 1964 Color: 0
Size: 410 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6010 Color: 3
Size: 1946 Color: 7
Size: 388 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6124 Color: 10
Size: 2012 Color: 14
Size: 208 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 15
Size: 1821 Color: 16
Size: 362 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6201 Color: 1
Size: 1787 Color: 1
Size: 356 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6284 Color: 7
Size: 1612 Color: 5
Size: 448 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6585 Color: 17
Size: 1587 Color: 2
Size: 172 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6636 Color: 18
Size: 1390 Color: 2
Size: 318 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 18
Size: 1208 Color: 10
Size: 472 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6817 Color: 15
Size: 1273 Color: 18
Size: 254 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 8
Size: 802 Color: 16
Size: 688 Color: 16

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 12
Size: 1227 Color: 16
Size: 244 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6896 Color: 11
Size: 1128 Color: 13
Size: 320 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 3
Size: 1036 Color: 14
Size: 358 Color: 8

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7085 Color: 14
Size: 1051 Color: 2
Size: 208 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 14
Size: 1084 Color: 18
Size: 152 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7111 Color: 14
Size: 1029 Color: 14
Size: 204 Color: 8

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 1
Size: 1058 Color: 19
Size: 236 Color: 9

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 4
Size: 734 Color: 4
Size: 536 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 16
Size: 804 Color: 19
Size: 368 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 12
Size: 978 Color: 17
Size: 192 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 14
Size: 883 Color: 8
Size: 226 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7263 Color: 15
Size: 809 Color: 10
Size: 272 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 4
Size: 664 Color: 10
Size: 406 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7345 Color: 0
Size: 769 Color: 0
Size: 230 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7350 Color: 1
Size: 592 Color: 9
Size: 402 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 1
Size: 594 Color: 10
Size: 322 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7434 Color: 7
Size: 580 Color: 10
Size: 330 Color: 14

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 4890 Color: 8
Size: 3453 Color: 2

Bin 36: 1 of cap free
Amount of items: 4
Items: 
Size: 5221 Color: 9
Size: 2262 Color: 15
Size: 692 Color: 19
Size: 168 Color: 14

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5764 Color: 11
Size: 2053 Color: 0
Size: 526 Color: 18

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5921 Color: 19
Size: 2242 Color: 11
Size: 180 Color: 12

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 16
Size: 1987 Color: 14
Size: 424 Color: 13

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6050 Color: 16
Size: 2033 Color: 17
Size: 260 Color: 11

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 11
Size: 1082 Color: 16
Size: 1076 Color: 14

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6361 Color: 11
Size: 1594 Color: 0
Size: 388 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 8
Size: 1806 Color: 0
Size: 152 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 10
Size: 1601 Color: 6
Size: 316 Color: 18

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6441 Color: 11
Size: 1212 Color: 13
Size: 690 Color: 5

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 7
Size: 1262 Color: 5
Size: 520 Color: 17

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 5
Size: 694 Color: 3
Size: 688 Color: 19

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 6
Size: 982 Color: 0
Size: 320 Color: 13

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 7177 Color: 14
Size: 1062 Color: 1
Size: 104 Color: 2

Bin 50: 2 of cap free
Amount of items: 6
Items: 
Size: 4173 Color: 19
Size: 1365 Color: 10
Size: 1364 Color: 19
Size: 714 Color: 16
Size: 452 Color: 17
Size: 274 Color: 3

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5124 Color: 18
Size: 2978 Color: 3
Size: 240 Color: 10

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 1
Size: 1187 Color: 14
Size: 372 Color: 13

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 6425 Color: 3
Size: 1724 Color: 4
Size: 192 Color: 9

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 6826 Color: 13
Size: 1371 Color: 11
Size: 144 Color: 4

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 6921 Color: 15
Size: 1420 Color: 6

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 6985 Color: 3
Size: 764 Color: 11
Size: 592 Color: 5

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 7025 Color: 19
Size: 1316 Color: 5

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 17
Size: 3468 Color: 13
Size: 692 Color: 10

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 4201 Color: 5
Size: 3673 Color: 16
Size: 466 Color: 9

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 0
Size: 2914 Color: 14
Size: 176 Color: 10

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6458 Color: 6
Size: 1882 Color: 5

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 6707 Color: 13
Size: 1633 Color: 3

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 5609 Color: 15
Size: 2602 Color: 10
Size: 128 Color: 8

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 7375 Color: 10
Size: 924 Color: 12
Size: 40 Color: 14

Bin 65: 6 of cap free
Amount of items: 3
Items: 
Size: 5585 Color: 11
Size: 2617 Color: 2
Size: 136 Color: 17

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 5654 Color: 3
Size: 2684 Color: 6

Bin 67: 6 of cap free
Amount of items: 3
Items: 
Size: 7285 Color: 9
Size: 901 Color: 3
Size: 152 Color: 14

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7494 Color: 16
Size: 844 Color: 5

Bin 69: 7 of cap free
Amount of items: 5
Items: 
Size: 4177 Color: 19
Size: 2962 Color: 0
Size: 830 Color: 6
Size: 184 Color: 15
Size: 184 Color: 4

Bin 70: 7 of cap free
Amount of items: 3
Items: 
Size: 4190 Color: 12
Size: 2906 Color: 12
Size: 1241 Color: 3

Bin 71: 7 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 11
Size: 2001 Color: 18
Size: 246 Color: 19

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 6716 Color: 4
Size: 1621 Color: 3

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 7236 Color: 10
Size: 1101 Color: 0

Bin 74: 7 of cap free
Amount of items: 2
Items: 
Size: 7250 Color: 5
Size: 1087 Color: 11

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 19
Size: 1032 Color: 12

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 7423 Color: 17
Size: 914 Color: 2

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 7388 Color: 5
Size: 947 Color: 3

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 4198 Color: 17
Size: 4136 Color: 4

Bin 79: 10 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 18
Size: 2281 Color: 8
Size: 148 Color: 14

Bin 80: 10 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 11
Size: 2156 Color: 7

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 7332 Color: 4
Size: 1001 Color: 19

Bin 82: 11 of cap free
Amount of items: 3
Items: 
Size: 7382 Color: 8
Size: 925 Color: 12
Size: 26 Color: 5

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 7466 Color: 2
Size: 867 Color: 8

Bin 84: 12 of cap free
Amount of items: 3
Items: 
Size: 5881 Color: 6
Size: 2267 Color: 2
Size: 184 Color: 8

Bin 85: 12 of cap free
Amount of items: 3
Items: 
Size: 6699 Color: 8
Size: 1121 Color: 0
Size: 512 Color: 11

Bin 86: 12 of cap free
Amount of items: 2
Items: 
Size: 6926 Color: 16
Size: 1406 Color: 19

Bin 87: 12 of cap free
Amount of items: 3
Items: 
Size: 7143 Color: 3
Size: 1133 Color: 5
Size: 56 Color: 15

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 7150 Color: 0
Size: 1182 Color: 12

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 3
Size: 1162 Color: 5

Bin 90: 13 of cap free
Amount of items: 3
Items: 
Size: 4668 Color: 19
Size: 3403 Color: 7
Size: 260 Color: 4

Bin 91: 13 of cap free
Amount of items: 2
Items: 
Size: 4858 Color: 4
Size: 3473 Color: 2

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 6662 Color: 17
Size: 1668 Color: 3

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 7468 Color: 8
Size: 862 Color: 6

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 5348 Color: 18
Size: 2981 Color: 5

Bin 95: 15 of cap free
Amount of items: 2
Items: 
Size: 5996 Color: 1
Size: 2333 Color: 16

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 4850 Color: 10
Size: 3478 Color: 8

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 5569 Color: 4
Size: 2301 Color: 18
Size: 458 Color: 19

Bin 98: 16 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 15
Size: 1402 Color: 8
Size: 96 Color: 18

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 7044 Color: 6
Size: 1284 Color: 2

Bin 100: 17 of cap free
Amount of items: 3
Items: 
Size: 6643 Color: 15
Size: 1620 Color: 18
Size: 64 Color: 14

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 4
Size: 1914 Color: 7

Bin 102: 19 of cap free
Amount of items: 2
Items: 
Size: 5961 Color: 17
Size: 2364 Color: 14

Bin 103: 20 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 19
Size: 3001 Color: 5
Size: 1141 Color: 3

Bin 104: 20 of cap free
Amount of items: 3
Items: 
Size: 5226 Color: 18
Size: 2582 Color: 11
Size: 516 Color: 17

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 6857 Color: 12
Size: 1467 Color: 14

Bin 106: 21 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 13
Size: 2021 Color: 7
Size: 1962 Color: 10

Bin 107: 21 of cap free
Amount of items: 3
Items: 
Size: 4745 Color: 13
Size: 3458 Color: 0
Size: 120 Color: 3

Bin 108: 24 of cap free
Amount of items: 3
Items: 
Size: 6154 Color: 11
Size: 2114 Color: 3
Size: 52 Color: 8

Bin 109: 27 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 2
Size: 1545 Color: 19

Bin 110: 28 of cap free
Amount of items: 3
Items: 
Size: 6401 Color: 14
Size: 1153 Color: 14
Size: 762 Color: 11

Bin 111: 30 of cap free
Amount of items: 15
Items: 
Size: 1272 Color: 4
Size: 692 Color: 2
Size: 680 Color: 18
Size: 664 Color: 7
Size: 588 Color: 13
Size: 584 Color: 10
Size: 580 Color: 6
Size: 522 Color: 19
Size: 512 Color: 7
Size: 498 Color: 0
Size: 496 Color: 15
Size: 380 Color: 1
Size: 292 Color: 17
Size: 282 Color: 2
Size: 272 Color: 14

Bin 112: 30 of cap free
Amount of items: 3
Items: 
Size: 6404 Color: 0
Size: 1826 Color: 1
Size: 84 Color: 16

Bin 113: 33 of cap free
Amount of items: 2
Items: 
Size: 6658 Color: 0
Size: 1653 Color: 16

Bin 114: 33 of cap free
Amount of items: 2
Items: 
Size: 6892 Color: 12
Size: 1419 Color: 13

Bin 115: 37 of cap free
Amount of items: 2
Items: 
Size: 5994 Color: 4
Size: 2313 Color: 7

Bin 116: 37 of cap free
Amount of items: 2
Items: 
Size: 7001 Color: 3
Size: 1306 Color: 12

Bin 117: 42 of cap free
Amount of items: 2
Items: 
Size: 6450 Color: 8
Size: 1852 Color: 19

Bin 118: 44 of cap free
Amount of items: 3
Items: 
Size: 4188 Color: 8
Size: 3664 Color: 3
Size: 448 Color: 10

Bin 119: 44 of cap free
Amount of items: 2
Items: 
Size: 7314 Color: 1
Size: 986 Color: 15

Bin 120: 47 of cap free
Amount of items: 2
Items: 
Size: 6996 Color: 16
Size: 1301 Color: 5

Bin 121: 48 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 13
Size: 3340 Color: 9
Size: 160 Color: 17

Bin 122: 50 of cap free
Amount of items: 2
Items: 
Size: 6644 Color: 6
Size: 1650 Color: 10

Bin 123: 55 of cap free
Amount of items: 2
Items: 
Size: 5545 Color: 14
Size: 2744 Color: 6

Bin 124: 62 of cap free
Amount of items: 3
Items: 
Size: 5056 Color: 2
Size: 3004 Color: 6
Size: 222 Color: 19

Bin 125: 63 of cap free
Amount of items: 2
Items: 
Size: 4261 Color: 19
Size: 4020 Color: 12

Bin 126: 67 of cap free
Amount of items: 2
Items: 
Size: 5181 Color: 17
Size: 3096 Color: 7

Bin 127: 71 of cap free
Amount of items: 2
Items: 
Size: 5205 Color: 2
Size: 3068 Color: 16

Bin 128: 74 of cap free
Amount of items: 26
Items: 
Size: 472 Color: 18
Size: 462 Color: 8
Size: 454 Color: 18
Size: 452 Color: 13
Size: 400 Color: 12
Size: 400 Color: 5
Size: 398 Color: 8
Size: 396 Color: 15
Size: 384 Color: 2
Size: 364 Color: 6
Size: 360 Color: 1
Size: 326 Color: 4
Size: 300 Color: 1
Size: 280 Color: 16
Size: 280 Color: 15
Size: 268 Color: 17
Size: 264 Color: 18
Size: 256 Color: 4
Size: 252 Color: 9
Size: 240 Color: 17
Size: 238 Color: 16
Size: 236 Color: 7
Size: 216 Color: 11
Size: 212 Color: 14
Size: 200 Color: 3
Size: 160 Color: 14

Bin 129: 77 of cap free
Amount of items: 2
Items: 
Size: 5630 Color: 13
Size: 2637 Color: 15

Bin 130: 89 of cap free
Amount of items: 2
Items: 
Size: 4785 Color: 16
Size: 3470 Color: 11

Bin 131: 108 of cap free
Amount of items: 2
Items: 
Size: 4774 Color: 13
Size: 3462 Color: 0

Bin 132: 116 of cap free
Amount of items: 2
Items: 
Size: 5625 Color: 11
Size: 2603 Color: 0

Bin 133: 6414 of cap free
Amount of items: 11
Items: 
Size: 232 Color: 4
Size: 218 Color: 8
Size: 216 Color: 17
Size: 200 Color: 10
Size: 180 Color: 2
Size: 168 Color: 11
Size: 164 Color: 1
Size: 144 Color: 12
Size: 144 Color: 3
Size: 136 Color: 14
Size: 128 Color: 7

Total size: 1101408
Total free space: 8344

