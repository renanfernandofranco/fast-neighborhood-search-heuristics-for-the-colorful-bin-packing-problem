Capicity Bin: 9824
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 4916 Color: 0
Size: 856 Color: 0
Size: 816 Color: 0
Size: 704 Color: 1
Size: 688 Color: 1
Size: 672 Color: 1
Size: 648 Color: 1
Size: 352 Color: 1
Size: 172 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 1
Size: 3228 Color: 0
Size: 1648 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5456 Color: 1
Size: 4088 Color: 1
Size: 280 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5956 Color: 1
Size: 3664 Color: 0
Size: 204 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6144 Color: 0
Size: 3312 Color: 0
Size: 368 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 0
Size: 3033 Color: 0
Size: 606 Color: 1

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 6864 Color: 0
Size: 2724 Color: 1
Size: 160 Color: 1
Size: 76 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 0
Size: 2726 Color: 1
Size: 220 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 1
Size: 2440 Color: 1
Size: 480 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 0
Size: 1898 Color: 1
Size: 602 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 1
Size: 1640 Color: 0
Size: 340 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 8040 Color: 0
Size: 1208 Color: 1
Size: 576 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 8062 Color: 0
Size: 944 Color: 0
Size: 818 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 8080 Color: 0
Size: 1168 Color: 1
Size: 576 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8134 Color: 0
Size: 1088 Color: 0
Size: 602 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8092 Color: 1
Size: 1092 Color: 0
Size: 640 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8180 Color: 0
Size: 828 Color: 1
Size: 816 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8208 Color: 0
Size: 816 Color: 1
Size: 800 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 0
Size: 1336 Color: 1
Size: 148 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 1
Size: 1084 Color: 0
Size: 328 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8372 Color: 0
Size: 844 Color: 0
Size: 608 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8596 Color: 1
Size: 892 Color: 0
Size: 336 Color: 0

Bin 23: 0 of cap free
Amount of items: 4
Items: 
Size: 8756 Color: 1
Size: 972 Color: 0
Size: 64 Color: 0
Size: 32 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8812 Color: 1
Size: 808 Color: 0
Size: 204 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8768 Color: 0
Size: 800 Color: 1
Size: 256 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 0
Size: 700 Color: 0
Size: 288 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 0
Size: 3013 Color: 1
Size: 256 Color: 0

Bin 28: 2 of cap free
Amount of items: 10
Items: 
Size: 4914 Color: 0
Size: 712 Color: 0
Size: 712 Color: 0
Size: 640 Color: 1
Size: 624 Color: 0
Size: 592 Color: 1
Size: 568 Color: 1
Size: 544 Color: 1
Size: 352 Color: 0
Size: 164 Color: 1

Bin 29: 2 of cap free
Amount of items: 5
Items: 
Size: 4918 Color: 0
Size: 1848 Color: 1
Size: 1808 Color: 1
Size: 904 Color: 0
Size: 344 Color: 0

Bin 30: 2 of cap free
Amount of items: 5
Items: 
Size: 4932 Color: 0
Size: 2084 Color: 1
Size: 2006 Color: 1
Size: 544 Color: 0
Size: 256 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 5546 Color: 1
Size: 3900 Color: 0
Size: 376 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 1
Size: 3448 Color: 0
Size: 296 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 6514 Color: 1
Size: 3020 Color: 0
Size: 288 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 0
Size: 2356 Color: 0
Size: 180 Color: 1

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 7550 Color: 0
Size: 2272 Color: 1

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 7556 Color: 1
Size: 1578 Color: 0
Size: 688 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 8026 Color: 0
Size: 1588 Color: 0
Size: 208 Color: 1

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 8376 Color: 1
Size: 1446 Color: 0

Bin 39: 2 of cap free
Amount of items: 4
Items: 
Size: 8578 Color: 1
Size: 1120 Color: 0
Size: 76 Color: 1
Size: 48 Color: 0

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 8714 Color: 0
Size: 1108 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 8808 Color: 0
Size: 1014 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 8838 Color: 0
Size: 968 Color: 1
Size: 16 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 6074 Color: 1
Size: 3045 Color: 0
Size: 702 Color: 0

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 4952 Color: 0
Size: 4068 Color: 1
Size: 800 Color: 0

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 1
Size: 3000 Color: 0
Size: 264 Color: 1

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 1
Size: 2426 Color: 1
Size: 416 Color: 0

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 1
Size: 2544 Color: 0
Size: 272 Color: 0

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 7604 Color: 1
Size: 2216 Color: 0

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 8386 Color: 1
Size: 1114 Color: 0
Size: 320 Color: 1

Bin 50: 4 of cap free
Amount of items: 2
Items: 
Size: 8490 Color: 1
Size: 1330 Color: 0

Bin 51: 4 of cap free
Amount of items: 2
Items: 
Size: 8520 Color: 0
Size: 1300 Color: 1

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 8774 Color: 1
Size: 1046 Color: 0

Bin 53: 6 of cap free
Amount of items: 2
Items: 
Size: 7266 Color: 0
Size: 2552 Color: 1

Bin 54: 6 of cap free
Amount of items: 3
Items: 
Size: 7288 Color: 1
Size: 2394 Color: 0
Size: 136 Color: 1

Bin 55: 6 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 0
Size: 1410 Color: 1
Size: 192 Color: 0

Bin 56: 6 of cap free
Amount of items: 2
Items: 
Size: 8362 Color: 1
Size: 1456 Color: 0

Bin 57: 6 of cap free
Amount of items: 3
Items: 
Size: 8432 Color: 1
Size: 1322 Color: 0
Size: 64 Color: 1

Bin 58: 7 of cap free
Amount of items: 2
Items: 
Size: 6296 Color: 0
Size: 3521 Color: 1

Bin 59: 8 of cap free
Amount of items: 3
Items: 
Size: 5872 Color: 1
Size: 3122 Color: 0
Size: 822 Color: 0

Bin 60: 8 of cap free
Amount of items: 2
Items: 
Size: 7924 Color: 1
Size: 1892 Color: 0

Bin 61: 8 of cap free
Amount of items: 2
Items: 
Size: 8500 Color: 0
Size: 1316 Color: 1

Bin 62: 8 of cap free
Amount of items: 2
Items: 
Size: 8680 Color: 0
Size: 1136 Color: 1

Bin 63: 8 of cap free
Amount of items: 4
Items: 
Size: 8720 Color: 0
Size: 928 Color: 1
Size: 104 Color: 0
Size: 64 Color: 1

Bin 64: 8 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 0
Size: 1000 Color: 1
Size: 32 Color: 1

Bin 65: 10 of cap free
Amount of items: 2
Items: 
Size: 7440 Color: 0
Size: 2374 Color: 1

Bin 66: 12 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 1
Size: 1532 Color: 0
Size: 1444 Color: 0

Bin 67: 12 of cap free
Amount of items: 3
Items: 
Size: 7594 Color: 0
Size: 2090 Color: 1
Size: 128 Color: 0

Bin 68: 12 of cap free
Amount of items: 2
Items: 
Size: 8632 Color: 1
Size: 1180 Color: 0

Bin 69: 13 of cap free
Amount of items: 5
Items: 
Size: 6171 Color: 0
Size: 2458 Color: 1
Size: 918 Color: 1
Size: 176 Color: 1
Size: 88 Color: 0

Bin 70: 14 of cap free
Amount of items: 2
Items: 
Size: 5550 Color: 1
Size: 4260 Color: 0

Bin 71: 14 of cap free
Amount of items: 2
Items: 
Size: 7318 Color: 0
Size: 2492 Color: 1

Bin 72: 14 of cap free
Amount of items: 2
Items: 
Size: 8308 Color: 1
Size: 1502 Color: 0

Bin 73: 16 of cap free
Amount of items: 28
Items: 
Size: 488 Color: 0
Size: 484 Color: 0
Size: 480 Color: 0
Size: 476 Color: 0
Size: 472 Color: 0
Size: 464 Color: 0
Size: 416 Color: 1
Size: 416 Color: 0
Size: 416 Color: 0
Size: 384 Color: 1
Size: 384 Color: 1
Size: 376 Color: 1
Size: 352 Color: 0
Size: 352 Color: 0
Size: 320 Color: 1
Size: 320 Color: 1
Size: 312 Color: 1
Size: 312 Color: 0
Size: 312 Color: 0
Size: 304 Color: 0
Size: 292 Color: 0
Size: 288 Color: 1
Size: 280 Color: 1
Size: 240 Color: 1
Size: 228 Color: 1
Size: 224 Color: 1
Size: 224 Color: 1
Size: 192 Color: 1

Bin 74: 16 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 0
Size: 3024 Color: 1

Bin 75: 16 of cap free
Amount of items: 2
Items: 
Size: 7782 Color: 1
Size: 2026 Color: 0

Bin 76: 18 of cap free
Amount of items: 4
Items: 
Size: 4920 Color: 0
Size: 3496 Color: 0
Size: 878 Color: 1
Size: 512 Color: 1

Bin 77: 20 of cap free
Amount of items: 2
Items: 
Size: 8268 Color: 0
Size: 1536 Color: 1

Bin 78: 22 of cap free
Amount of items: 2
Items: 
Size: 7668 Color: 1
Size: 2134 Color: 0

Bin 79: 24 of cap free
Amount of items: 2
Items: 
Size: 7800 Color: 1
Size: 2000 Color: 0

Bin 80: 24 of cap free
Amount of items: 2
Items: 
Size: 8440 Color: 1
Size: 1360 Color: 0

Bin 81: 24 of cap free
Amount of items: 2
Items: 
Size: 8532 Color: 1
Size: 1268 Color: 0

Bin 82: 26 of cap free
Amount of items: 2
Items: 
Size: 6232 Color: 0
Size: 3566 Color: 1

Bin 83: 26 of cap free
Amount of items: 2
Items: 
Size: 8426 Color: 0
Size: 1372 Color: 1

Bin 84: 28 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 1
Size: 2396 Color: 1
Size: 624 Color: 0

Bin 85: 28 of cap free
Amount of items: 2
Items: 
Size: 8584 Color: 0
Size: 1212 Color: 1

Bin 86: 31 of cap free
Amount of items: 11
Items: 
Size: 4913 Color: 0
Size: 600 Color: 0
Size: 548 Color: 0
Size: 512 Color: 1
Size: 496 Color: 0
Size: 496 Color: 0
Size: 496 Color: 0
Size: 472 Color: 1
Size: 424 Color: 1
Size: 420 Color: 1
Size: 416 Color: 1

Bin 87: 32 of cap free
Amount of items: 2
Items: 
Size: 6840 Color: 1
Size: 2952 Color: 0

Bin 88: 32 of cap free
Amount of items: 2
Items: 
Size: 7056 Color: 1
Size: 2736 Color: 0

Bin 89: 32 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 1
Size: 2488 Color: 0

Bin 90: 32 of cap free
Amount of items: 2
Items: 
Size: 7864 Color: 0
Size: 1928 Color: 1

Bin 91: 32 of cap free
Amount of items: 2
Items: 
Size: 8744 Color: 0
Size: 1048 Color: 1

Bin 92: 34 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 1
Size: 2876 Color: 0

Bin 93: 34 of cap free
Amount of items: 2
Items: 
Size: 7742 Color: 0
Size: 2048 Color: 1

Bin 94: 34 of cap free
Amount of items: 2
Items: 
Size: 8088 Color: 1
Size: 1702 Color: 0

Bin 95: 36 of cap free
Amount of items: 3
Items: 
Size: 4944 Color: 0
Size: 4028 Color: 1
Size: 816 Color: 0

Bin 96: 36 of cap free
Amount of items: 2
Items: 
Size: 5704 Color: 1
Size: 4084 Color: 0

Bin 97: 36 of cap free
Amount of items: 2
Items: 
Size: 7664 Color: 1
Size: 2124 Color: 0

Bin 98: 38 of cap free
Amount of items: 2
Items: 
Size: 8434 Color: 1
Size: 1352 Color: 0

Bin 99: 38 of cap free
Amount of items: 2
Items: 
Size: 8496 Color: 0
Size: 1290 Color: 1

Bin 100: 42 of cap free
Amount of items: 2
Items: 
Size: 8620 Color: 1
Size: 1162 Color: 0

Bin 101: 44 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 1
Size: 3904 Color: 0
Size: 880 Color: 1

Bin 102: 44 of cap free
Amount of items: 2
Items: 
Size: 7934 Color: 1
Size: 1846 Color: 0

Bin 103: 48 of cap free
Amount of items: 2
Items: 
Size: 8298 Color: 1
Size: 1478 Color: 0

Bin 104: 48 of cap free
Amount of items: 2
Items: 
Size: 8660 Color: 0
Size: 1116 Color: 1

Bin 105: 53 of cap free
Amount of items: 2
Items: 
Size: 6209 Color: 1
Size: 3562 Color: 0

Bin 106: 54 of cap free
Amount of items: 2
Items: 
Size: 8610 Color: 1
Size: 1160 Color: 0

Bin 107: 56 of cap free
Amount of items: 2
Items: 
Size: 7184 Color: 1
Size: 2584 Color: 0

Bin 108: 56 of cap free
Amount of items: 2
Items: 
Size: 7716 Color: 1
Size: 2052 Color: 0

Bin 109: 56 of cap free
Amount of items: 2
Items: 
Size: 8726 Color: 0
Size: 1042 Color: 1

Bin 110: 60 of cap free
Amount of items: 2
Items: 
Size: 7284 Color: 0
Size: 2480 Color: 1

Bin 111: 60 of cap free
Amount of items: 2
Items: 
Size: 7856 Color: 0
Size: 1908 Color: 1

Bin 112: 70 of cap free
Amount of items: 2
Items: 
Size: 5660 Color: 0
Size: 4094 Color: 1

Bin 113: 71 of cap free
Amount of items: 5
Items: 
Size: 4917 Color: 0
Size: 1672 Color: 0
Size: 1652 Color: 0
Size: 808 Color: 1
Size: 704 Color: 1

Bin 114: 72 of cap free
Amount of items: 2
Items: 
Size: 7988 Color: 1
Size: 1764 Color: 0

Bin 115: 75 of cap free
Amount of items: 2
Items: 
Size: 6728 Color: 0
Size: 3021 Color: 1

Bin 116: 79 of cap free
Amount of items: 2
Items: 
Size: 6224 Color: 0
Size: 3521 Color: 1

Bin 117: 82 of cap free
Amount of items: 2
Items: 
Size: 7624 Color: 1
Size: 2118 Color: 0

Bin 118: 84 of cap free
Amount of items: 2
Items: 
Size: 6380 Color: 1
Size: 3360 Color: 0

Bin 119: 86 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 1
Size: 1688 Color: 0
Size: 1096 Color: 0

Bin 120: 88 of cap free
Amount of items: 2
Items: 
Size: 7512 Color: 1
Size: 2224 Color: 0

Bin 121: 91 of cap free
Amount of items: 2
Items: 
Size: 5640 Color: 0
Size: 4093 Color: 1

Bin 122: 96 of cap free
Amount of items: 2
Items: 
Size: 8232 Color: 0
Size: 1496 Color: 1

Bin 123: 100 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 1
Size: 4080 Color: 0

Bin 124: 106 of cap free
Amount of items: 2
Items: 
Size: 6956 Color: 0
Size: 2762 Color: 1

Bin 125: 110 of cap free
Amount of items: 2
Items: 
Size: 7610 Color: 0
Size: 2104 Color: 1

Bin 126: 113 of cap free
Amount of items: 2
Items: 
Size: 6204 Color: 1
Size: 3507 Color: 0

Bin 127: 122 of cap free
Amount of items: 2
Items: 
Size: 6576 Color: 0
Size: 3126 Color: 1

Bin 128: 124 of cap free
Amount of items: 2
Items: 
Size: 8230 Color: 0
Size: 1470 Color: 1

Bin 129: 132 of cap free
Amount of items: 2
Items: 
Size: 5601 Color: 0
Size: 4091 Color: 1

Bin 130: 135 of cap free
Amount of items: 2
Items: 
Size: 5599 Color: 0
Size: 4090 Color: 1

Bin 131: 135 of cap free
Amount of items: 2
Items: 
Size: 5617 Color: 1
Size: 4072 Color: 0

Bin 132: 147 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 0
Size: 3476 Color: 1

Bin 133: 6064 of cap free
Amount of items: 19
Items: 
Size: 256 Color: 0
Size: 248 Color: 0
Size: 240 Color: 0
Size: 232 Color: 0
Size: 224 Color: 1
Size: 216 Color: 1
Size: 208 Color: 0
Size: 200 Color: 0
Size: 192 Color: 1
Size: 192 Color: 1
Size: 192 Color: 1
Size: 192 Color: 0
Size: 176 Color: 1
Size: 176 Color: 0
Size: 168 Color: 0
Size: 168 Color: 0
Size: 160 Color: 1
Size: 160 Color: 1
Size: 160 Color: 1

Total size: 1296768
Total free space: 9824

