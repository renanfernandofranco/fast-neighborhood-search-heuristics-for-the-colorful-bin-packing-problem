Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 1
Size: 470 Color: 0
Size: 449 Color: 3
Size: 200 Color: 2
Size: 108 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 0
Size: 1020 Color: 3
Size: 72 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 2
Size: 916 Color: 1
Size: 72 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1556 Color: 2
Size: 828 Color: 0
Size: 72 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 2
Size: 719 Color: 1
Size: 142 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 0
Size: 684 Color: 1
Size: 128 Color: 4

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 1707 Color: 3
Size: 653 Color: 4
Size: 48 Color: 2
Size: 48 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 2
Size: 620 Color: 2
Size: 120 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1926 Color: 1
Size: 370 Color: 3
Size: 160 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 0
Size: 508 Color: 4
Size: 8 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 1
Size: 379 Color: 3
Size: 80 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2002 Color: 1
Size: 232 Color: 3
Size: 222 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2012 Color: 3
Size: 436 Color: 2
Size: 8 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 3
Size: 306 Color: 2
Size: 60 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 3
Size: 300 Color: 2
Size: 40 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2139 Color: 3
Size: 265 Color: 4
Size: 52 Color: 1

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 2100 Color: 2
Size: 324 Color: 1
Size: 24 Color: 4
Size: 8 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 1
Size: 261 Color: 4
Size: 50 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 1
Size: 244 Color: 3
Size: 64 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 3
Size: 204 Color: 4
Size: 62 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 1
Size: 176 Color: 2
Size: 92 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 212 Color: 0
Size: 40 Color: 3

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 1021 Color: 0
Size: 204 Color: 4

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 2
Size: 756 Color: 3

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 564 Color: 0
Size: 92 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 0
Size: 555 Color: 4
Size: 48 Color: 4

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2003 Color: 4
Size: 364 Color: 3
Size: 88 Color: 1

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 3
Size: 384 Color: 1
Size: 60 Color: 0

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 3
Size: 369 Color: 1
Size: 8 Color: 4

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 0
Size: 753 Color: 3
Size: 467 Color: 3

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 0
Size: 538 Color: 3
Size: 152 Color: 1

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 4
Size: 460 Color: 0

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2015 Color: 1
Size: 383 Color: 3
Size: 56 Color: 3

Bin 34: 2 of cap free
Amount of items: 5
Items: 
Size: 2028 Color: 3
Size: 382 Color: 0
Size: 16 Color: 1
Size: 16 Color: 0
Size: 12 Color: 4

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2192 Color: 4
Size: 262 Color: 2

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1897 Color: 2
Size: 512 Color: 0
Size: 44 Color: 1

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 2081 Color: 0
Size: 372 Color: 2

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 2082 Color: 2
Size: 371 Color: 0

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 1
Size: 307 Color: 2

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 2
Size: 862 Color: 1
Size: 64 Color: 4

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 2
Size: 778 Color: 0

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 1
Size: 568 Color: 0
Size: 90 Color: 4

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 0
Size: 610 Color: 1
Size: 32 Color: 0

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 2
Size: 386 Color: 1
Size: 156 Color: 4

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1911 Color: 4
Size: 541 Color: 3

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 2
Size: 280 Color: 4

Bin 47: 5 of cap free
Amount of items: 3
Items: 
Size: 1231 Color: 2
Size: 976 Color: 1
Size: 244 Color: 3

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1437 Color: 1
Size: 938 Color: 1
Size: 76 Color: 0

Bin 49: 5 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 1
Size: 442 Color: 3
Size: 313 Color: 2

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 2089 Color: 3
Size: 362 Color: 4

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 1908 Color: 0
Size: 542 Color: 4

Bin 52: 6 of cap free
Amount of items: 3
Items: 
Size: 1919 Color: 3
Size: 455 Color: 0
Size: 76 Color: 1

Bin 53: 7 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 3
Size: 1023 Color: 1

Bin 54: 7 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 1
Size: 851 Color: 2
Size: 88 Color: 3

Bin 55: 8 of cap free
Amount of items: 2
Items: 
Size: 1334 Color: 1
Size: 1114 Color: 2

Bin 56: 8 of cap free
Amount of items: 2
Items: 
Size: 1782 Color: 4
Size: 666 Color: 3

Bin 57: 8 of cap free
Amount of items: 2
Items: 
Size: 1894 Color: 1
Size: 554 Color: 3

Bin 58: 10 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 1
Size: 620 Color: 4
Size: 168 Color: 0

Bin 59: 13 of cap free
Amount of items: 2
Items: 
Size: 1603 Color: 2
Size: 840 Color: 4

Bin 60: 15 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 3
Size: 857 Color: 4
Size: 48 Color: 1

Bin 61: 18 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 4
Size: 631 Color: 3

Bin 62: 20 of cap free
Amount of items: 18
Items: 
Size: 272 Color: 3
Size: 260 Color: 4
Size: 200 Color: 4
Size: 168 Color: 1
Size: 144 Color: 4
Size: 132 Color: 2
Size: 128 Color: 0
Size: 126 Color: 2
Size: 112 Color: 3
Size: 112 Color: 1
Size: 108 Color: 3
Size: 104 Color: 0
Size: 102 Color: 0
Size: 100 Color: 4
Size: 96 Color: 3
Size: 96 Color: 0
Size: 88 Color: 4
Size: 88 Color: 2

Bin 63: 22 of cap free
Amount of items: 2
Items: 
Size: 1780 Color: 3
Size: 654 Color: 4

Bin 64: 28 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 3
Size: 1022 Color: 0
Size: 170 Color: 1

Bin 65: 53 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 4
Size: 790 Color: 1
Size: 184 Color: 0

Bin 66: 2148 of cap free
Amount of items: 4
Items: 
Size: 88 Color: 0
Size: 74 Color: 2
Size: 74 Color: 2
Size: 72 Color: 4

Total size: 159640
Total free space: 2456

