Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 210
Size: 312 Color: 124
Size: 268 Color: 55

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 186
Size: 367 Color: 172
Size: 251 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 7
Size: 250 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 160
Size: 352 Color: 158
Size: 295 Color: 100

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 168
Size: 320 Color: 133
Size: 317 Color: 131

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 182
Size: 338 Color: 149
Size: 283 Color: 84

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 177
Size: 365 Color: 170
Size: 259 Color: 33

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 240
Size: 263 Color: 45
Size: 250 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 199
Size: 314 Color: 128
Size: 283 Color: 85

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 222
Size: 287 Color: 89
Size: 270 Color: 58

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 234
Size: 276 Color: 70
Size: 253 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 244
Size: 259 Color: 31
Size: 252 Color: 13

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 205
Size: 313 Color: 127
Size: 275 Color: 69

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 193
Size: 346 Color: 154
Size: 265 Color: 50

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 238
Size: 268 Color: 54
Size: 252 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 191
Size: 310 Color: 121
Size: 304 Color: 113

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 209
Size: 320 Color: 134
Size: 261 Color: 41

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 197
Size: 333 Color: 144
Size: 267 Color: 51

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 180
Size: 323 Color: 136
Size: 299 Color: 106

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 175
Size: 353 Color: 159
Size: 273 Color: 64

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 220
Size: 312 Color: 123
Size: 250 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 173
Size: 329 Color: 141
Size: 301 Color: 111

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 230
Size: 278 Color: 75
Size: 259 Color: 32

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 148
Size: 335 Color: 146
Size: 329 Color: 140

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 223
Size: 304 Color: 114
Size: 252 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 208
Size: 297 Color: 101
Size: 285 Color: 87

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 211
Size: 307 Color: 117
Size: 271 Color: 60

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 242
Size: 260 Color: 36
Size: 251 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 212
Size: 289 Color: 92
Size: 286 Color: 88

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 239
Size: 264 Color: 48
Size: 252 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 185
Size: 360 Color: 164
Size: 259 Color: 35

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 178
Size: 366 Color: 171
Size: 258 Color: 30

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 179
Size: 324 Color: 137
Size: 299 Color: 105

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 189
Size: 356 Color: 161
Size: 260 Color: 39

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 207
Size: 307 Color: 116
Size: 279 Color: 77

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 243
Size: 257 Color: 27
Size: 254 Color: 22

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 221
Size: 295 Color: 99
Size: 265 Color: 49

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 202
Size: 326 Color: 138
Size: 269 Color: 56

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 231
Size: 285 Color: 86
Size: 252 Color: 15

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 187
Size: 319 Color: 132
Size: 299 Color: 107

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 225
Size: 279 Color: 76
Size: 274 Color: 65

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 232
Size: 282 Color: 82
Size: 252 Color: 16

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 226
Size: 297 Color: 104
Size: 253 Color: 19

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 206
Size: 297 Color: 103
Size: 290 Color: 93

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 203
Size: 335 Color: 147
Size: 258 Color: 28

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 192
Size: 350 Color: 155
Size: 262 Color: 42

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 219
Size: 300 Color: 108
Size: 262 Color: 43

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 183
Size: 312 Color: 125
Size: 308 Color: 119

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 224
Size: 279 Color: 79
Size: 276 Color: 74

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 235
Size: 274 Color: 66
Size: 254 Color: 21

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 241
Size: 260 Color: 37
Size: 252 Color: 14

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 214
Size: 300 Color: 110
Size: 275 Color: 68

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 217
Size: 273 Color: 61
Size: 294 Color: 97

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 247
Size: 252 Color: 9
Size: 250 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 200
Size: 313 Color: 126
Size: 283 Color: 83

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 213
Size: 315 Color: 129
Size: 260 Color: 38

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 233
Size: 281 Color: 81
Size: 252 Color: 11

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 190
Size: 345 Color: 153
Size: 270 Color: 59

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 245
Size: 255 Color: 25
Size: 254 Color: 20

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 236
Size: 263 Color: 44
Size: 259 Color: 34

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 162
Size: 350 Color: 156
Size: 293 Color: 96

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 229
Size: 270 Color: 57
Size: 267 Color: 52

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 194
Size: 309 Color: 120
Size: 297 Color: 102

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 228
Size: 276 Color: 73
Size: 263 Color: 46

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 188
Size: 315 Color: 130
Size: 301 Color: 112

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 204
Size: 328 Color: 139
Size: 261 Color: 40

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 215
Size: 293 Color: 95
Size: 276 Color: 72

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 195
Size: 330 Color: 143
Size: 276 Color: 71

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 237
Size: 268 Color: 53
Size: 253 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 227
Size: 274 Color: 67
Size: 273 Color: 63

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 157
Size: 343 Color: 151
Size: 305 Color: 115

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 181
Size: 330 Color: 142
Size: 291 Color: 94

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 169
Size: 362 Color: 166
Size: 273 Color: 62

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 198
Size: 334 Color: 145
Size: 263 Color: 47

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 246
Size: 257 Color: 26
Size: 250 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 176
Size: 371 Color: 174
Size: 255 Color: 23

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 184
Size: 361 Color: 165
Size: 258 Color: 29

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 196
Size: 321 Color: 135
Size: 280 Color: 80

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 216
Size: 288 Color: 90
Size: 279 Color: 78

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 167
Size: 343 Color: 152
Size: 294 Color: 98

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 218
Size: 310 Color: 122
Size: 255 Color: 24

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 201
Size: 307 Color: 118
Size: 289 Color: 91

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 163
Size: 342 Color: 150
Size: 300 Color: 109

Total size: 83000
Total free space: 0

