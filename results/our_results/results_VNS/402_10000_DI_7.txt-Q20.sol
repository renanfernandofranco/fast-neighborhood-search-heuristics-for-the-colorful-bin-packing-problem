Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4078 Color: 18
Size: 3382 Color: 5
Size: 676 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4352 Color: 14
Size: 3372 Color: 18
Size: 412 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4626 Color: 9
Size: 2926 Color: 5
Size: 584 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4779 Color: 2
Size: 3149 Color: 15
Size: 208 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5462 Color: 2
Size: 2542 Color: 14
Size: 132 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 3
Size: 1906 Color: 17
Size: 380 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5994 Color: 9
Size: 1914 Color: 6
Size: 228 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6026 Color: 9
Size: 1762 Color: 3
Size: 348 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 5
Size: 1874 Color: 18
Size: 228 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 5
Size: 1660 Color: 18
Size: 360 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6121 Color: 0
Size: 1511 Color: 3
Size: 504 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 2
Size: 1524 Color: 4
Size: 248 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 3
Size: 1554 Color: 18
Size: 128 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6444 Color: 1
Size: 1484 Color: 13
Size: 208 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6502 Color: 6
Size: 1362 Color: 19
Size: 272 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 4
Size: 1236 Color: 9
Size: 380 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 14
Size: 892 Color: 9
Size: 584 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 9
Size: 1092 Color: 4
Size: 288 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6777 Color: 13
Size: 995 Color: 0
Size: 364 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6782 Color: 8
Size: 922 Color: 13
Size: 432 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6805 Color: 14
Size: 1133 Color: 15
Size: 198 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 17
Size: 1062 Color: 14
Size: 208 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 14
Size: 802 Color: 4
Size: 372 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 16
Size: 630 Color: 7
Size: 552 Color: 17

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 6
Size: 1130 Color: 3
Size: 50 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 14
Size: 852 Color: 6
Size: 280 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7034 Color: 10
Size: 722 Color: 6
Size: 380 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7042 Color: 10
Size: 874 Color: 14
Size: 220 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7081 Color: 3
Size: 881 Color: 18
Size: 174 Color: 19

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7082 Color: 10
Size: 820 Color: 13
Size: 234 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7116 Color: 15
Size: 676 Color: 11
Size: 344 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 19
Size: 672 Color: 12
Size: 322 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 10
Size: 766 Color: 11
Size: 176 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 19
Size: 780 Color: 12
Size: 152 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 8
Size: 672 Color: 5
Size: 196 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7270 Color: 4
Size: 706 Color: 2
Size: 160 Color: 17

Bin 37: 1 of cap free
Amount of items: 16
Items: 
Size: 882 Color: 0
Size: 881 Color: 18
Size: 682 Color: 5
Size: 624 Color: 2
Size: 548 Color: 5
Size: 512 Color: 15
Size: 508 Color: 16
Size: 508 Color: 4
Size: 476 Color: 14
Size: 444 Color: 1
Size: 432 Color: 10
Size: 424 Color: 18
Size: 384 Color: 3
Size: 378 Color: 13
Size: 244 Color: 7
Size: 208 Color: 18

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 4069 Color: 5
Size: 2922 Color: 8
Size: 1144 Color: 4

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4796 Color: 13
Size: 3131 Color: 4
Size: 208 Color: 12

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4839 Color: 12
Size: 3160 Color: 10
Size: 136 Color: 12

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5082 Color: 1
Size: 2749 Color: 14
Size: 304 Color: 17

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 13
Size: 2167 Color: 6
Size: 172 Color: 13

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6269 Color: 14
Size: 1642 Color: 12
Size: 224 Color: 15

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 16
Size: 997 Color: 1
Size: 676 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6551 Color: 3
Size: 1398 Color: 4
Size: 186 Color: 9

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 2
Size: 1221 Color: 13
Size: 288 Color: 13

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 2
Size: 1150 Color: 9
Size: 328 Color: 9

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6721 Color: 16
Size: 982 Color: 18
Size: 432 Color: 5

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 15
Size: 1363 Color: 6

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 10
Size: 734 Color: 13
Size: 464 Color: 6

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6943 Color: 14
Size: 1000 Color: 10
Size: 192 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 4299 Color: 18
Size: 3235 Color: 17
Size: 600 Color: 1

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 4387 Color: 3
Size: 3391 Color: 8
Size: 356 Color: 18

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 16
Size: 2164 Color: 16
Size: 136 Color: 19

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5991 Color: 3
Size: 1891 Color: 4
Size: 252 Color: 1

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 11
Size: 1754 Color: 12
Size: 448 Color: 9

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 16
Size: 1412 Color: 3
Size: 448 Color: 11

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6346 Color: 5
Size: 1140 Color: 7
Size: 648 Color: 4

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6580 Color: 9
Size: 1554 Color: 7

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6868 Color: 16
Size: 930 Color: 9
Size: 336 Color: 8

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 9
Size: 672 Color: 18
Size: 172 Color: 14

Bin 62: 3 of cap free
Amount of items: 6
Items: 
Size: 4070 Color: 17
Size: 1211 Color: 7
Size: 988 Color: 9
Size: 900 Color: 8
Size: 708 Color: 3
Size: 256 Color: 15

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 4355 Color: 15
Size: 3378 Color: 14
Size: 400 Color: 13

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 4500 Color: 15
Size: 3633 Color: 19

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 5497 Color: 17
Size: 2636 Color: 4

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 3
Size: 1181 Color: 18

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 7022 Color: 4
Size: 1111 Color: 16

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 4092 Color: 19
Size: 3560 Color: 0
Size: 480 Color: 11

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5898 Color: 4
Size: 1842 Color: 16
Size: 392 Color: 10

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6266 Color: 1
Size: 1866 Color: 15

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 6479 Color: 12
Size: 1557 Color: 7
Size: 96 Color: 3

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 6
Size: 1042 Color: 8

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 7218 Color: 18
Size: 914 Color: 1

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 5537 Color: 16
Size: 2410 Color: 3
Size: 184 Color: 18

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6750 Color: 3
Size: 1381 Color: 16

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 0
Size: 2776 Color: 13
Size: 264 Color: 9

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 5890 Color: 9
Size: 1956 Color: 7
Size: 284 Color: 5

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6886 Color: 1
Size: 1244 Color: 4

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 7079 Color: 19
Size: 1051 Color: 6

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 7114 Color: 9
Size: 1016 Color: 2

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 4630 Color: 17
Size: 3151 Color: 11
Size: 348 Color: 0

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 6685 Color: 17
Size: 1444 Color: 18

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 9
Size: 1069 Color: 13

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 4700 Color: 18
Size: 3428 Color: 16

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 7021 Color: 10
Size: 1107 Color: 16

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 12
Size: 1060 Color: 15

Bin 87: 8 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 11
Size: 854 Color: 0
Size: 16 Color: 17

Bin 88: 9 of cap free
Amount of items: 3
Items: 
Size: 5466 Color: 3
Size: 2385 Color: 0
Size: 276 Color: 8

Bin 89: 9 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 17
Size: 2395 Color: 0
Size: 184 Color: 5

Bin 90: 9 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 15
Size: 931 Color: 16
Size: 40 Color: 7

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 5474 Color: 1
Size: 2546 Color: 17
Size: 106 Color: 9

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 18
Size: 1742 Color: 8
Size: 602 Color: 8

Bin 93: 10 of cap free
Amount of items: 2
Items: 
Size: 6282 Color: 11
Size: 1844 Color: 4

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 7322 Color: 6
Size: 804 Color: 8

Bin 95: 11 of cap free
Amount of items: 3
Items: 
Size: 4379 Color: 2
Size: 3390 Color: 13
Size: 356 Color: 0

Bin 96: 13 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 19
Size: 1922 Color: 9

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 11
Size: 948 Color: 13

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 7292 Color: 10
Size: 830 Color: 16

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 5845 Color: 14
Size: 2276 Color: 4

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 6855 Color: 14
Size: 1266 Color: 12

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 1
Size: 1300 Color: 16

Bin 102: 18 of cap free
Amount of items: 4
Items: 
Size: 5412 Color: 3
Size: 1613 Color: 3
Size: 989 Color: 14
Size: 104 Color: 7

Bin 103: 19 of cap free
Amount of items: 2
Items: 
Size: 7017 Color: 13
Size: 1100 Color: 6

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 5842 Color: 5
Size: 2274 Color: 2

Bin 105: 21 of cap free
Amount of items: 2
Items: 
Size: 6050 Color: 13
Size: 2065 Color: 19

Bin 106: 21 of cap free
Amount of items: 2
Items: 
Size: 7089 Color: 19
Size: 1026 Color: 17

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 6325 Color: 16
Size: 1789 Color: 19

Bin 108: 23 of cap free
Amount of items: 2
Items: 
Size: 5077 Color: 9
Size: 3036 Color: 1

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 11
Size: 1494 Color: 14

Bin 110: 26 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 18
Size: 1962 Color: 6

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 6877 Color: 8
Size: 1233 Color: 13

Bin 112: 27 of cap free
Amount of items: 2
Items: 
Size: 6951 Color: 4
Size: 1158 Color: 6

Bin 113: 31 of cap free
Amount of items: 2
Items: 
Size: 6421 Color: 14
Size: 1684 Color: 5

Bin 114: 31 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 12
Size: 933 Color: 8

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 5930 Color: 13
Size: 2174 Color: 9

Bin 116: 32 of cap free
Amount of items: 2
Items: 
Size: 6673 Color: 14
Size: 1431 Color: 7

Bin 117: 42 of cap free
Amount of items: 2
Items: 
Size: 6308 Color: 7
Size: 1786 Color: 14

Bin 118: 43 of cap free
Amount of items: 2
Items: 
Size: 5867 Color: 6
Size: 2226 Color: 10

Bin 119: 44 of cap free
Amount of items: 2
Items: 
Size: 4076 Color: 2
Size: 4016 Color: 6

Bin 120: 46 of cap free
Amount of items: 2
Items: 
Size: 6828 Color: 16
Size: 1262 Color: 11

Bin 121: 49 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 14
Size: 2127 Color: 0
Size: 724 Color: 3

Bin 122: 51 of cap free
Amount of items: 3
Items: 
Size: 4071 Color: 19
Size: 3388 Color: 14
Size: 626 Color: 17

Bin 123: 51 of cap free
Amount of items: 2
Items: 
Size: 6404 Color: 14
Size: 1681 Color: 12

Bin 124: 52 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 0
Size: 3212 Color: 0
Size: 786 Color: 6

Bin 125: 55 of cap free
Amount of items: 2
Items: 
Size: 5530 Color: 14
Size: 2551 Color: 8

Bin 126: 57 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 14
Size: 3643 Color: 3

Bin 127: 57 of cap free
Amount of items: 2
Items: 
Size: 5659 Color: 9
Size: 2420 Color: 2

Bin 128: 57 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 19
Size: 1321 Color: 8

Bin 129: 73 of cap free
Amount of items: 2
Items: 
Size: 5275 Color: 19
Size: 2788 Color: 6

Bin 130: 73 of cap free
Amount of items: 2
Items: 
Size: 6501 Color: 17
Size: 1562 Color: 9

Bin 131: 76 of cap free
Amount of items: 2
Items: 
Size: 5036 Color: 14
Size: 3024 Color: 2

Bin 132: 78 of cap free
Amount of items: 34
Items: 
Size: 372 Color: 9
Size: 352 Color: 10
Size: 334 Color: 0
Size: 310 Color: 10
Size: 308 Color: 5
Size: 308 Color: 3
Size: 300 Color: 13
Size: 296 Color: 8
Size: 276 Color: 9
Size: 272 Color: 18
Size: 272 Color: 8
Size: 252 Color: 1
Size: 246 Color: 10
Size: 242 Color: 15
Size: 240 Color: 16
Size: 240 Color: 10
Size: 226 Color: 3
Size: 224 Color: 9
Size: 224 Color: 5
Size: 216 Color: 7
Size: 216 Color: 1
Size: 212 Color: 14
Size: 212 Color: 11
Size: 192 Color: 0
Size: 184 Color: 7
Size: 184 Color: 2
Size: 180 Color: 16
Size: 180 Color: 7
Size: 176 Color: 15
Size: 176 Color: 13
Size: 168 Color: 1
Size: 164 Color: 17
Size: 160 Color: 3
Size: 144 Color: 8

Bin 133: 6524 of cap free
Amount of items: 11
Items: 
Size: 168 Color: 5
Size: 164 Color: 15
Size: 160 Color: 1
Size: 156 Color: 19
Size: 156 Color: 4
Size: 152 Color: 17
Size: 144 Color: 8
Size: 144 Color: 0
Size: 140 Color: 12
Size: 136 Color: 7
Size: 92 Color: 3

Total size: 1073952
Total free space: 8136

