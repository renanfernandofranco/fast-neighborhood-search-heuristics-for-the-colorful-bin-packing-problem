Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 1
Size: 1028 Color: 1
Size: 200 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1415 Color: 1
Size: 881 Color: 1
Size: 176 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 0
Size: 828 Color: 1
Size: 160 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1482 Color: 1
Size: 840 Color: 1
Size: 150 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 0
Size: 276 Color: 1
Size: 258 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 1
Size: 450 Color: 1
Size: 88 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 0
Size: 272 Color: 1
Size: 244 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 0
Size: 273 Color: 1
Size: 180 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 0
Size: 278 Color: 1
Size: 160 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2142 Color: 1
Size: 226 Color: 0
Size: 104 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 0
Size: 150 Color: 0
Size: 120 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 1
Size: 297 Color: 1
Size: 22 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 214 Color: 0
Size: 54 Color: 1

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 2218 Color: 0
Size: 196 Color: 1
Size: 32 Color: 0
Size: 26 Color: 1

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1604 Color: 0
Size: 747 Color: 1
Size: 120 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 0
Size: 753 Color: 1
Size: 48 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 1
Size: 491 Color: 1
Size: 192 Color: 0

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 0
Size: 645 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 1
Size: 476 Color: 0
Size: 48 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 2027 Color: 0
Size: 370 Color: 1
Size: 74 Color: 1

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 2035 Color: 0
Size: 436 Color: 1

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 2105 Color: 1
Size: 366 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2137 Color: 0
Size: 318 Color: 0
Size: 16 Color: 1

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 2148 Color: 0
Size: 307 Color: 1
Size: 8 Color: 1
Size: 8 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 1
Size: 724 Color: 1
Size: 60 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 1
Size: 686 Color: 0
Size: 80 Color: 0

Bin 27: 3 of cap free
Amount of items: 22
Items: 
Size: 379 Color: 0
Size: 272 Color: 0
Size: 136 Color: 0
Size: 132 Color: 0
Size: 130 Color: 0
Size: 128 Color: 0
Size: 128 Color: 0
Size: 112 Color: 0
Size: 108 Color: 0
Size: 98 Color: 1
Size: 96 Color: 0
Size: 88 Color: 1
Size: 88 Color: 1
Size: 86 Color: 1
Size: 72 Color: 1
Size: 72 Color: 1
Size: 64 Color: 1
Size: 64 Color: 1
Size: 64 Color: 1
Size: 64 Color: 1
Size: 52 Color: 0
Size: 36 Color: 1

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1699 Color: 1
Size: 658 Color: 1
Size: 112 Color: 0

Bin 29: 3 of cap free
Amount of items: 2
Items: 
Size: 1799 Color: 0
Size: 670 Color: 1

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1818 Color: 0
Size: 651 Color: 1

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 2030 Color: 0
Size: 439 Color: 1

Bin 32: 3 of cap free
Amount of items: 2
Items: 
Size: 2044 Color: 1
Size: 425 Color: 0

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 0
Size: 281 Color: 1
Size: 48 Color: 1

Bin 34: 3 of cap free
Amount of items: 4
Items: 
Size: 2145 Color: 0
Size: 284 Color: 1
Size: 24 Color: 1
Size: 16 Color: 0

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 2166 Color: 0
Size: 302 Color: 1

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 1
Size: 830 Color: 1
Size: 76 Color: 0

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1963 Color: 1
Size: 504 Color: 0

Bin 38: 5 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 1
Size: 349 Color: 0

Bin 39: 6 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 978 Color: 0
Size: 176 Color: 1
Size: 74 Color: 0

Bin 40: 6 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 0
Size: 662 Color: 1
Size: 96 Color: 0

Bin 41: 7 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 1
Size: 1022 Color: 1
Size: 204 Color: 0

Bin 42: 9 of cap free
Amount of items: 2
Items: 
Size: 2079 Color: 0
Size: 384 Color: 1

Bin 43: 10 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 1
Size: 567 Color: 0
Size: 204 Color: 0

Bin 44: 10 of cap free
Amount of items: 2
Items: 
Size: 2097 Color: 1
Size: 365 Color: 0

Bin 45: 12 of cap free
Amount of items: 2
Items: 
Size: 1577 Color: 1
Size: 883 Color: 0

Bin 46: 12 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 1
Size: 810 Color: 0

Bin 47: 13 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 1
Size: 561 Color: 0
Size: 329 Color: 0

Bin 48: 13 of cap free
Amount of items: 2
Items: 
Size: 2013 Color: 1
Size: 446 Color: 0

Bin 49: 14 of cap free
Amount of items: 2
Items: 
Size: 1404 Color: 1
Size: 1054 Color: 0

Bin 50: 14 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 1
Size: 542 Color: 0
Size: 364 Color: 0

Bin 51: 15 of cap free
Amount of items: 2
Items: 
Size: 1885 Color: 1
Size: 572 Color: 0

Bin 52: 16 of cap free
Amount of items: 2
Items: 
Size: 1860 Color: 0
Size: 596 Color: 1

Bin 53: 17 of cap free
Amount of items: 6
Items: 
Size: 1237 Color: 0
Size: 431 Color: 0
Size: 383 Color: 0
Size: 160 Color: 1
Size: 132 Color: 1
Size: 112 Color: 1

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1908 Color: 0
Size: 546 Color: 1

Bin 55: 19 of cap free
Amount of items: 2
Items: 
Size: 1877 Color: 0
Size: 576 Color: 1

Bin 56: 20 of cap free
Amount of items: 2
Items: 
Size: 1955 Color: 0
Size: 497 Color: 1

Bin 57: 21 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 0
Size: 1029 Color: 1
Size: 144 Color: 0

Bin 58: 22 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 1000 Color: 1
Size: 204 Color: 0

Bin 59: 22 of cap free
Amount of items: 2
Items: 
Size: 1502 Color: 0
Size: 948 Color: 1

Bin 60: 22 of cap free
Amount of items: 2
Items: 
Size: 2110 Color: 0
Size: 340 Color: 1

Bin 61: 24 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 1
Size: 998 Color: 0
Size: 148 Color: 0

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1413 Color: 0
Size: 1031 Color: 1

Bin 63: 33 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 0
Size: 761 Color: 1

Bin 64: 33 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 0
Size: 371 Color: 1

Bin 65: 35 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 1
Size: 644 Color: 0

Bin 66: 1944 of cap free
Amount of items: 8
Items: 
Size: 88 Color: 0
Size: 86 Color: 0
Size: 84 Color: 0
Size: 72 Color: 0
Size: 60 Color: 1
Size: 54 Color: 1
Size: 44 Color: 1
Size: 40 Color: 1

Total size: 160680
Total free space: 2472

