Capicity Bin: 2012
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1111 Color: 0
Size: 751 Color: 1
Size: 150 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1194 Color: 1
Size: 754 Color: 0
Size: 64 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 1260 Color: 0
Size: 752 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1522 Color: 1
Size: 410 Color: 0
Size: 80 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 1
Size: 362 Color: 0
Size: 68 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 0
Size: 293 Color: 1
Size: 116 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 1
Size: 329 Color: 1
Size: 66 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1654 Color: 1
Size: 312 Color: 1
Size: 46 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 0
Size: 287 Color: 1
Size: 64 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 1
Size: 158 Color: 0
Size: 136 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1757 Color: 1
Size: 213 Color: 1
Size: 42 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 1
Size: 210 Color: 1
Size: 38 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 1
Size: 112 Color: 0
Size: 110 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1770 Color: 0
Size: 202 Color: 0
Size: 40 Color: 1

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 1
Size: 671 Color: 0
Size: 74 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1279 Color: 1
Size: 684 Color: 1
Size: 48 Color: 0

Bin 17: 1 of cap free
Amount of items: 4
Items: 
Size: 1392 Color: 1
Size: 553 Color: 0
Size: 36 Color: 1
Size: 30 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 1
Size: 420 Color: 0
Size: 36 Color: 0

Bin 19: 1 of cap free
Amount of items: 5
Items: 
Size: 1653 Color: 1
Size: 142 Color: 0
Size: 126 Color: 0
Size: 48 Color: 0
Size: 42 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 1
Size: 186 Color: 0
Size: 134 Color: 1

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 0
Size: 282 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 1
Size: 218 Color: 0
Size: 16 Color: 0

Bin 23: 2 of cap free
Amount of items: 3
Items: 
Size: 1247 Color: 1
Size: 711 Color: 1
Size: 52 Color: 0

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1311 Color: 0
Size: 643 Color: 0
Size: 56 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1442 Color: 0
Size: 464 Color: 1
Size: 104 Color: 1

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1669 Color: 1
Size: 341 Color: 0

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 1
Size: 309 Color: 0

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1749 Color: 0
Size: 261 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 1
Size: 207 Color: 0
Size: 8 Color: 0

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1807 Color: 0
Size: 203 Color: 1

Bin 31: 3 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 1
Size: 366 Color: 0

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 1
Size: 246 Color: 0
Size: 40 Color: 0

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1747 Color: 1
Size: 262 Color: 0

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1785 Color: 1
Size: 224 Color: 0

Bin 35: 4 of cap free
Amount of items: 2
Items: 
Size: 1326 Color: 1
Size: 682 Color: 0

Bin 36: 4 of cap free
Amount of items: 3
Items: 
Size: 1431 Color: 0
Size: 411 Color: 0
Size: 166 Color: 1

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 1
Size: 330 Color: 0

Bin 38: 5 of cap free
Amount of items: 4
Items: 
Size: 1063 Color: 1
Size: 639 Color: 0
Size: 221 Color: 1
Size: 84 Color: 0

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 1349 Color: 0
Size: 574 Color: 1
Size: 84 Color: 1

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 0
Size: 269 Color: 1

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 0
Size: 193 Color: 1
Size: 8 Color: 1

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 0
Size: 241 Color: 1

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1798 Color: 1
Size: 208 Color: 0

Bin 44: 7 of cap free
Amount of items: 4
Items: 
Size: 1010 Color: 0
Size: 919 Color: 1
Size: 40 Color: 1
Size: 36 Color: 0

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1499 Color: 1
Size: 506 Color: 0

Bin 46: 8 of cap free
Amount of items: 3
Items: 
Size: 1110 Color: 0
Size: 838 Color: 0
Size: 56 Color: 1

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1381 Color: 0
Size: 622 Color: 1

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 0
Size: 429 Color: 1

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1159 Color: 1
Size: 747 Color: 0
Size: 96 Color: 0

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 1
Size: 221 Color: 0

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1698 Color: 1
Size: 302 Color: 0

Bin 52: 13 of cap free
Amount of items: 2
Items: 
Size: 1521 Color: 1
Size: 478 Color: 0

Bin 53: 13 of cap free
Amount of items: 2
Items: 
Size: 1573 Color: 1
Size: 426 Color: 0

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1762 Color: 0
Size: 237 Color: 1

Bin 55: 14 of cap free
Amount of items: 2
Items: 
Size: 1207 Color: 0
Size: 791 Color: 1

Bin 56: 15 of cap free
Amount of items: 19
Items: 
Size: 191 Color: 1
Size: 189 Color: 1
Size: 182 Color: 1
Size: 181 Color: 1
Size: 174 Color: 1
Size: 164 Color: 1
Size: 148 Color: 1
Size: 92 Color: 1
Size: 92 Color: 1
Size: 72 Color: 0
Size: 72 Color: 0
Size: 68 Color: 0
Size: 60 Color: 0
Size: 58 Color: 0
Size: 56 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0
Size: 50 Color: 0
Size: 44 Color: 0

Bin 57: 21 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 0
Size: 585 Color: 1

Bin 58: 22 of cap free
Amount of items: 2
Items: 
Size: 1151 Color: 1
Size: 839 Color: 0

Bin 59: 25 of cap free
Amount of items: 2
Items: 
Size: 1502 Color: 0
Size: 485 Color: 1

Bin 60: 26 of cap free
Amount of items: 8
Items: 
Size: 1007 Color: 1
Size: 331 Color: 1
Size: 220 Color: 1
Size: 100 Color: 1
Size: 90 Color: 0
Size: 82 Color: 0
Size: 80 Color: 0
Size: 76 Color: 0

Bin 61: 26 of cap free
Amount of items: 2
Items: 
Size: 1199 Color: 1
Size: 787 Color: 0

Bin 62: 27 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 0
Size: 367 Color: 1

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1457 Color: 0
Size: 527 Color: 1

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1754 Color: 0
Size: 230 Color: 1

Bin 65: 33 of cap free
Amount of items: 4
Items: 
Size: 1011 Color: 1
Size: 463 Color: 1
Size: 381 Color: 0
Size: 124 Color: 0

Bin 66: 1556 of cap free
Amount of items: 10
Items: 
Size: 76 Color: 1
Size: 72 Color: 1
Size: 52 Color: 1
Size: 44 Color: 0
Size: 40 Color: 1
Size: 40 Color: 1
Size: 36 Color: 0
Size: 32 Color: 0
Size: 32 Color: 0
Size: 32 Color: 0

Total size: 130780
Total free space: 2012

