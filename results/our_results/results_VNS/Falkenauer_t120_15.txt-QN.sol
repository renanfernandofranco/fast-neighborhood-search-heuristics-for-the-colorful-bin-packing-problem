Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 102
Size: 307 Color: 62
Size: 250 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 119
Size: 263 Color: 21
Size: 250 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 101
Size: 282 Color: 47
Size: 278 Color: 42

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 90
Size: 362 Color: 78
Size: 251 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 117
Size: 267 Color: 27
Size: 250 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 99
Size: 304 Color: 58
Size: 270 Color: 30

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 91
Size: 330 Color: 69
Size: 270 Color: 32

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 116
Size: 262 Color: 20
Size: 256 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 80
Size: 334 Color: 72
Size: 303 Color: 56

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 115
Size: 266 Color: 25
Size: 255 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 83
Size: 337 Color: 73
Size: 296 Color: 53

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 113
Size: 266 Color: 24
Size: 262 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 96
Size: 320 Color: 65
Size: 258 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 95
Size: 310 Color: 63
Size: 279 Color: 43

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 100
Size: 303 Color: 55
Size: 264 Color: 23

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 103
Size: 306 Color: 61
Size: 251 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 98
Size: 317 Color: 64
Size: 257 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 92
Size: 333 Color: 71
Size: 267 Color: 28

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 106
Size: 280 Color: 45
Size: 274 Color: 38

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 112
Size: 276 Color: 39
Size: 252 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 104
Size: 285 Color: 48
Size: 272 Color: 35

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 88
Size: 363 Color: 79
Size: 251 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 114
Size: 269 Color: 29
Size: 258 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 89
Size: 357 Color: 76
Size: 256 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 110
Size: 274 Color: 37
Size: 261 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 82
Size: 332 Color: 70
Size: 303 Color: 57

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 87
Size: 322 Color: 68
Size: 292 Color: 50

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 86
Size: 362 Color: 77
Size: 260 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 109
Size: 270 Color: 31
Size: 267 Color: 26

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 118
Size: 261 Color: 17
Size: 255 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 111
Size: 279 Color: 44
Size: 252 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 108
Size: 271 Color: 33
Size: 271 Color: 34

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 85
Size: 363 Color: 81
Size: 264 Color: 22

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 107
Size: 296 Color: 52
Size: 251 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 84
Size: 322 Color: 66
Size: 306 Color: 60

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 93
Size: 322 Color: 67
Size: 274 Color: 36

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 75
Size: 344 Color: 74
Size: 302 Color: 54

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 94
Size: 305 Color: 59
Size: 287 Color: 49

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 97
Size: 294 Color: 51
Size: 281 Color: 46

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 105
Size: 277 Color: 41
Size: 277 Color: 40

Total size: 40000
Total free space: 0

