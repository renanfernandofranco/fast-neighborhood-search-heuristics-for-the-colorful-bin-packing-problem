Capicity Bin: 6528
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3751 Color: 17
Size: 2319 Color: 19
Size: 458 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4060 Color: 15
Size: 2228 Color: 12
Size: 240 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 7
Size: 1652 Color: 13
Size: 536 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4412 Color: 0
Size: 1954 Color: 9
Size: 162 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4548 Color: 8
Size: 1822 Color: 6
Size: 158 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4622 Color: 7
Size: 1828 Color: 16
Size: 78 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4646 Color: 7
Size: 1570 Color: 1
Size: 312 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4868 Color: 9
Size: 1484 Color: 14
Size: 176 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 13
Size: 970 Color: 6
Size: 680 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5070 Color: 16
Size: 1254 Color: 15
Size: 204 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5123 Color: 19
Size: 937 Color: 7
Size: 468 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5139 Color: 7
Size: 1135 Color: 3
Size: 254 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5147 Color: 6
Size: 1155 Color: 17
Size: 226 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5143 Color: 3
Size: 1151 Color: 17
Size: 234 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5171 Color: 6
Size: 1131 Color: 13
Size: 226 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5232 Color: 0
Size: 1148 Color: 9
Size: 148 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5276 Color: 0
Size: 1156 Color: 12
Size: 96 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5409 Color: 9
Size: 933 Color: 2
Size: 186 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 0
Size: 575 Color: 5
Size: 540 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5500 Color: 8
Size: 566 Color: 16
Size: 462 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 13
Size: 772 Color: 15
Size: 230 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5579 Color: 18
Size: 795 Color: 1
Size: 154 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5650 Color: 1
Size: 734 Color: 19
Size: 144 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 8
Size: 643 Color: 11
Size: 226 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5683 Color: 6
Size: 615 Color: 4
Size: 230 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5694 Color: 15
Size: 698 Color: 8
Size: 136 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5712 Color: 2
Size: 562 Color: 5
Size: 254 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5730 Color: 4
Size: 462 Color: 15
Size: 336 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5777 Color: 18
Size: 627 Color: 11
Size: 124 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 18
Size: 619 Color: 14
Size: 122 Color: 17

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5843 Color: 11
Size: 571 Color: 2
Size: 114 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 13
Size: 380 Color: 6
Size: 298 Color: 16

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 2
Size: 2713 Color: 9
Size: 540 Color: 4

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 3676 Color: 16
Size: 2315 Color: 7
Size: 536 Color: 4

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 9
Size: 2249 Color: 14
Size: 312 Color: 3

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4509 Color: 17
Size: 1564 Color: 15
Size: 454 Color: 17

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 8
Size: 1171 Color: 11
Size: 200 Color: 13

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 18
Size: 800 Color: 0
Size: 564 Color: 13

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5366 Color: 19
Size: 931 Color: 8
Size: 230 Color: 12

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 5401 Color: 15
Size: 1082 Color: 16
Size: 44 Color: 16

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5531 Color: 15
Size: 860 Color: 0
Size: 136 Color: 6

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5599 Color: 1
Size: 756 Color: 6
Size: 172 Color: 18

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5791 Color: 18
Size: 540 Color: 16
Size: 196 Color: 6

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5839 Color: 1
Size: 464 Color: 11
Size: 224 Color: 13

Bin 45: 2 of cap free
Amount of items: 15
Items: 
Size: 618 Color: 2
Size: 542 Color: 15
Size: 542 Color: 12
Size: 540 Color: 3
Size: 504 Color: 17
Size: 450 Color: 16
Size: 450 Color: 3
Size: 440 Color: 18
Size: 440 Color: 8
Size: 424 Color: 9
Size: 408 Color: 11
Size: 408 Color: 6
Size: 324 Color: 0
Size: 244 Color: 14
Size: 192 Color: 7

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 3460 Color: 5
Size: 2714 Color: 5
Size: 352 Color: 11

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 4967 Color: 12
Size: 1437 Color: 13
Size: 122 Color: 15

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 5027 Color: 6
Size: 1195 Color: 8
Size: 304 Color: 1

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 5119 Color: 0
Size: 1407 Color: 18

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 5148 Color: 12
Size: 1378 Color: 2

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 5405 Color: 17
Size: 1121 Color: 19

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5628 Color: 14
Size: 898 Color: 2

Bin 53: 3 of cap free
Amount of items: 4
Items: 
Size: 3268 Color: 9
Size: 2295 Color: 3
Size: 794 Color: 5
Size: 168 Color: 10

Bin 54: 3 of cap free
Amount of items: 4
Items: 
Size: 3269 Color: 18
Size: 2326 Color: 11
Size: 818 Color: 4
Size: 112 Color: 1

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 3709 Color: 3
Size: 2380 Color: 11
Size: 436 Color: 7

Bin 56: 3 of cap free
Amount of items: 2
Items: 
Size: 3819 Color: 19
Size: 2706 Color: 12

Bin 57: 3 of cap free
Amount of items: 2
Items: 
Size: 3823 Color: 15
Size: 2702 Color: 4

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 4226 Color: 8
Size: 2299 Color: 11

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 4250 Color: 3
Size: 2275 Color: 8

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 4995 Color: 11
Size: 1218 Color: 16
Size: 312 Color: 4

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 5026 Color: 0
Size: 1275 Color: 14
Size: 224 Color: 8

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 5322 Color: 6
Size: 811 Color: 19
Size: 392 Color: 7

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5604 Color: 19
Size: 873 Color: 7
Size: 48 Color: 1

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 16
Size: 761 Color: 13

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 1
Size: 649 Color: 15
Size: 24 Color: 19

Bin 66: 4 of cap free
Amount of items: 4
Items: 
Size: 3277 Color: 12
Size: 2711 Color: 6
Size: 316 Color: 14
Size: 220 Color: 5

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5454 Color: 11
Size: 1006 Color: 19
Size: 64 Color: 2

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 3722 Color: 6
Size: 2351 Color: 1
Size: 450 Color: 3

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 3799 Color: 6
Size: 2724 Color: 14

Bin 70: 5 of cap free
Amount of items: 3
Items: 
Size: 5046 Color: 10
Size: 867 Color: 14
Size: 610 Color: 12

Bin 71: 5 of cap free
Amount of items: 2
Items: 
Size: 5575 Color: 4
Size: 948 Color: 1

Bin 72: 5 of cap free
Amount of items: 2
Items: 
Size: 5692 Color: 10
Size: 831 Color: 2

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 4
Size: 2259 Color: 15
Size: 990 Color: 6

Bin 74: 6 of cap free
Amount of items: 3
Items: 
Size: 3860 Color: 0
Size: 2214 Color: 5
Size: 448 Color: 2

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 4671 Color: 8
Size: 1491 Color: 5
Size: 360 Color: 2

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 4854 Color: 10
Size: 1580 Color: 16
Size: 88 Color: 18

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 5167 Color: 10
Size: 1279 Color: 5
Size: 76 Color: 7

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 18
Size: 1238 Color: 10

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 4322 Color: 7
Size: 2198 Color: 1

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 4971 Color: 18
Size: 1549 Color: 0

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 5396 Color: 6
Size: 940 Color: 15
Size: 184 Color: 12

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 5381 Color: 8
Size: 1139 Color: 3

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 5626 Color: 15
Size: 894 Color: 10

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 5815 Color: 16
Size: 705 Color: 2

Bin 85: 9 of cap free
Amount of items: 3
Items: 
Size: 3771 Color: 11
Size: 2564 Color: 14
Size: 184 Color: 17

Bin 86: 9 of cap free
Amount of items: 3
Items: 
Size: 3831 Color: 0
Size: 2432 Color: 6
Size: 256 Color: 0

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 4929 Color: 13
Size: 1590 Color: 12

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 5299 Color: 5
Size: 1220 Color: 13

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 5615 Color: 0
Size: 904 Color: 2

Bin 90: 10 of cap free
Amount of items: 3
Items: 
Size: 3795 Color: 16
Size: 2251 Color: 2
Size: 472 Color: 19

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 5854 Color: 11
Size: 660 Color: 5
Size: 4 Color: 4

Bin 92: 11 of cap free
Amount of items: 4
Items: 
Size: 3266 Color: 12
Size: 2255 Color: 17
Size: 542 Color: 2
Size: 454 Color: 0

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 4171 Color: 2
Size: 2232 Color: 10
Size: 114 Color: 6

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 4737 Color: 10
Size: 1692 Color: 19
Size: 88 Color: 12

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 5342 Color: 19
Size: 1175 Color: 18

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 4237 Color: 17
Size: 2279 Color: 18

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 4999 Color: 11
Size: 1517 Color: 19

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 5798 Color: 12
Size: 718 Color: 14

Bin 99: 13 of cap free
Amount of items: 3
Items: 
Size: 4346 Color: 2
Size: 1911 Color: 9
Size: 258 Color: 11

Bin 100: 13 of cap free
Amount of items: 2
Items: 
Size: 4455 Color: 4
Size: 2060 Color: 12

Bin 101: 13 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 5
Size: 1191 Color: 5
Size: 336 Color: 4

Bin 102: 13 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 7
Size: 941 Color: 13
Size: 24 Color: 18

Bin 103: 13 of cap free
Amount of items: 2
Items: 
Size: 5740 Color: 16
Size: 775 Color: 8

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 3290 Color: 17
Size: 3224 Color: 6

Bin 105: 14 of cap free
Amount of items: 2
Items: 
Size: 5230 Color: 14
Size: 1284 Color: 1

Bin 106: 14 of cap free
Amount of items: 2
Items: 
Size: 5489 Color: 8
Size: 1025 Color: 18

Bin 107: 14 of cap free
Amount of items: 2
Items: 
Size: 5814 Color: 16
Size: 700 Color: 17

Bin 108: 16 of cap free
Amount of items: 3
Items: 
Size: 3894 Color: 14
Size: 2530 Color: 10
Size: 88 Color: 5

Bin 109: 16 of cap free
Amount of items: 2
Items: 
Size: 4748 Color: 11
Size: 1764 Color: 13

Bin 110: 16 of cap free
Amount of items: 2
Items: 
Size: 5555 Color: 11
Size: 957 Color: 8

Bin 111: 17 of cap free
Amount of items: 2
Items: 
Size: 5757 Color: 8
Size: 754 Color: 2

Bin 112: 20 of cap free
Amount of items: 2
Items: 
Size: 5183 Color: 9
Size: 1325 Color: 11

Bin 113: 20 of cap free
Amount of items: 2
Items: 
Size: 5670 Color: 8
Size: 838 Color: 5

Bin 114: 22 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 5
Size: 1622 Color: 3
Size: 224 Color: 6

Bin 115: 22 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 11
Size: 1102 Color: 16

Bin 116: 23 of cap free
Amount of items: 2
Items: 
Size: 5206 Color: 15
Size: 1299 Color: 9

Bin 117: 26 of cap free
Amount of items: 2
Items: 
Size: 5458 Color: 8
Size: 1044 Color: 3

Bin 118: 29 of cap free
Amount of items: 6
Items: 
Size: 3265 Color: 8
Size: 1159 Color: 15
Size: 791 Color: 13
Size: 644 Color: 8
Size: 364 Color: 13
Size: 276 Color: 14

Bin 119: 31 of cap free
Amount of items: 3
Items: 
Size: 3827 Color: 5
Size: 2212 Color: 1
Size: 458 Color: 12

Bin 120: 31 of cap free
Amount of items: 2
Items: 
Size: 5099 Color: 13
Size: 1398 Color: 10

Bin 121: 32 of cap free
Amount of items: 2
Items: 
Size: 5003 Color: 19
Size: 1493 Color: 18

Bin 122: 36 of cap free
Amount of items: 2
Items: 
Size: 3775 Color: 2
Size: 2717 Color: 17

Bin 123: 38 of cap free
Amount of items: 2
Items: 
Size: 4741 Color: 15
Size: 1749 Color: 13

Bin 124: 42 of cap free
Amount of items: 3
Items: 
Size: 4204 Color: 16
Size: 2138 Color: 7
Size: 144 Color: 0

Bin 125: 45 of cap free
Amount of items: 2
Items: 
Size: 5095 Color: 7
Size: 1388 Color: 18

Bin 126: 50 of cap free
Amount of items: 2
Items: 
Size: 4636 Color: 19
Size: 1842 Color: 9

Bin 127: 55 of cap free
Amount of items: 3
Items: 
Size: 4582 Color: 1
Size: 1683 Color: 7
Size: 208 Color: 2

Bin 128: 59 of cap free
Amount of items: 2
Items: 
Size: 3747 Color: 12
Size: 2722 Color: 15

Bin 129: 62 of cap free
Amount of items: 3
Items: 
Size: 3282 Color: 10
Size: 2856 Color: 11
Size: 328 Color: 0

Bin 130: 63 of cap free
Amount of items: 2
Items: 
Size: 4500 Color: 5
Size: 1965 Color: 14

Bin 131: 69 of cap free
Amount of items: 2
Items: 
Size: 3738 Color: 13
Size: 2721 Color: 18

Bin 132: 82 of cap free
Amount of items: 28
Items: 
Size: 360 Color: 1
Size: 352 Color: 9
Size: 324 Color: 10
Size: 324 Color: 2
Size: 308 Color: 0
Size: 296 Color: 15
Size: 296 Color: 2
Size: 276 Color: 12
Size: 272 Color: 12
Size: 272 Color: 7
Size: 248 Color: 6
Size: 238 Color: 17
Size: 238 Color: 3
Size: 234 Color: 12
Size: 216 Color: 19
Size: 192 Color: 0
Size: 190 Color: 0
Size: 186 Color: 14
Size: 186 Color: 2
Size: 184 Color: 11
Size: 176 Color: 17
Size: 176 Color: 14
Size: 166 Color: 3
Size: 164 Color: 4
Size: 160 Color: 6
Size: 152 Color: 1
Size: 140 Color: 9
Size: 120 Color: 10

Bin 133: 5156 of cap free
Amount of items: 11
Items: 
Size: 152 Color: 18
Size: 140 Color: 8
Size: 136 Color: 13
Size: 128 Color: 11
Size: 128 Color: 7
Size: 120 Color: 9
Size: 120 Color: 1
Size: 112 Color: 15
Size: 112 Color: 14
Size: 112 Color: 10
Size: 112 Color: 5

Total size: 861696
Total free space: 6528

