Capicity Bin: 8064
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4042 Color: 5
Size: 3354 Color: 1
Size: 668 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 19
Size: 3348 Color: 6
Size: 664 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4226 Color: 0
Size: 3202 Color: 16
Size: 636 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 18
Size: 3332 Color: 8
Size: 226 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4724 Color: 10
Size: 3148 Color: 12
Size: 192 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4979 Color: 4
Size: 2571 Color: 14
Size: 514 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 17
Size: 2551 Color: 3
Size: 510 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 14
Size: 2545 Color: 19
Size: 508 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5639 Color: 17
Size: 2243 Color: 17
Size: 182 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5992 Color: 15
Size: 1872 Color: 1
Size: 200 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 19
Size: 1322 Color: 13
Size: 640 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 2
Size: 1664 Color: 9
Size: 212 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6290 Color: 7
Size: 1482 Color: 2
Size: 292 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 14
Size: 1316 Color: 13
Size: 136 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 0
Size: 964 Color: 12
Size: 400 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 15
Size: 1002 Color: 1
Size: 348 Color: 18

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 15
Size: 1048 Color: 11
Size: 272 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 4
Size: 985 Color: 4
Size: 280 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 3
Size: 1192 Color: 7
Size: 48 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 0
Size: 998 Color: 3
Size: 208 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6870 Color: 19
Size: 694 Color: 14
Size: 500 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6944 Color: 14
Size: 960 Color: 15
Size: 160 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 1
Size: 686 Color: 3
Size: 364 Color: 14

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7024 Color: 8
Size: 776 Color: 15
Size: 264 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 17
Size: 1020 Color: 13
Size: 16 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 19
Size: 792 Color: 13
Size: 182 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 2
Size: 668 Color: 13
Size: 304 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 13
Size: 714 Color: 9
Size: 248 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7144 Color: 19
Size: 896 Color: 6
Size: 24 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 18
Size: 580 Color: 15
Size: 336 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 19
Size: 664 Color: 18
Size: 240 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 5
Size: 670 Color: 10
Size: 184 Color: 14

Bin 33: 1 of cap free
Amount of items: 6
Items: 
Size: 4037 Color: 19
Size: 1055 Color: 6
Size: 1015 Color: 3
Size: 968 Color: 14
Size: 778 Color: 16
Size: 210 Color: 14

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 4941 Color: 4
Size: 2746 Color: 17
Size: 376 Color: 13

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 14
Size: 2150 Color: 18
Size: 160 Color: 9

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 2
Size: 2081 Color: 5
Size: 202 Color: 6

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6348 Color: 12
Size: 1171 Color: 16
Size: 544 Color: 16

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 18
Size: 1496 Color: 13

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6883 Color: 6
Size: 592 Color: 18
Size: 588 Color: 2

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6931 Color: 1
Size: 844 Color: 14
Size: 288 Color: 13

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 11
Size: 945 Color: 1
Size: 160 Color: 2

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 8
Size: 3324 Color: 4
Size: 670 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 14
Size: 2062 Color: 0
Size: 664 Color: 9

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 5484 Color: 14
Size: 1908 Color: 12
Size: 670 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 4
Size: 2148 Color: 9
Size: 364 Color: 14

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 6391 Color: 0
Size: 1595 Color: 1
Size: 76 Color: 2

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 6552 Color: 11
Size: 1462 Color: 10
Size: 48 Color: 8

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 6798 Color: 6
Size: 1112 Color: 14
Size: 152 Color: 17

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 13
Size: 1218 Color: 16

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 4
Size: 1154 Color: 7

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 5
Size: 812 Color: 12
Size: 16 Color: 15

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 4
Size: 2245 Color: 18
Size: 176 Color: 6

Bin 53: 3 of cap free
Amount of items: 3
Items: 
Size: 6126 Color: 5
Size: 1759 Color: 4
Size: 176 Color: 2

Bin 54: 3 of cap free
Amount of items: 3
Items: 
Size: 6347 Color: 7
Size: 868 Color: 4
Size: 846 Color: 2

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 4536 Color: 19
Size: 3364 Color: 6
Size: 160 Color: 1

Bin 56: 4 of cap free
Amount of items: 4
Items: 
Size: 4854 Color: 17
Size: 2756 Color: 11
Size: 254 Color: 6
Size: 196 Color: 15

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 5182 Color: 0
Size: 2518 Color: 17
Size: 360 Color: 3

Bin 58: 4 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 17
Size: 2206 Color: 14
Size: 208 Color: 0

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6125 Color: 2
Size: 1791 Color: 15
Size: 144 Color: 2

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 19
Size: 1436 Color: 5
Size: 132 Color: 17

Bin 61: 4 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 18
Size: 1141 Color: 2
Size: 384 Color: 11

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 3
Size: 1474 Color: 9

Bin 63: 5 of cap free
Amount of items: 10
Items: 
Size: 4033 Color: 1
Size: 576 Color: 12
Size: 524 Color: 16
Size: 520 Color: 7
Size: 480 Color: 19
Size: 480 Color: 8
Size: 450 Color: 4
Size: 364 Color: 0
Size: 320 Color: 15
Size: 312 Color: 10

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 4050 Color: 2
Size: 3353 Color: 13
Size: 656 Color: 18

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 5339 Color: 16
Size: 2720 Color: 17

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 5915 Color: 0
Size: 1342 Color: 14
Size: 802 Color: 1

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 6847 Color: 18
Size: 1212 Color: 3

Bin 68: 6 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 4
Size: 3357 Color: 3
Size: 168 Color: 15

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 6440 Color: 2
Size: 1618 Color: 7

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 7134 Color: 13
Size: 924 Color: 6

Bin 71: 7 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 19
Size: 1451 Color: 0

Bin 72: 8 of cap free
Amount of items: 29
Items: 
Size: 448 Color: 13
Size: 420 Color: 11
Size: 412 Color: 5
Size: 400 Color: 11
Size: 368 Color: 14
Size: 336 Color: 1
Size: 324 Color: 19
Size: 312 Color: 15
Size: 304 Color: 16
Size: 292 Color: 15
Size: 278 Color: 0
Size: 268 Color: 2
Size: 268 Color: 2
Size: 256 Color: 17
Size: 256 Color: 12
Size: 256 Color: 4
Size: 248 Color: 18
Size: 240 Color: 3
Size: 240 Color: 3
Size: 236 Color: 12
Size: 236 Color: 4
Size: 224 Color: 5
Size: 224 Color: 0
Size: 214 Color: 13
Size: 208 Color: 16
Size: 208 Color: 14
Size: 200 Color: 10
Size: 192 Color: 10
Size: 188 Color: 7

Bin 73: 8 of cap free
Amount of items: 4
Items: 
Size: 4041 Color: 4
Size: 3351 Color: 19
Size: 440 Color: 11
Size: 224 Color: 14

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 13
Size: 1006 Color: 10

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 7242 Color: 18
Size: 814 Color: 1

Bin 76: 9 of cap free
Amount of items: 7
Items: 
Size: 4036 Color: 1
Size: 952 Color: 1
Size: 911 Color: 14
Size: 896 Color: 11
Size: 454 Color: 13
Size: 448 Color: 16
Size: 358 Color: 15

Bin 77: 9 of cap free
Amount of items: 2
Items: 
Size: 6971 Color: 12
Size: 1084 Color: 13

Bin 78: 10 of cap free
Amount of items: 7
Items: 
Size: 4034 Color: 6
Size: 880 Color: 19
Size: 764 Color: 3
Size: 760 Color: 11
Size: 668 Color: 6
Size: 612 Color: 4
Size: 336 Color: 1

Bin 79: 10 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 4
Size: 2156 Color: 15

Bin 80: 10 of cap free
Amount of items: 3
Items: 
Size: 5935 Color: 1
Size: 1927 Color: 2
Size: 192 Color: 4

Bin 81: 10 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 8
Size: 1928 Color: 11
Size: 122 Color: 15

Bin 82: 10 of cap free
Amount of items: 2
Items: 
Size: 6478 Color: 9
Size: 1576 Color: 17

Bin 83: 11 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 16
Size: 1058 Color: 19

Bin 84: 11 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 3
Size: 993 Color: 8

Bin 85: 14 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 6
Size: 1368 Color: 4

Bin 86: 14 of cap free
Amount of items: 2
Items: 
Size: 6775 Color: 15
Size: 1275 Color: 4

Bin 87: 14 of cap free
Amount of items: 2
Items: 
Size: 7128 Color: 13
Size: 922 Color: 17

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 5371 Color: 10
Size: 2678 Color: 15

Bin 89: 15 of cap free
Amount of items: 3
Items: 
Size: 6641 Color: 9
Size: 1248 Color: 16
Size: 160 Color: 17

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 5768 Color: 16
Size: 2280 Color: 8

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 6047 Color: 19
Size: 2001 Color: 12

Bin 92: 17 of cap free
Amount of items: 2
Items: 
Size: 6323 Color: 9
Size: 1724 Color: 14

Bin 93: 17 of cap free
Amount of items: 2
Items: 
Size: 6344 Color: 16
Size: 1703 Color: 17

Bin 94: 18 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 2
Size: 1349 Color: 19

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 6774 Color: 11
Size: 1272 Color: 9

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 3
Size: 1078 Color: 9

Bin 97: 20 of cap free
Amount of items: 2
Items: 
Size: 4378 Color: 0
Size: 3666 Color: 8

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 6904 Color: 16
Size: 1140 Color: 8

Bin 99: 21 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 9
Size: 2905 Color: 8
Size: 228 Color: 0

Bin 100: 21 of cap free
Amount of items: 3
Items: 
Size: 6298 Color: 15
Size: 1681 Color: 2
Size: 64 Color: 1

Bin 101: 21 of cap free
Amount of items: 2
Items: 
Size: 6648 Color: 7
Size: 1395 Color: 4

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 4968 Color: 7
Size: 3074 Color: 11

Bin 103: 23 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 1
Size: 2603 Color: 12
Size: 282 Color: 4

Bin 104: 23 of cap free
Amount of items: 2
Items: 
Size: 6023 Color: 17
Size: 2018 Color: 15

Bin 105: 24 of cap free
Amount of items: 2
Items: 
Size: 4612 Color: 17
Size: 3428 Color: 6

Bin 106: 25 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 1
Size: 1075 Color: 14

Bin 107: 26 of cap free
Amount of items: 2
Items: 
Size: 5072 Color: 18
Size: 2966 Color: 6

Bin 108: 29 of cap free
Amount of items: 4
Items: 
Size: 4040 Color: 18
Size: 2584 Color: 5
Size: 1187 Color: 2
Size: 224 Color: 15

Bin 109: 30 of cap free
Amount of items: 3
Items: 
Size: 5418 Color: 18
Size: 1638 Color: 13
Size: 978 Color: 2

Bin 110: 33 of cap free
Amount of items: 2
Items: 
Size: 6761 Color: 13
Size: 1270 Color: 1

Bin 111: 38 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 17
Size: 1736 Color: 1
Size: 424 Color: 3

Bin 112: 38 of cap free
Amount of items: 2
Items: 
Size: 5874 Color: 16
Size: 2152 Color: 17

Bin 113: 39 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 14
Size: 1571 Color: 11

Bin 114: 42 of cap free
Amount of items: 2
Items: 
Size: 5070 Color: 15
Size: 2952 Color: 19

Bin 115: 43 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 2
Size: 1249 Color: 5

Bin 116: 44 of cap free
Amount of items: 2
Items: 
Size: 6894 Color: 11
Size: 1126 Color: 9

Bin 117: 46 of cap free
Amount of items: 2
Items: 
Size: 5590 Color: 19
Size: 2428 Color: 9

Bin 118: 46 of cap free
Amount of items: 2
Items: 
Size: 6184 Color: 5
Size: 1834 Color: 14

Bin 119: 49 of cap free
Amount of items: 2
Items: 
Size: 6314 Color: 19
Size: 1701 Color: 13

Bin 120: 52 of cap free
Amount of items: 2
Items: 
Size: 4764 Color: 0
Size: 3248 Color: 9

Bin 121: 53 of cap free
Amount of items: 2
Items: 
Size: 6447 Color: 17
Size: 1564 Color: 5

Bin 122: 55 of cap free
Amount of items: 2
Items: 
Size: 6203 Color: 1
Size: 1806 Color: 17

Bin 123: 57 of cap free
Amount of items: 2
Items: 
Size: 5756 Color: 3
Size: 2251 Color: 13

Bin 124: 57 of cap free
Amount of items: 2
Items: 
Size: 6181 Color: 5
Size: 1826 Color: 11

Bin 125: 71 of cap free
Amount of items: 2
Items: 
Size: 5363 Color: 1
Size: 2630 Color: 6

Bin 126: 75 of cap free
Amount of items: 2
Items: 
Size: 5046 Color: 3
Size: 2943 Color: 9

Bin 127: 94 of cap free
Amount of items: 2
Items: 
Size: 5568 Color: 0
Size: 2402 Color: 19

Bin 128: 101 of cap free
Amount of items: 2
Items: 
Size: 4579 Color: 2
Size: 3384 Color: 19

Bin 129: 106 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 1
Size: 3362 Color: 11
Size: 512 Color: 3

Bin 130: 126 of cap free
Amount of items: 3
Items: 
Size: 4045 Color: 14
Size: 3361 Color: 7
Size: 532 Color: 15

Bin 131: 130 of cap free
Amount of items: 3
Items: 
Size: 5526 Color: 10
Size: 2118 Color: 7
Size: 290 Color: 6

Bin 132: 130 of cap free
Amount of items: 2
Items: 
Size: 5663 Color: 16
Size: 2271 Color: 3

Bin 133: 5800 of cap free
Amount of items: 14
Items: 
Size: 196 Color: 11
Size: 192 Color: 9
Size: 192 Color: 6
Size: 176 Color: 16
Size: 168 Color: 0
Size: 160 Color: 17
Size: 160 Color: 15
Size: 160 Color: 7
Size: 152 Color: 2
Size: 144 Color: 13
Size: 144 Color: 8
Size: 144 Color: 3
Size: 140 Color: 18
Size: 136 Color: 10

Total size: 1064448
Total free space: 8064

