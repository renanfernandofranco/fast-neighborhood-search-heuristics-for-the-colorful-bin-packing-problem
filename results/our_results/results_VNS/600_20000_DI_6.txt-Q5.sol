Capicity Bin: 13392
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 37
Items: 
Size: 544 Color: 2
Size: 536 Color: 2
Size: 524 Color: 0
Size: 472 Color: 3
Size: 464 Color: 4
Size: 460 Color: 4
Size: 458 Color: 2
Size: 436 Color: 3
Size: 436 Color: 2
Size: 432 Color: 2
Size: 432 Color: 2
Size: 416 Color: 2
Size: 396 Color: 4
Size: 372 Color: 2
Size: 370 Color: 3
Size: 368 Color: 3
Size: 358 Color: 3
Size: 352 Color: 3
Size: 352 Color: 3
Size: 344 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 318 Color: 1
Size: 312 Color: 0
Size: 300 Color: 0
Size: 296 Color: 4
Size: 294 Color: 0
Size: 290 Color: 0
Size: 288 Color: 4
Size: 260 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0
Size: 244 Color: 1
Size: 220 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8968 Color: 3
Size: 4132 Color: 2
Size: 292 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9084 Color: 1
Size: 3992 Color: 1
Size: 316 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 3
Size: 3656 Color: 4
Size: 272 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9519 Color: 3
Size: 3229 Color: 2
Size: 644 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9578 Color: 2
Size: 2886 Color: 3
Size: 928 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9684 Color: 4
Size: 3064 Color: 2
Size: 644 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10180 Color: 3
Size: 2684 Color: 2
Size: 528 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 0
Size: 2232 Color: 2
Size: 736 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 3
Size: 2396 Color: 2
Size: 472 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 10778 Color: 1
Size: 2186 Color: 0
Size: 428 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10824 Color: 0
Size: 1364 Color: 2
Size: 1204 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10843 Color: 3
Size: 2125 Color: 1
Size: 424 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10852 Color: 4
Size: 1924 Color: 1
Size: 616 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10930 Color: 0
Size: 2082 Color: 3
Size: 380 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 10975 Color: 3
Size: 2015 Color: 3
Size: 402 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11103 Color: 2
Size: 1313 Color: 4
Size: 976 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 2
Size: 1896 Color: 1
Size: 368 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 0
Size: 1176 Color: 0
Size: 1124 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11146 Color: 4
Size: 1142 Color: 2
Size: 1104 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 2
Size: 1588 Color: 0
Size: 580 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11277 Color: 1
Size: 1763 Color: 3
Size: 352 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11306 Color: 1
Size: 1124 Color: 3
Size: 962 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 1
Size: 1724 Color: 3
Size: 336 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11382 Color: 1
Size: 1730 Color: 3
Size: 280 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11391 Color: 4
Size: 1617 Color: 3
Size: 384 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 1
Size: 1114 Color: 1
Size: 816 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11493 Color: 1
Size: 1467 Color: 2
Size: 432 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11473 Color: 3
Size: 1583 Color: 0
Size: 336 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11570 Color: 1
Size: 1114 Color: 0
Size: 708 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11524 Color: 3
Size: 1452 Color: 1
Size: 416 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11594 Color: 0
Size: 1326 Color: 4
Size: 472 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11625 Color: 1
Size: 1441 Color: 2
Size: 326 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11643 Color: 4
Size: 1373 Color: 2
Size: 376 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11652 Color: 0
Size: 1288 Color: 1
Size: 452 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11694 Color: 2
Size: 854 Color: 4
Size: 844 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11726 Color: 3
Size: 1446 Color: 1
Size: 220 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11737 Color: 2
Size: 1371 Color: 4
Size: 284 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11745 Color: 3
Size: 1431 Color: 1
Size: 216 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11749 Color: 3
Size: 1393 Color: 2
Size: 250 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11764 Color: 3
Size: 1302 Color: 3
Size: 326 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 11769 Color: 2
Size: 1215 Color: 0
Size: 408 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 2
Size: 844 Color: 1
Size: 752 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 11808 Color: 1
Size: 952 Color: 2
Size: 632 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 11817 Color: 3
Size: 1253 Color: 1
Size: 322 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 0
Size: 1247 Color: 0
Size: 312 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 11845 Color: 3
Size: 1319 Color: 2
Size: 228 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 11864 Color: 4
Size: 1224 Color: 0
Size: 304 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 4
Size: 1234 Color: 2
Size: 274 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 11935 Color: 2
Size: 1183 Color: 3
Size: 274 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 11914 Color: 0
Size: 1198 Color: 2
Size: 280 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 11956 Color: 1
Size: 1172 Color: 2
Size: 264 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 11959 Color: 0
Size: 1351 Color: 2
Size: 82 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 3
Size: 896 Color: 2
Size: 504 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12005 Color: 3
Size: 1157 Color: 1
Size: 230 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12022 Color: 4
Size: 1122 Color: 0
Size: 248 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 4
Size: 864 Color: 2
Size: 484 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 12052 Color: 3
Size: 1112 Color: 2
Size: 228 Color: 4

Bin 59: 1 of cap free
Amount of items: 9
Items: 
Size: 6697 Color: 1
Size: 1204 Color: 2
Size: 1130 Color: 4
Size: 1112 Color: 3
Size: 960 Color: 4
Size: 768 Color: 1
Size: 688 Color: 0
Size: 608 Color: 0
Size: 224 Color: 4

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 7530 Color: 3
Size: 5573 Color: 0
Size: 288 Color: 1

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 2
Size: 4771 Color: 4
Size: 416 Color: 1

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 9809 Color: 0
Size: 2424 Color: 1
Size: 1158 Color: 4

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10226 Color: 2
Size: 2901 Color: 3
Size: 264 Color: 2

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 10579 Color: 2
Size: 2100 Color: 1
Size: 712 Color: 3

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10674 Color: 1
Size: 1613 Color: 4
Size: 1104 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 10813 Color: 1
Size: 2182 Color: 4
Size: 396 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 2
Size: 1981 Color: 3
Size: 270 Color: 4

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 11165 Color: 2
Size: 1610 Color: 4
Size: 616 Color: 3

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 11361 Color: 1
Size: 1806 Color: 4
Size: 224 Color: 3

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 4
Size: 2051 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 11450 Color: 3
Size: 1473 Color: 4
Size: 468 Color: 1

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 11425 Color: 4
Size: 1966 Color: 2

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 11492 Color: 1
Size: 1563 Color: 3
Size: 336 Color: 0

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 11544 Color: 4
Size: 1847 Color: 0

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 11657 Color: 2
Size: 1282 Color: 4
Size: 452 Color: 1

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 11973 Color: 2
Size: 1114 Color: 1
Size: 304 Color: 4

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 12038 Color: 0
Size: 1353 Color: 3

Bin 78: 2 of cap free
Amount of items: 5
Items: 
Size: 6705 Color: 3
Size: 3288 Color: 4
Size: 1597 Color: 0
Size: 1164 Color: 3
Size: 636 Color: 1

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 7534 Color: 2
Size: 5856 Color: 3

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 7669 Color: 4
Size: 5431 Color: 2
Size: 290 Color: 1

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 8841 Color: 0
Size: 4061 Color: 0
Size: 488 Color: 1

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 4
Size: 2667 Color: 2
Size: 1260 Color: 0

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 9522 Color: 4
Size: 3564 Color: 2
Size: 304 Color: 0

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 9652 Color: 2
Size: 3084 Color: 2
Size: 654 Color: 0

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 10164 Color: 3
Size: 3226 Color: 2

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 10470 Color: 0
Size: 2560 Color: 2
Size: 360 Color: 4

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 10550 Color: 1
Size: 2840 Color: 4

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 1
Size: 2340 Color: 3
Size: 322 Color: 0

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 11124 Color: 0
Size: 2266 Color: 4

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 6701 Color: 0
Size: 5576 Color: 3
Size: 1112 Color: 3

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 7617 Color: 2
Size: 5556 Color: 4
Size: 216 Color: 1

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 8306 Color: 3
Size: 4821 Color: 0
Size: 262 Color: 2

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 8890 Color: 4
Size: 4275 Color: 2
Size: 224 Color: 4

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 9001 Color: 3
Size: 4226 Color: 0
Size: 162 Color: 1

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 10097 Color: 3
Size: 3092 Color: 1
Size: 200 Color: 0

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 10124 Color: 3
Size: 2987 Color: 2
Size: 278 Color: 4

Bin 97: 3 of cap free
Amount of items: 2
Items: 
Size: 11237 Color: 1
Size: 2152 Color: 4

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 8436 Color: 4
Size: 4728 Color: 3
Size: 224 Color: 1

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 8468 Color: 3
Size: 4324 Color: 1
Size: 596 Color: 4

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 8552 Color: 3
Size: 4504 Color: 1
Size: 332 Color: 0

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 10113 Color: 3
Size: 3275 Color: 2

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 2
Size: 2392 Color: 4
Size: 242 Color: 0

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 10306 Color: 1
Size: 3081 Color: 0

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 10367 Color: 4
Size: 2124 Color: 1
Size: 896 Color: 1

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 4
Size: 1459 Color: 3

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 12006 Color: 3
Size: 1381 Color: 0

Bin 107: 6 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 2
Size: 3202 Color: 1
Size: 192 Color: 4

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 10014 Color: 1
Size: 3124 Color: 4
Size: 248 Color: 0

Bin 109: 6 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 0
Size: 2370 Color: 4

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 11318 Color: 4
Size: 1972 Color: 2
Size: 96 Color: 1

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 11670 Color: 2
Size: 1716 Color: 4

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 1
Size: 1438 Color: 0

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 10457 Color: 3
Size: 2584 Color: 1
Size: 344 Color: 4

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 2
Size: 1601 Color: 0

Bin 115: 7 of cap free
Amount of items: 2
Items: 
Size: 11812 Color: 0
Size: 1573 Color: 3

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 11841 Color: 4
Size: 1544 Color: 0

Bin 117: 8 of cap free
Amount of items: 3
Items: 
Size: 8330 Color: 0
Size: 4886 Color: 1
Size: 168 Color: 2

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 10664 Color: 1
Size: 2048 Color: 3
Size: 672 Color: 1

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 12004 Color: 0
Size: 1380 Color: 4

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 9716 Color: 1
Size: 3551 Color: 3
Size: 116 Color: 1

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 6732 Color: 4
Size: 5578 Color: 4
Size: 1072 Color: 2

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 7692 Color: 2
Size: 5690 Color: 4

Bin 123: 10 of cap free
Amount of items: 3
Items: 
Size: 10536 Color: 1
Size: 1986 Color: 3
Size: 860 Color: 1

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 8263 Color: 4
Size: 4780 Color: 0
Size: 338 Color: 2

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 11740 Color: 0
Size: 1641 Color: 4

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 4
Size: 2824 Color: 0

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 10688 Color: 2
Size: 2692 Color: 0

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 11758 Color: 2
Size: 1622 Color: 3

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 11015 Color: 3
Size: 2364 Color: 4

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 11034 Color: 3
Size: 2345 Color: 0

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 11177 Color: 3
Size: 2202 Color: 1

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 11897 Color: 1
Size: 1482 Color: 4

Bin 133: 15 of cap free
Amount of items: 3
Items: 
Size: 7976 Color: 3
Size: 4108 Color: 4
Size: 1293 Color: 3

Bin 134: 15 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 3
Size: 2521 Color: 2
Size: 640 Color: 4

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 11226 Color: 0
Size: 2151 Color: 1

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 7992 Color: 2
Size: 5384 Color: 3

Bin 137: 16 of cap free
Amount of items: 3
Items: 
Size: 9358 Color: 3
Size: 3306 Color: 4
Size: 712 Color: 0

Bin 138: 16 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 3
Size: 1624 Color: 2
Size: 128 Color: 3

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 11707 Color: 4
Size: 1669 Color: 0

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 11854 Color: 4
Size: 1522 Color: 1

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 10193 Color: 4
Size: 3182 Color: 2

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 11633 Color: 3
Size: 1742 Color: 4

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 9132 Color: 1
Size: 4242 Color: 3

Bin 144: 18 of cap free
Amount of items: 2
Items: 
Size: 10132 Color: 4
Size: 3242 Color: 2

Bin 145: 18 of cap free
Amount of items: 2
Items: 
Size: 11517 Color: 0
Size: 1857 Color: 3

Bin 146: 18 of cap free
Amount of items: 2
Items: 
Size: 12050 Color: 4
Size: 1324 Color: 0

Bin 147: 19 of cap free
Amount of items: 3
Items: 
Size: 7614 Color: 2
Size: 5599 Color: 0
Size: 160 Color: 2

Bin 148: 19 of cap free
Amount of items: 2
Items: 
Size: 11736 Color: 0
Size: 1637 Color: 4

Bin 149: 21 of cap free
Amount of items: 2
Items: 
Size: 10933 Color: 4
Size: 2438 Color: 2

Bin 150: 21 of cap free
Amount of items: 3
Items: 
Size: 11429 Color: 1
Size: 1824 Color: 2
Size: 118 Color: 2

Bin 151: 22 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 2
Size: 5582 Color: 0
Size: 1072 Color: 4

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 10637 Color: 2
Size: 2733 Color: 3

Bin 153: 23 of cap free
Amount of items: 3
Items: 
Size: 7513 Color: 3
Size: 5512 Color: 4
Size: 344 Color: 1

Bin 154: 23 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 2
Size: 1648 Color: 0

Bin 155: 23 of cap free
Amount of items: 2
Items: 
Size: 11889 Color: 4
Size: 1480 Color: 0

Bin 156: 24 of cap free
Amount of items: 3
Items: 
Size: 7128 Color: 3
Size: 5580 Color: 4
Size: 660 Color: 1

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 11457 Color: 3
Size: 1909 Color: 2

Bin 158: 26 of cap free
Amount of items: 2
Items: 
Size: 11802 Color: 0
Size: 1564 Color: 4

Bin 159: 28 of cap free
Amount of items: 3
Items: 
Size: 9116 Color: 0
Size: 4040 Color: 4
Size: 208 Color: 3

Bin 160: 29 of cap free
Amount of items: 2
Items: 
Size: 11958 Color: 1
Size: 1405 Color: 0

Bin 161: 30 of cap free
Amount of items: 4
Items: 
Size: 6698 Color: 0
Size: 3556 Color: 2
Size: 2724 Color: 2
Size: 384 Color: 1

Bin 162: 30 of cap free
Amount of items: 3
Items: 
Size: 8226 Color: 1
Size: 4818 Color: 0
Size: 318 Color: 3

Bin 163: 31 of cap free
Amount of items: 2
Items: 
Size: 11477 Color: 2
Size: 1884 Color: 0

Bin 164: 37 of cap free
Amount of items: 2
Items: 
Size: 9133 Color: 0
Size: 4222 Color: 3

Bin 165: 42 of cap free
Amount of items: 2
Items: 
Size: 11988 Color: 4
Size: 1362 Color: 1

Bin 166: 43 of cap free
Amount of items: 2
Items: 
Size: 11873 Color: 4
Size: 1476 Color: 3

Bin 167: 45 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 0
Size: 5571 Color: 2
Size: 840 Color: 1

Bin 168: 48 of cap free
Amount of items: 2
Items: 
Size: 10770 Color: 2
Size: 2574 Color: 3

Bin 169: 48 of cap free
Amount of items: 2
Items: 
Size: 11352 Color: 1
Size: 1992 Color: 4

Bin 170: 50 of cap free
Amount of items: 2
Items: 
Size: 8519 Color: 4
Size: 4823 Color: 0

Bin 171: 52 of cap free
Amount of items: 7
Items: 
Size: 6700 Color: 1
Size: 1352 Color: 2
Size: 1332 Color: 2
Size: 1328 Color: 4
Size: 1112 Color: 1
Size: 812 Color: 0
Size: 704 Color: 0

Bin 172: 54 of cap free
Amount of items: 4
Items: 
Size: 9426 Color: 4
Size: 1816 Color: 1
Size: 1704 Color: 1
Size: 392 Color: 0

Bin 173: 57 of cap free
Amount of items: 2
Items: 
Size: 11453 Color: 0
Size: 1882 Color: 2

Bin 174: 58 of cap free
Amount of items: 2
Items: 
Size: 8322 Color: 2
Size: 5012 Color: 4

Bin 175: 60 of cap free
Amount of items: 2
Items: 
Size: 11830 Color: 3
Size: 1502 Color: 4

Bin 176: 64 of cap free
Amount of items: 2
Items: 
Size: 9016 Color: 2
Size: 4312 Color: 3

Bin 177: 69 of cap free
Amount of items: 2
Items: 
Size: 9530 Color: 3
Size: 3793 Color: 4

Bin 178: 69 of cap free
Amount of items: 2
Items: 
Size: 10876 Color: 0
Size: 2447 Color: 4

Bin 179: 70 of cap free
Amount of items: 2
Items: 
Size: 11448 Color: 3
Size: 1874 Color: 0

Bin 180: 71 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 3
Size: 1693 Color: 2

Bin 181: 72 of cap free
Amount of items: 2
Items: 
Size: 6712 Color: 1
Size: 6608 Color: 4

Bin 182: 76 of cap free
Amount of items: 2
Items: 
Size: 9720 Color: 0
Size: 3596 Color: 3

Bin 183: 79 of cap free
Amount of items: 2
Items: 
Size: 7736 Color: 0
Size: 5577 Color: 1

Bin 184: 79 of cap free
Amount of items: 2
Items: 
Size: 9911 Color: 2
Size: 3402 Color: 4

Bin 185: 80 of cap free
Amount of items: 2
Items: 
Size: 10588 Color: 0
Size: 2724 Color: 2

Bin 186: 81 of cap free
Amount of items: 2
Items: 
Size: 10669 Color: 2
Size: 2642 Color: 0

Bin 187: 100 of cap free
Amount of items: 5
Items: 
Size: 6702 Color: 3
Size: 2297 Color: 3
Size: 1797 Color: 4
Size: 1384 Color: 2
Size: 1112 Color: 0

Bin 188: 100 of cap free
Amount of items: 2
Items: 
Size: 9930 Color: 4
Size: 3362 Color: 2

Bin 189: 111 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 3
Size: 2271 Color: 0

Bin 190: 112 of cap free
Amount of items: 2
Items: 
Size: 8760 Color: 4
Size: 4520 Color: 0

Bin 191: 116 of cap free
Amount of items: 2
Items: 
Size: 8970 Color: 4
Size: 4306 Color: 0

Bin 192: 118 of cap free
Amount of items: 21
Items: 
Size: 952 Color: 3
Size: 832 Color: 3
Size: 824 Color: 2
Size: 800 Color: 4
Size: 758 Color: 2
Size: 748 Color: 2
Size: 730 Color: 4
Size: 720 Color: 2
Size: 644 Color: 0
Size: 640 Color: 4
Size: 592 Color: 4
Size: 576 Color: 3
Size: 560 Color: 2
Size: 560 Color: 0
Size: 546 Color: 1
Size: 536 Color: 0
Size: 532 Color: 0
Size: 512 Color: 3
Size: 464 Color: 1
Size: 380 Color: 1
Size: 368 Color: 1

Bin 193: 132 of cap free
Amount of items: 2
Items: 
Size: 9506 Color: 1
Size: 3754 Color: 4

Bin 194: 137 of cap free
Amount of items: 2
Items: 
Size: 8279 Color: 1
Size: 4976 Color: 3

Bin 195: 151 of cap free
Amount of items: 2
Items: 
Size: 7660 Color: 0
Size: 5581 Color: 3

Bin 196: 152 of cap free
Amount of items: 2
Items: 
Size: 9554 Color: 4
Size: 3686 Color: 3

Bin 197: 204 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 2
Size: 3661 Color: 4
Size: 2818 Color: 3

Bin 198: 219 of cap free
Amount of items: 2
Items: 
Size: 7609 Color: 3
Size: 5564 Color: 1

Bin 199: 9344 of cap free
Amount of items: 16
Items: 
Size: 296 Color: 2
Size: 288 Color: 2
Size: 272 Color: 4
Size: 272 Color: 4
Size: 272 Color: 3
Size: 264 Color: 3
Size: 258 Color: 3
Size: 256 Color: 4
Size: 240 Color: 4
Size: 240 Color: 3
Size: 240 Color: 1
Size: 236 Color: 1
Size: 236 Color: 0
Size: 232 Color: 0
Size: 232 Color: 0
Size: 214 Color: 1

Total size: 2651616
Total free space: 13392

