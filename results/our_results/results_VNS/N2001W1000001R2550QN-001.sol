Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2001

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 390678 Color: 1581
Size: 358510 Color: 1344
Size: 250813 Color: 31

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 375000 Color: 1477
Size: 369181 Color: 1428
Size: 255820 Color: 190

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 359089 Color: 1350
Size: 358260 Color: 1338
Size: 282652 Color: 622

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 391652 Color: 1586
Size: 350941 Color: 1289
Size: 257408 Color: 218

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 405882 Color: 1665
Size: 326396 Color: 1068
Size: 267723 Color: 418

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 398012 Color: 1628
Size: 351919 Color: 1297
Size: 250070 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 379377 Color: 1511
Size: 368141 Color: 1417
Size: 252483 Color: 82

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 388365 Color: 1570
Size: 344923 Color: 1228
Size: 266713 Color: 397

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 363078 Color: 1379
Size: 338312 Color: 1182
Size: 298611 Color: 819

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 366949 Color: 1406
Size: 332258 Color: 1116
Size: 300794 Color: 838

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 347173 Color: 1250
Size: 343700 Color: 1219
Size: 309128 Color: 913

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 404529 Color: 1661
Size: 315528 Color: 967
Size: 279944 Color: 578

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 385545 Color: 1549
Size: 344098 Color: 1222
Size: 270358 Color: 461

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 396119 Color: 1612
Size: 349803 Color: 1273
Size: 254079 Color: 135

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 365539 Color: 1395
Size: 347889 Color: 1259
Size: 286573 Color: 668

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 376274 Color: 1485
Size: 323803 Color: 1037
Size: 299924 Color: 835

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 398848 Color: 1635
Size: 339636 Color: 1191
Size: 261517 Color: 307

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 401722 Color: 1646
Size: 324982 Color: 1050
Size: 273297 Color: 504

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 387614 Color: 1568
Size: 357071 Color: 1330
Size: 255316 Color: 169

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 399461 Color: 1638
Size: 348543 Color: 1263
Size: 251997 Color: 70

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 393210 Color: 1595
Size: 343917 Color: 1220
Size: 262874 Color: 327

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 373814 Color: 1462
Size: 343592 Color: 1217
Size: 282595 Color: 621

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 393144 Color: 1594
Size: 315523 Color: 966
Size: 291334 Color: 725

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 386039 Color: 1553
Size: 316159 Color: 973
Size: 297803 Color: 807

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 355326 Color: 1320
Size: 322459 Color: 1022
Size: 322216 Color: 1018

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 388533 Color: 1572
Size: 358476 Color: 1342
Size: 252992 Color: 98

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 396831 Color: 1618
Size: 343676 Color: 1218
Size: 259494 Color: 271

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 379197 Color: 1510
Size: 318341 Color: 989
Size: 302463 Color: 848

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 378520 Color: 1508
Size: 360377 Color: 1362
Size: 261104 Color: 300

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 397514 Color: 1625
Size: 347370 Color: 1253
Size: 255117 Color: 163

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 388497 Color: 1571
Size: 360189 Color: 1358
Size: 251315 Color: 47

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 361034 Color: 1367
Size: 332488 Color: 1119
Size: 306479 Color: 884

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 399223 Color: 1636
Size: 350772 Color: 1284
Size: 250006 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 397514 Color: 1624
Size: 308641 Color: 909
Size: 293846 Color: 753

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 377181 Color: 1498
Size: 342650 Color: 1211
Size: 280170 Color: 581

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 406152 Color: 1667
Size: 321327 Color: 1013
Size: 272522 Color: 490

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 385142 Color: 1546
Size: 361805 Color: 1370
Size: 253054 Color: 103

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 387421 Color: 1565
Size: 348884 Color: 1266
Size: 263696 Color: 345

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 396152 Color: 1613
Size: 333974 Color: 1136
Size: 269875 Color: 451

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 398775 Color: 1634
Size: 347834 Color: 1258
Size: 253392 Color: 117

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 397202 Color: 1622
Size: 347469 Color: 1254
Size: 255330 Color: 171

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 402594 Color: 1651
Size: 341271 Color: 1206
Size: 256136 Color: 197

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 389441 Color: 1575
Size: 348336 Color: 1261
Size: 262224 Color: 318

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 367170 Color: 1408
Size: 338657 Color: 1183
Size: 294174 Color: 758

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 395436 Color: 1608
Size: 338997 Color: 1188
Size: 265568 Color: 368

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 393494 Color: 1596
Size: 346001 Color: 1239
Size: 260506 Color: 293

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 400833 Color: 1643
Size: 322314 Color: 1019
Size: 276854 Color: 540

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 369318 Color: 1430
Size: 333140 Color: 1127
Size: 297543 Color: 803

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 387231 Color: 1562
Size: 340334 Color: 1199
Size: 272436 Color: 488

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 387748 Color: 1569
Size: 351215 Color: 1292
Size: 261038 Color: 299

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 390594 Color: 1580
Size: 317594 Color: 984
Size: 291813 Color: 734

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 403500 Color: 1656
Size: 305229 Color: 875
Size: 291272 Color: 724

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 389037 Color: 1574
Size: 332919 Color: 1125
Size: 278045 Color: 556

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 399354 Color: 1637
Size: 334700 Color: 1145
Size: 265947 Color: 378

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 395491 Color: 1609
Size: 337422 Color: 1167
Size: 267088 Color: 404

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 392136 Color: 1589
Size: 336780 Color: 1162
Size: 271085 Color: 472

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 383224 Color: 1539
Size: 331176 Color: 1106
Size: 285601 Color: 655

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 383082 Color: 1536
Size: 358685 Color: 1346
Size: 258234 Color: 238

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 386243 Color: 1556
Size: 328256 Color: 1077
Size: 285502 Color: 654

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 386366 Color: 1559
Size: 362879 Color: 1378
Size: 250756 Color: 29

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 386034 Color: 1552
Size: 332039 Color: 1113
Size: 281928 Color: 607

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 386078 Color: 1554
Size: 333215 Color: 1128
Size: 280708 Color: 586

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 395341 Color: 1605
Size: 334487 Color: 1142
Size: 270173 Color: 457

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 394942 Color: 1602
Size: 331648 Color: 1110
Size: 273411 Color: 506

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 401880 Color: 1647
Size: 330564 Color: 1102
Size: 267557 Color: 413

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 347251 Color: 1252
Size: 329711 Color: 1092
Size: 323039 Color: 1031

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 374708 Color: 1474
Size: 352135 Color: 1299
Size: 273158 Color: 502

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 366332 Color: 1399
Size: 348742 Color: 1264
Size: 284927 Color: 643

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 384677 Color: 1542
Size: 334185 Color: 1139
Size: 281139 Color: 592

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 385535 Color: 1548
Size: 354777 Color: 1318
Size: 259689 Color: 278

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 380705 Color: 1525
Size: 324995 Color: 1051
Size: 294301 Color: 761

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 373450 Color: 1460
Size: 339964 Color: 1196
Size: 286587 Color: 670

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 371611 Color: 1445
Size: 346943 Color: 1248
Size: 281447 Color: 596

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 363335 Color: 1381
Size: 359385 Color: 1353
Size: 277281 Color: 546

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 380995 Color: 1527
Size: 336427 Color: 1156
Size: 282579 Color: 620

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 343018 Color: 1213
Size: 329373 Color: 1090
Size: 327610 Color: 1074

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 398285 Color: 1630
Size: 319279 Color: 996
Size: 282437 Color: 616

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 378465 Color: 1506
Size: 329151 Color: 1086
Size: 292385 Color: 737

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 377869 Color: 1501
Size: 339999 Color: 1197
Size: 282133 Color: 611

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 368152 Color: 1418
Size: 337167 Color: 1165
Size: 294682 Color: 767

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 375623 Color: 1481
Size: 331064 Color: 1105
Size: 293314 Color: 747

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 387089 Color: 1561
Size: 315897 Color: 970
Size: 297015 Color: 797

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 379776 Color: 1514
Size: 346350 Color: 1242
Size: 273875 Color: 515

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 384791 Color: 1543
Size: 352228 Color: 1300
Size: 262982 Color: 330

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 403833 Color: 1657
Size: 325776 Color: 1062
Size: 270392 Color: 462

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 402242 Color: 1649
Size: 312758 Color: 940
Size: 285001 Color: 645

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 377265 Color: 1499
Size: 368597 Color: 1422
Size: 254139 Color: 136

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 376537 Color: 1490
Size: 360294 Color: 1361
Size: 263170 Color: 335

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 376417 Color: 1488
Size: 337711 Color: 1170
Size: 285873 Color: 658

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 376026 Color: 1484
Size: 372296 Color: 1450
Size: 251679 Color: 58

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 370947 Color: 1437
Size: 345542 Color: 1233
Size: 283512 Color: 632

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 351512 Color: 1294
Size: 349383 Color: 1269
Size: 299106 Color: 825

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 375676 Color: 1482
Size: 339934 Color: 1195
Size: 284391 Color: 639

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 398288 Color: 1631
Size: 334043 Color: 1137
Size: 267670 Color: 415

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 367665 Color: 1413
Size: 341000 Color: 1204
Size: 291336 Color: 726

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 368605 Color: 1423
Size: 336557 Color: 1157
Size: 294839 Color: 772

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 373406 Color: 1459
Size: 318877 Color: 994
Size: 307718 Color: 901

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 378512 Color: 1507
Size: 350338 Color: 1278
Size: 271151 Color: 473

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 372951 Color: 1457
Size: 359199 Color: 1351
Size: 267851 Color: 423

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 374019 Color: 1465
Size: 366677 Color: 1402
Size: 259305 Color: 268

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 396317 Color: 1615
Size: 328271 Color: 1078
Size: 275413 Color: 527

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 402635 Color: 1652
Size: 307694 Color: 900
Size: 289672 Color: 707

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 395887 Color: 1610
Size: 324240 Color: 1041
Size: 279874 Color: 577

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 368039 Color: 1416
Size: 327300 Color: 1073
Size: 304662 Color: 869

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 371891 Color: 1449
Size: 364791 Color: 1392
Size: 263319 Color: 337

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 371538 Color: 1443
Size: 325449 Color: 1056
Size: 303014 Color: 853

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 341561 Color: 1207
Size: 338173 Color: 1179
Size: 320267 Color: 1002

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 374010 Color: 1464
Size: 346975 Color: 1249
Size: 279016 Color: 571

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 371153 Color: 1439
Size: 315403 Color: 964
Size: 313445 Color: 947

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 370934 Color: 1436
Size: 360451 Color: 1363
Size: 268616 Color: 433

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 372512 Color: 1454
Size: 358775 Color: 1347
Size: 268714 Color: 434

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 370020 Color: 1432
Size: 352474 Color: 1302
Size: 277507 Color: 550

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 369055 Color: 1426
Size: 317283 Color: 982
Size: 313663 Color: 950

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 396158 Color: 1614
Size: 336363 Color: 1155
Size: 267480 Color: 411

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 379538 Color: 1513
Size: 322977 Color: 1030
Size: 297486 Color: 802

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 383141 Color: 1538
Size: 350768 Color: 1283
Size: 266092 Color: 384

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 345278 Color: 1229
Size: 329296 Color: 1089
Size: 325427 Color: 1055

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 376911 Color: 1495
Size: 325100 Color: 1052
Size: 297990 Color: 809

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 399533 Color: 1639
Size: 318414 Color: 990
Size: 282054 Color: 609

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 368941 Color: 1425
Size: 349937 Color: 1274
Size: 281123 Color: 591

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 354036 Color: 1312
Size: 328054 Color: 1076
Size: 317911 Color: 986

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 368308 Color: 1419
Size: 334066 Color: 1138
Size: 297627 Color: 804

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 373826 Color: 1463
Size: 332594 Color: 1122
Size: 293581 Color: 750

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 390481 Color: 1579
Size: 319394 Color: 997
Size: 290126 Color: 713

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 366692 Color: 1403
Size: 324846 Color: 1047
Size: 308463 Color: 907

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 370378 Color: 1434
Size: 330373 Color: 1099
Size: 299250 Color: 826

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 359900 Color: 1354
Size: 333742 Color: 1134
Size: 306359 Color: 881

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 401687 Color: 1645
Size: 315069 Color: 959
Size: 283245 Color: 631

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 367261 Color: 1410
Size: 344297 Color: 1224
Size: 288443 Color: 694

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 364533 Color: 1389
Size: 358532 Color: 1345
Size: 276936 Color: 541

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 367513 Color: 1411
Size: 328606 Color: 1081
Size: 303882 Color: 863

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 366669 Color: 1401
Size: 333783 Color: 1135
Size: 299549 Color: 830

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 364022 Color: 1387
Size: 329412 Color: 1091
Size: 306567 Color: 888

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 358010 Color: 1335
Size: 338048 Color: 1176
Size: 303943 Color: 864

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 363578 Color: 1383
Size: 331466 Color: 1109
Size: 304957 Color: 873

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 363687 Color: 1385
Size: 362852 Color: 1377
Size: 273462 Color: 509

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 363634 Color: 1384
Size: 337765 Color: 1172
Size: 298602 Color: 818

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 364358 Color: 1388
Size: 362530 Color: 1376
Size: 273113 Color: 501

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 394530 Color: 1601
Size: 352849 Color: 1303
Size: 252622 Color: 88

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 362455 Color: 1375
Size: 356767 Color: 1329
Size: 280779 Color: 588

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 372314 Color: 1451
Size: 360202 Color: 1359
Size: 267485 Color: 412

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 360030 Color: 1357
Size: 359925 Color: 1355
Size: 280046 Color: 580

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 342618 Color: 1210
Size: 335508 Color: 1150
Size: 321875 Color: 1016

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 377972 Color: 1502
Size: 358969 Color: 1348
Size: 263060 Color: 332

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 358986 Color: 1349
Size: 358483 Color: 1343
Size: 282532 Color: 619

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 368358 Color: 1421
Size: 358259 Color: 1337
Size: 273384 Color: 505

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 376302 Color: 1486
Size: 356013 Color: 1326
Size: 267686 Color: 417

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 376728 Color: 1492
Size: 331808 Color: 1111
Size: 291465 Color: 729

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 351770 Color: 1296
Size: 345571 Color: 1235
Size: 302660 Color: 851

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 396793 Color: 1617
Size: 335777 Color: 1152
Size: 267431 Color: 409

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 392387 Color: 1590
Size: 355446 Color: 1322
Size: 252168 Color: 72

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 389775 Color: 1577
Size: 354536 Color: 1315
Size: 255690 Color: 183

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 376751 Color: 1493
Size: 332567 Color: 1121
Size: 290683 Color: 719

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 358462 Color: 1341
Size: 354003 Color: 1311
Size: 287536 Color: 680

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 398042 Color: 1629
Size: 351929 Color: 1298
Size: 250030 Color: 2

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 372381 Color: 1453
Size: 371678 Color: 1447
Size: 255942 Color: 192

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 360543 Color: 1364
Size: 356280 Color: 1328
Size: 283178 Color: 630

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 369928 Color: 1431
Size: 335010 Color: 1147
Size: 295063 Color: 778

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 353055 Color: 1306
Size: 350935 Color: 1288
Size: 296011 Color: 789

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 393092 Color: 1593
Size: 311940 Color: 934
Size: 294969 Color: 776

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 380643 Color: 1524
Size: 363311 Color: 1380
Size: 256047 Color: 194

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 362182 Color: 1373
Size: 350884 Color: 1285
Size: 286935 Color: 675

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 360023 Color: 1356
Size: 351190 Color: 1290
Size: 288788 Color: 696

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 366192 Color: 1398
Size: 350710 Color: 1280
Size: 283099 Color: 628

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 350237 Color: 1276
Size: 350190 Color: 1275
Size: 299574 Color: 831

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 391846 Color: 1587
Size: 349540 Color: 1271
Size: 258615 Color: 245

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 388775 Color: 1573
Size: 306938 Color: 889
Size: 304288 Color: 866

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 344574 Color: 1226
Size: 329936 Color: 1094
Size: 325491 Color: 1057

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 394371 Color: 1598
Size: 333590 Color: 1133
Size: 272040 Color: 484

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 400398 Color: 1641
Size: 322349 Color: 1021
Size: 277254 Color: 545

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 383115 Color: 1537
Size: 357758 Color: 1334
Size: 259128 Color: 260

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 387390 Color: 1564
Size: 353739 Color: 1310
Size: 258872 Color: 255

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 383610 Color: 1540
Size: 332605 Color: 1123
Size: 283786 Color: 635

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 357457 Color: 1332
Size: 335253 Color: 1149
Size: 307291 Color: 896

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 402652 Color: 1653
Size: 326359 Color: 1067
Size: 270990 Color: 470

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 345669 Color: 1237
Size: 345588 Color: 1236
Size: 308744 Color: 910

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 378242 Color: 1505
Size: 349226 Color: 1268
Size: 272533 Color: 492

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 406185 Color: 1668
Size: 312215 Color: 935
Size: 281601 Color: 601

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 406301 Color: 1669
Size: 332187 Color: 1115
Size: 261513 Color: 306

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 406323 Color: 1670
Size: 338221 Color: 1180
Size: 255457 Color: 178

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 406477 Color: 1671
Size: 320458 Color: 1007
Size: 273066 Color: 499

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 407687 Color: 1672
Size: 324006 Color: 1040
Size: 268308 Color: 428

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 407720 Color: 1673
Size: 304043 Color: 865
Size: 288238 Color: 690

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 407816 Color: 1674
Size: 340418 Color: 1200
Size: 251767 Color: 60

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 407901 Color: 1675
Size: 332416 Color: 1117
Size: 259684 Color: 277

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 408195 Color: 1676
Size: 324905 Color: 1049
Size: 266901 Color: 402

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 408247 Color: 1677
Size: 296343 Color: 794
Size: 295411 Color: 784

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 408839 Color: 1679
Size: 317743 Color: 985
Size: 273419 Color: 507

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 408852 Color: 1680
Size: 303734 Color: 860
Size: 287415 Color: 678

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 408998 Color: 1681
Size: 322522 Color: 1023
Size: 268481 Color: 430

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 409029 Color: 1682
Size: 324404 Color: 1043
Size: 266568 Color: 393

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 409168 Color: 1683
Size: 313176 Color: 945
Size: 277657 Color: 552

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 409613 Color: 1685
Size: 324599 Color: 1044
Size: 265789 Color: 374

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 410445 Color: 1686
Size: 315225 Color: 962
Size: 274331 Color: 520

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 410543 Color: 1687
Size: 303101 Color: 855
Size: 286357 Color: 663

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 410719 Color: 1688
Size: 336679 Color: 1159
Size: 252603 Color: 85

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 410815 Color: 1689
Size: 302490 Color: 849
Size: 286696 Color: 671

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 410974 Color: 1690
Size: 319692 Color: 1000
Size: 269335 Color: 442

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 411361 Color: 1691
Size: 330926 Color: 1103
Size: 257714 Color: 229

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 411450 Color: 1692
Size: 333454 Color: 1131
Size: 255097 Color: 159

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 411886 Color: 1693
Size: 334945 Color: 1146
Size: 253170 Color: 109

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 411993 Color: 1694
Size: 302874 Color: 852
Size: 285134 Color: 647

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 412016 Color: 1695
Size: 334533 Color: 1143
Size: 253452 Color: 119

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 412304 Color: 1696
Size: 313202 Color: 946
Size: 274495 Color: 522

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 412478 Color: 1697
Size: 315913 Color: 971
Size: 271610 Color: 478

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 412585 Color: 1698
Size: 313573 Color: 948
Size: 273843 Color: 513

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 412655 Color: 1699
Size: 322734 Color: 1028
Size: 264612 Color: 358

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 412682 Color: 1700
Size: 297246 Color: 801
Size: 290073 Color: 711

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 412718 Color: 1701
Size: 305102 Color: 874
Size: 282181 Color: 613

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 412752 Color: 1702
Size: 304293 Color: 867
Size: 282956 Color: 625

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 413422 Color: 1703
Size: 320621 Color: 1009
Size: 265958 Color: 379

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 413472 Color: 1704
Size: 315155 Color: 961
Size: 271374 Color: 474

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 413548 Color: 1705
Size: 316388 Color: 976
Size: 270065 Color: 454

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 413741 Color: 1706
Size: 324006 Color: 1039
Size: 262254 Color: 319

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 414405 Color: 1707
Size: 299292 Color: 827
Size: 286304 Color: 662

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 414537 Color: 1708
Size: 316216 Color: 974
Size: 269248 Color: 441

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 414931 Color: 1709
Size: 323664 Color: 1036
Size: 261406 Color: 305

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 415658 Color: 1710
Size: 316116 Color: 972
Size: 268227 Color: 427

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 416615 Color: 1712
Size: 321176 Color: 1011
Size: 262210 Color: 317

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 417346 Color: 1713
Size: 315487 Color: 965
Size: 267168 Color: 405

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 418028 Color: 1714
Size: 322727 Color: 1027
Size: 259246 Color: 266

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 418068 Color: 1715
Size: 293559 Color: 749
Size: 288374 Color: 692

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 418098 Color: 1716
Size: 294045 Color: 755
Size: 287858 Color: 686

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 418424 Color: 1717
Size: 316269 Color: 975
Size: 265308 Color: 364

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 418437 Color: 1718
Size: 296216 Color: 792
Size: 285348 Color: 650

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 418565 Color: 1720
Size: 304846 Color: 871
Size: 276590 Color: 538

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 419218 Color: 1721
Size: 316604 Color: 978
Size: 264179 Color: 353

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 419363 Color: 1722
Size: 295196 Color: 779
Size: 285442 Color: 653

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 419854 Color: 1723
Size: 325535 Color: 1058
Size: 254612 Color: 149

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 420906 Color: 1725
Size: 322538 Color: 1024
Size: 256557 Color: 204

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 420942 Color: 1726
Size: 320308 Color: 1005
Size: 258751 Color: 251

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 420971 Color: 1727
Size: 325772 Color: 1061
Size: 253258 Color: 110

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 421522 Color: 1729
Size: 312890 Color: 941
Size: 265589 Color: 370

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 421855 Color: 1730
Size: 289089 Color: 700
Size: 289057 Color: 699

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 422079 Color: 1731
Size: 311422 Color: 929
Size: 266500 Color: 392

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 422345 Color: 1733
Size: 295822 Color: 788
Size: 281834 Color: 605

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 422795 Color: 1734
Size: 294736 Color: 769
Size: 282470 Color: 617

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 422870 Color: 1735
Size: 309923 Color: 920
Size: 267208 Color: 406

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 423224 Color: 1736
Size: 298067 Color: 811
Size: 278710 Color: 568

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 423257 Color: 1737
Size: 321304 Color: 1012
Size: 255440 Color: 177

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 423421 Color: 1738
Size: 298278 Color: 815
Size: 278302 Color: 561

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 423924 Color: 1739
Size: 318843 Color: 993
Size: 257234 Color: 215

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 424001 Color: 1740
Size: 291261 Color: 723
Size: 284739 Color: 642

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 424124 Color: 1741
Size: 319633 Color: 999
Size: 256244 Color: 202

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 424521 Color: 1742
Size: 305941 Color: 879
Size: 269539 Color: 446

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 426280 Color: 1743
Size: 319433 Color: 998
Size: 254288 Color: 139

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 426281 Color: 1744
Size: 287723 Color: 684
Size: 285997 Color: 660

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 426582 Color: 1746
Size: 308846 Color: 911
Size: 264573 Color: 357

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 426784 Color: 1747
Size: 310200 Color: 922
Size: 263017 Color: 331

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 426994 Color: 1748
Size: 292205 Color: 735
Size: 280802 Color: 589

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 427297 Color: 1749
Size: 288205 Color: 689
Size: 284499 Color: 640

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 427357 Color: 1750
Size: 287659 Color: 682
Size: 284985 Color: 644

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 427500 Color: 1751
Size: 308544 Color: 908
Size: 263957 Color: 349

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 427985 Color: 1753
Size: 301772 Color: 846
Size: 270244 Color: 459

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 428519 Color: 1755
Size: 295730 Color: 787
Size: 275752 Color: 532

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 428744 Color: 1756
Size: 299696 Color: 834
Size: 271561 Color: 477

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 428798 Color: 1757
Size: 292653 Color: 741
Size: 278550 Color: 567

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 429351 Color: 1758
Size: 292947 Color: 744
Size: 277703 Color: 553

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 429355 Color: 1759
Size: 301628 Color: 844
Size: 269018 Color: 437

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 429488 Color: 1760
Size: 309771 Color: 918
Size: 260742 Color: 297

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 429708 Color: 1761
Size: 287169 Color: 676
Size: 283124 Color: 629

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 430139 Color: 1762
Size: 303834 Color: 862
Size: 266028 Color: 381

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 430818 Color: 1763
Size: 305286 Color: 877
Size: 263897 Color: 347

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 430923 Color: 1764
Size: 298679 Color: 822
Size: 270399 Color: 463

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 431211 Color: 1766
Size: 300859 Color: 839
Size: 267931 Color: 424

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 431861 Color: 1767
Size: 285388 Color: 651
Size: 282752 Color: 623

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 432427 Color: 1769
Size: 304435 Color: 868
Size: 263139 Color: 333

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 432960 Color: 1770
Size: 291569 Color: 731
Size: 275472 Color: 528

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 433075 Color: 1771
Size: 308248 Color: 905
Size: 258678 Color: 248

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 434207 Color: 1773
Size: 286877 Color: 673
Size: 278917 Color: 570

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 435148 Color: 1774
Size: 298650 Color: 821
Size: 266203 Color: 387

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 435255 Color: 1775
Size: 313031 Color: 944
Size: 251715 Color: 59

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 435529 Color: 1776
Size: 297145 Color: 799
Size: 267327 Color: 408

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 436473 Color: 1777
Size: 299997 Color: 836
Size: 263531 Color: 341

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 436909 Color: 1778
Size: 310986 Color: 925
Size: 252106 Color: 71

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 437084 Color: 1779
Size: 293488 Color: 748
Size: 269429 Color: 443

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 437932 Color: 1781
Size: 306314 Color: 880
Size: 255755 Color: 185

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 438346 Color: 1782
Size: 298488 Color: 817
Size: 263167 Color: 334

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 438361 Color: 1783
Size: 294763 Color: 770
Size: 266877 Color: 401

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 438461 Color: 1784
Size: 301167 Color: 841
Size: 260373 Color: 292

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 439434 Color: 1786
Size: 295023 Color: 777
Size: 265544 Color: 367

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 439438 Color: 1787
Size: 309843 Color: 919
Size: 250720 Color: 27

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 439513 Color: 1788
Size: 303367 Color: 856
Size: 257121 Color: 211

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 440290 Color: 1790
Size: 282429 Color: 615
Size: 277282 Color: 547

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 440681 Color: 1791
Size: 293014 Color: 745
Size: 266306 Color: 389

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 440985 Color: 1792
Size: 301602 Color: 843
Size: 257414 Color: 220

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 440994 Color: 1793
Size: 289308 Color: 704
Size: 269699 Color: 448

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 441183 Color: 1794
Size: 294771 Color: 771
Size: 264047 Color: 351

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 441582 Color: 1795
Size: 290571 Color: 717
Size: 267848 Color: 422

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 442149 Color: 1796
Size: 294293 Color: 760
Size: 263559 Color: 343

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 442353 Color: 1797
Size: 289861 Color: 709
Size: 267787 Color: 420

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 442615 Color: 1798
Size: 298156 Color: 812
Size: 259230 Color: 264

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 442854 Color: 1799
Size: 294535 Color: 764
Size: 262612 Color: 323

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 442944 Color: 1800
Size: 300494 Color: 837
Size: 256563 Color: 205

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 444473 Color: 1803
Size: 303690 Color: 859
Size: 251838 Color: 63

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 444866 Color: 1804
Size: 292372 Color: 736
Size: 262763 Color: 325

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 445680 Color: 1805
Size: 298622 Color: 820
Size: 255699 Color: 184

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 445685 Color: 1806
Size: 294649 Color: 766
Size: 259667 Color: 276

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 445769 Color: 1807
Size: 295379 Color: 782
Size: 258853 Color: 254

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 446417 Color: 1808
Size: 285440 Color: 652
Size: 268144 Color: 425

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 446749 Color: 1809
Size: 294439 Color: 763
Size: 258813 Color: 253

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 446955 Color: 1810
Size: 287689 Color: 683
Size: 265357 Color: 365

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 447098 Color: 1811
Size: 295213 Color: 781
Size: 257690 Color: 227

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 447425 Color: 1812
Size: 298251 Color: 814
Size: 254325 Color: 140

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 447465 Color: 1813
Size: 276430 Color: 536
Size: 276106 Color: 534

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 447572 Color: 1814
Size: 278305 Color: 562
Size: 274124 Color: 517

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 447625 Color: 1815
Size: 281519 Color: 599
Size: 270857 Color: 468

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 447662 Color: 1816
Size: 280651 Color: 585
Size: 271688 Color: 481

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 448090 Color: 1817
Size: 294212 Color: 759
Size: 257699 Color: 228

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 448481 Color: 1818
Size: 278271 Color: 560
Size: 273249 Color: 503

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 448759 Color: 1819
Size: 298359 Color: 816
Size: 252883 Color: 96

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 448866 Color: 1820
Size: 296160 Color: 791
Size: 254975 Color: 157

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 448985 Color: 1821
Size: 290902 Color: 720
Size: 260114 Color: 290

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 449439 Color: 1822
Size: 282805 Color: 624
Size: 267757 Color: 419

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 449606 Color: 1824
Size: 296704 Color: 796
Size: 253691 Color: 123

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 449904 Color: 1825
Size: 291776 Color: 733
Size: 258321 Color: 239

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 450028 Color: 1826
Size: 299307 Color: 828
Size: 250666 Color: 25

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 450277 Color: 1827
Size: 277067 Color: 543
Size: 272657 Color: 495

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 450324 Color: 1828
Size: 298225 Color: 813
Size: 251452 Color: 53

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 450583 Color: 1829
Size: 281078 Color: 590
Size: 268340 Color: 429

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 450597 Color: 1830
Size: 298934 Color: 824
Size: 250470 Color: 20

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 450963 Color: 1831
Size: 290352 Color: 716
Size: 258686 Color: 249

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 451019 Color: 1832
Size: 274806 Color: 524
Size: 274176 Color: 518

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 451512 Color: 1833
Size: 295402 Color: 783
Size: 253087 Color: 107

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 451558 Color: 1834
Size: 296079 Color: 790
Size: 252364 Color: 79

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 452053 Color: 1835
Size: 285830 Color: 657
Size: 262118 Color: 316

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 452113 Color: 1836
Size: 289266 Color: 703
Size: 258622 Color: 247

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 452386 Color: 1837
Size: 289504 Color: 706
Size: 258111 Color: 236

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 453286 Color: 1838
Size: 285603 Color: 656
Size: 261112 Color: 301

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 453428 Color: 1839
Size: 291016 Color: 721
Size: 255557 Color: 180

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 453587 Color: 1840
Size: 294962 Color: 775
Size: 251452 Color: 54

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 453849 Color: 1841
Size: 281482 Color: 598
Size: 264670 Color: 359

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 453962 Color: 1842
Size: 288949 Color: 698
Size: 257090 Color: 209

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 454101 Color: 1843
Size: 286112 Color: 661
Size: 259788 Color: 281

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 454161 Color: 1844
Size: 279065 Color: 572
Size: 266775 Color: 399

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 454796 Color: 1845
Size: 287756 Color: 685
Size: 257449 Color: 222

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 454918 Color: 1846
Size: 278161 Color: 558
Size: 266922 Color: 403

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 454935 Color: 1847
Size: 274492 Color: 521
Size: 270574 Color: 465

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 454954 Color: 1848
Size: 290222 Color: 714
Size: 254825 Color: 155

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 455370 Color: 1849
Size: 291559 Color: 730
Size: 253072 Color: 105

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 455663 Color: 1850
Size: 286538 Color: 666
Size: 257800 Color: 231

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 455956 Color: 1851
Size: 277221 Color: 544
Size: 266824 Color: 400

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 456211 Color: 1852
Size: 285049 Color: 646
Size: 258741 Color: 250

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 456327 Color: 1853
Size: 286544 Color: 667
Size: 257130 Color: 212

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 456707 Color: 1854
Size: 283776 Color: 634
Size: 259518 Color: 272

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 457070 Color: 1855
Size: 282036 Color: 608
Size: 260895 Color: 298

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 457380 Color: 1856
Size: 277043 Color: 542
Size: 265578 Color: 369

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 457400 Color: 1857
Size: 285157 Color: 648
Size: 257444 Color: 221

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 457836 Color: 1858
Size: 287532 Color: 679
Size: 254633 Color: 150

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 459150 Color: 1859
Size: 277890 Color: 555
Size: 262961 Color: 329

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 459187 Color: 1860
Size: 275062 Color: 526
Size: 265752 Color: 373

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 459199 Color: 1861
Size: 281738 Color: 603
Size: 259064 Color: 258

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 459479 Color: 1862
Size: 271936 Color: 482
Size: 268586 Color: 432

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 459778 Color: 1863
Size: 288899 Color: 697
Size: 251324 Color: 49

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 460083 Color: 1864
Size: 278207 Color: 559
Size: 261711 Color: 310

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 460245 Color: 1865
Size: 285953 Color: 659
Size: 253803 Color: 127

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 461086 Color: 1866
Size: 270156 Color: 456
Size: 268759 Color: 436

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 461678 Color: 1867
Size: 278794 Color: 569
Size: 259529 Color: 273

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 462014 Color: 1868
Size: 280406 Color: 583
Size: 257581 Color: 224

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 462089 Color: 1869
Size: 273505 Color: 510
Size: 264407 Color: 356

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 462315 Color: 1870
Size: 271427 Color: 475
Size: 266259 Color: 388

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 462446 Color: 1871
Size: 277847 Color: 554
Size: 259708 Color: 280

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 462679 Color: 1872
Size: 279539 Color: 576
Size: 257783 Color: 230

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 462762 Color: 1873
Size: 278337 Color: 564
Size: 258902 Color: 257

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 462771 Color: 1874
Size: 283063 Color: 627
Size: 254167 Color: 137

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 462920 Color: 1875
Size: 282102 Color: 610
Size: 254979 Color: 158

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 463080 Color: 1876
Size: 271503 Color: 476
Size: 265418 Color: 366

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 463622 Color: 1877
Size: 282399 Color: 614
Size: 253980 Color: 133

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 463948 Color: 1878
Size: 268576 Color: 431
Size: 267477 Color: 410

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 463982 Color: 1879
Size: 278385 Color: 565
Size: 257634 Color: 225

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 464048 Color: 1880
Size: 284303 Color: 637
Size: 251650 Color: 56

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 464135 Color: 1881
Size: 279981 Color: 579
Size: 255885 Color: 191

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 464167 Color: 1882
Size: 281407 Color: 595
Size: 254427 Color: 141

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 464265 Color: 1883
Size: 276509 Color: 537
Size: 259227 Color: 263

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 464841 Color: 1884
Size: 281349 Color: 594
Size: 253811 Color: 128

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 465970 Color: 1885
Size: 280726 Color: 587
Size: 253305 Color: 113

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 466623 Color: 1886
Size: 270896 Color: 469
Size: 262482 Color: 321

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 467003 Color: 1887
Size: 271064 Color: 471
Size: 261934 Color: 313

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 467326 Color: 1888
Size: 280402 Color: 582
Size: 252273 Color: 76

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 467381 Color: 1889
Size: 282167 Color: 612
Size: 250453 Color: 18

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 467987 Color: 1890
Size: 281455 Color: 597
Size: 250559 Color: 23

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 468043 Color: 1891
Size: 267830 Color: 421
Size: 264128 Color: 352

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 468641 Color: 1892
Size: 275600 Color: 530
Size: 255760 Color: 186

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 468652 Color: 1893
Size: 270073 Color: 455
Size: 261276 Color: 303

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 469076 Color: 1894
Size: 272484 Color: 489
Size: 258441 Color: 242

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 469320 Color: 1896
Size: 268180 Color: 426
Size: 262501 Color: 322

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 469323 Color: 1897
Size: 273681 Color: 512
Size: 256997 Color: 208

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 469435 Color: 1898
Size: 269191 Color: 440
Size: 261375 Color: 304

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 469473 Color: 1899
Size: 274565 Color: 523
Size: 255963 Color: 193

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 469731 Color: 1900
Size: 273948 Color: 516
Size: 256322 Color: 203

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 469821 Color: 1901
Size: 266326 Color: 390
Size: 263854 Color: 346

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 470170 Color: 1902
Size: 273645 Color: 511
Size: 256186 Color: 199

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 471180 Color: 1903
Size: 273023 Color: 498
Size: 255798 Color: 188

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 471466 Color: 1904
Size: 272376 Color: 487
Size: 256159 Color: 198

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 471595 Color: 1905
Size: 275677 Color: 531
Size: 252729 Color: 92

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 472062 Color: 1906
Size: 265850 Color: 376
Size: 262089 Color: 315

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 472273 Color: 1907
Size: 265734 Color: 372
Size: 261994 Color: 314

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 472474 Color: 1908
Size: 277304 Color: 548
Size: 250223 Color: 12

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 472545 Color: 1909
Size: 263908 Color: 348
Size: 263548 Color: 342

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 472972 Color: 1910
Size: 267674 Color: 416
Size: 259355 Color: 269

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 473719 Color: 1911
Size: 266179 Color: 385
Size: 260103 Color: 289

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 473999 Color: 1912
Size: 272973 Color: 497
Size: 253029 Color: 101

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 474185 Color: 1914
Size: 270306 Color: 460
Size: 255510 Color: 179

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 474231 Color: 1915
Size: 275575 Color: 529
Size: 250195 Color: 11

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 474382 Color: 1916
Size: 273095 Color: 500
Size: 252524 Color: 83

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 474571 Color: 1917
Size: 272525 Color: 491
Size: 252905 Color: 97

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 474758 Color: 1918
Size: 266001 Color: 380
Size: 259242 Color: 265

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 474891 Color: 1919
Size: 274191 Color: 519
Size: 250919 Color: 37

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 475073 Color: 1920
Size: 271662 Color: 480
Size: 253266 Color: 111

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 475319 Color: 1921
Size: 262900 Color: 328
Size: 261782 Color: 311

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 475524 Color: 1922
Size: 272539 Color: 493
Size: 251938 Color: 67

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 475829 Color: 1923
Size: 267211 Color: 407
Size: 256961 Color: 207

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 476223 Color: 1924
Size: 272929 Color: 496
Size: 250849 Color: 34

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 476986 Color: 1925
Size: 269480 Color: 444
Size: 253535 Color: 121

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 477050 Color: 1926
Size: 269527 Color: 445
Size: 253424 Color: 118

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 477385 Color: 1927
Size: 264926 Color: 363
Size: 257690 Color: 226

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 478062 Color: 1928
Size: 269093 Color: 439
Size: 252846 Color: 95

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 478063 Color: 1929
Size: 269753 Color: 449
Size: 252185 Color: 73

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 478595 Color: 1930
Size: 270012 Color: 453
Size: 251394 Color: 52

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 478807 Color: 1931
Size: 266088 Color: 383
Size: 255106 Color: 161

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 478845 Color: 1932
Size: 262263 Color: 320
Size: 258893 Color: 256

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 479359 Color: 1933
Size: 267606 Color: 414
Size: 253036 Color: 102

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 479458 Color: 1934
Size: 270452 Color: 464
Size: 250091 Color: 8

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 480068 Color: 1935
Size: 266588 Color: 394
Size: 253345 Color: 115

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 480293 Color: 1936
Size: 266701 Color: 396
Size: 253007 Color: 100

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 480350 Color: 1937
Size: 264332 Color: 355
Size: 255319 Color: 170

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 480444 Color: 1938
Size: 263367 Color: 338
Size: 256190 Color: 200

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 480878 Color: 1939
Size: 260052 Color: 287
Size: 259071 Color: 259

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 481252 Color: 1940
Size: 264892 Color: 362
Size: 253857 Color: 130

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 481577 Color: 1941
Size: 264195 Color: 354
Size: 254229 Color: 138

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 482899 Color: 1942
Size: 261829 Color: 312
Size: 255273 Color: 167

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 482955 Color: 1943
Size: 263979 Color: 350
Size: 253067 Color: 104

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 483536 Color: 1944
Size: 265928 Color: 377
Size: 250537 Color: 21

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 483726 Color: 1945
Size: 263193 Color: 336
Size: 253082 Color: 106

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 483829 Color: 1946
Size: 258808 Color: 252
Size: 257364 Color: 217

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 484461 Color: 1947
Size: 260722 Color: 296
Size: 254818 Color: 154

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 484500 Color: 1948
Size: 264770 Color: 361
Size: 250731 Color: 28

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 484932 Color: 1949
Size: 259692 Color: 279
Size: 255377 Color: 174

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 485793 Color: 1950
Size: 263497 Color: 340
Size: 250711 Color: 26

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 486086 Color: 1951
Size: 258097 Color: 235
Size: 255818 Color: 189

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 486655 Color: 1952
Size: 260037 Color: 286
Size: 253309 Color: 114

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 486771 Color: 1953
Size: 262658 Color: 324
Size: 250572 Color: 24

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 486880 Color: 1954
Size: 259662 Color: 275
Size: 253459 Color: 120

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 487225 Color: 1955
Size: 257413 Color: 219
Size: 255363 Color: 173

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 487346 Color: 1956
Size: 259837 Color: 282
Size: 252818 Color: 94

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 487439 Color: 1957
Size: 260640 Color: 295
Size: 251922 Color: 65

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 487724 Color: 1958
Size: 259883 Color: 283
Size: 252394 Color: 80

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 488081 Color: 1959
Size: 260590 Color: 294
Size: 251330 Color: 50

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 488198 Color: 1960
Size: 256114 Color: 196
Size: 255689 Color: 182

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 488274 Color: 1961
Size: 259397 Color: 270
Size: 252330 Color: 77

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 488854 Color: 1962
Size: 259279 Color: 267
Size: 251868 Color: 64

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 489193 Color: 1963
Size: 260012 Color: 285
Size: 250796 Color: 30

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 489371 Color: 1964
Size: 255341 Color: 172
Size: 255289 Color: 168

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 489577 Color: 1965
Size: 258077 Color: 234
Size: 252347 Color: 78

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 489740 Color: 1966
Size: 258618 Color: 246
Size: 251643 Color: 55

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 489978 Color: 1967
Size: 259208 Color: 261
Size: 250815 Color: 33

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 490171 Color: 1968
Size: 255147 Color: 164
Size: 254683 Color: 151

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 490285 Color: 1969
Size: 255169 Color: 165
Size: 254547 Color: 147

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 490293 Color: 1970
Size: 256892 Color: 206
Size: 252816 Color: 93

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 490325 Color: 1971
Size: 259212 Color: 262
Size: 250464 Color: 19

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 490349 Color: 1972
Size: 257225 Color: 214
Size: 252427 Color: 81

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 490402 Color: 1973
Size: 255108 Color: 162
Size: 254491 Color: 145

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 490674 Color: 1974
Size: 258004 Color: 233
Size: 251323 Color: 48

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 491111 Color: 1975
Size: 258604 Color: 244
Size: 250286 Color: 14

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 491836 Color: 1976
Size: 254503 Color: 146
Size: 253662 Color: 122

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 492182 Color: 1977
Size: 255106 Color: 160
Size: 252713 Color: 91

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 492549 Color: 1978
Size: 254807 Color: 153
Size: 252645 Color: 89

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 492567 Color: 1979
Size: 254430 Color: 142
Size: 253004 Color: 99

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 492725 Color: 1980
Size: 256089 Color: 195
Size: 251187 Color: 43

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 492731 Color: 1981
Size: 255436 Color: 176
Size: 251834 Color: 62

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 492878 Color: 1982
Size: 254866 Color: 156
Size: 252257 Color: 74

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 492884 Color: 1983
Size: 253982 Color: 134
Size: 253135 Color: 108

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 492904 Color: 1984
Size: 254569 Color: 148
Size: 252528 Color: 84

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 493480 Color: 1985
Size: 253910 Color: 131
Size: 252611 Color: 86

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 493585 Color: 1986
Size: 254473 Color: 144
Size: 251943 Color: 68

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 494492 Color: 1987
Size: 253852 Color: 129
Size: 251657 Color: 57

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 494690 Color: 1988
Size: 254460 Color: 143
Size: 250851 Color: 35

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 494769 Color: 1989
Size: 253917 Color: 132
Size: 251315 Color: 46

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 496478 Color: 1990
Size: 252710 Color: 90
Size: 250813 Color: 32

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 496764 Color: 1991
Size: 251927 Color: 66
Size: 251310 Color: 45

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 496933 Color: 1992
Size: 251990 Color: 69
Size: 251078 Color: 41

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 497016 Color: 1993
Size: 252617 Color: 87
Size: 250368 Color: 17

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 498072 Color: 1994
Size: 251035 Color: 39
Size: 250894 Color: 36

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 498102 Color: 1995
Size: 251828 Color: 61
Size: 250071 Color: 6

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 498593 Color: 1996
Size: 251111 Color: 42
Size: 250297 Color: 15

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 498642 Color: 1997
Size: 251048 Color: 40
Size: 250311 Color: 16

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 499702 Color: 1998
Size: 250246 Color: 13
Size: 250053 Color: 3

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 499816 Color: 2000
Size: 250109 Color: 9
Size: 250076 Color: 7

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 499816 Color: 1999
Size: 250118 Color: 10
Size: 250067 Color: 4

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 408588 Color: 1678
Size: 298900 Color: 823
Size: 292512 Color: 739

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 409241 Color: 1684
Size: 297096 Color: 798
Size: 293663 Color: 751

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 418452 Color: 1719
Size: 303393 Color: 857
Size: 278155 Color: 557

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 426536 Color: 1745
Size: 307387 Color: 897
Size: 266077 Color: 382

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 427924 Color: 1752
Size: 288443 Color: 695
Size: 283633 Color: 633

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 431052 Color: 1765
Size: 291354 Color: 728
Size: 277594 Color: 551

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 432352 Color: 1768
Size: 298054 Color: 810
Size: 269594 Color: 447

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 433200 Color: 1772
Size: 294435 Color: 762
Size: 272365 Color: 486

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 438517 Color: 1785
Size: 305284 Color: 876
Size: 256199 Color: 201

Bin 499: 1 of cap free
Amount of items: 3
Items: 
Size: 379968 Color: 1517
Size: 333573 Color: 1132
Size: 286459 Color: 665

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 449469 Color: 1823
Size: 281788 Color: 604
Size: 268743 Color: 435

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 469278 Color: 1895
Size: 279458 Color: 575
Size: 251264 Color: 44

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 406062 Color: 1666
Size: 335604 Color: 1151
Size: 258334 Color: 240

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 402549 Color: 1650
Size: 309326 Color: 915
Size: 288125 Color: 688

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 403927 Color: 1658
Size: 335825 Color: 1153
Size: 260248 Color: 291

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 402718 Color: 1654
Size: 339057 Color: 1189
Size: 258225 Color: 237

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 380530 Color: 1522
Size: 342861 Color: 1212
Size: 276609 Color: 539

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 397954 Color: 1627
Size: 307484 Color: 898
Size: 294562 Color: 765

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 380232 Color: 1520
Size: 362242 Color: 1374
Size: 257526 Color: 223

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 398537 Color: 1633
Size: 347722 Color: 1257
Size: 253741 Color: 124

Bin 510: 1 of cap free
Amount of items: 3
Items: 
Size: 351669 Color: 1295
Size: 346621 Color: 1247
Size: 301710 Color: 845

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 379843 Color: 1515
Size: 331812 Color: 1112
Size: 288345 Color: 691

Bin 512: 1 of cap free
Amount of items: 3
Items: 
Size: 375051 Color: 1478
Size: 327116 Color: 1070
Size: 297833 Color: 808

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 378980 Color: 1509
Size: 326173 Color: 1065
Size: 294847 Color: 773

Bin 514: 1 of cap free
Amount of items: 3
Items: 
Size: 369292 Color: 1429
Size: 332905 Color: 1124
Size: 297803 Color: 806

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 391612 Color: 1585
Size: 354635 Color: 1317
Size: 253753 Color: 125

Bin 516: 1 of cap free
Amount of items: 3
Items: 
Size: 386121 Color: 1555
Size: 358284 Color: 1340
Size: 255595 Color: 181

Bin 517: 1 of cap free
Amount of items: 3
Items: 
Size: 367241 Color: 1409
Size: 320306 Color: 1004
Size: 312453 Color: 938

Bin 518: 1 of cap free
Amount of items: 3
Items: 
Size: 374643 Color: 1471
Size: 374395 Color: 1469
Size: 250962 Color: 38

Bin 519: 1 of cap free
Amount of items: 3
Items: 
Size: 371176 Color: 1440
Size: 315039 Color: 958
Size: 313785 Color: 953

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 374066 Color: 1466
Size: 370536 Color: 1435
Size: 255398 Color: 175

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 375699 Color: 1483
Size: 330300 Color: 1098
Size: 294001 Color: 754

Bin 522: 1 of cap free
Amount of items: 3
Items: 
Size: 349770 Color: 1272
Size: 343026 Color: 1214
Size: 307204 Color: 894

Bin 523: 1 of cap free
Amount of items: 3
Items: 
Size: 380603 Color: 1523
Size: 312315 Color: 936
Size: 307082 Color: 892

Bin 524: 1 of cap free
Amount of items: 3
Items: 
Size: 344649 Color: 1227
Size: 340674 Color: 1201
Size: 314677 Color: 955

Bin 525: 1 of cap free
Amount of items: 3
Items: 
Size: 396458 Color: 1616
Size: 330091 Color: 1095
Size: 273451 Color: 508

Bin 526: 1 of cap free
Amount of items: 3
Items: 
Size: 377686 Color: 1500
Size: 357634 Color: 1333
Size: 264680 Color: 360

Bin 527: 1 of cap free
Amount of items: 3
Items: 
Size: 404025 Color: 1659
Size: 338088 Color: 1177
Size: 257887 Color: 232

Bin 528: 1 of cap free
Amount of items: 3
Items: 
Size: 355360 Color: 1321
Size: 354590 Color: 1316
Size: 290050 Color: 710

Bin 529: 1 of cap free
Amount of items: 3
Items: 
Size: 355978 Color: 1324
Size: 354810 Color: 1319
Size: 289212 Color: 702

Bin 530: 1 of cap free
Amount of items: 3
Items: 
Size: 382032 Color: 1532
Size: 337531 Color: 1169
Size: 280437 Color: 584

Bin 531: 2 of cap free
Amount of items: 3
Items: 
Size: 416455 Color: 1711
Size: 325117 Color: 1053
Size: 258427 Color: 241

Bin 532: 2 of cap free
Amount of items: 3
Items: 
Size: 422171 Color: 1732
Size: 294862 Color: 774
Size: 282966 Color: 626

Bin 533: 2 of cap free
Amount of items: 3
Items: 
Size: 385729 Color: 1551
Size: 353088 Color: 1307
Size: 261182 Color: 302

Bin 534: 2 of cap free
Amount of items: 3
Items: 
Size: 398523 Color: 1632
Size: 339918 Color: 1194
Size: 261558 Color: 308

Bin 535: 2 of cap free
Amount of items: 3
Items: 
Size: 349519 Color: 1270
Size: 339882 Color: 1193
Size: 310598 Color: 924

Bin 536: 2 of cap free
Amount of items: 3
Items: 
Size: 405457 Color: 1663
Size: 337449 Color: 1168
Size: 257093 Color: 210

Bin 537: 2 of cap free
Amount of items: 3
Items: 
Size: 391106 Color: 1582
Size: 345311 Color: 1231
Size: 263582 Color: 344

Bin 538: 2 of cap free
Amount of items: 3
Items: 
Size: 380998 Color: 1528
Size: 332166 Color: 1114
Size: 286835 Color: 672

Bin 539: 2 of cap free
Amount of items: 3
Items: 
Size: 403089 Color: 1655
Size: 326901 Color: 1069
Size: 270009 Color: 452

Bin 540: 2 of cap free
Amount of items: 3
Items: 
Size: 386897 Color: 1560
Size: 353041 Color: 1305
Size: 260061 Color: 288

Bin 541: 2 of cap free
Amount of items: 3
Items: 
Size: 365847 Color: 1396
Size: 340937 Color: 1203
Size: 293215 Color: 746

Bin 542: 2 of cap free
Amount of items: 3
Items: 
Size: 374958 Color: 1476
Size: 340690 Color: 1202
Size: 284351 Color: 638

Bin 543: 2 of cap free
Amount of items: 3
Items: 
Size: 381026 Color: 1529
Size: 329288 Color: 1088
Size: 289685 Color: 708

Bin 544: 2 of cap free
Amount of items: 3
Items: 
Size: 368714 Color: 1424
Size: 338800 Color: 1186
Size: 292485 Color: 738

Bin 545: 2 of cap free
Amount of items: 3
Items: 
Size: 379958 Color: 1516
Size: 324832 Color: 1046
Size: 295209 Color: 780

Bin 546: 2 of cap free
Amount of items: 3
Items: 
Size: 380456 Color: 1521
Size: 337040 Color: 1164
Size: 282503 Color: 618

Bin 547: 2 of cap free
Amount of items: 3
Items: 
Size: 374705 Color: 1473
Size: 331242 Color: 1107
Size: 294052 Color: 756

Bin 548: 2 of cap free
Amount of items: 3
Items: 
Size: 382205 Color: 1533
Size: 326082 Color: 1064
Size: 291712 Color: 732

Bin 549: 2 of cap free
Amount of items: 3
Items: 
Size: 360266 Color: 1360
Size: 329718 Color: 1093
Size: 310015 Color: 921

Bin 550: 2 of cap free
Amount of items: 3
Items: 
Size: 366397 Color: 1400
Size: 325557 Color: 1059
Size: 308045 Color: 904

Bin 551: 2 of cap free
Amount of items: 3
Items: 
Size: 364560 Color: 1391
Size: 356003 Color: 1325
Size: 279436 Color: 574

Bin 552: 2 of cap free
Amount of items: 3
Items: 
Size: 386302 Color: 1557
Size: 348070 Color: 1260
Size: 265627 Color: 371

Bin 553: 2 of cap free
Amount of items: 3
Items: 
Size: 389545 Color: 1576
Size: 339746 Color: 1192
Size: 270708 Color: 467

Bin 554: 2 of cap free
Amount of items: 3
Items: 
Size: 376702 Color: 1491
Size: 316936 Color: 979
Size: 306361 Color: 882

Bin 555: 2 of cap free
Amount of items: 3
Items: 
Size: 346409 Color: 1243
Size: 342177 Color: 1209
Size: 311413 Color: 928

Bin 556: 3 of cap free
Amount of items: 3
Items: 
Size: 474104 Color: 1913
Size: 272621 Color: 494
Size: 253273 Color: 112

Bin 557: 3 of cap free
Amount of items: 3
Items: 
Size: 394399 Color: 1600
Size: 324256 Color: 1042
Size: 281343 Color: 593

Bin 558: 3 of cap free
Amount of items: 3
Items: 
Size: 350730 Color: 1281
Size: 346221 Color: 1240
Size: 303047 Color: 854

Bin 559: 3 of cap free
Amount of items: 3
Items: 
Size: 401594 Color: 1644
Size: 322644 Color: 1025
Size: 275760 Color: 533

Bin 560: 3 of cap free
Amount of items: 3
Items: 
Size: 395426 Color: 1606
Size: 351200 Color: 1291
Size: 253372 Color: 116

Bin 561: 3 of cap free
Amount of items: 3
Items: 
Size: 399819 Color: 1640
Size: 345406 Color: 1232
Size: 254773 Color: 152

Bin 562: 3 of cap free
Amount of items: 3
Items: 
Size: 376428 Color: 1489
Size: 333279 Color: 1129
Size: 290291 Color: 715

Bin 563: 3 of cap free
Amount of items: 3
Items: 
Size: 437696 Color: 1780
Size: 283791 Color: 636
Size: 278511 Color: 566

Bin 564: 3 of cap free
Amount of items: 3
Items: 
Size: 382011 Color: 1531
Size: 345870 Color: 1238
Size: 272117 Color: 485

Bin 565: 3 of cap free
Amount of items: 3
Items: 
Size: 348521 Color: 1262
Size: 337758 Color: 1171
Size: 313719 Color: 951

Bin 566: 3 of cap free
Amount of items: 3
Items: 
Size: 370273 Color: 1433
Size: 315683 Color: 969
Size: 314042 Color: 954

Bin 567: 3 of cap free
Amount of items: 3
Items: 
Size: 397433 Color: 1623
Size: 335867 Color: 1154
Size: 266698 Color: 395

Bin 568: 3 of cap free
Amount of items: 3
Items: 
Size: 375489 Color: 1480
Size: 324852 Color: 1048
Size: 299657 Color: 832

Bin 569: 3 of cap free
Amount of items: 3
Items: 
Size: 377070 Color: 1496
Size: 341014 Color: 1205
Size: 281914 Color: 606

Bin 570: 3 of cap free
Amount of items: 3
Items: 
Size: 395113 Color: 1604
Size: 303448 Color: 858
Size: 301437 Color: 842

Bin 571: 3 of cap free
Amount of items: 3
Items: 
Size: 375265 Color: 1479
Size: 322329 Color: 1020
Size: 302404 Color: 847

Bin 572: 3 of cap free
Amount of items: 3
Items: 
Size: 421096 Color: 1728
Size: 318898 Color: 995
Size: 260004 Color: 284

Bin 573: 3 of cap free
Amount of items: 3
Items: 
Size: 374299 Color: 1468
Size: 335091 Color: 1148
Size: 290608 Color: 718

Bin 574: 3 of cap free
Amount of items: 3
Items: 
Size: 367646 Color: 1412
Size: 362134 Color: 1372
Size: 270218 Color: 458

Bin 575: 3 of cap free
Amount of items: 3
Items: 
Size: 379381 Color: 1512
Size: 315090 Color: 960
Size: 305527 Color: 878

Bin 576: 3 of cap free
Amount of items: 3
Items: 
Size: 397140 Color: 1620
Size: 323538 Color: 1034
Size: 279320 Color: 573

Bin 577: 3 of cap free
Amount of items: 3
Items: 
Size: 360941 Color: 1366
Size: 332559 Color: 1120
Size: 306498 Color: 885

Bin 578: 3 of cap free
Amount of items: 3
Items: 
Size: 350488 Color: 1279
Size: 329229 Color: 1087
Size: 320281 Color: 1003

Bin 579: 4 of cap free
Amount of items: 3
Items: 
Size: 420869 Color: 1724
Size: 312938 Color: 942
Size: 266190 Color: 386

Bin 580: 4 of cap free
Amount of items: 3
Items: 
Size: 400451 Color: 1642
Size: 344321 Color: 1225
Size: 255225 Color: 166

Bin 581: 5 of cap free
Amount of items: 3
Items: 
Size: 377076 Color: 1497
Size: 372900 Color: 1456
Size: 250020 Color: 1

Bin 582: 5 of cap free
Amount of items: 3
Items: 
Size: 371115 Color: 1438
Size: 338774 Color: 1184
Size: 290107 Color: 712

Bin 583: 5 of cap free
Amount of items: 3
Items: 
Size: 371571 Color: 1444
Size: 321903 Color: 1017
Size: 306522 Color: 886

Bin 584: 5 of cap free
Amount of items: 3
Items: 
Size: 372951 Color: 1458
Size: 334416 Color: 1141
Size: 292629 Color: 740

Bin 585: 6 of cap free
Amount of items: 3
Items: 
Size: 395993 Color: 1611
Size: 314886 Color: 957
Size: 289116 Color: 701

Bin 586: 6 of cap free
Amount of items: 3
Items: 
Size: 404527 Color: 1660
Size: 338232 Color: 1181
Size: 257236 Color: 216

Bin 587: 6 of cap free
Amount of items: 3
Items: 
Size: 390331 Color: 1578
Size: 358270 Color: 1339
Size: 251394 Color: 51

Bin 588: 6 of cap free
Amount of items: 3
Items: 
Size: 395056 Color: 1603
Size: 330118 Color: 1096
Size: 274821 Color: 525

Bin 589: 7 of cap free
Amount of items: 3
Items: 
Size: 380154 Color: 1519
Size: 327174 Color: 1071
Size: 292666 Color: 742

Bin 590: 7 of cap free
Amount of items: 3
Items: 
Size: 378146 Color: 1504
Size: 325605 Color: 1060
Size: 296243 Color: 793

Bin 591: 7 of cap free
Amount of items: 3
Items: 
Size: 374274 Color: 1467
Size: 354104 Color: 1313
Size: 271616 Color: 479

Bin 592: 7 of cap free
Amount of items: 3
Items: 
Size: 350252 Color: 1277
Size: 325816 Color: 1063
Size: 323926 Color: 1038

Bin 593: 7 of cap free
Amount of items: 3
Items: 
Size: 442960 Color: 1801
Size: 291194 Color: 722
Size: 265840 Color: 375

Bin 594: 7 of cap free
Amount of items: 3
Items: 
Size: 391922 Color: 1588
Size: 346451 Color: 1244
Size: 261621 Color: 309

Bin 595: 7 of cap free
Amount of items: 3
Items: 
Size: 348793 Color: 1265
Size: 328476 Color: 1080
Size: 322725 Color: 1026

Bin 596: 8 of cap free
Amount of items: 3
Items: 
Size: 384886 Color: 1545
Size: 338786 Color: 1185
Size: 276321 Color: 535

Bin 597: 8 of cap free
Amount of items: 3
Items: 
Size: 383730 Color: 1541
Size: 338927 Color: 1187
Size: 277336 Color: 549

Bin 598: 8 of cap free
Amount of items: 3
Items: 
Size: 363574 Color: 1382
Size: 336753 Color: 1161
Size: 299666 Color: 833

Bin 599: 8 of cap free
Amount of items: 3
Items: 
Size: 378087 Color: 1503
Size: 327196 Color: 1072
Size: 294710 Color: 768

Bin 600: 8 of cap free
Amount of items: 3
Items: 
Size: 384872 Color: 1544
Size: 361335 Color: 1368
Size: 253786 Color: 126

Bin 601: 8 of cap free
Amount of items: 3
Items: 
Size: 372856 Color: 1455
Size: 357314 Color: 1331
Size: 269823 Color: 450

Bin 602: 8 of cap free
Amount of items: 3
Items: 
Size: 350931 Color: 1287
Size: 332487 Color: 1118
Size: 316575 Color: 977

Bin 603: 9 of cap free
Amount of items: 3
Items: 
Size: 402037 Color: 1648
Size: 334569 Color: 1144
Size: 263386 Color: 339

Bin 604: 9 of cap free
Amount of items: 3
Items: 
Size: 392851 Color: 1591
Size: 347542 Color: 1255
Size: 259599 Color: 274

Bin 605: 9 of cap free
Amount of items: 3
Items: 
Size: 382347 Color: 1534
Size: 331248 Color: 1108
Size: 286397 Color: 664

Bin 606: 10 of cap free
Amount of items: 3
Items: 
Size: 394388 Color: 1599
Size: 339125 Color: 1190
Size: 266478 Color: 391

Bin 607: 10 of cap free
Amount of items: 3
Items: 
Size: 405795 Color: 1664
Size: 309536 Color: 916
Size: 284660 Color: 641

Bin 608: 10 of cap free
Amount of items: 3
Items: 
Size: 366015 Color: 1397
Size: 326175 Color: 1066
Size: 307801 Color: 903

Bin 609: 10 of cap free
Amount of items: 3
Items: 
Size: 352982 Color: 1304
Size: 347620 Color: 1256
Size: 299389 Color: 829

Bin 610: 11 of cap free
Amount of items: 3
Items: 
Size: 397168 Color: 1621
Size: 315258 Color: 963
Size: 287564 Color: 681

Bin 611: 11 of cap free
Amount of items: 3
Items: 
Size: 373474 Color: 1461
Size: 315528 Color: 968
Size: 310988 Color: 926

Bin 612: 11 of cap free
Amount of items: 3
Items: 
Size: 347241 Color: 1251
Size: 337896 Color: 1175
Size: 314853 Color: 956

Bin 613: 12 of cap free
Amount of items: 3
Items: 
Size: 385270 Color: 1547
Size: 318046 Color: 987
Size: 296673 Color: 795

Bin 614: 12 of cap free
Amount of items: 3
Items: 
Size: 404763 Color: 1662
Size: 324643 Color: 1045
Size: 270583 Color: 466

Bin 615: 12 of cap free
Amount of items: 3
Items: 
Size: 358164 Color: 1336
Size: 349132 Color: 1267
Size: 292693 Color: 743

Bin 616: 12 of cap free
Amount of items: 3
Items: 
Size: 380079 Color: 1518
Size: 333334 Color: 1130
Size: 286576 Color: 669

Bin 617: 13 of cap free
Amount of items: 3
Items: 
Size: 353690 Color: 1309
Size: 337294 Color: 1166
Size: 309004 Color: 912

Bin 618: 13 of cap free
Amount of items: 3
Items: 
Size: 385719 Color: 1550
Size: 307736 Color: 902
Size: 306533 Color: 887

Bin 619: 14 of cap free
Amount of items: 3
Items: 
Size: 397920 Color: 1626
Size: 343548 Color: 1216
Size: 258519 Color: 243

Bin 620: 14 of cap free
Amount of items: 3
Items: 
Size: 376856 Color: 1494
Size: 318219 Color: 988
Size: 304912 Color: 872

Bin 621: 16 of cap free
Amount of items: 3
Items: 
Size: 371668 Color: 1446
Size: 317049 Color: 981
Size: 311268 Color: 927

Bin 622: 17 of cap free
Amount of items: 3
Items: 
Size: 360834 Color: 1365
Size: 350762 Color: 1282
Size: 288388 Color: 693

Bin 623: 19 of cap free
Amount of items: 3
Items: 
Size: 440075 Color: 1789
Size: 281577 Color: 600
Size: 278330 Color: 563

Bin 624: 19 of cap free
Amount of items: 3
Items: 
Size: 356249 Color: 1327
Size: 352385 Color: 1301
Size: 291348 Color: 727

Bin 625: 20 of cap free
Amount of items: 3
Items: 
Size: 374798 Color: 1475
Size: 313605 Color: 949
Size: 311578 Color: 930

Bin 626: 22 of cap free
Amount of items: 3
Items: 
Size: 367925 Color: 1415
Size: 320123 Color: 1001
Size: 311931 Color: 933

Bin 627: 22 of cap free
Amount of items: 3
Items: 
Size: 367006 Color: 1407
Size: 323328 Color: 1033
Size: 309645 Color: 917

Bin 628: 22 of cap free
Amount of items: 3
Items: 
Size: 387499 Color: 1566
Size: 318752 Color: 991
Size: 293728 Color: 752

Bin 629: 30 of cap free
Amount of items: 3
Items: 
Size: 428258 Color: 1754
Size: 302631 Color: 850
Size: 269082 Color: 438

Bin 630: 34 of cap free
Amount of items: 3
Items: 
Size: 374663 Color: 1472
Size: 318842 Color: 992
Size: 306462 Color: 883

Bin 631: 42 of cap free
Amount of items: 3
Items: 
Size: 374522 Color: 1470
Size: 328285 Color: 1079
Size: 297152 Color: 800

Bin 632: 42 of cap free
Amount of items: 3
Items: 
Size: 395430 Color: 1607
Size: 337796 Color: 1174
Size: 266733 Color: 398

Bin 633: 48 of cap free
Amount of items: 3
Items: 
Size: 366929 Color: 1405
Size: 321171 Color: 1010
Size: 311853 Color: 932

Bin 634: 55 of cap free
Amount of items: 3
Items: 
Size: 362082 Color: 1371
Size: 336735 Color: 1160
Size: 301129 Color: 840

Bin 635: 56 of cap free
Amount of items: 3
Items: 
Size: 363836 Color: 1386
Size: 329021 Color: 1085
Size: 307088 Color: 893

Bin 636: 57 of cap free
Amount of items: 3
Items: 
Size: 365359 Color: 1394
Size: 325279 Color: 1054
Size: 309306 Color: 914

Bin 637: 58 of cap free
Amount of items: 3
Items: 
Size: 396845 Color: 1619
Size: 340316 Color: 1198
Size: 262782 Color: 326

Bin 638: 61 of cap free
Amount of items: 3
Items: 
Size: 364557 Color: 1390
Size: 321623 Color: 1014
Size: 313760 Color: 952

Bin 639: 65 of cap free
Amount of items: 3
Items: 
Size: 387388 Color: 1563
Size: 323168 Color: 1032
Size: 289380 Color: 705

Bin 640: 68 of cap free
Amount of items: 3
Items: 
Size: 344261 Color: 1223
Size: 344026 Color: 1221
Size: 311646 Color: 931

Bin 641: 70 of cap free
Amount of items: 3
Items: 
Size: 392875 Color: 1592
Size: 321779 Color: 1015
Size: 285277 Color: 649

Bin 642: 73 of cap free
Amount of items: 3
Items: 
Size: 366854 Color: 1404
Size: 320528 Color: 1008
Size: 312546 Color: 939

Bin 643: 76 of cap free
Amount of items: 3
Items: 
Size: 382581 Color: 1535
Size: 361559 Color: 1369
Size: 255785 Color: 187

Bin 644: 83 of cap free
Amount of items: 3
Items: 
Size: 355913 Color: 1323
Size: 331018 Color: 1104
Size: 312987 Color: 943

Bin 645: 84 of cap free
Amount of items: 3
Items: 
Size: 346574 Color: 1246
Size: 346340 Color: 1241
Size: 307003 Color: 891

Bin 646: 87 of cap free
Amount of items: 3
Items: 
Size: 359311 Color: 1352
Size: 346477 Color: 1245
Size: 294126 Color: 757

Bin 647: 90 of cap free
Amount of items: 3
Items: 
Size: 336881 Color: 1163
Size: 334317 Color: 1140
Size: 328713 Color: 1082

Bin 648: 93 of cap free
Amount of items: 3
Items: 
Size: 376394 Color: 1487
Size: 336586 Color: 1158
Size: 286928 Color: 674

Bin 649: 105 of cap free
Amount of items: 3
Items: 
Size: 371870 Color: 1448
Size: 317519 Color: 983
Size: 310507 Color: 923

Bin 650: 106 of cap free
Amount of items: 3
Items: 
Size: 391587 Color: 1584
Size: 320312 Color: 1006
Size: 287996 Color: 687

Bin 651: 128 of cap free
Amount of items: 3
Items: 
Size: 371493 Color: 1442
Size: 354524 Color: 1314
Size: 273856 Color: 514

Bin 652: 188 of cap free
Amount of items: 3
Items: 
Size: 380932 Color: 1526
Size: 368333 Color: 1420
Size: 250548 Color: 22

Bin 653: 190 of cap free
Amount of items: 3
Items: 
Size: 387527 Color: 1567
Size: 307602 Color: 899
Size: 304682 Color: 870

Bin 654: 202 of cap free
Amount of items: 3
Items: 
Size: 337772 Color: 1173
Size: 333106 Color: 1126
Size: 328921 Color: 1084

Bin 655: 222 of cap free
Amount of items: 3
Items: 
Size: 391338 Color: 1583
Size: 351260 Color: 1293
Size: 257181 Color: 213

Bin 656: 230 of cap free
Amount of items: 3
Items: 
Size: 367866 Color: 1414
Size: 323624 Color: 1035
Size: 308281 Color: 906

Bin 657: 256 of cap free
Amount of items: 3
Items: 
Size: 350924 Color: 1286
Size: 341851 Color: 1208
Size: 306970 Color: 890

Bin 658: 293 of cap free
Amount of items: 3
Items: 
Size: 343111 Color: 1215
Size: 328835 Color: 1083
Size: 327762 Color: 1075

Bin 659: 309 of cap free
Amount of items: 3
Items: 
Size: 393888 Color: 1597
Size: 353540 Color: 1308
Size: 252264 Color: 75

Bin 660: 313 of cap free
Amount of items: 3
Items: 
Size: 381944 Color: 1530
Size: 330417 Color: 1101
Size: 287327 Color: 677

Bin 661: 392 of cap free
Amount of items: 3
Items: 
Size: 371428 Color: 1441
Size: 330403 Color: 1100
Size: 297778 Color: 805

Bin 662: 916 of cap free
Amount of items: 3
Items: 
Size: 365348 Color: 1393
Size: 338098 Color: 1178
Size: 295639 Color: 786

Bin 663: 1155 of cap free
Amount of items: 3
Items: 
Size: 372330 Color: 1452
Size: 322742 Color: 1029
Size: 303774 Color: 861

Bin 664: 1445 of cap free
Amount of items: 3
Items: 
Size: 369131 Color: 1427
Size: 316979 Color: 980
Size: 312446 Color: 937

Bin 665: 1758 of cap free
Amount of items: 3
Items: 
Size: 386323 Color: 1558
Size: 330205 Color: 1097
Size: 281715 Color: 602

Bin 666: 1879 of cap free
Amount of items: 3
Items: 
Size: 345555 Color: 1234
Size: 345299 Color: 1230
Size: 307268 Color: 895

Bin 667: 259949 of cap free
Amount of items: 2
Items: 
Size: 444415 Color: 1802
Size: 295637 Color: 785

Bin 668: 728017 of cap free
Amount of items: 1
Items: 
Size: 271984 Color: 483

Total size: 667000667
Total free space: 1000001

