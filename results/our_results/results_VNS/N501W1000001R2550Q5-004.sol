Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 346334 Color: 1
Size: 332268 Color: 2
Size: 321399 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 374163 Color: 3
Size: 328008 Color: 0
Size: 297830 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 373142 Color: 1
Size: 343399 Color: 3
Size: 283460 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 383834 Color: 3
Size: 364141 Color: 3
Size: 252026 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 389408 Color: 0
Size: 327551 Color: 4
Size: 283042 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 356633 Color: 4
Size: 348801 Color: 3
Size: 294567 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 400910 Color: 2
Size: 345398 Color: 1
Size: 253693 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 455112 Color: 4
Size: 278244 Color: 3
Size: 266645 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 456223 Color: 3
Size: 283405 Color: 3
Size: 260373 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 269833 Color: 4
Size: 417106 Color: 0
Size: 313062 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 477026 Color: 4
Size: 268283 Color: 2
Size: 254692 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 446206 Color: 0
Size: 286303 Color: 2
Size: 267492 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 387626 Color: 1
Size: 312587 Color: 0
Size: 299788 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 474593 Color: 1
Size: 275345 Color: 0
Size: 250063 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 463511 Color: 0
Size: 284199 Color: 2
Size: 252291 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 396482 Color: 0
Size: 353168 Color: 1
Size: 250351 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 378565 Color: 4
Size: 339297 Color: 3
Size: 282139 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 387498 Color: 0
Size: 329899 Color: 4
Size: 282604 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 378273 Color: 4
Size: 323865 Color: 0
Size: 297863 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 472865 Color: 3
Size: 273110 Color: 1
Size: 254026 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 391626 Color: 2
Size: 308604 Color: 3
Size: 299771 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 455244 Color: 0
Size: 288323 Color: 1
Size: 256434 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 388169 Color: 0
Size: 306473 Color: 4
Size: 305359 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 426121 Color: 3
Size: 307278 Color: 1
Size: 266602 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 374049 Color: 3
Size: 350959 Color: 0
Size: 274993 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 433809 Color: 3
Size: 305255 Color: 3
Size: 260937 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 471325 Color: 0
Size: 265915 Color: 2
Size: 262761 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 348011 Color: 4
Size: 330302 Color: 0
Size: 321688 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 463091 Color: 4
Size: 283558 Color: 3
Size: 253352 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 353385 Color: 2
Size: 327071 Color: 3
Size: 319545 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 447686 Color: 1
Size: 286329 Color: 3
Size: 265986 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 375379 Color: 4
Size: 367521 Color: 1
Size: 257101 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 342795 Color: 3
Size: 332215 Color: 0
Size: 324991 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 428346 Color: 3
Size: 309952 Color: 4
Size: 261703 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 436166 Color: 1
Size: 289088 Color: 0
Size: 274747 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 466008 Color: 0
Size: 271208 Color: 1
Size: 262785 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 375489 Color: 1
Size: 353283 Color: 4
Size: 271229 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 354192 Color: 0
Size: 334941 Color: 1
Size: 310868 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 391237 Color: 3
Size: 336221 Color: 4
Size: 272543 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 487916 Color: 0
Size: 261866 Color: 4
Size: 250219 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 365276 Color: 1
Size: 347413 Color: 4
Size: 287312 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 429521 Color: 4
Size: 320368 Color: 0
Size: 250112 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 465761 Color: 2
Size: 281416 Color: 4
Size: 252824 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 434556 Color: 1
Size: 309217 Color: 0
Size: 256228 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 402946 Color: 0
Size: 303469 Color: 1
Size: 293586 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 484198 Color: 4
Size: 265424 Color: 1
Size: 250379 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 369472 Color: 0
Size: 350664 Color: 0
Size: 279865 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 414205 Color: 1
Size: 300024 Color: 0
Size: 285772 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 429680 Color: 3
Size: 295423 Color: 1
Size: 274898 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 475277 Color: 2
Size: 266068 Color: 3
Size: 258656 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 426063 Color: 0
Size: 297949 Color: 2
Size: 275989 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 413316 Color: 0
Size: 301749 Color: 4
Size: 284936 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 446760 Color: 0
Size: 297519 Color: 1
Size: 255722 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 386522 Color: 0
Size: 317757 Color: 1
Size: 295722 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 348863 Color: 0
Size: 325706 Color: 3
Size: 325432 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 411773 Color: 4
Size: 319908 Color: 3
Size: 268320 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 461969 Color: 0
Size: 287749 Color: 4
Size: 250283 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 432331 Color: 4
Size: 289097 Color: 1
Size: 278573 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 476281 Color: 3
Size: 266739 Color: 1
Size: 256981 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 461417 Color: 2
Size: 273259 Color: 2
Size: 265325 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 486228 Color: 2
Size: 259986 Color: 1
Size: 253787 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 412693 Color: 1
Size: 304531 Color: 3
Size: 282777 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 424770 Color: 4
Size: 313609 Color: 2
Size: 261622 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 481638 Color: 3
Size: 267197 Color: 0
Size: 251166 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 438285 Color: 4
Size: 288919 Color: 1
Size: 272797 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 391402 Color: 2
Size: 313661 Color: 0
Size: 294938 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 380668 Color: 4
Size: 311996 Color: 0
Size: 307337 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 343974 Color: 3
Size: 340421 Color: 1
Size: 315606 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 396157 Color: 2
Size: 330646 Color: 0
Size: 273198 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 360500 Color: 4
Size: 338484 Color: 0
Size: 301017 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 384279 Color: 1
Size: 317245 Color: 2
Size: 298477 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 364778 Color: 3
Size: 349193 Color: 1
Size: 286030 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 379088 Color: 4
Size: 336596 Color: 3
Size: 284317 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 499336 Color: 1
Size: 250404 Color: 1
Size: 250261 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 487794 Color: 0
Size: 261704 Color: 2
Size: 250503 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 402728 Color: 4
Size: 300851 Color: 4
Size: 296422 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 419271 Color: 1
Size: 293927 Color: 3
Size: 286803 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 462625 Color: 1
Size: 271964 Color: 0
Size: 265412 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 441579 Color: 4
Size: 284835 Color: 0
Size: 273587 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 368211 Color: 1
Size: 333593 Color: 1
Size: 298197 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 364911 Color: 3
Size: 349059 Color: 4
Size: 286031 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 434578 Color: 2
Size: 312188 Color: 2
Size: 253235 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 356777 Color: 3
Size: 350034 Color: 2
Size: 293190 Color: 3

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 396183 Color: 3
Size: 302339 Color: 4
Size: 301479 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 435953 Color: 1
Size: 293682 Color: 3
Size: 270366 Color: 2

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 491486 Color: 0
Size: 256774 Color: 3
Size: 251741 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 366874 Color: 4
Size: 355926 Color: 4
Size: 277201 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 449984 Color: 4
Size: 291284 Color: 2
Size: 258733 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 411137 Color: 1
Size: 335252 Color: 2
Size: 253612 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 354701 Color: 3
Size: 343925 Color: 1
Size: 301375 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 455981 Color: 3
Size: 273805 Color: 3
Size: 270215 Color: 2

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 415965 Color: 4
Size: 292336 Color: 0
Size: 291700 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 347215 Color: 4
Size: 329575 Color: 1
Size: 323211 Color: 4

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 390177 Color: 1
Size: 311368 Color: 1
Size: 298456 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 402932 Color: 0
Size: 338059 Color: 3
Size: 259010 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 371127 Color: 3
Size: 334550 Color: 1
Size: 294324 Color: 2

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 409501 Color: 4
Size: 331398 Color: 0
Size: 259102 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 382296 Color: 0
Size: 359958 Color: 3
Size: 257747 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 389694 Color: 2
Size: 344652 Color: 1
Size: 265655 Color: 4

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 430423 Color: 3
Size: 294994 Color: 2
Size: 274584 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 495760 Color: 4
Size: 254100 Color: 3
Size: 250141 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 477577 Color: 2
Size: 271204 Color: 0
Size: 251220 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 478718 Color: 3
Size: 261703 Color: 4
Size: 259580 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 488539 Color: 2
Size: 258857 Color: 0
Size: 252605 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 470785 Color: 4
Size: 266064 Color: 3
Size: 263152 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 375041 Color: 2
Size: 323616 Color: 4
Size: 301344 Color: 2

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 406731 Color: 2
Size: 309795 Color: 3
Size: 283475 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 462496 Color: 4
Size: 280128 Color: 1
Size: 257377 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 388285 Color: 0
Size: 340355 Color: 1
Size: 271361 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 461893 Color: 1
Size: 281841 Color: 0
Size: 256267 Color: 4

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 440848 Color: 4
Size: 304256 Color: 1
Size: 254897 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 389371 Color: 2
Size: 319298 Color: 4
Size: 291332 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 457856 Color: 2
Size: 287504 Color: 0
Size: 254641 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 372873 Color: 0
Size: 360451 Color: 1
Size: 266677 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 412225 Color: 3
Size: 296021 Color: 1
Size: 291755 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 397600 Color: 1
Size: 349337 Color: 1
Size: 253064 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 463742 Color: 0
Size: 284550 Color: 2
Size: 251709 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 376745 Color: 4
Size: 371842 Color: 4
Size: 251414 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 482252 Color: 2
Size: 264599 Color: 1
Size: 253150 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 427378 Color: 1
Size: 309030 Color: 0
Size: 263593 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 348335 Color: 4
Size: 333366 Color: 0
Size: 318300 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 481248 Color: 4
Size: 267055 Color: 2
Size: 251698 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 362138 Color: 3
Size: 326287 Color: 4
Size: 311576 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 479959 Color: 0
Size: 264569 Color: 2
Size: 255473 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 397072 Color: 3
Size: 319589 Color: 3
Size: 283340 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 490316 Color: 4
Size: 255025 Color: 0
Size: 254660 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 405082 Color: 1
Size: 339876 Color: 0
Size: 255043 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 379530 Color: 2
Size: 347220 Color: 4
Size: 273251 Color: 3

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 401800 Color: 0
Size: 306248 Color: 1
Size: 291953 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 362889 Color: 4
Size: 327285 Color: 4
Size: 309827 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 356028 Color: 2
Size: 332273 Color: 4
Size: 311700 Color: 2

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 463014 Color: 0
Size: 272657 Color: 4
Size: 264330 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 360001 Color: 2
Size: 329081 Color: 4
Size: 310919 Color: 2

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 450217 Color: 4
Size: 287630 Color: 1
Size: 262154 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 382002 Color: 1
Size: 357995 Color: 4
Size: 260004 Color: 3

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 364119 Color: 2
Size: 351305 Color: 4
Size: 284577 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 395619 Color: 3
Size: 325145 Color: 0
Size: 279237 Color: 4

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 497587 Color: 4
Size: 251656 Color: 0
Size: 250758 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 390076 Color: 3
Size: 315881 Color: 0
Size: 294044 Color: 3

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 412966 Color: 2
Size: 322565 Color: 2
Size: 264470 Color: 1

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 420764 Color: 1
Size: 323433 Color: 0
Size: 255804 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 497412 Color: 2
Size: 252139 Color: 0
Size: 250450 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 465722 Color: 0
Size: 272388 Color: 4
Size: 261891 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 440516 Color: 2
Size: 285925 Color: 0
Size: 273560 Color: 3

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 467407 Color: 1
Size: 278004 Color: 2
Size: 254590 Color: 3

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 491341 Color: 1
Size: 257976 Color: 3
Size: 250684 Color: 3

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 398477 Color: 4
Size: 305277 Color: 1
Size: 296247 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 422249 Color: 3
Size: 316698 Color: 1
Size: 261054 Color: 3

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 426001 Color: 2
Size: 297803 Color: 0
Size: 276197 Color: 1

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 351276 Color: 0
Size: 341275 Color: 1
Size: 307450 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 463741 Color: 0
Size: 270137 Color: 1
Size: 266123 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 372022 Color: 3
Size: 360095 Color: 3
Size: 267884 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 403701 Color: 1
Size: 320194 Color: 4
Size: 276106 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 391500 Color: 4
Size: 326292 Color: 1
Size: 282209 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 354799 Color: 3
Size: 353451 Color: 4
Size: 291751 Color: 4

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 436710 Color: 4
Size: 281838 Color: 1
Size: 281453 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 475384 Color: 0
Size: 268563 Color: 3
Size: 256054 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 418871 Color: 2
Size: 299719 Color: 0
Size: 281411 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 470152 Color: 1
Size: 270930 Color: 1
Size: 258919 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 342299 Color: 4
Size: 337730 Color: 0
Size: 319972 Color: 4

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 356450 Color: 1
Size: 325738 Color: 2
Size: 317813 Color: 3

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 401593 Color: 4
Size: 334612 Color: 4
Size: 263796 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 475944 Color: 2
Size: 264790 Color: 0
Size: 259267 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 396598 Color: 1
Size: 319921 Color: 4
Size: 283482 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 489811 Color: 2
Size: 258146 Color: 3
Size: 252044 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 379182 Color: 0
Size: 312271 Color: 3
Size: 308548 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 453631 Color: 2
Size: 286254 Color: 4
Size: 260116 Color: 2

Total size: 167000167
Total free space: 0

