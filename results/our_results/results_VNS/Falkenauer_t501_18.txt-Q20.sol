Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 5
Size: 268 Color: 18
Size: 260 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 4
Size: 268 Color: 14
Size: 251 Color: 16

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 3
Size: 273 Color: 1
Size: 251 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 6
Size: 257 Color: 13
Size: 250 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8
Size: 294 Color: 5
Size: 281 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 11
Size: 260 Color: 0
Size: 255 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 9
Size: 355 Color: 14
Size: 277 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 15
Size: 345 Color: 2
Size: 257 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 12
Size: 351 Color: 18
Size: 290 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 13
Size: 277 Color: 12
Size: 260 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 323 Color: 1
Size: 294 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 300 Color: 17
Size: 265 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 11
Size: 251 Color: 18
Size: 250 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 10
Size: 325 Color: 9
Size: 252 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 5
Size: 271 Color: 5
Size: 261 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 9
Size: 312 Color: 19
Size: 263 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 14
Size: 295 Color: 17
Size: 293 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 11
Size: 288 Color: 17
Size: 258 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 6
Size: 278 Color: 9
Size: 250 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 2
Size: 330 Color: 3
Size: 317 Color: 9

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 7
Size: 303 Color: 6
Size: 293 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 11
Size: 318 Color: 7
Size: 287 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 7
Size: 362 Color: 15
Size: 259 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 16
Size: 367 Color: 15
Size: 261 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 7
Size: 301 Color: 11
Size: 297 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 7
Size: 283 Color: 19
Size: 263 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 13
Size: 269 Color: 6
Size: 252 Color: 17

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 298 Color: 3
Size: 271 Color: 19

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 15
Size: 373 Color: 8
Size: 252 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 7
Size: 323 Color: 9
Size: 300 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 4
Size: 306 Color: 3
Size: 266 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 10
Size: 306 Color: 5
Size: 250 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 331 Color: 6
Size: 258 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 19
Size: 332 Color: 17
Size: 299 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 9
Size: 286 Color: 7
Size: 252 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 3
Size: 305 Color: 12
Size: 293 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 252 Color: 2
Size: 251 Color: 5

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 283 Color: 16
Size: 257 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 17
Size: 283 Color: 7
Size: 277 Color: 8

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 346 Color: 5
Size: 285 Color: 9

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 7
Size: 294 Color: 1
Size: 255 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 315 Color: 6
Size: 262 Color: 15

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 289 Color: 3
Size: 266 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 7
Size: 274 Color: 6
Size: 259 Color: 14

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 19
Size: 357 Color: 17
Size: 285 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 8
Size: 313 Color: 14
Size: 287 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 15
Size: 266 Color: 6
Size: 258 Color: 19

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 285 Color: 6
Size: 253 Color: 12

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 4
Size: 327 Color: 19
Size: 271 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 9
Size: 344 Color: 15
Size: 294 Color: 18

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 8
Size: 342 Color: 12
Size: 263 Color: 8

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 19
Size: 269 Color: 8
Size: 254 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 0
Size: 316 Color: 4
Size: 271 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 9
Size: 272 Color: 2
Size: 264 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 312 Color: 15
Size: 283 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 293 Color: 19
Size: 288 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 365 Color: 17
Size: 259 Color: 14

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 2
Size: 340 Color: 5
Size: 252 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 13
Size: 287 Color: 12
Size: 252 Color: 8

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 12
Size: 288 Color: 13
Size: 284 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 279 Color: 18
Size: 263 Color: 14

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 14
Size: 292 Color: 19
Size: 260 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 12
Size: 288 Color: 4
Size: 265 Color: 17

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 292 Color: 12
Size: 254 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 5
Size: 334 Color: 3
Size: 313 Color: 2

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 13
Size: 289 Color: 14
Size: 273 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 15
Size: 305 Color: 12
Size: 271 Color: 17

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 15
Size: 316 Color: 12
Size: 261 Color: 11

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 327 Color: 4
Size: 302 Color: 5

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 2
Size: 250 Color: 10

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 278 Color: 10
Size: 262 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 5
Size: 271 Color: 18
Size: 267 Color: 6

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 368 Color: 13
Size: 263 Color: 10

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 15
Size: 342 Color: 4
Size: 298 Color: 5

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 305 Color: 3
Size: 257 Color: 18

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 5
Size: 269 Color: 3
Size: 267 Color: 14

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 15
Size: 298 Color: 15
Size: 264 Color: 17

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 4
Size: 262 Color: 7
Size: 255 Color: 16

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 16
Size: 292 Color: 2
Size: 266 Color: 18

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 16
Size: 297 Color: 0
Size: 286 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 19
Size: 352 Color: 10
Size: 277 Color: 10

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 270 Color: 10
Size: 259 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 9
Size: 291 Color: 1
Size: 265 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 6
Size: 300 Color: 0
Size: 277 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 6
Size: 262 Color: 17
Size: 250 Color: 13

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 3
Size: 270 Color: 2
Size: 268 Color: 12

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 11
Size: 315 Color: 5
Size: 252 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 16
Size: 348 Color: 8
Size: 286 Color: 5

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 19
Size: 252 Color: 13
Size: 250 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 14
Size: 327 Color: 0
Size: 316 Color: 15

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 15
Size: 335 Color: 1
Size: 293 Color: 15

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 15
Size: 357 Color: 3
Size: 270 Color: 8

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 3
Size: 282 Color: 1
Size: 257 Color: 7

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 10
Size: 275 Color: 1
Size: 261 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 255 Color: 3
Size: 254 Color: 19

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 17
Size: 301 Color: 3
Size: 250 Color: 16

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 13
Size: 269 Color: 11
Size: 264 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 5
Size: 274 Color: 14
Size: 266 Color: 9

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 6
Size: 307 Color: 5
Size: 357 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 15
Size: 321 Color: 9
Size: 250 Color: 15

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 8
Size: 282 Color: 8
Size: 259 Color: 10

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 11
Size: 256 Color: 16
Size: 250 Color: 18

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 17
Size: 362 Color: 4
Size: 271 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 11
Size: 357 Color: 12
Size: 259 Color: 7

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 274 Color: 9
Size: 260 Color: 19

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 326 Color: 2
Size: 262 Color: 7

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 283 Color: 0
Size: 272 Color: 13

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 18
Size: 282 Color: 5
Size: 265 Color: 6

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 339 Color: 3
Size: 273 Color: 9

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 2
Size: 338 Color: 7
Size: 317 Color: 5

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 0
Size: 291 Color: 16
Size: 278 Color: 12

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 16
Size: 263 Color: 14
Size: 255 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 5
Size: 341 Color: 5
Size: 301 Color: 18

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 278 Color: 3
Size: 263 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 362 Color: 9
Size: 258 Color: 3

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 19
Size: 334 Color: 10
Size: 284 Color: 18

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 18
Size: 308 Color: 17
Size: 251 Color: 2

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 302 Color: 14
Size: 264 Color: 3

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 299 Color: 4
Size: 262 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 266 Color: 1
Size: 254 Color: 16

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 274 Color: 8
Size: 274 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 7
Size: 345 Color: 9
Size: 250 Color: 11

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 12
Size: 305 Color: 7
Size: 251 Color: 19

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 9
Size: 279 Color: 19
Size: 275 Color: 14

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 14
Size: 280 Color: 9
Size: 258 Color: 7

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 16
Size: 293 Color: 12
Size: 285 Color: 16

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 8
Size: 373 Color: 2
Size: 250 Color: 7

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 10
Size: 317 Color: 13
Size: 257 Color: 11

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 363 Color: 13
Size: 262 Color: 12

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 274 Color: 2
Size: 271 Color: 9

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 7
Size: 302 Color: 10
Size: 278 Color: 12

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 9
Size: 308 Color: 8
Size: 253 Color: 8

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 251 Color: 13
Size: 250 Color: 11

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 308 Color: 1
Size: 306 Color: 18

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 9
Size: 296 Color: 12
Size: 271 Color: 5

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 18
Size: 362 Color: 13
Size: 270 Color: 2

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 9
Size: 311 Color: 1
Size: 251 Color: 16

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 16
Size: 281 Color: 3
Size: 256 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 18
Size: 309 Color: 9
Size: 301 Color: 15

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 13
Size: 252 Color: 5
Size: 250 Color: 16

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 18
Size: 317 Color: 0
Size: 281 Color: 9

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 14
Size: 285 Color: 1
Size: 250 Color: 13

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9
Size: 266 Color: 2
Size: 264 Color: 12

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 309 Color: 19
Size: 277 Color: 17

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 11
Size: 253 Color: 10
Size: 251 Color: 4

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 3
Size: 265 Color: 10
Size: 258 Color: 19

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 13
Size: 294 Color: 8
Size: 283 Color: 8

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 18
Size: 275 Color: 2
Size: 274 Color: 8

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 357 Color: 10
Size: 273 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 19
Size: 362 Color: 11
Size: 259 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 8
Size: 350 Color: 11
Size: 275 Color: 8

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 10
Size: 290 Color: 2
Size: 281 Color: 14

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 9
Size: 251 Color: 3

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 12
Size: 283 Color: 11
Size: 260 Color: 19

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 19
Size: 292 Color: 0
Size: 265 Color: 8

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 13
Size: 330 Color: 12
Size: 301 Color: 12

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 7
Size: 327 Color: 18
Size: 323 Color: 16

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 7
Size: 334 Color: 14
Size: 307 Color: 10

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 336 Color: 9
Size: 283 Color: 14

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 17
Size: 354 Color: 7
Size: 261 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 2
Size: 352 Color: 18
Size: 258 Color: 10

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 17
Size: 304 Color: 13
Size: 284 Color: 18

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 285 Color: 8
Size: 281 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 302 Color: 11
Size: 260 Color: 10

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 284 Color: 19
Size: 275 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 18
Size: 299 Color: 13
Size: 250 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 19
Size: 275 Color: 19
Size: 257 Color: 8

Total size: 167000
Total free space: 0

