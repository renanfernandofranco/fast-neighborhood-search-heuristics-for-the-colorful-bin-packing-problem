Capicity Bin: 1000001
Lower Bound: 896

Bins used: 904
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 729732 Color: 13
Size: 169512 Color: 19
Size: 100757 Color: 14

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 714378 Color: 2
Size: 163776 Color: 8
Size: 121847 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 700270 Color: 1
Size: 194084 Color: 16
Size: 105647 Color: 0

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 513676 Color: 6
Size: 191461 Color: 14
Size: 154955 Color: 13
Size: 139909 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 672793 Color: 14
Size: 190539 Color: 2
Size: 136669 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 657958 Color: 5
Size: 236042 Color: 4
Size: 106001 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 662274 Color: 9
Size: 206233 Color: 9
Size: 131494 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 684667 Color: 6
Size: 169583 Color: 16
Size: 145751 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 762306 Color: 2
Size: 120427 Color: 16
Size: 117268 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 514882 Color: 7
Size: 334926 Color: 0
Size: 150193 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 452142 Color: 0
Size: 263655 Color: 8
Size: 142869 Color: 17
Size: 141335 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 738527 Color: 5
Size: 137881 Color: 19
Size: 123593 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 483491 Color: 3
Size: 323058 Color: 4
Size: 193452 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 574141 Color: 2
Size: 303353 Color: 17
Size: 122507 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 745506 Color: 19
Size: 141986 Color: 10
Size: 112509 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 772040 Color: 5
Size: 125368 Color: 18
Size: 102593 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 731810 Color: 14
Size: 161076 Color: 2
Size: 107115 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 645852 Color: 4
Size: 186936 Color: 3
Size: 167213 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 608145 Color: 0
Size: 273757 Color: 6
Size: 118099 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 691248 Color: 18
Size: 177568 Color: 17
Size: 131185 Color: 8

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 500014 Color: 6
Size: 189877 Color: 8
Size: 166080 Color: 3
Size: 144030 Color: 3

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 700499 Color: 8
Size: 299502 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 649306 Color: 10
Size: 213962 Color: 13
Size: 136733 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 710594 Color: 10
Size: 179666 Color: 2
Size: 109741 Color: 8

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 502110 Color: 5
Size: 320959 Color: 5
Size: 176932 Color: 17

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 702503 Color: 6
Size: 188907 Color: 2
Size: 108591 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 507281 Color: 4
Size: 306200 Color: 0
Size: 186520 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 665491 Color: 14
Size: 204126 Color: 15
Size: 130384 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 655977 Color: 19
Size: 178885 Color: 3
Size: 165139 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 465910 Color: 8
Size: 324691 Color: 17
Size: 209400 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 731681 Color: 11
Size: 145974 Color: 17
Size: 122346 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 740301 Color: 4
Size: 145505 Color: 3
Size: 114195 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 740121 Color: 17
Size: 131913 Color: 14
Size: 127967 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 554652 Color: 3
Size: 234469 Color: 11
Size: 210880 Color: 9

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 470446 Color: 13
Size: 303889 Color: 0
Size: 225666 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 486151 Color: 7
Size: 381713 Color: 8
Size: 132137 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 440986 Color: 17
Size: 422173 Color: 6
Size: 136842 Color: 14

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 430468 Color: 12
Size: 410337 Color: 17
Size: 159196 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 405976 Color: 1
Size: 404184 Color: 14
Size: 189841 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 532075 Color: 15
Size: 294446 Color: 19
Size: 173480 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 560013 Color: 14
Size: 294882 Color: 0
Size: 145106 Color: 19

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 588079 Color: 0
Size: 411922 Color: 6

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 596134 Color: 19
Size: 403867 Color: 12

Bin 44: 0 of cap free
Amount of items: 2
Items: 
Size: 620426 Color: 9
Size: 379575 Color: 16

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 686325 Color: 18
Size: 169138 Color: 8
Size: 144538 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 686522 Color: 18
Size: 159287 Color: 12
Size: 154192 Color: 19

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 718840 Color: 11
Size: 154933 Color: 1
Size: 126228 Color: 1

Bin 48: 0 of cap free
Amount of items: 2
Items: 
Size: 719424 Color: 19
Size: 280577 Color: 14

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 615112 Color: 5
Size: 384888 Color: 7

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 649830 Color: 16
Size: 350170 Color: 14

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 672770 Color: 14
Size: 196888 Color: 14
Size: 130342 Color: 18

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 697153 Color: 0
Size: 165097 Color: 9
Size: 137750 Color: 7

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 750732 Color: 19
Size: 133812 Color: 6
Size: 115456 Color: 16

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 796433 Color: 3
Size: 203567 Color: 2

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 507909 Color: 19
Size: 313828 Color: 2
Size: 178263 Color: 17

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 720860 Color: 10
Size: 174593 Color: 13
Size: 104547 Color: 16

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 784657 Color: 16
Size: 113456 Color: 9
Size: 101887 Color: 10

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 676794 Color: 1
Size: 174977 Color: 10
Size: 148229 Color: 10

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 655082 Color: 2
Size: 189313 Color: 14
Size: 155605 Color: 15

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 486998 Color: 16
Size: 330212 Color: 2
Size: 182790 Color: 8

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 740234 Color: 9
Size: 144185 Color: 4
Size: 115581 Color: 16

Bin 62: 1 of cap free
Amount of items: 4
Items: 
Size: 466402 Color: 5
Size: 261914 Color: 7
Size: 144536 Color: 9
Size: 127148 Color: 18

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 501997 Color: 9
Size: 365042 Color: 19
Size: 132961 Color: 5

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 537724 Color: 6
Size: 294858 Color: 1
Size: 167418 Color: 6

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 747995 Color: 17
Size: 139897 Color: 4
Size: 112108 Color: 0

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 609757 Color: 18
Size: 288522 Color: 4
Size: 101721 Color: 15

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 681794 Color: 18
Size: 202133 Color: 15
Size: 116073 Color: 2

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 740486 Color: 17
Size: 142823 Color: 17
Size: 116691 Color: 11

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 462384 Color: 6
Size: 430436 Color: 7
Size: 107180 Color: 5

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 415155 Color: 13
Size: 414125 Color: 15
Size: 170720 Color: 16

Bin 71: 1 of cap free
Amount of items: 4
Items: 
Size: 424089 Color: 4
Size: 371744 Color: 8
Size: 102963 Color: 9
Size: 101204 Color: 19

Bin 72: 1 of cap free
Amount of items: 4
Items: 
Size: 382029 Color: 10
Size: 371516 Color: 16
Size: 138429 Color: 6
Size: 108026 Color: 4

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 602032 Color: 13
Size: 397967 Color: 5

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 719167 Color: 6
Size: 171125 Color: 3
Size: 109707 Color: 7

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 695360 Color: 7
Size: 185182 Color: 8
Size: 119457 Color: 3

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 567858 Color: 10
Size: 286277 Color: 3
Size: 145864 Color: 17

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 513467 Color: 16
Size: 304572 Color: 14
Size: 181960 Color: 14

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 753475 Color: 1
Size: 142750 Color: 1
Size: 103774 Color: 5

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 649424 Color: 3
Size: 176404 Color: 15
Size: 174171 Color: 10

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 727693 Color: 10
Size: 148710 Color: 0
Size: 123596 Color: 6

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 375465 Color: 6
Size: 371395 Color: 13
Size: 253139 Color: 0

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 365458 Color: 6
Size: 363733 Color: 13
Size: 270808 Color: 18

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 681008 Color: 16
Size: 196408 Color: 18
Size: 122582 Color: 12

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 602496 Color: 4
Size: 276531 Color: 16
Size: 120971 Color: 6

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 772632 Color: 12
Size: 118064 Color: 9
Size: 109302 Color: 17

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 685070 Color: 7
Size: 178704 Color: 9
Size: 136224 Color: 6

Bin 87: 3 of cap free
Amount of items: 4
Items: 
Size: 590255 Color: 17
Size: 157797 Color: 15
Size: 130987 Color: 15
Size: 120959 Color: 16

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 604193 Color: 8
Size: 395805 Color: 14

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 403681 Color: 15
Size: 359169 Color: 3
Size: 237148 Color: 18

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 629168 Color: 3
Size: 370829 Color: 5

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 719384 Color: 15
Size: 280613 Color: 5

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 753196 Color: 5
Size: 129646 Color: 12
Size: 117155 Color: 16

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 716370 Color: 2
Size: 143605 Color: 15
Size: 140022 Color: 18

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 702578 Color: 10
Size: 177006 Color: 2
Size: 120413 Color: 13

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 712482 Color: 1
Size: 166194 Color: 8
Size: 121321 Color: 0

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 470157 Color: 3
Size: 344198 Color: 11
Size: 185641 Color: 3

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 711067 Color: 3
Size: 149609 Color: 10
Size: 139320 Color: 6

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 716672 Color: 15
Size: 158029 Color: 10
Size: 125295 Color: 7

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 758657 Color: 5
Size: 241339 Color: 0

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 678433 Color: 14
Size: 181292 Color: 14
Size: 140271 Color: 12

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 761661 Color: 6
Size: 131390 Color: 10
Size: 106945 Color: 11

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 770692 Color: 16
Size: 122703 Color: 3
Size: 106601 Color: 9

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 755206 Color: 8
Size: 130833 Color: 5
Size: 113957 Color: 11

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 663479 Color: 10
Size: 208744 Color: 5
Size: 127773 Color: 1

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 681329 Color: 3
Size: 192693 Color: 10
Size: 125974 Color: 14

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 471701 Color: 7
Size: 335900 Color: 18
Size: 192395 Color: 4

Bin 107: 5 of cap free
Amount of items: 3
Items: 
Size: 469296 Color: 10
Size: 333066 Color: 2
Size: 197634 Color: 17

Bin 108: 5 of cap free
Amount of items: 3
Items: 
Size: 412013 Color: 4
Size: 406205 Color: 12
Size: 181778 Color: 13

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 687354 Color: 19
Size: 182102 Color: 18
Size: 130539 Color: 1

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 695128 Color: 1
Size: 176015 Color: 6
Size: 128852 Color: 17

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 540781 Color: 2
Size: 299618 Color: 6
Size: 159596 Color: 13

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 538307 Color: 18
Size: 284983 Color: 17
Size: 176705 Color: 18

Bin 113: 6 of cap free
Amount of items: 3
Items: 
Size: 339499 Color: 5
Size: 330661 Color: 18
Size: 329835 Color: 16

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 409469 Color: 5
Size: 396995 Color: 12
Size: 193531 Color: 11

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 701500 Color: 15
Size: 170886 Color: 0
Size: 127608 Color: 5

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 686585 Color: 7
Size: 176309 Color: 12
Size: 137100 Color: 12

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 774880 Color: 1
Size: 115040 Color: 16
Size: 110074 Color: 1

Bin 118: 7 of cap free
Amount of items: 3
Items: 
Size: 681826 Color: 4
Size: 207886 Color: 16
Size: 110282 Color: 1

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 697726 Color: 17
Size: 302268 Color: 18

Bin 120: 7 of cap free
Amount of items: 3
Items: 
Size: 482822 Color: 7
Size: 333099 Color: 13
Size: 184073 Color: 17

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 564908 Color: 9
Size: 435085 Color: 4

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 704411 Color: 11
Size: 295582 Color: 14

Bin 123: 8 of cap free
Amount of items: 2
Items: 
Size: 782352 Color: 7
Size: 217641 Color: 11

Bin 124: 8 of cap free
Amount of items: 3
Items: 
Size: 611059 Color: 15
Size: 262121 Color: 6
Size: 126813 Color: 3

Bin 125: 8 of cap free
Amount of items: 3
Items: 
Size: 662506 Color: 0
Size: 209327 Color: 10
Size: 128160 Color: 1

Bin 126: 8 of cap free
Amount of items: 3
Items: 
Size: 497428 Color: 17
Size: 325598 Color: 4
Size: 176967 Color: 1

Bin 127: 9 of cap free
Amount of items: 2
Items: 
Size: 656642 Color: 3
Size: 343350 Color: 1

Bin 128: 9 of cap free
Amount of items: 3
Items: 
Size: 718499 Color: 12
Size: 143642 Color: 18
Size: 137851 Color: 10

Bin 129: 9 of cap free
Amount of items: 3
Items: 
Size: 453033 Color: 7
Size: 295754 Color: 12
Size: 251205 Color: 15

Bin 130: 9 of cap free
Amount of items: 2
Items: 
Size: 724808 Color: 8
Size: 275184 Color: 6

Bin 131: 9 of cap free
Amount of items: 2
Items: 
Size: 626528 Color: 9
Size: 373464 Color: 4

Bin 132: 10 of cap free
Amount of items: 3
Items: 
Size: 670252 Color: 3
Size: 185450 Color: 10
Size: 144289 Color: 18

Bin 133: 10 of cap free
Amount of items: 3
Items: 
Size: 549838 Color: 8
Size: 277627 Color: 4
Size: 172526 Color: 17

Bin 134: 10 of cap free
Amount of items: 3
Items: 
Size: 490246 Color: 4
Size: 330648 Color: 6
Size: 179097 Color: 7

Bin 135: 10 of cap free
Amount of items: 3
Items: 
Size: 417380 Color: 9
Size: 416729 Color: 4
Size: 165882 Color: 9

Bin 136: 11 of cap free
Amount of items: 2
Items: 
Size: 529490 Color: 19
Size: 470500 Color: 7

Bin 137: 11 of cap free
Amount of items: 3
Items: 
Size: 681389 Color: 1
Size: 170451 Color: 1
Size: 148150 Color: 17

Bin 138: 11 of cap free
Amount of items: 3
Items: 
Size: 709969 Color: 4
Size: 145973 Color: 14
Size: 144048 Color: 4

Bin 139: 11 of cap free
Amount of items: 3
Items: 
Size: 741628 Color: 3
Size: 142620 Color: 15
Size: 115742 Color: 8

Bin 140: 11 of cap free
Amount of items: 2
Items: 
Size: 659625 Color: 6
Size: 340365 Color: 9

Bin 141: 11 of cap free
Amount of items: 3
Items: 
Size: 492181 Color: 4
Size: 309373 Color: 12
Size: 198436 Color: 19

Bin 142: 12 of cap free
Amount of items: 2
Items: 
Size: 590281 Color: 2
Size: 409708 Color: 18

Bin 143: 12 of cap free
Amount of items: 2
Items: 
Size: 611233 Color: 15
Size: 388756 Color: 19

Bin 144: 12 of cap free
Amount of items: 2
Items: 
Size: 611378 Color: 14
Size: 388611 Color: 15

Bin 145: 12 of cap free
Amount of items: 3
Items: 
Size: 693620 Color: 19
Size: 154702 Color: 3
Size: 151667 Color: 10

Bin 146: 12 of cap free
Amount of items: 2
Items: 
Size: 735356 Color: 11
Size: 264633 Color: 8

Bin 147: 12 of cap free
Amount of items: 3
Items: 
Size: 672458 Color: 18
Size: 175000 Color: 1
Size: 152531 Color: 8

Bin 148: 12 of cap free
Amount of items: 3
Items: 
Size: 488370 Color: 12
Size: 329988 Color: 3
Size: 181631 Color: 8

Bin 149: 12 of cap free
Amount of items: 3
Items: 
Size: 413886 Color: 16
Size: 410716 Color: 9
Size: 175387 Color: 19

Bin 150: 13 of cap free
Amount of items: 3
Items: 
Size: 678508 Color: 13
Size: 169456 Color: 0
Size: 152024 Color: 16

Bin 151: 13 of cap free
Amount of items: 2
Items: 
Size: 767381 Color: 14
Size: 232607 Color: 1

Bin 152: 13 of cap free
Amount of items: 3
Items: 
Size: 518207 Color: 8
Size: 305969 Color: 1
Size: 175812 Color: 4

Bin 153: 13 of cap free
Amount of items: 2
Items: 
Size: 604464 Color: 8
Size: 395524 Color: 13

Bin 154: 14 of cap free
Amount of items: 2
Items: 
Size: 515570 Color: 1
Size: 484417 Color: 11

Bin 155: 14 of cap free
Amount of items: 2
Items: 
Size: 558550 Color: 19
Size: 441437 Color: 17

Bin 156: 14 of cap free
Amount of items: 2
Items: 
Size: 595081 Color: 13
Size: 404906 Color: 17

Bin 157: 14 of cap free
Amount of items: 3
Items: 
Size: 587030 Color: 11
Size: 273390 Color: 10
Size: 139567 Color: 9

Bin 158: 15 of cap free
Amount of items: 2
Items: 
Size: 677095 Color: 12
Size: 322891 Color: 11

Bin 159: 15 of cap free
Amount of items: 2
Items: 
Size: 792758 Color: 15
Size: 207228 Color: 0

Bin 160: 15 of cap free
Amount of items: 3
Items: 
Size: 487132 Color: 5
Size: 340213 Color: 5
Size: 172641 Color: 12

Bin 161: 15 of cap free
Amount of items: 3
Items: 
Size: 614267 Color: 3
Size: 195093 Color: 17
Size: 190626 Color: 2

Bin 162: 16 of cap free
Amount of items: 2
Items: 
Size: 690017 Color: 1
Size: 309968 Color: 14

Bin 163: 16 of cap free
Amount of items: 2
Items: 
Size: 781098 Color: 5
Size: 218887 Color: 17

Bin 164: 17 of cap free
Amount of items: 2
Items: 
Size: 703317 Color: 17
Size: 296667 Color: 8

Bin 165: 17 of cap free
Amount of items: 3
Items: 
Size: 525858 Color: 12
Size: 297527 Color: 11
Size: 176599 Color: 1

Bin 166: 17 of cap free
Amount of items: 3
Items: 
Size: 407863 Color: 18
Size: 406350 Color: 2
Size: 185771 Color: 11

Bin 167: 17 of cap free
Amount of items: 3
Items: 
Size: 371587 Color: 12
Size: 368365 Color: 9
Size: 260032 Color: 10

Bin 168: 18 of cap free
Amount of items: 2
Items: 
Size: 786054 Color: 6
Size: 213929 Color: 17

Bin 169: 18 of cap free
Amount of items: 2
Items: 
Size: 797301 Color: 16
Size: 202682 Color: 5

Bin 170: 18 of cap free
Amount of items: 3
Items: 
Size: 527184 Color: 13
Size: 294544 Color: 16
Size: 178255 Color: 7

Bin 171: 18 of cap free
Amount of items: 3
Items: 
Size: 496520 Color: 19
Size: 320757 Color: 9
Size: 182706 Color: 4

Bin 172: 18 of cap free
Amount of items: 3
Items: 
Size: 382599 Color: 18
Size: 380435 Color: 13
Size: 236949 Color: 18

Bin 173: 18 of cap free
Amount of items: 3
Items: 
Size: 503577 Color: 9
Size: 311104 Color: 15
Size: 185302 Color: 0

Bin 174: 19 of cap free
Amount of items: 3
Items: 
Size: 684759 Color: 2
Size: 195006 Color: 16
Size: 120217 Color: 19

Bin 175: 19 of cap free
Amount of items: 2
Items: 
Size: 739623 Color: 2
Size: 260359 Color: 0

Bin 176: 19 of cap free
Amount of items: 2
Items: 
Size: 741719 Color: 12
Size: 258263 Color: 19

Bin 177: 19 of cap free
Amount of items: 3
Items: 
Size: 455737 Color: 14
Size: 300509 Color: 18
Size: 243736 Color: 11

Bin 178: 19 of cap free
Amount of items: 2
Items: 
Size: 784149 Color: 10
Size: 215833 Color: 16

Bin 179: 19 of cap free
Amount of items: 3
Items: 
Size: 693954 Color: 19
Size: 159294 Color: 6
Size: 146734 Color: 6

Bin 180: 20 of cap free
Amount of items: 2
Items: 
Size: 678825 Color: 17
Size: 321156 Color: 10

Bin 181: 20 of cap free
Amount of items: 3
Items: 
Size: 416197 Color: 5
Size: 406310 Color: 0
Size: 177474 Color: 5

Bin 182: 21 of cap free
Amount of items: 2
Items: 
Size: 775360 Color: 16
Size: 224620 Color: 9

Bin 183: 21 of cap free
Amount of items: 3
Items: 
Size: 507719 Color: 14
Size: 306081 Color: 3
Size: 186180 Color: 3

Bin 184: 21 of cap free
Amount of items: 2
Items: 
Size: 634950 Color: 10
Size: 365030 Color: 5

Bin 185: 22 of cap free
Amount of items: 2
Items: 
Size: 784033 Color: 17
Size: 215946 Color: 2

Bin 186: 23 of cap free
Amount of items: 2
Items: 
Size: 683016 Color: 15
Size: 316962 Color: 2

Bin 187: 23 of cap free
Amount of items: 2
Items: 
Size: 622913 Color: 15
Size: 377065 Color: 3

Bin 188: 23 of cap free
Amount of items: 2
Items: 
Size: 646859 Color: 4
Size: 353119 Color: 16

Bin 189: 23 of cap free
Amount of items: 3
Items: 
Size: 696988 Color: 11
Size: 156747 Color: 13
Size: 146243 Color: 19

Bin 190: 23 of cap free
Amount of items: 3
Items: 
Size: 504081 Color: 4
Size: 314333 Color: 2
Size: 181564 Color: 1

Bin 191: 23 of cap free
Amount of items: 2
Items: 
Size: 666580 Color: 8
Size: 333398 Color: 1

Bin 192: 23 of cap free
Amount of items: 3
Items: 
Size: 564671 Color: 2
Size: 319112 Color: 1
Size: 116195 Color: 17

Bin 193: 23 of cap free
Amount of items: 2
Items: 
Size: 594200 Color: 4
Size: 405778 Color: 14

Bin 194: 24 of cap free
Amount of items: 2
Items: 
Size: 695904 Color: 6
Size: 304073 Color: 8

Bin 195: 24 of cap free
Amount of items: 2
Items: 
Size: 554941 Color: 6
Size: 445036 Color: 19

Bin 196: 24 of cap free
Amount of items: 3
Items: 
Size: 546103 Color: 7
Size: 300436 Color: 2
Size: 153438 Color: 6

Bin 197: 24 of cap free
Amount of items: 3
Items: 
Size: 777981 Color: 2
Size: 117155 Color: 13
Size: 104841 Color: 15

Bin 198: 25 of cap free
Amount of items: 2
Items: 
Size: 669606 Color: 19
Size: 330370 Color: 15

Bin 199: 25 of cap free
Amount of items: 2
Items: 
Size: 663260 Color: 6
Size: 336716 Color: 2

Bin 200: 25 of cap free
Amount of items: 3
Items: 
Size: 515752 Color: 12
Size: 299946 Color: 11
Size: 184278 Color: 12

Bin 201: 26 of cap free
Amount of items: 3
Items: 
Size: 690452 Color: 14
Size: 197509 Color: 10
Size: 112014 Color: 12

Bin 202: 27 of cap free
Amount of items: 2
Items: 
Size: 592932 Color: 1
Size: 407042 Color: 10

Bin 203: 27 of cap free
Amount of items: 2
Items: 
Size: 717502 Color: 18
Size: 282472 Color: 4

Bin 204: 27 of cap free
Amount of items: 2
Items: 
Size: 798248 Color: 13
Size: 201726 Color: 17

Bin 205: 28 of cap free
Amount of items: 2
Items: 
Size: 650253 Color: 11
Size: 349720 Color: 1

Bin 206: 28 of cap free
Amount of items: 3
Items: 
Size: 534858 Color: 19
Size: 303582 Color: 11
Size: 161533 Color: 12

Bin 207: 28 of cap free
Amount of items: 3
Items: 
Size: 401311 Color: 14
Size: 395816 Color: 18
Size: 202846 Color: 5

Bin 208: 29 of cap free
Amount of items: 3
Items: 
Size: 584425 Color: 5
Size: 296876 Color: 17
Size: 118671 Color: 14

Bin 209: 29 of cap free
Amount of items: 2
Items: 
Size: 531520 Color: 7
Size: 468452 Color: 0

Bin 210: 29 of cap free
Amount of items: 3
Items: 
Size: 401794 Color: 8
Size: 372854 Color: 9
Size: 225324 Color: 16

Bin 211: 30 of cap free
Amount of items: 3
Items: 
Size: 511055 Color: 17
Size: 317084 Color: 3
Size: 171832 Color: 14

Bin 212: 30 of cap free
Amount of items: 2
Items: 
Size: 662812 Color: 15
Size: 337159 Color: 10

Bin 213: 30 of cap free
Amount of items: 4
Items: 
Size: 554734 Color: 14
Size: 180086 Color: 2
Size: 151807 Color: 5
Size: 113344 Color: 13

Bin 214: 30 of cap free
Amount of items: 3
Items: 
Size: 465909 Color: 5
Size: 344825 Color: 8
Size: 189237 Color: 15

Bin 215: 31 of cap free
Amount of items: 3
Items: 
Size: 738569 Color: 3
Size: 148549 Color: 9
Size: 112852 Color: 1

Bin 216: 31 of cap free
Amount of items: 2
Items: 
Size: 783749 Color: 7
Size: 216221 Color: 13

Bin 217: 32 of cap free
Amount of items: 2
Items: 
Size: 646996 Color: 6
Size: 352973 Color: 17

Bin 218: 32 of cap free
Amount of items: 3
Items: 
Size: 685828 Color: 11
Size: 160574 Color: 9
Size: 153567 Color: 10

Bin 219: 32 of cap free
Amount of items: 3
Items: 
Size: 412817 Color: 0
Size: 394557 Color: 19
Size: 192595 Color: 6

Bin 220: 33 of cap free
Amount of items: 2
Items: 
Size: 537474 Color: 17
Size: 462494 Color: 14

Bin 221: 33 of cap free
Amount of items: 3
Items: 
Size: 558828 Color: 9
Size: 287879 Color: 6
Size: 153261 Color: 12

Bin 222: 33 of cap free
Amount of items: 3
Items: 
Size: 487317 Color: 8
Size: 333154 Color: 10
Size: 179497 Color: 16

Bin 223: 33 of cap free
Amount of items: 3
Items: 
Size: 537864 Color: 10
Size: 305906 Color: 5
Size: 156198 Color: 15

Bin 224: 34 of cap free
Amount of items: 2
Items: 
Size: 638619 Color: 10
Size: 361348 Color: 0

Bin 225: 34 of cap free
Amount of items: 3
Items: 
Size: 497188 Color: 17
Size: 323037 Color: 7
Size: 179742 Color: 6

Bin 226: 34 of cap free
Amount of items: 2
Items: 
Size: 517604 Color: 4
Size: 482363 Color: 3

Bin 227: 34 of cap free
Amount of items: 3
Items: 
Size: 467130 Color: 3
Size: 412488 Color: 2
Size: 120349 Color: 7

Bin 228: 35 of cap free
Amount of items: 2
Items: 
Size: 708763 Color: 5
Size: 291203 Color: 9

Bin 229: 36 of cap free
Amount of items: 2
Items: 
Size: 704589 Color: 9
Size: 295376 Color: 4

Bin 230: 37 of cap free
Amount of items: 2
Items: 
Size: 533768 Color: 11
Size: 466196 Color: 16

Bin 231: 37 of cap free
Amount of items: 2
Items: 
Size: 536628 Color: 18
Size: 463336 Color: 3

Bin 232: 37 of cap free
Amount of items: 2
Items: 
Size: 750886 Color: 2
Size: 249078 Color: 18

Bin 233: 37 of cap free
Amount of items: 2
Items: 
Size: 650805 Color: 11
Size: 349159 Color: 18

Bin 234: 38 of cap free
Amount of items: 3
Items: 
Size: 553743 Color: 1
Size: 293983 Color: 0
Size: 152237 Color: 11

Bin 235: 38 of cap free
Amount of items: 2
Items: 
Size: 639962 Color: 6
Size: 360001 Color: 12

Bin 236: 38 of cap free
Amount of items: 2
Items: 
Size: 577780 Color: 10
Size: 422183 Color: 2

Bin 237: 39 of cap free
Amount of items: 2
Items: 
Size: 655185 Color: 9
Size: 344777 Color: 11

Bin 238: 39 of cap free
Amount of items: 2
Items: 
Size: 797178 Color: 1
Size: 202784 Color: 7

Bin 239: 39 of cap free
Amount of items: 3
Items: 
Size: 672090 Color: 1
Size: 168526 Color: 3
Size: 159346 Color: 2

Bin 240: 40 of cap free
Amount of items: 2
Items: 
Size: 565784 Color: 17
Size: 434177 Color: 8

Bin 241: 40 of cap free
Amount of items: 2
Items: 
Size: 571241 Color: 19
Size: 428720 Color: 0

Bin 242: 41 of cap free
Amount of items: 3
Items: 
Size: 509562 Color: 3
Size: 319585 Color: 5
Size: 170813 Color: 10

Bin 243: 41 of cap free
Amount of items: 2
Items: 
Size: 735617 Color: 7
Size: 264343 Color: 12

Bin 244: 42 of cap free
Amount of items: 2
Items: 
Size: 746686 Color: 3
Size: 253273 Color: 0

Bin 245: 42 of cap free
Amount of items: 2
Items: 
Size: 787417 Color: 1
Size: 212542 Color: 15

Bin 246: 42 of cap free
Amount of items: 2
Items: 
Size: 523214 Color: 3
Size: 476745 Color: 2

Bin 247: 42 of cap free
Amount of items: 3
Items: 
Size: 505853 Color: 16
Size: 315601 Color: 19
Size: 178505 Color: 10

Bin 248: 43 of cap free
Amount of items: 2
Items: 
Size: 705616 Color: 15
Size: 294342 Color: 9

Bin 249: 43 of cap free
Amount of items: 3
Items: 
Size: 573669 Color: 18
Size: 277374 Color: 16
Size: 148915 Color: 11

Bin 250: 43 of cap free
Amount of items: 3
Items: 
Size: 539223 Color: 2
Size: 297359 Color: 4
Size: 163376 Color: 2

Bin 251: 44 of cap free
Amount of items: 2
Items: 
Size: 770604 Color: 5
Size: 229353 Color: 17

Bin 252: 44 of cap free
Amount of items: 3
Items: 
Size: 368505 Color: 17
Size: 365473 Color: 16
Size: 265979 Color: 10

Bin 253: 46 of cap free
Amount of items: 2
Items: 
Size: 797588 Color: 3
Size: 202367 Color: 18

Bin 254: 46 of cap free
Amount of items: 3
Items: 
Size: 549575 Color: 14
Size: 252177 Color: 19
Size: 198203 Color: 16

Bin 255: 47 of cap free
Amount of items: 2
Items: 
Size: 796442 Color: 10
Size: 203512 Color: 2

Bin 256: 47 of cap free
Amount of items: 3
Items: 
Size: 700143 Color: 7
Size: 188992 Color: 14
Size: 110819 Color: 3

Bin 257: 47 of cap free
Amount of items: 3
Items: 
Size: 506968 Color: 6
Size: 320982 Color: 13
Size: 172004 Color: 7

Bin 258: 48 of cap free
Amount of items: 3
Items: 
Size: 365724 Color: 18
Size: 323032 Color: 12
Size: 311197 Color: 13

Bin 259: 50 of cap free
Amount of items: 2
Items: 
Size: 676781 Color: 7
Size: 323170 Color: 4

Bin 260: 50 of cap free
Amount of items: 2
Items: 
Size: 509383 Color: 14
Size: 490568 Color: 12

Bin 261: 50 of cap free
Amount of items: 3
Items: 
Size: 590642 Color: 12
Size: 286380 Color: 3
Size: 122929 Color: 16

Bin 262: 50 of cap free
Amount of items: 2
Items: 
Size: 518718 Color: 8
Size: 481233 Color: 4

Bin 263: 50 of cap free
Amount of items: 2
Items: 
Size: 746106 Color: 2
Size: 253845 Color: 0

Bin 264: 50 of cap free
Amount of items: 3
Items: 
Size: 395225 Color: 10
Size: 371617 Color: 8
Size: 233109 Color: 4

Bin 265: 51 of cap free
Amount of items: 3
Items: 
Size: 612973 Color: 4
Size: 196425 Color: 10
Size: 190552 Color: 9

Bin 266: 51 of cap free
Amount of items: 2
Items: 
Size: 710963 Color: 1
Size: 288987 Color: 18

Bin 267: 52 of cap free
Amount of items: 2
Items: 
Size: 503659 Color: 7
Size: 496290 Color: 15

Bin 268: 52 of cap free
Amount of items: 2
Items: 
Size: 548634 Color: 14
Size: 451315 Color: 10

Bin 269: 52 of cap free
Amount of items: 2
Items: 
Size: 614631 Color: 6
Size: 385318 Color: 19

Bin 270: 52 of cap free
Amount of items: 2
Items: 
Size: 643877 Color: 9
Size: 356072 Color: 4

Bin 271: 52 of cap free
Amount of items: 2
Items: 
Size: 563691 Color: 13
Size: 436258 Color: 6

Bin 272: 52 of cap free
Amount of items: 2
Items: 
Size: 522572 Color: 9
Size: 477377 Color: 16

Bin 273: 54 of cap free
Amount of items: 2
Items: 
Size: 737836 Color: 7
Size: 262111 Color: 9

Bin 274: 54 of cap free
Amount of items: 2
Items: 
Size: 518642 Color: 14
Size: 481305 Color: 12

Bin 275: 54 of cap free
Amount of items: 2
Items: 
Size: 746944 Color: 0
Size: 253003 Color: 11

Bin 276: 55 of cap free
Amount of items: 2
Items: 
Size: 741298 Color: 8
Size: 258648 Color: 19

Bin 277: 55 of cap free
Amount of items: 3
Items: 
Size: 422621 Color: 16
Size: 422262 Color: 18
Size: 155063 Color: 10

Bin 278: 56 of cap free
Amount of items: 2
Items: 
Size: 774889 Color: 14
Size: 225056 Color: 13

Bin 279: 57 of cap free
Amount of items: 2
Items: 
Size: 556754 Color: 2
Size: 443190 Color: 19

Bin 280: 57 of cap free
Amount of items: 2
Items: 
Size: 616048 Color: 17
Size: 383896 Color: 10

Bin 281: 57 of cap free
Amount of items: 2
Items: 
Size: 751442 Color: 7
Size: 248502 Color: 12

Bin 282: 57 of cap free
Amount of items: 2
Items: 
Size: 717282 Color: 19
Size: 282662 Color: 2

Bin 283: 59 of cap free
Amount of items: 2
Items: 
Size: 793788 Color: 10
Size: 206154 Color: 5

Bin 284: 60 of cap free
Amount of items: 2
Items: 
Size: 516470 Color: 18
Size: 483471 Color: 5

Bin 285: 60 of cap free
Amount of items: 2
Items: 
Size: 650009 Color: 0
Size: 349932 Color: 14

Bin 286: 60 of cap free
Amount of items: 2
Items: 
Size: 786261 Color: 9
Size: 213680 Color: 5

Bin 287: 61 of cap free
Amount of items: 2
Items: 
Size: 556631 Color: 19
Size: 443309 Color: 8

Bin 288: 61 of cap free
Amount of items: 2
Items: 
Size: 739679 Color: 4
Size: 260261 Color: 15

Bin 289: 62 of cap free
Amount of items: 2
Items: 
Size: 578056 Color: 8
Size: 421883 Color: 3

Bin 290: 63 of cap free
Amount of items: 2
Items: 
Size: 534062 Color: 6
Size: 465876 Color: 9

Bin 291: 64 of cap free
Amount of items: 3
Items: 
Size: 401113 Color: 13
Size: 394641 Color: 2
Size: 204183 Color: 13

Bin 292: 66 of cap free
Amount of items: 3
Items: 
Size: 479294 Color: 15
Size: 329799 Color: 5
Size: 190842 Color: 4

Bin 293: 66 of cap free
Amount of items: 3
Items: 
Size: 685391 Color: 5
Size: 188719 Color: 16
Size: 125825 Color: 12

Bin 294: 67 of cap free
Amount of items: 2
Items: 
Size: 767051 Color: 18
Size: 232883 Color: 12

Bin 295: 69 of cap free
Amount of items: 2
Items: 
Size: 641829 Color: 19
Size: 358103 Color: 2

Bin 296: 70 of cap free
Amount of items: 3
Items: 
Size: 469028 Color: 14
Size: 374551 Color: 18
Size: 156352 Color: 13

Bin 297: 71 of cap free
Amount of items: 3
Items: 
Size: 573535 Color: 2
Size: 277043 Color: 10
Size: 149352 Color: 19

Bin 298: 72 of cap free
Amount of items: 3
Items: 
Size: 546204 Color: 0
Size: 301275 Color: 4
Size: 152450 Color: 6

Bin 299: 72 of cap free
Amount of items: 2
Items: 
Size: 615011 Color: 0
Size: 384918 Color: 18

Bin 300: 74 of cap free
Amount of items: 2
Items: 
Size: 550162 Color: 2
Size: 449765 Color: 0

Bin 301: 74 of cap free
Amount of items: 3
Items: 
Size: 438887 Color: 13
Size: 411069 Color: 12
Size: 149971 Color: 1

Bin 302: 76 of cap free
Amount of items: 2
Items: 
Size: 613610 Color: 1
Size: 386315 Color: 16

Bin 303: 76 of cap free
Amount of items: 3
Items: 
Size: 413336 Color: 9
Size: 405364 Color: 6
Size: 181225 Color: 3

Bin 304: 78 of cap free
Amount of items: 3
Items: 
Size: 408111 Color: 0
Size: 383739 Color: 3
Size: 208073 Color: 1

Bin 305: 80 of cap free
Amount of items: 2
Items: 
Size: 794908 Color: 6
Size: 205013 Color: 11

Bin 306: 80 of cap free
Amount of items: 2
Items: 
Size: 635639 Color: 5
Size: 364282 Color: 8

Bin 307: 81 of cap free
Amount of items: 3
Items: 
Size: 699204 Color: 5
Size: 165774 Color: 2
Size: 134942 Color: 7

Bin 308: 81 of cap free
Amount of items: 3
Items: 
Size: 683339 Color: 12
Size: 175960 Color: 15
Size: 140621 Color: 18

Bin 309: 83 of cap free
Amount of items: 2
Items: 
Size: 689023 Color: 15
Size: 310895 Color: 8

Bin 310: 85 of cap free
Amount of items: 2
Items: 
Size: 736809 Color: 6
Size: 263107 Color: 13

Bin 311: 85 of cap free
Amount of items: 2
Items: 
Size: 788670 Color: 18
Size: 211246 Color: 6

Bin 312: 86 of cap free
Amount of items: 2
Items: 
Size: 688825 Color: 17
Size: 311090 Color: 2

Bin 313: 87 of cap free
Amount of items: 2
Items: 
Size: 609680 Color: 14
Size: 390234 Color: 13

Bin 314: 88 of cap free
Amount of items: 2
Items: 
Size: 759125 Color: 9
Size: 240788 Color: 13

Bin 315: 88 of cap free
Amount of items: 2
Items: 
Size: 747754 Color: 8
Size: 252159 Color: 2

Bin 316: 89 of cap free
Amount of items: 3
Items: 
Size: 695959 Color: 14
Size: 156593 Color: 16
Size: 147360 Color: 19

Bin 317: 89 of cap free
Amount of items: 2
Items: 
Size: 607921 Color: 1
Size: 391991 Color: 5

Bin 318: 90 of cap free
Amount of items: 2
Items: 
Size: 544230 Color: 4
Size: 455681 Color: 3

Bin 319: 91 of cap free
Amount of items: 2
Items: 
Size: 583440 Color: 10
Size: 416470 Color: 17

Bin 320: 91 of cap free
Amount of items: 2
Items: 
Size: 620361 Color: 18
Size: 379549 Color: 5

Bin 321: 91 of cap free
Amount of items: 2
Items: 
Size: 634713 Color: 10
Size: 365197 Color: 16

Bin 322: 91 of cap free
Amount of items: 2
Items: 
Size: 738647 Color: 7
Size: 261263 Color: 5

Bin 323: 91 of cap free
Amount of items: 3
Items: 
Size: 486878 Color: 17
Size: 336188 Color: 9
Size: 176844 Color: 18

Bin 324: 91 of cap free
Amount of items: 2
Items: 
Size: 676106 Color: 11
Size: 323804 Color: 19

Bin 325: 94 of cap free
Amount of items: 2
Items: 
Size: 644439 Color: 6
Size: 355468 Color: 11

Bin 326: 94 of cap free
Amount of items: 2
Items: 
Size: 763934 Color: 14
Size: 235973 Color: 9

Bin 327: 95 of cap free
Amount of items: 2
Items: 
Size: 624445 Color: 5
Size: 375461 Color: 9

Bin 328: 96 of cap free
Amount of items: 2
Items: 
Size: 596963 Color: 13
Size: 402942 Color: 10

Bin 329: 96 of cap free
Amount of items: 2
Items: 
Size: 707201 Color: 5
Size: 292704 Color: 11

Bin 330: 96 of cap free
Amount of items: 2
Items: 
Size: 679268 Color: 15
Size: 320637 Color: 17

Bin 331: 97 of cap free
Amount of items: 3
Items: 
Size: 663077 Color: 10
Size: 181398 Color: 3
Size: 155429 Color: 1

Bin 332: 98 of cap free
Amount of items: 3
Items: 
Size: 469509 Color: 3
Size: 344237 Color: 7
Size: 186157 Color: 0

Bin 333: 98 of cap free
Amount of items: 2
Items: 
Size: 747874 Color: 8
Size: 252029 Color: 14

Bin 334: 98 of cap free
Amount of items: 3
Items: 
Size: 381911 Color: 8
Size: 369526 Color: 1
Size: 248466 Color: 5

Bin 335: 99 of cap free
Amount of items: 2
Items: 
Size: 512457 Color: 4
Size: 487445 Color: 8

Bin 336: 100 of cap free
Amount of items: 2
Items: 
Size: 580825 Color: 10
Size: 419076 Color: 11

Bin 337: 100 of cap free
Amount of items: 2
Items: 
Size: 725187 Color: 12
Size: 274714 Color: 8

Bin 338: 101 of cap free
Amount of items: 2
Items: 
Size: 508295 Color: 5
Size: 491605 Color: 13

Bin 339: 102 of cap free
Amount of items: 3
Items: 
Size: 527616 Color: 7
Size: 305076 Color: 6
Size: 167207 Color: 2

Bin 340: 102 of cap free
Amount of items: 2
Items: 
Size: 682994 Color: 8
Size: 316905 Color: 0

Bin 341: 102 of cap free
Amount of items: 3
Items: 
Size: 394274 Color: 2
Size: 329775 Color: 19
Size: 275850 Color: 14

Bin 342: 103 of cap free
Amount of items: 2
Items: 
Size: 558607 Color: 9
Size: 441291 Color: 18

Bin 343: 104 of cap free
Amount of items: 3
Items: 
Size: 605968 Color: 3
Size: 208394 Color: 16
Size: 185535 Color: 0

Bin 344: 104 of cap free
Amount of items: 3
Items: 
Size: 401465 Color: 11
Size: 381597 Color: 19
Size: 216835 Color: 1

Bin 345: 105 of cap free
Amount of items: 3
Items: 
Size: 525520 Color: 6
Size: 311781 Color: 14
Size: 162595 Color: 1

Bin 346: 105 of cap free
Amount of items: 2
Items: 
Size: 572938 Color: 12
Size: 426958 Color: 2

Bin 347: 105 of cap free
Amount of items: 2
Items: 
Size: 579142 Color: 3
Size: 420754 Color: 15

Bin 348: 106 of cap free
Amount of items: 2
Items: 
Size: 551247 Color: 3
Size: 448648 Color: 16

Bin 349: 107 of cap free
Amount of items: 2
Items: 
Size: 513359 Color: 3
Size: 486535 Color: 0

Bin 350: 107 of cap free
Amount of items: 3
Items: 
Size: 525985 Color: 3
Size: 332179 Color: 9
Size: 141730 Color: 14

Bin 351: 108 of cap free
Amount of items: 2
Items: 
Size: 602479 Color: 8
Size: 397414 Color: 10

Bin 352: 108 of cap free
Amount of items: 2
Items: 
Size: 634014 Color: 2
Size: 365879 Color: 12

Bin 353: 108 of cap free
Amount of items: 2
Items: 
Size: 737562 Color: 4
Size: 262331 Color: 19

Bin 354: 109 of cap free
Amount of items: 2
Items: 
Size: 644586 Color: 10
Size: 355306 Color: 19

Bin 355: 109 of cap free
Amount of items: 2
Items: 
Size: 787043 Color: 18
Size: 212849 Color: 12

Bin 356: 109 of cap free
Amount of items: 2
Items: 
Size: 561252 Color: 8
Size: 438640 Color: 15

Bin 357: 110 of cap free
Amount of items: 3
Items: 
Size: 518167 Color: 18
Size: 333961 Color: 2
Size: 147763 Color: 1

Bin 358: 110 of cap free
Amount of items: 2
Items: 
Size: 610461 Color: 2
Size: 389430 Color: 6

Bin 359: 112 of cap free
Amount of items: 2
Items: 
Size: 503858 Color: 4
Size: 496031 Color: 8

Bin 360: 112 of cap free
Amount of items: 2
Items: 
Size: 675316 Color: 17
Size: 324573 Color: 19

Bin 361: 113 of cap free
Amount of items: 2
Items: 
Size: 541562 Color: 10
Size: 458326 Color: 12

Bin 362: 114 of cap free
Amount of items: 2
Items: 
Size: 745295 Color: 5
Size: 254592 Color: 9

Bin 363: 114 of cap free
Amount of items: 2
Items: 
Size: 786899 Color: 5
Size: 212988 Color: 4

Bin 364: 115 of cap free
Amount of items: 2
Items: 
Size: 505049 Color: 0
Size: 494837 Color: 8

Bin 365: 116 of cap free
Amount of items: 2
Items: 
Size: 708511 Color: 6
Size: 291374 Color: 14

Bin 366: 117 of cap free
Amount of items: 2
Items: 
Size: 795401 Color: 18
Size: 204483 Color: 7

Bin 367: 118 of cap free
Amount of items: 2
Items: 
Size: 778965 Color: 1
Size: 220918 Color: 18

Bin 368: 118 of cap free
Amount of items: 2
Items: 
Size: 672225 Color: 13
Size: 327658 Color: 15

Bin 369: 119 of cap free
Amount of items: 2
Items: 
Size: 620186 Color: 5
Size: 379696 Color: 8

Bin 370: 119 of cap free
Amount of items: 2
Items: 
Size: 686180 Color: 12
Size: 313702 Color: 17

Bin 371: 119 of cap free
Amount of items: 2
Items: 
Size: 705131 Color: 8
Size: 294751 Color: 15

Bin 372: 120 of cap free
Amount of items: 3
Items: 
Size: 518918 Color: 11
Size: 307984 Color: 7
Size: 172979 Color: 3

Bin 373: 121 of cap free
Amount of items: 2
Items: 
Size: 520195 Color: 2
Size: 479685 Color: 3

Bin 374: 121 of cap free
Amount of items: 2
Items: 
Size: 775940 Color: 3
Size: 223940 Color: 6

Bin 375: 123 of cap free
Amount of items: 2
Items: 
Size: 672046 Color: 9
Size: 327832 Color: 7

Bin 376: 125 of cap free
Amount of items: 2
Items: 
Size: 687979 Color: 13
Size: 311897 Color: 17

Bin 377: 125 of cap free
Amount of items: 2
Items: 
Size: 539145 Color: 9
Size: 460731 Color: 6

Bin 378: 126 of cap free
Amount of items: 2
Items: 
Size: 522926 Color: 2
Size: 476949 Color: 17

Bin 379: 126 of cap free
Amount of items: 2
Items: 
Size: 713867 Color: 5
Size: 286008 Color: 19

Bin 380: 127 of cap free
Amount of items: 2
Items: 
Size: 757815 Color: 4
Size: 242059 Color: 2

Bin 381: 127 of cap free
Amount of items: 2
Items: 
Size: 528853 Color: 8
Size: 471021 Color: 5

Bin 382: 128 of cap free
Amount of items: 2
Items: 
Size: 637042 Color: 6
Size: 362831 Color: 18

Bin 383: 129 of cap free
Amount of items: 2
Items: 
Size: 606461 Color: 13
Size: 393411 Color: 11

Bin 384: 129 of cap free
Amount of items: 2
Items: 
Size: 745955 Color: 18
Size: 253917 Color: 6

Bin 385: 130 of cap free
Amount of items: 2
Items: 
Size: 612301 Color: 15
Size: 387570 Color: 7

Bin 386: 130 of cap free
Amount of items: 2
Items: 
Size: 617094 Color: 4
Size: 382777 Color: 5

Bin 387: 130 of cap free
Amount of items: 2
Items: 
Size: 507054 Color: 17
Size: 492817 Color: 15

Bin 388: 131 of cap free
Amount of items: 2
Items: 
Size: 738933 Color: 0
Size: 260937 Color: 19

Bin 389: 133 of cap free
Amount of items: 2
Items: 
Size: 550675 Color: 0
Size: 449193 Color: 13

Bin 390: 133 of cap free
Amount of items: 2
Items: 
Size: 625140 Color: 0
Size: 374728 Color: 7

Bin 391: 135 of cap free
Amount of items: 2
Items: 
Size: 566844 Color: 8
Size: 433022 Color: 19

Bin 392: 137 of cap free
Amount of items: 2
Items: 
Size: 580196 Color: 3
Size: 419668 Color: 17

Bin 393: 138 of cap free
Amount of items: 2
Items: 
Size: 524132 Color: 6
Size: 475731 Color: 8

Bin 394: 138 of cap free
Amount of items: 2
Items: 
Size: 650924 Color: 5
Size: 348939 Color: 17

Bin 395: 138 of cap free
Amount of items: 2
Items: 
Size: 652470 Color: 12
Size: 347393 Color: 17

Bin 396: 138 of cap free
Amount of items: 2
Items: 
Size: 756876 Color: 4
Size: 242987 Color: 15

Bin 397: 139 of cap free
Amount of items: 2
Items: 
Size: 622996 Color: 15
Size: 376866 Color: 8

Bin 398: 139 of cap free
Amount of items: 2
Items: 
Size: 687534 Color: 3
Size: 312328 Color: 14

Bin 399: 140 of cap free
Amount of items: 2
Items: 
Size: 758459 Color: 17
Size: 241402 Color: 0

Bin 400: 141 of cap free
Amount of items: 2
Items: 
Size: 552965 Color: 14
Size: 446895 Color: 11

Bin 401: 141 of cap free
Amount of items: 2
Items: 
Size: 735288 Color: 17
Size: 264572 Color: 14

Bin 402: 143 of cap free
Amount of items: 2
Items: 
Size: 656623 Color: 7
Size: 343235 Color: 6

Bin 403: 143 of cap free
Amount of items: 2
Items: 
Size: 679381 Color: 12
Size: 320477 Color: 2

Bin 404: 144 of cap free
Amount of items: 2
Items: 
Size: 658484 Color: 0
Size: 341373 Color: 11

Bin 405: 144 of cap free
Amount of items: 2
Items: 
Size: 693675 Color: 16
Size: 306182 Color: 6

Bin 406: 145 of cap free
Amount of items: 2
Items: 
Size: 521767 Color: 2
Size: 478089 Color: 3

Bin 407: 145 of cap free
Amount of items: 2
Items: 
Size: 673018 Color: 2
Size: 326838 Color: 5

Bin 408: 146 of cap free
Amount of items: 3
Items: 
Size: 698355 Color: 18
Size: 178981 Color: 5
Size: 122519 Color: 0

Bin 409: 148 of cap free
Amount of items: 2
Items: 
Size: 554008 Color: 1
Size: 445845 Color: 12

Bin 410: 148 of cap free
Amount of items: 2
Items: 
Size: 682785 Color: 14
Size: 317068 Color: 7

Bin 411: 149 of cap free
Amount of items: 2
Items: 
Size: 701005 Color: 3
Size: 298847 Color: 19

Bin 412: 149 of cap free
Amount of items: 3
Items: 
Size: 602452 Color: 8
Size: 200299 Color: 14
Size: 197101 Color: 5

Bin 413: 150 of cap free
Amount of items: 2
Items: 
Size: 536979 Color: 16
Size: 462872 Color: 3

Bin 414: 151 of cap free
Amount of items: 2
Items: 
Size: 540174 Color: 15
Size: 459676 Color: 18

Bin 415: 155 of cap free
Amount of items: 2
Items: 
Size: 532954 Color: 9
Size: 466892 Color: 4

Bin 416: 156 of cap free
Amount of items: 2
Items: 
Size: 581198 Color: 7
Size: 418647 Color: 12

Bin 417: 158 of cap free
Amount of items: 2
Items: 
Size: 636262 Color: 5
Size: 363581 Color: 14

Bin 418: 159 of cap free
Amount of items: 2
Items: 
Size: 612698 Color: 13
Size: 387144 Color: 15

Bin 419: 159 of cap free
Amount of items: 2
Items: 
Size: 630878 Color: 1
Size: 368964 Color: 15

Bin 420: 160 of cap free
Amount of items: 2
Items: 
Size: 566373 Color: 11
Size: 433468 Color: 0

Bin 421: 161 of cap free
Amount of items: 2
Items: 
Size: 676738 Color: 17
Size: 323102 Color: 0

Bin 422: 163 of cap free
Amount of items: 2
Items: 
Size: 699493 Color: 19
Size: 300345 Color: 3

Bin 423: 163 of cap free
Amount of items: 2
Items: 
Size: 761962 Color: 16
Size: 237876 Color: 14

Bin 424: 164 of cap free
Amount of items: 2
Items: 
Size: 621022 Color: 8
Size: 378815 Color: 7

Bin 425: 164 of cap free
Amount of items: 2
Items: 
Size: 690929 Color: 2
Size: 308908 Color: 15

Bin 426: 164 of cap free
Amount of items: 2
Items: 
Size: 720899 Color: 4
Size: 278938 Color: 7

Bin 427: 164 of cap free
Amount of items: 2
Items: 
Size: 572189 Color: 15
Size: 427648 Color: 12

Bin 428: 165 of cap free
Amount of items: 2
Items: 
Size: 524517 Color: 9
Size: 475319 Color: 18

Bin 429: 166 of cap free
Amount of items: 2
Items: 
Size: 771955 Color: 3
Size: 227880 Color: 5

Bin 430: 167 of cap free
Amount of items: 2
Items: 
Size: 724264 Color: 15
Size: 275570 Color: 11

Bin 431: 168 of cap free
Amount of items: 2
Items: 
Size: 609320 Color: 6
Size: 390513 Color: 5

Bin 432: 169 of cap free
Amount of items: 3
Items: 
Size: 488293 Color: 2
Size: 324702 Color: 17
Size: 186837 Color: 3

Bin 433: 172 of cap free
Amount of items: 2
Items: 
Size: 769740 Color: 5
Size: 230089 Color: 17

Bin 434: 173 of cap free
Amount of items: 2
Items: 
Size: 610584 Color: 9
Size: 389244 Color: 4

Bin 435: 177 of cap free
Amount of items: 2
Items: 
Size: 505070 Color: 8
Size: 494754 Color: 1

Bin 436: 178 of cap free
Amount of items: 2
Items: 
Size: 526499 Color: 12
Size: 473324 Color: 6

Bin 437: 178 of cap free
Amount of items: 2
Items: 
Size: 657052 Color: 9
Size: 342771 Color: 15

Bin 438: 183 of cap free
Amount of items: 2
Items: 
Size: 683647 Color: 1
Size: 316171 Color: 9

Bin 439: 184 of cap free
Amount of items: 2
Items: 
Size: 531182 Color: 3
Size: 468635 Color: 19

Bin 440: 184 of cap free
Amount of items: 2
Items: 
Size: 595062 Color: 14
Size: 404755 Color: 16

Bin 441: 185 of cap free
Amount of items: 2
Items: 
Size: 557247 Color: 7
Size: 442569 Color: 14

Bin 442: 186 of cap free
Amount of items: 2
Items: 
Size: 760519 Color: 6
Size: 239296 Color: 13

Bin 443: 188 of cap free
Amount of items: 2
Items: 
Size: 534986 Color: 16
Size: 464827 Color: 12

Bin 444: 192 of cap free
Amount of items: 2
Items: 
Size: 718136 Color: 13
Size: 281673 Color: 8

Bin 445: 196 of cap free
Amount of items: 2
Items: 
Size: 591997 Color: 10
Size: 407808 Color: 15

Bin 446: 199 of cap free
Amount of items: 2
Items: 
Size: 569921 Color: 6
Size: 429881 Color: 18

Bin 447: 199 of cap free
Amount of items: 3
Items: 
Size: 410487 Color: 10
Size: 366585 Color: 15
Size: 222730 Color: 17

Bin 448: 201 of cap free
Amount of items: 2
Items: 
Size: 656622 Color: 12
Size: 343178 Color: 18

Bin 449: 201 of cap free
Amount of items: 2
Items: 
Size: 532226 Color: 14
Size: 467574 Color: 2

Bin 450: 202 of cap free
Amount of items: 2
Items: 
Size: 796317 Color: 12
Size: 203482 Color: 15

Bin 451: 204 of cap free
Amount of items: 2
Items: 
Size: 682059 Color: 12
Size: 317738 Color: 18

Bin 452: 204 of cap free
Amount of items: 2
Items: 
Size: 713230 Color: 18
Size: 286567 Color: 4

Bin 453: 204 of cap free
Amount of items: 2
Items: 
Size: 507486 Color: 15
Size: 492311 Color: 18

Bin 454: 205 of cap free
Amount of items: 2
Items: 
Size: 694789 Color: 17
Size: 305007 Color: 0

Bin 455: 207 of cap free
Amount of items: 2
Items: 
Size: 664151 Color: 2
Size: 335643 Color: 7

Bin 456: 209 of cap free
Amount of items: 3
Items: 
Size: 509778 Color: 8
Size: 319808 Color: 8
Size: 170206 Color: 12

Bin 457: 210 of cap free
Amount of items: 2
Items: 
Size: 628932 Color: 11
Size: 370859 Color: 0

Bin 458: 210 of cap free
Amount of items: 2
Items: 
Size: 611850 Color: 9
Size: 387941 Color: 19

Bin 459: 211 of cap free
Amount of items: 2
Items: 
Size: 619442 Color: 5
Size: 380348 Color: 2

Bin 460: 213 of cap free
Amount of items: 2
Items: 
Size: 596097 Color: 15
Size: 403691 Color: 19

Bin 461: 213 of cap free
Amount of items: 2
Items: 
Size: 661496 Color: 8
Size: 338292 Color: 7

Bin 462: 213 of cap free
Amount of items: 2
Items: 
Size: 773702 Color: 0
Size: 226086 Color: 17

Bin 463: 216 of cap free
Amount of items: 2
Items: 
Size: 594503 Color: 2
Size: 405282 Color: 10

Bin 464: 217 of cap free
Amount of items: 2
Items: 
Size: 585965 Color: 8
Size: 413819 Color: 4

Bin 465: 218 of cap free
Amount of items: 3
Items: 
Size: 368267 Color: 19
Size: 366349 Color: 3
Size: 265167 Color: 10

Bin 466: 219 of cap free
Amount of items: 2
Items: 
Size: 710927 Color: 18
Size: 288855 Color: 7

Bin 467: 219 of cap free
Amount of items: 2
Items: 
Size: 742859 Color: 2
Size: 256923 Color: 7

Bin 468: 219 of cap free
Amount of items: 2
Items: 
Size: 786435 Color: 11
Size: 213347 Color: 8

Bin 469: 219 of cap free
Amount of items: 2
Items: 
Size: 560648 Color: 19
Size: 439134 Color: 8

Bin 470: 222 of cap free
Amount of items: 2
Items: 
Size: 679376 Color: 9
Size: 320403 Color: 2

Bin 471: 225 of cap free
Amount of items: 2
Items: 
Size: 728588 Color: 0
Size: 271188 Color: 9

Bin 472: 226 of cap free
Amount of items: 2
Items: 
Size: 627189 Color: 7
Size: 372586 Color: 5

Bin 473: 226 of cap free
Amount of items: 3
Items: 
Size: 705360 Color: 10
Size: 159293 Color: 8
Size: 135122 Color: 6

Bin 474: 228 of cap free
Amount of items: 2
Items: 
Size: 684205 Color: 4
Size: 315568 Color: 17

Bin 475: 228 of cap free
Amount of items: 2
Items: 
Size: 789282 Color: 5
Size: 210491 Color: 7

Bin 476: 230 of cap free
Amount of items: 2
Items: 
Size: 612243 Color: 0
Size: 387528 Color: 3

Bin 477: 231 of cap free
Amount of items: 2
Items: 
Size: 657652 Color: 2
Size: 342118 Color: 6

Bin 478: 232 of cap free
Amount of items: 2
Items: 
Size: 558581 Color: 12
Size: 441188 Color: 13

Bin 479: 234 of cap free
Amount of items: 2
Items: 
Size: 616501 Color: 15
Size: 383266 Color: 2

Bin 480: 234 of cap free
Amount of items: 2
Items: 
Size: 760116 Color: 7
Size: 239651 Color: 9

Bin 481: 238 of cap free
Amount of items: 2
Items: 
Size: 556204 Color: 9
Size: 443559 Color: 8

Bin 482: 238 of cap free
Amount of items: 2
Items: 
Size: 733891 Color: 8
Size: 265872 Color: 14

Bin 483: 241 of cap free
Amount of items: 2
Items: 
Size: 632496 Color: 4
Size: 367264 Color: 12

Bin 484: 244 of cap free
Amount of items: 2
Items: 
Size: 779861 Color: 7
Size: 219896 Color: 4

Bin 485: 246 of cap free
Amount of items: 2
Items: 
Size: 546566 Color: 7
Size: 453189 Color: 16

Bin 486: 247 of cap free
Amount of items: 2
Items: 
Size: 684855 Color: 0
Size: 314899 Color: 15

Bin 487: 248 of cap free
Amount of items: 2
Items: 
Size: 741899 Color: 9
Size: 257854 Color: 11

Bin 488: 248 of cap free
Amount of items: 2
Items: 
Size: 546128 Color: 7
Size: 453625 Color: 8

Bin 489: 251 of cap free
Amount of items: 2
Items: 
Size: 660364 Color: 16
Size: 339386 Color: 1

Bin 490: 251 of cap free
Amount of items: 2
Items: 
Size: 643844 Color: 3
Size: 355906 Color: 5

Bin 491: 251 of cap free
Amount of items: 2
Items: 
Size: 753610 Color: 19
Size: 246140 Color: 4

Bin 492: 253 of cap free
Amount of items: 2
Items: 
Size: 686727 Color: 17
Size: 313021 Color: 13

Bin 493: 258 of cap free
Amount of items: 2
Items: 
Size: 693208 Color: 0
Size: 306535 Color: 16

Bin 494: 261 of cap free
Amount of items: 2
Items: 
Size: 756762 Color: 13
Size: 242978 Color: 4

Bin 495: 264 of cap free
Amount of items: 2
Items: 
Size: 597186 Color: 5
Size: 402551 Color: 19

Bin 496: 271 of cap free
Amount of items: 2
Items: 
Size: 587846 Color: 3
Size: 411884 Color: 15

Bin 497: 272 of cap free
Amount of items: 2
Items: 
Size: 529905 Color: 9
Size: 469824 Color: 14

Bin 498: 272 of cap free
Amount of items: 2
Items: 
Size: 656962 Color: 17
Size: 342767 Color: 1

Bin 499: 273 of cap free
Amount of items: 2
Items: 
Size: 771055 Color: 13
Size: 228673 Color: 10

Bin 500: 275 of cap free
Amount of items: 2
Items: 
Size: 525237 Color: 13
Size: 474489 Color: 17

Bin 501: 275 of cap free
Amount of items: 2
Items: 
Size: 730309 Color: 6
Size: 269417 Color: 18

Bin 502: 276 of cap free
Amount of items: 2
Items: 
Size: 536928 Color: 15
Size: 462797 Color: 11

Bin 503: 283 of cap free
Amount of items: 2
Items: 
Size: 619425 Color: 18
Size: 380293 Color: 17

Bin 504: 285 of cap free
Amount of items: 2
Items: 
Size: 790138 Color: 14
Size: 209578 Color: 19

Bin 505: 289 of cap free
Amount of items: 2
Items: 
Size: 556888 Color: 15
Size: 442824 Color: 1

Bin 506: 289 of cap free
Amount of items: 2
Items: 
Size: 534497 Color: 8
Size: 465215 Color: 7

Bin 507: 290 of cap free
Amount of items: 2
Items: 
Size: 606877 Color: 7
Size: 392834 Color: 15

Bin 508: 291 of cap free
Amount of items: 2
Items: 
Size: 648076 Color: 8
Size: 351634 Color: 12

Bin 509: 291 of cap free
Amount of items: 2
Items: 
Size: 721743 Color: 3
Size: 277967 Color: 19

Bin 510: 295 of cap free
Amount of items: 2
Items: 
Size: 595046 Color: 12
Size: 404660 Color: 15

Bin 511: 299 of cap free
Amount of items: 2
Items: 
Size: 614312 Color: 16
Size: 385390 Color: 9

Bin 512: 299 of cap free
Amount of items: 2
Items: 
Size: 649899 Color: 18
Size: 349803 Color: 17

Bin 513: 300 of cap free
Amount of items: 3
Items: 
Size: 710552 Color: 16
Size: 155022 Color: 16
Size: 134127 Color: 13

Bin 514: 302 of cap free
Amount of items: 3
Items: 
Size: 416985 Color: 11
Size: 414762 Color: 0
Size: 167952 Color: 6

Bin 515: 305 of cap free
Amount of items: 3
Items: 
Size: 539048 Color: 16
Size: 298142 Color: 9
Size: 162506 Color: 4

Bin 516: 307 of cap free
Amount of items: 2
Items: 
Size: 504961 Color: 12
Size: 494733 Color: 3

Bin 517: 308 of cap free
Amount of items: 2
Items: 
Size: 763591 Color: 10
Size: 236102 Color: 19

Bin 518: 311 of cap free
Amount of items: 2
Items: 
Size: 641172 Color: 1
Size: 358518 Color: 4

Bin 519: 312 of cap free
Amount of items: 2
Items: 
Size: 733103 Color: 6
Size: 266586 Color: 15

Bin 520: 312 of cap free
Amount of items: 2
Items: 
Size: 614625 Color: 11
Size: 385064 Color: 9

Bin 521: 313 of cap free
Amount of items: 2
Items: 
Size: 552122 Color: 3
Size: 447566 Color: 14

Bin 522: 315 of cap free
Amount of items: 3
Items: 
Size: 613735 Color: 10
Size: 196718 Color: 15
Size: 189233 Color: 2

Bin 523: 315 of cap free
Amount of items: 2
Items: 
Size: 511493 Color: 13
Size: 488193 Color: 16

Bin 524: 315 of cap free
Amount of items: 2
Items: 
Size: 640611 Color: 3
Size: 359075 Color: 9

Bin 525: 318 of cap free
Amount of items: 2
Items: 
Size: 521172 Color: 16
Size: 478511 Color: 19

Bin 526: 325 of cap free
Amount of items: 2
Items: 
Size: 763576 Color: 3
Size: 236100 Color: 5

Bin 527: 326 of cap free
Amount of items: 2
Items: 
Size: 624380 Color: 9
Size: 375295 Color: 12

Bin 528: 330 of cap free
Amount of items: 2
Items: 
Size: 792488 Color: 3
Size: 207183 Color: 12

Bin 529: 332 of cap free
Amount of items: 2
Items: 
Size: 747337 Color: 13
Size: 252332 Color: 8

Bin 530: 333 of cap free
Amount of items: 2
Items: 
Size: 568958 Color: 15
Size: 430710 Color: 17

Bin 531: 335 of cap free
Amount of items: 2
Items: 
Size: 589992 Color: 7
Size: 409674 Color: 1

Bin 532: 336 of cap free
Amount of items: 2
Items: 
Size: 516352 Color: 6
Size: 483313 Color: 18

Bin 533: 337 of cap free
Amount of items: 2
Items: 
Size: 753977 Color: 19
Size: 245687 Color: 18

Bin 534: 346 of cap free
Amount of items: 2
Items: 
Size: 563536 Color: 12
Size: 436119 Color: 18

Bin 535: 347 of cap free
Amount of items: 2
Items: 
Size: 527060 Color: 2
Size: 472594 Color: 14

Bin 536: 350 of cap free
Amount of items: 2
Items: 
Size: 741411 Color: 14
Size: 258240 Color: 15

Bin 537: 351 of cap free
Amount of items: 2
Items: 
Size: 667843 Color: 2
Size: 331807 Color: 13

Bin 538: 352 of cap free
Amount of items: 2
Items: 
Size: 757105 Color: 8
Size: 242544 Color: 9

Bin 539: 356 of cap free
Amount of items: 2
Items: 
Size: 719534 Color: 17
Size: 280111 Color: 7

Bin 540: 357 of cap free
Amount of items: 2
Items: 
Size: 696156 Color: 15
Size: 303488 Color: 14

Bin 541: 357 of cap free
Amount of items: 2
Items: 
Size: 793984 Color: 3
Size: 205660 Color: 13

Bin 542: 357 of cap free
Amount of items: 2
Items: 
Size: 798017 Color: 19
Size: 201627 Color: 1

Bin 543: 367 of cap free
Amount of items: 2
Items: 
Size: 550259 Color: 12
Size: 449375 Color: 14

Bin 544: 367 of cap free
Amount of items: 2
Items: 
Size: 540046 Color: 9
Size: 459588 Color: 2

Bin 545: 369 of cap free
Amount of items: 2
Items: 
Size: 522735 Color: 13
Size: 476897 Color: 19

Bin 546: 370 of cap free
Amount of items: 2
Items: 
Size: 774123 Color: 7
Size: 225508 Color: 13

Bin 547: 374 of cap free
Amount of items: 2
Items: 
Size: 758265 Color: 5
Size: 241362 Color: 12

Bin 548: 375 of cap free
Amount of items: 2
Items: 
Size: 657569 Color: 14
Size: 342057 Color: 9

Bin 549: 376 of cap free
Amount of items: 2
Items: 
Size: 713845 Color: 12
Size: 285780 Color: 19

Bin 550: 381 of cap free
Amount of items: 2
Items: 
Size: 695772 Color: 14
Size: 303848 Color: 17

Bin 551: 383 of cap free
Amount of items: 2
Items: 
Size: 637220 Color: 3
Size: 362398 Color: 13

Bin 552: 390 of cap free
Amount of items: 2
Items: 
Size: 562653 Color: 5
Size: 436958 Color: 3

Bin 553: 391 of cap free
Amount of items: 2
Items: 
Size: 633832 Color: 18
Size: 365778 Color: 10

Bin 554: 394 of cap free
Amount of items: 2
Items: 
Size: 624918 Color: 3
Size: 374689 Color: 4

Bin 555: 398 of cap free
Amount of items: 2
Items: 
Size: 728836 Color: 8
Size: 270767 Color: 15

Bin 556: 400 of cap free
Amount of items: 2
Items: 
Size: 560051 Color: 2
Size: 439550 Color: 15

Bin 557: 401 of cap free
Amount of items: 2
Items: 
Size: 555480 Color: 2
Size: 444120 Color: 8

Bin 558: 404 of cap free
Amount of items: 3
Items: 
Size: 573620 Color: 8
Size: 277327 Color: 8
Size: 148650 Color: 17

Bin 559: 411 of cap free
Amount of items: 2
Items: 
Size: 736140 Color: 8
Size: 263450 Color: 11

Bin 560: 415 of cap free
Amount of items: 2
Items: 
Size: 549829 Color: 13
Size: 449757 Color: 18

Bin 561: 415 of cap free
Amount of items: 2
Items: 
Size: 783852 Color: 17
Size: 215734 Color: 5

Bin 562: 416 of cap free
Amount of items: 2
Items: 
Size: 716187 Color: 9
Size: 283398 Color: 8

Bin 563: 417 of cap free
Amount of items: 2
Items: 
Size: 540705 Color: 10
Size: 458879 Color: 3

Bin 564: 418 of cap free
Amount of items: 2
Items: 
Size: 568439 Color: 10
Size: 431144 Color: 7

Bin 565: 424 of cap free
Amount of items: 2
Items: 
Size: 731068 Color: 10
Size: 268509 Color: 2

Bin 566: 427 of cap free
Amount of items: 2
Items: 
Size: 658349 Color: 19
Size: 341225 Color: 12

Bin 567: 427 of cap free
Amount of items: 2
Items: 
Size: 760058 Color: 13
Size: 239516 Color: 9

Bin 568: 429 of cap free
Amount of items: 2
Items: 
Size: 525198 Color: 6
Size: 474374 Color: 3

Bin 569: 429 of cap free
Amount of items: 2
Items: 
Size: 661286 Color: 3
Size: 338286 Color: 2

Bin 570: 432 of cap free
Amount of items: 2
Items: 
Size: 701540 Color: 19
Size: 298029 Color: 15

Bin 571: 433 of cap free
Amount of items: 2
Items: 
Size: 764041 Color: 15
Size: 235527 Color: 19

Bin 572: 433 of cap free
Amount of items: 2
Items: 
Size: 653037 Color: 14
Size: 346531 Color: 2

Bin 573: 436 of cap free
Amount of items: 2
Items: 
Size: 768290 Color: 11
Size: 231275 Color: 18

Bin 574: 436 of cap free
Amount of items: 2
Items: 
Size: 729604 Color: 14
Size: 269961 Color: 3

Bin 575: 437 of cap free
Amount of items: 2
Items: 
Size: 653660 Color: 8
Size: 345904 Color: 6

Bin 576: 441 of cap free
Amount of items: 2
Items: 
Size: 512127 Color: 7
Size: 487433 Color: 4

Bin 577: 443 of cap free
Amount of items: 2
Items: 
Size: 562645 Color: 9
Size: 436913 Color: 12

Bin 578: 445 of cap free
Amount of items: 2
Items: 
Size: 720895 Color: 5
Size: 278661 Color: 17

Bin 579: 450 of cap free
Amount of items: 2
Items: 
Size: 726903 Color: 1
Size: 272648 Color: 12

Bin 580: 452 of cap free
Amount of items: 2
Items: 
Size: 719477 Color: 15
Size: 280072 Color: 4

Bin 581: 455 of cap free
Amount of items: 2
Items: 
Size: 581086 Color: 11
Size: 418460 Color: 9

Bin 582: 457 of cap free
Amount of items: 2
Items: 
Size: 653023 Color: 9
Size: 346521 Color: 8

Bin 583: 457 of cap free
Amount of items: 2
Items: 
Size: 570565 Color: 8
Size: 428979 Color: 11

Bin 584: 459 of cap free
Amount of items: 2
Items: 
Size: 685278 Color: 16
Size: 314264 Color: 13

Bin 585: 465 of cap free
Amount of items: 2
Items: 
Size: 752320 Color: 14
Size: 247216 Color: 2

Bin 586: 466 of cap free
Amount of items: 2
Items: 
Size: 672828 Color: 14
Size: 326707 Color: 19

Bin 587: 467 of cap free
Amount of items: 2
Items: 
Size: 606398 Color: 15
Size: 393136 Color: 18

Bin 588: 468 of cap free
Amount of items: 2
Items: 
Size: 548242 Color: 19
Size: 451291 Color: 0

Bin 589: 468 of cap free
Amount of items: 2
Items: 
Size: 656363 Color: 19
Size: 343170 Color: 11

Bin 590: 469 of cap free
Amount of items: 2
Items: 
Size: 688927 Color: 7
Size: 310605 Color: 10

Bin 591: 471 of cap free
Amount of items: 2
Items: 
Size: 558508 Color: 19
Size: 441022 Color: 12

Bin 592: 473 of cap free
Amount of items: 2
Items: 
Size: 508908 Color: 1
Size: 490620 Color: 16

Bin 593: 476 of cap free
Amount of items: 2
Items: 
Size: 770402 Color: 2
Size: 229123 Color: 9

Bin 594: 481 of cap free
Amount of items: 2
Items: 
Size: 634706 Color: 17
Size: 364814 Color: 15

Bin 595: 482 of cap free
Amount of items: 2
Items: 
Size: 630016 Color: 14
Size: 369503 Color: 18

Bin 596: 484 of cap free
Amount of items: 2
Items: 
Size: 744939 Color: 13
Size: 254578 Color: 19

Bin 597: 488 of cap free
Amount of items: 2
Items: 
Size: 624293 Color: 11
Size: 375220 Color: 15

Bin 598: 488 of cap free
Amount of items: 2
Items: 
Size: 514718 Color: 10
Size: 484795 Color: 4

Bin 599: 493 of cap free
Amount of items: 2
Items: 
Size: 621014 Color: 15
Size: 378494 Color: 19

Bin 600: 501 of cap free
Amount of items: 2
Items: 
Size: 757087 Color: 3
Size: 242413 Color: 19

Bin 601: 506 of cap free
Amount of items: 2
Items: 
Size: 561527 Color: 1
Size: 437968 Color: 8

Bin 602: 512 of cap free
Amount of items: 2
Items: 
Size: 673341 Color: 17
Size: 326148 Color: 9

Bin 603: 524 of cap free
Amount of items: 2
Items: 
Size: 574103 Color: 6
Size: 425374 Color: 16

Bin 604: 525 of cap free
Amount of items: 2
Items: 
Size: 731026 Color: 17
Size: 268450 Color: 14

Bin 605: 526 of cap free
Amount of items: 2
Items: 
Size: 797999 Color: 2
Size: 201476 Color: 18

Bin 606: 527 of cap free
Amount of items: 2
Items: 
Size: 688921 Color: 19
Size: 310553 Color: 6

Bin 607: 528 of cap free
Amount of items: 2
Items: 
Size: 592459 Color: 5
Size: 407014 Color: 16

Bin 608: 529 of cap free
Amount of items: 2
Items: 
Size: 513302 Color: 18
Size: 486170 Color: 6

Bin 609: 533 of cap free
Amount of items: 2
Items: 
Size: 718420 Color: 8
Size: 281048 Color: 14

Bin 610: 535 of cap free
Amount of items: 2
Items: 
Size: 728342 Color: 3
Size: 271124 Color: 8

Bin 611: 541 of cap free
Amount of items: 2
Items: 
Size: 774548 Color: 4
Size: 224912 Color: 12

Bin 612: 541 of cap free
Amount of items: 2
Items: 
Size: 650339 Color: 1
Size: 349121 Color: 15

Bin 613: 546 of cap free
Amount of items: 2
Items: 
Size: 547013 Color: 6
Size: 452442 Color: 9

Bin 614: 551 of cap free
Amount of items: 2
Items: 
Size: 799413 Color: 5
Size: 200037 Color: 0

Bin 615: 560 of cap free
Amount of items: 2
Items: 
Size: 565377 Color: 2
Size: 434064 Color: 0

Bin 616: 561 of cap free
Amount of items: 2
Items: 
Size: 660324 Color: 11
Size: 339116 Color: 3

Bin 617: 562 of cap free
Amount of items: 2
Items: 
Size: 758740 Color: 6
Size: 240699 Color: 1

Bin 618: 565 of cap free
Amount of items: 2
Items: 
Size: 499839 Color: 17
Size: 499597 Color: 1

Bin 619: 565 of cap free
Amount of items: 2
Items: 
Size: 645766 Color: 3
Size: 353670 Color: 9

Bin 620: 571 of cap free
Amount of items: 2
Items: 
Size: 767546 Color: 0
Size: 231884 Color: 7

Bin 621: 584 of cap free
Amount of items: 2
Items: 
Size: 506260 Color: 8
Size: 493157 Color: 3

Bin 622: 585 of cap free
Amount of items: 2
Items: 
Size: 595432 Color: 14
Size: 403984 Color: 17

Bin 623: 590 of cap free
Amount of items: 2
Items: 
Size: 560034 Color: 4
Size: 439377 Color: 13

Bin 624: 593 of cap free
Amount of items: 2
Items: 
Size: 581628 Color: 14
Size: 417780 Color: 17

Bin 625: 596 of cap free
Amount of items: 2
Items: 
Size: 796084 Color: 5
Size: 203321 Color: 9

Bin 626: 600 of cap free
Amount of items: 2
Items: 
Size: 749175 Color: 13
Size: 250226 Color: 18

Bin 627: 606 of cap free
Amount of items: 2
Items: 
Size: 523145 Color: 0
Size: 476250 Color: 4

Bin 628: 609 of cap free
Amount of items: 2
Items: 
Size: 584239 Color: 0
Size: 415153 Color: 12

Bin 629: 609 of cap free
Amount of items: 2
Items: 
Size: 578786 Color: 15
Size: 420606 Color: 0

Bin 630: 610 of cap free
Amount of items: 2
Items: 
Size: 555451 Color: 17
Size: 443940 Color: 10

Bin 631: 619 of cap free
Amount of items: 2
Items: 
Size: 692262 Color: 6
Size: 307120 Color: 15

Bin 632: 619 of cap free
Amount of items: 2
Items: 
Size: 707026 Color: 9
Size: 292356 Color: 18

Bin 633: 619 of cap free
Amount of items: 2
Items: 
Size: 597176 Color: 13
Size: 402206 Color: 15

Bin 634: 624 of cap free
Amount of items: 2
Items: 
Size: 660302 Color: 4
Size: 339075 Color: 12

Bin 635: 631 of cap free
Amount of items: 2
Items: 
Size: 620096 Color: 11
Size: 379274 Color: 18

Bin 636: 639 of cap free
Amount of items: 2
Items: 
Size: 724978 Color: 11
Size: 274384 Color: 1

Bin 637: 642 of cap free
Amount of items: 2
Items: 
Size: 739353 Color: 14
Size: 260006 Color: 12

Bin 638: 650 of cap free
Amount of items: 2
Items: 
Size: 780644 Color: 10
Size: 218707 Color: 18

Bin 639: 656 of cap free
Amount of items: 2
Items: 
Size: 524062 Color: 12
Size: 475283 Color: 8

Bin 640: 675 of cap free
Amount of items: 2
Items: 
Size: 743291 Color: 19
Size: 256035 Color: 8

Bin 641: 677 of cap free
Amount of items: 2
Items: 
Size: 518447 Color: 7
Size: 480877 Color: 10

Bin 642: 689 of cap free
Amount of items: 2
Items: 
Size: 708752 Color: 11
Size: 290560 Color: 4

Bin 643: 690 of cap free
Amount of items: 2
Items: 
Size: 652283 Color: 10
Size: 347028 Color: 18

Bin 644: 690 of cap free
Amount of items: 2
Items: 
Size: 687782 Color: 8
Size: 311529 Color: 15

Bin 645: 693 of cap free
Amount of items: 2
Items: 
Size: 736064 Color: 12
Size: 263244 Color: 1

Bin 646: 703 of cap free
Amount of items: 2
Items: 
Size: 683133 Color: 7
Size: 316165 Color: 19

Bin 647: 705 of cap free
Amount of items: 2
Items: 
Size: 510270 Color: 4
Size: 489026 Color: 17

Bin 648: 720 of cap free
Amount of items: 2
Items: 
Size: 501734 Color: 5
Size: 497547 Color: 8

Bin 649: 730 of cap free
Amount of items: 2
Items: 
Size: 635197 Color: 3
Size: 364074 Color: 6

Bin 650: 730 of cap free
Amount of items: 2
Items: 
Size: 604105 Color: 13
Size: 395166 Color: 7

Bin 651: 752 of cap free
Amount of items: 2
Items: 
Size: 658078 Color: 10
Size: 341171 Color: 6

Bin 652: 770 of cap free
Amount of items: 2
Items: 
Size: 559091 Color: 2
Size: 440140 Color: 13

Bin 653: 776 of cap free
Amount of items: 2
Items: 
Size: 509464 Color: 1
Size: 489761 Color: 8

Bin 654: 776 of cap free
Amount of items: 2
Items: 
Size: 670760 Color: 16
Size: 328465 Color: 17

Bin 655: 780 of cap free
Amount of items: 2
Items: 
Size: 695768 Color: 11
Size: 303453 Color: 0

Bin 656: 782 of cap free
Amount of items: 2
Items: 
Size: 781447 Color: 9
Size: 217772 Color: 5

Bin 657: 796 of cap free
Amount of items: 2
Items: 
Size: 586270 Color: 12
Size: 412935 Color: 15

Bin 658: 797 of cap free
Amount of items: 2
Items: 
Size: 544656 Color: 19
Size: 454548 Color: 6

Bin 659: 832 of cap free
Amount of items: 2
Items: 
Size: 670717 Color: 11
Size: 328452 Color: 1

Bin 660: 839 of cap free
Amount of items: 2
Items: 
Size: 639751 Color: 10
Size: 359411 Color: 5

Bin 661: 841 of cap free
Amount of items: 2
Items: 
Size: 703170 Color: 11
Size: 295990 Color: 3

Bin 662: 844 of cap free
Amount of items: 2
Items: 
Size: 654561 Color: 14
Size: 344596 Color: 2

Bin 663: 857 of cap free
Amount of items: 2
Items: 
Size: 775387 Color: 7
Size: 223757 Color: 13

Bin 664: 864 of cap free
Amount of items: 2
Items: 
Size: 731962 Color: 4
Size: 267175 Color: 8

Bin 665: 870 of cap free
Amount of items: 2
Items: 
Size: 520938 Color: 6
Size: 478193 Color: 2

Bin 666: 872 of cap free
Amount of items: 2
Items: 
Size: 718380 Color: 13
Size: 280749 Color: 10

Bin 667: 881 of cap free
Amount of items: 2
Items: 
Size: 705755 Color: 17
Size: 293365 Color: 10

Bin 668: 881 of cap free
Amount of items: 2
Items: 
Size: 773040 Color: 10
Size: 226080 Color: 7

Bin 669: 884 of cap free
Amount of items: 2
Items: 
Size: 514531 Color: 9
Size: 484586 Color: 1

Bin 670: 887 of cap free
Amount of items: 2
Items: 
Size: 747300 Color: 0
Size: 251814 Color: 3

Bin 671: 892 of cap free
Amount of items: 2
Items: 
Size: 576697 Color: 9
Size: 422412 Color: 16

Bin 672: 901 of cap free
Amount of items: 2
Items: 
Size: 706903 Color: 17
Size: 292197 Color: 0

Bin 673: 902 of cap free
Amount of items: 2
Items: 
Size: 691314 Color: 16
Size: 307785 Color: 12

Bin 674: 902 of cap free
Amount of items: 2
Items: 
Size: 544647 Color: 7
Size: 454452 Color: 0

Bin 675: 903 of cap free
Amount of items: 2
Items: 
Size: 613291 Color: 11
Size: 385807 Color: 19

Bin 676: 906 of cap free
Amount of items: 2
Items: 
Size: 739198 Color: 13
Size: 259897 Color: 1

Bin 677: 921 of cap free
Amount of items: 2
Items: 
Size: 794734 Color: 9
Size: 204346 Color: 13

Bin 678: 922 of cap free
Amount of items: 2
Items: 
Size: 676322 Color: 1
Size: 322757 Color: 5

Bin 679: 927 of cap free
Amount of items: 2
Items: 
Size: 668230 Color: 17
Size: 330844 Color: 12

Bin 680: 939 of cap free
Amount of items: 2
Items: 
Size: 781368 Color: 3
Size: 217694 Color: 2

Bin 681: 958 of cap free
Amount of items: 2
Items: 
Size: 559977 Color: 9
Size: 439066 Color: 10

Bin 682: 962 of cap free
Amount of items: 2
Items: 
Size: 708650 Color: 3
Size: 290389 Color: 4

Bin 683: 969 of cap free
Amount of items: 2
Items: 
Size: 693633 Color: 1
Size: 305399 Color: 6

Bin 684: 989 of cap free
Amount of items: 2
Items: 
Size: 700996 Color: 19
Size: 298016 Color: 17

Bin 685: 998 of cap free
Amount of items: 2
Items: 
Size: 745883 Color: 5
Size: 253120 Color: 2

Bin 686: 1008 of cap free
Amount of items: 2
Items: 
Size: 514413 Color: 7
Size: 484580 Color: 6

Bin 687: 1011 of cap free
Amount of items: 2
Items: 
Size: 596831 Color: 8
Size: 402159 Color: 9

Bin 688: 1024 of cap free
Amount of items: 2
Items: 
Size: 532676 Color: 0
Size: 466301 Color: 17

Bin 689: 1026 of cap free
Amount of items: 2
Items: 
Size: 685049 Color: 15
Size: 313926 Color: 8

Bin 690: 1043 of cap free
Amount of items: 2
Items: 
Size: 504431 Color: 7
Size: 494527 Color: 19

Bin 691: 1064 of cap free
Amount of items: 2
Items: 
Size: 729581 Color: 7
Size: 269356 Color: 10

Bin 692: 1083 of cap free
Amount of items: 2
Items: 
Size: 605947 Color: 15
Size: 392971 Color: 7

Bin 693: 1094 of cap free
Amount of items: 2
Items: 
Size: 783395 Color: 19
Size: 215512 Color: 4

Bin 694: 1120 of cap free
Amount of items: 2
Items: 
Size: 626336 Color: 0
Size: 372545 Color: 4

Bin 695: 1136 of cap free
Amount of items: 2
Items: 
Size: 771687 Color: 9
Size: 227178 Color: 5

Bin 696: 1142 of cap free
Amount of items: 2
Items: 
Size: 578345 Color: 16
Size: 420514 Color: 6

Bin 697: 1143 of cap free
Amount of items: 2
Items: 
Size: 757593 Color: 0
Size: 241265 Color: 16

Bin 698: 1167 of cap free
Amount of items: 2
Items: 
Size: 749139 Color: 8
Size: 249695 Color: 16

Bin 699: 1176 of cap free
Amount of items: 2
Items: 
Size: 660002 Color: 2
Size: 338823 Color: 10

Bin 700: 1184 of cap free
Amount of items: 2
Items: 
Size: 724434 Color: 4
Size: 274383 Color: 13

Bin 701: 1193 of cap free
Amount of items: 2
Items: 
Size: 588262 Color: 10
Size: 410546 Color: 6

Bin 702: 1198 of cap free
Amount of items: 2
Items: 
Size: 636522 Color: 18
Size: 362281 Color: 7

Bin 703: 1209 of cap free
Amount of items: 2
Items: 
Size: 620828 Color: 9
Size: 377964 Color: 0

Bin 704: 1210 of cap free
Amount of items: 2
Items: 
Size: 791675 Color: 5
Size: 207116 Color: 19

Bin 705: 1215 of cap free
Amount of items: 2
Items: 
Size: 783388 Color: 10
Size: 215398 Color: 3

Bin 706: 1218 of cap free
Amount of items: 2
Items: 
Size: 603730 Color: 17
Size: 395053 Color: 16

Bin 707: 1228 of cap free
Amount of items: 2
Items: 
Size: 759990 Color: 0
Size: 238783 Color: 7

Bin 708: 1230 of cap free
Amount of items: 2
Items: 
Size: 551715 Color: 15
Size: 447056 Color: 7

Bin 709: 1243 of cap free
Amount of items: 2
Items: 
Size: 592256 Color: 0
Size: 406502 Color: 16

Bin 710: 1291 of cap free
Amount of items: 2
Items: 
Size: 559884 Color: 9
Size: 438826 Color: 16

Bin 711: 1324 of cap free
Amount of items: 2
Items: 
Size: 643692 Color: 16
Size: 354985 Color: 5

Bin 712: 1332 of cap free
Amount of items: 2
Items: 
Size: 734267 Color: 0
Size: 264402 Color: 1

Bin 713: 1334 of cap free
Amount of items: 2
Items: 
Size: 750943 Color: 6
Size: 247724 Color: 5

Bin 714: 1335 of cap free
Amount of items: 2
Items: 
Size: 608862 Color: 13
Size: 389804 Color: 9

Bin 715: 1363 of cap free
Amount of items: 2
Items: 
Size: 516322 Color: 17
Size: 482316 Color: 18

Bin 716: 1372 of cap free
Amount of items: 2
Items: 
Size: 682542 Color: 12
Size: 316087 Color: 18

Bin 717: 1379 of cap free
Amount of items: 2
Items: 
Size: 650335 Color: 11
Size: 348287 Color: 8

Bin 718: 1385 of cap free
Amount of items: 2
Items: 
Size: 539795 Color: 5
Size: 458821 Color: 15

Bin 719: 1399 of cap free
Amount of items: 2
Items: 
Size: 698551 Color: 1
Size: 300051 Color: 4

Bin 720: 1401 of cap free
Amount of items: 2
Items: 
Size: 693615 Color: 17
Size: 304985 Color: 8

Bin 721: 1418 of cap free
Amount of items: 2
Items: 
Size: 706885 Color: 4
Size: 291698 Color: 7

Bin 722: 1439 of cap free
Amount of items: 2
Items: 
Size: 578258 Color: 0
Size: 420304 Color: 10

Bin 723: 1468 of cap free
Amount of items: 2
Items: 
Size: 698530 Color: 2
Size: 300003 Color: 4

Bin 724: 1469 of cap free
Amount of items: 2
Items: 
Size: 744199 Color: 9
Size: 254333 Color: 0

Bin 725: 1472 of cap free
Amount of items: 2
Items: 
Size: 616354 Color: 1
Size: 382175 Color: 3

Bin 726: 1483 of cap free
Amount of items: 2
Items: 
Size: 568323 Color: 16
Size: 430195 Color: 12

Bin 727: 1498 of cap free
Amount of items: 2
Items: 
Size: 712241 Color: 11
Size: 286262 Color: 16

Bin 728: 1513 of cap free
Amount of items: 2
Items: 
Size: 583747 Color: 2
Size: 414741 Color: 11

Bin 729: 1514 of cap free
Amount of items: 2
Items: 
Size: 576615 Color: 12
Size: 421872 Color: 15

Bin 730: 1516 of cap free
Amount of items: 2
Items: 
Size: 783051 Color: 19
Size: 215434 Color: 10

Bin 731: 1537 of cap free
Amount of items: 2
Items: 
Size: 596510 Color: 14
Size: 401954 Color: 13

Bin 732: 1574 of cap free
Amount of items: 2
Items: 
Size: 527434 Color: 8
Size: 470993 Color: 12

Bin 733: 1610 of cap free
Amount of items: 2
Items: 
Size: 678218 Color: 13
Size: 320173 Color: 4

Bin 734: 1654 of cap free
Amount of items: 2
Items: 
Size: 506219 Color: 13
Size: 492128 Color: 0

Bin 735: 1656 of cap free
Amount of items: 2
Items: 
Size: 729286 Color: 19
Size: 269059 Color: 13

Bin 736: 1665 of cap free
Amount of items: 2
Items: 
Size: 710080 Color: 8
Size: 288256 Color: 6

Bin 737: 1666 of cap free
Amount of items: 2
Items: 
Size: 539731 Color: 19
Size: 458604 Color: 6

Bin 738: 1700 of cap free
Amount of items: 2
Items: 
Size: 501207 Color: 2
Size: 497094 Color: 10

Bin 739: 1755 of cap free
Amount of items: 2
Items: 
Size: 772948 Color: 12
Size: 225298 Color: 4

Bin 740: 1759 of cap free
Amount of items: 2
Items: 
Size: 629468 Color: 2
Size: 368774 Color: 9

Bin 741: 1759 of cap free
Amount of items: 2
Items: 
Size: 549249 Color: 11
Size: 448993 Color: 7

Bin 742: 1762 of cap free
Amount of items: 2
Items: 
Size: 601030 Color: 16
Size: 397209 Color: 4

Bin 743: 1793 of cap free
Amount of items: 2
Items: 
Size: 643651 Color: 9
Size: 354557 Color: 5

Bin 744: 1816 of cap free
Amount of items: 2
Items: 
Size: 626143 Color: 11
Size: 372042 Color: 15

Bin 745: 1838 of cap free
Amount of items: 2
Items: 
Size: 759838 Color: 0
Size: 238325 Color: 19

Bin 746: 1919 of cap free
Amount of items: 2
Items: 
Size: 659981 Color: 6
Size: 338101 Color: 2

Bin 747: 1938 of cap free
Amount of items: 2
Items: 
Size: 798571 Color: 14
Size: 199492 Color: 7

Bin 748: 1967 of cap free
Amount of items: 2
Items: 
Size: 769462 Color: 11
Size: 228572 Color: 3

Bin 749: 1990 of cap free
Amount of items: 2
Items: 
Size: 527052 Color: 8
Size: 470959 Color: 1

Bin 750: 2072 of cap free
Amount of items: 2
Items: 
Size: 667297 Color: 4
Size: 330632 Color: 15

Bin 751: 2078 of cap free
Amount of items: 2
Items: 
Size: 798968 Color: 7
Size: 198955 Color: 13

Bin 752: 2079 of cap free
Amount of items: 2
Items: 
Size: 638926 Color: 11
Size: 358996 Color: 18

Bin 753: 2085 of cap free
Amount of items: 2
Items: 
Size: 603053 Color: 10
Size: 394863 Color: 7

Bin 754: 2103 of cap free
Amount of items: 2
Items: 
Size: 650135 Color: 17
Size: 347763 Color: 16

Bin 755: 2110 of cap free
Amount of items: 2
Items: 
Size: 573512 Color: 19
Size: 424379 Color: 15

Bin 756: 2139 of cap free
Amount of items: 2
Items: 
Size: 565002 Color: 10
Size: 432860 Color: 3

Bin 757: 2143 of cap free
Amount of items: 2
Items: 
Size: 548953 Color: 8
Size: 448905 Color: 15

Bin 758: 2144 of cap free
Amount of items: 2
Items: 
Size: 501058 Color: 15
Size: 496799 Color: 16

Bin 759: 2148 of cap free
Amount of items: 2
Items: 
Size: 672398 Color: 9
Size: 325455 Color: 11

Bin 760: 2155 of cap free
Amount of items: 2
Items: 
Size: 638921 Color: 14
Size: 358925 Color: 15

Bin 761: 2174 of cap free
Amount of items: 2
Items: 
Size: 687338 Color: 12
Size: 310489 Color: 4

Bin 762: 2179 of cap free
Amount of items: 2
Items: 
Size: 644205 Color: 5
Size: 353617 Color: 3

Bin 763: 2211 of cap free
Amount of items: 2
Items: 
Size: 793737 Color: 7
Size: 204053 Color: 8

Bin 764: 2239 of cap free
Amount of items: 2
Items: 
Size: 501002 Color: 2
Size: 496760 Color: 15

Bin 765: 2252 of cap free
Amount of items: 2
Items: 
Size: 608058 Color: 1
Size: 389691 Color: 15

Bin 766: 2299 of cap free
Amount of items: 2
Items: 
Size: 766525 Color: 9
Size: 231177 Color: 5

Bin 767: 2330 of cap free
Amount of items: 2
Items: 
Size: 687249 Color: 16
Size: 310422 Color: 10

Bin 768: 2351 of cap free
Amount of items: 2
Items: 
Size: 734323 Color: 1
Size: 263327 Color: 12

Bin 769: 2413 of cap free
Amount of items: 2
Items: 
Size: 766486 Color: 10
Size: 231102 Color: 11

Bin 770: 2432 of cap free
Amount of items: 2
Items: 
Size: 682401 Color: 11
Size: 315168 Color: 5

Bin 771: 2499 of cap free
Amount of items: 2
Items: 
Size: 564775 Color: 19
Size: 432727 Color: 5

Bin 772: 2543 of cap free
Amount of items: 2
Items: 
Size: 548857 Color: 16
Size: 448601 Color: 5

Bin 773: 2564 of cap free
Amount of items: 2
Items: 
Size: 672221 Color: 7
Size: 325216 Color: 5

Bin 774: 2585 of cap free
Amount of items: 2
Items: 
Size: 759809 Color: 12
Size: 237607 Color: 13

Bin 775: 2607 of cap free
Amount of items: 2
Items: 
Size: 583217 Color: 4
Size: 414177 Color: 17

Bin 776: 2654 of cap free
Amount of items: 2
Items: 
Size: 750894 Color: 18
Size: 246453 Color: 10

Bin 777: 2723 of cap free
Amount of items: 2
Items: 
Size: 734186 Color: 11
Size: 263092 Color: 15

Bin 778: 2748 of cap free
Amount of items: 2
Items: 
Size: 508699 Color: 8
Size: 488554 Color: 15

Bin 779: 2924 of cap free
Amount of items: 2
Items: 
Size: 766403 Color: 7
Size: 230674 Color: 12

Bin 780: 2994 of cap free
Amount of items: 2
Items: 
Size: 523995 Color: 8
Size: 473012 Color: 15

Bin 781: 3094 of cap free
Amount of items: 2
Items: 
Size: 659049 Color: 5
Size: 337858 Color: 17

Bin 782: 3233 of cap free
Amount of items: 2
Items: 
Size: 638733 Color: 13
Size: 358035 Color: 5

Bin 783: 3253 of cap free
Amount of items: 2
Items: 
Size: 564170 Color: 4
Size: 432578 Color: 1

Bin 784: 3346 of cap free
Amount of items: 2
Items: 
Size: 793442 Color: 16
Size: 203213 Color: 2

Bin 785: 3379 of cap free
Amount of items: 2
Items: 
Size: 607612 Color: 13
Size: 389010 Color: 15

Bin 786: 3384 of cap free
Amount of items: 2
Items: 
Size: 572534 Color: 16
Size: 424083 Color: 7

Bin 787: 3415 of cap free
Amount of items: 2
Items: 
Size: 715943 Color: 9
Size: 280643 Color: 4

Bin 788: 3556 of cap free
Amount of items: 2
Items: 
Size: 759616 Color: 17
Size: 236829 Color: 13

Bin 789: 3567 of cap free
Amount of items: 2
Items: 
Size: 500756 Color: 6
Size: 495678 Color: 7

Bin 790: 3699 of cap free
Amount of items: 2
Items: 
Size: 781303 Color: 14
Size: 214999 Color: 18

Bin 791: 3706 of cap free
Amount of items: 2
Items: 
Size: 638213 Color: 5
Size: 358082 Color: 1

Bin 792: 3744 of cap free
Amount of items: 2
Items: 
Size: 759526 Color: 7
Size: 236731 Color: 16

Bin 793: 3759 of cap free
Amount of items: 2
Items: 
Size: 743268 Color: 6
Size: 252974 Color: 12

Bin 794: 3760 of cap free
Amount of items: 2
Items: 
Size: 583467 Color: 17
Size: 412774 Color: 3

Bin 795: 3763 of cap free
Amount of items: 3
Items: 
Size: 765599 Color: 17
Size: 127671 Color: 14
Size: 102968 Color: 3

Bin 796: 3821 of cap free
Amount of items: 2
Items: 
Size: 642907 Color: 8
Size: 353273 Color: 16

Bin 797: 3857 of cap free
Amount of items: 2
Items: 
Size: 607437 Color: 6
Size: 388707 Color: 10

Bin 798: 3978 of cap free
Amount of items: 2
Items: 
Size: 631997 Color: 15
Size: 364026 Color: 6

Bin 799: 4047 of cap free
Amount of items: 2
Items: 
Size: 558082 Color: 0
Size: 437872 Color: 5

Bin 800: 4056 of cap free
Amount of items: 2
Items: 
Size: 531482 Color: 16
Size: 464463 Color: 6

Bin 801: 4111 of cap free
Amount of items: 2
Items: 
Size: 759381 Color: 3
Size: 236509 Color: 1

Bin 802: 4196 of cap free
Amount of items: 2
Items: 
Size: 733636 Color: 9
Size: 262169 Color: 13

Bin 803: 4265 of cap free
Amount of items: 2
Items: 
Size: 642675 Color: 13
Size: 353061 Color: 3

Bin 804: 4413 of cap free
Amount of items: 2
Items: 
Size: 715749 Color: 4
Size: 279839 Color: 12

Bin 805: 4524 of cap free
Amount of items: 2
Items: 
Size: 624288 Color: 13
Size: 371189 Color: 10

Bin 806: 4609 of cap free
Amount of items: 2
Items: 
Size: 780457 Color: 9
Size: 214935 Color: 19

Bin 807: 4839 of cap free
Amount of items: 2
Items: 
Size: 571365 Color: 5
Size: 423797 Color: 10

Bin 808: 4869 of cap free
Amount of items: 2
Items: 
Size: 670606 Color: 10
Size: 324526 Color: 0

Bin 809: 4911 of cap free
Amount of items: 2
Items: 
Size: 780353 Color: 13
Size: 214737 Color: 11

Bin 810: 5009 of cap free
Amount of items: 2
Items: 
Size: 743244 Color: 7
Size: 251748 Color: 0

Bin 811: 5082 of cap free
Amount of items: 2
Items: 
Size: 690684 Color: 8
Size: 304235 Color: 11

Bin 812: 5138 of cap free
Amount of items: 2
Items: 
Size: 562598 Color: 7
Size: 432265 Color: 5

Bin 813: 5141 of cap free
Amount of items: 2
Items: 
Size: 571333 Color: 18
Size: 423527 Color: 14

Bin 814: 5318 of cap free
Amount of items: 2
Items: 
Size: 523957 Color: 9
Size: 470726 Color: 8

Bin 815: 5545 of cap free
Amount of items: 2
Items: 
Size: 670353 Color: 10
Size: 324103 Color: 15

Bin 816: 5557 of cap free
Amount of items: 2
Items: 
Size: 649890 Color: 9
Size: 344554 Color: 19

Bin 817: 5565 of cap free
Amount of items: 2
Items: 
Size: 642333 Color: 12
Size: 352103 Color: 7

Bin 818: 5577 of cap free
Amount of items: 2
Items: 
Size: 500576 Color: 15
Size: 493848 Color: 14

Bin 819: 5580 of cap free
Amount of items: 2
Items: 
Size: 658263 Color: 6
Size: 336158 Color: 13

Bin 820: 5604 of cap free
Amount of items: 2
Items: 
Size: 605826 Color: 18
Size: 388571 Color: 8

Bin 821: 5933 of cap free
Amount of items: 2
Items: 
Size: 649567 Color: 17
Size: 344501 Color: 14

Bin 822: 6036 of cap free
Amount of items: 2
Items: 
Size: 670228 Color: 7
Size: 323737 Color: 12

Bin 823: 6107 of cap free
Amount of items: 2
Items: 
Size: 599346 Color: 8
Size: 394548 Color: 10

Bin 824: 6209 of cap free
Amount of items: 2
Items: 
Size: 715376 Color: 1
Size: 278416 Color: 8

Bin 825: 6248 of cap free
Amount of items: 2
Items: 
Size: 742370 Color: 11
Size: 251383 Color: 16

Bin 826: 6311 of cap free
Amount of items: 2
Items: 
Size: 649461 Color: 4
Size: 344229 Color: 12

Bin 827: 6360 of cap free
Amount of items: 2
Items: 
Size: 731675 Color: 4
Size: 261966 Color: 11

Bin 828: 6884 of cap free
Amount of items: 2
Items: 
Size: 756648 Color: 11
Size: 236469 Color: 15

Bin 829: 6957 of cap free
Amount of items: 2
Items: 
Size: 522463 Color: 6
Size: 470581 Color: 5

Bin 830: 7099 of cap free
Amount of items: 2
Items: 
Size: 522360 Color: 15
Size: 470542 Color: 9

Bin 831: 8097 of cap free
Amount of items: 2
Items: 
Size: 522194 Color: 5
Size: 469710 Color: 12

Bin 832: 8177 of cap free
Amount of items: 2
Items: 
Size: 756394 Color: 6
Size: 235430 Color: 14

Bin 833: 8266 of cap free
Amount of items: 2
Items: 
Size: 740702 Color: 2
Size: 251033 Color: 10

Bin 834: 8677 of cap free
Amount of items: 2
Items: 
Size: 715235 Color: 0
Size: 276089 Color: 9

Bin 835: 9257 of cap free
Amount of items: 2
Items: 
Size: 756160 Color: 3
Size: 234584 Color: 5

Bin 836: 9984 of cap free
Amount of items: 2
Items: 
Size: 520904 Color: 11
Size: 469113 Color: 6

Bin 837: 12494 of cap free
Amount of items: 2
Items: 
Size: 599627 Color: 18
Size: 387880 Color: 8

Bin 838: 12918 of cap free
Amount of items: 2
Items: 
Size: 557512 Color: 15
Size: 429571 Color: 9

Bin 839: 14043 of cap free
Amount of items: 2
Items: 
Size: 665831 Color: 9
Size: 320127 Color: 4

Bin 840: 14802 of cap free
Amount of items: 2
Items: 
Size: 797157 Color: 13
Size: 188042 Color: 0

Bin 841: 15304 of cap free
Amount of items: 2
Items: 
Size: 555249 Color: 11
Size: 429448 Color: 9

Bin 842: 15422 of cap free
Amount of items: 2
Items: 
Size: 756013 Color: 9
Size: 228566 Color: 16

Bin 843: 15835 of cap free
Amount of items: 2
Items: 
Size: 555215 Color: 8
Size: 428951 Color: 19

Bin 844: 15898 of cap free
Amount of items: 2
Items: 
Size: 492060 Color: 0
Size: 492043 Color: 9

Bin 845: 16001 of cap free
Amount of items: 2
Items: 
Size: 596501 Color: 8
Size: 387499 Color: 2

Bin 846: 19918 of cap free
Amount of items: 2
Items: 
Size: 596427 Color: 2
Size: 383656 Color: 12

Bin 847: 25183 of cap free
Amount of items: 2
Items: 
Size: 554982 Color: 9
Size: 419836 Color: 13

Bin 848: 25809 of cap free
Amount of items: 2
Items: 
Size: 554702 Color: 16
Size: 419490 Color: 6

Bin 849: 26249 of cap free
Amount of items: 2
Items: 
Size: 622503 Color: 12
Size: 351249 Color: 3

Bin 850: 27496 of cap free
Amount of items: 2
Items: 
Size: 553579 Color: 18
Size: 418926 Color: 0

Bin 851: 27853 of cap free
Amount of items: 2
Items: 
Size: 793319 Color: 16
Size: 178829 Color: 7

Bin 852: 29875 of cap free
Amount of items: 2
Items: 
Size: 485603 Color: 15
Size: 484523 Color: 11

Bin 853: 30606 of cap free
Amount of items: 2
Items: 
Size: 793237 Color: 9
Size: 176158 Color: 3

Bin 854: 34049 of cap free
Amount of items: 2
Items: 
Size: 793212 Color: 9
Size: 172740 Color: 15

Bin 855: 37058 of cap free
Amount of items: 2
Items: 
Size: 554669 Color: 15
Size: 408274 Color: 18

Bin 856: 37867 of cap free
Amount of items: 2
Items: 
Size: 482069 Color: 9
Size: 480065 Color: 7

Bin 857: 38561 of cap free
Amount of items: 2
Items: 
Size: 793036 Color: 16
Size: 168404 Color: 17

Bin 858: 42165 of cap free
Amount of items: 2
Items: 
Size: 791266 Color: 1
Size: 166570 Color: 6

Bin 859: 45507 of cap free
Amount of items: 2
Items: 
Size: 619217 Color: 17
Size: 335277 Color: 11

Bin 860: 52058 of cap free
Amount of items: 2
Items: 
Size: 790758 Color: 10
Size: 157185 Color: 12

Bin 861: 53379 of cap free
Amount of items: 2
Items: 
Size: 790989 Color: 12
Size: 155633 Color: 1

Bin 862: 62113 of cap free
Amount of items: 2
Items: 
Size: 468980 Color: 12
Size: 468908 Color: 15

Bin 863: 67503 of cap free
Amount of items: 2
Items: 
Size: 468597 Color: 9
Size: 463901 Color: 5

Bin 864: 72358 of cap free
Amount of items: 2
Items: 
Size: 463900 Color: 19
Size: 463743 Color: 6

Bin 865: 73189 of cap free
Amount of items: 2
Items: 
Size: 463484 Color: 3
Size: 463328 Color: 14

Bin 866: 73997 of cap free
Amount of items: 2
Items: 
Size: 463274 Color: 14
Size: 462730 Color: 1

Bin 867: 74176 of cap free
Amount of items: 2
Items: 
Size: 463145 Color: 14
Size: 462680 Color: 9

Bin 868: 74881 of cap free
Amount of items: 2
Items: 
Size: 462587 Color: 0
Size: 462533 Color: 17

Bin 869: 75373 of cap free
Amount of items: 2
Items: 
Size: 462577 Color: 0
Size: 462051 Color: 10

Bin 870: 81562 of cap free
Amount of items: 2
Items: 
Size: 460364 Color: 13
Size: 458075 Color: 8

Bin 871: 84106 of cap free
Amount of items: 2
Items: 
Size: 458028 Color: 0
Size: 457867 Color: 13

Bin 872: 85022 of cap free
Amount of items: 2
Items: 
Size: 457512 Color: 19
Size: 457467 Color: 6

Bin 873: 85734 of cap free
Amount of items: 2
Items: 
Size: 457437 Color: 15
Size: 456830 Color: 14

Bin 874: 90735 of cap free
Amount of items: 2
Items: 
Size: 455365 Color: 4
Size: 453901 Color: 11

Bin 875: 93964 of cap free
Amount of items: 2
Items: 
Size: 453045 Color: 2
Size: 452992 Color: 1

Bin 876: 210218 of cap free
Amount of items: 1
Items: 
Size: 789783 Color: 5

Bin 877: 210422 of cap free
Amount of items: 1
Items: 
Size: 789579 Color: 0

Bin 878: 210901 of cap free
Amount of items: 1
Items: 
Size: 789100 Color: 10

Bin 879: 211578 of cap free
Amount of items: 1
Items: 
Size: 788423 Color: 19

Bin 880: 212216 of cap free
Amount of items: 1
Items: 
Size: 787785 Color: 9

Bin 881: 213275 of cap free
Amount of items: 1
Items: 
Size: 786726 Color: 10

Bin 882: 214094 of cap free
Amount of items: 1
Items: 
Size: 785907 Color: 12

Bin 883: 214165 of cap free
Amount of items: 1
Items: 
Size: 785836 Color: 1

Bin 884: 214210 of cap free
Amount of items: 1
Items: 
Size: 785791 Color: 1

Bin 885: 219662 of cap free
Amount of items: 1
Items: 
Size: 780339 Color: 19

Bin 886: 219808 of cap free
Amount of items: 1
Items: 
Size: 780193 Color: 19

Bin 887: 219828 of cap free
Amount of items: 1
Items: 
Size: 780173 Color: 2

Bin 888: 219895 of cap free
Amount of items: 1
Items: 
Size: 780106 Color: 16

Bin 889: 220423 of cap free
Amount of items: 1
Items: 
Size: 779578 Color: 13

Bin 890: 221176 of cap free
Amount of items: 1
Items: 
Size: 778825 Color: 11

Bin 891: 221231 of cap free
Amount of items: 1
Items: 
Size: 778770 Color: 7

Bin 892: 221866 of cap free
Amount of items: 1
Items: 
Size: 778135 Color: 15

Bin 893: 222228 of cap free
Amount of items: 1
Items: 
Size: 777773 Color: 15

Bin 894: 222283 of cap free
Amount of items: 1
Items: 
Size: 777718 Color: 14

Bin 895: 222712 of cap free
Amount of items: 1
Items: 
Size: 777289 Color: 12

Bin 896: 223740 of cap free
Amount of items: 1
Items: 
Size: 776261 Color: 12

Bin 897: 224996 of cap free
Amount of items: 1
Items: 
Size: 775005 Color: 12

Bin 898: 226052 of cap free
Amount of items: 1
Items: 
Size: 773949 Color: 4

Bin 899: 228529 of cap free
Amount of items: 1
Items: 
Size: 771472 Color: 4

Bin 900: 244446 of cap free
Amount of items: 1
Items: 
Size: 755555 Color: 19

Bin 901: 245045 of cap free
Amount of items: 1
Items: 
Size: 754956 Color: 6

Bin 902: 245400 of cap free
Amount of items: 1
Items: 
Size: 754601 Color: 6

Bin 903: 249445 of cap free
Amount of items: 1
Items: 
Size: 750556 Color: 15

Bin 904: 249559 of cap free
Amount of items: 1
Items: 
Size: 750442 Color: 7

Total size: 895152129
Total free space: 8848775

