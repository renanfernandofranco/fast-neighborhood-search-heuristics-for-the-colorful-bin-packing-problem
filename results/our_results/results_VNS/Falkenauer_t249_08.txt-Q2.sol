Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 278 Color: 0
Size: 254 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 360 Color: 0
Size: 258 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 286 Color: 0
Size: 269 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 308 Color: 0
Size: 286 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 349 Color: 0
Size: 253 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 334 Color: 1
Size: 263 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 350 Color: 1
Size: 267 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 359 Color: 0
Size: 270 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 347 Color: 1
Size: 275 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 331 Color: 0
Size: 261 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 265 Color: 0
Size: 252 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 307 Color: 1
Size: 291 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 257 Color: 0
Size: 250 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 275 Color: 1
Size: 256 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 369 Color: 0
Size: 251 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 320 Color: 0
Size: 317 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 271 Color: 1
Size: 264 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 357 Color: 0
Size: 276 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 313 Color: 1
Size: 309 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 268 Color: 0
Size: 250 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 289 Color: 0
Size: 269 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 334 Color: 0
Size: 270 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 281 Color: 0
Size: 270 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 0
Size: 351 Color: 0
Size: 261 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 288 Color: 0
Size: 258 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 0
Size: 252 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 336 Color: 0
Size: 263 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 345 Color: 1
Size: 254 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 335 Color: 1
Size: 276 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 0
Size: 325 Color: 1
Size: 324 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 334 Color: 0
Size: 259 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 319 Color: 1
Size: 285 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 0
Size: 271 Color: 0
Size: 263 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 284 Color: 1
Size: 257 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 261 Color: 0
Size: 259 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 308 Color: 0
Size: 257 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 277 Color: 0
Size: 268 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 305 Color: 1
Size: 293 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 283 Color: 0
Size: 275 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 317 Color: 1
Size: 301 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 279 Color: 0
Size: 270 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 264 Color: 0
Size: 260 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 348 Color: 1
Size: 267 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 300 Color: 0
Size: 264 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 365 Color: 0
Size: 269 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 319 Color: 1
Size: 295 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 260 Color: 0
Size: 252 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 0
Size: 255 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 266 Color: 1
Size: 255 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 260 Color: 0
Size: 250 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 278 Color: 0
Size: 275 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 262 Color: 1
Size: 259 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 0
Size: 334 Color: 1
Size: 275 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 262 Color: 0
Size: 250 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 286 Color: 0
Size: 264 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 325 Color: 0
Size: 271 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 320 Color: 0
Size: 278 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 298 Color: 0
Size: 273 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 346 Color: 0
Size: 250 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 278 Color: 0
Size: 274 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 263 Color: 0
Size: 254 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 263 Color: 0
Size: 255 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 339 Color: 0
Size: 266 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 270 Color: 0
Size: 266 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 326 Color: 0
Size: 295 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 362 Color: 1
Size: 260 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 0
Size: 312 Color: 1
Size: 250 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 358 Color: 1
Size: 279 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 300 Color: 1
Size: 251 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 317 Color: 1
Size: 306 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 320 Color: 0
Size: 275 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 0
Size: 250 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 355 Color: 0
Size: 262 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 361 Color: 0
Size: 276 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 269 Color: 1
Size: 256 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 287 Color: 0
Size: 254 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 0
Size: 314 Color: 0
Size: 275 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 313 Color: 0
Size: 251 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 346 Color: 1
Size: 283 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 338 Color: 0
Size: 267 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 0
Size: 340 Color: 0
Size: 317 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 267 Color: 1
Size: 252 Color: 0

Total size: 83000
Total free space: 0

