Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 1
Size: 1022 Color: 1
Size: 100 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 851 Color: 0
Size: 778 Color: 0
Size: 753 Color: 1
Size: 74 Color: 1

Bin 3: 0 of cap free
Amount of items: 18
Items: 
Size: 244 Color: 0
Size: 200 Color: 0
Size: 176 Color: 0
Size: 170 Color: 1
Size: 168 Color: 1
Size: 156 Color: 0
Size: 144 Color: 1
Size: 132 Color: 0
Size: 128 Color: 1
Size: 128 Color: 0
Size: 120 Color: 0
Size: 112 Color: 0
Size: 108 Color: 1
Size: 108 Color: 1
Size: 92 Color: 1
Size: 92 Color: 0
Size: 90 Color: 1
Size: 88 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 0
Size: 610 Color: 1
Size: 64 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 2012 Color: 1
Size: 382 Color: 0
Size: 62 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 0
Size: 436 Color: 1
Size: 313 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2089 Color: 0
Size: 307 Color: 0
Size: 60 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1644 Color: 0
Size: 512 Color: 1
Size: 300 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 1
Size: 384 Color: 1
Size: 44 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1894 Color: 1
Size: 554 Color: 0
Size: 8 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 508 Color: 0
Size: 8 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 1
Size: 442 Color: 0
Size: 204 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1603 Color: 0
Size: 631 Color: 0
Size: 222 Color: 1

Bin 14: 0 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 0
Size: 790 Color: 0
Size: 379 Color: 1
Size: 32 Color: 0
Size: 24 Color: 1

Bin 15: 0 of cap free
Amount of items: 5
Items: 
Size: 1229 Color: 1
Size: 455 Color: 0
Size: 280 Color: 1
Size: 260 Color: 1
Size: 232 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 1
Size: 564 Color: 0
Size: 40 Color: 1

Bin 17: 0 of cap free
Amount of items: 4
Items: 
Size: 2082 Color: 1
Size: 168 Color: 0
Size: 142 Color: 1
Size: 64 Color: 0

Bin 18: 0 of cap free
Amount of items: 5
Items: 
Size: 1926 Color: 0
Size: 362 Color: 0
Size: 72 Color: 0
Size: 48 Color: 1
Size: 48 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1364 Color: 1
Size: 1020 Color: 1
Size: 72 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1468 Color: 0
Size: 828 Color: 0
Size: 160 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1764 Color: 1
Size: 620 Color: 0
Size: 72 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 0
Size: 460 Color: 0
Size: 88 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1911 Color: 1
Size: 449 Color: 1
Size: 96 Color: 0

Bin 24: 0 of cap free
Amount of items: 4
Items: 
Size: 2002 Color: 1
Size: 364 Color: 0
Size: 50 Color: 1
Size: 40 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 0
Size: 371 Color: 0
Size: 88 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 2011 Color: 0
Size: 369 Color: 0
Size: 76 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 0
Size: 370 Color: 1
Size: 8 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 0
Size: 306 Color: 0
Size: 60 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 2139 Color: 0
Size: 261 Color: 0
Size: 56 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 0
Size: 184 Color: 0
Size: 126 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 0
Size: 212 Color: 0
Size: 96 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 2172 Color: 1
Size: 272 Color: 0
Size: 12 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 1
Size: 152 Color: 0
Size: 112 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 0
Size: 244 Color: 1
Size: 8 Color: 0

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 1
Size: 684 Color: 0
Size: 541 Color: 0

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 1437 Color: 0
Size: 938 Color: 0
Size: 80 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 1
Size: 654 Color: 0
Size: 265 Color: 1

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 0
Size: 756 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 0
Size: 262 Color: 0
Size: 48 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 568 Color: 1
Size: 88 Color: 0

Bin 41: 1 of cap free
Amount of items: 5
Items: 
Size: 1234 Color: 0
Size: 1023 Color: 0
Size: 76 Color: 1
Size: 74 Color: 0
Size: 48 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 1510 Color: 0
Size: 840 Color: 0
Size: 104 Color: 1

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 1716 Color: 0
Size: 538 Color: 1
Size: 200 Color: 1

Bin 44: 2 of cap free
Amount of items: 4
Items: 
Size: 2003 Color: 1
Size: 383 Color: 0
Size: 52 Color: 0
Size: 16 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 0
Size: 470 Color: 1
Size: 204 Color: 1

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 2081 Color: 0
Size: 372 Color: 1

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 1
Size: 1114 Color: 1
Size: 102 Color: 0

Bin 48: 4 of cap free
Amount of items: 2
Items: 
Size: 1897 Color: 0
Size: 555 Color: 1

Bin 49: 4 of cap free
Amount of items: 2
Items: 
Size: 1910 Color: 0
Size: 542 Color: 1

Bin 50: 4 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 1
Size: 857 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 0
Size: 386 Color: 1
Size: 72 Color: 0

Bin 52: 6 of cap free
Amount of items: 3
Items: 
Size: 1696 Color: 0
Size: 666 Color: 0
Size: 88 Color: 1

Bin 53: 6 of cap free
Amount of items: 2
Items: 
Size: 1429 Color: 0
Size: 1021 Color: 1

Bin 54: 7 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 0
Size: 467 Color: 0
Size: 324 Color: 1

Bin 55: 9 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 0
Size: 653 Color: 1

Bin 56: 13 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 0
Size: 620 Color: 1
Size: 16 Color: 0

Bin 57: 14 of cap free
Amount of items: 2
Items: 
Size: 1526 Color: 0
Size: 916 Color: 1

Bin 58: 38 of cap free
Amount of items: 2
Items: 
Size: 1556 Color: 0
Size: 862 Color: 1

Bin 59: 54 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 0
Size: 976 Color: 1

Bin 60: 63 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 1
Size: 719 Color: 0

Bin 61: 266 of cap free
Amount of items: 1
Items: 
Size: 2190 Color: 1

Bin 62: 268 of cap free
Amount of items: 1
Items: 
Size: 2188 Color: 0

Bin 63: 340 of cap free
Amount of items: 1
Items: 
Size: 2116 Color: 1

Bin 64: 356 of cap free
Amount of items: 1
Items: 
Size: 2100 Color: 0

Bin 65: 441 of cap free
Amount of items: 1
Items: 
Size: 2015 Color: 0

Bin 66: 537 of cap free
Amount of items: 1
Items: 
Size: 1919 Color: 0

Total size: 159640
Total free space: 2456

