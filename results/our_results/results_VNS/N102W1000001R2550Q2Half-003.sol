Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 349629 Color: 1
Size: 323810 Color: 0
Size: 326562 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 356741 Color: 1
Size: 333290 Color: 1
Size: 309970 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 358115 Color: 1
Size: 334297 Color: 1
Size: 307589 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 360690 Color: 1
Size: 347572 Color: 1
Size: 291739 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 363545 Color: 1
Size: 321968 Color: 1
Size: 314488 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 366475 Color: 1
Size: 327777 Color: 1
Size: 305749 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370697 Color: 1
Size: 323224 Color: 1
Size: 306080 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 371300 Color: 1
Size: 342697 Color: 1
Size: 286004 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 372182 Color: 1
Size: 323307 Color: 1
Size: 304512 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 375372 Color: 1
Size: 331249 Color: 1
Size: 293380 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 381516 Color: 1
Size: 362253 Color: 1
Size: 256232 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 383586 Color: 1
Size: 358236 Color: 1
Size: 258179 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 388357 Color: 1
Size: 326928 Color: 1
Size: 284716 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 392193 Color: 1
Size: 321268 Color: 1
Size: 286540 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 395226 Color: 1
Size: 313306 Color: 1
Size: 291469 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 396620 Color: 1
Size: 322434 Color: 1
Size: 280947 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 400123 Color: 1
Size: 339953 Color: 1
Size: 259925 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 403063 Color: 1
Size: 335219 Color: 1
Size: 261719 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 413346 Color: 1
Size: 319470 Color: 1
Size: 267185 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 423046 Color: 1
Size: 302395 Color: 1
Size: 274560 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 425338 Color: 1
Size: 319729 Color: 1
Size: 254934 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 425959 Color: 1
Size: 293410 Color: 1
Size: 280632 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 426155 Color: 1
Size: 307210 Color: 1
Size: 266636 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 429583 Color: 1
Size: 296351 Color: 1
Size: 274067 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 429777 Color: 1
Size: 315106 Color: 1
Size: 255118 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439939 Color: 1
Size: 295205 Color: 1
Size: 264857 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 443577 Color: 1
Size: 296622 Color: 1
Size: 259802 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 450067 Color: 1
Size: 293305 Color: 1
Size: 256629 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 450450 Color: 1
Size: 287105 Color: 1
Size: 262446 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 460861 Color: 1
Size: 288665 Color: 1
Size: 250475 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 466510 Color: 1
Size: 279616 Color: 1
Size: 253875 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 469843 Color: 1
Size: 268030 Color: 1
Size: 262128 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 469848 Color: 1
Size: 273436 Color: 1
Size: 256717 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 497235 Color: 1
Size: 251410 Color: 1
Size: 251356 Color: 0

Total size: 34000034
Total free space: 0

