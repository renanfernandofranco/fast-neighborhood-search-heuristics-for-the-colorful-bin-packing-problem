Capicity Bin: 2020
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 18
Items: 
Size: 267 Color: 0
Size: 211 Color: 0
Size: 166 Color: 0
Size: 136 Color: 1
Size: 128 Color: 0
Size: 118 Color: 0
Size: 118 Color: 0
Size: 116 Color: 0
Size: 100 Color: 1
Size: 92 Color: 1
Size: 88 Color: 1
Size: 86 Color: 1
Size: 72 Color: 0
Size: 72 Color: 0
Size: 70 Color: 1
Size: 70 Color: 1
Size: 60 Color: 1
Size: 50 Color: 1

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1011 Color: 0
Size: 437 Color: 0
Size: 277 Color: 0
Size: 231 Color: 1
Size: 64 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 0
Size: 274 Color: 0
Size: 96 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 0
Size: 180 Color: 1
Size: 166 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 0
Size: 290 Color: 1
Size: 36 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 0
Size: 241 Color: 1
Size: 52 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 0
Size: 166 Color: 0
Size: 120 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 0
Size: 237 Color: 1
Size: 46 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 0
Size: 218 Color: 1
Size: 40 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1769 Color: 1
Size: 227 Color: 0
Size: 24 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 0
Size: 182 Color: 1
Size: 56 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1795 Color: 0
Size: 189 Color: 1
Size: 36 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 1812 Color: 1
Size: 200 Color: 0
Size: 4 Color: 1
Size: 4 Color: 0

Bin 14: 1 of cap free
Amount of items: 5
Items: 
Size: 1017 Color: 1
Size: 654 Color: 0
Size: 232 Color: 1
Size: 68 Color: 0
Size: 48 Color: 1

Bin 15: 1 of cap free
Amount of items: 3
Items: 
Size: 1014 Color: 0
Size: 841 Color: 1
Size: 164 Color: 0

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1025 Color: 0
Size: 826 Color: 0
Size: 168 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 0
Size: 589 Color: 1
Size: 40 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 0
Size: 468 Color: 1
Size: 60 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 1
Size: 441 Color: 0
Size: 44 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 0
Size: 383 Color: 0
Size: 46 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 1
Size: 375 Color: 0
Size: 74 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1733 Color: 0
Size: 242 Color: 1
Size: 44 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1767 Color: 0
Size: 164 Color: 1
Size: 88 Color: 0

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 676 Color: 1
Size: 104 Color: 0

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1561 Color: 1
Size: 433 Color: 0
Size: 24 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 0
Size: 245 Color: 1
Size: 32 Color: 0

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1745 Color: 0
Size: 273 Color: 1

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 0
Size: 578 Color: 0
Size: 28 Color: 1

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 334 Color: 0
Size: 112 Color: 1

Bin 30: 4 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 0
Size: 349 Color: 1
Size: 100 Color: 1

Bin 31: 5 of cap free
Amount of items: 3
Items: 
Size: 1114 Color: 1
Size: 831 Color: 0
Size: 70 Color: 1

Bin 32: 5 of cap free
Amount of items: 2
Items: 
Size: 1182 Color: 0
Size: 833 Color: 1

Bin 33: 5 of cap free
Amount of items: 2
Items: 
Size: 1195 Color: 0
Size: 820 Color: 1

Bin 34: 5 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 0
Size: 378 Color: 1
Size: 46 Color: 0

Bin 35: 5 of cap free
Amount of items: 2
Items: 
Size: 1599 Color: 0
Size: 416 Color: 1

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 0
Size: 222 Color: 1
Size: 2 Color: 1

Bin 37: 6 of cap free
Amount of items: 2
Items: 
Size: 1505 Color: 0
Size: 509 Color: 1

Bin 38: 7 of cap free
Amount of items: 2
Items: 
Size: 1311 Color: 0
Size: 702 Color: 1

Bin 39: 7 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 0
Size: 607 Color: 0
Size: 76 Color: 1

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1701 Color: 0
Size: 312 Color: 1

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1780 Color: 1
Size: 233 Color: 0

Bin 42: 8 of cap free
Amount of items: 3
Items: 
Size: 1030 Color: 1
Size: 758 Color: 0
Size: 224 Color: 0

Bin 43: 8 of cap free
Amount of items: 2
Items: 
Size: 1171 Color: 1
Size: 841 Color: 0

Bin 44: 8 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 0
Size: 202 Color: 1
Size: 4 Color: 1

Bin 45: 9 of cap free
Amount of items: 3
Items: 
Size: 1497 Color: 0
Size: 470 Color: 1
Size: 44 Color: 0

Bin 46: 10 of cap free
Amount of items: 2
Items: 
Size: 1651 Color: 1
Size: 359 Color: 0

Bin 47: 11 of cap free
Amount of items: 2
Items: 
Size: 1595 Color: 0
Size: 414 Color: 1

Bin 48: 11 of cap free
Amount of items: 2
Items: 
Size: 1603 Color: 1
Size: 406 Color: 0

Bin 49: 13 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 1
Size: 910 Color: 1
Size: 76 Color: 0

Bin 50: 13 of cap free
Amount of items: 2
Items: 
Size: 1697 Color: 0
Size: 310 Color: 1

Bin 51: 13 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 0
Size: 249 Color: 1

Bin 52: 14 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 0
Size: 462 Color: 1
Size: 86 Color: 0

Bin 53: 16 of cap free
Amount of items: 2
Items: 
Size: 1167 Color: 1
Size: 837 Color: 0

Bin 54: 16 of cap free
Amount of items: 2
Items: 
Size: 1293 Color: 0
Size: 711 Color: 1

Bin 55: 16 of cap free
Amount of items: 2
Items: 
Size: 1315 Color: 1
Size: 689 Color: 0

Bin 56: 16 of cap free
Amount of items: 3
Items: 
Size: 1417 Color: 1
Size: 439 Color: 0
Size: 148 Color: 1

Bin 57: 16 of cap free
Amount of items: 2
Items: 
Size: 1501 Color: 1
Size: 503 Color: 0

Bin 58: 16 of cap free
Amount of items: 2
Items: 
Size: 1649 Color: 1
Size: 355 Color: 0

Bin 59: 16 of cap free
Amount of items: 2
Items: 
Size: 1693 Color: 1
Size: 311 Color: 0

Bin 60: 19 of cap free
Amount of items: 2
Items: 
Size: 1622 Color: 0
Size: 379 Color: 1

Bin 61: 21 of cap free
Amount of items: 2
Items: 
Size: 1268 Color: 1
Size: 731 Color: 0

Bin 62: 22 of cap free
Amount of items: 2
Items: 
Size: 1689 Color: 1
Size: 309 Color: 0

Bin 63: 24 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 0
Size: 526 Color: 1

Bin 64: 31 of cap free
Amount of items: 6
Items: 
Size: 1013 Color: 1
Size: 351 Color: 0
Size: 273 Color: 0
Size: 142 Color: 1
Size: 136 Color: 1
Size: 74 Color: 0

Bin 65: 33 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 0
Size: 591 Color: 1

Bin 66: 1548 of cap free
Amount of items: 9
Items: 
Size: 68 Color: 0
Size: 60 Color: 1
Size: 60 Color: 0
Size: 54 Color: 1
Size: 54 Color: 0
Size: 52 Color: 1
Size: 44 Color: 1
Size: 40 Color: 0
Size: 40 Color: 0

Total size: 131300
Total free space: 2020

