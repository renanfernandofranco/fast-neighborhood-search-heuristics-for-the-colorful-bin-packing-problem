Capicity Bin: 1001
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 483
Size: 258 Color: 50
Size: 269 Color: 95

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 397
Size: 345 Color: 312
Size: 258 Color: 51

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 489
Size: 269 Color: 98
Size: 250 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 359
Size: 365 Color: 352
Size: 263 Color: 70

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 460
Size: 284 Color: 148
Size: 268 Color: 90

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 477
Size: 284 Color: 152
Size: 251 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 379
Size: 315 Color: 253
Size: 302 Color: 209

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 466
Size: 279 Color: 133
Size: 268 Color: 89

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 365
Size: 360 Color: 343
Size: 265 Color: 76

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 375
Size: 332 Color: 285
Size: 287 Color: 165

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 355
Size: 326 Color: 274
Size: 305 Color: 222

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 463
Size: 284 Color: 153
Size: 265 Color: 75

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 486
Size: 272 Color: 107
Size: 251 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 386
Size: 306 Color: 225
Size: 304 Color: 218

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 429
Size: 320 Color: 261
Size: 262 Color: 64

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 412
Size: 342 Color: 306
Size: 252 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 406
Size: 335 Color: 292
Size: 263 Color: 68

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 485
Size: 263 Color: 69
Size: 260 Color: 59

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 461
Size: 283 Color: 144
Size: 269 Color: 93

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 420
Size: 303 Color: 213
Size: 286 Color: 158

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 369
Size: 324 Color: 269
Size: 298 Color: 199

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 366
Size: 346 Color: 315
Size: 278 Color: 126

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 362
Size: 339 Color: 298
Size: 287 Color: 162

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 341
Size: 351 Color: 326
Size: 291 Color: 180

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 353
Size: 340 Color: 300
Size: 295 Color: 190

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 363
Size: 313 Color: 243
Size: 313 Color: 241

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 445
Size: 293 Color: 184
Size: 276 Color: 122

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 400
Size: 314 Color: 247
Size: 288 Color: 169

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 458
Size: 288 Color: 166
Size: 268 Color: 88

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 464
Size: 286 Color: 159
Size: 262 Color: 65

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 395
Size: 329 Color: 279
Size: 275 Color: 121

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 391
Size: 324 Color: 270
Size: 281 Color: 139

Bin 33: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 500
Size: 500 Color: 499

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 423
Size: 305 Color: 221
Size: 280 Color: 135

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 438
Size: 296 Color: 191
Size: 275 Color: 120

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 482
Size: 269 Color: 97
Size: 258 Color: 52

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 349
Size: 350 Color: 323
Size: 288 Color: 170

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 481
Size: 265 Color: 80
Size: 265 Color: 79

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 442
Size: 316 Color: 254
Size: 254 Color: 32

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 338
Size: 343 Color: 308
Size: 301 Color: 208

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 413
Size: 307 Color: 228
Size: 287 Color: 163

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 428
Size: 302 Color: 211
Size: 281 Color: 137

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 360
Size: 348 Color: 319
Size: 279 Color: 131

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 329
Size: 347 Color: 316
Size: 302 Color: 212

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 473
Size: 286 Color: 157
Size: 254 Color: 30

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 318
Size: 347 Color: 317
Size: 307 Color: 227

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 358
Size: 353 Color: 330
Size: 275 Color: 119

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 339
Size: 342 Color: 305
Size: 302 Color: 210

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 480
Size: 274 Color: 116
Size: 257 Color: 44

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 381
Size: 307 Color: 229
Size: 306 Color: 224

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 426
Size: 316 Color: 256
Size: 268 Color: 87

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 398
Size: 304 Color: 215
Size: 299 Color: 202

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 294
Size: 333 Color: 286
Size: 332 Color: 284

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 474
Size: 284 Color: 147
Size: 253 Color: 23

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 322
Size: 333 Color: 288
Size: 319 Color: 260

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 404
Size: 345 Color: 311
Size: 253 Color: 25

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 408
Size: 341 Color: 304
Size: 256 Color: 40

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 347
Size: 361 Color: 346
Size: 279 Color: 127

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 388
Size: 356 Color: 335
Size: 251 Color: 11

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 344
Size: 359 Color: 340
Size: 282 Color: 141

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 455
Size: 291 Color: 182
Size: 267 Color: 86

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 479
Size: 285 Color: 155
Size: 250 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 457
Size: 298 Color: 198
Size: 258 Color: 53

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 331
Size: 348 Color: 321
Size: 300 Color: 204

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 382
Size: 323 Color: 267
Size: 289 Color: 175

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 385
Size: 325 Color: 272
Size: 285 Color: 154

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 410
Size: 313 Color: 239
Size: 283 Color: 146

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 357
Size: 316 Color: 257
Size: 313 Color: 242

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 384
Size: 360 Color: 345
Size: 252 Color: 16

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 414
Size: 309 Color: 234
Size: 284 Color: 151

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 401
Size: 325 Color: 271
Size: 274 Color: 117

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 484
Size: 272 Color: 108
Size: 253 Color: 26

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 394
Size: 337 Color: 295
Size: 267 Color: 85

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 368
Size: 342 Color: 307
Size: 282 Color: 143

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 399
Size: 334 Color: 289
Size: 269 Color: 96

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 431
Size: 327 Color: 275
Size: 252 Color: 18

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 468
Size: 289 Color: 171
Size: 258 Color: 56

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 416
Size: 322 Color: 264
Size: 270 Color: 99

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 441
Size: 305 Color: 219
Size: 265 Color: 78

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 446
Size: 287 Color: 161
Size: 279 Color: 130

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 465
Size: 287 Color: 164
Size: 261 Color: 63

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 417
Size: 303 Color: 214
Size: 289 Color: 176

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 402
Size: 335 Color: 291
Size: 264 Color: 74

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 336
Size: 351 Color: 324
Size: 294 Color: 187

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 367
Size: 315 Color: 250
Size: 309 Color: 235

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 314
Size: 344 Color: 310
Size: 311 Color: 237

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 467
Size: 294 Color: 186
Size: 253 Color: 22

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 419
Size: 301 Color: 206
Size: 288 Color: 167

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 449
Size: 293 Color: 185
Size: 270 Color: 102

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 433
Size: 307 Color: 230
Size: 271 Color: 106

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 377
Size: 340 Color: 302
Size: 278 Color: 125

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 456
Size: 285 Color: 156
Size: 271 Color: 104

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 448
Size: 311 Color: 238
Size: 253 Color: 19

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 383
Size: 323 Color: 266
Size: 289 Color: 172

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 472
Size: 279 Color: 132
Size: 261 Color: 61

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 447
Size: 290 Color: 179
Size: 275 Color: 118

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 432
Size: 301 Color: 207
Size: 278 Color: 124

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 370
Size: 333 Color: 287
Size: 289 Color: 174

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 364
Size: 374 Color: 361
Size: 251 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 439
Size: 306 Color: 226
Size: 265 Color: 77

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 459
Size: 297 Color: 196
Size: 255 Color: 39

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 498
Size: 254 Color: 31
Size: 251 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 478
Size: 271 Color: 103
Size: 264 Color: 73

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 350
Size: 354 Color: 332
Size: 284 Color: 149

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 411
Size: 340 Color: 301
Size: 255 Color: 38

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 434
Size: 297 Color: 194
Size: 277 Color: 123

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 297
Size: 334 Color: 290
Size: 329 Color: 280

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 421
Size: 325 Color: 273
Size: 262 Color: 66

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 492
Size: 261 Color: 62
Size: 256 Color: 41

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 488
Size: 264 Color: 72
Size: 255 Color: 37

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 356
Size: 316 Color: 255
Size: 315 Color: 252

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 387
Size: 336 Color: 293
Size: 273 Color: 112

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 403
Size: 341 Color: 303
Size: 257 Color: 46

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 389
Size: 308 Color: 231
Size: 298 Color: 197

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 470
Size: 281 Color: 138
Size: 264 Color: 71

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 487
Size: 266 Color: 83
Size: 253 Color: 27

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 396
Size: 329 Color: 278
Size: 274 Color: 114

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 444
Size: 300 Color: 205
Size: 269 Color: 94

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 372
Size: 348 Color: 320
Size: 273 Color: 113

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 436
Size: 321 Color: 262
Size: 251 Color: 8

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 497
Size: 255 Color: 35
Size: 250 Color: 2

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 374
Size: 330 Color: 281
Size: 289 Color: 173

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 496
Size: 257 Color: 47
Size: 251 Color: 10

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 373
Size: 315 Color: 248
Size: 305 Color: 223

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 392
Size: 315 Color: 249
Size: 290 Color: 177

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 427
Size: 313 Color: 244
Size: 270 Color: 101

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 376
Size: 318 Color: 258
Size: 300 Color: 203

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 471
Size: 284 Color: 150
Size: 259 Color: 57

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 469
Size: 283 Color: 145
Size: 263 Color: 67

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 491
Size: 267 Color: 84
Size: 250 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 495
Size: 258 Color: 48
Size: 252 Color: 15

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 440
Size: 315 Color: 251
Size: 255 Color: 36

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 337
Size: 355 Color: 334
Size: 290 Color: 178

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 407
Size: 323 Color: 268
Size: 274 Color: 115

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 409
Size: 299 Color: 201
Size: 297 Color: 192

Bin 136: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 12
Size: 250 Color: 6
Size: 250 Color: 5
Size: 250 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 453
Size: 281 Color: 140
Size: 279 Color: 128

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 475
Size: 279 Color: 129
Size: 258 Color: 55

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 378
Size: 323 Color: 265
Size: 295 Color: 189

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 418
Size: 331 Color: 283
Size: 260 Color: 58

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 371
Size: 314 Color: 246
Size: 308 Color: 232

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 452
Size: 288 Color: 168
Size: 272 Color: 109

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 393
Size: 313 Color: 240
Size: 291 Color: 181

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 435
Size: 294 Color: 188
Size: 279 Color: 134

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 380
Size: 361 Color: 348
Size: 253 Color: 20

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 451
Size: 287 Color: 160
Size: 273 Color: 111

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 425
Size: 304 Color: 217
Size: 281 Color: 136

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 351
Size: 340 Color: 299
Size: 297 Color: 195

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 342
Size: 343 Color: 309
Size: 299 Color: 200

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 462
Size: 282 Color: 142
Size: 268 Color: 92

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 476
Size: 270 Color: 100
Size: 266 Color: 82

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 422
Size: 328 Color: 276
Size: 258 Color: 49

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 443
Size: 313 Color: 245
Size: 257 Color: 45

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 424
Size: 328 Color: 277
Size: 257 Color: 42

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 415
Size: 321 Color: 263
Size: 271 Color: 105

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 328
Size: 352 Color: 327
Size: 297 Color: 193

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 437
Size: 318 Color: 259
Size: 254 Color: 29

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 490
Size: 265 Color: 81
Size: 253 Color: 21

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 454
Size: 291 Color: 183
Size: 268 Color: 91

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 450
Size: 305 Color: 220
Size: 257 Color: 43

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 390
Size: 351 Color: 325
Size: 255 Color: 34

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 405
Size: 345 Color: 313
Size: 253 Color: 28

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 494
Size: 258 Color: 54
Size: 254 Color: 33

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 333
Size: 337 Color: 296
Size: 309 Color: 236

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 354
Size: 330 Color: 282
Size: 304 Color: 216

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 430
Size: 308 Color: 233
Size: 273 Color: 110

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 493
Size: 260 Color: 60
Size: 253 Color: 24

Total size: 167167
Total free space: 0

