Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 4
Size: 256 Color: 2
Size: 250 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 257 Color: 3
Size: 350 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 269 Color: 1
Size: 253 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 0
Size: 362 Color: 1
Size: 255 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 0
Size: 251 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 251 Color: 4
Size: 250 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 270 Color: 4
Size: 255 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 295 Color: 0
Size: 252 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 4
Size: 335 Color: 3
Size: 313 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 332 Color: 0
Size: 256 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 363 Color: 2
Size: 257 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 264 Color: 1
Size: 254 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 318 Color: 2
Size: 269 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 3
Size: 321 Color: 4
Size: 284 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 274 Color: 0
Size: 259 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 313 Color: 0
Size: 293 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 310 Color: 4
Size: 258 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 4
Size: 259 Color: 0
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 2
Size: 321 Color: 3
Size: 264 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 2
Size: 316 Color: 2
Size: 256 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 254 Color: 3
Size: 250 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 300 Color: 4
Size: 256 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 3
Size: 250 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 3
Size: 264 Color: 2
Size: 256 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 4
Size: 289 Color: 3
Size: 282 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 306 Color: 3
Size: 254 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 4
Size: 290 Color: 3
Size: 257 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 314 Color: 0
Size: 294 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 303 Color: 2
Size: 289 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 300 Color: 3
Size: 300 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 289 Color: 4
Size: 254 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 351 Color: 2
Size: 274 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 332 Color: 1
Size: 265 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 285 Color: 3
Size: 262 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 346 Color: 4
Size: 297 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 3
Size: 295 Color: 0
Size: 266 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 280 Color: 4
Size: 254 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 314 Color: 2
Size: 266 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 2
Size: 352 Color: 0
Size: 253 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 312 Color: 1
Size: 308 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 2
Size: 372 Color: 1
Size: 252 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 313 Color: 2
Size: 298 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 284 Color: 1
Size: 259 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 341 Color: 1
Size: 253 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 0
Size: 252 Color: 4
Size: 251 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 369 Color: 1
Size: 261 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 346 Color: 3
Size: 258 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 0
Size: 350 Color: 1
Size: 298 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 0
Size: 294 Color: 4
Size: 283 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 342 Color: 1
Size: 289 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 0
Size: 275 Color: 4
Size: 273 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 321 Color: 0
Size: 305 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 2
Size: 340 Color: 0
Size: 308 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 3
Size: 325 Color: 3
Size: 260 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 3
Size: 257 Color: 4
Size: 250 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 310 Color: 2
Size: 263 Color: 2

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 357 Color: 1
Size: 259 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 263 Color: 1
Size: 258 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 290 Color: 3
Size: 268 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 266 Color: 1
Size: 255 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 289 Color: 2
Size: 251 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 2
Size: 302 Color: 0
Size: 251 Color: 3

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 366 Color: 0
Size: 263 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 2
Size: 354 Color: 3
Size: 282 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 0
Size: 253 Color: 1
Size: 252 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 293 Color: 2
Size: 264 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 339 Color: 2
Size: 251 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 284 Color: 4
Size: 284 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 2
Size: 271 Color: 1
Size: 250 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 278 Color: 3
Size: 274 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 356 Color: 4
Size: 278 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 331 Color: 4
Size: 255 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 3
Size: 335 Color: 1
Size: 278 Color: 2

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 4
Size: 285 Color: 0
Size: 250 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 300 Color: 0
Size: 293 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 317 Color: 1
Size: 269 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 4
Size: 351 Color: 3
Size: 263 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 277 Color: 2
Size: 262 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 2
Size: 316 Color: 3
Size: 291 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 309 Color: 2
Size: 255 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 336 Color: 2
Size: 250 Color: 2

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 4
Size: 271 Color: 1
Size: 261 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 282 Color: 1
Size: 270 Color: 4

Total size: 83000
Total free space: 0

