Capicity Bin: 7552
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 1
Size: 1010 Color: 1
Size: 208 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4310 Color: 1
Size: 2702 Color: 1
Size: 540 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5799 Color: 1
Size: 1555 Color: 1
Size: 198 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 884 Color: 1
Size: 168 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 1
Size: 1432 Color: 0
Size: 924 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 1
Size: 3312 Color: 1
Size: 308 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4524 Color: 1
Size: 2828 Color: 1
Size: 200 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 1
Size: 504 Color: 0
Size: 356 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4217 Color: 1
Size: 3147 Color: 1
Size: 188 Color: 0

Bin 10: 0 of cap free
Amount of items: 4
Items: 
Size: 5577 Color: 1
Size: 1647 Color: 1
Size: 224 Color: 0
Size: 104 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4550 Color: 1
Size: 2742 Color: 1
Size: 260 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 1
Size: 724 Color: 1
Size: 224 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6173 Color: 1
Size: 1043 Color: 1
Size: 336 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4798 Color: 1
Size: 2458 Color: 1
Size: 296 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6592 Color: 1
Size: 800 Color: 1
Size: 160 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6419 Color: 1
Size: 905 Color: 1
Size: 228 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6790 Color: 1
Size: 638 Color: 1
Size: 124 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 1
Size: 1395 Color: 1
Size: 278 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6754 Color: 1
Size: 654 Color: 1
Size: 144 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6467 Color: 1
Size: 757 Color: 1
Size: 328 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 1
Size: 821 Color: 1
Size: 224 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5163 Color: 1
Size: 1991 Color: 1
Size: 398 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 1
Size: 702 Color: 1
Size: 184 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6601 Color: 1
Size: 793 Color: 1
Size: 158 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6345 Color: 1
Size: 1097 Color: 1
Size: 110 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 3778 Color: 1
Size: 3146 Color: 1
Size: 628 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6653 Color: 1
Size: 751 Color: 1
Size: 148 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 1
Size: 1002 Color: 1
Size: 208 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6015 Color: 1
Size: 1137 Color: 1
Size: 400 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4828 Color: 1
Size: 2276 Color: 1
Size: 448 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6046 Color: 1
Size: 1258 Color: 1
Size: 248 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 1
Size: 1048 Color: 1
Size: 352 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 5675 Color: 1
Size: 1461 Color: 1
Size: 416 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 5927 Color: 1
Size: 1399 Color: 1
Size: 226 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 1
Size: 754 Color: 1
Size: 412 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 1
Size: 600 Color: 1
Size: 180 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 1
Size: 1160 Color: 1
Size: 124 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6161 Color: 1
Size: 999 Color: 1
Size: 392 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 1
Size: 717 Color: 1
Size: 456 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 4972 Color: 1
Size: 2156 Color: 1
Size: 424 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 1
Size: 1076 Color: 1
Size: 122 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 1
Size: 1778 Color: 1
Size: 220 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 1
Size: 820 Color: 1
Size: 160 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5932 Color: 1
Size: 1484 Color: 1
Size: 136 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 1
Size: 1067 Color: 1
Size: 184 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 1
Size: 858 Color: 1
Size: 168 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5251 Color: 1
Size: 2079 Color: 1
Size: 222 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 1
Size: 1314 Color: 1
Size: 208 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6650 Color: 1
Size: 746 Color: 1
Size: 156 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5858 Color: 1
Size: 1176 Color: 1
Size: 518 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 1
Size: 1746 Color: 1
Size: 234 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 6321 Color: 1
Size: 999 Color: 1
Size: 232 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 6237 Color: 1
Size: 1161 Color: 1
Size: 154 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 1
Size: 1148 Color: 1
Size: 258 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 3780 Color: 1
Size: 3148 Color: 1
Size: 624 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 1
Size: 804 Color: 1
Size: 160 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 1
Size: 1012 Color: 1
Size: 200 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6444 Color: 1
Size: 796 Color: 1
Size: 312 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 6180 Color: 1
Size: 1244 Color: 1
Size: 128 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 3786 Color: 1
Size: 3142 Color: 1
Size: 624 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6230 Color: 1
Size: 1130 Color: 1
Size: 192 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6594 Color: 1
Size: 802 Color: 1
Size: 156 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 1
Size: 698 Color: 1
Size: 252 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5059 Color: 1
Size: 1565 Color: 1
Size: 928 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 1
Size: 885 Color: 1
Size: 312 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5772 Color: 1
Size: 1524 Color: 1
Size: 256 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 1
Size: 773 Color: 1
Size: 336 Color: 0

Bin 68: 0 of cap free
Amount of items: 4
Items: 
Size: 4193 Color: 1
Size: 2443 Color: 1
Size: 500 Color: 0
Size: 416 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 1
Size: 1020 Color: 1
Size: 476 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 1
Size: 1580 Color: 1
Size: 240 Color: 0

Bin 71: 0 of cap free
Amount of items: 5
Items: 
Size: 5148 Color: 1
Size: 1117 Color: 1
Size: 843 Color: 1
Size: 296 Color: 0
Size: 148 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 4963 Color: 1
Size: 2159 Color: 1
Size: 430 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 4214 Color: 1
Size: 2782 Color: 1
Size: 556 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 1
Size: 1292 Color: 1
Size: 320 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6718 Color: 1
Size: 666 Color: 1
Size: 168 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5850 Color: 1
Size: 1174 Color: 1
Size: 528 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5262 Color: 1
Size: 1910 Color: 1
Size: 380 Color: 0

Bin 78: 0 of cap free
Amount of items: 4
Items: 
Size: 4364 Color: 1
Size: 2808 Color: 1
Size: 232 Color: 0
Size: 148 Color: 0

Bin 79: 0 of cap free
Amount of items: 5
Items: 
Size: 3020 Color: 1
Size: 2654 Color: 1
Size: 1554 Color: 1
Size: 172 Color: 0
Size: 152 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 6518 Color: 1
Size: 742 Color: 1
Size: 292 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6539 Color: 1
Size: 871 Color: 1
Size: 142 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 3779 Color: 1
Size: 3145 Color: 1
Size: 628 Color: 0

Bin 83: 0 of cap free
Amount of items: 5
Items: 
Size: 4486 Color: 1
Size: 1598 Color: 1
Size: 1204 Color: 1
Size: 136 Color: 0
Size: 128 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 1
Size: 1348 Color: 1
Size: 200 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 1
Size: 2078 Color: 1
Size: 412 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 4262 Color: 1
Size: 2966 Color: 1
Size: 324 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5951 Color: 1
Size: 1335 Color: 1
Size: 266 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 6695 Color: 1
Size: 715 Color: 1
Size: 142 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 6060 Color: 1
Size: 1356 Color: 1
Size: 136 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 4621 Color: 1
Size: 2781 Color: 1
Size: 150 Color: 0

Bin 91: 0 of cap free
Amount of items: 5
Items: 
Size: 2660 Color: 1
Size: 2524 Color: 1
Size: 2100 Color: 1
Size: 136 Color: 0
Size: 132 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5420 Color: 1
Size: 1652 Color: 1
Size: 480 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 6434 Color: 1
Size: 862 Color: 1
Size: 256 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 4847 Color: 1
Size: 2255 Color: 1
Size: 450 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 5036 Color: 1
Size: 2298 Color: 1
Size: 218 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 6189 Color: 1
Size: 1318 Color: 1
Size: 44 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 6693 Color: 1
Size: 684 Color: 1
Size: 174 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 1
Size: 652 Color: 1
Size: 488 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 5411 Color: 1
Size: 1812 Color: 1
Size: 328 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 5789 Color: 1
Size: 1482 Color: 1
Size: 280 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 1
Size: 974 Color: 1
Size: 112 Color: 0

Bin 102: 1 of cap free
Amount of items: 4
Items: 
Size: 4437 Color: 1
Size: 2502 Color: 1
Size: 382 Color: 0
Size: 230 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 4686 Color: 1
Size: 2597 Color: 1
Size: 268 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 1
Size: 1081 Color: 1
Size: 74 Color: 0

Bin 105: 1 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 1665 Color: 1
Size: 196 Color: 0

Bin 106: 1 of cap free
Amount of items: 3
Items: 
Size: 4164 Color: 1
Size: 3237 Color: 1
Size: 150 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 5995 Color: 1
Size: 1181 Color: 1
Size: 374 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 1
Size: 1102 Color: 1
Size: 332 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 6137 Color: 1
Size: 1205 Color: 1
Size: 208 Color: 0

Bin 110: 2 of cap free
Amount of items: 5
Items: 
Size: 4970 Color: 1
Size: 1299 Color: 1
Size: 1007 Color: 1
Size: 226 Color: 0
Size: 48 Color: 0

Bin 111: 3 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 1
Size: 1785 Color: 1
Size: 104 Color: 0

Bin 112: 3 of cap free
Amount of items: 3
Items: 
Size: 5299 Color: 1
Size: 1642 Color: 1
Size: 608 Color: 0

Bin 113: 3 of cap free
Amount of items: 3
Items: 
Size: 6198 Color: 1
Size: 1151 Color: 1
Size: 200 Color: 0

Bin 114: 4 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 1
Size: 1414 Color: 1
Size: 548 Color: 0

Bin 115: 4 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 1
Size: 1043 Color: 1
Size: 292 Color: 0

Bin 116: 5 of cap free
Amount of items: 5
Items: 
Size: 3777 Color: 1
Size: 2004 Color: 1
Size: 1018 Color: 1
Size: 554 Color: 0
Size: 194 Color: 0

Bin 117: 5 of cap free
Amount of items: 3
Items: 
Size: 6541 Color: 1
Size: 934 Color: 1
Size: 72 Color: 0

Bin 118: 9 of cap free
Amount of items: 3
Items: 
Size: 5555 Color: 1
Size: 1724 Color: 1
Size: 264 Color: 0

Bin 119: 9 of cap free
Amount of items: 3
Items: 
Size: 5492 Color: 1
Size: 1879 Color: 1
Size: 172 Color: 0

Bin 120: 15 of cap free
Amount of items: 5
Items: 
Size: 3736 Color: 1
Size: 1964 Color: 1
Size: 945 Color: 1
Size: 628 Color: 0
Size: 264 Color: 0

Bin 121: 15 of cap free
Amount of items: 3
Items: 
Size: 6107 Color: 1
Size: 870 Color: 1
Size: 560 Color: 0

Bin 122: 27 of cap free
Amount of items: 3
Items: 
Size: 6567 Color: 1
Size: 794 Color: 1
Size: 164 Color: 0

Bin 123: 49 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 1
Size: 1281 Color: 1
Size: 248 Color: 0

Bin 124: 83 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 1
Size: 1919 Color: 1
Size: 128 Color: 0

Bin 125: 381 of cap free
Amount of items: 3
Items: 
Size: 5778 Color: 1
Size: 979 Color: 1
Size: 414 Color: 0

Bin 126: 716 of cap free
Amount of items: 3
Items: 
Size: 4318 Color: 1
Size: 2390 Color: 1
Size: 128 Color: 0

Bin 127: 782 of cap free
Amount of items: 1
Items: 
Size: 6770 Color: 1

Bin 128: 812 of cap free
Amount of items: 1
Items: 
Size: 6740 Color: 1

Bin 129: 838 of cap free
Amount of items: 1
Items: 
Size: 6714 Color: 1

Bin 130: 894 of cap free
Amount of items: 1
Items: 
Size: 6658 Color: 1

Bin 131: 907 of cap free
Amount of items: 1
Items: 
Size: 6645 Color: 1

Bin 132: 927 of cap free
Amount of items: 1
Items: 
Size: 6625 Color: 1

Bin 133: 1042 of cap free
Amount of items: 1
Items: 
Size: 6510 Color: 1

Total size: 996864
Total free space: 7552

