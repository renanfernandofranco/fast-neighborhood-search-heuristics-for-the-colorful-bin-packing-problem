Capicity Bin: 1916
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 962 Color: 1
Size: 282 Color: 0
Size: 212 Color: 1
Size: 202 Color: 0
Size: 190 Color: 0
Size: 34 Color: 1
Size: 34 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 967 Color: 1
Size: 433 Color: 0
Size: 300 Color: 0
Size: 112 Color: 0
Size: 104 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 1
Size: 714 Color: 0
Size: 56 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1295 Color: 0
Size: 555 Color: 0
Size: 66 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1350 Color: 1
Size: 530 Color: 0
Size: 36 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 0
Size: 461 Color: 0
Size: 90 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 0
Size: 383 Color: 0
Size: 76 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 0
Size: 355 Color: 0
Size: 46 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1531 Color: 0
Size: 251 Color: 1
Size: 134 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 0
Size: 318 Color: 0
Size: 36 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1613 Color: 0
Size: 173 Color: 1
Size: 130 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1622 Color: 0
Size: 158 Color: 1
Size: 136 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1637 Color: 1
Size: 233 Color: 0
Size: 46 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 246 Color: 0
Size: 32 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 0
Size: 219 Color: 0
Size: 44 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1667 Color: 1
Size: 209 Color: 1
Size: 40 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 0
Size: 171 Color: 1
Size: 56 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1709 Color: 0
Size: 191 Color: 1
Size: 16 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 1
Size: 158 Color: 1
Size: 52 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1204 Color: 0
Size: 673 Color: 1
Size: 38 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1233 Color: 1
Size: 642 Color: 0
Size: 40 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1582 Color: 0
Size: 293 Color: 1
Size: 40 Color: 1

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1617 Color: 0
Size: 298 Color: 1

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1711 Color: 0
Size: 204 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1282 Color: 1
Size: 582 Color: 1
Size: 50 Color: 0

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 1
Size: 326 Color: 0
Size: 102 Color: 0

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 0
Size: 321 Color: 1

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 0
Size: 171 Color: 0
Size: 110 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 0
Size: 197 Color: 1
Size: 4 Color: 0

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1062 Color: 0
Size: 791 Color: 1
Size: 60 Color: 1

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 1
Size: 651 Color: 1
Size: 44 Color: 0

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 0
Size: 559 Color: 0
Size: 76 Color: 1

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1331 Color: 0
Size: 582 Color: 1

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1439 Color: 0
Size: 474 Color: 1

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1538 Color: 1
Size: 375 Color: 0

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1672 Color: 0
Size: 221 Color: 1
Size: 20 Color: 0

Bin 37: 4 of cap free
Amount of items: 4
Items: 
Size: 959 Color: 0
Size: 685 Color: 0
Size: 216 Color: 1
Size: 52 Color: 1

Bin 38: 5 of cap free
Amount of items: 2
Items: 
Size: 1267 Color: 0
Size: 644 Color: 1

Bin 39: 5 of cap free
Amount of items: 2
Items: 
Size: 1467 Color: 1
Size: 444 Color: 0

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 0
Size: 362 Color: 1

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 0
Size: 309 Color: 1

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1674 Color: 0
Size: 237 Color: 1

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 1
Size: 234 Color: 0

Bin 44: 5 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 1
Size: 221 Color: 0
Size: 4 Color: 1

Bin 45: 6 of cap free
Amount of items: 3
Items: 
Size: 1137 Color: 0
Size: 615 Color: 1
Size: 158 Color: 0

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1648 Color: 1
Size: 262 Color: 0

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 971 Color: 1
Size: 798 Color: 0
Size: 140 Color: 1

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1390 Color: 0
Size: 519 Color: 1

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1681 Color: 0
Size: 228 Color: 1

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 963 Color: 0
Size: 789 Color: 0
Size: 156 Color: 1

Bin 51: 8 of cap free
Amount of items: 2
Items: 
Size: 1109 Color: 0
Size: 799 Color: 1

Bin 52: 8 of cap free
Amount of items: 2
Items: 
Size: 1251 Color: 0
Size: 657 Color: 1

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1511 Color: 0
Size: 396 Color: 1

Bin 54: 9 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 0
Size: 271 Color: 1
Size: 110 Color: 1

Bin 55: 11 of cap free
Amount of items: 2
Items: 
Size: 1133 Color: 0
Size: 772 Color: 1

Bin 56: 11 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 0
Size: 394 Color: 1
Size: 20 Color: 1

Bin 57: 12 of cap free
Amount of items: 21
Items: 
Size: 194 Color: 1
Size: 156 Color: 0
Size: 130 Color: 1
Size: 128 Color: 1
Size: 128 Color: 0
Size: 116 Color: 0
Size: 96 Color: 0
Size: 92 Color: 0
Size: 84 Color: 0
Size: 84 Color: 0
Size: 74 Color: 0
Size: 72 Color: 1
Size: 70 Color: 1
Size: 68 Color: 0
Size: 66 Color: 1
Size: 64 Color: 1
Size: 64 Color: 1
Size: 60 Color: 0
Size: 58 Color: 0
Size: 52 Color: 1
Size: 48 Color: 1

Bin 58: 12 of cap free
Amount of items: 2
Items: 
Size: 1565 Color: 1
Size: 339 Color: 0

Bin 59: 12 of cap free
Amount of items: 2
Items: 
Size: 1651 Color: 0
Size: 253 Color: 1

Bin 60: 16 of cap free
Amount of items: 2
Items: 
Size: 1247 Color: 1
Size: 653 Color: 0

Bin 61: 22 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 0
Size: 489 Color: 1

Bin 62: 25 of cap free
Amount of items: 3
Items: 
Size: 1129 Color: 0
Size: 427 Color: 1
Size: 335 Color: 0

Bin 63: 25 of cap free
Amount of items: 2
Items: 
Size: 1320 Color: 1
Size: 571 Color: 0

Bin 64: 26 of cap free
Amount of items: 2
Items: 
Size: 1095 Color: 0
Size: 795 Color: 1

Bin 65: 28 of cap free
Amount of items: 2
Items: 
Size: 1446 Color: 0
Size: 442 Color: 1

Bin 66: 1566 of cap free
Amount of items: 8
Items: 
Size: 58 Color: 0
Size: 48 Color: 1
Size: 44 Color: 1
Size: 44 Color: 1
Size: 42 Color: 0
Size: 40 Color: 1
Size: 40 Color: 0
Size: 34 Color: 0

Total size: 124540
Total free space: 1916

