Capicity Bin: 1000001
Lower Bound: 226

Bins used: 229
Amount of Colors: 20

Bin 1: 1 of cap free
Amount of items: 3
Items: 
Size: 696326 Color: 9
Size: 172016 Color: 13
Size: 131658 Color: 14

Bin 2: 2 of cap free
Amount of items: 3
Items: 
Size: 717006 Color: 8
Size: 165922 Color: 3
Size: 117071 Color: 17

Bin 3: 2 of cap free
Amount of items: 3
Items: 
Size: 671674 Color: 3
Size: 227821 Color: 4
Size: 100504 Color: 8

Bin 4: 2 of cap free
Amount of items: 3
Items: 
Size: 725576 Color: 13
Size: 173529 Color: 4
Size: 100894 Color: 19

Bin 5: 2 of cap free
Amount of items: 3
Items: 
Size: 501795 Color: 4
Size: 365585 Color: 0
Size: 132619 Color: 2

Bin 6: 2 of cap free
Amount of items: 4
Items: 
Size: 499765 Color: 8
Size: 175933 Color: 19
Size: 171118 Color: 7
Size: 153183 Color: 10

Bin 7: 2 of cap free
Amount of items: 4
Items: 
Size: 332358 Color: 13
Size: 331226 Color: 8
Size: 222098 Color: 17
Size: 114317 Color: 6

Bin 8: 3 of cap free
Amount of items: 3
Items: 
Size: 367857 Color: 13
Size: 343062 Color: 3
Size: 289079 Color: 16

Bin 9: 4 of cap free
Amount of items: 2
Items: 
Size: 759704 Color: 12
Size: 240293 Color: 9

Bin 10: 4 of cap free
Amount of items: 3
Items: 
Size: 486518 Color: 8
Size: 367843 Color: 10
Size: 145636 Color: 6

Bin 11: 5 of cap free
Amount of items: 3
Items: 
Size: 523633 Color: 12
Size: 256681 Color: 6
Size: 219682 Color: 5

Bin 12: 5 of cap free
Amount of items: 3
Items: 
Size: 671461 Color: 10
Size: 166073 Color: 11
Size: 162462 Color: 16

Bin 13: 5 of cap free
Amount of items: 2
Items: 
Size: 551606 Color: 0
Size: 448390 Color: 1

Bin 14: 6 of cap free
Amount of items: 3
Items: 
Size: 731285 Color: 0
Size: 158777 Color: 12
Size: 109933 Color: 16

Bin 15: 6 of cap free
Amount of items: 3
Items: 
Size: 691022 Color: 6
Size: 164515 Color: 8
Size: 144458 Color: 15

Bin 16: 8 of cap free
Amount of items: 3
Items: 
Size: 688454 Color: 0
Size: 187232 Color: 7
Size: 124307 Color: 18

Bin 17: 8 of cap free
Amount of items: 3
Items: 
Size: 520566 Color: 12
Size: 330404 Color: 1
Size: 149023 Color: 5

Bin 18: 8 of cap free
Amount of items: 3
Items: 
Size: 353894 Color: 16
Size: 343258 Color: 8
Size: 302841 Color: 7

Bin 19: 9 of cap free
Amount of items: 3
Items: 
Size: 627610 Color: 12
Size: 187508 Color: 0
Size: 184874 Color: 11

Bin 20: 9 of cap free
Amount of items: 3
Items: 
Size: 694255 Color: 6
Size: 165759 Color: 15
Size: 139978 Color: 4

Bin 21: 9 of cap free
Amount of items: 3
Items: 
Size: 700796 Color: 1
Size: 167141 Color: 10
Size: 132055 Color: 1

Bin 22: 11 of cap free
Amount of items: 3
Items: 
Size: 645576 Color: 8
Size: 251401 Color: 12
Size: 103013 Color: 2

Bin 23: 15 of cap free
Amount of items: 2
Items: 
Size: 670875 Color: 15
Size: 329111 Color: 16

Bin 24: 16 of cap free
Amount of items: 3
Items: 
Size: 710842 Color: 5
Size: 148564 Color: 9
Size: 140579 Color: 5

Bin 25: 17 of cap free
Amount of items: 3
Items: 
Size: 771690 Color: 18
Size: 116097 Color: 8
Size: 112197 Color: 14

Bin 26: 21 of cap free
Amount of items: 3
Items: 
Size: 711413 Color: 5
Size: 181559 Color: 11
Size: 107008 Color: 6

Bin 27: 23 of cap free
Amount of items: 3
Items: 
Size: 537523 Color: 8
Size: 357690 Color: 4
Size: 104765 Color: 17

Bin 28: 25 of cap free
Amount of items: 3
Items: 
Size: 725252 Color: 14
Size: 138454 Color: 5
Size: 136270 Color: 9

Bin 29: 27 of cap free
Amount of items: 3
Items: 
Size: 658747 Color: 19
Size: 196326 Color: 5
Size: 144901 Color: 10

Bin 30: 31 of cap free
Amount of items: 2
Items: 
Size: 520492 Color: 0
Size: 479478 Color: 10

Bin 31: 35 of cap free
Amount of items: 3
Items: 
Size: 742101 Color: 2
Size: 140186 Color: 2
Size: 117679 Color: 9

Bin 32: 36 of cap free
Amount of items: 3
Items: 
Size: 653868 Color: 1
Size: 182924 Color: 15
Size: 163173 Color: 1

Bin 33: 48 of cap free
Amount of items: 3
Items: 
Size: 746920 Color: 13
Size: 134462 Color: 14
Size: 118571 Color: 1

Bin 34: 51 of cap free
Amount of items: 3
Items: 
Size: 643850 Color: 5
Size: 178096 Color: 9
Size: 178004 Color: 18

Bin 35: 55 of cap free
Amount of items: 3
Items: 
Size: 502946 Color: 12
Size: 364732 Color: 2
Size: 132268 Color: 0

Bin 36: 58 of cap free
Amount of items: 2
Items: 
Size: 653440 Color: 5
Size: 346503 Color: 1

Bin 37: 61 of cap free
Amount of items: 2
Items: 
Size: 527320 Color: 16
Size: 472620 Color: 12

Bin 38: 62 of cap free
Amount of items: 3
Items: 
Size: 681517 Color: 17
Size: 201826 Color: 11
Size: 116596 Color: 13

Bin 39: 69 of cap free
Amount of items: 2
Items: 
Size: 555619 Color: 5
Size: 444313 Color: 12

Bin 40: 70 of cap free
Amount of items: 3
Items: 
Size: 344823 Color: 8
Size: 332949 Color: 5
Size: 322159 Color: 19

Bin 41: 78 of cap free
Amount of items: 3
Items: 
Size: 689488 Color: 0
Size: 182750 Color: 16
Size: 127685 Color: 1

Bin 42: 78 of cap free
Amount of items: 3
Items: 
Size: 499398 Color: 10
Size: 398222 Color: 17
Size: 102303 Color: 2

Bin 43: 86 of cap free
Amount of items: 2
Items: 
Size: 659078 Color: 15
Size: 340837 Color: 7

Bin 44: 91 of cap free
Amount of items: 3
Items: 
Size: 687347 Color: 15
Size: 194224 Color: 1
Size: 118339 Color: 1

Bin 45: 93 of cap free
Amount of items: 2
Items: 
Size: 723494 Color: 1
Size: 276414 Color: 16

Bin 46: 94 of cap free
Amount of items: 2
Items: 
Size: 600372 Color: 15
Size: 399535 Color: 12

Bin 47: 103 of cap free
Amount of items: 2
Items: 
Size: 501273 Color: 15
Size: 498625 Color: 2

Bin 48: 119 of cap free
Amount of items: 3
Items: 
Size: 528449 Color: 19
Size: 296132 Color: 6
Size: 175301 Color: 14

Bin 49: 138 of cap free
Amount of items: 3
Items: 
Size: 675540 Color: 14
Size: 182725 Color: 13
Size: 141598 Color: 18

Bin 50: 141 of cap free
Amount of items: 3
Items: 
Size: 663824 Color: 0
Size: 170714 Color: 8
Size: 165322 Color: 18

Bin 51: 142 of cap free
Amount of items: 2
Items: 
Size: 567142 Color: 19
Size: 432717 Color: 6

Bin 52: 150 of cap free
Amount of items: 2
Items: 
Size: 546091 Color: 7
Size: 453760 Color: 15

Bin 53: 153 of cap free
Amount of items: 3
Items: 
Size: 697754 Color: 11
Size: 162728 Color: 13
Size: 139366 Color: 4

Bin 54: 155 of cap free
Amount of items: 3
Items: 
Size: 751075 Color: 7
Size: 146474 Color: 0
Size: 102297 Color: 14

Bin 55: 167 of cap free
Amount of items: 2
Items: 
Size: 617770 Color: 16
Size: 382064 Color: 7

Bin 56: 176 of cap free
Amount of items: 2
Items: 
Size: 550020 Color: 16
Size: 449805 Color: 12

Bin 57: 182 of cap free
Amount of items: 2
Items: 
Size: 701072 Color: 8
Size: 298747 Color: 1

Bin 58: 182 of cap free
Amount of items: 2
Items: 
Size: 756684 Color: 8
Size: 243135 Color: 5

Bin 59: 199 of cap free
Amount of items: 2
Items: 
Size: 618989 Color: 15
Size: 380813 Color: 6

Bin 60: 213 of cap free
Amount of items: 2
Items: 
Size: 597961 Color: 18
Size: 401827 Color: 17

Bin 61: 230 of cap free
Amount of items: 2
Items: 
Size: 732749 Color: 8
Size: 267022 Color: 2

Bin 62: 233 of cap free
Amount of items: 2
Items: 
Size: 525658 Color: 6
Size: 474110 Color: 3

Bin 63: 248 of cap free
Amount of items: 2
Items: 
Size: 569688 Color: 15
Size: 430065 Color: 14

Bin 64: 259 of cap free
Amount of items: 3
Items: 
Size: 397560 Color: 9
Size: 384352 Color: 11
Size: 217830 Color: 14

Bin 65: 263 of cap free
Amount of items: 2
Items: 
Size: 503203 Color: 1
Size: 496535 Color: 14

Bin 66: 265 of cap free
Amount of items: 2
Items: 
Size: 558467 Color: 4
Size: 441269 Color: 2

Bin 67: 267 of cap free
Amount of items: 3
Items: 
Size: 520360 Color: 14
Size: 306041 Color: 5
Size: 173333 Color: 16

Bin 68: 274 of cap free
Amount of items: 3
Items: 
Size: 699656 Color: 15
Size: 183978 Color: 9
Size: 116093 Color: 12

Bin 69: 275 of cap free
Amount of items: 2
Items: 
Size: 638190 Color: 8
Size: 361536 Color: 3

Bin 70: 290 of cap free
Amount of items: 2
Items: 
Size: 602563 Color: 15
Size: 397148 Color: 12

Bin 71: 308 of cap free
Amount of items: 2
Items: 
Size: 734174 Color: 18
Size: 265519 Color: 15

Bin 72: 318 of cap free
Amount of items: 3
Items: 
Size: 617685 Color: 14
Size: 199737 Color: 15
Size: 182261 Color: 7

Bin 73: 325 of cap free
Amount of items: 2
Items: 
Size: 523162 Color: 3
Size: 476514 Color: 8

Bin 74: 330 of cap free
Amount of items: 2
Items: 
Size: 658267 Color: 1
Size: 341404 Color: 10

Bin 75: 331 of cap free
Amount of items: 2
Items: 
Size: 614195 Color: 5
Size: 385475 Color: 19

Bin 76: 340 of cap free
Amount of items: 2
Items: 
Size: 527186 Color: 14
Size: 472475 Color: 3

Bin 77: 343 of cap free
Amount of items: 2
Items: 
Size: 559154 Color: 17
Size: 440504 Color: 15

Bin 78: 347 of cap free
Amount of items: 2
Items: 
Size: 768938 Color: 19
Size: 230716 Color: 17

Bin 79: 359 of cap free
Amount of items: 2
Items: 
Size: 707968 Color: 9
Size: 291674 Color: 4

Bin 80: 362 of cap free
Amount of items: 2
Items: 
Size: 790639 Color: 3
Size: 209000 Color: 7

Bin 81: 365 of cap free
Amount of items: 3
Items: 
Size: 626365 Color: 5
Size: 188403 Color: 15
Size: 184868 Color: 10

Bin 82: 389 of cap free
Amount of items: 2
Items: 
Size: 564086 Color: 5
Size: 435526 Color: 13

Bin 83: 396 of cap free
Amount of items: 2
Items: 
Size: 601442 Color: 13
Size: 398163 Color: 16

Bin 84: 397 of cap free
Amount of items: 2
Items: 
Size: 594289 Color: 2
Size: 405315 Color: 13

Bin 85: 404 of cap free
Amount of items: 2
Items: 
Size: 733321 Color: 14
Size: 266276 Color: 19

Bin 86: 404 of cap free
Amount of items: 2
Items: 
Size: 764542 Color: 18
Size: 235055 Color: 0

Bin 87: 426 of cap free
Amount of items: 2
Items: 
Size: 619909 Color: 16
Size: 379666 Color: 9

Bin 88: 438 of cap free
Amount of items: 2
Items: 
Size: 531224 Color: 3
Size: 468339 Color: 10

Bin 89: 441 of cap free
Amount of items: 3
Items: 
Size: 593016 Color: 8
Size: 226588 Color: 1
Size: 179956 Color: 2

Bin 90: 441 of cap free
Amount of items: 2
Items: 
Size: 534470 Color: 0
Size: 465090 Color: 1

Bin 91: 450 of cap free
Amount of items: 2
Items: 
Size: 663102 Color: 9
Size: 336449 Color: 0

Bin 92: 464 of cap free
Amount of items: 3
Items: 
Size: 686985 Color: 14
Size: 189589 Color: 10
Size: 122963 Color: 14

Bin 93: 467 of cap free
Amount of items: 2
Items: 
Size: 629380 Color: 16
Size: 370154 Color: 13

Bin 94: 516 of cap free
Amount of items: 2
Items: 
Size: 767045 Color: 14
Size: 232440 Color: 1

Bin 95: 520 of cap free
Amount of items: 2
Items: 
Size: 661655 Color: 10
Size: 337826 Color: 15

Bin 96: 541 of cap free
Amount of items: 2
Items: 
Size: 511553 Color: 10
Size: 487907 Color: 16

Bin 97: 561 of cap free
Amount of items: 3
Items: 
Size: 361974 Color: 8
Size: 358130 Color: 3
Size: 279336 Color: 2

Bin 98: 567 of cap free
Amount of items: 2
Items: 
Size: 614739 Color: 5
Size: 384695 Color: 7

Bin 99: 572 of cap free
Amount of items: 2
Items: 
Size: 753211 Color: 2
Size: 246218 Color: 6

Bin 100: 574 of cap free
Amount of items: 2
Items: 
Size: 772895 Color: 1
Size: 226532 Color: 11

Bin 101: 587 of cap free
Amount of items: 3
Items: 
Size: 644967 Color: 1
Size: 194869 Color: 1
Size: 159578 Color: 8

Bin 102: 590 of cap free
Amount of items: 2
Items: 
Size: 707805 Color: 9
Size: 291606 Color: 0

Bin 103: 608 of cap free
Amount of items: 2
Items: 
Size: 603116 Color: 5
Size: 396277 Color: 10

Bin 104: 621 of cap free
Amount of items: 2
Items: 
Size: 683445 Color: 4
Size: 315935 Color: 2

Bin 105: 630 of cap free
Amount of items: 2
Items: 
Size: 789630 Color: 4
Size: 209741 Color: 14

Bin 106: 672 of cap free
Amount of items: 2
Items: 
Size: 773955 Color: 5
Size: 225374 Color: 18

Bin 107: 677 of cap free
Amount of items: 2
Items: 
Size: 742853 Color: 11
Size: 256471 Color: 7

Bin 108: 725 of cap free
Amount of items: 2
Items: 
Size: 514634 Color: 14
Size: 484642 Color: 1

Bin 109: 743 of cap free
Amount of items: 2
Items: 
Size: 777297 Color: 0
Size: 221961 Color: 14

Bin 110: 751 of cap free
Amount of items: 2
Items: 
Size: 747248 Color: 3
Size: 252002 Color: 19

Bin 111: 765 of cap free
Amount of items: 2
Items: 
Size: 775841 Color: 15
Size: 223395 Color: 9

Bin 112: 787 of cap free
Amount of items: 2
Items: 
Size: 565641 Color: 2
Size: 433573 Color: 9

Bin 113: 830 of cap free
Amount of items: 3
Items: 
Size: 389859 Color: 18
Size: 333449 Color: 19
Size: 275863 Color: 18

Bin 114: 878 of cap free
Amount of items: 2
Items: 
Size: 621121 Color: 3
Size: 378002 Color: 4

Bin 115: 887 of cap free
Amount of items: 2
Items: 
Size: 562958 Color: 17
Size: 436156 Color: 16

Bin 116: 929 of cap free
Amount of items: 2
Items: 
Size: 701530 Color: 13
Size: 297542 Color: 18

Bin 117: 944 of cap free
Amount of items: 2
Items: 
Size: 757555 Color: 12
Size: 241502 Color: 0

Bin 118: 968 of cap free
Amount of items: 2
Items: 
Size: 735544 Color: 17
Size: 263489 Color: 15

Bin 119: 974 of cap free
Amount of items: 2
Items: 
Size: 585808 Color: 17
Size: 413219 Color: 0

Bin 120: 1017 of cap free
Amount of items: 2
Items: 
Size: 599798 Color: 14
Size: 399186 Color: 18

Bin 121: 1029 of cap free
Amount of items: 2
Items: 
Size: 513002 Color: 17
Size: 485970 Color: 15

Bin 122: 1043 of cap free
Amount of items: 2
Items: 
Size: 570007 Color: 9
Size: 428951 Color: 16

Bin 123: 1101 of cap free
Amount of items: 2
Items: 
Size: 528758 Color: 1
Size: 470142 Color: 13

Bin 124: 1127 of cap free
Amount of items: 3
Items: 
Size: 648094 Color: 6
Size: 185549 Color: 8
Size: 165231 Color: 4

Bin 125: 1160 of cap free
Amount of items: 2
Items: 
Size: 720848 Color: 5
Size: 277993 Color: 14

Bin 126: 1171 of cap free
Amount of items: 2
Items: 
Size: 729712 Color: 18
Size: 269118 Color: 10

Bin 127: 1172 of cap free
Amount of items: 2
Items: 
Size: 738293 Color: 10
Size: 260536 Color: 17

Bin 128: 1195 of cap free
Amount of items: 2
Items: 
Size: 530869 Color: 6
Size: 467937 Color: 11

Bin 129: 1212 of cap free
Amount of items: 2
Items: 
Size: 606892 Color: 5
Size: 391897 Color: 12

Bin 130: 1229 of cap free
Amount of items: 2
Items: 
Size: 539050 Color: 5
Size: 459722 Color: 16

Bin 131: 1269 of cap free
Amount of items: 2
Items: 
Size: 553342 Color: 9
Size: 445390 Color: 7

Bin 132: 1295 of cap free
Amount of items: 2
Items: 
Size: 733941 Color: 18
Size: 264765 Color: 17

Bin 133: 1434 of cap free
Amount of items: 2
Items: 
Size: 553220 Color: 10
Size: 445347 Color: 18

Bin 134: 1444 of cap free
Amount of items: 2
Items: 
Size: 778775 Color: 4
Size: 219782 Color: 6

Bin 135: 1462 of cap free
Amount of items: 2
Items: 
Size: 660079 Color: 18
Size: 338460 Color: 13

Bin 136: 1517 of cap free
Amount of items: 2
Items: 
Size: 699323 Color: 18
Size: 299161 Color: 16

Bin 137: 1525 of cap free
Amount of items: 2
Items: 
Size: 794669 Color: 15
Size: 203807 Color: 13

Bin 138: 1553 of cap free
Amount of items: 2
Items: 
Size: 743599 Color: 19
Size: 254849 Color: 0

Bin 139: 1575 of cap free
Amount of items: 2
Items: 
Size: 512972 Color: 5
Size: 485454 Color: 8

Bin 140: 1599 of cap free
Amount of items: 2
Items: 
Size: 585380 Color: 1
Size: 413022 Color: 0

Bin 141: 1624 of cap free
Amount of items: 2
Items: 
Size: 553160 Color: 13
Size: 445217 Color: 8

Bin 142: 1649 of cap free
Amount of items: 2
Items: 
Size: 571678 Color: 16
Size: 426674 Color: 4

Bin 143: 1712 of cap free
Amount of items: 2
Items: 
Size: 693183 Color: 10
Size: 305106 Color: 13

Bin 144: 1779 of cap free
Amount of items: 2
Items: 
Size: 763297 Color: 13
Size: 234925 Color: 9

Bin 145: 1812 of cap free
Amount of items: 2
Items: 
Size: 707590 Color: 7
Size: 290599 Color: 1

Bin 146: 1816 of cap free
Amount of items: 2
Items: 
Size: 574135 Color: 8
Size: 424050 Color: 1

Bin 147: 1836 of cap free
Amount of items: 2
Items: 
Size: 686197 Color: 9
Size: 311968 Color: 2

Bin 148: 1861 of cap free
Amount of items: 2
Items: 
Size: 561875 Color: 15
Size: 436265 Color: 17

Bin 149: 1876 of cap free
Amount of items: 2
Items: 
Size: 608691 Color: 0
Size: 389434 Color: 12

Bin 150: 1976 of cap free
Amount of items: 2
Items: 
Size: 543931 Color: 6
Size: 454094 Color: 7

Bin 151: 1979 of cap free
Amount of items: 2
Items: 
Size: 664924 Color: 2
Size: 333098 Color: 11

Bin 152: 2023 of cap free
Amount of items: 2
Items: 
Size: 618599 Color: 10
Size: 379379 Color: 2

Bin 153: 2031 of cap free
Amount of items: 2
Items: 
Size: 634787 Color: 8
Size: 363183 Color: 2

Bin 154: 2265 of cap free
Amount of items: 2
Items: 
Size: 756907 Color: 2
Size: 240829 Color: 0

Bin 155: 2321 of cap free
Amount of items: 2
Items: 
Size: 504305 Color: 6
Size: 493375 Color: 5

Bin 156: 2336 of cap free
Amount of items: 2
Items: 
Size: 767000 Color: 8
Size: 230665 Color: 6

Bin 157: 2379 of cap free
Amount of items: 2
Items: 
Size: 762880 Color: 1
Size: 234742 Color: 13

Bin 158: 2450 of cap free
Amount of items: 2
Items: 
Size: 525331 Color: 18
Size: 472220 Color: 10

Bin 159: 2570 of cap free
Amount of items: 2
Items: 
Size: 613272 Color: 2
Size: 384159 Color: 15

Bin 160: 2714 of cap free
Amount of items: 2
Items: 
Size: 625252 Color: 11
Size: 372035 Color: 18

Bin 161: 2791 of cap free
Amount of items: 2
Items: 
Size: 596580 Color: 17
Size: 400630 Color: 11

Bin 162: 3006 of cap free
Amount of items: 2
Items: 
Size: 605422 Color: 2
Size: 391573 Color: 9

Bin 163: 3027 of cap free
Amount of items: 2
Items: 
Size: 693112 Color: 7
Size: 303862 Color: 11

Bin 164: 3066 of cap free
Amount of items: 2
Items: 
Size: 732175 Color: 8
Size: 264760 Color: 5

Bin 165: 3105 of cap free
Amount of items: 2
Items: 
Size: 698313 Color: 19
Size: 298583 Color: 1

Bin 166: 3170 of cap free
Amount of items: 2
Items: 
Size: 537904 Color: 7
Size: 458927 Color: 5

Bin 167: 3180 of cap free
Amount of items: 2
Items: 
Size: 746461 Color: 17
Size: 250360 Color: 1

Bin 168: 3278 of cap free
Amount of items: 2
Items: 
Size: 529926 Color: 3
Size: 466797 Color: 18

Bin 169: 3445 of cap free
Amount of items: 2
Items: 
Size: 515599 Color: 9
Size: 480957 Color: 4

Bin 170: 3792 of cap free
Amount of items: 2
Items: 
Size: 591751 Color: 0
Size: 404458 Color: 16

Bin 171: 3810 of cap free
Amount of items: 2
Items: 
Size: 636300 Color: 2
Size: 359891 Color: 5

Bin 172: 3955 of cap free
Amount of items: 2
Items: 
Size: 731295 Color: 11
Size: 264751 Color: 2

Bin 173: 4019 of cap free
Amount of items: 2
Items: 
Size: 646076 Color: 16
Size: 349906 Color: 1

Bin 174: 4066 of cap free
Amount of items: 2
Items: 
Size: 519045 Color: 10
Size: 476890 Color: 8

Bin 175: 4098 of cap free
Amount of items: 2
Items: 
Size: 624834 Color: 11
Size: 371069 Color: 0

Bin 176: 4379 of cap free
Amount of items: 2
Items: 
Size: 717755 Color: 13
Size: 277867 Color: 8

Bin 177: 4565 of cap free
Amount of items: 2
Items: 
Size: 681149 Color: 5
Size: 314287 Color: 0

Bin 178: 4888 of cap free
Amount of items: 2
Items: 
Size: 709505 Color: 14
Size: 285608 Color: 11

Bin 179: 5179 of cap free
Amount of items: 2
Items: 
Size: 550790 Color: 7
Size: 444032 Color: 12

Bin 180: 5275 of cap free
Amount of items: 2
Items: 
Size: 792535 Color: 11
Size: 202191 Color: 17

Bin 181: 5554 of cap free
Amount of items: 2
Items: 
Size: 766302 Color: 2
Size: 228145 Color: 3

Bin 182: 5814 of cap free
Amount of items: 2
Items: 
Size: 536523 Color: 13
Size: 457664 Color: 8

Bin 183: 5915 of cap free
Amount of items: 2
Items: 
Size: 624447 Color: 1
Size: 369639 Color: 15

Bin 184: 5955 of cap free
Amount of items: 2
Items: 
Size: 759682 Color: 14
Size: 234364 Color: 7

Bin 185: 6859 of cap free
Amount of items: 2
Items: 
Size: 690472 Color: 12
Size: 302670 Color: 3

Bin 186: 7061 of cap free
Amount of items: 2
Items: 
Size: 634246 Color: 14
Size: 358694 Color: 19

Bin 187: 7232 of cap free
Amount of items: 2
Items: 
Size: 503627 Color: 15
Size: 489142 Color: 7

Bin 188: 7241 of cap free
Amount of items: 2
Items: 
Size: 624288 Color: 0
Size: 368472 Color: 9

Bin 189: 7346 of cap free
Amount of items: 2
Items: 
Size: 662657 Color: 14
Size: 329998 Color: 7

Bin 190: 7452 of cap free
Amount of items: 2
Items: 
Size: 707555 Color: 9
Size: 284994 Color: 19

Bin 191: 7543 of cap free
Amount of items: 2
Items: 
Size: 536223 Color: 17
Size: 456235 Color: 14

Bin 192: 7564 of cap free
Amount of items: 2
Items: 
Size: 569580 Color: 12
Size: 422857 Color: 17

Bin 193: 7785 of cap free
Amount of items: 2
Items: 
Size: 792022 Color: 16
Size: 200194 Color: 7

Bin 194: 8000 of cap free
Amount of items: 2
Items: 
Size: 591457 Color: 5
Size: 400544 Color: 0

Bin 195: 9091 of cap free
Amount of items: 2
Items: 
Size: 727851 Color: 5
Size: 263059 Color: 19

Bin 196: 10036 of cap free
Amount of items: 2
Items: 
Size: 568231 Color: 6
Size: 421734 Color: 12

Bin 197: 10371 of cap free
Amount of items: 2
Items: 
Size: 755324 Color: 15
Size: 234306 Color: 10

Bin 198: 12399 of cap free
Amount of items: 2
Items: 
Size: 604719 Color: 2
Size: 382883 Color: 13

Bin 199: 12547 of cap free
Amount of items: 2
Items: 
Size: 502036 Color: 11
Size: 485418 Color: 19

Bin 200: 13051 of cap free
Amount of items: 2
Items: 
Size: 535053 Color: 3
Size: 451897 Color: 2

Bin 201: 13505 of cap free
Amount of items: 2
Items: 
Size: 502029 Color: 2
Size: 484467 Color: 6

Bin 202: 13634 of cap free
Amount of items: 2
Items: 
Size: 568213 Color: 17
Size: 418154 Color: 12

Bin 203: 14319 of cap free
Amount of items: 2
Items: 
Size: 788391 Color: 16
Size: 197291 Color: 11

Bin 204: 16357 of cap free
Amount of items: 2
Items: 
Size: 658852 Color: 8
Size: 324792 Color: 3

Bin 205: 17321 of cap free
Amount of items: 2
Items: 
Size: 727551 Color: 9
Size: 255129 Color: 19

Bin 206: 18199 of cap free
Amount of items: 2
Items: 
Size: 786081 Color: 2
Size: 195721 Color: 19

Bin 207: 18578 of cap free
Amount of items: 2
Items: 
Size: 563924 Color: 3
Size: 417499 Color: 19

Bin 208: 19061 of cap free
Amount of items: 2
Items: 
Size: 705203 Color: 16
Size: 275737 Color: 18

Bin 209: 20390 of cap free
Amount of items: 2
Items: 
Size: 528320 Color: 16
Size: 451291 Color: 1

Bin 210: 22688 of cap free
Amount of items: 2
Items: 
Size: 560931 Color: 9
Size: 416382 Color: 3

Bin 211: 23748 of cap free
Amount of items: 2
Items: 
Size: 559880 Color: 10
Size: 416373 Color: 0

Bin 212: 26402 of cap free
Amount of items: 2
Items: 
Size: 560898 Color: 0
Size: 412701 Color: 8

Bin 213: 28121 of cap free
Amount of items: 2
Items: 
Size: 528141 Color: 12
Size: 443739 Color: 1

Bin 214: 28178 of cap free
Amount of items: 2
Items: 
Size: 656353 Color: 14
Size: 315470 Color: 5

Bin 215: 29824 of cap free
Amount of items: 2
Items: 
Size: 528091 Color: 1
Size: 442086 Color: 0

Bin 216: 32632 of cap free
Amount of items: 2
Items: 
Size: 558254 Color: 15
Size: 409115 Color: 19

Bin 217: 59654 of cap free
Amount of items: 2
Items: 
Size: 787063 Color: 19
Size: 153284 Color: 7

Bin 218: 107373 of cap free
Amount of items: 2
Items: 
Size: 524183 Color: 17
Size: 368445 Color: 19

Bin 219: 214760 of cap free
Amount of items: 1
Items: 
Size: 785241 Color: 8

Bin 220: 214837 of cap free
Amount of items: 1
Items: 
Size: 785164 Color: 18

Bin 221: 215107 of cap free
Amount of items: 1
Items: 
Size: 784894 Color: 4

Bin 222: 217621 of cap free
Amount of items: 1
Items: 
Size: 782380 Color: 9

Bin 223: 225098 of cap free
Amount of items: 1
Items: 
Size: 774903 Color: 0

Bin 224: 228118 of cap free
Amount of items: 1
Items: 
Size: 771883 Color: 0

Bin 225: 233876 of cap free
Amount of items: 1
Items: 
Size: 766125 Color: 1

Bin 226: 240802 of cap free
Amount of items: 1
Items: 
Size: 759199 Color: 10

Bin 227: 246946 of cap free
Amount of items: 1
Items: 
Size: 753055 Color: 18

Bin 228: 272827 of cap free
Amount of items: 1
Items: 
Size: 727174 Color: 16

Bin 229: 282980 of cap free
Amount of items: 1
Items: 
Size: 717021 Color: 18

Total size: 225554786
Total free space: 3445443

