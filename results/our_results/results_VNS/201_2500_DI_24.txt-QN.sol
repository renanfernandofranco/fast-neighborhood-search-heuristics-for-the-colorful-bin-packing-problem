Capicity Bin: 2032
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 159
Size: 559 Color: 120
Size: 16 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 163
Size: 420 Color: 111
Size: 78 Color: 46

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 166
Size: 418 Color: 110
Size: 36 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 168
Size: 363 Color: 103
Size: 78 Color: 48

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 176
Size: 293 Color: 95
Size: 58 Color: 34

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 177
Size: 314 Color: 97
Size: 36 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1722 Color: 182
Size: 276 Color: 92
Size: 34 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 183
Size: 197 Color: 75
Size: 108 Color: 57

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1744 Color: 185
Size: 168 Color: 68
Size: 120 Color: 59

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 187
Size: 228 Color: 82
Size: 46 Color: 23

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 190
Size: 190 Color: 72
Size: 60 Color: 35

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 191
Size: 201 Color: 76
Size: 48 Color: 26

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 192
Size: 206 Color: 77
Size: 40 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 195
Size: 191 Color: 73
Size: 44 Color: 20

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 197
Size: 177 Color: 70
Size: 50 Color: 28

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 198
Size: 186 Color: 71
Size: 40 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 199
Size: 158 Color: 66
Size: 64 Color: 41

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1334 Color: 153
Size: 673 Color: 126
Size: 24 Color: 3

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1478 Color: 160
Size: 553 Color: 119

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 172
Size: 262 Color: 90
Size: 126 Color: 60

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 1649 Color: 173
Size: 382 Color: 106

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 178
Size: 338 Color: 102
Size: 8 Color: 1

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1791 Color: 194
Size: 240 Color: 87

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1801 Color: 196
Size: 230 Color: 85

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1821 Color: 200
Size: 210 Color: 80

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1825 Color: 201
Size: 206 Color: 78

Bin 27: 2 of cap free
Amount of items: 7
Items: 
Size: 1018 Color: 139
Size: 316 Color: 98
Size: 291 Color: 94
Size: 251 Color: 88
Size: 56 Color: 33
Size: 50 Color: 27
Size: 48 Color: 25

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1587 Color: 167
Size: 443 Color: 112

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 181
Size: 321 Color: 100

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1759 Color: 188
Size: 271 Color: 91

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 189
Size: 255 Color: 89
Size: 4 Color: 0

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 156
Size: 461 Color: 113
Size: 168 Color: 67

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1618 Color: 169
Size: 411 Color: 109

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 175
Size: 371 Color: 105

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1790 Color: 193
Size: 239 Color: 86

Bin 36: 5 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 145
Size: 820 Color: 133
Size: 40 Color: 15

Bin 37: 5 of cap free
Amount of items: 2
Items: 
Size: 1702 Color: 179
Size: 325 Color: 101

Bin 38: 6 of cap free
Amount of items: 5
Items: 
Size: 1079 Color: 141
Size: 795 Color: 132
Size: 64 Color: 40
Size: 44 Color: 22
Size: 44 Color: 21

Bin 39: 7 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 157
Size: 611 Color: 122

Bin 40: 7 of cap free
Amount of items: 2
Items: 
Size: 1563 Color: 165
Size: 462 Color: 114

Bin 41: 7 of cap free
Amount of items: 2
Items: 
Size: 1708 Color: 180
Size: 317 Color: 99

Bin 42: 7 of cap free
Amount of items: 2
Items: 
Size: 1731 Color: 184
Size: 294 Color: 96

Bin 43: 7 of cap free
Amount of items: 2
Items: 
Size: 1747 Color: 186
Size: 278 Color: 93

Bin 44: 8 of cap free
Amount of items: 3
Items: 
Size: 1265 Color: 149
Size: 721 Color: 128
Size: 38 Color: 12

Bin 45: 8 of cap free
Amount of items: 3
Items: 
Size: 1379 Color: 154
Size: 472 Color: 115
Size: 173 Color: 69

Bin 46: 8 of cap free
Amount of items: 2
Items: 
Size: 1383 Color: 155
Size: 641 Color: 123

Bin 47: 9 of cap free
Amount of items: 2
Items: 
Size: 1441 Color: 158
Size: 582 Color: 121

Bin 48: 10 of cap free
Amount of items: 2
Items: 
Size: 1481 Color: 161
Size: 541 Color: 118

Bin 49: 10 of cap free
Amount of items: 2
Items: 
Size: 1653 Color: 174
Size: 369 Color: 104

Bin 50: 11 of cap free
Amount of items: 2
Items: 
Size: 1630 Color: 171
Size: 391 Color: 108

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1539 Color: 164
Size: 481 Color: 116

Bin 52: 13 of cap free
Amount of items: 2
Items: 
Size: 1501 Color: 162
Size: 518 Color: 117

Bin 53: 13 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 170
Size: 390 Color: 107

Bin 54: 16 of cap free
Amount of items: 3
Items: 
Size: 1130 Color: 144
Size: 846 Color: 136
Size: 40 Color: 17

Bin 55: 16 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 146
Size: 754 Color: 129
Size: 40 Color: 14

Bin 56: 17 of cap free
Amount of items: 4
Items: 
Size: 1281 Color: 151
Size: 662 Color: 125
Size: 36 Color: 7
Size: 36 Color: 6

Bin 57: 17 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 152
Size: 680 Color: 127
Size: 34 Color: 5

Bin 58: 19 of cap free
Amount of items: 3
Items: 
Size: 1127 Color: 143
Size: 846 Color: 135
Size: 40 Color: 18

Bin 59: 19 of cap free
Amount of items: 4
Items: 
Size: 1280 Color: 150
Size: 657 Color: 124
Size: 38 Color: 11
Size: 38 Color: 10

Bin 60: 21 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 142
Size: 843 Color: 134
Size: 42 Color: 19

Bin 61: 26 of cap free
Amount of items: 7
Items: 
Size: 1017 Color: 138
Size: 229 Color: 84
Size: 228 Color: 83
Size: 219 Color: 81
Size: 209 Color: 79
Size: 52 Color: 30
Size: 52 Color: 29

Bin 62: 26 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 140
Size: 937 Color: 137
Size: 48 Color: 24

Bin 63: 29 of cap free
Amount of items: 2
Items: 
Size: 1245 Color: 148
Size: 758 Color: 131

Bin 64: 35 of cap free
Amount of items: 2
Items: 
Size: 1242 Color: 147
Size: 755 Color: 130

Bin 65: 53 of cap free
Amount of items: 20
Items: 
Size: 193 Color: 74
Size: 150 Color: 65
Size: 148 Color: 64
Size: 144 Color: 63
Size: 130 Color: 62
Size: 128 Color: 61
Size: 116 Color: 58
Size: 100 Color: 56
Size: 94 Color: 55
Size: 94 Color: 54
Size: 92 Color: 53
Size: 90 Color: 52
Size: 72 Color: 43
Size: 72 Color: 42
Size: 64 Color: 39
Size: 62 Color: 38
Size: 62 Color: 37
Size: 60 Color: 36
Size: 56 Color: 32
Size: 52 Color: 31

Bin 66: 1554 of cap free
Amount of items: 6
Items: 
Size: 88 Color: 51
Size: 82 Color: 50
Size: 80 Color: 49
Size: 78 Color: 47
Size: 76 Color: 45
Size: 74 Color: 44

Total size: 132080
Total free space: 2032

