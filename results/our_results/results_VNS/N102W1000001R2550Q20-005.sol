Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 366285 Color: 2
Size: 360757 Color: 9
Size: 272959 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 368861 Color: 16
Size: 362062 Color: 12
Size: 269078 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 397818 Color: 8
Size: 351623 Color: 2
Size: 250560 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 383471 Color: 7
Size: 314682 Color: 12
Size: 301848 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 455580 Color: 19
Size: 277876 Color: 10
Size: 266545 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 371054 Color: 15
Size: 320073 Color: 14
Size: 308874 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 474217 Color: 11
Size: 264899 Color: 7
Size: 260885 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 405262 Color: 8
Size: 305175 Color: 11
Size: 289564 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 384730 Color: 11
Size: 335809 Color: 14
Size: 279462 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 400142 Color: 15
Size: 332432 Color: 12
Size: 267427 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 392444 Color: 1
Size: 352348 Color: 5
Size: 255209 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 446522 Color: 10
Size: 287924 Color: 11
Size: 265555 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 389598 Color: 10
Size: 335563 Color: 17
Size: 274840 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 498030 Color: 14
Size: 250196 Color: 19
Size: 251775 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 459422 Color: 11
Size: 289763 Color: 16
Size: 250816 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 409910 Color: 7
Size: 311366 Color: 11
Size: 278725 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 393561 Color: 10
Size: 340574 Color: 0
Size: 265866 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 447967 Color: 3
Size: 286557 Color: 0
Size: 265477 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 346909 Color: 18
Size: 328626 Color: 2
Size: 324466 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 405465 Color: 18
Size: 299232 Color: 1
Size: 295304 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372133 Color: 15
Size: 357841 Color: 8
Size: 270027 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 449689 Color: 13
Size: 285895 Color: 3
Size: 264417 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 388417 Color: 11
Size: 316622 Color: 0
Size: 294962 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 434960 Color: 7
Size: 290053 Color: 5
Size: 274988 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 478006 Color: 1
Size: 265555 Color: 17
Size: 256440 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 378060 Color: 8
Size: 317588 Color: 17
Size: 304353 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 427695 Color: 18
Size: 313124 Color: 15
Size: 259182 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 458958 Color: 2
Size: 282872 Color: 10
Size: 258171 Color: 8

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 404050 Color: 6
Size: 298653 Color: 9
Size: 297298 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 380597 Color: 18
Size: 310992 Color: 15
Size: 308412 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 408975 Color: 14
Size: 298765 Color: 12
Size: 292261 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 416287 Color: 4
Size: 325634 Color: 17
Size: 258080 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 483954 Color: 14
Size: 258237 Color: 19
Size: 257810 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 495372 Color: 16
Size: 252965 Color: 8
Size: 251664 Color: 11

Total size: 34000034
Total free space: 0

