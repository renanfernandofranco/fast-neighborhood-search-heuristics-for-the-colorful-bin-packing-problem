Capicity Bin: 9824
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 22
Items: 
Size: 968 Color: 1
Size: 904 Color: 2
Size: 704 Color: 3
Size: 704 Color: 0
Size: 640 Color: 3
Size: 608 Color: 1
Size: 480 Color: 1
Size: 472 Color: 0
Size: 416 Color: 1
Size: 384 Color: 3
Size: 352 Color: 4
Size: 352 Color: 3
Size: 352 Color: 1
Size: 336 Color: 4
Size: 320 Color: 4
Size: 312 Color: 2
Size: 280 Color: 3
Size: 272 Color: 2
Size: 264 Color: 3
Size: 256 Color: 4
Size: 240 Color: 2
Size: 208 Color: 4

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 4920 Color: 2
Size: 3000 Color: 3
Size: 1268 Color: 4
Size: 476 Color: 4
Size: 160 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 2
Size: 4028 Color: 0
Size: 800 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5456 Color: 0
Size: 4080 Color: 2
Size: 288 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6204 Color: 0
Size: 3020 Color: 4
Size: 600 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6232 Color: 0
Size: 3024 Color: 3
Size: 568 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6296 Color: 3
Size: 3360 Color: 1
Size: 168 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6776 Color: 0
Size: 2552 Color: 1
Size: 496 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6864 Color: 2
Size: 2544 Color: 4
Size: 416 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6878 Color: 3
Size: 2458 Color: 1
Size: 488 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 2
Size: 2104 Color: 0
Size: 816 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 0
Size: 2396 Color: 3
Size: 472 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7184 Color: 0
Size: 2440 Color: 3
Size: 200 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7288 Color: 0
Size: 2356 Color: 3
Size: 180 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 4
Size: 2084 Color: 0
Size: 416 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7440 Color: 4
Size: 1808 Color: 3
Size: 576 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7594 Color: 0
Size: 2026 Color: 3
Size: 204 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7556 Color: 4
Size: 2048 Color: 1
Size: 220 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7604 Color: 0
Size: 1928 Color: 2
Size: 292 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7624 Color: 2
Size: 1652 Color: 0
Size: 548 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7664 Color: 3
Size: 2000 Color: 1
Size: 160 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 0
Size: 1180 Color: 2
Size: 800 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7856 Color: 3
Size: 1672 Color: 0
Size: 296 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 2
Size: 1578 Color: 2
Size: 312 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7988 Color: 1
Size: 1352 Color: 4
Size: 484 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8026 Color: 2
Size: 1470 Color: 1
Size: 328 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8062 Color: 1
Size: 1410 Color: 3
Size: 352 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8092 Color: 2
Size: 1316 Color: 1
Size: 416 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8088 Color: 1
Size: 1360 Color: 2
Size: 376 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8208 Color: 1
Size: 816 Color: 2
Size: 800 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8232 Color: 2
Size: 1208 Color: 4
Size: 384 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 4
Size: 1532 Color: 3
Size: 76 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 8230 Color: 1
Size: 1290 Color: 2
Size: 304 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 2
Size: 1300 Color: 3
Size: 216 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 8298 Color: 4
Size: 1046 Color: 1
Size: 480 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 8386 Color: 2
Size: 1014 Color: 1
Size: 424 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 8372 Color: 3
Size: 828 Color: 4
Size: 624 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 8432 Color: 2
Size: 816 Color: 0
Size: 576 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 3
Size: 702 Color: 2
Size: 688 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 8440 Color: 4
Size: 1336 Color: 3
Size: 48 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 8496 Color: 1
Size: 688 Color: 0
Size: 640 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 0
Size: 712 Color: 2
Size: 592 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 8532 Color: 0
Size: 1116 Color: 3
Size: 176 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 8584 Color: 4
Size: 1048 Color: 2
Size: 192 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 8596 Color: 0
Size: 808 Color: 4
Size: 420 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 8620 Color: 0
Size: 1000 Color: 2
Size: 204 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 8714 Color: 2
Size: 918 Color: 4
Size: 192 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 8680 Color: 0
Size: 856 Color: 2
Size: 288 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 8756 Color: 2
Size: 892 Color: 4
Size: 176 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 8768 Color: 2
Size: 880 Color: 3
Size: 176 Color: 4

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 8784 Color: 2
Size: 816 Color: 0
Size: 224 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 8812 Color: 2
Size: 700 Color: 4
Size: 312 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 8774 Color: 4
Size: 818 Color: 3
Size: 232 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 0
Size: 844 Color: 4
Size: 172 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 8836 Color: 4
Size: 972 Color: 3
Size: 16 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8838 Color: 0
Size: 822 Color: 2
Size: 164 Color: 1

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6074 Color: 2
Size: 3521 Color: 0
Size: 228 Color: 1

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 5546 Color: 4
Size: 3900 Color: 1
Size: 376 Color: 0

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 6078 Color: 0
Size: 3496 Color: 1
Size: 248 Color: 4

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 3
Size: 3122 Color: 4
Size: 320 Color: 0

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 0
Size: 2726 Color: 3
Size: 256 Color: 2

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 7056 Color: 2
Size: 2118 Color: 1
Size: 648 Color: 3

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 4
Size: 2216 Color: 0
Size: 320 Color: 1

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 7304 Color: 2
Size: 1846 Color: 2
Size: 672 Color: 3

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 7550 Color: 0
Size: 1648 Color: 3
Size: 624 Color: 3

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 7610 Color: 2
Size: 2124 Color: 3
Size: 88 Color: 4

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 7782 Color: 3
Size: 1848 Color: 1
Size: 192 Color: 0

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 8134 Color: 1
Size: 1688 Color: 4

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 8376 Color: 4
Size: 1446 Color: 1

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 8412 Color: 1
Size: 1042 Color: 2
Size: 368 Color: 1

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 8500 Color: 3
Size: 1322 Color: 0

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 8610 Color: 4
Size: 1212 Color: 0

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 8660 Color: 4
Size: 1162 Color: 1

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 8726 Color: 3
Size: 1096 Color: 0

Bin 75: 3 of cap free
Amount of items: 4
Items: 
Size: 4917 Color: 0
Size: 4068 Color: 3
Size: 496 Color: 0
Size: 340 Color: 2

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 0
Size: 3476 Color: 0
Size: 160 Color: 2

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 5644 Color: 3
Size: 3664 Color: 0
Size: 512 Color: 4

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 4
Size: 2426 Color: 2
Size: 416 Color: 0

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 7266 Color: 4
Size: 2090 Color: 2
Size: 464 Color: 0

Bin 80: 4 of cap free
Amount of items: 2
Items: 
Size: 8490 Color: 2
Size: 1330 Color: 0

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 1
Size: 928 Color: 0
Size: 148 Color: 4

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 3
Size: 4094 Color: 4
Size: 64 Color: 2

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 6836 Color: 1
Size: 1898 Color: 3
Size: 1084 Color: 0

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 7716 Color: 2
Size: 1496 Color: 3
Size: 606 Color: 0

Bin 85: 6 of cap free
Amount of items: 4
Items: 
Size: 8180 Color: 0
Size: 1502 Color: 1
Size: 104 Color: 2
Size: 32 Color: 3

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 8340 Color: 0
Size: 1478 Color: 1

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 8362 Color: 0
Size: 1456 Color: 3

Bin 88: 7 of cap free
Amount of items: 8
Items: 
Size: 4913 Color: 0
Size: 1136 Color: 2
Size: 1114 Color: 3
Size: 1108 Color: 0
Size: 602 Color: 4
Size: 512 Color: 4
Size: 240 Color: 2
Size: 192 Color: 1

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 6209 Color: 0
Size: 3448 Color: 2
Size: 160 Color: 3

Bin 90: 7 of cap free
Amount of items: 2
Items: 
Size: 6784 Color: 1
Size: 3033 Color: 4

Bin 91: 8 of cap free
Amount of items: 4
Items: 
Size: 4944 Color: 1
Size: 2952 Color: 2
Size: 1640 Color: 0
Size: 280 Color: 1

Bin 92: 8 of cap free
Amount of items: 4
Items: 
Size: 4948 Color: 0
Size: 4084 Color: 1
Size: 496 Color: 2
Size: 288 Color: 4

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 7924 Color: 3
Size: 1892 Color: 4

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 6171 Color: 2
Size: 3507 Color: 0
Size: 136 Color: 4

Bin 95: 10 of cap free
Amount of items: 3
Items: 
Size: 8080 Color: 1
Size: 1702 Color: 4
Size: 32 Color: 0

Bin 96: 10 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 4
Size: 1160 Color: 3
Size: 76 Color: 4

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 4
Size: 4072 Color: 3
Size: 808 Color: 1

Bin 98: 12 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 1
Size: 2584 Color: 0
Size: 1588 Color: 2

Bin 99: 12 of cap free
Amount of items: 3
Items: 
Size: 5956 Color: 3
Size: 3312 Color: 3
Size: 544 Color: 2

Bin 100: 12 of cap free
Amount of items: 2
Items: 
Size: 8720 Color: 3
Size: 1092 Color: 4

Bin 101: 14 of cap free
Amount of items: 2
Items: 
Size: 5550 Color: 0
Size: 4260 Color: 1

Bin 102: 16 of cap free
Amount of items: 5
Items: 
Size: 4918 Color: 2
Size: 2224 Color: 0
Size: 1120 Color: 3
Size: 944 Color: 2
Size: 602 Color: 3

Bin 103: 16 of cap free
Amount of items: 4
Items: 
Size: 4952 Color: 4
Size: 4088 Color: 3
Size: 544 Color: 2
Size: 224 Color: 0

Bin 104: 16 of cap free
Amount of items: 3
Items: 
Size: 6514 Color: 2
Size: 3126 Color: 1
Size: 168 Color: 1

Bin 105: 16 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 4
Size: 2374 Color: 0
Size: 878 Color: 4

Bin 106: 18 of cap free
Amount of items: 2
Items: 
Size: 7318 Color: 3
Size: 2488 Color: 4

Bin 107: 18 of cap free
Amount of items: 2
Items: 
Size: 7800 Color: 1
Size: 2006 Color: 2

Bin 108: 19 of cap free
Amount of items: 3
Items: 
Size: 5704 Color: 1
Size: 3013 Color: 4
Size: 1088 Color: 2

Bin 109: 20 of cap free
Amount of items: 2
Items: 
Size: 6576 Color: 4
Size: 3228 Color: 2

Bin 110: 20 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 1
Size: 2736 Color: 4
Size: 64 Color: 3

Bin 111: 20 of cap free
Amount of items: 2
Items: 
Size: 8040 Color: 3
Size: 1764 Color: 4

Bin 112: 20 of cap free
Amount of items: 2
Items: 
Size: 8268 Color: 4
Size: 1536 Color: 1

Bin 113: 22 of cap free
Amount of items: 2
Items: 
Size: 7668 Color: 3
Size: 2134 Color: 1

Bin 114: 24 of cap free
Amount of items: 2
Items: 
Size: 8632 Color: 0
Size: 1168 Color: 1

Bin 115: 25 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 3
Size: 3021 Color: 2
Size: 224 Color: 0

Bin 116: 26 of cap free
Amount of items: 2
Items: 
Size: 8426 Color: 3
Size: 1372 Color: 4

Bin 117: 28 of cap free
Amount of items: 4
Items: 
Size: 4916 Color: 3
Size: 2724 Color: 2
Size: 1444 Color: 4
Size: 712 Color: 1

Bin 118: 30 of cap free
Amount of items: 2
Items: 
Size: 7742 Color: 1
Size: 2052 Color: 3

Bin 119: 31 of cap free
Amount of items: 3
Items: 
Size: 6144 Color: 3
Size: 3521 Color: 4
Size: 128 Color: 2

Bin 120: 34 of cap free
Amount of items: 2
Items: 
Size: 6224 Color: 2
Size: 3566 Color: 4

Bin 121: 34 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 4
Size: 2876 Color: 0

Bin 122: 36 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 1
Size: 2480 Color: 0
Size: 2394 Color: 0

Bin 123: 40 of cap free
Amount of items: 2
Items: 
Size: 7512 Color: 0
Size: 2272 Color: 4

Bin 124: 44 of cap free
Amount of items: 3
Items: 
Size: 6954 Color: 2
Size: 2762 Color: 1
Size: 64 Color: 3

Bin 125: 48 of cap free
Amount of items: 2
Items: 
Size: 5872 Color: 1
Size: 3904 Color: 3

Bin 126: 48 of cap free
Amount of items: 2
Items: 
Size: 7284 Color: 2
Size: 2492 Color: 3

Bin 127: 51 of cap free
Amount of items: 2
Items: 
Size: 6728 Color: 3
Size: 3045 Color: 2

Bin 128: 52 of cap free
Amount of items: 2
Items: 
Size: 7864 Color: 2
Size: 1908 Color: 1

Bin 129: 61 of cap free
Amount of items: 2
Items: 
Size: 6201 Color: 3
Size: 3562 Color: 4

Bin 130: 116 of cap free
Amount of items: 2
Items: 
Size: 5617 Color: 2
Size: 4091 Color: 1

Bin 131: 132 of cap free
Amount of items: 2
Items: 
Size: 5599 Color: 0
Size: 4093 Color: 4

Bin 132: 133 of cap free
Amount of items: 2
Items: 
Size: 5601 Color: 4
Size: 4090 Color: 2

Bin 133: 8376 of cap free
Amount of items: 6
Items: 
Size: 344 Color: 0
Size: 256 Color: 3
Size: 256 Color: 2
Size: 208 Color: 1
Size: 192 Color: 4
Size: 192 Color: 4

Total size: 1296768
Total free space: 9824

