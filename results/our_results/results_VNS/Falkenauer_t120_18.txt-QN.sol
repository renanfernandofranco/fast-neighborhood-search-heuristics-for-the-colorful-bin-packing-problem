Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 102
Size: 306 Color: 60
Size: 250 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 104
Size: 278 Color: 39
Size: 267 Color: 30

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 103
Size: 261 Color: 24
Size: 286 Color: 47

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 119
Size: 251 Color: 7
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 112
Size: 272 Color: 35
Size: 252 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 110
Size: 276 Color: 38
Size: 255 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 85
Size: 323 Color: 69
Size: 292 Color: 51

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 100
Size: 304 Color: 58
Size: 268 Color: 33

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 117
Size: 255 Color: 14
Size: 250 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 114
Size: 262 Color: 26
Size: 250 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 106
Size: 286 Color: 46
Size: 256 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 105
Size: 275 Color: 37
Size: 268 Color: 32

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 109
Size: 280 Color: 40
Size: 254 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 87
Size: 317 Color: 66
Size: 290 Color: 49

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 95
Size: 309 Color: 62
Size: 274 Color: 36

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 116
Size: 256 Color: 15
Size: 251 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 89
Size: 304 Color: 59
Size: 298 Color: 55

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 78
Size: 340 Color: 77
Size: 314 Color: 65

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 98
Size: 312 Color: 63
Size: 267 Color: 31

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 115
Size: 258 Color: 18
Size: 254 Color: 10

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 86
Size: 327 Color: 71
Size: 282 Color: 41

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 84
Size: 369 Color: 82
Size: 258 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 111
Size: 268 Color: 34
Size: 259 Color: 21

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 93
Size: 327 Color: 72
Size: 265 Color: 29

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 97
Size: 300 Color: 57
Size: 282 Color: 43

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 96
Size: 318 Color: 68
Size: 264 Color: 28

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 90
Size: 313 Color: 64
Size: 287 Color: 48

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 99
Size: 317 Color: 67
Size: 259 Color: 20

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 91
Size: 334 Color: 74
Size: 259 Color: 23

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 94
Size: 308 Color: 61
Size: 282 Color: 42

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 101
Size: 300 Color: 56
Size: 262 Color: 25

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 83
Size: 338 Color: 75
Size: 292 Color: 50

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 81
Size: 339 Color: 76
Size: 295 Color: 53

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 118
Size: 255 Color: 12
Size: 250 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 79
Size: 329 Color: 73
Size: 323 Color: 70

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 88
Size: 355 Color: 80
Size: 250 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 113
Size: 264 Color: 27
Size: 259 Color: 22

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 92
Size: 297 Color: 54
Size: 295 Color: 52

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 108
Size: 286 Color: 45
Size: 253 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 107
Size: 284 Color: 44
Size: 256 Color: 16

Total size: 40000
Total free space: 0

