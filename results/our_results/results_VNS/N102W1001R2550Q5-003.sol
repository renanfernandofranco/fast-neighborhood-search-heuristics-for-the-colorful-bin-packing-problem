Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 263 Color: 0
Size: 253 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 352 Color: 2
Size: 268 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 267 Color: 4
Size: 258 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 323 Color: 3
Size: 259 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 2
Size: 303 Color: 4
Size: 273 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 280 Color: 4
Size: 278 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 346 Color: 4
Size: 253 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 2
Size: 252 Color: 0
Size: 251 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 370 Color: 1
Size: 258 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 4
Size: 316 Color: 2
Size: 254 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 317 Color: 3
Size: 294 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 366 Color: 0
Size: 261 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 337 Color: 1
Size: 290 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 3
Size: 316 Color: 2
Size: 250 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 335 Color: 0
Size: 313 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 282 Color: 0
Size: 256 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 320 Color: 1
Size: 262 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 327 Color: 3
Size: 301 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 297 Color: 4
Size: 267 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 2
Size: 290 Color: 0
Size: 264 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 341 Color: 2
Size: 285 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 314 Color: 4
Size: 283 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 2
Size: 272 Color: 1
Size: 261 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 2
Size: 261 Color: 2
Size: 254 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 306 Color: 0
Size: 287 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 327 Color: 1
Size: 290 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 281 Color: 1
Size: 263 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 329 Color: 0
Size: 263 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 307 Color: 2
Size: 441 Color: 4
Size: 253 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 2
Size: 303 Color: 4
Size: 296 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 297 Color: 1
Size: 265 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 278 Color: 0
Size: 275 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 256 Color: 3
Size: 253 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 262 Color: 2
Size: 261 Color: 4

Total size: 34034
Total free space: 0

