Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 454153 Color: 4
Size: 287106 Color: 3
Size: 258742 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 466110 Color: 3
Size: 278580 Color: 1
Size: 255311 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 428379 Color: 1
Size: 305962 Color: 1
Size: 265660 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 402226 Color: 3
Size: 304735 Color: 4
Size: 293040 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 405982 Color: 4
Size: 299991 Color: 1
Size: 294028 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 449101 Color: 3
Size: 292633 Color: 3
Size: 258267 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 420221 Color: 2
Size: 314806 Color: 1
Size: 264974 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 478549 Color: 4
Size: 270625 Color: 4
Size: 250827 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 409271 Color: 1
Size: 331063 Color: 3
Size: 259667 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 448698 Color: 3
Size: 285662 Color: 4
Size: 265641 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 453617 Color: 1
Size: 277264 Color: 0
Size: 269120 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 375901 Color: 4
Size: 316038 Color: 2
Size: 308062 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 378995 Color: 4
Size: 354526 Color: 3
Size: 266480 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 427757 Color: 1
Size: 302791 Color: 4
Size: 269453 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 427810 Color: 1
Size: 300271 Color: 2
Size: 271920 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 356393 Color: 3
Size: 326562 Color: 3
Size: 317046 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 352496 Color: 2
Size: 347586 Color: 4
Size: 299919 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 389828 Color: 1
Size: 331763 Color: 2
Size: 278410 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 492340 Color: 2
Size: 254625 Color: 1
Size: 253036 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 423991 Color: 2
Size: 300411 Color: 0
Size: 275599 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 364238 Color: 4
Size: 347565 Color: 3
Size: 288198 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 398223 Color: 2
Size: 346911 Color: 0
Size: 254867 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 393488 Color: 0
Size: 305328 Color: 2
Size: 301185 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 480064 Color: 1
Size: 260768 Color: 3
Size: 259169 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 371951 Color: 4
Size: 338104 Color: 3
Size: 289946 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 476073 Color: 1
Size: 272944 Color: 1
Size: 250984 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 469656 Color: 3
Size: 275522 Color: 2
Size: 254823 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 421499 Color: 2
Size: 303789 Color: 3
Size: 274713 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 403102 Color: 3
Size: 318624 Color: 1
Size: 278275 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 429680 Color: 4
Size: 300677 Color: 3
Size: 269644 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 365017 Color: 4
Size: 351577 Color: 2
Size: 283407 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 377535 Color: 0
Size: 332891 Color: 4
Size: 289575 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 425137 Color: 1
Size: 320844 Color: 2
Size: 254020 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 438470 Color: 3
Size: 281267 Color: 1
Size: 280264 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 433715 Color: 2
Size: 300983 Color: 2
Size: 265303 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 467308 Color: 2
Size: 272843 Color: 4
Size: 259850 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 371733 Color: 0
Size: 319279 Color: 0
Size: 308989 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 394546 Color: 4
Size: 343475 Color: 1
Size: 261980 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 415440 Color: 0
Size: 316364 Color: 4
Size: 268197 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 376633 Color: 4
Size: 363796 Color: 3
Size: 259572 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 381664 Color: 3
Size: 340621 Color: 0
Size: 277716 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 429404 Color: 1
Size: 291782 Color: 4
Size: 278815 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 412653 Color: 1
Size: 332841 Color: 4
Size: 254507 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 412097 Color: 3
Size: 309184 Color: 0
Size: 278720 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 404631 Color: 0
Size: 327665 Color: 2
Size: 267705 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 485310 Color: 4
Size: 263608 Color: 0
Size: 251083 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 420486 Color: 4
Size: 314071 Color: 3
Size: 265444 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 355108 Color: 0
Size: 330300 Color: 0
Size: 314593 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 383711 Color: 0
Size: 308851 Color: 2
Size: 307439 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 362399 Color: 3
Size: 323156 Color: 3
Size: 314446 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 402713 Color: 0
Size: 302747 Color: 3
Size: 294541 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 465825 Color: 2
Size: 267259 Color: 1
Size: 266917 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 391632 Color: 4
Size: 342354 Color: 3
Size: 266015 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 499190 Color: 3
Size: 250451 Color: 4
Size: 250360 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 434084 Color: 1
Size: 307746 Color: 0
Size: 258171 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 414915 Color: 3
Size: 315453 Color: 2
Size: 269633 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 386037 Color: 4
Size: 352070 Color: 0
Size: 261894 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 395004 Color: 1
Size: 305397 Color: 2
Size: 299600 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 442203 Color: 4
Size: 304820 Color: 3
Size: 252978 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 454322 Color: 3
Size: 286419 Color: 0
Size: 259260 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 404100 Color: 1
Size: 338029 Color: 3
Size: 257872 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 370712 Color: 0
Size: 323308 Color: 1
Size: 305981 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 481071 Color: 4
Size: 262748 Color: 1
Size: 256182 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 389346 Color: 2
Size: 309660 Color: 1
Size: 300995 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 464976 Color: 4
Size: 284529 Color: 1
Size: 250496 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 498568 Color: 3
Size: 251075 Color: 4
Size: 250358 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 433356 Color: 2
Size: 315559 Color: 3
Size: 251086 Color: 3

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 410351 Color: 0
Size: 329867 Color: 2
Size: 259783 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 388121 Color: 2
Size: 308959 Color: 2
Size: 302921 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 481596 Color: 3
Size: 259643 Color: 2
Size: 258762 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 444474 Color: 2
Size: 280059 Color: 2
Size: 275468 Color: 3

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 410469 Color: 2
Size: 331613 Color: 0
Size: 257919 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 395104 Color: 0
Size: 348887 Color: 2
Size: 256010 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 388417 Color: 0
Size: 325807 Color: 2
Size: 285777 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 407964 Color: 0
Size: 318769 Color: 2
Size: 273268 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 486857 Color: 3
Size: 259315 Color: 1
Size: 253829 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 410009 Color: 4
Size: 332616 Color: 3
Size: 257376 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 414757 Color: 3
Size: 307005 Color: 4
Size: 278239 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 459190 Color: 3
Size: 289291 Color: 1
Size: 251520 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 425704 Color: 1
Size: 317701 Color: 0
Size: 256596 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 468140 Color: 1
Size: 280423 Color: 4
Size: 251438 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 400540 Color: 0
Size: 332339 Color: 2
Size: 267122 Color: 2

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 458067 Color: 3
Size: 276739 Color: 0
Size: 265195 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 448575 Color: 0
Size: 289242 Color: 4
Size: 262184 Color: 2

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 426696 Color: 0
Size: 320821 Color: 1
Size: 252484 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 384909 Color: 3
Size: 346959 Color: 0
Size: 268133 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 376156 Color: 1
Size: 345601 Color: 3
Size: 278244 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 427060 Color: 4
Size: 288943 Color: 2
Size: 283998 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 369546 Color: 4
Size: 352534 Color: 3
Size: 277921 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 383739 Color: 2
Size: 326097 Color: 4
Size: 290165 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 493182 Color: 1
Size: 255986 Color: 3
Size: 250833 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 434017 Color: 2
Size: 315862 Color: 4
Size: 250122 Color: 4

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 444694 Color: 3
Size: 288156 Color: 1
Size: 267151 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 351386 Color: 1
Size: 325904 Color: 0
Size: 322711 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 420413 Color: 4
Size: 327009 Color: 0
Size: 252579 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 382296 Color: 3
Size: 360569 Color: 0
Size: 257136 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 386465 Color: 4
Size: 323370 Color: 0
Size: 290166 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 404572 Color: 2
Size: 343797 Color: 3
Size: 251632 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 376660 Color: 0
Size: 332918 Color: 3
Size: 290423 Color: 3

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 376716 Color: 0
Size: 328797 Color: 2
Size: 294488 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 369651 Color: 0
Size: 369157 Color: 2
Size: 261193 Color: 2

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 363550 Color: 4
Size: 326778 Color: 0
Size: 309673 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 424924 Color: 3
Size: 306480 Color: 3
Size: 268597 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 497622 Color: 4
Size: 252197 Color: 2
Size: 250182 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 366714 Color: 0
Size: 347100 Color: 3
Size: 286187 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 406976 Color: 2
Size: 331725 Color: 0
Size: 261300 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 386794 Color: 1
Size: 359156 Color: 2
Size: 254051 Color: 4

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 371753 Color: 1
Size: 361475 Color: 3
Size: 266773 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 443696 Color: 0
Size: 281505 Color: 2
Size: 274800 Color: 2

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 368465 Color: 4
Size: 327151 Color: 1
Size: 304385 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 436964 Color: 1
Size: 288375 Color: 2
Size: 274662 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 455853 Color: 2
Size: 272847 Color: 3
Size: 271301 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 375122 Color: 4
Size: 327657 Color: 0
Size: 297222 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 358234 Color: 2
Size: 355444 Color: 1
Size: 286323 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 461994 Color: 0
Size: 272747 Color: 3
Size: 265260 Color: 1

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 402416 Color: 1
Size: 314842 Color: 3
Size: 282743 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 418596 Color: 0
Size: 326302 Color: 4
Size: 255103 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 389763 Color: 4
Size: 358573 Color: 1
Size: 251665 Color: 2

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 376737 Color: 2
Size: 319165 Color: 1
Size: 304099 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 384686 Color: 1
Size: 363312 Color: 2
Size: 252003 Color: 4

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 438434 Color: 4
Size: 289361 Color: 3
Size: 272206 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 379303 Color: 1
Size: 337625 Color: 4
Size: 283073 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 402888 Color: 4
Size: 308712 Color: 1
Size: 288401 Color: 2

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 379475 Color: 1
Size: 365390 Color: 3
Size: 255136 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 404885 Color: 2
Size: 307200 Color: 4
Size: 287916 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 426104 Color: 1
Size: 302270 Color: 3
Size: 271627 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 491186 Color: 1
Size: 255277 Color: 3
Size: 253538 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 405998 Color: 2
Size: 299888 Color: 3
Size: 294115 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 371350 Color: 2
Size: 329230 Color: 1
Size: 299421 Color: 4

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 371649 Color: 2
Size: 348785 Color: 3
Size: 279567 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 458647 Color: 3
Size: 289750 Color: 2
Size: 251604 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 397774 Color: 4
Size: 346315 Color: 4
Size: 255912 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 369044 Color: 1
Size: 317176 Color: 3
Size: 313781 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 468819 Color: 1
Size: 276374 Color: 0
Size: 254808 Color: 2

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 492352 Color: 3
Size: 255992 Color: 1
Size: 251657 Color: 4

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 398402 Color: 0
Size: 310360 Color: 3
Size: 291239 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 398812 Color: 0
Size: 346007 Color: 3
Size: 255182 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 363264 Color: 4
Size: 318642 Color: 3
Size: 318095 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 376383 Color: 2
Size: 348556 Color: 0
Size: 275062 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 377636 Color: 3
Size: 356639 Color: 4
Size: 265726 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 448479 Color: 3
Size: 297160 Color: 3
Size: 254362 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 472592 Color: 3
Size: 268492 Color: 4
Size: 258917 Color: 4

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414534 Color: 1
Size: 334023 Color: 4
Size: 251444 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 406777 Color: 0
Size: 330960 Color: 4
Size: 262264 Color: 3

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 379288 Color: 4
Size: 365401 Color: 2
Size: 255312 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 408235 Color: 0
Size: 300722 Color: 4
Size: 291044 Color: 4

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 402085 Color: 2
Size: 346472 Color: 3
Size: 251444 Color: 4

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 416815 Color: 4
Size: 314214 Color: 3
Size: 268972 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 429496 Color: 4
Size: 300138 Color: 1
Size: 270367 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 439635 Color: 2
Size: 295448 Color: 3
Size: 264918 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 446802 Color: 4
Size: 282083 Color: 0
Size: 271116 Color: 2

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 492615 Color: 0
Size: 254627 Color: 4
Size: 252759 Color: 3

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 457200 Color: 3
Size: 289154 Color: 0
Size: 253647 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 405803 Color: 4
Size: 332821 Color: 4
Size: 261377 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 393420 Color: 2
Size: 354436 Color: 1
Size: 252145 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 395404 Color: 1
Size: 312163 Color: 4
Size: 292434 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 390589 Color: 2
Size: 358917 Color: 3
Size: 250495 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 460331 Color: 0
Size: 281116 Color: 2
Size: 258554 Color: 4

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 355634 Color: 1
Size: 338429 Color: 2
Size: 305938 Color: 2

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 373048 Color: 3
Size: 324163 Color: 4
Size: 302790 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 481813 Color: 4
Size: 266176 Color: 3
Size: 252012 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 460946 Color: 4
Size: 275003 Color: 0
Size: 264052 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 377263 Color: 0
Size: 326397 Color: 2
Size: 296341 Color: 2

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 403540 Color: 0
Size: 332395 Color: 1
Size: 264066 Color: 3

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 401603 Color: 3
Size: 332239 Color: 4
Size: 266159 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 403976 Color: 3
Size: 310627 Color: 4
Size: 285398 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 365811 Color: 1
Size: 329081 Color: 2
Size: 305109 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 478065 Color: 2
Size: 269333 Color: 0
Size: 252603 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 380778 Color: 3
Size: 332026 Color: 0
Size: 287197 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 417992 Color: 0
Size: 297437 Color: 4
Size: 284572 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 420081 Color: 0
Size: 320385 Color: 2
Size: 259535 Color: 3

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 409341 Color: 1
Size: 309619 Color: 3
Size: 281041 Color: 2

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 377155 Color: 3
Size: 343825 Color: 4
Size: 279021 Color: 2

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 392243 Color: 0
Size: 312261 Color: 3
Size: 295497 Color: 2

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 434923 Color: 3
Size: 314969 Color: 2
Size: 250109 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 445917 Color: 3
Size: 283084 Color: 4
Size: 271000 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 403796 Color: 4
Size: 305042 Color: 0
Size: 291163 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 389976 Color: 4
Size: 339341 Color: 2
Size: 270684 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 483203 Color: 0
Size: 259715 Color: 1
Size: 257083 Color: 3

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 480069 Color: 2
Size: 260953 Color: 4
Size: 258979 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 430722 Color: 0
Size: 306578 Color: 1
Size: 262701 Color: 1

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 487455 Color: 1
Size: 258219 Color: 3
Size: 254327 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 442292 Color: 0
Size: 305150 Color: 1
Size: 252559 Color: 2

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 372528 Color: 2
Size: 370826 Color: 3
Size: 256647 Color: 3

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 425276 Color: 3
Size: 321917 Color: 4
Size: 252808 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 389230 Color: 1
Size: 308966 Color: 4
Size: 301805 Color: 2

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 423667 Color: 0
Size: 316938 Color: 4
Size: 259396 Color: 2

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 415492 Color: 4
Size: 295927 Color: 2
Size: 288582 Color: 4

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 464310 Color: 0
Size: 283791 Color: 4
Size: 251900 Color: 3

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 398997 Color: 4
Size: 302310 Color: 1
Size: 298694 Color: 1

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 469493 Color: 4
Size: 273897 Color: 3
Size: 256611 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 387993 Color: 4
Size: 311011 Color: 3
Size: 300997 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 451313 Color: 3
Size: 291919 Color: 0
Size: 256769 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 483474 Color: 2
Size: 260365 Color: 3
Size: 256162 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 442424 Color: 3
Size: 288355 Color: 4
Size: 269222 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 451575 Color: 1
Size: 278965 Color: 2
Size: 269461 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 462921 Color: 1
Size: 285357 Color: 4
Size: 251723 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 417987 Color: 1
Size: 300239 Color: 3
Size: 281775 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 455470 Color: 3
Size: 288374 Color: 2
Size: 256157 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 448789 Color: 2
Size: 291821 Color: 0
Size: 259391 Color: 3

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 414099 Color: 3
Size: 324407 Color: 0
Size: 261495 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 446297 Color: 1
Size: 284715 Color: 4
Size: 268989 Color: 3

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 404394 Color: 0
Size: 306621 Color: 3
Size: 288986 Color: 3

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 377231 Color: 1
Size: 319845 Color: 3
Size: 302925 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 386136 Color: 4
Size: 358312 Color: 1
Size: 255553 Color: 2

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 389845 Color: 3
Size: 340091 Color: 0
Size: 270065 Color: 2

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 390940 Color: 1
Size: 315075 Color: 3
Size: 293986 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 367030 Color: 1
Size: 330144 Color: 0
Size: 302827 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 424578 Color: 3
Size: 309277 Color: 4
Size: 266146 Color: 3

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 436347 Color: 2
Size: 288751 Color: 4
Size: 274903 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 444242 Color: 3
Size: 297122 Color: 4
Size: 258637 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 363298 Color: 4
Size: 326417 Color: 2
Size: 310286 Color: 4

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 414713 Color: 1
Size: 306247 Color: 0
Size: 279041 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 422040 Color: 2
Size: 290926 Color: 3
Size: 287035 Color: 4

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 398019 Color: 1
Size: 345306 Color: 2
Size: 256676 Color: 4

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 422945 Color: 1
Size: 303174 Color: 0
Size: 273882 Color: 4

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 486860 Color: 3
Size: 256739 Color: 2
Size: 256402 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 421158 Color: 1
Size: 309559 Color: 2
Size: 269284 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 388485 Color: 3
Size: 319037 Color: 0
Size: 292479 Color: 2

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 472561 Color: 0
Size: 275234 Color: 4
Size: 252206 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 383951 Color: 2
Size: 347479 Color: 1
Size: 268571 Color: 2

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 430385 Color: 0
Size: 315732 Color: 1
Size: 253884 Color: 3

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 412441 Color: 4
Size: 301182 Color: 1
Size: 286378 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 390482 Color: 4
Size: 347959 Color: 2
Size: 261560 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 380017 Color: 3
Size: 343309 Color: 0
Size: 276675 Color: 4

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 394980 Color: 3
Size: 335896 Color: 3
Size: 269125 Color: 2

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 428456 Color: 3
Size: 295863 Color: 1
Size: 275682 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 489852 Color: 1
Size: 259201 Color: 3
Size: 250948 Color: 4

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 394208 Color: 0
Size: 308015 Color: 1
Size: 297778 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 432222 Color: 1
Size: 284798 Color: 2
Size: 282981 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 377862 Color: 4
Size: 314726 Color: 2
Size: 307413 Color: 4

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 464165 Color: 4
Size: 273330 Color: 1
Size: 262506 Color: 3

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 389157 Color: 1
Size: 344781 Color: 3
Size: 266063 Color: 2

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 366828 Color: 4
Size: 361920 Color: 0
Size: 271253 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 373511 Color: 4
Size: 364774 Color: 0
Size: 261716 Color: 4

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 496536 Color: 3
Size: 253074 Color: 2
Size: 250391 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 426681 Color: 2
Size: 297974 Color: 0
Size: 275346 Color: 2

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 468035 Color: 1
Size: 280653 Color: 4
Size: 251313 Color: 2

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 472662 Color: 4
Size: 270763 Color: 0
Size: 256576 Color: 3

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 413734 Color: 3
Size: 334592 Color: 0
Size: 251675 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 351886 Color: 3
Size: 331332 Color: 0
Size: 316783 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 418555 Color: 2
Size: 302389 Color: 0
Size: 279057 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 488003 Color: 3
Size: 257760 Color: 1
Size: 254238 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 435522 Color: 3
Size: 302280 Color: 0
Size: 262199 Color: 4

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 367713 Color: 3
Size: 327483 Color: 4
Size: 304805 Color: 4

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 376166 Color: 0
Size: 327018 Color: 1
Size: 296817 Color: 4

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 455559 Color: 0
Size: 277970 Color: 3
Size: 266472 Color: 4

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 376998 Color: 2
Size: 350727 Color: 4
Size: 272276 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 416838 Color: 4
Size: 303995 Color: 3
Size: 279168 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 460541 Color: 3
Size: 272093 Color: 4
Size: 267367 Color: 1

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 386970 Color: 1
Size: 331848 Color: 3
Size: 281183 Color: 3

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 443601 Color: 0
Size: 288116 Color: 4
Size: 268284 Color: 3

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 383237 Color: 0
Size: 346233 Color: 3
Size: 270531 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 391751 Color: 0
Size: 304966 Color: 3
Size: 303284 Color: 4

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 421144 Color: 2
Size: 321403 Color: 1
Size: 257454 Color: 2

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 482038 Color: 1
Size: 259828 Color: 4
Size: 258135 Color: 4

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 480927 Color: 2
Size: 260575 Color: 3
Size: 258499 Color: 2

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 373839 Color: 0
Size: 369399 Color: 2
Size: 256763 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 498029 Color: 3
Size: 251839 Color: 2
Size: 250133 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 402156 Color: 1
Size: 318707 Color: 3
Size: 279138 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 419408 Color: 3
Size: 292836 Color: 4
Size: 287757 Color: 2

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 450495 Color: 3
Size: 288439 Color: 1
Size: 261067 Color: 3

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 362663 Color: 1
Size: 337753 Color: 2
Size: 299585 Color: 4

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 432246 Color: 2
Size: 295220 Color: 0
Size: 272535 Color: 4

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 412319 Color: 0
Size: 322445 Color: 3
Size: 265237 Color: 2

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 462336 Color: 0
Size: 287559 Color: 4
Size: 250106 Color: 4

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 372482 Color: 1
Size: 370014 Color: 3
Size: 257505 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 440619 Color: 3
Size: 301492 Color: 1
Size: 257890 Color: 4

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 375698 Color: 4
Size: 340435 Color: 0
Size: 283868 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 346617 Color: 2
Size: 327063 Color: 0
Size: 326321 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 408292 Color: 3
Size: 298500 Color: 3
Size: 293209 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 401670 Color: 1
Size: 318694 Color: 4
Size: 279637 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 408912 Color: 4
Size: 305599 Color: 0
Size: 285490 Color: 2

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 376327 Color: 1
Size: 367505 Color: 3
Size: 256169 Color: 3

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 482847 Color: 0
Size: 260968 Color: 2
Size: 256186 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 335770 Color: 2
Size: 333937 Color: 3
Size: 330294 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 496246 Color: 2
Size: 252505 Color: 4
Size: 251250 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 484777 Color: 0
Size: 258991 Color: 3
Size: 256233 Color: 4

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 460324 Color: 4
Size: 270284 Color: 4
Size: 269393 Color: 3

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 361174 Color: 0
Size: 338125 Color: 3
Size: 300702 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 428017 Color: 1
Size: 308901 Color: 4
Size: 263083 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 428960 Color: 1
Size: 290677 Color: 1
Size: 280364 Color: 4

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 412033 Color: 3
Size: 315721 Color: 1
Size: 272247 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 370698 Color: 4
Size: 330442 Color: 2
Size: 298861 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 429303 Color: 0
Size: 285529 Color: 1
Size: 285169 Color: 2

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 406564 Color: 0
Size: 328367 Color: 4
Size: 265070 Color: 3

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 353943 Color: 0
Size: 346142 Color: 1
Size: 299916 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 457612 Color: 4
Size: 274554 Color: 3
Size: 267835 Color: 4

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 394842 Color: 4
Size: 306594 Color: 2
Size: 298565 Color: 3

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 371415 Color: 3
Size: 325664 Color: 3
Size: 302922 Color: 4

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 453038 Color: 2
Size: 287835 Color: 2
Size: 259128 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 452944 Color: 1
Size: 287217 Color: 2
Size: 259840 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 475869 Color: 1
Size: 268003 Color: 0
Size: 256129 Color: 2

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 387384 Color: 3
Size: 355158 Color: 2
Size: 257459 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 436825 Color: 1
Size: 286059 Color: 0
Size: 277117 Color: 3

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 429367 Color: 4
Size: 313533 Color: 1
Size: 257101 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 447726 Color: 1
Size: 278400 Color: 0
Size: 273875 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 448682 Color: 0
Size: 281439 Color: 2
Size: 269880 Color: 4

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 477044 Color: 3
Size: 266403 Color: 1
Size: 256554 Color: 1

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 477332 Color: 4
Size: 270680 Color: 0
Size: 251989 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 429804 Color: 2
Size: 308158 Color: 1
Size: 262039 Color: 4

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 420493 Color: 4
Size: 300001 Color: 0
Size: 279507 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 391945 Color: 1
Size: 327117 Color: 0
Size: 280939 Color: 2

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 356930 Color: 4
Size: 341787 Color: 1
Size: 301284 Color: 3

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 366112 Color: 4
Size: 324323 Color: 2
Size: 309566 Color: 4

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 379712 Color: 3
Size: 332907 Color: 0
Size: 287382 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 461905 Color: 4
Size: 283015 Color: 1
Size: 255081 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 400452 Color: 0
Size: 304823 Color: 3
Size: 294726 Color: 4

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 447950 Color: 1
Size: 297188 Color: 0
Size: 254863 Color: 4

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 444935 Color: 1
Size: 301015 Color: 2
Size: 254051 Color: 4

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 483554 Color: 4
Size: 265733 Color: 0
Size: 250714 Color: 4

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 416517 Color: 3
Size: 292835 Color: 1
Size: 290649 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 467135 Color: 2
Size: 275223 Color: 0
Size: 257643 Color: 4

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 455739 Color: 4
Size: 284891 Color: 2
Size: 259371 Color: 2

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 402323 Color: 4
Size: 301180 Color: 2
Size: 296498 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 456120 Color: 0
Size: 293397 Color: 1
Size: 250484 Color: 2

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 449268 Color: 2
Size: 291651 Color: 1
Size: 259082 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 439540 Color: 0
Size: 290096 Color: 1
Size: 270365 Color: 2

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 388761 Color: 4
Size: 313526 Color: 1
Size: 297714 Color: 3

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 471959 Color: 0
Size: 277284 Color: 0
Size: 250758 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 350348 Color: 1
Size: 328668 Color: 3
Size: 320985 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 481296 Color: 0
Size: 260525 Color: 3
Size: 258180 Color: 4

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 385672 Color: 2
Size: 309225 Color: 3
Size: 305104 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 433847 Color: 2
Size: 285565 Color: 0
Size: 280589 Color: 4

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 419280 Color: 0
Size: 312988 Color: 3
Size: 267733 Color: 3

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 376073 Color: 4
Size: 335370 Color: 0
Size: 288558 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 431466 Color: 3
Size: 288580 Color: 4
Size: 279955 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 370747 Color: 0
Size: 343845 Color: 3
Size: 285409 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 396547 Color: 3
Size: 336116 Color: 0
Size: 267338 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 390237 Color: 0
Size: 344451 Color: 3
Size: 265313 Color: 2

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 422545 Color: 4
Size: 310616 Color: 1
Size: 266840 Color: 2

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 382067 Color: 3
Size: 330458 Color: 4
Size: 287476 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 454633 Color: 0
Size: 278467 Color: 4
Size: 266901 Color: 2

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 384438 Color: 1
Size: 315517 Color: 4
Size: 300046 Color: 2

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 423320 Color: 3
Size: 301102 Color: 0
Size: 275579 Color: 4

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 459796 Color: 2
Size: 270963 Color: 1
Size: 269242 Color: 3

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 451151 Color: 3
Size: 296342 Color: 1
Size: 252508 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 459416 Color: 2
Size: 280208 Color: 1
Size: 260377 Color: 3

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 489924 Color: 3
Size: 257272 Color: 4
Size: 252805 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 457024 Color: 3
Size: 276256 Color: 1
Size: 266721 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 405262 Color: 0
Size: 323203 Color: 1
Size: 271536 Color: 3

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 424776 Color: 2
Size: 308167 Color: 4
Size: 267058 Color: 4

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 424041 Color: 4
Size: 311331 Color: 3
Size: 264629 Color: 1

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 400231 Color: 2
Size: 307959 Color: 1
Size: 291811 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 380151 Color: 1
Size: 359685 Color: 3
Size: 260165 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 343135 Color: 1
Size: 328946 Color: 3
Size: 327920 Color: 1

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 411621 Color: 0
Size: 314039 Color: 1
Size: 274341 Color: 1

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 383483 Color: 3
Size: 332008 Color: 2
Size: 284510 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 381783 Color: 3
Size: 366600 Color: 1
Size: 251618 Color: 1

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 376885 Color: 1
Size: 312330 Color: 2
Size: 310786 Color: 3

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 447155 Color: 2
Size: 286416 Color: 3
Size: 266430 Color: 4

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 367849 Color: 0
Size: 318950 Color: 4
Size: 313202 Color: 2

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 416055 Color: 2
Size: 299791 Color: 0
Size: 284155 Color: 3

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 473234 Color: 3
Size: 272150 Color: 2
Size: 254617 Color: 2

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 400645 Color: 3
Size: 340059 Color: 0
Size: 259297 Color: 2

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 394179 Color: 3
Size: 339325 Color: 1
Size: 266497 Color: 4

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 353916 Color: 4
Size: 327635 Color: 2
Size: 318450 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 417430 Color: 1
Size: 330670 Color: 2
Size: 251901 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 425391 Color: 2
Size: 308852 Color: 4
Size: 265758 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 381982 Color: 3
Size: 359113 Color: 4
Size: 258906 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 378738 Color: 0
Size: 363911 Color: 1
Size: 257352 Color: 2

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 466509 Color: 3
Size: 281428 Color: 0
Size: 252064 Color: 2

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 493177 Color: 2
Size: 253748 Color: 4
Size: 253076 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 376374 Color: 1
Size: 340239 Color: 2
Size: 283388 Color: 2

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 434405 Color: 0
Size: 290782 Color: 4
Size: 274814 Color: 3

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 361963 Color: 1
Size: 360712 Color: 2
Size: 277326 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 448209 Color: 3
Size: 276453 Color: 2
Size: 275339 Color: 3

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 457484 Color: 2
Size: 275324 Color: 3
Size: 267193 Color: 4

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 367176 Color: 4
Size: 365828 Color: 0
Size: 266997 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 470286 Color: 4
Size: 278215 Color: 0
Size: 251500 Color: 4

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 442292 Color: 3
Size: 301264 Color: 2
Size: 256445 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 401354 Color: 4
Size: 311017 Color: 4
Size: 287630 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 433904 Color: 2
Size: 291171 Color: 2
Size: 274926 Color: 1

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 397597 Color: 2
Size: 338248 Color: 1
Size: 264156 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 428527 Color: 4
Size: 308944 Color: 3
Size: 262530 Color: 3

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 445711 Color: 1
Size: 283178 Color: 2
Size: 271112 Color: 3

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 444364 Color: 2
Size: 281759 Color: 2
Size: 273878 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 349424 Color: 2
Size: 344890 Color: 0
Size: 305687 Color: 1

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 393088 Color: 2
Size: 316208 Color: 0
Size: 290705 Color: 4

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 395128 Color: 3
Size: 305862 Color: 3
Size: 299011 Color: 4

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 443981 Color: 1
Size: 289599 Color: 3
Size: 266421 Color: 2

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 405804 Color: 0
Size: 300869 Color: 4
Size: 293328 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 383410 Color: 4
Size: 333300 Color: 3
Size: 283291 Color: 4

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 423647 Color: 1
Size: 323728 Color: 3
Size: 252626 Color: 3

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 370423 Color: 3
Size: 323840 Color: 3
Size: 305738 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 382478 Color: 2
Size: 349256 Color: 0
Size: 268267 Color: 1

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 486536 Color: 4
Size: 257637 Color: 0
Size: 255828 Color: 1

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 485763 Color: 2
Size: 258010 Color: 2
Size: 256228 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 448241 Color: 3
Size: 287625 Color: 0
Size: 264135 Color: 1

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 421485 Color: 4
Size: 291092 Color: 3
Size: 287424 Color: 3

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 351030 Color: 0
Size: 341210 Color: 4
Size: 307761 Color: 2

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 453577 Color: 0
Size: 294530 Color: 2
Size: 251894 Color: 3

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 428822 Color: 4
Size: 286740 Color: 0
Size: 284439 Color: 3

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 417335 Color: 0
Size: 318391 Color: 1
Size: 264275 Color: 4

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 409152 Color: 1
Size: 312090 Color: 3
Size: 278759 Color: 2

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 429085 Color: 4
Size: 317933 Color: 1
Size: 252983 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 472171 Color: 3
Size: 272134 Color: 1
Size: 255696 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 375067 Color: 1
Size: 323344 Color: 1
Size: 301590 Color: 4

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 364754 Color: 1
Size: 334497 Color: 3
Size: 300750 Color: 2

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 418691 Color: 0
Size: 300151 Color: 2
Size: 281159 Color: 3

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 469681 Color: 3
Size: 272186 Color: 2
Size: 258134 Color: 2

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 430513 Color: 1
Size: 302780 Color: 0
Size: 266708 Color: 4

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 391005 Color: 4
Size: 337184 Color: 0
Size: 271812 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 453915 Color: 1
Size: 286701 Color: 0
Size: 259385 Color: 3

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 388692 Color: 4
Size: 344080 Color: 2
Size: 267229 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 428728 Color: 2
Size: 319309 Color: 3
Size: 251964 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 355859 Color: 2
Size: 338105 Color: 0
Size: 306037 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 406101 Color: 2
Size: 337866 Color: 3
Size: 256034 Color: 2

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 381644 Color: 2
Size: 328269 Color: 4
Size: 290088 Color: 1

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 409586 Color: 2
Size: 320241 Color: 1
Size: 270174 Color: 1

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 453664 Color: 1
Size: 290890 Color: 2
Size: 255447 Color: 3

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 374942 Color: 4
Size: 354792 Color: 3
Size: 270267 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 432269 Color: 4
Size: 291116 Color: 2
Size: 276616 Color: 4

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 405555 Color: 2
Size: 312106 Color: 1
Size: 282340 Color: 4

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 378416 Color: 0
Size: 343433 Color: 1
Size: 278152 Color: 3

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 460463 Color: 2
Size: 277485 Color: 1
Size: 262053 Color: 4

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 458882 Color: 4
Size: 284089 Color: 2
Size: 257030 Color: 3

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 353849 Color: 0
Size: 350712 Color: 1
Size: 295440 Color: 4

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 483067 Color: 3
Size: 264542 Color: 4
Size: 252392 Color: 3

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 402290 Color: 3
Size: 332646 Color: 4
Size: 265065 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 348892 Color: 0
Size: 336076 Color: 2
Size: 315033 Color: 1

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 441272 Color: 0
Size: 285935 Color: 4
Size: 272794 Color: 3

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 353537 Color: 2
Size: 326188 Color: 2
Size: 320276 Color: 1

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 397854 Color: 4
Size: 302741 Color: 4
Size: 299406 Color: 3

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 381349 Color: 1
Size: 346977 Color: 3
Size: 271675 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 434528 Color: 1
Size: 285643 Color: 4
Size: 279830 Color: 2

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 379916 Color: 3
Size: 349008 Color: 1
Size: 271077 Color: 4

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 426957 Color: 2
Size: 312637 Color: 3
Size: 260407 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 481060 Color: 4
Size: 268616 Color: 1
Size: 250325 Color: 2

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 458100 Color: 4
Size: 289059 Color: 4
Size: 252842 Color: 1

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 466767 Color: 4
Size: 268039 Color: 2
Size: 265195 Color: 3

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 390146 Color: 0
Size: 353447 Color: 3
Size: 256408 Color: 4

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 360178 Color: 1
Size: 356629 Color: 2
Size: 283194 Color: 3

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 441431 Color: 1
Size: 293422 Color: 0
Size: 265148 Color: 4

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 410612 Color: 4
Size: 309837 Color: 2
Size: 279552 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 384956 Color: 2
Size: 342524 Color: 1
Size: 272521 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 465873 Color: 2
Size: 281157 Color: 4
Size: 252971 Color: 3

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 380505 Color: 1
Size: 323223 Color: 0
Size: 296273 Color: 3

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 495337 Color: 4
Size: 252966 Color: 3
Size: 251698 Color: 1

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 394218 Color: 3
Size: 353558 Color: 3
Size: 252225 Color: 1

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 424213 Color: 4
Size: 316713 Color: 2
Size: 259075 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 450749 Color: 0
Size: 285628 Color: 4
Size: 263624 Color: 1

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 397379 Color: 4
Size: 323935 Color: 1
Size: 278687 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 482834 Color: 4
Size: 260585 Color: 0
Size: 256582 Color: 3

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 452681 Color: 2
Size: 281019 Color: 4
Size: 266301 Color: 3

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 409489 Color: 3
Size: 319130 Color: 1
Size: 271382 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 433040 Color: 4
Size: 306642 Color: 3
Size: 260319 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 388965 Color: 3
Size: 346931 Color: 0
Size: 264105 Color: 2

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 450742 Color: 1
Size: 297498 Color: 3
Size: 251761 Color: 1

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 387836 Color: 4
Size: 339707 Color: 0
Size: 272458 Color: 1

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 387253 Color: 2
Size: 337883 Color: 4
Size: 274865 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 392939 Color: 0
Size: 333433 Color: 1
Size: 273629 Color: 3

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 406139 Color: 0
Size: 333680 Color: 4
Size: 260182 Color: 2

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 467526 Color: 0
Size: 280322 Color: 0
Size: 252153 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 468497 Color: 3
Size: 271843 Color: 0
Size: 259661 Color: 4

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 410645 Color: 2
Size: 316591 Color: 0
Size: 272765 Color: 4

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 419247 Color: 3
Size: 327110 Color: 0
Size: 253644 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 396287 Color: 2
Size: 313420 Color: 4
Size: 290294 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 494349 Color: 1
Size: 255344 Color: 4
Size: 250308 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 410680 Color: 1
Size: 305351 Color: 2
Size: 283970 Color: 3

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 371451 Color: 1
Size: 346576 Color: 0
Size: 281974 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 386620 Color: 1
Size: 329259 Color: 0
Size: 284122 Color: 4

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 422412 Color: 1
Size: 306068 Color: 1
Size: 271521 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 422493 Color: 2
Size: 314880 Color: 3
Size: 262628 Color: 3

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 475457 Color: 4
Size: 266909 Color: 3
Size: 257635 Color: 2

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 382288 Color: 2
Size: 342964 Color: 3
Size: 274749 Color: 4

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 359517 Color: 0
Size: 345682 Color: 2
Size: 294802 Color: 2

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 444969 Color: 4
Size: 278438 Color: 0
Size: 276594 Color: 2

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 369224 Color: 2
Size: 361406 Color: 4
Size: 269371 Color: 4

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 464969 Color: 3
Size: 283217 Color: 2
Size: 251815 Color: 4

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 469930 Color: 1
Size: 275489 Color: 0
Size: 254582 Color: 4

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 445155 Color: 4
Size: 302189 Color: 3
Size: 252657 Color: 3

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 396840 Color: 3
Size: 329793 Color: 4
Size: 273368 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 414133 Color: 3
Size: 330884 Color: 2
Size: 254984 Color: 3

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 367096 Color: 3
Size: 328760 Color: 3
Size: 304145 Color: 2

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 381437 Color: 0
Size: 335466 Color: 3
Size: 283098 Color: 4

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 474507 Color: 3
Size: 274053 Color: 2
Size: 251441 Color: 1

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 457342 Color: 3
Size: 289466 Color: 4
Size: 253193 Color: 2

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 389605 Color: 1
Size: 339506 Color: 4
Size: 270890 Color: 3

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 467347 Color: 4
Size: 272340 Color: 1
Size: 260314 Color: 4

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 346126 Color: 1
Size: 339854 Color: 2
Size: 314021 Color: 3

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 475415 Color: 4
Size: 272553 Color: 4
Size: 252033 Color: 1

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 378914 Color: 2
Size: 333395 Color: 2
Size: 287692 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 485068 Color: 0
Size: 260014 Color: 4
Size: 254919 Color: 3

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 495646 Color: 0
Size: 254118 Color: 4
Size: 250237 Color: 2

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 413722 Color: 0
Size: 313291 Color: 3
Size: 272988 Color: 4

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 390249 Color: 0
Size: 317921 Color: 2
Size: 291831 Color: 3

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 370931 Color: 3
Size: 343924 Color: 0
Size: 285146 Color: 1

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 394388 Color: 2
Size: 348292 Color: 3
Size: 257321 Color: 4

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 409537 Color: 0
Size: 327874 Color: 4
Size: 262590 Color: 4

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 391816 Color: 0
Size: 306504 Color: 4
Size: 301681 Color: 1

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 460367 Color: 1
Size: 288006 Color: 1
Size: 251628 Color: 3

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 389212 Color: 3
Size: 351422 Color: 2
Size: 259367 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 388900 Color: 4
Size: 315703 Color: 2
Size: 295398 Color: 1

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 471966 Color: 0
Size: 274125 Color: 1
Size: 253910 Color: 2

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 431221 Color: 1
Size: 303245 Color: 3
Size: 265535 Color: 4

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 379739 Color: 2
Size: 340163 Color: 4
Size: 280099 Color: 3

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 462321 Color: 1
Size: 280718 Color: 3
Size: 256962 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 442511 Color: 3
Size: 295863 Color: 4
Size: 261627 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 430241 Color: 0
Size: 318894 Color: 4
Size: 250866 Color: 4

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 472922 Color: 3
Size: 267705 Color: 2
Size: 259374 Color: 2

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 419180 Color: 1
Size: 306886 Color: 4
Size: 273935 Color: 4

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 361729 Color: 4
Size: 351688 Color: 0
Size: 286584 Color: 3

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 495425 Color: 3
Size: 253996 Color: 2
Size: 250580 Color: 4

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 366470 Color: 3
Size: 334033 Color: 1
Size: 299498 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 396799 Color: 3
Size: 315122 Color: 0
Size: 288080 Color: 2

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 399911 Color: 2
Size: 336692 Color: 1
Size: 263398 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 413286 Color: 2
Size: 326267 Color: 0
Size: 260448 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 450164 Color: 2
Size: 284708 Color: 0
Size: 265129 Color: 4

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 439233 Color: 4
Size: 305347 Color: 3
Size: 255421 Color: 3

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 343994 Color: 0
Size: 342474 Color: 2
Size: 313533 Color: 2

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 493281 Color: 2
Size: 254338 Color: 0
Size: 252382 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 421040 Color: 0
Size: 299660 Color: 4
Size: 279301 Color: 4

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 362386 Color: 1
Size: 346499 Color: 3
Size: 291116 Color: 4

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 492991 Color: 0
Size: 255354 Color: 1
Size: 251656 Color: 3

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 467399 Color: 2
Size: 268136 Color: 3
Size: 264466 Color: 2

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 472424 Color: 3
Size: 271950 Color: 4
Size: 255627 Color: 4

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 382139 Color: 0
Size: 367033 Color: 3
Size: 250829 Color: 1

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 493646 Color: 4
Size: 255768 Color: 3
Size: 250587 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 442656 Color: 2
Size: 281088 Color: 0
Size: 276257 Color: 3

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 466794 Color: 4
Size: 272955 Color: 1
Size: 260252 Color: 2

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 409099 Color: 1
Size: 316435 Color: 4
Size: 274467 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 415414 Color: 1
Size: 298762 Color: 3
Size: 285825 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 495378 Color: 4
Size: 253242 Color: 1
Size: 251381 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 462314 Color: 4
Size: 275820 Color: 4
Size: 261867 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 374749 Color: 1
Size: 329589 Color: 2
Size: 295663 Color: 2

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 413946 Color: 2
Size: 302554 Color: 0
Size: 283501 Color: 2

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 361561 Color: 4
Size: 339010 Color: 2
Size: 299430 Color: 4

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 412088 Color: 2
Size: 327767 Color: 3
Size: 260146 Color: 3

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 443786 Color: 3
Size: 280900 Color: 3
Size: 275315 Color: 4

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 428656 Color: 1
Size: 303670 Color: 4
Size: 267675 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 425367 Color: 1
Size: 323762 Color: 4
Size: 250872 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 479261 Color: 3
Size: 263775 Color: 2
Size: 256965 Color: 2

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 439554 Color: 1
Size: 305180 Color: 4
Size: 255267 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 366390 Color: 2
Size: 331223 Color: 4
Size: 302388 Color: 2

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 435486 Color: 4
Size: 310120 Color: 2
Size: 254395 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 495222 Color: 1
Size: 252989 Color: 2
Size: 251790 Color: 3

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 446053 Color: 3
Size: 297900 Color: 4
Size: 256048 Color: 2

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 399399 Color: 0
Size: 319804 Color: 1
Size: 280798 Color: 2

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 376780 Color: 4
Size: 350947 Color: 3
Size: 272274 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 364943 Color: 1
Size: 357237 Color: 3
Size: 277821 Color: 1

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 359120 Color: 1
Size: 357641 Color: 0
Size: 283240 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 453388 Color: 3
Size: 293810 Color: 4
Size: 252803 Color: 3

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 463170 Color: 2
Size: 277901 Color: 0
Size: 258930 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 368486 Color: 3
Size: 343782 Color: 0
Size: 287733 Color: 1

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 371304 Color: 3
Size: 345103 Color: 4
Size: 283594 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 387619 Color: 3
Size: 336923 Color: 2
Size: 275459 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 401761 Color: 0
Size: 327482 Color: 4
Size: 270758 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 419643 Color: 1
Size: 304439 Color: 3
Size: 275919 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 427005 Color: 4
Size: 318308 Color: 2
Size: 254688 Color: 1

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 441315 Color: 3
Size: 299148 Color: 4
Size: 259538 Color: 3

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 497045 Color: 3
Size: 251982 Color: 1
Size: 250974 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 362208 Color: 3
Size: 323808 Color: 2
Size: 313985 Color: 2

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 357241 Color: 0
Size: 348237 Color: 3
Size: 294523 Color: 2

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 359271 Color: 4
Size: 355505 Color: 0
Size: 285225 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 397776 Color: 4
Size: 315017 Color: 0
Size: 287208 Color: 3

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 408404 Color: 0
Size: 298781 Color: 2
Size: 292816 Color: 3

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 401982 Color: 2
Size: 326528 Color: 3
Size: 271491 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 427243 Color: 0
Size: 286763 Color: 4
Size: 285995 Color: 3

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 394951 Color: 0
Size: 341179 Color: 2
Size: 263871 Color: 3

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 476190 Color: 1
Size: 270932 Color: 3
Size: 252879 Color: 1

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 406217 Color: 2
Size: 334161 Color: 4
Size: 259623 Color: 3

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 410340 Color: 0
Size: 301801 Color: 4
Size: 287860 Color: 2

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 365960 Color: 0
Size: 327126 Color: 3
Size: 306915 Color: 1

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 451989 Color: 2
Size: 295953 Color: 4
Size: 252059 Color: 3

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 478581 Color: 1
Size: 263329 Color: 4
Size: 258091 Color: 3

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 408680 Color: 3
Size: 339936 Color: 0
Size: 251385 Color: 4

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 421951 Color: 0
Size: 324124 Color: 4
Size: 253926 Color: 4

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 454299 Color: 3
Size: 287965 Color: 1
Size: 257737 Color: 4

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 356651 Color: 4
Size: 339691 Color: 2
Size: 303659 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 363206 Color: 2
Size: 327635 Color: 1
Size: 309160 Color: 1

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 460683 Color: 1
Size: 285106 Color: 2
Size: 254212 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 354144 Color: 2
Size: 344119 Color: 0
Size: 301738 Color: 3

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 365880 Color: 4
Size: 348859 Color: 3
Size: 285262 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 345575 Color: 4
Size: 337511 Color: 1
Size: 316915 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 361375 Color: 1
Size: 320143 Color: 3
Size: 318483 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 348575 Color: 1
Size: 333799 Color: 4
Size: 317627 Color: 4

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 366083 Color: 3
Size: 334505 Color: 2
Size: 299413 Color: 1

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 344853 Color: 4
Size: 335033 Color: 0
Size: 320115 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 358122 Color: 1
Size: 336446 Color: 0
Size: 305433 Color: 4

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 352322 Color: 3
Size: 335518 Color: 4
Size: 312161 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 349983 Color: 0
Size: 348072 Color: 0
Size: 301946 Color: 3

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 352587 Color: 4
Size: 329610 Color: 2
Size: 317804 Color: 1

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 361317 Color: 2
Size: 324220 Color: 3
Size: 314464 Color: 4

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 354114 Color: 2
Size: 334710 Color: 0
Size: 311177 Color: 3

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 343104 Color: 0
Size: 334371 Color: 3
Size: 322526 Color: 3

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 357717 Color: 4
Size: 354723 Color: 0
Size: 287561 Color: 1

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 356590 Color: 3
Size: 337415 Color: 0
Size: 305996 Color: 4

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 357295 Color: 1
Size: 352311 Color: 3
Size: 290395 Color: 3

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 367155 Color: 2
Size: 353536 Color: 3
Size: 279310 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 395694 Color: 0
Size: 333872 Color: 2
Size: 270435 Color: 4

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 346969 Color: 4
Size: 335791 Color: 0
Size: 317241 Color: 4

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 371737 Color: 4
Size: 334581 Color: 3
Size: 293683 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 365770 Color: 0
Size: 318983 Color: 1
Size: 315248 Color: 1

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 382709 Color: 0
Size: 353549 Color: 3
Size: 263743 Color: 1

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 355051 Color: 3
Size: 326512 Color: 2
Size: 318438 Color: 4

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 359088 Color: 4
Size: 327460 Color: 2
Size: 313453 Color: 2

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 361404 Color: 1
Size: 346978 Color: 3
Size: 291619 Color: 3

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 364353 Color: 0
Size: 357810 Color: 3
Size: 277838 Color: 4

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 353510 Color: 2
Size: 336684 Color: 3
Size: 309807 Color: 4

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 363704 Color: 2
Size: 362518 Color: 2
Size: 273779 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 363125 Color: 4
Size: 356789 Color: 1
Size: 280087 Color: 2

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 354124 Color: 3
Size: 349150 Color: 3
Size: 296727 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 338169 Color: 4
Size: 331447 Color: 3
Size: 330385 Color: 1

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 367811 Color: 1
Size: 364813 Color: 4
Size: 267377 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 347254 Color: 3
Size: 344599 Color: 2
Size: 308148 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 392700 Color: 3
Size: 354915 Color: 2
Size: 252386 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 349167 Color: 1
Size: 332747 Color: 1
Size: 318087 Color: 2

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 349557 Color: 2
Size: 330354 Color: 2
Size: 320090 Color: 3

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 353483 Color: 4
Size: 332553 Color: 3
Size: 313965 Color: 2

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 351023 Color: 1
Size: 348043 Color: 3
Size: 300935 Color: 3

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 355997 Color: 2
Size: 349242 Color: 4
Size: 294762 Color: 2

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 348257 Color: 3
Size: 339125 Color: 0
Size: 312619 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 360515 Color: 0
Size: 342971 Color: 0
Size: 296515 Color: 3

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 344470 Color: 4
Size: 344379 Color: 0
Size: 311152 Color: 1

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 343233 Color: 4
Size: 335781 Color: 0
Size: 320987 Color: 2

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 356259 Color: 3
Size: 340219 Color: 4
Size: 303523 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 356391 Color: 3
Size: 355133 Color: 0
Size: 288477 Color: 3

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 356915 Color: 1
Size: 329602 Color: 2
Size: 313484 Color: 2

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 347275 Color: 2
Size: 331407 Color: 1
Size: 321319 Color: 2

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 345146 Color: 2
Size: 328213 Color: 4
Size: 326642 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 351952 Color: 4
Size: 333536 Color: 1
Size: 314513 Color: 3

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 359991 Color: 0
Size: 354187 Color: 3
Size: 285823 Color: 1

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 358262 Color: 0
Size: 321688 Color: 1
Size: 320051 Color: 2

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 358991 Color: 2
Size: 333986 Color: 0
Size: 307024 Color: 3

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 361340 Color: 0
Size: 324133 Color: 0
Size: 314528 Color: 3

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 350317 Color: 3
Size: 342454 Color: 4
Size: 307230 Color: 1

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 364063 Color: 2
Size: 361548 Color: 4
Size: 274390 Color: 1

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 354153 Color: 0
Size: 348277 Color: 3
Size: 297571 Color: 1

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 357714 Color: 0
Size: 337813 Color: 1
Size: 304474 Color: 4

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 353660 Color: 2
Size: 350587 Color: 4
Size: 295754 Color: 1

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 359782 Color: 3
Size: 356690 Color: 4
Size: 283529 Color: 2

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 348613 Color: 4
Size: 332040 Color: 2
Size: 319348 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 358722 Color: 2
Size: 346513 Color: 0
Size: 294766 Color: 2

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 360944 Color: 2
Size: 358460 Color: 2
Size: 280597 Color: 1

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 362479 Color: 2
Size: 350205 Color: 0
Size: 287317 Color: 2

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 348334 Color: 0
Size: 340407 Color: 3
Size: 311260 Color: 3

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 358557 Color: 1
Size: 342283 Color: 2
Size: 299161 Color: 2

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 349682 Color: 1
Size: 332636 Color: 0
Size: 317683 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 355725 Color: 3
Size: 344417 Color: 2
Size: 299859 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 356341 Color: 1
Size: 330943 Color: 4
Size: 312717 Color: 3

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 368447 Color: 2
Size: 354889 Color: 1
Size: 276665 Color: 2

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 361999 Color: 1
Size: 342741 Color: 0
Size: 295261 Color: 1

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 354905 Color: 3
Size: 334371 Color: 3
Size: 310725 Color: 4

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 356554 Color: 3
Size: 348803 Color: 1
Size: 294644 Color: 2

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 354094 Color: 3
Size: 351008 Color: 0
Size: 294899 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 340126 Color: 2
Size: 331828 Color: 4
Size: 328047 Color: 2

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 346461 Color: 4
Size: 327432 Color: 4
Size: 326108 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 338584 Color: 0
Size: 333226 Color: 0
Size: 328191 Color: 2

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 355717 Color: 1
Size: 328593 Color: 2
Size: 315691 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 342152 Color: 3
Size: 335665 Color: 4
Size: 322184 Color: 3

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 486919 Color: 1
Size: 258223 Color: 4
Size: 254859 Color: 4

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 356325 Color: 1
Size: 347078 Color: 3
Size: 296598 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 343584 Color: 0
Size: 340731 Color: 0
Size: 315686 Color: 1

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 337629 Color: 1
Size: 332056 Color: 0
Size: 330316 Color: 4

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 369105 Color: 1
Size: 355049 Color: 4
Size: 275847 Color: 4

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 350049 Color: 4
Size: 328804 Color: 0
Size: 321148 Color: 2

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 350142 Color: 0
Size: 334293 Color: 4
Size: 315566 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 335526 Color: 0
Size: 333487 Color: 0
Size: 330988 Color: 4

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 349092 Color: 0
Size: 333301 Color: 4
Size: 317608 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 497964 Color: 0
Size: 251203 Color: 0
Size: 250834 Color: 4

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 353147 Color: 2
Size: 350549 Color: 1
Size: 296305 Color: 2

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 408755 Color: 4
Size: 338376 Color: 4
Size: 252870 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 355219 Color: 3
Size: 326310 Color: 2
Size: 318472 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 344631 Color: 4
Size: 330575 Color: 0
Size: 324795 Color: 2

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 387843 Color: 3
Size: 357600 Color: 4
Size: 254558 Color: 3

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 352903 Color: 2
Size: 327783 Color: 4
Size: 319315 Color: 1

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 352854 Color: 2
Size: 352668 Color: 3
Size: 294479 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 357105 Color: 2
Size: 351550 Color: 4
Size: 291346 Color: 4

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 351362 Color: 4
Size: 338943 Color: 1
Size: 309696 Color: 3

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 398797 Color: 4
Size: 346646 Color: 2
Size: 254558 Color: 4

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 358776 Color: 1
Size: 349936 Color: 3
Size: 291289 Color: 4

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 358342 Color: 0
Size: 340946 Color: 1
Size: 300713 Color: 4

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 482743 Color: 0
Size: 259242 Color: 4
Size: 258016 Color: 2

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 357915 Color: 4
Size: 342806 Color: 0
Size: 299280 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 343196 Color: 4
Size: 342506 Color: 2
Size: 314299 Color: 4

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 354198 Color: 2
Size: 345703 Color: 3
Size: 300100 Color: 1

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 351341 Color: 0
Size: 339051 Color: 4
Size: 309609 Color: 3

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 355753 Color: 3
Size: 337909 Color: 4
Size: 306339 Color: 3

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 341729 Color: 4
Size: 336969 Color: 3
Size: 321303 Color: 2

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 346186 Color: 2
Size: 345546 Color: 0
Size: 308269 Color: 1

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 351183 Color: 1
Size: 346551 Color: 0
Size: 302267 Color: 2

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 358042 Color: 4
Size: 345841 Color: 1
Size: 296118 Color: 4

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 345654 Color: 1
Size: 344990 Color: 3
Size: 309357 Color: 1

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 351097 Color: 2
Size: 345452 Color: 1
Size: 303452 Color: 1

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 344082 Color: 2
Size: 336262 Color: 1
Size: 319657 Color: 3

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 364851 Color: 3
Size: 344793 Color: 4
Size: 290357 Color: 2

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 354582 Color: 4
Size: 344070 Color: 2
Size: 301349 Color: 3

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 352033 Color: 2
Size: 344566 Color: 3
Size: 303402 Color: 1

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 370602 Color: 3
Size: 364100 Color: 4
Size: 265299 Color: 4

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 348569 Color: 2
Size: 342992 Color: 1
Size: 308440 Color: 2

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 346671 Color: 0
Size: 342703 Color: 1
Size: 310627 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 341698 Color: 2
Size: 340700 Color: 0
Size: 317603 Color: 2

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 353100 Color: 2
Size: 340552 Color: 3
Size: 306349 Color: 2

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 356875 Color: 4
Size: 340954 Color: 0
Size: 302172 Color: 4

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 342868 Color: 4
Size: 338223 Color: 0
Size: 318910 Color: 4

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 351393 Color: 3
Size: 340575 Color: 3
Size: 308033 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 337003 Color: 1
Size: 334320 Color: 1
Size: 328678 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 363666 Color: 4
Size: 346873 Color: 0
Size: 289462 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 346829 Color: 1
Size: 336209 Color: 0
Size: 316963 Color: 1

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 349319 Color: 3
Size: 325401 Color: 3
Size: 325281 Color: 1

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 337812 Color: 1
Size: 337345 Color: 3
Size: 324844 Color: 3

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 353744 Color: 3
Size: 335657 Color: 0
Size: 310600 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 347895 Color: 1
Size: 345511 Color: 3
Size: 306595 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 345029 Color: 3
Size: 329298 Color: 4
Size: 325674 Color: 4

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 348114 Color: 0
Size: 331878 Color: 2
Size: 320009 Color: 4

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 356660 Color: 0
Size: 329641 Color: 3
Size: 313700 Color: 4

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 356897 Color: 2
Size: 327881 Color: 4
Size: 315223 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 357933 Color: 4
Size: 325600 Color: 2
Size: 316468 Color: 4

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 358175 Color: 4
Size: 337387 Color: 2
Size: 304439 Color: 2

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 358095 Color: 3
Size: 334348 Color: 4
Size: 307558 Color: 3

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 358794 Color: 4
Size: 332045 Color: 1
Size: 309162 Color: 4

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 359724 Color: 1
Size: 332655 Color: 4
Size: 307622 Color: 3

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 360152 Color: 1
Size: 353106 Color: 2
Size: 286743 Color: 4

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 360313 Color: 0
Size: 343718 Color: 3
Size: 295970 Color: 3

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 360657 Color: 0
Size: 326532 Color: 3
Size: 312812 Color: 4

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 360539 Color: 1
Size: 323038 Color: 2
Size: 316424 Color: 4

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 360849 Color: 2
Size: 321809 Color: 3
Size: 317343 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 361309 Color: 3
Size: 341664 Color: 3
Size: 297028 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 361402 Color: 3
Size: 358192 Color: 2
Size: 280407 Color: 3

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 361560 Color: 0
Size: 330527 Color: 3
Size: 307914 Color: 1

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 361709 Color: 0
Size: 358176 Color: 2
Size: 280116 Color: 4

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 361737 Color: 0
Size: 345556 Color: 3
Size: 292708 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 361749 Color: 3
Size: 336112 Color: 2
Size: 302140 Color: 4

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 361754 Color: 3
Size: 341220 Color: 1
Size: 297027 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 362300 Color: 1
Size: 343544 Color: 4
Size: 294157 Color: 0

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 362520 Color: 3
Size: 355189 Color: 0
Size: 282292 Color: 4

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 362607 Color: 2
Size: 324336 Color: 3
Size: 313058 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 362724 Color: 4
Size: 326188 Color: 0
Size: 311089 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 362565 Color: 1
Size: 330690 Color: 1
Size: 306746 Color: 3

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 362748 Color: 1
Size: 329374 Color: 4
Size: 307879 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 362880 Color: 2
Size: 324872 Color: 0
Size: 312249 Color: 1

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 362827 Color: 1
Size: 343347 Color: 2
Size: 293827 Color: 3

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 362993 Color: 4
Size: 339727 Color: 2
Size: 297281 Color: 2

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 362939 Color: 1
Size: 350994 Color: 2
Size: 286068 Color: 3

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 363027 Color: 1
Size: 345369 Color: 4
Size: 291605 Color: 3

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 363123 Color: 1
Size: 334782 Color: 4
Size: 302096 Color: 3

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 363218 Color: 3
Size: 327711 Color: 0
Size: 309072 Color: 4

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 363124 Color: 1
Size: 349761 Color: 3
Size: 287116 Color: 3

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 363546 Color: 0
Size: 328406 Color: 4
Size: 308049 Color: 1

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 363577 Color: 0
Size: 329264 Color: 4
Size: 307160 Color: 3

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 363408 Color: 1
Size: 324342 Color: 2
Size: 312251 Color: 2

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 363578 Color: 0
Size: 336239 Color: 2
Size: 300184 Color: 2

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 363544 Color: 2
Size: 327473 Color: 1
Size: 308984 Color: 4

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 363631 Color: 0
Size: 335134 Color: 2
Size: 301236 Color: 4

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 363646 Color: 3
Size: 333576 Color: 2
Size: 302779 Color: 3

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 363657 Color: 1
Size: 338085 Color: 1
Size: 298259 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 363915 Color: 2
Size: 348998 Color: 4
Size: 287088 Color: 2

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 364096 Color: 0
Size: 325877 Color: 2
Size: 310028 Color: 4

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 364048 Color: 1
Size: 362154 Color: 2
Size: 273799 Color: 1

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 364286 Color: 0
Size: 354292 Color: 4
Size: 281423 Color: 2

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 364338 Color: 1
Size: 318071 Color: 0
Size: 317592 Color: 3

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 364427 Color: 1
Size: 344499 Color: 4
Size: 291075 Color: 2

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 364368 Color: 3
Size: 327562 Color: 3
Size: 308071 Color: 2

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 364415 Color: 4
Size: 336505 Color: 1
Size: 299081 Color: 4

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 364671 Color: 0
Size: 319971 Color: 4
Size: 315359 Color: 2

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 364737 Color: 3
Size: 336939 Color: 1
Size: 298325 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 364817 Color: 3
Size: 362954 Color: 4
Size: 272230 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 364959 Color: 4
Size: 336023 Color: 2
Size: 299019 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 365036 Color: 4
Size: 342461 Color: 1
Size: 292504 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 365175 Color: 2
Size: 358573 Color: 4
Size: 276253 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 365227 Color: 3
Size: 349100 Color: 2
Size: 285674 Color: 3

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 365299 Color: 3
Size: 343821 Color: 2
Size: 290881 Color: 1

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 365369 Color: 2
Size: 362342 Color: 0
Size: 272290 Color: 2

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 365458 Color: 3
Size: 326050 Color: 2
Size: 308493 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 365494 Color: 3
Size: 338910 Color: 0
Size: 295597 Color: 3

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 365505 Color: 0
Size: 329767 Color: 3
Size: 304729 Color: 1

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 365555 Color: 2
Size: 320852 Color: 3
Size: 313594 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 365563 Color: 4
Size: 361603 Color: 1
Size: 272835 Color: 2

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 365575 Color: 0
Size: 336867 Color: 1
Size: 297559 Color: 3

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 365618 Color: 4
Size: 322271 Color: 3
Size: 312112 Color: 4

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 365647 Color: 0
Size: 335252 Color: 1
Size: 299102 Color: 3

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 365733 Color: 4
Size: 352621 Color: 2
Size: 281647 Color: 4

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 365792 Color: 0
Size: 346082 Color: 1
Size: 288127 Color: 2

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 366020 Color: 0
Size: 363142 Color: 3
Size: 270839 Color: 2

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 365744 Color: 1
Size: 364218 Color: 4
Size: 270039 Color: 3

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 366166 Color: 4
Size: 364434 Color: 1
Size: 269401 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 365865 Color: 1
Size: 361373 Color: 4
Size: 272763 Color: 2

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 366299 Color: 3
Size: 323029 Color: 0
Size: 310673 Color: 4

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 366362 Color: 1
Size: 353829 Color: 2
Size: 279810 Color: 3

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 366356 Color: 2
Size: 345477 Color: 3
Size: 288168 Color: 4

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 366366 Color: 3
Size: 329537 Color: 2
Size: 304098 Color: 1

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 366430 Color: 2
Size: 326129 Color: 1
Size: 307442 Color: 2

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 366554 Color: 1
Size: 344345 Color: 3
Size: 289102 Color: 3

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 366431 Color: 4
Size: 356754 Color: 2
Size: 276816 Color: 3

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 366478 Color: 2
Size: 332675 Color: 4
Size: 300848 Color: 3

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 366542 Color: 2
Size: 359928 Color: 0
Size: 273531 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 366574 Color: 1
Size: 337402 Color: 4
Size: 296025 Color: 2

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 366562 Color: 0
Size: 344159 Color: 3
Size: 289280 Color: 2

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 366584 Color: 4
Size: 328100 Color: 1
Size: 305317 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 366594 Color: 2
Size: 366464 Color: 3
Size: 266943 Color: 4

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 366622 Color: 0
Size: 353055 Color: 0
Size: 280324 Color: 1

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 366674 Color: 4
Size: 350980 Color: 2
Size: 282347 Color: 4

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 366821 Color: 1
Size: 337241 Color: 0
Size: 295939 Color: 3

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 366755 Color: 0
Size: 331772 Color: 3
Size: 301474 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 366964 Color: 1
Size: 349176 Color: 2
Size: 283861 Color: 2

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 366783 Color: 0
Size: 335869 Color: 2
Size: 297349 Color: 3

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 366895 Color: 3
Size: 347049 Color: 4
Size: 286057 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 366941 Color: 4
Size: 342744 Color: 4
Size: 290316 Color: 1

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 366975 Color: 3
Size: 363350 Color: 0
Size: 269676 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 367029 Color: 4
Size: 325699 Color: 1
Size: 307273 Color: 3

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 367242 Color: 1
Size: 352822 Color: 4
Size: 279937 Color: 3

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 367292 Color: 3
Size: 361209 Color: 2
Size: 271500 Color: 1

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 367411 Color: 0
Size: 351178 Color: 0
Size: 281412 Color: 2

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 367557 Color: 1
Size: 325712 Color: 4
Size: 306732 Color: 2

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 367424 Color: 3
Size: 328545 Color: 3
Size: 304032 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 367451 Color: 3
Size: 340977 Color: 1
Size: 291573 Color: 3

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 367507 Color: 3
Size: 344464 Color: 2
Size: 288030 Color: 2

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 367531 Color: 0
Size: 347202 Color: 1
Size: 285268 Color: 2

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 367577 Color: 0
Size: 349900 Color: 1
Size: 282524 Color: 2

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 367599 Color: 3
Size: 320633 Color: 3
Size: 311769 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 367623 Color: 2
Size: 350550 Color: 1
Size: 281828 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 367605 Color: 1
Size: 330264 Color: 2
Size: 302132 Color: 3

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 367669 Color: 2
Size: 324257 Color: 4
Size: 308075 Color: 2

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 367620 Color: 3
Size: 317021 Color: 4
Size: 315360 Color: 2

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 367786 Color: 2
Size: 330768 Color: 1
Size: 301447 Color: 4

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 367871 Color: 2
Size: 329091 Color: 1
Size: 303039 Color: 4

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 367875 Color: 4
Size: 340883 Color: 3
Size: 291243 Color: 1

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 367886 Color: 3
Size: 321032 Color: 4
Size: 311083 Color: 3

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 367923 Color: 0
Size: 325522 Color: 0
Size: 306556 Color: 1

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 367936 Color: 1
Size: 329114 Color: 0
Size: 302951 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 368055 Color: 0
Size: 356336 Color: 3
Size: 275610 Color: 2

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 368277 Color: 1
Size: 350594 Color: 0
Size: 281130 Color: 2

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 368101 Color: 2
Size: 360481 Color: 0
Size: 271419 Color: 3

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 368186 Color: 2
Size: 320709 Color: 1
Size: 311106 Color: 2

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 367899 Color: 3
Size: 329549 Color: 0
Size: 302553 Color: 3

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 368319 Color: 4
Size: 337977 Color: 0
Size: 293705 Color: 1

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 367922 Color: 3
Size: 325432 Color: 1
Size: 306647 Color: 4

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 368173 Color: 3
Size: 362165 Color: 4
Size: 269663 Color: 2

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 368556 Color: 2
Size: 333098 Color: 1
Size: 298347 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 368610 Color: 2
Size: 362200 Color: 4
Size: 269191 Color: 2

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 368636 Color: 2
Size: 353124 Color: 4
Size: 278241 Color: 1

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 368648 Color: 4
Size: 348720 Color: 0
Size: 282633 Color: 3

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 368701 Color: 4
Size: 356136 Color: 1
Size: 275164 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 368927 Color: 1
Size: 365534 Color: 0
Size: 265540 Color: 3

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 368859 Color: 4
Size: 341535 Color: 3
Size: 289607 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 369047 Color: 0
Size: 361801 Color: 2
Size: 269153 Color: 3

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 368642 Color: 3
Size: 321263 Color: 2
Size: 310096 Color: 1

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 369107 Color: 4
Size: 331540 Color: 0
Size: 299354 Color: 1

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 368763 Color: 3
Size: 331069 Color: 2
Size: 300169 Color: 4

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 369117 Color: 4
Size: 342691 Color: 1
Size: 288193 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 369147 Color: 4
Size: 344944 Color: 0
Size: 285910 Color: 2

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 369308 Color: 2
Size: 325671 Color: 3
Size: 305022 Color: 1

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 369576 Color: 0
Size: 364555 Color: 3
Size: 265870 Color: 1

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 368915 Color: 3
Size: 334826 Color: 1
Size: 296260 Color: 4

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 369581 Color: 0
Size: 341054 Color: 2
Size: 289366 Color: 1

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 369322 Color: 3
Size: 346435 Color: 2
Size: 284244 Color: 2

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 369653 Color: 4
Size: 361570 Color: 4
Size: 268778 Color: 3

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 369665 Color: 0
Size: 360025 Color: 2
Size: 270311 Color: 3

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 369766 Color: 3
Size: 322965 Color: 2
Size: 307270 Color: 2

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 369700 Color: 1
Size: 353820 Color: 0
Size: 276481 Color: 4

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 369702 Color: 0
Size: 355839 Color: 3
Size: 274460 Color: 2

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 369785 Color: 3
Size: 316333 Color: 0
Size: 313883 Color: 4

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 369768 Color: 4
Size: 361931 Color: 2
Size: 268302 Color: 1

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 369821 Color: 1
Size: 342314 Color: 3
Size: 287866 Color: 2

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 369892 Color: 0
Size: 320173 Color: 4
Size: 309936 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 369913 Color: 4
Size: 315391 Color: 4
Size: 314697 Color: 3

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 369859 Color: 3
Size: 338134 Color: 4
Size: 292008 Color: 2

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 369931 Color: 4
Size: 347651 Color: 1
Size: 282419 Color: 1

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 370032 Color: 1
Size: 357942 Color: 3
Size: 272027 Color: 1

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 370183 Color: 1
Size: 354207 Color: 3
Size: 275611 Color: 4

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 370085 Color: 2
Size: 323290 Color: 4
Size: 306626 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 370235 Color: 1
Size: 324468 Color: 3
Size: 305298 Color: 2

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 369895 Color: 3
Size: 334798 Color: 0
Size: 295308 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 370516 Color: 2
Size: 347771 Color: 2
Size: 281714 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 370575 Color: 0
Size: 351035 Color: 1
Size: 278391 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 370582 Color: 4
Size: 352133 Color: 3
Size: 277286 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 370612 Color: 4
Size: 366876 Color: 1
Size: 262513 Color: 2

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 370636 Color: 2
Size: 355303 Color: 3
Size: 274062 Color: 4

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 370639 Color: 4
Size: 369693 Color: 3
Size: 259669 Color: 1

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 370712 Color: 4
Size: 332043 Color: 2
Size: 297246 Color: 2

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 370711 Color: 1
Size: 320139 Color: 4
Size: 309151 Color: 3

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 370726 Color: 4
Size: 327144 Color: 3
Size: 302131 Color: 4

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 370812 Color: 2
Size: 339805 Color: 2
Size: 289384 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 370826 Color: 0
Size: 332755 Color: 3
Size: 296420 Color: 2

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 370766 Color: 1
Size: 355091 Color: 1
Size: 274144 Color: 3

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 370691 Color: 3
Size: 341228 Color: 0
Size: 288082 Color: 2

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 370833 Color: 4
Size: 350189 Color: 2
Size: 278979 Color: 1

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 370739 Color: 3
Size: 316598 Color: 4
Size: 312664 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 370865 Color: 0
Size: 365562 Color: 1
Size: 263574 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 370926 Color: 1
Size: 326503 Color: 3
Size: 302572 Color: 2

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 370849 Color: 3
Size: 359720 Color: 2
Size: 269432 Color: 4

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 370900 Color: 4
Size: 332230 Color: 4
Size: 296871 Color: 1

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 370927 Color: 1
Size: 350625 Color: 0
Size: 278449 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 371064 Color: 2
Size: 345109 Color: 4
Size: 283828 Color: 2

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 371097 Color: 2
Size: 347054 Color: 0
Size: 281850 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 371021 Color: 1
Size: 366863 Color: 0
Size: 262117 Color: 2

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 371128 Color: 0
Size: 367046 Color: 3
Size: 261827 Color: 4

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 371161 Color: 4
Size: 343815 Color: 1
Size: 285025 Color: 3

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 371168 Color: 3
Size: 324768 Color: 2
Size: 304065 Color: 4

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 371178 Color: 3
Size: 325126 Color: 0
Size: 303697 Color: 1

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 371188 Color: 0
Size: 347748 Color: 1
Size: 281065 Color: 3

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 371180 Color: 3
Size: 345013 Color: 4
Size: 283808 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 371225 Color: 0
Size: 351190 Color: 4
Size: 277586 Color: 1

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 371087 Color: 1
Size: 329852 Color: 4
Size: 299062 Color: 4

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 371404 Color: 1
Size: 324715 Color: 0
Size: 303882 Color: 3

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 371310 Color: 0
Size: 356387 Color: 4
Size: 272304 Color: 4

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 371496 Color: 4
Size: 329927 Color: 2
Size: 298578 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 371514 Color: 3
Size: 316113 Color: 0
Size: 312374 Color: 2

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 371554 Color: 0
Size: 323756 Color: 4
Size: 304691 Color: 1

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 371576 Color: 1
Size: 353403 Color: 0
Size: 275022 Color: 3

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 371579 Color: 2
Size: 324030 Color: 2
Size: 304392 Color: 3

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 371796 Color: 3
Size: 324243 Color: 3
Size: 303962 Color: 4

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 371855 Color: 3
Size: 336490 Color: 1
Size: 291656 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 371868 Color: 0
Size: 361170 Color: 1
Size: 266963 Color: 0

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 371919 Color: 3
Size: 340818 Color: 0
Size: 287264 Color: 2

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 371935 Color: 4
Size: 354733 Color: 4
Size: 273333 Color: 1

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 371960 Color: 4
Size: 329399 Color: 4
Size: 298642 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 372000 Color: 1
Size: 337687 Color: 2
Size: 290314 Color: 2

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 372077 Color: 0
Size: 319226 Color: 3
Size: 308698 Color: 2

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 372091 Color: 0
Size: 321316 Color: 1
Size: 306594 Color: 4

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 372017 Color: 1
Size: 333168 Color: 0
Size: 294816 Color: 4

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 372091 Color: 0
Size: 337205 Color: 2
Size: 290705 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 372099 Color: 3
Size: 366989 Color: 1
Size: 260913 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 372230 Color: 0
Size: 327577 Color: 4
Size: 300194 Color: 4

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 372243 Color: 2
Size: 362976 Color: 1
Size: 264782 Color: 2

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 372307 Color: 4
Size: 330579 Color: 1
Size: 297115 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 372458 Color: 0
Size: 343112 Color: 2
Size: 284431 Color: 3

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 372500 Color: 0
Size: 369199 Color: 1
Size: 258302 Color: 4

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 372527 Color: 0
Size: 324058 Color: 3
Size: 303416 Color: 1

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 372381 Color: 3
Size: 319145 Color: 0
Size: 308475 Color: 2

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 372536 Color: 0
Size: 342145 Color: 3
Size: 285320 Color: 1

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 372350 Color: 1
Size: 372193 Color: 3
Size: 255458 Color: 4

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 372504 Color: 4
Size: 360790 Color: 2
Size: 266707 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 372531 Color: 2
Size: 316961 Color: 1
Size: 310509 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 372619 Color: 3
Size: 359210 Color: 1
Size: 268172 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 372628 Color: 4
Size: 317029 Color: 3
Size: 310344 Color: 3

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 372392 Color: 1
Size: 369893 Color: 1
Size: 257716 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 372676 Color: 4
Size: 334947 Color: 0
Size: 292378 Color: 3

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 372448 Color: 1
Size: 322092 Color: 2
Size: 305461 Color: 3

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 372701 Color: 3
Size: 372419 Color: 0
Size: 254881 Color: 2

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 372732 Color: 4
Size: 321419 Color: 1
Size: 305850 Color: 4

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 372783 Color: 3
Size: 322229 Color: 0
Size: 304989 Color: 2

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 372856 Color: 0
Size: 356839 Color: 2
Size: 270306 Color: 1

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 372899 Color: 2
Size: 315697 Color: 0
Size: 311405 Color: 2

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 373043 Color: 4
Size: 338541 Color: 1
Size: 288417 Color: 2

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 372566 Color: 1
Size: 365097 Color: 2
Size: 262338 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 373073 Color: 1
Size: 372733 Color: 3
Size: 254195 Color: 4

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 373195 Color: 2
Size: 347896 Color: 3
Size: 278910 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 373196 Color: 0
Size: 343414 Color: 1
Size: 283391 Color: 4

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 373217 Color: 4
Size: 347171 Color: 4
Size: 279613 Color: 2

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 373261 Color: 3
Size: 366426 Color: 4
Size: 260314 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 373284 Color: 3
Size: 352320 Color: 3
Size: 274397 Color: 2

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 373287 Color: 1
Size: 351672 Color: 2
Size: 275042 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 373292 Color: 1
Size: 358100 Color: 0
Size: 268609 Color: 2

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 373345 Color: 2
Size: 322281 Color: 4
Size: 304375 Color: 3

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 373351 Color: 2
Size: 351751 Color: 4
Size: 274899 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 373381 Color: 3
Size: 340774 Color: 1
Size: 285846 Color: 2

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 373416 Color: 4
Size: 362046 Color: 1
Size: 264539 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 373636 Color: 4
Size: 336339 Color: 3
Size: 290026 Color: 1

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 373616 Color: 0
Size: 342271 Color: 3
Size: 284114 Color: 3

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 373709 Color: 4
Size: 350073 Color: 1
Size: 276219 Color: 1

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 373780 Color: 4
Size: 329095 Color: 3
Size: 297126 Color: 4

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 373835 Color: 2
Size: 330228 Color: 2
Size: 295938 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 373964 Color: 0
Size: 323885 Color: 4
Size: 302152 Color: 2

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 373859 Color: 2
Size: 354031 Color: 2
Size: 272111 Color: 1

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 373894 Color: 1
Size: 369628 Color: 4
Size: 256479 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 373894 Color: 2
Size: 357494 Color: 2
Size: 268613 Color: 3

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 373977 Color: 1
Size: 354542 Color: 0
Size: 271482 Color: 2

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 374040 Color: 3
Size: 322786 Color: 1
Size: 303175 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 374125 Color: 2
Size: 358633 Color: 3
Size: 267243 Color: 3

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 374128 Color: 1
Size: 358472 Color: 1
Size: 267401 Color: 4

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 374223 Color: 0
Size: 366575 Color: 1
Size: 259203 Color: 4

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 374180 Color: 4
Size: 328497 Color: 3
Size: 297324 Color: 1

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 374321 Color: 4
Size: 355572 Color: 1
Size: 270108 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 374429 Color: 3
Size: 347199 Color: 1
Size: 278373 Color: 4

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 374431 Color: 2
Size: 329687 Color: 0
Size: 295883 Color: 3

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 374553 Color: 4
Size: 359342 Color: 3
Size: 266106 Color: 3

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 374555 Color: 3
Size: 344834 Color: 0
Size: 280612 Color: 2

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 374600 Color: 1
Size: 334176 Color: 2
Size: 291225 Color: 2

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 374649 Color: 1
Size: 328050 Color: 4
Size: 297302 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 374658 Color: 0
Size: 326334 Color: 2
Size: 299009 Color: 4

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 374695 Color: 3
Size: 313107 Color: 2
Size: 312199 Color: 2

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 374723 Color: 1
Size: 315037 Color: 2
Size: 310241 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 374680 Color: 0
Size: 338811 Color: 4
Size: 286510 Color: 4

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 374806 Color: 1
Size: 333451 Color: 2
Size: 291744 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 374833 Color: 2
Size: 344686 Color: 1
Size: 280482 Color: 4

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 374860 Color: 3
Size: 360117 Color: 1
Size: 265024 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 374924 Color: 4
Size: 349749 Color: 3
Size: 275328 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 374972 Color: 2
Size: 336177 Color: 0
Size: 288852 Color: 3

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 375107 Color: 4
Size: 317255 Color: 3
Size: 307639 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 375090 Color: 1
Size: 349479 Color: 4
Size: 275432 Color: 2

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 375172 Color: 4
Size: 372166 Color: 4
Size: 252663 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 375047 Color: 0
Size: 338784 Color: 2
Size: 286170 Color: 1

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 375203 Color: 3
Size: 343165 Color: 2
Size: 281633 Color: 2

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 375346 Color: 0
Size: 320132 Color: 4
Size: 304523 Color: 4

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 375231 Color: 1
Size: 340142 Color: 3
Size: 284628 Color: 3

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 375324 Color: 2
Size: 372780 Color: 2
Size: 251897 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 375447 Color: 0
Size: 318938 Color: 2
Size: 305616 Color: 1

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 375339 Color: 4
Size: 349247 Color: 1
Size: 275415 Color: 3

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 375504 Color: 4
Size: 323377 Color: 1
Size: 301120 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 375314 Color: 1
Size: 367881 Color: 2
Size: 256806 Color: 4

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 375472 Color: 1
Size: 316142 Color: 0
Size: 308387 Color: 3

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 375518 Color: 2
Size: 317633 Color: 0
Size: 306850 Color: 2

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 375478 Color: 1
Size: 322060 Color: 4
Size: 302463 Color: 4

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 375609 Color: 4
Size: 367763 Color: 4
Size: 256629 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 375527 Color: 0
Size: 359335 Color: 1
Size: 265139 Color: 3

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 375610 Color: 2
Size: 313831 Color: 4
Size: 310560 Color: 2

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 375778 Color: 0
Size: 337589 Color: 2
Size: 286634 Color: 3

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 375619 Color: 1
Size: 371563 Color: 1
Size: 252819 Color: 4

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 375836 Color: 0
Size: 333295 Color: 4
Size: 290870 Color: 3

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 375824 Color: 2
Size: 362036 Color: 1
Size: 262141 Color: 3

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 375896 Color: 3
Size: 321383 Color: 4
Size: 302722 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 375942 Color: 3
Size: 371461 Color: 1
Size: 252598 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 375986 Color: 4
Size: 332720 Color: 1
Size: 291295 Color: 4

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 375993 Color: 4
Size: 371271 Color: 0
Size: 252737 Color: 2

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 376033 Color: 3
Size: 326180 Color: 0
Size: 297788 Color: 1

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 376126 Color: 3
Size: 362861 Color: 0
Size: 261014 Color: 4

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 376178 Color: 3
Size: 368099 Color: 4
Size: 255724 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 376231 Color: 0
Size: 360873 Color: 1
Size: 262897 Color: 4

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 376209 Color: 4
Size: 356072 Color: 2
Size: 267720 Color: 2

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 376214 Color: 2
Size: 347239 Color: 0
Size: 276548 Color: 4

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 376233 Color: 4
Size: 324326 Color: 3
Size: 299442 Color: 1

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 376318 Color: 3
Size: 358993 Color: 3
Size: 264690 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 376298 Color: 0
Size: 362758 Color: 1
Size: 260945 Color: 2

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 376338 Color: 1
Size: 341516 Color: 0
Size: 282147 Color: 3

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 376562 Color: 0
Size: 348228 Color: 4
Size: 275211 Color: 1

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 376456 Color: 3
Size: 316783 Color: 2
Size: 306762 Color: 2

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 376671 Color: 0
Size: 332318 Color: 3
Size: 291012 Color: 1

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 376607 Color: 3
Size: 343987 Color: 2
Size: 279407 Color: 4

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 376612 Color: 4
Size: 370707 Color: 0
Size: 252682 Color: 4

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 376644 Color: 4
Size: 321737 Color: 3
Size: 301620 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 376730 Color: 2
Size: 328562 Color: 3
Size: 294709 Color: 2

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 376767 Color: 2
Size: 312622 Color: 1
Size: 310612 Color: 4

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 376777 Color: 3
Size: 358361 Color: 0
Size: 264863 Color: 4

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 376920 Color: 1
Size: 343932 Color: 4
Size: 279149 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 376888 Color: 0
Size: 352262 Color: 2
Size: 270851 Color: 3

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 376961 Color: 2
Size: 368028 Color: 1
Size: 255012 Color: 1

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 377050 Color: 0
Size: 347939 Color: 1
Size: 275012 Color: 3

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 376991 Color: 1
Size: 359840 Color: 1
Size: 263170 Color: 3

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 377107 Color: 0
Size: 343128 Color: 2
Size: 279766 Color: 1

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 377052 Color: 1
Size: 354706 Color: 1
Size: 268243 Color: 2

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 377061 Color: 2
Size: 369815 Color: 3
Size: 253125 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 377161 Color: 1
Size: 359287 Color: 4
Size: 263553 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 377165 Color: 1
Size: 345031 Color: 1
Size: 277805 Color: 4

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 377230 Color: 4
Size: 359944 Color: 0
Size: 262827 Color: 2

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 377317 Color: 0
Size: 325449 Color: 2
Size: 297235 Color: 2

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 377242 Color: 4
Size: 354519 Color: 1
Size: 268240 Color: 1

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 377413 Color: 3
Size: 332371 Color: 4
Size: 290217 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 377419 Color: 2
Size: 335066 Color: 1
Size: 287516 Color: 4

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 377423 Color: 4
Size: 319400 Color: 0
Size: 303178 Color: 3

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 377472 Color: 0
Size: 361641 Color: 1
Size: 260888 Color: 3

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 377430 Color: 3
Size: 354510 Color: 2
Size: 268061 Color: 4

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 377468 Color: 2
Size: 316240 Color: 1
Size: 306293 Color: 3

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 377518 Color: 3
Size: 365518 Color: 1
Size: 256965 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 377521 Color: 2
Size: 358916 Color: 0
Size: 263564 Color: 4

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 377544 Color: 0
Size: 370891 Color: 3
Size: 251566 Color: 1

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 377444 Color: 1
Size: 342662 Color: 3
Size: 279895 Color: 2

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 377600 Color: 3
Size: 356379 Color: 0
Size: 266022 Color: 4

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 377659 Color: 4
Size: 333673 Color: 0
Size: 288669 Color: 3

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 377669 Color: 2
Size: 318552 Color: 4
Size: 303780 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 377680 Color: 4
Size: 328496 Color: 1
Size: 293825 Color: 3

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 377687 Color: 1
Size: 352449 Color: 1
Size: 269865 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 377720 Color: 2
Size: 311725 Color: 4
Size: 310556 Color: 4

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 377736 Color: 2
Size: 369277 Color: 1
Size: 252988 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 377727 Color: 0
Size: 346478 Color: 3
Size: 275796 Color: 4

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 377760 Color: 4
Size: 329625 Color: 2
Size: 292616 Color: 1

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 377838 Color: 3
Size: 330928 Color: 0
Size: 291235 Color: 4

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 377758 Color: 0
Size: 362321 Color: 1
Size: 259922 Color: 4

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 377860 Color: 3
Size: 338034 Color: 2
Size: 284107 Color: 4

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 377861 Color: 1
Size: 343083 Color: 1
Size: 279057 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 377878 Color: 3
Size: 327693 Color: 1
Size: 294430 Color: 3

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 377879 Color: 0
Size: 339332 Color: 0
Size: 282790 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 377909 Color: 4
Size: 354546 Color: 2
Size: 267546 Color: 4

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 377924 Color: 2
Size: 329269 Color: 1
Size: 292808 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 377910 Color: 1
Size: 334263 Color: 4
Size: 287828 Color: 2

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 377930 Color: 3
Size: 371808 Color: 0
Size: 250263 Color: 4

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 377933 Color: 1
Size: 352793 Color: 0
Size: 269275 Color: 3

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 377938 Color: 1
Size: 351276 Color: 2
Size: 270787 Color: 2

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 377949 Color: 3
Size: 332745 Color: 0
Size: 289307 Color: 4

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 378042 Color: 1
Size: 317762 Color: 0
Size: 304197 Color: 3

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 378093 Color: 4
Size: 320247 Color: 2
Size: 301661 Color: 4

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 378262 Color: 3
Size: 345537 Color: 0
Size: 276202 Color: 1

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 378059 Color: 1
Size: 320332 Color: 2
Size: 301610 Color: 3

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 378272 Color: 3
Size: 357014 Color: 0
Size: 264715 Color: 4

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 378285 Color: 3
Size: 326145 Color: 4
Size: 295571 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 378313 Color: 3
Size: 330295 Color: 3
Size: 291393 Color: 4

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 378434 Color: 0
Size: 319574 Color: 4
Size: 301993 Color: 1

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 378551 Color: 3
Size: 342562 Color: 0
Size: 278888 Color: 4

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 378615 Color: 3
Size: 317404 Color: 4
Size: 303982 Color: 1

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 378637 Color: 4
Size: 332810 Color: 4
Size: 288554 Color: 3

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 378708 Color: 0
Size: 312003 Color: 3
Size: 309290 Color: 1

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 378343 Color: 1
Size: 321508 Color: 4
Size: 300150 Color: 4

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 378730 Color: 4
Size: 319843 Color: 4
Size: 301428 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 378628 Color: 1
Size: 353269 Color: 4
Size: 268104 Color: 3

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 378737 Color: 2
Size: 359715 Color: 0
Size: 261549 Color: 2

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 378805 Color: 0
Size: 322064 Color: 1
Size: 299132 Color: 3

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 378666 Color: 1
Size: 344471 Color: 4
Size: 276864 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 378748 Color: 2
Size: 352583 Color: 0
Size: 268670 Color: 2

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 378776 Color: 1
Size: 367245 Color: 4
Size: 253980 Color: 3

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 378921 Color: 0
Size: 337977 Color: 4
Size: 283103 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 378951 Color: 2
Size: 350445 Color: 3
Size: 270605 Color: 1

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 378977 Color: 2
Size: 346553 Color: 1
Size: 274471 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 378832 Color: 1
Size: 344148 Color: 0
Size: 277021 Color: 4

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 379004 Color: 3
Size: 362322 Color: 0
Size: 258675 Color: 3

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 379000 Color: 1
Size: 323306 Color: 4
Size: 297695 Color: 3

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 379008 Color: 3
Size: 311275 Color: 4
Size: 309718 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 378921 Color: 3
Size: 345334 Color: 2
Size: 275746 Color: 4

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 379115 Color: 3
Size: 315750 Color: 1
Size: 305136 Color: 2

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 379209 Color: 0
Size: 313123 Color: 2
Size: 307669 Color: 1

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 379093 Color: 1
Size: 342713 Color: 3
Size: 278195 Color: 3

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 379211 Color: 3
Size: 340149 Color: 4
Size: 280641 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 379257 Color: 0
Size: 355863 Color: 3
Size: 264881 Color: 3

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 379262 Color: 1
Size: 314617 Color: 4
Size: 306122 Color: 2

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 379310 Color: 3
Size: 355695 Color: 4
Size: 264996 Color: 1

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 379348 Color: 2
Size: 340626 Color: 3
Size: 280027 Color: 1

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 379368 Color: 0
Size: 324316 Color: 4
Size: 296317 Color: 3

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 379398 Color: 4
Size: 350831 Color: 1
Size: 269772 Color: 3

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 379403 Color: 3
Size: 353067 Color: 0
Size: 267531 Color: 4

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 379488 Color: 3
Size: 341499 Color: 1
Size: 279014 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 379492 Color: 3
Size: 346094 Color: 4
Size: 274415 Color: 2

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 379499 Color: 2
Size: 362645 Color: 0
Size: 257857 Color: 1

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 379666 Color: 1
Size: 337119 Color: 3
Size: 283216 Color: 4

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 379557 Color: 0
Size: 355186 Color: 3
Size: 265258 Color: 3

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 379637 Color: 4
Size: 356429 Color: 2
Size: 263935 Color: 1

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 379671 Color: 4
Size: 313085 Color: 4
Size: 307245 Color: 2

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 379683 Color: 3
Size: 349415 Color: 1
Size: 270903 Color: 2

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 379699 Color: 3
Size: 360741 Color: 0
Size: 259561 Color: 1

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 379732 Color: 2
Size: 310153 Color: 0
Size: 310116 Color: 1

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 379671 Color: 1
Size: 348038 Color: 4
Size: 272292 Color: 4

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 379798 Color: 0
Size: 328975 Color: 1
Size: 291228 Color: 3

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 379817 Color: 3
Size: 327354 Color: 4
Size: 292830 Color: 4

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 379829 Color: 2
Size: 349005 Color: 1
Size: 271167 Color: 2

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 380026 Color: 0
Size: 359112 Color: 3
Size: 260863 Color: 4

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 380395 Color: 1
Size: 314033 Color: 4
Size: 305573 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 380423 Color: 1
Size: 325359 Color: 3
Size: 294219 Color: 3

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 380036 Color: 3
Size: 340518 Color: 0
Size: 279447 Color: 4

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 380303 Color: 2
Size: 327815 Color: 4
Size: 291883 Color: 1

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 380417 Color: 0
Size: 364643 Color: 2
Size: 254941 Color: 3

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 380541 Color: 1
Size: 359736 Color: 2
Size: 259724 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 380437 Color: 3
Size: 336906 Color: 3
Size: 282658 Color: 2

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 380452 Color: 0
Size: 315804 Color: 0
Size: 303745 Color: 1

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 380561 Color: 1
Size: 335538 Color: 4
Size: 283902 Color: 2

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 380500 Color: 4
Size: 328928 Color: 3
Size: 290573 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 380503 Color: 4
Size: 319911 Color: 1
Size: 299587 Color: 4

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 380513 Color: 3
Size: 324659 Color: 0
Size: 294829 Color: 3

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 380540 Color: 4
Size: 309819 Color: 1
Size: 309642 Color: 4

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 380610 Color: 3
Size: 341323 Color: 1
Size: 278068 Color: 2

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 380632 Color: 2
Size: 364657 Color: 0
Size: 254712 Color: 3

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 380638 Color: 4
Size: 348853 Color: 4
Size: 270510 Color: 1

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 380657 Color: 1
Size: 316640 Color: 2
Size: 302704 Color: 4

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 380729 Color: 4
Size: 361750 Color: 0
Size: 257522 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 380861 Color: 1
Size: 355219 Color: 3
Size: 263921 Color: 3

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 380851 Color: 4
Size: 353788 Color: 0
Size: 265362 Color: 1

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 380908 Color: 1
Size: 325535 Color: 4
Size: 293558 Color: 4

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 380873 Color: 0
Size: 346751 Color: 4
Size: 272377 Color: 3

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 380886 Color: 0
Size: 333222 Color: 1
Size: 285893 Color: 4

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 381017 Color: 3
Size: 341090 Color: 3
Size: 277894 Color: 4

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 381025 Color: 0
Size: 354520 Color: 3
Size: 264456 Color: 1

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 381056 Color: 1
Size: 342156 Color: 3
Size: 276789 Color: 2

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 381071 Color: 4
Size: 367206 Color: 3
Size: 251724 Color: 2

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 381130 Color: 0
Size: 338470 Color: 2
Size: 280401 Color: 3

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 381145 Color: 0
Size: 352024 Color: 3
Size: 266832 Color: 1

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 381244 Color: 4
Size: 340078 Color: 0
Size: 278679 Color: 4

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 381329 Color: 1
Size: 329582 Color: 4
Size: 289090 Color: 3

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 381330 Color: 1
Size: 348081 Color: 0
Size: 270590 Color: 3

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 381304 Color: 2
Size: 367795 Color: 1
Size: 250902 Color: 2

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 381333 Color: 2
Size: 321834 Color: 0
Size: 296834 Color: 2

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 381358 Color: 4
Size: 338111 Color: 4
Size: 280532 Color: 3

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 381414 Color: 1
Size: 339090 Color: 4
Size: 279497 Color: 3

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 381426 Color: 2
Size: 356573 Color: 2
Size: 262002 Color: 1

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 381539 Color: 1
Size: 342941 Color: 2
Size: 275521 Color: 3

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 381462 Color: 0
Size: 326910 Color: 4
Size: 291629 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 381585 Color: 4
Size: 348691 Color: 0
Size: 269725 Color: 3

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 381593 Color: 4
Size: 339017 Color: 0
Size: 279391 Color: 1

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 381627 Color: 1
Size: 311653 Color: 2
Size: 306721 Color: 2

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 381527 Color: 3
Size: 359680 Color: 2
Size: 258794 Color: 4

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 381676 Color: 1
Size: 340374 Color: 4
Size: 277951 Color: 2

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 381712 Color: 4
Size: 313703 Color: 2
Size: 304586 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 381765 Color: 2
Size: 324557 Color: 2
Size: 293679 Color: 4

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 381834 Color: 0
Size: 313901 Color: 1
Size: 304266 Color: 4

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 381864 Color: 3
Size: 355757 Color: 2
Size: 262380 Color: 2

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 381914 Color: 3
Size: 357994 Color: 1
Size: 260093 Color: 2

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 381972 Color: 2
Size: 338726 Color: 3
Size: 279303 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 382006 Color: 1
Size: 354759 Color: 3
Size: 263236 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 382091 Color: 0
Size: 364376 Color: 1
Size: 253534 Color: 4

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 382209 Color: 0
Size: 333093 Color: 4
Size: 284699 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 382264 Color: 1
Size: 350626 Color: 0
Size: 267111 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 382434 Color: 4
Size: 343380 Color: 0
Size: 274187 Color: 3

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 382461 Color: 0
Size: 367067 Color: 1
Size: 250473 Color: 3

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 382540 Color: 0
Size: 316616 Color: 4
Size: 300845 Color: 3

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 382718 Color: 4
Size: 319589 Color: 0
Size: 297694 Color: 3

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 382740 Color: 3
Size: 365155 Color: 1
Size: 252106 Color: 4

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 382810 Color: 2
Size: 348779 Color: 0
Size: 268412 Color: 4

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 382871 Color: 1
Size: 331639 Color: 4
Size: 285491 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 382874 Color: 2
Size: 322020 Color: 4
Size: 295107 Color: 3

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 382740 Color: 3
Size: 323749 Color: 2
Size: 293512 Color: 2

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 382894 Color: 1
Size: 351971 Color: 0
Size: 265136 Color: 4

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 382893 Color: 3
Size: 344185 Color: 2
Size: 272923 Color: 2

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 382930 Color: 4
Size: 314353 Color: 0
Size: 302718 Color: 2

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 382941 Color: 1
Size: 320642 Color: 1
Size: 296418 Color: 2

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 383090 Color: 1
Size: 315108 Color: 0
Size: 301803 Color: 4

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 383152 Color: 1
Size: 317474 Color: 3
Size: 299375 Color: 2

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 383152 Color: 4
Size: 354446 Color: 0
Size: 262403 Color: 1

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 383192 Color: 2
Size: 329681 Color: 4
Size: 287128 Color: 3

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 383297 Color: 3
Size: 323404 Color: 1
Size: 293300 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 383182 Color: 1
Size: 320424 Color: 3
Size: 296395 Color: 2

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 383325 Color: 3
Size: 338281 Color: 0
Size: 278395 Color: 2

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 383330 Color: 0
Size: 342438 Color: 1
Size: 274233 Color: 4

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 383289 Color: 1
Size: 359012 Color: 4
Size: 257700 Color: 4

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 383372 Color: 3
Size: 332689 Color: 0
Size: 283940 Color: 2

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 383379 Color: 2
Size: 364135 Color: 4
Size: 252487 Color: 4

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 383396 Color: 4
Size: 358249 Color: 0
Size: 258356 Color: 4

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 383411 Color: 0
Size: 356685 Color: 1
Size: 259905 Color: 2

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 383456 Color: 2
Size: 338551 Color: 4
Size: 277994 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 383416 Color: 0
Size: 309869 Color: 1
Size: 306716 Color: 2

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 383480 Color: 2
Size: 317113 Color: 4
Size: 299408 Color: 1

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 383498 Color: 2
Size: 349864 Color: 1
Size: 266639 Color: 4

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 383513 Color: 3
Size: 337461 Color: 0
Size: 279027 Color: 4

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 383555 Color: 3
Size: 309321 Color: 0
Size: 307125 Color: 4

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 383576 Color: 1
Size: 318220 Color: 1
Size: 298205 Color: 2

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 383586 Color: 3
Size: 322006 Color: 2
Size: 294409 Color: 3

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 383628 Color: 4
Size: 342427 Color: 1
Size: 273946 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 383786 Color: 2
Size: 316763 Color: 1
Size: 299452 Color: 1

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 383800 Color: 3
Size: 339309 Color: 0
Size: 276892 Color: 1

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 383899 Color: 0
Size: 361164 Color: 4
Size: 254938 Color: 2

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 383830 Color: 4
Size: 337058 Color: 1
Size: 279113 Color: 4

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 383932 Color: 2
Size: 339537 Color: 4
Size: 276532 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 383949 Color: 1
Size: 360555 Color: 3
Size: 255497 Color: 2

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 383903 Color: 0
Size: 308925 Color: 0
Size: 307173 Color: 1

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 383968 Color: 3
Size: 355279 Color: 4
Size: 260754 Color: 4

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 384006 Color: 4
Size: 361341 Color: 3
Size: 254654 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 384042 Color: 3
Size: 312308 Color: 4
Size: 303651 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 384093 Color: 4
Size: 319351 Color: 0
Size: 296557 Color: 2

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 384086 Color: 0
Size: 333161 Color: 4
Size: 282754 Color: 3

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 384138 Color: 4
Size: 338021 Color: 2
Size: 277842 Color: 3

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 392673 Color: 0
Size: 347545 Color: 1
Size: 259783 Color: 4

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 384199 Color: 0
Size: 343960 Color: 1
Size: 271842 Color: 3

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 384260 Color: 3
Size: 329690 Color: 1
Size: 286051 Color: 1

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 384319 Color: 4
Size: 363734 Color: 3
Size: 251948 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 384441 Color: 4
Size: 309605 Color: 3
Size: 305955 Color: 0

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 384487 Color: 4
Size: 353989 Color: 0
Size: 261525 Color: 3

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 384574 Color: 3
Size: 355839 Color: 2
Size: 259588 Color: 4

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 384638 Color: 2
Size: 318349 Color: 0
Size: 297014 Color: 4

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 384742 Color: 1
Size: 317094 Color: 0
Size: 298165 Color: 2

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 384779 Color: 1
Size: 352829 Color: 0
Size: 262393 Color: 1

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 384801 Color: 4
Size: 343508 Color: 1
Size: 271692 Color: 2

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 384928 Color: 2
Size: 342461 Color: 1
Size: 272612 Color: 2

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 384931 Color: 4
Size: 333786 Color: 3
Size: 281284 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 384859 Color: 1
Size: 350811 Color: 1
Size: 264331 Color: 2

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 384976 Color: 3
Size: 326826 Color: 2
Size: 288199 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 385011 Color: 3
Size: 332517 Color: 3
Size: 282473 Color: 2

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 385105 Color: 1
Size: 308875 Color: 2
Size: 306021 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 385157 Color: 3
Size: 360167 Color: 3
Size: 254677 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 385177 Color: 3
Size: 325442 Color: 2
Size: 289382 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 385222 Color: 0
Size: 362630 Color: 2
Size: 252149 Color: 1

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 385188 Color: 2
Size: 335790 Color: 3
Size: 279023 Color: 2

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 385341 Color: 2
Size: 310772 Color: 4
Size: 303888 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 385316 Color: 0
Size: 332781 Color: 1
Size: 281904 Color: 1

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 385520 Color: 2
Size: 328591 Color: 1
Size: 285890 Color: 1

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 385529 Color: 4
Size: 324701 Color: 0
Size: 289771 Color: 2

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 385644 Color: 2
Size: 357400 Color: 4
Size: 256957 Color: 4

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 385670 Color: 0
Size: 310384 Color: 1
Size: 303947 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 385688 Color: 3
Size: 363898 Color: 3
Size: 250415 Color: 2

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 385689 Color: 4
Size: 338663 Color: 3
Size: 275649 Color: 1

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 385706 Color: 4
Size: 347272 Color: 1
Size: 267023 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 385581 Color: 1
Size: 358745 Color: 2
Size: 255675 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 385721 Color: 2
Size: 311435 Color: 0
Size: 302845 Color: 4

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 385606 Color: 1
Size: 361984 Color: 4
Size: 252411 Color: 2

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 385754 Color: 2
Size: 323531 Color: 0
Size: 290716 Color: 3

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 385756 Color: 3
Size: 347039 Color: 2
Size: 267206 Color: 2

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 385761 Color: 2
Size: 344355 Color: 1
Size: 269885 Color: 3

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 385809 Color: 0
Size: 350680 Color: 1
Size: 263512 Color: 4

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 385774 Color: 3
Size: 347014 Color: 0
Size: 267213 Color: 2

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 385916 Color: 0
Size: 314806 Color: 3
Size: 299279 Color: 4

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 385906 Color: 1
Size: 334678 Color: 4
Size: 279417 Color: 3

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 386019 Color: 4
Size: 321267 Color: 3
Size: 292715 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 386026 Color: 4
Size: 309551 Color: 3
Size: 304424 Color: 3

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 386129 Color: 4
Size: 312388 Color: 1
Size: 301484 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 386156 Color: 1
Size: 318464 Color: 1
Size: 295381 Color: 4

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 386139 Color: 3
Size: 346904 Color: 2
Size: 266958 Color: 2

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 386208 Color: 3
Size: 341907 Color: 1
Size: 271886 Color: 2

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 386265 Color: 4
Size: 322850 Color: 1
Size: 290886 Color: 2

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 386292 Color: 4
Size: 315056 Color: 3
Size: 298653 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 386408 Color: 0
Size: 361887 Color: 1
Size: 251706 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 386344 Color: 1
Size: 357181 Color: 3
Size: 256476 Color: 4

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 386409 Color: 0
Size: 309712 Color: 2
Size: 303880 Color: 2

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 386431 Color: 0
Size: 348856 Color: 0
Size: 264714 Color: 1

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 386349 Color: 4
Size: 335242 Color: 3
Size: 278410 Color: 4

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 386731 Color: 3
Size: 353811 Color: 2
Size: 259459 Color: 2

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 386748 Color: 2
Size: 328661 Color: 1
Size: 284592 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 386720 Color: 1
Size: 353465 Color: 4
Size: 259816 Color: 3

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 386792 Color: 4
Size: 316158 Color: 0
Size: 297051 Color: 4

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 386801 Color: 0
Size: 315879 Color: 1
Size: 297321 Color: 3

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 386849 Color: 3
Size: 360147 Color: 2
Size: 253005 Color: 4

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 386867 Color: 3
Size: 314404 Color: 0
Size: 298730 Color: 1

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 386818 Color: 0
Size: 311031 Color: 4
Size: 302152 Color: 4

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 386889 Color: 3
Size: 336862 Color: 1
Size: 276250 Color: 3

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 386891 Color: 4
Size: 342437 Color: 0
Size: 270673 Color: 1

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 386893 Color: 3
Size: 332107 Color: 0
Size: 281001 Color: 2

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 386939 Color: 3
Size: 318607 Color: 4
Size: 294455 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 386838 Color: 1
Size: 309199 Color: 3
Size: 303964 Color: 4

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 386986 Color: 2
Size: 350671 Color: 0
Size: 262344 Color: 3

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 387072 Color: 2
Size: 352026 Color: 4
Size: 260903 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 387103 Color: 0
Size: 327445 Color: 1
Size: 285453 Color: 2

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 387145 Color: 4
Size: 315144 Color: 2
Size: 297712 Color: 3

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 387155 Color: 2
Size: 345597 Color: 3
Size: 267249 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 387134 Color: 0
Size: 338538 Color: 1
Size: 274329 Color: 2

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 387214 Color: 2
Size: 322033 Color: 1
Size: 290754 Color: 3

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 387238 Color: 4
Size: 359481 Color: 0
Size: 253282 Color: 1

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 387264 Color: 3
Size: 327968 Color: 0
Size: 284769 Color: 2

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 387216 Color: 0
Size: 309706 Color: 1
Size: 303079 Color: 2

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 387379 Color: 2
Size: 354904 Color: 2
Size: 257718 Color: 3

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 387420 Color: 0
Size: 353086 Color: 1
Size: 259495 Color: 4

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 387514 Color: 4
Size: 335143 Color: 0
Size: 277344 Color: 3

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 387553 Color: 0
Size: 352270 Color: 1
Size: 260178 Color: 2

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 387335 Color: 1
Size: 310446 Color: 2
Size: 302220 Color: 4

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 387582 Color: 0
Size: 350213 Color: 2
Size: 262206 Color: 3

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 387621 Color: 1
Size: 309614 Color: 3
Size: 302766 Color: 4

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 387689 Color: 0
Size: 321686 Color: 4
Size: 290626 Color: 1

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 387818 Color: 4
Size: 350432 Color: 1
Size: 261751 Color: 4

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 387823 Color: 4
Size: 357946 Color: 0
Size: 254232 Color: 2

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 387789 Color: 0
Size: 329406 Color: 4
Size: 282806 Color: 3

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 356911 Color: 1
Size: 350348 Color: 0
Size: 292742 Color: 4

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 387861 Color: 3
Size: 311357 Color: 0
Size: 300783 Color: 4

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 388096 Color: 4
Size: 318670 Color: 2
Size: 293235 Color: 3

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 388138 Color: 4
Size: 308524 Color: 3
Size: 303339 Color: 3

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 388159 Color: 2
Size: 307934 Color: 3
Size: 303908 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 388193 Color: 3
Size: 321457 Color: 3
Size: 290351 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 388231 Color: 3
Size: 352466 Color: 1
Size: 259304 Color: 2

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 388378 Color: 0
Size: 345519 Color: 4
Size: 266104 Color: 3

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 388278 Color: 4
Size: 323379 Color: 2
Size: 288344 Color: 1

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 388472 Color: 4
Size: 329170 Color: 0
Size: 282359 Color: 3

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 388484 Color: 3
Size: 319825 Color: 1
Size: 291692 Color: 1

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 388733 Color: 0
Size: 352710 Color: 1
Size: 258558 Color: 4

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 388594 Color: 1
Size: 347043 Color: 4
Size: 264364 Color: 1

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 388719 Color: 4
Size: 321037 Color: 3
Size: 290245 Color: 2

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 388732 Color: 1
Size: 341495 Color: 0
Size: 269774 Color: 2

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 388786 Color: 0
Size: 359050 Color: 1
Size: 252165 Color: 3

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 388828 Color: 3
Size: 322391 Color: 0
Size: 288782 Color: 3

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 388865 Color: 3
Size: 323167 Color: 0
Size: 287969 Color: 4

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 389018 Color: 0
Size: 343520 Color: 2
Size: 267463 Color: 3

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 388946 Color: 4
Size: 311949 Color: 4
Size: 299106 Color: 1

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 389050 Color: 0
Size: 331792 Color: 4
Size: 279159 Color: 3

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 388975 Color: 1
Size: 306710 Color: 3
Size: 304316 Color: 3

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 388986 Color: 3
Size: 344849 Color: 0
Size: 266166 Color: 4

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 389006 Color: 4
Size: 318762 Color: 3
Size: 292233 Color: 4

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 389048 Color: 4
Size: 306165 Color: 0
Size: 304788 Color: 3

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 389242 Color: 0
Size: 324524 Color: 4
Size: 286235 Color: 1

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 389309 Color: 0
Size: 308233 Color: 2
Size: 302459 Color: 1

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 389320 Color: 2
Size: 360481 Color: 3
Size: 250200 Color: 3

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 389417 Color: 0
Size: 351629 Color: 1
Size: 258955 Color: 3

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 389362 Color: 3
Size: 324599 Color: 4
Size: 286040 Color: 3

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 389409 Color: 2
Size: 311312 Color: 4
Size: 299280 Color: 3

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 389507 Color: 3
Size: 356148 Color: 1
Size: 254346 Color: 3

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 389536 Color: 1
Size: 307636 Color: 1
Size: 302829 Color: 0

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 389544 Color: 0
Size: 308347 Color: 3
Size: 302110 Color: 4

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 389565 Color: 4
Size: 349636 Color: 3
Size: 260800 Color: 2

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 389604 Color: 1
Size: 336524 Color: 3
Size: 273873 Color: 2

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 389728 Color: 4
Size: 306414 Color: 0
Size: 303859 Color: 1

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 389794 Color: 3
Size: 354622 Color: 4
Size: 255585 Color: 2

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 389829 Color: 0
Size: 336491 Color: 3
Size: 273681 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 389907 Color: 0
Size: 314246 Color: 0
Size: 295848 Color: 3

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 389888 Color: 3
Size: 344826 Color: 2
Size: 265287 Color: 4

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 389913 Color: 3
Size: 338127 Color: 4
Size: 271961 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 390053 Color: 2
Size: 332930 Color: 4
Size: 277018 Color: 3

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 390067 Color: 4
Size: 307727 Color: 0
Size: 302207 Color: 2

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 390082 Color: 3
Size: 342991 Color: 4
Size: 266928 Color: 1

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 390088 Color: 3
Size: 325328 Color: 4
Size: 284585 Color: 4

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 390175 Color: 2
Size: 328431 Color: 0
Size: 281395 Color: 4

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 390186 Color: 4
Size: 312579 Color: 2
Size: 297236 Color: 2

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 390199 Color: 4
Size: 349013 Color: 2
Size: 260789 Color: 4

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 390241 Color: 2
Size: 312790 Color: 1
Size: 296970 Color: 4

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 390287 Color: 3
Size: 345924 Color: 0
Size: 263790 Color: 1

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 390429 Color: 4
Size: 307729 Color: 0
Size: 301843 Color: 1

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 390437 Color: 2
Size: 323799 Color: 2
Size: 285765 Color: 4

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 390550 Color: 2
Size: 314500 Color: 3
Size: 294951 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 390677 Color: 3
Size: 348385 Color: 4
Size: 260939 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 390687 Color: 2
Size: 338883 Color: 3
Size: 270431 Color: 4

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 390754 Color: 3
Size: 345088 Color: 0
Size: 264159 Color: 2

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 390787 Color: 2
Size: 335823 Color: 0
Size: 273391 Color: 2

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 390826 Color: 2
Size: 328951 Color: 4
Size: 280224 Color: 2

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 390864 Color: 4
Size: 315771 Color: 1
Size: 293366 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 390868 Color: 1
Size: 334657 Color: 3
Size: 274476 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 390947 Color: 3
Size: 349561 Color: 0
Size: 259493 Color: 1

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 391002 Color: 3
Size: 341757 Color: 1
Size: 267242 Color: 3

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 390923 Color: 0
Size: 317087 Color: 1
Size: 291991 Color: 2

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 391030 Color: 4
Size: 329666 Color: 3
Size: 279305 Color: 1

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 391118 Color: 4
Size: 316512 Color: 2
Size: 292371 Color: 3

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 391257 Color: 0
Size: 346695 Color: 3
Size: 262049 Color: 4

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 391418 Color: 4
Size: 340785 Color: 4
Size: 267798 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 391459 Color: 2
Size: 315734 Color: 3
Size: 292808 Color: 1

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 391525 Color: 4
Size: 305312 Color: 0
Size: 303164 Color: 3

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 391528 Color: 2
Size: 336111 Color: 0
Size: 272362 Color: 1

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 391534 Color: 1
Size: 346870 Color: 2
Size: 261597 Color: 4

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 391544 Color: 0
Size: 356129 Color: 3
Size: 252328 Color: 3

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 391607 Color: 1
Size: 326747 Color: 1
Size: 281647 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 391609 Color: 1
Size: 350031 Color: 4
Size: 258361 Color: 1

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 391694 Color: 1
Size: 355953 Color: 1
Size: 252354 Color: 3

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 391714 Color: 1
Size: 311770 Color: 0
Size: 296517 Color: 2

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 391671 Color: 3
Size: 349088 Color: 4
Size: 259242 Color: 4

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 391776 Color: 2
Size: 351902 Color: 4
Size: 256323 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 391882 Color: 1
Size: 313267 Color: 4
Size: 294852 Color: 1

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 391913 Color: 3
Size: 346161 Color: 0
Size: 261927 Color: 1

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 391916 Color: 3
Size: 328699 Color: 4
Size: 279386 Color: 1

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 391930 Color: 3
Size: 325921 Color: 0
Size: 282150 Color: 2

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 391983 Color: 1
Size: 330729 Color: 4
Size: 277289 Color: 3

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 392008 Color: 2
Size: 344842 Color: 3
Size: 263151 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 391976 Color: 3
Size: 328532 Color: 2
Size: 279493 Color: 2

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 392029 Color: 2
Size: 305268 Color: 0
Size: 302704 Color: 2

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 392189 Color: 0
Size: 326069 Color: 1
Size: 281743 Color: 2

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 392158 Color: 1
Size: 345481 Color: 3
Size: 262362 Color: 1

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 392223 Color: 2
Size: 316206 Color: 3
Size: 291572 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 392329 Color: 3
Size: 333873 Color: 4
Size: 273799 Color: 4

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 392350 Color: 2
Size: 313287 Color: 1
Size: 294364 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 392378 Color: 3
Size: 326007 Color: 1
Size: 281616 Color: 4

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 392403 Color: 4
Size: 337470 Color: 2
Size: 270128 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 392497 Color: 4
Size: 329553 Color: 0
Size: 277951 Color: 4

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 392547 Color: 1
Size: 344825 Color: 2
Size: 262629 Color: 2

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 392902 Color: 0
Size: 348076 Color: 4
Size: 259023 Color: 4

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 392735 Color: 2
Size: 335745 Color: 4
Size: 271521 Color: 1

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 392858 Color: 2
Size: 336107 Color: 1
Size: 271036 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 392973 Color: 0
Size: 322378 Color: 4
Size: 284650 Color: 2

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 392955 Color: 3
Size: 349157 Color: 3
Size: 257889 Color: 2

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 393032 Color: 3
Size: 315282 Color: 1
Size: 291687 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 393039 Color: 4
Size: 341999 Color: 2
Size: 264963 Color: 2

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 393061 Color: 0
Size: 339795 Color: 1
Size: 267145 Color: 3

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 393104 Color: 3
Size: 336772 Color: 1
Size: 270125 Color: 4

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 393235 Color: 0
Size: 325173 Color: 2
Size: 281593 Color: 2

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 393195 Color: 3
Size: 327439 Color: 3
Size: 279367 Color: 1

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 393204 Color: 3
Size: 303970 Color: 1
Size: 302827 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 393337 Color: 0
Size: 347661 Color: 4
Size: 259003 Color: 1

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 393209 Color: 2
Size: 317390 Color: 3
Size: 289402 Color: 1

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 393239 Color: 2
Size: 323431 Color: 2
Size: 283331 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 393351 Color: 0
Size: 352394 Color: 2
Size: 254256 Color: 4

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 393384 Color: 4
Size: 314748 Color: 1
Size: 291869 Color: 3

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 393404 Color: 4
Size: 323255 Color: 2
Size: 283342 Color: 4

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 393601 Color: 4
Size: 314494 Color: 1
Size: 291906 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 393618 Color: 2
Size: 321454 Color: 1
Size: 284929 Color: 3

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 393657 Color: 3
Size: 342701 Color: 0
Size: 263643 Color: 3

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 393717 Color: 4
Size: 347447 Color: 2
Size: 258837 Color: 1

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 393796 Color: 3
Size: 324464 Color: 0
Size: 281741 Color: 3

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 393799 Color: 4
Size: 324205 Color: 1
Size: 281997 Color: 4

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 393899 Color: 4
Size: 342231 Color: 2
Size: 263871 Color: 1

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 393924 Color: 2
Size: 353308 Color: 4
Size: 252769 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 394025 Color: 0
Size: 325335 Color: 3
Size: 280641 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 393969 Color: 2
Size: 320114 Color: 1
Size: 285918 Color: 1

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 394045 Color: 0
Size: 322298 Color: 1
Size: 283658 Color: 4

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 394019 Color: 2
Size: 345708 Color: 1
Size: 260274 Color: 2

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 394045 Color: 4
Size: 305440 Color: 1
Size: 300516 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 394173 Color: 0
Size: 354544 Color: 4
Size: 251284 Color: 4

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 394146 Color: 4
Size: 307327 Color: 1
Size: 298528 Color: 1

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 394204 Color: 0
Size: 309057 Color: 1
Size: 296740 Color: 4

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 394294 Color: 4
Size: 340299 Color: 2
Size: 265408 Color: 2

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 394310 Color: 3
Size: 305058 Color: 1
Size: 300633 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 394349 Color: 2
Size: 353303 Color: 1
Size: 252349 Color: 3

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 394371 Color: 4
Size: 321920 Color: 1
Size: 283710 Color: 0

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 394571 Color: 0
Size: 309458 Color: 4
Size: 295972 Color: 1

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 394582 Color: 0
Size: 353739 Color: 1
Size: 251680 Color: 3

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 394554 Color: 4
Size: 341675 Color: 1
Size: 263772 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 394682 Color: 3
Size: 304263 Color: 3
Size: 301056 Color: 4

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 394921 Color: 0
Size: 349051 Color: 1
Size: 256029 Color: 4

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 394919 Color: 3
Size: 338327 Color: 0
Size: 266755 Color: 2

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 394958 Color: 0
Size: 336118 Color: 1
Size: 268925 Color: 1

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 395013 Color: 2
Size: 336298 Color: 2
Size: 268690 Color: 1

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 395054 Color: 4
Size: 320370 Color: 2
Size: 284577 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 395208 Color: 4
Size: 312538 Color: 2
Size: 292255 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 395328 Color: 3
Size: 328182 Color: 2
Size: 276491 Color: 2

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 395359 Color: 1
Size: 332298 Color: 0
Size: 272344 Color: 4

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 395389 Color: 4
Size: 354430 Color: 1
Size: 250182 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 395393 Color: 2
Size: 325330 Color: 4
Size: 279278 Color: 4

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 395408 Color: 1
Size: 323042 Color: 1
Size: 281551 Color: 2

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 395492 Color: 3
Size: 333630 Color: 0
Size: 270879 Color: 3

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 395506 Color: 3
Size: 352273 Color: 4
Size: 252222 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 395558 Color: 3
Size: 304545 Color: 2
Size: 299898 Color: 2

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 395573 Color: 1
Size: 313540 Color: 3
Size: 290888 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 395646 Color: 3
Size: 327634 Color: 1
Size: 276721 Color: 1

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 395731 Color: 0
Size: 319205 Color: 3
Size: 285065 Color: 1

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 395713 Color: 3
Size: 302354 Color: 1
Size: 301934 Color: 4

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 395768 Color: 2
Size: 339705 Color: 3
Size: 264528 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 395859 Color: 0
Size: 337480 Color: 3
Size: 266662 Color: 4

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 395835 Color: 4
Size: 321544 Color: 2
Size: 282622 Color: 1

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 396055 Color: 0
Size: 344935 Color: 3
Size: 259011 Color: 2

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 395880 Color: 4
Size: 303381 Color: 2
Size: 300740 Color: 2

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 395926 Color: 1
Size: 328775 Color: 3
Size: 275300 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 396110 Color: 0
Size: 349543 Color: 2
Size: 254348 Color: 2

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 396101 Color: 3
Size: 325469 Color: 3
Size: 278431 Color: 2

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 396136 Color: 4
Size: 314369 Color: 3
Size: 289496 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 396225 Color: 2
Size: 350648 Color: 1
Size: 253128 Color: 4

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 396254 Color: 4
Size: 327477 Color: 2
Size: 276270 Color: 1

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 396274 Color: 3
Size: 342135 Color: 0
Size: 261592 Color: 1

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 396395 Color: 0
Size: 317897 Color: 2
Size: 285709 Color: 2

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 396336 Color: 1
Size: 349912 Color: 3
Size: 253753 Color: 2

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 396349 Color: 1
Size: 317315 Color: 0
Size: 286337 Color: 4

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 396368 Color: 4
Size: 349223 Color: 1
Size: 254410 Color: 4

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 396407 Color: 3
Size: 330097 Color: 0
Size: 273497 Color: 1

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 396540 Color: 4
Size: 319152 Color: 1
Size: 284309 Color: 4

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 396507 Color: 0
Size: 352081 Color: 2
Size: 251413 Color: 4

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 396573 Color: 1
Size: 307351 Color: 1
Size: 296077 Color: 2

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 396595 Color: 2
Size: 310370 Color: 1
Size: 293036 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 396665 Color: 1
Size: 323852 Color: 3
Size: 279484 Color: 3

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 396857 Color: 3
Size: 313389 Color: 2
Size: 289755 Color: 2

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 397056 Color: 0
Size: 302010 Color: 4
Size: 300935 Color: 1

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 396917 Color: 1
Size: 308038 Color: 4
Size: 295046 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 397057 Color: 0
Size: 313717 Color: 2
Size: 289227 Color: 4

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 396959 Color: 2
Size: 301823 Color: 1
Size: 301219 Color: 4

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 397026 Color: 3
Size: 335263 Color: 4
Size: 267712 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 397062 Color: 0
Size: 302027 Color: 1
Size: 300912 Color: 4

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 397061 Color: 3
Size: 349712 Color: 2
Size: 253228 Color: 4

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 397147 Color: 2
Size: 347925 Color: 0
Size: 254929 Color: 4

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 397169 Color: 4
Size: 312390 Color: 2
Size: 290442 Color: 3

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 397209 Color: 0
Size: 330072 Color: 3
Size: 272720 Color: 1

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 397295 Color: 2
Size: 351006 Color: 4
Size: 251700 Color: 1

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 397303 Color: 3
Size: 345712 Color: 4
Size: 256986 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 397310 Color: 3
Size: 328047 Color: 2
Size: 274644 Color: 4

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 397426 Color: 0
Size: 346694 Color: 2
Size: 255881 Color: 3

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 397392 Color: 3
Size: 347007 Color: 1
Size: 255602 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 397593 Color: 0
Size: 350256 Color: 2
Size: 252152 Color: 3

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 397612 Color: 1
Size: 309313 Color: 2
Size: 293076 Color: 4

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 397655 Color: 4
Size: 302701 Color: 1
Size: 299645 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 397674 Color: 3
Size: 304332 Color: 1
Size: 297995 Color: 4

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 397816 Color: 2
Size: 311568 Color: 1
Size: 290617 Color: 4

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 397847 Color: 3
Size: 321816 Color: 0
Size: 280338 Color: 3

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 397926 Color: 4
Size: 311906 Color: 1
Size: 290169 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 397942 Color: 1
Size: 304803 Color: 4
Size: 297256 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 398063 Color: 4
Size: 351779 Color: 2
Size: 250159 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 398055 Color: 0
Size: 346515 Color: 1
Size: 255431 Color: 1

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 398094 Color: 4
Size: 318952 Color: 1
Size: 282955 Color: 3

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 398119 Color: 3
Size: 347784 Color: 2
Size: 254098 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 398207 Color: 3
Size: 310630 Color: 2
Size: 291164 Color: 1

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 398245 Color: 3
Size: 336078 Color: 0
Size: 265678 Color: 2

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 398279 Color: 3
Size: 329751 Color: 2
Size: 271971 Color: 4

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 398281 Color: 2
Size: 318590 Color: 0
Size: 283130 Color: 3

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 398293 Color: 3
Size: 318815 Color: 4
Size: 282893 Color: 4

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 398356 Color: 1
Size: 332864 Color: 0
Size: 268781 Color: 4

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 398414 Color: 1
Size: 330597 Color: 4
Size: 270990 Color: 2

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 398429 Color: 2
Size: 340229 Color: 0
Size: 261343 Color: 4

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 398514 Color: 0
Size: 311757 Color: 4
Size: 289730 Color: 2

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 398462 Color: 4
Size: 308801 Color: 4
Size: 292738 Color: 3

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 398638 Color: 4
Size: 310181 Color: 3
Size: 291182 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 398691 Color: 3
Size: 334907 Color: 1
Size: 266403 Color: 1

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 398722 Color: 4
Size: 335402 Color: 1
Size: 265877 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 398773 Color: 1
Size: 313476 Color: 1
Size: 287752 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 398802 Color: 2
Size: 332151 Color: 1
Size: 269048 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 398809 Color: 1
Size: 309146 Color: 1
Size: 292046 Color: 4

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 398832 Color: 2
Size: 304093 Color: 0
Size: 297076 Color: 3

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 398921 Color: 2
Size: 338224 Color: 2
Size: 262856 Color: 4

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 398987 Color: 2
Size: 350965 Color: 0
Size: 250049 Color: 1

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 399006 Color: 3
Size: 334168 Color: 0
Size: 266827 Color: 2

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 399085 Color: 0
Size: 343919 Color: 1
Size: 256997 Color: 4

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 399056 Color: 1
Size: 330377 Color: 3
Size: 270568 Color: 1

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 399072 Color: 1
Size: 300872 Color: 0
Size: 300057 Color: 4

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 399157 Color: 0
Size: 310950 Color: 3
Size: 289894 Color: 2

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 399100 Color: 3
Size: 310743 Color: 2
Size: 290158 Color: 4

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 399283 Color: 0
Size: 304456 Color: 4
Size: 296262 Color: 1

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 399321 Color: 0
Size: 329302 Color: 1
Size: 271378 Color: 3

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 399172 Color: 1
Size: 337576 Color: 2
Size: 263253 Color: 2

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 399181 Color: 4
Size: 318340 Color: 3
Size: 282480 Color: 3

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 399534 Color: 0
Size: 332167 Color: 1
Size: 268300 Color: 1

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 399378 Color: 1
Size: 323781 Color: 2
Size: 276842 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 399592 Color: 0
Size: 341934 Color: 4
Size: 258475 Color: 3

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 399511 Color: 1
Size: 324918 Color: 4
Size: 275572 Color: 4

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 399521 Color: 2
Size: 323248 Color: 3
Size: 277232 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 399595 Color: 0
Size: 328298 Color: 3
Size: 272108 Color: 2

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 399558 Color: 1
Size: 344648 Color: 2
Size: 255795 Color: 4

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 399587 Color: 4
Size: 321106 Color: 0
Size: 279308 Color: 4

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 399621 Color: 1
Size: 301403 Color: 2
Size: 298977 Color: 2

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 399631 Color: 2
Size: 314168 Color: 1
Size: 286202 Color: 0

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 399681 Color: 2
Size: 340325 Color: 2
Size: 259995 Color: 4

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 399757 Color: 3
Size: 345372 Color: 0
Size: 254872 Color: 4

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 399779 Color: 3
Size: 343087 Color: 4
Size: 257135 Color: 4

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 399790 Color: 3
Size: 321091 Color: 1
Size: 279120 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 399830 Color: 1
Size: 342915 Color: 2
Size: 257256 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 399837 Color: 3
Size: 318265 Color: 1
Size: 281899 Color: 1

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 399858 Color: 3
Size: 342117 Color: 0
Size: 258026 Color: 1

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 399983 Color: 3
Size: 321772 Color: 4
Size: 278246 Color: 2

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 400017 Color: 4
Size: 300542 Color: 1
Size: 299442 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 400028 Color: 4
Size: 314800 Color: 3
Size: 285173 Color: 4

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 400031 Color: 3
Size: 337764 Color: 0
Size: 262206 Color: 2

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 400043 Color: 3
Size: 341280 Color: 4
Size: 258678 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 400064 Color: 2
Size: 300263 Color: 4
Size: 299674 Color: 3

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 400078 Color: 1
Size: 347025 Color: 4
Size: 252898 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 400178 Color: 1
Size: 300026 Color: 1
Size: 299797 Color: 4

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 400209 Color: 3
Size: 331185 Color: 2
Size: 268607 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 400249 Color: 1
Size: 301013 Color: 2
Size: 298739 Color: 3

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 400259 Color: 1
Size: 301822 Color: 0
Size: 297920 Color: 2

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 400311 Color: 2
Size: 305569 Color: 0
Size: 294121 Color: 1

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 400372 Color: 3
Size: 319305 Color: 1
Size: 280324 Color: 1

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 400415 Color: 3
Size: 310909 Color: 2
Size: 288677 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 400440 Color: 2
Size: 309575 Color: 1
Size: 289986 Color: 3

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 400443 Color: 3
Size: 345919 Color: 0
Size: 253639 Color: 2

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 400472 Color: 3
Size: 309470 Color: 4
Size: 290059 Color: 3

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 400608 Color: 2
Size: 317361 Color: 1
Size: 282032 Color: 3

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 400668 Color: 3
Size: 322288 Color: 3
Size: 277045 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 400673 Color: 1
Size: 341057 Color: 3
Size: 258271 Color: 4

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 400831 Color: 3
Size: 319157 Color: 2
Size: 280013 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 400842 Color: 1
Size: 340984 Color: 3
Size: 258175 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 400874 Color: 3
Size: 321293 Color: 3
Size: 277834 Color: 4

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 400967 Color: 0
Size: 316921 Color: 2
Size: 282113 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 400920 Color: 3
Size: 330502 Color: 2
Size: 268579 Color: 4

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 401040 Color: 0
Size: 344744 Color: 3
Size: 254217 Color: 1

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 401020 Color: 2
Size: 332736 Color: 3
Size: 266245 Color: 1

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 401248 Color: 0
Size: 326028 Color: 2
Size: 272725 Color: 1

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 401251 Color: 4
Size: 306193 Color: 4
Size: 292557 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 401251 Color: 2
Size: 332808 Color: 3
Size: 265942 Color: 4

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 401301 Color: 2
Size: 300527 Color: 4
Size: 298173 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 401289 Color: 0
Size: 323009 Color: 4
Size: 275703 Color: 2

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 401308 Color: 4
Size: 333517 Color: 4
Size: 265176 Color: 3

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 401394 Color: 4
Size: 328403 Color: 2
Size: 270204 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 401557 Color: 1
Size: 346467 Color: 4
Size: 251977 Color: 1

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 401568 Color: 2
Size: 313633 Color: 1
Size: 284800 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 401610 Color: 1
Size: 305750 Color: 3
Size: 292641 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 401625 Color: 3
Size: 346134 Color: 0
Size: 252242 Color: 4

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 401625 Color: 1
Size: 331759 Color: 3
Size: 266617 Color: 2

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 401695 Color: 1
Size: 313276 Color: 1
Size: 285030 Color: 4

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 401757 Color: 1
Size: 341766 Color: 0
Size: 256478 Color: 1

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 401790 Color: 1
Size: 319172 Color: 2
Size: 279039 Color: 2

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 401808 Color: 1
Size: 300914 Color: 2
Size: 297279 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 401868 Color: 1
Size: 308952 Color: 3
Size: 289181 Color: 4

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 401876 Color: 2
Size: 343872 Color: 2
Size: 254253 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 401942 Color: 1
Size: 309640 Color: 1
Size: 288419 Color: 4

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 402103 Color: 3
Size: 310072 Color: 3
Size: 287826 Color: 2

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 402109 Color: 4
Size: 342102 Color: 0
Size: 255790 Color: 1

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 402121 Color: 2
Size: 311532 Color: 1
Size: 286348 Color: 4

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 402174 Color: 4
Size: 335873 Color: 2
Size: 261954 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 402215 Color: 2
Size: 322764 Color: 1
Size: 275022 Color: 4

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 402256 Color: 4
Size: 345085 Color: 2
Size: 252660 Color: 4

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 402337 Color: 3
Size: 334715 Color: 2
Size: 262949 Color: 2

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 402664 Color: 0
Size: 303089 Color: 3
Size: 294248 Color: 3

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 402478 Color: 2
Size: 305827 Color: 1
Size: 291696 Color: 3

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 402574 Color: 2
Size: 336850 Color: 3
Size: 260577 Color: 4

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 402686 Color: 3
Size: 332993 Color: 3
Size: 264322 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 402856 Color: 0
Size: 301939 Color: 3
Size: 295206 Color: 1

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 402916 Color: 1
Size: 340315 Color: 0
Size: 256770 Color: 2

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 402968 Color: 4
Size: 343418 Color: 2
Size: 253615 Color: 3

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 403002 Color: 4
Size: 306890 Color: 0
Size: 290109 Color: 2

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 403114 Color: 3
Size: 312577 Color: 1
Size: 284310 Color: 1

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 403134 Color: 1
Size: 303558 Color: 0
Size: 293309 Color: 1

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 403188 Color: 0
Size: 311300 Color: 3
Size: 285513 Color: 1

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 403151 Color: 4
Size: 302855 Color: 1
Size: 293995 Color: 1

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 403187 Color: 4
Size: 298662 Color: 2
Size: 298152 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 403258 Color: 3
Size: 335550 Color: 2
Size: 261193 Color: 1

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 403360 Color: 3
Size: 345554 Color: 2
Size: 251087 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 403410 Color: 0
Size: 313568 Color: 1
Size: 283023 Color: 2

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 403377 Color: 3
Size: 328565 Color: 1
Size: 268059 Color: 4

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 403477 Color: 0
Size: 342211 Color: 1
Size: 254313 Color: 2

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 403382 Color: 3
Size: 329387 Color: 1
Size: 267232 Color: 2

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 403486 Color: 2
Size: 332484 Color: 4
Size: 264031 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 403592 Color: 3
Size: 316410 Color: 2
Size: 279999 Color: 2

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 403630 Color: 4
Size: 330407 Color: 3
Size: 265964 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 403691 Color: 2
Size: 338313 Color: 3
Size: 257997 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 403718 Color: 1
Size: 318708 Color: 4
Size: 277575 Color: 4

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 403844 Color: 1
Size: 318029 Color: 4
Size: 278128 Color: 3

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 403856 Color: 4
Size: 333522 Color: 2
Size: 262623 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 403860 Color: 0
Size: 304556 Color: 2
Size: 291585 Color: 2

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 403868 Color: 1
Size: 302890 Color: 3
Size: 293243 Color: 2

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 403975 Color: 1
Size: 302298 Color: 2
Size: 293728 Color: 1

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 403975 Color: 0
Size: 335546 Color: 3
Size: 260480 Color: 1

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 404028 Color: 1
Size: 315655 Color: 0
Size: 280318 Color: 2

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 404030 Color: 3
Size: 319759 Color: 4
Size: 276212 Color: 3

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 404032 Color: 2
Size: 323155 Color: 4
Size: 272814 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 404168 Color: 0
Size: 341490 Color: 3
Size: 254343 Color: 1

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 404158 Color: 3
Size: 319868 Color: 3
Size: 275975 Color: 1

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 404377 Color: 0
Size: 314046 Color: 2
Size: 281578 Color: 2

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 404203 Color: 3
Size: 336365 Color: 2
Size: 259433 Color: 1

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 404351 Color: 4
Size: 301196 Color: 4
Size: 294454 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 404382 Color: 4
Size: 332171 Color: 4
Size: 263448 Color: 2

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 404385 Color: 1
Size: 317335 Color: 1
Size: 278281 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 404458 Color: 0
Size: 326056 Color: 2
Size: 269487 Color: 1

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 404400 Color: 2
Size: 315692 Color: 3
Size: 279909 Color: 2

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 404526 Color: 1
Size: 325644 Color: 0
Size: 269831 Color: 3

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 0
Size: 298727 Color: 1
Size: 296761 Color: 3

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 404628 Color: 2
Size: 344500 Color: 3
Size: 250873 Color: 4

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 404653 Color: 1
Size: 308600 Color: 4
Size: 286748 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 404745 Color: 4
Size: 337510 Color: 4
Size: 257746 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 404768 Color: 2
Size: 324101 Color: 4
Size: 271132 Color: 3

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 404881 Color: 0
Size: 344596 Color: 1
Size: 250524 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 404787 Color: 1
Size: 336823 Color: 3
Size: 258391 Color: 4

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 404816 Color: 4
Size: 308239 Color: 0
Size: 286946 Color: 3

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 405080 Color: 0
Size: 328125 Color: 2
Size: 266796 Color: 4

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 405060 Color: 2
Size: 326266 Color: 0
Size: 268675 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 405098 Color: 0
Size: 322915 Color: 1
Size: 271988 Color: 2

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 405084 Color: 2
Size: 339688 Color: 4
Size: 255229 Color: 4

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 405208 Color: 2
Size: 312410 Color: 2
Size: 282383 Color: 1

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 405285 Color: 2
Size: 344127 Color: 2
Size: 250589 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 405454 Color: 2
Size: 301078 Color: 1
Size: 293469 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 405484 Color: 1
Size: 311381 Color: 3
Size: 283136 Color: 4

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 405520 Color: 1
Size: 336241 Color: 1
Size: 258240 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 405600 Color: 1
Size: 328378 Color: 2
Size: 266023 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 405619 Color: 0
Size: 327389 Color: 1
Size: 266993 Color: 4

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 405613 Color: 2
Size: 310851 Color: 2
Size: 283537 Color: 3

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 405709 Color: 2
Size: 343881 Color: 0
Size: 250411 Color: 4

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 405679 Color: 0
Size: 325711 Color: 4
Size: 268611 Color: 1

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 405714 Color: 1
Size: 328995 Color: 3
Size: 265292 Color: 3

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 405757 Color: 2
Size: 334203 Color: 4
Size: 260041 Color: 2

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 405872 Color: 0
Size: 312616 Color: 2
Size: 281513 Color: 2

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 405916 Color: 0
Size: 343424 Color: 2
Size: 250661 Color: 4

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 405948 Color: 0
Size: 305582 Color: 2
Size: 288471 Color: 1

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 405835 Color: 2
Size: 297704 Color: 4
Size: 296462 Color: 2

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 405913 Color: 3
Size: 338625 Color: 0
Size: 255463 Color: 4

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 406102 Color: 0
Size: 323288 Color: 1
Size: 270611 Color: 3

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 405961 Color: 2
Size: 298157 Color: 4
Size: 295883 Color: 2

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 406214 Color: 0
Size: 315487 Color: 3
Size: 278300 Color: 3

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 406316 Color: 0
Size: 334079 Color: 4
Size: 259606 Color: 3

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 406418 Color: 0
Size: 310262 Color: 1
Size: 283321 Color: 3

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 406423 Color: 0
Size: 338133 Color: 3
Size: 255445 Color: 3

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 406219 Color: 3
Size: 306102 Color: 3
Size: 287680 Color: 4

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 406341 Color: 1
Size: 335954 Color: 2
Size: 257706 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 406440 Color: 0
Size: 341750 Color: 2
Size: 251811 Color: 4

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 406358 Color: 3
Size: 304038 Color: 2
Size: 289605 Color: 1

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 406453 Color: 0
Size: 305565 Color: 1
Size: 287983 Color: 4

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 406385 Color: 4
Size: 323288 Color: 1
Size: 270328 Color: 3

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 406553 Color: 3
Size: 334188 Color: 0
Size: 259260 Color: 1

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 406572 Color: 2
Size: 308753 Color: 4
Size: 284676 Color: 4

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 406574 Color: 3
Size: 330444 Color: 0
Size: 262983 Color: 2

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 406599 Color: 1
Size: 316173 Color: 1
Size: 277229 Color: 4

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 406864 Color: 4
Size: 335709 Color: 4
Size: 257428 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 406955 Color: 2
Size: 300032 Color: 1
Size: 293014 Color: 3

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 406994 Color: 2
Size: 309949 Color: 3
Size: 283058 Color: 1

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 407076 Color: 4
Size: 307146 Color: 2
Size: 285779 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 407179 Color: 3
Size: 330499 Color: 2
Size: 262323 Color: 4

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 407242 Color: 3
Size: 338706 Color: 0
Size: 254053 Color: 3

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 407350 Color: 0
Size: 316881 Color: 2
Size: 275770 Color: 3

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 407288 Color: 1
Size: 319156 Color: 4
Size: 273557 Color: 3

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 407449 Color: 0
Size: 322591 Color: 4
Size: 269961 Color: 3

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 407459 Color: 0
Size: 298532 Color: 2
Size: 294010 Color: 4

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 407403 Color: 4
Size: 323572 Color: 4
Size: 269026 Color: 1

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 407441 Color: 2
Size: 322202 Color: 0
Size: 270358 Color: 3

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 407511 Color: 4
Size: 332199 Color: 4
Size: 260291 Color: 2

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 407568 Color: 1
Size: 300688 Color: 2
Size: 291745 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 407570 Color: 3
Size: 318665 Color: 0
Size: 273766 Color: 4

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 407662 Color: 2
Size: 338359 Color: 4
Size: 253980 Color: 3

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 407799 Color: 0
Size: 317183 Color: 1
Size: 275019 Color: 3

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 407703 Color: 2
Size: 323412 Color: 4
Size: 268886 Color: 1

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 407717 Color: 3
Size: 314367 Color: 0
Size: 277917 Color: 1

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 407862 Color: 0
Size: 314101 Color: 4
Size: 278038 Color: 2

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 407776 Color: 2
Size: 325342 Color: 1
Size: 266883 Color: 4

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 407917 Color: 0
Size: 328568 Color: 3
Size: 263516 Color: 1

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 407859 Color: 2
Size: 318424 Color: 3
Size: 273718 Color: 3

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 408067 Color: 3
Size: 302332 Color: 0
Size: 289602 Color: 1

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 408086 Color: 3
Size: 296802 Color: 0
Size: 295113 Color: 3

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 408147 Color: 1
Size: 325064 Color: 0
Size: 266790 Color: 4

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 408172 Color: 0
Size: 321470 Color: 4
Size: 270359 Color: 3

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 408176 Color: 2
Size: 315015 Color: 4
Size: 276810 Color: 3

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 408230 Color: 1
Size: 301683 Color: 4
Size: 290088 Color: 1

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 408230 Color: 1
Size: 332118 Color: 0
Size: 259653 Color: 3

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 408250 Color: 2
Size: 314502 Color: 4
Size: 277249 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 408299 Color: 2
Size: 332088 Color: 3
Size: 259614 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 408334 Color: 3
Size: 337860 Color: 3
Size: 253807 Color: 4

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 408453 Color: 0
Size: 311206 Color: 1
Size: 280342 Color: 3

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 408387 Color: 4
Size: 308269 Color: 1
Size: 283345 Color: 2

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 408493 Color: 3
Size: 339336 Color: 4
Size: 252172 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 408459 Color: 0
Size: 308354 Color: 4
Size: 283188 Color: 2

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 408542 Color: 1
Size: 331955 Color: 4
Size: 259504 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 408618 Color: 0
Size: 337146 Color: 2
Size: 254237 Color: 4

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 408561 Color: 4
Size: 308239 Color: 1
Size: 283201 Color: 3

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 408639 Color: 1
Size: 317613 Color: 3
Size: 273749 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 408725 Color: 3
Size: 337475 Color: 3
Size: 253801 Color: 4

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 408775 Color: 3
Size: 333546 Color: 2
Size: 257680 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 408778 Color: 3
Size: 314163 Color: 2
Size: 277060 Color: 4

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 408779 Color: 3
Size: 331743 Color: 1
Size: 259479 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 408893 Color: 1
Size: 329006 Color: 1
Size: 262102 Color: 3

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 409019 Color: 0
Size: 327276 Color: 3
Size: 263706 Color: 3

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 408972 Color: 1
Size: 301069 Color: 4
Size: 289960 Color: 2

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 409046 Color: 0
Size: 297318 Color: 4
Size: 293637 Color: 4

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 409015 Color: 3
Size: 330012 Color: 4
Size: 260974 Color: 4

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 409056 Color: 3
Size: 337574 Color: 4
Size: 253371 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 409081 Color: 0
Size: 310660 Color: 4
Size: 280260 Color: 1

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 409092 Color: 4
Size: 335059 Color: 3
Size: 255850 Color: 2

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 409113 Color: 0
Size: 324728 Color: 3
Size: 266160 Color: 4

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 409115 Color: 4
Size: 331761 Color: 4
Size: 259125 Color: 1

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 409136 Color: 2
Size: 335373 Color: 4
Size: 255492 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 409184 Color: 2
Size: 322496 Color: 3
Size: 268321 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 409332 Color: 2
Size: 330086 Color: 1
Size: 260583 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 409405 Color: 4
Size: 315580 Color: 2
Size: 275016 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 409421 Color: 0
Size: 328541 Color: 1
Size: 262039 Color: 1

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 409440 Color: 1
Size: 303065 Color: 1
Size: 287496 Color: 4

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 409559 Color: 2
Size: 334507 Color: 0
Size: 255935 Color: 4

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 409588 Color: 0
Size: 334272 Color: 2
Size: 256141 Color: 1

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 409638 Color: 3
Size: 328557 Color: 2
Size: 261806 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 409793 Color: 1
Size: 311861 Color: 1
Size: 278347 Color: 2

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 409646 Color: 4
Size: 319127 Color: 3
Size: 271228 Color: 3

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 409793 Color: 0
Size: 325304 Color: 0
Size: 264904 Color: 2

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 409812 Color: 3
Size: 334615 Color: 2
Size: 255574 Color: 4

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 409815 Color: 1
Size: 305025 Color: 4
Size: 285161 Color: 2

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 409894 Color: 0
Size: 321652 Color: 1
Size: 268455 Color: 4

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 409827 Color: 2
Size: 325370 Color: 2
Size: 264804 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 409967 Color: 0
Size: 315699 Color: 3
Size: 274335 Color: 1

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 409908 Color: 4
Size: 318927 Color: 4
Size: 271166 Color: 1

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 409963 Color: 4
Size: 304728 Color: 1
Size: 285310 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 409973 Color: 4
Size: 313258 Color: 1
Size: 276770 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 410089 Color: 4
Size: 320941 Color: 0
Size: 268971 Color: 4

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 410076 Color: 0
Size: 314935 Color: 1
Size: 274990 Color: 2

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 410111 Color: 2
Size: 302397 Color: 3
Size: 287493 Color: 3

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 410134 Color: 4
Size: 338954 Color: 1
Size: 250913 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 410205 Color: 1
Size: 307682 Color: 2
Size: 282114 Color: 3

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 410269 Color: 4
Size: 300645 Color: 2
Size: 289087 Color: 3

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 410554 Color: 0
Size: 334180 Color: 3
Size: 255267 Color: 1

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 410416 Color: 1
Size: 339427 Color: 4
Size: 250158 Color: 2

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 410560 Color: 3
Size: 314419 Color: 0
Size: 275022 Color: 4

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 410568 Color: 4
Size: 298023 Color: 3
Size: 291410 Color: 2

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 410657 Color: 1
Size: 326613 Color: 3
Size: 262731 Color: 2

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 410815 Color: 0
Size: 325513 Color: 4
Size: 263673 Color: 4

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 410759 Color: 3
Size: 325064 Color: 4
Size: 264178 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 410866 Color: 1
Size: 329024 Color: 0
Size: 260111 Color: 2

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 410897 Color: 4
Size: 326636 Color: 2
Size: 262468 Color: 2

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 410942 Color: 3
Size: 328441 Color: 4
Size: 260618 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 411070 Color: 4
Size: 295985 Color: 2
Size: 292946 Color: 3

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 411076 Color: 4
Size: 332229 Color: 0
Size: 256696 Color: 3

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 411179 Color: 0
Size: 337236 Color: 2
Size: 251586 Color: 4

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 411114 Color: 2
Size: 327569 Color: 4
Size: 261318 Color: 4

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 411222 Color: 0
Size: 323475 Color: 4
Size: 265304 Color: 2

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 411177 Color: 1
Size: 317509 Color: 4
Size: 271315 Color: 2

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 411232 Color: 0
Size: 328738 Color: 4
Size: 260031 Color: 3

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 411193 Color: 1
Size: 320723 Color: 2
Size: 268085 Color: 4

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 411261 Color: 2
Size: 321260 Color: 4
Size: 267480 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 411310 Color: 0
Size: 337396 Color: 2
Size: 251295 Color: 3

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 411299 Color: 1
Size: 301701 Color: 3
Size: 287001 Color: 2

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 411363 Color: 0
Size: 337595 Color: 3
Size: 251043 Color: 2

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 411340 Color: 2
Size: 319504 Color: 4
Size: 269157 Color: 2

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 411413 Color: 3
Size: 311017 Color: 4
Size: 277571 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 411421 Color: 2
Size: 326606 Color: 0
Size: 261974 Color: 2

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 411494 Color: 1
Size: 311749 Color: 1
Size: 276758 Color: 4

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 411536 Color: 1
Size: 321822 Color: 0
Size: 266643 Color: 3

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 411543 Color: 1
Size: 312199 Color: 3
Size: 276259 Color: 2

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 411662 Color: 4
Size: 299571 Color: 3
Size: 288768 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 411670 Color: 3
Size: 331195 Color: 4
Size: 257136 Color: 3

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 411887 Color: 1
Size: 322057 Color: 0
Size: 266057 Color: 4

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 411924 Color: 3
Size: 318850 Color: 0
Size: 269227 Color: 4

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 411981 Color: 4
Size: 307611 Color: 2
Size: 280409 Color: 3

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 412098 Color: 0
Size: 295202 Color: 3
Size: 292701 Color: 4

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 412133 Color: 1
Size: 294528 Color: 3
Size: 293340 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 412321 Color: 0
Size: 310754 Color: 3
Size: 276926 Color: 1

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 412252 Color: 1
Size: 333567 Color: 2
Size: 254182 Color: 4

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 412481 Color: 1
Size: 320454 Color: 4
Size: 267066 Color: 4

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 412492 Color: 1
Size: 333579 Color: 2
Size: 253930 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 412477 Color: 0
Size: 319183 Color: 3
Size: 268341 Color: 3

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 412600 Color: 2
Size: 305060 Color: 1
Size: 282341 Color: 4

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 412652 Color: 1
Size: 301018 Color: 2
Size: 286331 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 412696 Color: 2
Size: 333262 Color: 0
Size: 254043 Color: 1

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 412800 Color: 3
Size: 312068 Color: 2
Size: 275133 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 412910 Color: 2
Size: 330193 Color: 1
Size: 256898 Color: 3

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 412964 Color: 2
Size: 322198 Color: 0
Size: 264839 Color: 4

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 413084 Color: 0
Size: 329351 Color: 4
Size: 257566 Color: 2

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 413087 Color: 4
Size: 324825 Color: 4
Size: 262089 Color: 3

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 413136 Color: 0
Size: 328956 Color: 3
Size: 257909 Color: 1

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 413105 Color: 4
Size: 321478 Color: 2
Size: 265418 Color: 1

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 413174 Color: 2
Size: 321205 Color: 0
Size: 265622 Color: 3

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 413221 Color: 4
Size: 320396 Color: 2
Size: 266384 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 413228 Color: 3
Size: 332327 Color: 2
Size: 254446 Color: 1

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 413298 Color: 2
Size: 322527 Color: 1
Size: 264176 Color: 4

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 413386 Color: 4
Size: 305483 Color: 0
Size: 281132 Color: 2

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 413450 Color: 0
Size: 322006 Color: 4
Size: 264545 Color: 4

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 413461 Color: 2
Size: 321619 Color: 1
Size: 264921 Color: 3

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 413488 Color: 4
Size: 306110 Color: 1
Size: 280403 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 413514 Color: 3
Size: 333348 Color: 1
Size: 253139 Color: 4

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 413569 Color: 2
Size: 327422 Color: 2
Size: 259010 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 413607 Color: 1
Size: 303631 Color: 2
Size: 282763 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 413607 Color: 3
Size: 295112 Color: 3
Size: 291282 Color: 4

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 413628 Color: 4
Size: 302775 Color: 3
Size: 283598 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 413679 Color: 2
Size: 299088 Color: 3
Size: 287234 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 413687 Color: 1
Size: 314389 Color: 1
Size: 271925 Color: 3

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 406966 Color: 0
Size: 340328 Color: 4
Size: 252707 Color: 2

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 413774 Color: 1
Size: 304490 Color: 4
Size: 281737 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 413772 Color: 0
Size: 308271 Color: 0
Size: 277958 Color: 2

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 413862 Color: 4
Size: 333806 Color: 2
Size: 252333 Color: 1

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 413985 Color: 1
Size: 320963 Color: 4
Size: 265053 Color: 3

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 414145 Color: 0
Size: 321925 Color: 4
Size: 263931 Color: 3

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 414225 Color: 0
Size: 293364 Color: 4
Size: 292412 Color: 3

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 414139 Color: 4
Size: 300204 Color: 1
Size: 285658 Color: 2

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 414225 Color: 2
Size: 303824 Color: 2
Size: 281952 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 414352 Color: 2
Size: 327380 Color: 2
Size: 258269 Color: 3

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 414407 Color: 3
Size: 324096 Color: 0
Size: 261498 Color: 2

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 414482 Color: 0
Size: 309320 Color: 4
Size: 276199 Color: 3

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 414453 Color: 4
Size: 323019 Color: 1
Size: 262529 Color: 4

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 414641 Color: 0
Size: 328627 Color: 3
Size: 256733 Color: 4

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 414713 Color: 1
Size: 303992 Color: 0
Size: 281296 Color: 3

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 414736 Color: 3
Size: 321592 Color: 3
Size: 263673 Color: 2

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 414750 Color: 0
Size: 328254 Color: 3
Size: 256997 Color: 3

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 414760 Color: 1
Size: 301990 Color: 2
Size: 283251 Color: 2

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 414873 Color: 0
Size: 296398 Color: 2
Size: 288730 Color: 2

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 414762 Color: 3
Size: 311672 Color: 3
Size: 273567 Color: 4

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 414874 Color: 0
Size: 321753 Color: 1
Size: 263374 Color: 3

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 414888 Color: 0
Size: 308415 Color: 1
Size: 276698 Color: 2

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 414785 Color: 3
Size: 316806 Color: 2
Size: 268410 Color: 4

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 414966 Color: 0
Size: 309484 Color: 1
Size: 275551 Color: 2

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 415044 Color: 0
Size: 311635 Color: 4
Size: 273322 Color: 4

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 415099 Color: 0
Size: 300672 Color: 2
Size: 284230 Color: 4

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 414963 Color: 2
Size: 293076 Color: 3
Size: 291962 Color: 1

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 415150 Color: 0
Size: 329744 Color: 4
Size: 255107 Color: 3

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 415055 Color: 2
Size: 302359 Color: 1
Size: 282587 Color: 3

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 415206 Color: 0
Size: 294396 Color: 3
Size: 290399 Color: 2

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 415242 Color: 2
Size: 298745 Color: 4
Size: 286014 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 415328 Color: 1
Size: 324025 Color: 2
Size: 260648 Color: 3

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 415517 Color: 3
Size: 329933 Color: 3
Size: 254551 Color: 4

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 415567 Color: 3
Size: 318450 Color: 0
Size: 265984 Color: 2

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 415645 Color: 4
Size: 316538 Color: 0
Size: 267818 Color: 2

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 415662 Color: 2
Size: 293493 Color: 3
Size: 290846 Color: 1

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 415700 Color: 1
Size: 325945 Color: 0
Size: 258356 Color: 3

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 415706 Color: 3
Size: 311382 Color: 2
Size: 272913 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 415811 Color: 4
Size: 318817 Color: 1
Size: 265373 Color: 3

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 415888 Color: 1
Size: 312195 Color: 0
Size: 271918 Color: 2

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 415942 Color: 1
Size: 308136 Color: 2
Size: 275923 Color: 4

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 416038 Color: 1
Size: 330235 Color: 4
Size: 253728 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 416213 Color: 1
Size: 321776 Color: 2
Size: 262012 Color: 2

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 416240 Color: 3
Size: 332710 Color: 2
Size: 251051 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 416260 Color: 1
Size: 327964 Color: 3
Size: 255777 Color: 1

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 416267 Color: 1
Size: 326392 Color: 0
Size: 257342 Color: 2

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 416294 Color: 2
Size: 331831 Color: 0
Size: 251876 Color: 2

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 416314 Color: 4
Size: 307726 Color: 3
Size: 275961 Color: 2

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 416316 Color: 4
Size: 301676 Color: 0
Size: 282009 Color: 2

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 416493 Color: 1
Size: 303300 Color: 4
Size: 280208 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 416496 Color: 1
Size: 310188 Color: 2
Size: 273317 Color: 3

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 416501 Color: 1
Size: 333457 Color: 0
Size: 250043 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 416676 Color: 3
Size: 296802 Color: 4
Size: 286523 Color: 1

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 416681 Color: 2
Size: 331280 Color: 0
Size: 252040 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 416810 Color: 1
Size: 301386 Color: 2
Size: 281805 Color: 4

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 416991 Color: 4
Size: 305094 Color: 2
Size: 277916 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 417095 Color: 0
Size: 302569 Color: 3
Size: 280337 Color: 2

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 417009 Color: 1
Size: 325625 Color: 4
Size: 257367 Color: 2

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 417086 Color: 4
Size: 305326 Color: 0
Size: 277589 Color: 3

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 417222 Color: 0
Size: 330427 Color: 3
Size: 252352 Color: 3

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 417134 Color: 1
Size: 325274 Color: 4
Size: 257593 Color: 2

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 417184 Color: 4
Size: 302696 Color: 3
Size: 280121 Color: 3

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 417356 Color: 4
Size: 299306 Color: 0
Size: 283339 Color: 3

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 417441 Color: 3
Size: 296973 Color: 4
Size: 285587 Color: 4

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 417461 Color: 2
Size: 316825 Color: 1
Size: 265715 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 417497 Color: 4
Size: 323724 Color: 3
Size: 258780 Color: 4

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 417520 Color: 1
Size: 299015 Color: 3
Size: 283466 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 417657 Color: 0
Size: 301707 Color: 2
Size: 280637 Color: 3

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 417777 Color: 3
Size: 304748 Color: 4
Size: 277476 Color: 4

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 417901 Color: 0
Size: 299843 Color: 2
Size: 282257 Color: 3

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 417860 Color: 3
Size: 295440 Color: 2
Size: 286701 Color: 4

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 417902 Color: 3
Size: 326337 Color: 2
Size: 255762 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 417904 Color: 3
Size: 315225 Color: 3
Size: 266872 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 417969 Color: 3
Size: 313380 Color: 2
Size: 268652 Color: 4

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 418029 Color: 3
Size: 298132 Color: 4
Size: 283840 Color: 2

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 418120 Color: 0
Size: 317612 Color: 1
Size: 264269 Color: 4

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 418047 Color: 3
Size: 299329 Color: 1
Size: 282625 Color: 1

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 418051 Color: 3
Size: 308413 Color: 0
Size: 273537 Color: 1

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 418162 Color: 1
Size: 331498 Color: 0
Size: 250341 Color: 4

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 418266 Color: 4
Size: 319344 Color: 4
Size: 262391 Color: 3

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 418351 Color: 0
Size: 295403 Color: 3
Size: 286247 Color: 4

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 418293 Color: 3
Size: 304228 Color: 2
Size: 277480 Color: 2

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 418305 Color: 1
Size: 306843 Color: 0
Size: 274853 Color: 1

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 418336 Color: 4
Size: 315179 Color: 1
Size: 266486 Color: 1

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 418592 Color: 1
Size: 330615 Color: 3
Size: 250794 Color: 2

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 418705 Color: 0
Size: 304027 Color: 1
Size: 277269 Color: 3

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 418603 Color: 4
Size: 313319 Color: 3
Size: 268079 Color: 1

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 418628 Color: 1
Size: 319326 Color: 3
Size: 262047 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 418788 Color: 4
Size: 322534 Color: 2
Size: 258679 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 418803 Color: 1
Size: 329242 Color: 2
Size: 251956 Color: 3

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 418829 Color: 1
Size: 305978 Color: 0
Size: 275194 Color: 4

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 418911 Color: 3
Size: 294065 Color: 2
Size: 287025 Color: 2

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 418981 Color: 2
Size: 328751 Color: 2
Size: 252269 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 418991 Color: 3
Size: 314233 Color: 2
Size: 266777 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 419039 Color: 4
Size: 326129 Color: 2
Size: 254833 Color: 4

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 419143 Color: 1
Size: 304657 Color: 0
Size: 276201 Color: 4

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 419160 Color: 3
Size: 295680 Color: 0
Size: 285161 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 419208 Color: 3
Size: 291610 Color: 0
Size: 289183 Color: 4

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 419246 Color: 4
Size: 310467 Color: 1
Size: 270288 Color: 1

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 419312 Color: 2
Size: 299627 Color: 3
Size: 281062 Color: 2

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 419384 Color: 1
Size: 306034 Color: 0
Size: 274583 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 419450 Color: 3
Size: 291071 Color: 4
Size: 289480 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 419560 Color: 0
Size: 313222 Color: 3
Size: 267219 Color: 2

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 419549 Color: 3
Size: 329197 Color: 2
Size: 251255 Color: 1

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 419775 Color: 0
Size: 291794 Color: 3
Size: 288432 Color: 3

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 419655 Color: 2
Size: 326919 Color: 1
Size: 253427 Color: 2

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 419967 Color: 0
Size: 328075 Color: 4
Size: 251959 Color: 3

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 419991 Color: 3
Size: 311493 Color: 2
Size: 268517 Color: 2

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 420013 Color: 2
Size: 297524 Color: 1
Size: 282464 Color: 0

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 420044 Color: 3
Size: 299256 Color: 1
Size: 280701 Color: 4

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 420083 Color: 3
Size: 318277 Color: 1
Size: 261641 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 420111 Color: 3
Size: 321292 Color: 3
Size: 258598 Color: 2

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 420167 Color: 2
Size: 305353 Color: 0
Size: 274481 Color: 3

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 420251 Color: 0
Size: 303516 Color: 3
Size: 276234 Color: 3

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 420195 Color: 2
Size: 323879 Color: 1
Size: 255927 Color: 4

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 420399 Color: 0
Size: 304440 Color: 1
Size: 275162 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 420409 Color: 3
Size: 309636 Color: 1
Size: 269956 Color: 4

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 420442 Color: 2
Size: 308110 Color: 4
Size: 271449 Color: 1

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 420549 Color: 3
Size: 315404 Color: 4
Size: 264048 Color: 2

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 420653 Color: 0
Size: 290205 Color: 4
Size: 289143 Color: 2

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 420569 Color: 1
Size: 291454 Color: 4
Size: 287978 Color: 3

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 420687 Color: 1
Size: 322818 Color: 0
Size: 256496 Color: 4

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 420739 Color: 2
Size: 305511 Color: 2
Size: 273751 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 420752 Color: 1
Size: 299856 Color: 3
Size: 279393 Color: 2

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 420809 Color: 3
Size: 299614 Color: 0
Size: 279578 Color: 3

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 420820 Color: 1
Size: 314154 Color: 0
Size: 265027 Color: 3

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 420893 Color: 2
Size: 325383 Color: 4
Size: 253725 Color: 4

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 421076 Color: 3
Size: 301907 Color: 4
Size: 277018 Color: 1

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 421091 Color: 3
Size: 317019 Color: 0
Size: 261891 Color: 1

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 421177 Color: 0
Size: 293878 Color: 2
Size: 284946 Color: 3

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 421179 Color: 2
Size: 317107 Color: 1
Size: 261715 Color: 3

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 421245 Color: 4
Size: 319863 Color: 0
Size: 258893 Color: 1

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 421327 Color: 0
Size: 320522 Color: 2
Size: 258152 Color: 2

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 421323 Color: 1
Size: 294747 Color: 3
Size: 283931 Color: 3

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 421376 Color: 4
Size: 306981 Color: 3
Size: 271644 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 421413 Color: 0
Size: 320351 Color: 1
Size: 258237 Color: 4

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 421458 Color: 4
Size: 311377 Color: 1
Size: 267166 Color: 2

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 421486 Color: 0
Size: 290256 Color: 3
Size: 288259 Color: 1

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 421644 Color: 0
Size: 326567 Color: 2
Size: 251790 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 421485 Color: 2
Size: 295570 Color: 1
Size: 282946 Color: 4

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 421683 Color: 0
Size: 299817 Color: 3
Size: 278501 Color: 2

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 421721 Color: 0
Size: 294858 Color: 1
Size: 283422 Color: 1

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 421639 Color: 3
Size: 311419 Color: 0
Size: 266943 Color: 1

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 421661 Color: 1
Size: 315223 Color: 3
Size: 263117 Color: 2

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 421881 Color: 0
Size: 295624 Color: 3
Size: 282496 Color: 2

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 421926 Color: 0
Size: 290187 Color: 2
Size: 287888 Color: 2

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 421706 Color: 3
Size: 314289 Color: 4
Size: 264006 Color: 3

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 421839 Color: 2
Size: 323026 Color: 0
Size: 255136 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 421925 Color: 3
Size: 318920 Color: 2
Size: 259156 Color: 3

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 421995 Color: 4
Size: 306283 Color: 0
Size: 271723 Color: 2

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 422009 Color: 3
Size: 305415 Color: 4
Size: 272577 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 422013 Color: 2
Size: 305174 Color: 3
Size: 272814 Color: 1

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 422131 Color: 0
Size: 315075 Color: 2
Size: 262795 Color: 4

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 422137 Color: 0
Size: 315419 Color: 2
Size: 262445 Color: 4

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 422154 Color: 0
Size: 318783 Color: 3
Size: 259064 Color: 4

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 422042 Color: 1
Size: 289721 Color: 4
Size: 288238 Color: 2

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 422225 Color: 0
Size: 317551 Color: 3
Size: 260225 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 422258 Color: 3
Size: 314542 Color: 1
Size: 263201 Color: 2

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 422262 Color: 1
Size: 301119 Color: 0
Size: 276620 Color: 1

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 422318 Color: 0
Size: 322345 Color: 1
Size: 255338 Color: 4

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 422334 Color: 3
Size: 294332 Color: 2
Size: 283335 Color: 4

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 422381 Color: 2
Size: 318944 Color: 4
Size: 258676 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 422523 Color: 3
Size: 327443 Color: 2
Size: 250035 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 422670 Color: 0
Size: 324536 Color: 4
Size: 252795 Color: 2

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 422780 Color: 0
Size: 296970 Color: 3
Size: 280251 Color: 3

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 422692 Color: 2
Size: 295769 Color: 1
Size: 281540 Color: 3

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 422850 Color: 1
Size: 291783 Color: 0
Size: 285368 Color: 3

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 422887 Color: 4
Size: 321864 Color: 2
Size: 255250 Color: 3

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 422891 Color: 4
Size: 305327 Color: 0
Size: 271783 Color: 3

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 422955 Color: 3
Size: 311882 Color: 1
Size: 265164 Color: 4

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 423045 Color: 4
Size: 301756 Color: 2
Size: 275200 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 423012 Color: 0
Size: 326199 Color: 4
Size: 250790 Color: 3

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 423094 Color: 4
Size: 313675 Color: 1
Size: 263232 Color: 3

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 423122 Color: 2
Size: 297931 Color: 1
Size: 278948 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 423192 Color: 2
Size: 323039 Color: 3
Size: 253770 Color: 1

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 423256 Color: 2
Size: 303563 Color: 0
Size: 273182 Color: 2

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 423258 Color: 3
Size: 317657 Color: 0
Size: 259086 Color: 1

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 423268 Color: 4
Size: 299401 Color: 3
Size: 277332 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 423363 Color: 0
Size: 302505 Color: 4
Size: 274133 Color: 1

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 423296 Color: 4
Size: 307284 Color: 4
Size: 269421 Color: 3

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 423452 Color: 2
Size: 298195 Color: 0
Size: 278354 Color: 2

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 423474 Color: 4
Size: 309786 Color: 4
Size: 266741 Color: 1

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 423539 Color: 0
Size: 313027 Color: 2
Size: 263435 Color: 3

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 423587 Color: 0
Size: 291177 Color: 2
Size: 285237 Color: 3

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 423486 Color: 3
Size: 296377 Color: 2
Size: 280138 Color: 3

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 423494 Color: 4
Size: 321771 Color: 4
Size: 254736 Color: 2

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 423595 Color: 3
Size: 301497 Color: 1
Size: 274909 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 423629 Color: 3
Size: 324695 Color: 4
Size: 251677 Color: 1

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 423642 Color: 2
Size: 311653 Color: 3
Size: 264706 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 423712 Color: 2
Size: 316645 Color: 4
Size: 259644 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 423798 Color: 4
Size: 317498 Color: 0
Size: 258705 Color: 4

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 423846 Color: 4
Size: 311003 Color: 0
Size: 265152 Color: 1

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 424171 Color: 3
Size: 325294 Color: 0
Size: 250536 Color: 2

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 424189 Color: 1
Size: 325024 Color: 1
Size: 250788 Color: 4

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 424310 Color: 0
Size: 306178 Color: 1
Size: 269513 Color: 3

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 424253 Color: 3
Size: 289435 Color: 4
Size: 286313 Color: 1

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 424379 Color: 4
Size: 293341 Color: 1
Size: 282281 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 424391 Color: 2
Size: 317461 Color: 3
Size: 258149 Color: 4

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 424425 Color: 2
Size: 295984 Color: 2
Size: 279592 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 424508 Color: 0
Size: 314456 Color: 2
Size: 261037 Color: 2

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 424469 Color: 4
Size: 299886 Color: 3
Size: 275646 Color: 4

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 424476 Color: 2
Size: 293385 Color: 0
Size: 282140 Color: 4

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 424585 Color: 0
Size: 291871 Color: 3
Size: 283545 Color: 2

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 424623 Color: 4
Size: 315645 Color: 3
Size: 259733 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 424651 Color: 3
Size: 318355 Color: 4
Size: 256995 Color: 2

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 424680 Color: 4
Size: 291717 Color: 1
Size: 283604 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 424663 Color: 0
Size: 316940 Color: 4
Size: 258398 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 424703 Color: 3
Size: 307787 Color: 2
Size: 267511 Color: 4

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 424822 Color: 0
Size: 295778 Color: 3
Size: 279401 Color: 2

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 424814 Color: 2
Size: 309495 Color: 0
Size: 265692 Color: 3

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 424837 Color: 1
Size: 293103 Color: 2
Size: 282061 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 424892 Color: 4
Size: 321964 Color: 2
Size: 253145 Color: 2

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 424946 Color: 3
Size: 303765 Color: 0
Size: 271290 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 424973 Color: 4
Size: 298954 Color: 1
Size: 276074 Color: 1

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 425023 Color: 3
Size: 306224 Color: 0
Size: 268754 Color: 1

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 425079 Color: 3
Size: 298143 Color: 4
Size: 276779 Color: 3

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 425266 Color: 0
Size: 324090 Color: 1
Size: 250645 Color: 4

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 425181 Color: 3
Size: 309818 Color: 3
Size: 265002 Color: 4

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 425325 Color: 1
Size: 306340 Color: 4
Size: 268336 Color: 4

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 425443 Color: 3
Size: 314937 Color: 2
Size: 259621 Color: 4

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 425498 Color: 2
Size: 322783 Color: 0
Size: 251720 Color: 4

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 425519 Color: 1
Size: 302320 Color: 1
Size: 272162 Color: 4

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 425681 Color: 3
Size: 318961 Color: 2
Size: 255359 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 425806 Color: 4
Size: 323443 Color: 3
Size: 250752 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 425878 Color: 4
Size: 320253 Color: 3
Size: 253870 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 425918 Color: 0
Size: 288791 Color: 2
Size: 285292 Color: 2

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 425971 Color: 2
Size: 291239 Color: 3
Size: 282791 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 426013 Color: 4
Size: 323083 Color: 2
Size: 250905 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 426050 Color: 1
Size: 308392 Color: 2
Size: 265559 Color: 2

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 426140 Color: 3
Size: 292407 Color: 0
Size: 281454 Color: 3

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 426165 Color: 2
Size: 298664 Color: 3
Size: 275172 Color: 4

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 426267 Color: 1
Size: 302687 Color: 0
Size: 271047 Color: 3

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 426318 Color: 4
Size: 313232 Color: 4
Size: 260451 Color: 1

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 426436 Color: 2
Size: 305378 Color: 0
Size: 268187 Color: 1

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 426460 Color: 4
Size: 314116 Color: 0
Size: 259425 Color: 1

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 426501 Color: 3
Size: 296142 Color: 4
Size: 277358 Color: 4

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 426635 Color: 4
Size: 302081 Color: 0
Size: 271285 Color: 2

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 426661 Color: 3
Size: 307975 Color: 1
Size: 265365 Color: 2

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 426720 Color: 4
Size: 318836 Color: 4
Size: 254445 Color: 2

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 426722 Color: 3
Size: 306873 Color: 0
Size: 266406 Color: 2

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 426775 Color: 0
Size: 293933 Color: 4
Size: 279293 Color: 2

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 426763 Color: 4
Size: 287417 Color: 3
Size: 285821 Color: 3

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 426769 Color: 2
Size: 300921 Color: 3
Size: 272311 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 426803 Color: 2
Size: 296780 Color: 2
Size: 276418 Color: 3

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 426829 Color: 4
Size: 291135 Color: 1
Size: 282037 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 427004 Color: 2
Size: 301102 Color: 2
Size: 271895 Color: 4

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 427016 Color: 2
Size: 315609 Color: 0
Size: 257376 Color: 2

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 427055 Color: 2
Size: 294619 Color: 1
Size: 278327 Color: 3

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 427140 Color: 0
Size: 299942 Color: 2
Size: 272919 Color: 1

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 427239 Color: 0
Size: 321082 Color: 1
Size: 251680 Color: 1

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 427217 Color: 4
Size: 287279 Color: 2
Size: 285505 Color: 3

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 427224 Color: 4
Size: 303854 Color: 4
Size: 268923 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 427257 Color: 2
Size: 320504 Color: 4
Size: 252240 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 427274 Color: 2
Size: 305823 Color: 3
Size: 266904 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 427358 Color: 4
Size: 286709 Color: 3
Size: 285934 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 427388 Color: 3
Size: 310291 Color: 1
Size: 262322 Color: 2

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 427458 Color: 0
Size: 301157 Color: 3
Size: 271386 Color: 4

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 427396 Color: 4
Size: 320147 Color: 1
Size: 252458 Color: 1

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 427609 Color: 0
Size: 316053 Color: 2
Size: 256339 Color: 2

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 427610 Color: 0
Size: 296249 Color: 3
Size: 276142 Color: 2

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 427589 Color: 1
Size: 303141 Color: 4
Size: 269271 Color: 4

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 427600 Color: 2
Size: 299073 Color: 0
Size: 273328 Color: 3

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 427701 Color: 3
Size: 299365 Color: 2
Size: 272935 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 427775 Color: 2
Size: 313547 Color: 4
Size: 258679 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 427876 Color: 1
Size: 295037 Color: 4
Size: 277088 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 427888 Color: 3
Size: 305570 Color: 4
Size: 266543 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 427891 Color: 1
Size: 293930 Color: 1
Size: 278180 Color: 4

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 427929 Color: 3
Size: 304279 Color: 0
Size: 267793 Color: 4

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 427932 Color: 2
Size: 315291 Color: 0
Size: 256778 Color: 4

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 428064 Color: 0
Size: 290650 Color: 4
Size: 281287 Color: 1

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 428083 Color: 4
Size: 287026 Color: 1
Size: 284892 Color: 2

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 428090 Color: 4
Size: 293942 Color: 4
Size: 277969 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 428316 Color: 2
Size: 314499 Color: 0
Size: 257186 Color: 1

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 428366 Color: 3
Size: 318952 Color: 3
Size: 252683 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 428440 Color: 4
Size: 306024 Color: 1
Size: 265537 Color: 2

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 428477 Color: 0
Size: 287612 Color: 4
Size: 283912 Color: 4

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 428471 Color: 2
Size: 306067 Color: 4
Size: 265463 Color: 3

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 428498 Color: 3
Size: 299197 Color: 2
Size: 272306 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 428611 Color: 0
Size: 313265 Color: 4
Size: 258125 Color: 1

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 428634 Color: 4
Size: 297371 Color: 0
Size: 273996 Color: 3

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 428645 Color: 4
Size: 285996 Color: 1
Size: 285360 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 428653 Color: 0
Size: 314045 Color: 4
Size: 257303 Color: 4

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 428689 Color: 2
Size: 298137 Color: 4
Size: 273175 Color: 1

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 428797 Color: 0
Size: 320789 Color: 3
Size: 250415 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 428734 Color: 4
Size: 316496 Color: 0
Size: 254771 Color: 4

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 428861 Color: 0
Size: 293118 Color: 1
Size: 278022 Color: 2

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 428750 Color: 1
Size: 304412 Color: 3
Size: 266839 Color: 4

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 429110 Color: 0
Size: 310093 Color: 4
Size: 260798 Color: 1

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 429136 Color: 0
Size: 295100 Color: 4
Size: 275765 Color: 3

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 429111 Color: 2
Size: 314822 Color: 1
Size: 256068 Color: 2

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 429330 Color: 0
Size: 318227 Color: 1
Size: 252444 Color: 3

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 429334 Color: 3
Size: 311093 Color: 0
Size: 259574 Color: 4

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 429421 Color: 2
Size: 288192 Color: 1
Size: 282388 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 429468 Color: 2
Size: 318304 Color: 1
Size: 252229 Color: 2

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 429522 Color: 0
Size: 291969 Color: 4
Size: 278510 Color: 3

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 429504 Color: 2
Size: 298143 Color: 1
Size: 272354 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 429528 Color: 2
Size: 308007 Color: 4
Size: 262466 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 429538 Color: 1
Size: 309719 Color: 0
Size: 260744 Color: 1

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 429527 Color: 0
Size: 303027 Color: 3
Size: 267447 Color: 1

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 429561 Color: 1
Size: 301674 Color: 4
Size: 268766 Color: 2

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 429564 Color: 4
Size: 317176 Color: 3
Size: 253261 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 429609 Color: 1
Size: 308398 Color: 2
Size: 261994 Color: 4

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 429649 Color: 3
Size: 315220 Color: 4
Size: 255132 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 429683 Color: 2
Size: 290468 Color: 3
Size: 279850 Color: 4

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 429683 Color: 2
Size: 295002 Color: 4
Size: 275316 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 429694 Color: 3
Size: 290454 Color: 4
Size: 279853 Color: 3

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 429770 Color: 4
Size: 286698 Color: 3
Size: 283533 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 429782 Color: 0
Size: 313414 Color: 1
Size: 256805 Color: 4

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 429818 Color: 2
Size: 306270 Color: 3
Size: 263913 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 429850 Color: 2
Size: 312014 Color: 3
Size: 258137 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 429858 Color: 1
Size: 286258 Color: 2
Size: 283885 Color: 3

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 429953 Color: 0
Size: 314610 Color: 3
Size: 255438 Color: 4

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 429894 Color: 3
Size: 306371 Color: 4
Size: 263736 Color: 1

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 429960 Color: 3
Size: 315842 Color: 3
Size: 254199 Color: 4

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 430396 Color: 2
Size: 309122 Color: 0
Size: 260483 Color: 4

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 430446 Color: 1
Size: 314695 Color: 2
Size: 254860 Color: 4

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 430519 Color: 0
Size: 304207 Color: 3
Size: 265275 Color: 1

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 430580 Color: 3
Size: 307746 Color: 3
Size: 261675 Color: 4

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 430595 Color: 3
Size: 292542 Color: 1
Size: 276864 Color: 2

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 430797 Color: 4
Size: 315962 Color: 4
Size: 253242 Color: 2

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 430887 Color: 4
Size: 306794 Color: 0
Size: 262320 Color: 3

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 430940 Color: 4
Size: 315068 Color: 3
Size: 253993 Color: 2

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 430959 Color: 2
Size: 305130 Color: 0
Size: 263912 Color: 1

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 431028 Color: 4
Size: 296759 Color: 0
Size: 272214 Color: 2

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 431033 Color: 3
Size: 293399 Color: 4
Size: 275569 Color: 3

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 431118 Color: 0
Size: 295517 Color: 2
Size: 273366 Color: 1

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 431054 Color: 3
Size: 316415 Color: 4
Size: 252532 Color: 3

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 431175 Color: 4
Size: 290365 Color: 3
Size: 278461 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 431236 Color: 0
Size: 304886 Color: 3
Size: 263879 Color: 3

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 431401 Color: 3
Size: 301562 Color: 1
Size: 267038 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 431561 Color: 3
Size: 294468 Color: 1
Size: 273972 Color: 3

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 431735 Color: 0
Size: 300460 Color: 2
Size: 267806 Color: 1

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 431650 Color: 4
Size: 284736 Color: 4
Size: 283615 Color: 3

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 431870 Color: 0
Size: 306925 Color: 4
Size: 261206 Color: 4

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 431910 Color: 1
Size: 293472 Color: 0
Size: 274619 Color: 4

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 431943 Color: 4
Size: 295573 Color: 4
Size: 272485 Color: 3

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 432152 Color: 0
Size: 317774 Color: 4
Size: 250075 Color: 3

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 431965 Color: 3
Size: 295999 Color: 2
Size: 272037 Color: 1

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 432099 Color: 2
Size: 293340 Color: 1
Size: 274562 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 432159 Color: 0
Size: 299640 Color: 1
Size: 268202 Color: 4

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 432425 Color: 3
Size: 299388 Color: 0
Size: 268188 Color: 1

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 432442 Color: 2
Size: 301148 Color: 3
Size: 266411 Color: 4

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 432513 Color: 3
Size: 287240 Color: 2
Size: 280248 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 432544 Color: 3
Size: 301180 Color: 1
Size: 266277 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 432651 Color: 2
Size: 289287 Color: 1
Size: 278063 Color: 1

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 432651 Color: 1
Size: 290579 Color: 2
Size: 276771 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 432806 Color: 2
Size: 290076 Color: 4
Size: 277119 Color: 3

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 432829 Color: 2
Size: 283848 Color: 4
Size: 283324 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 432834 Color: 3
Size: 306378 Color: 0
Size: 260789 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 432842 Color: 2
Size: 307657 Color: 3
Size: 259502 Color: 4

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 432890 Color: 3
Size: 310249 Color: 1
Size: 256862 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 432956 Color: 2
Size: 286807 Color: 0
Size: 280238 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 432983 Color: 1
Size: 285034 Color: 4
Size: 281984 Color: 3

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 433005 Color: 2
Size: 301480 Color: 4
Size: 265516 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 433013 Color: 3
Size: 295610 Color: 3
Size: 271378 Color: 1

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 433062 Color: 0
Size: 286060 Color: 2
Size: 280879 Color: 3

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 433052 Color: 4
Size: 296140 Color: 1
Size: 270809 Color: 3

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 433060 Color: 4
Size: 297612 Color: 2
Size: 269329 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 433070 Color: 1
Size: 284294 Color: 1
Size: 282637 Color: 3

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 433260 Color: 4
Size: 310333 Color: 0
Size: 256408 Color: 1

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 433338 Color: 0
Size: 311132 Color: 2
Size: 255531 Color: 2

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 433287 Color: 3
Size: 295683 Color: 1
Size: 271031 Color: 3

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 433343 Color: 4
Size: 311720 Color: 0
Size: 254938 Color: 3

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 433338 Color: 0
Size: 300150 Color: 0
Size: 266513 Color: 2

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 433347 Color: 4
Size: 304074 Color: 1
Size: 262580 Color: 4

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 433386 Color: 0
Size: 310965 Color: 2
Size: 255650 Color: 2

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 433415 Color: 2
Size: 307472 Color: 0
Size: 259114 Color: 2

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 433483 Color: 1
Size: 315176 Color: 0
Size: 251342 Color: 1

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 433512 Color: 4
Size: 288691 Color: 2
Size: 277798 Color: 2

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 433672 Color: 2
Size: 304662 Color: 4
Size: 261667 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 433711 Color: 1
Size: 298888 Color: 3
Size: 267402 Color: 1

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 433771 Color: 4
Size: 303795 Color: 1
Size: 262435 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 433801 Color: 4
Size: 306182 Color: 2
Size: 260018 Color: 4

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 433805 Color: 3
Size: 286700 Color: 1
Size: 279496 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 433871 Color: 4
Size: 286108 Color: 4
Size: 280022 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 433933 Color: 2
Size: 285681 Color: 0
Size: 280387 Color: 2

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 434014 Color: 0
Size: 295168 Color: 1
Size: 270819 Color: 4

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 434021 Color: 3
Size: 286922 Color: 1
Size: 279058 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 434036 Color: 3
Size: 309248 Color: 2
Size: 256717 Color: 1

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 434133 Color: 1
Size: 303002 Color: 0
Size: 262866 Color: 1

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 434215 Color: 4
Size: 305015 Color: 1
Size: 260771 Color: 2

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 434239 Color: 3
Size: 285811 Color: 0
Size: 279951 Color: 4

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 434253 Color: 2
Size: 284354 Color: 1
Size: 281394 Color: 3

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 434274 Color: 4
Size: 305497 Color: 0
Size: 260230 Color: 1

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 434310 Color: 4
Size: 306658 Color: 2
Size: 259033 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 434325 Color: 4
Size: 294730 Color: 2
Size: 270946 Color: 2

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 434331 Color: 3
Size: 303995 Color: 2
Size: 261675 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 434412 Color: 2
Size: 285513 Color: 3
Size: 280076 Color: 4

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 434483 Color: 0
Size: 299525 Color: 1
Size: 265993 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 434591 Color: 3
Size: 306066 Color: 0
Size: 259344 Color: 4

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 434639 Color: 4
Size: 304233 Color: 3
Size: 261129 Color: 3

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 434741 Color: 3
Size: 297362 Color: 0
Size: 267898 Color: 1

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 434744 Color: 4
Size: 283989 Color: 1
Size: 281268 Color: 1

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 434856 Color: 4
Size: 296167 Color: 2
Size: 268978 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 434954 Color: 3
Size: 285498 Color: 2
Size: 279549 Color: 2

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 435078 Color: 2
Size: 290944 Color: 3
Size: 273979 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 435157 Color: 0
Size: 296682 Color: 3
Size: 268162 Color: 4

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 435135 Color: 3
Size: 313092 Color: 2
Size: 251774 Color: 2

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 435184 Color: 1
Size: 309642 Color: 0
Size: 255175 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 435205 Color: 2
Size: 284185 Color: 3
Size: 280611 Color: 4

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 435217 Color: 3
Size: 296044 Color: 0
Size: 268740 Color: 1

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 435302 Color: 2
Size: 296099 Color: 0
Size: 268600 Color: 1

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 435383 Color: 3
Size: 282425 Color: 3
Size: 282193 Color: 2

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 435427 Color: 0
Size: 302979 Color: 4
Size: 261595 Color: 3

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 435461 Color: 3
Size: 311807 Color: 1
Size: 252733 Color: 1

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 435613 Color: 2
Size: 307462 Color: 3
Size: 256926 Color: 3

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 435643 Color: 3
Size: 300439 Color: 4
Size: 263919 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 435847 Color: 4
Size: 286122 Color: 1
Size: 278032 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 435884 Color: 4
Size: 302726 Color: 1
Size: 261391 Color: 3

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 436013 Color: 3
Size: 289841 Color: 1
Size: 274147 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 436027 Color: 4
Size: 288797 Color: 1
Size: 275177 Color: 2

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 436036 Color: 1
Size: 284345 Color: 0
Size: 279620 Color: 3

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 436172 Color: 0
Size: 300544 Color: 1
Size: 263285 Color: 3

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 436043 Color: 3
Size: 299673 Color: 2
Size: 264285 Color: 1

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 436219 Color: 0
Size: 311099 Color: 4
Size: 252683 Color: 3

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 436093 Color: 2
Size: 288803 Color: 1
Size: 275105 Color: 3

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 436255 Color: 3
Size: 310256 Color: 4
Size: 253490 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 436272 Color: 0
Size: 292665 Color: 4
Size: 271064 Color: 4

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 436290 Color: 4
Size: 302477 Color: 4
Size: 261234 Color: 3

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 436344 Color: 1
Size: 307491 Color: 2
Size: 256166 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 436418 Color: 0
Size: 309199 Color: 1
Size: 254384 Color: 4

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 436419 Color: 1
Size: 291580 Color: 0
Size: 272002 Color: 3

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 436491 Color: 3
Size: 309121 Color: 2
Size: 254389 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 436509 Color: 2
Size: 292666 Color: 0
Size: 270826 Color: 3

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 436620 Color: 2
Size: 297024 Color: 3
Size: 266357 Color: 1

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 436670 Color: 3
Size: 296860 Color: 0
Size: 266471 Color: 2

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 436735 Color: 4
Size: 306149 Color: 3
Size: 257117 Color: 4

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 436764 Color: 1
Size: 283216 Color: 3
Size: 280021 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 436765 Color: 0
Size: 312839 Color: 4
Size: 250397 Color: 3

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 436799 Color: 3
Size: 301052 Color: 2
Size: 262150 Color: 3

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 436905 Color: 2
Size: 283955 Color: 0
Size: 279141 Color: 1

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 436957 Color: 1
Size: 303929 Color: 3
Size: 259115 Color: 4

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 437071 Color: 4
Size: 296249 Color: 3
Size: 266681 Color: 3

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 437135 Color: 2
Size: 295966 Color: 0
Size: 266900 Color: 3

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 437159 Color: 2
Size: 288147 Color: 4
Size: 274695 Color: 1

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 437351 Color: 0
Size: 308021 Color: 3
Size: 254629 Color: 3

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 437290 Color: 1
Size: 304455 Color: 3
Size: 258256 Color: 4

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 437344 Color: 3
Size: 309827 Color: 0
Size: 252830 Color: 2

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 437369 Color: 2
Size: 301566 Color: 2
Size: 261066 Color: 1

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 437532 Color: 0
Size: 294713 Color: 4
Size: 267756 Color: 2

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 437528 Color: 4
Size: 303936 Color: 3
Size: 258537 Color: 1

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 437558 Color: 3
Size: 304960 Color: 1
Size: 257483 Color: 1

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 437596 Color: 2
Size: 289299 Color: 3
Size: 273106 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 437664 Color: 1
Size: 308090 Color: 3
Size: 254247 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 437710 Color: 3
Size: 309861 Color: 1
Size: 252430 Color: 4

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 437806 Color: 3
Size: 289517 Color: 3
Size: 272678 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 437847 Color: 4
Size: 282499 Color: 1
Size: 279655 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 437890 Color: 2
Size: 299220 Color: 3
Size: 262891 Color: 1

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 437996 Color: 0
Size: 310092 Color: 4
Size: 251913 Color: 4

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 437950 Color: 1
Size: 299843 Color: 1
Size: 262208 Color: 4

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 437962 Color: 3
Size: 305788 Color: 0
Size: 256251 Color: 2

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 437983 Color: 2
Size: 292697 Color: 3
Size: 269321 Color: 4

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 438217 Color: 0
Size: 308745 Color: 1
Size: 253039 Color: 3

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 438233 Color: 0
Size: 291251 Color: 3
Size: 270517 Color: 1

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 438098 Color: 2
Size: 299193 Color: 1
Size: 262710 Color: 2

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 438219 Color: 2
Size: 300080 Color: 0
Size: 261702 Color: 1

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 438300 Color: 3
Size: 311552 Color: 2
Size: 250149 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 438361 Color: 4
Size: 303961 Color: 4
Size: 257679 Color: 3

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 438375 Color: 1
Size: 289698 Color: 0
Size: 271928 Color: 4

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 438387 Color: 1
Size: 296543 Color: 0
Size: 265071 Color: 4

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 438396 Color: 2
Size: 311189 Color: 4
Size: 250416 Color: 1

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 438477 Color: 1
Size: 288603 Color: 4
Size: 272921 Color: 0

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 438493 Color: 3
Size: 284325 Color: 1
Size: 277183 Color: 4

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 438653 Color: 1
Size: 307410 Color: 2
Size: 253938 Color: 4

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 438808 Color: 3
Size: 310448 Color: 4
Size: 250745 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 438851 Color: 3
Size: 306883 Color: 3
Size: 254267 Color: 2

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 438912 Color: 4
Size: 290377 Color: 0
Size: 270712 Color: 3

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 439069 Color: 3
Size: 306274 Color: 0
Size: 254658 Color: 3

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 439173 Color: 4
Size: 303482 Color: 3
Size: 257346 Color: 2

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 439177 Color: 2
Size: 290426 Color: 4
Size: 270398 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 439407 Color: 0
Size: 284079 Color: 3
Size: 276515 Color: 4

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 439473 Color: 0
Size: 290429 Color: 4
Size: 270099 Color: 2

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 439356 Color: 2
Size: 302109 Color: 3
Size: 258536 Color: 3

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 439508 Color: 3
Size: 282091 Color: 1
Size: 278402 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 439577 Color: 4
Size: 286689 Color: 0
Size: 273735 Color: 1

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 439589 Color: 4
Size: 297699 Color: 1
Size: 262713 Color: 1

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 439594 Color: 1
Size: 295677 Color: 0
Size: 264730 Color: 3

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 439708 Color: 0
Size: 303104 Color: 4
Size: 257189 Color: 1

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 439682 Color: 3
Size: 281077 Color: 0
Size: 279242 Color: 4

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 439782 Color: 3
Size: 293606 Color: 0
Size: 266613 Color: 3

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 439785 Color: 1
Size: 284395 Color: 2
Size: 275821 Color: 3

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 439807 Color: 3
Size: 309538 Color: 0
Size: 250656 Color: 1

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 439878 Color: 3
Size: 282757 Color: 1
Size: 277366 Color: 3

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 439914 Color: 4
Size: 284348 Color: 0
Size: 275739 Color: 1

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 439919 Color: 0
Size: 310061 Color: 4
Size: 250021 Color: 2

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 439945 Color: 4
Size: 304119 Color: 1
Size: 255937 Color: 1

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 439950 Color: 1
Size: 288879 Color: 0
Size: 271172 Color: 3

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 439950 Color: 3
Size: 303426 Color: 4
Size: 256625 Color: 2

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 439953 Color: 1
Size: 285303 Color: 0
Size: 274745 Color: 1

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 440077 Color: 0
Size: 297555 Color: 3
Size: 262369 Color: 2

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 440019 Color: 4
Size: 307159 Color: 4
Size: 252823 Color: 1

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 440327 Color: 3
Size: 288510 Color: 4
Size: 271164 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 440373 Color: 1
Size: 292328 Color: 3
Size: 267300 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 440419 Color: 2
Size: 284394 Color: 4
Size: 275188 Color: 4

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 440514 Color: 4
Size: 291998 Color: 0
Size: 267489 Color: 1

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 440607 Color: 4
Size: 288433 Color: 3
Size: 270961 Color: 1

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 440614 Color: 1
Size: 296571 Color: 0
Size: 262816 Color: 2

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 440861 Color: 0
Size: 297243 Color: 4
Size: 261897 Color: 2

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 440910 Color: 0
Size: 294449 Color: 1
Size: 264642 Color: 3

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 440759 Color: 3
Size: 299205 Color: 1
Size: 260037 Color: 3

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 440821 Color: 2
Size: 296415 Color: 0
Size: 262765 Color: 4

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 440878 Color: 3
Size: 302784 Color: 1
Size: 256339 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 441150 Color: 0
Size: 303642 Color: 1
Size: 255209 Color: 1

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 441175 Color: 4
Size: 306235 Color: 2
Size: 252591 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 441155 Color: 0
Size: 294367 Color: 1
Size: 264479 Color: 1

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 441231 Color: 4
Size: 283174 Color: 4
Size: 275596 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 441240 Color: 3
Size: 291878 Color: 2
Size: 266883 Color: 4

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 441287 Color: 2
Size: 294590 Color: 3
Size: 264124 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 441303 Color: 1
Size: 307396 Color: 4
Size: 251302 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 441392 Color: 3
Size: 298478 Color: 0
Size: 260131 Color: 2

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 441404 Color: 3
Size: 299113 Color: 1
Size: 259484 Color: 3

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 441479 Color: 1
Size: 294654 Color: 2
Size: 263868 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 441484 Color: 4
Size: 281720 Color: 4
Size: 276797 Color: 2

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 441538 Color: 4
Size: 302258 Color: 4
Size: 256205 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 441517 Color: 0
Size: 284590 Color: 4
Size: 273894 Color: 3

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 441761 Color: 1
Size: 294640 Color: 4
Size: 263600 Color: 4

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 441900 Color: 3
Size: 306768 Color: 1
Size: 251333 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 441904 Color: 4
Size: 305872 Color: 4
Size: 252225 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 442044 Color: 1
Size: 282494 Color: 0
Size: 275463 Color: 3

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 442041 Color: 0
Size: 290163 Color: 2
Size: 267797 Color: 4

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 442173 Color: 1
Size: 297098 Color: 4
Size: 260730 Color: 2

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 442263 Color: 1
Size: 282127 Color: 2
Size: 275611 Color: 1

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 442286 Color: 3
Size: 292080 Color: 0
Size: 265635 Color: 3

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 442774 Color: 4
Size: 299085 Color: 4
Size: 258142 Color: 0

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 442826 Color: 3
Size: 305481 Color: 4
Size: 251694 Color: 3

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 442890 Color: 2
Size: 305322 Color: 0
Size: 251789 Color: 3

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 442985 Color: 0
Size: 295826 Color: 2
Size: 261190 Color: 2

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 442890 Color: 3
Size: 285788 Color: 1
Size: 271323 Color: 3

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 443095 Color: 0
Size: 305064 Color: 1
Size: 251842 Color: 1

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 443003 Color: 4
Size: 286488 Color: 2
Size: 270510 Color: 3

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 443043 Color: 4
Size: 299064 Color: 3
Size: 257894 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 443111 Color: 4
Size: 306852 Color: 3
Size: 250038 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 443156 Color: 2
Size: 291507 Color: 1
Size: 265338 Color: 1

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 443206 Color: 0
Size: 281029 Color: 1
Size: 275766 Color: 4

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 443303 Color: 1
Size: 288334 Color: 1
Size: 268364 Color: 2

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 443583 Color: 0
Size: 283580 Color: 2
Size: 272838 Color: 3

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 443346 Color: 1
Size: 300438 Color: 4
Size: 256217 Color: 4

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 443572 Color: 3
Size: 296121 Color: 0
Size: 260308 Color: 1

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 443631 Color: 0
Size: 303314 Color: 2
Size: 253056 Color: 2

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 443635 Color: 3
Size: 285159 Color: 1
Size: 271207 Color: 3

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 443642 Color: 3
Size: 285872 Color: 2
Size: 270487 Color: 4

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 443663 Color: 4
Size: 284855 Color: 0
Size: 271483 Color: 3

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 443749 Color: 0
Size: 289789 Color: 2
Size: 266463 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 443743 Color: 1
Size: 303665 Color: 1
Size: 252593 Color: 2

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 443871 Color: 0
Size: 303108 Color: 1
Size: 253022 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 443800 Color: 2
Size: 280292 Color: 1
Size: 275909 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 443941 Color: 2
Size: 288206 Color: 0
Size: 267854 Color: 1

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 444109 Color: 2
Size: 305384 Color: 3
Size: 250508 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 444125 Color: 3
Size: 287961 Color: 0
Size: 267915 Color: 4

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 444127 Color: 2
Size: 305275 Color: 1
Size: 250599 Color: 4

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 444331 Color: 4
Size: 279139 Color: 3
Size: 276531 Color: 3

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 444386 Color: 3
Size: 294919 Color: 1
Size: 260696 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 444624 Color: 3
Size: 288412 Color: 0
Size: 266965 Color: 3

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 444739 Color: 4
Size: 277693 Color: 0
Size: 277569 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 444760 Color: 0
Size: 278876 Color: 3
Size: 276365 Color: 2

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 444977 Color: 3
Size: 278593 Color: 0
Size: 276431 Color: 3

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 444987 Color: 1
Size: 290722 Color: 4
Size: 264292 Color: 4

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 444992 Color: 4
Size: 292864 Color: 2
Size: 262145 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 445007 Color: 1
Size: 297518 Color: 1
Size: 257476 Color: 2

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 445238 Color: 0
Size: 298787 Color: 2
Size: 255976 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 445443 Color: 0
Size: 296265 Color: 1
Size: 258293 Color: 1

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 445338 Color: 2
Size: 303481 Color: 1
Size: 251182 Color: 2

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 445377 Color: 4
Size: 280858 Color: 0
Size: 273766 Color: 4

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 445521 Color: 0
Size: 299874 Color: 3
Size: 254606 Color: 1

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 445403 Color: 4
Size: 281445 Color: 2
Size: 273153 Color: 3

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 445527 Color: 1
Size: 296959 Color: 0
Size: 257515 Color: 2

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 445614 Color: 0
Size: 299102 Color: 4
Size: 255285 Color: 3

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 445611 Color: 1
Size: 304192 Color: 4
Size: 250198 Color: 1

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 445700 Color: 4
Size: 277286 Color: 0
Size: 277015 Color: 1

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 445754 Color: 3
Size: 279025 Color: 0
Size: 275222 Color: 4

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 445793 Color: 2
Size: 283277 Color: 2
Size: 270931 Color: 4

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 445896 Color: 4
Size: 291086 Color: 1
Size: 263019 Color: 4

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 445964 Color: 0
Size: 277023 Color: 2
Size: 277014 Color: 1

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 445920 Color: 4
Size: 297216 Color: 0
Size: 256865 Color: 1

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 446093 Color: 3
Size: 286683 Color: 3
Size: 267225 Color: 2

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 446073 Color: 0
Size: 293406 Color: 3
Size: 260522 Color: 3

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 446116 Color: 2
Size: 283795 Color: 4
Size: 270090 Color: 2

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 446119 Color: 4
Size: 284365 Color: 0
Size: 269517 Color: 1

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 446200 Color: 3
Size: 299639 Color: 4
Size: 254162 Color: 3

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 446276 Color: 2
Size: 296828 Color: 0
Size: 256897 Color: 2

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 446414 Color: 0
Size: 291957 Color: 3
Size: 261630 Color: 4

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 446443 Color: 0
Size: 301431 Color: 2
Size: 252127 Color: 2

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 446374 Color: 2
Size: 299846 Color: 4
Size: 253781 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 446381 Color: 4
Size: 296514 Color: 2
Size: 257106 Color: 4

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 446572 Color: 0
Size: 303252 Color: 3
Size: 250177 Color: 4

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 446609 Color: 4
Size: 289319 Color: 0
Size: 264073 Color: 3

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 446672 Color: 1
Size: 297730 Color: 3
Size: 255599 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 446774 Color: 3
Size: 287618 Color: 3
Size: 265609 Color: 4

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 446810 Color: 2
Size: 276878 Color: 3
Size: 276313 Color: 2

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 446925 Color: 2
Size: 292195 Color: 0
Size: 260881 Color: 4

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 446966 Color: 2
Size: 300655 Color: 0
Size: 252380 Color: 3

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 446989 Color: 4
Size: 284498 Color: 2
Size: 268514 Color: 3

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 447048 Color: 1
Size: 300480 Color: 3
Size: 252473 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 447079 Color: 2
Size: 301568 Color: 1
Size: 251354 Color: 2

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 447103 Color: 1
Size: 280228 Color: 4
Size: 272670 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 447119 Color: 3
Size: 278902 Color: 4
Size: 273980 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 447203 Color: 4
Size: 278039 Color: 0
Size: 274759 Color: 1

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 447233 Color: 3
Size: 277176 Color: 2
Size: 275592 Color: 1

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 447348 Color: 3
Size: 293750 Color: 4
Size: 258903 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 468007 Color: 2
Size: 268869 Color: 1
Size: 263125 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 447641 Color: 2
Size: 286927 Color: 3
Size: 265433 Color: 3

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 447759 Color: 0
Size: 285564 Color: 3
Size: 266678 Color: 1

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 447765 Color: 4
Size: 284606 Color: 4
Size: 267630 Color: 1

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 447780 Color: 4
Size: 277183 Color: 0
Size: 275038 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 447959 Color: 0
Size: 296053 Color: 4
Size: 255989 Color: 1

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 448041 Color: 0
Size: 293436 Color: 1
Size: 258524 Color: 1

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 447999 Color: 2
Size: 282255 Color: 2
Size: 269747 Color: 3

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 448172 Color: 1
Size: 289122 Color: 2
Size: 262707 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 448227 Color: 1
Size: 291820 Color: 0
Size: 259954 Color: 3

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 448302 Color: 4
Size: 301661 Color: 3
Size: 250038 Color: 2

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 448449 Color: 0
Size: 285416 Color: 3
Size: 266136 Color: 3

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 448523 Color: 2
Size: 284598 Color: 3
Size: 266880 Color: 4

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 448640 Color: 3
Size: 287407 Color: 0
Size: 263954 Color: 4

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 448707 Color: 0
Size: 293616 Color: 3
Size: 257678 Color: 4

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 448666 Color: 2
Size: 281731 Color: 1
Size: 269604 Color: 1

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 448738 Color: 1
Size: 283700 Color: 1
Size: 267563 Color: 3

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 448771 Color: 0
Size: 292820 Color: 3
Size: 258410 Color: 2

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 448784 Color: 4
Size: 295023 Color: 0
Size: 256194 Color: 1

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 448867 Color: 4
Size: 292596 Color: 0
Size: 258538 Color: 1

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 448903 Color: 0
Size: 283218 Color: 2
Size: 267880 Color: 3

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 448937 Color: 2
Size: 279017 Color: 1
Size: 272047 Color: 4

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 448985 Color: 4
Size: 300131 Color: 0
Size: 250885 Color: 3

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 449020 Color: 1
Size: 298390 Color: 4
Size: 252591 Color: 4

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 449042 Color: 2
Size: 275982 Color: 4
Size: 274977 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 449138 Color: 4
Size: 299842 Color: 3
Size: 251021 Color: 4

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 449354 Color: 2
Size: 296578 Color: 0
Size: 254069 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 449560 Color: 1
Size: 288606 Color: 3
Size: 261835 Color: 3

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 449571 Color: 2
Size: 285169 Color: 0
Size: 265261 Color: 1

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 449612 Color: 1
Size: 281290 Color: 0
Size: 269099 Color: 4

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 449631 Color: 2
Size: 276848 Color: 3
Size: 273522 Color: 3

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 449753 Color: 4
Size: 286607 Color: 2
Size: 263641 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 449819 Color: 2
Size: 282250 Color: 1
Size: 267932 Color: 3

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 449827 Color: 1
Size: 280153 Color: 0
Size: 270021 Color: 4

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 449832 Color: 3
Size: 293264 Color: 4
Size: 256905 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 450250 Color: 2
Size: 275304 Color: 1
Size: 274447 Color: 3

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 450256 Color: 2
Size: 294259 Color: 0
Size: 255486 Color: 4

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 450281 Color: 1
Size: 285211 Color: 4
Size: 264509 Color: 3

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 450298 Color: 1
Size: 292061 Color: 4
Size: 257642 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 450347 Color: 4
Size: 280816 Color: 4
Size: 268838 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 450377 Color: 4
Size: 275437 Color: 4
Size: 274187 Color: 1

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 450528 Color: 0
Size: 288078 Color: 1
Size: 261395 Color: 2

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 450498 Color: 4
Size: 281812 Color: 2
Size: 267691 Color: 2

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 450635 Color: 3
Size: 293648 Color: 3
Size: 255718 Color: 2

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 450571 Color: 2
Size: 276785 Color: 4
Size: 272645 Color: 1

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 450659 Color: 4
Size: 288063 Color: 2
Size: 261279 Color: 3

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 450806 Color: 2
Size: 286829 Color: 3
Size: 262366 Color: 4

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 450828 Color: 1
Size: 282935 Color: 4
Size: 266238 Color: 2

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 450838 Color: 1
Size: 289335 Color: 0
Size: 259828 Color: 4

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 450907 Color: 0
Size: 293905 Color: 3
Size: 255189 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 450914 Color: 0
Size: 292398 Color: 2
Size: 256689 Color: 2

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 450935 Color: 0
Size: 281046 Color: 0
Size: 268020 Color: 3

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 450945 Color: 4
Size: 277570 Color: 1
Size: 271486 Color: 3

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 451006 Color: 4
Size: 281047 Color: 0
Size: 267948 Color: 3

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 451009 Color: 2
Size: 296795 Color: 0
Size: 252197 Color: 3

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 451051 Color: 4
Size: 297780 Color: 1
Size: 251170 Color: 2

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 451197 Color: 1
Size: 277873 Color: 0
Size: 270931 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 451237 Color: 3
Size: 280696 Color: 4
Size: 268068 Color: 2

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 451480 Color: 2
Size: 298190 Color: 4
Size: 250331 Color: 3

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 451340 Color: 3
Size: 283360 Color: 1
Size: 265301 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 451494 Color: 1
Size: 281019 Color: 2
Size: 267488 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 451570 Color: 1
Size: 293064 Color: 2
Size: 255367 Color: 4

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 451581 Color: 4
Size: 291682 Color: 2
Size: 256738 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 451622 Color: 4
Size: 289625 Color: 1
Size: 258754 Color: 1

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 451667 Color: 0
Size: 287851 Color: 3
Size: 260483 Color: 2

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 451766 Color: 2
Size: 293879 Color: 1
Size: 254356 Color: 4

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 451970 Color: 3
Size: 275010 Color: 0
Size: 273021 Color: 2

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 452035 Color: 0
Size: 279716 Color: 4
Size: 268250 Color: 2

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 452029 Color: 1
Size: 275388 Color: 0
Size: 272584 Color: 1

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 452041 Color: 3
Size: 291183 Color: 3
Size: 256777 Color: 2

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 452177 Color: 0
Size: 279553 Color: 1
Size: 268271 Color: 3

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 452080 Color: 1
Size: 291455 Color: 2
Size: 256466 Color: 2

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 452349 Color: 0
Size: 291409 Color: 1
Size: 256243 Color: 1

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 452324 Color: 4
Size: 276475 Color: 3
Size: 271202 Color: 1

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 452421 Color: 2
Size: 280630 Color: 2
Size: 266950 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 452472 Color: 0
Size: 294576 Color: 3
Size: 252953 Color: 3

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 452430 Color: 1
Size: 274810 Color: 2
Size: 272761 Color: 2

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 452462 Color: 4
Size: 274062 Color: 0
Size: 273477 Color: 2

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 452517 Color: 3
Size: 276140 Color: 2
Size: 271344 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 452622 Color: 4
Size: 290220 Color: 2
Size: 257159 Color: 3

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 452667 Color: 4
Size: 281440 Color: 0
Size: 265894 Color: 2

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 452791 Color: 2
Size: 290327 Color: 0
Size: 256883 Color: 4

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 452823 Color: 0
Size: 288276 Color: 4
Size: 258902 Color: 1

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 452825 Color: 3
Size: 282947 Color: 2
Size: 264229 Color: 3

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 453013 Color: 2
Size: 275945 Color: 3
Size: 271043 Color: 1

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 453013 Color: 2
Size: 292077 Color: 3
Size: 254911 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 453080 Color: 3
Size: 285123 Color: 1
Size: 261798 Color: 4

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 453191 Color: 2
Size: 281884 Color: 4
Size: 264926 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 453225 Color: 1
Size: 286417 Color: 0
Size: 260359 Color: 4

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 453372 Color: 3
Size: 293348 Color: 4
Size: 253281 Color: 3

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 453416 Color: 0
Size: 289534 Color: 4
Size: 257051 Color: 1

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 453668 Color: 2
Size: 288219 Color: 4
Size: 258114 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 453624 Color: 3
Size: 284849 Color: 4
Size: 261528 Color: 1

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 453771 Color: 1
Size: 283509 Color: 0
Size: 262721 Color: 4

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 453787 Color: 2
Size: 275998 Color: 4
Size: 270216 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 453806 Color: 3
Size: 278184 Color: 2
Size: 268011 Color: 4

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 453914 Color: 0
Size: 292067 Color: 3
Size: 254020 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 453956 Color: 4
Size: 277869 Color: 2
Size: 268176 Color: 1

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 454095 Color: 1
Size: 275928 Color: 0
Size: 269978 Color: 2

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 454188 Color: 3
Size: 290819 Color: 4
Size: 254994 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 454231 Color: 0
Size: 273291 Color: 1
Size: 272479 Color: 1

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 454249 Color: 2
Size: 287849 Color: 3
Size: 257903 Color: 1

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 454253 Color: 2
Size: 273648 Color: 0
Size: 272100 Color: 4

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 454420 Color: 0
Size: 291162 Color: 3
Size: 254419 Color: 2

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 454364 Color: 2
Size: 293443 Color: 2
Size: 252194 Color: 1

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 454437 Color: 2
Size: 280957 Color: 2
Size: 264607 Color: 3

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 454552 Color: 3
Size: 282176 Color: 1
Size: 263273 Color: 2

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 454455 Color: 1
Size: 291322 Color: 1
Size: 254224 Color: 2

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 454594 Color: 4
Size: 273424 Color: 4
Size: 271983 Color: 3

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 454690 Color: 0
Size: 293481 Color: 3
Size: 251830 Color: 4

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 454748 Color: 1
Size: 283141 Color: 2
Size: 262112 Color: 3

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 454803 Color: 4
Size: 285436 Color: 0
Size: 259762 Color: 2

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 454932 Color: 3
Size: 276179 Color: 0
Size: 268890 Color: 2

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 455054 Color: 1
Size: 279849 Color: 4
Size: 265098 Color: 4

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 455087 Color: 4
Size: 286377 Color: 3
Size: 258537 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 455106 Color: 2
Size: 285141 Color: 1
Size: 259754 Color: 3

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 455206 Color: 2
Size: 280655 Color: 1
Size: 264140 Color: 1

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 455211 Color: 0
Size: 278701 Color: 4
Size: 266089 Color: 3

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 455240 Color: 1
Size: 286559 Color: 2
Size: 258202 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 455469 Color: 1
Size: 288577 Color: 3
Size: 255955 Color: 4

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 455496 Color: 2
Size: 277475 Color: 0
Size: 267030 Color: 1

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 455517 Color: 1
Size: 282594 Color: 4
Size: 261890 Color: 3

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 455571 Color: 4
Size: 278542 Color: 2
Size: 265888 Color: 2

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 455675 Color: 4
Size: 288095 Color: 3
Size: 256231 Color: 4

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 455761 Color: 2
Size: 285699 Color: 3
Size: 258541 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 455812 Color: 2
Size: 290903 Color: 3
Size: 253286 Color: 1

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 455839 Color: 2
Size: 280330 Color: 0
Size: 263832 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 455946 Color: 2
Size: 293492 Color: 2
Size: 250563 Color: 4

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 456037 Color: 2
Size: 278627 Color: 4
Size: 265337 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 456058 Color: 3
Size: 288943 Color: 4
Size: 255000 Color: 4

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 456247 Color: 2
Size: 274174 Color: 3
Size: 269580 Color: 1

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 456289 Color: 0
Size: 291252 Color: 4
Size: 252460 Color: 3

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 456348 Color: 2
Size: 276252 Color: 0
Size: 267401 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 456376 Color: 1
Size: 280563 Color: 2
Size: 263062 Color: 3

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 456437 Color: 1
Size: 274125 Color: 1
Size: 269439 Color: 2

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 456460 Color: 1
Size: 273230 Color: 2
Size: 270311 Color: 4

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 456590 Color: 4
Size: 282040 Color: 0
Size: 261371 Color: 3

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 456602 Color: 4
Size: 280494 Color: 1
Size: 262905 Color: 1

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 456738 Color: 3
Size: 292703 Color: 2
Size: 250560 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 456760 Color: 3
Size: 276414 Color: 1
Size: 266827 Color: 4

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 456826 Color: 4
Size: 291104 Color: 2
Size: 252071 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 456847 Color: 1
Size: 287506 Color: 3
Size: 255648 Color: 1

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 456920 Color: 2
Size: 292851 Color: 2
Size: 250230 Color: 4

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 456938 Color: 0
Size: 283838 Color: 4
Size: 259225 Color: 3

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 457064 Color: 0
Size: 278914 Color: 2
Size: 264023 Color: 4

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 457143 Color: 0
Size: 277187 Color: 2
Size: 265671 Color: 4

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 457210 Color: 4
Size: 287154 Color: 0
Size: 255637 Color: 3

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 457263 Color: 2
Size: 284202 Color: 0
Size: 258536 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 457505 Color: 3
Size: 286894 Color: 1
Size: 255602 Color: 4

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 457398 Color: 4
Size: 275378 Color: 2
Size: 267225 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 457638 Color: 1
Size: 279444 Color: 2
Size: 262919 Color: 4

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 457679 Color: 2
Size: 274693 Color: 3
Size: 267629 Color: 4

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 457726 Color: 3
Size: 277980 Color: 0
Size: 264295 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 457770 Color: 1
Size: 279642 Color: 4
Size: 262589 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 457786 Color: 0
Size: 283588 Color: 2
Size: 258627 Color: 3

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 457828 Color: 2
Size: 277594 Color: 4
Size: 264579 Color: 1

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 458207 Color: 3
Size: 280946 Color: 4
Size: 260848 Color: 4

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 458121 Color: 1
Size: 285789 Color: 0
Size: 256091 Color: 2

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 458273 Color: 2
Size: 278469 Color: 0
Size: 263259 Color: 3

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 458331 Color: 0
Size: 281063 Color: 3
Size: 260607 Color: 4

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 458357 Color: 1
Size: 286269 Color: 4
Size: 255375 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 458404 Color: 4
Size: 275115 Color: 4
Size: 266482 Color: 3

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 458489 Color: 4
Size: 288350 Color: 0
Size: 253162 Color: 1

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 458647 Color: 4
Size: 288724 Color: 3
Size: 252630 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 458696 Color: 1
Size: 283977 Color: 1
Size: 257328 Color: 4

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 458896 Color: 4
Size: 289246 Color: 1
Size: 251859 Color: 2

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 459093 Color: 3
Size: 289065 Color: 1
Size: 251843 Color: 4

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 459054 Color: 2
Size: 273354 Color: 1
Size: 267593 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 459306 Color: 0
Size: 284556 Color: 3
Size: 256139 Color: 2

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 459370 Color: 2
Size: 290419 Color: 3
Size: 250212 Color: 4

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 459395 Color: 0
Size: 280764 Color: 1
Size: 259842 Color: 1

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 459418 Color: 0
Size: 284128 Color: 3
Size: 256455 Color: 2

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 459471 Color: 2
Size: 284505 Color: 4
Size: 256025 Color: 1

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 459732 Color: 4
Size: 289558 Color: 3
Size: 250711 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 459739 Color: 0
Size: 284471 Color: 2
Size: 255791 Color: 2

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 459790 Color: 1
Size: 271120 Color: 3
Size: 269091 Color: 2

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 459801 Color: 2
Size: 277677 Color: 4
Size: 262523 Color: 4

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 459907 Color: 3
Size: 272166 Color: 0
Size: 267928 Color: 2

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 459876 Color: 4
Size: 282880 Color: 0
Size: 257245 Color: 2

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 459889 Color: 2
Size: 281729 Color: 1
Size: 258383 Color: 3

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 459970 Color: 3
Size: 282288 Color: 1
Size: 257743 Color: 4

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 459948 Color: 2
Size: 286423 Color: 1
Size: 253630 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 460000 Color: 4
Size: 284925 Color: 3
Size: 255076 Color: 1

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 460047 Color: 1
Size: 282826 Color: 3
Size: 257128 Color: 4

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 460074 Color: 0
Size: 279556 Color: 1
Size: 260371 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 460155 Color: 2
Size: 286643 Color: 0
Size: 253203 Color: 3

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 460211 Color: 2
Size: 274027 Color: 1
Size: 265763 Color: 1

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 460297 Color: 0
Size: 278730 Color: 4
Size: 260974 Color: 3

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 460375 Color: 2
Size: 272664 Color: 0
Size: 266962 Color: 4

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 460415 Color: 0
Size: 286254 Color: 2
Size: 253332 Color: 3

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 460570 Color: 3
Size: 286117 Color: 1
Size: 253314 Color: 3

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 460570 Color: 0
Size: 283441 Color: 1
Size: 255990 Color: 2

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 460624 Color: 1
Size: 271757 Color: 4
Size: 267620 Color: 1

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 460643 Color: 3
Size: 275196 Color: 2
Size: 264162 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 460762 Color: 0
Size: 284872 Color: 3
Size: 254367 Color: 2

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 460810 Color: 2
Size: 287728 Color: 1
Size: 251463 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 460979 Color: 3
Size: 286607 Color: 1
Size: 252415 Color: 2

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 461049 Color: 3
Size: 281618 Color: 0
Size: 257334 Color: 1

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 461054 Color: 1
Size: 274104 Color: 3
Size: 264843 Color: 2

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 461164 Color: 3
Size: 276757 Color: 1
Size: 262080 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 461059 Color: 4
Size: 277198 Color: 0
Size: 261744 Color: 2

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 461294 Color: 3
Size: 283388 Color: 4
Size: 255319 Color: 4

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 461332 Color: 1
Size: 283356 Color: 4
Size: 255313 Color: 4

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 461493 Color: 3
Size: 281589 Color: 2
Size: 256919 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 461783 Color: 0
Size: 283371 Color: 3
Size: 254847 Color: 4

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 461809 Color: 2
Size: 283671 Color: 1
Size: 254521 Color: 2

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 461818 Color: 0
Size: 276303 Color: 3
Size: 261880 Color: 4

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 461830 Color: 3
Size: 279041 Color: 1
Size: 259130 Color: 2

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 461839 Color: 2
Size: 279259 Color: 1
Size: 258903 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 461876 Color: 4
Size: 273516 Color: 3
Size: 264609 Color: 4

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 462021 Color: 4
Size: 273975 Color: 3
Size: 264005 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 462081 Color: 2
Size: 276239 Color: 2
Size: 261681 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 462108 Color: 0
Size: 284181 Color: 3
Size: 253712 Color: 4

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 462116 Color: 1
Size: 285443 Color: 3
Size: 252442 Color: 4

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 462443 Color: 2
Size: 273186 Color: 4
Size: 264372 Color: 3

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 462504 Color: 1
Size: 286533 Color: 3
Size: 250964 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 462670 Color: 1
Size: 271272 Color: 1
Size: 266059 Color: 4

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 462859 Color: 3
Size: 275021 Color: 2
Size: 262121 Color: 2

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 462868 Color: 2
Size: 270511 Color: 3
Size: 266622 Color: 4

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 462943 Color: 1
Size: 286106 Color: 2
Size: 250952 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 463124 Color: 3
Size: 280287 Color: 2
Size: 256590 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 463034 Color: 0
Size: 275887 Color: 2
Size: 261080 Color: 2

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 463078 Color: 2
Size: 280046 Color: 1
Size: 256877 Color: 3

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 463120 Color: 0
Size: 278084 Color: 4
Size: 258797 Color: 3

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 463200 Color: 4
Size: 271895 Color: 2
Size: 264906 Color: 3

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 463301 Color: 2
Size: 278985 Color: 3
Size: 257715 Color: 4

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 463373 Color: 0
Size: 280962 Color: 2
Size: 255666 Color: 1

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 463589 Color: 1
Size: 268673 Color: 2
Size: 267739 Color: 3

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 463634 Color: 4
Size: 278271 Color: 0
Size: 258096 Color: 2

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 463677 Color: 4
Size: 283044 Color: 0
Size: 253280 Color: 3

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 463711 Color: 1
Size: 276745 Color: 3
Size: 259545 Color: 1

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 463760 Color: 0
Size: 281620 Color: 2
Size: 254621 Color: 4

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 463934 Color: 3
Size: 268779 Color: 4
Size: 267288 Color: 1

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 463833 Color: 0
Size: 272419 Color: 4
Size: 263749 Color: 4

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 463951 Color: 2
Size: 273599 Color: 3
Size: 262451 Color: 4

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 464033 Color: 3
Size: 277743 Color: 1
Size: 258225 Color: 2

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 464011 Color: 2
Size: 283393 Color: 1
Size: 252597 Color: 4

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 464092 Color: 0
Size: 268545 Color: 2
Size: 267364 Color: 3

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 464206 Color: 4
Size: 277747 Color: 0
Size: 258048 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 464333 Color: 4
Size: 274551 Color: 0
Size: 261117 Color: 2

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 464337 Color: 0
Size: 279606 Color: 3
Size: 256058 Color: 1

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 464465 Color: 3
Size: 281997 Color: 0
Size: 253539 Color: 1

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 464392 Color: 4
Size: 281446 Color: 2
Size: 254163 Color: 2

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 464483 Color: 2
Size: 278752 Color: 3
Size: 256766 Color: 4

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 464588 Color: 1
Size: 277324 Color: 2
Size: 258089 Color: 1

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 464802 Color: 1
Size: 278135 Color: 3
Size: 257064 Color: 1

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 464947 Color: 3
Size: 282431 Color: 0
Size: 252623 Color: 1

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 464822 Color: 4
Size: 272460 Color: 0
Size: 262719 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 464983 Color: 3
Size: 273569 Color: 4
Size: 261449 Color: 2

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 464942 Color: 1
Size: 279333 Color: 4
Size: 255726 Color: 4

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 465031 Color: 4
Size: 280465 Color: 1
Size: 254505 Color: 1

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 465161 Color: 4
Size: 279747 Color: 3
Size: 255093 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 465213 Color: 1
Size: 280114 Color: 3
Size: 254674 Color: 4

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 465250 Color: 1
Size: 275464 Color: 1
Size: 259287 Color: 2

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 465453 Color: 3
Size: 272283 Color: 2
Size: 262265 Color: 4

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 465349 Color: 2
Size: 280785 Color: 1
Size: 253867 Color: 4

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 465479 Color: 0
Size: 273785 Color: 3
Size: 260737 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 465507 Color: 4
Size: 279199 Color: 3
Size: 255295 Color: 2

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 465563 Color: 2
Size: 274138 Color: 4
Size: 260300 Color: 4

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 465719 Color: 1
Size: 282843 Color: 3
Size: 251439 Color: 1

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 465850 Color: 3
Size: 279808 Color: 2
Size: 254343 Color: 1

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 465856 Color: 3
Size: 278765 Color: 0
Size: 255380 Color: 2

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 465836 Color: 2
Size: 271200 Color: 4
Size: 262965 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 465970 Color: 3
Size: 269348 Color: 2
Size: 264683 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 465908 Color: 1
Size: 278828 Color: 4
Size: 255265 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 466000 Color: 3
Size: 270601 Color: 0
Size: 263400 Color: 2

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 465944 Color: 4
Size: 280853 Color: 2
Size: 253204 Color: 4

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 466066 Color: 4
Size: 279006 Color: 0
Size: 254929 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 466148 Color: 0
Size: 270005 Color: 3
Size: 263848 Color: 1

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 466143 Color: 3
Size: 283715 Color: 0
Size: 250143 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 466218 Color: 4
Size: 272944 Color: 4
Size: 260839 Color: 1

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 466265 Color: 1
Size: 279309 Color: 0
Size: 254427 Color: 2

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 466497 Color: 3
Size: 281356 Color: 0
Size: 252148 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 466588 Color: 3
Size: 270506 Color: 4
Size: 262907 Color: 4

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 466592 Color: 4
Size: 278336 Color: 1
Size: 255073 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 466621 Color: 4
Size: 274032 Color: 3
Size: 259348 Color: 2

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 466772 Color: 0
Size: 278808 Color: 0
Size: 254421 Color: 3

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 467002 Color: 3
Size: 269830 Color: 1
Size: 263169 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 466920 Color: 0
Size: 276448 Color: 1
Size: 256633 Color: 2

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 467028 Color: 3
Size: 280753 Color: 1
Size: 252220 Color: 2

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 467038 Color: 3
Size: 281643 Color: 0
Size: 251320 Color: 4

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 466938 Color: 1
Size: 276870 Color: 0
Size: 256193 Color: 2

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 467052 Color: 3
Size: 282653 Color: 0
Size: 250296 Color: 1

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 467132 Color: 3
Size: 282744 Color: 4
Size: 250125 Color: 4

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 467191 Color: 3
Size: 273402 Color: 2
Size: 259408 Color: 1

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 467194 Color: 0
Size: 279601 Color: 1
Size: 253206 Color: 1

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 467341 Color: 1
Size: 279180 Color: 3
Size: 253480 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 467356 Color: 4
Size: 270340 Color: 2
Size: 262305 Color: 3

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 467366 Color: 3
Size: 280365 Color: 4
Size: 252270 Color: 2

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 467398 Color: 1
Size: 275049 Color: 0
Size: 257554 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 467443 Color: 1
Size: 272809 Color: 0
Size: 259749 Color: 3

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 467647 Color: 3
Size: 280199 Color: 2
Size: 252155 Color: 1

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 467749 Color: 3
Size: 268143 Color: 2
Size: 264109 Color: 4

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 467667 Color: 2
Size: 280157 Color: 4
Size: 252177 Color: 1

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 467746 Color: 0
Size: 276027 Color: 3
Size: 256228 Color: 3

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 467832 Color: 0
Size: 266703 Color: 2
Size: 265466 Color: 3

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 467839 Color: 2
Size: 267379 Color: 0
Size: 264783 Color: 4

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 468043 Color: 1
Size: 280741 Color: 2
Size: 251217 Color: 3

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 468055 Color: 1
Size: 276901 Color: 3
Size: 255045 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 468142 Color: 2
Size: 274615 Color: 0
Size: 257244 Color: 3

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 468169 Color: 4
Size: 272113 Color: 3
Size: 259719 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 468233 Color: 0
Size: 268885 Color: 1
Size: 262883 Color: 4

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 468344 Color: 3
Size: 278974 Color: 2
Size: 252683 Color: 2

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 468242 Color: 4
Size: 274137 Color: 1
Size: 257622 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 468315 Color: 4
Size: 273977 Color: 3
Size: 257709 Color: 1

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 468456 Color: 3
Size: 270456 Color: 2
Size: 261089 Color: 2

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 468402 Color: 4
Size: 269188 Color: 1
Size: 262411 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 468440 Color: 4
Size: 273532 Color: 0
Size: 258029 Color: 2

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 468528 Color: 0
Size: 280625 Color: 0
Size: 250848 Color: 3

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 468640 Color: 3
Size: 273438 Color: 0
Size: 257923 Color: 1

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 468547 Color: 1
Size: 279909 Color: 1
Size: 251545 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 468645 Color: 3
Size: 276933 Color: 4
Size: 254423 Color: 1

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 468715 Color: 0
Size: 270877 Color: 4
Size: 260409 Color: 3

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 468848 Color: 3
Size: 266449 Color: 0
Size: 264704 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 468882 Color: 2
Size: 275449 Color: 3
Size: 255670 Color: 4

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 468949 Color: 2
Size: 268970 Color: 3
Size: 262082 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 469045 Color: 2
Size: 280380 Color: 1
Size: 250576 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 469062 Color: 1
Size: 269069 Color: 3
Size: 261870 Color: 4

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 469096 Color: 0
Size: 268200 Color: 1
Size: 262705 Color: 1

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 469338 Color: 1
Size: 271567 Color: 3
Size: 259096 Color: 4

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 469418 Color: 3
Size: 273840 Color: 2
Size: 256743 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 469488 Color: 1
Size: 272183 Color: 1
Size: 258330 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 469529 Color: 1
Size: 273489 Color: 1
Size: 256983 Color: 2

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 469662 Color: 4
Size: 273885 Color: 3
Size: 256454 Color: 2

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 469710 Color: 3
Size: 280001 Color: 2
Size: 250290 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 469679 Color: 0
Size: 265358 Color: 4
Size: 264964 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 469722 Color: 2
Size: 265457 Color: 4
Size: 264822 Color: 3

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 469854 Color: 0
Size: 274993 Color: 0
Size: 255154 Color: 4

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 469861 Color: 1
Size: 274818 Color: 3
Size: 255322 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 469979 Color: 3
Size: 270114 Color: 2
Size: 259908 Color: 2

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 470001 Color: 1
Size: 266669 Color: 0
Size: 263331 Color: 3

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 470219 Color: 4
Size: 275193 Color: 3
Size: 254589 Color: 2

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 470311 Color: 4
Size: 267238 Color: 3
Size: 262452 Color: 2

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 470348 Color: 2
Size: 273434 Color: 1
Size: 256219 Color: 4

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 470527 Color: 0
Size: 271319 Color: 4
Size: 258155 Color: 3

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 470547 Color: 3
Size: 278096 Color: 2
Size: 251358 Color: 2

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 470551 Color: 1
Size: 277178 Color: 4
Size: 252272 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 470569 Color: 2
Size: 265120 Color: 4
Size: 264312 Color: 3

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 470571 Color: 2
Size: 268132 Color: 1
Size: 261298 Color: 1

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 470585 Color: 2
Size: 275892 Color: 4
Size: 253524 Color: 3

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 470730 Color: 0
Size: 279226 Color: 3
Size: 250045 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 470765 Color: 0
Size: 270612 Color: 2
Size: 258624 Color: 2

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 470838 Color: 0
Size: 274517 Color: 3
Size: 254646 Color: 1

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 470839 Color: 0
Size: 269727 Color: 2
Size: 259435 Color: 3

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 470860 Color: 4
Size: 273999 Color: 2
Size: 255142 Color: 1

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 470912 Color: 1
Size: 269836 Color: 3
Size: 259253 Color: 2

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 470957 Color: 0
Size: 275046 Color: 4
Size: 253998 Color: 4

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 470982 Color: 4
Size: 265319 Color: 3
Size: 263700 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 471137 Color: 4
Size: 269821 Color: 1
Size: 259043 Color: 3

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 471173 Color: 0
Size: 270309 Color: 4
Size: 258519 Color: 1

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 471196 Color: 1
Size: 271257 Color: 3
Size: 257548 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 471272 Color: 2
Size: 267565 Color: 4
Size: 261164 Color: 2

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 471294 Color: 0
Size: 269161 Color: 2
Size: 259546 Color: 3

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 471288 Color: 3
Size: 268513 Color: 0
Size: 260200 Color: 2

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 471303 Color: 2
Size: 276979 Color: 1
Size: 251719 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 471328 Color: 0
Size: 273155 Color: 3
Size: 255518 Color: 4

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 471356 Color: 1
Size: 264508 Color: 3
Size: 264137 Color: 1

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 471484 Color: 0
Size: 276915 Color: 1
Size: 251602 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 471649 Color: 3
Size: 278210 Color: 4
Size: 250142 Color: 2

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 471576 Color: 0
Size: 269291 Color: 4
Size: 259134 Color: 2

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 471638 Color: 4
Size: 268547 Color: 1
Size: 259816 Color: 3

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 471759 Color: 3
Size: 273872 Color: 4
Size: 254370 Color: 2

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 471693 Color: 0
Size: 276346 Color: 4
Size: 251962 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 471705 Color: 4
Size: 275676 Color: 1
Size: 252620 Color: 2

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 471929 Color: 3
Size: 269381 Color: 2
Size: 258691 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 471960 Color: 4
Size: 276238 Color: 0
Size: 251803 Color: 2

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 472124 Color: 3
Size: 270217 Color: 4
Size: 257660 Color: 1

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 471960 Color: 4
Size: 266158 Color: 2
Size: 261883 Color: 4

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 472371 Color: 3
Size: 272929 Color: 1
Size: 254701 Color: 1

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 472432 Color: 1
Size: 273307 Color: 3
Size: 254262 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 472456 Color: 3
Size: 268117 Color: 4
Size: 259428 Color: 1

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 472597 Color: 0
Size: 275379 Color: 1
Size: 252025 Color: 4

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 472703 Color: 3
Size: 274276 Color: 0
Size: 253022 Color: 1

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 472676 Color: 2
Size: 271556 Color: 1
Size: 255769 Color: 4

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 472859 Color: 4
Size: 263644 Color: 1
Size: 263498 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 472978 Color: 3
Size: 264972 Color: 4
Size: 262051 Color: 2

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 472982 Color: 3
Size: 274492 Color: 2
Size: 252527 Color: 2

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 472927 Color: 0
Size: 274619 Color: 2
Size: 252455 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 473001 Color: 1
Size: 272754 Color: 3
Size: 254246 Color: 1

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 473033 Color: 2
Size: 272255 Color: 3
Size: 254713 Color: 4

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 473098 Color: 1
Size: 272435 Color: 1
Size: 254468 Color: 4

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 473315 Color: 4
Size: 276420 Color: 2
Size: 250266 Color: 4

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 473322 Color: 4
Size: 268902 Color: 3
Size: 257777 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 473369 Color: 3
Size: 269342 Color: 2
Size: 257290 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 473414 Color: 4
Size: 269881 Color: 1
Size: 256706 Color: 1

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 473431 Color: 1
Size: 272781 Color: 4
Size: 253789 Color: 4

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 473603 Color: 3
Size: 273056 Color: 4
Size: 253342 Color: 4

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 473463 Color: 0
Size: 273447 Color: 2
Size: 253091 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 473692 Color: 3
Size: 276300 Color: 0
Size: 250009 Color: 2

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 473472 Color: 1
Size: 263794 Color: 1
Size: 262735 Color: 4

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 473801 Color: 3
Size: 266461 Color: 0
Size: 259739 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 473962 Color: 3
Size: 275895 Color: 0
Size: 250144 Color: 4

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 474124 Color: 3
Size: 264637 Color: 1
Size: 261240 Color: 2

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 474143 Color: 3
Size: 263939 Color: 4
Size: 261919 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 473995 Color: 4
Size: 275179 Color: 2
Size: 250827 Color: 3

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 474211 Color: 1
Size: 264898 Color: 3
Size: 260892 Color: 2

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 474362 Color: 0
Size: 271641 Color: 2
Size: 253998 Color: 4

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 474486 Color: 0
Size: 270308 Color: 4
Size: 255207 Color: 1

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 474840 Color: 3
Size: 271699 Color: 4
Size: 253462 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 474816 Color: 1
Size: 268063 Color: 0
Size: 257122 Color: 3

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 474907 Color: 3
Size: 265530 Color: 2
Size: 259564 Color: 2

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 475087 Color: 0
Size: 270167 Color: 1
Size: 254747 Color: 4

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 475106 Color: 4
Size: 271928 Color: 3
Size: 252967 Color: 4

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 475120 Color: 1
Size: 270227 Color: 4
Size: 254654 Color: 3

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 475125 Color: 2
Size: 269366 Color: 1
Size: 255510 Color: 4

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 475144 Color: 3
Size: 268156 Color: 1
Size: 256701 Color: 4

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 475128 Color: 1
Size: 267516 Color: 1
Size: 257357 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 475361 Color: 4
Size: 267395 Color: 3
Size: 257245 Color: 4

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 475407 Color: 3
Size: 267141 Color: 4
Size: 257453 Color: 4

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 475471 Color: 1
Size: 266588 Color: 1
Size: 257942 Color: 4

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 475513 Color: 4
Size: 268062 Color: 3
Size: 256426 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 475553 Color: 0
Size: 271797 Color: 3
Size: 252651 Color: 4

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 475777 Color: 0
Size: 265988 Color: 4
Size: 258236 Color: 4

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 475795 Color: 0
Size: 271862 Color: 3
Size: 252344 Color: 4

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 475916 Color: 3
Size: 266781 Color: 1
Size: 257304 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 476024 Color: 3
Size: 273482 Color: 2
Size: 250495 Color: 2

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 475918 Color: 0
Size: 268358 Color: 1
Size: 255725 Color: 2

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 476205 Color: 3
Size: 273709 Color: 1
Size: 250087 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 476176 Color: 4
Size: 263070 Color: 0
Size: 260755 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 476213 Color: 0
Size: 263960 Color: 1
Size: 259828 Color: 3

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 476377 Color: 4
Size: 267857 Color: 2
Size: 255767 Color: 3

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 476544 Color: 1
Size: 268078 Color: 3
Size: 255379 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 476554 Color: 2
Size: 273413 Color: 1
Size: 250034 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 476645 Color: 1
Size: 267954 Color: 1
Size: 255402 Color: 3

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 476649 Color: 2
Size: 266263 Color: 4
Size: 257089 Color: 4

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 476666 Color: 1
Size: 264450 Color: 1
Size: 258885 Color: 4

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 477068 Color: 3
Size: 263808 Color: 2
Size: 259125 Color: 1

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 477114 Color: 3
Size: 266049 Color: 4
Size: 256838 Color: 2

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 477127 Color: 3
Size: 268453 Color: 0
Size: 254421 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 477025 Color: 4
Size: 272557 Color: 2
Size: 250419 Color: 1

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 477102 Color: 2
Size: 264464 Color: 4
Size: 258435 Color: 3

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 477234 Color: 4
Size: 270223 Color: 0
Size: 252544 Color: 4

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 477237 Color: 4
Size: 265625 Color: 3
Size: 257139 Color: 1

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 477265 Color: 3
Size: 270435 Color: 1
Size: 252301 Color: 1

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 477360 Color: 2
Size: 270533 Color: 1
Size: 252108 Color: 3

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 477382 Color: 4
Size: 271398 Color: 3
Size: 251221 Color: 4

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 477392 Color: 2
Size: 270345 Color: 1
Size: 252264 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 477453 Color: 2
Size: 268174 Color: 3
Size: 254374 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 477607 Color: 4
Size: 261419 Color: 2
Size: 260975 Color: 2

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 477685 Color: 2
Size: 267897 Color: 0
Size: 254419 Color: 3

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 477882 Color: 1
Size: 265143 Color: 3
Size: 256976 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 477909 Color: 4
Size: 264053 Color: 1
Size: 258039 Color: 2

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 478146 Color: 3
Size: 261244 Color: 4
Size: 260611 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 478148 Color: 2
Size: 264578 Color: 3
Size: 257275 Color: 2

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 478278 Color: 2
Size: 264284 Color: 1
Size: 257439 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 478434 Color: 3
Size: 261741 Color: 0
Size: 259826 Color: 2

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 478430 Color: 4
Size: 267580 Color: 3
Size: 253991 Color: 2

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 478482 Color: 1
Size: 263291 Color: 4
Size: 258228 Color: 3

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 478534 Color: 4
Size: 270009 Color: 4
Size: 251458 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 478579 Color: 4
Size: 268878 Color: 0
Size: 252544 Color: 4

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 478593 Color: 1
Size: 267989 Color: 4
Size: 253419 Color: 3

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 478613 Color: 1
Size: 261955 Color: 0
Size: 259433 Color: 4

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 478682 Color: 2
Size: 263823 Color: 3
Size: 257496 Color: 4

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 478698 Color: 0
Size: 266519 Color: 1
Size: 254784 Color: 1

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 478743 Color: 0
Size: 265651 Color: 3
Size: 255607 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 478786 Color: 4
Size: 266859 Color: 3
Size: 254356 Color: 3

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 478807 Color: 2
Size: 263390 Color: 0
Size: 257804 Color: 4

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 478973 Color: 3
Size: 264058 Color: 0
Size: 256970 Color: 1

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 478845 Color: 0
Size: 266749 Color: 4
Size: 254407 Color: 2

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 478909 Color: 0
Size: 268334 Color: 4
Size: 252758 Color: 3

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 479253 Color: 3
Size: 268431 Color: 4
Size: 252317 Color: 1

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 479179 Color: 1
Size: 266996 Color: 2
Size: 253826 Color: 2

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 479221 Color: 2
Size: 265633 Color: 3
Size: 255147 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 479240 Color: 2
Size: 267801 Color: 1
Size: 252960 Color: 2

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 479251 Color: 2
Size: 267766 Color: 3
Size: 252984 Color: 1

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 479339 Color: 1
Size: 260653 Color: 0
Size: 260009 Color: 0

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 479431 Color: 3
Size: 269242 Color: 4
Size: 251328 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 479459 Color: 3
Size: 260460 Color: 1
Size: 260082 Color: 1

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 479474 Color: 1
Size: 269709 Color: 2
Size: 250818 Color: 4

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 479570 Color: 4
Size: 260752 Color: 3
Size: 259679 Color: 1

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 479758 Color: 2
Size: 266174 Color: 1
Size: 254069 Color: 3

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 479909 Color: 4
Size: 268458 Color: 3
Size: 251634 Color: 4

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 480069 Color: 2
Size: 262891 Color: 3
Size: 257041 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 480088 Color: 1
Size: 268259 Color: 2
Size: 251654 Color: 3

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 480241 Color: 0
Size: 261733 Color: 0
Size: 258027 Color: 2

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 480394 Color: 2
Size: 269291 Color: 4
Size: 250316 Color: 2

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 480687 Color: 3
Size: 261741 Color: 1
Size: 257573 Color: 4

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 480659 Color: 2
Size: 269253 Color: 3
Size: 250089 Color: 2

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 480773 Color: 2
Size: 264540 Color: 3
Size: 254688 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 480776 Color: 2
Size: 266945 Color: 1
Size: 252280 Color: 1

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 480783 Color: 2
Size: 260686 Color: 3
Size: 258532 Color: 1

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 481003 Color: 3
Size: 264946 Color: 2
Size: 254052 Color: 2

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 480937 Color: 4
Size: 268892 Color: 0
Size: 250172 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 481045 Color: 0
Size: 260011 Color: 3
Size: 258945 Color: 4

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 481026 Color: 3
Size: 261255 Color: 1
Size: 257720 Color: 1

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 481124 Color: 1
Size: 268324 Color: 0
Size: 250553 Color: 4

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 481147 Color: 0
Size: 268286 Color: 0
Size: 250568 Color: 3

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 481220 Color: 2
Size: 265705 Color: 3
Size: 253076 Color: 1

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 481246 Color: 2
Size: 260755 Color: 2
Size: 258000 Color: 1

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 481414 Color: 0
Size: 261296 Color: 1
Size: 257291 Color: 4

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 481592 Color: 1
Size: 267473 Color: 2
Size: 250936 Color: 3

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 481610 Color: 1
Size: 260649 Color: 0
Size: 257742 Color: 4

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 481751 Color: 4
Size: 262563 Color: 0
Size: 255687 Color: 3

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 481904 Color: 0
Size: 259191 Color: 1
Size: 258906 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 481932 Color: 0
Size: 265677 Color: 1
Size: 252392 Color: 2

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 482135 Color: 4
Size: 262256 Color: 0
Size: 255610 Color: 3

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 482207 Color: 1
Size: 260066 Color: 2
Size: 257728 Color: 3

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 482216 Color: 1
Size: 261168 Color: 0
Size: 256617 Color: 4

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 482273 Color: 1
Size: 263301 Color: 3
Size: 254427 Color: 2

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 482363 Color: 4
Size: 267276 Color: 1
Size: 250362 Color: 3

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 482456 Color: 1
Size: 259888 Color: 3
Size: 257657 Color: 2

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 482491 Color: 4
Size: 260232 Color: 0
Size: 257278 Color: 3

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 482555 Color: 0
Size: 259750 Color: 4
Size: 257696 Color: 4

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 482565 Color: 1
Size: 261272 Color: 4
Size: 256164 Color: 3

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 482601 Color: 3
Size: 258804 Color: 4
Size: 258596 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 482568 Color: 1
Size: 260502 Color: 0
Size: 256931 Color: 0

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 482619 Color: 2
Size: 261368 Color: 3
Size: 256014 Color: 4

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 482780 Color: 0
Size: 262211 Color: 3
Size: 255010 Color: 2

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 482890 Color: 4
Size: 259482 Color: 3
Size: 257629 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 482989 Color: 1
Size: 266635 Color: 0
Size: 250377 Color: 2

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 483272 Color: 2
Size: 263031 Color: 0
Size: 253698 Color: 2

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 483450 Color: 2
Size: 258611 Color: 0
Size: 257940 Color: 3

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 483565 Color: 4
Size: 264482 Color: 0
Size: 251954 Color: 2

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 483708 Color: 3
Size: 259991 Color: 4
Size: 256302 Color: 4

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 483826 Color: 1
Size: 261492 Color: 0
Size: 254683 Color: 3

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 483831 Color: 2
Size: 263741 Color: 3
Size: 252429 Color: 0

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 484029 Color: 2
Size: 259070 Color: 1
Size: 256902 Color: 1

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 484045 Color: 0
Size: 265713 Color: 1
Size: 250243 Color: 3

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 484086 Color: 0
Size: 265853 Color: 1
Size: 250062 Color: 1

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 484209 Color: 4
Size: 259983 Color: 3
Size: 255809 Color: 2

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 484233 Color: 0
Size: 260328 Color: 1
Size: 255440 Color: 3

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 484395 Color: 0
Size: 262478 Color: 2
Size: 253128 Color: 0

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 484412 Color: 2
Size: 262258 Color: 4
Size: 253331 Color: 3

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 484473 Color: 0
Size: 258565 Color: 3
Size: 256963 Color: 2

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 484567 Color: 1
Size: 259993 Color: 0
Size: 255441 Color: 4

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 484639 Color: 2
Size: 262290 Color: 3
Size: 253072 Color: 1

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 484702 Color: 2
Size: 264016 Color: 4
Size: 251283 Color: 1

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 484713 Color: 1
Size: 264076 Color: 3
Size: 251212 Color: 1

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 484798 Color: 4
Size: 261526 Color: 2
Size: 253677 Color: 0

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 484814 Color: 2
Size: 258634 Color: 3
Size: 256553 Color: 4

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 484882 Color: 4
Size: 264529 Color: 1
Size: 250590 Color: 4

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 485035 Color: 0
Size: 258430 Color: 3
Size: 256536 Color: 1

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 485047 Color: 0
Size: 260197 Color: 3
Size: 254757 Color: 0

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 485224 Color: 1
Size: 264235 Color: 2
Size: 250542 Color: 4

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 485244 Color: 2
Size: 260688 Color: 0
Size: 254069 Color: 1

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 485304 Color: 0
Size: 264541 Color: 3
Size: 250156 Color: 1

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 485356 Color: 2
Size: 262447 Color: 1
Size: 252198 Color: 3

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 485430 Color: 0
Size: 258805 Color: 0
Size: 255766 Color: 4

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 485538 Color: 3
Size: 263581 Color: 0
Size: 250882 Color: 0

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 485659 Color: 0
Size: 263071 Color: 3
Size: 251271 Color: 1

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 485698 Color: 4
Size: 262443 Color: 4
Size: 251860 Color: 1

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 485813 Color: 2
Size: 258105 Color: 2
Size: 256083 Color: 4

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 485975 Color: 3
Size: 258230 Color: 4
Size: 255796 Color: 4

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 486005 Color: 3
Size: 262385 Color: 0
Size: 251611 Color: 2

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 486017 Color: 0
Size: 263143 Color: 2
Size: 250841 Color: 0

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 486127 Color: 3
Size: 257018 Color: 2
Size: 256856 Color: 4

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 486141 Color: 4
Size: 258288 Color: 1
Size: 255572 Color: 3

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 486184 Color: 4
Size: 258513 Color: 4
Size: 255304 Color: 0

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 486456 Color: 3
Size: 256884 Color: 0
Size: 256661 Color: 2

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 486530 Color: 0
Size: 263408 Color: 3
Size: 250063 Color: 0

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 486583 Color: 4
Size: 262979 Color: 2
Size: 250439 Color: 3

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 486612 Color: 2
Size: 262641 Color: 0
Size: 250748 Color: 4

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 486621 Color: 1
Size: 258523 Color: 3
Size: 254857 Color: 2

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 486637 Color: 4
Size: 260719 Color: 1
Size: 252645 Color: 0

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 486744 Color: 0
Size: 259462 Color: 1
Size: 253795 Color: 0

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 486801 Color: 2
Size: 262947 Color: 3
Size: 250253 Color: 2

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 486908 Color: 3
Size: 257857 Color: 1
Size: 255236 Color: 1

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 487045 Color: 1
Size: 258388 Color: 3
Size: 254568 Color: 4

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 487088 Color: 1
Size: 262645 Color: 3
Size: 250268 Color: 4

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 487099 Color: 4
Size: 257853 Color: 1
Size: 255049 Color: 0

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 487168 Color: 3
Size: 256540 Color: 4
Size: 256293 Color: 0

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 487131 Color: 4
Size: 261383 Color: 1
Size: 251487 Color: 4

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 487211 Color: 2
Size: 257455 Color: 3
Size: 255335 Color: 4

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 487312 Color: 3
Size: 256835 Color: 0
Size: 255854 Color: 4

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 487229 Color: 0
Size: 262102 Color: 1
Size: 250670 Color: 2

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 487401 Color: 4
Size: 258958 Color: 1
Size: 253642 Color: 3

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 487429 Color: 4
Size: 261991 Color: 0
Size: 250581 Color: 0

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 487561 Color: 0
Size: 262204 Color: 1
Size: 250236 Color: 4

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 487869 Color: 0
Size: 261482 Color: 3
Size: 250650 Color: 1

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 487975 Color: 3
Size: 258815 Color: 0
Size: 253211 Color: 4

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 487969 Color: 4
Size: 257061 Color: 1
Size: 254971 Color: 4

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 488078 Color: 1
Size: 258598 Color: 1
Size: 253325 Color: 3

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 488256 Color: 3
Size: 261638 Color: 2
Size: 250107 Color: 0

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 488171 Color: 1
Size: 261258 Color: 0
Size: 250572 Color: 4

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 488374 Color: 3
Size: 259540 Color: 2
Size: 252087 Color: 1

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 488254 Color: 4
Size: 257957 Color: 1
Size: 253790 Color: 1

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 488371 Color: 0
Size: 261067 Color: 0
Size: 250563 Color: 3

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 488418 Color: 1
Size: 258690 Color: 2
Size: 252893 Color: 3

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 488485 Color: 1
Size: 258481 Color: 0
Size: 253035 Color: 4

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 488486 Color: 2
Size: 260945 Color: 1
Size: 250570 Color: 3

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 488591 Color: 2
Size: 260023 Color: 1
Size: 251387 Color: 3

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 488635 Color: 4
Size: 255999 Color: 0
Size: 255367 Color: 4

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 488772 Color: 0
Size: 260708 Color: 2
Size: 250521 Color: 4

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 489106 Color: 3
Size: 259587 Color: 1
Size: 251308 Color: 0

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 489121 Color: 2
Size: 258927 Color: 4
Size: 251953 Color: 1

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 489201 Color: 0
Size: 255539 Color: 4
Size: 255261 Color: 1

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 489409 Color: 3
Size: 257515 Color: 3
Size: 253077 Color: 0

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 489446 Color: 3
Size: 259534 Color: 1
Size: 251021 Color: 0

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 489455 Color: 3
Size: 260219 Color: 1
Size: 250327 Color: 2

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 489577 Color: 1
Size: 259167 Color: 2
Size: 251257 Color: 4

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 489878 Color: 4
Size: 256705 Color: 2
Size: 253418 Color: 0

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 489844 Color: 3
Size: 257698 Color: 2
Size: 252459 Color: 2

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 489986 Color: 2
Size: 256219 Color: 0
Size: 253796 Color: 0

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 490001 Color: 1
Size: 258273 Color: 4
Size: 251727 Color: 0

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 490190 Color: 2
Size: 255233 Color: 1
Size: 254578 Color: 1

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 490251 Color: 1
Size: 255717 Color: 3
Size: 254033 Color: 4

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 490302 Color: 4
Size: 259373 Color: 0
Size: 250326 Color: 0

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 490424 Color: 1
Size: 259140 Color: 0
Size: 250437 Color: 0

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 490535 Color: 4
Size: 257459 Color: 2
Size: 252007 Color: 1

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 490606 Color: 2
Size: 257549 Color: 1
Size: 251846 Color: 1

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 490619 Color: 3
Size: 255109 Color: 4
Size: 254273 Color: 3

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 490625 Color: 3
Size: 258420 Color: 2
Size: 250956 Color: 4

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 490653 Color: 3
Size: 255669 Color: 2
Size: 253679 Color: 0

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 490751 Color: 2
Size: 258246 Color: 4
Size: 251004 Color: 3

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 490796 Color: 0
Size: 258615 Color: 4
Size: 250590 Color: 0

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 490977 Color: 1
Size: 255123 Color: 3
Size: 253901 Color: 2

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 491008 Color: 2
Size: 254774 Color: 4
Size: 254219 Color: 3

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 491146 Color: 3
Size: 256324 Color: 3
Size: 252531 Color: 1

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 491120 Color: 1
Size: 257013 Color: 3
Size: 251868 Color: 2

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 491210 Color: 0
Size: 256924 Color: 0
Size: 251867 Color: 2

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 491225 Color: 4
Size: 258760 Color: 3
Size: 250016 Color: 0

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 491385 Color: 1
Size: 257453 Color: 0
Size: 251163 Color: 4

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 491562 Color: 4
Size: 255192 Color: 3
Size: 253247 Color: 0

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 491613 Color: 1
Size: 256908 Color: 3
Size: 251480 Color: 2

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 491620 Color: 0
Size: 255963 Color: 2
Size: 252418 Color: 4

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 492093 Color: 0
Size: 255840 Color: 1
Size: 252068 Color: 4

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 492286 Color: 1
Size: 254678 Color: 1
Size: 253037 Color: 3

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 492465 Color: 3
Size: 256071 Color: 4
Size: 251465 Color: 0

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 492445 Color: 1
Size: 255385 Color: 3
Size: 252171 Color: 4

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 492446 Color: 4
Size: 256351 Color: 2
Size: 251204 Color: 2

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 492733 Color: 2
Size: 255401 Color: 3
Size: 251867 Color: 1

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 492768 Color: 3
Size: 254867 Color: 1
Size: 252366 Color: 0

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 492944 Color: 1
Size: 256796 Color: 4
Size: 250261 Color: 4

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 493046 Color: 2
Size: 253611 Color: 1
Size: 253344 Color: 0

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 493108 Color: 0
Size: 254797 Color: 3
Size: 252096 Color: 4

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 493131 Color: 0
Size: 254310 Color: 4
Size: 252560 Color: 1

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 493153 Color: 2
Size: 254608 Color: 3
Size: 252240 Color: 1

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 493170 Color: 0
Size: 256297 Color: 2
Size: 250534 Color: 3

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 493233 Color: 3
Size: 255590 Color: 2
Size: 251178 Color: 0

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 493214 Color: 1
Size: 256081 Color: 4
Size: 250706 Color: 1

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 493311 Color: 4
Size: 256670 Color: 4
Size: 250020 Color: 0

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 493383 Color: 3
Size: 255911 Color: 1
Size: 250707 Color: 4

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 493428 Color: 3
Size: 253917 Color: 4
Size: 252656 Color: 2

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 493510 Color: 3
Size: 255433 Color: 2
Size: 251058 Color: 2

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 493534 Color: 3
Size: 254653 Color: 1
Size: 251814 Color: 0

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 493497 Color: 1
Size: 253890 Color: 0
Size: 252614 Color: 2

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 493709 Color: 0
Size: 254632 Color: 3
Size: 251660 Color: 4

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 493899 Color: 4
Size: 255928 Color: 2
Size: 250174 Color: 1

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 493902 Color: 2
Size: 254451 Color: 0
Size: 251648 Color: 3

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 493937 Color: 0
Size: 255829 Color: 4
Size: 250235 Color: 3

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 494234 Color: 2
Size: 254750 Color: 4
Size: 251017 Color: 0

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 494679 Color: 3
Size: 255287 Color: 1
Size: 250035 Color: 0

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 494631 Color: 0
Size: 254091 Color: 1
Size: 251279 Color: 3

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 494682 Color: 4
Size: 255174 Color: 1
Size: 250145 Color: 0

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 494866 Color: 3
Size: 253216 Color: 0
Size: 251919 Color: 1

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 494854 Color: 0
Size: 253458 Color: 4
Size: 251689 Color: 3

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 494854 Color: 2
Size: 255060 Color: 2
Size: 250087 Color: 1

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 494874 Color: 4
Size: 254086 Color: 3
Size: 251041 Color: 0

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 495106 Color: 3
Size: 254844 Color: 4
Size: 250051 Color: 0

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 495271 Color: 4
Size: 252903 Color: 0
Size: 251827 Color: 3

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 495582 Color: 0
Size: 254410 Color: 2
Size: 250009 Color: 1

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 495587 Color: 1
Size: 253143 Color: 2
Size: 251271 Color: 3

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 495733 Color: 3
Size: 254210 Color: 4
Size: 250058 Color: 0

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 495792 Color: 0
Size: 253290 Color: 2
Size: 250919 Color: 3

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 495811 Color: 4
Size: 252491 Color: 1
Size: 251699 Color: 2

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 496003 Color: 4
Size: 253642 Color: 3
Size: 250356 Color: 0

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 496048 Color: 2
Size: 252827 Color: 3
Size: 251126 Color: 1

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 496048 Color: 2
Size: 253818 Color: 1
Size: 250135 Color: 1

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 496064 Color: 2
Size: 252239 Color: 0
Size: 251698 Color: 3

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 496068 Color: 1
Size: 252722 Color: 3
Size: 251211 Color: 0

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 496177 Color: 1
Size: 252156 Color: 0
Size: 251668 Color: 4

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 496225 Color: 3
Size: 252765 Color: 2
Size: 251011 Color: 2

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 496283 Color: 2
Size: 252617 Color: 3
Size: 251101 Color: 1

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 496477 Color: 1
Size: 252996 Color: 0
Size: 250528 Color: 3

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 496505 Color: 2
Size: 252935 Color: 0
Size: 250561 Color: 0

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 496579 Color: 1
Size: 252458 Color: 2
Size: 250964 Color: 3

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 496649 Color: 3
Size: 252742 Color: 4
Size: 250610 Color: 1

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 496645 Color: 1
Size: 252275 Color: 2
Size: 251081 Color: 4

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 496724 Color: 0
Size: 252177 Color: 3
Size: 251100 Color: 1

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 496855 Color: 3
Size: 252317 Color: 0
Size: 250829 Color: 4

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 496810 Color: 2
Size: 252234 Color: 4
Size: 250957 Color: 2

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 497227 Color: 4
Size: 251531 Color: 0
Size: 251243 Color: 4

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 497374 Color: 1
Size: 251317 Color: 4
Size: 251310 Color: 3

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 497407 Color: 3
Size: 251897 Color: 1
Size: 250697 Color: 2

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 497408 Color: 4
Size: 252534 Color: 2
Size: 250059 Color: 4

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 497536 Color: 3
Size: 251765 Color: 0
Size: 250700 Color: 4

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 497527 Color: 0
Size: 251575 Color: 2
Size: 250899 Color: 1

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 497707 Color: 0
Size: 251979 Color: 1
Size: 250315 Color: 3

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 497742 Color: 1
Size: 251372 Color: 0
Size: 250887 Color: 2

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 497770 Color: 1
Size: 251593 Color: 3
Size: 250638 Color: 0

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 497995 Color: 3
Size: 251978 Color: 4
Size: 250028 Color: 2

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 497928 Color: 2
Size: 251321 Color: 1
Size: 250752 Color: 2

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 498031 Color: 4
Size: 251562 Color: 1
Size: 250408 Color: 4

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 498189 Color: 3
Size: 251338 Color: 2
Size: 250474 Color: 2

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 498060 Color: 4
Size: 251494 Color: 1
Size: 250447 Color: 2

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 498243 Color: 3
Size: 251330 Color: 0
Size: 250428 Color: 2

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 498637 Color: 3
Size: 251111 Color: 1
Size: 250253 Color: 0

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 498737 Color: 0
Size: 251204 Color: 4
Size: 250060 Color: 3

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 498901 Color: 1
Size: 250917 Color: 2
Size: 250183 Color: 1

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 499089 Color: 3
Size: 250780 Color: 4
Size: 250132 Color: 4

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 499153 Color: 0
Size: 250435 Color: 0
Size: 250413 Color: 1

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 499281 Color: 3
Size: 250613 Color: 2
Size: 250107 Color: 4

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 499287 Color: 4
Size: 250362 Color: 1
Size: 250352 Color: 0

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 499698 Color: 0
Size: 250299 Color: 3
Size: 250004 Color: 0

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 362771 Color: 1
Size: 321028 Color: 3
Size: 316201 Color: 3

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 355094 Color: 3
Size: 335773 Color: 4
Size: 309133 Color: 4

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 344873 Color: 4
Size: 339258 Color: 2
Size: 315869 Color: 3

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 356456 Color: 4
Size: 332233 Color: 2
Size: 311311 Color: 1

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 360279 Color: 3
Size: 358135 Color: 3
Size: 281586 Color: 2

Bin 3151: 1 of cap free
Amount of items: 3
Items: 
Size: 358653 Color: 4
Size: 327105 Color: 0
Size: 314242 Color: 4

Bin 3152: 1 of cap free
Amount of items: 3
Items: 
Size: 360883 Color: 0
Size: 320108 Color: 1
Size: 319009 Color: 1

Bin 3153: 1 of cap free
Amount of items: 3
Items: 
Size: 361581 Color: 3
Size: 331312 Color: 1
Size: 307107 Color: 4

Bin 3154: 1 of cap free
Amount of items: 3
Items: 
Size: 361456 Color: 1
Size: 325502 Color: 3
Size: 313042 Color: 1

Bin 3155: 1 of cap free
Amount of items: 3
Items: 
Size: 362164 Color: 3
Size: 348911 Color: 0
Size: 288925 Color: 1

Bin 3156: 1 of cap free
Amount of items: 3
Items: 
Size: 362751 Color: 4
Size: 327453 Color: 0
Size: 309796 Color: 4

Bin 3157: 1 of cap free
Amount of items: 3
Items: 
Size: 363319 Color: 3
Size: 320007 Color: 3
Size: 316674 Color: 0

Bin 3158: 1 of cap free
Amount of items: 3
Items: 
Size: 363668 Color: 1
Size: 357498 Color: 0
Size: 278834 Color: 1

Bin 3159: 1 of cap free
Amount of items: 3
Items: 
Size: 353813 Color: 0
Size: 335901 Color: 4
Size: 310286 Color: 3

Bin 3160: 1 of cap free
Amount of items: 3
Items: 
Size: 365163 Color: 0
Size: 359625 Color: 2
Size: 275212 Color: 1

Bin 3161: 1 of cap free
Amount of items: 3
Items: 
Size: 365184 Color: 0
Size: 325575 Color: 2
Size: 309241 Color: 1

Bin 3162: 1 of cap free
Amount of items: 3
Items: 
Size: 366038 Color: 4
Size: 335183 Color: 3
Size: 298779 Color: 0

Bin 3163: 1 of cap free
Amount of items: 3
Items: 
Size: 367766 Color: 4
Size: 342188 Color: 3
Size: 290046 Color: 1

Bin 3164: 1 of cap free
Amount of items: 3
Items: 
Size: 368326 Color: 1
Size: 358985 Color: 3
Size: 272689 Color: 2

Bin 3165: 1 of cap free
Amount of items: 3
Items: 
Size: 344103 Color: 1
Size: 340623 Color: 0
Size: 315274 Color: 3

Bin 3166: 1 of cap free
Amount of items: 3
Items: 
Size: 350358 Color: 0
Size: 328408 Color: 3
Size: 321234 Color: 0

Bin 3167: 1 of cap free
Amount of items: 3
Items: 
Size: 382875 Color: 2
Size: 355195 Color: 3
Size: 261930 Color: 1

Bin 3168: 1 of cap free
Amount of items: 3
Items: 
Size: 499845 Color: 1
Size: 250090 Color: 1
Size: 250065 Color: 3

Bin 3169: 1 of cap free
Amount of items: 3
Items: 
Size: 369448 Color: 0
Size: 322281 Color: 2
Size: 308271 Color: 3

Bin 3170: 1 of cap free
Amount of items: 3
Items: 
Size: 347153 Color: 3
Size: 334092 Color: 0
Size: 318755 Color: 2

Bin 3171: 1 of cap free
Amount of items: 3
Items: 
Size: 363675 Color: 4
Size: 358972 Color: 1
Size: 277353 Color: 3

Bin 3172: 1 of cap free
Amount of items: 3
Items: 
Size: 364983 Color: 2
Size: 361373 Color: 4
Size: 273644 Color: 4

Bin 3173: 1 of cap free
Amount of items: 3
Items: 
Size: 362291 Color: 3
Size: 336860 Color: 4
Size: 300849 Color: 1

Bin 3174: 1 of cap free
Amount of items: 3
Items: 
Size: 360028 Color: 2
Size: 358105 Color: 0
Size: 281867 Color: 2

Bin 3175: 1 of cap free
Amount of items: 3
Items: 
Size: 361560 Color: 2
Size: 357075 Color: 1
Size: 281365 Color: 3

Bin 3176: 1 of cap free
Amount of items: 3
Items: 
Size: 351505 Color: 0
Size: 347779 Color: 0
Size: 300716 Color: 3

Bin 3177: 1 of cap free
Amount of items: 3
Items: 
Size: 360520 Color: 0
Size: 346861 Color: 2
Size: 292619 Color: 3

Bin 3178: 1 of cap free
Amount of items: 3
Items: 
Size: 385870 Color: 0
Size: 359575 Color: 2
Size: 254555 Color: 4

Bin 3179: 1 of cap free
Amount of items: 3
Items: 
Size: 356387 Color: 4
Size: 332776 Color: 3
Size: 310837 Color: 4

Bin 3180: 1 of cap free
Amount of items: 3
Items: 
Size: 357693 Color: 1
Size: 343459 Color: 0
Size: 298848 Color: 0

Bin 3181: 1 of cap free
Amount of items: 3
Items: 
Size: 349754 Color: 2
Size: 339850 Color: 3
Size: 310396 Color: 1

Bin 3182: 1 of cap free
Amount of items: 3
Items: 
Size: 396881 Color: 1
Size: 338942 Color: 1
Size: 264177 Color: 4

Bin 3183: 1 of cap free
Amount of items: 3
Items: 
Size: 423669 Color: 0
Size: 320142 Color: 1
Size: 256189 Color: 1

Bin 3184: 1 of cap free
Amount of items: 3
Items: 
Size: 449935 Color: 4
Size: 289945 Color: 0
Size: 260120 Color: 1

Bin 3185: 2 of cap free
Amount of items: 3
Items: 
Size: 362474 Color: 1
Size: 325405 Color: 4
Size: 312120 Color: 2

Bin 3186: 2 of cap free
Amount of items: 3
Items: 
Size: 446356 Color: 2
Size: 278628 Color: 2
Size: 275015 Color: 1

Bin 3187: 2 of cap free
Amount of items: 3
Items: 
Size: 348213 Color: 1
Size: 345185 Color: 2
Size: 306601 Color: 3

Bin 3188: 2 of cap free
Amount of items: 3
Items: 
Size: 343664 Color: 1
Size: 333470 Color: 1
Size: 322865 Color: 4

Bin 3189: 2 of cap free
Amount of items: 3
Items: 
Size: 364238 Color: 1
Size: 356364 Color: 3
Size: 279397 Color: 3

Bin 3190: 2 of cap free
Amount of items: 3
Items: 
Size: 360901 Color: 2
Size: 349685 Color: 3
Size: 289413 Color: 2

Bin 3191: 2 of cap free
Amount of items: 3
Items: 
Size: 373692 Color: 2
Size: 366151 Color: 3
Size: 260156 Color: 3

Bin 3192: 2 of cap free
Amount of items: 3
Items: 
Size: 361144 Color: 1
Size: 345408 Color: 1
Size: 293447 Color: 2

Bin 3193: 2 of cap free
Amount of items: 3
Items: 
Size: 362406 Color: 3
Size: 323330 Color: 0
Size: 314263 Color: 4

Bin 3194: 2 of cap free
Amount of items: 3
Items: 
Size: 360952 Color: 1
Size: 337130 Color: 0
Size: 301917 Color: 4

Bin 3195: 2 of cap free
Amount of items: 3
Items: 
Size: 339120 Color: 4
Size: 337787 Color: 1
Size: 323092 Color: 3

Bin 3196: 2 of cap free
Amount of items: 3
Items: 
Size: 359176 Color: 3
Size: 358420 Color: 1
Size: 282403 Color: 0

Bin 3197: 2 of cap free
Amount of items: 3
Items: 
Size: 343171 Color: 4
Size: 335418 Color: 2
Size: 321410 Color: 2

Bin 3198: 2 of cap free
Amount of items: 3
Items: 
Size: 342668 Color: 1
Size: 340748 Color: 2
Size: 316583 Color: 0

Bin 3199: 2 of cap free
Amount of items: 3
Items: 
Size: 347082 Color: 1
Size: 330396 Color: 1
Size: 322521 Color: 0

Bin 3200: 2 of cap free
Amount of items: 3
Items: 
Size: 349416 Color: 1
Size: 348430 Color: 0
Size: 302153 Color: 0

Bin 3201: 2 of cap free
Amount of items: 3
Items: 
Size: 354009 Color: 2
Size: 343503 Color: 2
Size: 302487 Color: 4

Bin 3202: 2 of cap free
Amount of items: 3
Items: 
Size: 343935 Color: 1
Size: 329596 Color: 0
Size: 326468 Color: 1

Bin 3203: 2 of cap free
Amount of items: 3
Items: 
Size: 350217 Color: 1
Size: 341651 Color: 2
Size: 308131 Color: 1

Bin 3204: 2 of cap free
Amount of items: 3
Items: 
Size: 384976 Color: 2
Size: 348070 Color: 2
Size: 266953 Color: 4

Bin 3205: 2 of cap free
Amount of items: 3
Items: 
Size: 429532 Color: 2
Size: 305178 Color: 0
Size: 265289 Color: 1

Bin 3206: 2 of cap free
Amount of items: 3
Items: 
Size: 433808 Color: 4
Size: 286883 Color: 4
Size: 279308 Color: 1

Bin 3207: 3 of cap free
Amount of items: 3
Items: 
Size: 347125 Color: 3
Size: 344102 Color: 4
Size: 308771 Color: 4

Bin 3208: 3 of cap free
Amount of items: 3
Items: 
Size: 348074 Color: 4
Size: 335312 Color: 3
Size: 316612 Color: 0

Bin 3209: 3 of cap free
Amount of items: 3
Items: 
Size: 344504 Color: 0
Size: 329999 Color: 2
Size: 325495 Color: 1

Bin 3210: 3 of cap free
Amount of items: 3
Items: 
Size: 357651 Color: 3
Size: 341387 Color: 3
Size: 300960 Color: 1

Bin 3211: 3 of cap free
Amount of items: 3
Items: 
Size: 367822 Color: 2
Size: 358809 Color: 0
Size: 273367 Color: 3

Bin 3212: 3 of cap free
Amount of items: 3
Items: 
Size: 363123 Color: 2
Size: 363033 Color: 1
Size: 273842 Color: 2

Bin 3213: 3 of cap free
Amount of items: 3
Items: 
Size: 338427 Color: 4
Size: 331416 Color: 0
Size: 330155 Color: 2

Bin 3214: 3 of cap free
Amount of items: 3
Items: 
Size: 348023 Color: 1
Size: 336329 Color: 3
Size: 315646 Color: 3

Bin 3215: 3 of cap free
Amount of items: 3
Items: 
Size: 452397 Color: 0
Size: 294759 Color: 1
Size: 252842 Color: 2

Bin 3216: 4 of cap free
Amount of items: 3
Items: 
Size: 487957 Color: 4
Size: 256923 Color: 4
Size: 255117 Color: 2

Bin 3217: 4 of cap free
Amount of items: 3
Items: 
Size: 370598 Color: 2
Size: 355667 Color: 4
Size: 273732 Color: 1

Bin 3218: 4 of cap free
Amount of items: 3
Items: 
Size: 354491 Color: 0
Size: 352257 Color: 1
Size: 293249 Color: 2

Bin 3219: 4 of cap free
Amount of items: 3
Items: 
Size: 359222 Color: 2
Size: 336928 Color: 2
Size: 303847 Color: 0

Bin 3220: 4 of cap free
Amount of items: 3
Items: 
Size: 353571 Color: 4
Size: 330800 Color: 3
Size: 315626 Color: 3

Bin 3221: 4 of cap free
Amount of items: 3
Items: 
Size: 362908 Color: 3
Size: 362056 Color: 2
Size: 275033 Color: 2

Bin 3222: 5 of cap free
Amount of items: 3
Items: 
Size: 399138 Color: 4
Size: 346876 Color: 3
Size: 253982 Color: 1

Bin 3223: 5 of cap free
Amount of items: 3
Items: 
Size: 352512 Color: 4
Size: 327496 Color: 0
Size: 319988 Color: 2

Bin 3224: 5 of cap free
Amount of items: 3
Items: 
Size: 356267 Color: 3
Size: 329680 Color: 3
Size: 314049 Color: 4

Bin 3225: 5 of cap free
Amount of items: 3
Items: 
Size: 415614 Color: 0
Size: 331278 Color: 2
Size: 253104 Color: 0

Bin 3226: 5 of cap free
Amount of items: 3
Items: 
Size: 355708 Color: 3
Size: 334834 Color: 2
Size: 309454 Color: 4

Bin 3227: 5 of cap free
Amount of items: 3
Items: 
Size: 367068 Color: 3
Size: 364938 Color: 3
Size: 267990 Color: 1

Bin 3228: 5 of cap free
Amount of items: 3
Items: 
Size: 447415 Color: 4
Size: 292831 Color: 2
Size: 259750 Color: 4

Bin 3229: 6 of cap free
Amount of items: 3
Items: 
Size: 445368 Color: 0
Size: 302999 Color: 3
Size: 251628 Color: 3

Bin 3230: 6 of cap free
Amount of items: 3
Items: 
Size: 348127 Color: 3
Size: 334744 Color: 3
Size: 317124 Color: 2

Bin 3231: 6 of cap free
Amount of items: 3
Items: 
Size: 349107 Color: 4
Size: 332434 Color: 3
Size: 318454 Color: 0

Bin 3232: 6 of cap free
Amount of items: 3
Items: 
Size: 364947 Color: 0
Size: 364858 Color: 3
Size: 270190 Color: 4

Bin 3233: 7 of cap free
Amount of items: 3
Items: 
Size: 369309 Color: 1
Size: 364074 Color: 1
Size: 266611 Color: 3

Bin 3234: 7 of cap free
Amount of items: 3
Items: 
Size: 358362 Color: 0
Size: 339168 Color: 0
Size: 302464 Color: 4

Bin 3235: 7 of cap free
Amount of items: 3
Items: 
Size: 466647 Color: 4
Size: 266727 Color: 4
Size: 266620 Color: 0

Bin 3236: 7 of cap free
Amount of items: 3
Items: 
Size: 349035 Color: 2
Size: 334985 Color: 0
Size: 315974 Color: 0

Bin 3237: 7 of cap free
Amount of items: 3
Items: 
Size: 374416 Color: 0
Size: 364202 Color: 1
Size: 261376 Color: 1

Bin 3238: 7 of cap free
Amount of items: 3
Items: 
Size: 362056 Color: 3
Size: 319975 Color: 0
Size: 317963 Color: 2

Bin 3239: 7 of cap free
Amount of items: 3
Items: 
Size: 335288 Color: 1
Size: 334295 Color: 2
Size: 330411 Color: 1

Bin 3240: 7 of cap free
Amount of items: 3
Items: 
Size: 353842 Color: 1
Size: 337755 Color: 0
Size: 308397 Color: 1

Bin 3241: 7 of cap free
Amount of items: 3
Items: 
Size: 333395 Color: 3
Size: 333392 Color: 1
Size: 333207 Color: 2

Bin 3242: 7 of cap free
Amount of items: 3
Items: 
Size: 351316 Color: 4
Size: 350689 Color: 0
Size: 297989 Color: 3

Bin 3243: 7 of cap free
Amount of items: 3
Items: 
Size: 335073 Color: 1
Size: 335072 Color: 4
Size: 329849 Color: 2

Bin 3244: 8 of cap free
Amount of items: 3
Items: 
Size: 377459 Color: 4
Size: 370916 Color: 4
Size: 251618 Color: 0

Bin 3245: 9 of cap free
Amount of items: 3
Items: 
Size: 458198 Color: 3
Size: 289398 Color: 2
Size: 252396 Color: 0

Bin 3246: 9 of cap free
Amount of items: 3
Items: 
Size: 365371 Color: 3
Size: 358940 Color: 3
Size: 275681 Color: 4

Bin 3247: 9 of cap free
Amount of items: 3
Items: 
Size: 347349 Color: 3
Size: 328814 Color: 2
Size: 323829 Color: 1

Bin 3248: 10 of cap free
Amount of items: 3
Items: 
Size: 387833 Color: 1
Size: 338751 Color: 3
Size: 273407 Color: 2

Bin 3249: 10 of cap free
Amount of items: 3
Items: 
Size: 361193 Color: 1
Size: 360152 Color: 1
Size: 278646 Color: 0

Bin 3250: 10 of cap free
Amount of items: 3
Items: 
Size: 360200 Color: 1
Size: 358144 Color: 0
Size: 281647 Color: 0

Bin 3251: 11 of cap free
Amount of items: 3
Items: 
Size: 397234 Color: 2
Size: 350716 Color: 4
Size: 252040 Color: 4

Bin 3252: 11 of cap free
Amount of items: 3
Items: 
Size: 352639 Color: 4
Size: 348984 Color: 4
Size: 298367 Color: 1

Bin 3253: 12 of cap free
Amount of items: 3
Items: 
Size: 345467 Color: 4
Size: 343419 Color: 4
Size: 311103 Color: 0

Bin 3254: 12 of cap free
Amount of items: 3
Items: 
Size: 389250 Color: 0
Size: 346632 Color: 0
Size: 264107 Color: 4

Bin 3255: 12 of cap free
Amount of items: 3
Items: 
Size: 342048 Color: 1
Size: 329707 Color: 4
Size: 328234 Color: 0

Bin 3256: 12 of cap free
Amount of items: 3
Items: 
Size: 353249 Color: 2
Size: 325994 Color: 0
Size: 320746 Color: 2

Bin 3257: 13 of cap free
Amount of items: 3
Items: 
Size: 366040 Color: 0
Size: 349707 Color: 4
Size: 284241 Color: 1

Bin 3258: 13 of cap free
Amount of items: 3
Items: 
Size: 352148 Color: 2
Size: 351782 Color: 1
Size: 296058 Color: 3

Bin 3259: 13 of cap free
Amount of items: 3
Items: 
Size: 361727 Color: 1
Size: 354448 Color: 3
Size: 283813 Color: 0

Bin 3260: 16 of cap free
Amount of items: 3
Items: 
Size: 347454 Color: 3
Size: 332776 Color: 4
Size: 319755 Color: 4

Bin 3261: 16 of cap free
Amount of items: 3
Items: 
Size: 404052 Color: 1
Size: 336430 Color: 1
Size: 259503 Color: 2

Bin 3262: 17 of cap free
Amount of items: 3
Items: 
Size: 363711 Color: 4
Size: 335797 Color: 1
Size: 300476 Color: 4

Bin 3263: 17 of cap free
Amount of items: 3
Items: 
Size: 487432 Color: 2
Size: 260489 Color: 4
Size: 252063 Color: 2

Bin 3264: 19 of cap free
Amount of items: 3
Items: 
Size: 355331 Color: 0
Size: 350740 Color: 4
Size: 293911 Color: 2

Bin 3265: 20 of cap free
Amount of items: 3
Items: 
Size: 336203 Color: 4
Size: 335098 Color: 4
Size: 328680 Color: 3

Bin 3266: 20 of cap free
Amount of items: 3
Items: 
Size: 370117 Color: 0
Size: 355976 Color: 0
Size: 273888 Color: 3

Bin 3267: 22 of cap free
Amount of items: 3
Items: 
Size: 336559 Color: 0
Size: 332357 Color: 0
Size: 331063 Color: 4

Bin 3268: 25 of cap free
Amount of items: 3
Items: 
Size: 366611 Color: 2
Size: 354385 Color: 1
Size: 278980 Color: 1

Bin 3269: 26 of cap free
Amount of items: 3
Items: 
Size: 396520 Color: 3
Size: 348728 Color: 1
Size: 254727 Color: 4

Bin 3270: 26 of cap free
Amount of items: 3
Items: 
Size: 357901 Color: 4
Size: 350453 Color: 2
Size: 291621 Color: 1

Bin 3271: 26 of cap free
Amount of items: 3
Items: 
Size: 349306 Color: 1
Size: 346624 Color: 4
Size: 304045 Color: 2

Bin 3272: 27 of cap free
Amount of items: 3
Items: 
Size: 357209 Color: 0
Size: 349503 Color: 1
Size: 293262 Color: 1

Bin 3273: 28 of cap free
Amount of items: 3
Items: 
Size: 423813 Color: 1
Size: 319341 Color: 2
Size: 256819 Color: 4

Bin 3274: 29 of cap free
Amount of items: 3
Items: 
Size: 399652 Color: 1
Size: 338425 Color: 4
Size: 261895 Color: 1

Bin 3275: 30 of cap free
Amount of items: 3
Items: 
Size: 349761 Color: 2
Size: 332907 Color: 3
Size: 317303 Color: 1

Bin 3276: 37 of cap free
Amount of items: 3
Items: 
Size: 358570 Color: 4
Size: 353318 Color: 2
Size: 288076 Color: 1

Bin 3277: 37 of cap free
Amount of items: 3
Items: 
Size: 365270 Color: 0
Size: 360176 Color: 3
Size: 274518 Color: 4

Bin 3278: 37 of cap free
Amount of items: 3
Items: 
Size: 361443 Color: 2
Size: 319426 Color: 2
Size: 319095 Color: 0

Bin 3279: 39 of cap free
Amount of items: 3
Items: 
Size: 436487 Color: 4
Size: 308748 Color: 2
Size: 254727 Color: 0

Bin 3280: 40 of cap free
Amount of items: 3
Items: 
Size: 466068 Color: 3
Size: 269787 Color: 4
Size: 264106 Color: 2

Bin 3281: 42 of cap free
Amount of items: 3
Items: 
Size: 468839 Color: 4
Size: 276539 Color: 2
Size: 254581 Color: 4

Bin 3282: 43 of cap free
Amount of items: 3
Items: 
Size: 359522 Color: 2
Size: 349298 Color: 3
Size: 291138 Color: 4

Bin 3283: 47 of cap free
Amount of items: 3
Items: 
Size: 496519 Color: 4
Size: 252464 Color: 1
Size: 250971 Color: 4

Bin 3284: 48 of cap free
Amount of items: 3
Items: 
Size: 382905 Color: 4
Size: 354932 Color: 1
Size: 262116 Color: 3

Bin 3285: 48 of cap free
Amount of items: 3
Items: 
Size: 366229 Color: 3
Size: 362916 Color: 0
Size: 270808 Color: 2

Bin 3286: 50 of cap free
Amount of items: 3
Items: 
Size: 411122 Color: 2
Size: 336621 Color: 0
Size: 252208 Color: 3

Bin 3287: 50 of cap free
Amount of items: 3
Items: 
Size: 353670 Color: 2
Size: 352387 Color: 3
Size: 293894 Color: 3

Bin 3288: 54 of cap free
Amount of items: 3
Items: 
Size: 462779 Color: 4
Size: 273700 Color: 1
Size: 263468 Color: 2

Bin 3289: 55 of cap free
Amount of items: 3
Items: 
Size: 357959 Color: 4
Size: 352025 Color: 3
Size: 289962 Color: 1

Bin 3290: 57 of cap free
Amount of items: 3
Items: 
Size: 485061 Color: 0
Size: 264102 Color: 2
Size: 250781 Color: 3

Bin 3291: 60 of cap free
Amount of items: 3
Items: 
Size: 480845 Color: 1
Size: 267548 Color: 3
Size: 251548 Color: 2

Bin 3292: 60 of cap free
Amount of items: 3
Items: 
Size: 341402 Color: 2
Size: 331492 Color: 2
Size: 327047 Color: 3

Bin 3293: 64 of cap free
Amount of items: 3
Items: 
Size: 346923 Color: 1
Size: 338933 Color: 4
Size: 314081 Color: 0

Bin 3294: 64 of cap free
Amount of items: 3
Items: 
Size: 384174 Color: 2
Size: 355394 Color: 4
Size: 260369 Color: 1

Bin 3295: 69 of cap free
Amount of items: 3
Items: 
Size: 434542 Color: 4
Size: 313111 Color: 3
Size: 252279 Color: 0

Bin 3296: 71 of cap free
Amount of items: 3
Items: 
Size: 359283 Color: 2
Size: 342067 Color: 0
Size: 298580 Color: 1

Bin 3297: 72 of cap free
Amount of items: 3
Items: 
Size: 367424 Color: 0
Size: 361972 Color: 1
Size: 270533 Color: 0

Bin 3298: 84 of cap free
Amount of items: 3
Items: 
Size: 499115 Color: 0
Size: 250682 Color: 3
Size: 250120 Color: 1

Bin 3299: 91 of cap free
Amount of items: 3
Items: 
Size: 358088 Color: 2
Size: 336088 Color: 3
Size: 305734 Color: 3

Bin 3300: 109 of cap free
Amount of items: 2
Items: 
Size: 499969 Color: 1
Size: 499923 Color: 0

Bin 3301: 128 of cap free
Amount of items: 3
Items: 
Size: 347393 Color: 2
Size: 340694 Color: 0
Size: 311786 Color: 0

Bin 3302: 142 of cap free
Amount of items: 3
Items: 
Size: 346352 Color: 3
Size: 329386 Color: 4
Size: 324121 Color: 4

Bin 3303: 144 of cap free
Amount of items: 3
Items: 
Size: 355323 Color: 1
Size: 354333 Color: 4
Size: 290201 Color: 1

Bin 3304: 148 of cap free
Amount of items: 3
Items: 
Size: 437219 Color: 0
Size: 297894 Color: 1
Size: 264740 Color: 1

Bin 3305: 154 of cap free
Amount of items: 3
Items: 
Size: 365589 Color: 1
Size: 363794 Color: 0
Size: 270464 Color: 3

Bin 3306: 168 of cap free
Amount of items: 3
Items: 
Size: 369469 Color: 2
Size: 364795 Color: 3
Size: 265569 Color: 3

Bin 3307: 186 of cap free
Amount of items: 3
Items: 
Size: 351007 Color: 2
Size: 332265 Color: 0
Size: 316543 Color: 0

Bin 3308: 224 of cap free
Amount of items: 3
Items: 
Size: 446060 Color: 0
Size: 293088 Color: 3
Size: 260629 Color: 2

Bin 3309: 228 of cap free
Amount of items: 3
Items: 
Size: 469574 Color: 1
Size: 266440 Color: 2
Size: 263759 Color: 4

Bin 3310: 232 of cap free
Amount of items: 3
Items: 
Size: 401785 Color: 1
Size: 342926 Color: 2
Size: 255058 Color: 0

Bin 3311: 241 of cap free
Amount of items: 3
Items: 
Size: 359163 Color: 4
Size: 356880 Color: 4
Size: 283717 Color: 0

Bin 3312: 248 of cap free
Amount of items: 3
Items: 
Size: 362829 Color: 4
Size: 334968 Color: 0
Size: 301956 Color: 0

Bin 3313: 257 of cap free
Amount of items: 3
Items: 
Size: 357194 Color: 1
Size: 346246 Color: 1
Size: 296304 Color: 0

Bin 3314: 287 of cap free
Amount of items: 3
Items: 
Size: 394321 Color: 1
Size: 341952 Color: 0
Size: 263441 Color: 2

Bin 3315: 293 of cap free
Amount of items: 3
Items: 
Size: 362795 Color: 3
Size: 361600 Color: 1
Size: 275313 Color: 2

Bin 3316: 436 of cap free
Amount of items: 3
Items: 
Size: 396277 Color: 4
Size: 348930 Color: 0
Size: 254358 Color: 3

Bin 3317: 461 of cap free
Amount of items: 3
Items: 
Size: 383046 Color: 1
Size: 353838 Color: 2
Size: 262656 Color: 2

Bin 3318: 578 of cap free
Amount of items: 3
Items: 
Size: 467441 Color: 1
Size: 267253 Color: 3
Size: 264729 Color: 1

Bin 3319: 654 of cap free
Amount of items: 3
Items: 
Size: 432221 Color: 4
Size: 309324 Color: 0
Size: 257802 Color: 1

Bin 3320: 797 of cap free
Amount of items: 2
Items: 
Size: 499698 Color: 1
Size: 499506 Color: 2

Bin 3321: 823 of cap free
Amount of items: 3
Items: 
Size: 409324 Color: 0
Size: 297860 Color: 2
Size: 291994 Color: 0

Bin 3322: 930 of cap free
Amount of items: 3
Items: 
Size: 361424 Color: 1
Size: 358762 Color: 2
Size: 278885 Color: 0

Bin 3323: 1042 of cap free
Amount of items: 3
Items: 
Size: 370354 Color: 1
Size: 360005 Color: 0
Size: 268600 Color: 4

Bin 3324: 1254 of cap free
Amount of items: 3
Items: 
Size: 414138 Color: 1
Size: 313864 Color: 0
Size: 270745 Color: 2

Bin 3325: 2283 of cap free
Amount of items: 2
Items: 
Size: 499010 Color: 4
Size: 498708 Color: 1

Bin 3326: 2860 of cap free
Amount of items: 2
Items: 
Size: 498583 Color: 2
Size: 498558 Color: 4

Bin 3327: 6729 of cap free
Amount of items: 3
Items: 
Size: 362729 Color: 2
Size: 319056 Color: 1
Size: 311487 Color: 4

Bin 3328: 12189 of cap free
Amount of items: 3
Items: 
Size: 362418 Color: 3
Size: 359938 Color: 2
Size: 265456 Color: 0

Bin 3329: 23691 of cap free
Amount of items: 3
Items: 
Size: 356361 Color: 3
Size: 355293 Color: 1
Size: 264656 Color: 1

Bin 3330: 80724 of cap free
Amount of items: 3
Items: 
Size: 355082 Color: 1
Size: 299620 Color: 0
Size: 264575 Color: 4

Bin 3331: 212009 of cap free
Amount of items: 3
Items: 
Size: 264389 Color: 2
Size: 261815 Color: 0
Size: 261788 Color: 4

Bin 3332: 217034 of cap free
Amount of items: 3
Items: 
Size: 261593 Color: 2
Size: 261137 Color: 0
Size: 260237 Color: 2

Bin 3333: 221066 of cap free
Amount of items: 3
Items: 
Size: 260217 Color: 2
Size: 259654 Color: 2
Size: 259064 Color: 4

Bin 3334: 227449 of cap free
Amount of items: 3
Items: 
Size: 258982 Color: 3
Size: 257006 Color: 4
Size: 256564 Color: 0

Bin 3335: 234241 of cap free
Amount of items: 3
Items: 
Size: 256313 Color: 3
Size: 256288 Color: 4
Size: 253159 Color: 0

Bin 3336: 747436 of cap free
Amount of items: 1
Items: 
Size: 252565 Color: 3

Total size: 3334003334
Total free space: 2000002

