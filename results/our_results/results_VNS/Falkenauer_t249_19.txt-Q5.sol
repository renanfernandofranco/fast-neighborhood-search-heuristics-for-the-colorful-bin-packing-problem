Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 312 Color: 1
Size: 271 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 353 Color: 4
Size: 270 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 3
Size: 252 Color: 2
Size: 250 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 264 Color: 0
Size: 251 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 314 Color: 3
Size: 259 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 276 Color: 3
Size: 260 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 309 Color: 0
Size: 274 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 256 Color: 3
Size: 251 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 4
Size: 324 Color: 2
Size: 270 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 313 Color: 4
Size: 298 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 286 Color: 0
Size: 273 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 318 Color: 1
Size: 419 Color: 3
Size: 263 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 279 Color: 2
Size: 250 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 320 Color: 0
Size: 253 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 326 Color: 0
Size: 255 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 2
Size: 270 Color: 3
Size: 261 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 257 Color: 1
Size: 256 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 4
Size: 335 Color: 3
Size: 255 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 3
Size: 256 Color: 2
Size: 252 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 289 Color: 2
Size: 263 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 278 Color: 0
Size: 256 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 2
Size: 325 Color: 3
Size: 271 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 4
Size: 299 Color: 0
Size: 267 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 261 Color: 2
Size: 251 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 0
Size: 253 Color: 1
Size: 251 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 280 Color: 1
Size: 253 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 274 Color: 3
Size: 287 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 4
Size: 270 Color: 3
Size: 268 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 291 Color: 0
Size: 264 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 4
Size: 353 Color: 3
Size: 291 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 318 Color: 4
Size: 305 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 313 Color: 4
Size: 278 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 3
Size: 250 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 3
Size: 272 Color: 2
Size: 262 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 4
Size: 296 Color: 0
Size: 277 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 3
Size: 306 Color: 1
Size: 252 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 2
Size: 303 Color: 0
Size: 255 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 3
Size: 326 Color: 0
Size: 271 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 269 Color: 0
Size: 263 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 2
Size: 363 Color: 3
Size: 267 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 0
Size: 296 Color: 0
Size: 284 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 258 Color: 3
Size: 258 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 280 Color: 4
Size: 251 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 347 Color: 1
Size: 275 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 260 Color: 4
Size: 256 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 2
Size: 339 Color: 3
Size: 308 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 273 Color: 0
Size: 263 Color: 2

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 341 Color: 2
Size: 254 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 3
Size: 332 Color: 3
Size: 331 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 305 Color: 1
Size: 277 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 350 Color: 1
Size: 289 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 4
Size: 282 Color: 2
Size: 251 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 250 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 284 Color: 4
Size: 262 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 288 Color: 1
Size: 280 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 2
Size: 320 Color: 3
Size: 256 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 322 Color: 0
Size: 255 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 264 Color: 1
Size: 258 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 266 Color: 0
Size: 260 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 274 Color: 0
Size: 254 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 4
Size: 296 Color: 2
Size: 252 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 4
Size: 318 Color: 0
Size: 316 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 299 Color: 2
Size: 255 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 3
Size: 315 Color: 1
Size: 284 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 2
Size: 312 Color: 4
Size: 275 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 271 Color: 4
Size: 261 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 283 Color: 4
Size: 259 Color: 2

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 0
Size: 266 Color: 4
Size: 252 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 0
Size: 268 Color: 2
Size: 257 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 2
Size: 342 Color: 4
Size: 253 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 271 Color: 2
Size: 269 Color: 4

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 261 Color: 2
Size: 250 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 271 Color: 2
Size: 253 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 260 Color: 0
Size: 252 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 259 Color: 4
Size: 254 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 277 Color: 2
Size: 273 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 0
Size: 270 Color: 2
Size: 260 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 282 Color: 3
Size: 259 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 2
Size: 323 Color: 4
Size: 281 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 0
Size: 293 Color: 4
Size: 250 Color: 3

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 280 Color: 3
Size: 278 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 0
Size: 260 Color: 1
Size: 256 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 290 Color: 0
Size: 285 Color: 3

Total size: 83000
Total free space: 0

