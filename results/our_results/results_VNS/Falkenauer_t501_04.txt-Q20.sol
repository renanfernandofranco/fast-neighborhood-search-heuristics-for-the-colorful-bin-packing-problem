Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 8
Size: 279 Color: 10
Size: 268 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 3
Size: 251 Color: 1
Size: 250 Color: 12

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 16
Size: 366 Color: 11
Size: 253 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 338 Color: 3
Size: 280 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 282 Color: 6
Size: 280 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 11
Size: 251 Color: 2
Size: 250 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 11
Size: 363 Color: 5
Size: 266 Color: 15

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 6
Size: 335 Color: 6
Size: 306 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 330 Color: 10
Size: 275 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 261 Color: 10
Size: 253 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 280 Color: 13
Size: 264 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 6
Size: 313 Color: 11
Size: 290 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 10
Size: 356 Color: 18
Size: 264 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 18
Size: 272 Color: 9
Size: 267 Color: 11

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 13
Size: 257 Color: 2
Size: 256 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 343 Color: 1
Size: 254 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 6
Size: 318 Color: 15
Size: 287 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 307 Color: 2
Size: 285 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 7
Size: 252 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 2
Size: 263 Color: 3
Size: 252 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 330 Color: 7
Size: 253 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 5
Size: 263 Color: 17
Size: 251 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 17
Size: 256 Color: 9
Size: 251 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 18
Size: 285 Color: 16
Size: 250 Color: 7

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 340 Color: 16
Size: 290 Color: 11

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 7
Size: 326 Color: 5
Size: 257 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 11
Size: 285 Color: 7
Size: 260 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 7
Size: 264 Color: 0
Size: 252 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 17
Size: 261 Color: 9
Size: 254 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 17
Size: 312 Color: 6
Size: 258 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9
Size: 302 Color: 16
Size: 256 Color: 13

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 12
Size: 351 Color: 2
Size: 274 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 7
Size: 319 Color: 17
Size: 255 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 15
Size: 266 Color: 4
Size: 253 Color: 15

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 342 Color: 11
Size: 298 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 15
Size: 350 Color: 4
Size: 270 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 331 Color: 18
Size: 278 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 17
Size: 296 Color: 9
Size: 257 Color: 11

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 2
Size: 289 Color: 13
Size: 276 Color: 11

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 6
Size: 318 Color: 0
Size: 267 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 19
Size: 252 Color: 16
Size: 250 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 3
Size: 329 Color: 9
Size: 265 Color: 10

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 268 Color: 19
Size: 255 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 2
Size: 281 Color: 5
Size: 257 Color: 8

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 10
Size: 342 Color: 16
Size: 302 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 298 Color: 14
Size: 274 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 281 Color: 14
Size: 264 Color: 7

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 13
Size: 325 Color: 19
Size: 283 Color: 7

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 18
Size: 342 Color: 7
Size: 265 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 12
Size: 285 Color: 3
Size: 279 Color: 11

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 265 Color: 0
Size: 256 Color: 11

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 307 Color: 11
Size: 270 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 282 Color: 16
Size: 253 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 15
Size: 316 Color: 14
Size: 283 Color: 14

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 319 Color: 14
Size: 309 Color: 9

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 337 Color: 18
Size: 264 Color: 7

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 332 Color: 15
Size: 265 Color: 13

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 6
Size: 361 Color: 3
Size: 267 Color: 15

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 9
Size: 296 Color: 7
Size: 292 Color: 18

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 4
Size: 330 Color: 19
Size: 289 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 10
Size: 363 Color: 18
Size: 273 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 8
Size: 319 Color: 7
Size: 256 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 1
Size: 333 Color: 0
Size: 326 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 278 Color: 7
Size: 259 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 312 Color: 8
Size: 301 Color: 13

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 12
Size: 284 Color: 10
Size: 267 Color: 18

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 7
Size: 351 Color: 8
Size: 264 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 368 Color: 10
Size: 264 Color: 14

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 0
Size: 254 Color: 18
Size: 253 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 8
Size: 293 Color: 10
Size: 253 Color: 3

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 10
Size: 284 Color: 16
Size: 269 Color: 18

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 13
Size: 300 Color: 11
Size: 291 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 9
Size: 313 Color: 19
Size: 277 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 310 Color: 18
Size: 306 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 15
Size: 288 Color: 14
Size: 273 Color: 11

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 14
Size: 259 Color: 0
Size: 255 Color: 17

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 9
Size: 323 Color: 14
Size: 251 Color: 7

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 317 Color: 7
Size: 263 Color: 15

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 13
Size: 261 Color: 7
Size: 253 Color: 10

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 297 Color: 12
Size: 282 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 16
Size: 326 Color: 10
Size: 294 Color: 8

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 15
Size: 314 Color: 3
Size: 282 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 316 Color: 16
Size: 257 Color: 18

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 341 Color: 0
Size: 261 Color: 17

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 2
Size: 318 Color: 12
Size: 270 Color: 9

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 354 Color: 7
Size: 261 Color: 12

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 14
Size: 315 Color: 4
Size: 256 Color: 10

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 19
Size: 267 Color: 15
Size: 263 Color: 19

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 11
Size: 269 Color: 8
Size: 265 Color: 17

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 13
Size: 269 Color: 3
Size: 266 Color: 19

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 346 Color: 0
Size: 294 Color: 11

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 340 Color: 8
Size: 277 Color: 7

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 7
Size: 324 Color: 13
Size: 275 Color: 6

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 371 Color: 10
Size: 252 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 16
Size: 311 Color: 3
Size: 295 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 352 Color: 1
Size: 254 Color: 4

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 306 Color: 5
Size: 269 Color: 16

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 2
Size: 278 Color: 0
Size: 256 Color: 14

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 18
Size: 360 Color: 11
Size: 264 Color: 19

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 6
Size: 325 Color: 5
Size: 298 Color: 19

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 5
Size: 343 Color: 2
Size: 298 Color: 4

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 288 Color: 1
Size: 280 Color: 14

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 296 Color: 7
Size: 282 Color: 13

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 9
Size: 274 Color: 17
Size: 253 Color: 2

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 11
Size: 259 Color: 17
Size: 258 Color: 10

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 304 Color: 17
Size: 293 Color: 17

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 13
Size: 338 Color: 18
Size: 264 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 286 Color: 9
Size: 258 Color: 11

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 19
Size: 315 Color: 19
Size: 275 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 18
Size: 330 Color: 0
Size: 274 Color: 16

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 318 Color: 6
Size: 254 Color: 19

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 12
Size: 352 Color: 3
Size: 266 Color: 16

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 18
Size: 343 Color: 3
Size: 288 Color: 14

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 11
Size: 326 Color: 7
Size: 250 Color: 17

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 19
Size: 331 Color: 1
Size: 316 Color: 15

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 0
Size: 365 Color: 1
Size: 260 Color: 12

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 15
Size: 294 Color: 6
Size: 260 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 8
Size: 320 Color: 6
Size: 281 Color: 4

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 8
Size: 288 Color: 11
Size: 268 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 370 Color: 6
Size: 259 Color: 3

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 11
Size: 268 Color: 2
Size: 258 Color: 18

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 340 Color: 1
Size: 252 Color: 11

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 7
Size: 341 Color: 9
Size: 299 Color: 19

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 15
Size: 255 Color: 15
Size: 255 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 353 Color: 16
Size: 258 Color: 17

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 12
Size: 283 Color: 3
Size: 269 Color: 5

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 10
Size: 286 Color: 16
Size: 274 Color: 18

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 16
Size: 344 Color: 13
Size: 281 Color: 18

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 12
Size: 331 Color: 0
Size: 274 Color: 2

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 10
Size: 346 Color: 9
Size: 253 Color: 16

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 13
Size: 267 Color: 1
Size: 254 Color: 5

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 255 Color: 10
Size: 250 Color: 9

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 4
Size: 326 Color: 10
Size: 280 Color: 11

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 9
Size: 288 Color: 13
Size: 251 Color: 4

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 7
Size: 257 Color: 17
Size: 255 Color: 12

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 357 Color: 10
Size: 264 Color: 19

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 16
Size: 350 Color: 19
Size: 276 Color: 6

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 13
Size: 273 Color: 16
Size: 256 Color: 6

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 3
Size: 335 Color: 9
Size: 283 Color: 7

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 341 Color: 16
Size: 289 Color: 12

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 285 Color: 3
Size: 263 Color: 17
Size: 452 Color: 12

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 11
Size: 291 Color: 6
Size: 259 Color: 11

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 9
Size: 308 Color: 3
Size: 283 Color: 17

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 19
Size: 312 Color: 10
Size: 266 Color: 13

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 19
Size: 282 Color: 8
Size: 258 Color: 19

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 280 Color: 4
Size: 260 Color: 16
Size: 460 Color: 10

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 17
Size: 283 Color: 11
Size: 254 Color: 17

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 15
Size: 351 Color: 15
Size: 274 Color: 17

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 17
Size: 283 Color: 0
Size: 263 Color: 8

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 11
Size: 280 Color: 15
Size: 261 Color: 13

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 12
Size: 330 Color: 19
Size: 280 Color: 12

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 17
Size: 276 Color: 0
Size: 253 Color: 13

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 365 Color: 2
Size: 266 Color: 18

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 312 Color: 16
Size: 291 Color: 3

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 19
Size: 335 Color: 3
Size: 268 Color: 15

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 17
Size: 294 Color: 18
Size: 284 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 261 Color: 14
Size: 252 Color: 18

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 16
Size: 250 Color: 17

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 8
Size: 260 Color: 15
Size: 255 Color: 16

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 11
Size: 272 Color: 19
Size: 258 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 320 Color: 0
Size: 262 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 10
Size: 316 Color: 16
Size: 256 Color: 10

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 3
Size: 323 Color: 5
Size: 252 Color: 14

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 295 Color: 15
Size: 277 Color: 17

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 10
Size: 366 Color: 10
Size: 263 Color: 17

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 320 Color: 7
Size: 258 Color: 13

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 17
Size: 328 Color: 10
Size: 292 Color: 8

Total size: 167000
Total free space: 0

