Capicity Bin: 2000
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1065 Color: 1
Size: 781 Color: 0
Size: 154 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1069 Color: 1
Size: 833 Color: 1
Size: 98 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1256 Color: 1
Size: 676 Color: 1
Size: 68 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 0
Size: 663 Color: 1
Size: 20 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 1
Size: 487 Color: 1
Size: 114 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1526 Color: 1
Size: 416 Color: 0
Size: 58 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1570 Color: 0
Size: 362 Color: 1
Size: 68 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1611 Color: 1
Size: 325 Color: 0
Size: 32 Color: 1
Size: 32 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 0
Size: 220 Color: 0
Size: 174 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 0
Size: 311 Color: 1
Size: 42 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 0
Size: 218 Color: 1
Size: 76 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 0
Size: 231 Color: 1
Size: 56 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1741 Color: 1
Size: 225 Color: 0
Size: 34 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 1
Size: 164 Color: 0
Size: 58 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 0
Size: 187 Color: 1
Size: 36 Color: 0

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 1794 Color: 1
Size: 182 Color: 0
Size: 16 Color: 0
Size: 8 Color: 1

Bin 17: 1 of cap free
Amount of items: 5
Items: 
Size: 1001 Color: 0
Size: 442 Color: 1
Size: 262 Color: 0
Size: 166 Color: 0
Size: 128 Color: 1

Bin 18: 1 of cap free
Amount of items: 4
Items: 
Size: 1081 Color: 1
Size: 808 Color: 0
Size: 64 Color: 1
Size: 46 Color: 0

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1230 Color: 0
Size: 669 Color: 1
Size: 100 Color: 0

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1384 Color: 0
Size: 571 Color: 0
Size: 44 Color: 1

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 498 Color: 1
Size: 96 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1401 Color: 0
Size: 562 Color: 1
Size: 36 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 1
Size: 398 Color: 0
Size: 60 Color: 0

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 1
Size: 344 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 0
Size: 220 Color: 0
Size: 96 Color: 1

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1717 Color: 1
Size: 282 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1724 Color: 1
Size: 211 Color: 0
Size: 64 Color: 1

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 1
Size: 241 Color: 0

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1515 Color: 0
Size: 431 Color: 1
Size: 52 Color: 0

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 1707 Color: 0
Size: 291 Color: 1

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1002 Color: 1
Size: 907 Color: 1
Size: 88 Color: 0

Bin 32: 3 of cap free
Amount of items: 4
Items: 
Size: 1303 Color: 0
Size: 457 Color: 1
Size: 197 Color: 1
Size: 40 Color: 0

Bin 33: 3 of cap free
Amount of items: 2
Items: 
Size: 1592 Color: 0
Size: 405 Color: 1

Bin 34: 4 of cap free
Amount of items: 3
Items: 
Size: 1326 Color: 1
Size: 460 Color: 1
Size: 210 Color: 0

Bin 35: 4 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 0
Size: 302 Color: 1
Size: 52 Color: 1

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 0
Size: 330 Color: 1

Bin 37: 4 of cap free
Amount of items: 2
Items: 
Size: 1750 Color: 0
Size: 246 Color: 1

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 1
Size: 213 Color: 0

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 0
Size: 202 Color: 1
Size: 8 Color: 1

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 0
Size: 642 Color: 1
Size: 40 Color: 0

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 1
Size: 230 Color: 0

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 1
Size: 767 Color: 0
Size: 154 Color: 0

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1417 Color: 0
Size: 577 Color: 1

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 1
Size: 295 Color: 1
Size: 132 Color: 0

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1651 Color: 0
Size: 343 Color: 1

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 1
Size: 308 Color: 0

Bin 47: 6 of cap free
Amount of items: 2
Items: 
Size: 1749 Color: 0
Size: 245 Color: 1

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1726 Color: 0
Size: 265 Color: 1

Bin 49: 10 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 1
Size: 581 Color: 0

Bin 50: 10 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 1
Size: 361 Color: 0

Bin 51: 12 of cap free
Amount of items: 2
Items: 
Size: 1725 Color: 0
Size: 263 Color: 1

Bin 52: 14 of cap free
Amount of items: 2
Items: 
Size: 1485 Color: 0
Size: 501 Color: 1

Bin 53: 17 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 1
Size: 493 Color: 0
Size: 181 Color: 0

Bin 54: 18 of cap free
Amount of items: 2
Items: 
Size: 1205 Color: 0
Size: 777 Color: 1

Bin 55: 19 of cap free
Amount of items: 2
Items: 
Size: 1745 Color: 1
Size: 236 Color: 0

Bin 56: 20 of cap free
Amount of items: 21
Items: 
Size: 154 Color: 1
Size: 152 Color: 0
Size: 132 Color: 1
Size: 128 Color: 1
Size: 116 Color: 1
Size: 114 Color: 1
Size: 112 Color: 1
Size: 112 Color: 0
Size: 98 Color: 1
Size: 98 Color: 1
Size: 88 Color: 1
Size: 84 Color: 0
Size: 80 Color: 0
Size: 76 Color: 1
Size: 76 Color: 0
Size: 72 Color: 0
Size: 64 Color: 0
Size: 64 Color: 0
Size: 56 Color: 0
Size: 52 Color: 0
Size: 52 Color: 0

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 1479 Color: 1
Size: 501 Color: 0

Bin 58: 21 of cap free
Amount of items: 2
Items: 
Size: 1406 Color: 0
Size: 573 Color: 1

Bin 59: 21 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 0
Size: 237 Color: 1

Bin 60: 23 of cap free
Amount of items: 4
Items: 
Size: 1005 Color: 0
Size: 738 Color: 0
Size: 186 Color: 1
Size: 48 Color: 1

Bin 61: 26 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 1
Size: 289 Color: 0

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 1199 Color: 0
Size: 773 Color: 1

Bin 63: 28 of cap free
Amount of items: 2
Items: 
Size: 1589 Color: 0
Size: 383 Color: 1

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1134 Color: 0
Size: 834 Color: 1

Bin 65: 33 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 1
Size: 497 Color: 0

Bin 66: 1544 of cap free
Amount of items: 10
Items: 
Size: 72 Color: 1
Size: 52 Color: 0
Size: 48 Color: 0
Size: 46 Color: 0
Size: 44 Color: 1
Size: 40 Color: 1
Size: 40 Color: 1
Size: 40 Color: 0
Size: 38 Color: 0
Size: 36 Color: 1

Total size: 130000
Total free space: 2000

