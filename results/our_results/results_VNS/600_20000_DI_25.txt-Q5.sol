Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 22
Items: 
Size: 984 Color: 1
Size: 976 Color: 1
Size: 942 Color: 1
Size: 904 Color: 2
Size: 850 Color: 0
Size: 844 Color: 0
Size: 816 Color: 1
Size: 788 Color: 1
Size: 788 Color: 1
Size: 776 Color: 3
Size: 764 Color: 4
Size: 732 Color: 4
Size: 710 Color: 3
Size: 692 Color: 0
Size: 660 Color: 2
Size: 618 Color: 3
Size: 608 Color: 2
Size: 608 Color: 2
Size: 528 Color: 3
Size: 512 Color: 3
Size: 512 Color: 0
Size: 388 Color: 3

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 8008 Color: 1
Size: 2450 Color: 1
Size: 2400 Color: 0
Size: 1772 Color: 3
Size: 1370 Color: 2

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 8010 Color: 4
Size: 3614 Color: 2
Size: 2964 Color: 1
Size: 864 Color: 4
Size: 548 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8936 Color: 1
Size: 6664 Color: 0
Size: 400 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9131 Color: 4
Size: 6133 Color: 0
Size: 736 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10946 Color: 3
Size: 4652 Color: 0
Size: 402 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11666 Color: 0
Size: 3894 Color: 4
Size: 440 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11652 Color: 4
Size: 4044 Color: 0
Size: 304 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11828 Color: 0
Size: 3682 Color: 2
Size: 490 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 4
Size: 3752 Color: 1
Size: 352 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 2
Size: 3208 Color: 0
Size: 624 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12230 Color: 0
Size: 3434 Color: 4
Size: 336 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12300 Color: 0
Size: 2642 Color: 4
Size: 1058 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12740 Color: 0
Size: 2444 Color: 4
Size: 816 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12743 Color: 4
Size: 2715 Color: 3
Size: 542 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 2
Size: 2724 Color: 1
Size: 520 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12847 Color: 0
Size: 2417 Color: 4
Size: 736 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 0
Size: 2728 Color: 1
Size: 368 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 0
Size: 2722 Color: 3
Size: 312 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 1
Size: 2708 Color: 3
Size: 276 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13053 Color: 3
Size: 2011 Color: 1
Size: 936 Color: 2

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13062 Color: 3
Size: 1556 Color: 2
Size: 1382 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13158 Color: 1
Size: 2530 Color: 3
Size: 312 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13274 Color: 1
Size: 2318 Color: 3
Size: 408 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 3
Size: 2136 Color: 1
Size: 528 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13308 Color: 1
Size: 2356 Color: 3
Size: 336 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13365 Color: 2
Size: 2403 Color: 1
Size: 232 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13370 Color: 1
Size: 1870 Color: 3
Size: 760 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13388 Color: 3
Size: 2180 Color: 0
Size: 432 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13492 Color: 3
Size: 1684 Color: 2
Size: 824 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 3
Size: 1942 Color: 1
Size: 544 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13519 Color: 2
Size: 1853 Color: 1
Size: 628 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13564 Color: 4
Size: 1784 Color: 3
Size: 652 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13587 Color: 0
Size: 1961 Color: 3
Size: 452 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13624 Color: 2
Size: 1280 Color: 2
Size: 1096 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 3
Size: 1844 Color: 0
Size: 436 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13670 Color: 2
Size: 2206 Color: 3
Size: 124 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13714 Color: 2
Size: 1718 Color: 0
Size: 568 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13749 Color: 4
Size: 2155 Color: 4
Size: 96 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 3
Size: 1741 Color: 0
Size: 482 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13774 Color: 0
Size: 1328 Color: 4
Size: 898 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 3
Size: 1736 Color: 2
Size: 432 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 3
Size: 1988 Color: 2
Size: 112 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13942 Color: 3
Size: 1570 Color: 0
Size: 488 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13972 Color: 3
Size: 1356 Color: 1
Size: 672 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13980 Color: 3
Size: 1332 Color: 4
Size: 688 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13909 Color: 4
Size: 1563 Color: 2
Size: 528 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13944 Color: 1
Size: 1368 Color: 3
Size: 688 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13997 Color: 3
Size: 1671 Color: 0
Size: 332 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 3
Size: 1626 Color: 4
Size: 352 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14024 Color: 0
Size: 1192 Color: 1
Size: 784 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14066 Color: 2
Size: 1116 Color: 3
Size: 818 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 4
Size: 1300 Color: 3
Size: 624 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14106 Color: 1
Size: 1394 Color: 3
Size: 500 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 3
Size: 1720 Color: 0
Size: 140 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 3
Size: 1152 Color: 0
Size: 648 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 3
Size: 1136 Color: 4
Size: 616 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 3
Size: 1000 Color: 2
Size: 732 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14242 Color: 4
Size: 1444 Color: 2
Size: 314 Color: 3

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 1
Size: 1404 Color: 4
Size: 272 Color: 4

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14380 Color: 3
Size: 1328 Color: 0
Size: 292 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14346 Color: 4
Size: 1332 Color: 3
Size: 322 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 1
Size: 1152 Color: 3
Size: 488 Color: 4

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 9545 Color: 2
Size: 6070 Color: 1
Size: 384 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 10605 Color: 0
Size: 4938 Color: 1
Size: 456 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 11081 Color: 4
Size: 4550 Color: 4
Size: 368 Color: 2

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 0
Size: 3823 Color: 3
Size: 396 Color: 3

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 12078 Color: 3
Size: 3921 Color: 1

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12283 Color: 0
Size: 2452 Color: 3
Size: 1264 Color: 3

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12347 Color: 4
Size: 3300 Color: 0
Size: 352 Color: 2

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 1
Size: 3045 Color: 0
Size: 380 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12611 Color: 4
Size: 3084 Color: 3
Size: 304 Color: 0

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12685 Color: 2
Size: 1986 Color: 0
Size: 1328 Color: 4

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12788 Color: 0
Size: 1638 Color: 3
Size: 1573 Color: 2

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 13101 Color: 0
Size: 2626 Color: 3
Size: 272 Color: 1

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 13945 Color: 4
Size: 2054 Color: 0

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 14113 Color: 0
Size: 1582 Color: 3
Size: 304 Color: 4

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 9502 Color: 3
Size: 5968 Color: 2
Size: 528 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 10558 Color: 3
Size: 5048 Color: 0
Size: 392 Color: 1

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 1
Size: 5014 Color: 0
Size: 192 Color: 2

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 10897 Color: 1
Size: 4717 Color: 0
Size: 384 Color: 1

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 11250 Color: 0
Size: 3056 Color: 4
Size: 1692 Color: 2

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 11512 Color: 1
Size: 3962 Color: 0
Size: 524 Color: 2

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 11586 Color: 1
Size: 4088 Color: 1
Size: 324 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 12101 Color: 4
Size: 3469 Color: 0
Size: 428 Color: 2

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 0
Size: 2906 Color: 1
Size: 744 Color: 2

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 3
Size: 2754 Color: 2
Size: 800 Color: 0

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 12986 Color: 4
Size: 1548 Color: 1
Size: 1464 Color: 3

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 2
Size: 1948 Color: 1

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 4
Size: 1930 Color: 0

Bin 91: 3 of cap free
Amount of items: 7
Items: 
Size: 8003 Color: 4
Size: 2147 Color: 4
Size: 2069 Color: 1
Size: 1466 Color: 1
Size: 1328 Color: 3
Size: 696 Color: 0
Size: 288 Color: 4

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 10341 Color: 0
Size: 5336 Color: 2
Size: 320 Color: 0

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 10340 Color: 3
Size: 5297 Color: 0
Size: 360 Color: 1

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 11413 Color: 3
Size: 4584 Color: 4

Bin 95: 3 of cap free
Amount of items: 3
Items: 
Size: 13253 Color: 2
Size: 1992 Color: 3
Size: 752 Color: 0

Bin 96: 3 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 2
Size: 1877 Color: 4

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 8197 Color: 3
Size: 7459 Color: 4
Size: 340 Color: 1

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 11288 Color: 2
Size: 4708 Color: 4

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 1
Size: 2936 Color: 0

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 13300 Color: 4
Size: 2488 Color: 0
Size: 208 Color: 0

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 9164 Color: 1
Size: 6503 Color: 2
Size: 328 Color: 0

Bin 102: 5 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 1
Size: 3228 Color: 0
Size: 928 Color: 3

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 4
Size: 3251 Color: 1

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 12800 Color: 3
Size: 2763 Color: 4
Size: 432 Color: 0

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 2
Size: 2457 Color: 1

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 4
Size: 2162 Color: 0

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 14083 Color: 1
Size: 1912 Color: 2

Bin 108: 6 of cap free
Amount of items: 3
Items: 
Size: 11297 Color: 0
Size: 4497 Color: 3
Size: 200 Color: 4

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 12102 Color: 0
Size: 3432 Color: 2
Size: 460 Color: 4

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 12338 Color: 4
Size: 3656 Color: 1

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 14148 Color: 1
Size: 1846 Color: 2

Bin 112: 7 of cap free
Amount of items: 7
Items: 
Size: 8002 Color: 0
Size: 1807 Color: 4
Size: 1672 Color: 4
Size: 1490 Color: 4
Size: 1302 Color: 2
Size: 1000 Color: 3
Size: 720 Color: 2

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 10068 Color: 2
Size: 3555 Color: 0
Size: 2370 Color: 0

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 13793 Color: 4
Size: 2200 Color: 0

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 11580 Color: 0
Size: 4092 Color: 1
Size: 320 Color: 1

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 12850 Color: 3
Size: 3142 Color: 4

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 13448 Color: 1
Size: 2544 Color: 2

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 11266 Color: 3
Size: 4724 Color: 4

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 0
Size: 2232 Color: 4

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13954 Color: 1
Size: 2036 Color: 0

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 0
Size: 4981 Color: 4
Size: 504 Color: 2

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 12890 Color: 2
Size: 3099 Color: 3

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 2
Size: 2913 Color: 4

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 9988 Color: 4
Size: 5092 Color: 1
Size: 908 Color: 2

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 10356 Color: 0
Size: 5404 Color: 1
Size: 228 Color: 3

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 11735 Color: 1
Size: 4253 Color: 4

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 4
Size: 2302 Color: 1

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 14338 Color: 2
Size: 1650 Color: 1

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 4
Size: 2633 Color: 2

Bin 130: 14 of cap free
Amount of items: 3
Items: 
Size: 8408 Color: 4
Size: 6666 Color: 2
Size: 912 Color: 3

Bin 131: 14 of cap free
Amount of items: 3
Items: 
Size: 10062 Color: 1
Size: 5700 Color: 0
Size: 224 Color: 3

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 2
Size: 4950 Color: 4

Bin 133: 14 of cap free
Amount of items: 3
Items: 
Size: 11534 Color: 1
Size: 4164 Color: 3
Size: 288 Color: 2

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 13302 Color: 4
Size: 2684 Color: 1

Bin 135: 14 of cap free
Amount of items: 3
Items: 
Size: 14330 Color: 4
Size: 1604 Color: 2
Size: 52 Color: 3

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 0
Size: 2197 Color: 2

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14125 Color: 4
Size: 1859 Color: 1

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 3
Size: 3270 Color: 1

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 2
Size: 2744 Color: 0

Bin 140: 18 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 1
Size: 2584 Color: 2

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 0
Size: 2252 Color: 1
Size: 112 Color: 0

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 1
Size: 2016 Color: 4
Size: 36 Color: 3

Bin 143: 20 of cap free
Amount of items: 2
Items: 
Size: 14214 Color: 1
Size: 1766 Color: 2

Bin 144: 21 of cap free
Amount of items: 3
Items: 
Size: 10078 Color: 3
Size: 5725 Color: 2
Size: 176 Color: 3

Bin 145: 22 of cap free
Amount of items: 3
Items: 
Size: 8814 Color: 2
Size: 6668 Color: 0
Size: 496 Color: 4

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 8040 Color: 2
Size: 7936 Color: 0

Bin 147: 24 of cap free
Amount of items: 3
Items: 
Size: 11096 Color: 4
Size: 4538 Color: 3
Size: 342 Color: 0

Bin 148: 24 of cap free
Amount of items: 2
Items: 
Size: 14118 Color: 4
Size: 1858 Color: 0

Bin 149: 25 of cap free
Amount of items: 2
Items: 
Size: 13005 Color: 4
Size: 2970 Color: 1

Bin 150: 25 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 2
Size: 2858 Color: 1

Bin 151: 27 of cap free
Amount of items: 2
Items: 
Size: 9645 Color: 1
Size: 6328 Color: 3

Bin 152: 28 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 2
Size: 6660 Color: 3
Size: 296 Color: 0

Bin 153: 28 of cap free
Amount of items: 3
Items: 
Size: 11330 Color: 3
Size: 3130 Color: 0
Size: 1512 Color: 1

Bin 154: 30 of cap free
Amount of items: 2
Items: 
Size: 14358 Color: 4
Size: 1612 Color: 0

Bin 155: 32 of cap free
Amount of items: 2
Items: 
Size: 9432 Color: 1
Size: 6536 Color: 3

Bin 156: 32 of cap free
Amount of items: 2
Items: 
Size: 11624 Color: 3
Size: 4344 Color: 4

Bin 157: 32 of cap free
Amount of items: 2
Items: 
Size: 12022 Color: 2
Size: 3946 Color: 3

Bin 158: 32 of cap free
Amount of items: 2
Items: 
Size: 12246 Color: 1
Size: 3722 Color: 2

Bin 159: 32 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 2
Size: 2092 Color: 4

Bin 160: 33 of cap free
Amount of items: 2
Items: 
Size: 11866 Color: 2
Size: 4101 Color: 3

Bin 161: 35 of cap free
Amount of items: 2
Items: 
Size: 13773 Color: 4
Size: 2192 Color: 1

Bin 162: 36 of cap free
Amount of items: 2
Items: 
Size: 9986 Color: 4
Size: 5978 Color: 1

Bin 163: 36 of cap free
Amount of items: 2
Items: 
Size: 10825 Color: 2
Size: 5139 Color: 3

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 4
Size: 3684 Color: 3

Bin 165: 36 of cap free
Amount of items: 2
Items: 
Size: 12440 Color: 3
Size: 3524 Color: 4

Bin 166: 38 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 4
Size: 3484 Color: 0
Size: 1386 Color: 2

Bin 167: 40 of cap free
Amount of items: 2
Items: 
Size: 10542 Color: 4
Size: 5418 Color: 3

Bin 168: 40 of cap free
Amount of items: 2
Items: 
Size: 12514 Color: 3
Size: 3446 Color: 1

Bin 169: 41 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 4
Size: 2291 Color: 2

Bin 170: 43 of cap free
Amount of items: 3
Items: 
Size: 9076 Color: 4
Size: 6099 Color: 1
Size: 782 Color: 0

Bin 171: 44 of cap free
Amount of items: 2
Items: 
Size: 13882 Color: 4
Size: 2074 Color: 1

Bin 172: 49 of cap free
Amount of items: 2
Items: 
Size: 12187 Color: 3
Size: 3764 Color: 2

Bin 173: 50 of cap free
Amount of items: 2
Items: 
Size: 12632 Color: 3
Size: 3318 Color: 4

Bin 174: 52 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 1
Size: 3250 Color: 2

Bin 175: 54 of cap free
Amount of items: 2
Items: 
Size: 12834 Color: 1
Size: 3112 Color: 3

Bin 176: 56 of cap free
Amount of items: 2
Items: 
Size: 12976 Color: 1
Size: 2968 Color: 4

Bin 177: 58 of cap free
Amount of items: 2
Items: 
Size: 10930 Color: 2
Size: 5012 Color: 4

Bin 178: 61 of cap free
Amount of items: 2
Items: 
Size: 13425 Color: 0
Size: 2514 Color: 2

Bin 179: 74 of cap free
Amount of items: 2
Items: 
Size: 12260 Color: 2
Size: 3666 Color: 3

Bin 180: 77 of cap free
Amount of items: 2
Items: 
Size: 9410 Color: 2
Size: 6513 Color: 4

Bin 181: 79 of cap free
Amount of items: 2
Items: 
Size: 13647 Color: 1
Size: 2274 Color: 4

Bin 182: 83 of cap free
Amount of items: 2
Items: 
Size: 12738 Color: 2
Size: 3179 Color: 1

Bin 183: 86 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 1
Size: 5494 Color: 3

Bin 184: 90 of cap free
Amount of items: 33
Items: 
Size: 720 Color: 1
Size: 648 Color: 1
Size: 634 Color: 1
Size: 592 Color: 4
Size: 592 Color: 2
Size: 584 Color: 1
Size: 580 Color: 4
Size: 552 Color: 0
Size: 540 Color: 4
Size: 540 Color: 1
Size: 536 Color: 2
Size: 536 Color: 2
Size: 524 Color: 0
Size: 498 Color: 3
Size: 480 Color: 4
Size: 480 Color: 1
Size: 480 Color: 0
Size: 476 Color: 3
Size: 472 Color: 3
Size: 448 Color: 4
Size: 438 Color: 3
Size: 432 Color: 4
Size: 432 Color: 3
Size: 416 Color: 4
Size: 416 Color: 0
Size: 412 Color: 3
Size: 384 Color: 0
Size: 374 Color: 0
Size: 372 Color: 0
Size: 370 Color: 2
Size: 336 Color: 2
Size: 312 Color: 2
Size: 304 Color: 2

Bin 185: 103 of cap free
Amount of items: 5
Items: 
Size: 8004 Color: 0
Size: 2194 Color: 4
Size: 2170 Color: 1
Size: 1816 Color: 1
Size: 1713 Color: 3

Bin 186: 106 of cap free
Amount of items: 2
Items: 
Size: 11668 Color: 3
Size: 4226 Color: 4

Bin 187: 128 of cap free
Amount of items: 2
Items: 
Size: 10392 Color: 2
Size: 5480 Color: 1

Bin 188: 135 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 4
Size: 2497 Color: 0

Bin 189: 136 of cap free
Amount of items: 3
Items: 
Size: 8012 Color: 3
Size: 6708 Color: 0
Size: 1144 Color: 2

Bin 190: 137 of cap free
Amount of items: 9
Items: 
Size: 8001 Color: 0
Size: 1088 Color: 0
Size: 1080 Color: 4
Size: 994 Color: 4
Size: 992 Color: 2
Size: 988 Color: 0
Size: 936 Color: 3
Size: 936 Color: 2
Size: 848 Color: 3

Bin 191: 159 of cap free
Amount of items: 2
Items: 
Size: 8185 Color: 4
Size: 7656 Color: 0

Bin 192: 168 of cap free
Amount of items: 2
Items: 
Size: 11602 Color: 1
Size: 4230 Color: 3

Bin 193: 190 of cap free
Amount of items: 3
Items: 
Size: 10025 Color: 2
Size: 3928 Color: 3
Size: 1857 Color: 0

Bin 194: 194 of cap free
Amount of items: 2
Items: 
Size: 8830 Color: 4
Size: 6976 Color: 0

Bin 195: 194 of cap free
Amount of items: 2
Items: 
Size: 9141 Color: 2
Size: 6665 Color: 3

Bin 196: 208 of cap free
Amount of items: 2
Items: 
Size: 9960 Color: 0
Size: 5832 Color: 1

Bin 197: 254 of cap free
Amount of items: 2
Items: 
Size: 9084 Color: 2
Size: 6662 Color: 3

Bin 198: 256 of cap free
Amount of items: 2
Items: 
Size: 9972 Color: 2
Size: 5772 Color: 1

Bin 199: 11326 of cap free
Amount of items: 15
Items: 
Size: 412 Color: 3
Size: 370 Color: 4
Size: 368 Color: 4
Size: 352 Color: 3
Size: 336 Color: 3
Size: 336 Color: 1
Size: 296 Color: 0
Size: 288 Color: 2
Size: 288 Color: 0
Size: 276 Color: 1
Size: 272 Color: 3
Size: 272 Color: 2
Size: 272 Color: 2
Size: 272 Color: 0
Size: 264 Color: 2

Total size: 3168000
Total free space: 16000

