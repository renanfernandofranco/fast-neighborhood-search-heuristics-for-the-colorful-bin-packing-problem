Capicity Bin: 8184
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 29
Items: 
Size: 428 Color: 2
Size: 420 Color: 16
Size: 408 Color: 9
Size: 376 Color: 19
Size: 366 Color: 10
Size: 360 Color: 9
Size: 336 Color: 3
Size: 336 Color: 0
Size: 312 Color: 13
Size: 312 Color: 7
Size: 304 Color: 0
Size: 300 Color: 11
Size: 296 Color: 1
Size: 280 Color: 15
Size: 272 Color: 11
Size: 264 Color: 14
Size: 262 Color: 15
Size: 248 Color: 16
Size: 224 Color: 12
Size: 224 Color: 5
Size: 224 Color: 4
Size: 216 Color: 10
Size: 216 Color: 3
Size: 208 Color: 1
Size: 206 Color: 6
Size: 202 Color: 17
Size: 200 Color: 15
Size: 200 Color: 8
Size: 184 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4487 Color: 5
Size: 3081 Color: 3
Size: 616 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 1
Size: 3252 Color: 13
Size: 176 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5285 Color: 14
Size: 2417 Color: 3
Size: 482 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5562 Color: 13
Size: 2142 Color: 15
Size: 480 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5617 Color: 8
Size: 2103 Color: 11
Size: 464 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5914 Color: 3
Size: 2092 Color: 13
Size: 178 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5989 Color: 12
Size: 1831 Color: 18
Size: 364 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6162 Color: 9
Size: 1510 Color: 19
Size: 512 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 18
Size: 1450 Color: 19
Size: 370 Color: 12

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6366 Color: 2
Size: 1014 Color: 8
Size: 804 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 8
Size: 1252 Color: 6
Size: 440 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 19
Size: 938 Color: 5
Size: 640 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6684 Color: 6
Size: 874 Color: 18
Size: 626 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6687 Color: 16
Size: 1249 Color: 7
Size: 248 Color: 13

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 1
Size: 846 Color: 9
Size: 456 Color: 7

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 15
Size: 1186 Color: 3
Size: 74 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6970 Color: 14
Size: 914 Color: 0
Size: 300 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6973 Color: 13
Size: 835 Color: 6
Size: 376 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 16
Size: 862 Color: 11
Size: 260 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7076 Color: 0
Size: 588 Color: 15
Size: 520 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7138 Color: 13
Size: 680 Color: 11
Size: 366 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7140 Color: 2
Size: 754 Color: 4
Size: 290 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7146 Color: 1
Size: 542 Color: 8
Size: 496 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 8
Size: 748 Color: 11
Size: 248 Color: 10

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7194 Color: 12
Size: 754 Color: 16
Size: 236 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7202 Color: 8
Size: 822 Color: 9
Size: 160 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7247 Color: 4
Size: 781 Color: 18
Size: 156 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7277 Color: 9
Size: 757 Color: 12
Size: 150 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 2
Size: 764 Color: 9
Size: 144 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7330 Color: 5
Size: 428 Color: 9
Size: 426 Color: 8

Bin 32: 1 of cap free
Amount of items: 5
Items: 
Size: 4098 Color: 15
Size: 2338 Color: 15
Size: 1315 Color: 4
Size: 284 Color: 1
Size: 148 Color: 2

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 4475 Color: 9
Size: 3124 Color: 18
Size: 584 Color: 14

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 19
Size: 2141 Color: 13
Size: 424 Color: 10

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5981 Color: 0
Size: 1686 Color: 1
Size: 516 Color: 4

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6528 Color: 19
Size: 1427 Color: 3
Size: 228 Color: 8

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6607 Color: 17
Size: 1518 Color: 19
Size: 58 Color: 12

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 14
Size: 744 Color: 18
Size: 624 Color: 10

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6828 Color: 4
Size: 1187 Color: 3
Size: 168 Color: 7

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6923 Color: 10
Size: 1092 Color: 18
Size: 168 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 7027 Color: 16
Size: 924 Color: 10
Size: 232 Color: 3

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 7183 Color: 11
Size: 940 Color: 17
Size: 60 Color: 2

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 7292 Color: 8
Size: 891 Color: 17

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 7309 Color: 18
Size: 618 Color: 13
Size: 256 Color: 6

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7364 Color: 19
Size: 819 Color: 11

Bin 46: 2 of cap free
Amount of items: 17
Items: 
Size: 672 Color: 12
Size: 600 Color: 7
Size: 596 Color: 5
Size: 588 Color: 18
Size: 568 Color: 12
Size: 540 Color: 4
Size: 528 Color: 2
Size: 508 Color: 19
Size: 508 Color: 10
Size: 464 Color: 8
Size: 436 Color: 2
Size: 432 Color: 13
Size: 430 Color: 11
Size: 416 Color: 0
Size: 384 Color: 1
Size: 296 Color: 6
Size: 216 Color: 16

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 4443 Color: 1
Size: 3577 Color: 10
Size: 162 Color: 8

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 4708 Color: 6
Size: 3256 Color: 12
Size: 218 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5084 Color: 10
Size: 2942 Color: 16
Size: 156 Color: 15

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5301 Color: 15
Size: 2719 Color: 3
Size: 162 Color: 8

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5587 Color: 9
Size: 2403 Color: 12
Size: 192 Color: 7

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 3
Size: 2146 Color: 11
Size: 184 Color: 4

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5910 Color: 4
Size: 1256 Color: 11
Size: 1016 Color: 4

Bin 54: 3 of cap free
Amount of items: 4
Items: 
Size: 4100 Color: 3
Size: 2930 Color: 5
Size: 1011 Color: 13
Size: 140 Color: 5

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 5649 Color: 1
Size: 2356 Color: 12
Size: 176 Color: 9

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 6233 Color: 6
Size: 1690 Color: 17
Size: 258 Color: 2

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 5
Size: 1291 Color: 3
Size: 144 Color: 9

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 7115 Color: 9
Size: 1018 Color: 4
Size: 48 Color: 8

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6243 Color: 5
Size: 1841 Color: 3
Size: 96 Color: 9

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 18
Size: 876 Color: 17

Bin 61: 5 of cap free
Amount of items: 2
Items: 
Size: 7038 Color: 1
Size: 1141 Color: 12

Bin 62: 5 of cap free
Amount of items: 2
Items: 
Size: 7127 Color: 16
Size: 1052 Color: 6

Bin 63: 6 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 8
Size: 2626 Color: 13
Size: 188 Color: 9

Bin 64: 6 of cap free
Amount of items: 2
Items: 
Size: 5382 Color: 6
Size: 2796 Color: 10

Bin 65: 6 of cap free
Amount of items: 3
Items: 
Size: 6374 Color: 0
Size: 1090 Color: 3
Size: 714 Color: 2

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 5
Size: 1498 Color: 0

Bin 67: 6 of cap free
Amount of items: 2
Items: 
Size: 7220 Color: 15
Size: 958 Color: 0

Bin 68: 6 of cap free
Amount of items: 2
Items: 
Size: 7342 Color: 0
Size: 836 Color: 3

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 6390 Color: 12
Size: 1719 Color: 0
Size: 68 Color: 17

Bin 70: 8 of cap free
Amount of items: 3
Items: 
Size: 4444 Color: 17
Size: 3592 Color: 18
Size: 140 Color: 13

Bin 71: 8 of cap free
Amount of items: 3
Items: 
Size: 4598 Color: 15
Size: 3406 Color: 19
Size: 172 Color: 2

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 6437 Color: 18
Size: 1739 Color: 16

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 8
Size: 1334 Color: 4
Size: 80 Color: 16

Bin 74: 8 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 18
Size: 1412 Color: 5

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 7053 Color: 4
Size: 1123 Color: 7

Bin 76: 9 of cap free
Amount of items: 3
Items: 
Size: 6635 Color: 7
Size: 1132 Color: 13
Size: 408 Color: 3

Bin 77: 10 of cap free
Amount of items: 2
Items: 
Size: 7154 Color: 18
Size: 1020 Color: 19

Bin 78: 11 of cap free
Amount of items: 7
Items: 
Size: 4093 Color: 8
Size: 798 Color: 8
Size: 684 Color: 12
Size: 680 Color: 5
Size: 680 Color: 2
Size: 622 Color: 19
Size: 616 Color: 0

Bin 79: 11 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 12
Size: 2372 Color: 17
Size: 200 Color: 9

Bin 80: 11 of cap free
Amount of items: 2
Items: 
Size: 6473 Color: 18
Size: 1700 Color: 19

Bin 81: 11 of cap free
Amount of items: 2
Items: 
Size: 6546 Color: 5
Size: 1627 Color: 12

Bin 82: 11 of cap free
Amount of items: 2
Items: 
Size: 7230 Color: 11
Size: 943 Color: 12

Bin 83: 12 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 4
Size: 1564 Color: 7
Size: 76 Color: 5

Bin 84: 12 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 7
Size: 1112 Color: 5

Bin 85: 13 of cap free
Amount of items: 3
Items: 
Size: 4923 Color: 8
Size: 3016 Color: 1
Size: 232 Color: 18

Bin 86: 14 of cap free
Amount of items: 3
Items: 
Size: 5134 Color: 9
Size: 2860 Color: 14
Size: 176 Color: 1

Bin 87: 14 of cap free
Amount of items: 2
Items: 
Size: 6646 Color: 19
Size: 1524 Color: 7

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 5078 Color: 6
Size: 3091 Color: 3

Bin 89: 15 of cap free
Amount of items: 2
Items: 
Size: 6878 Color: 4
Size: 1291 Color: 14

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 6966 Color: 10
Size: 1202 Color: 14

Bin 91: 16 of cap free
Amount of items: 2
Items: 
Size: 7203 Color: 12
Size: 965 Color: 9

Bin 92: 17 of cap free
Amount of items: 3
Items: 
Size: 5126 Color: 12
Size: 2705 Color: 16
Size: 336 Color: 13

Bin 93: 17 of cap free
Amount of items: 2
Items: 
Size: 5700 Color: 3
Size: 2467 Color: 5

Bin 94: 17 of cap free
Amount of items: 2
Items: 
Size: 7350 Color: 19
Size: 817 Color: 17

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 4
Size: 2186 Color: 11

Bin 96: 19 of cap free
Amount of items: 2
Items: 
Size: 6941 Color: 17
Size: 1224 Color: 1

Bin 97: 20 of cap free
Amount of items: 2
Items: 
Size: 4124 Color: 15
Size: 4040 Color: 10

Bin 98: 20 of cap free
Amount of items: 2
Items: 
Size: 6311 Color: 17
Size: 1853 Color: 6

Bin 99: 20 of cap free
Amount of items: 2
Items: 
Size: 7282 Color: 7
Size: 882 Color: 0

Bin 100: 21 of cap free
Amount of items: 2
Items: 
Size: 7126 Color: 12
Size: 1037 Color: 7

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 5612 Color: 2
Size: 2550 Color: 0

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 0
Size: 1576 Color: 10

Bin 103: 24 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 2
Size: 3164 Color: 19

Bin 104: 24 of cap free
Amount of items: 2
Items: 
Size: 5500 Color: 10
Size: 2660 Color: 3

Bin 105: 24 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 0
Size: 1948 Color: 6
Size: 64 Color: 6

Bin 106: 25 of cap free
Amount of items: 2
Items: 
Size: 7278 Color: 19
Size: 881 Color: 18

Bin 107: 26 of cap free
Amount of items: 3
Items: 
Size: 6028 Color: 7
Size: 1894 Color: 1
Size: 236 Color: 3

Bin 108: 26 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 9
Size: 1380 Color: 11

Bin 109: 26 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 5
Size: 1282 Color: 9

Bin 110: 29 of cap free
Amount of items: 2
Items: 
Size: 5661 Color: 17
Size: 2494 Color: 3

Bin 111: 29 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 4
Size: 1318 Color: 10

Bin 112: 30 of cap free
Amount of items: 2
Items: 
Size: 4670 Color: 13
Size: 3484 Color: 3

Bin 113: 32 of cap free
Amount of items: 2
Items: 
Size: 5610 Color: 17
Size: 2542 Color: 7

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 6308 Color: 7
Size: 1844 Color: 16

Bin 115: 32 of cap free
Amount of items: 2
Items: 
Size: 6964 Color: 9
Size: 1188 Color: 10

Bin 116: 38 of cap free
Amount of items: 2
Items: 
Size: 5196 Color: 9
Size: 2950 Color: 8

Bin 117: 42 of cap free
Amount of items: 2
Items: 
Size: 5977 Color: 6
Size: 2165 Color: 5

Bin 118: 44 of cap free
Amount of items: 2
Items: 
Size: 5150 Color: 0
Size: 2990 Color: 12

Bin 119: 44 of cap free
Amount of items: 2
Items: 
Size: 6303 Color: 19
Size: 1837 Color: 14

Bin 120: 45 of cap free
Amount of items: 3
Items: 
Size: 4388 Color: 9
Size: 3119 Color: 13
Size: 632 Color: 7

Bin 121: 45 of cap free
Amount of items: 3
Items: 
Size: 4427 Color: 1
Size: 3388 Color: 6
Size: 324 Color: 2

Bin 122: 52 of cap free
Amount of items: 2
Items: 
Size: 6675 Color: 18
Size: 1457 Color: 4

Bin 123: 60 of cap free
Amount of items: 4
Items: 
Size: 4095 Color: 7
Size: 2153 Color: 7
Size: 1174 Color: 11
Size: 702 Color: 6

Bin 124: 62 of cap free
Amount of items: 4
Items: 
Size: 4094 Color: 8
Size: 2076 Color: 13
Size: 1086 Color: 2
Size: 866 Color: 11

Bin 125: 69 of cap free
Amount of items: 2
Items: 
Size: 5961 Color: 10
Size: 2154 Color: 14

Bin 126: 80 of cap free
Amount of items: 2
Items: 
Size: 5514 Color: 1
Size: 2590 Color: 2

Bin 127: 90 of cap free
Amount of items: 2
Items: 
Size: 5602 Color: 19
Size: 2492 Color: 7

Bin 128: 91 of cap free
Amount of items: 3
Items: 
Size: 4288 Color: 7
Size: 2244 Color: 19
Size: 1561 Color: 4

Bin 129: 114 of cap free
Amount of items: 2
Items: 
Size: 4939 Color: 12
Size: 3131 Color: 14

Bin 130: 119 of cap free
Amount of items: 2
Items: 
Size: 4654 Color: 0
Size: 3411 Color: 10

Bin 131: 128 of cap free
Amount of items: 2
Items: 
Size: 4646 Color: 9
Size: 3410 Color: 16

Bin 132: 128 of cap free
Amount of items: 2
Items: 
Size: 6158 Color: 1
Size: 1898 Color: 9

Bin 133: 6006 of cap free
Amount of items: 13
Items: 
Size: 200 Color: 12
Size: 200 Color: 5
Size: 188 Color: 13
Size: 188 Color: 10
Size: 184 Color: 9
Size: 172 Color: 3
Size: 166 Color: 11
Size: 160 Color: 18
Size: 160 Color: 14
Size: 152 Color: 7
Size: 136 Color: 19
Size: 136 Color: 6
Size: 136 Color: 0

Total size: 1080288
Total free space: 8184

