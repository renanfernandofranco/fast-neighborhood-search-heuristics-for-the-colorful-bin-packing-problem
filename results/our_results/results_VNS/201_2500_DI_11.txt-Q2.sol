Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 0
Size: 1022 Color: 1
Size: 204 Color: 0

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 1387 Color: 1
Size: 931 Color: 0
Size: 88 Color: 1
Size: 66 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1486 Color: 0
Size: 818 Color: 0
Size: 168 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1478 Color: 1
Size: 830 Color: 0
Size: 164 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 658 Color: 1
Size: 128 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1748 Color: 0
Size: 684 Color: 1
Size: 40 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 1
Size: 504 Color: 0
Size: 150 Color: 1

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 2030 Color: 1
Size: 366 Color: 0
Size: 56 Color: 1
Size: 20 Color: 0

Bin 9: 0 of cap free
Amount of items: 4
Items: 
Size: 2034 Color: 1
Size: 370 Color: 0
Size: 52 Color: 1
Size: 16 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 0
Size: 272 Color: 0
Size: 164 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 1
Size: 230 Color: 1
Size: 132 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 0
Size: 184 Color: 1
Size: 130 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2179 Color: 0
Size: 233 Color: 1
Size: 60 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 0
Size: 152 Color: 1
Size: 122 Color: 0

Bin 15: 0 of cap free
Amount of items: 5
Items: 
Size: 2212 Color: 1
Size: 220 Color: 0
Size: 24 Color: 0
Size: 8 Color: 1
Size: 8 Color: 1

Bin 16: 1 of cap free
Amount of items: 6
Items: 
Size: 1237 Color: 1
Size: 515 Color: 0
Size: 283 Color: 1
Size: 200 Color: 0
Size: 164 Color: 1
Size: 72 Color: 0

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1529 Color: 0
Size: 846 Color: 0
Size: 96 Color: 1

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 0
Size: 758 Color: 0
Size: 142 Color: 1

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 0
Size: 333 Color: 0
Size: 200 Color: 1

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 1
Size: 475 Color: 1
Size: 62 Color: 0

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1985 Color: 1
Size: 446 Color: 0
Size: 40 Color: 0

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 0
Size: 311 Color: 1
Size: 44 Color: 0

Bin 23: 2 of cap free
Amount of items: 15
Items: 
Size: 275 Color: 1
Size: 268 Color: 1
Size: 259 Color: 1
Size: 196 Color: 0
Size: 176 Color: 0
Size: 176 Color: 0
Size: 160 Color: 1
Size: 156 Color: 1
Size: 144 Color: 0
Size: 136 Color: 0
Size: 132 Color: 1
Size: 128 Color: 0
Size: 112 Color: 0
Size: 102 Color: 1
Size: 50 Color: 0

Bin 24: 2 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 0
Size: 1000 Color: 1

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 1
Size: 662 Color: 0
Size: 146 Color: 1

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 0
Size: 678 Color: 1
Size: 88 Color: 1

Bin 27: 2 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 0
Size: 540 Color: 0
Size: 104 Color: 1

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 2093 Color: 0
Size: 245 Color: 1
Size: 132 Color: 0

Bin 29: 3 of cap free
Amount of items: 3
Items: 
Size: 1262 Color: 1
Size: 1103 Color: 0
Size: 104 Color: 1

Bin 30: 3 of cap free
Amount of items: 3
Items: 
Size: 1652 Color: 1
Size: 751 Color: 0
Size: 66 Color: 0

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 0
Size: 739 Color: 0
Size: 60 Color: 1

Bin 32: 3 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 0
Size: 542 Color: 1
Size: 72 Color: 0

Bin 33: 3 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 0
Size: 441 Color: 1
Size: 88 Color: 1

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 2019 Color: 1
Size: 450 Color: 0

Bin 35: 4 of cap free
Amount of items: 4
Items: 
Size: 1238 Color: 1
Size: 576 Color: 0
Size: 546 Color: 0
Size: 108 Color: 1

Bin 36: 4 of cap free
Amount of items: 2
Items: 
Size: 1458 Color: 0
Size: 1010 Color: 1

Bin 37: 4 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 1
Size: 693 Color: 0
Size: 38 Color: 1

Bin 38: 4 of cap free
Amount of items: 2
Items: 
Size: 2151 Color: 0
Size: 317 Color: 1

Bin 39: 5 of cap free
Amount of items: 3
Items: 
Size: 1254 Color: 1
Size: 1093 Color: 0
Size: 120 Color: 1

Bin 40: 5 of cap free
Amount of items: 2
Items: 
Size: 1797 Color: 0
Size: 670 Color: 1

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1943 Color: 0
Size: 444 Color: 1
Size: 80 Color: 0

Bin 42: 5 of cap free
Amount of items: 3
Items: 
Size: 2163 Color: 1
Size: 272 Color: 0
Size: 32 Color: 1

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1903 Color: 0
Size: 563 Color: 1

Bin 44: 6 of cap free
Amount of items: 2
Items: 
Size: 2164 Color: 1
Size: 302 Color: 0

Bin 45: 7 of cap free
Amount of items: 2
Items: 
Size: 1678 Color: 0
Size: 787 Color: 1

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 2101 Color: 0
Size: 364 Color: 1

Bin 47: 9 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 0
Size: 822 Color: 1
Size: 54 Color: 1

Bin 48: 9 of cap free
Amount of items: 3
Items: 
Size: 2039 Color: 1
Size: 384 Color: 0
Size: 40 Color: 1

Bin 49: 9 of cap free
Amount of items: 2
Items: 
Size: 2201 Color: 1
Size: 262 Color: 0

Bin 50: 12 of cap free
Amount of items: 2
Items: 
Size: 2073 Color: 1
Size: 387 Color: 0

Bin 51: 13 of cap free
Amount of items: 3
Items: 
Size: 1357 Color: 0
Size: 1030 Color: 1
Size: 72 Color: 0

Bin 52: 13 of cap free
Amount of items: 2
Items: 
Size: 2143 Color: 0
Size: 316 Color: 1

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 0
Size: 729 Color: 1

Bin 54: 16 of cap free
Amount of items: 4
Items: 
Size: 1241 Color: 1
Size: 604 Color: 0
Size: 407 Color: 1
Size: 204 Color: 0

Bin 55: 20 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 1
Size: 334 Color: 0

Bin 56: 23 of cap free
Amount of items: 2
Items: 
Size: 1494 Color: 0
Size: 955 Color: 1

Bin 57: 23 of cap free
Amount of items: 2
Items: 
Size: 1828 Color: 0
Size: 621 Color: 1

Bin 58: 24 of cap free
Amount of items: 2
Items: 
Size: 1892 Color: 0
Size: 556 Color: 1

Bin 59: 29 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 0
Size: 838 Color: 0
Size: 361 Color: 1

Bin 60: 31 of cap free
Amount of items: 2
Items: 
Size: 1552 Color: 1
Size: 889 Color: 0

Bin 61: 33 of cap free
Amount of items: 2
Items: 
Size: 1599 Color: 0
Size: 840 Color: 1

Bin 62: 34 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 1
Size: 780 Color: 0

Bin 63: 37 of cap free
Amount of items: 2
Items: 
Size: 1407 Color: 1
Size: 1028 Color: 0

Bin 64: 40 of cap free
Amount of items: 2
Items: 
Size: 1540 Color: 0
Size: 892 Color: 1

Bin 65: 46 of cap free
Amount of items: 2
Items: 
Size: 1404 Color: 1
Size: 1022 Color: 0

Bin 66: 1938 of cap free
Amount of items: 8
Items: 
Size: 94 Color: 1
Size: 88 Color: 1
Size: 88 Color: 1
Size: 72 Color: 1
Size: 64 Color: 0
Size: 48 Color: 0
Size: 42 Color: 0
Size: 38 Color: 0

Total size: 160680
Total free space: 2472

