Capicity Bin: 15744
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 14
Items: 
Size: 7874 Color: 0
Size: 688 Color: 0
Size: 688 Color: 0
Size: 688 Color: 0
Size: 672 Color: 0
Size: 668 Color: 0
Size: 616 Color: 1
Size: 612 Color: 1
Size: 600 Color: 1
Size: 576 Color: 1
Size: 576 Color: 1
Size: 560 Color: 1
Size: 478 Color: 1
Size: 448 Color: 0

Bin 2: 0 of cap free
Amount of items: 11
Items: 
Size: 7876 Color: 1
Size: 880 Color: 0
Size: 872 Color: 0
Size: 864 Color: 1
Size: 864 Color: 0
Size: 830 Color: 1
Size: 820 Color: 1
Size: 808 Color: 1
Size: 808 Color: 0
Size: 738 Color: 0
Size: 384 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7888 Color: 1
Size: 6576 Color: 0
Size: 1280 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 7976 Color: 0
Size: 6488 Color: 0
Size: 1280 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7898 Color: 1
Size: 6542 Color: 0
Size: 1304 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 7912 Color: 1
Size: 6504 Color: 0
Size: 1328 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 8217 Color: 0
Size: 6273 Color: 0
Size: 1254 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 8852 Color: 0
Size: 6536 Color: 0
Size: 356 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 1
Size: 5352 Color: 0
Size: 1120 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 9712 Color: 1
Size: 5692 Color: 1
Size: 340 Color: 0

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 9888 Color: 1
Size: 5856 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 0
Size: 5368 Color: 1
Size: 272 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 10150 Color: 1
Size: 4662 Color: 0
Size: 932 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 1
Size: 4408 Color: 0
Size: 928 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 1
Size: 4554 Color: 0
Size: 436 Color: 1

Bin 16: 0 of cap free
Amount of items: 4
Items: 
Size: 11112 Color: 0
Size: 2212 Color: 1
Size: 2128 Color: 1
Size: 292 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11228 Color: 0
Size: 4040 Color: 1
Size: 476 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11445 Color: 1
Size: 3583 Color: 1
Size: 716 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11696 Color: 0
Size: 3582 Color: 1
Size: 466 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 1
Size: 3400 Color: 1
Size: 288 Color: 0

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 12272 Color: 1
Size: 1784 Color: 0
Size: 1324 Color: 0
Size: 364 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 1
Size: 1736 Color: 0
Size: 1308 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 1
Size: 2540 Color: 1
Size: 424 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12874 Color: 1
Size: 1734 Color: 0
Size: 1136 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13200 Color: 0
Size: 1296 Color: 0
Size: 1248 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 0
Size: 1128 Color: 1
Size: 1008 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 1
Size: 1786 Color: 0
Size: 356 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 1
Size: 1374 Color: 0
Size: 516 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 0
Size: 1442 Color: 1
Size: 288 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 0
Size: 1098 Color: 1
Size: 546 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14118 Color: 1
Size: 1358 Color: 0
Size: 268 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 12187 Color: 1
Size: 3184 Color: 1
Size: 372 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 12873 Color: 1
Size: 1802 Color: 0
Size: 1068 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 12985 Color: 0
Size: 1448 Color: 1
Size: 1310 Color: 1

Bin 35: 1 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 1
Size: 2545 Color: 0

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 13349 Color: 0
Size: 2394 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 13656 Color: 0
Size: 1997 Color: 1
Size: 90 Color: 0

Bin 38: 2 of cap free
Amount of items: 9
Items: 
Size: 7880 Color: 0
Size: 1400 Color: 1
Size: 1368 Color: 1
Size: 1308 Color: 1
Size: 1072 Color: 1
Size: 992 Color: 0
Size: 950 Color: 0
Size: 448 Color: 1
Size: 324 Color: 0

Bin 39: 2 of cap free
Amount of items: 7
Items: 
Size: 7890 Color: 0
Size: 1522 Color: 1
Size: 1466 Color: 1
Size: 1428 Color: 1
Size: 1220 Color: 1
Size: 1144 Color: 0
Size: 1072 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 7892 Color: 1
Size: 6554 Color: 0
Size: 1296 Color: 1

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 12422 Color: 1
Size: 1692 Color: 1
Size: 1628 Color: 0

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 12525 Color: 0
Size: 2737 Color: 1
Size: 480 Color: 0

Bin 43: 2 of cap free
Amount of items: 3
Items: 
Size: 12624 Color: 0
Size: 2838 Color: 1
Size: 280 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 13538 Color: 0
Size: 1372 Color: 1
Size: 832 Color: 1

Bin 45: 2 of cap free
Amount of items: 2
Items: 
Size: 14064 Color: 1
Size: 1678 Color: 0

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 0
Size: 1578 Color: 1

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 8880 Color: 0
Size: 6557 Color: 1
Size: 304 Color: 1

Bin 48: 3 of cap free
Amount of items: 5
Items: 
Size: 9251 Color: 0
Size: 1848 Color: 1
Size: 1728 Color: 0
Size: 1604 Color: 1
Size: 1310 Color: 1

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 11604 Color: 1
Size: 3725 Color: 0
Size: 412 Color: 1

Bin 50: 3 of cap free
Amount of items: 3
Items: 
Size: 12691 Color: 1
Size: 2542 Color: 1
Size: 508 Color: 0

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 0
Size: 2069 Color: 1

Bin 52: 4 of cap free
Amount of items: 7
Items: 
Size: 7882 Color: 1
Size: 1540 Color: 1
Size: 1526 Color: 1
Size: 1524 Color: 1
Size: 1308 Color: 0
Size: 1304 Color: 0
Size: 656 Color: 0

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 10880 Color: 1
Size: 4396 Color: 0
Size: 464 Color: 1

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 12342 Color: 1
Size: 3006 Color: 0
Size: 392 Color: 1

Bin 55: 4 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 1
Size: 3068 Color: 0
Size: 296 Color: 0

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 12444 Color: 0
Size: 2152 Color: 1
Size: 1144 Color: 0

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 1
Size: 2328 Color: 0
Size: 568 Color: 0

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 1
Size: 2414 Color: 0

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 13348 Color: 0
Size: 2392 Color: 1

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 13588 Color: 1
Size: 2152 Color: 0

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 13736 Color: 0
Size: 2004 Color: 1

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 13798 Color: 1
Size: 1942 Color: 0

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 13892 Color: 0
Size: 1848 Color: 1

Bin 64: 4 of cap free
Amount of items: 4
Items: 
Size: 14120 Color: 1
Size: 1424 Color: 0
Size: 132 Color: 1
Size: 64 Color: 0

Bin 65: 5 of cap free
Amount of items: 15
Items: 
Size: 7873 Color: 0
Size: 634 Color: 0
Size: 608 Color: 0
Size: 608 Color: 0
Size: 608 Color: 0
Size: 604 Color: 0
Size: 576 Color: 0
Size: 552 Color: 1
Size: 552 Color: 1
Size: 544 Color: 1
Size: 544 Color: 1
Size: 524 Color: 0
Size: 512 Color: 1
Size: 504 Color: 1
Size: 496 Color: 1

Bin 66: 5 of cap free
Amount of items: 12
Items: 
Size: 7877 Color: 0
Size: 768 Color: 1
Size: 768 Color: 1
Size: 752 Color: 1
Size: 744 Color: 1
Size: 736 Color: 0
Size: 736 Color: 0
Size: 726 Color: 1
Size: 720 Color: 0
Size: 712 Color: 1
Size: 712 Color: 0
Size: 488 Color: 0

Bin 67: 5 of cap free
Amount of items: 3
Items: 
Size: 8874 Color: 0
Size: 6561 Color: 0
Size: 304 Color: 1

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 10129 Color: 1
Size: 4610 Color: 0
Size: 1000 Color: 1

Bin 69: 5 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 0
Size: 4667 Color: 0
Size: 632 Color: 1

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 11692 Color: 1
Size: 4047 Color: 0

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 9690 Color: 1
Size: 5400 Color: 0
Size: 648 Color: 0

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 10081 Color: 1
Size: 5345 Color: 0
Size: 312 Color: 1

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 10472 Color: 0
Size: 5266 Color: 1

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 13262 Color: 0
Size: 2476 Color: 1

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 0
Size: 2420 Color: 1

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 13458 Color: 1
Size: 2280 Color: 0

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 1
Size: 2044 Color: 0

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 13716 Color: 1
Size: 2022 Color: 0

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 1
Size: 1688 Color: 0

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 14098 Color: 1
Size: 1640 Color: 0

Bin 81: 7 of cap free
Amount of items: 10
Items: 
Size: 7881 Color: 1
Size: 1056 Color: 1
Size: 942 Color: 0
Size: 934 Color: 0
Size: 932 Color: 0
Size: 928 Color: 0
Size: 920 Color: 1
Size: 880 Color: 1
Size: 880 Color: 1
Size: 384 Color: 0

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 11782 Color: 0
Size: 3698 Color: 1
Size: 256 Color: 1

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 0
Size: 3452 Color: 1
Size: 280 Color: 0

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 12138 Color: 0
Size: 3358 Color: 1
Size: 240 Color: 0

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 0
Size: 2176 Color: 1
Size: 1120 Color: 1

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 0
Size: 2432 Color: 1

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 13666 Color: 1
Size: 2070 Color: 0

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 13696 Color: 0
Size: 2040 Color: 1

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 13788 Color: 0
Size: 1948 Color: 1

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 1
Size: 2191 Color: 0

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 7885 Color: 1
Size: 6553 Color: 0
Size: 1296 Color: 1

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 8916 Color: 0
Size: 6546 Color: 1
Size: 272 Color: 1

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 11124 Color: 0
Size: 4444 Color: 0
Size: 166 Color: 1

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 11310 Color: 1
Size: 4424 Color: 0

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 1
Size: 4062 Color: 0

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 11048 Color: 1
Size: 4493 Color: 0
Size: 192 Color: 1

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 12937 Color: 1
Size: 2796 Color: 0

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 0
Size: 2301 Color: 1

Bin 99: 12 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 1
Size: 5312 Color: 0

Bin 100: 12 of cap free
Amount of items: 3
Items: 
Size: 12764 Color: 1
Size: 2552 Color: 0
Size: 416 Color: 0

Bin 101: 13 of cap free
Amount of items: 3
Items: 
Size: 10064 Color: 0
Size: 5411 Color: 1
Size: 256 Color: 0

Bin 102: 13 of cap free
Amount of items: 2
Items: 
Size: 12283 Color: 1
Size: 3448 Color: 0

Bin 103: 13 of cap free
Amount of items: 2
Items: 
Size: 12412 Color: 0
Size: 3319 Color: 1

Bin 104: 14 of cap free
Amount of items: 2
Items: 
Size: 13176 Color: 0
Size: 2554 Color: 1

Bin 105: 14 of cap free
Amount of items: 3
Items: 
Size: 13586 Color: 1
Size: 2018 Color: 0
Size: 126 Color: 1

Bin 106: 14 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 0
Size: 1710 Color: 1
Size: 112 Color: 0

Bin 107: 14 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 0
Size: 1744 Color: 1

Bin 108: 15 of cap free
Amount of items: 2
Items: 
Size: 10145 Color: 0
Size: 5584 Color: 1

Bin 109: 15 of cap free
Amount of items: 3
Items: 
Size: 10761 Color: 1
Size: 4712 Color: 0
Size: 256 Color: 0

Bin 110: 15 of cap free
Amount of items: 2
Items: 
Size: 12698 Color: 0
Size: 3031 Color: 1

Bin 111: 15 of cap free
Amount of items: 2
Items: 
Size: 13124 Color: 0
Size: 2605 Color: 1

Bin 112: 16 of cap free
Amount of items: 2
Items: 
Size: 12760 Color: 0
Size: 2968 Color: 1

Bin 113: 18 of cap free
Amount of items: 2
Items: 
Size: 12396 Color: 1
Size: 3330 Color: 0

Bin 114: 19 of cap free
Amount of items: 2
Items: 
Size: 12461 Color: 1
Size: 3264 Color: 0

Bin 115: 20 of cap free
Amount of items: 2
Items: 
Size: 11416 Color: 1
Size: 4308 Color: 0

Bin 116: 20 of cap free
Amount of items: 3
Items: 
Size: 11544 Color: 1
Size: 3764 Color: 0
Size: 416 Color: 0

Bin 117: 20 of cap free
Amount of items: 2
Items: 
Size: 13506 Color: 0
Size: 2218 Color: 1

Bin 118: 21 of cap free
Amount of items: 2
Items: 
Size: 9155 Color: 0
Size: 6568 Color: 1

Bin 119: 21 of cap free
Amount of items: 2
Items: 
Size: 12332 Color: 1
Size: 3391 Color: 0

Bin 120: 22 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 0
Size: 3080 Color: 1

Bin 121: 23 of cap free
Amount of items: 2
Items: 
Size: 13380 Color: 0
Size: 2341 Color: 1

Bin 122: 24 of cap free
Amount of items: 2
Items: 
Size: 13263 Color: 1
Size: 2457 Color: 0

Bin 123: 24 of cap free
Amount of items: 2
Items: 
Size: 13972 Color: 1
Size: 1748 Color: 0

Bin 124: 24 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 0
Size: 1648 Color: 1

Bin 125: 24 of cap free
Amount of items: 4
Items: 
Size: 14156 Color: 1
Size: 1484 Color: 0
Size: 64 Color: 1
Size: 16 Color: 0

Bin 126: 25 of cap free
Amount of items: 2
Items: 
Size: 11855 Color: 1
Size: 3864 Color: 0

Bin 127: 26 of cap free
Amount of items: 2
Items: 
Size: 12938 Color: 0
Size: 2780 Color: 1

Bin 128: 27 of cap free
Amount of items: 2
Items: 
Size: 12109 Color: 1
Size: 3608 Color: 0

Bin 129: 29 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 0
Size: 6107 Color: 0
Size: 272 Color: 1

Bin 130: 30 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 1
Size: 3512 Color: 1
Size: 954 Color: 0

Bin 131: 30 of cap free
Amount of items: 2
Items: 
Size: 11608 Color: 1
Size: 4106 Color: 0

Bin 132: 30 of cap free
Amount of items: 2
Items: 
Size: 13321 Color: 1
Size: 2393 Color: 0

Bin 133: 30 of cap free
Amount of items: 2
Items: 
Size: 14036 Color: 1
Size: 1678 Color: 0

Bin 134: 32 of cap free
Amount of items: 2
Items: 
Size: 11952 Color: 0
Size: 3760 Color: 1

Bin 135: 32 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 1
Size: 2760 Color: 0

Bin 136: 32 of cap free
Amount of items: 2
Items: 
Size: 13022 Color: 0
Size: 2690 Color: 1

Bin 137: 32 of cap free
Amount of items: 2
Items: 
Size: 13328 Color: 0
Size: 2384 Color: 1

Bin 138: 32 of cap free
Amount of items: 2
Items: 
Size: 13522 Color: 1
Size: 2190 Color: 0

Bin 139: 32 of cap free
Amount of items: 2
Items: 
Size: 13680 Color: 0
Size: 2032 Color: 1

Bin 140: 32 of cap free
Amount of items: 2
Items: 
Size: 13784 Color: 0
Size: 1928 Color: 1

Bin 141: 32 of cap free
Amount of items: 2
Items: 
Size: 13844 Color: 0
Size: 1868 Color: 1

Bin 142: 34 of cap free
Amount of items: 2
Items: 
Size: 12812 Color: 0
Size: 2898 Color: 1

Bin 143: 35 of cap free
Amount of items: 3
Items: 
Size: 12696 Color: 0
Size: 2885 Color: 1
Size: 128 Color: 0

Bin 144: 35 of cap free
Amount of items: 2
Items: 
Size: 13376 Color: 0
Size: 2333 Color: 1

Bin 145: 36 of cap free
Amount of items: 2
Items: 
Size: 13092 Color: 1
Size: 2616 Color: 0

Bin 146: 36 of cap free
Amount of items: 2
Items: 
Size: 14160 Color: 0
Size: 1548 Color: 1

Bin 147: 38 of cap free
Amount of items: 2
Items: 
Size: 12058 Color: 0
Size: 3648 Color: 1

Bin 148: 38 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 1
Size: 1972 Color: 0

Bin 149: 39 of cap free
Amount of items: 2
Items: 
Size: 10214 Color: 1
Size: 5491 Color: 0

Bin 150: 39 of cap free
Amount of items: 2
Items: 
Size: 12068 Color: 1
Size: 3637 Color: 0

Bin 151: 40 of cap free
Amount of items: 2
Items: 
Size: 13036 Color: 1
Size: 2668 Color: 0

Bin 152: 41 of cap free
Amount of items: 2
Items: 
Size: 13117 Color: 0
Size: 2586 Color: 1

Bin 153: 42 of cap free
Amount of items: 2
Items: 
Size: 12578 Color: 1
Size: 3124 Color: 0

Bin 154: 44 of cap free
Amount of items: 2
Items: 
Size: 13256 Color: 0
Size: 2444 Color: 1

Bin 155: 45 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 0
Size: 2683 Color: 1

Bin 156: 48 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 1
Size: 2808 Color: 0

Bin 157: 48 of cap free
Amount of items: 2
Items: 
Size: 13508 Color: 1
Size: 2188 Color: 0

Bin 158: 50 of cap free
Amount of items: 2
Items: 
Size: 13086 Color: 1
Size: 2608 Color: 0

Bin 159: 50 of cap free
Amount of items: 3
Items: 
Size: 13242 Color: 0
Size: 2260 Color: 1
Size: 192 Color: 1

Bin 160: 51 of cap free
Amount of items: 2
Items: 
Size: 12797 Color: 0
Size: 2896 Color: 1

Bin 161: 54 of cap free
Amount of items: 3
Items: 
Size: 9708 Color: 0
Size: 5726 Color: 1
Size: 256 Color: 0

Bin 162: 55 of cap free
Amount of items: 2
Items: 
Size: 10033 Color: 1
Size: 5656 Color: 0

Bin 163: 56 of cap free
Amount of items: 2
Items: 
Size: 13652 Color: 1
Size: 2036 Color: 0

Bin 164: 60 of cap free
Amount of items: 2
Items: 
Size: 10704 Color: 0
Size: 4980 Color: 1

Bin 165: 62 of cap free
Amount of items: 2
Items: 
Size: 13412 Color: 0
Size: 2270 Color: 1

Bin 166: 62 of cap free
Amount of items: 2
Items: 
Size: 13776 Color: 0
Size: 1906 Color: 1

Bin 167: 66 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 0
Size: 1760 Color: 1

Bin 168: 68 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 1
Size: 3476 Color: 0

Bin 169: 70 of cap free
Amount of items: 2
Items: 
Size: 13914 Color: 0
Size: 1760 Color: 1

Bin 170: 74 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 1
Size: 1842 Color: 0

Bin 171: 76 of cap free
Amount of items: 2
Items: 
Size: 12912 Color: 0
Size: 2756 Color: 1

Bin 172: 77 of cap free
Amount of items: 2
Items: 
Size: 10017 Color: 1
Size: 5650 Color: 0

Bin 173: 77 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 0
Size: 3241 Color: 1
Size: 1310 Color: 0

Bin 174: 82 of cap free
Amount of items: 2
Items: 
Size: 10889 Color: 0
Size: 4773 Color: 1

Bin 175: 84 of cap free
Amount of items: 35
Items: 
Size: 576 Color: 0
Size: 576 Color: 0
Size: 564 Color: 0
Size: 536 Color: 0
Size: 536 Color: 0
Size: 512 Color: 0
Size: 508 Color: 0
Size: 504 Color: 0
Size: 490 Color: 1
Size: 488 Color: 0
Size: 480 Color: 0
Size: 474 Color: 1
Size: 464 Color: 1
Size: 464 Color: 0
Size: 458 Color: 1
Size: 452 Color: 1
Size: 448 Color: 1
Size: 448 Color: 0
Size: 440 Color: 0
Size: 440 Color: 0
Size: 436 Color: 1
Size: 432 Color: 1
Size: 412 Color: 0
Size: 404 Color: 1
Size: 400 Color: 1
Size: 400 Color: 1
Size: 400 Color: 0
Size: 398 Color: 0
Size: 392 Color: 1
Size: 384 Color: 1
Size: 368 Color: 1
Size: 352 Color: 1
Size: 344 Color: 1
Size: 344 Color: 1
Size: 336 Color: 1

Bin 176: 84 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 1
Size: 1636 Color: 0

Bin 177: 86 of cap free
Amount of items: 2
Items: 
Size: 11450 Color: 1
Size: 4208 Color: 0

Bin 178: 89 of cap free
Amount of items: 2
Items: 
Size: 13013 Color: 0
Size: 2642 Color: 1

Bin 179: 91 of cap free
Amount of items: 3
Items: 
Size: 9331 Color: 0
Size: 4456 Color: 0
Size: 1866 Color: 1

Bin 180: 94 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 1
Size: 2342 Color: 0

Bin 181: 97 of cap free
Amount of items: 2
Items: 
Size: 11791 Color: 1
Size: 3856 Color: 0

Bin 182: 114 of cap free
Amount of items: 3
Items: 
Size: 7944 Color: 0
Size: 6604 Color: 0
Size: 1082 Color: 1

Bin 183: 129 of cap free
Amount of items: 2
Items: 
Size: 11275 Color: 0
Size: 4340 Color: 1

Bin 184: 137 of cap free
Amount of items: 3
Items: 
Size: 9304 Color: 0
Size: 4681 Color: 0
Size: 1622 Color: 1

Bin 185: 149 of cap free
Amount of items: 2
Items: 
Size: 10874 Color: 1
Size: 4721 Color: 0

Bin 186: 152 of cap free
Amount of items: 2
Items: 
Size: 12518 Color: 1
Size: 3074 Color: 0

Bin 187: 158 of cap free
Amount of items: 2
Items: 
Size: 10540 Color: 1
Size: 5046 Color: 0

Bin 188: 165 of cap free
Amount of items: 2
Items: 
Size: 10818 Color: 0
Size: 4761 Color: 1

Bin 189: 174 of cap free
Amount of items: 2
Items: 
Size: 11718 Color: 1
Size: 3852 Color: 0

Bin 190: 206 of cap free
Amount of items: 2
Items: 
Size: 8417 Color: 0
Size: 7121 Color: 1

Bin 191: 210 of cap free
Amount of items: 2
Items: 
Size: 11381 Color: 1
Size: 4153 Color: 0

Bin 192: 212 of cap free
Amount of items: 2
Items: 
Size: 8968 Color: 0
Size: 6564 Color: 1

Bin 193: 215 of cap free
Amount of items: 2
Items: 
Size: 10777 Color: 0
Size: 4752 Color: 1

Bin 194: 216 of cap free
Amount of items: 2
Items: 
Size: 8966 Color: 0
Size: 6562 Color: 1

Bin 195: 218 of cap free
Amount of items: 2
Items: 
Size: 12682 Color: 0
Size: 2844 Color: 1

Bin 196: 228 of cap free
Amount of items: 2
Items: 
Size: 9772 Color: 1
Size: 5744 Color: 0

Bin 197: 230 of cap free
Amount of items: 2
Items: 
Size: 9766 Color: 0
Size: 5748 Color: 1

Bin 198: 232 of cap free
Amount of items: 2
Items: 
Size: 10476 Color: 1
Size: 5036 Color: 0

Bin 199: 8944 of cap free
Amount of items: 21
Items: 
Size: 380 Color: 0
Size: 368 Color: 0
Size: 352 Color: 0
Size: 352 Color: 0
Size: 336 Color: 1
Size: 336 Color: 0
Size: 332 Color: 0
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0
Size: 320 Color: 0
Size: 312 Color: 0
Size: 312 Color: 0
Size: 312 Color: 0
Size: 304 Color: 1
Size: 272 Color: 1
Size: 272 Color: 1

Total size: 3117312
Total free space: 15744

