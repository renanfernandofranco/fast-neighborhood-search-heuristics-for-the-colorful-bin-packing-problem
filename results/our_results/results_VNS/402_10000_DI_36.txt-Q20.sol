Capicity Bin: 8256
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 0
Size: 2916 Color: 19
Size: 200 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5288 Color: 7
Size: 2784 Color: 0
Size: 184 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5696 Color: 17
Size: 2364 Color: 14
Size: 196 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5784 Color: 7
Size: 1928 Color: 12
Size: 544 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5923 Color: 4
Size: 1975 Color: 16
Size: 358 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5987 Color: 2
Size: 1947 Color: 17
Size: 322 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5995 Color: 5
Size: 2013 Color: 3
Size: 248 Color: 18

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6026 Color: 17
Size: 2020 Color: 19
Size: 210 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6200 Color: 1
Size: 1892 Color: 4
Size: 164 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6226 Color: 11
Size: 1358 Color: 7
Size: 672 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6253 Color: 11
Size: 1887 Color: 14
Size: 116 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 17
Size: 1788 Color: 7
Size: 152 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 12
Size: 1778 Color: 5
Size: 204 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6285 Color: 12
Size: 1643 Color: 10
Size: 328 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6435 Color: 11
Size: 1055 Color: 10
Size: 766 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 11
Size: 1464 Color: 17
Size: 204 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6655 Color: 16
Size: 1225 Color: 8
Size: 376 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 9
Size: 1032 Color: 17
Size: 480 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 6
Size: 732 Color: 3
Size: 584 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 6
Size: 990 Color: 10
Size: 320 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 16
Size: 684 Color: 3
Size: 568 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7032 Color: 19
Size: 712 Color: 4
Size: 512 Color: 7

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7094 Color: 0
Size: 1022 Color: 9
Size: 140 Color: 11

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 16
Size: 586 Color: 13
Size: 488 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 15
Size: 904 Color: 15
Size: 92 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7298 Color: 11
Size: 686 Color: 3
Size: 272 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 12
Size: 828 Color: 10
Size: 128 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7370 Color: 15
Size: 502 Color: 19
Size: 384 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7372 Color: 12
Size: 740 Color: 2
Size: 144 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7380 Color: 3
Size: 684 Color: 2
Size: 192 Color: 14

Bin 31: 1 of cap free
Amount of items: 6
Items: 
Size: 4130 Color: 8
Size: 1174 Color: 3
Size: 1145 Color: 10
Size: 1094 Color: 13
Size: 504 Color: 14
Size: 208 Color: 19

Bin 32: 1 of cap free
Amount of items: 5
Items: 
Size: 4140 Color: 13
Size: 2507 Color: 14
Size: 1256 Color: 2
Size: 208 Color: 15
Size: 144 Color: 11

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 4146 Color: 0
Size: 3775 Color: 6
Size: 334 Color: 7

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 4333 Color: 14
Size: 3422 Color: 10
Size: 500 Color: 4

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 4667 Color: 15
Size: 3328 Color: 17
Size: 260 Color: 6

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5601 Color: 12
Size: 2462 Color: 8
Size: 192 Color: 17

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5629 Color: 10
Size: 2450 Color: 17
Size: 176 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 9
Size: 2213 Color: 9
Size: 136 Color: 12

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5917 Color: 17
Size: 2182 Color: 18
Size: 156 Color: 6

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6249 Color: 13
Size: 1654 Color: 3
Size: 352 Color: 8

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 10
Size: 1481 Color: 11
Size: 176 Color: 16

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6787 Color: 13
Size: 1284 Color: 11
Size: 184 Color: 14

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6837 Color: 18
Size: 970 Color: 2
Size: 448 Color: 1

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6991 Color: 8
Size: 792 Color: 13
Size: 472 Color: 11

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 7104 Color: 19
Size: 1079 Color: 10
Size: 72 Color: 14

Bin 46: 2 of cap free
Amount of items: 5
Items: 
Size: 4136 Color: 19
Size: 2372 Color: 1
Size: 1202 Color: 6
Size: 328 Color: 1
Size: 216 Color: 5

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 16
Size: 3804 Color: 17
Size: 296 Color: 9

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 4682 Color: 1
Size: 3448 Color: 17
Size: 124 Color: 14

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 4820 Color: 17
Size: 3434 Color: 10

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5544 Color: 6
Size: 2478 Color: 14
Size: 232 Color: 12

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 7
Size: 2138 Color: 11

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6463 Color: 18
Size: 1359 Color: 7
Size: 432 Color: 11

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 14
Size: 1183 Color: 11
Size: 400 Color: 6

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6694 Color: 0
Size: 1122 Color: 10
Size: 438 Color: 3

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 7
Size: 1302 Color: 1

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6982 Color: 1
Size: 1272 Color: 14

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 18
Size: 964 Color: 17
Size: 276 Color: 14

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 7176 Color: 13
Size: 834 Color: 13
Size: 244 Color: 14

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7210 Color: 13
Size: 1044 Color: 12

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 7430 Color: 16
Size: 808 Color: 15
Size: 16 Color: 3

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 6095 Color: 5
Size: 1862 Color: 2
Size: 296 Color: 6

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6963 Color: 9
Size: 1290 Color: 1

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 4666 Color: 12
Size: 3442 Color: 2
Size: 144 Color: 8

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 5922 Color: 2
Size: 2170 Color: 13
Size: 160 Color: 15

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 5988 Color: 14
Size: 2264 Color: 18

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 6479 Color: 16
Size: 1673 Color: 10
Size: 100 Color: 8

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 0
Size: 1528 Color: 19

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 6760 Color: 13
Size: 1176 Color: 15
Size: 316 Color: 17

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 6856 Color: 10
Size: 1268 Color: 14
Size: 128 Color: 0

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 7304 Color: 18
Size: 948 Color: 14

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 7416 Color: 6
Size: 836 Color: 5

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 5
Size: 2931 Color: 12
Size: 352 Color: 1

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6930 Color: 2
Size: 1321 Color: 6

Bin 74: 6 of cap free
Amount of items: 4
Items: 
Size: 4138 Color: 3
Size: 1951 Color: 18
Size: 1801 Color: 17
Size: 360 Color: 6

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 4584 Color: 19
Size: 3426 Color: 16
Size: 240 Color: 4

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 5218 Color: 10
Size: 2744 Color: 7
Size: 288 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 6630 Color: 2
Size: 1620 Color: 14

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 12
Size: 1382 Color: 19
Size: 64 Color: 13

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 6850 Color: 12
Size: 1096 Color: 17
Size: 304 Color: 6

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 15
Size: 1367 Color: 7

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 7338 Color: 18
Size: 912 Color: 15

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 5617 Color: 8
Size: 2488 Color: 10
Size: 144 Color: 2

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 6914 Color: 10
Size: 1335 Color: 3

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 6344 Color: 10
Size: 1904 Color: 18

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 7320 Color: 17
Size: 928 Color: 2

Bin 86: 9 of cap free
Amount of items: 2
Items: 
Size: 5183 Color: 16
Size: 3064 Color: 11

Bin 87: 9 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 5
Size: 2191 Color: 8
Size: 220 Color: 14

Bin 88: 12 of cap free
Amount of items: 2
Items: 
Size: 7172 Color: 8
Size: 1072 Color: 3

Bin 89: 12 of cap free
Amount of items: 2
Items: 
Size: 7242 Color: 5
Size: 1002 Color: 13

Bin 90: 13 of cap free
Amount of items: 2
Items: 
Size: 5249 Color: 11
Size: 2994 Color: 13

Bin 91: 14 of cap free
Amount of items: 2
Items: 
Size: 5654 Color: 9
Size: 2588 Color: 5

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 8
Size: 1212 Color: 0

Bin 93: 15 of cap free
Amount of items: 3
Items: 
Size: 6470 Color: 1
Size: 1555 Color: 7
Size: 216 Color: 17

Bin 94: 16 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 10
Size: 1100 Color: 11
Size: 32 Color: 17

Bin 95: 18 of cap free
Amount of items: 2
Items: 
Size: 6374 Color: 14
Size: 1864 Color: 5

Bin 96: 18 of cap free
Amount of items: 2
Items: 
Size: 6424 Color: 15
Size: 1814 Color: 12

Bin 97: 19 of cap free
Amount of items: 9
Items: 
Size: 4129 Color: 6
Size: 680 Color: 2
Size: 624 Color: 5
Size: 608 Color: 3
Size: 596 Color: 18
Size: 512 Color: 6
Size: 492 Color: 13
Size: 348 Color: 16
Size: 248 Color: 14

Bin 98: 21 of cap free
Amount of items: 2
Items: 
Size: 6627 Color: 6
Size: 1608 Color: 13

Bin 99: 21 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 13
Size: 1111 Color: 6

Bin 100: 22 of cap free
Amount of items: 2
Items: 
Size: 7258 Color: 16
Size: 976 Color: 19

Bin 101: 26 of cap free
Amount of items: 3
Items: 
Size: 4747 Color: 1
Size: 3271 Color: 11
Size: 212 Color: 4

Bin 102: 26 of cap free
Amount of items: 3
Items: 
Size: 5184 Color: 2
Size: 2870 Color: 8
Size: 176 Color: 6

Bin 103: 26 of cap free
Amount of items: 3
Items: 
Size: 6358 Color: 14
Size: 1484 Color: 10
Size: 388 Color: 17

Bin 104: 26 of cap free
Amount of items: 2
Items: 
Size: 6504 Color: 9
Size: 1726 Color: 1

Bin 105: 26 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 0
Size: 1490 Color: 18

Bin 106: 27 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 13
Size: 1519 Color: 16

Bin 107: 28 of cap free
Amount of items: 6
Items: 
Size: 4132 Color: 5
Size: 1038 Color: 19
Size: 908 Color: 2
Size: 874 Color: 15
Size: 834 Color: 9
Size: 442 Color: 0

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 19
Size: 1062 Color: 8

Bin 109: 30 of cap free
Amount of items: 2
Items: 
Size: 7120 Color: 11
Size: 1106 Color: 16

Bin 110: 31 of cap free
Amount of items: 2
Items: 
Size: 5286 Color: 0
Size: 2939 Color: 2

Bin 111: 31 of cap free
Amount of items: 3
Items: 
Size: 5960 Color: 9
Size: 2201 Color: 10
Size: 64 Color: 19

Bin 112: 34 of cap free
Amount of items: 2
Items: 
Size: 5420 Color: 19
Size: 2802 Color: 10

Bin 113: 35 of cap free
Amount of items: 2
Items: 
Size: 6925 Color: 3
Size: 1296 Color: 0

Bin 114: 38 of cap free
Amount of items: 3
Items: 
Size: 4914 Color: 10
Size: 2868 Color: 14
Size: 436 Color: 6

Bin 115: 38 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 8
Size: 3190 Color: 5

Bin 116: 42 of cap free
Amount of items: 3
Items: 
Size: 4898 Color: 2
Size: 3140 Color: 12
Size: 176 Color: 13

Bin 117: 46 of cap free
Amount of items: 2
Items: 
Size: 6814 Color: 1
Size: 1396 Color: 14

Bin 118: 51 of cap free
Amount of items: 2
Items: 
Size: 5644 Color: 10
Size: 2561 Color: 17

Bin 119: 53 of cap free
Amount of items: 3
Items: 
Size: 5302 Color: 13
Size: 2513 Color: 15
Size: 388 Color: 2

Bin 120: 54 of cap free
Amount of items: 3
Items: 
Size: 5318 Color: 11
Size: 1962 Color: 1
Size: 922 Color: 7

Bin 121: 57 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 10
Size: 1582 Color: 18

Bin 122: 58 of cap free
Amount of items: 2
Items: 
Size: 6126 Color: 9
Size: 2072 Color: 6

Bin 123: 60 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 11
Size: 1712 Color: 4

Bin 124: 64 of cap free
Amount of items: 26
Items: 
Size: 556 Color: 1
Size: 492 Color: 10
Size: 472 Color: 11
Size: 436 Color: 4
Size: 400 Color: 18
Size: 400 Color: 6
Size: 368 Color: 19
Size: 368 Color: 4
Size: 312 Color: 8
Size: 304 Color: 16
Size: 304 Color: 16
Size: 302 Color: 4
Size: 272 Color: 2
Size: 270 Color: 10
Size: 268 Color: 9
Size: 266 Color: 17
Size: 264 Color: 15
Size: 256 Color: 19
Size: 256 Color: 15
Size: 240 Color: 14
Size: 240 Color: 14
Size: 240 Color: 13
Size: 240 Color: 8
Size: 236 Color: 5
Size: 216 Color: 11
Size: 214 Color: 3

Bin 125: 68 of cap free
Amount of items: 2
Items: 
Size: 4739 Color: 11
Size: 3449 Color: 13

Bin 126: 72 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 1
Size: 3724 Color: 16
Size: 288 Color: 11

Bin 127: 72 of cap free
Amount of items: 2
Items: 
Size: 5156 Color: 16
Size: 3028 Color: 7

Bin 128: 75 of cap free
Amount of items: 3
Items: 
Size: 4492 Color: 4
Size: 3441 Color: 9
Size: 248 Color: 13

Bin 129: 84 of cap free
Amount of items: 2
Items: 
Size: 5638 Color: 12
Size: 2534 Color: 14

Bin 130: 89 of cap free
Amount of items: 7
Items: 
Size: 4133 Color: 2
Size: 742 Color: 13
Size: 690 Color: 4
Size: 684 Color: 1
Size: 680 Color: 5
Size: 652 Color: 11
Size: 586 Color: 8

Bin 131: 90 of cap free
Amount of items: 2
Items: 
Size: 5241 Color: 12
Size: 2925 Color: 17

Bin 132: 121 of cap free
Amount of items: 2
Items: 
Size: 4731 Color: 2
Size: 3404 Color: 6

Bin 133: 6242 of cap free
Amount of items: 10
Items: 
Size: 238 Color: 0
Size: 228 Color: 1
Size: 224 Color: 9
Size: 220 Color: 13
Size: 220 Color: 7
Size: 200 Color: 14
Size: 192 Color: 16
Size: 176 Color: 8
Size: 172 Color: 11
Size: 144 Color: 3

Total size: 1089792
Total free space: 8256

