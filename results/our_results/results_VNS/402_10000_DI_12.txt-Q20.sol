Capicity Bin: 8016
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 8
Size: 3338 Color: 15
Size: 180 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4612 Color: 6
Size: 3260 Color: 6
Size: 144 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5075 Color: 15
Size: 2451 Color: 0
Size: 490 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5170 Color: 17
Size: 2374 Color: 12
Size: 472 Color: 18

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5215 Color: 19
Size: 2335 Color: 13
Size: 466 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5223 Color: 16
Size: 2329 Color: 13
Size: 464 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5700 Color: 16
Size: 1528 Color: 2
Size: 788 Color: 10

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5905 Color: 13
Size: 1527 Color: 4
Size: 584 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5961 Color: 14
Size: 1282 Color: 16
Size: 773 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6030 Color: 5
Size: 1658 Color: 13
Size: 328 Color: 4

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6342 Color: 5
Size: 1522 Color: 10
Size: 152 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6405 Color: 3
Size: 1335 Color: 16
Size: 276 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6478 Color: 5
Size: 1350 Color: 1
Size: 188 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 5
Size: 1356 Color: 3
Size: 136 Color: 6

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6587 Color: 9
Size: 981 Color: 11
Size: 448 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6591 Color: 18
Size: 849 Color: 6
Size: 576 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 16
Size: 1294 Color: 4
Size: 96 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 0
Size: 1164 Color: 15
Size: 200 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6658 Color: 5
Size: 1186 Color: 12
Size: 172 Color: 6

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6709 Color: 9
Size: 1091 Color: 0
Size: 216 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 8
Size: 1046 Color: 11
Size: 208 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 6
Size: 876 Color: 17
Size: 328 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6805 Color: 10
Size: 1057 Color: 1
Size: 154 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 6
Size: 724 Color: 1
Size: 384 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 19
Size: 750 Color: 14
Size: 368 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6962 Color: 6
Size: 914 Color: 14
Size: 140 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6935 Color: 2
Size: 731 Color: 18
Size: 350 Color: 7

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7018 Color: 17
Size: 766 Color: 3
Size: 232 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7020 Color: 0
Size: 836 Color: 18
Size: 160 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7081 Color: 6
Size: 781 Color: 13
Size: 154 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7056 Color: 12
Size: 648 Color: 16
Size: 312 Color: 8

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7057 Color: 8
Size: 901 Color: 8
Size: 58 Color: 3

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 2
Size: 798 Color: 6
Size: 156 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7098 Color: 13
Size: 710 Color: 4
Size: 208 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7113 Color: 6
Size: 753 Color: 15
Size: 150 Color: 11

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 2
Size: 664 Color: 11
Size: 196 Color: 18

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7166 Color: 11
Size: 508 Color: 15
Size: 342 Color: 8

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7188 Color: 1
Size: 516 Color: 6
Size: 312 Color: 4

Bin 39: 1 of cap free
Amount of items: 7
Items: 
Size: 4009 Color: 18
Size: 834 Color: 9
Size: 800 Color: 13
Size: 690 Color: 12
Size: 664 Color: 2
Size: 664 Color: 1
Size: 354 Color: 18

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4108 Color: 5
Size: 3331 Color: 10
Size: 576 Color: 9

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4502 Color: 0
Size: 3333 Color: 13
Size: 180 Color: 18

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 18
Size: 2172 Color: 2
Size: 1191 Color: 12

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5092 Color: 5
Size: 2685 Color: 6
Size: 238 Color: 14

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 5211 Color: 8
Size: 2804 Color: 15

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 8
Size: 2491 Color: 9

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5567 Color: 16
Size: 2356 Color: 19
Size: 92 Color: 3

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5937 Color: 11
Size: 1854 Color: 2
Size: 224 Color: 17

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5974 Color: 9
Size: 1761 Color: 14
Size: 280 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6254 Color: 17
Size: 1189 Color: 6
Size: 572 Color: 13

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6316 Color: 1
Size: 1491 Color: 17
Size: 208 Color: 13

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6411 Color: 12
Size: 1500 Color: 6
Size: 104 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6447 Color: 16
Size: 1216 Color: 11
Size: 352 Color: 3

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 6676 Color: 16
Size: 1339 Color: 7

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6803 Color: 10
Size: 1042 Color: 16
Size: 170 Color: 16

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6885 Color: 13
Size: 666 Color: 17
Size: 464 Color: 18

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 6919 Color: 15
Size: 1096 Color: 19

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 19
Size: 851 Color: 5
Size: 192 Color: 6

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 7206 Color: 2
Size: 809 Color: 3

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5398 Color: 9
Size: 2468 Color: 14
Size: 148 Color: 19

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6012 Color: 12
Size: 2002 Color: 8

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 15
Size: 1498 Color: 13
Size: 408 Color: 12

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 10
Size: 1658 Color: 14
Size: 218 Color: 1

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6408 Color: 18
Size: 1134 Color: 3
Size: 472 Color: 19

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 6594 Color: 8
Size: 1420 Color: 11

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6705 Color: 7
Size: 1309 Color: 12

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 13
Size: 919 Color: 16
Size: 256 Color: 4

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 7010 Color: 11
Size: 1004 Color: 1

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5855 Color: 12
Size: 1804 Color: 14
Size: 354 Color: 9

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5897 Color: 1
Size: 1932 Color: 5
Size: 184 Color: 12

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 6446 Color: 13
Size: 1567 Color: 1

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 7089 Color: 10
Size: 924 Color: 9

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6310 Color: 16
Size: 1702 Color: 4

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 6398 Color: 17
Size: 1566 Color: 10
Size: 48 Color: 0

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 4962 Color: 9
Size: 2825 Color: 9
Size: 224 Color: 3

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5965 Color: 4
Size: 1758 Color: 16
Size: 288 Color: 13

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6415 Color: 1
Size: 1596 Color: 12

Bin 77: 6 of cap free
Amount of items: 5
Items: 
Size: 4012 Color: 0
Size: 1853 Color: 14
Size: 1711 Color: 14
Size: 266 Color: 2
Size: 168 Color: 19

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 4906 Color: 3
Size: 2968 Color: 13
Size: 136 Color: 7

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 5196 Color: 1
Size: 2546 Color: 11
Size: 268 Color: 14

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 5892 Color: 0
Size: 2118 Color: 8

Bin 81: 6 of cap free
Amount of items: 3
Items: 
Size: 6235 Color: 11
Size: 1343 Color: 6
Size: 432 Color: 14

Bin 82: 6 of cap free
Amount of items: 2
Items: 
Size: 6556 Color: 15
Size: 1454 Color: 2

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 7076 Color: 17
Size: 934 Color: 18

Bin 84: 7 of cap free
Amount of items: 2
Items: 
Size: 5605 Color: 6
Size: 2404 Color: 17

Bin 85: 8 of cap free
Amount of items: 4
Items: 
Size: 4021 Color: 4
Size: 3337 Color: 0
Size: 466 Color: 18
Size: 184 Color: 5

Bin 86: 8 of cap free
Amount of items: 3
Items: 
Size: 5023 Color: 1
Size: 2873 Color: 6
Size: 112 Color: 14

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 6231 Color: 6
Size: 1777 Color: 7

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 6396 Color: 14
Size: 1124 Color: 16
Size: 488 Color: 6

Bin 89: 8 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 13
Size: 1356 Color: 19
Size: 32 Color: 15

Bin 90: 8 of cap free
Amount of items: 2
Items: 
Size: 6915 Color: 1
Size: 1093 Color: 13

Bin 91: 8 of cap free
Amount of items: 2
Items: 
Size: 7093 Color: 16
Size: 915 Color: 4

Bin 92: 9 of cap free
Amount of items: 2
Items: 
Size: 5123 Color: 15
Size: 2884 Color: 5

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 5140 Color: 19
Size: 2867 Color: 12

Bin 94: 10 of cap free
Amount of items: 2
Items: 
Size: 5412 Color: 6
Size: 2594 Color: 7

Bin 95: 10 of cap free
Amount of items: 2
Items: 
Size: 6995 Color: 9
Size: 1011 Color: 18

Bin 96: 10 of cap free
Amount of items: 2
Items: 
Size: 7141 Color: 13
Size: 865 Color: 5

Bin 97: 11 of cap free
Amount of items: 2
Items: 
Size: 5029 Color: 14
Size: 2976 Color: 1

Bin 98: 11 of cap free
Amount of items: 2
Items: 
Size: 5071 Color: 19
Size: 2934 Color: 1

Bin 99: 12 of cap free
Amount of items: 4
Items: 
Size: 4013 Color: 6
Size: 2419 Color: 17
Size: 1212 Color: 13
Size: 360 Color: 18

Bin 100: 13 of cap free
Amount of items: 3
Items: 
Size: 4577 Color: 8
Size: 2930 Color: 6
Size: 496 Color: 7

Bin 101: 14 of cap free
Amount of items: 15
Items: 
Size: 678 Color: 5
Size: 666 Color: 18
Size: 664 Color: 1
Size: 640 Color: 0
Size: 574 Color: 11
Size: 574 Color: 5
Size: 560 Color: 17
Size: 560 Color: 10
Size: 496 Color: 11
Size: 490 Color: 4
Size: 482 Color: 8
Size: 482 Color: 4
Size: 420 Color: 8
Size: 376 Color: 6
Size: 340 Color: 2

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 8
Size: 1773 Color: 11

Bin 103: 16 of cap free
Amount of items: 3
Items: 
Size: 6546 Color: 5
Size: 1398 Color: 16
Size: 56 Color: 8

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 7118 Color: 12
Size: 882 Color: 5

Bin 105: 18 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 17
Size: 1226 Color: 1

Bin 106: 22 of cap free
Amount of items: 2
Items: 
Size: 5910 Color: 6
Size: 2084 Color: 1

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 6854 Color: 12
Size: 1140 Color: 1

Bin 108: 24 of cap free
Amount of items: 3
Items: 
Size: 5885 Color: 11
Size: 2011 Color: 7
Size: 96 Color: 10

Bin 109: 25 of cap free
Amount of items: 2
Items: 
Size: 6999 Color: 18
Size: 992 Color: 11

Bin 110: 25 of cap free
Amount of items: 2
Items: 
Size: 7190 Color: 9
Size: 801 Color: 5

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 7047 Color: 12
Size: 943 Color: 10

Bin 112: 29 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 9
Size: 1713 Color: 11

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 6185 Color: 11
Size: 1801 Color: 12

Bin 114: 30 of cap free
Amount of items: 2
Items: 
Size: 6766 Color: 11
Size: 1220 Color: 18

Bin 115: 31 of cap free
Amount of items: 2
Items: 
Size: 6220 Color: 6
Size: 1765 Color: 7

Bin 116: 32 of cap free
Amount of items: 2
Items: 
Size: 4472 Color: 0
Size: 3512 Color: 14

Bin 117: 33 of cap free
Amount of items: 3
Items: 
Size: 4978 Color: 17
Size: 2339 Color: 6
Size: 666 Color: 18

Bin 118: 34 of cap free
Amount of items: 2
Items: 
Size: 4014 Color: 19
Size: 3968 Color: 11

Bin 119: 34 of cap free
Amount of items: 3
Items: 
Size: 5614 Color: 17
Size: 1676 Color: 12
Size: 692 Color: 19

Bin 120: 37 of cap free
Amount of items: 4
Items: 
Size: 4010 Color: 19
Size: 2415 Color: 12
Size: 970 Color: 18
Size: 584 Color: 17

Bin 121: 39 of cap free
Amount of items: 2
Items: 
Size: 5119 Color: 14
Size: 2858 Color: 2

Bin 122: 40 of cap free
Amount of items: 2
Items: 
Size: 5794 Color: 4
Size: 2182 Color: 7

Bin 123: 44 of cap free
Amount of items: 3
Items: 
Size: 5852 Color: 16
Size: 1772 Color: 14
Size: 348 Color: 19

Bin 124: 57 of cap free
Amount of items: 2
Items: 
Size: 5115 Color: 14
Size: 2844 Color: 12

Bin 125: 59 of cap free
Amount of items: 3
Items: 
Size: 4017 Color: 3
Size: 2455 Color: 10
Size: 1485 Color: 16

Bin 126: 62 of cap free
Amount of items: 28
Items: 
Size: 456 Color: 15
Size: 436 Color: 9
Size: 408 Color: 11
Size: 400 Color: 1
Size: 400 Color: 0
Size: 360 Color: 12
Size: 340 Color: 10
Size: 314 Color: 14
Size: 304 Color: 1
Size: 298 Color: 2
Size: 296 Color: 8
Size: 296 Color: 7
Size: 268 Color: 18
Size: 266 Color: 17
Size: 266 Color: 14
Size: 264 Color: 1
Size: 260 Color: 13
Size: 244 Color: 2
Size: 240 Color: 6
Size: 236 Color: 15
Size: 236 Color: 8
Size: 218 Color: 9
Size: 216 Color: 12
Size: 208 Color: 4
Size: 200 Color: 10
Size: 182 Color: 17
Size: 182 Color: 6
Size: 160 Color: 12

Bin 127: 71 of cap free
Amount of items: 2
Items: 
Size: 5068 Color: 19
Size: 2877 Color: 14

Bin 128: 78 of cap free
Amount of items: 2
Items: 
Size: 5478 Color: 19
Size: 2460 Color: 18

Bin 129: 86 of cap free
Amount of items: 2
Items: 
Size: 5889 Color: 8
Size: 2041 Color: 5

Bin 130: 105 of cap free
Amount of items: 2
Items: 
Size: 4569 Color: 3
Size: 3342 Color: 9

Bin 131: 111 of cap free
Amount of items: 2
Items: 
Size: 4565 Color: 10
Size: 3340 Color: 2

Bin 132: 119 of cap free
Amount of items: 2
Items: 
Size: 4556 Color: 8
Size: 3341 Color: 0

Bin 133: 6358 of cap free
Amount of items: 11
Items: 
Size: 168 Color: 11
Size: 168 Color: 3
Size: 164 Color: 0
Size: 160 Color: 2
Size: 158 Color: 15
Size: 156 Color: 7
Size: 152 Color: 5
Size: 136 Color: 19
Size: 136 Color: 6
Size: 132 Color: 12
Size: 128 Color: 13

Total size: 1058112
Total free space: 8016

