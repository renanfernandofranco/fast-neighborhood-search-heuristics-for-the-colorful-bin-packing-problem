Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 14352 Color: 481
Size: 3600 Color: 286
Size: 1696 Color: 205

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 482
Size: 3098 Color: 267
Size: 2176 Color: 230

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 484
Size: 4280 Color: 312
Size: 978 Color: 143

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 489
Size: 4368 Color: 318
Size: 800 Color: 116

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 14692 Color: 499
Size: 4728 Color: 327
Size: 228 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 15006 Color: 504
Size: 3864 Color: 293
Size: 778 Color: 115

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 15008 Color: 505
Size: 4320 Color: 315
Size: 320 Color: 24

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 15009 Color: 506
Size: 3867 Color: 294
Size: 772 Color: 112

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 15016 Color: 507
Size: 3608 Color: 287
Size: 1024 Color: 147

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 15022 Color: 508
Size: 4382 Color: 319
Size: 244 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 15064 Color: 511
Size: 3832 Color: 290
Size: 752 Color: 104

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 15128 Color: 512
Size: 2264 Color: 237
Size: 2256 Color: 236

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 15436 Color: 521
Size: 3364 Color: 272
Size: 848 Color: 124

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 15532 Color: 527
Size: 3870 Color: 295
Size: 246 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 15840 Color: 533
Size: 2942 Color: 262
Size: 866 Color: 130

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 15939 Color: 536
Size: 2885 Color: 259
Size: 824 Color: 119

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 15944 Color: 537
Size: 2852 Color: 258
Size: 852 Color: 125

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 15992 Color: 538
Size: 3560 Color: 285
Size: 96 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 16112 Color: 540
Size: 3096 Color: 266
Size: 440 Color: 50

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 16244 Color: 546
Size: 2208 Color: 232
Size: 1196 Color: 162

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 16304 Color: 547
Size: 1712 Color: 207
Size: 1632 Color: 197

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 16456 Color: 550
Size: 2616 Color: 249
Size: 576 Color: 78

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 16474 Color: 552
Size: 2350 Color: 241
Size: 824 Color: 118

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 16480 Color: 553
Size: 2096 Color: 224
Size: 1072 Color: 155

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 16520 Color: 554
Size: 1704 Color: 206
Size: 1424 Color: 185

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 16627 Color: 557
Size: 1997 Color: 222
Size: 1024 Color: 148

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 16828 Color: 560
Size: 2364 Color: 243
Size: 456 Color: 57

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 16830 Color: 561
Size: 2210 Color: 233
Size: 608 Color: 82

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 16888 Color: 563
Size: 2312 Color: 240
Size: 448 Color: 55

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 16936 Color: 565
Size: 1960 Color: 218
Size: 752 Color: 105

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16966 Color: 566
Size: 1830 Color: 213
Size: 852 Color: 126

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 17040 Color: 571
Size: 1408 Color: 182
Size: 1200 Color: 164

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 17056 Color: 573
Size: 2576 Color: 247
Size: 16 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 17093 Color: 574
Size: 2131 Color: 228
Size: 424 Color: 48

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 17104 Color: 575
Size: 1768 Color: 210
Size: 776 Color: 114

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 17115 Color: 576
Size: 2111 Color: 226
Size: 422 Color: 47

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 17168 Color: 578
Size: 1760 Color: 209
Size: 720 Color: 103

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 17300 Color: 582
Size: 1964 Color: 219
Size: 384 Color: 36

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 17304 Color: 583
Size: 2008 Color: 223
Size: 336 Color: 27

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 17308 Color: 584
Size: 1956 Color: 217
Size: 384 Color: 37

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 17442 Color: 586
Size: 1654 Color: 202
Size: 552 Color: 75

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 17451 Color: 587
Size: 1831 Color: 214
Size: 366 Color: 31

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 17504 Color: 590
Size: 1568 Color: 191
Size: 576 Color: 80

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 17568 Color: 593
Size: 1632 Color: 198
Size: 448 Color: 56

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17584 Color: 594
Size: 1216 Color: 169
Size: 848 Color: 123

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 595
Size: 1592 Color: 192
Size: 448 Color: 53

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17616 Color: 596
Size: 1392 Color: 180
Size: 640 Color: 89

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 597
Size: 1408 Color: 183
Size: 608 Color: 84

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17634 Color: 598
Size: 1512 Color: 188
Size: 502 Color: 63

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17672 Color: 600
Size: 1408 Color: 184
Size: 568 Color: 77

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 13347 Color: 461
Size: 6300 Color: 367

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 14332 Color: 480
Size: 4339 Color: 317
Size: 976 Color: 142

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 483
Size: 4830 Color: 330
Size: 438 Color: 49

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 14459 Color: 488
Size: 3552 Color: 284
Size: 1636 Color: 201

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 494
Size: 5011 Color: 335
Size: 102 Color: 7

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 14550 Color: 495
Size: 3841 Color: 291
Size: 1256 Color: 175

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 15023 Color: 509
Size: 4624 Color: 325

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 15039 Color: 510
Size: 4608 Color: 324

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 15523 Color: 526
Size: 3548 Color: 283
Size: 576 Color: 79

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 15539 Color: 528
Size: 4108 Color: 302

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 15923 Color: 535
Size: 3436 Color: 276
Size: 288 Color: 17

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 16196 Color: 544
Size: 3451 Color: 278

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 16463 Color: 551
Size: 3184 Color: 268

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 16812 Color: 559
Size: 1971 Color: 220
Size: 864 Color: 129

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 17009 Color: 570
Size: 1600 Color: 194
Size: 1038 Color: 149

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 13112 Color: 453
Size: 4296 Color: 313
Size: 2238 Color: 235

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 465
Size: 6248 Color: 365

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 13504 Color: 468
Size: 5810 Color: 354
Size: 332 Color: 26

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 491
Size: 5146 Color: 339

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 14518 Color: 492
Size: 4048 Color: 300
Size: 1080 Color: 157

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 15384 Color: 517
Size: 4262 Color: 310

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 15396 Color: 518
Size: 4250 Color: 309

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 15402 Color: 519
Size: 4244 Color: 308

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 15414 Color: 520
Size: 4232 Color: 307

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 15742 Color: 532
Size: 3872 Color: 296
Size: 32 Color: 1

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 16982 Color: 568
Size: 2664 Color: 254

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 17253 Color: 580
Size: 2393 Color: 244

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 17454 Color: 588
Size: 2192 Color: 231

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 17518 Color: 591
Size: 2128 Color: 227

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 17544 Color: 592
Size: 2102 Color: 225

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 14406 Color: 485
Size: 5239 Color: 342

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 14564 Color: 496
Size: 4391 Color: 320
Size: 690 Color: 95

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 15320 Color: 515
Size: 4325 Color: 316

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 17126 Color: 577
Size: 2519 Color: 246

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 460
Size: 6076 Color: 360
Size: 336 Color: 28

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 14800 Color: 501
Size: 4844 Color: 332

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 15612 Color: 530
Size: 4032 Color: 299

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 15696 Color: 531
Size: 3948 Color: 297

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 16160 Color: 542
Size: 3484 Color: 280

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 16800 Color: 558
Size: 2844 Color: 257

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 16998 Color: 569
Size: 2646 Color: 251

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 17666 Color: 599
Size: 1978 Color: 221

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 16907 Color: 564
Size: 2736 Color: 255

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 17467 Color: 589
Size: 2176 Color: 229

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 425
Size: 7984 Color: 392
Size: 530 Color: 70

Bin 96: 6 of cap free
Amount of items: 4
Items: 
Size: 13856 Color: 475
Size: 5428 Color: 349
Size: 198 Color: 12
Size: 160 Color: 11

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 14443 Color: 487
Size: 5199 Color: 340

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 15344 Color: 516
Size: 4298 Color: 314

Bin 99: 7 of cap free
Amount of items: 2
Items: 
Size: 17046 Color: 572
Size: 2595 Color: 248

Bin 100: 7 of cap free
Amount of items: 2
Items: 
Size: 17285 Color: 581
Size: 2356 Color: 242

Bin 101: 8 of cap free
Amount of items: 7
Items: 
Size: 9834 Color: 409
Size: 2619 Color: 250
Size: 2286 Color: 239
Size: 2285 Color: 238
Size: 1216 Color: 168
Size: 704 Color: 98
Size: 696 Color: 97

Bin 102: 8 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 440
Size: 6974 Color: 377
Size: 448 Color: 54

Bin 103: 8 of cap free
Amount of items: 3
Items: 
Size: 13140 Color: 454
Size: 4818 Color: 329
Size: 1682 Color: 204

Bin 104: 8 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 476
Size: 5610 Color: 353
Size: 160 Color: 10

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 502
Size: 4776 Color: 328

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 17240 Color: 579
Size: 2400 Color: 245

Bin 107: 9 of cap free
Amount of items: 2
Items: 
Size: 15507 Color: 525
Size: 4132 Color: 304

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 16591 Color: 556
Size: 3048 Color: 263

Bin 109: 10 of cap free
Amount of items: 3
Items: 
Size: 12678 Color: 452
Size: 6576 Color: 370
Size: 384 Color: 35

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 13382 Color: 463
Size: 6256 Color: 366

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 14416 Color: 486
Size: 5222 Color: 341

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 15456 Color: 522
Size: 4182 Color: 306

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 16122 Color: 541
Size: 3516 Color: 281

Bin 114: 11 of cap free
Amount of items: 2
Items: 
Size: 16855 Color: 562
Size: 2782 Color: 256

Bin 115: 12 of cap free
Amount of items: 2
Items: 
Size: 11010 Color: 421
Size: 8626 Color: 401

Bin 116: 12 of cap free
Amount of items: 2
Items: 
Size: 14520 Color: 493
Size: 5116 Color: 337

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 16228 Color: 545
Size: 3408 Color: 273

Bin 118: 13 of cap free
Amount of items: 2
Items: 
Size: 13411 Color: 466
Size: 6224 Color: 364

Bin 119: 13 of cap free
Amount of items: 4
Items: 
Size: 13635 Color: 470
Size: 5360 Color: 344
Size: 320 Color: 22
Size: 320 Color: 21

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 17413 Color: 585
Size: 2222 Color: 234

Bin 121: 14 of cap free
Amount of items: 2
Items: 
Size: 15236 Color: 514
Size: 4398 Color: 321

Bin 122: 15 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 424
Size: 7977 Color: 391
Size: 544 Color: 71

Bin 123: 16 of cap free
Amount of items: 4
Items: 
Size: 13928 Color: 477
Size: 5448 Color: 350
Size: 128 Color: 9
Size: 128 Color: 8

Bin 124: 16 of cap free
Amount of items: 2
Items: 
Size: 14496 Color: 490
Size: 5136 Color: 338

Bin 125: 16 of cap free
Amount of items: 2
Items: 
Size: 15468 Color: 523
Size: 4164 Color: 305

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 15564 Color: 529
Size: 4068 Color: 301

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 16560 Color: 555
Size: 3072 Color: 264

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 16976 Color: 567
Size: 2656 Color: 253

Bin 129: 17 of cap free
Amount of items: 3
Items: 
Size: 13363 Color: 462
Size: 4832 Color: 331
Size: 1436 Color: 187

Bin 130: 18 of cap free
Amount of items: 2
Items: 
Size: 14942 Color: 503
Size: 4688 Color: 326

Bin 131: 18 of cap free
Amount of items: 2
Items: 
Size: 15500 Color: 524
Size: 4130 Color: 303

Bin 132: 19 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 441
Size: 3855 Color: 292
Size: 3542 Color: 282

Bin 133: 19 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 498
Size: 4969 Color: 334

Bin 134: 20 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 434
Size: 7132 Color: 382
Size: 468 Color: 60

Bin 135: 20 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 436
Size: 7072 Color: 379
Size: 464 Color: 59

Bin 136: 20 of cap free
Amount of items: 3
Items: 
Size: 12144 Color: 437
Size: 4016 Color: 298
Size: 3468 Color: 279

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 15192 Color: 513
Size: 4436 Color: 323

Bin 138: 22 of cap free
Amount of items: 2
Items: 
Size: 16187 Color: 543
Size: 3439 Color: 277

Bin 139: 22 of cap free
Amount of items: 2
Items: 
Size: 16314 Color: 548
Size: 3312 Color: 271

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 16368 Color: 549
Size: 3258 Color: 270

Bin 141: 24 of cap free
Amount of items: 3
Items: 
Size: 12364 Color: 445
Size: 6844 Color: 373
Size: 416 Color: 45

Bin 142: 24 of cap free
Amount of items: 2
Items: 
Size: 13488 Color: 467
Size: 6136 Color: 361

Bin 143: 24 of cap free
Amount of items: 2
Items: 
Size: 15856 Color: 534
Size: 3768 Color: 289

Bin 144: 25 of cap free
Amount of items: 4
Items: 
Size: 14032 Color: 479
Size: 5475 Color: 352
Size: 64 Color: 3
Size: 52 Color: 2

Bin 145: 26 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 459
Size: 6046 Color: 359
Size: 352 Color: 29

Bin 146: 27 of cap free
Amount of items: 2
Items: 
Size: 15998 Color: 539
Size: 3623 Color: 288

Bin 147: 30 of cap free
Amount of items: 3
Items: 
Size: 12168 Color: 438
Size: 6986 Color: 378
Size: 464 Color: 58

Bin 148: 31 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 443
Size: 6885 Color: 375
Size: 444 Color: 51

Bin 149: 32 of cap free
Amount of items: 3
Items: 
Size: 11168 Color: 426
Size: 7920 Color: 389
Size: 528 Color: 69

Bin 150: 32 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 433
Size: 7128 Color: 381
Size: 472 Color: 61

Bin 151: 33 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 444
Size: 6899 Color: 376
Size: 420 Color: 46

Bin 152: 33 of cap free
Amount of items: 2
Items: 
Size: 14584 Color: 497
Size: 5031 Color: 336

Bin 153: 40 of cap free
Amount of items: 4
Items: 
Size: 13992 Color: 478
Size: 5472 Color: 351
Size: 80 Color: 5
Size: 64 Color: 4

Bin 154: 40 of cap free
Amount of items: 2
Items: 
Size: 14724 Color: 500
Size: 4884 Color: 333

Bin 155: 49 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 423
Size: 7963 Color: 390
Size: 544 Color: 72

Bin 156: 52 of cap free
Amount of items: 2
Items: 
Size: 12394 Color: 446
Size: 7202 Color: 384

Bin 157: 53 of cap free
Amount of items: 3
Items: 
Size: 11387 Color: 430
Size: 7696 Color: 387
Size: 512 Color: 65

Bin 158: 55 of cap free
Amount of items: 4
Items: 
Size: 12449 Color: 448
Size: 6320 Color: 368
Size: 416 Color: 42
Size: 408 Color: 41

Bin 159: 58 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 451
Size: 6720 Color: 372
Size: 392 Color: 38

Bin 160: 60 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 458
Size: 6020 Color: 358
Size: 364 Color: 30

Bin 161: 66 of cap free
Amount of items: 3
Items: 
Size: 12256 Color: 442
Size: 6882 Color: 374
Size: 444 Color: 52

Bin 162: 66 of cap free
Amount of items: 4
Items: 
Size: 13611 Color: 469
Size: 5323 Color: 343
Size: 328 Color: 25
Size: 320 Color: 23

Bin 163: 69 of cap free
Amount of items: 2
Items: 
Size: 13395 Color: 464
Size: 6184 Color: 363

Bin 164: 71 of cap free
Amount of items: 3
Items: 
Size: 12208 Color: 439
Size: 4278 Color: 311
Size: 3091 Color: 265

Bin 165: 72 of cap free
Amount of items: 3
Items: 
Size: 13844 Color: 473
Size: 5416 Color: 347
Size: 316 Color: 18

Bin 166: 83 of cap free
Amount of items: 4
Items: 
Size: 10077 Color: 415
Size: 8168 Color: 394
Size: 672 Color: 91
Size: 648 Color: 90

Bin 167: 99 of cap free
Amount of items: 4
Items: 
Size: 10093 Color: 416
Size: 8176 Color: 395
Size: 640 Color: 88
Size: 640 Color: 87

Bin 168: 107 of cap free
Amount of items: 3
Items: 
Size: 13168 Color: 457
Size: 6001 Color: 357
Size: 372 Color: 32

Bin 169: 114 of cap free
Amount of items: 4
Items: 
Size: 10160 Color: 418
Size: 8182 Color: 397
Size: 608 Color: 83
Size: 584 Color: 81

Bin 170: 114 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 456
Size: 5990 Color: 356
Size: 384 Color: 33

Bin 171: 114 of cap free
Amount of items: 3
Items: 
Size: 13854 Color: 474
Size: 5424 Color: 348
Size: 256 Color: 16

Bin 172: 122 of cap free
Amount of items: 3
Items: 
Size: 11760 Color: 432
Size: 7270 Color: 385
Size: 496 Color: 62

Bin 173: 125 of cap free
Amount of items: 3
Items: 
Size: 13152 Color: 455
Size: 5987 Color: 355
Size: 384 Color: 34

Bin 174: 138 of cap free
Amount of items: 4
Items: 
Size: 10096 Color: 417
Size: 8180 Color: 396
Size: 618 Color: 86
Size: 616 Color: 85

Bin 175: 145 of cap free
Amount of items: 3
Items: 
Size: 12465 Color: 450
Size: 6640 Color: 371
Size: 398 Color: 39

Bin 176: 168 of cap free
Amount of items: 3
Items: 
Size: 13788 Color: 472
Size: 5372 Color: 346
Size: 320 Color: 19

Bin 177: 194 of cap free
Amount of items: 2
Items: 
Size: 11266 Color: 427
Size: 8188 Color: 400

Bin 178: 195 of cap free
Amount of items: 3
Items: 
Size: 11371 Color: 429
Size: 7570 Color: 386
Size: 512 Color: 66

Bin 179: 196 of cap free
Amount of items: 3
Items: 
Size: 11028 Color: 422
Size: 7872 Color: 388
Size: 552 Color: 73

Bin 180: 208 of cap free
Amount of items: 3
Items: 
Size: 12080 Color: 435
Size: 4432 Color: 322
Size: 2928 Color: 261

Bin 181: 212 of cap free
Amount of items: 4
Items: 
Size: 12428 Color: 447
Size: 6176 Color: 362
Size: 416 Color: 44
Size: 416 Color: 43

Bin 182: 214 of cap free
Amount of items: 4
Items: 
Size: 11282 Color: 428
Size: 7112 Color: 380
Size: 528 Color: 68
Size: 512 Color: 67

Bin 183: 220 of cap free
Amount of items: 4
Items: 
Size: 9904 Color: 414
Size: 8160 Color: 393
Size: 684 Color: 93
Size: 680 Color: 92

Bin 184: 247 of cap free
Amount of items: 7
Items: 
Size: 9832 Color: 408
Size: 1863 Color: 215
Size: 1824 Color: 212
Size: 1778 Color: 211
Size: 1744 Color: 208
Size: 1656 Color: 203
Size: 704 Color: 99

Bin 185: 252 of cap free
Amount of items: 10
Items: 
Size: 9826 Color: 405
Size: 1376 Color: 178
Size: 1312 Color: 177
Size: 1312 Color: 176
Size: 1248 Color: 174
Size: 1248 Color: 173
Size: 770 Color: 111
Size: 768 Color: 110
Size: 768 Color: 109
Size: 768 Color: 108

Bin 186: 252 of cap free
Amount of items: 3
Items: 
Size: 11696 Color: 431
Size: 7188 Color: 383
Size: 512 Color: 64

Bin 187: 268 of cap free
Amount of items: 19
Items: 
Size: 1208 Color: 166
Size: 1208 Color: 165
Size: 1198 Color: 163
Size: 1196 Color: 161
Size: 1196 Color: 160
Size: 1160 Color: 159
Size: 1088 Color: 158
Size: 1072 Color: 156
Size: 1056 Color: 154
Size: 960 Color: 138
Size: 944 Color: 137
Size: 928 Color: 136
Size: 928 Color: 135
Size: 880 Color: 134
Size: 878 Color: 133
Size: 876 Color: 132
Size: 876 Color: 131
Size: 864 Color: 128
Size: 864 Color: 127

Bin 188: 271 of cap free
Amount of items: 9
Items: 
Size: 9827 Color: 406
Size: 1568 Color: 190
Size: 1536 Color: 189
Size: 1432 Color: 186
Size: 1396 Color: 181
Size: 1378 Color: 179
Size: 768 Color: 107
Size: 768 Color: 106
Size: 704 Color: 102

Bin 189: 273 of cap free
Amount of items: 3
Items: 
Size: 13687 Color: 471
Size: 5368 Color: 345
Size: 320 Color: 20

Bin 190: 290 of cap free
Amount of items: 8
Items: 
Size: 9828 Color: 407
Size: 1632 Color: 200
Size: 1632 Color: 199
Size: 1632 Color: 196
Size: 1632 Color: 195
Size: 1594 Color: 193
Size: 704 Color: 101
Size: 704 Color: 100

Bin 191: 317 of cap free
Amount of items: 5
Items: 
Size: 9836 Color: 410
Size: 3232 Color: 269
Size: 2912 Color: 260
Size: 2655 Color: 252
Size: 696 Color: 96

Bin 192: 324 of cap free
Amount of items: 5
Items: 
Size: 9840 Color: 411
Size: 3436 Color: 275
Size: 3425 Color: 274
Size: 1937 Color: 216
Size: 686 Color: 94

Bin 193: 345 of cap free
Amount of items: 3
Items: 
Size: 10566 Color: 420
Size: 8185 Color: 399
Size: 552 Color: 74

Bin 194: 402 of cap free
Amount of items: 3
Items: 
Size: 12462 Color: 449
Size: 6384 Color: 369
Size: 400 Color: 40

Bin 195: 464 of cap free
Amount of items: 2
Items: 
Size: 9888 Color: 413
Size: 9296 Color: 403

Bin 196: 488 of cap free
Amount of items: 3
Items: 
Size: 10416 Color: 419
Size: 8184 Color: 398
Size: 560 Color: 76

Bin 197: 529 of cap free
Amount of items: 2
Items: 
Size: 9848 Color: 412
Size: 9271 Color: 402

Bin 198: 803 of cap free
Amount of items: 10
Items: 
Size: 9825 Color: 404
Size: 1248 Color: 172
Size: 1232 Color: 171
Size: 1232 Color: 170
Size: 1216 Color: 167
Size: 840 Color: 122
Size: 832 Color: 121
Size: 832 Color: 120
Size: 816 Color: 117
Size: 772 Color: 113

Bin 199: 9562 of cap free
Amount of items: 10
Items: 
Size: 1056 Color: 153
Size: 1056 Color: 152
Size: 1046 Color: 151
Size: 1044 Color: 150
Size: 1006 Color: 146
Size: 1002 Color: 145
Size: 992 Color: 144
Size: 964 Color: 141
Size: 960 Color: 140
Size: 960 Color: 139

Total size: 3890304
Total free space: 19648

