Capicity Bin: 1000001
Lower Bound: 226

Bins used: 231
Amount of Colors: 2

Bin 1: 19 of cap free
Amount of items: 2
Items: 
Size: 627242 Color: 1
Size: 372740 Color: 0

Bin 2: 23 of cap free
Amount of items: 3
Items: 
Size: 764646 Color: 1
Size: 120935 Color: 1
Size: 114397 Color: 0

Bin 3: 29 of cap free
Amount of items: 2
Items: 
Size: 627939 Color: 0
Size: 372033 Color: 1

Bin 4: 44 of cap free
Amount of items: 3
Items: 
Size: 737237 Color: 1
Size: 139197 Color: 0
Size: 123523 Color: 1

Bin 5: 49 of cap free
Amount of items: 3
Items: 
Size: 756236 Color: 0
Size: 123733 Color: 0
Size: 119983 Color: 1

Bin 6: 50 of cap free
Amount of items: 2
Items: 
Size: 713054 Color: 1
Size: 286897 Color: 0

Bin 7: 52 of cap free
Amount of items: 2
Items: 
Size: 698194 Color: 1
Size: 301755 Color: 0

Bin 8: 67 of cap free
Amount of items: 2
Items: 
Size: 757880 Color: 0
Size: 242054 Color: 1

Bin 9: 92 of cap free
Amount of items: 3
Items: 
Size: 683453 Color: 1
Size: 167042 Color: 0
Size: 149414 Color: 1

Bin 10: 95 of cap free
Amount of items: 3
Items: 
Size: 736074 Color: 1
Size: 140731 Color: 0
Size: 123101 Color: 1

Bin 11: 116 of cap free
Amount of items: 2
Items: 
Size: 530982 Color: 1
Size: 468903 Color: 0

Bin 12: 116 of cap free
Amount of items: 3
Items: 
Size: 682353 Color: 0
Size: 173365 Color: 0
Size: 144167 Color: 1

Bin 13: 122 of cap free
Amount of items: 3
Items: 
Size: 789238 Color: 1
Size: 107765 Color: 1
Size: 102876 Color: 0

Bin 14: 138 of cap free
Amount of items: 3
Items: 
Size: 530520 Color: 0
Size: 300151 Color: 1
Size: 169192 Color: 1

Bin 15: 155 of cap free
Amount of items: 3
Items: 
Size: 773708 Color: 1
Size: 119513 Color: 0
Size: 106625 Color: 1

Bin 16: 159 of cap free
Amount of items: 2
Items: 
Size: 660723 Color: 0
Size: 339119 Color: 1

Bin 17: 170 of cap free
Amount of items: 2
Items: 
Size: 745485 Color: 1
Size: 254346 Color: 0

Bin 18: 176 of cap free
Amount of items: 3
Items: 
Size: 554951 Color: 0
Size: 284420 Color: 1
Size: 160454 Color: 1

Bin 19: 185 of cap free
Amount of items: 3
Items: 
Size: 756801 Color: 1
Size: 123142 Color: 1
Size: 119873 Color: 0

Bin 20: 189 of cap free
Amount of items: 3
Items: 
Size: 780392 Color: 1
Size: 113861 Color: 0
Size: 105559 Color: 0

Bin 21: 215 of cap free
Amount of items: 3
Items: 
Size: 700607 Color: 1
Size: 155938 Color: 0
Size: 143241 Color: 1

Bin 22: 223 of cap free
Amount of items: 2
Items: 
Size: 708735 Color: 1
Size: 291043 Color: 0

Bin 23: 231 of cap free
Amount of items: 2
Items: 
Size: 738411 Color: 0
Size: 261359 Color: 1

Bin 24: 248 of cap free
Amount of items: 3
Items: 
Size: 732118 Color: 0
Size: 156886 Color: 0
Size: 110749 Color: 1

Bin 25: 261 of cap free
Amount of items: 2
Items: 
Size: 683683 Color: 1
Size: 316057 Color: 0

Bin 26: 266 of cap free
Amount of items: 3
Items: 
Size: 546588 Color: 0
Size: 297563 Color: 1
Size: 155584 Color: 1

Bin 27: 295 of cap free
Amount of items: 2
Items: 
Size: 737552 Color: 1
Size: 262154 Color: 0

Bin 28: 317 of cap free
Amount of items: 3
Items: 
Size: 783611 Color: 0
Size: 114777 Color: 1
Size: 101296 Color: 1

Bin 29: 392 of cap free
Amount of items: 3
Items: 
Size: 764873 Color: 0
Size: 127144 Color: 0
Size: 107592 Color: 1

Bin 30: 406 of cap free
Amount of items: 2
Items: 
Size: 554179 Color: 1
Size: 445416 Color: 0

Bin 31: 426 of cap free
Amount of items: 3
Items: 
Size: 753007 Color: 1
Size: 130809 Color: 1
Size: 115759 Color: 0

Bin 32: 440 of cap free
Amount of items: 2
Items: 
Size: 529806 Color: 0
Size: 469755 Color: 1

Bin 33: 441 of cap free
Amount of items: 3
Items: 
Size: 537095 Color: 0
Size: 275444 Color: 1
Size: 187021 Color: 1

Bin 34: 469 of cap free
Amount of items: 2
Items: 
Size: 520071 Color: 0
Size: 479461 Color: 1

Bin 35: 488 of cap free
Amount of items: 2
Items: 
Size: 621484 Color: 1
Size: 378029 Color: 0

Bin 36: 507 of cap free
Amount of items: 2
Items: 
Size: 716649 Color: 1
Size: 282845 Color: 0

Bin 37: 513 of cap free
Amount of items: 3
Items: 
Size: 655301 Color: 1
Size: 181154 Color: 1
Size: 163033 Color: 0

Bin 38: 523 of cap free
Amount of items: 2
Items: 
Size: 688687 Color: 0
Size: 310791 Color: 1

Bin 39: 529 of cap free
Amount of items: 2
Items: 
Size: 507988 Color: 0
Size: 491484 Color: 1

Bin 40: 536 of cap free
Amount of items: 2
Items: 
Size: 559528 Color: 0
Size: 439937 Color: 1

Bin 41: 617 of cap free
Amount of items: 2
Items: 
Size: 735337 Color: 1
Size: 264047 Color: 0

Bin 42: 622 of cap free
Amount of items: 2
Items: 
Size: 525961 Color: 1
Size: 473418 Color: 0

Bin 43: 624 of cap free
Amount of items: 2
Items: 
Size: 503854 Color: 0
Size: 495523 Color: 1

Bin 44: 641 of cap free
Amount of items: 2
Items: 
Size: 746738 Color: 1
Size: 252622 Color: 0

Bin 45: 645 of cap free
Amount of items: 3
Items: 
Size: 789412 Color: 0
Size: 106925 Color: 0
Size: 103019 Color: 1

Bin 46: 647 of cap free
Amount of items: 3
Items: 
Size: 636636 Color: 0
Size: 195906 Color: 1
Size: 166812 Color: 1

Bin 47: 651 of cap free
Amount of items: 2
Items: 
Size: 656311 Color: 0
Size: 343039 Color: 1

Bin 48: 662 of cap free
Amount of items: 2
Items: 
Size: 506324 Color: 0
Size: 493015 Color: 1

Bin 49: 662 of cap free
Amount of items: 2
Items: 
Size: 598787 Color: 1
Size: 400552 Color: 0

Bin 50: 664 of cap free
Amount of items: 3
Items: 
Size: 608844 Color: 1
Size: 224425 Color: 0
Size: 166068 Color: 1

Bin 51: 669 of cap free
Amount of items: 2
Items: 
Size: 533895 Color: 0
Size: 465437 Color: 1

Bin 52: 671 of cap free
Amount of items: 2
Items: 
Size: 577235 Color: 1
Size: 422095 Color: 0

Bin 53: 676 of cap free
Amount of items: 2
Items: 
Size: 700958 Color: 1
Size: 298367 Color: 0

Bin 54: 682 of cap free
Amount of items: 2
Items: 
Size: 644165 Color: 1
Size: 355154 Color: 0

Bin 55: 690 of cap free
Amount of items: 2
Items: 
Size: 513084 Color: 1
Size: 486227 Color: 0

Bin 56: 711 of cap free
Amount of items: 2
Items: 
Size: 585150 Color: 1
Size: 414140 Color: 0

Bin 57: 714 of cap free
Amount of items: 2
Items: 
Size: 527679 Color: 0
Size: 471608 Color: 1

Bin 58: 716 of cap free
Amount of items: 2
Items: 
Size: 712514 Color: 0
Size: 286771 Color: 1

Bin 59: 722 of cap free
Amount of items: 2
Items: 
Size: 544364 Color: 1
Size: 454915 Color: 0

Bin 60: 746 of cap free
Amount of items: 2
Items: 
Size: 597768 Color: 1
Size: 401487 Color: 0

Bin 61: 750 of cap free
Amount of items: 3
Items: 
Size: 527035 Color: 0
Size: 296647 Color: 1
Size: 175569 Color: 1

Bin 62: 769 of cap free
Amount of items: 2
Items: 
Size: 656690 Color: 1
Size: 342542 Color: 0

Bin 63: 807 of cap free
Amount of items: 3
Items: 
Size: 721913 Color: 1
Size: 146893 Color: 0
Size: 130388 Color: 1

Bin 64: 818 of cap free
Amount of items: 2
Items: 
Size: 660985 Color: 1
Size: 338198 Color: 0

Bin 65: 837 of cap free
Amount of items: 2
Items: 
Size: 614979 Color: 1
Size: 384185 Color: 0

Bin 66: 838 of cap free
Amount of items: 2
Items: 
Size: 508476 Color: 1
Size: 490687 Color: 0

Bin 67: 839 of cap free
Amount of items: 2
Items: 
Size: 587670 Color: 1
Size: 411492 Color: 0

Bin 68: 866 of cap free
Amount of items: 2
Items: 
Size: 589541 Color: 1
Size: 409594 Color: 0

Bin 69: 870 of cap free
Amount of items: 3
Items: 
Size: 575178 Color: 0
Size: 222684 Color: 0
Size: 201269 Color: 1

Bin 70: 898 of cap free
Amount of items: 2
Items: 
Size: 696478 Color: 0
Size: 302625 Color: 1

Bin 71: 967 of cap free
Amount of items: 2
Items: 
Size: 575440 Color: 1
Size: 423594 Color: 0

Bin 72: 993 of cap free
Amount of items: 2
Items: 
Size: 672957 Color: 0
Size: 326051 Color: 1

Bin 73: 1004 of cap free
Amount of items: 3
Items: 
Size: 571686 Color: 1
Size: 222663 Color: 0
Size: 204648 Color: 1

Bin 74: 1056 of cap free
Amount of items: 2
Items: 
Size: 629920 Color: 1
Size: 369025 Color: 0

Bin 75: 1067 of cap free
Amount of items: 3
Items: 
Size: 641503 Color: 0
Size: 184618 Color: 1
Size: 172813 Color: 1

Bin 76: 1075 of cap free
Amount of items: 3
Items: 
Size: 682206 Color: 0
Size: 166874 Color: 0
Size: 149846 Color: 1

Bin 77: 1087 of cap free
Amount of items: 2
Items: 
Size: 557105 Color: 1
Size: 441809 Color: 0

Bin 78: 1146 of cap free
Amount of items: 2
Items: 
Size: 501929 Color: 1
Size: 496926 Color: 0

Bin 79: 1147 of cap free
Amount of items: 2
Items: 
Size: 713534 Color: 0
Size: 285320 Color: 1

Bin 80: 1148 of cap free
Amount of items: 2
Items: 
Size: 544000 Color: 0
Size: 454853 Color: 1

Bin 81: 1156 of cap free
Amount of items: 2
Items: 
Size: 677089 Color: 0
Size: 321756 Color: 1

Bin 82: 1176 of cap free
Amount of items: 2
Items: 
Size: 568949 Color: 1
Size: 429876 Color: 0

Bin 83: 1179 of cap free
Amount of items: 2
Items: 
Size: 579922 Color: 1
Size: 418900 Color: 0

Bin 84: 1212 of cap free
Amount of items: 2
Items: 
Size: 623637 Color: 0
Size: 375152 Color: 1

Bin 85: 1223 of cap free
Amount of items: 3
Items: 
Size: 525426 Color: 0
Size: 277887 Color: 1
Size: 195465 Color: 1

Bin 86: 1256 of cap free
Amount of items: 3
Items: 
Size: 683368 Color: 1
Size: 163796 Color: 0
Size: 151581 Color: 1

Bin 87: 1260 of cap free
Amount of items: 3
Items: 
Size: 672558 Color: 1
Size: 182353 Color: 0
Size: 143830 Color: 1

Bin 88: 1307 of cap free
Amount of items: 3
Items: 
Size: 751453 Color: 1
Size: 126420 Color: 0
Size: 120821 Color: 1

Bin 89: 1323 of cap free
Amount of items: 2
Items: 
Size: 751700 Color: 0
Size: 246978 Color: 1

Bin 90: 1337 of cap free
Amount of items: 3
Items: 
Size: 703835 Color: 0
Size: 154054 Color: 0
Size: 140775 Color: 1

Bin 91: 1347 of cap free
Amount of items: 2
Items: 
Size: 782374 Color: 1
Size: 216280 Color: 0

Bin 92: 1348 of cap free
Amount of items: 2
Items: 
Size: 766755 Color: 1
Size: 231898 Color: 0

Bin 93: 1435 of cap free
Amount of items: 3
Items: 
Size: 639405 Color: 0
Size: 205920 Color: 1
Size: 153241 Color: 0

Bin 94: 1450 of cap free
Amount of items: 2
Items: 
Size: 511363 Color: 0
Size: 487188 Color: 1

Bin 95: 1461 of cap free
Amount of items: 2
Items: 
Size: 504769 Color: 1
Size: 493771 Color: 0

Bin 96: 1462 of cap free
Amount of items: 2
Items: 
Size: 611592 Color: 0
Size: 386947 Color: 1

Bin 97: 1480 of cap free
Amount of items: 2
Items: 
Size: 670628 Color: 1
Size: 327893 Color: 0

Bin 98: 1595 of cap free
Amount of items: 2
Items: 
Size: 780523 Color: 0
Size: 217883 Color: 1

Bin 99: 1642 of cap free
Amount of items: 2
Items: 
Size: 560550 Color: 0
Size: 437809 Color: 1

Bin 100: 1710 of cap free
Amount of items: 2
Items: 
Size: 762956 Color: 0
Size: 235335 Color: 1

Bin 101: 1755 of cap free
Amount of items: 2
Items: 
Size: 676158 Color: 1
Size: 322088 Color: 0

Bin 102: 1801 of cap free
Amount of items: 2
Items: 
Size: 754183 Color: 0
Size: 244017 Color: 1

Bin 103: 1831 of cap free
Amount of items: 2
Items: 
Size: 616173 Color: 1
Size: 381997 Color: 0

Bin 104: 1863 of cap free
Amount of items: 2
Items: 
Size: 578567 Color: 0
Size: 419571 Color: 1

Bin 105: 1866 of cap free
Amount of items: 2
Items: 
Size: 730910 Color: 0
Size: 267225 Color: 1

Bin 106: 1947 of cap free
Amount of items: 2
Items: 
Size: 642381 Color: 0
Size: 355673 Color: 1

Bin 107: 1979 of cap free
Amount of items: 2
Items: 
Size: 796709 Color: 1
Size: 201313 Color: 0

Bin 108: 2101 of cap free
Amount of items: 2
Items: 
Size: 629467 Color: 1
Size: 368433 Color: 0

Bin 109: 2102 of cap free
Amount of items: 2
Items: 
Size: 753149 Color: 1
Size: 244750 Color: 0

Bin 110: 2183 of cap free
Amount of items: 2
Items: 
Size: 501205 Color: 1
Size: 496613 Color: 0

Bin 111: 2359 of cap free
Amount of items: 2
Items: 
Size: 517786 Color: 1
Size: 479856 Color: 0

Bin 112: 2366 of cap free
Amount of items: 2
Items: 
Size: 541188 Color: 1
Size: 456447 Color: 0

Bin 113: 2405 of cap free
Amount of items: 2
Items: 
Size: 690259 Color: 0
Size: 307337 Color: 1

Bin 114: 2509 of cap free
Amount of items: 2
Items: 
Size: 658832 Color: 0
Size: 338660 Color: 1

Bin 115: 2548 of cap free
Amount of items: 2
Items: 
Size: 768556 Color: 0
Size: 228897 Color: 1

Bin 116: 2656 of cap free
Amount of items: 2
Items: 
Size: 697339 Color: 1
Size: 300006 Color: 0

Bin 117: 2666 of cap free
Amount of items: 2
Items: 
Size: 708287 Color: 1
Size: 289048 Color: 0

Bin 118: 2816 of cap free
Amount of items: 3
Items: 
Size: 653096 Color: 0
Size: 178810 Color: 1
Size: 165279 Color: 0

Bin 119: 2820 of cap free
Amount of items: 2
Items: 
Size: 547965 Color: 1
Size: 449216 Color: 0

Bin 120: 2840 of cap free
Amount of items: 2
Items: 
Size: 539730 Color: 0
Size: 457431 Color: 1

Bin 121: 2847 of cap free
Amount of items: 2
Items: 
Size: 501746 Color: 0
Size: 495408 Color: 1

Bin 122: 2911 of cap free
Amount of items: 2
Items: 
Size: 684033 Color: 1
Size: 313057 Color: 0

Bin 123: 3003 of cap free
Amount of items: 2
Items: 
Size: 570735 Color: 0
Size: 426263 Color: 1

Bin 124: 3018 of cap free
Amount of items: 2
Items: 
Size: 672091 Color: 0
Size: 324892 Color: 1

Bin 125: 3058 of cap free
Amount of items: 2
Items: 
Size: 631577 Color: 0
Size: 365366 Color: 1

Bin 126: 3100 of cap free
Amount of items: 2
Items: 
Size: 746841 Color: 0
Size: 250060 Color: 1

Bin 127: 3109 of cap free
Amount of items: 2
Items: 
Size: 665977 Color: 1
Size: 330915 Color: 0

Bin 128: 3164 of cap free
Amount of items: 2
Items: 
Size: 762178 Color: 1
Size: 234659 Color: 0

Bin 129: 3217 of cap free
Amount of items: 2
Items: 
Size: 796642 Color: 1
Size: 200142 Color: 0

Bin 130: 3323 of cap free
Amount of items: 3
Items: 
Size: 720330 Color: 1
Size: 158131 Color: 0
Size: 118217 Color: 1

Bin 131: 3463 of cap free
Amount of items: 2
Items: 
Size: 607508 Color: 0
Size: 389030 Color: 1

Bin 132: 3489 of cap free
Amount of items: 2
Items: 
Size: 786223 Color: 0
Size: 210289 Color: 1

Bin 133: 3524 of cap free
Amount of items: 2
Items: 
Size: 615159 Color: 0
Size: 381318 Color: 1

Bin 134: 3540 of cap free
Amount of items: 2
Items: 
Size: 608827 Color: 1
Size: 387634 Color: 0

Bin 135: 3654 of cap free
Amount of items: 2
Items: 
Size: 660383 Color: 1
Size: 335964 Color: 0

Bin 136: 3684 of cap free
Amount of items: 2
Items: 
Size: 647617 Color: 0
Size: 348700 Color: 1

Bin 137: 3742 of cap free
Amount of items: 2
Items: 
Size: 707321 Color: 1
Size: 288938 Color: 0

Bin 138: 3784 of cap free
Amount of items: 2
Items: 
Size: 558448 Color: 0
Size: 437769 Color: 1

Bin 139: 3793 of cap free
Amount of items: 2
Items: 
Size: 626405 Color: 0
Size: 369803 Color: 1

Bin 140: 3801 of cap free
Amount of items: 2
Items: 
Size: 509698 Color: 0
Size: 486502 Color: 1

Bin 141: 3838 of cap free
Amount of items: 2
Items: 
Size: 728580 Color: 1
Size: 267583 Color: 0

Bin 142: 3895 of cap free
Amount of items: 2
Items: 
Size: 579662 Color: 1
Size: 416444 Color: 0

Bin 143: 4047 of cap free
Amount of items: 2
Items: 
Size: 549229 Color: 0
Size: 446725 Color: 1

Bin 144: 4157 of cap free
Amount of items: 3
Items: 
Size: 446810 Color: 0
Size: 345360 Color: 0
Size: 203674 Color: 1

Bin 145: 4226 of cap free
Amount of items: 3
Items: 
Size: 751693 Color: 1
Size: 126563 Color: 0
Size: 117519 Color: 0

Bin 146: 4310 of cap free
Amount of items: 2
Items: 
Size: 620419 Color: 1
Size: 375272 Color: 0

Bin 147: 4358 of cap free
Amount of items: 2
Items: 
Size: 741116 Color: 1
Size: 254527 Color: 0

Bin 148: 4359 of cap free
Amount of items: 2
Items: 
Size: 657193 Color: 0
Size: 338449 Color: 1

Bin 149: 4451 of cap free
Amount of items: 2
Items: 
Size: 620322 Color: 1
Size: 375228 Color: 0

Bin 150: 4588 of cap free
Amount of items: 2
Items: 
Size: 766273 Color: 1
Size: 229140 Color: 0

Bin 151: 4697 of cap free
Amount of items: 2
Items: 
Size: 756120 Color: 0
Size: 239184 Color: 1

Bin 152: 4773 of cap free
Amount of items: 2
Items: 
Size: 590586 Color: 1
Size: 404642 Color: 0

Bin 153: 4940 of cap free
Amount of items: 2
Items: 
Size: 683531 Color: 0
Size: 311530 Color: 1

Bin 154: 5008 of cap free
Amount of items: 3
Items: 
Size: 722817 Color: 1
Size: 138411 Color: 0
Size: 133765 Color: 0

Bin 155: 5058 of cap free
Amount of items: 2
Items: 
Size: 547492 Color: 1
Size: 447451 Color: 0

Bin 156: 5137 of cap free
Amount of items: 2
Items: 
Size: 631015 Color: 0
Size: 363849 Color: 1

Bin 157: 5287 of cap free
Amount of items: 2
Items: 
Size: 613008 Color: 1
Size: 381706 Color: 0

Bin 158: 5545 of cap free
Amount of items: 3
Items: 
Size: 577229 Color: 0
Size: 256569 Color: 1
Size: 160658 Color: 1

Bin 159: 5762 of cap free
Amount of items: 2
Items: 
Size: 695119 Color: 1
Size: 299120 Color: 0

Bin 160: 5953 of cap free
Amount of items: 3
Items: 
Size: 522761 Color: 0
Size: 281887 Color: 1
Size: 189400 Color: 1

Bin 161: 6044 of cap free
Amount of items: 2
Items: 
Size: 673743 Color: 1
Size: 320214 Color: 0

Bin 162: 6046 of cap free
Amount of items: 2
Items: 
Size: 514691 Color: 1
Size: 479264 Color: 0

Bin 163: 6076 of cap free
Amount of items: 2
Items: 
Size: 780937 Color: 1
Size: 212988 Color: 0

Bin 164: 6154 of cap free
Amount of items: 2
Items: 
Size: 743871 Color: 0
Size: 249976 Color: 1

Bin 165: 6179 of cap free
Amount of items: 2
Items: 
Size: 630818 Color: 0
Size: 363004 Color: 1

Bin 166: 6344 of cap free
Amount of items: 2
Items: 
Size: 596357 Color: 0
Size: 397300 Color: 1

Bin 167: 7076 of cap free
Amount of items: 2
Items: 
Size: 730891 Color: 0
Size: 262034 Color: 1

Bin 168: 7121 of cap free
Amount of items: 2
Items: 
Size: 727906 Color: 1
Size: 264974 Color: 0

Bin 169: 7134 of cap free
Amount of items: 2
Items: 
Size: 563089 Color: 0
Size: 429778 Color: 1

Bin 170: 7786 of cap free
Amount of items: 2
Items: 
Size: 707168 Color: 1
Size: 285047 Color: 0

Bin 171: 7820 of cap free
Amount of items: 2
Items: 
Size: 645610 Color: 0
Size: 346571 Color: 1

Bin 172: 7858 of cap free
Amount of items: 2
Items: 
Size: 743792 Color: 0
Size: 248351 Color: 1

Bin 173: 8135 of cap free
Amount of items: 2
Items: 
Size: 640105 Color: 1
Size: 351761 Color: 0

Bin 174: 8640 of cap free
Amount of items: 2
Items: 
Size: 754046 Color: 0
Size: 237315 Color: 1

Bin 175: 8871 of cap free
Amount of items: 2
Items: 
Size: 706770 Color: 1
Size: 284360 Color: 0

Bin 176: 9136 of cap free
Amount of items: 2
Items: 
Size: 495549 Color: 0
Size: 495316 Color: 1

Bin 177: 9154 of cap free
Amount of items: 2
Items: 
Size: 578157 Color: 0
Size: 412690 Color: 1

Bin 178: 9177 of cap free
Amount of items: 2
Items: 
Size: 566018 Color: 1
Size: 424806 Color: 0

Bin 179: 9772 of cap free
Amount of items: 2
Items: 
Size: 590496 Color: 1
Size: 399733 Color: 0

Bin 180: 9970 of cap free
Amount of items: 2
Items: 
Size: 514681 Color: 1
Size: 475350 Color: 0

Bin 181: 10489 of cap free
Amount of items: 2
Items: 
Size: 738243 Color: 1
Size: 251269 Color: 0

Bin 182: 10943 of cap free
Amount of items: 2
Items: 
Size: 792691 Color: 1
Size: 196367 Color: 0

Bin 183: 11007 of cap free
Amount of items: 2
Items: 
Size: 638342 Color: 1
Size: 350652 Color: 0

Bin 184: 11309 of cap free
Amount of items: 2
Items: 
Size: 565691 Color: 1
Size: 423001 Color: 0

Bin 185: 11507 of cap free
Amount of items: 2
Items: 
Size: 659042 Color: 1
Size: 329452 Color: 0

Bin 186: 12089 of cap free
Amount of items: 2
Items: 
Size: 494358 Color: 1
Size: 493554 Color: 0

Bin 187: 12536 of cap free
Amount of items: 2
Items: 
Size: 791112 Color: 1
Size: 196353 Color: 0

Bin 188: 13033 of cap free
Amount of items: 2
Items: 
Size: 606016 Color: 1
Size: 380952 Color: 0

Bin 189: 13242 of cap free
Amount of items: 3
Items: 
Size: 521796 Color: 0
Size: 304674 Color: 1
Size: 160289 Color: 1

Bin 190: 13464 of cap free
Amount of items: 2
Items: 
Size: 725180 Color: 0
Size: 261357 Color: 1

Bin 191: 13615 of cap free
Amount of items: 2
Items: 
Size: 681320 Color: 0
Size: 305066 Color: 1

Bin 192: 13749 of cap free
Amount of items: 2
Items: 
Size: 689426 Color: 1
Size: 296826 Color: 0

Bin 193: 13994 of cap free
Amount of items: 2
Items: 
Size: 658402 Color: 1
Size: 327605 Color: 0

Bin 194: 14766 of cap free
Amount of items: 2
Items: 
Size: 492670 Color: 1
Size: 492565 Color: 0

Bin 195: 14992 of cap free
Amount of items: 2
Items: 
Size: 575088 Color: 0
Size: 409921 Color: 1

Bin 196: 15178 of cap free
Amount of items: 2
Items: 
Size: 724788 Color: 0
Size: 260035 Color: 1

Bin 197: 15226 of cap free
Amount of items: 2
Items: 
Size: 789809 Color: 1
Size: 194966 Color: 0

Bin 198: 16575 of cap free
Amount of items: 2
Items: 
Size: 758078 Color: 1
Size: 225348 Color: 0

Bin 199: 16601 of cap free
Amount of items: 2
Items: 
Size: 687606 Color: 1
Size: 295794 Color: 0

Bin 200: 17161 of cap free
Amount of items: 2
Items: 
Size: 583668 Color: 1
Size: 399172 Color: 0

Bin 201: 17546 of cap free
Amount of items: 2
Items: 
Size: 757607 Color: 1
Size: 224848 Color: 0

Bin 202: 21187 of cap free
Amount of items: 2
Items: 
Size: 632129 Color: 1
Size: 346685 Color: 0

Bin 203: 21267 of cap free
Amount of items: 2
Items: 
Size: 605694 Color: 1
Size: 373040 Color: 0

Bin 204: 21856 of cap free
Amount of items: 2
Items: 
Size: 512040 Color: 1
Size: 466105 Color: 0

Bin 205: 22002 of cap free
Amount of items: 2
Items: 
Size: 579199 Color: 1
Size: 398800 Color: 0

Bin 206: 25308 of cap free
Amount of items: 2
Items: 
Size: 750024 Color: 1
Size: 224669 Color: 0

Bin 207: 27304 of cap free
Amount of items: 2
Items: 
Size: 601773 Color: 1
Size: 370924 Color: 0

Bin 208: 30889 of cap free
Amount of items: 2
Items: 
Size: 488303 Color: 0
Size: 480809 Color: 1

Bin 209: 31543 of cap free
Amount of items: 2
Items: 
Size: 780144 Color: 1
Size: 188314 Color: 0

Bin 210: 34077 of cap free
Amount of items: 2
Items: 
Size: 556544 Color: 0
Size: 409380 Color: 1

Bin 211: 37191 of cap free
Amount of items: 2
Items: 
Size: 554355 Color: 0
Size: 408455 Color: 1

Bin 212: 44279 of cap free
Amount of items: 2
Items: 
Size: 548022 Color: 0
Size: 407700 Color: 1

Bin 213: 45905 of cap free
Amount of items: 2
Items: 
Size: 547600 Color: 0
Size: 406496 Color: 1

Bin 214: 57226 of cap free
Amount of items: 2
Items: 
Size: 480580 Color: 1
Size: 462195 Color: 0

Bin 215: 60373 of cap free
Amount of items: 2
Items: 
Size: 546832 Color: 0
Size: 392796 Color: 1

Bin 216: 75440 of cap free
Amount of items: 2
Items: 
Size: 539159 Color: 0
Size: 385402 Color: 1

Bin 217: 81321 of cap free
Amount of items: 2
Items: 
Size: 538726 Color: 0
Size: 379954 Color: 1

Bin 218: 100778 of cap free
Amount of items: 2
Items: 
Size: 536764 Color: 0
Size: 362459 Color: 1

Bin 219: 105736 of cap free
Amount of items: 2
Items: 
Size: 533324 Color: 0
Size: 360941 Color: 1

Bin 220: 122209 of cap free
Amount of items: 2
Items: 
Size: 531628 Color: 0
Size: 346164 Color: 1

Bin 221: 148195 of cap free
Amount of items: 2
Items: 
Size: 523185 Color: 0
Size: 328621 Color: 1

Bin 222: 174309 of cap free
Amount of items: 2
Items: 
Size: 521665 Color: 0
Size: 304027 Color: 1

Bin 223: 177477 of cap free
Amount of items: 2
Items: 
Size: 521284 Color: 0
Size: 301240 Color: 1

Bin 224: 178236 of cap free
Amount of items: 2
Items: 
Size: 520746 Color: 0
Size: 301019 Color: 1

Bin 225: 248564 of cap free
Amount of items: 2
Items: 
Size: 459252 Color: 0
Size: 292185 Color: 1

Bin 226: 282543 of cap free
Amount of items: 2
Items: 
Size: 438073 Color: 0
Size: 279385 Color: 1

Bin 227: 302877 of cap free
Amount of items: 2
Items: 
Size: 420817 Color: 0
Size: 276307 Color: 1

Bin 228: 536920 of cap free
Amount of items: 1
Items: 
Size: 463081 Color: 0

Bin 229: 538068 of cap free
Amount of items: 1
Items: 
Size: 461933 Color: 0

Bin 230: 561066 of cap free
Amount of items: 1
Items: 
Size: 438935 Color: 0

Bin 231: 564831 of cap free
Amount of items: 1
Items: 
Size: 435170 Color: 0

Total size: 225601168
Total free space: 5399063

