Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 16
Size: 500 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 18
Size: 276 Color: 2
Size: 255 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 13
Size: 374 Color: 8
Size: 253 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 8
Size: 299 Color: 9
Size: 250 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 4
Size: 281 Color: 9
Size: 269 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 18
Size: 318 Color: 2
Size: 311 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 351 Color: 17
Size: 260 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 5
Size: 274 Color: 6
Size: 252 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 18
Size: 302 Color: 16
Size: 275 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 18
Size: 343 Color: 5
Size: 282 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 7
Size: 306 Color: 11
Size: 269 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 17
Size: 256 Color: 5
Size: 255 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 11
Size: 263 Color: 5
Size: 254 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 14
Size: 251 Color: 4
Size: 250 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 263 Color: 13
Size: 261 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 2
Size: 257 Color: 12
Size: 251 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 0
Size: 345 Color: 8
Size: 301 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 353 Color: 7
Size: 264 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 10
Size: 325 Color: 19
Size: 268 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 2
Size: 326 Color: 11
Size: 290 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 15
Size: 324 Color: 5
Size: 305 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 8
Size: 288 Color: 15
Size: 250 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 317 Color: 18
Size: 274 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 274 Color: 3
Size: 270 Color: 15

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 10
Size: 320 Color: 11
Size: 259 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 7
Size: 339 Color: 5
Size: 264 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 9
Size: 360 Color: 0
Size: 280 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 17
Size: 286 Color: 0
Size: 254 Color: 6

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 8
Size: 312 Color: 9
Size: 250 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 322 Color: 2
Size: 292 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 325 Color: 15
Size: 250 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 9
Size: 303 Color: 13
Size: 284 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 6
Size: 304 Color: 0
Size: 262 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 330 Color: 5
Size: 283 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 18
Size: 260 Color: 11
Size: 255 Color: 12

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9
Size: 260 Color: 13
Size: 260 Color: 11

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 289 Color: 15
Size: 266 Color: 16

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 16
Size: 285 Color: 0
Size: 250 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 9
Size: 300 Color: 11
Size: 281 Color: 19

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 6
Size: 340 Color: 15
Size: 275 Color: 17

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 322 Color: 6
Size: 302 Color: 19

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 7
Size: 261 Color: 14
Size: 251 Color: 7

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 6
Size: 342 Color: 19
Size: 304 Color: 9

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 301 Color: 19
Size: 278 Color: 17

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 10
Size: 298 Color: 14
Size: 255 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 7
Size: 327 Color: 1
Size: 256 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 13
Size: 361 Color: 16
Size: 256 Color: 4

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 9
Size: 333 Color: 14
Size: 311 Color: 17

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 285 Color: 12
Size: 265 Color: 14

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 9
Size: 296 Color: 7
Size: 263 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 16
Size: 312 Color: 14
Size: 268 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 343 Color: 17
Size: 251 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 9
Size: 258 Color: 1
Size: 252 Color: 7

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 8
Size: 313 Color: 7
Size: 291 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 8
Size: 253 Color: 0
Size: 250 Color: 3

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 9
Size: 337 Color: 4
Size: 251 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 5
Size: 349 Color: 10
Size: 293 Color: 18

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 297 Color: 16
Size: 287 Color: 5

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 17
Size: 295 Color: 11
Size: 278 Color: 7

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 329 Color: 11
Size: 258 Color: 19

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 16
Size: 261 Color: 16
Size: 258 Color: 2

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 304 Color: 13
Size: 264 Color: 1

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 16
Size: 293 Color: 17
Size: 262 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 290 Color: 8
Size: 257 Color: 14

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 324 Color: 9
Size: 295 Color: 17

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 12
Size: 266 Color: 17
Size: 264 Color: 10

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 317 Color: 9
Size: 287 Color: 7

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 275 Color: 6
Size: 263 Color: 10

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 4
Size: 321 Color: 18
Size: 300 Color: 16

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 10
Size: 280 Color: 11
Size: 257 Color: 18

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 9
Size: 272 Color: 17
Size: 250 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 17
Size: 306 Color: 8
Size: 267 Color: 15

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 19
Size: 270 Color: 15
Size: 268 Color: 17

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 13
Size: 301 Color: 0
Size: 275 Color: 6

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 11
Size: 288 Color: 5
Size: 268 Color: 16

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 326 Color: 12
Size: 308 Color: 13

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 17
Size: 325 Color: 4
Size: 271 Color: 8

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 2
Size: 300 Color: 2
Size: 257 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 2
Size: 323 Color: 18
Size: 297 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 13
Size: 286 Color: 16
Size: 283 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 11
Size: 267 Color: 17
Size: 259 Color: 11

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 12
Size: 263 Color: 4
Size: 259 Color: 17

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 11
Size: 302 Color: 13
Size: 287 Color: 16

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7
Size: 355 Color: 8
Size: 251 Color: 6

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 291 Color: 5
Size: 288 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 6
Size: 257 Color: 9
Size: 255 Color: 14

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 2
Size: 344 Color: 17
Size: 303 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 0
Size: 299 Color: 7
Size: 256 Color: 14

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 12
Size: 369 Color: 13
Size: 252 Color: 16

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 12
Size: 321 Color: 10
Size: 270 Color: 4

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 296 Color: 7
Size: 279 Color: 11

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 309 Color: 17
Size: 305 Color: 18

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 299 Color: 10
Size: 297 Color: 5

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8
Size: 290 Color: 4
Size: 278 Color: 4

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 11
Size: 325 Color: 7
Size: 270 Color: 2

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 9
Size: 291 Color: 15
Size: 265 Color: 3

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 9
Size: 279 Color: 0
Size: 275 Color: 19

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 11
Size: 282 Color: 6
Size: 273 Color: 8

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 15
Size: 305 Color: 13
Size: 300 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 293 Color: 15
Size: 250 Color: 6

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 17
Size: 272 Color: 13
Size: 265 Color: 19

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 15
Size: 298 Color: 16
Size: 253 Color: 6

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 19
Size: 324 Color: 17
Size: 265 Color: 3

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8
Size: 346 Color: 9
Size: 252 Color: 11

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 339 Color: 14
Size: 296 Color: 4

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 5
Size: 323 Color: 3
Size: 277 Color: 18

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 5
Size: 277 Color: 2
Size: 261 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 3
Size: 273 Color: 14
Size: 264 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 12
Size: 345 Color: 9
Size: 302 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 15
Size: 279 Color: 3
Size: 252 Color: 3

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 10
Size: 299 Color: 15
Size: 274 Color: 1

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 351 Color: 13
Size: 283 Color: 18

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 12
Size: 343 Color: 13
Size: 264 Color: 11

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 17
Size: 318 Color: 2
Size: 306 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 354 Color: 13
Size: 290 Color: 2

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 260 Color: 4
Size: 254 Color: 18

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 18
Size: 336 Color: 7
Size: 273 Color: 12

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 7
Size: 298 Color: 19
Size: 260 Color: 10

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 311 Color: 15
Size: 276 Color: 4

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 7
Size: 330 Color: 0
Size: 322 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 6
Size: 284 Color: 19
Size: 258 Color: 13

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 13
Size: 299 Color: 10
Size: 273 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 9
Size: 334 Color: 3
Size: 251 Color: 12

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 311 Color: 0
Size: 309 Color: 8

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 16
Size: 327 Color: 9
Size: 290 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 8
Size: 329 Color: 17
Size: 252 Color: 11

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 10
Size: 258 Color: 1
Size: 256 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 5
Size: 310 Color: 19
Size: 250 Color: 13

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 4
Size: 310 Color: 0
Size: 267 Color: 9

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 12
Size: 303 Color: 12
Size: 299 Color: 13

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 311 Color: 16
Size: 256 Color: 5

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 18
Size: 335 Color: 13
Size: 312 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 9
Size: 256 Color: 2
Size: 253 Color: 6

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 337 Color: 19
Size: 304 Color: 19

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 15
Size: 266 Color: 0
Size: 260 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 16
Size: 359 Color: 17
Size: 273 Color: 15

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 10
Size: 268 Color: 12
Size: 265 Color: 19

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 6
Size: 262 Color: 11
Size: 260 Color: 12

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 11
Size: 314 Color: 3
Size: 266 Color: 9

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 19
Size: 326 Color: 12
Size: 323 Color: 6

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 0
Size: 302 Color: 6
Size: 282 Color: 6

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 19
Size: 288 Color: 12
Size: 269 Color: 19

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 272 Color: 10
Size: 272 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 19
Size: 324 Color: 11
Size: 276 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 282 Color: 19
Size: 250 Color: 15

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 276 Color: 4
Size: 254 Color: 7

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 15
Size: 329 Color: 12
Size: 305 Color: 11

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 371 Color: 5
Size: 251 Color: 17

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 5
Size: 301 Color: 14
Size: 292 Color: 8

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 357 Color: 19
Size: 268 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 5
Size: 273 Color: 3
Size: 257 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 6
Size: 286 Color: 0
Size: 271 Color: 9

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 18
Size: 278 Color: 18
Size: 251 Color: 14

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7
Size: 357 Color: 18
Size: 255 Color: 17

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 7
Size: 300 Color: 3
Size: 260 Color: 17

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 18
Size: 331 Color: 13
Size: 268 Color: 18

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 7
Size: 287 Color: 11
Size: 254 Color: 14

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 0
Size: 277 Color: 9
Size: 255 Color: 7

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 9
Size: 333 Color: 15
Size: 316 Color: 10

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 19
Size: 284 Color: 10
Size: 256 Color: 5

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 288 Color: 17
Size: 254 Color: 16

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 11
Size: 349 Color: 6
Size: 252 Color: 4

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 5
Size: 299 Color: 10
Size: 291 Color: 8

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 19
Size: 305 Color: 17
Size: 261 Color: 6

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 328 Color: 15
Size: 309 Color: 19

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 14
Size: 320 Color: 18
Size: 260 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 12
Size: 278 Color: 12
Size: 264 Color: 5

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 338 Color: 13
Size: 290 Color: 8

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 321 Color: 0
Size: 286 Color: 3

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 16
Size: 337 Color: 1
Size: 295 Color: 3

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 2
Size: 260 Color: 12
Size: 250 Color: 17

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 308 Color: 14
Size: 266 Color: 8

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 13
Size: 289 Color: 9
Size: 272 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 19
Size: 293 Color: 12
Size: 259 Color: 15

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 6
Size: 291 Color: 15
Size: 266 Color: 6

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 15
Size: 359 Color: 1
Size: 259 Color: 18

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 338 Color: 6
Size: 287 Color: 8

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 18
Size: 265 Color: 7
Size: 261 Color: 3

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 6
Size: 279 Color: 8
Size: 274 Color: 2

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 14
Size: 288 Color: 10
Size: 252 Color: 17

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 13
Size: 353 Color: 4
Size: 281 Color: 7

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 10
Size: 254 Color: 12
Size: 251 Color: 6

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 15
Size: 253 Color: 16
Size: 251 Color: 11

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 0
Size: 354 Color: 12
Size: 286 Color: 18

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 9
Size: 299 Color: 3
Size: 293 Color: 13

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 10
Size: 258 Color: 8
Size: 258 Color: 2

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 372 Color: 14
Size: 255 Color: 14

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 17
Size: 342 Color: 10
Size: 288 Color: 14

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 13
Size: 314 Color: 1
Size: 314 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 9
Size: 299 Color: 1
Size: 293 Color: 13

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 14
Size: 332 Color: 9
Size: 310 Color: 11

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 6
Size: 294 Color: 12
Size: 255 Color: 15

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 7
Size: 299 Color: 10
Size: 251 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 18
Size: 278 Color: 17
Size: 255 Color: 15

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 15
Size: 272 Color: 4
Size: 250 Color: 10

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 19
Size: 348 Color: 16
Size: 277 Color: 3

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 357 Color: 18
Size: 257 Color: 11

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 8
Size: 288 Color: 11
Size: 253 Color: 14

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 8
Size: 300 Color: 3
Size: 282 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 16
Size: 360 Color: 3
Size: 250 Color: 6

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 13
Size: 260 Color: 16
Size: 252 Color: 2

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 9
Size: 288 Color: 8
Size: 255 Color: 9

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 6
Size: 315 Color: 3
Size: 255 Color: 19

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 18
Size: 272 Color: 15
Size: 259 Color: 4

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 8
Size: 316 Color: 8
Size: 311 Color: 17

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 2
Size: 354 Color: 0
Size: 276 Color: 18

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 4
Size: 263 Color: 19
Size: 263 Color: 15

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 6
Size: 323 Color: 6
Size: 272 Color: 3

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 19
Size: 292 Color: 18
Size: 280 Color: 18

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 5
Size: 335 Color: 14
Size: 312 Color: 12

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 291 Color: 3
Size: 263 Color: 14

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 11
Size: 352 Color: 16
Size: 264 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 12
Size: 290 Color: 6
Size: 272 Color: 9

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 18
Size: 289 Color: 11
Size: 283 Color: 12

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 7
Size: 268 Color: 3
Size: 250 Color: 14

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 10
Size: 304 Color: 5
Size: 259 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 12
Size: 359 Color: 10
Size: 279 Color: 1

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 9
Size: 302 Color: 10
Size: 281 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 269 Color: 10
Size: 254 Color: 7

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 2
Size: 274 Color: 3
Size: 269 Color: 9

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 7
Size: 306 Color: 12
Size: 255 Color: 13

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 316 Color: 18
Size: 275 Color: 19

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 15
Size: 281 Color: 0
Size: 278 Color: 12

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 289 Color: 16
Size: 250 Color: 15

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 18
Size: 289 Color: 0
Size: 259 Color: 13

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 309 Color: 18
Size: 276 Color: 16

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 0
Size: 340 Color: 16
Size: 280 Color: 10

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 357 Color: 17
Size: 251 Color: 13

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 0
Size: 268 Color: 17
Size: 253 Color: 2

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 299 Color: 13
Size: 282 Color: 11

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 6
Size: 333 Color: 6
Size: 268 Color: 4

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 10
Size: 349 Color: 17
Size: 287 Color: 7

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 334 Color: 11
Size: 252 Color: 11

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 0
Size: 266 Color: 13
Size: 257 Color: 12

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 306 Color: 7
Size: 288 Color: 18

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 5
Size: 321 Color: 8
Size: 268 Color: 16

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 2
Size: 269 Color: 18
Size: 255 Color: 5

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 19
Size: 272 Color: 4
Size: 264 Color: 6

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 18
Size: 341 Color: 0
Size: 312 Color: 5

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 343 Color: 19
Size: 276 Color: 7

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 316 Color: 6
Size: 314 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 10
Size: 289 Color: 0
Size: 279 Color: 2

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 5
Size: 331 Color: 8
Size: 252 Color: 12

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 5
Size: 313 Color: 8
Size: 287 Color: 8

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 9
Size: 353 Color: 10
Size: 292 Color: 15

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 13
Size: 280 Color: 10
Size: 250 Color: 16

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 10
Size: 356 Color: 3
Size: 281 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 9
Size: 273 Color: 6
Size: 272 Color: 1

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 9
Size: 331 Color: 16
Size: 254 Color: 5

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 16
Size: 301 Color: 18
Size: 293 Color: 12

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 11
Size: 296 Color: 16
Size: 294 Color: 8

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 296 Color: 11
Size: 295 Color: 5

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 4
Size: 341 Color: 17
Size: 254 Color: 2

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 3
Size: 275 Color: 19
Size: 269 Color: 19

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 297 Color: 11
Size: 255 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 3
Size: 312 Color: 3
Size: 252 Color: 2

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 306 Color: 8
Size: 301 Color: 11

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 5
Size: 285 Color: 16
Size: 265 Color: 7

Bin 259: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 0
Size: 250 Color: 18
Size: 250 Color: 14
Size: 250 Color: 10

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 6
Size: 321 Color: 15
Size: 254 Color: 9

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 8
Size: 327 Color: 5
Size: 294 Color: 9

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 12
Size: 278 Color: 3
Size: 276 Color: 9

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 9
Size: 331 Color: 9
Size: 305 Color: 11

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 277 Color: 6
Size: 260 Color: 13

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 12
Size: 257 Color: 1
Size: 256 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 287 Color: 17
Size: 259 Color: 11

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 12
Size: 263 Color: 2
Size: 257 Color: 11

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 12
Size: 322 Color: 17
Size: 280 Color: 6

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 19
Size: 315 Color: 4
Size: 250 Color: 4

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 258 Color: 7
Size: 253 Color: 2

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 6
Size: 333 Color: 17
Size: 262 Color: 16

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 17
Size: 297 Color: 17
Size: 274 Color: 10

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 5
Size: 349 Color: 15
Size: 254 Color: 18

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 15
Size: 332 Color: 6
Size: 271 Color: 19

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 7
Size: 269 Color: 19
Size: 269 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 19
Size: 340 Color: 0
Size: 253 Color: 12

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 3
Size: 267 Color: 0
Size: 251 Color: 14

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 4
Size: 287 Color: 0
Size: 250 Color: 15

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 326 Color: 12
Size: 265 Color: 4

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 326 Color: 4
Size: 254 Color: 9

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8
Size: 303 Color: 4
Size: 276 Color: 11

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 11
Size: 309 Color: 1
Size: 263 Color: 2

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 5
Size: 318 Color: 8
Size: 255 Color: 9

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 17
Size: 332 Color: 4
Size: 252 Color: 7

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 13
Size: 317 Color: 2
Size: 250 Color: 13

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 15
Size: 286 Color: 12
Size: 263 Color: 11

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 276 Color: 19
Size: 270 Color: 6

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 13
Size: 380 Color: 7
Size: 250 Color: 8

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 11
Size: 288 Color: 8
Size: 251 Color: 10

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 312 Color: 19
Size: 274 Color: 9

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 3
Size: 250 Color: 8
Size: 250 Color: 7

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 12
Size: 298 Color: 14
Size: 272 Color: 17

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 8
Size: 256 Color: 8
Size: 253 Color: 4

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 18
Size: 318 Color: 16
Size: 255 Color: 19

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 11
Size: 297 Color: 19
Size: 288 Color: 7

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 13
Size: 256 Color: 1
Size: 254 Color: 7

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 326 Color: 9
Size: 270 Color: 5

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 273 Color: 0
Size: 252 Color: 5

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 12
Size: 281 Color: 15
Size: 262 Color: 3

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 331 Color: 7
Size: 282 Color: 18

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 302 Color: 8
Size: 263 Color: 8

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 17
Size: 351 Color: 9
Size: 294 Color: 11

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 14
Size: 366 Color: 15
Size: 253 Color: 17

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 18
Size: 326 Color: 8
Size: 250 Color: 13

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 10
Size: 261 Color: 0
Size: 257 Color: 13

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 326 Color: 8
Size: 256 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 7
Size: 289 Color: 11
Size: 251 Color: 17

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 9
Size: 340 Color: 9
Size: 272 Color: 4

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 2
Size: 322 Color: 16
Size: 263 Color: 12

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 275 Color: 13
Size: 266 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 13
Size: 328 Color: 9
Size: 279 Color: 19

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 18
Size: 272 Color: 2
Size: 265 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 19
Size: 347 Color: 6
Size: 282 Color: 14

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 11
Size: 359 Color: 9
Size: 255 Color: 12

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 12
Size: 264 Color: 9
Size: 262 Color: 16

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 6
Size: 262 Color: 0
Size: 257 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 5
Size: 303 Color: 8
Size: 287 Color: 18

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 6
Size: 290 Color: 4
Size: 268 Color: 10

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 6
Size: 311 Color: 16
Size: 253 Color: 8

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 7
Size: 350 Color: 8
Size: 285 Color: 18

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 15
Size: 356 Color: 9
Size: 258 Color: 18

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 10
Size: 291 Color: 12
Size: 252 Color: 10

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 3
Size: 315 Color: 11
Size: 258 Color: 6

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 11
Size: 300 Color: 16
Size: 255 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 19
Size: 312 Color: 3
Size: 303 Color: 15

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 286 Color: 10
Size: 256 Color: 15

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 19
Size: 295 Color: 19
Size: 276 Color: 10

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 15
Size: 280 Color: 9
Size: 256 Color: 15

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 298 Color: 5
Size: 277 Color: 4

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 12
Size: 319 Color: 8
Size: 289 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 279 Color: 9
Size: 252 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 304 Color: 11
Size: 293 Color: 3

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 6
Size: 318 Color: 16
Size: 278 Color: 12

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 304 Color: 12
Size: 278 Color: 18

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 19
Size: 317 Color: 9
Size: 254 Color: 18

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 7
Size: 306 Color: 14
Size: 283 Color: 17

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 311 Color: 10
Size: 286 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 13
Size: 344 Color: 15
Size: 289 Color: 8

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 10
Size: 336 Color: 11
Size: 317 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 15
Size: 258 Color: 4
Size: 254 Color: 19

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 317 Color: 10
Size: 258 Color: 8

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 350 Color: 19
Size: 263 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 16
Size: 326 Color: 1
Size: 274 Color: 18

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 12
Size: 322 Color: 19
Size: 260 Color: 14

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7
Size: 308 Color: 15
Size: 296 Color: 6

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 314 Color: 15
Size: 264 Color: 18

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 5
Size: 319 Color: 13
Size: 294 Color: 10

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 11
Size: 316 Color: 16
Size: 274 Color: 19

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 310 Color: 2
Size: 303 Color: 13

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 7
Size: 349 Color: 5
Size: 258 Color: 8

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 15
Size: 315 Color: 8
Size: 286 Color: 10

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 12
Size: 345 Color: 18
Size: 250 Color: 7

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 16
Size: 308 Color: 3
Size: 299 Color: 16

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 324 Color: 19
Size: 286 Color: 17

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 16
Size: 362 Color: 16
Size: 266 Color: 12

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 16
Size: 335 Color: 14
Size: 300 Color: 5

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 18
Size: 322 Color: 10
Size: 279 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 8
Size: 355 Color: 17
Size: 274 Color: 15

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 278 Color: 18
Size: 278 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 272 Color: 18
Size: 269 Color: 19

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 10
Size: 256 Color: 16
Size: 250 Color: 12

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 282 Color: 17
Size: 274 Color: 13

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 17
Size: 267 Color: 13
Size: 265 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 14
Size: 341 Color: 11
Size: 298 Color: 14

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 315 Color: 7
Size: 281 Color: 19

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 17
Size: 256 Color: 17
Size: 254 Color: 7

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 13
Size: 305 Color: 4
Size: 262 Color: 18

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 8
Size: 335 Color: 11
Size: 281 Color: 16

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 336 Color: 16
Size: 334 Color: 0
Size: 331 Color: 10

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 307 Color: 8
Size: 297 Color: 19

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 10
Size: 289 Color: 14
Size: 280 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 18
Size: 308 Color: 5
Size: 299 Color: 8

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 353 Color: 10
Size: 295 Color: 5

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 3
Size: 271 Color: 8
Size: 270 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 10
Size: 337 Color: 14
Size: 265 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 10
Size: 293 Color: 8
Size: 273 Color: 14

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 7
Size: 296 Color: 2
Size: 288 Color: 19

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 12
Size: 302 Color: 12
Size: 274 Color: 16

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 265 Color: 16
Size: 261 Color: 14

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 14
Size: 255 Color: 16
Size: 255 Color: 8

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 16
Size: 308 Color: 18
Size: 275 Color: 12

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 346 Color: 8
Size: 262 Color: 11

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 14
Size: 358 Color: 18
Size: 273 Color: 18

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 12
Size: 329 Color: 15
Size: 300 Color: 5

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 7
Size: 346 Color: 3
Size: 255 Color: 4

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 267 Color: 18
Size: 258 Color: 14

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 3
Size: 264 Color: 12
Size: 252 Color: 10

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 9
Size: 345 Color: 1
Size: 251 Color: 5

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 11
Size: 252 Color: 11
Size: 251 Color: 15

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 14
Size: 262 Color: 10
Size: 253 Color: 8

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 4
Size: 313 Color: 4
Size: 256 Color: 5

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 14
Size: 344 Color: 7
Size: 306 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 12
Size: 337 Color: 19
Size: 314 Color: 19

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 17
Size: 344 Color: 9
Size: 304 Color: 13

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 12
Size: 322 Color: 2
Size: 289 Color: 3

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 12
Size: 318 Color: 11
Size: 261 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 11
Size: 253 Color: 12
Size: 253 Color: 9

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 3
Size: 280 Color: 13
Size: 265 Color: 4

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 276 Color: 1
Size: 269 Color: 11

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 7
Size: 328 Color: 2
Size: 321 Color: 3

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 17
Size: 361 Color: 2
Size: 265 Color: 2

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 14
Size: 316 Color: 12
Size: 314 Color: 17

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 364 Color: 19
Size: 254 Color: 13

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 297 Color: 10
Size: 256 Color: 18

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 307 Color: 13
Size: 254 Color: 2

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 17
Size: 330 Color: 5
Size: 264 Color: 6

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 11
Size: 343 Color: 10
Size: 253 Color: 18

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 11
Size: 323 Color: 7
Size: 254 Color: 1

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 8
Size: 347 Color: 6
Size: 270 Color: 16

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 8
Size: 355 Color: 15
Size: 285 Color: 10

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 2
Size: 326 Color: 8
Size: 258 Color: 2

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 14
Size: 313 Color: 17
Size: 310 Color: 12

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 11
Size: 363 Color: 5
Size: 254 Color: 17

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 12
Size: 341 Color: 4
Size: 286 Color: 4

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 11
Size: 326 Color: 19
Size: 304 Color: 4

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 17
Size: 345 Color: 14
Size: 265 Color: 5

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 4
Size: 252 Color: 15
Size: 251 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 5
Size: 347 Color: 14
Size: 292 Color: 13

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 18
Size: 304 Color: 4
Size: 270 Color: 5

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 8
Size: 353 Color: 2
Size: 261 Color: 17

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 16
Size: 282 Color: 2
Size: 267 Color: 16

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 13
Size: 325 Color: 13
Size: 275 Color: 2

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 7
Size: 341 Color: 18
Size: 270 Color: 12

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 19
Size: 349 Color: 15
Size: 286 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 13
Size: 337 Color: 19
Size: 281 Color: 11

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 13
Size: 343 Color: 1
Size: 270 Color: 16

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 3
Size: 365 Color: 0
Size: 260 Color: 2

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 320 Color: 1
Size: 288 Color: 6

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 346 Color: 17
Size: 344 Color: 6
Size: 311 Color: 16

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 13
Size: 367 Color: 9
Size: 254 Color: 7

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 12
Size: 321 Color: 5
Size: 270 Color: 15

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 14
Size: 270 Color: 18
Size: 266 Color: 19

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 314 Color: 15
Size: 309 Color: 5

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 5
Size: 337 Color: 15
Size: 299 Color: 5

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 14
Size: 287 Color: 11
Size: 264 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 12
Size: 290 Color: 16
Size: 257 Color: 17

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 328 Color: 15
Size: 256 Color: 19

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 5
Size: 336 Color: 18
Size: 322 Color: 16

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 2
Size: 318 Color: 7
Size: 316 Color: 15

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 263 Color: 11
Size: 250 Color: 3

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 13
Size: 336 Color: 2
Size: 320 Color: 10

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 9
Size: 365 Color: 12
Size: 257 Color: 3

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 332 Color: 10
Size: 290 Color: 8

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 9
Size: 281 Color: 11
Size: 265 Color: 18

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 8
Size: 268 Color: 6
Size: 257 Color: 9

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 9
Size: 335 Color: 10
Size: 277 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 336 Color: 15
Size: 255 Color: 4

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 14
Size: 306 Color: 14
Size: 299 Color: 4

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 18
Size: 331 Color: 7
Size: 320 Color: 11

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 13
Size: 325 Color: 14
Size: 313 Color: 8

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 16
Size: 290 Color: 18
Size: 275 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 7
Size: 318 Color: 0
Size: 314 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 15
Size: 261 Color: 1
Size: 254 Color: 18

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 299 Color: 10
Size: 284 Color: 18

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 5
Size: 322 Color: 10
Size: 289 Color: 15

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 14
Size: 307 Color: 11
Size: 281 Color: 12

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 15
Size: 266 Color: 3
Size: 262 Color: 4

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 6
Size: 294 Color: 5
Size: 285 Color: 6

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 327 Color: 15
Size: 304 Color: 17

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 17
Size: 283 Color: 18
Size: 281 Color: 19

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 15
Size: 313 Color: 11
Size: 286 Color: 6

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 8
Size: 347 Color: 0
Size: 295 Color: 15

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 6
Size: 268 Color: 9
Size: 260 Color: 9

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 10
Size: 337 Color: 12
Size: 300 Color: 3

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 14
Size: 287 Color: 14
Size: 273 Color: 12

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 11
Size: 319 Color: 1
Size: 296 Color: 7

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 14
Size: 271 Color: 19
Size: 255 Color: 2

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 13
Size: 344 Color: 7
Size: 285 Color: 5

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 257 Color: 17
Size: 256 Color: 6

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 19
Size: 326 Color: 19
Size: 267 Color: 12

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 284 Color: 11
Size: 259 Color: 12

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 324 Color: 12
Size: 293 Color: 11

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 11
Size: 363 Color: 18
Size: 273 Color: 5

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 316 Color: 16
Size: 299 Color: 4

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 0
Size: 356 Color: 6
Size: 276 Color: 1

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 10
Size: 252 Color: 17
Size: 251 Color: 17

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 5
Size: 334 Color: 18
Size: 325 Color: 18

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 269 Color: 14
Size: 267 Color: 2

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 290 Color: 8
Size: 280 Color: 19

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 6
Size: 361 Color: 17
Size: 265 Color: 13

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 0
Size: 318 Color: 8
Size: 295 Color: 5

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 301 Color: 19
Size: 250 Color: 13

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 334 Color: 18
Size: 278 Color: 8

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 8
Size: 338 Color: 7
Size: 318 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 339 Color: 1
Size: 272 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 10
Size: 261 Color: 1
Size: 254 Color: 4

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 18
Size: 347 Color: 13
Size: 284 Color: 4

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 4
Size: 317 Color: 0
Size: 271 Color: 9

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 253 Color: 9
Size: 252 Color: 2

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 2
Size: 297 Color: 10
Size: 295 Color: 3

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 14
Size: 363 Color: 3
Size: 272 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 18
Size: 369 Color: 6
Size: 250 Color: 8

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 12
Size: 352 Color: 10
Size: 264 Color: 11

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 18
Size: 262 Color: 6
Size: 250 Color: 2

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 8
Size: 253 Color: 4
Size: 252 Color: 10

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 15
Size: 342 Color: 11
Size: 269 Color: 19

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 6
Size: 322 Color: 0
Size: 319 Color: 10

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 19
Size: 346 Color: 14
Size: 254 Color: 4

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 16
Size: 357 Color: 17
Size: 256 Color: 8

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 264 Color: 10
Size: 252 Color: 16

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 286 Color: 6
Size: 255 Color: 13

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 311 Color: 16
Size: 250 Color: 18

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 286 Color: 18
Size: 268 Color: 2

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9
Size: 261 Color: 6
Size: 252 Color: 9

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7
Size: 310 Color: 1
Size: 302 Color: 7

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 7
Size: 327 Color: 18
Size: 263 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 302 Color: 17
Size: 263 Color: 15

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 17
Size: 290 Color: 8
Size: 275 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 5
Size: 321 Color: 4
Size: 276 Color: 17

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 9
Size: 299 Color: 16
Size: 296 Color: 6

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 11
Size: 256 Color: 6
Size: 252 Color: 19

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 4
Size: 345 Color: 1
Size: 307 Color: 10

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 322 Color: 12
Size: 251 Color: 6

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 5
Size: 336 Color: 8
Size: 263 Color: 1

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 18
Size: 276 Color: 3
Size: 254 Color: 15

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8
Size: 289 Color: 18
Size: 276 Color: 18

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 12
Size: 278 Color: 4
Size: 259 Color: 13

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 19
Size: 303 Color: 1
Size: 268 Color: 19

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 9
Size: 319 Color: 14
Size: 319 Color: 11

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 7
Size: 287 Color: 16
Size: 263 Color: 3

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 13
Size: 343 Color: 13
Size: 250 Color: 1

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 324 Color: 6
Size: 259 Color: 11

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 19
Size: 359 Color: 11
Size: 256 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 343 Color: 4
Size: 256 Color: 12

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 3
Size: 310 Color: 19
Size: 298 Color: 18

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 19
Size: 300 Color: 7
Size: 274 Color: 14

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 12
Size: 318 Color: 18
Size: 270 Color: 17

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 16
Size: 335 Color: 1
Size: 308 Color: 14

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 352 Color: 2
Size: 258 Color: 8

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 13
Size: 348 Color: 4
Size: 301 Color: 19

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 0
Size: 338 Color: 9
Size: 312 Color: 17

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 11
Size: 356 Color: 11
Size: 272 Color: 16

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 10
Size: 304 Color: 11
Size: 282 Color: 1

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 8
Size: 316 Color: 18
Size: 253 Color: 11

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 346 Color: 17
Size: 303 Color: 5

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 332 Color: 8
Size: 301 Color: 12

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 2
Size: 266 Color: 0
Size: 260 Color: 6

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 18
Size: 349 Color: 7
Size: 269 Color: 7

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 14
Size: 282 Color: 7
Size: 250 Color: 6

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 13
Size: 313 Color: 6
Size: 272 Color: 15

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 15
Size: 345 Color: 11
Size: 261 Color: 14

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 323 Color: 5
Size: 286 Color: 10

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 7
Size: 282 Color: 15
Size: 259 Color: 9

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 368 Color: 11
Size: 256 Color: 6

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 10
Size: 298 Color: 2
Size: 270 Color: 5

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 17
Size: 307 Color: 1
Size: 269 Color: 7

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 345 Color: 10
Size: 292 Color: 12

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 4
Size: 288 Color: 3
Size: 278 Color: 18

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 18
Size: 291 Color: 2
Size: 255 Color: 12

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 19
Size: 303 Color: 1
Size: 302 Color: 15

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 8
Size: 311 Color: 11
Size: 278 Color: 17

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 4
Size: 302 Color: 14
Size: 281 Color: 1

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 19
Size: 281 Color: 17
Size: 274 Color: 19

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 13
Size: 302 Color: 0
Size: 279 Color: 4

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 13
Size: 314 Color: 17
Size: 293 Color: 11

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 309 Color: 19
Size: 293 Color: 4

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 6
Size: 349 Color: 14
Size: 298 Color: 5

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 8
Size: 321 Color: 10
Size: 294 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 8
Size: 290 Color: 11
Size: 259 Color: 14

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 351 Color: 13
Size: 298 Color: 17

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 6
Size: 262 Color: 18
Size: 261 Color: 12

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 8
Size: 372 Color: 13
Size: 251 Color: 16

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 357 Color: 3
Size: 257 Color: 16

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 2
Size: 272 Color: 0
Size: 258 Color: 18

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 19
Size: 329 Color: 14
Size: 310 Color: 10

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 11
Size: 270 Color: 5
Size: 257 Color: 8

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 12
Size: 322 Color: 1
Size: 311 Color: 10

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 274 Color: 9
Size: 273 Color: 10

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 289 Color: 13
Size: 285 Color: 7

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 5
Size: 286 Color: 5
Size: 256 Color: 13

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 11
Size: 304 Color: 9
Size: 255 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 0
Size: 327 Color: 8
Size: 311 Color: 19

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 15
Size: 318 Color: 3
Size: 282 Color: 2

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 11
Size: 267 Color: 9
Size: 257 Color: 19

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 18
Size: 325 Color: 6
Size: 270 Color: 4

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 316 Color: 4
Size: 310 Color: 4

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 9
Size: 332 Color: 15
Size: 275 Color: 6

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 14
Size: 303 Color: 10
Size: 289 Color: 12

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 318 Color: 7
Size: 306 Color: 2

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 335 Color: 18
Size: 335 Color: 7
Size: 331 Color: 18

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 7
Size: 268 Color: 19
Size: 262 Color: 5

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 10
Size: 323 Color: 6
Size: 290 Color: 15

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 5
Size: 289 Color: 3
Size: 281 Color: 16

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 2
Size: 277 Color: 3
Size: 273 Color: 3

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 13
Size: 341 Color: 2
Size: 268 Color: 12

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 333 Color: 0
Size: 278 Color: 13

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 15
Size: 327 Color: 2
Size: 296 Color: 18

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 275 Color: 8
Size: 257 Color: 6

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 17
Size: 276 Color: 13
Size: 258 Color: 9

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 11
Size: 286 Color: 17
Size: 271 Color: 12

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 11
Size: 336 Color: 2
Size: 260 Color: 16

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 12
Size: 321 Color: 11
Size: 283 Color: 18

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 16
Size: 310 Color: 14
Size: 276 Color: 9

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 14
Size: 321 Color: 19
Size: 301 Color: 10

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 7
Size: 296 Color: 14
Size: 277 Color: 16

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 321 Color: 12
Size: 272 Color: 15

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 281 Color: 2
Size: 261 Color: 19

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 6
Size: 289 Color: 14
Size: 286 Color: 1

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 11
Size: 292 Color: 18
Size: 252 Color: 13

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 13
Size: 326 Color: 16
Size: 299 Color: 9

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 17
Size: 337 Color: 13
Size: 306 Color: 2

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 16
Size: 341 Color: 14
Size: 256 Color: 16

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 283 Color: 16
Size: 269 Color: 9

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 332 Color: 9
Size: 317 Color: 11

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 6
Size: 315 Color: 8
Size: 267 Color: 11

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 14
Size: 310 Color: 4
Size: 261 Color: 18

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 14
Size: 337 Color: 13
Size: 301 Color: 11

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 333 Color: 2
Size: 264 Color: 11

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 10
Size: 326 Color: 9
Size: 312 Color: 7

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 9
Size: 327 Color: 6
Size: 318 Color: 2

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 14
Size: 334 Color: 1
Size: 298 Color: 14

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 6
Size: 321 Color: 15
Size: 295 Color: 4

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 15
Size: 368 Color: 17
Size: 254 Color: 16

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 5
Size: 259 Color: 17
Size: 259 Color: 10

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 17
Size: 301 Color: 5
Size: 281 Color: 3

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 348 Color: 11
Size: 266 Color: 3

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 8
Size: 271 Color: 11
Size: 269 Color: 5

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 4
Size: 270 Color: 3
Size: 256 Color: 9

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 301 Color: 7
Size: 268 Color: 17
Size: 432 Color: 12

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 319 Color: 16
Size: 305 Color: 4

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 13
Size: 260 Color: 11
Size: 254 Color: 2

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 4
Size: 330 Color: 11
Size: 278 Color: 9

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 13
Size: 329 Color: 7
Size: 283 Color: 5

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 12
Size: 330 Color: 4
Size: 308 Color: 12

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 368 Color: 19
Size: 263 Color: 11

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 8
Size: 282 Color: 7
Size: 254 Color: 3

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 2
Size: 261 Color: 15
Size: 259 Color: 14

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 14
Size: 344 Color: 1
Size: 310 Color: 6

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 343 Color: 14
Size: 260 Color: 16

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 17
Size: 320 Color: 14
Size: 285 Color: 11

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 16
Size: 317 Color: 18
Size: 289 Color: 12

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 9
Size: 357 Color: 17
Size: 286 Color: 18

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 16
Size: 272 Color: 6
Size: 253 Color: 16

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 322 Color: 14
Size: 253 Color: 16

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 10
Size: 295 Color: 8
Size: 269 Color: 17

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 356 Color: 6
Size: 257 Color: 8

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 13
Size: 328 Color: 14
Size: 284 Color: 15

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 11
Size: 328 Color: 11
Size: 265 Color: 4

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 12
Size: 302 Color: 11
Size: 281 Color: 6

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 5
Size: 327 Color: 12
Size: 252 Color: 12

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 7
Size: 364 Color: 3
Size: 266 Color: 9

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 15
Size: 287 Color: 19
Size: 276 Color: 5

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 19
Size: 281 Color: 5
Size: 262 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 5
Size: 325 Color: 8
Size: 295 Color: 16

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 7
Size: 272 Color: 8
Size: 265 Color: 16

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 9
Size: 313 Color: 19
Size: 312 Color: 2

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 3
Size: 320 Color: 5
Size: 296 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 366 Color: 4
Size: 268 Color: 13

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 12
Size: 317 Color: 9
Size: 250 Color: 6

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 12
Size: 336 Color: 2
Size: 259 Color: 5

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 7
Size: 324 Color: 14
Size: 253 Color: 5

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 15
Size: 343 Color: 7
Size: 276 Color: 8

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 342 Color: 13
Size: 292 Color: 10

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 9
Size: 317 Color: 8
Size: 288 Color: 8

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 335 Color: 3
Size: 252 Color: 3

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 3
Size: 284 Color: 15
Size: 283 Color: 15

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 341 Color: 7
Size: 286 Color: 11

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 18
Size: 359 Color: 13
Size: 265 Color: 9

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 310 Color: 15
Size: 284 Color: 15

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 17
Size: 309 Color: 10
Size: 270 Color: 6

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 311 Color: 3
Size: 292 Color: 3

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 17
Size: 326 Color: 12
Size: 304 Color: 12

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 6
Size: 334 Color: 19
Size: 306 Color: 16

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 324 Color: 19
Size: 273 Color: 10

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 18
Size: 318 Color: 15
Size: 273 Color: 9

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 366 Color: 19
Size: 250 Color: 17

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 10
Size: 307 Color: 10
Size: 303 Color: 5

Total size: 667667
Total free space: 0

