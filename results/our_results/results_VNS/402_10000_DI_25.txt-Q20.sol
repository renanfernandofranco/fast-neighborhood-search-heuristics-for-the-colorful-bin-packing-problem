Capicity Bin: 8120
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6362 Color: 2
Size: 1606 Color: 1
Size: 152 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 17
Size: 1364 Color: 3
Size: 274 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 16
Size: 1544 Color: 6
Size: 92 Color: 6

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 15
Size: 1367 Color: 10
Size: 288 Color: 12

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 13
Size: 1476 Color: 1
Size: 140 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 7
Size: 1022 Color: 5
Size: 436 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 1
Size: 988 Color: 19
Size: 370 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 16
Size: 1134 Color: 7
Size: 206 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6897 Color: 6
Size: 1021 Color: 9
Size: 202 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 8
Size: 874 Color: 18
Size: 274 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6975 Color: 7
Size: 1001 Color: 14
Size: 144 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 1
Size: 891 Color: 4
Size: 188 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 4
Size: 724 Color: 6
Size: 352 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7074 Color: 7
Size: 884 Color: 9
Size: 162 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 0
Size: 758 Color: 8
Size: 272 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7145 Color: 17
Size: 691 Color: 18
Size: 284 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7148 Color: 7
Size: 796 Color: 15
Size: 176 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7169 Color: 1
Size: 747 Color: 11
Size: 204 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 12
Size: 802 Color: 2
Size: 128 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7203 Color: 12
Size: 725 Color: 18
Size: 192 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 2
Size: 640 Color: 1
Size: 276 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7243 Color: 6
Size: 731 Color: 7
Size: 146 Color: 12

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 19
Size: 672 Color: 11
Size: 204 Color: 5

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 9
Size: 706 Color: 5
Size: 160 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7265 Color: 6
Size: 713 Color: 12
Size: 142 Color: 5

Bin 26: 1 of cap free
Amount of items: 5
Items: 
Size: 4063 Color: 9
Size: 2194 Color: 9
Size: 1292 Color: 18
Size: 344 Color: 15
Size: 226 Color: 17

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5463 Color: 1
Size: 2518 Color: 11
Size: 138 Color: 11

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 10
Size: 2174 Color: 16
Size: 158 Color: 17

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6271 Color: 5
Size: 1692 Color: 16
Size: 156 Color: 8

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 7
Size: 1129 Color: 0
Size: 580 Color: 15

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 12
Size: 1547 Color: 4

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 2
Size: 1490 Color: 14

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 6738 Color: 8
Size: 1381 Color: 5

Bin 34: 1 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 10
Size: 1373 Color: 19

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6755 Color: 18
Size: 692 Color: 5
Size: 672 Color: 19

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 16
Size: 1120 Color: 1
Size: 132 Color: 11

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 7049 Color: 10
Size: 764 Color: 13
Size: 306 Color: 10

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 7053 Color: 4
Size: 682 Color: 1
Size: 384 Color: 16

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 7103 Color: 9
Size: 576 Color: 15
Size: 440 Color: 1

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 7164 Color: 8
Size: 955 Color: 13

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 13
Size: 945 Color: 18

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 1
Size: 765 Color: 8
Size: 80 Color: 14

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 7291 Color: 17
Size: 496 Color: 6
Size: 332 Color: 13

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 7306 Color: 7
Size: 813 Color: 17

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 7
Size: 2364 Color: 0
Size: 240 Color: 2

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 15
Size: 2186 Color: 15
Size: 128 Color: 1

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 18
Size: 1882 Color: 15
Size: 248 Color: 8

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 6
Size: 1525 Color: 5
Size: 328 Color: 17

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 12
Size: 1394 Color: 2
Size: 368 Color: 15

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6442 Color: 2
Size: 1676 Color: 6

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6987 Color: 1
Size: 1131 Color: 8

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 14
Size: 1124 Color: 13

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 7225 Color: 13
Size: 893 Color: 6

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 5908 Color: 6
Size: 2209 Color: 15

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 6051 Color: 11
Size: 1918 Color: 16
Size: 148 Color: 14

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 9
Size: 1402 Color: 7
Size: 432 Color: 4

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 5
Size: 1249 Color: 19
Size: 200 Color: 1

Bin 58: 3 of cap free
Amount of items: 2
Items: 
Size: 6874 Color: 8
Size: 1243 Color: 14

Bin 59: 3 of cap free
Amount of items: 2
Items: 
Size: 7230 Color: 17
Size: 887 Color: 9

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 17
Size: 812 Color: 5

Bin 61: 4 of cap free
Amount of items: 7
Items: 
Size: 4061 Color: 1
Size: 862 Color: 1
Size: 793 Color: 16
Size: 790 Color: 18
Size: 742 Color: 18
Size: 584 Color: 15
Size: 284 Color: 4

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 7
Size: 3568 Color: 12
Size: 480 Color: 11

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 14
Size: 1474 Color: 7

Bin 64: 5 of cap free
Amount of items: 15
Items: 
Size: 722 Color: 11
Size: 681 Color: 17
Size: 676 Color: 18
Size: 676 Color: 11
Size: 676 Color: 9
Size: 592 Color: 17
Size: 584 Color: 13
Size: 560 Color: 3
Size: 512 Color: 10
Size: 504 Color: 9
Size: 500 Color: 10
Size: 472 Color: 16
Size: 448 Color: 13
Size: 376 Color: 9
Size: 136 Color: 0

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6402 Color: 3
Size: 1713 Color: 18

Bin 66: 5 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 2
Size: 1386 Color: 17
Size: 256 Color: 1

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 13
Size: 901 Color: 17

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 11
Size: 849 Color: 12
Size: 14 Color: 13

Bin 69: 6 of cap free
Amount of items: 4
Items: 
Size: 5220 Color: 12
Size: 2482 Color: 2
Size: 228 Color: 17
Size: 184 Color: 7

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 4
Size: 1780 Color: 15

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 11
Size: 1434 Color: 13

Bin 72: 6 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 16
Size: 782 Color: 9
Size: 520 Color: 2

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 6882 Color: 4
Size: 1232 Color: 18

Bin 74: 6 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 11
Size: 992 Color: 6
Size: 224 Color: 2

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 7292 Color: 4
Size: 822 Color: 16

Bin 76: 7 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 4
Size: 2677 Color: 9
Size: 152 Color: 0

Bin 77: 7 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 10
Size: 1045 Color: 14

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 5692 Color: 19
Size: 2420 Color: 17

Bin 79: 8 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 14
Size: 1894 Color: 13
Size: 128 Color: 7

Bin 80: 8 of cap free
Amount of items: 2
Items: 
Size: 7105 Color: 14
Size: 1007 Color: 13

Bin 81: 10 of cap free
Amount of items: 2
Items: 
Size: 5102 Color: 19
Size: 3008 Color: 4

Bin 82: 11 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 4
Size: 1531 Color: 10
Size: 224 Color: 7

Bin 83: 11 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 18
Size: 1270 Color: 2
Size: 80 Color: 17

Bin 84: 12 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 7
Size: 1139 Color: 3
Size: 96 Color: 13

Bin 85: 13 of cap free
Amount of items: 2
Items: 
Size: 7066 Color: 4
Size: 1041 Color: 9

Bin 86: 14 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 10
Size: 1366 Color: 9

Bin 87: 14 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 7
Size: 1092 Color: 17

Bin 88: 15 of cap free
Amount of items: 2
Items: 
Size: 7141 Color: 16
Size: 964 Color: 5

Bin 89: 16 of cap free
Amount of items: 2
Items: 
Size: 6450 Color: 19
Size: 1654 Color: 10

Bin 90: 16 of cap free
Amount of items: 2
Items: 
Size: 7182 Color: 16
Size: 922 Color: 3

Bin 91: 19 of cap free
Amount of items: 6
Items: 
Size: 4062 Color: 8
Size: 1033 Color: 5
Size: 900 Color: 18
Size: 882 Color: 3
Size: 880 Color: 2
Size: 344 Color: 11

Bin 92: 21 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 17
Size: 1218 Color: 8

Bin 93: 22 of cap free
Amount of items: 2
Items: 
Size: 7064 Color: 12
Size: 1034 Color: 15

Bin 94: 24 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 3
Size: 3380 Color: 16
Size: 176 Color: 13

Bin 95: 24 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 16
Size: 1156 Color: 14

Bin 96: 25 of cap free
Amount of items: 3
Items: 
Size: 7251 Color: 10
Size: 804 Color: 0
Size: 40 Color: 5

Bin 97: 30 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 14
Size: 3378 Color: 7
Size: 388 Color: 18

Bin 98: 30 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 8
Size: 1000 Color: 4
Size: 632 Color: 13

Bin 99: 30 of cap free
Amount of items: 2
Items: 
Size: 6936 Color: 10
Size: 1154 Color: 11

Bin 100: 32 of cap free
Amount of items: 2
Items: 
Size: 5476 Color: 18
Size: 2612 Color: 1

Bin 101: 32 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 12
Size: 1844 Color: 3

Bin 102: 34 of cap free
Amount of items: 2
Items: 
Size: 4070 Color: 14
Size: 4016 Color: 13

Bin 103: 37 of cap free
Amount of items: 2
Items: 
Size: 6767 Color: 18
Size: 1316 Color: 11

Bin 104: 38 of cap free
Amount of items: 3
Items: 
Size: 5498 Color: 2
Size: 2204 Color: 1
Size: 380 Color: 4

Bin 105: 39 of cap free
Amount of items: 2
Items: 
Size: 5093 Color: 11
Size: 2988 Color: 15

Bin 106: 44 of cap free
Amount of items: 2
Items: 
Size: 7134 Color: 8
Size: 942 Color: 2

Bin 107: 45 of cap free
Amount of items: 2
Items: 
Size: 6763 Color: 4
Size: 1312 Color: 2

Bin 108: 46 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 16
Size: 2844 Color: 0
Size: 580 Color: 18

Bin 109: 47 of cap free
Amount of items: 2
Items: 
Size: 6045 Color: 16
Size: 2028 Color: 8

Bin 110: 48 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 1
Size: 2914 Color: 0
Size: 442 Color: 9

Bin 111: 50 of cap free
Amount of items: 2
Items: 
Size: 6138 Color: 13
Size: 1932 Color: 14

Bin 112: 53 of cap free
Amount of items: 2
Items: 
Size: 6116 Color: 14
Size: 1951 Color: 13

Bin 113: 54 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 0
Size: 2894 Color: 15
Size: 184 Color: 7

Bin 114: 55 of cap free
Amount of items: 2
Items: 
Size: 5142 Color: 4
Size: 2923 Color: 13

Bin 115: 61 of cap free
Amount of items: 2
Items: 
Size: 6114 Color: 1
Size: 1945 Color: 13

Bin 116: 61 of cap free
Amount of items: 2
Items: 
Size: 6913 Color: 10
Size: 1146 Color: 5

Bin 117: 63 of cap free
Amount of items: 2
Items: 
Size: 6332 Color: 1
Size: 1725 Color: 4

Bin 118: 67 of cap free
Amount of items: 3
Items: 
Size: 4613 Color: 19
Size: 3164 Color: 7
Size: 276 Color: 3

Bin 119: 71 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 11
Size: 1426 Color: 2

Bin 120: 75 of cap free
Amount of items: 2
Items: 
Size: 6481 Color: 13
Size: 1564 Color: 17

Bin 121: 81 of cap free
Amount of items: 3
Items: 
Size: 5862 Color: 2
Size: 1135 Color: 9
Size: 1042 Color: 1

Bin 122: 97 of cap free
Amount of items: 2
Items: 
Size: 5490 Color: 9
Size: 2533 Color: 18

Bin 123: 98 of cap free
Amount of items: 2
Items: 
Size: 6291 Color: 17
Size: 1731 Color: 15

Bin 124: 103 of cap free
Amount of items: 2
Items: 
Size: 4634 Color: 8
Size: 3383 Color: 2

Bin 125: 113 of cap free
Amount of items: 2
Items: 
Size: 4626 Color: 14
Size: 3381 Color: 9

Bin 126: 118 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 19
Size: 1674 Color: 15
Size: 506 Color: 7

Bin 127: 122 of cap free
Amount of items: 30
Items: 
Size: 440 Color: 1
Size: 436 Color: 6
Size: 400 Color: 1
Size: 388 Color: 18
Size: 384 Color: 3
Size: 328 Color: 4
Size: 312 Color: 19
Size: 308 Color: 16
Size: 304 Color: 4
Size: 296 Color: 12
Size: 292 Color: 2
Size: 276 Color: 5
Size: 272 Color: 10
Size: 272 Color: 8
Size: 248 Color: 0
Size: 228 Color: 10
Size: 226 Color: 15
Size: 226 Color: 8
Size: 224 Color: 16
Size: 216 Color: 17
Size: 216 Color: 10
Size: 208 Color: 13
Size: 208 Color: 11
Size: 206 Color: 2
Size: 200 Color: 19
Size: 190 Color: 3
Size: 178 Color: 4
Size: 176 Color: 14
Size: 172 Color: 7
Size: 168 Color: 0

Bin 128: 123 of cap free
Amount of items: 2
Items: 
Size: 4615 Color: 7
Size: 3382 Color: 14

Bin 129: 124 of cap free
Amount of items: 2
Items: 
Size: 5781 Color: 3
Size: 2215 Color: 2

Bin 130: 126 of cap free
Amount of items: 2
Items: 
Size: 5073 Color: 19
Size: 2921 Color: 17

Bin 131: 126 of cap free
Amount of items: 2
Items: 
Size: 5471 Color: 8
Size: 2523 Color: 2

Bin 132: 133 of cap free
Amount of items: 2
Items: 
Size: 5081 Color: 0
Size: 2906 Color: 4

Bin 133: 5172 of cap free
Amount of items: 19
Items: 
Size: 192 Color: 9
Size: 184 Color: 3
Size: 178 Color: 17
Size: 172 Color: 4
Size: 168 Color: 7
Size: 168 Color: 1
Size: 164 Color: 14
Size: 156 Color: 6
Size: 152 Color: 7
Size: 152 Color: 6
Size: 148 Color: 15
Size: 148 Color: 0
Size: 144 Color: 16
Size: 144 Color: 12
Size: 136 Color: 17
Size: 136 Color: 13
Size: 136 Color: 13
Size: 136 Color: 8
Size: 134 Color: 14

Total size: 1071840
Total free space: 8120

