Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 385673 Color: 1
Size: 315935 Color: 0
Size: 298393 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 431250 Color: 1
Size: 285349 Color: 1
Size: 283402 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 489375 Color: 1
Size: 260360 Color: 0
Size: 250266 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 425049 Color: 1
Size: 304868 Color: 0
Size: 270084 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 399786 Color: 0
Size: 324086 Color: 1
Size: 276129 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 368498 Color: 1
Size: 365751 Color: 0
Size: 265752 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 372944 Color: 1
Size: 356551 Color: 1
Size: 270506 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 391620 Color: 0
Size: 333335 Color: 1
Size: 275046 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 377255 Color: 0
Size: 339626 Color: 1
Size: 283120 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 356903 Color: 0
Size: 345715 Color: 1
Size: 297383 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 376719 Color: 1
Size: 320080 Color: 0
Size: 303202 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 434366 Color: 0
Size: 293761 Color: 1
Size: 271874 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 468622 Color: 0
Size: 269104 Color: 1
Size: 262275 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 379334 Color: 0
Size: 357635 Color: 1
Size: 263032 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 338107 Color: 1
Size: 336363 Color: 0
Size: 325531 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 493956 Color: 1
Size: 253707 Color: 0
Size: 252338 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 385031 Color: 1
Size: 314073 Color: 1
Size: 300897 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 483763 Color: 1
Size: 263194 Color: 0
Size: 253044 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 467108 Color: 1
Size: 266892 Color: 0
Size: 266001 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 383434 Color: 0
Size: 315540 Color: 1
Size: 301027 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 365698 Color: 0
Size: 355609 Color: 1
Size: 278694 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 487618 Color: 1
Size: 256453 Color: 0
Size: 255930 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 404995 Color: 0
Size: 314763 Color: 0
Size: 280243 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 476299 Color: 0
Size: 269699 Color: 1
Size: 254003 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 353966 Color: 0
Size: 326324 Color: 1
Size: 319711 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 371112 Color: 0
Size: 321422 Color: 0
Size: 307467 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 404840 Color: 1
Size: 337273 Color: 0
Size: 257888 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 468610 Color: 1
Size: 276283 Color: 0
Size: 255108 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 389365 Color: 1
Size: 357595 Color: 0
Size: 253041 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 414162 Color: 1
Size: 319291 Color: 1
Size: 266548 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 479299 Color: 0
Size: 268301 Color: 1
Size: 252401 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 401169 Color: 0
Size: 332865 Color: 1
Size: 265967 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 375128 Color: 1
Size: 371361 Color: 1
Size: 253512 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 400489 Color: 1
Size: 304932 Color: 0
Size: 294580 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 378764 Color: 1
Size: 323233 Color: 0
Size: 298004 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 454192 Color: 0
Size: 282000 Color: 1
Size: 263809 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 391903 Color: 1
Size: 332378 Color: 0
Size: 275720 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 466577 Color: 1
Size: 280732 Color: 1
Size: 252692 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 368790 Color: 1
Size: 325860 Color: 0
Size: 305351 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 400150 Color: 1
Size: 345170 Color: 0
Size: 254681 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 476222 Color: 0
Size: 268382 Color: 1
Size: 255397 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 476474 Color: 0
Size: 271992 Color: 1
Size: 251535 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 418621 Color: 0
Size: 321567 Color: 1
Size: 259813 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 482983 Color: 0
Size: 265661 Color: 1
Size: 251357 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 385065 Color: 0
Size: 357428 Color: 1
Size: 257508 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 370471 Color: 1
Size: 350798 Color: 0
Size: 278732 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 467737 Color: 0
Size: 267797 Color: 0
Size: 264467 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 395263 Color: 1
Size: 322392 Color: 0
Size: 282346 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 468167 Color: 1
Size: 275059 Color: 0
Size: 256775 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 428884 Color: 1
Size: 294794 Color: 1
Size: 276323 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 412202 Color: 0
Size: 321489 Color: 1
Size: 266310 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 466212 Color: 0
Size: 269624 Color: 1
Size: 264165 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 388701 Color: 1
Size: 337906 Color: 0
Size: 273394 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 353201 Color: 1
Size: 336864 Color: 0
Size: 309936 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 385904 Color: 1
Size: 331264 Color: 0
Size: 282833 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 457327 Color: 1
Size: 284768 Color: 0
Size: 257906 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 429292 Color: 0
Size: 296736 Color: 1
Size: 273973 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 455107 Color: 1
Size: 287317 Color: 1
Size: 257577 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 350567 Color: 0
Size: 336313 Color: 0
Size: 313121 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 380731 Color: 0
Size: 342499 Color: 0
Size: 276771 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 353337 Color: 0
Size: 346560 Color: 1
Size: 300104 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 419945 Color: 1
Size: 324344 Color: 0
Size: 255712 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 415766 Color: 1
Size: 315992 Color: 0
Size: 268243 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 411801 Color: 0
Size: 330628 Color: 0
Size: 257572 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 377220 Color: 1
Size: 349198 Color: 0
Size: 273583 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 472572 Color: 0
Size: 271191 Color: 1
Size: 256238 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 470066 Color: 1
Size: 265369 Color: 0
Size: 264566 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 402908 Color: 1
Size: 342164 Color: 0
Size: 254929 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 364545 Color: 0
Size: 337856 Color: 1
Size: 297600 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 370127 Color: 1
Size: 364282 Color: 0
Size: 265592 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 432776 Color: 0
Size: 299103 Color: 0
Size: 268122 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 362851 Color: 0
Size: 351226 Color: 1
Size: 285924 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 470270 Color: 0
Size: 273299 Color: 1
Size: 256432 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 422027 Color: 0
Size: 317963 Color: 1
Size: 260011 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 421501 Color: 1
Size: 294026 Color: 0
Size: 284474 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 443159 Color: 0
Size: 302256 Color: 0
Size: 254586 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 398832 Color: 1
Size: 348536 Color: 0
Size: 252633 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 399245 Color: 1
Size: 309780 Color: 1
Size: 290976 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 378480 Color: 0
Size: 334049 Color: 1
Size: 287472 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 411177 Color: 0
Size: 297558 Color: 1
Size: 291266 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 368713 Color: 0
Size: 340979 Color: 1
Size: 290309 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 463863 Color: 1
Size: 280567 Color: 0
Size: 255571 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 350071 Color: 0
Size: 332129 Color: 0
Size: 317801 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 441728 Color: 0
Size: 283697 Color: 0
Size: 274576 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 342733 Color: 1
Size: 339675 Color: 0
Size: 317593 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 363482 Color: 1
Size: 328419 Color: 0
Size: 308100 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 368074 Color: 0
Size: 367021 Color: 1
Size: 264906 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 383897 Color: 0
Size: 329781 Color: 1
Size: 286323 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 413442 Color: 1
Size: 305454 Color: 1
Size: 281105 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 387238 Color: 1
Size: 321481 Color: 1
Size: 291282 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 482451 Color: 0
Size: 261976 Color: 1
Size: 255574 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 432670 Color: 0
Size: 294775 Color: 1
Size: 272556 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 385838 Color: 0
Size: 313039 Color: 1
Size: 301124 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 404380 Color: 0
Size: 300371 Color: 1
Size: 295250 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 355030 Color: 0
Size: 322951 Color: 0
Size: 322020 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 364221 Color: 0
Size: 347126 Color: 1
Size: 288654 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 406629 Color: 0
Size: 313208 Color: 1
Size: 280164 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 399364 Color: 1
Size: 337352 Color: 0
Size: 263285 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 454685 Color: 1
Size: 284291 Color: 0
Size: 261025 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 366065 Color: 1
Size: 343018 Color: 0
Size: 290918 Color: 1

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 399228 Color: 0
Size: 324057 Color: 1
Size: 276716 Color: 1

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 358181 Color: 1
Size: 327180 Color: 1
Size: 314640 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 457229 Color: 1
Size: 287696 Color: 0
Size: 255076 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 371097 Color: 1
Size: 369599 Color: 1
Size: 259305 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 393579 Color: 1
Size: 303759 Color: 0
Size: 302663 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 402141 Color: 0
Size: 309077 Color: 1
Size: 288783 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 385556 Color: 1
Size: 354141 Color: 0
Size: 260304 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 400935 Color: 1
Size: 299971 Color: 0
Size: 299095 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 477978 Color: 0
Size: 262287 Color: 1
Size: 259736 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 400687 Color: 1
Size: 308822 Color: 0
Size: 290492 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 352366 Color: 1
Size: 336408 Color: 0
Size: 311227 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 451731 Color: 0
Size: 289417 Color: 0
Size: 258853 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 428353 Color: 0
Size: 306429 Color: 1
Size: 265219 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 456499 Color: 1
Size: 288870 Color: 0
Size: 254632 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 412350 Color: 1
Size: 302903 Color: 0
Size: 284748 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 369513 Color: 0
Size: 368393 Color: 1
Size: 262095 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 353171 Color: 0
Size: 348925 Color: 1
Size: 297905 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 342812 Color: 0
Size: 333064 Color: 1
Size: 324125 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 397609 Color: 0
Size: 338851 Color: 1
Size: 263541 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 356303 Color: 1
Size: 348572 Color: 0
Size: 295126 Color: 1

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 387152 Color: 1
Size: 356311 Color: 1
Size: 256538 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 408765 Color: 1
Size: 341088 Color: 1
Size: 250148 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 427477 Color: 1
Size: 321446 Color: 0
Size: 251078 Color: 1

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 477487 Color: 1
Size: 263243 Color: 0
Size: 259271 Color: 1

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 448258 Color: 1
Size: 293705 Color: 0
Size: 258038 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 411070 Color: 1
Size: 322575 Color: 1
Size: 266356 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 408621 Color: 1
Size: 308647 Color: 1
Size: 282733 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 366404 Color: 0
Size: 327564 Color: 1
Size: 306033 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 447823 Color: 0
Size: 291931 Color: 0
Size: 260247 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 374516 Color: 0
Size: 347750 Color: 1
Size: 277735 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 430161 Color: 1
Size: 292248 Color: 0
Size: 277592 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 418465 Color: 1
Size: 329989 Color: 0
Size: 251547 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 410175 Color: 1
Size: 313779 Color: 0
Size: 276047 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 494522 Color: 0
Size: 253456 Color: 0
Size: 252023 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 497197 Color: 0
Size: 251849 Color: 1
Size: 250955 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 405227 Color: 1
Size: 333243 Color: 0
Size: 261531 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 429579 Color: 0
Size: 298044 Color: 1
Size: 272378 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 424611 Color: 1
Size: 298634 Color: 0
Size: 276756 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 450729 Color: 0
Size: 281364 Color: 1
Size: 267908 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 448908 Color: 0
Size: 286208 Color: 1
Size: 264885 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 482289 Color: 0
Size: 262293 Color: 1
Size: 255419 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 381765 Color: 1
Size: 312547 Color: 0
Size: 305689 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 399128 Color: 1
Size: 306318 Color: 0
Size: 294555 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 426377 Color: 0
Size: 307472 Color: 1
Size: 266152 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 482723 Color: 0
Size: 265495 Color: 1
Size: 251783 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 350964 Color: 0
Size: 346834 Color: 0
Size: 302203 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 427750 Color: 1
Size: 297709 Color: 0
Size: 274542 Color: 1

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 484212 Color: 1
Size: 260801 Color: 1
Size: 254988 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 405642 Color: 1
Size: 333968 Color: 0
Size: 260391 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 404150 Color: 1
Size: 303569 Color: 1
Size: 292282 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 427635 Color: 1
Size: 321151 Color: 0
Size: 251215 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 451149 Color: 0
Size: 297771 Color: 1
Size: 251081 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 437500 Color: 1
Size: 287407 Color: 0
Size: 275094 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 430715 Color: 0
Size: 307686 Color: 1
Size: 261600 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 407936 Color: 0
Size: 305170 Color: 1
Size: 286895 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 452480 Color: 0
Size: 281932 Color: 1
Size: 265589 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 447627 Color: 1
Size: 276788 Color: 0
Size: 275586 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 402718 Color: 1
Size: 344546 Color: 0
Size: 252737 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 467915 Color: 0
Size: 280106 Color: 1
Size: 251980 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 451737 Color: 1
Size: 288518 Color: 1
Size: 259746 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 410618 Color: 0
Size: 318310 Color: 1
Size: 271073 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 419930 Color: 0
Size: 325979 Color: 1
Size: 254092 Color: 1

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 355475 Color: 0
Size: 317397 Color: 1
Size: 327129 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 368363 Color: 1
Size: 333513 Color: 0
Size: 298125 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 488504 Color: 1
Size: 257223 Color: 0
Size: 254274 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 401441 Color: 1
Size: 318483 Color: 1
Size: 280077 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 445038 Color: 1
Size: 278644 Color: 0
Size: 276319 Color: 1

Total size: 167000167
Total free space: 0

