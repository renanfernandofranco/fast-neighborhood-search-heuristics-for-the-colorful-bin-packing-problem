Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 19
Size: 360 Color: 3
Size: 256 Color: 9

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 9
Size: 352 Color: 19
Size: 276 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 5
Size: 332 Color: 6
Size: 305 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 17
Size: 296 Color: 17
Size: 266 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 344 Color: 12
Size: 253 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 13
Size: 352 Color: 12
Size: 252 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 289 Color: 1
Size: 280 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 5
Size: 270 Color: 17
Size: 261 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 6
Size: 308 Color: 3
Size: 306 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 259 Color: 18
Size: 253 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 8
Size: 275 Color: 11
Size: 256 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 276 Color: 11
Size: 256 Color: 16

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 8
Size: 288 Color: 5
Size: 267 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 14
Size: 336 Color: 1
Size: 255 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 6
Size: 256 Color: 14
Size: 250 Color: 17

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 16
Size: 288 Color: 16
Size: 257 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 17
Size: 342 Color: 2
Size: 297 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 14
Size: 339 Color: 16
Size: 295 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 13
Size: 280 Color: 17
Size: 271 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 331 Color: 10
Size: 294 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 11
Size: 297 Color: 7
Size: 261 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 298 Color: 6
Size: 288 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 4
Size: 257 Color: 15
Size: 250 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 17
Size: 286 Color: 7
Size: 259 Color: 19

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 18
Size: 328 Color: 9
Size: 254 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 9
Size: 353 Color: 5
Size: 262 Color: 8

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 9
Size: 269 Color: 15
Size: 253 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 19
Size: 292 Color: 0
Size: 269 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 12
Size: 318 Color: 11
Size: 310 Color: 16

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 292 Color: 7
Size: 275 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 13
Size: 318 Color: 15
Size: 275 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 13
Size: 296 Color: 15
Size: 272 Color: 15

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 13
Size: 325 Color: 18
Size: 257 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 17
Size: 355 Color: 12
Size: 276 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 7
Size: 304 Color: 10
Size: 271 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 7
Size: 301 Color: 13
Size: 251 Color: 3

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 370 Color: 5
Size: 250 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 13
Size: 346 Color: 19
Size: 301 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 328 Color: 9
Size: 275 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 8
Size: 345 Color: 16
Size: 274 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 8
Size: 313 Color: 13
Size: 278 Color: 6

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 7
Size: 366 Color: 13
Size: 250 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 15
Size: 358 Color: 1
Size: 269 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 18
Size: 348 Color: 6
Size: 296 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 257 Color: 18
Size: 254 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 9
Size: 265 Color: 0
Size: 254 Color: 6

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 8
Size: 287 Color: 17
Size: 256 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 360 Color: 15
Size: 277 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 6
Size: 251 Color: 7
Size: 258 Color: 8

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 9
Size: 366 Color: 1
Size: 256 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 8
Size: 279 Color: 8
Size: 250 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 8
Size: 288 Color: 8
Size: 279 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 6
Size: 316 Color: 12
Size: 270 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 346 Color: 13
Size: 274 Color: 7

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 337 Color: 2
Size: 300 Color: 14

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 19
Size: 335 Color: 11
Size: 253 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 15
Size: 362 Color: 7
Size: 260 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 0
Size: 312 Color: 6
Size: 267 Color: 11

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 15
Size: 363 Color: 9
Size: 261 Color: 15

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 10
Size: 334 Color: 14
Size: 298 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 9
Size: 339 Color: 18
Size: 260 Color: 16

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 12
Size: 295 Color: 2
Size: 269 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 5
Size: 277 Color: 17
Size: 277 Color: 16

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 9
Size: 271 Color: 1
Size: 257 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 317 Color: 2
Size: 256 Color: 9

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 17
Size: 291 Color: 16
Size: 278 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 7
Size: 332 Color: 15
Size: 313 Color: 11

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 324 Color: 15
Size: 316 Color: 18

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 295 Color: 16
Size: 252 Color: 4

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 15
Size: 270 Color: 4
Size: 256 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 351 Color: 1
Size: 258 Color: 7

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 18
Size: 260 Color: 10
Size: 254 Color: 8

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 10
Size: 290 Color: 19
Size: 254 Color: 13

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 334 Color: 6
Size: 294 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 0
Size: 357 Color: 12
Size: 271 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 345 Color: 11
Size: 302 Color: 8

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 347 Color: 4
Size: 276 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 10
Size: 354 Color: 2
Size: 262 Color: 13

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 295 Color: 17
Size: 267 Color: 3

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 278 Color: 7
Size: 263 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 8
Size: 276 Color: 5
Size: 251 Color: 14

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 10
Size: 311 Color: 7
Size: 264 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 3
Size: 272 Color: 0
Size: 250 Color: 15

Total size: 83000
Total free space: 0

