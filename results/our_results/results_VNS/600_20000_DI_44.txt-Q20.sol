Capicity Bin: 15632
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 7828 Color: 18
Size: 4582 Color: 10
Size: 1896 Color: 1
Size: 894 Color: 7
Size: 432 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8818 Color: 9
Size: 5126 Color: 17
Size: 1688 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9516 Color: 9
Size: 4648 Color: 5
Size: 1468 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9532 Color: 19
Size: 5688 Color: 4
Size: 412 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9571 Color: 16
Size: 5051 Color: 19
Size: 1010 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 4
Size: 5020 Color: 0
Size: 1000 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 9631 Color: 19
Size: 5699 Color: 12
Size: 302 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9958 Color: 8
Size: 5084 Color: 5
Size: 590 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 6
Size: 5256 Color: 2
Size: 320 Color: 19

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10319 Color: 5
Size: 5001 Color: 6
Size: 312 Color: 14

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 3
Size: 3768 Color: 7
Size: 800 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 11
Size: 4076 Color: 17
Size: 464 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11259 Color: 19
Size: 3645 Color: 16
Size: 728 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11262 Color: 11
Size: 4014 Color: 7
Size: 356 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11364 Color: 12
Size: 3912 Color: 7
Size: 356 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11598 Color: 1
Size: 2802 Color: 1
Size: 1232 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11844 Color: 19
Size: 2352 Color: 13
Size: 1436 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11852 Color: 19
Size: 3156 Color: 6
Size: 624 Color: 10

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 16
Size: 3130 Color: 1
Size: 508 Color: 18

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12037 Color: 1
Size: 2997 Color: 15
Size: 598 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12274 Color: 7
Size: 3246 Color: 17
Size: 112 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 12
Size: 1606 Color: 10
Size: 1448 Color: 8

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12821 Color: 16
Size: 1811 Color: 1
Size: 1000 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 11
Size: 2360 Color: 14
Size: 320 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 1
Size: 1678 Color: 5
Size: 1016 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 17
Size: 1932 Color: 3
Size: 608 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 9
Size: 2070 Color: 1
Size: 460 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13160 Color: 7
Size: 1768 Color: 17
Size: 704 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13271 Color: 8
Size: 1969 Color: 5
Size: 392 Color: 6

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13339 Color: 17
Size: 1781 Color: 17
Size: 512 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13360 Color: 4
Size: 1296 Color: 18
Size: 976 Color: 6

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13446 Color: 13
Size: 1944 Color: 9
Size: 242 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 14
Size: 1700 Color: 4
Size: 420 Color: 7

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13526 Color: 2
Size: 1350 Color: 12
Size: 756 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13590 Color: 5
Size: 1450 Color: 14
Size: 592 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13596 Color: 14
Size: 1610 Color: 10
Size: 426 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13627 Color: 14
Size: 1517 Color: 12
Size: 488 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13639 Color: 4
Size: 1661 Color: 5
Size: 332 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 2
Size: 1491 Color: 18
Size: 382 Color: 12

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 4
Size: 1088 Color: 11
Size: 780 Color: 12

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 16
Size: 1538 Color: 17
Size: 304 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 16
Size: 1040 Color: 0
Size: 760 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 11
Size: 1334 Color: 14
Size: 402 Color: 12

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13903 Color: 14
Size: 1441 Color: 14
Size: 288 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 14
Size: 1300 Color: 10
Size: 416 Color: 16

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14014 Color: 14
Size: 1146 Color: 5
Size: 472 Color: 7

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14020 Color: 6
Size: 1296 Color: 17
Size: 316 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 10
Size: 1052 Color: 17
Size: 528 Color: 19

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 11
Size: 5739 Color: 10
Size: 472 Color: 5

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 9482 Color: 18
Size: 5011 Color: 0
Size: 1138 Color: 3

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 10307 Color: 13
Size: 5324 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 7
Size: 2343 Color: 5
Size: 1956 Color: 11

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 11615 Color: 17
Size: 2992 Color: 13
Size: 1024 Color: 7

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 11689 Color: 7
Size: 3662 Color: 2
Size: 280 Color: 1

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12244 Color: 1
Size: 2659 Color: 9
Size: 728 Color: 10

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 2
Size: 2791 Color: 19
Size: 280 Color: 9

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12703 Color: 18
Size: 1604 Color: 4
Size: 1324 Color: 5

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12979 Color: 9
Size: 2248 Color: 0
Size: 404 Color: 3

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13039 Color: 19
Size: 1590 Color: 10
Size: 1002 Color: 13

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13094 Color: 6
Size: 2281 Color: 2
Size: 256 Color: 10

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13147 Color: 18
Size: 2152 Color: 19
Size: 332 Color: 15

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 15
Size: 2441 Color: 5

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13243 Color: 13
Size: 2148 Color: 5
Size: 240 Color: 19

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 12
Size: 1402 Color: 6
Size: 384 Color: 8

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 9308 Color: 2
Size: 5666 Color: 1
Size: 656 Color: 18

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 10114 Color: 19
Size: 5260 Color: 10
Size: 256 Color: 0

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 11
Size: 3702 Color: 19

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 12858 Color: 11
Size: 2348 Color: 7
Size: 424 Color: 0

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 13292 Color: 2
Size: 1982 Color: 8
Size: 356 Color: 0

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 16
Size: 2314 Color: 11

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 13366 Color: 17
Size: 1300 Color: 0
Size: 964 Color: 1

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 13542 Color: 15
Size: 1740 Color: 14
Size: 348 Color: 6

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 13619 Color: 2
Size: 2011 Color: 18

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 6
Size: 1016 Color: 1
Size: 912 Color: 14

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13808 Color: 3
Size: 1822 Color: 12

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 1
Size: 1336 Color: 10
Size: 416 Color: 8

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 4
Size: 5689 Color: 12
Size: 604 Color: 15

Bin 78: 3 of cap free
Amount of items: 3
Items: 
Size: 9619 Color: 5
Size: 5770 Color: 10
Size: 240 Color: 8

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 13
Size: 3911 Color: 5
Size: 624 Color: 4

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 11878 Color: 15
Size: 3571 Color: 5
Size: 180 Color: 0

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 11911 Color: 2
Size: 3718 Color: 13

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 12085 Color: 0
Size: 3208 Color: 10
Size: 336 Color: 19

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 15
Size: 3349 Color: 17

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 12778 Color: 11
Size: 1703 Color: 1
Size: 1148 Color: 10

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 12883 Color: 6
Size: 2682 Color: 7
Size: 64 Color: 7

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13150 Color: 6
Size: 1859 Color: 2
Size: 620 Color: 10

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13950 Color: 17
Size: 1679 Color: 16

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 10818 Color: 11
Size: 4602 Color: 4
Size: 208 Color: 18

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 15
Size: 3096 Color: 0
Size: 280 Color: 8

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 12808 Color: 0
Size: 2820 Color: 7

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 12772 Color: 19
Size: 2856 Color: 2

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 13452 Color: 9
Size: 1348 Color: 1
Size: 828 Color: 0

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13708 Color: 18
Size: 1920 Color: 10

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 14
Size: 1758 Color: 5

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 13980 Color: 5
Size: 1608 Color: 11
Size: 40 Color: 16

Bin 96: 5 of cap free
Amount of items: 7
Items: 
Size: 7821 Color: 0
Size: 1872 Color: 3
Size: 1852 Color: 15
Size: 1838 Color: 7
Size: 1300 Color: 17
Size: 648 Color: 4
Size: 296 Color: 1

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 8795 Color: 10
Size: 6832 Color: 9

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 12580 Color: 9
Size: 2605 Color: 14
Size: 442 Color: 1

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 13589 Color: 2
Size: 2038 Color: 10

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 13608 Color: 15
Size: 2019 Color: 1

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 9096 Color: 10
Size: 6242 Color: 0
Size: 288 Color: 12

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 12370 Color: 6
Size: 2936 Color: 13
Size: 320 Color: 10

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 12418 Color: 18
Size: 2600 Color: 6
Size: 608 Color: 1

Bin 104: 6 of cap free
Amount of items: 3
Items: 
Size: 12722 Color: 8
Size: 2372 Color: 19
Size: 532 Color: 18

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 11349 Color: 19
Size: 4276 Color: 15

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 9596 Color: 17
Size: 5612 Color: 18
Size: 416 Color: 3

Bin 107: 8 of cap free
Amount of items: 3
Items: 
Size: 10831 Color: 6
Size: 4001 Color: 17
Size: 792 Color: 5

Bin 108: 8 of cap free
Amount of items: 3
Items: 
Size: 11640 Color: 0
Size: 3564 Color: 5
Size: 420 Color: 6

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 12658 Color: 12
Size: 2966 Color: 18

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 13118 Color: 2
Size: 2382 Color: 17
Size: 124 Color: 8

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 13198 Color: 12
Size: 2426 Color: 6

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 13500 Color: 5
Size: 2124 Color: 18

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13804 Color: 2
Size: 1820 Color: 10

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 0
Size: 1564 Color: 2

Bin 115: 9 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 8
Size: 6511 Color: 3
Size: 304 Color: 5

Bin 116: 9 of cap free
Amount of items: 3
Items: 
Size: 12820 Color: 0
Size: 2091 Color: 4
Size: 712 Color: 10

Bin 117: 10 of cap free
Amount of items: 2
Items: 
Size: 13411 Color: 4
Size: 2211 Color: 14

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 13430 Color: 16
Size: 2192 Color: 11

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13992 Color: 19
Size: 1630 Color: 13

Bin 120: 11 of cap free
Amount of items: 3
Items: 
Size: 10941 Color: 8
Size: 4328 Color: 17
Size: 352 Color: 0

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 12520 Color: 1
Size: 3101 Color: 14

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 13219 Color: 7
Size: 2402 Color: 19

Bin 123: 12 of cap free
Amount of items: 3
Items: 
Size: 8834 Color: 9
Size: 6514 Color: 1
Size: 272 Color: 3

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 9298 Color: 10
Size: 5682 Color: 5
Size: 640 Color: 0

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 12442 Color: 19
Size: 2794 Color: 0
Size: 384 Color: 9

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13459 Color: 1
Size: 2161 Color: 16

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 13548 Color: 13
Size: 2072 Color: 10

Bin 128: 13 of cap free
Amount of items: 2
Items: 
Size: 11190 Color: 12
Size: 4429 Color: 3

Bin 129: 14 of cap free
Amount of items: 2
Items: 
Size: 12201 Color: 2
Size: 3417 Color: 6

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13876 Color: 14
Size: 1742 Color: 19

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 2
Size: 1724 Color: 7

Bin 132: 15 of cap free
Amount of items: 7
Items: 
Size: 7820 Color: 6
Size: 1702 Color: 7
Size: 1671 Color: 15
Size: 1524 Color: 10
Size: 1296 Color: 2
Size: 1048 Color: 17
Size: 556 Color: 12

Bin 133: 15 of cap free
Amount of items: 3
Items: 
Size: 8807 Color: 4
Size: 6506 Color: 12
Size: 304 Color: 13

Bin 134: 15 of cap free
Amount of items: 2
Items: 
Size: 12612 Color: 7
Size: 3005 Color: 5

Bin 135: 15 of cap free
Amount of items: 2
Items: 
Size: 12895 Color: 6
Size: 2722 Color: 3

Bin 136: 15 of cap free
Amount of items: 2
Items: 
Size: 13706 Color: 8
Size: 1911 Color: 3

Bin 137: 16 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 6
Size: 3588 Color: 10
Size: 916 Color: 0

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 12788 Color: 14
Size: 2828 Color: 5

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 13726 Color: 9
Size: 1890 Color: 2

Bin 140: 17 of cap free
Amount of items: 2
Items: 
Size: 11973 Color: 16
Size: 3642 Color: 1

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 15
Size: 2071 Color: 16

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 13368 Color: 7
Size: 2246 Color: 4

Bin 143: 19 of cap free
Amount of items: 2
Items: 
Size: 13495 Color: 7
Size: 2118 Color: 12

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 13622 Color: 19
Size: 1991 Color: 3

Bin 145: 20 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 15
Size: 2548 Color: 16

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 13125 Color: 18
Size: 2487 Color: 15

Bin 147: 24 of cap free
Amount of items: 2
Items: 
Size: 10508 Color: 3
Size: 5100 Color: 11

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 12443 Color: 11
Size: 3164 Color: 1

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 12649 Color: 6
Size: 2957 Color: 8

Bin 150: 26 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 3
Size: 2546 Color: 7

Bin 151: 27 of cap free
Amount of items: 4
Items: 
Size: 8747 Color: 16
Size: 5626 Color: 8
Size: 848 Color: 0
Size: 384 Color: 4

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 13494 Color: 2
Size: 2110 Color: 19

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 12074 Color: 2
Size: 3528 Color: 10

Bin 154: 31 of cap free
Amount of items: 2
Items: 
Size: 14040 Color: 17
Size: 1561 Color: 12

Bin 155: 32 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 5
Size: 4024 Color: 11
Size: 1136 Color: 6

Bin 156: 32 of cap free
Amount of items: 2
Items: 
Size: 10420 Color: 19
Size: 5180 Color: 10

Bin 157: 32 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 1
Size: 1880 Color: 2

Bin 158: 33 of cap free
Amount of items: 2
Items: 
Size: 13211 Color: 4
Size: 2388 Color: 1

Bin 159: 33 of cap free
Amount of items: 2
Items: 
Size: 13304 Color: 7
Size: 2295 Color: 2

Bin 160: 34 of cap free
Amount of items: 3
Items: 
Size: 11174 Color: 17
Size: 4328 Color: 2
Size: 96 Color: 10

Bin 161: 37 of cap free
Amount of items: 2
Items: 
Size: 13813 Color: 13
Size: 1782 Color: 0

Bin 162: 38 of cap free
Amount of items: 2
Items: 
Size: 13564 Color: 13
Size: 2030 Color: 19

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 14034 Color: 7
Size: 1560 Color: 13

Bin 164: 40 of cap free
Amount of items: 4
Items: 
Size: 8172 Color: 9
Size: 4584 Color: 13
Size: 1520 Color: 17
Size: 1316 Color: 1

Bin 165: 42 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 10
Size: 2662 Color: 16
Size: 712 Color: 19

Bin 166: 46 of cap free
Amount of items: 2
Items: 
Size: 11238 Color: 13
Size: 4348 Color: 2

Bin 167: 47 of cap free
Amount of items: 2
Items: 
Size: 13294 Color: 4
Size: 2291 Color: 16

Bin 168: 48 of cap free
Amount of items: 2
Items: 
Size: 10136 Color: 12
Size: 5448 Color: 13

Bin 169: 48 of cap free
Amount of items: 2
Items: 
Size: 13403 Color: 18
Size: 2181 Color: 10

Bin 170: 50 of cap free
Amount of items: 3
Items: 
Size: 9244 Color: 11
Size: 5036 Color: 7
Size: 1302 Color: 3

Bin 171: 51 of cap free
Amount of items: 9
Items: 
Size: 7817 Color: 15
Size: 1132 Color: 19
Size: 1132 Color: 2
Size: 1124 Color: 11
Size: 1120 Color: 13
Size: 1064 Color: 18
Size: 944 Color: 16
Size: 800 Color: 6
Size: 448 Color: 4

Bin 172: 60 of cap free
Amount of items: 2
Items: 
Size: 12285 Color: 8
Size: 3287 Color: 6

Bin 173: 66 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 0
Size: 3782 Color: 9

Bin 174: 68 of cap free
Amount of items: 2
Items: 
Size: 9756 Color: 19
Size: 5808 Color: 6

Bin 175: 70 of cap free
Amount of items: 2
Items: 
Size: 8914 Color: 15
Size: 6648 Color: 11

Bin 176: 74 of cap free
Amount of items: 2
Items: 
Size: 10658 Color: 0
Size: 4900 Color: 2

Bin 177: 78 of cap free
Amount of items: 2
Items: 
Size: 7826 Color: 2
Size: 7728 Color: 0

Bin 178: 80 of cap free
Amount of items: 2
Items: 
Size: 8900 Color: 18
Size: 6652 Color: 15

Bin 179: 82 of cap free
Amount of items: 2
Items: 
Size: 11738 Color: 1
Size: 3812 Color: 15

Bin 180: 85 of cap free
Amount of items: 2
Items: 
Size: 12879 Color: 14
Size: 2668 Color: 15

Bin 181: 86 of cap free
Amount of items: 2
Items: 
Size: 11400 Color: 4
Size: 4146 Color: 2

Bin 182: 89 of cap free
Amount of items: 2
Items: 
Size: 12181 Color: 15
Size: 3362 Color: 13

Bin 183: 91 of cap free
Amount of items: 2
Items: 
Size: 10259 Color: 2
Size: 5282 Color: 5

Bin 184: 91 of cap free
Amount of items: 2
Items: 
Size: 12507 Color: 14
Size: 3034 Color: 4

Bin 185: 92 of cap free
Amount of items: 3
Items: 
Size: 8882 Color: 15
Size: 5746 Color: 5
Size: 912 Color: 1

Bin 186: 93 of cap free
Amount of items: 2
Items: 
Size: 11060 Color: 13
Size: 4479 Color: 3

Bin 187: 94 of cap free
Amount of items: 2
Items: 
Size: 10808 Color: 13
Size: 4730 Color: 19

Bin 188: 98 of cap free
Amount of items: 21
Items: 
Size: 1032 Color: 17
Size: 1000 Color: 10
Size: 944 Color: 10
Size: 886 Color: 13
Size: 884 Color: 3
Size: 864 Color: 10
Size: 864 Color: 10
Size: 864 Color: 2
Size: 808 Color: 0
Size: 800 Color: 7
Size: 740 Color: 16
Size: 740 Color: 13
Size: 732 Color: 5
Size: 672 Color: 0
Size: 668 Color: 6
Size: 624 Color: 6
Size: 576 Color: 17
Size: 560 Color: 1
Size: 458 Color: 12
Size: 414 Color: 4
Size: 404 Color: 12

Bin 189: 98 of cap free
Amount of items: 3
Items: 
Size: 8738 Color: 5
Size: 6508 Color: 11
Size: 288 Color: 6

Bin 190: 98 of cap free
Amount of items: 2
Items: 
Size: 10258 Color: 6
Size: 5276 Color: 13

Bin 191: 112 of cap free
Amount of items: 2
Items: 
Size: 9324 Color: 5
Size: 6196 Color: 8

Bin 192: 118 of cap free
Amount of items: 36
Items: 
Size: 574 Color: 5
Size: 560 Color: 17
Size: 560 Color: 14
Size: 556 Color: 18
Size: 540 Color: 2
Size: 530 Color: 1
Size: 520 Color: 6
Size: 504 Color: 10
Size: 496 Color: 15
Size: 484 Color: 14
Size: 476 Color: 6
Size: 472 Color: 18
Size: 468 Color: 11
Size: 464 Color: 17
Size: 464 Color: 14
Size: 464 Color: 9
Size: 464 Color: 6
Size: 458 Color: 17
Size: 456 Color: 0
Size: 432 Color: 16
Size: 416 Color: 8
Size: 402 Color: 10
Size: 400 Color: 14
Size: 398 Color: 12
Size: 370 Color: 12
Size: 364 Color: 9
Size: 360 Color: 3
Size: 352 Color: 1
Size: 344 Color: 15
Size: 340 Color: 7
Size: 340 Color: 4
Size: 334 Color: 3
Size: 304 Color: 4
Size: 292 Color: 7
Size: 288 Color: 4
Size: 268 Color: 3

Bin 193: 186 of cap free
Amount of items: 3
Items: 
Size: 7834 Color: 6
Size: 7236 Color: 1
Size: 376 Color: 3

Bin 194: 188 of cap free
Amount of items: 3
Items: 
Size: 8204 Color: 8
Size: 6504 Color: 19
Size: 736 Color: 12

Bin 195: 221 of cap free
Amount of items: 2
Items: 
Size: 8898 Color: 14
Size: 6513 Color: 6

Bin 196: 236 of cap free
Amount of items: 7
Items: 
Size: 7818 Color: 19
Size: 1470 Color: 16
Size: 1380 Color: 5
Size: 1368 Color: 0
Size: 1296 Color: 16
Size: 1136 Color: 19
Size: 928 Color: 3

Bin 197: 244 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 12
Size: 4439 Color: 0
Size: 2877 Color: 4

Bin 198: 250 of cap free
Amount of items: 3
Items: 
Size: 7832 Color: 0
Size: 6502 Color: 9
Size: 1048 Color: 7

Bin 199: 10904 of cap free
Amount of items: 15
Items: 
Size: 368 Color: 16
Size: 364 Color: 12
Size: 362 Color: 13
Size: 352 Color: 18
Size: 348 Color: 1
Size: 344 Color: 19
Size: 336 Color: 19
Size: 334 Color: 15
Size: 304 Color: 9
Size: 288 Color: 7
Size: 272 Color: 17
Size: 272 Color: 15
Size: 264 Color: 12
Size: 264 Color: 3
Size: 256 Color: 4

Total size: 3095136
Total free space: 15632

