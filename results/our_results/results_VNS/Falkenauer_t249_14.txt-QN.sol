Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 233
Size: 274 Color: 66
Size: 263 Color: 43

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 311 Color: 123
Size: 266 Color: 52
Size: 423 Color: 211

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 205
Size: 329 Color: 136
Size: 257 Color: 23

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 226
Size: 299 Color: 104
Size: 256 Color: 20

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 218
Size: 293 Color: 99
Size: 279 Color: 75

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 232
Size: 280 Color: 77
Size: 260 Color: 37

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 212
Size: 293 Color: 98
Size: 284 Color: 87

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 239
Size: 264 Color: 47
Size: 257 Color: 22

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 180
Size: 344 Color: 148
Size: 280 Color: 78

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 240
Size: 259 Color: 28
Size: 258 Color: 26

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 160
Size: 355 Color: 159
Size: 290 Color: 95

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 200
Size: 329 Color: 137
Size: 268 Color: 55

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 188
Size: 314 Color: 126
Size: 305 Color: 113

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 216
Size: 287 Color: 91
Size: 287 Color: 90

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 192
Size: 347 Color: 150
Size: 269 Color: 57

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 222
Size: 308 Color: 117
Size: 259 Color: 27

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 243
Size: 259 Color: 29
Size: 256 Color: 18

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 202
Size: 326 Color: 132
Size: 267 Color: 53

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 230
Size: 281 Color: 80
Size: 266 Color: 51

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 217
Size: 309 Color: 119
Size: 264 Color: 46

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 208
Size: 321 Color: 128
Size: 262 Color: 40

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 191
Size: 311 Color: 122
Size: 305 Color: 114

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 224
Size: 281 Color: 81
Size: 279 Color: 76

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 193
Size: 309 Color: 120
Size: 304 Color: 112

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 187
Size: 345 Color: 149
Size: 275 Color: 70

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 161
Size: 349 Color: 151
Size: 295 Color: 101

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 190
Size: 366 Color: 171
Size: 251 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 166
Size: 356 Color: 162
Size: 284 Color: 86

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 203
Size: 327 Color: 133
Size: 263 Color: 42

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 236
Size: 274 Color: 65
Size: 257 Color: 25

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 210
Size: 318 Color: 127
Size: 260 Color: 38

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 201
Size: 330 Color: 138
Size: 264 Color: 45

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 153
Size: 350 Color: 152
Size: 300 Color: 106

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 245
Size: 255 Color: 15
Size: 252 Color: 8

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 181
Size: 354 Color: 158
Size: 268 Color: 56

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 186
Size: 342 Color: 146
Size: 279 Color: 74

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 197
Size: 343 Color: 147
Size: 260 Color: 35

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 235
Size: 285 Color: 89
Size: 251 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 234
Size: 276 Color: 71
Size: 260 Color: 31

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 195
Size: 308 Color: 116
Size: 301 Color: 107

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 229
Size: 297 Color: 102
Size: 254 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 247
Size: 254 Color: 13
Size: 251 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 244
Size: 260 Color: 33
Size: 253 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 198
Size: 352 Color: 157
Size: 251 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 194
Size: 306 Color: 115
Size: 303 Color: 110

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 213
Size: 314 Color: 124
Size: 263 Color: 44

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 175
Size: 324 Color: 129
Size: 302 Color: 109

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 228
Size: 289 Color: 93
Size: 262 Color: 41

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 207
Size: 335 Color: 142
Size: 250 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 221
Size: 290 Color: 94
Size: 278 Color: 73

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 223
Size: 297 Color: 103
Size: 266 Color: 49

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 173
Size: 326 Color: 131
Size: 302 Color: 108

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 206
Size: 325 Color: 130
Size: 260 Color: 32

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 196
Size: 332 Color: 141
Size: 275 Color: 68

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 209
Size: 308 Color: 118
Size: 273 Color: 64

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 219
Size: 314 Color: 125
Size: 256 Color: 16

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 177
Size: 352 Color: 156
Size: 273 Color: 63

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 169
Size: 361 Color: 168
Size: 276 Color: 72

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 199
Size: 327 Color: 134
Size: 271 Color: 60

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 172
Size: 358 Color: 165
Size: 275 Color: 67

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 174
Size: 358 Color: 164
Size: 270 Color: 59

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 227
Size: 280 Color: 79
Size: 272 Color: 61

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 238
Size: 269 Color: 58
Size: 255 Color: 14

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 185
Size: 339 Color: 143
Size: 282 Color: 83

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 189
Size: 361 Color: 167
Size: 257 Color: 21

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 237
Size: 267 Color: 54
Size: 261 Color: 39

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 220
Size: 310 Color: 121
Size: 260 Color: 34

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 242
Size: 264 Color: 48
Size: 252 Color: 7

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 182
Size: 339 Color: 144
Size: 283 Color: 85

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 183
Size: 328 Color: 135
Size: 294 Color: 100

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 246
Size: 254 Color: 11
Size: 251 Color: 5

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 179
Size: 358 Color: 163
Size: 266 Color: 50

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 241
Size: 259 Color: 30
Size: 257 Color: 24

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 231
Size: 288 Color: 92
Size: 256 Color: 17

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 225
Size: 283 Color: 84
Size: 275 Color: 69

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 178
Size: 365 Color: 170
Size: 260 Color: 36

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 214
Size: 291 Color: 96
Size: 285 Color: 88

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 184
Size: 339 Color: 145
Size: 282 Color: 82

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 176
Size: 332 Color: 140
Size: 293 Color: 97

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 215
Size: 303 Color: 111
Size: 272 Color: 62

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 248
Size: 252 Color: 9
Size: 250 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 204
Size: 331 Color: 139
Size: 256 Color: 19

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 155
Size: 350 Color: 154
Size: 299 Color: 105

Total size: 83000
Total free space: 0

