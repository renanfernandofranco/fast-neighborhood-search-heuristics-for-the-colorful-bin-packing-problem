Capicity Bin: 15616
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8104 Color: 413
Size: 6502 Color: 395
Size: 354 Color: 64
Size: 336 Color: 54
Size: 320 Color: 51

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 430
Size: 5744 Color: 386
Size: 296 Color: 34

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11492 Color: 464
Size: 3052 Color: 323
Size: 1072 Color: 188

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11563 Color: 467
Size: 3365 Color: 334
Size: 688 Color: 148

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11939 Color: 478
Size: 3375 Color: 336
Size: 302 Color: 35

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12028 Color: 482
Size: 3412 Color: 337
Size: 176 Color: 7

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 485
Size: 2596 Color: 306
Size: 880 Color: 173

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12343 Color: 490
Size: 3065 Color: 324
Size: 208 Color: 9

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12425 Color: 493
Size: 2383 Color: 297
Size: 808 Color: 161

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 498
Size: 2442 Color: 300
Size: 686 Color: 147

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 508
Size: 1867 Color: 261
Size: 992 Color: 181

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12792 Color: 509
Size: 1528 Color: 225
Size: 1296 Color: 197

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12796 Color: 510
Size: 2360 Color: 296
Size: 460 Color: 99

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12810 Color: 511
Size: 1784 Color: 253
Size: 1022 Color: 185

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12846 Color: 514
Size: 2024 Color: 273
Size: 746 Color: 154

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 517
Size: 2356 Color: 295
Size: 280 Color: 25

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 520
Size: 2088 Color: 278
Size: 496 Color: 114

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13052 Color: 521
Size: 1806 Color: 256
Size: 758 Color: 157

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13095 Color: 524
Size: 1561 Color: 231
Size: 960 Color: 177

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13178 Color: 529
Size: 2298 Color: 290
Size: 140 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13190 Color: 530
Size: 2022 Color: 272
Size: 404 Color: 82

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13191 Color: 531
Size: 2065 Color: 276
Size: 360 Color: 67

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 532
Size: 1584 Color: 233
Size: 840 Color: 170

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13325 Color: 538
Size: 1715 Color: 248
Size: 576 Color: 129

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13334 Color: 539
Size: 1458 Color: 218
Size: 824 Color: 166

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13336 Color: 540
Size: 1534 Color: 228
Size: 746 Color: 153

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13348 Color: 541
Size: 1300 Color: 203
Size: 968 Color: 179

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13377 Color: 542
Size: 1911 Color: 265
Size: 328 Color: 52

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13428 Color: 544
Size: 1708 Color: 246
Size: 480 Color: 109

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13451 Color: 547
Size: 1805 Color: 255
Size: 360 Color: 69

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13454 Color: 548
Size: 1802 Color: 254
Size: 360 Color: 65

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13559 Color: 555
Size: 1525 Color: 224
Size: 532 Color: 124

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13566 Color: 556
Size: 1556 Color: 230
Size: 494 Color: 112

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13598 Color: 558
Size: 1614 Color: 238
Size: 404 Color: 84

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13607 Color: 559
Size: 1649 Color: 242
Size: 360 Color: 66

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13608 Color: 560
Size: 1396 Color: 213
Size: 612 Color: 135

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13611 Color: 561
Size: 1671 Color: 243
Size: 334 Color: 53

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13660 Color: 563
Size: 1636 Color: 241
Size: 320 Color: 49

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13674 Color: 564
Size: 1448 Color: 217
Size: 494 Color: 113

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13684 Color: 566
Size: 1420 Color: 215
Size: 512 Color: 116

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13728 Color: 571
Size: 1612 Color: 237
Size: 276 Color: 23

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 573
Size: 1621 Color: 239
Size: 218 Color: 11

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 574
Size: 1522 Color: 223
Size: 316 Color: 43

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 575
Size: 1364 Color: 209
Size: 472 Color: 107

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13790 Color: 577
Size: 918 Color: 176
Size: 908 Color: 174

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13845 Color: 581
Size: 1467 Color: 219
Size: 304 Color: 37

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13870 Color: 584
Size: 1404 Color: 214
Size: 342 Color: 58

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13880 Color: 585
Size: 1296 Color: 199
Size: 440 Color: 96

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13910 Color: 586
Size: 1330 Color: 206
Size: 376 Color: 74

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13912 Color: 587
Size: 1024 Color: 186
Size: 680 Color: 146

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 588
Size: 1384 Color: 211
Size: 316 Color: 44

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13948 Color: 591
Size: 1528 Color: 226
Size: 140 Color: 4

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13950 Color: 592
Size: 1136 Color: 191
Size: 530 Color: 123

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13958 Color: 593
Size: 1390 Color: 212
Size: 268 Color: 18

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 13960 Color: 594
Size: 1056 Color: 187
Size: 600 Color: 134

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 595
Size: 1324 Color: 205
Size: 304 Color: 38

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 599
Size: 1184 Color: 195
Size: 404 Color: 83

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14040 Color: 600
Size: 1296 Color: 198
Size: 280 Color: 26

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 10548 Color: 440
Size: 5067 Color: 376

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 11512 Color: 466
Size: 4103 Color: 358

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 11927 Color: 477
Size: 3688 Color: 345

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12414 Color: 491
Size: 2325 Color: 292
Size: 876 Color: 171

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 495
Size: 2647 Color: 309
Size: 528 Color: 119

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12641 Color: 502
Size: 2670 Color: 314
Size: 304 Color: 39

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12645 Color: 503
Size: 1958 Color: 268
Size: 1012 Color: 184

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12811 Color: 512
Size: 2190 Color: 287
Size: 614 Color: 137

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 12967 Color: 516
Size: 2648 Color: 310

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 13017 Color: 519
Size: 2598 Color: 307

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 13079 Color: 523
Size: 2020 Color: 270
Size: 516 Color: 118

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 13102 Color: 525
Size: 2021 Color: 271
Size: 492 Color: 111

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 13148 Color: 528
Size: 2467 Color: 301

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 13203 Color: 534
Size: 2412 Color: 299

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 551
Size: 2128 Color: 283

Bin 74: 1 of cap free
Amount of items: 2
Items: 
Size: 13517 Color: 553
Size: 2098 Color: 280

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 13682 Color: 565
Size: 1933 Color: 267

Bin 76: 1 of cap free
Amount of items: 2
Items: 
Size: 13787 Color: 576
Size: 1828 Color: 259

Bin 77: 1 of cap free
Amount of items: 2
Items: 
Size: 13801 Color: 579
Size: 1814 Color: 258

Bin 78: 2 of cap free
Amount of items: 5
Items: 
Size: 7912 Color: 412
Size: 6500 Color: 394
Size: 530 Color: 122
Size: 336 Color: 56
Size: 336 Color: 55

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11300 Color: 458
Size: 3338 Color: 333
Size: 976 Color: 180

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 11834 Color: 474
Size: 3780 Color: 350

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 12282 Color: 488
Size: 3068 Color: 325
Size: 264 Color: 17

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12289 Color: 489
Size: 2661 Color: 312
Size: 664 Color: 142

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 12724 Color: 506
Size: 2890 Color: 319

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 13272 Color: 536
Size: 2342 Color: 294

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13474 Color: 549
Size: 2140 Color: 284

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 13524 Color: 554
Size: 2090 Color: 279

Bin 87: 2 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 568
Size: 1904 Color: 264

Bin 88: 2 of cap free
Amount of items: 2
Items: 
Size: 13926 Color: 589
Size: 1688 Color: 245

Bin 89: 2 of cap free
Amount of items: 2
Items: 
Size: 13932 Color: 590
Size: 1682 Color: 244

Bin 90: 2 of cap free
Amount of items: 2
Items: 
Size: 14022 Color: 598
Size: 1592 Color: 235

Bin 91: 3 of cap free
Amount of items: 11
Items: 
Size: 7809 Color: 405
Size: 1148 Color: 192
Size: 1120 Color: 190
Size: 1072 Color: 189
Size: 1008 Color: 183
Size: 1008 Color: 182
Size: 968 Color: 178
Size: 376 Color: 75
Size: 368 Color: 72
Size: 368 Color: 71
Size: 368 Color: 70

Bin 92: 3 of cap free
Amount of items: 7
Items: 
Size: 7816 Color: 409
Size: 1748 Color: 249
Size: 1710 Color: 247
Size: 1590 Color: 234
Size: 1581 Color: 232
Size: 824 Color: 165
Size: 344 Color: 59

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 13132 Color: 527
Size: 2481 Color: 303

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 570
Size: 1893 Color: 263

Bin 95: 3 of cap free
Amount of items: 2
Items: 
Size: 14010 Color: 597
Size: 1603 Color: 236

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 10564 Color: 441
Size: 5048 Color: 375

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 12088 Color: 484
Size: 3524 Color: 341

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 12420 Color: 492
Size: 3192 Color: 331

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 13444 Color: 546
Size: 2168 Color: 286

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 578
Size: 1812 Color: 257

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 13990 Color: 596
Size: 1622 Color: 240

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 12480 Color: 497
Size: 3131 Color: 328

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 13719 Color: 569
Size: 1892 Color: 262

Bin 104: 5 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 580
Size: 1775 Color: 252

Bin 105: 6 of cap free
Amount of items: 9
Items: 
Size: 7810 Color: 406
Size: 1300 Color: 201
Size: 1296 Color: 200
Size: 1248 Color: 196
Size: 1184 Color: 194
Size: 1148 Color: 193
Size: 912 Color: 175
Size: 360 Color: 68
Size: 352 Color: 63

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 10006 Color: 434
Size: 5604 Color: 384

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13846 Color: 582
Size: 1764 Color: 251

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 8782 Color: 418
Size: 6507 Color: 399
Size: 320 Color: 45

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 12429 Color: 494
Size: 3180 Color: 330

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 504
Size: 2952 Color: 321

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 12827 Color: 513
Size: 2782 Color: 316

Bin 112: 7 of cap free
Amount of items: 2
Items: 
Size: 13270 Color: 535
Size: 2339 Color: 293

Bin 113: 7 of cap free
Amount of items: 2
Items: 
Size: 13442 Color: 545
Size: 2167 Color: 285

Bin 114: 7 of cap free
Amount of items: 2
Items: 
Size: 13508 Color: 552
Size: 2101 Color: 281

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 11067 Color: 451
Size: 4541 Color: 368

Bin 116: 8 of cap free
Amount of items: 3
Items: 
Size: 11080 Color: 452
Size: 3208 Color: 332
Size: 1320 Color: 204

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 11380 Color: 461
Size: 4228 Color: 365

Bin 118: 8 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 583
Size: 1751 Color: 250

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 11567 Color: 468
Size: 4040 Color: 355

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 12744 Color: 507
Size: 2863 Color: 317

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 13297 Color: 537
Size: 2310 Color: 291

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 13398 Color: 543
Size: 2209 Color: 289

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 12990 Color: 518
Size: 2616 Color: 308

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 13068 Color: 522
Size: 2538 Color: 305

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13572 Color: 557
Size: 2034 Color: 274

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 13756 Color: 572
Size: 1850 Color: 260

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 13693 Color: 567
Size: 1912 Color: 266

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 13196 Color: 533
Size: 2408 Color: 298

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13480 Color: 550
Size: 2124 Color: 282

Bin 130: 13 of cap free
Amount of items: 2
Items: 
Size: 12159 Color: 487
Size: 3444 Color: 339

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 526
Size: 2491 Color: 304

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 8980 Color: 421
Size: 6622 Color: 402

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 11994 Color: 481
Size: 3608 Color: 343

Bin 134: 15 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 446
Size: 2881 Color: 318
Size: 2060 Color: 275

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 8400 Color: 414
Size: 6880 Color: 403
Size: 320 Color: 50

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 10708 Color: 448
Size: 4616 Color: 370
Size: 276 Color: 24

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 11964 Color: 480
Size: 3636 Color: 344

Bin 138: 17 of cap free
Amount of items: 2
Items: 
Size: 13639 Color: 562
Size: 1960 Color: 269

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 11859 Color: 475
Size: 3739 Color: 348

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 9450 Color: 426
Size: 6146 Color: 390

Bin 141: 20 of cap free
Amount of items: 2
Items: 
Size: 12574 Color: 501
Size: 3022 Color: 322

Bin 142: 21 of cap free
Amount of items: 2
Items: 
Size: 12441 Color: 496
Size: 3154 Color: 329

Bin 143: 23 of cap free
Amount of items: 7
Items: 
Size: 7812 Color: 408
Size: 1550 Color: 229
Size: 1532 Color: 227
Size: 1513 Color: 222
Size: 1484 Color: 221
Size: 1358 Color: 208
Size: 344 Color: 60

Bin 144: 24 of cap free
Amount of items: 3
Items: 
Size: 9537 Color: 429
Size: 5751 Color: 388
Size: 304 Color: 36

Bin 145: 24 of cap free
Amount of items: 3
Items: 
Size: 11432 Color: 463
Size: 3940 Color: 354
Size: 220 Color: 12

Bin 146: 25 of cap free
Amount of items: 2
Items: 
Size: 11499 Color: 465
Size: 4092 Color: 357

Bin 147: 25 of cap free
Amount of items: 2
Items: 
Size: 11800 Color: 473
Size: 3791 Color: 352

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 12862 Color: 515
Size: 2729 Color: 315

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 11378 Color: 460
Size: 4212 Color: 364

Bin 150: 29 of cap free
Amount of items: 2
Items: 
Size: 12508 Color: 500
Size: 3079 Color: 327

Bin 151: 30 of cap free
Amount of items: 3
Items: 
Size: 9250 Color: 425
Size: 6032 Color: 389
Size: 304 Color: 42

Bin 152: 30 of cap free
Amount of items: 2
Items: 
Size: 12686 Color: 505
Size: 2900 Color: 320

Bin 153: 35 of cap free
Amount of items: 2
Items: 
Size: 12150 Color: 486
Size: 3431 Color: 338

Bin 154: 38 of cap free
Amount of items: 3
Items: 
Size: 11260 Color: 457
Size: 4054 Color: 356
Size: 264 Color: 14

Bin 155: 38 of cap free
Amount of items: 2
Items: 
Size: 12044 Color: 483
Size: 3534 Color: 342

Bin 156: 39 of cap free
Amount of items: 2
Items: 
Size: 12502 Color: 499
Size: 3075 Color: 326

Bin 157: 40 of cap free
Amount of items: 3
Items: 
Size: 10167 Color: 437
Size: 5121 Color: 378
Size: 288 Color: 29

Bin 158: 41 of cap free
Amount of items: 3
Items: 
Size: 11147 Color: 456
Size: 4164 Color: 362
Size: 264 Color: 15

Bin 159: 44 of cap free
Amount of items: 3
Items: 
Size: 10693 Color: 447
Size: 4595 Color: 369
Size: 284 Color: 27

Bin 160: 45 of cap free
Amount of items: 4
Items: 
Size: 11923 Color: 476
Size: 3372 Color: 335
Size: 160 Color: 5
Size: 116 Color: 2

Bin 161: 46 of cap free
Amount of items: 23
Items: 
Size: 880 Color: 172
Size: 840 Color: 169
Size: 832 Color: 168
Size: 830 Color: 167
Size: 824 Color: 164
Size: 820 Color: 163
Size: 816 Color: 162
Size: 808 Color: 160
Size: 784 Color: 159
Size: 784 Color: 158
Size: 752 Color: 156
Size: 752 Color: 155
Size: 744 Color: 152
Size: 720 Color: 151
Size: 704 Color: 150
Size: 696 Color: 149
Size: 674 Color: 145
Size: 400 Color: 80
Size: 388 Color: 79
Size: 386 Color: 78
Size: 384 Color: 77
Size: 380 Color: 76
Size: 372 Color: 73

Bin 162: 46 of cap free
Amount of items: 4
Items: 
Size: 11948 Color: 479
Size: 3496 Color: 340
Size: 80 Color: 1
Size: 46 Color: 0

Bin 163: 48 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 472
Size: 3784 Color: 351

Bin 164: 51 of cap free
Amount of items: 3
Items: 
Size: 11396 Color: 462
Size: 3913 Color: 353
Size: 256 Color: 13

Bin 165: 60 of cap free
Amount of items: 3
Items: 
Size: 11135 Color: 455
Size: 4157 Color: 361
Size: 264 Color: 16

Bin 166: 64 of cap free
Amount of items: 2
Items: 
Size: 9140 Color: 423
Size: 6412 Color: 393

Bin 167: 64 of cap free
Amount of items: 2
Items: 
Size: 11352 Color: 459
Size: 4200 Color: 363

Bin 168: 65 of cap free
Amount of items: 3
Items: 
Size: 11131 Color: 454
Size: 4148 Color: 360
Size: 272 Color: 19

Bin 169: 67 of cap free
Amount of items: 3
Items: 
Size: 9496 Color: 428
Size: 5749 Color: 387
Size: 304 Color: 40

Bin 170: 68 of cap free
Amount of items: 2
Items: 
Size: 7820 Color: 411
Size: 7728 Color: 404

Bin 171: 71 of cap free
Amount of items: 3
Items: 
Size: 8719 Color: 417
Size: 6506 Color: 398
Size: 320 Color: 46

Bin 172: 72 of cap free
Amount of items: 3
Items: 
Size: 11614 Color: 471
Size: 3770 Color: 349
Size: 160 Color: 6

Bin 173: 73 of cap free
Amount of items: 3
Items: 
Size: 8718 Color: 416
Size: 6505 Color: 397
Size: 320 Color: 47

Bin 174: 75 of cap free
Amount of items: 3
Items: 
Size: 8717 Color: 415
Size: 6504 Color: 396
Size: 320 Color: 48

Bin 175: 104 of cap free
Amount of items: 3
Items: 
Size: 9820 Color: 433
Size: 5404 Color: 382
Size: 288 Color: 31

Bin 176: 109 of cap free
Amount of items: 3
Items: 
Size: 11572 Color: 469
Size: 3725 Color: 346
Size: 210 Color: 10

Bin 177: 110 of cap free
Amount of items: 3
Items: 
Size: 11579 Color: 470
Size: 3735 Color: 347
Size: 192 Color: 8

Bin 178: 112 of cap free
Amount of items: 2
Items: 
Size: 10088 Color: 435
Size: 5416 Color: 383

Bin 179: 112 of cap free
Amount of items: 2
Items: 
Size: 10644 Color: 445
Size: 4860 Color: 374

Bin 180: 113 of cap free
Amount of items: 3
Items: 
Size: 10103 Color: 436
Size: 5112 Color: 377
Size: 288 Color: 30

Bin 181: 116 of cap free
Amount of items: 3
Items: 
Size: 10628 Color: 443
Size: 2668 Color: 313
Size: 2204 Color: 288

Bin 182: 118 of cap free
Amount of items: 3
Items: 
Size: 11094 Color: 453
Size: 4132 Color: 359
Size: 272 Color: 20

Bin 183: 128 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 450
Size: 4424 Color: 367
Size: 272 Color: 21

Bin 184: 140 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 424
Size: 6268 Color: 392

Bin 185: 141 of cap free
Amount of items: 3
Items: 
Size: 9473 Color: 427
Size: 5698 Color: 385
Size: 304 Color: 41

Bin 186: 143 of cap free
Amount of items: 2
Items: 
Size: 10629 Color: 444
Size: 4844 Color: 373

Bin 187: 148 of cap free
Amount of items: 30
Items: 
Size: 672 Color: 144
Size: 672 Color: 143
Size: 628 Color: 141
Size: 626 Color: 140
Size: 624 Color: 139
Size: 624 Color: 138
Size: 614 Color: 136
Size: 600 Color: 133
Size: 576 Color: 132
Size: 576 Color: 131
Size: 576 Color: 130
Size: 576 Color: 128
Size: 552 Color: 127
Size: 544 Color: 126
Size: 536 Color: 125
Size: 528 Color: 121
Size: 456 Color: 98
Size: 456 Color: 97
Size: 436 Color: 95
Size: 432 Color: 94
Size: 432 Color: 93
Size: 424 Color: 92
Size: 424 Color: 91
Size: 420 Color: 90
Size: 416 Color: 89
Size: 416 Color: 88
Size: 416 Color: 87
Size: 408 Color: 86
Size: 408 Color: 85
Size: 400 Color: 81

Bin 188: 162 of cap free
Amount of items: 2
Items: 
Size: 10312 Color: 438
Size: 5142 Color: 379

Bin 189: 168 of cap free
Amount of items: 3
Items: 
Size: 9804 Color: 432
Size: 5352 Color: 381
Size: 292 Color: 32

Bin 190: 180 of cap free
Amount of items: 3
Items: 
Size: 10346 Color: 439
Size: 4802 Color: 371
Size: 288 Color: 28

Bin 191: 185 of cap free
Amount of items: 8
Items: 
Size: 7811 Color: 407
Size: 1478 Color: 220
Size: 1422 Color: 216
Size: 1382 Color: 210
Size: 1342 Color: 207
Size: 1300 Color: 202
Size: 348 Color: 62
Size: 348 Color: 61

Bin 192: 192 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 449
Size: 4394 Color: 366
Size: 276 Color: 22

Bin 193: 196 of cap free
Amount of items: 2
Items: 
Size: 10584 Color: 442
Size: 4836 Color: 372

Bin 194: 204 of cap free
Amount of items: 2
Items: 
Size: 8892 Color: 420
Size: 6520 Color: 401

Bin 195: 208 of cap free
Amount of items: 3
Items: 
Size: 9788 Color: 431
Size: 5328 Color: 380
Size: 292 Color: 33

Bin 196: 224 of cap free
Amount of items: 2
Items: 
Size: 8884 Color: 419
Size: 6508 Color: 400

Bin 197: 224 of cap free
Amount of items: 2
Items: 
Size: 9128 Color: 422
Size: 6264 Color: 391

Bin 198: 248 of cap free
Amount of items: 5
Items: 
Size: 7818 Color: 410
Size: 2657 Color: 311
Size: 2477 Color: 302
Size: 2076 Color: 277
Size: 340 Color: 57

Bin 199: 9858 of cap free
Amount of items: 12
Items: 
Size: 528 Color: 120
Size: 512 Color: 117
Size: 504 Color: 115
Size: 488 Color: 110
Size: 476 Color: 108
Size: 466 Color: 106
Size: 464 Color: 105
Size: 464 Color: 104
Size: 464 Color: 103
Size: 464 Color: 102
Size: 464 Color: 101
Size: 464 Color: 100

Total size: 3091968
Total free space: 15616

