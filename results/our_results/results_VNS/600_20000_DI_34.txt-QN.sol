Capicity Bin: 16544
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8360 Color: 414
Size: 6824 Color: 393
Size: 512 Color: 92
Size: 432 Color: 75
Size: 416 Color: 74

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12316 Color: 478
Size: 4164 Color: 343
Size: 64 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12449 Color: 480
Size: 2084 Color: 257
Size: 2011 Color: 253

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12560 Color: 484
Size: 3704 Color: 329
Size: 280 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12578 Color: 485
Size: 2454 Color: 275
Size: 1512 Color: 212

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12610 Color: 487
Size: 2904 Color: 299
Size: 1030 Color: 171

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12626 Color: 489
Size: 3270 Color: 314
Size: 648 Color: 126

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12642 Color: 490
Size: 3702 Color: 328
Size: 200 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 493
Size: 3272 Color: 316
Size: 528 Color: 97

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12756 Color: 495
Size: 3324 Color: 319
Size: 464 Color: 83

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12889 Color: 498
Size: 3047 Color: 303
Size: 608 Color: 115

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 500
Size: 1964 Color: 246
Size: 1676 Color: 224

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 502
Size: 3266 Color: 313
Size: 362 Color: 51

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 503
Size: 3044 Color: 302
Size: 564 Color: 108

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13026 Color: 504
Size: 2758 Color: 292
Size: 760 Color: 144

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 506
Size: 3176 Color: 308
Size: 304 Color: 30

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13232 Color: 510
Size: 1968 Color: 247
Size: 1344 Color: 193

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 522
Size: 3048 Color: 304
Size: 96 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 523
Size: 2748 Color: 291
Size: 360 Color: 50

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 525
Size: 2512 Color: 277
Size: 512 Color: 93

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 526
Size: 2768 Color: 294
Size: 224 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13556 Color: 527
Size: 2592 Color: 280
Size: 396 Color: 65

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 534
Size: 2684 Color: 288
Size: 60 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13804 Color: 535
Size: 1940 Color: 244
Size: 800 Color: 149

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13904 Color: 539
Size: 1424 Color: 204
Size: 1216 Color: 188

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 545
Size: 2086 Color: 258
Size: 416 Color: 71

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 547
Size: 1808 Color: 230
Size: 672 Color: 132

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14163 Color: 552
Size: 1805 Color: 229
Size: 576 Color: 111

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14182 Color: 554
Size: 1522 Color: 214
Size: 840 Color: 153

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14192 Color: 555
Size: 1344 Color: 192
Size: 1008 Color: 167

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14264 Color: 559
Size: 1864 Color: 240
Size: 416 Color: 72

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14308 Color: 561
Size: 1868 Color: 241
Size: 368 Color: 52

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 564
Size: 1184 Color: 183
Size: 1032 Color: 173

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 567
Size: 1581 Color: 217
Size: 584 Color: 112

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14390 Color: 568
Size: 1524 Color: 215
Size: 630 Color: 123

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14392 Color: 569
Size: 1470 Color: 208
Size: 682 Color: 134

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 570
Size: 1616 Color: 219
Size: 524 Color: 95

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14406 Color: 571
Size: 1862 Color: 239
Size: 276 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 572
Size: 1456 Color: 207
Size: 664 Color: 131

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14480 Color: 574
Size: 1816 Color: 231
Size: 248 Color: 11

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14534 Color: 576
Size: 1128 Color: 180
Size: 882 Color: 156

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14540 Color: 577
Size: 1378 Color: 201
Size: 626 Color: 122

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14608 Color: 582
Size: 1152 Color: 181
Size: 784 Color: 147

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14636 Color: 583
Size: 1596 Color: 218
Size: 312 Color: 32

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14716 Color: 587
Size: 1476 Color: 209
Size: 352 Color: 46

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14760 Color: 592
Size: 1056 Color: 174
Size: 728 Color: 137

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14780 Color: 593
Size: 1360 Color: 194
Size: 404 Color: 70

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 595
Size: 1360 Color: 196
Size: 384 Color: 59

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14804 Color: 596
Size: 1244 Color: 191
Size: 496 Color: 91

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14811 Color: 597
Size: 1445 Color: 205
Size: 288 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14845 Color: 598
Size: 1417 Color: 203
Size: 282 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 600
Size: 1026 Color: 170
Size: 640 Color: 124

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 11975 Color: 471
Size: 3704 Color: 330
Size: 864 Color: 154

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 488
Size: 3927 Color: 338

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 496
Size: 3690 Color: 327
Size: 96 Color: 6

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13311 Color: 515
Size: 3056 Color: 305
Size: 176 Color: 7

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 13332 Color: 516
Size: 2675 Color: 287
Size: 536 Color: 100

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 13335 Color: 517
Size: 3208 Color: 310

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13399 Color: 521
Size: 1952 Color: 245
Size: 1192 Color: 187

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13679 Color: 530
Size: 2544 Color: 279
Size: 320 Color: 37

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13748 Color: 532
Size: 2795 Color: 295

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13784 Color: 533
Size: 2759 Color: 293

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13911 Color: 541
Size: 2632 Color: 284

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 549
Size: 1556 Color: 216
Size: 878 Color: 155

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 14319 Color: 563
Size: 2224 Color: 263

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14352 Color: 565
Size: 2191 Color: 261

Bin 67: 2 of cap free
Amount of items: 5
Items: 
Size: 8304 Color: 412
Size: 2868 Color: 297
Size: 2642 Color: 285
Size: 2284 Color: 267
Size: 444 Color: 78

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 11148 Color: 450
Size: 5080 Color: 363
Size: 314 Color: 33

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 12445 Color: 479
Size: 3809 Color: 335
Size: 288 Color: 22

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 12912 Color: 501
Size: 3254 Color: 312
Size: 376 Color: 57

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 14464 Color: 573
Size: 2078 Color: 256

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 14557 Color: 580
Size: 1985 Color: 251

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14723 Color: 589
Size: 1819 Color: 233

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 14744 Color: 590
Size: 1798 Color: 228

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 594
Size: 1760 Color: 226

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 14864 Color: 599
Size: 1678 Color: 225

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 12080 Color: 473
Size: 4461 Color: 351

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 12657 Color: 491
Size: 3884 Color: 337

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 12752 Color: 494
Size: 3789 Color: 333

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 14543 Color: 578
Size: 1998 Color: 252

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 14566 Color: 581
Size: 1975 Color: 249

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 14649 Color: 584
Size: 1892 Color: 242

Bin 83: 4 of cap free
Amount of items: 5
Items: 
Size: 8328 Color: 413
Size: 6808 Color: 392
Size: 528 Color: 96
Size: 438 Color: 77
Size: 438 Color: 76

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 13512 Color: 524
Size: 3028 Color: 301

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 529
Size: 2878 Color: 298

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 14104 Color: 548
Size: 2436 Color: 274

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 14684 Color: 585
Size: 1856 Color: 238

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 14722 Color: 588
Size: 1818 Color: 232

Bin 89: 5 of cap free
Amount of items: 3
Items: 
Size: 13235 Color: 511
Size: 3272 Color: 315
Size: 32 Color: 1

Bin 90: 5 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 531
Size: 2838 Color: 296

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 13943 Color: 543
Size: 2596 Color: 281

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 14150 Color: 551
Size: 2389 Color: 272

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 10608 Color: 443
Size: 5600 Color: 377
Size: 330 Color: 41

Bin 94: 6 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 508
Size: 3368 Color: 321
Size: 28 Color: 0

Bin 95: 6 of cap free
Amount of items: 2
Items: 
Size: 13374 Color: 520
Size: 3164 Color: 307

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 14698 Color: 586
Size: 1840 Color: 234

Bin 97: 7 of cap free
Amount of items: 3
Items: 
Size: 10417 Color: 440
Size: 5784 Color: 379
Size: 336 Color: 44

Bin 98: 7 of cap free
Amount of items: 3
Items: 
Size: 11225 Color: 453
Size: 5000 Color: 362
Size: 312 Color: 31

Bin 99: 7 of cap free
Amount of items: 3
Items: 
Size: 11240 Color: 454
Size: 4401 Color: 347
Size: 896 Color: 160

Bin 100: 7 of cap free
Amount of items: 3
Items: 
Size: 11257 Color: 456
Size: 4976 Color: 361
Size: 304 Color: 29

Bin 101: 7 of cap free
Amount of items: 2
Items: 
Size: 12104 Color: 474
Size: 4433 Color: 349

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 12556 Color: 483
Size: 3413 Color: 322
Size: 568 Color: 109

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 12590 Color: 486
Size: 3947 Color: 339

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 14166 Color: 553
Size: 2371 Color: 271

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 11424 Color: 461
Size: 4812 Color: 359
Size: 300 Color: 26

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 499
Size: 3644 Color: 326

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 512
Size: 3298 Color: 318

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 13602 Color: 528
Size: 2934 Color: 300

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 13826 Color: 536
Size: 2710 Color: 290

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 13915 Color: 542
Size: 2621 Color: 283

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 546
Size: 2492 Color: 276

Bin 112: 8 of cap free
Amount of items: 2
Items: 
Size: 14240 Color: 558
Size: 2296 Color: 268

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 14276 Color: 560
Size: 2260 Color: 266

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 14310 Color: 562
Size: 2226 Color: 264

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14748 Color: 591
Size: 1788 Color: 227

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13191 Color: 509
Size: 3344 Color: 320

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 13294 Color: 514
Size: 3241 Color: 311

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 14495 Color: 575
Size: 2040 Color: 255

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 11120 Color: 449
Size: 5414 Color: 372

Bin 120: 10 of cap free
Amount of items: 3
Items: 
Size: 11285 Color: 458
Size: 4947 Color: 360
Size: 302 Color: 28

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13046 Color: 505
Size: 3488 Color: 324

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13252 Color: 513
Size: 3282 Color: 317

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 14552 Color: 579
Size: 1982 Color: 250

Bin 124: 11 of cap free
Amount of items: 7
Items: 
Size: 8280 Color: 409
Size: 1855 Color: 236
Size: 1848 Color: 235
Size: 1669 Color: 222
Size: 1657 Color: 221
Size: 768 Color: 145
Size: 456 Color: 82

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 14131 Color: 550
Size: 2402 Color: 273

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 557
Size: 2312 Color: 269

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 14363 Color: 566
Size: 2169 Color: 260

Bin 128: 13 of cap free
Amount of items: 3
Items: 
Size: 11971 Color: 470
Size: 3808 Color: 334
Size: 752 Color: 142

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 537
Size: 2695 Color: 289

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 14012 Color: 544
Size: 2518 Color: 278

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 519
Size: 3157 Color: 306

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 492
Size: 3816 Color: 336

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 14196 Color: 556
Size: 2332 Color: 270

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 13342 Color: 518
Size: 3184 Color: 309

Bin 135: 19 of cap free
Amount of items: 2
Items: 
Size: 11241 Color: 455
Size: 5284 Color: 370

Bin 136: 19 of cap free
Amount of items: 2
Items: 
Size: 12453 Color: 481
Size: 4072 Color: 341

Bin 137: 19 of cap free
Amount of items: 2
Items: 
Size: 13108 Color: 507
Size: 3417 Color: 323

Bin 138: 20 of cap free
Amount of items: 3
Items: 
Size: 10204 Color: 433
Size: 5964 Color: 383
Size: 356 Color: 49

Bin 139: 20 of cap free
Amount of items: 3
Items: 
Size: 12106 Color: 475
Size: 4162 Color: 342
Size: 256 Color: 12

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 540
Size: 2616 Color: 282

Bin 141: 23 of cap free
Amount of items: 7
Items: 
Size: 8296 Color: 411
Size: 2238 Color: 265
Size: 2195 Color: 262
Size: 2096 Color: 259
Size: 800 Color: 148
Size: 448 Color: 80
Size: 448 Color: 79

Bin 142: 23 of cap free
Amount of items: 3
Items: 
Size: 11728 Color: 467
Size: 4505 Color: 354
Size: 288 Color: 18

Bin 143: 24 of cap free
Amount of items: 3
Items: 
Size: 9512 Color: 426
Size: 6624 Color: 391
Size: 384 Color: 60

Bin 144: 24 of cap free
Amount of items: 3
Items: 
Size: 12118 Color: 476
Size: 4174 Color: 344
Size: 228 Color: 10

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 12504 Color: 482
Size: 4016 Color: 340

Bin 146: 26 of cap free
Amount of items: 2
Items: 
Size: 13874 Color: 538
Size: 2644 Color: 286

Bin 147: 34 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 468
Size: 4421 Color: 348
Size: 256 Color: 14

Bin 148: 38 of cap free
Amount of items: 3
Items: 
Size: 10772 Color: 445
Size: 5410 Color: 371
Size: 324 Color: 38

Bin 149: 40 of cap free
Amount of items: 2
Items: 
Size: 9608 Color: 427
Size: 6896 Color: 401

Bin 150: 40 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 472
Size: 4528 Color: 355

Bin 151: 40 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 497
Size: 3728 Color: 332

Bin 152: 41 of cap free
Amount of items: 4
Items: 
Size: 10740 Color: 444
Size: 5107 Color: 364
Size: 328 Color: 40
Size: 328 Color: 39

Bin 153: 42 of cap free
Amount of items: 3
Items: 
Size: 11265 Color: 457
Size: 3718 Color: 331
Size: 1519 Color: 213

Bin 154: 43 of cap free
Amount of items: 6
Items: 
Size: 8284 Color: 410
Size: 2031 Color: 254
Size: 1970 Color: 248
Size: 1912 Color: 243
Size: 1856 Color: 237
Size: 448 Color: 81

Bin 155: 46 of cap free
Amount of items: 3
Items: 
Size: 11460 Color: 462
Size: 4742 Color: 358
Size: 296 Color: 25

Bin 156: 47 of cap free
Amount of items: 2
Items: 
Size: 11336 Color: 459
Size: 5161 Color: 369

Bin 157: 52 of cap free
Amount of items: 3
Items: 
Size: 11556 Color: 465
Size: 4648 Color: 356
Size: 288 Color: 21

Bin 158: 52 of cap free
Amount of items: 3
Items: 
Size: 11892 Color: 469
Size: 4344 Color: 346
Size: 256 Color: 13

Bin 159: 54 of cap free
Amount of items: 3
Items: 
Size: 12172 Color: 477
Size: 4244 Color: 345
Size: 74 Color: 4

Bin 160: 57 of cap free
Amount of items: 2
Items: 
Size: 9407 Color: 423
Size: 7080 Color: 402

Bin 161: 59 of cap free
Amount of items: 2
Items: 
Size: 10357 Color: 436
Size: 6128 Color: 386

Bin 162: 60 of cap free
Amount of items: 3
Items: 
Size: 9200 Color: 420
Size: 6892 Color: 398
Size: 392 Color: 63

Bin 163: 61 of cap free
Amount of items: 3
Items: 
Size: 9196 Color: 419
Size: 6891 Color: 397
Size: 396 Color: 64

Bin 164: 67 of cap free
Amount of items: 2
Items: 
Size: 10353 Color: 435
Size: 6124 Color: 385

Bin 165: 72 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 418
Size: 6888 Color: 396
Size: 396 Color: 66

Bin 166: 80 of cap free
Amount of items: 23
Items: 
Size: 944 Color: 162
Size: 896 Color: 161
Size: 890 Color: 159
Size: 890 Color: 158
Size: 886 Color: 157
Size: 832 Color: 152
Size: 832 Color: 151
Size: 824 Color: 150
Size: 782 Color: 146
Size: 752 Color: 143
Size: 736 Color: 141
Size: 736 Color: 140
Size: 736 Color: 139
Size: 608 Color: 117
Size: 608 Color: 116
Size: 600 Color: 114
Size: 592 Color: 113
Size: 576 Color: 110
Size: 558 Color: 107
Size: 550 Color: 106
Size: 548 Color: 105
Size: 544 Color: 104
Size: 544 Color: 103

Bin 167: 80 of cap free
Amount of items: 3
Items: 
Size: 10456 Color: 441
Size: 5676 Color: 378
Size: 332 Color: 43

Bin 168: 82 of cap free
Amount of items: 3
Items: 
Size: 9868 Color: 429
Size: 6222 Color: 388
Size: 372 Color: 56

Bin 169: 84 of cap free
Amount of items: 3
Items: 
Size: 11672 Color: 466
Size: 4500 Color: 353
Size: 288 Color: 20

Bin 170: 96 of cap free
Amount of items: 3
Items: 
Size: 10552 Color: 442
Size: 5564 Color: 376
Size: 332 Color: 42

Bin 171: 108 of cap free
Amount of items: 9
Items: 
Size: 8276 Color: 406
Size: 1240 Color: 190
Size: 1224 Color: 189
Size: 1192 Color: 186
Size: 1188 Color: 185
Size: 1188 Color: 184
Size: 1168 Color: 182
Size: 480 Color: 88
Size: 480 Color: 87

Bin 172: 136 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 425
Size: 6604 Color: 390
Size: 384 Color: 61

Bin 173: 140 of cap free
Amount of items: 4
Items: 
Size: 8392 Color: 415
Size: 7194 Color: 403
Size: 416 Color: 73
Size: 402 Color: 69

Bin 174: 151 of cap free
Amount of items: 3
Items: 
Size: 11197 Color: 452
Size: 3524 Color: 325
Size: 1672 Color: 223

Bin 175: 159 of cap free
Amount of items: 3
Items: 
Size: 11353 Color: 460
Size: 4730 Color: 357
Size: 302 Color: 27

Bin 176: 163 of cap free
Amount of items: 3
Items: 
Size: 9409 Color: 424
Size: 6588 Color: 389
Size: 384 Color: 62

Bin 177: 173 of cap free
Amount of items: 3
Items: 
Size: 10054 Color: 432
Size: 5949 Color: 382
Size: 368 Color: 53

Bin 178: 174 of cap free
Amount of items: 3
Items: 
Size: 9086 Color: 417
Size: 6884 Color: 395
Size: 400 Color: 67

Bin 179: 179 of cap free
Amount of items: 3
Items: 
Size: 10050 Color: 431
Size: 5947 Color: 381
Size: 368 Color: 54

Bin 180: 190 of cap free
Amount of items: 2
Items: 
Size: 11193 Color: 451
Size: 5161 Color: 368

Bin 181: 197 of cap free
Amount of items: 2
Items: 
Size: 10351 Color: 434
Size: 5996 Color: 384

Bin 182: 197 of cap free
Amount of items: 3
Items: 
Size: 10870 Color: 448
Size: 5157 Color: 367
Size: 320 Color: 34

Bin 183: 202 of cap free
Amount of items: 3
Items: 
Size: 9740 Color: 428
Size: 6218 Color: 387
Size: 384 Color: 58

Bin 184: 210 of cap free
Amount of items: 3
Items: 
Size: 9078 Color: 416
Size: 6856 Color: 394
Size: 400 Color: 68

Bin 185: 229 of cap free
Amount of items: 3
Items: 
Size: 10862 Color: 447
Size: 5133 Color: 366
Size: 320 Color: 35

Bin 186: 233 of cap free
Amount of items: 11
Items: 
Size: 8273 Color: 404
Size: 1022 Color: 169
Size: 1020 Color: 168
Size: 992 Color: 166
Size: 960 Color: 165
Size: 960 Color: 164
Size: 944 Color: 163
Size: 540 Color: 102
Size: 538 Color: 101
Size: 534 Color: 99
Size: 528 Color: 98

Bin 187: 239 of cap free
Amount of items: 3
Items: 
Size: 11550 Color: 464
Size: 4467 Color: 352
Size: 288 Color: 23

Bin 188: 245 of cap free
Amount of items: 3
Items: 
Size: 10858 Color: 446
Size: 5121 Color: 365
Size: 320 Color: 36

Bin 189: 257 of cap free
Amount of items: 3
Items: 
Size: 11538 Color: 463
Size: 4457 Color: 350
Size: 292 Color: 24

Bin 190: 262 of cap free
Amount of items: 2
Items: 
Size: 9388 Color: 422
Size: 6894 Color: 400

Bin 191: 283 of cap free
Amount of items: 3
Items: 
Size: 10401 Color: 439
Size: 5524 Color: 375
Size: 336 Color: 45

Bin 192: 295 of cap free
Amount of items: 2
Items: 
Size: 9356 Color: 421
Size: 6893 Color: 399

Bin 193: 301 of cap free
Amount of items: 3
Items: 
Size: 10385 Color: 438
Size: 5506 Color: 374
Size: 352 Color: 47

Bin 194: 306 of cap free
Amount of items: 7
Items: 
Size: 8278 Color: 408
Size: 1650 Color: 220
Size: 1500 Color: 211
Size: 1496 Color: 210
Size: 1452 Color: 206
Size: 1390 Color: 202
Size: 472 Color: 84

Bin 195: 312 of cap free
Amount of items: 10
Items: 
Size: 8274 Color: 405
Size: 1112 Color: 179
Size: 1088 Color: 178
Size: 1080 Color: 177
Size: 1080 Color: 176
Size: 1072 Color: 175
Size: 1030 Color: 172
Size: 512 Color: 94
Size: 496 Color: 90
Size: 488 Color: 89

Bin 196: 336 of cap free
Amount of items: 3
Items: 
Size: 10368 Color: 437
Size: 5488 Color: 373
Size: 352 Color: 48

Bin 197: 342 of cap free
Amount of items: 3
Items: 
Size: 9968 Color: 430
Size: 5864 Color: 380
Size: 370 Color: 55

Bin 198: 447 of cap free
Amount of items: 8
Items: 
Size: 8277 Color: 407
Size: 1376 Color: 200
Size: 1376 Color: 199
Size: 1376 Color: 198
Size: 1376 Color: 197
Size: 1360 Color: 195
Size: 480 Color: 86
Size: 476 Color: 85

Bin 199: 7956 of cap free
Amount of items: 13
Items: 
Size: 736 Color: 138
Size: 724 Color: 136
Size: 704 Color: 135
Size: 682 Color: 133
Size: 656 Color: 130
Size: 656 Color: 129
Size: 652 Color: 128
Size: 652 Color: 127
Size: 646 Color: 125
Size: 624 Color: 121
Size: 624 Color: 120
Size: 624 Color: 119
Size: 608 Color: 118

Total size: 3275712
Total free space: 16544

