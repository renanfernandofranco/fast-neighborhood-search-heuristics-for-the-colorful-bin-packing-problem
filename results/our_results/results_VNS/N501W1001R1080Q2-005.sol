Capicity Bin: 1001
Lower Bound: 232

Bins used: 235
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 345 Color: 0

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 662 Color: 0
Size: 339 Color: 1

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 0
Size: 439 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 616 Color: 0
Size: 200 Color: 1
Size: 185 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 737 Color: 0
Size: 149 Color: 0
Size: 115 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 780 Color: 1
Size: 114 Color: 0
Size: 107 Color: 1

Bin 7: 0 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 1
Size: 419 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 626 Color: 0
Size: 202 Color: 1
Size: 173 Color: 0

Bin 9: 0 of cap free
Amount of items: 2
Items: 
Size: 690 Color: 1
Size: 311 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 281 Color: 1
Size: 226 Color: 1

Bin 11: 0 of cap free
Amount of items: 2
Items: 
Size: 583 Color: 0
Size: 418 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 508 Color: 0
Size: 373 Color: 1
Size: 120 Color: 1

Bin 13: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 0
Size: 316 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 555 Color: 1
Size: 243 Color: 0
Size: 203 Color: 1

Bin 15: 0 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 1
Size: 464 Color: 0

Bin 16: 0 of cap free
Amount of items: 2
Items: 
Size: 513 Color: 0
Size: 488 Color: 1

Bin 17: 0 of cap free
Amount of items: 2
Items: 
Size: 717 Color: 1
Size: 284 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 578 Color: 1
Size: 242 Color: 0
Size: 181 Color: 0

Bin 19: 0 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 401 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 670 Color: 0
Size: 177 Color: 1
Size: 154 Color: 0

Bin 21: 0 of cap free
Amount of items: 2
Items: 
Size: 740 Color: 0
Size: 261 Color: 1

Bin 22: 0 of cap free
Amount of items: 2
Items: 
Size: 731 Color: 1
Size: 270 Color: 0

Bin 23: 0 of cap free
Amount of items: 2
Items: 
Size: 681 Color: 0
Size: 320 Color: 1

Bin 24: 0 of cap free
Amount of items: 2
Items: 
Size: 597 Color: 1
Size: 404 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 617 Color: 0
Size: 217 Color: 1
Size: 167 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 1
Size: 297 Color: 1
Size: 133 Color: 0

Bin 27: 0 of cap free
Amount of items: 2
Items: 
Size: 726 Color: 0
Size: 275 Color: 1

Bin 28: 0 of cap free
Amount of items: 2
Items: 
Size: 755 Color: 0
Size: 246 Color: 1

Bin 29: 0 of cap free
Amount of items: 2
Items: 
Size: 799 Color: 1
Size: 202 Color: 0

Bin 30: 0 of cap free
Amount of items: 2
Items: 
Size: 777 Color: 0
Size: 224 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 571 Color: 1
Size: 218 Color: 0
Size: 212 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 758 Color: 0
Size: 129 Color: 0
Size: 114 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 324 Color: 1
Size: 182 Color: 0

Bin 34: 0 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 0
Size: 417 Color: 1

Bin 35: 0 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 213 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 1
Size: 216 Color: 0
Size: 200 Color: 1

Bin 37: 0 of cap free
Amount of items: 2
Items: 
Size: 685 Color: 1
Size: 316 Color: 0

Bin 38: 0 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 388 Color: 1

Bin 39: 0 of cap free
Amount of items: 2
Items: 
Size: 782 Color: 0
Size: 219 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 575 Color: 1
Size: 215 Color: 0
Size: 211 Color: 1

Bin 41: 0 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 1
Size: 276 Color: 0

Bin 42: 0 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 386 Color: 0

Bin 43: 0 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 411 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 724 Color: 0
Size: 141 Color: 1
Size: 136 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 560 Color: 1
Size: 285 Color: 0
Size: 156 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 351 Color: 1
Size: 171 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 351 Color: 1
Size: 178 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 348 Color: 1
Size: 193 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 348 Color: 1
Size: 182 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 283 Color: 1
Size: 260 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 0
Size: 282 Color: 1
Size: 266 Color: 1

Bin 52: 0 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 465 Color: 1

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 287 Color: 1
Size: 242 Color: 0

Bin 54: 0 of cap free
Amount of items: 2
Items: 
Size: 502 Color: 0
Size: 499 Color: 1

Bin 55: 0 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 0
Size: 461 Color: 1

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 551 Color: 0
Size: 450 Color: 1

Bin 57: 0 of cap free
Amount of items: 2
Items: 
Size: 572 Color: 0
Size: 429 Color: 1

Bin 58: 0 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 1
Size: 394 Color: 0

Bin 59: 0 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 392 Color: 1

Bin 60: 0 of cap free
Amount of items: 2
Items: 
Size: 620 Color: 0
Size: 381 Color: 1

Bin 61: 0 of cap free
Amount of items: 2
Items: 
Size: 624 Color: 0
Size: 377 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 629 Color: 1
Size: 194 Color: 1
Size: 178 Color: 0

Bin 63: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 0

Bin 64: 0 of cap free
Amount of items: 2
Items: 
Size: 631 Color: 1
Size: 370 Color: 0

Bin 65: 0 of cap free
Amount of items: 2
Items: 
Size: 634 Color: 0
Size: 367 Color: 1

Bin 66: 0 of cap free
Amount of items: 2
Items: 
Size: 636 Color: 1
Size: 365 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 640 Color: 1
Size: 181 Color: 0
Size: 180 Color: 0

Bin 68: 0 of cap free
Amount of items: 2
Items: 
Size: 642 Color: 1
Size: 359 Color: 0

Bin 69: 0 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 356 Color: 0

Bin 70: 0 of cap free
Amount of items: 2
Items: 
Size: 649 Color: 1
Size: 352 Color: 0

Bin 71: 0 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 1
Size: 345 Color: 0

Bin 72: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 1
Size: 343 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 677 Color: 1
Size: 166 Color: 0
Size: 158 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 678 Color: 0
Size: 170 Color: 1
Size: 153 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 684 Color: 1
Size: 161 Color: 0
Size: 156 Color: 0

Bin 76: 0 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 1
Size: 313 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 689 Color: 0
Size: 160 Color: 1
Size: 152 Color: 1

Bin 78: 0 of cap free
Amount of items: 2
Items: 
Size: 698 Color: 1
Size: 303 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 0
Size: 148 Color: 1
Size: 148 Color: 1

Bin 80: 0 of cap free
Amount of items: 2
Items: 
Size: 723 Color: 0
Size: 278 Color: 1

Bin 81: 0 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 0
Size: 274 Color: 1

Bin 82: 0 of cap free
Amount of items: 2
Items: 
Size: 730 Color: 1
Size: 271 Color: 0

Bin 83: 0 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 0
Size: 263 Color: 1

Bin 84: 0 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 1
Size: 228 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 744 Color: 1
Size: 143 Color: 0
Size: 114 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 752 Color: 0
Size: 136 Color: 1
Size: 113 Color: 1

Bin 87: 0 of cap free
Amount of items: 2
Items: 
Size: 762 Color: 1
Size: 239 Color: 0

Bin 88: 0 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 0
Size: 232 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 778 Color: 1
Size: 113 Color: 0
Size: 110 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 788 Color: 1
Size: 108 Color: 0
Size: 105 Color: 1

Bin 91: 1 of cap free
Amount of items: 2
Items: 
Size: 644 Color: 0
Size: 356 Color: 1

Bin 92: 1 of cap free
Amount of items: 2
Items: 
Size: 505 Color: 1
Size: 495 Color: 0

Bin 93: 1 of cap free
Amount of items: 2
Items: 
Size: 667 Color: 1
Size: 333 Color: 0

Bin 94: 1 of cap free
Amount of items: 2
Items: 
Size: 615 Color: 1
Size: 385 Color: 0

Bin 95: 1 of cap free
Amount of items: 2
Items: 
Size: 621 Color: 1
Size: 379 Color: 0

Bin 96: 1 of cap free
Amount of items: 2
Items: 
Size: 609 Color: 0
Size: 391 Color: 1

Bin 97: 1 of cap free
Amount of items: 2
Items: 
Size: 503 Color: 0
Size: 497 Color: 1

Bin 98: 1 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 1
Size: 320 Color: 0

Bin 99: 1 of cap free
Amount of items: 2
Items: 
Size: 738 Color: 1
Size: 262 Color: 0

Bin 100: 1 of cap free
Amount of items: 2
Items: 
Size: 739 Color: 0
Size: 261 Color: 1

Bin 101: 1 of cap free
Amount of items: 2
Items: 
Size: 751 Color: 1
Size: 249 Color: 0

Bin 102: 1 of cap free
Amount of items: 2
Items: 
Size: 618 Color: 0
Size: 382 Color: 1

Bin 103: 1 of cap free
Amount of items: 2
Items: 
Size: 639 Color: 0
Size: 361 Color: 1

Bin 104: 1 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 0
Size: 359 Color: 1

Bin 105: 1 of cap free
Amount of items: 2
Items: 
Size: 746 Color: 1
Size: 254 Color: 0

Bin 106: 1 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 227 Color: 1

Bin 107: 1 of cap free
Amount of items: 2
Items: 
Size: 518 Color: 0
Size: 482 Color: 1

Bin 108: 1 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 251 Color: 0

Bin 109: 1 of cap free
Amount of items: 3
Items: 
Size: 639 Color: 1
Size: 210 Color: 1
Size: 151 Color: 0

Bin 110: 1 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 488 Color: 0

Bin 111: 1 of cap free
Amount of items: 2
Items: 
Size: 779 Color: 0
Size: 221 Color: 1

Bin 112: 1 of cap free
Amount of items: 2
Items: 
Size: 556 Color: 0
Size: 444 Color: 1

Bin 113: 1 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 387 Color: 1

Bin 114: 2 of cap free
Amount of items: 2
Items: 
Size: 689 Color: 0
Size: 310 Color: 1

Bin 115: 2 of cap free
Amount of items: 2
Items: 
Size: 531 Color: 1
Size: 468 Color: 0

Bin 116: 2 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 0
Size: 361 Color: 1

Bin 117: 2 of cap free
Amount of items: 2
Items: 
Size: 540 Color: 0
Size: 459 Color: 1

Bin 118: 2 of cap free
Amount of items: 2
Items: 
Size: 595 Color: 0
Size: 404 Color: 1

Bin 119: 2 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 319 Color: 1

Bin 120: 2 of cap free
Amount of items: 2
Items: 
Size: 682 Color: 1
Size: 317 Color: 0

Bin 121: 2 of cap free
Amount of items: 2
Items: 
Size: 715 Color: 0
Size: 284 Color: 1

Bin 122: 2 of cap free
Amount of items: 3
Items: 
Size: 705 Color: 0
Size: 162 Color: 1
Size: 132 Color: 1

Bin 123: 2 of cap free
Amount of items: 2
Items: 
Size: 592 Color: 0
Size: 407 Color: 1

Bin 124: 2 of cap free
Amount of items: 2
Items: 
Size: 591 Color: 1
Size: 408 Color: 0

Bin 125: 2 of cap free
Amount of items: 2
Items: 
Size: 793 Color: 1
Size: 206 Color: 0

Bin 126: 2 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 300 Color: 1

Bin 127: 2 of cap free
Amount of items: 2
Items: 
Size: 638 Color: 0
Size: 361 Color: 1

Bin 128: 2 of cap free
Amount of items: 2
Items: 
Size: 564 Color: 0
Size: 435 Color: 1

Bin 129: 2 of cap free
Amount of items: 2
Items: 
Size: 695 Color: 0
Size: 304 Color: 1

Bin 130: 3 of cap free
Amount of items: 2
Items: 
Size: 727 Color: 0
Size: 271 Color: 1

Bin 131: 3 of cap free
Amount of items: 2
Items: 
Size: 798 Color: 1
Size: 200 Color: 0

Bin 132: 3 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 382 Color: 1

Bin 133: 3 of cap free
Amount of items: 3
Items: 
Size: 694 Color: 0
Size: 184 Color: 0
Size: 120 Color: 1

Bin 134: 3 of cap free
Amount of items: 2
Items: 
Size: 694 Color: 0
Size: 304 Color: 1

Bin 135: 3 of cap free
Amount of items: 2
Items: 
Size: 745 Color: 1
Size: 253 Color: 0

Bin 136: 3 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 341 Color: 1

Bin 137: 3 of cap free
Amount of items: 2
Items: 
Size: 699 Color: 0
Size: 299 Color: 1

Bin 138: 3 of cap free
Amount of items: 2
Items: 
Size: 737 Color: 0
Size: 261 Color: 1

Bin 139: 3 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 1
Size: 245 Color: 0

Bin 140: 3 of cap free
Amount of items: 2
Items: 
Size: 766 Color: 1
Size: 232 Color: 0

Bin 141: 3 of cap free
Amount of items: 2
Items: 
Size: 657 Color: 0
Size: 341 Color: 1

Bin 142: 3 of cap free
Amount of items: 2
Items: 
Size: 562 Color: 1
Size: 436 Color: 0

Bin 143: 3 of cap free
Amount of items: 2
Items: 
Size: 705 Color: 1
Size: 293 Color: 0

Bin 144: 3 of cap free
Amount of items: 2
Items: 
Size: 527 Color: 0
Size: 471 Color: 1

Bin 145: 3 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 0
Size: 351 Color: 1
Size: 184 Color: 0

Bin 146: 3 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 0
Size: 417 Color: 1

Bin 147: 3 of cap free
Amount of items: 2
Items: 
Size: 684 Color: 0
Size: 314 Color: 1

Bin 148: 4 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 1
Size: 482 Color: 0

Bin 149: 4 of cap free
Amount of items: 2
Items: 
Size: 534 Color: 1
Size: 463 Color: 0

Bin 150: 4 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 424 Color: 0

Bin 151: 4 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 481 Color: 1

Bin 152: 4 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 0
Size: 488 Color: 1

Bin 153: 4 of cap free
Amount of items: 2
Items: 
Size: 659 Color: 1
Size: 338 Color: 0

Bin 154: 4 of cap free
Amount of items: 2
Items: 
Size: 704 Color: 1
Size: 293 Color: 0

Bin 155: 4 of cap free
Amount of items: 2
Items: 
Size: 735 Color: 1
Size: 262 Color: 0

Bin 156: 4 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 1
Size: 407 Color: 0

Bin 157: 4 of cap free
Amount of items: 2
Items: 
Size: 567 Color: 0
Size: 430 Color: 1

Bin 158: 4 of cap free
Amount of items: 2
Items: 
Size: 539 Color: 0
Size: 458 Color: 1

Bin 159: 4 of cap free
Amount of items: 2
Items: 
Size: 673 Color: 1
Size: 324 Color: 0

Bin 160: 4 of cap free
Amount of items: 2
Items: 
Size: 775 Color: 1
Size: 222 Color: 0

Bin 161: 5 of cap free
Amount of items: 2
Items: 
Size: 643 Color: 1
Size: 353 Color: 0

Bin 162: 5 of cap free
Amount of items: 2
Items: 
Size: 687 Color: 1
Size: 309 Color: 0

Bin 163: 5 of cap free
Amount of items: 2
Items: 
Size: 773 Color: 0
Size: 223 Color: 1

Bin 164: 5 of cap free
Amount of items: 2
Items: 
Size: 625 Color: 0
Size: 371 Color: 1

Bin 165: 5 of cap free
Amount of items: 2
Items: 
Size: 600 Color: 0
Size: 396 Color: 1

Bin 166: 5 of cap free
Amount of items: 2
Items: 
Size: 764 Color: 1
Size: 232 Color: 0

Bin 167: 5 of cap free
Amount of items: 2
Items: 
Size: 797 Color: 1
Size: 199 Color: 0

Bin 168: 6 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 1
Size: 292 Color: 0

Bin 169: 6 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 242 Color: 1

Bin 170: 6 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 479 Color: 1

Bin 171: 6 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 0
Size: 435 Color: 1

Bin 172: 6 of cap free
Amount of items: 2
Items: 
Size: 632 Color: 1
Size: 363 Color: 0

Bin 173: 6 of cap free
Amount of items: 2
Items: 
Size: 733 Color: 1
Size: 262 Color: 0

Bin 174: 6 of cap free
Amount of items: 2
Items: 
Size: 763 Color: 1
Size: 232 Color: 0

Bin 175: 7 of cap free
Amount of items: 2
Items: 
Size: 610 Color: 1
Size: 384 Color: 0

Bin 176: 7 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 434 Color: 0

Bin 177: 7 of cap free
Amount of items: 2
Items: 
Size: 749 Color: 1
Size: 245 Color: 0

Bin 178: 7 of cap free
Amount of items: 2
Items: 
Size: 590 Color: 0
Size: 404 Color: 1

Bin 179: 7 of cap free
Amount of items: 2
Items: 
Size: 795 Color: 0
Size: 199 Color: 1

Bin 180: 7 of cap free
Amount of items: 2
Items: 
Size: 573 Color: 1
Size: 421 Color: 0

Bin 181: 7 of cap free
Amount of items: 2
Items: 
Size: 680 Color: 0
Size: 314 Color: 1

Bin 182: 7 of cap free
Amount of items: 2
Items: 
Size: 787 Color: 0
Size: 207 Color: 1

Bin 183: 8 of cap free
Amount of items: 2
Items: 
Size: 725 Color: 0
Size: 268 Color: 1

Bin 184: 8 of cap free
Amount of items: 2
Items: 
Size: 587 Color: 1
Size: 406 Color: 0

Bin 185: 8 of cap free
Amount of items: 2
Items: 
Size: 656 Color: 0
Size: 337 Color: 1

Bin 186: 8 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 328 Color: 1

Bin 187: 8 of cap free
Amount of items: 2
Items: 
Size: 497 Color: 0
Size: 496 Color: 1

Bin 188: 9 of cap free
Amount of items: 2
Items: 
Size: 753 Color: 0
Size: 239 Color: 1

Bin 189: 9 of cap free
Amount of items: 2
Items: 
Size: 732 Color: 1
Size: 260 Color: 0

Bin 190: 9 of cap free
Amount of items: 2
Items: 
Size: 560 Color: 1
Size: 432 Color: 0

Bin 191: 10 of cap free
Amount of items: 2
Items: 
Size: 693 Color: 0
Size: 298 Color: 1

Bin 192: 10 of cap free
Amount of items: 2
Items: 
Size: 703 Color: 1
Size: 288 Color: 0

Bin 193: 10 of cap free
Amount of items: 2
Items: 
Size: 509 Color: 1
Size: 482 Color: 0

Bin 194: 10 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 1
Size: 432 Color: 0

Bin 195: 10 of cap free
Amount of items: 2
Items: 
Size: 669 Color: 1
Size: 322 Color: 0

Bin 196: 11 of cap free
Amount of items: 2
Items: 
Size: 791 Color: 1
Size: 199 Color: 0

Bin 197: 12 of cap free
Amount of items: 2
Items: 
Size: 623 Color: 0
Size: 366 Color: 1

Bin 198: 12 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 1
Size: 481 Color: 0

Bin 199: 14 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 1
Size: 479 Color: 0

Bin 200: 14 of cap free
Amount of items: 2
Items: 
Size: 622 Color: 0
Size: 365 Color: 1

Bin 201: 15 of cap free
Amount of items: 2
Items: 
Size: 742 Color: 1
Size: 244 Color: 0

Bin 202: 15 of cap free
Amount of items: 2
Items: 
Size: 516 Color: 0
Size: 470 Color: 1

Bin 203: 16 of cap free
Amount of items: 2
Items: 
Size: 603 Color: 1
Size: 382 Color: 0

Bin 204: 19 of cap free
Amount of items: 2
Items: 
Size: 744 Color: 0
Size: 238 Color: 1

Bin 205: 19 of cap free
Amount of items: 2
Items: 
Size: 523 Color: 1
Size: 459 Color: 0

Bin 206: 19 of cap free
Amount of items: 2
Items: 
Size: 697 Color: 1
Size: 285 Color: 0

Bin 207: 19 of cap free
Amount of items: 2
Items: 
Size: 559 Color: 0
Size: 423 Color: 1

Bin 208: 21 of cap free
Amount of items: 2
Items: 
Size: 688 Color: 0
Size: 292 Color: 1

Bin 209: 27 of cap free
Amount of items: 2
Items: 
Size: 616 Color: 0
Size: 358 Color: 1

Bin 210: 27 of cap free
Amount of items: 2
Items: 
Size: 508 Color: 0
Size: 466 Color: 1

Bin 211: 31 of cap free
Amount of items: 2
Items: 
Size: 788 Color: 1
Size: 182 Color: 0

Bin 212: 31 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 305 Color: 0

Bin 213: 32 of cap free
Amount of items: 2
Items: 
Size: 613 Color: 0
Size: 356 Color: 1

Bin 214: 33 of cap free
Amount of items: 2
Items: 
Size: 547 Color: 0
Size: 421 Color: 1

Bin 215: 34 of cap free
Amount of items: 2
Items: 
Size: 586 Color: 1
Size: 381 Color: 0

Bin 216: 38 of cap free
Amount of items: 2
Items: 
Size: 585 Color: 1
Size: 378 Color: 0

Bin 217: 40 of cap free
Amount of items: 2
Items: 
Size: 611 Color: 0
Size: 350 Color: 1

Bin 218: 40 of cap free
Amount of items: 2
Items: 
Size: 584 Color: 1
Size: 377 Color: 0

Bin 219: 43 of cap free
Amount of items: 2
Items: 
Size: 729 Color: 1
Size: 229 Color: 0

Bin 220: 48 of cap free
Amount of items: 2
Items: 
Size: 537 Color: 0
Size: 416 Color: 1

Bin 221: 51 of cap free
Amount of items: 2
Items: 
Size: 536 Color: 0
Size: 414 Color: 1

Bin 222: 53 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 1
Size: 283 Color: 0

Bin 223: 53 of cap free
Amount of items: 2
Items: 
Size: 770 Color: 0
Size: 178 Color: 1

Bin 224: 56 of cap free
Amount of items: 2
Items: 
Size: 571 Color: 1
Size: 374 Color: 0

Bin 225: 59 of cap free
Amount of items: 2
Items: 
Size: 769 Color: 0
Size: 173 Color: 1

Bin 226: 101 of cap free
Amount of items: 2
Items: 
Size: 507 Color: 0
Size: 393 Color: 1

Bin 227: 219 of cap free
Amount of items: 1
Items: 
Size: 782 Color: 1

Bin 228: 227 of cap free
Amount of items: 1
Items: 
Size: 774 Color: 1

Bin 229: 237 of cap free
Amount of items: 1
Items: 
Size: 764 Color: 0

Bin 230: 258 of cap free
Amount of items: 1
Items: 
Size: 743 Color: 0

Bin 231: 278 of cap free
Amount of items: 1
Items: 
Size: 723 Color: 0

Bin 232: 278 of cap free
Amount of items: 1
Items: 
Size: 723 Color: 1

Bin 233: 279 of cap free
Amount of items: 1
Items: 
Size: 722 Color: 0

Bin 234: 280 of cap free
Amount of items: 1
Items: 
Size: 721 Color: 1

Bin 235: 351 of cap free
Amount of items: 1
Items: 
Size: 650 Color: 1

Total size: 231414
Total free space: 3821

