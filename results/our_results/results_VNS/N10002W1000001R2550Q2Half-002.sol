Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 384395 Color: 1
Size: 314162 Color: 1
Size: 301444 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 372545 Color: 1
Size: 335030 Color: 1
Size: 292426 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 402736 Color: 1
Size: 329009 Color: 1
Size: 268256 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 422359 Color: 1
Size: 314405 Color: 1
Size: 263237 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 392779 Color: 1
Size: 335546 Color: 1
Size: 271676 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 390577 Color: 1
Size: 336757 Color: 1
Size: 272667 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 451720 Color: 1
Size: 286354 Color: 1
Size: 261927 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 440255 Color: 1
Size: 308510 Color: 1
Size: 251236 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 362139 Color: 1
Size: 334519 Color: 1
Size: 303343 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 456967 Color: 1
Size: 291529 Color: 1
Size: 251505 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 358408 Color: 1
Size: 343381 Color: 1
Size: 298212 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 361924 Color: 1
Size: 340354 Color: 1
Size: 297723 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 466876 Color: 1
Size: 266886 Color: 0
Size: 266239 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 478097 Color: 1
Size: 270558 Color: 1
Size: 251346 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 464107 Color: 1
Size: 281283 Color: 1
Size: 254611 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 435596 Color: 1
Size: 302051 Color: 1
Size: 262354 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 456908 Color: 1
Size: 292271 Color: 1
Size: 250822 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 460526 Color: 1
Size: 284558 Color: 1
Size: 254917 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 416132 Color: 1
Size: 307406 Color: 1
Size: 276463 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 469560 Color: 1
Size: 270398 Color: 1
Size: 260043 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 391491 Color: 1
Size: 351253 Color: 1
Size: 257257 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 488255 Color: 1
Size: 256140 Color: 0
Size: 255606 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 372969 Color: 1
Size: 366682 Color: 1
Size: 260350 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 419711 Color: 1
Size: 302435 Color: 1
Size: 277855 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 375063 Color: 1
Size: 344823 Color: 1
Size: 280115 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439786 Color: 1
Size: 290403 Color: 1
Size: 269812 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 425825 Color: 1
Size: 313630 Color: 1
Size: 260546 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 400276 Color: 1
Size: 318263 Color: 1
Size: 281462 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 458000 Color: 1
Size: 278983 Color: 1
Size: 263018 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 435566 Color: 1
Size: 301859 Color: 1
Size: 262576 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 450533 Color: 1
Size: 296447 Color: 1
Size: 253021 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 439694 Color: 1
Size: 301963 Color: 1
Size: 258344 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 412310 Color: 1
Size: 302903 Color: 0
Size: 284788 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 375584 Color: 1
Size: 337911 Color: 1
Size: 286506 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 387113 Color: 1
Size: 358456 Color: 1
Size: 254432 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 363686 Color: 1
Size: 344270 Color: 1
Size: 292045 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 494311 Color: 1
Size: 255665 Color: 1
Size: 250025 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 499195 Color: 1
Size: 250661 Color: 1
Size: 250145 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 483035 Color: 1
Size: 265135 Color: 1
Size: 251831 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 490121 Color: 1
Size: 256348 Color: 1
Size: 253532 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 494778 Color: 1
Size: 254874 Color: 1
Size: 250349 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 420887 Color: 1
Size: 311096 Color: 1
Size: 268018 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 377518 Color: 1
Size: 343240 Color: 1
Size: 279243 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 370092 Color: 1
Size: 350330 Color: 1
Size: 279579 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 478938 Color: 1
Size: 264610 Color: 1
Size: 256453 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 371871 Color: 1
Size: 342680 Color: 1
Size: 285450 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 373499 Color: 1
Size: 316034 Color: 0
Size: 310468 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 425313 Color: 1
Size: 321716 Color: 1
Size: 252972 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 410696 Color: 1
Size: 300625 Color: 1
Size: 288680 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 480584 Color: 1
Size: 264630 Color: 1
Size: 254787 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 488544 Color: 1
Size: 259834 Color: 1
Size: 251623 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 355842 Color: 1
Size: 327451 Color: 1
Size: 316708 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 398359 Color: 1
Size: 337083 Color: 1
Size: 264559 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 413302 Color: 1
Size: 304788 Color: 1
Size: 281911 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 397004 Color: 1
Size: 316336 Color: 1
Size: 286661 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 482531 Color: 1
Size: 265152 Color: 1
Size: 252318 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 371926 Color: 1
Size: 337657 Color: 1
Size: 290418 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 418726 Color: 1
Size: 313895 Color: 1
Size: 267380 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 467823 Color: 1
Size: 276733 Color: 1
Size: 255445 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 405454 Color: 1
Size: 332518 Color: 1
Size: 262029 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 356825 Color: 1
Size: 348197 Color: 1
Size: 294979 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 405088 Color: 1
Size: 333989 Color: 1
Size: 260924 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 394755 Color: 1
Size: 346351 Color: 1
Size: 258895 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 454716 Color: 1
Size: 294225 Color: 1
Size: 251060 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 467613 Color: 1
Size: 276672 Color: 1
Size: 255716 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 417275 Color: 1
Size: 317264 Color: 1
Size: 265462 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 417884 Color: 1
Size: 330568 Color: 1
Size: 251549 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 348394 Color: 1
Size: 329648 Color: 1
Size: 321959 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 387264 Color: 1
Size: 351660 Color: 1
Size: 261077 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 439827 Color: 1
Size: 292993 Color: 1
Size: 267181 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 488435 Color: 1
Size: 261503 Color: 1
Size: 250063 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 411089 Color: 1
Size: 328847 Color: 1
Size: 260065 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 461803 Color: 1
Size: 277860 Color: 1
Size: 260338 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 401773 Color: 1
Size: 323732 Color: 1
Size: 274496 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 367541 Color: 1
Size: 323358 Color: 0
Size: 309102 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 492151 Color: 1
Size: 255503 Color: 1
Size: 252347 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 443299 Color: 1
Size: 297566 Color: 1
Size: 259136 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 436299 Color: 1
Size: 296419 Color: 1
Size: 267283 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 499876 Color: 1
Size: 250094 Color: 1
Size: 250031 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 475659 Color: 1
Size: 271365 Color: 1
Size: 252977 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 381214 Color: 1
Size: 348766 Color: 1
Size: 270021 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 428077 Color: 1
Size: 309354 Color: 1
Size: 262570 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 360950 Color: 1
Size: 325822 Color: 1
Size: 313229 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 443792 Color: 1
Size: 295182 Color: 1
Size: 261027 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 409323 Color: 1
Size: 304884 Color: 1
Size: 285794 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 401170 Color: 1
Size: 300975 Color: 0
Size: 297856 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 437963 Color: 1
Size: 301287 Color: 1
Size: 260751 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 421079 Color: 1
Size: 322637 Color: 1
Size: 256285 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 342023 Color: 1
Size: 333844 Color: 1
Size: 324134 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 371376 Color: 1
Size: 365606 Color: 1
Size: 263019 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 414629 Color: 1
Size: 311193 Color: 1
Size: 274179 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 475694 Color: 1
Size: 270818 Color: 1
Size: 253489 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 354123 Color: 1
Size: 329855 Color: 1
Size: 316023 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 447257 Color: 1
Size: 294328 Color: 1
Size: 258416 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 436616 Color: 1
Size: 295798 Color: 1
Size: 267587 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 352631 Color: 1
Size: 336168 Color: 1
Size: 311202 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 359630 Color: 1
Size: 324952 Color: 0
Size: 315419 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 391805 Color: 1
Size: 331840 Color: 1
Size: 276356 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 453694 Color: 1
Size: 276267 Color: 1
Size: 270040 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 439954 Color: 1
Size: 286314 Color: 1
Size: 273733 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 418803 Color: 1
Size: 315992 Color: 1
Size: 265206 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 366081 Color: 1
Size: 365049 Color: 1
Size: 268871 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 362312 Color: 1
Size: 348663 Color: 1
Size: 289026 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 445610 Color: 1
Size: 292244 Color: 1
Size: 262147 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 440063 Color: 1
Size: 302083 Color: 1
Size: 257855 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 408257 Color: 1
Size: 323567 Color: 1
Size: 268177 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 401923 Color: 1
Size: 322069 Color: 1
Size: 276009 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 465137 Color: 1
Size: 270071 Color: 1
Size: 264793 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 472317 Color: 1
Size: 275333 Color: 1
Size: 252351 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 426896 Color: 1
Size: 318112 Color: 1
Size: 254993 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 387532 Color: 1
Size: 350669 Color: 1
Size: 261800 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 415720 Color: 1
Size: 328639 Color: 1
Size: 255642 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 416137 Color: 1
Size: 312620 Color: 1
Size: 271244 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 497595 Color: 1
Size: 251356 Color: 1
Size: 251050 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 436868 Color: 1
Size: 296062 Color: 1
Size: 267071 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 402556 Color: 1
Size: 311810 Color: 1
Size: 285635 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 432124 Color: 1
Size: 305754 Color: 1
Size: 262123 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 401532 Color: 1
Size: 334685 Color: 1
Size: 263784 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 445474 Color: 1
Size: 280261 Color: 1
Size: 274266 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 386832 Color: 1
Size: 324898 Color: 1
Size: 288271 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 482485 Color: 1
Size: 264722 Color: 1
Size: 252794 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 380343 Color: 1
Size: 330011 Color: 1
Size: 289647 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 400925 Color: 1
Size: 306581 Color: 1
Size: 292495 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 446054 Color: 1
Size: 287282 Color: 1
Size: 266665 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 453880 Color: 1
Size: 278289 Color: 1
Size: 267832 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 415669 Color: 1
Size: 296599 Color: 1
Size: 287733 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 385569 Color: 1
Size: 342848 Color: 1
Size: 271584 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 476302 Color: 1
Size: 271644 Color: 1
Size: 252055 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 361819 Color: 1
Size: 359662 Color: 1
Size: 278520 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 465949 Color: 1
Size: 276649 Color: 1
Size: 257403 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 378504 Color: 1
Size: 317127 Color: 0
Size: 304370 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 393222 Color: 1
Size: 321521 Color: 1
Size: 285258 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 440056 Color: 1
Size: 300147 Color: 1
Size: 259798 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 366606 Color: 1
Size: 350720 Color: 1
Size: 282675 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 373620 Color: 1
Size: 343083 Color: 1
Size: 283298 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 454898 Color: 1
Size: 288253 Color: 1
Size: 256850 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 393320 Color: 1
Size: 328548 Color: 1
Size: 278133 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 477908 Color: 1
Size: 263296 Color: 1
Size: 258797 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 477471 Color: 1
Size: 271087 Color: 1
Size: 251443 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 472264 Color: 1
Size: 276515 Color: 1
Size: 251222 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 488870 Color: 1
Size: 255595 Color: 0
Size: 255536 Color: 1

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 399358 Color: 1
Size: 327932 Color: 1
Size: 272711 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 468390 Color: 1
Size: 276126 Color: 1
Size: 255485 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 361874 Color: 1
Size: 341031 Color: 1
Size: 297096 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 453870 Color: 1
Size: 290897 Color: 1
Size: 255234 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 450539 Color: 1
Size: 288502 Color: 1
Size: 260960 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 459675 Color: 1
Size: 284712 Color: 1
Size: 255614 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 361745 Color: 1
Size: 323556 Color: 0
Size: 314700 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 497805 Color: 1
Size: 251618 Color: 1
Size: 250578 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 431898 Color: 1
Size: 312237 Color: 1
Size: 255866 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 456938 Color: 1
Size: 273075 Color: 1
Size: 269988 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 468549 Color: 1
Size: 269025 Color: 1
Size: 262427 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 454593 Color: 1
Size: 285883 Color: 1
Size: 259525 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 427645 Color: 1
Size: 313346 Color: 1
Size: 259010 Color: 0

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 443548 Color: 1
Size: 286035 Color: 1
Size: 270418 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 459963 Color: 1
Size: 274942 Color: 1
Size: 265096 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 440589 Color: 1
Size: 303215 Color: 1
Size: 256197 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 386478 Color: 1
Size: 319309 Color: 1
Size: 294214 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 388189 Color: 1
Size: 331636 Color: 1
Size: 280176 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 356770 Color: 1
Size: 342532 Color: 1
Size: 300699 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 372015 Color: 1
Size: 362651 Color: 1
Size: 265335 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 402641 Color: 1
Size: 321559 Color: 1
Size: 275801 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 428625 Color: 1
Size: 310988 Color: 1
Size: 260388 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 456858 Color: 1
Size: 276746 Color: 0
Size: 266397 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 440889 Color: 1
Size: 289872 Color: 1
Size: 269240 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 458799 Color: 1
Size: 275772 Color: 1
Size: 265430 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 485567 Color: 1
Size: 261468 Color: 1
Size: 252966 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 494274 Color: 1
Size: 255450 Color: 1
Size: 250277 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 373887 Color: 1
Size: 357679 Color: 1
Size: 268435 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 402401 Color: 1
Size: 320078 Color: 1
Size: 277522 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 405633 Color: 1
Size: 307635 Color: 1
Size: 286733 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 431988 Color: 1
Size: 313056 Color: 1
Size: 254957 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 414284 Color: 1
Size: 322541 Color: 1
Size: 263176 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 427799 Color: 1
Size: 304754 Color: 1
Size: 267448 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 393024 Color: 1
Size: 306271 Color: 1
Size: 300706 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 455364 Color: 1
Size: 291748 Color: 1
Size: 252889 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 371535 Color: 1
Size: 362826 Color: 1
Size: 265640 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 421958 Color: 1
Size: 299107 Color: 1
Size: 278936 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 435902 Color: 1
Size: 289145 Color: 1
Size: 274954 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 481861 Color: 1
Size: 264982 Color: 1
Size: 253158 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 377782 Color: 1
Size: 330871 Color: 1
Size: 291348 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 430303 Color: 1
Size: 288008 Color: 1
Size: 281690 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 425617 Color: 1
Size: 317561 Color: 1
Size: 256823 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 467137 Color: 1
Size: 276412 Color: 1
Size: 256452 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 378142 Color: 1
Size: 346914 Color: 1
Size: 274945 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 389405 Color: 1
Size: 358413 Color: 1
Size: 252183 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 391906 Color: 1
Size: 327867 Color: 1
Size: 280228 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 426975 Color: 1
Size: 310067 Color: 1
Size: 262959 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 455539 Color: 1
Size: 294235 Color: 1
Size: 250227 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 378034 Color: 1
Size: 361846 Color: 1
Size: 260121 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 484671 Color: 1
Size: 265202 Color: 1
Size: 250128 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 354937 Color: 1
Size: 329145 Color: 1
Size: 315919 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 399501 Color: 1
Size: 328868 Color: 1
Size: 271632 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 399372 Color: 1
Size: 330000 Color: 1
Size: 270629 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 480288 Color: 1
Size: 265675 Color: 1
Size: 254038 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 401398 Color: 1
Size: 326035 Color: 1
Size: 272568 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 475837 Color: 1
Size: 269449 Color: 1
Size: 254715 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 403495 Color: 1
Size: 325474 Color: 1
Size: 271032 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 358168 Color: 1
Size: 322812 Color: 1
Size: 319021 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 365016 Color: 1
Size: 364816 Color: 1
Size: 270169 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 396063 Color: 1
Size: 321909 Color: 1
Size: 282029 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 397848 Color: 1
Size: 309710 Color: 0
Size: 292443 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 419578 Color: 1
Size: 317337 Color: 1
Size: 263086 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 495011 Color: 1
Size: 254856 Color: 1
Size: 250134 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 455001 Color: 1
Size: 282317 Color: 1
Size: 262683 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 419628 Color: 1
Size: 310490 Color: 1
Size: 269883 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 441389 Color: 1
Size: 286478 Color: 1
Size: 272134 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 402188 Color: 1
Size: 303193 Color: 1
Size: 294620 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 398398 Color: 1
Size: 317128 Color: 1
Size: 284475 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 436622 Color: 1
Size: 311520 Color: 1
Size: 251859 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 429758 Color: 1
Size: 299775 Color: 1
Size: 270468 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 363169 Color: 1
Size: 361615 Color: 1
Size: 275217 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 426258 Color: 1
Size: 315705 Color: 1
Size: 258038 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 377197 Color: 1
Size: 353114 Color: 1
Size: 269690 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 420940 Color: 1
Size: 292872 Color: 0
Size: 286189 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 440068 Color: 1
Size: 289601 Color: 0
Size: 270332 Color: 1

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 382285 Color: 1
Size: 344237 Color: 1
Size: 273479 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 418658 Color: 1
Size: 293241 Color: 1
Size: 288102 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 376999 Color: 1
Size: 345041 Color: 1
Size: 277961 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 424826 Color: 1
Size: 313717 Color: 1
Size: 261458 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 457202 Color: 1
Size: 271724 Color: 0
Size: 271075 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 390780 Color: 1
Size: 336687 Color: 1
Size: 272534 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 366405 Color: 1
Size: 349491 Color: 1
Size: 284105 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 378504 Color: 1
Size: 354551 Color: 1
Size: 266946 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 425774 Color: 1
Size: 317062 Color: 1
Size: 257165 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 419094 Color: 1
Size: 320651 Color: 1
Size: 260256 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 499094 Color: 1
Size: 250843 Color: 1
Size: 250064 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 427528 Color: 1
Size: 290652 Color: 0
Size: 281821 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 414010 Color: 1
Size: 323387 Color: 1
Size: 262604 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 402372 Color: 1
Size: 324978 Color: 1
Size: 272651 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 440467 Color: 1
Size: 296114 Color: 1
Size: 263420 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 429623 Color: 1
Size: 313987 Color: 1
Size: 256391 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 367914 Color: 1
Size: 346289 Color: 1
Size: 285798 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 367541 Color: 1
Size: 340147 Color: 1
Size: 292313 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 444786 Color: 1
Size: 305167 Color: 1
Size: 250048 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 426948 Color: 1
Size: 311123 Color: 1
Size: 261930 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 367296 Color: 1
Size: 335570 Color: 1
Size: 297135 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 412370 Color: 1
Size: 319476 Color: 0
Size: 268155 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 383891 Color: 1
Size: 342746 Color: 1
Size: 273364 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 473138 Color: 1
Size: 271163 Color: 1
Size: 255700 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 498560 Color: 1
Size: 250849 Color: 1
Size: 250592 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 454924 Color: 1
Size: 286989 Color: 1
Size: 258088 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 396352 Color: 1
Size: 319206 Color: 1
Size: 284443 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 366824 Color: 1
Size: 335071 Color: 1
Size: 298106 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 404122 Color: 1
Size: 327879 Color: 1
Size: 268000 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 378666 Color: 1
Size: 323786 Color: 1
Size: 297549 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 399960 Color: 1
Size: 319270 Color: 1
Size: 280771 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 486357 Color: 1
Size: 261307 Color: 1
Size: 252337 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 452129 Color: 1
Size: 281327 Color: 0
Size: 266545 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 412299 Color: 1
Size: 323075 Color: 1
Size: 264627 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 422006 Color: 1
Size: 307998 Color: 1
Size: 269997 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 428793 Color: 1
Size: 301849 Color: 1
Size: 269359 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 452452 Color: 1
Size: 284634 Color: 1
Size: 262915 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 360007 Color: 1
Size: 323816 Color: 1
Size: 316178 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 497334 Color: 1
Size: 251405 Color: 0
Size: 251262 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 486901 Color: 1
Size: 261312 Color: 0
Size: 251788 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 479035 Color: 1
Size: 265960 Color: 1
Size: 255006 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 388473 Color: 1
Size: 315253 Color: 0
Size: 296275 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 404018 Color: 1
Size: 341561 Color: 1
Size: 254422 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 454643 Color: 1
Size: 288361 Color: 1
Size: 256997 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 426673 Color: 1
Size: 310631 Color: 1
Size: 262697 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 458708 Color: 1
Size: 284627 Color: 1
Size: 256666 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 409964 Color: 1
Size: 319561 Color: 1
Size: 270476 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 479190 Color: 1
Size: 266647 Color: 1
Size: 254164 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 380405 Color: 1
Size: 353002 Color: 1
Size: 266594 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 415721 Color: 1
Size: 302655 Color: 0
Size: 281625 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 452005 Color: 1
Size: 281578 Color: 1
Size: 266418 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 426047 Color: 1
Size: 314437 Color: 1
Size: 259517 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 491979 Color: 1
Size: 254840 Color: 1
Size: 253182 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 394405 Color: 1
Size: 338130 Color: 1
Size: 267466 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 404664 Color: 1
Size: 331018 Color: 1
Size: 264319 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 399136 Color: 1
Size: 332414 Color: 1
Size: 268451 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 477083 Color: 1
Size: 268314 Color: 1
Size: 254604 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 395386 Color: 1
Size: 325318 Color: 1
Size: 279297 Color: 0

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 438936 Color: 1
Size: 298615 Color: 1
Size: 262450 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 429627 Color: 1
Size: 285704 Color: 0
Size: 284670 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 450128 Color: 1
Size: 299039 Color: 1
Size: 250834 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 439160 Color: 1
Size: 308522 Color: 1
Size: 252319 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 363415 Color: 1
Size: 328603 Color: 1
Size: 307983 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 406545 Color: 1
Size: 309461 Color: 1
Size: 283995 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 439961 Color: 1
Size: 302822 Color: 1
Size: 257218 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 450838 Color: 1
Size: 280508 Color: 1
Size: 268655 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 449098 Color: 1
Size: 296256 Color: 1
Size: 254647 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 439889 Color: 1
Size: 301917 Color: 1
Size: 258195 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 462209 Color: 1
Size: 283317 Color: 1
Size: 254475 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 423077 Color: 1
Size: 293989 Color: 1
Size: 282935 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 435591 Color: 1
Size: 310670 Color: 1
Size: 253740 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 464220 Color: 1
Size: 276494 Color: 1
Size: 259287 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 378042 Color: 1
Size: 342397 Color: 1
Size: 279562 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 398705 Color: 1
Size: 346380 Color: 1
Size: 254916 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 449444 Color: 1
Size: 293048 Color: 1
Size: 257509 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 386718 Color: 1
Size: 313521 Color: 1
Size: 299762 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 456430 Color: 1
Size: 289666 Color: 1
Size: 253905 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 464322 Color: 1
Size: 269677 Color: 1
Size: 266002 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 465637 Color: 1
Size: 275960 Color: 1
Size: 258404 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 464953 Color: 1
Size: 278445 Color: 1
Size: 256603 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 402859 Color: 1
Size: 332314 Color: 1
Size: 264828 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 401135 Color: 1
Size: 315700 Color: 1
Size: 283166 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 398148 Color: 1
Size: 329727 Color: 1
Size: 272126 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 470060 Color: 1
Size: 274589 Color: 1
Size: 255352 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 408354 Color: 1
Size: 312547 Color: 1
Size: 279100 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 468192 Color: 1
Size: 272195 Color: 1
Size: 259614 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 423807 Color: 1
Size: 318359 Color: 1
Size: 257835 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 417837 Color: 1
Size: 291658 Color: 1
Size: 290506 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 384580 Color: 1
Size: 344051 Color: 1
Size: 271370 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 393422 Color: 1
Size: 327313 Color: 1
Size: 279266 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 437176 Color: 1
Size: 303112 Color: 1
Size: 259713 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 379862 Color: 1
Size: 347901 Color: 1
Size: 272238 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 389453 Color: 1
Size: 322817 Color: 1
Size: 287731 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 378378 Color: 1
Size: 359069 Color: 1
Size: 262554 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 392883 Color: 1
Size: 330826 Color: 1
Size: 276292 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 437243 Color: 1
Size: 284657 Color: 1
Size: 278101 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 395028 Color: 1
Size: 339642 Color: 1
Size: 265331 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 433917 Color: 1
Size: 292488 Color: 1
Size: 273596 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 432500 Color: 1
Size: 303661 Color: 1
Size: 263840 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 465776 Color: 1
Size: 275351 Color: 1
Size: 258874 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 471499 Color: 1
Size: 277992 Color: 1
Size: 250510 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 479827 Color: 1
Size: 262318 Color: 1
Size: 257856 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 427698 Color: 1
Size: 294092 Color: 1
Size: 278211 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 454694 Color: 1
Size: 294347 Color: 1
Size: 250960 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 378264 Color: 1
Size: 357549 Color: 1
Size: 264188 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 361901 Color: 1
Size: 337123 Color: 1
Size: 300977 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 419226 Color: 1
Size: 300402 Color: 0
Size: 280373 Color: 1

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 464380 Color: 1
Size: 267853 Color: 0
Size: 267768 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 402532 Color: 1
Size: 317783 Color: 1
Size: 279686 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 385202 Color: 1
Size: 316493 Color: 0
Size: 298306 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 491070 Color: 1
Size: 257033 Color: 1
Size: 251898 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 419820 Color: 1
Size: 329202 Color: 1
Size: 250979 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 489415 Color: 1
Size: 258708 Color: 1
Size: 251878 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 452100 Color: 1
Size: 294032 Color: 1
Size: 253869 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 352579 Color: 1
Size: 337985 Color: 1
Size: 309437 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 413811 Color: 1
Size: 323932 Color: 1
Size: 262258 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 454315 Color: 1
Size: 294799 Color: 1
Size: 250887 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 425406 Color: 1
Size: 317676 Color: 1
Size: 256919 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 390327 Color: 1
Size: 336563 Color: 1
Size: 273111 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 420970 Color: 1
Size: 306369 Color: 1
Size: 272662 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 360953 Color: 1
Size: 347639 Color: 1
Size: 291409 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 460265 Color: 1
Size: 274835 Color: 0
Size: 264901 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 386380 Color: 1
Size: 326047 Color: 1
Size: 287574 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 427998 Color: 1
Size: 292449 Color: 0
Size: 279554 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 378312 Color: 1
Size: 319722 Color: 1
Size: 301967 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 355507 Color: 1
Size: 354354 Color: 1
Size: 290140 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 449833 Color: 1
Size: 295884 Color: 1
Size: 254284 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 471795 Color: 1
Size: 273593 Color: 1
Size: 254613 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 443740 Color: 1
Size: 295161 Color: 1
Size: 261100 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 351695 Color: 1
Size: 348076 Color: 1
Size: 300230 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 446164 Color: 1
Size: 296029 Color: 1
Size: 257808 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 361371 Color: 1
Size: 355121 Color: 1
Size: 283509 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 382128 Color: 1
Size: 344275 Color: 1
Size: 273598 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 494952 Color: 1
Size: 254285 Color: 1
Size: 250764 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 373997 Color: 1
Size: 334622 Color: 1
Size: 291382 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 391428 Color: 1
Size: 330539 Color: 1
Size: 278034 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 477254 Color: 1
Size: 270082 Color: 1
Size: 252665 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 372809 Color: 1
Size: 319936 Color: 0
Size: 307256 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 454756 Color: 1
Size: 293309 Color: 1
Size: 251936 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 363551 Color: 1
Size: 325514 Color: 1
Size: 310936 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 374485 Color: 1
Size: 336100 Color: 1
Size: 289416 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 492911 Color: 1
Size: 255709 Color: 1
Size: 251381 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 391592 Color: 1
Size: 331275 Color: 1
Size: 277134 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 440203 Color: 1
Size: 308155 Color: 1
Size: 251643 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 419292 Color: 1
Size: 300652 Color: 1
Size: 280057 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 406917 Color: 1
Size: 327815 Color: 1
Size: 265269 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 458791 Color: 1
Size: 290333 Color: 1
Size: 250877 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 443197 Color: 1
Size: 290856 Color: 1
Size: 265948 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 434551 Color: 1
Size: 283807 Color: 1
Size: 281643 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 432525 Color: 1
Size: 301837 Color: 1
Size: 265639 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 413032 Color: 1
Size: 314996 Color: 1
Size: 271973 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 414711 Color: 1
Size: 318753 Color: 0
Size: 266537 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 489630 Color: 1
Size: 256448 Color: 1
Size: 253923 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 405973 Color: 1
Size: 330690 Color: 1
Size: 263338 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 378819 Color: 1
Size: 333345 Color: 1
Size: 287837 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 451742 Color: 1
Size: 280378 Color: 1
Size: 267881 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 427640 Color: 1
Size: 300949 Color: 1
Size: 271412 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 394668 Color: 1
Size: 349221 Color: 1
Size: 256112 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 411991 Color: 1
Size: 329709 Color: 1
Size: 258301 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 494329 Color: 1
Size: 254592 Color: 1
Size: 251080 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 433946 Color: 1
Size: 310796 Color: 1
Size: 255259 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 427809 Color: 1
Size: 306706 Color: 1
Size: 265486 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 387202 Color: 1
Size: 315075 Color: 0
Size: 297724 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 444865 Color: 1
Size: 296982 Color: 1
Size: 258154 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 438948 Color: 1
Size: 300720 Color: 1
Size: 260333 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 404894 Color: 1
Size: 309637 Color: 1
Size: 285470 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 441795 Color: 1
Size: 303268 Color: 1
Size: 254938 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 459863 Color: 1
Size: 274755 Color: 1
Size: 265383 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 496007 Color: 1
Size: 252932 Color: 1
Size: 251062 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 352274 Color: 1
Size: 350148 Color: 1
Size: 297579 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 406996 Color: 1
Size: 308977 Color: 1
Size: 284028 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 423475 Color: 1
Size: 297432 Color: 1
Size: 279094 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 432067 Color: 1
Size: 295190 Color: 0
Size: 272744 Color: 1

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 445132 Color: 1
Size: 295972 Color: 1
Size: 258897 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 440445 Color: 1
Size: 304534 Color: 1
Size: 255022 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 378859 Color: 1
Size: 323015 Color: 1
Size: 298127 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 478489 Color: 1
Size: 260941 Color: 0
Size: 260571 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 361423 Color: 1
Size: 328123 Color: 1
Size: 310455 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 485035 Color: 1
Size: 263002 Color: 1
Size: 251964 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 410491 Color: 1
Size: 312384 Color: 1
Size: 277126 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 377746 Color: 1
Size: 326519 Color: 1
Size: 295736 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 359128 Color: 1
Size: 352535 Color: 1
Size: 288338 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 481062 Color: 1
Size: 268613 Color: 1
Size: 250326 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 391376 Color: 1
Size: 337451 Color: 1
Size: 271174 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 373499 Color: 1
Size: 335478 Color: 1
Size: 291024 Color: 0

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 492646 Color: 1
Size: 255535 Color: 1
Size: 251820 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 450021 Color: 1
Size: 278160 Color: 1
Size: 271820 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 408875 Color: 1
Size: 331363 Color: 1
Size: 259763 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 431253 Color: 1
Size: 302044 Color: 1
Size: 266704 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 435909 Color: 1
Size: 292992 Color: 0
Size: 271100 Color: 1

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 361561 Color: 1
Size: 320876 Color: 1
Size: 317564 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 486609 Color: 1
Size: 262916 Color: 1
Size: 250476 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 410341 Color: 1
Size: 309467 Color: 1
Size: 280193 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 459989 Color: 1
Size: 285655 Color: 1
Size: 254357 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 414788 Color: 1
Size: 295545 Color: 1
Size: 289668 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 377491 Color: 1
Size: 330635 Color: 1
Size: 291875 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 366963 Color: 1
Size: 346744 Color: 1
Size: 286294 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 423605 Color: 1
Size: 306038 Color: 1
Size: 270358 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 471391 Color: 1
Size: 265008 Color: 1
Size: 263602 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 397898 Color: 1
Size: 333419 Color: 1
Size: 268684 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 423061 Color: 1
Size: 318380 Color: 1
Size: 258560 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 398366 Color: 1
Size: 302972 Color: 1
Size: 298663 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 438792 Color: 1
Size: 307686 Color: 1
Size: 253523 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 379774 Color: 1
Size: 337754 Color: 1
Size: 282473 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 424097 Color: 1
Size: 320229 Color: 1
Size: 255675 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 384599 Color: 1
Size: 342698 Color: 1
Size: 272704 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 453568 Color: 1
Size: 285569 Color: 1
Size: 260864 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 373661 Color: 1
Size: 327002 Color: 1
Size: 299338 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 407605 Color: 1
Size: 321894 Color: 1
Size: 270502 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 480307 Color: 1
Size: 264327 Color: 1
Size: 255367 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 377283 Color: 1
Size: 343053 Color: 1
Size: 279665 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 395641 Color: 1
Size: 309239 Color: 1
Size: 295121 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 394779 Color: 1
Size: 354937 Color: 1
Size: 250285 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 411701 Color: 1
Size: 332245 Color: 1
Size: 256055 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 473477 Color: 1
Size: 275505 Color: 1
Size: 251019 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 393962 Color: 1
Size: 345591 Color: 1
Size: 260448 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 364812 Color: 1
Size: 335526 Color: 1
Size: 299663 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 473541 Color: 1
Size: 270486 Color: 1
Size: 255974 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 371915 Color: 1
Size: 347909 Color: 1
Size: 280177 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 391149 Color: 1
Size: 349296 Color: 1
Size: 259556 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 431715 Color: 1
Size: 299022 Color: 1
Size: 269264 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 432454 Color: 1
Size: 314885 Color: 1
Size: 252662 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 422506 Color: 1
Size: 321879 Color: 1
Size: 255616 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 383183 Color: 1
Size: 321310 Color: 1
Size: 295508 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 420695 Color: 1
Size: 325865 Color: 1
Size: 253441 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 440169 Color: 1
Size: 299595 Color: 1
Size: 260237 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 408110 Color: 1
Size: 337551 Color: 1
Size: 254340 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 377669 Color: 1
Size: 316355 Color: 1
Size: 305977 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 451144 Color: 1
Size: 291380 Color: 1
Size: 257477 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 469819 Color: 1
Size: 269562 Color: 1
Size: 260620 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 434340 Color: 1
Size: 305425 Color: 1
Size: 260236 Color: 0

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 393594 Color: 1
Size: 343882 Color: 1
Size: 262525 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 392621 Color: 1
Size: 304737 Color: 1
Size: 302643 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 465110 Color: 1
Size: 281460 Color: 1
Size: 253431 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 386965 Color: 1
Size: 355692 Color: 1
Size: 257344 Color: 0

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 484296 Color: 1
Size: 264645 Color: 1
Size: 251060 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 469818 Color: 1
Size: 272620 Color: 0
Size: 257563 Color: 1

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 385261 Color: 1
Size: 307766 Color: 0
Size: 306974 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 408837 Color: 1
Size: 331795 Color: 1
Size: 259369 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 375440 Color: 1
Size: 350474 Color: 1
Size: 274087 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 375831 Color: 1
Size: 323526 Color: 0
Size: 300644 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 475782 Color: 1
Size: 267720 Color: 1
Size: 256499 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 431220 Color: 1
Size: 298413 Color: 1
Size: 270368 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 376411 Color: 1
Size: 356121 Color: 1
Size: 267469 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 488195 Color: 1
Size: 259520 Color: 1
Size: 252286 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 426568 Color: 1
Size: 309155 Color: 1
Size: 264278 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 367100 Color: 1
Size: 361704 Color: 1
Size: 271197 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 383583 Color: 1
Size: 338181 Color: 1
Size: 278237 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 461108 Color: 1
Size: 284858 Color: 1
Size: 254035 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 478339 Color: 1
Size: 266405 Color: 1
Size: 255257 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 465500 Color: 1
Size: 283918 Color: 1
Size: 250583 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 459497 Color: 1
Size: 278908 Color: 1
Size: 261596 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 429501 Color: 1
Size: 299054 Color: 1
Size: 271446 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 398309 Color: 1
Size: 337479 Color: 1
Size: 264213 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 421377 Color: 1
Size: 311420 Color: 1
Size: 267204 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 424300 Color: 1
Size: 313072 Color: 1
Size: 262629 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 406133 Color: 1
Size: 320963 Color: 1
Size: 272905 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 441292 Color: 1
Size: 302040 Color: 1
Size: 256669 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 376465 Color: 1
Size: 351879 Color: 1
Size: 271657 Color: 0

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 439755 Color: 1
Size: 297197 Color: 1
Size: 263049 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 373587 Color: 1
Size: 334434 Color: 1
Size: 291980 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 456044 Color: 1
Size: 287314 Color: 1
Size: 256643 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 388532 Color: 1
Size: 313704 Color: 1
Size: 297765 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 381313 Color: 1
Size: 317650 Color: 0
Size: 301038 Color: 1

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 413863 Color: 1
Size: 313924 Color: 1
Size: 272214 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 419461 Color: 1
Size: 326171 Color: 1
Size: 254369 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 362112 Color: 1
Size: 361949 Color: 1
Size: 275940 Color: 0

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 465627 Color: 1
Size: 271937 Color: 1
Size: 262437 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 439072 Color: 1
Size: 302082 Color: 1
Size: 258847 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 407798 Color: 1
Size: 323712 Color: 1
Size: 268491 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 438019 Color: 1
Size: 310135 Color: 1
Size: 251847 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 490218 Color: 1
Size: 257548 Color: 1
Size: 252235 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 354824 Color: 1
Size: 346639 Color: 1
Size: 298538 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 370262 Color: 1
Size: 328827 Color: 1
Size: 300912 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 459785 Color: 1
Size: 284888 Color: 1
Size: 255328 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 492455 Color: 1
Size: 256367 Color: 1
Size: 251179 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 449167 Color: 1
Size: 286682 Color: 1
Size: 264152 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 457651 Color: 1
Size: 288850 Color: 1
Size: 253500 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 492232 Color: 1
Size: 257676 Color: 1
Size: 250093 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 389670 Color: 1
Size: 350829 Color: 1
Size: 259502 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 373020 Color: 1
Size: 344406 Color: 1
Size: 282575 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 472474 Color: 1
Size: 264583 Color: 1
Size: 262944 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 480375 Color: 1
Size: 267194 Color: 1
Size: 252432 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 401780 Color: 1
Size: 313163 Color: 1
Size: 285058 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 398224 Color: 1
Size: 334077 Color: 1
Size: 267700 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 374900 Color: 1
Size: 351861 Color: 1
Size: 273240 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 357754 Color: 1
Size: 343893 Color: 1
Size: 298354 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 477336 Color: 1
Size: 269702 Color: 1
Size: 252963 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 369650 Color: 1
Size: 348947 Color: 1
Size: 281404 Color: 0

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 454304 Color: 1
Size: 284386 Color: 1
Size: 261311 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 420439 Color: 1
Size: 297072 Color: 0
Size: 282490 Color: 1

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 442213 Color: 1
Size: 282181 Color: 1
Size: 275607 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 354812 Color: 1
Size: 336651 Color: 1
Size: 308538 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 454265 Color: 1
Size: 280759 Color: 1
Size: 264977 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 440572 Color: 1
Size: 298712 Color: 1
Size: 260717 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 486155 Color: 1
Size: 259098 Color: 1
Size: 254748 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 484356 Color: 1
Size: 261584 Color: 1
Size: 254061 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 492184 Color: 1
Size: 255661 Color: 1
Size: 252156 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 359195 Color: 1
Size: 323074 Color: 1
Size: 317732 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 360143 Color: 1
Size: 328711 Color: 1
Size: 311147 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 464055 Color: 1
Size: 282756 Color: 1
Size: 253190 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 363491 Color: 1
Size: 338731 Color: 1
Size: 297779 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 445155 Color: 1
Size: 294702 Color: 1
Size: 260144 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 465802 Color: 1
Size: 277775 Color: 1
Size: 256424 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 362525 Color: 1
Size: 339340 Color: 1
Size: 298136 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 355613 Color: 1
Size: 344259 Color: 1
Size: 300129 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 405044 Color: 1
Size: 320255 Color: 1
Size: 274702 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 458387 Color: 1
Size: 272036 Color: 0
Size: 269578 Color: 1

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 492056 Color: 1
Size: 257872 Color: 1
Size: 250073 Color: 0

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 412249 Color: 1
Size: 325371 Color: 1
Size: 262381 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 444770 Color: 1
Size: 302210 Color: 1
Size: 253021 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 467196 Color: 1
Size: 280912 Color: 1
Size: 251893 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 360591 Color: 1
Size: 358313 Color: 1
Size: 281097 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 487563 Color: 1
Size: 259543 Color: 1
Size: 252895 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 421116 Color: 1
Size: 297293 Color: 1
Size: 281592 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 468029 Color: 1
Size: 280975 Color: 1
Size: 250997 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 375334 Color: 1
Size: 331408 Color: 1
Size: 293259 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 496377 Color: 1
Size: 252443 Color: 0
Size: 251181 Color: 1

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 364174 Color: 1
Size: 323026 Color: 1
Size: 312801 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 498466 Color: 1
Size: 251523 Color: 1
Size: 250012 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 414642 Color: 1
Size: 298379 Color: 1
Size: 286980 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 377813 Color: 1
Size: 327996 Color: 0
Size: 294192 Color: 1

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 452913 Color: 1
Size: 284737 Color: 1
Size: 262351 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 492463 Color: 1
Size: 254160 Color: 1
Size: 253378 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 394162 Color: 1
Size: 309866 Color: 1
Size: 295973 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 435151 Color: 1
Size: 293097 Color: 1
Size: 271753 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 374289 Color: 1
Size: 327216 Color: 1
Size: 298496 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 495723 Color: 1
Size: 253757 Color: 1
Size: 250521 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 402303 Color: 1
Size: 301432 Color: 1
Size: 296266 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 401271 Color: 1
Size: 331058 Color: 1
Size: 267672 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 465551 Color: 1
Size: 278606 Color: 1
Size: 255844 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 449537 Color: 1
Size: 295250 Color: 1
Size: 255214 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 492279 Color: 1
Size: 257192 Color: 1
Size: 250530 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 425992 Color: 1
Size: 306653 Color: 1
Size: 267356 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 444945 Color: 1
Size: 298109 Color: 1
Size: 256947 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 356278 Color: 1
Size: 325436 Color: 1
Size: 318287 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 470773 Color: 1
Size: 273042 Color: 1
Size: 256186 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 391141 Color: 1
Size: 305621 Color: 1
Size: 303239 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 463293 Color: 1
Size: 278115 Color: 1
Size: 258593 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 399905 Color: 1
Size: 330863 Color: 1
Size: 269233 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 464034 Color: 1
Size: 272551 Color: 0
Size: 263416 Color: 1

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 432648 Color: 1
Size: 300124 Color: 1
Size: 267229 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 468969 Color: 1
Size: 277810 Color: 1
Size: 253222 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 393675 Color: 1
Size: 345279 Color: 1
Size: 261047 Color: 0

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 441835 Color: 1
Size: 296905 Color: 1
Size: 261261 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 410191 Color: 1
Size: 326132 Color: 1
Size: 263678 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 416000 Color: 1
Size: 316552 Color: 1
Size: 267449 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 462739 Color: 1
Size: 279631 Color: 1
Size: 257631 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 389738 Color: 1
Size: 358700 Color: 1
Size: 251563 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 369149 Color: 1
Size: 330977 Color: 0
Size: 299875 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 371346 Color: 1
Size: 340254 Color: 1
Size: 288401 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 420620 Color: 1
Size: 318334 Color: 1
Size: 261047 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 457355 Color: 1
Size: 278959 Color: 1
Size: 263687 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 378569 Color: 1
Size: 330391 Color: 1
Size: 291041 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 422274 Color: 1
Size: 313437 Color: 1
Size: 264290 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 375041 Color: 1
Size: 355835 Color: 1
Size: 269125 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 422455 Color: 1
Size: 314123 Color: 1
Size: 263423 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 497423 Color: 1
Size: 252521 Color: 1
Size: 250057 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 469522 Color: 1
Size: 270048 Color: 1
Size: 260431 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 395020 Color: 1
Size: 331295 Color: 1
Size: 273686 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 470203 Color: 1
Size: 273960 Color: 1
Size: 255838 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 428816 Color: 1
Size: 302372 Color: 1
Size: 268813 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 384614 Color: 1
Size: 343823 Color: 1
Size: 271564 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 400561 Color: 1
Size: 337322 Color: 1
Size: 262118 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 374993 Color: 1
Size: 349143 Color: 1
Size: 275865 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 462031 Color: 1
Size: 282442 Color: 1
Size: 255528 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 445135 Color: 1
Size: 298681 Color: 1
Size: 256185 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 401939 Color: 1
Size: 328832 Color: 1
Size: 269230 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 443070 Color: 1
Size: 300771 Color: 1
Size: 256160 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 492414 Color: 1
Size: 257156 Color: 1
Size: 250431 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 414176 Color: 1
Size: 325685 Color: 1
Size: 260140 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 468008 Color: 1
Size: 281629 Color: 1
Size: 250364 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 377977 Color: 1
Size: 346353 Color: 1
Size: 275671 Color: 0

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 358238 Color: 1
Size: 347313 Color: 1
Size: 294450 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 485662 Color: 1
Size: 262590 Color: 1
Size: 251749 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 450914 Color: 1
Size: 297393 Color: 1
Size: 251694 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 373369 Color: 1
Size: 350582 Color: 1
Size: 276050 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 450228 Color: 1
Size: 282781 Color: 1
Size: 266992 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 463284 Color: 1
Size: 274246 Color: 1
Size: 262471 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 411683 Color: 1
Size: 303178 Color: 1
Size: 285140 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 478112 Color: 1
Size: 266327 Color: 1
Size: 255562 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 420327 Color: 1
Size: 312467 Color: 1
Size: 267207 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 471852 Color: 1
Size: 276191 Color: 1
Size: 251958 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 473431 Color: 1
Size: 272809 Color: 1
Size: 253761 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 408133 Color: 1
Size: 307157 Color: 0
Size: 284711 Color: 1

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 447565 Color: 1
Size: 298175 Color: 1
Size: 254261 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 433023 Color: 1
Size: 288053 Color: 1
Size: 278925 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 428281 Color: 1
Size: 296840 Color: 1
Size: 274880 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 482605 Color: 1
Size: 263547 Color: 1
Size: 253849 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 476516 Color: 1
Size: 268244 Color: 1
Size: 255241 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 419803 Color: 1
Size: 308381 Color: 1
Size: 271817 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 495702 Color: 1
Size: 252345 Color: 1
Size: 251954 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 375918 Color: 1
Size: 332397 Color: 1
Size: 291686 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 400601 Color: 1
Size: 338559 Color: 1
Size: 260841 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 430817 Color: 1
Size: 309164 Color: 1
Size: 260020 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 405820 Color: 1
Size: 333152 Color: 1
Size: 261029 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 474532 Color: 1
Size: 271850 Color: 1
Size: 253619 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 466670 Color: 1
Size: 272431 Color: 1
Size: 260900 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 412951 Color: 1
Size: 325815 Color: 1
Size: 261235 Color: 0

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 425578 Color: 1
Size: 323127 Color: 1
Size: 251296 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 466460 Color: 1
Size: 270011 Color: 1
Size: 263530 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 387090 Color: 1
Size: 352730 Color: 1
Size: 260181 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 442864 Color: 1
Size: 281488 Color: 1
Size: 275649 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 393787 Color: 1
Size: 346888 Color: 1
Size: 259326 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 451415 Color: 1
Size: 293742 Color: 1
Size: 254844 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 371265 Color: 1
Size: 358515 Color: 1
Size: 270221 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 458054 Color: 1
Size: 281993 Color: 1
Size: 259954 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 441349 Color: 1
Size: 289161 Color: 1
Size: 269491 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 357767 Color: 1
Size: 327190 Color: 1
Size: 315044 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 370284 Color: 1
Size: 362276 Color: 1
Size: 267441 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 371207 Color: 1
Size: 331981 Color: 1
Size: 296813 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 425293 Color: 1
Size: 318560 Color: 1
Size: 256148 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 398830 Color: 1
Size: 312814 Color: 1
Size: 288357 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 402206 Color: 1
Size: 336087 Color: 1
Size: 261708 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 379207 Color: 1
Size: 319054 Color: 1
Size: 301740 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 490890 Color: 1
Size: 258177 Color: 1
Size: 250934 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 412154 Color: 1
Size: 304828 Color: 0
Size: 283019 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 395568 Color: 1
Size: 326312 Color: 1
Size: 278121 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 356921 Color: 1
Size: 347597 Color: 1
Size: 295483 Color: 0

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 447986 Color: 1
Size: 290942 Color: 1
Size: 261073 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 400820 Color: 1
Size: 326861 Color: 0
Size: 272320 Color: 1

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 456643 Color: 1
Size: 283809 Color: 1
Size: 259549 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 444603 Color: 1
Size: 294472 Color: 1
Size: 260926 Color: 0

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 368127 Color: 1
Size: 338839 Color: 1
Size: 293035 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 418757 Color: 1
Size: 301176 Color: 1
Size: 280068 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 359081 Color: 1
Size: 355152 Color: 1
Size: 285768 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 362338 Color: 1
Size: 349235 Color: 1
Size: 288428 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 467856 Color: 1
Size: 276881 Color: 1
Size: 255264 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 419718 Color: 1
Size: 328805 Color: 1
Size: 251478 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 463905 Color: 1
Size: 279339 Color: 1
Size: 256757 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 399425 Color: 1
Size: 339773 Color: 1
Size: 260803 Color: 0

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 434803 Color: 1
Size: 305366 Color: 1
Size: 259832 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 364148 Color: 1
Size: 330002 Color: 1
Size: 305851 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 397807 Color: 1
Size: 310186 Color: 1
Size: 292008 Color: 0

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 359994 Color: 1
Size: 332466 Color: 1
Size: 307541 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 362573 Color: 1
Size: 320402 Color: 1
Size: 317026 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 409857 Color: 1
Size: 303425 Color: 0
Size: 286719 Color: 1

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 404875 Color: 1
Size: 341579 Color: 1
Size: 253547 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 351313 Color: 1
Size: 347524 Color: 1
Size: 301164 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 451876 Color: 1
Size: 295527 Color: 1
Size: 252598 Color: 0

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 366153 Color: 1
Size: 321238 Color: 0
Size: 312610 Color: 1

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 359496 Color: 1
Size: 347498 Color: 1
Size: 293007 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 384200 Color: 1
Size: 330291 Color: 1
Size: 285510 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 420960 Color: 1
Size: 325149 Color: 1
Size: 253892 Color: 0

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 358722 Color: 1
Size: 338764 Color: 1
Size: 302515 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 358786 Color: 1
Size: 346169 Color: 1
Size: 295046 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 495701 Color: 1
Size: 254233 Color: 1
Size: 250067 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 473728 Color: 1
Size: 276065 Color: 1
Size: 250208 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 424053 Color: 1
Size: 322982 Color: 0
Size: 252966 Color: 1

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 375211 Color: 1
Size: 338281 Color: 1
Size: 286509 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 491505 Color: 1
Size: 254296 Color: 1
Size: 254200 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 354529 Color: 1
Size: 351088 Color: 1
Size: 294384 Color: 0

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 446392 Color: 1
Size: 291646 Color: 1
Size: 261963 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 408854 Color: 1
Size: 319597 Color: 1
Size: 271550 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 447436 Color: 1
Size: 288289 Color: 1
Size: 264276 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 355441 Color: 1
Size: 322802 Color: 0
Size: 321758 Color: 1

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 425484 Color: 1
Size: 319920 Color: 1
Size: 254597 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 461162 Color: 1
Size: 272363 Color: 1
Size: 266476 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 449727 Color: 1
Size: 297155 Color: 1
Size: 253119 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 476054 Color: 1
Size: 272727 Color: 1
Size: 251220 Color: 0

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 480819 Color: 1
Size: 266776 Color: 1
Size: 252406 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 353931 Color: 1
Size: 351476 Color: 1
Size: 294594 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 419760 Color: 1
Size: 313011 Color: 1
Size: 267230 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 447869 Color: 1
Size: 294350 Color: 1
Size: 257782 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 406781 Color: 1
Size: 325235 Color: 1
Size: 267985 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 467328 Color: 1
Size: 267607 Color: 1
Size: 265066 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 356478 Color: 1
Size: 323124 Color: 1
Size: 320399 Color: 0

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 475433 Color: 1
Size: 267879 Color: 1
Size: 256689 Color: 0

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 473104 Color: 1
Size: 276082 Color: 1
Size: 250815 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 411709 Color: 1
Size: 336278 Color: 1
Size: 252014 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 359228 Color: 1
Size: 356646 Color: 1
Size: 284127 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 385463 Color: 1
Size: 325439 Color: 1
Size: 289099 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 396072 Color: 1
Size: 340083 Color: 1
Size: 263846 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 356181 Color: 1
Size: 328912 Color: 1
Size: 314908 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 370100 Color: 1
Size: 361133 Color: 1
Size: 268768 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 414284 Color: 1
Size: 313117 Color: 1
Size: 272600 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 388570 Color: 1
Size: 344401 Color: 1
Size: 267030 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 347066 Color: 1
Size: 329209 Color: 1
Size: 323726 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 403850 Color: 1
Size: 332172 Color: 1
Size: 263979 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 446275 Color: 1
Size: 290889 Color: 0
Size: 262837 Color: 1

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 377722 Color: 1
Size: 359124 Color: 1
Size: 263155 Color: 0

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 357322 Color: 1
Size: 334964 Color: 1
Size: 307715 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 424221 Color: 1
Size: 316356 Color: 1
Size: 259424 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 384613 Color: 1
Size: 316744 Color: 0
Size: 298644 Color: 1

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 349054 Color: 1
Size: 336239 Color: 1
Size: 314708 Color: 0

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 354646 Color: 1
Size: 352066 Color: 1
Size: 293289 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 358384 Color: 1
Size: 355694 Color: 1
Size: 285923 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 377113 Color: 1
Size: 361892 Color: 1
Size: 260996 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 361037 Color: 1
Size: 327863 Color: 0
Size: 311101 Color: 1

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 379985 Color: 1
Size: 347318 Color: 1
Size: 272698 Color: 0

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 381800 Color: 1
Size: 363505 Color: 1
Size: 254696 Color: 0

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 461226 Color: 1
Size: 288114 Color: 1
Size: 250661 Color: 0

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 396653 Color: 1
Size: 305548 Color: 0
Size: 297800 Color: 1

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 380083 Color: 1
Size: 352137 Color: 1
Size: 267781 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 382767 Color: 1
Size: 338883 Color: 1
Size: 278351 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 381788 Color: 1
Size: 326497 Color: 1
Size: 291716 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 359097 Color: 1
Size: 344776 Color: 1
Size: 296128 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 495473 Color: 1
Size: 254525 Color: 1
Size: 250003 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 399897 Color: 1
Size: 312577 Color: 0
Size: 287527 Color: 1

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 369982 Color: 1
Size: 328375 Color: 1
Size: 301644 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 359691 Color: 1
Size: 344724 Color: 1
Size: 295586 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 355994 Color: 1
Size: 346389 Color: 1
Size: 297618 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 397258 Color: 1
Size: 304718 Color: 1
Size: 298025 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 355555 Color: 1
Size: 346926 Color: 1
Size: 297520 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 375527 Color: 1
Size: 328220 Color: 1
Size: 296254 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 357020 Color: 1
Size: 343754 Color: 1
Size: 299227 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 360803 Color: 1
Size: 346526 Color: 1
Size: 292672 Color: 0

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 361374 Color: 1
Size: 339470 Color: 1
Size: 299157 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 379616 Color: 1
Size: 347827 Color: 1
Size: 272558 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 361486 Color: 1
Size: 338444 Color: 1
Size: 300071 Color: 0

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 360176 Color: 1
Size: 326343 Color: 1
Size: 313482 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 362606 Color: 1
Size: 325073 Color: 0
Size: 312322 Color: 1

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 359706 Color: 1
Size: 350120 Color: 1
Size: 290175 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 360672 Color: 1
Size: 326080 Color: 1
Size: 313249 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 359757 Color: 1
Size: 344232 Color: 1
Size: 296012 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 425654 Color: 1
Size: 312453 Color: 1
Size: 261894 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 360407 Color: 1
Size: 320568 Color: 1
Size: 319026 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 354599 Color: 1
Size: 347664 Color: 1
Size: 297738 Color: 0

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 411723 Color: 1
Size: 303969 Color: 0
Size: 284309 Color: 1

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 405521 Color: 1
Size: 307233 Color: 0
Size: 287247 Color: 1

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 349780 Color: 1
Size: 338714 Color: 1
Size: 311507 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 361622 Color: 1
Size: 321830 Color: 0
Size: 316549 Color: 1

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 347832 Color: 1
Size: 346381 Color: 1
Size: 305788 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 361929 Color: 1
Size: 343402 Color: 1
Size: 294670 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 462134 Color: 1
Size: 285066 Color: 1
Size: 252801 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 352818 Color: 1
Size: 334185 Color: 1
Size: 312998 Color: 0

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 377044 Color: 1
Size: 339456 Color: 1
Size: 283501 Color: 0

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 347911 Color: 1
Size: 345446 Color: 1
Size: 306644 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 360375 Color: 1
Size: 323169 Color: 1
Size: 316457 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 353219 Color: 1
Size: 334004 Color: 1
Size: 312778 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 354799 Color: 1
Size: 342675 Color: 1
Size: 302527 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 360033 Color: 1
Size: 324202 Color: 1
Size: 315766 Color: 0

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 357340 Color: 1
Size: 332578 Color: 1
Size: 310083 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 416392 Color: 1
Size: 308091 Color: 1
Size: 275518 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 361982 Color: 1
Size: 361921 Color: 1
Size: 276098 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 359826 Color: 1
Size: 320686 Color: 1
Size: 319489 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 361706 Color: 1
Size: 325424 Color: 0
Size: 312871 Color: 1

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 372016 Color: 1
Size: 360040 Color: 1
Size: 267945 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 357699 Color: 1
Size: 321358 Color: 0
Size: 320944 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 386653 Color: 1
Size: 339803 Color: 1
Size: 273545 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 399485 Color: 1
Size: 343448 Color: 1
Size: 257068 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 359235 Color: 1
Size: 358396 Color: 1
Size: 282370 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 360397 Color: 1
Size: 360094 Color: 1
Size: 279510 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 359562 Color: 1
Size: 359531 Color: 1
Size: 280908 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 356455 Color: 1
Size: 329314 Color: 1
Size: 314232 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 356372 Color: 1
Size: 337591 Color: 1
Size: 306038 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 357600 Color: 1
Size: 342244 Color: 1
Size: 300157 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 392972 Color: 1
Size: 337043 Color: 1
Size: 269986 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 352584 Color: 1
Size: 346044 Color: 1
Size: 301373 Color: 0

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 353174 Color: 1
Size: 349011 Color: 1
Size: 297816 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 346025 Color: 1
Size: 343624 Color: 1
Size: 310352 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 347307 Color: 1
Size: 341042 Color: 1
Size: 311652 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 356650 Color: 1
Size: 325810 Color: 1
Size: 317541 Color: 0

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 392666 Color: 1
Size: 341231 Color: 1
Size: 266104 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 358076 Color: 1
Size: 357725 Color: 1
Size: 284200 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 356660 Color: 1
Size: 341460 Color: 1
Size: 301881 Color: 0

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 360601 Color: 1
Size: 322520 Color: 1
Size: 316880 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 353894 Color: 1
Size: 334164 Color: 1
Size: 311943 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 355891 Color: 1
Size: 330779 Color: 1
Size: 313331 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 360922 Color: 1
Size: 343983 Color: 1
Size: 295096 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 366806 Color: 1
Size: 323636 Color: 0
Size: 309559 Color: 1

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 357327 Color: 1
Size: 354215 Color: 1
Size: 288459 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 356681 Color: 1
Size: 341335 Color: 1
Size: 301985 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 354444 Color: 1
Size: 344799 Color: 1
Size: 300758 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 438359 Color: 1
Size: 306759 Color: 1
Size: 254883 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 361222 Color: 1
Size: 323801 Color: 0
Size: 314978 Color: 1

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 353167 Color: 1
Size: 337639 Color: 1
Size: 309195 Color: 0

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 360002 Color: 1
Size: 321344 Color: 0
Size: 318655 Color: 1

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 356624 Color: 1
Size: 338059 Color: 1
Size: 305318 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 357989 Color: 1
Size: 325489 Color: 1
Size: 316523 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 351083 Color: 1
Size: 326182 Color: 0
Size: 322736 Color: 1

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 400964 Color: 1
Size: 309142 Color: 0
Size: 289895 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 351479 Color: 1
Size: 350059 Color: 1
Size: 298463 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 417648 Color: 1
Size: 331221 Color: 1
Size: 251132 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 443475 Color: 1
Size: 287889 Color: 1
Size: 268637 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 357605 Color: 1
Size: 343139 Color: 1
Size: 299257 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 350372 Color: 1
Size: 342438 Color: 1
Size: 307191 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 349770 Color: 1
Size: 339308 Color: 1
Size: 310923 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 356763 Color: 1
Size: 334601 Color: 1
Size: 308637 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 350672 Color: 1
Size: 340939 Color: 1
Size: 308390 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 399090 Color: 1
Size: 310718 Color: 1
Size: 290193 Color: 0

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 344458 Color: 1
Size: 340948 Color: 1
Size: 314595 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 347425 Color: 1
Size: 338812 Color: 1
Size: 313764 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 347260 Color: 1
Size: 330881 Color: 1
Size: 321860 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 357632 Color: 1
Size: 334682 Color: 1
Size: 307687 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 387466 Color: 1
Size: 356265 Color: 1
Size: 256270 Color: 0

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 371594 Color: 1
Size: 318262 Color: 1
Size: 310145 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 352211 Color: 1
Size: 344113 Color: 1
Size: 303677 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 355381 Color: 1
Size: 336318 Color: 1
Size: 308302 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 353666 Color: 1
Size: 344646 Color: 1
Size: 301689 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 351577 Color: 1
Size: 334611 Color: 1
Size: 313813 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 354192 Color: 1
Size: 324285 Color: 1
Size: 321524 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 350288 Color: 1
Size: 344668 Color: 1
Size: 305045 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 360656 Color: 1
Size: 324909 Color: 1
Size: 314436 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 356164 Color: 1
Size: 322117 Color: 1
Size: 321720 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 362057 Color: 1
Size: 326969 Color: 1
Size: 310975 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 355341 Color: 1
Size: 343559 Color: 1
Size: 301101 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 354773 Color: 1
Size: 333622 Color: 1
Size: 311606 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 383633 Color: 1
Size: 358871 Color: 1
Size: 257497 Color: 0

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 354150 Color: 1
Size: 339588 Color: 1
Size: 306263 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 353152 Color: 1
Size: 341816 Color: 1
Size: 305033 Color: 0

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 352785 Color: 1
Size: 342297 Color: 1
Size: 304919 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 352024 Color: 1
Size: 351485 Color: 1
Size: 296492 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 351378 Color: 1
Size: 338627 Color: 1
Size: 309996 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 351261 Color: 1
Size: 342030 Color: 1
Size: 306710 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 351208 Color: 1
Size: 328114 Color: 1
Size: 320679 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 468360 Color: 1
Size: 272124 Color: 1
Size: 259517 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 350613 Color: 1
Size: 338678 Color: 1
Size: 310710 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 350409 Color: 1
Size: 330325 Color: 1
Size: 319267 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 350322 Color: 1
Size: 337768 Color: 1
Size: 311911 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 349523 Color: 1
Size: 343890 Color: 1
Size: 306588 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 348989 Color: 1
Size: 335185 Color: 1
Size: 315827 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 348765 Color: 1
Size: 328109 Color: 1
Size: 323127 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 348502 Color: 1
Size: 341997 Color: 1
Size: 309502 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 360604 Color: 1
Size: 326998 Color: 0
Size: 312399 Color: 1

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 361454 Color: 1
Size: 348730 Color: 1
Size: 289817 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 361880 Color: 1
Size: 349142 Color: 1
Size: 288979 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 362389 Color: 1
Size: 346230 Color: 1
Size: 291382 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 362455 Color: 1
Size: 341111 Color: 1
Size: 296435 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 362687 Color: 1
Size: 342239 Color: 1
Size: 295075 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 362842 Color: 1
Size: 337127 Color: 1
Size: 300032 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 362928 Color: 1
Size: 345336 Color: 1
Size: 291737 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 362964 Color: 1
Size: 337110 Color: 1
Size: 299927 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 363384 Color: 1
Size: 318579 Color: 1
Size: 318038 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 363406 Color: 1
Size: 341001 Color: 1
Size: 295594 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 363500 Color: 1
Size: 339209 Color: 1
Size: 297292 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 363538 Color: 1
Size: 341332 Color: 1
Size: 295131 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 363574 Color: 1
Size: 345687 Color: 1
Size: 290740 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 363623 Color: 1
Size: 353111 Color: 1
Size: 283267 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 363802 Color: 1
Size: 344695 Color: 1
Size: 291504 Color: 0

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 363827 Color: 1
Size: 340830 Color: 1
Size: 295344 Color: 0

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 363953 Color: 1
Size: 328998 Color: 0
Size: 307050 Color: 1

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 363979 Color: 1
Size: 329770 Color: 1
Size: 306252 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 364022 Color: 1
Size: 335226 Color: 1
Size: 300753 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 364055 Color: 1
Size: 348127 Color: 1
Size: 287819 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 364065 Color: 1
Size: 325582 Color: 1
Size: 310354 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 364094 Color: 1
Size: 333925 Color: 1
Size: 301982 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 364100 Color: 1
Size: 355794 Color: 1
Size: 280107 Color: 0

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 364358 Color: 1
Size: 326463 Color: 0
Size: 309180 Color: 1

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 364406 Color: 1
Size: 333776 Color: 1
Size: 301819 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 364471 Color: 1
Size: 318800 Color: 1
Size: 316730 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 364509 Color: 1
Size: 319164 Color: 0
Size: 316328 Color: 1

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 364525 Color: 1
Size: 346408 Color: 1
Size: 289068 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 364692 Color: 1
Size: 353298 Color: 1
Size: 282011 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 364708 Color: 1
Size: 346916 Color: 1
Size: 288377 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 364723 Color: 1
Size: 328334 Color: 0
Size: 306944 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 364834 Color: 1
Size: 343286 Color: 1
Size: 291881 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 364834 Color: 1
Size: 342486 Color: 1
Size: 292681 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 364838 Color: 1
Size: 329740 Color: 1
Size: 305423 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 364918 Color: 1
Size: 337453 Color: 1
Size: 297630 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 365066 Color: 1
Size: 320178 Color: 1
Size: 314757 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 365120 Color: 1
Size: 347378 Color: 1
Size: 287503 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 365133 Color: 1
Size: 346154 Color: 1
Size: 288714 Color: 0

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 365158 Color: 1
Size: 326217 Color: 1
Size: 308626 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 365189 Color: 1
Size: 323896 Color: 1
Size: 310916 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 365300 Color: 1
Size: 327777 Color: 0
Size: 306924 Color: 1

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 365345 Color: 1
Size: 321915 Color: 0
Size: 312741 Color: 1

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 365402 Color: 1
Size: 330994 Color: 1
Size: 303605 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 365468 Color: 1
Size: 344844 Color: 1
Size: 289689 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 365595 Color: 1
Size: 351790 Color: 1
Size: 282616 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 365701 Color: 1
Size: 328284 Color: 1
Size: 306016 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 365753 Color: 1
Size: 326054 Color: 1
Size: 308194 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 365754 Color: 1
Size: 325994 Color: 1
Size: 308253 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 365891 Color: 1
Size: 359964 Color: 1
Size: 274146 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 365910 Color: 1
Size: 353650 Color: 1
Size: 280441 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 365959 Color: 1
Size: 336713 Color: 1
Size: 297329 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 366003 Color: 1
Size: 345846 Color: 1
Size: 288152 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 366068 Color: 1
Size: 332908 Color: 1
Size: 301025 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 366117 Color: 1
Size: 348041 Color: 1
Size: 285843 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 366154 Color: 1
Size: 322165 Color: 0
Size: 311682 Color: 1

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 366160 Color: 1
Size: 335652 Color: 1
Size: 298189 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 366463 Color: 1
Size: 333100 Color: 1
Size: 300438 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 366498 Color: 1
Size: 356512 Color: 1
Size: 276991 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 366550 Color: 1
Size: 343407 Color: 1
Size: 290044 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 366612 Color: 1
Size: 341468 Color: 1
Size: 291921 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 366713 Color: 1
Size: 349715 Color: 1
Size: 283573 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 366838 Color: 1
Size: 347114 Color: 1
Size: 286049 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 366933 Color: 1
Size: 323745 Color: 0
Size: 309323 Color: 1

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 366935 Color: 1
Size: 351952 Color: 1
Size: 281114 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 367009 Color: 1
Size: 340389 Color: 1
Size: 292603 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 367034 Color: 1
Size: 339443 Color: 1
Size: 293524 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 367038 Color: 1
Size: 317992 Color: 0
Size: 314971 Color: 1

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 367070 Color: 1
Size: 337728 Color: 1
Size: 295203 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 367088 Color: 1
Size: 342399 Color: 1
Size: 290514 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 367103 Color: 1
Size: 327608 Color: 1
Size: 305290 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 367213 Color: 1
Size: 331339 Color: 1
Size: 301449 Color: 0

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 367220 Color: 1
Size: 351517 Color: 1
Size: 281264 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 367242 Color: 1
Size: 355034 Color: 1
Size: 277725 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 367251 Color: 1
Size: 319473 Color: 0
Size: 313277 Color: 1

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 367398 Color: 1
Size: 343078 Color: 1
Size: 289525 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 367452 Color: 1
Size: 343052 Color: 1
Size: 289497 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 367493 Color: 1
Size: 349510 Color: 1
Size: 282998 Color: 0

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 367497 Color: 1
Size: 348821 Color: 1
Size: 283683 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 367544 Color: 1
Size: 363674 Color: 1
Size: 268783 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 367575 Color: 1
Size: 326188 Color: 1
Size: 306238 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 367597 Color: 1
Size: 343341 Color: 1
Size: 289063 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 367628 Color: 1
Size: 330657 Color: 1
Size: 301716 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 367661 Color: 1
Size: 341353 Color: 1
Size: 290987 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 367695 Color: 1
Size: 358752 Color: 1
Size: 273554 Color: 0

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 367699 Color: 1
Size: 332678 Color: 1
Size: 299624 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 367730 Color: 1
Size: 319415 Color: 1
Size: 312856 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 367776 Color: 1
Size: 355815 Color: 1
Size: 276410 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 367832 Color: 1
Size: 341374 Color: 1
Size: 290795 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 367879 Color: 1
Size: 335402 Color: 1
Size: 296720 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 368025 Color: 1
Size: 333338 Color: 1
Size: 298638 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 368070 Color: 1
Size: 331768 Color: 1
Size: 300163 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 368269 Color: 1
Size: 360206 Color: 1
Size: 271526 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 368270 Color: 1
Size: 355744 Color: 1
Size: 275987 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 368274 Color: 1
Size: 337082 Color: 1
Size: 294645 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 368296 Color: 1
Size: 350243 Color: 1
Size: 281462 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 368322 Color: 1
Size: 325128 Color: 1
Size: 306551 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 368360 Color: 1
Size: 327962 Color: 1
Size: 303679 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 368429 Color: 1
Size: 333854 Color: 1
Size: 297718 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 368582 Color: 1
Size: 319430 Color: 0
Size: 311989 Color: 1

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 368653 Color: 1
Size: 316616 Color: 0
Size: 314732 Color: 1

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 368689 Color: 1
Size: 321773 Color: 0
Size: 309539 Color: 1

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 368720 Color: 1
Size: 328720 Color: 1
Size: 302561 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 368784 Color: 1
Size: 361276 Color: 1
Size: 269941 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 368817 Color: 1
Size: 331513 Color: 1
Size: 299671 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 368857 Color: 1
Size: 321601 Color: 0
Size: 309543 Color: 1

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 369065 Color: 1
Size: 354689 Color: 1
Size: 276247 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 369104 Color: 1
Size: 339193 Color: 1
Size: 291704 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 369110 Color: 1
Size: 341087 Color: 1
Size: 289804 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 369120 Color: 1
Size: 333210 Color: 1
Size: 297671 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 369191 Color: 1
Size: 338540 Color: 1
Size: 292270 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 369208 Color: 1
Size: 331906 Color: 1
Size: 298887 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 369290 Color: 1
Size: 344344 Color: 1
Size: 286367 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 369318 Color: 1
Size: 343968 Color: 1
Size: 286715 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 369396 Color: 1
Size: 361984 Color: 1
Size: 268621 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 369466 Color: 1
Size: 357962 Color: 1
Size: 272573 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 369499 Color: 1
Size: 329844 Color: 1
Size: 300658 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 369620 Color: 1
Size: 349923 Color: 1
Size: 280458 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 369668 Color: 1
Size: 337311 Color: 1
Size: 293022 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 369749 Color: 1
Size: 339117 Color: 1
Size: 291135 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 369770 Color: 1
Size: 335594 Color: 1
Size: 294637 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 369792 Color: 1
Size: 354280 Color: 1
Size: 275929 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 369808 Color: 1
Size: 351264 Color: 1
Size: 278929 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 369866 Color: 1
Size: 350641 Color: 1
Size: 279494 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 369907 Color: 1
Size: 320774 Color: 1
Size: 309320 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 369948 Color: 1
Size: 343300 Color: 1
Size: 286753 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 370016 Color: 1
Size: 332816 Color: 1
Size: 297169 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 370048 Color: 1
Size: 345705 Color: 1
Size: 284248 Color: 0

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 370114 Color: 1
Size: 353091 Color: 1
Size: 276796 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 370173 Color: 1
Size: 323495 Color: 0
Size: 306333 Color: 1

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 370249 Color: 1
Size: 334643 Color: 1
Size: 295109 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 410646 Color: 1
Size: 316745 Color: 1
Size: 272610 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 370286 Color: 1
Size: 348301 Color: 1
Size: 281414 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 370286 Color: 1
Size: 340982 Color: 1
Size: 288733 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 370286 Color: 1
Size: 355847 Color: 1
Size: 273868 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 370299 Color: 1
Size: 351216 Color: 1
Size: 278486 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 370342 Color: 1
Size: 332104 Color: 1
Size: 297555 Color: 0

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 370436 Color: 1
Size: 348890 Color: 1
Size: 280675 Color: 0

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 370468 Color: 1
Size: 334458 Color: 1
Size: 295075 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 370478 Color: 1
Size: 342721 Color: 1
Size: 286802 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 370509 Color: 1
Size: 325857 Color: 1
Size: 303635 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 370512 Color: 1
Size: 333555 Color: 1
Size: 295934 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 370572 Color: 1
Size: 357446 Color: 1
Size: 271983 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 370610 Color: 1
Size: 351626 Color: 1
Size: 277765 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 370611 Color: 1
Size: 335956 Color: 1
Size: 293434 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 370670 Color: 1
Size: 350447 Color: 1
Size: 278884 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 370744 Color: 1
Size: 368971 Color: 1
Size: 260286 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 370760 Color: 1
Size: 344749 Color: 1
Size: 284492 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 370762 Color: 1
Size: 351811 Color: 1
Size: 277428 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 370797 Color: 1
Size: 351858 Color: 1
Size: 277346 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 370820 Color: 1
Size: 337325 Color: 1
Size: 291856 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 370884 Color: 1
Size: 340233 Color: 1
Size: 288884 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 370913 Color: 1
Size: 342209 Color: 1
Size: 286879 Color: 0

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 370913 Color: 1
Size: 323206 Color: 1
Size: 305882 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 370982 Color: 1
Size: 354332 Color: 1
Size: 274687 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 371003 Color: 1
Size: 342910 Color: 1
Size: 286088 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 371045 Color: 1
Size: 321688 Color: 1
Size: 307268 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 371096 Color: 1
Size: 323221 Color: 0
Size: 305684 Color: 1

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 371129 Color: 1
Size: 335420 Color: 1
Size: 293452 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 371251 Color: 1
Size: 350416 Color: 1
Size: 278334 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 371261 Color: 1
Size: 329230 Color: 0
Size: 299510 Color: 1

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 371261 Color: 1
Size: 315295 Color: 1
Size: 313445 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 371292 Color: 1
Size: 340862 Color: 1
Size: 287847 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 371321 Color: 1
Size: 343628 Color: 1
Size: 285052 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 371341 Color: 1
Size: 334310 Color: 1
Size: 294350 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 371421 Color: 1
Size: 348933 Color: 1
Size: 279647 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 371477 Color: 1
Size: 336940 Color: 1
Size: 291584 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 371480 Color: 1
Size: 335969 Color: 1
Size: 292552 Color: 0

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 371507 Color: 1
Size: 343928 Color: 1
Size: 284566 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 371551 Color: 1
Size: 350440 Color: 1
Size: 278010 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 371605 Color: 1
Size: 332406 Color: 1
Size: 295990 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 371656 Color: 1
Size: 335741 Color: 1
Size: 292604 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 371659 Color: 1
Size: 340151 Color: 1
Size: 288191 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 371790 Color: 1
Size: 351670 Color: 1
Size: 276541 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 371992 Color: 1
Size: 332331 Color: 1
Size: 295678 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 372001 Color: 1
Size: 322520 Color: 0
Size: 305480 Color: 1

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 372009 Color: 1
Size: 342349 Color: 1
Size: 285643 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 372051 Color: 1
Size: 318195 Color: 0
Size: 309755 Color: 1

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 372078 Color: 1
Size: 336644 Color: 1
Size: 291279 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 372173 Color: 1
Size: 353330 Color: 1
Size: 274498 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 372204 Color: 1
Size: 341170 Color: 1
Size: 286627 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 372204 Color: 1
Size: 340684 Color: 1
Size: 287113 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 372354 Color: 1
Size: 337742 Color: 1
Size: 289905 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 372420 Color: 1
Size: 351441 Color: 1
Size: 276140 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 372447 Color: 1
Size: 340121 Color: 1
Size: 287433 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 372534 Color: 1
Size: 366100 Color: 1
Size: 261367 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 372584 Color: 1
Size: 353252 Color: 1
Size: 274165 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 372620 Color: 1
Size: 328922 Color: 1
Size: 298459 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 372621 Color: 1
Size: 367539 Color: 1
Size: 259841 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 372666 Color: 1
Size: 339383 Color: 1
Size: 287952 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 404087 Color: 1
Size: 341360 Color: 1
Size: 254554 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 372742 Color: 1
Size: 335613 Color: 1
Size: 291646 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 372807 Color: 1
Size: 315327 Color: 1
Size: 311867 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 372823 Color: 1
Size: 328407 Color: 1
Size: 298771 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 372889 Color: 1
Size: 343053 Color: 1
Size: 284059 Color: 0

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 372931 Color: 1
Size: 357290 Color: 1
Size: 269780 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 372942 Color: 1
Size: 349122 Color: 1
Size: 277937 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 372963 Color: 1
Size: 341559 Color: 1
Size: 285479 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 372974 Color: 1
Size: 329878 Color: 1
Size: 297149 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 373102 Color: 1
Size: 372012 Color: 1
Size: 254887 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 373174 Color: 1
Size: 346558 Color: 1
Size: 280269 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 373218 Color: 1
Size: 338918 Color: 1
Size: 287865 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 373321 Color: 1
Size: 345648 Color: 1
Size: 281032 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 373323 Color: 1
Size: 340505 Color: 1
Size: 286173 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 373342 Color: 1
Size: 315571 Color: 0
Size: 311088 Color: 1

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 373393 Color: 1
Size: 340532 Color: 1
Size: 286076 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 373398 Color: 1
Size: 343501 Color: 1
Size: 283102 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 373442 Color: 1
Size: 350666 Color: 1
Size: 275893 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 373465 Color: 1
Size: 352910 Color: 1
Size: 273626 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 358080 Color: 1
Size: 354172 Color: 1
Size: 287749 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 373504 Color: 1
Size: 345860 Color: 1
Size: 280637 Color: 0

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 373608 Color: 1
Size: 340040 Color: 1
Size: 286353 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 373636 Color: 1
Size: 315858 Color: 0
Size: 310507 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 373664 Color: 1
Size: 350443 Color: 1
Size: 275894 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 373669 Color: 1
Size: 346492 Color: 1
Size: 279840 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 373850 Color: 1
Size: 340405 Color: 1
Size: 285746 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 373967 Color: 1
Size: 343378 Color: 1
Size: 282656 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 373982 Color: 1
Size: 342614 Color: 1
Size: 283405 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 373987 Color: 1
Size: 327347 Color: 1
Size: 298667 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 374028 Color: 1
Size: 347885 Color: 1
Size: 278088 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 374111 Color: 1
Size: 363820 Color: 1
Size: 262070 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 374154 Color: 1
Size: 346978 Color: 1
Size: 278869 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 374162 Color: 1
Size: 325018 Color: 1
Size: 300821 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 374203 Color: 1
Size: 352148 Color: 1
Size: 273650 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 374220 Color: 1
Size: 335817 Color: 1
Size: 289964 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 374252 Color: 1
Size: 317275 Color: 1
Size: 308474 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 374281 Color: 1
Size: 339427 Color: 1
Size: 286293 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 438571 Color: 1
Size: 282903 Color: 1
Size: 278527 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 374323 Color: 1
Size: 344857 Color: 1
Size: 280821 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 374343 Color: 1
Size: 372169 Color: 1
Size: 253489 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 374427 Color: 1
Size: 331456 Color: 1
Size: 294118 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 374519 Color: 1
Size: 334902 Color: 1
Size: 290580 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 374527 Color: 1
Size: 335723 Color: 1
Size: 289751 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 374533 Color: 1
Size: 359592 Color: 1
Size: 265876 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 374544 Color: 1
Size: 335773 Color: 1
Size: 289684 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 374603 Color: 1
Size: 369298 Color: 1
Size: 256100 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 374611 Color: 1
Size: 352779 Color: 1
Size: 272611 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 374629 Color: 1
Size: 317100 Color: 1
Size: 308272 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 374665 Color: 1
Size: 354331 Color: 1
Size: 271005 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 374725 Color: 1
Size: 332485 Color: 1
Size: 292791 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 374729 Color: 1
Size: 357247 Color: 1
Size: 268025 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 374771 Color: 1
Size: 348208 Color: 1
Size: 277022 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 374877 Color: 1
Size: 352158 Color: 1
Size: 272966 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 375005 Color: 1
Size: 352755 Color: 1
Size: 272241 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 375081 Color: 1
Size: 328918 Color: 1
Size: 296002 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 375174 Color: 1
Size: 333748 Color: 1
Size: 291079 Color: 0

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 375240 Color: 1
Size: 333392 Color: 1
Size: 291369 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 375294 Color: 1
Size: 349791 Color: 1
Size: 274916 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 375335 Color: 1
Size: 336185 Color: 1
Size: 288481 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 375387 Color: 1
Size: 327643 Color: 1
Size: 296971 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 375442 Color: 1
Size: 346967 Color: 1
Size: 277592 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 375449 Color: 1
Size: 360864 Color: 1
Size: 263688 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 375492 Color: 1
Size: 345830 Color: 1
Size: 278679 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 375515 Color: 1
Size: 332677 Color: 1
Size: 291809 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 375541 Color: 1
Size: 328810 Color: 1
Size: 295650 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 375561 Color: 1
Size: 323712 Color: 0
Size: 300728 Color: 1

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 375680 Color: 1
Size: 367844 Color: 1
Size: 256477 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 375732 Color: 1
Size: 330305 Color: 1
Size: 293964 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 375762 Color: 1
Size: 341643 Color: 1
Size: 282596 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 375832 Color: 1
Size: 357745 Color: 1
Size: 266424 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 375838 Color: 1
Size: 343774 Color: 1
Size: 280389 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 375862 Color: 1
Size: 344232 Color: 1
Size: 279907 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 375865 Color: 1
Size: 325907 Color: 1
Size: 298229 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 375927 Color: 1
Size: 330241 Color: 1
Size: 293833 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 375980 Color: 1
Size: 334086 Color: 1
Size: 289935 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 375998 Color: 1
Size: 351292 Color: 1
Size: 272711 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 376000 Color: 1
Size: 335227 Color: 1
Size: 288774 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 376093 Color: 1
Size: 323862 Color: 0
Size: 300046 Color: 1

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 376103 Color: 1
Size: 342539 Color: 1
Size: 281359 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 376146 Color: 1
Size: 360518 Color: 1
Size: 263337 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 376220 Color: 1
Size: 331665 Color: 1
Size: 292116 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 376235 Color: 1
Size: 316180 Color: 0
Size: 307586 Color: 1

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 376237 Color: 1
Size: 330991 Color: 1
Size: 292773 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 376314 Color: 1
Size: 339453 Color: 1
Size: 284234 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 376341 Color: 1
Size: 334328 Color: 1
Size: 289332 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 376408 Color: 1
Size: 353957 Color: 1
Size: 269636 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 376461 Color: 1
Size: 352027 Color: 1
Size: 271513 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 376495 Color: 1
Size: 356554 Color: 1
Size: 266952 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 376515 Color: 1
Size: 318940 Color: 1
Size: 304546 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 376519 Color: 1
Size: 340210 Color: 1
Size: 283272 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 376687 Color: 1
Size: 356097 Color: 1
Size: 267217 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 376691 Color: 1
Size: 325724 Color: 1
Size: 297586 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 376731 Color: 1
Size: 337949 Color: 1
Size: 285321 Color: 0

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 376804 Color: 1
Size: 315034 Color: 1
Size: 308163 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 376925 Color: 1
Size: 323569 Color: 1
Size: 299507 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 376930 Color: 1
Size: 335987 Color: 1
Size: 287084 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 376973 Color: 1
Size: 352379 Color: 1
Size: 270649 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 377020 Color: 1
Size: 322637 Color: 1
Size: 300344 Color: 0

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 377126 Color: 1
Size: 353606 Color: 1
Size: 269269 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 377139 Color: 1
Size: 324225 Color: 1
Size: 298637 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 377174 Color: 1
Size: 351080 Color: 1
Size: 271747 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 377231 Color: 1
Size: 329701 Color: 1
Size: 293069 Color: 0

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 377360 Color: 1
Size: 366039 Color: 1
Size: 256602 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 377372 Color: 1
Size: 341226 Color: 1
Size: 281403 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 377448 Color: 1
Size: 328176 Color: 1
Size: 294377 Color: 0

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 377489 Color: 1
Size: 326852 Color: 1
Size: 295660 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 377496 Color: 1
Size: 357471 Color: 1
Size: 265034 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 377554 Color: 1
Size: 331557 Color: 1
Size: 290890 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 377701 Color: 1
Size: 313208 Color: 1
Size: 309092 Color: 0

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 377762 Color: 1
Size: 351809 Color: 1
Size: 270430 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 377771 Color: 1
Size: 327156 Color: 1
Size: 295074 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 377789 Color: 1
Size: 343991 Color: 1
Size: 278221 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 377795 Color: 1
Size: 346560 Color: 1
Size: 275646 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 377911 Color: 1
Size: 344293 Color: 1
Size: 277797 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 377922 Color: 1
Size: 338596 Color: 1
Size: 283483 Color: 0

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 378044 Color: 1
Size: 330793 Color: 1
Size: 291164 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 378048 Color: 1
Size: 341900 Color: 1
Size: 280053 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 378203 Color: 1
Size: 342977 Color: 1
Size: 278821 Color: 0

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 378226 Color: 1
Size: 314613 Color: 0
Size: 307162 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 378276 Color: 1
Size: 341033 Color: 1
Size: 280692 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 378326 Color: 1
Size: 322049 Color: 1
Size: 299626 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 378528 Color: 1
Size: 329940 Color: 1
Size: 291533 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 378601 Color: 1
Size: 323420 Color: 1
Size: 297980 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 378698 Color: 1
Size: 342469 Color: 1
Size: 278834 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 378716 Color: 1
Size: 311359 Color: 1
Size: 309926 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 378726 Color: 1
Size: 347153 Color: 1
Size: 274122 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 378740 Color: 1
Size: 345641 Color: 1
Size: 275620 Color: 0

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 378840 Color: 1
Size: 331265 Color: 1
Size: 289896 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 378846 Color: 1
Size: 327436 Color: 1
Size: 293719 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 378869 Color: 1
Size: 316904 Color: 1
Size: 304228 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 378897 Color: 1
Size: 335979 Color: 1
Size: 285125 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 378917 Color: 1
Size: 340945 Color: 1
Size: 280139 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 378940 Color: 1
Size: 327722 Color: 1
Size: 293339 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 378950 Color: 1
Size: 318380 Color: 0
Size: 302671 Color: 1

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 378966 Color: 1
Size: 343531 Color: 1
Size: 277504 Color: 0

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 378995 Color: 1
Size: 317561 Color: 1
Size: 303445 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 379164 Color: 1
Size: 316426 Color: 1
Size: 304411 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 379168 Color: 1
Size: 329776 Color: 1
Size: 291057 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 379218 Color: 1
Size: 333516 Color: 1
Size: 287267 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 379221 Color: 1
Size: 330256 Color: 1
Size: 290524 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 379245 Color: 1
Size: 335720 Color: 1
Size: 285036 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 379307 Color: 1
Size: 342192 Color: 1
Size: 278502 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 379309 Color: 1
Size: 349972 Color: 1
Size: 270720 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 379444 Color: 1
Size: 327282 Color: 1
Size: 293275 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 379492 Color: 1
Size: 321240 Color: 1
Size: 299269 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 379559 Color: 1
Size: 335859 Color: 1
Size: 284583 Color: 0

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 379648 Color: 1
Size: 343218 Color: 1
Size: 277135 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 379676 Color: 1
Size: 337212 Color: 1
Size: 283113 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 379789 Color: 1
Size: 318401 Color: 0
Size: 301811 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 379791 Color: 1
Size: 367173 Color: 1
Size: 253037 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 379854 Color: 1
Size: 345149 Color: 1
Size: 274998 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 379864 Color: 1
Size: 320784 Color: 1
Size: 299353 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 379910 Color: 1
Size: 345203 Color: 1
Size: 274888 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 379988 Color: 1
Size: 331480 Color: 1
Size: 288533 Color: 0

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 380025 Color: 1
Size: 324551 Color: 1
Size: 295425 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 380041 Color: 1
Size: 343325 Color: 1
Size: 276635 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 380188 Color: 1
Size: 344397 Color: 1
Size: 275416 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 380283 Color: 1
Size: 338920 Color: 1
Size: 280798 Color: 0

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 380311 Color: 1
Size: 316866 Color: 0
Size: 302824 Color: 1

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 380411 Color: 1
Size: 334053 Color: 1
Size: 285537 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 380421 Color: 1
Size: 317510 Color: 1
Size: 302070 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 380458 Color: 1
Size: 320737 Color: 1
Size: 298806 Color: 0

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 380618 Color: 1
Size: 342984 Color: 1
Size: 276399 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 380631 Color: 1
Size: 342851 Color: 1
Size: 276519 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 380666 Color: 1
Size: 349562 Color: 1
Size: 269773 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 380757 Color: 1
Size: 321816 Color: 1
Size: 297428 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 380816 Color: 1
Size: 334143 Color: 1
Size: 285042 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 380946 Color: 1
Size: 343462 Color: 1
Size: 275593 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 380958 Color: 1
Size: 333626 Color: 1
Size: 285417 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 380984 Color: 1
Size: 339801 Color: 1
Size: 279216 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 381001 Color: 1
Size: 354692 Color: 1
Size: 264308 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 381006 Color: 1
Size: 346566 Color: 1
Size: 272429 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 381076 Color: 1
Size: 347459 Color: 1
Size: 271466 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 381129 Color: 1
Size: 337416 Color: 1
Size: 281456 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 381189 Color: 1
Size: 334442 Color: 1
Size: 284370 Color: 0

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 381212 Color: 1
Size: 346188 Color: 1
Size: 272601 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 381273 Color: 1
Size: 318901 Color: 1
Size: 299827 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 381297 Color: 1
Size: 357599 Color: 1
Size: 261105 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 381307 Color: 1
Size: 344905 Color: 1
Size: 273789 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 381405 Color: 1
Size: 334971 Color: 1
Size: 283625 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 381436 Color: 1
Size: 366628 Color: 1
Size: 251937 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 381583 Color: 1
Size: 331830 Color: 1
Size: 286588 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 381610 Color: 1
Size: 331059 Color: 1
Size: 287332 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 381705 Color: 1
Size: 312656 Color: 1
Size: 305640 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 381706 Color: 1
Size: 336923 Color: 1
Size: 281372 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 381727 Color: 1
Size: 336743 Color: 1
Size: 281531 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 381760 Color: 1
Size: 319440 Color: 1
Size: 298801 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 381787 Color: 1
Size: 346844 Color: 1
Size: 271370 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 381847 Color: 1
Size: 344083 Color: 1
Size: 274071 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 381861 Color: 1
Size: 309300 Color: 1
Size: 308840 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 381914 Color: 1
Size: 358123 Color: 1
Size: 259964 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 382025 Color: 1
Size: 334833 Color: 1
Size: 283143 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 382062 Color: 1
Size: 355823 Color: 1
Size: 262116 Color: 0

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 382083 Color: 1
Size: 357778 Color: 1
Size: 260140 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 382088 Color: 1
Size: 348156 Color: 1
Size: 269757 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 382111 Color: 1
Size: 361043 Color: 1
Size: 256847 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 382173 Color: 1
Size: 361993 Color: 1
Size: 255835 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 382214 Color: 1
Size: 311893 Color: 1
Size: 305894 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 382216 Color: 1
Size: 339172 Color: 1
Size: 278613 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 382255 Color: 1
Size: 345399 Color: 1
Size: 272347 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 382299 Color: 1
Size: 331034 Color: 1
Size: 286668 Color: 0

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 382307 Color: 1
Size: 348642 Color: 1
Size: 269052 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 382327 Color: 1
Size: 348309 Color: 1
Size: 269365 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 382331 Color: 1
Size: 343326 Color: 1
Size: 274344 Color: 0

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 382351 Color: 1
Size: 329247 Color: 1
Size: 288403 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 382372 Color: 1
Size: 324203 Color: 1
Size: 293426 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 382452 Color: 1
Size: 318895 Color: 1
Size: 298654 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 382485 Color: 1
Size: 336986 Color: 1
Size: 280530 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 382548 Color: 1
Size: 344336 Color: 1
Size: 273117 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 382551 Color: 1
Size: 325386 Color: 1
Size: 292064 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 382554 Color: 1
Size: 356243 Color: 1
Size: 261204 Color: 0

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 382594 Color: 1
Size: 355371 Color: 1
Size: 262036 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 382609 Color: 1
Size: 338337 Color: 1
Size: 279055 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 382642 Color: 1
Size: 341007 Color: 1
Size: 276352 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 382683 Color: 1
Size: 312820 Color: 1
Size: 304498 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 382683 Color: 1
Size: 346399 Color: 1
Size: 270919 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 382749 Color: 1
Size: 333098 Color: 1
Size: 284154 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 382764 Color: 1
Size: 329808 Color: 1
Size: 287429 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 382839 Color: 1
Size: 345331 Color: 1
Size: 271831 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 382858 Color: 1
Size: 323929 Color: 1
Size: 293214 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 382939 Color: 1
Size: 325407 Color: 1
Size: 291655 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 382981 Color: 1
Size: 331799 Color: 1
Size: 285221 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 383057 Color: 1
Size: 315741 Color: 1
Size: 301203 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 383118 Color: 1
Size: 325366 Color: 1
Size: 291517 Color: 0

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 383142 Color: 1
Size: 318533 Color: 1
Size: 298326 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 383276 Color: 1
Size: 332999 Color: 1
Size: 283726 Color: 0

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 383351 Color: 1
Size: 340091 Color: 1
Size: 276559 Color: 0

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 383360 Color: 1
Size: 354062 Color: 1
Size: 262579 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 383438 Color: 1
Size: 353387 Color: 1
Size: 263176 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 383442 Color: 1
Size: 334303 Color: 1
Size: 282256 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 383454 Color: 1
Size: 334829 Color: 1
Size: 281718 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 383494 Color: 1
Size: 332634 Color: 1
Size: 283873 Color: 0

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 383528 Color: 1
Size: 315736 Color: 1
Size: 300737 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 383601 Color: 1
Size: 340709 Color: 1
Size: 275691 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 383721 Color: 1
Size: 327822 Color: 1
Size: 288458 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 383727 Color: 1
Size: 324856 Color: 1
Size: 291418 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 383896 Color: 1
Size: 342141 Color: 1
Size: 273964 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 383944 Color: 1
Size: 327767 Color: 1
Size: 288290 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 383989 Color: 1
Size: 328755 Color: 1
Size: 287257 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 384003 Color: 1
Size: 348128 Color: 1
Size: 267870 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 384078 Color: 1
Size: 351917 Color: 1
Size: 264006 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 384108 Color: 1
Size: 339526 Color: 1
Size: 276367 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 384168 Color: 1
Size: 348528 Color: 1
Size: 267305 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 384176 Color: 1
Size: 347255 Color: 1
Size: 268570 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 384219 Color: 1
Size: 353963 Color: 1
Size: 261819 Color: 0

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 384221 Color: 1
Size: 317490 Color: 1
Size: 298290 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 384224 Color: 1
Size: 346818 Color: 1
Size: 268959 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 384276 Color: 1
Size: 317515 Color: 1
Size: 298210 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 384310 Color: 1
Size: 345608 Color: 1
Size: 270083 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 384383 Color: 1
Size: 345206 Color: 1
Size: 270412 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 384410 Color: 1
Size: 330466 Color: 1
Size: 285125 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 384444 Color: 1
Size: 357348 Color: 1
Size: 258209 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 384465 Color: 1
Size: 333382 Color: 1
Size: 282154 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 384467 Color: 1
Size: 319097 Color: 0
Size: 296437 Color: 1

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 384470 Color: 1
Size: 333126 Color: 1
Size: 282405 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 384487 Color: 1
Size: 313448 Color: 1
Size: 302066 Color: 0

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 384650 Color: 1
Size: 333490 Color: 1
Size: 281861 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 384667 Color: 1
Size: 322315 Color: 1
Size: 293019 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 384684 Color: 1
Size: 343184 Color: 1
Size: 272133 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 384720 Color: 1
Size: 352061 Color: 1
Size: 263220 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 384739 Color: 1
Size: 353296 Color: 1
Size: 261966 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 384744 Color: 1
Size: 318576 Color: 1
Size: 296681 Color: 0

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 384757 Color: 1
Size: 317698 Color: 1
Size: 297546 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 384758 Color: 1
Size: 336908 Color: 1
Size: 278335 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 384823 Color: 1
Size: 345674 Color: 1
Size: 269504 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 384857 Color: 1
Size: 353549 Color: 1
Size: 261595 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 384884 Color: 1
Size: 345696 Color: 1
Size: 269421 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 384901 Color: 1
Size: 327613 Color: 1
Size: 287487 Color: 0

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 384919 Color: 1
Size: 324112 Color: 1
Size: 290970 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 384941 Color: 1
Size: 346449 Color: 1
Size: 268611 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 384958 Color: 1
Size: 350071 Color: 1
Size: 264972 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 384985 Color: 1
Size: 348975 Color: 1
Size: 266041 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 385110 Color: 1
Size: 312591 Color: 1
Size: 302300 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 385125 Color: 1
Size: 338989 Color: 1
Size: 275887 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 385141 Color: 1
Size: 313120 Color: 0
Size: 301740 Color: 1

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 385150 Color: 1
Size: 334869 Color: 1
Size: 279982 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 385158 Color: 1
Size: 347592 Color: 1
Size: 267251 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 385217 Color: 1
Size: 309107 Color: 0
Size: 305677 Color: 1

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 385233 Color: 1
Size: 330331 Color: 1
Size: 284437 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 385280 Color: 1
Size: 322658 Color: 1
Size: 292063 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 385292 Color: 1
Size: 337415 Color: 1
Size: 277294 Color: 0

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 385516 Color: 1
Size: 328186 Color: 1
Size: 286299 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 385520 Color: 1
Size: 308179 Color: 0
Size: 306302 Color: 1

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 385529 Color: 1
Size: 308788 Color: 0
Size: 305684 Color: 1

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 385579 Color: 1
Size: 346923 Color: 1
Size: 267499 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 385687 Color: 1
Size: 336703 Color: 1
Size: 277611 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 385861 Color: 1
Size: 322681 Color: 1
Size: 291459 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 385880 Color: 1
Size: 342220 Color: 1
Size: 271901 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 385970 Color: 1
Size: 335251 Color: 1
Size: 278780 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 386010 Color: 1
Size: 315112 Color: 1
Size: 298879 Color: 0

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 386240 Color: 1
Size: 346506 Color: 1
Size: 267255 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 386258 Color: 1
Size: 343702 Color: 1
Size: 270041 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 386259 Color: 1
Size: 337289 Color: 1
Size: 276453 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 386370 Color: 1
Size: 313621 Color: 1
Size: 300010 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 386397 Color: 1
Size: 342609 Color: 1
Size: 270995 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 386441 Color: 1
Size: 318579 Color: 1
Size: 294981 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 386454 Color: 1
Size: 326664 Color: 1
Size: 286883 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 386476 Color: 1
Size: 320877 Color: 1
Size: 292648 Color: 0

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 386487 Color: 1
Size: 346749 Color: 1
Size: 266765 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 386496 Color: 1
Size: 310709 Color: 1
Size: 302796 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 386506 Color: 1
Size: 317775 Color: 1
Size: 295720 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 386721 Color: 1
Size: 317023 Color: 1
Size: 296257 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 387124 Color: 1
Size: 307445 Color: 1
Size: 305432 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 387138 Color: 1
Size: 337625 Color: 1
Size: 275238 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 387151 Color: 1
Size: 342280 Color: 1
Size: 270570 Color: 0

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 387223 Color: 1
Size: 311446 Color: 1
Size: 301332 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 387244 Color: 1
Size: 347337 Color: 1
Size: 265420 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 387310 Color: 1
Size: 338410 Color: 1
Size: 274281 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 387443 Color: 1
Size: 339394 Color: 1
Size: 273164 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 387464 Color: 1
Size: 348477 Color: 1
Size: 264060 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 387466 Color: 1
Size: 346676 Color: 1
Size: 265859 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 387507 Color: 1
Size: 317882 Color: 0
Size: 294612 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 387511 Color: 1
Size: 306734 Color: 1
Size: 305756 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 387550 Color: 1
Size: 339636 Color: 1
Size: 272815 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 387551 Color: 1
Size: 343348 Color: 1
Size: 269102 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 387556 Color: 1
Size: 339494 Color: 1
Size: 272951 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 387573 Color: 1
Size: 324386 Color: 1
Size: 288042 Color: 0

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 387641 Color: 1
Size: 335926 Color: 1
Size: 276434 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 387765 Color: 1
Size: 333354 Color: 1
Size: 278882 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 387767 Color: 1
Size: 330467 Color: 1
Size: 281767 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 387776 Color: 1
Size: 339456 Color: 1
Size: 272769 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 387881 Color: 1
Size: 322273 Color: 1
Size: 289847 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 387882 Color: 1
Size: 334033 Color: 1
Size: 278086 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 387994 Color: 1
Size: 321729 Color: 1
Size: 290278 Color: 0

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 388093 Color: 1
Size: 316173 Color: 1
Size: 295735 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 388142 Color: 1
Size: 343376 Color: 1
Size: 268483 Color: 0

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 388157 Color: 1
Size: 319487 Color: 1
Size: 292357 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 388225 Color: 1
Size: 342163 Color: 1
Size: 269613 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 388332 Color: 1
Size: 343855 Color: 1
Size: 267814 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 388360 Color: 1
Size: 309908 Color: 1
Size: 301733 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 388371 Color: 1
Size: 319218 Color: 1
Size: 292412 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 388461 Color: 1
Size: 334415 Color: 1
Size: 277125 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 388470 Color: 1
Size: 341667 Color: 1
Size: 269864 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 388490 Color: 1
Size: 347231 Color: 1
Size: 264280 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 388663 Color: 1
Size: 328568 Color: 1
Size: 282770 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 388675 Color: 1
Size: 309711 Color: 1
Size: 301615 Color: 0

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 388682 Color: 1
Size: 339570 Color: 1
Size: 271749 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 388809 Color: 1
Size: 315372 Color: 1
Size: 295820 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 388838 Color: 1
Size: 334177 Color: 1
Size: 276986 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 388887 Color: 1
Size: 316681 Color: 1
Size: 294433 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 388895 Color: 1
Size: 333808 Color: 1
Size: 277298 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 388922 Color: 1
Size: 332970 Color: 1
Size: 278109 Color: 0

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 389024 Color: 1
Size: 342940 Color: 1
Size: 268037 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 389029 Color: 1
Size: 332679 Color: 1
Size: 278293 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 389120 Color: 1
Size: 342556 Color: 1
Size: 268325 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 389127 Color: 1
Size: 341114 Color: 1
Size: 269760 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 389146 Color: 1
Size: 334655 Color: 1
Size: 276200 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 389192 Color: 1
Size: 326320 Color: 0
Size: 284489 Color: 1

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 389211 Color: 1
Size: 332267 Color: 1
Size: 278523 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 389259 Color: 1
Size: 352128 Color: 1
Size: 258614 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 389289 Color: 1
Size: 323752 Color: 1
Size: 286960 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 389338 Color: 1
Size: 343976 Color: 1
Size: 266687 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 389391 Color: 1
Size: 309399 Color: 1
Size: 301211 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 389495 Color: 1
Size: 310613 Color: 0
Size: 299893 Color: 1

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 389508 Color: 1
Size: 327287 Color: 1
Size: 283206 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 389554 Color: 1
Size: 345353 Color: 1
Size: 265094 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 389602 Color: 1
Size: 319167 Color: 1
Size: 291232 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 389656 Color: 1
Size: 311201 Color: 1
Size: 299144 Color: 0

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 389678 Color: 1
Size: 332569 Color: 1
Size: 277754 Color: 0

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 389744 Color: 1
Size: 343566 Color: 1
Size: 266691 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 389776 Color: 1
Size: 345958 Color: 1
Size: 264267 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 389836 Color: 1
Size: 305592 Color: 0
Size: 304573 Color: 1

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 389852 Color: 1
Size: 314840 Color: 1
Size: 295309 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 389866 Color: 1
Size: 328846 Color: 1
Size: 281289 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 389902 Color: 1
Size: 337234 Color: 1
Size: 272865 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 389977 Color: 1
Size: 310495 Color: 1
Size: 299529 Color: 0

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 390047 Color: 1
Size: 313874 Color: 1
Size: 296080 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 390065 Color: 1
Size: 312136 Color: 1
Size: 297800 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 390116 Color: 1
Size: 336075 Color: 1
Size: 273810 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 390119 Color: 1
Size: 321967 Color: 1
Size: 287915 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 390122 Color: 1
Size: 319594 Color: 1
Size: 290285 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 390158 Color: 1
Size: 336666 Color: 1
Size: 273177 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 390168 Color: 1
Size: 307425 Color: 1
Size: 302408 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 390168 Color: 1
Size: 311421 Color: 1
Size: 298412 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 390191 Color: 1
Size: 327142 Color: 1
Size: 282668 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 390252 Color: 1
Size: 333233 Color: 1
Size: 276516 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 390259 Color: 1
Size: 339166 Color: 1
Size: 270576 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 390349 Color: 1
Size: 319321 Color: 0
Size: 290331 Color: 1

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 390352 Color: 1
Size: 321072 Color: 0
Size: 288577 Color: 1

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 390429 Color: 1
Size: 320952 Color: 1
Size: 288620 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 390436 Color: 1
Size: 334400 Color: 1
Size: 275165 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 390487 Color: 1
Size: 322837 Color: 1
Size: 286677 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 390549 Color: 1
Size: 327168 Color: 1
Size: 282284 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 390599 Color: 1
Size: 314785 Color: 1
Size: 294617 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 390640 Color: 1
Size: 344360 Color: 1
Size: 265001 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 390661 Color: 1
Size: 310966 Color: 1
Size: 298374 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 390674 Color: 1
Size: 312546 Color: 1
Size: 296781 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 390687 Color: 1
Size: 345670 Color: 1
Size: 263644 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 390697 Color: 1
Size: 330774 Color: 1
Size: 278530 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 390748 Color: 1
Size: 336865 Color: 1
Size: 272388 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 390804 Color: 1
Size: 327127 Color: 1
Size: 282070 Color: 0

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 390967 Color: 1
Size: 350162 Color: 1
Size: 258872 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 391053 Color: 1
Size: 340142 Color: 1
Size: 268806 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 391070 Color: 1
Size: 335968 Color: 1
Size: 272963 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 391147 Color: 1
Size: 332150 Color: 1
Size: 276704 Color: 0

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 391192 Color: 1
Size: 307205 Color: 0
Size: 301604 Color: 1

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 391346 Color: 1
Size: 349220 Color: 1
Size: 259435 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 391354 Color: 1
Size: 346767 Color: 1
Size: 261880 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 391375 Color: 1
Size: 329591 Color: 1
Size: 279035 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 391449 Color: 1
Size: 326853 Color: 1
Size: 281699 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 391613 Color: 1
Size: 348158 Color: 1
Size: 260230 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 391770 Color: 1
Size: 325854 Color: 1
Size: 282377 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 391873 Color: 1
Size: 324165 Color: 1
Size: 283963 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 391964 Color: 1
Size: 337017 Color: 1
Size: 271020 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 392125 Color: 1
Size: 312505 Color: 1
Size: 295371 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 392157 Color: 1
Size: 354840 Color: 1
Size: 253004 Color: 0

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 392187 Color: 1
Size: 322762 Color: 1
Size: 285052 Color: 0

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 392200 Color: 1
Size: 331112 Color: 1
Size: 276689 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 392312 Color: 1
Size: 332000 Color: 1
Size: 275689 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 392359 Color: 1
Size: 306725 Color: 1
Size: 300917 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 392395 Color: 1
Size: 340096 Color: 1
Size: 267510 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 392423 Color: 1
Size: 330226 Color: 1
Size: 277352 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 392465 Color: 1
Size: 317079 Color: 1
Size: 290457 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 392491 Color: 1
Size: 346462 Color: 1
Size: 261048 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 392627 Color: 1
Size: 344022 Color: 1
Size: 263352 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 392641 Color: 1
Size: 340796 Color: 1
Size: 266564 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 392642 Color: 1
Size: 304913 Color: 1
Size: 302446 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 392652 Color: 1
Size: 335097 Color: 1
Size: 272252 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 392653 Color: 1
Size: 339412 Color: 1
Size: 267936 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 392673 Color: 1
Size: 328009 Color: 1
Size: 279319 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 392793 Color: 1
Size: 337312 Color: 1
Size: 269896 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 392800 Color: 1
Size: 329863 Color: 1
Size: 277338 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 392835 Color: 1
Size: 304344 Color: 0
Size: 302822 Color: 1

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 392947 Color: 1
Size: 331323 Color: 1
Size: 275731 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 393162 Color: 1
Size: 335430 Color: 1
Size: 271409 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 393286 Color: 1
Size: 307950 Color: 1
Size: 298765 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 393300 Color: 1
Size: 304241 Color: 0
Size: 302460 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 393377 Color: 1
Size: 344632 Color: 1
Size: 261992 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 393409 Color: 1
Size: 331211 Color: 1
Size: 275381 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 393542 Color: 1
Size: 325252 Color: 1
Size: 281207 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 393563 Color: 1
Size: 325378 Color: 1
Size: 281060 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 393576 Color: 1
Size: 317066 Color: 1
Size: 289359 Color: 0

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 393729 Color: 1
Size: 331591 Color: 1
Size: 274681 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 393762 Color: 1
Size: 317415 Color: 1
Size: 288824 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 393802 Color: 1
Size: 315361 Color: 1
Size: 290838 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 393849 Color: 1
Size: 314014 Color: 1
Size: 292138 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 393850 Color: 1
Size: 309071 Color: 1
Size: 297080 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 393926 Color: 1
Size: 353511 Color: 1
Size: 252564 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 393978 Color: 1
Size: 340692 Color: 1
Size: 265331 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 394004 Color: 1
Size: 326082 Color: 1
Size: 279915 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 394082 Color: 1
Size: 333025 Color: 1
Size: 272894 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 394098 Color: 1
Size: 347393 Color: 1
Size: 258510 Color: 0

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 394104 Color: 1
Size: 328399 Color: 1
Size: 277498 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 394184 Color: 1
Size: 332866 Color: 1
Size: 272951 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 394202 Color: 1
Size: 329844 Color: 1
Size: 275955 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 394251 Color: 1
Size: 344216 Color: 1
Size: 261534 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 394298 Color: 1
Size: 341329 Color: 1
Size: 264374 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 394384 Color: 1
Size: 314684 Color: 1
Size: 290933 Color: 0

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 394441 Color: 1
Size: 315129 Color: 1
Size: 290431 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 394498 Color: 1
Size: 336853 Color: 1
Size: 268650 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 394567 Color: 1
Size: 348684 Color: 1
Size: 256750 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 394644 Color: 1
Size: 334107 Color: 1
Size: 271250 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 394676 Color: 1
Size: 332650 Color: 1
Size: 272675 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 394749 Color: 1
Size: 304486 Color: 1
Size: 300766 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 372491 Color: 1
Size: 345219 Color: 1
Size: 282291 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 394920 Color: 1
Size: 324818 Color: 1
Size: 280263 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 394924 Color: 1
Size: 333279 Color: 1
Size: 271798 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 394950 Color: 1
Size: 332554 Color: 1
Size: 272497 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 395002 Color: 1
Size: 314354 Color: 1
Size: 290645 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 395037 Color: 1
Size: 314176 Color: 1
Size: 290788 Color: 0

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 395088 Color: 1
Size: 334377 Color: 1
Size: 270536 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 395160 Color: 1
Size: 324652 Color: 1
Size: 280189 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 395166 Color: 1
Size: 322895 Color: 1
Size: 281940 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 395355 Color: 1
Size: 302965 Color: 1
Size: 301681 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 395408 Color: 1
Size: 335181 Color: 1
Size: 269412 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 395464 Color: 1
Size: 343014 Color: 1
Size: 261523 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 395514 Color: 1
Size: 317484 Color: 1
Size: 287003 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 395523 Color: 1
Size: 323855 Color: 1
Size: 280623 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 395563 Color: 1
Size: 345752 Color: 1
Size: 258686 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 395605 Color: 1
Size: 345411 Color: 1
Size: 258985 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 395651 Color: 1
Size: 318977 Color: 1
Size: 285373 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 395719 Color: 1
Size: 348783 Color: 1
Size: 255499 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 395738 Color: 1
Size: 352830 Color: 1
Size: 251433 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 395832 Color: 1
Size: 338545 Color: 1
Size: 265624 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 396025 Color: 1
Size: 341222 Color: 1
Size: 262754 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 396034 Color: 1
Size: 330269 Color: 1
Size: 273698 Color: 0

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 396120 Color: 1
Size: 332025 Color: 1
Size: 271856 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 396147 Color: 1
Size: 333587 Color: 1
Size: 270267 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 396170 Color: 1
Size: 343615 Color: 1
Size: 260216 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 396279 Color: 1
Size: 327057 Color: 1
Size: 276665 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 396356 Color: 1
Size: 322811 Color: 1
Size: 280834 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 396388 Color: 1
Size: 314055 Color: 1
Size: 289558 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 396445 Color: 1
Size: 348675 Color: 1
Size: 254881 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 396502 Color: 1
Size: 334330 Color: 1
Size: 269169 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 396532 Color: 1
Size: 332764 Color: 1
Size: 270705 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 396545 Color: 1
Size: 320165 Color: 1
Size: 283291 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 1
Size: 311978 Color: 0
Size: 291259 Color: 1

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 396771 Color: 1
Size: 336772 Color: 1
Size: 266458 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 396981 Color: 1
Size: 327538 Color: 1
Size: 275482 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 397123 Color: 1
Size: 307664 Color: 0
Size: 295214 Color: 1

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 397146 Color: 1
Size: 327711 Color: 1
Size: 275144 Color: 0

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 397173 Color: 1
Size: 312991 Color: 0
Size: 289837 Color: 1

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 397242 Color: 1
Size: 325482 Color: 1
Size: 277277 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 397281 Color: 1
Size: 343749 Color: 1
Size: 258971 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 380415 Color: 1
Size: 311575 Color: 0
Size: 308011 Color: 1

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 397464 Color: 1
Size: 323465 Color: 1
Size: 279072 Color: 0

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 397468 Color: 1
Size: 326000 Color: 1
Size: 276533 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 397566 Color: 1
Size: 328836 Color: 1
Size: 273599 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 397597 Color: 1
Size: 333955 Color: 1
Size: 268449 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 397676 Color: 1
Size: 349525 Color: 1
Size: 252800 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 397797 Color: 1
Size: 344752 Color: 1
Size: 257452 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 397797 Color: 1
Size: 337610 Color: 1
Size: 264594 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 397978 Color: 1
Size: 330726 Color: 1
Size: 271297 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 398022 Color: 1
Size: 333651 Color: 1
Size: 268328 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 398026 Color: 1
Size: 318529 Color: 1
Size: 283446 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 398056 Color: 1
Size: 324879 Color: 1
Size: 277066 Color: 0

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 398116 Color: 1
Size: 336971 Color: 1
Size: 264914 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 398131 Color: 1
Size: 334200 Color: 1
Size: 267670 Color: 0

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 398205 Color: 1
Size: 335661 Color: 1
Size: 266135 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 398314 Color: 1
Size: 331796 Color: 1
Size: 269891 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 398400 Color: 1
Size: 331232 Color: 1
Size: 270369 Color: 0

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 398426 Color: 1
Size: 331843 Color: 1
Size: 269732 Color: 0

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 398555 Color: 1
Size: 306109 Color: 1
Size: 295337 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 398823 Color: 1
Size: 321282 Color: 1
Size: 279896 Color: 0

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 398852 Color: 1
Size: 339261 Color: 1
Size: 261888 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 398947 Color: 1
Size: 325592 Color: 1
Size: 275462 Color: 0

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 398962 Color: 1
Size: 328586 Color: 1
Size: 272453 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 398967 Color: 1
Size: 317127 Color: 1
Size: 283907 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 399005 Color: 1
Size: 326581 Color: 1
Size: 274415 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 399017 Color: 1
Size: 338866 Color: 1
Size: 262118 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 399020 Color: 1
Size: 304298 Color: 1
Size: 296683 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 399093 Color: 1
Size: 311155 Color: 1
Size: 289753 Color: 0

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 399099 Color: 1
Size: 340111 Color: 1
Size: 260791 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 399106 Color: 1
Size: 340342 Color: 1
Size: 260553 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 399110 Color: 1
Size: 338264 Color: 1
Size: 262627 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 399148 Color: 1
Size: 332078 Color: 1
Size: 268775 Color: 0

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 399192 Color: 1
Size: 306491 Color: 1
Size: 294318 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 399242 Color: 1
Size: 337727 Color: 1
Size: 263032 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 399360 Color: 1
Size: 327319 Color: 1
Size: 273322 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 399370 Color: 1
Size: 333641 Color: 1
Size: 266990 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 399511 Color: 1
Size: 321884 Color: 1
Size: 278606 Color: 0

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 399531 Color: 1
Size: 310773 Color: 1
Size: 289697 Color: 0

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 399557 Color: 1
Size: 323684 Color: 1
Size: 276760 Color: 0

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 399604 Color: 1
Size: 346826 Color: 1
Size: 253571 Color: 0

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 399605 Color: 1
Size: 315184 Color: 1
Size: 285212 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 399674 Color: 1
Size: 326312 Color: 1
Size: 274015 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 399782 Color: 1
Size: 324895 Color: 1
Size: 275324 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 399845 Color: 1
Size: 314702 Color: 0
Size: 285454 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 399876 Color: 1
Size: 324379 Color: 1
Size: 275746 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 399915 Color: 1
Size: 312345 Color: 1
Size: 287741 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 408638 Color: 1
Size: 337991 Color: 1
Size: 253372 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 400005 Color: 1
Size: 329074 Color: 1
Size: 270922 Color: 0

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 400120 Color: 1
Size: 334890 Color: 1
Size: 264991 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 400168 Color: 1
Size: 322768 Color: 1
Size: 277065 Color: 0

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 400208 Color: 1
Size: 318375 Color: 1
Size: 281418 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 400214 Color: 1
Size: 321423 Color: 1
Size: 278364 Color: 0

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 400236 Color: 1
Size: 302045 Color: 1
Size: 297720 Color: 0

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 400266 Color: 1
Size: 327094 Color: 1
Size: 272641 Color: 0

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 400273 Color: 1
Size: 331031 Color: 1
Size: 268697 Color: 0

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 400288 Color: 1
Size: 315315 Color: 1
Size: 284398 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 400289 Color: 1
Size: 342147 Color: 1
Size: 257565 Color: 0

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 400360 Color: 1
Size: 333630 Color: 1
Size: 266011 Color: 0

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 400373 Color: 1
Size: 344705 Color: 1
Size: 254923 Color: 0

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 400702 Color: 1
Size: 333218 Color: 1
Size: 266081 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 390306 Color: 1
Size: 359506 Color: 1
Size: 250189 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 400919 Color: 1
Size: 341562 Color: 1
Size: 257520 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 400924 Color: 1
Size: 305959 Color: 1
Size: 293118 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 358144 Color: 1
Size: 329020 Color: 1
Size: 312837 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 401021 Color: 1
Size: 311776 Color: 1
Size: 287204 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 401109 Color: 1
Size: 333144 Color: 1
Size: 265748 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 401126 Color: 1
Size: 333897 Color: 1
Size: 264978 Color: 0

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 401202 Color: 1
Size: 327579 Color: 1
Size: 271220 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 401284 Color: 1
Size: 302131 Color: 1
Size: 296586 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 401308 Color: 1
Size: 333205 Color: 1
Size: 265488 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 401330 Color: 1
Size: 322813 Color: 1
Size: 275858 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 401438 Color: 1
Size: 320918 Color: 0
Size: 277645 Color: 1

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 401456 Color: 1
Size: 331749 Color: 1
Size: 266796 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 401471 Color: 1
Size: 332455 Color: 1
Size: 266075 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 401492 Color: 1
Size: 331676 Color: 1
Size: 266833 Color: 0

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 401519 Color: 1
Size: 327270 Color: 1
Size: 271212 Color: 0

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 401579 Color: 1
Size: 317558 Color: 1
Size: 280864 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 401616 Color: 1
Size: 332859 Color: 1
Size: 265526 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 401617 Color: 1
Size: 312470 Color: 1
Size: 285914 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 401627 Color: 1
Size: 324096 Color: 1
Size: 274278 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 401661 Color: 1
Size: 343590 Color: 1
Size: 254750 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 401785 Color: 1
Size: 316746 Color: 1
Size: 281470 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 401826 Color: 1
Size: 333584 Color: 1
Size: 264591 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 401945 Color: 1
Size: 334569 Color: 1
Size: 263487 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 402014 Color: 1
Size: 312031 Color: 1
Size: 285956 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 402055 Color: 1
Size: 322738 Color: 1
Size: 275208 Color: 0

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 402085 Color: 1
Size: 316613 Color: 1
Size: 281303 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 402157 Color: 1
Size: 327806 Color: 1
Size: 270038 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 402242 Color: 1
Size: 321519 Color: 1
Size: 276240 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 402304 Color: 1
Size: 305756 Color: 1
Size: 291941 Color: 0

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 402349 Color: 1
Size: 335121 Color: 1
Size: 262531 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 402555 Color: 1
Size: 329465 Color: 1
Size: 267981 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 402640 Color: 1
Size: 327669 Color: 1
Size: 269692 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 402650 Color: 1
Size: 331791 Color: 1
Size: 265560 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 402658 Color: 1
Size: 300624 Color: 0
Size: 296719 Color: 1

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 402756 Color: 1
Size: 305065 Color: 1
Size: 292180 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 402774 Color: 1
Size: 345000 Color: 1
Size: 252227 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 402874 Color: 1
Size: 324092 Color: 1
Size: 273035 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 402904 Color: 1
Size: 333147 Color: 1
Size: 263950 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 402925 Color: 1
Size: 317270 Color: 1
Size: 279806 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 402960 Color: 1
Size: 299715 Color: 0
Size: 297326 Color: 1

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 402972 Color: 1
Size: 333016 Color: 1
Size: 264013 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 403043 Color: 1
Size: 335548 Color: 1
Size: 261410 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 403043 Color: 1
Size: 328775 Color: 1
Size: 268183 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 375745 Color: 1
Size: 322481 Color: 0
Size: 301775 Color: 1

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 403077 Color: 1
Size: 300142 Color: 0
Size: 296782 Color: 1

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 403086 Color: 1
Size: 324432 Color: 1
Size: 272483 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 403122 Color: 1
Size: 322829 Color: 1
Size: 274050 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 403144 Color: 1
Size: 325545 Color: 1
Size: 271312 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 403179 Color: 1
Size: 309963 Color: 1
Size: 286859 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 403208 Color: 1
Size: 334136 Color: 1
Size: 262657 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 403271 Color: 1
Size: 312105 Color: 1
Size: 284625 Color: 0

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 403287 Color: 1
Size: 306079 Color: 1
Size: 290635 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 403324 Color: 1
Size: 322462 Color: 1
Size: 274215 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 403326 Color: 1
Size: 314567 Color: 1
Size: 282108 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 403348 Color: 1
Size: 302819 Color: 1
Size: 293834 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 403352 Color: 1
Size: 324680 Color: 0
Size: 271969 Color: 1

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 403392 Color: 1
Size: 331024 Color: 1
Size: 265585 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 403414 Color: 1
Size: 326582 Color: 1
Size: 270005 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 403424 Color: 1
Size: 320205 Color: 1
Size: 276372 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 403453 Color: 1
Size: 309887 Color: 1
Size: 286661 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 403591 Color: 1
Size: 334300 Color: 1
Size: 262110 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 403597 Color: 1
Size: 331527 Color: 1
Size: 264877 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 403838 Color: 1
Size: 322189 Color: 1
Size: 273974 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 403962 Color: 1
Size: 337144 Color: 1
Size: 258895 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 403977 Color: 1
Size: 328829 Color: 1
Size: 267195 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 404054 Color: 1
Size: 318009 Color: 1
Size: 277938 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 404158 Color: 1
Size: 307811 Color: 0
Size: 288032 Color: 1

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 404309 Color: 1
Size: 339399 Color: 1
Size: 256293 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 404353 Color: 1
Size: 320542 Color: 1
Size: 275106 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 404372 Color: 1
Size: 311339 Color: 1
Size: 284290 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 404401 Color: 1
Size: 327312 Color: 1
Size: 268288 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 404403 Color: 1
Size: 329467 Color: 1
Size: 266131 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 404458 Color: 1
Size: 339226 Color: 1
Size: 256317 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 404529 Color: 1
Size: 343628 Color: 1
Size: 251844 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 404539 Color: 1
Size: 309887 Color: 1
Size: 285575 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 404703 Color: 1
Size: 335279 Color: 1
Size: 260019 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 404800 Color: 1
Size: 320747 Color: 1
Size: 274454 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 404837 Color: 1
Size: 319409 Color: 1
Size: 275755 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 404919 Color: 1
Size: 319076 Color: 1
Size: 276006 Color: 0

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 404960 Color: 1
Size: 334142 Color: 1
Size: 260899 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 404964 Color: 1
Size: 326492 Color: 1
Size: 268545 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 404971 Color: 1
Size: 340764 Color: 1
Size: 254266 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 404981 Color: 1
Size: 307373 Color: 1
Size: 287647 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 404987 Color: 1
Size: 317125 Color: 1
Size: 277889 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 404987 Color: 1
Size: 325333 Color: 1
Size: 269681 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 405010 Color: 1
Size: 305323 Color: 0
Size: 289668 Color: 1

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 405086 Color: 1
Size: 339920 Color: 1
Size: 254995 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 405105 Color: 1
Size: 329073 Color: 1
Size: 265823 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 405136 Color: 1
Size: 333166 Color: 1
Size: 261699 Color: 0

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 405148 Color: 1
Size: 314970 Color: 1
Size: 279883 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 405184 Color: 1
Size: 324685 Color: 1
Size: 270132 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 405315 Color: 1
Size: 299743 Color: 0
Size: 294943 Color: 1

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 405397 Color: 1
Size: 329650 Color: 1
Size: 264954 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 405397 Color: 1
Size: 325616 Color: 1
Size: 268988 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 405517 Color: 1
Size: 305992 Color: 1
Size: 288492 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 405580 Color: 1
Size: 300901 Color: 1
Size: 293520 Color: 0

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 405582 Color: 1
Size: 319657 Color: 1
Size: 274762 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 405596 Color: 1
Size: 312134 Color: 0
Size: 282271 Color: 1

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 405622 Color: 1
Size: 324066 Color: 1
Size: 270313 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 405638 Color: 1
Size: 300370 Color: 1
Size: 293993 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 405714 Color: 1
Size: 324615 Color: 1
Size: 269672 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 405840 Color: 1
Size: 336155 Color: 1
Size: 258006 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 405895 Color: 1
Size: 321744 Color: 1
Size: 272362 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 405919 Color: 1
Size: 334384 Color: 1
Size: 259698 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 406029 Color: 1
Size: 308425 Color: 1
Size: 285547 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 406077 Color: 1
Size: 330944 Color: 1
Size: 262980 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 406150 Color: 1
Size: 307339 Color: 1
Size: 286512 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 406189 Color: 1
Size: 322315 Color: 0
Size: 271497 Color: 1

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 406274 Color: 1
Size: 311821 Color: 1
Size: 281906 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 406307 Color: 1
Size: 321853 Color: 1
Size: 271841 Color: 0

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 406330 Color: 1
Size: 306623 Color: 0
Size: 287048 Color: 1

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 406332 Color: 1
Size: 306743 Color: 1
Size: 286926 Color: 0

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 406475 Color: 1
Size: 321789 Color: 1
Size: 271737 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 406589 Color: 1
Size: 332454 Color: 1
Size: 260958 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 406660 Color: 1
Size: 331114 Color: 1
Size: 262227 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 406664 Color: 1
Size: 312047 Color: 1
Size: 281290 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 406738 Color: 1
Size: 329406 Color: 1
Size: 263857 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 406748 Color: 1
Size: 322880 Color: 1
Size: 270373 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 406803 Color: 1
Size: 316051 Color: 0
Size: 277147 Color: 1

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 407026 Color: 1
Size: 309072 Color: 1
Size: 283903 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 407041 Color: 1
Size: 334587 Color: 1
Size: 258373 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 407162 Color: 1
Size: 312401 Color: 1
Size: 280438 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 407306 Color: 1
Size: 311032 Color: 1
Size: 281663 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 407345 Color: 1
Size: 314778 Color: 1
Size: 277878 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 407412 Color: 1
Size: 320425 Color: 1
Size: 272164 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 407465 Color: 1
Size: 324810 Color: 1
Size: 267726 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 407499 Color: 1
Size: 306710 Color: 1
Size: 285792 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 407782 Color: 1
Size: 326504 Color: 1
Size: 265715 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 407823 Color: 1
Size: 322169 Color: 1
Size: 270009 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 408145 Color: 1
Size: 306446 Color: 1
Size: 285410 Color: 0

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 408146 Color: 1
Size: 340071 Color: 1
Size: 251784 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 408302 Color: 1
Size: 306066 Color: 1
Size: 285633 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 408311 Color: 1
Size: 311080 Color: 0
Size: 280610 Color: 1

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 408517 Color: 1
Size: 331943 Color: 1
Size: 259541 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 408549 Color: 1
Size: 335811 Color: 1
Size: 255641 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 408570 Color: 1
Size: 340249 Color: 1
Size: 251182 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 408703 Color: 1
Size: 331525 Color: 1
Size: 259773 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 408804 Color: 1
Size: 309277 Color: 1
Size: 281920 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 408899 Color: 1
Size: 316983 Color: 1
Size: 274119 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 408972 Color: 1
Size: 316647 Color: 1
Size: 274382 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 408975 Color: 1
Size: 332136 Color: 1
Size: 258890 Color: 0

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 408984 Color: 1
Size: 336249 Color: 1
Size: 254768 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 409069 Color: 1
Size: 298536 Color: 0
Size: 292396 Color: 1

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 409128 Color: 1
Size: 328086 Color: 1
Size: 262787 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 409155 Color: 1
Size: 327456 Color: 1
Size: 263390 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 409171 Color: 1
Size: 336804 Color: 1
Size: 254026 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 409204 Color: 1
Size: 338105 Color: 1
Size: 252692 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 409300 Color: 1
Size: 319175 Color: 1
Size: 271526 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 409449 Color: 1
Size: 316176 Color: 1
Size: 274376 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 409491 Color: 1
Size: 306014 Color: 1
Size: 284496 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 409548 Color: 1
Size: 308485 Color: 1
Size: 281968 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 409569 Color: 1
Size: 318907 Color: 1
Size: 271525 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 409587 Color: 1
Size: 309929 Color: 1
Size: 280485 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 409619 Color: 1
Size: 332031 Color: 1
Size: 258351 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 409632 Color: 1
Size: 321500 Color: 1
Size: 268869 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 409691 Color: 1
Size: 307054 Color: 1
Size: 283256 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 409724 Color: 1
Size: 306373 Color: 1
Size: 283904 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 409829 Color: 1
Size: 302584 Color: 1
Size: 287588 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 410244 Color: 1
Size: 327050 Color: 1
Size: 262707 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 410323 Color: 1
Size: 311775 Color: 1
Size: 277903 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 410375 Color: 1
Size: 334571 Color: 1
Size: 255055 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 410398 Color: 1
Size: 315823 Color: 1
Size: 273780 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 410439 Color: 1
Size: 330119 Color: 1
Size: 259443 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 410505 Color: 1
Size: 316107 Color: 1
Size: 273389 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 410519 Color: 1
Size: 304410 Color: 1
Size: 285072 Color: 0

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 410607 Color: 1
Size: 318625 Color: 1
Size: 270769 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 410645 Color: 1
Size: 310211 Color: 1
Size: 279145 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 410721 Color: 1
Size: 317256 Color: 1
Size: 272024 Color: 0

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 410942 Color: 1
Size: 327285 Color: 1
Size: 261774 Color: 0

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 410954 Color: 1
Size: 335691 Color: 1
Size: 253356 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 411058 Color: 1
Size: 296439 Color: 1
Size: 292504 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 411092 Color: 1
Size: 329133 Color: 1
Size: 259776 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 411204 Color: 1
Size: 318571 Color: 1
Size: 270226 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 411240 Color: 1
Size: 313386 Color: 1
Size: 275375 Color: 0

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 411263 Color: 1
Size: 301251 Color: 1
Size: 287487 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 411266 Color: 1
Size: 323964 Color: 1
Size: 264771 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 411267 Color: 1
Size: 327574 Color: 1
Size: 261160 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 411302 Color: 1
Size: 324102 Color: 1
Size: 264597 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 411332 Color: 1
Size: 309743 Color: 1
Size: 278926 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 411343 Color: 1
Size: 333626 Color: 1
Size: 255032 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 411386 Color: 1
Size: 308457 Color: 1
Size: 280158 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 411412 Color: 1
Size: 326590 Color: 1
Size: 261999 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 411434 Color: 1
Size: 319273 Color: 1
Size: 269294 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 411550 Color: 1
Size: 309740 Color: 1
Size: 278711 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 411557 Color: 1
Size: 334944 Color: 1
Size: 253500 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 411640 Color: 1
Size: 334601 Color: 1
Size: 253760 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 411678 Color: 1
Size: 311195 Color: 1
Size: 277128 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 411692 Color: 1
Size: 330555 Color: 1
Size: 257754 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 411728 Color: 1
Size: 318767 Color: 1
Size: 269506 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 411767 Color: 1
Size: 294854 Color: 0
Size: 293380 Color: 1

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 411838 Color: 1
Size: 316544 Color: 1
Size: 271619 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 411935 Color: 1
Size: 328197 Color: 1
Size: 259869 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 411960 Color: 1
Size: 299185 Color: 1
Size: 288856 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 412025 Color: 1
Size: 307435 Color: 1
Size: 280541 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 412053 Color: 1
Size: 328609 Color: 1
Size: 259339 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 412070 Color: 1
Size: 325699 Color: 1
Size: 262232 Color: 0

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 412073 Color: 1
Size: 314790 Color: 1
Size: 273138 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 412076 Color: 1
Size: 315786 Color: 0
Size: 272139 Color: 1

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 412085 Color: 1
Size: 334505 Color: 1
Size: 253411 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 412092 Color: 1
Size: 307103 Color: 1
Size: 280806 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 412165 Color: 1
Size: 311949 Color: 1
Size: 275887 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 412165 Color: 1
Size: 303218 Color: 0
Size: 284618 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 412203 Color: 1
Size: 312939 Color: 1
Size: 274859 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 412236 Color: 1
Size: 316460 Color: 1
Size: 271305 Color: 0

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 412248 Color: 1
Size: 299731 Color: 1
Size: 288022 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 412254 Color: 1
Size: 334619 Color: 1
Size: 253128 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 412267 Color: 1
Size: 318300 Color: 1
Size: 269434 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 412363 Color: 1
Size: 320593 Color: 1
Size: 267045 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 412425 Color: 1
Size: 300592 Color: 1
Size: 286984 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 412452 Color: 1
Size: 300703 Color: 1
Size: 286846 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 412505 Color: 1
Size: 326452 Color: 1
Size: 261044 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 412508 Color: 1
Size: 303408 Color: 1
Size: 284085 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 412567 Color: 1
Size: 298034 Color: 1
Size: 289400 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 412638 Color: 1
Size: 330945 Color: 1
Size: 256418 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 412832 Color: 1
Size: 301640 Color: 1
Size: 285529 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 412858 Color: 1
Size: 327995 Color: 1
Size: 259148 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 412880 Color: 1
Size: 303444 Color: 1
Size: 283677 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 412882 Color: 1
Size: 303927 Color: 1
Size: 283192 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 412998 Color: 1
Size: 322564 Color: 1
Size: 264439 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 413146 Color: 1
Size: 324227 Color: 1
Size: 262628 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 413213 Color: 1
Size: 309873 Color: 1
Size: 276915 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 413254 Color: 1
Size: 331298 Color: 1
Size: 255449 Color: 0

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 413287 Color: 1
Size: 328559 Color: 1
Size: 258155 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 413309 Color: 1
Size: 314794 Color: 1
Size: 271898 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 413314 Color: 1
Size: 315584 Color: 1
Size: 271103 Color: 0

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 413346 Color: 1
Size: 316687 Color: 1
Size: 269968 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 413353 Color: 1
Size: 326640 Color: 1
Size: 260008 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 413423 Color: 1
Size: 311379 Color: 1
Size: 275199 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 413458 Color: 1
Size: 320875 Color: 1
Size: 265668 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 413489 Color: 1
Size: 320214 Color: 1
Size: 266298 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 413514 Color: 1
Size: 322894 Color: 1
Size: 263593 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 413603 Color: 1
Size: 332657 Color: 1
Size: 253741 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 413645 Color: 1
Size: 318121 Color: 1
Size: 268235 Color: 0

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 413653 Color: 1
Size: 322601 Color: 1
Size: 263747 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 413709 Color: 1
Size: 314549 Color: 1
Size: 271743 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 413732 Color: 1
Size: 309985 Color: 1
Size: 276284 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 413838 Color: 1
Size: 314381 Color: 1
Size: 271782 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 413970 Color: 1
Size: 315114 Color: 1
Size: 270917 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 414004 Color: 1
Size: 328653 Color: 1
Size: 257344 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 414025 Color: 1
Size: 328478 Color: 1
Size: 257498 Color: 0

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 414066 Color: 1
Size: 299868 Color: 1
Size: 286067 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 414099 Color: 1
Size: 302772 Color: 1
Size: 283130 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 414232 Color: 1
Size: 307711 Color: 1
Size: 278058 Color: 0

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 414250 Color: 1
Size: 330477 Color: 1
Size: 255274 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 414271 Color: 1
Size: 310858 Color: 1
Size: 274872 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 414275 Color: 1
Size: 328772 Color: 1
Size: 256954 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 414290 Color: 1
Size: 323563 Color: 1
Size: 262148 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 414306 Color: 1
Size: 332148 Color: 1
Size: 253547 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 414321 Color: 1
Size: 309588 Color: 1
Size: 276092 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 414339 Color: 1
Size: 326791 Color: 1
Size: 258871 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 414342 Color: 1
Size: 320442 Color: 1
Size: 265217 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 414357 Color: 1
Size: 300887 Color: 1
Size: 284757 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 414367 Color: 1
Size: 320722 Color: 1
Size: 264912 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 414376 Color: 1
Size: 312021 Color: 1
Size: 273604 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 414498 Color: 1
Size: 314196 Color: 1
Size: 271307 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 414504 Color: 1
Size: 330112 Color: 1
Size: 255385 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 414504 Color: 1
Size: 315734 Color: 1
Size: 269763 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 414602 Color: 1
Size: 318215 Color: 1
Size: 267184 Color: 0

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 414789 Color: 1
Size: 321763 Color: 1
Size: 263449 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 414795 Color: 1
Size: 320610 Color: 1
Size: 264596 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 414845 Color: 1
Size: 330440 Color: 1
Size: 254716 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 414878 Color: 1
Size: 310321 Color: 1
Size: 274802 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 414989 Color: 1
Size: 309530 Color: 1
Size: 275482 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 415067 Color: 1
Size: 306542 Color: 1
Size: 278392 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 415264 Color: 1
Size: 312529 Color: 1
Size: 272208 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 415315 Color: 1
Size: 334400 Color: 1
Size: 250286 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 415460 Color: 1
Size: 316342 Color: 1
Size: 268199 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 415501 Color: 1
Size: 320311 Color: 1
Size: 264189 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 382595 Color: 1
Size: 330818 Color: 1
Size: 286588 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 415536 Color: 1
Size: 315179 Color: 1
Size: 269286 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 415609 Color: 1
Size: 305777 Color: 1
Size: 278615 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 415648 Color: 1
Size: 312217 Color: 0
Size: 272136 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 415786 Color: 1
Size: 303273 Color: 1
Size: 280942 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 415882 Color: 1
Size: 317305 Color: 1
Size: 266814 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 415925 Color: 1
Size: 322210 Color: 1
Size: 261866 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 415934 Color: 1
Size: 325030 Color: 1
Size: 259037 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 415977 Color: 1
Size: 294120 Color: 1
Size: 289904 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 416014 Color: 1
Size: 317272 Color: 1
Size: 266715 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 416073 Color: 1
Size: 293783 Color: 0
Size: 290145 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 416248 Color: 1
Size: 316889 Color: 1
Size: 266864 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 416277 Color: 1
Size: 322442 Color: 1
Size: 261282 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 416288 Color: 1
Size: 320934 Color: 1
Size: 262779 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 416359 Color: 1
Size: 322999 Color: 1
Size: 260643 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 416470 Color: 1
Size: 306173 Color: 0
Size: 277358 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 416576 Color: 1
Size: 320707 Color: 1
Size: 262718 Color: 0

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 416602 Color: 1
Size: 309555 Color: 1
Size: 273844 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 416639 Color: 1
Size: 320916 Color: 1
Size: 262446 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 416828 Color: 1
Size: 328994 Color: 1
Size: 254179 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 416887 Color: 1
Size: 313558 Color: 1
Size: 269556 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 416934 Color: 1
Size: 296364 Color: 0
Size: 286703 Color: 1

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 417012 Color: 1
Size: 298474 Color: 1
Size: 284515 Color: 0

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 417022 Color: 1
Size: 310560 Color: 1
Size: 272419 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 417067 Color: 1
Size: 310047 Color: 1
Size: 272887 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 417217 Color: 1
Size: 313892 Color: 1
Size: 268892 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 417265 Color: 1
Size: 319707 Color: 1
Size: 263029 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 417329 Color: 1
Size: 299412 Color: 1
Size: 283260 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 417365 Color: 1
Size: 323469 Color: 1
Size: 259167 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 417376 Color: 1
Size: 305560 Color: 1
Size: 277065 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 417425 Color: 1
Size: 319528 Color: 1
Size: 263048 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 417451 Color: 1
Size: 314733 Color: 1
Size: 267817 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 417531 Color: 1
Size: 316751 Color: 1
Size: 265719 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 417573 Color: 1
Size: 296028 Color: 1
Size: 286400 Color: 0

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 417618 Color: 1
Size: 312035 Color: 1
Size: 270348 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 417645 Color: 1
Size: 312389 Color: 1
Size: 269967 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 417674 Color: 1
Size: 325724 Color: 1
Size: 256603 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 417772 Color: 1
Size: 316839 Color: 1
Size: 265390 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 417793 Color: 1
Size: 304134 Color: 1
Size: 278074 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 417867 Color: 1
Size: 307897 Color: 1
Size: 274237 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 417871 Color: 1
Size: 311381 Color: 1
Size: 270749 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 417977 Color: 1
Size: 326424 Color: 1
Size: 255600 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 417992 Color: 1
Size: 325273 Color: 1
Size: 256736 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 417997 Color: 1
Size: 297626 Color: 1
Size: 284378 Color: 0

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 418001 Color: 1
Size: 310372 Color: 0
Size: 271628 Color: 1

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 418017 Color: 1
Size: 307562 Color: 1
Size: 274422 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 418085 Color: 1
Size: 307657 Color: 1
Size: 274259 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 418228 Color: 1
Size: 329046 Color: 1
Size: 252727 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 418347 Color: 1
Size: 319465 Color: 1
Size: 262189 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 418357 Color: 1
Size: 301700 Color: 1
Size: 279944 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 418470 Color: 1
Size: 299150 Color: 1
Size: 282381 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 418517 Color: 1
Size: 320287 Color: 1
Size: 261197 Color: 0

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 418572 Color: 1
Size: 313469 Color: 1
Size: 267960 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 418587 Color: 1
Size: 313229 Color: 1
Size: 268185 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 418703 Color: 1
Size: 315926 Color: 0
Size: 265372 Color: 1

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 418808 Color: 1
Size: 315039 Color: 1
Size: 266154 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 418822 Color: 1
Size: 303244 Color: 1
Size: 277935 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 418889 Color: 1
Size: 299638 Color: 0
Size: 281474 Color: 1

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 418906 Color: 1
Size: 318867 Color: 1
Size: 262228 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 418993 Color: 1
Size: 311996 Color: 1
Size: 269012 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 419037 Color: 1
Size: 298609 Color: 1
Size: 282355 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 419078 Color: 1
Size: 294262 Color: 1
Size: 286661 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 419145 Color: 1
Size: 308235 Color: 1
Size: 272621 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 419146 Color: 1
Size: 290689 Color: 1
Size: 290166 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 419210 Color: 1
Size: 312488 Color: 1
Size: 268303 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 419214 Color: 1
Size: 303576 Color: 1
Size: 277211 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 419220 Color: 1
Size: 314445 Color: 1
Size: 266336 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 419225 Color: 1
Size: 318330 Color: 1
Size: 262446 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 419245 Color: 1
Size: 320590 Color: 1
Size: 260166 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 419273 Color: 1
Size: 317111 Color: 1
Size: 263617 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 419324 Color: 1
Size: 320185 Color: 1
Size: 260492 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 419385 Color: 1
Size: 319262 Color: 0
Size: 261354 Color: 1

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 419388 Color: 1
Size: 322740 Color: 1
Size: 257873 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 419428 Color: 1
Size: 303586 Color: 1
Size: 276987 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 419495 Color: 1
Size: 309132 Color: 1
Size: 271374 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 419569 Color: 1
Size: 293115 Color: 1
Size: 287317 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 419706 Color: 1
Size: 308270 Color: 1
Size: 272025 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 419736 Color: 1
Size: 322345 Color: 1
Size: 257920 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 419762 Color: 1
Size: 319763 Color: 1
Size: 260476 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 419764 Color: 1
Size: 312965 Color: 1
Size: 267272 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 419803 Color: 1
Size: 295949 Color: 1
Size: 284249 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 419814 Color: 1
Size: 318124 Color: 1
Size: 262063 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 419869 Color: 1
Size: 322180 Color: 1
Size: 257952 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 419945 Color: 1
Size: 308097 Color: 1
Size: 271959 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 420051 Color: 1
Size: 321928 Color: 1
Size: 258022 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 420052 Color: 1
Size: 327113 Color: 1
Size: 252836 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 420072 Color: 1
Size: 320709 Color: 1
Size: 259220 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 420119 Color: 1
Size: 310741 Color: 1
Size: 269141 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 420194 Color: 1
Size: 329591 Color: 1
Size: 250216 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 420207 Color: 1
Size: 308018 Color: 1
Size: 271776 Color: 0

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 420295 Color: 1
Size: 315594 Color: 1
Size: 264112 Color: 0

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 420297 Color: 1
Size: 309682 Color: 1
Size: 270022 Color: 0

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 420345 Color: 1
Size: 305052 Color: 1
Size: 274604 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 420354 Color: 1
Size: 299843 Color: 1
Size: 279804 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 420361 Color: 1
Size: 306177 Color: 1
Size: 273463 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 420406 Color: 1
Size: 316907 Color: 1
Size: 262688 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 420409 Color: 1
Size: 299780 Color: 1
Size: 279812 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 420430 Color: 1
Size: 326976 Color: 1
Size: 252595 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 420543 Color: 1
Size: 315461 Color: 1
Size: 263997 Color: 0

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 420839 Color: 1
Size: 324115 Color: 1
Size: 255047 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 420727 Color: 1
Size: 311847 Color: 1
Size: 267427 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 420843 Color: 1
Size: 305759 Color: 1
Size: 273399 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 421094 Color: 1
Size: 313133 Color: 1
Size: 265774 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 421135 Color: 1
Size: 317222 Color: 1
Size: 261644 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 421188 Color: 1
Size: 323665 Color: 1
Size: 255148 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 421357 Color: 1
Size: 301250 Color: 1
Size: 277394 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 421357 Color: 1
Size: 323163 Color: 1
Size: 255481 Color: 0

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 421379 Color: 1
Size: 313419 Color: 1
Size: 265203 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 421379 Color: 1
Size: 311791 Color: 1
Size: 266831 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 421412 Color: 1
Size: 296514 Color: 1
Size: 282075 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 421413 Color: 1
Size: 318753 Color: 1
Size: 259835 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 421472 Color: 1
Size: 318730 Color: 1
Size: 259799 Color: 0

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 421569 Color: 1
Size: 307377 Color: 1
Size: 271055 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 421673 Color: 1
Size: 324882 Color: 1
Size: 253446 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 421915 Color: 1
Size: 308986 Color: 1
Size: 269100 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 421932 Color: 1
Size: 326461 Color: 1
Size: 251608 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 421939 Color: 1
Size: 307589 Color: 1
Size: 270473 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 422144 Color: 1
Size: 312430 Color: 1
Size: 265427 Color: 0

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 422280 Color: 1
Size: 304604 Color: 1
Size: 273117 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 422298 Color: 1
Size: 306579 Color: 1
Size: 271124 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 422397 Color: 1
Size: 300983 Color: 1
Size: 276621 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 422488 Color: 1
Size: 308853 Color: 1
Size: 268660 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 422613 Color: 1
Size: 325529 Color: 1
Size: 251859 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 422705 Color: 1
Size: 312349 Color: 1
Size: 264947 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 422709 Color: 1
Size: 325119 Color: 1
Size: 252173 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 422737 Color: 1
Size: 324746 Color: 1
Size: 252518 Color: 0

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 422752 Color: 1
Size: 315506 Color: 1
Size: 261743 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 422952 Color: 1
Size: 288595 Color: 1
Size: 288454 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 423143 Color: 1
Size: 301911 Color: 1
Size: 274947 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 423199 Color: 1
Size: 322991 Color: 1
Size: 253811 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 423214 Color: 1
Size: 311768 Color: 1
Size: 265019 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 423331 Color: 1
Size: 299500 Color: 1
Size: 277170 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 423339 Color: 1
Size: 295756 Color: 1
Size: 280906 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 423358 Color: 1
Size: 323358 Color: 1
Size: 253285 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 423547 Color: 1
Size: 307480 Color: 1
Size: 268974 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 423580 Color: 1
Size: 306461 Color: 1
Size: 269960 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 423630 Color: 1
Size: 291338 Color: 1
Size: 285033 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 423761 Color: 1
Size: 310927 Color: 1
Size: 265313 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 423890 Color: 1
Size: 310629 Color: 1
Size: 265482 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 423933 Color: 1
Size: 298583 Color: 1
Size: 277485 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 424053 Color: 1
Size: 297528 Color: 1
Size: 278420 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 424191 Color: 1
Size: 313420 Color: 1
Size: 262390 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 424217 Color: 1
Size: 291958 Color: 0
Size: 283826 Color: 1

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 424255 Color: 1
Size: 304695 Color: 1
Size: 271051 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 424307 Color: 1
Size: 319905 Color: 1
Size: 255789 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 424313 Color: 1
Size: 316033 Color: 1
Size: 259655 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 424410 Color: 1
Size: 293299 Color: 0
Size: 282292 Color: 1

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 424500 Color: 1
Size: 322051 Color: 0
Size: 253450 Color: 1

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 424604 Color: 1
Size: 311883 Color: 1
Size: 263514 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 424673 Color: 1
Size: 317698 Color: 1
Size: 257630 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 424719 Color: 1
Size: 304728 Color: 1
Size: 270554 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 424749 Color: 1
Size: 324446 Color: 1
Size: 250806 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 424818 Color: 1
Size: 311022 Color: 0
Size: 264161 Color: 1

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 424870 Color: 1
Size: 295644 Color: 1
Size: 279487 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 424969 Color: 1
Size: 313744 Color: 1
Size: 261288 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 424977 Color: 1
Size: 323860 Color: 1
Size: 251164 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 425165 Color: 1
Size: 296940 Color: 0
Size: 277896 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 425422 Color: 1
Size: 323024 Color: 1
Size: 251555 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 425428 Color: 1
Size: 296630 Color: 1
Size: 277943 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 425467 Color: 1
Size: 291222 Color: 1
Size: 283312 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 425544 Color: 1
Size: 310977 Color: 1
Size: 263480 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 425702 Color: 1
Size: 303865 Color: 1
Size: 270434 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 425757 Color: 1
Size: 308937 Color: 1
Size: 265307 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 425860 Color: 1
Size: 295790 Color: 1
Size: 278351 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 425869 Color: 1
Size: 308649 Color: 1
Size: 265483 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 425999 Color: 1
Size: 291257 Color: 0
Size: 282745 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 426058 Color: 1
Size: 299017 Color: 1
Size: 274926 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 426162 Color: 1
Size: 323698 Color: 1
Size: 250141 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 426205 Color: 1
Size: 320056 Color: 1
Size: 253740 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 426251 Color: 1
Size: 303613 Color: 1
Size: 270137 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 426255 Color: 1
Size: 307374 Color: 1
Size: 266372 Color: 0

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 426276 Color: 1
Size: 308163 Color: 1
Size: 265562 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 426403 Color: 1
Size: 319113 Color: 1
Size: 254485 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 426489 Color: 1
Size: 309973 Color: 1
Size: 263539 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 426577 Color: 1
Size: 318797 Color: 1
Size: 254627 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 426605 Color: 1
Size: 315303 Color: 1
Size: 258093 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 426639 Color: 1
Size: 317789 Color: 1
Size: 255573 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 426899 Color: 1
Size: 308421 Color: 1
Size: 264681 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 426929 Color: 1
Size: 299956 Color: 1
Size: 273116 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 426949 Color: 1
Size: 308838 Color: 0
Size: 264214 Color: 1

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 426970 Color: 1
Size: 309652 Color: 1
Size: 263379 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 426978 Color: 1
Size: 294387 Color: 1
Size: 278636 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 427060 Color: 1
Size: 307274 Color: 1
Size: 265667 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 427100 Color: 1
Size: 288280 Color: 1
Size: 284621 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 427320 Color: 1
Size: 314031 Color: 1
Size: 258650 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 427359 Color: 1
Size: 298851 Color: 1
Size: 273791 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 427369 Color: 1
Size: 308870 Color: 1
Size: 263762 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 427398 Color: 1
Size: 300871 Color: 1
Size: 271732 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 427418 Color: 1
Size: 299234 Color: 1
Size: 273349 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 427585 Color: 1
Size: 298325 Color: 1
Size: 274091 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 427632 Color: 1
Size: 301704 Color: 1
Size: 270665 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 427646 Color: 1
Size: 313539 Color: 1
Size: 258816 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 427672 Color: 1
Size: 318180 Color: 1
Size: 254149 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 427803 Color: 1
Size: 305321 Color: 1
Size: 266877 Color: 0

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 427888 Color: 1
Size: 314550 Color: 1
Size: 257563 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 428049 Color: 1
Size: 308624 Color: 1
Size: 263328 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 428058 Color: 1
Size: 314490 Color: 1
Size: 257453 Color: 0

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 428090 Color: 1
Size: 307855 Color: 1
Size: 264056 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 428107 Color: 1
Size: 314020 Color: 1
Size: 257874 Color: 0

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 428189 Color: 1
Size: 311820 Color: 1
Size: 259992 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 428295 Color: 1
Size: 304123 Color: 1
Size: 267583 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 428304 Color: 1
Size: 306256 Color: 1
Size: 265441 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 428384 Color: 1
Size: 306675 Color: 1
Size: 264942 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 428441 Color: 1
Size: 311128 Color: 1
Size: 260432 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 428512 Color: 1
Size: 311696 Color: 1
Size: 259793 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 428516 Color: 1
Size: 286478 Color: 1
Size: 285007 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 428521 Color: 1
Size: 321279 Color: 1
Size: 250201 Color: 0

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 428594 Color: 1
Size: 296717 Color: 0
Size: 274690 Color: 1

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 428645 Color: 1
Size: 309347 Color: 1
Size: 262009 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 428665 Color: 1
Size: 307663 Color: 1
Size: 263673 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 428716 Color: 1
Size: 307026 Color: 1
Size: 264259 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 428860 Color: 1
Size: 316481 Color: 1
Size: 254660 Color: 0

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 428860 Color: 1
Size: 303999 Color: 1
Size: 267142 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 429001 Color: 1
Size: 296276 Color: 0
Size: 274724 Color: 1

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 429027 Color: 1
Size: 290862 Color: 1
Size: 280112 Color: 0

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 429041 Color: 1
Size: 287560 Color: 0
Size: 283400 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 429130 Color: 1
Size: 317848 Color: 1
Size: 253023 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 429150 Color: 1
Size: 308580 Color: 1
Size: 262271 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 429186 Color: 1
Size: 307271 Color: 1
Size: 263544 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 429291 Color: 1
Size: 294574 Color: 1
Size: 276136 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 429327 Color: 1
Size: 299893 Color: 1
Size: 270781 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 429586 Color: 1
Size: 307872 Color: 1
Size: 262543 Color: 0

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 429649 Color: 1
Size: 313352 Color: 1
Size: 257000 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 429669 Color: 1
Size: 300721 Color: 1
Size: 269611 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 429731 Color: 1
Size: 315303 Color: 1
Size: 254967 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 429788 Color: 1
Size: 311201 Color: 1
Size: 259012 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 429788 Color: 1
Size: 310307 Color: 1
Size: 259906 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 429834 Color: 1
Size: 313450 Color: 1
Size: 256717 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 429867 Color: 1
Size: 310289 Color: 1
Size: 259845 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 429870 Color: 1
Size: 307107 Color: 1
Size: 263024 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 430032 Color: 1
Size: 301228 Color: 1
Size: 268741 Color: 0

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 430048 Color: 1
Size: 314244 Color: 1
Size: 255709 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 430107 Color: 1
Size: 292823 Color: 1
Size: 277071 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 430137 Color: 1
Size: 312295 Color: 1
Size: 257569 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 430157 Color: 1
Size: 294642 Color: 1
Size: 275202 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 430226 Color: 1
Size: 308227 Color: 1
Size: 261548 Color: 0

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 430275 Color: 1
Size: 285998 Color: 1
Size: 283728 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 430348 Color: 1
Size: 318205 Color: 1
Size: 251448 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 430400 Color: 1
Size: 318420 Color: 1
Size: 251181 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 430427 Color: 1
Size: 316564 Color: 1
Size: 253010 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 430428 Color: 1
Size: 286168 Color: 0
Size: 283405 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 430430 Color: 1
Size: 311065 Color: 1
Size: 258506 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 430570 Color: 1
Size: 297025 Color: 1
Size: 272406 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 430697 Color: 1
Size: 302907 Color: 1
Size: 266397 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 430708 Color: 1
Size: 315949 Color: 1
Size: 253344 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 430869 Color: 1
Size: 314167 Color: 1
Size: 254965 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 431105 Color: 1
Size: 315284 Color: 1
Size: 253612 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 431107 Color: 1
Size: 300085 Color: 1
Size: 268809 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 431214 Color: 1
Size: 314274 Color: 1
Size: 254513 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 431247 Color: 1
Size: 307218 Color: 1
Size: 261536 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 431315 Color: 1
Size: 309951 Color: 1
Size: 258735 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 431391 Color: 1
Size: 294650 Color: 1
Size: 273960 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 431449 Color: 1
Size: 303417 Color: 1
Size: 265135 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 431486 Color: 1
Size: 314806 Color: 1
Size: 253709 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 431531 Color: 1
Size: 313823 Color: 1
Size: 254647 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 431767 Color: 1
Size: 303504 Color: 1
Size: 264730 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 431795 Color: 1
Size: 302022 Color: 1
Size: 266184 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 431877 Color: 1
Size: 306695 Color: 1
Size: 261429 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 431896 Color: 1
Size: 302570 Color: 1
Size: 265535 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 431924 Color: 1
Size: 307655 Color: 1
Size: 260422 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 431945 Color: 1
Size: 310648 Color: 1
Size: 257408 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 432139 Color: 1
Size: 310036 Color: 1
Size: 257826 Color: 0

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 432191 Color: 1
Size: 304331 Color: 1
Size: 263479 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 432217 Color: 1
Size: 305682 Color: 1
Size: 262102 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 432525 Color: 1
Size: 303783 Color: 1
Size: 263693 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 432613 Color: 1
Size: 300271 Color: 1
Size: 267117 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 432628 Color: 1
Size: 301651 Color: 1
Size: 265722 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 432637 Color: 1
Size: 301300 Color: 1
Size: 266064 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 432820 Color: 1
Size: 304994 Color: 1
Size: 262187 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 432863 Color: 1
Size: 310757 Color: 1
Size: 256381 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 433019 Color: 1
Size: 310204 Color: 1
Size: 256778 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 433050 Color: 1
Size: 305784 Color: 1
Size: 261167 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 433053 Color: 1
Size: 302744 Color: 1
Size: 264204 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 433140 Color: 1
Size: 307992 Color: 1
Size: 258869 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 433231 Color: 1
Size: 285039 Color: 1
Size: 281731 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 433242 Color: 1
Size: 305554 Color: 1
Size: 261205 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 433260 Color: 1
Size: 293699 Color: 1
Size: 273042 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 433288 Color: 1
Size: 302036 Color: 1
Size: 264677 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 433330 Color: 1
Size: 310567 Color: 1
Size: 256104 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 433397 Color: 1
Size: 314635 Color: 1
Size: 251969 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 433423 Color: 1
Size: 303746 Color: 1
Size: 262832 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 433507 Color: 1
Size: 309511 Color: 1
Size: 256983 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 433510 Color: 1
Size: 308349 Color: 1
Size: 258142 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 433537 Color: 1
Size: 291294 Color: 1
Size: 275170 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 433597 Color: 1
Size: 310320 Color: 1
Size: 256084 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 433631 Color: 1
Size: 315051 Color: 1
Size: 251319 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 433645 Color: 1
Size: 288108 Color: 1
Size: 278248 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 433731 Color: 1
Size: 293094 Color: 1
Size: 273176 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 433814 Color: 1
Size: 297350 Color: 1
Size: 268837 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 433876 Color: 1
Size: 291168 Color: 1
Size: 274957 Color: 0

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 434028 Color: 1
Size: 300450 Color: 1
Size: 265523 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 434210 Color: 1
Size: 296323 Color: 1
Size: 269468 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 434252 Color: 1
Size: 304234 Color: 1
Size: 261515 Color: 0

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 434288 Color: 1
Size: 305320 Color: 1
Size: 260393 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 434331 Color: 1
Size: 301881 Color: 1
Size: 263789 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 434452 Color: 1
Size: 302817 Color: 1
Size: 262732 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 434508 Color: 1
Size: 295441 Color: 1
Size: 270052 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 434571 Color: 1
Size: 298372 Color: 1
Size: 267058 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 434643 Color: 1
Size: 312383 Color: 1
Size: 252975 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 434683 Color: 1
Size: 292612 Color: 1
Size: 272706 Color: 0

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 434731 Color: 1
Size: 288240 Color: 1
Size: 277030 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 434734 Color: 1
Size: 309855 Color: 1
Size: 255412 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 434752 Color: 1
Size: 298723 Color: 1
Size: 266526 Color: 0

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 434784 Color: 1
Size: 307631 Color: 1
Size: 257586 Color: 0

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 434786 Color: 1
Size: 288263 Color: 1
Size: 276952 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 434855 Color: 1
Size: 287762 Color: 1
Size: 277384 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 434915 Color: 1
Size: 307843 Color: 1
Size: 257243 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 434972 Color: 1
Size: 299451 Color: 1
Size: 265578 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 435043 Color: 1
Size: 292149 Color: 1
Size: 272809 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 435050 Color: 1
Size: 302735 Color: 1
Size: 262216 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 435182 Color: 1
Size: 295592 Color: 1
Size: 269227 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 435308 Color: 1
Size: 309300 Color: 1
Size: 255393 Color: 0

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 435321 Color: 1
Size: 303929 Color: 1
Size: 260751 Color: 0

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 435348 Color: 1
Size: 298816 Color: 1
Size: 265837 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 435438 Color: 1
Size: 303058 Color: 1
Size: 261505 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 435442 Color: 1
Size: 287567 Color: 1
Size: 276992 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 435505 Color: 1
Size: 290935 Color: 1
Size: 273561 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 435552 Color: 1
Size: 303672 Color: 1
Size: 260777 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 435580 Color: 1
Size: 295130 Color: 0
Size: 269291 Color: 1

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 435643 Color: 1
Size: 308989 Color: 1
Size: 255369 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 435696 Color: 1
Size: 297393 Color: 1
Size: 266912 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 435749 Color: 1
Size: 296313 Color: 1
Size: 267939 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 435783 Color: 1
Size: 291282 Color: 1
Size: 272936 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 435831 Color: 1
Size: 284181 Color: 0
Size: 279989 Color: 1

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 435908 Color: 1
Size: 309032 Color: 1
Size: 255061 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 436011 Color: 1
Size: 291733 Color: 1
Size: 272257 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 436034 Color: 1
Size: 295868 Color: 1
Size: 268099 Color: 0

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 436132 Color: 1
Size: 296606 Color: 1
Size: 267263 Color: 0

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 436253 Color: 1
Size: 303158 Color: 1
Size: 260590 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 436337 Color: 1
Size: 300445 Color: 1
Size: 263219 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 436398 Color: 1
Size: 292970 Color: 1
Size: 270633 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 436536 Color: 1
Size: 304617 Color: 1
Size: 258848 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 436561 Color: 1
Size: 283319 Color: 0
Size: 280121 Color: 1

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 436566 Color: 1
Size: 306211 Color: 1
Size: 257224 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 436567 Color: 1
Size: 311329 Color: 1
Size: 252105 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 436732 Color: 1
Size: 284925 Color: 0
Size: 278344 Color: 1

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 436918 Color: 1
Size: 304224 Color: 1
Size: 258859 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 436930 Color: 1
Size: 301189 Color: 1
Size: 261882 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 436950 Color: 1
Size: 289185 Color: 1
Size: 273866 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 437105 Color: 1
Size: 291522 Color: 1
Size: 271374 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 437126 Color: 1
Size: 294876 Color: 1
Size: 267999 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 437266 Color: 1
Size: 300367 Color: 1
Size: 262368 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 437288 Color: 1
Size: 287710 Color: 1
Size: 275003 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 437363 Color: 1
Size: 291130 Color: 1
Size: 271508 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 437371 Color: 1
Size: 304620 Color: 1
Size: 258010 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 437465 Color: 1
Size: 305122 Color: 1
Size: 257414 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 437553 Color: 1
Size: 308922 Color: 1
Size: 253526 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 437655 Color: 1
Size: 298986 Color: 1
Size: 263360 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 437751 Color: 1
Size: 283194 Color: 1
Size: 279056 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 437770 Color: 1
Size: 307549 Color: 1
Size: 254682 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 437895 Color: 1
Size: 289718 Color: 1
Size: 272388 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 438078 Color: 1
Size: 291557 Color: 1
Size: 270366 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 438147 Color: 1
Size: 308421 Color: 1
Size: 253433 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 438202 Color: 1
Size: 305050 Color: 1
Size: 256749 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 438225 Color: 1
Size: 305629 Color: 1
Size: 256147 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 438238 Color: 1
Size: 288959 Color: 1
Size: 272804 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 438289 Color: 1
Size: 308268 Color: 1
Size: 253444 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 438328 Color: 1
Size: 294411 Color: 1
Size: 267262 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 438365 Color: 1
Size: 309136 Color: 1
Size: 252500 Color: 0

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 438400 Color: 1
Size: 301504 Color: 1
Size: 260097 Color: 0

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 438458 Color: 1
Size: 297292 Color: 1
Size: 264251 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 438502 Color: 1
Size: 302243 Color: 1
Size: 259256 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 438570 Color: 1
Size: 305067 Color: 1
Size: 256364 Color: 0

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 438633 Color: 1
Size: 301586 Color: 1
Size: 259782 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 438694 Color: 1
Size: 285638 Color: 1
Size: 275669 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 438710 Color: 1
Size: 303900 Color: 1
Size: 257391 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 438739 Color: 1
Size: 281463 Color: 1
Size: 279799 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 438772 Color: 1
Size: 291296 Color: 1
Size: 269933 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 438890 Color: 1
Size: 304679 Color: 1
Size: 256432 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 438904 Color: 1
Size: 298536 Color: 1
Size: 262561 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 438935 Color: 1
Size: 294622 Color: 1
Size: 266444 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 439120 Color: 1
Size: 295350 Color: 1
Size: 265531 Color: 0

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 439143 Color: 1
Size: 292631 Color: 1
Size: 268227 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 439143 Color: 1
Size: 283612 Color: 1
Size: 277246 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 439327 Color: 1
Size: 309316 Color: 1
Size: 251358 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 439505 Color: 1
Size: 286271 Color: 1
Size: 274225 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 439511 Color: 1
Size: 285741 Color: 0
Size: 274749 Color: 1

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 439518 Color: 1
Size: 289351 Color: 1
Size: 271132 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 439640 Color: 1
Size: 295042 Color: 1
Size: 265319 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 439658 Color: 1
Size: 292853 Color: 1
Size: 267490 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 439730 Color: 1
Size: 305302 Color: 1
Size: 254969 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 439842 Color: 1
Size: 299814 Color: 1
Size: 260345 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 439883 Color: 1
Size: 288544 Color: 1
Size: 271574 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 439958 Color: 1
Size: 304251 Color: 1
Size: 255792 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 439995 Color: 1
Size: 290101 Color: 1
Size: 269905 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 440002 Color: 1
Size: 290883 Color: 1
Size: 269116 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 440005 Color: 1
Size: 280631 Color: 0
Size: 279365 Color: 1

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 440141 Color: 1
Size: 302387 Color: 1
Size: 257473 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 440205 Color: 1
Size: 295163 Color: 1
Size: 264633 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 440241 Color: 1
Size: 294795 Color: 1
Size: 264965 Color: 0

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 440424 Color: 1
Size: 299087 Color: 1
Size: 260490 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 440567 Color: 1
Size: 300665 Color: 1
Size: 258769 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 440642 Color: 1
Size: 305801 Color: 1
Size: 253558 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 440661 Color: 1
Size: 282041 Color: 0
Size: 277299 Color: 1

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 440758 Color: 1
Size: 298296 Color: 1
Size: 260947 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 441009 Color: 1
Size: 288359 Color: 1
Size: 270633 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 441086 Color: 1
Size: 305913 Color: 1
Size: 253002 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 441103 Color: 1
Size: 294684 Color: 0
Size: 264214 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 441137 Color: 1
Size: 304002 Color: 1
Size: 254862 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 447383 Color: 1
Size: 301913 Color: 1
Size: 250705 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 441202 Color: 1
Size: 301088 Color: 1
Size: 257711 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 441212 Color: 1
Size: 293751 Color: 1
Size: 265038 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 441280 Color: 1
Size: 294931 Color: 1
Size: 263790 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 441324 Color: 1
Size: 305441 Color: 1
Size: 253236 Color: 0

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 441328 Color: 1
Size: 290911 Color: 1
Size: 267762 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 441381 Color: 1
Size: 281897 Color: 1
Size: 276723 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 441387 Color: 1
Size: 303116 Color: 1
Size: 255498 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 441453 Color: 1
Size: 298656 Color: 1
Size: 259892 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 441516 Color: 1
Size: 301157 Color: 1
Size: 257328 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 441526 Color: 1
Size: 297919 Color: 1
Size: 260556 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 441650 Color: 1
Size: 303679 Color: 1
Size: 254672 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 441719 Color: 1
Size: 306979 Color: 1
Size: 251303 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 441855 Color: 1
Size: 295402 Color: 1
Size: 262744 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 441929 Color: 1
Size: 304446 Color: 1
Size: 253626 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 441975 Color: 1
Size: 294961 Color: 0
Size: 263065 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 442022 Color: 1
Size: 293613 Color: 1
Size: 264366 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 442083 Color: 1
Size: 279022 Color: 1
Size: 278896 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 442124 Color: 1
Size: 280584 Color: 1
Size: 277293 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 442124 Color: 1
Size: 279421 Color: 1
Size: 278456 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 442174 Color: 1
Size: 288709 Color: 1
Size: 269118 Color: 0

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 442208 Color: 1
Size: 300248 Color: 1
Size: 257545 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 442236 Color: 1
Size: 286802 Color: 0
Size: 270963 Color: 1

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 442289 Color: 1
Size: 298993 Color: 1
Size: 258719 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 442316 Color: 1
Size: 293780 Color: 1
Size: 263905 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 442572 Color: 1
Size: 280970 Color: 0
Size: 276459 Color: 1

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 442823 Color: 1
Size: 299600 Color: 1
Size: 257578 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 442905 Color: 1
Size: 301075 Color: 1
Size: 256021 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 442944 Color: 1
Size: 298887 Color: 1
Size: 258170 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 443085 Color: 1
Size: 283209 Color: 1
Size: 273707 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 443133 Color: 1
Size: 299716 Color: 1
Size: 257152 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 443225 Color: 1
Size: 293938 Color: 0
Size: 262838 Color: 1

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 443281 Color: 1
Size: 292229 Color: 1
Size: 264491 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 443368 Color: 1
Size: 291382 Color: 1
Size: 265251 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 443381 Color: 1
Size: 286798 Color: 1
Size: 269822 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 443434 Color: 1
Size: 287261 Color: 1
Size: 269306 Color: 0

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 443438 Color: 1
Size: 304063 Color: 1
Size: 252500 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 443440 Color: 1
Size: 281780 Color: 1
Size: 274781 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 443472 Color: 1
Size: 296509 Color: 1
Size: 260020 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 443475 Color: 1
Size: 294483 Color: 1
Size: 262043 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 443552 Color: 1
Size: 304165 Color: 1
Size: 252284 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 443558 Color: 1
Size: 295192 Color: 1
Size: 261251 Color: 0

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 443592 Color: 1
Size: 280970 Color: 1
Size: 275439 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 443652 Color: 1
Size: 285074 Color: 1
Size: 271275 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 443660 Color: 1
Size: 290348 Color: 1
Size: 265993 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 443718 Color: 1
Size: 298078 Color: 1
Size: 258205 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 443739 Color: 1
Size: 297086 Color: 1
Size: 259176 Color: 0

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 443772 Color: 1
Size: 299103 Color: 1
Size: 257126 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 443802 Color: 1
Size: 296908 Color: 1
Size: 259291 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 443911 Color: 1
Size: 280119 Color: 0
Size: 275971 Color: 1

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 444000 Color: 1
Size: 296630 Color: 1
Size: 259371 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 444088 Color: 1
Size: 300647 Color: 1
Size: 255266 Color: 0

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 444325 Color: 1
Size: 301523 Color: 1
Size: 254153 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 444333 Color: 1
Size: 303814 Color: 1
Size: 251854 Color: 0

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 444379 Color: 1
Size: 302410 Color: 1
Size: 253212 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 444388 Color: 1
Size: 297713 Color: 1
Size: 257900 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 444478 Color: 1
Size: 278732 Color: 1
Size: 276791 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 444489 Color: 1
Size: 302486 Color: 1
Size: 253026 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 444556 Color: 1
Size: 279774 Color: 0
Size: 275671 Color: 1

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 444646 Color: 1
Size: 298678 Color: 1
Size: 256677 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 444652 Color: 1
Size: 294294 Color: 1
Size: 261055 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 444748 Color: 1
Size: 282690 Color: 1
Size: 272563 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 444827 Color: 1
Size: 290197 Color: 1
Size: 264977 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 444997 Color: 1
Size: 300530 Color: 1
Size: 254474 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 445110 Color: 1
Size: 291349 Color: 1
Size: 263542 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 445188 Color: 1
Size: 277636 Color: 1
Size: 277177 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 445196 Color: 1
Size: 298546 Color: 1
Size: 256259 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 445226 Color: 1
Size: 287764 Color: 1
Size: 267011 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 445263 Color: 1
Size: 290718 Color: 1
Size: 264020 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 445270 Color: 1
Size: 291409 Color: 1
Size: 263322 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 445409 Color: 1
Size: 282618 Color: 1
Size: 271974 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 445818 Color: 1
Size: 280926 Color: 0
Size: 273257 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 445836 Color: 1
Size: 285661 Color: 1
Size: 268504 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 397286 Color: 1
Size: 344064 Color: 1
Size: 258651 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 445927 Color: 1
Size: 280115 Color: 1
Size: 273959 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 446070 Color: 1
Size: 279086 Color: 0
Size: 274845 Color: 1

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 446113 Color: 1
Size: 297097 Color: 1
Size: 256791 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 446121 Color: 1
Size: 295334 Color: 1
Size: 258546 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 446125 Color: 1
Size: 289457 Color: 1
Size: 264419 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 446127 Color: 1
Size: 285297 Color: 1
Size: 268577 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 446140 Color: 1
Size: 301015 Color: 1
Size: 252846 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 446226 Color: 1
Size: 300815 Color: 1
Size: 252960 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 446398 Color: 1
Size: 296098 Color: 1
Size: 257505 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 446411 Color: 1
Size: 287225 Color: 1
Size: 266365 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 446434 Color: 1
Size: 293284 Color: 1
Size: 260283 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 446509 Color: 1
Size: 294292 Color: 1
Size: 259200 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 446655 Color: 1
Size: 297000 Color: 1
Size: 256346 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 446703 Color: 1
Size: 290757 Color: 1
Size: 262541 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 446762 Color: 1
Size: 283039 Color: 1
Size: 270200 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 446852 Color: 1
Size: 291514 Color: 1
Size: 261635 Color: 0

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 446924 Color: 1
Size: 291166 Color: 1
Size: 261911 Color: 0

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 446936 Color: 1
Size: 293403 Color: 1
Size: 259662 Color: 0

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 446948 Color: 1
Size: 281752 Color: 1
Size: 271301 Color: 0

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 446968 Color: 1
Size: 298031 Color: 1
Size: 255002 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 447135 Color: 1
Size: 292832 Color: 1
Size: 260034 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 447219 Color: 1
Size: 298430 Color: 1
Size: 254352 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 447237 Color: 1
Size: 293911 Color: 1
Size: 258853 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 447304 Color: 1
Size: 297336 Color: 1
Size: 255361 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 447423 Color: 1
Size: 285187 Color: 1
Size: 267391 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 447511 Color: 1
Size: 300759 Color: 1
Size: 251731 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 447532 Color: 1
Size: 291373 Color: 1
Size: 261096 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 447623 Color: 1
Size: 285001 Color: 1
Size: 267377 Color: 0

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 447633 Color: 1
Size: 283743 Color: 1
Size: 268625 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 447634 Color: 1
Size: 285857 Color: 1
Size: 266510 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 447677 Color: 1
Size: 295860 Color: 1
Size: 256464 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 447688 Color: 1
Size: 281582 Color: 1
Size: 270731 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 447796 Color: 1
Size: 295420 Color: 1
Size: 256785 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 447802 Color: 1
Size: 288775 Color: 1
Size: 263424 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 447826 Color: 1
Size: 297359 Color: 1
Size: 254816 Color: 0

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 448008 Color: 1
Size: 278338 Color: 1
Size: 273655 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 448029 Color: 1
Size: 297091 Color: 1
Size: 254881 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 448114 Color: 1
Size: 290871 Color: 1
Size: 261016 Color: 0

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 448240 Color: 1
Size: 294570 Color: 1
Size: 257191 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 448531 Color: 1
Size: 295076 Color: 1
Size: 256394 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 448602 Color: 1
Size: 295716 Color: 1
Size: 255683 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 448673 Color: 1
Size: 296651 Color: 1
Size: 254677 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 448712 Color: 1
Size: 286795 Color: 1
Size: 264494 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 448789 Color: 1
Size: 295130 Color: 1
Size: 256082 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 448812 Color: 1
Size: 291385 Color: 1
Size: 259804 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 448839 Color: 1
Size: 286352 Color: 1
Size: 264810 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 448926 Color: 1
Size: 292254 Color: 1
Size: 258821 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 448996 Color: 1
Size: 291135 Color: 1
Size: 259870 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 449115 Color: 1
Size: 297699 Color: 1
Size: 253187 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 449170 Color: 1
Size: 290262 Color: 1
Size: 260569 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 449235 Color: 1
Size: 299408 Color: 1
Size: 251358 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 449286 Color: 1
Size: 295263 Color: 1
Size: 255452 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 449354 Color: 1
Size: 296115 Color: 1
Size: 254532 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 449358 Color: 1
Size: 294155 Color: 1
Size: 256488 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 449402 Color: 1
Size: 288966 Color: 1
Size: 261633 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 449420 Color: 1
Size: 280354 Color: 1
Size: 270227 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 449472 Color: 1
Size: 287669 Color: 1
Size: 262860 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 449477 Color: 1
Size: 296243 Color: 1
Size: 254281 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 449512 Color: 1
Size: 287580 Color: 1
Size: 262909 Color: 0

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 449585 Color: 1
Size: 283462 Color: 0
Size: 266954 Color: 1

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 449646 Color: 1
Size: 281516 Color: 1
Size: 268839 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 449663 Color: 1
Size: 294583 Color: 1
Size: 255755 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 449814 Color: 1
Size: 278018 Color: 1
Size: 272169 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 449852 Color: 1
Size: 295535 Color: 1
Size: 254614 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 449919 Color: 1
Size: 290259 Color: 1
Size: 259823 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 449926 Color: 1
Size: 292311 Color: 1
Size: 257764 Color: 0

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 450114 Color: 1
Size: 298825 Color: 1
Size: 251062 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 450151 Color: 1
Size: 294132 Color: 1
Size: 255718 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 450215 Color: 1
Size: 292030 Color: 1
Size: 257756 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 450317 Color: 1
Size: 298258 Color: 1
Size: 251426 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 450404 Color: 1
Size: 291431 Color: 1
Size: 258166 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 450413 Color: 1
Size: 289081 Color: 1
Size: 260507 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 450438 Color: 1
Size: 276809 Color: 1
Size: 272754 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 450444 Color: 1
Size: 291024 Color: 1
Size: 258533 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 450463 Color: 1
Size: 283216 Color: 1
Size: 266322 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 450469 Color: 1
Size: 286516 Color: 1
Size: 263016 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 450508 Color: 1
Size: 283257 Color: 1
Size: 266236 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 450552 Color: 1
Size: 286764 Color: 1
Size: 262685 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 450580 Color: 1
Size: 275487 Color: 0
Size: 273934 Color: 1

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 450596 Color: 1
Size: 293757 Color: 1
Size: 255648 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 450605 Color: 1
Size: 286153 Color: 1
Size: 263243 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 450774 Color: 1
Size: 278189 Color: 1
Size: 271038 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 451006 Color: 1
Size: 289553 Color: 1
Size: 259442 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 451010 Color: 1
Size: 291527 Color: 1
Size: 257464 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 451014 Color: 1
Size: 274723 Color: 1
Size: 274264 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 451161 Color: 1
Size: 287866 Color: 1
Size: 260974 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 451230 Color: 1
Size: 285357 Color: 1
Size: 263414 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 451260 Color: 1
Size: 298650 Color: 1
Size: 250091 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 451263 Color: 1
Size: 289840 Color: 1
Size: 258898 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 451529 Color: 1
Size: 295775 Color: 1
Size: 252697 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 451622 Color: 1
Size: 275063 Color: 0
Size: 273316 Color: 1

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 451791 Color: 1
Size: 286178 Color: 1
Size: 262032 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 451912 Color: 1
Size: 291293 Color: 1
Size: 256796 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 452027 Color: 1
Size: 291716 Color: 1
Size: 256258 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 452220 Color: 1
Size: 294821 Color: 1
Size: 252960 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 452250 Color: 1
Size: 274891 Color: 0
Size: 272860 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 452292 Color: 1
Size: 294553 Color: 1
Size: 253156 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 452320 Color: 1
Size: 289432 Color: 1
Size: 258249 Color: 0

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 452409 Color: 1
Size: 278212 Color: 1
Size: 269380 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 452534 Color: 1
Size: 280880 Color: 1
Size: 266587 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 452545 Color: 1
Size: 282190 Color: 1
Size: 265266 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 452614 Color: 1
Size: 280691 Color: 1
Size: 266696 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 452628 Color: 1
Size: 286174 Color: 1
Size: 261199 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 452636 Color: 1
Size: 292198 Color: 1
Size: 255167 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 452695 Color: 1
Size: 277679 Color: 1
Size: 269627 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 452704 Color: 1
Size: 286323 Color: 1
Size: 260974 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 452719 Color: 1
Size: 289468 Color: 1
Size: 257814 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 452819 Color: 1
Size: 292121 Color: 1
Size: 255061 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 452906 Color: 1
Size: 292598 Color: 1
Size: 254497 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 452961 Color: 1
Size: 290431 Color: 1
Size: 256609 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 452962 Color: 1
Size: 286660 Color: 1
Size: 260379 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 452973 Color: 1
Size: 278428 Color: 1
Size: 268600 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 453119 Color: 1
Size: 274800 Color: 1
Size: 272082 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 453152 Color: 1
Size: 292523 Color: 1
Size: 254326 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 453213 Color: 1
Size: 293805 Color: 1
Size: 252983 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 453219 Color: 1
Size: 290886 Color: 1
Size: 255896 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 453351 Color: 1
Size: 290725 Color: 1
Size: 255925 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 453404 Color: 1
Size: 294941 Color: 1
Size: 251656 Color: 0

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 453580 Color: 1
Size: 290867 Color: 1
Size: 255554 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 453593 Color: 1
Size: 275945 Color: 1
Size: 270463 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 453609 Color: 1
Size: 292457 Color: 1
Size: 253935 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 453655 Color: 1
Size: 280311 Color: 1
Size: 266035 Color: 0

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 453660 Color: 1
Size: 289315 Color: 1
Size: 257026 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 453667 Color: 1
Size: 291451 Color: 1
Size: 254883 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 453751 Color: 1
Size: 293781 Color: 1
Size: 252469 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 453805 Color: 1
Size: 283196 Color: 1
Size: 263000 Color: 0

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 453884 Color: 1
Size: 279364 Color: 1
Size: 266753 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 454120 Color: 1
Size: 293712 Color: 1
Size: 252169 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 454208 Color: 1
Size: 281188 Color: 1
Size: 264605 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 454209 Color: 1
Size: 279586 Color: 1
Size: 266206 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 454210 Color: 1
Size: 288821 Color: 1
Size: 256970 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 454216 Color: 1
Size: 284636 Color: 1
Size: 261149 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 454284 Color: 1
Size: 281815 Color: 1
Size: 263902 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 454750 Color: 1
Size: 283804 Color: 1
Size: 261447 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 454782 Color: 1
Size: 278323 Color: 1
Size: 266896 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 454885 Color: 1
Size: 290981 Color: 1
Size: 254135 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 454935 Color: 1
Size: 279526 Color: 1
Size: 265540 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 455057 Color: 1
Size: 274115 Color: 0
Size: 270829 Color: 1

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 455069 Color: 1
Size: 284212 Color: 1
Size: 260720 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 455088 Color: 1
Size: 276753 Color: 0
Size: 268160 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 455088 Color: 1
Size: 275799 Color: 0
Size: 269114 Color: 1

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 455172 Color: 1
Size: 294477 Color: 1
Size: 250352 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 455348 Color: 1
Size: 283824 Color: 1
Size: 260829 Color: 0

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 455352 Color: 1
Size: 290151 Color: 1
Size: 254498 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 455374 Color: 1
Size: 291429 Color: 1
Size: 253198 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 455466 Color: 1
Size: 290740 Color: 1
Size: 253795 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 455525 Color: 1
Size: 288889 Color: 1
Size: 255587 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 455560 Color: 1
Size: 278889 Color: 1
Size: 265552 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 455563 Color: 1
Size: 293154 Color: 1
Size: 251284 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 455662 Color: 1
Size: 292532 Color: 1
Size: 251807 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 455743 Color: 1
Size: 286187 Color: 1
Size: 258071 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 455744 Color: 1
Size: 285389 Color: 1
Size: 258868 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 455754 Color: 1
Size: 285122 Color: 1
Size: 259125 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 455766 Color: 1
Size: 279924 Color: 1
Size: 264311 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 455813 Color: 1
Size: 284476 Color: 1
Size: 259712 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 455865 Color: 1
Size: 288041 Color: 1
Size: 256095 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 455906 Color: 1
Size: 274723 Color: 1
Size: 269372 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 456046 Color: 1
Size: 287708 Color: 1
Size: 256247 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 456088 Color: 1
Size: 282195 Color: 1
Size: 261718 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 456090 Color: 1
Size: 293051 Color: 1
Size: 250860 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 456103 Color: 1
Size: 279098 Color: 1
Size: 264800 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 456248 Color: 1
Size: 276501 Color: 1
Size: 267252 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 456366 Color: 1
Size: 283522 Color: 1
Size: 260113 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 456531 Color: 1
Size: 286977 Color: 1
Size: 256493 Color: 0

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 456880 Color: 1
Size: 271704 Color: 1
Size: 271417 Color: 0

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 456911 Color: 1
Size: 290803 Color: 1
Size: 252287 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 457203 Color: 1
Size: 279390 Color: 1
Size: 263408 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 457283 Color: 1
Size: 287323 Color: 1
Size: 255395 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 457613 Color: 1
Size: 273044 Color: 0
Size: 269344 Color: 1

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 457769 Color: 1
Size: 277435 Color: 1
Size: 264797 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 457784 Color: 1
Size: 285474 Color: 1
Size: 256743 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 457812 Color: 1
Size: 288700 Color: 1
Size: 253489 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 457815 Color: 1
Size: 291474 Color: 1
Size: 250712 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 457913 Color: 1
Size: 272051 Color: 1
Size: 270037 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 457938 Color: 1
Size: 284208 Color: 1
Size: 257855 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 458070 Color: 1
Size: 283556 Color: 1
Size: 258375 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 458106 Color: 1
Size: 278512 Color: 1
Size: 263383 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 458132 Color: 1
Size: 282463 Color: 1
Size: 259406 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 458173 Color: 1
Size: 288714 Color: 1
Size: 253114 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 458209 Color: 1
Size: 273101 Color: 1
Size: 268691 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 458234 Color: 1
Size: 275154 Color: 0
Size: 266613 Color: 1

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 458241 Color: 1
Size: 290501 Color: 1
Size: 251259 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 458256 Color: 1
Size: 278284 Color: 1
Size: 263461 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 458508 Color: 1
Size: 284847 Color: 1
Size: 256646 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 458530 Color: 1
Size: 279839 Color: 1
Size: 261632 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 458729 Color: 1
Size: 287131 Color: 1
Size: 254141 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 458825 Color: 1
Size: 287968 Color: 1
Size: 253208 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 458868 Color: 1
Size: 278521 Color: 1
Size: 262612 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 458975 Color: 1
Size: 290113 Color: 1
Size: 250913 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 459043 Color: 1
Size: 272725 Color: 0
Size: 268233 Color: 1

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 459148 Color: 1
Size: 283074 Color: 1
Size: 257779 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 459262 Color: 1
Size: 285991 Color: 1
Size: 254748 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 459318 Color: 1
Size: 271638 Color: 1
Size: 269045 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 459393 Color: 1
Size: 274404 Color: 0
Size: 266204 Color: 1

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 459414 Color: 1
Size: 289188 Color: 1
Size: 251399 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 459494 Color: 1
Size: 275094 Color: 1
Size: 265413 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 459520 Color: 1
Size: 284636 Color: 1
Size: 255845 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 459741 Color: 1
Size: 281756 Color: 1
Size: 258504 Color: 0

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 459881 Color: 1
Size: 285407 Color: 1
Size: 254713 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 459912 Color: 1
Size: 270748 Color: 1
Size: 269341 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 459972 Color: 1
Size: 276943 Color: 1
Size: 263086 Color: 0

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 460078 Color: 1
Size: 277140 Color: 1
Size: 262783 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 460105 Color: 1
Size: 282211 Color: 1
Size: 257685 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 460139 Color: 1
Size: 274846 Color: 1
Size: 265016 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 460144 Color: 1
Size: 282434 Color: 1
Size: 257423 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 460234 Color: 1
Size: 279005 Color: 1
Size: 260762 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 460240 Color: 1
Size: 284054 Color: 1
Size: 255707 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 460268 Color: 1
Size: 288836 Color: 1
Size: 250897 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 460278 Color: 1
Size: 285670 Color: 1
Size: 254053 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 460660 Color: 1
Size: 288769 Color: 1
Size: 250572 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 460866 Color: 1
Size: 288909 Color: 1
Size: 250226 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 460866 Color: 1
Size: 282588 Color: 1
Size: 256547 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 460939 Color: 1
Size: 282134 Color: 1
Size: 256928 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 460962 Color: 1
Size: 272513 Color: 1
Size: 266526 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 461270 Color: 1
Size: 270455 Color: 0
Size: 268276 Color: 1

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 461293 Color: 1
Size: 281081 Color: 1
Size: 257627 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 461355 Color: 1
Size: 273754 Color: 1
Size: 264892 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 461359 Color: 1
Size: 279974 Color: 1
Size: 258668 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 461410 Color: 1
Size: 282328 Color: 1
Size: 256263 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 461537 Color: 1
Size: 280028 Color: 1
Size: 258436 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 461553 Color: 1
Size: 279497 Color: 1
Size: 258951 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 461764 Color: 1
Size: 285147 Color: 1
Size: 253090 Color: 0

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 461998 Color: 1
Size: 283389 Color: 1
Size: 254614 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 462114 Color: 1
Size: 272046 Color: 1
Size: 265841 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 462265 Color: 1
Size: 279650 Color: 1
Size: 258086 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 462370 Color: 1
Size: 283611 Color: 1
Size: 254020 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 462429 Color: 1
Size: 282150 Color: 1
Size: 255422 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 462462 Color: 1
Size: 286320 Color: 1
Size: 251219 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 462534 Color: 1
Size: 273681 Color: 1
Size: 263786 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 462823 Color: 1
Size: 282588 Color: 1
Size: 254590 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 462866 Color: 1
Size: 286278 Color: 1
Size: 250857 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 463098 Color: 1
Size: 280950 Color: 1
Size: 255953 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 463197 Color: 1
Size: 283845 Color: 1
Size: 252959 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 463237 Color: 1
Size: 275047 Color: 1
Size: 261717 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 463251 Color: 1
Size: 279487 Color: 1
Size: 257263 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 463413 Color: 1
Size: 286422 Color: 1
Size: 250166 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 463550 Color: 1
Size: 275734 Color: 1
Size: 260717 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 463633 Color: 1
Size: 273684 Color: 1
Size: 262684 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 463909 Color: 1
Size: 277375 Color: 1
Size: 258717 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 464070 Color: 1
Size: 285135 Color: 1
Size: 250796 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 464111 Color: 1
Size: 280297 Color: 1
Size: 255593 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 464129 Color: 1
Size: 268455 Color: 1
Size: 267417 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 464137 Color: 1
Size: 280724 Color: 1
Size: 255140 Color: 0

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 464260 Color: 1
Size: 272885 Color: 1
Size: 262856 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 464393 Color: 1
Size: 281049 Color: 1
Size: 254559 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 464422 Color: 1
Size: 271687 Color: 1
Size: 263892 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 464447 Color: 1
Size: 275279 Color: 1
Size: 260275 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 464525 Color: 1
Size: 281596 Color: 1
Size: 253880 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 464683 Color: 1
Size: 280257 Color: 1
Size: 255061 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 464796 Color: 1
Size: 284938 Color: 1
Size: 250267 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 464970 Color: 1
Size: 270844 Color: 0
Size: 264187 Color: 1

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 465131 Color: 1
Size: 270754 Color: 1
Size: 264116 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 465451 Color: 1
Size: 281075 Color: 1
Size: 253475 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 465595 Color: 1
Size: 283139 Color: 1
Size: 251267 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 465625 Color: 1
Size: 283990 Color: 1
Size: 250386 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 465757 Color: 1
Size: 281764 Color: 1
Size: 252480 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 465781 Color: 1
Size: 281032 Color: 1
Size: 253188 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 465820 Color: 1
Size: 280553 Color: 1
Size: 253628 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 465865 Color: 1
Size: 280577 Color: 1
Size: 253559 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 465904 Color: 1
Size: 271433 Color: 1
Size: 262664 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 466000 Color: 1
Size: 271446 Color: 1
Size: 262555 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 466025 Color: 1
Size: 282851 Color: 1
Size: 251125 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 466048 Color: 1
Size: 273868 Color: 1
Size: 260085 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 466060 Color: 1
Size: 283530 Color: 1
Size: 250411 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 466090 Color: 1
Size: 283339 Color: 1
Size: 250572 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 466195 Color: 1
Size: 275747 Color: 1
Size: 258059 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 466220 Color: 1
Size: 271458 Color: 1
Size: 262323 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 466255 Color: 1
Size: 278873 Color: 1
Size: 254873 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 466361 Color: 1
Size: 273411 Color: 1
Size: 260229 Color: 0

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 466390 Color: 1
Size: 279222 Color: 1
Size: 254389 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 466429 Color: 1
Size: 281647 Color: 1
Size: 251925 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 466475 Color: 1
Size: 272519 Color: 1
Size: 261007 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 466477 Color: 1
Size: 276891 Color: 1
Size: 256633 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 466801 Color: 1
Size: 282925 Color: 1
Size: 250275 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 466834 Color: 1
Size: 273995 Color: 1
Size: 259172 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 466850 Color: 1
Size: 274474 Color: 1
Size: 258677 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 467142 Color: 1
Size: 275013 Color: 1
Size: 257846 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 467144 Color: 1
Size: 276461 Color: 1
Size: 256396 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 467181 Color: 1
Size: 269403 Color: 0
Size: 263417 Color: 1

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 467272 Color: 1
Size: 281969 Color: 1
Size: 250760 Color: 0

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 467281 Color: 1
Size: 267996 Color: 0
Size: 264724 Color: 1

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 467321 Color: 1
Size: 280391 Color: 1
Size: 252289 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 467479 Color: 1
Size: 275924 Color: 1
Size: 256598 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 467546 Color: 1
Size: 270757 Color: 1
Size: 261698 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 467706 Color: 1
Size: 275972 Color: 1
Size: 256323 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 467710 Color: 1
Size: 278360 Color: 1
Size: 253931 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 467945 Color: 1
Size: 281963 Color: 1
Size: 250093 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 467987 Color: 1
Size: 277398 Color: 1
Size: 254616 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 468070 Color: 1
Size: 277424 Color: 1
Size: 254507 Color: 0

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 468078 Color: 1
Size: 279064 Color: 1
Size: 252859 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 468111 Color: 1
Size: 277062 Color: 1
Size: 254828 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 468190 Color: 1
Size: 268700 Color: 1
Size: 263111 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 468219 Color: 1
Size: 271248 Color: 1
Size: 260534 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 468306 Color: 1
Size: 280963 Color: 1
Size: 250732 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 468495 Color: 1
Size: 274057 Color: 1
Size: 257449 Color: 0

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 468738 Color: 1
Size: 278148 Color: 1
Size: 253115 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 468788 Color: 1
Size: 278038 Color: 1
Size: 253175 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 468800 Color: 1
Size: 279609 Color: 1
Size: 251592 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 468807 Color: 1
Size: 269752 Color: 1
Size: 261442 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 468828 Color: 1
Size: 269549 Color: 1
Size: 261624 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 468915 Color: 1
Size: 269559 Color: 1
Size: 261527 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 468929 Color: 1
Size: 268787 Color: 0
Size: 262285 Color: 1

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 468950 Color: 1
Size: 275561 Color: 1
Size: 255490 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 468962 Color: 1
Size: 275089 Color: 1
Size: 255950 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 469101 Color: 1
Size: 271892 Color: 1
Size: 259008 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 469288 Color: 1
Size: 273064 Color: 1
Size: 257649 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 469319 Color: 1
Size: 270133 Color: 1
Size: 260549 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 469346 Color: 1
Size: 273510 Color: 1
Size: 257145 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 469500 Color: 1
Size: 269316 Color: 1
Size: 261185 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 469528 Color: 1
Size: 276810 Color: 1
Size: 253663 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 469545 Color: 1
Size: 268098 Color: 1
Size: 262358 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 469669 Color: 1
Size: 266076 Color: 1
Size: 264256 Color: 0

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 469681 Color: 1
Size: 265739 Color: 1
Size: 264581 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 469736 Color: 1
Size: 271567 Color: 1
Size: 258698 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 469737 Color: 1
Size: 268846 Color: 1
Size: 261418 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 469760 Color: 1
Size: 270709 Color: 1
Size: 259532 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 469909 Color: 1
Size: 266559 Color: 1
Size: 263533 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 469923 Color: 1
Size: 276132 Color: 1
Size: 253946 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 470020 Color: 1
Size: 267415 Color: 1
Size: 262566 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 470254 Color: 1
Size: 270424 Color: 1
Size: 259323 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 470332 Color: 1
Size: 271874 Color: 1
Size: 257795 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 470348 Color: 1
Size: 269711 Color: 1
Size: 259942 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 470349 Color: 1
Size: 275670 Color: 1
Size: 253982 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 470359 Color: 1
Size: 271778 Color: 1
Size: 257864 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 470503 Color: 1
Size: 274791 Color: 1
Size: 254707 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 470552 Color: 1
Size: 273555 Color: 1
Size: 255894 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 470614 Color: 1
Size: 277304 Color: 1
Size: 252083 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 470703 Color: 1
Size: 269475 Color: 1
Size: 259823 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 470826 Color: 1
Size: 269360 Color: 0
Size: 259815 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 470867 Color: 1
Size: 267690 Color: 0
Size: 261444 Color: 1

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 470983 Color: 1
Size: 271694 Color: 1
Size: 257324 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 471188 Color: 1
Size: 270762 Color: 1
Size: 258051 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 471202 Color: 1
Size: 278182 Color: 1
Size: 250617 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 471207 Color: 1
Size: 277015 Color: 1
Size: 251779 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 471313 Color: 1
Size: 277168 Color: 1
Size: 251520 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 471470 Color: 1
Size: 277027 Color: 1
Size: 251504 Color: 0

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 471644 Color: 1
Size: 271147 Color: 1
Size: 257210 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 471662 Color: 1
Size: 270914 Color: 1
Size: 257425 Color: 0

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 471959 Color: 1
Size: 274732 Color: 1
Size: 253310 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 471990 Color: 1
Size: 277102 Color: 1
Size: 250909 Color: 0

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 471991 Color: 1
Size: 275939 Color: 1
Size: 252071 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 472089 Color: 1
Size: 274548 Color: 1
Size: 253364 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 472167 Color: 1
Size: 277716 Color: 1
Size: 250118 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 472214 Color: 1
Size: 269562 Color: 1
Size: 258225 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 472275 Color: 1
Size: 264239 Color: 1
Size: 263487 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 472308 Color: 1
Size: 265291 Color: 0
Size: 262402 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 472349 Color: 1
Size: 270558 Color: 1
Size: 257094 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 472398 Color: 1
Size: 267995 Color: 1
Size: 259608 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 472409 Color: 1
Size: 270635 Color: 1
Size: 256957 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 472423 Color: 1
Size: 268275 Color: 1
Size: 259303 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 472432 Color: 1
Size: 267372 Color: 1
Size: 260197 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 472533 Color: 1
Size: 272988 Color: 1
Size: 254480 Color: 0

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 472620 Color: 1
Size: 276086 Color: 1
Size: 251295 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 472682 Color: 1
Size: 275439 Color: 1
Size: 251880 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 472805 Color: 1
Size: 270003 Color: 1
Size: 257193 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 472841 Color: 1
Size: 273127 Color: 1
Size: 254033 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 472939 Color: 1
Size: 270902 Color: 1
Size: 256160 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 472947 Color: 1
Size: 274575 Color: 1
Size: 252479 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 472955 Color: 1
Size: 273365 Color: 1
Size: 253681 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 472981 Color: 1
Size: 273231 Color: 1
Size: 253789 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 473083 Color: 1
Size: 263946 Color: 1
Size: 262972 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 473606 Color: 1
Size: 272187 Color: 1
Size: 254208 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 473756 Color: 1
Size: 273391 Color: 1
Size: 252854 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 473779 Color: 1
Size: 275246 Color: 1
Size: 250976 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 473845 Color: 1
Size: 268082 Color: 1
Size: 258074 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 473978 Color: 1
Size: 268573 Color: 1
Size: 257450 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 473984 Color: 1
Size: 275224 Color: 1
Size: 250793 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 474075 Color: 1
Size: 270109 Color: 1
Size: 255817 Color: 0

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 474158 Color: 1
Size: 271356 Color: 1
Size: 254487 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 474189 Color: 1
Size: 275652 Color: 1
Size: 250160 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 474219 Color: 1
Size: 262900 Color: 1
Size: 262882 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 474257 Color: 1
Size: 274956 Color: 1
Size: 250788 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 474260 Color: 1
Size: 274323 Color: 1
Size: 251418 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 474355 Color: 1
Size: 272073 Color: 1
Size: 253573 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 474371 Color: 1
Size: 273215 Color: 1
Size: 252415 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 474452 Color: 1
Size: 273876 Color: 1
Size: 251673 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 474492 Color: 1
Size: 270836 Color: 1
Size: 254673 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 474493 Color: 1
Size: 272695 Color: 1
Size: 252813 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 474561 Color: 1
Size: 272994 Color: 1
Size: 252446 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 474593 Color: 1
Size: 273116 Color: 1
Size: 252292 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 474687 Color: 1
Size: 273699 Color: 1
Size: 251615 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 474716 Color: 1
Size: 270687 Color: 1
Size: 254598 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 474845 Color: 1
Size: 263401 Color: 0
Size: 261755 Color: 1

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 475035 Color: 1
Size: 273124 Color: 1
Size: 251842 Color: 0

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 475065 Color: 1
Size: 262913 Color: 1
Size: 262023 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 475129 Color: 1
Size: 262628 Color: 1
Size: 262244 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 475288 Color: 1
Size: 267372 Color: 1
Size: 257341 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 475325 Color: 1
Size: 265452 Color: 0
Size: 259224 Color: 1

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 475414 Color: 1
Size: 270712 Color: 1
Size: 253875 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 475496 Color: 1
Size: 270596 Color: 1
Size: 253909 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 475543 Color: 1
Size: 271239 Color: 1
Size: 253219 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 475543 Color: 1
Size: 264282 Color: 0
Size: 260176 Color: 1

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 475591 Color: 1
Size: 273568 Color: 1
Size: 250842 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 475826 Color: 1
Size: 270439 Color: 1
Size: 253736 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 476070 Color: 1
Size: 273532 Color: 1
Size: 250399 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 476513 Color: 1
Size: 268201 Color: 1
Size: 255287 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 476517 Color: 1
Size: 263785 Color: 1
Size: 259699 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 476547 Color: 1
Size: 272555 Color: 1
Size: 250899 Color: 0

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 476622 Color: 1
Size: 264268 Color: 0
Size: 259111 Color: 1

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 476704 Color: 1
Size: 264885 Color: 1
Size: 258412 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 476759 Color: 1
Size: 267201 Color: 1
Size: 256041 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 476819 Color: 1
Size: 272822 Color: 1
Size: 250360 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 476862 Color: 1
Size: 268653 Color: 1
Size: 254486 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 476929 Color: 1
Size: 271622 Color: 1
Size: 251450 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 476943 Color: 1
Size: 261694 Color: 1
Size: 261364 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 477140 Color: 1
Size: 268722 Color: 1
Size: 254139 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 477142 Color: 1
Size: 261747 Color: 1
Size: 261112 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 477224 Color: 1
Size: 266487 Color: 1
Size: 256290 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 477407 Color: 1
Size: 264421 Color: 1
Size: 258173 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 477436 Color: 1
Size: 267836 Color: 1
Size: 254729 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 477484 Color: 1
Size: 272225 Color: 1
Size: 250292 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 477568 Color: 1
Size: 264586 Color: 1
Size: 257847 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 477579 Color: 1
Size: 267250 Color: 1
Size: 255172 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 477628 Color: 1
Size: 261735 Color: 1
Size: 260638 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 477762 Color: 1
Size: 266634 Color: 1
Size: 255605 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 477805 Color: 1
Size: 263600 Color: 1
Size: 258596 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 477811 Color: 1
Size: 266936 Color: 1
Size: 255254 Color: 0

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 356331 Color: 1
Size: 353797 Color: 1
Size: 289873 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 478058 Color: 1
Size: 268978 Color: 1
Size: 252965 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 478096 Color: 1
Size: 267544 Color: 1
Size: 254361 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 478268 Color: 1
Size: 262488 Color: 0
Size: 259245 Color: 1

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 478276 Color: 1
Size: 270563 Color: 1
Size: 251162 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 478289 Color: 1
Size: 261556 Color: 1
Size: 260156 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 478458 Color: 1
Size: 263146 Color: 1
Size: 258397 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 478577 Color: 1
Size: 267061 Color: 1
Size: 254363 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 478677 Color: 1
Size: 269739 Color: 1
Size: 251585 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 478828 Color: 1
Size: 266923 Color: 1
Size: 254250 Color: 0

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 478864 Color: 1
Size: 261562 Color: 0
Size: 259575 Color: 1

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 447837 Color: 1
Size: 299924 Color: 1
Size: 252240 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 479194 Color: 1
Size: 267650 Color: 1
Size: 253157 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 479209 Color: 1
Size: 264275 Color: 1
Size: 256517 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 479230 Color: 1
Size: 260740 Color: 0
Size: 260031 Color: 1

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 479579 Color: 1
Size: 266509 Color: 1
Size: 253913 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 479658 Color: 1
Size: 261720 Color: 1
Size: 258623 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 479757 Color: 1
Size: 267224 Color: 1
Size: 253020 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 479821 Color: 1
Size: 263050 Color: 1
Size: 257130 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 479921 Color: 1
Size: 260839 Color: 1
Size: 259241 Color: 0

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 480047 Color: 1
Size: 263613 Color: 1
Size: 256341 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 480057 Color: 1
Size: 268471 Color: 1
Size: 251473 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 480072 Color: 1
Size: 266321 Color: 1
Size: 253608 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 480174 Color: 1
Size: 268392 Color: 1
Size: 251435 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 480253 Color: 1
Size: 269514 Color: 1
Size: 250234 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 480276 Color: 1
Size: 267093 Color: 1
Size: 252632 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 480340 Color: 1
Size: 267152 Color: 1
Size: 252509 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 480378 Color: 1
Size: 268465 Color: 1
Size: 251158 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 480448 Color: 1
Size: 266504 Color: 1
Size: 253049 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 480570 Color: 1
Size: 264871 Color: 1
Size: 254560 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 480580 Color: 1
Size: 265266 Color: 1
Size: 254155 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 480598 Color: 1
Size: 267686 Color: 1
Size: 251717 Color: 0

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 480710 Color: 1
Size: 266215 Color: 1
Size: 253076 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 480750 Color: 1
Size: 266332 Color: 1
Size: 252919 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 480942 Color: 1
Size: 267888 Color: 1
Size: 251171 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 480976 Color: 1
Size: 264956 Color: 1
Size: 254069 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 480976 Color: 1
Size: 268461 Color: 1
Size: 250564 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 481056 Color: 1
Size: 264639 Color: 1
Size: 254306 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 481155 Color: 1
Size: 264587 Color: 1
Size: 254259 Color: 0

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 481350 Color: 1
Size: 266710 Color: 1
Size: 251941 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 481356 Color: 1
Size: 261748 Color: 1
Size: 256897 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 481456 Color: 1
Size: 259276 Color: 1
Size: 259269 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 481600 Color: 1
Size: 265348 Color: 1
Size: 253053 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 481797 Color: 1
Size: 265735 Color: 1
Size: 252469 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 481840 Color: 1
Size: 264890 Color: 1
Size: 253271 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 481869 Color: 1
Size: 264894 Color: 1
Size: 253238 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 481878 Color: 1
Size: 263430 Color: 1
Size: 254693 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 481916 Color: 1
Size: 264091 Color: 1
Size: 253994 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 481931 Color: 1
Size: 264430 Color: 1
Size: 253640 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 481944 Color: 1
Size: 265334 Color: 1
Size: 252723 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 482115 Color: 1
Size: 266476 Color: 1
Size: 251410 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 482177 Color: 1
Size: 264579 Color: 1
Size: 253245 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 482248 Color: 1
Size: 265059 Color: 1
Size: 252694 Color: 0

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 482412 Color: 1
Size: 258833 Color: 0
Size: 258756 Color: 1

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 482456 Color: 1
Size: 259954 Color: 1
Size: 257591 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 482505 Color: 1
Size: 263975 Color: 1
Size: 253521 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 482770 Color: 1
Size: 260012 Color: 1
Size: 257219 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 482817 Color: 1
Size: 259747 Color: 1
Size: 257437 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 482856 Color: 1
Size: 264779 Color: 1
Size: 252366 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 482876 Color: 1
Size: 263294 Color: 1
Size: 253831 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 482891 Color: 1
Size: 262429 Color: 1
Size: 254681 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 482893 Color: 1
Size: 262934 Color: 1
Size: 254174 Color: 0

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 483083 Color: 1
Size: 264534 Color: 1
Size: 252384 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 483117 Color: 1
Size: 264737 Color: 1
Size: 252147 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 483195 Color: 1
Size: 262166 Color: 1
Size: 254640 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 483232 Color: 1
Size: 261036 Color: 1
Size: 255733 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 483233 Color: 1
Size: 263713 Color: 1
Size: 253055 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 483467 Color: 1
Size: 264394 Color: 1
Size: 252140 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 483476 Color: 1
Size: 259588 Color: 1
Size: 256937 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 483542 Color: 1
Size: 259206 Color: 0
Size: 257253 Color: 1

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 483629 Color: 1
Size: 258584 Color: 0
Size: 257788 Color: 1

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 483690 Color: 1
Size: 264613 Color: 1
Size: 251698 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 483943 Color: 1
Size: 264448 Color: 1
Size: 251610 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 484039 Color: 1
Size: 262984 Color: 1
Size: 252978 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 484096 Color: 1
Size: 259879 Color: 0
Size: 256026 Color: 1

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 484155 Color: 1
Size: 263879 Color: 1
Size: 251967 Color: 0

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 484214 Color: 1
Size: 260395 Color: 1
Size: 255392 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 484265 Color: 1
Size: 265375 Color: 1
Size: 250361 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 484327 Color: 1
Size: 261894 Color: 0
Size: 253780 Color: 1

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 484371 Color: 1
Size: 259523 Color: 1
Size: 256107 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 484924 Color: 1
Size: 264642 Color: 1
Size: 250435 Color: 0

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 485150 Color: 1
Size: 260839 Color: 0
Size: 254012 Color: 1

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 485184 Color: 1
Size: 257508 Color: 1
Size: 257309 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 485244 Color: 1
Size: 264204 Color: 1
Size: 250553 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 485868 Color: 1
Size: 262236 Color: 1
Size: 251897 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 486017 Color: 1
Size: 262836 Color: 1
Size: 251148 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 486073 Color: 1
Size: 258292 Color: 1
Size: 255636 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 486135 Color: 1
Size: 260855 Color: 1
Size: 253011 Color: 0

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 486346 Color: 1
Size: 261897 Color: 1
Size: 251758 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 486436 Color: 1
Size: 259354 Color: 1
Size: 254211 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 486459 Color: 1
Size: 262097 Color: 1
Size: 251445 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 486573 Color: 1
Size: 262426 Color: 1
Size: 251002 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 486645 Color: 1
Size: 260179 Color: 1
Size: 253177 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 486661 Color: 1
Size: 262806 Color: 1
Size: 250534 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 486680 Color: 1
Size: 262507 Color: 1
Size: 250814 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 486968 Color: 1
Size: 259327 Color: 1
Size: 253706 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 487034 Color: 1
Size: 257923 Color: 1
Size: 255044 Color: 0

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 487067 Color: 1
Size: 261203 Color: 1
Size: 251731 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 487068 Color: 1
Size: 261444 Color: 1
Size: 251489 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 487127 Color: 1
Size: 258424 Color: 1
Size: 254450 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 487181 Color: 1
Size: 256525 Color: 1
Size: 256295 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 487316 Color: 1
Size: 256589 Color: 1
Size: 256096 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 487427 Color: 1
Size: 259719 Color: 1
Size: 252855 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 487595 Color: 1
Size: 260697 Color: 1
Size: 251709 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 487772 Color: 1
Size: 262002 Color: 1
Size: 250227 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 487803 Color: 1
Size: 259077 Color: 1
Size: 253121 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 487810 Color: 1
Size: 261656 Color: 1
Size: 250535 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 487913 Color: 1
Size: 256356 Color: 0
Size: 255732 Color: 1

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 487994 Color: 1
Size: 259378 Color: 1
Size: 252629 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 488068 Color: 1
Size: 256094 Color: 1
Size: 255839 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 488101 Color: 1
Size: 260563 Color: 1
Size: 251337 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 488298 Color: 1
Size: 257567 Color: 1
Size: 254136 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 488312 Color: 1
Size: 258572 Color: 1
Size: 253117 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 488351 Color: 1
Size: 259277 Color: 1
Size: 252373 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 488515 Color: 1
Size: 260364 Color: 1
Size: 251122 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 488522 Color: 1
Size: 257932 Color: 1
Size: 253547 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 488946 Color: 1
Size: 257281 Color: 1
Size: 253774 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 488957 Color: 1
Size: 260231 Color: 1
Size: 250813 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 488962 Color: 1
Size: 258153 Color: 0
Size: 252886 Color: 1

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 489047 Color: 1
Size: 259299 Color: 1
Size: 251655 Color: 0

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 489073 Color: 1
Size: 259753 Color: 1
Size: 251175 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 489236 Color: 1
Size: 257427 Color: 1
Size: 253338 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 489363 Color: 1
Size: 259383 Color: 1
Size: 251255 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 489418 Color: 1
Size: 259113 Color: 1
Size: 251470 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 489599 Color: 1
Size: 257615 Color: 1
Size: 252787 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 489635 Color: 1
Size: 259892 Color: 1
Size: 250474 Color: 0

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 489897 Color: 1
Size: 259883 Color: 1
Size: 250221 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 490181 Color: 1
Size: 258128 Color: 1
Size: 251692 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 490206 Color: 1
Size: 259690 Color: 1
Size: 250105 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 490299 Color: 1
Size: 256550 Color: 1
Size: 253152 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 490311 Color: 1
Size: 257114 Color: 1
Size: 252576 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 490344 Color: 1
Size: 256158 Color: 1
Size: 253499 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 490520 Color: 1
Size: 258239 Color: 1
Size: 251242 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 490527 Color: 1
Size: 256089 Color: 1
Size: 253385 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 490561 Color: 1
Size: 256306 Color: 1
Size: 253134 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 490663 Color: 1
Size: 259204 Color: 1
Size: 250134 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 490681 Color: 1
Size: 255405 Color: 1
Size: 253915 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 490697 Color: 1
Size: 258916 Color: 1
Size: 250388 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 490790 Color: 1
Size: 259139 Color: 1
Size: 250072 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 490882 Color: 1
Size: 258294 Color: 1
Size: 250825 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 490981 Color: 1
Size: 255273 Color: 0
Size: 253747 Color: 1

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 491064 Color: 1
Size: 256450 Color: 1
Size: 252487 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 491148 Color: 1
Size: 257385 Color: 1
Size: 251468 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 491180 Color: 1
Size: 255089 Color: 1
Size: 253732 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 491282 Color: 1
Size: 257214 Color: 1
Size: 251505 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 491375 Color: 1
Size: 256000 Color: 1
Size: 252626 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 491379 Color: 1
Size: 255983 Color: 1
Size: 252639 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 491436 Color: 1
Size: 255270 Color: 1
Size: 253295 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 491495 Color: 1
Size: 257403 Color: 1
Size: 251103 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 491585 Color: 1
Size: 256368 Color: 1
Size: 252048 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 491887 Color: 1
Size: 254341 Color: 1
Size: 253773 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 492092 Color: 1
Size: 254986 Color: 0
Size: 252923 Color: 1

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 492407 Color: 1
Size: 254623 Color: 1
Size: 252971 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 492513 Color: 1
Size: 254957 Color: 1
Size: 252531 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 492564 Color: 1
Size: 255876 Color: 1
Size: 251561 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 492610 Color: 1
Size: 255267 Color: 1
Size: 252124 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 492622 Color: 1
Size: 254886 Color: 1
Size: 252493 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 492684 Color: 1
Size: 256284 Color: 1
Size: 251033 Color: 0

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 492698 Color: 1
Size: 255221 Color: 1
Size: 252082 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 492705 Color: 1
Size: 255126 Color: 1
Size: 252170 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 492818 Color: 1
Size: 256524 Color: 1
Size: 250659 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 492866 Color: 1
Size: 254223 Color: 1
Size: 252912 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 492876 Color: 1
Size: 256679 Color: 1
Size: 250446 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 493270 Color: 1
Size: 253981 Color: 1
Size: 252750 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 493439 Color: 1
Size: 253802 Color: 1
Size: 252760 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 493444 Color: 1
Size: 255173 Color: 1
Size: 251384 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 493490 Color: 1
Size: 254038 Color: 1
Size: 252473 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 493523 Color: 1
Size: 254459 Color: 1
Size: 252019 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 493591 Color: 1
Size: 254181 Color: 1
Size: 252229 Color: 0

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 494008 Color: 1
Size: 255858 Color: 1
Size: 250135 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 494181 Color: 1
Size: 254595 Color: 1
Size: 251225 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 494970 Color: 1
Size: 254662 Color: 1
Size: 250369 Color: 0

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 495244 Color: 1
Size: 253159 Color: 1
Size: 251598 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 495519 Color: 1
Size: 253930 Color: 1
Size: 250552 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 495589 Color: 1
Size: 253209 Color: 1
Size: 251203 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 495699 Color: 1
Size: 253461 Color: 1
Size: 250841 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 495910 Color: 1
Size: 254064 Color: 1
Size: 250027 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 495936 Color: 1
Size: 252688 Color: 1
Size: 251377 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 462984 Color: 1
Size: 269526 Color: 1
Size: 267491 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 496317 Color: 1
Size: 252961 Color: 1
Size: 250723 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 496465 Color: 1
Size: 252623 Color: 1
Size: 250913 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 496529 Color: 1
Size: 252255 Color: 1
Size: 251217 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 496533 Color: 1
Size: 252928 Color: 1
Size: 250540 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 496595 Color: 1
Size: 252802 Color: 1
Size: 250604 Color: 0

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 496602 Color: 1
Size: 252269 Color: 1
Size: 251130 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 496615 Color: 1
Size: 252490 Color: 1
Size: 250896 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 466368 Color: 1
Size: 278289 Color: 1
Size: 255344 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 497026 Color: 1
Size: 251820 Color: 1
Size: 251155 Color: 0

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 497262 Color: 1
Size: 252235 Color: 1
Size: 250504 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 497271 Color: 1
Size: 251443 Color: 1
Size: 251287 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 494368 Color: 1
Size: 255621 Color: 0
Size: 250012 Color: 1

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 497458 Color: 1
Size: 251310 Color: 1
Size: 251233 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 497573 Color: 1
Size: 252377 Color: 1
Size: 250051 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 497799 Color: 1
Size: 252145 Color: 1
Size: 250057 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 497860 Color: 1
Size: 252118 Color: 1
Size: 250023 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 497871 Color: 1
Size: 251456 Color: 1
Size: 250674 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 498077 Color: 1
Size: 251878 Color: 1
Size: 250046 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 498082 Color: 1
Size: 251505 Color: 1
Size: 250414 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 498136 Color: 1
Size: 251526 Color: 1
Size: 250339 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 498201 Color: 1
Size: 251377 Color: 1
Size: 250423 Color: 0

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 498215 Color: 1
Size: 251780 Color: 1
Size: 250006 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 498245 Color: 1
Size: 251602 Color: 1
Size: 250154 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 498519 Color: 1
Size: 251289 Color: 1
Size: 250193 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 498592 Color: 1
Size: 251071 Color: 1
Size: 250338 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 498612 Color: 1
Size: 251225 Color: 1
Size: 250164 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 498689 Color: 1
Size: 251229 Color: 1
Size: 250083 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 498774 Color: 1
Size: 251023 Color: 1
Size: 250204 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 498851 Color: 1
Size: 250941 Color: 1
Size: 250209 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 498972 Color: 1
Size: 250771 Color: 1
Size: 250258 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 499004 Color: 1
Size: 250624 Color: 1
Size: 250373 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 499116 Color: 1
Size: 250699 Color: 1
Size: 250186 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 499208 Color: 1
Size: 250726 Color: 1
Size: 250067 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 499436 Color: 1
Size: 250376 Color: 1
Size: 250189 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 499696 Color: 1
Size: 250299 Color: 1
Size: 250006 Color: 0

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 499747 Color: 1
Size: 250163 Color: 1
Size: 250091 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 499834 Color: 1
Size: 250096 Color: 1
Size: 250071 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 498413 Color: 1
Size: 251212 Color: 1
Size: 250376 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 499989 Color: 1
Size: 250007 Color: 1
Size: 250005 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 499997 Color: 1
Size: 250004 Color: 1
Size: 250000 Color: 0

Bin 2953: 1 of cap free
Amount of items: 3
Items: 
Size: 432619 Color: 1
Size: 300322 Color: 1
Size: 267059 Color: 0

Bin 2954: 1 of cap free
Amount of items: 3
Items: 
Size: 405134 Color: 1
Size: 341408 Color: 1
Size: 253458 Color: 0

Bin 2955: 1 of cap free
Amount of items: 3
Items: 
Size: 420144 Color: 1
Size: 305516 Color: 1
Size: 274340 Color: 0

Bin 2956: 1 of cap free
Amount of items: 3
Items: 
Size: 385235 Color: 1
Size: 350916 Color: 1
Size: 263849 Color: 0

Bin 2957: 1 of cap free
Amount of items: 3
Items: 
Size: 414984 Color: 1
Size: 326760 Color: 1
Size: 258256 Color: 0

Bin 2958: 1 of cap free
Amount of items: 3
Items: 
Size: 371140 Color: 1
Size: 359971 Color: 1
Size: 268889 Color: 0

Bin 2959: 1 of cap free
Amount of items: 3
Items: 
Size: 442070 Color: 1
Size: 295088 Color: 1
Size: 262842 Color: 0

Bin 2960: 1 of cap free
Amount of items: 3
Items: 
Size: 412186 Color: 1
Size: 334241 Color: 1
Size: 253573 Color: 0

Bin 2961: 1 of cap free
Amount of items: 3
Items: 
Size: 363086 Color: 1
Size: 336193 Color: 1
Size: 300721 Color: 0

Bin 2962: 1 of cap free
Amount of items: 3
Items: 
Size: 394086 Color: 1
Size: 355913 Color: 1
Size: 250001 Color: 0

Bin 2963: 1 of cap free
Amount of items: 3
Items: 
Size: 351156 Color: 1
Size: 348659 Color: 1
Size: 300185 Color: 0

Bin 2964: 1 of cap free
Amount of items: 3
Items: 
Size: 361140 Color: 1
Size: 350977 Color: 1
Size: 287883 Color: 0

Bin 2965: 1 of cap free
Amount of items: 3
Items: 
Size: 392356 Color: 1
Size: 312948 Color: 1
Size: 294696 Color: 0

Bin 2966: 1 of cap free
Amount of items: 3
Items: 
Size: 374072 Color: 1
Size: 327107 Color: 0
Size: 298821 Color: 1

Bin 2967: 1 of cap free
Amount of items: 3
Items: 
Size: 345324 Color: 1
Size: 344884 Color: 1
Size: 309792 Color: 0

Bin 2968: 1 of cap free
Amount of items: 3
Items: 
Size: 388902 Color: 1
Size: 353237 Color: 1
Size: 257861 Color: 0

Bin 2969: 1 of cap free
Amount of items: 3
Items: 
Size: 417841 Color: 1
Size: 303689 Color: 1
Size: 278470 Color: 0

Bin 2970: 1 of cap free
Amount of items: 3
Items: 
Size: 359987 Color: 1
Size: 337771 Color: 1
Size: 302242 Color: 0

Bin 2971: 1 of cap free
Amount of items: 3
Items: 
Size: 359016 Color: 1
Size: 356274 Color: 1
Size: 284710 Color: 0

Bin 2972: 1 of cap free
Amount of items: 3
Items: 
Size: 446948 Color: 1
Size: 289854 Color: 1
Size: 263198 Color: 0

Bin 2973: 1 of cap free
Amount of items: 3
Items: 
Size: 425719 Color: 1
Size: 296171 Color: 1
Size: 278110 Color: 0

Bin 2974: 1 of cap free
Amount of items: 3
Items: 
Size: 373478 Color: 1
Size: 366920 Color: 1
Size: 259602 Color: 0

Bin 2975: 1 of cap free
Amount of items: 3
Items: 
Size: 386909 Color: 1
Size: 361673 Color: 1
Size: 251418 Color: 0

Bin 2976: 1 of cap free
Amount of items: 3
Items: 
Size: 358440 Color: 1
Size: 324214 Color: 0
Size: 317346 Color: 1

Bin 2977: 1 of cap free
Amount of items: 3
Items: 
Size: 360249 Color: 1
Size: 360232 Color: 1
Size: 279519 Color: 0

Bin 2978: 1 of cap free
Amount of items: 3
Items: 
Size: 371615 Color: 1
Size: 347281 Color: 1
Size: 281104 Color: 0

Bin 2979: 1 of cap free
Amount of items: 3
Items: 
Size: 357990 Color: 1
Size: 355095 Color: 1
Size: 286915 Color: 0

Bin 2980: 1 of cap free
Amount of items: 3
Items: 
Size: 385464 Color: 1
Size: 357458 Color: 1
Size: 257078 Color: 0

Bin 2981: 1 of cap free
Amount of items: 3
Items: 
Size: 364881 Color: 1
Size: 332949 Color: 1
Size: 302170 Color: 0

Bin 2982: 1 of cap free
Amount of items: 3
Items: 
Size: 345543 Color: 1
Size: 344706 Color: 1
Size: 309751 Color: 0

Bin 2983: 1 of cap free
Amount of items: 3
Items: 
Size: 364398 Color: 1
Size: 353317 Color: 1
Size: 282285 Color: 0

Bin 2984: 1 of cap free
Amount of items: 3
Items: 
Size: 433313 Color: 1
Size: 284544 Color: 1
Size: 282143 Color: 0

Bin 2985: 1 of cap free
Amount of items: 3
Items: 
Size: 347309 Color: 1
Size: 341760 Color: 1
Size: 310931 Color: 0

Bin 2986: 1 of cap free
Amount of items: 3
Items: 
Size: 354996 Color: 1
Size: 341502 Color: 1
Size: 303502 Color: 0

Bin 2987: 1 of cap free
Amount of items: 3
Items: 
Size: 365264 Color: 1
Size: 357872 Color: 1
Size: 276864 Color: 0

Bin 2988: 1 of cap free
Amount of items: 3
Items: 
Size: 494044 Color: 1
Size: 254759 Color: 1
Size: 251197 Color: 0

Bin 2989: 1 of cap free
Amount of items: 3
Items: 
Size: 367409 Color: 1
Size: 365874 Color: 1
Size: 266717 Color: 0

Bin 2990: 1 of cap free
Amount of items: 3
Items: 
Size: 381133 Color: 1
Size: 344708 Color: 1
Size: 274159 Color: 0

Bin 2991: 1 of cap free
Amount of items: 3
Items: 
Size: 366710 Color: 1
Size: 355302 Color: 1
Size: 277988 Color: 0

Bin 2992: 1 of cap free
Amount of items: 3
Items: 
Size: 347738 Color: 1
Size: 346392 Color: 1
Size: 305870 Color: 0

Bin 2993: 1 of cap free
Amount of items: 3
Items: 
Size: 348931 Color: 1
Size: 345693 Color: 1
Size: 305376 Color: 0

Bin 2994: 1 of cap free
Amount of items: 3
Items: 
Size: 360976 Color: 1
Size: 341293 Color: 1
Size: 297731 Color: 0

Bin 2995: 1 of cap free
Amount of items: 3
Items: 
Size: 361435 Color: 1
Size: 320342 Color: 1
Size: 318223 Color: 0

Bin 2996: 1 of cap free
Amount of items: 3
Items: 
Size: 362180 Color: 1
Size: 326613 Color: 0
Size: 311207 Color: 1

Bin 2997: 1 of cap free
Amount of items: 3
Items: 
Size: 362665 Color: 1
Size: 331376 Color: 1
Size: 305959 Color: 0

Bin 2998: 1 of cap free
Amount of items: 3
Items: 
Size: 363020 Color: 1
Size: 337904 Color: 1
Size: 299076 Color: 0

Bin 2999: 1 of cap free
Amount of items: 3
Items: 
Size: 363137 Color: 1
Size: 339410 Color: 1
Size: 297453 Color: 0

Bin 3000: 1 of cap free
Amount of items: 3
Items: 
Size: 363889 Color: 1
Size: 343333 Color: 1
Size: 292778 Color: 0

Bin 3001: 1 of cap free
Amount of items: 3
Items: 
Size: 364024 Color: 1
Size: 332093 Color: 1
Size: 303883 Color: 0

Bin 3002: 1 of cap free
Amount of items: 3
Items: 
Size: 364346 Color: 1
Size: 342434 Color: 1
Size: 293220 Color: 0

Bin 3003: 1 of cap free
Amount of items: 3
Items: 
Size: 365003 Color: 1
Size: 338758 Color: 1
Size: 296239 Color: 0

Bin 3004: 1 of cap free
Amount of items: 3
Items: 
Size: 365563 Color: 1
Size: 356977 Color: 1
Size: 277460 Color: 0

Bin 3005: 1 of cap free
Amount of items: 3
Items: 
Size: 377278 Color: 1
Size: 353438 Color: 1
Size: 269284 Color: 0

Bin 3006: 1 of cap free
Amount of items: 3
Items: 
Size: 366567 Color: 1
Size: 336764 Color: 1
Size: 296669 Color: 0

Bin 3007: 1 of cap free
Amount of items: 3
Items: 
Size: 367375 Color: 1
Size: 334887 Color: 1
Size: 297738 Color: 0

Bin 3008: 1 of cap free
Amount of items: 3
Items: 
Size: 369617 Color: 1
Size: 339016 Color: 1
Size: 291367 Color: 0

Bin 3009: 1 of cap free
Amount of items: 3
Items: 
Size: 370159 Color: 1
Size: 343740 Color: 1
Size: 286101 Color: 0

Bin 3010: 1 of cap free
Amount of items: 3
Items: 
Size: 492510 Color: 1
Size: 256067 Color: 1
Size: 251423 Color: 0

Bin 3011: 1 of cap free
Amount of items: 3
Items: 
Size: 496267 Color: 1
Size: 253550 Color: 1
Size: 250183 Color: 0

Bin 3012: 1 of cap free
Amount of items: 3
Items: 
Size: 497060 Color: 1
Size: 251485 Color: 1
Size: 251455 Color: 0

Bin 3013: 1 of cap free
Amount of items: 3
Items: 
Size: 497635 Color: 1
Size: 251489 Color: 0
Size: 250876 Color: 1

Bin 3014: 1 of cap free
Amount of items: 3
Items: 
Size: 355327 Color: 1
Size: 340258 Color: 1
Size: 304415 Color: 0

Bin 3015: 1 of cap free
Amount of items: 3
Items: 
Size: 366351 Color: 1
Size: 319497 Color: 1
Size: 314152 Color: 0

Bin 3016: 1 of cap free
Amount of items: 3
Items: 
Size: 363992 Color: 1
Size: 320844 Color: 0
Size: 315164 Color: 1

Bin 3017: 1 of cap free
Amount of items: 3
Items: 
Size: 350890 Color: 1
Size: 343631 Color: 1
Size: 305479 Color: 0

Bin 3018: 1 of cap free
Amount of items: 3
Items: 
Size: 393506 Color: 1
Size: 306135 Color: 1
Size: 300359 Color: 0

Bin 3019: 1 of cap free
Amount of items: 3
Items: 
Size: 360441 Color: 1
Size: 333492 Color: 1
Size: 306067 Color: 0

Bin 3020: 1 of cap free
Amount of items: 3
Items: 
Size: 396423 Color: 1
Size: 342776 Color: 1
Size: 260801 Color: 0

Bin 3021: 1 of cap free
Amount of items: 3
Items: 
Size: 357330 Color: 1
Size: 343281 Color: 1
Size: 299389 Color: 0

Bin 3022: 1 of cap free
Amount of items: 3
Items: 
Size: 358554 Color: 1
Size: 321528 Color: 1
Size: 319918 Color: 0

Bin 3023: 1 of cap free
Amount of items: 3
Items: 
Size: 358863 Color: 1
Size: 323957 Color: 0
Size: 317180 Color: 1

Bin 3024: 1 of cap free
Amount of items: 3
Items: 
Size: 395024 Color: 1
Size: 312531 Color: 0
Size: 292445 Color: 1

Bin 3025: 1 of cap free
Amount of items: 3
Items: 
Size: 357589 Color: 1
Size: 344543 Color: 1
Size: 297868 Color: 0

Bin 3026: 1 of cap free
Amount of items: 3
Items: 
Size: 369139 Color: 1
Size: 359739 Color: 1
Size: 271122 Color: 0

Bin 3027: 1 of cap free
Amount of items: 3
Items: 
Size: 356506 Color: 1
Size: 344898 Color: 1
Size: 298596 Color: 0

Bin 3028: 1 of cap free
Amount of items: 3
Items: 
Size: 358022 Color: 1
Size: 329608 Color: 0
Size: 312370 Color: 1

Bin 3029: 1 of cap free
Amount of items: 3
Items: 
Size: 351831 Color: 1
Size: 350039 Color: 1
Size: 298130 Color: 0

Bin 3030: 1 of cap free
Amount of items: 3
Items: 
Size: 358762 Color: 1
Size: 332408 Color: 1
Size: 308830 Color: 0

Bin 3031: 1 of cap free
Amount of items: 3
Items: 
Size: 349175 Color: 1
Size: 344086 Color: 1
Size: 306739 Color: 0

Bin 3032: 1 of cap free
Amount of items: 3
Items: 
Size: 357380 Color: 1
Size: 339802 Color: 1
Size: 302818 Color: 0

Bin 3033: 1 of cap free
Amount of items: 3
Items: 
Size: 357653 Color: 1
Size: 322695 Color: 0
Size: 319652 Color: 1

Bin 3034: 1 of cap free
Amount of items: 3
Items: 
Size: 350746 Color: 1
Size: 350687 Color: 1
Size: 298567 Color: 0

Bin 3035: 1 of cap free
Amount of items: 3
Items: 
Size: 445894 Color: 1
Size: 287460 Color: 1
Size: 266646 Color: 0

Bin 3036: 1 of cap free
Amount of items: 3
Items: 
Size: 356785 Color: 1
Size: 348399 Color: 1
Size: 294816 Color: 0

Bin 3037: 1 of cap free
Amount of items: 3
Items: 
Size: 382238 Color: 1
Size: 328335 Color: 0
Size: 289427 Color: 1

Bin 3038: 1 of cap free
Amount of items: 3
Items: 
Size: 407621 Color: 1
Size: 321309 Color: 1
Size: 271070 Color: 0

Bin 3039: 1 of cap free
Amount of items: 3
Items: 
Size: 354856 Color: 1
Size: 343657 Color: 1
Size: 301487 Color: 0

Bin 3040: 1 of cap free
Amount of items: 3
Items: 
Size: 356236 Color: 1
Size: 342080 Color: 1
Size: 301684 Color: 0

Bin 3041: 2 of cap free
Amount of items: 3
Items: 
Size: 459273 Color: 1
Size: 289537 Color: 1
Size: 251189 Color: 0

Bin 3042: 2 of cap free
Amount of items: 3
Items: 
Size: 407959 Color: 1
Size: 301231 Color: 1
Size: 290809 Color: 0

Bin 3043: 2 of cap free
Amount of items: 3
Items: 
Size: 361077 Color: 1
Size: 352744 Color: 1
Size: 286178 Color: 0

Bin 3044: 2 of cap free
Amount of items: 3
Items: 
Size: 391419 Color: 1
Size: 314217 Color: 1
Size: 294363 Color: 0

Bin 3045: 2 of cap free
Amount of items: 3
Items: 
Size: 361545 Color: 1
Size: 338028 Color: 1
Size: 300426 Color: 0

Bin 3046: 2 of cap free
Amount of items: 3
Items: 
Size: 399805 Color: 1
Size: 302111 Color: 0
Size: 298083 Color: 1

Bin 3047: 2 of cap free
Amount of items: 3
Items: 
Size: 400939 Color: 1
Size: 331562 Color: 1
Size: 267498 Color: 0

Bin 3048: 2 of cap free
Amount of items: 3
Items: 
Size: 418537 Color: 1
Size: 309250 Color: 1
Size: 272212 Color: 0

Bin 3049: 2 of cap free
Amount of items: 3
Items: 
Size: 360816 Color: 1
Size: 332800 Color: 1
Size: 306383 Color: 0

Bin 3050: 2 of cap free
Amount of items: 3
Items: 
Size: 355980 Color: 1
Size: 354523 Color: 1
Size: 289496 Color: 0

Bin 3051: 2 of cap free
Amount of items: 3
Items: 
Size: 344520 Color: 1
Size: 335124 Color: 1
Size: 320355 Color: 0

Bin 3052: 2 of cap free
Amount of items: 3
Items: 
Size: 382579 Color: 1
Size: 355386 Color: 1
Size: 262034 Color: 0

Bin 3053: 2 of cap free
Amount of items: 3
Items: 
Size: 363203 Color: 1
Size: 319280 Color: 1
Size: 317516 Color: 0

Bin 3054: 2 of cap free
Amount of items: 3
Items: 
Size: 398857 Color: 1
Size: 331295 Color: 1
Size: 269847 Color: 0

Bin 3055: 2 of cap free
Amount of items: 3
Items: 
Size: 359150 Color: 1
Size: 340675 Color: 1
Size: 300174 Color: 0

Bin 3056: 2 of cap free
Amount of items: 3
Items: 
Size: 360201 Color: 1
Size: 348589 Color: 1
Size: 291209 Color: 0

Bin 3057: 2 of cap free
Amount of items: 3
Items: 
Size: 354871 Color: 1
Size: 324452 Color: 1
Size: 320676 Color: 0

Bin 3058: 2 of cap free
Amount of items: 3
Items: 
Size: 416502 Color: 1
Size: 324329 Color: 1
Size: 259168 Color: 0

Bin 3059: 2 of cap free
Amount of items: 3
Items: 
Size: 351933 Color: 1
Size: 342097 Color: 1
Size: 305969 Color: 0

Bin 3060: 2 of cap free
Amount of items: 3
Items: 
Size: 384873 Color: 1
Size: 312018 Color: 0
Size: 303108 Color: 1

Bin 3061: 2 of cap free
Amount of items: 3
Items: 
Size: 362409 Color: 1
Size: 355020 Color: 1
Size: 282570 Color: 0

Bin 3062: 2 of cap free
Amount of items: 3
Items: 
Size: 359683 Color: 1
Size: 346037 Color: 1
Size: 294279 Color: 0

Bin 3063: 2 of cap free
Amount of items: 3
Items: 
Size: 360986 Color: 1
Size: 341169 Color: 1
Size: 297844 Color: 0

Bin 3064: 2 of cap free
Amount of items: 3
Items: 
Size: 364790 Color: 1
Size: 343739 Color: 1
Size: 291470 Color: 0

Bin 3065: 2 of cap free
Amount of items: 3
Items: 
Size: 343095 Color: 1
Size: 342072 Color: 1
Size: 314832 Color: 0

Bin 3066: 2 of cap free
Amount of items: 3
Items: 
Size: 361495 Color: 1
Size: 337950 Color: 1
Size: 300554 Color: 0

Bin 3067: 2 of cap free
Amount of items: 3
Items: 
Size: 359394 Color: 1
Size: 356248 Color: 1
Size: 284357 Color: 0

Bin 3068: 2 of cap free
Amount of items: 3
Items: 
Size: 360095 Color: 1
Size: 329409 Color: 1
Size: 310495 Color: 0

Bin 3069: 2 of cap free
Amount of items: 3
Items: 
Size: 410739 Color: 1
Size: 322021 Color: 0
Size: 267239 Color: 1

Bin 3070: 2 of cap free
Amount of items: 3
Items: 
Size: 357327 Color: 1
Size: 348336 Color: 1
Size: 294336 Color: 0

Bin 3071: 2 of cap free
Amount of items: 3
Items: 
Size: 359256 Color: 1
Size: 346249 Color: 1
Size: 294494 Color: 0

Bin 3072: 2 of cap free
Amount of items: 3
Items: 
Size: 353611 Color: 1
Size: 345981 Color: 1
Size: 300407 Color: 0

Bin 3073: 2 of cap free
Amount of items: 3
Items: 
Size: 372250 Color: 1
Size: 353008 Color: 1
Size: 274741 Color: 0

Bin 3074: 2 of cap free
Amount of items: 3
Items: 
Size: 368051 Color: 1
Size: 352783 Color: 1
Size: 279165 Color: 0

Bin 3075: 2 of cap free
Amount of items: 3
Items: 
Size: 373164 Color: 1
Size: 326680 Color: 1
Size: 300155 Color: 0

Bin 3076: 2 of cap free
Amount of items: 3
Items: 
Size: 483066 Color: 1
Size: 261457 Color: 1
Size: 255476 Color: 0

Bin 3077: 3 of cap free
Amount of items: 3
Items: 
Size: 419551 Color: 1
Size: 306886 Color: 1
Size: 273561 Color: 0

Bin 3078: 3 of cap free
Amount of items: 3
Items: 
Size: 387231 Color: 1
Size: 325534 Color: 1
Size: 287233 Color: 0

Bin 3079: 3 of cap free
Amount of items: 3
Items: 
Size: 402057 Color: 1
Size: 306757 Color: 0
Size: 291184 Color: 1

Bin 3080: 3 of cap free
Amount of items: 3
Items: 
Size: 383449 Color: 1
Size: 330386 Color: 1
Size: 286163 Color: 0

Bin 3081: 3 of cap free
Amount of items: 3
Items: 
Size: 386042 Color: 1
Size: 347441 Color: 1
Size: 266515 Color: 0

Bin 3082: 3 of cap free
Amount of items: 3
Items: 
Size: 395646 Color: 1
Size: 350529 Color: 1
Size: 253823 Color: 0

Bin 3083: 3 of cap free
Amount of items: 3
Items: 
Size: 368800 Color: 1
Size: 324162 Color: 0
Size: 307036 Color: 1

Bin 3084: 3 of cap free
Amount of items: 3
Items: 
Size: 353799 Color: 1
Size: 339211 Color: 1
Size: 306988 Color: 0

Bin 3085: 3 of cap free
Amount of items: 3
Items: 
Size: 395455 Color: 1
Size: 306009 Color: 0
Size: 298534 Color: 1

Bin 3086: 3 of cap free
Amount of items: 3
Items: 
Size: 499231 Color: 1
Size: 250722 Color: 1
Size: 250045 Color: 0

Bin 3087: 3 of cap free
Amount of items: 3
Items: 
Size: 405661 Color: 1
Size: 321692 Color: 0
Size: 272645 Color: 1

Bin 3088: 3 of cap free
Amount of items: 3
Items: 
Size: 378549 Color: 1
Size: 312260 Color: 0
Size: 309189 Color: 1

Bin 3089: 3 of cap free
Amount of items: 3
Items: 
Size: 380625 Color: 1
Size: 359325 Color: 1
Size: 260048 Color: 0

Bin 3090: 3 of cap free
Amount of items: 3
Items: 
Size: 359088 Color: 1
Size: 323774 Color: 1
Size: 317136 Color: 0

Bin 3091: 3 of cap free
Amount of items: 3
Items: 
Size: 353232 Color: 1
Size: 324514 Color: 1
Size: 322252 Color: 0

Bin 3092: 3 of cap free
Amount of items: 3
Items: 
Size: 374214 Color: 1
Size: 332805 Color: 1
Size: 292979 Color: 0

Bin 3093: 3 of cap free
Amount of items: 3
Items: 
Size: 378901 Color: 1
Size: 367863 Color: 1
Size: 253234 Color: 0

Bin 3094: 3 of cap free
Amount of items: 3
Items: 
Size: 356438 Color: 1
Size: 325811 Color: 0
Size: 317749 Color: 1

Bin 3095: 3 of cap free
Amount of items: 3
Items: 
Size: 363129 Color: 1
Size: 347156 Color: 1
Size: 289713 Color: 0

Bin 3096: 3 of cap free
Amount of items: 3
Items: 
Size: 367331 Color: 1
Size: 355850 Color: 1
Size: 276817 Color: 0

Bin 3097: 3 of cap free
Amount of items: 3
Items: 
Size: 359227 Color: 1
Size: 321055 Color: 0
Size: 319716 Color: 1

Bin 3098: 3 of cap free
Amount of items: 3
Items: 
Size: 363684 Color: 1
Size: 327139 Color: 0
Size: 309175 Color: 1

Bin 3099: 3 of cap free
Amount of items: 3
Items: 
Size: 364224 Color: 1
Size: 361223 Color: 1
Size: 274551 Color: 0

Bin 3100: 3 of cap free
Amount of items: 3
Items: 
Size: 356235 Color: 1
Size: 347641 Color: 1
Size: 296122 Color: 0

Bin 3101: 3 of cap free
Amount of items: 3
Items: 
Size: 360163 Color: 1
Size: 357752 Color: 1
Size: 282083 Color: 0

Bin 3102: 3 of cap free
Amount of items: 3
Items: 
Size: 394115 Color: 1
Size: 337050 Color: 1
Size: 268833 Color: 0

Bin 3103: 3 of cap free
Amount of items: 3
Items: 
Size: 361866 Color: 1
Size: 357985 Color: 1
Size: 280147 Color: 0

Bin 3104: 4 of cap free
Amount of items: 3
Items: 
Size: 397539 Color: 1
Size: 345272 Color: 1
Size: 257186 Color: 0

Bin 3105: 4 of cap free
Amount of items: 3
Items: 
Size: 414163 Color: 1
Size: 298257 Color: 0
Size: 287577 Color: 1

Bin 3106: 4 of cap free
Amount of items: 3
Items: 
Size: 388213 Color: 1
Size: 347571 Color: 1
Size: 264213 Color: 0

Bin 3107: 4 of cap free
Amount of items: 3
Items: 
Size: 335955 Color: 1
Size: 334254 Color: 1
Size: 329788 Color: 0

Bin 3108: 4 of cap free
Amount of items: 3
Items: 
Size: 443752 Color: 1
Size: 287837 Color: 1
Size: 268408 Color: 0

Bin 3109: 4 of cap free
Amount of items: 3
Items: 
Size: 373550 Color: 1
Size: 319895 Color: 1
Size: 306552 Color: 0

Bin 3110: 4 of cap free
Amount of items: 3
Items: 
Size: 442050 Color: 1
Size: 284163 Color: 1
Size: 273784 Color: 0

Bin 3111: 4 of cap free
Amount of items: 3
Items: 
Size: 405844 Color: 1
Size: 316199 Color: 1
Size: 277954 Color: 0

Bin 3112: 4 of cap free
Amount of items: 3
Items: 
Size: 362907 Color: 1
Size: 324122 Color: 1
Size: 312968 Color: 0

Bin 3113: 4 of cap free
Amount of items: 3
Items: 
Size: 384404 Color: 1
Size: 328773 Color: 1
Size: 286820 Color: 0

Bin 3114: 4 of cap free
Amount of items: 3
Items: 
Size: 368663 Color: 1
Size: 347204 Color: 1
Size: 284130 Color: 0

Bin 3115: 4 of cap free
Amount of items: 3
Items: 
Size: 362266 Color: 1
Size: 340613 Color: 1
Size: 297118 Color: 0

Bin 3116: 4 of cap free
Amount of items: 3
Items: 
Size: 357621 Color: 1
Size: 344154 Color: 1
Size: 298222 Color: 0

Bin 3117: 4 of cap free
Amount of items: 3
Items: 
Size: 370137 Color: 1
Size: 357249 Color: 1
Size: 272611 Color: 0

Bin 3118: 4 of cap free
Amount of items: 3
Items: 
Size: 381630 Color: 1
Size: 319091 Color: 0
Size: 299276 Color: 1

Bin 3119: 4 of cap free
Amount of items: 3
Items: 
Size: 360979 Color: 1
Size: 340356 Color: 1
Size: 298662 Color: 0

Bin 3120: 4 of cap free
Amount of items: 3
Items: 
Size: 354248 Color: 1
Size: 350868 Color: 1
Size: 294881 Color: 0

Bin 3121: 4 of cap free
Amount of items: 3
Items: 
Size: 363316 Color: 1
Size: 326490 Color: 1
Size: 310191 Color: 0

Bin 3122: 4 of cap free
Amount of items: 3
Items: 
Size: 359155 Color: 1
Size: 327234 Color: 1
Size: 313608 Color: 0

Bin 3123: 4 of cap free
Amount of items: 3
Items: 
Size: 366868 Color: 1
Size: 322692 Color: 1
Size: 310437 Color: 0

Bin 3124: 4 of cap free
Amount of items: 3
Items: 
Size: 386656 Color: 1
Size: 346771 Color: 1
Size: 266570 Color: 0

Bin 3125: 5 of cap free
Amount of items: 3
Items: 
Size: 378224 Color: 1
Size: 362865 Color: 1
Size: 258907 Color: 0

Bin 3126: 5 of cap free
Amount of items: 3
Items: 
Size: 342475 Color: 1
Size: 334472 Color: 1
Size: 323049 Color: 0

Bin 3127: 5 of cap free
Amount of items: 3
Items: 
Size: 409939 Color: 1
Size: 322825 Color: 1
Size: 267232 Color: 0

Bin 3128: 5 of cap free
Amount of items: 3
Items: 
Size: 361538 Color: 1
Size: 324604 Color: 0
Size: 313854 Color: 1

Bin 3129: 5 of cap free
Amount of items: 3
Items: 
Size: 378240 Color: 1
Size: 354067 Color: 1
Size: 267689 Color: 0

Bin 3130: 5 of cap free
Amount of items: 3
Items: 
Size: 357484 Color: 1
Size: 341630 Color: 1
Size: 300882 Color: 0

Bin 3131: 5 of cap free
Amount of items: 3
Items: 
Size: 448238 Color: 1
Size: 277624 Color: 0
Size: 274134 Color: 1

Bin 3132: 5 of cap free
Amount of items: 3
Items: 
Size: 356055 Color: 1
Size: 349731 Color: 1
Size: 294210 Color: 0

Bin 3133: 5 of cap free
Amount of items: 3
Items: 
Size: 365034 Color: 1
Size: 361290 Color: 1
Size: 273672 Color: 0

Bin 3134: 5 of cap free
Amount of items: 3
Items: 
Size: 352398 Color: 1
Size: 342365 Color: 1
Size: 305233 Color: 0

Bin 3135: 5 of cap free
Amount of items: 3
Items: 
Size: 358826 Color: 1
Size: 357529 Color: 1
Size: 283641 Color: 0

Bin 3136: 5 of cap free
Amount of items: 3
Items: 
Size: 377108 Color: 1
Size: 333563 Color: 1
Size: 289325 Color: 0

Bin 3137: 6 of cap free
Amount of items: 3
Items: 
Size: 385242 Color: 1
Size: 329062 Color: 1
Size: 285691 Color: 0

Bin 3138: 6 of cap free
Amount of items: 3
Items: 
Size: 419215 Color: 1
Size: 326879 Color: 1
Size: 253901 Color: 0

Bin 3139: 6 of cap free
Amount of items: 3
Items: 
Size: 389948 Color: 1
Size: 324332 Color: 1
Size: 285715 Color: 0

Bin 3140: 6 of cap free
Amount of items: 3
Items: 
Size: 449646 Color: 1
Size: 284583 Color: 1
Size: 265766 Color: 0

Bin 3141: 6 of cap free
Amount of items: 3
Items: 
Size: 348827 Color: 1
Size: 341024 Color: 1
Size: 310144 Color: 0

Bin 3142: 6 of cap free
Amount of items: 3
Items: 
Size: 357134 Color: 1
Size: 356796 Color: 1
Size: 286065 Color: 0

Bin 3143: 6 of cap free
Amount of items: 3
Items: 
Size: 362898 Color: 1
Size: 357517 Color: 1
Size: 279580 Color: 0

Bin 3144: 6 of cap free
Amount of items: 3
Items: 
Size: 370270 Color: 1
Size: 336767 Color: 1
Size: 292958 Color: 0

Bin 3145: 6 of cap free
Amount of items: 3
Items: 
Size: 391068 Color: 1
Size: 322185 Color: 1
Size: 286742 Color: 0

Bin 3146: 6 of cap free
Amount of items: 3
Items: 
Size: 428465 Color: 1
Size: 298101 Color: 1
Size: 273429 Color: 0

Bin 3147: 6 of cap free
Amount of items: 3
Items: 
Size: 361117 Color: 1
Size: 339076 Color: 1
Size: 299802 Color: 0

Bin 3148: 6 of cap free
Amount of items: 3
Items: 
Size: 355669 Color: 1
Size: 327316 Color: 1
Size: 317010 Color: 0

Bin 3149: 6 of cap free
Amount of items: 3
Items: 
Size: 352494 Color: 1
Size: 340806 Color: 1
Size: 306695 Color: 0

Bin 3150: 6 of cap free
Amount of items: 3
Items: 
Size: 364376 Color: 1
Size: 342526 Color: 1
Size: 293093 Color: 0

Bin 3151: 6 of cap free
Amount of items: 3
Items: 
Size: 355624 Color: 1
Size: 353091 Color: 1
Size: 291280 Color: 0

Bin 3152: 7 of cap free
Amount of items: 3
Items: 
Size: 346043 Color: 1
Size: 341250 Color: 1
Size: 312701 Color: 0

Bin 3153: 7 of cap free
Amount of items: 3
Items: 
Size: 374990 Color: 1
Size: 342822 Color: 1
Size: 282182 Color: 0

Bin 3154: 7 of cap free
Amount of items: 3
Items: 
Size: 376077 Color: 1
Size: 342165 Color: 1
Size: 281752 Color: 0

Bin 3155: 7 of cap free
Amount of items: 3
Items: 
Size: 397895 Color: 1
Size: 330942 Color: 0
Size: 271157 Color: 1

Bin 3156: 7 of cap free
Amount of items: 3
Items: 
Size: 403047 Color: 1
Size: 315671 Color: 0
Size: 281276 Color: 1

Bin 3157: 7 of cap free
Amount of items: 3
Items: 
Size: 349647 Color: 1
Size: 348948 Color: 1
Size: 301399 Color: 0

Bin 3158: 7 of cap free
Amount of items: 3
Items: 
Size: 382118 Color: 1
Size: 341211 Color: 1
Size: 276665 Color: 0

Bin 3159: 8 of cap free
Amount of items: 3
Items: 
Size: 380063 Color: 1
Size: 341721 Color: 1
Size: 278209 Color: 0

Bin 3160: 8 of cap free
Amount of items: 3
Items: 
Size: 466118 Color: 1
Size: 275000 Color: 1
Size: 258875 Color: 0

Bin 3161: 8 of cap free
Amount of items: 3
Items: 
Size: 401513 Color: 1
Size: 333430 Color: 1
Size: 265050 Color: 0

Bin 3162: 8 of cap free
Amount of items: 3
Items: 
Size: 353671 Color: 1
Size: 341747 Color: 1
Size: 304575 Color: 0

Bin 3163: 8 of cap free
Amount of items: 3
Items: 
Size: 362608 Color: 1
Size: 344452 Color: 1
Size: 292933 Color: 0

Bin 3164: 8 of cap free
Amount of items: 3
Items: 
Size: 401042 Color: 1
Size: 309345 Color: 1
Size: 289606 Color: 0

Bin 3165: 8 of cap free
Amount of items: 3
Items: 
Size: 412322 Color: 1
Size: 295059 Color: 1
Size: 292612 Color: 0

Bin 3166: 8 of cap free
Amount of items: 3
Items: 
Size: 399033 Color: 1
Size: 321746 Color: 0
Size: 279214 Color: 1

Bin 3167: 8 of cap free
Amount of items: 3
Items: 
Size: 349686 Color: 1
Size: 349601 Color: 1
Size: 300706 Color: 0

Bin 3168: 9 of cap free
Amount of items: 3
Items: 
Size: 393342 Color: 1
Size: 316824 Color: 1
Size: 289826 Color: 0

Bin 3169: 9 of cap free
Amount of items: 3
Items: 
Size: 398708 Color: 1
Size: 334160 Color: 1
Size: 267124 Color: 0

Bin 3170: 9 of cap free
Amount of items: 3
Items: 
Size: 350214 Color: 1
Size: 330456 Color: 0
Size: 319322 Color: 1

Bin 3171: 9 of cap free
Amount of items: 3
Items: 
Size: 360008 Color: 1
Size: 331745 Color: 1
Size: 308239 Color: 0

Bin 3172: 9 of cap free
Amount of items: 3
Items: 
Size: 395379 Color: 1
Size: 352572 Color: 1
Size: 252041 Color: 0

Bin 3173: 10 of cap free
Amount of items: 3
Items: 
Size: 362017 Color: 1
Size: 345453 Color: 1
Size: 292521 Color: 0

Bin 3174: 10 of cap free
Amount of items: 3
Items: 
Size: 391987 Color: 1
Size: 325458 Color: 1
Size: 282546 Color: 0

Bin 3175: 10 of cap free
Amount of items: 3
Items: 
Size: 362957 Color: 1
Size: 356718 Color: 1
Size: 280316 Color: 0

Bin 3176: 10 of cap free
Amount of items: 3
Items: 
Size: 392459 Color: 1
Size: 351720 Color: 1
Size: 255812 Color: 0

Bin 3177: 10 of cap free
Amount of items: 3
Items: 
Size: 466935 Color: 1
Size: 279000 Color: 1
Size: 254056 Color: 0

Bin 3178: 10 of cap free
Amount of items: 3
Items: 
Size: 357190 Color: 1
Size: 340530 Color: 1
Size: 302271 Color: 0

Bin 3179: 10 of cap free
Amount of items: 3
Items: 
Size: 360108 Color: 1
Size: 343198 Color: 1
Size: 296685 Color: 0

Bin 3180: 10 of cap free
Amount of items: 3
Items: 
Size: 439683 Color: 1
Size: 299515 Color: 1
Size: 260793 Color: 0

Bin 3181: 11 of cap free
Amount of items: 3
Items: 
Size: 397243 Color: 1
Size: 325472 Color: 1
Size: 277275 Color: 0

Bin 3182: 11 of cap free
Amount of items: 3
Items: 
Size: 376296 Color: 1
Size: 352261 Color: 1
Size: 271433 Color: 0

Bin 3183: 11 of cap free
Amount of items: 3
Items: 
Size: 372318 Color: 1
Size: 343788 Color: 1
Size: 283884 Color: 0

Bin 3184: 11 of cap free
Amount of items: 3
Items: 
Size: 376747 Color: 1
Size: 357297 Color: 1
Size: 265946 Color: 0

Bin 3185: 11 of cap free
Amount of items: 3
Items: 
Size: 433185 Color: 1
Size: 290362 Color: 1
Size: 276443 Color: 0

Bin 3186: 11 of cap free
Amount of items: 3
Items: 
Size: 397812 Color: 1
Size: 317252 Color: 1
Size: 284926 Color: 0

Bin 3187: 11 of cap free
Amount of items: 3
Items: 
Size: 409028 Color: 1
Size: 316400 Color: 1
Size: 274562 Color: 0

Bin 3188: 11 of cap free
Amount of items: 3
Items: 
Size: 345381 Color: 1
Size: 328812 Color: 0
Size: 325797 Color: 1

Bin 3189: 11 of cap free
Amount of items: 3
Items: 
Size: 364635 Color: 1
Size: 351351 Color: 1
Size: 284004 Color: 0

Bin 3190: 11 of cap free
Amount of items: 3
Items: 
Size: 351778 Color: 1
Size: 331291 Color: 1
Size: 316921 Color: 0

Bin 3191: 11 of cap free
Amount of items: 3
Items: 
Size: 351822 Color: 1
Size: 347701 Color: 1
Size: 300467 Color: 0

Bin 3192: 11 of cap free
Amount of items: 3
Items: 
Size: 355154 Color: 1
Size: 353151 Color: 1
Size: 291685 Color: 0

Bin 3193: 12 of cap free
Amount of items: 3
Items: 
Size: 360027 Color: 1
Size: 346222 Color: 1
Size: 293740 Color: 0

Bin 3194: 12 of cap free
Amount of items: 3
Items: 
Size: 373293 Color: 1
Size: 325694 Color: 0
Size: 301002 Color: 1

Bin 3195: 12 of cap free
Amount of items: 3
Items: 
Size: 359661 Color: 1
Size: 322274 Color: 1
Size: 318054 Color: 0

Bin 3196: 12 of cap free
Amount of items: 3
Items: 
Size: 372694 Color: 1
Size: 350651 Color: 1
Size: 276644 Color: 0

Bin 3197: 12 of cap free
Amount of items: 3
Items: 
Size: 361737 Color: 1
Size: 347737 Color: 1
Size: 290515 Color: 0

Bin 3198: 13 of cap free
Amount of items: 3
Items: 
Size: 399930 Color: 1
Size: 334000 Color: 1
Size: 266058 Color: 0

Bin 3199: 13 of cap free
Amount of items: 3
Items: 
Size: 360934 Color: 1
Size: 354661 Color: 1
Size: 284393 Color: 0

Bin 3200: 13 of cap free
Amount of items: 3
Items: 
Size: 391769 Color: 1
Size: 346135 Color: 1
Size: 262084 Color: 0

Bin 3201: 13 of cap free
Amount of items: 3
Items: 
Size: 369482 Color: 1
Size: 326851 Color: 1
Size: 303655 Color: 0

Bin 3202: 13 of cap free
Amount of items: 3
Items: 
Size: 359992 Color: 1
Size: 349526 Color: 1
Size: 290470 Color: 0

Bin 3203: 13 of cap free
Amount of items: 3
Items: 
Size: 375435 Color: 1
Size: 358437 Color: 1
Size: 266116 Color: 0

Bin 3204: 13 of cap free
Amount of items: 3
Items: 
Size: 396368 Color: 1
Size: 343500 Color: 1
Size: 260120 Color: 0

Bin 3205: 13 of cap free
Amount of items: 3
Items: 
Size: 393979 Color: 1
Size: 303072 Color: 1
Size: 302937 Color: 0

Bin 3206: 14 of cap free
Amount of items: 3
Items: 
Size: 468902 Color: 1
Size: 274197 Color: 1
Size: 256888 Color: 0

Bin 3207: 14 of cap free
Amount of items: 3
Items: 
Size: 423908 Color: 1
Size: 302412 Color: 1
Size: 273667 Color: 0

Bin 3208: 14 of cap free
Amount of items: 3
Items: 
Size: 417703 Color: 1
Size: 293745 Color: 1
Size: 288539 Color: 0

Bin 3209: 14 of cap free
Amount of items: 3
Items: 
Size: 371518 Color: 1
Size: 321033 Color: 0
Size: 307436 Color: 1

Bin 3210: 14 of cap free
Amount of items: 3
Items: 
Size: 388829 Color: 1
Size: 357745 Color: 1
Size: 253413 Color: 0

Bin 3211: 14 of cap free
Amount of items: 3
Items: 
Size: 401984 Color: 1
Size: 319491 Color: 1
Size: 278512 Color: 0

Bin 3212: 15 of cap free
Amount of items: 3
Items: 
Size: 409799 Color: 1
Size: 315402 Color: 1
Size: 274785 Color: 0

Bin 3213: 15 of cap free
Amount of items: 3
Items: 
Size: 415523 Color: 1
Size: 307556 Color: 0
Size: 276907 Color: 1

Bin 3214: 15 of cap free
Amount of items: 3
Items: 
Size: 392237 Color: 1
Size: 327113 Color: 1
Size: 280636 Color: 0

Bin 3215: 15 of cap free
Amount of items: 3
Items: 
Size: 377618 Color: 1
Size: 356738 Color: 1
Size: 265630 Color: 0

Bin 3216: 15 of cap free
Amount of items: 3
Items: 
Size: 350937 Color: 1
Size: 337991 Color: 1
Size: 311058 Color: 0

Bin 3217: 16 of cap free
Amount of items: 3
Items: 
Size: 369685 Color: 1
Size: 338252 Color: 1
Size: 292048 Color: 0

Bin 3218: 16 of cap free
Amount of items: 3
Items: 
Size: 369498 Color: 1
Size: 358179 Color: 1
Size: 272308 Color: 0

Bin 3219: 17 of cap free
Amount of items: 3
Items: 
Size: 393081 Color: 1
Size: 309106 Color: 1
Size: 297797 Color: 0

Bin 3220: 18 of cap free
Amount of items: 3
Items: 
Size: 386850 Color: 1
Size: 349562 Color: 1
Size: 263571 Color: 0

Bin 3221: 18 of cap free
Amount of items: 3
Items: 
Size: 367654 Color: 1
Size: 356062 Color: 1
Size: 276267 Color: 0

Bin 3222: 19 of cap free
Amount of items: 3
Items: 
Size: 376890 Color: 1
Size: 318722 Color: 0
Size: 304370 Color: 1

Bin 3223: 20 of cap free
Amount of items: 3
Items: 
Size: 375796 Color: 1
Size: 344502 Color: 1
Size: 279683 Color: 0

Bin 3224: 20 of cap free
Amount of items: 3
Items: 
Size: 412898 Color: 1
Size: 308212 Color: 0
Size: 278871 Color: 1

Bin 3225: 20 of cap free
Amount of items: 3
Items: 
Size: 393535 Color: 1
Size: 320390 Color: 0
Size: 286056 Color: 1

Bin 3226: 20 of cap free
Amount of items: 3
Items: 
Size: 413046 Color: 1
Size: 317503 Color: 1
Size: 269432 Color: 0

Bin 3227: 21 of cap free
Amount of items: 3
Items: 
Size: 454630 Color: 1
Size: 281510 Color: 1
Size: 263840 Color: 0

Bin 3228: 21 of cap free
Amount of items: 3
Items: 
Size: 403695 Color: 1
Size: 309639 Color: 0
Size: 286646 Color: 1

Bin 3229: 21 of cap free
Amount of items: 3
Items: 
Size: 384719 Color: 1
Size: 347754 Color: 1
Size: 267507 Color: 0

Bin 3230: 22 of cap free
Amount of items: 3
Items: 
Size: 420537 Color: 1
Size: 312022 Color: 1
Size: 267420 Color: 0

Bin 3231: 23 of cap free
Amount of items: 3
Items: 
Size: 347590 Color: 1
Size: 329806 Color: 0
Size: 322582 Color: 1

Bin 3232: 23 of cap free
Amount of items: 3
Items: 
Size: 401072 Color: 1
Size: 341412 Color: 1
Size: 257494 Color: 0

Bin 3233: 23 of cap free
Amount of items: 3
Items: 
Size: 361828 Color: 1
Size: 335030 Color: 1
Size: 303120 Color: 0

Bin 3234: 24 of cap free
Amount of items: 3
Items: 
Size: 378969 Color: 1
Size: 354111 Color: 1
Size: 266897 Color: 0

Bin 3235: 24 of cap free
Amount of items: 3
Items: 
Size: 378737 Color: 1
Size: 353661 Color: 1
Size: 267579 Color: 0

Bin 3236: 24 of cap free
Amount of items: 3
Items: 
Size: 391575 Color: 1
Size: 310503 Color: 1
Size: 297899 Color: 0

Bin 3237: 24 of cap free
Amount of items: 3
Items: 
Size: 406886 Color: 1
Size: 339065 Color: 1
Size: 254026 Color: 0

Bin 3238: 24 of cap free
Amount of items: 3
Items: 
Size: 467914 Color: 1
Size: 273919 Color: 1
Size: 258144 Color: 0

Bin 3239: 24 of cap free
Amount of items: 3
Items: 
Size: 358824 Color: 1
Size: 348575 Color: 1
Size: 292578 Color: 0

Bin 3240: 26 of cap free
Amount of items: 3
Items: 
Size: 411672 Color: 1
Size: 314908 Color: 1
Size: 273395 Color: 0

Bin 3241: 27 of cap free
Amount of items: 3
Items: 
Size: 380438 Color: 1
Size: 356784 Color: 1
Size: 262752 Color: 0

Bin 3242: 28 of cap free
Amount of items: 3
Items: 
Size: 413794 Color: 1
Size: 327385 Color: 1
Size: 258794 Color: 0

Bin 3243: 29 of cap free
Amount of items: 3
Items: 
Size: 385907 Color: 1
Size: 314913 Color: 0
Size: 299152 Color: 1

Bin 3244: 29 of cap free
Amount of items: 3
Items: 
Size: 442750 Color: 1
Size: 280560 Color: 1
Size: 276662 Color: 0

Bin 3245: 30 of cap free
Amount of items: 3
Items: 
Size: 442958 Color: 1
Size: 300901 Color: 1
Size: 256112 Color: 0

Bin 3246: 30 of cap free
Amount of items: 3
Items: 
Size: 421249 Color: 1
Size: 317184 Color: 1
Size: 261538 Color: 0

Bin 3247: 31 of cap free
Amount of items: 3
Items: 
Size: 454336 Color: 1
Size: 288532 Color: 1
Size: 257102 Color: 0

Bin 3248: 31 of cap free
Amount of items: 3
Items: 
Size: 420606 Color: 1
Size: 291271 Color: 1
Size: 288093 Color: 0

Bin 3249: 31 of cap free
Amount of items: 3
Items: 
Size: 359571 Color: 1
Size: 322898 Color: 1
Size: 317501 Color: 0

Bin 3250: 32 of cap free
Amount of items: 3
Items: 
Size: 361565 Color: 1
Size: 345541 Color: 1
Size: 292863 Color: 0

Bin 3251: 32 of cap free
Amount of items: 3
Items: 
Size: 378008 Color: 1
Size: 318846 Color: 1
Size: 303115 Color: 0

Bin 3252: 33 of cap free
Amount of items: 3
Items: 
Size: 362084 Color: 1
Size: 324706 Color: 0
Size: 313178 Color: 1

Bin 3253: 33 of cap free
Amount of items: 3
Items: 
Size: 391140 Color: 1
Size: 324272 Color: 0
Size: 284556 Color: 1

Bin 3254: 34 of cap free
Amount of items: 3
Items: 
Size: 403852 Color: 1
Size: 316863 Color: 1
Size: 279252 Color: 0

Bin 3255: 34 of cap free
Amount of items: 3
Items: 
Size: 407717 Color: 1
Size: 310387 Color: 1
Size: 281863 Color: 0

Bin 3256: 34 of cap free
Amount of items: 3
Items: 
Size: 441148 Color: 1
Size: 298978 Color: 1
Size: 259841 Color: 0

Bin 3257: 34 of cap free
Amount of items: 3
Items: 
Size: 406605 Color: 1
Size: 340923 Color: 1
Size: 252439 Color: 0

Bin 3258: 35 of cap free
Amount of items: 3
Items: 
Size: 446898 Color: 1
Size: 277393 Color: 1
Size: 275675 Color: 0

Bin 3259: 35 of cap free
Amount of items: 3
Items: 
Size: 370192 Color: 1
Size: 336055 Color: 1
Size: 293719 Color: 0

Bin 3260: 35 of cap free
Amount of items: 3
Items: 
Size: 464065 Color: 1
Size: 277533 Color: 1
Size: 258368 Color: 0

Bin 3261: 36 of cap free
Amount of items: 3
Items: 
Size: 393645 Color: 1
Size: 342075 Color: 1
Size: 264245 Color: 0

Bin 3262: 36 of cap free
Amount of items: 3
Items: 
Size: 366877 Color: 1
Size: 326521 Color: 0
Size: 306567 Color: 1

Bin 3263: 37 of cap free
Amount of items: 3
Items: 
Size: 364425 Color: 1
Size: 345869 Color: 1
Size: 289670 Color: 0

Bin 3264: 37 of cap free
Amount of items: 3
Items: 
Size: 391860 Color: 1
Size: 325975 Color: 1
Size: 282129 Color: 0

Bin 3265: 39 of cap free
Amount of items: 3
Items: 
Size: 434588 Color: 1
Size: 289821 Color: 1
Size: 275553 Color: 0

Bin 3266: 41 of cap free
Amount of items: 3
Items: 
Size: 401255 Color: 1
Size: 330991 Color: 1
Size: 267714 Color: 0

Bin 3267: 41 of cap free
Amount of items: 3
Items: 
Size: 450599 Color: 1
Size: 293253 Color: 1
Size: 256108 Color: 0

Bin 3268: 41 of cap free
Amount of items: 3
Items: 
Size: 381897 Color: 1
Size: 322059 Color: 1
Size: 296004 Color: 0

Bin 3269: 42 of cap free
Amount of items: 3
Items: 
Size: 372716 Color: 1
Size: 344313 Color: 1
Size: 282930 Color: 0

Bin 3270: 44 of cap free
Amount of items: 3
Items: 
Size: 383391 Color: 1
Size: 353544 Color: 1
Size: 263022 Color: 0

Bin 3271: 46 of cap free
Amount of items: 3
Items: 
Size: 358082 Color: 1
Size: 341196 Color: 1
Size: 300677 Color: 0

Bin 3272: 46 of cap free
Amount of items: 3
Items: 
Size: 350326 Color: 1
Size: 335185 Color: 1
Size: 314444 Color: 0

Bin 3273: 49 of cap free
Amount of items: 3
Items: 
Size: 358889 Color: 1
Size: 352970 Color: 1
Size: 288093 Color: 0

Bin 3274: 50 of cap free
Amount of items: 3
Items: 
Size: 427113 Color: 1
Size: 317864 Color: 1
Size: 254974 Color: 0

Bin 3275: 56 of cap free
Amount of items: 3
Items: 
Size: 410128 Color: 1
Size: 314611 Color: 1
Size: 275206 Color: 0

Bin 3276: 56 of cap free
Amount of items: 3
Items: 
Size: 421753 Color: 1
Size: 307433 Color: 1
Size: 270759 Color: 0

Bin 3277: 56 of cap free
Amount of items: 3
Items: 
Size: 342776 Color: 1
Size: 336242 Color: 1
Size: 320927 Color: 0

Bin 3278: 57 of cap free
Amount of items: 3
Items: 
Size: 426295 Color: 1
Size: 289324 Color: 1
Size: 284325 Color: 0

Bin 3279: 57 of cap free
Amount of items: 3
Items: 
Size: 359074 Color: 1
Size: 338996 Color: 1
Size: 301874 Color: 0

Bin 3280: 58 of cap free
Amount of items: 3
Items: 
Size: 386583 Color: 1
Size: 362802 Color: 1
Size: 250558 Color: 0

Bin 3281: 58 of cap free
Amount of items: 3
Items: 
Size: 475582 Color: 1
Size: 264919 Color: 0
Size: 259442 Color: 1

Bin 3282: 60 of cap free
Amount of items: 3
Items: 
Size: 408765 Color: 1
Size: 327973 Color: 1
Size: 263203 Color: 0

Bin 3283: 60 of cap free
Amount of items: 3
Items: 
Size: 424502 Color: 1
Size: 321573 Color: 1
Size: 253866 Color: 0

Bin 3284: 62 of cap free
Amount of items: 3
Items: 
Size: 434224 Color: 1
Size: 297840 Color: 1
Size: 267875 Color: 0

Bin 3285: 62 of cap free
Amount of items: 3
Items: 
Size: 382074 Color: 1
Size: 350761 Color: 1
Size: 267104 Color: 0

Bin 3286: 62 of cap free
Amount of items: 3
Items: 
Size: 477979 Color: 1
Size: 268497 Color: 1
Size: 253463 Color: 0

Bin 3287: 66 of cap free
Amount of items: 3
Items: 
Size: 487563 Color: 1
Size: 260810 Color: 1
Size: 251562 Color: 0

Bin 3288: 70 of cap free
Amount of items: 3
Items: 
Size: 360478 Color: 1
Size: 344878 Color: 1
Size: 294575 Color: 0

Bin 3289: 70 of cap free
Amount of items: 3
Items: 
Size: 376374 Color: 1
Size: 313837 Color: 0
Size: 309720 Color: 1

Bin 3290: 71 of cap free
Amount of items: 3
Items: 
Size: 368928 Color: 1
Size: 359976 Color: 1
Size: 271026 Color: 0

Bin 3291: 72 of cap free
Amount of items: 3
Items: 
Size: 412761 Color: 1
Size: 300280 Color: 0
Size: 286888 Color: 1

Bin 3292: 75 of cap free
Amount of items: 3
Items: 
Size: 366767 Color: 1
Size: 346134 Color: 1
Size: 287025 Color: 0

Bin 3293: 77 of cap free
Amount of items: 3
Items: 
Size: 391558 Color: 1
Size: 318427 Color: 1
Size: 289939 Color: 0

Bin 3294: 80 of cap free
Amount of items: 3
Items: 
Size: 410904 Color: 1
Size: 299722 Color: 1
Size: 289295 Color: 0

Bin 3295: 81 of cap free
Amount of items: 3
Items: 
Size: 445914 Color: 1
Size: 278374 Color: 0
Size: 275632 Color: 1

Bin 3296: 86 of cap free
Amount of items: 3
Items: 
Size: 407517 Color: 1
Size: 299307 Color: 0
Size: 293091 Color: 1

Bin 3297: 88 of cap free
Amount of items: 3
Items: 
Size: 436725 Color: 1
Size: 290800 Color: 1
Size: 272388 Color: 0

Bin 3298: 89 of cap free
Amount of items: 3
Items: 
Size: 365332 Color: 1
Size: 329841 Color: 1
Size: 304739 Color: 0

Bin 3299: 90 of cap free
Amount of items: 3
Items: 
Size: 445510 Color: 1
Size: 296913 Color: 1
Size: 257488 Color: 0

Bin 3300: 101 of cap free
Amount of items: 3
Items: 
Size: 370784 Color: 1
Size: 321130 Color: 0
Size: 307986 Color: 1

Bin 3301: 101 of cap free
Amount of items: 3
Items: 
Size: 348899 Color: 1
Size: 335083 Color: 1
Size: 315918 Color: 0

Bin 3302: 108 of cap free
Amount of items: 3
Items: 
Size: 383345 Color: 1
Size: 350861 Color: 1
Size: 265687 Color: 0

Bin 3303: 123 of cap free
Amount of items: 3
Items: 
Size: 354702 Color: 1
Size: 329090 Color: 1
Size: 316086 Color: 0

Bin 3304: 124 of cap free
Amount of items: 3
Items: 
Size: 479091 Color: 1
Size: 263306 Color: 0
Size: 257480 Color: 1

Bin 3305: 132 of cap free
Amount of items: 3
Items: 
Size: 406885 Color: 1
Size: 322727 Color: 1
Size: 270257 Color: 0

Bin 3306: 132 of cap free
Amount of items: 3
Items: 
Size: 459146 Color: 1
Size: 288802 Color: 1
Size: 251921 Color: 0

Bin 3307: 136 of cap free
Amount of items: 3
Items: 
Size: 399467 Color: 1
Size: 322330 Color: 1
Size: 278068 Color: 0

Bin 3308: 138 of cap free
Amount of items: 3
Items: 
Size: 425545 Color: 1
Size: 312307 Color: 1
Size: 262011 Color: 0

Bin 3309: 140 of cap free
Amount of items: 3
Items: 
Size: 350540 Color: 1
Size: 340066 Color: 1
Size: 309255 Color: 0

Bin 3310: 151 of cap free
Amount of items: 3
Items: 
Size: 460808 Color: 1
Size: 277901 Color: 1
Size: 261141 Color: 0

Bin 3311: 153 of cap free
Amount of items: 3
Items: 
Size: 360596 Color: 1
Size: 355323 Color: 1
Size: 283929 Color: 0

Bin 3312: 153 of cap free
Amount of items: 3
Items: 
Size: 358403 Color: 1
Size: 348521 Color: 1
Size: 292924 Color: 0

Bin 3313: 157 of cap free
Amount of items: 3
Items: 
Size: 394594 Color: 1
Size: 333125 Color: 1
Size: 272125 Color: 0

Bin 3314: 163 of cap free
Amount of items: 3
Items: 
Size: 369882 Color: 1
Size: 332565 Color: 1
Size: 297391 Color: 0

Bin 3315: 173 of cap free
Amount of items: 3
Items: 
Size: 397452 Color: 1
Size: 304019 Color: 1
Size: 298357 Color: 0

Bin 3316: 184 of cap free
Amount of items: 3
Items: 
Size: 362380 Color: 1
Size: 351869 Color: 1
Size: 285568 Color: 0

Bin 3317: 207 of cap free
Amount of items: 3
Items: 
Size: 445079 Color: 1
Size: 301475 Color: 1
Size: 253240 Color: 0

Bin 3318: 231 of cap free
Amount of items: 3
Items: 
Size: 421580 Color: 1
Size: 309569 Color: 1
Size: 268621 Color: 0

Bin 3319: 282 of cap free
Amount of items: 3
Items: 
Size: 381604 Color: 1
Size: 318312 Color: 0
Size: 299803 Color: 1

Bin 3320: 321 of cap free
Amount of items: 3
Items: 
Size: 443951 Color: 1
Size: 283830 Color: 0
Size: 271899 Color: 1

Bin 3321: 333 of cap free
Amount of items: 3
Items: 
Size: 429620 Color: 1
Size: 289884 Color: 1
Size: 280164 Color: 0

Bin 3322: 336 of cap free
Amount of items: 3
Items: 
Size: 438519 Color: 1
Size: 295878 Color: 1
Size: 265268 Color: 0

Bin 3323: 432 of cap free
Amount of items: 3
Items: 
Size: 382176 Color: 1
Size: 344109 Color: 1
Size: 273284 Color: 0

Bin 3324: 464 of cap free
Amount of items: 3
Items: 
Size: 417900 Color: 1
Size: 319183 Color: 1
Size: 262454 Color: 0

Bin 3325: 482 of cap free
Amount of items: 3
Items: 
Size: 365134 Color: 1
Size: 324055 Color: 1
Size: 310330 Color: 0

Bin 3326: 526 of cap free
Amount of items: 3
Items: 
Size: 475291 Color: 1
Size: 273005 Color: 1
Size: 251179 Color: 0

Bin 3327: 688 of cap free
Amount of items: 3
Items: 
Size: 355814 Color: 1
Size: 346495 Color: 1
Size: 297004 Color: 0

Bin 3328: 968 of cap free
Amount of items: 3
Items: 
Size: 395520 Color: 1
Size: 318179 Color: 0
Size: 285334 Color: 1

Bin 3329: 1039 of cap free
Amount of items: 3
Items: 
Size: 347715 Color: 1
Size: 335885 Color: 1
Size: 315362 Color: 0

Bin 3330: 4369 of cap free
Amount of items: 3
Items: 
Size: 379204 Color: 1
Size: 354444 Color: 1
Size: 261984 Color: 0

Bin 3331: 4541 of cap free
Amount of items: 3
Items: 
Size: 369730 Color: 1
Size: 368505 Color: 1
Size: 257225 Color: 0

Bin 3332: 56483 of cap free
Amount of items: 3
Items: 
Size: 335847 Color: 1
Size: 307424 Color: 1
Size: 300247 Color: 0

Bin 3333: 170259 of cap free
Amount of items: 3
Items: 
Size: 293249 Color: 1
Size: 269586 Color: 0
Size: 266907 Color: 1

Bin 3334: 247891 of cap free
Amount of items: 2
Items: 
Size: 499986 Color: 1
Size: 252124 Color: 0

Bin 3335: 503155 of cap free
Amount of items: 1
Items: 
Size: 496846 Color: 1

Total size: 3334003334
Total free space: 1000001

