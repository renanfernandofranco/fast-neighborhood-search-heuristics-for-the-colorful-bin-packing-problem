Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 18
Size: 263 Color: 4
Size: 259 Color: 12

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 17
Size: 356 Color: 0
Size: 282 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 327 Color: 9
Size: 276 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 6
Size: 250 Color: 13
Size: 252 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 12
Size: 289 Color: 19
Size: 281 Color: 14

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 17
Size: 252 Color: 9
Size: 250 Color: 16

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 324 Color: 3
Size: 276 Color: 4
Size: 400 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 18
Size: 304 Color: 19
Size: 262 Color: 16

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 18
Size: 262 Color: 12
Size: 252 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 13
Size: 352 Color: 11
Size: 261 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 270 Color: 15
Size: 250 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 12
Size: 259 Color: 17
Size: 251 Color: 5

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 18
Size: 356 Color: 15
Size: 250 Color: 17

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 344 Color: 5
Size: 270 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 16
Size: 347 Color: 6
Size: 292 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 262 Color: 9
Size: 256 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 323 Color: 3
Size: 250 Color: 5

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 4
Size: 285 Color: 3
Size: 253 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 9
Size: 293 Color: 6
Size: 275 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 330 Color: 9
Size: 256 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 0
Size: 336 Color: 9
Size: 296 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 17
Size: 254 Color: 4
Size: 251 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 10
Size: 336 Color: 2
Size: 269 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 4
Size: 338 Color: 10
Size: 280 Color: 14

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 4
Size: 346 Color: 15
Size: 284 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 17
Size: 340 Color: 16
Size: 252 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 7
Size: 288 Color: 5
Size: 278 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 17
Size: 344 Color: 2
Size: 269 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 0
Size: 329 Color: 6
Size: 326 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 353 Color: 18
Size: 272 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 6
Size: 357 Color: 5
Size: 277 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 9
Size: 297 Color: 7
Size: 295 Color: 18

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 305 Color: 2
Size: 304 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 314 Color: 18
Size: 292 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 18
Size: 314 Color: 19
Size: 276 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 13
Size: 362 Color: 15
Size: 274 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 7
Size: 271 Color: 7
Size: 251 Color: 19

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 13
Size: 288 Color: 13
Size: 284 Color: 19

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 5
Size: 300 Color: 17
Size: 281 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 5
Size: 251 Color: 13
Size: 250 Color: 11

Total size: 40000
Total free space: 0

