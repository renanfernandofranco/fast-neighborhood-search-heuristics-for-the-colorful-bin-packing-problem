Capicity Bin: 7568
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 32
Items: 
Size: 448 Color: 1
Size: 412 Color: 1
Size: 362 Color: 1
Size: 356 Color: 1
Size: 328 Color: 1
Size: 320 Color: 1
Size: 300 Color: 1
Size: 288 Color: 1
Size: 288 Color: 1
Size: 280 Color: 1
Size: 256 Color: 1
Size: 256 Color: 1
Size: 240 Color: 1
Size: 236 Color: 1
Size: 236 Color: 1
Size: 228 Color: 0
Size: 206 Color: 0
Size: 204 Color: 0
Size: 200 Color: 0
Size: 200 Color: 0
Size: 176 Color: 0
Size: 176 Color: 0
Size: 176 Color: 0
Size: 172 Color: 0
Size: 168 Color: 0
Size: 164 Color: 0
Size: 164 Color: 0
Size: 160 Color: 0
Size: 156 Color: 0
Size: 150 Color: 0
Size: 134 Color: 0
Size: 128 Color: 1

Bin 2: 0 of cap free
Amount of items: 18
Items: 
Size: 628 Color: 1
Size: 616 Color: 1
Size: 608 Color: 1
Size: 600 Color: 1
Size: 540 Color: 1
Size: 472 Color: 1
Size: 464 Color: 1
Size: 456 Color: 1
Size: 424 Color: 0
Size: 404 Color: 0
Size: 400 Color: 0
Size: 384 Color: 0
Size: 360 Color: 0
Size: 282 Color: 0
Size: 282 Color: 0
Size: 260 Color: 1
Size: 232 Color: 0
Size: 156 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 0
Size: 2660 Color: 0
Size: 212 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4729 Color: 0
Size: 2367 Color: 1
Size: 472 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5083 Color: 0
Size: 2071 Color: 1
Size: 414 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 1
Size: 2322 Color: 0
Size: 156 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 0
Size: 2061 Color: 1
Size: 412 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5091 Color: 1
Size: 2357 Color: 0
Size: 120 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 1
Size: 1860 Color: 0
Size: 368 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5426 Color: 1
Size: 1942 Color: 0
Size: 200 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5522 Color: 0
Size: 1786 Color: 0
Size: 260 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 1
Size: 1794 Color: 1
Size: 188 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 0
Size: 1151 Color: 0
Size: 789 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 0
Size: 1591 Color: 0
Size: 340 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 5643 Color: 1
Size: 1793 Color: 1
Size: 132 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 0
Size: 1654 Color: 0
Size: 188 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 1
Size: 830 Color: 1
Size: 624 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6192 Color: 1
Size: 828 Color: 1
Size: 548 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 1
Size: 1162 Color: 0
Size: 132 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 0
Size: 951 Color: 0
Size: 316 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 1
Size: 652 Color: 0
Size: 544 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 1
Size: 949 Color: 0
Size: 192 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 0
Size: 674 Color: 0
Size: 468 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 0
Size: 676 Color: 0
Size: 320 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 1
Size: 829 Color: 1
Size: 164 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 1
Size: 654 Color: 1
Size: 292 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 1
Size: 764 Color: 1
Size: 152 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 1
Size: 673 Color: 1
Size: 224 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6686 Color: 1
Size: 666 Color: 0
Size: 216 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 1
Size: 663 Color: 1
Size: 144 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 1
Size: 544 Color: 0
Size: 254 Color: 1

Bin 32: 1 of cap free
Amount of items: 5
Items: 
Size: 3785 Color: 0
Size: 2066 Color: 1
Size: 1022 Color: 1
Size: 464 Color: 0
Size: 230 Color: 0

Bin 33: 1 of cap free
Amount of items: 2
Items: 
Size: 5758 Color: 0
Size: 1809 Color: 1

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 5796 Color: 1
Size: 1611 Color: 0
Size: 160 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 1
Size: 1585 Color: 0
Size: 176 Color: 1

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5875 Color: 0
Size: 954 Color: 0
Size: 738 Color: 1

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 0
Size: 1006 Color: 1
Size: 682 Color: 0

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5990 Color: 1
Size: 1409 Color: 0
Size: 168 Color: 1

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6179 Color: 0
Size: 1156 Color: 0
Size: 232 Color: 1

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6563 Color: 0
Size: 1004 Color: 1

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6582 Color: 0
Size: 889 Color: 1
Size: 96 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 1
Size: 540 Color: 0
Size: 364 Color: 0

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 0
Size: 870 Color: 1

Bin 44: 2 of cap free
Amount of items: 3
Items: 
Size: 4722 Color: 0
Size: 2308 Color: 1
Size: 536 Color: 1

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 1
Size: 1964 Color: 0
Size: 144 Color: 1

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 1
Size: 1273 Color: 0
Size: 634 Color: 1

Bin 47: 2 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 1
Size: 1786 Color: 0

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 0
Size: 1620 Color: 0
Size: 148 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 1
Size: 1442 Color: 0
Size: 128 Color: 0

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6754 Color: 1
Size: 812 Color: 0

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6788 Color: 1
Size: 778 Color: 0

Bin 52: 3 of cap free
Amount of items: 5
Items: 
Size: 3794 Color: 1
Size: 2268 Color: 0
Size: 1023 Color: 1
Size: 256 Color: 1
Size: 224 Color: 0

Bin 53: 3 of cap free
Amount of items: 2
Items: 
Size: 6621 Color: 1
Size: 944 Color: 0

Bin 54: 3 of cap free
Amount of items: 2
Items: 
Size: 6743 Color: 1
Size: 822 Color: 0

Bin 55: 3 of cap free
Amount of items: 2
Items: 
Size: 6810 Color: 0
Size: 755 Color: 1

Bin 56: 4 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 1
Size: 2706 Color: 0
Size: 528 Color: 1

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 1
Size: 1815 Color: 0
Size: 368 Color: 1

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 5538 Color: 0
Size: 2026 Color: 1

Bin 59: 4 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 1
Size: 1152 Color: 0
Size: 240 Color: 0

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 6145 Color: 0
Size: 1419 Color: 1

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6482 Color: 0
Size: 1082 Color: 1

Bin 62: 4 of cap free
Amount of items: 3
Items: 
Size: 6773 Color: 1
Size: 727 Color: 0
Size: 64 Color: 0

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 0
Size: 1706 Color: 0
Size: 190 Color: 1

Bin 64: 5 of cap free
Amount of items: 2
Items: 
Size: 6506 Color: 1
Size: 1057 Color: 0

Bin 65: 5 of cap free
Amount of items: 4
Items: 
Size: 6700 Color: 1
Size: 703 Color: 0
Size: 112 Color: 1
Size: 48 Color: 0

Bin 66: 6 of cap free
Amount of items: 2
Items: 
Size: 4852 Color: 1
Size: 2710 Color: 0

Bin 67: 6 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 0
Size: 891 Color: 1
Size: 48 Color: 0

Bin 68: 7 of cap free
Amount of items: 8
Items: 
Size: 3786 Color: 1
Size: 721 Color: 1
Size: 689 Color: 1
Size: 677 Color: 1
Size: 472 Color: 0
Size: 464 Color: 0
Size: 456 Color: 0
Size: 296 Color: 0

Bin 69: 7 of cap free
Amount of items: 3
Items: 
Size: 4299 Color: 1
Size: 3052 Color: 0
Size: 210 Color: 1

Bin 70: 7 of cap free
Amount of items: 2
Items: 
Size: 6573 Color: 1
Size: 988 Color: 0

Bin 71: 8 of cap free
Amount of items: 2
Items: 
Size: 6270 Color: 1
Size: 1290 Color: 0

Bin 72: 8 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 0
Size: 892 Color: 1

Bin 73: 8 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 1
Size: 724 Color: 0
Size: 72 Color: 0

Bin 74: 9 of cap free
Amount of items: 3
Items: 
Size: 5867 Color: 1
Size: 1448 Color: 0
Size: 244 Color: 1

Bin 75: 9 of cap free
Amount of items: 2
Items: 
Size: 6049 Color: 1
Size: 1510 Color: 0

Bin 76: 10 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 1
Size: 1823 Color: 0
Size: 336 Color: 0

Bin 77: 10 of cap free
Amount of items: 2
Items: 
Size: 6526 Color: 0
Size: 1032 Color: 1

Bin 78: 10 of cap free
Amount of items: 2
Items: 
Size: 6722 Color: 0
Size: 836 Color: 1

Bin 79: 11 of cap free
Amount of items: 2
Items: 
Size: 6431 Color: 1
Size: 1126 Color: 0

Bin 80: 11 of cap free
Amount of items: 4
Items: 
Size: 6703 Color: 1
Size: 706 Color: 0
Size: 100 Color: 0
Size: 48 Color: 1

Bin 81: 12 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 0
Size: 886 Color: 1
Size: 96 Color: 1

Bin 82: 12 of cap free
Amount of items: 2
Items: 
Size: 6725 Color: 1
Size: 831 Color: 0

Bin 83: 14 of cap free
Amount of items: 2
Items: 
Size: 5180 Color: 0
Size: 2374 Color: 1

Bin 84: 14 of cap free
Amount of items: 2
Items: 
Size: 6340 Color: 0
Size: 1214 Color: 1

Bin 85: 15 of cap free
Amount of items: 2
Items: 
Size: 6142 Color: 0
Size: 1411 Color: 1

Bin 86: 15 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 0
Size: 957 Color: 1

Bin 87: 15 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 0
Size: 791 Color: 1

Bin 88: 16 of cap free
Amount of items: 5
Items: 
Size: 3788 Color: 1
Size: 2342 Color: 1
Size: 630 Color: 0
Size: 628 Color: 0
Size: 164 Color: 1

Bin 89: 21 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 1
Size: 1044 Color: 0

Bin 90: 21 of cap free
Amount of items: 2
Items: 
Size: 6757 Color: 0
Size: 790 Color: 1

Bin 91: 22 of cap free
Amount of items: 2
Items: 
Size: 5138 Color: 0
Size: 2408 Color: 1

Bin 92: 22 of cap free
Amount of items: 2
Items: 
Size: 5838 Color: 0
Size: 1708 Color: 1

Bin 93: 22 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 0
Size: 1046 Color: 1

Bin 94: 23 of cap free
Amount of items: 2
Items: 
Size: 6638 Color: 1
Size: 907 Color: 0

Bin 95: 24 of cap free
Amount of items: 2
Items: 
Size: 5220 Color: 1
Size: 2324 Color: 0

Bin 96: 25 of cap free
Amount of items: 2
Items: 
Size: 4727 Color: 1
Size: 2816 Color: 0

Bin 97: 25 of cap free
Amount of items: 2
Items: 
Size: 6041 Color: 1
Size: 1502 Color: 0

Bin 98: 29 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 1
Size: 2148 Color: 0

Bin 99: 30 of cap free
Amount of items: 2
Items: 
Size: 6501 Color: 1
Size: 1037 Color: 0

Bin 100: 32 of cap free
Amount of items: 2
Items: 
Size: 4380 Color: 0
Size: 3156 Color: 1

Bin 101: 32 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 0
Size: 1028 Color: 1
Size: 48 Color: 1

Bin 102: 33 of cap free
Amount of items: 2
Items: 
Size: 6786 Color: 1
Size: 749 Color: 0

Bin 103: 36 of cap free
Amount of items: 2
Items: 
Size: 3796 Color: 1
Size: 3736 Color: 0

Bin 104: 37 of cap free
Amount of items: 2
Items: 
Size: 5162 Color: 1
Size: 2369 Color: 0

Bin 105: 37 of cap free
Amount of items: 2
Items: 
Size: 6341 Color: 1
Size: 1190 Color: 0

Bin 106: 38 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 0
Size: 3304 Color: 1
Size: 318 Color: 0

Bin 107: 38 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 1
Size: 2006 Color: 0

Bin 108: 40 of cap free
Amount of items: 2
Items: 
Size: 4782 Color: 0
Size: 2746 Color: 1

Bin 109: 41 of cap free
Amount of items: 2
Items: 
Size: 4804 Color: 1
Size: 2723 Color: 0

Bin 110: 43 of cap free
Amount of items: 2
Items: 
Size: 6338 Color: 1
Size: 1187 Color: 0

Bin 111: 44 of cap free
Amount of items: 2
Items: 
Size: 6040 Color: 1
Size: 1484 Color: 0

Bin 112: 45 of cap free
Amount of items: 2
Items: 
Size: 6561 Color: 1
Size: 962 Color: 0

Bin 113: 47 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 0
Size: 1159 Color: 1

Bin 114: 54 of cap free
Amount of items: 2
Items: 
Size: 6022 Color: 0
Size: 1492 Color: 1

Bin 115: 58 of cap free
Amount of items: 2
Items: 
Size: 4788 Color: 1
Size: 2722 Color: 0

Bin 116: 61 of cap free
Amount of items: 2
Items: 
Size: 6474 Color: 1
Size: 1033 Color: 0

Bin 117: 62 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 1
Size: 1318 Color: 0

Bin 118: 65 of cap free
Amount of items: 2
Items: 
Size: 6187 Color: 1
Size: 1316 Color: 0

Bin 119: 66 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 1
Size: 2700 Color: 0
Size: 528 Color: 0

Bin 120: 69 of cap free
Amount of items: 2
Items: 
Size: 6191 Color: 0
Size: 1308 Color: 1

Bin 121: 75 of cap free
Amount of items: 2
Items: 
Size: 6329 Color: 1
Size: 1164 Color: 0

Bin 122: 83 of cap free
Amount of items: 2
Items: 
Size: 4332 Color: 0
Size: 3153 Color: 1

Bin 123: 84 of cap free
Amount of items: 2
Items: 
Size: 6458 Color: 1
Size: 1026 Color: 0

Bin 124: 85 of cap free
Amount of items: 2
Items: 
Size: 4758 Color: 0
Size: 2725 Color: 1

Bin 125: 86 of cap free
Amount of items: 2
Items: 
Size: 6004 Color: 0
Size: 1478 Color: 1

Bin 126: 87 of cap free
Amount of items: 2
Items: 
Size: 6174 Color: 1
Size: 1307 Color: 0

Bin 127: 88 of cap free
Amount of items: 2
Items: 
Size: 4322 Color: 1
Size: 3158 Color: 0

Bin 128: 94 of cap free
Amount of items: 2
Items: 
Size: 6325 Color: 0
Size: 1149 Color: 1

Bin 129: 100 of cap free
Amount of items: 2
Items: 
Size: 6324 Color: 0
Size: 1144 Color: 1

Bin 130: 103 of cap free
Amount of items: 2
Items: 
Size: 3789 Color: 1
Size: 3676 Color: 0

Bin 131: 108 of cap free
Amount of items: 2
Items: 
Size: 4306 Color: 1
Size: 3154 Color: 0

Bin 132: 116 of cap free
Amount of items: 2
Items: 
Size: 4301 Color: 1
Size: 3151 Color: 0

Bin 133: 4894 of cap free
Amount of items: 18
Items: 
Size: 230 Color: 1
Size: 206 Color: 1
Size: 204 Color: 1
Size: 152 Color: 1
Size: 148 Color: 0
Size: 144 Color: 0
Size: 144 Color: 0
Size: 140 Color: 1
Size: 140 Color: 1
Size: 136 Color: 0
Size: 134 Color: 0
Size: 132 Color: 1
Size: 132 Color: 0
Size: 132 Color: 0
Size: 128 Color: 0
Size: 128 Color: 0
Size: 124 Color: 1
Size: 120 Color: 1

Total size: 998976
Total free space: 7568

