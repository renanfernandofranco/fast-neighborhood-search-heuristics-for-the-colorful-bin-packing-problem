Capicity Bin: 15872
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 9032 Color: 422
Size: 5704 Color: 383
Size: 464 Color: 102
Size: 336 Color: 59
Size: 336 Color: 58

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 9924 Color: 433
Size: 4924 Color: 367
Size: 400 Color: 81
Size: 312 Color: 47
Size: 312 Color: 46

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 476
Size: 2648 Color: 310
Size: 992 Color: 179

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 477
Size: 2815 Color: 319
Size: 812 Color: 159

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 483
Size: 2920 Color: 320
Size: 576 Color: 130

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 484
Size: 2468 Color: 297
Size: 928 Color: 169

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12645 Color: 491
Size: 2187 Color: 284
Size: 1040 Color: 183

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12699 Color: 493
Size: 1853 Color: 257
Size: 1320 Color: 202

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 495
Size: 2952 Color: 321
Size: 200 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12779 Color: 497
Size: 2813 Color: 318
Size: 280 Color: 26

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12811 Color: 499
Size: 2261 Color: 287
Size: 800 Color: 158

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12851 Color: 502
Size: 2509 Color: 300
Size: 512 Color: 118

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 508
Size: 2488 Color: 298
Size: 400 Color: 83

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 509
Size: 2176 Color: 282
Size: 700 Color: 149

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13015 Color: 510
Size: 2545 Color: 303
Size: 312 Color: 48

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 511
Size: 1642 Color: 239
Size: 1170 Color: 194

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13074 Color: 514
Size: 1486 Color: 222
Size: 1312 Color: 198

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 521
Size: 1400 Color: 210
Size: 1216 Color: 195

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 522
Size: 2175 Color: 281
Size: 434 Color: 94

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 523
Size: 1636 Color: 238
Size: 960 Color: 171

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13279 Color: 524
Size: 2149 Color: 278
Size: 444 Color: 97

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 534
Size: 1916 Color: 262
Size: 456 Color: 100

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13565 Color: 538
Size: 1923 Color: 263
Size: 384 Color: 76

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 541
Size: 1764 Color: 252
Size: 496 Color: 112

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 542
Size: 1980 Color: 268
Size: 274 Color: 24

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 544
Size: 1492 Color: 223
Size: 736 Color: 152

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13658 Color: 545
Size: 1914 Color: 261
Size: 300 Color: 42

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13758 Color: 551
Size: 1458 Color: 218
Size: 656 Color: 144

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 552
Size: 1753 Color: 250
Size: 360 Color: 69

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13806 Color: 556
Size: 1562 Color: 230
Size: 504 Color: 115

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 557
Size: 1721 Color: 247
Size: 342 Color: 61

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13825 Color: 558
Size: 1619 Color: 236
Size: 428 Color: 91

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 561
Size: 1656 Color: 243
Size: 320 Color: 51

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 562
Size: 1625 Color: 237
Size: 350 Color: 65

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 563
Size: 1560 Color: 229
Size: 412 Color: 85

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 565
Size: 1044 Color: 184
Size: 912 Color: 167

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13993 Color: 570
Size: 1495 Color: 224
Size: 384 Color: 77

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 571
Size: 1565 Color: 232
Size: 312 Color: 45

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 572
Size: 1404 Color: 211
Size: 472 Color: 106

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14069 Color: 577
Size: 1459 Color: 219
Size: 344 Color: 63

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14075 Color: 578
Size: 1499 Color: 226
Size: 298 Color: 41

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14078 Color: 579
Size: 1394 Color: 209
Size: 400 Color: 82

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14079 Color: 580
Size: 1503 Color: 227
Size: 290 Color: 37

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14090 Color: 581
Size: 1484 Color: 221
Size: 298 Color: 40

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14120 Color: 583
Size: 1464 Color: 220
Size: 288 Color: 31

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14126 Color: 585
Size: 1088 Color: 185
Size: 658 Color: 145

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 586
Size: 1455 Color: 217
Size: 290 Color: 36

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14154 Color: 588
Size: 1434 Color: 215
Size: 284 Color: 28

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14155 Color: 589
Size: 1431 Color: 214
Size: 286 Color: 29

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 592
Size: 1024 Color: 181
Size: 652 Color: 143

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 593
Size: 1332 Color: 205
Size: 340 Color: 60

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 594
Size: 1160 Color: 193
Size: 510 Color: 116

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14218 Color: 595
Size: 1382 Color: 208
Size: 272 Color: 23

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 597
Size: 1136 Color: 190
Size: 476 Color: 107

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14276 Color: 598
Size: 1248 Color: 196
Size: 348 Color: 64

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 599
Size: 1140 Color: 192
Size: 452 Color: 98

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14282 Color: 600
Size: 1136 Color: 189
Size: 454 Color: 99

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 11741 Color: 463
Size: 3282 Color: 334
Size: 848 Color: 163

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 12330 Color: 481
Size: 3541 Color: 343

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 486
Size: 2761 Color: 314
Size: 604 Color: 136

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 12644 Color: 490
Size: 2691 Color: 312
Size: 536 Color: 123

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 498
Size: 1761 Color: 251
Size: 1320 Color: 200

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 516
Size: 2408 Color: 295
Size: 320 Color: 49

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13352 Color: 529
Size: 2519 Color: 301

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 13633 Color: 543
Size: 2238 Color: 286

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 13769 Color: 554
Size: 2102 Color: 273

Bin 67: 2 of cap free
Amount of items: 7
Items: 
Size: 7941 Color: 408
Size: 1644 Color: 240
Size: 1614 Color: 235
Size: 1600 Color: 234
Size: 1567 Color: 233
Size: 1088 Color: 186
Size: 416 Color: 87

Bin 68: 2 of cap free
Amount of items: 5
Items: 
Size: 9954 Color: 435
Size: 4936 Color: 369
Size: 388 Color: 78
Size: 296 Color: 39
Size: 296 Color: 38

Bin 69: 2 of cap free
Amount of items: 5
Items: 
Size: 9956 Color: 436
Size: 4946 Color: 370
Size: 392 Color: 79
Size: 288 Color: 35
Size: 288 Color: 34

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 461
Size: 4056 Color: 354
Size: 164 Color: 8

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 466
Size: 3294 Color: 335
Size: 708 Color: 150

Bin 72: 2 of cap free
Amount of items: 4
Items: 
Size: 12299 Color: 479
Size: 3443 Color: 340
Size: 80 Color: 1
Size: 48 Color: 0

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12916 Color: 506
Size: 2954 Color: 322

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 507
Size: 2628 Color: 308
Size: 288 Color: 32

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13064 Color: 513
Size: 2806 Color: 317

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 519
Size: 2632 Color: 309

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13466 Color: 533
Size: 2404 Color: 294

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 13828 Color: 559
Size: 2042 Color: 271

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14123 Color: 584
Size: 1747 Color: 249

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 505
Size: 2965 Color: 323

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 527
Size: 2551 Color: 304

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 549
Size: 2124 Color: 276

Bin 83: 4 of cap free
Amount of items: 5
Items: 
Size: 7956 Color: 411
Size: 6610 Color: 395
Size: 502 Color: 114
Size: 404 Color: 84
Size: 396 Color: 80

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 10248 Color: 441
Size: 5348 Color: 379
Size: 272 Color: 20

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 11677 Color: 462
Size: 4191 Color: 358

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 12712 Color: 494
Size: 3156 Color: 329

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 13249 Color: 520
Size: 2619 Color: 307

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13704 Color: 548
Size: 2164 Color: 280

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13752 Color: 550
Size: 2116 Color: 275

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 553
Size: 2104 Color: 274

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13923 Color: 566
Size: 1945 Color: 266

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 574
Size: 1860 Color: 258

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14100 Color: 582
Size: 1768 Color: 253

Bin 94: 5 of cap free
Amount of items: 5
Items: 
Size: 8024 Color: 412
Size: 6611 Color: 396
Size: 488 Color: 111
Size: 372 Color: 74
Size: 372 Color: 73

Bin 95: 5 of cap free
Amount of items: 3
Items: 
Size: 8911 Color: 417
Size: 6604 Color: 394
Size: 352 Color: 67

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 12495 Color: 485
Size: 3372 Color: 338

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 12819 Color: 500
Size: 3048 Color: 327

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 504
Size: 2979 Color: 324

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 13931 Color: 567
Size: 1936 Color: 265

Bin 100: 5 of cap free
Amount of items: 2
Items: 
Size: 14159 Color: 590
Size: 1708 Color: 246

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 596
Size: 1647 Color: 242

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 10994 Color: 450
Size: 4872 Color: 366

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 482
Size: 3522 Color: 341

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13938 Color: 568
Size: 1928 Color: 264

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13984 Color: 569
Size: 1882 Color: 260

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14050 Color: 576
Size: 1816 Color: 255

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14180 Color: 591
Size: 1686 Color: 244

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11938 Color: 470
Size: 3783 Color: 348
Size: 144 Color: 4

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13061 Color: 512
Size: 2804 Color: 316

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 13295 Color: 526
Size: 2570 Color: 305

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 13998 Color: 573
Size: 1867 Color: 259

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 11356 Color: 455
Size: 4316 Color: 360
Size: 192 Color: 11

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 12060 Color: 473
Size: 3804 Color: 350

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13332 Color: 528
Size: 2532 Color: 302

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13516 Color: 535
Size: 2348 Color: 292

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 525
Size: 2579 Color: 306

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 13375 Color: 530
Size: 2488 Color: 299

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 11919 Color: 469
Size: 3799 Color: 349
Size: 144 Color: 5

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 536
Size: 2334 Color: 291

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 13539 Color: 537
Size: 2323 Color: 290

Bin 121: 10 of cap free
Amount of items: 2
Items: 
Size: 13698 Color: 547
Size: 2164 Color: 279

Bin 122: 10 of cap free
Amount of items: 2
Items: 
Size: 13902 Color: 564
Size: 1960 Color: 267

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 587
Size: 1722 Color: 248

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 454
Size: 3180 Color: 330
Size: 1348 Color: 207

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 10700 Color: 445
Size: 5160 Color: 375

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 12616 Color: 489
Size: 3244 Color: 333

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 518
Size: 2670 Color: 311

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 531
Size: 2434 Color: 296

Bin 129: 13 of cap free
Amount of items: 2
Items: 
Size: 12836 Color: 501
Size: 3023 Color: 326

Bin 130: 14 of cap free
Amount of items: 2
Items: 
Size: 13777 Color: 555
Size: 2081 Color: 272

Bin 131: 14 of cap free
Amount of items: 2
Items: 
Size: 14044 Color: 575
Size: 1814 Color: 254

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 13659 Color: 546
Size: 2198 Color: 285

Bin 133: 15 of cap free
Amount of items: 2
Items: 
Size: 13850 Color: 560
Size: 2007 Color: 270

Bin 134: 16 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 460
Size: 3241 Color: 332
Size: 992 Color: 178

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 474
Size: 3652 Color: 344
Size: 112 Color: 3

Bin 136: 16 of cap free
Amount of items: 2
Items: 
Size: 13580 Color: 540
Size: 2276 Color: 289

Bin 137: 18 of cap free
Amount of items: 5
Items: 
Size: 9938 Color: 434
Size: 4932 Color: 368
Size: 376 Color: 75
Size: 304 Color: 44
Size: 304 Color: 43

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 12559 Color: 488
Size: 3295 Color: 336

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 12863 Color: 503
Size: 2991 Color: 325

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 10845 Color: 447
Size: 5008 Color: 373

Bin 141: 21 of cap free
Amount of items: 2
Items: 
Size: 13159 Color: 517
Size: 2692 Color: 313

Bin 142: 21 of cap free
Amount of items: 2
Items: 
Size: 13576 Color: 539
Size: 2275 Color: 288

Bin 143: 22 of cap free
Amount of items: 2
Items: 
Size: 11828 Color: 465
Size: 4022 Color: 353

Bin 144: 22 of cap free
Amount of items: 2
Items: 
Size: 12670 Color: 492
Size: 3180 Color: 331

Bin 145: 24 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 415
Size: 6976 Color: 402
Size: 368 Color: 70

Bin 146: 25 of cap free
Amount of items: 3
Items: 
Size: 11016 Color: 451
Size: 4615 Color: 363
Size: 216 Color: 14

Bin 147: 25 of cap free
Amount of items: 2
Items: 
Size: 12315 Color: 480
Size: 3532 Color: 342

Bin 148: 26 of cap free
Amount of items: 5
Items: 
Size: 9028 Color: 421
Size: 5686 Color: 382
Size: 436 Color: 96
Size: 352 Color: 66
Size: 344 Color: 62

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 13085 Color: 515
Size: 2761 Color: 315

Bin 150: 26 of cap free
Amount of items: 2
Items: 
Size: 13465 Color: 532
Size: 2381 Color: 293

Bin 151: 30 of cap free
Amount of items: 3
Items: 
Size: 10399 Color: 443
Size: 5179 Color: 376
Size: 264 Color: 18

Bin 152: 32 of cap free
Amount of items: 3
Items: 
Size: 11496 Color: 458
Size: 4168 Color: 357
Size: 176 Color: 9

Bin 153: 36 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 448
Size: 4964 Color: 372

Bin 154: 36 of cap free
Amount of items: 2
Items: 
Size: 12724 Color: 496
Size: 3112 Color: 328

Bin 155: 37 of cap free
Amount of items: 3
Items: 
Size: 10899 Color: 449
Size: 4696 Color: 364
Size: 240 Color: 15

Bin 156: 38 of cap free
Amount of items: 20
Items: 
Size: 1024 Color: 180
Size: 988 Color: 177
Size: 984 Color: 176
Size: 984 Color: 175
Size: 976 Color: 174
Size: 976 Color: 173
Size: 960 Color: 172
Size: 960 Color: 170
Size: 922 Color: 168
Size: 892 Color: 166
Size: 864 Color: 165
Size: 856 Color: 164
Size: 836 Color: 162
Size: 832 Color: 161
Size: 480 Color: 108
Size: 472 Color: 105
Size: 464 Color: 104
Size: 464 Color: 103
Size: 464 Color: 101
Size: 436 Color: 95

Bin 157: 38 of cap free
Amount of items: 2
Items: 
Size: 11368 Color: 456
Size: 4466 Color: 361

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 11558 Color: 459
Size: 4276 Color: 359

Bin 159: 38 of cap free
Amount of items: 2
Items: 
Size: 11768 Color: 464
Size: 4066 Color: 355

Bin 160: 38 of cap free
Amount of items: 2
Items: 
Size: 12178 Color: 475
Size: 3656 Color: 345

Bin 161: 39 of cap free
Amount of items: 2
Items: 
Size: 11983 Color: 472
Size: 3850 Color: 352

Bin 162: 40 of cap free
Amount of items: 2
Items: 
Size: 7944 Color: 410
Size: 7888 Color: 404

Bin 163: 41 of cap free
Amount of items: 9
Items: 
Size: 7937 Color: 405
Size: 1320 Color: 199
Size: 1264 Color: 197
Size: 1140 Color: 191
Size: 1136 Color: 188
Size: 1136 Color: 187
Size: 1034 Color: 182
Size: 432 Color: 93
Size: 432 Color: 92

Bin 164: 42 of cap free
Amount of items: 2
Items: 
Size: 11269 Color: 452
Size: 4561 Color: 362

Bin 165: 44 of cap free
Amount of items: 2
Items: 
Size: 12508 Color: 487
Size: 3320 Color: 337

Bin 166: 48 of cap free
Amount of items: 3
Items: 
Size: 10040 Color: 439
Size: 5512 Color: 381
Size: 272 Color: 22

Bin 167: 50 of cap free
Amount of items: 3
Items: 
Size: 10335 Color: 442
Size: 5223 Color: 377
Size: 264 Color: 19

Bin 168: 50 of cap free
Amount of items: 4
Items: 
Size: 11316 Color: 453
Size: 2184 Color: 283
Size: 2130 Color: 277
Size: 192 Color: 12

Bin 169: 52 of cap free
Amount of items: 4
Items: 
Size: 9972 Color: 438
Size: 5288 Color: 378
Size: 284 Color: 27
Size: 276 Color: 25

Bin 170: 52 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 467
Size: 3764 Color: 346
Size: 160 Color: 7

Bin 171: 55 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 471
Size: 3837 Color: 351

Bin 172: 57 of cap free
Amount of items: 7
Items: 
Size: 7940 Color: 407
Size: 1564 Color: 231
Size: 1524 Color: 228
Size: 1498 Color: 225
Size: 1444 Color: 216
Size: 1429 Color: 213
Size: 416 Color: 88

Bin 173: 60 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 446
Size: 4808 Color: 365
Size: 256 Color: 16

Bin 174: 60 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 468
Size: 3768 Color: 347
Size: 144 Color: 6

Bin 175: 61 of cap free
Amount of items: 3
Items: 
Size: 8847 Color: 416
Size: 6612 Color: 397
Size: 352 Color: 68

Bin 176: 75 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 429
Size: 5754 Color: 388
Size: 320 Color: 50

Bin 177: 77 of cap free
Amount of items: 3
Items: 
Size: 12283 Color: 478
Size: 3432 Color: 339
Size: 80 Color: 2

Bin 178: 80 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 413
Size: 7208 Color: 403
Size: 368 Color: 72

Bin 179: 82 of cap free
Amount of items: 2
Items: 
Size: 9918 Color: 432
Size: 5872 Color: 391

Bin 180: 101 of cap free
Amount of items: 2
Items: 
Size: 9916 Color: 431
Size: 5855 Color: 390

Bin 181: 102 of cap free
Amount of items: 3
Items: 
Size: 9050 Color: 423
Size: 6392 Color: 393
Size: 328 Color: 57

Bin 182: 104 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 440
Size: 5392 Color: 380
Size: 272 Color: 21

Bin 183: 120 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 424
Size: 6152 Color: 392
Size: 328 Color: 56

Bin 184: 132 of cap free
Amount of items: 3
Items: 
Size: 10514 Color: 444
Size: 4962 Color: 371
Size: 264 Color: 17

Bin 185: 142 of cap free
Amount of items: 3
Items: 
Size: 9688 Color: 428
Size: 5722 Color: 387
Size: 320 Color: 52

Bin 186: 144 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 414
Size: 6904 Color: 401
Size: 368 Color: 71

Bin 187: 157 of cap free
Amount of items: 3
Items: 
Size: 11394 Color: 457
Size: 4145 Color: 356
Size: 176 Color: 10

Bin 188: 166 of cap free
Amount of items: 2
Items: 
Size: 9026 Color: 420
Size: 6680 Color: 400

Bin 189: 175 of cap free
Amount of items: 3
Items: 
Size: 9659 Color: 427
Size: 5716 Color: 386
Size: 322 Color: 53

Bin 190: 199 of cap free
Amount of items: 2
Items: 
Size: 9872 Color: 430
Size: 5801 Color: 389

Bin 191: 211 of cap free
Amount of items: 4
Items: 
Size: 9960 Color: 437
Size: 5125 Color: 374
Size: 288 Color: 33
Size: 288 Color: 30

Bin 192: 228 of cap free
Amount of items: 25
Items: 
Size: 828 Color: 160
Size: 768 Color: 157
Size: 766 Color: 156
Size: 756 Color: 155
Size: 752 Color: 154
Size: 752 Color: 153
Size: 720 Color: 151
Size: 688 Color: 148
Size: 672 Color: 147
Size: 672 Color: 146
Size: 648 Color: 142
Size: 648 Color: 141
Size: 640 Color: 140
Size: 632 Color: 139
Size: 560 Color: 126
Size: 552 Color: 125
Size: 536 Color: 124
Size: 532 Color: 122
Size: 520 Color: 121
Size: 514 Color: 120
Size: 512 Color: 119
Size: 512 Color: 117
Size: 500 Color: 113
Size: 484 Color: 110
Size: 480 Color: 109

Bin 193: 235 of cap free
Amount of items: 3
Items: 
Size: 9605 Color: 426
Size: 5708 Color: 385
Size: 324 Color: 54

Bin 194: 238 of cap free
Amount of items: 2
Items: 
Size: 9020 Color: 419
Size: 6614 Color: 399

Bin 195: 249 of cap free
Amount of items: 2
Items: 
Size: 9010 Color: 418
Size: 6613 Color: 398

Bin 196: 294 of cap free
Amount of items: 3
Items: 
Size: 9544 Color: 425
Size: 5706 Color: 384
Size: 328 Color: 55

Bin 197: 310 of cap free
Amount of items: 6
Items: 
Size: 7942 Color: 409
Size: 2006 Color: 269
Size: 1845 Color: 256
Size: 1707 Color: 245
Size: 1646 Color: 241
Size: 416 Color: 86

Bin 198: 370 of cap free
Amount of items: 8
Items: 
Size: 7938 Color: 406
Size: 1412 Color: 212
Size: 1336 Color: 206
Size: 1326 Color: 204
Size: 1322 Color: 203
Size: 1320 Color: 201
Size: 424 Color: 90
Size: 424 Color: 89

Bin 199: 9978 of cap free
Amount of items: 10
Items: 
Size: 624 Color: 138
Size: 608 Color: 137
Size: 598 Color: 135
Size: 594 Color: 134
Size: 592 Color: 133
Size: 592 Color: 132
Size: 588 Color: 131
Size: 576 Color: 129
Size: 562 Color: 128
Size: 560 Color: 127

Total size: 3142656
Total free space: 15872

