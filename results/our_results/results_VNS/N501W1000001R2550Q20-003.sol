Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 422406 Color: 13
Size: 326138 Color: 15
Size: 251457 Color: 13

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 480829 Color: 8
Size: 262225 Color: 1
Size: 256947 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 495267 Color: 5
Size: 254502 Color: 10
Size: 250232 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 397950 Color: 6
Size: 335440 Color: 5
Size: 266611 Color: 14

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 419193 Color: 8
Size: 330044 Color: 11
Size: 250764 Color: 13

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 473195 Color: 1
Size: 264736 Color: 12
Size: 262070 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 423538 Color: 4
Size: 303336 Color: 14
Size: 273127 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 499431 Color: 10
Size: 250469 Color: 18
Size: 250101 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 449207 Color: 19
Size: 284544 Color: 18
Size: 266250 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 483442 Color: 0
Size: 261719 Color: 0
Size: 254840 Color: 7

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 383158 Color: 0
Size: 322499 Color: 13
Size: 294344 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 451566 Color: 2
Size: 290590 Color: 11
Size: 257845 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 449251 Color: 11
Size: 288828 Color: 5
Size: 261922 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 372342 Color: 2
Size: 326230 Color: 4
Size: 301429 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 373493 Color: 18
Size: 323929 Color: 7
Size: 302579 Color: 8

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 400478 Color: 6
Size: 315370 Color: 15
Size: 284153 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 416695 Color: 11
Size: 301597 Color: 12
Size: 281709 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 460338 Color: 2
Size: 283727 Color: 6
Size: 255936 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 435921 Color: 1
Size: 285286 Color: 16
Size: 278794 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 344968 Color: 11
Size: 331216 Color: 5
Size: 323817 Color: 11

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 428411 Color: 19
Size: 300088 Color: 8
Size: 271502 Color: 12

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 449842 Color: 3
Size: 293210 Color: 14
Size: 256949 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 442128 Color: 0
Size: 281733 Color: 3
Size: 276140 Color: 18

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 398799 Color: 2
Size: 301072 Color: 8
Size: 300130 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 424305 Color: 8
Size: 322888 Color: 1
Size: 252808 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 421348 Color: 14
Size: 290177 Color: 16
Size: 288476 Color: 10

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 407167 Color: 16
Size: 330056 Color: 11
Size: 262778 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 430883 Color: 0
Size: 318960 Color: 18
Size: 250158 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 403523 Color: 3
Size: 326307 Color: 8
Size: 270171 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 355244 Color: 0
Size: 340495 Color: 10
Size: 304262 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 397738 Color: 17
Size: 342901 Color: 0
Size: 259362 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 440214 Color: 1
Size: 302487 Color: 17
Size: 257300 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 368216 Color: 19
Size: 352287 Color: 14
Size: 279498 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 427103 Color: 13
Size: 298816 Color: 18
Size: 274082 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 428206 Color: 14
Size: 299528 Color: 17
Size: 272267 Color: 16

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 354902 Color: 8
Size: 331206 Color: 10
Size: 313893 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 361402 Color: 16
Size: 356039 Color: 4
Size: 282560 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 418794 Color: 13
Size: 329933 Color: 15
Size: 251274 Color: 14

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 378177 Color: 11
Size: 327506 Color: 13
Size: 294318 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 413484 Color: 12
Size: 297785 Color: 2
Size: 288732 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 435346 Color: 7
Size: 282474 Color: 1
Size: 282181 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 358635 Color: 0
Size: 332800 Color: 6
Size: 308566 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 424582 Color: 3
Size: 296291 Color: 2
Size: 279128 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 499611 Color: 0
Size: 250342 Color: 17
Size: 250048 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 490745 Color: 13
Size: 257830 Color: 13
Size: 251426 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 419107 Color: 6
Size: 298698 Color: 5
Size: 282196 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 439733 Color: 8
Size: 290134 Color: 16
Size: 270134 Color: 6

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 401468 Color: 14
Size: 311744 Color: 12
Size: 286789 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 426016 Color: 10
Size: 319735 Color: 4
Size: 254250 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 403643 Color: 1
Size: 311767 Color: 9
Size: 284591 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 406757 Color: 3
Size: 316922 Color: 13
Size: 276322 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 372698 Color: 18
Size: 365937 Color: 0
Size: 261366 Color: 11

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 487682 Color: 15
Size: 261974 Color: 11
Size: 250345 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 347423 Color: 16
Size: 333079 Color: 16
Size: 319499 Color: 5

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 397229 Color: 8
Size: 325112 Color: 14
Size: 277660 Color: 7

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 334920 Color: 12
Size: 333595 Color: 14
Size: 331486 Color: 13

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 387269 Color: 11
Size: 360507 Color: 7
Size: 252225 Color: 7

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 451978 Color: 15
Size: 289729 Color: 6
Size: 258294 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 413640 Color: 17
Size: 300947 Color: 16
Size: 285414 Color: 5

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 370373 Color: 15
Size: 353522 Color: 8
Size: 276106 Color: 6

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 366800 Color: 5
Size: 351336 Color: 10
Size: 281865 Color: 9

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 379537 Color: 15
Size: 354995 Color: 19
Size: 265469 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 412374 Color: 17
Size: 325003 Color: 8
Size: 262624 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 427164 Color: 15
Size: 293294 Color: 3
Size: 279543 Color: 13

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 440207 Color: 11
Size: 280875 Color: 7
Size: 278919 Color: 8

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 382562 Color: 16
Size: 349028 Color: 6
Size: 268411 Color: 19

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 445443 Color: 9
Size: 291729 Color: 5
Size: 262829 Color: 15

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 476188 Color: 2
Size: 266663 Color: 10
Size: 257150 Color: 16

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 492954 Color: 4
Size: 254072 Color: 14
Size: 252975 Color: 5

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 415152 Color: 0
Size: 312693 Color: 14
Size: 272156 Color: 17

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 391281 Color: 0
Size: 346478 Color: 1
Size: 262242 Color: 14

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 493353 Color: 13
Size: 254276 Color: 4
Size: 252372 Color: 14

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 438011 Color: 12
Size: 307853 Color: 2
Size: 254137 Color: 7

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 445013 Color: 5
Size: 284308 Color: 9
Size: 270680 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 418383 Color: 19
Size: 328401 Color: 15
Size: 253217 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 456499 Color: 14
Size: 292254 Color: 9
Size: 251248 Color: 6

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 459456 Color: 0
Size: 290076 Color: 5
Size: 250469 Color: 12

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 384330 Color: 4
Size: 310198 Color: 12
Size: 305473 Color: 6

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 347293 Color: 16
Size: 342624 Color: 10
Size: 310084 Color: 5

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 340189 Color: 14
Size: 337406 Color: 4
Size: 322406 Color: 8

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 493115 Color: 5
Size: 254003 Color: 19
Size: 252883 Color: 15

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 428825 Color: 17
Size: 301534 Color: 16
Size: 269642 Color: 15

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 418144 Color: 2
Size: 295233 Color: 1
Size: 286624 Color: 7

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 421713 Color: 16
Size: 298223 Color: 18
Size: 280065 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 455903 Color: 18
Size: 286237 Color: 8
Size: 257861 Color: 8

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 354631 Color: 11
Size: 343141 Color: 19
Size: 302229 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 447812 Color: 0
Size: 286233 Color: 9
Size: 265956 Color: 8

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 484056 Color: 4
Size: 263807 Color: 4
Size: 252138 Color: 5

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 429474 Color: 10
Size: 309295 Color: 8
Size: 261232 Color: 19

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 431818 Color: 7
Size: 295029 Color: 12
Size: 273154 Color: 18

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 386762 Color: 16
Size: 322887 Color: 19
Size: 290352 Color: 19

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 426889 Color: 1
Size: 320314 Color: 1
Size: 252798 Color: 15

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 387899 Color: 14
Size: 309749 Color: 1
Size: 302353 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 496312 Color: 0
Size: 253360 Color: 12
Size: 250329 Color: 12

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 454031 Color: 2
Size: 274207 Color: 3
Size: 271763 Color: 10

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 421211 Color: 4
Size: 313031 Color: 19
Size: 265759 Color: 19

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 378436 Color: 12
Size: 348634 Color: 16
Size: 272931 Color: 13

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 498427 Color: 9
Size: 250932 Color: 19
Size: 250642 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 365342 Color: 4
Size: 335690 Color: 18
Size: 298969 Color: 11

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 390938 Color: 3
Size: 283239 Color: 8
Size: 325824 Color: 16

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 428919 Color: 17
Size: 290218 Color: 19
Size: 280864 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 381094 Color: 18
Size: 320882 Color: 16
Size: 298025 Color: 18

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 371825 Color: 10
Size: 322104 Color: 0
Size: 306072 Color: 9

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 444841 Color: 3
Size: 285982 Color: 1
Size: 269178 Color: 15

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 468463 Color: 10
Size: 273942 Color: 4
Size: 257596 Color: 16

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 472895 Color: 16
Size: 277071 Color: 3
Size: 250035 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 462979 Color: 7
Size: 274109 Color: 14
Size: 262913 Color: 11

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 382376 Color: 7
Size: 332387 Color: 10
Size: 285238 Color: 2

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 449354 Color: 5
Size: 281261 Color: 0
Size: 269386 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 481505 Color: 9
Size: 268384 Color: 18
Size: 250112 Color: 7

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 373823 Color: 18
Size: 359066 Color: 5
Size: 267112 Color: 8

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 358599 Color: 6
Size: 355523 Color: 18
Size: 285879 Color: 13

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 392486 Color: 15
Size: 344356 Color: 15
Size: 263159 Color: 2

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 428823 Color: 13
Size: 303941 Color: 16
Size: 267237 Color: 16

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 387184 Color: 2
Size: 313525 Color: 7
Size: 299292 Color: 10

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 372816 Color: 17
Size: 359306 Color: 4
Size: 267879 Color: 13

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 434717 Color: 2
Size: 310920 Color: 16
Size: 254364 Color: 14

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 342079 Color: 8
Size: 333446 Color: 13
Size: 324476 Color: 12

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 437319 Color: 12
Size: 310421 Color: 6
Size: 252261 Color: 8

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 491244 Color: 4
Size: 254777 Color: 13
Size: 253980 Color: 13

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 390927 Color: 6
Size: 329012 Color: 7
Size: 280062 Color: 7

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 375202 Color: 10
Size: 328936 Color: 17
Size: 295863 Color: 16

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 382822 Color: 18
Size: 320677 Color: 0
Size: 296502 Color: 18

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 419141 Color: 12
Size: 302861 Color: 6
Size: 277999 Color: 11

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 384679 Color: 6
Size: 349644 Color: 19
Size: 265678 Color: 6

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 389634 Color: 1
Size: 355194 Color: 18
Size: 255173 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 361735 Color: 7
Size: 326032 Color: 17
Size: 312234 Color: 17

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 394493 Color: 2
Size: 305582 Color: 12
Size: 299926 Color: 19

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 436432 Color: 11
Size: 308740 Color: 12
Size: 254829 Color: 10

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 455389 Color: 5
Size: 286857 Color: 9
Size: 257755 Color: 10

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 366610 Color: 9
Size: 343764 Color: 10
Size: 289627 Color: 18

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 399947 Color: 9
Size: 306579 Color: 6
Size: 293475 Color: 9

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 473213 Color: 19
Size: 267883 Color: 14
Size: 258905 Color: 12

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 417457 Color: 13
Size: 329125 Color: 14
Size: 253419 Color: 15

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 370840 Color: 17
Size: 325687 Color: 16
Size: 303474 Color: 17

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 382394 Color: 2
Size: 353633 Color: 17
Size: 263974 Color: 9

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 394784 Color: 4
Size: 335687 Color: 13
Size: 269530 Color: 8

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 404002 Color: 14
Size: 317087 Color: 18
Size: 278912 Color: 19

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 479546 Color: 3
Size: 264119 Color: 12
Size: 256336 Color: 2

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 374597 Color: 10
Size: 342486 Color: 2
Size: 282918 Color: 14

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 367364 Color: 17
Size: 327866 Color: 15
Size: 304771 Color: 8

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 435614 Color: 8
Size: 283611 Color: 17
Size: 280776 Color: 6

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 427519 Color: 9
Size: 303027 Color: 9
Size: 269455 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 351643 Color: 16
Size: 345260 Color: 19
Size: 303098 Color: 14

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 487140 Color: 9
Size: 262065 Color: 19
Size: 250796 Color: 9

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 433688 Color: 5
Size: 315917 Color: 2
Size: 250396 Color: 7

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 417845 Color: 15
Size: 328384 Color: 5
Size: 253772 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 414150 Color: 0
Size: 315344 Color: 3
Size: 270507 Color: 11

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 458873 Color: 16
Size: 286363 Color: 8
Size: 254765 Color: 3

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 372335 Color: 18
Size: 325922 Color: 8
Size: 301744 Color: 15

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 458491 Color: 6
Size: 278525 Color: 5
Size: 262985 Color: 11

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 382938 Color: 4
Size: 313680 Color: 9
Size: 303383 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 465169 Color: 1
Size: 273238 Color: 9
Size: 261594 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 376645 Color: 19
Size: 314585 Color: 6
Size: 308771 Color: 15

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 409438 Color: 0
Size: 304223 Color: 0
Size: 286340 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 375647 Color: 1
Size: 323106 Color: 9
Size: 301248 Color: 18

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 366561 Color: 19
Size: 335435 Color: 5
Size: 298005 Color: 9

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 457422 Color: 8
Size: 276592 Color: 17
Size: 265987 Color: 5

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 378245 Color: 19
Size: 319309 Color: 8
Size: 302447 Color: 14

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 417317 Color: 13
Size: 317578 Color: 17
Size: 265106 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 386749 Color: 13
Size: 329915 Color: 11
Size: 283337 Color: 6

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 473758 Color: 16
Size: 274068 Color: 18
Size: 252175 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 448333 Color: 12
Size: 301458 Color: 14
Size: 250210 Color: 3

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 378023 Color: 8
Size: 316017 Color: 18
Size: 305961 Color: 10

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 496660 Color: 18
Size: 252342 Color: 9
Size: 250999 Color: 14

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 435200 Color: 1
Size: 311228 Color: 16
Size: 253573 Color: 9

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 385994 Color: 14
Size: 321701 Color: 3
Size: 292306 Color: 0

Total size: 167000167
Total free space: 0

