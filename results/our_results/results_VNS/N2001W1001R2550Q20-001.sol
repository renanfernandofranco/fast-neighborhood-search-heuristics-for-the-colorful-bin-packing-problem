Capicity Bin: 1001
Lower Bound: 667

Bins used: 667
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 306 Color: 12
Size: 325 Color: 19

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 19
Size: 258 Color: 7
Size: 258 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 14
Size: 279 Color: 11
Size: 256 Color: 18

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 323 Color: 8
Size: 250 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 328 Color: 18
Size: 313 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 14
Size: 279 Color: 7
Size: 271 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 19
Size: 296 Color: 5
Size: 272 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 12
Size: 281 Color: 17
Size: 251 Color: 6

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 7
Size: 306 Color: 12
Size: 290 Color: 5

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 19
Size: 299 Color: 9
Size: 288 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 15
Size: 312 Color: 9
Size: 255 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 14
Size: 317 Color: 18
Size: 252 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 2
Size: 335 Color: 2
Size: 313 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 13
Size: 276 Color: 13
Size: 257 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 5
Size: 335 Color: 11
Size: 280 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 16
Size: 319 Color: 18
Size: 316 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 4
Size: 318 Color: 17
Size: 311 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 16
Size: 328 Color: 13
Size: 250 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 13
Size: 265 Color: 16
Size: 262 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 0
Size: 314 Color: 18
Size: 283 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 5
Size: 296 Color: 16
Size: 271 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 16
Size: 318 Color: 15
Size: 268 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 5
Size: 353 Color: 16
Size: 289 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 363 Color: 19
Size: 268 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 4
Size: 340 Color: 9
Size: 255 Color: 9

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 4
Size: 316 Color: 8
Size: 314 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 13
Size: 292 Color: 1
Size: 288 Color: 19

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 4
Size: 289 Color: 7
Size: 276 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 6
Size: 287 Color: 2
Size: 272 Color: 5

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 2
Size: 301 Color: 9
Size: 294 Color: 10

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 16
Size: 338 Color: 11
Size: 312 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 12
Size: 255 Color: 2
Size: 250 Color: 6

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 343 Color: 1
Size: 268 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 0
Size: 326 Color: 9
Size: 319 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 19
Size: 272 Color: 17
Size: 269 Color: 10

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8
Size: 293 Color: 2
Size: 282 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 9
Size: 330 Color: 17
Size: 256 Color: 9

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 17
Size: 272 Color: 10
Size: 265 Color: 15

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 15
Size: 288 Color: 2
Size: 283 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 12
Size: 295 Color: 18
Size: 261 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 6
Size: 342 Color: 12
Size: 312 Color: 15

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 7
Size: 287 Color: 18
Size: 270 Color: 9

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 4
Size: 286 Color: 17
Size: 276 Color: 5

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 11
Size: 328 Color: 6
Size: 291 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 13
Size: 308 Color: 15
Size: 252 Color: 18

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 4
Size: 360 Color: 12
Size: 277 Color: 8

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 15
Size: 269 Color: 14
Size: 266 Color: 12

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 16
Size: 339 Color: 9
Size: 299 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 311 Color: 10
Size: 308 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 12
Size: 332 Color: 1
Size: 303 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 6
Size: 346 Color: 6
Size: 286 Color: 9

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 5
Size: 321 Color: 8
Size: 276 Color: 14

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 279 Color: 18
Size: 255 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 315 Color: 7
Size: 295 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 347 Color: 2
Size: 290 Color: 6

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 14
Size: 289 Color: 8
Size: 281 Color: 10

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 13
Size: 277 Color: 9
Size: 256 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 2
Size: 334 Color: 0
Size: 277 Color: 19

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 307 Color: 13
Size: 297 Color: 12

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 13
Size: 253 Color: 7
Size: 252 Color: 11

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 5
Size: 320 Color: 15
Size: 289 Color: 19

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 4
Size: 351 Color: 17
Size: 282 Color: 8

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 11
Size: 327 Color: 9
Size: 276 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 9
Size: 271 Color: 5
Size: 263 Color: 15

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 7
Size: 311 Color: 14
Size: 252 Color: 9

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 17
Size: 288 Color: 16
Size: 271 Color: 11

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 13
Size: 318 Color: 3
Size: 299 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 0
Size: 306 Color: 14
Size: 253 Color: 5

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8
Size: 331 Color: 2
Size: 254 Color: 13

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 6
Size: 304 Color: 14
Size: 259 Color: 7

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 331 Color: 7
Size: 263 Color: 2

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 5
Size: 339 Color: 12
Size: 250 Color: 19

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 19
Size: 333 Color: 10
Size: 306 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 3
Size: 258 Color: 4
Size: 253 Color: 16

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 15
Size: 341 Color: 12
Size: 308 Color: 10

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 16
Size: 275 Color: 3
Size: 265 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 11
Size: 316 Color: 12
Size: 283 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 7
Size: 301 Color: 5
Size: 254 Color: 8

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 18
Size: 287 Color: 15
Size: 253 Color: 6

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 16
Size: 292 Color: 12
Size: 279 Color: 2

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 15
Size: 312 Color: 15
Size: 289 Color: 12

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 315 Color: 1
Size: 282 Color: 3

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 7
Size: 317 Color: 16
Size: 254 Color: 7

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 6
Size: 268 Color: 13
Size: 250 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 15
Size: 271 Color: 9
Size: 256 Color: 17

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 9
Size: 303 Color: 6
Size: 270 Color: 16

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 9
Size: 350 Color: 5
Size: 254 Color: 6

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 10
Size: 324 Color: 6
Size: 259 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 3
Size: 310 Color: 1
Size: 267 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 329 Color: 16
Size: 288 Color: 8

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 15
Size: 258 Color: 9
Size: 253 Color: 12

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 14
Size: 332 Color: 19
Size: 307 Color: 17

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 18
Size: 320 Color: 14
Size: 315 Color: 17

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 5
Size: 322 Color: 0
Size: 293 Color: 8

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 16
Size: 251 Color: 11

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 303 Color: 17
Size: 288 Color: 16

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 0
Size: 273 Color: 4
Size: 255 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9
Size: 264 Color: 3
Size: 253 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 19
Size: 326 Color: 6
Size: 259 Color: 14

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 299 Color: 4
Size: 288 Color: 3

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 11
Size: 323 Color: 16
Size: 261 Color: 17

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 9
Size: 358 Color: 12
Size: 266 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 15
Size: 291 Color: 7
Size: 275 Color: 6

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 17
Size: 311 Color: 1
Size: 285 Color: 3

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 5
Size: 350 Color: 4
Size: 273 Color: 5

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 14
Size: 333 Color: 16
Size: 312 Color: 18

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 19
Size: 303 Color: 15
Size: 284 Color: 3

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 11
Size: 340 Color: 12
Size: 255 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 271 Color: 2
Size: 266 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 10
Size: 275 Color: 4
Size: 257 Color: 12

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 9
Size: 301 Color: 8
Size: 259 Color: 13

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 365 Color: 18
Size: 266 Color: 12

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 11
Size: 299 Color: 19
Size: 265 Color: 4

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 359 Color: 12
Size: 282 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 0
Size: 317 Color: 10
Size: 305 Color: 15

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 15
Size: 328 Color: 17
Size: 311 Color: 7

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 5
Size: 310 Color: 4
Size: 265 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 3
Size: 291 Color: 14
Size: 291 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 299 Color: 13
Size: 264 Color: 3

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 10
Size: 296 Color: 14
Size: 255 Color: 5

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 14
Size: 324 Color: 2
Size: 292 Color: 13

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 16
Size: 359 Color: 14
Size: 272 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 14
Size: 328 Color: 8
Size: 300 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 12
Size: 327 Color: 2
Size: 275 Color: 3

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 11
Size: 333 Color: 15
Size: 329 Color: 9

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 16
Size: 350 Color: 17
Size: 292 Color: 19

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 14
Size: 320 Color: 0
Size: 313 Color: 17

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 16
Size: 320 Color: 6
Size: 314 Color: 13

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 12
Size: 323 Color: 6
Size: 302 Color: 16

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 16
Size: 263 Color: 19
Size: 260 Color: 15

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 9
Size: 351 Color: 12
Size: 276 Color: 17

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 14
Size: 311 Color: 19
Size: 270 Color: 14

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 5
Size: 332 Color: 9
Size: 316 Color: 7

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 7
Size: 329 Color: 6
Size: 268 Color: 12

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 16
Size: 302 Color: 17
Size: 280 Color: 10

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 270 Color: 8
Size: 257 Color: 16

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 2
Size: 335 Color: 9
Size: 263 Color: 4

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 3
Size: 345 Color: 17
Size: 253 Color: 10

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 12
Size: 328 Color: 14
Size: 290 Color: 15

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 0
Size: 253 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 16
Size: 282 Color: 2
Size: 279 Color: 10

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 9
Size: 315 Color: 6
Size: 292 Color: 8

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 4
Size: 315 Color: 13
Size: 302 Color: 18

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 317 Color: 1
Size: 261 Color: 10

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 5
Size: 324 Color: 5
Size: 276 Color: 8

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 307 Color: 8
Size: 288 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 9
Size: 370 Color: 17
Size: 260 Color: 13

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 11
Size: 279 Color: 5
Size: 266 Color: 6

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 8
Size: 339 Color: 14
Size: 292 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 8
Size: 358 Color: 8
Size: 272 Color: 7

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 349 Color: 8
Size: 277 Color: 12

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 10
Size: 316 Color: 18
Size: 301 Color: 4

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 16
Size: 313 Color: 3
Size: 262 Color: 13

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 0
Size: 356 Color: 2
Size: 285 Color: 10

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 6
Size: 323 Color: 9
Size: 304 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 6
Size: 336 Color: 18
Size: 326 Color: 8

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 8
Size: 338 Color: 4
Size: 304 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 7
Size: 252 Color: 16
Size: 250 Color: 18

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 15
Size: 299 Color: 18
Size: 282 Color: 19

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 9
Size: 261 Color: 17
Size: 252 Color: 7

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 331 Color: 8
Size: 292 Color: 14

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 13
Size: 316 Color: 2
Size: 276 Color: 18

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 15
Size: 257 Color: 17
Size: 257 Color: 4

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 19
Size: 337 Color: 15
Size: 312 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 12
Size: 333 Color: 4
Size: 260 Color: 5

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 18
Size: 332 Color: 17
Size: 311 Color: 16

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 14
Size: 340 Color: 10
Size: 257 Color: 11

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 13
Size: 321 Color: 19
Size: 261 Color: 13

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 15
Size: 315 Color: 18
Size: 275 Color: 11

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 19
Size: 302 Color: 11
Size: 293 Color: 2

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 6
Size: 293 Color: 9
Size: 261 Color: 8

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 0
Size: 306 Color: 13
Size: 300 Color: 14

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 332 Color: 12
Size: 294 Color: 13

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 11
Size: 267 Color: 4
Size: 259 Color: 17

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8
Size: 288 Color: 4
Size: 278 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 6
Size: 360 Color: 9
Size: 256 Color: 18

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 11
Size: 308 Color: 2
Size: 298 Color: 5

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 19
Size: 295 Color: 12
Size: 289 Color: 8

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 10
Size: 313 Color: 1
Size: 276 Color: 7

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 8
Size: 254 Color: 7

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 296 Color: 16
Size: 291 Color: 2

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 294 Color: 10
Size: 293 Color: 19

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 0
Size: 315 Color: 2
Size: 302 Color: 13

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 11
Size: 320 Color: 12
Size: 257 Color: 11

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 9
Size: 260 Color: 3
Size: 255 Color: 14

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 11
Size: 272 Color: 1
Size: 265 Color: 5

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 16
Size: 335 Color: 18
Size: 291 Color: 13

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 318 Color: 9
Size: 297 Color: 17

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 8
Size: 346 Color: 7
Size: 277 Color: 17

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 17
Size: 284 Color: 2
Size: 281 Color: 19

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 9
Size: 285 Color: 11
Size: 256 Color: 19

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 4
Size: 316 Color: 4
Size: 280 Color: 18

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 18
Size: 264 Color: 6
Size: 261 Color: 4

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 18
Size: 266 Color: 9
Size: 256 Color: 1

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 4
Size: 331 Color: 14
Size: 320 Color: 14

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 5
Size: 311 Color: 8
Size: 265 Color: 16

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 12
Size: 365 Color: 17
Size: 265 Color: 7

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 15
Size: 338 Color: 14
Size: 251 Color: 8

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 7
Size: 311 Color: 10
Size: 261 Color: 12

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 8
Size: 279 Color: 10
Size: 272 Color: 2

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 15
Size: 325 Color: 12
Size: 273 Color: 10

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 12
Size: 300 Color: 11
Size: 285 Color: 18

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 17
Size: 351 Color: 15
Size: 288 Color: 15

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 12
Size: 336 Color: 3
Size: 290 Color: 5

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 5
Size: 316 Color: 4
Size: 290 Color: 4

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 19
Size: 355 Color: 8
Size: 281 Color: 19

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 19
Size: 304 Color: 18
Size: 254 Color: 6

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 309 Color: 18
Size: 281 Color: 4

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 16
Size: 327 Color: 3
Size: 263 Color: 2

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 303 Color: 1
Size: 281 Color: 14

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 11
Size: 338 Color: 10
Size: 250 Color: 19

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 6
Size: 299 Color: 9
Size: 255 Color: 2

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 12
Size: 333 Color: 7
Size: 311 Color: 10

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 19
Size: 306 Color: 15
Size: 272 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 12
Size: 303 Color: 14
Size: 263 Color: 19

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 2
Size: 268 Color: 3
Size: 255 Color: 15

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 11
Size: 339 Color: 18
Size: 302 Color: 4

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 19
Size: 266 Color: 9
Size: 263 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 7
Size: 336 Color: 10
Size: 302 Color: 16

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 3
Size: 344 Color: 14
Size: 253 Color: 11

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 15
Size: 332 Color: 14
Size: 306 Color: 6

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 304 Color: 16
Size: 250 Color: 7

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 349 Color: 9
Size: 278 Color: 13

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 6
Size: 355 Color: 2
Size: 265 Color: 8

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 12
Size: 317 Color: 15
Size: 304 Color: 4

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 4
Size: 296 Color: 2
Size: 267 Color: 9

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 6
Size: 369 Color: 9
Size: 250 Color: 19

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 19
Size: 336 Color: 6
Size: 285 Color: 13

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 291 Color: 15
Size: 255 Color: 4

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 14
Size: 281 Color: 3
Size: 280 Color: 7

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 19
Size: 308 Color: 19
Size: 259 Color: 8

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 17
Size: 262 Color: 0
Size: 253 Color: 3

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 17
Size: 290 Color: 8
Size: 258 Color: 9

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 8
Size: 345 Color: 10
Size: 264 Color: 14

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 326 Color: 12
Size: 283 Color: 17

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 2
Size: 331 Color: 14
Size: 272 Color: 17

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 2
Size: 339 Color: 7
Size: 312 Color: 16

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 15
Size: 294 Color: 12
Size: 262 Color: 7

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 4
Size: 355 Color: 12
Size: 270 Color: 4

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 15
Size: 373 Color: 11
Size: 251 Color: 19

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 16
Size: 336 Color: 10
Size: 320 Color: 2

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 271 Color: 17
Size: 266 Color: 11

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 14
Size: 323 Color: 3
Size: 306 Color: 3

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 11
Size: 366 Color: 13
Size: 265 Color: 16

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 19
Size: 341 Color: 5
Size: 282 Color: 5

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 14
Size: 263 Color: 12
Size: 262 Color: 6

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 18
Size: 342 Color: 10
Size: 298 Color: 17

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 5
Size: 333 Color: 9
Size: 304 Color: 9

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 2
Size: 277 Color: 15
Size: 272 Color: 8

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 320 Color: 17
Size: 303 Color: 9

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 308 Color: 8
Size: 303 Color: 6

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 339 Color: 0
Size: 286 Color: 6

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 8
Size: 301 Color: 19
Size: 292 Color: 4

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 9
Size: 262 Color: 6
Size: 255 Color: 12

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 3
Size: 257 Color: 18
Size: 256 Color: 8

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 8
Size: 299 Color: 10
Size: 286 Color: 19

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 15
Size: 306 Color: 12
Size: 277 Color: 4

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 8
Size: 275 Color: 11
Size: 262 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 9
Size: 260 Color: 16
Size: 256 Color: 14

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 9
Size: 261 Color: 11
Size: 251 Color: 8

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 19
Size: 273 Color: 12
Size: 271 Color: 3

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 19
Size: 292 Color: 11
Size: 254 Color: 16

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 18
Size: 338 Color: 4
Size: 311 Color: 16

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 500 Color: 18

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 364 Color: 9
Size: 257 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 13
Size: 320 Color: 6
Size: 295 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 7
Size: 325 Color: 4
Size: 265 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 259 Color: 5
Size: 255 Color: 10

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 4
Size: 341 Color: 2
Size: 299 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 317 Color: 15
Size: 313 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 9
Size: 344 Color: 14
Size: 269 Color: 5

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 12
Size: 281 Color: 14
Size: 254 Color: 2

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 10
Size: 351 Color: 8
Size: 288 Color: 18

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 17
Size: 268 Color: 7
Size: 260 Color: 10

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 355 Color: 6
Size: 269 Color: 1

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 15
Size: 343 Color: 2
Size: 250 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 15
Size: 255 Color: 2
Size: 253 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 266 Color: 8
Size: 251 Color: 11

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 5
Size: 343 Color: 19
Size: 268 Color: 17

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 17
Size: 270 Color: 11
Size: 259 Color: 9

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 19
Size: 288 Color: 1
Size: 253 Color: 3

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 19
Size: 257 Color: 18
Size: 252 Color: 13

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 11
Size: 320 Color: 5
Size: 252 Color: 17

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 15
Size: 356 Color: 18
Size: 286 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 16
Size: 280 Color: 16
Size: 255 Color: 15

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 361 Color: 11
Size: 260 Color: 9

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 7
Size: 335 Color: 11
Size: 277 Color: 8

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 15
Size: 319 Color: 7
Size: 314 Color: 4

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 7
Size: 264 Color: 10
Size: 261 Color: 2

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 9
Size: 305 Color: 4
Size: 264 Color: 3

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 15
Size: 306 Color: 3
Size: 286 Color: 3

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 2
Size: 367 Color: 14
Size: 259 Color: 15

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 5
Size: 333 Color: 14
Size: 325 Color: 18

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 10
Size: 317 Color: 19
Size: 269 Color: 16

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 313 Color: 5
Size: 251 Color: 14

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 3
Size: 261 Color: 17
Size: 253 Color: 9

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 5
Size: 281 Color: 15
Size: 256 Color: 17

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 19
Size: 260 Color: 13
Size: 260 Color: 3

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 9
Size: 298 Color: 12
Size: 296 Color: 15

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 2
Size: 287 Color: 0
Size: 262 Color: 4

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 19
Size: 308 Color: 12
Size: 269 Color: 14

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 3
Size: 274 Color: 15
Size: 259 Color: 5

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 13
Size: 317 Color: 5
Size: 303 Color: 12

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 17
Size: 295 Color: 3
Size: 286 Color: 14

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 10
Size: 273 Color: 10
Size: 265 Color: 7

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 17
Size: 312 Color: 9
Size: 260 Color: 17

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 11
Size: 251 Color: 2

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 321 Color: 18
Size: 282 Color: 2

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 17
Size: 308 Color: 14
Size: 281 Color: 15

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 14
Size: 318 Color: 0
Size: 293 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 3
Size: 342 Color: 16
Size: 303 Color: 7

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 7
Size: 291 Color: 10
Size: 281 Color: 7

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 362 Color: 2
Size: 256 Color: 9

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 7
Size: 312 Color: 15
Size: 307 Color: 18

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 18
Size: 310 Color: 12
Size: 308 Color: 4

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 18
Size: 274 Color: 1
Size: 272 Color: 2

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 298 Color: 10
Size: 294 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 7
Size: 289 Color: 13
Size: 252 Color: 19

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 5
Size: 348 Color: 0
Size: 286 Color: 7

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 352 Color: 10
Size: 282 Color: 4

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 19
Size: 324 Color: 7
Size: 277 Color: 7

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 319 Color: 13
Size: 266 Color: 2

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 283 Color: 16
Size: 282 Color: 16

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 9
Size: 326 Color: 5
Size: 262 Color: 10

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 352 Color: 12
Size: 268 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 3
Size: 353 Color: 16
Size: 260 Color: 8

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 9
Size: 344 Color: 10
Size: 267 Color: 16

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 9
Size: 324 Color: 6
Size: 262 Color: 19

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 2
Size: 279 Color: 13
Size: 258 Color: 11

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 291 Color: 11
Size: 261 Color: 9

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 16
Size: 337 Color: 9
Size: 310 Color: 16

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9
Size: 266 Color: 6
Size: 265 Color: 19

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 356 Color: 9
Size: 281 Color: 3

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 16
Size: 359 Color: 4
Size: 251 Color: 17

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 300 Color: 0
Size: 297 Color: 2

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 10
Size: 322 Color: 7
Size: 303 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 5
Size: 351 Color: 7
Size: 268 Color: 13

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 5
Size: 301 Color: 0
Size: 268 Color: 7

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 14
Size: 323 Color: 2
Size: 317 Color: 19

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 15
Size: 273 Color: 1
Size: 251 Color: 4

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 15
Size: 345 Color: 14
Size: 287 Color: 14

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 8
Size: 336 Color: 19
Size: 318 Color: 2

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 272 Color: 3
Size: 259 Color: 8

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 4
Size: 294 Color: 0
Size: 263 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 291 Color: 2
Size: 287 Color: 19

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 18
Size: 258 Color: 7
Size: 251 Color: 14

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 9
Size: 355 Color: 15
Size: 270 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 17
Size: 287 Color: 17
Size: 253 Color: 8

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 16
Size: 269 Color: 3
Size: 260 Color: 3

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 18
Size: 338 Color: 4
Size: 323 Color: 11

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 11
Size: 357 Color: 6
Size: 262 Color: 5

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 5
Size: 332 Color: 19
Size: 308 Color: 12

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 17
Size: 279 Color: 6
Size: 278 Color: 17

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 290 Color: 19
Size: 250 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 6
Size: 277 Color: 7
Size: 260 Color: 6

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 320 Color: 4
Size: 298 Color: 19

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 4
Size: 312 Color: 3
Size: 280 Color: 1

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 2
Size: 333 Color: 17
Size: 271 Color: 13

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 3
Size: 323 Color: 11
Size: 251 Color: 5

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 327 Color: 18
Size: 285 Color: 8

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 14
Size: 320 Color: 6
Size: 313 Color: 16

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 262 Color: 9
Size: 256 Color: 2

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 16
Size: 252 Color: 13
Size: 250 Color: 5

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 18
Size: 331 Color: 11
Size: 313 Color: 9

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 11
Size: 329 Color: 0
Size: 278 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 16
Size: 345 Color: 8
Size: 298 Color: 18

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 4
Size: 339 Color: 6
Size: 285 Color: 3

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 337 Color: 15
Size: 254 Color: 18

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 6
Size: 327 Color: 8
Size: 295 Color: 8

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 7
Size: 321 Color: 1
Size: 319 Color: 9

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 9
Size: 369 Color: 11
Size: 250 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 8
Size: 352 Color: 5
Size: 255 Color: 17

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 7
Size: 293 Color: 4
Size: 259 Color: 2

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 18
Size: 272 Color: 2
Size: 250 Color: 4

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 2
Size: 257 Color: 4
Size: 256 Color: 17

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 5
Size: 333 Color: 3
Size: 273 Color: 2

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 7
Size: 306 Color: 15
Size: 277 Color: 9

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 0
Size: 316 Color: 2
Size: 275 Color: 4

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 9
Size: 307 Color: 13
Size: 269 Color: 9

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 11
Size: 299 Color: 17
Size: 275 Color: 10

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 0
Size: 283 Color: 15
Size: 260 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 3
Size: 333 Color: 0
Size: 289 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 10
Size: 309 Color: 17
Size: 303 Color: 6

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 11
Size: 327 Color: 12
Size: 251 Color: 15

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 7
Size: 327 Color: 14
Size: 267 Color: 14

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 8
Size: 350 Color: 17
Size: 265 Color: 13

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 19
Size: 339 Color: 19
Size: 271 Color: 4

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 14
Size: 323 Color: 14
Size: 321 Color: 10

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 12
Size: 299 Color: 4
Size: 265 Color: 11

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 304 Color: 16
Size: 300 Color: 10

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 292 Color: 17
Size: 286 Color: 13

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 14
Size: 308 Color: 0
Size: 292 Color: 16

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 3
Size: 337 Color: 5
Size: 278 Color: 11

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 13
Size: 256 Color: 15
Size: 255 Color: 3

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 363 Color: 17
Size: 265 Color: 14

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 12
Size: 265 Color: 6
Size: 256 Color: 19

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 12
Size: 260 Color: 0
Size: 251 Color: 19

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 310 Color: 0
Size: 307 Color: 11

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 331 Color: 5
Size: 256 Color: 16

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 9
Size: 298 Color: 4
Size: 274 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 15
Size: 340 Color: 19
Size: 277 Color: 12

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 8
Size: 286 Color: 16
Size: 280 Color: 15

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 7
Size: 309 Color: 15
Size: 264 Color: 12

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 15
Size: 336 Color: 5
Size: 257 Color: 19

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 18
Size: 308 Color: 7
Size: 252 Color: 2

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 6
Size: 304 Color: 10
Size: 286 Color: 18

Bin 407: 0 of cap free
Amount of items: 4
Items: 
Size: 251 Color: 3
Size: 250 Color: 16
Size: 250 Color: 6
Size: 250 Color: 4

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 11
Size: 324 Color: 9
Size: 254 Color: 3

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 9
Size: 341 Color: 9
Size: 261 Color: 18

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 19
Size: 321 Color: 7
Size: 290 Color: 19

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 10
Size: 316 Color: 19
Size: 289 Color: 14

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 0
Size: 347 Color: 12
Size: 296 Color: 15

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 319 Color: 2
Size: 288 Color: 9

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 295 Color: 5
Size: 257 Color: 9

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 6
Size: 343 Color: 19
Size: 273 Color: 19

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 17
Size: 324 Color: 15
Size: 308 Color: 18

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 18
Size: 302 Color: 8
Size: 252 Color: 5

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 309 Color: 9
Size: 289 Color: 13

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 288 Color: 19
Size: 279 Color: 18

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 342 Color: 19
Size: 299 Color: 12

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 0
Size: 323 Color: 7
Size: 264 Color: 17

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 8
Size: 322 Color: 4
Size: 309 Color: 6

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 4
Size: 267 Color: 4
Size: 263 Color: 6

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 11
Size: 283 Color: 0
Size: 268 Color: 7

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 15
Size: 298 Color: 13
Size: 260 Color: 2

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 16
Size: 259 Color: 11
Size: 259 Color: 8

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 17
Size: 250 Color: 11
Size: 250 Color: 3

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 261 Color: 12
Size: 252 Color: 6

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 9
Size: 358 Color: 2
Size: 280 Color: 12

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 10
Size: 315 Color: 17
Size: 290 Color: 9

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 8
Size: 342 Color: 8
Size: 263 Color: 16

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 18
Size: 361 Color: 16
Size: 263 Color: 16

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 5
Size: 337 Color: 8
Size: 258 Color: 2

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 4
Size: 251 Color: 15
Size: 250 Color: 8

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 285 Color: 17
Size: 256 Color: 4

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 7
Size: 303 Color: 9
Size: 260 Color: 19

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 15
Size: 250 Color: 19
Size: 250 Color: 15

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 10
Size: 362 Color: 7
Size: 262 Color: 16

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 2
Size: 298 Color: 17
Size: 267 Color: 9

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 2
Size: 331 Color: 17
Size: 250 Color: 10

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 11
Size: 329 Color: 17
Size: 263 Color: 12

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 19
Size: 353 Color: 1
Size: 277 Color: 17

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 14
Size: 289 Color: 19
Size: 276 Color: 4

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 10
Size: 335 Color: 0
Size: 288 Color: 6

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 15
Size: 333 Color: 15
Size: 279 Color: 11

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 322 Color: 11
Size: 253 Color: 17

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 309 Color: 8
Size: 305 Color: 6

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 2
Size: 258 Color: 19
Size: 253 Color: 2

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 6
Size: 307 Color: 11
Size: 266 Color: 15

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 16
Size: 291 Color: 13
Size: 277 Color: 17

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 17
Size: 319 Color: 11
Size: 308 Color: 18

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 16
Size: 280 Color: 13
Size: 263 Color: 13

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 10
Size: 282 Color: 16
Size: 267 Color: 17

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 13
Size: 330 Color: 18
Size: 292 Color: 3

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 263 Color: 1
Size: 250 Color: 19

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 16
Size: 296 Color: 16
Size: 264 Color: 18

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 19
Size: 308 Color: 0
Size: 254 Color: 11

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 323 Color: 8
Size: 297 Color: 7

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 3
Size: 285 Color: 19
Size: 250 Color: 1

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 14
Size: 322 Color: 15
Size: 296 Color: 17

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 0
Size: 292 Color: 15
Size: 281 Color: 3

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 0
Size: 361 Color: 13
Size: 267 Color: 14

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 4
Size: 329 Color: 16
Size: 253 Color: 16

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 10
Size: 274 Color: 8
Size: 269 Color: 12

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 19
Size: 288 Color: 18
Size: 266 Color: 3

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 12
Size: 298 Color: 10
Size: 276 Color: 10

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 13
Size: 312 Color: 16
Size: 275 Color: 11

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 14
Size: 273 Color: 18
Size: 270 Color: 9

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 17
Size: 276 Color: 7
Size: 262 Color: 10

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 16
Size: 338 Color: 11
Size: 253 Color: 13

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 7
Size: 347 Color: 18
Size: 305 Color: 17

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 19
Size: 318 Color: 2
Size: 258 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 5
Size: 313 Color: 19
Size: 292 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 19
Size: 313 Color: 10
Size: 250 Color: 2

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 17
Size: 323 Color: 1
Size: 293 Color: 3

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 333 Color: 13
Size: 311 Color: 7

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 17
Size: 294 Color: 5
Size: 278 Color: 9

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 16
Size: 297 Color: 17
Size: 285 Color: 8

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 361 Color: 13
Size: 258 Color: 19

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 9
Size: 316 Color: 9
Size: 277 Color: 5

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 11
Size: 337 Color: 18
Size: 264 Color: 2

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 15
Size: 335 Color: 7
Size: 309 Color: 1

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 326 Color: 14
Size: 279 Color: 9

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 8
Size: 337 Color: 1
Size: 311 Color: 4

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 11
Size: 337 Color: 17
Size: 281 Color: 2

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 16
Size: 288 Color: 13
Size: 284 Color: 11

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 16
Size: 363 Color: 11
Size: 256 Color: 9

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 347 Color: 10
Size: 277 Color: 17

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 275 Color: 3
Size: 256 Color: 14

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 8
Size: 341 Color: 6
Size: 257 Color: 19

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 18
Size: 320 Color: 15
Size: 254 Color: 2

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 13
Size: 319 Color: 16
Size: 307 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 18
Size: 315 Color: 4
Size: 264 Color: 15

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 18
Size: 304 Color: 1
Size: 255 Color: 3

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 2
Size: 303 Color: 8
Size: 269 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 14
Size: 355 Color: 7
Size: 291 Color: 4

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7
Size: 306 Color: 13
Size: 298 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 12
Size: 357 Color: 8
Size: 283 Color: 13

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 364 Color: 11
Size: 267 Color: 2

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 9
Size: 303 Color: 13
Size: 273 Color: 3

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 5
Size: 272 Color: 6
Size: 254 Color: 5

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 10
Size: 323 Color: 18
Size: 308 Color: 12

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 17
Size: 339 Color: 12
Size: 280 Color: 5

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 11
Size: 336 Color: 19
Size: 307 Color: 6

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 12
Size: 337 Color: 4
Size: 268 Color: 12

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 2
Size: 333 Color: 3
Size: 311 Color: 5

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 14
Size: 323 Color: 19
Size: 322 Color: 6

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 3
Size: 331 Color: 5
Size: 308 Color: 19

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 346 Color: 16
Size: 292 Color: 5

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 10
Size: 290 Color: 16
Size: 290 Color: 11

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 0
Size: 286 Color: 1
Size: 285 Color: 6

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 9
Size: 314 Color: 6
Size: 256 Color: 3

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 10
Size: 321 Color: 18
Size: 281 Color: 19

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 18
Size: 330 Color: 4
Size: 304 Color: 7

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 336 Color: 3
Size: 269 Color: 10

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 13
Size: 286 Color: 14
Size: 270 Color: 14

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 13
Size: 272 Color: 9
Size: 266 Color: 13

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 4
Size: 331 Color: 17
Size: 321 Color: 8

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 4
Size: 319 Color: 9
Size: 262 Color: 5

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 10
Size: 330 Color: 4
Size: 292 Color: 16

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 18
Size: 270 Color: 12
Size: 252 Color: 4

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 8
Size: 350 Color: 1
Size: 253 Color: 14

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 3
Size: 316 Color: 3
Size: 277 Color: 12

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 4
Size: 332 Color: 12
Size: 332 Color: 1

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 13
Size: 353 Color: 10
Size: 270 Color: 10

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 11
Size: 334 Color: 16
Size: 309 Color: 5

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 6
Size: 303 Color: 5
Size: 269 Color: 10

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 12
Size: 332 Color: 2
Size: 263 Color: 8

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 342 Color: 12
Size: 250 Color: 4

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 14
Size: 322 Color: 5
Size: 285 Color: 11

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 3
Size: 277 Color: 13
Size: 250 Color: 17

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 6
Size: 366 Color: 13
Size: 255 Color: 8

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 7
Size: 331 Color: 0
Size: 305 Color: 16

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 6
Size: 368 Color: 2
Size: 263 Color: 8

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 6
Size: 352 Color: 17
Size: 276 Color: 14

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 19
Size: 307 Color: 13
Size: 295 Color: 13

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 15
Size: 309 Color: 11
Size: 278 Color: 10

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 3
Size: 320 Color: 0
Size: 258 Color: 16

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 4
Size: 256 Color: 19
Size: 255 Color: 9

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 276 Color: 15
Size: 275 Color: 10

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 3
Size: 301 Color: 4
Size: 251 Color: 18

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 351 Color: 18
Size: 258 Color: 1

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 13
Size: 296 Color: 13
Size: 270 Color: 17

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 275 Color: 16
Size: 269 Color: 1

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 18
Size: 298 Color: 10
Size: 273 Color: 2

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 18
Size: 265 Color: 10
Size: 255 Color: 1

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 13
Size: 277 Color: 9
Size: 273 Color: 1

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 16
Size: 265 Color: 13
Size: 257 Color: 1

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 14
Size: 326 Color: 2
Size: 264 Color: 19

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 7
Size: 330 Color: 10
Size: 257 Color: 12

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 0
Size: 329 Color: 9
Size: 308 Color: 16

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 16
Size: 270 Color: 13
Size: 259 Color: 13

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 9
Size: 308 Color: 18
Size: 266 Color: 2

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 7
Size: 319 Color: 2
Size: 296 Color: 18

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 11
Size: 334 Color: 11
Size: 277 Color: 6

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 8
Size: 319 Color: 17
Size: 260 Color: 11

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 11
Size: 326 Color: 3
Size: 257 Color: 16

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 352 Color: 4
Size: 277 Color: 8

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 7
Size: 317 Color: 2
Size: 253 Color: 8

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 18
Size: 350 Color: 14
Size: 282 Color: 7

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 333 Color: 15
Size: 302 Color: 2

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 8
Size: 331 Color: 15
Size: 300 Color: 8

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 19
Size: 293 Color: 9
Size: 266 Color: 14

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 308 Color: 4
Size: 271 Color: 13

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 13
Size: 350 Color: 5
Size: 297 Color: 13

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 15
Size: 312 Color: 16
Size: 250 Color: 9

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 7
Size: 358 Color: 10
Size: 256 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 7
Size: 339 Color: 1
Size: 287 Color: 5

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 12
Size: 335 Color: 17
Size: 289 Color: 7

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 15
Size: 302 Color: 15
Size: 271 Color: 5

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 14
Size: 341 Color: 6
Size: 302 Color: 16

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 7
Size: 265 Color: 0
Size: 252 Color: 16

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 11
Size: 355 Color: 10
Size: 276 Color: 4

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 3
Size: 347 Color: 0
Size: 254 Color: 9

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 3
Size: 332 Color: 15
Size: 299 Color: 0

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 6
Size: 304 Color: 0
Size: 297 Color: 9

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 18
Size: 328 Color: 14
Size: 285 Color: 4

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 283 Color: 7
Size: 253 Color: 9

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 16
Size: 353 Color: 18
Size: 252 Color: 17

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 10
Size: 284 Color: 19
Size: 255 Color: 13

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 2
Size: 283 Color: 19
Size: 263 Color: 9

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 18
Size: 320 Color: 4
Size: 302 Color: 19

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 9
Size: 332 Color: 10
Size: 311 Color: 16

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 0
Size: 293 Color: 11
Size: 253 Color: 3

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 8
Size: 344 Color: 4
Size: 292 Color: 1

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 7
Size: 372 Color: 14
Size: 253 Color: 8

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 6
Size: 340 Color: 14
Size: 253 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 5
Size: 309 Color: 9
Size: 298 Color: 8

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 7
Size: 282 Color: 10
Size: 262 Color: 13

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 9
Size: 296 Color: 19
Size: 291 Color: 5

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 13
Size: 336 Color: 11
Size: 284 Color: 7

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 8
Size: 357 Color: 16
Size: 256 Color: 17

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 9
Size: 313 Color: 4
Size: 268 Color: 3

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 282 Color: 9
Size: 273 Color: 11

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 3
Size: 305 Color: 9
Size: 272 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 19
Size: 297 Color: 16
Size: 265 Color: 4

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 10
Size: 290 Color: 6
Size: 252 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 11
Size: 354 Color: 15
Size: 250 Color: 14

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 267 Color: 15
Size: 250 Color: 7

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 2
Size: 299 Color: 10
Size: 262 Color: 13

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 14
Size: 329 Color: 15
Size: 262 Color: 11

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 10
Size: 258 Color: 14
Size: 258 Color: 11

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 11
Size: 310 Color: 7
Size: 250 Color: 9

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 5
Size: 305 Color: 11
Size: 281 Color: 3

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 279 Color: 4
Size: 276 Color: 14

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 10
Size: 292 Color: 10
Size: 262 Color: 18

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 12
Size: 326 Color: 18
Size: 305 Color: 12

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 7
Size: 281 Color: 6
Size: 276 Color: 19

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 7
Size: 342 Color: 1
Size: 293 Color: 15

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 18
Size: 332 Color: 1
Size: 267 Color: 3

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 7
Size: 341 Color: 11
Size: 264 Color: 8

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 19
Size: 323 Color: 0
Size: 263 Color: 11

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 7
Size: 320 Color: 2
Size: 255 Color: 7

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 18
Size: 323 Color: 1
Size: 269 Color: 19

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 325 Color: 1
Size: 301 Color: 14

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 3
Size: 353 Color: 7
Size: 282 Color: 1

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 337 Color: 0
Size: 257 Color: 1

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 17
Size: 270 Color: 9
Size: 266 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 322 Color: 10
Size: 304 Color: 3

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 289 Color: 7
Size: 255 Color: 7

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 3
Size: 342 Color: 0
Size: 269 Color: 10

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 5
Size: 338 Color: 8
Size: 263 Color: 11

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 14
Size: 322 Color: 17
Size: 282 Color: 1

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 17
Size: 252 Color: 13
Size: 250 Color: 16

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 5
Size: 348 Color: 6
Size: 287 Color: 4

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 12
Size: 329 Color: 9
Size: 267 Color: 19

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 4
Size: 252 Color: 19
Size: 250 Color: 3

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 7
Size: 315 Color: 3
Size: 271 Color: 9

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 264 Color: 19
Size: 250 Color: 17

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 9
Size: 325 Color: 3
Size: 307 Color: 4

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 19
Size: 275 Color: 6
Size: 257 Color: 2

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 15
Size: 308 Color: 2
Size: 252 Color: 3

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 272 Color: 5
Size: 269 Color: 2

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 12
Size: 269 Color: 4
Size: 260 Color: 13

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 304 Color: 9
Size: 289 Color: 5

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 4
Size: 331 Color: 8
Size: 321 Color: 5

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 14
Size: 261 Color: 13
Size: 256 Color: 3

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 6
Size: 303 Color: 5
Size: 281 Color: 7

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 2
Size: 363 Color: 6
Size: 272 Color: 15

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 14
Size: 325 Color: 10
Size: 307 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 4
Size: 292 Color: 7
Size: 269 Color: 11

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 19
Size: 309 Color: 17
Size: 308 Color: 13

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 279 Color: 5
Size: 257 Color: 8

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 12
Size: 265 Color: 17
Size: 259 Color: 6

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 12
Size: 345 Color: 13
Size: 252 Color: 5

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 8
Size: 281 Color: 0
Size: 279 Color: 11

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 7
Size: 262 Color: 18
Size: 255 Color: 7

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 281 Color: 1
Size: 253 Color: 5

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 11
Size: 267 Color: 5
Size: 260 Color: 6

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 303 Color: 5
Size: 297 Color: 12

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 6
Size: 302 Color: 18
Size: 265 Color: 5

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 5
Size: 330 Color: 6
Size: 257 Color: 17

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 5
Size: 293 Color: 6
Size: 252 Color: 11

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 314 Color: 4
Size: 250 Color: 8

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 8
Size: 326 Color: 6
Size: 271 Color: 7

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 3
Size: 322 Color: 11
Size: 283 Color: 16

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 8
Size: 297 Color: 11
Size: 286 Color: 1

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 314 Color: 19
Size: 254 Color: 2

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 6
Size: 272 Color: 12
Size: 262 Color: 14

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 14
Size: 282 Color: 19
Size: 259 Color: 8

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 3
Size: 295 Color: 12
Size: 258 Color: 5

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 14
Size: 254 Color: 19
Size: 252 Color: 15

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 14
Size: 315 Color: 4
Size: 276 Color: 12

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 15
Size: 333 Color: 9
Size: 253 Color: 9

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 19
Size: 283 Color: 19
Size: 274 Color: 6

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 271 Color: 14
Size: 271 Color: 11

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 18
Size: 351 Color: 18
Size: 264 Color: 9

Total size: 667667
Total free space: 0

