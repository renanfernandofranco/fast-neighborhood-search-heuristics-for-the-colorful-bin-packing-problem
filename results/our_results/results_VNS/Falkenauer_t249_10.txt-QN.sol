Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 219
Size: 274 Color: 83
Size: 267 Color: 68

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 248
Size: 251 Color: 11
Size: 250 Color: 7

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 229
Size: 265 Color: 63
Size: 260 Color: 37

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 201
Size: 299 Color: 121
Size: 279 Color: 91

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 217
Size: 284 Color: 99
Size: 258 Color: 33

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 237
Size: 267 Color: 69
Size: 250 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 199
Size: 295 Color: 117
Size: 289 Color: 107

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 234
Size: 266 Color: 65
Size: 254 Color: 21

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 178
Size: 313 Color: 139
Size: 304 Color: 129

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 205
Size: 290 Color: 108
Size: 274 Color: 82

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 233
Size: 271 Color: 77
Size: 252 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 173
Size: 362 Color: 166
Size: 264 Color: 57

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 236
Size: 269 Color: 75
Size: 250 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 239
Size: 265 Color: 64
Size: 250 Color: 5

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 157
Size: 352 Color: 158
Size: 296 Color: 119

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 226
Size: 274 Color: 81
Size: 254 Color: 20

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 172
Size: 348 Color: 154
Size: 279 Color: 92

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 170
Size: 329 Color: 149
Size: 305 Color: 131

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 216
Size: 287 Color: 104
Size: 256 Color: 28

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 183
Size: 352 Color: 159
Size: 258 Color: 34

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 186
Size: 345 Color: 152
Size: 260 Color: 42

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 223
Size: 275 Color: 84
Size: 260 Color: 41

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 198
Size: 310 Color: 136
Size: 275 Color: 85

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 221
Size: 277 Color: 88
Size: 262 Color: 51

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 191
Size: 307 Color: 133
Size: 294 Color: 116

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 188
Size: 303 Color: 128
Size: 300 Color: 123

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 203
Size: 312 Color: 137
Size: 254 Color: 22

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 179
Size: 366 Color: 169
Size: 251 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 181
Size: 330 Color: 150
Size: 281 Color: 97

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 227
Size: 266 Color: 67
Size: 261 Color: 46

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 241
Size: 258 Color: 35
Size: 255 Color: 26

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 208
Size: 294 Color: 114
Size: 265 Color: 60

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 184
Size: 347 Color: 153
Size: 260 Color: 43

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 240
Size: 261 Color: 47
Size: 253 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 194
Size: 326 Color: 147
Size: 269 Color: 73

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 215
Size: 284 Color: 100
Size: 261 Color: 49

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 197
Size: 318 Color: 141
Size: 272 Color: 79

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 174
Size: 357 Color: 161
Size: 269 Color: 74

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 209
Size: 296 Color: 118
Size: 261 Color: 44

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 190
Size: 324 Color: 146
Size: 278 Color: 90

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 171
Size: 365 Color: 168
Size: 264 Color: 58

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 218
Size: 271 Color: 78
Size: 270 Color: 76

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 224
Size: 279 Color: 93
Size: 250 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 204
Size: 303 Color: 127
Size: 261 Color: 50

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 192
Size: 308 Color: 134
Size: 292 Color: 111

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 182
Size: 315 Color: 140
Size: 296 Color: 120

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 212
Size: 288 Color: 105
Size: 263 Color: 53

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 243
Size: 261 Color: 48
Size: 250 Color: 3

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 235
Size: 267 Color: 70
Size: 252 Color: 16

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 156
Size: 349 Color: 155
Size: 301 Color: 124

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 231
Size: 268 Color: 71
Size: 255 Color: 27

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 245
Size: 257 Color: 30
Size: 251 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 247
Size: 255 Color: 24
Size: 251 Color: 10

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 210
Size: 294 Color: 115
Size: 261 Color: 45

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 196
Size: 326 Color: 148
Size: 265 Color: 61

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 200
Size: 292 Color: 112
Size: 290 Color: 109

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 246
Size: 254 Color: 23
Size: 253 Color: 19

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 228
Size: 275 Color: 86
Size: 251 Color: 14

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 176
Size: 363 Color: 167
Size: 260 Color: 39

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 230
Size: 266 Color: 66
Size: 259 Color: 36

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 244
Size: 257 Color: 31
Size: 251 Color: 13

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 213
Size: 287 Color: 102
Size: 263 Color: 52

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 211
Size: 280 Color: 95
Size: 272 Color: 80

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 202
Size: 319 Color: 142
Size: 257 Color: 32

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 242
Size: 257 Color: 29
Size: 255 Color: 25

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 175
Size: 324 Color: 145
Size: 302 Color: 126

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 232
Size: 263 Color: 54
Size: 260 Color: 38

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 187
Size: 354 Color: 160
Size: 250 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 214
Size: 284 Color: 101
Size: 264 Color: 59

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 238
Size: 263 Color: 55
Size: 252 Color: 15

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 189
Size: 309 Color: 135
Size: 293 Color: 113

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 185
Size: 305 Color: 130
Size: 302 Color: 125

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 222
Size: 288 Color: 106
Size: 251 Color: 9

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 195
Size: 305 Color: 132
Size: 287 Color: 103

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 164
Size: 359 Color: 163
Size: 281 Color: 98

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 206
Size: 313 Color: 138
Size: 250 Color: 6

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 193
Size: 321 Color: 143
Size: 277 Color: 89

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 177
Size: 323 Color: 144
Size: 300 Color: 122

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 207
Size: 291 Color: 110
Size: 269 Color: 72

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 225
Size: 265 Color: 62
Size: 264 Color: 56

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 165
Size: 358 Color: 162
Size: 280 Color: 96

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 220
Size: 279 Color: 94
Size: 260 Color: 40

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 180
Size: 339 Color: 151
Size: 276 Color: 87

Total size: 83000
Total free space: 0

