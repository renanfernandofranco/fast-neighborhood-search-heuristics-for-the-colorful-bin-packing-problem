Capicity Bin: 1001
Lower Bound: 34

Bins used: 34
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 4
Size: 305 Color: 3
Size: 250 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 289 Color: 0
Size: 273 Color: 4
Size: 439 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 2
Size: 345 Color: 3
Size: 279 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 4
Size: 362 Color: 3
Size: 251 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 2
Size: 299 Color: 4
Size: 250 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 0
Size: 336 Color: 0
Size: 298 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 340 Color: 1
Size: 337 Color: 4
Size: 324 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 0
Size: 322 Color: 4
Size: 291 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 342 Color: 2
Size: 296 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 4
Size: 274 Color: 0
Size: 290 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 311 Color: 3
Size: 296 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 2
Size: 250 Color: 3
Size: 250 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 278 Color: 2
Size: 250 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 0
Size: 284 Color: 0
Size: 282 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 268 Color: 2
Size: 256 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 299 Color: 0
Size: 268 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 262 Color: 3
Size: 253 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 364 Color: 0
Size: 263 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 3
Size: 368 Color: 2
Size: 253 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 325 Color: 2
Size: 304 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 324 Color: 1
Size: 293 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 3
Size: 282 Color: 4
Size: 254 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 3
Size: 366 Color: 1
Size: 252 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 3
Size: 356 Color: 3
Size: 253 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 277 Color: 1
Size: 259 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 2
Size: 281 Color: 1
Size: 260 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 0
Size: 330 Color: 4
Size: 268 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 336 Color: 0
Size: 250 Color: 3

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 2
Size: 352 Color: 0
Size: 297 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 344 Color: 2
Size: 271 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 336 Color: 4
Size: 279 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 4
Size: 340 Color: 4
Size: 308 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 3
Size: 280 Color: 1
Size: 276 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 294 Color: 0
Size: 272 Color: 2
Size: 435 Color: 2

Total size: 34034
Total free space: 0

