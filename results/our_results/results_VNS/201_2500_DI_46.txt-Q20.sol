Capicity Bin: 2328
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1166 Color: 10
Size: 886 Color: 17
Size: 144 Color: 4
Size: 88 Color: 0
Size: 44 Color: 11

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1266 Color: 8
Size: 970 Color: 1
Size: 92 Color: 17

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 15
Size: 646 Color: 1
Size: 108 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 4
Size: 533 Color: 10
Size: 144 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 18
Size: 565 Color: 19
Size: 90 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 11
Size: 531 Color: 4
Size: 104 Color: 6

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 10
Size: 578 Color: 5
Size: 40 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 17
Size: 459 Color: 19
Size: 90 Color: 18

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 6
Size: 462 Color: 11
Size: 52 Color: 13

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1854 Color: 1
Size: 322 Color: 15
Size: 152 Color: 9

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1871 Color: 17
Size: 391 Color: 12
Size: 66 Color: 11

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1864 Color: 3
Size: 288 Color: 18
Size: 176 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 18
Size: 394 Color: 1
Size: 36 Color: 6

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 5
Size: 337 Color: 4
Size: 66 Color: 9

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1929 Color: 17
Size: 333 Color: 6
Size: 66 Color: 15

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 9
Size: 246 Color: 19
Size: 128 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 302 Color: 0
Size: 56 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1994 Color: 17
Size: 282 Color: 8
Size: 52 Color: 14

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1999 Color: 16
Size: 275 Color: 5
Size: 54 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 15
Size: 202 Color: 17
Size: 112 Color: 2

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 12
Size: 156 Color: 1
Size: 94 Color: 6

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2086 Color: 3
Size: 184 Color: 17
Size: 58 Color: 12

Bin 23: 1 of cap free
Amount of items: 9
Items: 
Size: 553 Color: 1
Size: 455 Color: 6
Size: 348 Color: 17
Size: 323 Color: 2
Size: 258 Color: 12
Size: 210 Color: 8
Size: 76 Color: 10
Size: 64 Color: 4
Size: 40 Color: 11

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 15
Size: 926 Color: 16
Size: 76 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 16
Size: 829 Color: 9
Size: 128 Color: 9

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 12
Size: 723 Color: 11
Size: 198 Color: 16

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 19
Size: 741 Color: 1
Size: 40 Color: 13

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 10
Size: 537 Color: 13
Size: 96 Color: 17

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1787 Color: 18
Size: 448 Color: 11
Size: 92 Color: 6

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1858 Color: 6
Size: 469 Color: 5

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1861 Color: 7
Size: 466 Color: 12

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 17
Size: 341 Color: 4
Size: 64 Color: 7

Bin 33: 2 of cap free
Amount of items: 18
Items: 
Size: 192 Color: 2
Size: 192 Color: 1
Size: 188 Color: 12
Size: 166 Color: 16
Size: 164 Color: 7
Size: 156 Color: 17
Size: 144 Color: 5
Size: 128 Color: 17
Size: 128 Color: 11
Size: 124 Color: 8
Size: 124 Color: 6
Size: 120 Color: 18
Size: 110 Color: 14
Size: 106 Color: 11
Size: 104 Color: 13
Size: 68 Color: 1
Size: 64 Color: 19
Size: 48 Color: 4

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1573 Color: 7
Size: 561 Color: 17
Size: 192 Color: 2

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 5
Size: 478 Color: 7
Size: 90 Color: 15

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 451 Color: 7
Size: 76 Color: 19

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 8
Size: 947 Color: 13
Size: 56 Color: 2

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1461 Color: 2
Size: 864 Color: 12

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 15
Size: 802 Color: 7
Size: 56 Color: 0

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 3
Size: 770 Color: 8

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 3
Size: 534 Color: 17
Size: 106 Color: 8

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 1778 Color: 17
Size: 471 Color: 10
Size: 76 Color: 5

Bin 43: 3 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 11
Size: 346 Color: 1

Bin 44: 3 of cap free
Amount of items: 2
Items: 
Size: 2034 Color: 18
Size: 291 Color: 19

Bin 45: 4 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 15
Size: 835 Color: 15
Size: 324 Color: 18

Bin 46: 4 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 18
Size: 850 Color: 15
Size: 28 Color: 1

Bin 47: 4 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 17
Size: 441 Color: 2
Size: 28 Color: 7

Bin 48: 4 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 5
Size: 226 Color: 10
Size: 4 Color: 10

Bin 49: 5 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 7
Size: 362 Color: 19
Size: 20 Color: 19

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 1942 Color: 13
Size: 381 Color: 11

Bin 51: 6 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 18
Size: 1016 Color: 13
Size: 88 Color: 2

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1690 Color: 9
Size: 631 Color: 18

Bin 53: 7 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 13
Size: 538 Color: 2

Bin 54: 8 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 3
Size: 738 Color: 8
Size: 28 Color: 8

Bin 55: 8 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 5
Size: 654 Color: 8

Bin 56: 8 of cap free
Amount of items: 2
Items: 
Size: 2022 Color: 0
Size: 298 Color: 2

Bin 57: 8 of cap free
Amount of items: 2
Items: 
Size: 2058 Color: 14
Size: 262 Color: 0

Bin 58: 9 of cap free
Amount of items: 2
Items: 
Size: 1167 Color: 13
Size: 1152 Color: 16

Bin 59: 9 of cap free
Amount of items: 2
Items: 
Size: 1689 Color: 6
Size: 630 Color: 4

Bin 60: 9 of cap free
Amount of items: 2
Items: 
Size: 1765 Color: 15
Size: 554 Color: 5

Bin 61: 9 of cap free
Amount of items: 2
Items: 
Size: 1921 Color: 10
Size: 398 Color: 3

Bin 62: 12 of cap free
Amount of items: 2
Items: 
Size: 1974 Color: 6
Size: 342 Color: 0

Bin 63: 18 of cap free
Amount of items: 2
Items: 
Size: 1665 Color: 11
Size: 645 Color: 9

Bin 64: 22 of cap free
Amount of items: 2
Items: 
Size: 1335 Color: 14
Size: 971 Color: 6

Bin 65: 32 of cap free
Amount of items: 2
Items: 
Size: 1327 Color: 7
Size: 969 Color: 12

Bin 66: 2088 of cap free
Amount of items: 4
Items: 
Size: 104 Color: 18
Size: 48 Color: 13
Size: 48 Color: 3
Size: 40 Color: 4

Total size: 151320
Total free space: 2328

