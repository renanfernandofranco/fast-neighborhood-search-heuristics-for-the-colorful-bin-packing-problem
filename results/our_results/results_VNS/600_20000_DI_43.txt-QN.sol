Capicity Bin: 16288
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 2
Items: 
Size: 10240 Color: 436
Size: 6048 Color: 387

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 481
Size: 2050 Color: 253
Size: 2044 Color: 252

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12200 Color: 482
Size: 3776 Color: 334
Size: 312 Color: 28

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 485
Size: 3524 Color: 328
Size: 368 Color: 51

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12428 Color: 487
Size: 3216 Color: 316
Size: 644 Color: 129

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12568 Color: 492
Size: 3440 Color: 325
Size: 280 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12798 Color: 497
Size: 2818 Color: 303
Size: 672 Color: 132

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12799 Color: 498
Size: 2305 Color: 273
Size: 1184 Color: 187

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12804 Color: 499
Size: 2908 Color: 305
Size: 576 Color: 116

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12924 Color: 502
Size: 1964 Color: 245
Size: 1400 Color: 203

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13016 Color: 504
Size: 1816 Color: 234
Size: 1456 Color: 208

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13116 Color: 508
Size: 2020 Color: 250
Size: 1152 Color: 183

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 512
Size: 1588 Color: 219
Size: 1508 Color: 213

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 514
Size: 2208 Color: 264
Size: 784 Color: 146

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13322 Color: 518
Size: 2832 Color: 304
Size: 134 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13394 Color: 523
Size: 2210 Color: 265
Size: 684 Color: 134

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 526
Size: 2324 Color: 276
Size: 464 Color: 87

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13514 Color: 527
Size: 1886 Color: 240
Size: 888 Color: 159

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13524 Color: 528
Size: 2108 Color: 258
Size: 656 Color: 131

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13616 Color: 533
Size: 2188 Color: 262
Size: 484 Color: 94

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13626 Color: 534
Size: 2212 Color: 266
Size: 450 Color: 84

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13706 Color: 541
Size: 1884 Color: 239
Size: 698 Color: 135

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13712 Color: 542
Size: 2284 Color: 271
Size: 292 Color: 23

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 547
Size: 1562 Color: 216
Size: 908 Color: 161

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 554
Size: 2008 Color: 248
Size: 384 Color: 58

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13900 Color: 555
Size: 1620 Color: 222
Size: 768 Color: 145

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13984 Color: 559
Size: 1824 Color: 237
Size: 480 Color: 89

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 561
Size: 1902 Color: 241
Size: 376 Color: 54

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14028 Color: 563
Size: 1700 Color: 227
Size: 560 Color: 112

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14110 Color: 567
Size: 1474 Color: 212
Size: 704 Color: 140

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 570
Size: 1468 Color: 211
Size: 634 Color: 126

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 572
Size: 1630 Color: 223
Size: 456 Color: 85

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 580
Size: 1656 Color: 224
Size: 272 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14366 Color: 581
Size: 1602 Color: 220
Size: 320 Color: 32

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 582
Size: 1356 Color: 199
Size: 544 Color: 110

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14416 Color: 583
Size: 1512 Color: 214
Size: 360 Color: 50

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14418 Color: 584
Size: 1428 Color: 206
Size: 442 Color: 80

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14484 Color: 587
Size: 1404 Color: 204
Size: 400 Color: 65

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14486 Color: 588
Size: 1426 Color: 205
Size: 376 Color: 55

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 589
Size: 1608 Color: 221
Size: 176 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 590
Size: 1344 Color: 193
Size: 422 Color: 75

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 591
Size: 1464 Color: 210
Size: 288 Color: 20

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 594
Size: 1008 Color: 173
Size: 704 Color: 138

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14578 Color: 595
Size: 1382 Color: 202
Size: 328 Color: 36

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14580 Color: 596
Size: 1036 Color: 175
Size: 672 Color: 133

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 597
Size: 1194 Color: 189
Size: 482 Color: 93

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14634 Color: 599
Size: 1304 Color: 191
Size: 350 Color: 43

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14644 Color: 600
Size: 1336 Color: 192
Size: 308 Color: 27

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12567 Color: 491
Size: 3416 Color: 323
Size: 304 Color: 25

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12910 Color: 500
Size: 2801 Color: 301
Size: 576 Color: 115

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13017 Color: 505
Size: 2726 Color: 298
Size: 544 Color: 107

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 517
Size: 2971 Color: 310

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 524
Size: 2482 Color: 286
Size: 408 Color: 66

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 13400 Color: 525
Size: 2411 Color: 278
Size: 476 Color: 88

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 13577 Color: 531
Size: 2152 Color: 260
Size: 558 Color: 111

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13601 Color: 532
Size: 1742 Color: 229
Size: 944 Color: 166

Bin 57: 1 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 548
Size: 2457 Color: 284

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13921 Color: 556
Size: 1442 Color: 207
Size: 924 Color: 164

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 14026 Color: 562
Size: 2261 Color: 270

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 573
Size: 2067 Color: 256

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 10328 Color: 437
Size: 5958 Color: 384

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 463
Size: 3612 Color: 331
Size: 1168 Color: 184

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 12422 Color: 486
Size: 2804 Color: 302
Size: 1060 Color: 178

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 12912 Color: 501
Size: 3374 Color: 321

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 13313 Color: 515
Size: 2413 Color: 279
Size: 560 Color: 113

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 522
Size: 2597 Color: 292
Size: 296 Color: 24

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 13636 Color: 537
Size: 2650 Color: 297

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 13878 Color: 553
Size: 2408 Color: 277

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 13978 Color: 558
Size: 2308 Color: 274

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 14224 Color: 574
Size: 2062 Color: 255

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 14334 Color: 578
Size: 1952 Color: 244

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 14562 Color: 593
Size: 1724 Color: 228

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 598
Size: 1670 Color: 226

Bin 74: 3 of cap free
Amount of items: 2
Items: 
Size: 12725 Color: 495
Size: 3560 Color: 329

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 12929 Color: 503
Size: 3324 Color: 320
Size: 32 Color: 0

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 13110 Color: 507
Size: 3175 Color: 315

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 13173 Color: 511
Size: 3112 Color: 313

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 14168 Color: 569
Size: 2117 Color: 259

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 14312 Color: 577
Size: 1973 Color: 246

Bin 80: 4 of cap free
Amount of items: 5
Items: 
Size: 8244 Color: 415
Size: 6776 Color: 394
Size: 432 Color: 76
Size: 416 Color: 74
Size: 416 Color: 73

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 9352 Color: 426
Size: 6532 Color: 390
Size: 400 Color: 64

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 9919 Color: 430
Size: 5981 Color: 385
Size: 384 Color: 59

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 11472 Color: 460
Size: 4528 Color: 354
Size: 284 Color: 18

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 11498 Color: 462
Size: 3222 Color: 318
Size: 1564 Color: 217

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 11956 Color: 474
Size: 4328 Color: 351

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 12479 Color: 489
Size: 3805 Color: 336

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 530
Size: 2728 Color: 300

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13868 Color: 552
Size: 2416 Color: 281

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 571
Size: 2096 Color: 257

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14540 Color: 592
Size: 1744 Color: 230

Bin 91: 5 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 546
Size: 2474 Color: 285

Bin 92: 6 of cap free
Amount of items: 5
Items: 
Size: 8180 Color: 413
Size: 6768 Color: 392
Size: 448 Color: 83
Size: 446 Color: 81
Size: 440 Color: 79

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 11478 Color: 461
Size: 3220 Color: 317
Size: 1584 Color: 218

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 12776 Color: 496
Size: 3506 Color: 327

Bin 95: 6 of cap free
Amount of items: 2
Items: 
Size: 13968 Color: 557
Size: 2314 Color: 275

Bin 96: 6 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 560
Size: 2296 Color: 272

Bin 97: 6 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 565
Size: 2222 Color: 268

Bin 98: 6 of cap free
Amount of items: 2
Items: 
Size: 14092 Color: 566
Size: 2190 Color: 263

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 14286 Color: 576
Size: 1996 Color: 247

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 14458 Color: 586
Size: 1824 Color: 236

Bin 101: 7 of cap free
Amount of items: 5
Items: 
Size: 8168 Color: 411
Size: 3392 Color: 322
Size: 2505 Color: 287
Size: 1768 Color: 233
Size: 448 Color: 82

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 13314 Color: 516
Size: 2967 Color: 309

Bin 103: 8 of cap free
Amount of items: 3
Items: 
Size: 11592 Color: 468
Size: 3792 Color: 335
Size: 896 Color: 160

Bin 104: 8 of cap free
Amount of items: 2
Items: 
Size: 12308 Color: 484
Size: 3972 Color: 340

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 13633 Color: 536
Size: 2647 Color: 296

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 13696 Color: 540
Size: 2584 Color: 291

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 568
Size: 2160 Color: 261

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 14420 Color: 585
Size: 1860 Color: 238

Bin 109: 10 of cap free
Amount of items: 3
Items: 
Size: 10182 Color: 434
Size: 5724 Color: 378
Size: 372 Color: 52

Bin 110: 10 of cap free
Amount of items: 3
Items: 
Size: 11277 Color: 456
Size: 4697 Color: 361
Size: 304 Color: 26

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 12432 Color: 488
Size: 3846 Color: 337

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 12592 Color: 493
Size: 3686 Color: 332

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 550
Size: 2442 Color: 283

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 13864 Color: 551
Size: 2414 Color: 280

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 14348 Color: 579
Size: 1930 Color: 243

Bin 116: 11 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 444
Size: 5309 Color: 374
Size: 348 Color: 42

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 12534 Color: 490
Size: 3743 Color: 333

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 13341 Color: 519
Size: 2936 Color: 308

Bin 119: 11 of cap free
Amount of items: 2
Items: 
Size: 13649 Color: 538
Size: 2628 Color: 294

Bin 120: 11 of cap free
Amount of items: 2
Items: 
Size: 13749 Color: 544
Size: 2528 Color: 289

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 543
Size: 2556 Color: 290

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 545
Size: 2512 Color: 288

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 14252 Color: 575
Size: 2024 Color: 251

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 11365 Color: 458
Size: 4622 Color: 359
Size: 288 Color: 21

Bin 125: 13 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 459
Size: 4623 Color: 360
Size: 284 Color: 19

Bin 126: 13 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 521
Size: 2910 Color: 307

Bin 127: 14 of cap free
Amount of items: 3
Items: 
Size: 10864 Color: 451
Size: 5090 Color: 370
Size: 320 Color: 34

Bin 128: 15 of cap free
Amount of items: 2
Items: 
Size: 11524 Color: 465
Size: 4749 Color: 363

Bin 129: 15 of cap free
Amount of items: 3
Items: 
Size: 11797 Color: 473
Size: 4244 Color: 349
Size: 232 Color: 9

Bin 130: 15 of cap free
Amount of items: 2
Items: 
Size: 13172 Color: 510
Size: 3101 Color: 312

Bin 131: 15 of cap free
Amount of items: 2
Items: 
Size: 13629 Color: 535
Size: 2644 Color: 295

Bin 132: 15 of cap free
Amount of items: 2
Items: 
Size: 14032 Color: 564
Size: 2241 Color: 269

Bin 133: 16 of cap free
Amount of items: 3
Items: 
Size: 10746 Color: 447
Size: 5182 Color: 371
Size: 344 Color: 39

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 13668 Color: 539
Size: 2604 Color: 293

Bin 135: 17 of cap free
Amount of items: 2
Items: 
Size: 13362 Color: 520
Size: 2909 Color: 306

Bin 136: 17 of cap free
Amount of items: 2
Items: 
Size: 13544 Color: 529
Size: 2727 Color: 299

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 13140 Color: 509
Size: 3130 Color: 314

Bin 138: 19 of cap free
Amount of items: 2
Items: 
Size: 13832 Color: 549
Size: 2437 Color: 282

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 12700 Color: 494
Size: 3568 Color: 330

Bin 140: 22 of cap free
Amount of items: 3
Items: 
Size: 11540 Color: 467
Size: 4454 Color: 353
Size: 272 Color: 15

Bin 141: 24 of cap free
Amount of items: 3
Items: 
Size: 10440 Color: 442
Size: 5472 Color: 377
Size: 352 Color: 45

Bin 142: 26 of cap free
Amount of items: 2
Items: 
Size: 13018 Color: 506
Size: 3244 Color: 319

Bin 143: 27 of cap free
Amount of items: 3
Items: 
Size: 11723 Color: 471
Size: 4282 Color: 350
Size: 256 Color: 11

Bin 144: 28 of cap free
Amount of items: 2
Items: 
Size: 10196 Color: 435
Size: 6064 Color: 388

Bin 145: 30 of cap free
Amount of items: 3
Items: 
Size: 9142 Color: 421
Size: 6708 Color: 391
Size: 408 Color: 67

Bin 146: 30 of cap free
Amount of items: 2
Items: 
Size: 9262 Color: 424
Size: 6996 Color: 401

Bin 147: 30 of cap free
Amount of items: 2
Items: 
Size: 12242 Color: 483
Size: 4016 Color: 345

Bin 148: 31 of cap free
Amount of items: 2
Items: 
Size: 13261 Color: 513
Size: 2996 Color: 311

Bin 149: 34 of cap free
Amount of items: 3
Items: 
Size: 12140 Color: 479
Size: 3994 Color: 343
Size: 120 Color: 2

Bin 150: 35 of cap free
Amount of items: 3
Items: 
Size: 12183 Color: 480
Size: 4010 Color: 344
Size: 60 Color: 1

Bin 151: 36 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 425
Size: 3495 Color: 326
Size: 3421 Color: 324

Bin 152: 36 of cap free
Amount of items: 3
Items: 
Size: 11530 Color: 466
Size: 4450 Color: 352
Size: 272 Color: 16

Bin 153: 37 of cap free
Amount of items: 2
Items: 
Size: 10344 Color: 438
Size: 5907 Color: 383

Bin 154: 42 of cap free
Amount of items: 3
Items: 
Size: 10838 Color: 450
Size: 5084 Color: 369
Size: 324 Color: 35

Bin 155: 42 of cap free
Amount of items: 3
Items: 
Size: 10950 Color: 452
Size: 4976 Color: 368
Size: 320 Color: 33

Bin 156: 44 of cap free
Amount of items: 3
Items: 
Size: 10653 Color: 445
Size: 5247 Color: 372
Size: 344 Color: 41

Bin 157: 44 of cap free
Amount of items: 3
Items: 
Size: 12082 Color: 477
Size: 3986 Color: 341
Size: 176 Color: 5

Bin 158: 45 of cap free
Amount of items: 3
Items: 
Size: 10591 Color: 443
Size: 5300 Color: 373
Size: 352 Color: 44

Bin 159: 46 of cap free
Amount of items: 3
Items: 
Size: 10070 Color: 433
Size: 5800 Color: 380
Size: 372 Color: 53

Bin 160: 53 of cap free
Amount of items: 7
Items: 
Size: 8164 Color: 410
Size: 2217 Color: 267
Size: 2056 Color: 254
Size: 2010 Color: 249
Size: 848 Color: 158
Size: 480 Color: 90
Size: 460 Color: 86

Bin 161: 54 of cap free
Amount of items: 3
Items: 
Size: 9040 Color: 419
Size: 6786 Color: 397
Size: 408 Color: 68

Bin 162: 56 of cap free
Amount of items: 2
Items: 
Size: 11508 Color: 464
Size: 4724 Color: 362

Bin 163: 57 of cap free
Amount of items: 9
Items: 
Size: 8147 Color: 406
Size: 1264 Color: 190
Size: 1188 Color: 188
Size: 1180 Color: 186
Size: 1172 Color: 185
Size: 1152 Color: 182
Size: 1144 Color: 181
Size: 492 Color: 98
Size: 492 Color: 97

Bin 164: 57 of cap free
Amount of items: 3
Items: 
Size: 9993 Color: 432
Size: 5862 Color: 382
Size: 376 Color: 56

Bin 165: 63 of cap free
Amount of items: 3
Items: 
Size: 11792 Color: 472
Size: 4177 Color: 348
Size: 256 Color: 10

Bin 166: 64 of cap free
Amount of items: 3
Items: 
Size: 11316 Color: 457
Size: 4620 Color: 358
Size: 288 Color: 22

Bin 167: 65 of cap free
Amount of items: 3
Items: 
Size: 12095 Color: 478
Size: 3988 Color: 342
Size: 140 Color: 4

Bin 168: 72 of cap free
Amount of items: 3
Items: 
Size: 10408 Color: 441
Size: 5456 Color: 376
Size: 352 Color: 46

Bin 169: 108 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 476
Size: 3964 Color: 339
Size: 192 Color: 7

Bin 170: 114 of cap free
Amount of items: 4
Items: 
Size: 9596 Color: 428
Size: 5784 Color: 379
Size: 400 Color: 62
Size: 394 Color: 61

Bin 171: 114 of cap free
Amount of items: 3
Items: 
Size: 9932 Color: 431
Size: 5858 Color: 381
Size: 384 Color: 57

Bin 172: 120 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 475
Size: 3928 Color: 338
Size: 224 Color: 8

Bin 173: 132 of cap free
Amount of items: 3
Items: 
Size: 11232 Color: 455
Size: 4604 Color: 357
Size: 320 Color: 29

Bin 174: 136 of cap free
Amount of items: 3
Items: 
Size: 9420 Color: 427
Size: 6332 Color: 389
Size: 400 Color: 63

Bin 175: 144 of cap free
Amount of items: 3
Items: 
Size: 10376 Color: 440
Size: 5416 Color: 375
Size: 352 Color: 47

Bin 176: 152 of cap free
Amount of items: 3
Items: 
Size: 10836 Color: 449
Size: 4968 Color: 367
Size: 332 Color: 37

Bin 177: 164 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 417
Size: 7228 Color: 402
Size: 412 Color: 70

Bin 178: 164 of cap free
Amount of items: 3
Items: 
Size: 9744 Color: 429
Size: 5988 Color: 386
Size: 392 Color: 60

Bin 179: 188 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 454
Size: 4584 Color: 356
Size: 320 Color: 30

Bin 180: 192 of cap free
Amount of items: 3
Items: 
Size: 10792 Color: 448
Size: 4968 Color: 366
Size: 336 Color: 38

Bin 181: 222 of cap free
Amount of items: 4
Items: 
Size: 8452 Color: 416
Size: 6782 Color: 395
Size: 416 Color: 72
Size: 416 Color: 71

Bin 182: 242 of cap free
Amount of items: 2
Items: 
Size: 9254 Color: 423
Size: 6792 Color: 400

Bin 183: 254 of cap free
Amount of items: 3
Items: 
Size: 11674 Color: 470
Size: 4104 Color: 347
Size: 256 Color: 12

Bin 184: 255 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 469
Size: 4103 Color: 346
Size: 272 Color: 13

Bin 185: 267 of cap free
Amount of items: 3
Items: 
Size: 10741 Color: 446
Size: 4936 Color: 365
Size: 344 Color: 40

Bin 186: 291 of cap free
Amount of items: 11
Items: 
Size: 8145 Color: 404
Size: 976 Color: 171
Size: 976 Color: 170
Size: 976 Color: 169
Size: 960 Color: 168
Size: 948 Color: 167
Size: 938 Color: 165
Size: 528 Color: 105
Size: 520 Color: 104
Size: 518 Color: 103
Size: 512 Color: 102

Bin 187: 294 of cap free
Amount of items: 10
Items: 
Size: 8146 Color: 405
Size: 1112 Color: 180
Size: 1088 Color: 179
Size: 1056 Color: 177
Size: 1048 Color: 176
Size: 1016 Color: 174
Size: 992 Color: 172
Size: 512 Color: 101
Size: 512 Color: 100
Size: 512 Color: 99

Bin 188: 296 of cap free
Amount of items: 7
Items: 
Size: 8152 Color: 408
Size: 1662 Color: 225
Size: 1526 Color: 215
Size: 1460 Color: 209
Size: 1356 Color: 201
Size: 1356 Color: 200
Size: 480 Color: 92

Bin 189: 299 of cap free
Amount of items: 2
Items: 
Size: 9201 Color: 422
Size: 6788 Color: 399

Bin 190: 314 of cap free
Amount of items: 3
Items: 
Size: 11112 Color: 453
Size: 4542 Color: 355
Size: 320 Color: 31

Bin 191: 328 of cap free
Amount of items: 4
Items: 
Size: 10352 Color: 439
Size: 4904 Color: 364
Size: 352 Color: 49
Size: 352 Color: 48

Bin 192: 388 of cap free
Amount of items: 2
Items: 
Size: 9113 Color: 420
Size: 6787 Color: 398

Bin 193: 403 of cap free
Amount of items: 3
Items: 
Size: 8692 Color: 418
Size: 6785 Color: 396
Size: 408 Color: 69

Bin 194: 416 of cap free
Amount of items: 4
Items: 
Size: 8228 Color: 414
Size: 6772 Color: 393
Size: 440 Color: 78
Size: 432 Color: 77

Bin 195: 420 of cap free
Amount of items: 8
Items: 
Size: 8148 Color: 407
Size: 1352 Color: 198
Size: 1352 Color: 197
Size: 1352 Color: 196
Size: 1344 Color: 195
Size: 1344 Color: 194
Size: 490 Color: 96
Size: 486 Color: 95

Bin 196: 422 of cap free
Amount of items: 6
Items: 
Size: 8154 Color: 409
Size: 1904 Color: 242
Size: 1818 Color: 235
Size: 1756 Color: 232
Size: 1754 Color: 231
Size: 480 Color: 91

Bin 197: 436 of cap free
Amount of items: 2
Items: 
Size: 8176 Color: 412
Size: 7676 Color: 403

Bin 198: 554 of cap free
Amount of items: 22
Items: 
Size: 920 Color: 163
Size: 912 Color: 162
Size: 848 Color: 157
Size: 848 Color: 156
Size: 834 Color: 155
Size: 820 Color: 154
Size: 816 Color: 153
Size: 800 Color: 152
Size: 800 Color: 151
Size: 796 Color: 150
Size: 796 Color: 149
Size: 792 Color: 148
Size: 608 Color: 122
Size: 608 Color: 121
Size: 592 Color: 120
Size: 592 Color: 119
Size: 580 Color: 118
Size: 580 Color: 117
Size: 576 Color: 114
Size: 544 Color: 109
Size: 544 Color: 108
Size: 528 Color: 106

Bin 199: 6596 of cap free
Amount of items: 14
Items: 
Size: 792 Color: 147
Size: 768 Color: 144
Size: 760 Color: 143
Size: 748 Color: 142
Size: 720 Color: 141
Size: 704 Color: 139
Size: 704 Color: 137
Size: 700 Color: 136
Size: 648 Color: 130
Size: 640 Color: 128
Size: 640 Color: 127
Size: 624 Color: 125
Size: 624 Color: 124
Size: 620 Color: 123

Total size: 3225024
Total free space: 16288

