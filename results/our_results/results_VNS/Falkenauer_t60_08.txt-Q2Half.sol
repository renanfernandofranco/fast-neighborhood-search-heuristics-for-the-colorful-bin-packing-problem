Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 354 Color: 1
Size: 255 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 286 Color: 1
Size: 252 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 282 Color: 1
Size: 250 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 258 Color: 1
Size: 251 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 346 Color: 1
Size: 293 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 352 Color: 1
Size: 280 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 343 Color: 1
Size: 259 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 295 Color: 1
Size: 251 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 275 Color: 1
Size: 272 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 259 Color: 1
Size: 256 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 284 Color: 1
Size: 263 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 275 Color: 0
Size: 357 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 341 Color: 1
Size: 278 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 293 Color: 1
Size: 256 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 356 Color: 1
Size: 283 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 370 Color: 1
Size: 252 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 292 Color: 1
Size: 269 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 363 Color: 1
Size: 254 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 335 Color: 1
Size: 312 Color: 0

Total size: 20000
Total free space: 0

