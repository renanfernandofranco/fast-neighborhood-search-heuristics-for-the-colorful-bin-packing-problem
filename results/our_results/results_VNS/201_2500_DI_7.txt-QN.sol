Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1246 Color: 142
Size: 1022 Color: 133
Size: 86 Color: 33
Size: 62 Color: 21
Size: 56 Color: 20

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1596 Color: 154
Size: 842 Color: 126
Size: 34 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1773 Color: 164
Size: 647 Color: 114
Size: 52 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 170
Size: 530 Color: 105
Size: 82 Color: 31

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 174
Size: 434 Color: 95
Size: 132 Color: 52

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1916 Color: 175
Size: 548 Color: 106
Size: 8 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 2004 Color: 182
Size: 396 Color: 89
Size: 72 Color: 26

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2026 Color: 184
Size: 428 Color: 94
Size: 18 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2057 Color: 186
Size: 313 Color: 81
Size: 102 Color: 41

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2084 Color: 189
Size: 244 Color: 71
Size: 144 Color: 53

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 192
Size: 324 Color: 82
Size: 40 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 193
Size: 230 Color: 69
Size: 124 Color: 48

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2142 Color: 195
Size: 262 Color: 73
Size: 68 Color: 25

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2153 Color: 196
Size: 267 Color: 74
Size: 52 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 200
Size: 168 Color: 59
Size: 116 Color: 47

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 201
Size: 176 Color: 62
Size: 98 Color: 40

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 156
Size: 401 Color: 91
Size: 397 Color: 90

Bin 18: 1 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 160
Size: 767 Color: 123

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1775 Color: 165
Size: 644 Color: 113
Size: 52 Color: 14

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 2061 Color: 187
Size: 278 Color: 77
Size: 132 Color: 50

Bin 21: 1 of cap free
Amount of items: 2
Items: 
Size: 2097 Color: 191
Size: 374 Color: 86

Bin 22: 2 of cap free
Amount of items: 5
Items: 
Size: 1244 Color: 141
Size: 1018 Color: 132
Size: 80 Color: 30
Size: 64 Color: 23
Size: 64 Color: 22

Bin 23: 2 of cap free
Amount of items: 5
Items: 
Size: 1254 Color: 143
Size: 1028 Color: 134
Size: 76 Color: 29
Size: 56 Color: 19
Size: 56 Color: 18

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 150
Size: 576 Color: 109
Size: 342 Color: 83

Bin 25: 2 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 155
Size: 836 Color: 124
Size: 32 Color: 7

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1859 Color: 169
Size: 435 Color: 96
Size: 176 Color: 61

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1898 Color: 173
Size: 572 Color: 108

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1951 Color: 176
Size: 511 Color: 103
Size: 8 Color: 1

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 1954 Color: 177
Size: 516 Color: 104

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2051 Color: 185
Size: 419 Color: 93

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 190
Size: 384 Color: 87

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 2127 Color: 194
Size: 343 Color: 84

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 2162 Color: 197
Size: 308 Color: 80

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 2172 Color: 198
Size: 298 Color: 79

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1675 Color: 157
Size: 662 Color: 115
Size: 132 Color: 51

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 166
Size: 665 Color: 116
Size: 16 Color: 3

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1879 Color: 171
Size: 590 Color: 111

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 2180 Color: 199
Size: 289 Color: 78

Bin 39: 4 of cap free
Amount of items: 5
Items: 
Size: 1239 Color: 140
Size: 567 Color: 107
Size: 482 Color: 99
Size: 116 Color: 46
Size: 64 Color: 24

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1766 Color: 163
Size: 702 Color: 118

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1838 Color: 168
Size: 622 Color: 112
Size: 8 Color: 0

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1887 Color: 172
Size: 581 Color: 110

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1964 Color: 178
Size: 504 Color: 102

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1979 Color: 180
Size: 489 Color: 100

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1994 Color: 181
Size: 474 Color: 98

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 2066 Color: 188
Size: 402 Color: 92

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 148
Size: 956 Color: 130
Size: 48 Color: 13

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1971 Color: 179
Size: 495 Color: 101

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 2007 Color: 183
Size: 459 Color: 97

Bin 50: 7 of cap free
Amount of items: 2
Items: 
Size: 1700 Color: 159
Size: 765 Color: 122

Bin 51: 8 of cap free
Amount of items: 17
Items: 
Size: 204 Color: 67
Size: 204 Color: 66
Size: 204 Color: 65
Size: 200 Color: 64
Size: 200 Color: 63
Size: 172 Color: 60
Size: 160 Color: 58
Size: 152 Color: 57
Size: 152 Color: 56
Size: 128 Color: 49
Size: 112 Color: 43
Size: 104 Color: 42
Size: 96 Color: 39
Size: 96 Color: 38
Size: 96 Color: 37
Size: 92 Color: 36
Size: 92 Color: 35

Bin 52: 8 of cap free
Amount of items: 3
Items: 
Size: 1718 Color: 161
Size: 726 Color: 119
Size: 20 Color: 5

Bin 53: 9 of cap free
Amount of items: 3
Items: 
Size: 1411 Color: 146
Size: 1000 Color: 131
Size: 52 Color: 17

Bin 54: 12 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 158
Size: 762 Color: 121
Size: 20 Color: 6

Bin 55: 12 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 167
Size: 667 Color: 117

Bin 56: 14 of cap free
Amount of items: 2
Items: 
Size: 1726 Color: 162
Size: 732 Color: 120

Bin 57: 19 of cap free
Amount of items: 2
Items: 
Size: 1422 Color: 147
Size: 1031 Color: 137

Bin 58: 22 of cap free
Amount of items: 4
Items: 
Size: 1476 Color: 149
Size: 878 Color: 127
Size: 48 Color: 12
Size: 48 Color: 11

Bin 59: 26 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 153
Size: 840 Color: 125
Size: 44 Color: 10

Bin 60: 30 of cap free
Amount of items: 2
Items: 
Size: 1555 Color: 152
Size: 887 Color: 129

Bin 61: 33 of cap free
Amount of items: 2
Items: 
Size: 1409 Color: 145
Size: 1030 Color: 136

Bin 62: 34 of cap free
Amount of items: 2
Items: 
Size: 1553 Color: 151
Size: 885 Color: 128

Bin 63: 39 of cap free
Amount of items: 2
Items: 
Size: 1404 Color: 144
Size: 1029 Color: 135

Bin 64: 59 of cap free
Amount of items: 6
Items: 
Size: 1238 Color: 139
Size: 389 Color: 88
Size: 366 Color: 85
Size: 272 Color: 76
Size: 76 Color: 28
Size: 72 Color: 27

Bin 65: 91 of cap free
Amount of items: 7
Items: 
Size: 1237 Color: 138
Size: 272 Color: 75
Size: 252 Color: 72
Size: 244 Color: 70
Size: 204 Color: 68
Size: 88 Color: 34
Size: 84 Color: 32

Bin 66: 1956 of cap free
Amount of items: 4
Items: 
Size: 148 Color: 55
Size: 144 Color: 54
Size: 112 Color: 45
Size: 112 Color: 44

Total size: 160680
Total free space: 2472

