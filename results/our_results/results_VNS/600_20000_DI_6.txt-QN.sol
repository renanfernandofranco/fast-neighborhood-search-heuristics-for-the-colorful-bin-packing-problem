Capicity Bin: 13392
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 6716 Color: 413
Size: 5556 Color: 391
Size: 532 Color: 136
Size: 296 Color: 63
Size: 292 Color: 61

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 7976 Color: 427
Size: 1446 Color: 248
Size: 1405 Color: 244
Size: 1393 Color: 243
Size: 1172 Color: 211

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10216 Color: 477
Size: 2886 Color: 338
Size: 290 Color: 59

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 481
Size: 2724 Color: 332
Size: 244 Color: 31

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10588 Color: 489
Size: 2340 Color: 315
Size: 464 Color: 124

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10754 Color: 496
Size: 2424 Color: 321
Size: 214 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10770 Color: 497
Size: 2186 Color: 309
Size: 436 Color: 118

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10876 Color: 503
Size: 1564 Color: 260
Size: 952 Color: 187

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11010 Color: 507
Size: 1260 Color: 222
Size: 1122 Color: 203

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 514
Size: 1806 Color: 283
Size: 458 Color: 122

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11140 Color: 515
Size: 2124 Color: 304
Size: 128 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 519
Size: 1480 Color: 254
Size: 688 Color: 160

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11318 Color: 524
Size: 1882 Color: 289
Size: 192 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11352 Color: 527
Size: 1704 Color: 276
Size: 336 Color: 84

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 533
Size: 1352 Color: 234
Size: 592 Color: 145

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11450 Color: 534
Size: 1824 Color: 285
Size: 118 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11453 Color: 535
Size: 1563 Color: 259
Size: 376 Color: 100

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11457 Color: 536
Size: 1617 Color: 268
Size: 318 Color: 72

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11473 Color: 538
Size: 1467 Color: 251
Size: 452 Color: 121

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11624 Color: 547
Size: 952 Color: 186
Size: 816 Color: 174

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11625 Color: 548
Size: 1473 Color: 252
Size: 294 Color: 62

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11633 Color: 550
Size: 1597 Color: 264
Size: 162 Color: 6

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11707 Color: 556
Size: 1293 Color: 225
Size: 392 Color: 105

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11726 Color: 558
Size: 1328 Color: 231
Size: 338 Color: 86

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11736 Color: 559
Size: 1224 Color: 218
Size: 432 Color: 115

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 11737 Color: 560
Size: 1381 Color: 241
Size: 274 Color: 51

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 11758 Color: 564
Size: 1198 Color: 214
Size: 436 Color: 119

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 11796 Color: 568
Size: 844 Color: 178
Size: 752 Color: 169

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11812 Color: 571
Size: 1364 Color: 237
Size: 216 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 11830 Color: 573
Size: 832 Color: 176
Size: 730 Color: 166

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 11833 Color: 574
Size: 1351 Color: 233
Size: 208 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 11841 Color: 575
Size: 1319 Color: 228
Size: 232 Color: 24

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 11854 Color: 577
Size: 1114 Color: 201
Size: 424 Color: 113

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 11873 Color: 579
Size: 1247 Color: 220
Size: 272 Color: 47

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 11884 Color: 580
Size: 1288 Color: 224
Size: 220 Color: 15

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 11897 Color: 582
Size: 1215 Color: 217
Size: 280 Color: 54

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 11956 Color: 587
Size: 1114 Color: 202
Size: 322 Color: 79

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 11958 Color: 588
Size: 1130 Color: 206
Size: 304 Color: 68

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 11973 Color: 590
Size: 1183 Color: 213
Size: 236 Color: 26

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 11988 Color: 591
Size: 1176 Color: 212
Size: 228 Color: 20

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 592
Size: 854 Color: 180
Size: 546 Color: 140

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 593
Size: 1124 Color: 204
Size: 264 Color: 44

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12005 Color: 594
Size: 1157 Color: 208
Size: 230 Color: 22

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12022 Color: 596
Size: 1114 Color: 200
Size: 256 Color: 38

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 598
Size: 824 Color: 175
Size: 524 Color: 134

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12050 Color: 599
Size: 962 Color: 189
Size: 380 Color: 102

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12052 Color: 600
Size: 1112 Color: 198
Size: 228 Color: 21

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 9116 Color: 448
Size: 4275 Color: 373

Bin 49: 1 of cap free
Amount of items: 5
Items: 
Size: 9426 Color: 452
Size: 1981 Color: 296
Size: 1482 Color: 255
Size: 262 Color: 41
Size: 240 Color: 29

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 9809 Color: 465
Size: 3362 Color: 354
Size: 220 Color: 14

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 10524 Color: 484
Size: 2667 Color: 329
Size: 200 Color: 9

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 10669 Color: 492
Size: 1610 Color: 266
Size: 1112 Color: 196

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 10843 Color: 501
Size: 1972 Color: 295
Size: 576 Color: 143

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 11165 Color: 517
Size: 1362 Color: 236
Size: 864 Color: 182

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 11340 Color: 526
Size: 2051 Color: 301

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 11425 Color: 531
Size: 1966 Color: 294

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 11429 Color: 532
Size: 1204 Color: 215
Size: 758 Color: 170

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 11517 Color: 542
Size: 1874 Color: 288

Bin 59: 1 of cap free
Amount of items: 2
Items: 
Size: 11544 Color: 544
Size: 1847 Color: 286

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 11594 Color: 546
Size: 1797 Color: 282

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 11628 Color: 549
Size: 1763 Color: 281

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 11769 Color: 566
Size: 1622 Color: 269

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 11808 Color: 570
Size: 1583 Color: 262

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 11889 Color: 581
Size: 1502 Color: 256

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 12038 Color: 597
Size: 1353 Color: 235

Bin 66: 2 of cap free
Amount of items: 9
Items: 
Size: 6700 Color: 407
Size: 1124 Color: 205
Size: 1112 Color: 199
Size: 1112 Color: 197
Size: 1112 Color: 195
Size: 1104 Color: 194
Size: 488 Color: 131
Size: 320 Color: 74
Size: 318 Color: 73

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 9652 Color: 461
Size: 2125 Color: 305
Size: 1613 Color: 267

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 10306 Color: 479
Size: 3084 Color: 343

Bin 69: 2 of cap free
Amount of items: 2
Items: 
Size: 10550 Color: 486
Size: 2840 Color: 337

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 10579 Color: 488
Size: 1438 Color: 246
Size: 1373 Color: 239

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 11124 Color: 513
Size: 2266 Color: 312

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 11721 Color: 557
Size: 1669 Color: 274

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 11749 Color: 563
Size: 1641 Color: 272

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 11802 Color: 569
Size: 1588 Color: 263

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 11817 Color: 572
Size: 1573 Color: 261

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 11914 Color: 583
Size: 1476 Color: 253

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 11959 Color: 589
Size: 1431 Color: 245

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 12006 Color: 595
Size: 1384 Color: 242

Bin 79: 3 of cap free
Amount of items: 2
Items: 
Size: 11092 Color: 511
Size: 2297 Color: 314

Bin 80: 3 of cap free
Amount of items: 2
Items: 
Size: 11237 Color: 521
Size: 2152 Color: 307

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 11493 Color: 541
Size: 1896 Color: 291

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 11845 Color: 576
Size: 1544 Color: 258

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 11948 Color: 586
Size: 1441 Color: 247

Bin 84: 4 of cap free
Amount of items: 2
Items: 
Size: 10113 Color: 471
Size: 3275 Color: 351

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 10664 Color: 491
Size: 2724 Color: 333

Bin 86: 4 of cap free
Amount of items: 2
Items: 
Size: 11306 Color: 523
Size: 2082 Color: 302

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 11740 Color: 561
Size: 1648 Color: 273

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 11764 Color: 565
Size: 1624 Color: 270

Bin 89: 5 of cap free
Amount of items: 5
Items: 
Size: 6936 Color: 415
Size: 5571 Color: 393
Size: 304 Color: 67
Size: 288 Color: 57
Size: 288 Color: 56

Bin 90: 5 of cap free
Amount of items: 3
Items: 
Size: 9084 Color: 447
Size: 4061 Color: 367
Size: 242 Color: 30

Bin 91: 5 of cap free
Amount of items: 3
Items: 
Size: 10457 Color: 482
Size: 2182 Color: 308
Size: 748 Color: 168

Bin 92: 5 of cap free
Amount of items: 2
Items: 
Size: 10813 Color: 499
Size: 2574 Color: 326

Bin 93: 5 of cap free
Amount of items: 2
Items: 
Size: 11657 Color: 553
Size: 1730 Color: 279

Bin 94: 5 of cap free
Amount of items: 2
Items: 
Size: 11694 Color: 555
Size: 1693 Color: 275

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 11928 Color: 584
Size: 1459 Color: 250

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 11935 Color: 585
Size: 1452 Color: 249

Bin 97: 6 of cap free
Amount of items: 5
Items: 
Size: 6732 Color: 414
Size: 5564 Color: 392
Size: 512 Color: 133
Size: 290 Color: 60
Size: 288 Color: 58

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 9992 Color: 468
Size: 3226 Color: 348
Size: 168 Color: 7

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 509
Size: 2370 Color: 318

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 11462 Color: 537
Size: 1924 Color: 293

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 11477 Color: 539
Size: 1909 Color: 292

Bin 102: 6 of cap free
Amount of items: 2
Items: 
Size: 11570 Color: 545
Size: 1816 Color: 284

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 11670 Color: 554
Size: 1716 Color: 277

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 11864 Color: 578
Size: 1522 Color: 257

Bin 105: 7 of cap free
Amount of items: 3
Items: 
Size: 7534 Color: 419
Size: 5577 Color: 396
Size: 274 Color: 50

Bin 106: 7 of cap free
Amount of items: 2
Items: 
Size: 10097 Color: 470
Size: 3288 Color: 352

Bin 107: 7 of cap free
Amount of items: 2
Items: 
Size: 11643 Color: 551
Size: 1742 Color: 280

Bin 108: 7 of cap free
Amount of items: 2
Items: 
Size: 11784 Color: 567
Size: 1601 Color: 265

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 7530 Color: 418
Size: 5576 Color: 395
Size: 278 Color: 52

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 10824 Color: 500
Size: 2560 Color: 325

Bin 111: 9 of cap free
Amount of items: 2
Items: 
Size: 11391 Color: 530
Size: 1992 Color: 298

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 7692 Color: 425
Size: 5690 Color: 402

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 11745 Color: 562
Size: 1637 Color: 271

Bin 114: 11 of cap free
Amount of items: 3
Items: 
Size: 9001 Color: 445
Size: 4132 Color: 369
Size: 248 Color: 33

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 11524 Color: 543
Size: 1857 Color: 287

Bin 116: 12 of cap free
Amount of items: 5
Items: 
Size: 6712 Color: 412
Size: 5512 Color: 390
Size: 560 Color: 142
Size: 300 Color: 65
Size: 296 Color: 64

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 487
Size: 2824 Color: 336

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 10688 Color: 494
Size: 2692 Color: 331

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 10933 Color: 505
Size: 2447 Color: 323

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 11332 Color: 525
Size: 2048 Color: 300

Bin 121: 13 of cap free
Amount of items: 2
Items: 
Size: 11015 Color: 508
Size: 2364 Color: 317

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 11034 Color: 510
Size: 2345 Color: 316

Bin 123: 13 of cap free
Amount of items: 2
Items: 
Size: 11177 Color: 518
Size: 2202 Color: 310

Bin 124: 14 of cap free
Amount of items: 7
Items: 
Size: 6705 Color: 410
Size: 1380 Color: 240
Size: 1371 Color: 238
Size: 1332 Color: 232
Size: 1326 Color: 230
Size: 960 Color: 188
Size: 304 Color: 66

Bin 125: 14 of cap free
Amount of items: 2
Items: 
Size: 11146 Color: 516
Size: 2232 Color: 311

Bin 126: 15 of cap free
Amount of items: 7
Items: 
Size: 6702 Color: 409
Size: 1324 Color: 229
Size: 1313 Color: 227
Size: 1302 Color: 226
Size: 1282 Color: 223
Size: 1142 Color: 207
Size: 312 Color: 69

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 11226 Color: 520
Size: 2151 Color: 306

Bin 128: 15 of cap free
Amount of items: 2
Items: 
Size: 11277 Color: 522
Size: 2100 Color: 303

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 7992 Color: 428
Size: 5384 Color: 388

Bin 130: 16 of cap free
Amount of items: 3
Items: 
Size: 10014 Color: 469
Size: 3202 Color: 347
Size: 160 Color: 5

Bin 131: 16 of cap free
Amount of items: 2
Items: 
Size: 11361 Color: 528
Size: 2015 Color: 299

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 11492 Color: 540
Size: 1884 Color: 290

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 11652 Color: 552
Size: 1724 Color: 278

Bin 134: 17 of cap free
Amount of items: 2
Items: 
Size: 9133 Color: 450
Size: 4242 Color: 372

Bin 135: 17 of cap free
Amount of items: 3
Items: 
Size: 9911 Color: 466
Size: 2392 Color: 319
Size: 1072 Color: 191

Bin 136: 18 of cap free
Amount of items: 3
Items: 
Size: 8330 Color: 435
Size: 4780 Color: 381
Size: 264 Color: 42

Bin 137: 18 of cap free
Amount of items: 3
Items: 
Size: 9554 Color: 459
Size: 3596 Color: 359
Size: 224 Color: 19

Bin 138: 18 of cap free
Amount of items: 2
Items: 
Size: 10132 Color: 473
Size: 3242 Color: 350

Bin 139: 18 of cap free
Amount of items: 2
Items: 
Size: 11103 Color: 512
Size: 2271 Color: 313

Bin 140: 19 of cap free
Amount of items: 2
Items: 
Size: 10852 Color: 502
Size: 2521 Color: 324

Bin 141: 20 of cap free
Amount of items: 3
Items: 
Size: 7669 Color: 424
Size: 5431 Color: 389
Size: 272 Color: 49

Bin 142: 20 of cap free
Amount of items: 3
Items: 
Size: 8279 Color: 432
Size: 4821 Color: 383
Size: 272 Color: 46

Bin 143: 20 of cap free
Amount of items: 3
Items: 
Size: 9016 Color: 446
Size: 4108 Color: 368
Size: 248 Color: 32

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 10470 Color: 483
Size: 2901 Color: 339

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 10975 Color: 506
Size: 2396 Color: 320

Bin 146: 22 of cap free
Amount of items: 2
Items: 
Size: 9684 Color: 462
Size: 3686 Color: 362

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 10637 Color: 490
Size: 2733 Color: 334

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 10728 Color: 495
Size: 2642 Color: 328

Bin 149: 24 of cap free
Amount of items: 3
Items: 
Size: 8890 Color: 442
Size: 4222 Color: 370
Size: 256 Color: 35

Bin 150: 24 of cap free
Amount of items: 2
Items: 
Size: 10930 Color: 504
Size: 2438 Color: 322

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 11382 Color: 529
Size: 1986 Color: 297

Bin 152: 25 of cap free
Amount of items: 3
Items: 
Size: 10193 Color: 476
Size: 3092 Color: 344
Size: 82 Color: 0

Bin 153: 26 of cap free
Amount of items: 3
Items: 
Size: 7513 Color: 417
Size: 5573 Color: 394
Size: 280 Color: 53

Bin 154: 26 of cap free
Amount of items: 3
Items: 
Size: 9578 Color: 460
Size: 3564 Color: 358
Size: 224 Color: 18

Bin 155: 27 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 454
Size: 3661 Color: 361
Size: 240 Color: 27

Bin 156: 30 of cap free
Amount of items: 2
Items: 
Size: 10778 Color: 498
Size: 2584 Color: 327

Bin 157: 31 of cap free
Amount of items: 2
Items: 
Size: 8841 Color: 441
Size: 4520 Color: 378

Bin 158: 33 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 453
Size: 3656 Color: 360
Size: 240 Color: 28

Bin 159: 34 of cap free
Amount of items: 2
Items: 
Size: 9132 Color: 449
Size: 4226 Color: 371

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 10674 Color: 493
Size: 2684 Color: 330

Bin 161: 35 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 434
Size: 4771 Color: 380
Size: 264 Color: 43

Bin 162: 35 of cap free
Amount of items: 3
Items: 
Size: 10180 Color: 475
Size: 3081 Color: 342
Size: 96 Color: 1

Bin 163: 38 of cap free
Amount of items: 2
Items: 
Size: 8468 Color: 437
Size: 4886 Color: 385

Bin 164: 38 of cap free
Amount of items: 2
Items: 
Size: 10367 Color: 480
Size: 2987 Color: 340

Bin 165: 38 of cap free
Amount of items: 2
Items: 
Size: 10536 Color: 485
Size: 2818 Color: 335

Bin 166: 39 of cap free
Amount of items: 3
Items: 
Size: 8263 Color: 431
Size: 4818 Color: 382
Size: 272 Color: 48

Bin 167: 39 of cap free
Amount of items: 2
Items: 
Size: 10124 Color: 472
Size: 3229 Color: 349

Bin 168: 42 of cap free
Amount of items: 2
Items: 
Size: 9358 Color: 451
Size: 3992 Color: 365

Bin 169: 42 of cap free
Amount of items: 2
Items: 
Size: 10226 Color: 478
Size: 3124 Color: 345

Bin 170: 46 of cap free
Amount of items: 3
Items: 
Size: 9720 Color: 464
Size: 3402 Color: 355
Size: 224 Color: 16

Bin 171: 48 of cap free
Amount of items: 3
Items: 
Size: 10164 Color: 474
Size: 3064 Color: 341
Size: 116 Color: 2

Bin 172: 49 of cap free
Amount of items: 4
Items: 
Size: 8519 Color: 438
Size: 4306 Color: 374
Size: 260 Color: 40
Size: 258 Color: 39

Bin 173: 50 of cap free
Amount of items: 8
Items: 
Size: 6701 Color: 408
Size: 1253 Color: 221
Size: 1234 Color: 219
Size: 1204 Color: 216
Size: 1164 Color: 210
Size: 1158 Color: 209
Size: 316 Color: 71
Size: 312 Color: 70

Bin 174: 57 of cap free
Amount of items: 2
Items: 
Size: 7736 Color: 426
Size: 5599 Color: 401

Bin 175: 64 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 440
Size: 4312 Color: 375
Size: 256 Color: 36

Bin 176: 64 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 467
Size: 3182 Color: 346
Size: 216 Color: 12

Bin 177: 67 of cap free
Amount of items: 11
Items: 
Size: 6697 Color: 405
Size: 860 Color: 181
Size: 844 Color: 179
Size: 840 Color: 177
Size: 812 Color: 173
Size: 800 Color: 172
Size: 768 Color: 171
Size: 736 Color: 167
Size: 326 Color: 80
Size: 322 Color: 78
Size: 320 Color: 77

Bin 178: 74 of cap free
Amount of items: 3
Items: 
Size: 9530 Color: 458
Size: 3556 Color: 357
Size: 232 Color: 23

Bin 179: 75 of cap free
Amount of items: 2
Items: 
Size: 6709 Color: 411
Size: 6608 Color: 404

Bin 180: 80 of cap free
Amount of items: 3
Items: 
Size: 8552 Color: 439
Size: 4504 Color: 377
Size: 256 Color: 37

Bin 181: 80 of cap free
Amount of items: 2
Items: 
Size: 9519 Color: 456
Size: 3793 Color: 364

Bin 182: 83 of cap free
Amount of items: 3
Items: 
Size: 9522 Color: 457
Size: 3551 Color: 356
Size: 236 Color: 25

Bin 183: 88 of cap free
Amount of items: 3
Items: 
Size: 8306 Color: 433
Size: 4728 Color: 379
Size: 270 Color: 45

Bin 184: 100 of cap free
Amount of items: 2
Items: 
Size: 8968 Color: 443
Size: 4324 Color: 376

Bin 185: 120 of cap free
Amount of items: 23
Items: 
Size: 720 Color: 165
Size: 712 Color: 164
Size: 712 Color: 163
Size: 708 Color: 162
Size: 704 Color: 161
Size: 672 Color: 159
Size: 660 Color: 158
Size: 654 Color: 157
Size: 644 Color: 156
Size: 644 Color: 155
Size: 644 Color: 154
Size: 640 Color: 153
Size: 640 Color: 152
Size: 636 Color: 151
Size: 632 Color: 150
Size: 616 Color: 149
Size: 616 Color: 148
Size: 344 Color: 88
Size: 344 Color: 87
Size: 336 Color: 85
Size: 336 Color: 83
Size: 332 Color: 82
Size: 326 Color: 81

Bin 186: 124 of cap free
Amount of items: 3
Items: 
Size: 7128 Color: 416
Size: 5856 Color: 403
Size: 284 Color: 55

Bin 187: 132 of cap free
Amount of items: 3
Items: 
Size: 8970 Color: 444
Size: 4040 Color: 366
Size: 250 Color: 34

Bin 188: 132 of cap free
Amount of items: 2
Items: 
Size: 9506 Color: 455
Size: 3754 Color: 363

Bin 189: 133 of cap free
Amount of items: 2
Items: 
Size: 8436 Color: 436
Size: 4823 Color: 384

Bin 190: 146 of cap free
Amount of items: 3
Items: 
Size: 9716 Color: 463
Size: 3306 Color: 353
Size: 224 Color: 17

Bin 191: 150 of cap free
Amount of items: 2
Items: 
Size: 7660 Color: 423
Size: 5582 Color: 400

Bin 192: 154 of cap free
Amount of items: 2
Items: 
Size: 8226 Color: 430
Size: 5012 Color: 387

Bin 193: 182 of cap free
Amount of items: 9
Items: 
Size: 6698 Color: 406
Size: 1104 Color: 193
Size: 1072 Color: 192
Size: 976 Color: 190
Size: 928 Color: 185
Size: 896 Color: 184
Size: 896 Color: 183
Size: 320 Color: 76
Size: 320 Color: 75

Bin 194: 194 of cap free
Amount of items: 2
Items: 
Size: 7617 Color: 422
Size: 5581 Color: 399

Bin 195: 198 of cap free
Amount of items: 2
Items: 
Size: 7614 Color: 421
Size: 5580 Color: 398

Bin 196: 205 of cap free
Amount of items: 2
Items: 
Size: 7609 Color: 420
Size: 5578 Color: 397

Bin 197: 212 of cap free
Amount of items: 2
Items: 
Size: 8204 Color: 429
Size: 4976 Color: 386

Bin 198: 380 of cap free
Amount of items: 29
Items: 
Size: 608 Color: 147
Size: 596 Color: 146
Size: 580 Color: 144
Size: 560 Color: 141
Size: 544 Color: 139
Size: 536 Color: 138
Size: 536 Color: 137
Size: 528 Color: 135
Size: 504 Color: 132
Size: 484 Color: 130
Size: 472 Color: 129
Size: 472 Color: 128
Size: 472 Color: 127
Size: 468 Color: 126
Size: 464 Color: 125
Size: 460 Color: 123
Size: 384 Color: 103
Size: 380 Color: 101
Size: 372 Color: 99
Size: 370 Color: 98
Size: 368 Color: 97
Size: 368 Color: 96
Size: 368 Color: 95
Size: 360 Color: 94
Size: 358 Color: 93
Size: 352 Color: 92
Size: 352 Color: 91
Size: 352 Color: 90
Size: 344 Color: 89

Bin 199: 8414 of cap free
Amount of items: 12
Items: 
Size: 452 Color: 120
Size: 432 Color: 117
Size: 432 Color: 116
Size: 428 Color: 114
Size: 416 Color: 112
Size: 416 Color: 111
Size: 416 Color: 110
Size: 408 Color: 109
Size: 402 Color: 108
Size: 396 Color: 107
Size: 396 Color: 106
Size: 384 Color: 104

Total size: 2651616
Total free space: 13392

