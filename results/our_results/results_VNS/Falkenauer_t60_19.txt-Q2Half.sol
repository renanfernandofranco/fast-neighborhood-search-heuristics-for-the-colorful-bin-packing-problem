Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 273 Color: 1
Size: 267 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 363 Color: 1
Size: 270 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 342 Color: 1
Size: 292 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 259 Color: 0
Size: 350 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 329 Color: 1
Size: 303 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 384 Color: 1
Size: 258 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 342 Color: 1
Size: 263 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 361 Color: 1
Size: 257 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 343 Color: 1
Size: 252 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 305 Color: 1
Size: 288 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 298 Color: 1
Size: 287 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 324 Color: 1
Size: 253 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 316 Color: 1
Size: 257 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 282 Color: 1
Size: 259 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 279 Color: 1
Size: 261 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 286 Color: 1
Size: 255 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 276 Color: 1
Size: 254 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 261 Color: 1
Size: 251 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 253 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Total size: 20000
Total free space: 0

