Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 1
Size: 1029 Color: 0
Size: 204 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 2
Size: 662 Color: 3
Size: 78 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 4
Size: 542 Color: 0
Size: 108 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1867 Color: 0
Size: 461 Color: 3
Size: 144 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 3
Size: 482 Color: 2
Size: 92 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1932 Color: 3
Size: 500 Color: 2
Size: 40 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1958 Color: 2
Size: 398 Color: 4
Size: 116 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1980 Color: 4
Size: 372 Color: 3
Size: 120 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2022 Color: 3
Size: 386 Color: 1
Size: 64 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2028 Color: 3
Size: 412 Color: 0
Size: 32 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2082 Color: 4
Size: 326 Color: 2
Size: 64 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 0
Size: 246 Color: 3
Size: 124 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2108 Color: 4
Size: 340 Color: 0
Size: 24 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 0
Size: 302 Color: 3
Size: 60 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 4
Size: 218 Color: 3
Size: 136 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 3
Size: 230 Color: 2
Size: 76 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2190 Color: 2
Size: 238 Color: 3
Size: 44 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2196 Color: 3
Size: 176 Color: 2
Size: 100 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 1
Size: 148 Color: 0
Size: 120 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2214 Color: 4
Size: 162 Color: 4
Size: 96 Color: 3

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1388 Color: 2
Size: 883 Color: 0
Size: 200 Color: 2

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1413 Color: 3
Size: 1018 Color: 3
Size: 40 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 3
Size: 989 Color: 1
Size: 60 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 1
Size: 620 Color: 0
Size: 272 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 1
Size: 745 Color: 4
Size: 48 Color: 2

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 3
Size: 681 Color: 4
Size: 56 Color: 0

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1771 Color: 1
Size: 700 Color: 2

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 3
Size: 314 Color: 2
Size: 236 Color: 2

Bin 29: 1 of cap free
Amount of items: 5
Items: 
Size: 1993 Color: 0
Size: 446 Color: 1
Size: 16 Color: 4
Size: 8 Color: 4
Size: 8 Color: 2

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1244 Color: 2
Size: 796 Color: 1
Size: 430 Color: 2

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1704 Color: 3
Size: 726 Color: 3
Size: 40 Color: 0

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 0
Size: 538 Color: 2
Size: 222 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 3
Size: 378 Color: 0
Size: 154 Color: 2

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 4
Size: 384 Color: 1
Size: 88 Color: 3

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 4
Size: 204 Color: 4
Size: 204 Color: 3

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 2198 Color: 2
Size: 272 Color: 0

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1491 Color: 2
Size: 802 Color: 3
Size: 176 Color: 0

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1755 Color: 0
Size: 618 Color: 3
Size: 96 Color: 2

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 2068 Color: 4
Size: 401 Color: 0

Bin 40: 4 of cap free
Amount of items: 4
Items: 
Size: 1254 Color: 0
Size: 638 Color: 0
Size: 504 Color: 3
Size: 72 Color: 4

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1524 Color: 3
Size: 878 Color: 1
Size: 66 Color: 1

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1817 Color: 4
Size: 651 Color: 1

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 2178 Color: 0
Size: 290 Color: 1

Bin 44: 5 of cap free
Amount of items: 4
Items: 
Size: 1657 Color: 4
Size: 418 Color: 1
Size: 308 Color: 1
Size: 84 Color: 0

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1812 Color: 0
Size: 547 Color: 4
Size: 108 Color: 2

Bin 46: 6 of cap free
Amount of items: 3
Items: 
Size: 2006 Color: 0
Size: 452 Color: 4
Size: 8 Color: 4

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 2140 Color: 2
Size: 310 Color: 0
Size: 16 Color: 2

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 2206 Color: 2
Size: 260 Color: 1

Bin 49: 9 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 2
Size: 1022 Color: 4
Size: 204 Color: 3

Bin 50: 9 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 0
Size: 779 Color: 1
Size: 48 Color: 4

Bin 51: 10 of cap free
Amount of items: 2
Items: 
Size: 1462 Color: 4
Size: 1000 Color: 2

Bin 52: 11 of cap free
Amount of items: 3
Items: 
Size: 1562 Color: 1
Size: 819 Color: 3
Size: 80 Color: 0

Bin 53: 11 of cap free
Amount of items: 2
Items: 
Size: 1876 Color: 4
Size: 585 Color: 1

Bin 54: 12 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 2
Size: 1030 Color: 0
Size: 184 Color: 1

Bin 55: 16 of cap free
Amount of items: 15
Items: 
Size: 481 Color: 3
Size: 393 Color: 4
Size: 284 Color: 4
Size: 172 Color: 0
Size: 152 Color: 0
Size: 134 Color: 0
Size: 132 Color: 0
Size: 128 Color: 0
Size: 104 Color: 3
Size: 94 Color: 1
Size: 90 Color: 2
Size: 88 Color: 2
Size: 88 Color: 1
Size: 72 Color: 1
Size: 44 Color: 1

Bin 56: 19 of cap free
Amount of items: 2
Items: 
Size: 1897 Color: 1
Size: 556 Color: 2

Bin 57: 20 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 2
Size: 643 Color: 0
Size: 108 Color: 3

Bin 58: 23 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 1
Size: 1044 Color: 3

Bin 59: 25 of cap free
Amount of items: 2
Items: 
Size: 1539 Color: 0
Size: 908 Color: 3

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1941 Color: 4
Size: 505 Color: 0

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1602 Color: 2
Size: 842 Color: 1

Bin 62: 28 of cap free
Amount of items: 2
Items: 
Size: 2001 Color: 3
Size: 443 Color: 0

Bin 63: 32 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 4
Size: 840 Color: 1
Size: 48 Color: 0

Bin 64: 34 of cap free
Amount of items: 2
Items: 
Size: 1862 Color: 3
Size: 576 Color: 4

Bin 65: 35 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 3
Size: 1031 Color: 4
Size: 168 Color: 4

Bin 66: 2048 of cap free
Amount of items: 6
Items: 
Size: 88 Color: 4
Size: 78 Color: 3
Size: 78 Color: 3
Size: 72 Color: 2
Size: 64 Color: 4
Size: 44 Color: 1

Total size: 160680
Total free space: 2472

