Capicity Bin: 1000001
Lower Bound: 45

Bins used: 45
Amount of Colors: 5

Bin 1: 21 of cap free
Amount of items: 2
Items: 
Size: 699659 Color: 1
Size: 300321 Color: 2

Bin 2: 24 of cap free
Amount of items: 3
Items: 
Size: 507242 Color: 2
Size: 277277 Color: 3
Size: 215458 Color: 1

Bin 3: 110 of cap free
Amount of items: 3
Items: 
Size: 711858 Color: 0
Size: 170093 Color: 0
Size: 117940 Color: 3

Bin 4: 171 of cap free
Amount of items: 3
Items: 
Size: 566072 Color: 0
Size: 297711 Color: 2
Size: 136047 Color: 4

Bin 5: 214 of cap free
Amount of items: 3
Items: 
Size: 709552 Color: 0
Size: 184903 Color: 0
Size: 105332 Color: 2

Bin 6: 276 of cap free
Amount of items: 3
Items: 
Size: 600740 Color: 2
Size: 201963 Color: 4
Size: 197022 Color: 3

Bin 7: 320 of cap free
Amount of items: 3
Items: 
Size: 551363 Color: 4
Size: 345793 Color: 2
Size: 102525 Color: 4

Bin 8: 499 of cap free
Amount of items: 3
Items: 
Size: 498822 Color: 2
Size: 332931 Color: 2
Size: 167749 Color: 3

Bin 9: 657 of cap free
Amount of items: 2
Items: 
Size: 538903 Color: 1
Size: 460441 Color: 0

Bin 10: 693 of cap free
Amount of items: 2
Items: 
Size: 705130 Color: 0
Size: 294178 Color: 2

Bin 11: 781 of cap free
Amount of items: 2
Items: 
Size: 754790 Color: 2
Size: 244430 Color: 1

Bin 12: 840 of cap free
Amount of items: 3
Items: 
Size: 583003 Color: 3
Size: 305040 Color: 3
Size: 111118 Color: 2

Bin 13: 861 of cap free
Amount of items: 3
Items: 
Size: 743798 Color: 0
Size: 145613 Color: 2
Size: 109729 Color: 0

Bin 14: 1087 of cap free
Amount of items: 3
Items: 
Size: 652734 Color: 3
Size: 176528 Color: 1
Size: 169652 Color: 1

Bin 15: 1271 of cap free
Amount of items: 3
Items: 
Size: 533747 Color: 4
Size: 303303 Color: 4
Size: 161680 Color: 1

Bin 16: 1654 of cap free
Amount of items: 3
Items: 
Size: 597025 Color: 2
Size: 265507 Color: 0
Size: 135815 Color: 0

Bin 17: 1784 of cap free
Amount of items: 2
Items: 
Size: 594287 Color: 3
Size: 403930 Color: 0

Bin 18: 1975 of cap free
Amount of items: 2
Items: 
Size: 570667 Color: 4
Size: 427359 Color: 2

Bin 19: 2107 of cap free
Amount of items: 2
Items: 
Size: 561816 Color: 2
Size: 436078 Color: 3

Bin 20: 2151 of cap free
Amount of items: 2
Items: 
Size: 517481 Color: 3
Size: 480369 Color: 2

Bin 21: 2616 of cap free
Amount of items: 2
Items: 
Size: 742882 Color: 2
Size: 254503 Color: 0

Bin 22: 2690 of cap free
Amount of items: 2
Items: 
Size: 612740 Color: 3
Size: 384571 Color: 2

Bin 23: 2841 of cap free
Amount of items: 2
Items: 
Size: 578287 Color: 3
Size: 418873 Color: 1

Bin 24: 2910 of cap free
Amount of items: 2
Items: 
Size: 774118 Color: 4
Size: 222973 Color: 3

Bin 25: 3341 of cap free
Amount of items: 2
Items: 
Size: 585486 Color: 1
Size: 411174 Color: 4

Bin 26: 3767 of cap free
Amount of items: 2
Items: 
Size: 522279 Color: 2
Size: 473955 Color: 0

Bin 27: 3782 of cap free
Amount of items: 2
Items: 
Size: 509052 Color: 4
Size: 487167 Color: 3

Bin 28: 7457 of cap free
Amount of items: 2
Items: 
Size: 742672 Color: 4
Size: 249872 Color: 0

Bin 29: 7750 of cap free
Amount of items: 2
Items: 
Size: 642109 Color: 4
Size: 350142 Color: 0

Bin 30: 9258 of cap free
Amount of items: 2
Items: 
Size: 550157 Color: 1
Size: 440586 Color: 2

Bin 31: 10347 of cap free
Amount of items: 2
Items: 
Size: 630287 Color: 2
Size: 359367 Color: 0

Bin 32: 16405 of cap free
Amount of items: 2
Items: 
Size: 582035 Color: 0
Size: 401561 Color: 2

Bin 33: 20396 of cap free
Amount of items: 2
Items: 
Size: 508596 Color: 3
Size: 471009 Color: 0

Bin 34: 21329 of cap free
Amount of items: 2
Items: 
Size: 602658 Color: 2
Size: 376014 Color: 0

Bin 35: 22442 of cap free
Amount of items: 2
Items: 
Size: 545417 Color: 4
Size: 432142 Color: 0

Bin 36: 24596 of cap free
Amount of items: 2
Items: 
Size: 508014 Color: 4
Size: 467391 Color: 1

Bin 37: 27795 of cap free
Amount of items: 2
Items: 
Size: 602306 Color: 3
Size: 369900 Color: 2

Bin 38: 28947 of cap free
Amount of items: 2
Items: 
Size: 794565 Color: 3
Size: 176489 Color: 0

Bin 39: 29204 of cap free
Amount of items: 2
Items: 
Size: 672860 Color: 0
Size: 297937 Color: 2

Bin 40: 33677 of cap free
Amount of items: 2
Items: 
Size: 756218 Color: 1
Size: 210106 Color: 2

Bin 41: 34763 of cap free
Amount of items: 2
Items: 
Size: 668819 Color: 2
Size: 296419 Color: 0

Bin 42: 35763 of cap free
Amount of items: 2
Items: 
Size: 709800 Color: 2
Size: 254438 Color: 4

Bin 43: 38274 of cap free
Amount of items: 2
Items: 
Size: 702610 Color: 1
Size: 259117 Color: 3

Bin 44: 39140 of cap free
Amount of items: 2
Items: 
Size: 555241 Color: 0
Size: 405620 Color: 3

Bin 45: 73032 of cap free
Amount of items: 2
Items: 
Size: 741369 Color: 4
Size: 185600 Color: 1

Total size: 44480027
Total free space: 520018

