Capicity Bin: 9504
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4757 Color: 1
Size: 3957 Color: 1
Size: 790 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 1
Size: 4040 Color: 1
Size: 704 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 1
Size: 3954 Color: 1
Size: 788 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4764 Color: 1
Size: 3956 Color: 1
Size: 784 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4765 Color: 1
Size: 3951 Color: 1
Size: 788 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4770 Color: 1
Size: 3946 Color: 1
Size: 788 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4778 Color: 1
Size: 3942 Color: 1
Size: 784 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 1
Size: 3576 Color: 1
Size: 704 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 1
Size: 3452 Color: 1
Size: 688 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 5372 Color: 1
Size: 3444 Color: 1
Size: 688 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 5378 Color: 1
Size: 3442 Color: 1
Size: 684 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 1
Size: 3414 Color: 1
Size: 680 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 1
Size: 3028 Color: 1
Size: 536 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 5943 Color: 1
Size: 2969 Color: 1
Size: 592 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 1
Size: 2872 Color: 1
Size: 560 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6452 Color: 1
Size: 2604 Color: 1
Size: 448 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6537 Color: 1
Size: 2285 Color: 1
Size: 682 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 1
Size: 2600 Color: 1
Size: 298 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 1
Size: 2262 Color: 1
Size: 448 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 1
Size: 2164 Color: 1
Size: 520 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6870 Color: 1
Size: 2198 Color: 1
Size: 436 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 1
Size: 2220 Color: 1
Size: 376 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 1
Size: 2163 Color: 1
Size: 432 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 1
Size: 2142 Color: 1
Size: 428 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 1
Size: 2456 Color: 1
Size: 96 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7067 Color: 1
Size: 1981 Color: 1
Size: 456 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 1
Size: 1948 Color: 1
Size: 384 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 1
Size: 2072 Color: 1
Size: 240 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7256 Color: 1
Size: 1880 Color: 1
Size: 368 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 1
Size: 1742 Color: 1
Size: 504 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 1
Size: 1574 Color: 1
Size: 640 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 1
Size: 1844 Color: 1
Size: 368 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7321 Color: 1
Size: 1821 Color: 1
Size: 362 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7333 Color: 1
Size: 1811 Color: 1
Size: 360 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7414 Color: 1
Size: 1598 Color: 1
Size: 492 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7448 Color: 1
Size: 1900 Color: 1
Size: 156 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7531 Color: 1
Size: 1645 Color: 1
Size: 328 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7548 Color: 1
Size: 1720 Color: 1
Size: 236 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7558 Color: 1
Size: 1622 Color: 1
Size: 324 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7572 Color: 1
Size: 1612 Color: 1
Size: 320 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7573 Color: 1
Size: 1611 Color: 1
Size: 320 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 7584 Color: 1
Size: 1456 Color: 1
Size: 464 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7590 Color: 1
Size: 1320 Color: 1
Size: 594 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 7608 Color: 1
Size: 1736 Color: 1
Size: 160 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 7672 Color: 1
Size: 1460 Color: 1
Size: 372 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 7719 Color: 1
Size: 1557 Color: 1
Size: 228 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 7756 Color: 1
Size: 1252 Color: 1
Size: 496 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7800 Color: 1
Size: 1388 Color: 1
Size: 316 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 7863 Color: 1
Size: 1337 Color: 1
Size: 304 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7864 Color: 1
Size: 1608 Color: 1
Size: 32 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 7878 Color: 1
Size: 1400 Color: 1
Size: 226 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7920 Color: 1
Size: 1328 Color: 1
Size: 256 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7928 Color: 1
Size: 1296 Color: 1
Size: 280 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 7934 Color: 1
Size: 1310 Color: 1
Size: 260 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 7949 Color: 1
Size: 1297 Color: 1
Size: 258 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 8010 Color: 1
Size: 1246 Color: 1
Size: 248 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 8042 Color: 1
Size: 1222 Color: 1
Size: 240 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 8068 Color: 1
Size: 1204 Color: 1
Size: 232 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 1
Size: 1148 Color: 1
Size: 284 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 8078 Color: 1
Size: 1358 Color: 1
Size: 68 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 1
Size: 1172 Color: 1
Size: 248 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 8100 Color: 1
Size: 1188 Color: 1
Size: 216 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 8110 Color: 1
Size: 1166 Color: 1
Size: 228 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 8132 Color: 1
Size: 1200 Color: 1
Size: 172 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 8134 Color: 1
Size: 1098 Color: 1
Size: 272 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 1
Size: 1070 Color: 1
Size: 244 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 1
Size: 986 Color: 1
Size: 296 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 8244 Color: 1
Size: 1048 Color: 1
Size: 212 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 8248 Color: 1
Size: 1080 Color: 1
Size: 176 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 8254 Color: 1
Size: 1042 Color: 1
Size: 208 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 1
Size: 1028 Color: 1
Size: 168 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 1
Size: 914 Color: 1
Size: 250 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 8344 Color: 1
Size: 968 Color: 1
Size: 192 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 8346 Color: 1
Size: 886 Color: 1
Size: 272 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 8368 Color: 1
Size: 972 Color: 1
Size: 164 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 1
Size: 942 Color: 1
Size: 184 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 1
Size: 856 Color: 1
Size: 238 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 8428 Color: 1
Size: 900 Color: 1
Size: 176 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 8470 Color: 1
Size: 922 Color: 1
Size: 112 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 1
Size: 852 Color: 1
Size: 168 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 8488 Color: 1
Size: 922 Color: 1
Size: 94 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 1
Size: 810 Color: 1
Size: 180 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 8516 Color: 1
Size: 828 Color: 1
Size: 160 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 8534 Color: 1
Size: 704 Color: 1
Size: 266 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 8542 Color: 1
Size: 802 Color: 1
Size: 160 Color: 0

Bin 86: 1 of cap free
Amount of items: 3
Items: 
Size: 4754 Color: 1
Size: 3961 Color: 1
Size: 788 Color: 0

Bin 87: 1 of cap free
Amount of items: 3
Items: 
Size: 5413 Color: 1
Size: 3410 Color: 1
Size: 680 Color: 0

Bin 88: 1 of cap free
Amount of items: 3
Items: 
Size: 5874 Color: 1
Size: 3421 Color: 1
Size: 208 Color: 0

Bin 89: 1 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 1
Size: 2905 Color: 1
Size: 692 Color: 0

Bin 90: 1 of cap free
Amount of items: 3
Items: 
Size: 6300 Color: 1
Size: 2979 Color: 1
Size: 224 Color: 0

Bin 91: 1 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 1
Size: 3002 Color: 1
Size: 128 Color: 0

Bin 92: 1 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 1
Size: 2606 Color: 1
Size: 512 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 1
Size: 2475 Color: 1
Size: 528 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 1
Size: 2676 Color: 1
Size: 64 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 6888 Color: 1
Size: 2455 Color: 1
Size: 160 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 1
Size: 2136 Color: 1
Size: 240 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 7541 Color: 1
Size: 1444 Color: 1
Size: 518 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 7707 Color: 1
Size: 1636 Color: 1
Size: 160 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 1
Size: 1369 Color: 1
Size: 336 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 1
Size: 1499 Color: 1
Size: 160 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 7853 Color: 1
Size: 1522 Color: 1
Size: 128 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 7933 Color: 1
Size: 1190 Color: 1
Size: 380 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 7997 Color: 1
Size: 1208 Color: 1
Size: 298 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 8004 Color: 1
Size: 1231 Color: 1
Size: 268 Color: 0

Bin 105: 1 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 1
Size: 1228 Color: 1
Size: 246 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 4756 Color: 1
Size: 3962 Color: 1
Size: 784 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 1
Size: 3964 Color: 1
Size: 192 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 1
Size: 3466 Color: 1
Size: 348 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 5931 Color: 1
Size: 3411 Color: 1
Size: 160 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 1
Size: 2972 Color: 1
Size: 256 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 1
Size: 2740 Color: 1
Size: 406 Color: 0

Bin 112: 2 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 1
Size: 2694 Color: 1
Size: 416 Color: 0

Bin 113: 2 of cap free
Amount of items: 3
Items: 
Size: 7228 Color: 1
Size: 1874 Color: 1
Size: 400 Color: 0

Bin 114: 2 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 1
Size: 1528 Color: 1
Size: 208 Color: 0

Bin 115: 2 of cap free
Amount of items: 3
Items: 
Size: 7901 Color: 1
Size: 1257 Color: 1
Size: 344 Color: 0

Bin 116: 2 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 1
Size: 1142 Color: 1
Size: 144 Color: 0

Bin 117: 3 of cap free
Amount of items: 3
Items: 
Size: 5401 Color: 1
Size: 3504 Color: 1
Size: 596 Color: 0

Bin 118: 3 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 1
Size: 2601 Color: 1
Size: 480 Color: 0

Bin 119: 3 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 1
Size: 2031 Color: 1
Size: 368 Color: 0

Bin 120: 3 of cap free
Amount of items: 3
Items: 
Size: 7780 Color: 1
Size: 1325 Color: 1
Size: 396 Color: 0

Bin 121: 4 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 1
Size: 3192 Color: 1
Size: 432 Color: 0

Bin 122: 4 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 1
Size: 2548 Color: 1
Size: 288 Color: 0

Bin 123: 6 of cap free
Amount of items: 3
Items: 
Size: 6518 Color: 1
Size: 2376 Color: 1
Size: 604 Color: 0

Bin 124: 6 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 1
Size: 976 Color: 0
Size: 80 Color: 0

Bin 125: 10 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 1
Size: 2418 Color: 1
Size: 232 Color: 0

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 1
Size: 3026 Color: 1
Size: 528 Color: 0

Bin 127: 13 of cap free
Amount of items: 3
Items: 
Size: 6751 Color: 1
Size: 2508 Color: 1
Size: 232 Color: 0

Bin 128: 18 of cap free
Amount of items: 2
Items: 
Size: 7678 Color: 1
Size: 1808 Color: 0

Bin 129: 24 of cap free
Amount of items: 3
Items: 
Size: 4688 Color: 1
Size: 4192 Color: 1
Size: 600 Color: 0

Bin 130: 95 of cap free
Amount of items: 5
Items: 
Size: 4761 Color: 1
Size: 2628 Color: 1
Size: 790 Color: 0
Size: 790 Color: 0
Size: 440 Color: 0

Bin 131: 174 of cap free
Amount of items: 3
Items: 
Size: 4753 Color: 1
Size: 3953 Color: 1
Size: 624 Color: 0

Bin 132: 1002 of cap free
Amount of items: 8
Items: 
Size: 2002 Color: 1
Size: 1846 Color: 1
Size: 1725 Color: 1
Size: 1489 Color: 1
Size: 592 Color: 0
Size: 320 Color: 0
Size: 304 Color: 0
Size: 224 Color: 0

Bin 133: 8082 of cap free
Amount of items: 1
Items: 
Size: 1422 Color: 1

Total size: 1254528
Total free space: 9504

