Capicity Bin: 8120
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4070 Color: 277
Size: 3378 Color: 266
Size: 332 Color: 95
Size: 172 Color: 39
Size: 168 Color: 37

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 314
Size: 992 Color: 178
Size: 884 Color: 166

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 317
Size: 1531 Color: 222
Size: 306 Color: 90

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 327
Size: 1394 Color: 214
Size: 276 Color: 83

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 332
Size: 1366 Color: 209
Size: 272 Color: 76

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 339
Size: 882 Color: 165
Size: 576 Color: 124

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 346
Size: 1135 Color: 195
Size: 226 Color: 68

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 347
Size: 988 Color: 177
Size: 370 Color: 100

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6767 Color: 349
Size: 1129 Color: 192
Size: 224 Color: 66

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6881 Color: 355
Size: 1033 Color: 184
Size: 206 Color: 58

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6897 Color: 357
Size: 1021 Color: 182
Size: 202 Color: 55

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6913 Color: 359
Size: 1001 Color: 180
Size: 206 Color: 59

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 360
Size: 900 Color: 170
Size: 284 Color: 85

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 361
Size: 796 Color: 155
Size: 384 Color: 104

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7014 Color: 366
Size: 1092 Color: 189
Size: 14 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 367
Size: 891 Color: 168
Size: 188 Color: 49

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 368
Size: 692 Color: 140
Size: 384 Color: 103

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7053 Color: 370
Size: 793 Color: 154
Size: 274 Color: 79

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7064 Color: 371
Size: 584 Color: 128
Size: 472 Color: 115

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7103 Color: 376
Size: 849 Color: 161
Size: 168 Color: 38

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 378
Size: 506 Color: 120
Size: 480 Color: 116

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7141 Color: 379
Size: 731 Color: 146
Size: 248 Color: 74

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7145 Color: 380
Size: 747 Color: 148
Size: 228 Color: 71

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 385
Size: 802 Color: 156
Size: 136 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7203 Color: 387
Size: 765 Color: 151
Size: 152 Color: 29

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 388
Size: 676 Color: 135
Size: 240 Color: 72

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7243 Color: 392
Size: 725 Color: 145
Size: 152 Color: 27

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7251 Color: 394
Size: 691 Color: 139
Size: 178 Color: 44

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 395
Size: 676 Color: 136
Size: 192 Color: 52

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7265 Color: 397
Size: 713 Color: 142
Size: 142 Color: 18

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 398
Size: 682 Color: 138
Size: 164 Color: 35

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7291 Color: 399
Size: 681 Color: 137
Size: 148 Color: 23

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 400
Size: 672 Color: 133
Size: 156 Color: 30

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 6291 Color: 318
Size: 1156 Color: 199
Size: 672 Color: 132

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 328
Size: 1525 Color: 221
Size: 136 Color: 15

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 329
Size: 1654 Color: 227

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 330
Size: 1134 Color: 194
Size: 512 Color: 121

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6481 Color: 331
Size: 1270 Color: 204
Size: 368 Color: 99

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 335
Size: 1547 Color: 224

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 337
Size: 1490 Color: 220

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6738 Color: 342
Size: 1381 Color: 212

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6746 Color: 344
Size: 1373 Color: 211

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6755 Color: 345
Size: 1364 Color: 208

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 348
Size: 764 Color: 150
Size: 592 Color: 129

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 353
Size: 1154 Color: 198
Size: 92 Color: 4

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 7049 Color: 369
Size: 782 Color: 152
Size: 288 Color: 86

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 7074 Color: 374
Size: 1045 Color: 188

Bin 48: 1 of cap free
Amount of items: 2
Items: 
Size: 7164 Color: 382
Size: 955 Color: 175

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 7174 Color: 384
Size: 945 Color: 174

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 7306 Color: 402
Size: 813 Color: 159

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5498 Color: 298
Size: 2482 Color: 251
Size: 138 Color: 16

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 308
Size: 1945 Color: 240
Size: 128 Color: 7

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6090 Color: 310
Size: 2028 Color: 242

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 6354 Color: 321
Size: 1564 Color: 225
Size: 200 Color: 54

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6442 Color: 326
Size: 1676 Color: 229

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 333
Size: 1386 Color: 213
Size: 248 Color: 73

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6642 Color: 338
Size: 1476 Color: 219

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6972 Color: 362
Size: 1146 Color: 197

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6987 Color: 364
Size: 1131 Color: 193

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 365
Size: 1124 Color: 191

Bin 61: 2 of cap free
Amount of items: 2
Items: 
Size: 7225 Color: 390
Size: 893 Color: 169

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 7244 Color: 393
Size: 874 Color: 163

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 5908 Color: 306
Size: 2209 Color: 247

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 315
Size: 1544 Color: 223
Size: 308 Color: 91

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6874 Color: 354
Size: 1243 Color: 202

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 7230 Color: 391
Size: 887 Color: 167

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 401
Size: 812 Color: 158

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 6867 Color: 352
Size: 1249 Color: 203

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 6898 Color: 358
Size: 1218 Color: 200

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 7254 Color: 396
Size: 862 Color: 162

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 5073 Color: 287
Size: 2894 Color: 258
Size: 148 Color: 24

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 302
Size: 2194 Color: 245
Size: 134 Color: 10

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 6271 Color: 316
Size: 1844 Color: 235

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6402 Color: 324
Size: 1713 Color: 231

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 389
Size: 901 Color: 171

Bin 76: 6 of cap free
Amount of items: 5
Items: 
Size: 4634 Color: 283
Size: 2906 Color: 259
Size: 256 Color: 75
Size: 160 Color: 33
Size: 158 Color: 32

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 312
Size: 1918 Color: 238
Size: 80 Color: 3

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 320
Size: 1780 Color: 234

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6680 Color: 341
Size: 1434 Color: 217

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6882 Color: 356
Size: 1232 Color: 201

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 6975 Color: 363
Size: 1139 Color: 196

Bin 82: 8 of cap free
Amount of items: 3
Items: 
Size: 6138 Color: 313
Size: 1894 Color: 237
Size: 80 Color: 2

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 375
Size: 1022 Color: 183

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 7105 Color: 377
Size: 1007 Color: 181

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 7148 Color: 381
Size: 964 Color: 176

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 7190 Color: 386
Size: 922 Color: 172

Bin 87: 9 of cap free
Amount of items: 3
Items: 
Size: 6051 Color: 309
Size: 1932 Color: 239
Size: 128 Color: 6

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 7169 Color: 383
Size: 942 Color: 173

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 303
Size: 2174 Color: 243
Size: 132 Color: 9

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 6504 Color: 334
Size: 1606 Color: 226

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 373
Size: 1042 Color: 187

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 343
Size: 1367 Color: 210

Bin 93: 13 of cap free
Amount of items: 2
Items: 
Size: 7066 Color: 372
Size: 1041 Color: 186

Bin 94: 15 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 293
Size: 2677 Color: 256
Size: 144 Color: 19

Bin 95: 16 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 304
Size: 1402 Color: 215
Size: 880 Color: 164

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 6812 Color: 351
Size: 1292 Color: 205

Bin 97: 17 of cap free
Amount of items: 7
Items: 
Size: 4063 Color: 275
Size: 1312 Color: 206
Size: 1120 Color: 190
Size: 1034 Color: 185
Size: 226 Color: 69
Size: 176 Color: 41
Size: 172 Color: 40

Bin 98: 17 of cap free
Amount of items: 3
Items: 
Size: 5781 Color: 301
Size: 2186 Color: 244
Size: 136 Color: 12

Bin 99: 18 of cap free
Amount of items: 7
Items: 
Size: 4062 Color: 274
Size: 1000 Color: 179
Size: 822 Color: 160
Size: 804 Color: 157
Size: 790 Color: 153
Size: 448 Color: 114
Size: 176 Color: 42

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 6410 Color: 325
Size: 1692 Color: 230

Bin 101: 19 of cap free
Amount of items: 2
Items: 
Size: 5093 Color: 289
Size: 3008 Color: 264

Bin 102: 20 of cap free
Amount of items: 5
Items: 
Size: 4650 Color: 284
Size: 2914 Color: 260
Size: 228 Color: 70
Size: 156 Color: 31
Size: 152 Color: 28

Bin 103: 23 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 336
Size: 1474 Color: 218

Bin 104: 24 of cap free
Amount of items: 2
Items: 
Size: 6780 Color: 350
Size: 1316 Color: 207

Bin 105: 26 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 340
Size: 1426 Color: 216

Bin 106: 27 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 323
Size: 1731 Color: 233

Bin 107: 28 of cap free
Amount of items: 3
Items: 
Size: 5102 Color: 290
Size: 2844 Color: 257
Size: 146 Color: 22

Bin 108: 28 of cap free
Amount of items: 3
Items: 
Size: 6114 Color: 311
Size: 1882 Color: 236
Size: 96 Color: 5

Bin 109: 32 of cap free
Amount of items: 18
Items: 
Size: 676 Color: 134
Size: 640 Color: 131
Size: 632 Color: 130
Size: 584 Color: 127
Size: 580 Color: 126
Size: 580 Color: 125
Size: 560 Color: 123
Size: 520 Color: 122
Size: 504 Color: 119
Size: 500 Color: 118
Size: 496 Color: 117
Size: 442 Color: 113
Size: 440 Color: 112
Size: 192 Color: 51
Size: 190 Color: 50
Size: 184 Color: 48
Size: 184 Color: 47
Size: 184 Color: 46

Bin 110: 32 of cap free
Amount of items: 2
Items: 
Size: 5476 Color: 296
Size: 2612 Color: 255

Bin 111: 34 of cap free
Amount of items: 26
Items: 
Size: 440 Color: 111
Size: 436 Color: 110
Size: 436 Color: 109
Size: 432 Color: 108
Size: 400 Color: 107
Size: 388 Color: 106
Size: 388 Color: 105
Size: 380 Color: 102
Size: 376 Color: 101
Size: 352 Color: 98
Size: 344 Color: 97
Size: 344 Color: 96
Size: 328 Color: 94
Size: 328 Color: 93
Size: 312 Color: 92
Size: 272 Color: 77
Size: 226 Color: 67
Size: 224 Color: 65
Size: 224 Color: 64
Size: 216 Color: 63
Size: 216 Color: 62
Size: 208 Color: 61
Size: 208 Color: 60
Size: 204 Color: 57
Size: 204 Color: 56
Size: 200 Color: 53

Bin 112: 36 of cap free
Amount of items: 2
Items: 
Size: 4068 Color: 276
Size: 4016 Color: 272

Bin 113: 38 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 279
Size: 3380 Color: 267
Size: 162 Color: 34

Bin 114: 39 of cap free
Amount of items: 2
Items: 
Size: 6356 Color: 322
Size: 1725 Color: 232

Bin 115: 43 of cap free
Amount of items: 2
Items: 
Size: 5862 Color: 305
Size: 2215 Color: 248

Bin 116: 50 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 299
Size: 2420 Color: 250
Size: 136 Color: 14

Bin 117: 51 of cap free
Amount of items: 2
Items: 
Size: 5081 Color: 288
Size: 2988 Color: 263

Bin 118: 53 of cap free
Amount of items: 8
Items: 
Size: 4061 Color: 273
Size: 758 Color: 149
Size: 742 Color: 147
Size: 724 Color: 144
Size: 722 Color: 143
Size: 706 Color: 141
Size: 178 Color: 45
Size: 176 Color: 43

Bin 119: 53 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 307
Size: 1951 Color: 241
Size: 128 Color: 8

Bin 120: 55 of cap free
Amount of items: 2
Items: 
Size: 5142 Color: 291
Size: 2923 Color: 262

Bin 121: 60 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 278
Size: 3568 Color: 271
Size: 168 Color: 36

Bin 122: 63 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 286
Size: 2921 Color: 261
Size: 148 Color: 25

Bin 123: 74 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 319
Size: 1674 Color: 228
Size: 40 Color: 1

Bin 124: 88 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 285
Size: 3164 Color: 265
Size: 152 Color: 26

Bin 125: 88 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 300
Size: 2204 Color: 246
Size: 136 Color: 13

Bin 126: 94 of cap free
Amount of items: 4
Items: 
Size: 5220 Color: 292
Size: 2518 Color: 252
Size: 144 Color: 21
Size: 144 Color: 20

Bin 127: 111 of cap free
Amount of items: 2
Items: 
Size: 4626 Color: 282
Size: 3383 Color: 270

Bin 128: 116 of cap free
Amount of items: 2
Items: 
Size: 5471 Color: 295
Size: 2533 Color: 254

Bin 129: 123 of cap free
Amount of items: 2
Items: 
Size: 4615 Color: 281
Size: 3382 Color: 269

Bin 130: 126 of cap free
Amount of items: 2
Items: 
Size: 4613 Color: 280
Size: 3381 Color: 268

Bin 131: 126 of cap free
Amount of items: 3
Items: 
Size: 5490 Color: 297
Size: 2364 Color: 249
Size: 140 Color: 17

Bin 132: 134 of cap free
Amount of items: 2
Items: 
Size: 5463 Color: 294
Size: 2523 Color: 253

Bin 133: 5846 of cap free
Amount of items: 8
Items: 
Size: 304 Color: 89
Size: 296 Color: 88
Size: 292 Color: 87
Size: 284 Color: 84
Size: 276 Color: 82
Size: 276 Color: 81
Size: 274 Color: 80
Size: 272 Color: 78

Total size: 1071840
Total free space: 8120

