Capicity Bin: 5392
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3326 Color: 294
Size: 1966 Color: 257
Size: 100 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3692 Color: 313
Size: 1500 Color: 236
Size: 200 Color: 79

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3699 Color: 314
Size: 1085 Color: 205
Size: 608 Color: 156

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3784 Color: 319
Size: 1484 Color: 234
Size: 124 Color: 34

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3804 Color: 320
Size: 1420 Color: 227
Size: 168 Color: 59

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3916 Color: 324
Size: 1302 Color: 220
Size: 174 Color: 61

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4014 Color: 329
Size: 994 Color: 199
Size: 384 Color: 119

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4020 Color: 330
Size: 740 Color: 173
Size: 632 Color: 160

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4024 Color: 331
Size: 764 Color: 176
Size: 604 Color: 155

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4052 Color: 333
Size: 1144 Color: 209
Size: 196 Color: 77

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 338
Size: 1120 Color: 207
Size: 100 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4182 Color: 339
Size: 1146 Color: 210
Size: 64 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4280 Color: 345
Size: 982 Color: 198
Size: 130 Color: 41

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4316 Color: 347
Size: 924 Color: 193
Size: 152 Color: 51

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4318 Color: 348
Size: 898 Color: 191
Size: 176 Color: 66

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 349
Size: 540 Color: 144
Size: 522 Color: 143

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4341 Color: 351
Size: 769 Color: 178
Size: 282 Color: 100

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4345 Color: 352
Size: 871 Color: 187
Size: 176 Color: 67

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4471 Color: 361
Size: 791 Color: 180
Size: 130 Color: 40

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 362
Size: 700 Color: 169
Size: 216 Color: 82

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4552 Color: 369
Size: 454 Color: 136
Size: 386 Color: 121

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4556 Color: 370
Size: 440 Color: 131
Size: 396 Color: 125

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4603 Color: 374
Size: 649 Color: 162
Size: 140 Color: 45

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4607 Color: 375
Size: 609 Color: 157
Size: 176 Color: 65

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4611 Color: 376
Size: 651 Color: 163
Size: 130 Color: 42

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4612 Color: 377
Size: 564 Color: 147
Size: 216 Color: 83

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4662 Color: 382
Size: 594 Color: 153
Size: 136 Color: 44

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4680 Color: 385
Size: 600 Color: 154
Size: 112 Color: 20

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4707 Color: 388
Size: 571 Color: 150
Size: 114 Color: 27

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4712 Color: 389
Size: 496 Color: 140
Size: 184 Color: 68

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4715 Color: 390
Size: 565 Color: 148
Size: 112 Color: 21

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4744 Color: 393
Size: 592 Color: 152
Size: 56 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 394
Size: 400 Color: 127
Size: 244 Color: 91

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4770 Color: 395
Size: 470 Color: 137
Size: 152 Color: 52

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4802 Color: 398
Size: 344 Color: 116
Size: 246 Color: 92

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 399
Size: 396 Color: 126
Size: 192 Color: 72

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 400
Size: 386 Color: 123
Size: 176 Color: 63

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 4832 Color: 401
Size: 416 Color: 128
Size: 144 Color: 48

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 4850 Color: 402
Size: 368 Color: 118
Size: 174 Color: 62

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 3034 Color: 285
Size: 2245 Color: 267
Size: 112 Color: 23

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 3354 Color: 297
Size: 1941 Color: 256
Size: 96 Color: 12

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 3644 Color: 308
Size: 1395 Color: 224
Size: 352 Color: 117

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 3685 Color: 312
Size: 1706 Color: 243

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 3712 Color: 315
Size: 1679 Color: 241

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 4006 Color: 328
Size: 1385 Color: 223

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 4085 Color: 334
Size: 1306 Color: 221

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 4420 Color: 357
Size: 971 Color: 196

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 4439 Color: 358
Size: 568 Color: 149
Size: 384 Color: 120

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 4443 Color: 359
Size: 948 Color: 195

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 4518 Color: 365
Size: 873 Color: 188

Bin 51: 1 of cap free
Amount of items: 2
Items: 
Size: 4543 Color: 368
Size: 848 Color: 184

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 4615 Color: 378
Size: 776 Color: 179

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 4661 Color: 381
Size: 386 Color: 122
Size: 344 Color: 114

Bin 54: 1 of cap free
Amount of items: 2
Items: 
Size: 4668 Color: 384
Size: 723 Color: 172

Bin 55: 1 of cap free
Amount of items: 2
Items: 
Size: 4682 Color: 386
Size: 709 Color: 170

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 4780 Color: 397
Size: 611 Color: 159

Bin 57: 2 of cap free
Amount of items: 7
Items: 
Size: 2700 Color: 275
Size: 546 Color: 145
Size: 516 Color: 142
Size: 514 Color: 141
Size: 494 Color: 139
Size: 492 Color: 138
Size: 128 Color: 35

Bin 58: 2 of cap free
Amount of items: 7
Items: 
Size: 2702 Color: 276
Size: 655 Color: 165
Size: 638 Color: 161
Size: 575 Color: 151
Size: 552 Color: 146
Size: 148 Color: 50
Size: 120 Color: 33

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 3580 Color: 304
Size: 1486 Color: 235
Size: 324 Color: 109

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 3826 Color: 321
Size: 1436 Color: 229
Size: 128 Color: 39

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 3834 Color: 322
Size: 1448 Color: 231
Size: 108 Color: 19

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 4202 Color: 340
Size: 1172 Color: 214
Size: 16 Color: 1

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 4218 Color: 342
Size: 1172 Color: 213

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 4578 Color: 371
Size: 812 Color: 183

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 4630 Color: 379
Size: 760 Color: 175

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 4648 Color: 380
Size: 742 Color: 174

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 4738 Color: 392
Size: 652 Color: 164

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 3921 Color: 325
Size: 1468 Color: 233

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 4168 Color: 337
Size: 1221 Color: 216

Bin 70: 3 of cap free
Amount of items: 2
Items: 
Size: 4703 Color: 387
Size: 686 Color: 168

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 3656 Color: 310
Size: 1732 Color: 246

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 3731 Color: 317
Size: 1625 Color: 238
Size: 32 Color: 2

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 4488 Color: 363
Size: 900 Color: 192

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 4526 Color: 367
Size: 862 Color: 186

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 396
Size: 610 Color: 158

Bin 76: 5 of cap free
Amount of items: 9
Items: 
Size: 2699 Color: 274
Size: 448 Color: 135
Size: 448 Color: 134
Size: 448 Color: 133
Size: 448 Color: 132
Size: 440 Color: 130
Size: 200 Color: 78
Size: 128 Color: 37
Size: 128 Color: 36

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 3077 Color: 289
Size: 2310 Color: 270

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 3379 Color: 299
Size: 2008 Color: 260

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 3633 Color: 307
Size: 1702 Color: 242
Size: 52 Color: 3

Bin 80: 5 of cap free
Amount of items: 3
Items: 
Size: 3740 Color: 318
Size: 1631 Color: 239
Size: 16 Color: 0

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 4221 Color: 343
Size: 1166 Color: 212

Bin 82: 5 of cap free
Amount of items: 2
Items: 
Size: 4335 Color: 350
Size: 1052 Color: 204

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 4506 Color: 364
Size: 881 Color: 190

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 4716 Color: 391
Size: 671 Color: 167

Bin 85: 6 of cap free
Amount of items: 2
Items: 
Size: 4038 Color: 332
Size: 1348 Color: 222

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 4376 Color: 355
Size: 1010 Color: 201

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 4450 Color: 360
Size: 936 Color: 194

Bin 88: 7 of cap free
Amount of items: 2
Items: 
Size: 3927 Color: 326
Size: 1458 Color: 232

Bin 89: 7 of cap free
Amount of items: 2
Items: 
Size: 4663 Color: 383
Size: 722 Color: 171

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 3180 Color: 290
Size: 2092 Color: 261
Size: 112 Color: 22

Bin 91: 8 of cap free
Amount of items: 4
Items: 
Size: 3416 Color: 301
Size: 1816 Color: 250
Size: 88 Color: 9
Size: 64 Color: 7

Bin 92: 8 of cap free
Amount of items: 2
Items: 
Size: 3676 Color: 311
Size: 1708 Color: 244

Bin 93: 8 of cap free
Amount of items: 2
Items: 
Size: 3988 Color: 327
Size: 1396 Color: 225

Bin 94: 8 of cap free
Amount of items: 2
Items: 
Size: 4204 Color: 341
Size: 1180 Color: 215

Bin 95: 8 of cap free
Amount of items: 2
Items: 
Size: 4388 Color: 356
Size: 996 Color: 200

Bin 96: 8 of cap free
Amount of items: 2
Items: 
Size: 4589 Color: 373
Size: 795 Color: 182

Bin 97: 9 of cap free
Amount of items: 2
Items: 
Size: 3612 Color: 306
Size: 1771 Color: 249

Bin 98: 9 of cap free
Amount of items: 2
Items: 
Size: 4292 Color: 346
Size: 1091 Color: 206

Bin 99: 10 of cap free
Amount of items: 2
Items: 
Size: 4362 Color: 354
Size: 1020 Color: 202

Bin 100: 10 of cap free
Amount of items: 2
Items: 
Size: 4588 Color: 372
Size: 794 Color: 181

Bin 101: 11 of cap free
Amount of items: 3
Items: 
Size: 3348 Color: 296
Size: 1937 Color: 255
Size: 96 Color: 13

Bin 102: 11 of cap free
Amount of items: 2
Items: 
Size: 4349 Color: 353
Size: 1032 Color: 203

Bin 103: 11 of cap free
Amount of items: 2
Items: 
Size: 4525 Color: 366
Size: 856 Color: 185

Bin 104: 12 of cap free
Amount of items: 5
Items: 
Size: 2740 Color: 280
Size: 2228 Color: 263
Size: 176 Color: 64
Size: 120 Color: 30
Size: 116 Color: 29

Bin 105: 12 of cap free
Amount of items: 2
Items: 
Size: 3864 Color: 323
Size: 1516 Color: 237

Bin 106: 12 of cap free
Amount of items: 2
Items: 
Size: 4132 Color: 336
Size: 1248 Color: 218

Bin 107: 13 of cap free
Amount of items: 2
Items: 
Size: 4091 Color: 335
Size: 1288 Color: 219

Bin 108: 13 of cap free
Amount of items: 2
Items: 
Size: 4229 Color: 344
Size: 1150 Color: 211

Bin 109: 14 of cap free
Amount of items: 3
Items: 
Size: 3284 Color: 292
Size: 1986 Color: 258
Size: 108 Color: 18

Bin 110: 17 of cap free
Amount of items: 3
Items: 
Size: 3346 Color: 295
Size: 1933 Color: 254
Size: 96 Color: 14

Bin 111: 17 of cap free
Amount of items: 2
Items: 
Size: 3719 Color: 316
Size: 1656 Color: 240

Bin 112: 18 of cap free
Amount of items: 2
Items: 
Size: 3610 Color: 305
Size: 1764 Color: 248

Bin 113: 19 of cap free
Amount of items: 3
Items: 
Size: 3073 Color: 288
Size: 1423 Color: 228
Size: 877 Color: 189

Bin 114: 22 of cap free
Amount of items: 2
Items: 
Size: 3376 Color: 298
Size: 1994 Color: 259

Bin 115: 24 of cap free
Amount of items: 2
Items: 
Size: 3224 Color: 291
Size: 2144 Color: 262

Bin 116: 24 of cap free
Amount of items: 2
Items: 
Size: 3646 Color: 309
Size: 1722 Color: 245

Bin 117: 26 of cap free
Amount of items: 3
Items: 
Size: 3010 Color: 284
Size: 2244 Color: 266
Size: 112 Color: 24

Bin 118: 30 of cap free
Amount of items: 3
Items: 
Size: 2724 Color: 279
Size: 1411 Color: 226
Size: 1227 Color: 217

Bin 119: 34 of cap free
Amount of items: 3
Items: 
Size: 3002 Color: 283
Size: 2244 Color: 265
Size: 112 Color: 25

Bin 120: 38 of cap free
Amount of items: 3
Items: 
Size: 2884 Color: 281
Size: 2356 Color: 271
Size: 114 Color: 28

Bin 121: 41 of cap free
Amount of items: 3
Items: 
Size: 3316 Color: 293
Size: 1931 Color: 253
Size: 104 Color: 17

Bin 122: 41 of cap free
Amount of items: 3
Items: 
Size: 3443 Color: 303
Size: 1844 Color: 252
Size: 64 Color: 5

Bin 123: 42 of cap free
Amount of items: 6
Items: 
Size: 2708 Color: 277
Size: 977 Color: 197
Size: 766 Color: 177
Size: 659 Color: 166
Size: 120 Color: 32
Size: 120 Color: 31

Bin 124: 54 of cap free
Amount of items: 3
Items: 
Size: 2984 Color: 282
Size: 2242 Color: 264
Size: 112 Color: 26

Bin 125: 57 of cap free
Amount of items: 3
Items: 
Size: 3431 Color: 302
Size: 1840 Color: 251
Size: 64 Color: 6

Bin 126: 58 of cap free
Amount of items: 4
Items: 
Size: 3383 Color: 300
Size: 1763 Color: 247
Size: 96 Color: 11
Size: 92 Color: 10

Bin 127: 75 of cap free
Amount of items: 2
Items: 
Size: 3069 Color: 287
Size: 2248 Color: 269

Bin 128: 80 of cap free
Amount of items: 2
Items: 
Size: 3065 Color: 286
Size: 2247 Color: 268

Bin 129: 93 of cap free
Amount of items: 12
Items: 
Size: 2697 Color: 272
Size: 334 Color: 110
Size: 320 Color: 108
Size: 296 Color: 107
Size: 296 Color: 106
Size: 296 Color: 105
Size: 296 Color: 104
Size: 160 Color: 55
Size: 158 Color: 54
Size: 158 Color: 53
Size: 144 Color: 49
Size: 144 Color: 47

Bin 130: 102 of cap free
Amount of items: 3
Items: 
Size: 2712 Color: 278
Size: 1448 Color: 230
Size: 1130 Color: 208

Bin 131: 110 of cap free
Amount of items: 10
Items: 
Size: 2698 Color: 273
Size: 432 Color: 129
Size: 392 Color: 124
Size: 344 Color: 115
Size: 340 Color: 113
Size: 336 Color: 112
Size: 336 Color: 111
Size: 144 Color: 46
Size: 132 Color: 43
Size: 128 Color: 38

Bin 132: 136 of cap free
Amount of items: 23
Items: 
Size: 288 Color: 103
Size: 288 Color: 102
Size: 284 Color: 101
Size: 280 Color: 99
Size: 280 Color: 98
Size: 278 Color: 97
Size: 276 Color: 96
Size: 260 Color: 95
Size: 256 Color: 94
Size: 248 Color: 93
Size: 244 Color: 90
Size: 240 Color: 89
Size: 232 Color: 88
Size: 192 Color: 75
Size: 192 Color: 74
Size: 192 Color: 73
Size: 192 Color: 71
Size: 192 Color: 70
Size: 190 Color: 69
Size: 172 Color: 60
Size: 160 Color: 58
Size: 160 Color: 57
Size: 160 Color: 56

Bin 133: 3886 of cap free
Amount of items: 7
Items: 
Size: 228 Color: 87
Size: 224 Color: 86
Size: 224 Color: 85
Size: 220 Color: 84
Size: 208 Color: 81
Size: 208 Color: 80
Size: 194 Color: 76

Total size: 711744
Total free space: 5392

