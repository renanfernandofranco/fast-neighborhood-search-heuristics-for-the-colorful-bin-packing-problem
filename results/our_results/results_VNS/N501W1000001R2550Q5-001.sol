Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 409329 Color: 3
Size: 326266 Color: 4
Size: 264406 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 477255 Color: 2
Size: 262135 Color: 1
Size: 260611 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 423937 Color: 4
Size: 293879 Color: 3
Size: 282185 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 348495 Color: 2
Size: 337204 Color: 0
Size: 314302 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 467482 Color: 0
Size: 270339 Color: 3
Size: 262180 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 369369 Color: 2
Size: 357245 Color: 1
Size: 273387 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 487574 Color: 2
Size: 259312 Color: 1
Size: 253115 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 446572 Color: 3
Size: 301991 Color: 4
Size: 251438 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 398207 Color: 0
Size: 306844 Color: 2
Size: 294950 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 461273 Color: 4
Size: 276617 Color: 3
Size: 262111 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 373503 Color: 3
Size: 331393 Color: 2
Size: 295105 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 409938 Color: 2
Size: 311718 Color: 1
Size: 278345 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 413798 Color: 0
Size: 331443 Color: 2
Size: 254760 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 447010 Color: 0
Size: 284255 Color: 1
Size: 268736 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 456371 Color: 2
Size: 292191 Color: 2
Size: 251439 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 367919 Color: 4
Size: 328688 Color: 4
Size: 303394 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 387020 Color: 2
Size: 308526 Color: 4
Size: 304455 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 375420 Color: 4
Size: 344434 Color: 1
Size: 280147 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 369234 Color: 2
Size: 365375 Color: 1
Size: 265392 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 375136 Color: 4
Size: 359694 Color: 0
Size: 265171 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 407295 Color: 4
Size: 300132 Color: 1
Size: 292574 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 391276 Color: 1
Size: 348464 Color: 0
Size: 260261 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 466521 Color: 2
Size: 273620 Color: 3
Size: 259860 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 480757 Color: 0
Size: 263473 Color: 3
Size: 255771 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 459202 Color: 2
Size: 284920 Color: 4
Size: 255879 Color: 4

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 406337 Color: 1
Size: 316426 Color: 1
Size: 277238 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 401349 Color: 3
Size: 341513 Color: 1
Size: 257139 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 374666 Color: 1
Size: 315144 Color: 4
Size: 310191 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 407839 Color: 0
Size: 318080 Color: 3
Size: 274082 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 359830 Color: 4
Size: 352829 Color: 2
Size: 287342 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 382202 Color: 2
Size: 338542 Color: 3
Size: 279257 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 336888 Color: 2
Size: 343892 Color: 3
Size: 319221 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 423104 Color: 3
Size: 303167 Color: 1
Size: 273730 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 438168 Color: 0
Size: 287422 Color: 3
Size: 274411 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 397021 Color: 4
Size: 346407 Color: 1
Size: 256573 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 431010 Color: 2
Size: 303712 Color: 0
Size: 265279 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 384225 Color: 1
Size: 340093 Color: 4
Size: 275683 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 397557 Color: 2
Size: 316099 Color: 2
Size: 286345 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 412105 Color: 2
Size: 329731 Color: 0
Size: 258165 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 472791 Color: 0
Size: 267632 Color: 0
Size: 259578 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 370864 Color: 4
Size: 315091 Color: 0
Size: 314046 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 482860 Color: 1
Size: 261810 Color: 0
Size: 255331 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 443143 Color: 0
Size: 294093 Color: 0
Size: 262765 Color: 4

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 424094 Color: 3
Size: 323471 Color: 4
Size: 252436 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 390574 Color: 3
Size: 356952 Color: 1
Size: 252475 Color: 2

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 405346 Color: 2
Size: 340381 Color: 1
Size: 254274 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 353376 Color: 2
Size: 326267 Color: 4
Size: 320358 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 418731 Color: 2
Size: 314128 Color: 0
Size: 267142 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 410338 Color: 1
Size: 333150 Color: 3
Size: 256513 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 369322 Color: 2
Size: 329860 Color: 1
Size: 300819 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 458187 Color: 4
Size: 274252 Color: 2
Size: 267562 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 454467 Color: 4
Size: 287050 Color: 2
Size: 258484 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 366775 Color: 0
Size: 366553 Color: 4
Size: 266673 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 349892 Color: 4
Size: 325761 Color: 3
Size: 324348 Color: 1

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 383868 Color: 2
Size: 314562 Color: 0
Size: 301571 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 425035 Color: 0
Size: 311217 Color: 1
Size: 263749 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 492886 Color: 1
Size: 256098 Color: 2
Size: 251017 Color: 2

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 389395 Color: 3
Size: 356493 Color: 2
Size: 254113 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 406523 Color: 3
Size: 339960 Color: 3
Size: 253518 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 411038 Color: 3
Size: 307938 Color: 3
Size: 281025 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 451353 Color: 2
Size: 287492 Color: 1
Size: 261156 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 433346 Color: 0
Size: 293387 Color: 0
Size: 273268 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 397168 Color: 1
Size: 324548 Color: 2
Size: 278285 Color: 2

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 376595 Color: 3
Size: 334806 Color: 0
Size: 288600 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 380090 Color: 1
Size: 344350 Color: 3
Size: 275561 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 386099 Color: 3
Size: 324536 Color: 0
Size: 289366 Color: 2

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 383411 Color: 1
Size: 324812 Color: 2
Size: 291778 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 373259 Color: 3
Size: 372531 Color: 3
Size: 254211 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 383420 Color: 3
Size: 350787 Color: 1
Size: 265794 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 430438 Color: 2
Size: 287568 Color: 4
Size: 281995 Color: 4

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 479321 Color: 3
Size: 266953 Color: 3
Size: 253727 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 388895 Color: 2
Size: 328381 Color: 1
Size: 282725 Color: 3

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 367615 Color: 0
Size: 328721 Color: 0
Size: 303665 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 436014 Color: 4
Size: 288609 Color: 4
Size: 275378 Color: 2

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 394179 Color: 2
Size: 326452 Color: 0
Size: 279370 Color: 2

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 380929 Color: 2
Size: 364002 Color: 3
Size: 255070 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 461602 Color: 2
Size: 274906 Color: 3
Size: 263493 Color: 4

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 427351 Color: 1
Size: 316656 Color: 3
Size: 255994 Color: 2

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 384915 Color: 0
Size: 355252 Color: 0
Size: 259834 Color: 4

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 447111 Color: 3
Size: 294191 Color: 1
Size: 258699 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 376845 Color: 4
Size: 335473 Color: 4
Size: 287683 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 480648 Color: 3
Size: 264534 Color: 1
Size: 254819 Color: 4

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 385462 Color: 3
Size: 320410 Color: 0
Size: 294129 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 354604 Color: 0
Size: 344924 Color: 3
Size: 300473 Color: 4

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 482859 Color: 3
Size: 265778 Color: 2
Size: 251364 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 458142 Color: 2
Size: 289095 Color: 3
Size: 252764 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 499767 Color: 1
Size: 250148 Color: 3
Size: 250086 Color: 2

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 422100 Color: 3
Size: 305329 Color: 3
Size: 272572 Color: 4

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 380463 Color: 2
Size: 322844 Color: 2
Size: 296694 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 380149 Color: 2
Size: 359708 Color: 4
Size: 260144 Color: 2

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 469368 Color: 3
Size: 272330 Color: 4
Size: 258303 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 366015 Color: 1
Size: 339754 Color: 0
Size: 294232 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 384968 Color: 1
Size: 351958 Color: 4
Size: 263075 Color: 2

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 397980 Color: 4
Size: 312395 Color: 4
Size: 289626 Color: 3

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 413368 Color: 0
Size: 322498 Color: 3
Size: 264135 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 382939 Color: 3
Size: 366766 Color: 2
Size: 250296 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 436045 Color: 2
Size: 312250 Color: 3
Size: 251706 Color: 2

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 365799 Color: 1
Size: 360919 Color: 1
Size: 273283 Color: 4

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 459038 Color: 4
Size: 283006 Color: 1
Size: 257957 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 429723 Color: 0
Size: 319608 Color: 3
Size: 250670 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 453437 Color: 0
Size: 289682 Color: 3
Size: 256882 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 384847 Color: 3
Size: 340829 Color: 4
Size: 274325 Color: 4

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 380993 Color: 4
Size: 362692 Color: 1
Size: 256316 Color: 4

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 421677 Color: 3
Size: 301665 Color: 4
Size: 276659 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 387890 Color: 2
Size: 337912 Color: 3
Size: 274199 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 409564 Color: 3
Size: 311290 Color: 4
Size: 279147 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 471922 Color: 4
Size: 271758 Color: 3
Size: 256321 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 448258 Color: 1
Size: 301495 Color: 1
Size: 250248 Color: 3

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 403742 Color: 1
Size: 302384 Color: 4
Size: 293875 Color: 3

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 434447 Color: 0
Size: 296673 Color: 4
Size: 268881 Color: 2

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 404641 Color: 1
Size: 309613 Color: 1
Size: 285747 Color: 4

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 465190 Color: 3
Size: 278421 Color: 2
Size: 256390 Color: 4

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 379510 Color: 0
Size: 341013 Color: 1
Size: 279478 Color: 3

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 428804 Color: 2
Size: 304168 Color: 3
Size: 267029 Color: 3

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 356240 Color: 2
Size: 338628 Color: 0
Size: 305133 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 355078 Color: 2
Size: 353888 Color: 1
Size: 291035 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 389548 Color: 1
Size: 338546 Color: 1
Size: 271907 Color: 4

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 362129 Color: 1
Size: 324996 Color: 0
Size: 312876 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 381166 Color: 1
Size: 364276 Color: 3
Size: 254559 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 499009 Color: 1
Size: 250889 Color: 1
Size: 250103 Color: 2

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 383661 Color: 2
Size: 341308 Color: 4
Size: 275032 Color: 3

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 438720 Color: 3
Size: 308477 Color: 2
Size: 252804 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 421289 Color: 1
Size: 299275 Color: 1
Size: 279437 Color: 4

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 384767 Color: 3
Size: 308527 Color: 4
Size: 306707 Color: 2

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 432393 Color: 1
Size: 316699 Color: 1
Size: 250909 Color: 4

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 393233 Color: 2
Size: 340649 Color: 0
Size: 266119 Color: 3

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 471936 Color: 4
Size: 270851 Color: 2
Size: 257214 Color: 2

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 409323 Color: 1
Size: 329276 Color: 0
Size: 261402 Color: 2

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 356337 Color: 0
Size: 353933 Color: 2
Size: 289731 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 408161 Color: 3
Size: 332474 Color: 2
Size: 259366 Color: 2

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 395120 Color: 2
Size: 353814 Color: 1
Size: 251067 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 368342 Color: 0
Size: 339952 Color: 2
Size: 291707 Color: 2

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 361901 Color: 0
Size: 333456 Color: 3
Size: 304644 Color: 3

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 406863 Color: 0
Size: 305529 Color: 4
Size: 287609 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 484868 Color: 0
Size: 260815 Color: 2
Size: 254318 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 414739 Color: 3
Size: 317362 Color: 2
Size: 267900 Color: 3

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 399631 Color: 3
Size: 312251 Color: 2
Size: 288119 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 378703 Color: 2
Size: 367542 Color: 4
Size: 253756 Color: 3

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 347345 Color: 0
Size: 326438 Color: 0
Size: 326218 Color: 4

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 478783 Color: 3
Size: 261046 Color: 0
Size: 260172 Color: 2

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 440818 Color: 1
Size: 293374 Color: 2
Size: 265809 Color: 2

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 350200 Color: 2
Size: 347326 Color: 3
Size: 302475 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 477700 Color: 3
Size: 266373 Color: 0
Size: 255928 Color: 2

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 389096 Color: 3
Size: 336533 Color: 2
Size: 274372 Color: 4

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 473544 Color: 2
Size: 268709 Color: 2
Size: 257748 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 429993 Color: 1
Size: 307413 Color: 3
Size: 262595 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 455506 Color: 1
Size: 284809 Color: 2
Size: 259686 Color: 2

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 492639 Color: 4
Size: 256797 Color: 1
Size: 250565 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 406097 Color: 0
Size: 324597 Color: 2
Size: 269307 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 365968 Color: 2
Size: 318760 Color: 2
Size: 315273 Color: 4

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 389834 Color: 3
Size: 317208 Color: 4
Size: 292959 Color: 3

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 369261 Color: 1
Size: 340217 Color: 3
Size: 290523 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 417870 Color: 3
Size: 323823 Color: 3
Size: 258308 Color: 2

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 361647 Color: 3
Size: 330403 Color: 4
Size: 307951 Color: 4

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 433444 Color: 4
Size: 285851 Color: 0
Size: 280706 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 457676 Color: 3
Size: 273752 Color: 2
Size: 268573 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 401816 Color: 4
Size: 301575 Color: 3
Size: 296610 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 420345 Color: 3
Size: 325678 Color: 2
Size: 253978 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 412531 Color: 1
Size: 335552 Color: 4
Size: 251918 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 426132 Color: 3
Size: 295142 Color: 1
Size: 278727 Color: 3

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 349667 Color: 1
Size: 334672 Color: 0
Size: 315662 Color: 2

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 433684 Color: 1
Size: 290649 Color: 0
Size: 275668 Color: 3

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 497696 Color: 3
Size: 251493 Color: 3
Size: 250812 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 364517 Color: 4
Size: 331913 Color: 0
Size: 303571 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 480060 Color: 0
Size: 262627 Color: 0
Size: 257314 Color: 1

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 400390 Color: 0
Size: 312008 Color: 3
Size: 287603 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 377792 Color: 0
Size: 331392 Color: 4
Size: 290817 Color: 1

Total size: 167000167
Total free space: 0

