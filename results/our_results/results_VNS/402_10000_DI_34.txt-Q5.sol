Capicity Bin: 7744
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4397 Color: 1
Size: 2791 Color: 0
Size: 556 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 1
Size: 3084 Color: 0
Size: 176 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 3
Size: 2604 Color: 0
Size: 152 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 4
Size: 2364 Color: 0
Size: 182 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 0
Size: 1926 Color: 3
Size: 472 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5434 Color: 0
Size: 1758 Color: 4
Size: 552 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5769 Color: 3
Size: 1335 Color: 4
Size: 640 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 1
Size: 1244 Color: 0
Size: 640 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6082 Color: 4
Size: 994 Color: 1
Size: 668 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 4
Size: 1476 Color: 3
Size: 160 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6159 Color: 3
Size: 1217 Color: 4
Size: 368 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6238 Color: 2
Size: 1242 Color: 4
Size: 264 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 2
Size: 1258 Color: 0
Size: 226 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6305 Color: 4
Size: 967 Color: 2
Size: 472 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 0
Size: 860 Color: 0
Size: 608 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6309 Color: 4
Size: 1197 Color: 2
Size: 238 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 0
Size: 1201 Color: 4
Size: 242 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 4
Size: 1056 Color: 3
Size: 374 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6333 Color: 4
Size: 1107 Color: 2
Size: 304 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 3
Size: 1285 Color: 4
Size: 146 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 0
Size: 1108 Color: 4
Size: 266 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 2
Size: 1014 Color: 1
Size: 328 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6442 Color: 4
Size: 1162 Color: 2
Size: 140 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6420 Color: 1
Size: 1154 Color: 1
Size: 170 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6532 Color: 4
Size: 748 Color: 3
Size: 464 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6507 Color: 0
Size: 1009 Color: 4
Size: 228 Color: 2

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6538 Color: 4
Size: 876 Color: 1
Size: 330 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 0
Size: 1009 Color: 1
Size: 200 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6554 Color: 1
Size: 1012 Color: 2
Size: 178 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6611 Color: 1
Size: 913 Color: 4
Size: 220 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 4
Size: 788 Color: 3
Size: 344 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6617 Color: 4
Size: 893 Color: 3
Size: 234 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6649 Color: 1
Size: 857 Color: 4
Size: 238 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6660 Color: 1
Size: 772 Color: 3
Size: 312 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6678 Color: 2
Size: 934 Color: 2
Size: 132 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 1
Size: 908 Color: 4
Size: 136 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 3
Size: 644 Color: 1
Size: 384 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6734 Color: 2
Size: 662 Color: 3
Size: 348 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6764 Color: 1
Size: 842 Color: 4
Size: 138 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6783 Color: 4
Size: 827 Color: 0
Size: 134 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 1
Size: 644 Color: 4
Size: 312 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 6804 Color: 4
Size: 644 Color: 0
Size: 296 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 1
Size: 516 Color: 4
Size: 408 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 6830 Color: 3
Size: 782 Color: 4
Size: 132 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6835 Color: 2
Size: 701 Color: 3
Size: 208 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 0
Size: 592 Color: 2
Size: 294 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 3
Size: 756 Color: 4
Size: 112 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 6903 Color: 3
Size: 677 Color: 4
Size: 164 Color: 2

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 1
Size: 762 Color: 4
Size: 32 Color: 3

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 3
Size: 653 Color: 0
Size: 130 Color: 4

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 5532 Color: 4
Size: 1881 Color: 1
Size: 330 Color: 0

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 2
Size: 1675 Color: 4
Size: 202 Color: 0

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 2
Size: 1657 Color: 1
Size: 184 Color: 1

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6143 Color: 4
Size: 1228 Color: 2
Size: 372 Color: 2

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6285 Color: 3
Size: 742 Color: 3
Size: 716 Color: 2

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 6357 Color: 4
Size: 1146 Color: 2
Size: 240 Color: 3

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6626 Color: 0
Size: 989 Color: 4
Size: 128 Color: 2

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 6745 Color: 2
Size: 686 Color: 2
Size: 312 Color: 4

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 3
Size: 900 Color: 1
Size: 34 Color: 4

Bin 60: 2 of cap free
Amount of items: 4
Items: 
Size: 3874 Color: 2
Size: 3220 Color: 4
Size: 416 Color: 0
Size: 232 Color: 4

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5638 Color: 3
Size: 1948 Color: 4
Size: 156 Color: 0

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 5973 Color: 3
Size: 1477 Color: 2
Size: 292 Color: 4

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6220 Color: 3
Size: 1386 Color: 4
Size: 136 Color: 1

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 6308 Color: 4
Size: 1194 Color: 2
Size: 240 Color: 1

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6852 Color: 3
Size: 890 Color: 1

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 4172 Color: 0
Size: 3225 Color: 2
Size: 344 Color: 2

Bin 67: 3 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 4
Size: 2429 Color: 0
Size: 396 Color: 3

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 5189 Color: 0
Size: 2440 Color: 4
Size: 112 Color: 3

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 2
Size: 1604 Color: 1
Size: 384 Color: 4

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 5977 Color: 1
Size: 1566 Color: 3
Size: 198 Color: 4

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 6824 Color: 2
Size: 917 Color: 1

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 4398 Color: 3
Size: 3142 Color: 4
Size: 200 Color: 0

Bin 73: 4 of cap free
Amount of items: 3
Items: 
Size: 5412 Color: 1
Size: 1844 Color: 4
Size: 484 Color: 0

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6106 Color: 3
Size: 1122 Color: 4
Size: 512 Color: 0

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5191 Color: 1
Size: 2300 Color: 0
Size: 248 Color: 1

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 5489 Color: 3
Size: 2122 Color: 0
Size: 128 Color: 4

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 5743 Color: 4
Size: 1572 Color: 4
Size: 424 Color: 3

Bin 78: 5 of cap free
Amount of items: 3
Items: 
Size: 5757 Color: 4
Size: 1558 Color: 2
Size: 424 Color: 2

Bin 79: 5 of cap free
Amount of items: 3
Items: 
Size: 6377 Color: 0
Size: 1306 Color: 2
Size: 56 Color: 0

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 6562 Color: 1
Size: 1177 Color: 3

Bin 81: 5 of cap free
Amount of items: 2
Items: 
Size: 6806 Color: 2
Size: 933 Color: 3

Bin 82: 5 of cap free
Amount of items: 2
Items: 
Size: 6933 Color: 4
Size: 806 Color: 3

Bin 83: 6 of cap free
Amount of items: 3
Items: 
Size: 4834 Color: 1
Size: 2708 Color: 4
Size: 196 Color: 2

Bin 84: 6 of cap free
Amount of items: 2
Items: 
Size: 6258 Color: 0
Size: 1480 Color: 3

Bin 85: 7 of cap free
Amount of items: 9
Items: 
Size: 1867 Color: 1
Size: 1157 Color: 2
Size: 1141 Color: 3
Size: 1058 Color: 0
Size: 658 Color: 1
Size: 536 Color: 4
Size: 488 Color: 2
Size: 416 Color: 2
Size: 416 Color: 0

Bin 86: 7 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 2
Size: 2778 Color: 1
Size: 128 Color: 0

Bin 87: 7 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 2
Size: 1463 Color: 3

Bin 88: 8 of cap free
Amount of items: 3
Items: 
Size: 4628 Color: 4
Size: 2980 Color: 4
Size: 128 Color: 0

Bin 89: 8 of cap free
Amount of items: 2
Items: 
Size: 6717 Color: 3
Size: 1019 Color: 1

Bin 90: 8 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 0
Size: 776 Color: 2
Size: 2 Color: 1

Bin 91: 9 of cap free
Amount of items: 3
Items: 
Size: 4588 Color: 4
Size: 1647 Color: 1
Size: 1500 Color: 4

Bin 92: 9 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 3
Size: 759 Color: 1
Size: 28 Color: 0

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 4
Size: 2426 Color: 1
Size: 88 Color: 3

Bin 94: 10 of cap free
Amount of items: 3
Items: 
Size: 5493 Color: 0
Size: 2129 Color: 4
Size: 112 Color: 3

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 6673 Color: 3
Size: 1060 Color: 1

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 1
Size: 1114 Color: 2

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6701 Color: 2
Size: 1031 Color: 1

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 6410 Color: 0
Size: 1321 Color: 3

Bin 99: 13 of cap free
Amount of items: 2
Items: 
Size: 6645 Color: 0
Size: 1086 Color: 2

Bin 100: 14 of cap free
Amount of items: 2
Items: 
Size: 6192 Color: 0
Size: 1538 Color: 2

Bin 101: 17 of cap free
Amount of items: 2
Items: 
Size: 4500 Color: 0
Size: 3227 Color: 3

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 6523 Color: 1
Size: 1204 Color: 0

Bin 103: 18 of cap free
Amount of items: 3
Items: 
Size: 6306 Color: 1
Size: 1364 Color: 0
Size: 56 Color: 1

Bin 104: 18 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 0
Size: 948 Color: 1

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 3884 Color: 2
Size: 3840 Color: 1

Bin 106: 20 of cap free
Amount of items: 3
Items: 
Size: 5505 Color: 2
Size: 2131 Color: 4
Size: 88 Color: 1

Bin 107: 21 of cap free
Amount of items: 2
Items: 
Size: 6922 Color: 3
Size: 801 Color: 4

Bin 108: 23 of cap free
Amount of items: 2
Items: 
Size: 6476 Color: 0
Size: 1245 Color: 1

Bin 109: 23 of cap free
Amount of items: 2
Items: 
Size: 6753 Color: 1
Size: 968 Color: 3

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 6354 Color: 1
Size: 1366 Color: 0

Bin 111: 25 of cap free
Amount of items: 2
Items: 
Size: 6058 Color: 2
Size: 1661 Color: 3

Bin 112: 26 of cap free
Amount of items: 2
Items: 
Size: 4994 Color: 1
Size: 2724 Color: 4

Bin 113: 26 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 2
Size: 1738 Color: 1

Bin 114: 29 of cap free
Amount of items: 2
Items: 
Size: 5284 Color: 4
Size: 2431 Color: 2

Bin 115: 30 of cap free
Amount of items: 2
Items: 
Size: 5662 Color: 1
Size: 2052 Color: 4

Bin 116: 31 of cap free
Amount of items: 2
Items: 
Size: 5989 Color: 1
Size: 1724 Color: 2

Bin 117: 32 of cap free
Amount of items: 2
Items: 
Size: 6892 Color: 0
Size: 820 Color: 3

Bin 118: 34 of cap free
Amount of items: 2
Items: 
Size: 4642 Color: 0
Size: 3068 Color: 2

Bin 119: 35 of cap free
Amount of items: 3
Items: 
Size: 5948 Color: 3
Size: 1669 Color: 2
Size: 92 Color: 4

Bin 120: 39 of cap free
Amount of items: 2
Items: 
Size: 5828 Color: 1
Size: 1877 Color: 0

Bin 121: 40 of cap free
Amount of items: 2
Items: 
Size: 4832 Color: 2
Size: 2872 Color: 0

Bin 122: 41 of cap free
Amount of items: 4
Items: 
Size: 3873 Color: 4
Size: 2294 Color: 0
Size: 1168 Color: 4
Size: 368 Color: 3

Bin 123: 41 of cap free
Amount of items: 3
Items: 
Size: 4393 Color: 0
Size: 2108 Color: 1
Size: 1202 Color: 3

Bin 124: 42 of cap free
Amount of items: 3
Items: 
Size: 3882 Color: 4
Size: 3396 Color: 3
Size: 424 Color: 0

Bin 125: 43 of cap free
Amount of items: 3
Items: 
Size: 4827 Color: 3
Size: 2586 Color: 4
Size: 288 Color: 3

Bin 126: 51 of cap free
Amount of items: 2
Items: 
Size: 6417 Color: 4
Size: 1276 Color: 3

Bin 127: 54 of cap free
Amount of items: 28
Items: 
Size: 536 Color: 1
Size: 486 Color: 1
Size: 484 Color: 4
Size: 456 Color: 3
Size: 456 Color: 2
Size: 374 Color: 0
Size: 332 Color: 4
Size: 276 Color: 3
Size: 272 Color: 3
Size: 272 Color: 0
Size: 248 Color: 2
Size: 244 Color: 0
Size: 236 Color: 2
Size: 236 Color: 1
Size: 230 Color: 4
Size: 228 Color: 0
Size: 220 Color: 2
Size: 220 Color: 0
Size: 216 Color: 4
Size: 216 Color: 2
Size: 206 Color: 0
Size: 204 Color: 2
Size: 184 Color: 1
Size: 182 Color: 1
Size: 176 Color: 0
Size: 168 Color: 4
Size: 168 Color: 0
Size: 164 Color: 4

Bin 128: 66 of cap free
Amount of items: 2
Items: 
Size: 5676 Color: 2
Size: 2002 Color: 0

Bin 129: 74 of cap free
Amount of items: 3
Items: 
Size: 3876 Color: 4
Size: 2793 Color: 0
Size: 1001 Color: 1

Bin 130: 89 of cap free
Amount of items: 3
Items: 
Size: 3875 Color: 4
Size: 3222 Color: 1
Size: 558 Color: 3

Bin 131: 104 of cap free
Amount of items: 2
Items: 
Size: 4414 Color: 1
Size: 3226 Color: 4

Bin 132: 108 of cap free
Amount of items: 2
Items: 
Size: 4244 Color: 3
Size: 3392 Color: 2

Bin 133: 6222 of cap free
Amount of items: 10
Items: 
Size: 168 Color: 0
Size: 160 Color: 2
Size: 160 Color: 2
Size: 152 Color: 3
Size: 152 Color: 0
Size: 150 Color: 1
Size: 148 Color: 3
Size: 144 Color: 2
Size: 144 Color: 1
Size: 144 Color: 1

Total size: 1022208
Total free space: 7744

