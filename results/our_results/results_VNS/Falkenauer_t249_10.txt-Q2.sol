Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 309 Color: 0
Size: 302 Color: 0
Size: 389 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 279 Color: 0
Size: 269 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 349 Color: 0
Size: 289 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 0
Size: 258 Color: 1
Size: 254 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 0
Size: 276 Color: 0
Size: 253 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 305 Color: 0
Size: 290 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 0
Size: 294 Color: 1
Size: 291 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 357 Color: 0
Size: 260 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 0
Size: 319 Color: 1
Size: 296 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 0
Size: 313 Color: 1
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 326 Color: 0
Size: 279 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 0
Size: 321 Color: 0
Size: 261 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 0
Size: 366 Color: 1
Size: 263 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 0
Size: 303 Color: 0
Size: 288 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 299 Color: 1
Size: 267 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 0
Size: 294 Color: 1
Size: 261 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 280 Color: 0
Size: 261 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 257 Color: 1
Size: 257 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 0
Size: 287 Color: 0
Size: 265 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 265 Color: 0
Size: 260 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 324 Color: 0
Size: 266 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 269 Color: 1
Size: 254 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 363 Color: 1
Size: 263 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 0
Size: 277 Color: 1
Size: 251 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 0
Size: 266 Color: 1
Size: 261 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 0
Size: 264 Color: 0
Size: 259 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 293 Color: 1
Size: 271 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 281 Color: 0
Size: 269 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 0
Size: 307 Color: 0
Size: 296 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 302 Color: 1
Size: 255 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 0
Size: 350 Color: 1
Size: 261 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 0
Size: 254 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 360 Color: 0
Size: 267 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 280 Color: 0
Size: 263 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 0
Size: 352 Color: 1
Size: 250 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 339 Color: 0
Size: 263 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 366 Color: 1
Size: 260 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 323 Color: 1
Size: 284 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 348 Color: 1
Size: 287 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 275 Color: 1
Size: 266 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 359 Color: 0
Size: 279 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 329 Color: 0
Size: 324 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 0
Size: 315 Color: 1
Size: 292 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 290 Color: 0
Size: 252 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 0
Size: 277 Color: 1
Size: 274 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 0
Size: 300 Color: 0
Size: 260 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 260 Color: 0
Size: 255 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 262 Color: 0
Size: 251 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 0
Size: 256 Color: 1
Size: 250 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 275 Color: 1
Size: 264 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 279 Color: 0
Size: 250 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 278 Color: 1
Size: 267 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 0
Size: 261 Color: 0
Size: 250 Color: 1

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 275 Color: 0
Size: 260 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 358 Color: 1
Size: 265 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 352 Color: 0
Size: 258 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 0
Size: 251 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 0
Size: 352 Color: 1
Size: 274 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 0
Size: 292 Color: 0
Size: 272 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 0
Size: 294 Color: 1
Size: 284 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 0
Size: 251 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 0
Size: 304 Color: 0
Size: 300 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 313 Color: 0
Size: 310 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 284 Color: 0
Size: 255 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 0
Size: 345 Color: 1
Size: 301 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 318 Color: 0
Size: 274 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 312 Color: 0
Size: 264 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 0
Size: 268 Color: 1
Size: 251 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 264 Color: 1
Size: 253 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 330 Color: 1
Size: 287 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 0
Size: 305 Color: 0
Size: 296 Color: 1

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 265 Color: 1
Size: 260 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 305 Color: 0
Size: 295 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 0
Size: 308 Color: 1
Size: 251 Color: 1

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 265 Color: 1
Size: 254 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 0
Size: 326 Color: 1
Size: 272 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 1
Size: 250 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 270 Color: 0
Size: 250 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 0
Size: 303 Color: 1
Size: 281 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 0
Size: 269 Color: 1
Size: 257 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 271 Color: 0
Size: 252 Color: 0

Total size: 83000
Total free space: 0

