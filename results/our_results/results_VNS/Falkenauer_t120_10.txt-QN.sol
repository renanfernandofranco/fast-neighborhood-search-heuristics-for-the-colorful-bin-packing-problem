Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 120

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 345 Color: 73
Size: 339 Color: 70
Size: 316 Color: 64

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 88
Size: 320 Color: 66
Size: 297 Color: 50

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 94
Size: 281 Color: 38
Size: 312 Color: 62

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 115
Size: 258 Color: 17
Size: 254 Color: 8

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 89
Size: 342 Color: 72
Size: 273 Color: 33

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 99
Size: 300 Color: 54
Size: 279 Color: 37

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 111
Size: 266 Color: 24
Size: 263 Color: 20

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 80
Size: 339 Color: 71
Size: 306 Color: 60

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 87
Size: 352 Color: 77
Size: 270 Color: 30

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 114
Size: 264 Color: 21
Size: 257 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 98
Size: 329 Color: 68
Size: 250 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 95
Size: 306 Color: 59
Size: 287 Color: 45

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 83
Size: 346 Color: 75
Size: 292 Color: 47

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 90
Size: 346 Color: 74
Size: 269 Color: 27

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 107
Size: 303 Color: 56
Size: 255 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 105
Size: 297 Color: 51
Size: 265 Color: 22

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 108
Size: 276 Color: 35
Size: 272 Color: 31

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 84
Size: 355 Color: 78
Size: 277 Color: 36

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 109
Size: 284 Color: 41
Size: 257 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 104
Size: 282 Color: 40
Size: 282 Color: 39

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 103
Size: 308 Color: 61
Size: 265 Color: 23

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 106
Size: 285 Color: 42
Size: 274 Color: 34

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 93
Size: 301 Color: 55
Size: 297 Color: 49

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 96
Size: 298 Color: 52
Size: 294 Color: 48

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 117
Size: 257 Color: 14
Size: 251 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 112
Size: 269 Color: 28
Size: 257 Color: 12

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 85
Size: 356 Color: 81
Size: 268 Color: 26

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 116
Size: 258 Color: 16
Size: 251 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 119
Size: 254 Color: 7
Size: 251 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 86
Size: 361 Color: 82
Size: 262 Color: 19

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 91
Size: 352 Color: 76
Size: 258 Color: 18

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 101
Size: 324 Color: 67
Size: 251 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 92
Size: 305 Color: 58
Size: 305 Color: 57

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 110
Size: 272 Color: 32
Size: 266 Color: 25

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 79
Size: 330 Color: 69
Size: 315 Color: 63

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 102
Size: 319 Color: 65
Size: 255 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 118
Size: 255 Color: 11
Size: 250 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 113
Size: 269 Color: 29
Size: 253 Color: 6

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 100
Size: 292 Color: 46
Size: 287 Color: 44

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 97
Size: 298 Color: 53
Size: 287 Color: 43

Total size: 40000
Total free space: 0

