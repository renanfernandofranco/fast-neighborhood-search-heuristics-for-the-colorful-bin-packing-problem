Capicity Bin: 2456
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 4
Size: 738 Color: 3
Size: 144 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 3
Size: 611 Color: 4
Size: 144 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 3
Size: 609 Color: 2
Size: 124 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 1
Size: 585 Color: 3
Size: 144 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1732 Color: 3
Size: 692 Color: 4
Size: 32 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1820 Color: 3
Size: 436 Color: 2
Size: 200 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 3
Size: 508 Color: 0
Size: 122 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1852 Color: 3
Size: 564 Color: 0
Size: 40 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1906 Color: 2
Size: 522 Color: 3
Size: 28 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1910 Color: 1
Size: 284 Color: 4
Size: 262 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1964 Color: 4
Size: 412 Color: 1
Size: 80 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 0
Size: 370 Color: 4
Size: 112 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2058 Color: 4
Size: 276 Color: 3
Size: 122 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2036 Color: 2
Size: 228 Color: 4
Size: 192 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2062 Color: 2
Size: 220 Color: 0
Size: 174 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2114 Color: 0
Size: 248 Color: 4
Size: 94 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2116 Color: 0
Size: 172 Color: 4
Size: 168 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2146 Color: 3
Size: 262 Color: 4
Size: 48 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2158 Color: 1
Size: 272 Color: 0
Size: 26 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 2188 Color: 3
Size: 204 Color: 4
Size: 64 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 2192 Color: 4
Size: 200 Color: 2
Size: 64 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 2198 Color: 1
Size: 254 Color: 0
Size: 4 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 3
Size: 204 Color: 0
Size: 48 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 4
Size: 740 Color: 3
Size: 146 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1891 Color: 3
Size: 532 Color: 4
Size: 32 Color: 2

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1907 Color: 3
Size: 512 Color: 2
Size: 36 Color: 4

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1970 Color: 0
Size: 485 Color: 1

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1410 Color: 3
Size: 980 Color: 0
Size: 64 Color: 0

Bin 29: 2 of cap free
Amount of items: 4
Items: 
Size: 1484 Color: 1
Size: 862 Color: 0
Size: 64 Color: 3
Size: 44 Color: 2

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 2
Size: 526 Color: 3
Size: 370 Color: 1

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1660 Color: 2
Size: 750 Color: 4
Size: 44 Color: 4

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1823 Color: 0
Size: 631 Color: 4

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1940 Color: 1
Size: 458 Color: 4
Size: 56 Color: 1

Bin 34: 2 of cap free
Amount of items: 4
Items: 
Size: 2074 Color: 1
Size: 356 Color: 0
Size: 16 Color: 3
Size: 8 Color: 1

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2132 Color: 1
Size: 322 Color: 0

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 2174 Color: 0
Size: 280 Color: 3

Bin 37: 3 of cap free
Amount of items: 4
Items: 
Size: 1229 Color: 4
Size: 1020 Color: 2
Size: 104 Color: 4
Size: 100 Color: 3

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1421 Color: 3
Size: 976 Color: 4
Size: 56 Color: 0

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 2
Size: 840 Color: 1
Size: 204 Color: 3

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1581 Color: 3
Size: 812 Color: 0
Size: 60 Color: 0

Bin 41: 3 of cap free
Amount of items: 3
Items: 
Size: 1830 Color: 4
Size: 541 Color: 3
Size: 82 Color: 4

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1734 Color: 4
Size: 638 Color: 1
Size: 80 Color: 3

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 2014 Color: 1
Size: 438 Color: 2

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 2206 Color: 4
Size: 238 Color: 0
Size: 8 Color: 2

Bin 45: 5 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 2
Size: 1021 Color: 4
Size: 104 Color: 3
Size: 96 Color: 4

Bin 46: 5 of cap free
Amount of items: 2
Items: 
Size: 1849 Color: 0
Size: 602 Color: 2

Bin 47: 6 of cap free
Amount of items: 14
Items: 
Size: 406 Color: 2
Size: 332 Color: 2
Size: 330 Color: 3
Size: 286 Color: 3
Size: 148 Color: 1
Size: 128 Color: 1
Size: 128 Color: 0
Size: 124 Color: 0
Size: 120 Color: 4
Size: 120 Color: 1
Size: 104 Color: 0
Size: 88 Color: 2
Size: 88 Color: 0
Size: 48 Color: 3

Bin 48: 6 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 4
Size: 668 Color: 3
Size: 72 Color: 2

Bin 49: 6 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 4
Size: 731 Color: 2

Bin 50: 7 of cap free
Amount of items: 2
Items: 
Size: 1426 Color: 2
Size: 1023 Color: 0

Bin 51: 7 of cap free
Amount of items: 3
Items: 
Size: 1536 Color: 4
Size: 873 Color: 0
Size: 40 Color: 3

Bin 52: 8 of cap free
Amount of items: 5
Items: 
Size: 1231 Color: 3
Size: 507 Color: 0
Size: 462 Color: 3
Size: 160 Color: 4
Size: 88 Color: 3

Bin 53: 8 of cap free
Amount of items: 2
Items: 
Size: 1833 Color: 4
Size: 615 Color: 0

Bin 54: 10 of cap free
Amount of items: 2
Items: 
Size: 1572 Color: 0
Size: 874 Color: 2

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 0
Size: 483 Color: 2
Size: 16 Color: 1

Bin 56: 12 of cap free
Amount of items: 2
Items: 
Size: 2060 Color: 4
Size: 384 Color: 2

Bin 57: 14 of cap free
Amount of items: 3
Items: 
Size: 1276 Color: 4
Size: 1018 Color: 3
Size: 148 Color: 2

Bin 58: 14 of cap free
Amount of items: 3
Items: 
Size: 1780 Color: 0
Size: 646 Color: 1
Size: 16 Color: 1

Bin 59: 19 of cap free
Amount of items: 3
Items: 
Size: 1236 Color: 4
Size: 730 Color: 4
Size: 471 Color: 1

Bin 60: 19 of cap free
Amount of items: 2
Items: 
Size: 1696 Color: 4
Size: 741 Color: 0

Bin 61: 20 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 3
Size: 988 Color: 1
Size: 210 Color: 0

Bin 62: 22 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 3
Size: 568 Color: 4
Size: 172 Color: 0

Bin 63: 25 of cap free
Amount of items: 2
Items: 
Size: 1682 Color: 3
Size: 749 Color: 4

Bin 64: 34 of cap free
Amount of items: 2
Items: 
Size: 1559 Color: 0
Size: 863 Color: 4

Bin 65: 38 of cap free
Amount of items: 2
Items: 
Size: 1396 Color: 1
Size: 1022 Color: 2

Bin 66: 2112 of cap free
Amount of items: 5
Items: 
Size: 88 Color: 4
Size: 80 Color: 2
Size: 80 Color: 0
Size: 48 Color: 1
Size: 48 Color: 1

Total size: 159640
Total free space: 2456

