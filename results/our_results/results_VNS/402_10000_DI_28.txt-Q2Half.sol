Capicity Bin: 8160
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 6937 Color: 1
Size: 839 Color: 1
Size: 384 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 6901 Color: 1
Size: 1051 Color: 1
Size: 208 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 1
Size: 762 Color: 1
Size: 152 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6529 Color: 1
Size: 1465 Color: 1
Size: 166 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 1
Size: 852 Color: 1
Size: 136 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5861 Color: 1
Size: 1917 Color: 1
Size: 382 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 1
Size: 892 Color: 1
Size: 176 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4521 Color: 1
Size: 3033 Color: 1
Size: 606 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4082 Color: 1
Size: 3926 Color: 1
Size: 152 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6844 Color: 1
Size: 1178 Color: 1
Size: 138 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6169 Color: 1
Size: 1661 Color: 1
Size: 330 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 1
Size: 932 Color: 1
Size: 184 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7250 Color: 1
Size: 734 Color: 1
Size: 176 Color: 0

Bin 14: 0 of cap free
Amount of items: 4
Items: 
Size: 6882 Color: 1
Size: 496 Color: 1
Size: 448 Color: 0
Size: 334 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 1
Size: 1036 Color: 1
Size: 136 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 7322 Color: 1
Size: 694 Color: 1
Size: 144 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6604 Color: 1
Size: 1436 Color: 1
Size: 120 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6681 Color: 1
Size: 1233 Color: 1
Size: 246 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 1
Size: 804 Color: 1
Size: 160 Color: 0

Bin 20: 0 of cap free
Amount of items: 5
Items: 
Size: 5041 Color: 1
Size: 1300 Color: 1
Size: 883 Color: 1
Size: 488 Color: 0
Size: 448 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4090 Color: 1
Size: 3402 Color: 1
Size: 668 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7132 Color: 1
Size: 860 Color: 1
Size: 168 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6262 Color: 1
Size: 1582 Color: 1
Size: 316 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 1
Size: 722 Color: 1
Size: 144 Color: 0

Bin 25: 0 of cap free
Amount of items: 5
Items: 
Size: 6246 Color: 1
Size: 1082 Color: 1
Size: 512 Color: 1
Size: 164 Color: 0
Size: 156 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4106 Color: 1
Size: 3918 Color: 1
Size: 136 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7303 Color: 1
Size: 715 Color: 1
Size: 142 Color: 0

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 4114 Color: 1
Size: 2828 Color: 1
Size: 664 Color: 0
Size: 554 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4146 Color: 1
Size: 3796 Color: 1
Size: 218 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6581 Color: 1
Size: 1537 Color: 1
Size: 42 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7102 Color: 1
Size: 738 Color: 1
Size: 320 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7222 Color: 1
Size: 782 Color: 1
Size: 156 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 1
Size: 3342 Color: 1
Size: 342 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 1
Size: 1420 Color: 1
Size: 280 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 1
Size: 882 Color: 1
Size: 528 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5901 Color: 1
Size: 1883 Color: 1
Size: 376 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5581 Color: 1
Size: 2151 Color: 1
Size: 428 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6444 Color: 1
Size: 1414 Color: 1
Size: 302 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 1
Size: 2782 Color: 1
Size: 158 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6560 Color: 1
Size: 1066 Color: 1
Size: 534 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 1
Size: 2284 Color: 1
Size: 270 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 4652 Color: 1
Size: 3362 Color: 1
Size: 146 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 7245 Color: 1
Size: 731 Color: 1
Size: 184 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 4098 Color: 1
Size: 3386 Color: 1
Size: 676 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 6854 Color: 1
Size: 822 Color: 1
Size: 484 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 6012 Color: 1
Size: 1882 Color: 1
Size: 266 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 6966 Color: 1
Size: 998 Color: 1
Size: 196 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 7183 Color: 1
Size: 761 Color: 1
Size: 216 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 6185 Color: 1
Size: 1647 Color: 1
Size: 328 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 7101 Color: 1
Size: 763 Color: 1
Size: 296 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 1
Size: 1298 Color: 1
Size: 256 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 7340 Color: 1
Size: 684 Color: 1
Size: 136 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 7130 Color: 1
Size: 764 Color: 1
Size: 266 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 4138 Color: 1
Size: 3346 Color: 1
Size: 676 Color: 0

Bin 55: 0 of cap free
Amount of items: 5
Items: 
Size: 3394 Color: 1
Size: 1937 Color: 1
Size: 1552 Color: 0
Size: 901 Color: 1
Size: 376 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 4130 Color: 1
Size: 3354 Color: 1
Size: 676 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 6145 Color: 1
Size: 1497 Color: 1
Size: 518 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 6365 Color: 1
Size: 1483 Color: 1
Size: 312 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5088 Color: 1
Size: 2894 Color: 1
Size: 178 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 1
Size: 978 Color: 1
Size: 192 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 6866 Color: 1
Size: 1062 Color: 1
Size: 232 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 1
Size: 1166 Color: 1
Size: 232 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5596 Color: 1
Size: 2140 Color: 1
Size: 424 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 1
Size: 700 Color: 1
Size: 136 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5626 Color: 1
Size: 2286 Color: 1
Size: 248 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 1
Size: 3076 Color: 1
Size: 280 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 4689 Color: 1
Size: 2893 Color: 1
Size: 578 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 7248 Color: 1
Size: 768 Color: 1
Size: 144 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 4180 Color: 1
Size: 3404 Color: 1
Size: 576 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5100 Color: 1
Size: 2804 Color: 1
Size: 256 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5779 Color: 1
Size: 1713 Color: 1
Size: 668 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 1
Size: 862 Color: 1
Size: 212 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 6018 Color: 1
Size: 1786 Color: 1
Size: 356 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 4321 Color: 1
Size: 3201 Color: 1
Size: 638 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 1
Size: 1100 Color: 1
Size: 312 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 6381 Color: 1
Size: 1339 Color: 1
Size: 440 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 7050 Color: 1
Size: 926 Color: 1
Size: 184 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5274 Color: 1
Size: 2406 Color: 1
Size: 480 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 1
Size: 957 Color: 1
Size: 190 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5746 Color: 1
Size: 2130 Color: 1
Size: 284 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 6620 Color: 1
Size: 1284 Color: 1
Size: 256 Color: 0

Bin 82: 0 of cap free
Amount of items: 5
Items: 
Size: 5245 Color: 1
Size: 1884 Color: 1
Size: 695 Color: 1
Size: 192 Color: 0
Size: 144 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 6034 Color: 1
Size: 1774 Color: 1
Size: 352 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 1
Size: 1286 Color: 1
Size: 424 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 1
Size: 1426 Color: 1
Size: 832 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5250 Color: 1
Size: 2426 Color: 1
Size: 484 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 7055 Color: 1
Size: 921 Color: 1
Size: 184 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 4332 Color: 1
Size: 3548 Color: 1
Size: 280 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 4830 Color: 1
Size: 2778 Color: 1
Size: 552 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 7081 Color: 1
Size: 879 Color: 1
Size: 200 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5740 Color: 1
Size: 2020 Color: 1
Size: 400 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 6228 Color: 1
Size: 1580 Color: 1
Size: 352 Color: 0

Bin 93: 1 of cap free
Amount of items: 3
Items: 
Size: 7327 Color: 1
Size: 680 Color: 1
Size: 152 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 4690 Color: 1
Size: 3401 Color: 1
Size: 68 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 7205 Color: 1
Size: 810 Color: 1
Size: 144 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 6466 Color: 1
Size: 1517 Color: 1
Size: 176 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 7155 Color: 1
Size: 980 Color: 1
Size: 24 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 7150 Color: 1
Size: 797 Color: 1
Size: 212 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 6753 Color: 1
Size: 1090 Color: 1
Size: 316 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 7249 Color: 1
Size: 822 Color: 1
Size: 88 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 6841 Color: 1
Size: 918 Color: 1
Size: 400 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 1
Size: 1333 Color: 1
Size: 208 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 6769 Color: 1
Size: 1240 Color: 1
Size: 150 Color: 0

Bin 104: 1 of cap free
Amount of items: 4
Items: 
Size: 4945 Color: 1
Size: 2452 Color: 1
Size: 386 Color: 0
Size: 376 Color: 0

Bin 105: 1 of cap free
Amount of items: 5
Items: 
Size: 3040 Color: 1
Size: 2777 Color: 1
Size: 1598 Color: 1
Size: 584 Color: 0
Size: 160 Color: 0

Bin 106: 2 of cap free
Amount of items: 3
Items: 
Size: 6129 Color: 1
Size: 1361 Color: 1
Size: 668 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 5511 Color: 1
Size: 2431 Color: 1
Size: 216 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 6505 Color: 1
Size: 1101 Color: 1
Size: 552 Color: 0

Bin 109: 3 of cap free
Amount of items: 3
Items: 
Size: 6105 Color: 1
Size: 1796 Color: 1
Size: 256 Color: 0

Bin 110: 3 of cap free
Amount of items: 3
Items: 
Size: 4085 Color: 1
Size: 4032 Color: 1
Size: 40 Color: 0

Bin 111: 4 of cap free
Amount of items: 5
Items: 
Size: 3568 Color: 1
Size: 2533 Color: 1
Size: 1161 Color: 1
Size: 556 Color: 0
Size: 338 Color: 0

Bin 112: 4 of cap free
Amount of items: 3
Items: 
Size: 5428 Color: 1
Size: 2560 Color: 1
Size: 168 Color: 0

Bin 113: 7 of cap free
Amount of items: 3
Items: 
Size: 6270 Color: 1
Size: 1681 Color: 1
Size: 202 Color: 0

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 4154 Color: 1
Size: 3809 Color: 1
Size: 190 Color: 0

Bin 115: 8 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 1
Size: 2924 Color: 1
Size: 1144 Color: 0

Bin 116: 9 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 1
Size: 1578 Color: 1
Size: 232 Color: 0

Bin 117: 9 of cap free
Amount of items: 5
Items: 
Size: 4829 Color: 1
Size: 1612 Color: 1
Size: 1180 Color: 1
Size: 298 Color: 0
Size: 232 Color: 0

Bin 118: 14 of cap free
Amount of items: 3
Items: 
Size: 5121 Color: 1
Size: 2601 Color: 1
Size: 424 Color: 0

Bin 119: 14 of cap free
Amount of items: 3
Items: 
Size: 5837 Color: 1
Size: 2209 Color: 1
Size: 100 Color: 0

Bin 120: 15 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 1
Size: 976 Color: 1
Size: 608 Color: 0

Bin 121: 16 of cap free
Amount of items: 3
Items: 
Size: 5900 Color: 1
Size: 2014 Color: 1
Size: 230 Color: 0

Bin 122: 16 of cap free
Amount of items: 3
Items: 
Size: 6890 Color: 1
Size: 858 Color: 1
Size: 396 Color: 0

Bin 123: 39 of cap free
Amount of items: 3
Items: 
Size: 6268 Color: 1
Size: 1693 Color: 1
Size: 160 Color: 0

Bin 124: 63 of cap free
Amount of items: 3
Items: 
Size: 6924 Color: 1
Size: 1021 Color: 1
Size: 152 Color: 0

Bin 125: 151 of cap free
Amount of items: 3
Items: 
Size: 4822 Color: 1
Size: 2681 Color: 1
Size: 506 Color: 0

Bin 126: 152 of cap free
Amount of items: 2
Items: 
Size: 7330 Color: 1
Size: 678 Color: 0

Bin 127: 877 of cap free
Amount of items: 1
Items: 
Size: 7283 Color: 1

Bin 128: 878 of cap free
Amount of items: 1
Items: 
Size: 7282 Color: 1

Bin 129: 908 of cap free
Amount of items: 1
Items: 
Size: 7252 Color: 1

Bin 130: 970 of cap free
Amount of items: 1
Items: 
Size: 7190 Color: 1

Bin 131: 986 of cap free
Amount of items: 1
Items: 
Size: 7174 Color: 1

Bin 132: 992 of cap free
Amount of items: 1
Items: 
Size: 7168 Color: 1

Bin 133: 1996 of cap free
Amount of items: 3
Items: 
Size: 4081 Color: 1
Size: 1985 Color: 1
Size: 98 Color: 0

Total size: 1077120
Total free space: 8160

