Capicity Bin: 1000001
Lower Bound: 48

Bins used: 50
Amount of Colors: 20

Bin 1: 9 of cap free
Amount of items: 3
Items: 
Size: 577155 Color: 19
Size: 256705 Color: 13
Size: 166132 Color: 17

Bin 2: 141 of cap free
Amount of items: 2
Items: 
Size: 621881 Color: 3
Size: 377979 Color: 0

Bin 3: 216 of cap free
Amount of items: 3
Items: 
Size: 620078 Color: 18
Size: 234350 Color: 2
Size: 145357 Color: 14

Bin 4: 227 of cap free
Amount of items: 3
Items: 
Size: 600837 Color: 15
Size: 247467 Color: 16
Size: 151470 Color: 0

Bin 5: 368 of cap free
Amount of items: 2
Items: 
Size: 738298 Color: 1
Size: 261335 Color: 15

Bin 6: 376 of cap free
Amount of items: 3
Items: 
Size: 640114 Color: 19
Size: 215884 Color: 4
Size: 143627 Color: 4

Bin 7: 408 of cap free
Amount of items: 2
Items: 
Size: 670844 Color: 5
Size: 328749 Color: 19

Bin 8: 414 of cap free
Amount of items: 3
Items: 
Size: 598363 Color: 18
Size: 269401 Color: 10
Size: 131823 Color: 17

Bin 9: 714 of cap free
Amount of items: 3
Items: 
Size: 597064 Color: 3
Size: 224418 Color: 14
Size: 177805 Color: 7

Bin 10: 762 of cap free
Amount of items: 3
Items: 
Size: 650146 Color: 13
Size: 186138 Color: 2
Size: 162955 Color: 2

Bin 11: 889 of cap free
Amount of items: 3
Items: 
Size: 642062 Color: 11
Size: 230635 Color: 18
Size: 126415 Color: 0

Bin 12: 1334 of cap free
Amount of items: 2
Items: 
Size: 568259 Color: 15
Size: 430408 Color: 14

Bin 13: 1448 of cap free
Amount of items: 2
Items: 
Size: 729691 Color: 18
Size: 268862 Color: 11

Bin 14: 2009 of cap free
Amount of items: 2
Items: 
Size: 774641 Color: 7
Size: 223351 Color: 19

Bin 15: 2343 of cap free
Amount of items: 2
Items: 
Size: 545435 Color: 5
Size: 452223 Color: 6

Bin 16: 2606 of cap free
Amount of items: 2
Items: 
Size: 582605 Color: 16
Size: 414790 Color: 5

Bin 17: 2738 of cap free
Amount of items: 2
Items: 
Size: 658072 Color: 14
Size: 339191 Color: 12

Bin 18: 3269 of cap free
Amount of items: 2
Items: 
Size: 661017 Color: 11
Size: 335715 Color: 15

Bin 19: 3484 of cap free
Amount of items: 2
Items: 
Size: 643187 Color: 17
Size: 353330 Color: 12

Bin 20: 3625 of cap free
Amount of items: 2
Items: 
Size: 567846 Color: 14
Size: 428530 Color: 2

Bin 21: 3649 of cap free
Amount of items: 2
Items: 
Size: 506631 Color: 17
Size: 489721 Color: 9

Bin 22: 3653 of cap free
Amount of items: 2
Items: 
Size: 585918 Color: 12
Size: 410430 Color: 1

Bin 23: 3712 of cap free
Amount of items: 2
Items: 
Size: 619651 Color: 16
Size: 376638 Color: 11

Bin 24: 4129 of cap free
Amount of items: 2
Items: 
Size: 577117 Color: 3
Size: 418755 Color: 7

Bin 25: 4336 of cap free
Amount of items: 2
Items: 
Size: 623489 Color: 12
Size: 372176 Color: 15

Bin 26: 4876 of cap free
Amount of items: 3
Items: 
Size: 478593 Color: 6
Size: 315201 Color: 7
Size: 201331 Color: 3

Bin 27: 4887 of cap free
Amount of items: 2
Items: 
Size: 711648 Color: 19
Size: 283466 Color: 4

Bin 28: 5131 of cap free
Amount of items: 2
Items: 
Size: 601197 Color: 4
Size: 393673 Color: 3

Bin 29: 5787 of cap free
Amount of items: 2
Items: 
Size: 646859 Color: 5
Size: 347355 Color: 9

Bin 30: 5976 of cap free
Amount of items: 2
Items: 
Size: 727007 Color: 18
Size: 267018 Color: 7

Bin 31: 6212 of cap free
Amount of items: 2
Items: 
Size: 717360 Color: 9
Size: 276429 Color: 1

Bin 32: 7142 of cap free
Amount of items: 3
Items: 
Size: 688164 Color: 8
Size: 200393 Color: 7
Size: 104302 Color: 8

Bin 33: 7572 of cap free
Amount of items: 2
Items: 
Size: 725583 Color: 19
Size: 266846 Color: 13

Bin 34: 7592 of cap free
Amount of items: 2
Items: 
Size: 556605 Color: 6
Size: 435804 Color: 5

Bin 35: 8020 of cap free
Amount of items: 2
Items: 
Size: 735202 Color: 3
Size: 256779 Color: 8

Bin 36: 8021 of cap free
Amount of items: 2
Items: 
Size: 513611 Color: 7
Size: 478369 Color: 6

Bin 37: 9068 of cap free
Amount of items: 2
Items: 
Size: 524447 Color: 7
Size: 466486 Color: 12

Bin 38: 11038 of cap free
Amount of items: 2
Items: 
Size: 541046 Color: 13
Size: 447917 Color: 1

Bin 39: 11126 of cap free
Amount of items: 2
Items: 
Size: 688050 Color: 16
Size: 300825 Color: 0

Bin 40: 15457 of cap free
Amount of items: 2
Items: 
Size: 617226 Color: 10
Size: 367318 Color: 15

Bin 41: 26526 of cap free
Amount of items: 2
Items: 
Size: 486810 Color: 12
Size: 486665 Color: 8

Bin 42: 32417 of cap free
Amount of items: 2
Items: 
Size: 539136 Color: 6
Size: 428448 Color: 7

Bin 43: 200247 of cap free
Amount of items: 1
Items: 
Size: 799754 Color: 15

Bin 44: 220380 of cap free
Amount of items: 1
Items: 
Size: 779621 Color: 19

Bin 45: 232458 of cap free
Amount of items: 1
Items: 
Size: 767543 Color: 3

Bin 46: 234203 of cap free
Amount of items: 1
Items: 
Size: 765798 Color: 13

Bin 47: 247697 of cap free
Amount of items: 1
Items: 
Size: 752304 Color: 2

Bin 48: 255450 of cap free
Amount of items: 1
Items: 
Size: 744551 Color: 6

Bin 49: 266352 of cap free
Amount of items: 1
Items: 
Size: 733649 Color: 1

Bin 50: 298050 of cap free
Amount of items: 1
Items: 
Size: 701951 Color: 16

Total size: 47821506
Total free space: 2178544

