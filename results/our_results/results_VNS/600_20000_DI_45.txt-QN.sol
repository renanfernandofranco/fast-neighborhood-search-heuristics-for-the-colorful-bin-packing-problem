Capicity Bin: 16224
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9974 Color: 433
Size: 5930 Color: 383
Size: 320 Color: 53

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10829 Color: 447
Size: 5107 Color: 366
Size: 288 Color: 33

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12423 Color: 475
Size: 3627 Color: 340
Size: 174 Color: 4

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 488
Size: 1862 Color: 256
Size: 1486 Color: 221

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12904 Color: 489
Size: 2072 Color: 275
Size: 1248 Color: 191

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12960 Color: 491
Size: 2416 Color: 296
Size: 848 Color: 159

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 494
Size: 2580 Color: 304
Size: 640 Color: 138

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13106 Color: 498
Size: 2602 Color: 308
Size: 516 Color: 117

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13127 Color: 500
Size: 2581 Color: 305
Size: 516 Color: 118

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13154 Color: 502
Size: 1894 Color: 259
Size: 1176 Color: 186

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13170 Color: 503
Size: 2598 Color: 306
Size: 456 Color: 103

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13282 Color: 506
Size: 1540 Color: 226
Size: 1402 Color: 208

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13454 Color: 513
Size: 2578 Color: 303
Size: 192 Color: 8

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13544 Color: 517
Size: 2356 Color: 293
Size: 324 Color: 55

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 519
Size: 2248 Color: 288
Size: 416 Color: 90

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13571 Color: 520
Size: 2407 Color: 295
Size: 246 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13572 Color: 521
Size: 1548 Color: 227
Size: 1104 Color: 183

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 525
Size: 1608 Color: 231
Size: 948 Color: 173

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13742 Color: 528
Size: 2070 Color: 274
Size: 412 Color: 89

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13748 Color: 529
Size: 2068 Color: 273
Size: 408 Color: 86

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13752 Color: 530
Size: 1672 Color: 237
Size: 800 Color: 153

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13832 Color: 535
Size: 1248 Color: 192
Size: 1144 Color: 185

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13918 Color: 540
Size: 1280 Color: 193
Size: 1026 Color: 178

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13949 Color: 542
Size: 2075 Color: 276
Size: 200 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14018 Color: 549
Size: 1622 Color: 234
Size: 584 Color: 132

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 553
Size: 1478 Color: 219
Size: 632 Color: 137

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14122 Color: 554
Size: 1694 Color: 240
Size: 408 Color: 85

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14125 Color: 555
Size: 1559 Color: 228
Size: 540 Color: 123

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 556
Size: 1749 Color: 245
Size: 348 Color: 65

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 560
Size: 1522 Color: 224
Size: 508 Color: 114

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 561
Size: 1692 Color: 239
Size: 336 Color: 60

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14284 Color: 566
Size: 1528 Color: 225
Size: 412 Color: 88

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14306 Color: 568
Size: 1348 Color: 199
Size: 570 Color: 126

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14308 Color: 569
Size: 1072 Color: 181
Size: 844 Color: 158

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14311 Color: 570
Size: 1595 Color: 229
Size: 318 Color: 50

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 571
Size: 1620 Color: 233
Size: 292 Color: 35

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14334 Color: 573
Size: 1096 Color: 182
Size: 794 Color: 152

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14391 Color: 577
Size: 1713 Color: 243
Size: 120 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14398 Color: 579
Size: 1416 Color: 209
Size: 410 Color: 87

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 580
Size: 1515 Color: 223
Size: 302 Color: 38

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14423 Color: 581
Size: 1501 Color: 222
Size: 300 Color: 37

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14442 Color: 582
Size: 1038 Color: 179
Size: 744 Color: 148

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 583
Size: 1484 Color: 220
Size: 288 Color: 30

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 584
Size: 1458 Color: 216
Size: 312 Color: 49

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 586
Size: 1348 Color: 200
Size: 400 Color: 82

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 587
Size: 1442 Color: 214
Size: 304 Color: 42

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 588
Size: 1384 Color: 206
Size: 352 Color: 66

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 589
Size: 1422 Color: 211
Size: 304 Color: 39

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 591
Size: 1428 Color: 212
Size: 280 Color: 24

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14519 Color: 592
Size: 1421 Color: 210
Size: 284 Color: 27

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14536 Color: 594
Size: 1136 Color: 184
Size: 552 Color: 125

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14542 Color: 595
Size: 1348 Color: 201
Size: 334 Color: 57

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14563 Color: 596
Size: 1385 Color: 207
Size: 276 Color: 23

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14581 Color: 599
Size: 1371 Color: 204
Size: 272 Color: 20

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 600
Size: 1344 Color: 197
Size: 284 Color: 28

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12074 Color: 470
Size: 3965 Color: 345
Size: 184 Color: 6

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12746 Color: 485
Size: 1857 Color: 255
Size: 1620 Color: 232

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12973 Color: 492
Size: 2902 Color: 319
Size: 348 Color: 63

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13245 Color: 505
Size: 2118 Color: 278
Size: 860 Color: 161

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 13447 Color: 512
Size: 2776 Color: 314

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13623 Color: 523
Size: 2600 Color: 307

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 13753 Color: 531
Size: 2066 Color: 272
Size: 404 Color: 83

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13844 Color: 536
Size: 2379 Color: 294

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13895 Color: 538
Size: 1752 Color: 247
Size: 576 Color: 130

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14029 Color: 550
Size: 1922 Color: 261
Size: 272 Color: 22

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14162 Color: 558
Size: 2061 Color: 271

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 14215 Color: 562
Size: 2008 Color: 266

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14282 Color: 565
Size: 1941 Color: 263

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 14392 Color: 578
Size: 1831 Color: 251

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 14461 Color: 585
Size: 1762 Color: 248

Bin 71: 1 of cap free
Amount of items: 2
Items: 
Size: 14578 Color: 598
Size: 1645 Color: 235

Bin 72: 2 of cap free
Amount of items: 4
Items: 
Size: 8732 Color: 418
Size: 6754 Color: 396
Size: 368 Color: 71
Size: 368 Color: 70

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 12308 Color: 473
Size: 2890 Color: 318
Size: 1024 Color: 177

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 13404 Color: 510
Size: 2818 Color: 316

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 511
Size: 2796 Color: 315

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13556 Color: 518
Size: 2666 Color: 310

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13994 Color: 545
Size: 2228 Color: 285

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 576
Size: 1842 Color: 252

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14522 Color: 593
Size: 1700 Color: 241

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 597
Size: 1654 Color: 236

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 12429 Color: 476
Size: 3368 Color: 331
Size: 424 Color: 92

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 484
Size: 2853 Color: 317
Size: 624 Color: 136

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13112 Color: 499
Size: 3109 Color: 324

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 13395 Color: 509
Size: 1802 Color: 250
Size: 1024 Color: 176

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 13510 Color: 515
Size: 2711 Color: 313

Bin 86: 3 of cap free
Amount of items: 2
Items: 
Size: 13906 Color: 539
Size: 2315 Color: 291

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13997 Color: 546
Size: 2224 Color: 284

Bin 88: 3 of cap free
Amount of items: 2
Items: 
Size: 14355 Color: 574
Size: 1866 Color: 257

Bin 89: 3 of cap free
Amount of items: 2
Items: 
Size: 14499 Color: 590
Size: 1722 Color: 244

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 11467 Color: 461
Size: 4497 Color: 357
Size: 256 Color: 16

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 12758 Color: 486
Size: 3462 Color: 334

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13316 Color: 508
Size: 2904 Color: 320

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13764 Color: 533
Size: 2456 Color: 299

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 548
Size: 2212 Color: 283

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14232 Color: 563
Size: 1988 Color: 265

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14320 Color: 572
Size: 1900 Color: 260

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14372 Color: 575
Size: 1848 Color: 254

Bin 98: 5 of cap free
Amount of items: 2
Items: 
Size: 11985 Color: 466
Size: 4234 Color: 352

Bin 99: 5 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 559
Size: 2031 Color: 267

Bin 100: 6 of cap free
Amount of items: 9
Items: 
Size: 8114 Color: 406
Size: 1374 Color: 205
Size: 1364 Color: 203
Size: 1350 Color: 202
Size: 1344 Color: 198
Size: 1344 Color: 196
Size: 464 Color: 107
Size: 432 Color: 94
Size: 432 Color: 93

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 10868 Color: 449
Size: 5350 Color: 372

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 11146 Color: 454
Size: 4792 Color: 363
Size: 280 Color: 26

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 12978 Color: 493
Size: 3240 Color: 329

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 516
Size: 2684 Color: 311

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 524
Size: 2562 Color: 302

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13986 Color: 544
Size: 2232 Color: 286

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14066 Color: 551
Size: 2152 Color: 280

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11658 Color: 462
Size: 4307 Color: 354
Size: 252 Color: 15

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 527
Size: 2483 Color: 300

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 13789 Color: 534
Size: 2428 Color: 297

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 9636 Color: 429
Size: 6244 Color: 388
Size: 336 Color: 59

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 9990 Color: 435
Size: 5906 Color: 381
Size: 320 Color: 51

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13082 Color: 497
Size: 3134 Color: 327

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13132 Color: 501
Size: 3084 Color: 323

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14084 Color: 552
Size: 2132 Color: 279

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 567
Size: 1924 Color: 262

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 11993 Color: 467
Size: 4222 Color: 351

Bin 118: 9 of cap free
Amount of items: 2
Items: 
Size: 13953 Color: 543
Size: 2262 Color: 289

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 547
Size: 2211 Color: 282

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 14136 Color: 557
Size: 2078 Color: 277

Bin 121: 11 of cap free
Amount of items: 3
Items: 
Size: 10720 Color: 445
Size: 5197 Color: 370
Size: 296 Color: 36

Bin 122: 11 of cap free
Amount of items: 2
Items: 
Size: 13208 Color: 504
Size: 3005 Color: 322

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 14251 Color: 564
Size: 1962 Color: 264

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 457
Size: 4608 Color: 359
Size: 272 Color: 21

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 13288 Color: 507
Size: 2924 Color: 321

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 532
Size: 2454 Color: 298

Bin 127: 13 of cap free
Amount of items: 7
Items: 
Size: 8117 Color: 408
Size: 1788 Color: 249
Size: 1751 Color: 246
Size: 1712 Color: 242
Size: 1675 Color: 238
Size: 760 Color: 149
Size: 408 Color: 84

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 9154 Color: 422
Size: 7056 Color: 401

Bin 129: 15 of cap free
Amount of items: 5
Items: 
Size: 8157 Color: 415
Size: 6746 Color: 394
Size: 544 Color: 124
Size: 384 Color: 76
Size: 378 Color: 75

Bin 130: 16 of cap free
Amount of items: 2
Items: 
Size: 12716 Color: 483
Size: 3492 Color: 335

Bin 131: 17 of cap free
Amount of items: 9
Items: 
Size: 8113 Color: 405
Size: 1344 Color: 195
Size: 1344 Color: 194
Size: 1232 Color: 190
Size: 1184 Color: 189
Size: 1180 Color: 188
Size: 944 Color: 172
Size: 434 Color: 96
Size: 432 Color: 95

Bin 132: 18 of cap free
Amount of items: 3
Items: 
Size: 9119 Color: 420
Size: 6723 Color: 391
Size: 364 Color: 68

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 12648 Color: 482
Size: 3558 Color: 339

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 13686 Color: 526
Size: 2520 Color: 301

Bin 135: 19 of cap free
Amount of items: 2
Items: 
Size: 13579 Color: 522
Size: 2626 Color: 309

Bin 136: 20 of cap free
Amount of items: 2
Items: 
Size: 11272 Color: 456
Size: 4932 Color: 365

Bin 137: 20 of cap free
Amount of items: 3
Items: 
Size: 11928 Color: 465
Size: 4084 Color: 349
Size: 192 Color: 10

Bin 138: 20 of cap free
Amount of items: 2
Items: 
Size: 12466 Color: 478
Size: 3738 Color: 341

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 537
Size: 2334 Color: 292

Bin 140: 21 of cap free
Amount of items: 3
Items: 
Size: 9989 Color: 434
Size: 5894 Color: 380
Size: 320 Color: 52

Bin 141: 21 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 487
Size: 3402 Color: 333

Bin 142: 22 of cap free
Amount of items: 2
Items: 
Size: 13924 Color: 541
Size: 2278 Color: 290

Bin 143: 23 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 458
Size: 4561 Color: 358
Size: 272 Color: 19

Bin 144: 24 of cap free
Amount of items: 3
Items: 
Size: 9959 Color: 432
Size: 5921 Color: 382
Size: 320 Color: 54

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13494 Color: 514
Size: 2706 Color: 312

Bin 146: 25 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 448
Size: 3120 Color: 325
Size: 2242 Color: 287

Bin 147: 26 of cap free
Amount of items: 3
Items: 
Size: 12036 Color: 469
Size: 3976 Color: 347
Size: 186 Color: 7

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 12142 Color: 471
Size: 4056 Color: 348

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 12434 Color: 477
Size: 3764 Color: 342

Bin 150: 27 of cap free
Amount of items: 3
Items: 
Size: 9157 Color: 423
Size: 6684 Color: 390
Size: 356 Color: 67

Bin 151: 27 of cap free
Amount of items: 2
Items: 
Size: 10065 Color: 436
Size: 6132 Color: 385

Bin 152: 27 of cap free
Amount of items: 2
Items: 
Size: 12929 Color: 490
Size: 3268 Color: 330

Bin 153: 28 of cap free
Amount of items: 3
Items: 
Size: 11464 Color: 460
Size: 4468 Color: 356
Size: 264 Color: 17

Bin 154: 28 of cap free
Amount of items: 3
Items: 
Size: 12619 Color: 481
Size: 3533 Color: 338
Size: 44 Color: 0

Bin 155: 28 of cap free
Amount of items: 2
Items: 
Size: 13074 Color: 496
Size: 3122 Color: 326

Bin 156: 30 of cap free
Amount of items: 5
Items: 
Size: 8136 Color: 413
Size: 6742 Color: 392
Size: 536 Color: 121
Size: 392 Color: 80
Size: 388 Color: 79

Bin 157: 32 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 428
Size: 6328 Color: 389
Size: 336 Color: 61

Bin 158: 32 of cap free
Amount of items: 3
Items: 
Size: 10120 Color: 439
Size: 5768 Color: 378
Size: 304 Color: 45

Bin 159: 32 of cap free
Amount of items: 3
Items: 
Size: 10488 Color: 441
Size: 5400 Color: 373
Size: 304 Color: 43

Bin 160: 32 of cap free
Amount of items: 3
Items: 
Size: 11742 Color: 464
Size: 4242 Color: 353
Size: 208 Color: 12

Bin 161: 34 of cap free
Amount of items: 5
Items: 
Size: 8138 Color: 414
Size: 6744 Color: 393
Size: 540 Color: 122
Size: 384 Color: 78
Size: 384 Color: 77

Bin 162: 35 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 495
Size: 3163 Color: 328

Bin 163: 36 of cap free
Amount of items: 3
Items: 
Size: 11162 Color: 455
Size: 4746 Color: 362
Size: 280 Color: 25

Bin 164: 37 of cap free
Amount of items: 3
Items: 
Size: 12024 Color: 468
Size: 3971 Color: 346
Size: 192 Color: 9

Bin 165: 40 of cap free
Amount of items: 4
Items: 
Size: 10070 Color: 437
Size: 5492 Color: 374
Size: 312 Color: 48
Size: 310 Color: 47

Bin 166: 46 of cap free
Amount of items: 3
Items: 
Size: 10753 Color: 446
Size: 5133 Color: 369
Size: 292 Color: 34

Bin 167: 48 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 474
Size: 3832 Color: 344

Bin 168: 52 of cap free
Amount of items: 2
Items: 
Size: 9316 Color: 425
Size: 6856 Color: 400

Bin 169: 56 of cap free
Amount of items: 3
Items: 
Size: 10981 Color: 451
Size: 4899 Color: 364
Size: 288 Color: 32

Bin 170: 58 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 472
Size: 3806 Color: 343
Size: 176 Color: 5

Bin 171: 69 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 480
Size: 3527 Color: 337
Size: 96 Color: 1

Bin 172: 70 of cap free
Amount of items: 3
Items: 
Size: 11134 Color: 453
Size: 4734 Color: 361
Size: 286 Color: 29

Bin 173: 76 of cap free
Amount of items: 2
Items: 
Size: 8132 Color: 412
Size: 8016 Color: 404

Bin 174: 77 of cap free
Amount of items: 3
Items: 
Size: 11459 Color: 459
Size: 4424 Color: 355
Size: 264 Color: 18

Bin 175: 83 of cap free
Amount of items: 5
Items: 
Size: 8122 Color: 410
Size: 3400 Color: 332
Size: 2169 Color: 281
Size: 2058 Color: 270
Size: 392 Color: 81

Bin 176: 83 of cap free
Amount of items: 3
Items: 
Size: 10073 Color: 438
Size: 5764 Color: 377
Size: 304 Color: 46

Bin 177: 83 of cap free
Amount of items: 2
Items: 
Size: 10920 Color: 450
Size: 5221 Color: 371

Bin 178: 84 of cap free
Amount of items: 3
Items: 
Size: 10308 Color: 440
Size: 5528 Color: 375
Size: 304 Color: 44

Bin 179: 86 of cap free
Amount of items: 4
Items: 
Size: 8648 Color: 417
Size: 6748 Color: 395
Size: 372 Color: 73
Size: 370 Color: 72

Bin 180: 88 of cap free
Amount of items: 3
Items: 
Size: 9752 Color: 430
Size: 6048 Color: 384
Size: 336 Color: 58

Bin 181: 97 of cap free
Amount of items: 3
Items: 
Size: 9908 Color: 431
Size: 5891 Color: 379
Size: 328 Color: 56

Bin 182: 98 of cap free
Amount of items: 3
Items: 
Size: 12478 Color: 479
Size: 3512 Color: 336
Size: 136 Color: 3

Bin 183: 102 of cap free
Amount of items: 2
Items: 
Size: 10530 Color: 442
Size: 5592 Color: 376

Bin 184: 140 of cap free
Amount of items: 3
Items: 
Size: 8456 Color: 416
Size: 7252 Color: 402
Size: 376 Color: 74

Bin 185: 140 of cap free
Amount of items: 3
Items: 
Size: 11716 Color: 463
Size: 4136 Color: 350
Size: 232 Color: 13

Bin 186: 174 of cap free
Amount of items: 3
Items: 
Size: 10616 Color: 444
Size: 5130 Color: 368
Size: 304 Color: 40

Bin 187: 199 of cap free
Amount of items: 3
Items: 
Size: 11057 Color: 452
Size: 4680 Color: 360
Size: 288 Color: 31

Bin 188: 226 of cap free
Amount of items: 2
Items: 
Size: 9236 Color: 424
Size: 6762 Color: 399

Bin 189: 244 of cap free
Amount of items: 3
Items: 
Size: 9396 Color: 427
Size: 6244 Color: 387
Size: 340 Color: 62

Bin 190: 247 of cap free
Amount of items: 3
Items: 
Size: 10546 Color: 443
Size: 5127 Color: 367
Size: 304 Color: 41

Bin 191: 265 of cap free
Amount of items: 5
Items: 
Size: 8120 Color: 409
Size: 2052 Color: 269
Size: 2048 Color: 268
Size: 1893 Color: 258
Size: 1846 Color: 253

Bin 192: 266 of cap free
Amount of items: 7
Items: 
Size: 8116 Color: 407
Size: 1604 Color: 230
Size: 1471 Color: 218
Size: 1460 Color: 217
Size: 1448 Color: 215
Size: 1439 Color: 213
Size: 420 Color: 91

Bin 193: 324 of cap free
Amount of items: 3
Items: 
Size: 9320 Color: 426
Size: 6232 Color: 386
Size: 348 Color: 64

Bin 194: 325 of cap free
Amount of items: 2
Items: 
Size: 9138 Color: 421
Size: 6761 Color: 398

Bin 195: 340 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 419
Size: 6760 Color: 397
Size: 364 Color: 69

Bin 196: 371 of cap free
Amount of items: 2
Items: 
Size: 8130 Color: 411
Size: 7723 Color: 403

Bin 197: 374 of cap free
Amount of items: 19
Items: 
Size: 1176 Color: 187
Size: 1044 Color: 180
Size: 992 Color: 175
Size: 984 Color: 174
Size: 944 Color: 171
Size: 928 Color: 170
Size: 912 Color: 169
Size: 910 Color: 168
Size: 900 Color: 167
Size: 898 Color: 166
Size: 896 Color: 165
Size: 888 Color: 164
Size: 880 Color: 163
Size: 864 Color: 162
Size: 856 Color: 160
Size: 450 Color: 100
Size: 448 Color: 99
Size: 440 Color: 98
Size: 440 Color: 97

Bin 198: 432 of cap free
Amount of items: 25
Items: 
Size: 840 Color: 157
Size: 816 Color: 156
Size: 808 Color: 155
Size: 800 Color: 154
Size: 792 Color: 151
Size: 784 Color: 150
Size: 744 Color: 147
Size: 706 Color: 146
Size: 704 Color: 145
Size: 696 Color: 144
Size: 688 Color: 143
Size: 688 Color: 142
Size: 680 Color: 141
Size: 672 Color: 140
Size: 648 Color: 139
Size: 496 Color: 112
Size: 488 Color: 111
Size: 488 Color: 110
Size: 480 Color: 109
Size: 480 Color: 108
Size: 464 Color: 106
Size: 464 Color: 105
Size: 462 Color: 104
Size: 452 Color: 102
Size: 452 Color: 101

Bin 199: 9504 of cap free
Amount of items: 12
Items: 
Size: 624 Color: 135
Size: 608 Color: 134
Size: 600 Color: 133
Size: 584 Color: 131
Size: 576 Color: 129
Size: 576 Color: 128
Size: 576 Color: 127
Size: 532 Color: 120
Size: 524 Color: 119
Size: 512 Color: 116
Size: 512 Color: 115
Size: 496 Color: 113

Total size: 3212352
Total free space: 16224

