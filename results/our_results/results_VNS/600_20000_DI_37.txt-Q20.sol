Capicity Bin: 16000
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 9748 Color: 2
Size: 5952 Color: 5
Size: 300 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9922 Color: 11
Size: 5732 Color: 10
Size: 346 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10018 Color: 4
Size: 5686 Color: 9
Size: 296 Color: 13

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10504 Color: 16
Size: 4168 Color: 4
Size: 1328 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 10660 Color: 3
Size: 4920 Color: 14
Size: 420 Color: 6

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 10770 Color: 16
Size: 4946 Color: 19
Size: 284 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 18
Size: 4362 Color: 2
Size: 484 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11288 Color: 17
Size: 4364 Color: 6
Size: 348 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11332 Color: 11
Size: 4042 Color: 15
Size: 626 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11386 Color: 4
Size: 4308 Color: 3
Size: 306 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11720 Color: 8
Size: 3576 Color: 8
Size: 704 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12463 Color: 13
Size: 2949 Color: 8
Size: 588 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12674 Color: 5
Size: 2542 Color: 2
Size: 784 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 4
Size: 2968 Color: 10
Size: 288 Color: 16

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12908 Color: 18
Size: 2648 Color: 3
Size: 444 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 5
Size: 3000 Color: 1
Size: 80 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 14
Size: 2568 Color: 18
Size: 448 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 15
Size: 2456 Color: 17
Size: 370 Color: 4

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13254 Color: 6
Size: 2438 Color: 5
Size: 308 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13416 Color: 4
Size: 1848 Color: 14
Size: 736 Color: 16

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13460 Color: 6
Size: 2332 Color: 3
Size: 208 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13484 Color: 10
Size: 1924 Color: 2
Size: 592 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13494 Color: 18
Size: 1718 Color: 5
Size: 788 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13631 Color: 9
Size: 1617 Color: 11
Size: 752 Color: 13

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13632 Color: 14
Size: 1328 Color: 12
Size: 1040 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13716 Color: 1
Size: 1844 Color: 7
Size: 440 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13720 Color: 18
Size: 1412 Color: 12
Size: 868 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13818 Color: 8
Size: 1148 Color: 13
Size: 1034 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 5
Size: 1788 Color: 3
Size: 316 Color: 9

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13908 Color: 12
Size: 1444 Color: 8
Size: 648 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13974 Color: 10
Size: 1586 Color: 4
Size: 440 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 1
Size: 1312 Color: 6
Size: 636 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 0
Size: 1328 Color: 8
Size: 558 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14124 Color: 15
Size: 1172 Color: 3
Size: 704 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14189 Color: 0
Size: 1511 Color: 16
Size: 300 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14194 Color: 0
Size: 1160 Color: 18
Size: 646 Color: 8

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14268 Color: 3
Size: 1444 Color: 19
Size: 288 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14237 Color: 9
Size: 1449 Color: 13
Size: 314 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14278 Color: 3
Size: 1378 Color: 9
Size: 344 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14294 Color: 17
Size: 1438 Color: 16
Size: 268 Color: 7

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14296 Color: 13
Size: 1148 Color: 1
Size: 556 Color: 11

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14326 Color: 3
Size: 1398 Color: 8
Size: 276 Color: 14

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14356 Color: 4
Size: 1332 Color: 15
Size: 312 Color: 11

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14360 Color: 1
Size: 1144 Color: 1
Size: 496 Color: 0

Bin 45: 1 of cap free
Amount of items: 7
Items: 
Size: 8004 Color: 3
Size: 1810 Color: 17
Size: 1588 Color: 5
Size: 1517 Color: 5
Size: 1184 Color: 8
Size: 1012 Color: 12
Size: 884 Color: 16

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 10818 Color: 4
Size: 5181 Color: 16

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 11106 Color: 5
Size: 3356 Color: 3
Size: 1537 Color: 12

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 12070 Color: 17
Size: 3627 Color: 10
Size: 302 Color: 1

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12239 Color: 5
Size: 3240 Color: 2
Size: 520 Color: 14

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12966 Color: 6
Size: 2449 Color: 15
Size: 584 Color: 2

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 13204 Color: 9
Size: 1755 Color: 3
Size: 1040 Color: 14

Bin 52: 1 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 1
Size: 2535 Color: 4

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 13525 Color: 7
Size: 2090 Color: 1
Size: 384 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 13647 Color: 5
Size: 1992 Color: 3
Size: 360 Color: 17

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 13754 Color: 16
Size: 1917 Color: 3
Size: 328 Color: 6

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 13809 Color: 10
Size: 1574 Color: 0
Size: 616 Color: 15

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 13958 Color: 5
Size: 1577 Color: 16
Size: 464 Color: 1

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 13985 Color: 6
Size: 1668 Color: 0
Size: 346 Color: 3

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 14061 Color: 8
Size: 1562 Color: 12
Size: 376 Color: 7

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 14062 Color: 8
Size: 1681 Color: 3
Size: 256 Color: 2

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 14109 Color: 11
Size: 1422 Color: 0
Size: 468 Color: 5

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 14172 Color: 10
Size: 1827 Color: 1

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 14258 Color: 2
Size: 1741 Color: 1

Bin 64: 2 of cap free
Amount of items: 23
Items: 
Size: 888 Color: 6
Size: 864 Color: 16
Size: 864 Color: 11
Size: 860 Color: 13
Size: 860 Color: 0
Size: 812 Color: 11
Size: 804 Color: 12
Size: 782 Color: 16
Size: 768 Color: 7
Size: 760 Color: 4
Size: 752 Color: 6
Size: 712 Color: 8
Size: 688 Color: 16
Size: 680 Color: 5
Size: 664 Color: 19
Size: 656 Color: 12
Size: 640 Color: 1
Size: 576 Color: 9
Size: 512 Color: 1
Size: 504 Color: 14
Size: 488 Color: 14
Size: 448 Color: 8
Size: 416 Color: 2

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 10060 Color: 2
Size: 5762 Color: 12
Size: 176 Color: 1

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 10644 Color: 19
Size: 5066 Color: 13
Size: 288 Color: 17

Bin 67: 2 of cap free
Amount of items: 2
Items: 
Size: 10850 Color: 10
Size: 5148 Color: 11

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 11064 Color: 6
Size: 4934 Color: 19

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 1
Size: 4442 Color: 0
Size: 312 Color: 1

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11758 Color: 3
Size: 3928 Color: 16
Size: 312 Color: 6

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11858 Color: 18
Size: 3804 Color: 0
Size: 336 Color: 15

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 12612 Color: 9
Size: 2964 Color: 3
Size: 422 Color: 17

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 12840 Color: 10
Size: 3158 Color: 14

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12844 Color: 1
Size: 2786 Color: 12
Size: 368 Color: 11

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 13318 Color: 5
Size: 2120 Color: 17
Size: 560 Color: 9

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13478 Color: 17
Size: 2520 Color: 13

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 16
Size: 2168 Color: 12

Bin 78: 2 of cap free
Amount of items: 2
Items: 
Size: 14308 Color: 17
Size: 1690 Color: 5

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 14374 Color: 17
Size: 1176 Color: 3
Size: 448 Color: 12

Bin 80: 3 of cap free
Amount of items: 6
Items: 
Size: 8008 Color: 1
Size: 1975 Color: 3
Size: 1922 Color: 0
Size: 1822 Color: 6
Size: 1454 Color: 2
Size: 816 Color: 8

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 10639 Color: 14
Size: 4986 Color: 13
Size: 372 Color: 9

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 13305 Color: 7
Size: 1428 Color: 14
Size: 1264 Color: 2

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13895 Color: 14
Size: 2102 Color: 7

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14085 Color: 18
Size: 1912 Color: 4

Bin 85: 3 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 14
Size: 1908 Color: 5

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 10728 Color: 11
Size: 4996 Color: 19
Size: 272 Color: 0

Bin 87: 4 of cap free
Amount of items: 2
Items: 
Size: 10960 Color: 14
Size: 5036 Color: 1

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 14
Size: 2238 Color: 1
Size: 1506 Color: 15

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 12892 Color: 4
Size: 2480 Color: 0
Size: 624 Color: 9

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13624 Color: 4
Size: 2372 Color: 8

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 12
Size: 2302 Color: 6

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 14263 Color: 11
Size: 1733 Color: 16

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14276 Color: 11
Size: 1720 Color: 8

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 9090 Color: 10
Size: 6665 Color: 8
Size: 240 Color: 12

Bin 95: 5 of cap free
Amount of items: 5
Items: 
Size: 10417 Color: 6
Size: 4322 Color: 8
Size: 556 Color: 2
Size: 420 Color: 7
Size: 280 Color: 19

Bin 96: 5 of cap free
Amount of items: 3
Items: 
Size: 12302 Color: 18
Size: 3359 Color: 13
Size: 334 Color: 0

Bin 97: 5 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 3
Size: 2119 Color: 12
Size: 816 Color: 19

Bin 98: 5 of cap free
Amount of items: 3
Items: 
Size: 13459 Color: 15
Size: 1676 Color: 5
Size: 860 Color: 9

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 9122 Color: 16
Size: 6600 Color: 11
Size: 272 Color: 15

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 10674 Color: 0
Size: 5128 Color: 7
Size: 192 Color: 8

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 12456 Color: 3
Size: 3538 Color: 16

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 12526 Color: 13
Size: 3244 Color: 3
Size: 224 Color: 16

Bin 103: 6 of cap free
Amount of items: 2
Items: 
Size: 13414 Color: 3
Size: 2580 Color: 0

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 8
Size: 2100 Color: 16

Bin 105: 6 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 19
Size: 1748 Color: 10
Size: 46 Color: 19

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 6
Size: 1702 Color: 2

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 14376 Color: 5
Size: 1618 Color: 2

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 8933 Color: 4
Size: 6660 Color: 3
Size: 400 Color: 16

Bin 109: 7 of cap free
Amount of items: 2
Items: 
Size: 13921 Color: 14
Size: 2072 Color: 12

Bin 110: 7 of cap free
Amount of items: 2
Items: 
Size: 14181 Color: 9
Size: 1812 Color: 10

Bin 111: 8 of cap free
Amount of items: 2
Items: 
Size: 8088 Color: 4
Size: 7904 Color: 7

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 12056 Color: 15
Size: 3660 Color: 12
Size: 276 Color: 16

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 13462 Color: 2
Size: 2530 Color: 18

Bin 114: 8 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 11
Size: 1494 Color: 11
Size: 670 Color: 9

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 14157 Color: 1
Size: 1835 Color: 12

Bin 116: 9 of cap free
Amount of items: 2
Items: 
Size: 13701 Color: 9
Size: 2290 Color: 6

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 14041 Color: 17
Size: 1950 Color: 5

Bin 118: 10 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 0
Size: 2462 Color: 17

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 13662 Color: 19
Size: 2328 Color: 11

Bin 120: 10 of cap free
Amount of items: 2
Items: 
Size: 14334 Color: 5
Size: 1656 Color: 4

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 14004 Color: 18
Size: 1984 Color: 5

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 13351 Color: 19
Size: 2636 Color: 10

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 13777 Color: 9
Size: 2209 Color: 18

Bin 124: 15 of cap free
Amount of items: 3
Items: 
Size: 9124 Color: 0
Size: 5837 Color: 1
Size: 1024 Color: 10

Bin 125: 15 of cap free
Amount of items: 2
Items: 
Size: 14024 Color: 5
Size: 1961 Color: 13

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 10772 Color: 9
Size: 5212 Color: 17

Bin 127: 16 of cap free
Amount of items: 2
Items: 
Size: 11304 Color: 8
Size: 4680 Color: 19

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 11649 Color: 18
Size: 4335 Color: 9

Bin 129: 16 of cap free
Amount of items: 2
Items: 
Size: 13156 Color: 14
Size: 2828 Color: 1

Bin 130: 16 of cap free
Amount of items: 2
Items: 
Size: 13860 Color: 6
Size: 2124 Color: 17

Bin 131: 17 of cap free
Amount of items: 2
Items: 
Size: 12959 Color: 12
Size: 3024 Color: 2

Bin 132: 17 of cap free
Amount of items: 2
Items: 
Size: 13320 Color: 0
Size: 2663 Color: 1

Bin 133: 17 of cap free
Amount of items: 2
Items: 
Size: 14130 Color: 16
Size: 1853 Color: 11

Bin 134: 17 of cap free
Amount of items: 2
Items: 
Size: 14350 Color: 16
Size: 1633 Color: 9

Bin 135: 17 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 7
Size: 1593 Color: 15

Bin 136: 20 of cap free
Amount of items: 2
Items: 
Size: 9028 Color: 13
Size: 6952 Color: 10

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 11528 Color: 16
Size: 4452 Color: 17

Bin 138: 21 of cap free
Amount of items: 3
Items: 
Size: 10883 Color: 4
Size: 4778 Color: 6
Size: 318 Color: 0

Bin 139: 22 of cap free
Amount of items: 8
Items: 
Size: 8002 Color: 8
Size: 1358 Color: 4
Size: 1342 Color: 6
Size: 1332 Color: 13
Size: 1176 Color: 0
Size: 1156 Color: 0
Size: 984 Color: 14
Size: 628 Color: 10

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 11263 Color: 11
Size: 4715 Color: 14

Bin 141: 22 of cap free
Amount of items: 2
Items: 
Size: 12166 Color: 11
Size: 3812 Color: 14

Bin 142: 22 of cap free
Amount of items: 2
Items: 
Size: 14210 Color: 15
Size: 1768 Color: 4

Bin 143: 23 of cap free
Amount of items: 3
Items: 
Size: 12108 Color: 17
Size: 2357 Color: 3
Size: 1512 Color: 0

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 13078 Color: 18
Size: 2898 Color: 0

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 16
Size: 2176 Color: 15

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 13913 Color: 5
Size: 2063 Color: 13

Bin 147: 24 of cap free
Amount of items: 2
Items: 
Size: 14388 Color: 7
Size: 1588 Color: 10

Bin 148: 25 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 12
Size: 4959 Color: 15

Bin 149: 25 of cap free
Amount of items: 2
Items: 
Size: 13173 Color: 13
Size: 2802 Color: 8

Bin 150: 26 of cap free
Amount of items: 3
Items: 
Size: 10104 Color: 7
Size: 4306 Color: 16
Size: 1564 Color: 12

Bin 151: 26 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 15
Size: 2654 Color: 11
Size: 96 Color: 9

Bin 152: 27 of cap free
Amount of items: 2
Items: 
Size: 10082 Color: 6
Size: 5891 Color: 2

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 11444 Color: 5
Size: 4528 Color: 3

Bin 154: 28 of cap free
Amount of items: 2
Items: 
Size: 14098 Color: 19
Size: 1874 Color: 11

Bin 155: 30 of cap free
Amount of items: 2
Items: 
Size: 8946 Color: 10
Size: 7024 Color: 16

Bin 156: 34 of cap free
Amount of items: 3
Items: 
Size: 10834 Color: 7
Size: 4408 Color: 13
Size: 724 Color: 9

Bin 157: 34 of cap free
Amount of items: 2
Items: 
Size: 11884 Color: 12
Size: 4082 Color: 9

Bin 158: 34 of cap free
Amount of items: 2
Items: 
Size: 12120 Color: 17
Size: 3846 Color: 19

Bin 159: 34 of cap free
Amount of items: 2
Items: 
Size: 12954 Color: 13
Size: 3012 Color: 2

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 13238 Color: 17
Size: 2728 Color: 19

Bin 161: 35 of cap free
Amount of items: 2
Items: 
Size: 8012 Color: 11
Size: 7953 Color: 3

Bin 162: 36 of cap free
Amount of items: 3
Items: 
Size: 8942 Color: 9
Size: 6662 Color: 15
Size: 360 Color: 1

Bin 163: 36 of cap free
Amount of items: 3
Items: 
Size: 11428 Color: 14
Size: 4120 Color: 17
Size: 416 Color: 0

Bin 164: 38 of cap free
Amount of items: 4
Items: 
Size: 8856 Color: 15
Size: 5802 Color: 9
Size: 792 Color: 7
Size: 512 Color: 12

Bin 165: 38 of cap free
Amount of items: 2
Items: 
Size: 14204 Color: 15
Size: 1758 Color: 9

Bin 166: 39 of cap free
Amount of items: 3
Items: 
Size: 12784 Color: 3
Size: 2793 Color: 7
Size: 384 Color: 9

Bin 167: 42 of cap free
Amount of items: 2
Items: 
Size: 10076 Color: 0
Size: 5882 Color: 13

Bin 168: 46 of cap free
Amount of items: 2
Items: 
Size: 13596 Color: 14
Size: 2358 Color: 5

Bin 169: 46 of cap free
Amount of items: 2
Items: 
Size: 13796 Color: 2
Size: 2158 Color: 10

Bin 170: 47 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 12
Size: 3135 Color: 2

Bin 171: 48 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 17
Size: 3956 Color: 12

Bin 172: 50 of cap free
Amount of items: 2
Items: 
Size: 12214 Color: 19
Size: 3736 Color: 4

Bin 173: 51 of cap free
Amount of items: 2
Items: 
Size: 13063 Color: 10
Size: 2886 Color: 15

Bin 174: 52 of cap free
Amount of items: 5
Items: 
Size: 8010 Color: 7
Size: 3082 Color: 5
Size: 2118 Color: 7
Size: 1390 Color: 3
Size: 1348 Color: 1

Bin 175: 52 of cap free
Amount of items: 2
Items: 
Size: 10066 Color: 0
Size: 5882 Color: 19

Bin 176: 52 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 19
Size: 2004 Color: 4

Bin 177: 54 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 15
Size: 5746 Color: 17
Size: 928 Color: 10

Bin 178: 54 of cap free
Amount of items: 2
Items: 
Size: 12658 Color: 9
Size: 3288 Color: 12

Bin 179: 54 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 12
Size: 2628 Color: 4
Size: 96 Color: 14

Bin 180: 61 of cap free
Amount of items: 3
Items: 
Size: 9707 Color: 13
Size: 5608 Color: 7
Size: 624 Color: 3

Bin 181: 61 of cap free
Amount of items: 2
Items: 
Size: 13692 Color: 8
Size: 2247 Color: 4

Bin 182: 71 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 7
Size: 3949 Color: 16

Bin 183: 71 of cap free
Amount of items: 2
Items: 
Size: 12805 Color: 18
Size: 3124 Color: 0

Bin 184: 73 of cap free
Amount of items: 2
Items: 
Size: 12649 Color: 14
Size: 3278 Color: 9

Bin 185: 74 of cap free
Amount of items: 2
Items: 
Size: 13942 Color: 5
Size: 1984 Color: 16

Bin 186: 86 of cap free
Amount of items: 3
Items: 
Size: 9106 Color: 3
Size: 5812 Color: 9
Size: 996 Color: 7

Bin 187: 90 of cap free
Amount of items: 7
Items: 
Size: 8003 Color: 9
Size: 1471 Color: 3
Size: 1452 Color: 2
Size: 1432 Color: 7
Size: 1368 Color: 4
Size: 1368 Color: 0
Size: 816 Color: 12

Bin 188: 94 of cap free
Amount of items: 2
Items: 
Size: 12452 Color: 11
Size: 3454 Color: 9

Bin 189: 98 of cap free
Amount of items: 3
Items: 
Size: 9042 Color: 14
Size: 5948 Color: 0
Size: 912 Color: 16

Bin 190: 101 of cap free
Amount of items: 2
Items: 
Size: 11971 Color: 18
Size: 3928 Color: 1

Bin 191: 113 of cap free
Amount of items: 10
Items: 
Size: 8001 Color: 15
Size: 1136 Color: 18
Size: 1120 Color: 10
Size: 1024 Color: 1
Size: 988 Color: 12
Size: 976 Color: 6
Size: 896 Color: 3
Size: 768 Color: 9
Size: 506 Color: 2
Size: 472 Color: 11

Bin 192: 154 of cap free
Amount of items: 2
Items: 
Size: 9178 Color: 15
Size: 6668 Color: 8

Bin 193: 156 of cap free
Amount of items: 2
Items: 
Size: 12408 Color: 8
Size: 3436 Color: 14

Bin 194: 160 of cap free
Amount of items: 2
Items: 
Size: 12642 Color: 2
Size: 3198 Color: 14

Bin 195: 192 of cap free
Amount of items: 2
Items: 
Size: 9848 Color: 1
Size: 5960 Color: 9

Bin 196: 194 of cap free
Amount of items: 2
Items: 
Size: 9140 Color: 17
Size: 6666 Color: 2

Bin 197: 197 of cap free
Amount of items: 2
Items: 
Size: 9785 Color: 0
Size: 6018 Color: 12

Bin 198: 208 of cap free
Amount of items: 38
Items: 
Size: 652 Color: 18
Size: 576 Color: 0
Size: 532 Color: 16
Size: 528 Color: 9
Size: 528 Color: 1
Size: 512 Color: 16
Size: 512 Color: 15
Size: 504 Color: 17
Size: 470 Color: 17
Size: 460 Color: 10
Size: 456 Color: 19
Size: 456 Color: 8
Size: 428 Color: 14
Size: 416 Color: 17
Size: 416 Color: 11
Size: 416 Color: 1
Size: 412 Color: 10
Size: 400 Color: 11
Size: 394 Color: 7
Size: 392 Color: 12
Size: 388 Color: 2
Size: 384 Color: 14
Size: 384 Color: 12
Size: 382 Color: 8
Size: 372 Color: 16
Size: 364 Color: 10
Size: 360 Color: 3
Size: 360 Color: 2
Size: 352 Color: 10
Size: 352 Color: 4
Size: 350 Color: 18
Size: 344 Color: 5
Size: 340 Color: 9
Size: 340 Color: 8
Size: 336 Color: 9
Size: 320 Color: 5
Size: 320 Color: 2
Size: 284 Color: 7

Bin 199: 11700 of cap free
Amount of items: 15
Items: 
Size: 336 Color: 1
Size: 326 Color: 14
Size: 322 Color: 14
Size: 320 Color: 12
Size: 316 Color: 19
Size: 292 Color: 4
Size: 288 Color: 9
Size: 280 Color: 19
Size: 280 Color: 7
Size: 272 Color: 17
Size: 272 Color: 2
Size: 268 Color: 13
Size: 264 Color: 3
Size: 240 Color: 5
Size: 224 Color: 11

Total size: 3168000
Total free space: 16000

