Capicity Bin: 1000001
Lower Bound: 3334

Bins used: 3336
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 408973 Color: 1
Size: 323574 Color: 4
Size: 267454 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 471122 Color: 2
Size: 278336 Color: 4
Size: 250543 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 486283 Color: 1
Size: 261756 Color: 0
Size: 251962 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 465915 Color: 3
Size: 269321 Color: 2
Size: 264765 Color: 2

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 447309 Color: 4
Size: 299439 Color: 1
Size: 253253 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 461917 Color: 2
Size: 285520 Color: 4
Size: 252564 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 483502 Color: 4
Size: 258825 Color: 4
Size: 257674 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 421182 Color: 0
Size: 307488 Color: 4
Size: 271331 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 396912 Color: 1
Size: 333476 Color: 4
Size: 269613 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 491829 Color: 0
Size: 254587 Color: 4
Size: 253585 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 459472 Color: 3
Size: 289417 Color: 4
Size: 251112 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 357956 Color: 1
Size: 356530 Color: 3
Size: 285515 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 383955 Color: 2
Size: 345216 Color: 0
Size: 270830 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 368838 Color: 0
Size: 322516 Color: 4
Size: 308647 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 391521 Color: 3
Size: 341635 Color: 3
Size: 266845 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 368206 Color: 0
Size: 318173 Color: 1
Size: 313622 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 404641 Color: 4
Size: 314746 Color: 2
Size: 280614 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 343888 Color: 1
Size: 343852 Color: 1
Size: 312261 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 407858 Color: 3
Size: 329291 Color: 2
Size: 262852 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 487664 Color: 3
Size: 257863 Color: 0
Size: 254474 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 384325 Color: 2
Size: 360045 Color: 0
Size: 255631 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 419681 Color: 0
Size: 325542 Color: 0
Size: 254778 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 439518 Color: 1
Size: 299733 Color: 1
Size: 260750 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 486614 Color: 3
Size: 259928 Color: 2
Size: 253459 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 449450 Color: 2
Size: 297548 Color: 0
Size: 253003 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 404741 Color: 4
Size: 326398 Color: 2
Size: 268862 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 384482 Color: 2
Size: 308660 Color: 4
Size: 306859 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 406355 Color: 1
Size: 336741 Color: 3
Size: 256905 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 385104 Color: 0
Size: 320583 Color: 4
Size: 294314 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 371199 Color: 1
Size: 336137 Color: 4
Size: 292665 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 395940 Color: 3
Size: 302546 Color: 2
Size: 301515 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 484498 Color: 3
Size: 263664 Color: 1
Size: 251839 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 371554 Color: 2
Size: 325286 Color: 2
Size: 303161 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 440784 Color: 4
Size: 288159 Color: 0
Size: 271058 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 407191 Color: 0
Size: 319335 Color: 2
Size: 273475 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 394649 Color: 0
Size: 332953 Color: 1
Size: 272399 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 479975 Color: 2
Size: 269874 Color: 2
Size: 250152 Color: 3

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 381486 Color: 3
Size: 321728 Color: 0
Size: 296787 Color: 4

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 490243 Color: 1
Size: 258404 Color: 4
Size: 251354 Color: 3

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 397915 Color: 3
Size: 347096 Color: 3
Size: 254990 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 408513 Color: 4
Size: 314552 Color: 2
Size: 276936 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 469691 Color: 4
Size: 274112 Color: 2
Size: 256198 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 479913 Color: 4
Size: 260310 Color: 0
Size: 259778 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 388521 Color: 3
Size: 355433 Color: 2
Size: 256047 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 398034 Color: 0
Size: 314933 Color: 1
Size: 287034 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 417006 Color: 4
Size: 317211 Color: 3
Size: 265784 Color: 1

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 396575 Color: 1
Size: 335814 Color: 0
Size: 267612 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 359111 Color: 4
Size: 328115 Color: 1
Size: 312775 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 394948 Color: 3
Size: 313784 Color: 2
Size: 291269 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 464916 Color: 4
Size: 283711 Color: 0
Size: 251374 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 435929 Color: 4
Size: 298997 Color: 1
Size: 265075 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 482841 Color: 4
Size: 264130 Color: 0
Size: 253030 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 483981 Color: 4
Size: 259187 Color: 2
Size: 256833 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 446357 Color: 3
Size: 286737 Color: 4
Size: 266907 Color: 4

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 400957 Color: 0
Size: 305711 Color: 1
Size: 293333 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 458510 Color: 1
Size: 281304 Color: 4
Size: 260187 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 419091 Color: 3
Size: 296090 Color: 1
Size: 284820 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 477659 Color: 0
Size: 272286 Color: 1
Size: 250056 Color: 3

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 425872 Color: 2
Size: 306644 Color: 0
Size: 267485 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 469364 Color: 3
Size: 266256 Color: 0
Size: 264381 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 434473 Color: 0
Size: 312830 Color: 1
Size: 252698 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 370020 Color: 4
Size: 342574 Color: 2
Size: 287407 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 481757 Color: 3
Size: 262306 Color: 4
Size: 255938 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 417377 Color: 3
Size: 315535 Color: 2
Size: 267089 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 426377 Color: 0
Size: 301298 Color: 2
Size: 272326 Color: 3

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 422336 Color: 2
Size: 315168 Color: 0
Size: 262497 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 454539 Color: 4
Size: 278650 Color: 0
Size: 266812 Color: 4

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 382955 Color: 4
Size: 352920 Color: 1
Size: 264126 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 401734 Color: 1
Size: 320764 Color: 4
Size: 277503 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 399702 Color: 3
Size: 314450 Color: 0
Size: 285849 Color: 2

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 388318 Color: 3
Size: 353037 Color: 2
Size: 258646 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 380183 Color: 4
Size: 347033 Color: 4
Size: 272785 Color: 2

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 444834 Color: 1
Size: 299300 Color: 1
Size: 255867 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 378015 Color: 4
Size: 356505 Color: 0
Size: 265481 Color: 4

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 438192 Color: 1
Size: 288584 Color: 3
Size: 273225 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 451410 Color: 3
Size: 296004 Color: 4
Size: 252587 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 427713 Color: 0
Size: 300649 Color: 2
Size: 271639 Color: 3

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 497707 Color: 4
Size: 251731 Color: 1
Size: 250563 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 420656 Color: 1
Size: 290979 Color: 3
Size: 288366 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 421551 Color: 4
Size: 292996 Color: 3
Size: 285454 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 406457 Color: 2
Size: 313154 Color: 3
Size: 280390 Color: 1

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 485185 Color: 0
Size: 264635 Color: 2
Size: 250181 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 366871 Color: 4
Size: 333458 Color: 2
Size: 299672 Color: 2

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 452635 Color: 3
Size: 289053 Color: 4
Size: 258313 Color: 1

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 434567 Color: 1
Size: 309259 Color: 4
Size: 256175 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 398988 Color: 1
Size: 338479 Color: 2
Size: 262534 Color: 2

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 416601 Color: 0
Size: 292702 Color: 3
Size: 290698 Color: 4

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 494038 Color: 1
Size: 255735 Color: 3
Size: 250228 Color: 3

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 451557 Color: 2
Size: 287507 Color: 4
Size: 260937 Color: 1

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 492193 Color: 2
Size: 256337 Color: 1
Size: 251471 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 458184 Color: 1
Size: 278660 Color: 3
Size: 263157 Color: 2

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 392186 Color: 4
Size: 314855 Color: 0
Size: 292960 Color: 2

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 439430 Color: 1
Size: 290484 Color: 2
Size: 270087 Color: 3

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 344842 Color: 1
Size: 330333 Color: 3
Size: 324826 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 366815 Color: 1
Size: 350510 Color: 3
Size: 282676 Color: 4

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 430952 Color: 2
Size: 284787 Color: 3
Size: 284262 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 468178 Color: 3
Size: 275735 Color: 0
Size: 256088 Color: 4

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 484151 Color: 0
Size: 259576 Color: 1
Size: 256274 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 374731 Color: 1
Size: 343693 Color: 4
Size: 281577 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 438392 Color: 1
Size: 291625 Color: 0
Size: 269984 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 427830 Color: 4
Size: 308013 Color: 0
Size: 264158 Color: 3

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 422889 Color: 1
Size: 297321 Color: 0
Size: 279791 Color: 3

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 445529 Color: 0
Size: 299430 Color: 3
Size: 255042 Color: 2

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 376428 Color: 3
Size: 324777 Color: 4
Size: 298796 Color: 4

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 494397 Color: 1
Size: 255253 Color: 3
Size: 250351 Color: 2

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 398913 Color: 3
Size: 337262 Color: 2
Size: 263826 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 365482 Color: 4
Size: 359062 Color: 0
Size: 275457 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 405661 Color: 3
Size: 321790 Color: 1
Size: 272550 Color: 4

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 367925 Color: 0
Size: 329811 Color: 3
Size: 302265 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 353794 Color: 0
Size: 334929 Color: 1
Size: 311278 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 426284 Color: 0
Size: 306670 Color: 3
Size: 267047 Color: 2

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 437065 Color: 2
Size: 307084 Color: 1
Size: 255852 Color: 3

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 376902 Color: 1
Size: 323058 Color: 1
Size: 300041 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 394702 Color: 0
Size: 342861 Color: 2
Size: 262438 Color: 4

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 359992 Color: 0
Size: 347576 Color: 2
Size: 292433 Color: 4

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 468814 Color: 1
Size: 274613 Color: 4
Size: 256574 Color: 3

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 423680 Color: 0
Size: 300855 Color: 4
Size: 275466 Color: 1

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 430714 Color: 0
Size: 294261 Color: 1
Size: 275026 Color: 1

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 380303 Color: 4
Size: 362462 Color: 2
Size: 257236 Color: 2

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 410790 Color: 2
Size: 328755 Color: 0
Size: 260456 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 481946 Color: 4
Size: 267941 Color: 4
Size: 250114 Color: 1

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 419004 Color: 0
Size: 311465 Color: 0
Size: 269532 Color: 3

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 386926 Color: 2
Size: 349453 Color: 1
Size: 263622 Color: 3

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 384385 Color: 4
Size: 356807 Color: 1
Size: 258809 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 482051 Color: 4
Size: 263453 Color: 3
Size: 254497 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 418558 Color: 0
Size: 316104 Color: 4
Size: 265339 Color: 4

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 389065 Color: 3
Size: 315881 Color: 4
Size: 295055 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 491587 Color: 1
Size: 256100 Color: 3
Size: 252314 Color: 4

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 418237 Color: 2
Size: 311214 Color: 4
Size: 270550 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 445388 Color: 0
Size: 290818 Color: 3
Size: 263795 Color: 1

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 386730 Color: 1
Size: 315686 Color: 3
Size: 297585 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 443419 Color: 4
Size: 296777 Color: 1
Size: 259805 Color: 3

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 393805 Color: 2
Size: 350365 Color: 0
Size: 255831 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 368399 Color: 4
Size: 357455 Color: 1
Size: 274147 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 403981 Color: 1
Size: 308774 Color: 1
Size: 287246 Color: 2

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 430851 Color: 2
Size: 306080 Color: 0
Size: 263070 Color: 4

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 421536 Color: 1
Size: 302282 Color: 2
Size: 276183 Color: 2

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 435310 Color: 3
Size: 286291 Color: 2
Size: 278400 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 461280 Color: 2
Size: 284679 Color: 1
Size: 254042 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 472823 Color: 2
Size: 268693 Color: 2
Size: 258485 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 462855 Color: 4
Size: 275062 Color: 4
Size: 262084 Color: 3

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 478447 Color: 2
Size: 267694 Color: 1
Size: 253860 Color: 2

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 389489 Color: 0
Size: 332671 Color: 2
Size: 277841 Color: 1

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 375562 Color: 2
Size: 355604 Color: 0
Size: 268835 Color: 2

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 445936 Color: 2
Size: 300871 Color: 0
Size: 253194 Color: 1

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 376332 Color: 2
Size: 334690 Color: 4
Size: 288979 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 418808 Color: 3
Size: 300579 Color: 4
Size: 280614 Color: 3

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 490371 Color: 0
Size: 258407 Color: 3
Size: 251223 Color: 1

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 466056 Color: 3
Size: 277154 Color: 0
Size: 256791 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 427776 Color: 1
Size: 296431 Color: 2
Size: 275794 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 435348 Color: 3
Size: 293054 Color: 0
Size: 271599 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 377335 Color: 1
Size: 314467 Color: 2
Size: 308199 Color: 4

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 369937 Color: 1
Size: 347272 Color: 1
Size: 282792 Color: 4

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 398362 Color: 1
Size: 316542 Color: 2
Size: 285097 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 408254 Color: 2
Size: 326740 Color: 4
Size: 265007 Color: 3

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 474142 Color: 4
Size: 263117 Color: 1
Size: 262742 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 382838 Color: 0
Size: 348498 Color: 3
Size: 268665 Color: 2

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 392525 Color: 0
Size: 357051 Color: 3
Size: 250425 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 371241 Color: 2
Size: 323925 Color: 0
Size: 304835 Color: 3

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 376859 Color: 3
Size: 336132 Color: 1
Size: 287010 Color: 2

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 426892 Color: 4
Size: 314154 Color: 1
Size: 258955 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 413882 Color: 3
Size: 308817 Color: 2
Size: 277302 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 390203 Color: 4
Size: 342487 Color: 2
Size: 267311 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 408602 Color: 3
Size: 322910 Color: 4
Size: 268489 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 469079 Color: 4
Size: 269645 Color: 0
Size: 261277 Color: 3

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 497746 Color: 3
Size: 251233 Color: 2
Size: 251022 Color: 4

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 484070 Color: 0
Size: 264239 Color: 3
Size: 251692 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 391953 Color: 3
Size: 331447 Color: 3
Size: 276601 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 391519 Color: 3
Size: 352106 Color: 1
Size: 256376 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 444324 Color: 2
Size: 304244 Color: 1
Size: 251433 Color: 1

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 403867 Color: 3
Size: 330820 Color: 4
Size: 265314 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 361113 Color: 3
Size: 353979 Color: 4
Size: 284909 Color: 3

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 445307 Color: 3
Size: 288215 Color: 0
Size: 266479 Color: 4

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 385719 Color: 4
Size: 336111 Color: 1
Size: 278171 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 444619 Color: 0
Size: 287288 Color: 1
Size: 268094 Color: 3

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 394204 Color: 4
Size: 325082 Color: 0
Size: 280715 Color: 2

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 392207 Color: 1
Size: 343681 Color: 2
Size: 264113 Color: 3

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 374644 Color: 0
Size: 367700 Color: 4
Size: 257657 Color: 4

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 366528 Color: 0
Size: 322515 Color: 3
Size: 310958 Color: 4

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 437125 Color: 4
Size: 295970 Color: 0
Size: 266906 Color: 3

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 402249 Color: 0
Size: 337897 Color: 3
Size: 259855 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 497417 Color: 3
Size: 251781 Color: 3
Size: 250803 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 461089 Color: 2
Size: 273689 Color: 4
Size: 265223 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 439093 Color: 3
Size: 286236 Color: 4
Size: 274672 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 396855 Color: 1
Size: 343510 Color: 0
Size: 259636 Color: 4

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 395824 Color: 0
Size: 313323 Color: 3
Size: 290854 Color: 2

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 361933 Color: 4
Size: 333884 Color: 4
Size: 304184 Color: 3

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 398964 Color: 3
Size: 344191 Color: 3
Size: 256846 Color: 2

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 490725 Color: 0
Size: 255974 Color: 1
Size: 253302 Color: 3

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 366019 Color: 0
Size: 347982 Color: 2
Size: 286000 Color: 3

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 497767 Color: 3
Size: 252048 Color: 1
Size: 250186 Color: 4

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 395720 Color: 3
Size: 302658 Color: 0
Size: 301623 Color: 2

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 430370 Color: 4
Size: 297326 Color: 2
Size: 272305 Color: 2

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 416405 Color: 4
Size: 333392 Color: 0
Size: 250204 Color: 3

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 414201 Color: 3
Size: 297472 Color: 0
Size: 288328 Color: 4

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 431918 Color: 0
Size: 302760 Color: 3
Size: 265323 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 449914 Color: 2
Size: 285002 Color: 2
Size: 265085 Color: 3

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 416274 Color: 0
Size: 294811 Color: 3
Size: 288916 Color: 1

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 371399 Color: 4
Size: 363330 Color: 0
Size: 265272 Color: 2

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 340598 Color: 2
Size: 334288 Color: 3
Size: 325115 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 416392 Color: 0
Size: 307359 Color: 0
Size: 276250 Color: 3

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 361967 Color: 1
Size: 358057 Color: 0
Size: 279977 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 419374 Color: 4
Size: 299039 Color: 2
Size: 281588 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 369526 Color: 0
Size: 346658 Color: 1
Size: 283817 Color: 2

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 465505 Color: 3
Size: 276936 Color: 4
Size: 257560 Color: 4

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 487344 Color: 0
Size: 259827 Color: 3
Size: 252830 Color: 4

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 463522 Color: 1
Size: 276066 Color: 3
Size: 260413 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 435662 Color: 3
Size: 301085 Color: 4
Size: 263254 Color: 2

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 489368 Color: 3
Size: 259510 Color: 1
Size: 251123 Color: 2

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 441164 Color: 1
Size: 291612 Color: 3
Size: 267225 Color: 0

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 398984 Color: 4
Size: 330548 Color: 0
Size: 270469 Color: 2

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 369978 Color: 1
Size: 317154 Color: 0
Size: 312869 Color: 3

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 347262 Color: 2
Size: 336172 Color: 1
Size: 316567 Color: 2

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 374243 Color: 2
Size: 330824 Color: 0
Size: 294934 Color: 3

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 365614 Color: 1
Size: 330107 Color: 1
Size: 304280 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 474356 Color: 0
Size: 275474 Color: 3
Size: 250171 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 346741 Color: 2
Size: 334292 Color: 0
Size: 318968 Color: 2

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 477402 Color: 1
Size: 272027 Color: 3
Size: 250572 Color: 4

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 382693 Color: 2
Size: 345617 Color: 2
Size: 271691 Color: 1

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 387970 Color: 1
Size: 321231 Color: 1
Size: 290800 Color: 2

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 366037 Color: 4
Size: 364008 Color: 3
Size: 269956 Color: 2

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 393553 Color: 1
Size: 328375 Color: 0
Size: 278073 Color: 2

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 395981 Color: 4
Size: 352829 Color: 0
Size: 251191 Color: 2

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 479683 Color: 0
Size: 260182 Color: 1
Size: 260136 Color: 4

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 489799 Color: 0
Size: 255752 Color: 0
Size: 254450 Color: 2

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 385652 Color: 3
Size: 355056 Color: 2
Size: 259293 Color: 2

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 381621 Color: 3
Size: 327981 Color: 0
Size: 290399 Color: 3

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 485546 Color: 3
Size: 259976 Color: 2
Size: 254479 Color: 2

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 455956 Color: 1
Size: 282000 Color: 3
Size: 262045 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 438981 Color: 4
Size: 292486 Color: 1
Size: 268534 Color: 4

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 395475 Color: 0
Size: 339501 Color: 1
Size: 265025 Color: 3

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 390428 Color: 0
Size: 354060 Color: 4
Size: 255513 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 452464 Color: 3
Size: 285202 Color: 1
Size: 262335 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 378548 Color: 3
Size: 346251 Color: 1
Size: 275202 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 438451 Color: 2
Size: 281300 Color: 1
Size: 280250 Color: 3

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 401060 Color: 4
Size: 309450 Color: 3
Size: 289491 Color: 2

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 470646 Color: 1
Size: 265842 Color: 3
Size: 263513 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 483391 Color: 3
Size: 260459 Color: 4
Size: 256151 Color: 4

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 392019 Color: 2
Size: 355177 Color: 3
Size: 252805 Color: 4

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 496078 Color: 0
Size: 253482 Color: 0
Size: 250441 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 385180 Color: 2
Size: 325337 Color: 1
Size: 289484 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 422592 Color: 1
Size: 308425 Color: 0
Size: 268984 Color: 4

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 423657 Color: 1
Size: 313105 Color: 2
Size: 263239 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 417337 Color: 2
Size: 312019 Color: 0
Size: 270645 Color: 4

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 377254 Color: 3
Size: 358874 Color: 1
Size: 263873 Color: 4

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 465469 Color: 4
Size: 283516 Color: 2
Size: 251016 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 444937 Color: 0
Size: 292229 Color: 4
Size: 262835 Color: 4

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 474347 Color: 3
Size: 275334 Color: 4
Size: 250320 Color: 2

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 337990 Color: 3
Size: 335898 Color: 0
Size: 326113 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 375852 Color: 2
Size: 315721 Color: 4
Size: 308428 Color: 2

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 402212 Color: 1
Size: 323133 Color: 3
Size: 274656 Color: 2

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 435336 Color: 2
Size: 300108 Color: 1
Size: 264557 Color: 2

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 349966 Color: 1
Size: 348552 Color: 3
Size: 301483 Color: 2

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 427699 Color: 2
Size: 318034 Color: 3
Size: 254268 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 444418 Color: 2
Size: 280034 Color: 0
Size: 275549 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 427450 Color: 0
Size: 307830 Color: 3
Size: 264721 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 416092 Color: 4
Size: 302295 Color: 0
Size: 281614 Color: 2

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 439459 Color: 0
Size: 298448 Color: 1
Size: 262094 Color: 3

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 382820 Color: 0
Size: 315711 Color: 4
Size: 301470 Color: 2

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 376624 Color: 4
Size: 315236 Color: 1
Size: 308141 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 484270 Color: 3
Size: 262031 Color: 4
Size: 253700 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 414875 Color: 0
Size: 331417 Color: 3
Size: 253709 Color: 4

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 376441 Color: 1
Size: 316408 Color: 4
Size: 307152 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 351752 Color: 1
Size: 333749 Color: 1
Size: 314500 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 456626 Color: 3
Size: 273049 Color: 2
Size: 270326 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 423476 Color: 4
Size: 321232 Color: 2
Size: 255293 Color: 1

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 352522 Color: 4
Size: 347827 Color: 3
Size: 299652 Color: 3

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 392413 Color: 2
Size: 343730 Color: 1
Size: 263858 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 416376 Color: 4
Size: 300863 Color: 0
Size: 282762 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 427316 Color: 1
Size: 301964 Color: 3
Size: 270721 Color: 4

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 379426 Color: 1
Size: 321844 Color: 0
Size: 298731 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 485016 Color: 3
Size: 257842 Color: 0
Size: 257143 Color: 3

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 414060 Color: 3
Size: 327620 Color: 0
Size: 258321 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 440614 Color: 0
Size: 287850 Color: 1
Size: 271537 Color: 2

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 468190 Color: 4
Size: 281514 Color: 3
Size: 250297 Color: 2

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 357937 Color: 4
Size: 326088 Color: 4
Size: 315976 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 445392 Color: 3
Size: 283701 Color: 4
Size: 270908 Color: 2

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 384414 Color: 4
Size: 323054 Color: 0
Size: 292533 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 452959 Color: 1
Size: 282169 Color: 3
Size: 264873 Color: 2

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 381589 Color: 0
Size: 326767 Color: 4
Size: 291645 Color: 3

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 498344 Color: 0
Size: 251584 Color: 2
Size: 250073 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 434493 Color: 2
Size: 310271 Color: 1
Size: 255237 Color: 3

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 412090 Color: 4
Size: 326574 Color: 2
Size: 261337 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 458838 Color: 1
Size: 276915 Color: 1
Size: 264248 Color: 4

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 374253 Color: 0
Size: 352044 Color: 1
Size: 273704 Color: 1

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 417776 Color: 4
Size: 293112 Color: 2
Size: 289113 Color: 3

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 442401 Color: 2
Size: 298195 Color: 0
Size: 259405 Color: 3

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 477693 Color: 1
Size: 267887 Color: 0
Size: 254421 Color: 3

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 406723 Color: 4
Size: 309102 Color: 2
Size: 284176 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 382175 Color: 4
Size: 319842 Color: 4
Size: 297984 Color: 2

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 389755 Color: 3
Size: 320906 Color: 4
Size: 289340 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 401261 Color: 0
Size: 310074 Color: 1
Size: 288666 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 412289 Color: 1
Size: 311224 Color: 3
Size: 276488 Color: 2

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 357912 Color: 3
Size: 338563 Color: 1
Size: 303526 Color: 3

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 491282 Color: 4
Size: 254604 Color: 1
Size: 254115 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 414182 Color: 4
Size: 327877 Color: 0
Size: 257942 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 414278 Color: 4
Size: 297407 Color: 2
Size: 288316 Color: 3

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 456854 Color: 3
Size: 287689 Color: 4
Size: 255458 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 444626 Color: 3
Size: 281604 Color: 2
Size: 273771 Color: 4

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 376121 Color: 0
Size: 352675 Color: 4
Size: 271205 Color: 3

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 427907 Color: 4
Size: 308241 Color: 0
Size: 263853 Color: 3

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 397983 Color: 1
Size: 336129 Color: 3
Size: 265889 Color: 3

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 436952 Color: 3
Size: 291347 Color: 1
Size: 271702 Color: 2

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 462016 Color: 3
Size: 278079 Color: 0
Size: 259906 Color: 2

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 429042 Color: 4
Size: 320517 Color: 2
Size: 250442 Color: 3

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 371349 Color: 3
Size: 348610 Color: 4
Size: 280042 Color: 4

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 373585 Color: 3
Size: 361708 Color: 1
Size: 264708 Color: 2

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 369545 Color: 3
Size: 330229 Color: 4
Size: 300227 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 490453 Color: 0
Size: 259196 Color: 4
Size: 250352 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 414519 Color: 0
Size: 331321 Color: 1
Size: 254161 Color: 3

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 392129 Color: 0
Size: 342998 Color: 1
Size: 264874 Color: 3

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 459676 Color: 4
Size: 287509 Color: 1
Size: 252816 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 379305 Color: 4
Size: 338261 Color: 0
Size: 282435 Color: 2

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 483046 Color: 4
Size: 265626 Color: 3
Size: 251329 Color: 2

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 484358 Color: 1
Size: 265066 Color: 3
Size: 250577 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 364738 Color: 0
Size: 333377 Color: 3
Size: 301886 Color: 2

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 385571 Color: 4
Size: 307307 Color: 0
Size: 307123 Color: 3

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 395273 Color: 0
Size: 313337 Color: 1
Size: 291391 Color: 4

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 383892 Color: 2
Size: 327134 Color: 0
Size: 288975 Color: 4

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 440760 Color: 4
Size: 288427 Color: 1
Size: 270814 Color: 3

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 460318 Color: 4
Size: 285712 Color: 3
Size: 253971 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 456499 Color: 4
Size: 285880 Color: 0
Size: 257622 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 337066 Color: 3
Size: 336222 Color: 3
Size: 326713 Color: 2

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 347174 Color: 2
Size: 341112 Color: 4
Size: 311715 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 418142 Color: 3
Size: 303426 Color: 4
Size: 278433 Color: 2

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 489649 Color: 3
Size: 260196 Color: 0
Size: 250156 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 389018 Color: 3
Size: 321483 Color: 4
Size: 289500 Color: 2

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 450925 Color: 2
Size: 296818 Color: 0
Size: 252258 Color: 3

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 437886 Color: 3
Size: 285661 Color: 4
Size: 276454 Color: 1

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 378848 Color: 1
Size: 356569 Color: 2
Size: 264584 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 381851 Color: 2
Size: 351936 Color: 0
Size: 266214 Color: 3

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 443221 Color: 1
Size: 300925 Color: 3
Size: 255855 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 451507 Color: 1
Size: 285068 Color: 2
Size: 263426 Color: 3

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 375359 Color: 2
Size: 328374 Color: 4
Size: 296268 Color: 2

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 393713 Color: 2
Size: 316320 Color: 3
Size: 289968 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 375281 Color: 3
Size: 361594 Color: 2
Size: 263126 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 474814 Color: 4
Size: 273204 Color: 2
Size: 251983 Color: 3

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 468736 Color: 1
Size: 275347 Color: 4
Size: 255918 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 362760 Color: 2
Size: 331421 Color: 0
Size: 305820 Color: 3

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 424807 Color: 2
Size: 321576 Color: 4
Size: 253618 Color: 1

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 427682 Color: 1
Size: 288590 Color: 3
Size: 283729 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 403074 Color: 2
Size: 346482 Color: 3
Size: 250445 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 421925 Color: 0
Size: 322513 Color: 0
Size: 255563 Color: 4

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 367940 Color: 1
Size: 343726 Color: 4
Size: 288335 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 374849 Color: 0
Size: 341818 Color: 2
Size: 283334 Color: 3

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 377569 Color: 4
Size: 325278 Color: 3
Size: 297154 Color: 4

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 484536 Color: 2
Size: 262141 Color: 3
Size: 253324 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 455325 Color: 2
Size: 292522 Color: 4
Size: 252154 Color: 3

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 494803 Color: 0
Size: 255117 Color: 4
Size: 250081 Color: 4

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 396623 Color: 1
Size: 316077 Color: 0
Size: 287301 Color: 4

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 463140 Color: 4
Size: 281782 Color: 0
Size: 255079 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 411882 Color: 4
Size: 328822 Color: 2
Size: 259297 Color: 3

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 436798 Color: 3
Size: 312812 Color: 0
Size: 250391 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 415211 Color: 0
Size: 309846 Color: 0
Size: 274944 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 391097 Color: 2
Size: 346517 Color: 2
Size: 262387 Color: 4

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 401518 Color: 4
Size: 347020 Color: 3
Size: 251463 Color: 0

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 377098 Color: 4
Size: 370501 Color: 3
Size: 252402 Color: 3

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 405373 Color: 4
Size: 325164 Color: 2
Size: 269464 Color: 1

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 444445 Color: 2
Size: 284230 Color: 3
Size: 271326 Color: 4

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 434572 Color: 2
Size: 303092 Color: 3
Size: 262337 Color: 4

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 441549 Color: 3
Size: 282446 Color: 1
Size: 276006 Color: 2

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 419851 Color: 4
Size: 307912 Color: 0
Size: 272238 Color: 2

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 360663 Color: 4
Size: 350085 Color: 3
Size: 289253 Color: 2

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 396113 Color: 1
Size: 351632 Color: 0
Size: 252256 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 423781 Color: 4
Size: 326166 Color: 0
Size: 250054 Color: 3

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 370421 Color: 0
Size: 341730 Color: 0
Size: 287850 Color: 4

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 404942 Color: 0
Size: 297880 Color: 0
Size: 297179 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 408995 Color: 3
Size: 333565 Color: 1
Size: 257441 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 433659 Color: 3
Size: 290751 Color: 1
Size: 275591 Color: 4

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 398180 Color: 4
Size: 329186 Color: 0
Size: 272635 Color: 4

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 396552 Color: 4
Size: 320125 Color: 3
Size: 283324 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 432903 Color: 4
Size: 288182 Color: 3
Size: 278916 Color: 2

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 408796 Color: 3
Size: 309175 Color: 2
Size: 282030 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 418931 Color: 4
Size: 299949 Color: 1
Size: 281121 Color: 3

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 362903 Color: 1
Size: 340771 Color: 1
Size: 296327 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 476097 Color: 1
Size: 273601 Color: 2
Size: 250303 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 487913 Color: 0
Size: 256754 Color: 1
Size: 255334 Color: 3

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 419609 Color: 1
Size: 291516 Color: 3
Size: 288876 Color: 3

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 409528 Color: 4
Size: 295403 Color: 4
Size: 295070 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 370874 Color: 4
Size: 344042 Color: 3
Size: 285085 Color: 1

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 435804 Color: 4
Size: 298007 Color: 2
Size: 266190 Color: 4

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 381863 Color: 3
Size: 362499 Color: 4
Size: 255639 Color: 2

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 439815 Color: 4
Size: 286534 Color: 2
Size: 273652 Color: 3

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 395695 Color: 4
Size: 353469 Color: 1
Size: 250837 Color: 2

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 449952 Color: 0
Size: 297819 Color: 1
Size: 252230 Color: 1

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 382294 Color: 1
Size: 340052 Color: 4
Size: 277655 Color: 4

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 437563 Color: 1
Size: 296869 Color: 3
Size: 265569 Color: 2

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 365932 Color: 0
Size: 355081 Color: 1
Size: 278988 Color: 2

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 361244 Color: 1
Size: 349186 Color: 1
Size: 289571 Color: 3

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 376472 Color: 4
Size: 324280 Color: 2
Size: 299249 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 435490 Color: 0
Size: 314497 Color: 2
Size: 250014 Color: 4

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 387699 Color: 4
Size: 328552 Color: 1
Size: 283750 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 376712 Color: 3
Size: 331459 Color: 1
Size: 291830 Color: 3

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 360988 Color: 4
Size: 354533 Color: 1
Size: 284480 Color: 4

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 445910 Color: 1
Size: 299001 Color: 3
Size: 255090 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 413388 Color: 4
Size: 331118 Color: 1
Size: 255495 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 412273 Color: 1
Size: 331669 Color: 1
Size: 256059 Color: 2

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 448431 Color: 1
Size: 293991 Color: 0
Size: 257579 Color: 3

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 370225 Color: 0
Size: 362210 Color: 1
Size: 267566 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 440452 Color: 3
Size: 302551 Color: 4
Size: 256998 Color: 1

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 387354 Color: 4
Size: 329237 Color: 3
Size: 283410 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 383773 Color: 3
Size: 351504 Color: 3
Size: 264724 Color: 4

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 361206 Color: 2
Size: 331142 Color: 2
Size: 307653 Color: 3

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 468021 Color: 0
Size: 266078 Color: 1
Size: 265902 Color: 3

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 468030 Color: 1
Size: 273766 Color: 2
Size: 258205 Color: 2

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 453307 Color: 1
Size: 273401 Color: 2
Size: 273293 Color: 3

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 363088 Color: 3
Size: 328998 Color: 0
Size: 307915 Color: 1

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 383073 Color: 4
Size: 323970 Color: 3
Size: 292958 Color: 2

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 377098 Color: 3
Size: 350703 Color: 0
Size: 272200 Color: 3

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 394184 Color: 4
Size: 313096 Color: 4
Size: 292721 Color: 3

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 452934 Color: 0
Size: 292534 Color: 4
Size: 254533 Color: 2

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 406547 Color: 2
Size: 342808 Color: 0
Size: 250646 Color: 3

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 464491 Color: 1
Size: 269602 Color: 3
Size: 265908 Color: 1

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 409476 Color: 2
Size: 339880 Color: 1
Size: 250645 Color: 4

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 413447 Color: 0
Size: 294147 Color: 3
Size: 292407 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 388746 Color: 1
Size: 345320 Color: 3
Size: 265935 Color: 2

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 369804 Color: 3
Size: 342052 Color: 1
Size: 288145 Color: 4

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 397035 Color: 1
Size: 320854 Color: 0
Size: 282112 Color: 4

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 485241 Color: 3
Size: 263875 Color: 2
Size: 250885 Color: 3

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 405160 Color: 3
Size: 304508 Color: 4
Size: 290333 Color: 4

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 392370 Color: 3
Size: 351371 Color: 4
Size: 256260 Color: 3

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 431731 Color: 3
Size: 310212 Color: 1
Size: 258058 Color: 2

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 380974 Color: 3
Size: 335976 Color: 1
Size: 283051 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 389680 Color: 0
Size: 342348 Color: 0
Size: 267973 Color: 4

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 445990 Color: 3
Size: 277943 Color: 0
Size: 276068 Color: 4

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 403065 Color: 4
Size: 325159 Color: 1
Size: 271777 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 468627 Color: 0
Size: 269593 Color: 3
Size: 261781 Color: 2

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 354549 Color: 3
Size: 351153 Color: 4
Size: 294299 Color: 2

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 437374 Color: 4
Size: 286653 Color: 0
Size: 275974 Color: 3

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 373013 Color: 4
Size: 338171 Color: 3
Size: 288817 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 417101 Color: 1
Size: 316399 Color: 3
Size: 266501 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 422720 Color: 3
Size: 294091 Color: 4
Size: 283190 Color: 2

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 350190 Color: 2
Size: 333517 Color: 0
Size: 316294 Color: 2

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 426015 Color: 4
Size: 307938 Color: 2
Size: 266048 Color: 3

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 424598 Color: 0
Size: 288734 Color: 4
Size: 286669 Color: 3

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 353488 Color: 3
Size: 349342 Color: 3
Size: 297171 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 375771 Color: 1
Size: 344734 Color: 0
Size: 279496 Color: 2

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 423438 Color: 1
Size: 294595 Color: 2
Size: 281968 Color: 4

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 404880 Color: 0
Size: 317007 Color: 2
Size: 278114 Color: 3

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 371951 Color: 4
Size: 352497 Color: 0
Size: 275553 Color: 4

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 433959 Color: 0
Size: 290206 Color: 4
Size: 275836 Color: 1

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 379277 Color: 0
Size: 336277 Color: 2
Size: 284447 Color: 1

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 472146 Color: 0
Size: 272018 Color: 3
Size: 255837 Color: 4

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 415628 Color: 0
Size: 333468 Color: 1
Size: 250905 Color: 2

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 434757 Color: 0
Size: 291925 Color: 4
Size: 273319 Color: 2

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 495491 Color: 4
Size: 254314 Color: 3
Size: 250196 Color: 1

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 471228 Color: 2
Size: 265004 Color: 1
Size: 263769 Color: 3

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 392238 Color: 0
Size: 354811 Color: 1
Size: 252952 Color: 1

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 402080 Color: 4
Size: 313035 Color: 3
Size: 284886 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 462781 Color: 0
Size: 281913 Color: 3
Size: 255307 Color: 4

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 391567 Color: 0
Size: 321899 Color: 3
Size: 286535 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 495450 Color: 4
Size: 254174 Color: 3
Size: 250377 Color: 4

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 414128 Color: 1
Size: 312127 Color: 1
Size: 273746 Color: 3

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 363345 Color: 0
Size: 334818 Color: 4
Size: 301838 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 441642 Color: 1
Size: 294178 Color: 3
Size: 264181 Color: 4

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 424541 Color: 1
Size: 318280 Color: 1
Size: 257180 Color: 3

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 375164 Color: 0
Size: 351910 Color: 4
Size: 272927 Color: 2

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 396576 Color: 4
Size: 305531 Color: 3
Size: 297894 Color: 3

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 369824 Color: 1
Size: 353669 Color: 4
Size: 276508 Color: 3

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 482312 Color: 2
Size: 260898 Color: 3
Size: 256791 Color: 4

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 416183 Color: 4
Size: 293834 Color: 3
Size: 289984 Color: 1

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 368929 Color: 3
Size: 327408 Color: 0
Size: 303664 Color: 1

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 424589 Color: 2
Size: 289127 Color: 2
Size: 286285 Color: 4

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 423931 Color: 1
Size: 288672 Color: 2
Size: 287398 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 405797 Color: 3
Size: 333910 Color: 4
Size: 260294 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 470779 Color: 2
Size: 267852 Color: 0
Size: 261370 Color: 3

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 438170 Color: 2
Size: 303735 Color: 2
Size: 258096 Color: 4

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 492169 Color: 1
Size: 255196 Color: 3
Size: 252636 Color: 4

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 484826 Color: 1
Size: 260601 Color: 0
Size: 254574 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 367497 Color: 4
Size: 356688 Color: 1
Size: 275816 Color: 3

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 404093 Color: 3
Size: 322335 Color: 3
Size: 273573 Color: 4

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 450175 Color: 0
Size: 293311 Color: 3
Size: 256515 Color: 1

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 478175 Color: 3
Size: 267165 Color: 4
Size: 254661 Color: 0

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 417970 Color: 2
Size: 321651 Color: 1
Size: 260380 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 440319 Color: 1
Size: 290035 Color: 4
Size: 269647 Color: 4

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 401785 Color: 1
Size: 321422 Color: 3
Size: 276794 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 483664 Color: 2
Size: 258902 Color: 1
Size: 257435 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 414514 Color: 3
Size: 297711 Color: 4
Size: 287776 Color: 4

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 424204 Color: 3
Size: 306297 Color: 4
Size: 269500 Color: 1

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 416368 Color: 4
Size: 325443 Color: 3
Size: 258190 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 359743 Color: 2
Size: 333876 Color: 4
Size: 306382 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 367380 Color: 3
Size: 362026 Color: 1
Size: 270595 Color: 2

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 460205 Color: 3
Size: 277180 Color: 2
Size: 262616 Color: 4

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 441779 Color: 2
Size: 279668 Color: 2
Size: 278554 Color: 1

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 433798 Color: 1
Size: 287170 Color: 0
Size: 279033 Color: 2

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 393868 Color: 0
Size: 331643 Color: 2
Size: 274490 Color: 1

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 382092 Color: 3
Size: 349872 Color: 2
Size: 268037 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 462960 Color: 3
Size: 280488 Color: 1
Size: 256553 Color: 1

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 388017 Color: 4
Size: 311374 Color: 1
Size: 300610 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 475536 Color: 4
Size: 266228 Color: 4
Size: 258237 Color: 3

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 374015 Color: 0
Size: 316470 Color: 3
Size: 309516 Color: 4

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 490871 Color: 1
Size: 254761 Color: 3
Size: 254369 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 419946 Color: 3
Size: 303137 Color: 1
Size: 276918 Color: 1

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 423902 Color: 2
Size: 319100 Color: 2
Size: 256999 Color: 3

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 479126 Color: 0
Size: 265833 Color: 3
Size: 255042 Color: 4

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 374406 Color: 3
Size: 333608 Color: 4
Size: 291987 Color: 1

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 412909 Color: 3
Size: 323592 Color: 4
Size: 263500 Color: 2

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 430302 Color: 2
Size: 297724 Color: 3
Size: 271975 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 409605 Color: 3
Size: 316236 Color: 1
Size: 274160 Color: 1

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 390712 Color: 3
Size: 308454 Color: 0
Size: 300835 Color: 4

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 439997 Color: 3
Size: 294899 Color: 1
Size: 265105 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 420052 Color: 2
Size: 302893 Color: 4
Size: 277056 Color: 1

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 452908 Color: 0
Size: 286753 Color: 3
Size: 260340 Color: 4

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 419258 Color: 0
Size: 301702 Color: 2
Size: 279041 Color: 1

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 415090 Color: 1
Size: 301593 Color: 0
Size: 283318 Color: 3

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 377241 Color: 3
Size: 341393 Color: 0
Size: 281367 Color: 1

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 420251 Color: 1
Size: 313159 Color: 0
Size: 266591 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 365517 Color: 0
Size: 363039 Color: 4
Size: 271445 Color: 1

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 409789 Color: 2
Size: 329797 Color: 3
Size: 260415 Color: 1

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 488340 Color: 1
Size: 260021 Color: 4
Size: 251640 Color: 4

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 365870 Color: 1
Size: 328198 Color: 4
Size: 305933 Color: 4

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 379996 Color: 4
Size: 346408 Color: 0
Size: 273597 Color: 3

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 375070 Color: 4
Size: 339266 Color: 3
Size: 285665 Color: 4

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 352615 Color: 2
Size: 332238 Color: 0
Size: 315148 Color: 2

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 392499 Color: 0
Size: 316760 Color: 1
Size: 290742 Color: 1

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 388136 Color: 4
Size: 344382 Color: 4
Size: 267483 Color: 1

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 380771 Color: 2
Size: 353272 Color: 0
Size: 265958 Color: 1

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 462584 Color: 0
Size: 283276 Color: 3
Size: 254141 Color: 4

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 460776 Color: 2
Size: 278731 Color: 1
Size: 260494 Color: 1

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 386272 Color: 1
Size: 326828 Color: 0
Size: 286901 Color: 3

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 416893 Color: 4
Size: 304850 Color: 2
Size: 278258 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 359023 Color: 0
Size: 336626 Color: 1
Size: 304352 Color: 2

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 482185 Color: 2
Size: 260586 Color: 2
Size: 257230 Color: 1

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 445328 Color: 4
Size: 300476 Color: 0
Size: 254197 Color: 2

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 424589 Color: 4
Size: 300880 Color: 3
Size: 274532 Color: 2

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 382955 Color: 3
Size: 321707 Color: 0
Size: 295339 Color: 3

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 414028 Color: 3
Size: 293806 Color: 0
Size: 292167 Color: 1

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 362950 Color: 1
Size: 321710 Color: 3
Size: 315341 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 368043 Color: 1
Size: 367838 Color: 1
Size: 264120 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 497924 Color: 4
Size: 251125 Color: 3
Size: 250952 Color: 1

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 412402 Color: 0
Size: 296700 Color: 4
Size: 290899 Color: 4

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 436646 Color: 3
Size: 291311 Color: 0
Size: 272044 Color: 1

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 365458 Color: 0
Size: 358434 Color: 3
Size: 276109 Color: 1

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 347497 Color: 4
Size: 343540 Color: 3
Size: 308964 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 395633 Color: 0
Size: 350164 Color: 4
Size: 254204 Color: 3

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 355611 Color: 0
Size: 350043 Color: 2
Size: 294347 Color: 2

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 380876 Color: 4
Size: 348197 Color: 2
Size: 270928 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 371180 Color: 3
Size: 356748 Color: 2
Size: 272073 Color: 2

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 494548 Color: 1
Size: 253780 Color: 3
Size: 251673 Color: 4

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 404666 Color: 0
Size: 330413 Color: 4
Size: 264922 Color: 4

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 435913 Color: 2
Size: 311305 Color: 3
Size: 252783 Color: 1

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 422359 Color: 0
Size: 302879 Color: 4
Size: 274763 Color: 2

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 396384 Color: 0
Size: 308965 Color: 4
Size: 294652 Color: 3

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 477770 Color: 4
Size: 267559 Color: 1
Size: 254672 Color: 2

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 431888 Color: 2
Size: 305206 Color: 3
Size: 262907 Color: 1

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 369390 Color: 4
Size: 364228 Color: 4
Size: 266383 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 376590 Color: 2
Size: 335976 Color: 2
Size: 287435 Color: 1

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 449467 Color: 3
Size: 279223 Color: 1
Size: 271311 Color: 4

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 374519 Color: 0
Size: 372754 Color: 2
Size: 252728 Color: 1

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 362638 Color: 0
Size: 327922 Color: 1
Size: 309441 Color: 3

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 392366 Color: 1
Size: 322151 Color: 0
Size: 285484 Color: 3

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 360850 Color: 4
Size: 338240 Color: 0
Size: 300911 Color: 2

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 435472 Color: 1
Size: 313921 Color: 4
Size: 250608 Color: 2

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 429008 Color: 2
Size: 305707 Color: 1
Size: 265286 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 387796 Color: 2
Size: 322469 Color: 4
Size: 289736 Color: 3

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 342172 Color: 1
Size: 337803 Color: 0
Size: 320026 Color: 3

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 475999 Color: 0
Size: 270722 Color: 3
Size: 253280 Color: 2

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 479124 Color: 2
Size: 265541 Color: 1
Size: 255336 Color: 4

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 373566 Color: 4
Size: 328762 Color: 1
Size: 297673 Color: 3

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 493379 Color: 1
Size: 256568 Color: 2
Size: 250054 Color: 3

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 384473 Color: 1
Size: 324716 Color: 2
Size: 290812 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 368932 Color: 2
Size: 357677 Color: 1
Size: 273392 Color: 1

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 372128 Color: 4
Size: 315448 Color: 0
Size: 312425 Color: 3

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 451295 Color: 3
Size: 280278 Color: 4
Size: 268428 Color: 2

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 457321 Color: 0
Size: 286788 Color: 0
Size: 255892 Color: 4

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 376815 Color: 3
Size: 313913 Color: 0
Size: 309273 Color: 1

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 381945 Color: 3
Size: 362307 Color: 1
Size: 255749 Color: 3

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 392154 Color: 0
Size: 334407 Color: 2
Size: 273440 Color: 2

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 483184 Color: 2
Size: 261929 Color: 0
Size: 254888 Color: 1

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 481399 Color: 1
Size: 265020 Color: 2
Size: 253582 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 497929 Color: 1
Size: 251946 Color: 4
Size: 250126 Color: 2

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 401665 Color: 3
Size: 330151 Color: 0
Size: 268185 Color: 4

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 418514 Color: 1
Size: 301282 Color: 3
Size: 280205 Color: 1

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 448372 Color: 3
Size: 282266 Color: 2
Size: 269363 Color: 1

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 433640 Color: 3
Size: 300259 Color: 2
Size: 266102 Color: 2

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 431550 Color: 2
Size: 315636 Color: 1
Size: 252815 Color: 3

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 447131 Color: 0
Size: 279489 Color: 2
Size: 273381 Color: 3

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 378388 Color: 3
Size: 323495 Color: 4
Size: 298118 Color: 0

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 474297 Color: 3
Size: 267511 Color: 2
Size: 258193 Color: 3

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 457304 Color: 4
Size: 280767 Color: 4
Size: 261930 Color: 0

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 408267 Color: 2
Size: 310391 Color: 0
Size: 281343 Color: 1

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 378795 Color: 2
Size: 326494 Color: 0
Size: 294712 Color: 4

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 338130 Color: 0
Size: 336421 Color: 0
Size: 325450 Color: 3

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 452170 Color: 3
Size: 290659 Color: 2
Size: 257172 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 410100 Color: 0
Size: 323630 Color: 3
Size: 266271 Color: 4

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 471368 Color: 2
Size: 267626 Color: 0
Size: 261007 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 436084 Color: 2
Size: 305473 Color: 4
Size: 258444 Color: 1

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 366862 Color: 3
Size: 351949 Color: 0
Size: 281190 Color: 2

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 471036 Color: 3
Size: 272030 Color: 0
Size: 256935 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 478193 Color: 1
Size: 267383 Color: 4
Size: 254425 Color: 1

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 451383 Color: 1
Size: 281161 Color: 1
Size: 267457 Color: 3

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 377700 Color: 2
Size: 329719 Color: 4
Size: 292582 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 402687 Color: 2
Size: 326259 Color: 2
Size: 271055 Color: 3

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 397412 Color: 0
Size: 330115 Color: 2
Size: 272474 Color: 2

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 427695 Color: 1
Size: 291030 Color: 0
Size: 281276 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 389432 Color: 3
Size: 307342 Color: 1
Size: 303227 Color: 2

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 434415 Color: 2
Size: 300707 Color: 3
Size: 264879 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 400127 Color: 4
Size: 319961 Color: 0
Size: 279913 Color: 2

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 361243 Color: 2
Size: 333528 Color: 0
Size: 305230 Color: 1

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 439660 Color: 0
Size: 287522 Color: 3
Size: 272819 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 401593 Color: 1
Size: 321048 Color: 4
Size: 277360 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 494961 Color: 2
Size: 252959 Color: 0
Size: 252081 Color: 2

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 416669 Color: 4
Size: 325285 Color: 4
Size: 258047 Color: 3

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 496624 Color: 2
Size: 252762 Color: 1
Size: 250615 Color: 3

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 453085 Color: 1
Size: 274904 Color: 3
Size: 272012 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 369291 Color: 2
Size: 362706 Color: 1
Size: 268004 Color: 3

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 446446 Color: 3
Size: 280914 Color: 1
Size: 272641 Color: 2

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 379714 Color: 4
Size: 341908 Color: 2
Size: 278379 Color: 3

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 489282 Color: 2
Size: 257771 Color: 0
Size: 252948 Color: 1

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 463252 Color: 3
Size: 280716 Color: 2
Size: 256033 Color: 2

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 488618 Color: 4
Size: 257972 Color: 3
Size: 253411 Color: 1

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 405761 Color: 0
Size: 328222 Color: 2
Size: 266018 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 478191 Color: 3
Size: 261237 Color: 0
Size: 260573 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 492542 Color: 3
Size: 255545 Color: 0
Size: 251914 Color: 1

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 396149 Color: 4
Size: 324357 Color: 2
Size: 279495 Color: 3

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 391051 Color: 4
Size: 313944 Color: 1
Size: 295006 Color: 1

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 392817 Color: 3
Size: 353177 Color: 0
Size: 254007 Color: 4

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 434203 Color: 4
Size: 296227 Color: 3
Size: 269571 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 406260 Color: 1
Size: 310430 Color: 0
Size: 283311 Color: 1

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 450342 Color: 4
Size: 292239 Color: 2
Size: 257420 Color: 3

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 426976 Color: 0
Size: 290297 Color: 3
Size: 282728 Color: 1

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 379090 Color: 4
Size: 310469 Color: 3
Size: 310442 Color: 2

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 488446 Color: 2
Size: 260182 Color: 3
Size: 251373 Color: 4

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 395642 Color: 3
Size: 327434 Color: 2
Size: 276925 Color: 2

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 451958 Color: 3
Size: 289840 Color: 2
Size: 258203 Color: 2

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 452110 Color: 3
Size: 292637 Color: 2
Size: 255254 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 484033 Color: 4
Size: 265394 Color: 1
Size: 250574 Color: 1

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 499287 Color: 3
Size: 250382 Color: 2
Size: 250332 Color: 4

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 447960 Color: 4
Size: 284061 Color: 4
Size: 267980 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 350944 Color: 3
Size: 349599 Color: 2
Size: 299458 Color: 2

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 348055 Color: 0
Size: 328380 Color: 4
Size: 323566 Color: 3

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 447699 Color: 4
Size: 301252 Color: 2
Size: 251050 Color: 2

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 490513 Color: 0
Size: 259279 Color: 3
Size: 250209 Color: 1

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 388749 Color: 3
Size: 330000 Color: 2
Size: 281252 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 409279 Color: 3
Size: 335077 Color: 4
Size: 255645 Color: 2

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 343602 Color: 0
Size: 331211 Color: 2
Size: 325188 Color: 4

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 379134 Color: 2
Size: 344125 Color: 0
Size: 276742 Color: 4

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 379621 Color: 1
Size: 336832 Color: 2
Size: 283548 Color: 2

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 383436 Color: 1
Size: 360092 Color: 0
Size: 256473 Color: 4

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 368845 Color: 0
Size: 365638 Color: 2
Size: 265518 Color: 1

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 484822 Color: 2
Size: 259022 Color: 3
Size: 256157 Color: 1

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 476826 Color: 4
Size: 262868 Color: 4
Size: 260307 Color: 0

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 460722 Color: 2
Size: 278417 Color: 1
Size: 260862 Color: 3

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 437555 Color: 3
Size: 282701 Color: 1
Size: 279745 Color: 2

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 364328 Color: 1
Size: 335425 Color: 0
Size: 300248 Color: 1

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 424569 Color: 4
Size: 292585 Color: 1
Size: 282847 Color: 3

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 472500 Color: 1
Size: 275180 Color: 3
Size: 252321 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 426670 Color: 3
Size: 299769 Color: 1
Size: 273562 Color: 4

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 377997 Color: 0
Size: 351629 Color: 3
Size: 270375 Color: 1

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 444745 Color: 3
Size: 277683 Color: 0
Size: 277573 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 399047 Color: 1
Size: 331940 Color: 2
Size: 269014 Color: 2

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 443670 Color: 2
Size: 300857 Color: 3
Size: 255474 Color: 2

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 460716 Color: 4
Size: 280272 Color: 2
Size: 259013 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 389208 Color: 3
Size: 328828 Color: 4
Size: 281965 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 417391 Color: 1
Size: 301241 Color: 2
Size: 281369 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 496992 Color: 1
Size: 251593 Color: 4
Size: 251416 Color: 3

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 441061 Color: 1
Size: 280542 Color: 0
Size: 278398 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 483439 Color: 3
Size: 261372 Color: 2
Size: 255190 Color: 1

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 405792 Color: 4
Size: 330329 Color: 2
Size: 263880 Color: 3

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 412712 Color: 4
Size: 335468 Color: 0
Size: 251821 Color: 4

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 358861 Color: 0
Size: 348567 Color: 1
Size: 292573 Color: 2

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 424993 Color: 3
Size: 322616 Color: 4
Size: 252392 Color: 2

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 384075 Color: 3
Size: 365910 Color: 3
Size: 250016 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 427885 Color: 4
Size: 299331 Color: 4
Size: 272785 Color: 1

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 400432 Color: 2
Size: 337047 Color: 4
Size: 262522 Color: 3

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 437908 Color: 4
Size: 287495 Color: 0
Size: 274598 Color: 2

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 423884 Color: 0
Size: 325492 Color: 2
Size: 250625 Color: 4

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 432431 Color: 1
Size: 302657 Color: 4
Size: 264913 Color: 3

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 412281 Color: 1
Size: 329477 Color: 3
Size: 258243 Color: 2

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 437761 Color: 3
Size: 303912 Color: 0
Size: 258328 Color: 4

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 367926 Color: 4
Size: 323620 Color: 2
Size: 308455 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 422633 Color: 2
Size: 298444 Color: 1
Size: 278924 Color: 3

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 350721 Color: 0
Size: 340390 Color: 4
Size: 308890 Color: 4

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 386544 Color: 2
Size: 308452 Color: 0
Size: 305005 Color: 3

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 384008 Color: 2
Size: 320264 Color: 4
Size: 295729 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 359164 Color: 1
Size: 352395 Color: 1
Size: 288442 Color: 2

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 497949 Color: 0
Size: 251907 Color: 1
Size: 250145 Color: 1

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 384670 Color: 3
Size: 348751 Color: 3
Size: 266580 Color: 2

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 470865 Color: 3
Size: 270376 Color: 2
Size: 258760 Color: 1

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 443891 Color: 2
Size: 283220 Color: 0
Size: 272890 Color: 1

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 354244 Color: 2
Size: 345845 Color: 1
Size: 299912 Color: 2

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 383814 Color: 0
Size: 346665 Color: 0
Size: 269522 Color: 4

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 350060 Color: 2
Size: 331058 Color: 3
Size: 318883 Color: 2

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 435593 Color: 1
Size: 285970 Color: 4
Size: 278438 Color: 4

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 445351 Color: 2
Size: 287195 Color: 1
Size: 267455 Color: 1

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 374704 Color: 2
Size: 366669 Color: 1
Size: 258628 Color: 4

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 376071 Color: 0
Size: 327013 Color: 2
Size: 296917 Color: 1

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 481942 Color: 0
Size: 262413 Color: 3
Size: 255646 Color: 4

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 375065 Color: 2
Size: 329601 Color: 2
Size: 295335 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 443717 Color: 2
Size: 293096 Color: 2
Size: 263188 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 345708 Color: 1
Size: 336989 Color: 0
Size: 317304 Color: 1

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 436433 Color: 2
Size: 309512 Color: 3
Size: 254056 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 366605 Color: 0
Size: 325930 Color: 1
Size: 307466 Color: 4

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 489637 Color: 4
Size: 257711 Color: 1
Size: 252653 Color: 1

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 365789 Color: 4
Size: 331191 Color: 0
Size: 303021 Color: 1

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 459625 Color: 1
Size: 290016 Color: 0
Size: 250360 Color: 4

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 444326 Color: 4
Size: 300733 Color: 3
Size: 254942 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 375921 Color: 3
Size: 328181 Color: 0
Size: 295899 Color: 3

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 432116 Color: 4
Size: 310179 Color: 3
Size: 257706 Color: 2

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 476166 Color: 4
Size: 269920 Color: 2
Size: 253915 Color: 3

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 431273 Color: 3
Size: 315693 Color: 4
Size: 253035 Color: 2

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 485273 Color: 3
Size: 259027 Color: 4
Size: 255701 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 403689 Color: 0
Size: 320380 Color: 2
Size: 275932 Color: 4

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 372915 Color: 0
Size: 354250 Color: 2
Size: 272836 Color: 3

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 445439 Color: 2
Size: 291509 Color: 3
Size: 263053 Color: 2

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 451827 Color: 3
Size: 296097 Color: 1
Size: 252077 Color: 1

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 365942 Color: 3
Size: 340708 Color: 2
Size: 293351 Color: 1

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 429572 Color: 4
Size: 316978 Color: 0
Size: 253451 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 497092 Color: 4
Size: 251602 Color: 1
Size: 251307 Color: 1

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 399954 Color: 4
Size: 307019 Color: 0
Size: 293028 Color: 1

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 414352 Color: 4
Size: 335606 Color: 0
Size: 250043 Color: 3

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 398949 Color: 3
Size: 334336 Color: 3
Size: 266716 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 374733 Color: 0
Size: 347273 Color: 2
Size: 277995 Color: 2

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 439217 Color: 1
Size: 304084 Color: 4
Size: 256700 Color: 4

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 492549 Color: 3
Size: 255064 Color: 1
Size: 252388 Color: 4

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 402828 Color: 4
Size: 308751 Color: 3
Size: 288422 Color: 2

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 446209 Color: 3
Size: 300600 Color: 4
Size: 253192 Color: 0

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 376060 Color: 1
Size: 342752 Color: 0
Size: 281189 Color: 1

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 379615 Color: 2
Size: 339010 Color: 1
Size: 281376 Color: 1

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 373007 Color: 0
Size: 372415 Color: 1
Size: 254579 Color: 3

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 433586 Color: 3
Size: 288269 Color: 1
Size: 278146 Color: 2

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 473911 Color: 1
Size: 273945 Color: 4
Size: 252145 Color: 2

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 455939 Color: 0
Size: 272308 Color: 2
Size: 271754 Color: 1

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 375071 Color: 0
Size: 318576 Color: 4
Size: 306354 Color: 1

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 437146 Color: 4
Size: 310840 Color: 3
Size: 252015 Color: 3

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 357106 Color: 2
Size: 337785 Color: 4
Size: 305110 Color: 2

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 374648 Color: 2
Size: 337266 Color: 0
Size: 288087 Color: 3

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 407212 Color: 2
Size: 310905 Color: 1
Size: 281884 Color: 3

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 388911 Color: 4
Size: 334105 Color: 1
Size: 276985 Color: 1

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 457624 Color: 4
Size: 291550 Color: 1
Size: 250827 Color: 2

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 412410 Color: 0
Size: 303756 Color: 1
Size: 283835 Color: 3

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 385191 Color: 4
Size: 355174 Color: 3
Size: 259636 Color: 1

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 474851 Color: 4
Size: 270720 Color: 3
Size: 254430 Color: 1

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 399067 Color: 2
Size: 327097 Color: 3
Size: 273837 Color: 3

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 366317 Color: 4
Size: 327261 Color: 3
Size: 306423 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 398475 Color: 0
Size: 306843 Color: 1
Size: 294683 Color: 4

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 447988 Color: 1
Size: 279743 Color: 3
Size: 272270 Color: 2

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 381917 Color: 3
Size: 348712 Color: 0
Size: 269372 Color: 2

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 441602 Color: 2
Size: 279906 Color: 0
Size: 278493 Color: 3

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 367404 Color: 2
Size: 324570 Color: 1
Size: 308027 Color: 3

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 375972 Color: 2
Size: 373866 Color: 0
Size: 250163 Color: 2

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 368220 Color: 2
Size: 339843 Color: 2
Size: 291938 Color: 1

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 379050 Color: 3
Size: 335672 Color: 0
Size: 285279 Color: 3

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 398468 Color: 2
Size: 308129 Color: 1
Size: 293404 Color: 3

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 477307 Color: 4
Size: 270916 Color: 1
Size: 251778 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 491045 Color: 3
Size: 258272 Color: 1
Size: 250684 Color: 4

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 434816 Color: 1
Size: 293695 Color: 4
Size: 271490 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 367734 Color: 1
Size: 332864 Color: 0
Size: 299403 Color: 4

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 363089 Color: 2
Size: 354053 Color: 3
Size: 282859 Color: 2

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 409114 Color: 3
Size: 318182 Color: 1
Size: 272705 Color: 4

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 415140 Color: 2
Size: 308048 Color: 3
Size: 276813 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 407671 Color: 4
Size: 328969 Color: 3
Size: 263361 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 338017 Color: 1
Size: 337125 Color: 0
Size: 324859 Color: 4

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 371566 Color: 4
Size: 322531 Color: 3
Size: 305904 Color: 2

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 447237 Color: 2
Size: 291807 Color: 3
Size: 260957 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 428348 Color: 1
Size: 289950 Color: 4
Size: 281703 Color: 3

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 394582 Color: 3
Size: 330081 Color: 4
Size: 275338 Color: 1

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 398301 Color: 2
Size: 341129 Color: 1
Size: 260571 Color: 4

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 424874 Color: 2
Size: 291496 Color: 4
Size: 283631 Color: 1

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 419654 Color: 1
Size: 316452 Color: 4
Size: 263895 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 440241 Color: 1
Size: 296143 Color: 3
Size: 263617 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 414687 Color: 0
Size: 307439 Color: 4
Size: 277875 Color: 3

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 411007 Color: 2
Size: 322735 Color: 0
Size: 266259 Color: 4

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 420163 Color: 4
Size: 327823 Color: 1
Size: 252015 Color: 4

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 354690 Color: 2
Size: 346525 Color: 3
Size: 298786 Color: 3

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 373981 Color: 3
Size: 339583 Color: 3
Size: 286437 Color: 1

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 391829 Color: 2
Size: 314560 Color: 0
Size: 293612 Color: 1

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 438000 Color: 2
Size: 284250 Color: 4
Size: 277751 Color: 1

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 486992 Color: 1
Size: 261510 Color: 1
Size: 251499 Color: 3

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 435480 Color: 2
Size: 284287 Color: 4
Size: 280234 Color: 3

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 484947 Color: 2
Size: 259005 Color: 0
Size: 256049 Color: 4

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 383860 Color: 0
Size: 331374 Color: 1
Size: 284767 Color: 3

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 425333 Color: 2
Size: 323229 Color: 1
Size: 251439 Color: 1

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 476527 Color: 3
Size: 273170 Color: 1
Size: 250304 Color: 2

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 462369 Color: 3
Size: 271663 Color: 4
Size: 265969 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 361677 Color: 2
Size: 353519 Color: 4
Size: 284805 Color: 3

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 366039 Color: 0
Size: 337732 Color: 2
Size: 296230 Color: 4

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 403509 Color: 1
Size: 313393 Color: 4
Size: 283099 Color: 3

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 366782 Color: 3
Size: 349915 Color: 2
Size: 283304 Color: 1

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 468347 Color: 3
Size: 267422 Color: 4
Size: 264232 Color: 1

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 428933 Color: 3
Size: 311533 Color: 1
Size: 259535 Color: 1

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 433788 Color: 4
Size: 312876 Color: 3
Size: 253337 Color: 4

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 343499 Color: 0
Size: 334359 Color: 4
Size: 322143 Color: 2

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 483153 Color: 0
Size: 258819 Color: 0
Size: 258029 Color: 4

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 357008 Color: 4
Size: 350384 Color: 0
Size: 292609 Color: 3

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 365067 Color: 4
Size: 359944 Color: 4
Size: 274990 Color: 2

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 433216 Color: 2
Size: 290064 Color: 0
Size: 276721 Color: 4

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 459957 Color: 3
Size: 283297 Color: 2
Size: 256747 Color: 1

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 405125 Color: 1
Size: 333210 Color: 3
Size: 261666 Color: 1

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 370287 Color: 4
Size: 327798 Color: 3
Size: 301916 Color: 4

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 366832 Color: 2
Size: 332018 Color: 4
Size: 301151 Color: 1

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 480745 Color: 4
Size: 269250 Color: 0
Size: 250006 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 467641 Color: 1
Size: 280438 Color: 0
Size: 251922 Color: 2

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 397767 Color: 2
Size: 320891 Color: 3
Size: 281343 Color: 1

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 400622 Color: 1
Size: 336872 Color: 3
Size: 262507 Color: 1

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 469398 Color: 3
Size: 269000 Color: 4
Size: 261603 Color: 3

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 418013 Color: 1
Size: 306251 Color: 1
Size: 275737 Color: 0

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 425479 Color: 3
Size: 288175 Color: 0
Size: 286347 Color: 2

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 402653 Color: 4
Size: 341153 Color: 3
Size: 256195 Color: 2

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 486883 Color: 1
Size: 257016 Color: 0
Size: 256102 Color: 3

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 408962 Color: 2
Size: 326310 Color: 3
Size: 264729 Color: 4

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 352419 Color: 3
Size: 342676 Color: 2
Size: 304906 Color: 3

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 426140 Color: 3
Size: 319357 Color: 1
Size: 254504 Color: 2

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 358273 Color: 2
Size: 327014 Color: 3
Size: 314714 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 345168 Color: 2
Size: 330568 Color: 2
Size: 324265 Color: 1

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 337382 Color: 1
Size: 334995 Color: 1
Size: 327624 Color: 2

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 348912 Color: 4
Size: 333241 Color: 0
Size: 317848 Color: 4

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 361638 Color: 4
Size: 361528 Color: 2
Size: 276835 Color: 1

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 361743 Color: 3
Size: 355037 Color: 3
Size: 283221 Color: 4

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 362489 Color: 0
Size: 355912 Color: 4
Size: 281600 Color: 2

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 362587 Color: 3
Size: 360156 Color: 2
Size: 277258 Color: 1

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 348808 Color: 3
Size: 326354 Color: 2
Size: 324839 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 368958 Color: 3
Size: 322483 Color: 0
Size: 308560 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 364113 Color: 1
Size: 357880 Color: 0
Size: 278008 Color: 4

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 372565 Color: 3
Size: 314081 Color: 2
Size: 313355 Color: 2

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 356972 Color: 4
Size: 322742 Color: 3
Size: 320287 Color: 4

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 354884 Color: 0
Size: 334503 Color: 4
Size: 310614 Color: 2

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 348572 Color: 0
Size: 347879 Color: 0
Size: 303550 Color: 4

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 347471 Color: 3
Size: 333151 Color: 2
Size: 319379 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 356889 Color: 3
Size: 321628 Color: 0
Size: 321484 Color: 3

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 357432 Color: 4
Size: 335721 Color: 0
Size: 306848 Color: 1

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 359227 Color: 1
Size: 340767 Color: 1
Size: 300007 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 401597 Color: 3
Size: 342957 Color: 2
Size: 255447 Color: 2

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 350102 Color: 4
Size: 333022 Color: 1
Size: 316877 Color: 2

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 355869 Color: 0
Size: 347901 Color: 2
Size: 296231 Color: 4

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 369443 Color: 1
Size: 358707 Color: 2
Size: 271851 Color: 3

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 364464 Color: 1
Size: 356974 Color: 2
Size: 278563 Color: 1

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 370743 Color: 4
Size: 365490 Color: 2
Size: 263768 Color: 4

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 356550 Color: 3
Size: 323602 Color: 0
Size: 319849 Color: 1

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 359346 Color: 0
Size: 356680 Color: 1
Size: 283975 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 348816 Color: 2
Size: 348636 Color: 2
Size: 302549 Color: 4

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 359787 Color: 3
Size: 357964 Color: 4
Size: 282250 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 362576 Color: 1
Size: 335141 Color: 0
Size: 302284 Color: 4

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 364675 Color: 1
Size: 328288 Color: 1
Size: 307038 Color: 4

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 348373 Color: 2
Size: 340558 Color: 3
Size: 311070 Color: 2

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 358765 Color: 1
Size: 347569 Color: 1
Size: 293667 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 362515 Color: 0
Size: 322289 Color: 0
Size: 315197 Color: 4

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 351705 Color: 1
Size: 335864 Color: 4
Size: 312432 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 369473 Color: 0
Size: 357317 Color: 4
Size: 273211 Color: 4

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 361561 Color: 0
Size: 340066 Color: 1
Size: 298374 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 379001 Color: 3
Size: 354866 Color: 3
Size: 266134 Color: 1

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 356730 Color: 3
Size: 347628 Color: 4
Size: 295643 Color: 4

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 357007 Color: 2
Size: 343520 Color: 4
Size: 299474 Color: 2

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 353957 Color: 3
Size: 330553 Color: 1
Size: 315491 Color: 1

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 342356 Color: 1
Size: 337033 Color: 4
Size: 320612 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 344415 Color: 0
Size: 341598 Color: 1
Size: 313988 Color: 2

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 417852 Color: 0
Size: 318433 Color: 3
Size: 263716 Color: 0

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 357367 Color: 4
Size: 322139 Color: 4
Size: 320495 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 347142 Color: 2
Size: 344345 Color: 2
Size: 308514 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 357484 Color: 3
Size: 321628 Color: 0
Size: 320889 Color: 2

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 359065 Color: 1
Size: 343704 Color: 0
Size: 297232 Color: 4

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 337161 Color: 4
Size: 331917 Color: 0
Size: 330923 Color: 2

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 352101 Color: 4
Size: 332450 Color: 1
Size: 315450 Color: 1

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 364410 Color: 2
Size: 352130 Color: 4
Size: 283461 Color: 3

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 355074 Color: 0
Size: 326933 Color: 3
Size: 317994 Color: 3

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 355977 Color: 3
Size: 355582 Color: 2
Size: 288442 Color: 3

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 355451 Color: 1
Size: 348043 Color: 1
Size: 296507 Color: 3

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 385930 Color: 0
Size: 342305 Color: 3
Size: 271766 Color: 3

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 343363 Color: 4
Size: 332185 Color: 0
Size: 324453 Color: 3

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 350186 Color: 2
Size: 347825 Color: 0
Size: 301990 Color: 4

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 343538 Color: 2
Size: 342868 Color: 0
Size: 313595 Color: 1

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 391469 Color: 2
Size: 333754 Color: 4
Size: 274778 Color: 1

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 342467 Color: 3
Size: 337618 Color: 3
Size: 319916 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 351842 Color: 3
Size: 332247 Color: 4
Size: 315912 Color: 3

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 359005 Color: 1
Size: 344941 Color: 4
Size: 296055 Color: 3

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 335362 Color: 3
Size: 332734 Color: 4
Size: 331905 Color: 1

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 344680 Color: 1
Size: 334723 Color: 2
Size: 320598 Color: 1

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 341376 Color: 1
Size: 331987 Color: 3
Size: 326638 Color: 4

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 338285 Color: 4
Size: 334466 Color: 3
Size: 327250 Color: 2

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 352889 Color: 0
Size: 329267 Color: 0
Size: 317845 Color: 1

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 352081 Color: 3
Size: 332882 Color: 1
Size: 315038 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 335449 Color: 4
Size: 332912 Color: 4
Size: 331640 Color: 3

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 498878 Color: 2
Size: 250593 Color: 3
Size: 250530 Color: 4

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 436143 Color: 1
Size: 293574 Color: 4
Size: 270284 Color: 3

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 365427 Color: 0
Size: 357908 Color: 0
Size: 276666 Color: 3

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 344082 Color: 2
Size: 339235 Color: 3
Size: 316684 Color: 1

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 364756 Color: 2
Size: 328241 Color: 0
Size: 307004 Color: 3

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 386732 Color: 4
Size: 348202 Color: 4
Size: 265067 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 352403 Color: 1
Size: 352260 Color: 1
Size: 295338 Color: 3

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 351388 Color: 1
Size: 351260 Color: 0
Size: 297353 Color: 1

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 386416 Color: 2
Size: 359316 Color: 3
Size: 254269 Color: 4

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 352902 Color: 2
Size: 347488 Color: 0
Size: 299611 Color: 4

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 360044 Color: 3
Size: 353978 Color: 0
Size: 285979 Color: 3

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 401909 Color: 2
Size: 323124 Color: 0
Size: 274968 Color: 1

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 377523 Color: 0
Size: 359825 Color: 1
Size: 262653 Color: 3

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 419798 Color: 2
Size: 303569 Color: 1
Size: 276634 Color: 3

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 351955 Color: 4
Size: 346010 Color: 4
Size: 302036 Color: 3

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 351719 Color: 0
Size: 338859 Color: 1
Size: 309423 Color: 2

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 488648 Color: 0
Size: 256769 Color: 3
Size: 254584 Color: 4

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 363431 Color: 3
Size: 322793 Color: 3
Size: 313777 Color: 2

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 494960 Color: 4
Size: 254907 Color: 2
Size: 250134 Color: 1

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 364999 Color: 2
Size: 364378 Color: 3
Size: 270624 Color: 4

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 345877 Color: 3
Size: 339869 Color: 0
Size: 314255 Color: 3

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 344848 Color: 0
Size: 339203 Color: 2
Size: 315950 Color: 4

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 358221 Color: 2
Size: 344088 Color: 0
Size: 297692 Color: 2

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 343753 Color: 1
Size: 341402 Color: 3
Size: 314846 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 345657 Color: 1
Size: 341006 Color: 0
Size: 313338 Color: 3

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 358108 Color: 4
Size: 337687 Color: 2
Size: 304206 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 350312 Color: 2
Size: 343724 Color: 2
Size: 305965 Color: 3

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 354049 Color: 4
Size: 343188 Color: 2
Size: 302764 Color: 2

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 347014 Color: 0
Size: 338766 Color: 2
Size: 314221 Color: 2

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 349061 Color: 4
Size: 341589 Color: 1
Size: 309351 Color: 3

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 346806 Color: 3
Size: 342905 Color: 4
Size: 310290 Color: 2

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 343133 Color: 2
Size: 341265 Color: 0
Size: 315603 Color: 1

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 340682 Color: 2
Size: 339477 Color: 3
Size: 319842 Color: 2

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 363471 Color: 3
Size: 340410 Color: 4
Size: 296120 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 356085 Color: 3
Size: 337073 Color: 4
Size: 306843 Color: 1

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 348939 Color: 0
Size: 340191 Color: 2
Size: 310871 Color: 1

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 345665 Color: 4
Size: 339500 Color: 2
Size: 314836 Color: 3

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 354627 Color: 1
Size: 348650 Color: 1
Size: 296724 Color: 2

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 348857 Color: 3
Size: 339513 Color: 4
Size: 311631 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 350178 Color: 3
Size: 340319 Color: 3
Size: 309504 Color: 4

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 341110 Color: 3
Size: 339811 Color: 0
Size: 319080 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 350931 Color: 4
Size: 332309 Color: 1
Size: 316761 Color: 1

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 339926 Color: 1
Size: 336539 Color: 1
Size: 323536 Color: 2

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 341458 Color: 1
Size: 337295 Color: 0
Size: 321248 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 352881 Color: 0
Size: 338110 Color: 0
Size: 309010 Color: 1

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 339263 Color: 4
Size: 333893 Color: 0
Size: 326845 Color: 2

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 337952 Color: 0
Size: 335968 Color: 1
Size: 326081 Color: 1

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 337633 Color: 2
Size: 336590 Color: 3
Size: 325778 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 348828 Color: 1
Size: 336326 Color: 3
Size: 314847 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 344297 Color: 0
Size: 336622 Color: 4
Size: 319082 Color: 2

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 335465 Color: 0
Size: 333028 Color: 1
Size: 331508 Color: 1

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 343967 Color: 1
Size: 335339 Color: 0
Size: 320695 Color: 4

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 349457 Color: 4
Size: 334635 Color: 0
Size: 315909 Color: 4

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 341728 Color: 1
Size: 334019 Color: 1
Size: 324254 Color: 3

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 355128 Color: 1
Size: 331730 Color: 2
Size: 313143 Color: 4

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 355610 Color: 0
Size: 330798 Color: 4
Size: 313593 Color: 2

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 354470 Color: 4
Size: 335552 Color: 4
Size: 309979 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 356459 Color: 1
Size: 324872 Color: 0
Size: 318670 Color: 1

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 356217 Color: 4
Size: 326247 Color: 3
Size: 317537 Color: 4

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 357361 Color: 3
Size: 354934 Color: 2
Size: 287706 Color: 0

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 358319 Color: 1
Size: 342943 Color: 0
Size: 298739 Color: 2

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 358543 Color: 3
Size: 322457 Color: 0
Size: 319001 Color: 1

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 358873 Color: 1
Size: 328435 Color: 0
Size: 312693 Color: 2

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 358984 Color: 4
Size: 328193 Color: 3
Size: 312824 Color: 4

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 359213 Color: 3
Size: 321048 Color: 2
Size: 319740 Color: 2

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 359039 Color: 0
Size: 353534 Color: 1
Size: 287428 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 359246 Color: 1
Size: 339110 Color: 2
Size: 301645 Color: 2

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 359491 Color: 2
Size: 342548 Color: 1
Size: 297962 Color: 3

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 359582 Color: 1
Size: 326258 Color: 4
Size: 314161 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 359615 Color: 4
Size: 326682 Color: 3
Size: 313704 Color: 2

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 360128 Color: 0
Size: 342745 Color: 3
Size: 297128 Color: 3

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 360274 Color: 0
Size: 352985 Color: 2
Size: 286742 Color: 1

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 359989 Color: 3
Size: 340182 Color: 4
Size: 299830 Color: 2

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 359959 Color: 2
Size: 321070 Color: 2
Size: 318972 Color: 1

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 360620 Color: 1
Size: 322031 Color: 0
Size: 317350 Color: 1

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 360006 Color: 2
Size: 342438 Color: 1
Size: 297557 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 360933 Color: 1
Size: 354228 Color: 4
Size: 284840 Color: 4

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 361009 Color: 0
Size: 321340 Color: 1
Size: 317652 Color: 2

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 361022 Color: 2
Size: 337902 Color: 3
Size: 301077 Color: 1

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 361118 Color: 0
Size: 330187 Color: 1
Size: 308696 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 361395 Color: 2
Size: 353765 Color: 2
Size: 284841 Color: 3

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 361412 Color: 2
Size: 348286 Color: 3
Size: 290303 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 361788 Color: 4
Size: 352613 Color: 2
Size: 285600 Color: 1

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 361943 Color: 1
Size: 331555 Color: 1
Size: 306503 Color: 2

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 361963 Color: 3
Size: 335147 Color: 2
Size: 302891 Color: 1

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 362386 Color: 3
Size: 325733 Color: 3
Size: 311882 Color: 1

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 361753 Color: 2
Size: 332570 Color: 2
Size: 305678 Color: 3

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 362508 Color: 0
Size: 334706 Color: 2
Size: 302787 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 362007 Color: 2
Size: 344158 Color: 0
Size: 293836 Color: 2

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 362145 Color: 2
Size: 325011 Color: 3
Size: 312845 Color: 3

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 362761 Color: 4
Size: 324864 Color: 1
Size: 312376 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 362796 Color: 4
Size: 320130 Color: 4
Size: 317075 Color: 3

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 362713 Color: 0
Size: 352379 Color: 4
Size: 284909 Color: 3

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 362934 Color: 1
Size: 342772 Color: 3
Size: 294295 Color: 2

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 362995 Color: 3
Size: 347654 Color: 3
Size: 289352 Color: 2

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 363132 Color: 4
Size: 340436 Color: 2
Size: 296433 Color: 3

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 363239 Color: 1
Size: 323325 Color: 3
Size: 313437 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 363272 Color: 0
Size: 327950 Color: 1
Size: 308779 Color: 3

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 363252 Color: 4
Size: 333183 Color: 4
Size: 303566 Color: 2

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 363255 Color: 1
Size: 347265 Color: 0
Size: 289481 Color: 3

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 363296 Color: 1
Size: 347719 Color: 1
Size: 288986 Color: 4

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 363349 Color: 4
Size: 326767 Color: 4
Size: 309885 Color: 3

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 363361 Color: 1
Size: 351701 Color: 2
Size: 284939 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 363369 Color: 0
Size: 339722 Color: 4
Size: 296910 Color: 4

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 363560 Color: 4
Size: 351446 Color: 0
Size: 284995 Color: 3

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 363823 Color: 4
Size: 337574 Color: 2
Size: 298604 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 364060 Color: 2
Size: 331406 Color: 4
Size: 304535 Color: 3

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 364213 Color: 3
Size: 349175 Color: 0
Size: 286613 Color: 2

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 364339 Color: 4
Size: 333450 Color: 2
Size: 302212 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 364460 Color: 0
Size: 337965 Color: 4
Size: 297576 Color: 3

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 364437 Color: 4
Size: 355659 Color: 3
Size: 279905 Color: 3

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 364556 Color: 3
Size: 353430 Color: 1
Size: 282015 Color: 4

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 364659 Color: 3
Size: 322804 Color: 0
Size: 312538 Color: 2

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 364696 Color: 0
Size: 354425 Color: 3
Size: 280880 Color: 1

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 364689 Color: 3
Size: 327901 Color: 4
Size: 307411 Color: 4

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 364873 Color: 1
Size: 345400 Color: 0
Size: 289728 Color: 2

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 364916 Color: 1
Size: 346714 Color: 4
Size: 288371 Color: 1

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 364942 Color: 3
Size: 327853 Color: 1
Size: 307206 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 364942 Color: 0
Size: 325363 Color: 3
Size: 309696 Color: 2

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 365021 Color: 4
Size: 353291 Color: 2
Size: 281689 Color: 1

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 365161 Color: 0
Size: 319239 Color: 2
Size: 315601 Color: 1

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 365103 Color: 2
Size: 349930 Color: 1
Size: 284968 Color: 3

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 365169 Color: 1
Size: 344982 Color: 3
Size: 289850 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 365413 Color: 4
Size: 327384 Color: 2
Size: 307204 Color: 3

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 365415 Color: 2
Size: 360351 Color: 3
Size: 274235 Color: 0

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 365419 Color: 1
Size: 346437 Color: 3
Size: 288145 Color: 2

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 365556 Color: 4
Size: 339358 Color: 0
Size: 295087 Color: 2

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 365564 Color: 4
Size: 353480 Color: 3
Size: 280957 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 365787 Color: 0
Size: 341672 Color: 3
Size: 292542 Color: 2

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 365583 Color: 4
Size: 349165 Color: 2
Size: 285253 Color: 4

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 365845 Color: 0
Size: 326487 Color: 2
Size: 307669 Color: 1

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 365601 Color: 2
Size: 358409 Color: 2
Size: 275991 Color: 3

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 365733 Color: 4
Size: 340099 Color: 2
Size: 294169 Color: 4

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 365652 Color: 1
Size: 324836 Color: 4
Size: 309513 Color: 2

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 365819 Color: 3
Size: 352303 Color: 2
Size: 281879 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 366010 Color: 0
Size: 332334 Color: 3
Size: 301657 Color: 2

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 365881 Color: 4
Size: 335215 Color: 1
Size: 298905 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 365989 Color: 4
Size: 333959 Color: 1
Size: 300053 Color: 3

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 366207 Color: 3
Size: 362814 Color: 2
Size: 270980 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 366229 Color: 3
Size: 344490 Color: 2
Size: 289282 Color: 4

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 366292 Color: 1
Size: 350686 Color: 1
Size: 283023 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 366307 Color: 3
Size: 361721 Color: 4
Size: 271973 Color: 1

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 366121 Color: 0
Size: 356630 Color: 4
Size: 277250 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 366365 Color: 4
Size: 335354 Color: 3
Size: 298282 Color: 3

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 366369 Color: 1
Size: 319566 Color: 2
Size: 314066 Color: 2

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 366370 Color: 3
Size: 338635 Color: 0
Size: 294996 Color: 4

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 366568 Color: 0
Size: 320422 Color: 4
Size: 313011 Color: 4

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 366398 Color: 1
Size: 332253 Color: 2
Size: 301350 Color: 4

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 366477 Color: 3
Size: 354664 Color: 1
Size: 278860 Color: 1

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 366528 Color: 3
Size: 334507 Color: 2
Size: 298966 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 366659 Color: 1
Size: 360261 Color: 2
Size: 273081 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 366796 Color: 4
Size: 318809 Color: 1
Size: 314396 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 366766 Color: 0
Size: 363924 Color: 3
Size: 269311 Color: 3

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 366843 Color: 4
Size: 355403 Color: 0
Size: 277755 Color: 1

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 366866 Color: 0
Size: 358943 Color: 3
Size: 274192 Color: 2

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 366900 Color: 2
Size: 327692 Color: 2
Size: 305409 Color: 1

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 366928 Color: 3
Size: 318076 Color: 0
Size: 314997 Color: 1

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 367098 Color: 4
Size: 359622 Color: 0
Size: 273281 Color: 3

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 367100 Color: 3
Size: 322287 Color: 1
Size: 310614 Color: 3

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 367130 Color: 1
Size: 354458 Color: 2
Size: 278413 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 367167 Color: 2
Size: 329281 Color: 3
Size: 303553 Color: 3

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 367188 Color: 4
Size: 344919 Color: 0
Size: 287894 Color: 1

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 367239 Color: 3
Size: 363582 Color: 4
Size: 269180 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 367256 Color: 3
Size: 335011 Color: 1
Size: 297734 Color: 1

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 367291 Color: 4
Size: 322268 Color: 0
Size: 310442 Color: 1

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 367222 Color: 0
Size: 342044 Color: 2
Size: 290735 Color: 4

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 367310 Color: 4
Size: 331343 Color: 3
Size: 301348 Color: 2

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 367323 Color: 3
Size: 332366 Color: 1
Size: 300312 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 367491 Color: 0
Size: 317978 Color: 3
Size: 314532 Color: 2

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 367410 Color: 1
Size: 319270 Color: 0
Size: 313321 Color: 2

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 367512 Color: 2
Size: 356864 Color: 1
Size: 275625 Color: 4

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 367586 Color: 0
Size: 340462 Color: 1
Size: 291953 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 367665 Color: 0
Size: 336739 Color: 3
Size: 295597 Color: 4

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 367748 Color: 2
Size: 324664 Color: 3
Size: 307589 Color: 1

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 367759 Color: 1
Size: 327726 Color: 2
Size: 304516 Color: 3

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 367825 Color: 3
Size: 329231 Color: 2
Size: 302945 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 367932 Color: 0
Size: 325566 Color: 2
Size: 306503 Color: 3

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 367923 Color: 4
Size: 350473 Color: 3
Size: 281605 Color: 3

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 367937 Color: 1
Size: 336807 Color: 2
Size: 295257 Color: 3

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 368151 Color: 0
Size: 325615 Color: 4
Size: 306235 Color: 4

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 368033 Color: 4
Size: 342181 Color: 4
Size: 289787 Color: 2

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 368121 Color: 4
Size: 325126 Color: 4
Size: 306754 Color: 3

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 368297 Color: 0
Size: 335960 Color: 2
Size: 295744 Color: 1

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 368123 Color: 3
Size: 322300 Color: 2
Size: 309578 Color: 2

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 368159 Color: 2
Size: 355990 Color: 4
Size: 275852 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 368399 Color: 0
Size: 343921 Color: 3
Size: 287681 Color: 4

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 368194 Color: 4
Size: 316923 Color: 4
Size: 314884 Color: 3

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 368482 Color: 0
Size: 320823 Color: 2
Size: 310696 Color: 4

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 368320 Color: 4
Size: 352521 Color: 3
Size: 279160 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 368656 Color: 0
Size: 324843 Color: 1
Size: 306502 Color: 4

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 368377 Color: 2
Size: 364488 Color: 4
Size: 267136 Color: 1

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 368762 Color: 0
Size: 333961 Color: 3
Size: 297278 Color: 1

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 368820 Color: 0
Size: 328280 Color: 2
Size: 302901 Color: 2

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 368384 Color: 3
Size: 354711 Color: 2
Size: 276906 Color: 3

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 368665 Color: 3
Size: 328106 Color: 2
Size: 303230 Color: 3

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 368732 Color: 3
Size: 344194 Color: 1
Size: 287075 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 368805 Color: 3
Size: 330643 Color: 1
Size: 300553 Color: 2

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 369149 Color: 0
Size: 338898 Color: 1
Size: 291954 Color: 2

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 369026 Color: 3
Size: 364825 Color: 2
Size: 266150 Color: 2

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 369146 Color: 1
Size: 352886 Color: 0
Size: 277969 Color: 2

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 369163 Color: 3
Size: 319786 Color: 1
Size: 311052 Color: 3

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 369265 Color: 2
Size: 332687 Color: 0
Size: 298049 Color: 3

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 369382 Color: 0
Size: 351233 Color: 4
Size: 279386 Color: 1

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 369465 Color: 0
Size: 319302 Color: 2
Size: 311234 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 369302 Color: 1
Size: 326046 Color: 4
Size: 304653 Color: 3

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 369463 Color: 2
Size: 326679 Color: 4
Size: 303859 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 369475 Color: 0
Size: 328449 Color: 2
Size: 302077 Color: 3

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 369464 Color: 3
Size: 349812 Color: 2
Size: 280725 Color: 2

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 369476 Color: 3
Size: 359278 Color: 4
Size: 271247 Color: 1

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 369662 Color: 4
Size: 323619 Color: 0
Size: 306720 Color: 4

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 369783 Color: 4
Size: 369417 Color: 3
Size: 260801 Color: 3

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 369945 Color: 1
Size: 339972 Color: 4
Size: 290084 Color: 3

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 369815 Color: 3
Size: 329704 Color: 4
Size: 300482 Color: 3

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 369844 Color: 3
Size: 354197 Color: 4
Size: 275960 Color: 1

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 369950 Color: 3
Size: 327565 Color: 4
Size: 302486 Color: 2

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 369961 Color: 0
Size: 323582 Color: 1
Size: 306458 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 370265 Color: 2
Size: 325452 Color: 1
Size: 304284 Color: 3

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 370278 Color: 4
Size: 370041 Color: 0
Size: 259682 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 370092 Color: 1
Size: 345977 Color: 4
Size: 283932 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 370509 Color: 1
Size: 362805 Color: 3
Size: 266687 Color: 3

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 370620 Color: 2
Size: 351756 Color: 4
Size: 277625 Color: 0

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 370622 Color: 3
Size: 328636 Color: 4
Size: 300743 Color: 4

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 370680 Color: 3
Size: 329856 Color: 1
Size: 299465 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 370930 Color: 0
Size: 332333 Color: 2
Size: 296738 Color: 2

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 370824 Color: 4
Size: 325609 Color: 2
Size: 303568 Color: 4

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 370866 Color: 1
Size: 348509 Color: 0
Size: 280626 Color: 4

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 370944 Color: 2
Size: 347509 Color: 0
Size: 281548 Color: 1

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 371007 Color: 3
Size: 355529 Color: 4
Size: 273465 Color: 4

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 371099 Color: 1
Size: 353444 Color: 4
Size: 275458 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 371203 Color: 1
Size: 330934 Color: 0
Size: 297864 Color: 2

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 371209 Color: 4
Size: 352186 Color: 2
Size: 276606 Color: 3

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 371245 Color: 2
Size: 356557 Color: 3
Size: 272199 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 371258 Color: 4
Size: 314568 Color: 1
Size: 314175 Color: 1

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 371273 Color: 4
Size: 348023 Color: 0
Size: 280705 Color: 1

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 371401 Color: 4
Size: 315425 Color: 2
Size: 313175 Color: 3

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 371419 Color: 2
Size: 365730 Color: 4
Size: 262852 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 371437 Color: 3
Size: 335899 Color: 4
Size: 292665 Color: 3

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 371466 Color: 4
Size: 368286 Color: 0
Size: 260249 Color: 2

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 371627 Color: 0
Size: 342485 Color: 1
Size: 285889 Color: 3

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 371535 Color: 4
Size: 321462 Color: 2
Size: 307004 Color: 2

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 371804 Color: 0
Size: 332632 Color: 2
Size: 295565 Color: 4

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 371820 Color: 0
Size: 357187 Color: 2
Size: 270994 Color: 3

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 371591 Color: 1
Size: 348815 Color: 2
Size: 279595 Color: 4

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 371632 Color: 3
Size: 354655 Color: 0
Size: 273714 Color: 1

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 371884 Color: 0
Size: 344462 Color: 1
Size: 283655 Color: 1

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 371763 Color: 3
Size: 314378 Color: 4
Size: 313860 Color: 3

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 371809 Color: 3
Size: 351940 Color: 4
Size: 276252 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 371841 Color: 2
Size: 351409 Color: 4
Size: 276751 Color: 2

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 372006 Color: 1
Size: 358016 Color: 3
Size: 269979 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 372128 Color: 2
Size: 332498 Color: 1
Size: 295375 Color: 2

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 372074 Color: 0
Size: 320345 Color: 4
Size: 307582 Color: 4

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 372267 Color: 1
Size: 321709 Color: 3
Size: 306025 Color: 4

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 372338 Color: 2
Size: 333900 Color: 3
Size: 293763 Color: 3

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 372436 Color: 1
Size: 364503 Color: 0
Size: 263062 Color: 3

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 372402 Color: 0
Size: 352540 Color: 2
Size: 275059 Color: 1

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 372466 Color: 1
Size: 337235 Color: 2
Size: 290300 Color: 3

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 372467 Color: 2
Size: 366058 Color: 0
Size: 261476 Color: 3

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 372500 Color: 3
Size: 364050 Color: 4
Size: 263451 Color: 2

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 372543 Color: 4
Size: 356435 Color: 3
Size: 271023 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 372575 Color: 2
Size: 338545 Color: 4
Size: 288881 Color: 4

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 372669 Color: 2
Size: 321111 Color: 0
Size: 306221 Color: 1

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 372719 Color: 0
Size: 365209 Color: 2
Size: 262073 Color: 1

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 372677 Color: 2
Size: 329693 Color: 3
Size: 297631 Color: 4

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 372794 Color: 3
Size: 314545 Color: 1
Size: 312662 Color: 4

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 372799 Color: 1
Size: 362511 Color: 4
Size: 264691 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 372864 Color: 3
Size: 317476 Color: 0
Size: 309661 Color: 4

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 372913 Color: 2
Size: 324551 Color: 3
Size: 302537 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 373022 Color: 3
Size: 332243 Color: 1
Size: 294736 Color: 1

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 373059 Color: 2
Size: 352652 Color: 1
Size: 274290 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 373067 Color: 0
Size: 335589 Color: 1
Size: 291345 Color: 1

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 373082 Color: 2
Size: 355993 Color: 1
Size: 270926 Color: 1

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 373191 Color: 0
Size: 327837 Color: 1
Size: 298973 Color: 2

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 373086 Color: 2
Size: 320775 Color: 2
Size: 306140 Color: 3

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 373089 Color: 2
Size: 317143 Color: 1
Size: 309769 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 373198 Color: 0
Size: 318376 Color: 3
Size: 308427 Color: 4

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 373110 Color: 2
Size: 334540 Color: 3
Size: 292351 Color: 3

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 373224 Color: 1
Size: 364273 Color: 3
Size: 262504 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 373236 Color: 4
Size: 341262 Color: 1
Size: 285503 Color: 3

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 373247 Color: 1
Size: 330427 Color: 3
Size: 296327 Color: 4

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 373352 Color: 1
Size: 328148 Color: 3
Size: 298501 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 373395 Color: 2
Size: 327500 Color: 2
Size: 299106 Color: 4

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 373432 Color: 4
Size: 333515 Color: 1
Size: 293054 Color: 4

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 373557 Color: 0
Size: 344881 Color: 3
Size: 281563 Color: 3

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 373596 Color: 0
Size: 321926 Color: 2
Size: 304479 Color: 1

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 373574 Color: 2
Size: 320518 Color: 4
Size: 305909 Color: 4

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 373578 Color: 4
Size: 321795 Color: 0
Size: 304628 Color: 3

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 373651 Color: 0
Size: 363273 Color: 1
Size: 263077 Color: 3

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 373703 Color: 0
Size: 349752 Color: 1
Size: 276546 Color: 4

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 373649 Color: 1
Size: 339956 Color: 4
Size: 286396 Color: 2

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 373690 Color: 4
Size: 363378 Color: 2
Size: 262933 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 373890 Color: 0
Size: 341936 Color: 3
Size: 284175 Color: 1

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 373811 Color: 1
Size: 335792 Color: 2
Size: 290398 Color: 1

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 373828 Color: 3
Size: 323381 Color: 4
Size: 302792 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 373860 Color: 2
Size: 344115 Color: 2
Size: 282026 Color: 4

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 373881 Color: 3
Size: 343754 Color: 2
Size: 282366 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 373929 Color: 0
Size: 373434 Color: 1
Size: 252638 Color: 4

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 373941 Color: 1
Size: 313981 Color: 3
Size: 312079 Color: 1

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 373981 Color: 1
Size: 354657 Color: 2
Size: 271363 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 373990 Color: 2
Size: 314131 Color: 3
Size: 311880 Color: 4

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 374044 Color: 4
Size: 370540 Color: 4
Size: 255417 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 374093 Color: 2
Size: 372733 Color: 4
Size: 253175 Color: 2

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 374318 Color: 0
Size: 367653 Color: 3
Size: 258030 Color: 1

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 374165 Color: 1
Size: 314939 Color: 4
Size: 310897 Color: 4

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 374487 Color: 0
Size: 348905 Color: 4
Size: 276609 Color: 3

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 374268 Color: 4
Size: 334154 Color: 4
Size: 291579 Color: 1

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 374319 Color: 4
Size: 356223 Color: 0
Size: 269459 Color: 4

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 374504 Color: 2
Size: 352004 Color: 2
Size: 273493 Color: 4

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 374369 Color: 1
Size: 371476 Color: 3
Size: 254156 Color: 3

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 374464 Color: 4
Size: 344071 Color: 2
Size: 281466 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 374541 Color: 0
Size: 321104 Color: 4
Size: 304356 Color: 1

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 374504 Color: 0
Size: 325657 Color: 4
Size: 299840 Color: 3

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 374557 Color: 2
Size: 326337 Color: 4
Size: 299107 Color: 1

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 374660 Color: 1
Size: 359586 Color: 1
Size: 265755 Color: 2

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 374661 Color: 1
Size: 363152 Color: 0
Size: 262188 Color: 1

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 374753 Color: 1
Size: 346403 Color: 4
Size: 278845 Color: 3

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 374824 Color: 1
Size: 346637 Color: 4
Size: 278540 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 374789 Color: 0
Size: 351187 Color: 4
Size: 274025 Color: 2

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 374869 Color: 4
Size: 321195 Color: 3
Size: 303937 Color: 3

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 374875 Color: 4
Size: 337942 Color: 2
Size: 287184 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 374876 Color: 3
Size: 372468 Color: 1
Size: 252657 Color: 1

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 374888 Color: 2
Size: 351879 Color: 4
Size: 273234 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 374937 Color: 2
Size: 316443 Color: 4
Size: 308621 Color: 3

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 374965 Color: 3
Size: 329395 Color: 0
Size: 295641 Color: 2

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 374981 Color: 2
Size: 318328 Color: 3
Size: 306692 Color: 3

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 375011 Color: 2
Size: 349983 Color: 1
Size: 275007 Color: 1

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 375074 Color: 3
Size: 351328 Color: 0
Size: 273599 Color: 4

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 375104 Color: 1
Size: 341407 Color: 2
Size: 283490 Color: 1

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 375115 Color: 4
Size: 346488 Color: 3
Size: 278398 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 375199 Color: 1
Size: 348929 Color: 0
Size: 275873 Color: 4

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 375244 Color: 0
Size: 314533 Color: 4
Size: 310224 Color: 1

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 375255 Color: 2
Size: 352383 Color: 0
Size: 272363 Color: 1

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 375244 Color: 4
Size: 356175 Color: 4
Size: 268582 Color: 1

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 375259 Color: 4
Size: 328315 Color: 1
Size: 296427 Color: 1

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 375264 Color: 4
Size: 372518 Color: 2
Size: 252219 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 375283 Color: 1
Size: 351119 Color: 0
Size: 273599 Color: 3

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 375290 Color: 0
Size: 341624 Color: 2
Size: 283087 Color: 2

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 375288 Color: 1
Size: 363272 Color: 3
Size: 261441 Color: 2

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 375294 Color: 1
Size: 356753 Color: 3
Size: 267954 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 375384 Color: 3
Size: 371749 Color: 1
Size: 252868 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 375398 Color: 1
Size: 330051 Color: 4
Size: 294552 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 375458 Color: 3
Size: 365286 Color: 4
Size: 259257 Color: 2

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 375480 Color: 2
Size: 331886 Color: 3
Size: 292635 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 375516 Color: 1
Size: 349245 Color: 0
Size: 275240 Color: 3

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 375521 Color: 4
Size: 321127 Color: 3
Size: 303353 Color: 3

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 375584 Color: 1
Size: 318083 Color: 4
Size: 306334 Color: 4

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 375628 Color: 3
Size: 371136 Color: 3
Size: 253237 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 375622 Color: 0
Size: 346344 Color: 1
Size: 278035 Color: 4

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 375643 Color: 4
Size: 359895 Color: 2
Size: 264463 Color: 2

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 375674 Color: 2
Size: 316834 Color: 0
Size: 307493 Color: 3

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 375731 Color: 2
Size: 360189 Color: 0
Size: 264081 Color: 3

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 375740 Color: 1
Size: 331869 Color: 0
Size: 292392 Color: 1

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 375842 Color: 0
Size: 348572 Color: 3
Size: 275587 Color: 2

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 375874 Color: 1
Size: 354575 Color: 2
Size: 269552 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 375910 Color: 4
Size: 371033 Color: 2
Size: 253058 Color: 2

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 375899 Color: 0
Size: 353480 Color: 2
Size: 270622 Color: 4

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 375951 Color: 4
Size: 347189 Color: 1
Size: 276861 Color: 4

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 375966 Color: 3
Size: 373486 Color: 3
Size: 250549 Color: 2

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 375989 Color: 1
Size: 321036 Color: 2
Size: 302976 Color: 1

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 376011 Color: 3
Size: 360276 Color: 0
Size: 263714 Color: 2

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 376032 Color: 2
Size: 345620 Color: 4
Size: 278349 Color: 1

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 376120 Color: 1
Size: 341344 Color: 2
Size: 282537 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 376171 Color: 3
Size: 355481 Color: 1
Size: 268349 Color: 3

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 376265 Color: 2
Size: 346486 Color: 4
Size: 277250 Color: 2

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 376340 Color: 2
Size: 325189 Color: 3
Size: 298472 Color: 4

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 376375 Color: 3
Size: 332395 Color: 0
Size: 291231 Color: 1

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 376393 Color: 2
Size: 337778 Color: 2
Size: 285830 Color: 3

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 376401 Color: 2
Size: 319528 Color: 0
Size: 304072 Color: 1

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 376406 Color: 3
Size: 363949 Color: 3
Size: 259646 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 376447 Color: 3
Size: 358171 Color: 4
Size: 265383 Color: 2

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 376458 Color: 2
Size: 334750 Color: 0
Size: 288793 Color: 4

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 376602 Color: 3
Size: 328317 Color: 4
Size: 295082 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 376635 Color: 0
Size: 314492 Color: 1
Size: 308874 Color: 4

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 376650 Color: 4
Size: 353250 Color: 0
Size: 270101 Color: 3

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 376673 Color: 1
Size: 327872 Color: 1
Size: 295456 Color: 3

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 376678 Color: 4
Size: 327226 Color: 0
Size: 296097 Color: 4

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 376749 Color: 0
Size: 322779 Color: 4
Size: 300473 Color: 1

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 376755 Color: 3
Size: 325816 Color: 0
Size: 297430 Color: 2

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 376769 Color: 4
Size: 313147 Color: 1
Size: 310085 Color: 4

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 376793 Color: 1
Size: 321552 Color: 3
Size: 301656 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 376876 Color: 4
Size: 334937 Color: 2
Size: 288188 Color: 0

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 376928 Color: 1
Size: 337134 Color: 2
Size: 285939 Color: 4

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 376963 Color: 1
Size: 370209 Color: 2
Size: 252829 Color: 2

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 376997 Color: 3
Size: 323094 Color: 4
Size: 299910 Color: 4

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 377094 Color: 3
Size: 321583 Color: 0
Size: 301324 Color: 1

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 377151 Color: 2
Size: 329990 Color: 2
Size: 292860 Color: 0

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 377168 Color: 3
Size: 356196 Color: 4
Size: 266637 Color: 4

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 377197 Color: 2
Size: 355737 Color: 0
Size: 267067 Color: 2

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 377278 Color: 4
Size: 365430 Color: 3
Size: 257293 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 377326 Color: 0
Size: 367992 Color: 2
Size: 254683 Color: 2

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 377348 Color: 2
Size: 359744 Color: 2
Size: 262909 Color: 3

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 377410 Color: 2
Size: 370002 Color: 3
Size: 252589 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 377541 Color: 0
Size: 332804 Color: 3
Size: 289656 Color: 1

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 377500 Color: 3
Size: 327811 Color: 2
Size: 294690 Color: 4

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 377520 Color: 4
Size: 347776 Color: 0
Size: 274705 Color: 4

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 377576 Color: 3
Size: 312699 Color: 2
Size: 309726 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 377636 Color: 0
Size: 367723 Color: 3
Size: 254642 Color: 2

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 377615 Color: 1
Size: 322763 Color: 2
Size: 299623 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 377658 Color: 0
Size: 364250 Color: 4
Size: 258093 Color: 4

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 377635 Color: 4
Size: 346275 Color: 2
Size: 276091 Color: 2

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 377724 Color: 2
Size: 332713 Color: 4
Size: 289564 Color: 1

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 377739 Color: 3
Size: 335906 Color: 0
Size: 286356 Color: 1

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 377874 Color: 3
Size: 356081 Color: 2
Size: 266046 Color: 4

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 378022 Color: 4
Size: 349170 Color: 1
Size: 272809 Color: 3

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 378022 Color: 2
Size: 343724 Color: 0
Size: 278255 Color: 1

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 378103 Color: 4
Size: 315779 Color: 0
Size: 306119 Color: 1

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 378143 Color: 2
Size: 314075 Color: 4
Size: 307783 Color: 1

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 378182 Color: 2
Size: 325329 Color: 0
Size: 296490 Color: 3

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 378227 Color: 1
Size: 351280 Color: 4
Size: 270494 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 378297 Color: 3
Size: 354515 Color: 2
Size: 267189 Color: 4

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 378449 Color: 2
Size: 339759 Color: 4
Size: 281793 Color: 4

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 378450 Color: 4
Size: 367116 Color: 0
Size: 254435 Color: 3

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 378519 Color: 3
Size: 325426 Color: 4
Size: 296056 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 378519 Color: 4
Size: 316670 Color: 1
Size: 304812 Color: 4

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 378526 Color: 1
Size: 345475 Color: 4
Size: 276000 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 378545 Color: 4
Size: 370726 Color: 1
Size: 250730 Color: 1

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 378532 Color: 0
Size: 355747 Color: 4
Size: 265722 Color: 3

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 378555 Color: 4
Size: 366381 Color: 3
Size: 255065 Color: 2

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 378564 Color: 1
Size: 329251 Color: 4
Size: 292186 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 378602 Color: 0
Size: 324431 Color: 1
Size: 296968 Color: 1

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 378576 Color: 2
Size: 314565 Color: 2
Size: 306860 Color: 3

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 378598 Color: 1
Size: 358078 Color: 0
Size: 263325 Color: 4

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 378608 Color: 3
Size: 319785 Color: 2
Size: 301608 Color: 4

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 378709 Color: 0
Size: 345733 Color: 2
Size: 275559 Color: 2

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 378690 Color: 4
Size: 350817 Color: 1
Size: 270494 Color: 3

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 378728 Color: 1
Size: 341077 Color: 4
Size: 280196 Color: 1

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 378737 Color: 2
Size: 318176 Color: 0
Size: 303088 Color: 1

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 378847 Color: 0
Size: 316373 Color: 1
Size: 304781 Color: 2

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 378785 Color: 4
Size: 313060 Color: 3
Size: 308156 Color: 2

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 378825 Color: 1
Size: 353669 Color: 2
Size: 267507 Color: 3

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 378859 Color: 2
Size: 313438 Color: 2
Size: 307704 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 378884 Color: 1
Size: 319625 Color: 3
Size: 301492 Color: 3

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 378964 Color: 1
Size: 368785 Color: 3
Size: 252252 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 378959 Color: 0
Size: 362783 Color: 3
Size: 258259 Color: 4

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 379145 Color: 1
Size: 335745 Color: 1
Size: 285111 Color: 2

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 379217 Color: 3
Size: 357573 Color: 4
Size: 263211 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 379292 Color: 1
Size: 335489 Color: 1
Size: 285220 Color: 2

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 379319 Color: 1
Size: 311564 Color: 0
Size: 309118 Color: 2

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 379403 Color: 4
Size: 322320 Color: 2
Size: 298278 Color: 2

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 379418 Color: 0
Size: 345870 Color: 4
Size: 274713 Color: 2

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 379485 Color: 4
Size: 321923 Color: 4
Size: 298593 Color: 2

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 379504 Color: 4
Size: 331889 Color: 1
Size: 288608 Color: 2

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 379595 Color: 2
Size: 312964 Color: 1
Size: 307442 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 379582 Color: 0
Size: 336879 Color: 2
Size: 283540 Color: 2

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 379615 Color: 2
Size: 361443 Color: 3
Size: 258943 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 379638 Color: 3
Size: 353987 Color: 4
Size: 266376 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 379665 Color: 4
Size: 370323 Color: 0
Size: 250013 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 379666 Color: 1
Size: 355642 Color: 3
Size: 264693 Color: 2

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 379696 Color: 1
Size: 331962 Color: 0
Size: 288343 Color: 1

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 379752 Color: 0
Size: 322123 Color: 3
Size: 298126 Color: 2

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 379749 Color: 2
Size: 334945 Color: 0
Size: 285307 Color: 1

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 379783 Color: 1
Size: 341061 Color: 2
Size: 279157 Color: 2

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 379813 Color: 1
Size: 368382 Color: 0
Size: 251806 Color: 2

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 379817 Color: 0
Size: 314617 Color: 1
Size: 305567 Color: 4

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 379858 Color: 2
Size: 348907 Color: 3
Size: 271236 Color: 1

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 379864 Color: 1
Size: 346079 Color: 0
Size: 274058 Color: 1

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 379966 Color: 4
Size: 327593 Color: 1
Size: 292442 Color: 1

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 380128 Color: 4
Size: 364947 Color: 4
Size: 254926 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 380212 Color: 2
Size: 345125 Color: 0
Size: 274664 Color: 3

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 380206 Color: 0
Size: 355809 Color: 1
Size: 263986 Color: 4

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 380336 Color: 2
Size: 361150 Color: 0
Size: 258515 Color: 1

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 380373 Color: 3
Size: 367938 Color: 3
Size: 251690 Color: 2

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 380567 Color: 1
Size: 345283 Color: 0
Size: 274151 Color: 3

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 380567 Color: 3
Size: 345130 Color: 4
Size: 274304 Color: 1

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 380668 Color: 0
Size: 361251 Color: 1
Size: 258082 Color: 3

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 380570 Color: 2
Size: 327700 Color: 1
Size: 291731 Color: 4

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 380962 Color: 1
Size: 314739 Color: 4
Size: 304300 Color: 1

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 380977 Color: 3
Size: 349069 Color: 4
Size: 269955 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 381020 Color: 3
Size: 367446 Color: 1
Size: 251535 Color: 1

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 381036 Color: 4
Size: 322905 Color: 2
Size: 296060 Color: 2

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 381216 Color: 2
Size: 336292 Color: 3
Size: 282493 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 381213 Color: 0
Size: 357644 Color: 3
Size: 261144 Color: 1

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 381425 Color: 2
Size: 310898 Color: 4
Size: 307678 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 381428 Color: 1
Size: 335601 Color: 0
Size: 282972 Color: 2

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 381455 Color: 3
Size: 362482 Color: 1
Size: 256064 Color: 3

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 381460 Color: 1
Size: 353038 Color: 3
Size: 265503 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 381546 Color: 4
Size: 346206 Color: 2
Size: 272249 Color: 1

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 381558 Color: 4
Size: 325609 Color: 4
Size: 292834 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 381573 Color: 2
Size: 362975 Color: 0
Size: 255453 Color: 3

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 381620 Color: 3
Size: 320838 Color: 2
Size: 297543 Color: 1

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 381649 Color: 2
Size: 343913 Color: 4
Size: 274439 Color: 4

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 381656 Color: 1
Size: 313954 Color: 0
Size: 304391 Color: 2

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 381664 Color: 4
Size: 344835 Color: 1
Size: 273502 Color: 2

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 381786 Color: 4
Size: 327490 Color: 0
Size: 290725 Color: 4

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 381799 Color: 4
Size: 352225 Color: 3
Size: 265977 Color: 2

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 381854 Color: 4
Size: 324930 Color: 0
Size: 293217 Color: 3

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 381920 Color: 1
Size: 339887 Color: 3
Size: 278194 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 381991 Color: 2
Size: 330927 Color: 0
Size: 287083 Color: 3

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 382014 Color: 2
Size: 358784 Color: 0
Size: 259203 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 382031 Color: 3
Size: 329662 Color: 0
Size: 288308 Color: 1

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 382114 Color: 1
Size: 361461 Color: 3
Size: 256426 Color: 4

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 382222 Color: 2
Size: 318666 Color: 0
Size: 299113 Color: 4

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 382260 Color: 2
Size: 323132 Color: 1
Size: 294609 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 382380 Color: 4
Size: 365081 Color: 0
Size: 252540 Color: 2

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 382580 Color: 3
Size: 341105 Color: 2
Size: 276316 Color: 1

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 382597 Color: 1
Size: 311132 Color: 0
Size: 306272 Color: 3

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 382653 Color: 1
Size: 337757 Color: 0
Size: 279591 Color: 3

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 382683 Color: 4
Size: 310338 Color: 3
Size: 306980 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 382687 Color: 4
Size: 350465 Color: 4
Size: 266849 Color: 2

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 382738 Color: 3
Size: 323615 Color: 2
Size: 293648 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 382921 Color: 0
Size: 356923 Color: 2
Size: 260157 Color: 4

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 382810 Color: 1
Size: 309520 Color: 4
Size: 307671 Color: 4

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 382944 Color: 0
Size: 331922 Color: 3
Size: 285135 Color: 3

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 382828 Color: 3
Size: 329923 Color: 4
Size: 287250 Color: 4

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 382845 Color: 2
Size: 311473 Color: 0
Size: 305683 Color: 1

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 383065 Color: 3
Size: 327620 Color: 1
Size: 289316 Color: 2

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 383013 Color: 0
Size: 361834 Color: 2
Size: 255154 Color: 4

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 383211 Color: 4
Size: 314217 Color: 0
Size: 302573 Color: 1

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 383212 Color: 1
Size: 318257 Color: 4
Size: 298532 Color: 2

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 383218 Color: 3
Size: 356732 Color: 4
Size: 260051 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 383266 Color: 0
Size: 317980 Color: 4
Size: 298755 Color: 4

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 383274 Color: 1
Size: 366039 Color: 4
Size: 250688 Color: 2

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 383490 Color: 0
Size: 328154 Color: 3
Size: 288357 Color: 2

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 383298 Color: 3
Size: 364925 Color: 4
Size: 251778 Color: 2

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 383586 Color: 0
Size: 345022 Color: 4
Size: 271393 Color: 4

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 383575 Color: 2
Size: 354772 Color: 3
Size: 261654 Color: 3

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 383651 Color: 4
Size: 312621 Color: 4
Size: 303729 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 383611 Color: 0
Size: 332385 Color: 4
Size: 284005 Color: 2

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 383800 Color: 1
Size: 347812 Color: 3
Size: 268389 Color: 3

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 383831 Color: 1
Size: 350364 Color: 1
Size: 265806 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 383879 Color: 3
Size: 326935 Color: 4
Size: 289187 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 383885 Color: 3
Size: 339790 Color: 2
Size: 276326 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 383887 Color: 2
Size: 356073 Color: 4
Size: 260041 Color: 4

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 383920 Color: 4
Size: 325606 Color: 0
Size: 290475 Color: 2

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 383955 Color: 1
Size: 338374 Color: 3
Size: 277672 Color: 2

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 383960 Color: 4
Size: 326847 Color: 3
Size: 289194 Color: 2

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 384028 Color: 1
Size: 325949 Color: 3
Size: 290024 Color: 3

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 384070 Color: 4
Size: 336800 Color: 4
Size: 279131 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 384096 Color: 2
Size: 350293 Color: 3
Size: 265612 Color: 3

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 384125 Color: 2
Size: 352459 Color: 0
Size: 263417 Color: 3

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 384212 Color: 4
Size: 313223 Color: 1
Size: 302566 Color: 4

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 384225 Color: 1
Size: 365324 Color: 0
Size: 250452 Color: 3

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 384236 Color: 4
Size: 354832 Color: 2
Size: 260933 Color: 2

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 384336 Color: 4
Size: 361468 Color: 2
Size: 254197 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 384337 Color: 1
Size: 320817 Color: 1
Size: 294847 Color: 2

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 384408 Color: 3
Size: 332041 Color: 4
Size: 283552 Color: 3

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 384479 Color: 1
Size: 316805 Color: 1
Size: 298717 Color: 3

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 384576 Color: 0
Size: 343431 Color: 1
Size: 271994 Color: 3

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 384647 Color: 1
Size: 319739 Color: 0
Size: 295615 Color: 3

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 384742 Color: 0
Size: 362642 Color: 4
Size: 252617 Color: 3

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 384653 Color: 3
Size: 338576 Color: 1
Size: 276772 Color: 3

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 384751 Color: 0
Size: 323548 Color: 3
Size: 291702 Color: 1

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 384673 Color: 2
Size: 309686 Color: 4
Size: 305642 Color: 1

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 384751 Color: 0
Size: 357984 Color: 2
Size: 257266 Color: 4

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 384706 Color: 4
Size: 332467 Color: 3
Size: 282828 Color: 3

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 384716 Color: 2
Size: 313340 Color: 0
Size: 301945 Color: 1

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 384932 Color: 0
Size: 332298 Color: 4
Size: 282771 Color: 2

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 384788 Color: 4
Size: 347738 Color: 2
Size: 267475 Color: 4

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 384861 Color: 2
Size: 338022 Color: 4
Size: 277118 Color: 4

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 384923 Color: 4
Size: 355431 Color: 0
Size: 259647 Color: 2

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 385218 Color: 3
Size: 308561 Color: 0
Size: 306222 Color: 1

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 385169 Color: 0
Size: 311786 Color: 2
Size: 303046 Color: 3

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 385339 Color: 0
Size: 318237 Color: 4
Size: 296425 Color: 3

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 385247 Color: 3
Size: 309528 Color: 4
Size: 305226 Color: 1

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 385357 Color: 4
Size: 308783 Color: 0
Size: 305861 Color: 3

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 385401 Color: 0
Size: 334786 Color: 3
Size: 279814 Color: 1

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 385473 Color: 4
Size: 349369 Color: 3
Size: 265159 Color: 3

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 385653 Color: 3
Size: 363532 Color: 0
Size: 250816 Color: 1

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 385674 Color: 2
Size: 314820 Color: 4
Size: 299507 Color: 4

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 385678 Color: 4
Size: 317029 Color: 4
Size: 297294 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 385735 Color: 4
Size: 355906 Color: 3
Size: 258360 Color: 2

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 385753 Color: 3
Size: 312925 Color: 3
Size: 301323 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 385745 Color: 0
Size: 357254 Color: 3
Size: 257002 Color: 3

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 385775 Color: 1
Size: 360906 Color: 4
Size: 253320 Color: 2

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 385786 Color: 1
Size: 343620 Color: 3
Size: 270595 Color: 2

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 385963 Color: 0
Size: 353101 Color: 4
Size: 260937 Color: 3

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 385986 Color: 0
Size: 363717 Color: 2
Size: 250298 Color: 2

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 385805 Color: 2
Size: 349387 Color: 1
Size: 264809 Color: 4

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 386049 Color: 0
Size: 344213 Color: 4
Size: 269739 Color: 1

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 385970 Color: 2
Size: 352364 Color: 3
Size: 261667 Color: 4

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 386148 Color: 4
Size: 327517 Color: 1
Size: 286336 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 386244 Color: 4
Size: 329019 Color: 0
Size: 284738 Color: 4

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 386249 Color: 2
Size: 351912 Color: 4
Size: 261840 Color: 3

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 386299 Color: 2
Size: 341117 Color: 0
Size: 272585 Color: 1

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 386306 Color: 1
Size: 357714 Color: 2
Size: 255981 Color: 1

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 386357 Color: 1
Size: 331617 Color: 0
Size: 282027 Color: 2

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 386378 Color: 4
Size: 333467 Color: 3
Size: 280156 Color: 2

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 386411 Color: 3
Size: 317502 Color: 0
Size: 296088 Color: 1

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 386415 Color: 0
Size: 331250 Color: 4
Size: 282336 Color: 2

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 386411 Color: 3
Size: 321600 Color: 2
Size: 291990 Color: 2

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 386493 Color: 3
Size: 313216 Color: 1
Size: 300292 Color: 1

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 386582 Color: 4
Size: 311306 Color: 3
Size: 302113 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 386689 Color: 3
Size: 308657 Color: 4
Size: 304655 Color: 4

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 386709 Color: 0
Size: 322449 Color: 1
Size: 290843 Color: 3

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 386897 Color: 0
Size: 349115 Color: 2
Size: 263989 Color: 1

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 386798 Color: 2
Size: 338617 Color: 2
Size: 274586 Color: 4

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 386860 Color: 1
Size: 343419 Color: 0
Size: 269722 Color: 1

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 386985 Color: 0
Size: 308589 Color: 1
Size: 304427 Color: 3

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 386900 Color: 3
Size: 323172 Color: 4
Size: 289929 Color: 3

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 386915 Color: 1
Size: 311100 Color: 3
Size: 301986 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 387075 Color: 2
Size: 349551 Color: 0
Size: 263375 Color: 1

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 387122 Color: 4
Size: 338027 Color: 0
Size: 274852 Color: 4

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 387145 Color: 1
Size: 315056 Color: 2
Size: 297800 Color: 1

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 387161 Color: 3
Size: 338026 Color: 1
Size: 274814 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 387369 Color: 0
Size: 362466 Color: 1
Size: 250166 Color: 4

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 387303 Color: 3
Size: 312124 Color: 4
Size: 300574 Color: 3

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 387457 Color: 0
Size: 338418 Color: 1
Size: 274126 Color: 3

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 387384 Color: 3
Size: 307776 Color: 0
Size: 304841 Color: 1

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 387528 Color: 2
Size: 351410 Color: 0
Size: 261063 Color: 3

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 387537 Color: 4
Size: 320459 Color: 4
Size: 292005 Color: 3

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 387596 Color: 2
Size: 351773 Color: 0
Size: 260632 Color: 4

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 387648 Color: 1
Size: 344161 Color: 3
Size: 268192 Color: 2

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 387727 Color: 2
Size: 357834 Color: 4
Size: 254440 Color: 0

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 387811 Color: 4
Size: 314881 Color: 2
Size: 297309 Color: 0

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 387961 Color: 0
Size: 325009 Color: 3
Size: 287031 Color: 4

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 387887 Color: 3
Size: 344136 Color: 3
Size: 267978 Color: 2

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 388011 Color: 0
Size: 333545 Color: 4
Size: 278445 Color: 2

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 388160 Color: 0
Size: 314702 Color: 2
Size: 297139 Color: 1

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 388053 Color: 3
Size: 359976 Color: 4
Size: 251972 Color: 3

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 388122 Color: 1
Size: 315076 Color: 2
Size: 296803 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 388164 Color: 3
Size: 337865 Color: 0
Size: 273972 Color: 4

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 388212 Color: 1
Size: 314388 Color: 0
Size: 297401 Color: 3

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 388228 Color: 3
Size: 338797 Color: 2
Size: 272976 Color: 1

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 388282 Color: 4
Size: 306625 Color: 1
Size: 305094 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 388311 Color: 2
Size: 318471 Color: 0
Size: 293219 Color: 4

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 388391 Color: 4
Size: 333111 Color: 0
Size: 278499 Color: 1

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 388410 Color: 4
Size: 342223 Color: 3
Size: 269368 Color: 3

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 388550 Color: 0
Size: 307700 Color: 2
Size: 303751 Color: 2

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 388575 Color: 0
Size: 341570 Color: 4
Size: 269856 Color: 4

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 388546 Color: 4
Size: 317376 Color: 0
Size: 294079 Color: 1

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 388558 Color: 4
Size: 347034 Color: 4
Size: 264409 Color: 2

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 388647 Color: 2
Size: 347586 Color: 0
Size: 263768 Color: 1

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 388676 Color: 4
Size: 321841 Color: 3
Size: 289484 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 388754 Color: 4
Size: 323974 Color: 1
Size: 287273 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 388770 Color: 2
Size: 320326 Color: 4
Size: 290905 Color: 3

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 388877 Color: 4
Size: 319569 Color: 0
Size: 291555 Color: 2

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 388877 Color: 4
Size: 356608 Color: 3
Size: 254516 Color: 1

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 388890 Color: 4
Size: 339832 Color: 0
Size: 271279 Color: 2

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 389077 Color: 0
Size: 330131 Color: 4
Size: 280793 Color: 4

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 389155 Color: 0
Size: 338644 Color: 4
Size: 272202 Color: 4

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 388996 Color: 2
Size: 337187 Color: 1
Size: 273818 Color: 3

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 389227 Color: 0
Size: 308873 Color: 3
Size: 301901 Color: 2

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 389238 Color: 0
Size: 350195 Color: 4
Size: 260568 Color: 4

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 389325 Color: 0
Size: 345659 Color: 3
Size: 265017 Color: 2

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 389330 Color: 3
Size: 324024 Color: 1
Size: 286647 Color: 4

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 389474 Color: 3
Size: 336347 Color: 4
Size: 274180 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 389413 Color: 4
Size: 328738 Color: 3
Size: 281850 Color: 3

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 389629 Color: 4
Size: 328062 Color: 2
Size: 282310 Color: 3

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 389520 Color: 2
Size: 334752 Color: 3
Size: 275729 Color: 2

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 389582 Color: 2
Size: 319056 Color: 4
Size: 291363 Color: 3

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 389725 Color: 2
Size: 356983 Color: 1
Size: 253293 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 389786 Color: 3
Size: 343503 Color: 2
Size: 266712 Color: 2

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 389813 Color: 1
Size: 348426 Color: 0
Size: 261762 Color: 4

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 389838 Color: 2
Size: 338285 Color: 2
Size: 271878 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 389844 Color: 0
Size: 308654 Color: 2
Size: 301503 Color: 3

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 389907 Color: 2
Size: 327761 Color: 4
Size: 282333 Color: 3

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 389921 Color: 4
Size: 317239 Color: 2
Size: 292841 Color: 3

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 389922 Color: 1
Size: 343458 Color: 3
Size: 266621 Color: 1

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 389930 Color: 1
Size: 344224 Color: 4
Size: 265847 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 389968 Color: 2
Size: 316957 Color: 3
Size: 293076 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 390036 Color: 3
Size: 308785 Color: 0
Size: 301180 Color: 4

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 390116 Color: 2
Size: 342046 Color: 0
Size: 267839 Color: 1

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 390322 Color: 4
Size: 353600 Color: 3
Size: 256079 Color: 1

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 390383 Color: 2
Size: 312816 Color: 1
Size: 296802 Color: 2

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 390396 Color: 0
Size: 347204 Color: 3
Size: 262401 Color: 4

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 390453 Color: 0
Size: 341518 Color: 2
Size: 268030 Color: 1

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 390467 Color: 2
Size: 333183 Color: 3
Size: 276351 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 390470 Color: 1
Size: 309299 Color: 4
Size: 300232 Color: 2

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 390472 Color: 0
Size: 337762 Color: 3
Size: 271767 Color: 3

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 390583 Color: 4
Size: 345993 Color: 3
Size: 263425 Color: 2

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 390524 Color: 0
Size: 313211 Color: 3
Size: 296266 Color: 2

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 390618 Color: 0
Size: 332258 Color: 4
Size: 277125 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 390762 Color: 0
Size: 347656 Color: 0
Size: 261583 Color: 4

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 390618 Color: 2
Size: 340096 Color: 1
Size: 269287 Color: 1

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 390651 Color: 4
Size: 314126 Color: 2
Size: 295224 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 390771 Color: 3
Size: 345295 Color: 0
Size: 263935 Color: 2

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 390739 Color: 4
Size: 331077 Color: 3
Size: 278185 Color: 1

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 390906 Color: 2
Size: 323745 Color: 1
Size: 285350 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 390908 Color: 3
Size: 331087 Color: 2
Size: 278006 Color: 0

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 390919 Color: 4
Size: 356161 Color: 3
Size: 252921 Color: 3

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 391024 Color: 1
Size: 324000 Color: 0
Size: 284977 Color: 4

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 391088 Color: 3
Size: 330763 Color: 1
Size: 278150 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 391274 Color: 0
Size: 328590 Color: 2
Size: 280137 Color: 2

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 391331 Color: 0
Size: 343938 Color: 2
Size: 264732 Color: 3

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 391243 Color: 4
Size: 306638 Color: 2
Size: 302120 Color: 3

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 391270 Color: 1
Size: 334323 Color: 4
Size: 274408 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 391349 Color: 2
Size: 311413 Color: 0
Size: 297239 Color: 1

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 391414 Color: 1
Size: 356617 Color: 3
Size: 251970 Color: 1

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 391424 Color: 3
Size: 343583 Color: 1
Size: 264994 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 391517 Color: 0
Size: 323733 Color: 1
Size: 284751 Color: 4

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 391430 Color: 2
Size: 326392 Color: 3
Size: 282179 Color: 1

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 391585 Color: 0
Size: 327947 Color: 3
Size: 280469 Color: 4

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 391640 Color: 0
Size: 330742 Color: 4
Size: 277619 Color: 3

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 391592 Color: 2
Size: 344849 Color: 1
Size: 263560 Color: 4

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 391668 Color: 2
Size: 340919 Color: 2
Size: 267414 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 391709 Color: 1
Size: 309538 Color: 3
Size: 298754 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 391726 Color: 4
Size: 338323 Color: 1
Size: 269952 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 391763 Color: 3
Size: 307230 Color: 0
Size: 301008 Color: 3

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 391697 Color: 0
Size: 350692 Color: 3
Size: 257612 Color: 1

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 391799 Color: 1
Size: 318133 Color: 4
Size: 290069 Color: 2

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 391883 Color: 0
Size: 327414 Color: 4
Size: 280704 Color: 1

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 391820 Color: 3
Size: 346599 Color: 2
Size: 261582 Color: 3

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 391974 Color: 0
Size: 309234 Color: 2
Size: 298793 Color: 1

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 391881 Color: 2
Size: 315014 Color: 3
Size: 293106 Color: 2

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 392028 Color: 0
Size: 351917 Color: 3
Size: 256056 Color: 3

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 391922 Color: 1
Size: 341095 Color: 2
Size: 266984 Color: 1

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 392085 Color: 4
Size: 334601 Color: 0
Size: 273315 Color: 2

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 392117 Color: 2
Size: 331757 Color: 3
Size: 276127 Color: 3

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 392163 Color: 1
Size: 321400 Color: 3
Size: 286438 Color: 1

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 392351 Color: 0
Size: 332406 Color: 3
Size: 275244 Color: 4

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 392426 Color: 0
Size: 304559 Color: 2
Size: 303016 Color: 2

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 392241 Color: 4
Size: 354658 Color: 3
Size: 253102 Color: 1

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 392413 Color: 2
Size: 319612 Color: 1
Size: 287976 Color: 2

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 392541 Color: 0
Size: 326145 Color: 4
Size: 281315 Color: 4

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 392438 Color: 4
Size: 330208 Color: 3
Size: 277355 Color: 1

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 392447 Color: 1
Size: 351572 Color: 0
Size: 255982 Color: 1

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 392554 Color: 0
Size: 319390 Color: 1
Size: 288057 Color: 4

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 392519 Color: 3
Size: 356189 Color: 2
Size: 251293 Color: 1

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 392743 Color: 0
Size: 320917 Color: 3
Size: 286341 Color: 4

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 392573 Color: 1
Size: 343153 Color: 2
Size: 264275 Color: 3

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 392732 Color: 3
Size: 346090 Color: 0
Size: 261179 Color: 4

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 392829 Color: 0
Size: 336428 Color: 3
Size: 270744 Color: 3

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 392783 Color: 2
Size: 322099 Color: 4
Size: 285119 Color: 1

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 392899 Color: 2
Size: 346066 Color: 4
Size: 261036 Color: 1

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 392949 Color: 4
Size: 325161 Color: 0
Size: 281891 Color: 4

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 393075 Color: 3
Size: 338141 Color: 0
Size: 268785 Color: 1

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 393141 Color: 1
Size: 339099 Color: 3
Size: 267761 Color: 3

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 393148 Color: 4
Size: 315925 Color: 0
Size: 290928 Color: 3

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 393199 Color: 0
Size: 316843 Color: 1
Size: 289959 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 393162 Color: 1
Size: 343340 Color: 2
Size: 263499 Color: 4

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 393304 Color: 1
Size: 311496 Color: 1
Size: 295201 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 393394 Color: 1
Size: 338980 Color: 2
Size: 267627 Color: 2

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 393403 Color: 3
Size: 330861 Color: 2
Size: 275737 Color: 2

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 393484 Color: 1
Size: 310958 Color: 2
Size: 295559 Color: 2

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 393623 Color: 0
Size: 354302 Color: 3
Size: 252076 Color: 3

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 393493 Color: 3
Size: 350532 Color: 3
Size: 255976 Color: 4

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 393639 Color: 3
Size: 304528 Color: 3
Size: 301834 Color: 0

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 393670 Color: 2
Size: 329553 Color: 2
Size: 276778 Color: 4

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 393830 Color: 4
Size: 305697 Color: 2
Size: 300474 Color: 2

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 393831 Color: 3
Size: 326598 Color: 0
Size: 279572 Color: 2

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 393840 Color: 2
Size: 340602 Color: 2
Size: 265559 Color: 4

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 393889 Color: 4
Size: 316327 Color: 3
Size: 289785 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 393916 Color: 1
Size: 307123 Color: 4
Size: 298962 Color: 3

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 393927 Color: 2
Size: 342178 Color: 0
Size: 263896 Color: 1

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 393935 Color: 4
Size: 344336 Color: 1
Size: 261730 Color: 3

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 393990 Color: 1
Size: 341400 Color: 0
Size: 264611 Color: 3

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 393989 Color: 0
Size: 339358 Color: 1
Size: 266654 Color: 4

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 394159 Color: 1
Size: 308822 Color: 4
Size: 297020 Color: 2

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 394162 Color: 2
Size: 321944 Color: 0
Size: 283895 Color: 4

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 394181 Color: 4
Size: 353859 Color: 1
Size: 251961 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 394217 Color: 2
Size: 313133 Color: 4
Size: 292651 Color: 4

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 394345 Color: 2
Size: 335773 Color: 0
Size: 269883 Color: 1

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 394418 Color: 0
Size: 338960 Color: 2
Size: 266623 Color: 2

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 394399 Color: 4
Size: 323383 Color: 2
Size: 282219 Color: 1

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 394399 Color: 4
Size: 314208 Color: 1
Size: 291394 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 394402 Color: 2
Size: 310426 Color: 1
Size: 295173 Color: 3

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 394413 Color: 2
Size: 346208 Color: 0
Size: 259380 Color: 1

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 394442 Color: 2
Size: 329066 Color: 0
Size: 276493 Color: 4

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 394470 Color: 2
Size: 349603 Color: 3
Size: 255928 Color: 4

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 394499 Color: 1
Size: 340864 Color: 3
Size: 264638 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 394640 Color: 4
Size: 352532 Color: 4
Size: 252829 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 394798 Color: 1
Size: 309104 Color: 0
Size: 296099 Color: 4

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 394844 Color: 1
Size: 325987 Color: 2
Size: 279170 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 394847 Color: 3
Size: 311668 Color: 1
Size: 293486 Color: 4

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 394978 Color: 3
Size: 340159 Color: 2
Size: 264864 Color: 2

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 395060 Color: 3
Size: 302542 Color: 1
Size: 302399 Color: 0

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 395064 Color: 3
Size: 309752 Color: 2
Size: 295185 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 395078 Color: 1
Size: 350563 Color: 2
Size: 254360 Color: 3

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 395138 Color: 4
Size: 317113 Color: 2
Size: 287750 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 395241 Color: 2
Size: 326112 Color: 1
Size: 278648 Color: 4

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 395320 Color: 0
Size: 327446 Color: 1
Size: 277235 Color: 1

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 395326 Color: 3
Size: 309197 Color: 2
Size: 295478 Color: 2

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 395443 Color: 0
Size: 347008 Color: 1
Size: 257550 Color: 2

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 395347 Color: 2
Size: 309353 Color: 1
Size: 295301 Color: 3

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 395494 Color: 2
Size: 317503 Color: 0
Size: 287004 Color: 3

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 395504 Color: 4
Size: 312193 Color: 1
Size: 292304 Color: 4

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 395522 Color: 4
Size: 326335 Color: 0
Size: 278144 Color: 3

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 395587 Color: 1
Size: 353981 Color: 0
Size: 250433 Color: 3

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 395649 Color: 2
Size: 309063 Color: 1
Size: 295289 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 395800 Color: 0
Size: 327855 Color: 4
Size: 276346 Color: 2

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 395712 Color: 4
Size: 353472 Color: 3
Size: 250817 Color: 4

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 395725 Color: 2
Size: 316157 Color: 4
Size: 288119 Color: 4

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 395823 Color: 3
Size: 337833 Color: 3
Size: 266345 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 395872 Color: 2
Size: 310627 Color: 3
Size: 293502 Color: 1

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 395872 Color: 2
Size: 327365 Color: 0
Size: 276764 Color: 3

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 395916 Color: 4
Size: 315046 Color: 2
Size: 289039 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 395944 Color: 4
Size: 303532 Color: 2
Size: 300525 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 395946 Color: 4
Size: 316311 Color: 2
Size: 287744 Color: 2

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 395958 Color: 1
Size: 342972 Color: 4
Size: 261071 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 396014 Color: 2
Size: 334339 Color: 4
Size: 269648 Color: 1

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 396035 Color: 4
Size: 330145 Color: 0
Size: 273821 Color: 1

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 396012 Color: 0
Size: 337698 Color: 3
Size: 266291 Color: 3

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 396139 Color: 3
Size: 338037 Color: 2
Size: 265825 Color: 2

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 396148 Color: 2
Size: 324786 Color: 4
Size: 279067 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 396214 Color: 0
Size: 331329 Color: 1
Size: 272458 Color: 4

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 396202 Color: 2
Size: 333761 Color: 0
Size: 270038 Color: 3

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 396245 Color: 3
Size: 314098 Color: 2
Size: 289658 Color: 1

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 396279 Color: 1
Size: 333479 Color: 0
Size: 270243 Color: 2

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 396316 Color: 0
Size: 332095 Color: 1
Size: 271590 Color: 2

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 396308 Color: 4
Size: 303181 Color: 1
Size: 300512 Color: 3

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 396345 Color: 1
Size: 307838 Color: 3
Size: 295818 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 396349 Color: 2
Size: 328644 Color: 2
Size: 275008 Color: 3

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 396522 Color: 0
Size: 327671 Color: 2
Size: 275808 Color: 2

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 396633 Color: 0
Size: 311640 Color: 3
Size: 291728 Color: 3

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 396696 Color: 4
Size: 336575 Color: 2
Size: 266730 Color: 1

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 396784 Color: 2
Size: 342090 Color: 0
Size: 261127 Color: 3

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 396857 Color: 2
Size: 311089 Color: 1
Size: 292055 Color: 3

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 396920 Color: 4
Size: 341665 Color: 2
Size: 261416 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 396998 Color: 1
Size: 334863 Color: 3
Size: 268140 Color: 3

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 397009 Color: 1
Size: 324663 Color: 1
Size: 278329 Color: 2

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 397176 Color: 0
Size: 305652 Color: 2
Size: 297173 Color: 2

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 397062 Color: 4
Size: 336345 Color: 4
Size: 266594 Color: 3

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 397094 Color: 4
Size: 339070 Color: 3
Size: 263837 Color: 4

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 397293 Color: 3
Size: 335898 Color: 0
Size: 266810 Color: 2

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 397414 Color: 0
Size: 324122 Color: 3
Size: 278465 Color: 1

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 397364 Color: 3
Size: 335619 Color: 3
Size: 267018 Color: 4

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 397443 Color: 3
Size: 311218 Color: 0
Size: 291340 Color: 2

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 397459 Color: 0
Size: 335733 Color: 4
Size: 266809 Color: 1

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 397476 Color: 4
Size: 322556 Color: 1
Size: 279969 Color: 1

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 397520 Color: 0
Size: 331449 Color: 3
Size: 271032 Color: 3

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 397547 Color: 3
Size: 342606 Color: 1
Size: 259848 Color: 3

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 397569 Color: 3
Size: 339677 Color: 0
Size: 262755 Color: 4

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 397608 Color: 1
Size: 317683 Color: 2
Size: 284710 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 397658 Color: 4
Size: 312449 Color: 3
Size: 289894 Color: 1

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 397753 Color: 0
Size: 320702 Color: 1
Size: 281546 Color: 2

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 397840 Color: 4
Size: 341107 Color: 0
Size: 261054 Color: 2

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 397898 Color: 1
Size: 335066 Color: 1
Size: 267037 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 397917 Color: 4
Size: 340923 Color: 3
Size: 261161 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 398083 Color: 0
Size: 318448 Color: 3
Size: 283470 Color: 2

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 398005 Color: 1
Size: 335781 Color: 3
Size: 266215 Color: 3

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 398122 Color: 2
Size: 311687 Color: 0
Size: 290192 Color: 2

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 398157 Color: 2
Size: 302641 Color: 1
Size: 299203 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 398280 Color: 3
Size: 340977 Color: 0
Size: 260744 Color: 4

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 398323 Color: 2
Size: 306973 Color: 3
Size: 294705 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 398349 Color: 0
Size: 322355 Color: 3
Size: 279297 Color: 1

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 398367 Color: 2
Size: 329186 Color: 2
Size: 272448 Color: 3

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 398568 Color: 0
Size: 302368 Color: 3
Size: 299065 Color: 2

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 398490 Color: 3
Size: 309567 Color: 4
Size: 291944 Color: 1

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 398508 Color: 2
Size: 305046 Color: 3
Size: 296447 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 398684 Color: 0
Size: 306946 Color: 1
Size: 294371 Color: 3

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 398593 Color: 4
Size: 314405 Color: 3
Size: 287003 Color: 2

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 398719 Color: 3
Size: 332253 Color: 1
Size: 269029 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 398819 Color: 4
Size: 307209 Color: 3
Size: 293973 Color: 3

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 398882 Color: 4
Size: 334324 Color: 0
Size: 266795 Color: 3

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 398885 Color: 3
Size: 319545 Color: 2
Size: 281571 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 398910 Color: 0
Size: 328402 Color: 3
Size: 272689 Color: 4

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 399018 Color: 3
Size: 332676 Color: 1
Size: 268307 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 399039 Color: 1
Size: 311798 Color: 0
Size: 289164 Color: 4

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 399061 Color: 3
Size: 346667 Color: 0
Size: 254273 Color: 1

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 399081 Color: 4
Size: 311954 Color: 0
Size: 288966 Color: 3

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 399099 Color: 3
Size: 304179 Color: 4
Size: 296723 Color: 1

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 399099 Color: 0
Size: 319205 Color: 2
Size: 281697 Color: 3

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 399101 Color: 2
Size: 310274 Color: 1
Size: 290626 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 399108 Color: 4
Size: 332041 Color: 4
Size: 268852 Color: 1

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 399202 Color: 2
Size: 323825 Color: 0
Size: 276974 Color: 4

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 399341 Color: 1
Size: 312594 Color: 0
Size: 288066 Color: 3

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 399372 Color: 2
Size: 326999 Color: 4
Size: 273630 Color: 2

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 399384 Color: 2
Size: 330387 Color: 3
Size: 270230 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 399375 Color: 0
Size: 323443 Color: 4
Size: 277183 Color: 2

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 399412 Color: 3
Size: 334221 Color: 2
Size: 266368 Color: 3

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 399472 Color: 0
Size: 316038 Color: 4
Size: 284491 Color: 2

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 399470 Color: 1
Size: 340266 Color: 3
Size: 260265 Color: 4

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 399477 Color: 3
Size: 325131 Color: 2
Size: 275393 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 399774 Color: 3
Size: 310687 Color: 2
Size: 289540 Color: 4

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 399838 Color: 3
Size: 305915 Color: 3
Size: 294248 Color: 4

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 399892 Color: 2
Size: 323572 Color: 0
Size: 276537 Color: 4

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 399990 Color: 3
Size: 335873 Color: 3
Size: 264138 Color: 1

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 399996 Color: 4
Size: 308468 Color: 2
Size: 291537 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 400071 Color: 2
Size: 343419 Color: 0
Size: 256511 Color: 3

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 400080 Color: 3
Size: 306082 Color: 3
Size: 293839 Color: 2

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 400164 Color: 1
Size: 345995 Color: 4
Size: 253842 Color: 1

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 400165 Color: 1
Size: 300677 Color: 0
Size: 299159 Color: 2

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 400217 Color: 3
Size: 325744 Color: 3
Size: 274040 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 400288 Color: 2
Size: 319314 Color: 2
Size: 280399 Color: 3

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 400359 Color: 2
Size: 314341 Color: 0
Size: 285301 Color: 1

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 400405 Color: 2
Size: 325016 Color: 4
Size: 274580 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 400417 Color: 4
Size: 315169 Color: 0
Size: 284415 Color: 1

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 400596 Color: 0
Size: 325170 Color: 2
Size: 274235 Color: 4

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 400625 Color: 0
Size: 302987 Color: 4
Size: 296389 Color: 2

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 400860 Color: 0
Size: 301608 Color: 4
Size: 297533 Color: 4

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 400673 Color: 2
Size: 314028 Color: 4
Size: 285300 Color: 2

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 400830 Color: 3
Size: 315860 Color: 0
Size: 283311 Color: 3

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 400862 Color: 4
Size: 335245 Color: 3
Size: 263894 Color: 1

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 400967 Color: 0
Size: 320541 Color: 3
Size: 278493 Color: 1

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 400944 Color: 3
Size: 319328 Color: 3
Size: 279729 Color: 1

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 400961 Color: 4
Size: 303900 Color: 0
Size: 295140 Color: 3

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 400993 Color: 0
Size: 340810 Color: 3
Size: 258198 Color: 2

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 400961 Color: 3
Size: 335433 Color: 2
Size: 263607 Color: 2

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 400964 Color: 4
Size: 302101 Color: 2
Size: 296936 Color: 0

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 401033 Color: 0
Size: 309164 Color: 4
Size: 289804 Color: 4

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 401020 Color: 3
Size: 306453 Color: 1
Size: 292528 Color: 1

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 401249 Color: 4
Size: 303782 Color: 0
Size: 294970 Color: 1

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 401275 Color: 3
Size: 342730 Color: 1
Size: 255996 Color: 4

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 401587 Color: 2
Size: 327119 Color: 3
Size: 271295 Color: 1

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 401653 Color: 4
Size: 332955 Color: 4
Size: 265393 Color: 2

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 401672 Color: 4
Size: 307632 Color: 2
Size: 290697 Color: 3

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 401713 Color: 2
Size: 312371 Color: 4
Size: 285917 Color: 1

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 401783 Color: 2
Size: 346118 Color: 1
Size: 252100 Color: 1

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 401864 Color: 4
Size: 325746 Color: 3
Size: 272391 Color: 2

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 401916 Color: 4
Size: 340364 Color: 3
Size: 257721 Color: 4

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 401926 Color: 1
Size: 303515 Color: 0
Size: 294560 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 401930 Color: 2
Size: 315964 Color: 4
Size: 282107 Color: 3

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 401930 Color: 0
Size: 302996 Color: 3
Size: 295075 Color: 1

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 401948 Color: 2
Size: 339044 Color: 4
Size: 259009 Color: 4

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 402002 Color: 4
Size: 330814 Color: 3
Size: 267185 Color: 3

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 401986 Color: 3
Size: 335059 Color: 4
Size: 262956 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 402073 Color: 2
Size: 333035 Color: 4
Size: 264893 Color: 4

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 402133 Color: 4
Size: 335374 Color: 2
Size: 262494 Color: 2

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 402158 Color: 4
Size: 335624 Color: 3
Size: 262219 Color: 4

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 402213 Color: 3
Size: 346175 Color: 2
Size: 251613 Color: 2

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 402186 Color: 0
Size: 334353 Color: 2
Size: 263462 Color: 2

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 402245 Color: 2
Size: 301363 Color: 0
Size: 296393 Color: 4

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 402310 Color: 3
Size: 306246 Color: 1
Size: 291445 Color: 4

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 402359 Color: 1
Size: 340078 Color: 0
Size: 257564 Color: 4

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 402404 Color: 4
Size: 313006 Color: 4
Size: 284591 Color: 3

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 402423 Color: 4
Size: 328024 Color: 2
Size: 269554 Color: 1

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 402424 Color: 0
Size: 321811 Color: 0
Size: 275766 Color: 3

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 402468 Color: 4
Size: 301210 Color: 2
Size: 296323 Color: 2

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 402490 Color: 0
Size: 346301 Color: 3
Size: 251210 Color: 2

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 402674 Color: 3
Size: 322641 Color: 4
Size: 274686 Color: 4

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 402513 Color: 1
Size: 345304 Color: 4
Size: 252184 Color: 4

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 402643 Color: 4
Size: 299819 Color: 1
Size: 297539 Color: 3

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 402764 Color: 2
Size: 331431 Color: 0
Size: 265806 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 402787 Color: 2
Size: 309898 Color: 1
Size: 287316 Color: 3

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 402803 Color: 0
Size: 314768 Color: 2
Size: 282430 Color: 4

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 402817 Color: 1
Size: 306258 Color: 3
Size: 290926 Color: 4

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 402925 Color: 4
Size: 327108 Color: 0
Size: 269968 Color: 2

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 403040 Color: 4
Size: 310669 Color: 3
Size: 286292 Color: 1

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 403102 Color: 1
Size: 302063 Color: 2
Size: 294836 Color: 2

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 403155 Color: 0
Size: 315660 Color: 2
Size: 281186 Color: 3

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 403336 Color: 3
Size: 346362 Color: 0
Size: 250303 Color: 1

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 403171 Color: 1
Size: 329654 Color: 4
Size: 267176 Color: 2

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 403366 Color: 3
Size: 317327 Color: 1
Size: 279308 Color: 2

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 403260 Color: 0
Size: 339485 Color: 0
Size: 257256 Color: 2

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 403355 Color: 0
Size: 304501 Color: 2
Size: 292145 Color: 3

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 403452 Color: 1
Size: 328643 Color: 2
Size: 267906 Color: 3

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 403482 Color: 2
Size: 329535 Color: 0
Size: 266984 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 403489 Color: 4
Size: 323577 Color: 3
Size: 272935 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 403541 Color: 4
Size: 333675 Color: 2
Size: 262785 Color: 4

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 403549 Color: 1
Size: 320577 Color: 2
Size: 275875 Color: 3

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 403586 Color: 1
Size: 334287 Color: 4
Size: 262128 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 403616 Color: 0
Size: 328205 Color: 1
Size: 268180 Color: 3

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 403838 Color: 3
Size: 310524 Color: 4
Size: 285639 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 403659 Color: 2
Size: 300430 Color: 0
Size: 295912 Color: 1

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 403897 Color: 2
Size: 303197 Color: 2
Size: 292907 Color: 3

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 403956 Color: 3
Size: 341093 Color: 0
Size: 254952 Color: 3

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 404021 Color: 4
Size: 305395 Color: 2
Size: 290585 Color: 3

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 404210 Color: 1
Size: 342063 Color: 4
Size: 253728 Color: 4

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 404247 Color: 0
Size: 334083 Color: 2
Size: 261671 Color: 3

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 404299 Color: 1
Size: 335944 Color: 4
Size: 259758 Color: 2

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 404513 Color: 3
Size: 308914 Color: 2
Size: 286574 Color: 1

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 404602 Color: 3
Size: 332833 Color: 0
Size: 262566 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 404493 Color: 2
Size: 300654 Color: 4
Size: 294854 Color: 1

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 404636 Color: 3
Size: 324281 Color: 1
Size: 271084 Color: 2

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 404560 Color: 4
Size: 331198 Color: 1
Size: 264243 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 404705 Color: 3
Size: 301139 Color: 4
Size: 294157 Color: 1

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 404774 Color: 4
Size: 328479 Color: 3
Size: 266748 Color: 1

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 404779 Color: 4
Size: 298555 Color: 2
Size: 296667 Color: 1

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 404815 Color: 0
Size: 337681 Color: 0
Size: 257505 Color: 3

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 404873 Color: 1
Size: 337674 Color: 4
Size: 257454 Color: 3

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 405002 Color: 3
Size: 324491 Color: 4
Size: 270508 Color: 1

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 404976 Color: 2
Size: 341084 Color: 2
Size: 253941 Color: 4

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 405100 Color: 2
Size: 328532 Color: 3
Size: 266369 Color: 1

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 405118 Color: 4
Size: 331952 Color: 1
Size: 262931 Color: 0

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 405196 Color: 3
Size: 322891 Color: 0
Size: 271914 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 405230 Color: 0
Size: 315553 Color: 2
Size: 279218 Color: 1

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 405308 Color: 3
Size: 322595 Color: 4
Size: 272098 Color: 4

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 405268 Color: 1
Size: 309153 Color: 0
Size: 285580 Color: 0

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 405310 Color: 2
Size: 314677 Color: 0
Size: 280014 Color: 3

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 405406 Color: 3
Size: 318133 Color: 4
Size: 276462 Color: 1

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 405377 Color: 4
Size: 297636 Color: 2
Size: 296988 Color: 3

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 405427 Color: 0
Size: 298919 Color: 0
Size: 295655 Color: 2

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 405502 Color: 2
Size: 343623 Color: 3
Size: 250876 Color: 2

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 405519 Color: 3
Size: 324028 Color: 2
Size: 270454 Color: 4

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 405575 Color: 1
Size: 328504 Color: 4
Size: 265922 Color: 1

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 405605 Color: 0
Size: 322437 Color: 3
Size: 271959 Color: 2

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 405718 Color: 4
Size: 322622 Color: 1
Size: 271661 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 405858 Color: 1
Size: 299759 Color: 0
Size: 294384 Color: 1

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 405869 Color: 1
Size: 298056 Color: 3
Size: 296076 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 405975 Color: 4
Size: 330577 Color: 0
Size: 263449 Color: 1

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 406055 Color: 1
Size: 320002 Color: 4
Size: 273944 Color: 3

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 406148 Color: 4
Size: 309993 Color: 3
Size: 283860 Color: 4

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 406172 Color: 1
Size: 306795 Color: 2
Size: 287034 Color: 3

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 406205 Color: 1
Size: 333686 Color: 2
Size: 260110 Color: 3

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 406290 Color: 1
Size: 302176 Color: 4
Size: 291535 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 406344 Color: 3
Size: 326277 Color: 2
Size: 267380 Color: 1

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 406329 Color: 1
Size: 300191 Color: 2
Size: 293481 Color: 4

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 406509 Color: 4
Size: 327094 Color: 0
Size: 266398 Color: 4

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 406569 Color: 1
Size: 305336 Color: 1
Size: 288096 Color: 4

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 406683 Color: 2
Size: 330793 Color: 4
Size: 262525 Color: 3

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 406740 Color: 3
Size: 337373 Color: 2
Size: 255888 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 406875 Color: 3
Size: 323192 Color: 4
Size: 269934 Color: 4

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 406766 Color: 2
Size: 336664 Color: 1
Size: 256571 Color: 0

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 406782 Color: 0
Size: 342862 Color: 3
Size: 250357 Color: 2

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 406879 Color: 4
Size: 338611 Color: 4
Size: 254511 Color: 3

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 406885 Color: 2
Size: 335970 Color: 4
Size: 257146 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 406893 Color: 4
Size: 306272 Color: 3
Size: 286836 Color: 0

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 406950 Color: 4
Size: 327387 Color: 3
Size: 265664 Color: 1

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 406981 Color: 2
Size: 336041 Color: 2
Size: 256979 Color: 1

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 406999 Color: 0
Size: 321792 Color: 1
Size: 271210 Color: 3

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 407194 Color: 0
Size: 301503 Color: 0
Size: 291304 Color: 2

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 407333 Color: 0
Size: 339312 Color: 4
Size: 253356 Color: 2

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 407375 Color: 4
Size: 339210 Color: 4
Size: 253416 Color: 3

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 407383 Color: 0
Size: 316710 Color: 0
Size: 275908 Color: 4

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 407492 Color: 1
Size: 335366 Color: 0
Size: 257143 Color: 4

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 407558 Color: 1
Size: 310184 Color: 3
Size: 282259 Color: 2

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 407651 Color: 0
Size: 325750 Color: 4
Size: 266600 Color: 2

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 407765 Color: 1
Size: 336603 Color: 3
Size: 255633 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 407816 Color: 2
Size: 306739 Color: 4
Size: 285446 Color: 1

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 407900 Color: 1
Size: 313168 Color: 4
Size: 278933 Color: 1

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 407927 Color: 1
Size: 337209 Color: 4
Size: 254865 Color: 3

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 407990 Color: 2
Size: 309208 Color: 0
Size: 282803 Color: 3

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 407996 Color: 4
Size: 314712 Color: 1
Size: 277293 Color: 1

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 408054 Color: 3
Size: 310688 Color: 1
Size: 281259 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 408165 Color: 2
Size: 323216 Color: 4
Size: 268620 Color: 1

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 408199 Color: 4
Size: 300438 Color: 3
Size: 291364 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 408289 Color: 1
Size: 333658 Color: 0
Size: 258054 Color: 3

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 408390 Color: 4
Size: 337665 Color: 3
Size: 253946 Color: 4

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 408474 Color: 4
Size: 319290 Color: 2
Size: 272237 Color: 2

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 408562 Color: 0
Size: 307345 Color: 2
Size: 284094 Color: 3

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 408663 Color: 3
Size: 318044 Color: 1
Size: 273294 Color: 2

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 408642 Color: 4
Size: 340124 Color: 4
Size: 251235 Color: 1

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 408652 Color: 1
Size: 332807 Color: 4
Size: 258542 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 408708 Color: 1
Size: 313414 Color: 4
Size: 277879 Color: 3

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 408978 Color: 4
Size: 309221 Color: 1
Size: 281802 Color: 3

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 408982 Color: 2
Size: 328887 Color: 4
Size: 262132 Color: 4

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 409023 Color: 1
Size: 322037 Color: 4
Size: 268941 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 409124 Color: 4
Size: 314943 Color: 0
Size: 275934 Color: 3

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 409212 Color: 2
Size: 299526 Color: 2
Size: 291263 Color: 1

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 409257 Color: 4
Size: 299029 Color: 3
Size: 291715 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 409329 Color: 2
Size: 327732 Color: 4
Size: 262940 Color: 1

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 409346 Color: 1
Size: 297319 Color: 0
Size: 293336 Color: 3

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 409437 Color: 1
Size: 338704 Color: 3
Size: 251860 Color: 2

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 409600 Color: 3
Size: 323777 Color: 4
Size: 266624 Color: 2

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 409647 Color: 3
Size: 297447 Color: 1
Size: 292907 Color: 2

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 409568 Color: 0
Size: 306309 Color: 2
Size: 284124 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 409582 Color: 1
Size: 330148 Color: 4
Size: 260271 Color: 3

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 409732 Color: 3
Size: 321485 Color: 1
Size: 268784 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 409647 Color: 2
Size: 310482 Color: 1
Size: 279872 Color: 1

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 409793 Color: 0
Size: 332658 Color: 0
Size: 257550 Color: 4

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 409811 Color: 4
Size: 331769 Color: 0
Size: 258421 Color: 3

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 409812 Color: 3
Size: 331436 Color: 2
Size: 258753 Color: 1

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 409826 Color: 4
Size: 297527 Color: 0
Size: 292648 Color: 1

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 409829 Color: 2
Size: 323215 Color: 3
Size: 266957 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 409894 Color: 2
Size: 338888 Color: 3
Size: 251219 Color: 1

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 409992 Color: 2
Size: 334275 Color: 1
Size: 255734 Color: 4

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 410013 Color: 2
Size: 311265 Color: 1
Size: 278723 Color: 3

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 410059 Color: 1
Size: 328327 Color: 4
Size: 261615 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 410079 Color: 4
Size: 315085 Color: 3
Size: 274837 Color: 1

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 410116 Color: 0
Size: 321088 Color: 4
Size: 268797 Color: 4

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 410214 Color: 4
Size: 329829 Color: 3
Size: 259958 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 410380 Color: 4
Size: 339582 Color: 3
Size: 250039 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 410386 Color: 0
Size: 300130 Color: 4
Size: 289485 Color: 4

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 410405 Color: 1
Size: 314527 Color: 3
Size: 275069 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 410342 Color: 3
Size: 330499 Color: 0
Size: 259160 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 410442 Color: 0
Size: 318046 Color: 1
Size: 271513 Color: 2

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 410458 Color: 1
Size: 324498 Color: 3
Size: 265045 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 410486 Color: 1
Size: 321419 Color: 4
Size: 268096 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 410525 Color: 2
Size: 303903 Color: 0
Size: 285573 Color: 3

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 410517 Color: 3
Size: 306116 Color: 4
Size: 283368 Color: 1

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 410555 Color: 2
Size: 330548 Color: 0
Size: 258898 Color: 4

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 410643 Color: 0
Size: 334234 Color: 3
Size: 255124 Color: 1

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 410753 Color: 2
Size: 325421 Color: 4
Size: 263827 Color: 2

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 410759 Color: 2
Size: 308752 Color: 3
Size: 280490 Color: 1

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 410829 Color: 0
Size: 311414 Color: 3
Size: 277758 Color: 2

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 410835 Color: 4
Size: 295255 Color: 1
Size: 293911 Color: 1

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 410874 Color: 4
Size: 301666 Color: 3
Size: 287461 Color: 0

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 411008 Color: 4
Size: 337241 Color: 3
Size: 251752 Color: 1

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 411130 Color: 4
Size: 298169 Color: 1
Size: 290702 Color: 3

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 411189 Color: 0
Size: 311355 Color: 0
Size: 277457 Color: 4

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 411266 Color: 3
Size: 333771 Color: 1
Size: 254964 Color: 1

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 411298 Color: 0
Size: 321035 Color: 1
Size: 267668 Color: 4

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 411350 Color: 4
Size: 315977 Color: 4
Size: 272674 Color: 3

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 411351 Color: 0
Size: 308078 Color: 1
Size: 280572 Color: 3

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 411361 Color: 0
Size: 323743 Color: 4
Size: 264897 Color: 2

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 411404 Color: 0
Size: 318032 Color: 4
Size: 270565 Color: 4

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 411625 Color: 1
Size: 313728 Color: 3
Size: 274648 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 411727 Color: 2
Size: 328860 Color: 4
Size: 259414 Color: 3

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 411760 Color: 0
Size: 327721 Color: 0
Size: 260520 Color: 1

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 411763 Color: 1
Size: 325914 Color: 2
Size: 262324 Color: 3

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 411934 Color: 1
Size: 319662 Color: 0
Size: 268405 Color: 4

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 412025 Color: 3
Size: 328952 Color: 2
Size: 259024 Color: 4

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 411937 Color: 4
Size: 320332 Color: 2
Size: 267732 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 411956 Color: 4
Size: 330810 Color: 4
Size: 257235 Color: 3

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 412081 Color: 3
Size: 319927 Color: 4
Size: 267993 Color: 1

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 412158 Color: 3
Size: 298242 Color: 1
Size: 289601 Color: 1

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 412111 Color: 1
Size: 326975 Color: 0
Size: 260915 Color: 4

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 412137 Color: 4
Size: 321672 Color: 1
Size: 266192 Color: 3

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 412196 Color: 1
Size: 306805 Color: 3
Size: 281000 Color: 4

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 412438 Color: 2
Size: 298169 Color: 4
Size: 289394 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 412509 Color: 0
Size: 309404 Color: 0
Size: 278088 Color: 3

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 412503 Color: 3
Size: 312408 Color: 1
Size: 275090 Color: 0

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 412530 Color: 1
Size: 308439 Color: 4
Size: 279032 Color: 4

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 412719 Color: 3
Size: 306887 Color: 0
Size: 280395 Color: 4

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 412647 Color: 1
Size: 316604 Color: 2
Size: 270750 Color: 4

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 412798 Color: 4
Size: 296095 Color: 2
Size: 291108 Color: 3

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 412812 Color: 0
Size: 329387 Color: 4
Size: 257802 Color: 4

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 412912 Color: 0
Size: 307638 Color: 4
Size: 279451 Color: 3

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 412929 Color: 2
Size: 326496 Color: 1
Size: 260576 Color: 4

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 412954 Color: 0
Size: 320400 Color: 1
Size: 266647 Color: 3

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 412958 Color: 4
Size: 299027 Color: 0
Size: 288016 Color: 1

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 412983 Color: 1
Size: 298569 Color: 2
Size: 288449 Color: 3

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 412986 Color: 4
Size: 317106 Color: 1
Size: 269909 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 413138 Color: 3
Size: 324370 Color: 1
Size: 262493 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 413048 Color: 0
Size: 328698 Color: 0
Size: 258255 Color: 4

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 413174 Color: 0
Size: 330942 Color: 2
Size: 255885 Color: 3

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 413232 Color: 1
Size: 336697 Color: 3
Size: 250072 Color: 1

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 413327 Color: 1
Size: 332183 Color: 4
Size: 254491 Color: 2

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 413375 Color: 4
Size: 321973 Color: 2
Size: 264653 Color: 3

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 413337 Color: 3
Size: 317282 Color: 0
Size: 269382 Color: 1

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 413418 Color: 3
Size: 304208 Color: 2
Size: 282375 Color: 4

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 413400 Color: 2
Size: 330981 Color: 0
Size: 255620 Color: 1

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 413492 Color: 0
Size: 293690 Color: 3
Size: 292819 Color: 1

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 413537 Color: 2
Size: 311556 Color: 1
Size: 274908 Color: 1

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 413569 Color: 0
Size: 335008 Color: 3
Size: 251424 Color: 2

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 413579 Color: 4
Size: 308422 Color: 1
Size: 278000 Color: 1

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 413773 Color: 3
Size: 313996 Color: 1
Size: 272232 Color: 1

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 413600 Color: 1
Size: 304944 Color: 0
Size: 281457 Color: 2

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 413879 Color: 3
Size: 316380 Color: 2
Size: 269742 Color: 1

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 413601 Color: 2
Size: 315410 Color: 1
Size: 270990 Color: 4

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 413878 Color: 0
Size: 309212 Color: 4
Size: 276911 Color: 4

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 413976 Color: 0
Size: 299676 Color: 3
Size: 286349 Color: 2

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 414098 Color: 3
Size: 304228 Color: 2
Size: 281675 Color: 4

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 414074 Color: 0
Size: 325703 Color: 4
Size: 260224 Color: 2

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 414144 Color: 0
Size: 324274 Color: 2
Size: 261583 Color: 1

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 414172 Color: 4
Size: 301512 Color: 3
Size: 284317 Color: 2

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 414198 Color: 2
Size: 304591 Color: 4
Size: 281212 Color: 3

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 414239 Color: 4
Size: 324276 Color: 2
Size: 261486 Color: 2

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 414325 Color: 3
Size: 316824 Color: 4
Size: 268852 Color: 3

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 414346 Color: 2
Size: 319583 Color: 4
Size: 266072 Color: 1

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 414425 Color: 4
Size: 310829 Color: 1
Size: 274747 Color: 1

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 414491 Color: 2
Size: 328873 Color: 1
Size: 256637 Color: 3

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 414503 Color: 0
Size: 305621 Color: 1
Size: 279877 Color: 2

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 414518 Color: 3
Size: 331540 Color: 1
Size: 253943 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 414532 Color: 0
Size: 324024 Color: 2
Size: 261445 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 414588 Color: 1
Size: 333699 Color: 3
Size: 251714 Color: 2

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 414679 Color: 4
Size: 323957 Color: 1
Size: 261365 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 414779 Color: 1
Size: 305324 Color: 3
Size: 279898 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 414853 Color: 0
Size: 307906 Color: 4
Size: 277242 Color: 1

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 414858 Color: 1
Size: 311521 Color: 3
Size: 273622 Color: 2

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 414902 Color: 4
Size: 293727 Color: 1
Size: 291372 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 415098 Color: 1
Size: 331384 Color: 2
Size: 253519 Color: 1

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 415138 Color: 4
Size: 297943 Color: 3
Size: 286920 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 415189 Color: 0
Size: 305137 Color: 2
Size: 279675 Color: 4

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 415404 Color: 3
Size: 312540 Color: 4
Size: 272057 Color: 2

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 415371 Color: 1
Size: 315984 Color: 3
Size: 268646 Color: 2

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 415474 Color: 3
Size: 293778 Color: 4
Size: 290749 Color: 1

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 415488 Color: 4
Size: 334290 Color: 2
Size: 250223 Color: 1

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 415533 Color: 1
Size: 313845 Color: 3
Size: 270623 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 415536 Color: 3
Size: 316329 Color: 0
Size: 268136 Color: 2

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 415545 Color: 4
Size: 307078 Color: 2
Size: 277378 Color: 4

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 415727 Color: 3
Size: 317745 Color: 4
Size: 266529 Color: 1

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 415673 Color: 4
Size: 295523 Color: 0
Size: 288805 Color: 3

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 415856 Color: 2
Size: 324588 Color: 4
Size: 259557 Color: 2

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 415873 Color: 2
Size: 318772 Color: 1
Size: 265356 Color: 3

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 415920 Color: 3
Size: 306978 Color: 4
Size: 277103 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 415969 Color: 1
Size: 292942 Color: 3
Size: 291090 Color: 1

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 416078 Color: 3
Size: 303091 Color: 0
Size: 280832 Color: 2

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 416127 Color: 1
Size: 320562 Color: 3
Size: 263312 Color: 1

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 416170 Color: 3
Size: 323312 Color: 1
Size: 260519 Color: 1

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 416176 Color: 1
Size: 319507 Color: 4
Size: 264318 Color: 1

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 416183 Color: 1
Size: 292073 Color: 4
Size: 291745 Color: 4

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 416411 Color: 4
Size: 309874 Color: 4
Size: 273716 Color: 3

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 416548 Color: 3
Size: 296758 Color: 1
Size: 286695 Color: 2

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 416471 Color: 0
Size: 298239 Color: 2
Size: 285291 Color: 1

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 416597 Color: 3
Size: 312244 Color: 2
Size: 271160 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 416597 Color: 3
Size: 315025 Color: 4
Size: 268379 Color: 2

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 416556 Color: 1
Size: 319455 Color: 4
Size: 263990 Color: 4

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 416617 Color: 2
Size: 294540 Color: 2
Size: 288844 Color: 1

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 416672 Color: 4
Size: 306889 Color: 3
Size: 276440 Color: 2

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 416688 Color: 4
Size: 320515 Color: 2
Size: 262798 Color: 1

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 416715 Color: 1
Size: 312684 Color: 4
Size: 270602 Color: 3

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 416779 Color: 2
Size: 330736 Color: 0
Size: 252486 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 416882 Color: 1
Size: 325782 Color: 4
Size: 257337 Color: 3

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 416889 Color: 4
Size: 302583 Color: 1
Size: 280529 Color: 2

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 416987 Color: 4
Size: 320813 Color: 1
Size: 262201 Color: 3

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 416988 Color: 2
Size: 322134 Color: 3
Size: 260879 Color: 1

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 416996 Color: 2
Size: 329826 Color: 1
Size: 253179 Color: 2

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 417027 Color: 4
Size: 312846 Color: 1
Size: 270128 Color: 2

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 417119 Color: 1
Size: 294655 Color: 2
Size: 288227 Color: 3

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 417210 Color: 0
Size: 311032 Color: 0
Size: 271759 Color: 1

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 417241 Color: 1
Size: 328029 Color: 3
Size: 254731 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 417385 Color: 2
Size: 301031 Color: 4
Size: 281585 Color: 3

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 417412 Color: 0
Size: 291391 Color: 3
Size: 291198 Color: 4

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 417469 Color: 3
Size: 320333 Color: 2
Size: 262199 Color: 4

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 417425 Color: 0
Size: 330606 Color: 4
Size: 251970 Color: 4

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 417530 Color: 3
Size: 302449 Color: 0
Size: 280022 Color: 4

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 417449 Color: 1
Size: 294016 Color: 2
Size: 288536 Color: 2

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 417452 Color: 2
Size: 332421 Color: 1
Size: 250128 Color: 3

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 417656 Color: 3
Size: 330637 Color: 0
Size: 251708 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 417545 Color: 4
Size: 306367 Color: 1
Size: 276089 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 417756 Color: 0
Size: 326145 Color: 3
Size: 256100 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 418100 Color: 3
Size: 302210 Color: 2
Size: 279691 Color: 1

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 418244 Color: 1
Size: 320222 Color: 3
Size: 261535 Color: 2

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 418359 Color: 3
Size: 326669 Color: 1
Size: 254973 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 418271 Color: 1
Size: 304707 Color: 4
Size: 277023 Color: 2

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 418420 Color: 3
Size: 320657 Color: 0
Size: 260924 Color: 4

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 418308 Color: 4
Size: 306153 Color: 0
Size: 275540 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 418339 Color: 1
Size: 321682 Color: 3
Size: 259980 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 418380 Color: 1
Size: 296755 Color: 1
Size: 284866 Color: 2

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 418450 Color: 3
Size: 325301 Color: 0
Size: 256250 Color: 4

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 418393 Color: 4
Size: 321370 Color: 1
Size: 260238 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 418533 Color: 3
Size: 295934 Color: 4
Size: 285534 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 418565 Color: 1
Size: 328157 Color: 2
Size: 253279 Color: 3

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 418670 Color: 3
Size: 297864 Color: 4
Size: 283467 Color: 1

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 418572 Color: 0
Size: 291444 Color: 2
Size: 289985 Color: 1

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 418684 Color: 0
Size: 306503 Color: 1
Size: 274814 Color: 2

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 418697 Color: 0
Size: 300341 Color: 1
Size: 280963 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 418721 Color: 4
Size: 330814 Color: 2
Size: 250466 Color: 1

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 418746 Color: 2
Size: 323943 Color: 4
Size: 257312 Color: 1

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 418788 Color: 2
Size: 329585 Color: 3
Size: 251628 Color: 2

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 418829 Color: 0
Size: 324714 Color: 0
Size: 256458 Color: 1

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 418978 Color: 3
Size: 313504 Color: 0
Size: 267519 Color: 2

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 418949 Color: 0
Size: 326428 Color: 0
Size: 254624 Color: 1

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 419195 Color: 1
Size: 300111 Color: 2
Size: 280695 Color: 2

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 419251 Color: 0
Size: 329726 Color: 1
Size: 251024 Color: 3

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 419297 Color: 1
Size: 294533 Color: 3
Size: 286171 Color: 4

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 419402 Color: 3
Size: 306147 Color: 0
Size: 274452 Color: 4

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 419493 Color: 0
Size: 310129 Color: 4
Size: 270379 Color: 3

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 419622 Color: 4
Size: 296734 Color: 3
Size: 283645 Color: 2

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 419720 Color: 1
Size: 315122 Color: 0
Size: 265159 Color: 3

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 419989 Color: 4
Size: 317114 Color: 4
Size: 262898 Color: 3

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 420142 Color: 2
Size: 322335 Color: 2
Size: 257524 Color: 1

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 420154 Color: 0
Size: 311819 Color: 1
Size: 268028 Color: 4

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 420158 Color: 0
Size: 290044 Color: 3
Size: 289799 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 420264 Color: 1
Size: 303402 Color: 3
Size: 276335 Color: 2

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 420335 Color: 3
Size: 323525 Color: 0
Size: 256141 Color: 2

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 420307 Color: 0
Size: 298075 Color: 2
Size: 281619 Color: 1

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 420426 Color: 3
Size: 321539 Color: 2
Size: 258036 Color: 1

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 420351 Color: 0
Size: 317111 Color: 1
Size: 262539 Color: 2

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 420388 Color: 4
Size: 318191 Color: 0
Size: 261422 Color: 3

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 420511 Color: 3
Size: 329483 Color: 0
Size: 250007 Color: 4

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 420448 Color: 1
Size: 304634 Color: 2
Size: 274919 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 420461 Color: 1
Size: 321631 Color: 4
Size: 257909 Color: 2

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 420705 Color: 3
Size: 293338 Color: 1
Size: 285958 Color: 2

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 420822 Color: 2
Size: 298288 Color: 0
Size: 280891 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 421012 Color: 3
Size: 324549 Color: 1
Size: 254440 Color: 4

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 421133 Color: 2
Size: 316779 Color: 2
Size: 262089 Color: 3

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 421155 Color: 2
Size: 320151 Color: 3
Size: 258695 Color: 4

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 421266 Color: 3
Size: 310815 Color: 4
Size: 267920 Color: 1

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 421185 Color: 1
Size: 328108 Color: 2
Size: 250708 Color: 4

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 421254 Color: 2
Size: 310540 Color: 4
Size: 268207 Color: 3

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 421320 Color: 4
Size: 304138 Color: 3
Size: 274543 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 421365 Color: 4
Size: 319707 Color: 0
Size: 258929 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 421411 Color: 1
Size: 306609 Color: 0
Size: 271981 Color: 3

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 421452 Color: 4
Size: 322571 Color: 0
Size: 255978 Color: 1

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 421497 Color: 1
Size: 309729 Color: 2
Size: 268775 Color: 3

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 421511 Color: 0
Size: 302021 Color: 2
Size: 276469 Color: 3

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 421557 Color: 2
Size: 289342 Color: 1
Size: 289102 Color: 3

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 421639 Color: 1
Size: 305447 Color: 2
Size: 272915 Color: 2

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 421688 Color: 1
Size: 313891 Color: 0
Size: 264422 Color: 3

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 421691 Color: 0
Size: 293148 Color: 4
Size: 285162 Color: 1

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 421724 Color: 2
Size: 290267 Color: 3
Size: 288010 Color: 4

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 421739 Color: 0
Size: 320768 Color: 1
Size: 257494 Color: 3

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 421888 Color: 2
Size: 324108 Color: 2
Size: 254005 Color: 4

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 421901 Color: 1
Size: 295705 Color: 1
Size: 282395 Color: 3

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 421927 Color: 4
Size: 320812 Color: 3
Size: 257262 Color: 1

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 421965 Color: 4
Size: 295936 Color: 0
Size: 282100 Color: 2

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 422205 Color: 3
Size: 322498 Color: 4
Size: 255298 Color: 2

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 422056 Color: 2
Size: 322816 Color: 4
Size: 255129 Color: 0

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 422302 Color: 3
Size: 313975 Color: 2
Size: 263724 Color: 2

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 422333 Color: 4
Size: 308699 Color: 3
Size: 268969 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 422399 Color: 0
Size: 316584 Color: 3
Size: 261018 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 422586 Color: 3
Size: 325959 Color: 0
Size: 251456 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 422575 Color: 2
Size: 323557 Color: 4
Size: 253869 Color: 1

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 422588 Color: 0
Size: 321429 Color: 3
Size: 255984 Color: 2

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 422660 Color: 4
Size: 318426 Color: 0
Size: 258915 Color: 2

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 422747 Color: 1
Size: 289862 Color: 3
Size: 287392 Color: 4

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 422817 Color: 4
Size: 314889 Color: 1
Size: 262295 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 422821 Color: 0
Size: 295794 Color: 3
Size: 281386 Color: 2

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 422944 Color: 2
Size: 290672 Color: 4
Size: 286385 Color: 1

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 422975 Color: 3
Size: 304730 Color: 0
Size: 272296 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 422985 Color: 2
Size: 324159 Color: 2
Size: 252857 Color: 1

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 423044 Color: 4
Size: 290560 Color: 1
Size: 286397 Color: 3

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 423046 Color: 1
Size: 318880 Color: 0
Size: 258075 Color: 1

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 423139 Color: 2
Size: 301365 Color: 3
Size: 275497 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 423203 Color: 2
Size: 298922 Color: 3
Size: 277876 Color: 1

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 423213 Color: 2
Size: 311875 Color: 1
Size: 264913 Color: 4

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 423272 Color: 4
Size: 311569 Color: 3
Size: 265160 Color: 2

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 423286 Color: 4
Size: 293614 Color: 1
Size: 283101 Color: 4

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 423338 Color: 0
Size: 318120 Color: 3
Size: 258543 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 423344 Color: 3
Size: 312272 Color: 3
Size: 264385 Color: 1

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 423359 Color: 2
Size: 309199 Color: 1
Size: 267443 Color: 4

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 423389 Color: 4
Size: 294379 Color: 4
Size: 282233 Color: 3

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 423344 Color: 0
Size: 304978 Color: 1
Size: 271679 Color: 2

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 423459 Color: 0
Size: 323094 Color: 4
Size: 253448 Color: 3

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 423589 Color: 3
Size: 308143 Color: 0
Size: 268269 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 423667 Color: 3
Size: 322328 Color: 4
Size: 254006 Color: 1

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 423513 Color: 0
Size: 307781 Color: 4
Size: 268707 Color: 2

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 423726 Color: 3
Size: 323317 Color: 0
Size: 252958 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 423557 Color: 1
Size: 319724 Color: 2
Size: 256720 Color: 4

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 423759 Color: 3
Size: 322270 Color: 4
Size: 253972 Color: 2

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 423919 Color: 3
Size: 317036 Color: 2
Size: 259046 Color: 0

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 424009 Color: 2
Size: 291008 Color: 3
Size: 284984 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 424047 Color: 0
Size: 324991 Color: 3
Size: 250963 Color: 1

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 424047 Color: 2
Size: 315182 Color: 1
Size: 260772 Color: 2

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 424153 Color: 0
Size: 295482 Color: 1
Size: 280366 Color: 2

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 424213 Color: 4
Size: 294912 Color: 3
Size: 280876 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 424215 Color: 4
Size: 292030 Color: 3
Size: 283756 Color: 1

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 424317 Color: 2
Size: 318795 Color: 2
Size: 256889 Color: 4

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 424363 Color: 2
Size: 307816 Color: 3
Size: 267822 Color: 4

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 424420 Color: 1
Size: 313643 Color: 4
Size: 261938 Color: 3

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 424455 Color: 4
Size: 295660 Color: 1
Size: 279886 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 424471 Color: 2
Size: 300279 Color: 3
Size: 275251 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 424476 Color: 4
Size: 317872 Color: 2
Size: 257653 Color: 2

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 424482 Color: 4
Size: 317462 Color: 2
Size: 258057 Color: 3

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 424505 Color: 0
Size: 305339 Color: 1
Size: 270157 Color: 3

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 424530 Color: 0
Size: 321452 Color: 1
Size: 254019 Color: 1

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 424549 Color: 2
Size: 296164 Color: 0
Size: 279288 Color: 4

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 424634 Color: 0
Size: 313694 Color: 4
Size: 261673 Color: 3

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 424645 Color: 4
Size: 304945 Color: 2
Size: 270411 Color: 1

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 424775 Color: 3
Size: 318575 Color: 2
Size: 256651 Color: 2

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 424808 Color: 4
Size: 295029 Color: 0
Size: 280164 Color: 3

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 424821 Color: 2
Size: 307671 Color: 1
Size: 267509 Color: 3

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 424838 Color: 4
Size: 289668 Color: 4
Size: 285495 Color: 1

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 424950 Color: 0
Size: 291483 Color: 2
Size: 283568 Color: 3

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 425192 Color: 1
Size: 304851 Color: 2
Size: 269958 Color: 3

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 425336 Color: 1
Size: 294206 Color: 3
Size: 280459 Color: 2

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 425462 Color: 4
Size: 314447 Color: 1
Size: 260092 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 425481 Color: 3
Size: 299147 Color: 4
Size: 275373 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 425507 Color: 1
Size: 322368 Color: 2
Size: 252126 Color: 1

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 425555 Color: 1
Size: 323665 Color: 1
Size: 250781 Color: 3

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 425707 Color: 1
Size: 298698 Color: 2
Size: 275596 Color: 3

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 425708 Color: 2
Size: 305688 Color: 1
Size: 268605 Color: 1

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 425747 Color: 4
Size: 310251 Color: 3
Size: 264003 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 426124 Color: 1
Size: 306852 Color: 2
Size: 267025 Color: 1

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 426178 Color: 2
Size: 301727 Color: 3
Size: 272096 Color: 4

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 426181 Color: 0
Size: 309027 Color: 2
Size: 264793 Color: 2

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 426188 Color: 4
Size: 299514 Color: 3
Size: 274299 Color: 2

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 426351 Color: 0
Size: 313709 Color: 0
Size: 259941 Color: 4

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 426430 Color: 3
Size: 292287 Color: 0
Size: 281284 Color: 1

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 426444 Color: 1
Size: 291670 Color: 1
Size: 281887 Color: 4

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 426555 Color: 4
Size: 289948 Color: 3
Size: 283498 Color: 1

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 426572 Color: 2
Size: 313566 Color: 2
Size: 259863 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 426617 Color: 4
Size: 307233 Color: 3
Size: 266151 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 426633 Color: 4
Size: 308778 Color: 1
Size: 264590 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 426661 Color: 0
Size: 300806 Color: 4
Size: 272534 Color: 3

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 426809 Color: 0
Size: 293373 Color: 2
Size: 279819 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 426816 Color: 4
Size: 309864 Color: 3
Size: 263321 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 426829 Color: 4
Size: 309744 Color: 3
Size: 263428 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 426899 Color: 2
Size: 304801 Color: 4
Size: 268301 Color: 3

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 426993 Color: 2
Size: 289414 Color: 0
Size: 283594 Color: 1

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 427066 Color: 3
Size: 300600 Color: 4
Size: 272335 Color: 1

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 426995 Color: 0
Size: 317852 Color: 1
Size: 255154 Color: 2

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 427001 Color: 4
Size: 299905 Color: 3
Size: 273095 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 427137 Color: 4
Size: 322635 Color: 3
Size: 250229 Color: 2

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 427145 Color: 2
Size: 321365 Color: 4
Size: 251491 Color: 4

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 427251 Color: 1
Size: 319522 Color: 2
Size: 253228 Color: 3

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 427316 Color: 0
Size: 286858 Color: 0
Size: 285827 Color: 2

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 427413 Color: 3
Size: 288882 Color: 1
Size: 283706 Color: 1

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 427338 Color: 4
Size: 291414 Color: 4
Size: 281249 Color: 1

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 427514 Color: 3
Size: 297071 Color: 2
Size: 275416 Color: 4

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 427506 Color: 1
Size: 309364 Color: 1
Size: 263131 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 427518 Color: 0
Size: 287501 Color: 3
Size: 284982 Color: 2

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 427583 Color: 0
Size: 320252 Color: 1
Size: 252166 Color: 1

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 427611 Color: 2
Size: 319835 Color: 3
Size: 252555 Color: 1

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 427630 Color: 0
Size: 313501 Color: 4
Size: 258870 Color: 3

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 427641 Color: 1
Size: 310521 Color: 2
Size: 261839 Color: 2

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 427815 Color: 1
Size: 292509 Color: 3
Size: 279677 Color: 2

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 427983 Color: 1
Size: 304848 Color: 2
Size: 267170 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 428094 Color: 4
Size: 300574 Color: 4
Size: 271333 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 428100 Color: 3
Size: 315683 Color: 2
Size: 256218 Color: 1

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 428171 Color: 1
Size: 301896 Color: 4
Size: 269934 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 428190 Color: 1
Size: 309049 Color: 3
Size: 262762 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 428241 Color: 4
Size: 304024 Color: 0
Size: 267736 Color: 2

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 428257 Color: 4
Size: 289674 Color: 1
Size: 282070 Color: 3

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 428312 Color: 4
Size: 302018 Color: 0
Size: 269671 Color: 3

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 428317 Color: 0
Size: 294151 Color: 2
Size: 277533 Color: 4

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 428618 Color: 3
Size: 316739 Color: 4
Size: 254644 Color: 4

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 428595 Color: 0
Size: 305706 Color: 2
Size: 265700 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 428752 Color: 2
Size: 314058 Color: 0
Size: 257191 Color: 4

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 428998 Color: 0
Size: 286096 Color: 2
Size: 284907 Color: 3

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 428983 Color: 3
Size: 317687 Color: 0
Size: 253331 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 429103 Color: 0
Size: 319869 Color: 1
Size: 251029 Color: 1

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 429131 Color: 2
Size: 316408 Color: 1
Size: 254462 Color: 3

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 429142 Color: 2
Size: 306145 Color: 1
Size: 264714 Color: 3

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 429163 Color: 2
Size: 308473 Color: 1
Size: 262365 Color: 1

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 429236 Color: 4
Size: 309177 Color: 1
Size: 261588 Color: 3

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 429345 Color: 3
Size: 305423 Color: 0
Size: 265233 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 429317 Color: 4
Size: 315278 Color: 1
Size: 255406 Color: 1

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 429379 Color: 1
Size: 290231 Color: 2
Size: 280391 Color: 3

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 429411 Color: 2
Size: 311489 Color: 4
Size: 259101 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 429501 Color: 2
Size: 318121 Color: 4
Size: 252379 Color: 3

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 429538 Color: 3
Size: 305521 Color: 0
Size: 264942 Color: 1

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 429575 Color: 0
Size: 303520 Color: 1
Size: 266906 Color: 3

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 429657 Color: 2
Size: 300869 Color: 1
Size: 269475 Color: 1

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 429668 Color: 1
Size: 286713 Color: 3
Size: 283620 Color: 2

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 429846 Color: 3
Size: 293754 Color: 2
Size: 276401 Color: 2

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 429822 Color: 0
Size: 317182 Color: 1
Size: 252997 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 429833 Color: 2
Size: 291187 Color: 1
Size: 278981 Color: 3

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 429923 Color: 0
Size: 301186 Color: 1
Size: 268892 Color: 3

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 429985 Color: 1
Size: 307232 Color: 2
Size: 262784 Color: 2

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 430023 Color: 2
Size: 296375 Color: 1
Size: 273603 Color: 3

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 430056 Color: 1
Size: 287859 Color: 0
Size: 282086 Color: 2

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 430111 Color: 0
Size: 287390 Color: 3
Size: 282500 Color: 4

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 430210 Color: 1
Size: 303579 Color: 3
Size: 266212 Color: 2

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 430239 Color: 2
Size: 287128 Color: 4
Size: 282634 Color: 1

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 430243 Color: 0
Size: 307426 Color: 3
Size: 262332 Color: 1

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 430283 Color: 4
Size: 298454 Color: 2
Size: 271264 Color: 2

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 430348 Color: 3
Size: 319419 Color: 0
Size: 250234 Color: 4

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 430327 Color: 1
Size: 290791 Color: 2
Size: 278883 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 430357 Color: 0
Size: 298443 Color: 4
Size: 271201 Color: 3

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 430462 Color: 0
Size: 298099 Color: 3
Size: 271440 Color: 4

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 430680 Color: 1
Size: 289841 Color: 3
Size: 279480 Color: 2

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 430726 Color: 4
Size: 287708 Color: 1
Size: 281567 Color: 3

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 430733 Color: 0
Size: 303399 Color: 4
Size: 265869 Color: 3

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 430793 Color: 0
Size: 295259 Color: 2
Size: 273949 Color: 1

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 430798 Color: 4
Size: 288843 Color: 1
Size: 280360 Color: 3

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 430874 Color: 4
Size: 297259 Color: 1
Size: 271868 Color: 3

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 430970 Color: 3
Size: 292066 Color: 2
Size: 276965 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 430921 Color: 4
Size: 288444 Color: 0
Size: 280636 Color: 1

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 430978 Color: 4
Size: 304814 Color: 1
Size: 264209 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 431023 Color: 2
Size: 302887 Color: 1
Size: 266091 Color: 3

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 431220 Color: 1
Size: 288110 Color: 4
Size: 280671 Color: 0

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 431252 Color: 0
Size: 287187 Color: 4
Size: 281562 Color: 3

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 431332 Color: 3
Size: 305874 Color: 4
Size: 262795 Color: 2

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 431432 Color: 2
Size: 297572 Color: 4
Size: 270997 Color: 0

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 431585 Color: 1
Size: 286350 Color: 1
Size: 282066 Color: 2

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 431587 Color: 1
Size: 299483 Color: 3
Size: 268931 Color: 1

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 431615 Color: 4
Size: 302012 Color: 3
Size: 266374 Color: 1

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 431643 Color: 2
Size: 287876 Color: 0
Size: 280482 Color: 1

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 431709 Color: 4
Size: 311768 Color: 1
Size: 256524 Color: 1

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 431886 Color: 3
Size: 301377 Color: 1
Size: 266738 Color: 1

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 431895 Color: 3
Size: 306398 Color: 4
Size: 261708 Color: 2

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 431731 Color: 2
Size: 308371 Color: 0
Size: 259899 Color: 4

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 432055 Color: 3
Size: 316975 Color: 2
Size: 250971 Color: 2

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 432151 Color: 3
Size: 303841 Color: 1
Size: 264009 Color: 2

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 432057 Color: 2
Size: 314520 Color: 0
Size: 253424 Color: 1

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 432204 Color: 3
Size: 309583 Color: 1
Size: 258214 Color: 2

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 432161 Color: 1
Size: 295823 Color: 0
Size: 272017 Color: 4

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 432195 Color: 2
Size: 295849 Color: 1
Size: 271957 Color: 3

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 432415 Color: 2
Size: 295837 Color: 2
Size: 271749 Color: 4

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 432607 Color: 0
Size: 285805 Color: 3
Size: 281589 Color: 4

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 432665 Color: 1
Size: 290876 Color: 1
Size: 276460 Color: 4

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 432736 Color: 2
Size: 284012 Color: 0
Size: 283253 Color: 3

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 432874 Color: 4
Size: 287972 Color: 1
Size: 279155 Color: 3

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 432879 Color: 1
Size: 295847 Color: 4
Size: 271275 Color: 2

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 432910 Color: 2
Size: 299517 Color: 1
Size: 267574 Color: 4

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 432949 Color: 2
Size: 311462 Color: 3
Size: 255590 Color: 1

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 433026 Color: 3
Size: 287601 Color: 0
Size: 279374 Color: 4

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 433015 Color: 1
Size: 311335 Color: 2
Size: 255651 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 433120 Color: 3
Size: 308845 Color: 0
Size: 258036 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 433062 Color: 0
Size: 301284 Color: 4
Size: 265655 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 433091 Color: 4
Size: 307124 Color: 3
Size: 259786 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 433121 Color: 3
Size: 284030 Color: 0
Size: 282850 Color: 1

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 433102 Color: 1
Size: 286895 Color: 2
Size: 280004 Color: 4

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 433148 Color: 2
Size: 306017 Color: 4
Size: 260836 Color: 3

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 433194 Color: 2
Size: 300775 Color: 3
Size: 266032 Color: 1

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 433198 Color: 0
Size: 294600 Color: 1
Size: 272203 Color: 1

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 433263 Color: 2
Size: 303639 Color: 4
Size: 263099 Color: 4

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 433263 Color: 3
Size: 284756 Color: 1
Size: 281982 Color: 1

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 433512 Color: 3
Size: 301092 Color: 2
Size: 265397 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 433598 Color: 3
Size: 294465 Color: 2
Size: 271938 Color: 1

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 433460 Color: 0
Size: 292261 Color: 1
Size: 274280 Color: 2

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 433941 Color: 1
Size: 288035 Color: 4
Size: 278025 Color: 3

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 434115 Color: 3
Size: 307711 Color: 0
Size: 258175 Color: 2

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 434192 Color: 3
Size: 308627 Color: 4
Size: 257182 Color: 4

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 434075 Color: 0
Size: 291381 Color: 4
Size: 274545 Color: 2

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 434349 Color: 4
Size: 296492 Color: 2
Size: 269160 Color: 2

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 434362 Color: 4
Size: 290729 Color: 4
Size: 274910 Color: 3

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 434441 Color: 0
Size: 314646 Color: 1
Size: 250914 Color: 1

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 434633 Color: 2
Size: 286897 Color: 0
Size: 278471 Color: 1

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 434694 Color: 1
Size: 295027 Color: 3
Size: 270280 Color: 2

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 434701 Color: 1
Size: 288824 Color: 4
Size: 276476 Color: 3

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 434912 Color: 3
Size: 285254 Color: 0
Size: 279835 Color: 4

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 434840 Color: 0
Size: 300499 Color: 4
Size: 264662 Color: 3

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 434846 Color: 2
Size: 309704 Color: 4
Size: 255451 Color: 1

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 435038 Color: 3
Size: 290881 Color: 0
Size: 274082 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 434848 Color: 4
Size: 309498 Color: 4
Size: 255655 Color: 2

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 435166 Color: 3
Size: 286787 Color: 0
Size: 278048 Color: 1

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 435170 Color: 4
Size: 302559 Color: 3
Size: 262272 Color: 2

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 435234 Color: 0
Size: 309644 Color: 2
Size: 255123 Color: 2

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 435319 Color: 2
Size: 283346 Color: 2
Size: 281336 Color: 1

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 435329 Color: 2
Size: 302017 Color: 1
Size: 262655 Color: 3

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 435388 Color: 2
Size: 298026 Color: 3
Size: 266587 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 435393 Color: 2
Size: 293961 Color: 1
Size: 270647 Color: 3

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 435494 Color: 0
Size: 301327 Color: 2
Size: 263180 Color: 3

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 435508 Color: 2
Size: 289481 Color: 3
Size: 275012 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 435797 Color: 3
Size: 282497 Color: 2
Size: 281707 Color: 2

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 435684 Color: 1
Size: 298661 Color: 2
Size: 265656 Color: 2

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 435782 Color: 0
Size: 299321 Color: 3
Size: 264898 Color: 4

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 435867 Color: 4
Size: 288876 Color: 1
Size: 275258 Color: 2

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 436001 Color: 3
Size: 311615 Color: 2
Size: 252385 Color: 2

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 435968 Color: 0
Size: 298261 Color: 1
Size: 265772 Color: 1

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 435988 Color: 0
Size: 300976 Color: 2
Size: 263037 Color: 3

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 436025 Color: 2
Size: 293821 Color: 1
Size: 270155 Color: 3

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 436184 Color: 3
Size: 308067 Color: 2
Size: 255750 Color: 4

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 436161 Color: 0
Size: 308761 Color: 1
Size: 255079 Color: 4

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 436204 Color: 4
Size: 293771 Color: 2
Size: 270026 Color: 3

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 436319 Color: 4
Size: 283803 Color: 4
Size: 279879 Color: 2

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 436380 Color: 4
Size: 302346 Color: 3
Size: 261275 Color: 1

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 436486 Color: 0
Size: 297581 Color: 4
Size: 265934 Color: 1

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 436501 Color: 2
Size: 304490 Color: 3
Size: 259010 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 436596 Color: 4
Size: 294322 Color: 2
Size: 269083 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 436640 Color: 0
Size: 307970 Color: 1
Size: 255391 Color: 3

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 436647 Color: 0
Size: 303471 Color: 1
Size: 259883 Color: 4

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 436734 Color: 0
Size: 305074 Color: 4
Size: 258193 Color: 1

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 436774 Color: 0
Size: 304061 Color: 4
Size: 259166 Color: 2

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 436808 Color: 3
Size: 295166 Color: 0
Size: 268027 Color: 2

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 436865 Color: 0
Size: 289000 Color: 4
Size: 274136 Color: 1

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 436882 Color: 4
Size: 312672 Color: 3
Size: 250447 Color: 1

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 436912 Color: 4
Size: 286630 Color: 0
Size: 276459 Color: 4

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 436953 Color: 0
Size: 290683 Color: 3
Size: 272365 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 436965 Color: 1
Size: 291457 Color: 0
Size: 271579 Color: 1

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 437100 Color: 4
Size: 301078 Color: 4
Size: 261823 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 437161 Color: 4
Size: 299522 Color: 2
Size: 263318 Color: 4

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 437200 Color: 0
Size: 296062 Color: 3
Size: 266739 Color: 2

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 437272 Color: 4
Size: 308734 Color: 1
Size: 253995 Color: 1

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 437311 Color: 4
Size: 284663 Color: 1
Size: 278027 Color: 3

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 437286 Color: 3
Size: 294829 Color: 2
Size: 267886 Color: 4

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 437313 Color: 1
Size: 298340 Color: 4
Size: 264348 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 437515 Color: 4
Size: 294374 Color: 2
Size: 268112 Color: 4

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 437776 Color: 1
Size: 295194 Color: 0
Size: 267031 Color: 4

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 437790 Color: 1
Size: 289141 Color: 0
Size: 273070 Color: 3

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 437856 Color: 1
Size: 294404 Color: 4
Size: 267741 Color: 4

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 437925 Color: 0
Size: 282877 Color: 3
Size: 279199 Color: 1

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 437989 Color: 2
Size: 311932 Color: 3
Size: 250080 Color: 2

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 438068 Color: 3
Size: 288490 Color: 4
Size: 273443 Color: 4

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 438051 Color: 1
Size: 309418 Color: 2
Size: 252532 Color: 2

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 438059 Color: 1
Size: 299019 Color: 4
Size: 262923 Color: 3

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 438089 Color: 3
Size: 285963 Color: 1
Size: 275949 Color: 4

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 438200 Color: 3
Size: 294057 Color: 1
Size: 267744 Color: 1

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 438279 Color: 4
Size: 299512 Color: 0
Size: 262210 Color: 1

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 438379 Color: 4
Size: 283674 Color: 3
Size: 277948 Color: 2

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 438460 Color: 1
Size: 294629 Color: 1
Size: 266912 Color: 3

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 438510 Color: 4
Size: 287980 Color: 2
Size: 273511 Color: 2

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 438547 Color: 0
Size: 300231 Color: 4
Size: 261223 Color: 3

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 438587 Color: 4
Size: 302444 Color: 2
Size: 258970 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 438753 Color: 4
Size: 301937 Color: 3
Size: 259311 Color: 1

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 438764 Color: 1
Size: 297738 Color: 3
Size: 263499 Color: 1

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 438813 Color: 1
Size: 299355 Color: 4
Size: 261833 Color: 4

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 438840 Color: 2
Size: 299796 Color: 3
Size: 261365 Color: 1

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 438855 Color: 0
Size: 299092 Color: 0
Size: 262054 Color: 4

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 438873 Color: 2
Size: 308438 Color: 4
Size: 252690 Color: 3

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 438971 Color: 3
Size: 307943 Color: 1
Size: 253087 Color: 2

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 438909 Color: 2
Size: 300268 Color: 2
Size: 260824 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 438940 Color: 4
Size: 285021 Color: 3
Size: 276040 Color: 2

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 439103 Color: 4
Size: 309683 Color: 3
Size: 251215 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 439226 Color: 4
Size: 298220 Color: 3
Size: 262555 Color: 4

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 439293 Color: 2
Size: 298433 Color: 1
Size: 262275 Color: 3

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 439321 Color: 1
Size: 304072 Color: 2
Size: 256608 Color: 1

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 439322 Color: 0
Size: 293610 Color: 0
Size: 267069 Color: 3

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 439377 Color: 2
Size: 289732 Color: 4
Size: 270892 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 439434 Color: 4
Size: 307956 Color: 1
Size: 252611 Color: 3

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 439454 Color: 1
Size: 280597 Color: 4
Size: 279950 Color: 1

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 439480 Color: 0
Size: 307473 Color: 3
Size: 253048 Color: 2

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 439481 Color: 2
Size: 306700 Color: 0
Size: 253820 Color: 1

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 439502 Color: 2
Size: 289839 Color: 3
Size: 270660 Color: 2

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 439517 Color: 4
Size: 287802 Color: 0
Size: 272682 Color: 4

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 439601 Color: 2
Size: 305278 Color: 3
Size: 255122 Color: 4

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 439617 Color: 1
Size: 283700 Color: 2
Size: 276684 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 439733 Color: 0
Size: 305682 Color: 2
Size: 254586 Color: 4

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 439797 Color: 4
Size: 308401 Color: 0
Size: 251803 Color: 3

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 439871 Color: 1
Size: 297680 Color: 0
Size: 262450 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 439883 Color: 4
Size: 283524 Color: 3
Size: 276594 Color: 2

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 439947 Color: 1
Size: 307514 Color: 2
Size: 252540 Color: 2

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 440023 Color: 1
Size: 305045 Color: 2
Size: 254933 Color: 3

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 440026 Color: 4
Size: 291932 Color: 4
Size: 268043 Color: 1

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 440064 Color: 0
Size: 292240 Color: 3
Size: 267697 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 440178 Color: 3
Size: 308160 Color: 4
Size: 251663 Color: 1

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 440191 Color: 2
Size: 282857 Color: 0
Size: 276953 Color: 4

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 440264 Color: 0
Size: 309600 Color: 2
Size: 250137 Color: 1

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 440409 Color: 3
Size: 300508 Color: 0
Size: 259084 Color: 1

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 440435 Color: 3
Size: 280495 Color: 1
Size: 279071 Color: 4

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 440420 Color: 4
Size: 283109 Color: 0
Size: 276472 Color: 3

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 440730 Color: 3
Size: 297042 Color: 4
Size: 262229 Color: 2

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 440652 Color: 0
Size: 304939 Color: 4
Size: 254410 Color: 4

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 440815 Color: 3
Size: 285218 Color: 1
Size: 273968 Color: 2

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 440854 Color: 3
Size: 299107 Color: 1
Size: 260040 Color: 1

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 440805 Color: 2
Size: 298236 Color: 1
Size: 260960 Color: 4

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 440859 Color: 2
Size: 308906 Color: 3
Size: 250236 Color: 1

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 440994 Color: 0
Size: 307314 Color: 3
Size: 251693 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 441077 Color: 0
Size: 281624 Color: 0
Size: 277300 Color: 3

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 441321 Color: 4
Size: 290443 Color: 1
Size: 268237 Color: 4

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 441323 Color: 0
Size: 307417 Color: 3
Size: 251261 Color: 4

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 441384 Color: 1
Size: 291010 Color: 1
Size: 267607 Color: 2

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 441393 Color: 0
Size: 308601 Color: 3
Size: 250007 Color: 4

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 441455 Color: 3
Size: 301210 Color: 0
Size: 257336 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 441457 Color: 1
Size: 279434 Color: 2
Size: 279110 Color: 2

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 441522 Color: 0
Size: 298322 Color: 4
Size: 260157 Color: 4

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 441645 Color: 4
Size: 290311 Color: 0
Size: 268045 Color: 1

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 441708 Color: 2
Size: 280160 Color: 1
Size: 278133 Color: 3

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 441694 Color: 3
Size: 279675 Color: 4
Size: 278632 Color: 1

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 441710 Color: 0
Size: 304734 Color: 1
Size: 253557 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 441742 Color: 2
Size: 280164 Color: 3
Size: 278095 Color: 4

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 441854 Color: 4
Size: 298924 Color: 1
Size: 259223 Color: 3

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 441931 Color: 0
Size: 300743 Color: 3
Size: 257327 Color: 3

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 442054 Color: 0
Size: 303448 Color: 1
Size: 254499 Color: 4

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 442090 Color: 0
Size: 301746 Color: 4
Size: 256165 Color: 3

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 442097 Color: 4
Size: 300125 Color: 2
Size: 257779 Color: 2

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 442185 Color: 0
Size: 302483 Color: 3
Size: 255333 Color: 4

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 442186 Color: 2
Size: 287742 Color: 4
Size: 270073 Color: 3

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 442236 Color: 4
Size: 305674 Color: 2
Size: 252091 Color: 4

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 442258 Color: 1
Size: 287672 Color: 4
Size: 270071 Color: 3

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 442413 Color: 0
Size: 282216 Color: 2
Size: 275372 Color: 1

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 442453 Color: 0
Size: 294372 Color: 4
Size: 263176 Color: 3

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 442592 Color: 4
Size: 299555 Color: 4
Size: 257854 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 442617 Color: 2
Size: 307316 Color: 4
Size: 250068 Color: 3

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 442625 Color: 1
Size: 278735 Color: 3
Size: 278641 Color: 4

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 442777 Color: 0
Size: 289008 Color: 4
Size: 268216 Color: 4

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 443082 Color: 3
Size: 280333 Color: 2
Size: 276586 Color: 2

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 443178 Color: 0
Size: 289891 Color: 0
Size: 266932 Color: 4

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 443220 Color: 3
Size: 279511 Color: 1
Size: 277270 Color: 2

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 443273 Color: 2
Size: 283820 Color: 1
Size: 272908 Color: 1

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 443292 Color: 0
Size: 295606 Color: 3
Size: 261103 Color: 2

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 443378 Color: 4
Size: 282099 Color: 2
Size: 274524 Color: 4

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 443434 Color: 4
Size: 285124 Color: 1
Size: 271443 Color: 3

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 443439 Color: 0
Size: 294225 Color: 2
Size: 262337 Color: 2

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 443599 Color: 0
Size: 285983 Color: 2
Size: 270419 Color: 3

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 443640 Color: 2
Size: 281076 Color: 3
Size: 275285 Color: 4

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 443665 Color: 0
Size: 300056 Color: 1
Size: 256280 Color: 1

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 443805 Color: 1
Size: 287061 Color: 4
Size: 269135 Color: 3

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 443972 Color: 3
Size: 305082 Color: 1
Size: 250947 Color: 4

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 444016 Color: 3
Size: 300985 Color: 4
Size: 255000 Color: 2

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 443894 Color: 4
Size: 293178 Color: 1
Size: 262929 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 443939 Color: 0
Size: 305949 Color: 3
Size: 250113 Color: 1

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 444033 Color: 3
Size: 291570 Color: 0
Size: 264398 Color: 4

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 444163 Color: 2
Size: 301352 Color: 0
Size: 254486 Color: 1

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 444180 Color: 0
Size: 289006 Color: 1
Size: 266815 Color: 3

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 444238 Color: 3
Size: 284079 Color: 1
Size: 271684 Color: 1

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 444324 Color: 2
Size: 295193 Color: 3
Size: 260484 Color: 4

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 444325 Color: 1
Size: 281528 Color: 3
Size: 274148 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 444351 Color: 1
Size: 301579 Color: 2
Size: 254071 Color: 1

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 444543 Color: 2
Size: 281294 Color: 2
Size: 274164 Color: 3

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 444544 Color: 0
Size: 280604 Color: 4
Size: 274853 Color: 4

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 444675 Color: 1
Size: 290357 Color: 2
Size: 264969 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 444696 Color: 1
Size: 289254 Color: 2
Size: 266051 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 444754 Color: 1
Size: 284776 Color: 3
Size: 270471 Color: 2

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 444916 Color: 3
Size: 302046 Color: 0
Size: 253039 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 444975 Color: 4
Size: 283521 Color: 4
Size: 271505 Color: 3

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 445113 Color: 2
Size: 289487 Color: 0
Size: 265401 Color: 3

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 445213 Color: 2
Size: 278134 Color: 1
Size: 276654 Color: 4

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 445243 Color: 4
Size: 287858 Color: 0
Size: 266900 Color: 3

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 445214 Color: 3
Size: 288833 Color: 0
Size: 265954 Color: 1

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 445275 Color: 2
Size: 302162 Color: 2
Size: 252564 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 445330 Color: 1
Size: 299963 Color: 0
Size: 254708 Color: 3

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 445429 Color: 2
Size: 301995 Color: 0
Size: 252577 Color: 1

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 445513 Color: 3
Size: 282480 Color: 1
Size: 272008 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 445479 Color: 2
Size: 297719 Color: 0
Size: 256803 Color: 4

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 445569 Color: 3
Size: 294121 Color: 1
Size: 260311 Color: 1

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 445646 Color: 4
Size: 296863 Color: 0
Size: 257492 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 445698 Color: 0
Size: 295620 Color: 1
Size: 258683 Color: 3

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 445822 Color: 2
Size: 288048 Color: 2
Size: 266131 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 445880 Color: 1
Size: 285269 Color: 0
Size: 268852 Color: 2

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 445934 Color: 1
Size: 297917 Color: 1
Size: 256150 Color: 4

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 446121 Color: 0
Size: 295496 Color: 3
Size: 258384 Color: 4

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 446302 Color: 0
Size: 301396 Color: 1
Size: 252303 Color: 4

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 446322 Color: 1
Size: 298853 Color: 3
Size: 254826 Color: 4

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 446330 Color: 4
Size: 283930 Color: 2
Size: 269741 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 446405 Color: 1
Size: 284353 Color: 2
Size: 269243 Color: 3

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 446426 Color: 2
Size: 288834 Color: 1
Size: 264741 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 446434 Color: 4
Size: 283923 Color: 0
Size: 269644 Color: 3

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 446508 Color: 0
Size: 291649 Color: 1
Size: 261844 Color: 4

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 446522 Color: 1
Size: 293153 Color: 0
Size: 260326 Color: 3

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 446532 Color: 0
Size: 286632 Color: 2
Size: 266837 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 446728 Color: 4
Size: 284192 Color: 3
Size: 269081 Color: 1

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 446739 Color: 1
Size: 300179 Color: 4
Size: 253083 Color: 3

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 446805 Color: 0
Size: 283275 Color: 1
Size: 269921 Color: 1

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 446856 Color: 1
Size: 284993 Color: 3
Size: 268152 Color: 4

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 446868 Color: 3
Size: 286515 Color: 4
Size: 266618 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 446917 Color: 0
Size: 280444 Color: 4
Size: 272640 Color: 4

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 403019 Color: 1
Size: 341572 Color: 3
Size: 255410 Color: 2

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 447028 Color: 4
Size: 288226 Color: 1
Size: 264747 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 447267 Color: 2
Size: 277518 Color: 0
Size: 275216 Color: 4

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 447291 Color: 4
Size: 293998 Color: 0
Size: 258712 Color: 3

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 447311 Color: 0
Size: 285231 Color: 4
Size: 267459 Color: 3

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 447461 Color: 4
Size: 292867 Color: 3
Size: 259673 Color: 4

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 447691 Color: 2
Size: 285658 Color: 2
Size: 266652 Color: 4

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 447780 Color: 3
Size: 288305 Color: 2
Size: 263916 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 447730 Color: 4
Size: 301529 Color: 3
Size: 250742 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 447932 Color: 2
Size: 290397 Color: 4
Size: 261672 Color: 3

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 448038 Color: 4
Size: 286765 Color: 3
Size: 265198 Color: 2

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 448228 Color: 3
Size: 300230 Color: 4
Size: 251543 Color: 2

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 448178 Color: 0
Size: 280095 Color: 2
Size: 271728 Color: 1

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 448497 Color: 0
Size: 300275 Color: 4
Size: 251229 Color: 4

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 448620 Color: 3
Size: 291262 Color: 1
Size: 260119 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 448565 Color: 4
Size: 278394 Color: 1
Size: 273042 Color: 2

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 448590 Color: 2
Size: 294644 Color: 3
Size: 256767 Color: 1

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 448667 Color: 2
Size: 278285 Color: 3
Size: 273049 Color: 1

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 448668 Color: 1
Size: 292214 Color: 0
Size: 259119 Color: 2

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 448909 Color: 3
Size: 291404 Color: 4
Size: 259688 Color: 1

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 449130 Color: 2
Size: 294635 Color: 3
Size: 256236 Color: 2

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 449153 Color: 1
Size: 292417 Color: 1
Size: 258431 Color: 2

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 449218 Color: 3
Size: 276392 Color: 4
Size: 274391 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 449163 Color: 4
Size: 276185 Color: 0
Size: 274653 Color: 1

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 449169 Color: 2
Size: 296392 Color: 1
Size: 254440 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 449629 Color: 3
Size: 278667 Color: 4
Size: 271705 Color: 1

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 449550 Color: 2
Size: 289575 Color: 3
Size: 260876 Color: 1

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 449714 Color: 2
Size: 297670 Color: 2
Size: 252617 Color: 1

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 449838 Color: 0
Size: 280173 Color: 4
Size: 269990 Color: 2

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 450060 Color: 3
Size: 278582 Color: 4
Size: 271359 Color: 2

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 450111 Color: 4
Size: 279089 Color: 2
Size: 270801 Color: 3

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 450171 Color: 2
Size: 275500 Color: 4
Size: 274330 Color: 2

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 450289 Color: 0
Size: 275662 Color: 3
Size: 274050 Color: 4

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 450306 Color: 0
Size: 280253 Color: 2
Size: 269442 Color: 1

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 450313 Color: 4
Size: 290926 Color: 0
Size: 258762 Color: 4

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 450358 Color: 1
Size: 285142 Color: 3
Size: 264501 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 450397 Color: 1
Size: 275886 Color: 0
Size: 273718 Color: 2

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 450473 Color: 1
Size: 292719 Color: 3
Size: 256809 Color: 4

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 450549 Color: 2
Size: 279665 Color: 3
Size: 269787 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 450559 Color: 0
Size: 283527 Color: 2
Size: 265915 Color: 2

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 450603 Color: 4
Size: 288848 Color: 3
Size: 260550 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 450670 Color: 1
Size: 292846 Color: 1
Size: 256485 Color: 4

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 450750 Color: 1
Size: 297229 Color: 3
Size: 252022 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 450777 Color: 4
Size: 292283 Color: 3
Size: 256941 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 450799 Color: 1
Size: 295798 Color: 0
Size: 253404 Color: 1

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 450926 Color: 4
Size: 274939 Color: 2
Size: 274136 Color: 2

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 451010 Color: 2
Size: 292197 Color: 3
Size: 256794 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 451178 Color: 4
Size: 288084 Color: 3
Size: 260739 Color: 4

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 451205 Color: 1
Size: 290041 Color: 4
Size: 258755 Color: 2

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 451209 Color: 1
Size: 281629 Color: 4
Size: 267163 Color: 3

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 451239 Color: 1
Size: 286851 Color: 4
Size: 261911 Color: 4

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 451250 Color: 2
Size: 285446 Color: 0
Size: 263305 Color: 1

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 451437 Color: 3
Size: 280640 Color: 4
Size: 267924 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 451415 Color: 0
Size: 289971 Color: 4
Size: 258615 Color: 1

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 451440 Color: 4
Size: 288435 Color: 1
Size: 260126 Color: 3

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 451459 Color: 2
Size: 275057 Color: 1
Size: 273485 Color: 2

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 451555 Color: 1
Size: 278850 Color: 4
Size: 269596 Color: 3

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 451582 Color: 0
Size: 281135 Color: 2
Size: 267284 Color: 3

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 451629 Color: 2
Size: 283224 Color: 1
Size: 265148 Color: 4

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 451869 Color: 3
Size: 278797 Color: 2
Size: 269335 Color: 2

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 451719 Color: 1
Size: 286109 Color: 1
Size: 262173 Color: 2

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 451775 Color: 0
Size: 274391 Color: 2
Size: 273835 Color: 4

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 452097 Color: 3
Size: 281096 Color: 1
Size: 266808 Color: 2

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 452078 Color: 1
Size: 290739 Color: 0
Size: 257184 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 452335 Color: 3
Size: 283414 Color: 2
Size: 264252 Color: 4

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 452214 Color: 1
Size: 281792 Color: 2
Size: 265995 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 452219 Color: 2
Size: 285300 Color: 1
Size: 262482 Color: 2

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 452431 Color: 3
Size: 290123 Color: 0
Size: 257447 Color: 2

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 452464 Color: 3
Size: 284660 Color: 4
Size: 262877 Color: 2

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 452395 Color: 2
Size: 286423 Color: 4
Size: 261183 Color: 4

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 452472 Color: 3
Size: 289780 Color: 0
Size: 257749 Color: 2

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 452503 Color: 0
Size: 290939 Color: 2
Size: 256559 Color: 3

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 452506 Color: 4
Size: 278647 Color: 2
Size: 268848 Color: 2

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 452594 Color: 2
Size: 277087 Color: 2
Size: 270320 Color: 4

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 452648 Color: 0
Size: 278232 Color: 4
Size: 269121 Color: 3

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 452661 Color: 1
Size: 285428 Color: 3
Size: 261912 Color: 2

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 452888 Color: 2
Size: 282912 Color: 1
Size: 264201 Color: 4

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 453020 Color: 3
Size: 277301 Color: 4
Size: 269680 Color: 0

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 452981 Color: 4
Size: 295208 Color: 1
Size: 251812 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 453004 Color: 0
Size: 283318 Color: 3
Size: 263679 Color: 2

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 453086 Color: 3
Size: 295171 Color: 1
Size: 251744 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 453014 Color: 2
Size: 292000 Color: 0
Size: 254987 Color: 1

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 453089 Color: 1
Size: 291798 Color: 0
Size: 255114 Color: 4

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 453193 Color: 4
Size: 279617 Color: 1
Size: 267191 Color: 3

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 453235 Color: 0
Size: 285827 Color: 4
Size: 260939 Color: 3

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 453255 Color: 1
Size: 276804 Color: 1
Size: 269942 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 453315 Color: 1
Size: 279875 Color: 2
Size: 266811 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 453368 Color: 0
Size: 281895 Color: 3
Size: 264738 Color: 2

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 453517 Color: 3
Size: 294991 Color: 1
Size: 251493 Color: 1

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 453426 Color: 2
Size: 279225 Color: 4
Size: 267350 Color: 2

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 453725 Color: 3
Size: 293553 Color: 4
Size: 252723 Color: 1

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 453580 Color: 1
Size: 289921 Color: 0
Size: 256500 Color: 2

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 453609 Color: 0
Size: 295503 Color: 1
Size: 250889 Color: 1

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 453869 Color: 3
Size: 284374 Color: 0
Size: 261758 Color: 0

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 453897 Color: 3
Size: 278780 Color: 4
Size: 267324 Color: 2

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 453893 Color: 1
Size: 282984 Color: 2
Size: 263124 Color: 3

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 453992 Color: 1
Size: 294228 Color: 2
Size: 251781 Color: 1

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 454020 Color: 2
Size: 279465 Color: 0
Size: 266516 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 454176 Color: 3
Size: 287196 Color: 1
Size: 258629 Color: 2

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 454158 Color: 1
Size: 292433 Color: 3
Size: 253410 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 454218 Color: 1
Size: 285630 Color: 2
Size: 260153 Color: 3

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 454352 Color: 3
Size: 289314 Color: 4
Size: 256335 Color: 4

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 454242 Color: 1
Size: 276550 Color: 0
Size: 269209 Color: 4

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 454408 Color: 1
Size: 275080 Color: 2
Size: 270513 Color: 3

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 454516 Color: 3
Size: 276483 Color: 2
Size: 269002 Color: 2

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 454870 Color: 3
Size: 279762 Color: 1
Size: 265369 Color: 4

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 454763 Color: 1
Size: 275703 Color: 4
Size: 269535 Color: 2

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 454875 Color: 1
Size: 287379 Color: 0
Size: 257747 Color: 3

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 454924 Color: 3
Size: 289540 Color: 1
Size: 255537 Color: 1

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 454901 Color: 2
Size: 286032 Color: 0
Size: 259068 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 454993 Color: 3
Size: 279830 Color: 4
Size: 265178 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 454950 Color: 1
Size: 284109 Color: 2
Size: 260942 Color: 0

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 455461 Color: 3
Size: 285204 Color: 0
Size: 259336 Color: 1

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 455386 Color: 1
Size: 285357 Color: 4
Size: 259258 Color: 1

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 455523 Color: 1
Size: 279261 Color: 3
Size: 265217 Color: 4

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 455551 Color: 2
Size: 282004 Color: 3
Size: 262446 Color: 1

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 455595 Color: 1
Size: 276294 Color: 1
Size: 268112 Color: 2

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 455582 Color: 4
Size: 288948 Color: 1
Size: 255471 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 455595 Color: 3
Size: 290913 Color: 0
Size: 253493 Color: 3

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 455727 Color: 3
Size: 289423 Color: 2
Size: 254851 Color: 4

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 455708 Color: 4
Size: 288235 Color: 4
Size: 256058 Color: 2

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 455793 Color: 1
Size: 292460 Color: 2
Size: 251748 Color: 3

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 455816 Color: 4
Size: 294070 Color: 1
Size: 250115 Color: 1

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 455846 Color: 0
Size: 292906 Color: 3
Size: 251249 Color: 4

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 455880 Color: 0
Size: 292543 Color: 1
Size: 251578 Color: 3

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 455975 Color: 4
Size: 290428 Color: 1
Size: 253598 Color: 3

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 456098 Color: 2
Size: 283790 Color: 4
Size: 260113 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 456180 Color: 3
Size: 282542 Color: 2
Size: 261279 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 456216 Color: 4
Size: 286599 Color: 0
Size: 257186 Color: 2

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 456292 Color: 0
Size: 280948 Color: 1
Size: 262761 Color: 3

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 456308 Color: 1
Size: 293348 Color: 1
Size: 250345 Color: 3

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 456404 Color: 1
Size: 286745 Color: 0
Size: 256852 Color: 2

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 456454 Color: 3
Size: 277180 Color: 0
Size: 266367 Color: 1

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 456438 Color: 2
Size: 272117 Color: 1
Size: 271446 Color: 1

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 456510 Color: 2
Size: 287263 Color: 4
Size: 256228 Color: 4

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 456710 Color: 1
Size: 282290 Color: 3
Size: 261001 Color: 2

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 456759 Color: 3
Size: 287725 Color: 0
Size: 255517 Color: 2

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 456761 Color: 0
Size: 290182 Color: 2
Size: 253058 Color: 2

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 456852 Color: 3
Size: 287950 Color: 4
Size: 255199 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 456806 Color: 2
Size: 285292 Color: 4
Size: 257903 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 456840 Color: 0
Size: 279467 Color: 4
Size: 263694 Color: 3

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 456916 Color: 2
Size: 278852 Color: 1
Size: 264233 Color: 1

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 456951 Color: 1
Size: 283330 Color: 0
Size: 259720 Color: 3

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 456989 Color: 2
Size: 273492 Color: 1
Size: 269520 Color: 1

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 457002 Color: 0
Size: 281089 Color: 2
Size: 261910 Color: 3

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 457015 Color: 3
Size: 289141 Color: 4
Size: 253845 Color: 4

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 457198 Color: 1
Size: 272086 Color: 4
Size: 270717 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 457243 Color: 1
Size: 286573 Color: 0
Size: 256185 Color: 3

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 457280 Color: 1
Size: 273676 Color: 4
Size: 269045 Color: 3

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 457310 Color: 1
Size: 290006 Color: 0
Size: 252685 Color: 3

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 457454 Color: 3
Size: 273296 Color: 0
Size: 269251 Color: 2

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 457342 Color: 4
Size: 281298 Color: 1
Size: 261361 Color: 0

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 457498 Color: 3
Size: 288496 Color: 4
Size: 254007 Color: 1

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 457569 Color: 1
Size: 291905 Color: 3
Size: 250527 Color: 2

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 457755 Color: 1
Size: 284731 Color: 3
Size: 257515 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 457780 Color: 4
Size: 286282 Color: 2
Size: 255939 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 458025 Color: 3
Size: 276007 Color: 2
Size: 265969 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 458021 Color: 4
Size: 278858 Color: 3
Size: 263122 Color: 4

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 458177 Color: 2
Size: 278592 Color: 0
Size: 263232 Color: 1

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 458210 Color: 2
Size: 285530 Color: 4
Size: 256261 Color: 3

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 458469 Color: 3
Size: 285665 Color: 4
Size: 255867 Color: 1

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 458416 Color: 1
Size: 286605 Color: 1
Size: 254980 Color: 3

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 458539 Color: 3
Size: 285852 Color: 2
Size: 255610 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 458581 Color: 2
Size: 279817 Color: 0
Size: 261603 Color: 2

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 458585 Color: 0
Size: 278294 Color: 1
Size: 263122 Color: 1

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 458804 Color: 2
Size: 282466 Color: 1
Size: 258731 Color: 3

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 458894 Color: 3
Size: 276434 Color: 2
Size: 264673 Color: 2

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 458837 Color: 0
Size: 285186 Color: 4
Size: 255978 Color: 1

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 459067 Color: 3
Size: 274925 Color: 0
Size: 266009 Color: 4

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 459382 Color: 1
Size: 290159 Color: 4
Size: 250460 Color: 4

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 459542 Color: 2
Size: 278372 Color: 4
Size: 262087 Color: 3

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 459560 Color: 4
Size: 282382 Color: 4
Size: 258059 Color: 3

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 459654 Color: 2
Size: 273281 Color: 3
Size: 267066 Color: 1

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 459765 Color: 1
Size: 273052 Color: 3
Size: 267184 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 460152 Color: 2
Size: 287344 Color: 4
Size: 252505 Color: 2

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 460338 Color: 4
Size: 284657 Color: 2
Size: 255006 Color: 4

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 460590 Color: 3
Size: 278992 Color: 4
Size: 260419 Color: 4

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 460644 Color: 4
Size: 280000 Color: 0
Size: 259357 Color: 3

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 460624 Color: 3
Size: 275784 Color: 4
Size: 263593 Color: 1

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 460777 Color: 2
Size: 284548 Color: 4
Size: 254676 Color: 3

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 460867 Color: 3
Size: 269597 Color: 1
Size: 269537 Color: 1

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 460802 Color: 0
Size: 282709 Color: 4
Size: 256490 Color: 1

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 460929 Color: 4
Size: 278524 Color: 3
Size: 260548 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 460947 Color: 2
Size: 281526 Color: 1
Size: 257528 Color: 4

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 461056 Color: 4
Size: 279486 Color: 2
Size: 259459 Color: 3

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 461065 Color: 2
Size: 269964 Color: 1
Size: 268972 Color: 3

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 461165 Color: 2
Size: 280280 Color: 0
Size: 258556 Color: 3

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 461295 Color: 3
Size: 281510 Color: 2
Size: 257196 Color: 1

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 461508 Color: 3
Size: 272116 Color: 1
Size: 266377 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 461478 Color: 1
Size: 272087 Color: 0
Size: 266436 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 461526 Color: 4
Size: 276696 Color: 1
Size: 261779 Color: 3

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 461543 Color: 2
Size: 284780 Color: 0
Size: 253678 Color: 3

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 461550 Color: 1
Size: 279749 Color: 4
Size: 258702 Color: 4

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 461761 Color: 2
Size: 283712 Color: 2
Size: 254528 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 461788 Color: 4
Size: 281451 Color: 3
Size: 256762 Color: 1

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 461843 Color: 4
Size: 273485 Color: 0
Size: 264673 Color: 4

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 461878 Color: 0
Size: 281444 Color: 1
Size: 256679 Color: 1

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 462143 Color: 3
Size: 273366 Color: 0
Size: 264492 Color: 1

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 462564 Color: 1
Size: 278789 Color: 3
Size: 258648 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 462691 Color: 4
Size: 284646 Color: 4
Size: 252664 Color: 2

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 462819 Color: 2
Size: 284643 Color: 4
Size: 252539 Color: 1

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 462931 Color: 3
Size: 274876 Color: 2
Size: 262194 Color: 1

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 462894 Color: 4
Size: 281601 Color: 1
Size: 255506 Color: 1

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 462921 Color: 4
Size: 276152 Color: 3
Size: 260928 Color: 1

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 462949 Color: 1
Size: 275153 Color: 0
Size: 261899 Color: 1

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 463112 Color: 3
Size: 283460 Color: 0
Size: 253429 Color: 1

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 463197 Color: 4
Size: 273161 Color: 3
Size: 263643 Color: 2

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 463219 Color: 0
Size: 269888 Color: 2
Size: 266894 Color: 1

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 463303 Color: 4
Size: 282940 Color: 3
Size: 253758 Color: 2

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 463356 Color: 4
Size: 269945 Color: 2
Size: 266700 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 463558 Color: 4
Size: 270200 Color: 0
Size: 266243 Color: 3

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 463599 Color: 2
Size: 283463 Color: 0
Size: 252939 Color: 4

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 463620 Color: 0
Size: 286153 Color: 4
Size: 250228 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 463637 Color: 4
Size: 277183 Color: 3
Size: 259181 Color: 0

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 463646 Color: 1
Size: 281381 Color: 2
Size: 254974 Color: 2

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 463698 Color: 3
Size: 276374 Color: 0
Size: 259929 Color: 1

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 463771 Color: 0
Size: 286195 Color: 2
Size: 250035 Color: 2

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 463806 Color: 0
Size: 274758 Color: 3
Size: 261437 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 463831 Color: 2
Size: 283171 Color: 3
Size: 252999 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 463882 Color: 2
Size: 284301 Color: 4
Size: 251818 Color: 1

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 464024 Color: 0
Size: 272974 Color: 4
Size: 263003 Color: 4

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 464053 Color: 0
Size: 271756 Color: 0
Size: 264192 Color: 3

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 464060 Color: 3
Size: 275507 Color: 1
Size: 260434 Color: 1

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 464079 Color: 1
Size: 275764 Color: 0
Size: 260158 Color: 2

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 464194 Color: 3
Size: 271652 Color: 4
Size: 264155 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 464114 Color: 1
Size: 284482 Color: 0
Size: 251405 Color: 4

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 464313 Color: 1
Size: 274176 Color: 3
Size: 261512 Color: 4

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 464404 Color: 1
Size: 274492 Color: 0
Size: 261105 Color: 3

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 464427 Color: 2
Size: 276061 Color: 0
Size: 259513 Color: 4

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 464445 Color: 1
Size: 281644 Color: 3
Size: 253912 Color: 1

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 464474 Color: 0
Size: 283377 Color: 2
Size: 252150 Color: 3

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 464803 Color: 2
Size: 283991 Color: 3
Size: 251207 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 465086 Color: 3
Size: 275415 Color: 1
Size: 259500 Color: 4

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 464958 Color: 2
Size: 270802 Color: 4
Size: 264241 Color: 2

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 465163 Color: 3
Size: 275838 Color: 2
Size: 259000 Color: 4

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 465178 Color: 3
Size: 273281 Color: 1
Size: 261542 Color: 2

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 465214 Color: 1
Size: 283732 Color: 0
Size: 251055 Color: 1

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 465309 Color: 0
Size: 277309 Color: 3
Size: 257383 Color: 2

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 465551 Color: 4
Size: 277026 Color: 2
Size: 257424 Color: 3

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 465591 Color: 2
Size: 269699 Color: 1
Size: 264711 Color: 4

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 465910 Color: 1
Size: 283229 Color: 4
Size: 250862 Color: 4

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 466081 Color: 3
Size: 281922 Color: 1
Size: 251998 Color: 4

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 466142 Color: 2
Size: 269910 Color: 4
Size: 263949 Color: 3

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 466167 Color: 2
Size: 282352 Color: 4
Size: 251482 Color: 1

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 466217 Color: 2
Size: 278112 Color: 0
Size: 255672 Color: 3

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 466319 Color: 2
Size: 270399 Color: 0
Size: 263283 Color: 4

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 466324 Color: 4
Size: 269959 Color: 1
Size: 263718 Color: 3

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 466368 Color: 0
Size: 270484 Color: 1
Size: 263149 Color: 3

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 466498 Color: 1
Size: 281862 Color: 4
Size: 251641 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 466646 Color: 2
Size: 282132 Color: 4
Size: 251223 Color: 3

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 466679 Color: 3
Size: 279640 Color: 2
Size: 253682 Color: 4

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 466723 Color: 1
Size: 281353 Color: 0
Size: 251925 Color: 1

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 466842 Color: 3
Size: 278519 Color: 4
Size: 254640 Color: 4

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 466753 Color: 0
Size: 274861 Color: 2
Size: 258387 Color: 4

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 466883 Color: 3
Size: 269704 Color: 0
Size: 263414 Color: 1

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 467015 Color: 3
Size: 278914 Color: 4
Size: 254072 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 466993 Color: 0
Size: 270864 Color: 1
Size: 262144 Color: 4

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 467096 Color: 0
Size: 277837 Color: 3
Size: 255068 Color: 4

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 467188 Color: 1
Size: 270045 Color: 2
Size: 262768 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 467205 Color: 2
Size: 267264 Color: 1
Size: 265532 Color: 3

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 467408 Color: 2
Size: 273632 Color: 3
Size: 258961 Color: 0

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 467502 Color: 0
Size: 272544 Color: 0
Size: 259955 Color: 1

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 467538 Color: 0
Size: 282259 Color: 4
Size: 250204 Color: 2

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 467550 Color: 4
Size: 272341 Color: 1
Size: 260110 Color: 3

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 467738 Color: 3
Size: 279692 Color: 2
Size: 252571 Color: 1

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 467810 Color: 2
Size: 268923 Color: 0
Size: 263268 Color: 4

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 467859 Color: 2
Size: 281075 Color: 3
Size: 251067 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 468102 Color: 1
Size: 268585 Color: 0
Size: 263314 Color: 4

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 468454 Color: 4
Size: 270602 Color: 0
Size: 260945 Color: 2

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 468722 Color: 2
Size: 273755 Color: 3
Size: 257524 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 468753 Color: 1
Size: 276228 Color: 3
Size: 255020 Color: 4

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 468770 Color: 1
Size: 272494 Color: 2
Size: 258737 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 469160 Color: 2
Size: 273807 Color: 3
Size: 257034 Color: 1

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 469226 Color: 4
Size: 280301 Color: 1
Size: 250474 Color: 1

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 469349 Color: 3
Size: 268322 Color: 4
Size: 262330 Color: 4

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 469399 Color: 0
Size: 274375 Color: 3
Size: 256227 Color: 2

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 469424 Color: 4
Size: 274021 Color: 0
Size: 256556 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 469466 Color: 4
Size: 266566 Color: 3
Size: 263969 Color: 1

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 469520 Color: 1
Size: 276260 Color: 2
Size: 254221 Color: 2

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 469687 Color: 4
Size: 276814 Color: 0
Size: 253500 Color: 3

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 469679 Color: 3
Size: 267478 Color: 0
Size: 262844 Color: 4

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 469784 Color: 2
Size: 269453 Color: 3
Size: 260764 Color: 4

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 469881 Color: 1
Size: 278898 Color: 4
Size: 251222 Color: 1

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 469987 Color: 1
Size: 278818 Color: 3
Size: 251196 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 470099 Color: 1
Size: 267344 Color: 3
Size: 262558 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 470190 Color: 4
Size: 267385 Color: 2
Size: 262426 Color: 4

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 470329 Color: 2
Size: 277934 Color: 1
Size: 251738 Color: 3

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 470345 Color: 2
Size: 279522 Color: 3
Size: 250134 Color: 3

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 470359 Color: 4
Size: 274979 Color: 2
Size: 254663 Color: 4

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 470393 Color: 3
Size: 276519 Color: 2
Size: 253089 Color: 2

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 470368 Color: 2
Size: 271799 Color: 2
Size: 257834 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 470434 Color: 0
Size: 273957 Color: 2
Size: 255610 Color: 3

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 470540 Color: 3
Size: 269725 Color: 4
Size: 259736 Color: 2

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 470554 Color: 0
Size: 277303 Color: 1
Size: 252144 Color: 1

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 470694 Color: 1
Size: 267235 Color: 2
Size: 262072 Color: 4

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 470703 Color: 0
Size: 272064 Color: 3
Size: 257234 Color: 4

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 470819 Color: 1
Size: 270150 Color: 2
Size: 259032 Color: 2

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 470883 Color: 2
Size: 277320 Color: 1
Size: 251798 Color: 3

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 471026 Color: 0
Size: 278438 Color: 0
Size: 250537 Color: 3

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 471135 Color: 0
Size: 272157 Color: 1
Size: 256709 Color: 1

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 471235 Color: 2
Size: 273685 Color: 1
Size: 255081 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 471322 Color: 0
Size: 274704 Color: 3
Size: 253975 Color: 2

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 471390 Color: 0
Size: 277686 Color: 3
Size: 250925 Color: 1

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 471434 Color: 2
Size: 269536 Color: 3
Size: 259031 Color: 1

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 471649 Color: 3
Size: 273185 Color: 0
Size: 255167 Color: 2

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 471689 Color: 3
Size: 273723 Color: 1
Size: 254589 Color: 1

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 471611 Color: 4
Size: 273718 Color: 0
Size: 254672 Color: 1

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 471789 Color: 3
Size: 270734 Color: 2
Size: 257478 Color: 2

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 472040 Color: 2
Size: 267285 Color: 4
Size: 260676 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 472077 Color: 4
Size: 275690 Color: 0
Size: 252234 Color: 3

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 472129 Color: 3
Size: 270914 Color: 0
Size: 256958 Color: 4

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 472152 Color: 4
Size: 273562 Color: 2
Size: 254287 Color: 0

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 472216 Color: 4
Size: 269091 Color: 0
Size: 258694 Color: 3

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 472255 Color: 3
Size: 265335 Color: 4
Size: 262411 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 472266 Color: 2
Size: 265716 Color: 1
Size: 262019 Color: 1

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 472331 Color: 3
Size: 265527 Color: 2
Size: 262143 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 472428 Color: 0
Size: 276766 Color: 3
Size: 250807 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 472483 Color: 2
Size: 272473 Color: 0
Size: 255045 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 472930 Color: 1
Size: 271848 Color: 0
Size: 255223 Color: 3

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 473205 Color: 1
Size: 271434 Color: 1
Size: 255362 Color: 3

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 473216 Color: 1
Size: 276783 Color: 0
Size: 250002 Color: 2

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 473365 Color: 1
Size: 270143 Color: 3
Size: 256493 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 473504 Color: 1
Size: 268653 Color: 4
Size: 257844 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 473922 Color: 3
Size: 271190 Color: 4
Size: 254889 Color: 4

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 473889 Color: 0
Size: 271931 Color: 1
Size: 254181 Color: 2

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 474049 Color: 3
Size: 273830 Color: 2
Size: 252122 Color: 2

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 474242 Color: 3
Size: 275644 Color: 1
Size: 250115 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 474057 Color: 0
Size: 263290 Color: 1
Size: 262654 Color: 0

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 474352 Color: 3
Size: 263308 Color: 4
Size: 262341 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 474357 Color: 3
Size: 274542 Color: 4
Size: 251102 Color: 3

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 474363 Color: 3
Size: 263417 Color: 2
Size: 262221 Color: 1

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 474267 Color: 1
Size: 271108 Color: 2
Size: 254626 Color: 4

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 474373 Color: 4
Size: 269707 Color: 1
Size: 255921 Color: 3

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 474429 Color: 0
Size: 270333 Color: 0
Size: 255239 Color: 4

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 474501 Color: 2
Size: 271261 Color: 4
Size: 254239 Color: 3

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 474661 Color: 2
Size: 263696 Color: 1
Size: 261644 Color: 3

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 474692 Color: 0
Size: 270797 Color: 2
Size: 254512 Color: 1

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 474824 Color: 4
Size: 269206 Color: 0
Size: 255971 Color: 2

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 474971 Color: 3
Size: 265914 Color: 2
Size: 259116 Color: 4

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 474963 Color: 1
Size: 267248 Color: 2
Size: 257790 Color: 4

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 475279 Color: 4
Size: 272899 Color: 3
Size: 251823 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 475284 Color: 2
Size: 268186 Color: 3
Size: 256531 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 475371 Color: 0
Size: 267808 Color: 1
Size: 256822 Color: 1

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 475558 Color: 4
Size: 269781 Color: 2
Size: 254662 Color: 1

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 475712 Color: 1
Size: 264349 Color: 4
Size: 259940 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 475772 Color: 2
Size: 272298 Color: 3
Size: 251931 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 475785 Color: 0
Size: 268166 Color: 4
Size: 256050 Color: 2

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 475805 Color: 0
Size: 271994 Color: 3
Size: 252202 Color: 4

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 475834 Color: 0
Size: 274140 Color: 3
Size: 250027 Color: 1

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 475919 Color: 0
Size: 268301 Color: 4
Size: 255781 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 476131 Color: 2
Size: 262865 Color: 3
Size: 261005 Color: 4

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 476238 Color: 2
Size: 262355 Color: 0
Size: 261408 Color: 2

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 476239 Color: 0
Size: 268010 Color: 3
Size: 255752 Color: 4

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 476285 Color: 0
Size: 270674 Color: 1
Size: 253042 Color: 0

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 476538 Color: 2
Size: 268528 Color: 3
Size: 254935 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 476608 Color: 1
Size: 264267 Color: 1
Size: 259126 Color: 2

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 476742 Color: 4
Size: 264301 Color: 4
Size: 258958 Color: 3

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 476820 Color: 1
Size: 266576 Color: 4
Size: 256605 Color: 3

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 476891 Color: 3
Size: 265493 Color: 4
Size: 257617 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 476910 Color: 4
Size: 269273 Color: 0
Size: 253818 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 477002 Color: 2
Size: 272939 Color: 3
Size: 250060 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 477068 Color: 1
Size: 263922 Color: 4
Size: 259011 Color: 3

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 477106 Color: 0
Size: 269684 Color: 4
Size: 253211 Color: 2

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 477412 Color: 2
Size: 268235 Color: 3
Size: 254354 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 477578 Color: 2
Size: 271587 Color: 0
Size: 250836 Color: 1

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 477752 Color: 2
Size: 264106 Color: 0
Size: 258143 Color: 4

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 478142 Color: 0
Size: 265095 Color: 2
Size: 256764 Color: 3

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 478265 Color: 3
Size: 264343 Color: 2
Size: 257393 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 478199 Color: 0
Size: 265060 Color: 1
Size: 256742 Color: 2

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 478213 Color: 2
Size: 262112 Color: 3
Size: 259676 Color: 4

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 478308 Color: 0
Size: 266999 Color: 3
Size: 254694 Color: 1

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 478389 Color: 0
Size: 262155 Color: 4
Size: 259457 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 478396 Color: 2
Size: 270909 Color: 2
Size: 250696 Color: 3

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 478442 Color: 2
Size: 271449 Color: 0
Size: 250110 Color: 3

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 478719 Color: 1
Size: 263229 Color: 3
Size: 258053 Color: 2

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 478929 Color: 0
Size: 268953 Color: 4
Size: 252119 Color: 2

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 479006 Color: 0
Size: 263005 Color: 3
Size: 257990 Color: 1

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 479078 Color: 3
Size: 262565 Color: 1
Size: 258358 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 479219 Color: 2
Size: 266515 Color: 1
Size: 254267 Color: 4

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 479295 Color: 0
Size: 262484 Color: 2
Size: 258222 Color: 3

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 479456 Color: 4
Size: 267965 Color: 2
Size: 252580 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 479466 Color: 0
Size: 265075 Color: 1
Size: 255460 Color: 3

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 479475 Color: 3
Size: 270136 Color: 1
Size: 250390 Color: 4

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 479472 Color: 4
Size: 267828 Color: 4
Size: 252701 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 479479 Color: 1
Size: 266136 Color: 4
Size: 254386 Color: 3

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 479778 Color: 0
Size: 268572 Color: 4
Size: 251651 Color: 3

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 479814 Color: 0
Size: 267054 Color: 1
Size: 253133 Color: 3

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 480003 Color: 0
Size: 260556 Color: 1
Size: 259442 Color: 4

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 480147 Color: 1
Size: 269577 Color: 3
Size: 250277 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 480180 Color: 4
Size: 262226 Color: 1
Size: 257595 Color: 3

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 480221 Color: 2
Size: 265222 Color: 0
Size: 254558 Color: 4

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 480350 Color: 3
Size: 268791 Color: 4
Size: 250860 Color: 2

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 480383 Color: 0
Size: 261864 Color: 2
Size: 257754 Color: 2

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 480558 Color: 3
Size: 263416 Color: 1
Size: 256027 Color: 2

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 480674 Color: 2
Size: 265588 Color: 3
Size: 253739 Color: 1

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 480828 Color: 3
Size: 262158 Color: 2
Size: 257015 Color: 2

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 480887 Color: 1
Size: 268992 Color: 4
Size: 250122 Color: 3

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 480936 Color: 2
Size: 261073 Color: 1
Size: 257992 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 480952 Color: 2
Size: 269030 Color: 1
Size: 250019 Color: 2

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 481266 Color: 3
Size: 265531 Color: 2
Size: 253204 Color: 4

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 481298 Color: 3
Size: 267431 Color: 2
Size: 251272 Color: 2

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 481371 Color: 3
Size: 261155 Color: 2
Size: 257475 Color: 4

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 481565 Color: 3
Size: 260690 Color: 2
Size: 257746 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 481710 Color: 3
Size: 267498 Color: 4
Size: 250793 Color: 1

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 481911 Color: 1
Size: 262501 Color: 4
Size: 255589 Color: 2

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 481935 Color: 1
Size: 267287 Color: 2
Size: 250779 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 481974 Color: 3
Size: 266364 Color: 4
Size: 251663 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 482004 Color: 2
Size: 265663 Color: 4
Size: 252334 Color: 1

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 482110 Color: 1
Size: 261630 Color: 4
Size: 256261 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 482220 Color: 0
Size: 264651 Color: 3
Size: 253130 Color: 2

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 482437 Color: 2
Size: 259953 Color: 4
Size: 257611 Color: 1

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 482703 Color: 4
Size: 259541 Color: 3
Size: 257757 Color: 1

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 482957 Color: 3
Size: 259148 Color: 1
Size: 257896 Color: 2

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 483177 Color: 4
Size: 263822 Color: 3
Size: 253002 Color: 2

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 483273 Color: 3
Size: 260182 Color: 1
Size: 256546 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 483369 Color: 3
Size: 261669 Color: 4
Size: 254963 Color: 2

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 483260 Color: 1
Size: 265556 Color: 2
Size: 251185 Color: 0

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 483453 Color: 1
Size: 259588 Color: 4
Size: 256960 Color: 1

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 483461 Color: 1
Size: 260045 Color: 3
Size: 256495 Color: 4

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 483647 Color: 0
Size: 261722 Color: 3
Size: 254632 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 483654 Color: 0
Size: 261151 Color: 0
Size: 255196 Color: 3

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 483749 Color: 1
Size: 261434 Color: 3
Size: 254818 Color: 4

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 483951 Color: 0
Size: 258840 Color: 2
Size: 257210 Color: 1

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 483951 Color: 4
Size: 260152 Color: 1
Size: 255898 Color: 1

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 484271 Color: 1
Size: 258701 Color: 0
Size: 257029 Color: 3

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 484302 Color: 1
Size: 258584 Color: 2
Size: 257115 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 484515 Color: 4
Size: 260420 Color: 4
Size: 255066 Color: 1

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 484633 Color: 2
Size: 262276 Color: 4
Size: 253092 Color: 1

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 484636 Color: 2
Size: 259705 Color: 3
Size: 255660 Color: 4

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 484642 Color: 0
Size: 265206 Color: 2
Size: 250153 Color: 3

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 484648 Color: 2
Size: 265077 Color: 4
Size: 250276 Color: 1

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 484869 Color: 4
Size: 262033 Color: 3
Size: 253099 Color: 1

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 484935 Color: 1
Size: 262604 Color: 0
Size: 252462 Color: 3

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 484961 Color: 4
Size: 261506 Color: 3
Size: 253534 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 433524 Color: 4
Size: 288629 Color: 1
Size: 277848 Color: 4

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 484962 Color: 0
Size: 263781 Color: 2
Size: 251258 Color: 1

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 485113 Color: 2
Size: 258020 Color: 1
Size: 256868 Color: 0

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 485178 Color: 4
Size: 262238 Color: 4
Size: 252585 Color: 3

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 485301 Color: 2
Size: 257616 Color: 4
Size: 257084 Color: 3

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 485395 Color: 2
Size: 260342 Color: 1
Size: 254264 Color: 4

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 485484 Color: 1
Size: 263921 Color: 4
Size: 250596 Color: 3

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 485704 Color: 4
Size: 263693 Color: 2
Size: 250604 Color: 2

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 485710 Color: 0
Size: 260067 Color: 4
Size: 254224 Color: 3

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 485798 Color: 3
Size: 263993 Color: 0
Size: 250210 Color: 4

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 485752 Color: 0
Size: 260847 Color: 2
Size: 253402 Color: 1

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 485784 Color: 4
Size: 263021 Color: 4
Size: 251196 Color: 1

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 485978 Color: 3
Size: 258825 Color: 1
Size: 255198 Color: 2

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 485989 Color: 3
Size: 263564 Color: 0
Size: 250448 Color: 0

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 486114 Color: 3
Size: 263512 Color: 4
Size: 250375 Color: 4

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 486314 Color: 0
Size: 258292 Color: 1
Size: 255395 Color: 2

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 486330 Color: 2
Size: 257578 Color: 0
Size: 256093 Color: 3

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 486456 Color: 3
Size: 262014 Color: 1
Size: 251531 Color: 4

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 486352 Color: 1
Size: 261632 Color: 4
Size: 252017 Color: 1

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 486557 Color: 1
Size: 256810 Color: 3
Size: 256634 Color: 0

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 486685 Color: 4
Size: 258913 Color: 4
Size: 254403 Color: 2

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 486803 Color: 3
Size: 257283 Color: 4
Size: 255915 Color: 2

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 486757 Color: 1
Size: 263175 Color: 2
Size: 250069 Color: 0

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 487394 Color: 3
Size: 257594 Color: 2
Size: 255013 Color: 2

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 487260 Color: 1
Size: 259187 Color: 1
Size: 253554 Color: 4

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 487404 Color: 1
Size: 261712 Color: 0
Size: 250885 Color: 0

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 487473 Color: 4
Size: 258813 Color: 2
Size: 253715 Color: 0

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 487485 Color: 4
Size: 262047 Color: 3
Size: 250469 Color: 4

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 487512 Color: 0
Size: 257461 Color: 1
Size: 255028 Color: 3

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 487514 Color: 2
Size: 258245 Color: 4
Size: 254242 Color: 1

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 487567 Color: 1
Size: 261894 Color: 4
Size: 250540 Color: 2

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 487797 Color: 2
Size: 261480 Color: 3
Size: 250724 Color: 2

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 487917 Color: 4
Size: 261155 Color: 1
Size: 250929 Color: 2

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 487936 Color: 1
Size: 257403 Color: 3
Size: 254662 Color: 3

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 487960 Color: 4
Size: 256384 Color: 4
Size: 255657 Color: 0

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 488044 Color: 1
Size: 261674 Color: 3
Size: 250283 Color: 2

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 488261 Color: 2
Size: 259464 Color: 4
Size: 252276 Color: 3

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 488344 Color: 1
Size: 260817 Color: 3
Size: 250840 Color: 0

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 488478 Color: 2
Size: 260954 Color: 4
Size: 250569 Color: 2

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 488572 Color: 4
Size: 260067 Color: 3
Size: 251362 Color: 1

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 488575 Color: 0
Size: 256981 Color: 2
Size: 254445 Color: 2

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 488607 Color: 4
Size: 260446 Color: 1
Size: 250948 Color: 3

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 488634 Color: 2
Size: 256707 Color: 4
Size: 254660 Color: 1

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 488732 Color: 4
Size: 261192 Color: 2
Size: 250077 Color: 1

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 488902 Color: 1
Size: 260108 Color: 0
Size: 250991 Color: 3

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 489095 Color: 0
Size: 256746 Color: 2
Size: 254160 Color: 3

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 489289 Color: 4
Size: 256394 Color: 0
Size: 254318 Color: 4

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 489376 Color: 3
Size: 259216 Color: 0
Size: 251409 Color: 4

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 489352 Color: 0
Size: 260028 Color: 1
Size: 250621 Color: 2

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 489554 Color: 2
Size: 255415 Color: 1
Size: 255032 Color: 3

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 489713 Color: 1
Size: 256819 Color: 1
Size: 253469 Color: 3

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 489890 Color: 3
Size: 256362 Color: 0
Size: 253749 Color: 1

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 489959 Color: 2
Size: 259425 Color: 3
Size: 250617 Color: 2

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 490128 Color: 4
Size: 259636 Color: 3
Size: 250237 Color: 2

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 490160 Color: 2
Size: 259429 Color: 3
Size: 250412 Color: 0

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 490186 Color: 4
Size: 256701 Color: 2
Size: 253114 Color: 2

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 490193 Color: 4
Size: 257754 Color: 0
Size: 252054 Color: 3

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 490361 Color: 4
Size: 256792 Color: 2
Size: 252848 Color: 4

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 490363 Color: 4
Size: 255118 Color: 3
Size: 254520 Color: 0

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 490391 Color: 1
Size: 255429 Color: 0
Size: 254181 Color: 4

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 490558 Color: 3
Size: 255422 Color: 1
Size: 254021 Color: 2

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 490653 Color: 0
Size: 256675 Color: 3
Size: 252673 Color: 4

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 490665 Color: 1
Size: 257957 Color: 4
Size: 251379 Color: 2

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 490805 Color: 2
Size: 258009 Color: 0
Size: 251187 Color: 0

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 490882 Color: 2
Size: 256636 Color: 4
Size: 252483 Color: 1

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 491003 Color: 0
Size: 256768 Color: 3
Size: 252230 Color: 1

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 491181 Color: 1
Size: 254952 Color: 0
Size: 253868 Color: 4

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 491194 Color: 1
Size: 255600 Color: 2
Size: 253207 Color: 0

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 491233 Color: 4
Size: 257172 Color: 1
Size: 251596 Color: 3

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 491449 Color: 3
Size: 256116 Color: 1
Size: 252436 Color: 1

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 491299 Color: 4
Size: 258028 Color: 2
Size: 250674 Color: 4

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 491564 Color: 3
Size: 258150 Color: 2
Size: 250287 Color: 1

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 491674 Color: 2
Size: 257455 Color: 0
Size: 250872 Color: 1

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 491682 Color: 1
Size: 256010 Color: 3
Size: 252309 Color: 0

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 491796 Color: 1
Size: 255722 Color: 3
Size: 252483 Color: 0

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 491819 Color: 4
Size: 256939 Color: 1
Size: 251243 Color: 1

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 491853 Color: 0
Size: 257427 Color: 3
Size: 250721 Color: 1

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 491864 Color: 0
Size: 254268 Color: 1
Size: 253869 Color: 1

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 491926 Color: 3
Size: 255279 Color: 1
Size: 252796 Color: 0

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 492220 Color: 4
Size: 255014 Color: 4
Size: 252767 Color: 2

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 492315 Color: 3
Size: 256877 Color: 0
Size: 250809 Color: 2

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 492247 Color: 0
Size: 255417 Color: 4
Size: 252337 Color: 2

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 492269 Color: 0
Size: 254312 Color: 0
Size: 253420 Color: 2

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 492560 Color: 0
Size: 254812 Color: 3
Size: 252629 Color: 0

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 492661 Color: 0
Size: 257249 Color: 0
Size: 250091 Color: 1

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 492840 Color: 3
Size: 254213 Color: 0
Size: 252948 Color: 4

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 385238 Color: 4
Size: 355447 Color: 3
Size: 259316 Color: 2

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 492968 Color: 3
Size: 256254 Color: 4
Size: 250779 Color: 3

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 493070 Color: 4
Size: 255455 Color: 3
Size: 251476 Color: 1

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 493767 Color: 1
Size: 254779 Color: 4
Size: 251455 Color: 0

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 493779 Color: 2
Size: 255109 Color: 3
Size: 251113 Color: 1

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 493865 Color: 0
Size: 255686 Color: 3
Size: 250450 Color: 3

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 493979 Color: 2
Size: 254300 Color: 3
Size: 251722 Color: 2

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 494011 Color: 1
Size: 253395 Color: 0
Size: 252595 Color: 4

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 494040 Color: 1
Size: 254957 Color: 0
Size: 251004 Color: 3

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 494101 Color: 0
Size: 255835 Color: 1
Size: 250065 Color: 4

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 494199 Color: 4
Size: 255132 Color: 3
Size: 250670 Color: 4

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 494253 Color: 1
Size: 255712 Color: 2
Size: 250036 Color: 0

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 494452 Color: 4
Size: 255099 Color: 0
Size: 250450 Color: 0

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 494459 Color: 4
Size: 255517 Color: 0
Size: 250025 Color: 1

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 494545 Color: 3
Size: 254611 Color: 4
Size: 250845 Color: 2

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 494521 Color: 1
Size: 255301 Color: 2
Size: 250179 Color: 2

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 494757 Color: 0
Size: 254433 Color: 4
Size: 250811 Color: 3

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 494887 Color: 4
Size: 254394 Color: 1
Size: 250720 Color: 3

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 495014 Color: 4
Size: 254124 Color: 2
Size: 250863 Color: 3

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 495057 Color: 0
Size: 254006 Color: 1
Size: 250938 Color: 2

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 495072 Color: 2
Size: 253108 Color: 3
Size: 251821 Color: 3

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 495203 Color: 2
Size: 253775 Color: 1
Size: 251023 Color: 3

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 495381 Color: 2
Size: 254562 Color: 1
Size: 250058 Color: 2

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 495471 Color: 0
Size: 254027 Color: 0
Size: 250503 Color: 4

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 495788 Color: 1
Size: 252745 Color: 3
Size: 251468 Color: 2

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 495944 Color: 0
Size: 253501 Color: 4
Size: 250556 Color: 1

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 496014 Color: 2
Size: 253588 Color: 1
Size: 250399 Color: 3

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 496056 Color: 4
Size: 252848 Color: 2
Size: 251097 Color: 3

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 496105 Color: 1
Size: 252931 Color: 3
Size: 250965 Color: 0

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 496119 Color: 0
Size: 253663 Color: 1
Size: 250219 Color: 2

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 496201 Color: 0
Size: 252490 Color: 3
Size: 251310 Color: 1

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 496267 Color: 3
Size: 252974 Color: 4
Size: 250760 Color: 0

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 496217 Color: 0
Size: 252680 Color: 2
Size: 251104 Color: 4

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 496319 Color: 2
Size: 252478 Color: 0
Size: 251204 Color: 3

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 496421 Color: 4
Size: 253411 Color: 1
Size: 250169 Color: 2

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 496446 Color: 4
Size: 252978 Color: 0
Size: 250577 Color: 3

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 496529 Color: 3
Size: 253200 Color: 0
Size: 250272 Color: 4

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 496499 Color: 2
Size: 252667 Color: 2
Size: 250835 Color: 1

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 496655 Color: 0
Size: 252958 Color: 4
Size: 250388 Color: 0

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 496713 Color: 4
Size: 251780 Color: 3
Size: 251508 Color: 2

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 496776 Color: 2
Size: 251889 Color: 3
Size: 251336 Color: 0

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 496929 Color: 4
Size: 252047 Color: 2
Size: 251025 Color: 1

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 497071 Color: 2
Size: 252439 Color: 3
Size: 250491 Color: 4

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 497086 Color: 1
Size: 251628 Color: 1
Size: 251287 Color: 2

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 497104 Color: 3
Size: 251583 Color: 0
Size: 251314 Color: 4

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 497121 Color: 1
Size: 252706 Color: 0
Size: 250174 Color: 1

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 497846 Color: 4
Size: 251199 Color: 1
Size: 250956 Color: 3

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 498025 Color: 3
Size: 251266 Color: 0
Size: 250710 Color: 2

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 498111 Color: 3
Size: 251270 Color: 0
Size: 250620 Color: 2

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 498313 Color: 3
Size: 251279 Color: 1
Size: 250409 Color: 4

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 498485 Color: 4
Size: 251102 Color: 0
Size: 250414 Color: 1

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 498583 Color: 3
Size: 250885 Color: 2
Size: 250533 Color: 4

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 498687 Color: 4
Size: 251111 Color: 0
Size: 250203 Color: 2

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 499472 Color: 2
Size: 250285 Color: 3
Size: 250244 Color: 1

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 499776 Color: 3
Size: 250203 Color: 3
Size: 250022 Color: 0

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 499785 Color: 0
Size: 250173 Color: 1
Size: 250043 Color: 3

Bin 3099: 1 of cap free
Amount of items: 3
Items: 
Size: 358708 Color: 2
Size: 336921 Color: 1
Size: 304371 Color: 1

Bin 3100: 1 of cap free
Amount of items: 3
Items: 
Size: 363269 Color: 2
Size: 348062 Color: 3
Size: 288669 Color: 3

Bin 3101: 1 of cap free
Amount of items: 3
Items: 
Size: 487213 Color: 0
Size: 256771 Color: 4
Size: 256016 Color: 2

Bin 3102: 1 of cap free
Amount of items: 3
Items: 
Size: 336154 Color: 3
Size: 332168 Color: 0
Size: 331678 Color: 1

Bin 3103: 1 of cap free
Amount of items: 3
Items: 
Size: 410236 Color: 1
Size: 325294 Color: 2
Size: 264470 Color: 0

Bin 3104: 1 of cap free
Amount of items: 3
Items: 
Size: 402416 Color: 3
Size: 332911 Color: 2
Size: 264673 Color: 2

Bin 3105: 1 of cap free
Amount of items: 3
Items: 
Size: 346961 Color: 2
Size: 328077 Color: 2
Size: 324962 Color: 0

Bin 3106: 1 of cap free
Amount of items: 3
Items: 
Size: 354261 Color: 0
Size: 350178 Color: 4
Size: 295561 Color: 1

Bin 3107: 1 of cap free
Amount of items: 3
Items: 
Size: 355888 Color: 1
Size: 340843 Color: 2
Size: 303269 Color: 1

Bin 3108: 1 of cap free
Amount of items: 3
Items: 
Size: 336446 Color: 1
Size: 333916 Color: 4
Size: 329638 Color: 0

Bin 3109: 1 of cap free
Amount of items: 3
Items: 
Size: 355886 Color: 2
Size: 354153 Color: 1
Size: 289961 Color: 0

Bin 3110: 1 of cap free
Amount of items: 3
Items: 
Size: 370687 Color: 2
Size: 356795 Color: 4
Size: 272518 Color: 0

Bin 3111: 1 of cap free
Amount of items: 3
Items: 
Size: 393460 Color: 3
Size: 334884 Color: 1
Size: 271656 Color: 4

Bin 3112: 1 of cap free
Amount of items: 3
Items: 
Size: 356969 Color: 2
Size: 350520 Color: 4
Size: 292511 Color: 3

Bin 3113: 1 of cap free
Amount of items: 3
Items: 
Size: 498560 Color: 2
Size: 251093 Color: 2
Size: 250347 Color: 3

Bin 3114: 1 of cap free
Amount of items: 3
Items: 
Size: 359994 Color: 2
Size: 345469 Color: 4
Size: 294537 Color: 0

Bin 3115: 1 of cap free
Amount of items: 3
Items: 
Size: 344271 Color: 3
Size: 337942 Color: 1
Size: 317787 Color: 2

Bin 3116: 1 of cap free
Amount of items: 3
Items: 
Size: 360852 Color: 4
Size: 352669 Color: 0
Size: 286479 Color: 1

Bin 3117: 1 of cap free
Amount of items: 3
Items: 
Size: 356639 Color: 1
Size: 331517 Color: 2
Size: 311844 Color: 1

Bin 3118: 1 of cap free
Amount of items: 3
Items: 
Size: 356498 Color: 4
Size: 328914 Color: 1
Size: 314588 Color: 3

Bin 3119: 1 of cap free
Amount of items: 3
Items: 
Size: 358645 Color: 3
Size: 323279 Color: 4
Size: 318076 Color: 3

Bin 3120: 1 of cap free
Amount of items: 3
Items: 
Size: 350395 Color: 3
Size: 333509 Color: 2
Size: 316096 Color: 3

Bin 3121: 1 of cap free
Amount of items: 3
Items: 
Size: 360122 Color: 1
Size: 351972 Color: 1
Size: 287906 Color: 0

Bin 3122: 1 of cap free
Amount of items: 3
Items: 
Size: 360376 Color: 1
Size: 340102 Color: 4
Size: 299522 Color: 0

Bin 3123: 1 of cap free
Amount of items: 3
Items: 
Size: 360562 Color: 4
Size: 345632 Color: 2
Size: 293806 Color: 0

Bin 3124: 1 of cap free
Amount of items: 3
Items: 
Size: 360824 Color: 4
Size: 328446 Color: 1
Size: 310730 Color: 2

Bin 3125: 1 of cap free
Amount of items: 3
Items: 
Size: 336993 Color: 3
Size: 336811 Color: 4
Size: 326196 Color: 0

Bin 3126: 1 of cap free
Amount of items: 3
Items: 
Size: 361276 Color: 4
Size: 349870 Color: 0
Size: 288854 Color: 3

Bin 3127: 1 of cap free
Amount of items: 3
Items: 
Size: 361698 Color: 0
Size: 358867 Color: 2
Size: 279435 Color: 4

Bin 3128: 1 of cap free
Amount of items: 3
Items: 
Size: 362741 Color: 3
Size: 342416 Color: 3
Size: 294843 Color: 0

Bin 3129: 1 of cap free
Amount of items: 3
Items: 
Size: 363090 Color: 1
Size: 332618 Color: 4
Size: 304292 Color: 0

Bin 3130: 1 of cap free
Amount of items: 3
Items: 
Size: 365184 Color: 2
Size: 331849 Color: 4
Size: 302967 Color: 3

Bin 3131: 1 of cap free
Amount of items: 3
Items: 
Size: 365451 Color: 3
Size: 363074 Color: 1
Size: 271475 Color: 3

Bin 3132: 1 of cap free
Amount of items: 3
Items: 
Size: 365969 Color: 2
Size: 319226 Color: 2
Size: 314805 Color: 0

Bin 3133: 1 of cap free
Amount of items: 3
Items: 
Size: 366030 Color: 4
Size: 353753 Color: 1
Size: 280217 Color: 0

Bin 3134: 1 of cap free
Amount of items: 3
Items: 
Size: 367571 Color: 3
Size: 362687 Color: 3
Size: 269742 Color: 2

Bin 3135: 1 of cap free
Amount of items: 3
Items: 
Size: 370672 Color: 2
Size: 354175 Color: 4
Size: 275153 Color: 1

Bin 3136: 1 of cap free
Amount of items: 3
Items: 
Size: 349437 Color: 0
Size: 335922 Color: 4
Size: 314641 Color: 0

Bin 3137: 1 of cap free
Amount of items: 3
Items: 
Size: 368614 Color: 4
Size: 320696 Color: 3
Size: 310690 Color: 3

Bin 3138: 1 of cap free
Amount of items: 3
Items: 
Size: 359360 Color: 0
Size: 359185 Color: 3
Size: 281455 Color: 4

Bin 3139: 1 of cap free
Amount of items: 3
Items: 
Size: 351294 Color: 1
Size: 350268 Color: 0
Size: 298438 Color: 0

Bin 3140: 1 of cap free
Amount of items: 3
Items: 
Size: 363656 Color: 0
Size: 358456 Color: 4
Size: 277888 Color: 0

Bin 3141: 1 of cap free
Amount of items: 3
Items: 
Size: 353917 Color: 4
Size: 345232 Color: 3
Size: 300851 Color: 0

Bin 3142: 1 of cap free
Amount of items: 3
Items: 
Size: 342463 Color: 4
Size: 331937 Color: 0
Size: 325600 Color: 2

Bin 3143: 1 of cap free
Amount of items: 3
Items: 
Size: 356971 Color: 0
Size: 336961 Color: 0
Size: 306068 Color: 2

Bin 3144: 1 of cap free
Amount of items: 3
Items: 
Size: 346084 Color: 4
Size: 344879 Color: 3
Size: 309037 Color: 0

Bin 3145: 1 of cap free
Amount of items: 3
Items: 
Size: 350810 Color: 0
Size: 331344 Color: 1
Size: 317846 Color: 4

Bin 3146: 1 of cap free
Amount of items: 3
Items: 
Size: 364073 Color: 1
Size: 362570 Color: 3
Size: 273357 Color: 1

Bin 3147: 1 of cap free
Amount of items: 3
Items: 
Size: 361971 Color: 0
Size: 348230 Color: 2
Size: 289799 Color: 0

Bin 3148: 1 of cap free
Amount of items: 3
Items: 
Size: 385441 Color: 4
Size: 356164 Color: 1
Size: 258395 Color: 2

Bin 3149: 1 of cap free
Amount of items: 3
Items: 
Size: 354246 Color: 3
Size: 345049 Color: 0
Size: 300705 Color: 0

Bin 3150: 1 of cap free
Amount of items: 3
Items: 
Size: 471408 Color: 4
Size: 277257 Color: 4
Size: 251335 Color: 1

Bin 3151: 2 of cap free
Amount of items: 3
Items: 
Size: 481351 Color: 4
Size: 261052 Color: 3
Size: 257596 Color: 1

Bin 3152: 2 of cap free
Amount of items: 3
Items: 
Size: 339571 Color: 0
Size: 330471 Color: 2
Size: 329957 Color: 1

Bin 3153: 2 of cap free
Amount of items: 3
Items: 
Size: 471453 Color: 0
Size: 273825 Color: 3
Size: 254721 Color: 4

Bin 3154: 2 of cap free
Amount of items: 3
Items: 
Size: 384235 Color: 1
Size: 360767 Color: 4
Size: 254997 Color: 1

Bin 3155: 2 of cap free
Amount of items: 3
Items: 
Size: 474973 Color: 4
Size: 262815 Color: 2
Size: 262211 Color: 1

Bin 3156: 2 of cap free
Amount of items: 3
Items: 
Size: 356897 Color: 4
Size: 333960 Color: 2
Size: 309142 Color: 2

Bin 3157: 2 of cap free
Amount of items: 3
Items: 
Size: 348062 Color: 1
Size: 327852 Color: 3
Size: 324085 Color: 3

Bin 3158: 2 of cap free
Amount of items: 3
Items: 
Size: 356031 Color: 4
Size: 329451 Color: 1
Size: 314517 Color: 2

Bin 3159: 2 of cap free
Amount of items: 3
Items: 
Size: 363947 Color: 1
Size: 326551 Color: 2
Size: 309501 Color: 1

Bin 3160: 2 of cap free
Amount of items: 3
Items: 
Size: 363312 Color: 3
Size: 320578 Color: 0
Size: 316109 Color: 2

Bin 3161: 2 of cap free
Amount of items: 3
Items: 
Size: 346846 Color: 3
Size: 342663 Color: 4
Size: 310490 Color: 1

Bin 3162: 2 of cap free
Amount of items: 3
Items: 
Size: 349076 Color: 3
Size: 346679 Color: 1
Size: 304244 Color: 4

Bin 3163: 2 of cap free
Amount of items: 3
Items: 
Size: 344291 Color: 1
Size: 334168 Color: 3
Size: 321540 Color: 4

Bin 3164: 2 of cap free
Amount of items: 3
Items: 
Size: 378649 Color: 2
Size: 364736 Color: 1
Size: 256614 Color: 1

Bin 3165: 3 of cap free
Amount of items: 3
Items: 
Size: 378712 Color: 2
Size: 358263 Color: 3
Size: 263023 Color: 3

Bin 3166: 3 of cap free
Amount of items: 3
Items: 
Size: 344928 Color: 1
Size: 329814 Color: 3
Size: 325256 Color: 3

Bin 3167: 3 of cap free
Amount of items: 3
Items: 
Size: 345243 Color: 3
Size: 333312 Color: 4
Size: 321443 Color: 1

Bin 3168: 3 of cap free
Amount of items: 3
Items: 
Size: 346064 Color: 0
Size: 342667 Color: 4
Size: 311267 Color: 3

Bin 3169: 3 of cap free
Amount of items: 3
Items: 
Size: 343909 Color: 4
Size: 336236 Color: 1
Size: 319853 Color: 1

Bin 3170: 3 of cap free
Amount of items: 3
Items: 
Size: 360782 Color: 3
Size: 349386 Color: 2
Size: 289830 Color: 2

Bin 3171: 3 of cap free
Amount of items: 3
Items: 
Size: 401401 Color: 0
Size: 343141 Color: 4
Size: 255456 Color: 3

Bin 3172: 3 of cap free
Amount of items: 3
Items: 
Size: 368841 Color: 1
Size: 362236 Color: 0
Size: 268921 Color: 0

Bin 3173: 3 of cap free
Amount of items: 3
Items: 
Size: 364264 Color: 4
Size: 361867 Color: 4
Size: 273867 Color: 0

Bin 3174: 3 of cap free
Amount of items: 3
Items: 
Size: 355667 Color: 0
Size: 353611 Color: 4
Size: 290720 Color: 3

Bin 3175: 3 of cap free
Amount of items: 3
Items: 
Size: 390790 Color: 1
Size: 345489 Color: 2
Size: 263719 Color: 1

Bin 3176: 3 of cap free
Amount of items: 3
Items: 
Size: 346982 Color: 4
Size: 346009 Color: 2
Size: 307007 Color: 4

Bin 3177: 3 of cap free
Amount of items: 3
Items: 
Size: 358820 Color: 3
Size: 353475 Color: 4
Size: 287703 Color: 2

Bin 3178: 3 of cap free
Amount of items: 3
Items: 
Size: 348637 Color: 2
Size: 325919 Color: 2
Size: 325442 Color: 3

Bin 3179: 3 of cap free
Amount of items: 3
Items: 
Size: 375761 Color: 3
Size: 355197 Color: 0
Size: 269040 Color: 2

Bin 3180: 3 of cap free
Amount of items: 3
Items: 
Size: 345695 Color: 0
Size: 339829 Color: 2
Size: 314474 Color: 0

Bin 3181: 3 of cap free
Amount of items: 3
Items: 
Size: 346817 Color: 0
Size: 330439 Color: 1
Size: 322742 Color: 4

Bin 3182: 4 of cap free
Amount of items: 3
Items: 
Size: 374889 Color: 1
Size: 358764 Color: 3
Size: 266344 Color: 1

Bin 3183: 4 of cap free
Amount of items: 3
Items: 
Size: 354147 Color: 2
Size: 350854 Color: 0
Size: 294996 Color: 4

Bin 3184: 4 of cap free
Amount of items: 3
Items: 
Size: 360263 Color: 4
Size: 349659 Color: 0
Size: 290075 Color: 1

Bin 3185: 4 of cap free
Amount of items: 3
Items: 
Size: 360739 Color: 3
Size: 347311 Color: 4
Size: 291947 Color: 4

Bin 3186: 4 of cap free
Amount of items: 3
Items: 
Size: 336086 Color: 1
Size: 334375 Color: 4
Size: 329536 Color: 0

Bin 3187: 4 of cap free
Amount of items: 3
Items: 
Size: 352255 Color: 2
Size: 343168 Color: 3
Size: 304574 Color: 1

Bin 3188: 5 of cap free
Amount of items: 3
Items: 
Size: 404833 Color: 1
Size: 341930 Color: 0
Size: 253233 Color: 2

Bin 3189: 5 of cap free
Amount of items: 3
Items: 
Size: 422959 Color: 1
Size: 306912 Color: 4
Size: 270125 Color: 4

Bin 3190: 5 of cap free
Amount of items: 3
Items: 
Size: 371371 Color: 2
Size: 354893 Color: 2
Size: 273732 Color: 0

Bin 3191: 5 of cap free
Amount of items: 3
Items: 
Size: 446945 Color: 0
Size: 299005 Color: 2
Size: 254046 Color: 1

Bin 3192: 5 of cap free
Amount of items: 3
Items: 
Size: 358648 Color: 1
Size: 350713 Color: 0
Size: 290635 Color: 0

Bin 3193: 5 of cap free
Amount of items: 3
Items: 
Size: 350610 Color: 0
Size: 349847 Color: 2
Size: 299539 Color: 4

Bin 3194: 5 of cap free
Amount of items: 3
Items: 
Size: 358173 Color: 4
Size: 353074 Color: 0
Size: 288749 Color: 4

Bin 3195: 6 of cap free
Amount of items: 3
Items: 
Size: 346137 Color: 3
Size: 340622 Color: 3
Size: 313236 Color: 2

Bin 3196: 6 of cap free
Amount of items: 3
Items: 
Size: 349912 Color: 1
Size: 349263 Color: 4
Size: 300820 Color: 0

Bin 3197: 6 of cap free
Amount of items: 3
Items: 
Size: 357630 Color: 3
Size: 333268 Color: 0
Size: 309097 Color: 4

Bin 3198: 6 of cap free
Amount of items: 3
Items: 
Size: 366808 Color: 2
Size: 352658 Color: 1
Size: 280529 Color: 3

Bin 3199: 6 of cap free
Amount of items: 3
Items: 
Size: 342998 Color: 4
Size: 342990 Color: 4
Size: 314007 Color: 3

Bin 3200: 6 of cap free
Amount of items: 3
Items: 
Size: 499778 Color: 4
Size: 250193 Color: 3
Size: 250024 Color: 1

Bin 3201: 6 of cap free
Amount of items: 3
Items: 
Size: 360898 Color: 3
Size: 324254 Color: 1
Size: 314843 Color: 3

Bin 3202: 6 of cap free
Amount of items: 3
Items: 
Size: 419744 Color: 1
Size: 324820 Color: 1
Size: 255431 Color: 2

Bin 3203: 7 of cap free
Amount of items: 3
Items: 
Size: 363049 Color: 4
Size: 328437 Color: 1
Size: 308508 Color: 3

Bin 3204: 8 of cap free
Amount of items: 3
Items: 
Size: 364522 Color: 4
Size: 350042 Color: 2
Size: 285429 Color: 3

Bin 3205: 8 of cap free
Amount of items: 3
Items: 
Size: 479358 Color: 4
Size: 268765 Color: 4
Size: 251870 Color: 2

Bin 3206: 8 of cap free
Amount of items: 3
Items: 
Size: 346933 Color: 4
Size: 344914 Color: 1
Size: 308146 Color: 3

Bin 3207: 8 of cap free
Amount of items: 3
Items: 
Size: 374655 Color: 1
Size: 350903 Color: 3
Size: 274435 Color: 4

Bin 3208: 8 of cap free
Amount of items: 3
Items: 
Size: 499364 Color: 1
Size: 250390 Color: 4
Size: 250239 Color: 1

Bin 3209: 9 of cap free
Amount of items: 3
Items: 
Size: 361205 Color: 1
Size: 324461 Color: 0
Size: 314326 Color: 0

Bin 3210: 9 of cap free
Amount of items: 3
Items: 
Size: 355313 Color: 1
Size: 335243 Color: 2
Size: 309436 Color: 2

Bin 3211: 10 of cap free
Amount of items: 3
Items: 
Size: 498542 Color: 2
Size: 251146 Color: 2
Size: 250303 Color: 1

Bin 3212: 10 of cap free
Amount of items: 3
Items: 
Size: 361768 Color: 0
Size: 360682 Color: 4
Size: 277541 Color: 3

Bin 3213: 10 of cap free
Amount of items: 3
Items: 
Size: 359187 Color: 3
Size: 349002 Color: 0
Size: 291802 Color: 0

Bin 3214: 11 of cap free
Amount of items: 3
Items: 
Size: 361657 Color: 0
Size: 344227 Color: 3
Size: 294106 Color: 3

Bin 3215: 11 of cap free
Amount of items: 3
Items: 
Size: 410334 Color: 2
Size: 327638 Color: 2
Size: 262018 Color: 3

Bin 3216: 11 of cap free
Amount of items: 3
Items: 
Size: 396100 Color: 1
Size: 341948 Color: 0
Size: 261942 Color: 1

Bin 3217: 11 of cap free
Amount of items: 3
Items: 
Size: 372819 Color: 3
Size: 348797 Color: 4
Size: 278374 Color: 4

Bin 3218: 12 of cap free
Amount of items: 3
Items: 
Size: 377591 Color: 1
Size: 348306 Color: 0
Size: 274092 Color: 0

Bin 3219: 12 of cap free
Amount of items: 3
Items: 
Size: 448171 Color: 4
Size: 287333 Color: 3
Size: 264485 Color: 3

Bin 3220: 13 of cap free
Amount of items: 3
Items: 
Size: 344775 Color: 0
Size: 344447 Color: 0
Size: 310766 Color: 2

Bin 3221: 13 of cap free
Amount of items: 3
Items: 
Size: 364947 Color: 3
Size: 364117 Color: 1
Size: 270924 Color: 0

Bin 3222: 13 of cap free
Amount of items: 3
Items: 
Size: 353357 Color: 1
Size: 352909 Color: 3
Size: 293722 Color: 1

Bin 3223: 14 of cap free
Amount of items: 3
Items: 
Size: 380536 Color: 4
Size: 349179 Color: 0
Size: 270272 Color: 2

Bin 3224: 14 of cap free
Amount of items: 3
Items: 
Size: 382656 Color: 3
Size: 345556 Color: 3
Size: 271775 Color: 4

Bin 3225: 14 of cap free
Amount of items: 3
Items: 
Size: 459289 Color: 3
Size: 275389 Color: 4
Size: 265309 Color: 2

Bin 3226: 15 of cap free
Amount of items: 3
Items: 
Size: 411462 Color: 3
Size: 327703 Color: 2
Size: 260821 Color: 1

Bin 3227: 15 of cap free
Amount of items: 3
Items: 
Size: 359702 Color: 1
Size: 339719 Color: 0
Size: 300565 Color: 4

Bin 3228: 16 of cap free
Amount of items: 3
Items: 
Size: 379547 Color: 0
Size: 353816 Color: 3
Size: 266622 Color: 1

Bin 3229: 17 of cap free
Amount of items: 3
Items: 
Size: 444204 Color: 2
Size: 292267 Color: 1
Size: 263513 Color: 3

Bin 3230: 17 of cap free
Amount of items: 3
Items: 
Size: 360676 Color: 4
Size: 342662 Color: 0
Size: 296646 Color: 0

Bin 3231: 17 of cap free
Amount of items: 3
Items: 
Size: 459810 Color: 2
Size: 270602 Color: 4
Size: 269572 Color: 4

Bin 3232: 17 of cap free
Amount of items: 3
Items: 
Size: 346582 Color: 2
Size: 337127 Color: 1
Size: 316275 Color: 3

Bin 3233: 17 of cap free
Amount of items: 3
Items: 
Size: 368227 Color: 0
Size: 360935 Color: 3
Size: 270822 Color: 2

Bin 3234: 18 of cap free
Amount of items: 3
Items: 
Size: 350379 Color: 0
Size: 347528 Color: 3
Size: 302076 Color: 1

Bin 3235: 18 of cap free
Amount of items: 3
Items: 
Size: 355466 Color: 2
Size: 331432 Color: 3
Size: 313085 Color: 3

Bin 3236: 18 of cap free
Amount of items: 3
Items: 
Size: 372337 Color: 3
Size: 345641 Color: 0
Size: 282005 Color: 3

Bin 3237: 19 of cap free
Amount of items: 3
Items: 
Size: 349158 Color: 2
Size: 334997 Color: 4
Size: 315827 Color: 1

Bin 3238: 19 of cap free
Amount of items: 3
Items: 
Size: 338113 Color: 2
Size: 333708 Color: 0
Size: 328161 Color: 0

Bin 3239: 19 of cap free
Amount of items: 3
Items: 
Size: 360576 Color: 1
Size: 359831 Color: 2
Size: 279575 Color: 0

Bin 3240: 19 of cap free
Amount of items: 3
Items: 
Size: 348268 Color: 3
Size: 345469 Color: 4
Size: 306245 Color: 1

Bin 3241: 20 of cap free
Amount of items: 3
Items: 
Size: 493786 Color: 2
Size: 255744 Color: 1
Size: 250451 Color: 3

Bin 3242: 20 of cap free
Amount of items: 3
Items: 
Size: 498766 Color: 4
Size: 251035 Color: 3
Size: 250180 Color: 3

Bin 3243: 20 of cap free
Amount of items: 3
Items: 
Size: 357903 Color: 1
Size: 331601 Color: 3
Size: 310477 Color: 0

Bin 3244: 20 of cap free
Amount of items: 3
Items: 
Size: 355439 Color: 4
Size: 351414 Color: 3
Size: 293128 Color: 2

Bin 3245: 21 of cap free
Amount of items: 3
Items: 
Size: 455561 Color: 4
Size: 285716 Color: 1
Size: 258703 Color: 0

Bin 3246: 21 of cap free
Amount of items: 3
Items: 
Size: 472101 Color: 1
Size: 267713 Color: 3
Size: 260166 Color: 1

Bin 3247: 21 of cap free
Amount of items: 3
Items: 
Size: 469122 Color: 2
Size: 269899 Color: 0
Size: 260959 Color: 2

Bin 3248: 21 of cap free
Amount of items: 3
Items: 
Size: 354319 Color: 0
Size: 328784 Color: 0
Size: 316877 Color: 3

Bin 3249: 22 of cap free
Amount of items: 3
Items: 
Size: 363863 Color: 3
Size: 358406 Color: 1
Size: 277710 Color: 3

Bin 3250: 22 of cap free
Amount of items: 3
Items: 
Size: 470785 Color: 2
Size: 267442 Color: 3
Size: 261752 Color: 0

Bin 3251: 22 of cap free
Amount of items: 3
Items: 
Size: 470939 Color: 2
Size: 275553 Color: 3
Size: 253487 Color: 4

Bin 3252: 23 of cap free
Amount of items: 3
Items: 
Size: 471608 Color: 2
Size: 264544 Color: 3
Size: 263826 Color: 4

Bin 3253: 24 of cap free
Amount of items: 3
Items: 
Size: 367262 Color: 3
Size: 359621 Color: 0
Size: 273094 Color: 0

Bin 3254: 25 of cap free
Amount of items: 3
Items: 
Size: 417764 Color: 3
Size: 320462 Color: 4
Size: 261750 Color: 4

Bin 3255: 25 of cap free
Amount of items: 3
Items: 
Size: 358099 Color: 4
Size: 341854 Color: 3
Size: 300023 Color: 1

Bin 3256: 25 of cap free
Amount of items: 3
Items: 
Size: 450307 Color: 4
Size: 275183 Color: 1
Size: 274486 Color: 4

Bin 3257: 25 of cap free
Amount of items: 3
Items: 
Size: 354587 Color: 3
Size: 332450 Color: 2
Size: 312939 Color: 1

Bin 3258: 25 of cap free
Amount of items: 3
Items: 
Size: 356813 Color: 1
Size: 349759 Color: 2
Size: 293404 Color: 2

Bin 3259: 26 of cap free
Amount of items: 3
Items: 
Size: 366297 Color: 1
Size: 341903 Color: 0
Size: 291775 Color: 4

Bin 3260: 26 of cap free
Amount of items: 3
Items: 
Size: 402700 Color: 2
Size: 345717 Color: 3
Size: 251558 Color: 4

Bin 3261: 26 of cap free
Amount of items: 3
Items: 
Size: 471314 Color: 4
Size: 267086 Color: 4
Size: 261575 Color: 2

Bin 3262: 26 of cap free
Amount of items: 3
Items: 
Size: 359226 Color: 3
Size: 324526 Color: 2
Size: 316223 Color: 3

Bin 3263: 27 of cap free
Amount of items: 3
Items: 
Size: 355296 Color: 1
Size: 354632 Color: 0
Size: 290046 Color: 1

Bin 3264: 28 of cap free
Amount of items: 3
Items: 
Size: 361566 Color: 0
Size: 338111 Color: 2
Size: 300296 Color: 0

Bin 3265: 28 of cap free
Amount of items: 3
Items: 
Size: 353978 Color: 2
Size: 330080 Color: 3
Size: 315915 Color: 0

Bin 3266: 28 of cap free
Amount of items: 3
Items: 
Size: 356042 Color: 2
Size: 355559 Color: 2
Size: 288372 Color: 3

Bin 3267: 29 of cap free
Amount of items: 3
Items: 
Size: 357557 Color: 1
Size: 346361 Color: 4
Size: 296054 Color: 1

Bin 3268: 29 of cap free
Amount of items: 3
Items: 
Size: 415920 Color: 2
Size: 293649 Color: 0
Size: 290403 Color: 4

Bin 3269: 30 of cap free
Amount of items: 3
Items: 
Size: 489516 Color: 3
Size: 257706 Color: 2
Size: 252749 Color: 2

Bin 3270: 31 of cap free
Amount of items: 3
Items: 
Size: 354044 Color: 2
Size: 325353 Color: 4
Size: 320573 Color: 4

Bin 3271: 32 of cap free
Amount of items: 3
Items: 
Size: 357613 Color: 1
Size: 331809 Color: 2
Size: 310547 Color: 3

Bin 3272: 33 of cap free
Amount of items: 3
Items: 
Size: 410965 Color: 2
Size: 334369 Color: 3
Size: 254634 Color: 2

Bin 3273: 33 of cap free
Amount of items: 3
Items: 
Size: 345502 Color: 0
Size: 339006 Color: 3
Size: 315460 Color: 4

Bin 3274: 34 of cap free
Amount of items: 3
Items: 
Size: 351880 Color: 2
Size: 341711 Color: 3
Size: 306376 Color: 0

Bin 3275: 34 of cap free
Amount of items: 3
Items: 
Size: 361010 Color: 0
Size: 326400 Color: 0
Size: 312557 Color: 4

Bin 3276: 35 of cap free
Amount of items: 3
Items: 
Size: 475083 Color: 4
Size: 262482 Color: 4
Size: 262401 Color: 1

Bin 3277: 39 of cap free
Amount of items: 3
Items: 
Size: 492137 Color: 4
Size: 255924 Color: 4
Size: 251901 Color: 2

Bin 3278: 39 of cap free
Amount of items: 3
Items: 
Size: 444359 Color: 1
Size: 286040 Color: 4
Size: 269563 Color: 4

Bin 3279: 40 of cap free
Amount of items: 3
Items: 
Size: 370935 Color: 0
Size: 349049 Color: 1
Size: 279977 Color: 1

Bin 3280: 42 of cap free
Amount of items: 3
Items: 
Size: 353436 Color: 4
Size: 340614 Color: 0
Size: 305909 Color: 2

Bin 3281: 43 of cap free
Amount of items: 3
Items: 
Size: 382187 Color: 0
Size: 351167 Color: 3
Size: 266604 Color: 4

Bin 3282: 44 of cap free
Amount of items: 3
Items: 
Size: 461686 Color: 4
Size: 272051 Color: 4
Size: 266220 Color: 3

Bin 3283: 46 of cap free
Amount of items: 3
Items: 
Size: 437063 Color: 4
Size: 288286 Color: 0
Size: 274606 Color: 3

Bin 3284: 47 of cap free
Amount of items: 3
Items: 
Size: 360476 Color: 1
Size: 334075 Color: 0
Size: 305403 Color: 3

Bin 3285: 50 of cap free
Amount of items: 3
Items: 
Size: 342348 Color: 1
Size: 331859 Color: 2
Size: 325744 Color: 3

Bin 3286: 58 of cap free
Amount of items: 3
Items: 
Size: 435238 Color: 3
Size: 307322 Color: 4
Size: 257383 Color: 4

Bin 3287: 60 of cap free
Amount of items: 3
Items: 
Size: 492775 Color: 1
Size: 253604 Color: 3
Size: 253562 Color: 3

Bin 3288: 61 of cap free
Amount of items: 3
Items: 
Size: 462077 Color: 3
Size: 278546 Color: 3
Size: 259317 Color: 2

Bin 3289: 62 of cap free
Amount of items: 3
Items: 
Size: 365471 Color: 1
Size: 348962 Color: 2
Size: 285506 Color: 4

Bin 3290: 63 of cap free
Amount of items: 3
Items: 
Size: 433752 Color: 0
Size: 300904 Color: 4
Size: 265282 Color: 0

Bin 3291: 64 of cap free
Amount of items: 3
Items: 
Size: 399917 Color: 0
Size: 343555 Color: 0
Size: 256465 Color: 4

Bin 3292: 66 of cap free
Amount of items: 3
Items: 
Size: 349318 Color: 0
Size: 342204 Color: 0
Size: 308413 Color: 2

Bin 3293: 70 of cap free
Amount of items: 3
Items: 
Size: 410767 Color: 2
Size: 306950 Color: 1
Size: 282214 Color: 4

Bin 3294: 75 of cap free
Amount of items: 3
Items: 
Size: 477124 Color: 2
Size: 264540 Color: 2
Size: 258262 Color: 3

Bin 3295: 82 of cap free
Amount of items: 3
Items: 
Size: 464019 Color: 0
Size: 273136 Color: 4
Size: 262764 Color: 2

Bin 3296: 84 of cap free
Amount of items: 3
Items: 
Size: 370474 Color: 4
Size: 362676 Color: 3
Size: 266767 Color: 1

Bin 3297: 89 of cap free
Amount of items: 3
Items: 
Size: 486389 Color: 3
Size: 256866 Color: 0
Size: 256657 Color: 2

Bin 3298: 91 of cap free
Amount of items: 3
Items: 
Size: 397001 Color: 2
Size: 352639 Color: 2
Size: 250270 Color: 0

Bin 3299: 108 of cap free
Amount of items: 3
Items: 
Size: 444688 Color: 1
Size: 285665 Color: 0
Size: 269540 Color: 0

Bin 3300: 116 of cap free
Amount of items: 3
Items: 
Size: 385629 Color: 3
Size: 361554 Color: 0
Size: 252702 Color: 2

Bin 3301: 122 of cap free
Amount of items: 3
Items: 
Size: 359480 Color: 0
Size: 348360 Color: 3
Size: 292039 Color: 4

Bin 3302: 126 of cap free
Amount of items: 3
Items: 
Size: 419502 Color: 2
Size: 317436 Color: 3
Size: 262937 Color: 3

Bin 3303: 128 of cap free
Amount of items: 3
Items: 
Size: 499284 Color: 2
Size: 250553 Color: 0
Size: 250036 Color: 0

Bin 3304: 137 of cap free
Amount of items: 3
Items: 
Size: 405849 Color: 4
Size: 333376 Color: 4
Size: 260639 Color: 0

Bin 3305: 138 of cap free
Amount of items: 3
Items: 
Size: 449486 Color: 3
Size: 276973 Color: 2
Size: 273404 Color: 4

Bin 3306: 140 of cap free
Amount of items: 3
Items: 
Size: 469308 Color: 2
Size: 267465 Color: 2
Size: 263088 Color: 4

Bin 3307: 142 of cap free
Amount of items: 3
Items: 
Size: 418025 Color: 2
Size: 322824 Color: 0
Size: 259010 Color: 0

Bin 3308: 170 of cap free
Amount of items: 3
Items: 
Size: 373972 Color: 4
Size: 356364 Color: 3
Size: 269495 Color: 2

Bin 3309: 174 of cap free
Amount of items: 3
Items: 
Size: 383879 Color: 3
Size: 331024 Color: 0
Size: 284924 Color: 1

Bin 3310: 190 of cap free
Amount of items: 3
Items: 
Size: 366883 Color: 2
Size: 352101 Color: 1
Size: 280827 Color: 3

Bin 3311: 222 of cap free
Amount of items: 3
Items: 
Size: 381119 Color: 0
Size: 351221 Color: 3
Size: 267439 Color: 1

Bin 3312: 236 of cap free
Amount of items: 3
Items: 
Size: 362594 Color: 4
Size: 362262 Color: 1
Size: 274909 Color: 4

Bin 3313: 306 of cap free
Amount of items: 2
Items: 
Size: 499885 Color: 1
Size: 499810 Color: 0

Bin 3314: 398 of cap free
Amount of items: 3
Items: 
Size: 365712 Color: 1
Size: 361290 Color: 4
Size: 272601 Color: 0

Bin 3315: 403 of cap free
Amount of items: 3
Items: 
Size: 385380 Color: 4
Size: 334165 Color: 2
Size: 280053 Color: 2

Bin 3316: 493 of cap free
Amount of items: 3
Items: 
Size: 354849 Color: 2
Size: 333136 Color: 0
Size: 311523 Color: 2

Bin 3317: 524 of cap free
Amount of items: 2
Items: 
Size: 499802 Color: 1
Size: 499675 Color: 2

Bin 3318: 1063 of cap free
Amount of items: 3
Items: 
Size: 412968 Color: 0
Size: 319032 Color: 2
Size: 266938 Color: 4

Bin 3319: 1176 of cap free
Amount of items: 3
Items: 
Size: 350375 Color: 4
Size: 325299 Color: 1
Size: 323151 Color: 3

Bin 3320: 1393 of cap free
Amount of items: 3
Items: 
Size: 400384 Color: 4
Size: 339001 Color: 2
Size: 259223 Color: 2

Bin 3321: 1581 of cap free
Amount of items: 2
Items: 
Size: 499523 Color: 2
Size: 498897 Color: 0

Bin 3322: 1811 of cap free
Amount of items: 3
Items: 
Size: 367385 Color: 4
Size: 345550 Color: 4
Size: 285255 Color: 2

Bin 3323: 2261 of cap free
Amount of items: 2
Items: 
Size: 498936 Color: 2
Size: 498804 Color: 0

Bin 3324: 2993 of cap free
Amount of items: 2
Items: 
Size: 498555 Color: 4
Size: 498453 Color: 2

Bin 3325: 3273 of cap free
Amount of items: 2
Items: 
Size: 498512 Color: 4
Size: 498216 Color: 2

Bin 3326: 3461 of cap free
Amount of items: 3
Items: 
Size: 418432 Color: 3
Size: 302109 Color: 3
Size: 275999 Color: 4

Bin 3327: 5766 of cap free
Amount of items: 3
Items: 
Size: 375701 Color: 4
Size: 359349 Color: 4
Size: 259185 Color: 0

Bin 3328: 80517 of cap free
Amount of items: 3
Items: 
Size: 327179 Color: 2
Size: 318343 Color: 1
Size: 273962 Color: 3

Bin 3329: 218983 of cap free
Amount of items: 3
Items: 
Size: 265452 Color: 3
Size: 257885 Color: 3
Size: 257681 Color: 2

Bin 3330: 229965 of cap free
Amount of items: 3
Items: 
Size: 257038 Color: 4
Size: 256601 Color: 4
Size: 256397 Color: 1

Bin 3331: 231192 of cap free
Amount of items: 3
Items: 
Size: 256447 Color: 4
Size: 256290 Color: 2
Size: 256072 Color: 2

Bin 3332: 235045 of cap free
Amount of items: 3
Items: 
Size: 255146 Color: 2
Size: 254978 Color: 4
Size: 254832 Color: 4

Bin 3333: 237415 of cap free
Amount of items: 3
Items: 
Size: 254464 Color: 1
Size: 254335 Color: 0
Size: 253787 Color: 0

Bin 3334: 243143 of cap free
Amount of items: 3
Items: 
Size: 252537 Color: 0
Size: 252259 Color: 0
Size: 252062 Color: 4

Bin 3335: 245004 of cap free
Amount of items: 3
Items: 
Size: 251712 Color: 1
Size: 251652 Color: 0
Size: 251633 Color: 3

Bin 3336: 246692 of cap free
Amount of items: 3
Items: 
Size: 251352 Color: 2
Size: 251031 Color: 4
Size: 250926 Color: 2

Total size: 3334003334
Total free space: 2000002

