Capicity Bin: 9504
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4760 Color: 17
Size: 3954 Color: 14
Size: 790 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5364 Color: 13
Size: 3956 Color: 17
Size: 184 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 7
Size: 3444 Color: 0
Size: 372 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 10
Size: 3192 Color: 2
Size: 240 Color: 10

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 19
Size: 2694 Color: 0
Size: 536 Color: 12

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 5
Size: 2972 Color: 12
Size: 176 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6664 Color: 1
Size: 2676 Color: 11
Size: 164 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7258 Color: 1
Size: 2002 Color: 6
Size: 244 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 10
Size: 1844 Color: 2
Size: 368 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7584 Color: 1
Size: 1296 Color: 1
Size: 624 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 7
Size: 922 Color: 6
Size: 784 Color: 17

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7844 Color: 16
Size: 1228 Color: 12
Size: 432 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7901 Color: 13
Size: 1337 Color: 10
Size: 266 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7920 Color: 6
Size: 1222 Color: 2
Size: 362 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 8004 Color: 11
Size: 1328 Color: 17
Size: 172 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8072 Color: 5
Size: 1320 Color: 12
Size: 112 Color: 11

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8078 Color: 18
Size: 1190 Color: 2
Size: 236 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8084 Color: 9
Size: 1188 Color: 10
Size: 232 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8134 Color: 15
Size: 688 Color: 17
Size: 682 Color: 10

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 10
Size: 1098 Color: 16
Size: 216 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8254 Color: 16
Size: 922 Color: 6
Size: 328 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8308 Color: 7
Size: 972 Color: 18
Size: 224 Color: 15

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8368 Color: 6
Size: 856 Color: 14
Size: 280 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 15
Size: 914 Color: 7
Size: 212 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8410 Color: 19
Size: 688 Color: 16
Size: 406 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8470 Color: 18
Size: 788 Color: 15
Size: 246 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8488 Color: 0
Size: 784 Color: 19
Size: 232 Color: 14

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 5943 Color: 10
Size: 3466 Color: 13
Size: 94 Color: 12

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6300 Color: 0
Size: 2979 Color: 3
Size: 224 Color: 10

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 1
Size: 2285 Color: 2
Size: 284 Color: 19

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 7127 Color: 8
Size: 1574 Color: 17
Size: 802 Color: 6

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 7707 Color: 0
Size: 1460 Color: 3
Size: 336 Color: 15

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 7719 Color: 15
Size: 1608 Color: 19
Size: 176 Color: 11

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 7756 Color: 8
Size: 1499 Color: 7
Size: 248 Color: 2

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 7997 Color: 6
Size: 1208 Color: 14
Size: 298 Color: 17

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 4770 Color: 10
Size: 3942 Color: 19
Size: 790 Color: 17

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 5378 Color: 0
Size: 3964 Color: 8
Size: 160 Color: 11

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 7766 Color: 15
Size: 1736 Color: 7

Bin 39: 2 of cap free
Amount of items: 3
Items: 
Size: 7949 Color: 1
Size: 1297 Color: 18
Size: 256 Color: 11

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 1
Size: 684 Color: 11
Size: 596 Color: 13

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 8514 Color: 7
Size: 604 Color: 10
Size: 384 Color: 6

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 8516 Color: 2
Size: 986 Color: 18

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 8534 Color: 18
Size: 968 Color: 4

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 6
Size: 2601 Color: 3
Size: 400 Color: 18

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 19
Size: 2136 Color: 7
Size: 456 Color: 19

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 7172 Color: 12
Size: 1981 Color: 14
Size: 348 Color: 10

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 7573 Color: 0
Size: 1028 Color: 1
Size: 900 Color: 6

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 7853 Color: 17
Size: 1200 Color: 10
Size: 448 Color: 1

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 8029 Color: 1
Size: 1204 Color: 14
Size: 268 Color: 2

Bin 50: 3 of cap free
Amount of items: 2
Items: 
Size: 8132 Color: 19
Size: 1369 Color: 16

Bin 51: 3 of cap free
Amount of items: 2
Items: 
Size: 8244 Color: 2
Size: 1257 Color: 4

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 4765 Color: 14
Size: 3410 Color: 2
Size: 1325 Color: 8

Bin 53: 4 of cap free
Amount of items: 3
Items: 
Size: 4764 Color: 17
Size: 3946 Color: 1
Size: 790 Color: 19

Bin 54: 4 of cap free
Amount of items: 3
Items: 
Size: 7558 Color: 14
Size: 1874 Color: 4
Size: 68 Color: 13

Bin 55: 4 of cap free
Amount of items: 2
Items: 
Size: 7780 Color: 5
Size: 1720 Color: 13

Bin 56: 4 of cap free
Amount of items: 2
Items: 
Size: 7864 Color: 14
Size: 1636 Color: 16

Bin 57: 4 of cap free
Amount of items: 2
Items: 
Size: 7878 Color: 7
Size: 1622 Color: 13

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 8100 Color: 18
Size: 1400 Color: 7

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 8248 Color: 10
Size: 1252 Color: 7

Bin 60: 4 of cap free
Amount of items: 3
Items: 
Size: 8340 Color: 11
Size: 640 Color: 1
Size: 520 Color: 6

Bin 61: 5 of cap free
Amount of items: 6
Items: 
Size: 4753 Color: 12
Size: 1444 Color: 11
Size: 1166 Color: 10
Size: 1080 Color: 16
Size: 828 Color: 8
Size: 228 Color: 18

Bin 62: 5 of cap free
Amount of items: 3
Items: 
Size: 5346 Color: 15
Size: 3961 Color: 12
Size: 192 Color: 4

Bin 63: 5 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 11
Size: 2163 Color: 2
Size: 144 Color: 3

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 7531 Color: 11
Size: 1808 Color: 18
Size: 160 Color: 10

Bin 65: 5 of cap free
Amount of items: 3
Items: 
Size: 7590 Color: 8
Size: 1611 Color: 1
Size: 298 Color: 3

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 7678 Color: 10
Size: 1821 Color: 11

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 8010 Color: 14
Size: 1489 Color: 16

Bin 68: 6 of cap free
Amount of items: 5
Items: 
Size: 4756 Color: 1
Size: 3442 Color: 2
Size: 692 Color: 13
Size: 320 Color: 15
Size: 288 Color: 10

Bin 69: 6 of cap free
Amount of items: 3
Items: 
Size: 5876 Color: 6
Size: 3414 Color: 16
Size: 208 Color: 13

Bin 70: 6 of cap free
Amount of items: 3
Items: 
Size: 6763 Color: 11
Size: 2475 Color: 18
Size: 260 Color: 10

Bin 71: 6 of cap free
Amount of items: 3
Items: 
Size: 6820 Color: 8
Size: 2262 Color: 2
Size: 416 Color: 14

Bin 72: 6 of cap free
Amount of items: 2
Items: 
Size: 6870 Color: 0
Size: 2628 Color: 13

Bin 73: 6 of cap free
Amount of items: 3
Items: 
Size: 7541 Color: 18
Size: 1725 Color: 15
Size: 232 Color: 2

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 8042 Color: 3
Size: 1456 Color: 19

Bin 75: 6 of cap free
Amount of items: 2
Items: 
Size: 8110 Color: 16
Size: 1388 Color: 15

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 8428 Color: 10
Size: 1070 Color: 6

Bin 77: 7 of cap free
Amount of items: 3
Items: 
Size: 6537 Color: 15
Size: 2600 Color: 2
Size: 360 Color: 5

Bin 78: 7 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 11
Size: 1358 Color: 7
Size: 1231 Color: 5

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 7333 Color: 13
Size: 2164 Color: 18

Bin 80: 8 of cap free
Amount of items: 3
Items: 
Size: 4757 Color: 3
Size: 3951 Color: 14
Size: 788 Color: 5

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 7548 Color: 3
Size: 1948 Color: 12

Bin 82: 9 of cap free
Amount of items: 3
Items: 
Size: 5906 Color: 6
Size: 3421 Color: 1
Size: 168 Color: 12

Bin 83: 10 of cap free
Amount of items: 3
Items: 
Size: 5372 Color: 11
Size: 3962 Color: 10
Size: 160 Color: 1

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6888 Color: 10
Size: 2606 Color: 6

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 8346 Color: 14
Size: 1148 Color: 10

Bin 86: 11 of cap free
Amount of items: 4
Items: 
Size: 5940 Color: 3
Size: 2969 Color: 19
Size: 504 Color: 17
Size: 80 Color: 8

Bin 87: 11 of cap free
Amount of items: 3
Items: 
Size: 6385 Color: 7
Size: 2548 Color: 2
Size: 560 Color: 3

Bin 88: 12 of cap free
Amount of items: 3
Items: 
Size: 8484 Color: 1
Size: 976 Color: 4
Size: 32 Color: 14

Bin 89: 13 of cap free
Amount of items: 3
Items: 
Size: 5410 Color: 8
Size: 3953 Color: 1
Size: 128 Color: 14

Bin 90: 13 of cap free
Amount of items: 3
Items: 
Size: 6518 Color: 19
Size: 2455 Color: 9
Size: 518 Color: 17

Bin 91: 13 of cap free
Amount of items: 2
Items: 
Size: 6751 Color: 8
Size: 2740 Color: 0

Bin 92: 13 of cap free
Amount of items: 2
Items: 
Size: 7934 Color: 17
Size: 1557 Color: 16

Bin 93: 14 of cap free
Amount of items: 3
Items: 
Size: 6392 Color: 1
Size: 3002 Color: 0
Size: 96 Color: 11

Bin 94: 14 of cap free
Amount of items: 3
Items: 
Size: 6794 Color: 11
Size: 2456 Color: 15
Size: 240 Color: 17

Bin 95: 14 of cap free
Amount of items: 3
Items: 
Size: 7228 Color: 7
Size: 2198 Color: 17
Size: 64 Color: 9

Bin 96: 14 of cap free
Amount of items: 2
Items: 
Size: 8068 Color: 13
Size: 1422 Color: 16

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 8442 Color: 5
Size: 1048 Color: 4

Bin 98: 16 of cap free
Amount of items: 28
Items: 
Size: 496 Color: 10
Size: 492 Color: 15
Size: 480 Color: 14
Size: 464 Color: 16
Size: 448 Color: 2
Size: 440 Color: 16
Size: 432 Color: 8
Size: 428 Color: 6
Size: 396 Color: 7
Size: 380 Color: 3
Size: 368 Color: 19
Size: 368 Color: 13
Size: 344 Color: 16
Size: 324 Color: 2
Size: 320 Color: 19
Size: 320 Color: 2
Size: 304 Color: 1
Size: 304 Color: 0
Size: 296 Color: 4
Size: 272 Color: 17
Size: 258 Color: 7
Size: 256 Color: 4
Size: 250 Color: 1
Size: 248 Color: 12
Size: 240 Color: 4
Size: 208 Color: 14
Size: 192 Color: 9
Size: 160 Color: 13

Bin 99: 16 of cap free
Amount of items: 2
Items: 
Size: 7608 Color: 19
Size: 1880 Color: 18

Bin 100: 17 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 5
Size: 3411 Color: 7
Size: 852 Color: 13

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 7414 Color: 7
Size: 2072 Color: 8

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 8344 Color: 16
Size: 1142 Color: 2

Bin 103: 19 of cap free
Amount of items: 2
Items: 
Size: 7067 Color: 16
Size: 2418 Color: 14

Bin 104: 20 of cap free
Amount of items: 2
Items: 
Size: 8542 Color: 6
Size: 942 Color: 17

Bin 105: 21 of cap free
Amount of items: 2
Items: 
Size: 7672 Color: 9
Size: 1811 Color: 13

Bin 106: 23 of cap free
Amount of items: 3
Items: 
Size: 4761 Color: 2
Size: 4192 Color: 16
Size: 528 Color: 14

Bin 107: 23 of cap free
Amount of items: 4
Items: 
Size: 5401 Color: 14
Size: 3576 Color: 6
Size: 376 Color: 4
Size: 128 Color: 2

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 11
Size: 3028 Color: 16

Bin 109: 25 of cap free
Amount of items: 2
Items: 
Size: 7448 Color: 16
Size: 2031 Color: 3

Bin 110: 26 of cap free
Amount of items: 2
Items: 
Size: 6606 Color: 10
Size: 2872 Color: 16

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 10
Size: 2376 Color: 11

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 7256 Color: 4
Size: 2220 Color: 16

Bin 113: 29 of cap free
Amount of items: 2
Items: 
Size: 7863 Color: 19
Size: 1612 Color: 3

Bin 114: 32 of cap free
Amount of items: 2
Items: 
Size: 7572 Color: 8
Size: 1900 Color: 14

Bin 115: 36 of cap free
Amount of items: 3
Items: 
Size: 7290 Color: 0
Size: 1742 Color: 16
Size: 436 Color: 11

Bin 116: 38 of cap free
Amount of items: 2
Items: 
Size: 4778 Color: 4
Size: 4688 Color: 9

Bin 117: 38 of cap free
Amount of items: 4
Items: 
Size: 5874 Color: 6
Size: 1846 Color: 18
Size: 1042 Color: 17
Size: 704 Color: 7

Bin 118: 41 of cap free
Amount of items: 2
Items: 
Size: 7321 Color: 6
Size: 2142 Color: 1

Bin 119: 42 of cap free
Amount of items: 2
Items: 
Size: 8216 Color: 7
Size: 1246 Color: 14

Bin 120: 43 of cap free
Amount of items: 2
Items: 
Size: 7933 Color: 3
Size: 1528 Color: 0

Bin 121: 44 of cap free
Amount of items: 2
Items: 
Size: 6952 Color: 18
Size: 2508 Color: 3

Bin 122: 51 of cap free
Amount of items: 2
Items: 
Size: 5413 Color: 9
Size: 4040 Color: 8

Bin 123: 54 of cap free
Amount of items: 2
Items: 
Size: 7928 Color: 17
Size: 1522 Color: 9

Bin 124: 56 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 0
Size: 2604 Color: 14

Bin 125: 58 of cap free
Amount of items: 3
Items: 
Size: 6373 Color: 4
Size: 2905 Color: 3
Size: 168 Color: 17

Bin 126: 58 of cap free
Amount of items: 2
Items: 
Size: 6420 Color: 7
Size: 3026 Color: 10

Bin 127: 59 of cap free
Amount of items: 2
Items: 
Size: 7800 Color: 1
Size: 1645 Color: 16

Bin 128: 62 of cap free
Amount of items: 2
Items: 
Size: 5938 Color: 5
Size: 3504 Color: 2

Bin 129: 70 of cap free
Amount of items: 5
Items: 
Size: 4754 Color: 17
Size: 1598 Color: 0
Size: 1310 Color: 19
Size: 1172 Color: 16
Size: 600 Color: 13

Bin 130: 81 of cap free
Amount of items: 3
Items: 
Size: 4762 Color: 4
Size: 3957 Color: 12
Size: 704 Color: 13

Bin 131: 90 of cap free
Amount of items: 15
Items: 
Size: 886 Color: 1
Size: 810 Color: 1
Size: 788 Color: 15
Size: 788 Color: 11
Size: 784 Color: 11
Size: 704 Color: 19
Size: 680 Color: 17
Size: 680 Color: 15
Size: 594 Color: 19
Size: 592 Color: 18
Size: 592 Color: 9
Size: 528 Color: 3
Size: 512 Color: 3
Size: 316 Color: 4
Size: 160 Color: 0

Bin 132: 121 of cap free
Amount of items: 2
Items: 
Size: 5931 Color: 5
Size: 3452 Color: 7

Bin 133: 7676 of cap free
Amount of items: 9
Items: 
Size: 272 Color: 0
Size: 238 Color: 17
Size: 228 Color: 15
Size: 226 Color: 8
Size: 208 Color: 6
Size: 180 Color: 18
Size: 160 Color: 13
Size: 160 Color: 9
Size: 156 Color: 11

Total size: 1254528
Total free space: 9504

