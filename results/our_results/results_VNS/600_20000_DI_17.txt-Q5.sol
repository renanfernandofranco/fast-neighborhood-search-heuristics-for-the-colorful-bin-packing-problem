Capicity Bin: 15872
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 20
Items: 
Size: 1320 Color: 0
Size: 1034 Color: 4
Size: 1024 Color: 3
Size: 992 Color: 1
Size: 984 Color: 0
Size: 976 Color: 1
Size: 960 Color: 3
Size: 864 Color: 0
Size: 856 Color: 0
Size: 832 Color: 0
Size: 812 Color: 1
Size: 766 Color: 1
Size: 756 Color: 0
Size: 700 Color: 4
Size: 648 Color: 1
Size: 640 Color: 3
Size: 504 Color: 2
Size: 500 Color: 2
Size: 416 Color: 2
Size: 288 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8504 Color: 1
Size: 6904 Color: 4
Size: 464 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 9544 Color: 3
Size: 5704 Color: 3
Size: 624 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 9916 Color: 4
Size: 5716 Color: 2
Size: 240 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9938 Color: 1
Size: 4946 Color: 1
Size: 988 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9960 Color: 4
Size: 5160 Color: 2
Size: 752 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10040 Color: 1
Size: 4872 Color: 2
Size: 960 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 4
Size: 4696 Color: 1
Size: 304 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 11623 Color: 3
Size: 2815 Color: 1
Size: 1434 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 11938 Color: 2
Size: 3282 Color: 2
Size: 652 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 1
Size: 3652 Color: 0
Size: 160 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12178 Color: 2
Size: 3294 Color: 1
Size: 400 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12283 Color: 0
Size: 2761 Color: 0
Size: 828 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12476 Color: 3
Size: 2804 Color: 4
Size: 592 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12508 Color: 0
Size: 2628 Color: 1
Size: 736 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12616 Color: 0
Size: 3112 Color: 4
Size: 144 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 12712 Color: 1
Size: 2238 Color: 3
Size: 922 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12720 Color: 2
Size: 2952 Color: 1
Size: 200 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12724 Color: 1
Size: 2348 Color: 2
Size: 800 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 12819 Color: 2
Size: 2551 Color: 1
Size: 502 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12851 Color: 2
Size: 2509 Color: 3
Size: 512 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12984 Color: 1
Size: 2632 Color: 3
Size: 256 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 12996 Color: 4
Size: 2488 Color: 2
Size: 388 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13061 Color: 1
Size: 1499 Color: 0
Size: 1312 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13064 Color: 3
Size: 2408 Color: 1
Size: 400 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13143 Color: 3
Size: 2381 Color: 1
Size: 348 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13256 Color: 1
Size: 2276 Color: 4
Size: 340 Color: 2

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 3
Size: 1882 Color: 1
Size: 752 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 3
Size: 2175 Color: 1
Size: 434 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 3
Size: 2184 Color: 3
Size: 412 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13295 Color: 1
Size: 1945 Color: 3
Size: 632 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13332 Color: 1
Size: 1400 Color: 2
Size: 1140 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13352 Color: 4
Size: 1560 Color: 1
Size: 960 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13466 Color: 4
Size: 1686 Color: 3
Size: 720 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 1
Size: 2164 Color: 3
Size: 192 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13576 Color: 4
Size: 1160 Color: 1
Size: 1136 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13565 Color: 1
Size: 1923 Color: 4
Size: 384 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13612 Color: 0
Size: 1332 Color: 2
Size: 928 Color: 3

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13644 Color: 1
Size: 1336 Color: 2
Size: 892 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 3
Size: 1486 Color: 0
Size: 768 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13658 Color: 3
Size: 1914 Color: 2
Size: 300 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13698 Color: 0
Size: 1722 Color: 0
Size: 452 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13704 Color: 0
Size: 1320 Color: 4
Size: 848 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 1916 Color: 3
Size: 192 Color: 2

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 13769 Color: 3
Size: 1707 Color: 0
Size: 396 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 13806 Color: 2
Size: 1394 Color: 3
Size: 672 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 3
Size: 1464 Color: 2
Size: 512 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 13897 Color: 2
Size: 1503 Color: 3
Size: 472 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 13931 Color: 3
Size: 1431 Color: 0
Size: 510 Color: 4

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 13916 Color: 0
Size: 1444 Color: 4
Size: 512 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 13938 Color: 4
Size: 1614 Color: 3
Size: 320 Color: 4

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 13984 Color: 4
Size: 976 Color: 3
Size: 912 Color: 2

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 1
Size: 1565 Color: 3
Size: 312 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 13998 Color: 0
Size: 1600 Color: 1
Size: 274 Color: 3

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14044 Color: 3
Size: 1492 Color: 4
Size: 336 Color: 2

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 4
Size: 1498 Color: 2
Size: 324 Color: 3

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14075 Color: 4
Size: 1455 Color: 4
Size: 342 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14079 Color: 0
Size: 1495 Color: 3
Size: 298 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14100 Color: 1
Size: 1412 Color: 3
Size: 360 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14126 Color: 4
Size: 1458 Color: 1
Size: 288 Color: 3

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14140 Color: 4
Size: 1404 Color: 3
Size: 328 Color: 4

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 14159 Color: 3
Size: 1429 Color: 1
Size: 284 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 14196 Color: 0
Size: 1348 Color: 4
Size: 328 Color: 3

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 14202 Color: 4
Size: 1320 Color: 3
Size: 350 Color: 2

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 14282 Color: 3
Size: 1136 Color: 1
Size: 454 Color: 4

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 14260 Color: 0
Size: 1326 Color: 3
Size: 286 Color: 4

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 14280 Color: 4
Size: 1248 Color: 3
Size: 344 Color: 1

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 7941 Color: 3
Size: 6610 Color: 2
Size: 1320 Color: 0

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 10248 Color: 3
Size: 5179 Color: 1
Size: 444 Color: 3

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 10399 Color: 2
Size: 4936 Color: 3
Size: 536 Color: 1

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 11768 Color: 2
Size: 3799 Color: 2
Size: 304 Color: 3

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 12092 Color: 0
Size: 3443 Color: 1
Size: 336 Color: 0

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 12315 Color: 1
Size: 2468 Color: 0
Size: 1088 Color: 2

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 4
Size: 2519 Color: 1
Size: 562 Color: 0

Bin 75: 1 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 2
Size: 2691 Color: 0
Size: 344 Color: 4

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 12863 Color: 3
Size: 2404 Color: 1
Size: 604 Color: 4

Bin 77: 1 of cap free
Amount of items: 3
Items: 
Size: 13500 Color: 1
Size: 2081 Color: 0
Size: 290 Color: 4

Bin 78: 1 of cap free
Amount of items: 3
Items: 
Size: 13539 Color: 4
Size: 1980 Color: 2
Size: 352 Color: 1

Bin 79: 1 of cap free
Amount of items: 3
Items: 
Size: 13659 Color: 1
Size: 1524 Color: 2
Size: 688 Color: 4

Bin 80: 1 of cap free
Amount of items: 3
Items: 
Size: 13759 Color: 1
Size: 1814 Color: 2
Size: 298 Color: 4

Bin 81: 1 of cap free
Amount of items: 3
Items: 
Size: 13777 Color: 1
Size: 1562 Color: 0
Size: 532 Color: 2

Bin 82: 1 of cap free
Amount of items: 3
Items: 
Size: 13828 Color: 0
Size: 1619 Color: 3
Size: 424 Color: 4

Bin 83: 1 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 1
Size: 1264 Color: 4
Size: 484 Color: 3

Bin 84: 1 of cap free
Amount of items: 3
Items: 
Size: 14155 Color: 2
Size: 1636 Color: 3
Size: 80 Color: 0

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 9605 Color: 4
Size: 5125 Color: 2
Size: 1140 Color: 1

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 9924 Color: 3
Size: 5288 Color: 1
Size: 658 Color: 0

Bin 87: 2 of cap free
Amount of items: 3
Items: 
Size: 9954 Color: 1
Size: 4932 Color: 2
Size: 984 Color: 3

Bin 88: 2 of cap free
Amount of items: 3
Items: 
Size: 10514 Color: 1
Size: 4924 Color: 0
Size: 432 Color: 2

Bin 89: 2 of cap free
Amount of items: 3
Items: 
Size: 10748 Color: 4
Size: 4466 Color: 0
Size: 656 Color: 1

Bin 90: 2 of cap free
Amount of items: 3
Items: 
Size: 11394 Color: 1
Size: 3768 Color: 0
Size: 708 Color: 2

Bin 91: 2 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 0
Size: 4056 Color: 1
Size: 164 Color: 2

Bin 92: 2 of cap free
Amount of items: 3
Items: 
Size: 11900 Color: 0
Size: 2648 Color: 0
Size: 1322 Color: 1

Bin 93: 2 of cap free
Amount of items: 3
Items: 
Size: 12670 Color: 3
Size: 2176 Color: 1
Size: 1024 Color: 0

Bin 94: 2 of cap free
Amount of items: 3
Items: 
Size: 12954 Color: 1
Size: 2532 Color: 3
Size: 384 Color: 2

Bin 95: 2 of cap free
Amount of items: 3
Items: 
Size: 13085 Color: 1
Size: 2187 Color: 1
Size: 598 Color: 2

Bin 96: 3 of cap free
Amount of items: 3
Items: 
Size: 11269 Color: 1
Size: 3764 Color: 4
Size: 836 Color: 4

Bin 97: 3 of cap free
Amount of items: 3
Items: 
Size: 11356 Color: 0
Size: 4145 Color: 1
Size: 368 Color: 3

Bin 98: 3 of cap free
Amount of items: 3
Items: 
Size: 11741 Color: 2
Size: 3656 Color: 1
Size: 472 Color: 4

Bin 99: 3 of cap free
Amount of items: 3
Items: 
Size: 12330 Color: 3
Size: 2979 Color: 1
Size: 560 Color: 2

Bin 100: 3 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 1
Size: 3241 Color: 2
Size: 284 Color: 4

Bin 101: 3 of cap free
Amount of items: 2
Items: 
Size: 12904 Color: 2
Size: 2965 Color: 3

Bin 102: 3 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 0
Size: 2124 Color: 3

Bin 103: 4 of cap free
Amount of items: 2
Items: 
Size: 11677 Color: 3
Size: 4191 Color: 2

Bin 104: 4 of cap free
Amount of items: 3
Items: 
Size: 12232 Color: 3
Size: 3372 Color: 2
Size: 264 Color: 2

Bin 105: 4 of cap free
Amount of items: 3
Items: 
Size: 12376 Color: 2
Size: 1867 Color: 3
Size: 1625 Color: 0

Bin 106: 4 of cap free
Amount of items: 2
Items: 
Size: 13249 Color: 0
Size: 2619 Color: 4

Bin 107: 4 of cap free
Amount of items: 2
Items: 
Size: 13752 Color: 3
Size: 2116 Color: 4

Bin 108: 5 of cap free
Amount of items: 5
Items: 
Size: 7940 Color: 2
Size: 5223 Color: 3
Size: 2164 Color: 4
Size: 276 Color: 0
Size: 264 Color: 3

Bin 109: 5 of cap free
Amount of items: 3
Items: 
Size: 9723 Color: 3
Size: 5708 Color: 0
Size: 436 Color: 1

Bin 110: 5 of cap free
Amount of items: 3
Items: 
Size: 10994 Color: 4
Size: 4561 Color: 0
Size: 312 Color: 1

Bin 111: 5 of cap free
Amount of items: 2
Items: 
Size: 14120 Color: 2
Size: 1747 Color: 0

Bin 112: 5 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 1
Size: 1647 Color: 2

Bin 113: 6 of cap free
Amount of items: 5
Items: 
Size: 7956 Color: 0
Size: 4615 Color: 1
Size: 2275 Color: 4
Size: 648 Color: 3
Size: 372 Color: 1

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 13060 Color: 3
Size: 2806 Color: 0

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 9659 Color: 0
Size: 5686 Color: 1
Size: 520 Color: 4

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 12506 Color: 3
Size: 2991 Color: 1
Size: 368 Color: 0

Bin 117: 8 of cap free
Amount of items: 5
Items: 
Size: 7942 Color: 0
Size: 3432 Color: 1
Size: 2149 Color: 4
Size: 1845 Color: 3
Size: 496 Color: 1

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 9032 Color: 3
Size: 5348 Color: 0
Size: 1484 Color: 1

Bin 119: 9 of cap free
Amount of items: 2
Items: 
Size: 10899 Color: 4
Size: 4964 Color: 0

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 13318 Color: 2
Size: 2545 Color: 0

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 13375 Color: 3
Size: 2488 Color: 4

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 9020 Color: 1
Size: 5706 Color: 4
Size: 1136 Color: 2

Bin 123: 10 of cap free
Amount of items: 2
Items: 
Size: 13902 Color: 4
Size: 1960 Color: 2

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 14154 Color: 2
Size: 1708 Color: 1

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 14218 Color: 0
Size: 1644 Color: 1

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 0
Size: 1853 Color: 4

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 8216 Color: 2
Size: 6604 Color: 0
Size: 1040 Color: 1

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 11316 Color: 4
Size: 4168 Color: 2
Size: 376 Color: 1

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 11368 Color: 0
Size: 4316 Color: 2
Size: 176 Color: 0

Bin 130: 12 of cap free
Amount of items: 3
Items: 
Size: 11868 Color: 2
Size: 3320 Color: 1
Size: 672 Color: 3

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 13190 Color: 2
Size: 2670 Color: 0

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 13426 Color: 3
Size: 2434 Color: 2

Bin 133: 12 of cap free
Amount of items: 2
Items: 
Size: 13758 Color: 4
Size: 2102 Color: 3

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 12811 Color: 4
Size: 3048 Color: 0

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 1
Size: 5754 Color: 3

Bin 136: 14 of cap free
Amount of items: 3
Items: 
Size: 11496 Color: 3
Size: 4066 Color: 2
Size: 296 Color: 1

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13279 Color: 3
Size: 2579 Color: 0

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 13993 Color: 2
Size: 1753 Color: 4
Size: 112 Color: 4

Bin 139: 14 of cap free
Amount of items: 2
Items: 
Size: 14090 Color: 1
Size: 1768 Color: 4

Bin 140: 15 of cap free
Amount of items: 3
Items: 
Size: 11333 Color: 0
Size: 3532 Color: 3
Size: 992 Color: 4

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 13850 Color: 1
Size: 2007 Color: 4

Bin 142: 16 of cap free
Amount of items: 3
Items: 
Size: 9918 Color: 0
Size: 5722 Color: 2
Size: 216 Color: 0

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 13996 Color: 1
Size: 1860 Color: 0

Bin 144: 16 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 4
Size: 1656 Color: 1

Bin 145: 17 of cap free
Amount of items: 3
Items: 
Size: 11896 Color: 2
Size: 3783 Color: 3
Size: 176 Color: 4

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 12699 Color: 3
Size: 3156 Color: 4

Bin 147: 18 of cap free
Amount of items: 2
Items: 
Size: 12559 Color: 3
Size: 3295 Color: 4

Bin 148: 18 of cap free
Amount of items: 2
Items: 
Size: 13284 Color: 0
Size: 2570 Color: 3

Bin 149: 19 of cap free
Amount of items: 2
Items: 
Size: 10845 Color: 0
Size: 5008 Color: 2

Bin 150: 21 of cap free
Amount of items: 3
Items: 
Size: 8024 Color: 2
Size: 6611 Color: 3
Size: 1216 Color: 0

Bin 151: 21 of cap free
Amount of items: 2
Items: 
Size: 13159 Color: 0
Size: 2692 Color: 4

Bin 152: 21 of cap free
Amount of items: 2
Items: 
Size: 13528 Color: 3
Size: 2323 Color: 0

Bin 153: 21 of cap free
Amount of items: 2
Items: 
Size: 13809 Color: 4
Size: 2042 Color: 0

Bin 154: 21 of cap free
Amount of items: 2
Items: 
Size: 13923 Color: 4
Size: 1928 Color: 2

Bin 155: 22 of cap free
Amount of items: 2
Items: 
Size: 11828 Color: 1
Size: 4022 Color: 3

Bin 156: 23 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 0
Size: 1642 Color: 2
Size: 80 Color: 0

Bin 157: 25 of cap free
Amount of items: 2
Items: 
Size: 10335 Color: 0
Size: 5512 Color: 3

Bin 158: 25 of cap free
Amount of items: 3
Items: 
Size: 13465 Color: 2
Size: 2334 Color: 0
Size: 48 Color: 2

Bin 159: 26 of cap free
Amount of items: 3
Items: 
Size: 8911 Color: 3
Size: 6613 Color: 4
Size: 322 Color: 1

Bin 160: 28 of cap free
Amount of items: 2
Items: 
Size: 9972 Color: 3
Size: 5872 Color: 4

Bin 161: 30 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 0
Size: 2954 Color: 2

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 14078 Color: 2
Size: 1764 Color: 1

Bin 163: 31 of cap free
Amount of items: 2
Items: 
Size: 13580 Color: 4
Size: 2261 Color: 3

Bin 164: 32 of cap free
Amount of items: 2
Items: 
Size: 9688 Color: 2
Size: 6152 Color: 3

Bin 165: 32 of cap free
Amount of items: 2
Items: 
Size: 14276 Color: 1
Size: 1564 Color: 2

Bin 166: 36 of cap free
Amount of items: 2
Items: 
Size: 12916 Color: 2
Size: 2920 Color: 3

Bin 167: 36 of cap free
Amount of items: 2
Items: 
Size: 13900 Color: 0
Size: 1936 Color: 4

Bin 168: 37 of cap free
Amount of items: 2
Items: 
Size: 13074 Color: 3
Size: 2761 Color: 4

Bin 169: 38 of cap free
Amount of items: 3
Items: 
Size: 9272 Color: 0
Size: 5392 Color: 0
Size: 1170 Color: 1

Bin 170: 38 of cap free
Amount of items: 2
Items: 
Size: 11558 Color: 0
Size: 4276 Color: 3

Bin 171: 39 of cap free
Amount of items: 2
Items: 
Size: 11983 Color: 3
Size: 3850 Color: 4

Bin 172: 40 of cap free
Amount of items: 2
Items: 
Size: 7944 Color: 1
Size: 7888 Color: 4

Bin 173: 41 of cap free
Amount of items: 2
Items: 
Size: 13633 Color: 3
Size: 2198 Color: 4

Bin 174: 41 of cap free
Amount of items: 2
Items: 
Size: 13825 Color: 3
Size: 2006 Color: 0

Bin 175: 42 of cap free
Amount of items: 2
Items: 
Size: 14069 Color: 0
Size: 1761 Color: 1

Bin 176: 44 of cap free
Amount of items: 2
Items: 
Size: 13015 Color: 2
Size: 2813 Color: 3

Bin 177: 46 of cap free
Amount of items: 2
Items: 
Size: 14180 Color: 2
Size: 1646 Color: 0

Bin 178: 47 of cap free
Amount of items: 2
Items: 
Size: 12645 Color: 3
Size: 3180 Color: 2

Bin 179: 48 of cap free
Amount of items: 2
Items: 
Size: 11016 Color: 0
Size: 4808 Color: 2

Bin 180: 48 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 3
Size: 3180 Color: 4

Bin 181: 49 of cap free
Amount of items: 2
Items: 
Size: 8847 Color: 4
Size: 6976 Color: 2

Bin 182: 51 of cap free
Amount of items: 2
Items: 
Size: 12299 Color: 3
Size: 3522 Color: 2

Bin 183: 54 of cap free
Amount of items: 3
Items: 
Size: 9026 Color: 4
Size: 6392 Color: 2
Size: 400 Color: 1

Bin 184: 55 of cap free
Amount of items: 3
Items: 
Size: 9872 Color: 2
Size: 5801 Color: 3
Size: 144 Color: 4

Bin 185: 61 of cap free
Amount of items: 2
Items: 
Size: 9956 Color: 4
Size: 5855 Color: 3

Bin 186: 66 of cap free
Amount of items: 3
Items: 
Size: 10700 Color: 4
Size: 4962 Color: 3
Size: 144 Color: 3

Bin 187: 70 of cap free
Amount of items: 2
Items: 
Size: 12779 Color: 3
Size: 3023 Color: 2

Bin 188: 86 of cap free
Amount of items: 2
Items: 
Size: 12245 Color: 4
Size: 3541 Color: 3

Bin 189: 88 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 4
Size: 3804 Color: 2

Bin 190: 108 of cap free
Amount of items: 35
Items: 
Size: 608 Color: 3
Size: 594 Color: 0
Size: 592 Color: 0
Size: 588 Color: 0
Size: 576 Color: 3
Size: 576 Color: 1
Size: 560 Color: 0
Size: 552 Color: 4
Size: 536 Color: 3
Size: 514 Color: 3
Size: 488 Color: 0
Size: 480 Color: 4
Size: 480 Color: 3
Size: 476 Color: 2
Size: 464 Color: 2
Size: 464 Color: 2
Size: 464 Color: 0
Size: 456 Color: 1
Size: 436 Color: 4
Size: 432 Color: 2
Size: 428 Color: 3
Size: 424 Color: 0
Size: 416 Color: 4
Size: 416 Color: 2
Size: 404 Color: 2
Size: 392 Color: 0
Size: 372 Color: 1
Size: 368 Color: 1
Size: 352 Color: 4
Size: 328 Color: 4
Size: 320 Color: 2
Size: 320 Color: 1
Size: 312 Color: 4
Size: 288 Color: 1
Size: 288 Color: 1

Bin 191: 116 of cap free
Amount of items: 2
Items: 
Size: 11919 Color: 3
Size: 3837 Color: 4

Bin 192: 133 of cap free
Amount of items: 2
Items: 
Size: 12495 Color: 2
Size: 3244 Color: 4

Bin 193: 163 of cap free
Amount of items: 5
Items: 
Size: 7938 Color: 2
Size: 2130 Color: 3
Size: 2104 Color: 4
Size: 1816 Color: 3
Size: 1721 Color: 0

Bin 194: 164 of cap free
Amount of items: 2
Items: 
Size: 9028 Color: 2
Size: 6680 Color: 3

Bin 195: 208 of cap free
Amount of items: 2
Items: 
Size: 8456 Color: 4
Size: 7208 Color: 2

Bin 196: 208 of cap free
Amount of items: 2
Items: 
Size: 9050 Color: 3
Size: 6614 Color: 2

Bin 197: 250 of cap free
Amount of items: 2
Items: 
Size: 9010 Color: 4
Size: 6612 Color: 0

Bin 198: 259 of cap free
Amount of items: 7
Items: 
Size: 7937 Color: 3
Size: 1567 Color: 1
Size: 1459 Color: 0
Size: 1382 Color: 2
Size: 1136 Color: 1
Size: 1088 Color: 2
Size: 1044 Color: 2

Bin 199: 12094 of cap free
Amount of items: 13
Items: 
Size: 352 Color: 3
Size: 320 Color: 2
Size: 312 Color: 3
Size: 296 Color: 3
Size: 290 Color: 4
Size: 288 Color: 4
Size: 288 Color: 0
Size: 280 Color: 1
Size: 272 Color: 4
Size: 272 Color: 2
Size: 272 Color: 2
Size: 272 Color: 1
Size: 264 Color: 1

Total size: 3142656
Total free space: 15872

