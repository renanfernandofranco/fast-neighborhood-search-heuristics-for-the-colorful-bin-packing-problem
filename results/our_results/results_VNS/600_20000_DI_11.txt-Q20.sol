Capicity Bin: 14480
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 22
Items: 
Size: 928 Color: 17
Size: 912 Color: 16
Size: 866 Color: 11
Size: 854 Color: 11
Size: 852 Color: 2
Size: 752 Color: 0
Size: 700 Color: 17
Size: 696 Color: 12
Size: 672 Color: 18
Size: 670 Color: 15
Size: 664 Color: 11
Size: 664 Color: 4
Size: 640 Color: 15
Size: 640 Color: 6
Size: 636 Color: 5
Size: 620 Color: 2
Size: 560 Color: 1
Size: 544 Color: 5
Size: 448 Color: 9
Size: 414 Color: 14
Size: 380 Color: 8
Size: 368 Color: 7

Bin 2: 0 of cap free
Amount of items: 9
Items: 
Size: 7242 Color: 2
Size: 1310 Color: 2
Size: 1204 Color: 16
Size: 1200 Color: 3
Size: 1200 Color: 3
Size: 1024 Color: 15
Size: 752 Color: 9
Size: 308 Color: 3
Size: 240 Color: 14

Bin 3: 0 of cap free
Amount of items: 5
Items: 
Size: 8632 Color: 6
Size: 4540 Color: 0
Size: 776 Color: 14
Size: 272 Color: 9
Size: 260 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 8934 Color: 16
Size: 5218 Color: 12
Size: 328 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 9192 Color: 19
Size: 4588 Color: 8
Size: 700 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 9614 Color: 2
Size: 4538 Color: 10
Size: 328 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 10459 Color: 17
Size: 3351 Color: 2
Size: 670 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 10600 Color: 12
Size: 3480 Color: 15
Size: 400 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 10911 Color: 5
Size: 2975 Color: 5
Size: 594 Color: 18

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 10919 Color: 17
Size: 2969 Color: 5
Size: 592 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 11052 Color: 11
Size: 3102 Color: 2
Size: 326 Color: 18

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 11064 Color: 12
Size: 2768 Color: 7
Size: 648 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 11160 Color: 13
Size: 3016 Color: 6
Size: 304 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 11196 Color: 3
Size: 2244 Color: 5
Size: 1040 Color: 15

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 11283 Color: 3
Size: 2921 Color: 10
Size: 276 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 6
Size: 2776 Color: 1
Size: 400 Color: 5

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 11476 Color: 9
Size: 2508 Color: 14
Size: 496 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 11560 Color: 9
Size: 2192 Color: 3
Size: 728 Color: 18

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 11593 Color: 7
Size: 1751 Color: 0
Size: 1136 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 11751 Color: 18
Size: 2275 Color: 0
Size: 454 Color: 5

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 17
Size: 2266 Color: 0
Size: 452 Color: 15

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 11767 Color: 16
Size: 2261 Color: 13
Size: 452 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 11768 Color: 1
Size: 2404 Color: 2
Size: 308 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 11985 Color: 14
Size: 1571 Color: 2
Size: 924 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 11989 Color: 3
Size: 2051 Color: 1
Size: 440 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 12021 Color: 0
Size: 1479 Color: 9
Size: 980 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 12060 Color: 16
Size: 1512 Color: 9
Size: 908 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 12186 Color: 3
Size: 1558 Color: 7
Size: 736 Color: 12

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 12194 Color: 12
Size: 1502 Color: 0
Size: 784 Color: 8

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 16
Size: 2018 Color: 10
Size: 214 Color: 5

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12252 Color: 16
Size: 1420 Color: 0
Size: 808 Color: 16

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12350 Color: 5
Size: 1778 Color: 14
Size: 352 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 18
Size: 1364 Color: 9
Size: 680 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 14
Size: 1484 Color: 0
Size: 508 Color: 16

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12502 Color: 6
Size: 1514 Color: 19
Size: 464 Color: 7

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 12519 Color: 13
Size: 1599 Color: 16
Size: 362 Color: 8

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 12536 Color: 13
Size: 1040 Color: 14
Size: 904 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 12604 Color: 10
Size: 1476 Color: 3
Size: 400 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 12646 Color: 1
Size: 1318 Color: 9
Size: 516 Color: 9

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 3
Size: 1404 Color: 7
Size: 408 Color: 6

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 12691 Color: 12
Size: 1491 Color: 10
Size: 298 Color: 13

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 12700 Color: 6
Size: 1292 Color: 17
Size: 488 Color: 6

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 12754 Color: 0
Size: 1304 Color: 5
Size: 422 Color: 10

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 12742 Color: 14
Size: 1324 Color: 11
Size: 414 Color: 4

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 7
Size: 904 Color: 9
Size: 832 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 12824 Color: 0
Size: 1040 Color: 6
Size: 616 Color: 17

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 12780 Color: 7
Size: 1406 Color: 15
Size: 294 Color: 14

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 9
Size: 1342 Color: 16
Size: 352 Color: 10

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 12874 Color: 0
Size: 1054 Color: 7
Size: 552 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 12822 Color: 2
Size: 1206 Color: 2
Size: 452 Color: 19

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 12828 Color: 9
Size: 1204 Color: 19
Size: 448 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 12910 Color: 0
Size: 904 Color: 12
Size: 666 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 13
Size: 896 Color: 17
Size: 682 Color: 8

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 12918 Color: 7
Size: 1302 Color: 17
Size: 260 Color: 2

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 2
Size: 1192 Color: 0
Size: 368 Color: 11

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 10
Size: 1204 Color: 8
Size: 304 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 12988 Color: 14
Size: 1260 Color: 0
Size: 232 Color: 18

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 8203 Color: 16
Size: 6028 Color: 18
Size: 248 Color: 12

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 10451 Color: 13
Size: 3340 Color: 8
Size: 688 Color: 19

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 10460 Color: 17
Size: 3763 Color: 8
Size: 256 Color: 0

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 10620 Color: 10
Size: 2979 Color: 10
Size: 880 Color: 0

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10843 Color: 1
Size: 3436 Color: 0
Size: 200 Color: 15

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 11132 Color: 4
Size: 2971 Color: 12
Size: 376 Color: 18

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11267 Color: 3
Size: 2012 Color: 11
Size: 1200 Color: 2

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 11363 Color: 12
Size: 2778 Color: 10
Size: 338 Color: 11

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12062 Color: 0
Size: 1883 Color: 0
Size: 534 Color: 7

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12443 Color: 17
Size: 1564 Color: 2
Size: 472 Color: 13

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12595 Color: 4
Size: 1168 Color: 6
Size: 716 Color: 13

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12707 Color: 0
Size: 1244 Color: 15
Size: 528 Color: 11

Bin 70: 2 of cap free
Amount of items: 36
Items: 
Size: 606 Color: 11
Size: 594 Color: 17
Size: 594 Color: 3
Size: 568 Color: 11
Size: 560 Color: 10
Size: 542 Color: 10
Size: 480 Color: 6
Size: 480 Color: 4
Size: 480 Color: 1
Size: 440 Color: 19
Size: 440 Color: 2
Size: 432 Color: 0
Size: 424 Color: 1
Size: 412 Color: 14
Size: 400 Color: 4
Size: 384 Color: 9
Size: 384 Color: 1
Size: 380 Color: 3
Size: 378 Color: 8
Size: 376 Color: 14
Size: 368 Color: 0
Size: 352 Color: 13
Size: 348 Color: 1
Size: 344 Color: 16
Size: 344 Color: 8
Size: 336 Color: 10
Size: 320 Color: 8
Size: 320 Color: 7
Size: 318 Color: 4
Size: 314 Color: 0
Size: 312 Color: 9
Size: 296 Color: 9
Size: 296 Color: 6
Size: 288 Color: 7
Size: 288 Color: 7
Size: 280 Color: 14

Bin 71: 2 of cap free
Amount of items: 5
Items: 
Size: 7258 Color: 12
Size: 6024 Color: 9
Size: 668 Color: 8
Size: 288 Color: 18
Size: 240 Color: 6

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 8898 Color: 0
Size: 5212 Color: 0
Size: 368 Color: 11

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 9355 Color: 18
Size: 4571 Color: 7
Size: 552 Color: 5

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9812 Color: 7
Size: 3031 Color: 2
Size: 1635 Color: 5

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 10266 Color: 2
Size: 4212 Color: 8

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 10915 Color: 8
Size: 3359 Color: 17
Size: 204 Color: 16

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 11478 Color: 1
Size: 2740 Color: 10
Size: 260 Color: 13

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11734 Color: 12
Size: 1530 Color: 3
Size: 1214 Color: 18

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11754 Color: 19
Size: 2312 Color: 12
Size: 412 Color: 7

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 12354 Color: 18
Size: 1450 Color: 0
Size: 674 Color: 17

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 12614 Color: 5
Size: 1864 Color: 18

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 12770 Color: 12
Size: 1708 Color: 17

Bin 83: 3 of cap free
Amount of items: 5
Items: 
Size: 7250 Color: 4
Size: 3356 Color: 10
Size: 2983 Color: 12
Size: 568 Color: 19
Size: 320 Color: 2

Bin 84: 3 of cap free
Amount of items: 4
Items: 
Size: 7252 Color: 7
Size: 4291 Color: 0
Size: 2502 Color: 19
Size: 432 Color: 14

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 19
Size: 6026 Color: 11
Size: 296 Color: 1

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 9038 Color: 0
Size: 5191 Color: 10
Size: 248 Color: 9

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 9359 Color: 2
Size: 2774 Color: 15
Size: 2344 Color: 15

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 9656 Color: 0
Size: 4269 Color: 6
Size: 552 Color: 2

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 10274 Color: 18
Size: 3345 Color: 6
Size: 858 Color: 6

Bin 90: 3 of cap free
Amount of items: 2
Items: 
Size: 11486 Color: 7
Size: 2991 Color: 19

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 11573 Color: 8
Size: 2008 Color: 3
Size: 896 Color: 4

Bin 92: 3 of cap free
Amount of items: 2
Items: 
Size: 11992 Color: 8
Size: 2485 Color: 11

Bin 93: 3 of cap free
Amount of items: 2
Items: 
Size: 12932 Color: 15
Size: 1545 Color: 19

Bin 94: 3 of cap free
Amount of items: 2
Items: 
Size: 13016 Color: 8
Size: 1461 Color: 3

Bin 95: 4 of cap free
Amount of items: 7
Items: 
Size: 7244 Color: 9
Size: 1914 Color: 0
Size: 1592 Color: 12
Size: 1500 Color: 19
Size: 1442 Color: 12
Size: 544 Color: 16
Size: 240 Color: 8

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 8228 Color: 1
Size: 5896 Color: 7
Size: 352 Color: 14

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 9828 Color: 15
Size: 4408 Color: 0
Size: 240 Color: 7

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 10762 Color: 5
Size: 2922 Color: 7
Size: 792 Color: 19

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 11828 Color: 14
Size: 2648 Color: 5

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 12202 Color: 19
Size: 2274 Color: 15

Bin 101: 4 of cap free
Amount of items: 2
Items: 
Size: 12388 Color: 3
Size: 2088 Color: 6

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 9331 Color: 2
Size: 5144 Color: 8

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 9756 Color: 3
Size: 4271 Color: 13
Size: 448 Color: 15

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 11359 Color: 0
Size: 2876 Color: 7
Size: 240 Color: 12

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 11796 Color: 19
Size: 2679 Color: 9

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 12068 Color: 6
Size: 2407 Color: 9

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 12244 Color: 6
Size: 2231 Color: 17

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 12727 Color: 7
Size: 1748 Color: 3

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 10282 Color: 5
Size: 3594 Color: 15
Size: 598 Color: 9

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 11998 Color: 14
Size: 2476 Color: 11

Bin 111: 6 of cap free
Amount of items: 2
Items: 
Size: 12404 Color: 11
Size: 2070 Color: 14

Bin 112: 6 of cap free
Amount of items: 2
Items: 
Size: 13026 Color: 12
Size: 1448 Color: 16

Bin 113: 7 of cap free
Amount of items: 7
Items: 
Size: 7241 Color: 12
Size: 1384 Color: 8
Size: 1382 Color: 13
Size: 1380 Color: 13
Size: 1288 Color: 6
Size: 1038 Color: 15
Size: 760 Color: 6

Bin 114: 7 of cap free
Amount of items: 3
Items: 
Size: 8178 Color: 0
Size: 6031 Color: 5
Size: 264 Color: 14

Bin 115: 7 of cap free
Amount of items: 3
Items: 
Size: 8222 Color: 13
Size: 5611 Color: 15
Size: 640 Color: 1

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 12344 Color: 5
Size: 2129 Color: 11

Bin 117: 8 of cap free
Amount of items: 3
Items: 
Size: 7640 Color: 12
Size: 6336 Color: 12
Size: 496 Color: 19

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 11770 Color: 11
Size: 1774 Color: 5
Size: 928 Color: 7

Bin 119: 8 of cap free
Amount of items: 2
Items: 
Size: 12680 Color: 6
Size: 1792 Color: 7

Bin 120: 9 of cap free
Amount of items: 3
Items: 
Size: 12490 Color: 16
Size: 1901 Color: 14
Size: 80 Color: 2

Bin 121: 10 of cap free
Amount of items: 3
Items: 
Size: 11482 Color: 18
Size: 2188 Color: 18
Size: 800 Color: 7

Bin 122: 10 of cap free
Amount of items: 3
Items: 
Size: 12312 Color: 17
Size: 2078 Color: 5
Size: 80 Color: 19

Bin 123: 11 of cap free
Amount of items: 2
Items: 
Size: 12563 Color: 16
Size: 1906 Color: 5

Bin 124: 12 of cap free
Amount of items: 3
Items: 
Size: 9283 Color: 12
Size: 4905 Color: 7
Size: 280 Color: 14

Bin 125: 12 of cap free
Amount of items: 2
Items: 
Size: 11672 Color: 8
Size: 2796 Color: 1

Bin 126: 12 of cap free
Amount of items: 2
Items: 
Size: 12684 Color: 7
Size: 1784 Color: 15

Bin 127: 12 of cap free
Amount of items: 2
Items: 
Size: 12796 Color: 11
Size: 1672 Color: 4

Bin 128: 12 of cap free
Amount of items: 2
Items: 
Size: 12926 Color: 15
Size: 1542 Color: 11

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 12952 Color: 13
Size: 1516 Color: 7

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 13012 Color: 13
Size: 1456 Color: 15

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 12768 Color: 11
Size: 1699 Color: 4

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 9924 Color: 13
Size: 4542 Color: 7

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 10170 Color: 9
Size: 4296 Color: 14

Bin 134: 14 of cap free
Amount of items: 3
Items: 
Size: 10312 Color: 0
Size: 3106 Color: 8
Size: 1048 Color: 12

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12088 Color: 11
Size: 2378 Color: 3

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 12892 Color: 15
Size: 1574 Color: 12

Bin 137: 15 of cap free
Amount of items: 3
Items: 
Size: 8251 Color: 13
Size: 5974 Color: 12
Size: 240 Color: 16

Bin 138: 15 of cap free
Amount of items: 2
Items: 
Size: 9965 Color: 12
Size: 4500 Color: 7

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 12201 Color: 6
Size: 2264 Color: 10

Bin 140: 16 of cap free
Amount of items: 2
Items: 
Size: 12852 Color: 3
Size: 1612 Color: 10

Bin 141: 18 of cap free
Amount of items: 3
Items: 
Size: 10040 Color: 18
Size: 3194 Color: 16
Size: 1228 Color: 12

Bin 142: 18 of cap free
Amount of items: 2
Items: 
Size: 12381 Color: 5
Size: 2081 Color: 15

Bin 143: 18 of cap free
Amount of items: 2
Items: 
Size: 12594 Color: 18
Size: 1868 Color: 8

Bin 144: 18 of cap free
Amount of items: 3
Items: 
Size: 12716 Color: 6
Size: 1650 Color: 1
Size: 96 Color: 5

Bin 145: 19 of cap free
Amount of items: 2
Items: 
Size: 8997 Color: 16
Size: 5464 Color: 2

Bin 146: 20 of cap free
Amount of items: 2
Items: 
Size: 9084 Color: 15
Size: 5376 Color: 12

Bin 147: 21 of cap free
Amount of items: 2
Items: 
Size: 10435 Color: 4
Size: 4024 Color: 1

Bin 148: 22 of cap free
Amount of items: 3
Items: 
Size: 10364 Color: 0
Size: 3502 Color: 7
Size: 592 Color: 2

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 10476 Color: 15
Size: 3982 Color: 12

Bin 150: 22 of cap free
Amount of items: 2
Items: 
Size: 10754 Color: 1
Size: 3704 Color: 17

Bin 151: 22 of cap free
Amount of items: 3
Items: 
Size: 12305 Color: 5
Size: 2077 Color: 10
Size: 76 Color: 5

Bin 152: 22 of cap free
Amount of items: 2
Items: 
Size: 12726 Color: 19
Size: 1732 Color: 8

Bin 153: 24 of cap free
Amount of items: 3
Items: 
Size: 11150 Color: 1
Size: 3240 Color: 3
Size: 66 Color: 19

Bin 154: 24 of cap free
Amount of items: 2
Items: 
Size: 11596 Color: 12
Size: 2860 Color: 13

Bin 155: 24 of cap free
Amount of items: 2
Items: 
Size: 12794 Color: 4
Size: 1662 Color: 11

Bin 156: 26 of cap free
Amount of items: 3
Items: 
Size: 10094 Color: 6
Size: 3208 Color: 15
Size: 1152 Color: 7

Bin 157: 26 of cap free
Amount of items: 2
Items: 
Size: 10650 Color: 7
Size: 3804 Color: 5

Bin 158: 30 of cap free
Amount of items: 3
Items: 
Size: 9036 Color: 10
Size: 5254 Color: 18
Size: 160 Color: 7

Bin 159: 30 of cap free
Amount of items: 2
Items: 
Size: 12548 Color: 1
Size: 1902 Color: 4

Bin 160: 31 of cap free
Amount of items: 2
Items: 
Size: 11927 Color: 13
Size: 2522 Color: 6

Bin 161: 32 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 15
Size: 3892 Color: 7

Bin 162: 33 of cap free
Amount of items: 2
Items: 
Size: 11036 Color: 8
Size: 3411 Color: 14

Bin 163: 34 of cap free
Amount of items: 2
Items: 
Size: 12630 Color: 17
Size: 1816 Color: 8

Bin 164: 35 of cap free
Amount of items: 3
Items: 
Size: 8220 Color: 3
Size: 6033 Color: 15
Size: 192 Color: 15

Bin 165: 35 of cap free
Amount of items: 2
Items: 
Size: 10387 Color: 15
Size: 4058 Color: 14

Bin 166: 35 of cap free
Amount of items: 2
Items: 
Size: 11860 Color: 14
Size: 2585 Color: 19

Bin 167: 35 of cap free
Amount of items: 2
Items: 
Size: 12183 Color: 3
Size: 2262 Color: 9

Bin 168: 36 of cap free
Amount of items: 2
Items: 
Size: 12584 Color: 8
Size: 1860 Color: 3

Bin 169: 37 of cap free
Amount of items: 2
Items: 
Size: 9919 Color: 10
Size: 4524 Color: 17

Bin 170: 38 of cap free
Amount of items: 3
Items: 
Size: 9030 Color: 19
Size: 5220 Color: 18
Size: 192 Color: 18

Bin 171: 40 of cap free
Amount of items: 2
Items: 
Size: 11516 Color: 17
Size: 2924 Color: 18

Bin 172: 40 of cap free
Amount of items: 2
Items: 
Size: 12627 Color: 2
Size: 1813 Color: 3

Bin 173: 41 of cap free
Amount of items: 2
Items: 
Size: 11219 Color: 7
Size: 3220 Color: 16

Bin 174: 41 of cap free
Amount of items: 2
Items: 
Size: 11720 Color: 10
Size: 2719 Color: 2

Bin 175: 47 of cap free
Amount of items: 3
Items: 
Size: 7314 Color: 14
Size: 5641 Color: 0
Size: 1478 Color: 2

Bin 176: 47 of cap free
Amount of items: 2
Items: 
Size: 10632 Color: 13
Size: 3801 Color: 6

Bin 177: 47 of cap free
Amount of items: 2
Items: 
Size: 12221 Color: 5
Size: 2212 Color: 17

Bin 178: 48 of cap free
Amount of items: 2
Items: 
Size: 9752 Color: 4
Size: 4680 Color: 10

Bin 179: 50 of cap free
Amount of items: 2
Items: 
Size: 11154 Color: 19
Size: 3276 Color: 17

Bin 180: 50 of cap free
Amount of items: 2
Items: 
Size: 11990 Color: 0
Size: 2440 Color: 12

Bin 181: 53 of cap free
Amount of items: 3
Items: 
Size: 8872 Color: 7
Size: 4331 Color: 17
Size: 1224 Color: 19

Bin 182: 56 of cap free
Amount of items: 2
Items: 
Size: 11568 Color: 1
Size: 2856 Color: 2

Bin 183: 59 of cap free
Amount of items: 2
Items: 
Size: 10907 Color: 14
Size: 3514 Color: 16

Bin 184: 64 of cap free
Amount of items: 2
Items: 
Size: 11379 Color: 17
Size: 3037 Color: 18

Bin 185: 73 of cap free
Amount of items: 2
Items: 
Size: 10467 Color: 1
Size: 3940 Color: 16

Bin 186: 76 of cap free
Amount of items: 2
Items: 
Size: 10746 Color: 4
Size: 3658 Color: 15

Bin 187: 83 of cap free
Amount of items: 2
Items: 
Size: 10891 Color: 16
Size: 3506 Color: 10

Bin 188: 85 of cap free
Amount of items: 5
Items: 
Size: 7245 Color: 9
Size: 2020 Color: 2
Size: 2000 Color: 6
Size: 1624 Color: 0
Size: 1506 Color: 8

Bin 189: 88 of cap free
Amount of items: 2
Items: 
Size: 7256 Color: 13
Size: 7136 Color: 18

Bin 190: 106 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 15
Size: 3502 Color: 4

Bin 191: 113 of cap free
Amount of items: 2
Items: 
Size: 8331 Color: 1
Size: 6036 Color: 8

Bin 192: 113 of cap free
Amount of items: 2
Items: 
Size: 10996 Color: 15
Size: 3371 Color: 3

Bin 193: 120 of cap free
Amount of items: 2
Items: 
Size: 9706 Color: 9
Size: 4654 Color: 1

Bin 194: 126 of cap free
Amount of items: 2
Items: 
Size: 9732 Color: 0
Size: 4622 Color: 6

Bin 195: 130 of cap free
Amount of items: 2
Items: 
Size: 7416 Color: 14
Size: 6934 Color: 7

Bin 196: 134 of cap free
Amount of items: 2
Items: 
Size: 8312 Color: 5
Size: 6034 Color: 19

Bin 197: 157 of cap free
Amount of items: 2
Items: 
Size: 9052 Color: 14
Size: 5271 Color: 16

Bin 198: 181 of cap free
Amount of items: 2
Items: 
Size: 8595 Color: 18
Size: 5704 Color: 1

Bin 199: 10924 of cap free
Amount of items: 13
Items: 
Size: 312 Color: 16
Size: 308 Color: 14
Size: 292 Color: 2
Size: 288 Color: 16
Size: 288 Color: 3
Size: 284 Color: 19
Size: 280 Color: 9
Size: 272 Color: 8
Size: 264 Color: 12
Size: 264 Color: 7
Size: 256 Color: 10
Size: 256 Color: 6
Size: 192 Color: 13

Total size: 2867040
Total free space: 14480

