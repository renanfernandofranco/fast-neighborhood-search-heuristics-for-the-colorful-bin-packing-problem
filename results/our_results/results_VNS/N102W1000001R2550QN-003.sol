Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 102

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 326289 Color: 57
Size: 299907 Color: 37
Size: 373805 Color: 72

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 349979 Color: 67
Size: 334263 Color: 60
Size: 315759 Color: 50

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 402947 Color: 82
Size: 254078 Color: 4
Size: 342976 Color: 63

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 375573 Color: 73
Size: 332296 Color: 59
Size: 292132 Color: 33

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 397744 Color: 78
Size: 346188 Color: 66
Size: 256069 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 398729 Color: 80
Size: 311423 Color: 46
Size: 289849 Color: 32

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 398207 Color: 79
Size: 325408 Color: 56
Size: 276386 Color: 22

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 366964 Color: 71
Size: 315476 Color: 49
Size: 317561 Color: 52

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 383735 Color: 76
Size: 314770 Color: 47
Size: 301496 Color: 40

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 376124 Color: 74
Size: 282424 Color: 28
Size: 341453 Color: 62

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 406989 Color: 83
Size: 287483 Color: 31
Size: 305529 Color: 44

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 379332 Color: 75
Size: 343066 Color: 64
Size: 277603 Color: 24

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 386417 Color: 77
Size: 309172 Color: 45
Size: 304412 Color: 43

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 361900 Color: 70
Size: 358020 Color: 68
Size: 280081 Color: 27

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 358236 Color: 69
Size: 321045 Color: 55
Size: 320720 Color: 54

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 344676 Color: 65
Size: 400599 Color: 81
Size: 254726 Color: 6

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 409906 Color: 84
Size: 339882 Color: 61
Size: 250213 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 415318 Color: 85
Size: 330533 Color: 58
Size: 254150 Color: 5

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 415686 Color: 86
Size: 315001 Color: 48
Size: 269314 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 424558 Color: 87
Size: 303307 Color: 42
Size: 272136 Color: 20

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 426257 Color: 88
Size: 301704 Color: 41
Size: 272040 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 426981 Color: 89
Size: 319381 Color: 53
Size: 253639 Color: 3

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 428993 Color: 90
Size: 300101 Color: 38
Size: 270907 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 432303 Color: 91
Size: 316504 Color: 51
Size: 251194 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 438039 Color: 92
Size: 301121 Color: 39
Size: 260841 Color: 12

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439832 Color: 93
Size: 293194 Color: 34
Size: 266975 Color: 15

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 443970 Color: 94
Size: 283723 Color: 30
Size: 272308 Color: 21

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 444366 Color: 95
Size: 295241 Color: 36
Size: 260394 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 449332 Color: 96
Size: 278858 Color: 25
Size: 271811 Color: 18

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 450761 Color: 97
Size: 293832 Color: 35
Size: 255408 Color: 7

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 454427 Color: 98
Size: 279699 Color: 26
Size: 265875 Color: 14

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 460946 Color: 99
Size: 277599 Color: 23
Size: 261456 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 465260 Color: 100
Size: 282949 Color: 29
Size: 251792 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 484786 Color: 101
Size: 259408 Color: 10
Size: 255807 Color: 8

Total size: 34000034
Total free space: 0

