Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 7
Size: 272 Color: 6
Size: 255 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 294 Color: 10
Size: 292 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 5
Size: 295 Color: 6
Size: 285 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 2
Size: 274 Color: 14
Size: 250 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 10
Size: 256 Color: 19
Size: 255 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 278 Color: 5
Size: 258 Color: 17
Size: 464 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 313 Color: 7
Size: 305 Color: 19

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 0
Size: 324 Color: 3
Size: 282 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 17
Size: 275 Color: 12
Size: 269 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 15
Size: 353 Color: 8
Size: 291 Color: 18

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 318 Color: 0
Size: 260 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 13
Size: 274 Color: 2
Size: 260 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 5
Size: 317 Color: 8
Size: 273 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 8
Size: 314 Color: 17
Size: 280 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 290 Color: 13
Size: 255 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 6
Size: 283 Color: 17
Size: 255 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 294 Color: 7
Size: 255 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 4
Size: 340 Color: 7
Size: 300 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 361 Color: 9
Size: 257 Color: 8

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 296 Color: 12
Size: 255 Color: 17

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 4
Size: 283 Color: 8
Size: 275 Color: 11

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 7
Size: 340 Color: 7
Size: 257 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 6
Size: 325 Color: 0
Size: 269 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 252 Color: 19
Size: 251 Color: 16

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 18
Size: 265 Color: 16
Size: 261 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 11
Size: 306 Color: 18
Size: 261 Color: 17

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 11
Size: 264 Color: 3
Size: 253 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 15
Size: 275 Color: 7
Size: 272 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 6
Size: 293 Color: 4
Size: 290 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 279 Color: 17
Size: 255 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 15
Size: 254 Color: 15
Size: 251 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 6
Size: 359 Color: 9
Size: 255 Color: 14

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 0
Size: 358 Color: 14
Size: 264 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 15
Size: 316 Color: 13
Size: 280 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 4
Size: 278 Color: 1
Size: 278 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 18
Size: 324 Color: 0
Size: 253 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 11
Size: 362 Color: 4
Size: 264 Color: 6

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 19
Size: 329 Color: 6
Size: 270 Color: 16

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 5
Size: 259 Color: 6
Size: 253 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 0
Size: 262 Color: 2
Size: 252 Color: 8

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 18
Size: 294 Color: 17
Size: 294 Color: 12

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 9
Size: 358 Color: 3
Size: 264 Color: 12

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 8
Size: 289 Color: 8
Size: 274 Color: 17

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 271 Color: 3
Size: 268 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 17
Size: 256 Color: 16
Size: 251 Color: 11

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 15
Size: 342 Color: 3
Size: 255 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 3
Size: 305 Color: 4
Size: 281 Color: 13

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 11
Size: 359 Color: 19
Size: 261 Color: 6

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 10
Size: 268 Color: 15
Size: 256 Color: 7

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 2
Size: 309 Color: 4
Size: 281 Color: 17

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 15
Size: 352 Color: 18
Size: 256 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 3
Size: 303 Color: 13
Size: 265 Color: 7

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 8
Size: 307 Color: 5
Size: 280 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 14
Size: 294 Color: 8
Size: 252 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 18
Size: 285 Color: 2
Size: 266 Color: 11

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 5
Size: 342 Color: 3
Size: 253 Color: 9

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 4
Size: 274 Color: 15
Size: 253 Color: 14

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 13
Size: 281 Color: 11
Size: 252 Color: 10

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 14
Size: 362 Color: 11
Size: 274 Color: 19

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 10
Size: 326 Color: 15
Size: 318 Color: 15

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 18
Size: 311 Color: 12
Size: 261 Color: 18

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 280 Color: 13
Size: 265 Color: 4

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 17
Size: 276 Color: 0
Size: 273 Color: 7

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 13
Size: 319 Color: 18
Size: 255 Color: 18

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 14
Size: 316 Color: 7
Size: 260 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 7
Size: 316 Color: 8
Size: 289 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 5
Size: 302 Color: 5
Size: 265 Color: 16

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 17
Size: 305 Color: 17
Size: 299 Color: 19

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 8
Size: 261 Color: 6
Size: 257 Color: 17

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 19
Size: 334 Color: 10
Size: 255 Color: 19

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 338 Color: 17
Size: 332 Color: 8
Size: 330 Color: 17

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 7
Size: 314 Color: 7
Size: 266 Color: 12

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 7
Size: 307 Color: 19
Size: 296 Color: 15

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 275 Color: 3
Size: 253 Color: 13

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 7
Size: 284 Color: 15
Size: 257 Color: 5

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 2
Size: 356 Color: 14
Size: 253 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 11
Size: 326 Color: 18
Size: 321 Color: 10

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 345 Color: 2
Size: 271 Color: 8

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 15
Size: 320 Color: 6
Size: 257 Color: 5

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 17
Size: 331 Color: 0
Size: 270 Color: 4

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 320 Color: 6
Size: 256 Color: 3

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 10
Size: 346 Color: 18
Size: 282 Color: 5

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 8
Size: 280 Color: 13
Size: 270 Color: 4

Total size: 83000
Total free space: 0

