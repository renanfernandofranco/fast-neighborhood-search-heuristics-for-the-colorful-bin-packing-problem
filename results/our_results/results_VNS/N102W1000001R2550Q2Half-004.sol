Capicity Bin: 1000001
Lower Bound: 34

Bins used: 34
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 357838 Color: 1
Size: 297588 Color: 0
Size: 344575 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 362112 Color: 1
Size: 336067 Color: 1
Size: 301822 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 370240 Color: 1
Size: 349874 Color: 1
Size: 279887 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 370901 Color: 1
Size: 321512 Color: 1
Size: 307588 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 381466 Color: 1
Size: 330571 Color: 1
Size: 287964 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 382960 Color: 1
Size: 349816 Color: 1
Size: 267225 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 385850 Color: 1
Size: 307690 Color: 1
Size: 306461 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 390621 Color: 1
Size: 350821 Color: 1
Size: 258559 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 393575 Color: 1
Size: 319591 Color: 1
Size: 286835 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 394031 Color: 1
Size: 337559 Color: 1
Size: 268411 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 406350 Color: 1
Size: 304717 Color: 1
Size: 288934 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 406363 Color: 1
Size: 311117 Color: 1
Size: 282521 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 408899 Color: 1
Size: 305123 Color: 1
Size: 285979 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 410856 Color: 1
Size: 335850 Color: 1
Size: 253295 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 411447 Color: 1
Size: 302558 Color: 1
Size: 285996 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 419135 Color: 1
Size: 300061 Color: 1
Size: 280805 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 420759 Color: 1
Size: 305355 Color: 1
Size: 273887 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 424323 Color: 1
Size: 316764 Color: 1
Size: 258914 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 434682 Color: 1
Size: 306820 Color: 1
Size: 258499 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 441080 Color: 1
Size: 285584 Color: 1
Size: 273337 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 441361 Color: 1
Size: 284278 Color: 1
Size: 274362 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 447321 Color: 1
Size: 277198 Color: 1
Size: 275482 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 454062 Color: 1
Size: 295698 Color: 1
Size: 250241 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 462656 Color: 1
Size: 276469 Color: 1
Size: 260876 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 466486 Color: 1
Size: 277477 Color: 1
Size: 256038 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 467314 Color: 1
Size: 277576 Color: 1
Size: 255111 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 467730 Color: 1
Size: 281306 Color: 1
Size: 250965 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 472301 Color: 1
Size: 275615 Color: 1
Size: 252085 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 473839 Color: 1
Size: 266446 Color: 1
Size: 259716 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 482224 Color: 1
Size: 266733 Color: 1
Size: 251044 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 485716 Color: 1
Size: 260334 Color: 1
Size: 253951 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 487631 Color: 1
Size: 258351 Color: 1
Size: 254019 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 493320 Color: 1
Size: 254135 Color: 1
Size: 252546 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 494099 Color: 1
Size: 253928 Color: 1
Size: 251974 Color: 0

Total size: 34000034
Total free space: 0

