Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 39
Size: 351 Color: 36
Size: 288 Color: 24

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 49
Size: 320 Color: 32
Size: 261 Color: 8

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 43
Size: 366 Color: 42
Size: 268 Color: 11

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 45
Size: 355 Color: 37
Size: 273 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 47
Size: 315 Color: 31
Size: 275 Color: 21

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 57
Size: 275 Color: 20
Size: 252 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 34
Size: 347 Color: 33
Size: 303 Color: 29

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 51
Size: 292 Color: 25
Size: 269 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 41
Size: 363 Color: 40
Size: 271 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 46
Size: 350 Color: 35
Size: 255 Color: 5

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 56
Size: 269 Color: 12
Size: 259 Color: 7

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 272 Color: 16
Size: 262 Color: 9
Size: 466 Color: 55

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 59
Size: 254 Color: 4
Size: 251 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 44
Size: 357 Color: 38
Size: 273 Color: 18

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 50
Size: 307 Color: 30
Size: 263 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 48
Size: 299 Color: 28
Size: 287 Color: 23

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 53
Size: 283 Color: 22
Size: 272 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 54
Size: 298 Color: 27
Size: 252 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 58
Size: 274 Color: 19
Size: 252 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 52
Size: 298 Color: 26
Size: 258 Color: 6

Total size: 20000
Total free space: 0

