Capicity Bin: 1000001
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 384528 Color: 11
Size: 309298 Color: 6
Size: 306175 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 418866 Color: 9
Size: 307914 Color: 2
Size: 273221 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 457440 Color: 1
Size: 275748 Color: 7
Size: 266813 Color: 14

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 400844 Color: 8
Size: 327367 Color: 11
Size: 271790 Color: 5

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 380722 Color: 1
Size: 368960 Color: 0
Size: 250319 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 417348 Color: 11
Size: 305906 Color: 12
Size: 276747 Color: 5

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 466060 Color: 0
Size: 280078 Color: 3
Size: 253863 Color: 14

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 374038 Color: 8
Size: 314246 Color: 4
Size: 311717 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 492072 Color: 5
Size: 257265 Color: 15
Size: 250664 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 392779 Color: 14
Size: 334636 Color: 4
Size: 272586 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 408881 Color: 15
Size: 333147 Color: 6
Size: 257973 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 368055 Color: 10
Size: 365090 Color: 14
Size: 266856 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 365043 Color: 16
Size: 354870 Color: 0
Size: 280088 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 388151 Color: 8
Size: 349529 Color: 11
Size: 262321 Color: 8

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 374420 Color: 14
Size: 356062 Color: 10
Size: 269519 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 413219 Color: 10
Size: 330036 Color: 13
Size: 256746 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 388662 Color: 17
Size: 341066 Color: 0
Size: 270273 Color: 7

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 450156 Color: 0
Size: 277466 Color: 19
Size: 272379 Color: 16

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 407372 Color: 6
Size: 334728 Color: 10
Size: 257901 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 424775 Color: 17
Size: 297523 Color: 13
Size: 277703 Color: 14

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 483321 Color: 19
Size: 266492 Color: 6
Size: 250188 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 389036 Color: 13
Size: 340407 Color: 16
Size: 270558 Color: 13

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 431724 Color: 12
Size: 316126 Color: 4
Size: 252151 Color: 6

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 446260 Color: 9
Size: 289893 Color: 9
Size: 263848 Color: 18

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 444660 Color: 11
Size: 289308 Color: 5
Size: 266033 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 376420 Color: 7
Size: 354803 Color: 11
Size: 268778 Color: 5

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 406753 Color: 4
Size: 329114 Color: 15
Size: 264134 Color: 9

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 365906 Color: 4
Size: 360753 Color: 10
Size: 273342 Color: 17

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 473314 Color: 13
Size: 273800 Color: 0
Size: 252887 Color: 13

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 432120 Color: 4
Size: 285073 Color: 17
Size: 282808 Color: 9

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 439348 Color: 19
Size: 282855 Color: 0
Size: 277798 Color: 4

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 495758 Color: 0
Size: 253982 Color: 12
Size: 250261 Color: 8

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 363406 Color: 8
Size: 354118 Color: 4
Size: 282477 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 391142 Color: 15
Size: 322776 Color: 2
Size: 286083 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 478692 Color: 18
Size: 270196 Color: 17
Size: 251113 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 423927 Color: 1
Size: 310614 Color: 2
Size: 265460 Color: 10

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 394830 Color: 13
Size: 334172 Color: 19
Size: 270999 Color: 15

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 454205 Color: 12
Size: 276082 Color: 2
Size: 269714 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 400046 Color: 4
Size: 305228 Color: 8
Size: 294727 Color: 16

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 355328 Color: 16
Size: 353384 Color: 12
Size: 291289 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 367406 Color: 2
Size: 357602 Color: 7
Size: 274993 Color: 2

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 423165 Color: 15
Size: 305065 Color: 11
Size: 271771 Color: 19

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 438803 Color: 1
Size: 300863 Color: 11
Size: 260335 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 442217 Color: 2
Size: 304818 Color: 19
Size: 252966 Color: 18

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 379356 Color: 14
Size: 345110 Color: 6
Size: 275535 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 395595 Color: 8
Size: 349882 Color: 0
Size: 254524 Color: 2

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 404022 Color: 2
Size: 344434 Color: 3
Size: 251545 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 378314 Color: 14
Size: 343988 Color: 3
Size: 277699 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 485559 Color: 15
Size: 258669 Color: 2
Size: 255773 Color: 17

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 364170 Color: 9
Size: 324487 Color: 12
Size: 311344 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 424417 Color: 14
Size: 308994 Color: 0
Size: 266590 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 399988 Color: 14
Size: 303945 Color: 5
Size: 296068 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 398758 Color: 2
Size: 327130 Color: 15
Size: 274113 Color: 6

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 375853 Color: 0
Size: 317843 Color: 16
Size: 306305 Color: 7

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 423840 Color: 15
Size: 314191 Color: 1
Size: 261970 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 469002 Color: 5
Size: 270144 Color: 14
Size: 260855 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 480685 Color: 1
Size: 268893 Color: 2
Size: 250423 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 351964 Color: 19
Size: 331384 Color: 4
Size: 316653 Color: 16

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 474313 Color: 0
Size: 272790 Color: 4
Size: 252898 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 398751 Color: 14
Size: 335100 Color: 6
Size: 266150 Color: 13

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 438859 Color: 12
Size: 310885 Color: 5
Size: 250257 Color: 10

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 422019 Color: 19
Size: 313926 Color: 9
Size: 264056 Color: 5

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 361111 Color: 7
Size: 352402 Color: 1
Size: 286488 Color: 15

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 462256 Color: 16
Size: 280181 Color: 19
Size: 257564 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 388661 Color: 18
Size: 353276 Color: 0
Size: 258064 Color: 19

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 374764 Color: 12
Size: 368324 Color: 19
Size: 256913 Color: 5

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 401183 Color: 14
Size: 310031 Color: 11
Size: 288787 Color: 19

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 388121 Color: 12
Size: 355284 Color: 11
Size: 256596 Color: 4

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 386159 Color: 12
Size: 342013 Color: 2
Size: 271829 Color: 3

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 474759 Color: 10
Size: 271956 Color: 1
Size: 253286 Color: 6

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 419499 Color: 5
Size: 296728 Color: 13
Size: 283774 Color: 5

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 475085 Color: 3
Size: 273391 Color: 13
Size: 251525 Color: 14

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 437627 Color: 9
Size: 281339 Color: 9
Size: 281035 Color: 4

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 364164 Color: 18
Size: 344421 Color: 4
Size: 291416 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 433108 Color: 14
Size: 303573 Color: 15
Size: 263320 Color: 3

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 369727 Color: 8
Size: 329760 Color: 12
Size: 300514 Color: 19

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 482025 Color: 13
Size: 267416 Color: 4
Size: 250560 Color: 11

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 486147 Color: 1
Size: 262052 Color: 2
Size: 251802 Color: 8

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 394901 Color: 19
Size: 341747 Color: 19
Size: 263353 Color: 5

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 348473 Color: 6
Size: 333430 Color: 3
Size: 318098 Color: 18

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 379405 Color: 6
Size: 369017 Color: 19
Size: 251579 Color: 15

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 424658 Color: 8
Size: 312987 Color: 12
Size: 262356 Color: 12

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 421012 Color: 5
Size: 325858 Color: 12
Size: 253131 Color: 6

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 391264 Color: 18
Size: 305835 Color: 19
Size: 302902 Color: 18

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 398043 Color: 7
Size: 327423 Color: 12
Size: 274535 Color: 8

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 376048 Color: 8
Size: 360463 Color: 9
Size: 263490 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 410021 Color: 0
Size: 300043 Color: 6
Size: 289937 Color: 9

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 380124 Color: 17
Size: 342536 Color: 4
Size: 277341 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 493058 Color: 14
Size: 253667 Color: 4
Size: 253276 Color: 17

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 362361 Color: 3
Size: 323551 Color: 8
Size: 314089 Color: 17

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 377584 Color: 15
Size: 333412 Color: 4
Size: 289005 Color: 3

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 398903 Color: 18
Size: 337861 Color: 9
Size: 263237 Color: 14

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 491755 Color: 17
Size: 255279 Color: 13
Size: 252967 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 384687 Color: 12
Size: 329792 Color: 0
Size: 285522 Color: 18

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 477702 Color: 10
Size: 264318 Color: 12
Size: 257981 Color: 11

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 495604 Color: 7
Size: 254358 Color: 13
Size: 250039 Color: 19

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 339299 Color: 11
Size: 338415 Color: 12
Size: 322287 Color: 6

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 401358 Color: 14
Size: 338343 Color: 1
Size: 260300 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 427529 Color: 11
Size: 310890 Color: 17
Size: 261582 Color: 13

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 355505 Color: 17
Size: 353127 Color: 6
Size: 291369 Color: 2

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 417763 Color: 6
Size: 325796 Color: 18
Size: 256442 Color: 7

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 447980 Color: 14
Size: 292680 Color: 10
Size: 259341 Color: 13

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 380055 Color: 0
Size: 364908 Color: 5
Size: 255038 Color: 8

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 432388 Color: 7
Size: 286311 Color: 17
Size: 281302 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 409664 Color: 13
Size: 339905 Color: 7
Size: 250432 Color: 19

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 378019 Color: 1
Size: 371808 Color: 2
Size: 250174 Color: 4

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 467825 Color: 11
Size: 268873 Color: 9
Size: 263303 Color: 10

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 399237 Color: 13
Size: 325663 Color: 10
Size: 275101 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 423161 Color: 2
Size: 324999 Color: 7
Size: 251841 Color: 4

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 377141 Color: 0
Size: 335748 Color: 5
Size: 287112 Color: 13

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 400953 Color: 15
Size: 325566 Color: 6
Size: 273482 Color: 15

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 425257 Color: 8
Size: 301448 Color: 14
Size: 273296 Color: 16

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 401166 Color: 15
Size: 335898 Color: 2
Size: 262937 Color: 6

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 475346 Color: 8
Size: 272971 Color: 15
Size: 251684 Color: 5

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 357505 Color: 5
Size: 354041 Color: 0
Size: 288455 Color: 11

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 352971 Color: 17
Size: 328408 Color: 17
Size: 318622 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 401208 Color: 16
Size: 317746 Color: 8
Size: 281047 Color: 9

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 496621 Color: 5
Size: 253307 Color: 3
Size: 250073 Color: 11

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 412040 Color: 4
Size: 314729 Color: 4
Size: 273232 Color: 19

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 365097 Color: 8
Size: 329094 Color: 1
Size: 305810 Color: 9

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 461318 Color: 5
Size: 273208 Color: 6
Size: 265475 Color: 4

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 403656 Color: 7
Size: 309009 Color: 9
Size: 287336 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 379765 Color: 2
Size: 340842 Color: 0
Size: 279394 Color: 16

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 390431 Color: 7
Size: 331855 Color: 0
Size: 277715 Color: 4

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 492348 Color: 17
Size: 256562 Color: 3
Size: 251091 Color: 13

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 357918 Color: 18
Size: 347260 Color: 18
Size: 294823 Color: 14

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 470195 Color: 0
Size: 269320 Color: 8
Size: 260486 Color: 4

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 433610 Color: 1
Size: 294054 Color: 11
Size: 272337 Color: 17

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 437310 Color: 11
Size: 310633 Color: 10
Size: 252058 Color: 18

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 399136 Color: 8
Size: 315921 Color: 7
Size: 284944 Color: 13

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 462784 Color: 17
Size: 282297 Color: 7
Size: 254920 Color: 10

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 484236 Color: 15
Size: 260509 Color: 17
Size: 255256 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 424875 Color: 4
Size: 293869 Color: 0
Size: 281257 Color: 4

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 394071 Color: 5
Size: 319279 Color: 9
Size: 286651 Color: 6

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 371780 Color: 13
Size: 356709 Color: 10
Size: 271512 Color: 12

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 415548 Color: 12
Size: 314689 Color: 7
Size: 269764 Color: 10

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 457034 Color: 0
Size: 282518 Color: 10
Size: 260449 Color: 8

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 409252 Color: 11
Size: 319352 Color: 5
Size: 271397 Color: 2

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 419389 Color: 4
Size: 315287 Color: 10
Size: 265325 Color: 18

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 475914 Color: 15
Size: 265033 Color: 8
Size: 259054 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 439403 Color: 19
Size: 301699 Color: 8
Size: 258899 Color: 17

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 492731 Color: 6
Size: 256411 Color: 19
Size: 250859 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 414534 Color: 2
Size: 309636 Color: 12
Size: 275831 Color: 10

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 413910 Color: 7
Size: 329357 Color: 2
Size: 256734 Color: 1

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 462352 Color: 11
Size: 286971 Color: 16
Size: 250678 Color: 9

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 388731 Color: 3
Size: 323591 Color: 1
Size: 287679 Color: 16

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 415000 Color: 14
Size: 294135 Color: 5
Size: 290866 Color: 13

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 381938 Color: 7
Size: 320200 Color: 9
Size: 297863 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 465294 Color: 5
Size: 267853 Color: 10
Size: 266854 Color: 4

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 389412 Color: 16
Size: 314236 Color: 17
Size: 296353 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 394962 Color: 9
Size: 328466 Color: 11
Size: 276573 Color: 9

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 359751 Color: 8
Size: 341291 Color: 14
Size: 298959 Color: 17

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 364952 Color: 7
Size: 353397 Color: 18
Size: 281652 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 477721 Color: 4
Size: 270544 Color: 5
Size: 251736 Color: 9

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 345320 Color: 3
Size: 343910 Color: 17
Size: 310771 Color: 19

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 383943 Color: 11
Size: 363822 Color: 6
Size: 252236 Color: 2

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 452878 Color: 9
Size: 290164 Color: 7
Size: 256959 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 380262 Color: 15
Size: 318516 Color: 13
Size: 301223 Color: 9

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 382468 Color: 0
Size: 330591 Color: 16
Size: 286942 Color: 18

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 353086 Color: 7
Size: 351946 Color: 8
Size: 294969 Color: 17

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 445102 Color: 10
Size: 282625 Color: 2
Size: 272274 Color: 11

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 482460 Color: 0
Size: 260181 Color: 3
Size: 257360 Color: 12

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 499664 Color: 2
Size: 250275 Color: 8
Size: 250062 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 409579 Color: 14
Size: 334542 Color: 2
Size: 255880 Color: 4

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 370586 Color: 13
Size: 332233 Color: 6
Size: 297182 Color: 11

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 412975 Color: 14
Size: 306123 Color: 13
Size: 280903 Color: 19

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 468644 Color: 2
Size: 274304 Color: 19
Size: 257053 Color: 15

Total size: 167000167
Total free space: 0

