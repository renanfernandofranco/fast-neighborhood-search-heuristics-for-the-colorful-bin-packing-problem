Capicity Bin: 16432
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 19
Items: 
Size: 1144 Color: 185
Size: 1056 Color: 181
Size: 1054 Color: 180
Size: 1040 Color: 178
Size: 1036 Color: 177
Size: 1032 Color: 176
Size: 1024 Color: 174
Size: 992 Color: 172
Size: 992 Color: 171
Size: 984 Color: 169
Size: 976 Color: 167
Size: 960 Color: 166
Size: 932 Color: 164
Size: 604 Color: 121
Size: 544 Color: 106
Size: 528 Color: 105
Size: 518 Color: 104
Size: 512 Color: 102
Size: 504 Color: 101

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10725 Color: 440
Size: 5357 Color: 381
Size: 350 Color: 52

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11953 Color: 463
Size: 3283 Color: 326
Size: 1196 Color: 189

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11969 Color: 464
Size: 2813 Color: 308
Size: 1650 Color: 228

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12104 Color: 467
Size: 2844 Color: 309
Size: 1484 Color: 209

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 482
Size: 3222 Color: 323
Size: 644 Color: 126

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12744 Color: 484
Size: 3320 Color: 330
Size: 368 Color: 58

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12930 Color: 489
Size: 1890 Color: 248
Size: 1612 Color: 224

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12938 Color: 490
Size: 3218 Color: 322
Size: 276 Color: 20

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12962 Color: 492
Size: 2418 Color: 284
Size: 1052 Color: 179

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12968 Color: 493
Size: 2906 Color: 313
Size: 558 Color: 110

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 494
Size: 3244 Color: 324
Size: 208 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 495
Size: 1896 Color: 249
Size: 1524 Color: 215

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 496
Size: 3014 Color: 317
Size: 390 Color: 66

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13057 Color: 497
Size: 1761 Color: 240
Size: 1614 Color: 225

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13224 Color: 503
Size: 2852 Color: 310
Size: 356 Color: 55

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13316 Color: 504
Size: 2884 Color: 311
Size: 232 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13368 Color: 507
Size: 2734 Color: 301
Size: 330 Color: 45

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13420 Color: 510
Size: 2556 Color: 294
Size: 456 Color: 87

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 511
Size: 1642 Color: 227
Size: 1368 Color: 201

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13438 Color: 512
Size: 2510 Color: 289
Size: 484 Color: 94

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13440 Color: 513
Size: 2084 Color: 265
Size: 908 Color: 161

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13464 Color: 514
Size: 2524 Color: 292
Size: 444 Color: 82

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13470 Color: 515
Size: 2532 Color: 293
Size: 430 Color: 78

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13699 Color: 524
Size: 2317 Color: 280
Size: 416 Color: 74

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13700 Color: 525
Size: 2324 Color: 281
Size: 408 Color: 71

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13711 Color: 526
Size: 2005 Color: 258
Size: 716 Color: 137

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13784 Color: 528
Size: 2216 Color: 274
Size: 432 Color: 80

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13797 Color: 529
Size: 2075 Color: 264
Size: 560 Color: 111

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13940 Color: 538
Size: 1948 Color: 252
Size: 544 Color: 107

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14025 Color: 542
Size: 2037 Color: 260
Size: 370 Color: 60

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14050 Color: 543
Size: 1986 Color: 256
Size: 396 Color: 69

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 544
Size: 1688 Color: 234
Size: 688 Color: 134

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14058 Color: 545
Size: 2098 Color: 267
Size: 276 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14073 Color: 546
Size: 2043 Color: 262
Size: 316 Color: 36

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14076 Color: 547
Size: 1652 Color: 229
Size: 704 Color: 135

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 551
Size: 1528 Color: 216
Size: 738 Color: 141

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 552
Size: 1748 Color: 239
Size: 516 Color: 103

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14263 Color: 556
Size: 1741 Color: 238
Size: 428 Color: 77

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 560
Size: 1560 Color: 219
Size: 562 Color: 112

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14398 Color: 565
Size: 1200 Color: 191
Size: 834 Color: 153

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14407 Color: 566
Size: 1689 Color: 235
Size: 336 Color: 48

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14408 Color: 567
Size: 1368 Color: 200
Size: 656 Color: 130

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 571
Size: 1684 Color: 233
Size: 296 Color: 25

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14454 Color: 572
Size: 1498 Color: 211
Size: 480 Color: 93

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14456 Color: 573
Size: 1000 Color: 173
Size: 976 Color: 168

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14485 Color: 575
Size: 1623 Color: 226
Size: 324 Color: 41

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14498 Color: 576
Size: 984 Color: 170
Size: 950 Color: 165

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14557 Color: 580
Size: 1607 Color: 223
Size: 268 Color: 17

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14568 Color: 581
Size: 1360 Color: 197
Size: 504 Color: 100

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14574 Color: 582
Size: 1442 Color: 206
Size: 416 Color: 75

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14600 Color: 584
Size: 1264 Color: 193
Size: 568 Color: 114

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14605 Color: 585
Size: 1721 Color: 237
Size: 106 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14621 Color: 587
Size: 1511 Color: 214
Size: 300 Color: 29

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 590
Size: 1136 Color: 184
Size: 658 Color: 131

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14643 Color: 591
Size: 1491 Color: 210
Size: 298 Color: 27

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14654 Color: 592
Size: 1198 Color: 190
Size: 580 Color: 118

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 595
Size: 1448 Color: 207
Size: 308 Color: 33

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 14696 Color: 596
Size: 1344 Color: 195
Size: 392 Color: 68

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 14702 Color: 597
Size: 1394 Color: 205
Size: 336 Color: 47

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 14786 Color: 600
Size: 1070 Color: 183
Size: 576 Color: 115

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13404 Color: 509
Size: 3027 Color: 319

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13517 Color: 518
Size: 2914 Color: 314

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 13943 Color: 539
Size: 2488 Color: 287

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 564
Size: 2041 Color: 261

Bin 66: 1 of cap free
Amount of items: 2
Items: 
Size: 14449 Color: 570
Size: 1982 Color: 255

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 14663 Color: 594
Size: 1024 Color: 175
Size: 744 Color: 143

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 11900 Color: 461
Size: 4530 Color: 359

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 12946 Color: 491
Size: 3320 Color: 331
Size: 164 Color: 8

Bin 70: 2 of cap free
Amount of items: 2
Items: 
Size: 13826 Color: 531
Size: 2604 Color: 299

Bin 71: 2 of cap free
Amount of items: 2
Items: 
Size: 13918 Color: 536
Size: 2512 Color: 290

Bin 72: 2 of cap free
Amount of items: 2
Items: 
Size: 14466 Color: 574
Size: 1964 Color: 254

Bin 73: 2 of cap free
Amount of items: 2
Items: 
Size: 14660 Color: 593
Size: 1770 Color: 241

Bin 74: 2 of cap free
Amount of items: 2
Items: 
Size: 14762 Color: 598
Size: 1668 Color: 232

Bin 75: 2 of cap free
Amount of items: 2
Items: 
Size: 14774 Color: 599
Size: 1656 Color: 231

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 9141 Color: 414
Size: 6836 Color: 396
Size: 452 Color: 86

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 11236 Color: 449
Size: 4873 Color: 366
Size: 320 Color: 39

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 12419 Color: 473
Size: 4010 Color: 348

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 12574 Color: 483
Size: 3813 Color: 345
Size: 42 Color: 1

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 12916 Color: 488
Size: 2801 Color: 307
Size: 712 Color: 136

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 13861 Color: 534
Size: 2568 Color: 295

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 13928 Color: 537
Size: 2501 Color: 288

Bin 83: 3 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 541
Size: 2441 Color: 286

Bin 84: 3 of cap free
Amount of items: 2
Items: 
Size: 14385 Color: 563
Size: 2044 Color: 263

Bin 85: 4 of cap free
Amount of items: 3
Items: 
Size: 9612 Color: 424
Size: 6408 Color: 393
Size: 408 Color: 72

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 10821 Color: 441
Size: 5263 Color: 378
Size: 344 Color: 51

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 12009 Color: 466
Size: 4131 Color: 351
Size: 288 Color: 21

Bin 88: 4 of cap free
Amount of items: 2
Items: 
Size: 13836 Color: 532
Size: 2592 Color: 297

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 13845 Color: 533
Size: 2583 Color: 296

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14340 Color: 562
Size: 2088 Color: 266

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 14412 Color: 568
Size: 2016 Color: 259

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 14436 Color: 569
Size: 1992 Color: 257

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 586
Size: 1816 Color: 244

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 14630 Color: 589
Size: 1798 Color: 242

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 14267 Color: 558
Size: 2160 Color: 271

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 14622 Color: 588
Size: 1805 Color: 243

Bin 97: 6 of cap free
Amount of items: 3
Items: 
Size: 11576 Color: 456
Size: 4550 Color: 361
Size: 300 Color: 30

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 12856 Color: 487
Size: 3528 Color: 335
Size: 42 Color: 2

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 13138 Color: 501
Size: 3288 Color: 328

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 13910 Color: 535
Size: 2516 Color: 291

Bin 101: 7 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 508
Size: 3025 Color: 318
Size: 28 Color: 0

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 13503 Color: 517
Size: 2922 Color: 315

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 13644 Color: 521
Size: 2781 Color: 306

Bin 104: 7 of cap free
Amount of items: 2
Items: 
Size: 14582 Color: 583
Size: 1843 Color: 245

Bin 105: 8 of cap free
Amount of items: 2
Items: 
Size: 14140 Color: 549
Size: 2284 Color: 278

Bin 106: 8 of cap free
Amount of items: 2
Items: 
Size: 14548 Color: 579
Size: 1876 Color: 246

Bin 107: 9 of cap free
Amount of items: 2
Items: 
Size: 13656 Color: 522
Size: 2767 Color: 304

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 13669 Color: 523
Size: 2754 Color: 303

Bin 109: 9 of cap free
Amount of items: 2
Items: 
Size: 14321 Color: 561
Size: 2102 Color: 268

Bin 110: 10 of cap free
Amount of items: 2
Items: 
Size: 13534 Color: 519
Size: 2888 Color: 312

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 14153 Color: 550
Size: 2269 Color: 277

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 14500 Color: 577
Size: 1922 Color: 251

Bin 113: 11 of cap free
Amount of items: 2
Items: 
Size: 13333 Color: 506
Size: 3088 Color: 321

Bin 114: 11 of cap free
Amount of items: 2
Items: 
Size: 14264 Color: 557
Size: 2157 Color: 270

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 14278 Color: 559
Size: 2143 Color: 269

Bin 116: 12 of cap free
Amount of items: 4
Items: 
Size: 10516 Color: 436
Size: 5168 Color: 375
Size: 368 Color: 59
Size: 368 Color: 57

Bin 117: 12 of cap free
Amount of items: 2
Items: 
Size: 13642 Color: 520
Size: 2778 Color: 305

Bin 118: 12 of cap free
Amount of items: 2
Items: 
Size: 14170 Color: 553
Size: 2250 Color: 276

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 14188 Color: 554
Size: 2232 Color: 275

Bin 120: 13 of cap free
Amount of items: 2
Items: 
Size: 13487 Color: 516
Size: 2932 Color: 316

Bin 121: 13 of cap free
Amount of items: 2
Items: 
Size: 14518 Color: 578
Size: 1901 Color: 250

Bin 122: 14 of cap free
Amount of items: 2
Items: 
Size: 13073 Color: 499
Size: 3345 Color: 333

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 14221 Color: 555
Size: 2197 Color: 273

Bin 124: 15 of cap free
Amount of items: 3
Items: 
Size: 11940 Color: 462
Size: 2303 Color: 279
Size: 2174 Color: 272

Bin 125: 15 of cap free
Amount of items: 2
Items: 
Size: 14089 Color: 548
Size: 2328 Color: 283

Bin 126: 16 of cap free
Amount of items: 2
Items: 
Size: 10408 Color: 433
Size: 6008 Color: 389

Bin 127: 16 of cap free
Amount of items: 3
Items: 
Size: 12138 Color: 470
Size: 4022 Color: 349
Size: 256 Color: 16

Bin 128: 16 of cap free
Amount of items: 3
Items: 
Size: 12216 Color: 472
Size: 3960 Color: 346
Size: 240 Color: 15

Bin 129: 18 of cap free
Amount of items: 2
Items: 
Size: 13734 Color: 527
Size: 2680 Color: 300

Bin 130: 18 of cap free
Amount of items: 2
Items: 
Size: 13983 Color: 540
Size: 2431 Color: 285

Bin 131: 19 of cap free
Amount of items: 2
Items: 
Size: 13816 Color: 530
Size: 2597 Color: 298

Bin 132: 21 of cap free
Amount of items: 2
Items: 
Size: 12803 Color: 486
Size: 3608 Color: 339

Bin 133: 22 of cap free
Amount of items: 2
Items: 
Size: 13113 Color: 500
Size: 3297 Color: 329

Bin 134: 24 of cap free
Amount of items: 2
Items: 
Size: 12801 Color: 485
Size: 3607 Color: 338

Bin 135: 26 of cap free
Amount of items: 2
Items: 
Size: 13154 Color: 502
Size: 3252 Color: 325

Bin 136: 27 of cap free
Amount of items: 3
Items: 
Size: 11857 Color: 460
Size: 4260 Color: 355
Size: 288 Color: 23

Bin 137: 28 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 498
Size: 3332 Color: 332

Bin 138: 31 of cap free
Amount of items: 3
Items: 
Size: 11336 Color: 453
Size: 4757 Color: 364
Size: 308 Color: 34

Bin 139: 34 of cap free
Amount of items: 3
Items: 
Size: 12493 Color: 477
Size: 3721 Color: 340
Size: 184 Color: 9

Bin 140: 35 of cap free
Amount of items: 2
Items: 
Size: 13317 Color: 505
Size: 3080 Color: 320

Bin 141: 37 of cap free
Amount of items: 9
Items: 
Size: 8217 Color: 405
Size: 1360 Color: 196
Size: 1214 Color: 192
Size: 1184 Color: 188
Size: 1184 Color: 187
Size: 1184 Color: 186
Size: 1056 Color: 182
Size: 500 Color: 99
Size: 496 Color: 98

Bin 142: 40 of cap free
Amount of items: 3
Items: 
Size: 12532 Color: 480
Size: 3748 Color: 343
Size: 112 Color: 5

Bin 143: 40 of cap free
Amount of items: 3
Items: 
Size: 12548 Color: 481
Size: 3780 Color: 344
Size: 64 Color: 3

Bin 144: 43 of cap free
Amount of items: 2
Items: 
Size: 12105 Color: 468
Size: 4284 Color: 356

Bin 145: 44 of cap free
Amount of items: 3
Items: 
Size: 10005 Color: 425
Size: 5983 Color: 387
Size: 400 Color: 70

Bin 146: 44 of cap free
Amount of items: 3
Items: 
Size: 10264 Color: 432
Size: 5740 Color: 383
Size: 384 Color: 65

Bin 147: 44 of cap free
Amount of items: 3
Items: 
Size: 11421 Color: 454
Size: 4663 Color: 363
Size: 304 Color: 32

Bin 148: 51 of cap free
Amount of items: 3
Items: 
Size: 12130 Color: 469
Size: 3979 Color: 347
Size: 272 Color: 18

Bin 149: 52 of cap free
Amount of items: 3
Items: 
Size: 9336 Color: 422
Size: 6612 Color: 394
Size: 432 Color: 79

Bin 150: 52 of cap free
Amount of items: 4
Items: 
Size: 10484 Color: 435
Size: 5144 Color: 374
Size: 376 Color: 62
Size: 376 Color: 61

Bin 151: 55 of cap free
Amount of items: 2
Items: 
Size: 12200 Color: 471
Size: 4177 Color: 352

Bin 152: 59 of cap free
Amount of items: 7
Items: 
Size: 8220 Color: 408
Size: 1653 Color: 230
Size: 1598 Color: 222
Size: 1572 Color: 221
Size: 1570 Color: 220
Size: 1280 Color: 194
Size: 480 Color: 92

Bin 153: 60 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 448
Size: 4824 Color: 365
Size: 324 Color: 40

Bin 154: 65 of cap free
Amount of items: 3
Items: 
Size: 10837 Color: 442
Size: 5186 Color: 377
Size: 344 Color: 50

Bin 155: 72 of cap free
Amount of items: 2
Items: 
Size: 8232 Color: 410
Size: 8128 Color: 404

Bin 156: 82 of cap free
Amount of items: 3
Items: 
Size: 12500 Color: 478
Size: 3724 Color: 341
Size: 126 Color: 7

Bin 157: 83 of cap free
Amount of items: 3
Items: 
Size: 12504 Color: 479
Size: 3733 Color: 342
Size: 112 Color: 6

Bin 158: 92 of cap free
Amount of items: 3
Items: 
Size: 11000 Color: 447
Size: 5012 Color: 372
Size: 328 Color: 42

Bin 159: 93 of cap free
Amount of items: 3
Items: 
Size: 11477 Color: 455
Size: 4558 Color: 362
Size: 304 Color: 31

Bin 160: 108 of cap free
Amount of items: 4
Items: 
Size: 9548 Color: 423
Size: 5942 Color: 386
Size: 420 Color: 76
Size: 414 Color: 73

Bin 161: 108 of cap free
Amount of items: 2
Items: 
Size: 11292 Color: 450
Size: 5032 Color: 373

Bin 162: 112 of cap free
Amount of items: 3
Items: 
Size: 11688 Color: 459
Size: 4344 Color: 358
Size: 288 Color: 24

Bin 163: 116 of cap free
Amount of items: 3
Items: 
Size: 10240 Color: 431
Size: 5684 Color: 382
Size: 392 Color: 67

Bin 164: 116 of cap free
Amount of items: 3
Items: 
Size: 11972 Color: 465
Size: 4056 Color: 350
Size: 288 Color: 22

Bin 165: 137 of cap free
Amount of items: 2
Items: 
Size: 10218 Color: 430
Size: 6077 Color: 392

Bin 166: 142 of cap free
Amount of items: 3
Items: 
Size: 10998 Color: 446
Size: 4964 Color: 371
Size: 328 Color: 43

Bin 167: 144 of cap free
Amount of items: 3
Items: 
Size: 10648 Color: 439
Size: 5288 Color: 380
Size: 352 Color: 53

Bin 168: 153 of cap free
Amount of items: 7
Items: 
Size: 8219 Color: 407
Size: 1550 Color: 218
Size: 1542 Color: 217
Size: 1510 Color: 213
Size: 1502 Color: 212
Size: 1468 Color: 208
Size: 488 Color: 95

Bin 169: 158 of cap free
Amount of items: 2
Items: 
Size: 10210 Color: 429
Size: 6064 Color: 391

Bin 170: 170 of cap free
Amount of items: 3
Items: 
Size: 12456 Color: 475
Size: 3582 Color: 336
Size: 224 Color: 12

Bin 171: 177 of cap free
Amount of items: 3
Items: 
Size: 12477 Color: 476
Size: 3586 Color: 337
Size: 192 Color: 10

Bin 172: 182 of cap free
Amount of items: 3
Items: 
Size: 11622 Color: 458
Size: 4332 Color: 357
Size: 296 Color: 26

Bin 173: 190 of cap free
Amount of items: 3
Items: 
Size: 10982 Color: 445
Size: 4932 Color: 370
Size: 328 Color: 44

Bin 174: 198 of cap free
Amount of items: 3
Items: 
Size: 10974 Color: 444
Size: 4924 Color: 369
Size: 336 Color: 46

Bin 175: 206 of cap free
Amount of items: 3
Items: 
Size: 10966 Color: 443
Size: 4920 Color: 368
Size: 340 Color: 49

Bin 176: 220 of cap free
Amount of items: 4
Items: 
Size: 11324 Color: 451
Size: 4248 Color: 353
Size: 320 Color: 38
Size: 320 Color: 37

Bin 177: 228 of cap free
Amount of items: 3
Items: 
Size: 12436 Color: 474
Size: 3528 Color: 334
Size: 240 Color: 14

Bin 178: 238 of cap free
Amount of items: 22
Items: 
Size: 908 Color: 163
Size: 908 Color: 162
Size: 904 Color: 160
Size: 864 Color: 159
Size: 864 Color: 158
Size: 856 Color: 157
Size: 850 Color: 156
Size: 848 Color: 155
Size: 848 Color: 154
Size: 824 Color: 152
Size: 816 Color: 151
Size: 800 Color: 150
Size: 640 Color: 124
Size: 640 Color: 123
Size: 608 Color: 122
Size: 604 Color: 120
Size: 584 Color: 119
Size: 580 Color: 117
Size: 580 Color: 116
Size: 568 Color: 113
Size: 552 Color: 109
Size: 548 Color: 108

Bin 179: 253 of cap free
Amount of items: 3
Items: 
Size: 11325 Color: 452
Size: 4542 Color: 360
Size: 312 Color: 35

Bin 180: 265 of cap free
Amount of items: 3
Items: 
Size: 11610 Color: 457
Size: 4257 Color: 354
Size: 300 Color: 28

Bin 181: 267 of cap free
Amount of items: 3
Items: 
Size: 10536 Color: 438
Size: 5277 Color: 379
Size: 352 Color: 54

Bin 182: 275 of cap free
Amount of items: 2
Items: 
Size: 9310 Color: 419
Size: 6847 Color: 402

Bin 183: 275 of cap free
Amount of items: 2
Items: 
Size: 10117 Color: 428
Size: 6040 Color: 390

Bin 184: 280 of cap free
Amount of items: 2
Items: 
Size: 9306 Color: 418
Size: 6846 Color: 401

Bin 185: 284 of cap free
Amount of items: 4
Items: 
Size: 9324 Color: 421
Size: 5938 Color: 385
Size: 448 Color: 83
Size: 438 Color: 81

Bin 186: 288 of cap free
Amount of items: 4
Items: 
Size: 9316 Color: 420
Size: 5932 Color: 384
Size: 448 Color: 85
Size: 448 Color: 84

Bin 187: 301 of cap free
Amount of items: 3
Items: 
Size: 10101 Color: 426
Size: 3284 Color: 327
Size: 2746 Color: 302

Bin 188: 331 of cap free
Amount of items: 2
Items: 
Size: 10104 Color: 427
Size: 5997 Color: 388

Bin 189: 334 of cap free
Amount of items: 2
Items: 
Size: 9253 Color: 417
Size: 6845 Color: 400

Bin 190: 343 of cap free
Amount of items: 5
Items: 
Size: 8222 Color: 409
Size: 2326 Color: 282
Size: 1953 Color: 253
Size: 1886 Color: 247
Size: 1702 Color: 236

Bin 191: 351 of cap free
Amount of items: 2
Items: 
Size: 9237 Color: 416
Size: 6844 Color: 399

Bin 192: 358 of cap free
Amount of items: 3
Items: 
Size: 10532 Color: 437
Size: 5182 Color: 376
Size: 360 Color: 56

Bin 193: 362 of cap free
Amount of items: 8
Items: 
Size: 8218 Color: 406
Size: 1382 Color: 204
Size: 1374 Color: 203
Size: 1368 Color: 202
Size: 1368 Color: 199
Size: 1368 Color: 198
Size: 496 Color: 97
Size: 496 Color: 96

Bin 194: 362 of cap free
Amount of items: 4
Items: 
Size: 10420 Color: 434
Size: 4888 Color: 367
Size: 384 Color: 64
Size: 378 Color: 63

Bin 195: 366 of cap free
Amount of items: 2
Items: 
Size: 9224 Color: 415
Size: 6842 Color: 398

Bin 196: 388 of cap free
Amount of items: 3
Items: 
Size: 8744 Color: 413
Size: 6840 Color: 397
Size: 460 Color: 88

Bin 197: 408 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 412
Size: 7280 Color: 403
Size: 464 Color: 89

Bin 198: 444 of cap free
Amount of items: 4
Items: 
Size: 8236 Color: 411
Size: 6808 Color: 395
Size: 480 Color: 91
Size: 464 Color: 90

Bin 199: 4946 of cap free
Amount of items: 16
Items: 
Size: 800 Color: 149
Size: 800 Color: 148
Size: 784 Color: 147
Size: 762 Color: 146
Size: 752 Color: 145
Size: 746 Color: 144
Size: 742 Color: 142
Size: 736 Color: 140
Size: 720 Color: 139
Size: 720 Color: 138
Size: 668 Color: 133
Size: 664 Color: 132
Size: 656 Color: 129
Size: 648 Color: 128
Size: 648 Color: 127
Size: 640 Color: 125

Total size: 3253536
Total free space: 16432

