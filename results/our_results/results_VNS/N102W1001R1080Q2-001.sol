Capicity Bin: 1001
Lower Bound: 46

Bins used: 48
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 610 Color: 1
Size: 257 Color: 0
Size: 134 Color: 0

Bin 2: 0 of cap free
Amount of items: 2
Items: 
Size: 512 Color: 1
Size: 489 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 693 Color: 0
Size: 202 Color: 0
Size: 106 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 578 Color: 1
Size: 274 Color: 1
Size: 149 Color: 0

Bin 5: 0 of cap free
Amount of items: 2
Items: 
Size: 658 Color: 0
Size: 343 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 321 Color: 1
Size: 225 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 429 Color: 0
Size: 127 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 322 Color: 1
Size: 212 Color: 0

Bin 9: 1 of cap free
Amount of items: 2
Items: 
Size: 641 Color: 0
Size: 359 Color: 1

Bin 10: 1 of cap free
Amount of items: 2
Items: 
Size: 650 Color: 1
Size: 350 Color: 0

Bin 11: 1 of cap free
Amount of items: 2
Items: 
Size: 515 Color: 0
Size: 485 Color: 1

Bin 12: 1 of cap free
Amount of items: 2
Items: 
Size: 582 Color: 0
Size: 418 Color: 1

Bin 13: 1 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 0
Size: 380 Color: 1
Size: 156 Color: 1

Bin 14: 1 of cap free
Amount of items: 2
Items: 
Size: 538 Color: 1
Size: 462 Color: 0

Bin 15: 1 of cap free
Amount of items: 2
Items: 
Size: 607 Color: 0
Size: 393 Color: 1

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 585 Color: 1
Size: 265 Color: 0
Size: 150 Color: 1

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 630 Color: 1
Size: 218 Color: 1
Size: 152 Color: 0

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 0
Size: 370 Color: 1
Size: 170 Color: 1

Bin 19: 2 of cap free
Amount of items: 3
Items: 
Size: 643 Color: 0
Size: 186 Color: 1
Size: 170 Color: 1

Bin 20: 2 of cap free
Amount of items: 3
Items: 
Size: 561 Color: 1
Size: 275 Color: 0
Size: 163 Color: 1

Bin 21: 3 of cap free
Amount of items: 2
Items: 
Size: 648 Color: 0
Size: 350 Color: 1

Bin 22: 3 of cap free
Amount of items: 2
Items: 
Size: 614 Color: 1
Size: 384 Color: 0

Bin 23: 3 of cap free
Amount of items: 2
Items: 
Size: 781 Color: 0
Size: 217 Color: 1

Bin 24: 4 of cap free
Amount of items: 3
Items: 
Size: 707 Color: 0
Size: 162 Color: 1
Size: 128 Color: 1

Bin 25: 5 of cap free
Amount of items: 2
Items: 
Size: 665 Color: 0
Size: 331 Color: 1

Bin 26: 5 of cap free
Amount of items: 2
Items: 
Size: 743 Color: 1
Size: 253 Color: 0

Bin 27: 6 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 389 Color: 1
Size: 179 Color: 0

Bin 28: 6 of cap free
Amount of items: 2
Items: 
Size: 700 Color: 0
Size: 295 Color: 1

Bin 29: 7 of cap free
Amount of items: 2
Items: 
Size: 575 Color: 1
Size: 419 Color: 0

Bin 30: 13 of cap free
Amount of items: 2
Items: 
Size: 792 Color: 1
Size: 196 Color: 0

Bin 31: 20 of cap free
Amount of items: 2
Items: 
Size: 668 Color: 1
Size: 313 Color: 0

Bin 32: 23 of cap free
Amount of items: 3
Items: 
Size: 568 Color: 1
Size: 215 Color: 1
Size: 195 Color: 0

Bin 33: 33 of cap free
Amount of items: 2
Items: 
Size: 581 Color: 0
Size: 387 Color: 1

Bin 34: 44 of cap free
Amount of items: 2
Items: 
Size: 483 Color: 0
Size: 474 Color: 1

Bin 35: 49 of cap free
Amount of items: 2
Items: 
Size: 654 Color: 1
Size: 298 Color: 0

Bin 36: 53 of cap free
Amount of items: 2
Items: 
Size: 652 Color: 1
Size: 296 Color: 0

Bin 37: 56 of cap free
Amount of items: 2
Items: 
Size: 479 Color: 0
Size: 466 Color: 1

Bin 38: 70 of cap free
Amount of items: 2
Items: 
Size: 645 Color: 1
Size: 286 Color: 0

Bin 39: 92 of cap free
Amount of items: 2
Items: 
Size: 637 Color: 1
Size: 272 Color: 0

Bin 40: 207 of cap free
Amount of items: 1
Items: 
Size: 794 Color: 0

Bin 41: 241 of cap free
Amount of items: 1
Items: 
Size: 760 Color: 0

Bin 42: 245 of cap free
Amount of items: 1
Items: 
Size: 756 Color: 1

Bin 43: 246 of cap free
Amount of items: 1
Items: 
Size: 755 Color: 1

Bin 44: 249 of cap free
Amount of items: 1
Items: 
Size: 752 Color: 1

Bin 45: 260 of cap free
Amount of items: 1
Items: 
Size: 741 Color: 1

Bin 46: 263 of cap free
Amount of items: 1
Items: 
Size: 738 Color: 1

Bin 47: 288 of cap free
Amount of items: 1
Items: 
Size: 713 Color: 0

Bin 48: 391 of cap free
Amount of items: 1
Items: 
Size: 610 Color: 1

Total size: 45149
Total free space: 2899

