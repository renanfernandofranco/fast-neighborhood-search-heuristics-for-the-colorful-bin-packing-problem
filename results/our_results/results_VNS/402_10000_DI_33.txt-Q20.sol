Capicity Bin: 7824
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4344 Color: 19
Size: 3258 Color: 11
Size: 222 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4498 Color: 14
Size: 2882 Color: 7
Size: 444 Color: 19

Bin 3: 0 of cap free
Amount of items: 2
Items: 
Size: 4928 Color: 2
Size: 2896 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5017 Color: 2
Size: 2341 Color: 11
Size: 466 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5512 Color: 1
Size: 2168 Color: 4
Size: 144 Color: 15

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5940 Color: 13
Size: 1572 Color: 7
Size: 312 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 7
Size: 1196 Color: 14
Size: 640 Color: 6

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6070 Color: 19
Size: 1430 Color: 18
Size: 324 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6072 Color: 15
Size: 1608 Color: 2
Size: 144 Color: 6

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6141 Color: 14
Size: 1453 Color: 6
Size: 230 Color: 13

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6152 Color: 5
Size: 1528 Color: 11
Size: 144 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6182 Color: 9
Size: 1494 Color: 2
Size: 148 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6260 Color: 11
Size: 1308 Color: 12
Size: 256 Color: 12

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6274 Color: 4
Size: 1086 Color: 18
Size: 464 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6312 Color: 10
Size: 1216 Color: 2
Size: 296 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 13
Size: 934 Color: 11
Size: 480 Color: 9

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 2
Size: 1180 Color: 3
Size: 216 Color: 6

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6440 Color: 14
Size: 1160 Color: 10
Size: 224 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6520 Color: 8
Size: 1096 Color: 10
Size: 208 Color: 11

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6522 Color: 16
Size: 970 Color: 10
Size: 332 Color: 18

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6524 Color: 6
Size: 1164 Color: 14
Size: 136 Color: 17

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6588 Color: 2
Size: 728 Color: 8
Size: 508 Color: 17

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6600 Color: 4
Size: 648 Color: 19
Size: 576 Color: 16

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6618 Color: 17
Size: 1006 Color: 18
Size: 200 Color: 12

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 6
Size: 712 Color: 11
Size: 432 Color: 19

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6692 Color: 15
Size: 892 Color: 15
Size: 240 Color: 7

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6736 Color: 2
Size: 920 Color: 19
Size: 168 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6713 Color: 0
Size: 927 Color: 5
Size: 184 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6720 Color: 17
Size: 968 Color: 1
Size: 136 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 16
Size: 758 Color: 8
Size: 280 Color: 6

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 19
Size: 764 Color: 12
Size: 248 Color: 7

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6822 Color: 11
Size: 730 Color: 10
Size: 272 Color: 2

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 4
Size: 742 Color: 11
Size: 184 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6901 Color: 18
Size: 771 Color: 3
Size: 152 Color: 2

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6948 Color: 0
Size: 732 Color: 18
Size: 144 Color: 5

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6950 Color: 11
Size: 698 Color: 16
Size: 176 Color: 6

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 11
Size: 544 Color: 13
Size: 328 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6956 Color: 9
Size: 804 Color: 1
Size: 64 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6984 Color: 3
Size: 552 Color: 11
Size: 288 Color: 17

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 5
Size: 700 Color: 2
Size: 136 Color: 11

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 4442 Color: 10
Size: 3253 Color: 17
Size: 128 Color: 18

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 4567 Color: 1
Size: 3256 Color: 14

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4775 Color: 5
Size: 2756 Color: 10
Size: 292 Color: 19

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5453 Color: 17
Size: 2186 Color: 13
Size: 184 Color: 16

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5561 Color: 2
Size: 2182 Color: 12
Size: 80 Color: 8

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 6
Size: 1589 Color: 10
Size: 640 Color: 15

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 7
Size: 1751 Color: 16
Size: 290 Color: 6

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 8
Size: 1201 Color: 0
Size: 160 Color: 13

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6477 Color: 12
Size: 1060 Color: 2
Size: 286 Color: 13

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 6662 Color: 3
Size: 1161 Color: 14

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 4
Size: 2502 Color: 18
Size: 316 Color: 14

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 5652 Color: 4
Size: 1086 Color: 1
Size: 1084 Color: 14

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 5846 Color: 8
Size: 1800 Color: 4
Size: 176 Color: 1

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 5900 Color: 9
Size: 1922 Color: 2

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5992 Color: 6
Size: 1678 Color: 4
Size: 152 Color: 7

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6385 Color: 16
Size: 1437 Color: 6

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 4
Size: 1032 Color: 12
Size: 40 Color: 0

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 6
Size: 957 Color: 16

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 6978 Color: 14
Size: 844 Color: 3

Bin 60: 2 of cap free
Amount of items: 4
Items: 
Size: 7036 Color: 5
Size: 774 Color: 1
Size: 8 Color: 17
Size: 4 Color: 15

Bin 61: 3 of cap free
Amount of items: 11
Items: 
Size: 3913 Color: 2
Size: 488 Color: 13
Size: 464 Color: 0
Size: 436 Color: 0
Size: 432 Color: 12
Size: 408 Color: 5
Size: 400 Color: 3
Size: 384 Color: 8
Size: 368 Color: 1
Size: 352 Color: 11
Size: 176 Color: 17

Bin 62: 3 of cap free
Amount of items: 5
Items: 
Size: 3929 Color: 4
Size: 1824 Color: 13
Size: 1604 Color: 19
Size: 232 Color: 18
Size: 232 Color: 10

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 3932 Color: 14
Size: 3701 Color: 2
Size: 188 Color: 9

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 5707 Color: 9
Size: 2114 Color: 12

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 11
Size: 1812 Color: 8

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 9
Size: 961 Color: 2
Size: 208 Color: 0

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6706 Color: 16
Size: 1115 Color: 18

Bin 68: 4 of cap free
Amount of items: 6
Items: 
Size: 3917 Color: 9
Size: 1182 Color: 16
Size: 880 Color: 8
Size: 802 Color: 0
Size: 801 Color: 3
Size: 238 Color: 4

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 3921 Color: 7
Size: 3899 Color: 10

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 4916 Color: 16
Size: 2904 Color: 0

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 5392 Color: 16
Size: 2428 Color: 10

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 6110 Color: 1
Size: 1542 Color: 12
Size: 168 Color: 8

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6247 Color: 11
Size: 1573 Color: 19

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 6744 Color: 18
Size: 980 Color: 2
Size: 96 Color: 16

Bin 75: 4 of cap free
Amount of items: 2
Items: 
Size: 6872 Color: 19
Size: 948 Color: 12

Bin 76: 4 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 3
Size: 912 Color: 11

Bin 77: 5 of cap free
Amount of items: 3
Items: 
Size: 5210 Color: 8
Size: 1452 Color: 8
Size: 1157 Color: 13

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 6054 Color: 17
Size: 1765 Color: 0

Bin 79: 5 of cap free
Amount of items: 2
Items: 
Size: 6508 Color: 19
Size: 1311 Color: 10

Bin 80: 5 of cap free
Amount of items: 2
Items: 
Size: 6698 Color: 10
Size: 1121 Color: 9

Bin 81: 6 of cap free
Amount of items: 30
Items: 
Size: 394 Color: 2
Size: 384 Color: 7
Size: 360 Color: 10
Size: 352 Color: 13
Size: 352 Color: 0
Size: 340 Color: 19
Size: 336 Color: 8
Size: 320 Color: 5
Size: 304 Color: 17
Size: 304 Color: 11
Size: 288 Color: 4
Size: 288 Color: 1
Size: 284 Color: 1
Size: 272 Color: 12
Size: 256 Color: 7
Size: 256 Color: 1
Size: 230 Color: 6
Size: 228 Color: 9
Size: 224 Color: 7
Size: 216 Color: 16
Size: 216 Color: 2
Size: 194 Color: 13
Size: 192 Color: 18
Size: 192 Color: 12
Size: 192 Color: 6
Size: 192 Color: 5
Size: 176 Color: 15
Size: 176 Color: 4
Size: 152 Color: 11
Size: 148 Color: 18

Bin 82: 6 of cap free
Amount of items: 3
Items: 
Size: 4984 Color: 12
Size: 2458 Color: 10
Size: 376 Color: 2

Bin 83: 6 of cap free
Amount of items: 2
Items: 
Size: 5202 Color: 4
Size: 2616 Color: 5

Bin 84: 6 of cap free
Amount of items: 3
Items: 
Size: 5518 Color: 8
Size: 2060 Color: 5
Size: 240 Color: 15

Bin 85: 7 of cap free
Amount of items: 2
Items: 
Size: 5797 Color: 1
Size: 2020 Color: 16

Bin 86: 8 of cap free
Amount of items: 7
Items: 
Size: 3916 Color: 18
Size: 728 Color: 9
Size: 724 Color: 13
Size: 710 Color: 4
Size: 650 Color: 18
Size: 596 Color: 5
Size: 492 Color: 7

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 6574 Color: 17
Size: 1242 Color: 10

Bin 88: 8 of cap free
Amount of items: 2
Items: 
Size: 6918 Color: 19
Size: 898 Color: 5

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 4
Size: 1403 Color: 6

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 6677 Color: 6
Size: 1138 Color: 12

Bin 91: 10 of cap free
Amount of items: 3
Items: 
Size: 3918 Color: 17
Size: 3236 Color: 4
Size: 660 Color: 5

Bin 92: 10 of cap free
Amount of items: 2
Items: 
Size: 6934 Color: 8
Size: 880 Color: 9

Bin 93: 10 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 10
Size: 808 Color: 4
Size: 16 Color: 5

Bin 94: 11 of cap free
Amount of items: 2
Items: 
Size: 4822 Color: 17
Size: 2991 Color: 14

Bin 95: 12 of cap free
Amount of items: 3
Items: 
Size: 4878 Color: 11
Size: 2774 Color: 10
Size: 160 Color: 1

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 4
Size: 1464 Color: 13

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 3
Size: 944 Color: 6

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 6517 Color: 7
Size: 1294 Color: 0

Bin 99: 17 of cap free
Amount of items: 2
Items: 
Size: 6437 Color: 12
Size: 1370 Color: 0

Bin 100: 17 of cap free
Amount of items: 3
Items: 
Size: 6671 Color: 16
Size: 1100 Color: 9
Size: 36 Color: 13

Bin 101: 18 of cap free
Amount of items: 2
Items: 
Size: 5919 Color: 2
Size: 1887 Color: 3

Bin 102: 18 of cap free
Amount of items: 2
Items: 
Size: 6034 Color: 6
Size: 1772 Color: 3

Bin 103: 18 of cap free
Amount of items: 2
Items: 
Size: 6968 Color: 9
Size: 838 Color: 0

Bin 104: 20 of cap free
Amount of items: 8
Items: 
Size: 3914 Color: 9
Size: 650 Color: 15
Size: 648 Color: 16
Size: 648 Color: 14
Size: 648 Color: 9
Size: 552 Color: 14
Size: 512 Color: 3
Size: 232 Color: 7

Bin 105: 20 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 19
Size: 1616 Color: 0

Bin 106: 20 of cap free
Amount of items: 2
Items: 
Size: 6862 Color: 0
Size: 942 Color: 4

Bin 107: 21 of cap free
Amount of items: 2
Items: 
Size: 6101 Color: 8
Size: 1702 Color: 2

Bin 108: 24 of cap free
Amount of items: 2
Items: 
Size: 5404 Color: 13
Size: 2396 Color: 7

Bin 109: 26 of cap free
Amount of items: 2
Items: 
Size: 6756 Color: 1
Size: 1042 Color: 14

Bin 110: 27 of cap free
Amount of items: 2
Items: 
Size: 6433 Color: 1
Size: 1364 Color: 6

Bin 111: 28 of cap free
Amount of items: 4
Items: 
Size: 3948 Color: 11
Size: 3244 Color: 16
Size: 372 Color: 9
Size: 232 Color: 13

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 6334 Color: 5
Size: 1462 Color: 3

Bin 113: 28 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 0
Size: 1400 Color: 5

Bin 114: 30 of cap free
Amount of items: 3
Items: 
Size: 5672 Color: 12
Size: 1650 Color: 13
Size: 472 Color: 4

Bin 115: 33 of cap free
Amount of items: 2
Items: 
Size: 5814 Color: 1
Size: 1977 Color: 8

Bin 116: 34 of cap free
Amount of items: 3
Items: 
Size: 5224 Color: 16
Size: 2376 Color: 2
Size: 190 Color: 14

Bin 117: 35 of cap free
Amount of items: 2
Items: 
Size: 6481 Color: 3
Size: 1308 Color: 16

Bin 118: 39 of cap free
Amount of items: 2
Items: 
Size: 4524 Color: 1
Size: 3261 Color: 14

Bin 119: 39 of cap free
Amount of items: 2
Items: 
Size: 5700 Color: 9
Size: 2085 Color: 6

Bin 120: 40 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 6
Size: 1928 Color: 0
Size: 500 Color: 2

Bin 121: 49 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 14
Size: 1691 Color: 16

Bin 122: 50 of cap free
Amount of items: 2
Items: 
Size: 5912 Color: 8
Size: 1862 Color: 14

Bin 123: 51 of cap free
Amount of items: 2
Items: 
Size: 5149 Color: 7
Size: 2624 Color: 8

Bin 124: 53 of cap free
Amount of items: 3
Items: 
Size: 4237 Color: 11
Size: 3262 Color: 1
Size: 272 Color: 7

Bin 125: 67 of cap free
Amount of items: 2
Items: 
Size: 6081 Color: 1
Size: 1676 Color: 10

Bin 126: 70 of cap free
Amount of items: 2
Items: 
Size: 4696 Color: 14
Size: 3058 Color: 0

Bin 127: 71 of cap free
Amount of items: 2
Items: 
Size: 5522 Color: 3
Size: 2231 Color: 10

Bin 128: 72 of cap free
Amount of items: 2
Items: 
Size: 4956 Color: 17
Size: 2796 Color: 5

Bin 129: 83 of cap free
Amount of items: 3
Items: 
Size: 3928 Color: 14
Size: 2541 Color: 15
Size: 1272 Color: 1

Bin 130: 88 of cap free
Amount of items: 2
Items: 
Size: 4476 Color: 18
Size: 3260 Color: 11

Bin 131: 101 of cap free
Amount of items: 2
Items: 
Size: 5367 Color: 4
Size: 2356 Color: 11

Bin 132: 117 of cap free
Amount of items: 3
Items: 
Size: 4450 Color: 15
Size: 2715 Color: 2
Size: 542 Color: 13

Bin 133: 6102 of cap free
Amount of items: 11
Items: 
Size: 192 Color: 5
Size: 176 Color: 19
Size: 164 Color: 3
Size: 160 Color: 13
Size: 160 Color: 8
Size: 160 Color: 6
Size: 158 Color: 15
Size: 152 Color: 11
Size: 144 Color: 0
Size: 128 Color: 18
Size: 128 Color: 2

Total size: 1032768
Total free space: 7824

