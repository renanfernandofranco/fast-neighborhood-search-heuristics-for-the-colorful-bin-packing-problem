Capicity Bin: 19008
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 9509 Color: 11
Size: 2342 Color: 7
Size: 2251 Color: 18
Size: 2141 Color: 17
Size: 2021 Color: 8
Size: 400 Color: 12
Size: 344 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9538 Color: 0
Size: 7894 Color: 18
Size: 1576 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 18
Size: 6496 Color: 13
Size: 496 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 6
Size: 5743 Color: 19
Size: 1148 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 6
Size: 6212 Color: 6
Size: 502 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 5
Size: 4362 Color: 18
Size: 1726 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12940 Color: 4
Size: 5010 Color: 9
Size: 1058 Color: 4

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 12
Size: 4998 Color: 10
Size: 490 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13673 Color: 15
Size: 4447 Color: 8
Size: 888 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 4
Size: 5028 Color: 7
Size: 200 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13842 Color: 19
Size: 4494 Color: 4
Size: 672 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 2
Size: 4364 Color: 14
Size: 428 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 10
Size: 4272 Color: 17
Size: 512 Color: 7

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 18
Size: 4372 Color: 9
Size: 388 Color: 12

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14269 Color: 2
Size: 3951 Color: 2
Size: 788 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 9
Size: 4552 Color: 19
Size: 144 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 11
Size: 3828 Color: 11
Size: 784 Color: 8

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 13
Size: 4426 Color: 3
Size: 130 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 9
Size: 3778 Color: 13
Size: 752 Color: 15

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14805 Color: 16
Size: 3503 Color: 2
Size: 700 Color: 13

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14806 Color: 13
Size: 2354 Color: 2
Size: 1848 Color: 10

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14808 Color: 18
Size: 3512 Color: 5
Size: 688 Color: 19

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15006 Color: 11
Size: 3502 Color: 5
Size: 500 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15128 Color: 7
Size: 3480 Color: 10
Size: 400 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15246 Color: 19
Size: 2008 Color: 9
Size: 1754 Color: 15

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15306 Color: 1
Size: 3354 Color: 8
Size: 348 Color: 9

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15372 Color: 5
Size: 2060 Color: 7
Size: 1576 Color: 14

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 15498 Color: 19
Size: 2158 Color: 8
Size: 1352 Color: 16

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 15544 Color: 18
Size: 3288 Color: 12
Size: 176 Color: 7

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 15748 Color: 10
Size: 2492 Color: 16
Size: 768 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 15754 Color: 15
Size: 2742 Color: 19
Size: 512 Color: 17

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 4
Size: 2996 Color: 14
Size: 192 Color: 9

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 15822 Color: 15
Size: 2658 Color: 18
Size: 528 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 15896 Color: 17
Size: 2732 Color: 12
Size: 380 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16084 Color: 10
Size: 1928 Color: 3
Size: 996 Color: 13

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16136 Color: 1
Size: 1612 Color: 15
Size: 1260 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16208 Color: 1
Size: 2352 Color: 15
Size: 448 Color: 13

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16340 Color: 16
Size: 1580 Color: 5
Size: 1088 Color: 5

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16372 Color: 8
Size: 2100 Color: 1
Size: 536 Color: 18

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16464 Color: 4
Size: 1384 Color: 1
Size: 1160 Color: 19

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16471 Color: 7
Size: 2115 Color: 0
Size: 422 Color: 9

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16567 Color: 17
Size: 2035 Color: 9
Size: 406 Color: 18

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16582 Color: 17
Size: 1930 Color: 1
Size: 496 Color: 15

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16664 Color: 4
Size: 1632 Color: 18
Size: 712 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 16668 Color: 4
Size: 1956 Color: 14
Size: 384 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 16740 Color: 17
Size: 1152 Color: 7
Size: 1116 Color: 16

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 16688 Color: 11
Size: 1168 Color: 1
Size: 1152 Color: 18

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 16694 Color: 10
Size: 1950 Color: 15
Size: 364 Color: 12

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 17
Size: 1580 Color: 16
Size: 672 Color: 7

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 4
Size: 1360 Color: 1
Size: 872 Color: 14

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 16798 Color: 18
Size: 1842 Color: 4
Size: 368 Color: 17

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 19
Size: 1568 Color: 11
Size: 576 Color: 12

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 16900 Color: 11
Size: 1152 Color: 12
Size: 956 Color: 12

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 16906 Color: 12
Size: 1674 Color: 18
Size: 428 Color: 17

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 16964 Color: 5
Size: 1334 Color: 15
Size: 710 Color: 19

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 5
Size: 1824 Color: 19
Size: 208 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17036 Color: 17
Size: 1568 Color: 13
Size: 404 Color: 16

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17018 Color: 11
Size: 1536 Color: 2
Size: 454 Color: 13

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17066 Color: 16
Size: 1622 Color: 7
Size: 320 Color: 8

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17076 Color: 15
Size: 1602 Color: 17
Size: 330 Color: 12

Bin 61: 1 of cap free
Amount of items: 9
Items: 
Size: 9505 Color: 7
Size: 1708 Color: 19
Size: 1576 Color: 12
Size: 1576 Color: 6
Size: 1260 Color: 3
Size: 1256 Color: 18
Size: 974 Color: 19
Size: 768 Color: 4
Size: 384 Color: 11

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 4
Size: 7913 Color: 2
Size: 416 Color: 19

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 10811 Color: 5
Size: 7908 Color: 10
Size: 288 Color: 11

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 11611 Color: 14
Size: 6792 Color: 12
Size: 604 Color: 1

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14467 Color: 9
Size: 4264 Color: 13
Size: 276 Color: 4

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14998 Color: 8
Size: 2729 Color: 10
Size: 1280 Color: 4

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 15135 Color: 16
Size: 3376 Color: 11
Size: 496 Color: 0

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 15187 Color: 3
Size: 2272 Color: 17
Size: 1548 Color: 14

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 15264 Color: 14
Size: 3743 Color: 4

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 15388 Color: 4
Size: 2627 Color: 7
Size: 992 Color: 17

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 15976 Color: 7
Size: 2029 Color: 0
Size: 1002 Color: 1

Bin 72: 1 of cap free
Amount of items: 3
Items: 
Size: 15985 Color: 5
Size: 2022 Color: 17
Size: 1000 Color: 13

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 16239 Color: 16
Size: 1608 Color: 12
Size: 1160 Color: 8

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 16439 Color: 17
Size: 1672 Color: 16
Size: 896 Color: 7

Bin 75: 1 of cap free
Amount of items: 2
Items: 
Size: 16486 Color: 12
Size: 2521 Color: 14

Bin 76: 1 of cap free
Amount of items: 3
Items: 
Size: 16534 Color: 10
Size: 1929 Color: 10
Size: 544 Color: 7

Bin 77: 2 of cap free
Amount of items: 21
Items: 
Size: 1120 Color: 17
Size: 1120 Color: 15
Size: 1116 Color: 19
Size: 1094 Color: 18
Size: 1008 Color: 10
Size: 1000 Color: 3
Size: 996 Color: 1
Size: 992 Color: 18
Size: 992 Color: 12
Size: 912 Color: 17
Size: 896 Color: 10
Size: 884 Color: 17
Size: 860 Color: 5
Size: 832 Color: 15
Size: 832 Color: 0
Size: 800 Color: 11
Size: 800 Color: 8
Size: 760 Color: 14
Size: 672 Color: 3
Size: 664 Color: 4
Size: 656 Color: 14

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11007 Color: 19
Size: 7631 Color: 12
Size: 368 Color: 13

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 12052 Color: 7
Size: 6480 Color: 15
Size: 474 Color: 11

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 10
Size: 5606 Color: 9
Size: 448 Color: 6

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 16
Size: 5582 Color: 7
Size: 412 Color: 17

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 18
Size: 4976 Color: 9
Size: 412 Color: 17

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 13698 Color: 19
Size: 5004 Color: 12
Size: 304 Color: 16

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 14722 Color: 17
Size: 2444 Color: 9
Size: 1840 Color: 14

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 15920 Color: 15
Size: 3086 Color: 4

Bin 86: 2 of cap free
Amount of items: 2
Items: 
Size: 16696 Color: 12
Size: 2310 Color: 7

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 10704 Color: 12
Size: 7917 Color: 8
Size: 384 Color: 17

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 2
Size: 5570 Color: 14
Size: 448 Color: 11

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 14741 Color: 8
Size: 3396 Color: 19
Size: 868 Color: 17

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 15589 Color: 5
Size: 3240 Color: 18
Size: 176 Color: 17

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 16277 Color: 16
Size: 2028 Color: 17
Size: 700 Color: 14

Bin 92: 4 of cap free
Amount of items: 35
Items: 
Size: 756 Color: 6
Size: 752 Color: 13
Size: 704 Color: 10
Size: 688 Color: 6
Size: 672 Color: 1
Size: 668 Color: 17
Size: 658 Color: 12
Size: 640 Color: 11
Size: 624 Color: 1
Size: 616 Color: 19
Size: 600 Color: 11
Size: 600 Color: 7
Size: 598 Color: 3
Size: 592 Color: 4
Size: 584 Color: 4
Size: 576 Color: 6
Size: 576 Color: 2
Size: 568 Color: 0
Size: 556 Color: 14
Size: 544 Color: 4
Size: 536 Color: 11
Size: 528 Color: 14
Size: 524 Color: 11
Size: 480 Color: 16
Size: 464 Color: 17
Size: 464 Color: 9
Size: 456 Color: 10
Size: 448 Color: 14
Size: 432 Color: 2
Size: 376 Color: 7
Size: 376 Color: 5
Size: 356 Color: 0
Size: 352 Color: 5
Size: 352 Color: 0
Size: 288 Color: 7

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 12310 Color: 10
Size: 6290 Color: 6
Size: 404 Color: 12

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 7
Size: 4982 Color: 6
Size: 1008 Color: 12

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 14840 Color: 4
Size: 3844 Color: 17
Size: 320 Color: 14

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 15772 Color: 12
Size: 3020 Color: 4
Size: 212 Color: 11

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 11976 Color: 10
Size: 7027 Color: 17

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 11430 Color: 7
Size: 7104 Color: 16
Size: 468 Color: 2

Bin 99: 6 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 5
Size: 5776 Color: 3
Size: 1764 Color: 3

Bin 100: 6 of cap free
Amount of items: 2
Items: 
Size: 16002 Color: 15
Size: 3000 Color: 1

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 16518 Color: 3
Size: 2484 Color: 4

Bin 102: 7 of cap free
Amount of items: 3
Items: 
Size: 11003 Color: 19
Size: 7154 Color: 8
Size: 844 Color: 0

Bin 103: 7 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 15
Size: 4970 Color: 9
Size: 768 Color: 9

Bin 104: 8 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 6
Size: 5804 Color: 3
Size: 1176 Color: 18

Bin 105: 8 of cap free
Amount of items: 3
Items: 
Size: 12282 Color: 12
Size: 6302 Color: 12
Size: 416 Color: 19

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 17
Size: 5828 Color: 15
Size: 208 Color: 18

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 16938 Color: 2
Size: 2062 Color: 12

Bin 108: 9 of cap free
Amount of items: 2
Items: 
Size: 13939 Color: 18
Size: 5060 Color: 17

Bin 109: 10 of cap free
Amount of items: 7
Items: 
Size: 9506 Color: 19
Size: 1829 Color: 13
Size: 1789 Color: 10
Size: 1748 Color: 15
Size: 1744 Color: 13
Size: 1582 Color: 0
Size: 800 Color: 6

Bin 110: 10 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 17
Size: 7892 Color: 13
Size: 1582 Color: 10

Bin 111: 10 of cap free
Amount of items: 2
Items: 
Size: 15656 Color: 7
Size: 3342 Color: 11

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 16422 Color: 5
Size: 2576 Color: 2

Bin 113: 10 of cap free
Amount of items: 2
Items: 
Size: 16492 Color: 16
Size: 2506 Color: 8

Bin 114: 10 of cap free
Amount of items: 3
Items: 
Size: 16670 Color: 14
Size: 2264 Color: 11
Size: 64 Color: 9

Bin 115: 10 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 1
Size: 1964 Color: 11
Size: 32 Color: 2

Bin 116: 11 of cap free
Amount of items: 2
Items: 
Size: 12326 Color: 13
Size: 6671 Color: 8

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 16309 Color: 12
Size: 2688 Color: 16

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 16952 Color: 14
Size: 2045 Color: 2

Bin 119: 12 of cap free
Amount of items: 2
Items: 
Size: 14404 Color: 7
Size: 4592 Color: 0

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 15658 Color: 11
Size: 3338 Color: 1

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 16202 Color: 14
Size: 2794 Color: 1

Bin 122: 12 of cap free
Amount of items: 2
Items: 
Size: 16296 Color: 18
Size: 2700 Color: 16

Bin 123: 14 of cap free
Amount of items: 2
Items: 
Size: 15420 Color: 14
Size: 3574 Color: 15

Bin 124: 14 of cap free
Amount of items: 2
Items: 
Size: 15622 Color: 11
Size: 3372 Color: 14

Bin 125: 14 of cap free
Amount of items: 3
Items: 
Size: 16018 Color: 9
Size: 2864 Color: 5
Size: 112 Color: 13

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 16916 Color: 8
Size: 2078 Color: 3

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 14768 Color: 0
Size: 4225 Color: 8

Bin 128: 16 of cap free
Amount of items: 2
Items: 
Size: 15064 Color: 3
Size: 3928 Color: 8

Bin 129: 17 of cap free
Amount of items: 2
Items: 
Size: 11248 Color: 3
Size: 7743 Color: 16

Bin 130: 17 of cap free
Amount of items: 2
Items: 
Size: 16065 Color: 16
Size: 2926 Color: 18

Bin 131: 17 of cap free
Amount of items: 2
Items: 
Size: 16583 Color: 12
Size: 2408 Color: 18

Bin 132: 17 of cap free
Amount of items: 2
Items: 
Size: 16863 Color: 5
Size: 2128 Color: 6

Bin 133: 18 of cap free
Amount of items: 3
Items: 
Size: 11047 Color: 0
Size: 6831 Color: 7
Size: 1112 Color: 10

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 16454 Color: 7
Size: 2536 Color: 10

Bin 135: 19 of cap free
Amount of items: 2
Items: 
Size: 16616 Color: 11
Size: 2373 Color: 14

Bin 136: 20 of cap free
Amount of items: 5
Items: 
Size: 9512 Color: 2
Size: 2997 Color: 12
Size: 2860 Color: 9
Size: 2851 Color: 13
Size: 768 Color: 14

Bin 137: 20 of cap free
Amount of items: 3
Items: 
Size: 9522 Color: 13
Size: 7884 Color: 10
Size: 1582 Color: 5

Bin 138: 20 of cap free
Amount of items: 2
Items: 
Size: 14420 Color: 7
Size: 4568 Color: 17

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 17096 Color: 5
Size: 1892 Color: 14

Bin 140: 22 of cap free
Amount of items: 2
Items: 
Size: 14964 Color: 18
Size: 4022 Color: 16

Bin 141: 22 of cap free
Amount of items: 3
Items: 
Size: 15055 Color: 16
Size: 3295 Color: 10
Size: 636 Color: 17

Bin 142: 24 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 10
Size: 8816 Color: 16
Size: 640 Color: 7

Bin 143: 24 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 14
Size: 5044 Color: 2
Size: 176 Color: 7

Bin 144: 24 of cap free
Amount of items: 2
Items: 
Size: 16816 Color: 12
Size: 2168 Color: 4

Bin 145: 24 of cap free
Amount of items: 2
Items: 
Size: 17016 Color: 3
Size: 1968 Color: 14

Bin 146: 25 of cap free
Amount of items: 2
Items: 
Size: 15735 Color: 2
Size: 3248 Color: 10

Bin 147: 25 of cap free
Amount of items: 2
Items: 
Size: 16815 Color: 5
Size: 2168 Color: 14

Bin 148: 26 of cap free
Amount of items: 2
Items: 
Size: 11446 Color: 4
Size: 7536 Color: 1

Bin 149: 26 of cap free
Amount of items: 3
Items: 
Size: 17086 Color: 6
Size: 1864 Color: 5
Size: 32 Color: 15

Bin 150: 29 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 3
Size: 5019 Color: 19
Size: 432 Color: 12

Bin 151: 30 of cap free
Amount of items: 3
Items: 
Size: 9520 Color: 5
Size: 7922 Color: 16
Size: 1536 Color: 5

Bin 152: 30 of cap free
Amount of items: 3
Items: 
Size: 9717 Color: 12
Size: 8397 Color: 9
Size: 864 Color: 7

Bin 153: 30 of cap free
Amount of items: 2
Items: 
Size: 10690 Color: 15
Size: 8288 Color: 12

Bin 154: 31 of cap free
Amount of items: 2
Items: 
Size: 15792 Color: 3
Size: 3185 Color: 19

Bin 155: 32 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 1
Size: 5256 Color: 17
Size: 160 Color: 7

Bin 156: 32 of cap free
Amount of items: 2
Items: 
Size: 14960 Color: 8
Size: 4016 Color: 19

Bin 157: 33 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 10
Size: 4789 Color: 5

Bin 158: 35 of cap free
Amount of items: 2
Items: 
Size: 12655 Color: 2
Size: 6318 Color: 14

Bin 159: 35 of cap free
Amount of items: 2
Items: 
Size: 16769 Color: 1
Size: 2204 Color: 3

Bin 160: 36 of cap free
Amount of items: 3
Items: 
Size: 12008 Color: 9
Size: 5598 Color: 17
Size: 1366 Color: 6

Bin 161: 36 of cap free
Amount of items: 2
Items: 
Size: 16695 Color: 14
Size: 2277 Color: 1

Bin 162: 37 of cap free
Amount of items: 3
Items: 
Size: 9517 Color: 9
Size: 7792 Color: 3
Size: 1662 Color: 13

Bin 163: 38 of cap free
Amount of items: 3
Items: 
Size: 13904 Color: 18
Size: 4306 Color: 17
Size: 760 Color: 12

Bin 164: 38 of cap free
Amount of items: 2
Items: 
Size: 15413 Color: 8
Size: 3557 Color: 9

Bin 165: 39 of cap free
Amount of items: 4
Items: 
Size: 9513 Color: 0
Size: 3804 Color: 10
Size: 3785 Color: 7
Size: 1867 Color: 6

Bin 166: 39 of cap free
Amount of items: 2
Items: 
Size: 16161 Color: 18
Size: 2808 Color: 5

Bin 167: 40 of cap free
Amount of items: 2
Items: 
Size: 16284 Color: 18
Size: 2684 Color: 10

Bin 168: 44 of cap free
Amount of items: 2
Items: 
Size: 15120 Color: 8
Size: 3844 Color: 7

Bin 169: 45 of cap free
Amount of items: 2
Items: 
Size: 16687 Color: 9
Size: 2276 Color: 18

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 16580 Color: 9
Size: 2365 Color: 18

Bin 171: 64 of cap free
Amount of items: 2
Items: 
Size: 15408 Color: 18
Size: 3536 Color: 16

Bin 172: 66 of cap free
Amount of items: 2
Items: 
Size: 16186 Color: 5
Size: 2756 Color: 13

Bin 173: 68 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 16
Size: 4802 Color: 10

Bin 174: 70 of cap free
Amount of items: 2
Items: 
Size: 12004 Color: 4
Size: 6934 Color: 9

Bin 175: 76 of cap free
Amount of items: 2
Items: 
Size: 9556 Color: 5
Size: 9376 Color: 15

Bin 176: 78 of cap free
Amount of items: 3
Items: 
Size: 9680 Color: 8
Size: 7906 Color: 11
Size: 1344 Color: 13

Bin 177: 79 of cap free
Amount of items: 2
Items: 
Size: 16575 Color: 15
Size: 2354 Color: 9

Bin 178: 82 of cap free
Amount of items: 2
Items: 
Size: 15788 Color: 6
Size: 3138 Color: 11

Bin 179: 88 of cap free
Amount of items: 2
Items: 
Size: 15884 Color: 10
Size: 3036 Color: 5

Bin 180: 92 of cap free
Amount of items: 2
Items: 
Size: 14940 Color: 2
Size: 3976 Color: 9

Bin 181: 92 of cap free
Amount of items: 2
Items: 
Size: 16028 Color: 19
Size: 2888 Color: 8

Bin 182: 98 of cap free
Amount of items: 2
Items: 
Size: 13046 Color: 17
Size: 5864 Color: 11

Bin 183: 106 of cap free
Amount of items: 2
Items: 
Size: 16408 Color: 10
Size: 2494 Color: 1

Bin 184: 111 of cap free
Amount of items: 2
Items: 
Size: 13602 Color: 2
Size: 5295 Color: 15

Bin 185: 120 of cap free
Amount of items: 2
Items: 
Size: 13040 Color: 1
Size: 5848 Color: 14

Bin 186: 127 of cap free
Amount of items: 2
Items: 
Size: 15857 Color: 11
Size: 3024 Color: 0

Bin 187: 134 of cap free
Amount of items: 2
Items: 
Size: 13030 Color: 19
Size: 5844 Color: 8

Bin 188: 150 of cap free
Amount of items: 2
Items: 
Size: 9540 Color: 9
Size: 9318 Color: 18

Bin 189: 150 of cap free
Amount of items: 2
Items: 
Size: 13778 Color: 19
Size: 5080 Color: 14

Bin 190: 164 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 4
Size: 6772 Color: 1
Size: 1232 Color: 10

Bin 191: 164 of cap free
Amount of items: 2
Items: 
Size: 10900 Color: 0
Size: 7944 Color: 16

Bin 192: 187 of cap free
Amount of items: 2
Items: 
Size: 12656 Color: 6
Size: 6165 Color: 15

Bin 193: 188 of cap free
Amount of items: 2
Items: 
Size: 12980 Color: 18
Size: 5840 Color: 17

Bin 194: 196 of cap free
Amount of items: 2
Items: 
Size: 10884 Color: 2
Size: 7928 Color: 5

Bin 195: 204 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 11
Size: 6808 Color: 0

Bin 196: 212 of cap free
Amount of items: 2
Items: 
Size: 10872 Color: 13
Size: 7924 Color: 1

Bin 197: 229 of cap free
Amount of items: 3
Items: 
Size: 9514 Color: 11
Size: 7921 Color: 17
Size: 1344 Color: 6

Bin 198: 283 of cap free
Amount of items: 3
Items: 
Size: 9508 Color: 7
Size: 6764 Color: 9
Size: 2453 Color: 13

Bin 199: 13720 of cap free
Amount of items: 15
Items: 
Size: 404 Color: 15
Size: 404 Color: 8
Size: 384 Color: 15
Size: 384 Color: 9
Size: 372 Color: 14
Size: 368 Color: 4
Size: 352 Color: 2
Size: 344 Color: 6
Size: 336 Color: 7
Size: 332 Color: 19
Size: 328 Color: 19
Size: 320 Color: 12
Size: 320 Color: 12
Size: 320 Color: 5
Size: 320 Color: 1

Total size: 3763584
Total free space: 19008

