Capicity Bin: 16752
Lower Bound: 198

Bins used: 199
Amount of Colors: 601

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 8388 Color: 411
Size: 2662 Color: 299
Size: 2136 Color: 266
Size: 2130 Color: 265
Size: 664 Color: 131
Size: 388 Color: 59
Size: 384 Color: 58

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 423
Size: 6888 Color: 392
Size: 340 Color: 39

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 11340 Color: 447
Size: 5164 Color: 369
Size: 248 Color: 12

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12636 Color: 473
Size: 3484 Color: 332
Size: 632 Color: 124

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 12664 Color: 474
Size: 2494 Color: 290
Size: 1594 Color: 218

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 12668 Color: 475
Size: 3660 Color: 339
Size: 424 Color: 75

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 485
Size: 2600 Color: 297
Size: 1200 Color: 187

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13269 Color: 492
Size: 2507 Color: 292
Size: 976 Color: 168

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 13324 Color: 494
Size: 2860 Color: 310
Size: 568 Color: 117

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13633 Color: 508
Size: 2601 Color: 298
Size: 518 Color: 102

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13692 Color: 510
Size: 1700 Color: 225
Size: 1360 Color: 194

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 511
Size: 1959 Color: 248
Size: 1070 Color: 178

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13756 Color: 513
Size: 1944 Color: 246
Size: 1052 Color: 176

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13800 Color: 515
Size: 2664 Color: 300
Size: 288 Color: 23

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13896 Color: 518
Size: 1462 Color: 207
Size: 1394 Color: 203

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13919 Color: 519
Size: 2555 Color: 295
Size: 278 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 520
Size: 2262 Color: 272
Size: 560 Color: 112

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13951 Color: 523
Size: 2335 Color: 280
Size: 466 Color: 90

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13996 Color: 526
Size: 2288 Color: 275
Size: 468 Color: 91

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 14002 Color: 527
Size: 1470 Color: 208
Size: 1280 Color: 192

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 14032 Color: 530
Size: 2392 Color: 285
Size: 328 Color: 35

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14035 Color: 531
Size: 2339 Color: 282
Size: 378 Color: 55

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 14042 Color: 532
Size: 2682 Color: 302
Size: 28 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14198 Color: 537
Size: 1714 Color: 227
Size: 840 Color: 155

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14200 Color: 538
Size: 1804 Color: 235
Size: 748 Color: 146

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14207 Color: 539
Size: 2289 Color: 276
Size: 256 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14210 Color: 540
Size: 1862 Color: 240
Size: 680 Color: 134

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14285 Color: 545
Size: 2121 Color: 263
Size: 346 Color: 43

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14322 Color: 547
Size: 1232 Color: 191
Size: 1198 Color: 185

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14324 Color: 548
Size: 2026 Color: 255
Size: 402 Color: 65

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14370 Color: 550
Size: 1986 Color: 253
Size: 396 Color: 63

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14379 Color: 551
Size: 1979 Color: 252
Size: 394 Color: 62

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14514 Color: 561
Size: 1978 Color: 251
Size: 260 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 562
Size: 1826 Color: 237
Size: 404 Color: 66

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14523 Color: 563
Size: 1859 Color: 239
Size: 370 Color: 52

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14552 Color: 565
Size: 1636 Color: 219
Size: 564 Color: 113

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14562 Color: 566
Size: 1662 Color: 221
Size: 528 Color: 103

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14571 Color: 567
Size: 1819 Color: 236
Size: 362 Color: 48

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14575 Color: 568
Size: 1801 Color: 233
Size: 376 Color: 54

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14652 Color: 572
Size: 1642 Color: 220
Size: 458 Color: 85

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 575
Size: 1688 Color: 223
Size: 380 Color: 56

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14698 Color: 576
Size: 1394 Color: 201
Size: 660 Color: 130

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14710 Color: 578
Size: 1974 Color: 250
Size: 68 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14744 Color: 581
Size: 1476 Color: 209
Size: 532 Color: 105

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14846 Color: 584
Size: 1482 Color: 210
Size: 424 Color: 74

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14854 Color: 585
Size: 1394 Color: 202
Size: 504 Color: 99

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14878 Color: 588
Size: 1484 Color: 211
Size: 390 Color: 60

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14952 Color: 590
Size: 1216 Color: 190
Size: 584 Color: 120

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14958 Color: 591
Size: 1498 Color: 212
Size: 296 Color: 26

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14972 Color: 592
Size: 944 Color: 162
Size: 836 Color: 153

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14988 Color: 594
Size: 1184 Color: 183
Size: 580 Color: 118

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 15002 Color: 596
Size: 1152 Color: 182
Size: 598 Color: 121

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 15032 Color: 599
Size: 1136 Color: 181
Size: 584 Color: 119

Bin 54: 1 of cap free
Amount of items: 5
Items: 
Size: 9575 Color: 425
Size: 5992 Color: 381
Size: 512 Color: 101
Size: 336 Color: 38
Size: 336 Color: 37

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 12563 Color: 469
Size: 3736 Color: 342
Size: 452 Color: 82

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 12726 Color: 476
Size: 3485 Color: 333
Size: 540 Color: 108

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12790 Color: 479
Size: 2399 Color: 286
Size: 1562 Color: 214

Bin 58: 1 of cap free
Amount of items: 2
Items: 
Size: 12888 Color: 483
Size: 3863 Color: 344

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 12945 Color: 484
Size: 3302 Color: 325
Size: 504 Color: 98

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 13384 Color: 497
Size: 2299 Color: 277
Size: 1068 Color: 177

Bin 61: 1 of cap free
Amount of items: 2
Items: 
Size: 13490 Color: 502
Size: 3261 Color: 323

Bin 62: 1 of cap free
Amount of items: 2
Items: 
Size: 13817 Color: 516
Size: 2934 Color: 313

Bin 63: 1 of cap free
Amount of items: 2
Items: 
Size: 13947 Color: 522
Size: 2804 Color: 307

Bin 64: 1 of cap free
Amount of items: 2
Items: 
Size: 14007 Color: 528
Size: 2744 Color: 305

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 14060 Color: 533
Size: 2691 Color: 303

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14163 Color: 535
Size: 1392 Color: 199
Size: 1196 Color: 184

Bin 67: 1 of cap free
Amount of items: 2
Items: 
Size: 14226 Color: 541
Size: 2525 Color: 294

Bin 68: 1 of cap free
Amount of items: 2
Items: 
Size: 14403 Color: 553
Size: 2348 Color: 283

Bin 69: 1 of cap free
Amount of items: 2
Items: 
Size: 14415 Color: 554
Size: 2336 Color: 281

Bin 70: 1 of cap free
Amount of items: 2
Items: 
Size: 14503 Color: 560
Size: 2248 Color: 271

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 14671 Color: 574
Size: 1592 Color: 217
Size: 488 Color: 94

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 14700 Color: 577
Size: 2051 Color: 258

Bin 73: 1 of cap free
Amount of items: 2
Items: 
Size: 14856 Color: 586
Size: 1895 Color: 243

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 9442 Color: 419
Size: 6968 Color: 393
Size: 340 Color: 40

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 11686 Color: 451
Size: 4824 Color: 364
Size: 240 Color: 10

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 12568 Color: 470
Size: 4182 Color: 350

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 12774 Color: 478
Size: 3608 Color: 338
Size: 368 Color: 51

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 13995 Color: 525
Size: 2019 Color: 254
Size: 736 Color: 142

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14450 Color: 556
Size: 2300 Color: 278

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14591 Color: 570
Size: 2159 Color: 268

Bin 81: 2 of cap free
Amount of items: 2
Items: 
Size: 14722 Color: 580
Size: 2028 Color: 256

Bin 82: 2 of cap free
Amount of items: 2
Items: 
Size: 14898 Color: 589
Size: 1852 Color: 238

Bin 83: 2 of cap free
Amount of items: 2
Items: 
Size: 15026 Color: 598
Size: 1724 Color: 228

Bin 84: 2 of cap free
Amount of items: 2
Items: 
Size: 15048 Color: 600
Size: 1702 Color: 226

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 12254 Color: 462
Size: 2903 Color: 312
Size: 1592 Color: 216

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 12571 Color: 471
Size: 3698 Color: 340
Size: 480 Color: 93

Bin 87: 3 of cap free
Amount of items: 2
Items: 
Size: 13525 Color: 503
Size: 3224 Color: 322

Bin 88: 4 of cap free
Amount of items: 7
Items: 
Size: 8382 Color: 409
Size: 1908 Color: 244
Size: 1880 Color: 241
Size: 1804 Color: 234
Size: 1694 Color: 224
Size: 688 Color: 135
Size: 392 Color: 61

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 12459 Color: 467
Size: 3201 Color: 320
Size: 1088 Color: 179

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 13038 Color: 487
Size: 3710 Color: 341

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 13424 Color: 500
Size: 3324 Color: 327

Bin 92: 4 of cap free
Amount of items: 2
Items: 
Size: 13464 Color: 501
Size: 3284 Color: 324

Bin 93: 4 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 521
Size: 2808 Color: 308

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13971 Color: 524
Size: 2777 Color: 306

Bin 95: 4 of cap free
Amount of items: 2
Items: 
Size: 14026 Color: 529
Size: 2722 Color: 304

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 543
Size: 2500 Color: 291

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14626 Color: 571
Size: 2122 Color: 264

Bin 98: 4 of cap free
Amount of items: 2
Items: 
Size: 14664 Color: 573
Size: 2084 Color: 259

Bin 99: 4 of cap free
Amount of items: 2
Items: 
Size: 14716 Color: 579
Size: 2032 Color: 257

Bin 100: 4 of cap free
Amount of items: 2
Items: 
Size: 14974 Color: 593
Size: 1774 Color: 232

Bin 101: 5 of cap free
Amount of items: 2
Items: 
Size: 11480 Color: 449
Size: 5267 Color: 373

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 14782 Color: 582
Size: 1965 Color: 249

Bin 103: 5 of cap free
Amount of items: 2
Items: 
Size: 15012 Color: 597
Size: 1735 Color: 229

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 12302 Color: 464
Size: 4444 Color: 355

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 13388 Color: 498
Size: 3358 Color: 328

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 13538 Color: 504
Size: 3208 Color: 321

Bin 107: 6 of cap free
Amount of items: 2
Items: 
Size: 13870 Color: 517
Size: 2876 Color: 311

Bin 108: 6 of cap free
Amount of items: 2
Items: 
Size: 14990 Color: 595
Size: 1756 Color: 231

Bin 109: 7 of cap free
Amount of items: 3
Items: 
Size: 10460 Color: 433
Size: 5981 Color: 380
Size: 304 Color: 28

Bin 110: 7 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 466
Size: 4209 Color: 352
Size: 96 Color: 2

Bin 111: 7 of cap free
Amount of items: 2
Items: 
Size: 14796 Color: 583
Size: 1949 Color: 247

Bin 112: 8 of cap free
Amount of items: 4
Items: 
Size: 11720 Color: 454
Size: 4552 Color: 358
Size: 236 Color: 9
Size: 236 Color: 8

Bin 113: 8 of cap free
Amount of items: 2
Items: 
Size: 12764 Color: 477
Size: 3980 Color: 348

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 13212 Color: 489
Size: 3532 Color: 336

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 13308 Color: 493
Size: 3436 Color: 331

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 14234 Color: 542
Size: 2510 Color: 293

Bin 117: 8 of cap free
Amount of items: 2
Items: 
Size: 14479 Color: 558
Size: 2265 Color: 273

Bin 118: 9 of cap free
Amount of items: 5
Items: 
Size: 8392 Color: 413
Size: 6971 Color: 394
Size: 624 Color: 123
Size: 384 Color: 57
Size: 372 Color: 53

Bin 119: 9 of cap free
Amount of items: 5
Items: 
Size: 8504 Color: 415
Size: 6973 Color: 396
Size: 544 Color: 109
Size: 362 Color: 47
Size: 360 Color: 46

Bin 120: 9 of cap free
Amount of items: 2
Items: 
Size: 13327 Color: 496
Size: 3416 Color: 330

Bin 121: 9 of cap free
Amount of items: 2
Items: 
Size: 14187 Color: 536
Size: 2556 Color: 296

Bin 122: 9 of cap free
Amount of items: 2
Items: 
Size: 14424 Color: 555
Size: 2319 Color: 279

Bin 123: 10 of cap free
Amount of items: 9
Items: 
Size: 8380 Color: 407
Size: 1442 Color: 204
Size: 1392 Color: 200
Size: 1392 Color: 198
Size: 1392 Color: 197
Size: 1392 Color: 195
Size: 536 Color: 107
Size: 408 Color: 68
Size: 408 Color: 67

Bin 124: 10 of cap free
Amount of items: 2
Items: 
Size: 12572 Color: 472
Size: 4170 Color: 349

Bin 125: 10 of cap free
Amount of items: 2
Items: 
Size: 13745 Color: 512
Size: 2997 Color: 315

Bin 126: 10 of cap free
Amount of items: 2
Items: 
Size: 14468 Color: 557
Size: 2274 Color: 274

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 559
Size: 2244 Color: 270

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 14860 Color: 587
Size: 1882 Color: 242

Bin 129: 12 of cap free
Amount of items: 3
Items: 
Size: 10433 Color: 432
Size: 5995 Color: 382
Size: 312 Color: 29

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 12812 Color: 481
Size: 3928 Color: 347

Bin 131: 12 of cap free
Amount of items: 2
Items: 
Size: 14293 Color: 546
Size: 2447 Color: 288

Bin 132: 12 of cap free
Amount of items: 2
Items: 
Size: 14386 Color: 552
Size: 2354 Color: 284

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 13421 Color: 499
Size: 3318 Color: 326

Bin 134: 14 of cap free
Amount of items: 3
Items: 
Size: 10632 Color: 438
Size: 5810 Color: 379
Size: 296 Color: 25

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 12516 Color: 468
Size: 4222 Color: 353

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 13562 Color: 507
Size: 3176 Color: 319

Bin 137: 14 of cap free
Amount of items: 2
Items: 
Size: 13640 Color: 509
Size: 3098 Color: 316

Bin 138: 14 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 534
Size: 2666 Color: 301

Bin 139: 15 of cap free
Amount of items: 11
Items: 
Size: 8377 Color: 405
Size: 1048 Color: 175
Size: 1040 Color: 174
Size: 1032 Color: 173
Size: 1032 Color: 172
Size: 1024 Color: 171
Size: 1024 Color: 170
Size: 896 Color: 160
Size: 424 Color: 73
Size: 420 Color: 72
Size: 420 Color: 71

Bin 140: 15 of cap free
Amount of items: 2
Items: 
Size: 14588 Color: 569
Size: 2149 Color: 267

Bin 141: 16 of cap free
Amount of items: 3
Items: 
Size: 12044 Color: 459
Size: 4520 Color: 357
Size: 172 Color: 4

Bin 142: 16 of cap free
Amount of items: 2
Items: 
Size: 13157 Color: 488
Size: 3579 Color: 337

Bin 143: 17 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 564
Size: 2203 Color: 269

Bin 144: 19 of cap free
Amount of items: 2
Items: 
Size: 13560 Color: 506
Size: 3173 Color: 318

Bin 145: 19 of cap free
Amount of items: 2
Items: 
Size: 14331 Color: 549
Size: 2402 Color: 287

Bin 146: 20 of cap free
Amount of items: 3
Items: 
Size: 12040 Color: 458
Size: 4516 Color: 356
Size: 176 Color: 5

Bin 147: 20 of cap free
Amount of items: 2
Items: 
Size: 14260 Color: 544
Size: 2472 Color: 289

Bin 148: 21 of cap free
Amount of items: 2
Items: 
Size: 12117 Color: 461
Size: 4614 Color: 359

Bin 149: 22 of cap free
Amount of items: 2
Items: 
Size: 13234 Color: 491
Size: 3496 Color: 335

Bin 150: 24 of cap free
Amount of items: 5
Items: 
Size: 8456 Color: 414
Size: 6972 Color: 395
Size: 568 Color: 116
Size: 368 Color: 50
Size: 364 Color: 49

Bin 151: 24 of cap free
Amount of items: 2
Items: 
Size: 10968 Color: 440
Size: 5760 Color: 378

Bin 152: 26 of cap free
Amount of items: 3
Items: 
Size: 10378 Color: 431
Size: 6036 Color: 384
Size: 312 Color: 30

Bin 153: 27 of cap free
Amount of items: 3
Items: 
Size: 12115 Color: 460
Size: 2858 Color: 309
Size: 1752 Color: 230

Bin 154: 28 of cap free
Amount of items: 3
Items: 
Size: 11304 Color: 446
Size: 5160 Color: 368
Size: 260 Color: 14

Bin 155: 29 of cap free
Amount of items: 2
Items: 
Size: 10556 Color: 436
Size: 6167 Color: 388

Bin 156: 30 of cap free
Amount of items: 3
Items: 
Size: 10330 Color: 430
Size: 6072 Color: 385
Size: 320 Color: 31

Bin 157: 30 of cap free
Amount of items: 2
Items: 
Size: 12798 Color: 480
Size: 3924 Color: 346

Bin 158: 31 of cap free
Amount of items: 3
Items: 
Size: 11083 Color: 444
Size: 5354 Color: 375
Size: 284 Color: 18

Bin 159: 31 of cap free
Amount of items: 2
Items: 
Size: 13554 Color: 505
Size: 3167 Color: 317

Bin 160: 34 of cap free
Amount of items: 2
Items: 
Size: 11980 Color: 457
Size: 4738 Color: 362

Bin 161: 34 of cap free
Amount of items: 2
Items: 
Size: 13762 Color: 514
Size: 2956 Color: 314

Bin 162: 37 of cap free
Amount of items: 2
Items: 
Size: 13224 Color: 490
Size: 3491 Color: 334

Bin 163: 40 of cap free
Amount of items: 3
Items: 
Size: 10136 Color: 428
Size: 6256 Color: 389
Size: 320 Color: 33

Bin 164: 40 of cap free
Amount of items: 3
Items: 
Size: 11075 Color: 443
Size: 5353 Color: 374
Size: 284 Color: 19

Bin 165: 40 of cap free
Amount of items: 2
Items: 
Size: 13326 Color: 495
Size: 3386 Color: 329

Bin 166: 47 of cap free
Amount of items: 3
Items: 
Size: 11750 Color: 456
Size: 4731 Color: 361
Size: 224 Color: 6

Bin 167: 48 of cap free
Amount of items: 2
Items: 
Size: 12839 Color: 482
Size: 3865 Color: 345

Bin 168: 49 of cap free
Amount of items: 2
Items: 
Size: 9559 Color: 424
Size: 7144 Color: 402

Bin 169: 49 of cap free
Amount of items: 4
Items: 
Size: 10971 Color: 441
Size: 5156 Color: 367
Size: 288 Color: 22
Size: 288 Color: 21

Bin 170: 49 of cap free
Amount of items: 2
Items: 
Size: 12953 Color: 486
Size: 3750 Color: 343

Bin 171: 56 of cap free
Amount of items: 3
Items: 
Size: 11218 Color: 445
Size: 5206 Color: 371
Size: 272 Color: 16

Bin 172: 60 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 439
Size: 5528 Color: 376
Size: 292 Color: 24

Bin 173: 64 of cap free
Amount of items: 3
Items: 
Size: 9880 Color: 427
Size: 6488 Color: 390
Size: 320 Color: 34

Bin 174: 64 of cap free
Amount of items: 2
Items: 
Size: 12280 Color: 463
Size: 4408 Color: 354

Bin 175: 66 of cap free
Amount of items: 2
Items: 
Size: 11705 Color: 453
Size: 4981 Color: 366

Bin 176: 69 of cap free
Amount of items: 3
Items: 
Size: 11734 Color: 455
Size: 4725 Color: 360
Size: 224 Color: 7

Bin 177: 75 of cap free
Amount of items: 3
Items: 
Size: 10329 Color: 429
Size: 6028 Color: 383
Size: 320 Color: 32

Bin 178: 77 of cap free
Amount of items: 3
Items: 
Size: 9353 Color: 418
Size: 6978 Color: 398
Size: 344 Color: 41

Bin 179: 80 of cap free
Amount of items: 2
Items: 
Size: 11428 Color: 448
Size: 5244 Color: 372

Bin 180: 86 of cap free
Amount of items: 9
Items: 
Size: 8378 Color: 406
Size: 1392 Color: 196
Size: 1312 Color: 193
Size: 1216 Color: 189
Size: 1200 Color: 188
Size: 1200 Color: 186
Size: 1136 Color: 180
Size: 416 Color: 70
Size: 416 Color: 69

Bin 181: 94 of cap free
Amount of items: 3
Items: 
Size: 11599 Color: 450
Size: 4819 Color: 363
Size: 240 Color: 11

Bin 182: 96 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 417
Size: 7328 Color: 403
Size: 344 Color: 42

Bin 183: 107 of cap free
Amount of items: 2
Items: 
Size: 8389 Color: 412
Size: 8256 Color: 404

Bin 184: 115 of cap free
Amount of items: 3
Items: 
Size: 12318 Color: 465
Size: 4207 Color: 351
Size: 112 Color: 3

Bin 185: 117 of cap free
Amount of items: 5
Items: 
Size: 8385 Color: 410
Size: 2120 Color: 262
Size: 2106 Color: 261
Size: 2102 Color: 260
Size: 1922 Color: 245

Bin 186: 134 of cap free
Amount of items: 2
Items: 
Size: 10524 Color: 435
Size: 6094 Color: 387

Bin 187: 145 of cap free
Amount of items: 2
Items: 
Size: 11703 Color: 452
Size: 4904 Color: 365

Bin 188: 148 of cap free
Amount of items: 3
Items: 
Size: 10572 Color: 437
Size: 5736 Color: 377
Size: 296 Color: 27

Bin 189: 156 of cap free
Amount of items: 2
Items: 
Size: 10506 Color: 434
Size: 6090 Color: 386

Bin 190: 198 of cap free
Amount of items: 3
Items: 
Size: 11070 Color: 442
Size: 5196 Color: 370
Size: 288 Color: 20

Bin 191: 200 of cap free
Amount of items: 3
Items: 
Size: 9576 Color: 426
Size: 6640 Color: 391
Size: 336 Color: 36

Bin 192: 254 of cap free
Amount of items: 2
Items: 
Size: 9516 Color: 422
Size: 6982 Color: 401

Bin 193: 263 of cap free
Amount of items: 4
Items: 
Size: 8800 Color: 416
Size: 6977 Color: 397
Size: 360 Color: 45
Size: 352 Color: 44

Bin 194: 291 of cap free
Amount of items: 2
Items: 
Size: 9480 Color: 421
Size: 6981 Color: 400

Bin 195: 309 of cap free
Amount of items: 7
Items: 
Size: 8381 Color: 408
Size: 1670 Color: 222
Size: 1580 Color: 215
Size: 1512 Color: 213
Size: 1452 Color: 206
Size: 1448 Color: 205
Size: 400 Color: 64

Bin 196: 326 of cap free
Amount of items: 2
Items: 
Size: 9446 Color: 420
Size: 6980 Color: 399

Bin 197: 378 of cap free
Amount of items: 21
Items: 
Size: 976 Color: 169
Size: 962 Color: 167
Size: 960 Color: 166
Size: 960 Color: 165
Size: 946 Color: 164
Size: 944 Color: 163
Size: 920 Color: 161
Size: 896 Color: 159
Size: 880 Color: 158
Size: 864 Color: 157
Size: 844 Color: 156
Size: 840 Color: 154
Size: 832 Color: 152
Size: 792 Color: 151
Size: 784 Color: 150
Size: 784 Color: 149
Size: 448 Color: 80
Size: 448 Color: 79
Size: 432 Color: 78
Size: 432 Color: 77
Size: 430 Color: 76

Bin 198: 424 of cap free
Amount of items: 27
Items: 
Size: 772 Color: 148
Size: 772 Color: 147
Size: 740 Color: 145
Size: 736 Color: 144
Size: 736 Color: 143
Size: 714 Color: 141
Size: 704 Color: 140
Size: 704 Color: 139
Size: 698 Color: 138
Size: 696 Color: 137
Size: 696 Color: 136
Size: 672 Color: 133
Size: 668 Color: 132
Size: 660 Color: 129
Size: 656 Color: 128
Size: 512 Color: 100
Size: 500 Color: 97
Size: 496 Color: 96
Size: 496 Color: 95
Size: 480 Color: 92
Size: 466 Color: 89
Size: 464 Color: 88
Size: 464 Color: 87
Size: 462 Color: 86
Size: 456 Color: 84
Size: 456 Color: 83
Size: 452 Color: 81

Bin 199: 10900 of cap free
Amount of items: 10
Items: 
Size: 652 Color: 127
Size: 640 Color: 126
Size: 634 Color: 125
Size: 616 Color: 122
Size: 568 Color: 115
Size: 568 Color: 114
Size: 560 Color: 111
Size: 554 Color: 110
Size: 532 Color: 106
Size: 528 Color: 104

Total size: 3316896
Total free space: 16752

