Capicity Bin: 19648
Lower Bound: 198

Bins used: 199
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 10402 Color: 3
Size: 7706 Color: 3
Size: 1540 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 12448 Color: 3
Size: 6624 Color: 2
Size: 576 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 12592 Color: 3
Size: 6720 Color: 1
Size: 336 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 13552 Color: 1
Size: 5872 Color: 3
Size: 224 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13728 Color: 4
Size: 5472 Color: 2
Size: 448 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13814 Color: 0
Size: 5334 Color: 1
Size: 500 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 13816 Color: 3
Size: 4424 Color: 2
Size: 1408 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 13892 Color: 0
Size: 5388 Color: 3
Size: 368 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 14052 Color: 1
Size: 4064 Color: 0
Size: 1532 Color: 2

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 14064 Color: 2
Size: 5168 Color: 0
Size: 416 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 14112 Color: 4
Size: 4204 Color: 2
Size: 1332 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 14313 Color: 3
Size: 4447 Color: 4
Size: 888 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 14576 Color: 2
Size: 4656 Color: 0
Size: 416 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 2
Size: 3996 Color: 3
Size: 938 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 14816 Color: 2
Size: 3484 Color: 3
Size: 1348 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 14810 Color: 3
Size: 3506 Color: 1
Size: 1332 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 14866 Color: 3
Size: 4154 Color: 2
Size: 628 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 14892 Color: 4
Size: 2904 Color: 2
Size: 1852 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15005 Color: 4
Size: 3871 Color: 2
Size: 772 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 15112 Color: 2
Size: 3924 Color: 3
Size: 612 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 15136 Color: 1
Size: 4136 Color: 2
Size: 376 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 3
Size: 4096 Color: 2
Size: 288 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 15336 Color: 2
Size: 2176 Color: 4
Size: 2136 Color: 3

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 15392 Color: 4
Size: 3792 Color: 2
Size: 464 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 15420 Color: 1
Size: 3524 Color: 2
Size: 704 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 15476 Color: 2
Size: 2848 Color: 1
Size: 1324 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 15520 Color: 4
Size: 3808 Color: 2
Size: 320 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 15696 Color: 3
Size: 3536 Color: 4
Size: 416 Color: 2

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 4
Size: 3196 Color: 2
Size: 632 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 15964 Color: 0
Size: 3076 Color: 2
Size: 608 Color: 3

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 16072 Color: 2
Size: 2984 Color: 3
Size: 592 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 16224 Color: 2
Size: 2736 Color: 4
Size: 688 Color: 4

Bin 33: 0 of cap free
Amount of items: 4
Items: 
Size: 16168 Color: 0
Size: 3208 Color: 3
Size: 192 Color: 3
Size: 80 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 16240 Color: 2
Size: 2912 Color: 4
Size: 496 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 16268 Color: 3
Size: 3120 Color: 0
Size: 260 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 16368 Color: 3
Size: 3024 Color: 0
Size: 256 Color: 2

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 16484 Color: 4
Size: 1636 Color: 1
Size: 1528 Color: 2

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 16500 Color: 1
Size: 1600 Color: 0
Size: 1548 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 16642 Color: 2
Size: 2568 Color: 1
Size: 438 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 16680 Color: 2
Size: 1636 Color: 1
Size: 1332 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 16720 Color: 2
Size: 1904 Color: 0
Size: 1024 Color: 4

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 16730 Color: 3
Size: 2406 Color: 2
Size: 512 Color: 4

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 16920 Color: 3
Size: 2192 Color: 2
Size: 536 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 16956 Color: 4
Size: 1902 Color: 2
Size: 790 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 17008 Color: 0
Size: 2224 Color: 2
Size: 416 Color: 4

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 17056 Color: 0
Size: 2072 Color: 2
Size: 520 Color: 4

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 17076 Color: 2
Size: 1324 Color: 0
Size: 1248 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 17096 Color: 4
Size: 2240 Color: 2
Size: 312 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 17136 Color: 0
Size: 1744 Color: 1
Size: 768 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 17264 Color: 4
Size: 1600 Color: 2
Size: 784 Color: 3

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 17306 Color: 1
Size: 1540 Color: 4
Size: 802 Color: 2

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 17340 Color: 1
Size: 1344 Color: 2
Size: 964 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 17352 Color: 2
Size: 2208 Color: 0
Size: 88 Color: 4

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 17380 Color: 0
Size: 1356 Color: 2
Size: 912 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 17428 Color: 1
Size: 1856 Color: 2
Size: 364 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 17446 Color: 2
Size: 1426 Color: 3
Size: 776 Color: 1

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 17504 Color: 1
Size: 1688 Color: 2
Size: 456 Color: 4

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 17536 Color: 1
Size: 1768 Color: 1
Size: 344 Color: 2

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 17544 Color: 1
Size: 1168 Color: 3
Size: 936 Color: 2

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 17632 Color: 4
Size: 1696 Color: 2
Size: 320 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 17636 Color: 2
Size: 1636 Color: 3
Size: 376 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 17670 Color: 0
Size: 1650 Color: 2
Size: 328 Color: 0

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 13250 Color: 3
Size: 5849 Color: 1
Size: 548 Color: 0

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 13865 Color: 0
Size: 4822 Color: 2
Size: 960 Color: 4

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 14666 Color: 2
Size: 4597 Color: 1
Size: 384 Color: 3

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 14751 Color: 0
Size: 4544 Color: 2
Size: 352 Color: 1

Bin 67: 2 of cap free
Amount of items: 9
Items: 
Size: 9828 Color: 4
Size: 1730 Color: 2
Size: 1424 Color: 0
Size: 1392 Color: 1
Size: 1376 Color: 3
Size: 1376 Color: 0
Size: 1360 Color: 1
Size: 640 Color: 4
Size: 520 Color: 4

Bin 68: 2 of cap free
Amount of items: 5
Items: 
Size: 9848 Color: 4
Size: 4640 Color: 1
Size: 3914 Color: 0
Size: 820 Color: 4
Size: 424 Color: 2

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 10306 Color: 3
Size: 8160 Color: 1
Size: 1180 Color: 0

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 10354 Color: 1
Size: 8184 Color: 1
Size: 1108 Color: 0

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11408 Color: 0
Size: 6706 Color: 1
Size: 1532 Color: 2

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 11506 Color: 2
Size: 7660 Color: 0
Size: 480 Color: 2

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 11682 Color: 1
Size: 7132 Color: 4
Size: 832 Color: 0

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 11714 Color: 3
Size: 6896 Color: 0
Size: 1036 Color: 4

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 14013 Color: 4
Size: 5185 Color: 4
Size: 448 Color: 2

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 14166 Color: 1
Size: 4140 Color: 2
Size: 1340 Color: 1

Bin 77: 2 of cap free
Amount of items: 3
Items: 
Size: 14684 Color: 1
Size: 4034 Color: 4
Size: 928 Color: 2

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 14829 Color: 2
Size: 4081 Color: 0
Size: 736 Color: 1

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 14940 Color: 0
Size: 4114 Color: 3
Size: 592 Color: 2

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 1
Size: 4142 Color: 2
Size: 384 Color: 1

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 15442 Color: 1
Size: 3488 Color: 0
Size: 716 Color: 2

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 15874 Color: 3
Size: 2820 Color: 1
Size: 952 Color: 2

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 16430 Color: 2
Size: 2298 Color: 4
Size: 918 Color: 1

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 16894 Color: 2
Size: 1600 Color: 1
Size: 1152 Color: 4

Bin 85: 2 of cap free
Amount of items: 3
Items: 
Size: 16930 Color: 1
Size: 1632 Color: 1
Size: 1084 Color: 2

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 17574 Color: 4
Size: 1684 Color: 2
Size: 388 Color: 3

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 0
Size: 8169 Color: 3
Size: 384 Color: 3

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 12259 Color: 3
Size: 6682 Color: 4
Size: 704 Color: 0

Bin 89: 4 of cap free
Amount of items: 3
Items: 
Size: 11100 Color: 2
Size: 6746 Color: 3
Size: 1798 Color: 0

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 12728 Color: 1
Size: 6300 Color: 0
Size: 616 Color: 4

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 13092 Color: 4
Size: 5848 Color: 3
Size: 704 Color: 0

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 13122 Color: 4
Size: 5978 Color: 1
Size: 544 Color: 0

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 4
Size: 5524 Color: 2
Size: 480 Color: 0

Bin 94: 4 of cap free
Amount of items: 3
Items: 
Size: 14664 Color: 2
Size: 4724 Color: 3
Size: 256 Color: 1

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 15730 Color: 2
Size: 3586 Color: 1
Size: 328 Color: 0

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 15920 Color: 1
Size: 3084 Color: 3
Size: 640 Color: 2

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 17000 Color: 0
Size: 2644 Color: 3

Bin 98: 5 of cap free
Amount of items: 11
Items: 
Size: 9825 Color: 2
Size: 1152 Color: 1
Size: 1096 Color: 0
Size: 1072 Color: 1
Size: 1024 Color: 3
Size: 976 Color: 4
Size: 960 Color: 2
Size: 954 Color: 4
Size: 936 Color: 0
Size: 896 Color: 2
Size: 752 Color: 0

Bin 99: 5 of cap free
Amount of items: 3
Items: 
Size: 12464 Color: 0
Size: 6667 Color: 1
Size: 512 Color: 2

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 14590 Color: 0
Size: 4697 Color: 2
Size: 356 Color: 0

Bin 101: 6 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 0
Size: 8144 Color: 3
Size: 1064 Color: 2

Bin 102: 6 of cap free
Amount of items: 3
Items: 
Size: 11280 Color: 2
Size: 7746 Color: 3
Size: 616 Color: 0

Bin 103: 6 of cap free
Amount of items: 3
Items: 
Size: 11448 Color: 0
Size: 7682 Color: 4
Size: 512 Color: 4

Bin 104: 6 of cap free
Amount of items: 2
Items: 
Size: 15474 Color: 4
Size: 4168 Color: 3

Bin 105: 6 of cap free
Amount of items: 2
Items: 
Size: 16496 Color: 1
Size: 3146 Color: 3

Bin 106: 6 of cap free
Amount of items: 2
Items: 
Size: 17124 Color: 4
Size: 2518 Color: 3

Bin 107: 7 of cap free
Amount of items: 3
Items: 
Size: 10912 Color: 3
Size: 8185 Color: 1
Size: 544 Color: 1

Bin 108: 7 of cap free
Amount of items: 3
Items: 
Size: 11041 Color: 2
Size: 8176 Color: 0
Size: 424 Color: 4

Bin 109: 8 of cap free
Amount of items: 3
Items: 
Size: 10466 Color: 1
Size: 7328 Color: 0
Size: 1846 Color: 2

Bin 110: 8 of cap free
Amount of items: 3
Items: 
Size: 11602 Color: 0
Size: 7654 Color: 1
Size: 384 Color: 2

Bin 111: 8 of cap free
Amount of items: 3
Items: 
Size: 11650 Color: 2
Size: 6760 Color: 0
Size: 1230 Color: 4

Bin 112: 8 of cap free
Amount of items: 3
Items: 
Size: 12288 Color: 3
Size: 6968 Color: 3
Size: 384 Color: 2

Bin 113: 8 of cap free
Amount of items: 3
Items: 
Size: 12546 Color: 4
Size: 6614 Color: 2
Size: 480 Color: 4

Bin 114: 8 of cap free
Amount of items: 2
Items: 
Size: 15018 Color: 4
Size: 4622 Color: 0

Bin 115: 8 of cap free
Amount of items: 2
Items: 
Size: 15440 Color: 4
Size: 4200 Color: 0

Bin 116: 8 of cap free
Amount of items: 2
Items: 
Size: 17392 Color: 3
Size: 2248 Color: 4

Bin 117: 9 of cap free
Amount of items: 2
Items: 
Size: 15682 Color: 1
Size: 3957 Color: 0

Bin 118: 10 of cap free
Amount of items: 3
Items: 
Size: 13917 Color: 2
Size: 4017 Color: 1
Size: 1704 Color: 0

Bin 119: 10 of cap free
Amount of items: 2
Items: 
Size: 17190 Color: 1
Size: 2448 Color: 4

Bin 120: 11 of cap free
Amount of items: 7
Items: 
Size: 9827 Color: 2
Size: 1892 Color: 4
Size: 1838 Color: 2
Size: 1824 Color: 2
Size: 1632 Color: 4
Size: 1632 Color: 0
Size: 992 Color: 3

Bin 121: 12 of cap free
Amount of items: 2
Items: 
Size: 15948 Color: 4
Size: 3688 Color: 3

Bin 122: 12 of cap free
Amount of items: 4
Items: 
Size: 16864 Color: 4
Size: 2628 Color: 3
Size: 96 Color: 2
Size: 48 Color: 3

Bin 123: 12 of cap free
Amount of items: 2
Items: 
Size: 17494 Color: 3
Size: 2142 Color: 0

Bin 124: 13 of cap free
Amount of items: 3
Items: 
Size: 11649 Color: 3
Size: 7666 Color: 4
Size: 320 Color: 0

Bin 125: 14 of cap free
Amount of items: 3
Items: 
Size: 13058 Color: 3
Size: 3964 Color: 1
Size: 2612 Color: 0

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 15394 Color: 0
Size: 4240 Color: 4

Bin 127: 14 of cap free
Amount of items: 2
Items: 
Size: 16082 Color: 3
Size: 3552 Color: 1

Bin 128: 14 of cap free
Amount of items: 2
Items: 
Size: 17584 Color: 3
Size: 2050 Color: 2

Bin 129: 15 of cap free
Amount of items: 3
Items: 
Size: 12631 Color: 4
Size: 6666 Color: 2
Size: 336 Color: 2

Bin 130: 16 of cap free
Amount of items: 3
Items: 
Size: 13028 Color: 2
Size: 6084 Color: 3
Size: 520 Color: 4

Bin 131: 16 of cap free
Amount of items: 3
Items: 
Size: 13188 Color: 4
Size: 6252 Color: 3
Size: 192 Color: 4

Bin 132: 16 of cap free
Amount of items: 2
Items: 
Size: 14616 Color: 0
Size: 5016 Color: 4

Bin 133: 16 of cap free
Amount of items: 2
Items: 
Size: 16516 Color: 0
Size: 3116 Color: 1

Bin 134: 16 of cap free
Amount of items: 2
Items: 
Size: 16584 Color: 1
Size: 3048 Color: 4

Bin 135: 16 of cap free
Amount of items: 3
Items: 
Size: 17640 Color: 4
Size: 1928 Color: 1
Size: 64 Color: 4

Bin 136: 18 of cap free
Amount of items: 2
Items: 
Size: 13862 Color: 4
Size: 5768 Color: 1

Bin 137: 18 of cap free
Amount of items: 2
Items: 
Size: 17534 Color: 0
Size: 2096 Color: 1

Bin 138: 20 of cap free
Amount of items: 6
Items: 
Size: 9834 Color: 1
Size: 2400 Color: 0
Size: 2148 Color: 3
Size: 2108 Color: 0
Size: 2042 Color: 4
Size: 1096 Color: 1

Bin 139: 20 of cap free
Amount of items: 2
Items: 
Size: 12986 Color: 0
Size: 6642 Color: 4

Bin 140: 20 of cap free
Amount of items: 2
Items: 
Size: 14133 Color: 4
Size: 5495 Color: 3

Bin 141: 22 of cap free
Amount of items: 4
Items: 
Size: 9904 Color: 3
Size: 5284 Color: 0
Size: 3878 Color: 1
Size: 560 Color: 2

Bin 142: 22 of cap free
Amount of items: 3
Items: 
Size: 12148 Color: 1
Size: 5922 Color: 0
Size: 1556 Color: 2

Bin 143: 22 of cap free
Amount of items: 3
Items: 
Size: 17558 Color: 4
Size: 1924 Color: 3
Size: 144 Color: 1

Bin 144: 22 of cap free
Amount of items: 3
Items: 
Size: 17608 Color: 1
Size: 1954 Color: 4
Size: 64 Color: 3

Bin 145: 23 of cap free
Amount of items: 2
Items: 
Size: 14901 Color: 3
Size: 4724 Color: 4

Bin 146: 24 of cap free
Amount of items: 2
Items: 
Size: 13720 Color: 3
Size: 5904 Color: 4

Bin 147: 26 of cap free
Amount of items: 2
Items: 
Size: 17082 Color: 4
Size: 2540 Color: 1

Bin 148: 28 of cap free
Amount of items: 2
Items: 
Size: 16548 Color: 3
Size: 3072 Color: 0

Bin 149: 28 of cap free
Amount of items: 2
Items: 
Size: 16756 Color: 1
Size: 2864 Color: 0

Bin 150: 30 of cap free
Amount of items: 2
Items: 
Size: 15048 Color: 1
Size: 4570 Color: 3

Bin 151: 32 of cap free
Amount of items: 2
Items: 
Size: 13568 Color: 4
Size: 6048 Color: 3

Bin 152: 32 of cap free
Amount of items: 2
Items: 
Size: 16008 Color: 4
Size: 3608 Color: 3

Bin 153: 32 of cap free
Amount of items: 2
Items: 
Size: 17028 Color: 0
Size: 2588 Color: 4

Bin 154: 32 of cap free
Amount of items: 2
Items: 
Size: 17336 Color: 4
Size: 2280 Color: 3

Bin 155: 34 of cap free
Amount of items: 2
Items: 
Size: 17370 Color: 3
Size: 2244 Color: 4

Bin 156: 36 of cap free
Amount of items: 5
Items: 
Size: 9836 Color: 4
Size: 2974 Color: 3
Size: 2758 Color: 2
Size: 2412 Color: 1
Size: 1632 Color: 0

Bin 157: 36 of cap free
Amount of items: 3
Items: 
Size: 15240 Color: 3
Size: 4104 Color: 0
Size: 268 Color: 1

Bin 158: 38 of cap free
Amount of items: 2
Items: 
Size: 16928 Color: 3
Size: 2682 Color: 4

Bin 159: 40 of cap free
Amount of items: 2
Items: 
Size: 16342 Color: 4
Size: 3266 Color: 1

Bin 160: 44 of cap free
Amount of items: 3
Items: 
Size: 12690 Color: 2
Size: 6786 Color: 4
Size: 128 Color: 4

Bin 161: 48 of cap free
Amount of items: 2
Items: 
Size: 16288 Color: 3
Size: 3312 Color: 4

Bin 162: 52 of cap free
Amount of items: 3
Items: 
Size: 11554 Color: 3
Size: 7722 Color: 2
Size: 320 Color: 1

Bin 163: 52 of cap free
Amount of items: 2
Items: 
Size: 15916 Color: 1
Size: 3680 Color: 3

Bin 164: 55 of cap free
Amount of items: 3
Items: 
Size: 13069 Color: 3
Size: 4960 Color: 2
Size: 1564 Color: 2

Bin 165: 58 of cap free
Amount of items: 3
Items: 
Size: 10258 Color: 4
Size: 8180 Color: 3
Size: 1152 Color: 2

Bin 166: 58 of cap free
Amount of items: 3
Items: 
Size: 13456 Color: 1
Size: 3862 Color: 2
Size: 2272 Color: 2

Bin 167: 58 of cap free
Amount of items: 2
Items: 
Size: 14728 Color: 0
Size: 4862 Color: 4

Bin 168: 62 of cap free
Amount of items: 2
Items: 
Size: 12594 Color: 4
Size: 6992 Color: 0

Bin 169: 62 of cap free
Amount of items: 2
Items: 
Size: 13427 Color: 0
Size: 6159 Color: 2

Bin 170: 63 of cap free
Amount of items: 2
Items: 
Size: 14102 Color: 3
Size: 5483 Color: 1

Bin 171: 64 of cap free
Amount of items: 2
Items: 
Size: 11544 Color: 2
Size: 8040 Color: 4

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 14780 Color: 4
Size: 4804 Color: 1

Bin 173: 64 of cap free
Amount of items: 2
Items: 
Size: 15800 Color: 4
Size: 3784 Color: 3

Bin 174: 74 of cap free
Amount of items: 2
Items: 
Size: 14020 Color: 1
Size: 5554 Color: 4

Bin 175: 80 of cap free
Amount of items: 3
Items: 
Size: 12348 Color: 4
Size: 6996 Color: 3
Size: 224 Color: 0

Bin 176: 80 of cap free
Amount of items: 2
Items: 
Size: 14876 Color: 3
Size: 4692 Color: 0

Bin 177: 84 of cap free
Amount of items: 2
Items: 
Size: 14612 Color: 0
Size: 4952 Color: 1

Bin 178: 84 of cap free
Amount of items: 2
Items: 
Size: 15346 Color: 4
Size: 4218 Color: 1

Bin 179: 88 of cap free
Amount of items: 2
Items: 
Size: 9888 Color: 2
Size: 9672 Color: 4

Bin 180: 90 of cap free
Amount of items: 2
Items: 
Size: 13756 Color: 4
Size: 5802 Color: 2

Bin 181: 90 of cap free
Amount of items: 3
Items: 
Size: 13988 Color: 1
Size: 5442 Color: 3
Size: 128 Color: 2

Bin 182: 94 of cap free
Amount of items: 3
Items: 
Size: 10008 Color: 3
Size: 6000 Color: 0
Size: 3546 Color: 2

Bin 183: 96 of cap free
Amount of items: 25
Items: 
Size: 928 Color: 0
Size: 924 Color: 2
Size: 912 Color: 3
Size: 888 Color: 2
Size: 840 Color: 4
Size: 832 Color: 3
Size: 832 Color: 1
Size: 828 Color: 1
Size: 824 Color: 2
Size: 816 Color: 2
Size: 816 Color: 1
Size: 816 Color: 1
Size: 804 Color: 0
Size: 792 Color: 3
Size: 768 Color: 0
Size: 720 Color: 0
Size: 708 Color: 2
Size: 704 Color: 3
Size: 704 Color: 3
Size: 704 Color: 2
Size: 704 Color: 2
Size: 700 Color: 3
Size: 676 Color: 1
Size: 672 Color: 4
Size: 640 Color: 0

Bin 184: 96 of cap free
Amount of items: 2
Items: 
Size: 11744 Color: 1
Size: 7808 Color: 4

Bin 185: 111 of cap free
Amount of items: 2
Items: 
Size: 14760 Color: 4
Size: 4777 Color: 1

Bin 186: 118 of cap free
Amount of items: 2
Items: 
Size: 13152 Color: 2
Size: 6378 Color: 0

Bin 187: 124 of cap free
Amount of items: 2
Items: 
Size: 11698 Color: 2
Size: 7826 Color: 3

Bin 188: 130 of cap free
Amount of items: 3
Items: 
Size: 10386 Color: 1
Size: 6626 Color: 2
Size: 2506 Color: 2

Bin 189: 156 of cap free
Amount of items: 2
Items: 
Size: 11304 Color: 2
Size: 8188 Color: 4

Bin 190: 160 of cap free
Amount of items: 2
Items: 
Size: 9880 Color: 2
Size: 9608 Color: 0

Bin 191: 160 of cap free
Amount of items: 2
Items: 
Size: 12648 Color: 0
Size: 6840 Color: 2

Bin 192: 179 of cap free
Amount of items: 3
Items: 
Size: 9840 Color: 0
Size: 7141 Color: 4
Size: 2488 Color: 2

Bin 193: 198 of cap free
Amount of items: 9
Items: 
Size: 9826 Color: 4
Size: 1344 Color: 4
Size: 1320 Color: 0
Size: 1280 Color: 4
Size: 1216 Color: 0
Size: 1184 Color: 3
Size: 1156 Color: 1
Size: 1152 Color: 2
Size: 972 Color: 2

Bin 194: 201 of cap free
Amount of items: 2
Items: 
Size: 11260 Color: 1
Size: 8187 Color: 4

Bin 195: 228 of cap free
Amount of items: 2
Items: 
Size: 11634 Color: 2
Size: 7786 Color: 3

Bin 196: 276 of cap free
Amount of items: 3
Items: 
Size: 9832 Color: 0
Size: 4872 Color: 2
Size: 4668 Color: 3

Bin 197: 296 of cap free
Amount of items: 2
Items: 
Size: 10450 Color: 3
Size: 8902 Color: 4

Bin 198: 381 of cap free
Amount of items: 2
Items: 
Size: 11081 Color: 4
Size: 8186 Color: 3

Bin 199: 14044 of cap free
Amount of items: 11
Items: 
Size: 652 Color: 4
Size: 640 Color: 4
Size: 608 Color: 3
Size: 608 Color: 0
Size: 592 Color: 0
Size: 464 Color: 1
Size: 448 Color: 0
Size: 408 Color: 1
Size: 400 Color: 4
Size: 400 Color: 2
Size: 384 Color: 1

Total size: 3890304
Total free space: 19648

