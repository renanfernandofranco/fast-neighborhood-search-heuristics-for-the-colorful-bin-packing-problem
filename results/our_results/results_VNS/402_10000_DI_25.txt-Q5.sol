Capicity Bin: 8120
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4070 Color: 3
Size: 2921 Color: 0
Size: 1129 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4540 Color: 4
Size: 3380 Color: 0
Size: 200 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5498 Color: 4
Size: 2420 Color: 3
Size: 202 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6090 Color: 2
Size: 1894 Color: 0
Size: 136 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6410 Color: 2
Size: 1426 Color: 0
Size: 284 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6504 Color: 0
Size: 1474 Color: 2
Size: 142 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6481 Color: 4
Size: 1367 Color: 1
Size: 272 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6642 Color: 0
Size: 1386 Color: 1
Size: 92 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6680 Color: 0
Size: 880 Color: 3
Size: 560 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6738 Color: 0
Size: 742 Color: 1
Size: 640 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6746 Color: 2
Size: 1218 Color: 3
Size: 156 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6762 Color: 4
Size: 1134 Color: 3
Size: 224 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6812 Color: 3
Size: 1124 Color: 4
Size: 184 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6873 Color: 2
Size: 1007 Color: 3
Size: 240 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6936 Color: 1
Size: 1022 Color: 3
Size: 162 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6987 Color: 4
Size: 765 Color: 2
Size: 368 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 3
Size: 676 Color: 0
Size: 400 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7053 Color: 3
Size: 793 Color: 2
Size: 274 Color: 2

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7049 Color: 4
Size: 893 Color: 3
Size: 178 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7064 Color: 2
Size: 576 Color: 1
Size: 480 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7068 Color: 1
Size: 672 Color: 3
Size: 380 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 1
Size: 882 Color: 3
Size: 148 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7134 Color: 3
Size: 782 Color: 4
Size: 204 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7145 Color: 1
Size: 691 Color: 1
Size: 284 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7164 Color: 2
Size: 520 Color: 4
Size: 436 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 3
Size: 802 Color: 4
Size: 144 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7182 Color: 1
Size: 506 Color: 2
Size: 432 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 1
Size: 692 Color: 3
Size: 224 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7190 Color: 3
Size: 682 Color: 2
Size: 248 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7225 Color: 2
Size: 731 Color: 3
Size: 164 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7244 Color: 4
Size: 796 Color: 1
Size: 80 Color: 3

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7243 Color: 3
Size: 725 Color: 4
Size: 152 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 0
Size: 724 Color: 1
Size: 144 Color: 3

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7274 Color: 0
Size: 706 Color: 3
Size: 140 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7292 Color: 1
Size: 676 Color: 2
Size: 152 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7306 Color: 1
Size: 676 Color: 2
Size: 138 Color: 3

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4613 Color: 2
Size: 3378 Color: 0
Size: 128 Color: 1

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5862 Color: 3
Size: 1951 Color: 0
Size: 306 Color: 2

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6465 Color: 0
Size: 1654 Color: 1

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6450 Color: 1
Size: 1525 Color: 3
Size: 144 Color: 4

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6484 Color: 2
Size: 922 Color: 3
Size: 713 Color: 0

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6623 Color: 3
Size: 1000 Color: 0
Size: 496 Color: 2

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6755 Color: 3
Size: 1364 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6881 Color: 3
Size: 1092 Color: 1
Size: 146 Color: 4

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6867 Color: 2
Size: 1156 Color: 3
Size: 96 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6882 Color: 2
Size: 1021 Color: 0
Size: 216 Color: 3

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6898 Color: 4
Size: 945 Color: 3
Size: 276 Color: 1

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 3
Size: 891 Color: 4
Size: 288 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 6975 Color: 3
Size: 992 Color: 2
Size: 152 Color: 4

Bin 50: 1 of cap free
Amount of items: 2
Items: 
Size: 7074 Color: 2
Size: 1045 Color: 0

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 7103 Color: 1
Size: 790 Color: 2
Size: 226 Color: 3

Bin 52: 2 of cap free
Amount of items: 5
Items: 
Size: 4062 Color: 0
Size: 1531 Color: 1
Size: 1249 Color: 2
Size: 764 Color: 1
Size: 512 Color: 4

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 4626 Color: 1
Size: 2988 Color: 0
Size: 504 Color: 2

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 4
Size: 3164 Color: 0
Size: 304 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 5988 Color: 2
Size: 1882 Color: 0
Size: 248 Color: 4

Bin 56: 2 of cap free
Amount of items: 3
Items: 
Size: 6283 Color: 3
Size: 1243 Color: 3
Size: 592 Color: 0

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6442 Color: 4
Size: 1676 Color: 2

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 6759 Color: 1
Size: 1131 Color: 3
Size: 228 Color: 0

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7305 Color: 4
Size: 813 Color: 1

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5787 Color: 3
Size: 2194 Color: 1
Size: 136 Color: 0

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 5908 Color: 3
Size: 2209 Color: 2

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 6265 Color: 1
Size: 1692 Color: 0
Size: 160 Color: 4

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 2
Size: 1547 Color: 0
Size: 168 Color: 3

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 6473 Color: 4
Size: 1564 Color: 3
Size: 80 Color: 1

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 7291 Color: 0
Size: 812 Color: 4
Size: 14 Color: 1

Bin 66: 4 of cap free
Amount of items: 28
Items: 
Size: 500 Color: 2
Size: 448 Color: 3
Size: 442 Color: 2
Size: 440 Color: 2
Size: 436 Color: 3
Size: 384 Color: 3
Size: 370 Color: 3
Size: 332 Color: 4
Size: 328 Color: 4
Size: 328 Color: 1
Size: 312 Color: 3
Size: 308 Color: 4
Size: 296 Color: 2
Size: 276 Color: 1
Size: 274 Color: 2
Size: 272 Color: 3
Size: 226 Color: 4
Size: 226 Color: 2
Size: 224 Color: 4
Size: 216 Color: 1
Size: 204 Color: 0
Size: 200 Color: 0
Size: 192 Color: 0
Size: 192 Color: 0
Size: 190 Color: 0
Size: 188 Color: 4
Size: 184 Color: 0
Size: 128 Color: 1

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 5476 Color: 2
Size: 2364 Color: 0
Size: 276 Color: 3

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 4
Size: 1490 Color: 2
Size: 292 Color: 0

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 6458 Color: 3
Size: 1402 Color: 0
Size: 256 Color: 1

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6572 Color: 3
Size: 1544 Color: 4

Bin 71: 4 of cap free
Amount of items: 3
Items: 
Size: 6662 Color: 2
Size: 1270 Color: 1
Size: 184 Color: 0

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 7254 Color: 4
Size: 862 Color: 0

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 0
Size: 2533 Color: 3
Size: 440 Color: 4

Bin 74: 5 of cap free
Amount of items: 3
Items: 
Size: 5093 Color: 3
Size: 2844 Color: 0
Size: 178 Color: 3

Bin 75: 5 of cap free
Amount of items: 3
Items: 
Size: 5692 Color: 1
Size: 2215 Color: 0
Size: 208 Color: 3

Bin 76: 5 of cap free
Amount of items: 3
Items: 
Size: 6045 Color: 1
Size: 1918 Color: 0
Size: 152 Color: 3

Bin 77: 5 of cap free
Amount of items: 2
Items: 
Size: 6271 Color: 3
Size: 1844 Color: 2

Bin 78: 5 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 2
Size: 901 Color: 4

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 0
Size: 2174 Color: 1
Size: 136 Color: 3

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6994 Color: 4
Size: 1120 Color: 1

Bin 81: 6 of cap free
Amount of items: 2
Items: 
Size: 7265 Color: 3
Size: 849 Color: 1

Bin 82: 7 of cap free
Amount of items: 3
Items: 
Size: 5463 Color: 2
Size: 2518 Color: 4
Size: 132 Color: 3

Bin 83: 7 of cap free
Amount of items: 3
Items: 
Size: 6244 Color: 4
Size: 1713 Color: 0
Size: 156 Color: 4

Bin 84: 7 of cap free
Amount of items: 3
Items: 
Size: 6356 Color: 2
Size: 1381 Color: 1
Size: 376 Color: 0

Bin 85: 8 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 4
Size: 3008 Color: 0
Size: 388 Color: 2

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 7148 Color: 2
Size: 964 Color: 4

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 6972 Color: 0
Size: 1139 Color: 1

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 4988 Color: 3
Size: 2894 Color: 2
Size: 228 Color: 2

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 2482 Color: 4
Size: 344 Color: 2

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 2
Size: 3568 Color: 0
Size: 472 Color: 4

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 7066 Color: 2
Size: 1042 Color: 0

Bin 92: 14 of cap free
Amount of items: 2
Items: 
Size: 6740 Color: 1
Size: 1366 Color: 3

Bin 93: 14 of cap free
Amount of items: 2
Items: 
Size: 6874 Color: 4
Size: 1232 Color: 2

Bin 94: 14 of cap free
Amount of items: 2
Items: 
Size: 7105 Color: 4
Size: 1001 Color: 1

Bin 95: 15 of cap free
Amount of items: 2
Items: 
Size: 6629 Color: 2
Size: 1476 Color: 3

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 7230 Color: 1
Size: 874 Color: 2

Bin 97: 17 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 4
Size: 2677 Color: 2
Size: 206 Color: 3

Bin 98: 17 of cap free
Amount of items: 2
Items: 
Size: 7203 Color: 1
Size: 900 Color: 2

Bin 99: 18 of cap free
Amount of items: 2
Items: 
Size: 5490 Color: 4
Size: 2612 Color: 2

Bin 100: 18 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 1
Size: 1434 Color: 4

Bin 101: 24 of cap free
Amount of items: 2
Items: 
Size: 6780 Color: 4
Size: 1316 Color: 2

Bin 102: 24 of cap free
Amount of items: 2
Items: 
Size: 7141 Color: 2
Size: 955 Color: 0

Bin 103: 25 of cap free
Amount of items: 3
Items: 
Size: 7251 Color: 2
Size: 804 Color: 0
Size: 40 Color: 4

Bin 104: 27 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 3
Size: 1731 Color: 1

Bin 105: 30 of cap free
Amount of items: 3
Items: 
Size: 5781 Color: 0
Size: 1725 Color: 4
Size: 584 Color: 2

Bin 106: 31 of cap free
Amount of items: 3
Items: 
Size: 4324 Color: 4
Size: 3381 Color: 3
Size: 384 Color: 4

Bin 107: 32 of cap free
Amount of items: 3
Items: 
Size: 5514 Color: 0
Size: 2186 Color: 0
Size: 388 Color: 3

Bin 108: 32 of cap free
Amount of items: 2
Items: 
Size: 6482 Color: 3
Size: 1606 Color: 2

Bin 109: 37 of cap free
Amount of items: 2
Items: 
Size: 6138 Color: 4
Size: 1945 Color: 3

Bin 110: 41 of cap free
Amount of items: 2
Items: 
Size: 4063 Color: 3
Size: 4016 Color: 1

Bin 111: 41 of cap free
Amount of items: 2
Items: 
Size: 6051 Color: 2
Size: 2028 Color: 1

Bin 112: 41 of cap free
Amount of items: 2
Items: 
Size: 6767 Color: 4
Size: 1312 Color: 2

Bin 113: 42 of cap free
Amount of items: 3
Items: 
Size: 6332 Color: 1
Size: 1394 Color: 2
Size: 352 Color: 0

Bin 114: 45 of cap free
Amount of items: 2
Items: 
Size: 7041 Color: 1
Size: 1034 Color: 0

Bin 115: 47 of cap free
Amount of items: 3
Items: 
Size: 6116 Color: 4
Size: 1373 Color: 1
Size: 584 Color: 0

Bin 116: 49 of cap free
Amount of items: 2
Items: 
Size: 6291 Color: 3
Size: 1780 Color: 1

Bin 117: 53 of cap free
Amount of items: 2
Items: 
Size: 6913 Color: 0
Size: 1154 Color: 2

Bin 118: 57 of cap free
Amount of items: 5
Items: 
Size: 4061 Color: 0
Size: 1135 Color: 2
Size: 1041 Color: 3
Size: 942 Color: 4
Size: 884 Color: 3

Bin 119: 64 of cap free
Amount of items: 2
Items: 
Size: 7169 Color: 4
Size: 887 Color: 1

Bin 120: 65 of cap free
Amount of items: 2
Items: 
Size: 6763 Color: 1
Size: 1292 Color: 3

Bin 121: 73 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 1
Size: 1033 Color: 0

Bin 122: 74 of cap free
Amount of items: 2
Items: 
Size: 6114 Color: 3
Size: 1932 Color: 2

Bin 123: 77 of cap free
Amount of items: 2
Items: 
Size: 6897 Color: 4
Size: 1146 Color: 0

Bin 124: 92 of cap free
Amount of items: 2
Items: 
Size: 6354 Color: 4
Size: 1674 Color: 2

Bin 125: 94 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 4
Size: 2204 Color: 2

Bin 126: 103 of cap free
Amount of items: 2
Items: 
Size: 4634 Color: 3
Size: 3383 Color: 2

Bin 127: 112 of cap free
Amount of items: 2
Items: 
Size: 5102 Color: 0
Size: 2906 Color: 4

Bin 128: 114 of cap free
Amount of items: 13
Items: 
Size: 988 Color: 3
Size: 822 Color: 3
Size: 758 Color: 3
Size: 747 Color: 4
Size: 722 Color: 2
Size: 681 Color: 0
Size: 672 Color: 4
Size: 632 Color: 2
Size: 580 Color: 3
Size: 580 Color: 1
Size: 344 Color: 4
Size: 272 Color: 0
Size: 208 Color: 0

Bin 129: 123 of cap free
Amount of items: 2
Items: 
Size: 4615 Color: 3
Size: 3382 Color: 1

Bin 130: 124 of cap free
Amount of items: 2
Items: 
Size: 5073 Color: 4
Size: 2923 Color: 1

Bin 131: 125 of cap free
Amount of items: 2
Items: 
Size: 5081 Color: 1
Size: 2914 Color: 3

Bin 132: 126 of cap free
Amount of items: 2
Items: 
Size: 5471 Color: 4
Size: 2523 Color: 0

Bin 133: 5718 of cap free
Amount of items: 15
Items: 
Size: 206 Color: 1
Size: 176 Color: 4
Size: 176 Color: 3
Size: 176 Color: 0
Size: 172 Color: 4
Size: 172 Color: 3
Size: 168 Color: 4
Size: 168 Color: 1
Size: 158 Color: 0
Size: 148 Color: 2
Size: 148 Color: 0
Size: 136 Color: 4
Size: 136 Color: 0
Size: 134 Color: 2
Size: 128 Color: 1

Total size: 1071840
Total free space: 8120

