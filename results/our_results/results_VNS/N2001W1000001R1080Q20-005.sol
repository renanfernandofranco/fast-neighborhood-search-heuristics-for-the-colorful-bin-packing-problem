Capicity Bin: 1000001
Lower Bound: 901

Bins used: 906
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 742416 Color: 13
Size: 134245 Color: 18
Size: 123340 Color: 17

Bin 2: 0 of cap free
Amount of items: 4
Items: 
Size: 480049 Color: 14
Size: 256482 Color: 18
Size: 144345 Color: 5
Size: 119125 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 590575 Color: 3
Size: 243032 Color: 7
Size: 166394 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 721685 Color: 8
Size: 139942 Color: 0
Size: 138374 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 714080 Color: 5
Size: 158211 Color: 7
Size: 127710 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 719786 Color: 17
Size: 158546 Color: 3
Size: 121669 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 735867 Color: 9
Size: 133851 Color: 18
Size: 130283 Color: 5

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 747303 Color: 5
Size: 143692 Color: 4
Size: 109006 Color: 11

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 745750 Color: 17
Size: 148147 Color: 13
Size: 106104 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 704321 Color: 14
Size: 157572 Color: 3
Size: 138108 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 725704 Color: 4
Size: 168356 Color: 15
Size: 105941 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 598197 Color: 6
Size: 277703 Color: 1
Size: 124101 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 705940 Color: 1
Size: 160343 Color: 0
Size: 133718 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 768925 Color: 8
Size: 118367 Color: 19
Size: 112709 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 681028 Color: 14
Size: 186982 Color: 4
Size: 131991 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 581993 Color: 12
Size: 271547 Color: 10
Size: 146461 Color: 17

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 774698 Color: 12
Size: 123303 Color: 3
Size: 102000 Color: 13

Bin 18: 0 of cap free
Amount of items: 2
Items: 
Size: 533586 Color: 0
Size: 466415 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 639618 Color: 1
Size: 231393 Color: 10
Size: 128990 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 459390 Color: 17
Size: 331516 Color: 11
Size: 209095 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 760218 Color: 2
Size: 138839 Color: 4
Size: 100944 Color: 19

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 746134 Color: 2
Size: 153247 Color: 5
Size: 100620 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 575865 Color: 12
Size: 231062 Color: 14
Size: 193074 Color: 15

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 700543 Color: 12
Size: 192916 Color: 7
Size: 106542 Color: 6

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 769702 Color: 6
Size: 122223 Color: 2
Size: 108076 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 703052 Color: 10
Size: 187412 Color: 4
Size: 109537 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 630780 Color: 15
Size: 268072 Color: 19
Size: 101149 Color: 3

Bin 28: 0 of cap free
Amount of items: 4
Items: 
Size: 428625 Color: 13
Size: 280500 Color: 19
Size: 164771 Color: 5
Size: 126105 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 514175 Color: 9
Size: 243350 Color: 6
Size: 242476 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 601508 Color: 8
Size: 215209 Color: 5
Size: 183284 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 751157 Color: 7
Size: 132576 Color: 16
Size: 116268 Color: 19

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 723869 Color: 18
Size: 150193 Color: 9
Size: 125939 Color: 11

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 700714 Color: 6
Size: 195901 Color: 7
Size: 103386 Color: 11

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 493956 Color: 10
Size: 263953 Color: 18
Size: 242092 Color: 3

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 729300 Color: 1
Size: 136434 Color: 2
Size: 134267 Color: 18

Bin 36: 1 of cap free
Amount of items: 2
Items: 
Size: 548010 Color: 13
Size: 451990 Color: 11

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 555861 Color: 16
Size: 255938 Color: 8
Size: 188201 Color: 12

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 597246 Color: 6
Size: 402754 Color: 2

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 636780 Color: 12
Size: 182785 Color: 0
Size: 180435 Color: 7

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 640007 Color: 6
Size: 181940 Color: 9
Size: 178053 Color: 12

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 733890 Color: 7
Size: 137587 Color: 2
Size: 128523 Color: 5

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 660707 Color: 14
Size: 229334 Color: 14
Size: 109959 Color: 11

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 764025 Color: 17
Size: 121936 Color: 15
Size: 114039 Color: 10

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 741515 Color: 14
Size: 129930 Color: 8
Size: 128555 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 402830 Color: 3
Size: 312771 Color: 12
Size: 284399 Color: 15

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 660842 Color: 8
Size: 180696 Color: 16
Size: 158462 Color: 1

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 677969 Color: 10
Size: 164832 Color: 11
Size: 157199 Color: 16

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 747766 Color: 10
Size: 142700 Color: 6
Size: 109534 Color: 2

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 753143 Color: 8
Size: 124192 Color: 11
Size: 122665 Color: 10

Bin 50: 1 of cap free
Amount of items: 4
Items: 
Size: 403255 Color: 1
Size: 294516 Color: 6
Size: 152331 Color: 17
Size: 149898 Color: 11

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 658410 Color: 16
Size: 212061 Color: 15
Size: 129529 Color: 1

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 662488 Color: 3
Size: 178821 Color: 15
Size: 158691 Color: 11

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 723382 Color: 2
Size: 144519 Color: 16
Size: 132099 Color: 10

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 690268 Color: 3
Size: 173645 Color: 0
Size: 136087 Color: 4

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 457893 Color: 11
Size: 394013 Color: 3
Size: 148094 Color: 15

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 374295 Color: 8
Size: 372889 Color: 14
Size: 252816 Color: 6

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 684017 Color: 15
Size: 158256 Color: 12
Size: 157726 Color: 9

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 679163 Color: 7
Size: 166475 Color: 0
Size: 154361 Color: 5

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 705153 Color: 0
Size: 182808 Color: 13
Size: 112038 Color: 10

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 709611 Color: 5
Size: 146141 Color: 17
Size: 144247 Color: 7

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 674119 Color: 15
Size: 183640 Color: 18
Size: 142240 Color: 1

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 645756 Color: 11
Size: 178930 Color: 17
Size: 175313 Color: 10

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 769228 Color: 12
Size: 121537 Color: 0
Size: 109234 Color: 18

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 738682 Color: 18
Size: 152993 Color: 7
Size: 108324 Color: 15

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 730869 Color: 1
Size: 151076 Color: 11
Size: 118054 Color: 16

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 404235 Color: 10
Size: 316720 Color: 9
Size: 279044 Color: 10

Bin 67: 2 of cap free
Amount of items: 4
Items: 
Size: 425833 Color: 6
Size: 283408 Color: 7
Size: 155272 Color: 11
Size: 135486 Color: 0

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 465921 Color: 6
Size: 412929 Color: 17
Size: 121149 Color: 11

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 744873 Color: 14
Size: 255125 Color: 7

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 778735 Color: 9
Size: 112668 Color: 18
Size: 108595 Color: 18

Bin 71: 3 of cap free
Amount of items: 3
Items: 
Size: 764475 Color: 1
Size: 129322 Color: 17
Size: 106201 Color: 12

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 592552 Color: 14
Size: 210003 Color: 15
Size: 197443 Color: 11

Bin 73: 3 of cap free
Amount of items: 3
Items: 
Size: 602186 Color: 7
Size: 211355 Color: 11
Size: 186457 Color: 2

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 761231 Color: 5
Size: 125804 Color: 7
Size: 112963 Color: 3

Bin 75: 3 of cap free
Amount of items: 2
Items: 
Size: 791755 Color: 13
Size: 208243 Color: 17

Bin 76: 3 of cap free
Amount of items: 3
Items: 
Size: 733668 Color: 7
Size: 151528 Color: 3
Size: 114802 Color: 15

Bin 77: 3 of cap free
Amount of items: 2
Items: 
Size: 765609 Color: 7
Size: 234389 Color: 9

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 480098 Color: 14
Size: 274299 Color: 10
Size: 245600 Color: 1

Bin 79: 4 of cap free
Amount of items: 2
Items: 
Size: 547102 Color: 17
Size: 452895 Color: 6

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 736376 Color: 5
Size: 136999 Color: 15
Size: 126622 Color: 3

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 769107 Color: 6
Size: 121387 Color: 10
Size: 109503 Color: 19

Bin 82: 4 of cap free
Amount of items: 3
Items: 
Size: 752283 Color: 10
Size: 134546 Color: 19
Size: 113168 Color: 19

Bin 83: 5 of cap free
Amount of items: 2
Items: 
Size: 571544 Color: 16
Size: 428452 Color: 11

Bin 84: 5 of cap free
Amount of items: 2
Items: 
Size: 666114 Color: 11
Size: 333882 Color: 13

Bin 85: 5 of cap free
Amount of items: 3
Items: 
Size: 724274 Color: 3
Size: 146032 Color: 18
Size: 129690 Color: 0

Bin 86: 5 of cap free
Amount of items: 3
Items: 
Size: 774121 Color: 19
Size: 115562 Color: 13
Size: 110313 Color: 1

Bin 87: 5 of cap free
Amount of items: 3
Items: 
Size: 651270 Color: 3
Size: 174490 Color: 17
Size: 174236 Color: 12

Bin 88: 5 of cap free
Amount of items: 3
Items: 
Size: 592236 Color: 2
Size: 218451 Color: 5
Size: 189309 Color: 19

Bin 89: 6 of cap free
Amount of items: 3
Items: 
Size: 732563 Color: 18
Size: 134369 Color: 18
Size: 133063 Color: 7

Bin 90: 6 of cap free
Amount of items: 3
Items: 
Size: 758699 Color: 13
Size: 123956 Color: 3
Size: 117340 Color: 4

Bin 91: 6 of cap free
Amount of items: 3
Items: 
Size: 714133 Color: 17
Size: 146635 Color: 5
Size: 139227 Color: 16

Bin 92: 6 of cap free
Amount of items: 2
Items: 
Size: 754143 Color: 9
Size: 245852 Color: 15

Bin 93: 6 of cap free
Amount of items: 3
Items: 
Size: 749132 Color: 2
Size: 147333 Color: 14
Size: 103530 Color: 1

Bin 94: 6 of cap free
Amount of items: 2
Items: 
Size: 749903 Color: 13
Size: 250092 Color: 15

Bin 95: 6 of cap free
Amount of items: 3
Items: 
Size: 716785 Color: 5
Size: 152456 Color: 13
Size: 130754 Color: 13

Bin 96: 7 of cap free
Amount of items: 2
Items: 
Size: 627508 Color: 3
Size: 372486 Color: 15

Bin 97: 7 of cap free
Amount of items: 3
Items: 
Size: 717297 Color: 15
Size: 142600 Color: 2
Size: 140097 Color: 0

Bin 98: 7 of cap free
Amount of items: 3
Items: 
Size: 671120 Color: 7
Size: 224594 Color: 8
Size: 104280 Color: 3

Bin 99: 7 of cap free
Amount of items: 3
Items: 
Size: 749879 Color: 11
Size: 136604 Color: 9
Size: 113511 Color: 4

Bin 100: 7 of cap free
Amount of items: 2
Items: 
Size: 549283 Color: 12
Size: 450711 Color: 15

Bin 101: 8 of cap free
Amount of items: 2
Items: 
Size: 602488 Color: 7
Size: 397505 Color: 8

Bin 102: 8 of cap free
Amount of items: 2
Items: 
Size: 505426 Color: 13
Size: 494567 Color: 19

Bin 103: 8 of cap free
Amount of items: 3
Items: 
Size: 712903 Color: 2
Size: 156354 Color: 6
Size: 130736 Color: 10

Bin 104: 9 of cap free
Amount of items: 3
Items: 
Size: 612766 Color: 15
Size: 196700 Color: 3
Size: 190526 Color: 5

Bin 105: 9 of cap free
Amount of items: 3
Items: 
Size: 641438 Color: 18
Size: 180255 Color: 2
Size: 178299 Color: 2

Bin 106: 9 of cap free
Amount of items: 3
Items: 
Size: 652046 Color: 15
Size: 174198 Color: 17
Size: 173748 Color: 19

Bin 107: 9 of cap free
Amount of items: 3
Items: 
Size: 394776 Color: 12
Size: 317360 Color: 17
Size: 287856 Color: 0

Bin 108: 9 of cap free
Amount of items: 3
Items: 
Size: 369928 Color: 11
Size: 343374 Color: 11
Size: 286690 Color: 1

Bin 109: 9 of cap free
Amount of items: 3
Items: 
Size: 469962 Color: 19
Size: 265217 Color: 16
Size: 264813 Color: 12

Bin 110: 9 of cap free
Amount of items: 2
Items: 
Size: 648797 Color: 11
Size: 351195 Color: 8

Bin 111: 10 of cap free
Amount of items: 3
Items: 
Size: 608151 Color: 8
Size: 197752 Color: 6
Size: 194088 Color: 9

Bin 112: 10 of cap free
Amount of items: 2
Items: 
Size: 650595 Color: 16
Size: 349396 Color: 3

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 495914 Color: 13
Size: 260609 Color: 2
Size: 243468 Color: 1

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 514984 Color: 14
Size: 485007 Color: 6

Bin 115: 11 of cap free
Amount of items: 2
Items: 
Size: 624538 Color: 13
Size: 375452 Color: 6

Bin 116: 11 of cap free
Amount of items: 2
Items: 
Size: 645806 Color: 2
Size: 354184 Color: 5

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 699978 Color: 19
Size: 300012 Color: 15

Bin 118: 11 of cap free
Amount of items: 3
Items: 
Size: 735052 Color: 15
Size: 137351 Color: 5
Size: 127587 Color: 19

Bin 119: 11 of cap free
Amount of items: 3
Items: 
Size: 681882 Color: 12
Size: 163528 Color: 9
Size: 154580 Color: 6

Bin 120: 11 of cap free
Amount of items: 3
Items: 
Size: 458886 Color: 3
Size: 418050 Color: 3
Size: 123054 Color: 15

Bin 121: 11 of cap free
Amount of items: 2
Items: 
Size: 782926 Color: 15
Size: 217064 Color: 7

Bin 122: 11 of cap free
Amount of items: 3
Items: 
Size: 637602 Color: 4
Size: 183890 Color: 10
Size: 178498 Color: 15

Bin 123: 11 of cap free
Amount of items: 3
Items: 
Size: 703774 Color: 14
Size: 148309 Color: 7
Size: 147907 Color: 19

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 384002 Color: 18
Size: 310211 Color: 6
Size: 305777 Color: 8

Bin 125: 12 of cap free
Amount of items: 3
Items: 
Size: 715667 Color: 13
Size: 144828 Color: 3
Size: 139494 Color: 14

Bin 126: 12 of cap free
Amount of items: 3
Items: 
Size: 738738 Color: 17
Size: 140726 Color: 2
Size: 120525 Color: 18

Bin 127: 12 of cap free
Amount of items: 3
Items: 
Size: 391829 Color: 12
Size: 306461 Color: 2
Size: 301699 Color: 1

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 621988 Color: 13
Size: 194647 Color: 15
Size: 183354 Color: 0

Bin 129: 13 of cap free
Amount of items: 3
Items: 
Size: 505541 Color: 9
Size: 255951 Color: 11
Size: 238496 Color: 15

Bin 130: 13 of cap free
Amount of items: 3
Items: 
Size: 722494 Color: 10
Size: 146138 Color: 2
Size: 131356 Color: 12

Bin 131: 13 of cap free
Amount of items: 2
Items: 
Size: 525989 Color: 14
Size: 473999 Color: 10

Bin 132: 14 of cap free
Amount of items: 2
Items: 
Size: 567910 Color: 6
Size: 432077 Color: 1

Bin 133: 14 of cap free
Amount of items: 2
Items: 
Size: 716890 Color: 17
Size: 283097 Color: 15

Bin 134: 14 of cap free
Amount of items: 2
Items: 
Size: 770480 Color: 14
Size: 229507 Color: 16

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 788540 Color: 0
Size: 211447 Color: 4

Bin 136: 14 of cap free
Amount of items: 2
Items: 
Size: 720526 Color: 12
Size: 279461 Color: 7

Bin 137: 14 of cap free
Amount of items: 3
Items: 
Size: 508135 Color: 15
Size: 247524 Color: 17
Size: 244328 Color: 18

Bin 138: 14 of cap free
Amount of items: 3
Items: 
Size: 345770 Color: 10
Size: 342337 Color: 11
Size: 311880 Color: 12

Bin 139: 15 of cap free
Amount of items: 2
Items: 
Size: 683063 Color: 3
Size: 316923 Color: 17

Bin 140: 15 of cap free
Amount of items: 3
Items: 
Size: 740579 Color: 17
Size: 156757 Color: 16
Size: 102650 Color: 1

Bin 141: 15 of cap free
Amount of items: 2
Items: 
Size: 633313 Color: 2
Size: 366673 Color: 12

Bin 142: 15 of cap free
Amount of items: 2
Items: 
Size: 665686 Color: 9
Size: 334300 Color: 19

Bin 143: 16 of cap free
Amount of items: 2
Items: 
Size: 767532 Color: 7
Size: 232453 Color: 13

Bin 144: 16 of cap free
Amount of items: 3
Items: 
Size: 674285 Color: 14
Size: 176615 Color: 2
Size: 149085 Color: 11

Bin 145: 16 of cap free
Amount of items: 2
Items: 
Size: 747380 Color: 11
Size: 252605 Color: 9

Bin 146: 17 of cap free
Amount of items: 2
Items: 
Size: 558370 Color: 4
Size: 441614 Color: 11

Bin 147: 17 of cap free
Amount of items: 2
Items: 
Size: 626011 Color: 16
Size: 373973 Color: 0

Bin 148: 17 of cap free
Amount of items: 2
Items: 
Size: 636924 Color: 18
Size: 363060 Color: 0

Bin 149: 17 of cap free
Amount of items: 2
Items: 
Size: 745714 Color: 10
Size: 254270 Color: 5

Bin 150: 17 of cap free
Amount of items: 2
Items: 
Size: 795697 Color: 3
Size: 204287 Color: 7

Bin 151: 17 of cap free
Amount of items: 2
Items: 
Size: 525605 Color: 1
Size: 474379 Color: 7

Bin 152: 18 of cap free
Amount of items: 3
Items: 
Size: 600094 Color: 12
Size: 208789 Color: 3
Size: 191100 Color: 12

Bin 153: 18 of cap free
Amount of items: 2
Items: 
Size: 625540 Color: 16
Size: 374443 Color: 11

Bin 154: 18 of cap free
Amount of items: 2
Items: 
Size: 630277 Color: 7
Size: 369706 Color: 17

Bin 155: 18 of cap free
Amount of items: 2
Items: 
Size: 620169 Color: 2
Size: 379814 Color: 12

Bin 156: 18 of cap free
Amount of items: 3
Items: 
Size: 671051 Color: 7
Size: 227311 Color: 12
Size: 101621 Color: 15

Bin 157: 19 of cap free
Amount of items: 3
Items: 
Size: 603856 Color: 4
Size: 208588 Color: 6
Size: 187538 Color: 10

Bin 158: 19 of cap free
Amount of items: 3
Items: 
Size: 456561 Color: 15
Size: 428533 Color: 14
Size: 114888 Color: 10

Bin 159: 20 of cap free
Amount of items: 3
Items: 
Size: 367591 Color: 3
Size: 316640 Color: 5
Size: 315750 Color: 3

Bin 160: 20 of cap free
Amount of items: 3
Items: 
Size: 680833 Color: 15
Size: 159939 Color: 13
Size: 159209 Color: 9

Bin 161: 20 of cap free
Amount of items: 3
Items: 
Size: 641344 Color: 2
Size: 180327 Color: 7
Size: 178310 Color: 11

Bin 162: 20 of cap free
Amount of items: 3
Items: 
Size: 479170 Color: 18
Size: 398966 Color: 3
Size: 121845 Color: 4

Bin 163: 21 of cap free
Amount of items: 2
Items: 
Size: 538785 Color: 12
Size: 461195 Color: 8

Bin 164: 21 of cap free
Amount of items: 3
Items: 
Size: 728694 Color: 12
Size: 153475 Color: 2
Size: 117811 Color: 17

Bin 165: 21 of cap free
Amount of items: 3
Items: 
Size: 580062 Color: 18
Size: 230320 Color: 2
Size: 189598 Color: 16

Bin 166: 21 of cap free
Amount of items: 3
Items: 
Size: 677370 Color: 4
Size: 162758 Color: 6
Size: 159852 Color: 0

Bin 167: 21 of cap free
Amount of items: 3
Items: 
Size: 463395 Color: 5
Size: 271431 Color: 18
Size: 265154 Color: 4

Bin 168: 22 of cap free
Amount of items: 2
Items: 
Size: 586130 Color: 17
Size: 413849 Color: 16

Bin 169: 23 of cap free
Amount of items: 2
Items: 
Size: 578576 Color: 9
Size: 421402 Color: 18

Bin 170: 23 of cap free
Amount of items: 2
Items: 
Size: 637418 Color: 16
Size: 362560 Color: 14

Bin 171: 23 of cap free
Amount of items: 2
Items: 
Size: 771853 Color: 4
Size: 228125 Color: 1

Bin 172: 23 of cap free
Amount of items: 3
Items: 
Size: 684219 Color: 11
Size: 207426 Color: 3
Size: 108333 Color: 12

Bin 173: 24 of cap free
Amount of items: 2
Items: 
Size: 635857 Color: 10
Size: 364120 Color: 0

Bin 174: 24 of cap free
Amount of items: 2
Items: 
Size: 725880 Color: 11
Size: 274097 Color: 19

Bin 175: 24 of cap free
Amount of items: 2
Items: 
Size: 668059 Color: 14
Size: 331918 Color: 6

Bin 176: 24 of cap free
Amount of items: 2
Items: 
Size: 592840 Color: 1
Size: 407137 Color: 5

Bin 177: 25 of cap free
Amount of items: 2
Items: 
Size: 789564 Color: 5
Size: 210412 Color: 13

Bin 178: 25 of cap free
Amount of items: 3
Items: 
Size: 372517 Color: 13
Size: 348324 Color: 11
Size: 279135 Color: 17

Bin 179: 25 of cap free
Amount of items: 3
Items: 
Size: 348281 Color: 3
Size: 345107 Color: 0
Size: 306588 Color: 12

Bin 180: 26 of cap free
Amount of items: 3
Items: 
Size: 474313 Color: 12
Size: 262892 Color: 6
Size: 262770 Color: 12

Bin 181: 26 of cap free
Amount of items: 2
Items: 
Size: 514376 Color: 18
Size: 485599 Color: 19

Bin 182: 26 of cap free
Amount of items: 2
Items: 
Size: 759949 Color: 4
Size: 240026 Color: 15

Bin 183: 26 of cap free
Amount of items: 3
Items: 
Size: 690833 Color: 19
Size: 157400 Color: 16
Size: 151742 Color: 5

Bin 184: 26 of cap free
Amount of items: 2
Items: 
Size: 517355 Color: 8
Size: 482620 Color: 19

Bin 185: 26 of cap free
Amount of items: 2
Items: 
Size: 737757 Color: 7
Size: 262218 Color: 15

Bin 186: 27 of cap free
Amount of items: 2
Items: 
Size: 642564 Color: 10
Size: 357410 Color: 5

Bin 187: 27 of cap free
Amount of items: 2
Items: 
Size: 631684 Color: 14
Size: 368290 Color: 16

Bin 188: 27 of cap free
Amount of items: 3
Items: 
Size: 608966 Color: 2
Size: 197606 Color: 17
Size: 193402 Color: 14

Bin 189: 28 of cap free
Amount of items: 2
Items: 
Size: 515102 Color: 13
Size: 484871 Color: 0

Bin 190: 28 of cap free
Amount of items: 2
Items: 
Size: 666009 Color: 6
Size: 333964 Color: 10

Bin 191: 28 of cap free
Amount of items: 3
Items: 
Size: 639996 Color: 1
Size: 181308 Color: 0
Size: 178669 Color: 11

Bin 192: 28 of cap free
Amount of items: 3
Items: 
Size: 588316 Color: 18
Size: 215089 Color: 1
Size: 196568 Color: 6

Bin 193: 29 of cap free
Amount of items: 3
Items: 
Size: 391627 Color: 14
Size: 316386 Color: 8
Size: 291959 Color: 7

Bin 194: 30 of cap free
Amount of items: 2
Items: 
Size: 579346 Color: 4
Size: 420625 Color: 7

Bin 195: 30 of cap free
Amount of items: 2
Items: 
Size: 672524 Color: 14
Size: 327447 Color: 16

Bin 196: 30 of cap free
Amount of items: 3
Items: 
Size: 348019 Color: 16
Size: 347754 Color: 9
Size: 304198 Color: 3

Bin 197: 31 of cap free
Amount of items: 2
Items: 
Size: 581040 Color: 15
Size: 418930 Color: 0

Bin 198: 31 of cap free
Amount of items: 2
Items: 
Size: 618629 Color: 3
Size: 381341 Color: 13

Bin 199: 31 of cap free
Amount of items: 2
Items: 
Size: 561413 Color: 14
Size: 438557 Color: 12

Bin 200: 32 of cap free
Amount of items: 2
Items: 
Size: 708544 Color: 0
Size: 291425 Color: 16

Bin 201: 32 of cap free
Amount of items: 2
Items: 
Size: 749998 Color: 3
Size: 249971 Color: 10

Bin 202: 32 of cap free
Amount of items: 3
Items: 
Size: 618472 Color: 18
Size: 261911 Color: 18
Size: 119586 Color: 6

Bin 203: 32 of cap free
Amount of items: 2
Items: 
Size: 634466 Color: 6
Size: 365503 Color: 14

Bin 204: 33 of cap free
Amount of items: 2
Items: 
Size: 582048 Color: 18
Size: 417920 Color: 4

Bin 205: 33 of cap free
Amount of items: 2
Items: 
Size: 780443 Color: 15
Size: 219525 Color: 6

Bin 206: 34 of cap free
Amount of items: 3
Items: 
Size: 478796 Color: 7
Size: 261472 Color: 11
Size: 259699 Color: 17

Bin 207: 34 of cap free
Amount of items: 3
Items: 
Size: 600010 Color: 0
Size: 211466 Color: 9
Size: 188491 Color: 7

Bin 208: 34 of cap free
Amount of items: 2
Items: 
Size: 654811 Color: 8
Size: 345156 Color: 11

Bin 209: 35 of cap free
Amount of items: 2
Items: 
Size: 609212 Color: 9
Size: 390754 Color: 13

Bin 210: 36 of cap free
Amount of items: 2
Items: 
Size: 655061 Color: 0
Size: 344904 Color: 9

Bin 211: 36 of cap free
Amount of items: 2
Items: 
Size: 760916 Color: 14
Size: 239049 Color: 0

Bin 212: 36 of cap free
Amount of items: 3
Items: 
Size: 675404 Color: 6
Size: 171093 Color: 9
Size: 153468 Color: 10

Bin 213: 37 of cap free
Amount of items: 2
Items: 
Size: 750657 Color: 12
Size: 249307 Color: 18

Bin 214: 37 of cap free
Amount of items: 3
Items: 
Size: 370008 Color: 6
Size: 347913 Color: 8
Size: 282043 Color: 13

Bin 215: 38 of cap free
Amount of items: 2
Items: 
Size: 556803 Color: 0
Size: 443160 Color: 1

Bin 216: 38 of cap free
Amount of items: 3
Items: 
Size: 367513 Color: 15
Size: 316455 Color: 18
Size: 315995 Color: 2

Bin 217: 38 of cap free
Amount of items: 3
Items: 
Size: 346926 Color: 19
Size: 332840 Color: 11
Size: 320197 Color: 13

Bin 218: 40 of cap free
Amount of items: 3
Items: 
Size: 729514 Color: 12
Size: 141741 Color: 9
Size: 128706 Color: 11

Bin 219: 40 of cap free
Amount of items: 3
Items: 
Size: 648785 Color: 11
Size: 175721 Color: 7
Size: 175455 Color: 12

Bin 220: 41 of cap free
Amount of items: 2
Items: 
Size: 769430 Color: 11
Size: 230530 Color: 5

Bin 221: 42 of cap free
Amount of items: 2
Items: 
Size: 528687 Color: 19
Size: 471272 Color: 10

Bin 222: 42 of cap free
Amount of items: 2
Items: 
Size: 698232 Color: 16
Size: 301727 Color: 7

Bin 223: 42 of cap free
Amount of items: 2
Items: 
Size: 792256 Color: 10
Size: 207703 Color: 18

Bin 224: 42 of cap free
Amount of items: 3
Items: 
Size: 456709 Color: 13
Size: 373930 Color: 0
Size: 169320 Color: 19

Bin 225: 42 of cap free
Amount of items: 2
Items: 
Size: 717794 Color: 15
Size: 282165 Color: 10

Bin 226: 42 of cap free
Amount of items: 2
Items: 
Size: 606269 Color: 1
Size: 393690 Color: 8

Bin 227: 43 of cap free
Amount of items: 2
Items: 
Size: 625016 Color: 12
Size: 374942 Color: 2

Bin 228: 43 of cap free
Amount of items: 3
Items: 
Size: 343621 Color: 14
Size: 341460 Color: 1
Size: 314877 Color: 2

Bin 229: 44 of cap free
Amount of items: 3
Items: 
Size: 695501 Color: 8
Size: 166710 Color: 3
Size: 137746 Color: 6

Bin 230: 45 of cap free
Amount of items: 2
Items: 
Size: 545435 Color: 9
Size: 454521 Color: 16

Bin 231: 46 of cap free
Amount of items: 2
Items: 
Size: 595700 Color: 2
Size: 404255 Color: 14

Bin 232: 46 of cap free
Amount of items: 2
Items: 
Size: 522007 Color: 2
Size: 477948 Color: 15

Bin 233: 46 of cap free
Amount of items: 3
Items: 
Size: 389847 Color: 2
Size: 322305 Color: 3
Size: 287803 Color: 9

Bin 234: 46 of cap free
Amount of items: 3
Items: 
Size: 420261 Color: 14
Size: 301125 Color: 10
Size: 278569 Color: 7

Bin 235: 47 of cap free
Amount of items: 2
Items: 
Size: 599052 Color: 10
Size: 400902 Color: 17

Bin 236: 47 of cap free
Amount of items: 2
Items: 
Size: 689876 Color: 3
Size: 310078 Color: 19

Bin 237: 47 of cap free
Amount of items: 2
Items: 
Size: 763729 Color: 6
Size: 236225 Color: 4

Bin 238: 47 of cap free
Amount of items: 3
Items: 
Size: 718388 Color: 15
Size: 145914 Color: 6
Size: 135652 Color: 18

Bin 239: 47 of cap free
Amount of items: 3
Items: 
Size: 610968 Color: 12
Size: 196624 Color: 2
Size: 192362 Color: 15

Bin 240: 47 of cap free
Amount of items: 2
Items: 
Size: 600228 Color: 11
Size: 399726 Color: 3

Bin 241: 48 of cap free
Amount of items: 2
Items: 
Size: 742007 Color: 4
Size: 257946 Color: 13

Bin 242: 49 of cap free
Amount of items: 3
Items: 
Size: 596848 Color: 0
Size: 209903 Color: 16
Size: 193201 Color: 10

Bin 243: 49 of cap free
Amount of items: 2
Items: 
Size: 590146 Color: 6
Size: 409806 Color: 8

Bin 244: 50 of cap free
Amount of items: 3
Items: 
Size: 632374 Color: 17
Size: 184601 Color: 15
Size: 182976 Color: 13

Bin 245: 50 of cap free
Amount of items: 2
Items: 
Size: 641282 Color: 1
Size: 358669 Color: 10

Bin 246: 51 of cap free
Amount of items: 2
Items: 
Size: 575021 Color: 7
Size: 424929 Color: 3

Bin 247: 52 of cap free
Amount of items: 2
Items: 
Size: 744923 Color: 3
Size: 255026 Color: 12

Bin 248: 53 of cap free
Amount of items: 2
Items: 
Size: 737543 Color: 18
Size: 262405 Color: 11

Bin 249: 53 of cap free
Amount of items: 3
Items: 
Size: 595950 Color: 16
Size: 211233 Color: 11
Size: 192765 Color: 6

Bin 250: 55 of cap free
Amount of items: 2
Items: 
Size: 620270 Color: 6
Size: 379676 Color: 0

Bin 251: 56 of cap free
Amount of items: 3
Items: 
Size: 640395 Color: 10
Size: 183512 Color: 3
Size: 176038 Color: 6

Bin 252: 56 of cap free
Amount of items: 2
Items: 
Size: 583766 Color: 3
Size: 416179 Color: 16

Bin 253: 57 of cap free
Amount of items: 2
Items: 
Size: 783766 Color: 18
Size: 216178 Color: 2

Bin 254: 58 of cap free
Amount of items: 2
Items: 
Size: 522537 Color: 3
Size: 477406 Color: 2

Bin 255: 60 of cap free
Amount of items: 2
Items: 
Size: 701925 Color: 0
Size: 298016 Color: 14

Bin 256: 60 of cap free
Amount of items: 2
Items: 
Size: 526196 Color: 17
Size: 473745 Color: 4

Bin 257: 61 of cap free
Amount of items: 2
Items: 
Size: 565957 Color: 3
Size: 433983 Color: 6

Bin 258: 61 of cap free
Amount of items: 2
Items: 
Size: 609750 Color: 16
Size: 390190 Color: 7

Bin 259: 61 of cap free
Amount of items: 3
Items: 
Size: 547632 Color: 15
Size: 329635 Color: 1
Size: 122673 Color: 0

Bin 260: 62 of cap free
Amount of items: 3
Items: 
Size: 383789 Color: 15
Size: 366772 Color: 19
Size: 249378 Color: 4

Bin 261: 63 of cap free
Amount of items: 3
Items: 
Size: 688508 Color: 16
Size: 162200 Color: 5
Size: 149230 Color: 6

Bin 262: 63 of cap free
Amount of items: 2
Items: 
Size: 790768 Color: 6
Size: 209170 Color: 16

Bin 263: 63 of cap free
Amount of items: 3
Items: 
Size: 659990 Color: 17
Size: 175118 Color: 1
Size: 164830 Color: 5

Bin 264: 64 of cap free
Amount of items: 2
Items: 
Size: 685286 Color: 19
Size: 314651 Color: 18

Bin 265: 64 of cap free
Amount of items: 3
Items: 
Size: 597689 Color: 14
Size: 211167 Color: 3
Size: 191081 Color: 16

Bin 266: 65 of cap free
Amount of items: 2
Items: 
Size: 636956 Color: 1
Size: 362980 Color: 7

Bin 267: 66 of cap free
Amount of items: 2
Items: 
Size: 664587 Color: 14
Size: 335348 Color: 17

Bin 268: 66 of cap free
Amount of items: 2
Items: 
Size: 764917 Color: 8
Size: 235018 Color: 4

Bin 269: 66 of cap free
Amount of items: 3
Items: 
Size: 476978 Color: 18
Size: 261511 Color: 17
Size: 261446 Color: 2

Bin 270: 67 of cap free
Amount of items: 2
Items: 
Size: 657117 Color: 1
Size: 342817 Color: 8

Bin 271: 67 of cap free
Amount of items: 2
Items: 
Size: 728692 Color: 10
Size: 271242 Color: 15

Bin 272: 68 of cap free
Amount of items: 2
Items: 
Size: 555469 Color: 19
Size: 444464 Color: 18

Bin 273: 68 of cap free
Amount of items: 3
Items: 
Size: 717025 Color: 13
Size: 146645 Color: 14
Size: 136263 Color: 0

Bin 274: 68 of cap free
Amount of items: 3
Items: 
Size: 577050 Color: 11
Size: 229121 Color: 14
Size: 193762 Color: 2

Bin 275: 69 of cap free
Amount of items: 2
Items: 
Size: 690745 Color: 19
Size: 309187 Color: 17

Bin 276: 69 of cap free
Amount of items: 2
Items: 
Size: 679750 Color: 4
Size: 320182 Color: 9

Bin 277: 70 of cap free
Amount of items: 2
Items: 
Size: 669661 Color: 10
Size: 330270 Color: 18

Bin 278: 70 of cap free
Amount of items: 3
Items: 
Size: 714813 Color: 3
Size: 149309 Color: 7
Size: 135809 Color: 18

Bin 279: 70 of cap free
Amount of items: 2
Items: 
Size: 529477 Color: 5
Size: 470454 Color: 0

Bin 280: 70 of cap free
Amount of items: 2
Items: 
Size: 545862 Color: 8
Size: 454069 Color: 16

Bin 281: 71 of cap free
Amount of items: 3
Items: 
Size: 681830 Color: 2
Size: 173230 Color: 11
Size: 144870 Color: 9

Bin 282: 71 of cap free
Amount of items: 2
Items: 
Size: 792533 Color: 12
Size: 207397 Color: 8

Bin 283: 72 of cap free
Amount of items: 2
Items: 
Size: 699202 Color: 12
Size: 300727 Color: 4

Bin 284: 74 of cap free
Amount of items: 2
Items: 
Size: 791225 Color: 14
Size: 208702 Color: 17

Bin 285: 75 of cap free
Amount of items: 2
Items: 
Size: 546297 Color: 1
Size: 453629 Color: 8

Bin 286: 75 of cap free
Amount of items: 2
Items: 
Size: 725927 Color: 17
Size: 273999 Color: 10

Bin 287: 76 of cap free
Amount of items: 2
Items: 
Size: 600887 Color: 8
Size: 399038 Color: 5

Bin 288: 77 of cap free
Amount of items: 2
Items: 
Size: 565158 Color: 16
Size: 434766 Color: 8

Bin 289: 77 of cap free
Amount of items: 2
Items: 
Size: 666272 Color: 11
Size: 333652 Color: 3

Bin 290: 77 of cap free
Amount of items: 3
Items: 
Size: 367618 Color: 16
Size: 367022 Color: 15
Size: 265284 Color: 3

Bin 291: 78 of cap free
Amount of items: 3
Items: 
Size: 632631 Color: 0
Size: 219285 Color: 16
Size: 148007 Color: 4

Bin 292: 78 of cap free
Amount of items: 2
Items: 
Size: 603464 Color: 9
Size: 396459 Color: 3

Bin 293: 78 of cap free
Amount of items: 2
Items: 
Size: 624317 Color: 12
Size: 375606 Color: 10

Bin 294: 78 of cap free
Amount of items: 2
Items: 
Size: 750914 Color: 15
Size: 249009 Color: 13

Bin 295: 79 of cap free
Amount of items: 2
Items: 
Size: 663744 Color: 9
Size: 336178 Color: 15

Bin 296: 80 of cap free
Amount of items: 3
Items: 
Size: 404089 Color: 2
Size: 306122 Color: 0
Size: 289710 Color: 16

Bin 297: 81 of cap free
Amount of items: 2
Items: 
Size: 623443 Color: 14
Size: 376477 Color: 4

Bin 298: 81 of cap free
Amount of items: 2
Items: 
Size: 764507 Color: 8
Size: 235413 Color: 15

Bin 299: 82 of cap free
Amount of items: 2
Items: 
Size: 794432 Color: 19
Size: 205487 Color: 9

Bin 300: 82 of cap free
Amount of items: 2
Items: 
Size: 548204 Color: 10
Size: 451715 Color: 5

Bin 301: 82 of cap free
Amount of items: 2
Items: 
Size: 636625 Color: 17
Size: 363294 Color: 9

Bin 302: 83 of cap free
Amount of items: 3
Items: 
Size: 459767 Color: 3
Size: 274662 Color: 17
Size: 265489 Color: 0

Bin 303: 84 of cap free
Amount of items: 2
Items: 
Size: 798506 Color: 13
Size: 201411 Color: 18

Bin 304: 85 of cap free
Amount of items: 3
Items: 
Size: 733232 Color: 5
Size: 139035 Color: 3
Size: 127649 Color: 17

Bin 305: 85 of cap free
Amount of items: 3
Items: 
Size: 624764 Color: 4
Size: 192058 Color: 2
Size: 183094 Color: 5

Bin 306: 85 of cap free
Amount of items: 2
Items: 
Size: 702735 Color: 8
Size: 297181 Color: 11

Bin 307: 88 of cap free
Amount of items: 2
Items: 
Size: 668361 Color: 13
Size: 331552 Color: 18

Bin 308: 89 of cap free
Amount of items: 2
Items: 
Size: 618302 Color: 11
Size: 381610 Color: 16

Bin 309: 89 of cap free
Amount of items: 2
Items: 
Size: 789629 Color: 6
Size: 210283 Color: 2

Bin 310: 89 of cap free
Amount of items: 2
Items: 
Size: 775345 Color: 17
Size: 224567 Color: 1

Bin 311: 90 of cap free
Amount of items: 3
Items: 
Size: 589476 Color: 9
Size: 219914 Color: 15
Size: 190521 Color: 13

Bin 312: 91 of cap free
Amount of items: 3
Items: 
Size: 608830 Color: 4
Size: 197594 Color: 6
Size: 193486 Color: 0

Bin 313: 91 of cap free
Amount of items: 3
Items: 
Size: 408144 Color: 12
Size: 310406 Color: 13
Size: 281360 Color: 11

Bin 314: 91 of cap free
Amount of items: 3
Items: 
Size: 675458 Color: 0
Size: 206023 Color: 4
Size: 118429 Color: 1

Bin 315: 91 of cap free
Amount of items: 2
Items: 
Size: 506668 Color: 17
Size: 493242 Color: 9

Bin 316: 92 of cap free
Amount of items: 2
Items: 
Size: 649887 Color: 16
Size: 350022 Color: 5

Bin 317: 92 of cap free
Amount of items: 2
Items: 
Size: 762118 Color: 8
Size: 237791 Color: 10

Bin 318: 95 of cap free
Amount of items: 2
Items: 
Size: 507653 Color: 13
Size: 492253 Color: 7

Bin 319: 96 of cap free
Amount of items: 2
Items: 
Size: 601946 Color: 17
Size: 397959 Color: 8

Bin 320: 97 of cap free
Amount of items: 2
Items: 
Size: 792983 Color: 13
Size: 206921 Color: 12

Bin 321: 97 of cap free
Amount of items: 3
Items: 
Size: 452339 Color: 1
Size: 283858 Color: 7
Size: 263707 Color: 17

Bin 322: 98 of cap free
Amount of items: 2
Items: 
Size: 707906 Color: 13
Size: 291997 Color: 4

Bin 323: 99 of cap free
Amount of items: 2
Items: 
Size: 761637 Color: 4
Size: 238265 Color: 13

Bin 324: 100 of cap free
Amount of items: 2
Items: 
Size: 779032 Color: 8
Size: 220869 Color: 14

Bin 325: 101 of cap free
Amount of items: 3
Items: 
Size: 718800 Color: 0
Size: 154778 Color: 18
Size: 126322 Color: 9

Bin 326: 101 of cap free
Amount of items: 2
Items: 
Size: 501656 Color: 0
Size: 498244 Color: 4

Bin 327: 101 of cap free
Amount of items: 3
Items: 
Size: 337472 Color: 16
Size: 332446 Color: 8
Size: 329982 Color: 7

Bin 328: 102 of cap free
Amount of items: 2
Items: 
Size: 709847 Color: 8
Size: 290052 Color: 19

Bin 329: 103 of cap free
Amount of items: 2
Items: 
Size: 546173 Color: 7
Size: 453725 Color: 4

Bin 330: 103 of cap free
Amount of items: 2
Items: 
Size: 779559 Color: 2
Size: 220339 Color: 12

Bin 331: 105 of cap free
Amount of items: 2
Items: 
Size: 594188 Color: 9
Size: 405708 Color: 17

Bin 332: 106 of cap free
Amount of items: 2
Items: 
Size: 669644 Color: 14
Size: 330251 Color: 6

Bin 333: 107 of cap free
Amount of items: 2
Items: 
Size: 725524 Color: 17
Size: 274370 Color: 13

Bin 334: 107 of cap free
Amount of items: 2
Items: 
Size: 728365 Color: 9
Size: 271529 Color: 8

Bin 335: 107 of cap free
Amount of items: 2
Items: 
Size: 621545 Color: 13
Size: 378349 Color: 6

Bin 336: 108 of cap free
Amount of items: 2
Items: 
Size: 613691 Color: 6
Size: 386202 Color: 12

Bin 337: 109 of cap free
Amount of items: 2
Items: 
Size: 549507 Color: 16
Size: 450385 Color: 19

Bin 338: 109 of cap free
Amount of items: 2
Items: 
Size: 609614 Color: 2
Size: 390278 Color: 7

Bin 339: 109 of cap free
Amount of items: 2
Items: 
Size: 742824 Color: 9
Size: 257068 Color: 12

Bin 340: 112 of cap free
Amount of items: 2
Items: 
Size: 709084 Color: 4
Size: 290805 Color: 15

Bin 341: 113 of cap free
Amount of items: 2
Items: 
Size: 629317 Color: 1
Size: 370571 Color: 13

Bin 342: 113 of cap free
Amount of items: 2
Items: 
Size: 701700 Color: 4
Size: 298188 Color: 12

Bin 343: 114 of cap free
Amount of items: 3
Items: 
Size: 391263 Color: 7
Size: 384359 Color: 18
Size: 224265 Color: 15

Bin 344: 117 of cap free
Amount of items: 2
Items: 
Size: 633548 Color: 9
Size: 366336 Color: 4

Bin 345: 119 of cap free
Amount of items: 2
Items: 
Size: 572715 Color: 13
Size: 427167 Color: 15

Bin 346: 119 of cap free
Amount of items: 3
Items: 
Size: 509275 Color: 17
Size: 268339 Color: 1
Size: 222268 Color: 9

Bin 347: 120 of cap free
Amount of items: 2
Items: 
Size: 706404 Color: 12
Size: 293477 Color: 18

Bin 348: 121 of cap free
Amount of items: 2
Items: 
Size: 544564 Color: 13
Size: 455316 Color: 14

Bin 349: 123 of cap free
Amount of items: 3
Items: 
Size: 685023 Color: 1
Size: 158353 Color: 16
Size: 156502 Color: 1

Bin 350: 123 of cap free
Amount of items: 2
Items: 
Size: 524030 Color: 6
Size: 475848 Color: 0

Bin 351: 123 of cap free
Amount of items: 2
Items: 
Size: 578101 Color: 14
Size: 421777 Color: 5

Bin 352: 123 of cap free
Amount of items: 2
Items: 
Size: 674793 Color: 2
Size: 325085 Color: 14

Bin 353: 124 of cap free
Amount of items: 2
Items: 
Size: 542704 Color: 0
Size: 457173 Color: 18

Bin 354: 125 of cap free
Amount of items: 2
Items: 
Size: 551974 Color: 4
Size: 447902 Color: 17

Bin 355: 125 of cap free
Amount of items: 2
Items: 
Size: 699945 Color: 9
Size: 299931 Color: 17

Bin 356: 126 of cap free
Amount of items: 3
Items: 
Size: 372584 Color: 5
Size: 314823 Color: 14
Size: 312468 Color: 10

Bin 357: 127 of cap free
Amount of items: 2
Items: 
Size: 712672 Color: 1
Size: 287202 Color: 16

Bin 358: 128 of cap free
Amount of items: 2
Items: 
Size: 538269 Color: 3
Size: 461604 Color: 13

Bin 359: 131 of cap free
Amount of items: 2
Items: 
Size: 656922 Color: 13
Size: 342948 Color: 9

Bin 360: 131 of cap free
Amount of items: 2
Items: 
Size: 762933 Color: 3
Size: 236937 Color: 10

Bin 361: 131 of cap free
Amount of items: 2
Items: 
Size: 577183 Color: 17
Size: 422687 Color: 9

Bin 362: 132 of cap free
Amount of items: 2
Items: 
Size: 770540 Color: 10
Size: 229329 Color: 8

Bin 363: 133 of cap free
Amount of items: 2
Items: 
Size: 559605 Color: 11
Size: 440263 Color: 14

Bin 364: 133 of cap free
Amount of items: 2
Items: 
Size: 660846 Color: 16
Size: 339022 Color: 8

Bin 365: 134 of cap free
Amount of items: 2
Items: 
Size: 614293 Color: 5
Size: 385574 Color: 13

Bin 366: 134 of cap free
Amount of items: 2
Items: 
Size: 794992 Color: 13
Size: 204875 Color: 2

Bin 367: 136 of cap free
Amount of items: 2
Items: 
Size: 719261 Color: 7
Size: 280604 Color: 15

Bin 368: 139 of cap free
Amount of items: 2
Items: 
Size: 652293 Color: 13
Size: 347569 Color: 9

Bin 369: 139 of cap free
Amount of items: 3
Items: 
Size: 610314 Color: 1
Size: 199223 Color: 12
Size: 190325 Color: 16

Bin 370: 139 of cap free
Amount of items: 2
Items: 
Size: 651769 Color: 7
Size: 348093 Color: 17

Bin 371: 140 of cap free
Amount of items: 2
Items: 
Size: 790135 Color: 14
Size: 209726 Color: 0

Bin 372: 142 of cap free
Amount of items: 2
Items: 
Size: 535274 Color: 18
Size: 464585 Color: 9

Bin 373: 144 of cap free
Amount of items: 2
Items: 
Size: 727793 Color: 5
Size: 272064 Color: 4

Bin 374: 144 of cap free
Amount of items: 2
Items: 
Size: 645733 Color: 5
Size: 354124 Color: 0

Bin 375: 145 of cap free
Amount of items: 3
Items: 
Size: 691983 Color: 11
Size: 170309 Color: 13
Size: 137564 Color: 6

Bin 376: 148 of cap free
Amount of items: 2
Items: 
Size: 577005 Color: 1
Size: 422848 Color: 8

Bin 377: 148 of cap free
Amount of items: 3
Items: 
Size: 350991 Color: 15
Size: 344878 Color: 19
Size: 303984 Color: 19

Bin 378: 149 of cap free
Amount of items: 2
Items: 
Size: 573649 Color: 8
Size: 426203 Color: 0

Bin 379: 150 of cap free
Amount of items: 2
Items: 
Size: 507253 Color: 10
Size: 492598 Color: 5

Bin 380: 150 of cap free
Amount of items: 2
Items: 
Size: 670762 Color: 5
Size: 329089 Color: 17

Bin 381: 150 of cap free
Amount of items: 2
Items: 
Size: 763278 Color: 1
Size: 236573 Color: 12

Bin 382: 156 of cap free
Amount of items: 2
Items: 
Size: 673787 Color: 16
Size: 326058 Color: 10

Bin 383: 157 of cap free
Amount of items: 2
Items: 
Size: 683854 Color: 19
Size: 315990 Color: 8

Bin 384: 157 of cap free
Amount of items: 2
Items: 
Size: 619214 Color: 14
Size: 380630 Color: 3

Bin 385: 158 of cap free
Amount of items: 2
Items: 
Size: 747466 Color: 16
Size: 252377 Color: 7

Bin 386: 161 of cap free
Amount of items: 2
Items: 
Size: 708801 Color: 6
Size: 291039 Color: 2

Bin 387: 162 of cap free
Amount of items: 2
Items: 
Size: 566541 Color: 4
Size: 433298 Color: 14

Bin 388: 163 of cap free
Amount of items: 3
Items: 
Size: 347054 Color: 6
Size: 332665 Color: 18
Size: 320119 Color: 1

Bin 389: 165 of cap free
Amount of items: 2
Items: 
Size: 720408 Color: 3
Size: 279428 Color: 8

Bin 390: 165 of cap free
Amount of items: 2
Items: 
Size: 541159 Color: 1
Size: 458677 Color: 7

Bin 391: 167 of cap free
Amount of items: 2
Items: 
Size: 682076 Color: 19
Size: 317758 Color: 13

Bin 392: 167 of cap free
Amount of items: 2
Items: 
Size: 694203 Color: 12
Size: 305631 Color: 16

Bin 393: 168 of cap free
Amount of items: 3
Items: 
Size: 456223 Color: 9
Size: 278867 Color: 8
Size: 264743 Color: 4

Bin 394: 168 of cap free
Amount of items: 2
Items: 
Size: 572517 Color: 1
Size: 427316 Color: 2

Bin 395: 168 of cap free
Amount of items: 2
Items: 
Size: 739529 Color: 8
Size: 260304 Color: 0

Bin 396: 171 of cap free
Amount of items: 2
Items: 
Size: 692307 Color: 1
Size: 307523 Color: 9

Bin 397: 172 of cap free
Amount of items: 2
Items: 
Size: 622139 Color: 3
Size: 377690 Color: 17

Bin 398: 173 of cap free
Amount of items: 2
Items: 
Size: 509728 Color: 8
Size: 490100 Color: 19

Bin 399: 173 of cap free
Amount of items: 2
Items: 
Size: 741729 Color: 0
Size: 258099 Color: 2

Bin 400: 175 of cap free
Amount of items: 3
Items: 
Size: 621712 Color: 7
Size: 192763 Color: 17
Size: 185351 Color: 1

Bin 401: 176 of cap free
Amount of items: 2
Items: 
Size: 575343 Color: 12
Size: 424482 Color: 18

Bin 402: 177 of cap free
Amount of items: 2
Items: 
Size: 584098 Color: 2
Size: 415726 Color: 1

Bin 403: 179 of cap free
Amount of items: 2
Items: 
Size: 635445 Color: 15
Size: 364377 Color: 9

Bin 404: 179 of cap free
Amount of items: 3
Items: 
Size: 676265 Color: 19
Size: 172728 Color: 12
Size: 150829 Color: 16

Bin 405: 180 of cap free
Amount of items: 2
Items: 
Size: 540341 Color: 7
Size: 459480 Color: 19

Bin 406: 181 of cap free
Amount of items: 2
Items: 
Size: 564679 Color: 3
Size: 435141 Color: 13

Bin 407: 181 of cap free
Amount of items: 2
Items: 
Size: 630536 Color: 16
Size: 369284 Color: 3

Bin 408: 184 of cap free
Amount of items: 2
Items: 
Size: 579210 Color: 5
Size: 420607 Color: 9

Bin 409: 185 of cap free
Amount of items: 3
Items: 
Size: 344266 Color: 15
Size: 342361 Color: 19
Size: 313189 Color: 12

Bin 410: 187 of cap free
Amount of items: 2
Items: 
Size: 697295 Color: 10
Size: 302519 Color: 19

Bin 411: 188 of cap free
Amount of items: 2
Items: 
Size: 500603 Color: 12
Size: 499210 Color: 17

Bin 412: 189 of cap free
Amount of items: 3
Items: 
Size: 687682 Color: 17
Size: 167925 Color: 0
Size: 144205 Color: 13

Bin 413: 193 of cap free
Amount of items: 2
Items: 
Size: 567500 Color: 6
Size: 432308 Color: 16

Bin 414: 193 of cap free
Amount of items: 2
Items: 
Size: 572120 Color: 12
Size: 427688 Color: 6

Bin 415: 194 of cap free
Amount of items: 2
Items: 
Size: 607978 Color: 19
Size: 391829 Color: 3

Bin 416: 194 of cap free
Amount of items: 2
Items: 
Size: 563218 Color: 19
Size: 436589 Color: 18

Bin 417: 196 of cap free
Amount of items: 2
Items: 
Size: 678672 Color: 1
Size: 321133 Color: 5

Bin 418: 197 of cap free
Amount of items: 2
Items: 
Size: 543312 Color: 6
Size: 456492 Color: 8

Bin 419: 197 of cap free
Amount of items: 2
Items: 
Size: 616816 Color: 10
Size: 382988 Color: 18

Bin 420: 197 of cap free
Amount of items: 2
Items: 
Size: 766414 Color: 13
Size: 233390 Color: 2

Bin 421: 197 of cap free
Amount of items: 2
Items: 
Size: 522003 Color: 18
Size: 477801 Color: 6

Bin 422: 197 of cap free
Amount of items: 2
Items: 
Size: 778082 Color: 13
Size: 221722 Color: 17

Bin 423: 198 of cap free
Amount of items: 2
Items: 
Size: 651119 Color: 6
Size: 348684 Color: 19

Bin 424: 198 of cap free
Amount of items: 2
Items: 
Size: 518542 Color: 19
Size: 481261 Color: 1

Bin 425: 200 of cap free
Amount of items: 2
Items: 
Size: 529338 Color: 3
Size: 470463 Color: 5

Bin 426: 201 of cap free
Amount of items: 2
Items: 
Size: 660821 Color: 5
Size: 338979 Color: 9

Bin 427: 202 of cap free
Amount of items: 2
Items: 
Size: 731951 Color: 16
Size: 267848 Color: 6

Bin 428: 203 of cap free
Amount of items: 2
Items: 
Size: 535228 Color: 1
Size: 464570 Color: 18

Bin 429: 206 of cap free
Amount of items: 2
Items: 
Size: 536106 Color: 16
Size: 463689 Color: 5

Bin 430: 208 of cap free
Amount of items: 2
Items: 
Size: 789112 Color: 4
Size: 210681 Color: 3

Bin 431: 210 of cap free
Amount of items: 2
Items: 
Size: 787185 Color: 2
Size: 212606 Color: 6

Bin 432: 212 of cap free
Amount of items: 2
Items: 
Size: 540641 Color: 13
Size: 459148 Color: 16

Bin 433: 212 of cap free
Amount of items: 2
Items: 
Size: 665004 Color: 17
Size: 334785 Color: 15

Bin 434: 213 of cap free
Amount of items: 2
Items: 
Size: 714998 Color: 14
Size: 284790 Color: 15

Bin 435: 213 of cap free
Amount of items: 2
Items: 
Size: 710442 Color: 13
Size: 289346 Color: 18

Bin 436: 215 of cap free
Amount of items: 2
Items: 
Size: 514791 Color: 15
Size: 484995 Color: 11

Bin 437: 216 of cap free
Amount of items: 2
Items: 
Size: 509940 Color: 6
Size: 489845 Color: 1

Bin 438: 216 of cap free
Amount of items: 2
Items: 
Size: 581904 Color: 0
Size: 417881 Color: 5

Bin 439: 217 of cap free
Amount of items: 2
Items: 
Size: 679159 Color: 12
Size: 320625 Color: 19

Bin 440: 218 of cap free
Amount of items: 2
Items: 
Size: 775849 Color: 14
Size: 223934 Color: 3

Bin 441: 218 of cap free
Amount of items: 2
Items: 
Size: 781766 Color: 3
Size: 218017 Color: 9

Bin 442: 218 of cap free
Amount of items: 2
Items: 
Size: 774596 Color: 5
Size: 225187 Color: 19

Bin 443: 219 of cap free
Amount of items: 2
Items: 
Size: 754464 Color: 4
Size: 245318 Color: 11

Bin 444: 221 of cap free
Amount of items: 2
Items: 
Size: 647766 Color: 11
Size: 352014 Color: 1

Bin 445: 221 of cap free
Amount of items: 3
Items: 
Size: 401646 Color: 9
Size: 316804 Color: 18
Size: 281330 Color: 14

Bin 446: 224 of cap free
Amount of items: 2
Items: 
Size: 617355 Color: 14
Size: 382422 Color: 6

Bin 447: 224 of cap free
Amount of items: 2
Items: 
Size: 511873 Color: 8
Size: 487904 Color: 15

Bin 448: 228 of cap free
Amount of items: 2
Items: 
Size: 737947 Color: 3
Size: 261826 Color: 6

Bin 449: 229 of cap free
Amount of items: 2
Items: 
Size: 680013 Color: 2
Size: 319759 Color: 8

Bin 450: 229 of cap free
Amount of items: 2
Items: 
Size: 796803 Color: 13
Size: 202969 Color: 12

Bin 451: 230 of cap free
Amount of items: 2
Items: 
Size: 659434 Color: 8
Size: 340337 Color: 19

Bin 452: 230 of cap free
Amount of items: 2
Items: 
Size: 782253 Color: 13
Size: 217518 Color: 6

Bin 453: 232 of cap free
Amount of items: 2
Items: 
Size: 744828 Color: 4
Size: 254941 Color: 13

Bin 454: 232 of cap free
Amount of items: 2
Items: 
Size: 519177 Color: 7
Size: 480592 Color: 5

Bin 455: 232 of cap free
Amount of items: 2
Items: 
Size: 507597 Color: 8
Size: 492172 Color: 17

Bin 456: 233 of cap free
Amount of items: 2
Items: 
Size: 713482 Color: 4
Size: 286286 Color: 16

Bin 457: 235 of cap free
Amount of items: 2
Items: 
Size: 762247 Color: 7
Size: 237519 Color: 11

Bin 458: 237 of cap free
Amount of items: 2
Items: 
Size: 503900 Color: 11
Size: 495864 Color: 0

Bin 459: 239 of cap free
Amount of items: 2
Items: 
Size: 558636 Color: 11
Size: 441126 Color: 6

Bin 460: 243 of cap free
Amount of items: 2
Items: 
Size: 537464 Color: 3
Size: 462294 Color: 7

Bin 461: 245 of cap free
Amount of items: 3
Items: 
Size: 656849 Color: 19
Size: 181726 Color: 17
Size: 161181 Color: 4

Bin 462: 245 of cap free
Amount of items: 2
Items: 
Size: 783588 Color: 7
Size: 216168 Color: 9

Bin 463: 246 of cap free
Amount of items: 2
Items: 
Size: 725128 Color: 3
Size: 274627 Color: 1

Bin 464: 247 of cap free
Amount of items: 2
Items: 
Size: 792810 Color: 2
Size: 206944 Color: 13

Bin 465: 252 of cap free
Amount of items: 2
Items: 
Size: 578269 Color: 2
Size: 421480 Color: 13

Bin 466: 252 of cap free
Amount of items: 2
Items: 
Size: 585268 Color: 2
Size: 414481 Color: 18

Bin 467: 252 of cap free
Amount of items: 2
Items: 
Size: 602050 Color: 17
Size: 397699 Color: 13

Bin 468: 253 of cap free
Amount of items: 3
Items: 
Size: 392823 Color: 17
Size: 305339 Color: 8
Size: 301586 Color: 6

Bin 469: 256 of cap free
Amount of items: 2
Items: 
Size: 553942 Color: 17
Size: 445803 Color: 13

Bin 470: 258 of cap free
Amount of items: 2
Items: 
Size: 760090 Color: 15
Size: 239653 Color: 11

Bin 471: 261 of cap free
Amount of items: 2
Items: 
Size: 550323 Color: 19
Size: 449417 Color: 6

Bin 472: 261 of cap free
Amount of items: 2
Items: 
Size: 585260 Color: 10
Size: 414480 Color: 18

Bin 473: 263 of cap free
Amount of items: 2
Items: 
Size: 753977 Color: 19
Size: 245761 Color: 17

Bin 474: 267 of cap free
Amount of items: 2
Items: 
Size: 565527 Color: 11
Size: 434207 Color: 18

Bin 475: 268 of cap free
Amount of items: 2
Items: 
Size: 625686 Color: 16
Size: 374047 Color: 6

Bin 476: 268 of cap free
Amount of items: 2
Items: 
Size: 738890 Color: 19
Size: 260843 Color: 6

Bin 477: 269 of cap free
Amount of items: 2
Items: 
Size: 603582 Color: 0
Size: 396150 Color: 10

Bin 478: 274 of cap free
Amount of items: 2
Items: 
Size: 543697 Color: 9
Size: 456030 Color: 19

Bin 479: 276 of cap free
Amount of items: 2
Items: 
Size: 539541 Color: 2
Size: 460184 Color: 6

Bin 480: 277 of cap free
Amount of items: 2
Items: 
Size: 577572 Color: 15
Size: 422152 Color: 14

Bin 481: 281 of cap free
Amount of items: 2
Items: 
Size: 568483 Color: 17
Size: 431237 Color: 12

Bin 482: 281 of cap free
Amount of items: 2
Items: 
Size: 741170 Color: 12
Size: 258550 Color: 13

Bin 483: 282 of cap free
Amount of items: 2
Items: 
Size: 640159 Color: 1
Size: 359560 Color: 10

Bin 484: 284 of cap free
Amount of items: 2
Items: 
Size: 607254 Color: 18
Size: 392463 Color: 17

Bin 485: 285 of cap free
Amount of items: 2
Items: 
Size: 691461 Color: 10
Size: 308255 Color: 9

Bin 486: 286 of cap free
Amount of items: 2
Items: 
Size: 618843 Color: 7
Size: 380872 Color: 6

Bin 487: 289 of cap free
Amount of items: 2
Items: 
Size: 602458 Color: 16
Size: 397254 Color: 4

Bin 488: 290 of cap free
Amount of items: 2
Items: 
Size: 661533 Color: 14
Size: 338178 Color: 10

Bin 489: 290 of cap free
Amount of items: 2
Items: 
Size: 662247 Color: 10
Size: 337464 Color: 5

Bin 490: 290 of cap free
Amount of items: 2
Items: 
Size: 502913 Color: 19
Size: 496798 Color: 2

Bin 491: 291 of cap free
Amount of items: 2
Items: 
Size: 614612 Color: 1
Size: 385098 Color: 2

Bin 492: 295 of cap free
Amount of items: 2
Items: 
Size: 664484 Color: 8
Size: 335222 Color: 1

Bin 493: 295 of cap free
Amount of items: 2
Items: 
Size: 733923 Color: 7
Size: 265783 Color: 6

Bin 494: 297 of cap free
Amount of items: 2
Items: 
Size: 575705 Color: 4
Size: 423999 Color: 12

Bin 495: 299 of cap free
Amount of items: 2
Items: 
Size: 672624 Color: 2
Size: 327078 Color: 17

Bin 496: 300 of cap free
Amount of items: 2
Items: 
Size: 745146 Color: 14
Size: 254555 Color: 8

Bin 497: 301 of cap free
Amount of items: 2
Items: 
Size: 659755 Color: 3
Size: 339945 Color: 15

Bin 498: 308 of cap free
Amount of items: 2
Items: 
Size: 513433 Color: 4
Size: 486260 Color: 8

Bin 499: 309 of cap free
Amount of items: 2
Items: 
Size: 758530 Color: 3
Size: 241162 Color: 13

Bin 500: 310 of cap free
Amount of items: 2
Items: 
Size: 691896 Color: 2
Size: 307795 Color: 6

Bin 501: 311 of cap free
Amount of items: 2
Items: 
Size: 501857 Color: 14
Size: 497833 Color: 15

Bin 502: 311 of cap free
Amount of items: 2
Items: 
Size: 596808 Color: 5
Size: 402882 Color: 8

Bin 503: 312 of cap free
Amount of items: 2
Items: 
Size: 716816 Color: 6
Size: 282873 Color: 12

Bin 504: 321 of cap free
Amount of items: 2
Items: 
Size: 761271 Color: 15
Size: 238409 Color: 11

Bin 505: 322 of cap free
Amount of items: 2
Items: 
Size: 766359 Color: 13
Size: 233320 Color: 19

Bin 506: 324 of cap free
Amount of items: 2
Items: 
Size: 583323 Color: 11
Size: 416354 Color: 10

Bin 507: 324 of cap free
Amount of items: 2
Items: 
Size: 526996 Color: 13
Size: 472681 Color: 2

Bin 508: 326 of cap free
Amount of items: 3
Items: 
Size: 472228 Color: 1
Size: 263900 Color: 5
Size: 263547 Color: 11

Bin 509: 327 of cap free
Amount of items: 3
Items: 
Size: 643170 Color: 2
Size: 180198 Color: 6
Size: 176306 Color: 5

Bin 510: 329 of cap free
Amount of items: 2
Items: 
Size: 513993 Color: 15
Size: 485679 Color: 8

Bin 511: 331 of cap free
Amount of items: 2
Items: 
Size: 665359 Color: 5
Size: 334311 Color: 9

Bin 512: 333 of cap free
Amount of items: 2
Items: 
Size: 617332 Color: 19
Size: 382336 Color: 18

Bin 513: 336 of cap free
Amount of items: 2
Items: 
Size: 705966 Color: 5
Size: 293699 Color: 18

Bin 514: 337 of cap free
Amount of items: 2
Items: 
Size: 585820 Color: 8
Size: 413844 Color: 17

Bin 515: 338 of cap free
Amount of items: 2
Items: 
Size: 554736 Color: 16
Size: 444927 Color: 7

Bin 516: 345 of cap free
Amount of items: 2
Items: 
Size: 588759 Color: 2
Size: 410897 Color: 11

Bin 517: 345 of cap free
Amount of items: 2
Items: 
Size: 759679 Color: 8
Size: 239977 Color: 7

Bin 518: 347 of cap free
Amount of items: 3
Items: 
Size: 366976 Color: 3
Size: 316720 Color: 5
Size: 315958 Color: 8

Bin 519: 348 of cap free
Amount of items: 2
Items: 
Size: 534692 Color: 16
Size: 464961 Color: 17

Bin 520: 350 of cap free
Amount of items: 2
Items: 
Size: 508106 Color: 17
Size: 491545 Color: 15

Bin 521: 351 of cap free
Amount of items: 2
Items: 
Size: 751330 Color: 8
Size: 248320 Color: 19

Bin 522: 356 of cap free
Amount of items: 2
Items: 
Size: 576328 Color: 1
Size: 423317 Color: 4

Bin 523: 356 of cap free
Amount of items: 2
Items: 
Size: 712519 Color: 6
Size: 287126 Color: 15

Bin 524: 359 of cap free
Amount of items: 2
Items: 
Size: 727360 Color: 11
Size: 272282 Color: 7

Bin 525: 359 of cap free
Amount of items: 3
Items: 
Size: 627757 Color: 17
Size: 189676 Color: 16
Size: 182209 Color: 18

Bin 526: 360 of cap free
Amount of items: 2
Items: 
Size: 707200 Color: 1
Size: 292441 Color: 11

Bin 527: 361 of cap free
Amount of items: 3
Items: 
Size: 614079 Color: 5
Size: 196548 Color: 15
Size: 189013 Color: 18

Bin 528: 362 of cap free
Amount of items: 2
Items: 
Size: 755240 Color: 4
Size: 244399 Color: 14

Bin 529: 363 of cap free
Amount of items: 2
Items: 
Size: 637482 Color: 14
Size: 362156 Color: 18

Bin 530: 363 of cap free
Amount of items: 2
Items: 
Size: 638251 Color: 10
Size: 361387 Color: 14

Bin 531: 364 of cap free
Amount of items: 2
Items: 
Size: 631261 Color: 14
Size: 368376 Color: 7

Bin 532: 367 of cap free
Amount of items: 2
Items: 
Size: 545387 Color: 12
Size: 454247 Color: 5

Bin 533: 370 of cap free
Amount of items: 2
Items: 
Size: 649844 Color: 0
Size: 349787 Color: 10

Bin 534: 376 of cap free
Amount of items: 2
Items: 
Size: 643747 Color: 10
Size: 355878 Color: 16

Bin 535: 377 of cap free
Amount of items: 2
Items: 
Size: 565479 Color: 10
Size: 434145 Color: 11

Bin 536: 379 of cap free
Amount of items: 2
Items: 
Size: 592565 Color: 10
Size: 407057 Color: 13

Bin 537: 384 of cap free
Amount of items: 2
Items: 
Size: 762689 Color: 2
Size: 236928 Color: 5

Bin 538: 385 of cap free
Amount of items: 2
Items: 
Size: 647684 Color: 13
Size: 351932 Color: 10

Bin 539: 386 of cap free
Amount of items: 2
Items: 
Size: 678570 Color: 2
Size: 321045 Color: 14

Bin 540: 393 of cap free
Amount of items: 2
Items: 
Size: 744749 Color: 4
Size: 254859 Color: 15

Bin 541: 394 of cap free
Amount of items: 2
Items: 
Size: 509267 Color: 13
Size: 490340 Color: 11

Bin 542: 398 of cap free
Amount of items: 2
Items: 
Size: 553875 Color: 2
Size: 445728 Color: 15

Bin 543: 399 of cap free
Amount of items: 2
Items: 
Size: 557884 Color: 2
Size: 441718 Color: 5

Bin 544: 399 of cap free
Amount of items: 2
Items: 
Size: 629607 Color: 8
Size: 369995 Color: 16

Bin 545: 406 of cap free
Amount of items: 2
Items: 
Size: 562408 Color: 6
Size: 437187 Color: 16

Bin 546: 410 of cap free
Amount of items: 2
Items: 
Size: 559372 Color: 19
Size: 440219 Color: 7

Bin 547: 414 of cap free
Amount of items: 2
Items: 
Size: 664902 Color: 10
Size: 334685 Color: 8

Bin 548: 419 of cap free
Amount of items: 2
Items: 
Size: 537726 Color: 16
Size: 461856 Color: 3

Bin 549: 422 of cap free
Amount of items: 2
Items: 
Size: 757739 Color: 15
Size: 241840 Color: 4

Bin 550: 425 of cap free
Amount of items: 2
Items: 
Size: 585106 Color: 14
Size: 414470 Color: 2

Bin 551: 427 of cap free
Amount of items: 2
Items: 
Size: 597928 Color: 5
Size: 401646 Color: 7

Bin 552: 434 of cap free
Amount of items: 2
Items: 
Size: 718321 Color: 1
Size: 281246 Color: 8

Bin 553: 434 of cap free
Amount of items: 2
Items: 
Size: 714336 Color: 3
Size: 285231 Color: 7

Bin 554: 434 of cap free
Amount of items: 2
Items: 
Size: 727181 Color: 18
Size: 272386 Color: 11

Bin 555: 435 of cap free
Amount of items: 2
Items: 
Size: 721661 Color: 16
Size: 277905 Color: 18

Bin 556: 436 of cap free
Amount of items: 2
Items: 
Size: 719809 Color: 6
Size: 279756 Color: 2

Bin 557: 442 of cap free
Amount of items: 2
Items: 
Size: 557236 Color: 19
Size: 442323 Color: 4

Bin 558: 444 of cap free
Amount of items: 2
Items: 
Size: 520558 Color: 2
Size: 478999 Color: 6

Bin 559: 446 of cap free
Amount of items: 2
Items: 
Size: 786974 Color: 17
Size: 212581 Color: 7

Bin 560: 447 of cap free
Amount of items: 2
Items: 
Size: 672985 Color: 17
Size: 326569 Color: 8

Bin 561: 448 of cap free
Amount of items: 2
Items: 
Size: 620409 Color: 19
Size: 379144 Color: 14

Bin 562: 449 of cap free
Amount of items: 2
Items: 
Size: 538706 Color: 11
Size: 460846 Color: 3

Bin 563: 451 of cap free
Amount of items: 2
Items: 
Size: 568911 Color: 12
Size: 430639 Color: 7

Bin 564: 455 of cap free
Amount of items: 2
Items: 
Size: 584035 Color: 0
Size: 415511 Color: 12

Bin 565: 456 of cap free
Amount of items: 2
Items: 
Size: 519142 Color: 2
Size: 480403 Color: 10

Bin 566: 458 of cap free
Amount of items: 3
Items: 
Size: 688748 Color: 12
Size: 178344 Color: 5
Size: 132451 Color: 1

Bin 567: 459 of cap free
Amount of items: 2
Items: 
Size: 595940 Color: 18
Size: 403602 Color: 0

Bin 568: 460 of cap free
Amount of items: 2
Items: 
Size: 574639 Color: 2
Size: 424902 Color: 17

Bin 569: 463 of cap free
Amount of items: 2
Items: 
Size: 664863 Color: 11
Size: 334675 Color: 8

Bin 570: 464 of cap free
Amount of items: 2
Items: 
Size: 764008 Color: 12
Size: 235529 Color: 9

Bin 571: 465 of cap free
Amount of items: 2
Items: 
Size: 766263 Color: 4
Size: 233273 Color: 6

Bin 572: 467 of cap free
Amount of items: 2
Items: 
Size: 518466 Color: 3
Size: 481068 Color: 2

Bin 573: 469 of cap free
Amount of items: 3
Items: 
Size: 663030 Color: 13
Size: 176270 Color: 8
Size: 160232 Color: 6

Bin 574: 470 of cap free
Amount of items: 2
Items: 
Size: 597427 Color: 9
Size: 402104 Color: 1

Bin 575: 474 of cap free
Amount of items: 2
Items: 
Size: 733387 Color: 12
Size: 266140 Color: 3

Bin 576: 483 of cap free
Amount of items: 2
Items: 
Size: 516468 Color: 3
Size: 483050 Color: 8

Bin 577: 484 of cap free
Amount of items: 2
Items: 
Size: 710911 Color: 8
Size: 288606 Color: 19

Bin 578: 485 of cap free
Amount of items: 2
Items: 
Size: 610809 Color: 5
Size: 388707 Color: 4

Bin 579: 486 of cap free
Amount of items: 2
Items: 
Size: 762614 Color: 1
Size: 236901 Color: 9

Bin 580: 492 of cap free
Amount of items: 2
Items: 
Size: 528656 Color: 7
Size: 470853 Color: 10

Bin 581: 492 of cap free
Amount of items: 2
Items: 
Size: 672984 Color: 0
Size: 326525 Color: 1

Bin 582: 493 of cap free
Amount of items: 2
Items: 
Size: 612036 Color: 5
Size: 387472 Color: 8

Bin 583: 495 of cap free
Amount of items: 2
Items: 
Size: 640097 Color: 0
Size: 359409 Color: 11

Bin 584: 496 of cap free
Amount of items: 2
Items: 
Size: 777417 Color: 5
Size: 222088 Color: 17

Bin 585: 496 of cap free
Amount of items: 2
Items: 
Size: 551231 Color: 4
Size: 448274 Color: 15

Bin 586: 497 of cap free
Amount of items: 2
Items: 
Size: 722640 Color: 9
Size: 276864 Color: 13

Bin 587: 499 of cap free
Amount of items: 2
Items: 
Size: 780547 Color: 8
Size: 218955 Color: 4

Bin 588: 501 of cap free
Amount of items: 2
Items: 
Size: 608828 Color: 4
Size: 390672 Color: 7

Bin 589: 513 of cap free
Amount of items: 2
Items: 
Size: 615280 Color: 15
Size: 384208 Color: 3

Bin 590: 515 of cap free
Amount of items: 2
Items: 
Size: 699625 Color: 10
Size: 299861 Color: 12

Bin 591: 521 of cap free
Amount of items: 2
Items: 
Size: 588689 Color: 2
Size: 410791 Color: 5

Bin 592: 522 of cap free
Amount of items: 2
Items: 
Size: 599761 Color: 14
Size: 399718 Color: 17

Bin 593: 522 of cap free
Amount of items: 2
Items: 
Size: 791931 Color: 17
Size: 207548 Color: 4

Bin 594: 530 of cap free
Amount of items: 2
Items: 
Size: 587178 Color: 13
Size: 412293 Color: 10

Bin 595: 539 of cap free
Amount of items: 2
Items: 
Size: 572688 Color: 13
Size: 426774 Color: 4

Bin 596: 546 of cap free
Amount of items: 2
Items: 
Size: 567349 Color: 9
Size: 432106 Color: 15

Bin 597: 550 of cap free
Amount of items: 2
Items: 
Size: 619847 Color: 16
Size: 379604 Color: 12

Bin 598: 552 of cap free
Amount of items: 2
Items: 
Size: 523082 Color: 3
Size: 476367 Color: 1

Bin 599: 559 of cap free
Amount of items: 2
Items: 
Size: 555454 Color: 19
Size: 443988 Color: 18

Bin 600: 559 of cap free
Amount of items: 2
Items: 
Size: 656707 Color: 3
Size: 342735 Color: 0

Bin 601: 563 of cap free
Amount of items: 2
Items: 
Size: 519062 Color: 1
Size: 480376 Color: 13

Bin 602: 567 of cap free
Amount of items: 2
Items: 
Size: 515138 Color: 5
Size: 484296 Color: 6

Bin 603: 579 of cap free
Amount of items: 2
Items: 
Size: 640669 Color: 17
Size: 358753 Color: 10

Bin 604: 580 of cap free
Amount of items: 2
Items: 
Size: 646788 Color: 19
Size: 352633 Color: 14

Bin 605: 586 of cap free
Amount of items: 2
Items: 
Size: 749837 Color: 10
Size: 249578 Color: 1

Bin 606: 588 of cap free
Amount of items: 2
Items: 
Size: 553788 Color: 10
Size: 445625 Color: 12

Bin 607: 593 of cap free
Amount of items: 2
Items: 
Size: 769910 Color: 15
Size: 229498 Color: 17

Bin 608: 596 of cap free
Amount of items: 2
Items: 
Size: 793562 Color: 3
Size: 205843 Color: 6

Bin 609: 600 of cap free
Amount of items: 2
Items: 
Size: 641542 Color: 15
Size: 357859 Color: 1

Bin 610: 606 of cap free
Amount of items: 2
Items: 
Size: 551743 Color: 18
Size: 447652 Color: 3

Bin 611: 611 of cap free
Amount of items: 2
Items: 
Size: 607710 Color: 14
Size: 391680 Color: 15

Bin 612: 612 of cap free
Amount of items: 2
Items: 
Size: 681657 Color: 14
Size: 317732 Color: 12

Bin 613: 616 of cap free
Amount of items: 2
Items: 
Size: 554699 Color: 1
Size: 444686 Color: 17

Bin 614: 619 of cap free
Amount of items: 2
Items: 
Size: 544175 Color: 9
Size: 455207 Color: 3

Bin 615: 623 of cap free
Amount of items: 2
Items: 
Size: 680394 Color: 13
Size: 318984 Color: 14

Bin 616: 626 of cap free
Amount of items: 2
Items: 
Size: 678368 Color: 17
Size: 321007 Color: 11

Bin 617: 627 of cap free
Amount of items: 2
Items: 
Size: 649811 Color: 6
Size: 349563 Color: 12

Bin 618: 630 of cap free
Amount of items: 2
Items: 
Size: 642232 Color: 15
Size: 357139 Color: 6

Bin 619: 632 of cap free
Amount of items: 2
Items: 
Size: 633985 Color: 6
Size: 365384 Color: 7

Bin 620: 635 of cap free
Amount of items: 2
Items: 
Size: 798344 Color: 14
Size: 201022 Color: 1

Bin 621: 646 of cap free
Amount of items: 2
Items: 
Size: 666976 Color: 18
Size: 332379 Color: 4

Bin 622: 648 of cap free
Amount of items: 2
Items: 
Size: 564298 Color: 5
Size: 435055 Color: 7

Bin 623: 649 of cap free
Amount of items: 2
Items: 
Size: 684753 Color: 4
Size: 314599 Color: 19

Bin 624: 660 of cap free
Amount of items: 2
Items: 
Size: 638086 Color: 0
Size: 361255 Color: 5

Bin 625: 673 of cap free
Amount of items: 2
Items: 
Size: 542862 Color: 8
Size: 456466 Color: 16

Bin 626: 675 of cap free
Amount of items: 2
Items: 
Size: 748025 Color: 0
Size: 251301 Color: 14

Bin 627: 680 of cap free
Amount of items: 2
Items: 
Size: 685735 Color: 8
Size: 313586 Color: 4

Bin 628: 685 of cap free
Amount of items: 2
Items: 
Size: 510189 Color: 16
Size: 489127 Color: 8

Bin 629: 695 of cap free
Amount of items: 2
Items: 
Size: 792495 Color: 2
Size: 206811 Color: 17

Bin 630: 700 of cap free
Amount of items: 2
Items: 
Size: 626519 Color: 4
Size: 372782 Color: 9

Bin 631: 704 of cap free
Amount of items: 2
Items: 
Size: 638103 Color: 5
Size: 361194 Color: 15

Bin 632: 708 of cap free
Amount of items: 2
Items: 
Size: 610006 Color: 0
Size: 389287 Color: 11

Bin 633: 709 of cap free
Amount of items: 2
Items: 
Size: 621679 Color: 5
Size: 377613 Color: 6

Bin 634: 718 of cap free
Amount of items: 2
Items: 
Size: 599834 Color: 17
Size: 399449 Color: 15

Bin 635: 720 of cap free
Amount of items: 2
Items: 
Size: 546039 Color: 1
Size: 453242 Color: 5

Bin 636: 727 of cap free
Amount of items: 2
Items: 
Size: 574566 Color: 5
Size: 424708 Color: 13

Bin 637: 734 of cap free
Amount of items: 2
Items: 
Size: 692705 Color: 3
Size: 306562 Color: 4

Bin 638: 734 of cap free
Amount of items: 2
Items: 
Size: 504773 Color: 17
Size: 494494 Color: 18

Bin 639: 735 of cap free
Amount of items: 2
Items: 
Size: 597644 Color: 12
Size: 401622 Color: 9

Bin 640: 737 of cap free
Amount of items: 2
Items: 
Size: 799471 Color: 8
Size: 199793 Color: 10

Bin 641: 740 of cap free
Amount of items: 3
Items: 
Size: 617296 Color: 12
Size: 195545 Color: 8
Size: 186420 Color: 4

Bin 642: 742 of cap free
Amount of items: 2
Items: 
Size: 783162 Color: 3
Size: 216097 Color: 6

Bin 643: 742 of cap free
Amount of items: 2
Items: 
Size: 708282 Color: 13
Size: 290977 Color: 3

Bin 644: 745 of cap free
Amount of items: 2
Items: 
Size: 531919 Color: 10
Size: 467337 Color: 16

Bin 645: 746 of cap free
Amount of items: 2
Items: 
Size: 573484 Color: 7
Size: 425771 Color: 3

Bin 646: 747 of cap free
Amount of items: 2
Items: 
Size: 507154 Color: 12
Size: 492100 Color: 0

Bin 647: 750 of cap free
Amount of items: 2
Items: 
Size: 593031 Color: 1
Size: 406220 Color: 7

Bin 648: 751 of cap free
Amount of items: 2
Items: 
Size: 753261 Color: 18
Size: 245989 Color: 19

Bin 649: 754 of cap free
Amount of items: 2
Items: 
Size: 768269 Color: 13
Size: 230978 Color: 14

Bin 650: 767 of cap free
Amount of items: 2
Items: 
Size: 558313 Color: 3
Size: 440921 Color: 12

Bin 651: 769 of cap free
Amount of items: 2
Items: 
Size: 520372 Color: 15
Size: 478860 Color: 9

Bin 652: 774 of cap free
Amount of items: 2
Items: 
Size: 676248 Color: 9
Size: 322979 Color: 14

Bin 653: 778 of cap free
Amount of items: 2
Items: 
Size: 507055 Color: 7
Size: 492168 Color: 12

Bin 654: 789 of cap free
Amount of items: 2
Items: 
Size: 605593 Color: 7
Size: 393619 Color: 6

Bin 655: 793 of cap free
Amount of items: 2
Items: 
Size: 577892 Color: 2
Size: 421316 Color: 19

Bin 656: 794 of cap free
Amount of items: 2
Items: 
Size: 533302 Color: 6
Size: 465905 Color: 8

Bin 657: 795 of cap free
Amount of items: 2
Items: 
Size: 581793 Color: 11
Size: 417413 Color: 16

Bin 658: 796 of cap free
Amount of items: 2
Items: 
Size: 527814 Color: 6
Size: 471391 Color: 5

Bin 659: 814 of cap free
Amount of items: 2
Items: 
Size: 637166 Color: 2
Size: 362021 Color: 17

Bin 660: 817 of cap free
Amount of items: 2
Items: 
Size: 758228 Color: 4
Size: 240956 Color: 0

Bin 661: 826 of cap free
Amount of items: 2
Items: 
Size: 748940 Color: 2
Size: 250235 Color: 13

Bin 662: 834 of cap free
Amount of items: 2
Items: 
Size: 666887 Color: 17
Size: 332280 Color: 8

Bin 663: 868 of cap free
Amount of items: 2
Items: 
Size: 611854 Color: 5
Size: 387279 Color: 6

Bin 664: 869 of cap free
Amount of items: 2
Items: 
Size: 655786 Color: 1
Size: 343346 Color: 2

Bin 665: 885 of cap free
Amount of items: 2
Items: 
Size: 630777 Color: 7
Size: 368339 Color: 15

Bin 666: 895 of cap free
Amount of items: 2
Items: 
Size: 649591 Color: 9
Size: 349515 Color: 19

Bin 667: 904 of cap free
Amount of items: 2
Items: 
Size: 752101 Color: 19
Size: 246996 Color: 4

Bin 668: 910 of cap free
Amount of items: 2
Items: 
Size: 659325 Color: 3
Size: 339766 Color: 2

Bin 669: 916 of cap free
Amount of items: 2
Items: 
Size: 568449 Color: 14
Size: 430636 Color: 18

Bin 670: 917 of cap free
Amount of items: 2
Items: 
Size: 524521 Color: 9
Size: 474563 Color: 19

Bin 671: 933 of cap free
Amount of items: 2
Items: 
Size: 671393 Color: 14
Size: 327675 Color: 7

Bin 672: 938 of cap free
Amount of items: 2
Items: 
Size: 564122 Color: 0
Size: 434941 Color: 13

Bin 673: 940 of cap free
Amount of items: 2
Items: 
Size: 630771 Color: 0
Size: 368290 Color: 14

Bin 674: 947 of cap free
Amount of items: 2
Items: 
Size: 533292 Color: 6
Size: 465762 Color: 3

Bin 675: 959 of cap free
Amount of items: 2
Items: 
Size: 681512 Color: 19
Size: 317530 Color: 10

Bin 676: 959 of cap free
Amount of items: 2
Items: 
Size: 654767 Color: 15
Size: 344275 Color: 2

Bin 677: 975 of cap free
Amount of items: 2
Items: 
Size: 617124 Color: 15
Size: 381902 Color: 12

Bin 678: 983 of cap free
Amount of items: 2
Items: 
Size: 601896 Color: 17
Size: 397122 Color: 14

Bin 679: 991 of cap free
Amount of items: 2
Items: 
Size: 759095 Color: 10
Size: 239915 Color: 9

Bin 680: 1001 of cap free
Amount of items: 2
Items: 
Size: 504652 Color: 8
Size: 494348 Color: 6

Bin 681: 1008 of cap free
Amount of items: 2
Items: 
Size: 590908 Color: 7
Size: 408085 Color: 17

Bin 682: 1022 of cap free
Amount of items: 2
Items: 
Size: 567182 Color: 10
Size: 431797 Color: 0

Bin 683: 1024 of cap free
Amount of items: 2
Items: 
Size: 567049 Color: 9
Size: 431928 Color: 10

Bin 684: 1025 of cap free
Amount of items: 2
Items: 
Size: 603418 Color: 16
Size: 395558 Color: 10

Bin 685: 1033 of cap free
Amount of items: 2
Items: 
Size: 664863 Color: 14
Size: 334105 Color: 12

Bin 686: 1040 of cap free
Amount of items: 2
Items: 
Size: 783138 Color: 16
Size: 215823 Color: 17

Bin 687: 1044 of cap free
Amount of items: 2
Items: 
Size: 594728 Color: 5
Size: 404229 Color: 4

Bin 688: 1045 of cap free
Amount of items: 2
Items: 
Size: 672440 Color: 2
Size: 326516 Color: 11

Bin 689: 1050 of cap free
Amount of items: 2
Items: 
Size: 500320 Color: 9
Size: 498631 Color: 16

Bin 690: 1053 of cap free
Amount of items: 2
Items: 
Size: 619836 Color: 13
Size: 379112 Color: 15

Bin 691: 1055 of cap free
Amount of items: 2
Items: 
Size: 581559 Color: 1
Size: 417387 Color: 7

Bin 692: 1063 of cap free
Amount of items: 2
Items: 
Size: 606558 Color: 14
Size: 392380 Color: 10

Bin 693: 1064 of cap free
Amount of items: 2
Items: 
Size: 714199 Color: 8
Size: 284738 Color: 0

Bin 694: 1066 of cap free
Amount of items: 2
Items: 
Size: 662899 Color: 7
Size: 336036 Color: 3

Bin 695: 1066 of cap free
Amount of items: 2
Items: 
Size: 529238 Color: 19
Size: 469697 Color: 1

Bin 696: 1089 of cap free
Amount of items: 2
Items: 
Size: 669877 Color: 6
Size: 329035 Color: 8

Bin 697: 1089 of cap free
Amount of items: 2
Items: 
Size: 583491 Color: 10
Size: 415421 Color: 5

Bin 698: 1112 of cap free
Amount of items: 2
Items: 
Size: 659212 Color: 16
Size: 339677 Color: 6

Bin 699: 1137 of cap free
Amount of items: 2
Items: 
Size: 627643 Color: 9
Size: 371221 Color: 15

Bin 700: 1152 of cap free
Amount of items: 2
Items: 
Size: 560798 Color: 12
Size: 438051 Color: 7

Bin 701: 1157 of cap free
Amount of items: 2
Items: 
Size: 775114 Color: 14
Size: 223730 Color: 6

Bin 702: 1167 of cap free
Amount of items: 2
Items: 
Size: 695840 Color: 2
Size: 302994 Color: 10

Bin 703: 1168 of cap free
Amount of items: 2
Items: 
Size: 535527 Color: 1
Size: 463306 Color: 16

Bin 704: 1178 of cap free
Amount of items: 2
Items: 
Size: 659173 Color: 3
Size: 339650 Color: 8

Bin 705: 1184 of cap free
Amount of items: 2
Items: 
Size: 709685 Color: 7
Size: 289132 Color: 3

Bin 706: 1214 of cap free
Amount of items: 2
Items: 
Size: 652027 Color: 7
Size: 346760 Color: 10

Bin 707: 1229 of cap free
Amount of items: 2
Items: 
Size: 738653 Color: 1
Size: 260119 Color: 2

Bin 708: 1232 of cap free
Amount of items: 2
Items: 
Size: 520126 Color: 1
Size: 478643 Color: 2

Bin 709: 1248 of cap free
Amount of items: 2
Items: 
Size: 649260 Color: 13
Size: 349493 Color: 7

Bin 710: 1265 of cap free
Amount of items: 2
Items: 
Size: 639394 Color: 14
Size: 359342 Color: 1

Bin 711: 1301 of cap free
Amount of items: 2
Items: 
Size: 524225 Color: 1
Size: 474475 Color: 17

Bin 712: 1323 of cap free
Amount of items: 2
Items: 
Size: 766816 Color: 19
Size: 231862 Color: 18

Bin 713: 1337 of cap free
Amount of items: 2
Items: 
Size: 571907 Color: 13
Size: 426757 Color: 11

Bin 714: 1337 of cap free
Amount of items: 2
Items: 
Size: 518313 Color: 0
Size: 480351 Color: 13

Bin 715: 1339 of cap free
Amount of items: 2
Items: 
Size: 786494 Color: 2
Size: 212168 Color: 9

Bin 716: 1339 of cap free
Amount of items: 2
Items: 
Size: 522501 Color: 8
Size: 476161 Color: 16

Bin 717: 1356 of cap free
Amount of items: 2
Items: 
Size: 728601 Color: 16
Size: 270044 Color: 5

Bin 718: 1364 of cap free
Amount of items: 2
Items: 
Size: 577564 Color: 9
Size: 421073 Color: 10

Bin 719: 1370 of cap free
Amount of items: 2
Items: 
Size: 675932 Color: 17
Size: 322699 Color: 15

Bin 720: 1374 of cap free
Amount of items: 2
Items: 
Size: 706763 Color: 6
Size: 291864 Color: 17

Bin 721: 1397 of cap free
Amount of items: 2
Items: 
Size: 713902 Color: 5
Size: 284702 Color: 11

Bin 722: 1418 of cap free
Amount of items: 2
Items: 
Size: 769798 Color: 0
Size: 228785 Color: 15

Bin 723: 1422 of cap free
Amount of items: 2
Items: 
Size: 726303 Color: 15
Size: 272276 Color: 2

Bin 724: 1428 of cap free
Amount of items: 2
Items: 
Size: 538231 Color: 14
Size: 460342 Color: 2

Bin 725: 1436 of cap free
Amount of items: 2
Items: 
Size: 583153 Color: 0
Size: 415412 Color: 7

Bin 726: 1468 of cap free
Amount of items: 2
Items: 
Size: 646769 Color: 10
Size: 351764 Color: 15

Bin 727: 1485 of cap free
Amount of items: 2
Items: 
Size: 679992 Color: 0
Size: 318524 Color: 5

Bin 728: 1487 of cap free
Amount of items: 2
Items: 
Size: 592498 Color: 9
Size: 406016 Color: 4

Bin 729: 1493 of cap free
Amount of items: 2
Items: 
Size: 623043 Color: 3
Size: 375465 Color: 16

Bin 730: 1504 of cap free
Amount of items: 2
Items: 
Size: 583085 Color: 4
Size: 415412 Color: 12

Bin 731: 1536 of cap free
Amount of items: 2
Items: 
Size: 675907 Color: 5
Size: 322558 Color: 1

Bin 732: 1537 of cap free
Amount of items: 2
Items: 
Size: 499904 Color: 7
Size: 498560 Color: 18

Bin 733: 1550 of cap free
Amount of items: 2
Items: 
Size: 744182 Color: 5
Size: 254269 Color: 0

Bin 734: 1579 of cap free
Amount of items: 2
Items: 
Size: 512879 Color: 3
Size: 485543 Color: 10

Bin 735: 1596 of cap free
Amount of items: 2
Items: 
Size: 565108 Color: 13
Size: 433297 Color: 3

Bin 736: 1599 of cap free
Amount of items: 2
Items: 
Size: 571782 Color: 19
Size: 426620 Color: 16

Bin 737: 1603 of cap free
Amount of items: 2
Items: 
Size: 504257 Color: 17
Size: 494141 Color: 18

Bin 738: 1615 of cap free
Amount of items: 2
Items: 
Size: 775124 Color: 6
Size: 223262 Color: 19

Bin 739: 1631 of cap free
Amount of items: 2
Items: 
Size: 702393 Color: 2
Size: 295977 Color: 7

Bin 740: 1650 of cap free
Amount of items: 2
Items: 
Size: 592407 Color: 5
Size: 405944 Color: 15

Bin 741: 1652 of cap free
Amount of items: 2
Items: 
Size: 619594 Color: 14
Size: 378755 Color: 8

Bin 742: 1664 of cap free
Amount of items: 2
Items: 
Size: 552779 Color: 15
Size: 445558 Color: 12

Bin 743: 1678 of cap free
Amount of items: 2
Items: 
Size: 630394 Color: 7
Size: 367929 Color: 11

Bin 744: 1682 of cap free
Amount of items: 2
Items: 
Size: 548316 Color: 7
Size: 450003 Color: 14

Bin 745: 1730 of cap free
Amount of items: 2
Items: 
Size: 746682 Color: 2
Size: 251589 Color: 0

Bin 746: 1734 of cap free
Amount of items: 2
Items: 
Size: 506352 Color: 1
Size: 491915 Color: 6

Bin 747: 1735 of cap free
Amount of items: 2
Items: 
Size: 684736 Color: 13
Size: 313530 Color: 5

Bin 748: 1737 of cap free
Amount of items: 2
Items: 
Size: 569682 Color: 18
Size: 428582 Color: 5

Bin 749: 1740 of cap free
Amount of items: 2
Items: 
Size: 726263 Color: 1
Size: 271998 Color: 13

Bin 750: 1741 of cap free
Amount of items: 2
Items: 
Size: 520213 Color: 2
Size: 478047 Color: 4

Bin 751: 1757 of cap free
Amount of items: 2
Items: 
Size: 730487 Color: 2
Size: 267757 Color: 13

Bin 752: 1759 of cap free
Amount of items: 2
Items: 
Size: 603376 Color: 17
Size: 394866 Color: 2

Bin 753: 1762 of cap free
Amount of items: 2
Items: 
Size: 612761 Color: 12
Size: 385478 Color: 7

Bin 754: 1799 of cap free
Amount of items: 2
Items: 
Size: 786065 Color: 12
Size: 212137 Color: 3

Bin 755: 1824 of cap free
Amount of items: 2
Items: 
Size: 751327 Color: 17
Size: 246850 Color: 12

Bin 756: 1858 of cap free
Amount of items: 2
Items: 
Size: 658572 Color: 3
Size: 339571 Color: 12

Bin 757: 1868 of cap free
Amount of items: 2
Items: 
Size: 588606 Color: 0
Size: 409527 Color: 16

Bin 758: 1878 of cap free
Amount of items: 2
Items: 
Size: 740669 Color: 14
Size: 257454 Color: 13

Bin 759: 1895 of cap free
Amount of items: 2
Items: 
Size: 664367 Color: 10
Size: 333739 Color: 2

Bin 760: 1928 of cap free
Amount of items: 2
Items: 
Size: 601335 Color: 19
Size: 396738 Color: 11

Bin 761: 1938 of cap free
Amount of items: 2
Items: 
Size: 798041 Color: 6
Size: 200022 Color: 8

Bin 762: 1944 of cap free
Amount of items: 3
Items: 
Size: 630271 Color: 12
Size: 215195 Color: 8
Size: 152591 Color: 15

Bin 763: 1973 of cap free
Amount of items: 2
Items: 
Size: 675898 Color: 7
Size: 322130 Color: 19

Bin 764: 1983 of cap free
Amount of items: 2
Items: 
Size: 630270 Color: 10
Size: 367748 Color: 14

Bin 765: 1993 of cap free
Amount of items: 2
Items: 
Size: 785914 Color: 15
Size: 212094 Color: 0

Bin 766: 1995 of cap free
Amount of items: 2
Items: 
Size: 611850 Color: 7
Size: 386156 Color: 8

Bin 767: 2039 of cap free
Amount of items: 2
Items: 
Size: 637072 Color: 7
Size: 360890 Color: 0

Bin 768: 2115 of cap free
Amount of items: 2
Items: 
Size: 785865 Color: 17
Size: 212021 Color: 15

Bin 769: 2130 of cap free
Amount of items: 2
Items: 
Size: 782753 Color: 10
Size: 215118 Color: 3

Bin 770: 2163 of cap free
Amount of items: 2
Items: 
Size: 598548 Color: 16
Size: 399290 Color: 10

Bin 771: 2230 of cap free
Amount of items: 2
Items: 
Size: 701849 Color: 3
Size: 295922 Color: 7

Bin 772: 2272 of cap free
Amount of items: 2
Items: 
Size: 675622 Color: 15
Size: 322107 Color: 19

Bin 773: 2279 of cap free
Amount of items: 2
Items: 
Size: 760830 Color: 5
Size: 236892 Color: 11

Bin 774: 2286 of cap free
Amount of items: 2
Items: 
Size: 769094 Color: 0
Size: 228621 Color: 19

Bin 775: 2303 of cap free
Amount of items: 2
Items: 
Size: 577454 Color: 19
Size: 420244 Color: 14

Bin 776: 2319 of cap free
Amount of items: 2
Items: 
Size: 636950 Color: 6
Size: 360732 Color: 1

Bin 777: 2339 of cap free
Amount of items: 2
Items: 
Size: 560549 Color: 15
Size: 437113 Color: 2

Bin 778: 2342 of cap free
Amount of items: 2
Items: 
Size: 512384 Color: 10
Size: 485275 Color: 0

Bin 779: 2395 of cap free
Amount of items: 2
Items: 
Size: 774196 Color: 17
Size: 223410 Color: 6

Bin 780: 2427 of cap free
Amount of items: 2
Items: 
Size: 730098 Color: 3
Size: 267476 Color: 19

Bin 781: 2436 of cap free
Amount of items: 2
Items: 
Size: 591936 Color: 2
Size: 405629 Color: 3

Bin 782: 2439 of cap free
Amount of items: 2
Items: 
Size: 534277 Color: 6
Size: 463285 Color: 15

Bin 783: 2452 of cap free
Amount of items: 2
Items: 
Size: 684632 Color: 16
Size: 312917 Color: 3

Bin 784: 2466 of cap free
Amount of items: 2
Items: 
Size: 617100 Color: 18
Size: 380435 Color: 19

Bin 785: 2470 of cap free
Amount of items: 2
Items: 
Size: 753217 Color: 0
Size: 244314 Color: 3

Bin 786: 2554 of cap free
Amount of items: 2
Items: 
Size: 649193 Color: 7
Size: 348254 Color: 1

Bin 787: 2562 of cap free
Amount of items: 2
Items: 
Size: 709683 Color: 4
Size: 287756 Color: 5

Bin 788: 2573 of cap free
Amount of items: 2
Items: 
Size: 712990 Color: 13
Size: 284438 Color: 14

Bin 789: 2635 of cap free
Amount of items: 2
Items: 
Size: 701516 Color: 10
Size: 295850 Color: 12

Bin 790: 2664 of cap free
Amount of items: 2
Items: 
Size: 517031 Color: 13
Size: 480306 Color: 14

Bin 791: 2742 of cap free
Amount of items: 2
Items: 
Size: 797582 Color: 6
Size: 199677 Color: 10

Bin 792: 2782 of cap free
Amount of items: 2
Items: 
Size: 746024 Color: 17
Size: 251195 Color: 18

Bin 793: 2787 of cap free
Amount of items: 2
Items: 
Size: 757579 Color: 16
Size: 239635 Color: 0

Bin 794: 2856 of cap free
Amount of items: 2
Items: 
Size: 695036 Color: 6
Size: 302109 Color: 2

Bin 795: 2911 of cap free
Amount of items: 2
Items: 
Size: 745927 Color: 16
Size: 251163 Color: 3

Bin 796: 2918 of cap free
Amount of items: 2
Items: 
Size: 684326 Color: 15
Size: 312757 Color: 1

Bin 797: 3057 of cap free
Amount of items: 2
Items: 
Size: 536916 Color: 11
Size: 460028 Color: 4

Bin 798: 3095 of cap free
Amount of items: 2
Items: 
Size: 701248 Color: 12
Size: 295658 Color: 3

Bin 799: 3184 of cap free
Amount of items: 2
Items: 
Size: 785665 Color: 6
Size: 211152 Color: 18

Bin 800: 3201 of cap free
Amount of items: 2
Items: 
Size: 616390 Color: 18
Size: 380410 Color: 7

Bin 801: 3280 of cap free
Amount of items: 2
Items: 
Size: 560481 Color: 19
Size: 436240 Color: 8

Bin 802: 3290 of cap free
Amount of items: 2
Items: 
Size: 768269 Color: 12
Size: 228442 Color: 17

Bin 803: 3414 of cap free
Amount of items: 2
Items: 
Size: 511588 Color: 11
Size: 484999 Color: 15

Bin 804: 3427 of cap free
Amount of items: 2
Items: 
Size: 785425 Color: 11
Size: 211149 Color: 19

Bin 805: 3437 of cap free
Amount of items: 2
Items: 
Size: 797113 Color: 12
Size: 199451 Color: 9

Bin 806: 3445 of cap free
Amount of items: 2
Items: 
Size: 516281 Color: 14
Size: 480275 Color: 8

Bin 807: 3459 of cap free
Amount of items: 2
Items: 
Size: 552735 Color: 14
Size: 443807 Color: 3

Bin 808: 3515 of cap free
Amount of items: 2
Items: 
Size: 768175 Color: 10
Size: 228311 Color: 0

Bin 809: 3598 of cap free
Amount of items: 2
Items: 
Size: 621316 Color: 19
Size: 375087 Color: 13

Bin 810: 3638 of cap free
Amount of items: 2
Items: 
Size: 639283 Color: 8
Size: 357080 Color: 7

Bin 811: 3790 of cap free
Amount of items: 2
Items: 
Size: 581251 Color: 2
Size: 414960 Color: 18

Bin 812: 3866 of cap free
Amount of items: 2
Items: 
Size: 532890 Color: 12
Size: 463245 Color: 15

Bin 813: 3902 of cap free
Amount of items: 2
Items: 
Size: 552386 Color: 1
Size: 443713 Color: 2

Bin 814: 3980 of cap free
Amount of items: 2
Items: 
Size: 571575 Color: 4
Size: 424446 Color: 18

Bin 815: 4192 of cap free
Amount of items: 2
Items: 
Size: 629080 Color: 12
Size: 366729 Color: 6

Bin 816: 4448 of cap free
Amount of items: 2
Items: 
Size: 649166 Color: 7
Size: 346387 Color: 6

Bin 817: 4720 of cap free
Amount of items: 2
Items: 
Size: 785246 Color: 8
Size: 210035 Color: 15

Bin 818: 4765 of cap free
Amount of items: 2
Items: 
Size: 559331 Color: 2
Size: 435905 Color: 1

Bin 819: 4892 of cap free
Amount of items: 2
Items: 
Size: 510994 Color: 6
Size: 484115 Color: 7

Bin 820: 5019 of cap free
Amount of items: 2
Items: 
Size: 497606 Color: 12
Size: 497376 Color: 0

Bin 821: 5074 of cap free
Amount of items: 2
Items: 
Size: 639220 Color: 1
Size: 355707 Color: 12

Bin 822: 5141 of cap free
Amount of items: 2
Items: 
Size: 797082 Color: 2
Size: 197778 Color: 9

Bin 823: 5199 of cap free
Amount of items: 2
Items: 
Size: 581138 Color: 3
Size: 413664 Color: 15

Bin 824: 5448 of cap free
Amount of items: 2
Items: 
Size: 535467 Color: 15
Size: 459086 Color: 10

Bin 825: 5449 of cap free
Amount of items: 2
Items: 
Size: 700306 Color: 9
Size: 294246 Color: 5

Bin 826: 5576 of cap free
Amount of items: 2
Items: 
Size: 722483 Color: 9
Size: 271942 Color: 8

Bin 827: 5930 of cap free
Amount of items: 3
Items: 
Size: 722163 Color: 4
Size: 139477 Color: 8
Size: 132431 Color: 7

Bin 828: 6044 of cap free
Amount of items: 2
Items: 
Size: 550318 Color: 5
Size: 443639 Color: 15

Bin 829: 6149 of cap free
Amount of items: 2
Items: 
Size: 757561 Color: 5
Size: 236291 Color: 11

Bin 830: 6295 of cap free
Amount of items: 2
Items: 
Size: 580330 Color: 6
Size: 413376 Color: 19

Bin 831: 6339 of cap free
Amount of items: 2
Items: 
Size: 765785 Color: 5
Size: 227877 Color: 17

Bin 832: 6672 of cap free
Amount of items: 2
Items: 
Size: 497145 Color: 11
Size: 496184 Color: 4

Bin 833: 6742 of cap free
Amount of items: 2
Items: 
Size: 757038 Color: 2
Size: 236221 Color: 15

Bin 834: 7161 of cap free
Amount of items: 2
Items: 
Size: 675244 Color: 2
Size: 317596 Color: 19

Bin 835: 7257 of cap free
Amount of items: 2
Items: 
Size: 698592 Color: 18
Size: 294152 Color: 2

Bin 836: 7479 of cap free
Amount of items: 2
Items: 
Size: 675025 Color: 3
Size: 317497 Color: 2

Bin 837: 7501 of cap free
Amount of items: 2
Items: 
Size: 580216 Color: 1
Size: 412284 Color: 13

Bin 838: 7688 of cap free
Amount of items: 2
Items: 
Size: 559163 Color: 14
Size: 433150 Color: 7

Bin 839: 7912 of cap free
Amount of items: 2
Items: 
Size: 756948 Color: 1
Size: 235141 Color: 10

Bin 840: 8391 of cap free
Amount of items: 2
Items: 
Size: 636339 Color: 16
Size: 355271 Color: 12

Bin 841: 8633 of cap free
Amount of items: 2
Items: 
Size: 796723 Color: 16
Size: 194645 Color: 1

Bin 842: 8777 of cap free
Amount of items: 2
Items: 
Size: 548309 Color: 17
Size: 442915 Color: 16

Bin 843: 8855 of cap free
Amount of items: 2
Items: 
Size: 510928 Color: 18
Size: 480218 Color: 2

Bin 844: 9104 of cap free
Amount of items: 2
Items: 
Size: 557728 Color: 12
Size: 433169 Color: 14

Bin 845: 9140 of cap free
Amount of items: 2
Items: 
Size: 548074 Color: 7
Size: 442787 Color: 4

Bin 846: 9206 of cap free
Amount of items: 2
Items: 
Size: 636120 Color: 3
Size: 354675 Color: 19

Bin 847: 9409 of cap free
Amount of items: 2
Items: 
Size: 636103 Color: 16
Size: 354489 Color: 1

Bin 848: 9527 of cap free
Amount of items: 2
Items: 
Size: 674650 Color: 9
Size: 315824 Color: 6

Bin 849: 9850 of cap free
Amount of items: 2
Items: 
Size: 531769 Color: 4
Size: 458382 Color: 16

Bin 850: 10032 of cap free
Amount of items: 2
Items: 
Size: 495855 Color: 11
Size: 494114 Color: 14

Bin 851: 10091 of cap free
Amount of items: 2
Items: 
Size: 531680 Color: 19
Size: 458230 Color: 15

Bin 852: 10196 of cap free
Amount of items: 2
Items: 
Size: 509675 Color: 0
Size: 480130 Color: 8

Bin 853: 10713 of cap free
Amount of items: 2
Items: 
Size: 546991 Color: 1
Size: 442297 Color: 14

Bin 854: 10787 of cap free
Amount of items: 2
Items: 
Size: 795199 Color: 1
Size: 194015 Color: 8

Bin 855: 11402 of cap free
Amount of items: 2
Items: 
Size: 530856 Color: 15
Size: 457743 Color: 11

Bin 856: 11865 of cap free
Amount of items: 2
Items: 
Size: 580052 Color: 4
Size: 408084 Color: 5

Bin 857: 12421 of cap free
Amount of items: 2
Items: 
Size: 794838 Color: 19
Size: 192742 Color: 1

Bin 858: 12755 of cap free
Amount of items: 2
Items: 
Size: 493650 Color: 11
Size: 493596 Color: 1

Bin 859: 13463 of cap free
Amount of items: 2
Items: 
Size: 694807 Color: 0
Size: 291731 Color: 16

Bin 860: 13694 of cap free
Amount of items: 2
Items: 
Size: 737311 Color: 5
Size: 248996 Color: 15

Bin 861: 14396 of cap free
Amount of items: 2
Items: 
Size: 669799 Color: 15
Size: 315806 Color: 7

Bin 862: 14739 of cap free
Amount of items: 2
Items: 
Size: 509104 Color: 0
Size: 476158 Color: 17

Bin 863: 15720 of cap free
Amount of items: 2
Items: 
Size: 719027 Color: 14
Size: 265254 Color: 5

Bin 864: 16937 of cap free
Amount of items: 2
Items: 
Size: 491897 Color: 9
Size: 491167 Color: 7

Bin 865: 18901 of cap free
Amount of items: 2
Items: 
Size: 690344 Color: 15
Size: 290756 Color: 0

Bin 866: 19019 of cap free
Amount of items: 2
Items: 
Size: 634961 Color: 9
Size: 346021 Color: 8

Bin 867: 19311 of cap free
Amount of items: 2
Items: 
Size: 690063 Color: 2
Size: 290627 Color: 3

Bin 868: 19384 of cap free
Amount of items: 2
Items: 
Size: 717158 Color: 3
Size: 263459 Color: 0

Bin 869: 19527 of cap free
Amount of items: 2
Items: 
Size: 634889 Color: 15
Size: 345585 Color: 11

Bin 870: 20345 of cap free
Amount of items: 2
Items: 
Size: 576953 Color: 8
Size: 402703 Color: 9

Bin 871: 20752 of cap free
Amount of items: 2
Items: 
Size: 633820 Color: 13
Size: 345429 Color: 15

Bin 872: 22560 of cap free
Amount of items: 2
Items: 
Size: 791878 Color: 10
Size: 185563 Color: 19

Bin 873: 22920 of cap free
Amount of items: 2
Items: 
Size: 791755 Color: 17
Size: 185326 Color: 2

Bin 874: 23946 of cap free
Amount of items: 2
Items: 
Size: 791547 Color: 11
Size: 184508 Color: 8

Bin 875: 26270 of cap free
Amount of items: 2
Items: 
Size: 790478 Color: 4
Size: 183253 Color: 7

Bin 876: 28418 of cap free
Amount of items: 2
Items: 
Size: 661307 Color: 15
Size: 310276 Color: 1

Bin 877: 29632 of cap free
Amount of items: 2
Items: 
Size: 576780 Color: 18
Size: 393589 Color: 9

Bin 878: 30185 of cap free
Amount of items: 2
Items: 
Size: 790474 Color: 17
Size: 179342 Color: 9

Bin 879: 32955 of cap free
Amount of items: 2
Items: 
Size: 491039 Color: 11
Size: 476007 Color: 9

Bin 880: 35835 of cap free
Amount of items: 2
Items: 
Size: 717352 Color: 0
Size: 246814 Color: 16

Bin 881: 39039 of cap free
Amount of items: 2
Items: 
Size: 576056 Color: 7
Size: 384906 Color: 8

Bin 882: 40544 of cap free
Amount of items: 2
Items: 
Size: 785011 Color: 3
Size: 174446 Color: 0

Bin 883: 50454 of cap free
Amount of items: 2
Items: 
Size: 475831 Color: 8
Size: 473716 Color: 0

Bin 884: 50867 of cap free
Amount of items: 2
Items: 
Size: 781220 Color: 11
Size: 167914 Color: 2

Bin 885: 53493 of cap free
Amount of items: 2
Items: 
Size: 473268 Color: 1
Size: 473240 Color: 0

Bin 886: 70063 of cap free
Amount of items: 2
Items: 
Size: 472202 Color: 10
Size: 457736 Color: 5

Bin 887: 87667 of cap free
Amount of items: 2
Items: 
Size: 457480 Color: 2
Size: 454854 Color: 6

Bin 888: 91767 of cap free
Amount of items: 2
Items: 
Size: 454237 Color: 2
Size: 453997 Color: 3

Bin 889: 221180 of cap free
Amount of items: 1
Items: 
Size: 778821 Color: 10

Bin 890: 221203 of cap free
Amount of items: 1
Items: 
Size: 778798 Color: 0

Bin 891: 221268 of cap free
Amount of items: 1
Items: 
Size: 778733 Color: 2

Bin 892: 222853 of cap free
Amount of items: 1
Items: 
Size: 777148 Color: 3

Bin 893: 225890 of cap free
Amount of items: 1
Items: 
Size: 774111 Color: 2

Bin 894: 226419 of cap free
Amount of items: 1
Items: 
Size: 773582 Color: 19

Bin 895: 226647 of cap free
Amount of items: 1
Items: 
Size: 773354 Color: 0

Bin 896: 227514 of cap free
Amount of items: 1
Items: 
Size: 772487 Color: 8

Bin 897: 227747 of cap free
Amount of items: 1
Items: 
Size: 772254 Color: 16

Bin 898: 234762 of cap free
Amount of items: 1
Items: 
Size: 765239 Color: 0

Bin 899: 234814 of cap free
Amount of items: 1
Items: 
Size: 765187 Color: 2

Bin 900: 234941 of cap free
Amount of items: 1
Items: 
Size: 765060 Color: 8

Bin 901: 235047 of cap free
Amount of items: 1
Items: 
Size: 764954 Color: 4

Bin 902: 243132 of cap free
Amount of items: 1
Items: 
Size: 756869 Color: 4

Bin 903: 243277 of cap free
Amount of items: 1
Items: 
Size: 756724 Color: 0

Bin 904: 244298 of cap free
Amount of items: 1
Items: 
Size: 755703 Color: 10

Bin 905: 244304 of cap free
Amount of items: 1
Items: 
Size: 755697 Color: 7

Bin 906: 248927 of cap free
Amount of items: 1
Items: 
Size: 751074 Color: 16

Total size: 900107146
Total free space: 5893760

