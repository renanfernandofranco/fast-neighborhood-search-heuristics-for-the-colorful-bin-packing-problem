Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 249

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 242
Size: 263 Color: 46
Size: 256 Color: 26

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 214
Size: 295 Color: 102
Size: 274 Color: 66

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 240
Size: 267 Color: 51
Size: 255 Color: 20

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 189
Size: 363 Color: 170
Size: 257 Color: 29

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 183
Size: 328 Color: 131
Size: 297 Color: 107

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 225
Size: 277 Color: 79
Size: 275 Color: 70

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 164
Size: 332 Color: 134
Size: 308 Color: 118

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 235
Size: 270 Color: 57
Size: 261 Color: 42

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 191
Size: 345 Color: 146
Size: 271 Color: 63

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 233
Size: 275 Color: 68
Size: 257 Color: 30

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 184
Size: 368 Color: 175
Size: 256 Color: 27

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 182
Size: 316 Color: 125
Size: 311 Color: 120

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 185
Size: 318 Color: 128
Size: 305 Color: 116

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 212
Size: 294 Color: 99
Size: 279 Color: 84

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 199
Size: 342 Color: 143
Size: 257 Color: 33

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 154
Size: 351 Color: 151
Size: 296 Color: 106

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 244
Size: 259 Color: 36
Size: 253 Color: 11

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 206
Size: 336 Color: 139
Size: 250 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 187
Size: 346 Color: 148
Size: 276 Color: 75

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 195
Size: 345 Color: 145
Size: 269 Color: 54

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 220
Size: 295 Color: 103
Size: 267 Color: 50

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 218
Size: 288 Color: 90
Size: 276 Color: 76

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 193
Size: 360 Color: 165
Size: 256 Color: 25

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 227
Size: 278 Color: 80
Size: 269 Color: 55

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 169
Size: 361 Color: 166
Size: 276 Color: 72

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 192
Size: 264 Color: 47
Size: 352 Color: 152

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 194
Size: 346 Color: 147
Size: 269 Color: 53

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 208
Size: 332 Color: 135
Size: 250 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 202
Size: 316 Color: 124
Size: 275 Color: 71

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 200
Size: 301 Color: 112
Size: 296 Color: 104

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 201
Size: 334 Color: 136
Size: 259 Color: 37

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 188
Size: 328 Color: 132
Size: 292 Color: 97

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 186
Size: 372 Color: 180
Size: 250 Color: 5

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 178
Size: 339 Color: 141
Size: 289 Color: 93

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 177
Size: 324 Color: 129
Size: 306 Color: 117

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 190
Size: 318 Color: 127
Size: 301 Color: 113

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 176
Size: 335 Color: 138
Size: 296 Color: 105

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 181
Size: 331 Color: 133
Size: 297 Color: 108

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 168
Size: 339 Color: 142
Size: 298 Color: 109

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 172
Size: 363 Color: 171
Size: 271 Color: 60

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 174
Size: 362 Color: 167
Size: 272 Color: 65

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 173
Size: 358 Color: 162
Size: 276 Color: 73

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 163
Size: 354 Color: 157
Size: 286 Color: 87

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 161
Size: 355 Color: 159
Size: 288 Color: 91

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 158
Size: 292 Color: 96
Size: 353 Color: 156

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 155
Size: 352 Color: 153
Size: 295 Color: 100

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 179
Size: 356 Color: 160
Size: 272 Color: 64

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 196
Size: 348 Color: 150
Size: 261 Color: 41

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 197
Size: 344 Color: 144
Size: 260 Color: 40

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 198
Size: 347 Color: 149
Size: 256 Color: 24

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 203
Size: 337 Color: 140
Size: 254 Color: 16

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 204
Size: 334 Color: 137
Size: 254 Color: 18

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 205
Size: 312 Color: 121
Size: 274 Color: 67

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 207
Size: 313 Color: 123
Size: 269 Color: 56

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 209
Size: 325 Color: 130
Size: 254 Color: 17

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 210
Size: 317 Color: 126
Size: 258 Color: 35

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 211
Size: 313 Color: 122
Size: 262 Color: 45

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 213
Size: 304 Color: 115
Size: 265 Color: 48

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 215
Size: 298 Color: 110
Size: 270 Color: 59

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 217
Size: 287 Color: 88
Size: 280 Color: 86

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 216
Size: 288 Color: 92
Size: 279 Color: 83

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 219
Size: 310 Color: 119
Size: 252 Color: 9

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 221
Size: 291 Color: 95
Size: 270 Color: 58

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 222
Size: 302 Color: 114
Size: 256 Color: 28

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 223
Size: 295 Color: 101
Size: 260 Color: 39

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 224
Size: 277 Color: 78
Size: 277 Color: 77

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 226
Size: 300 Color: 111
Size: 251 Color: 7

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 228
Size: 294 Color: 98
Size: 251 Color: 8

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 229
Size: 288 Color: 89
Size: 257 Color: 31

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 230
Size: 278 Color: 82
Size: 266 Color: 49

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 231
Size: 290 Color: 94
Size: 253 Color: 12

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 232
Size: 280 Color: 85
Size: 261 Color: 43

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 236
Size: 271 Color: 62
Size: 258 Color: 34

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 234
Size: 275 Color: 69
Size: 256 Color: 22

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 238
Size: 267 Color: 52
Size: 260 Color: 38

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 237
Size: 278 Color: 81
Size: 250 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 239
Size: 276 Color: 74
Size: 250 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 241
Size: 271 Color: 61
Size: 251 Color: 6

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 243
Size: 262 Color: 44
Size: 252 Color: 10

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 245
Size: 257 Color: 32
Size: 254 Color: 15

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 246
Size: 256 Color: 21
Size: 253 Color: 14

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 247
Size: 254 Color: 19
Size: 253 Color: 13

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 248
Size: 256 Color: 23
Size: 250 Color: 4

Total size: 83000
Total free space: 0

