Capicity Bin: 2356
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1185 Color: 12
Size: 977 Color: 12
Size: 194 Color: 18

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 3
Size: 982 Color: 9
Size: 44 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 19
Size: 702 Color: 13
Size: 60 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1701 Color: 12
Size: 613 Color: 19
Size: 42 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1807 Color: 19
Size: 381 Color: 15
Size: 168 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 13
Size: 403 Color: 18
Size: 80 Color: 17

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 1
Size: 318 Color: 16
Size: 100 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 19
Size: 333 Color: 7
Size: 76 Color: 3

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1978 Color: 12
Size: 214 Color: 14
Size: 164 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1997 Color: 0
Size: 301 Color: 4
Size: 58 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2042 Color: 16
Size: 202 Color: 18
Size: 112 Color: 19

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2047 Color: 3
Size: 255 Color: 1
Size: 54 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2070 Color: 18
Size: 162 Color: 12
Size: 124 Color: 11

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2075 Color: 0
Size: 229 Color: 7
Size: 52 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 15
Size: 199 Color: 6
Size: 78 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2102 Color: 12
Size: 178 Color: 1
Size: 76 Color: 13

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2099 Color: 13
Size: 241 Color: 0
Size: 16 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2103 Color: 18
Size: 215 Color: 18
Size: 38 Color: 12

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1811 Color: 6
Size: 454 Color: 0
Size: 90 Color: 5

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 10
Size: 495 Color: 9
Size: 46 Color: 12

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1878 Color: 16
Size: 242 Color: 11
Size: 235 Color: 12

Bin 22: 1 of cap free
Amount of items: 2
Items: 
Size: 1880 Color: 16
Size: 475 Color: 17

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 2001 Color: 16
Size: 226 Color: 3
Size: 128 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 2017 Color: 12
Size: 292 Color: 2
Size: 46 Color: 17

Bin 25: 2 of cap free
Amount of items: 4
Items: 
Size: 1179 Color: 4
Size: 638 Color: 12
Size: 429 Color: 17
Size: 108 Color: 4

Bin 26: 2 of cap free
Amount of items: 3
Items: 
Size: 1281 Color: 9
Size: 981 Color: 0
Size: 92 Color: 10

Bin 27: 2 of cap free
Amount of items: 2
Items: 
Size: 1567 Color: 17
Size: 787 Color: 10

Bin 28: 2 of cap free
Amount of items: 2
Items: 
Size: 1899 Color: 10
Size: 455 Color: 1

Bin 29: 2 of cap free
Amount of items: 2
Items: 
Size: 2071 Color: 13
Size: 283 Color: 7

Bin 30: 2 of cap free
Amount of items: 2
Items: 
Size: 2083 Color: 10
Size: 271 Color: 8

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 2118 Color: 5
Size: 236 Color: 14

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 2119 Color: 14
Size: 231 Color: 17
Size: 4 Color: 6

Bin 33: 3 of cap free
Amount of items: 4
Items: 
Size: 1181 Color: 19
Size: 858 Color: 6
Size: 158 Color: 7
Size: 156 Color: 10

Bin 34: 3 of cap free
Amount of items: 2
Items: 
Size: 1189 Color: 11
Size: 1164 Color: 2

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1237 Color: 19
Size: 826 Color: 6
Size: 290 Color: 4

Bin 36: 3 of cap free
Amount of items: 4
Items: 
Size: 1366 Color: 18
Size: 897 Color: 12
Size: 58 Color: 17
Size: 32 Color: 14

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1518 Color: 5
Size: 691 Color: 7
Size: 144 Color: 18

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 4
Size: 742 Color: 7
Size: 32 Color: 4

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 13
Size: 554 Color: 18
Size: 36 Color: 2

Bin 40: 5 of cap free
Amount of items: 3
Items: 
Size: 1377 Color: 13
Size: 930 Color: 11
Size: 44 Color: 6

Bin 41: 5 of cap free
Amount of items: 2
Items: 
Size: 2010 Color: 18
Size: 341 Color: 10

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 2031 Color: 8
Size: 320 Color: 3

Bin 43: 5 of cap free
Amount of items: 3
Items: 
Size: 2080 Color: 19
Size: 259 Color: 7
Size: 12 Color: 0

Bin 44: 6 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 4
Size: 679 Color: 12
Size: 144 Color: 6

Bin 45: 6 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 13
Size: 474 Color: 10
Size: 134 Color: 6

Bin 46: 6 of cap free
Amount of items: 2
Items: 
Size: 1803 Color: 17
Size: 547 Color: 14

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1317 Color: 18
Size: 1032 Color: 11

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1890 Color: 14
Size: 459 Color: 19

Bin 49: 8 of cap free
Amount of items: 2
Items: 
Size: 1397 Color: 2
Size: 951 Color: 1

Bin 50: 8 of cap free
Amount of items: 2
Items: 
Size: 1681 Color: 10
Size: 667 Color: 9

Bin 51: 8 of cap free
Amount of items: 2
Items: 
Size: 2086 Color: 6
Size: 262 Color: 11

Bin 52: 9 of cap free
Amount of items: 3
Items: 
Size: 1182 Color: 13
Size: 1053 Color: 14
Size: 112 Color: 0

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1782 Color: 7
Size: 565 Color: 12

Bin 54: 9 of cap free
Amount of items: 2
Items: 
Size: 1957 Color: 17
Size: 390 Color: 3

Bin 55: 10 of cap free
Amount of items: 2
Items: 
Size: 1413 Color: 4
Size: 933 Color: 1

Bin 56: 10 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 5
Size: 876 Color: 18

Bin 57: 11 of cap free
Amount of items: 2
Items: 
Size: 1679 Color: 0
Size: 666 Color: 14

Bin 58: 12 of cap free
Amount of items: 2
Items: 
Size: 1543 Color: 2
Size: 801 Color: 5

Bin 59: 13 of cap free
Amount of items: 2
Items: 
Size: 1694 Color: 15
Size: 649 Color: 16

Bin 60: 14 of cap free
Amount of items: 2
Items: 
Size: 1843 Color: 0
Size: 499 Color: 4

Bin 61: 15 of cap free
Amount of items: 2
Items: 
Size: 1827 Color: 16
Size: 514 Color: 7

Bin 62: 17 of cap free
Amount of items: 7
Items: 
Size: 817 Color: 1
Size: 448 Color: 6
Size: 360 Color: 5
Size: 350 Color: 14
Size: 138 Color: 5
Size: 136 Color: 7
Size: 90 Color: 0

Bin 63: 24 of cap free
Amount of items: 3
Items: 
Size: 1658 Color: 2
Size: 482 Color: 12
Size: 192 Color: 4

Bin 64: 25 of cap free
Amount of items: 19
Items: 
Size: 297 Color: 12
Size: 196 Color: 6
Size: 194 Color: 10
Size: 188 Color: 17
Size: 186 Color: 8
Size: 184 Color: 15
Size: 136 Color: 3
Size: 122 Color: 5
Size: 114 Color: 11
Size: 98 Color: 3
Size: 88 Color: 10
Size: 88 Color: 8
Size: 84 Color: 19
Size: 68 Color: 16
Size: 68 Color: 7
Size: 66 Color: 0
Size: 62 Color: 11
Size: 56 Color: 17
Size: 36 Color: 14

Bin 65: 25 of cap free
Amount of items: 3
Items: 
Size: 1242 Color: 1
Size: 981 Color: 17
Size: 108 Color: 6

Bin 66: 2034 of cap free
Amount of items: 8
Items: 
Size: 56 Color: 15
Size: 50 Color: 17
Size: 44 Color: 16
Size: 40 Color: 9
Size: 40 Color: 7
Size: 32 Color: 10
Size: 30 Color: 14
Size: 30 Color: 0

Total size: 153140
Total free space: 2356

