Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1178 Color: 146
Size: 810 Color: 132
Size: 48 Color: 22

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1475 Color: 160
Size: 541 Color: 120
Size: 20 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 162
Size: 362 Color: 103
Size: 168 Color: 68

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 164
Size: 490 Color: 115
Size: 32 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 168
Size: 326 Color: 99
Size: 108 Color: 56

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1605 Color: 169
Size: 361 Color: 102
Size: 70 Color: 39

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1645 Color: 173
Size: 327 Color: 100
Size: 64 Color: 36

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1651 Color: 175
Size: 321 Color: 98
Size: 64 Color: 37

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1662 Color: 176
Size: 316 Color: 97
Size: 58 Color: 33

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 178
Size: 314 Color: 95
Size: 40 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 180
Size: 242 Color: 83
Size: 104 Color: 52

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1698 Color: 182
Size: 262 Color: 88
Size: 76 Color: 42

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 184
Size: 282 Color: 90
Size: 28 Color: 5

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 185
Size: 211 Color: 77
Size: 96 Color: 51

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1762 Color: 190
Size: 254 Color: 86
Size: 20 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 191
Size: 227 Color: 81
Size: 44 Color: 19

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 197
Size: 187 Color: 72
Size: 36 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 198
Size: 178 Color: 69
Size: 44 Color: 17

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1815 Color: 199
Size: 185 Color: 71
Size: 36 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 200
Size: 182 Color: 70
Size: 36 Color: 13

Bin 21: 1 of cap free
Amount of items: 9
Items: 
Size: 1019 Color: 138
Size: 224 Color: 78
Size: 190 Color: 74
Size: 190 Color: 73
Size: 168 Color: 67
Size: 76 Color: 43
Size: 56 Color: 30
Size: 56 Color: 29
Size: 56 Color: 28

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 144
Size: 820 Color: 133
Size: 48 Color: 23

Bin 23: 1 of cap free
Amount of items: 4
Items: 
Size: 1401 Color: 157
Size: 562 Color: 121
Size: 36 Color: 11
Size: 36 Color: 10

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 161
Size: 538 Color: 119
Size: 16 Color: 2

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 165
Size: 438 Color: 110
Size: 48 Color: 21

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1620 Color: 172
Size: 407 Color: 107
Size: 8 Color: 1

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1646 Color: 174
Size: 389 Color: 105

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1697 Color: 181
Size: 198 Color: 75
Size: 140 Color: 63

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1719 Color: 183
Size: 316 Color: 96

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1742 Color: 188
Size: 293 Color: 93

Bin 31: 1 of cap free
Amount of items: 2
Items: 
Size: 1810 Color: 196
Size: 225 Color: 80

Bin 32: 1 of cap free
Amount of items: 2
Items: 
Size: 1826 Color: 201
Size: 209 Color: 76

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 166
Size: 468 Color: 114

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1571 Color: 167
Size: 463 Color: 113

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1787 Color: 194
Size: 247 Color: 85

Bin 36: 3 of cap free
Amount of items: 5
Items: 
Size: 1022 Color: 140
Size: 442 Color: 112
Size: 441 Color: 111
Size: 76 Color: 44
Size: 52 Color: 25

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1290 Color: 150
Size: 743 Color: 131

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1609 Color: 170
Size: 424 Color: 109

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 179
Size: 348 Color: 101

Bin 40: 3 of cap free
Amount of items: 2
Items: 
Size: 1750 Color: 189
Size: 283 Color: 91

Bin 41: 4 of cap free
Amount of items: 2
Items: 
Size: 1366 Color: 153
Size: 666 Color: 126

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1507 Color: 163
Size: 525 Color: 116

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1617 Color: 171
Size: 407 Color: 108
Size: 8 Color: 0

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1734 Color: 186
Size: 298 Color: 94

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1767 Color: 192
Size: 265 Color: 89

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1802 Color: 195
Size: 230 Color: 82

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1664 Color: 177
Size: 367 Color: 104

Bin 48: 5 of cap free
Amount of items: 2
Items: 
Size: 1741 Color: 187
Size: 290 Color: 92

Bin 49: 5 of cap free
Amount of items: 2
Items: 
Size: 1785 Color: 193
Size: 246 Color: 84

Bin 50: 9 of cap free
Amount of items: 3
Items: 
Size: 1291 Color: 151
Size: 692 Color: 127
Size: 44 Color: 18

Bin 51: 17 of cap free
Amount of items: 3
Items: 
Size: 1450 Color: 159
Size: 537 Color: 118
Size: 32 Color: 6

Bin 52: 18 of cap free
Amount of items: 2
Items: 
Size: 1171 Color: 145
Size: 847 Color: 135

Bin 53: 20 of cap free
Amount of items: 2
Items: 
Size: 1289 Color: 149
Size: 727 Color: 130

Bin 54: 20 of cap free
Amount of items: 2
Items: 
Size: 1393 Color: 155
Size: 623 Color: 124

Bin 55: 23 of cap free
Amount of items: 3
Items: 
Size: 1408 Color: 158
Size: 569 Color: 122
Size: 36 Color: 8

Bin 56: 24 of cap free
Amount of items: 2
Items: 
Size: 1023 Color: 141
Size: 989 Color: 137

Bin 57: 24 of cap free
Amount of items: 2
Items: 
Size: 1355 Color: 152
Size: 657 Color: 125

Bin 58: 25 of cap free
Amount of items: 2
Items: 
Size: 1389 Color: 154
Size: 622 Color: 123

Bin 59: 26 of cap free
Amount of items: 2
Items: 
Size: 1165 Color: 143
Size: 845 Color: 134

Bin 60: 27 of cap free
Amount of items: 4
Items: 
Size: 1394 Color: 156
Size: 531 Color: 117
Size: 44 Color: 16
Size: 40 Color: 14

Bin 61: 30 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 142
Size: 890 Color: 136
Size: 50 Color: 24

Bin 62: 32 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 147
Size: 718 Color: 128
Size: 48 Color: 20

Bin 63: 36 of cap free
Amount of items: 6
Items: 
Size: 1021 Color: 139
Size: 394 Color: 106
Size: 257 Color: 87
Size: 224 Color: 79
Size: 52 Color: 27
Size: 52 Color: 26

Bin 64: 39 of cap free
Amount of items: 2
Items: 
Size: 1276 Color: 148
Size: 721 Color: 129

Bin 65: 78 of cap free
Amount of items: 19
Items: 
Size: 160 Color: 66
Size: 144 Color: 65
Size: 144 Color: 64
Size: 132 Color: 62
Size: 126 Color: 61
Size: 124 Color: 60
Size: 124 Color: 59
Size: 124 Color: 58
Size: 112 Color: 57
Size: 106 Color: 55
Size: 106 Color: 54
Size: 104 Color: 53
Size: 76 Color: 41
Size: 72 Color: 40
Size: 68 Color: 38
Size: 64 Color: 35
Size: 60 Color: 34
Size: 56 Color: 32
Size: 56 Color: 31

Bin 66: 1516 of cap free
Amount of items: 6
Items: 
Size: 92 Color: 50
Size: 88 Color: 49
Size: 88 Color: 48
Size: 88 Color: 47
Size: 84 Color: 46
Size: 80 Color: 45

Total size: 132340
Total free space: 2036

