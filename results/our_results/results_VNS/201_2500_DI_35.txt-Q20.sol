Capicity Bin: 2400
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 7
Items: 
Size: 705 Color: 12
Size: 611 Color: 18
Size: 331 Color: 5
Size: 301 Color: 9
Size: 262 Color: 3
Size: 106 Color: 12
Size: 84 Color: 6

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1438 Color: 9
Size: 822 Color: 5
Size: 140 Color: 10

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1643 Color: 4
Size: 559 Color: 18
Size: 198 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1743 Color: 6
Size: 549 Color: 19
Size: 108 Color: 17

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1842 Color: 13
Size: 460 Color: 13
Size: 98 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1857 Color: 13
Size: 401 Color: 15
Size: 142 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1881 Color: 11
Size: 433 Color: 19
Size: 86 Color: 11

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1914 Color: 13
Size: 350 Color: 14
Size: 136 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 17
Size: 287 Color: 14
Size: 192 Color: 9

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1963 Color: 10
Size: 365 Color: 16
Size: 72 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1982 Color: 6
Size: 298 Color: 13
Size: 120 Color: 12

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2041 Color: 7
Size: 283 Color: 18
Size: 76 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2049 Color: 6
Size: 293 Color: 4
Size: 58 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 12
Size: 249 Color: 11
Size: 72 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2090 Color: 6
Size: 168 Color: 13
Size: 142 Color: 12

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 5
Size: 198 Color: 0
Size: 92 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2124 Color: 16
Size: 164 Color: 10
Size: 112 Color: 12

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2130 Color: 10
Size: 206 Color: 1
Size: 64 Color: 6

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1418 Color: 7
Size: 835 Color: 18
Size: 146 Color: 10

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1794 Color: 19
Size: 605 Color: 5

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1813 Color: 8
Size: 554 Color: 11
Size: 32 Color: 6

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1882 Color: 12
Size: 427 Color: 17
Size: 90 Color: 8

Bin 23: 1 of cap free
Amount of items: 4
Items: 
Size: 1937 Color: 11
Size: 382 Color: 6
Size: 64 Color: 13
Size: 16 Color: 12

Bin 24: 1 of cap free
Amount of items: 2
Items: 
Size: 1958 Color: 19
Size: 441 Color: 6

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 2061 Color: 16
Size: 338 Color: 9

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 2070 Color: 18
Size: 329 Color: 14

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 2103 Color: 7
Size: 296 Color: 3

Bin 28: 2 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 0
Size: 993 Color: 5
Size: 196 Color: 1

Bin 29: 2 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 1
Size: 453 Color: 2
Size: 406 Color: 5

Bin 30: 2 of cap free
Amount of items: 4
Items: 
Size: 1547 Color: 18
Size: 711 Color: 10
Size: 108 Color: 5
Size: 32 Color: 0

Bin 31: 2 of cap free
Amount of items: 2
Items: 
Size: 1667 Color: 2
Size: 731 Color: 6

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 17
Size: 533 Color: 7
Size: 60 Color: 3

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1998 Color: 10
Size: 226 Color: 6
Size: 174 Color: 2

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 2042 Color: 7
Size: 356 Color: 1

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 2146 Color: 10
Size: 252 Color: 9

Bin 36: 3 of cap free
Amount of items: 2
Items: 
Size: 1395 Color: 17
Size: 1002 Color: 12

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1523 Color: 12
Size: 842 Color: 10
Size: 32 Color: 5

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 2095 Color: 4
Size: 302 Color: 2

Bin 39: 4 of cap free
Amount of items: 3
Items: 
Size: 1205 Color: 11
Size: 1025 Color: 14
Size: 166 Color: 3

Bin 40: 4 of cap free
Amount of items: 2
Items: 
Size: 1399 Color: 3
Size: 997 Color: 18

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 17
Size: 491 Color: 10
Size: 144 Color: 0

Bin 42: 4 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 12
Size: 619 Color: 13

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 2005 Color: 8
Size: 367 Color: 12
Size: 24 Color: 17

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 2154 Color: 7
Size: 242 Color: 17

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1221 Color: 13
Size: 1052 Color: 17
Size: 122 Color: 6

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 14
Size: 839 Color: 9
Size: 166 Color: 1

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 19
Size: 825 Color: 14

Bin 48: 5 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 10
Size: 610 Color: 4
Size: 126 Color: 18

Bin 49: 5 of cap free
Amount of items: 2
Items: 
Size: 1889 Color: 0
Size: 506 Color: 12

Bin 50: 5 of cap free
Amount of items: 2
Items: 
Size: 1961 Color: 11
Size: 434 Color: 15

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 1411 Color: 13
Size: 983 Color: 15

Bin 52: 6 of cap free
Amount of items: 2
Items: 
Size: 1675 Color: 13
Size: 719 Color: 11

Bin 53: 6 of cap free
Amount of items: 2
Items: 
Size: 2007 Color: 8
Size: 387 Color: 12

Bin 54: 7 of cap free
Amount of items: 4
Items: 
Size: 1202 Color: 14
Size: 863 Color: 1
Size: 196 Color: 2
Size: 132 Color: 8

Bin 55: 8 of cap free
Amount of items: 2
Items: 
Size: 1590 Color: 16
Size: 802 Color: 13

Bin 56: 9 of cap free
Amount of items: 3
Items: 
Size: 1234 Color: 13
Size: 888 Color: 4
Size: 269 Color: 8

Bin 57: 11 of cap free
Amount of items: 2
Items: 
Size: 1201 Color: 9
Size: 1188 Color: 13

Bin 58: 11 of cap free
Amount of items: 2
Items: 
Size: 2057 Color: 4
Size: 332 Color: 16

Bin 59: 12 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 1
Size: 678 Color: 12
Size: 40 Color: 10

Bin 60: 14 of cap free
Amount of items: 3
Items: 
Size: 1873 Color: 6
Size: 497 Color: 2
Size: 16 Color: 0

Bin 61: 14 of cap free
Amount of items: 2
Items: 
Size: 1920 Color: 19
Size: 466 Color: 2

Bin 62: 15 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 7
Size: 790 Color: 9
Size: 40 Color: 1

Bin 63: 17 of cap free
Amount of items: 3
Items: 
Size: 1365 Color: 19
Size: 974 Color: 10
Size: 44 Color: 6

Bin 64: 31 of cap free
Amount of items: 2
Items: 
Size: 1738 Color: 9
Size: 631 Color: 7

Bin 65: 43 of cap free
Amount of items: 21
Items: 
Size: 255 Color: 3
Size: 238 Color: 1
Size: 192 Color: 6
Size: 172 Color: 19
Size: 160 Color: 14
Size: 160 Color: 1
Size: 122 Color: 3
Size: 120 Color: 18
Size: 100 Color: 7
Size: 96 Color: 6
Size: 86 Color: 14
Size: 84 Color: 8
Size: 80 Color: 18
Size: 78 Color: 12
Size: 68 Color: 10
Size: 64 Color: 15
Size: 64 Color: 11
Size: 58 Color: 7
Size: 56 Color: 16
Size: 56 Color: 8
Size: 48 Color: 5

Bin 66: 2102 of cap free
Amount of items: 6
Items: 
Size: 56 Color: 9
Size: 52 Color: 16
Size: 50 Color: 19
Size: 48 Color: 8
Size: 48 Color: 7
Size: 44 Color: 11

Total size: 156000
Total free space: 2400

