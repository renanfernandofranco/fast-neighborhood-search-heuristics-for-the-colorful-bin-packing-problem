Capicity Bin: 2472
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1278 Color: 13
Size: 998 Color: 7
Size: 196 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1482 Color: 6
Size: 830 Color: 8
Size: 160 Color: 12

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 1561 Color: 12
Size: 561 Color: 4
Size: 214 Color: 19
Size: 136 Color: 2

Bin 4: 0 of cap free
Amount of items: 4
Items: 
Size: 1604 Color: 15
Size: 686 Color: 11
Size: 96 Color: 19
Size: 86 Color: 6

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 9
Size: 658 Color: 15
Size: 128 Color: 9

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1708 Color: 3
Size: 644 Color: 1
Size: 120 Color: 9

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 9
Size: 542 Color: 11
Size: 104 Color: 9

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 2035 Color: 19
Size: 365 Color: 7
Size: 72 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 4
Size: 366 Color: 12
Size: 72 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 2044 Color: 5
Size: 364 Color: 7
Size: 64 Color: 19

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 2079 Color: 16
Size: 297 Color: 13
Size: 96 Color: 8

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 2105 Color: 15
Size: 281 Color: 17
Size: 86 Color: 10

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 2118 Color: 17
Size: 302 Color: 14
Size: 52 Color: 4

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 2148 Color: 19
Size: 192 Color: 17
Size: 132 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 2142 Color: 13
Size: 258 Color: 3
Size: 72 Color: 10

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 2145 Color: 18
Size: 273 Color: 9
Size: 54 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 2204 Color: 19
Size: 160 Color: 11
Size: 108 Color: 13

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 2166 Color: 5
Size: 284 Color: 6
Size: 22 Color: 8

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2202 Color: 18
Size: 226 Color: 11
Size: 44 Color: 19

Bin 20: 1 of cap free
Amount of items: 3
Items: 
Size: 1239 Color: 8
Size: 1028 Color: 10
Size: 204 Color: 19

Bin 21: 1 of cap free
Amount of items: 4
Items: 
Size: 1244 Color: 13
Size: 1029 Color: 4
Size: 150 Color: 5
Size: 48 Color: 18

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1569 Color: 11
Size: 828 Color: 8
Size: 74 Color: 4

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 6
Size: 645 Color: 5
Size: 148 Color: 2

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1947 Color: 16
Size: 476 Color: 5
Size: 48 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1955 Color: 0
Size: 384 Color: 19
Size: 132 Color: 7

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 2019 Color: 9
Size: 272 Color: 0
Size: 180 Color: 15

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 2068 Color: 4
Size: 379 Color: 10
Size: 24 Color: 13

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 2097 Color: 13
Size: 276 Color: 4
Size: 98 Color: 10

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 2153 Color: 4
Size: 318 Color: 0

Bin 30: 2 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 9
Size: 978 Color: 1
Size: 88 Color: 16

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1484 Color: 19
Size: 810 Color: 1
Size: 176 Color: 4

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1908 Color: 7
Size: 546 Color: 4
Size: 16 Color: 2

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 2137 Color: 4
Size: 307 Color: 3
Size: 26 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 2218 Color: 5
Size: 244 Color: 14
Size: 8 Color: 0

Bin 35: 3 of cap free
Amount of items: 2
Items: 
Size: 1415 Color: 19
Size: 1054 Color: 7

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1691 Color: 0
Size: 724 Color: 9
Size: 54 Color: 19

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1799 Color: 3
Size: 670 Color: 6

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1818 Color: 15
Size: 651 Color: 4

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1938 Color: 10
Size: 491 Color: 1
Size: 40 Color: 1

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1956 Color: 6
Size: 497 Color: 9
Size: 16 Color: 14

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 2030 Color: 15
Size: 439 Color: 11

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 2140 Color: 9
Size: 329 Color: 6

Bin 43: 4 of cap free
Amount of items: 3
Items: 
Size: 1860 Color: 13
Size: 572 Color: 19
Size: 36 Color: 2

Bin 44: 4 of cap free
Amount of items: 3
Items: 
Size: 1934 Color: 0
Size: 446 Color: 19
Size: 88 Color: 5

Bin 45: 5 of cap free
Amount of items: 2
Items: 
Size: 1963 Color: 4
Size: 504 Color: 10

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 2110 Color: 13
Size: 349 Color: 0
Size: 8 Color: 19

Bin 47: 6 of cap free
Amount of items: 5
Items: 
Size: 1237 Color: 19
Size: 431 Color: 4
Size: 370 Color: 5
Size: 278 Color: 1
Size: 150 Color: 11

Bin 48: 7 of cap free
Amount of items: 3
Items: 
Size: 1552 Color: 0
Size: 881 Color: 4
Size: 32 Color: 3

Bin 49: 7 of cap free
Amount of items: 3
Items: 
Size: 1670 Color: 2
Size: 747 Color: 5
Size: 48 Color: 11

Bin 50: 7 of cap free
Amount of items: 2
Items: 
Size: 1704 Color: 15
Size: 761 Color: 11

Bin 51: 8 of cap free
Amount of items: 3
Items: 
Size: 1238 Color: 0
Size: 1022 Color: 19
Size: 204 Color: 3

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 2013 Color: 9
Size: 450 Color: 12

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 2027 Color: 7
Size: 436 Color: 5

Bin 54: 10 of cap free
Amount of items: 3
Items: 
Size: 1302 Color: 2
Size: 1000 Color: 18
Size: 160 Color: 12

Bin 55: 11 of cap free
Amount of items: 2
Items: 
Size: 1885 Color: 16
Size: 576 Color: 8

Bin 56: 12 of cap free
Amount of items: 2
Items: 
Size: 1577 Color: 18
Size: 883 Color: 0

Bin 57: 14 of cap free
Amount of items: 3
Items: 
Size: 1650 Color: 15
Size: 425 Color: 2
Size: 383 Color: 4

Bin 58: 14 of cap free
Amount of items: 3
Items: 
Size: 1788 Color: 10
Size: 596 Color: 5
Size: 74 Color: 19

Bin 59: 15 of cap free
Amount of items: 3
Items: 
Size: 1246 Color: 18
Size: 840 Color: 1
Size: 371 Color: 11

Bin 60: 17 of cap free
Amount of items: 2
Items: 
Size: 1793 Color: 1
Size: 662 Color: 18

Bin 61: 18 of cap free
Amount of items: 17
Items: 
Size: 340 Color: 7
Size: 272 Color: 17
Size: 204 Color: 9
Size: 200 Color: 8
Size: 176 Color: 14
Size: 144 Color: 12
Size: 130 Color: 0
Size: 128 Color: 6
Size: 120 Color: 18
Size: 112 Color: 7
Size: 112 Color: 5
Size: 112 Color: 2
Size: 88 Color: 3
Size: 88 Color: 1
Size: 84 Color: 16
Size: 80 Color: 11
Size: 64 Color: 1

Bin 62: 20 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 12
Size: 753 Color: 3

Bin 63: 22 of cap free
Amount of items: 2
Items: 
Size: 1502 Color: 14
Size: 948 Color: 19

Bin 64: 28 of cap free
Amount of items: 2
Items: 
Size: 1413 Color: 16
Size: 1031 Color: 13

Bin 65: 28 of cap free
Amount of items: 2
Items: 
Size: 1877 Color: 6
Size: 567 Color: 9

Bin 66: 2148 of cap free
Amount of items: 5
Items: 
Size: 76 Color: 18
Size: 64 Color: 15
Size: 64 Color: 6
Size: 60 Color: 5
Size: 60 Color: 4

Total size: 160680
Total free space: 2472

