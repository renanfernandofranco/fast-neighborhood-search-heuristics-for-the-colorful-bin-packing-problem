Capicity Bin: 1940
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 973 Color: 1
Size: 705 Color: 1
Size: 160 Color: 0
Size: 60 Color: 0
Size: 42 Color: 0

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1095 Color: 1
Size: 402 Color: 1
Size: 333 Color: 1
Size: 64 Color: 0
Size: 46 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 1
Size: 365 Color: 1
Size: 34 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1737 Color: 1
Size: 171 Color: 1
Size: 32 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 1
Size: 241 Color: 1
Size: 128 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1533 Color: 1
Size: 371 Color: 1
Size: 36 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1723 Color: 1
Size: 177 Color: 1
Size: 40 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 1
Size: 213 Color: 1
Size: 42 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1405 Color: 1
Size: 447 Color: 1
Size: 88 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1602 Color: 1
Size: 286 Color: 1
Size: 52 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 1570 Color: 1
Size: 258 Color: 1
Size: 64 Color: 0
Size: 48 Color: 0

Bin 12: 0 of cap free
Amount of items: 5
Items: 
Size: 1293 Color: 1
Size: 303 Color: 1
Size: 244 Color: 1
Size: 72 Color: 0
Size: 28 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 1
Size: 191 Color: 1
Size: 102 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1495 Color: 1
Size: 309 Color: 1
Size: 136 Color: 0

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 974 Color: 1
Size: 838 Color: 1
Size: 76 Color: 0
Size: 52 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 1
Size: 170 Color: 1
Size: 80 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1549 Color: 1
Size: 251 Color: 1
Size: 140 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 1
Size: 569 Color: 1
Size: 46 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 1
Size: 466 Color: 1
Size: 52 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1414 Color: 1
Size: 404 Color: 1
Size: 122 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1639 Color: 1
Size: 227 Color: 1
Size: 74 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1601 Color: 1
Size: 283 Color: 1
Size: 56 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1661 Color: 1
Size: 245 Color: 1
Size: 34 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 1451 Color: 1
Size: 409 Color: 1
Size: 80 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1678 Color: 1
Size: 222 Color: 1
Size: 40 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1097 Color: 1
Size: 683 Color: 1
Size: 160 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1282 Color: 1
Size: 442 Color: 1
Size: 216 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1715 Color: 1
Size: 181 Color: 1
Size: 44 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1595 Color: 1
Size: 289 Color: 1
Size: 56 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1098 Color: 1
Size: 738 Color: 1
Size: 104 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 1397 Color: 1
Size: 327 Color: 1
Size: 216 Color: 0

Bin 32: 0 of cap free
Amount of items: 5
Items: 
Size: 971 Color: 1
Size: 541 Color: 1
Size: 270 Color: 1
Size: 106 Color: 0
Size: 52 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1215 Color: 1
Size: 645 Color: 1
Size: 80 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1199 Color: 1
Size: 619 Color: 1
Size: 122 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1542 Color: 1
Size: 366 Color: 1
Size: 32 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1340 Color: 1
Size: 550 Color: 1
Size: 50 Color: 0

Bin 37: 0 of cap free
Amount of items: 5
Items: 
Size: 1577 Color: 1
Size: 221 Color: 1
Size: 90 Color: 0
Size: 44 Color: 0
Size: 8 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1503 Color: 1
Size: 341 Color: 1
Size: 96 Color: 0

Bin 39: 0 of cap free
Amount of items: 5
Items: 
Size: 1218 Color: 1
Size: 346 Color: 1
Size: 210 Color: 1
Size: 100 Color: 0
Size: 66 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1259 Color: 1
Size: 521 Color: 1
Size: 160 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 1
Size: 191 Color: 1
Size: 96 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 1
Size: 207 Color: 1
Size: 40 Color: 0

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 1216 Color: 1
Size: 611 Color: 1
Size: 112 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1669 Color: 1
Size: 206 Color: 1
Size: 64 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1729 Color: 1
Size: 166 Color: 1
Size: 44 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1354 Color: 1
Size: 513 Color: 1
Size: 72 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 1
Size: 233 Color: 1
Size: 72 Color: 0

Bin 48: 1 of cap free
Amount of items: 5
Items: 
Size: 807 Color: 1
Size: 724 Color: 1
Size: 300 Color: 1
Size: 60 Color: 0
Size: 48 Color: 0

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 1502 Color: 1
Size: 334 Color: 1
Size: 102 Color: 0

Bin 50: 2 of cap free
Amount of items: 5
Items: 
Size: 809 Color: 1
Size: 507 Color: 1
Size: 490 Color: 1
Size: 84 Color: 0
Size: 48 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 1
Size: 448 Color: 1
Size: 32 Color: 0

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 1333 Color: 1
Size: 587 Color: 1
Size: 16 Color: 0

Bin 53: 5 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 1
Size: 626 Color: 1
Size: 8 Color: 0

Bin 54: 9 of cap free
Amount of items: 3
Items: 
Size: 1462 Color: 1
Size: 403 Color: 1
Size: 66 Color: 0

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 1
Size: 656 Color: 1
Size: 108 Color: 0

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 1121 Color: 1
Size: 783 Color: 1
Size: 24 Color: 0

Bin 57: 16 of cap free
Amount of items: 3
Items: 
Size: 1058 Color: 1
Size: 806 Color: 1
Size: 60 Color: 0

Bin 58: 31 of cap free
Amount of items: 3
Items: 
Size: 1207 Color: 1
Size: 646 Color: 1
Size: 56 Color: 0

Bin 59: 130 of cap free
Amount of items: 3
Items: 
Size: 1317 Color: 1
Size: 453 Color: 1
Size: 40 Color: 0

Bin 60: 198 of cap free
Amount of items: 1
Items: 
Size: 1742 Color: 1

Bin 61: 202 of cap free
Amount of items: 1
Items: 
Size: 1738 Color: 1

Bin 62: 214 of cap free
Amount of items: 1
Items: 
Size: 1726 Color: 1

Bin 63: 219 of cap free
Amount of items: 1
Items: 
Size: 1721 Color: 1

Bin 64: 263 of cap free
Amount of items: 1
Items: 
Size: 1677 Color: 1

Bin 65: 292 of cap free
Amount of items: 1
Items: 
Size: 1648 Color: 1

Bin 66: 322 of cap free
Amount of items: 1
Items: 
Size: 1618 Color: 1

Total size: 126100
Total free space: 1940

