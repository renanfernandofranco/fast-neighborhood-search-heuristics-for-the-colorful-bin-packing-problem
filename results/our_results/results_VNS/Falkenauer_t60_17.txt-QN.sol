Capicity Bin: 1000
Lower Bound: 20

Bins used: 20
Amount of Colors: 60

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 42
Size: 302 Color: 34
Size: 292 Color: 31

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 50
Size: 294 Color: 33
Size: 251 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 53
Size: 278 Color: 23
Size: 255 Color: 9

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 44
Size: 309 Color: 36
Size: 269 Color: 21

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 46
Size: 306 Color: 35
Size: 266 Color: 18

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 43
Size: 333 Color: 37
Size: 256 Color: 10

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 40
Size: 355 Color: 39
Size: 251 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 51
Size: 279 Color: 25
Size: 262 Color: 15

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 45
Size: 290 Color: 30
Size: 285 Color: 29

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 57
Size: 258 Color: 12
Size: 250 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 59
Size: 253 Color: 5
Size: 251 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 56
Size: 257 Color: 11
Size: 254 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 52
Size: 281 Color: 27
Size: 260 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 41
Size: 342 Color: 38
Size: 255 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 54
Size: 267 Color: 19
Size: 264 Color: 16

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 47
Size: 294 Color: 32
Size: 270 Color: 22

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 58
Size: 255 Color: 8
Size: 250 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 49
Size: 279 Color: 26
Size: 268 Color: 20

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 55
Size: 264 Color: 17
Size: 258 Color: 13

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 48
Size: 285 Color: 28
Size: 278 Color: 24

Total size: 20000
Total free space: 0

