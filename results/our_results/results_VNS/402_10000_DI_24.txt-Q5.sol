Capicity Bin: 8192
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 29
Items: 
Size: 512 Color: 1
Size: 512 Color: 1
Size: 464 Color: 0
Size: 432 Color: 0
Size: 372 Color: 1
Size: 372 Color: 0
Size: 366 Color: 2
Size: 352 Color: 2
Size: 320 Color: 4
Size: 314 Color: 1
Size: 292 Color: 1
Size: 288 Color: 2
Size: 288 Color: 2
Size: 264 Color: 2
Size: 264 Color: 1
Size: 264 Color: 1
Size: 264 Color: 0
Size: 254 Color: 3
Size: 232 Color: 2
Size: 200 Color: 3
Size: 194 Color: 4
Size: 190 Color: 3
Size: 186 Color: 3
Size: 184 Color: 3
Size: 176 Color: 3
Size: 160 Color: 4
Size: 160 Color: 4
Size: 160 Color: 4
Size: 156 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 2
Size: 2954 Color: 4
Size: 588 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4676 Color: 1
Size: 3308 Color: 4
Size: 208 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5069 Color: 2
Size: 1797 Color: 0
Size: 1326 Color: 3

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5530 Color: 2
Size: 2322 Color: 4
Size: 340 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5705 Color: 2
Size: 1983 Color: 3
Size: 504 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5882 Color: 2
Size: 2162 Color: 0
Size: 148 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 4
Size: 1862 Color: 1
Size: 380 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6037 Color: 3
Size: 1763 Color: 4
Size: 392 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6148 Color: 4
Size: 1804 Color: 3
Size: 240 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6250 Color: 2
Size: 1628 Color: 4
Size: 314 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6297 Color: 2
Size: 1567 Color: 0
Size: 328 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6462 Color: 4
Size: 1568 Color: 0
Size: 162 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6569 Color: 4
Size: 1255 Color: 3
Size: 368 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6596 Color: 4
Size: 1332 Color: 2
Size: 264 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 4
Size: 1389 Color: 0
Size: 166 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 1
Size: 1321 Color: 4
Size: 262 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6657 Color: 4
Size: 1251 Color: 0
Size: 284 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 4
Size: 1324 Color: 1
Size: 112 Color: 2

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6726 Color: 2
Size: 1222 Color: 4
Size: 244 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6750 Color: 1
Size: 922 Color: 0
Size: 520 Color: 4

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6780 Color: 3
Size: 836 Color: 4
Size: 576 Color: 2

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6886 Color: 4
Size: 1090 Color: 3
Size: 216 Color: 2

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6862 Color: 0
Size: 1018 Color: 4
Size: 312 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6884 Color: 0
Size: 872 Color: 4
Size: 436 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6972 Color: 1
Size: 1076 Color: 0
Size: 144 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6978 Color: 1
Size: 640 Color: 4
Size: 574 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7044 Color: 4
Size: 982 Color: 3
Size: 166 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7037 Color: 1
Size: 1019 Color: 4
Size: 136 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7049 Color: 1
Size: 937 Color: 0
Size: 206 Color: 4

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7086 Color: 4
Size: 730 Color: 0
Size: 376 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7122 Color: 2
Size: 656 Color: 3
Size: 414 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 1
Size: 718 Color: 0
Size: 314 Color: 4

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7196 Color: 3
Size: 764 Color: 3
Size: 232 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7252 Color: 1
Size: 716 Color: 1
Size: 224 Color: 4

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 7280 Color: 1
Size: 588 Color: 4
Size: 324 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 7286 Color: 2
Size: 742 Color: 0
Size: 164 Color: 4

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 0
Size: 706 Color: 4
Size: 184 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 3
Size: 552 Color: 2
Size: 322 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 7334 Color: 1
Size: 682 Color: 0
Size: 176 Color: 4

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 7346 Color: 3
Size: 462 Color: 1
Size: 384 Color: 4

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5067 Color: 2
Size: 2908 Color: 4
Size: 216 Color: 1

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5958 Color: 0
Size: 2033 Color: 0
Size: 200 Color: 4

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 6242 Color: 3
Size: 1613 Color: 4
Size: 336 Color: 2

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6313 Color: 1
Size: 1650 Color: 4
Size: 228 Color: 3

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 6438 Color: 4
Size: 1313 Color: 3
Size: 440 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 6613 Color: 4
Size: 1202 Color: 1
Size: 376 Color: 3

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 6661 Color: 4
Size: 1126 Color: 1
Size: 404 Color: 2

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 6693 Color: 0
Size: 1498 Color: 2

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6819 Color: 1
Size: 1110 Color: 4
Size: 262 Color: 2

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6908 Color: 4
Size: 1187 Color: 1
Size: 96 Color: 2

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6921 Color: 4
Size: 1078 Color: 0
Size: 192 Color: 2

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6949 Color: 4
Size: 1024 Color: 0
Size: 218 Color: 3

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 3
Size: 1014 Color: 4
Size: 152 Color: 2

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 7069 Color: 4
Size: 764 Color: 0
Size: 358 Color: 3

Bin 56: 2 of cap free
Amount of items: 5
Items: 
Size: 4100 Color: 4
Size: 1573 Color: 4
Size: 1353 Color: 3
Size: 758 Color: 1
Size: 406 Color: 0

Bin 57: 2 of cap free
Amount of items: 3
Items: 
Size: 4653 Color: 4
Size: 2953 Color: 2
Size: 584 Color: 3

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 4649 Color: 2
Size: 3341 Color: 0
Size: 200 Color: 4

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5404 Color: 2
Size: 2570 Color: 3
Size: 216 Color: 0

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 5761 Color: 4
Size: 2281 Color: 0
Size: 148 Color: 1

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 6033 Color: 0
Size: 2037 Color: 0
Size: 120 Color: 4

Bin 62: 2 of cap free
Amount of items: 3
Items: 
Size: 6214 Color: 2
Size: 1092 Color: 4
Size: 884 Color: 2

Bin 63: 2 of cap free
Amount of items: 3
Items: 
Size: 6453 Color: 1
Size: 1229 Color: 2
Size: 508 Color: 4

Bin 64: 2 of cap free
Amount of items: 2
Items: 
Size: 6769 Color: 3
Size: 1421 Color: 2

Bin 65: 2 of cap free
Amount of items: 2
Items: 
Size: 6873 Color: 0
Size: 1317 Color: 2

Bin 66: 2 of cap free
Amount of items: 2
Items: 
Size: 7170 Color: 0
Size: 1020 Color: 3

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 7246 Color: 0
Size: 854 Color: 3
Size: 90 Color: 4

Bin 68: 3 of cap free
Amount of items: 3
Items: 
Size: 4860 Color: 0
Size: 3009 Color: 2
Size: 320 Color: 4

Bin 69: 3 of cap free
Amount of items: 3
Items: 
Size: 5749 Color: 0
Size: 2182 Color: 0
Size: 258 Color: 2

Bin 70: 3 of cap free
Amount of items: 3
Items: 
Size: 6036 Color: 2
Size: 2041 Color: 3
Size: 112 Color: 0

Bin 71: 3 of cap free
Amount of items: 2
Items: 
Size: 6356 Color: 3
Size: 1833 Color: 1

Bin 72: 3 of cap free
Amount of items: 2
Items: 
Size: 6617 Color: 3
Size: 1572 Color: 1

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 6985 Color: 1
Size: 1204 Color: 0

Bin 74: 4 of cap free
Amount of items: 3
Items: 
Size: 4609 Color: 4
Size: 3411 Color: 3
Size: 168 Color: 0

Bin 75: 4 of cap free
Amount of items: 3
Items: 
Size: 5110 Color: 4
Size: 2934 Color: 0
Size: 144 Color: 2

Bin 76: 4 of cap free
Amount of items: 3
Items: 
Size: 5172 Color: 0
Size: 2780 Color: 2
Size: 236 Color: 0

Bin 77: 4 of cap free
Amount of items: 3
Items: 
Size: 5578 Color: 4
Size: 1850 Color: 1
Size: 760 Color: 2

Bin 78: 4 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 3
Size: 2315 Color: 2
Size: 120 Color: 1

Bin 79: 4 of cap free
Amount of items: 3
Items: 
Size: 5993 Color: 3
Size: 2027 Color: 1
Size: 168 Color: 4

Bin 80: 4 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 4
Size: 1451 Color: 1
Size: 436 Color: 0

Bin 81: 4 of cap free
Amount of items: 3
Items: 
Size: 6652 Color: 2
Size: 1492 Color: 1
Size: 44 Color: 4

Bin 82: 4 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 0
Size: 1365 Color: 1

Bin 83: 5 of cap free
Amount of items: 3
Items: 
Size: 4607 Color: 2
Size: 3412 Color: 1
Size: 168 Color: 3

Bin 84: 5 of cap free
Amount of items: 3
Items: 
Size: 5415 Color: 0
Size: 2524 Color: 2
Size: 248 Color: 3

Bin 85: 5 of cap free
Amount of items: 2
Items: 
Size: 7046 Color: 1
Size: 1141 Color: 3

Bin 86: 6 of cap free
Amount of items: 2
Items: 
Size: 5974 Color: 0
Size: 2212 Color: 1

Bin 87: 6 of cap free
Amount of items: 2
Items: 
Size: 6905 Color: 2
Size: 1281 Color: 3

Bin 88: 6 of cap free
Amount of items: 2
Items: 
Size: 7214 Color: 0
Size: 972 Color: 2

Bin 89: 7 of cap free
Amount of items: 3
Items: 
Size: 7180 Color: 3
Size: 973 Color: 0
Size: 32 Color: 4

Bin 90: 8 of cap free
Amount of items: 4
Items: 
Size: 4098 Color: 3
Size: 3410 Color: 2
Size: 406 Color: 4
Size: 270 Color: 2

Bin 91: 8 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 2
Size: 2534 Color: 4
Size: 496 Color: 0

Bin 92: 8 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 4
Size: 1916 Color: 0
Size: 456 Color: 2

Bin 93: 9 of cap free
Amount of items: 2
Items: 
Size: 6257 Color: 0
Size: 1926 Color: 3

Bin 94: 9 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 3
Size: 1581 Color: 2

Bin 95: 10 of cap free
Amount of items: 3
Items: 
Size: 6590 Color: 0
Size: 1452 Color: 3
Size: 140 Color: 4

Bin 96: 11 of cap free
Amount of items: 3
Items: 
Size: 6641 Color: 3
Size: 1462 Color: 0
Size: 78 Color: 1

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 6842 Color: 3
Size: 1338 Color: 2

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 5130 Color: 4
Size: 3048 Color: 1

Bin 99: 14 of cap free
Amount of items: 3
Items: 
Size: 5570 Color: 4
Size: 2202 Color: 4
Size: 406 Color: 2

Bin 100: 14 of cap free
Amount of items: 2
Items: 
Size: 7284 Color: 0
Size: 894 Color: 2

Bin 101: 14 of cap free
Amount of items: 3
Items: 
Size: 7324 Color: 3
Size: 790 Color: 1
Size: 64 Color: 4

Bin 102: 15 of cap free
Amount of items: 2
Items: 
Size: 7140 Color: 2
Size: 1037 Color: 0

Bin 103: 16 of cap free
Amount of items: 4
Items: 
Size: 4097 Color: 3
Size: 2554 Color: 0
Size: 1061 Color: 4
Size: 464 Color: 3

Bin 104: 16 of cap free
Amount of items: 2
Items: 
Size: 5244 Color: 3
Size: 2932 Color: 4

Bin 105: 16 of cap free
Amount of items: 2
Items: 
Size: 6881 Color: 3
Size: 1295 Color: 2

Bin 106: 17 of cap free
Amount of items: 3
Items: 
Size: 5902 Color: 0
Size: 1232 Color: 3
Size: 1041 Color: 4

Bin 107: 17 of cap free
Amount of items: 2
Items: 
Size: 6305 Color: 2
Size: 1870 Color: 0

Bin 108: 22 of cap free
Amount of items: 2
Items: 
Size: 6544 Color: 3
Size: 1626 Color: 1

Bin 109: 24 of cap free
Amount of items: 2
Items: 
Size: 7276 Color: 3
Size: 892 Color: 2

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 7340 Color: 1
Size: 828 Color: 3

Bin 111: 29 of cap free
Amount of items: 2
Items: 
Size: 7070 Color: 0
Size: 1093 Color: 2

Bin 112: 32 of cap free
Amount of items: 2
Items: 
Size: 6452 Color: 0
Size: 1708 Color: 1

Bin 113: 33 of cap free
Amount of items: 2
Items: 
Size: 5554 Color: 0
Size: 2605 Color: 3

Bin 114: 36 of cap free
Amount of items: 2
Items: 
Size: 7028 Color: 0
Size: 1128 Color: 3

Bin 115: 38 of cap free
Amount of items: 2
Items: 
Size: 6244 Color: 0
Size: 1910 Color: 3

Bin 116: 38 of cap free
Amount of items: 2
Items: 
Size: 6974 Color: 1
Size: 1180 Color: 0

Bin 117: 40 of cap free
Amount of items: 2
Items: 
Size: 5900 Color: 0
Size: 2252 Color: 4

Bin 118: 42 of cap free
Amount of items: 2
Items: 
Size: 6077 Color: 3
Size: 2073 Color: 2

Bin 119: 43 of cap free
Amount of items: 2
Items: 
Size: 4101 Color: 2
Size: 4048 Color: 3

Bin 120: 49 of cap free
Amount of items: 3
Items: 
Size: 4228 Color: 1
Size: 2951 Color: 4
Size: 964 Color: 2

Bin 121: 56 of cap free
Amount of items: 2
Items: 
Size: 4544 Color: 0
Size: 3592 Color: 2

Bin 122: 56 of cap free
Amount of items: 2
Items: 
Size: 5676 Color: 3
Size: 2460 Color: 1

Bin 123: 64 of cap free
Amount of items: 3
Items: 
Size: 5540 Color: 4
Size: 1988 Color: 1
Size: 600 Color: 2

Bin 124: 70 of cap free
Amount of items: 2
Items: 
Size: 4708 Color: 2
Size: 3414 Color: 0

Bin 125: 83 of cap free
Amount of items: 2
Items: 
Size: 6487 Color: 2
Size: 1622 Color: 0

Bin 126: 83 of cap free
Amount of items: 2
Items: 
Size: 6532 Color: 0
Size: 1577 Color: 3

Bin 127: 88 of cap free
Amount of items: 2
Items: 
Size: 5918 Color: 2
Size: 2186 Color: 1

Bin 128: 99 of cap free
Amount of items: 12
Items: 
Size: 1297 Color: 3
Size: 818 Color: 4
Size: 724 Color: 2
Size: 680 Color: 1
Size: 680 Color: 1
Size: 680 Color: 0
Size: 680 Color: 0
Size: 590 Color: 1
Size: 584 Color: 0
Size: 520 Color: 2
Size: 488 Color: 2
Size: 352 Color: 4

Bin 129: 105 of cap free
Amount of items: 3
Items: 
Size: 4102 Color: 3
Size: 3032 Color: 4
Size: 953 Color: 1

Bin 130: 105 of cap free
Amount of items: 2
Items: 
Size: 4674 Color: 2
Size: 3413 Color: 3

Bin 131: 123 of cap free
Amount of items: 2
Items: 
Size: 5745 Color: 3
Size: 2324 Color: 4

Bin 132: 134 of cap free
Amount of items: 2
Items: 
Size: 5455 Color: 3
Size: 2603 Color: 0

Bin 133: 6316 of cap free
Amount of items: 10
Items: 
Size: 262 Color: 1
Size: 244 Color: 2
Size: 232 Color: 0
Size: 220 Color: 1
Size: 210 Color: 0
Size: 152 Color: 4
Size: 144 Color: 3
Size: 140 Color: 3
Size: 136 Color: 4
Size: 136 Color: 4

Total size: 1081344
Total free space: 8192

