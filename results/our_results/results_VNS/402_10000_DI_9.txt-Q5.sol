Capicity Bin: 9808
Lower Bound: 132

Bins used: 133
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 4964 Color: 1
Size: 4044 Color: 2
Size: 800 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4932 Color: 2
Size: 4068 Color: 3
Size: 808 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5716 Color: 1
Size: 3818 Color: 2
Size: 274 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5748 Color: 1
Size: 3784 Color: 0
Size: 276 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6103 Color: 1
Size: 3519 Color: 2
Size: 186 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6637 Color: 4
Size: 2491 Color: 1
Size: 680 Color: 2

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6990 Color: 0
Size: 2350 Color: 3
Size: 468 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 7428 Color: 1
Size: 1400 Color: 4
Size: 980 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 7652 Color: 3
Size: 1804 Color: 2
Size: 352 Color: 3

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 7785 Color: 1
Size: 1687 Color: 4
Size: 336 Color: 2

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 7766 Color: 2
Size: 1856 Color: 1
Size: 186 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 7806 Color: 0
Size: 1186 Color: 3
Size: 816 Color: 2

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 7836 Color: 3
Size: 1064 Color: 3
Size: 908 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 7965 Color: 4
Size: 1503 Color: 3
Size: 340 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 7990 Color: 0
Size: 1242 Color: 1
Size: 576 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 8214 Color: 3
Size: 1244 Color: 4
Size: 350 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 8227 Color: 3
Size: 1319 Color: 2
Size: 262 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 8310 Color: 2
Size: 1330 Color: 2
Size: 168 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 8280 Color: 0
Size: 1288 Color: 2
Size: 240 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 8378 Color: 2
Size: 966 Color: 4
Size: 464 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 8434 Color: 2
Size: 1074 Color: 1
Size: 300 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 8436 Color: 2
Size: 874 Color: 0
Size: 498 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 8490 Color: 1
Size: 986 Color: 2
Size: 332 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 8533 Color: 3
Size: 1051 Color: 2
Size: 224 Color: 3

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 8572 Color: 3
Size: 700 Color: 0
Size: 536 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 8644 Color: 2
Size: 864 Color: 3
Size: 300 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 8748 Color: 2
Size: 816 Color: 4
Size: 244 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 8742 Color: 1
Size: 856 Color: 2
Size: 210 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 8762 Color: 0
Size: 888 Color: 2
Size: 158 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 8792 Color: 3
Size: 828 Color: 4
Size: 188 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 8820 Color: 3
Size: 816 Color: 1
Size: 172 Color: 2

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6815 Color: 1
Size: 2840 Color: 2
Size: 152 Color: 4

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 8155 Color: 4
Size: 1504 Color: 3
Size: 148 Color: 4

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 8182 Color: 2
Size: 1537 Color: 0
Size: 88 Color: 1

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 8275 Color: 4
Size: 1326 Color: 3
Size: 206 Color: 4

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 8407 Color: 4
Size: 1184 Color: 2
Size: 216 Color: 3

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 8442 Color: 2
Size: 1105 Color: 4
Size: 260 Color: 3

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 8563 Color: 0
Size: 1036 Color: 3
Size: 208 Color: 1

Bin 39: 2 of cap free
Amount of items: 5
Items: 
Size: 4905 Color: 4
Size: 2643 Color: 2
Size: 1490 Color: 3
Size: 592 Color: 2
Size: 176 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 4948 Color: 1
Size: 4584 Color: 1
Size: 274 Color: 2

Bin 41: 2 of cap free
Amount of items: 3
Items: 
Size: 4916 Color: 3
Size: 4410 Color: 4
Size: 480 Color: 1

Bin 42: 2 of cap free
Amount of items: 3
Items: 
Size: 6146 Color: 0
Size: 3412 Color: 1
Size: 248 Color: 2

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 6292 Color: 3
Size: 3514 Color: 4

Bin 44: 2 of cap free
Amount of items: 2
Items: 
Size: 7290 Color: 0
Size: 2516 Color: 4

Bin 45: 2 of cap free
Amount of items: 3
Items: 
Size: 7461 Color: 2
Size: 2105 Color: 1
Size: 240 Color: 3

Bin 46: 2 of cap free
Amount of items: 2
Items: 
Size: 7594 Color: 0
Size: 2212 Color: 3

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 7798 Color: 1
Size: 1702 Color: 3
Size: 306 Color: 2

Bin 48: 2 of cap free
Amount of items: 2
Items: 
Size: 7960 Color: 2
Size: 1846 Color: 1

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 8107 Color: 1
Size: 1379 Color: 1
Size: 320 Color: 2

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 8124 Color: 1
Size: 1194 Color: 0
Size: 488 Color: 3

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 8136 Color: 3
Size: 1670 Color: 0

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 8236 Color: 0
Size: 890 Color: 4
Size: 680 Color: 3

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 8483 Color: 1
Size: 1195 Color: 0
Size: 128 Color: 2

Bin 54: 2 of cap free
Amount of items: 3
Items: 
Size: 8578 Color: 1
Size: 932 Color: 3
Size: 296 Color: 4

Bin 55: 2 of cap free
Amount of items: 3
Items: 
Size: 8650 Color: 2
Size: 584 Color: 2
Size: 572 Color: 3

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 5618 Color: 2
Size: 2908 Color: 3
Size: 1279 Color: 1

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 7151 Color: 4
Size: 2322 Color: 1
Size: 332 Color: 2

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 7477 Color: 2
Size: 2184 Color: 1
Size: 144 Color: 3

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 8005 Color: 3
Size: 1358 Color: 2
Size: 442 Color: 1

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 8287 Color: 3
Size: 1518 Color: 4

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 1
Size: 1419 Color: 0
Size: 64 Color: 2

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 8536 Color: 0
Size: 1269 Color: 1

Bin 63: 4 of cap free
Amount of items: 3
Items: 
Size: 6101 Color: 4
Size: 3543 Color: 2
Size: 160 Color: 3

Bin 64: 4 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 1
Size: 3272 Color: 2
Size: 192 Color: 0

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 6586 Color: 4
Size: 3042 Color: 3
Size: 176 Color: 1

Bin 66: 4 of cap free
Amount of items: 3
Items: 
Size: 6598 Color: 3
Size: 3054 Color: 1
Size: 152 Color: 3

Bin 67: 4 of cap free
Amount of items: 3
Items: 
Size: 7156 Color: 1
Size: 2488 Color: 4
Size: 160 Color: 4

Bin 68: 4 of cap free
Amount of items: 3
Items: 
Size: 7318 Color: 1
Size: 1678 Color: 2
Size: 808 Color: 4

Bin 69: 5 of cap free
Amount of items: 3
Items: 
Size: 5559 Color: 2
Size: 4084 Color: 4
Size: 160 Color: 3

Bin 70: 5 of cap free
Amount of items: 3
Items: 
Size: 7283 Color: 3
Size: 1988 Color: 4
Size: 532 Color: 1

Bin 71: 5 of cap free
Amount of items: 3
Items: 
Size: 7623 Color: 3
Size: 1928 Color: 2
Size: 252 Color: 1

Bin 72: 5 of cap free
Amount of items: 3
Items: 
Size: 8163 Color: 0
Size: 1440 Color: 2
Size: 200 Color: 0

Bin 73: 5 of cap free
Amount of items: 2
Items: 
Size: 8399 Color: 0
Size: 1404 Color: 4

Bin 74: 6 of cap free
Amount of items: 14
Items: 
Size: 1192 Color: 4
Size: 1188 Color: 4
Size: 846 Color: 4
Size: 760 Color: 2
Size: 752 Color: 3
Size: 708 Color: 3
Size: 708 Color: 1
Size: 700 Color: 4
Size: 696 Color: 4
Size: 640 Color: 4
Size: 528 Color: 0
Size: 440 Color: 0
Size: 432 Color: 0
Size: 212 Color: 1

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 5896 Color: 3
Size: 3522 Color: 2
Size: 384 Color: 1

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 7022 Color: 3
Size: 2596 Color: 2
Size: 184 Color: 1

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 7124 Color: 2
Size: 2678 Color: 3

Bin 78: 6 of cap free
Amount of items: 3
Items: 
Size: 7192 Color: 4
Size: 2190 Color: 2
Size: 420 Color: 4

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 7752 Color: 3
Size: 2050 Color: 4

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 8375 Color: 0
Size: 1427 Color: 1

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 4
Size: 3712 Color: 0
Size: 1169 Color: 4

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 8626 Color: 1
Size: 1175 Color: 2

Bin 83: 8 of cap free
Amount of items: 3
Items: 
Size: 6748 Color: 3
Size: 2892 Color: 2
Size: 160 Color: 2

Bin 84: 8 of cap free
Amount of items: 2
Items: 
Size: 8150 Color: 0
Size: 1650 Color: 4

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 8692 Color: 4
Size: 1108 Color: 0

Bin 86: 9 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 2
Size: 3541 Color: 2
Size: 672 Color: 1

Bin 87: 9 of cap free
Amount of items: 2
Items: 
Size: 8255 Color: 0
Size: 1544 Color: 2

Bin 88: 9 of cap free
Amount of items: 2
Items: 
Size: 8760 Color: 4
Size: 1039 Color: 1

Bin 89: 10 of cap free
Amount of items: 2
Items: 
Size: 8392 Color: 3
Size: 1406 Color: 1

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 5230 Color: 4
Size: 4087 Color: 1
Size: 480 Color: 0

Bin 91: 11 of cap free
Amount of items: 2
Items: 
Size: 5557 Color: 2
Size: 4240 Color: 1

Bin 92: 11 of cap free
Amount of items: 3
Items: 
Size: 6963 Color: 3
Size: 2650 Color: 0
Size: 184 Color: 1

Bin 93: 11 of cap free
Amount of items: 2
Items: 
Size: 8547 Color: 1
Size: 1250 Color: 4

Bin 94: 12 of cap free
Amount of items: 3
Items: 
Size: 6367 Color: 3
Size: 2869 Color: 1
Size: 560 Color: 0

Bin 95: 12 of cap free
Amount of items: 2
Items: 
Size: 6408 Color: 4
Size: 3388 Color: 3

Bin 96: 12 of cap free
Amount of items: 2
Items: 
Size: 7887 Color: 0
Size: 1909 Color: 3

Bin 97: 12 of cap free
Amount of items: 2
Items: 
Size: 8195 Color: 4
Size: 1601 Color: 1

Bin 98: 13 of cap free
Amount of items: 2
Items: 
Size: 7087 Color: 4
Size: 2708 Color: 2

Bin 99: 14 of cap free
Amount of items: 3
Items: 
Size: 8190 Color: 0
Size: 1516 Color: 2
Size: 88 Color: 3

Bin 100: 14 of cap free
Amount of items: 3
Items: 
Size: 8794 Color: 4
Size: 968 Color: 1
Size: 32 Color: 0

Bin 101: 15 of cap free
Amount of items: 3
Items: 
Size: 7562 Color: 4
Size: 1943 Color: 3
Size: 288 Color: 1

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 8410 Color: 1
Size: 1382 Color: 3

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 8574 Color: 2
Size: 1218 Color: 4

Bin 104: 20 of cap free
Amount of items: 3
Items: 
Size: 7996 Color: 1
Size: 1720 Color: 0
Size: 72 Color: 4

Bin 105: 21 of cap free
Amount of items: 3
Items: 
Size: 6796 Color: 3
Size: 2495 Color: 2
Size: 496 Color: 1

Bin 106: 22 of cap free
Amount of items: 3
Items: 
Size: 8324 Color: 2
Size: 1350 Color: 3
Size: 112 Color: 2

Bin 107: 29 of cap free
Amount of items: 2
Items: 
Size: 7701 Color: 0
Size: 2078 Color: 3

Bin 108: 29 of cap free
Amount of items: 2
Items: 
Size: 8022 Color: 3
Size: 1757 Color: 0

Bin 109: 29 of cap free
Amount of items: 2
Items: 
Size: 8484 Color: 4
Size: 1295 Color: 1

Bin 110: 30 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 1
Size: 4052 Color: 2
Size: 816 Color: 0

Bin 111: 30 of cap free
Amount of items: 2
Items: 
Size: 7904 Color: 3
Size: 1874 Color: 0

Bin 112: 34 of cap free
Amount of items: 5
Items: 
Size: 4906 Color: 1
Size: 3540 Color: 3
Size: 808 Color: 0
Size: 304 Color: 4
Size: 216 Color: 0

Bin 113: 36 of cap free
Amount of items: 2
Items: 
Size: 6840 Color: 4
Size: 2932 Color: 2

Bin 114: 38 of cap free
Amount of items: 2
Items: 
Size: 5684 Color: 2
Size: 4086 Color: 0

Bin 115: 42 of cap free
Amount of items: 2
Items: 
Size: 8122 Color: 0
Size: 1644 Color: 3

Bin 116: 42 of cap free
Amount of items: 2
Items: 
Size: 8664 Color: 4
Size: 1102 Color: 1

Bin 117: 43 of cap free
Amount of items: 6
Items: 
Size: 4904 Color: 3
Size: 1371 Color: 4
Size: 1316 Color: 4
Size: 1102 Color: 1
Size: 544 Color: 0
Size: 528 Color: 3

Bin 118: 46 of cap free
Amount of items: 2
Items: 
Size: 7741 Color: 1
Size: 2021 Color: 4

Bin 119: 52 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 3
Size: 3494 Color: 4
Size: 708 Color: 4

Bin 120: 53 of cap free
Amount of items: 2
Items: 
Size: 7830 Color: 3
Size: 1925 Color: 4

Bin 121: 65 of cap free
Amount of items: 4
Items: 
Size: 4908 Color: 3
Size: 3444 Color: 1
Size: 1063 Color: 0
Size: 328 Color: 4

Bin 122: 72 of cap free
Amount of items: 2
Items: 
Size: 4936 Color: 3
Size: 4800 Color: 4

Bin 123: 87 of cap free
Amount of items: 2
Items: 
Size: 5636 Color: 2
Size: 4085 Color: 4

Bin 124: 87 of cap free
Amount of items: 2
Items: 
Size: 6630 Color: 4
Size: 3091 Color: 0

Bin 125: 87 of cap free
Amount of items: 2
Items: 
Size: 7350 Color: 4
Size: 2371 Color: 3

Bin 126: 97 of cap free
Amount of items: 2
Items: 
Size: 7496 Color: 2
Size: 2215 Color: 0

Bin 127: 104 of cap free
Amount of items: 2
Items: 
Size: 6158 Color: 3
Size: 3546 Color: 2

Bin 128: 109 of cap free
Amount of items: 2
Items: 
Size: 4907 Color: 3
Size: 4792 Color: 1

Bin 129: 110 of cap free
Amount of items: 2
Items: 
Size: 7012 Color: 2
Size: 2686 Color: 4

Bin 130: 128 of cap free
Amount of items: 2
Items: 
Size: 5272 Color: 0
Size: 4408 Color: 1

Bin 131: 130 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 3
Size: 4084 Color: 0

Bin 132: 136 of cap free
Amount of items: 26
Items: 
Size: 632 Color: 4
Size: 616 Color: 1
Size: 608 Color: 4
Size: 608 Color: 4
Size: 474 Color: 1
Size: 412 Color: 3
Size: 408 Color: 1
Size: 392 Color: 2
Size: 388 Color: 3
Size: 384 Color: 1
Size: 372 Color: 3
Size: 368 Color: 0
Size: 336 Color: 0
Size: 328 Color: 2
Size: 328 Color: 0
Size: 326 Color: 2
Size: 296 Color: 3
Size: 282 Color: 2
Size: 280 Color: 3
Size: 280 Color: 1
Size: 272 Color: 1
Size: 268 Color: 4
Size: 264 Color: 0
Size: 258 Color: 0
Size: 256 Color: 0
Size: 236 Color: 2

Bin 133: 7592 of cap free
Amount of items: 10
Items: 
Size: 268 Color: 1
Size: 254 Color: 0
Size: 238 Color: 4
Size: 234 Color: 3
Size: 232 Color: 3
Size: 230 Color: 2
Size: 220 Color: 1
Size: 196 Color: 2
Size: 184 Color: 0
Size: 160 Color: 2

Total size: 1294656
Total free space: 9808

