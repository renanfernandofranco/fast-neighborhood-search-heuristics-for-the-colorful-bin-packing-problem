Capicity Bin: 19008
Lower Bound: 198

Bins used: 199
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 13263 Color: 1
Size: 3785 Color: 1
Size: 1260 Color: 0
Size: 700 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9505 Color: 1
Size: 7921 Color: 1
Size: 1582 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 13939 Color: 1
Size: 4225 Color: 1
Size: 844 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 12008 Color: 1
Size: 6212 Color: 1
Size: 788 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 13014 Color: 1
Size: 4998 Color: 1
Size: 996 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 13030 Color: 1
Size: 4982 Color: 1
Size: 996 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 12016 Color: 1
Size: 5840 Color: 1
Size: 1152 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 9528 Color: 1
Size: 7944 Color: 1
Size: 1536 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12964 Color: 1
Size: 5044 Color: 1
Size: 1000 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 9717 Color: 1
Size: 7743 Color: 1
Size: 1548 Color: 0

Bin 11: 0 of cap free
Amount of items: 4
Items: 
Size: 10704 Color: 1
Size: 4976 Color: 0
Size: 2354 Color: 1
Size: 974 Color: 0

Bin 12: 0 of cap free
Amount of items: 4
Items: 
Size: 9520 Color: 1
Size: 8816 Color: 1
Size: 640 Color: 0
Size: 32 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12920 Color: 1
Size: 5080 Color: 1
Size: 1008 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 9508 Color: 1
Size: 7924 Color: 1
Size: 1576 Color: 0

Bin 15: 0 of cap free
Amount of items: 4
Items: 
Size: 11430 Color: 1
Size: 4306 Color: 1
Size: 2272 Color: 0
Size: 1000 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 9513 Color: 1
Size: 7913 Color: 1
Size: 1582 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 9506 Color: 1
Size: 7922 Color: 1
Size: 1580 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 9376 Color: 1
Size: 8288 Color: 1
Size: 1344 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 15413 Color: 1
Size: 2997 Color: 1
Size: 598 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 9509 Color: 1
Size: 7917 Color: 1
Size: 1582 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 9522 Color: 1
Size: 7906 Color: 1
Size: 1580 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 9524 Color: 1
Size: 7908 Color: 1
Size: 1576 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 9538 Color: 1
Size: 7894 Color: 1
Size: 1576 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 9540 Color: 1
Size: 7892 Color: 1
Size: 1576 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 9556 Color: 1
Size: 7884 Color: 1
Size: 1568 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 9680 Color: 1
Size: 7792 Color: 1
Size: 1536 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 10872 Color: 1
Size: 7536 Color: 1
Size: 600 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 10884 Color: 1
Size: 6772 Color: 1
Size: 1352 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 11248 Color: 1
Size: 7104 Color: 1
Size: 656 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 12004 Color: 1
Size: 5844 Color: 1
Size: 1160 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 12020 Color: 1
Size: 5828 Color: 1
Size: 1160 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 12052 Color: 1
Size: 5804 Color: 1
Size: 1152 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 12294 Color: 1
Size: 5598 Color: 1
Size: 1116 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 12656 Color: 1
Size: 5776 Color: 1
Size: 576 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 12952 Color: 1
Size: 5256 Color: 1
Size: 800 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13520 Color: 1
Size: 4592 Color: 1
Size: 896 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13528 Color: 1
Size: 4364 Color: 1
Size: 1116 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13673 Color: 1
Size: 4447 Color: 1
Size: 888 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13698 Color: 1
Size: 3976 Color: 1
Size: 1334 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13778 Color: 1
Size: 4362 Color: 1
Size: 868 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 13780 Color: 1
Size: 3844 Color: 1
Size: 1384 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 13842 Color: 1
Size: 4494 Color: 1
Size: 672 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 13904 Color: 1
Size: 4016 Color: 1
Size: 1088 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14224 Color: 1
Size: 4272 Color: 1
Size: 512 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14248 Color: 1
Size: 4552 Color: 1
Size: 208 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14312 Color: 1
Size: 3928 Color: 1
Size: 768 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 1
Size: 3844 Color: 1
Size: 768 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14404 Color: 1
Size: 3036 Color: 1
Size: 1568 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 1
Size: 3804 Color: 1
Size: 752 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14478 Color: 1
Size: 3778 Color: 1
Size: 752 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14722 Color: 1
Size: 3574 Color: 1
Size: 712 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14808 Color: 1
Size: 3024 Color: 1
Size: 1176 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14840 Color: 1
Size: 2808 Color: 1
Size: 1360 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14940 Color: 1
Size: 3396 Color: 1
Size: 672 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14960 Color: 1
Size: 3480 Color: 1
Size: 568 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14964 Color: 1
Size: 3372 Color: 1
Size: 672 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14998 Color: 1
Size: 3342 Color: 1
Size: 668 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 15006 Color: 1
Size: 3338 Color: 1
Size: 664 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 15064 Color: 1
Size: 3240 Color: 1
Size: 704 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 15128 Color: 1
Size: 3512 Color: 1
Size: 368 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 15187 Color: 1
Size: 3185 Color: 1
Size: 636 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 15246 Color: 1
Size: 3138 Color: 1
Size: 624 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 15264 Color: 1
Size: 3536 Color: 1
Size: 208 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 15306 Color: 1
Size: 2700 Color: 1
Size: 1002 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 15372 Color: 1
Size: 3020 Color: 1
Size: 616 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 15388 Color: 1
Size: 2860 Color: 1
Size: 760 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 15408 Color: 1
Size: 3248 Color: 1
Size: 352 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 15420 Color: 1
Size: 3086 Color: 1
Size: 502 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 15498 Color: 1
Size: 2926 Color: 1
Size: 584 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 15544 Color: 1
Size: 3288 Color: 1
Size: 176 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 15656 Color: 1
Size: 2888 Color: 1
Size: 464 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 15658 Color: 1
Size: 2794 Color: 1
Size: 556 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 15735 Color: 1
Size: 2729 Color: 1
Size: 544 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 15748 Color: 1
Size: 2732 Color: 1
Size: 528 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 15754 Color: 1
Size: 2742 Color: 1
Size: 512 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 15788 Color: 1
Size: 2684 Color: 1
Size: 536 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 15792 Color: 1
Size: 2864 Color: 1
Size: 352 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 15820 Color: 1
Size: 2756 Color: 1
Size: 432 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 15822 Color: 1
Size: 2658 Color: 1
Size: 528 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 15857 Color: 1
Size: 2627 Color: 1
Size: 524 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 15896 Color: 1
Size: 2576 Color: 1
Size: 536 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 15920 Color: 1
Size: 2204 Color: 1
Size: 884 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 15976 Color: 1
Size: 3000 Color: 1
Size: 32 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 15985 Color: 1
Size: 2365 Color: 1
Size: 658 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 16002 Color: 1
Size: 2506 Color: 1
Size: 500 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 16018 Color: 1
Size: 2536 Color: 1
Size: 454 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 16028 Color: 1
Size: 2484 Color: 1
Size: 496 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 16065 Color: 1
Size: 2453 Color: 1
Size: 490 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 16136 Color: 1
Size: 2408 Color: 1
Size: 464 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 16186 Color: 1
Size: 2342 Color: 1
Size: 480 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 16202 Color: 1
Size: 2310 Color: 1
Size: 496 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 16208 Color: 1
Size: 2688 Color: 1
Size: 112 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 16284 Color: 1
Size: 2276 Color: 1
Size: 448 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 16296 Color: 1
Size: 1956 Color: 1
Size: 756 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 16309 Color: 1
Size: 2251 Color: 1
Size: 448 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 16340 Color: 1
Size: 2492 Color: 1
Size: 176 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 16372 Color: 1
Size: 2168 Color: 1
Size: 468 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 16408 Color: 1
Size: 2008 Color: 1
Size: 592 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 16422 Color: 1
Size: 2158 Color: 1
Size: 428 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 16454 Color: 1
Size: 2354 Color: 1
Size: 200 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 16464 Color: 1
Size: 2168 Color: 1
Size: 376 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 16471 Color: 1
Size: 2115 Color: 1
Size: 422 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 16492 Color: 1
Size: 2100 Color: 1
Size: 416 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 16534 Color: 1
Size: 1930 Color: 1
Size: 544 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 16567 Color: 1
Size: 2035 Color: 1
Size: 406 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 16575 Color: 1
Size: 2029 Color: 1
Size: 404 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 16580 Color: 1
Size: 2028 Color: 1
Size: 400 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 16583 Color: 1
Size: 2021 Color: 1
Size: 404 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 16616 Color: 1
Size: 2062 Color: 1
Size: 330 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 16664 Color: 1
Size: 1672 Color: 1
Size: 672 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 16668 Color: 1
Size: 1928 Color: 1
Size: 412 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 16670 Color: 1
Size: 1950 Color: 1
Size: 388 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 16687 Color: 1
Size: 2045 Color: 1
Size: 276 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 16688 Color: 1
Size: 1892 Color: 1
Size: 428 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 16694 Color: 1
Size: 1840 Color: 1
Size: 474 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 16695 Color: 1
Size: 1929 Color: 1
Size: 384 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 16740 Color: 1
Size: 1864 Color: 1
Size: 404 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 16756 Color: 1
Size: 2060 Color: 1
Size: 192 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 16769 Color: 1
Size: 1867 Color: 1
Size: 372 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 16798 Color: 1
Size: 1754 Color: 1
Size: 456 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 16815 Color: 1
Size: 1829 Color: 1
Size: 364 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 16816 Color: 1
Size: 1824 Color: 1
Size: 368 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 16863 Color: 1
Size: 1789 Color: 1
Size: 356 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 16864 Color: 1
Size: 1968 Color: 1
Size: 176 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 16900 Color: 1
Size: 1964 Color: 1
Size: 144 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 16916 Color: 1
Size: 1748 Color: 1
Size: 344 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 16938 Color: 1
Size: 1726 Color: 1
Size: 344 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 16952 Color: 1
Size: 1708 Color: 1
Size: 348 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 16964 Color: 1
Size: 1632 Color: 1
Size: 412 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 16976 Color: 1
Size: 1744 Color: 1
Size: 288 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 17002 Color: 1
Size: 1674 Color: 1
Size: 332 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 17016 Color: 1
Size: 1608 Color: 1
Size: 384 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 17018 Color: 1
Size: 1662 Color: 1
Size: 328 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 17036 Color: 1
Size: 1842 Color: 1
Size: 130 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 17066 Color: 1
Size: 1622 Color: 1
Size: 320 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 17076 Color: 1
Size: 1612 Color: 1
Size: 320 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 17086 Color: 1
Size: 1602 Color: 1
Size: 320 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 17096 Color: 1
Size: 1848 Color: 1
Size: 64 Color: 0

Bin 139: 1 of cap free
Amount of items: 3
Items: 
Size: 11047 Color: 1
Size: 6792 Color: 1
Size: 1168 Color: 0

Bin 140: 1 of cap free
Amount of items: 3
Items: 
Size: 11611 Color: 1
Size: 6302 Color: 1
Size: 1094 Color: 0

Bin 141: 1 of cap free
Amount of items: 3
Items: 
Size: 12117 Color: 1
Size: 6290 Color: 1
Size: 600 Color: 0

Bin 142: 1 of cap free
Amount of items: 3
Items: 
Size: 12980 Color: 1
Size: 5019 Color: 1
Size: 1008 Color: 0

Bin 143: 1 of cap free
Amount of items: 3
Items: 
Size: 12987 Color: 1
Size: 5028 Color: 1
Size: 992 Color: 0

Bin 144: 1 of cap free
Amount of items: 3
Items: 
Size: 14741 Color: 1
Size: 3354 Color: 1
Size: 912 Color: 0

Bin 145: 1 of cap free
Amount of items: 3
Items: 
Size: 14768 Color: 1
Size: 3951 Color: 1
Size: 288 Color: 0

Bin 146: 1 of cap free
Amount of items: 3
Items: 
Size: 15120 Color: 1
Size: 3503 Color: 1
Size: 384 Color: 0

Bin 147: 1 of cap free
Amount of items: 3
Items: 
Size: 15135 Color: 1
Size: 3376 Color: 1
Size: 496 Color: 0

Bin 148: 1 of cap free
Amount of items: 3
Items: 
Size: 15622 Color: 1
Size: 2521 Color: 1
Size: 864 Color: 0

Bin 149: 1 of cap free
Amount of items: 3
Items: 
Size: 15772 Color: 1
Size: 2851 Color: 1
Size: 384 Color: 0

Bin 150: 1 of cap free
Amount of items: 3
Items: 
Size: 16161 Color: 1
Size: 2494 Color: 1
Size: 352 Color: 0

Bin 151: 1 of cap free
Amount of items: 3
Items: 
Size: 16239 Color: 1
Size: 2352 Color: 1
Size: 416 Color: 0

Bin 152: 1 of cap free
Amount of items: 3
Items: 
Size: 16439 Color: 1
Size: 2264 Color: 1
Size: 304 Color: 0

Bin 153: 1 of cap free
Amount of items: 3
Items: 
Size: 16486 Color: 1
Size: 2141 Color: 1
Size: 380 Color: 0

Bin 154: 1 of cap free
Amount of items: 3
Items: 
Size: 16518 Color: 1
Size: 2277 Color: 1
Size: 212 Color: 0

Bin 155: 1 of cap free
Amount of items: 3
Items: 
Size: 13012 Color: 1
Size: 5295 Color: 1
Size: 700 Color: 0

Bin 156: 1 of cap free
Amount of items: 3
Items: 
Size: 15055 Color: 1
Size: 2996 Color: 1
Size: 956 Color: 0

Bin 157: 2 of cap free
Amount of items: 3
Items: 
Size: 12310 Color: 1
Size: 5864 Color: 1
Size: 832 Color: 0

Bin 158: 2 of cap free
Amount of items: 3
Items: 
Size: 12326 Color: 1
Size: 5848 Color: 1
Size: 832 Color: 0

Bin 159: 2 of cap free
Amount of items: 3
Items: 
Size: 13618 Color: 1
Size: 5004 Color: 1
Size: 384 Color: 0

Bin 160: 2 of cap free
Amount of items: 3
Items: 
Size: 14138 Color: 1
Size: 4264 Color: 1
Size: 604 Color: 0

Bin 161: 2 of cap free
Amount of items: 3
Items: 
Size: 14186 Color: 1
Size: 4372 Color: 1
Size: 448 Color: 0

Bin 162: 2 of cap free
Amount of items: 3
Items: 
Size: 14216 Color: 1
Size: 4022 Color: 1
Size: 768 Color: 0

Bin 163: 2 of cap free
Amount of items: 3
Items: 
Size: 14420 Color: 1
Size: 4426 Color: 1
Size: 160 Color: 0

Bin 164: 2 of cap free
Amount of items: 3
Items: 
Size: 16906 Color: 1
Size: 1764 Color: 1
Size: 336 Color: 0

Bin 165: 2 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 1
Size: 7928 Color: 1
Size: 400 Color: 0

Bin 166: 2 of cap free
Amount of items: 5
Items: 
Size: 7631 Color: 1
Size: 6480 Color: 1
Size: 3295 Color: 1
Size: 800 Color: 0
Size: 800 Color: 0

Bin 167: 3 of cap free
Amount of items: 3
Items: 
Size: 11462 Color: 1
Size: 6671 Color: 1
Size: 872 Color: 0

Bin 168: 3 of cap free
Amount of items: 3
Items: 
Size: 12655 Color: 1
Size: 5582 Color: 1
Size: 768 Color: 0

Bin 169: 3 of cap free
Amount of items: 3
Items: 
Size: 14467 Color: 1
Size: 3828 Color: 1
Size: 710 Color: 0

Bin 170: 3 of cap free
Amount of items: 5
Items: 
Size: 9517 Color: 1
Size: 5010 Color: 1
Size: 2022 Color: 1
Size: 1344 Color: 0
Size: 1112 Color: 0

Bin 171: 3 of cap free
Amount of items: 3
Items: 
Size: 10811 Color: 1
Size: 6934 Color: 1
Size: 1260 Color: 0

Bin 172: 4 of cap free
Amount of items: 3
Items: 
Size: 14269 Color: 1
Size: 3743 Color: 1
Size: 992 Color: 0

Bin 173: 4 of cap free
Amount of items: 3
Items: 
Size: 12282 Color: 1
Size: 5570 Color: 1
Size: 1152 Color: 0

Bin 174: 4 of cap free
Amount of items: 3
Items: 
Size: 13602 Color: 1
Size: 4970 Color: 1
Size: 432 Color: 0

Bin 175: 4 of cap free
Amount of items: 3
Items: 
Size: 11446 Color: 1
Size: 7154 Color: 1
Size: 404 Color: 0

Bin 176: 4 of cap free
Amount of items: 3
Items: 
Size: 15884 Color: 1
Size: 2128 Color: 1
Size: 992 Color: 0

Bin 177: 5 of cap free
Amount of items: 3
Items: 
Size: 11007 Color: 1
Size: 6764 Color: 1
Size: 1232 Color: 0

Bin 178: 5 of cap free
Amount of items: 3
Items: 
Size: 12940 Color: 1
Size: 5743 Color: 1
Size: 320 Color: 0

Bin 179: 5 of cap free
Amount of items: 3
Items: 
Size: 14806 Color: 1
Size: 3557 Color: 1
Size: 640 Color: 0

Bin 180: 6 of cap free
Amount of items: 3
Items: 
Size: 16776 Color: 1
Size: 1366 Color: 0
Size: 860 Color: 0

Bin 181: 6 of cap free
Amount of items: 3
Items: 
Size: 11996 Color: 1
Size: 6318 Color: 1
Size: 688 Color: 0

Bin 182: 6 of cap free
Amount of items: 3
Items: 
Size: 13046 Color: 1
Size: 5060 Color: 1
Size: 896 Color: 0

Bin 183: 6 of cap free
Amount of items: 5
Items: 
Size: 10690 Color: 1
Size: 6496 Color: 1
Size: 1120 Color: 1
Size: 376 Color: 0
Size: 320 Color: 0

Bin 184: 7 of cap free
Amount of items: 3
Items: 
Size: 13764 Color: 1
Size: 4789 Color: 1
Size: 448 Color: 0

Bin 185: 13 of cap free
Amount of items: 3
Items: 
Size: 9318 Color: 1
Size: 8397 Color: 1
Size: 1280 Color: 0

Bin 186: 13 of cap free
Amount of items: 3
Items: 
Size: 14805 Color: 1
Size: 3502 Color: 1
Size: 688 Color: 0

Bin 187: 18 of cap free
Amount of items: 3
Items: 
Size: 13040 Color: 1
Size: 4802 Color: 1
Size: 1148 Color: 0

Bin 188: 21 of cap free
Amount of items: 3
Items: 
Size: 10900 Color: 1
Size: 6831 Color: 1
Size: 1256 Color: 0

Bin 189: 21 of cap free
Amount of items: 3
Items: 
Size: 10840 Color: 1
Size: 7027 Color: 1
Size: 1120 Color: 0

Bin 190: 38 of cap free
Amount of items: 3
Items: 
Size: 16277 Color: 1
Size: 2373 Color: 1
Size: 320 Color: 0

Bin 191: 62 of cap free
Amount of items: 3
Items: 
Size: 16084 Color: 1
Size: 2078 Color: 1
Size: 784 Color: 0

Bin 192: 107 of cap free
Amount of items: 3
Items: 
Size: 11976 Color: 1
Size: 6165 Color: 1
Size: 760 Color: 0

Bin 193: 139 of cap free
Amount of items: 3
Items: 
Size: 11003 Color: 1
Size: 6808 Color: 1
Size: 1058 Color: 0

Bin 194: 304 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 1
Size: 4568 Color: 1
Size: 576 Color: 0

Bin 195: 399 of cap free
Amount of items: 3
Items: 
Size: 15589 Color: 1
Size: 2444 Color: 1
Size: 576 Color: 0

Bin 196: 2312 of cap free
Amount of items: 1
Items: 
Size: 16696 Color: 1

Bin 197: 2426 of cap free
Amount of items: 1
Items: 
Size: 16582 Color: 1

Bin 198: 3520 of cap free
Amount of items: 3
Items: 
Size: 9514 Color: 1
Size: 5606 Color: 1
Size: 368 Color: 0

Bin 199: 9496 of cap free
Amount of items: 1
Items: 
Size: 9512 Color: 1

Total size: 3763584
Total free space: 19008

