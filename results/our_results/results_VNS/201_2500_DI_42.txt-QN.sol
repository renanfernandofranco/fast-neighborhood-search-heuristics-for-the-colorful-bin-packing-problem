Capicity Bin: 1864
Lower Bound: 65

Bins used: 66
Amount of Colors: 202

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 941 Color: 141
Size: 425 Color: 110
Size: 382 Color: 106
Size: 60 Color: 31
Size: 56 Color: 27

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 161
Size: 482 Color: 115
Size: 52 Color: 25

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1358 Color: 165
Size: 422 Color: 109
Size: 84 Color: 45

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 166
Size: 398 Color: 108
Size: 76 Color: 43

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 167
Size: 250 Color: 88
Size: 208 Color: 82

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1490 Color: 175
Size: 330 Color: 99
Size: 44 Color: 18

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1538 Color: 181
Size: 314 Color: 98
Size: 12 Color: 2

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1565 Color: 183
Size: 191 Color: 74
Size: 108 Color: 54

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1579 Color: 186
Size: 181 Color: 71
Size: 104 Color: 53

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1594 Color: 187
Size: 202 Color: 79
Size: 68 Color: 37

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1619 Color: 189
Size: 157 Color: 69
Size: 88 Color: 47

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1626 Color: 191
Size: 194 Color: 76
Size: 44 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1633 Color: 192
Size: 193 Color: 75
Size: 38 Color: 15

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1634 Color: 193
Size: 144 Color: 63
Size: 86 Color: 46

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1636 Color: 194
Size: 182 Color: 72
Size: 46 Color: 20

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 197
Size: 163 Color: 70
Size: 52 Color: 26

Bin 17: 1 of cap free
Amount of items: 3
Items: 
Size: 1135 Color: 151
Size: 694 Color: 128
Size: 34 Color: 10

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 154
Size: 631 Color: 125
Size: 30 Color: 6

Bin 19: 1 of cap free
Amount of items: 2
Items: 
Size: 1231 Color: 156
Size: 632 Color: 126

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1435 Color: 171
Size: 428 Color: 111

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1443 Color: 172
Size: 288 Color: 92
Size: 132 Color: 60

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 173
Size: 380 Color: 105
Size: 36 Color: 11

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1506 Color: 178
Size: 349 Color: 100
Size: 8 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1519 Color: 180
Size: 274 Color: 91
Size: 70 Color: 39

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1637 Color: 195
Size: 226 Color: 85

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1642 Color: 196
Size: 221 Color: 84

Bin 27: 1 of cap free
Amount of items: 2
Items: 
Size: 1650 Color: 198
Size: 213 Color: 83

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 199
Size: 205 Color: 81

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1669 Color: 200
Size: 194 Color: 77

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1677 Color: 201
Size: 186 Color: 73

Bin 31: 2 of cap free
Amount of items: 5
Items: 
Size: 981 Color: 143
Size: 737 Color: 130
Size: 48 Color: 24
Size: 48 Color: 23
Size: 48 Color: 22

Bin 32: 2 of cap free
Amount of items: 2
Items: 
Size: 1109 Color: 148
Size: 753 Color: 133

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1131 Color: 150
Size: 731 Color: 129

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1333 Color: 162
Size: 529 Color: 118

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1355 Color: 164
Size: 507 Color: 116

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1416 Color: 169
Size: 446 Color: 114

Bin 37: 2 of cap free
Amount of items: 2
Items: 
Size: 1511 Color: 179
Size: 351 Color: 101

Bin 38: 2 of cap free
Amount of items: 2
Items: 
Size: 1551 Color: 182
Size: 311 Color: 97

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1123 Color: 149
Size: 738 Color: 131

Bin 40: 3 of cap free
Amount of items: 3
Items: 
Size: 1422 Color: 170
Size: 431 Color: 112
Size: 8 Color: 0

Bin 41: 3 of cap free
Amount of items: 2
Items: 
Size: 1491 Color: 176
Size: 370 Color: 103

Bin 42: 3 of cap free
Amount of items: 2
Items: 
Size: 1566 Color: 184
Size: 295 Color: 94

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1249 Color: 157
Size: 611 Color: 123

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1347 Color: 163
Size: 513 Color: 117

Bin 45: 4 of cap free
Amount of items: 2
Items: 
Size: 1599 Color: 188
Size: 261 Color: 90

Bin 46: 4 of cap free
Amount of items: 2
Items: 
Size: 1621 Color: 190
Size: 239 Color: 86

Bin 47: 5 of cap free
Amount of items: 2
Items: 
Size: 1257 Color: 158
Size: 602 Color: 121

Bin 48: 5 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 185
Size: 289 Color: 93

Bin 49: 6 of cap free
Amount of items: 3
Items: 
Size: 1292 Color: 160
Size: 554 Color: 120
Size: 12 Color: 3

Bin 50: 6 of cap free
Amount of items: 2
Items: 
Size: 1415 Color: 168
Size: 443 Color: 113

Bin 51: 6 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 174
Size: 388 Color: 107

Bin 52: 7 of cap free
Amount of items: 3
Items: 
Size: 1218 Color: 155
Size: 609 Color: 122
Size: 30 Color: 5

Bin 53: 7 of cap free
Amount of items: 2
Items: 
Size: 1498 Color: 177
Size: 359 Color: 102

Bin 54: 10 of cap free
Amount of items: 3
Items: 
Size: 1066 Color: 147
Size: 752 Color: 132
Size: 36 Color: 12

Bin 55: 13 of cap free
Amount of items: 6
Items: 
Size: 934 Color: 139
Size: 302 Color: 95
Size: 251 Color: 89
Size: 246 Color: 87
Size: 60 Color: 30
Size: 58 Color: 29

Bin 56: 13 of cap free
Amount of items: 4
Items: 
Size: 1168 Color: 153
Size: 619 Color: 124
Size: 32 Color: 8
Size: 32 Color: 7

Bin 57: 16 of cap free
Amount of items: 3
Items: 
Size: 1034 Color: 146
Size: 778 Color: 136
Size: 36 Color: 13

Bin 58: 16 of cap free
Amount of items: 3
Items: 
Size: 1286 Color: 159
Size: 542 Color: 119
Size: 20 Color: 4

Bin 59: 17 of cap free
Amount of items: 4
Items: 
Size: 982 Color: 144
Size: 773 Color: 134
Size: 48 Color: 21
Size: 44 Color: 17

Bin 60: 20 of cap free
Amount of items: 3
Items: 
Size: 1146 Color: 152
Size: 666 Color: 127
Size: 32 Color: 9

Bin 61: 22 of cap free
Amount of items: 4
Items: 
Size: 989 Color: 145
Size: 777 Color: 135
Size: 40 Color: 16
Size: 36 Color: 14

Bin 62: 24 of cap free
Amount of items: 2
Items: 
Size: 961 Color: 142
Size: 879 Color: 137

Bin 63: 31 of cap free
Amount of items: 8
Items: 
Size: 933 Color: 138
Size: 204 Color: 80
Size: 196 Color: 78
Size: 154 Color: 67
Size: 152 Color: 66
Size: 68 Color: 34
Size: 64 Color: 33
Size: 62 Color: 32

Bin 64: 36 of cap free
Amount of items: 5
Items: 
Size: 937 Color: 140
Size: 375 Color: 104
Size: 306 Color: 96
Size: 154 Color: 68
Size: 56 Color: 28

Bin 65: 68 of cap free
Amount of items: 17
Items: 
Size: 150 Color: 65
Size: 146 Color: 64
Size: 144 Color: 62
Size: 136 Color: 61
Size: 124 Color: 59
Size: 122 Color: 58
Size: 122 Color: 57
Size: 120 Color: 56
Size: 116 Color: 55
Size: 104 Color: 52
Size: 84 Color: 44
Size: 76 Color: 42
Size: 74 Color: 41
Size: 72 Color: 40
Size: 70 Color: 38
Size: 68 Color: 36
Size: 68 Color: 35

Bin 66: 1478 of cap free
Amount of items: 4
Items: 
Size: 102 Color: 51
Size: 100 Color: 50
Size: 96 Color: 49
Size: 88 Color: 48

Total size: 121160
Total free space: 1864

