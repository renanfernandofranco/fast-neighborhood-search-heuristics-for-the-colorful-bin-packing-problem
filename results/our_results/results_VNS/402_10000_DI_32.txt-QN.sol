Capicity Bin: 8064
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5156 Color: 303
Size: 2756 Color: 250
Size: 152 Color: 16

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5336 Color: 305
Size: 2584 Color: 244
Size: 144 Color: 14

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6004 Color: 329
Size: 1908 Color: 220
Size: 152 Color: 17

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6102 Color: 332
Size: 1826 Color: 217
Size: 136 Color: 9

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6181 Color: 335
Size: 1681 Color: 209
Size: 202 Color: 44

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6290 Color: 339
Size: 1482 Color: 200
Size: 292 Color: 78

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6344 Color: 343
Size: 1270 Color: 187
Size: 450 Color: 106

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6535 Color: 352
Size: 985 Color: 162
Size: 544 Color: 118

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6552 Color: 353
Size: 1272 Color: 188
Size: 240 Color: 62

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6641 Color: 358
Size: 1141 Color: 178
Size: 282 Color: 75

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6648 Color: 359
Size: 896 Color: 151
Size: 520 Color: 115

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 361
Size: 1075 Color: 172
Size: 292 Color: 79

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 362
Size: 1140 Color: 177
Size: 224 Color: 54

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 365
Size: 1055 Color: 170
Size: 248 Color: 63

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6772 Color: 366
Size: 1020 Color: 168
Size: 272 Color: 72

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6774 Color: 367
Size: 778 Color: 142
Size: 512 Color: 113

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6798 Color: 369
Size: 1078 Color: 173
Size: 188 Color: 34

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6799 Color: 370
Size: 1249 Color: 186
Size: 16 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6824 Color: 371
Size: 664 Color: 128
Size: 576 Color: 119

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6858 Color: 374
Size: 978 Color: 161
Size: 228 Color: 57

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6894 Color: 377
Size: 880 Color: 150
Size: 290 Color: 77

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6904 Color: 378
Size: 792 Color: 143
Size: 368 Color: 95

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6958 Color: 382
Size: 1058 Color: 171
Size: 48 Color: 4

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6964 Color: 383
Size: 846 Color: 148
Size: 254 Color: 65

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6968 Color: 384
Size: 776 Color: 141
Size: 320 Color: 84

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6995 Color: 386
Size: 993 Color: 163
Size: 76 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7024 Color: 388
Size: 844 Color: 147
Size: 196 Color: 41

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7028 Color: 389
Size: 812 Color: 145
Size: 224 Color: 52

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7090 Color: 392
Size: 764 Color: 140
Size: 210 Color: 49

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7128 Color: 395
Size: 668 Color: 132
Size: 268 Color: 71

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7144 Color: 397
Size: 760 Color: 139
Size: 160 Color: 21

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7160 Color: 399
Size: 636 Color: 124
Size: 268 Color: 70

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7210 Color: 400
Size: 694 Color: 137
Size: 160 Color: 23

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7234 Color: 401
Size: 670 Color: 135
Size: 160 Color: 19

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7242 Color: 402
Size: 640 Color: 125
Size: 182 Color: 32

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 287
Size: 3354 Color: 264
Size: 176 Color: 29

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4536 Color: 288
Size: 3351 Color: 262
Size: 176 Color: 28

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 320
Size: 2243 Color: 233
Size: 64 Color: 5

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 5866 Color: 323
Size: 1395 Color: 195
Size: 802 Color: 144

Bin 40: 1 of cap free
Amount of items: 2
Items: 
Size: 5915 Color: 326
Size: 2148 Color: 228

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6447 Color: 348
Size: 952 Color: 157
Size: 664 Color: 127

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6492 Color: 351
Size: 1571 Color: 203

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6567 Color: 354
Size: 1496 Color: 201

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6612 Color: 357
Size: 1451 Color: 197

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 6714 Color: 363
Size: 1349 Color: 193

Bin 46: 2 of cap free
Amount of items: 5
Items: 
Size: 4042 Color: 278
Size: 3324 Color: 259
Size: 280 Color: 74
Size: 208 Color: 48
Size: 208 Color: 47

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 6184 Color: 336
Size: 1638 Color: 207
Size: 240 Color: 60

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 6314 Color: 341
Size: 1724 Color: 212
Size: 24 Color: 2

Bin 49: 2 of cap free
Amount of items: 2
Items: 
Size: 6844 Color: 372
Size: 1218 Color: 184

Bin 50: 2 of cap free
Amount of items: 2
Items: 
Size: 6870 Color: 375
Size: 1192 Color: 182

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6908 Color: 379
Size: 1154 Color: 179

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 387
Size: 1048 Color: 169

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 7060 Color: 391
Size: 1002 Color: 165

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 394
Size: 960 Color: 158

Bin 55: 3 of cap free
Amount of items: 3
Items: 
Size: 5568 Color: 313
Size: 2081 Color: 226
Size: 412 Color: 100

Bin 56: 3 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 321
Size: 2245 Color: 234
Size: 48 Color: 3

Bin 57: 4 of cap free
Amount of items: 3
Items: 
Size: 5371 Color: 308
Size: 2545 Color: 241
Size: 144 Color: 12

Bin 58: 4 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 322
Size: 2280 Color: 237

Bin 59: 4 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 337
Size: 1872 Color: 219

Bin 60: 4 of cap free
Amount of items: 2
Items: 
Size: 6586 Color: 355
Size: 1474 Color: 199

Bin 61: 4 of cap free
Amount of items: 2
Items: 
Size: 6744 Color: 364
Size: 1316 Color: 190

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 7092 Color: 393
Size: 968 Color: 160

Bin 63: 5 of cap free
Amount of items: 2
Items: 
Size: 5339 Color: 306
Size: 2720 Color: 248

Bin 64: 5 of cap free
Amount of items: 3
Items: 
Size: 5550 Color: 312
Size: 1322 Color: 191
Size: 1187 Color: 181

Bin 65: 5 of cap free
Amount of items: 2
Items: 
Size: 6323 Color: 342
Size: 1736 Color: 213

Bin 66: 5 of cap free
Amount of items: 2
Items: 
Size: 6847 Color: 373
Size: 1212 Color: 183

Bin 67: 5 of cap free
Amount of items: 2
Items: 
Size: 7148 Color: 398
Size: 911 Color: 153

Bin 68: 6 of cap free
Amount of items: 3
Items: 
Size: 5363 Color: 307
Size: 2551 Color: 242
Size: 144 Color: 13

Bin 69: 6 of cap free
Amount of items: 2
Items: 
Size: 6440 Color: 347
Size: 1618 Color: 206

Bin 70: 6 of cap free
Amount of items: 3
Items: 
Size: 6606 Color: 356
Size: 1436 Color: 196
Size: 16 Color: 0

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 7134 Color: 396
Size: 924 Color: 155

Bin 72: 7 of cap free
Amount of items: 5
Items: 
Size: 4041 Color: 277
Size: 1806 Color: 216
Size: 1342 Color: 192
Size: 656 Color: 126
Size: 212 Color: 50

Bin 73: 7 of cap free
Amount of items: 5
Items: 
Size: 4052 Color: 281
Size: 3353 Color: 263
Size: 264 Color: 69
Size: 196 Color: 40
Size: 192 Color: 39

Bin 74: 7 of cap free
Amount of items: 5
Items: 
Size: 4068 Color: 282
Size: 3357 Color: 265
Size: 248 Color: 64
Size: 192 Color: 38
Size: 192 Color: 37

Bin 75: 7 of cap free
Amount of items: 2
Items: 
Size: 6298 Color: 340
Size: 1759 Color: 214

Bin 76: 7 of cap free
Amount of items: 2
Items: 
Size: 6931 Color: 380
Size: 1126 Color: 176

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 4854 Color: 293
Size: 3202 Color: 257

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6944 Color: 381
Size: 1112 Color: 175

Bin 79: 8 of cap free
Amount of items: 2
Items: 
Size: 7050 Color: 390
Size: 1006 Color: 166

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 5484 Color: 310
Size: 2571 Color: 243

Bin 81: 9 of cap free
Amount of items: 2
Items: 
Size: 6391 Color: 346
Size: 1664 Color: 208

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 6971 Color: 385
Size: 1084 Color: 174

Bin 83: 10 of cap free
Amount of items: 5
Items: 
Size: 4050 Color: 280
Size: 3348 Color: 261
Size: 256 Color: 68
Size: 200 Color: 43
Size: 200 Color: 42

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 5898 Color: 325
Size: 2156 Color: 231

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 5992 Color: 328
Size: 2062 Color: 225

Bin 86: 10 of cap free
Amount of items: 2
Items: 
Size: 6126 Color: 334
Size: 1928 Color: 222

Bin 87: 10 of cap free
Amount of items: 2
Items: 
Size: 6478 Color: 350
Size: 1576 Color: 204

Bin 88: 10 of cap free
Amount of items: 2
Items: 
Size: 6883 Color: 376
Size: 1171 Color: 180

Bin 89: 11 of cap free
Amount of items: 11
Items: 
Size: 4033 Color: 272
Size: 580 Color: 120
Size: 532 Color: 117
Size: 524 Color: 116
Size: 514 Color: 114
Size: 384 Color: 97
Size: 312 Color: 82
Size: 304 Color: 81
Size: 304 Color: 80
Size: 288 Color: 76
Size: 278 Color: 73

Bin 90: 11 of cap free
Amount of items: 3
Items: 
Size: 4941 Color: 295
Size: 2952 Color: 253
Size: 160 Color: 25

Bin 91: 11 of cap free
Amount of items: 3
Items: 
Size: 5646 Color: 317
Size: 2271 Color: 236
Size: 136 Color: 10

Bin 92: 11 of cap free
Amount of items: 2
Items: 
Size: 5935 Color: 327
Size: 2118 Color: 227

Bin 93: 12 of cap free
Amount of items: 8
Items: 
Size: 4036 Color: 274
Size: 814 Color: 146
Size: 714 Color: 138
Size: 686 Color: 136
Size: 670 Color: 134
Size: 670 Color: 133
Size: 236 Color: 58
Size: 226 Color: 56

Bin 94: 12 of cap free
Amount of items: 2
Items: 
Size: 6125 Color: 333
Size: 1927 Color: 221

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 6348 Color: 345
Size: 1703 Color: 211

Bin 96: 14 of cap free
Amount of items: 3
Items: 
Size: 5011 Color: 299
Size: 1791 Color: 215
Size: 1248 Color: 185

Bin 97: 14 of cap free
Amount of items: 2
Items: 
Size: 6682 Color: 360
Size: 1368 Color: 194

Bin 98: 14 of cap free
Amount of items: 2
Items: 
Size: 6775 Color: 368
Size: 1275 Color: 189

Bin 99: 15 of cap free
Amount of items: 5
Items: 
Size: 4045 Color: 279
Size: 3332 Color: 260
Size: 256 Color: 67
Size: 208 Color: 46
Size: 208 Color: 45

Bin 100: 15 of cap free
Amount of items: 2
Items: 
Size: 6454 Color: 349
Size: 1595 Color: 205

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 5418 Color: 309
Size: 2630 Color: 246

Bin 102: 16 of cap free
Amount of items: 2
Items: 
Size: 6047 Color: 331
Size: 2001 Color: 223

Bin 103: 16 of cap free
Amount of items: 2
Items: 
Size: 6347 Color: 344
Size: 1701 Color: 210

Bin 104: 17 of cap free
Amount of items: 7
Items: 
Size: 4040 Color: 276
Size: 1015 Color: 167
Size: 998 Color: 164
Size: 964 Color: 159
Size: 592 Color: 122
Size: 224 Color: 53
Size: 214 Color: 51

Bin 105: 18 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 286
Size: 3364 Color: 268
Size: 176 Color: 30

Bin 106: 18 of cap free
Amount of items: 3
Items: 
Size: 5663 Color: 318
Size: 2251 Color: 235
Size: 132 Color: 8

Bin 107: 20 of cap free
Amount of items: 3
Items: 
Size: 4979 Color: 297
Size: 2905 Color: 251
Size: 160 Color: 24

Bin 108: 20 of cap free
Amount of items: 2
Items: 
Size: 5526 Color: 311
Size: 2518 Color: 240

Bin 109: 22 of cap free
Amount of items: 2
Items: 
Size: 4968 Color: 296
Size: 3074 Color: 255

Bin 110: 23 of cap free
Amount of items: 2
Items: 
Size: 5639 Color: 315
Size: 2402 Color: 238

Bin 111: 23 of cap free
Amount of items: 2
Items: 
Size: 6023 Color: 330
Size: 2018 Color: 224

Bin 112: 24 of cap free
Amount of items: 2
Items: 
Size: 4612 Color: 290
Size: 3428 Color: 270

Bin 113: 24 of cap free
Amount of items: 3
Items: 
Size: 4724 Color: 291
Size: 3148 Color: 256
Size: 168 Color: 27

Bin 114: 27 of cap free
Amount of items: 2
Items: 
Size: 6203 Color: 338
Size: 1834 Color: 218

Bin 115: 35 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 298
Size: 1564 Color: 202
Size: 1462 Color: 198

Bin 116: 38 of cap free
Amount of items: 2
Items: 
Size: 5874 Color: 324
Size: 2152 Color: 230

Bin 117: 39 of cap free
Amount of items: 3
Items: 
Size: 5753 Color: 319
Size: 2150 Color: 229
Size: 122 Color: 7

Bin 118: 43 of cap free
Amount of items: 3
Items: 
Size: 4910 Color: 294
Size: 2943 Color: 252
Size: 168 Color: 26

Bin 119: 46 of cap free
Amount of items: 2
Items: 
Size: 5590 Color: 314
Size: 2428 Color: 239

Bin 120: 52 of cap free
Amount of items: 2
Items: 
Size: 4764 Color: 292
Size: 3248 Color: 258

Bin 121: 52 of cap free
Amount of items: 2
Items: 
Size: 5046 Color: 300
Size: 2966 Color: 254

Bin 122: 60 of cap free
Amount of items: 3
Items: 
Size: 5182 Color: 304
Size: 2678 Color: 247
Size: 144 Color: 15

Bin 123: 71 of cap free
Amount of items: 4
Items: 
Size: 5070 Color: 301
Size: 2603 Color: 245
Size: 160 Color: 22
Size: 160 Color: 20

Bin 124: 78 of cap free
Amount of items: 3
Items: 
Size: 5640 Color: 316
Size: 2206 Color: 232
Size: 140 Color: 11

Bin 125: 86 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 302
Size: 2746 Color: 249
Size: 160 Color: 18

Bin 126: 98 of cap free
Amount of items: 9
Items: 
Size: 4034 Color: 273
Size: 668 Color: 131
Size: 668 Color: 130
Size: 664 Color: 129
Size: 612 Color: 123
Size: 588 Color: 121
Size: 256 Color: 66
Size: 240 Color: 61
Size: 236 Color: 59

Bin 127: 101 of cap free
Amount of items: 4
Items: 
Size: 4226 Color: 284
Size: 3361 Color: 266
Size: 192 Color: 35
Size: 184 Color: 33

Bin 128: 101 of cap free
Amount of items: 2
Items: 
Size: 4579 Color: 289
Size: 3384 Color: 269

Bin 129: 122 of cap free
Amount of items: 3
Items: 
Size: 4084 Color: 283
Size: 3666 Color: 271
Size: 192 Color: 36

Bin 130: 142 of cap free
Amount of items: 3
Items: 
Size: 4378 Color: 285
Size: 3362 Color: 267
Size: 182 Color: 31

Bin 131: 172 of cap free
Amount of items: 6
Items: 
Size: 4037 Color: 275
Size: 945 Color: 156
Size: 922 Color: 154
Size: 896 Color: 152
Size: 868 Color: 149
Size: 224 Color: 55

Bin 132: 298 of cap free
Amount of items: 19
Items: 
Size: 510 Color: 112
Size: 508 Color: 111
Size: 500 Color: 110
Size: 480 Color: 109
Size: 480 Color: 108
Size: 454 Color: 107
Size: 448 Color: 105
Size: 448 Color: 104
Size: 440 Color: 103
Size: 424 Color: 102
Size: 364 Color: 92
Size: 360 Color: 91
Size: 358 Color: 90
Size: 348 Color: 89
Size: 336 Color: 88
Size: 336 Color: 87
Size: 336 Color: 86
Size: 324 Color: 85
Size: 312 Color: 83

Bin 133: 5740 of cap free
Amount of items: 6
Items: 
Size: 420 Color: 101
Size: 400 Color: 99
Size: 400 Color: 98
Size: 376 Color: 96
Size: 364 Color: 94
Size: 364 Color: 93

Total size: 1064448
Total free space: 8064

