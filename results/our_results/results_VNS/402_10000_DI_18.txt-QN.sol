Capicity Bin: 8136
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4254 Color: 279
Size: 3042 Color: 257
Size: 408 Color: 100
Size: 216 Color: 53
Size: 216 Color: 52

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 297
Size: 2900 Color: 253
Size: 164 Color: 28

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 6108 Color: 321
Size: 1020 Color: 173
Size: 1008 Color: 171

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 6142 Color: 324
Size: 1702 Color: 220
Size: 292 Color: 79

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 333
Size: 1145 Color: 184
Size: 612 Color: 126

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 334
Size: 1468 Color: 208
Size: 288 Color: 78

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6509 Color: 339
Size: 1357 Color: 202
Size: 270 Color: 70

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6516 Color: 340
Size: 1356 Color: 201
Size: 264 Color: 66

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6550 Color: 343
Size: 1182 Color: 185
Size: 404 Color: 98

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6578 Color: 344
Size: 830 Color: 153
Size: 728 Color: 140

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6629 Color: 346
Size: 851 Color: 155
Size: 656 Color: 131

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6702 Color: 351
Size: 1212 Color: 189
Size: 222 Color: 55

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6716 Color: 352
Size: 1300 Color: 196
Size: 120 Color: 10

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6756 Color: 354
Size: 862 Color: 156
Size: 518 Color: 115

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6853 Color: 361
Size: 1071 Color: 178
Size: 212 Color: 51

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6926 Color: 366
Size: 874 Color: 158
Size: 336 Color: 89

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6930 Color: 368
Size: 1006 Color: 170
Size: 200 Color: 46

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7003 Color: 370
Size: 945 Color: 167
Size: 188 Color: 42

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7013 Color: 371
Size: 759 Color: 142
Size: 364 Color: 93

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7043 Color: 376
Size: 885 Color: 160
Size: 208 Color: 50

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7092 Color: 379
Size: 524 Color: 118
Size: 520 Color: 116

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 7101 Color: 380
Size: 863 Color: 157
Size: 172 Color: 32

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7142 Color: 385
Size: 844 Color: 154
Size: 150 Color: 19

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7167 Color: 386
Size: 809 Color: 152
Size: 160 Color: 27

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7174 Color: 387
Size: 698 Color: 137
Size: 264 Color: 67

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7206 Color: 390
Size: 778 Color: 146
Size: 152 Color: 23

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7209 Color: 391
Size: 791 Color: 151
Size: 136 Color: 12

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7232 Color: 395
Size: 676 Color: 133
Size: 228 Color: 56

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7235 Color: 396
Size: 751 Color: 141
Size: 150 Color: 20

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7236 Color: 397
Size: 644 Color: 130
Size: 256 Color: 65

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7264 Color: 398
Size: 608 Color: 124
Size: 264 Color: 68

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7276 Color: 399
Size: 612 Color: 127
Size: 248 Color: 64

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7294 Color: 400
Size: 520 Color: 117
Size: 322 Color: 84

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7300 Color: 401
Size: 516 Color: 114
Size: 320 Color: 83

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7302 Color: 402
Size: 676 Color: 135
Size: 158 Color: 25

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 5388 Color: 301
Size: 2595 Color: 246
Size: 152 Color: 22

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 6134 Color: 323
Size: 1241 Color: 192
Size: 760 Color: 143

Bin 38: 1 of cap free
Amount of items: 3
Items: 
Size: 6207 Color: 328
Size: 1520 Color: 211
Size: 408 Color: 101

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 6371 Color: 332
Size: 1660 Color: 217
Size: 104 Color: 9

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6402 Color: 335
Size: 1465 Color: 207
Size: 268 Color: 69

Bin 41: 1 of cap free
Amount of items: 2
Items: 
Size: 6520 Color: 342
Size: 1615 Color: 214

Bin 42: 1 of cap free
Amount of items: 2
Items: 
Size: 6689 Color: 350
Size: 1446 Color: 206

Bin 43: 1 of cap free
Amount of items: 2
Items: 
Size: 6916 Color: 365
Size: 1219 Color: 191

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 7018 Color: 373
Size: 1117 Color: 182

Bin 45: 1 of cap free
Amount of items: 2
Items: 
Size: 7227 Color: 394
Size: 908 Color: 161

Bin 46: 2 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 302
Size: 2589 Color: 245
Size: 152 Color: 21

Bin 47: 2 of cap free
Amount of items: 3
Items: 
Size: 5394 Color: 303
Size: 2596 Color: 247
Size: 144 Color: 18

Bin 48: 2 of cap free
Amount of items: 3
Items: 
Size: 5422 Color: 305
Size: 2568 Color: 244
Size: 144 Color: 17

Bin 49: 2 of cap free
Amount of items: 4
Items: 
Size: 5446 Color: 307
Size: 2412 Color: 243
Size: 140 Color: 16
Size: 136 Color: 15

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5668 Color: 308
Size: 1692 Color: 219
Size: 774 Color: 145

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 6148 Color: 325
Size: 1986 Color: 229

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 6370 Color: 331
Size: 1188 Color: 186
Size: 576 Color: 121

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 6412 Color: 336
Size: 1722 Color: 221

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6604 Color: 345
Size: 1530 Color: 212

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6927 Color: 367
Size: 1207 Color: 188

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 7014 Color: 372
Size: 1120 Color: 183

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 7132 Color: 384
Size: 1002 Color: 169

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 7196 Color: 389
Size: 938 Color: 166

Bin 59: 3 of cap free
Amount of items: 7
Items: 
Size: 4069 Color: 273
Size: 788 Color: 150
Size: 788 Color: 149
Size: 785 Color: 148
Size: 779 Color: 147
Size: 676 Color: 136
Size: 248 Color: 62

Bin 60: 3 of cap free
Amount of items: 2
Items: 
Size: 7035 Color: 375
Size: 1098 Color: 180

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 7106 Color: 382
Size: 1027 Color: 175

Bin 62: 4 of cap free
Amount of items: 2
Items: 
Size: 6110 Color: 322
Size: 2022 Color: 230

Bin 63: 4 of cap free
Amount of items: 2
Items: 
Size: 6781 Color: 356
Size: 1351 Color: 200

Bin 64: 4 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 362
Size: 1272 Color: 194

Bin 65: 4 of cap free
Amount of items: 2
Items: 
Size: 6935 Color: 369
Size: 1197 Color: 187

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 7221 Color: 393
Size: 911 Color: 162

Bin 67: 5 of cap free
Amount of items: 7
Items: 
Size: 4070 Color: 274
Size: 1001 Color: 168
Size: 932 Color: 164
Size: 876 Color: 159
Size: 772 Color: 144
Size: 240 Color: 61
Size: 240 Color: 60

Bin 68: 5 of cap free
Amount of items: 3
Items: 
Size: 4998 Color: 293
Size: 2957 Color: 255
Size: 176 Color: 34

Bin 69: 5 of cap free
Amount of items: 2
Items: 
Size: 6199 Color: 327
Size: 1932 Color: 226

Bin 70: 6 of cap free
Amount of items: 2
Items: 
Size: 6460 Color: 337
Size: 1670 Color: 218

Bin 71: 6 of cap free
Amount of items: 2
Items: 
Size: 7020 Color: 374
Size: 1110 Color: 181

Bin 72: 7 of cap free
Amount of items: 2
Items: 
Size: 7195 Color: 388
Size: 934 Color: 165

Bin 73: 7 of cap free
Amount of items: 2
Items: 
Size: 7210 Color: 392
Size: 919 Color: 163

Bin 74: 8 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 312
Size: 2260 Color: 236
Size: 88 Color: 8

Bin 75: 8 of cap free
Amount of items: 2
Items: 
Size: 6500 Color: 338
Size: 1628 Color: 215

Bin 76: 8 of cap free
Amount of items: 2
Items: 
Size: 6684 Color: 349
Size: 1444 Color: 205

Bin 77: 8 of cap free
Amount of items: 2
Items: 
Size: 6797 Color: 357
Size: 1331 Color: 199

Bin 78: 8 of cap free
Amount of items: 2
Items: 
Size: 6806 Color: 358
Size: 1322 Color: 198

Bin 79: 9 of cap free
Amount of items: 2
Items: 
Size: 6763 Color: 355
Size: 1364 Color: 203

Bin 80: 9 of cap free
Amount of items: 2
Items: 
Size: 6909 Color: 364
Size: 1218 Color: 190

Bin 81: 9 of cap free
Amount of items: 2
Items: 
Size: 7052 Color: 377
Size: 1075 Color: 179

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 7075 Color: 378
Size: 1052 Color: 176

Bin 83: 9 of cap free
Amount of items: 2
Items: 
Size: 7117 Color: 383
Size: 1010 Color: 172

Bin 84: 10 of cap free
Amount of items: 2
Items: 
Size: 6517 Color: 341
Size: 1609 Color: 213

Bin 85: 10 of cap free
Amount of items: 2
Items: 
Size: 6722 Color: 353
Size: 1404 Color: 204

Bin 86: 11 of cap free
Amount of items: 3
Items: 
Size: 5406 Color: 304
Size: 1651 Color: 216
Size: 1068 Color: 177

Bin 87: 11 of cap free
Amount of items: 2
Items: 
Size: 6302 Color: 330
Size: 1823 Color: 223

Bin 88: 11 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 381
Size: 1023 Color: 174

Bin 89: 12 of cap free
Amount of items: 3
Items: 
Size: 5385 Color: 300
Size: 2037 Color: 232
Size: 702 Color: 138

Bin 90: 12 of cap free
Amount of items: 3
Items: 
Size: 5782 Color: 313
Size: 2262 Color: 237
Size: 80 Color: 7

Bin 91: 12 of cap free
Amount of items: 2
Items: 
Size: 6822 Color: 359
Size: 1302 Color: 197

Bin 92: 13 of cap free
Amount of items: 4
Items: 
Size: 4071 Color: 275
Size: 2031 Color: 231
Size: 1781 Color: 222
Size: 240 Color: 59

Bin 93: 15 of cap free
Amount of items: 2
Items: 
Size: 6157 Color: 326
Size: 1964 Color: 228

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 6647 Color: 348
Size: 1474 Color: 210

Bin 95: 15 of cap free
Amount of items: 2
Items: 
Size: 6837 Color: 360
Size: 1284 Color: 195

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 4916 Color: 290
Size: 3204 Color: 261

Bin 97: 16 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 314
Size: 2242 Color: 235
Size: 80 Color: 6

Bin 98: 16 of cap free
Amount of items: 2
Items: 
Size: 6876 Color: 363
Size: 1244 Color: 193

Bin 99: 18 of cap free
Amount of items: 5
Items: 
Size: 4078 Color: 277
Size: 3040 Color: 256
Size: 536 Color: 120
Size: 232 Color: 58
Size: 232 Color: 57

Bin 100: 20 of cap free
Amount of items: 3
Items: 
Size: 5701 Color: 311
Size: 2287 Color: 240
Size: 128 Color: 11

Bin 101: 20 of cap free
Amount of items: 4
Items: 
Size: 6094 Color: 320
Size: 1950 Color: 227
Size: 40 Color: 1
Size: 32 Color: 0

Bin 102: 21 of cap free
Amount of items: 3
Items: 
Size: 5693 Color: 310
Size: 2286 Color: 239
Size: 136 Color: 13

Bin 103: 21 of cap free
Amount of items: 2
Items: 
Size: 5822 Color: 316
Size: 2293 Color: 242

Bin 104: 21 of cap free
Amount of items: 2
Items: 
Size: 6644 Color: 347
Size: 1471 Color: 209

Bin 105: 22 of cap free
Amount of items: 2
Items: 
Size: 6268 Color: 329
Size: 1846 Color: 224

Bin 106: 24 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 306
Size: 2684 Color: 252

Bin 107: 24 of cap free
Amount of items: 2
Items: 
Size: 5820 Color: 315
Size: 2292 Color: 241

Bin 108: 27 of cap free
Amount of items: 3
Items: 
Size: 4982 Color: 292
Size: 2951 Color: 254
Size: 176 Color: 35

Bin 109: 28 of cap free
Amount of items: 4
Items: 
Size: 6090 Color: 319
Size: 1930 Color: 225
Size: 48 Color: 3
Size: 40 Color: 2

Bin 110: 29 of cap free
Amount of items: 3
Items: 
Size: 5999 Color: 318
Size: 2060 Color: 234
Size: 48 Color: 4

Bin 111: 35 of cap free
Amount of items: 2
Items: 
Size: 5023 Color: 294
Size: 3078 Color: 260

Bin 112: 35 of cap free
Amount of items: 3
Items: 
Size: 5687 Color: 309
Size: 2278 Color: 238
Size: 136 Color: 14

Bin 113: 36 of cap free
Amount of items: 2
Items: 
Size: 4076 Color: 276
Size: 4024 Color: 272

Bin 114: 56 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 289
Size: 3238 Color: 263
Size: 182 Color: 36

Bin 115: 60 of cap free
Amount of items: 3
Items: 
Size: 4086 Color: 278
Size: 3770 Color: 271
Size: 220 Color: 54

Bin 116: 64 of cap free
Amount of items: 3
Items: 
Size: 4294 Color: 282
Size: 3578 Color: 270
Size: 200 Color: 45

Bin 117: 66 of cap free
Amount of items: 3
Items: 
Size: 5949 Color: 317
Size: 2041 Color: 233
Size: 80 Color: 5

Bin 118: 68 of cap free
Amount of items: 3
Items: 
Size: 4292 Color: 281
Size: 3576 Color: 269
Size: 200 Color: 47

Bin 119: 72 of cap free
Amount of items: 4
Items: 
Size: 4270 Color: 280
Size: 3382 Color: 264
Size: 208 Color: 49
Size: 204 Color: 48

Bin 120: 75 of cap free
Amount of items: 3
Items: 
Size: 4486 Color: 286
Size: 3391 Color: 268
Size: 184 Color: 40

Bin 121: 85 of cap free
Amount of items: 3
Items: 
Size: 4462 Color: 284
Size: 3389 Color: 266
Size: 200 Color: 43

Bin 122: 86 of cap free
Amount of items: 3
Items: 
Size: 4476 Color: 285
Size: 3390 Color: 267
Size: 184 Color: 41

Bin 123: 90 of cap free
Amount of items: 3
Items: 
Size: 5244 Color: 299
Size: 2646 Color: 251
Size: 156 Color: 24

Bin 124: 102 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 283
Size: 3388 Color: 265
Size: 200 Color: 44

Bin 125: 112 of cap free
Amount of items: 2
Items: 
Size: 4962 Color: 291
Size: 3062 Color: 259

Bin 126: 114 of cap free
Amount of items: 16
Items: 
Size: 724 Color: 139
Size: 676 Color: 134
Size: 672 Color: 132
Size: 644 Color: 129
Size: 640 Color: 128
Size: 608 Color: 125
Size: 590 Color: 123
Size: 588 Color: 122
Size: 528 Color: 119
Size: 512 Color: 113
Size: 496 Color: 112
Size: 280 Color: 74
Size: 272 Color: 73
Size: 272 Color: 72
Size: 272 Color: 71
Size: 248 Color: 63

Bin 127: 118 of cap free
Amount of items: 21
Items: 
Size: 480 Color: 111
Size: 458 Color: 110
Size: 456 Color: 109
Size: 456 Color: 108
Size: 456 Color: 107
Size: 452 Color: 106
Size: 452 Color: 105
Size: 448 Color: 104
Size: 448 Color: 103
Size: 448 Color: 102
Size: 406 Color: 99
Size: 332 Color: 88
Size: 332 Color: 87
Size: 328 Color: 86
Size: 328 Color: 85
Size: 304 Color: 82
Size: 294 Color: 81
Size: 292 Color: 80
Size: 288 Color: 77
Size: 280 Color: 76
Size: 280 Color: 75

Bin 128: 120 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 298
Size: 2636 Color: 250
Size: 160 Color: 26

Bin 129: 129 of cap free
Amount of items: 4
Items: 
Size: 4589 Color: 287
Size: 3052 Color: 258
Size: 184 Color: 39
Size: 182 Color: 38

Bin 130: 135 of cap free
Amount of items: 3
Items: 
Size: 4597 Color: 288
Size: 3222 Color: 262
Size: 182 Color: 37

Bin 131: 139 of cap free
Amount of items: 4
Items: 
Size: 5031 Color: 296
Size: 2630 Color: 249
Size: 168 Color: 30
Size: 168 Color: 29

Bin 132: 146 of cap free
Amount of items: 4
Items: 
Size: 5028 Color: 295
Size: 2618 Color: 248
Size: 176 Color: 33
Size: 168 Color: 31

Bin 133: 5532 of cap free
Amount of items: 7
Items: 
Size: 392 Color: 97
Size: 388 Color: 96
Size: 384 Color: 95
Size: 384 Color: 94
Size: 360 Color: 92
Size: 356 Color: 91
Size: 340 Color: 90

Total size: 1073952
Total free space: 8136

