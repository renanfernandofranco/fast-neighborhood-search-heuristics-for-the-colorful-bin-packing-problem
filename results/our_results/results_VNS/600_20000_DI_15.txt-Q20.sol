Capicity Bin: 16384
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 8202 Color: 9
Size: 5908 Color: 5
Size: 1016 Color: 3
Size: 834 Color: 12
Size: 424 Color: 7

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 10292 Color: 0
Size: 5852 Color: 1
Size: 240 Color: 13

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10696 Color: 12
Size: 5080 Color: 4
Size: 608 Color: 7

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11748 Color: 10
Size: 4290 Color: 8
Size: 346 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11800 Color: 18
Size: 4376 Color: 13
Size: 208 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11801 Color: 1
Size: 3821 Color: 19
Size: 762 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11830 Color: 18
Size: 3798 Color: 13
Size: 756 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11910 Color: 14
Size: 3730 Color: 13
Size: 744 Color: 5

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12220 Color: 7
Size: 3868 Color: 2
Size: 296 Color: 10

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12669 Color: 1
Size: 3097 Color: 16
Size: 618 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12786 Color: 7
Size: 2418 Color: 2
Size: 1180 Color: 6

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12802 Color: 13
Size: 2986 Color: 11
Size: 596 Color: 19

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13032 Color: 7
Size: 3068 Color: 7
Size: 284 Color: 9

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13146 Color: 7
Size: 2694 Color: 1
Size: 544 Color: 2

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13193 Color: 11
Size: 2763 Color: 1
Size: 428 Color: 5

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13426 Color: 3
Size: 1752 Color: 1
Size: 1206 Color: 3

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13668 Color: 13
Size: 2360 Color: 3
Size: 356 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13688 Color: 17
Size: 2352 Color: 11
Size: 344 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13779 Color: 0
Size: 1861 Color: 10
Size: 744 Color: 12

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 10
Size: 1528 Color: 3
Size: 928 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13932 Color: 6
Size: 1364 Color: 4
Size: 1088 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 14006 Color: 1
Size: 1970 Color: 8
Size: 408 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13990 Color: 5
Size: 1998 Color: 4
Size: 396 Color: 17

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 14022 Color: 4
Size: 1982 Color: 13
Size: 380 Color: 5

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 14045 Color: 15
Size: 1951 Color: 5
Size: 388 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 14148 Color: 7
Size: 1548 Color: 6
Size: 688 Color: 6

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 14151 Color: 9
Size: 1841 Color: 0
Size: 392 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 6
Size: 1690 Color: 5
Size: 526 Color: 14

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14246 Color: 10
Size: 1482 Color: 8
Size: 656 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14262 Color: 5
Size: 1434 Color: 11
Size: 688 Color: 12

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14300 Color: 10
Size: 1176 Color: 11
Size: 908 Color: 10

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14388 Color: 9
Size: 1404 Color: 8
Size: 592 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14424 Color: 4
Size: 1364 Color: 15
Size: 596 Color: 19

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14490 Color: 4
Size: 1480 Color: 12
Size: 414 Color: 12

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14494 Color: 19
Size: 1360 Color: 15
Size: 530 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14504 Color: 8
Size: 1508 Color: 7
Size: 372 Color: 5

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14606 Color: 12
Size: 1428 Color: 4
Size: 350 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 11
Size: 1448 Color: 6
Size: 320 Color: 17

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14620 Color: 3
Size: 1280 Color: 5
Size: 484 Color: 10

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14674 Color: 7
Size: 1382 Color: 10
Size: 328 Color: 10

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14676 Color: 18
Size: 1264 Color: 6
Size: 444 Color: 14

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14724 Color: 11
Size: 1176 Color: 6
Size: 484 Color: 17

Bin 43: 1 of cap free
Amount of items: 5
Items: 
Size: 8195 Color: 19
Size: 2568 Color: 17
Size: 2516 Color: 19
Size: 1578 Color: 15
Size: 1526 Color: 19

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 10701 Color: 4
Size: 5194 Color: 14
Size: 488 Color: 14

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 11676 Color: 11
Size: 4171 Color: 19
Size: 536 Color: 13

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 11873 Color: 9
Size: 3806 Color: 17
Size: 704 Color: 16

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 12245 Color: 1
Size: 3704 Color: 12
Size: 434 Color: 10

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 12260 Color: 11
Size: 3727 Color: 6
Size: 396 Color: 19

Bin 49: 1 of cap free
Amount of items: 3
Items: 
Size: 12708 Color: 17
Size: 3451 Color: 14
Size: 224 Color: 5

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 12840 Color: 5
Size: 3087 Color: 19
Size: 456 Color: 9

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 12965 Color: 18
Size: 2130 Color: 16
Size: 1288 Color: 15

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 13521 Color: 8
Size: 1928 Color: 2
Size: 934 Color: 3

Bin 53: 1 of cap free
Amount of items: 2
Items: 
Size: 13619 Color: 1
Size: 2764 Color: 14

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 13811 Color: 15
Size: 2140 Color: 8
Size: 432 Color: 11

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 14221 Color: 14
Size: 1522 Color: 0
Size: 640 Color: 11

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 14305 Color: 0
Size: 1328 Color: 16
Size: 750 Color: 19

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 14429 Color: 10
Size: 1426 Color: 13
Size: 528 Color: 5

Bin 58: 2 of cap free
Amount of items: 23
Items: 
Size: 912 Color: 3
Size: 880 Color: 6
Size: 880 Color: 3
Size: 860 Color: 14
Size: 860 Color: 9
Size: 860 Color: 1
Size: 856 Color: 10
Size: 784 Color: 1
Size: 776 Color: 6
Size: 776 Color: 6
Size: 768 Color: 5
Size: 764 Color: 7
Size: 752 Color: 9
Size: 750 Color: 19
Size: 746 Color: 3
Size: 688 Color: 0
Size: 672 Color: 15
Size: 672 Color: 11
Size: 616 Color: 12
Size: 570 Color: 17
Size: 324 Color: 8
Size: 312 Color: 11
Size: 304 Color: 4

Bin 59: 2 of cap free
Amount of items: 9
Items: 
Size: 8194 Color: 5
Size: 1476 Color: 7
Size: 1360 Color: 1
Size: 1144 Color: 0
Size: 1136 Color: 8
Size: 1052 Color: 0
Size: 1008 Color: 3
Size: 640 Color: 14
Size: 372 Color: 6

Bin 60: 2 of cap free
Amount of items: 3
Items: 
Size: 10087 Color: 4
Size: 4671 Color: 1
Size: 1624 Color: 16

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 10276 Color: 11
Size: 5738 Color: 5
Size: 368 Color: 13

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 12072 Color: 7
Size: 4310 Color: 15

Bin 63: 2 of cap free
Amount of items: 2
Items: 
Size: 12774 Color: 18
Size: 3608 Color: 13

Bin 64: 2 of cap free
Amount of items: 3
Items: 
Size: 13184 Color: 4
Size: 2862 Color: 5
Size: 336 Color: 18

Bin 65: 2 of cap free
Amount of items: 3
Items: 
Size: 13198 Color: 13
Size: 3104 Color: 11
Size: 80 Color: 16

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 13288 Color: 6
Size: 2230 Color: 0
Size: 864 Color: 18

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 8
Size: 2222 Color: 11
Size: 864 Color: 4

Bin 68: 2 of cap free
Amount of items: 2
Items: 
Size: 13372 Color: 12
Size: 3010 Color: 3

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 13434 Color: 16
Size: 1588 Color: 7
Size: 1360 Color: 11

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 14358 Color: 5
Size: 1740 Color: 11
Size: 284 Color: 6

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 14660 Color: 9
Size: 1626 Color: 15
Size: 96 Color: 18

Bin 72: 3 of cap free
Amount of items: 3
Items: 
Size: 11373 Color: 15
Size: 4696 Color: 1
Size: 312 Color: 9

Bin 73: 3 of cap free
Amount of items: 2
Items: 
Size: 11381 Color: 8
Size: 5000 Color: 14

Bin 74: 3 of cap free
Amount of items: 3
Items: 
Size: 11795 Color: 15
Size: 4302 Color: 16
Size: 284 Color: 4

Bin 75: 3 of cap free
Amount of items: 3
Items: 
Size: 11881 Color: 12
Size: 2576 Color: 4
Size: 1924 Color: 7

Bin 76: 3 of cap free
Amount of items: 2
Items: 
Size: 12628 Color: 5
Size: 3753 Color: 15

Bin 77: 3 of cap free
Amount of items: 3
Items: 
Size: 12681 Color: 0
Size: 3334 Color: 14
Size: 366 Color: 9

Bin 78: 3 of cap free
Amount of items: 2
Items: 
Size: 13257 Color: 8
Size: 3124 Color: 0

Bin 79: 3 of cap free
Amount of items: 3
Items: 
Size: 13320 Color: 2
Size: 2917 Color: 19
Size: 144 Color: 5

Bin 80: 3 of cap free
Amount of items: 3
Items: 
Size: 13551 Color: 17
Size: 2248 Color: 1
Size: 582 Color: 11

Bin 81: 3 of cap free
Amount of items: 2
Items: 
Size: 14261 Color: 3
Size: 2120 Color: 9

Bin 82: 3 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 12
Size: 1733 Color: 6

Bin 83: 4 of cap free
Amount of items: 3
Items: 
Size: 11301 Color: 1
Size: 4791 Color: 8
Size: 288 Color: 4

Bin 84: 4 of cap free
Amount of items: 3
Items: 
Size: 12398 Color: 10
Size: 3742 Color: 19
Size: 240 Color: 13

Bin 85: 4 of cap free
Amount of items: 2
Items: 
Size: 12936 Color: 8
Size: 3444 Color: 18

Bin 86: 4 of cap free
Amount of items: 3
Items: 
Size: 13060 Color: 13
Size: 3256 Color: 8
Size: 64 Color: 3

Bin 87: 4 of cap free
Amount of items: 3
Items: 
Size: 13470 Color: 16
Size: 2820 Color: 7
Size: 90 Color: 14

Bin 88: 4 of cap free
Amount of items: 3
Items: 
Size: 13820 Color: 7
Size: 1408 Color: 0
Size: 1152 Color: 1

Bin 89: 4 of cap free
Amount of items: 2
Items: 
Size: 14177 Color: 12
Size: 2203 Color: 16

Bin 90: 4 of cap free
Amount of items: 2
Items: 
Size: 14344 Color: 6
Size: 2036 Color: 8

Bin 91: 4 of cap free
Amount of items: 2
Items: 
Size: 14532 Color: 13
Size: 1848 Color: 16

Bin 92: 5 of cap free
Amount of items: 10
Items: 
Size: 8193 Color: 0
Size: 1048 Color: 14
Size: 1040 Color: 7
Size: 1036 Color: 18
Size: 1016 Color: 9
Size: 1008 Color: 7
Size: 976 Color: 11
Size: 946 Color: 8
Size: 748 Color: 15
Size: 368 Color: 19

Bin 93: 5 of cap free
Amount of items: 3
Items: 
Size: 12488 Color: 18
Size: 3439 Color: 18
Size: 452 Color: 9

Bin 94: 5 of cap free
Amount of items: 3
Items: 
Size: 13702 Color: 7
Size: 2607 Color: 5
Size: 70 Color: 1

Bin 95: 5 of cap free
Amount of items: 2
Items: 
Size: 13718 Color: 7
Size: 2661 Color: 9

Bin 96: 5 of cap free
Amount of items: 2
Items: 
Size: 13899 Color: 2
Size: 2480 Color: 16

Bin 97: 5 of cap free
Amount of items: 2
Items: 
Size: 14708 Color: 7
Size: 1671 Color: 6

Bin 98: 6 of cap free
Amount of items: 3
Items: 
Size: 10061 Color: 10
Size: 6005 Color: 5
Size: 312 Color: 2

Bin 99: 6 of cap free
Amount of items: 2
Items: 
Size: 13225 Color: 3
Size: 3153 Color: 6

Bin 100: 6 of cap free
Amount of items: 3
Items: 
Size: 13560 Color: 4
Size: 1694 Color: 5
Size: 1124 Color: 0

Bin 101: 6 of cap free
Amount of items: 2
Items: 
Size: 13948 Color: 13
Size: 2430 Color: 18

Bin 102: 7 of cap free
Amount of items: 2
Items: 
Size: 11700 Color: 18
Size: 4677 Color: 8

Bin 103: 7 of cap free
Amount of items: 2
Items: 
Size: 12644 Color: 2
Size: 3733 Color: 18

Bin 104: 7 of cap free
Amount of items: 3
Items: 
Size: 13445 Color: 17
Size: 2324 Color: 12
Size: 608 Color: 5

Bin 105: 7 of cap free
Amount of items: 2
Items: 
Size: 14072 Color: 3
Size: 2305 Color: 12

Bin 106: 8 of cap free
Amount of items: 3
Items: 
Size: 10056 Color: 5
Size: 5704 Color: 10
Size: 616 Color: 3

Bin 107: 8 of cap free
Amount of items: 2
Items: 
Size: 13604 Color: 7
Size: 2772 Color: 13

Bin 108: 8 of cap free
Amount of items: 2
Items: 
Size: 14138 Color: 6
Size: 2238 Color: 10

Bin 109: 8 of cap free
Amount of items: 2
Items: 
Size: 14332 Color: 3
Size: 2044 Color: 8

Bin 110: 8 of cap free
Amount of items: 2
Items: 
Size: 14434 Color: 8
Size: 1942 Color: 19

Bin 111: 9 of cap free
Amount of items: 3
Items: 
Size: 12677 Color: 2
Size: 3346 Color: 8
Size: 352 Color: 11

Bin 112: 10 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 9
Size: 6462 Color: 0
Size: 448 Color: 19

Bin 113: 10 of cap free
Amount of items: 3
Items: 
Size: 10266 Color: 18
Size: 5892 Color: 11
Size: 216 Color: 19

Bin 114: 10 of cap free
Amount of items: 2
Items: 
Size: 13741 Color: 16
Size: 2633 Color: 4

Bin 115: 10 of cap free
Amount of items: 2
Items: 
Size: 14084 Color: 15
Size: 2290 Color: 14

Bin 116: 11 of cap free
Amount of items: 3
Items: 
Size: 12276 Color: 11
Size: 3761 Color: 13
Size: 336 Color: 9

Bin 117: 11 of cap free
Amount of items: 2
Items: 
Size: 12957 Color: 6
Size: 3416 Color: 18

Bin 118: 11 of cap free
Amount of items: 2
Items: 
Size: 14202 Color: 18
Size: 2171 Color: 19

Bin 119: 12 of cap free
Amount of items: 7
Items: 
Size: 8196 Color: 0
Size: 1604 Color: 7
Size: 1582 Color: 4
Size: 1576 Color: 4
Size: 1516 Color: 8
Size: 1162 Color: 16
Size: 736 Color: 11

Bin 120: 12 of cap free
Amount of items: 2
Items: 
Size: 13848 Color: 13
Size: 2524 Color: 3

Bin 121: 13 of cap free
Amount of items: 2
Items: 
Size: 9544 Color: 9
Size: 6827 Color: 16

Bin 122: 13 of cap free
Amount of items: 2
Items: 
Size: 14103 Color: 10
Size: 2268 Color: 16

Bin 123: 14 of cap free
Amount of items: 3
Items: 
Size: 9145 Color: 8
Size: 6825 Color: 5
Size: 400 Color: 7

Bin 124: 14 of cap free
Amount of items: 2
Items: 
Size: 14354 Color: 9
Size: 2016 Color: 17

Bin 125: 14 of cap free
Amount of items: 2
Items: 
Size: 14666 Color: 14
Size: 1704 Color: 11

Bin 126: 14 of cap free
Amount of items: 2
Items: 
Size: 14730 Color: 6
Size: 1640 Color: 17

Bin 127: 15 of cap free
Amount of items: 2
Items: 
Size: 13667 Color: 14
Size: 2702 Color: 15

Bin 128: 15 of cap free
Amount of items: 2
Items: 
Size: 14296 Color: 15
Size: 2073 Color: 8

Bin 129: 15 of cap free
Amount of items: 2
Items: 
Size: 14468 Color: 3
Size: 1901 Color: 1

Bin 130: 16 of cap free
Amount of items: 2
Items: 
Size: 10840 Color: 18
Size: 5528 Color: 10

Bin 131: 16 of cap free
Amount of items: 2
Items: 
Size: 11818 Color: 17
Size: 4550 Color: 18

Bin 132: 17 of cap free
Amount of items: 2
Items: 
Size: 13365 Color: 3
Size: 3002 Color: 16

Bin 133: 18 of cap free
Amount of items: 2
Items: 
Size: 12818 Color: 2
Size: 3548 Color: 4

Bin 134: 18 of cap free
Amount of items: 2
Items: 
Size: 13979 Color: 4
Size: 2387 Color: 3

Bin 135: 20 of cap free
Amount of items: 3
Items: 
Size: 13004 Color: 1
Size: 2808 Color: 4
Size: 552 Color: 18

Bin 136: 20 of cap free
Amount of items: 2
Items: 
Size: 13076 Color: 12
Size: 3288 Color: 16

Bin 137: 20 of cap free
Amount of items: 2
Items: 
Size: 14696 Color: 10
Size: 1668 Color: 14

Bin 138: 23 of cap free
Amount of items: 3
Items: 
Size: 9225 Color: 17
Size: 6712 Color: 18
Size: 424 Color: 3

Bin 139: 23 of cap free
Amount of items: 2
Items: 
Size: 12885 Color: 18
Size: 3476 Color: 1

Bin 140: 24 of cap free
Amount of items: 2
Items: 
Size: 13710 Color: 0
Size: 2650 Color: 4

Bin 141: 25 of cap free
Amount of items: 3
Items: 
Size: 11238 Color: 12
Size: 4737 Color: 15
Size: 384 Color: 11

Bin 142: 26 of cap free
Amount of items: 2
Items: 
Size: 13364 Color: 9
Size: 2994 Color: 0

Bin 143: 26 of cap free
Amount of items: 2
Items: 
Size: 14484 Color: 2
Size: 1874 Color: 11

Bin 144: 27 of cap free
Amount of items: 2
Items: 
Size: 11905 Color: 10
Size: 4452 Color: 6

Bin 145: 28 of cap free
Amount of items: 3
Items: 
Size: 9364 Color: 12
Size: 6480 Color: 15
Size: 512 Color: 1

Bin 146: 31 of cap free
Amount of items: 2
Items: 
Size: 12521 Color: 6
Size: 3832 Color: 15

Bin 147: 32 of cap free
Amount of items: 3
Items: 
Size: 12296 Color: 19
Size: 3912 Color: 13
Size: 144 Color: 3

Bin 148: 32 of cap free
Amount of items: 2
Items: 
Size: 14281 Color: 19
Size: 2071 Color: 15

Bin 149: 33 of cap free
Amount of items: 3
Items: 
Size: 10781 Color: 19
Size: 3825 Color: 10
Size: 1745 Color: 0

Bin 150: 34 of cap free
Amount of items: 2
Items: 
Size: 13206 Color: 9
Size: 3144 Color: 13

Bin 151: 34 of cap free
Amount of items: 2
Items: 
Size: 13766 Color: 1
Size: 2584 Color: 2

Bin 152: 34 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 13
Size: 1782 Color: 0

Bin 153: 36 of cap free
Amount of items: 3
Items: 
Size: 11944 Color: 19
Size: 3908 Color: 5
Size: 496 Color: 8

Bin 154: 37 of cap free
Amount of items: 2
Items: 
Size: 13830 Color: 17
Size: 2517 Color: 14

Bin 155: 37 of cap free
Amount of items: 2
Items: 
Size: 14291 Color: 15
Size: 2056 Color: 16

Bin 156: 38 of cap free
Amount of items: 3
Items: 
Size: 11144 Color: 1
Size: 4914 Color: 15
Size: 288 Color: 0

Bin 157: 38 of cap free
Amount of items: 2
Items: 
Size: 11222 Color: 2
Size: 5124 Color: 10

Bin 158: 38 of cap free
Amount of items: 3
Items: 
Size: 12440 Color: 15
Size: 2974 Color: 0
Size: 932 Color: 11

Bin 159: 38 of cap free
Amount of items: 2
Items: 
Size: 13884 Color: 0
Size: 2462 Color: 6

Bin 160: 39 of cap free
Amount of items: 2
Items: 
Size: 14200 Color: 10
Size: 2145 Color: 11

Bin 161: 40 of cap free
Amount of items: 3
Items: 
Size: 8760 Color: 11
Size: 6824 Color: 0
Size: 760 Color: 15

Bin 162: 41 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 6
Size: 2857 Color: 15

Bin 163: 43 of cap free
Amount of items: 2
Items: 
Size: 11897 Color: 11
Size: 4444 Color: 8

Bin 164: 44 of cap free
Amount of items: 3
Items: 
Size: 9293 Color: 5
Size: 6775 Color: 4
Size: 272 Color: 1

Bin 165: 44 of cap free
Amount of items: 3
Items: 
Size: 11913 Color: 5
Size: 2422 Color: 9
Size: 2005 Color: 0

Bin 166: 48 of cap free
Amount of items: 3
Items: 
Size: 10773 Color: 3
Size: 3112 Color: 5
Size: 2451 Color: 14

Bin 167: 50 of cap free
Amount of items: 3
Items: 
Size: 11128 Color: 2
Size: 4318 Color: 6
Size: 888 Color: 4

Bin 168: 51 of cap free
Amount of items: 3
Items: 
Size: 10760 Color: 3
Size: 3741 Color: 11
Size: 1832 Color: 4

Bin 169: 52 of cap free
Amount of items: 3
Items: 
Size: 8344 Color: 8
Size: 6820 Color: 1
Size: 1168 Color: 18

Bin 170: 54 of cap free
Amount of items: 2
Items: 
Size: 10154 Color: 14
Size: 6176 Color: 19

Bin 171: 55 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 5
Size: 4177 Color: 2

Bin 172: 55 of cap free
Amount of items: 2
Items: 
Size: 13478 Color: 9
Size: 2851 Color: 10

Bin 173: 55 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 15
Size: 1771 Color: 19

Bin 174: 56 of cap free
Amount of items: 2
Items: 
Size: 9502 Color: 9
Size: 6826 Color: 4

Bin 175: 60 of cap free
Amount of items: 2
Items: 
Size: 14554 Color: 18
Size: 1770 Color: 19

Bin 176: 63 of cap free
Amount of items: 2
Items: 
Size: 9217 Color: 8
Size: 7104 Color: 17

Bin 177: 68 of cap free
Amount of items: 2
Items: 
Size: 11214 Color: 8
Size: 5102 Color: 19

Bin 178: 68 of cap free
Amount of items: 2
Items: 
Size: 11684 Color: 0
Size: 4632 Color: 18

Bin 179: 74 of cap free
Amount of items: 2
Items: 
Size: 12386 Color: 14
Size: 3924 Color: 10

Bin 180: 78 of cap free
Amount of items: 3
Items: 
Size: 9316 Color: 6
Size: 5966 Color: 2
Size: 1024 Color: 3

Bin 181: 85 of cap free
Amount of items: 2
Items: 
Size: 12375 Color: 8
Size: 3924 Color: 1

Bin 182: 86 of cap free
Amount of items: 2
Items: 
Size: 11206 Color: 9
Size: 5092 Color: 14

Bin 183: 87 of cap free
Amount of items: 6
Items: 
Size: 8200 Color: 18
Size: 2265 Color: 16
Size: 2260 Color: 2
Size: 1716 Color: 5
Size: 1364 Color: 1
Size: 492 Color: 3

Bin 184: 90 of cap free
Amount of items: 2
Items: 
Size: 12794 Color: 5
Size: 3500 Color: 18

Bin 185: 91 of cap free
Amount of items: 2
Items: 
Size: 11044 Color: 15
Size: 5249 Color: 11

Bin 186: 96 of cap free
Amount of items: 3
Items: 
Size: 8624 Color: 4
Size: 5911 Color: 15
Size: 1753 Color: 2

Bin 187: 98 of cap free
Amount of items: 2
Items: 
Size: 11894 Color: 17
Size: 4392 Color: 9

Bin 188: 100 of cap free
Amount of items: 2
Items: 
Size: 8204 Color: 9
Size: 8080 Color: 0

Bin 189: 102 of cap free
Amount of items: 3
Items: 
Size: 9768 Color: 5
Size: 5768 Color: 10
Size: 746 Color: 8

Bin 190: 115 of cap free
Amount of items: 2
Items: 
Size: 10236 Color: 11
Size: 6033 Color: 18

Bin 191: 115 of cap free
Amount of items: 3
Items: 
Size: 11060 Color: 9
Size: 3341 Color: 5
Size: 1868 Color: 14

Bin 192: 124 of cap free
Amount of items: 3
Items: 
Size: 9300 Color: 0
Size: 6360 Color: 11
Size: 600 Color: 7

Bin 193: 154 of cap free
Amount of items: 2
Items: 
Size: 10942 Color: 13
Size: 5288 Color: 9

Bin 194: 164 of cap free
Amount of items: 34
Items: 
Size: 680 Color: 9
Size: 668 Color: 4
Size: 664 Color: 13
Size: 660 Color: 12
Size: 596 Color: 6
Size: 568 Color: 15
Size: 560 Color: 12
Size: 554 Color: 15
Size: 544 Color: 1
Size: 528 Color: 6
Size: 520 Color: 2
Size: 512 Color: 14
Size: 502 Color: 17
Size: 496 Color: 12
Size: 496 Color: 7
Size: 488 Color: 7
Size: 480 Color: 17
Size: 476 Color: 13
Size: 464 Color: 3
Size: 460 Color: 16
Size: 448 Color: 3
Size: 444 Color: 9
Size: 444 Color: 5
Size: 440 Color: 10
Size: 424 Color: 10
Size: 416 Color: 16
Size: 400 Color: 19
Size: 400 Color: 4
Size: 384 Color: 1
Size: 320 Color: 8
Size: 304 Color: 11
Size: 304 Color: 11
Size: 288 Color: 13
Size: 288 Color: 8

Bin 195: 172 of cap free
Amount of items: 3
Items: 
Size: 10296 Color: 10
Size: 5084 Color: 7
Size: 832 Color: 0

Bin 196: 187 of cap free
Amount of items: 2
Items: 
Size: 10926 Color: 4
Size: 5271 Color: 7

Bin 197: 228 of cap free
Amount of items: 2
Items: 
Size: 8634 Color: 0
Size: 7522 Color: 12

Bin 198: 262 of cap free
Amount of items: 2
Items: 
Size: 9294 Color: 19
Size: 6828 Color: 9

Bin 199: 11584 of cap free
Amount of items: 15
Items: 
Size: 376 Color: 19
Size: 352 Color: 19
Size: 352 Color: 15
Size: 348 Color: 17
Size: 336 Color: 16
Size: 336 Color: 15
Size: 336 Color: 5
Size: 328 Color: 1
Size: 312 Color: 14
Size: 304 Color: 18
Size: 304 Color: 10
Size: 292 Color: 11
Size: 280 Color: 17
Size: 272 Color: 13
Size: 272 Color: 8

Total size: 3244032
Total free space: 16384

