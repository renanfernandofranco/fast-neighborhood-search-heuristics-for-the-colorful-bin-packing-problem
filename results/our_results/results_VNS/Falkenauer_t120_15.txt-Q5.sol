Capicity Bin: 1000
Lower Bound: 40

Bins used: 40
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 3
Size: 292 Color: 3
Size: 255 Color: 2

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 2
Size: 322 Color: 3
Size: 267 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 2
Size: 332 Color: 1
Size: 282 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 267 Color: 3
Size: 261 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 2
Size: 262 Color: 1
Size: 251 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 317 Color: 0
Size: 258 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 296 Color: 3
Size: 296 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 362 Color: 1
Size: 266 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 2
Size: 303 Color: 3
Size: 251 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 0
Size: 277 Color: 2
Size: 280 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 3
Size: 305 Color: 0
Size: 255 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 2
Size: 287 Color: 2
Size: 270 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 2
Size: 344 Color: 1
Size: 256 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 3
Size: 279 Color: 4
Size: 258 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 2
Size: 260 Color: 1
Size: 256 Color: 2

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 3
Size: 266 Color: 1
Size: 252 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 334 Color: 4
Size: 304 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 363 Color: 3
Size: 250 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 303 Color: 0
Size: 271 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 4
Size: 322 Color: 0
Size: 274 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 0
Size: 274 Color: 1
Size: 261 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 281 Color: 2
Size: 250 Color: 1

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 4
Size: 320 Color: 4
Size: 302 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 333 Color: 4
Size: 310 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 0
Size: 267 Color: 2
Size: 250 Color: 3

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 3
Size: 306 Color: 4
Size: 272 Color: 3

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 285 Color: 0
Size: 269 Color: 3

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 3
Size: 365 Color: 4
Size: 262 Color: 4

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 2
Size: 322 Color: 0
Size: 252 Color: 3

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 4
Size: 303 Color: 0
Size: 264 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 4
Size: 330 Color: 4
Size: 270 Color: 2

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 2
Size: 271 Color: 2
Size: 257 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 4
Size: 354 Color: 0
Size: 279 Color: 2

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 2
Size: 276 Color: 4
Size: 251 Color: 4

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 2
Size: 363 Color: 0
Size: 274 Color: 3

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 0
Size: 337 Color: 1
Size: 277 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 307 Color: 2
Size: 306 Color: 1

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 2
Size: 294 Color: 4
Size: 263 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 3
Size: 278 Color: 0
Size: 264 Color: 4

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 0
Size: 270 Color: 3
Size: 251 Color: 1

Total size: 40000
Total free space: 0

