Capicity Bin: 16304
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8200 Color: 0
Size: 7400 Color: 1
Size: 704 Color: 15

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 8984 Color: 7
Size: 6872 Color: 1
Size: 448 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10168 Color: 18
Size: 5308 Color: 10
Size: 828 Color: 8

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 10598 Color: 5
Size: 5314 Color: 14
Size: 392 Color: 11

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11372 Color: 6
Size: 4836 Color: 0
Size: 96 Color: 3

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11452 Color: 19
Size: 4220 Color: 0
Size: 632 Color: 4

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11698 Color: 6
Size: 4200 Color: 16
Size: 406 Color: 16

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 11839 Color: 5
Size: 3411 Color: 9
Size: 1054 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12184 Color: 5
Size: 3604 Color: 19
Size: 516 Color: 8

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 12218 Color: 10
Size: 3406 Color: 16
Size: 680 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 12248 Color: 5
Size: 3688 Color: 15
Size: 368 Color: 14

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 12328 Color: 19
Size: 3608 Color: 17
Size: 368 Color: 8

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 12566 Color: 2
Size: 3260 Color: 1
Size: 478 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 12757 Color: 3
Size: 2957 Color: 0
Size: 590 Color: 17

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 12822 Color: 18
Size: 3090 Color: 12
Size: 392 Color: 7

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 12902 Color: 3
Size: 2020 Color: 5
Size: 1382 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 0
Size: 3196 Color: 19
Size: 108 Color: 17

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 12936 Color: 17
Size: 3176 Color: 7
Size: 192 Color: 7

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 12956 Color: 8
Size: 2902 Color: 4
Size: 446 Color: 5

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13036 Color: 0
Size: 1662 Color: 5
Size: 1606 Color: 19

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 12972 Color: 13
Size: 2396 Color: 9
Size: 936 Color: 13

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 12982 Color: 13
Size: 2570 Color: 15
Size: 752 Color: 9

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13103 Color: 8
Size: 2153 Color: 7
Size: 1048 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13142 Color: 1
Size: 2382 Color: 18
Size: 780 Color: 11

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13240 Color: 18
Size: 2568 Color: 8
Size: 496 Color: 18

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13372 Color: 19
Size: 1484 Color: 17
Size: 1448 Color: 14

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13422 Color: 8
Size: 2654 Color: 19
Size: 228 Color: 18

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 13436 Color: 2
Size: 2580 Color: 8
Size: 288 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 13427 Color: 8
Size: 2365 Color: 6
Size: 512 Color: 2

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 13512 Color: 13
Size: 2328 Color: 6
Size: 464 Color: 16

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 13516 Color: 10
Size: 2460 Color: 8
Size: 328 Color: 5

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 13592 Color: 19
Size: 2408 Color: 13
Size: 304 Color: 13

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 13610 Color: 5
Size: 2264 Color: 2
Size: 430 Color: 13

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 13676 Color: 18
Size: 1604 Color: 18
Size: 1024 Color: 13

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 13678 Color: 8
Size: 2138 Color: 14
Size: 488 Color: 8

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 13697 Color: 10
Size: 2173 Color: 0
Size: 434 Color: 13

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 13721 Color: 8
Size: 2131 Color: 15
Size: 452 Color: 11

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 13796 Color: 10
Size: 2092 Color: 2
Size: 416 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 13884 Color: 6
Size: 1676 Color: 2
Size: 744 Color: 14

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 13928 Color: 11
Size: 1928 Color: 1
Size: 448 Color: 16

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14010 Color: 14
Size: 1422 Color: 17
Size: 872 Color: 5

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14088 Color: 3
Size: 1344 Color: 13
Size: 872 Color: 2

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14168 Color: 12
Size: 1088 Color: 11
Size: 1048 Color: 6

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14178 Color: 3
Size: 1506 Color: 8
Size: 620 Color: 13

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14298 Color: 17
Size: 1674 Color: 18
Size: 332 Color: 19

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14300 Color: 7
Size: 1524 Color: 5
Size: 480 Color: 15

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14306 Color: 19
Size: 1190 Color: 0
Size: 808 Color: 8

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14344 Color: 9
Size: 1660 Color: 4
Size: 300 Color: 19

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14364 Color: 8
Size: 1384 Color: 9
Size: 556 Color: 1

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14434 Color: 7
Size: 1224 Color: 15
Size: 646 Color: 16

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14476 Color: 1
Size: 1356 Color: 12
Size: 472 Color: 18

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14488 Color: 16
Size: 1192 Color: 7
Size: 624 Color: 8

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 14516 Color: 7
Size: 1168 Color: 4
Size: 620 Color: 1

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 14532 Color: 11
Size: 1492 Color: 5
Size: 280 Color: 11

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 14596 Color: 8
Size: 1060 Color: 4
Size: 648 Color: 4

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 14598 Color: 4
Size: 1066 Color: 13
Size: 640 Color: 17

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 14612 Color: 19
Size: 1168 Color: 16
Size: 524 Color: 8

Bin 58: 1 of cap free
Amount of items: 5
Items: 
Size: 8168 Color: 2
Size: 3633 Color: 6
Size: 3118 Color: 0
Size: 832 Color: 18
Size: 552 Color: 12

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 8169 Color: 5
Size: 6782 Color: 1
Size: 1352 Color: 18

Bin 60: 1 of cap free
Amount of items: 3
Items: 
Size: 9688 Color: 10
Size: 5259 Color: 15
Size: 1356 Color: 5

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 11154 Color: 13
Size: 2903 Color: 18
Size: 2246 Color: 13

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 11992 Color: 0
Size: 3735 Color: 18
Size: 576 Color: 6

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 12072 Color: 10
Size: 3399 Color: 13
Size: 832 Color: 16

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 12211 Color: 8
Size: 2860 Color: 0
Size: 1232 Color: 9

Bin 65: 1 of cap free
Amount of items: 3
Items: 
Size: 12427 Color: 11
Size: 3596 Color: 10
Size: 280 Color: 18

Bin 66: 1 of cap free
Amount of items: 3
Items: 
Size: 12459 Color: 2
Size: 3384 Color: 0
Size: 460 Color: 6

Bin 67: 1 of cap free
Amount of items: 3
Items: 
Size: 12556 Color: 5
Size: 3311 Color: 13
Size: 436 Color: 15

Bin 68: 1 of cap free
Amount of items: 3
Items: 
Size: 12761 Color: 0
Size: 2190 Color: 8
Size: 1352 Color: 14

Bin 69: 1 of cap free
Amount of items: 3
Items: 
Size: 12727 Color: 19
Size: 3166 Color: 19
Size: 410 Color: 14

Bin 70: 1 of cap free
Amount of items: 3
Items: 
Size: 12876 Color: 13
Size: 2371 Color: 5
Size: 1056 Color: 14

Bin 71: 1 of cap free
Amount of items: 3
Items: 
Size: 12950 Color: 8
Size: 2669 Color: 7
Size: 684 Color: 7

Bin 72: 1 of cap free
Amount of items: 2
Items: 
Size: 13072 Color: 11
Size: 3231 Color: 18

Bin 73: 1 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 17
Size: 2521 Color: 10
Size: 560 Color: 16

Bin 74: 1 of cap free
Amount of items: 3
Items: 
Size: 13263 Color: 9
Size: 2288 Color: 13
Size: 752 Color: 17

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 10424 Color: 6
Size: 5246 Color: 12
Size: 632 Color: 1

Bin 76: 2 of cap free
Amount of items: 3
Items: 
Size: 11244 Color: 0
Size: 4290 Color: 6
Size: 768 Color: 0

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 11618 Color: 8
Size: 4684 Color: 15

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 11746 Color: 6
Size: 4248 Color: 8
Size: 308 Color: 3

Bin 79: 2 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 3
Size: 4284 Color: 2
Size: 256 Color: 10

Bin 80: 2 of cap free
Amount of items: 3
Items: 
Size: 12197 Color: 11
Size: 3721 Color: 5
Size: 384 Color: 10

Bin 81: 2 of cap free
Amount of items: 3
Items: 
Size: 12582 Color: 7
Size: 3124 Color: 16
Size: 596 Color: 15

Bin 82: 2 of cap free
Amount of items: 3
Items: 
Size: 12598 Color: 8
Size: 3528 Color: 7
Size: 176 Color: 4

Bin 83: 2 of cap free
Amount of items: 3
Items: 
Size: 13411 Color: 18
Size: 2731 Color: 0
Size: 160 Color: 9

Bin 84: 2 of cap free
Amount of items: 3
Items: 
Size: 13930 Color: 4
Size: 1992 Color: 8
Size: 380 Color: 11

Bin 85: 2 of cap free
Amount of items: 2
Items: 
Size: 13988 Color: 5
Size: 2314 Color: 15

Bin 86: 2 of cap free
Amount of items: 3
Items: 
Size: 14060 Color: 16
Size: 1858 Color: 9
Size: 384 Color: 8

Bin 87: 3 of cap free
Amount of items: 7
Items: 
Size: 8156 Color: 16
Size: 2041 Color: 11
Size: 1766 Color: 5
Size: 1666 Color: 18
Size: 1512 Color: 12
Size: 832 Color: 14
Size: 328 Color: 7

Bin 88: 3 of cap free
Amount of items: 3
Items: 
Size: 10678 Color: 8
Size: 5271 Color: 8
Size: 352 Color: 19

Bin 89: 3 of cap free
Amount of items: 3
Items: 
Size: 10781 Color: 13
Size: 5192 Color: 0
Size: 328 Color: 8

Bin 90: 3 of cap free
Amount of items: 3
Items: 
Size: 11224 Color: 19
Size: 4613 Color: 5
Size: 464 Color: 15

Bin 91: 3 of cap free
Amount of items: 3
Items: 
Size: 12396 Color: 0
Size: 3423 Color: 9
Size: 482 Color: 13

Bin 92: 3 of cap free
Amount of items: 3
Items: 
Size: 12331 Color: 1
Size: 3842 Color: 9
Size: 128 Color: 7

Bin 93: 3 of cap free
Amount of items: 3
Items: 
Size: 13029 Color: 8
Size: 2000 Color: 5
Size: 1272 Color: 0

Bin 94: 3 of cap free
Amount of items: 3
Items: 
Size: 13641 Color: 15
Size: 1364 Color: 0
Size: 1296 Color: 7

Bin 95: 3 of cap free
Amount of items: 2
Items: 
Size: 13857 Color: 16
Size: 2444 Color: 13

Bin 96: 4 of cap free
Amount of items: 3
Items: 
Size: 9161 Color: 6
Size: 6771 Color: 1
Size: 368 Color: 10

Bin 97: 4 of cap free
Amount of items: 3
Items: 
Size: 9940 Color: 14
Size: 5928 Color: 16
Size: 432 Color: 12

Bin 98: 4 of cap free
Amount of items: 3
Items: 
Size: 9979 Color: 15
Size: 5953 Color: 11
Size: 368 Color: 12

Bin 99: 4 of cap free
Amount of items: 3
Items: 
Size: 11272 Color: 3
Size: 3180 Color: 14
Size: 1848 Color: 0

Bin 100: 4 of cap free
Amount of items: 3
Items: 
Size: 11823 Color: 19
Size: 4109 Color: 5
Size: 368 Color: 18

Bin 101: 4 of cap free
Amount of items: 3
Items: 
Size: 11880 Color: 12
Size: 4124 Color: 7
Size: 296 Color: 7

Bin 102: 4 of cap free
Amount of items: 3
Items: 
Size: 12076 Color: 14
Size: 3592 Color: 0
Size: 632 Color: 15

Bin 103: 4 of cap free
Amount of items: 3
Items: 
Size: 13450 Color: 17
Size: 2770 Color: 0
Size: 80 Color: 7

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 9930 Color: 7
Size: 5927 Color: 13
Size: 442 Color: 18

Bin 105: 5 of cap free
Amount of items: 3
Items: 
Size: 11785 Color: 0
Size: 3102 Color: 15
Size: 1412 Color: 17

Bin 106: 5 of cap free
Amount of items: 2
Items: 
Size: 14008 Color: 1
Size: 2291 Color: 18

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 14078 Color: 15
Size: 2221 Color: 17

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 14238 Color: 11
Size: 2061 Color: 13

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 10010 Color: 3
Size: 5552 Color: 4
Size: 736 Color: 6

Bin 110: 6 of cap free
Amount of items: 3
Items: 
Size: 11375 Color: 10
Size: 4603 Color: 10
Size: 320 Color: 19

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 0
Size: 3774 Color: 0
Size: 746 Color: 7

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 15
Size: 2782 Color: 0
Size: 240 Color: 16

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 13774 Color: 18
Size: 2524 Color: 6

Bin 114: 6 of cap free
Amount of items: 2
Items: 
Size: 14316 Color: 6
Size: 1982 Color: 13

Bin 115: 6 of cap free
Amount of items: 2
Items: 
Size: 14430 Color: 14
Size: 1868 Color: 10

Bin 116: 7 of cap free
Amount of items: 3
Items: 
Size: 8162 Color: 9
Size: 6781 Color: 10
Size: 1354 Color: 2

Bin 117: 7 of cap free
Amount of items: 3
Items: 
Size: 9193 Color: 1
Size: 6488 Color: 1
Size: 616 Color: 19

Bin 118: 7 of cap free
Amount of items: 2
Items: 
Size: 10769 Color: 10
Size: 5528 Color: 5

Bin 119: 7 of cap free
Amount of items: 2
Items: 
Size: 12792 Color: 7
Size: 3505 Color: 14

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 9208 Color: 7
Size: 7088 Color: 2

Bin 121: 8 of cap free
Amount of items: 2
Items: 
Size: 14574 Color: 19
Size: 1722 Color: 18

Bin 122: 9 of cap free
Amount of items: 9
Items: 
Size: 8153 Color: 3
Size: 1352 Color: 2
Size: 1184 Color: 15
Size: 1168 Color: 4
Size: 1050 Color: 13
Size: 1008 Color: 14
Size: 948 Color: 18
Size: 928 Color: 15
Size: 504 Color: 19

Bin 123: 9 of cap free
Amount of items: 3
Items: 
Size: 8940 Color: 13
Size: 5245 Color: 10
Size: 2110 Color: 14

Bin 124: 9 of cap free
Amount of items: 2
Items: 
Size: 12821 Color: 8
Size: 3474 Color: 12

Bin 125: 9 of cap free
Amount of items: 2
Items: 
Size: 13657 Color: 14
Size: 2638 Color: 17

Bin 126: 9 of cap free
Amount of items: 2
Items: 
Size: 13833 Color: 9
Size: 2462 Color: 6

Bin 127: 10 of cap free
Amount of items: 2
Items: 
Size: 12138 Color: 12
Size: 4156 Color: 1

Bin 128: 10 of cap free
Amount of items: 2
Items: 
Size: 12492 Color: 13
Size: 3802 Color: 11

Bin 129: 10 of cap free
Amount of items: 3
Items: 
Size: 13201 Color: 16
Size: 2269 Color: 19
Size: 824 Color: 0

Bin 130: 10 of cap free
Amount of items: 2
Items: 
Size: 14332 Color: 16
Size: 1962 Color: 1

Bin 131: 10 of cap free
Amount of items: 2
Items: 
Size: 14380 Color: 5
Size: 1914 Color: 19

Bin 132: 10 of cap free
Amount of items: 2
Items: 
Size: 14650 Color: 19
Size: 1644 Color: 4

Bin 133: 11 of cap free
Amount of items: 2
Items: 
Size: 13312 Color: 8
Size: 2981 Color: 6

Bin 134: 11 of cap free
Amount of items: 2
Items: 
Size: 13356 Color: 7
Size: 2937 Color: 6

Bin 135: 12 of cap free
Amount of items: 3
Items: 
Size: 9860 Color: 10
Size: 6080 Color: 1
Size: 352 Color: 13

Bin 136: 12 of cap free
Amount of items: 2
Items: 
Size: 11164 Color: 13
Size: 5128 Color: 9

Bin 137: 12 of cap free
Amount of items: 2
Items: 
Size: 12506 Color: 4
Size: 3786 Color: 12

Bin 138: 12 of cap free
Amount of items: 2
Items: 
Size: 14552 Color: 1
Size: 1740 Color: 11

Bin 139: 13 of cap free
Amount of items: 2
Items: 
Size: 13567 Color: 0
Size: 2724 Color: 2

Bin 140: 14 of cap free
Amount of items: 33
Items: 
Size: 662 Color: 19
Size: 656 Color: 12
Size: 624 Color: 14
Size: 590 Color: 9
Size: 586 Color: 19
Size: 580 Color: 18
Size: 580 Color: 17
Size: 568 Color: 0
Size: 556 Color: 18
Size: 552 Color: 17
Size: 552 Color: 1
Size: 544 Color: 13
Size: 544 Color: 9
Size: 544 Color: 1
Size: 532 Color: 4
Size: 528 Color: 4
Size: 488 Color: 11
Size: 488 Color: 3
Size: 472 Color: 19
Size: 472 Color: 18
Size: 472 Color: 15
Size: 464 Color: 3
Size: 440 Color: 12
Size: 432 Color: 5
Size: 428 Color: 14
Size: 424 Color: 13
Size: 424 Color: 4
Size: 420 Color: 17
Size: 344 Color: 6
Size: 344 Color: 0
Size: 332 Color: 2
Size: 328 Color: 2
Size: 320 Color: 8

Bin 141: 14 of cap free
Amount of items: 3
Items: 
Size: 9130 Color: 7
Size: 6396 Color: 8
Size: 764 Color: 3

Bin 142: 14 of cap free
Amount of items: 2
Items: 
Size: 11996 Color: 19
Size: 4294 Color: 8

Bin 143: 14 of cap free
Amount of items: 2
Items: 
Size: 13354 Color: 8
Size: 2936 Color: 1

Bin 144: 14 of cap free
Amount of items: 2
Items: 
Size: 13452 Color: 14
Size: 2838 Color: 18

Bin 145: 14 of cap free
Amount of items: 2
Items: 
Size: 13530 Color: 4
Size: 2760 Color: 18

Bin 146: 16 of cap free
Amount of items: 2
Items: 
Size: 12152 Color: 11
Size: 4136 Color: 3

Bin 147: 16 of cap free
Amount of items: 2
Items: 
Size: 14648 Color: 16
Size: 1640 Color: 1

Bin 148: 16 of cap free
Amount of items: 2
Items: 
Size: 14668 Color: 17
Size: 1620 Color: 13

Bin 149: 17 of cap free
Amount of items: 3
Items: 
Size: 9995 Color: 16
Size: 5924 Color: 1
Size: 368 Color: 14

Bin 150: 17 of cap free
Amount of items: 2
Items: 
Size: 12243 Color: 13
Size: 4044 Color: 7

Bin 151: 18 of cap free
Amount of items: 3
Items: 
Size: 9464 Color: 12
Size: 5982 Color: 6
Size: 840 Color: 19

Bin 152: 18 of cap free
Amount of items: 2
Items: 
Size: 12966 Color: 15
Size: 3320 Color: 13

Bin 153: 18 of cap free
Amount of items: 2
Items: 
Size: 14568 Color: 14
Size: 1718 Color: 9

Bin 154: 19 of cap free
Amount of items: 2
Items: 
Size: 13080 Color: 18
Size: 3205 Color: 2

Bin 155: 20 of cap free
Amount of items: 2
Items: 
Size: 13749 Color: 6
Size: 2535 Color: 11

Bin 156: 22 of cap free
Amount of items: 3
Items: 
Size: 8520 Color: 10
Size: 6786 Color: 18
Size: 976 Color: 0

Bin 157: 22 of cap free
Amount of items: 2
Items: 
Size: 14498 Color: 7
Size: 1784 Color: 9

Bin 158: 23 of cap free
Amount of items: 3
Items: 
Size: 11324 Color: 8
Size: 4681 Color: 13
Size: 276 Color: 1

Bin 159: 25 of cap free
Amount of items: 2
Items: 
Size: 13583 Color: 19
Size: 2696 Color: 11

Bin 160: 26 of cap free
Amount of items: 2
Items: 
Size: 10428 Color: 15
Size: 5850 Color: 4

Bin 161: 27 of cap free
Amount of items: 2
Items: 
Size: 9196 Color: 15
Size: 7081 Color: 4

Bin 162: 30 of cap free
Amount of items: 2
Items: 
Size: 13950 Color: 6
Size: 2324 Color: 19

Bin 163: 30 of cap free
Amount of items: 2
Items: 
Size: 14074 Color: 2
Size: 2200 Color: 0

Bin 164: 36 of cap free
Amount of items: 2
Items: 
Size: 13470 Color: 15
Size: 2798 Color: 2

Bin 165: 38 of cap free
Amount of items: 7
Items: 
Size: 8154 Color: 1
Size: 1562 Color: 1
Size: 1528 Color: 9
Size: 1442 Color: 4
Size: 1428 Color: 12
Size: 1352 Color: 12
Size: 800 Color: 19

Bin 166: 39 of cap free
Amount of items: 2
Items: 
Size: 12359 Color: 6
Size: 3906 Color: 9

Bin 167: 39 of cap free
Amount of items: 2
Items: 
Size: 13854 Color: 16
Size: 2411 Color: 4

Bin 168: 40 of cap free
Amount of items: 2
Items: 
Size: 10692 Color: 19
Size: 5572 Color: 13

Bin 169: 40 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 2
Size: 2196 Color: 9

Bin 170: 40 of cap free
Amount of items: 2
Items: 
Size: 14490 Color: 13
Size: 1774 Color: 3

Bin 171: 41 of cap free
Amount of items: 2
Items: 
Size: 13467 Color: 17
Size: 2796 Color: 15

Bin 172: 42 of cap free
Amount of items: 2
Items: 
Size: 14220 Color: 4
Size: 2042 Color: 11

Bin 173: 45 of cap free
Amount of items: 2
Items: 
Size: 13672 Color: 6
Size: 2587 Color: 19

Bin 174: 46 of cap free
Amount of items: 2
Items: 
Size: 11500 Color: 11
Size: 4758 Color: 9

Bin 175: 48 of cap free
Amount of items: 2
Items: 
Size: 11352 Color: 2
Size: 4904 Color: 15

Bin 176: 50 of cap free
Amount of items: 2
Items: 
Size: 13742 Color: 10
Size: 2512 Color: 16

Bin 177: 50 of cap free
Amount of items: 2
Items: 
Size: 14378 Color: 11
Size: 1876 Color: 17

Bin 178: 58 of cap free
Amount of items: 2
Items: 
Size: 14314 Color: 1
Size: 1932 Color: 12

Bin 179: 61 of cap free
Amount of items: 2
Items: 
Size: 12476 Color: 8
Size: 3767 Color: 9

Bin 180: 64 of cap free
Amount of items: 2
Items: 
Size: 10088 Color: 8
Size: 6152 Color: 0

Bin 181: 72 of cap free
Amount of items: 2
Items: 
Size: 13279 Color: 12
Size: 2953 Color: 8

Bin 182: 73 of cap free
Amount of items: 2
Items: 
Size: 12227 Color: 5
Size: 4004 Color: 9

Bin 183: 75 of cap free
Amount of items: 2
Items: 
Size: 12781 Color: 14
Size: 3448 Color: 10

Bin 184: 84 of cap free
Amount of items: 2
Items: 
Size: 8172 Color: 19
Size: 8048 Color: 17

Bin 185: 92 of cap free
Amount of items: 2
Items: 
Size: 10508 Color: 10
Size: 5704 Color: 12

Bin 186: 92 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 14
Size: 2780 Color: 8

Bin 187: 107 of cap free
Amount of items: 3
Items: 
Size: 10687 Color: 6
Size: 4754 Color: 3
Size: 756 Color: 1

Bin 188: 110 of cap free
Amount of items: 21
Items: 
Size: 960 Color: 14
Size: 960 Color: 10
Size: 922 Color: 15
Size: 920 Color: 13
Size: 896 Color: 15
Size: 856 Color: 10
Size: 856 Color: 4
Size: 856 Color: 2
Size: 820 Color: 5
Size: 816 Color: 2
Size: 756 Color: 14
Size: 736 Color: 18
Size: 712 Color: 2
Size: 692 Color: 19
Size: 682 Color: 5
Size: 678 Color: 0
Size: 672 Color: 17
Size: 672 Color: 10
Size: 664 Color: 5
Size: 564 Color: 3
Size: 504 Color: 3

Bin 189: 116 of cap free
Amount of items: 2
Items: 
Size: 10488 Color: 19
Size: 5700 Color: 18

Bin 190: 119 of cap free
Amount of items: 2
Items: 
Size: 11329 Color: 8
Size: 4856 Color: 3

Bin 191: 153 of cap free
Amount of items: 2
Items: 
Size: 10011 Color: 17
Size: 6140 Color: 10

Bin 192: 158 of cap free
Amount of items: 3
Items: 
Size: 8188 Color: 17
Size: 7452 Color: 16
Size: 506 Color: 2

Bin 193: 159 of cap free
Amount of items: 3
Items: 
Size: 8181 Color: 3
Size: 6780 Color: 12
Size: 1184 Color: 15

Bin 194: 166 of cap free
Amount of items: 5
Items: 
Size: 8157 Color: 19
Size: 2399 Color: 16
Size: 2362 Color: 15
Size: 1862 Color: 15
Size: 1358 Color: 14

Bin 195: 170 of cap free
Amount of items: 3
Items: 
Size: 8636 Color: 0
Size: 6794 Color: 16
Size: 704 Color: 15

Bin 196: 191 of cap free
Amount of items: 3
Items: 
Size: 11158 Color: 8
Size: 4147 Color: 0
Size: 808 Color: 1

Bin 197: 205 of cap free
Amount of items: 3
Items: 
Size: 8170 Color: 19
Size: 6793 Color: 16
Size: 1136 Color: 14

Bin 198: 222 of cap free
Amount of items: 2
Items: 
Size: 9286 Color: 3
Size: 6796 Color: 6

Bin 199: 12300 of cap free
Amount of items: 13
Items: 
Size: 408 Color: 8
Size: 400 Color: 14
Size: 320 Color: 16
Size: 320 Color: 9
Size: 320 Color: 6
Size: 288 Color: 19
Size: 288 Color: 17
Size: 288 Color: 0
Size: 284 Color: 9
Size: 272 Color: 18
Size: 272 Color: 12
Size: 272 Color: 12
Size: 272 Color: 3

Total size: 3228192
Total free space: 16304

