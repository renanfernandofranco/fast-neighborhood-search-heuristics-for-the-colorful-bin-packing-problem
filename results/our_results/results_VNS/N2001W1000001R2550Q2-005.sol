Capicity Bin: 1000001
Lower Bound: 667

Bins used: 668
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 456816 Color: 1
Size: 287214 Color: 1
Size: 255971 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 495127 Color: 0
Size: 253819 Color: 1
Size: 251055 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 436505 Color: 1
Size: 295698 Color: 0
Size: 267798 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 484542 Color: 0
Size: 264496 Color: 0
Size: 250963 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 473216 Color: 1
Size: 268811 Color: 1
Size: 257974 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 484714 Color: 1
Size: 262492 Color: 1
Size: 252795 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 494914 Color: 1
Size: 254942 Color: 0
Size: 250145 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 445176 Color: 0
Size: 287897 Color: 0
Size: 266928 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 377077 Color: 0
Size: 370172 Color: 0
Size: 252752 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 438336 Color: 1
Size: 280952 Color: 1
Size: 280713 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 486161 Color: 0
Size: 258354 Color: 1
Size: 255486 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 399969 Color: 1
Size: 312839 Color: 0
Size: 287193 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 390920 Color: 1
Size: 316259 Color: 0
Size: 292822 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 374419 Color: 1
Size: 328972 Color: 1
Size: 296610 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 420777 Color: 0
Size: 291131 Color: 1
Size: 288093 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 386776 Color: 0
Size: 355764 Color: 1
Size: 257461 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 462419 Color: 0
Size: 270581 Color: 0
Size: 267001 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 354069 Color: 1
Size: 327913 Color: 1
Size: 318019 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 390436 Color: 0
Size: 357731 Color: 0
Size: 251834 Color: 1

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 393497 Color: 1
Size: 306302 Color: 0
Size: 300202 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 489577 Color: 0
Size: 258716 Color: 1
Size: 251708 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 452047 Color: 0
Size: 278440 Color: 1
Size: 269514 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 447802 Color: 0
Size: 286011 Color: 0
Size: 266188 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 473682 Color: 0
Size: 265823 Color: 1
Size: 260496 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 380543 Color: 0
Size: 349076 Color: 1
Size: 270382 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 464350 Color: 1
Size: 272723 Color: 1
Size: 262928 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 466173 Color: 1
Size: 271594 Color: 0
Size: 262234 Color: 1

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 460859 Color: 1
Size: 285595 Color: 0
Size: 253547 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 455677 Color: 1
Size: 277858 Color: 0
Size: 266466 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 377703 Color: 1
Size: 314557 Color: 1
Size: 307741 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 428031 Color: 1
Size: 309826 Color: 0
Size: 262144 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 486732 Color: 1
Size: 258823 Color: 1
Size: 254446 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 480231 Color: 1
Size: 263960 Color: 0
Size: 255810 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 380722 Color: 0
Size: 354665 Color: 1
Size: 264614 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 463795 Color: 0
Size: 283758 Color: 0
Size: 252448 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 470028 Color: 0
Size: 269619 Color: 1
Size: 260354 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 425166 Color: 0
Size: 305409 Color: 1
Size: 269426 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 406223 Color: 1
Size: 321703 Color: 1
Size: 272075 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 388288 Color: 1
Size: 340497 Color: 0
Size: 271216 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 440089 Color: 0
Size: 287515 Color: 0
Size: 272397 Color: 1

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 449042 Color: 1
Size: 299543 Color: 0
Size: 251416 Color: 1

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 428121 Color: 0
Size: 298642 Color: 1
Size: 273238 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 459172 Color: 0
Size: 282658 Color: 0
Size: 258171 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 489240 Color: 0
Size: 258216 Color: 0
Size: 252545 Color: 1

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 496975 Color: 0
Size: 251603 Color: 1
Size: 251423 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 475434 Color: 1
Size: 270518 Color: 0
Size: 254049 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 478257 Color: 0
Size: 271293 Color: 1
Size: 250451 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 490815 Color: 0
Size: 254676 Color: 0
Size: 254510 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 428413 Color: 0
Size: 317682 Color: 1
Size: 253906 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 379636 Color: 0
Size: 355055 Color: 0
Size: 265310 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 457640 Color: 1
Size: 290480 Color: 0
Size: 251881 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 337087 Color: 0
Size: 334797 Color: 1
Size: 328117 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 446824 Color: 0
Size: 280839 Color: 1
Size: 272338 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 418899 Color: 1
Size: 327836 Color: 1
Size: 253266 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 396498 Color: 1
Size: 348282 Color: 1
Size: 255221 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 435568 Color: 1
Size: 291013 Color: 0
Size: 273420 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 403710 Color: 0
Size: 335914 Color: 0
Size: 260377 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 468482 Color: 1
Size: 280486 Color: 0
Size: 251033 Color: 1

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 435331 Color: 1
Size: 296866 Color: 0
Size: 267804 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 391194 Color: 0
Size: 335597 Color: 1
Size: 273210 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 490406 Color: 1
Size: 257199 Color: 0
Size: 252396 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 487258 Color: 1
Size: 256701 Color: 0
Size: 256042 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 445966 Color: 0
Size: 297799 Color: 0
Size: 256236 Color: 1

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 458718 Color: 0
Size: 284997 Color: 1
Size: 256286 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 447790 Color: 0
Size: 296573 Color: 1
Size: 255638 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 406149 Color: 0
Size: 338464 Color: 0
Size: 255388 Color: 1

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 414026 Color: 0
Size: 326470 Color: 0
Size: 259505 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 410733 Color: 0
Size: 322544 Color: 0
Size: 266724 Color: 1

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 397413 Color: 0
Size: 350233 Color: 1
Size: 252355 Color: 1

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 445413 Color: 0
Size: 286546 Color: 0
Size: 268042 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 480361 Color: 1
Size: 269319 Color: 0
Size: 250321 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 441559 Color: 0
Size: 286692 Color: 0
Size: 271750 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 405170 Color: 1
Size: 303236 Color: 1
Size: 291595 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 478095 Color: 0
Size: 268437 Color: 0
Size: 253469 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 407580 Color: 1
Size: 322229 Color: 1
Size: 270192 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 488262 Color: 0
Size: 260029 Color: 1
Size: 251710 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 412817 Color: 1
Size: 310080 Color: 0
Size: 277104 Color: 1

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 464409 Color: 1
Size: 273373 Color: 0
Size: 262219 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 379101 Color: 1
Size: 311760 Color: 1
Size: 309140 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 376668 Color: 0
Size: 314972 Color: 1
Size: 308361 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 469605 Color: 0
Size: 274518 Color: 1
Size: 255878 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 467795 Color: 1
Size: 281519 Color: 0
Size: 250687 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 432009 Color: 1
Size: 289853 Color: 1
Size: 278139 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 441970 Color: 0
Size: 299950 Color: 1
Size: 258081 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 488553 Color: 0
Size: 261156 Color: 0
Size: 250292 Color: 1

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 491733 Color: 1
Size: 255795 Color: 0
Size: 252473 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 457318 Color: 0
Size: 284666 Color: 0
Size: 258017 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 436129 Color: 1
Size: 297659 Color: 0
Size: 266213 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 436364 Color: 1
Size: 285619 Color: 0
Size: 278018 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 430754 Color: 0
Size: 311428 Color: 1
Size: 257819 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 413877 Color: 0
Size: 293470 Color: 1
Size: 292654 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 437781 Color: 0
Size: 293873 Color: 1
Size: 268347 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 446919 Color: 0
Size: 289109 Color: 0
Size: 263973 Color: 1

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 452115 Color: 1
Size: 282992 Color: 0
Size: 264894 Color: 1

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 417854 Color: 0
Size: 299969 Color: 0
Size: 282178 Color: 1

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 481186 Color: 0
Size: 262772 Color: 1
Size: 256043 Color: 1

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 497054 Color: 1
Size: 252395 Color: 0
Size: 250552 Color: 1

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 488607 Color: 1
Size: 257475 Color: 0
Size: 253919 Color: 1

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 441968 Color: 0
Size: 296018 Color: 1
Size: 262015 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 493119 Color: 1
Size: 255469 Color: 0
Size: 251413 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 461236 Color: 1
Size: 281224 Color: 1
Size: 257541 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 493025 Color: 1
Size: 255220 Color: 0
Size: 251756 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 403482 Color: 0
Size: 337447 Color: 1
Size: 259072 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 349034 Color: 0
Size: 342049 Color: 1
Size: 308918 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 364723 Color: 1
Size: 359364 Color: 0
Size: 275914 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 459766 Color: 1
Size: 288382 Color: 0
Size: 251853 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 386945 Color: 1
Size: 353621 Color: 0
Size: 259435 Color: 1

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 422401 Color: 0
Size: 309692 Color: 1
Size: 267908 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 427818 Color: 1
Size: 302213 Color: 0
Size: 269970 Color: 1

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 388981 Color: 1
Size: 344185 Color: 0
Size: 266835 Color: 1

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 398979 Color: 0
Size: 350426 Color: 1
Size: 250596 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 448808 Color: 0
Size: 281237 Color: 1
Size: 269956 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 479401 Color: 1
Size: 260485 Color: 0
Size: 260115 Color: 1

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 485581 Color: 1
Size: 262715 Color: 0
Size: 251705 Color: 1

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 395794 Color: 0
Size: 336340 Color: 1
Size: 267867 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 362403 Color: 0
Size: 355980 Color: 1
Size: 281618 Color: 1

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 416208 Color: 1
Size: 292782 Color: 1
Size: 291011 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 381240 Color: 1
Size: 334485 Color: 1
Size: 284276 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 391705 Color: 0
Size: 323878 Color: 1
Size: 284418 Color: 1

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 465538 Color: 1
Size: 271297 Color: 1
Size: 263166 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 425969 Color: 1
Size: 320772 Color: 0
Size: 253260 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 362786 Color: 1
Size: 330496 Color: 0
Size: 306719 Color: 1

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 480970 Color: 1
Size: 265634 Color: 0
Size: 253397 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 365743 Color: 0
Size: 365622 Color: 1
Size: 268636 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 367951 Color: 0
Size: 342077 Color: 0
Size: 289973 Color: 1

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 370449 Color: 1
Size: 366534 Color: 0
Size: 263018 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 376205 Color: 0
Size: 344751 Color: 1
Size: 279045 Color: 1

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 364739 Color: 0
Size: 319645 Color: 1
Size: 315617 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 397442 Color: 0
Size: 303557 Color: 0
Size: 299002 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 471607 Color: 0
Size: 273167 Color: 1
Size: 255227 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 358966 Color: 0
Size: 341796 Color: 0
Size: 299239 Color: 1

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 499169 Color: 0
Size: 250734 Color: 1
Size: 250098 Color: 1

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 492022 Color: 1
Size: 256009 Color: 0
Size: 251970 Color: 1

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 499133 Color: 0
Size: 250559 Color: 1
Size: 250309 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 497433 Color: 0
Size: 251380 Color: 0
Size: 251188 Color: 1

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 366256 Color: 1
Size: 341818 Color: 0
Size: 291927 Color: 0

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 357483 Color: 1
Size: 323051 Color: 0
Size: 319467 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 349064 Color: 0
Size: 339859 Color: 0
Size: 311078 Color: 1

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 352776 Color: 1
Size: 348327 Color: 1
Size: 298898 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 473051 Color: 0
Size: 268651 Color: 1
Size: 258299 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 362792 Color: 1
Size: 345949 Color: 0
Size: 291260 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 363687 Color: 0
Size: 336332 Color: 1
Size: 299982 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 365230 Color: 0
Size: 321508 Color: 1
Size: 313263 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 369813 Color: 1
Size: 346281 Color: 1
Size: 283907 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 368769 Color: 0
Size: 328064 Color: 1
Size: 303168 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 370145 Color: 1
Size: 319017 Color: 1
Size: 310839 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 371136 Color: 1
Size: 316629 Color: 1
Size: 312236 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 372842 Color: 1
Size: 360220 Color: 0
Size: 266939 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 373422 Color: 0
Size: 349177 Color: 1
Size: 277402 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 374415 Color: 1
Size: 373298 Color: 0
Size: 252288 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 373605 Color: 0
Size: 335344 Color: 0
Size: 291052 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 375367 Color: 0
Size: 366897 Color: 1
Size: 257737 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 374974 Color: 1
Size: 317720 Color: 1
Size: 307307 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 375809 Color: 1
Size: 350380 Color: 0
Size: 273812 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 376232 Color: 1
Size: 332592 Color: 0
Size: 291177 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 376661 Color: 0
Size: 337646 Color: 0
Size: 285694 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 376687 Color: 1
Size: 342966 Color: 1
Size: 280348 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 377829 Color: 0
Size: 311293 Color: 0
Size: 310879 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 379337 Color: 0
Size: 316011 Color: 1
Size: 304653 Color: 0

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 380171 Color: 0
Size: 336971 Color: 1
Size: 282859 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 380562 Color: 0
Size: 343177 Color: 0
Size: 276262 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 381704 Color: 0
Size: 360538 Color: 1
Size: 257759 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 382830 Color: 1
Size: 330875 Color: 0
Size: 286296 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 383751 Color: 0
Size: 319257 Color: 0
Size: 296993 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 384779 Color: 1
Size: 360913 Color: 0
Size: 254309 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 386217 Color: 1
Size: 360115 Color: 0
Size: 253669 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 388069 Color: 1
Size: 342249 Color: 0
Size: 269683 Color: 1

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 388720 Color: 1
Size: 323899 Color: 1
Size: 287382 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 388499 Color: 0
Size: 323959 Color: 0
Size: 287543 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 388885 Color: 1
Size: 313873 Color: 1
Size: 297243 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 389062 Color: 0
Size: 305700 Color: 1
Size: 305239 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 389118 Color: 0
Size: 354809 Color: 1
Size: 256074 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 392496 Color: 0
Size: 326862 Color: 0
Size: 280643 Color: 1

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 392858 Color: 1
Size: 341277 Color: 1
Size: 265866 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 392789 Color: 0
Size: 305840 Color: 1
Size: 301372 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 392869 Color: 0
Size: 330575 Color: 1
Size: 276557 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 393092 Color: 1
Size: 349655 Color: 0
Size: 257254 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 395132 Color: 1
Size: 325962 Color: 1
Size: 278907 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 395317 Color: 1
Size: 344745 Color: 1
Size: 259939 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 395599 Color: 1
Size: 307623 Color: 0
Size: 296779 Color: 1

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 396007 Color: 0
Size: 312246 Color: 1
Size: 291748 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 396764 Color: 0
Size: 304177 Color: 1
Size: 299060 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 396834 Color: 1
Size: 334050 Color: 0
Size: 269117 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 399340 Color: 1
Size: 314132 Color: 0
Size: 286529 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 399053 Color: 0
Size: 346954 Color: 1
Size: 253994 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 399506 Color: 1
Size: 334302 Color: 0
Size: 266193 Color: 1

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 399242 Color: 0
Size: 337523 Color: 0
Size: 263236 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 399650 Color: 1
Size: 339369 Color: 0
Size: 260982 Color: 1

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 399445 Color: 0
Size: 348970 Color: 0
Size: 251586 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 400675 Color: 1
Size: 313525 Color: 1
Size: 285801 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 400286 Color: 0
Size: 331140 Color: 1
Size: 268575 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 402037 Color: 1
Size: 347950 Color: 1
Size: 250014 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 403161 Color: 1
Size: 329710 Color: 0
Size: 267130 Color: 1

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 404985 Color: 1
Size: 320526 Color: 1
Size: 274490 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 404533 Color: 0
Size: 337872 Color: 0
Size: 257596 Color: 1

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 404935 Color: 0
Size: 301853 Color: 0
Size: 293213 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 405756 Color: 0
Size: 313430 Color: 0
Size: 280815 Color: 1

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 405520 Color: 1
Size: 308787 Color: 1
Size: 285694 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 406118 Color: 0
Size: 323335 Color: 0
Size: 270548 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 406827 Color: 1
Size: 336664 Color: 1
Size: 256510 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 408110 Color: 0
Size: 338382 Color: 1
Size: 253509 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 408348 Color: 0
Size: 308574 Color: 1
Size: 283079 Color: 0

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 408933 Color: 1
Size: 325699 Color: 0
Size: 265369 Color: 1

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 408940 Color: 1
Size: 307368 Color: 0
Size: 283693 Color: 1

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 409202 Color: 0
Size: 318308 Color: 0
Size: 272491 Color: 1

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 409530 Color: 0
Size: 334602 Color: 0
Size: 255869 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 409899 Color: 1
Size: 298014 Color: 1
Size: 292088 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 410505 Color: 0
Size: 319308 Color: 1
Size: 270188 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 410570 Color: 1
Size: 313985 Color: 1
Size: 275446 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 411344 Color: 0
Size: 302148 Color: 0
Size: 286509 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 411582 Color: 0
Size: 319031 Color: 1
Size: 269388 Color: 1

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 411973 Color: 0
Size: 315062 Color: 1
Size: 272966 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 412463 Color: 0
Size: 303544 Color: 0
Size: 283994 Color: 1

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 413308 Color: 0
Size: 330133 Color: 0
Size: 256560 Color: 1

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 413409 Color: 0
Size: 331256 Color: 1
Size: 255336 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 413603 Color: 1
Size: 317527 Color: 1
Size: 268871 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 415196 Color: 1
Size: 298879 Color: 1
Size: 285926 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 416561 Color: 1
Size: 323291 Color: 0
Size: 260149 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 416867 Color: 1
Size: 315162 Color: 1
Size: 267972 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 416529 Color: 0
Size: 326089 Color: 0
Size: 257383 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 417783 Color: 1
Size: 314348 Color: 1
Size: 267870 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 419038 Color: 1
Size: 291634 Color: 0
Size: 289329 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 420212 Color: 0
Size: 325093 Color: 0
Size: 254696 Color: 1

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 422028 Color: 1
Size: 314127 Color: 0
Size: 263846 Color: 1

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 422160 Color: 0
Size: 314116 Color: 1
Size: 263725 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 423515 Color: 1
Size: 306696 Color: 0
Size: 269790 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 423550 Color: 1
Size: 296805 Color: 0
Size: 279646 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 423218 Color: 0
Size: 289323 Color: 1
Size: 287460 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 424388 Color: 0
Size: 312189 Color: 0
Size: 263424 Color: 1

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 424820 Color: 1
Size: 306611 Color: 1
Size: 268570 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 425943 Color: 0
Size: 318495 Color: 0
Size: 255563 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 426789 Color: 1
Size: 318989 Color: 0
Size: 254223 Color: 0

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 426992 Color: 1
Size: 320699 Color: 1
Size: 252310 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 427038 Color: 0
Size: 296304 Color: 1
Size: 276659 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 428043 Color: 1
Size: 297538 Color: 1
Size: 274420 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 428823 Color: 0
Size: 295036 Color: 1
Size: 276142 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 429401 Color: 0
Size: 318078 Color: 0
Size: 252522 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 429711 Color: 0
Size: 285234 Color: 0
Size: 285056 Color: 1

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 430419 Color: 0
Size: 318458 Color: 1
Size: 251124 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 431628 Color: 1
Size: 290656 Color: 0
Size: 277717 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 431582 Color: 0
Size: 300805 Color: 0
Size: 267614 Color: 1

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 432019 Color: 1
Size: 293982 Color: 1
Size: 274000 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 431897 Color: 0
Size: 316554 Color: 0
Size: 251550 Color: 1

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 432146 Color: 1
Size: 296867 Color: 1
Size: 270988 Color: 0

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 433875 Color: 1
Size: 299071 Color: 1
Size: 267055 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 434437 Color: 0
Size: 305493 Color: 1
Size: 260071 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 434449 Color: 1
Size: 291238 Color: 1
Size: 274314 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 435130 Color: 1
Size: 303610 Color: 0
Size: 261261 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 435231 Color: 1
Size: 308517 Color: 0
Size: 256253 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 436460 Color: 1
Size: 284958 Color: 0
Size: 278583 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 436908 Color: 1
Size: 312775 Color: 0
Size: 250318 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 437110 Color: 1
Size: 291048 Color: 1
Size: 271843 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 437162 Color: 0
Size: 300934 Color: 0
Size: 261905 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 437369 Color: 0
Size: 285129 Color: 0
Size: 277503 Color: 1

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 437995 Color: 1
Size: 303946 Color: 1
Size: 258060 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 438229 Color: 1
Size: 291089 Color: 0
Size: 270683 Color: 1

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 438182 Color: 0
Size: 289636 Color: 0
Size: 272183 Color: 1

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 438449 Color: 0
Size: 296288 Color: 0
Size: 265264 Color: 1

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 438419 Color: 1
Size: 293861 Color: 1
Size: 267721 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 439065 Color: 0
Size: 309757 Color: 0
Size: 251179 Color: 1

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 438454 Color: 1
Size: 297678 Color: 1
Size: 263869 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 439193 Color: 0
Size: 299025 Color: 1
Size: 261783 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 440041 Color: 1
Size: 280659 Color: 0
Size: 279301 Color: 1

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 440622 Color: 0
Size: 290852 Color: 1
Size: 268527 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 443442 Color: 0
Size: 282005 Color: 1
Size: 274554 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 443239 Color: 1
Size: 279432 Color: 1
Size: 277330 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 444025 Color: 0
Size: 283377 Color: 0
Size: 272599 Color: 1

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 444276 Color: 1
Size: 282603 Color: 0
Size: 273122 Color: 1

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 446296 Color: 1
Size: 288902 Color: 0
Size: 264803 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 447761 Color: 1
Size: 294922 Color: 0
Size: 257318 Color: 1

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 447908 Color: 1
Size: 279515 Color: 1
Size: 272578 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 448566 Color: 0
Size: 291640 Color: 0
Size: 259795 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 448773 Color: 1
Size: 289394 Color: 0
Size: 261834 Color: 1

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 450746 Color: 0
Size: 290950 Color: 0
Size: 258305 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 451336 Color: 1
Size: 291554 Color: 1
Size: 257111 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 450861 Color: 0
Size: 296429 Color: 0
Size: 252711 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 452023 Color: 0
Size: 294144 Color: 1
Size: 253834 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 452145 Color: 0
Size: 289070 Color: 1
Size: 258786 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 454861 Color: 1
Size: 278524 Color: 0
Size: 266616 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 455826 Color: 1
Size: 276856 Color: 0
Size: 267319 Color: 1

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 456329 Color: 0
Size: 287374 Color: 0
Size: 256298 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 458250 Color: 1
Size: 284005 Color: 0
Size: 257746 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 458500 Color: 1
Size: 278130 Color: 1
Size: 263371 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 458403 Color: 0
Size: 283780 Color: 0
Size: 257818 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 458700 Color: 1
Size: 284103 Color: 1
Size: 257198 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 459415 Color: 1
Size: 274160 Color: 1
Size: 266426 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 459486 Color: 0
Size: 284379 Color: 1
Size: 256136 Color: 0

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 460388 Color: 0
Size: 283964 Color: 0
Size: 255649 Color: 1

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 460395 Color: 1
Size: 280500 Color: 0
Size: 259106 Color: 1

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 460903 Color: 1
Size: 274362 Color: 0
Size: 264736 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 462279 Color: 0
Size: 281037 Color: 1
Size: 256685 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 462007 Color: 1
Size: 283631 Color: 0
Size: 254363 Color: 1

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 462531 Color: 1
Size: 287071 Color: 0
Size: 250399 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 463516 Color: 0
Size: 272290 Color: 1
Size: 264195 Color: 1

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 464832 Color: 0
Size: 283982 Color: 0
Size: 251187 Color: 1

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 466107 Color: 0
Size: 274798 Color: 1
Size: 259096 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 466637 Color: 0
Size: 282934 Color: 1
Size: 250430 Color: 1

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 468911 Color: 0
Size: 276332 Color: 1
Size: 254758 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 470302 Color: 1
Size: 266659 Color: 1
Size: 263040 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 470704 Color: 1
Size: 276482 Color: 1
Size: 252815 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 471716 Color: 1
Size: 271567 Color: 1
Size: 256718 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 472368 Color: 1
Size: 267337 Color: 0
Size: 260296 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 473632 Color: 0
Size: 270910 Color: 1
Size: 255459 Color: 1

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 473634 Color: 0
Size: 272532 Color: 1
Size: 253835 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 475008 Color: 1
Size: 267575 Color: 0
Size: 257418 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 475518 Color: 1
Size: 271576 Color: 0
Size: 252907 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 475756 Color: 1
Size: 265911 Color: 0
Size: 258334 Color: 1

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 476085 Color: 0
Size: 273377 Color: 1
Size: 250539 Color: 0

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 476676 Color: 1
Size: 261865 Color: 0
Size: 261460 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 477560 Color: 1
Size: 271881 Color: 0
Size: 250560 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 478352 Color: 0
Size: 266263 Color: 1
Size: 255386 Color: 1

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 478835 Color: 0
Size: 263878 Color: 0
Size: 257288 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 479492 Color: 0
Size: 267045 Color: 1
Size: 253464 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 479888 Color: 0
Size: 264024 Color: 1
Size: 256089 Color: 1

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 480530 Color: 1
Size: 263719 Color: 0
Size: 255752 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 480174 Color: 0
Size: 268774 Color: 1
Size: 251053 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 480940 Color: 0
Size: 268975 Color: 1
Size: 250086 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 482449 Color: 1
Size: 265864 Color: 0
Size: 251688 Color: 1

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 482985 Color: 0
Size: 262306 Color: 0
Size: 254710 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 483596 Color: 1
Size: 260758 Color: 0
Size: 255647 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 483801 Color: 0
Size: 261414 Color: 1
Size: 254786 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 484706 Color: 1
Size: 264171 Color: 1
Size: 251124 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 485189 Color: 1
Size: 258090 Color: 0
Size: 256722 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 486285 Color: 0
Size: 258987 Color: 0
Size: 254729 Color: 1

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 486575 Color: 0
Size: 259254 Color: 1
Size: 254172 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 487710 Color: 1
Size: 256371 Color: 0
Size: 255920 Color: 1

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 488170 Color: 0
Size: 259585 Color: 1
Size: 252246 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 488335 Color: 1
Size: 258648 Color: 0
Size: 253018 Color: 1

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 488447 Color: 1
Size: 258231 Color: 1
Size: 253323 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 489044 Color: 1
Size: 258344 Color: 1
Size: 252613 Color: 0

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 490011 Color: 0
Size: 255449 Color: 1
Size: 254541 Color: 1

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 490142 Color: 0
Size: 258635 Color: 0
Size: 251224 Color: 1

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 490460 Color: 1
Size: 257268 Color: 1
Size: 252273 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 490830 Color: 1
Size: 254797 Color: 1
Size: 254374 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 491091 Color: 1
Size: 258845 Color: 0
Size: 250065 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 492112 Color: 1
Size: 254551 Color: 0
Size: 253338 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 492415 Color: 1
Size: 257297 Color: 0
Size: 250289 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 493989 Color: 0
Size: 255642 Color: 1
Size: 250370 Color: 1

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 494599 Color: 1
Size: 253804 Color: 0
Size: 251598 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 405486 Color: 1
Size: 342013 Color: 0
Size: 252502 Color: 1

Bin 341: 1 of cap free
Amount of items: 3
Items: 
Size: 387142 Color: 0
Size: 361339 Color: 1
Size: 251519 Color: 0

Bin 342: 1 of cap free
Amount of items: 3
Items: 
Size: 390012 Color: 0
Size: 334066 Color: 1
Size: 275922 Color: 1

Bin 343: 1 of cap free
Amount of items: 3
Items: 
Size: 401142 Color: 1
Size: 323579 Color: 0
Size: 275279 Color: 1

Bin 344: 1 of cap free
Amount of items: 3
Items: 
Size: 485956 Color: 1
Size: 257609 Color: 1
Size: 256435 Color: 0

Bin 345: 1 of cap free
Amount of items: 3
Items: 
Size: 410663 Color: 1
Size: 303748 Color: 0
Size: 285589 Color: 1

Bin 346: 1 of cap free
Amount of items: 3
Items: 
Size: 447915 Color: 0
Size: 280656 Color: 1
Size: 271429 Color: 0

Bin 347: 1 of cap free
Amount of items: 3
Items: 
Size: 360909 Color: 0
Size: 334747 Color: 0
Size: 304344 Color: 1

Bin 348: 1 of cap free
Amount of items: 3
Items: 
Size: 402894 Color: 0
Size: 343442 Color: 0
Size: 253664 Color: 1

Bin 349: 1 of cap free
Amount of items: 3
Items: 
Size: 463978 Color: 0
Size: 283576 Color: 0
Size: 252446 Color: 1

Bin 350: 1 of cap free
Amount of items: 3
Items: 
Size: 449126 Color: 1
Size: 297739 Color: 0
Size: 253135 Color: 0

Bin 351: 1 of cap free
Amount of items: 3
Items: 
Size: 360660 Color: 0
Size: 331285 Color: 0
Size: 308055 Color: 1

Bin 352: 1 of cap free
Amount of items: 3
Items: 
Size: 397026 Color: 1
Size: 342669 Color: 0
Size: 260305 Color: 1

Bin 353: 1 of cap free
Amount of items: 3
Items: 
Size: 450028 Color: 1
Size: 277074 Color: 0
Size: 272898 Color: 1

Bin 354: 1 of cap free
Amount of items: 3
Items: 
Size: 448821 Color: 1
Size: 293534 Color: 0
Size: 257645 Color: 1

Bin 355: 1 of cap free
Amount of items: 3
Items: 
Size: 490793 Color: 1
Size: 259079 Color: 0
Size: 250128 Color: 0

Bin 356: 1 of cap free
Amount of items: 3
Items: 
Size: 408872 Color: 1
Size: 297500 Color: 0
Size: 293628 Color: 1

Bin 357: 1 of cap free
Amount of items: 3
Items: 
Size: 491383 Color: 1
Size: 257583 Color: 0
Size: 251034 Color: 1

Bin 358: 1 of cap free
Amount of items: 3
Items: 
Size: 380002 Color: 1
Size: 355361 Color: 0
Size: 264637 Color: 1

Bin 359: 1 of cap free
Amount of items: 3
Items: 
Size: 399090 Color: 1
Size: 302275 Color: 1
Size: 298635 Color: 0

Bin 360: 1 of cap free
Amount of items: 3
Items: 
Size: 409128 Color: 0
Size: 301249 Color: 0
Size: 289623 Color: 1

Bin 361: 1 of cap free
Amount of items: 3
Items: 
Size: 448134 Color: 0
Size: 288124 Color: 1
Size: 263742 Color: 0

Bin 362: 1 of cap free
Amount of items: 3
Items: 
Size: 466715 Color: 1
Size: 278155 Color: 0
Size: 255130 Color: 1

Bin 363: 1 of cap free
Amount of items: 3
Items: 
Size: 383802 Color: 1
Size: 359817 Color: 0
Size: 256381 Color: 0

Bin 364: 1 of cap free
Amount of items: 3
Items: 
Size: 447853 Color: 1
Size: 293686 Color: 0
Size: 258461 Color: 0

Bin 365: 1 of cap free
Amount of items: 3
Items: 
Size: 368539 Color: 0
Size: 334222 Color: 1
Size: 297239 Color: 0

Bin 366: 1 of cap free
Amount of items: 3
Items: 
Size: 476135 Color: 0
Size: 268148 Color: 1
Size: 255717 Color: 1

Bin 367: 1 of cap free
Amount of items: 3
Items: 
Size: 381266 Color: 1
Size: 356710 Color: 0
Size: 262024 Color: 0

Bin 368: 1 of cap free
Amount of items: 3
Items: 
Size: 363070 Color: 0
Size: 339320 Color: 1
Size: 297610 Color: 0

Bin 369: 1 of cap free
Amount of items: 3
Items: 
Size: 367459 Color: 0
Size: 326958 Color: 1
Size: 305583 Color: 1

Bin 370: 1 of cap free
Amount of items: 3
Items: 
Size: 368093 Color: 1
Size: 328503 Color: 0
Size: 303404 Color: 1

Bin 371: 1 of cap free
Amount of items: 3
Items: 
Size: 369768 Color: 1
Size: 350967 Color: 0
Size: 279265 Color: 0

Bin 372: 1 of cap free
Amount of items: 3
Items: 
Size: 371524 Color: 0
Size: 361213 Color: 0
Size: 267263 Color: 1

Bin 373: 1 of cap free
Amount of items: 3
Items: 
Size: 381094 Color: 1
Size: 341611 Color: 1
Size: 277295 Color: 0

Bin 374: 1 of cap free
Amount of items: 3
Items: 
Size: 381219 Color: 1
Size: 368385 Color: 1
Size: 250396 Color: 0

Bin 375: 1 of cap free
Amount of items: 3
Items: 
Size: 381173 Color: 0
Size: 320037 Color: 0
Size: 298790 Color: 1

Bin 376: 1 of cap free
Amount of items: 3
Items: 
Size: 381712 Color: 1
Size: 329715 Color: 0
Size: 288573 Color: 1

Bin 377: 1 of cap free
Amount of items: 3
Items: 
Size: 383285 Color: 1
Size: 335580 Color: 0
Size: 281135 Color: 0

Bin 378: 1 of cap free
Amount of items: 3
Items: 
Size: 383201 Color: 0
Size: 335980 Color: 0
Size: 280819 Color: 1

Bin 379: 1 of cap free
Amount of items: 3
Items: 
Size: 385740 Color: 1
Size: 352307 Color: 0
Size: 261953 Color: 0

Bin 380: 1 of cap free
Amount of items: 3
Items: 
Size: 390071 Color: 1
Size: 340731 Color: 0
Size: 269198 Color: 1

Bin 381: 1 of cap free
Amount of items: 3
Items: 
Size: 390748 Color: 0
Size: 308717 Color: 0
Size: 300535 Color: 1

Bin 382: 1 of cap free
Amount of items: 3
Items: 
Size: 396644 Color: 1
Size: 315172 Color: 0
Size: 288184 Color: 1

Bin 383: 1 of cap free
Amount of items: 3
Items: 
Size: 397947 Color: 0
Size: 343114 Color: 1
Size: 258939 Color: 1

Bin 384: 1 of cap free
Amount of items: 3
Items: 
Size: 399289 Color: 1
Size: 349660 Color: 0
Size: 251051 Color: 0

Bin 385: 1 of cap free
Amount of items: 3
Items: 
Size: 400603 Color: 1
Size: 347940 Color: 0
Size: 251457 Color: 0

Bin 386: 1 of cap free
Amount of items: 3
Items: 
Size: 401744 Color: 0
Size: 337370 Color: 0
Size: 260886 Color: 1

Bin 387: 1 of cap free
Amount of items: 3
Items: 
Size: 402849 Color: 1
Size: 344916 Color: 0
Size: 252235 Color: 1

Bin 388: 1 of cap free
Amount of items: 3
Items: 
Size: 406535 Color: 0
Size: 303445 Color: 0
Size: 290020 Color: 1

Bin 389: 1 of cap free
Amount of items: 3
Items: 
Size: 408293 Color: 0
Size: 338284 Color: 1
Size: 253423 Color: 0

Bin 390: 1 of cap free
Amount of items: 3
Items: 
Size: 410730 Color: 0
Size: 316235 Color: 0
Size: 273035 Color: 1

Bin 391: 1 of cap free
Amount of items: 3
Items: 
Size: 414888 Color: 1
Size: 332231 Color: 0
Size: 252881 Color: 1

Bin 392: 1 of cap free
Amount of items: 3
Items: 
Size: 416021 Color: 1
Size: 329171 Color: 0
Size: 254808 Color: 1

Bin 393: 1 of cap free
Amount of items: 3
Items: 
Size: 416029 Color: 0
Size: 314950 Color: 0
Size: 269021 Color: 1

Bin 394: 1 of cap free
Amount of items: 3
Items: 
Size: 419053 Color: 1
Size: 294029 Color: 0
Size: 286918 Color: 1

Bin 395: 1 of cap free
Amount of items: 3
Items: 
Size: 425892 Color: 1
Size: 319607 Color: 0
Size: 254501 Color: 0

Bin 396: 1 of cap free
Amount of items: 3
Items: 
Size: 426157 Color: 1
Size: 292962 Color: 0
Size: 280881 Color: 0

Bin 397: 1 of cap free
Amount of items: 3
Items: 
Size: 427271 Color: 1
Size: 321074 Color: 1
Size: 251655 Color: 0

Bin 398: 1 of cap free
Amount of items: 3
Items: 
Size: 428102 Color: 0
Size: 315370 Color: 1
Size: 256528 Color: 0

Bin 399: 1 of cap free
Amount of items: 3
Items: 
Size: 431591 Color: 1
Size: 295964 Color: 0
Size: 272445 Color: 1

Bin 400: 1 of cap free
Amount of items: 3
Items: 
Size: 432284 Color: 1
Size: 294993 Color: 0
Size: 272723 Color: 0

Bin 401: 1 of cap free
Amount of items: 3
Items: 
Size: 433670 Color: 1
Size: 287678 Color: 0
Size: 278652 Color: 0

Bin 402: 1 of cap free
Amount of items: 3
Items: 
Size: 433915 Color: 0
Size: 314215 Color: 1
Size: 251870 Color: 0

Bin 403: 1 of cap free
Amount of items: 3
Items: 
Size: 433953 Color: 1
Size: 315658 Color: 0
Size: 250389 Color: 1

Bin 404: 1 of cap free
Amount of items: 3
Items: 
Size: 436434 Color: 1
Size: 296162 Color: 1
Size: 267404 Color: 0

Bin 405: 1 of cap free
Amount of items: 3
Items: 
Size: 439321 Color: 0
Size: 310501 Color: 0
Size: 250178 Color: 1

Bin 406: 1 of cap free
Amount of items: 3
Items: 
Size: 452273 Color: 1
Size: 288724 Color: 0
Size: 259003 Color: 1

Bin 407: 1 of cap free
Amount of items: 3
Items: 
Size: 453463 Color: 1
Size: 274434 Color: 1
Size: 272103 Color: 0

Bin 408: 1 of cap free
Amount of items: 3
Items: 
Size: 454445 Color: 0
Size: 278368 Color: 0
Size: 267187 Color: 1

Bin 409: 1 of cap free
Amount of items: 3
Items: 
Size: 463892 Color: 0
Size: 268926 Color: 1
Size: 267182 Color: 1

Bin 410: 1 of cap free
Amount of items: 3
Items: 
Size: 470606 Color: 0
Size: 270187 Color: 1
Size: 259207 Color: 0

Bin 411: 1 of cap free
Amount of items: 3
Items: 
Size: 472007 Color: 1
Size: 277871 Color: 1
Size: 250122 Color: 0

Bin 412: 1 of cap free
Amount of items: 3
Items: 
Size: 477650 Color: 1
Size: 267683 Color: 0
Size: 254667 Color: 0

Bin 413: 1 of cap free
Amount of items: 3
Items: 
Size: 479427 Color: 0
Size: 260516 Color: 1
Size: 260057 Color: 1

Bin 414: 1 of cap free
Amount of items: 3
Items: 
Size: 479901 Color: 0
Size: 262392 Color: 0
Size: 257707 Color: 1

Bin 415: 1 of cap free
Amount of items: 3
Items: 
Size: 479788 Color: 1
Size: 267746 Color: 0
Size: 252466 Color: 1

Bin 416: 1 of cap free
Amount of items: 3
Items: 
Size: 381469 Color: 1
Size: 362881 Color: 1
Size: 255650 Color: 0

Bin 417: 1 of cap free
Amount of items: 3
Items: 
Size: 495813 Color: 1
Size: 253554 Color: 0
Size: 250633 Color: 1

Bin 418: 1 of cap free
Amount of items: 3
Items: 
Size: 347715 Color: 0
Size: 337207 Color: 0
Size: 315078 Color: 1

Bin 419: 1 of cap free
Amount of items: 3
Items: 
Size: 352577 Color: 0
Size: 350698 Color: 1
Size: 296725 Color: 1

Bin 420: 1 of cap free
Amount of items: 3
Items: 
Size: 381794 Color: 1
Size: 322984 Color: 1
Size: 295222 Color: 0

Bin 421: 1 of cap free
Amount of items: 3
Items: 
Size: 384061 Color: 0
Size: 340560 Color: 1
Size: 275379 Color: 0

Bin 422: 1 of cap free
Amount of items: 3
Items: 
Size: 380974 Color: 1
Size: 345999 Color: 0
Size: 273027 Color: 0

Bin 423: 1 of cap free
Amount of items: 3
Items: 
Size: 354337 Color: 1
Size: 350443 Color: 1
Size: 295220 Color: 0

Bin 424: 1 of cap free
Amount of items: 3
Items: 
Size: 387805 Color: 0
Size: 329261 Color: 1
Size: 282934 Color: 0

Bin 425: 1 of cap free
Amount of items: 3
Items: 
Size: 343507 Color: 1
Size: 330263 Color: 0
Size: 326230 Color: 0

Bin 426: 1 of cap free
Amount of items: 3
Items: 
Size: 341564 Color: 1
Size: 339920 Color: 1
Size: 318516 Color: 0

Bin 427: 1 of cap free
Amount of items: 3
Items: 
Size: 441702 Color: 1
Size: 284417 Color: 0
Size: 273881 Color: 0

Bin 428: 1 of cap free
Amount of items: 3
Items: 
Size: 400854 Color: 1
Size: 328046 Color: 1
Size: 271100 Color: 0

Bin 429: 1 of cap free
Amount of items: 3
Items: 
Size: 349233 Color: 0
Size: 329430 Color: 1
Size: 321337 Color: 0

Bin 430: 2 of cap free
Amount of items: 3
Items: 
Size: 372861 Color: 1
Size: 355020 Color: 1
Size: 272118 Color: 0

Bin 431: 2 of cap free
Amount of items: 3
Items: 
Size: 413388 Color: 0
Size: 332521 Color: 1
Size: 254090 Color: 1

Bin 432: 2 of cap free
Amount of items: 3
Items: 
Size: 396755 Color: 1
Size: 346236 Color: 0
Size: 257008 Color: 1

Bin 433: 2 of cap free
Amount of items: 3
Items: 
Size: 416498 Color: 0
Size: 299905 Color: 0
Size: 283596 Color: 1

Bin 434: 2 of cap free
Amount of items: 3
Items: 
Size: 497611 Color: 0
Size: 252142 Color: 0
Size: 250246 Color: 1

Bin 435: 2 of cap free
Amount of items: 3
Items: 
Size: 455377 Color: 0
Size: 273949 Color: 1
Size: 270673 Color: 1

Bin 436: 2 of cap free
Amount of items: 3
Items: 
Size: 413948 Color: 1
Size: 335480 Color: 1
Size: 250571 Color: 0

Bin 437: 2 of cap free
Amount of items: 3
Items: 
Size: 379811 Color: 0
Size: 356506 Color: 0
Size: 263682 Color: 1

Bin 438: 2 of cap free
Amount of items: 3
Items: 
Size: 413793 Color: 1
Size: 307497 Color: 0
Size: 278709 Color: 1

Bin 439: 2 of cap free
Amount of items: 3
Items: 
Size: 377783 Color: 1
Size: 320620 Color: 1
Size: 301596 Color: 0

Bin 440: 2 of cap free
Amount of items: 3
Items: 
Size: 389859 Color: 1
Size: 319130 Color: 1
Size: 291010 Color: 0

Bin 441: 2 of cap free
Amount of items: 3
Items: 
Size: 372012 Color: 0
Size: 345600 Color: 1
Size: 282387 Color: 1

Bin 442: 2 of cap free
Amount of items: 3
Items: 
Size: 402444 Color: 0
Size: 330236 Color: 0
Size: 267319 Color: 1

Bin 443: 2 of cap free
Amount of items: 3
Items: 
Size: 356197 Color: 0
Size: 340984 Color: 1
Size: 302818 Color: 0

Bin 444: 2 of cap free
Amount of items: 3
Items: 
Size: 419752 Color: 1
Size: 293229 Color: 0
Size: 287018 Color: 1

Bin 445: 2 of cap free
Amount of items: 3
Items: 
Size: 431163 Color: 1
Size: 286918 Color: 0
Size: 281918 Color: 1

Bin 446: 2 of cap free
Amount of items: 3
Items: 
Size: 409471 Color: 0
Size: 334484 Color: 1
Size: 256044 Color: 1

Bin 447: 2 of cap free
Amount of items: 3
Items: 
Size: 355155 Color: 1
Size: 340980 Color: 0
Size: 303864 Color: 1

Bin 448: 2 of cap free
Amount of items: 3
Items: 
Size: 358989 Color: 1
Size: 321512 Color: 1
Size: 319498 Color: 0

Bin 449: 2 of cap free
Amount of items: 3
Items: 
Size: 356833 Color: 1
Size: 329748 Color: 1
Size: 313418 Color: 0

Bin 450: 2 of cap free
Amount of items: 3
Items: 
Size: 363267 Color: 0
Size: 320129 Color: 1
Size: 316603 Color: 0

Bin 451: 2 of cap free
Amount of items: 3
Items: 
Size: 367977 Color: 1
Size: 357266 Color: 1
Size: 274756 Color: 0

Bin 452: 2 of cap free
Amount of items: 3
Items: 
Size: 368034 Color: 1
Size: 339234 Color: 0
Size: 292731 Color: 1

Bin 453: 2 of cap free
Amount of items: 3
Items: 
Size: 374387 Color: 0
Size: 359872 Color: 0
Size: 265740 Color: 1

Bin 454: 2 of cap free
Amount of items: 3
Items: 
Size: 377066 Color: 1
Size: 318721 Color: 1
Size: 304212 Color: 0

Bin 455: 2 of cap free
Amount of items: 3
Items: 
Size: 377394 Color: 1
Size: 328927 Color: 0
Size: 293678 Color: 0

Bin 456: 2 of cap free
Amount of items: 3
Items: 
Size: 378495 Color: 1
Size: 333340 Color: 0
Size: 288164 Color: 1

Bin 457: 2 of cap free
Amount of items: 3
Items: 
Size: 378729 Color: 1
Size: 358320 Color: 0
Size: 262950 Color: 0

Bin 458: 2 of cap free
Amount of items: 3
Items: 
Size: 379352 Color: 1
Size: 366754 Color: 0
Size: 253893 Color: 0

Bin 459: 2 of cap free
Amount of items: 3
Items: 
Size: 381003 Color: 0
Size: 366129 Color: 1
Size: 252867 Color: 0

Bin 460: 2 of cap free
Amount of items: 3
Items: 
Size: 382496 Color: 1
Size: 323972 Color: 1
Size: 293531 Color: 0

Bin 461: 2 of cap free
Amount of items: 3
Items: 
Size: 383660 Color: 0
Size: 358333 Color: 1
Size: 258006 Color: 1

Bin 462: 2 of cap free
Amount of items: 3
Items: 
Size: 384458 Color: 1
Size: 320687 Color: 1
Size: 294854 Color: 0

Bin 463: 2 of cap free
Amount of items: 3
Items: 
Size: 390450 Color: 1
Size: 357834 Color: 1
Size: 251715 Color: 0

Bin 464: 2 of cap free
Amount of items: 3
Items: 
Size: 392609 Color: 1
Size: 331563 Color: 0
Size: 275827 Color: 0

Bin 465: 2 of cap free
Amount of items: 3
Items: 
Size: 398706 Color: 0
Size: 348743 Color: 0
Size: 252550 Color: 1

Bin 466: 2 of cap free
Amount of items: 3
Items: 
Size: 401686 Color: 1
Size: 333447 Color: 1
Size: 264866 Color: 0

Bin 467: 2 of cap free
Amount of items: 3
Items: 
Size: 408550 Color: 0
Size: 300241 Color: 1
Size: 291208 Color: 0

Bin 468: 2 of cap free
Amount of items: 3
Items: 
Size: 409736 Color: 0
Size: 306080 Color: 1
Size: 284183 Color: 1

Bin 469: 2 of cap free
Amount of items: 3
Items: 
Size: 409825 Color: 0
Size: 327225 Color: 0
Size: 262949 Color: 1

Bin 470: 2 of cap free
Amount of items: 3
Items: 
Size: 415540 Color: 0
Size: 310419 Color: 0
Size: 274040 Color: 1

Bin 471: 2 of cap free
Amount of items: 3
Items: 
Size: 419876 Color: 0
Size: 293494 Color: 1
Size: 286629 Color: 0

Bin 472: 2 of cap free
Amount of items: 3
Items: 
Size: 423172 Color: 1
Size: 301292 Color: 0
Size: 275535 Color: 1

Bin 473: 2 of cap free
Amount of items: 3
Items: 
Size: 430686 Color: 0
Size: 310416 Color: 1
Size: 258897 Color: 1

Bin 474: 2 of cap free
Amount of items: 3
Items: 
Size: 433117 Color: 0
Size: 307847 Color: 1
Size: 259035 Color: 0

Bin 475: 2 of cap free
Amount of items: 3
Items: 
Size: 440106 Color: 1
Size: 281015 Color: 0
Size: 278878 Color: 1

Bin 476: 2 of cap free
Amount of items: 3
Items: 
Size: 445251 Color: 1
Size: 295731 Color: 1
Size: 259017 Color: 0

Bin 477: 2 of cap free
Amount of items: 3
Items: 
Size: 453309 Color: 1
Size: 296077 Color: 0
Size: 250613 Color: 0

Bin 478: 2 of cap free
Amount of items: 3
Items: 
Size: 459555 Color: 1
Size: 289467 Color: 0
Size: 250977 Color: 1

Bin 479: 2 of cap free
Amount of items: 3
Items: 
Size: 460829 Color: 0
Size: 285894 Color: 1
Size: 253276 Color: 0

Bin 480: 2 of cap free
Amount of items: 3
Items: 
Size: 464046 Color: 0
Size: 282963 Color: 1
Size: 252990 Color: 0

Bin 481: 2 of cap free
Amount of items: 3
Items: 
Size: 481765 Color: 1
Size: 259527 Color: 0
Size: 258707 Color: 0

Bin 482: 2 of cap free
Amount of items: 3
Items: 
Size: 372015 Color: 0
Size: 350850 Color: 0
Size: 277134 Color: 1

Bin 483: 2 of cap free
Amount of items: 3
Items: 
Size: 355759 Color: 0
Size: 336684 Color: 0
Size: 307556 Color: 1

Bin 484: 2 of cap free
Amount of items: 3
Items: 
Size: 366863 Color: 1
Size: 325154 Color: 1
Size: 307982 Color: 0

Bin 485: 2 of cap free
Amount of items: 3
Items: 
Size: 372392 Color: 0
Size: 344539 Color: 0
Size: 283068 Color: 1

Bin 486: 2 of cap free
Amount of items: 3
Items: 
Size: 387999 Color: 1
Size: 337244 Color: 0
Size: 274756 Color: 0

Bin 487: 2 of cap free
Amount of items: 3
Items: 
Size: 348540 Color: 1
Size: 337285 Color: 0
Size: 314174 Color: 0

Bin 488: 3 of cap free
Amount of items: 3
Items: 
Size: 446306 Color: 1
Size: 280905 Color: 0
Size: 272787 Color: 1

Bin 489: 3 of cap free
Amount of items: 3
Items: 
Size: 359091 Color: 0
Size: 350941 Color: 1
Size: 289966 Color: 1

Bin 490: 3 of cap free
Amount of items: 3
Items: 
Size: 347183 Color: 0
Size: 332643 Color: 0
Size: 320172 Color: 1

Bin 491: 3 of cap free
Amount of items: 3
Items: 
Size: 447907 Color: 1
Size: 280988 Color: 0
Size: 271103 Color: 0

Bin 492: 3 of cap free
Amount of items: 3
Items: 
Size: 421207 Color: 1
Size: 306425 Color: 1
Size: 272366 Color: 0

Bin 493: 3 of cap free
Amount of items: 3
Items: 
Size: 359619 Color: 0
Size: 341187 Color: 0
Size: 299192 Color: 1

Bin 494: 3 of cap free
Amount of items: 3
Items: 
Size: 357438 Color: 0
Size: 339924 Color: 1
Size: 302636 Color: 0

Bin 495: 3 of cap free
Amount of items: 3
Items: 
Size: 369551 Color: 1
Size: 339715 Color: 1
Size: 290732 Color: 0

Bin 496: 3 of cap free
Amount of items: 3
Items: 
Size: 378920 Color: 1
Size: 324997 Color: 1
Size: 296081 Color: 0

Bin 497: 3 of cap free
Amount of items: 3
Items: 
Size: 383738 Color: 0
Size: 321887 Color: 0
Size: 294373 Color: 1

Bin 498: 3 of cap free
Amount of items: 3
Items: 
Size: 388388 Color: 0
Size: 360351 Color: 1
Size: 251259 Color: 0

Bin 499: 3 of cap free
Amount of items: 3
Items: 
Size: 391684 Color: 0
Size: 351435 Color: 1
Size: 256879 Color: 1

Bin 500: 3 of cap free
Amount of items: 3
Items: 
Size: 405735 Color: 1
Size: 335442 Color: 0
Size: 258821 Color: 1

Bin 501: 3 of cap free
Amount of items: 3
Items: 
Size: 423984 Color: 1
Size: 301323 Color: 0
Size: 274691 Color: 1

Bin 502: 3 of cap free
Amount of items: 3
Items: 
Size: 424884 Color: 1
Size: 319713 Color: 0
Size: 255401 Color: 1

Bin 503: 3 of cap free
Amount of items: 3
Items: 
Size: 377543 Color: 0
Size: 372037 Color: 1
Size: 250418 Color: 0

Bin 504: 3 of cap free
Amount of items: 3
Items: 
Size: 372198 Color: 0
Size: 323357 Color: 1
Size: 304443 Color: 1

Bin 505: 3 of cap free
Amount of items: 3
Items: 
Size: 345170 Color: 1
Size: 339616 Color: 0
Size: 315212 Color: 0

Bin 506: 3 of cap free
Amount of items: 3
Items: 
Size: 352783 Color: 1
Size: 346915 Color: 0
Size: 300300 Color: 1

Bin 507: 4 of cap free
Amount of items: 3
Items: 
Size: 368654 Color: 0
Size: 342827 Color: 1
Size: 288516 Color: 1

Bin 508: 4 of cap free
Amount of items: 3
Items: 
Size: 422270 Color: 0
Size: 297125 Color: 1
Size: 280602 Color: 1

Bin 509: 4 of cap free
Amount of items: 3
Items: 
Size: 370147 Color: 1
Size: 359155 Color: 0
Size: 270695 Color: 0

Bin 510: 4 of cap free
Amount of items: 3
Items: 
Size: 384058 Color: 1
Size: 343808 Color: 0
Size: 272131 Color: 1

Bin 511: 4 of cap free
Amount of items: 3
Items: 
Size: 370111 Color: 1
Size: 344732 Color: 0
Size: 285154 Color: 1

Bin 512: 4 of cap free
Amount of items: 3
Items: 
Size: 379407 Color: 1
Size: 319466 Color: 0
Size: 301124 Color: 1

Bin 513: 4 of cap free
Amount of items: 3
Items: 
Size: 383128 Color: 0
Size: 328302 Color: 0
Size: 288567 Color: 1

Bin 514: 4 of cap free
Amount of items: 3
Items: 
Size: 399393 Color: 1
Size: 339244 Color: 0
Size: 261360 Color: 0

Bin 515: 4 of cap free
Amount of items: 3
Items: 
Size: 408111 Color: 1
Size: 334253 Color: 1
Size: 257633 Color: 0

Bin 516: 4 of cap free
Amount of items: 3
Items: 
Size: 408860 Color: 0
Size: 306096 Color: 1
Size: 285041 Color: 0

Bin 517: 4 of cap free
Amount of items: 3
Items: 
Size: 414496 Color: 0
Size: 299792 Color: 1
Size: 285709 Color: 1

Bin 518: 4 of cap free
Amount of items: 3
Items: 
Size: 365462 Color: 0
Size: 337503 Color: 1
Size: 297032 Color: 0

Bin 519: 5 of cap free
Amount of items: 3
Items: 
Size: 352443 Color: 0
Size: 350456 Color: 1
Size: 297097 Color: 0

Bin 520: 5 of cap free
Amount of items: 3
Items: 
Size: 394876 Color: 0
Size: 328319 Color: 0
Size: 276801 Color: 1

Bin 521: 5 of cap free
Amount of items: 3
Items: 
Size: 373110 Color: 1
Size: 314010 Color: 0
Size: 312876 Color: 0

Bin 522: 5 of cap free
Amount of items: 3
Items: 
Size: 404364 Color: 1
Size: 338456 Color: 0
Size: 257176 Color: 0

Bin 523: 5 of cap free
Amount of items: 3
Items: 
Size: 415222 Color: 0
Size: 302709 Color: 1
Size: 282065 Color: 0

Bin 524: 5 of cap free
Amount of items: 3
Items: 
Size: 387239 Color: 1
Size: 342234 Color: 0
Size: 270523 Color: 0

Bin 525: 5 of cap free
Amount of items: 3
Items: 
Size: 369135 Color: 1
Size: 333883 Color: 1
Size: 296978 Color: 0

Bin 526: 5 of cap free
Amount of items: 3
Items: 
Size: 372425 Color: 0
Size: 333205 Color: 0
Size: 294366 Color: 1

Bin 527: 5 of cap free
Amount of items: 3
Items: 
Size: 376880 Color: 0
Size: 345340 Color: 1
Size: 277776 Color: 0

Bin 528: 5 of cap free
Amount of items: 3
Items: 
Size: 390640 Color: 0
Size: 338639 Color: 1
Size: 270717 Color: 0

Bin 529: 5 of cap free
Amount of items: 3
Items: 
Size: 420352 Color: 0
Size: 307532 Color: 0
Size: 272112 Color: 1

Bin 530: 5 of cap free
Amount of items: 3
Items: 
Size: 367131 Color: 1
Size: 334960 Color: 0
Size: 297905 Color: 0

Bin 531: 5 of cap free
Amount of items: 3
Items: 
Size: 376368 Color: 1
Size: 335304 Color: 0
Size: 288324 Color: 1

Bin 532: 5 of cap free
Amount of items: 3
Items: 
Size: 397116 Color: 0
Size: 311337 Color: 1
Size: 291543 Color: 1

Bin 533: 5 of cap free
Amount of items: 3
Items: 
Size: 360067 Color: 1
Size: 338251 Color: 1
Size: 301678 Color: 0

Bin 534: 5 of cap free
Amount of items: 3
Items: 
Size: 372177 Color: 0
Size: 324759 Color: 1
Size: 303060 Color: 1

Bin 535: 5 of cap free
Amount of items: 3
Items: 
Size: 360633 Color: 0
Size: 342514 Color: 1
Size: 296849 Color: 1

Bin 536: 5 of cap free
Amount of items: 3
Items: 
Size: 350445 Color: 1
Size: 346634 Color: 0
Size: 302917 Color: 1

Bin 537: 6 of cap free
Amount of items: 3
Items: 
Size: 378354 Color: 0
Size: 352438 Color: 1
Size: 269203 Color: 0

Bin 538: 6 of cap free
Amount of items: 3
Items: 
Size: 350095 Color: 0
Size: 348829 Color: 1
Size: 301071 Color: 1

Bin 539: 6 of cap free
Amount of items: 3
Items: 
Size: 356459 Color: 1
Size: 326154 Color: 1
Size: 317382 Color: 0

Bin 540: 6 of cap free
Amount of items: 3
Items: 
Size: 365338 Color: 0
Size: 351346 Color: 0
Size: 283311 Color: 1

Bin 541: 6 of cap free
Amount of items: 3
Items: 
Size: 386634 Color: 0
Size: 308904 Color: 1
Size: 304457 Color: 1

Bin 542: 6 of cap free
Amount of items: 3
Items: 
Size: 368978 Color: 0
Size: 336231 Color: 0
Size: 294786 Color: 1

Bin 543: 6 of cap free
Amount of items: 3
Items: 
Size: 354560 Color: 1
Size: 331824 Color: 0
Size: 313611 Color: 1

Bin 544: 6 of cap free
Amount of items: 3
Items: 
Size: 376497 Color: 1
Size: 315892 Color: 0
Size: 307606 Color: 1

Bin 545: 6 of cap free
Amount of items: 3
Items: 
Size: 384785 Color: 1
Size: 314856 Color: 0
Size: 300354 Color: 1

Bin 546: 6 of cap free
Amount of items: 3
Items: 
Size: 352811 Color: 1
Size: 334772 Color: 1
Size: 312412 Color: 0

Bin 547: 7 of cap free
Amount of items: 3
Items: 
Size: 393012 Color: 1
Size: 318251 Color: 1
Size: 288731 Color: 0

Bin 548: 7 of cap free
Amount of items: 3
Items: 
Size: 482357 Color: 0
Size: 263084 Color: 1
Size: 254553 Color: 1

Bin 549: 7 of cap free
Amount of items: 3
Items: 
Size: 354061 Color: 0
Size: 338279 Color: 1
Size: 307654 Color: 1

Bin 550: 7 of cap free
Amount of items: 3
Items: 
Size: 352659 Color: 0
Size: 335215 Color: 1
Size: 312120 Color: 0

Bin 551: 7 of cap free
Amount of items: 3
Items: 
Size: 386894 Color: 0
Size: 316323 Color: 1
Size: 296777 Color: 0

Bin 552: 7 of cap free
Amount of items: 3
Items: 
Size: 368251 Color: 0
Size: 323081 Color: 0
Size: 308662 Color: 1

Bin 553: 7 of cap free
Amount of items: 3
Items: 
Size: 377740 Color: 1
Size: 368133 Color: 0
Size: 254121 Color: 1

Bin 554: 7 of cap free
Amount of items: 3
Items: 
Size: 421300 Color: 0
Size: 290865 Color: 1
Size: 287829 Color: 1

Bin 555: 8 of cap free
Amount of items: 3
Items: 
Size: 403979 Color: 1
Size: 309937 Color: 1
Size: 286077 Color: 0

Bin 556: 8 of cap free
Amount of items: 3
Items: 
Size: 345427 Color: 0
Size: 334472 Color: 0
Size: 320094 Color: 1

Bin 557: 8 of cap free
Amount of items: 3
Items: 
Size: 362348 Color: 0
Size: 322238 Color: 0
Size: 315407 Color: 1

Bin 558: 8 of cap free
Amount of items: 3
Items: 
Size: 366085 Color: 0
Size: 325177 Color: 1
Size: 308731 Color: 0

Bin 559: 9 of cap free
Amount of items: 3
Items: 
Size: 482507 Color: 0
Size: 259204 Color: 1
Size: 258281 Color: 1

Bin 560: 9 of cap free
Amount of items: 3
Items: 
Size: 353122 Color: 0
Size: 351257 Color: 1
Size: 295613 Color: 0

Bin 561: 10 of cap free
Amount of items: 3
Items: 
Size: 360701 Color: 1
Size: 350092 Color: 0
Size: 289198 Color: 1

Bin 562: 11 of cap free
Amount of items: 3
Items: 
Size: 421774 Color: 0
Size: 318981 Color: 0
Size: 259235 Color: 1

Bin 563: 11 of cap free
Amount of items: 3
Items: 
Size: 341532 Color: 1
Size: 335935 Color: 0
Size: 322523 Color: 0

Bin 564: 11 of cap free
Amount of items: 3
Items: 
Size: 388176 Color: 0
Size: 309776 Color: 1
Size: 302038 Color: 1

Bin 565: 12 of cap free
Amount of items: 3
Items: 
Size: 427014 Color: 0
Size: 291725 Color: 0
Size: 281250 Color: 1

Bin 566: 12 of cap free
Amount of items: 3
Items: 
Size: 367375 Color: 0
Size: 358078 Color: 1
Size: 274536 Color: 0

Bin 567: 13 of cap free
Amount of items: 3
Items: 
Size: 359413 Color: 1
Size: 348120 Color: 0
Size: 292455 Color: 1

Bin 568: 13 of cap free
Amount of items: 3
Items: 
Size: 359201 Color: 1
Size: 329425 Color: 1
Size: 311362 Color: 0

Bin 569: 14 of cap free
Amount of items: 3
Items: 
Size: 418841 Color: 1
Size: 295212 Color: 0
Size: 285934 Color: 0

Bin 570: 15 of cap free
Amount of items: 3
Items: 
Size: 474612 Color: 1
Size: 263342 Color: 1
Size: 262032 Color: 0

Bin 571: 15 of cap free
Amount of items: 3
Items: 
Size: 353830 Color: 1
Size: 333413 Color: 0
Size: 312743 Color: 0

Bin 572: 15 of cap free
Amount of items: 3
Items: 
Size: 371223 Color: 0
Size: 358204 Color: 0
Size: 270559 Color: 1

Bin 573: 15 of cap free
Amount of items: 3
Items: 
Size: 356275 Color: 0
Size: 341253 Color: 1
Size: 302458 Color: 1

Bin 574: 15 of cap free
Amount of items: 3
Items: 
Size: 365322 Color: 1
Size: 325602 Color: 1
Size: 309062 Color: 0

Bin 575: 16 of cap free
Amount of items: 3
Items: 
Size: 361156 Color: 1
Size: 329214 Color: 0
Size: 309615 Color: 1

Bin 576: 17 of cap free
Amount of items: 3
Items: 
Size: 374809 Color: 1
Size: 351356 Color: 0
Size: 273819 Color: 1

Bin 577: 17 of cap free
Amount of items: 3
Items: 
Size: 497590 Color: 1
Size: 251558 Color: 1
Size: 250836 Color: 0

Bin 578: 17 of cap free
Amount of items: 3
Items: 
Size: 376009 Color: 0
Size: 369936 Color: 1
Size: 254039 Color: 1

Bin 579: 17 of cap free
Amount of items: 3
Items: 
Size: 496631 Color: 1
Size: 253117 Color: 1
Size: 250236 Color: 0

Bin 580: 17 of cap free
Amount of items: 3
Items: 
Size: 355964 Color: 0
Size: 322106 Color: 1
Size: 321914 Color: 0

Bin 581: 17 of cap free
Amount of items: 3
Items: 
Size: 369747 Color: 1
Size: 348621 Color: 1
Size: 281616 Color: 0

Bin 582: 17 of cap free
Amount of items: 3
Items: 
Size: 342863 Color: 0
Size: 337987 Color: 1
Size: 319134 Color: 0

Bin 583: 17 of cap free
Amount of items: 3
Items: 
Size: 450267 Color: 1
Size: 279169 Color: 0
Size: 270548 Color: 0

Bin 584: 18 of cap free
Amount of items: 3
Items: 
Size: 398170 Color: 1
Size: 348785 Color: 0
Size: 253028 Color: 0

Bin 585: 19 of cap free
Amount of items: 3
Items: 
Size: 373462 Color: 0
Size: 319324 Color: 1
Size: 307196 Color: 0

Bin 586: 19 of cap free
Amount of items: 3
Items: 
Size: 353381 Color: 1
Size: 324830 Color: 0
Size: 321771 Color: 1

Bin 587: 19 of cap free
Amount of items: 3
Items: 
Size: 393194 Color: 0
Size: 335333 Color: 0
Size: 271455 Color: 1

Bin 588: 20 of cap free
Amount of items: 3
Items: 
Size: 368611 Color: 0
Size: 332377 Color: 0
Size: 298993 Color: 1

Bin 589: 20 of cap free
Amount of items: 3
Items: 
Size: 457104 Color: 0
Size: 272034 Color: 1
Size: 270843 Color: 0

Bin 590: 21 of cap free
Amount of items: 3
Items: 
Size: 388217 Color: 1
Size: 335400 Color: 0
Size: 276363 Color: 0

Bin 591: 21 of cap free
Amount of items: 3
Items: 
Size: 454545 Color: 1
Size: 283276 Color: 0
Size: 262159 Color: 1

Bin 592: 21 of cap free
Amount of items: 3
Items: 
Size: 341182 Color: 0
Size: 340655 Color: 0
Size: 318143 Color: 1

Bin 593: 22 of cap free
Amount of items: 3
Items: 
Size: 414575 Color: 0
Size: 295758 Color: 1
Size: 289646 Color: 1

Bin 594: 22 of cap free
Amount of items: 3
Items: 
Size: 429146 Color: 1
Size: 299560 Color: 0
Size: 271273 Color: 1

Bin 595: 25 of cap free
Amount of items: 3
Items: 
Size: 353716 Color: 1
Size: 335668 Color: 1
Size: 310592 Color: 0

Bin 596: 25 of cap free
Amount of items: 3
Items: 
Size: 339815 Color: 1
Size: 338675 Color: 0
Size: 321486 Color: 0

Bin 597: 27 of cap free
Amount of items: 3
Items: 
Size: 360368 Color: 1
Size: 350770 Color: 0
Size: 288836 Color: 1

Bin 598: 28 of cap free
Amount of items: 3
Items: 
Size: 493259 Color: 0
Size: 254353 Color: 1
Size: 252361 Color: 0

Bin 599: 30 of cap free
Amount of items: 3
Items: 
Size: 498164 Color: 1
Size: 251092 Color: 1
Size: 250715 Color: 0

Bin 600: 31 of cap free
Amount of items: 3
Items: 
Size: 473987 Color: 0
Size: 265676 Color: 1
Size: 260307 Color: 1

Bin 601: 32 of cap free
Amount of items: 3
Items: 
Size: 351997 Color: 0
Size: 348851 Color: 1
Size: 299121 Color: 1

Bin 602: 33 of cap free
Amount of items: 3
Items: 
Size: 498910 Color: 0
Size: 250872 Color: 1
Size: 250186 Color: 0

Bin 603: 33 of cap free
Amount of items: 3
Items: 
Size: 368408 Color: 0
Size: 334073 Color: 1
Size: 297487 Color: 0

Bin 604: 34 of cap free
Amount of items: 3
Items: 
Size: 474203 Color: 1
Size: 274059 Color: 0
Size: 251705 Color: 1

Bin 605: 35 of cap free
Amount of items: 3
Items: 
Size: 344534 Color: 1
Size: 335701 Color: 0
Size: 319731 Color: 0

Bin 606: 35 of cap free
Amount of items: 3
Items: 
Size: 431241 Color: 0
Size: 289951 Color: 1
Size: 278774 Color: 0

Bin 607: 35 of cap free
Amount of items: 3
Items: 
Size: 414340 Color: 0
Size: 320026 Color: 1
Size: 265600 Color: 1

Bin 608: 35 of cap free
Amount of items: 3
Items: 
Size: 352436 Color: 1
Size: 336859 Color: 1
Size: 310671 Color: 0

Bin 609: 37 of cap free
Amount of items: 3
Items: 
Size: 490051 Color: 1
Size: 257301 Color: 0
Size: 252612 Color: 0

Bin 610: 37 of cap free
Amount of items: 3
Items: 
Size: 376909 Color: 0
Size: 340779 Color: 0
Size: 282276 Color: 1

Bin 611: 37 of cap free
Amount of items: 3
Items: 
Size: 379786 Color: 1
Size: 351078 Color: 1
Size: 269100 Color: 0

Bin 612: 38 of cap free
Amount of items: 3
Items: 
Size: 419637 Color: 1
Size: 307366 Color: 0
Size: 272960 Color: 0

Bin 613: 39 of cap free
Amount of items: 3
Items: 
Size: 365484 Color: 0
Size: 360365 Color: 1
Size: 274113 Color: 1

Bin 614: 40 of cap free
Amount of items: 3
Items: 
Size: 418788 Color: 1
Size: 310224 Color: 0
Size: 270949 Color: 0

Bin 615: 45 of cap free
Amount of items: 3
Items: 
Size: 425897 Color: 1
Size: 291997 Color: 0
Size: 282062 Color: 0

Bin 616: 45 of cap free
Amount of items: 3
Items: 
Size: 381210 Color: 1
Size: 335457 Color: 0
Size: 283289 Color: 1

Bin 617: 47 of cap free
Amount of items: 3
Items: 
Size: 466655 Color: 1
Size: 274529 Color: 0
Size: 258770 Color: 1

Bin 618: 47 of cap free
Amount of items: 3
Items: 
Size: 417783 Color: 1
Size: 322813 Color: 0
Size: 259358 Color: 1

Bin 619: 52 of cap free
Amount of items: 3
Items: 
Size: 387669 Color: 0
Size: 347705 Color: 1
Size: 264575 Color: 0

Bin 620: 56 of cap free
Amount of items: 3
Items: 
Size: 342576 Color: 0
Size: 333803 Color: 1
Size: 323566 Color: 1

Bin 621: 58 of cap free
Amount of items: 3
Items: 
Size: 354410 Color: 0
Size: 343954 Color: 0
Size: 301579 Color: 1

Bin 622: 62 of cap free
Amount of items: 3
Items: 
Size: 344576 Color: 0
Size: 341528 Color: 1
Size: 313835 Color: 1

Bin 623: 63 of cap free
Amount of items: 3
Items: 
Size: 356413 Color: 1
Size: 338506 Color: 0
Size: 305019 Color: 1

Bin 624: 66 of cap free
Amount of items: 3
Items: 
Size: 360296 Color: 1
Size: 351522 Color: 1
Size: 288117 Color: 0

Bin 625: 73 of cap free
Amount of items: 3
Items: 
Size: 437501 Color: 1
Size: 283076 Color: 0
Size: 279351 Color: 1

Bin 626: 76 of cap free
Amount of items: 3
Items: 
Size: 357788 Color: 1
Size: 325391 Color: 0
Size: 316746 Color: 0

Bin 627: 78 of cap free
Amount of items: 3
Items: 
Size: 382223 Color: 0
Size: 363199 Color: 1
Size: 254501 Color: 0

Bin 628: 79 of cap free
Amount of items: 3
Items: 
Size: 466479 Color: 1
Size: 273637 Color: 1
Size: 259806 Color: 0

Bin 629: 82 of cap free
Amount of items: 3
Items: 
Size: 392599 Color: 1
Size: 329956 Color: 0
Size: 277364 Color: 0

Bin 630: 88 of cap free
Amount of items: 3
Items: 
Size: 448871 Color: 0
Size: 286968 Color: 1
Size: 264074 Color: 1

Bin 631: 89 of cap free
Amount of items: 3
Items: 
Size: 465468 Color: 0
Size: 270894 Color: 1
Size: 263550 Color: 0

Bin 632: 90 of cap free
Amount of items: 3
Items: 
Size: 430501 Color: 0
Size: 302928 Color: 1
Size: 266482 Color: 1

Bin 633: 94 of cap free
Amount of items: 3
Items: 
Size: 419928 Color: 0
Size: 311191 Color: 0
Size: 268788 Color: 1

Bin 634: 100 of cap free
Amount of items: 3
Items: 
Size: 366136 Color: 1
Size: 331627 Color: 1
Size: 302138 Color: 0

Bin 635: 112 of cap free
Amount of items: 3
Items: 
Size: 448376 Color: 0
Size: 283247 Color: 0
Size: 268266 Color: 1

Bin 636: 123 of cap free
Amount of items: 3
Items: 
Size: 350480 Color: 1
Size: 341330 Color: 0
Size: 308068 Color: 1

Bin 637: 123 of cap free
Amount of items: 3
Items: 
Size: 354240 Color: 1
Size: 325967 Color: 1
Size: 319671 Color: 0

Bin 638: 155 of cap free
Amount of items: 3
Items: 
Size: 395895 Color: 0
Size: 331027 Color: 1
Size: 272924 Color: 1

Bin 639: 157 of cap free
Amount of items: 3
Items: 
Size: 441387 Color: 0
Size: 284869 Color: 1
Size: 273588 Color: 0

Bin 640: 167 of cap free
Amount of items: 3
Items: 
Size: 396633 Color: 0
Size: 326253 Color: 0
Size: 276948 Color: 1

Bin 641: 194 of cap free
Amount of items: 3
Items: 
Size: 366913 Color: 1
Size: 335177 Color: 1
Size: 297717 Color: 0

Bin 642: 198 of cap free
Amount of items: 3
Items: 
Size: 348583 Color: 0
Size: 335175 Color: 1
Size: 316045 Color: 0

Bin 643: 198 of cap free
Amount of items: 3
Items: 
Size: 368959 Color: 1
Size: 352569 Color: 0
Size: 278275 Color: 0

Bin 644: 205 of cap free
Amount of items: 3
Items: 
Size: 342980 Color: 1
Size: 339125 Color: 0
Size: 317691 Color: 0

Bin 645: 221 of cap free
Amount of items: 3
Items: 
Size: 396450 Color: 1
Size: 320861 Color: 0
Size: 282469 Color: 0

Bin 646: 250 of cap free
Amount of items: 3
Items: 
Size: 471172 Color: 1
Size: 276685 Color: 0
Size: 251894 Color: 0

Bin 647: 273 of cap free
Amount of items: 3
Items: 
Size: 498620 Color: 1
Size: 250696 Color: 0
Size: 250412 Color: 0

Bin 648: 283 of cap free
Amount of items: 3
Items: 
Size: 392745 Color: 1
Size: 321115 Color: 0
Size: 285858 Color: 0

Bin 649: 311 of cap free
Amount of items: 3
Items: 
Size: 479530 Color: 0
Size: 263007 Color: 0
Size: 257153 Color: 1

Bin 650: 326 of cap free
Amount of items: 3
Items: 
Size: 370551 Color: 1
Size: 335128 Color: 0
Size: 293996 Color: 1

Bin 651: 328 of cap free
Amount of items: 3
Items: 
Size: 355293 Color: 0
Size: 336086 Color: 0
Size: 308294 Color: 1

Bin 652: 379 of cap free
Amount of items: 3
Items: 
Size: 375995 Color: 0
Size: 327125 Color: 1
Size: 296502 Color: 0

Bin 653: 489 of cap free
Amount of items: 3
Items: 
Size: 369687 Color: 1
Size: 321805 Color: 1
Size: 308020 Color: 0

Bin 654: 497 of cap free
Amount of items: 3
Items: 
Size: 410762 Color: 1
Size: 312699 Color: 1
Size: 276043 Color: 0

Bin 655: 573 of cap free
Amount of items: 2
Items: 
Size: 499741 Color: 0
Size: 499687 Color: 1

Bin 656: 611 of cap free
Amount of items: 3
Items: 
Size: 417678 Color: 0
Size: 310575 Color: 1
Size: 271137 Color: 1

Bin 657: 1232 of cap free
Amount of items: 3
Items: 
Size: 342066 Color: 0
Size: 341613 Color: 1
Size: 315090 Color: 0

Bin 658: 1252 of cap free
Amount of items: 2
Items: 
Size: 499467 Color: 0
Size: 499282 Color: 1

Bin 659: 2989 of cap free
Amount of items: 3
Items: 
Size: 373240 Color: 1
Size: 312054 Color: 0
Size: 311718 Color: 0

Bin 660: 3129 of cap free
Amount of items: 3
Items: 
Size: 366325 Color: 0
Size: 358543 Color: 1
Size: 272004 Color: 1

Bin 661: 4110 of cap free
Amount of items: 2
Items: 
Size: 498378 Color: 1
Size: 497513 Color: 0

Bin 662: 5176 of cap free
Amount of items: 3
Items: 
Size: 431057 Color: 0
Size: 288512 Color: 1
Size: 275256 Color: 1

Bin 663: 25260 of cap free
Amount of items: 3
Items: 
Size: 328152 Color: 0
Size: 325886 Color: 0
Size: 320703 Color: 1

Bin 664: 100558 of cap free
Amount of items: 3
Items: 
Size: 307947 Color: 0
Size: 303364 Color: 1
Size: 288132 Color: 1

Bin 665: 182646 of cap free
Amount of items: 3
Items: 
Size: 281911 Color: 0
Size: 269915 Color: 0
Size: 265529 Color: 1

Bin 666: 214168 of cap free
Amount of items: 3
Items: 
Size: 263717 Color: 1
Size: 261369 Color: 0
Size: 260747 Color: 0

Bin 667: 221123 of cap free
Amount of items: 3
Items: 
Size: 260024 Color: 0
Size: 259749 Color: 0
Size: 259105 Color: 1

Bin 668: 229007 of cap free
Amount of items: 3
Items: 
Size: 258690 Color: 0
Size: 256819 Color: 1
Size: 255485 Color: 1

Total size: 667000667
Total free space: 1000001

