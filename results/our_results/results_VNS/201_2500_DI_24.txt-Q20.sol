Capicity Bin: 2032
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 19
Items: 
Size: 472 Color: 19
Size: 158 Color: 16
Size: 144 Color: 13
Size: 130 Color: 18
Size: 128 Color: 18
Size: 126 Color: 17
Size: 120 Color: 17
Size: 94 Color: 15
Size: 94 Color: 12
Size: 92 Color: 11
Size: 78 Color: 10
Size: 64 Color: 3
Size: 56 Color: 1
Size: 52 Color: 14
Size: 50 Color: 14
Size: 50 Color: 6
Size: 46 Color: 9
Size: 40 Color: 0
Size: 38 Color: 9

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 1017 Color: 14
Size: 721 Color: 0
Size: 168 Color: 17
Size: 78 Color: 9
Size: 48 Color: 18

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1126 Color: 7
Size: 846 Color: 9
Size: 60 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1127 Color: 8
Size: 843 Color: 4
Size: 62 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1130 Color: 13
Size: 846 Color: 17
Size: 56 Color: 7

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1301 Color: 15
Size: 673 Color: 11
Size: 58 Color: 4

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 1379 Color: 3
Size: 411 Color: 14
Size: 190 Color: 7
Size: 52 Color: 7

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 14
Size: 462 Color: 4
Size: 36 Color: 8

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1591 Color: 13
Size: 325 Color: 10
Size: 116 Color: 12

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 10
Size: 369 Color: 16
Size: 34 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1649 Color: 7
Size: 293 Color: 7
Size: 90 Color: 2

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1681 Color: 7
Size: 291 Color: 16
Size: 60 Color: 14

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 0
Size: 314 Color: 2
Size: 36 Color: 13

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1731 Color: 6
Size: 193 Color: 0
Size: 108 Color: 13

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1759 Color: 8
Size: 197 Color: 11
Size: 76 Color: 19

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1771 Color: 15
Size: 173 Color: 13
Size: 88 Color: 16

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1790 Color: 2
Size: 206 Color: 1
Size: 36 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1791 Color: 8
Size: 201 Color: 14
Size: 40 Color: 13

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 18
Size: 148 Color: 3
Size: 78 Color: 19

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1821 Color: 14
Size: 177 Color: 2
Size: 34 Color: 15

Bin 21: 0 of cap free
Amount of items: 4
Items: 
Size: 1810 Color: 18
Size: 210 Color: 11
Size: 8 Color: 3
Size: 4 Color: 4

Bin 22: 1 of cap free
Amount of items: 4
Items: 
Size: 1018 Color: 10
Size: 657 Color: 15
Size: 316 Color: 7
Size: 40 Color: 8

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 1222 Color: 3
Size: 641 Color: 13
Size: 168 Color: 14

Bin 24: 1 of cap free
Amount of items: 4
Items: 
Size: 1281 Color: 12
Size: 662 Color: 9
Size: 48 Color: 7
Size: 40 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1400 Color: 15
Size: 559 Color: 13
Size: 72 Color: 4

Bin 26: 1 of cap free
Amount of items: 2
Items: 
Size: 1478 Color: 6
Size: 553 Color: 8

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1653 Color: 7
Size: 228 Color: 5
Size: 150 Color: 5

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1702 Color: 15
Size: 255 Color: 7
Size: 74 Color: 9

Bin 29: 1 of cap free
Amount of items: 2
Items: 
Size: 1801 Color: 9
Size: 230 Color: 12

Bin 30: 1 of cap free
Amount of items: 2
Items: 
Size: 1825 Color: 12
Size: 206 Color: 11

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1383 Color: 8
Size: 461 Color: 0
Size: 186 Color: 10

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 11
Size: 391 Color: 4
Size: 100 Color: 3

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1587 Color: 5
Size: 443 Color: 15

Bin 34: 2 of cap free
Amount of items: 2
Items: 
Size: 1709 Color: 4
Size: 321 Color: 17

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 1727 Color: 17
Size: 251 Color: 15
Size: 52 Color: 3

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1457 Color: 3
Size: 363 Color: 18
Size: 209 Color: 17

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1658 Color: 1
Size: 371 Color: 8

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 9
Size: 262 Color: 15
Size: 82 Color: 13

Bin 39: 3 of cap free
Amount of items: 2
Items: 
Size: 1758 Color: 9
Size: 271 Color: 18

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1618 Color: 1
Size: 338 Color: 4
Size: 72 Color: 14

Bin 41: 6 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 2
Size: 795 Color: 10
Size: 64 Color: 17

Bin 42: 6 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 11
Size: 228 Color: 3
Size: 16 Color: 16

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1797 Color: 14
Size: 229 Color: 15

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 18
Size: 611 Color: 4

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 8
Size: 418 Color: 1
Size: 44 Color: 0

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1643 Color: 2
Size: 382 Color: 3

Bin 47: 7 of cap free
Amount of items: 2
Items: 
Size: 1708 Color: 9
Size: 317 Color: 8

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1747 Color: 16
Size: 278 Color: 3

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1786 Color: 14
Size: 239 Color: 16

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 1481 Color: 0
Size: 481 Color: 12
Size: 62 Color: 13

Bin 51: 8 of cap free
Amount of items: 2
Items: 
Size: 1805 Color: 11
Size: 219 Color: 14

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1265 Color: 5
Size: 758 Color: 15

Bin 53: 9 of cap free
Amount of items: 2
Items: 
Size: 1441 Color: 9
Size: 582 Color: 1

Bin 54: 9 of cap free
Amount of items: 2
Items: 
Size: 1783 Color: 10
Size: 240 Color: 14

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 11
Size: 937 Color: 2
Size: 64 Color: 6

Bin 56: 10 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 15
Size: 420 Color: 12
Size: 24 Color: 12

Bin 57: 11 of cap free
Amount of items: 4
Items: 
Size: 1079 Color: 6
Size: 820 Color: 4
Size: 80 Color: 14
Size: 42 Color: 15

Bin 58: 12 of cap free
Amount of items: 2
Items: 
Size: 1630 Color: 13
Size: 390 Color: 19

Bin 59: 12 of cap free
Amount of items: 2
Items: 
Size: 1744 Color: 14
Size: 276 Color: 2

Bin 60: 13 of cap free
Amount of items: 2
Items: 
Size: 1501 Color: 12
Size: 518 Color: 5

Bin 61: 16 of cap free
Amount of items: 2
Items: 
Size: 1722 Color: 17
Size: 294 Color: 15

Bin 62: 18 of cap free
Amount of items: 2
Items: 
Size: 1334 Color: 13
Size: 680 Color: 4

Bin 63: 20 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 0
Size: 541 Color: 17
Size: 191 Color: 18

Bin 64: 33 of cap free
Amount of items: 2
Items: 
Size: 1245 Color: 13
Size: 754 Color: 0

Bin 65: 35 of cap free
Amount of items: 2
Items: 
Size: 1242 Color: 6
Size: 755 Color: 7

Bin 66: 1704 of cap free
Amount of items: 8
Items: 
Size: 48 Color: 13
Size: 44 Color: 12
Size: 44 Color: 10
Size: 40 Color: 15
Size: 40 Color: 4
Size: 38 Color: 16
Size: 38 Color: 0
Size: 36 Color: 11

Total size: 132080
Total free space: 2032

