Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 403

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 5735 Color: 316
Size: 1941 Color: 226
Size: 404 Color: 97

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 319
Size: 1928 Color: 225
Size: 316 Color: 81

Bin 3: 0 of cap free
Amount of items: 4
Items: 
Size: 5928 Color: 321
Size: 2056 Color: 228
Size: 64 Color: 4
Size: 32 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5950 Color: 323
Size: 1778 Color: 218
Size: 352 Color: 87

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 6056 Color: 327
Size: 1800 Color: 220
Size: 224 Color: 48

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6092 Color: 328
Size: 1852 Color: 222
Size: 136 Color: 14

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6239 Color: 335
Size: 1535 Color: 206
Size: 306 Color: 79

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6364 Color: 338
Size: 1092 Color: 171
Size: 624 Color: 125

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6365 Color: 339
Size: 1431 Color: 200
Size: 284 Color: 72

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6376 Color: 340
Size: 1272 Color: 184
Size: 432 Color: 104

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6424 Color: 344
Size: 1300 Color: 186
Size: 356 Color: 90

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6428 Color: 346
Size: 1324 Color: 188
Size: 328 Color: 83

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6443 Color: 347
Size: 1153 Color: 176
Size: 484 Color: 110

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6492 Color: 351
Size: 1436 Color: 203
Size: 152 Color: 20

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6568 Color: 355
Size: 1192 Color: 179
Size: 320 Color: 82

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6666 Color: 357
Size: 1006 Color: 165
Size: 408 Color: 98

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6696 Color: 361
Size: 896 Color: 156
Size: 488 Color: 113

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6788 Color: 368
Size: 1084 Color: 170
Size: 208 Color: 43

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6840 Color: 373
Size: 952 Color: 161
Size: 288 Color: 73

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6909 Color: 376
Size: 1043 Color: 166
Size: 128 Color: 12

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6952 Color: 379
Size: 634 Color: 126
Size: 494 Color: 114

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6961 Color: 380
Size: 933 Color: 159
Size: 186 Color: 35

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 7004 Color: 382
Size: 900 Color: 157
Size: 176 Color: 32

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 7016 Color: 384
Size: 680 Color: 136
Size: 384 Color: 93

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 7023 Color: 386
Size: 881 Color: 152
Size: 176 Color: 30

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 7025 Color: 387
Size: 881 Color: 153
Size: 174 Color: 28

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 7046 Color: 390
Size: 850 Color: 147
Size: 184 Color: 34

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 7062 Color: 391
Size: 886 Color: 154
Size: 132 Color: 13

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 7075 Color: 392
Size: 839 Color: 144
Size: 166 Color: 23

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 7080 Color: 393
Size: 770 Color: 142
Size: 230 Color: 52

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 7108 Color: 395
Size: 690 Color: 137
Size: 282 Color: 71

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 7158 Color: 396
Size: 656 Color: 127
Size: 266 Color: 62

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 7224 Color: 399
Size: 664 Color: 131
Size: 192 Color: 37

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 7254 Color: 401
Size: 558 Color: 119
Size: 268 Color: 64

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 7272 Color: 402
Size: 656 Color: 128
Size: 152 Color: 21

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4068 Color: 279
Size: 3771 Color: 271
Size: 240 Color: 55

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 5142 Color: 298
Size: 1790 Color: 219
Size: 1147 Color: 175

Bin 38: 1 of cap free
Amount of items: 5
Items: 
Size: 5170 Color: 303
Size: 2433 Color: 240
Size: 172 Color: 26
Size: 160 Color: 22
Size: 144 Color: 19

Bin 39: 1 of cap free
Amount of items: 2
Items: 
Size: 6124 Color: 329
Size: 1955 Color: 227

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 6156 Color: 330
Size: 1727 Color: 215
Size: 196 Color: 38

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 6168 Color: 331
Size: 980 Color: 163
Size: 931 Color: 158

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 345
Size: 1436 Color: 202
Size: 216 Color: 46

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 6705 Color: 363
Size: 1358 Color: 191
Size: 16 Color: 1

Bin 44: 1 of cap free
Amount of items: 2
Items: 
Size: 6772 Color: 367
Size: 1307 Color: 187

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 6809 Color: 370
Size: 702 Color: 138
Size: 568 Color: 121

Bin 46: 1 of cap free
Amount of items: 2
Items: 
Size: 6878 Color: 374
Size: 1201 Color: 180

Bin 47: 1 of cap free
Amount of items: 2
Items: 
Size: 7018 Color: 385
Size: 1061 Color: 168

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 7041 Color: 389
Size: 862 Color: 148
Size: 176 Color: 31

Bin 49: 2 of cap free
Amount of items: 5
Items: 
Size: 4056 Color: 278
Size: 3264 Color: 261
Size: 256 Color: 59
Size: 254 Color: 57
Size: 248 Color: 56

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 309
Size: 2426 Color: 239
Size: 128 Color: 9

Bin 51: 2 of cap free
Amount of items: 2
Items: 
Size: 5594 Color: 312
Size: 2484 Color: 246

Bin 52: 2 of cap free
Amount of items: 2
Items: 
Size: 5610 Color: 314
Size: 2468 Color: 244

Bin 53: 2 of cap free
Amount of items: 3
Items: 
Size: 6234 Color: 334
Size: 1432 Color: 201
Size: 412 Color: 99

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 6470 Color: 349
Size: 1608 Color: 211

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 6524 Color: 352
Size: 1554 Color: 208

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 6678 Color: 359
Size: 1400 Color: 197

Bin 57: 2 of cap free
Amount of items: 2
Items: 
Size: 6803 Color: 369
Size: 1275 Color: 185

Bin 58: 2 of cap free
Amount of items: 2
Items: 
Size: 7088 Color: 394
Size: 990 Color: 164

Bin 59: 2 of cap free
Amount of items: 2
Items: 
Size: 7238 Color: 400
Size: 840 Color: 145

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 5100 Color: 295
Size: 2801 Color: 252
Size: 176 Color: 33

Bin 61: 3 of cap free
Amount of items: 2
Items: 
Size: 6308 Color: 337
Size: 1769 Color: 217

Bin 62: 3 of cap free
Amount of items: 2
Items: 
Size: 6486 Color: 350
Size: 1591 Color: 209

Bin 63: 3 of cap free
Amount of items: 2
Items: 
Size: 6535 Color: 353
Size: 1542 Color: 207

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 6681 Color: 360
Size: 1396 Color: 196

Bin 65: 3 of cap free
Amount of items: 2
Items: 
Size: 6747 Color: 366
Size: 1330 Color: 189

Bin 66: 3 of cap free
Amount of items: 2
Items: 
Size: 6965 Color: 381
Size: 1112 Color: 172

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 7012 Color: 383
Size: 1065 Color: 169

Bin 68: 3 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 388
Size: 1047 Color: 167

Bin 69: 3 of cap free
Amount of items: 2
Items: 
Size: 7210 Color: 398
Size: 867 Color: 149

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 4744 Color: 290
Size: 3332 Color: 263

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 342
Size: 1680 Color: 213

Bin 72: 4 of cap free
Amount of items: 2
Items: 
Size: 6697 Color: 362
Size: 1379 Color: 193

Bin 73: 4 of cap free
Amount of items: 2
Items: 
Size: 6894 Color: 375
Size: 1182 Color: 178

Bin 74: 4 of cap free
Amount of items: 2
Items: 
Size: 7188 Color: 397
Size: 888 Color: 155

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6001 Color: 325
Size: 2074 Color: 230

Bin 76: 5 of cap free
Amount of items: 2
Items: 
Size: 6710 Color: 364
Size: 1365 Color: 192

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 5624 Color: 315
Size: 2450 Color: 243

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 6218 Color: 333
Size: 1856 Color: 223

Bin 79: 6 of cap free
Amount of items: 3
Items: 
Size: 6454 Color: 348
Size: 1604 Color: 210
Size: 16 Color: 0

Bin 80: 6 of cap free
Amount of items: 2
Items: 
Size: 6825 Color: 372
Size: 1249 Color: 183

Bin 81: 7 of cap free
Amount of items: 3
Items: 
Size: 4721 Color: 289
Size: 3144 Color: 259
Size: 208 Color: 42

Bin 82: 7 of cap free
Amount of items: 2
Items: 
Size: 5600 Color: 313
Size: 2473 Color: 245

Bin 83: 7 of cap free
Amount of items: 2
Items: 
Size: 6943 Color: 378
Size: 1130 Color: 173

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 305
Size: 2736 Color: 249
Size: 144 Color: 16

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 354
Size: 1521 Color: 205

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 6668 Color: 358
Size: 1404 Color: 198

Bin 87: 8 of cap free
Amount of items: 2
Items: 
Size: 6824 Color: 371
Size: 1248 Color: 182

Bin 88: 9 of cap free
Amount of items: 3
Items: 
Size: 5556 Color: 310
Size: 2387 Color: 236
Size: 128 Color: 8

Bin 89: 9 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 326
Size: 2062 Color: 229

Bin 90: 9 of cap free
Amount of items: 2
Items: 
Size: 6411 Color: 343
Size: 1660 Color: 212

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 6910 Color: 377
Size: 1160 Color: 177

Bin 92: 12 of cap free
Amount of items: 2
Items: 
Size: 6726 Color: 365
Size: 1342 Color: 190

Bin 93: 13 of cap free
Amount of items: 5
Items: 
Size: 4052 Color: 277
Size: 1419 Color: 199
Size: 1391 Color: 195
Size: 949 Color: 160
Size: 256 Color: 58

Bin 94: 13 of cap free
Amount of items: 2
Items: 
Size: 5959 Color: 324
Size: 2108 Color: 232

Bin 95: 13 of cap free
Amount of items: 2
Items: 
Size: 6255 Color: 336
Size: 1812 Color: 221

Bin 96: 13 of cap free
Amount of items: 2
Items: 
Size: 6379 Color: 341
Size: 1688 Color: 214

Bin 97: 13 of cap free
Amount of items: 2
Items: 
Size: 6583 Color: 356
Size: 1484 Color: 204

Bin 98: 15 of cap free
Amount of items: 3
Items: 
Size: 4676 Color: 288
Size: 3179 Color: 260
Size: 210 Color: 44

Bin 99: 18 of cap free
Amount of items: 9
Items: 
Size: 4042 Color: 273
Size: 672 Color: 134
Size: 672 Color: 133
Size: 672 Color: 132
Size: 664 Color: 130
Size: 502 Color: 116
Size: 280 Color: 70
Size: 280 Color: 69
Size: 278 Color: 68

Bin 100: 18 of cap free
Amount of items: 3
Items: 
Size: 4833 Color: 291
Size: 3021 Color: 258
Size: 208 Color: 41

Bin 101: 22 of cap free
Amount of items: 3
Items: 
Size: 5577 Color: 311
Size: 1733 Color: 216
Size: 748 Color: 141

Bin 102: 24 of cap free
Amount of items: 3
Items: 
Size: 5860 Color: 320
Size: 2132 Color: 233
Size: 64 Color: 5

Bin 103: 25 of cap free
Amount of items: 3
Items: 
Size: 5483 Color: 308
Size: 2444 Color: 242
Size: 128 Color: 10

Bin 104: 27 of cap free
Amount of items: 3
Items: 
Size: 5934 Color: 322
Size: 2087 Color: 231
Size: 32 Color: 2

Bin 105: 28 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 294
Size: 2792 Color: 251
Size: 188 Color: 36

Bin 106: 31 of cap free
Amount of items: 2
Items: 
Size: 6173 Color: 332
Size: 1876 Color: 224

Bin 107: 32 of cap free
Amount of items: 3
Items: 
Size: 5768 Color: 318
Size: 2200 Color: 235
Size: 80 Color: 6

Bin 108: 35 of cap free
Amount of items: 3
Items: 
Size: 5057 Color: 293
Size: 2792 Color: 250
Size: 196 Color: 39

Bin 109: 40 of cap free
Amount of items: 7
Items: 
Size: 4044 Color: 275
Size: 978 Color: 162
Size: 880 Color: 151
Size: 878 Color: 150
Size: 728 Color: 140
Size: 268 Color: 63
Size: 264 Color: 61

Bin 110: 41 of cap free
Amount of items: 5
Items: 
Size: 4046 Color: 276
Size: 1386 Color: 194
Size: 1201 Color: 181
Size: 1142 Color: 174
Size: 264 Color: 60

Bin 111: 49 of cap free
Amount of items: 3
Items: 
Size: 4455 Color: 284
Size: 3364 Color: 267
Size: 212 Color: 45

Bin 112: 54 of cap free
Amount of items: 2
Items: 
Size: 4658 Color: 287
Size: 3368 Color: 270

Bin 113: 56 of cap free
Amount of items: 2
Items: 
Size: 5156 Color: 300
Size: 2868 Color: 257

Bin 114: 59 of cap free
Amount of items: 3
Items: 
Size: 5217 Color: 306
Size: 2664 Color: 248
Size: 140 Color: 15

Bin 115: 60 of cap free
Amount of items: 2
Items: 
Size: 5154 Color: 299
Size: 2866 Color: 256

Bin 116: 62 of cap free
Amount of items: 3
Items: 
Size: 5448 Color: 307
Size: 2442 Color: 241
Size: 128 Color: 11

Bin 117: 69 of cap free
Amount of items: 2
Items: 
Size: 4644 Color: 286
Size: 3367 Color: 269

Bin 118: 72 of cap free
Amount of items: 2
Items: 
Size: 4642 Color: 285
Size: 3366 Color: 268

Bin 119: 76 of cap free
Amount of items: 3
Items: 
Size: 5751 Color: 317
Size: 2165 Color: 234
Size: 88 Color: 7

Bin 120: 85 of cap free
Amount of items: 4
Items: 
Size: 5186 Color: 304
Size: 2521 Color: 247
Size: 144 Color: 18
Size: 144 Color: 17

Bin 121: 102 of cap free
Amount of items: 2
Items: 
Size: 5124 Color: 297
Size: 2854 Color: 255

Bin 122: 123 of cap free
Amount of items: 2
Items: 
Size: 5113 Color: 296
Size: 2844 Color: 254

Bin 123: 124 of cap free
Amount of items: 19
Items: 
Size: 544 Color: 118
Size: 528 Color: 117
Size: 496 Color: 115
Size: 488 Color: 112
Size: 486 Color: 111
Size: 484 Color: 109
Size: 480 Color: 108
Size: 480 Color: 107
Size: 480 Color: 106
Size: 476 Color: 105
Size: 368 Color: 91
Size: 352 Color: 89
Size: 352 Color: 88
Size: 346 Color: 86
Size: 344 Color: 85
Size: 336 Color: 84
Size: 308 Color: 80
Size: 304 Color: 78
Size: 304 Color: 77

Bin 124: 151 of cap free
Amount of items: 3
Items: 
Size: 4888 Color: 292
Size: 2843 Color: 253
Size: 198 Color: 40

Bin 125: 153 of cap free
Amount of items: 8
Items: 
Size: 4043 Color: 274
Size: 848 Color: 146
Size: 820 Color: 143
Size: 726 Color: 139
Size: 672 Color: 135
Size: 274 Color: 67
Size: 272 Color: 66
Size: 272 Color: 65

Bin 126: 164 of cap free
Amount of items: 4
Items: 
Size: 5160 Color: 301
Size: 2408 Color: 237
Size: 176 Color: 29
Size: 172 Color: 27

Bin 127: 165 of cap free
Amount of items: 4
Items: 
Size: 5161 Color: 302
Size: 2414 Color: 238
Size: 172 Color: 25
Size: 168 Color: 24

Bin 128: 175 of cap free
Amount of items: 9
Items: 
Size: 4041 Color: 272
Size: 664 Color: 129
Size: 604 Color: 124
Size: 572 Color: 123
Size: 568 Color: 122
Size: 560 Color: 120
Size: 304 Color: 76
Size: 304 Color: 75
Size: 288 Color: 74

Bin 129: 176 of cap free
Amount of items: 4
Items: 
Size: 4100 Color: 281
Size: 3348 Color: 264
Size: 228 Color: 51
Size: 228 Color: 50

Bin 130: 188 of cap free
Amount of items: 3
Items: 
Size: 4312 Color: 283
Size: 3364 Color: 266
Size: 216 Color: 47

Bin 131: 202 of cap free
Amount of items: 4
Items: 
Size: 4084 Color: 280
Size: 3324 Color: 262
Size: 238 Color: 54
Size: 232 Color: 53

Bin 132: 227 of cap free
Amount of items: 3
Items: 
Size: 4267 Color: 282
Size: 3362 Color: 265
Size: 224 Color: 49

Bin 133: 4846 of cap free
Amount of items: 8
Items: 
Size: 432 Color: 103
Size: 424 Color: 102
Size: 416 Color: 101
Size: 416 Color: 100
Size: 400 Color: 96
Size: 390 Color: 95
Size: 388 Color: 94
Size: 368 Color: 92

Total size: 1066560
Total free space: 8080

