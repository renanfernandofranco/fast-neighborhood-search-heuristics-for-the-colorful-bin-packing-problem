Capicity Bin: 2036
Lower Bound: 65

Bins used: 66
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 13
Items: 
Size: 450 Color: 0
Size: 316 Color: 3
Size: 311 Color: 3
Size: 311 Color: 0
Size: 98 Color: 1
Size: 80 Color: 2
Size: 78 Color: 2
Size: 76 Color: 1
Size: 76 Color: 1
Size: 64 Color: 3
Size: 64 Color: 3
Size: 64 Color: 2
Size: 48 Color: 3

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1021 Color: 2
Size: 847 Color: 0
Size: 168 Color: 2

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1177 Color: 4
Size: 721 Color: 0
Size: 138 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1404 Color: 2
Size: 590 Color: 0
Size: 42 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1429 Color: 3
Size: 507 Color: 1
Size: 100 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1439 Color: 1
Size: 499 Color: 0
Size: 98 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1447 Color: 2
Size: 493 Color: 0
Size: 96 Color: 1

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1459 Color: 1
Size: 481 Color: 1
Size: 96 Color: 4

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1458 Color: 2
Size: 522 Color: 4
Size: 56 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1498 Color: 3
Size: 294 Color: 1
Size: 244 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1598 Color: 0
Size: 402 Color: 2
Size: 36 Color: 4

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 0
Size: 315 Color: 2
Size: 38 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1686 Color: 0
Size: 278 Color: 4
Size: 72 Color: 2

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1679 Color: 4
Size: 295 Color: 0
Size: 62 Color: 3

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 3
Size: 194 Color: 1
Size: 136 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 0
Size: 173 Color: 1
Size: 118 Color: 2

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1752 Color: 0
Size: 142 Color: 3
Size: 142 Color: 2

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1776 Color: 1
Size: 144 Color: 2
Size: 116 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1797 Color: 0
Size: 193 Color: 1
Size: 46 Color: 3

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1782 Color: 3
Size: 218 Color: 0
Size: 36 Color: 3

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1805 Color: 4
Size: 197 Color: 0
Size: 34 Color: 1

Bin 22: 0 of cap free
Amount of items: 4
Items: 
Size: 1826 Color: 1
Size: 178 Color: 3
Size: 28 Color: 1
Size: 4 Color: 4

Bin 23: 1 of cap free
Amount of items: 5
Items: 
Size: 1019 Color: 1
Size: 717 Color: 0
Size: 217 Color: 3
Size: 42 Color: 3
Size: 40 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1209 Color: 4
Size: 678 Color: 3
Size: 148 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 1339 Color: 3
Size: 495 Color: 4
Size: 201 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1578 Color: 0
Size: 395 Color: 4
Size: 62 Color: 2

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1763 Color: 3
Size: 228 Color: 1
Size: 44 Color: 3

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 1777 Color: 2
Size: 142 Color: 0
Size: 116 Color: 4

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1806 Color: 1
Size: 191 Color: 1
Size: 38 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1829 Color: 4
Size: 130 Color: 0
Size: 76 Color: 3

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1330 Color: 0
Size: 491 Color: 1
Size: 213 Color: 3

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1539 Color: 0
Size: 407 Color: 2
Size: 88 Color: 1

Bin 33: 2 of cap free
Amount of items: 3
Items: 
Size: 1563 Color: 1
Size: 257 Color: 0
Size: 214 Color: 2

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1571 Color: 2
Size: 399 Color: 3
Size: 64 Color: 0

Bin 35: 3 of cap free
Amount of items: 3
Items: 
Size: 1193 Color: 2
Size: 756 Color: 0
Size: 84 Color: 4

Bin 36: 3 of cap free
Amount of items: 3
Items: 
Size: 1638 Color: 1
Size: 323 Color: 0
Size: 72 Color: 2

Bin 37: 3 of cap free
Amount of items: 2
Items: 
Size: 1699 Color: 1
Size: 334 Color: 3

Bin 38: 3 of cap free
Amount of items: 3
Items: 
Size: 1746 Color: 3
Size: 229 Color: 0
Size: 58 Color: 3

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1809 Color: 0
Size: 220 Color: 1
Size: 4 Color: 3

Bin 40: 4 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 4
Size: 417 Color: 2
Size: 48 Color: 1

Bin 41: 5 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 4
Size: 701 Color: 3
Size: 50 Color: 0

Bin 42: 5 of cap free
Amount of items: 2
Items: 
Size: 1549 Color: 0
Size: 482 Color: 3

Bin 43: 5 of cap free
Amount of items: 2
Items: 
Size: 1559 Color: 4
Size: 472 Color: 1

Bin 44: 5 of cap free
Amount of items: 2
Items: 
Size: 1649 Color: 4
Size: 382 Color: 2

Bin 45: 6 of cap free
Amount of items: 2
Items: 
Size: 1181 Color: 4
Size: 849 Color: 1

Bin 46: 7 of cap free
Amount of items: 3
Items: 
Size: 1327 Color: 3
Size: 650 Color: 2
Size: 52 Color: 0

Bin 47: 7 of cap free
Amount of items: 3
Items: 
Size: 1390 Color: 4
Size: 591 Color: 0
Size: 48 Color: 3

Bin 48: 7 of cap free
Amount of items: 2
Items: 
Size: 1663 Color: 4
Size: 366 Color: 3

Bin 49: 7 of cap free
Amount of items: 2
Items: 
Size: 1801 Color: 1
Size: 228 Color: 2

Bin 50: 8 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 3
Size: 334 Color: 4
Size: 20 Color: 0

Bin 51: 9 of cap free
Amount of items: 3
Items: 
Size: 1558 Color: 2
Size: 391 Color: 3
Size: 78 Color: 0

Bin 52: 9 of cap free
Amount of items: 2
Items: 
Size: 1603 Color: 3
Size: 424 Color: 4

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1443 Color: 3
Size: 581 Color: 1

Bin 54: 13 of cap free
Amount of items: 3
Items: 
Size: 1171 Color: 4
Size: 684 Color: 0
Size: 168 Color: 3

Bin 55: 13 of cap free
Amount of items: 2
Items: 
Size: 1302 Color: 1
Size: 721 Color: 3

Bin 56: 13 of cap free
Amount of items: 2
Items: 
Size: 1781 Color: 4
Size: 242 Color: 2

Bin 57: 15 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 3
Size: 243 Color: 4

Bin 58: 16 of cap free
Amount of items: 3
Items: 
Size: 1197 Color: 0
Size: 691 Color: 2
Size: 132 Color: 4

Bin 59: 16 of cap free
Amount of items: 3
Items: 
Size: 1226 Color: 0
Size: 754 Color: 4
Size: 40 Color: 2

Bin 60: 16 of cap free
Amount of items: 2
Items: 
Size: 1414 Color: 1
Size: 606 Color: 3

Bin 61: 16 of cap free
Amount of items: 2
Items: 
Size: 1659 Color: 1
Size: 361 Color: 3

Bin 62: 17 of cap free
Amount of items: 2
Items: 
Size: 1173 Color: 2
Size: 846 Color: 3

Bin 63: 18 of cap free
Amount of items: 2
Items: 
Size: 1729 Color: 1
Size: 289 Color: 4

Bin 64: 21 of cap free
Amount of items: 3
Items: 
Size: 1134 Color: 2
Size: 713 Color: 4
Size: 168 Color: 4

Bin 65: 25 of cap free
Amount of items: 5
Items: 
Size: 1022 Color: 0
Size: 713 Color: 0
Size: 100 Color: 2
Size: 98 Color: 1
Size: 78 Color: 3

Bin 66: 1710 of cap free
Amount of items: 8
Items: 
Size: 48 Color: 4
Size: 48 Color: 1
Size: 40 Color: 4
Size: 40 Color: 2
Size: 40 Color: 2
Size: 40 Color: 0
Size: 38 Color: 1
Size: 32 Color: 0

Total size: 132340
Total free space: 2036

