Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 443
Size: 286 Color: 190
Size: 264 Color: 102

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 495
Size: 254 Color: 42
Size: 251 Color: 15

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 415
Size: 300 Color: 238
Size: 274 Color: 156

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 438
Size: 286 Color: 189
Size: 269 Color: 129

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 429
Size: 312 Color: 256
Size: 250 Color: 2

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 427
Size: 302 Color: 244
Size: 261 Color: 84

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 419
Size: 307 Color: 250
Size: 266 Color: 114

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 404
Size: 313 Color: 258
Size: 274 Color: 151

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 442
Size: 299 Color: 235
Size: 251 Color: 14

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 483
Size: 270 Color: 138
Size: 250 Color: 11

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 420
Size: 303 Color: 245
Size: 269 Color: 132

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 307
Size: 345 Color: 299
Size: 305 Color: 246

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 376
Size: 338 Color: 294
Size: 275 Color: 157

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 486
Size: 259 Color: 73
Size: 255 Color: 50

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 464
Size: 268 Color: 128
Size: 268 Color: 125

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 450
Size: 287 Color: 196
Size: 258 Color: 71

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 351
Size: 370 Color: 350
Size: 259 Color: 76

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 423
Size: 317 Color: 264
Size: 250 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 445
Size: 297 Color: 229
Size: 250 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 487
Size: 260 Color: 79
Size: 254 Color: 46

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 489
Size: 263 Color: 95
Size: 250 Color: 3

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 359
Size: 325 Color: 278
Size: 298 Color: 232

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 392
Size: 340 Color: 297
Size: 266 Color: 111

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 453
Size: 275 Color: 159
Size: 269 Color: 131

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 364
Size: 349 Color: 306
Size: 272 Color: 144

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 431
Size: 308 Color: 251
Size: 253 Color: 37

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 361
Size: 368 Color: 342
Size: 255 Color: 54

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 327
Size: 354 Color: 317
Size: 288 Color: 200

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 380
Size: 334 Color: 288
Size: 278 Color: 169

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 398
Size: 332 Color: 287
Size: 261 Color: 85

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 390
Size: 341 Color: 298
Size: 266 Color: 113

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 309
Size: 350 Color: 308
Size: 300 Color: 240

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 412
Size: 325 Color: 277
Size: 253 Color: 31

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 492
Size: 254 Color: 41
Size: 253 Color: 34

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 425
Size: 291 Color: 210
Size: 274 Color: 153

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 330
Size: 357 Color: 323
Size: 282 Color: 181

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 401
Size: 327 Color: 281
Size: 263 Color: 97

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 373
Size: 313 Color: 257
Size: 302 Color: 241

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 447
Size: 276 Color: 165
Size: 270 Color: 139

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 457
Size: 279 Color: 172
Size: 264 Color: 100

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 424
Size: 309 Color: 253
Size: 257 Color: 63

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 418
Size: 294 Color: 220
Size: 279 Color: 170

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 363
Size: 355 Color: 319
Size: 267 Color: 122

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 414
Size: 296 Color: 224
Size: 280 Color: 174

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 451
Size: 293 Color: 216
Size: 252 Color: 24

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 466
Size: 273 Color: 149
Size: 262 Color: 90

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 389
Size: 357 Color: 322
Size: 252 Color: 27

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 352
Size: 354 Color: 318
Size: 273 Color: 150

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 455
Size: 292 Color: 212
Size: 251 Color: 18

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 355
Size: 358 Color: 326
Size: 269 Color: 135

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 459
Size: 278 Color: 168
Size: 265 Color: 104

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 417
Size: 324 Color: 274
Size: 250 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 476
Size: 273 Color: 148
Size: 254 Color: 43

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 499
Size: 252 Color: 28
Size: 250 Color: 6

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 454
Size: 283 Color: 182
Size: 261 Color: 83

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 500
Size: 252 Color: 23
Size: 250 Color: 8

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 421
Size: 289 Color: 202
Size: 282 Color: 179

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 461
Size: 287 Color: 194
Size: 253 Color: 33

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 374
Size: 345 Color: 300
Size: 269 Color: 136

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 360
Size: 368 Color: 343
Size: 255 Color: 51

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 387
Size: 334 Color: 289
Size: 275 Color: 160

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 472
Size: 279 Color: 171
Size: 251 Color: 20

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 369
Size: 322 Color: 272
Size: 298 Color: 233

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 441
Size: 294 Color: 218
Size: 256 Color: 59

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 471
Size: 267 Color: 121
Size: 263 Color: 96

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 474
Size: 272 Color: 142
Size: 256 Color: 58

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 386
Size: 315 Color: 262
Size: 294 Color: 217

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 460
Size: 282 Color: 178
Size: 259 Color: 77

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 488
Size: 261 Color: 86
Size: 252 Color: 22

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 394
Size: 339 Color: 296
Size: 266 Color: 116

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 367
Size: 363 Color: 332
Size: 257 Color: 67

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 378
Size: 315 Color: 261
Size: 297 Color: 227

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 439
Size: 284 Color: 184
Size: 270 Color: 137

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 335
Size: 365 Color: 334
Size: 269 Color: 133

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 372
Size: 318 Color: 266
Size: 298 Color: 231

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 458
Size: 286 Color: 187
Size: 257 Color: 66

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 494
Size: 255 Color: 52
Size: 251 Color: 19

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 400
Size: 320 Color: 269
Size: 271 Color: 140

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 315
Size: 352 Color: 313
Size: 296 Color: 223

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 436
Size: 290 Color: 208
Size: 266 Color: 117

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 482
Size: 265 Color: 106
Size: 256 Color: 56

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 444
Size: 298 Color: 230
Size: 250 Color: 10

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 311
Size: 350 Color: 310
Size: 299 Color: 237

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 491
Size: 258 Color: 70
Size: 250 Color: 5

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 479
Size: 264 Color: 101
Size: 260 Color: 80

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 449
Size: 290 Color: 205
Size: 255 Color: 55

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 470
Size: 272 Color: 143
Size: 258 Color: 68

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 399
Size: 318 Color: 268
Size: 275 Color: 162

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 368
Size: 348 Color: 304
Size: 272 Color: 145

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 406
Size: 329 Color: 284
Size: 256 Color: 60

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 497
Size: 254 Color: 40
Size: 251 Color: 17

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 493
Size: 257 Color: 64
Size: 250 Color: 9

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 329
Size: 359 Color: 328
Size: 281 Color: 177

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 348
Size: 368 Color: 341
Size: 262 Color: 88

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 382
Size: 324 Color: 273
Size: 287 Color: 193

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 437
Size: 290 Color: 207
Size: 266 Color: 120

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 365
Size: 370 Color: 349
Size: 251 Color: 16

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 409
Size: 295 Color: 221
Size: 288 Color: 201

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 496
Size: 255 Color: 47
Size: 250 Color: 1

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 456
Size: 286 Color: 188
Size: 257 Color: 65

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 422
Size: 295 Color: 222
Size: 272 Color: 146

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 481
Size: 269 Color: 134
Size: 254 Color: 44

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 333
Size: 346 Color: 301
Size: 290 Color: 206

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 410
Size: 329 Color: 282
Size: 253 Color: 29

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 379
Size: 335 Color: 290
Size: 277 Color: 166

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 426
Size: 306 Color: 248
Size: 258 Color: 69

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 435
Size: 288 Color: 197
Size: 268 Color: 126

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 428
Size: 308 Color: 252
Size: 255 Color: 53

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 480
Size: 267 Color: 124
Size: 256 Color: 61

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 465
Size: 268 Color: 127
Size: 267 Color: 123

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 371
Size: 321 Color: 271
Size: 298 Color: 234

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 366
Size: 356 Color: 320
Size: 265 Color: 105

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 339
Size: 367 Color: 336
Size: 265 Color: 110

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 375
Size: 357 Color: 321
Size: 256 Color: 57

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 405
Size: 330 Color: 285
Size: 255 Color: 48

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 469
Size: 266 Color: 118
Size: 266 Color: 112

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 303
Size: 346 Color: 302
Size: 306 Color: 249

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 403
Size: 297 Color: 228
Size: 291 Color: 209

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 413
Size: 302 Color: 242
Size: 276 Color: 164

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 338
Size: 358 Color: 325
Size: 274 Color: 152

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 340
Size: 368 Color: 337
Size: 264 Color: 103

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 416
Size: 315 Color: 259
Size: 259 Color: 75

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 391
Size: 325 Color: 276
Size: 281 Color: 176

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 381
Size: 325 Color: 275
Size: 287 Color: 192

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 397
Size: 318 Color: 267
Size: 276 Color: 163

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 383
Size: 317 Color: 265
Size: 294 Color: 219

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 388
Size: 329 Color: 283
Size: 280 Color: 173

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 377
Size: 338 Color: 293
Size: 274 Color: 154

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 385
Size: 320 Color: 270
Size: 290 Color: 204

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 362
Size: 325 Color: 279
Size: 297 Color: 226

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 408
Size: 293 Color: 215
Size: 290 Color: 203

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 358
Size: 337 Color: 292
Size: 288 Color: 198

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 370
Size: 317 Color: 263
Size: 302 Color: 243

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 357
Size: 373 Color: 354
Size: 252 Color: 25

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 356
Size: 348 Color: 305
Size: 278 Color: 167

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 353
Size: 315 Color: 260
Size: 312 Color: 255

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 407
Size: 299 Color: 236
Size: 284 Color: 185

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 347
Size: 369 Color: 345
Size: 261 Color: 87

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 346
Size: 369 Color: 344
Size: 262 Color: 89

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 331
Size: 353 Color: 316
Size: 286 Color: 191

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 314
Size: 352 Color: 312
Size: 296 Color: 225

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 396
Size: 310 Color: 254
Size: 291 Color: 211

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 393
Size: 339 Color: 295
Size: 266 Color: 115

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 395
Size: 337 Color: 291
Size: 265 Color: 107

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 402
Size: 331 Color: 286
Size: 259 Color: 74

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 411
Size: 327 Color: 280
Size: 253 Color: 32

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 430
Size: 300 Color: 239
Size: 262 Color: 91

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 432
Size: 288 Color: 199
Size: 271 Color: 141

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 433
Size: 292 Color: 213
Size: 265 Color: 108

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 434
Size: 305 Color: 247
Size: 252 Color: 26

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 440
Size: 282 Color: 180
Size: 269 Color: 130

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 446
Size: 287 Color: 195
Size: 260 Color: 78

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 452
Size: 283 Color: 183
Size: 262 Color: 93

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 448
Size: 292 Color: 214
Size: 253 Color: 35

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 384
Size: 358 Color: 324
Size: 252 Color: 21

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 462
Size: 284 Color: 186
Size: 255 Color: 49

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 463
Size: 280 Color: 175
Size: 257 Color: 62

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 468
Size: 272 Color: 147
Size: 262 Color: 94

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 467
Size: 274 Color: 155
Size: 261 Color: 81

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 473
Size: 265 Color: 109
Size: 264 Color: 99

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 475
Size: 275 Color: 161
Size: 253 Color: 39

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 477
Size: 266 Color: 119
Size: 261 Color: 82

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 478
Size: 275 Color: 158
Size: 251 Color: 13

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 484
Size: 263 Color: 98
Size: 253 Color: 38

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 485
Size: 262 Color: 92
Size: 253 Color: 30

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 490
Size: 258 Color: 72
Size: 254 Color: 45

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 498
Size: 253 Color: 36
Size: 250 Color: 7

Total size: 167000
Total free space: 0

