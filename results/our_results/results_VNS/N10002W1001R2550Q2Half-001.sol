Capicity Bin: 1001
Lower Bound: 3334

Bins used: 3335
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 322 Color: 1
Size: 268 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 286 Color: 1
Size: 252 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 336 Color: 1
Size: 277 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 321 Color: 1
Size: 260 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 312 Color: 1
Size: 260 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 357 Color: 1
Size: 272 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 284 Color: 1
Size: 259 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 355 Color: 1
Size: 280 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 346 Color: 1
Size: 286 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 314 Color: 1
Size: 257 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 304 Color: 1
Size: 264 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 287 Color: 1
Size: 257 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 316 Color: 1
Size: 295 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 309 Color: 0
Size: 307 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 326 Color: 1
Size: 280 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 312 Color: 1
Size: 256 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 321 Color: 0
Size: 304 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 278 Color: 1
Size: 256 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 328 Color: 1
Size: 268 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 316 Color: 1
Size: 299 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 288 Color: 1
Size: 262 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 335 Color: 1
Size: 267 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 324 Color: 1
Size: 278 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 261 Color: 1
Size: 250 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 342 Color: 1
Size: 253 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 266 Color: 1
Size: 250 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 324 Color: 0
Size: 316 Color: 1

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 292 Color: 1
Size: 264 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 297 Color: 1
Size: 253 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 351 Color: 1
Size: 285 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 310 Color: 1
Size: 298 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 299 Color: 1
Size: 261 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 307 Color: 1
Size: 253 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 359 Color: 1
Size: 273 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 251 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 339 Color: 1
Size: 269 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 266 Color: 1
Size: 260 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 344 Color: 1
Size: 279 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 281 Color: 1
Size: 273 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 362 Color: 1
Size: 254 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 331 Color: 1
Size: 259 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 343 Color: 1
Size: 276 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 344 Color: 1
Size: 264 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 313 Color: 1
Size: 295 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 1
Size: 344 Color: 1
Size: 309 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 321 Color: 1
Size: 286 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 347 Color: 1
Size: 265 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 341 Color: 1
Size: 333 Color: 1
Size: 327 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 276 Color: 1
Size: 271 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 355 Color: 1
Size: 271 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 302 Color: 1
Size: 255 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 288 Color: 1
Size: 268 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 256 Color: 1
Size: 250 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 333 Color: 1
Size: 304 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 288 Color: 1
Size: 284 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 267 Color: 1
Size: 252 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 305 Color: 1
Size: 268 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 326 Color: 1
Size: 298 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 286 Color: 0
Size: 284 Color: 1

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 348 Color: 1
Size: 298 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 297 Color: 1
Size: 289 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 367 Color: 1
Size: 266 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 346 Color: 1
Size: 269 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 341 Color: 1
Size: 270 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 336 Color: 1
Size: 257 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 359 Color: 1
Size: 264 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 326 Color: 1
Size: 309 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 306 Color: 0
Size: 288 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 338 Color: 1
Size: 275 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 272 Color: 1
Size: 262 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 297 Color: 1
Size: 251 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 313 Color: 1
Size: 298 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 269 Color: 1
Size: 254 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 338 Color: 1
Size: 285 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 306 Color: 1
Size: 258 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 277 Color: 1
Size: 263 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 319 Color: 0
Size: 289 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 352 Color: 1
Size: 288 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 344 Color: 1
Size: 284 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 339 Color: 1
Size: 261 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 306 Color: 1
Size: 277 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 273 Color: 1
Size: 267 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 310 Color: 1
Size: 259 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 297 Color: 1
Size: 263 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 356 Color: 1
Size: 254 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 288 Color: 1
Size: 253 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 302 Color: 1
Size: 263 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 301 Color: 1
Size: 278 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 260 Color: 1
Size: 259 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 297 Color: 1
Size: 275 Color: 0

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 281 Color: 1
Size: 254 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 285 Color: 1
Size: 257 Color: 0

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 352 Color: 1
Size: 264 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 301 Color: 1
Size: 263 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 334 Color: 1
Size: 297 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 262 Color: 1
Size: 258 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 328 Color: 1
Size: 271 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 327 Color: 1
Size: 261 Color: 0

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 256 Color: 1
Size: 250 Color: 0

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 319 Color: 1
Size: 276 Color: 0

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 299 Color: 1
Size: 277 Color: 0

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 333 Color: 1
Size: 303 Color: 0

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 293 Color: 1
Size: 252 Color: 0

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 327 Color: 1
Size: 306 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 1
Size: 251 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 330 Color: 1
Size: 258 Color: 0

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 330 Color: 1
Size: 293 Color: 0

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 258 Color: 1
Size: 250 Color: 0

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 325 Color: 1
Size: 269 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 320 Color: 1
Size: 320 Color: 0

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 299 Color: 1
Size: 293 Color: 0

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 313 Color: 1
Size: 251 Color: 0

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 279 Color: 1
Size: 270 Color: 0

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 317 Color: 1
Size: 306 Color: 0

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 300 Color: 1
Size: 255 Color: 0

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 349 Color: 1
Size: 287 Color: 0

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 336 Color: 1
Size: 287 Color: 0

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 281 Color: 1
Size: 264 Color: 0

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 278 Color: 1
Size: 259 Color: 0

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 294 Color: 1
Size: 251 Color: 0

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 262 Color: 1
Size: 251 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 344 Color: 1
Size: 271 Color: 0

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 333 Color: 1
Size: 270 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 325 Color: 1
Size: 283 Color: 0

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 361 Color: 1
Size: 271 Color: 0

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 292 Color: 1
Size: 260 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 279 Color: 1
Size: 267 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 260 Color: 1
Size: 250 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 331 Color: 1
Size: 317 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 306 Color: 1
Size: 286 Color: 0

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 270 Color: 1
Size: 258 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 0
Size: 250 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 316 Color: 1
Size: 275 Color: 0

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 279 Color: 1
Size: 270 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 290 Color: 0
Size: 276 Color: 1

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 310 Color: 1
Size: 253 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 311 Color: 1
Size: 254 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 266 Color: 1
Size: 253 Color: 0

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 333 Color: 1
Size: 288 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 320 Color: 1
Size: 295 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 265 Color: 1
Size: 259 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 350 Color: 1
Size: 254 Color: 0

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 316 Color: 1
Size: 295 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 318 Color: 1
Size: 257 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 251 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 276 Color: 1
Size: 254 Color: 0

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 348 Color: 1
Size: 269 Color: 0

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 305 Color: 1
Size: 260 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 344 Color: 1
Size: 271 Color: 0

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 258 Color: 0
Size: 254 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 350 Color: 1
Size: 286 Color: 0

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 308 Color: 0
Size: 284 Color: 1

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 336 Color: 1
Size: 270 Color: 0

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 347 Color: 1
Size: 277 Color: 0

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 301 Color: 0
Size: 290 Color: 1

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 339 Color: 1
Size: 269 Color: 0

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 357 Color: 1
Size: 274 Color: 0

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 276 Color: 1
Size: 251 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 272 Color: 1
Size: 251 Color: 0

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 296 Color: 1
Size: 255 Color: 0

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 349 Color: 1
Size: 275 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 338 Color: 1
Size: 256 Color: 0

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 322 Color: 1
Size: 263 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 273 Color: 1
Size: 250 Color: 0

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 351 Color: 1
Size: 287 Color: 0

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 307 Color: 1
Size: 305 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 329 Color: 1
Size: 286 Color: 0

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 301 Color: 1
Size: 258 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 339 Color: 1
Size: 289 Color: 0

Bin 174: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 301 Color: 1
Size: 263 Color: 0

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 313 Color: 1
Size: 278 Color: 0

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 342 Color: 1
Size: 284 Color: 0

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 262 Color: 1
Size: 250 Color: 0

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 251 Color: 0

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 303 Color: 1
Size: 266 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 329 Color: 1
Size: 297 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 280 Color: 1
Size: 252 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 331 Color: 1
Size: 265 Color: 0

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 346 Color: 1
Size: 267 Color: 0

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 362 Color: 1
Size: 263 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 320 Color: 1
Size: 265 Color: 0

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 342 Color: 1
Size: 278 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 331 Color: 1
Size: 266 Color: 0

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 330 Color: 1
Size: 271 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 314 Color: 1
Size: 253 Color: 0

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 344 Color: 1
Size: 292 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 359 Color: 1
Size: 271 Color: 0

Bin 194: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 301 Color: 1
Size: 251 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 304 Color: 1
Size: 258 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 327 Color: 1
Size: 258 Color: 0

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 340 Color: 1
Size: 270 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 349 Color: 1
Size: 287 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 258 Color: 1
Size: 255 Color: 0

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 357 Color: 1
Size: 258 Color: 0

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 254 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 313 Color: 0
Size: 310 Color: 1

Bin 203: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 323 Color: 1
Size: 265 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 301 Color: 1
Size: 252 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 344 Color: 1
Size: 270 Color: 0

Bin 206: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 320 Color: 1
Size: 308 Color: 0

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 324 Color: 1
Size: 278 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 276 Color: 1
Size: 275 Color: 0

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 322 Color: 0
Size: 320 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 292 Color: 1
Size: 260 Color: 0

Bin 212: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 356 Color: 1
Size: 271 Color: 0

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 324 Color: 1
Size: 258 Color: 0

Bin 214: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 318 Color: 1
Size: 306 Color: 0

Bin 215: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 321 Color: 1
Size: 273 Color: 0

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 358 Color: 1
Size: 260 Color: 0

Bin 217: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 326 Color: 1
Size: 279 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 352 Color: 1
Size: 286 Color: 0

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 321 Color: 1
Size: 254 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 361 Color: 1
Size: 257 Color: 0

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 278 Color: 1
Size: 258 Color: 0

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 340 Color: 1
Size: 277 Color: 0

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 314 Color: 1
Size: 261 Color: 0

Bin 224: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 322 Color: 1
Size: 265 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 273 Color: 1
Size: 263 Color: 0

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 281 Color: 1
Size: 253 Color: 0

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 1
Size: 251 Color: 0

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 294 Color: 0
Size: 291 Color: 1

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 291 Color: 1
Size: 265 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 268 Color: 1
Size: 255 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 342 Color: 1
Size: 269 Color: 0

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 280 Color: 0
Size: 261 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 301 Color: 1
Size: 258 Color: 0

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 312 Color: 1
Size: 256 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 319 Color: 1
Size: 272 Color: 0

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 316 Color: 1
Size: 254 Color: 0

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 357 Color: 1
Size: 268 Color: 0

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 288 Color: 1
Size: 279 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 336 Color: 1
Size: 301 Color: 0

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 295 Color: 1
Size: 285 Color: 0

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 354 Color: 1
Size: 293 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 265 Color: 1
Size: 252 Color: 0

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 309 Color: 1
Size: 305 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 273 Color: 0
Size: 270 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 311 Color: 1
Size: 280 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 350 Color: 1
Size: 285 Color: 0

Bin 247: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 273 Color: 1
Size: 252 Color: 0

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 329 Color: 1
Size: 279 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 284 Color: 1
Size: 270 Color: 0

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 320 Color: 1
Size: 314 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 341 Color: 1
Size: 266 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 366 Color: 1
Size: 259 Color: 0

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 348 Color: 1
Size: 300 Color: 0

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 277 Color: 1
Size: 262 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 326 Color: 1
Size: 308 Color: 0

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 331 Color: 1
Size: 288 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 308 Color: 1
Size: 253 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 334 Color: 1
Size: 270 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 264 Color: 1
Size: 253 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 1
Size: 255 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 315 Color: 1
Size: 261 Color: 0

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 309 Color: 1
Size: 253 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 333 Color: 1
Size: 270 Color: 0

Bin 264: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 337 Color: 1
Size: 294 Color: 0

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 334 Color: 1
Size: 316 Color: 0

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 298 Color: 1
Size: 267 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 274 Color: 1
Size: 264 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 315 Color: 1
Size: 256 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 281 Color: 1
Size: 254 Color: 0

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 330 Color: 1
Size: 308 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 333 Color: 1
Size: 308 Color: 0

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 279 Color: 1
Size: 266 Color: 0

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 283 Color: 1
Size: 274 Color: 0

Bin 274: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 268 Color: 0
Size: 265 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 1
Size: 252 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 271 Color: 0
Size: 269 Color: 1

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 1
Size: 251 Color: 0

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 345 Color: 1
Size: 290 Color: 0

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 294 Color: 0
Size: 290 Color: 1

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 274 Color: 1
Size: 266 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 261 Color: 1
Size: 252 Color: 0

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 325 Color: 1
Size: 271 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 269 Color: 1
Size: 254 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 284 Color: 0
Size: 279 Color: 1

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 339 Color: 1
Size: 300 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 311 Color: 0
Size: 294 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 325 Color: 1
Size: 280 Color: 0

Bin 289: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 345 Color: 1
Size: 274 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 274 Color: 0
Size: 272 Color: 1

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 313 Color: 1
Size: 267 Color: 0

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 344 Color: 1
Size: 296 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 351 Color: 1
Size: 264 Color: 0

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 327 Color: 1
Size: 262 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 286 Color: 1
Size: 265 Color: 0

Bin 296: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 258 Color: 1
Size: 258 Color: 0

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 320 Color: 1
Size: 297 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 341 Color: 1
Size: 256 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 278 Color: 1
Size: 264 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 337 Color: 1
Size: 258 Color: 0

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 352 Color: 1
Size: 275 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 307 Color: 1
Size: 288 Color: 0

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 302 Color: 1
Size: 259 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 325 Color: 1
Size: 319 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 318 Color: 1
Size: 258 Color: 0

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 288 Color: 1
Size: 255 Color: 0

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 337 Color: 1
Size: 298 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 0
Size: 252 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 292 Color: 1
Size: 264 Color: 0

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 296 Color: 1
Size: 278 Color: 0

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 251 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 288 Color: 1
Size: 287 Color: 0

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 332 Color: 1
Size: 289 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 336 Color: 1
Size: 291 Color: 0

Bin 315: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 355 Color: 1
Size: 257 Color: 0

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 319 Color: 1
Size: 291 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 367 Color: 1
Size: 256 Color: 0

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 314 Color: 1
Size: 285 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 344 Color: 1
Size: 274 Color: 0

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 297 Color: 1
Size: 267 Color: 0

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 305 Color: 1
Size: 274 Color: 0

Bin 322: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 296 Color: 1
Size: 281 Color: 0

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 325 Color: 1
Size: 299 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 289 Color: 1
Size: 258 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 328 Color: 1
Size: 263 Color: 0

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 276 Color: 1
Size: 250 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 353 Color: 1
Size: 280 Color: 0

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 295 Color: 1
Size: 292 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 292 Color: 1
Size: 251 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 315 Color: 0
Size: 292 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 345 Color: 1
Size: 277 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 299 Color: 1
Size: 257 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 294 Color: 0
Size: 288 Color: 1

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 336 Color: 1
Size: 297 Color: 0

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 314 Color: 1
Size: 264 Color: 0

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 313 Color: 1
Size: 274 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 260 Color: 0
Size: 257 Color: 1

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 351 Color: 1
Size: 260 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 307 Color: 1
Size: 305 Color: 0

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 279 Color: 1
Size: 278 Color: 0

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 287 Color: 1
Size: 277 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 316 Color: 1
Size: 266 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 317 Color: 1
Size: 264 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 324 Color: 1
Size: 311 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 320 Color: 1
Size: 268 Color: 0

Bin 348: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 344 Color: 1
Size: 285 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 348 Color: 1
Size: 260 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 322 Color: 1
Size: 279 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 275 Color: 1
Size: 251 Color: 0

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 336 Color: 1
Size: 262 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 356 Color: 1
Size: 275 Color: 0

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 290 Color: 0
Size: 266 Color: 1

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 318 Color: 0
Size: 314 Color: 1

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 286 Color: 0
Size: 268 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 313 Color: 1
Size: 262 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 322 Color: 1
Size: 282 Color: 0

Bin 359: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 347 Color: 1
Size: 268 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 320 Color: 1
Size: 317 Color: 0

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 355 Color: 1
Size: 270 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 308 Color: 1
Size: 263 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 290 Color: 1
Size: 276 Color: 0

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 278 Color: 1
Size: 250 Color: 0

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 326 Color: 1
Size: 316 Color: 0

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 254 Color: 1
Size: 253 Color: 0

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 355 Color: 1
Size: 265 Color: 0

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 343 Color: 1
Size: 296 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 284 Color: 1
Size: 258 Color: 0

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 336 Color: 1
Size: 304 Color: 0

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 314 Color: 1
Size: 266 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 265 Color: 1
Size: 257 Color: 0

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 285 Color: 1
Size: 252 Color: 0

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 333 Color: 1
Size: 313 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 313 Color: 1
Size: 258 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 275 Color: 1
Size: 268 Color: 0

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 316 Color: 1
Size: 254 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 309 Color: 0
Size: 289 Color: 1

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 0
Size: 251 Color: 1

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 327 Color: 1
Size: 323 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 334 Color: 1
Size: 282 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 314 Color: 1
Size: 269 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 294 Color: 1
Size: 261 Color: 0

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 352 Color: 1
Size: 287 Color: 0

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 323 Color: 1
Size: 268 Color: 0

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 265 Color: 1
Size: 252 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 284 Color: 1
Size: 266 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 294 Color: 1
Size: 263 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 356 Color: 1
Size: 264 Color: 0

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 0
Size: 252 Color: 1

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 285 Color: 1
Size: 252 Color: 0

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 332 Color: 1
Size: 307 Color: 0

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 309 Color: 1
Size: 277 Color: 0

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 338 Color: 1
Size: 288 Color: 0

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 357 Color: 1
Size: 259 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 334 Color: 1
Size: 303 Color: 0

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 313 Color: 0
Size: 300 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 374 Color: 1
Size: 250 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 335 Color: 1
Size: 268 Color: 0

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 350 Color: 1
Size: 298 Color: 0

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 324 Color: 1
Size: 256 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 264 Color: 1
Size: 253 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 350 Color: 1
Size: 271 Color: 0

Bin 408: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 1
Size: 251 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 282 Color: 1
Size: 253 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 304 Color: 1
Size: 255 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 345 Color: 1
Size: 266 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 359 Color: 1
Size: 253 Color: 0

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 318 Color: 1
Size: 278 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 344 Color: 1
Size: 273 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 328 Color: 1
Size: 305 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 283 Color: 1
Size: 250 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 311 Color: 1
Size: 305 Color: 0

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 335 Color: 1
Size: 300 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 332 Color: 1
Size: 289 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 330 Color: 1
Size: 274 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 314 Color: 1
Size: 263 Color: 0

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 340 Color: 1
Size: 258 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 335 Color: 1
Size: 280 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 291 Color: 1
Size: 266 Color: 0

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 339 Color: 1
Size: 281 Color: 0

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 315 Color: 1
Size: 293 Color: 0

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 317 Color: 1
Size: 299 Color: 0

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 347 Color: 1
Size: 279 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 355 Color: 1
Size: 270 Color: 0

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 285 Color: 1
Size: 271 Color: 0

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 310 Color: 1
Size: 282 Color: 0

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 301 Color: 1
Size: 288 Color: 0

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 325 Color: 1
Size: 256 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 295 Color: 1
Size: 255 Color: 0

Bin 436: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 329 Color: 1
Size: 301 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 320 Color: 1
Size: 318 Color: 0

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 314 Color: 1
Size: 307 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 296 Color: 1
Size: 260 Color: 0

Bin 441: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 1
Size: 253 Color: 0

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 318 Color: 1
Size: 266 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 347 Color: 1
Size: 299 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 251 Color: 0

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 272 Color: 1
Size: 264 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 318 Color: 0
Size: 282 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 323 Color: 1
Size: 253 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 346 Color: 1
Size: 294 Color: 0

Bin 450: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 285 Color: 1
Size: 259 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 310 Color: 0
Size: 297 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 309 Color: 0
Size: 301 Color: 1

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 358 Color: 1
Size: 285 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 316 Color: 0
Size: 288 Color: 1

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 330 Color: 1
Size: 305 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 318 Color: 1
Size: 294 Color: 0

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 267 Color: 1
Size: 252 Color: 0

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 273 Color: 1
Size: 257 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 350 Color: 1
Size: 267 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 1
Size: 251 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 288 Color: 1
Size: 261 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 263 Color: 1
Size: 252 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 311 Color: 1
Size: 294 Color: 0

Bin 464: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 258 Color: 1
Size: 250 Color: 0

Bin 465: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 359 Color: 1
Size: 271 Color: 0

Bin 466: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 355 Color: 1
Size: 269 Color: 0

Bin 467: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 344 Color: 1
Size: 278 Color: 0

Bin 468: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 260 Color: 1
Size: 250 Color: 0

Bin 469: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 336 Color: 1
Size: 276 Color: 0

Bin 470: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 302 Color: 1
Size: 265 Color: 0

Bin 471: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 269 Color: 1
Size: 253 Color: 0

Bin 472: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 318 Color: 1
Size: 318 Color: 0

Bin 473: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 263 Color: 0
Size: 256 Color: 1

Bin 474: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 323 Color: 1
Size: 308 Color: 0

Bin 475: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 300 Color: 0
Size: 283 Color: 1

Bin 476: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 348 Color: 1
Size: 277 Color: 0

Bin 477: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 478: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 323 Color: 1
Size: 307 Color: 0

Bin 479: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 279 Color: 1
Size: 275 Color: 0

Bin 480: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 335 Color: 1
Size: 280 Color: 0

Bin 481: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 1
Size: 251 Color: 0

Bin 482: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 334 Color: 1
Size: 266 Color: 0

Bin 483: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 319 Color: 0
Size: 289 Color: 1

Bin 484: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 324 Color: 1
Size: 323 Color: 0

Bin 485: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 351 Color: 1
Size: 272 Color: 0

Bin 486: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 342 Color: 1
Size: 281 Color: 0

Bin 487: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 272 Color: 1
Size: 260 Color: 0

Bin 488: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 272 Color: 1
Size: 250 Color: 0

Bin 489: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 354 Color: 1
Size: 271 Color: 0

Bin 490: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 301 Color: 1
Size: 254 Color: 0

Bin 491: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 300 Color: 1
Size: 260 Color: 0

Bin 492: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 326 Color: 1
Size: 300 Color: 0

Bin 493: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 329 Color: 1
Size: 259 Color: 0

Bin 494: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 1
Size: 252 Color: 0

Bin 495: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 283 Color: 1
Size: 253 Color: 0

Bin 496: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 1
Size: 330 Color: 1
Size: 323 Color: 0

Bin 497: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 346 Color: 1
Size: 283 Color: 0

Bin 498: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 251 Color: 0

Bin 499: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 324 Color: 1
Size: 254 Color: 0

Bin 500: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 278 Color: 1
Size: 250 Color: 0

Bin 501: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 333 Color: 1
Size: 258 Color: 0

Bin 502: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 321 Color: 1
Size: 257 Color: 0

Bin 503: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 267 Color: 1
Size: 251 Color: 0

Bin 504: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 313 Color: 1
Size: 251 Color: 0

Bin 505: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 325 Color: 0
Size: 319 Color: 1

Bin 506: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 327 Color: 1
Size: 269 Color: 0

Bin 507: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 278 Color: 1
Size: 268 Color: 0

Bin 508: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 350 Color: 1
Size: 278 Color: 0

Bin 509: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 321 Color: 1
Size: 259 Color: 0

Bin 510: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 325 Color: 1
Size: 277 Color: 0

Bin 511: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 308 Color: 1
Size: 288 Color: 0

Bin 512: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 279 Color: 1
Size: 257 Color: 0

Bin 513: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 290 Color: 1
Size: 250 Color: 0

Bin 514: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 317 Color: 1
Size: 277 Color: 0

Bin 515: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 315 Color: 1
Size: 264 Color: 0

Bin 516: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 336 Color: 1
Size: 264 Color: 0

Bin 517: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 346 Color: 1
Size: 274 Color: 0

Bin 518: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 320 Color: 1
Size: 256 Color: 0

Bin 519: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 304 Color: 1
Size: 261 Color: 0

Bin 520: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 344 Color: 1
Size: 286 Color: 0

Bin 521: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 312 Color: 1
Size: 271 Color: 0

Bin 522: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 296 Color: 1
Size: 270 Color: 0

Bin 523: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 336 Color: 1
Size: 274 Color: 0

Bin 524: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 358 Color: 1
Size: 272 Color: 0

Bin 525: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 320 Color: 0
Size: 313 Color: 1

Bin 526: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 270 Color: 1
Size: 252 Color: 0

Bin 527: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 291 Color: 1
Size: 256 Color: 0

Bin 528: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 267 Color: 1
Size: 261 Color: 0

Bin 529: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 288 Color: 1
Size: 274 Color: 0

Bin 530: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 282 Color: 1
Size: 261 Color: 0

Bin 531: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 280 Color: 1
Size: 267 Color: 0

Bin 532: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 336 Color: 1
Size: 265 Color: 0

Bin 533: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 258 Color: 1
Size: 253 Color: 0

Bin 534: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 316 Color: 1
Size: 254 Color: 0

Bin 535: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 309 Color: 1
Size: 309 Color: 0

Bin 536: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 315 Color: 1
Size: 306 Color: 0

Bin 537: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 303 Color: 1
Size: 293 Color: 0

Bin 538: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 322 Color: 1
Size: 291 Color: 0

Bin 539: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 298 Color: 1
Size: 298 Color: 0

Bin 540: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 269 Color: 1
Size: 252 Color: 0

Bin 541: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 336 Color: 1
Size: 292 Color: 0

Bin 542: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 325 Color: 1
Size: 269 Color: 0

Bin 543: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 544: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 343 Color: 1
Size: 292 Color: 0

Bin 545: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 287 Color: 1
Size: 253 Color: 0

Bin 546: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 262 Color: 1
Size: 258 Color: 0

Bin 547: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 281 Color: 1
Size: 257 Color: 0

Bin 548: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 322 Color: 1
Size: 276 Color: 0

Bin 549: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 349 Color: 1
Size: 286 Color: 0

Bin 550: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 296 Color: 1
Size: 253 Color: 0

Bin 551: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 259 Color: 1
Size: 250 Color: 0

Bin 552: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 350 Color: 1
Size: 267 Color: 0

Bin 553: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 283 Color: 1
Size: 253 Color: 0

Bin 554: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 349 Color: 1
Size: 258 Color: 0

Bin 555: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 556: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 290 Color: 1
Size: 268 Color: 0

Bin 557: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 332 Color: 1
Size: 255 Color: 0

Bin 558: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 279 Color: 1
Size: 265 Color: 0

Bin 559: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 339 Color: 1
Size: 303 Color: 0

Bin 560: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 309 Color: 0
Size: 304 Color: 1

Bin 561: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 349 Color: 1
Size: 272 Color: 0

Bin 562: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 343 Color: 1
Size: 271 Color: 0

Bin 563: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 333 Color: 1
Size: 293 Color: 0

Bin 564: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 344 Color: 1
Size: 264 Color: 0

Bin 565: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 350 Color: 1
Size: 293 Color: 0

Bin 566: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 323 Color: 1
Size: 316 Color: 0

Bin 567: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 345 Color: 1
Size: 304 Color: 0

Bin 568: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 331 Color: 1
Size: 258 Color: 0

Bin 569: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 269 Color: 1
Size: 257 Color: 0

Bin 570: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 345 Color: 1
Size: 278 Color: 0

Bin 571: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 279 Color: 1
Size: 254 Color: 0

Bin 572: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 316 Color: 1
Size: 255 Color: 0

Bin 573: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 272 Color: 1
Size: 253 Color: 0

Bin 574: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 344 Color: 1
Size: 296 Color: 0

Bin 575: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 296 Color: 0
Size: 272 Color: 1

Bin 576: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 251 Color: 0

Bin 577: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 334 Color: 1
Size: 298 Color: 0

Bin 578: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 322 Color: 0
Size: 305 Color: 1

Bin 579: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 304 Color: 1
Size: 253 Color: 0

Bin 580: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 324 Color: 0
Size: 323 Color: 1

Bin 581: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 317 Color: 1
Size: 259 Color: 0

Bin 582: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 326 Color: 1
Size: 312 Color: 0

Bin 583: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 267 Color: 1
Size: 255 Color: 0

Bin 584: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 331 Color: 1
Size: 271 Color: 0

Bin 585: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 281 Color: 1
Size: 255 Color: 0

Bin 586: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 270 Color: 1
Size: 261 Color: 0

Bin 587: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 273 Color: 1
Size: 256 Color: 0

Bin 588: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 339 Color: 1
Size: 285 Color: 0

Bin 589: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 308 Color: 0
Size: 284 Color: 1

Bin 590: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 338 Color: 1
Size: 292 Color: 0

Bin 591: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 592: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 327 Color: 1
Size: 273 Color: 0

Bin 593: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 332 Color: 1
Size: 290 Color: 0

Bin 594: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 310 Color: 1
Size: 294 Color: 0

Bin 595: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 342 Color: 1
Size: 295 Color: 0

Bin 596: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 310 Color: 1
Size: 304 Color: 0

Bin 597: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 282 Color: 1
Size: 276 Color: 0

Bin 598: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 325 Color: 1
Size: 319 Color: 0

Bin 599: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 315 Color: 1
Size: 261 Color: 0

Bin 600: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 334 Color: 1
Size: 261 Color: 0

Bin 601: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 1
Size: 337 Color: 1
Size: 315 Color: 0

Bin 602: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 287 Color: 1
Size: 258 Color: 0

Bin 603: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 269 Color: 1
Size: 257 Color: 0

Bin 604: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 314 Color: 1
Size: 252 Color: 0

Bin 605: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 280 Color: 1
Size: 253 Color: 0

Bin 606: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 301 Color: 1
Size: 261 Color: 0

Bin 607: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 328 Color: 1
Size: 292 Color: 0

Bin 608: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 276 Color: 1
Size: 265 Color: 0

Bin 609: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 282 Color: 1
Size: 251 Color: 0

Bin 610: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 274 Color: 1
Size: 267 Color: 0

Bin 611: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 339 Color: 1
Size: 294 Color: 0

Bin 612: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 289 Color: 1
Size: 287 Color: 0

Bin 613: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 256 Color: 1
Size: 253 Color: 0

Bin 614: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 295 Color: 1
Size: 260 Color: 0

Bin 615: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 281 Color: 0
Size: 276 Color: 1

Bin 616: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 304 Color: 1
Size: 273 Color: 0

Bin 617: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 301 Color: 1
Size: 256 Color: 0

Bin 618: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 333 Color: 1
Size: 265 Color: 0

Bin 619: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 1
Size: 251 Color: 0

Bin 620: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 278 Color: 1
Size: 250 Color: 0

Bin 621: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 330 Color: 1
Size: 272 Color: 0

Bin 622: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 260 Color: 1
Size: 260 Color: 0

Bin 623: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 324 Color: 1
Size: 289 Color: 0

Bin 624: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 293 Color: 1
Size: 292 Color: 0

Bin 625: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 353 Color: 1
Size: 272 Color: 0

Bin 626: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 259 Color: 1
Size: 251 Color: 0

Bin 627: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 334 Color: 1
Size: 274 Color: 0

Bin 628: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 343 Color: 1
Size: 299 Color: 0

Bin 629: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 330 Color: 1
Size: 272 Color: 0

Bin 630: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 322 Color: 1
Size: 267 Color: 0

Bin 631: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 316 Color: 1
Size: 279 Color: 0

Bin 632: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 272 Color: 1
Size: 254 Color: 0

Bin 633: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 298 Color: 1
Size: 261 Color: 0

Bin 634: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 302 Color: 1
Size: 296 Color: 0

Bin 635: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 333 Color: 1
Size: 271 Color: 0

Bin 636: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 330 Color: 1
Size: 295 Color: 0

Bin 637: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 259 Color: 1
Size: 250 Color: 0

Bin 638: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 335 Color: 1
Size: 294 Color: 0

Bin 639: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 314 Color: 0
Size: 284 Color: 1

Bin 640: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 265 Color: 1
Size: 254 Color: 0

Bin 641: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 316 Color: 1
Size: 306 Color: 0

Bin 642: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 260 Color: 0
Size: 259 Color: 1

Bin 643: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 346 Color: 1
Size: 257 Color: 0

Bin 644: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 334 Color: 1
Size: 257 Color: 0

Bin 645: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 349 Color: 1
Size: 285 Color: 0

Bin 646: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 264 Color: 1
Size: 250 Color: 0

Bin 647: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 269 Color: 0
Size: 261 Color: 1

Bin 648: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 329 Color: 1
Size: 255 Color: 0

Bin 649: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 258 Color: 1
Size: 253 Color: 0

Bin 650: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 0
Size: 250 Color: 1

Bin 651: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 337 Color: 1
Size: 296 Color: 0

Bin 652: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 319 Color: 1
Size: 305 Color: 0

Bin 653: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 274 Color: 1
Size: 256 Color: 0

Bin 654: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 342 Color: 1
Size: 291 Color: 0

Bin 655: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 313 Color: 1
Size: 265 Color: 0

Bin 656: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 271 Color: 0
Size: 270 Color: 1

Bin 657: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 349 Color: 1
Size: 301 Color: 0

Bin 658: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 272 Color: 1
Size: 252 Color: 0

Bin 659: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 338 Color: 1
Size: 294 Color: 0

Bin 660: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 257 Color: 0
Size: 253 Color: 1

Bin 661: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 348 Color: 1
Size: 281 Color: 0

Bin 662: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 354 Color: 1
Size: 280 Color: 0

Bin 663: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 309 Color: 1
Size: 275 Color: 0

Bin 664: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 333 Color: 1
Size: 265 Color: 0

Bin 665: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 666: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 290 Color: 1
Size: 262 Color: 0

Bin 667: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 344 Color: 1
Size: 283 Color: 0

Bin 668: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 268 Color: 0
Size: 256 Color: 1

Bin 669: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 309 Color: 1
Size: 302 Color: 0

Bin 670: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 353 Color: 1
Size: 255 Color: 0

Bin 671: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 329 Color: 1
Size: 284 Color: 0

Bin 672: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 326 Color: 1
Size: 272 Color: 0

Bin 673: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 278 Color: 1
Size: 266 Color: 0

Bin 674: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 315 Color: 1
Size: 293 Color: 0

Bin 675: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 329 Color: 1
Size: 305 Color: 0

Bin 676: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 315 Color: 0
Size: 293 Color: 1

Bin 677: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 331 Color: 1
Size: 304 Color: 0

Bin 678: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 342 Color: 1
Size: 294 Color: 0

Bin 679: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 303 Color: 1
Size: 254 Color: 0

Bin 680: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 340 Color: 1
Size: 292 Color: 0

Bin 681: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 301 Color: 1
Size: 257 Color: 0

Bin 682: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 277 Color: 1
Size: 252 Color: 0

Bin 683: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 285 Color: 0
Size: 267 Color: 1

Bin 684: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 297 Color: 0
Size: 266 Color: 1

Bin 685: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 314 Color: 1
Size: 272 Color: 0

Bin 686: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 286 Color: 1
Size: 254 Color: 0

Bin 687: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 265 Color: 1
Size: 260 Color: 0

Bin 688: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 298 Color: 1
Size: 273 Color: 0

Bin 689: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 690: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 297 Color: 1
Size: 274 Color: 0

Bin 691: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 329 Color: 1
Size: 310 Color: 0

Bin 692: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 260 Color: 1
Size: 259 Color: 0

Bin 693: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 331 Color: 1
Size: 261 Color: 0

Bin 694: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 293 Color: 1
Size: 261 Color: 0

Bin 695: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 267 Color: 1
Size: 258 Color: 0

Bin 696: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 357 Color: 1
Size: 270 Color: 0

Bin 697: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 299 Color: 0
Size: 274 Color: 1

Bin 698: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 286 Color: 1
Size: 254 Color: 0

Bin 699: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 335 Color: 1
Size: 273 Color: 0

Bin 700: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 306 Color: 1
Size: 253 Color: 0

Bin 701: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 305 Color: 0
Size: 297 Color: 1

Bin 702: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 357 Color: 1
Size: 272 Color: 0

Bin 703: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 252 Color: 1
Size: 252 Color: 0

Bin 704: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 331 Color: 1
Size: 298 Color: 0

Bin 705: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 706: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 278 Color: 0
Size: 270 Color: 1

Bin 707: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 275 Color: 1
Size: 274 Color: 0

Bin 708: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 313 Color: 0
Size: 282 Color: 1

Bin 709: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 303 Color: 1
Size: 254 Color: 0

Bin 710: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 307 Color: 1
Size: 251 Color: 0

Bin 711: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 304 Color: 1
Size: 258 Color: 0

Bin 712: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 331 Color: 1
Size: 273 Color: 0

Bin 713: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 714: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 313 Color: 1
Size: 258 Color: 0

Bin 715: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 319 Color: 0
Size: 299 Color: 1

Bin 716: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 304 Color: 1
Size: 289 Color: 0

Bin 717: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 311 Color: 0
Size: 305 Color: 1

Bin 718: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 281 Color: 1
Size: 252 Color: 0

Bin 719: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 311 Color: 1
Size: 255 Color: 0

Bin 720: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 721: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 288 Color: 1
Size: 260 Color: 0

Bin 722: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 349 Color: 1
Size: 279 Color: 0

Bin 723: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 265 Color: 1
Size: 250 Color: 0

Bin 724: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 339 Color: 1
Size: 304 Color: 0

Bin 725: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 334 Color: 1
Size: 296 Color: 0

Bin 726: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 268 Color: 0
Size: 259 Color: 1

Bin 727: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 345 Color: 1
Size: 277 Color: 0

Bin 728: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 297 Color: 1
Size: 255 Color: 0

Bin 729: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 263 Color: 1
Size: 250 Color: 0

Bin 730: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 332 Color: 1
Size: 273 Color: 0

Bin 731: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 349 Color: 1
Size: 285 Color: 0

Bin 732: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 300 Color: 1
Size: 258 Color: 0

Bin 733: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 328 Color: 1
Size: 280 Color: 0

Bin 734: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 326 Color: 0
Size: 283 Color: 1

Bin 735: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 281 Color: 1
Size: 263 Color: 0

Bin 736: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 352 Color: 1
Size: 291 Color: 0

Bin 737: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 324 Color: 1
Size: 274 Color: 0

Bin 738: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 314 Color: 1
Size: 292 Color: 0

Bin 739: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 328 Color: 1
Size: 298 Color: 0

Bin 740: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 314 Color: 1
Size: 256 Color: 0

Bin 741: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 345 Color: 1
Size: 274 Color: 0

Bin 742: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 268 Color: 0
Size: 261 Color: 1

Bin 743: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 308 Color: 1
Size: 279 Color: 0

Bin 744: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 293 Color: 1
Size: 274 Color: 0

Bin 745: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 333 Color: 1
Size: 299 Color: 0

Bin 746: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 346 Color: 1
Size: 264 Color: 0

Bin 747: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 339 Color: 1
Size: 300 Color: 0

Bin 748: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 297 Color: 0
Size: 269 Color: 1

Bin 749: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 337 Color: 1
Size: 270 Color: 0

Bin 750: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 286 Color: 1
Size: 266 Color: 0

Bin 751: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 310 Color: 1
Size: 292 Color: 0

Bin 752: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 292 Color: 1
Size: 289 Color: 0

Bin 753: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 317 Color: 1
Size: 260 Color: 0

Bin 754: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 346 Color: 1
Size: 274 Color: 0

Bin 755: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 304 Color: 0
Size: 298 Color: 1

Bin 756: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 286 Color: 1
Size: 254 Color: 0

Bin 757: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 267 Color: 1
Size: 251 Color: 0

Bin 758: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 279 Color: 1
Size: 270 Color: 0

Bin 759: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 346 Color: 1
Size: 275 Color: 0

Bin 760: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 327 Color: 1
Size: 286 Color: 0

Bin 761: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 283 Color: 1
Size: 259 Color: 0

Bin 762: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 319 Color: 1
Size: 301 Color: 0

Bin 763: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 276 Color: 1
Size: 255 Color: 0

Bin 764: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 345 Color: 1
Size: 269 Color: 0

Bin 765: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 294 Color: 0
Size: 278 Color: 1

Bin 766: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 355 Color: 1
Size: 290 Color: 0

Bin 767: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 268 Color: 1
Size: 260 Color: 0

Bin 768: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 293 Color: 1
Size: 256 Color: 0

Bin 769: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 269 Color: 1
Size: 262 Color: 0

Bin 770: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 290 Color: 1
Size: 277 Color: 0

Bin 771: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 282 Color: 1
Size: 250 Color: 0

Bin 772: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 315 Color: 0
Size: 283 Color: 1

Bin 773: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 305 Color: 1
Size: 255 Color: 0

Bin 774: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 317 Color: 1
Size: 253 Color: 0

Bin 775: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 332 Color: 1
Size: 300 Color: 0

Bin 776: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 349 Color: 1
Size: 301 Color: 0

Bin 777: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 294 Color: 1
Size: 257 Color: 0

Bin 778: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 324 Color: 1
Size: 281 Color: 0

Bin 779: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 328 Color: 1
Size: 267 Color: 0

Bin 780: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 317 Color: 1
Size: 296 Color: 0

Bin 781: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 285 Color: 1
Size: 252 Color: 0

Bin 782: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 286 Color: 1
Size: 270 Color: 0

Bin 783: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 323 Color: 0
Size: 315 Color: 1

Bin 784: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 328 Color: 1
Size: 310 Color: 0

Bin 785: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 320 Color: 1
Size: 267 Color: 0

Bin 786: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 328 Color: 1
Size: 278 Color: 0

Bin 787: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 327 Color: 1
Size: 319 Color: 0

Bin 788: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 352 Color: 1
Size: 285 Color: 0

Bin 789: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 329 Color: 1
Size: 284 Color: 0

Bin 790: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 314 Color: 1
Size: 253 Color: 0

Bin 791: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 328 Color: 1
Size: 297 Color: 0

Bin 792: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 350 Color: 1
Size: 301 Color: 0

Bin 793: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 332 Color: 1
Size: 307 Color: 0

Bin 794: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 316 Color: 1
Size: 270 Color: 0

Bin 795: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 266 Color: 1
Size: 252 Color: 0

Bin 796: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 332 Color: 1
Size: 268 Color: 0

Bin 797: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 328 Color: 0
Size: 312 Color: 1

Bin 798: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 324 Color: 1
Size: 312 Color: 0

Bin 799: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 323 Color: 1
Size: 298 Color: 0

Bin 800: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 321 Color: 1
Size: 274 Color: 0

Bin 801: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 297 Color: 1
Size: 281 Color: 0

Bin 802: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 274 Color: 0
Size: 265 Color: 1

Bin 803: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 261 Color: 1
Size: 254 Color: 0

Bin 804: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 805: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 279 Color: 1
Size: 264 Color: 0

Bin 806: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 268 Color: 1
Size: 262 Color: 0

Bin 807: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 1
Size: 251 Color: 0

Bin 808: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 314 Color: 1
Size: 276 Color: 0

Bin 809: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 342 Color: 1
Size: 292 Color: 0

Bin 810: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 313 Color: 1
Size: 278 Color: 0

Bin 811: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 317 Color: 1
Size: 254 Color: 0

Bin 812: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 334 Color: 1
Size: 257 Color: 0

Bin 813: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 288 Color: 1
Size: 255 Color: 0

Bin 814: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 285 Color: 1
Size: 250 Color: 0

Bin 815: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 298 Color: 0
Size: 295 Color: 1

Bin 816: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 334 Color: 1
Size: 284 Color: 0

Bin 817: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 343 Color: 1
Size: 293 Color: 0

Bin 818: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 324 Color: 1
Size: 291 Color: 0

Bin 819: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 313 Color: 1
Size: 292 Color: 0

Bin 820: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 330 Color: 1
Size: 295 Color: 0

Bin 821: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 331 Color: 1
Size: 256 Color: 0

Bin 822: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 300 Color: 1
Size: 290 Color: 0

Bin 823: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 272 Color: 1
Size: 260 Color: 0

Bin 824: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 309 Color: 1
Size: 254 Color: 0

Bin 825: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 826: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 1
Size: 284 Color: 0

Bin 827: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 298 Color: 1
Size: 293 Color: 0

Bin 828: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 292 Color: 1
Size: 253 Color: 0

Bin 829: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 362 Color: 1
Size: 270 Color: 0

Bin 830: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 298 Color: 1
Size: 290 Color: 0

Bin 831: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 321 Color: 1
Size: 320 Color: 0

Bin 832: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 328 Color: 1
Size: 281 Color: 0

Bin 833: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 307 Color: 1
Size: 264 Color: 0

Bin 834: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 327 Color: 1
Size: 310 Color: 0

Bin 835: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 347 Color: 1
Size: 287 Color: 0

Bin 836: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 271 Color: 1
Size: 253 Color: 0

Bin 837: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 292 Color: 1
Size: 254 Color: 0

Bin 838: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 314 Color: 1
Size: 301 Color: 0

Bin 839: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 337 Color: 1
Size: 301 Color: 0

Bin 840: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 304 Color: 1
Size: 270 Color: 0

Bin 841: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 302 Color: 1
Size: 253 Color: 0

Bin 842: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 288 Color: 1
Size: 281 Color: 0

Bin 843: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 342 Color: 1
Size: 299 Color: 0

Bin 844: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 268 Color: 1
Size: 254 Color: 0

Bin 845: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 336 Color: 1
Size: 297 Color: 0

Bin 846: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 300 Color: 0
Size: 298 Color: 1

Bin 847: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 328 Color: 0
Size: 309 Color: 1

Bin 848: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 336 Color: 1
Size: 303 Color: 0

Bin 849: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 296 Color: 1
Size: 256 Color: 0

Bin 850: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 319 Color: 1
Size: 285 Color: 0

Bin 851: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 312 Color: 1
Size: 257 Color: 0

Bin 852: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 289 Color: 1
Size: 259 Color: 0

Bin 853: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 258 Color: 1
Size: 253 Color: 0

Bin 854: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 330 Color: 0
Size: 321 Color: 1

Bin 855: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 322 Color: 1
Size: 285 Color: 0

Bin 856: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 301 Color: 1
Size: 284 Color: 0

Bin 857: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 307 Color: 1
Size: 274 Color: 0

Bin 858: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 336 Color: 1
Size: 265 Color: 0

Bin 859: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 305 Color: 1
Size: 277 Color: 0

Bin 860: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 284 Color: 1
Size: 255 Color: 0

Bin 861: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 330 Color: 1
Size: 282 Color: 0

Bin 862: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 314 Color: 1
Size: 256 Color: 0

Bin 863: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 266 Color: 1
Size: 253 Color: 0

Bin 864: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 305 Color: 1
Size: 264 Color: 0

Bin 865: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 272 Color: 1
Size: 254 Color: 0

Bin 866: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 322 Color: 1
Size: 258 Color: 0

Bin 867: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 298 Color: 1
Size: 263 Color: 0

Bin 868: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 282 Color: 1
Size: 265 Color: 0

Bin 869: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 264 Color: 0
Size: 262 Color: 1

Bin 870: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 343 Color: 1
Size: 287 Color: 0

Bin 871: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 333 Color: 1
Size: 256 Color: 0

Bin 872: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 317 Color: 1
Size: 297 Color: 0

Bin 873: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 290 Color: 1
Size: 262 Color: 0

Bin 874: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 298 Color: 1
Size: 253 Color: 0

Bin 875: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 333 Color: 1
Size: 274 Color: 0

Bin 876: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 289 Color: 1
Size: 252 Color: 0

Bin 877: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 320 Color: 1
Size: 250 Color: 0

Bin 878: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 339 Color: 1
Size: 300 Color: 0

Bin 879: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 315 Color: 1
Size: 315 Color: 0

Bin 880: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 330 Color: 1
Size: 280 Color: 0

Bin 881: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 306 Color: 1
Size: 293 Color: 0

Bin 882: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 276 Color: 1
Size: 260 Color: 0

Bin 883: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 307 Color: 1
Size: 276 Color: 0

Bin 884: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 348 Color: 1
Size: 263 Color: 0

Bin 885: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 342 Color: 1
Size: 267 Color: 0

Bin 886: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 269 Color: 1
Size: 257 Color: 0

Bin 887: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 338 Color: 1
Size: 271 Color: 0

Bin 888: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 292 Color: 1
Size: 258 Color: 0

Bin 889: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 324 Color: 1
Size: 256 Color: 0

Bin 890: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 340 Color: 1
Size: 301 Color: 0

Bin 891: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 892: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 265 Color: 1
Size: 256 Color: 0

Bin 893: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 332 Color: 1
Size: 262 Color: 0

Bin 894: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 323 Color: 1
Size: 319 Color: 0

Bin 895: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 292 Color: 1
Size: 251 Color: 0

Bin 896: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 267 Color: 1
Size: 261 Color: 0

Bin 897: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 260 Color: 1
Size: 255 Color: 0

Bin 898: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 336 Color: 1
Size: 278 Color: 0

Bin 899: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 309 Color: 1
Size: 253 Color: 0

Bin 900: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 293 Color: 1
Size: 261 Color: 0

Bin 901: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 318 Color: 1
Size: 255 Color: 0

Bin 902: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 313 Color: 0
Size: 293 Color: 1

Bin 903: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 266 Color: 1
Size: 255 Color: 0

Bin 904: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 259 Color: 1
Size: 254 Color: 0

Bin 905: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 314 Color: 0
Size: 308 Color: 1

Bin 906: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 308 Color: 1
Size: 302 Color: 0

Bin 907: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 331 Color: 1
Size: 269 Color: 0

Bin 908: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 264 Color: 1
Size: 250 Color: 0

Bin 909: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 349 Color: 1
Size: 281 Color: 0

Bin 910: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 265 Color: 1
Size: 251 Color: 0

Bin 911: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 267 Color: 1
Size: 251 Color: 0

Bin 912: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 327 Color: 1
Size: 265 Color: 0

Bin 913: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 315 Color: 1
Size: 273 Color: 0

Bin 914: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 327 Color: 1
Size: 255 Color: 0

Bin 915: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 301 Color: 0
Size: 300 Color: 1

Bin 916: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 289 Color: 1
Size: 259 Color: 0

Bin 917: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 303 Color: 1
Size: 266 Color: 0

Bin 918: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 324 Color: 1
Size: 309 Color: 0

Bin 919: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 256 Color: 1
Size: 253 Color: 0

Bin 920: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 321 Color: 1
Size: 253 Color: 0

Bin 921: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 307 Color: 1
Size: 300 Color: 0

Bin 922: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 333 Color: 1
Size: 308 Color: 0

Bin 923: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 296 Color: 1
Size: 251 Color: 0

Bin 924: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 330 Color: 1
Size: 286 Color: 0

Bin 925: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 251 Color: 0

Bin 926: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 318 Color: 1
Size: 270 Color: 0

Bin 927: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 353 Color: 1
Size: 285 Color: 0

Bin 928: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 281 Color: 1
Size: 261 Color: 0

Bin 929: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 264 Color: 1
Size: 251 Color: 0

Bin 930: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 329 Color: 1
Size: 259 Color: 0

Bin 931: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 275 Color: 1
Size: 257 Color: 0

Bin 932: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 318 Color: 0
Size: 316 Color: 1

Bin 933: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 292 Color: 1
Size: 258 Color: 0

Bin 934: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 333 Color: 1
Size: 309 Color: 0

Bin 935: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 308 Color: 1
Size: 267 Color: 0

Bin 936: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 258 Color: 1
Size: 252 Color: 0

Bin 937: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 359 Color: 1
Size: 271 Color: 0

Bin 938: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 348 Color: 1
Size: 280 Color: 0

Bin 939: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 283 Color: 1
Size: 278 Color: 0

Bin 940: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 355 Color: 1
Size: 270 Color: 0

Bin 941: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 337 Color: 1
Size: 308 Color: 0

Bin 942: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 279 Color: 1
Size: 260 Color: 0

Bin 943: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 341 Color: 1
Size: 266 Color: 0

Bin 944: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 345 Color: 1
Size: 286 Color: 0

Bin 945: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 352 Color: 1
Size: 280 Color: 0

Bin 946: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 320 Color: 1
Size: 264 Color: 0

Bin 947: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 315 Color: 1
Size: 298 Color: 0

Bin 948: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 337 Color: 1
Size: 262 Color: 0

Bin 949: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 278 Color: 1
Size: 265 Color: 0

Bin 950: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 256 Color: 1
Size: 250 Color: 0

Bin 951: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 285 Color: 1
Size: 255 Color: 0

Bin 952: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 342 Color: 1
Size: 270 Color: 0

Bin 953: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 319 Color: 1
Size: 284 Color: 0

Bin 954: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 296 Color: 1
Size: 253 Color: 0

Bin 955: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 315 Color: 1
Size: 266 Color: 0

Bin 956: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 300 Color: 1
Size: 260 Color: 0

Bin 957: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 354 Color: 1
Size: 258 Color: 0

Bin 958: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 262 Color: 0
Size: 252 Color: 1

Bin 959: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 277 Color: 1
Size: 268 Color: 0

Bin 960: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 305 Color: 1
Size: 251 Color: 0

Bin 961: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 307 Color: 1
Size: 306 Color: 0

Bin 962: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 1
Size: 251 Color: 0

Bin 963: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 327 Color: 1
Size: 316 Color: 0

Bin 964: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 295 Color: 1
Size: 252 Color: 0

Bin 965: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 357 Color: 1
Size: 263 Color: 0

Bin 966: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 268 Color: 1
Size: 250 Color: 0

Bin 967: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 282 Color: 1
Size: 264 Color: 0

Bin 968: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 325 Color: 1
Size: 258 Color: 0

Bin 969: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 320 Color: 1
Size: 313 Color: 0

Bin 970: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 320 Color: 1
Size: 299 Color: 0

Bin 971: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 304 Color: 1
Size: 263 Color: 0

Bin 972: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 358 Color: 1
Size: 279 Color: 0

Bin 973: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 298 Color: 1
Size: 279 Color: 0

Bin 974: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 327 Color: 1
Size: 267 Color: 0

Bin 975: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 327 Color: 1
Size: 309 Color: 0

Bin 976: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 302 Color: 1
Size: 293 Color: 0

Bin 977: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 291 Color: 1
Size: 274 Color: 0

Bin 978: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 254 Color: 1
Size: 253 Color: 0

Bin 979: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 336 Color: 1
Size: 282 Color: 0

Bin 980: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 332 Color: 1
Size: 282 Color: 0

Bin 981: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 278 Color: 1
Size: 259 Color: 0

Bin 982: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 331 Color: 1
Size: 281 Color: 0

Bin 983: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 326 Color: 0
Size: 301 Color: 1

Bin 984: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 306 Color: 1
Size: 258 Color: 0

Bin 985: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 322 Color: 1
Size: 280 Color: 0

Bin 986: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 316 Color: 1
Size: 293 Color: 0

Bin 987: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 325 Color: 1
Size: 304 Color: 0

Bin 988: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0

Bin 989: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 303 Color: 1
Size: 257 Color: 0

Bin 990: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 345 Color: 1
Size: 296 Color: 0

Bin 991: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 313 Color: 1
Size: 264 Color: 0

Bin 992: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 309 Color: 1
Size: 258 Color: 0

Bin 993: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 338 Color: 1
Size: 307 Color: 0

Bin 994: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 323 Color: 1
Size: 264 Color: 0

Bin 995: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 340 Color: 1
Size: 296 Color: 0

Bin 996: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 334 Color: 1
Size: 264 Color: 0

Bin 997: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 282 Color: 0
Size: 274 Color: 1

Bin 998: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 293 Color: 0
Size: 283 Color: 1

Bin 999: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 266 Color: 1
Size: 266 Color: 0

Bin 1000: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 342 Color: 1
Size: 292 Color: 0

Bin 1001: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 325 Color: 1
Size: 301 Color: 0

Bin 1002: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 304 Color: 1
Size: 256 Color: 0

Bin 1003: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 351 Color: 1
Size: 277 Color: 0

Bin 1004: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 342 Color: 1
Size: 299 Color: 0

Bin 1005: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 334 Color: 1
Size: 263 Color: 0

Bin 1006: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 268 Color: 1
Size: 254 Color: 0

Bin 1007: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 301 Color: 1
Size: 284 Color: 0

Bin 1008: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 346 Color: 1
Size: 287 Color: 0

Bin 1009: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 297 Color: 1
Size: 279 Color: 0

Bin 1010: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 344 Color: 1
Size: 277 Color: 0

Bin 1011: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 293 Color: 1
Size: 262 Color: 0

Bin 1012: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 341 Color: 1
Size: 263 Color: 0

Bin 1013: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 351 Color: 1
Size: 262 Color: 0

Bin 1014: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 323 Color: 1
Size: 265 Color: 0

Bin 1015: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 264 Color: 1
Size: 253 Color: 0

Bin 1016: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 1
Size: 253 Color: 0

Bin 1017: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 288 Color: 1
Size: 266 Color: 0

Bin 1018: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 355 Color: 1
Size: 265 Color: 0

Bin 1019: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 345 Color: 1
Size: 266 Color: 0

Bin 1020: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 304 Color: 1
Size: 256 Color: 0

Bin 1021: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 276 Color: 1
Size: 251 Color: 0

Bin 1022: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 353 Color: 1
Size: 266 Color: 0

Bin 1023: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 316 Color: 1
Size: 292 Color: 0

Bin 1024: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 270 Color: 1
Size: 256 Color: 0

Bin 1025: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 278 Color: 0
Size: 265 Color: 1

Bin 1026: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 260 Color: 1
Size: 252 Color: 0

Bin 1027: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 284 Color: 1
Size: 255 Color: 0

Bin 1028: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 332 Color: 1
Size: 308 Color: 0

Bin 1029: 0 of cap free
Amount of items: 3
Items: 
Size: 347 Color: 1
Size: 339 Color: 1
Size: 315 Color: 0

Bin 1030: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 332 Color: 1
Size: 283 Color: 0

Bin 1031: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 322 Color: 1
Size: 309 Color: 0

Bin 1032: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 318 Color: 1
Size: 291 Color: 0

Bin 1033: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 297 Color: 1
Size: 251 Color: 0

Bin 1034: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 343 Color: 1
Size: 286 Color: 0

Bin 1035: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 328 Color: 1
Size: 286 Color: 0

Bin 1036: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 300 Color: 1
Size: 287 Color: 0

Bin 1037: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 277 Color: 1
Size: 253 Color: 0

Bin 1038: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 302 Color: 1
Size: 264 Color: 0

Bin 1039: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 269 Color: 1
Size: 266 Color: 0

Bin 1040: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 322 Color: 1
Size: 277 Color: 0

Bin 1041: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 304 Color: 1
Size: 260 Color: 0

Bin 1042: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 330 Color: 1
Size: 283 Color: 0

Bin 1043: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 268 Color: 0
Size: 251 Color: 1

Bin 1044: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 325 Color: 1
Size: 292 Color: 0

Bin 1045: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 286 Color: 1
Size: 265 Color: 0

Bin 1046: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1
Size: 343 Color: 1
Size: 314 Color: 0

Bin 1047: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 300 Color: 1
Size: 250 Color: 0

Bin 1048: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 307 Color: 1
Size: 261 Color: 0

Bin 1049: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 335 Color: 1
Size: 283 Color: 0

Bin 1050: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 345 Color: 1
Size: 268 Color: 0

Bin 1051: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 319 Color: 1
Size: 278 Color: 0

Bin 1052: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 341 Color: 1
Size: 290 Color: 0

Bin 1053: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 309 Color: 1
Size: 272 Color: 0

Bin 1054: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 325 Color: 1
Size: 317 Color: 0

Bin 1055: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 338 Color: 1
Size: 270 Color: 0

Bin 1056: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 307 Color: 1
Size: 282 Color: 0

Bin 1057: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 287 Color: 1
Size: 251 Color: 0

Bin 1058: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 327 Color: 1
Size: 285 Color: 0

Bin 1059: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 336 Color: 1
Size: 255 Color: 0

Bin 1060: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 257 Color: 1
Size: 254 Color: 0

Bin 1061: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 346 Color: 1
Size: 293 Color: 0

Bin 1062: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 343 Color: 1
Size: 265 Color: 0

Bin 1063: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 284 Color: 1
Size: 259 Color: 0

Bin 1064: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 351 Color: 1
Size: 297 Color: 0

Bin 1065: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 307 Color: 1
Size: 258 Color: 0

Bin 1066: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 347 Color: 1
Size: 263 Color: 0

Bin 1067: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 338 Color: 1
Size: 255 Color: 0

Bin 1068: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 284 Color: 1
Size: 274 Color: 0

Bin 1069: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 331 Color: 1
Size: 281 Color: 0

Bin 1070: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 323 Color: 1
Size: 315 Color: 0

Bin 1071: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 296 Color: 1
Size: 276 Color: 0

Bin 1072: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 329 Color: 1
Size: 286 Color: 0

Bin 1073: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 1074: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 329 Color: 1
Size: 292 Color: 0

Bin 1075: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 1076: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 319 Color: 0
Size: 313 Color: 1

Bin 1077: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 321 Color: 1
Size: 278 Color: 0

Bin 1078: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 340 Color: 1
Size: 275 Color: 0

Bin 1079: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 305 Color: 1
Size: 278 Color: 0

Bin 1080: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 360 Color: 1
Size: 264 Color: 0

Bin 1081: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 285 Color: 1
Size: 259 Color: 0

Bin 1082: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 310 Color: 1
Size: 284 Color: 0

Bin 1083: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 331 Color: 1
Size: 286 Color: 0

Bin 1084: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 339 Color: 1
Size: 309 Color: 0

Bin 1085: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 301 Color: 1
Size: 260 Color: 0

Bin 1086: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 345 Color: 1
Size: 287 Color: 0

Bin 1087: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 364 Color: 1
Size: 263 Color: 0

Bin 1088: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 274 Color: 1
Size: 268 Color: 0

Bin 1089: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 307 Color: 1
Size: 257 Color: 0

Bin 1090: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 337 Color: 1
Size: 294 Color: 0

Bin 1091: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 282 Color: 1
Size: 250 Color: 0

Bin 1092: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 313 Color: 1
Size: 313 Color: 0

Bin 1093: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 287 Color: 0
Size: 270 Color: 1

Bin 1094: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 309 Color: 1
Size: 266 Color: 0

Bin 1095: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 310 Color: 1
Size: 267 Color: 0

Bin 1096: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 1097: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 307 Color: 1
Size: 282 Color: 0

Bin 1098: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 346 Color: 1
Size: 275 Color: 0

Bin 1099: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 286 Color: 1
Size: 280 Color: 0

Bin 1100: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 263 Color: 1
Size: 251 Color: 0

Bin 1101: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 301 Color: 1
Size: 265 Color: 0

Bin 1102: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 329 Color: 1
Size: 300 Color: 0

Bin 1103: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 351 Color: 1
Size: 264 Color: 0

Bin 1104: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 311 Color: 1
Size: 275 Color: 0

Bin 1105: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 351 Color: 1
Size: 281 Color: 0

Bin 1106: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 349 Color: 1
Size: 275 Color: 0

Bin 1107: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 346 Color: 1
Size: 296 Color: 0

Bin 1108: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 268 Color: 1
Size: 250 Color: 0

Bin 1109: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 307 Color: 1
Size: 255 Color: 0

Bin 1110: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 361 Color: 1
Size: 263 Color: 0

Bin 1111: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 347 Color: 1
Size: 288 Color: 0

Bin 1112: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 350 Color: 1
Size: 287 Color: 0

Bin 1113: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 329 Color: 0
Size: 305 Color: 1

Bin 1114: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 338 Color: 1
Size: 258 Color: 0

Bin 1115: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 293 Color: 1
Size: 293 Color: 0

Bin 1116: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 283 Color: 1
Size: 279 Color: 0

Bin 1117: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 348 Color: 1
Size: 265 Color: 0

Bin 1118: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 298 Color: 0
Size: 294 Color: 1

Bin 1119: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 285 Color: 1
Size: 253 Color: 0

Bin 1120: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 302 Color: 1
Size: 251 Color: 0

Bin 1121: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 336 Color: 1
Size: 268 Color: 0

Bin 1122: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 318 Color: 1
Size: 283 Color: 0

Bin 1123: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 325 Color: 1
Size: 256 Color: 0

Bin 1124: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 321 Color: 1
Size: 259 Color: 0

Bin 1125: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 319 Color: 0
Size: 304 Color: 1

Bin 1126: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 348 Color: 1
Size: 285 Color: 0

Bin 1127: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 349 Color: 1
Size: 285 Color: 0

Bin 1128: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 265 Color: 1
Size: 252 Color: 0

Bin 1129: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 308 Color: 0
Size: 287 Color: 1

Bin 1130: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 290 Color: 1
Size: 259 Color: 0

Bin 1131: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 313 Color: 1
Size: 262 Color: 0

Bin 1132: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 289 Color: 1
Size: 266 Color: 0

Bin 1133: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 353 Color: 1
Size: 285 Color: 0

Bin 1134: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 353 Color: 1
Size: 285 Color: 0

Bin 1135: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 324 Color: 0
Size: 317 Color: 1

Bin 1136: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 317 Color: 1
Size: 258 Color: 0

Bin 1137: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 338 Color: 1
Size: 279 Color: 0

Bin 1138: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 322 Color: 0
Size: 290 Color: 1

Bin 1139: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 305 Color: 0
Size: 289 Color: 1

Bin 1140: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 279 Color: 1
Size: 265 Color: 0

Bin 1141: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 267 Color: 1
Size: 252 Color: 0

Bin 1142: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 282 Color: 1
Size: 253 Color: 0

Bin 1143: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 323 Color: 1
Size: 259 Color: 0

Bin 1144: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 296 Color: 1
Size: 257 Color: 0

Bin 1145: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 321 Color: 1
Size: 262 Color: 0

Bin 1146: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 332 Color: 1
Size: 253 Color: 0

Bin 1147: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 295 Color: 0
Size: 288 Color: 1

Bin 1148: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 307 Color: 1
Size: 291 Color: 0

Bin 1149: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 1150: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 285 Color: 1
Size: 251 Color: 0

Bin 1151: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 298 Color: 1
Size: 254 Color: 0

Bin 1152: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 324 Color: 1
Size: 254 Color: 0

Bin 1153: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 352 Color: 1
Size: 285 Color: 0

Bin 1154: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 358 Color: 1
Size: 265 Color: 0

Bin 1155: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 0
Size: 256 Color: 1

Bin 1156: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 1
Size: 256 Color: 0

Bin 1157: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 262 Color: 1
Size: 257 Color: 0

Bin 1158: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 288 Color: 1
Size: 257 Color: 0

Bin 1159: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 301 Color: 1
Size: 263 Color: 0

Bin 1160: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 351 Color: 1
Size: 278 Color: 0

Bin 1161: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 329 Color: 1
Size: 283 Color: 0

Bin 1162: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 262 Color: 1
Size: 252 Color: 0

Bin 1163: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 291 Color: 1
Size: 258 Color: 0

Bin 1164: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 295 Color: 1
Size: 279 Color: 0

Bin 1165: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 295 Color: 1
Size: 259 Color: 0

Bin 1166: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 330 Color: 0
Size: 315 Color: 1

Bin 1167: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 328 Color: 1
Size: 308 Color: 0

Bin 1168: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 279 Color: 1
Size: 254 Color: 0

Bin 1169: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 316 Color: 1
Size: 268 Color: 0

Bin 1170: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 1171: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 326 Color: 1
Size: 258 Color: 0

Bin 1172: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 290 Color: 1
Size: 269 Color: 0

Bin 1173: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 328 Color: 1
Size: 301 Color: 0

Bin 1174: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 324 Color: 0
Size: 320 Color: 1

Bin 1175: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 319 Color: 1
Size: 282 Color: 0

Bin 1176: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 328 Color: 1
Size: 257 Color: 0

Bin 1177: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 331 Color: 1
Size: 297 Color: 0

Bin 1178: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 314 Color: 0
Size: 285 Color: 1

Bin 1179: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 271 Color: 1
Size: 252 Color: 0

Bin 1180: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 1181: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 339 Color: 1
Size: 268 Color: 0

Bin 1182: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 303 Color: 0
Size: 290 Color: 1

Bin 1183: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 331 Color: 1
Size: 278 Color: 0

Bin 1184: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 348 Color: 1
Size: 272 Color: 0

Bin 1185: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 331 Color: 1
Size: 270 Color: 0

Bin 1186: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 323 Color: 1
Size: 260 Color: 0

Bin 1187: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 326 Color: 1
Size: 306 Color: 0

Bin 1188: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 256 Color: 1
Size: 253 Color: 0

Bin 1189: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 343 Color: 1
Size: 299 Color: 0

Bin 1190: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 259 Color: 1
Size: 250 Color: 0

Bin 1191: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 352 Color: 1
Size: 278 Color: 0

Bin 1192: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 351 Color: 1
Size: 279 Color: 0

Bin 1193: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 354 Color: 1
Size: 251 Color: 0

Bin 1194: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 272 Color: 1
Size: 250 Color: 0

Bin 1195: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 312 Color: 0
Size: 303 Color: 1

Bin 1196: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 346 Color: 1
Size: 296 Color: 0

Bin 1197: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 267 Color: 1
Size: 255 Color: 0

Bin 1198: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 344 Color: 1
Size: 300 Color: 0

Bin 1199: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 334 Color: 1
Size: 301 Color: 0

Bin 1200: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 282 Color: 1
Size: 259 Color: 0

Bin 1201: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 302 Color: 1
Size: 293 Color: 0

Bin 1202: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 344 Color: 1
Size: 295 Color: 0

Bin 1203: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 347 Color: 1
Size: 280 Color: 0

Bin 1204: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 257 Color: 1
Size: 256 Color: 0

Bin 1205: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 294 Color: 1
Size: 285 Color: 0

Bin 1206: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 339 Color: 1
Size: 290 Color: 0

Bin 1207: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 346 Color: 1
Size: 273 Color: 0

Bin 1208: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 279 Color: 1
Size: 259 Color: 0

Bin 1209: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 357 Color: 1
Size: 263 Color: 0

Bin 1210: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 319 Color: 1
Size: 259 Color: 0

Bin 1211: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 341 Color: 1
Size: 282 Color: 0

Bin 1212: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 291 Color: 1
Size: 252 Color: 0

Bin 1213: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 303 Color: 0
Size: 285 Color: 1

Bin 1214: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 323 Color: 1
Size: 295 Color: 0

Bin 1215: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 347 Color: 1
Size: 267 Color: 0

Bin 1216: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 327 Color: 1
Size: 262 Color: 0

Bin 1217: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 324 Color: 1
Size: 316 Color: 0

Bin 1218: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 337 Color: 1
Size: 308 Color: 0

Bin 1219: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 339 Color: 1
Size: 277 Color: 0

Bin 1220: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 285 Color: 1
Size: 255 Color: 0

Bin 1221: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 0
Size: 251 Color: 1

Bin 1222: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 282 Color: 1
Size: 264 Color: 0

Bin 1223: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 311 Color: 1
Size: 251 Color: 0

Bin 1224: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 314 Color: 0
Size: 309 Color: 1

Bin 1225: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 275 Color: 1
Size: 274 Color: 0

Bin 1226: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 327 Color: 1
Size: 307 Color: 0

Bin 1227: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 279 Color: 1
Size: 253 Color: 0

Bin 1228: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 264 Color: 1
Size: 250 Color: 0

Bin 1229: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 266 Color: 1
Size: 254 Color: 0

Bin 1230: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 261 Color: 1
Size: 250 Color: 0

Bin 1231: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 367 Color: 1
Size: 262 Color: 0

Bin 1232: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 327 Color: 1
Size: 279 Color: 0

Bin 1233: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 331 Color: 1
Size: 304 Color: 0

Bin 1234: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 354 Color: 1
Size: 271 Color: 0

Bin 1235: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 351 Color: 1
Size: 283 Color: 0

Bin 1236: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 291 Color: 1
Size: 283 Color: 0

Bin 1237: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 251 Color: 0

Bin 1238: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 339 Color: 1
Size: 269 Color: 0

Bin 1239: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 324 Color: 1
Size: 282 Color: 0

Bin 1240: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 274 Color: 1
Size: 255 Color: 0

Bin 1241: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 334 Color: 1
Size: 260 Color: 0

Bin 1242: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 346 Color: 1
Size: 286 Color: 0

Bin 1243: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 287 Color: 1
Size: 257 Color: 0

Bin 1244: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 306 Color: 0
Size: 303 Color: 1

Bin 1245: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 338 Color: 1
Size: 262 Color: 0

Bin 1246: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 333 Color: 0
Size: 282 Color: 1

Bin 1247: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 288 Color: 0
Size: 269 Color: 1

Bin 1248: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 287 Color: 1
Size: 253 Color: 0

Bin 1249: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 282 Color: 1
Size: 280 Color: 0

Bin 1250: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 291 Color: 1
Size: 251 Color: 0

Bin 1251: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 335 Color: 1
Size: 263 Color: 0

Bin 1252: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 345 Color: 1
Size: 283 Color: 0

Bin 1253: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 316 Color: 1
Size: 256 Color: 0

Bin 1254: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 315 Color: 1
Size: 293 Color: 0

Bin 1255: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 276 Color: 1
Size: 256 Color: 0

Bin 1256: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 346 Color: 1
Size: 268 Color: 0

Bin 1257: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 299 Color: 0
Size: 284 Color: 1

Bin 1258: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 333 Color: 1
Size: 258 Color: 0

Bin 1259: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 326 Color: 1
Size: 309 Color: 0

Bin 1260: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 321 Color: 1
Size: 285 Color: 0

Bin 1261: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 332 Color: 1
Size: 281 Color: 0

Bin 1262: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 330 Color: 1
Size: 270 Color: 0

Bin 1263: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 349 Color: 1
Size: 290 Color: 0

Bin 1264: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 352 Color: 1
Size: 292 Color: 0

Bin 1265: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 307 Color: 0
Size: 304 Color: 1

Bin 1266: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 299 Color: 1
Size: 267 Color: 0

Bin 1267: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 338 Color: 1
Size: 286 Color: 0

Bin 1268: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 350 Color: 1
Size: 291 Color: 0

Bin 1269: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 302 Color: 1
Size: 253 Color: 0

Bin 1270: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 298 Color: 1
Size: 270 Color: 0

Bin 1271: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 306 Color: 1
Size: 267 Color: 0

Bin 1272: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 281 Color: 1
Size: 257 Color: 0

Bin 1273: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 313 Color: 1
Size: 269 Color: 0

Bin 1274: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 304 Color: 1
Size: 284 Color: 0

Bin 1275: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 328 Color: 1
Size: 276 Color: 0

Bin 1276: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 287 Color: 0
Size: 277 Color: 1

Bin 1277: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 347 Color: 1
Size: 275 Color: 0

Bin 1278: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 272 Color: 1
Size: 256 Color: 0

Bin 1279: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 339 Color: 1
Size: 312 Color: 0

Bin 1280: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 317 Color: 1
Size: 280 Color: 0

Bin 1281: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 357 Color: 1
Size: 260 Color: 0

Bin 1282: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 312 Color: 1
Size: 260 Color: 0

Bin 1283: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 299 Color: 1
Size: 267 Color: 0

Bin 1284: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 337 Color: 1
Size: 286 Color: 0

Bin 1285: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 284 Color: 1
Size: 251 Color: 0

Bin 1286: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 325 Color: 1
Size: 271 Color: 0

Bin 1287: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 356 Color: 1
Size: 276 Color: 0

Bin 1288: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 317 Color: 0
Size: 300 Color: 1

Bin 1289: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 1290: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 337 Color: 1
Size: 276 Color: 0

Bin 1291: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 314 Color: 1
Size: 274 Color: 0

Bin 1292: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 339 Color: 1
Size: 294 Color: 0

Bin 1293: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 339 Color: 1
Size: 297 Color: 0

Bin 1294: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 342 Color: 1
Size: 299 Color: 0

Bin 1295: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 326 Color: 1
Size: 282 Color: 0

Bin 1296: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 286 Color: 1
Size: 276 Color: 0

Bin 1297: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 336 Color: 1
Size: 308 Color: 0

Bin 1298: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 274 Color: 1
Size: 261 Color: 0

Bin 1299: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 329 Color: 1
Size: 304 Color: 0

Bin 1300: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 354 Color: 1
Size: 271 Color: 0

Bin 1301: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 275 Color: 0
Size: 273 Color: 1

Bin 1302: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 326 Color: 1
Size: 258 Color: 0

Bin 1303: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 354 Color: 1
Size: 275 Color: 0

Bin 1304: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 304 Color: 1
Size: 264 Color: 0

Bin 1305: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 304 Color: 1
Size: 254 Color: 0

Bin 1306: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 278 Color: 1
Size: 270 Color: 0

Bin 1307: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 356 Color: 1
Size: 275 Color: 0

Bin 1308: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 342 Color: 1
Size: 267 Color: 0

Bin 1309: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 309 Color: 1
Size: 261 Color: 0

Bin 1310: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 276 Color: 0
Size: 261 Color: 1

Bin 1311: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 350 Color: 1
Size: 288 Color: 0

Bin 1312: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 324 Color: 1
Size: 267 Color: 0

Bin 1313: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 335 Color: 1
Size: 272 Color: 0

Bin 1314: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 301 Color: 1
Size: 252 Color: 0

Bin 1315: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 322 Color: 1
Size: 317 Color: 0

Bin 1316: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 327 Color: 1
Size: 257 Color: 0

Bin 1317: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 318 Color: 1
Size: 265 Color: 0

Bin 1318: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 320 Color: 0
Size: 297 Color: 1

Bin 1319: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 1320: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 335 Color: 1
Size: 261 Color: 0

Bin 1321: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 308 Color: 1
Size: 251 Color: 0

Bin 1322: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 320 Color: 1
Size: 288 Color: 0

Bin 1323: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 309 Color: 1
Size: 254 Color: 0

Bin 1324: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 302 Color: 1
Size: 272 Color: 0

Bin 1325: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 309 Color: 0
Size: 306 Color: 1

Bin 1326: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 260 Color: 1
Size: 256 Color: 0

Bin 1327: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 276 Color: 1
Size: 250 Color: 0

Bin 1328: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 298 Color: 1
Size: 252 Color: 0

Bin 1329: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 317 Color: 1
Size: 279 Color: 0

Bin 1330: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 312 Color: 1
Size: 270 Color: 0

Bin 1331: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 317 Color: 1
Size: 287 Color: 0

Bin 1332: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 278 Color: 1
Size: 265 Color: 0

Bin 1333: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 348 Color: 1
Size: 289 Color: 0

Bin 1334: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 327 Color: 1
Size: 311 Color: 0

Bin 1335: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 351 Color: 1
Size: 277 Color: 0

Bin 1336: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 289 Color: 1
Size: 257 Color: 0

Bin 1337: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 325 Color: 0
Size: 313 Color: 1

Bin 1338: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 313 Color: 1
Size: 272 Color: 0

Bin 1339: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 302 Color: 1
Size: 253 Color: 0

Bin 1340: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 339 Color: 1
Size: 296 Color: 0

Bin 1341: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 353 Color: 1
Size: 277 Color: 0

Bin 1342: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 286 Color: 1
Size: 265 Color: 0

Bin 1343: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 274 Color: 1
Size: 261 Color: 0

Bin 1344: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 276 Color: 0
Size: 266 Color: 1

Bin 1345: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 295 Color: 1
Size: 269 Color: 0

Bin 1346: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 263 Color: 1
Size: 257 Color: 0

Bin 1347: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 263 Color: 1
Size: 260 Color: 0

Bin 1348: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 329 Color: 1
Size: 301 Color: 0

Bin 1349: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 342 Color: 1
Size: 279 Color: 0

Bin 1350: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 346 Color: 1
Size: 263 Color: 0

Bin 1351: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 326 Color: 1
Size: 280 Color: 0

Bin 1352: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 343 Color: 1
Size: 263 Color: 0

Bin 1353: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 337 Color: 1
Size: 290 Color: 0

Bin 1354: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 310 Color: 1
Size: 280 Color: 0

Bin 1355: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 349 Color: 1
Size: 288 Color: 0

Bin 1356: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 301 Color: 1
Size: 277 Color: 0

Bin 1357: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 350 Color: 1
Size: 301 Color: 0

Bin 1358: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 326 Color: 1
Size: 285 Color: 0

Bin 1359: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 1360: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 255 Color: 1
Size: 255 Color: 0

Bin 1361: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 328 Color: 1
Size: 313 Color: 0

Bin 1362: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 320 Color: 0
Size: 297 Color: 1

Bin 1363: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 333 Color: 1
Size: 283 Color: 0

Bin 1364: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 352 Color: 1
Size: 263 Color: 0

Bin 1365: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 343 Color: 1
Size: 283 Color: 0

Bin 1366: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 321 Color: 1
Size: 316 Color: 0

Bin 1367: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 351 Color: 1
Size: 288 Color: 0

Bin 1368: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 344 Color: 1
Size: 296 Color: 0

Bin 1369: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 323 Color: 1
Size: 270 Color: 0

Bin 1370: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 331 Color: 1
Size: 302 Color: 0

Bin 1371: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 322 Color: 1
Size: 255 Color: 0

Bin 1372: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 337 Color: 1
Size: 263 Color: 0

Bin 1373: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 328 Color: 1
Size: 259 Color: 0

Bin 1374: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 313 Color: 1
Size: 263 Color: 0

Bin 1375: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 257 Color: 1
Size: 252 Color: 0

Bin 1376: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 314 Color: 1
Size: 309 Color: 0

Bin 1377: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 342 Color: 1
Size: 287 Color: 0

Bin 1378: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 330 Color: 0
Size: 302 Color: 1

Bin 1379: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 303 Color: 0
Size: 302 Color: 1

Bin 1380: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 358 Color: 1
Size: 272 Color: 0

Bin 1381: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 288 Color: 1
Size: 260 Color: 0

Bin 1382: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 267 Color: 1
Size: 251 Color: 0

Bin 1383: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 333 Color: 1
Size: 267 Color: 0

Bin 1384: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 350 Color: 1
Size: 272 Color: 0

Bin 1385: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 1
Size: 251 Color: 0

Bin 1386: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 368 Color: 1
Size: 252 Color: 0

Bin 1387: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 304 Color: 1
Size: 257 Color: 0

Bin 1388: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 1389: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 277 Color: 1
Size: 265 Color: 0

Bin 1390: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 337 Color: 1
Size: 302 Color: 0

Bin 1391: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 320 Color: 1
Size: 315 Color: 0

Bin 1392: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 342 Color: 1
Size: 267 Color: 0

Bin 1393: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 345 Color: 1
Size: 300 Color: 0

Bin 1394: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 342 Color: 1
Size: 280 Color: 0

Bin 1395: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 269 Color: 1
Size: 266 Color: 0

Bin 1396: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 354 Color: 1
Size: 293 Color: 0

Bin 1397: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 344 Color: 1
Size: 296 Color: 0

Bin 1398: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 308 Color: 1
Size: 266 Color: 0

Bin 1399: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 294 Color: 1
Size: 291 Color: 0

Bin 1400: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 330 Color: 1
Size: 263 Color: 0

Bin 1401: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 345 Color: 1
Size: 296 Color: 0

Bin 1402: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 345 Color: 1
Size: 264 Color: 0

Bin 1403: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 261 Color: 1
Size: 251 Color: 0

Bin 1404: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 324 Color: 1
Size: 271 Color: 0

Bin 1405: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 263 Color: 1
Size: 251 Color: 0

Bin 1406: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 338 Color: 1
Size: 292 Color: 0

Bin 1407: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 322 Color: 1
Size: 295 Color: 0

Bin 1408: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 270 Color: 1
Size: 255 Color: 0

Bin 1409: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 251 Color: 1
Size: 251 Color: 0

Bin 1410: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 280 Color: 1
Size: 256 Color: 0

Bin 1411: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 293 Color: 1
Size: 275 Color: 0

Bin 1412: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 340 Color: 1
Size: 261 Color: 0

Bin 1413: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 305 Color: 1
Size: 259 Color: 0

Bin 1414: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 340 Color: 1
Size: 291 Color: 0

Bin 1415: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 322 Color: 0
Size: 300 Color: 1

Bin 1416: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 309 Color: 0
Size: 292 Color: 1

Bin 1417: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 281 Color: 1
Size: 259 Color: 0

Bin 1418: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 329 Color: 1
Size: 277 Color: 0

Bin 1419: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 1420: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 1421: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 328 Color: 1
Size: 267 Color: 0

Bin 1422: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 319 Color: 1
Size: 268 Color: 0

Bin 1423: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 308 Color: 1
Size: 295 Color: 0

Bin 1424: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 293 Color: 1
Size: 291 Color: 0

Bin 1425: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 358 Color: 1
Size: 266 Color: 0

Bin 1426: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 346 Color: 1
Size: 279 Color: 0

Bin 1427: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 1
Size: 251 Color: 0

Bin 1428: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 326 Color: 1
Size: 276 Color: 0

Bin 1429: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 312 Color: 1
Size: 299 Color: 0

Bin 1430: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 293 Color: 1
Size: 267 Color: 0

Bin 1431: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 325 Color: 1
Size: 269 Color: 0

Bin 1432: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 334 Color: 1
Size: 267 Color: 0

Bin 1433: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 284 Color: 1
Size: 260 Color: 0

Bin 1434: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 331 Color: 1
Size: 269 Color: 0

Bin 1435: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 345 Color: 1
Size: 269 Color: 0

Bin 1436: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 339 Color: 1
Size: 289 Color: 0

Bin 1437: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 277 Color: 1
Size: 253 Color: 0

Bin 1438: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 289 Color: 1
Size: 253 Color: 0

Bin 1439: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 339 Color: 1
Size: 272 Color: 0

Bin 1440: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 311 Color: 1
Size: 260 Color: 0

Bin 1441: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 335 Color: 1
Size: 276 Color: 0

Bin 1442: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 345 Color: 1
Size: 285 Color: 0

Bin 1443: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 301 Color: 1
Size: 253 Color: 0

Bin 1444: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 281 Color: 1
Size: 275 Color: 0

Bin 1445: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 319 Color: 1
Size: 313 Color: 0

Bin 1446: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 294 Color: 1
Size: 260 Color: 0

Bin 1447: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 313 Color: 0
Size: 280 Color: 1

Bin 1448: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 333 Color: 1
Size: 253 Color: 0

Bin 1449: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 307 Color: 1
Size: 253 Color: 0

Bin 1450: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 321 Color: 1
Size: 308 Color: 0

Bin 1451: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 256 Color: 1
Size: 252 Color: 0

Bin 1452: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 347 Color: 1
Size: 269 Color: 0

Bin 1453: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 334 Color: 1
Size: 284 Color: 0

Bin 1454: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 327 Color: 1
Size: 267 Color: 0

Bin 1455: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 346 Color: 1
Size: 275 Color: 0

Bin 1456: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 319 Color: 1
Size: 294 Color: 0

Bin 1457: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 289 Color: 1
Size: 253 Color: 0

Bin 1458: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 273 Color: 1
Size: 263 Color: 0

Bin 1459: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 290 Color: 1
Size: 255 Color: 0

Bin 1460: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 338 Color: 1
Size: 266 Color: 0

Bin 1461: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 320 Color: 1
Size: 307 Color: 0

Bin 1462: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 353 Color: 1
Size: 265 Color: 0

Bin 1463: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 296 Color: 1
Size: 252 Color: 0

Bin 1464: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 349 Color: 1
Size: 301 Color: 0

Bin 1465: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 334 Color: 1
Size: 262 Color: 0

Bin 1466: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 287 Color: 1
Size: 260 Color: 0

Bin 1467: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 303 Color: 1
Size: 269 Color: 0

Bin 1468: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 319 Color: 0
Size: 302 Color: 1

Bin 1469: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 338 Color: 1
Size: 257 Color: 0

Bin 1470: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 279 Color: 1
Size: 257 Color: 0

Bin 1471: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 349 Color: 1
Size: 280 Color: 0

Bin 1472: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 311 Color: 1
Size: 264 Color: 0

Bin 1473: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 289 Color: 1
Size: 251 Color: 0

Bin 1474: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 255 Color: 0
Size: 254 Color: 1

Bin 1475: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 311 Color: 0
Size: 309 Color: 1

Bin 1476: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 350 Color: 1
Size: 275 Color: 0

Bin 1477: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 271 Color: 1
Size: 257 Color: 0

Bin 1478: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 340 Color: 1
Size: 291 Color: 0

Bin 1479: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 309 Color: 1
Size: 278 Color: 0

Bin 1480: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 265 Color: 1
Size: 256 Color: 0

Bin 1481: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 280 Color: 1
Size: 250 Color: 0

Bin 1482: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 288 Color: 1
Size: 274 Color: 0

Bin 1483: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1
Size: 334 Color: 1
Size: 323 Color: 0

Bin 1484: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 343 Color: 1
Size: 276 Color: 0

Bin 1485: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 359 Color: 1
Size: 265 Color: 0

Bin 1486: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 321 Color: 1
Size: 262 Color: 0

Bin 1487: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 272 Color: 1
Size: 250 Color: 0

Bin 1488: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 258 Color: 1
Size: 250 Color: 0

Bin 1489: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 337 Color: 1
Size: 288 Color: 0

Bin 1490: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 342 Color: 1
Size: 256 Color: 0

Bin 1491: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 342 Color: 1
Size: 296 Color: 0

Bin 1492: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 347 Color: 1
Size: 284 Color: 0

Bin 1493: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 318 Color: 1
Size: 266 Color: 0

Bin 1494: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 295 Color: 1
Size: 295 Color: 0

Bin 1495: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 347 Color: 1
Size: 284 Color: 0

Bin 1496: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 320 Color: 1
Size: 278 Color: 0

Bin 1497: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 301 Color: 1
Size: 256 Color: 0

Bin 1498: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 267 Color: 1
Size: 254 Color: 0

Bin 1499: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 282 Color: 1
Size: 274 Color: 0

Bin 1500: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 282 Color: 1
Size: 258 Color: 0

Bin 1501: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 340 Color: 1
Size: 296 Color: 0

Bin 1502: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 306 Color: 1
Size: 278 Color: 0

Bin 1503: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 343 Color: 1
Size: 277 Color: 0

Bin 1504: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 356 Color: 1
Size: 253 Color: 0

Bin 1505: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 308 Color: 1
Size: 275 Color: 0

Bin 1506: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 346 Color: 1
Size: 302 Color: 0

Bin 1507: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 307 Color: 1
Size: 263 Color: 0

Bin 1508: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 297 Color: 1
Size: 254 Color: 0

Bin 1509: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 268 Color: 1
Size: 253 Color: 0

Bin 1510: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 286 Color: 1
Size: 256 Color: 0

Bin 1511: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 355 Color: 1
Size: 270 Color: 0

Bin 1512: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 329 Color: 1
Size: 319 Color: 0

Bin 1513: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 344 Color: 1
Size: 299 Color: 0

Bin 1514: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 260 Color: 1
Size: 251 Color: 0

Bin 1515: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 303 Color: 1
Size: 255 Color: 0

Bin 1516: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 301 Color: 0
Size: 282 Color: 1

Bin 1517: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 254 Color: 0

Bin 1518: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 356 Color: 1
Size: 252 Color: 0

Bin 1519: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 353 Color: 1
Size: 294 Color: 0

Bin 1520: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 320 Color: 1
Size: 288 Color: 0

Bin 1521: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 358 Color: 1
Size: 278 Color: 0

Bin 1522: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 343 Color: 1
Size: 303 Color: 0

Bin 1523: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 357 Color: 1
Size: 287 Color: 0

Bin 1524: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 282 Color: 1
Size: 254 Color: 0

Bin 1525: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 356 Color: 1
Size: 255 Color: 0

Bin 1526: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 314 Color: 0
Size: 289 Color: 1

Bin 1527: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 355 Color: 1
Size: 290 Color: 0

Bin 1528: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 325 Color: 0
Size: 324 Color: 1

Bin 1529: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 353 Color: 1
Size: 277 Color: 0

Bin 1530: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 274 Color: 1
Size: 257 Color: 0

Bin 1531: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 317 Color: 0
Size: 289 Color: 1

Bin 1532: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 309 Color: 0
Size: 274 Color: 1

Bin 1533: 0 of cap free
Amount of items: 3
Items: 
Size: 344 Color: 1
Size: 335 Color: 1
Size: 322 Color: 0

Bin 1534: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 314 Color: 0
Size: 279 Color: 1

Bin 1535: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 346 Color: 1
Size: 303 Color: 0

Bin 1536: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 324 Color: 0
Size: 298 Color: 1

Bin 1537: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 326 Color: 1
Size: 316 Color: 0

Bin 1538: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 327 Color: 1
Size: 314 Color: 0

Bin 1539: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 326 Color: 1
Size: 316 Color: 0

Bin 1540: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 307 Color: 0
Size: 277 Color: 1

Bin 1541: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 324 Color: 1
Size: 318 Color: 0

Bin 1542: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 323 Color: 0
Size: 304 Color: 1

Bin 1543: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 358 Color: 1
Size: 278 Color: 0

Bin 1544: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 325 Color: 1
Size: 317 Color: 0

Bin 1545: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 336 Color: 1
Size: 307 Color: 0

Bin 1546: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 303 Color: 0
Size: 279 Color: 1

Bin 1547: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 343 Color: 1
Size: 300 Color: 0

Bin 1548: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 332 Color: 1
Size: 283 Color: 0

Bin 1549: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 317 Color: 1
Size: 270 Color: 0

Bin 1550: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 329 Color: 1
Size: 314 Color: 0

Bin 1551: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 330 Color: 0
Size: 312 Color: 1

Bin 1552: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 301 Color: 0
Size: 287 Color: 1

Bin 1553: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 324 Color: 0
Size: 321 Color: 1

Bin 1554: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 276 Color: 0
Size: 270 Color: 1

Bin 1555: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 358 Color: 1
Size: 264 Color: 0

Bin 1556: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 332 Color: 1
Size: 312 Color: 0

Bin 1557: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 356 Color: 1
Size: 268 Color: 0

Bin 1558: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 327 Color: 0
Size: 302 Color: 1

Bin 1559: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 359 Color: 1
Size: 282 Color: 0

Bin 1560: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 1
Size: 334 Color: 1
Size: 319 Color: 0

Bin 1561: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 356 Color: 1
Size: 288 Color: 0

Bin 1562: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 324 Color: 0
Size: 319 Color: 1

Bin 1563: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 331 Color: 1
Size: 313 Color: 0

Bin 1564: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 308 Color: 0
Size: 285 Color: 1

Bin 1565: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 331 Color: 1
Size: 313 Color: 0

Bin 1566: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 312 Color: 0
Size: 264 Color: 1

Bin 1567: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 316 Color: 0
Size: 272 Color: 1

Bin 1568: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 327 Color: 0
Size: 315 Color: 1

Bin 1569: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 301 Color: 0
Size: 277 Color: 1

Bin 1570: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 337 Color: 1
Size: 273 Color: 0

Bin 1571: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 313 Color: 0
Size: 292 Color: 1

Bin 1572: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 313 Color: 0
Size: 293 Color: 1

Bin 1573: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 325 Color: 0
Size: 319 Color: 1

Bin 1574: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 350 Color: 1
Size: 264 Color: 0

Bin 1575: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 353 Color: 1
Size: 293 Color: 0

Bin 1576: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 329 Color: 1
Size: 317 Color: 0

Bin 1577: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 329 Color: 1
Size: 317 Color: 0

Bin 1578: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 356 Color: 1
Size: 285 Color: 0

Bin 1579: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 344 Color: 1
Size: 299 Color: 0

Bin 1580: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 353 Color: 1
Size: 293 Color: 0

Bin 1581: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 324 Color: 0
Size: 293 Color: 1

Bin 1582: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 354 Color: 1
Size: 292 Color: 0

Bin 1583: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 353 Color: 1
Size: 293 Color: 0

Bin 1584: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 352 Color: 1
Size: 296 Color: 0

Bin 1585: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 352 Color: 1
Size: 297 Color: 0

Bin 1586: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 340 Color: 1
Size: 303 Color: 0

Bin 1587: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 336 Color: 1
Size: 307 Color: 0

Bin 1588: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 325 Color: 1
Size: 317 Color: 0

Bin 1589: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 325 Color: 0
Size: 320 Color: 1

Bin 1590: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 308 Color: 0
Size: 289 Color: 1

Bin 1591: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 357 Color: 1
Size: 270 Color: 0

Bin 1592: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 330 Color: 1
Size: 314 Color: 0

Bin 1593: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 321 Color: 1
Size: 314 Color: 0

Bin 1594: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 328 Color: 1
Size: 318 Color: 0

Bin 1595: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 330 Color: 1
Size: 314 Color: 0

Bin 1596: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 335 Color: 1
Size: 312 Color: 0

Bin 1597: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 334 Color: 1
Size: 312 Color: 0

Bin 1598: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 319 Color: 0
Size: 257 Color: 1

Bin 1599: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 1
Size: 337 Color: 1
Size: 322 Color: 0

Bin 1600: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 323 Color: 0
Size: 319 Color: 1

Bin 1601: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 325 Color: 1
Size: 319 Color: 0

Bin 1602: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 357 Color: 1
Size: 283 Color: 0

Bin 1603: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 349 Color: 1
Size: 301 Color: 0

Bin 1604: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 1
Size: 349 Color: 1
Size: 301 Color: 0

Bin 1605: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 348 Color: 1
Size: 300 Color: 0

Bin 1606: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 347 Color: 1
Size: 300 Color: 0

Bin 1607: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 347 Color: 1
Size: 300 Color: 0

Bin 1608: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 347 Color: 1
Size: 299 Color: 0

Bin 1609: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 347 Color: 1
Size: 299 Color: 0

Bin 1610: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 347 Color: 1
Size: 299 Color: 0

Bin 1611: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 347 Color: 1
Size: 299 Color: 0

Bin 1612: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 347 Color: 1
Size: 297 Color: 0

Bin 1613: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 343 Color: 1
Size: 299 Color: 0

Bin 1614: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 346 Color: 1
Size: 296 Color: 0

Bin 1615: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 339 Color: 1
Size: 301 Color: 0

Bin 1616: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 346 Color: 1
Size: 296 Color: 0

Bin 1617: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 331 Color: 1
Size: 310 Color: 0

Bin 1618: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 343 Color: 1
Size: 300 Color: 0

Bin 1619: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 345 Color: 1
Size: 304 Color: 0

Bin 1620: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 341 Color: 1
Size: 300 Color: 0

Bin 1621: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 329 Color: 1
Size: 312 Color: 0

Bin 1622: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 359 Color: 1
Size: 268 Color: 0

Bin 1623: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 349 Color: 1
Size: 297 Color: 0

Bin 1624: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 334 Color: 1
Size: 307 Color: 0

Bin 1625: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 341 Color: 1
Size: 300 Color: 0

Bin 1626: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 322 Color: 0
Size: 319 Color: 1

Bin 1627: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 324 Color: 0
Size: 317 Color: 1

Bin 1628: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 338 Color: 1
Size: 303 Color: 0

Bin 1629: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 342 Color: 1
Size: 299 Color: 0

Bin 1630: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 345 Color: 1
Size: 296 Color: 0

Bin 1631: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 330 Color: 1
Size: 310 Color: 0

Bin 1632: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 328 Color: 1
Size: 312 Color: 0

Bin 1633: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 345 Color: 1
Size: 296 Color: 0

Bin 1634: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 333 Color: 1
Size: 307 Color: 0

Bin 1635: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 340 Color: 1
Size: 300 Color: 0

Bin 1636: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 332 Color: 1
Size: 308 Color: 0

Bin 1637: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 330 Color: 1
Size: 310 Color: 0

Bin 1638: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 333 Color: 1
Size: 307 Color: 0

Bin 1639: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 344 Color: 1
Size: 296 Color: 0

Bin 1640: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 323 Color: 1
Size: 317 Color: 0

Bin 1641: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 343 Color: 1
Size: 299 Color: 0

Bin 1642: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 330 Color: 1
Size: 310 Color: 0

Bin 1643: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 327 Color: 1
Size: 312 Color: 0

Bin 1644: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 340 Color: 1
Size: 300 Color: 0

Bin 1645: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 350 Color: 1
Size: 288 Color: 0

Bin 1646: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 324 Color: 1
Size: 316 Color: 0

Bin 1647: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 345 Color: 1
Size: 296 Color: 0

Bin 1648: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 347 Color: 1
Size: 293 Color: 0

Bin 1649: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 346 Color: 1
Size: 288 Color: 0

Bin 1650: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 326 Color: 1
Size: 313 Color: 0

Bin 1651: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 346 Color: 1
Size: 294 Color: 0

Bin 1652: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 331 Color: 1
Size: 307 Color: 0

Bin 1653: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 329 Color: 1
Size: 310 Color: 0

Bin 1654: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 327 Color: 0
Size: 312 Color: 1

Bin 1655: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 345 Color: 1
Size: 294 Color: 0

Bin 1656: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 332 Color: 1
Size: 307 Color: 0

Bin 1657: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 343 Color: 1
Size: 296 Color: 0

Bin 1658: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 337 Color: 1
Size: 307 Color: 0

Bin 1659: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 340 Color: 1
Size: 299 Color: 0

Bin 1660: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 352 Color: 1
Size: 287 Color: 0

Bin 1661: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 326 Color: 1
Size: 311 Color: 0

Bin 1662: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 337 Color: 1
Size: 301 Color: 0

Bin 1663: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 352 Color: 1
Size: 286 Color: 0

Bin 1664: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 325 Color: 0
Size: 313 Color: 1

Bin 1665: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 345 Color: 1
Size: 293 Color: 0

Bin 1666: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 345 Color: 1
Size: 293 Color: 0

Bin 1667: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 351 Color: 1
Size: 287 Color: 0

Bin 1668: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 344 Color: 1
Size: 294 Color: 0

Bin 1669: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 356 Color: 1
Size: 286 Color: 0

Bin 1670: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 352 Color: 1
Size: 286 Color: 0

Bin 1671: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 327 Color: 0
Size: 311 Color: 1

Bin 1672: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 344 Color: 1
Size: 294 Color: 0

Bin 1673: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 325 Color: 1
Size: 313 Color: 0

Bin 1674: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 340 Color: 1
Size: 298 Color: 0

Bin 1675: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 326 Color: 1
Size: 312 Color: 0

Bin 1676: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 336 Color: 1
Size: 303 Color: 0

Bin 1677: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 343 Color: 1
Size: 295 Color: 0

Bin 1678: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 341 Color: 1
Size: 299 Color: 0

Bin 1679: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 324 Color: 1
Size: 314 Color: 0

Bin 1680: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 350 Color: 1
Size: 287 Color: 0

Bin 1681: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 353 Color: 1
Size: 285 Color: 0

Bin 1682: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 339 Color: 1
Size: 298 Color: 0

Bin 1683: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 329 Color: 1
Size: 308 Color: 0

Bin 1684: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 319 Color: 1
Size: 318 Color: 0

Bin 1685: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 327 Color: 0
Size: 310 Color: 1

Bin 1686: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 325 Color: 1
Size: 313 Color: 0

Bin 1687: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 342 Color: 1
Size: 295 Color: 0

Bin 1688: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 351 Color: 1
Size: 286 Color: 0

Bin 1689: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 342 Color: 1
Size: 295 Color: 0

Bin 1690: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 326 Color: 1
Size: 311 Color: 0

Bin 1691: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 334 Color: 1
Size: 303 Color: 0

Bin 1692: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 338 Color: 1
Size: 299 Color: 0

Bin 1693: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 340 Color: 1
Size: 297 Color: 0

Bin 1694: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 345 Color: 1
Size: 292 Color: 0

Bin 1695: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 343 Color: 1
Size: 301 Color: 0

Bin 1696: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 351 Color: 1
Size: 286 Color: 0

Bin 1697: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 352 Color: 1
Size: 285 Color: 0

Bin 1698: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 344 Color: 1
Size: 293 Color: 0

Bin 1699: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 331 Color: 1
Size: 314 Color: 0

Bin 1700: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 351 Color: 1
Size: 286 Color: 0

Bin 1701: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 320 Color: 1
Size: 316 Color: 0

Bin 1702: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 342 Color: 1
Size: 294 Color: 0

Bin 1703: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 351 Color: 1
Size: 285 Color: 0

Bin 1704: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 349 Color: 1
Size: 287 Color: 0

Bin 1705: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 323 Color: 0
Size: 313 Color: 1

Bin 1706: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 344 Color: 1
Size: 292 Color: 0

Bin 1707: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 331 Color: 1
Size: 305 Color: 0

Bin 1708: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 326 Color: 1
Size: 310 Color: 0

Bin 1709: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 325 Color: 1
Size: 311 Color: 0

Bin 1710: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 343 Color: 1
Size: 293 Color: 0

Bin 1711: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 349 Color: 1
Size: 287 Color: 0

Bin 1712: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 344 Color: 1
Size: 292 Color: 0

Bin 1713: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 351 Color: 1
Size: 285 Color: 0

Bin 1714: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 351 Color: 1
Size: 285 Color: 0

Bin 1715: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 325 Color: 1
Size: 311 Color: 0

Bin 1716: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 320 Color: 0
Size: 316 Color: 1

Bin 1717: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 345 Color: 1
Size: 292 Color: 0

Bin 1718: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 338 Color: 1
Size: 298 Color: 0

Bin 1719: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 328 Color: 1
Size: 308 Color: 0

Bin 1720: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 343 Color: 1
Size: 293 Color: 0

Bin 1721: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 328 Color: 1
Size: 308 Color: 0

Bin 1722: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 350 Color: 1
Size: 286 Color: 0

Bin 1723: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 329 Color: 1
Size: 307 Color: 0

Bin 1724: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 350 Color: 1
Size: 286 Color: 0

Bin 1725: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 335 Color: 1
Size: 300 Color: 0

Bin 1726: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 350 Color: 1
Size: 285 Color: 0

Bin 1727: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 331 Color: 1
Size: 304 Color: 0

Bin 1728: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 1
Size: 320 Color: 1
Size: 319 Color: 0

Bin 1729: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 334 Color: 1
Size: 299 Color: 0

Bin 1730: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 347 Color: 1
Size: 288 Color: 0

Bin 1731: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 350 Color: 1
Size: 285 Color: 0

Bin 1732: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 332 Color: 1
Size: 303 Color: 0

Bin 1733: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 330 Color: 1
Size: 305 Color: 0

Bin 1734: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 332 Color: 1
Size: 305 Color: 0

Bin 1735: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 355 Color: 1
Size: 280 Color: 0

Bin 1736: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 320 Color: 1
Size: 315 Color: 0

Bin 1737: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 321 Color: 1
Size: 314 Color: 0

Bin 1738: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 351 Color: 1
Size: 284 Color: 0

Bin 1739: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 355 Color: 1
Size: 280 Color: 0

Bin 1740: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 319 Color: 0
Size: 316 Color: 1

Bin 1741: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 1
Size: 327 Color: 1
Size: 311 Color: 0

Bin 1742: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 1
Size: 336 Color: 1
Size: 300 Color: 0

Bin 1743: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 337 Color: 1
Size: 298 Color: 0

Bin 1744: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 327 Color: 1
Size: 308 Color: 0

Bin 1745: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 343 Color: 1
Size: 292 Color: 0

Bin 1746: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 340 Color: 1
Size: 294 Color: 0

Bin 1747: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 346 Color: 1
Size: 288 Color: 0

Bin 1748: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 347 Color: 1
Size: 287 Color: 0

Bin 1749: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 332 Color: 0
Size: 302 Color: 1

Bin 1750: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 324 Color: 0
Size: 310 Color: 1

Bin 1751: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 349 Color: 1
Size: 285 Color: 0

Bin 1752: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 344 Color: 1
Size: 290 Color: 0

Bin 1753: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 339 Color: 1
Size: 295 Color: 0

Bin 1754: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 326 Color: 1
Size: 308 Color: 0

Bin 1755: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 1
Size: 333 Color: 1
Size: 318 Color: 0

Bin 1756: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 337 Color: 1
Size: 297 Color: 0

Bin 1757: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 322 Color: 1
Size: 312 Color: 0

Bin 1758: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 331 Color: 1
Size: 303 Color: 0

Bin 1759: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 325 Color: 1
Size: 309 Color: 0

Bin 1760: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 336 Color: 1
Size: 296 Color: 0

Bin 1761: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 347 Color: 1
Size: 287 Color: 0

Bin 1762: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 343 Color: 1
Size: 291 Color: 0

Bin 1763: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 319 Color: 1
Size: 315 Color: 0

Bin 1764: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 331 Color: 1
Size: 303 Color: 0

Bin 1765: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 350 Color: 1
Size: 284 Color: 0

Bin 1766: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 327 Color: 1
Size: 307 Color: 0

Bin 1767: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 348 Color: 1
Size: 286 Color: 0

Bin 1768: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 342 Color: 1
Size: 292 Color: 0

Bin 1769: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 353 Color: 1
Size: 280 Color: 0

Bin 1770: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 322 Color: 1
Size: 312 Color: 0

Bin 1771: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 321 Color: 0
Size: 312 Color: 1

Bin 1772: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 317 Color: 1
Size: 316 Color: 0

Bin 1773: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 323 Color: 1
Size: 310 Color: 0

Bin 1774: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 321 Color: 1
Size: 312 Color: 0

Bin 1775: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 349 Color: 1
Size: 284 Color: 0

Bin 1776: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 335 Color: 1
Size: 298 Color: 0

Bin 1777: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 338 Color: 1
Size: 297 Color: 0

Bin 1778: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 348 Color: 1
Size: 285 Color: 0

Bin 1779: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 333 Color: 1
Size: 300 Color: 0

Bin 1780: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 351 Color: 1
Size: 282 Color: 0

Bin 1781: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 342 Color: 1
Size: 290 Color: 0

Bin 1782: 0 of cap free
Amount of items: 3
Items: 
Size: 343 Color: 1
Size: 335 Color: 1
Size: 323 Color: 0

Bin 1783: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 350 Color: 1
Size: 283 Color: 0

Bin 1784: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 341 Color: 1
Size: 292 Color: 0

Bin 1785: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 345 Color: 1
Size: 285 Color: 0

Bin 1786: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 349 Color: 1
Size: 284 Color: 0

Bin 1787: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 348 Color: 1
Size: 284 Color: 0

Bin 1788: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 334 Color: 1
Size: 298 Color: 0

Bin 1789: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 342 Color: 1
Size: 290 Color: 0

Bin 1790: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 336 Color: 1
Size: 293 Color: 0

Bin 1791: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 355 Color: 1
Size: 251 Color: 0

Bin 1792: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 334 Color: 1
Size: 292 Color: 0

Bin 1793: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 345 Color: 1
Size: 287 Color: 0

Bin 1794: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 346 Color: 1
Size: 280 Color: 0

Bin 1795: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 342 Color: 1
Size: 290 Color: 0

Bin 1796: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 341 Color: 1
Size: 291 Color: 0

Bin 1797: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 320 Color: 0
Size: 312 Color: 1

Bin 1798: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 330 Color: 1
Size: 302 Color: 0

Bin 1799: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 349 Color: 1
Size: 283 Color: 0

Bin 1800: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 328 Color: 0
Size: 309 Color: 1

Bin 1801: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 322 Color: 1
Size: 310 Color: 0

Bin 1802: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 317 Color: 1
Size: 315 Color: 0

Bin 1803: 0 of cap free
Amount of items: 3
Items: 
Size: 349 Color: 1
Size: 332 Color: 1
Size: 320 Color: 0

Bin 1804: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 345 Color: 1
Size: 287 Color: 0

Bin 1805: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 342 Color: 1
Size: 289 Color: 0

Bin 1806: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 329 Color: 1
Size: 303 Color: 0

Bin 1807: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 349 Color: 1
Size: 283 Color: 0

Bin 1808: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 340 Color: 1
Size: 292 Color: 0

Bin 1809: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 338 Color: 1
Size: 293 Color: 0

Bin 1810: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 349 Color: 1
Size: 283 Color: 0

Bin 1811: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 329 Color: 1
Size: 277 Color: 0

Bin 1812: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 334 Color: 1
Size: 298 Color: 0

Bin 1813: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 355 Color: 1
Size: 277 Color: 0

Bin 1814: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 329 Color: 1
Size: 302 Color: 0

Bin 1815: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 299 Color: 0
Size: 293 Color: 1

Bin 1816: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 327 Color: 1
Size: 304 Color: 0

Bin 1817: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 328 Color: 1
Size: 303 Color: 0

Bin 1818: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 317 Color: 0
Size: 314 Color: 1

Bin 1819: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 347 Color: 1
Size: 272 Color: 0

Bin 1820: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 329 Color: 1
Size: 302 Color: 0

Bin 1821: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 340 Color: 1
Size: 291 Color: 0

Bin 1822: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 335 Color: 1
Size: 296 Color: 0

Bin 1823: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 326 Color: 1
Size: 305 Color: 0

Bin 1824: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 327 Color: 1
Size: 304 Color: 0

Bin 1825: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 316 Color: 0
Size: 315 Color: 1

Bin 1826: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 334 Color: 1
Size: 297 Color: 0

Bin 1827: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 347 Color: 1
Size: 275 Color: 0

Bin 1828: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 331 Color: 1
Size: 300 Color: 0

Bin 1829: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 321 Color: 0
Size: 309 Color: 1

Bin 1830: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 355 Color: 1
Size: 275 Color: 0

Bin 1831: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 327 Color: 1
Size: 317 Color: 0

Bin 1832: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 344 Color: 1
Size: 286 Color: 0

Bin 1833: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 316 Color: 0
Size: 314 Color: 1

Bin 1834: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 336 Color: 1
Size: 294 Color: 0

Bin 1835: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 338 Color: 1
Size: 292 Color: 0

Bin 1836: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 355 Color: 1
Size: 275 Color: 0

Bin 1837: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 326 Color: 1
Size: 304 Color: 0

Bin 1838: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 353 Color: 1
Size: 277 Color: 0

Bin 1839: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 343 Color: 1
Size: 287 Color: 0

Bin 1840: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 329 Color: 1
Size: 300 Color: 0

Bin 1841: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 338 Color: 1
Size: 291 Color: 0

Bin 1842: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 349 Color: 1
Size: 271 Color: 0

Bin 1843: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 334 Color: 1
Size: 295 Color: 0

Bin 1844: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 349 Color: 1
Size: 281 Color: 0

Bin 1845: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 357 Color: 1
Size: 272 Color: 0

Bin 1846: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 352 Color: 1
Size: 277 Color: 0

Bin 1847: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 321 Color: 1
Size: 301 Color: 0

Bin 1848: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 349 Color: 1
Size: 280 Color: 0

Bin 1849: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 332 Color: 1
Size: 297 Color: 0

Bin 1850: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 317 Color: 1
Size: 312 Color: 0

Bin 1851: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 352 Color: 1
Size: 279 Color: 0

Bin 1852: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 349 Color: 1
Size: 280 Color: 0

Bin 1853: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 349 Color: 1
Size: 280 Color: 0

Bin 1854: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 330 Color: 0
Size: 299 Color: 1

Bin 1855: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 351 Color: 1
Size: 278 Color: 0

Bin 1856: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 342 Color: 1
Size: 287 Color: 0

Bin 1857: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 354 Color: 1
Size: 275 Color: 0

Bin 1858: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 319 Color: 0
Size: 310 Color: 1

Bin 1859: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 344 Color: 1
Size: 298 Color: 0

Bin 1860: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 315 Color: 0
Size: 313 Color: 1

Bin 1861: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 350 Color: 1
Size: 278 Color: 0

Bin 1862: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 329 Color: 1
Size: 298 Color: 0

Bin 1863: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 341 Color: 1
Size: 287 Color: 0

Bin 1864: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 333 Color: 1
Size: 295 Color: 0

Bin 1865: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 351 Color: 1
Size: 277 Color: 0

Bin 1866: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 331 Color: 0
Size: 297 Color: 1

Bin 1867: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 355 Color: 1
Size: 273 Color: 0

Bin 1868: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 335 Color: 1
Size: 308 Color: 0

Bin 1869: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 335 Color: 1
Size: 293 Color: 0

Bin 1870: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 350 Color: 1
Size: 278 Color: 0

Bin 1871: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 358 Color: 1
Size: 270 Color: 0

Bin 1872: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 342 Color: 1
Size: 286 Color: 0

Bin 1873: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 329 Color: 1
Size: 299 Color: 0

Bin 1874: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 356 Color: 1
Size: 272 Color: 0

Bin 1875: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 363 Color: 1
Size: 265 Color: 0

Bin 1876: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 349 Color: 1
Size: 278 Color: 0

Bin 1877: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 318 Color: 0
Size: 309 Color: 1

Bin 1878: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 353 Color: 1
Size: 267 Color: 0

Bin 1879: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 368 Color: 1
Size: 259 Color: 0

Bin 1880: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 348 Color: 1
Size: 279 Color: 0

Bin 1881: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 363 Color: 1
Size: 267 Color: 0

Bin 1882: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 349 Color: 1
Size: 278 Color: 0

Bin 1883: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 345 Color: 1
Size: 282 Color: 0

Bin 1884: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 321 Color: 1
Size: 306 Color: 0

Bin 1885: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 341 Color: 1
Size: 291 Color: 0

Bin 1886: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 344 Color: 1
Size: 283 Color: 0

Bin 1887: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 348 Color: 1
Size: 279 Color: 0

Bin 1888: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 328 Color: 1
Size: 299 Color: 0

Bin 1889: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 364 Color: 1
Size: 263 Color: 0

Bin 1890: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 353 Color: 1
Size: 274 Color: 0

Bin 1891: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 323 Color: 1
Size: 304 Color: 0

Bin 1892: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 331 Color: 1
Size: 296 Color: 0

Bin 1893: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 339 Color: 1
Size: 298 Color: 0

Bin 1894: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 357 Color: 1
Size: 270 Color: 0

Bin 1895: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 344 Color: 1
Size: 282 Color: 0

Bin 1896: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 348 Color: 1
Size: 275 Color: 0

Bin 1897: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 343 Color: 1
Size: 283 Color: 0

Bin 1898: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 324 Color: 1
Size: 286 Color: 0

Bin 1899: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 319 Color: 1
Size: 307 Color: 0

Bin 1900: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 332 Color: 1
Size: 293 Color: 0

Bin 1901: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 349 Color: 1
Size: 277 Color: 0

Bin 1902: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 356 Color: 1
Size: 270 Color: 0

Bin 1903: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 322 Color: 1
Size: 304 Color: 0

Bin 1904: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 317 Color: 1
Size: 309 Color: 0

Bin 1905: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 354 Color: 1
Size: 272 Color: 0

Bin 1906: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 335 Color: 1
Size: 275 Color: 0

Bin 1907: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 353 Color: 1
Size: 272 Color: 0

Bin 1908: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 323 Color: 1
Size: 302 Color: 0

Bin 1909: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 328 Color: 1
Size: 297 Color: 0

Bin 1910: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 321 Color: 1
Size: 304 Color: 0

Bin 1911: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 326 Color: 1
Size: 322 Color: 0

Bin 1912: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 330 Color: 1
Size: 295 Color: 0

Bin 1913: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 347 Color: 1
Size: 278 Color: 0

Bin 1914: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 340 Color: 1
Size: 284 Color: 0

Bin 1915: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 361 Color: 1
Size: 263 Color: 0

Bin 1916: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 324 Color: 1
Size: 300 Color: 0

Bin 1917: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 337 Color: 1
Size: 287 Color: 0

Bin 1918: 0 of cap free
Amount of items: 3
Items: 
Size: 356 Color: 1
Size: 327 Color: 1
Size: 318 Color: 0

Bin 1919: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 349 Color: 1
Size: 275 Color: 0

Bin 1920: 0 of cap free
Amount of items: 3
Items: 
Size: 352 Color: 1
Size: 352 Color: 1
Size: 297 Color: 0

Bin 1921: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 329 Color: 1
Size: 295 Color: 0

Bin 1922: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 346 Color: 1
Size: 278 Color: 0

Bin 1923: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 319 Color: 1
Size: 305 Color: 0

Bin 1924: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 348 Color: 1
Size: 276 Color: 0

Bin 1925: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 344 Color: 1
Size: 280 Color: 0

Bin 1926: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 342 Color: 1
Size: 282 Color: 0

Bin 1927: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 317 Color: 1
Size: 274 Color: 0

Bin 1928: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 332 Color: 0
Size: 315 Color: 1

Bin 1929: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 332 Color: 1
Size: 292 Color: 0

Bin 1930: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 341 Color: 1
Size: 283 Color: 0

Bin 1931: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 359 Color: 1
Size: 258 Color: 0

Bin 1932: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 313 Color: 1
Size: 310 Color: 0

Bin 1933: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 317 Color: 0
Size: 306 Color: 1

Bin 1934: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 339 Color: 1
Size: 264 Color: 0

Bin 1935: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 319 Color: 0
Size: 304 Color: 1

Bin 1936: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 351 Color: 1
Size: 272 Color: 0

Bin 1937: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 320 Color: 1
Size: 303 Color: 0

Bin 1938: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 313 Color: 1
Size: 310 Color: 0

Bin 1939: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 326 Color: 1
Size: 297 Color: 0

Bin 1940: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 325 Color: 1
Size: 298 Color: 0

Bin 1941: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 345 Color: 1
Size: 278 Color: 0

Bin 1942: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 314 Color: 0
Size: 309 Color: 1

Bin 1943: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 341 Color: 1
Size: 282 Color: 0

Bin 1944: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 329 Color: 1
Size: 294 Color: 0

Bin 1945: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 318 Color: 1
Size: 286 Color: 0

Bin 1946: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 315 Color: 1
Size: 309 Color: 0

Bin 1947: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 338 Color: 1
Size: 285 Color: 0

Bin 1948: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 360 Color: 1
Size: 263 Color: 0

Bin 1949: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 335 Color: 1
Size: 288 Color: 0

Bin 1950: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 325 Color: 1
Size: 316 Color: 0

Bin 1951: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 344 Color: 1
Size: 279 Color: 0

Bin 1952: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 351 Color: 1
Size: 272 Color: 0

Bin 1953: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 321 Color: 0
Size: 302 Color: 1

Bin 1954: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 348 Color: 1
Size: 275 Color: 0

Bin 1955: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 333 Color: 1
Size: 290 Color: 0

Bin 1956: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 347 Color: 1
Size: 276 Color: 0

Bin 1957: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 318 Color: 1
Size: 304 Color: 0

Bin 1958: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 349 Color: 1
Size: 274 Color: 0

Bin 1959: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 358 Color: 1
Size: 279 Color: 0

Bin 1960: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 359 Color: 1
Size: 263 Color: 0

Bin 1961: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 349 Color: 1
Size: 273 Color: 0

Bin 1962: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 312 Color: 1
Size: 310 Color: 0

Bin 1963: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 340 Color: 1
Size: 282 Color: 0

Bin 1964: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 334 Color: 1
Size: 288 Color: 0

Bin 1965: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 344 Color: 1
Size: 278 Color: 0

Bin 1966: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 332 Color: 1
Size: 290 Color: 0

Bin 1967: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 339 Color: 1
Size: 283 Color: 0

Bin 1968: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 336 Color: 1
Size: 286 Color: 0

Bin 1969: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 313 Color: 0
Size: 308 Color: 1

Bin 1970: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 334 Color: 1
Size: 287 Color: 0

Bin 1971: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 312 Color: 1
Size: 309 Color: 0

Bin 1972: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 351 Color: 1
Size: 270 Color: 0

Bin 1973: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 347 Color: 1
Size: 274 Color: 0

Bin 1974: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 351 Color: 1
Size: 270 Color: 0

Bin 1975: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 338 Color: 1
Size: 283 Color: 0

Bin 1976: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 335 Color: 1
Size: 286 Color: 0

Bin 1977: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 343 Color: 1
Size: 283 Color: 0

Bin 1978: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 328 Color: 1
Size: 293 Color: 0

Bin 1979: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 358 Color: 1
Size: 263 Color: 0

Bin 1980: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 327 Color: 1
Size: 294 Color: 0

Bin 1981: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 316 Color: 1
Size: 305 Color: 0

Bin 1982: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 316 Color: 1
Size: 305 Color: 0

Bin 1983: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 324 Color: 1
Size: 296 Color: 0

Bin 1984: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 331 Color: 1
Size: 289 Color: 0

Bin 1985: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 323 Color: 1
Size: 297 Color: 0

Bin 1986: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 327 Color: 1
Size: 294 Color: 0

Bin 1987: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 348 Color: 1
Size: 272 Color: 0

Bin 1988: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 314 Color: 1
Size: 306 Color: 0

Bin 1989: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 358 Color: 1
Size: 279 Color: 0

Bin 1990: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 319 Color: 1
Size: 301 Color: 0

Bin 1991: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 342 Color: 1
Size: 278 Color: 0

Bin 1992: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 351 Color: 1
Size: 268 Color: 0

Bin 1993: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 349 Color: 1
Size: 271 Color: 0

Bin 1994: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 331 Color: 1
Size: 289 Color: 0

Bin 1995: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 349 Color: 1
Size: 271 Color: 0

Bin 1996: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 329 Color: 1
Size: 315 Color: 0

Bin 1997: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 351 Color: 1
Size: 269 Color: 0

Bin 1998: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 342 Color: 1
Size: 278 Color: 0

Bin 1999: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 335 Color: 1
Size: 266 Color: 0

Bin 2000: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 348 Color: 1
Size: 285 Color: 0

Bin 2001: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 329 Color: 1
Size: 289 Color: 0

Bin 2002: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 319 Color: 1
Size: 300 Color: 0

Bin 2003: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 336 Color: 1
Size: 283 Color: 0

Bin 2004: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 349 Color: 1
Size: 270 Color: 0

Bin 2005: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 331 Color: 1
Size: 288 Color: 0

Bin 2006: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 352 Color: 1
Size: 266 Color: 0

Bin 2007: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 345 Color: 1
Size: 273 Color: 0

Bin 2008: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 323 Color: 1
Size: 264 Color: 0

Bin 2009: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 327 Color: 1
Size: 292 Color: 0

Bin 2010: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 343 Color: 1
Size: 276 Color: 0

Bin 2011: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 311 Color: 1
Size: 306 Color: 0

Bin 2012: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 318 Color: 1
Size: 301 Color: 0

Bin 2013: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 329 Color: 1
Size: 290 Color: 0

Bin 2014: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 317 Color: 0
Size: 302 Color: 1

Bin 2015: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 323 Color: 1
Size: 297 Color: 0

Bin 2016: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 324 Color: 1
Size: 294 Color: 0

Bin 2017: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 323 Color: 1
Size: 295 Color: 0

Bin 2018: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 327 Color: 1
Size: 291 Color: 0

Bin 2019: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 338 Color: 1
Size: 280 Color: 0

Bin 2020: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 1
Size: 342 Color: 1
Size: 301 Color: 0

Bin 2021: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 316 Color: 1
Size: 302 Color: 0

Bin 2022: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 336 Color: 1
Size: 282 Color: 0

Bin 2023: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 338 Color: 1
Size: 280 Color: 0

Bin 2024: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 329 Color: 1
Size: 289 Color: 0

Bin 2025: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 315 Color: 1
Size: 289 Color: 0

Bin 2026: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 315 Color: 1
Size: 303 Color: 0

Bin 2027: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 322 Color: 1
Size: 296 Color: 0

Bin 2028: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 333 Color: 1
Size: 285 Color: 0

Bin 2029: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 351 Color: 1
Size: 268 Color: 0

Bin 2030: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 348 Color: 1
Size: 270 Color: 0

Bin 2031: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 353 Color: 1
Size: 265 Color: 0

Bin 2032: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 350 Color: 1
Size: 267 Color: 0

Bin 2033: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 1
Size: 352 Color: 1
Size: 269 Color: 0

Bin 2034: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 332 Color: 1
Size: 286 Color: 0

Bin 2035: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 327 Color: 1
Size: 291 Color: 0

Bin 2036: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 347 Color: 1
Size: 271 Color: 0

Bin 2037: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 310 Color: 0
Size: 308 Color: 1

Bin 2038: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 338 Color: 1
Size: 280 Color: 0

Bin 2039: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 323 Color: 1
Size: 294 Color: 0

Bin 2040: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 318 Color: 0
Size: 299 Color: 1

Bin 2041: 0 of cap free
Amount of items: 3
Items: 
Size: 360 Color: 1
Size: 333 Color: 1
Size: 308 Color: 0

Bin 2042: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 314 Color: 0
Size: 303 Color: 1

Bin 2043: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 327 Color: 1
Size: 290 Color: 0

Bin 2044: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 1
Size: 355 Color: 1
Size: 289 Color: 0

Bin 2045: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 342 Color: 1
Size: 275 Color: 0

Bin 2046: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 333 Color: 1
Size: 284 Color: 0

Bin 2047: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 345 Color: 1
Size: 272 Color: 0

Bin 2048: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 335 Color: 1
Size: 284 Color: 0

Bin 2049: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 350 Color: 1
Size: 267 Color: 0

Bin 2050: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 332 Color: 0
Size: 301 Color: 1

Bin 2051: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 339 Color: 1
Size: 278 Color: 0

Bin 2052: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 335 Color: 1
Size: 282 Color: 0

Bin 2053: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 360 Color: 1
Size: 257 Color: 0

Bin 2054: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 360 Color: 1
Size: 257 Color: 0

Bin 2055: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 1
Size: 316 Color: 0
Size: 301 Color: 1

Bin 2056: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 338 Color: 1
Size: 278 Color: 0

Bin 2057: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 359 Color: 1
Size: 257 Color: 0

Bin 2058: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 322 Color: 0
Size: 294 Color: 1

Bin 2059: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 311 Color: 0
Size: 305 Color: 1

Bin 2060: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 322 Color: 1
Size: 294 Color: 0

Bin 2061: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 321 Color: 1
Size: 295 Color: 0

Bin 2062: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 327 Color: 1
Size: 289 Color: 0

Bin 2063: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 335 Color: 1
Size: 281 Color: 0

Bin 2064: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 335 Color: 1
Size: 281 Color: 0

Bin 2065: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 326 Color: 0
Size: 290 Color: 1

Bin 2066: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 319 Color: 1
Size: 297 Color: 0

Bin 2067: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 348 Color: 1
Size: 268 Color: 0

Bin 2068: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 348 Color: 1
Size: 268 Color: 0

Bin 2069: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 338 Color: 1
Size: 278 Color: 0

Bin 2070: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 346 Color: 1
Size: 270 Color: 0

Bin 2071: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 312 Color: 1
Size: 304 Color: 0

Bin 2072: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 330 Color: 1
Size: 285 Color: 0

Bin 2073: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 300 Color: 1
Size: 284 Color: 0

Bin 2074: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 324 Color: 0
Size: 291 Color: 1

Bin 2075: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 309 Color: 1
Size: 306 Color: 0

Bin 2076: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 339 Color: 1
Size: 276 Color: 0

Bin 2077: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 346 Color: 1
Size: 272 Color: 0

Bin 2078: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 352 Color: 1
Size: 263 Color: 0

Bin 2079: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 309 Color: 0
Size: 305 Color: 1

Bin 2080: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 347 Color: 1
Size: 268 Color: 0

Bin 2081: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 323 Color: 1
Size: 291 Color: 0

Bin 2082: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 300 Color: 1
Size: 278 Color: 0

Bin 2083: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 340 Color: 1
Size: 274 Color: 0

Bin 2084: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 344 Color: 1
Size: 268 Color: 0

Bin 2085: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 340 Color: 1
Size: 274 Color: 0

Bin 2086: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 337 Color: 1
Size: 277 Color: 0

Bin 2087: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 1
Size: 337 Color: 1
Size: 278 Color: 0

Bin 2088: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 339 Color: 1
Size: 272 Color: 0

Bin 2089: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 340 Color: 1
Size: 274 Color: 0

Bin 2090: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 320 Color: 1
Size: 294 Color: 0

Bin 2091: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 328 Color: 1
Size: 286 Color: 0

Bin 2092: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 334 Color: 1
Size: 280 Color: 0

Bin 2093: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 341 Color: 1
Size: 272 Color: 0

Bin 2094: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 346 Color: 1
Size: 267 Color: 0

Bin 2095: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 307 Color: 1
Size: 306 Color: 0

Bin 2096: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 340 Color: 1
Size: 273 Color: 0

Bin 2097: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 309 Color: 1
Size: 303 Color: 0

Bin 2098: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 311 Color: 1
Size: 302 Color: 0

Bin 2099: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 338 Color: 1
Size: 264 Color: 0

Bin 2100: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 341 Color: 1
Size: 272 Color: 0

Bin 2101: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 338 Color: 1
Size: 275 Color: 0

Bin 2102: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 331 Color: 1
Size: 282 Color: 0

Bin 2103: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 347 Color: 1
Size: 276 Color: 0

Bin 2104: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 1
Size: 344 Color: 1
Size: 282 Color: 0

Bin 2105: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 307 Color: 0
Size: 305 Color: 1

Bin 2106: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 349 Color: 1
Size: 263 Color: 0

Bin 2107: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 336 Color: 1
Size: 276 Color: 0

Bin 2108: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 343 Color: 1
Size: 269 Color: 0

Bin 2109: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 322 Color: 1
Size: 290 Color: 0

Bin 2110: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 330 Color: 1
Size: 282 Color: 0

Bin 2111: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 309 Color: 1
Size: 303 Color: 0

Bin 2112: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 311 Color: 1
Size: 301 Color: 0

Bin 2113: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 307 Color: 1
Size: 305 Color: 0

Bin 2114: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 314 Color: 1
Size: 298 Color: 0

Bin 2115: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 343 Color: 1
Size: 269 Color: 0

Bin 2116: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 347 Color: 1
Size: 265 Color: 0

Bin 2117: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 337 Color: 1
Size: 275 Color: 0

Bin 2118: 0 of cap free
Amount of items: 3
Items: 
Size: 339 Color: 1
Size: 339 Color: 1
Size: 323 Color: 0

Bin 2119: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 320 Color: 1
Size: 291 Color: 0

Bin 2120: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 321 Color: 1
Size: 290 Color: 0

Bin 2121: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 317 Color: 0
Size: 294 Color: 1

Bin 2122: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 338 Color: 1
Size: 273 Color: 0

Bin 2123: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 311 Color: 1
Size: 300 Color: 0

Bin 2124: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 346 Color: 1
Size: 265 Color: 0

Bin 2125: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 331 Color: 1
Size: 280 Color: 0

Bin 2126: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 343 Color: 1
Size: 268 Color: 0

Bin 2127: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 316 Color: 1
Size: 295 Color: 0

Bin 2128: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 346 Color: 1
Size: 265 Color: 0

Bin 2129: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 339 Color: 1
Size: 274 Color: 0

Bin 2130: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 350 Color: 1
Size: 266 Color: 0

Bin 2131: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 336 Color: 1
Size: 275 Color: 0

Bin 2132: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 307 Color: 1
Size: 303 Color: 0

Bin 2133: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 331 Color: 0
Size: 280 Color: 1

Bin 2134: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 341 Color: 1
Size: 271 Color: 0

Bin 2135: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 1
Size: 355 Color: 1
Size: 264 Color: 0

Bin 2136: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 330 Color: 1
Size: 281 Color: 0

Bin 2137: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 327 Color: 1
Size: 283 Color: 0

Bin 2138: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 345 Color: 1
Size: 265 Color: 0

Bin 2139: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 328 Color: 1
Size: 283 Color: 0

Bin 2140: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 320 Color: 1
Size: 290 Color: 0

Bin 2141: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 327 Color: 1
Size: 283 Color: 0

Bin 2142: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 318 Color: 1
Size: 292 Color: 0

Bin 2143: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 306 Color: 0
Size: 304 Color: 1

Bin 2144: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 355 Color: 1
Size: 282 Color: 0

Bin 2145: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 341 Color: 1
Size: 268 Color: 0

Bin 2146: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 311 Color: 1
Size: 274 Color: 0

Bin 2147: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 331 Color: 1
Size: 278 Color: 0

Bin 2148: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 340 Color: 1
Size: 269 Color: 0

Bin 2149: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 307 Color: 1
Size: 302 Color: 0

Bin 2150: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 338 Color: 1
Size: 271 Color: 0

Bin 2151: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 334 Color: 1
Size: 275 Color: 0

Bin 2152: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 336 Color: 1
Size: 264 Color: 0

Bin 2153: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 338 Color: 1
Size: 271 Color: 0

Bin 2154: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 311 Color: 0
Size: 298 Color: 1

Bin 2155: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 290 Color: 0
Size: 281 Color: 1

Bin 2156: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 307 Color: 0
Size: 302 Color: 1

Bin 2157: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 320 Color: 1
Size: 288 Color: 0

Bin 2158: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 326 Color: 1
Size: 282 Color: 0

Bin 2159: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 337 Color: 1
Size: 272 Color: 0

Bin 2160: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 327 Color: 1
Size: 281 Color: 0

Bin 2161: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 306 Color: 1
Size: 302 Color: 0

Bin 2162: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 352 Color: 1
Size: 256 Color: 0

Bin 2163: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 338 Color: 1
Size: 269 Color: 0

Bin 2164: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 353 Color: 1
Size: 255 Color: 0

Bin 2165: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 336 Color: 1
Size: 272 Color: 0

Bin 2166: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 343 Color: 1
Size: 265 Color: 0

Bin 2167: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 311 Color: 1
Size: 296 Color: 0

Bin 2168: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 320 Color: 1
Size: 288 Color: 0

Bin 2169: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 339 Color: 1
Size: 269 Color: 0

Bin 2170: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 351 Color: 1
Size: 257 Color: 0

Bin 2171: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 306 Color: 0
Size: 302 Color: 1

Bin 2172: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 339 Color: 1
Size: 269 Color: 0

Bin 2173: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 351 Color: 1
Size: 257 Color: 0

Bin 2174: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 351 Color: 1
Size: 257 Color: 0

Bin 2175: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 338 Color: 1
Size: 270 Color: 0

Bin 2176: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 323 Color: 1
Size: 285 Color: 0

Bin 2177: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 339 Color: 1
Size: 267 Color: 0

Bin 2178: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 307 Color: 1
Size: 301 Color: 0

Bin 2179: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 332 Color: 1
Size: 276 Color: 0

Bin 2180: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 334 Color: 1
Size: 273 Color: 0

Bin 2181: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 316 Color: 1
Size: 291 Color: 0

Bin 2182: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 1
Size: 359 Color: 1
Size: 259 Color: 0

Bin 2183: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 337 Color: 1
Size: 298 Color: 0

Bin 2184: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 1
Size: 342 Color: 1
Size: 266 Color: 0

Bin 2185: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 331 Color: 1
Size: 276 Color: 0

Bin 2186: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 335 Color: 1
Size: 272 Color: 0

Bin 2187: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 341 Color: 1
Size: 266 Color: 0

Bin 2188: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 358 Color: 1
Size: 269 Color: 0

Bin 2189: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 326 Color: 1
Size: 281 Color: 0

Bin 2190: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 344 Color: 1
Size: 263 Color: 0

Bin 2191: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 321 Color: 1
Size: 286 Color: 0

Bin 2192: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 320 Color: 1
Size: 287 Color: 0

Bin 2193: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 338 Color: 1
Size: 269 Color: 0

Bin 2194: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 320 Color: 1
Size: 287 Color: 0

Bin 2195: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 343 Color: 1
Size: 263 Color: 0

Bin 2196: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 349 Color: 1
Size: 257 Color: 0

Bin 2197: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 341 Color: 1
Size: 265 Color: 0

Bin 2198: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 329 Color: 1
Size: 277 Color: 0

Bin 2199: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 316 Color: 1
Size: 290 Color: 0

Bin 2200: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 1
Size: 342 Color: 1
Size: 265 Color: 0

Bin 2201: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 1
Size: 357 Color: 1
Size: 272 Color: 0

Bin 2202: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 305 Color: 0
Size: 301 Color: 1

Bin 2203: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 305 Color: 0
Size: 301 Color: 1

Bin 2204: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 338 Color: 1
Size: 268 Color: 0

Bin 2205: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 304 Color: 1
Size: 302 Color: 0

Bin 2206: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 327 Color: 0
Size: 278 Color: 1

Bin 2207: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 325 Color: 1
Size: 280 Color: 0

Bin 2208: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 331 Color: 1
Size: 274 Color: 0

Bin 2209: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 329 Color: 1
Size: 276 Color: 0

Bin 2210: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 1
Size: 348 Color: 1
Size: 266 Color: 0

Bin 2211: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 330 Color: 1
Size: 275 Color: 0

Bin 2212: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 337 Color: 1
Size: 268 Color: 0

Bin 2213: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 326 Color: 1
Size: 279 Color: 0

Bin 2214: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 330 Color: 1
Size: 275 Color: 0

Bin 2215: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 341 Color: 1
Size: 264 Color: 0

Bin 2216: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 348 Color: 1
Size: 257 Color: 0

Bin 2217: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 342 Color: 1
Size: 263 Color: 0

Bin 2218: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 338 Color: 1
Size: 267 Color: 0

Bin 2219: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 354 Color: 1
Size: 266 Color: 0

Bin 2220: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 324 Color: 1
Size: 281 Color: 0

Bin 2221: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 315 Color: 1
Size: 269 Color: 0

Bin 2222: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 1
Size: 334 Color: 1
Size: 271 Color: 0

Bin 2223: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 327 Color: 1
Size: 277 Color: 0

Bin 2224: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 321 Color: 1
Size: 281 Color: 0

Bin 2225: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 1
Size: 336 Color: 1
Size: 280 Color: 0

Bin 2226: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 336 Color: 1
Size: 268 Color: 0

Bin 2227: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 335 Color: 1
Size: 269 Color: 0

Bin 2228: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 342 Color: 1
Size: 262 Color: 0

Bin 2229: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 328 Color: 1
Size: 276 Color: 0

Bin 2230: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 316 Color: 1
Size: 288 Color: 0

Bin 2231: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 339 Color: 1
Size: 265 Color: 0

Bin 2232: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 328 Color: 1
Size: 276 Color: 0

Bin 2233: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 335 Color: 1
Size: 269 Color: 0

Bin 2234: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 319 Color: 1
Size: 285 Color: 0

Bin 2235: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 338 Color: 1
Size: 265 Color: 0

Bin 2236: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 334 Color: 1
Size: 269 Color: 0

Bin 2237: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 339 Color: 1
Size: 264 Color: 0

Bin 2238: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 335 Color: 1
Size: 268 Color: 0

Bin 2239: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 348 Color: 1
Size: 300 Color: 0

Bin 2240: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 303 Color: 1
Size: 300 Color: 0

Bin 2241: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 339 Color: 1
Size: 264 Color: 0

Bin 2242: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 340 Color: 1
Size: 263 Color: 0

Bin 2243: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 309 Color: 1
Size: 293 Color: 0

Bin 2244: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 338 Color: 1
Size: 264 Color: 0

Bin 2245: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 322 Color: 1
Size: 289 Color: 0

Bin 2246: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 337 Color: 1
Size: 265 Color: 0

Bin 2247: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 311 Color: 0
Size: 291 Color: 1

Bin 2248: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 327 Color: 1
Size: 275 Color: 0

Bin 2249: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 328 Color: 1
Size: 274 Color: 0

Bin 2250: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 327 Color: 1
Size: 275 Color: 0

Bin 2251: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 322 Color: 1
Size: 280 Color: 0

Bin 2252: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 320 Color: 1
Size: 282 Color: 0

Bin 2253: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 318 Color: 1
Size: 284 Color: 0

Bin 2254: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 336 Color: 1
Size: 265 Color: 0

Bin 2255: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 335 Color: 1
Size: 266 Color: 0

Bin 2256: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 322 Color: 1
Size: 279 Color: 0

Bin 2257: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 308 Color: 1
Size: 293 Color: 0

Bin 2258: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 319 Color: 1
Size: 282 Color: 0

Bin 2259: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 328 Color: 1
Size: 274 Color: 0

Bin 2260: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 320 Color: 1
Size: 281 Color: 0

Bin 2261: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 335 Color: 1
Size: 266 Color: 0

Bin 2262: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 318 Color: 1
Size: 283 Color: 0

Bin 2263: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 326 Color: 0
Size: 275 Color: 1

Bin 2264: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 334 Color: 1
Size: 267 Color: 0

Bin 2265: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 314 Color: 1
Size: 287 Color: 0

Bin 2266: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 1
Size: 332 Color: 1
Size: 269 Color: 0

Bin 2267: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 323 Color: 1
Size: 277 Color: 0

Bin 2268: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 330 Color: 1
Size: 270 Color: 0

Bin 2269: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 330 Color: 1
Size: 270 Color: 0

Bin 2270: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 303 Color: 1
Size: 297 Color: 0

Bin 2271: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 339 Color: 1
Size: 261 Color: 0

Bin 2272: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 1
Size: 332 Color: 1
Size: 272 Color: 0

Bin 2273: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 311 Color: 1
Size: 289 Color: 0

Bin 2274: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 337 Color: 1
Size: 263 Color: 0

Bin 2275: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 315 Color: 0
Size: 282 Color: 1

Bin 2276: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 1
Size: 308 Color: 1
Size: 292 Color: 0

Bin 2277: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 327 Color: 1
Size: 272 Color: 0

Bin 2278: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 325 Color: 1
Size: 274 Color: 0

Bin 2279: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 325 Color: 1
Size: 274 Color: 0

Bin 2280: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 322 Color: 1
Size: 277 Color: 0

Bin 2281: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 308 Color: 1
Size: 291 Color: 0

Bin 2282: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 336 Color: 1
Size: 263 Color: 0

Bin 2283: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 322 Color: 1
Size: 277 Color: 0

Bin 2284: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 304 Color: 0
Size: 295 Color: 1

Bin 2285: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 338 Color: 1
Size: 261 Color: 0

Bin 2286: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 325 Color: 1
Size: 274 Color: 0

Bin 2287: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 319 Color: 1
Size: 280 Color: 0

Bin 2288: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 1
Size: 354 Color: 1
Size: 292 Color: 0

Bin 2289: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 314 Color: 0
Size: 285 Color: 1

Bin 2290: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 332 Color: 1
Size: 267 Color: 0

Bin 2291: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 341 Color: 1
Size: 257 Color: 0

Bin 2292: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 333 Color: 1
Size: 265 Color: 0

Bin 2293: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 321 Color: 1
Size: 277 Color: 0

Bin 2294: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 320 Color: 1
Size: 278 Color: 0

Bin 2295: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 337 Color: 1
Size: 261 Color: 0

Bin 2296: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 307 Color: 0
Size: 290 Color: 1

Bin 2297: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 319 Color: 1
Size: 279 Color: 0

Bin 2298: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 306 Color: 1
Size: 292 Color: 0

Bin 2299: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 312 Color: 1
Size: 286 Color: 0

Bin 2300: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 334 Color: 1
Size: 264 Color: 0

Bin 2301: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 329 Color: 1
Size: 269 Color: 0

Bin 2302: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 322 Color: 1
Size: 276 Color: 0

Bin 2303: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 303 Color: 1
Size: 295 Color: 0

Bin 2304: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 327 Color: 1
Size: 271 Color: 0

Bin 2305: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 317 Color: 1
Size: 281 Color: 0

Bin 2306: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 333 Color: 1
Size: 265 Color: 0

Bin 2307: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 325 Color: 1
Size: 273 Color: 0

Bin 2308: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 333 Color: 1
Size: 264 Color: 0

Bin 2309: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 324 Color: 1
Size: 273 Color: 0

Bin 2310: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 334 Color: 1
Size: 263 Color: 0

Bin 2311: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 339 Color: 1
Size: 258 Color: 0

Bin 2312: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 328 Color: 1
Size: 269 Color: 0

Bin 2313: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 1
Size: 314 Color: 1
Size: 284 Color: 0

Bin 2314: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 332 Color: 1
Size: 265 Color: 0

Bin 2315: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 341 Color: 1
Size: 256 Color: 0

Bin 2316: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 315 Color: 1
Size: 282 Color: 0

Bin 2317: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 331 Color: 1
Size: 266 Color: 0

Bin 2318: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 341 Color: 1
Size: 256 Color: 0

Bin 2319: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 322 Color: 1
Size: 275 Color: 0

Bin 2320: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 319 Color: 1
Size: 278 Color: 0

Bin 2321: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 341 Color: 1
Size: 256 Color: 0

Bin 2322: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 336 Color: 1
Size: 261 Color: 0

Bin 2323: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 1
Size: 334 Color: 1
Size: 263 Color: 0

Bin 2324: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 340 Color: 1
Size: 256 Color: 0

Bin 2325: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 301 Color: 1
Size: 292 Color: 0

Bin 2326: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 325 Color: 1
Size: 271 Color: 0

Bin 2327: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 305 Color: 1
Size: 291 Color: 0

Bin 2328: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 317 Color: 1
Size: 279 Color: 0

Bin 2329: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 339 Color: 1
Size: 257 Color: 0

Bin 2330: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 330 Color: 1
Size: 266 Color: 0

Bin 2331: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 333 Color: 1
Size: 263 Color: 0

Bin 2332: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 299 Color: 0
Size: 297 Color: 1

Bin 2333: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 303 Color: 0
Size: 293 Color: 1

Bin 2334: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 304 Color: 0
Size: 292 Color: 1

Bin 2335: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 302 Color: 0
Size: 293 Color: 1

Bin 2336: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 334 Color: 1
Size: 261 Color: 0

Bin 2337: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 1
Size: 350 Color: 1
Size: 272 Color: 0

Bin 2338: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 325 Color: 1
Size: 270 Color: 0

Bin 2339: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 333 Color: 1
Size: 262 Color: 0

Bin 2340: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 300 Color: 1
Size: 295 Color: 0

Bin 2341: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 333 Color: 1
Size: 262 Color: 0

Bin 2342: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 340 Color: 1
Size: 255 Color: 0

Bin 2343: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 316 Color: 1
Size: 279 Color: 0

Bin 2344: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 298 Color: 0
Size: 297 Color: 1

Bin 2345: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 314 Color: 1
Size: 281 Color: 0

Bin 2346: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 331 Color: 1
Size: 264 Color: 0

Bin 2347: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 334 Color: 1
Size: 261 Color: 0

Bin 2348: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 314 Color: 1
Size: 281 Color: 0

Bin 2349: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 338 Color: 1
Size: 256 Color: 0

Bin 2350: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 325 Color: 1
Size: 270 Color: 0

Bin 2351: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 323 Color: 1
Size: 271 Color: 0

Bin 2352: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 317 Color: 1
Size: 277 Color: 0

Bin 2353: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 313 Color: 1
Size: 281 Color: 0

Bin 2354: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 322 Color: 0
Size: 272 Color: 1

Bin 2355: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 307 Color: 1
Size: 287 Color: 0

Bin 2356: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 323 Color: 1
Size: 271 Color: 0

Bin 2357: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 360 Color: 1
Size: 280 Color: 0

Bin 2358: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 330 Color: 1
Size: 264 Color: 0

Bin 2359: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 329 Color: 1
Size: 265 Color: 0

Bin 2360: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 326 Color: 1
Size: 268 Color: 0

Bin 2361: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 338 Color: 1
Size: 256 Color: 0

Bin 2362: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 328 Color: 1
Size: 266 Color: 0

Bin 2363: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 1
Size: 328 Color: 1
Size: 278 Color: 0

Bin 2364: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 299 Color: 1
Size: 295 Color: 0

Bin 2365: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 316 Color: 1
Size: 278 Color: 0

Bin 2366: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 319 Color: 1
Size: 268 Color: 0

Bin 2367: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 339 Color: 1
Size: 255 Color: 0

Bin 2368: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 1
Size: 336 Color: 1
Size: 259 Color: 0

Bin 2369: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 337 Color: 1
Size: 256 Color: 0

Bin 2370: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 334 Color: 1
Size: 259 Color: 0

Bin 2371: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 304 Color: 1
Size: 289 Color: 0

Bin 2372: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 332 Color: 1
Size: 261 Color: 0

Bin 2373: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 323 Color: 1
Size: 270 Color: 0

Bin 2374: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 337 Color: 1
Size: 256 Color: 0

Bin 2375: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 302 Color: 0
Size: 291 Color: 1

Bin 2376: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 337 Color: 1
Size: 256 Color: 0

Bin 2377: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 320 Color: 1
Size: 273 Color: 0

Bin 2378: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 307 Color: 1
Size: 286 Color: 0

Bin 2379: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 311 Color: 1
Size: 282 Color: 0

Bin 2380: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 1
Size: 329 Color: 1
Size: 264 Color: 0

Bin 2381: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 317 Color: 1
Size: 277 Color: 0

Bin 2382: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 317 Color: 0
Size: 275 Color: 1

Bin 2383: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 334 Color: 1
Size: 258 Color: 0

Bin 2384: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 328 Color: 1
Size: 264 Color: 0

Bin 2385: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 315 Color: 1
Size: 277 Color: 0

Bin 2386: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 335 Color: 1
Size: 257 Color: 0

Bin 2387: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 316 Color: 1
Size: 276 Color: 0

Bin 2388: 0 of cap free
Amount of items: 3
Items: 
Size: 364 Color: 1
Size: 355 Color: 1
Size: 282 Color: 0

Bin 2389: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 312 Color: 1
Size: 261 Color: 0

Bin 2390: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 309 Color: 1
Size: 283 Color: 0

Bin 2391: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 332 Color: 1
Size: 260 Color: 0

Bin 2392: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 319 Color: 1
Size: 273 Color: 0

Bin 2393: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 319 Color: 1
Size: 273 Color: 0

Bin 2394: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 304 Color: 1
Size: 288 Color: 0

Bin 2395: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 316 Color: 1
Size: 276 Color: 0

Bin 2396: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 318 Color: 1
Size: 274 Color: 0

Bin 2397: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 337 Color: 1
Size: 255 Color: 0

Bin 2398: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 324 Color: 1
Size: 268 Color: 0

Bin 2399: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 325 Color: 1
Size: 266 Color: 0

Bin 2400: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 325 Color: 1
Size: 266 Color: 0

Bin 2401: 0 of cap free
Amount of items: 3
Items: 
Size: 366 Color: 1
Size: 339 Color: 1
Size: 296 Color: 0

Bin 2402: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 1
Size: 336 Color: 1
Size: 284 Color: 0

Bin 2403: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 331 Color: 1
Size: 260 Color: 0

Bin 2404: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 304 Color: 1
Size: 287 Color: 0

Bin 2405: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 335 Color: 1
Size: 256 Color: 0

Bin 2406: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 305 Color: 1
Size: 286 Color: 0

Bin 2407: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 334 Color: 1
Size: 257 Color: 0

Bin 2408: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 336 Color: 1
Size: 255 Color: 0

Bin 2409: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 330 Color: 1
Size: 261 Color: 0

Bin 2410: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 306 Color: 1
Size: 285 Color: 0

Bin 2411: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 334 Color: 1
Size: 257 Color: 0

Bin 2412: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 310 Color: 0
Size: 281 Color: 1

Bin 2413: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 1
Size: 320 Color: 1
Size: 271 Color: 0

Bin 2414: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 308 Color: 0
Size: 282 Color: 1

Bin 2415: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 320 Color: 1
Size: 270 Color: 0

Bin 2416: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 328 Color: 1
Size: 271 Color: 0

Bin 2417: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 329 Color: 1
Size: 261 Color: 0

Bin 2418: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 326 Color: 1
Size: 261 Color: 0

Bin 2419: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 328 Color: 1
Size: 262 Color: 0

Bin 2420: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 320 Color: 1
Size: 270 Color: 0

Bin 2421: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 306 Color: 1
Size: 284 Color: 0

Bin 2422: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 1
Size: 327 Color: 1
Size: 267 Color: 0

Bin 2423: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 312 Color: 1
Size: 278 Color: 0

Bin 2424: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 301 Color: 1
Size: 288 Color: 0

Bin 2425: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 312 Color: 1
Size: 277 Color: 0

Bin 2426: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 322 Color: 1
Size: 265 Color: 0

Bin 2427: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 322 Color: 1
Size: 267 Color: 0

Bin 2428: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 328 Color: 1
Size: 261 Color: 0

Bin 2429: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 334 Color: 1
Size: 255 Color: 0

Bin 2430: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 328 Color: 1
Size: 261 Color: 0

Bin 2431: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 308 Color: 1
Size: 281 Color: 0

Bin 2432: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 317 Color: 1
Size: 272 Color: 0

Bin 2433: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 300 Color: 1
Size: 289 Color: 0

Bin 2434: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 296 Color: 0
Size: 293 Color: 1

Bin 2435: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 329 Color: 1
Size: 260 Color: 0

Bin 2436: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 296 Color: 1
Size: 293 Color: 0

Bin 2437: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 301 Color: 1
Size: 286 Color: 0

Bin 2438: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 333 Color: 1
Size: 256 Color: 0

Bin 2439: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 300 Color: 1
Size: 289 Color: 0

Bin 2440: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 1
Size: 325 Color: 1
Size: 265 Color: 0

Bin 2441: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 299 Color: 1
Size: 290 Color: 0

Bin 2442: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 333 Color: 1
Size: 256 Color: 0

Bin 2443: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 301 Color: 1
Size: 287 Color: 0

Bin 2444: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 329 Color: 1
Size: 259 Color: 0

Bin 2445: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 332 Color: 1
Size: 256 Color: 0

Bin 2446: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 332 Color: 1
Size: 256 Color: 0

Bin 2447: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 315 Color: 1
Size: 273 Color: 0

Bin 2448: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 321 Color: 1
Size: 267 Color: 0

Bin 2449: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 321 Color: 1
Size: 267 Color: 0

Bin 2450: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 329 Color: 1
Size: 259 Color: 0

Bin 2451: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 319 Color: 1
Size: 269 Color: 0

Bin 2452: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 1
Size: 354 Color: 1
Size: 271 Color: 0

Bin 2453: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 309 Color: 1
Size: 279 Color: 0

Bin 2454: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 308 Color: 1
Size: 280 Color: 0

Bin 2455: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 307 Color: 1
Size: 261 Color: 0

Bin 2456: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 322 Color: 1
Size: 266 Color: 0

Bin 2457: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 323 Color: 1
Size: 265 Color: 0

Bin 2458: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 330 Color: 1
Size: 258 Color: 0

Bin 2459: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 298 Color: 0
Size: 287 Color: 1

Bin 2460: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 332 Color: 1
Size: 255 Color: 0

Bin 2461: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 331 Color: 1
Size: 256 Color: 0

Bin 2462: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 331 Color: 1
Size: 256 Color: 0

Bin 2463: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 1
Size: 337 Color: 1
Size: 290 Color: 0

Bin 2464: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 331 Color: 1
Size: 257 Color: 0

Bin 2465: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 327 Color: 1
Size: 260 Color: 0

Bin 2466: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 303 Color: 1
Size: 284 Color: 0

Bin 2467: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 322 Color: 1
Size: 265 Color: 0

Bin 2468: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 330 Color: 1
Size: 257 Color: 0

Bin 2469: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 317 Color: 1
Size: 270 Color: 0

Bin 2470: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 316 Color: 1
Size: 271 Color: 0

Bin 2471: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 308 Color: 1
Size: 279 Color: 0

Bin 2472: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 321 Color: 1
Size: 266 Color: 0

Bin 2473: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 321 Color: 1
Size: 266 Color: 0

Bin 2474: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 1
Size: 331 Color: 1
Size: 256 Color: 0

Bin 2475: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 1
Size: 314 Color: 1
Size: 278 Color: 0

Bin 2476: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 319 Color: 1
Size: 267 Color: 0

Bin 2477: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 310 Color: 1
Size: 276 Color: 0

Bin 2478: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 333 Color: 1
Size: 253 Color: 0

Bin 2479: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 319 Color: 1
Size: 267 Color: 0

Bin 2480: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 300 Color: 0
Size: 286 Color: 1

Bin 2481: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 311 Color: 0
Size: 275 Color: 1

Bin 2482: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 323 Color: 1
Size: 263 Color: 0

Bin 2483: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 327 Color: 1
Size: 259 Color: 0

Bin 2484: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 328 Color: 1
Size: 258 Color: 0

Bin 2485: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 299 Color: 0
Size: 287 Color: 1

Bin 2486: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 320 Color: 1
Size: 265 Color: 0

Bin 2487: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 296 Color: 1
Size: 289 Color: 0

Bin 2488: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 313 Color: 1
Size: 272 Color: 0

Bin 2489: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 325 Color: 1
Size: 260 Color: 0

Bin 2490: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 1
Size: 328 Color: 1
Size: 261 Color: 0

Bin 2491: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 329 Color: 1
Size: 256 Color: 0

Bin 2492: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 319 Color: 1
Size: 266 Color: 0

Bin 2493: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 296 Color: 1
Size: 289 Color: 0

Bin 2494: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 326 Color: 1
Size: 257 Color: 0

Bin 2495: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 332 Color: 1
Size: 253 Color: 0

Bin 2496: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 308 Color: 1
Size: 277 Color: 0

Bin 2497: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 314 Color: 1
Size: 271 Color: 0

Bin 2498: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 317 Color: 1
Size: 268 Color: 0

Bin 2499: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 318 Color: 1
Size: 267 Color: 0

Bin 2500: 0 of cap free
Amount of items: 3
Items: 
Size: 416 Color: 1
Size: 324 Color: 1
Size: 261 Color: 0

Bin 2501: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 318 Color: 1
Size: 266 Color: 0

Bin 2502: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 326 Color: 1
Size: 257 Color: 0

Bin 2503: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 331 Color: 1
Size: 253 Color: 0

Bin 2504: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 321 Color: 1
Size: 263 Color: 0

Bin 2505: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 310 Color: 1
Size: 274 Color: 0

Bin 2506: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 298 Color: 1
Size: 286 Color: 0

Bin 2507: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 322 Color: 1
Size: 262 Color: 0

Bin 2508: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 331 Color: 1
Size: 253 Color: 0

Bin 2509: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 313 Color: 1
Size: 271 Color: 0

Bin 2510: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 314 Color: 1
Size: 270 Color: 0

Bin 2511: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 325 Color: 1
Size: 258 Color: 0

Bin 2512: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 321 Color: 1
Size: 262 Color: 0

Bin 2513: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 317 Color: 1
Size: 266 Color: 0

Bin 2514: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 302 Color: 0
Size: 281 Color: 1

Bin 2515: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 322 Color: 1
Size: 261 Color: 0

Bin 2516: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 317 Color: 1
Size: 266 Color: 0

Bin 2517: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 297 Color: 0
Size: 286 Color: 1

Bin 2518: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 324 Color: 1
Size: 259 Color: 0

Bin 2519: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 314 Color: 1
Size: 269 Color: 0

Bin 2520: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 293 Color: 1
Size: 290 Color: 0

Bin 2521: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 327 Color: 1
Size: 256 Color: 0

Bin 2522: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 323 Color: 1
Size: 260 Color: 0

Bin 2523: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 327 Color: 1
Size: 255 Color: 0

Bin 2524: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 322 Color: 1
Size: 260 Color: 0

Bin 2525: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 324 Color: 1
Size: 258 Color: 0

Bin 2526: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 315 Color: 1
Size: 267 Color: 0

Bin 2527: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 314 Color: 1
Size: 268 Color: 0

Bin 2528: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 1
Size: 358 Color: 1
Size: 272 Color: 0

Bin 2529: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 329 Color: 1
Size: 253 Color: 0

Bin 2530: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 320 Color: 1
Size: 262 Color: 0

Bin 2531: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 315 Color: 1
Size: 267 Color: 0

Bin 2532: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 316 Color: 1
Size: 266 Color: 0

Bin 2533: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 294 Color: 1
Size: 288 Color: 0

Bin 2534: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 313 Color: 1
Size: 271 Color: 0

Bin 2535: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 326 Color: 1
Size: 256 Color: 0

Bin 2536: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 327 Color: 1
Size: 255 Color: 0

Bin 2537: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 314 Color: 1
Size: 267 Color: 0

Bin 2538: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 317 Color: 0
Size: 264 Color: 1

Bin 2539: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 326 Color: 1
Size: 255 Color: 0

Bin 2540: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 336 Color: 1
Size: 275 Color: 0

Bin 2541: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 318 Color: 1
Size: 263 Color: 0

Bin 2542: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 328 Color: 1
Size: 253 Color: 0

Bin 2543: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 313 Color: 1
Size: 268 Color: 0

Bin 2544: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 318 Color: 1
Size: 263 Color: 0

Bin 2545: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 299 Color: 1
Size: 282 Color: 0

Bin 2546: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 316 Color: 1
Size: 265 Color: 0

Bin 2547: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 316 Color: 1
Size: 265 Color: 0

Bin 2548: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 328 Color: 1
Size: 253 Color: 0

Bin 2549: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 1
Size: 295 Color: 1
Size: 286 Color: 0

Bin 2550: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 321 Color: 1
Size: 259 Color: 0

Bin 2551: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 323 Color: 1
Size: 257 Color: 0

Bin 2552: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 317 Color: 1
Size: 263 Color: 0

Bin 2553: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 324 Color: 1
Size: 256 Color: 0

Bin 2554: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 301 Color: 1
Size: 279 Color: 0

Bin 2555: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 315 Color: 1
Size: 265 Color: 0

Bin 2556: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 290 Color: 1
Size: 290 Color: 0

Bin 2557: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 307 Color: 1
Size: 273 Color: 0

Bin 2558: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 318 Color: 1
Size: 262 Color: 0

Bin 2559: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 324 Color: 0
Size: 256 Color: 1

Bin 2560: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 314 Color: 1
Size: 265 Color: 0

Bin 2561: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 311 Color: 1
Size: 268 Color: 0

Bin 2562: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 1
Size: 326 Color: 1
Size: 270 Color: 0

Bin 2563: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 320 Color: 1
Size: 259 Color: 0

Bin 2564: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 325 Color: 1
Size: 254 Color: 0

Bin 2565: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 298 Color: 1
Size: 280 Color: 0

Bin 2566: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 326 Color: 1
Size: 253 Color: 0

Bin 2567: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 308 Color: 1
Size: 271 Color: 0

Bin 2568: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 297 Color: 1
Size: 282 Color: 0

Bin 2569: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 308 Color: 1
Size: 271 Color: 0

Bin 2570: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 320 Color: 1
Size: 259 Color: 0

Bin 2571: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 319 Color: 1
Size: 260 Color: 0

Bin 2572: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 309 Color: 1
Size: 269 Color: 0

Bin 2573: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 313 Color: 1
Size: 265 Color: 0

Bin 2574: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 301 Color: 1
Size: 277 Color: 0

Bin 2575: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 311 Color: 1
Size: 267 Color: 0

Bin 2576: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 309 Color: 1
Size: 269 Color: 0

Bin 2577: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 325 Color: 1
Size: 254 Color: 0

Bin 2578: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 1
Size: 318 Color: 1
Size: 268 Color: 0

Bin 2579: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 323 Color: 1
Size: 255 Color: 0

Bin 2580: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 324 Color: 1
Size: 254 Color: 0

Bin 2581: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 316 Color: 1
Size: 262 Color: 0

Bin 2582: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 324 Color: 1
Size: 254 Color: 0

Bin 2583: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 1
Size: 333 Color: 1
Size: 279 Color: 0

Bin 2584: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 299 Color: 1
Size: 279 Color: 0

Bin 2585: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 295 Color: 0
Size: 283 Color: 1

Bin 2586: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 305 Color: 1
Size: 273 Color: 0

Bin 2587: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 311 Color: 1
Size: 267 Color: 0

Bin 2588: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 290 Color: 1
Size: 288 Color: 0

Bin 2589: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 316 Color: 1
Size: 262 Color: 0

Bin 2590: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 323 Color: 1
Size: 255 Color: 0

Bin 2591: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 311 Color: 1
Size: 266 Color: 0

Bin 2592: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 1
Size: 365 Color: 1
Size: 267 Color: 0

Bin 2593: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 305 Color: 1
Size: 272 Color: 0

Bin 2594: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 324 Color: 1
Size: 253 Color: 0

Bin 2595: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 316 Color: 1
Size: 261 Color: 0

Bin 2596: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 289 Color: 0
Size: 288 Color: 1

Bin 2597: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 320 Color: 1
Size: 257 Color: 0

Bin 2598: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 309 Color: 1
Size: 268 Color: 0

Bin 2599: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 320 Color: 1
Size: 257 Color: 0

Bin 2600: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 290 Color: 0
Size: 287 Color: 1

Bin 2601: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 310 Color: 1
Size: 267 Color: 0

Bin 2602: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 324 Color: 1
Size: 253 Color: 0

Bin 2603: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 324 Color: 1
Size: 253 Color: 0

Bin 2604: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 317 Color: 1
Size: 259 Color: 0

Bin 2605: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 290 Color: 0
Size: 286 Color: 1

Bin 2606: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 297 Color: 1
Size: 279 Color: 0

Bin 2607: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 354 Color: 1
Size: 256 Color: 0

Bin 2608: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 322 Color: 1
Size: 254 Color: 0

Bin 2609: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 322 Color: 1
Size: 254 Color: 0

Bin 2610: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 315 Color: 1
Size: 261 Color: 0

Bin 2611: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 323 Color: 1
Size: 253 Color: 0

Bin 2612: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 318 Color: 1
Size: 258 Color: 0

Bin 2613: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 310 Color: 1
Size: 266 Color: 0

Bin 2614: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 308 Color: 1
Size: 268 Color: 0

Bin 2615: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 303 Color: 1
Size: 273 Color: 0

Bin 2616: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 295 Color: 0
Size: 281 Color: 1

Bin 2617: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 316 Color: 1
Size: 255 Color: 0

Bin 2618: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 303 Color: 1
Size: 272 Color: 0

Bin 2619: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 322 Color: 1
Size: 253 Color: 0

Bin 2620: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 1
Size: 321 Color: 1
Size: 258 Color: 0

Bin 2621: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 311 Color: 1
Size: 264 Color: 0

Bin 2622: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 317 Color: 1
Size: 258 Color: 0

Bin 2623: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 320 Color: 1
Size: 255 Color: 0

Bin 2624: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 316 Color: 1
Size: 259 Color: 0

Bin 2625: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 312 Color: 1
Size: 263 Color: 0

Bin 2626: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 322 Color: 1
Size: 253 Color: 0

Bin 2627: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 318 Color: 1
Size: 257 Color: 0

Bin 2628: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 291 Color: 0
Size: 283 Color: 1

Bin 2629: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 312 Color: 1
Size: 262 Color: 0

Bin 2630: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 314 Color: 1
Size: 260 Color: 0

Bin 2631: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 302 Color: 1
Size: 272 Color: 0

Bin 2632: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 305 Color: 1
Size: 269 Color: 0

Bin 2633: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 299 Color: 1
Size: 275 Color: 0

Bin 2634: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 290 Color: 0
Size: 284 Color: 1

Bin 2635: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 301 Color: 1
Size: 273 Color: 0

Bin 2636: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 292 Color: 1
Size: 282 Color: 0

Bin 2637: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 317 Color: 1
Size: 257 Color: 0

Bin 2638: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 319 Color: 1
Size: 254 Color: 0

Bin 2639: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 298 Color: 1
Size: 275 Color: 0

Bin 2640: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 309 Color: 1
Size: 264 Color: 0

Bin 2641: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 312 Color: 1
Size: 261 Color: 0

Bin 2642: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 312 Color: 1
Size: 261 Color: 0

Bin 2643: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 319 Color: 1
Size: 254 Color: 0

Bin 2644: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 291 Color: 1
Size: 282 Color: 0

Bin 2645: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 315 Color: 1
Size: 258 Color: 0

Bin 2646: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 316 Color: 1
Size: 257 Color: 0

Bin 2647: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 313 Color: 1
Size: 259 Color: 0

Bin 2648: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 310 Color: 1
Size: 263 Color: 0

Bin 2649: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 297 Color: 1
Size: 276 Color: 0

Bin 2650: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 310 Color: 1
Size: 263 Color: 0

Bin 2651: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 316 Color: 1
Size: 257 Color: 0

Bin 2652: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 293 Color: 1
Size: 280 Color: 0

Bin 2653: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 315 Color: 1
Size: 257 Color: 0

Bin 2654: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 302 Color: 0
Size: 271 Color: 1

Bin 2655: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 316 Color: 1
Size: 256 Color: 0

Bin 2656: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 298 Color: 0
Size: 274 Color: 1

Bin 2657: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 314 Color: 1
Size: 258 Color: 0

Bin 2658: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 309 Color: 1
Size: 263 Color: 0

Bin 2659: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 310 Color: 1
Size: 262 Color: 0

Bin 2660: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 304 Color: 1
Size: 268 Color: 0

Bin 2661: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 318 Color: 1
Size: 254 Color: 0

Bin 2662: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 1
Size: 290 Color: 0
Size: 283 Color: 1

Bin 2663: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 317 Color: 1
Size: 255 Color: 0

Bin 2664: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 310 Color: 1
Size: 262 Color: 0

Bin 2665: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 334 Color: 1
Size: 289 Color: 0

Bin 2666: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 1
Size: 303 Color: 1
Size: 269 Color: 0

Bin 2667: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 315 Color: 1
Size: 256 Color: 0

Bin 2668: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 1
Size: 291 Color: 0
Size: 284 Color: 1

Bin 2669: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 297 Color: 1
Size: 274 Color: 0

Bin 2670: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 311 Color: 1
Size: 260 Color: 0

Bin 2671: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 298 Color: 1
Size: 273 Color: 0

Bin 2672: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 315 Color: 1
Size: 256 Color: 0

Bin 2673: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 288 Color: 1
Size: 283 Color: 0

Bin 2674: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 1
Size: 293 Color: 0
Size: 278 Color: 1

Bin 2675: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 317 Color: 1
Size: 253 Color: 0

Bin 2676: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 287 Color: 1
Size: 283 Color: 0

Bin 2677: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 297 Color: 1
Size: 273 Color: 0

Bin 2678: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 305 Color: 1
Size: 265 Color: 0

Bin 2679: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 295 Color: 1
Size: 275 Color: 0

Bin 2680: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 317 Color: 1
Size: 253 Color: 0

Bin 2681: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 297 Color: 1
Size: 273 Color: 0

Bin 2682: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 306 Color: 1
Size: 264 Color: 0

Bin 2683: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 311 Color: 1
Size: 259 Color: 0

Bin 2684: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 285 Color: 0
Size: 284 Color: 1

Bin 2685: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 304 Color: 1
Size: 266 Color: 0

Bin 2686: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 314 Color: 1
Size: 256 Color: 0

Bin 2687: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 301 Color: 1
Size: 269 Color: 0

Bin 2688: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 1
Size: 307 Color: 1
Size: 263 Color: 0

Bin 2689: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 312 Color: 1
Size: 257 Color: 0

Bin 2690: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 316 Color: 1
Size: 253 Color: 0

Bin 2691: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 310 Color: 1
Size: 259 Color: 0

Bin 2692: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 310 Color: 1
Size: 259 Color: 0

Bin 2693: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 302 Color: 1
Size: 267 Color: 0

Bin 2694: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 308 Color: 1
Size: 261 Color: 0

Bin 2695: 0 of cap free
Amount of items: 3
Items: 
Size: 432 Color: 1
Size: 307 Color: 1
Size: 262 Color: 0

Bin 2696: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 299 Color: 1
Size: 269 Color: 0

Bin 2697: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 299 Color: 1
Size: 269 Color: 0

Bin 2698: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 304 Color: 1
Size: 264 Color: 0

Bin 2699: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 1
Size: 361 Color: 1
Size: 279 Color: 0

Bin 2700: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 346 Color: 1
Size: 296 Color: 0

Bin 2701: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 313 Color: 1
Size: 255 Color: 0

Bin 2702: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 289 Color: 1
Size: 279 Color: 0

Bin 2703: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 308 Color: 1
Size: 260 Color: 0

Bin 2704: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 306 Color: 1
Size: 262 Color: 0

Bin 2705: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 312 Color: 1
Size: 256 Color: 0

Bin 2706: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 310 Color: 1
Size: 258 Color: 0

Bin 2707: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 306 Color: 1
Size: 262 Color: 0

Bin 2708: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 1
Size: 297 Color: 1
Size: 271 Color: 0

Bin 2709: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 312 Color: 1
Size: 255 Color: 0

Bin 2710: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 312 Color: 1
Size: 255 Color: 0

Bin 2711: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 311 Color: 1
Size: 256 Color: 0

Bin 2712: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 310 Color: 1
Size: 257 Color: 0

Bin 2713: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 294 Color: 1
Size: 273 Color: 0

Bin 2714: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 291 Color: 0
Size: 276 Color: 1

Bin 2715: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 299 Color: 1
Size: 268 Color: 0

Bin 2716: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 309 Color: 1
Size: 258 Color: 0

Bin 2717: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 309 Color: 1
Size: 258 Color: 0

Bin 2718: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 297 Color: 1
Size: 270 Color: 0

Bin 2719: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 303 Color: 1
Size: 264 Color: 0

Bin 2720: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 305 Color: 1
Size: 262 Color: 0

Bin 2721: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 308 Color: 1
Size: 259 Color: 0

Bin 2722: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 284 Color: 1
Size: 282 Color: 0

Bin 2723: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 304 Color: 1
Size: 262 Color: 0

Bin 2724: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 303 Color: 1
Size: 263 Color: 0

Bin 2725: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 311 Color: 1
Size: 255 Color: 0

Bin 2726: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 293 Color: 1
Size: 273 Color: 0

Bin 2727: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 308 Color: 1
Size: 258 Color: 0

Bin 2728: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 1
Size: 289 Color: 1
Size: 285 Color: 0

Bin 2729: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 303 Color: 1
Size: 263 Color: 0

Bin 2730: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 1
Size: 296 Color: 1
Size: 270 Color: 0

Bin 2731: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 310 Color: 1
Size: 255 Color: 0

Bin 2732: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 303 Color: 1
Size: 262 Color: 0

Bin 2733: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 289 Color: 0
Size: 276 Color: 1

Bin 2734: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 299 Color: 1
Size: 266 Color: 0

Bin 2735: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 305 Color: 1
Size: 260 Color: 0

Bin 2736: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 286 Color: 0
Size: 279 Color: 1

Bin 2737: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 311 Color: 1
Size: 254 Color: 0

Bin 2738: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 307 Color: 1
Size: 257 Color: 0

Bin 2739: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 293 Color: 1
Size: 272 Color: 0

Bin 2740: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 293 Color: 1
Size: 272 Color: 0

Bin 2741: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 292 Color: 1
Size: 273 Color: 0

Bin 2742: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 1
Size: 310 Color: 1
Size: 255 Color: 0

Bin 2743: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 284 Color: 0
Size: 280 Color: 1

Bin 2744: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 306 Color: 1
Size: 258 Color: 0

Bin 2745: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 337 Color: 1
Size: 262 Color: 0

Bin 2746: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 282 Color: 1
Size: 282 Color: 0

Bin 2747: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 310 Color: 1
Size: 254 Color: 0

Bin 2748: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 284 Color: 0
Size: 280 Color: 1

Bin 2749: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 307 Color: 1
Size: 257 Color: 0

Bin 2750: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 302 Color: 1
Size: 262 Color: 0

Bin 2751: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 1
Size: 287 Color: 1
Size: 277 Color: 0

Bin 2752: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 302 Color: 1
Size: 261 Color: 0

Bin 2753: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 288 Color: 1
Size: 274 Color: 0

Bin 2754: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 304 Color: 1
Size: 259 Color: 0

Bin 2755: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 301 Color: 1
Size: 261 Color: 0

Bin 2756: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 309 Color: 1
Size: 254 Color: 0

Bin 2757: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 301 Color: 1
Size: 262 Color: 0

Bin 2758: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 307 Color: 1
Size: 256 Color: 0

Bin 2759: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 290 Color: 1
Size: 273 Color: 0

Bin 2760: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 306 Color: 1
Size: 256 Color: 0

Bin 2761: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 296 Color: 1
Size: 266 Color: 0

Bin 2762: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 311 Color: 1
Size: 252 Color: 0

Bin 2763: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 304 Color: 1
Size: 258 Color: 0

Bin 2764: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 302 Color: 1
Size: 260 Color: 0

Bin 2765: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 286 Color: 0
Size: 276 Color: 1

Bin 2766: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 303 Color: 1
Size: 259 Color: 0

Bin 2767: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 306 Color: 1
Size: 256 Color: 0

Bin 2768: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 1
Size: 339 Color: 1
Size: 264 Color: 0

Bin 2769: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 1
Size: 311 Color: 1
Size: 252 Color: 0

Bin 2770: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 296 Color: 1
Size: 266 Color: 0

Bin 2771: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 295 Color: 1
Size: 267 Color: 0

Bin 2772: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 291 Color: 0
Size: 270 Color: 1

Bin 2773: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 299 Color: 1
Size: 262 Color: 0

Bin 2774: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 296 Color: 1
Size: 265 Color: 0

Bin 2775: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 290 Color: 1
Size: 271 Color: 0

Bin 2776: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 286 Color: 1
Size: 275 Color: 0

Bin 2777: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 298 Color: 1
Size: 263 Color: 0

Bin 2778: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 300 Color: 1
Size: 261 Color: 0

Bin 2779: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 304 Color: 1
Size: 256 Color: 0

Bin 2780: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 293 Color: 1
Size: 267 Color: 0

Bin 2781: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 1
Size: 328 Color: 1
Size: 282 Color: 0

Bin 2782: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 1
Size: 301 Color: 1
Size: 260 Color: 0

Bin 2783: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 290 Color: 0
Size: 270 Color: 1

Bin 2784: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 300 Color: 1
Size: 260 Color: 0

Bin 2785: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 297 Color: 1
Size: 263 Color: 0

Bin 2786: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 307 Color: 1
Size: 253 Color: 0

Bin 2787: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 307 Color: 1
Size: 252 Color: 0

Bin 2788: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 296 Color: 1
Size: 263 Color: 0

Bin 2789: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 299 Color: 1
Size: 260 Color: 0

Bin 2790: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 303 Color: 1
Size: 256 Color: 0

Bin 2791: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 305 Color: 1
Size: 254 Color: 0

Bin 2792: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 306 Color: 1
Size: 253 Color: 0

Bin 2793: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 283 Color: 1
Size: 275 Color: 0

Bin 2794: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 287 Color: 1
Size: 271 Color: 0

Bin 2795: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 284 Color: 0
Size: 274 Color: 1

Bin 2796: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 305 Color: 1
Size: 253 Color: 0

Bin 2797: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 303 Color: 1
Size: 255 Color: 0

Bin 2798: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 299 Color: 1
Size: 259 Color: 0

Bin 2799: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 291 Color: 1
Size: 267 Color: 0

Bin 2800: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 305 Color: 1
Size: 253 Color: 0

Bin 2801: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 307 Color: 1
Size: 251 Color: 0

Bin 2802: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 285 Color: 1
Size: 273 Color: 0

Bin 2803: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 303 Color: 1
Size: 255 Color: 0

Bin 2804: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 300 Color: 1
Size: 259 Color: 0

Bin 2805: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 300 Color: 1
Size: 258 Color: 0

Bin 2806: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 302 Color: 1
Size: 256 Color: 0

Bin 2807: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 307 Color: 1
Size: 251 Color: 0

Bin 2808: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 294 Color: 1
Size: 263 Color: 0

Bin 2809: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 280 Color: 0
Size: 277 Color: 1

Bin 2810: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 292 Color: 1
Size: 265 Color: 0

Bin 2811: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 295 Color: 1
Size: 262 Color: 0

Bin 2812: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 278 Color: 1
Size: 275 Color: 0

Bin 2813: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 1
Size: 289 Color: 1
Size: 271 Color: 0

Bin 2814: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 288 Color: 0
Size: 269 Color: 1

Bin 2815: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 282 Color: 1
Size: 275 Color: 0

Bin 2816: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 297 Color: 1
Size: 260 Color: 0

Bin 2817: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 300 Color: 1
Size: 257 Color: 0

Bin 2818: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 297 Color: 1
Size: 259 Color: 0

Bin 2819: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 298 Color: 1
Size: 258 Color: 0

Bin 2820: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 286 Color: 1
Size: 270 Color: 0

Bin 2821: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 281 Color: 0
Size: 275 Color: 1

Bin 2822: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 291 Color: 1
Size: 265 Color: 0

Bin 2823: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 294 Color: 1
Size: 263 Color: 0

Bin 2824: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 297 Color: 1
Size: 259 Color: 0

Bin 2825: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 288 Color: 1
Size: 268 Color: 0

Bin 2826: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 301 Color: 1
Size: 254 Color: 0

Bin 2827: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 296 Color: 1
Size: 259 Color: 0

Bin 2828: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 299 Color: 1
Size: 256 Color: 0

Bin 2829: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 296 Color: 1
Size: 259 Color: 0

Bin 2830: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 279 Color: 0
Size: 276 Color: 1

Bin 2831: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 295 Color: 1
Size: 260 Color: 0

Bin 2832: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 296 Color: 1
Size: 259 Color: 0

Bin 2833: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 301 Color: 1
Size: 253 Color: 0

Bin 2834: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 292 Color: 0
Size: 262 Color: 1

Bin 2835: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 297 Color: 1
Size: 257 Color: 0

Bin 2836: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 295 Color: 1
Size: 259 Color: 0

Bin 2837: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 280 Color: 1
Size: 274 Color: 0

Bin 2838: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 1
Size: 296 Color: 1
Size: 258 Color: 0

Bin 2839: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 298 Color: 1
Size: 255 Color: 0

Bin 2840: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 1
Size: 291 Color: 1
Size: 266 Color: 0

Bin 2841: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 278 Color: 0
Size: 275 Color: 1

Bin 2842: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 295 Color: 1
Size: 258 Color: 0

Bin 2843: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 284 Color: 1
Size: 269 Color: 0

Bin 2844: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 288 Color: 1
Size: 265 Color: 0

Bin 2845: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 291 Color: 1
Size: 262 Color: 0

Bin 2846: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 280 Color: 1
Size: 273 Color: 0

Bin 2847: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 277 Color: 1
Size: 276 Color: 0

Bin 2848: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 299 Color: 1
Size: 254 Color: 0

Bin 2849: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 296 Color: 1
Size: 257 Color: 0

Bin 2850: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 292 Color: 0
Size: 261 Color: 1

Bin 2851: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 287 Color: 1
Size: 266 Color: 0

Bin 2852: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 285 Color: 1
Size: 268 Color: 0

Bin 2853: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 278 Color: 0
Size: 275 Color: 1

Bin 2854: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 296 Color: 1
Size: 257 Color: 0

Bin 2855: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 297 Color: 1
Size: 256 Color: 0

Bin 2856: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 294 Color: 1
Size: 259 Color: 0

Bin 2857: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 1
Size: 286 Color: 1
Size: 267 Color: 0

Bin 2858: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 301 Color: 1
Size: 251 Color: 0

Bin 2859: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 282 Color: 0
Size: 270 Color: 1

Bin 2860: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 291 Color: 1
Size: 261 Color: 0

Bin 2861: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 295 Color: 1
Size: 257 Color: 0

Bin 2862: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 289 Color: 1
Size: 263 Color: 0

Bin 2863: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 299 Color: 1
Size: 253 Color: 0

Bin 2864: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 282 Color: 1
Size: 270 Color: 0

Bin 2865: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 297 Color: 1
Size: 255 Color: 0

Bin 2866: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 295 Color: 1
Size: 257 Color: 0

Bin 2867: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 299 Color: 1
Size: 253 Color: 0

Bin 2868: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 1
Size: 333 Color: 1
Size: 300 Color: 0

Bin 2869: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 299 Color: 1
Size: 253 Color: 0

Bin 2870: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 295 Color: 1
Size: 257 Color: 0

Bin 2871: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 297 Color: 1
Size: 255 Color: 0

Bin 2872: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 298 Color: 1
Size: 254 Color: 0

Bin 2873: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 283 Color: 1
Size: 268 Color: 0

Bin 2874: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 299 Color: 1
Size: 252 Color: 0

Bin 2875: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 284 Color: 1
Size: 267 Color: 0

Bin 2876: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 280 Color: 0
Size: 271 Color: 1

Bin 2877: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 293 Color: 1
Size: 258 Color: 0

Bin 2878: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 281 Color: 1
Size: 270 Color: 0

Bin 2879: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 297 Color: 1
Size: 253 Color: 0

Bin 2880: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 282 Color: 1
Size: 269 Color: 0

Bin 2881: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 299 Color: 1
Size: 251 Color: 0

Bin 2882: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 295 Color: 1
Size: 256 Color: 0

Bin 2883: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 287 Color: 1
Size: 264 Color: 0

Bin 2884: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 1
Size: 299 Color: 1
Size: 252 Color: 0

Bin 2885: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 290 Color: 1
Size: 260 Color: 0

Bin 2886: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 298 Color: 1
Size: 252 Color: 0

Bin 2887: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 299 Color: 1
Size: 251 Color: 0

Bin 2888: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 298 Color: 1
Size: 252 Color: 0

Bin 2889: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 288 Color: 1
Size: 262 Color: 0

Bin 2890: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 295 Color: 1
Size: 255 Color: 0

Bin 2891: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 295 Color: 1
Size: 255 Color: 0

Bin 2892: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 297 Color: 1
Size: 253 Color: 0

Bin 2893: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 290 Color: 1
Size: 260 Color: 0

Bin 2894: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 296 Color: 1
Size: 254 Color: 0

Bin 2895: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 294 Color: 1
Size: 256 Color: 0

Bin 2896: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 278 Color: 1
Size: 262 Color: 0

Bin 2897: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 297 Color: 1
Size: 253 Color: 0

Bin 2898: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 287 Color: 1
Size: 263 Color: 0

Bin 2899: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 296 Color: 1
Size: 253 Color: 0

Bin 2900: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 275 Color: 1
Size: 274 Color: 0

Bin 2901: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 293 Color: 1
Size: 256 Color: 0

Bin 2902: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 294 Color: 1
Size: 255 Color: 0

Bin 2903: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 296 Color: 1
Size: 253 Color: 0

Bin 2904: 0 of cap free
Amount of items: 3
Items: 
Size: 354 Color: 1
Size: 330 Color: 1
Size: 317 Color: 0

Bin 2905: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 296 Color: 1
Size: 253 Color: 0

Bin 2906: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 286 Color: 1
Size: 263 Color: 0

Bin 2907: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 293 Color: 1
Size: 256 Color: 0

Bin 2908: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 298 Color: 1
Size: 251 Color: 0

Bin 2909: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 1
Size: 299 Color: 1
Size: 251 Color: 0

Bin 2910: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 291 Color: 1
Size: 258 Color: 0

Bin 2911: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 293 Color: 1
Size: 256 Color: 0

Bin 2912: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 337 Color: 1
Size: 291 Color: 0

Bin 2913: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 1
Size: 289 Color: 1
Size: 260 Color: 0

Bin 2914: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 282 Color: 1
Size: 266 Color: 0

Bin 2915: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 280 Color: 1
Size: 268 Color: 0

Bin 2916: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 287 Color: 1
Size: 261 Color: 0

Bin 2917: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 278 Color: 0
Size: 270 Color: 1

Bin 2918: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 341 Color: 1
Size: 268 Color: 0

Bin 2919: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 291 Color: 1
Size: 257 Color: 0

Bin 2920: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 292 Color: 1
Size: 256 Color: 0

Bin 2921: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 279 Color: 0
Size: 269 Color: 1

Bin 2922: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 290 Color: 1
Size: 258 Color: 0

Bin 2923: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 288 Color: 1
Size: 259 Color: 0

Bin 2924: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 296 Color: 1
Size: 251 Color: 0

Bin 2925: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 289 Color: 1
Size: 258 Color: 0

Bin 2926: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 291 Color: 1
Size: 256 Color: 0

Bin 2927: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 280 Color: 1
Size: 267 Color: 0

Bin 2928: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 280 Color: 1
Size: 267 Color: 0

Bin 2929: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 287 Color: 1
Size: 260 Color: 0

Bin 2930: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 290 Color: 1
Size: 257 Color: 0

Bin 2931: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 289 Color: 1
Size: 258 Color: 0

Bin 2932: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 295 Color: 1
Size: 252 Color: 0

Bin 2933: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 278 Color: 0
Size: 269 Color: 1

Bin 2934: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 294 Color: 1
Size: 253 Color: 0

Bin 2935: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 1
Size: 278 Color: 1
Size: 269 Color: 0

Bin 2936: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 285 Color: 1
Size: 261 Color: 0

Bin 2937: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 284 Color: 1
Size: 262 Color: 0

Bin 2938: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 292 Color: 1
Size: 254 Color: 0

Bin 2939: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 277 Color: 0
Size: 269 Color: 1

Bin 2940: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 287 Color: 1
Size: 259 Color: 0

Bin 2941: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 293 Color: 1
Size: 253 Color: 0

Bin 2942: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 288 Color: 1
Size: 258 Color: 0

Bin 2943: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 1
Size: 359 Color: 1
Size: 283 Color: 0

Bin 2944: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 288 Color: 1
Size: 255 Color: 0

Bin 2945: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 288 Color: 1
Size: 257 Color: 0

Bin 2946: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 291 Color: 1
Size: 254 Color: 0

Bin 2947: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 274 Color: 0
Size: 271 Color: 1

Bin 2948: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 292 Color: 1
Size: 253 Color: 0

Bin 2949: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 277 Color: 1
Size: 268 Color: 0

Bin 2950: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 289 Color: 1
Size: 256 Color: 0

Bin 2951: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 289 Color: 1
Size: 256 Color: 0

Bin 2952: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 292 Color: 1
Size: 253 Color: 0

Bin 2953: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 282 Color: 1
Size: 263 Color: 0

Bin 2954: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 290 Color: 1
Size: 255 Color: 0

Bin 2955: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 280 Color: 1
Size: 265 Color: 0

Bin 2956: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 277 Color: 1
Size: 268 Color: 0

Bin 2957: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 293 Color: 1
Size: 252 Color: 0

Bin 2958: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 284 Color: 1
Size: 261 Color: 0

Bin 2959: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 283 Color: 1
Size: 262 Color: 0

Bin 2960: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 1
Size: 279 Color: 1
Size: 266 Color: 0

Bin 2961: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 292 Color: 1
Size: 252 Color: 0

Bin 2962: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 280 Color: 1
Size: 264 Color: 0

Bin 2963: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 287 Color: 1
Size: 255 Color: 0

Bin 2964: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 1
Size: 251 Color: 0

Bin 2965: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 1
Size: 251 Color: 0

Bin 2966: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 290 Color: 1
Size: 254 Color: 0

Bin 2967: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 287 Color: 1
Size: 257 Color: 0

Bin 2968: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 282 Color: 1
Size: 262 Color: 0

Bin 2969: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 281 Color: 1
Size: 263 Color: 0

Bin 2970: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 281 Color: 0
Size: 263 Color: 1

Bin 2971: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 284 Color: 1
Size: 260 Color: 0

Bin 2972: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 272 Color: 1
Size: 272 Color: 0

Bin 2973: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 275 Color: 1
Size: 269 Color: 0

Bin 2974: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 293 Color: 1
Size: 251 Color: 0

Bin 2975: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 1
Size: 295 Color: 1
Size: 281 Color: 0

Bin 2976: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 292 Color: 1
Size: 251 Color: 0

Bin 2977: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 292 Color: 1
Size: 251 Color: 0

Bin 2978: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 287 Color: 1
Size: 256 Color: 0

Bin 2979: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 284 Color: 1
Size: 259 Color: 0

Bin 2980: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 281 Color: 1
Size: 262 Color: 0

Bin 2981: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 281 Color: 1
Size: 262 Color: 0

Bin 2982: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 321 Color: 1
Size: 259 Color: 0

Bin 2983: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 289 Color: 1
Size: 254 Color: 0

Bin 2984: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 288 Color: 1
Size: 255 Color: 0

Bin 2985: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 1
Size: 331 Color: 1
Size: 317 Color: 0

Bin 2986: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 291 Color: 1
Size: 251 Color: 0

Bin 2987: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 280 Color: 1
Size: 263 Color: 0

Bin 2988: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 291 Color: 1
Size: 252 Color: 0

Bin 2989: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 272 Color: 1
Size: 271 Color: 0

Bin 2990: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 277 Color: 0
Size: 266 Color: 1

Bin 2991: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 290 Color: 1
Size: 253 Color: 0

Bin 2992: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 274 Color: 0
Size: 269 Color: 1

Bin 2993: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 278 Color: 1
Size: 264 Color: 0

Bin 2994: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 288 Color: 1
Size: 254 Color: 0

Bin 2995: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 283 Color: 1
Size: 259 Color: 0

Bin 2996: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 278 Color: 1
Size: 264 Color: 0

Bin 2997: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 289 Color: 1
Size: 255 Color: 0

Bin 2998: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 274 Color: 1
Size: 268 Color: 0

Bin 2999: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 1
Size: 322 Color: 1
Size: 261 Color: 0

Bin 3000: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 289 Color: 1
Size: 253 Color: 0

Bin 3001: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 282 Color: 1
Size: 260 Color: 0

Bin 3002: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 275 Color: 1
Size: 269 Color: 0

Bin 3003: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 287 Color: 1
Size: 255 Color: 0

Bin 3004: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 278 Color: 1
Size: 263 Color: 0

Bin 3005: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 1
Size: 326 Color: 1
Size: 273 Color: 0

Bin 3006: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 290 Color: 1
Size: 251 Color: 0

Bin 3007: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 283 Color: 1
Size: 256 Color: 0

Bin 3008: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 288 Color: 1
Size: 253 Color: 0

Bin 3009: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 270 Color: 1
Size: 265 Color: 0

Bin 3010: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 289 Color: 1
Size: 252 Color: 0

Bin 3011: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 1
Size: 284 Color: 1
Size: 258 Color: 0

Bin 3012: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 272 Color: 1
Size: 269 Color: 0

Bin 3013: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 276 Color: 0
Size: 265 Color: 1

Bin 3014: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 280 Color: 1
Size: 261 Color: 0

Bin 3015: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 279 Color: 1
Size: 261 Color: 0

Bin 3016: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 288 Color: 1
Size: 252 Color: 0

Bin 3017: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 274 Color: 1
Size: 266 Color: 0

Bin 3018: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 284 Color: 1
Size: 256 Color: 0

Bin 3019: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 1
Size: 280 Color: 1
Size: 260 Color: 0

Bin 3020: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 283 Color: 1
Size: 256 Color: 0

Bin 3021: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 274 Color: 1
Size: 267 Color: 0

Bin 3022: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 271 Color: 0
Size: 268 Color: 1

Bin 3023: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 288 Color: 1
Size: 251 Color: 0

Bin 3024: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 274 Color: 1
Size: 265 Color: 0

Bin 3025: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 277 Color: 1
Size: 262 Color: 0

Bin 3026: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 1
Size: 286 Color: 1
Size: 253 Color: 0

Bin 3027: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 1
Size: 325 Color: 1
Size: 257 Color: 0

Bin 3028: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 285 Color: 1
Size: 253 Color: 0

Bin 3029: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 285 Color: 1
Size: 253 Color: 0

Bin 3030: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 285 Color: 1
Size: 253 Color: 0

Bin 3031: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 286 Color: 1
Size: 252 Color: 0

Bin 3032: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 284 Color: 1
Size: 254 Color: 0

Bin 3033: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 287 Color: 1
Size: 251 Color: 0

Bin 3034: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 279 Color: 1
Size: 259 Color: 0

Bin 3035: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 283 Color: 1
Size: 255 Color: 0

Bin 3036: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 283 Color: 1
Size: 255 Color: 0

Bin 3037: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 287 Color: 1
Size: 251 Color: 0

Bin 3038: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 276 Color: 1
Size: 262 Color: 0

Bin 3039: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 1
Size: 284 Color: 1
Size: 254 Color: 0

Bin 3040: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 274 Color: 1
Size: 263 Color: 0

Bin 3041: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 278 Color: 1
Size: 259 Color: 0

Bin 3042: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 275 Color: 1
Size: 262 Color: 0

Bin 3043: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 275 Color: 1
Size: 262 Color: 0

Bin 3044: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 275 Color: 1
Size: 262 Color: 0

Bin 3045: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 1
Size: 272 Color: 1
Size: 269 Color: 0

Bin 3046: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 1
Size: 272 Color: 1
Size: 265 Color: 0

Bin 3047: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 286 Color: 1
Size: 250 Color: 0

Bin 3048: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 270 Color: 1
Size: 266 Color: 0

Bin 3049: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 272 Color: 1
Size: 264 Color: 0

Bin 3050: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 269 Color: 0
Size: 267 Color: 1

Bin 3051: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 269 Color: 1
Size: 267 Color: 0

Bin 3052: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 274 Color: 1
Size: 262 Color: 0

Bin 3053: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 284 Color: 1
Size: 252 Color: 0

Bin 3054: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 268 Color: 1
Size: 268 Color: 0

Bin 3055: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 286 Color: 1
Size: 250 Color: 0

Bin 3056: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 286 Color: 1
Size: 250 Color: 0

Bin 3057: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 285 Color: 1
Size: 251 Color: 0

Bin 3058: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 1
Size: 286 Color: 1
Size: 250 Color: 0

Bin 3059: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 275 Color: 1
Size: 260 Color: 0

Bin 3060: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 282 Color: 1
Size: 253 Color: 0

Bin 3061: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 271 Color: 1
Size: 264 Color: 0

Bin 3062: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 285 Color: 1
Size: 250 Color: 0

Bin 3063: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 284 Color: 1
Size: 251 Color: 0

Bin 3064: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 276 Color: 1
Size: 259 Color: 0

Bin 3065: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 268 Color: 1
Size: 266 Color: 0

Bin 3066: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 276 Color: 1
Size: 258 Color: 0

Bin 3067: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 282 Color: 1
Size: 252 Color: 0

Bin 3068: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 284 Color: 1
Size: 250 Color: 0

Bin 3069: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 280 Color: 1
Size: 254 Color: 0

Bin 3070: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 274 Color: 1
Size: 260 Color: 0

Bin 3071: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 284 Color: 1
Size: 250 Color: 0

Bin 3072: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 267 Color: 1
Size: 267 Color: 0

Bin 3073: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 278 Color: 1
Size: 256 Color: 0

Bin 3074: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 268 Color: 0
Size: 265 Color: 1

Bin 3075: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 282 Color: 1
Size: 251 Color: 0

Bin 3076: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 279 Color: 1
Size: 254 Color: 0

Bin 3077: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 278 Color: 1
Size: 255 Color: 0

Bin 3078: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 280 Color: 1
Size: 253 Color: 0

Bin 3079: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 273 Color: 1
Size: 260 Color: 0

Bin 3080: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 279 Color: 1
Size: 254 Color: 0

Bin 3081: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 269 Color: 1
Size: 264 Color: 0

Bin 3082: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 281 Color: 1
Size: 252 Color: 0

Bin 3083: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 279 Color: 1
Size: 253 Color: 0

Bin 3084: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 282 Color: 1
Size: 250 Color: 0

Bin 3085: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 282 Color: 1
Size: 251 Color: 0

Bin 3086: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 279 Color: 1
Size: 253 Color: 0

Bin 3087: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 282 Color: 1
Size: 250 Color: 0

Bin 3088: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 278 Color: 1
Size: 254 Color: 0

Bin 3089: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 274 Color: 1
Size: 258 Color: 0

Bin 3090: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 282 Color: 1
Size: 250 Color: 0

Bin 3091: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 1
Size: 276 Color: 1
Size: 257 Color: 0

Bin 3092: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 279 Color: 1
Size: 253 Color: 0

Bin 3093: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 273 Color: 1
Size: 259 Color: 0

Bin 3094: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 280 Color: 1
Size: 252 Color: 0

Bin 3095: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 280 Color: 1
Size: 252 Color: 0

Bin 3096: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 269 Color: 1
Size: 263 Color: 0

Bin 3097: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 272 Color: 1
Size: 260 Color: 0

Bin 3098: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 274 Color: 1
Size: 258 Color: 0

Bin 3099: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 1
Size: 270 Color: 0
Size: 262 Color: 1

Bin 3100: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 277 Color: 1
Size: 254 Color: 0

Bin 3101: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 279 Color: 1
Size: 252 Color: 0

Bin 3102: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 278 Color: 1
Size: 253 Color: 0

Bin 3103: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 1
Size: 272 Color: 1
Size: 259 Color: 0

Bin 3104: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 3105: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 265 Color: 1
Size: 265 Color: 0

Bin 3106: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 268 Color: 1
Size: 262 Color: 0

Bin 3107: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 276 Color: 1
Size: 254 Color: 0

Bin 3108: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 269 Color: 1
Size: 261 Color: 0

Bin 3109: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 271 Color: 0
Size: 259 Color: 1

Bin 3110: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 272 Color: 1
Size: 258 Color: 0

Bin 3111: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 1
Size: 320 Color: 1
Size: 257 Color: 0

Bin 3112: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 278 Color: 1
Size: 252 Color: 0

Bin 3113: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 277 Color: 1
Size: 253 Color: 0

Bin 3114: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 279 Color: 1
Size: 251 Color: 0

Bin 3115: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 274 Color: 1
Size: 256 Color: 0

Bin 3116: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 1
Size: 271 Color: 1
Size: 259 Color: 0

Bin 3117: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 273 Color: 1
Size: 256 Color: 0

Bin 3118: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 270 Color: 1
Size: 259 Color: 0

Bin 3119: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 266 Color: 0
Size: 263 Color: 1

Bin 3120: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 270 Color: 1
Size: 259 Color: 0

Bin 3121: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 275 Color: 1
Size: 254 Color: 0

Bin 3122: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 3123: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 279 Color: 1
Size: 250 Color: 0

Bin 3124: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 277 Color: 1
Size: 252 Color: 0

Bin 3125: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 278 Color: 1
Size: 250 Color: 0

Bin 3126: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 274 Color: 1
Size: 254 Color: 0

Bin 3127: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 1
Size: 277 Color: 1
Size: 251 Color: 0

Bin 3128: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 273 Color: 1
Size: 254 Color: 0

Bin 3129: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 273 Color: 1
Size: 254 Color: 0

Bin 3130: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 266 Color: 1
Size: 261 Color: 0

Bin 3131: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 267 Color: 1
Size: 260 Color: 0

Bin 3132: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 265 Color: 1
Size: 261 Color: 0

Bin 3133: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 275 Color: 1
Size: 252 Color: 0

Bin 3134: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 274 Color: 1
Size: 252 Color: 0

Bin 3135: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 274 Color: 1
Size: 252 Color: 0

Bin 3136: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 276 Color: 1
Size: 250 Color: 0

Bin 3137: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 276 Color: 1
Size: 250 Color: 0

Bin 3138: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 272 Color: 1
Size: 254 Color: 0

Bin 3139: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 273 Color: 1
Size: 253 Color: 0

Bin 3140: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 275 Color: 1
Size: 251 Color: 0

Bin 3141: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 1
Size: 275 Color: 1
Size: 252 Color: 0

Bin 3142: 0 of cap free
Amount of items: 3
Items: 
Size: 475 Color: 1
Size: 271 Color: 1
Size: 255 Color: 0

Bin 3143: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 1
Size: 251 Color: 0

Bin 3144: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 1
Size: 345 Color: 1
Size: 278 Color: 0

Bin 3145: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 1
Size: 251 Color: 0

Bin 3146: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 269 Color: 1
Size: 256 Color: 0

Bin 3147: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 275 Color: 1
Size: 250 Color: 0

Bin 3148: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 1
Size: 251 Color: 0

Bin 3149: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 274 Color: 1
Size: 251 Color: 0

Bin 3150: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 273 Color: 1
Size: 252 Color: 0

Bin 3151: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 271 Color: 1
Size: 254 Color: 0

Bin 3152: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 263 Color: 0
Size: 262 Color: 1

Bin 3153: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 267 Color: 1
Size: 256 Color: 0

Bin 3154: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 264 Color: 0
Size: 260 Color: 1

Bin 3155: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 272 Color: 1
Size: 252 Color: 0

Bin 3156: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 266 Color: 1
Size: 257 Color: 0

Bin 3157: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 273 Color: 1
Size: 250 Color: 0

Bin 3158: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 271 Color: 1
Size: 252 Color: 0

Bin 3159: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 269 Color: 1
Size: 253 Color: 0

Bin 3160: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 271 Color: 1
Size: 252 Color: 0

Bin 3161: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 264 Color: 1
Size: 258 Color: 0

Bin 3162: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 262 Color: 1
Size: 260 Color: 0

Bin 3163: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 262 Color: 0
Size: 260 Color: 1

Bin 3164: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 261 Color: 1
Size: 261 Color: 0

Bin 3165: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 272 Color: 1
Size: 250 Color: 0

Bin 3166: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 1
Size: 266 Color: 1
Size: 256 Color: 0

Bin 3167: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 267 Color: 1
Size: 254 Color: 0

Bin 3168: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 271 Color: 1
Size: 250 Color: 0

Bin 3169: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 271 Color: 1
Size: 250 Color: 0

Bin 3170: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 261 Color: 1
Size: 260 Color: 0

Bin 3171: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 262 Color: 1
Size: 259 Color: 0

Bin 3172: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 268 Color: 1
Size: 253 Color: 0

Bin 3173: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 264 Color: 1
Size: 257 Color: 0

Bin 3174: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 263 Color: 1
Size: 258 Color: 0

Bin 3175: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 1
Size: 270 Color: 1
Size: 251 Color: 0

Bin 3176: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 263 Color: 1
Size: 257 Color: 0

Bin 3177: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 267 Color: 1
Size: 253 Color: 0

Bin 3178: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 269 Color: 1
Size: 250 Color: 0

Bin 3179: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 263 Color: 1
Size: 256 Color: 0

Bin 3180: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 265 Color: 1
Size: 254 Color: 0

Bin 3181: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 264 Color: 1
Size: 255 Color: 0

Bin 3182: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 267 Color: 1
Size: 252 Color: 0

Bin 3183: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 261 Color: 0
Size: 258 Color: 1

Bin 3184: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 263 Color: 1
Size: 256 Color: 0

Bin 3185: 0 of cap free
Amount of items: 3
Items: 
Size: 478 Color: 1
Size: 272 Color: 1
Size: 251 Color: 0

Bin 3186: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 265 Color: 1
Size: 254 Color: 0

Bin 3187: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 269 Color: 1
Size: 250 Color: 0

Bin 3188: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 269 Color: 1
Size: 250 Color: 0

Bin 3189: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 267 Color: 1
Size: 252 Color: 0

Bin 3190: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 261 Color: 0
Size: 258 Color: 1

Bin 3191: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 1
Size: 264 Color: 1
Size: 255 Color: 0

Bin 3192: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 268 Color: 1
Size: 250 Color: 0

Bin 3193: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 268 Color: 1
Size: 250 Color: 0

Bin 3194: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 267 Color: 1
Size: 251 Color: 0

Bin 3195: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 264 Color: 1
Size: 254 Color: 0

Bin 3196: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 267 Color: 1
Size: 251 Color: 0

Bin 3197: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 265 Color: 1
Size: 253 Color: 0

Bin 3198: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 265 Color: 1
Size: 253 Color: 0

Bin 3199: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 266 Color: 1
Size: 252 Color: 0

Bin 3200: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 265 Color: 1
Size: 253 Color: 0

Bin 3201: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 266 Color: 1
Size: 252 Color: 0

Bin 3202: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 1
Size: 262 Color: 1
Size: 256 Color: 0

Bin 3203: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 1
Size: 258 Color: 0

Bin 3204: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 1
Size: 251 Color: 0

Bin 3205: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 1
Size: 251 Color: 0

Bin 3206: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 265 Color: 1
Size: 252 Color: 0

Bin 3207: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 0
Size: 258 Color: 1

Bin 3208: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 259 Color: 1
Size: 258 Color: 0

Bin 3209: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 1
Size: 255 Color: 0

Bin 3210: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 1
Size: 253 Color: 0

Bin 3211: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 259 Color: 0
Size: 257 Color: 1

Bin 3212: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 259 Color: 1
Size: 257 Color: 0

Bin 3213: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 265 Color: 1
Size: 251 Color: 0

Bin 3214: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 1
Size: 266 Color: 1
Size: 251 Color: 0

Bin 3215: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 263 Color: 1
Size: 253 Color: 0

Bin 3216: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 265 Color: 1
Size: 251 Color: 0

Bin 3217: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 1
Size: 255 Color: 0

Bin 3218: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 1
Size: 261 Color: 1
Size: 255 Color: 0

Bin 3219: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 263 Color: 1
Size: 252 Color: 0

Bin 3220: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 259 Color: 1
Size: 256 Color: 0

Bin 3221: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 263 Color: 1
Size: 252 Color: 0

Bin 3222: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 264 Color: 1
Size: 251 Color: 0

Bin 3223: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 258 Color: 0
Size: 257 Color: 1

Bin 3224: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 261 Color: 1
Size: 254 Color: 0

Bin 3225: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 265 Color: 1
Size: 250 Color: 0

Bin 3226: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 260 Color: 1
Size: 254 Color: 0

Bin 3227: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 264 Color: 1
Size: 250 Color: 0

Bin 3228: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 258 Color: 0
Size: 256 Color: 1

Bin 3229: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 262 Color: 1
Size: 252 Color: 0

Bin 3230: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 262 Color: 1
Size: 252 Color: 0

Bin 3231: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 257 Color: 1
Size: 257 Color: 0

Bin 3232: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 261 Color: 1
Size: 252 Color: 0

Bin 3233: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 261 Color: 1
Size: 252 Color: 0

Bin 3234: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 262 Color: 1
Size: 251 Color: 0

Bin 3235: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 263 Color: 1
Size: 250 Color: 0

Bin 3236: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 263 Color: 1
Size: 250 Color: 0

Bin 3237: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 263 Color: 1
Size: 250 Color: 0

Bin 3238: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 1
Size: 259 Color: 1
Size: 254 Color: 0

Bin 3239: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 256 Color: 1
Size: 256 Color: 0

Bin 3240: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 261 Color: 1
Size: 251 Color: 0

Bin 3241: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 1
Size: 253 Color: 0

Bin 3242: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 1
Size: 253 Color: 0

Bin 3243: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 260 Color: 1
Size: 252 Color: 0

Bin 3244: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 260 Color: 1
Size: 252 Color: 0

Bin 3245: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 262 Color: 1
Size: 250 Color: 0

Bin 3246: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 259 Color: 1
Size: 253 Color: 0

Bin 3247: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 257 Color: 1
Size: 255 Color: 0

Bin 3248: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 257 Color: 1
Size: 254 Color: 0

Bin 3249: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 261 Color: 1
Size: 250 Color: 0

Bin 3250: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 261 Color: 1
Size: 250 Color: 0

Bin 3251: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 1
Size: 256 Color: 0
Size: 255 Color: 1

Bin 3252: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 260 Color: 1
Size: 250 Color: 0

Bin 3253: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 255 Color: 1
Size: 255 Color: 0

Bin 3254: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 258 Color: 1
Size: 252 Color: 0

Bin 3255: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 256 Color: 0
Size: 254 Color: 1

Bin 3256: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 259 Color: 1
Size: 251 Color: 0

Bin 3257: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 1
Size: 260 Color: 1
Size: 250 Color: 0

Bin 3258: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 259 Color: 1
Size: 250 Color: 0

Bin 3259: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 1
Size: 258 Color: 1
Size: 251 Color: 0

Bin 3260: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 3261: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 258 Color: 1
Size: 250 Color: 0

Bin 3262: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 3263: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 3264: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 257 Color: 1
Size: 251 Color: 0

Bin 3265: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 0
Size: 253 Color: 1

Bin 3266: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 254 Color: 1
Size: 254 Color: 0

Bin 3267: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 1
Size: 255 Color: 1
Size: 253 Color: 0

Bin 3268: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 3269: 0 of cap free
Amount of items: 3
Items: 
Size: 489 Color: 1
Size: 260 Color: 1
Size: 252 Color: 0

Bin 3270: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 3271: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 3272: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 256 Color: 1
Size: 251 Color: 0

Bin 3273: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 3274: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 3275: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 3276: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 254 Color: 1
Size: 253 Color: 0

Bin 3277: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 257 Color: 1
Size: 250 Color: 0

Bin 3278: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 0
Size: 251 Color: 1

Bin 3279: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 1
Size: 255 Color: 1
Size: 252 Color: 0

Bin 3280: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 1
Size: 251 Color: 0

Bin 3281: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 1
Size: 252 Color: 0

Bin 3282: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 253 Color: 1
Size: 253 Color: 0

Bin 3283: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 254 Color: 1
Size: 252 Color: 0

Bin 3284: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 253 Color: 1
Size: 252 Color: 0

Bin 3285: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 3286: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 3287: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 253 Color: 0
Size: 252 Color: 1

Bin 3288: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 3289: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 3290: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 3291: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 3292: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 255 Color: 1
Size: 250 Color: 0

Bin 3293: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 1
Size: 254 Color: 1
Size: 251 Color: 0

Bin 3294: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 252 Color: 1
Size: 252 Color: 0

Bin 3295: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 3296: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 253 Color: 1
Size: 251 Color: 0

Bin 3297: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 1
Size: 254 Color: 1
Size: 250 Color: 0

Bin 3298: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 3299: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 3300: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 3301: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 3302: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 252 Color: 1
Size: 251 Color: 0

Bin 3303: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 1
Size: 279 Color: 1
Size: 256 Color: 0

Bin 3304: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 3305: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3306: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3307: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3308: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3309: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3310: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3311: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 1
Size: 252 Color: 1
Size: 250 Color: 0

Bin 3312: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 1
Size: 253 Color: 1
Size: 250 Color: 0

Bin 3313: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 3314: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 1
Size: 327 Color: 1
Size: 275 Color: 0

Bin 3315: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 3316: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 3317: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 282 Color: 0
Size: 264 Color: 1

Bin 3318: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 3319: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 3320: 0 of cap free
Amount of items: 3
Items: 
Size: 500 Color: 1
Size: 251 Color: 1
Size: 250 Color: 0

Bin 3321: 0 of cap free
Amount of items: 3
Items: 
Size: 501 Color: 1
Size: 250 Color: 1
Size: 250 Color: 0

Bin 3322: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 1
Size: 308 Color: 1
Size: 254 Color: 0

Bin 3323: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 269 Color: 1
Size: 251 Color: 0

Bin 3324: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 1
Size: 272 Color: 1
Size: 257 Color: 0

Bin 3325: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 1
Size: 273 Color: 1
Size: 252 Color: 0

Bin 3326: 1 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 1
Size: 315 Color: 1
Size: 297 Color: 0

Bin 3327: 1 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 1
Size: 347 Color: 1
Size: 286 Color: 0

Bin 3328: 1 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 351 Color: 1
Size: 279 Color: 0

Bin 3329: 2 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 1
Size: 306 Color: 1
Size: 259 Color: 0

Bin 3330: 3 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 1
Size: 311 Color: 1
Size: 264 Color: 0

Bin 3331: 12 of cap free
Amount of items: 3
Items: 
Size: 337 Color: 1
Size: 327 Color: 1
Size: 325 Color: 0

Bin 3332: 80 of cap free
Amount of items: 3
Items: 
Size: 317 Color: 1
Size: 305 Color: 1
Size: 299 Color: 0

Bin 3333: 152 of cap free
Amount of items: 3
Items: 
Size: 300 Color: 1
Size: 286 Color: 1
Size: 263 Color: 0

Bin 3334: 249 of cap free
Amount of items: 2
Items: 
Size: 501 Color: 1
Size: 251 Color: 0

Bin 3335: 500 of cap free
Amount of items: 1
Items: 
Size: 501 Color: 1

Total size: 3337334
Total free space: 1001

