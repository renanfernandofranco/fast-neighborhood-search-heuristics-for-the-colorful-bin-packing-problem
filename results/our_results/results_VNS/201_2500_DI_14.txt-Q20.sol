Capicity Bin: 2000
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 4
Items: 
Size: 1005 Color: 12
Size: 767 Color: 15
Size: 186 Color: 11
Size: 42 Color: 17

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1409 Color: 17
Size: 493 Color: 5
Size: 98 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1479 Color: 2
Size: 487 Color: 16
Size: 34 Color: 16

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1485 Color: 12
Size: 431 Color: 10
Size: 84 Color: 7

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1642 Color: 18
Size: 302 Color: 1
Size: 56 Color: 8

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1647 Color: 14
Size: 289 Color: 11
Size: 64 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1706 Color: 12
Size: 262 Color: 4
Size: 32 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1707 Color: 0
Size: 213 Color: 7
Size: 80 Color: 7

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1725 Color: 18
Size: 231 Color: 4
Size: 44 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1726 Color: 6
Size: 236 Color: 10
Size: 38 Color: 12

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 18
Size: 218 Color: 2
Size: 40 Color: 15

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1745 Color: 1
Size: 211 Color: 15
Size: 44 Color: 12

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 1
Size: 174 Color: 16
Size: 68 Color: 14

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 16
Size: 197 Color: 5
Size: 20 Color: 19

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1786 Color: 11
Size: 154 Color: 12
Size: 60 Color: 7

Bin 16: 1 of cap free
Amount of items: 3
Items: 
Size: 1309 Color: 2
Size: 642 Color: 9
Size: 48 Color: 19

Bin 17: 1 of cap free
Amount of items: 2
Items: 
Size: 1655 Color: 19
Size: 344 Color: 9

Bin 18: 1 of cap free
Amount of items: 3
Items: 
Size: 1683 Color: 17
Size: 164 Color: 1
Size: 152 Color: 16

Bin 19: 1 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 3
Size: 230 Color: 12
Size: 56 Color: 17

Bin 20: 1 of cap free
Amount of items: 2
Items: 
Size: 1717 Color: 6
Size: 282 Color: 17

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 3
Size: 202 Color: 12
Size: 32 Color: 8

Bin 22: 2 of cap free
Amount of items: 3
Items: 
Size: 1384 Color: 1
Size: 498 Color: 9
Size: 116 Color: 12

Bin 23: 2 of cap free
Amount of items: 2
Items: 
Size: 1417 Color: 0
Size: 581 Color: 13

Bin 24: 2 of cap free
Amount of items: 3
Items: 
Size: 1606 Color: 2
Size: 210 Color: 3
Size: 182 Color: 12

Bin 25: 2 of cap free
Amount of items: 4
Items: 
Size: 1741 Color: 18
Size: 241 Color: 19
Size: 8 Color: 4
Size: 8 Color: 1

Bin 26: 2 of cap free
Amount of items: 2
Items: 
Size: 1778 Color: 10
Size: 220 Color: 9

Bin 27: 3 of cap free
Amount of items: 4
Items: 
Size: 1230 Color: 11
Size: 571 Color: 14
Size: 128 Color: 19
Size: 68 Color: 8

Bin 28: 3 of cap free
Amount of items: 3
Items: 
Size: 1541 Color: 9
Size: 398 Color: 12
Size: 58 Color: 11

Bin 29: 3 of cap free
Amount of items: 2
Items: 
Size: 1686 Color: 3
Size: 311 Color: 8

Bin 30: 3 of cap free
Amount of items: 2
Items: 
Size: 1777 Color: 15
Size: 220 Color: 14

Bin 31: 3 of cap free
Amount of items: 3
Items: 
Size: 1794 Color: 5
Size: 187 Color: 18
Size: 16 Color: 3

Bin 32: 4 of cap free
Amount of items: 3
Items: 
Size: 1567 Color: 4
Size: 263 Color: 11
Size: 166 Color: 13

Bin 33: 4 of cap free
Amount of items: 2
Items: 
Size: 1666 Color: 17
Size: 330 Color: 19

Bin 34: 4 of cap free
Amount of items: 2
Items: 
Size: 1750 Color: 1
Size: 246 Color: 6

Bin 35: 5 of cap free
Amount of items: 3
Items: 
Size: 1073 Color: 13
Size: 808 Color: 7
Size: 114 Color: 5

Bin 36: 5 of cap free
Amount of items: 2
Items: 
Size: 1326 Color: 13
Size: 669 Color: 12

Bin 37: 5 of cap free
Amount of items: 3
Items: 
Size: 1399 Color: 2
Size: 442 Color: 11
Size: 154 Color: 13

Bin 38: 6 of cap free
Amount of items: 6
Items: 
Size: 1001 Color: 8
Size: 295 Color: 19
Size: 291 Color: 19
Size: 237 Color: 12
Size: 112 Color: 0
Size: 58 Color: 11

Bin 39: 6 of cap free
Amount of items: 2
Items: 
Size: 1256 Color: 18
Size: 738 Color: 16

Bin 40: 6 of cap free
Amount of items: 2
Items: 
Size: 1589 Color: 10
Size: 405 Color: 18

Bin 41: 6 of cap free
Amount of items: 2
Items: 
Size: 1611 Color: 16
Size: 383 Color: 19

Bin 42: 6 of cap free
Amount of items: 2
Items: 
Size: 1651 Color: 1
Size: 343 Color: 9

Bin 43: 6 of cap free
Amount of items: 2
Items: 
Size: 1749 Color: 13
Size: 245 Color: 5

Bin 44: 7 of cap free
Amount of items: 2
Items: 
Size: 1317 Color: 15
Size: 676 Color: 12

Bin 45: 7 of cap free
Amount of items: 3
Items: 
Size: 1592 Color: 13
Size: 361 Color: 19
Size: 40 Color: 18

Bin 46: 7 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 3
Size: 308 Color: 8

Bin 47: 9 of cap free
Amount of items: 3
Items: 
Size: 1313 Color: 18
Size: 497 Color: 11
Size: 181 Color: 12

Bin 48: 9 of cap free
Amount of items: 2
Items: 
Size: 1629 Color: 15
Size: 362 Color: 17

Bin 49: 10 of cap free
Amount of items: 3
Items: 
Size: 1081 Color: 18
Size: 833 Color: 19
Size: 76 Color: 11

Bin 50: 11 of cap free
Amount of items: 4
Items: 
Size: 1002 Color: 13
Size: 562 Color: 1
Size: 325 Color: 13
Size: 100 Color: 2

Bin 51: 11 of cap free
Amount of items: 2
Items: 
Size: 1724 Color: 13
Size: 265 Color: 2

Bin 52: 14 of cap free
Amount of items: 2
Items: 
Size: 1205 Color: 12
Size: 781 Color: 10

Bin 53: 14 of cap free
Amount of items: 2
Items: 
Size: 1526 Color: 14
Size: 460 Color: 18

Bin 54: 14 of cap free
Amount of items: 2
Items: 
Size: 1570 Color: 1
Size: 416 Color: 4

Bin 55: 15 of cap free
Amount of items: 21
Items: 
Size: 225 Color: 6
Size: 154 Color: 11
Size: 132 Color: 7
Size: 128 Color: 15
Size: 114 Color: 6
Size: 112 Color: 12
Size: 98 Color: 17
Size: 98 Color: 14
Size: 96 Color: 19
Size: 96 Color: 9
Size: 88 Color: 13
Size: 88 Color: 11
Size: 76 Color: 15
Size: 72 Color: 18
Size: 72 Color: 10
Size: 64 Color: 19
Size: 64 Color: 18
Size: 64 Color: 4
Size: 52 Color: 14
Size: 52 Color: 0
Size: 40 Color: 0

Bin 56: 17 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 10
Size: 501 Color: 18
Size: 76 Color: 12

Bin 57: 18 of cap free
Amount of items: 2
Items: 
Size: 1405 Color: 8
Size: 577 Color: 4

Bin 58: 24 of cap free
Amount of items: 2
Items: 
Size: 1069 Color: 16
Size: 907 Color: 5

Bin 59: 24 of cap free
Amount of items: 2
Items: 
Size: 1199 Color: 7
Size: 777 Color: 5

Bin 60: 26 of cap free
Amount of items: 2
Items: 
Size: 1401 Color: 17
Size: 573 Color: 15

Bin 61: 28 of cap free
Amount of items: 2
Items: 
Size: 1515 Color: 14
Size: 457 Color: 18

Bin 62: 29 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 1
Size: 501 Color: 7

Bin 63: 30 of cap free
Amount of items: 3
Items: 
Size: 1065 Color: 18
Size: 773 Color: 9
Size: 132 Color: 6

Bin 64: 32 of cap free
Amount of items: 2
Items: 
Size: 1134 Color: 19
Size: 834 Color: 16

Bin 65: 34 of cap free
Amount of items: 2
Items: 
Size: 1303 Color: 1
Size: 663 Color: 3

Bin 66: 1516 of cap free
Amount of items: 11
Items: 
Size: 52 Color: 11
Size: 52 Color: 9
Size: 52 Color: 4
Size: 48 Color: 18
Size: 46 Color: 14
Size: 46 Color: 12
Size: 40 Color: 10
Size: 40 Color: 9
Size: 36 Color: 18
Size: 36 Color: 16
Size: 36 Color: 0

Total size: 130000
Total free space: 2000

