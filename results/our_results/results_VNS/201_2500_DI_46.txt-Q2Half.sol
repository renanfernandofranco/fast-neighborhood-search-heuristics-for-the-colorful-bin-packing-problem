Capicity Bin: 2328
Lower Bound: 65

Bins used: 66
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 1325 Color: 1
Size: 947 Color: 1
Size: 56 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 2014 Color: 1
Size: 262 Color: 1
Size: 52 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1942 Color: 1
Size: 346 Color: 1
Size: 40 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1555 Color: 1
Size: 645 Color: 1
Size: 128 Color: 0

Bin 5: 0 of cap free
Amount of items: 4
Items: 
Size: 1787 Color: 1
Size: 455 Color: 1
Size: 66 Color: 0
Size: 20 Color: 0

Bin 6: 0 of cap free
Amount of items: 4
Items: 
Size: 1778 Color: 1
Size: 394 Color: 1
Size: 128 Color: 0
Size: 28 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1925 Color: 1
Size: 275 Color: 1
Size: 128 Color: 0

Bin 8: 0 of cap free
Amount of items: 4
Items: 
Size: 1218 Color: 1
Size: 802 Color: 1
Size: 164 Color: 0
Size: 144 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1370 Color: 1
Size: 864 Color: 1
Size: 94 Color: 0

Bin 10: 0 of cap free
Amount of items: 5
Items: 
Size: 1573 Color: 1
Size: 381 Color: 1
Size: 322 Color: 1
Size: 48 Color: 0
Size: 4 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 1
Size: 565 Color: 1
Size: 90 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 1
Size: 348 Color: 1
Size: 166 Color: 0

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1546 Color: 1
Size: 654 Color: 1
Size: 128 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1861 Color: 1
Size: 291 Color: 1
Size: 176 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1322 Color: 1
Size: 850 Color: 1
Size: 156 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1461 Color: 1
Size: 723 Color: 1
Size: 144 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1167 Color: 1
Size: 969 Color: 1
Size: 192 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1665 Color: 1
Size: 553 Color: 1
Size: 110 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 2078 Color: 1
Size: 210 Color: 1
Size: 40 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1970 Color: 1
Size: 302 Color: 1
Size: 56 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 1694 Color: 1
Size: 538 Color: 1
Size: 96 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 1871 Color: 1
Size: 333 Color: 1
Size: 124 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 1941 Color: 1
Size: 323 Color: 1
Size: 64 Color: 0

Bin 24: 0 of cap free
Amount of items: 5
Items: 
Size: 1152 Color: 1
Size: 561 Color: 1
Size: 471 Color: 1
Size: 76 Color: 0
Size: 68 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 1783 Color: 1
Size: 469 Color: 1
Size: 76 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 1974 Color: 1
Size: 202 Color: 1
Size: 152 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 1954 Color: 1
Size: 298 Color: 1
Size: 76 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 1779 Color: 1
Size: 459 Color: 1
Size: 90 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 1574 Color: 1
Size: 630 Color: 1
Size: 124 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 1922 Color: 1
Size: 342 Color: 1
Size: 64 Color: 0

Bin 31: 0 of cap free
Amount of items: 4
Items: 
Size: 1994 Color: 1
Size: 246 Color: 1
Size: 48 Color: 0
Size: 40 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 1467 Color: 1
Size: 741 Color: 1
Size: 120 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 1554 Color: 1
Size: 738 Color: 1
Size: 36 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 1864 Color: 1
Size: 398 Color: 1
Size: 66 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 1710 Color: 1
Size: 462 Color: 1
Size: 156 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 1921 Color: 1
Size: 341 Color: 1
Size: 66 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 1166 Color: 1
Size: 970 Color: 1
Size: 192 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 1689 Color: 1
Size: 533 Color: 1
Size: 106 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 1858 Color: 1
Size: 282 Color: 1
Size: 188 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 1693 Color: 1
Size: 451 Color: 1
Size: 184 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 1690 Color: 1
Size: 534 Color: 1
Size: 104 Color: 0

Bin 42: 0 of cap free
Amount of items: 5
Items: 
Size: 1327 Color: 1
Size: 537 Color: 1
Size: 324 Color: 1
Size: 76 Color: 0
Size: 64 Color: 0

Bin 43: 1 of cap free
Amount of items: 4
Items: 
Size: 1651 Color: 1
Size: 466 Color: 1
Size: 106 Color: 0
Size: 104 Color: 0

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 1898 Color: 1
Size: 337 Color: 1
Size: 92 Color: 0

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 1446 Color: 1
Size: 829 Color: 1
Size: 52 Color: 0

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 1979 Color: 1
Size: 258 Color: 1
Size: 90 Color: 0

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 1685 Color: 1
Size: 554 Color: 1
Size: 88 Color: 0

Bin 48: 2 of cap free
Amount of items: 5
Items: 
Size: 1266 Color: 1
Size: 770 Color: 1
Size: 192 Color: 1
Size: 58 Color: 0
Size: 40 Color: 0

Bin 49: 3 of cap free
Amount of items: 3
Items: 
Size: 1406 Color: 1
Size: 631 Color: 1
Size: 288 Color: 0

Bin 50: 3 of cap free
Amount of items: 5
Items: 
Size: 1016 Color: 1
Size: 646 Color: 1
Size: 531 Color: 1
Size: 88 Color: 0
Size: 44 Color: 0

Bin 51: 3 of cap free
Amount of items: 3
Items: 
Size: 1799 Color: 1
Size: 478 Color: 1
Size: 48 Color: 0

Bin 52: 3 of cap free
Amount of items: 3
Items: 
Size: 1855 Color: 1
Size: 362 Color: 1
Size: 108 Color: 0

Bin 53: 5 of cap free
Amount of items: 5
Items: 
Size: 926 Color: 1
Size: 886 Color: 1
Size: 391 Color: 1
Size: 92 Color: 0
Size: 28 Color: 0

Bin 54: 8 of cap free
Amount of items: 3
Items: 
Size: 2094 Color: 1
Size: 198 Color: 1
Size: 28 Color: 0

Bin 55: 10 of cap free
Amount of items: 3
Items: 
Size: 1765 Color: 1
Size: 441 Color: 1
Size: 112 Color: 0

Bin 56: 12 of cap free
Amount of items: 3
Items: 
Size: 2034 Color: 1
Size: 226 Color: 1
Size: 56 Color: 0

Bin 57: 14 of cap free
Amount of items: 3
Items: 
Size: 1335 Color: 1
Size: 835 Color: 1
Size: 144 Color: 0

Bin 58: 26 of cap free
Amount of items: 2
Items: 
Size: 1854 Color: 1
Size: 448 Color: 0

Bin 59: 30 of cap free
Amount of items: 3
Items: 
Size: 1666 Color: 1
Size: 578 Color: 1
Size: 54 Color: 0

Bin 60: 88 of cap free
Amount of items: 3
Items: 
Size: 1165 Color: 1
Size: 971 Color: 1
Size: 104 Color: 0

Bin 61: 242 of cap free
Amount of items: 1
Items: 
Size: 2086 Color: 1

Bin 62: 270 of cap free
Amount of items: 1
Items: 
Size: 2058 Color: 1

Bin 63: 306 of cap free
Amount of items: 1
Items: 
Size: 2022 Color: 1

Bin 64: 329 of cap free
Amount of items: 1
Items: 
Size: 1999 Color: 1

Bin 65: 399 of cap free
Amount of items: 1
Items: 
Size: 1929 Color: 1

Bin 66: 570 of cap free
Amount of items: 1
Items: 
Size: 1758 Color: 1

Total size: 151320
Total free space: 2328

