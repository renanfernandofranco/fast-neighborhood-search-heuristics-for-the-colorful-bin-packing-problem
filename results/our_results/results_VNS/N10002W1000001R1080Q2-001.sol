Capicity Bin: 1000001
Lower Bound: 4470

Bins used: 4474
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 580143 Color: 0
Size: 294571 Color: 1
Size: 125287 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 414241 Color: 1
Size: 325925 Color: 1
Size: 259835 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 595589 Color: 1
Size: 290298 Color: 1
Size: 114114 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 770238 Color: 1
Size: 122259 Color: 1
Size: 107504 Color: 0

Bin 5: 0 of cap free
Amount of items: 7
Items: 
Size: 152926 Color: 1
Size: 152766 Color: 1
Size: 152465 Color: 1
Size: 150961 Color: 0
Size: 150641 Color: 0
Size: 126766 Color: 0
Size: 113476 Color: 0

Bin 6: 0 of cap free
Amount of items: 5
Items: 
Size: 266821 Color: 1
Size: 266630 Color: 1
Size: 245184 Color: 0
Size: 118554 Color: 0
Size: 102812 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 538448 Color: 1
Size: 317004 Color: 0
Size: 144549 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 561933 Color: 0
Size: 318173 Color: 1
Size: 119895 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 597847 Color: 1
Size: 287523 Color: 0
Size: 114631 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 758443 Color: 0
Size: 139867 Color: 1
Size: 101691 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 470135 Color: 1
Size: 429240 Color: 0
Size: 100626 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 786329 Color: 1
Size: 108946 Color: 0
Size: 104726 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 469601 Color: 1
Size: 430110 Color: 0
Size: 100290 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 464439 Color: 0
Size: 435358 Color: 1
Size: 100204 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 399900 Color: 1
Size: 311068 Color: 0
Size: 289033 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 448679 Color: 1
Size: 445192 Color: 0
Size: 106130 Color: 1

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 631285 Color: 0
Size: 265805 Color: 1
Size: 102911 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 775234 Color: 0
Size: 118965 Color: 0
Size: 105802 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 778407 Color: 0
Size: 115867 Color: 1
Size: 105727 Color: 1

Bin 20: 0 of cap free
Amount of items: 4
Items: 
Size: 373723 Color: 0
Size: 299119 Color: 1
Size: 207352 Color: 1
Size: 119807 Color: 0

Bin 21: 0 of cap free
Amount of items: 5
Items: 
Size: 505717 Color: 0
Size: 139917 Color: 1
Size: 124406 Color: 1
Size: 119799 Color: 0
Size: 110162 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 744612 Color: 1
Size: 150233 Color: 0
Size: 105156 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 708737 Color: 0
Size: 176438 Color: 1
Size: 114826 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 760142 Color: 1
Size: 127120 Color: 0
Size: 112739 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 742923 Color: 0
Size: 128973 Color: 0
Size: 128105 Color: 1

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 747084 Color: 0
Size: 149143 Color: 1
Size: 103774 Color: 1

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 791783 Color: 1
Size: 105364 Color: 1
Size: 102854 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 605721 Color: 1
Size: 274953 Color: 0
Size: 119327 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 705021 Color: 0
Size: 154547 Color: 1
Size: 140433 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 719680 Color: 0
Size: 156598 Color: 0
Size: 123723 Color: 1

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 755119 Color: 0
Size: 131999 Color: 1
Size: 112883 Color: 1

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 607179 Color: 0
Size: 240904 Color: 1
Size: 151918 Color: 1

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 729991 Color: 0
Size: 160636 Color: 1
Size: 109374 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 674702 Color: 0
Size: 164674 Color: 1
Size: 160625 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 653697 Color: 1
Size: 243759 Color: 0
Size: 102545 Color: 1

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 677857 Color: 0
Size: 212565 Color: 1
Size: 109579 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 587993 Color: 1
Size: 207873 Color: 1
Size: 204135 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 574738 Color: 0
Size: 305265 Color: 1
Size: 119998 Color: 1

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 716524 Color: 0
Size: 164174 Color: 0
Size: 119303 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 792431 Color: 1
Size: 103976 Color: 0
Size: 103594 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 741802 Color: 0
Size: 157812 Color: 1
Size: 100387 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 613945 Color: 0
Size: 284745 Color: 1
Size: 101311 Color: 1

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 717108 Color: 0
Size: 155536 Color: 1
Size: 127357 Color: 1

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 788905 Color: 1
Size: 107804 Color: 0
Size: 103292 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 451185 Color: 0
Size: 437271 Color: 1
Size: 111545 Color: 1

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 561553 Color: 1
Size: 326810 Color: 0
Size: 111638 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 619034 Color: 0
Size: 250807 Color: 0
Size: 130160 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 745484 Color: 0
Size: 133789 Color: 1
Size: 120728 Color: 1

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 539125 Color: 1
Size: 238913 Color: 0
Size: 221963 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 754225 Color: 0
Size: 124765 Color: 1
Size: 121011 Color: 1

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 719826 Color: 0
Size: 152184 Color: 0
Size: 127991 Color: 1

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 639338 Color: 1
Size: 249021 Color: 1
Size: 111642 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 749434 Color: 0
Size: 130148 Color: 1
Size: 120419 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 726975 Color: 0
Size: 136944 Color: 1
Size: 136082 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 744623 Color: 0
Size: 145676 Color: 1
Size: 109702 Color: 0

Bin 56: 0 of cap free
Amount of items: 2
Items: 
Size: 755152 Color: 1
Size: 244849 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 683281 Color: 0
Size: 161894 Color: 0
Size: 154826 Color: 1

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 712187 Color: 0
Size: 171614 Color: 1
Size: 116200 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 714645 Color: 0
Size: 180734 Color: 1
Size: 104622 Color: 1

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 700739 Color: 0
Size: 149966 Color: 1
Size: 149296 Color: 1

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 718765 Color: 0
Size: 167682 Color: 1
Size: 113554 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 763236 Color: 1
Size: 120375 Color: 1
Size: 116390 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 531170 Color: 1
Size: 283486 Color: 1
Size: 185345 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 629046 Color: 0
Size: 212148 Color: 1
Size: 158807 Color: 1

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 360687 Color: 1
Size: 356379 Color: 0
Size: 282935 Color: 1

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 462712 Color: 1
Size: 429322 Color: 0
Size: 107967 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 736758 Color: 1
Size: 137399 Color: 1
Size: 125844 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 782991 Color: 0
Size: 108946 Color: 1
Size: 108064 Color: 0

Bin 69: 0 of cap free
Amount of items: 4
Items: 
Size: 332217 Color: 1
Size: 311108 Color: 1
Size: 251208 Color: 0
Size: 105468 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 708655 Color: 1
Size: 150076 Color: 0
Size: 141270 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 724777 Color: 1
Size: 139703 Color: 0
Size: 135521 Color: 1

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 783709 Color: 1
Size: 112261 Color: 0
Size: 104031 Color: 1

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 740539 Color: 1
Size: 133677 Color: 0
Size: 125785 Color: 1

Bin 74: 0 of cap free
Amount of items: 6
Items: 
Size: 182279 Color: 1
Size: 180228 Color: 1
Size: 178474 Color: 1
Size: 178046 Color: 0
Size: 176895 Color: 0
Size: 104079 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 644184 Color: 1
Size: 233617 Color: 0
Size: 122200 Color: 1

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 487867 Color: 1
Size: 308621 Color: 1
Size: 203513 Color: 0

Bin 77: 0 of cap free
Amount of items: 6
Items: 
Size: 214281 Color: 0
Size: 175130 Color: 1
Size: 174208 Color: 1
Size: 174150 Color: 1
Size: 138383 Color: 0
Size: 123849 Color: 0

Bin 78: 0 of cap free
Amount of items: 6
Items: 
Size: 203185 Color: 1
Size: 171336 Color: 0
Size: 170133 Color: 0
Size: 166628 Color: 1
Size: 156068 Color: 0
Size: 132651 Color: 1

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 770098 Color: 1
Size: 129166 Color: 0
Size: 100737 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 715960 Color: 1
Size: 177732 Color: 0
Size: 106309 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 601075 Color: 0
Size: 252875 Color: 1
Size: 146051 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 580419 Color: 0
Size: 210669 Color: 1
Size: 208913 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 758869 Color: 1
Size: 129597 Color: 0
Size: 111535 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 732139 Color: 1
Size: 134967 Color: 0
Size: 132895 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 543901 Color: 1
Size: 346240 Color: 0
Size: 109860 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 619999 Color: 0
Size: 260461 Color: 1
Size: 119541 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 747519 Color: 0
Size: 143593 Color: 1
Size: 108889 Color: 1

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 707110 Color: 1
Size: 173609 Color: 0
Size: 119282 Color: 1

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 467438 Color: 0
Size: 312332 Color: 1
Size: 220231 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 625406 Color: 1
Size: 233260 Color: 0
Size: 141335 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 719569 Color: 1
Size: 167628 Color: 1
Size: 112804 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 708430 Color: 1
Size: 172001 Color: 0
Size: 119570 Color: 0

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 634711 Color: 0
Size: 263399 Color: 1
Size: 101891 Color: 1

Bin 94: 0 of cap free
Amount of items: 7
Items: 
Size: 195444 Color: 0
Size: 163266 Color: 0
Size: 133235 Color: 1
Size: 127671 Color: 1
Size: 127152 Color: 1
Size: 127056 Color: 1
Size: 126177 Color: 0

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 521857 Color: 1
Size: 349901 Color: 0
Size: 128243 Color: 0

Bin 96: 0 of cap free
Amount of items: 2
Items: 
Size: 776933 Color: 1
Size: 223068 Color: 0

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 715530 Color: 1
Size: 149820 Color: 0
Size: 134651 Color: 0

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 640027 Color: 0
Size: 242747 Color: 1
Size: 117227 Color: 0

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 477521 Color: 1
Size: 412413 Color: 0
Size: 110067 Color: 0

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 731560 Color: 1
Size: 134377 Color: 0
Size: 134064 Color: 0

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 721655 Color: 1
Size: 177238 Color: 0
Size: 101108 Color: 1

Bin 102: 0 of cap free
Amount of items: 2
Items: 
Size: 607173 Color: 0
Size: 392828 Color: 1

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 724260 Color: 1
Size: 138010 Color: 0
Size: 137731 Color: 1

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 696905 Color: 1
Size: 159593 Color: 0
Size: 143503 Color: 1

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 749103 Color: 1
Size: 127966 Color: 0
Size: 122932 Color: 1

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 759567 Color: 0
Size: 125630 Color: 1
Size: 114804 Color: 1

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 441653 Color: 0
Size: 440883 Color: 1
Size: 117465 Color: 1

Bin 108: 0 of cap free
Amount of items: 5
Items: 
Size: 266157 Color: 1
Size: 236267 Color: 0
Size: 226371 Color: 0
Size: 143192 Color: 1
Size: 128014 Color: 0

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 658097 Color: 1
Size: 181832 Color: 1
Size: 160072 Color: 0

Bin 110: 0 of cap free
Amount of items: 7
Items: 
Size: 180012 Color: 1
Size: 141759 Color: 0
Size: 141730 Color: 0
Size: 140640 Color: 0
Size: 138056 Color: 1
Size: 137981 Color: 1
Size: 119823 Color: 0

Bin 111: 0 of cap free
Amount of items: 7
Items: 
Size: 247304 Color: 1
Size: 140991 Color: 0
Size: 138901 Color: 1
Size: 122667 Color: 1
Size: 122547 Color: 0
Size: 121575 Color: 1
Size: 106016 Color: 0

Bin 112: 0 of cap free
Amount of items: 5
Items: 
Size: 359642 Color: 1
Size: 169954 Color: 1
Size: 161045 Color: 0
Size: 160604 Color: 0
Size: 148756 Color: 1

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 354170 Color: 0
Size: 334582 Color: 1
Size: 311249 Color: 0

Bin 114: 0 of cap free
Amount of items: 5
Items: 
Size: 313648 Color: 0
Size: 190613 Color: 1
Size: 187850 Color: 0
Size: 163216 Color: 1
Size: 144674 Color: 0

Bin 115: 0 of cap free
Amount of items: 7
Items: 
Size: 191060 Color: 0
Size: 155027 Color: 1
Size: 135341 Color: 0
Size: 135004 Color: 0
Size: 134867 Color: 1
Size: 134639 Color: 1
Size: 114063 Color: 1

Bin 116: 0 of cap free
Amount of items: 7
Items: 
Size: 228227 Color: 0
Size: 191430 Color: 1
Size: 118524 Color: 1
Size: 117292 Color: 1
Size: 116208 Color: 1
Size: 114842 Color: 0
Size: 113478 Color: 0

Bin 117: 0 of cap free
Amount of items: 6
Items: 
Size: 261534 Color: 0
Size: 217860 Color: 0
Size: 131373 Color: 0
Size: 130215 Color: 1
Size: 129795 Color: 1
Size: 129224 Color: 1

Bin 118: 0 of cap free
Amount of items: 7
Items: 
Size: 333379 Color: 1
Size: 117036 Color: 1
Size: 116747 Color: 1
Size: 110470 Color: 0
Size: 109877 Color: 0
Size: 107467 Color: 0
Size: 105025 Color: 1

Bin 119: 0 of cap free
Amount of items: 5
Items: 
Size: 276806 Color: 0
Size: 227643 Color: 0
Size: 221583 Color: 1
Size: 137928 Color: 1
Size: 136041 Color: 1

Bin 120: 0 of cap free
Amount of items: 6
Items: 
Size: 257040 Color: 1
Size: 228930 Color: 0
Size: 135659 Color: 1
Size: 128736 Color: 0
Size: 128395 Color: 1
Size: 121241 Color: 0

Bin 121: 0 of cap free
Amount of items: 4
Items: 
Size: 459736 Color: 0
Size: 220626 Color: 1
Size: 211018 Color: 0
Size: 108621 Color: 1

Bin 122: 0 of cap free
Amount of items: 7
Items: 
Size: 147345 Color: 1
Size: 147066 Color: 1
Size: 146786 Color: 0
Size: 146514 Color: 0
Size: 146108 Color: 1
Size: 139661 Color: 0
Size: 126521 Color: 1

Bin 123: 0 of cap free
Amount of items: 5
Items: 
Size: 225709 Color: 1
Size: 217321 Color: 1
Size: 197716 Color: 0
Size: 189847 Color: 1
Size: 169408 Color: 0

Bin 124: 0 of cap free
Amount of items: 5
Items: 
Size: 229307 Color: 1
Size: 229292 Color: 1
Size: 204002 Color: 0
Size: 191010 Color: 1
Size: 146390 Color: 0

Bin 125: 0 of cap free
Amount of items: 5
Items: 
Size: 240724 Color: 1
Size: 235120 Color: 1
Size: 219385 Color: 0
Size: 193834 Color: 0
Size: 110938 Color: 0

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 429400 Color: 1
Size: 347268 Color: 0
Size: 223333 Color: 1

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 433307 Color: 0
Size: 414160 Color: 1
Size: 152534 Color: 0

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 434667 Color: 0
Size: 433926 Color: 0
Size: 131408 Color: 1

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 434415 Color: 1
Size: 403104 Color: 0
Size: 162482 Color: 1

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 434733 Color: 0
Size: 414475 Color: 1
Size: 150793 Color: 0

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 436874 Color: 1
Size: 391163 Color: 0
Size: 171964 Color: 0

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 436930 Color: 1
Size: 436703 Color: 1
Size: 126368 Color: 0

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 441974 Color: 0
Size: 429989 Color: 1
Size: 128038 Color: 0

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 442342 Color: 0
Size: 369147 Color: 1
Size: 188512 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 449333 Color: 1
Size: 376215 Color: 0
Size: 174453 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 449367 Color: 1
Size: 433975 Color: 0
Size: 116659 Color: 1

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 449818 Color: 1
Size: 435158 Color: 0
Size: 115025 Color: 1

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 450284 Color: 1
Size: 365991 Color: 1
Size: 183726 Color: 0

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 450615 Color: 1
Size: 371737 Color: 1
Size: 177649 Color: 0

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 450661 Color: 1
Size: 435051 Color: 0
Size: 114289 Color: 0

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 451221 Color: 1
Size: 407124 Color: 1
Size: 141656 Color: 0

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 451243 Color: 1
Size: 403862 Color: 0
Size: 144896 Color: 1

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 452020 Color: 1
Size: 442420 Color: 0
Size: 105561 Color: 0

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 457411 Color: 1
Size: 361399 Color: 1
Size: 181191 Color: 0

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 458957 Color: 1
Size: 392115 Color: 0
Size: 148929 Color: 0

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 461603 Color: 1
Size: 346824 Color: 0
Size: 191574 Color: 1

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 464118 Color: 0
Size: 361517 Color: 1
Size: 174366 Color: 0

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 471290 Color: 1
Size: 330642 Color: 0
Size: 198069 Color: 0

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 471307 Color: 1
Size: 365850 Color: 1
Size: 162844 Color: 0

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 472484 Color: 0
Size: 344103 Color: 0
Size: 183414 Color: 1

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 472171 Color: 1
Size: 311091 Color: 0
Size: 216739 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 473352 Color: 1
Size: 343503 Color: 0
Size: 183146 Color: 0

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 473393 Color: 1
Size: 342669 Color: 0
Size: 183939 Color: 1

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 474098 Color: 0
Size: 326923 Color: 0
Size: 198980 Color: 1

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 478488 Color: 0
Size: 405101 Color: 1
Size: 116412 Color: 1

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 478643 Color: 0
Size: 361063 Color: 1
Size: 160295 Color: 0

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 487388 Color: 1
Size: 347423 Color: 0
Size: 165190 Color: 1

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 487670 Color: 1
Size: 391045 Color: 0
Size: 121286 Color: 1

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 500688 Color: 1
Size: 343353 Color: 0
Size: 155960 Color: 1

Bin 160: 0 of cap free
Amount of items: 2
Items: 
Size: 501595 Color: 0
Size: 498406 Color: 1

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 501939 Color: 0
Size: 304341 Color: 0
Size: 193721 Color: 1

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 502530 Color: 0
Size: 360679 Color: 1
Size: 136792 Color: 0

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 502886 Color: 1
Size: 311041 Color: 0
Size: 186074 Color: 1

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 503665 Color: 0
Size: 316888 Color: 1
Size: 179448 Color: 1

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 505894 Color: 1
Size: 305682 Color: 1
Size: 188425 Color: 0

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 507638 Color: 0
Size: 305981 Color: 1
Size: 186382 Color: 1

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 511540 Color: 1
Size: 289883 Color: 1
Size: 198578 Color: 0

Bin 168: 0 of cap free
Amount of items: 3
Items: 
Size: 511958 Color: 0
Size: 330764 Color: 0
Size: 157279 Color: 1

Bin 169: 0 of cap free
Amount of items: 3
Items: 
Size: 511858 Color: 1
Size: 304025 Color: 0
Size: 184118 Color: 1

Bin 170: 0 of cap free
Amount of items: 3
Items: 
Size: 513139 Color: 0
Size: 296569 Color: 1
Size: 190293 Color: 0

Bin 171: 0 of cap free
Amount of items: 3
Items: 
Size: 515469 Color: 1
Size: 323618 Color: 0
Size: 160914 Color: 1

Bin 172: 0 of cap free
Amount of items: 3
Items: 
Size: 518182 Color: 1
Size: 283099 Color: 1
Size: 198720 Color: 0

Bin 173: 0 of cap free
Amount of items: 3
Items: 
Size: 520182 Color: 1
Size: 366729 Color: 1
Size: 113090 Color: 0

Bin 174: 0 of cap free
Amount of items: 2
Items: 
Size: 521181 Color: 0
Size: 478820 Color: 1

Bin 175: 0 of cap free
Amount of items: 3
Items: 
Size: 525228 Color: 1
Size: 324025 Color: 0
Size: 150748 Color: 1

Bin 176: 0 of cap free
Amount of items: 3
Items: 
Size: 528970 Color: 0
Size: 320532 Color: 1
Size: 150499 Color: 1

Bin 177: 0 of cap free
Amount of items: 3
Items: 
Size: 531038 Color: 0
Size: 338679 Color: 1
Size: 130284 Color: 1

Bin 178: 0 of cap free
Amount of items: 3
Items: 
Size: 536451 Color: 1
Size: 304932 Color: 0
Size: 158618 Color: 1

Bin 179: 0 of cap free
Amount of items: 3
Items: 
Size: 536902 Color: 0
Size: 332259 Color: 1
Size: 130840 Color: 0

Bin 180: 0 of cap free
Amount of items: 3
Items: 
Size: 536895 Color: 1
Size: 300834 Color: 1
Size: 162272 Color: 0

Bin 181: 0 of cap free
Amount of items: 3
Items: 
Size: 537232 Color: 0
Size: 354446 Color: 1
Size: 108323 Color: 0

Bin 182: 0 of cap free
Amount of items: 3
Items: 
Size: 537570 Color: 1
Size: 304876 Color: 1
Size: 157555 Color: 0

Bin 183: 0 of cap free
Amount of items: 3
Items: 
Size: 539047 Color: 1
Size: 278735 Color: 0
Size: 182219 Color: 1

Bin 184: 0 of cap free
Amount of items: 3
Items: 
Size: 541820 Color: 1
Size: 304949 Color: 0
Size: 153232 Color: 1

Bin 185: 0 of cap free
Amount of items: 3
Items: 
Size: 541973 Color: 1
Size: 326910 Color: 0
Size: 131118 Color: 0

Bin 186: 0 of cap free
Amount of items: 3
Items: 
Size: 543490 Color: 1
Size: 315967 Color: 1
Size: 140544 Color: 0

Bin 187: 0 of cap free
Amount of items: 3
Items: 
Size: 545263 Color: 0
Size: 332250 Color: 1
Size: 122488 Color: 1

Bin 188: 0 of cap free
Amount of items: 3
Items: 
Size: 549774 Color: 1
Size: 251058 Color: 1
Size: 199169 Color: 0

Bin 189: 0 of cap free
Amount of items: 3
Items: 
Size: 551768 Color: 1
Size: 304893 Color: 0
Size: 143340 Color: 1

Bin 190: 0 of cap free
Amount of items: 3
Items: 
Size: 551795 Color: 0
Size: 289245 Color: 1
Size: 158961 Color: 0

Bin 191: 0 of cap free
Amount of items: 3
Items: 
Size: 551817 Color: 1
Size: 250644 Color: 0
Size: 197540 Color: 1

Bin 192: 0 of cap free
Amount of items: 3
Items: 
Size: 552091 Color: 0
Size: 338109 Color: 1
Size: 109801 Color: 0

Bin 193: 0 of cap free
Amount of items: 3
Items: 
Size: 552092 Color: 0
Size: 311417 Color: 1
Size: 136492 Color: 1

Bin 194: 0 of cap free
Amount of items: 2
Items: 
Size: 557454 Color: 1
Size: 442547 Color: 0

Bin 195: 0 of cap free
Amount of items: 3
Items: 
Size: 570402 Color: 0
Size: 302777 Color: 1
Size: 126822 Color: 0

Bin 196: 0 of cap free
Amount of items: 3
Items: 
Size: 570428 Color: 1
Size: 280470 Color: 0
Size: 149103 Color: 1

Bin 197: 0 of cap free
Amount of items: 3
Items: 
Size: 570715 Color: 0
Size: 300415 Color: 1
Size: 128871 Color: 0

Bin 198: 0 of cap free
Amount of items: 3
Items: 
Size: 570835 Color: 0
Size: 284524 Color: 1
Size: 144642 Color: 0

Bin 199: 0 of cap free
Amount of items: 3
Items: 
Size: 573137 Color: 0
Size: 303973 Color: 0
Size: 122891 Color: 1

Bin 200: 0 of cap free
Amount of items: 3
Items: 
Size: 573196 Color: 0
Size: 256266 Color: 1
Size: 170539 Color: 1

Bin 201: 0 of cap free
Amount of items: 3
Items: 
Size: 573764 Color: 1
Size: 262045 Color: 1
Size: 164192 Color: 0

Bin 202: 0 of cap free
Amount of items: 3
Items: 
Size: 574979 Color: 1
Size: 318759 Color: 1
Size: 106263 Color: 0

Bin 203: 0 of cap free
Amount of items: 2
Items: 
Size: 575747 Color: 1
Size: 424254 Color: 0

Bin 204: 0 of cap free
Amount of items: 3
Items: 
Size: 576230 Color: 1
Size: 301512 Color: 1
Size: 122259 Color: 0

Bin 205: 0 of cap free
Amount of items: 3
Items: 
Size: 578301 Color: 1
Size: 302373 Color: 1
Size: 119327 Color: 0

Bin 206: 0 of cap free
Amount of items: 2
Items: 
Size: 579042 Color: 0
Size: 420959 Color: 1

Bin 207: 0 of cap free
Amount of items: 3
Items: 
Size: 580224 Color: 0
Size: 228381 Color: 1
Size: 191396 Color: 1

Bin 208: 0 of cap free
Amount of items: 3
Items: 
Size: 581782 Color: 1
Size: 305678 Color: 1
Size: 112541 Color: 0

Bin 209: 0 of cap free
Amount of items: 3
Items: 
Size: 585291 Color: 0
Size: 306102 Color: 1
Size: 108608 Color: 1

Bin 210: 0 of cap free
Amount of items: 3
Items: 
Size: 587143 Color: 0
Size: 296576 Color: 1
Size: 116282 Color: 1

Bin 211: 0 of cap free
Amount of items: 3
Items: 
Size: 588699 Color: 0
Size: 296912 Color: 1
Size: 114390 Color: 1

Bin 212: 0 of cap free
Amount of items: 2
Items: 
Size: 590214 Color: 0
Size: 409787 Color: 1

Bin 213: 0 of cap free
Amount of items: 3
Items: 
Size: 597094 Color: 1
Size: 289066 Color: 1
Size: 113841 Color: 0

Bin 214: 0 of cap free
Amount of items: 2
Items: 
Size: 597323 Color: 0
Size: 402678 Color: 1

Bin 215: 0 of cap free
Amount of items: 2
Items: 
Size: 597954 Color: 0
Size: 402047 Color: 1

Bin 216: 0 of cap free
Amount of items: 3
Items: 
Size: 599954 Color: 1
Size: 237862 Color: 0
Size: 162185 Color: 0

Bin 217: 0 of cap free
Amount of items: 2
Items: 
Size: 600360 Color: 1
Size: 399641 Color: 0

Bin 218: 0 of cap free
Amount of items: 3
Items: 
Size: 602293 Color: 1
Size: 221692 Color: 0
Size: 176016 Color: 1

Bin 219: 0 of cap free
Amount of items: 3
Items: 
Size: 602403 Color: 1
Size: 218571 Color: 0
Size: 179027 Color: 0

Bin 220: 0 of cap free
Amount of items: 3
Items: 
Size: 603975 Color: 1
Size: 250921 Color: 0
Size: 145105 Color: 1

Bin 221: 0 of cap free
Amount of items: 3
Items: 
Size: 604061 Color: 0
Size: 199150 Color: 0
Size: 196790 Color: 1

Bin 222: 0 of cap free
Amount of items: 3
Items: 
Size: 604077 Color: 0
Size: 235901 Color: 1
Size: 160023 Color: 1

Bin 223: 0 of cap free
Amount of items: 3
Items: 
Size: 604859 Color: 0
Size: 228546 Color: 1
Size: 166596 Color: 1

Bin 224: 0 of cap free
Amount of items: 2
Items: 
Size: 605198 Color: 1
Size: 394803 Color: 0

Bin 225: 0 of cap free
Amount of items: 3
Items: 
Size: 606415 Color: 1
Size: 250729 Color: 0
Size: 142857 Color: 1

Bin 226: 0 of cap free
Amount of items: 3
Items: 
Size: 607114 Color: 1
Size: 197617 Color: 0
Size: 195270 Color: 1

Bin 227: 0 of cap free
Amount of items: 3
Items: 
Size: 607478 Color: 1
Size: 196371 Color: 0
Size: 196152 Color: 1

Bin 228: 0 of cap free
Amount of items: 3
Items: 
Size: 607563 Color: 1
Size: 214600 Color: 1
Size: 177838 Color: 0

Bin 229: 0 of cap free
Amount of items: 3
Items: 
Size: 607518 Color: 0
Size: 197642 Color: 1
Size: 194841 Color: 0

Bin 230: 0 of cap free
Amount of items: 3
Items: 
Size: 607576 Color: 0
Size: 266567 Color: 1
Size: 125858 Color: 0

Bin 231: 0 of cap free
Amount of items: 3
Items: 
Size: 608060 Color: 0
Size: 284352 Color: 1
Size: 107589 Color: 1

Bin 232: 0 of cap free
Amount of items: 3
Items: 
Size: 609055 Color: 0
Size: 216583 Color: 1
Size: 174363 Color: 1

Bin 233: 0 of cap free
Amount of items: 3
Items: 
Size: 609330 Color: 0
Size: 250520 Color: 0
Size: 140151 Color: 1

Bin 234: 0 of cap free
Amount of items: 3
Items: 
Size: 609314 Color: 1
Size: 199544 Color: 1
Size: 191143 Color: 0

Bin 235: 0 of cap free
Amount of items: 3
Items: 
Size: 610110 Color: 1
Size: 197005 Color: 0
Size: 192886 Color: 1

Bin 236: 0 of cap free
Amount of items: 3
Items: 
Size: 613003 Color: 0
Size: 197663 Color: 1
Size: 189335 Color: 1

Bin 237: 0 of cap free
Amount of items: 3
Items: 
Size: 614108 Color: 0
Size: 212962 Color: 1
Size: 172931 Color: 1

Bin 238: 0 of cap free
Amount of items: 3
Items: 
Size: 615245 Color: 1
Size: 270023 Color: 1
Size: 114733 Color: 0

Bin 239: 0 of cap free
Amount of items: 3
Items: 
Size: 615600 Color: 0
Size: 192467 Color: 0
Size: 191934 Color: 1

Bin 240: 0 of cap free
Amount of items: 3
Items: 
Size: 615463 Color: 1
Size: 196623 Color: 0
Size: 187915 Color: 1

Bin 241: 0 of cap free
Amount of items: 3
Items: 
Size: 615701 Color: 0
Size: 213690 Color: 1
Size: 170610 Color: 0

Bin 242: 0 of cap free
Amount of items: 3
Items: 
Size: 616502 Color: 1
Size: 242097 Color: 0
Size: 141402 Color: 1

Bin 243: 0 of cap free
Amount of items: 3
Items: 
Size: 619398 Color: 1
Size: 197150 Color: 1
Size: 183453 Color: 0

Bin 244: 0 of cap free
Amount of items: 3
Items: 
Size: 619263 Color: 0
Size: 226451 Color: 0
Size: 154287 Color: 1

Bin 245: 0 of cap free
Amount of items: 3
Items: 
Size: 622316 Color: 1
Size: 190112 Color: 1
Size: 187573 Color: 0

Bin 246: 0 of cap free
Amount of items: 3
Items: 
Size: 625211 Color: 0
Size: 197455 Color: 1
Size: 177335 Color: 1

Bin 247: 0 of cap free
Amount of items: 2
Items: 
Size: 625638 Color: 0
Size: 374363 Color: 1

Bin 248: 0 of cap free
Amount of items: 3
Items: 
Size: 626843 Color: 1
Size: 196214 Color: 1
Size: 176944 Color: 0

Bin 249: 0 of cap free
Amount of items: 3
Items: 
Size: 628462 Color: 0
Size: 208171 Color: 1
Size: 163368 Color: 1

Bin 250: 0 of cap free
Amount of items: 3
Items: 
Size: 630460 Color: 0
Size: 190215 Color: 1
Size: 179326 Color: 0

Bin 251: 0 of cap free
Amount of items: 3
Items: 
Size: 630536 Color: 1
Size: 214691 Color: 1
Size: 154774 Color: 0

Bin 252: 0 of cap free
Amount of items: 3
Items: 
Size: 630640 Color: 0
Size: 237297 Color: 0
Size: 132064 Color: 1

Bin 253: 0 of cap free
Amount of items: 3
Items: 
Size: 631349 Color: 1
Size: 195845 Color: 0
Size: 172807 Color: 1

Bin 254: 0 of cap free
Amount of items: 3
Items: 
Size: 632019 Color: 0
Size: 214068 Color: 1
Size: 153914 Color: 0

Bin 255: 0 of cap free
Amount of items: 3
Items: 
Size: 632018 Color: 1
Size: 219461 Color: 0
Size: 148522 Color: 1

Bin 256: 0 of cap free
Amount of items: 3
Items: 
Size: 632583 Color: 1
Size: 198451 Color: 1
Size: 168967 Color: 0

Bin 257: 0 of cap free
Amount of items: 3
Items: 
Size: 633214 Color: 0
Size: 212864 Color: 1
Size: 153923 Color: 0

Bin 258: 0 of cap free
Amount of items: 3
Items: 
Size: 633199 Color: 1
Size: 190844 Color: 1
Size: 175958 Color: 0

Bin 259: 0 of cap free
Amount of items: 3
Items: 
Size: 634209 Color: 1
Size: 196228 Color: 1
Size: 169564 Color: 0

Bin 260: 0 of cap free
Amount of items: 3
Items: 
Size: 634312 Color: 0
Size: 197968 Color: 1
Size: 167721 Color: 0

Bin 261: 0 of cap free
Amount of items: 3
Items: 
Size: 634240 Color: 1
Size: 185884 Color: 0
Size: 179877 Color: 1

Bin 262: 0 of cap free
Amount of items: 3
Items: 
Size: 635382 Color: 1
Size: 186874 Color: 1
Size: 177745 Color: 0

Bin 263: 0 of cap free
Amount of items: 3
Items: 
Size: 636698 Color: 1
Size: 187842 Color: 1
Size: 175461 Color: 0

Bin 264: 0 of cap free
Amount of items: 2
Items: 
Size: 637789 Color: 0
Size: 362212 Color: 1

Bin 265: 0 of cap free
Amount of items: 3
Items: 
Size: 637948 Color: 1
Size: 185863 Color: 0
Size: 176190 Color: 1

Bin 266: 0 of cap free
Amount of items: 3
Items: 
Size: 640551 Color: 1
Size: 190726 Color: 1
Size: 168724 Color: 0

Bin 267: 0 of cap free
Amount of items: 3
Items: 
Size: 640610 Color: 0
Size: 182251 Color: 1
Size: 177140 Color: 0

Bin 268: 0 of cap free
Amount of items: 3
Items: 
Size: 641904 Color: 1
Size: 194968 Color: 1
Size: 163129 Color: 0

Bin 269: 0 of cap free
Amount of items: 3
Items: 
Size: 643671 Color: 1
Size: 189556 Color: 0
Size: 166774 Color: 1

Bin 270: 0 of cap free
Amount of items: 3
Items: 
Size: 643812 Color: 1
Size: 180765 Color: 1
Size: 175424 Color: 0

Bin 271: 0 of cap free
Amount of items: 3
Items: 
Size: 644143 Color: 0
Size: 188090 Color: 0
Size: 167768 Color: 1

Bin 272: 0 of cap free
Amount of items: 3
Items: 
Size: 644138 Color: 1
Size: 197537 Color: 0
Size: 158326 Color: 1

Bin 273: 0 of cap free
Amount of items: 3
Items: 
Size: 644829 Color: 1
Size: 190974 Color: 1
Size: 164198 Color: 0

Bin 274: 0 of cap free
Amount of items: 2
Items: 
Size: 645006 Color: 0
Size: 354995 Color: 1

Bin 275: 0 of cap free
Amount of items: 3
Items: 
Size: 645579 Color: 1
Size: 185998 Color: 1
Size: 168424 Color: 0

Bin 276: 0 of cap free
Amount of items: 3
Items: 
Size: 645589 Color: 0
Size: 188169 Color: 1
Size: 166243 Color: 0

Bin 277: 0 of cap free
Amount of items: 3
Items: 
Size: 645733 Color: 1
Size: 195028 Color: 0
Size: 159240 Color: 1

Bin 278: 0 of cap free
Amount of items: 3
Items: 
Size: 645729 Color: 0
Size: 184553 Color: 0
Size: 169719 Color: 1

Bin 279: 0 of cap free
Amount of items: 3
Items: 
Size: 645878 Color: 1
Size: 235260 Color: 1
Size: 118863 Color: 0

Bin 280: 0 of cap free
Amount of items: 3
Items: 
Size: 646017 Color: 0
Size: 234873 Color: 1
Size: 119111 Color: 0

Bin 281: 0 of cap free
Amount of items: 3
Items: 
Size: 646954 Color: 1
Size: 188058 Color: 0
Size: 164989 Color: 1

Bin 282: 0 of cap free
Amount of items: 3
Items: 
Size: 648996 Color: 1
Size: 185927 Color: 1
Size: 165078 Color: 0

Bin 283: 0 of cap free
Amount of items: 3
Items: 
Size: 651563 Color: 1
Size: 175605 Color: 1
Size: 172833 Color: 0

Bin 284: 0 of cap free
Amount of items: 3
Items: 
Size: 651634 Color: 0
Size: 188392 Color: 1
Size: 159975 Color: 0

Bin 285: 0 of cap free
Amount of items: 3
Items: 
Size: 651649 Color: 1
Size: 185697 Color: 1
Size: 162655 Color: 0

Bin 286: 0 of cap free
Amount of items: 3
Items: 
Size: 651670 Color: 0
Size: 180029 Color: 1
Size: 168302 Color: 0

Bin 287: 0 of cap free
Amount of items: 3
Items: 
Size: 652181 Color: 1
Size: 181397 Color: 0
Size: 166423 Color: 1

Bin 288: 0 of cap free
Amount of items: 3
Items: 
Size: 652287 Color: 0
Size: 196151 Color: 0
Size: 151563 Color: 1

Bin 289: 0 of cap free
Amount of items: 2
Items: 
Size: 654510 Color: 1
Size: 345491 Color: 0

Bin 290: 0 of cap free
Amount of items: 3
Items: 
Size: 654818 Color: 1
Size: 174186 Color: 1
Size: 170997 Color: 0

Bin 291: 0 of cap free
Amount of items: 3
Items: 
Size: 658085 Color: 1
Size: 181480 Color: 0
Size: 160436 Color: 1

Bin 292: 0 of cap free
Amount of items: 3
Items: 
Size: 658347 Color: 1
Size: 173916 Color: 1
Size: 167738 Color: 0

Bin 293: 0 of cap free
Amount of items: 3
Items: 
Size: 658515 Color: 0
Size: 176941 Color: 1
Size: 164545 Color: 1

Bin 294: 0 of cap free
Amount of items: 3
Items: 
Size: 662183 Color: 1
Size: 177291 Color: 1
Size: 160527 Color: 0

Bin 295: 0 of cap free
Amount of items: 3
Items: 
Size: 664325 Color: 0
Size: 217188 Color: 1
Size: 118488 Color: 1

Bin 296: 0 of cap free
Amount of items: 2
Items: 
Size: 664793 Color: 0
Size: 335208 Color: 1

Bin 297: 0 of cap free
Amount of items: 3
Items: 
Size: 665709 Color: 1
Size: 213835 Color: 1
Size: 120457 Color: 0

Bin 298: 0 of cap free
Amount of items: 3
Items: 
Size: 667991 Color: 1
Size: 170408 Color: 1
Size: 161602 Color: 0

Bin 299: 0 of cap free
Amount of items: 3
Items: 
Size: 668152 Color: 0
Size: 187479 Color: 1
Size: 144370 Color: 0

Bin 300: 0 of cap free
Amount of items: 3
Items: 
Size: 669677 Color: 1
Size: 181014 Color: 0
Size: 149310 Color: 1

Bin 301: 0 of cap free
Amount of items: 3
Items: 
Size: 669815 Color: 1
Size: 175207 Color: 0
Size: 154979 Color: 0

Bin 302: 0 of cap free
Amount of items: 3
Items: 
Size: 669817 Color: 1
Size: 166811 Color: 0
Size: 163373 Color: 1

Bin 303: 0 of cap free
Amount of items: 3
Items: 
Size: 671677 Color: 1
Size: 184644 Color: 1
Size: 143680 Color: 0

Bin 304: 0 of cap free
Amount of items: 3
Items: 
Size: 671553 Color: 0
Size: 177420 Color: 1
Size: 151028 Color: 0

Bin 305: 0 of cap free
Amount of items: 3
Items: 
Size: 671813 Color: 0
Size: 177366 Color: 1
Size: 150822 Color: 1

Bin 306: 0 of cap free
Amount of items: 3
Items: 
Size: 672763 Color: 1
Size: 174947 Color: 0
Size: 152291 Color: 1

Bin 307: 0 of cap free
Amount of items: 3
Items: 
Size: 672849 Color: 0
Size: 210239 Color: 1
Size: 116913 Color: 0

Bin 308: 0 of cap free
Amount of items: 3
Items: 
Size: 673050 Color: 0
Size: 171144 Color: 1
Size: 155807 Color: 1

Bin 309: 0 of cap free
Amount of items: 3
Items: 
Size: 673105 Color: 0
Size: 190549 Color: 0
Size: 136347 Color: 1

Bin 310: 0 of cap free
Amount of items: 3
Items: 
Size: 673129 Color: 0
Size: 199172 Color: 1
Size: 127700 Color: 1

Bin 311: 0 of cap free
Amount of items: 3
Items: 
Size: 674038 Color: 1
Size: 208491 Color: 1
Size: 117472 Color: 0

Bin 312: 0 of cap free
Amount of items: 3
Items: 
Size: 675602 Color: 1
Size: 186257 Color: 0
Size: 138142 Color: 1

Bin 313: 0 of cap free
Amount of items: 3
Items: 
Size: 677286 Color: 1
Size: 183464 Color: 1
Size: 139251 Color: 0

Bin 314: 0 of cap free
Amount of items: 3
Items: 
Size: 678478 Color: 1
Size: 177823 Color: 1
Size: 143700 Color: 0

Bin 315: 0 of cap free
Amount of items: 2
Items: 
Size: 679577 Color: 0
Size: 320424 Color: 1

Bin 316: 0 of cap free
Amount of items: 3
Items: 
Size: 682952 Color: 1
Size: 187875 Color: 1
Size: 129174 Color: 0

Bin 317: 0 of cap free
Amount of items: 3
Items: 
Size: 684798 Color: 1
Size: 171546 Color: 0
Size: 143657 Color: 1

Bin 318: 0 of cap free
Amount of items: 3
Items: 
Size: 684946 Color: 0
Size: 167119 Color: 1
Size: 147936 Color: 0

Bin 319: 0 of cap free
Amount of items: 3
Items: 
Size: 684965 Color: 0
Size: 179440 Color: 1
Size: 135596 Color: 1

Bin 320: 0 of cap free
Amount of items: 3
Items: 
Size: 686890 Color: 0
Size: 161189 Color: 1
Size: 151922 Color: 1

Bin 321: 0 of cap free
Amount of items: 3
Items: 
Size: 689098 Color: 1
Size: 158813 Color: 1
Size: 152090 Color: 0

Bin 322: 0 of cap free
Amount of items: 2
Items: 
Size: 690031 Color: 0
Size: 309970 Color: 1

Bin 323: 0 of cap free
Amount of items: 3
Items: 
Size: 690739 Color: 1
Size: 168516 Color: 1
Size: 140746 Color: 0

Bin 324: 0 of cap free
Amount of items: 3
Items: 
Size: 692004 Color: 1
Size: 169650 Color: 1
Size: 138347 Color: 0

Bin 325: 0 of cap free
Amount of items: 3
Items: 
Size: 693290 Color: 1
Size: 174441 Color: 0
Size: 132270 Color: 1

Bin 326: 0 of cap free
Amount of items: 3
Items: 
Size: 696237 Color: 1
Size: 152816 Color: 1
Size: 150948 Color: 0

Bin 327: 0 of cap free
Amount of items: 3
Items: 
Size: 696731 Color: 1
Size: 160802 Color: 0
Size: 142468 Color: 1

Bin 328: 0 of cap free
Amount of items: 3
Items: 
Size: 696628 Color: 0
Size: 152524 Color: 1
Size: 150849 Color: 0

Bin 329: 0 of cap free
Amount of items: 3
Items: 
Size: 696728 Color: 0
Size: 172915 Color: 1
Size: 130358 Color: 0

Bin 330: 0 of cap free
Amount of items: 3
Items: 
Size: 696848 Color: 1
Size: 153875 Color: 0
Size: 149278 Color: 1

Bin 331: 0 of cap free
Amount of items: 3
Items: 
Size: 696864 Color: 1
Size: 160394 Color: 1
Size: 142743 Color: 0

Bin 332: 0 of cap free
Amount of items: 3
Items: 
Size: 696892 Color: 1
Size: 189151 Color: 0
Size: 113958 Color: 0

Bin 333: 0 of cap free
Amount of items: 3
Items: 
Size: 697622 Color: 1
Size: 161461 Color: 1
Size: 140918 Color: 0

Bin 334: 0 of cap free
Amount of items: 3
Items: 
Size: 699381 Color: 1
Size: 155001 Color: 1
Size: 145619 Color: 0

Bin 335: 0 of cap free
Amount of items: 3
Items: 
Size: 699468 Color: 0
Size: 192651 Color: 0
Size: 107882 Color: 1

Bin 336: 0 of cap free
Amount of items: 3
Items: 
Size: 701053 Color: 0
Size: 164323 Color: 0
Size: 134625 Color: 1

Bin 337: 0 of cap free
Amount of items: 3
Items: 
Size: 701017 Color: 1
Size: 178980 Color: 0
Size: 120004 Color: 1

Bin 338: 0 of cap free
Amount of items: 3
Items: 
Size: 701855 Color: 1
Size: 153708 Color: 1
Size: 144438 Color: 0

Bin 339: 0 of cap free
Amount of items: 3
Items: 
Size: 702084 Color: 0
Size: 168968 Color: 1
Size: 128949 Color: 0

Bin 340: 0 of cap free
Amount of items: 3
Items: 
Size: 702100 Color: 1
Size: 175021 Color: 1
Size: 122880 Color: 0

Bin 341: 0 of cap free
Amount of items: 3
Items: 
Size: 702237 Color: 0
Size: 183623 Color: 0
Size: 114141 Color: 1

Bin 342: 0 of cap free
Amount of items: 3
Items: 
Size: 702257 Color: 1
Size: 173314 Color: 0
Size: 124430 Color: 1

Bin 343: 0 of cap free
Amount of items: 3
Items: 
Size: 702848 Color: 1
Size: 192323 Color: 1
Size: 104830 Color: 0

Bin 344: 0 of cap free
Amount of items: 3
Items: 
Size: 702901 Color: 1
Size: 186022 Color: 0
Size: 111078 Color: 0

Bin 345: 0 of cap free
Amount of items: 3
Items: 
Size: 703171 Color: 1
Size: 183009 Color: 1
Size: 113821 Color: 0

Bin 346: 0 of cap free
Amount of items: 3
Items: 
Size: 703175 Color: 1
Size: 178566 Color: 0
Size: 118260 Color: 0

Bin 347: 0 of cap free
Amount of items: 3
Items: 
Size: 703434 Color: 0
Size: 190107 Color: 1
Size: 106460 Color: 1

Bin 348: 0 of cap free
Amount of items: 2
Items: 
Size: 704571 Color: 1
Size: 295430 Color: 0

Bin 349: 0 of cap free
Amount of items: 3
Items: 
Size: 704811 Color: 1
Size: 190263 Color: 1
Size: 104927 Color: 0

Bin 350: 0 of cap free
Amount of items: 3
Items: 
Size: 706626 Color: 0
Size: 170803 Color: 1
Size: 122572 Color: 0

Bin 351: 0 of cap free
Amount of items: 3
Items: 
Size: 706565 Color: 1
Size: 169821 Color: 0
Size: 123615 Color: 1

Bin 352: 0 of cap free
Amount of items: 3
Items: 
Size: 707859 Color: 1
Size: 175076 Color: 1
Size: 117066 Color: 0

Bin 353: 0 of cap free
Amount of items: 3
Items: 
Size: 709709 Color: 0
Size: 146609 Color: 1
Size: 143683 Color: 1

Bin 354: 0 of cap free
Amount of items: 3
Items: 
Size: 710930 Color: 1
Size: 186031 Color: 1
Size: 103040 Color: 0

Bin 355: 0 of cap free
Amount of items: 3
Items: 
Size: 711064 Color: 0
Size: 186169 Color: 1
Size: 102768 Color: 0

Bin 356: 0 of cap free
Amount of items: 3
Items: 
Size: 711068 Color: 0
Size: 176230 Color: 1
Size: 112703 Color: 1

Bin 357: 0 of cap free
Amount of items: 3
Items: 
Size: 712529 Color: 1
Size: 157862 Color: 1
Size: 129610 Color: 0

Bin 358: 0 of cap free
Amount of items: 3
Items: 
Size: 713559 Color: 1
Size: 166298 Color: 1
Size: 120144 Color: 0

Bin 359: 0 of cap free
Amount of items: 2
Items: 
Size: 714574 Color: 1
Size: 285427 Color: 0

Bin 360: 0 of cap free
Amount of items: 3
Items: 
Size: 715280 Color: 0
Size: 168401 Color: 1
Size: 116320 Color: 1

Bin 361: 0 of cap free
Amount of items: 3
Items: 
Size: 715491 Color: 1
Size: 161542 Color: 0
Size: 122968 Color: 0

Bin 362: 0 of cap free
Amount of items: 3
Items: 
Size: 715494 Color: 1
Size: 176089 Color: 1
Size: 108418 Color: 0

Bin 363: 0 of cap free
Amount of items: 3
Items: 
Size: 715511 Color: 0
Size: 158394 Color: 0
Size: 126096 Color: 1

Bin 364: 0 of cap free
Amount of items: 3
Items: 
Size: 715527 Color: 1
Size: 168060 Color: 0
Size: 116414 Color: 1

Bin 365: 0 of cap free
Amount of items: 3
Items: 
Size: 717156 Color: 0
Size: 158253 Color: 1
Size: 124592 Color: 1

Bin 366: 0 of cap free
Amount of items: 3
Items: 
Size: 717875 Color: 0
Size: 161837 Color: 1
Size: 120289 Color: 1

Bin 367: 0 of cap free
Amount of items: 3
Items: 
Size: 718113 Color: 1
Size: 142581 Color: 0
Size: 139307 Color: 1

Bin 368: 0 of cap free
Amount of items: 3
Items: 
Size: 718309 Color: 0
Size: 145410 Color: 0
Size: 136282 Color: 1

Bin 369: 0 of cap free
Amount of items: 3
Items: 
Size: 718562 Color: 1
Size: 161285 Color: 1
Size: 120154 Color: 0

Bin 370: 0 of cap free
Amount of items: 3
Items: 
Size: 718489 Color: 0
Size: 177533 Color: 0
Size: 103979 Color: 1

Bin 371: 0 of cap free
Amount of items: 3
Items: 
Size: 718732 Color: 0
Size: 161025 Color: 1
Size: 120244 Color: 1

Bin 372: 0 of cap free
Amount of items: 3
Items: 
Size: 719627 Color: 1
Size: 154968 Color: 1
Size: 125406 Color: 0

Bin 373: 0 of cap free
Amount of items: 3
Items: 
Size: 719612 Color: 0
Size: 164675 Color: 1
Size: 115714 Color: 0

Bin 374: 0 of cap free
Amount of items: 3
Items: 
Size: 720229 Color: 0
Size: 160292 Color: 0
Size: 119480 Color: 1

Bin 375: 0 of cap free
Amount of items: 3
Items: 
Size: 720784 Color: 1
Size: 150216 Color: 0
Size: 129001 Color: 1

Bin 376: 0 of cap free
Amount of items: 3
Items: 
Size: 721088 Color: 0
Size: 142535 Color: 1
Size: 136378 Color: 0

Bin 377: 0 of cap free
Amount of items: 3
Items: 
Size: 721111 Color: 1
Size: 166369 Color: 1
Size: 112521 Color: 0

Bin 378: 0 of cap free
Amount of items: 3
Items: 
Size: 721197 Color: 0
Size: 167545 Color: 1
Size: 111259 Color: 0

Bin 379: 0 of cap free
Amount of items: 3
Items: 
Size: 721322 Color: 1
Size: 157230 Color: 0
Size: 121449 Color: 1

Bin 380: 0 of cap free
Amount of items: 3
Items: 
Size: 722548 Color: 1
Size: 142914 Color: 1
Size: 134539 Color: 0

Bin 381: 0 of cap free
Amount of items: 3
Items: 
Size: 723298 Color: 1
Size: 146626 Color: 1
Size: 130077 Color: 0

Bin 382: 0 of cap free
Amount of items: 3
Items: 
Size: 723534 Color: 1
Size: 148365 Color: 1
Size: 128102 Color: 0

Bin 383: 0 of cap free
Amount of items: 3
Items: 
Size: 723637 Color: 0
Size: 165378 Color: 1
Size: 110986 Color: 0

Bin 384: 0 of cap free
Amount of items: 3
Items: 
Size: 724902 Color: 1
Size: 143875 Color: 1
Size: 131224 Color: 0

Bin 385: 0 of cap free
Amount of items: 3
Items: 
Size: 726856 Color: 1
Size: 163374 Color: 1
Size: 109771 Color: 0

Bin 386: 0 of cap free
Amount of items: 3
Items: 
Size: 726796 Color: 0
Size: 160995 Color: 0
Size: 112210 Color: 1

Bin 387: 0 of cap free
Amount of items: 3
Items: 
Size: 726953 Color: 0
Size: 136979 Color: 1
Size: 136069 Color: 1

Bin 388: 0 of cap free
Amount of items: 3
Items: 
Size: 728013 Color: 1
Size: 153988 Color: 0
Size: 118000 Color: 1

Bin 389: 0 of cap free
Amount of items: 3
Items: 
Size: 728780 Color: 0
Size: 163149 Color: 1
Size: 108072 Color: 0

Bin 390: 0 of cap free
Amount of items: 3
Items: 
Size: 728831 Color: 1
Size: 139962 Color: 1
Size: 131208 Color: 0

Bin 391: 0 of cap free
Amount of items: 3
Items: 
Size: 728845 Color: 0
Size: 162220 Color: 1
Size: 108936 Color: 0

Bin 392: 0 of cap free
Amount of items: 3
Items: 
Size: 729007 Color: 1
Size: 157159 Color: 1
Size: 113835 Color: 0

Bin 393: 0 of cap free
Amount of items: 3
Items: 
Size: 729065 Color: 1
Size: 143588 Color: 0
Size: 127348 Color: 1

Bin 394: 0 of cap free
Amount of items: 3
Items: 
Size: 729265 Color: 1
Size: 149363 Color: 1
Size: 121373 Color: 0

Bin 395: 0 of cap free
Amount of items: 3
Items: 
Size: 729838 Color: 1
Size: 157490 Color: 0
Size: 112673 Color: 1

Bin 396: 0 of cap free
Amount of items: 3
Items: 
Size: 729914 Color: 0
Size: 138834 Color: 0
Size: 131253 Color: 1

Bin 397: 0 of cap free
Amount of items: 3
Items: 
Size: 729841 Color: 1
Size: 165710 Color: 0
Size: 104450 Color: 1

Bin 398: 0 of cap free
Amount of items: 3
Items: 
Size: 730044 Color: 1
Size: 142457 Color: 0
Size: 127500 Color: 1

Bin 399: 0 of cap free
Amount of items: 3
Items: 
Size: 730016 Color: 0
Size: 142023 Color: 1
Size: 127962 Color: 0

Bin 400: 0 of cap free
Amount of items: 3
Items: 
Size: 730083 Color: 1
Size: 135332 Color: 0
Size: 134586 Color: 1

Bin 401: 0 of cap free
Amount of items: 3
Items: 
Size: 731225 Color: 1
Size: 140911 Color: 0
Size: 127865 Color: 1

Bin 402: 0 of cap free
Amount of items: 3
Items: 
Size: 731382 Color: 1
Size: 146393 Color: 0
Size: 122226 Color: 0

Bin 403: 0 of cap free
Amount of items: 3
Items: 
Size: 734314 Color: 0
Size: 140637 Color: 1
Size: 125050 Color: 1

Bin 404: 0 of cap free
Amount of items: 3
Items: 
Size: 735052 Color: 1
Size: 150793 Color: 0
Size: 114156 Color: 1

Bin 405: 0 of cap free
Amount of items: 3
Items: 
Size: 735384 Color: 1
Size: 139559 Color: 1
Size: 125058 Color: 0

Bin 406: 0 of cap free
Amount of items: 3
Items: 
Size: 735420 Color: 1
Size: 150654 Color: 0
Size: 113927 Color: 0

Bin 407: 0 of cap free
Amount of items: 3
Items: 
Size: 735696 Color: 1
Size: 143179 Color: 0
Size: 121126 Color: 1

Bin 408: 0 of cap free
Amount of items: 2
Items: 
Size: 735865 Color: 1
Size: 264136 Color: 0

Bin 409: 0 of cap free
Amount of items: 3
Items: 
Size: 736249 Color: 1
Size: 146617 Color: 1
Size: 117135 Color: 0

Bin 410: 0 of cap free
Amount of items: 3
Items: 
Size: 736693 Color: 1
Size: 138363 Color: 1
Size: 124945 Color: 0

Bin 411: 0 of cap free
Amount of items: 3
Items: 
Size: 737963 Color: 1
Size: 134782 Color: 0
Size: 127256 Color: 0

Bin 412: 0 of cap free
Amount of items: 3
Items: 
Size: 738646 Color: 1
Size: 132785 Color: 0
Size: 128570 Color: 1

Bin 413: 0 of cap free
Amount of items: 3
Items: 
Size: 738655 Color: 0
Size: 144845 Color: 1
Size: 116501 Color: 0

Bin 414: 0 of cap free
Amount of items: 3
Items: 
Size: 738900 Color: 1
Size: 143354 Color: 1
Size: 117747 Color: 0

Bin 415: 0 of cap free
Amount of items: 3
Items: 
Size: 738749 Color: 0
Size: 133264 Color: 0
Size: 127988 Color: 1

Bin 416: 0 of cap free
Amount of items: 3
Items: 
Size: 738967 Color: 1
Size: 157323 Color: 1
Size: 103711 Color: 0

Bin 417: 0 of cap free
Amount of items: 3
Items: 
Size: 739024 Color: 0
Size: 136769 Color: 1
Size: 124208 Color: 0

Bin 418: 0 of cap free
Amount of items: 3
Items: 
Size: 739179 Color: 1
Size: 146048 Color: 0
Size: 114774 Color: 1

Bin 419: 0 of cap free
Amount of items: 3
Items: 
Size: 739889 Color: 1
Size: 137840 Color: 1
Size: 122272 Color: 0

Bin 420: 0 of cap free
Amount of items: 3
Items: 
Size: 742685 Color: 1
Size: 134060 Color: 1
Size: 123256 Color: 0

Bin 421: 0 of cap free
Amount of items: 3
Items: 
Size: 742976 Color: 1
Size: 154822 Color: 0
Size: 102203 Color: 0

Bin 422: 0 of cap free
Amount of items: 3
Items: 
Size: 743018 Color: 1
Size: 137836 Color: 0
Size: 119147 Color: 1

Bin 423: 0 of cap free
Amount of items: 3
Items: 
Size: 743109 Color: 0
Size: 148328 Color: 1
Size: 108564 Color: 0

Bin 424: 0 of cap free
Amount of items: 3
Items: 
Size: 743036 Color: 1
Size: 130681 Color: 1
Size: 126284 Color: 0

Bin 425: 0 of cap free
Amount of items: 3
Items: 
Size: 743277 Color: 0
Size: 150871 Color: 0
Size: 105853 Color: 1

Bin 426: 0 of cap free
Amount of items: 3
Items: 
Size: 743416 Color: 1
Size: 146079 Color: 0
Size: 110506 Color: 1

Bin 427: 0 of cap free
Amount of items: 3
Items: 
Size: 743627 Color: 1
Size: 152359 Color: 0
Size: 104015 Color: 1

Bin 428: 0 of cap free
Amount of items: 3
Items: 
Size: 743950 Color: 1
Size: 134436 Color: 0
Size: 121615 Color: 1

Bin 429: 0 of cap free
Amount of items: 3
Items: 
Size: 743987 Color: 1
Size: 128009 Color: 0
Size: 128005 Color: 0

Bin 430: 0 of cap free
Amount of items: 3
Items: 
Size: 744144 Color: 1
Size: 129926 Color: 0
Size: 125931 Color: 1

Bin 431: 0 of cap free
Amount of items: 3
Items: 
Size: 747468 Color: 0
Size: 136766 Color: 1
Size: 115767 Color: 1

Bin 432: 0 of cap free
Amount of items: 3
Items: 
Size: 749524 Color: 0
Size: 141757 Color: 0
Size: 108720 Color: 1

Bin 433: 0 of cap free
Amount of items: 3
Items: 
Size: 750410 Color: 0
Size: 132376 Color: 1
Size: 117215 Color: 1

Bin 434: 0 of cap free
Amount of items: 3
Items: 
Size: 751131 Color: 0
Size: 136175 Color: 1
Size: 112695 Color: 0

Bin 435: 0 of cap free
Amount of items: 3
Items: 
Size: 751314 Color: 1
Size: 135163 Color: 1
Size: 113524 Color: 0

Bin 436: 0 of cap free
Amount of items: 2
Items: 
Size: 752508 Color: 1
Size: 247493 Color: 0

Bin 437: 0 of cap free
Amount of items: 3
Items: 
Size: 752496 Color: 0
Size: 140502 Color: 0
Size: 107003 Color: 1

Bin 438: 0 of cap free
Amount of items: 3
Items: 
Size: 752519 Color: 1
Size: 127091 Color: 0
Size: 120391 Color: 1

Bin 439: 0 of cap free
Amount of items: 3
Items: 
Size: 753440 Color: 0
Size: 133472 Color: 1
Size: 113089 Color: 0

Bin 440: 0 of cap free
Amount of items: 3
Items: 
Size: 753693 Color: 0
Size: 134661 Color: 1
Size: 111647 Color: 1

Bin 441: 0 of cap free
Amount of items: 2
Items: 
Size: 758063 Color: 0
Size: 241938 Color: 1

Bin 442: 0 of cap free
Amount of items: 3
Items: 
Size: 759362 Color: 0
Size: 123791 Color: 1
Size: 116848 Color: 0

Bin 443: 0 of cap free
Amount of items: 3
Items: 
Size: 759739 Color: 1
Size: 122388 Color: 1
Size: 117874 Color: 0

Bin 444: 0 of cap free
Amount of items: 3
Items: 
Size: 759900 Color: 0
Size: 134873 Color: 0
Size: 105228 Color: 1

Bin 445: 0 of cap free
Amount of items: 3
Items: 
Size: 759941 Color: 1
Size: 127512 Color: 1
Size: 112548 Color: 0

Bin 446: 0 of cap free
Amount of items: 3
Items: 
Size: 760284 Color: 0
Size: 129572 Color: 1
Size: 110145 Color: 0

Bin 447: 0 of cap free
Amount of items: 3
Items: 
Size: 760316 Color: 0
Size: 132838 Color: 1
Size: 106847 Color: 1

Bin 448: 0 of cap free
Amount of items: 3
Items: 
Size: 761461 Color: 1
Size: 135359 Color: 1
Size: 103181 Color: 0

Bin 449: 0 of cap free
Amount of items: 3
Items: 
Size: 764094 Color: 1
Size: 131494 Color: 1
Size: 104413 Color: 0

Bin 450: 0 of cap free
Amount of items: 2
Items: 
Size: 764499 Color: 1
Size: 235502 Color: 0

Bin 451: 0 of cap free
Amount of items: 3
Items: 
Size: 767523 Color: 1
Size: 122804 Color: 0
Size: 109674 Color: 1

Bin 452: 0 of cap free
Amount of items: 3
Items: 
Size: 768586 Color: 1
Size: 125433 Color: 1
Size: 105982 Color: 0

Bin 453: 0 of cap free
Amount of items: 3
Items: 
Size: 768612 Color: 1
Size: 119241 Color: 0
Size: 112148 Color: 0

Bin 454: 0 of cap free
Amount of items: 3
Items: 
Size: 768629 Color: 1
Size: 121772 Color: 1
Size: 109600 Color: 0

Bin 455: 0 of cap free
Amount of items: 3
Items: 
Size: 768868 Color: 0
Size: 117623 Color: 1
Size: 113510 Color: 0

Bin 456: 0 of cap free
Amount of items: 3
Items: 
Size: 768826 Color: 1
Size: 115798 Color: 0
Size: 115377 Color: 1

Bin 457: 0 of cap free
Amount of items: 3
Items: 
Size: 770318 Color: 1
Size: 117975 Color: 0
Size: 111708 Color: 1

Bin 458: 0 of cap free
Amount of items: 3
Items: 
Size: 771686 Color: 1
Size: 121212 Color: 1
Size: 107103 Color: 0

Bin 459: 0 of cap free
Amount of items: 3
Items: 
Size: 773907 Color: 1
Size: 120476 Color: 1
Size: 105618 Color: 0

Bin 460: 0 of cap free
Amount of items: 3
Items: 
Size: 785761 Color: 1
Size: 109050 Color: 1
Size: 105190 Color: 0

Bin 461: 0 of cap free
Amount of items: 3
Items: 
Size: 786170 Color: 0
Size: 107776 Color: 1
Size: 106055 Color: 0

Bin 462: 0 of cap free
Amount of items: 3
Items: 
Size: 787371 Color: 1
Size: 111052 Color: 1
Size: 101578 Color: 0

Bin 463: 0 of cap free
Amount of items: 3
Items: 
Size: 792932 Color: 0
Size: 105610 Color: 1
Size: 101459 Color: 0

Bin 464: 1 of cap free
Amount of items: 3
Items: 
Size: 791555 Color: 0
Size: 104241 Color: 1
Size: 104204 Color: 0

Bin 465: 1 of cap free
Amount of items: 3
Items: 
Size: 600090 Color: 1
Size: 288746 Color: 0
Size: 111164 Color: 1

Bin 466: 1 of cap free
Amount of items: 2
Items: 
Size: 515757 Color: 0
Size: 484243 Color: 1

Bin 467: 1 of cap free
Amount of items: 2
Items: 
Size: 658662 Color: 1
Size: 341338 Color: 0

Bin 468: 1 of cap free
Amount of items: 7
Items: 
Size: 147525 Color: 1
Size: 147522 Color: 1
Size: 147027 Color: 0
Size: 146997 Color: 0
Size: 146945 Color: 0
Size: 138611 Color: 0
Size: 125373 Color: 1

Bin 469: 1 of cap free
Amount of items: 7
Items: 
Size: 149759 Color: 0
Size: 149029 Color: 1
Size: 148792 Color: 1
Size: 148701 Color: 1
Size: 148251 Color: 0
Size: 132875 Color: 0
Size: 122593 Color: 1

Bin 470: 1 of cap free
Amount of items: 7
Items: 
Size: 153826 Color: 1
Size: 153458 Color: 1
Size: 151472 Color: 0
Size: 151043 Color: 0
Size: 151037 Color: 0
Size: 132274 Color: 0
Size: 106890 Color: 1

Bin 471: 1 of cap free
Amount of items: 7
Items: 
Size: 155228 Color: 1
Size: 155221 Color: 1
Size: 155075 Color: 1
Size: 154477 Color: 0
Size: 154192 Color: 0
Size: 119398 Color: 0
Size: 106409 Color: 1

Bin 472: 1 of cap free
Amount of items: 3
Items: 
Size: 359431 Color: 0
Size: 336969 Color: 1
Size: 303600 Color: 0

Bin 473: 1 of cap free
Amount of items: 3
Items: 
Size: 407238 Color: 1
Size: 305407 Color: 1
Size: 287355 Color: 0

Bin 474: 1 of cap free
Amount of items: 3
Items: 
Size: 414322 Color: 1
Size: 401155 Color: 1
Size: 184523 Color: 0

Bin 475: 1 of cap free
Amount of items: 3
Items: 
Size: 434455 Color: 1
Size: 361527 Color: 1
Size: 204018 Color: 0

Bin 476: 1 of cap free
Amount of items: 3
Items: 
Size: 434787 Color: 0
Size: 413225 Color: 0
Size: 151988 Color: 1

Bin 477: 1 of cap free
Amount of items: 3
Items: 
Size: 436704 Color: 1
Size: 430203 Color: 0
Size: 133093 Color: 1

Bin 478: 1 of cap free
Amount of items: 3
Items: 
Size: 437279 Color: 1
Size: 436482 Color: 1
Size: 126239 Color: 0

Bin 479: 1 of cap free
Amount of items: 3
Items: 
Size: 439510 Color: 1
Size: 436651 Color: 1
Size: 123839 Color: 0

Bin 480: 1 of cap free
Amount of items: 3
Items: 
Size: 440825 Color: 1
Size: 434548 Color: 1
Size: 124627 Color: 0

Bin 481: 1 of cap free
Amount of items: 3
Items: 
Size: 441348 Color: 1
Size: 434985 Color: 0
Size: 123667 Color: 1

Bin 482: 1 of cap free
Amount of items: 3
Items: 
Size: 443825 Color: 0
Size: 433955 Color: 1
Size: 122220 Color: 0

Bin 483: 1 of cap free
Amount of items: 3
Items: 
Size: 443876 Color: 0
Size: 366633 Color: 1
Size: 189491 Color: 1

Bin 484: 1 of cap free
Amount of items: 3
Items: 
Size: 446546 Color: 0
Size: 391304 Color: 0
Size: 162150 Color: 1

Bin 485: 1 of cap free
Amount of items: 3
Items: 
Size: 447541 Color: 0
Size: 369711 Color: 1
Size: 182748 Color: 1

Bin 486: 1 of cap free
Amount of items: 3
Items: 
Size: 450255 Color: 1
Size: 419122 Color: 0
Size: 130623 Color: 1

Bin 487: 1 of cap free
Amount of items: 3
Items: 
Size: 451243 Color: 1
Size: 432782 Color: 0
Size: 115975 Color: 0

Bin 488: 1 of cap free
Amount of items: 3
Items: 
Size: 451334 Color: 1
Size: 437028 Color: 1
Size: 111638 Color: 0

Bin 489: 1 of cap free
Amount of items: 3
Items: 
Size: 458775 Color: 1
Size: 440048 Color: 1
Size: 101177 Color: 0

Bin 490: 1 of cap free
Amount of items: 3
Items: 
Size: 469263 Color: 1
Size: 344142 Color: 0
Size: 186595 Color: 1

Bin 491: 1 of cap free
Amount of items: 3
Items: 
Size: 469558 Color: 1
Size: 347130 Color: 0
Size: 183312 Color: 1

Bin 492: 1 of cap free
Amount of items: 3
Items: 
Size: 469782 Color: 1
Size: 355083 Color: 1
Size: 175135 Color: 0

Bin 493: 1 of cap free
Amount of items: 3
Items: 
Size: 471723 Color: 1
Size: 378916 Color: 0
Size: 149361 Color: 1

Bin 494: 1 of cap free
Amount of items: 3
Items: 
Size: 478494 Color: 0
Size: 397647 Color: 1
Size: 123859 Color: 0

Bin 495: 1 of cap free
Amount of items: 3
Items: 
Size: 478709 Color: 0
Size: 398973 Color: 1
Size: 122318 Color: 1

Bin 496: 1 of cap free
Amount of items: 3
Items: 
Size: 487208 Color: 1
Size: 326931 Color: 0
Size: 185861 Color: 0

Bin 497: 1 of cap free
Amount of items: 3
Items: 
Size: 487426 Color: 1
Size: 374652 Color: 0
Size: 137922 Color: 0

Bin 498: 1 of cap free
Amount of items: 3
Items: 
Size: 497352 Color: 0
Size: 375346 Color: 1
Size: 127302 Color: 1

Bin 499: 1 of cap free
Amount of items: 2
Items: 
Size: 501526 Color: 0
Size: 498474 Color: 1

Bin 500: 1 of cap free
Amount of items: 3
Items: 
Size: 501898 Color: 1
Size: 331732 Color: 0
Size: 166370 Color: 1

Bin 501: 1 of cap free
Amount of items: 3
Items: 
Size: 501901 Color: 1
Size: 375095 Color: 1
Size: 123004 Color: 0

Bin 502: 1 of cap free
Amount of items: 3
Items: 
Size: 502488 Color: 0
Size: 368092 Color: 1
Size: 129420 Color: 0

Bin 503: 1 of cap free
Amount of items: 3
Items: 
Size: 502584 Color: 0
Size: 315746 Color: 1
Size: 181670 Color: 0

Bin 504: 1 of cap free
Amount of items: 3
Items: 
Size: 502909 Color: 1
Size: 366499 Color: 1
Size: 130592 Color: 0

Bin 505: 1 of cap free
Amount of items: 3
Items: 
Size: 503288 Color: 1
Size: 378871 Color: 0
Size: 117841 Color: 1

Bin 506: 1 of cap free
Amount of items: 3
Items: 
Size: 503651 Color: 0
Size: 334563 Color: 0
Size: 161786 Color: 1

Bin 507: 1 of cap free
Amount of items: 3
Items: 
Size: 503672 Color: 0
Size: 359589 Color: 1
Size: 136739 Color: 0

Bin 508: 1 of cap free
Amount of items: 3
Items: 
Size: 509733 Color: 1
Size: 300903 Color: 1
Size: 189364 Color: 0

Bin 509: 1 of cap free
Amount of items: 3
Items: 
Size: 511733 Color: 1
Size: 348013 Color: 0
Size: 140254 Color: 1

Bin 510: 1 of cap free
Amount of items: 3
Items: 
Size: 511825 Color: 1
Size: 316474 Color: 1
Size: 171701 Color: 0

Bin 511: 1 of cap free
Amount of items: 3
Items: 
Size: 512872 Color: 1
Size: 323717 Color: 0
Size: 163411 Color: 1

Bin 512: 1 of cap free
Amount of items: 2
Items: 
Size: 513392 Color: 1
Size: 486608 Color: 0

Bin 513: 1 of cap free
Amount of items: 3
Items: 
Size: 514411 Color: 0
Size: 296562 Color: 1
Size: 189027 Color: 1

Bin 514: 1 of cap free
Amount of items: 2
Items: 
Size: 515545 Color: 0
Size: 484455 Color: 1

Bin 515: 1 of cap free
Amount of items: 3
Items: 
Size: 517356 Color: 0
Size: 294438 Color: 1
Size: 188206 Color: 1

Bin 516: 1 of cap free
Amount of items: 2
Items: 
Size: 519496 Color: 1
Size: 480504 Color: 0

Bin 517: 1 of cap free
Amount of items: 2
Items: 
Size: 520273 Color: 0
Size: 479727 Color: 1

Bin 518: 1 of cap free
Amount of items: 2
Items: 
Size: 520313 Color: 1
Size: 479687 Color: 0

Bin 519: 1 of cap free
Amount of items: 3
Items: 
Size: 521884 Color: 1
Size: 332020 Color: 0
Size: 146096 Color: 1

Bin 520: 1 of cap free
Amount of items: 3
Items: 
Size: 533359 Color: 1
Size: 305190 Color: 1
Size: 161451 Color: 0

Bin 521: 1 of cap free
Amount of items: 3
Items: 
Size: 537237 Color: 1
Size: 305780 Color: 1
Size: 156983 Color: 0

Bin 522: 1 of cap free
Amount of items: 3
Items: 
Size: 537356 Color: 1
Size: 346619 Color: 0
Size: 116025 Color: 0

Bin 523: 1 of cap free
Amount of items: 2
Items: 
Size: 538647 Color: 0
Size: 461353 Color: 1

Bin 524: 1 of cap free
Amount of items: 3
Items: 
Size: 538921 Color: 1
Size: 334087 Color: 0
Size: 126992 Color: 0

Bin 525: 1 of cap free
Amount of items: 3
Items: 
Size: 542110 Color: 1
Size: 338671 Color: 1
Size: 119219 Color: 0

Bin 526: 1 of cap free
Amount of items: 3
Items: 
Size: 546668 Color: 1
Size: 271485 Color: 0
Size: 181847 Color: 1

Bin 527: 1 of cap free
Amount of items: 3
Items: 
Size: 554818 Color: 1
Size: 280474 Color: 0
Size: 164708 Color: 1

Bin 528: 1 of cap free
Amount of items: 2
Items: 
Size: 559345 Color: 1
Size: 440655 Color: 0

Bin 529: 1 of cap free
Amount of items: 3
Items: 
Size: 565081 Color: 1
Size: 226562 Color: 0
Size: 208357 Color: 1

Bin 530: 1 of cap free
Amount of items: 3
Items: 
Size: 569938 Color: 1
Size: 255748 Color: 1
Size: 174314 Color: 0

Bin 531: 1 of cap free
Amount of items: 3
Items: 
Size: 571257 Color: 1
Size: 302708 Color: 1
Size: 126035 Color: 0

Bin 532: 1 of cap free
Amount of items: 3
Items: 
Size: 573988 Color: 0
Size: 230023 Color: 1
Size: 195989 Color: 0

Bin 533: 1 of cap free
Amount of items: 3
Items: 
Size: 575192 Color: 0
Size: 293874 Color: 1
Size: 130934 Color: 0

Bin 534: 1 of cap free
Amount of items: 3
Items: 
Size: 576381 Color: 1
Size: 286753 Color: 0
Size: 136866 Color: 1

Bin 535: 1 of cap free
Amount of items: 3
Items: 
Size: 580276 Color: 0
Size: 314577 Color: 0
Size: 105147 Color: 1

Bin 536: 1 of cap free
Amount of items: 3
Items: 
Size: 591961 Color: 1
Size: 264415 Color: 1
Size: 143624 Color: 0

Bin 537: 1 of cap free
Amount of items: 3
Items: 
Size: 601115 Color: 1
Size: 217836 Color: 1
Size: 181049 Color: 0

Bin 538: 1 of cap free
Amount of items: 3
Items: 
Size: 602946 Color: 0
Size: 213967 Color: 1
Size: 183087 Color: 1

Bin 539: 1 of cap free
Amount of items: 2
Items: 
Size: 603028 Color: 1
Size: 396972 Color: 0

Bin 540: 1 of cap free
Amount of items: 3
Items: 
Size: 607375 Color: 0
Size: 230479 Color: 1
Size: 162146 Color: 0

Bin 541: 1 of cap free
Amount of items: 3
Items: 
Size: 607718 Color: 1
Size: 226097 Color: 1
Size: 166185 Color: 0

Bin 542: 1 of cap free
Amount of items: 3
Items: 
Size: 607834 Color: 0
Size: 217322 Color: 1
Size: 174844 Color: 1

Bin 543: 1 of cap free
Amount of items: 2
Items: 
Size: 607884 Color: 0
Size: 392116 Color: 1

Bin 544: 1 of cap free
Amount of items: 3
Items: 
Size: 611108 Color: 1
Size: 263850 Color: 1
Size: 125042 Color: 0

Bin 545: 1 of cap free
Amount of items: 3
Items: 
Size: 615741 Color: 0
Size: 255533 Color: 1
Size: 128726 Color: 1

Bin 546: 1 of cap free
Amount of items: 3
Items: 
Size: 619517 Color: 0
Size: 226067 Color: 1
Size: 154416 Color: 1

Bin 547: 1 of cap free
Amount of items: 2
Items: 
Size: 624128 Color: 0
Size: 375872 Color: 1

Bin 548: 1 of cap free
Amount of items: 3
Items: 
Size: 630034 Color: 1
Size: 196829 Color: 0
Size: 173137 Color: 1

Bin 549: 1 of cap free
Amount of items: 3
Items: 
Size: 630329 Color: 1
Size: 188866 Color: 0
Size: 180805 Color: 0

Bin 550: 1 of cap free
Amount of items: 3
Items: 
Size: 630371 Color: 1
Size: 189769 Color: 0
Size: 179860 Color: 1

Bin 551: 1 of cap free
Amount of items: 3
Items: 
Size: 630810 Color: 1
Size: 262776 Color: 1
Size: 106414 Color: 0

Bin 552: 1 of cap free
Amount of items: 3
Items: 
Size: 631612 Color: 0
Size: 207472 Color: 1
Size: 160916 Color: 1

Bin 553: 1 of cap free
Amount of items: 3
Items: 
Size: 632110 Color: 1
Size: 255593 Color: 1
Size: 112297 Color: 0

Bin 554: 1 of cap free
Amount of items: 3
Items: 
Size: 640667 Color: 0
Size: 181015 Color: 1
Size: 178318 Color: 1

Bin 555: 1 of cap free
Amount of items: 3
Items: 
Size: 643790 Color: 1
Size: 232464 Color: 0
Size: 123746 Color: 0

Bin 556: 1 of cap free
Amount of items: 3
Items: 
Size: 650653 Color: 0
Size: 183049 Color: 1
Size: 166298 Color: 1

Bin 557: 1 of cap free
Amount of items: 3
Items: 
Size: 652410 Color: 0
Size: 199298 Color: 1
Size: 148292 Color: 1

Bin 558: 1 of cap free
Amount of items: 2
Items: 
Size: 652845 Color: 1
Size: 347155 Color: 0

Bin 559: 1 of cap free
Amount of items: 3
Items: 
Size: 653398 Color: 0
Size: 179966 Color: 1
Size: 166636 Color: 1

Bin 560: 1 of cap free
Amount of items: 3
Items: 
Size: 658008 Color: 1
Size: 222143 Color: 0
Size: 119849 Color: 0

Bin 561: 1 of cap free
Amount of items: 3
Items: 
Size: 659785 Color: 0
Size: 217478 Color: 1
Size: 122737 Color: 1

Bin 562: 1 of cap free
Amount of items: 2
Items: 
Size: 668612 Color: 0
Size: 331388 Color: 1

Bin 563: 1 of cap free
Amount of items: 2
Items: 
Size: 677714 Color: 0
Size: 322286 Color: 1

Bin 564: 1 of cap free
Amount of items: 3
Items: 
Size: 680204 Color: 1
Size: 212908 Color: 1
Size: 106888 Color: 0

Bin 565: 1 of cap free
Amount of items: 3
Items: 
Size: 696717 Color: 1
Size: 155481 Color: 0
Size: 147802 Color: 0

Bin 566: 1 of cap free
Amount of items: 3
Items: 
Size: 696760 Color: 1
Size: 157053 Color: 0
Size: 146187 Color: 1

Bin 567: 1 of cap free
Amount of items: 2
Items: 
Size: 697734 Color: 1
Size: 302266 Color: 0

Bin 568: 1 of cap free
Amount of items: 3
Items: 
Size: 706550 Color: 1
Size: 150553 Color: 0
Size: 142897 Color: 1

Bin 569: 1 of cap free
Amount of items: 2
Items: 
Size: 717112 Color: 1
Size: 282888 Color: 0

Bin 570: 1 of cap free
Amount of items: 3
Items: 
Size: 718268 Color: 0
Size: 146891 Color: 0
Size: 134841 Color: 1

Bin 571: 1 of cap free
Amount of items: 3
Items: 
Size: 720403 Color: 0
Size: 162974 Color: 1
Size: 116623 Color: 1

Bin 572: 1 of cap free
Amount of items: 2
Items: 
Size: 720535 Color: 1
Size: 279465 Color: 0

Bin 573: 1 of cap free
Amount of items: 3
Items: 
Size: 729265 Color: 1
Size: 149146 Color: 0
Size: 121589 Color: 0

Bin 574: 1 of cap free
Amount of items: 3
Items: 
Size: 730760 Color: 0
Size: 137331 Color: 1
Size: 131909 Color: 1

Bin 575: 1 of cap free
Amount of items: 3
Items: 
Size: 738041 Color: 1
Size: 138711 Color: 0
Size: 123248 Color: 1

Bin 576: 1 of cap free
Amount of items: 3
Items: 
Size: 743184 Color: 0
Size: 134567 Color: 0
Size: 122249 Color: 1

Bin 577: 1 of cap free
Amount of items: 3
Items: 
Size: 743373 Color: 1
Size: 151834 Color: 0
Size: 104793 Color: 1

Bin 578: 1 of cap free
Amount of items: 3
Items: 
Size: 743529 Color: 1
Size: 150910 Color: 0
Size: 105561 Color: 0

Bin 579: 1 of cap free
Amount of items: 3
Items: 
Size: 743657 Color: 1
Size: 133767 Color: 0
Size: 122576 Color: 0

Bin 580: 1 of cap free
Amount of items: 2
Items: 
Size: 748025 Color: 0
Size: 251975 Color: 1

Bin 581: 1 of cap free
Amount of items: 3
Items: 
Size: 749150 Color: 1
Size: 128194 Color: 0
Size: 122656 Color: 1

Bin 582: 1 of cap free
Amount of items: 3
Items: 
Size: 752263 Color: 1
Size: 142906 Color: 1
Size: 104831 Color: 0

Bin 583: 1 of cap free
Amount of items: 3
Items: 
Size: 753465 Color: 1
Size: 138341 Color: 1
Size: 108194 Color: 0

Bin 584: 1 of cap free
Amount of items: 2
Items: 
Size: 766620 Color: 0
Size: 233380 Color: 1

Bin 585: 1 of cap free
Amount of items: 3
Items: 
Size: 767580 Color: 0
Size: 127092 Color: 1
Size: 105328 Color: 0

Bin 586: 1 of cap free
Amount of items: 3
Items: 
Size: 778666 Color: 0
Size: 113387 Color: 1
Size: 107947 Color: 1

Bin 587: 1 of cap free
Amount of items: 2
Items: 
Size: 784401 Color: 1
Size: 215599 Color: 0

Bin 588: 1 of cap free
Amount of items: 3
Items: 
Size: 786054 Color: 0
Size: 110248 Color: 1
Size: 103698 Color: 0

Bin 589: 1 of cap free
Amount of items: 3
Items: 
Size: 786608 Color: 1
Size: 109980 Color: 0
Size: 103412 Color: 0

Bin 590: 1 of cap free
Amount of items: 2
Items: 
Size: 798838 Color: 0
Size: 201162 Color: 1

Bin 591: 1 of cap free
Amount of items: 3
Items: 
Size: 775108 Color: 1
Size: 118803 Color: 0
Size: 106089 Color: 0

Bin 592: 1 of cap free
Amount of items: 3
Items: 
Size: 627197 Color: 0
Size: 262625 Color: 1
Size: 110178 Color: 1

Bin 593: 1 of cap free
Amount of items: 3
Items: 
Size: 543481 Color: 1
Size: 318874 Color: 1
Size: 137645 Color: 0

Bin 594: 1 of cap free
Amount of items: 3
Items: 
Size: 451338 Color: 1
Size: 305819 Color: 1
Size: 242843 Color: 0

Bin 595: 1 of cap free
Amount of items: 3
Items: 
Size: 644152 Color: 1
Size: 253195 Color: 1
Size: 102653 Color: 0

Bin 596: 1 of cap free
Amount of items: 2
Items: 
Size: 586239 Color: 1
Size: 413761 Color: 0

Bin 597: 1 of cap free
Amount of items: 3
Items: 
Size: 794044 Color: 0
Size: 103694 Color: 1
Size: 102262 Color: 0

Bin 598: 1 of cap free
Amount of items: 3
Items: 
Size: 404962 Color: 1
Size: 314544 Color: 0
Size: 280494 Color: 1

Bin 599: 1 of cap free
Amount of items: 3
Items: 
Size: 796078 Color: 1
Size: 103343 Color: 1
Size: 100579 Color: 0

Bin 600: 1 of cap free
Amount of items: 6
Items: 
Size: 173791 Color: 1
Size: 172943 Color: 0
Size: 169172 Color: 1
Size: 169095 Color: 1
Size: 167796 Color: 0
Size: 147203 Color: 0

Bin 601: 1 of cap free
Amount of items: 4
Items: 
Size: 300811 Color: 1
Size: 289959 Color: 1
Size: 237787 Color: 0
Size: 171443 Color: 0

Bin 602: 1 of cap free
Amount of items: 3
Items: 
Size: 761636 Color: 1
Size: 137072 Color: 0
Size: 101292 Color: 0

Bin 603: 1 of cap free
Amount of items: 3
Items: 
Size: 700555 Color: 0
Size: 199388 Color: 0
Size: 100057 Color: 1

Bin 604: 1 of cap free
Amount of items: 3
Items: 
Size: 573069 Color: 0
Size: 320066 Color: 1
Size: 106865 Color: 1

Bin 605: 1 of cap free
Amount of items: 3
Items: 
Size: 755572 Color: 1
Size: 126877 Color: 1
Size: 117551 Color: 0

Bin 606: 1 of cap free
Amount of items: 3
Items: 
Size: 765202 Color: 0
Size: 120033 Color: 0
Size: 114765 Color: 1

Bin 607: 1 of cap free
Amount of items: 3
Items: 
Size: 778497 Color: 0
Size: 114181 Color: 1
Size: 107322 Color: 1

Bin 608: 1 of cap free
Amount of items: 3
Items: 
Size: 752574 Color: 1
Size: 127150 Color: 1
Size: 120276 Color: 0

Bin 609: 1 of cap free
Amount of items: 2
Items: 
Size: 613069 Color: 1
Size: 386931 Color: 0

Bin 610: 1 of cap free
Amount of items: 3
Items: 
Size: 736091 Color: 0
Size: 158795 Color: 1
Size: 105114 Color: 1

Bin 611: 1 of cap free
Amount of items: 3
Items: 
Size: 730156 Color: 0
Size: 137303 Color: 0
Size: 132541 Color: 1

Bin 612: 1 of cap free
Amount of items: 3
Items: 
Size: 739283 Color: 1
Size: 131118 Color: 1
Size: 129599 Color: 0

Bin 613: 1 of cap free
Amount of items: 4
Items: 
Size: 335698 Color: 0
Size: 282071 Color: 0
Size: 233907 Color: 1
Size: 148324 Color: 1

Bin 614: 1 of cap free
Amount of items: 5
Items: 
Size: 256718 Color: 1
Size: 190332 Color: 0
Size: 185911 Color: 0
Size: 185663 Color: 1
Size: 181376 Color: 1

Bin 615: 1 of cap free
Amount of items: 5
Items: 
Size: 259869 Color: 0
Size: 208101 Color: 0
Size: 184673 Color: 1
Size: 184549 Color: 1
Size: 162808 Color: 0

Bin 616: 1 of cap free
Amount of items: 5
Items: 
Size: 269714 Color: 1
Size: 183412 Color: 0
Size: 183402 Color: 0
Size: 181828 Color: 1
Size: 181644 Color: 1

Bin 617: 2 of cap free
Amount of items: 3
Items: 
Size: 781998 Color: 1
Size: 114113 Color: 0
Size: 103888 Color: 1

Bin 618: 2 of cap free
Amount of items: 7
Items: 
Size: 146368 Color: 0
Size: 146267 Color: 0
Size: 145923 Color: 0
Size: 145807 Color: 1
Size: 145370 Color: 1
Size: 136841 Color: 1
Size: 133423 Color: 0

Bin 619: 2 of cap free
Amount of items: 5
Items: 
Size: 232877 Color: 1
Size: 232707 Color: 1
Size: 216597 Color: 0
Size: 174108 Color: 0
Size: 143710 Color: 1

Bin 620: 2 of cap free
Amount of items: 3
Items: 
Size: 392064 Color: 0
Size: 375471 Color: 1
Size: 232464 Color: 0

Bin 621: 2 of cap free
Amount of items: 3
Items: 
Size: 432962 Color: 1
Size: 430197 Color: 1
Size: 136840 Color: 0

Bin 622: 2 of cap free
Amount of items: 3
Items: 
Size: 434332 Color: 1
Size: 408905 Color: 1
Size: 156762 Color: 0

Bin 623: 2 of cap free
Amount of items: 3
Items: 
Size: 434361 Color: 1
Size: 391521 Color: 0
Size: 174117 Color: 0

Bin 624: 2 of cap free
Amount of items: 3
Items: 
Size: 439622 Color: 1
Size: 375801 Color: 0
Size: 184576 Color: 0

Bin 625: 2 of cap free
Amount of items: 3
Items: 
Size: 440082 Color: 1
Size: 375297 Color: 1
Size: 184620 Color: 0

Bin 626: 2 of cap free
Amount of items: 3
Items: 
Size: 441027 Color: 1
Size: 365991 Color: 1
Size: 192981 Color: 0

Bin 627: 2 of cap free
Amount of items: 3
Items: 
Size: 444016 Color: 0
Size: 439682 Color: 1
Size: 116301 Color: 0

Bin 628: 2 of cap free
Amount of items: 3
Items: 
Size: 444445 Color: 0
Size: 360235 Color: 1
Size: 195319 Color: 0

Bin 629: 2 of cap free
Amount of items: 3
Items: 
Size: 447605 Color: 0
Size: 374874 Color: 1
Size: 177520 Color: 1

Bin 630: 2 of cap free
Amount of items: 3
Items: 
Size: 448202 Color: 0
Size: 443827 Color: 0
Size: 107970 Color: 1

Bin 631: 2 of cap free
Amount of items: 3
Items: 
Size: 448841 Color: 1
Size: 361361 Color: 1
Size: 189797 Color: 0

Bin 632: 2 of cap free
Amount of items: 3
Items: 
Size: 453449 Color: 1
Size: 304154 Color: 1
Size: 242396 Color: 0

Bin 633: 2 of cap free
Amount of items: 3
Items: 
Size: 464378 Color: 0
Size: 366177 Color: 1
Size: 169444 Color: 1

Bin 634: 2 of cap free
Amount of items: 3
Items: 
Size: 475027 Color: 1
Size: 368183 Color: 1
Size: 156789 Color: 0

Bin 635: 2 of cap free
Amount of items: 3
Items: 
Size: 478362 Color: 0
Size: 374659 Color: 1
Size: 146978 Color: 0

Bin 636: 2 of cap free
Amount of items: 3
Items: 
Size: 478511 Color: 0
Size: 354979 Color: 1
Size: 166509 Color: 1

Bin 637: 2 of cap free
Amount of items: 3
Items: 
Size: 478518 Color: 0
Size: 329658 Color: 1
Size: 191823 Color: 1

Bin 638: 2 of cap free
Amount of items: 3
Items: 
Size: 497337 Color: 0
Size: 349213 Color: 0
Size: 153449 Color: 1

Bin 639: 2 of cap free
Amount of items: 3
Items: 
Size: 503757 Color: 0
Size: 366685 Color: 1
Size: 129557 Color: 1

Bin 640: 2 of cap free
Amount of items: 2
Items: 
Size: 505449 Color: 0
Size: 494550 Color: 1

Bin 641: 2 of cap free
Amount of items: 2
Items: 
Size: 512052 Color: 1
Size: 487947 Color: 0

Bin 642: 2 of cap free
Amount of items: 3
Items: 
Size: 513187 Color: 0
Size: 306239 Color: 1
Size: 180573 Color: 1

Bin 643: 2 of cap free
Amount of items: 2
Items: 
Size: 519258 Color: 1
Size: 480741 Color: 0

Bin 644: 2 of cap free
Amount of items: 2
Items: 
Size: 521520 Color: 1
Size: 478479 Color: 0

Bin 645: 2 of cap free
Amount of items: 3
Items: 
Size: 535545 Color: 0
Size: 305983 Color: 1
Size: 158471 Color: 1

Bin 646: 2 of cap free
Amount of items: 2
Items: 
Size: 537713 Color: 0
Size: 462286 Color: 1

Bin 647: 2 of cap free
Amount of items: 3
Items: 
Size: 540332 Color: 0
Size: 304868 Color: 1
Size: 154799 Color: 1

Bin 648: 2 of cap free
Amount of items: 3
Items: 
Size: 542052 Color: 0
Size: 331416 Color: 0
Size: 126531 Color: 1

Bin 649: 2 of cap free
Amount of items: 3
Items: 
Size: 543471 Color: 0
Size: 293532 Color: 1
Size: 162996 Color: 0

Bin 650: 2 of cap free
Amount of items: 3
Items: 
Size: 556954 Color: 1
Size: 329385 Color: 1
Size: 113660 Color: 0

Bin 651: 2 of cap free
Amount of items: 2
Items: 
Size: 565284 Color: 0
Size: 434715 Color: 1

Bin 652: 2 of cap free
Amount of items: 3
Items: 
Size: 568366 Color: 0
Size: 296968 Color: 1
Size: 134665 Color: 1

Bin 653: 2 of cap free
Amount of items: 3
Items: 
Size: 570871 Color: 1
Size: 301462 Color: 1
Size: 127666 Color: 0

Bin 654: 2 of cap free
Amount of items: 2
Items: 
Size: 575114 Color: 0
Size: 424885 Color: 1

Bin 655: 2 of cap free
Amount of items: 3
Items: 
Size: 575201 Color: 0
Size: 256061 Color: 1
Size: 168737 Color: 1

Bin 656: 2 of cap free
Amount of items: 3
Items: 
Size: 576431 Color: 0
Size: 249141 Color: 0
Size: 174427 Color: 1

Bin 657: 2 of cap free
Amount of items: 2
Items: 
Size: 579942 Color: 1
Size: 420057 Color: 0

Bin 658: 2 of cap free
Amount of items: 3
Items: 
Size: 580009 Color: 1
Size: 284626 Color: 1
Size: 135364 Color: 0

Bin 659: 2 of cap free
Amount of items: 3
Items: 
Size: 593802 Color: 1
Size: 286775 Color: 0
Size: 119422 Color: 1

Bin 660: 2 of cap free
Amount of items: 2
Items: 
Size: 605539 Color: 0
Size: 394460 Color: 1

Bin 661: 2 of cap free
Amount of items: 2
Items: 
Size: 606980 Color: 0
Size: 393019 Color: 1

Bin 662: 2 of cap free
Amount of items: 3
Items: 
Size: 607923 Color: 0
Size: 228580 Color: 1
Size: 163496 Color: 0

Bin 663: 2 of cap free
Amount of items: 3
Items: 
Size: 609490 Color: 1
Size: 266742 Color: 1
Size: 123767 Color: 0

Bin 664: 2 of cap free
Amount of items: 2
Items: 
Size: 613519 Color: 0
Size: 386480 Color: 1

Bin 665: 2 of cap free
Amount of items: 3
Items: 
Size: 615557 Color: 0
Size: 235133 Color: 1
Size: 149309 Color: 1

Bin 666: 2 of cap free
Amount of items: 2
Items: 
Size: 617562 Color: 1
Size: 382437 Color: 0

Bin 667: 2 of cap free
Amount of items: 2
Items: 
Size: 617706 Color: 1
Size: 382293 Color: 0

Bin 668: 2 of cap free
Amount of items: 3
Items: 
Size: 618052 Color: 1
Size: 223328 Color: 1
Size: 158619 Color: 0

Bin 669: 2 of cap free
Amount of items: 3
Items: 
Size: 620699 Color: 0
Size: 196618 Color: 1
Size: 182682 Color: 1

Bin 670: 2 of cap free
Amount of items: 2
Items: 
Size: 633775 Color: 0
Size: 366224 Color: 1

Bin 671: 2 of cap free
Amount of items: 3
Items: 
Size: 646024 Color: 0
Size: 196756 Color: 1
Size: 157219 Color: 1

Bin 672: 2 of cap free
Amount of items: 2
Items: 
Size: 653714 Color: 1
Size: 346285 Color: 0

Bin 673: 2 of cap free
Amount of items: 2
Items: 
Size: 658842 Color: 0
Size: 341157 Color: 1

Bin 674: 2 of cap free
Amount of items: 3
Items: 
Size: 668164 Color: 0
Size: 169121 Color: 1
Size: 162714 Color: 1

Bin 675: 2 of cap free
Amount of items: 2
Items: 
Size: 693406 Color: 1
Size: 306593 Color: 0

Bin 676: 2 of cap free
Amount of items: 2
Items: 
Size: 694881 Color: 0
Size: 305118 Color: 1

Bin 677: 2 of cap free
Amount of items: 2
Items: 
Size: 698946 Color: 1
Size: 301053 Color: 0

Bin 678: 2 of cap free
Amount of items: 3
Items: 
Size: 700991 Color: 0
Size: 176345 Color: 1
Size: 122663 Color: 1

Bin 679: 2 of cap free
Amount of items: 2
Items: 
Size: 705443 Color: 0
Size: 294556 Color: 1

Bin 680: 2 of cap free
Amount of items: 3
Items: 
Size: 729373 Color: 1
Size: 139839 Color: 0
Size: 130787 Color: 0

Bin 681: 2 of cap free
Amount of items: 2
Items: 
Size: 731371 Color: 0
Size: 268628 Color: 1

Bin 682: 2 of cap free
Amount of items: 2
Items: 
Size: 742038 Color: 1
Size: 257961 Color: 0

Bin 683: 2 of cap free
Amount of items: 3
Items: 
Size: 743411 Color: 1
Size: 143619 Color: 1
Size: 112969 Color: 0

Bin 684: 2 of cap free
Amount of items: 3
Items: 
Size: 749436 Color: 0
Size: 133890 Color: 1
Size: 116673 Color: 1

Bin 685: 2 of cap free
Amount of items: 3
Items: 
Size: 756204 Color: 0
Size: 126779 Color: 1
Size: 117016 Color: 1

Bin 686: 2 of cap free
Amount of items: 2
Items: 
Size: 772133 Color: 1
Size: 227866 Color: 0

Bin 687: 2 of cap free
Amount of items: 3
Items: 
Size: 783470 Color: 0
Size: 111024 Color: 1
Size: 105505 Color: 1

Bin 688: 2 of cap free
Amount of items: 3
Items: 
Size: 784004 Color: 1
Size: 111739 Color: 1
Size: 104256 Color: 0

Bin 689: 2 of cap free
Amount of items: 2
Items: 
Size: 785044 Color: 0
Size: 214955 Color: 1

Bin 690: 2 of cap free
Amount of items: 2
Items: 
Size: 788855 Color: 1
Size: 211144 Color: 0

Bin 691: 2 of cap free
Amount of items: 2
Items: 
Size: 791338 Color: 1
Size: 208661 Color: 0

Bin 692: 2 of cap free
Amount of items: 2
Items: 
Size: 796978 Color: 1
Size: 203021 Color: 0

Bin 693: 2 of cap free
Amount of items: 3
Items: 
Size: 751358 Color: 1
Size: 126746 Color: 1
Size: 121895 Color: 0

Bin 694: 2 of cap free
Amount of items: 3
Items: 
Size: 784325 Color: 0
Size: 111245 Color: 1
Size: 104429 Color: 1

Bin 695: 2 of cap free
Amount of items: 3
Items: 
Size: 375401 Color: 1
Size: 320530 Color: 1
Size: 304068 Color: 0

Bin 696: 2 of cap free
Amount of items: 3
Items: 
Size: 723485 Color: 0
Size: 159806 Color: 1
Size: 116708 Color: 0

Bin 697: 2 of cap free
Amount of items: 3
Items: 
Size: 721527 Color: 1
Size: 152636 Color: 1
Size: 125836 Color: 0

Bin 698: 2 of cap free
Amount of items: 3
Items: 
Size: 470038 Color: 0
Size: 414682 Color: 1
Size: 115279 Color: 0

Bin 699: 2 of cap free
Amount of items: 5
Items: 
Size: 207007 Color: 1
Size: 200811 Color: 0
Size: 199765 Color: 1
Size: 196855 Color: 0
Size: 195561 Color: 0

Bin 700: 3 of cap free
Amount of items: 3
Items: 
Size: 558884 Color: 1
Size: 316145 Color: 0
Size: 124969 Color: 1

Bin 701: 3 of cap free
Amount of items: 3
Items: 
Size: 788195 Color: 1
Size: 108670 Color: 0
Size: 103133 Color: 0

Bin 702: 3 of cap free
Amount of items: 5
Items: 
Size: 213959 Color: 1
Size: 213192 Color: 1
Size: 197437 Color: 0
Size: 190732 Color: 0
Size: 184678 Color: 0

Bin 703: 3 of cap free
Amount of items: 5
Items: 
Size: 216661 Color: 1
Size: 216652 Color: 1
Size: 197680 Color: 0
Size: 191857 Color: 1
Size: 177148 Color: 0

Bin 704: 3 of cap free
Amount of items: 5
Items: 
Size: 227714 Color: 1
Size: 226054 Color: 1
Size: 197884 Color: 0
Size: 175243 Color: 0
Size: 173103 Color: 0

Bin 705: 3 of cap free
Amount of items: 3
Items: 
Size: 429574 Color: 1
Size: 409468 Color: 1
Size: 160956 Color: 0

Bin 706: 3 of cap free
Amount of items: 3
Items: 
Size: 434697 Color: 1
Size: 284730 Color: 1
Size: 280571 Color: 0

Bin 707: 3 of cap free
Amount of items: 3
Items: 
Size: 435198 Color: 0
Size: 430373 Color: 1
Size: 134427 Color: 1

Bin 708: 3 of cap free
Amount of items: 3
Items: 
Size: 440932 Color: 1
Size: 378851 Color: 0
Size: 180215 Color: 0

Bin 709: 3 of cap free
Amount of items: 3
Items: 
Size: 449533 Color: 1
Size: 378925 Color: 0
Size: 171540 Color: 0

Bin 710: 3 of cap free
Amount of items: 3
Items: 
Size: 450261 Color: 1
Size: 375450 Color: 0
Size: 174287 Color: 0

Bin 711: 3 of cap free
Amount of items: 3
Items: 
Size: 470126 Color: 1
Size: 412856 Color: 0
Size: 117016 Color: 0

Bin 712: 3 of cap free
Amount of items: 3
Items: 
Size: 476223 Color: 1
Size: 407614 Color: 1
Size: 116161 Color: 0

Bin 713: 3 of cap free
Amount of items: 3
Items: 
Size: 504700 Color: 0
Size: 333600 Color: 1
Size: 161698 Color: 1

Bin 714: 3 of cap free
Amount of items: 2
Items: 
Size: 508136 Color: 1
Size: 491862 Color: 0

Bin 715: 3 of cap free
Amount of items: 3
Items: 
Size: 508358 Color: 1
Size: 330230 Color: 0
Size: 161410 Color: 1

Bin 716: 3 of cap free
Amount of items: 2
Items: 
Size: 512857 Color: 1
Size: 487141 Color: 0

Bin 717: 3 of cap free
Amount of items: 2
Items: 
Size: 521266 Color: 1
Size: 478732 Color: 0

Bin 718: 3 of cap free
Amount of items: 2
Items: 
Size: 545281 Color: 1
Size: 454717 Color: 0

Bin 719: 3 of cap free
Amount of items: 2
Items: 
Size: 546897 Color: 0
Size: 453101 Color: 1

Bin 720: 3 of cap free
Amount of items: 2
Items: 
Size: 559800 Color: 1
Size: 440198 Color: 0

Bin 721: 3 of cap free
Amount of items: 2
Items: 
Size: 561669 Color: 0
Size: 438329 Color: 1

Bin 722: 3 of cap free
Amount of items: 2
Items: 
Size: 570890 Color: 0
Size: 429108 Color: 1

Bin 723: 3 of cap free
Amount of items: 3
Items: 
Size: 572889 Color: 1
Size: 242412 Color: 0
Size: 184697 Color: 1

Bin 724: 3 of cap free
Amount of items: 2
Items: 
Size: 573250 Color: 1
Size: 426748 Color: 0

Bin 725: 3 of cap free
Amount of items: 2
Items: 
Size: 573689 Color: 1
Size: 426309 Color: 0

Bin 726: 3 of cap free
Amount of items: 2
Items: 
Size: 579457 Color: 0
Size: 420541 Color: 1

Bin 727: 3 of cap free
Amount of items: 3
Items: 
Size: 583318 Color: 1
Size: 302428 Color: 1
Size: 114252 Color: 0

Bin 728: 3 of cap free
Amount of items: 2
Items: 
Size: 585514 Color: 1
Size: 414484 Color: 0

Bin 729: 3 of cap free
Amount of items: 3
Items: 
Size: 590355 Color: 0
Size: 233505 Color: 1
Size: 176138 Color: 1

Bin 730: 3 of cap free
Amount of items: 2
Items: 
Size: 599417 Color: 0
Size: 400581 Color: 1

Bin 731: 3 of cap free
Amount of items: 3
Items: 
Size: 602881 Color: 0
Size: 242832 Color: 0
Size: 154285 Color: 1

Bin 732: 3 of cap free
Amount of items: 3
Items: 
Size: 611718 Color: 0
Size: 255948 Color: 1
Size: 132332 Color: 1

Bin 733: 3 of cap free
Amount of items: 2
Items: 
Size: 619312 Color: 1
Size: 380686 Color: 0

Bin 734: 3 of cap free
Amount of items: 2
Items: 
Size: 627844 Color: 0
Size: 372154 Color: 1

Bin 735: 3 of cap free
Amount of items: 3
Items: 
Size: 637759 Color: 1
Size: 249049 Color: 0
Size: 113190 Color: 1

Bin 736: 3 of cap free
Amount of items: 2
Items: 
Size: 632643 Color: 1
Size: 367355 Color: 0

Bin 737: 3 of cap free
Amount of items: 2
Items: 
Size: 636799 Color: 1
Size: 363199 Color: 0

Bin 738: 3 of cap free
Amount of items: 2
Items: 
Size: 636962 Color: 1
Size: 363036 Color: 0

Bin 739: 3 of cap free
Amount of items: 2
Items: 
Size: 644111 Color: 1
Size: 355887 Color: 0

Bin 740: 3 of cap free
Amount of items: 2
Items: 
Size: 652908 Color: 1
Size: 347090 Color: 0

Bin 741: 3 of cap free
Amount of items: 3
Items: 
Size: 656872 Color: 0
Size: 181039 Color: 1
Size: 162087 Color: 1

Bin 742: 3 of cap free
Amount of items: 2
Items: 
Size: 663959 Color: 1
Size: 336039 Color: 0

Bin 743: 3 of cap free
Amount of items: 2
Items: 
Size: 666961 Color: 1
Size: 333037 Color: 0

Bin 744: 3 of cap free
Amount of items: 2
Items: 
Size: 683232 Color: 1
Size: 316766 Color: 0

Bin 745: 3 of cap free
Amount of items: 2
Items: 
Size: 685484 Color: 1
Size: 314514 Color: 0

Bin 746: 3 of cap free
Amount of items: 2
Items: 
Size: 685608 Color: 1
Size: 314390 Color: 0

Bin 747: 3 of cap free
Amount of items: 2
Items: 
Size: 701866 Color: 0
Size: 298132 Color: 1

Bin 748: 3 of cap free
Amount of items: 2
Items: 
Size: 705966 Color: 1
Size: 294032 Color: 0

Bin 749: 3 of cap free
Amount of items: 3
Items: 
Size: 714180 Color: 0
Size: 148887 Color: 1
Size: 136931 Color: 1

Bin 750: 3 of cap free
Amount of items: 2
Items: 
Size: 726996 Color: 0
Size: 273002 Color: 1

Bin 751: 3 of cap free
Amount of items: 2
Items: 
Size: 746033 Color: 1
Size: 253965 Color: 0

Bin 752: 3 of cap free
Amount of items: 2
Items: 
Size: 752940 Color: 0
Size: 247058 Color: 1

Bin 753: 3 of cap free
Amount of items: 3
Items: 
Size: 768296 Color: 0
Size: 118693 Color: 1
Size: 113009 Color: 1

Bin 754: 3 of cap free
Amount of items: 2
Items: 
Size: 772604 Color: 0
Size: 227394 Color: 1

Bin 755: 3 of cap free
Amount of items: 3
Items: 
Size: 786476 Color: 1
Size: 108471 Color: 1
Size: 105051 Color: 0

Bin 756: 3 of cap free
Amount of items: 2
Items: 
Size: 791558 Color: 0
Size: 208440 Color: 1

Bin 757: 3 of cap free
Amount of items: 2
Items: 
Size: 791636 Color: 1
Size: 208362 Color: 0

Bin 758: 3 of cap free
Amount of items: 3
Items: 
Size: 794079 Color: 1
Size: 103170 Color: 1
Size: 102749 Color: 0

Bin 759: 3 of cap free
Amount of items: 3
Items: 
Size: 459307 Color: 1
Size: 405712 Color: 1
Size: 134979 Color: 0

Bin 760: 3 of cap free
Amount of items: 5
Items: 
Size: 297221 Color: 1
Size: 272483 Color: 0
Size: 144284 Color: 0
Size: 143891 Color: 1
Size: 142119 Color: 1

Bin 761: 3 of cap free
Amount of items: 7
Items: 
Size: 144168 Color: 0
Size: 143952 Color: 0
Size: 143716 Color: 0
Size: 143504 Color: 1
Size: 143386 Color: 1
Size: 140777 Color: 0
Size: 140495 Color: 1

Bin 762: 3 of cap free
Amount of items: 5
Items: 
Size: 273052 Color: 1
Size: 269872 Color: 1
Size: 236365 Color: 0
Size: 117387 Color: 0
Size: 103322 Color: 0

Bin 763: 3 of cap free
Amount of items: 2
Items: 
Size: 710542 Color: 0
Size: 289456 Color: 1

Bin 764: 3 of cap free
Amount of items: 2
Items: 
Size: 507560 Color: 0
Size: 492438 Color: 1

Bin 765: 3 of cap free
Amount of items: 3
Items: 
Size: 705347 Color: 1
Size: 188724 Color: 0
Size: 105927 Color: 1

Bin 766: 3 of cap free
Amount of items: 3
Items: 
Size: 412056 Color: 0
Size: 311196 Color: 1
Size: 276746 Color: 0

Bin 767: 4 of cap free
Amount of items: 7
Items: 
Size: 145203 Color: 0
Size: 145172 Color: 0
Size: 144706 Color: 1
Size: 144267 Color: 1
Size: 144145 Color: 1
Size: 142993 Color: 1
Size: 133511 Color: 0

Bin 768: 4 of cap free
Amount of items: 5
Items: 
Size: 228958 Color: 1
Size: 228781 Color: 1
Size: 203930 Color: 0
Size: 170674 Color: 0
Size: 167654 Color: 0

Bin 769: 4 of cap free
Amount of items: 3
Items: 
Size: 340419 Color: 1
Size: 330489 Color: 0
Size: 329089 Color: 1

Bin 770: 4 of cap free
Amount of items: 3
Items: 
Size: 412674 Color: 0
Size: 369681 Color: 1
Size: 217642 Color: 1

Bin 771: 4 of cap free
Amount of items: 3
Items: 
Size: 436621 Color: 1
Size: 346851 Color: 0
Size: 216525 Color: 0

Bin 772: 4 of cap free
Amount of items: 3
Items: 
Size: 451297 Color: 1
Size: 413813 Color: 0
Size: 134887 Color: 0

Bin 773: 4 of cap free
Amount of items: 3
Items: 
Size: 476106 Color: 1
Size: 331638 Color: 0
Size: 192253 Color: 0

Bin 774: 4 of cap free
Amount of items: 3
Items: 
Size: 476773 Color: 1
Size: 358028 Color: 0
Size: 165196 Color: 0

Bin 775: 4 of cap free
Amount of items: 3
Items: 
Size: 478349 Color: 0
Size: 301797 Color: 1
Size: 219851 Color: 1

Bin 776: 4 of cap free
Amount of items: 3
Items: 
Size: 487860 Color: 1
Size: 344948 Color: 0
Size: 167189 Color: 0

Bin 777: 4 of cap free
Amount of items: 3
Items: 
Size: 527159 Color: 0
Size: 311208 Color: 1
Size: 161630 Color: 1

Bin 778: 4 of cap free
Amount of items: 3
Items: 
Size: 532021 Color: 1
Size: 335099 Color: 0
Size: 132877 Color: 1

Bin 779: 4 of cap free
Amount of items: 3
Items: 
Size: 538511 Color: 1
Size: 304933 Color: 1
Size: 156553 Color: 0

Bin 780: 4 of cap free
Amount of items: 2
Items: 
Size: 554238 Color: 0
Size: 445759 Color: 1

Bin 781: 4 of cap free
Amount of items: 3
Items: 
Size: 573993 Color: 0
Size: 240664 Color: 1
Size: 185340 Color: 1

Bin 782: 4 of cap free
Amount of items: 2
Items: 
Size: 599632 Color: 0
Size: 400365 Color: 1

Bin 783: 4 of cap free
Amount of items: 3
Items: 
Size: 599763 Color: 1
Size: 213581 Color: 1
Size: 186653 Color: 0

Bin 784: 4 of cap free
Amount of items: 3
Items: 
Size: 601280 Color: 1
Size: 290329 Color: 1
Size: 108388 Color: 0

Bin 785: 4 of cap free
Amount of items: 3
Items: 
Size: 602424 Color: 1
Size: 233130 Color: 0
Size: 164443 Color: 1

Bin 786: 4 of cap free
Amount of items: 2
Items: 
Size: 607506 Color: 0
Size: 392491 Color: 1

Bin 787: 4 of cap free
Amount of items: 3
Items: 
Size: 609534 Color: 1
Size: 237892 Color: 0
Size: 152571 Color: 0

Bin 788: 4 of cap free
Amount of items: 2
Items: 
Size: 624003 Color: 1
Size: 375994 Color: 0

Bin 789: 4 of cap free
Amount of items: 3
Items: 
Size: 632131 Color: 1
Size: 197059 Color: 0
Size: 170807 Color: 0

Bin 790: 4 of cap free
Amount of items: 2
Items: 
Size: 634532 Color: 0
Size: 365465 Color: 1

Bin 791: 4 of cap free
Amount of items: 2
Items: 
Size: 664893 Color: 0
Size: 335104 Color: 1

Bin 792: 4 of cap free
Amount of items: 2
Items: 
Size: 684802 Color: 0
Size: 315195 Color: 1

Bin 793: 4 of cap free
Amount of items: 2
Items: 
Size: 689198 Color: 1
Size: 310799 Color: 0

Bin 794: 4 of cap free
Amount of items: 2
Items: 
Size: 711766 Color: 1
Size: 288231 Color: 0

Bin 795: 4 of cap free
Amount of items: 2
Items: 
Size: 721388 Color: 0
Size: 278609 Color: 1

Bin 796: 4 of cap free
Amount of items: 2
Items: 
Size: 722221 Color: 0
Size: 277776 Color: 1

Bin 797: 4 of cap free
Amount of items: 2
Items: 
Size: 729626 Color: 0
Size: 270371 Color: 1

Bin 798: 4 of cap free
Amount of items: 3
Items: 
Size: 732081 Color: 0
Size: 138753 Color: 1
Size: 129163 Color: 1

Bin 799: 4 of cap free
Amount of items: 2
Items: 
Size: 735946 Color: 0
Size: 264051 Color: 1

Bin 800: 4 of cap free
Amount of items: 2
Items: 
Size: 738268 Color: 1
Size: 261729 Color: 0

Bin 801: 4 of cap free
Amount of items: 2
Items: 
Size: 749958 Color: 1
Size: 250039 Color: 0

Bin 802: 4 of cap free
Amount of items: 2
Items: 
Size: 761557 Color: 1
Size: 238440 Color: 0

Bin 803: 4 of cap free
Amount of items: 2
Items: 
Size: 764174 Color: 0
Size: 235823 Color: 1

Bin 804: 4 of cap free
Amount of items: 2
Items: 
Size: 764261 Color: 1
Size: 235736 Color: 0

Bin 805: 4 of cap free
Amount of items: 3
Items: 
Size: 768443 Color: 1
Size: 120801 Color: 0
Size: 110753 Color: 1

Bin 806: 4 of cap free
Amount of items: 2
Items: 
Size: 778234 Color: 0
Size: 221763 Color: 1

Bin 807: 4 of cap free
Amount of items: 3
Items: 
Size: 446679 Color: 0
Size: 374928 Color: 1
Size: 178390 Color: 1

Bin 808: 4 of cap free
Amount of items: 3
Items: 
Size: 434869 Color: 0
Size: 430204 Color: 1
Size: 134924 Color: 0

Bin 809: 4 of cap free
Amount of items: 5
Items: 
Size: 341486 Color: 1
Size: 179311 Color: 1
Size: 179109 Color: 0
Size: 178899 Color: 0
Size: 121192 Color: 0

Bin 810: 4 of cap free
Amount of items: 2
Items: 
Size: 769974 Color: 1
Size: 230023 Color: 0

Bin 811: 4 of cap free
Amount of items: 3
Items: 
Size: 368133 Color: 1
Size: 316600 Color: 1
Size: 315264 Color: 0

Bin 812: 4 of cap free
Amount of items: 3
Items: 
Size: 678513 Color: 0
Size: 220428 Color: 0
Size: 101056 Color: 1

Bin 813: 4 of cap free
Amount of items: 2
Items: 
Size: 588467 Color: 1
Size: 411530 Color: 0

Bin 814: 4 of cap free
Amount of items: 2
Items: 
Size: 794466 Color: 0
Size: 205531 Color: 1

Bin 815: 4 of cap free
Amount of items: 2
Items: 
Size: 513424 Color: 1
Size: 486573 Color: 0

Bin 816: 4 of cap free
Amount of items: 2
Items: 
Size: 673587 Color: 0
Size: 326410 Color: 1

Bin 817: 4 of cap free
Amount of items: 2
Items: 
Size: 652771 Color: 0
Size: 347226 Color: 1

Bin 818: 4 of cap free
Amount of items: 2
Items: 
Size: 724025 Color: 1
Size: 275972 Color: 0

Bin 819: 4 of cap free
Amount of items: 5
Items: 
Size: 224638 Color: 0
Size: 196095 Color: 1
Size: 196073 Color: 1
Size: 191650 Color: 0
Size: 191541 Color: 0

Bin 820: 4 of cap free
Amount of items: 7
Items: 
Size: 207485 Color: 0
Size: 132864 Color: 0
Size: 132828 Color: 0
Size: 132668 Color: 0
Size: 131709 Color: 1
Size: 131297 Color: 1
Size: 131146 Color: 1

Bin 821: 5 of cap free
Amount of items: 3
Items: 
Size: 691060 Color: 0
Size: 196204 Color: 1
Size: 112732 Color: 0

Bin 822: 5 of cap free
Amount of items: 3
Items: 
Size: 432373 Color: 0
Size: 408785 Color: 1
Size: 158838 Color: 0

Bin 823: 5 of cap free
Amount of items: 3
Items: 
Size: 433164 Color: 0
Size: 426178 Color: 0
Size: 140654 Color: 1

Bin 824: 5 of cap free
Amount of items: 3
Items: 
Size: 434407 Color: 1
Size: 367380 Color: 1
Size: 198209 Color: 0

Bin 825: 5 of cap free
Amount of items: 3
Items: 
Size: 448650 Color: 1
Size: 414075 Color: 1
Size: 137271 Color: 0

Bin 826: 5 of cap free
Amount of items: 3
Items: 
Size: 449563 Color: 1
Size: 407390 Color: 1
Size: 143043 Color: 0

Bin 827: 5 of cap free
Amount of items: 3
Items: 
Size: 461501 Color: 1
Size: 391042 Color: 0
Size: 147453 Color: 0

Bin 828: 5 of cap free
Amount of items: 3
Items: 
Size: 469266 Color: 1
Size: 311771 Color: 0
Size: 218959 Color: 0

Bin 829: 5 of cap free
Amount of items: 3
Items: 
Size: 511711 Color: 1
Size: 349151 Color: 0
Size: 139134 Color: 0

Bin 830: 5 of cap free
Amount of items: 3
Items: 
Size: 511895 Color: 0
Size: 335912 Color: 1
Size: 152189 Color: 0

Bin 831: 5 of cap free
Amount of items: 2
Items: 
Size: 512310 Color: 1
Size: 487686 Color: 0

Bin 832: 5 of cap free
Amount of items: 2
Items: 
Size: 525139 Color: 0
Size: 474857 Color: 1

Bin 833: 5 of cap free
Amount of items: 2
Items: 
Size: 528156 Color: 0
Size: 471840 Color: 1

Bin 834: 5 of cap free
Amount of items: 2
Items: 
Size: 539287 Color: 1
Size: 460709 Color: 0

Bin 835: 5 of cap free
Amount of items: 2
Items: 
Size: 543006 Color: 1
Size: 456990 Color: 0

Bin 836: 5 of cap free
Amount of items: 2
Items: 
Size: 552361 Color: 0
Size: 447635 Color: 1

Bin 837: 5 of cap free
Amount of items: 2
Items: 
Size: 558027 Color: 0
Size: 441969 Color: 1

Bin 838: 5 of cap free
Amount of items: 2
Items: 
Size: 560110 Color: 1
Size: 439886 Color: 0

Bin 839: 5 of cap free
Amount of items: 2
Items: 
Size: 560110 Color: 0
Size: 439886 Color: 1

Bin 840: 5 of cap free
Amount of items: 3
Items: 
Size: 561962 Color: 1
Size: 256245 Color: 1
Size: 181789 Color: 0

Bin 841: 5 of cap free
Amount of items: 3
Items: 
Size: 577372 Color: 0
Size: 263848 Color: 1
Size: 158776 Color: 1

Bin 842: 5 of cap free
Amount of items: 2
Items: 
Size: 577805 Color: 0
Size: 422191 Color: 1

Bin 843: 5 of cap free
Amount of items: 2
Items: 
Size: 616470 Color: 1
Size: 383526 Color: 0

Bin 844: 5 of cap free
Amount of items: 2
Items: 
Size: 627580 Color: 0
Size: 372416 Color: 1

Bin 845: 5 of cap free
Amount of items: 2
Items: 
Size: 663064 Color: 0
Size: 336932 Color: 1

Bin 846: 5 of cap free
Amount of items: 2
Items: 
Size: 663695 Color: 0
Size: 336301 Color: 1

Bin 847: 5 of cap free
Amount of items: 2
Items: 
Size: 664905 Color: 0
Size: 335091 Color: 1

Bin 848: 5 of cap free
Amount of items: 2
Items: 
Size: 673909 Color: 0
Size: 326087 Color: 1

Bin 849: 5 of cap free
Amount of items: 2
Items: 
Size: 719985 Color: 1
Size: 280011 Color: 0

Bin 850: 5 of cap free
Amount of items: 2
Items: 
Size: 740096 Color: 1
Size: 259900 Color: 0

Bin 851: 5 of cap free
Amount of items: 3
Items: 
Size: 375147 Color: 1
Size: 334113 Color: 0
Size: 290736 Color: 1

Bin 852: 6 of cap free
Amount of items: 5
Items: 
Size: 255974 Color: 1
Size: 246335 Color: 1
Size: 222793 Color: 0
Size: 154285 Color: 0
Size: 120608 Color: 1

Bin 853: 6 of cap free
Amount of items: 3
Items: 
Size: 369453 Color: 1
Size: 327087 Color: 0
Size: 303455 Color: 0

Bin 854: 6 of cap free
Amount of items: 3
Items: 
Size: 469565 Color: 1
Size: 392094 Color: 0
Size: 138336 Color: 0

Bin 855: 6 of cap free
Amount of items: 3
Items: 
Size: 471604 Color: 1
Size: 409446 Color: 1
Size: 118945 Color: 0

Bin 856: 6 of cap free
Amount of items: 3
Items: 
Size: 502878 Color: 1
Size: 361428 Color: 1
Size: 135689 Color: 0

Bin 857: 6 of cap free
Amount of items: 2
Items: 
Size: 505247 Color: 0
Size: 494748 Color: 1

Bin 858: 6 of cap free
Amount of items: 2
Items: 
Size: 523081 Color: 0
Size: 476914 Color: 1

Bin 859: 6 of cap free
Amount of items: 2
Items: 
Size: 527436 Color: 1
Size: 472559 Color: 0

Bin 860: 6 of cap free
Amount of items: 2
Items: 
Size: 543415 Color: 1
Size: 456580 Color: 0

Bin 861: 6 of cap free
Amount of items: 2
Items: 
Size: 550342 Color: 0
Size: 449653 Color: 1

Bin 862: 6 of cap free
Amount of items: 3
Items: 
Size: 567246 Color: 0
Size: 293876 Color: 1
Size: 138873 Color: 1

Bin 863: 6 of cap free
Amount of items: 2
Items: 
Size: 584093 Color: 0
Size: 415902 Color: 1

Bin 864: 6 of cap free
Amount of items: 2
Items: 
Size: 614947 Color: 1
Size: 385048 Color: 0

Bin 865: 6 of cap free
Amount of items: 2
Items: 
Size: 631686 Color: 1
Size: 368309 Color: 0

Bin 866: 6 of cap free
Amount of items: 2
Items: 
Size: 637720 Color: 1
Size: 362275 Color: 0

Bin 867: 6 of cap free
Amount of items: 2
Items: 
Size: 640852 Color: 0
Size: 359143 Color: 1

Bin 868: 6 of cap free
Amount of items: 2
Items: 
Size: 661685 Color: 0
Size: 338310 Color: 1

Bin 869: 6 of cap free
Amount of items: 2
Items: 
Size: 679784 Color: 1
Size: 320211 Color: 0

Bin 870: 6 of cap free
Amount of items: 2
Items: 
Size: 710330 Color: 1
Size: 289665 Color: 0

Bin 871: 6 of cap free
Amount of items: 2
Items: 
Size: 766535 Color: 1
Size: 233460 Color: 0

Bin 872: 6 of cap free
Amount of items: 2
Items: 
Size: 769674 Color: 0
Size: 230321 Color: 1

Bin 873: 6 of cap free
Amount of items: 2
Items: 
Size: 770786 Color: 1
Size: 229209 Color: 0

Bin 874: 6 of cap free
Amount of items: 2
Items: 
Size: 798496 Color: 0
Size: 201499 Color: 1

Bin 875: 6 of cap free
Amount of items: 4
Items: 
Size: 428675 Color: 0
Size: 311782 Color: 1
Size: 159263 Color: 1
Size: 100275 Color: 0

Bin 876: 6 of cap free
Amount of items: 3
Items: 
Size: 716105 Color: 0
Size: 180870 Color: 0
Size: 103020 Color: 1

Bin 877: 6 of cap free
Amount of items: 2
Items: 
Size: 524239 Color: 0
Size: 475756 Color: 1

Bin 878: 6 of cap free
Amount of items: 2
Items: 
Size: 707236 Color: 0
Size: 292759 Color: 1

Bin 879: 6 of cap free
Amount of items: 3
Items: 
Size: 355404 Color: 1
Size: 342535 Color: 0
Size: 302056 Color: 1

Bin 880: 6 of cap free
Amount of items: 3
Items: 
Size: 740081 Color: 0
Size: 143148 Color: 0
Size: 116766 Color: 1

Bin 881: 6 of cap free
Amount of items: 5
Items: 
Size: 261708 Color: 0
Size: 193609 Color: 1
Size: 193562 Color: 1
Size: 189950 Color: 0
Size: 161166 Color: 1

Bin 882: 6 of cap free
Amount of items: 5
Items: 
Size: 253449 Color: 1
Size: 187483 Color: 1
Size: 186525 Color: 1
Size: 186350 Color: 0
Size: 186188 Color: 0

Bin 883: 6 of cap free
Amount of items: 3
Items: 
Size: 570927 Color: 0
Size: 242942 Color: 1
Size: 186126 Color: 1

Bin 884: 7 of cap free
Amount of items: 2
Items: 
Size: 780630 Color: 1
Size: 219364 Color: 0

Bin 885: 7 of cap free
Amount of items: 5
Items: 
Size: 261794 Color: 1
Size: 256276 Color: 1
Size: 222836 Color: 0
Size: 141074 Color: 0
Size: 118014 Color: 1

Bin 886: 7 of cap free
Amount of items: 3
Items: 
Size: 412333 Color: 0
Size: 304428 Color: 1
Size: 283233 Color: 1

Bin 887: 7 of cap free
Amount of items: 3
Items: 
Size: 440182 Color: 1
Size: 392221 Color: 0
Size: 167591 Color: 0

Bin 888: 7 of cap free
Amount of items: 2
Items: 
Size: 507754 Color: 0
Size: 492240 Color: 1

Bin 889: 7 of cap free
Amount of items: 2
Items: 
Size: 538440 Color: 0
Size: 461554 Color: 1

Bin 890: 7 of cap free
Amount of items: 2
Items: 
Size: 556230 Color: 0
Size: 443764 Color: 1

Bin 891: 7 of cap free
Amount of items: 2
Items: 
Size: 568577 Color: 0
Size: 431417 Color: 1

Bin 892: 7 of cap free
Amount of items: 2
Items: 
Size: 576968 Color: 1
Size: 423026 Color: 0

Bin 893: 7 of cap free
Amount of items: 2
Items: 
Size: 577310 Color: 1
Size: 422684 Color: 0

Bin 894: 7 of cap free
Amount of items: 2
Items: 
Size: 592458 Color: 0
Size: 407536 Color: 1

Bin 895: 7 of cap free
Amount of items: 2
Items: 
Size: 629536 Color: 1
Size: 370458 Color: 0

Bin 896: 7 of cap free
Amount of items: 2
Items: 
Size: 634442 Color: 1
Size: 365552 Color: 0

Bin 897: 7 of cap free
Amount of items: 2
Items: 
Size: 640172 Color: 0
Size: 359822 Color: 1

Bin 898: 7 of cap free
Amount of items: 2
Items: 
Size: 667251 Color: 1
Size: 332743 Color: 0

Bin 899: 7 of cap free
Amount of items: 2
Items: 
Size: 684474 Color: 0
Size: 315520 Color: 1

Bin 900: 7 of cap free
Amount of items: 2
Items: 
Size: 686873 Color: 1
Size: 313121 Color: 0

Bin 901: 7 of cap free
Amount of items: 2
Items: 
Size: 692865 Color: 1
Size: 307129 Color: 0

Bin 902: 7 of cap free
Amount of items: 2
Items: 
Size: 707384 Color: 1
Size: 292610 Color: 0

Bin 903: 7 of cap free
Amount of items: 2
Items: 
Size: 751725 Color: 0
Size: 248269 Color: 1

Bin 904: 7 of cap free
Amount of items: 2
Items: 
Size: 794674 Color: 0
Size: 205320 Color: 1

Bin 905: 7 of cap free
Amount of items: 3
Items: 
Size: 778682 Color: 1
Size: 118619 Color: 1
Size: 102693 Color: 0

Bin 906: 7 of cap free
Amount of items: 3
Items: 
Size: 369238 Color: 1
Size: 327637 Color: 0
Size: 303119 Color: 1

Bin 907: 7 of cap free
Amount of items: 2
Items: 
Size: 798159 Color: 1
Size: 201835 Color: 0

Bin 908: 7 of cap free
Amount of items: 3
Items: 
Size: 450667 Color: 0
Size: 420383 Color: 1
Size: 128944 Color: 0

Bin 909: 7 of cap free
Amount of items: 2
Items: 
Size: 792578 Color: 1
Size: 207416 Color: 0

Bin 910: 7 of cap free
Amount of items: 5
Items: 
Size: 240195 Color: 0
Size: 191506 Color: 1
Size: 191496 Color: 1
Size: 188483 Color: 0
Size: 188314 Color: 0

Bin 911: 8 of cap free
Amount of items: 3
Items: 
Size: 413830 Color: 0
Size: 330542 Color: 0
Size: 255621 Color: 1

Bin 912: 8 of cap free
Amount of items: 3
Items: 
Size: 439477 Color: 1
Size: 435024 Color: 0
Size: 125492 Color: 0

Bin 913: 8 of cap free
Amount of items: 2
Items: 
Size: 504564 Color: 0
Size: 495429 Color: 1

Bin 914: 8 of cap free
Amount of items: 2
Items: 
Size: 523256 Color: 0
Size: 476737 Color: 1

Bin 915: 8 of cap free
Amount of items: 2
Items: 
Size: 528874 Color: 1
Size: 471119 Color: 0

Bin 916: 8 of cap free
Amount of items: 2
Items: 
Size: 529100 Color: 0
Size: 470893 Color: 1

Bin 917: 8 of cap free
Amount of items: 2
Items: 
Size: 531365 Color: 0
Size: 468628 Color: 1

Bin 918: 8 of cap free
Amount of items: 2
Items: 
Size: 574553 Color: 1
Size: 425440 Color: 0

Bin 919: 8 of cap free
Amount of items: 2
Items: 
Size: 578432 Color: 0
Size: 421561 Color: 1

Bin 920: 8 of cap free
Amount of items: 2
Items: 
Size: 587737 Color: 1
Size: 412256 Color: 0

Bin 921: 8 of cap free
Amount of items: 2
Items: 
Size: 610275 Color: 0
Size: 389718 Color: 1

Bin 922: 8 of cap free
Amount of items: 2
Items: 
Size: 647380 Color: 1
Size: 352613 Color: 0

Bin 923: 8 of cap free
Amount of items: 2
Items: 
Size: 653088 Color: 0
Size: 346905 Color: 1

Bin 924: 8 of cap free
Amount of items: 2
Items: 
Size: 672053 Color: 1
Size: 327940 Color: 0

Bin 925: 8 of cap free
Amount of items: 2
Items: 
Size: 679119 Color: 0
Size: 320874 Color: 1

Bin 926: 8 of cap free
Amount of items: 2
Items: 
Size: 689229 Color: 0
Size: 310764 Color: 1

Bin 927: 8 of cap free
Amount of items: 2
Items: 
Size: 698017 Color: 1
Size: 301976 Color: 0

Bin 928: 8 of cap free
Amount of items: 2
Items: 
Size: 708841 Color: 1
Size: 291152 Color: 0

Bin 929: 8 of cap free
Amount of items: 2
Items: 
Size: 722027 Color: 0
Size: 277966 Color: 1

Bin 930: 8 of cap free
Amount of items: 2
Items: 
Size: 737677 Color: 1
Size: 262316 Color: 0

Bin 931: 8 of cap free
Amount of items: 2
Items: 
Size: 742369 Color: 1
Size: 257624 Color: 0

Bin 932: 8 of cap free
Amount of items: 2
Items: 
Size: 750078 Color: 1
Size: 249915 Color: 0

Bin 933: 8 of cap free
Amount of items: 2
Items: 
Size: 771501 Color: 0
Size: 228492 Color: 1

Bin 934: 8 of cap free
Amount of items: 2
Items: 
Size: 779522 Color: 1
Size: 220471 Color: 0

Bin 935: 8 of cap free
Amount of items: 2
Items: 
Size: 787275 Color: 0
Size: 212718 Color: 1

Bin 936: 8 of cap free
Amount of items: 3
Items: 
Size: 368945 Color: 1
Size: 326726 Color: 0
Size: 304322 Color: 0

Bin 937: 8 of cap free
Amount of items: 5
Items: 
Size: 210201 Color: 1
Size: 207484 Color: 1
Size: 197248 Color: 0
Size: 192914 Color: 1
Size: 192146 Color: 0

Bin 938: 8 of cap free
Amount of items: 3
Items: 
Size: 375187 Color: 0
Size: 342602 Color: 0
Size: 282204 Color: 1

Bin 939: 8 of cap free
Amount of items: 3
Items: 
Size: 367690 Color: 1
Size: 326671 Color: 0
Size: 305632 Color: 0

Bin 940: 8 of cap free
Amount of items: 5
Items: 
Size: 295693 Color: 1
Size: 186366 Color: 0
Size: 185757 Color: 0
Size: 168892 Color: 1
Size: 163285 Color: 1

Bin 941: 8 of cap free
Amount of items: 2
Items: 
Size: 752666 Color: 0
Size: 247327 Color: 1

Bin 942: 9 of cap free
Amount of items: 3
Items: 
Size: 497217 Color: 0
Size: 368036 Color: 1
Size: 134739 Color: 1

Bin 943: 9 of cap free
Amount of items: 2
Items: 
Size: 501921 Color: 0
Size: 498071 Color: 1

Bin 944: 9 of cap free
Amount of items: 2
Items: 
Size: 529799 Color: 1
Size: 470193 Color: 0

Bin 945: 9 of cap free
Amount of items: 2
Items: 
Size: 529770 Color: 0
Size: 470222 Color: 1

Bin 946: 9 of cap free
Amount of items: 2
Items: 
Size: 544227 Color: 1
Size: 455765 Color: 0

Bin 947: 9 of cap free
Amount of items: 2
Items: 
Size: 545942 Color: 0
Size: 454050 Color: 1

Bin 948: 9 of cap free
Amount of items: 2
Items: 
Size: 551746 Color: 1
Size: 448246 Color: 0

Bin 949: 9 of cap free
Amount of items: 2
Items: 
Size: 556831 Color: 0
Size: 443161 Color: 1

Bin 950: 9 of cap free
Amount of items: 2
Items: 
Size: 558467 Color: 1
Size: 441525 Color: 0

Bin 951: 9 of cap free
Amount of items: 2
Items: 
Size: 560885 Color: 1
Size: 439107 Color: 0

Bin 952: 9 of cap free
Amount of items: 2
Items: 
Size: 563895 Color: 0
Size: 436097 Color: 1

Bin 953: 9 of cap free
Amount of items: 2
Items: 
Size: 569199 Color: 1
Size: 430793 Color: 0

Bin 954: 9 of cap free
Amount of items: 2
Items: 
Size: 570359 Color: 1
Size: 429633 Color: 0

Bin 955: 9 of cap free
Amount of items: 2
Items: 
Size: 586511 Color: 1
Size: 413481 Color: 0

Bin 956: 9 of cap free
Amount of items: 2
Items: 
Size: 593977 Color: 0
Size: 406015 Color: 1

Bin 957: 9 of cap free
Amount of items: 2
Items: 
Size: 598679 Color: 0
Size: 401313 Color: 1

Bin 958: 9 of cap free
Amount of items: 2
Items: 
Size: 614493 Color: 1
Size: 385499 Color: 0

Bin 959: 9 of cap free
Amount of items: 2
Items: 
Size: 615599 Color: 1
Size: 384393 Color: 0

Bin 960: 9 of cap free
Amount of items: 2
Items: 
Size: 635956 Color: 1
Size: 364036 Color: 0

Bin 961: 9 of cap free
Amount of items: 2
Items: 
Size: 690990 Color: 1
Size: 309002 Color: 0

Bin 962: 9 of cap free
Amount of items: 2
Items: 
Size: 703512 Color: 0
Size: 296480 Color: 1

Bin 963: 9 of cap free
Amount of items: 2
Items: 
Size: 721954 Color: 0
Size: 278038 Color: 1

Bin 964: 9 of cap free
Amount of items: 2
Items: 
Size: 760341 Color: 0
Size: 239651 Color: 1

Bin 965: 9 of cap free
Amount of items: 2
Items: 
Size: 760724 Color: 1
Size: 239268 Color: 0

Bin 966: 9 of cap free
Amount of items: 2
Items: 
Size: 627869 Color: 0
Size: 372123 Color: 1

Bin 967: 9 of cap free
Amount of items: 6
Items: 
Size: 173825 Color: 1
Size: 166465 Color: 0
Size: 166082 Color: 0
Size: 166005 Color: 0
Size: 163896 Color: 1
Size: 163719 Color: 1

Bin 968: 9 of cap free
Amount of items: 4
Items: 
Size: 370775 Color: 1
Size: 311223 Color: 0
Size: 159286 Color: 0
Size: 158708 Color: 1

Bin 969: 9 of cap free
Amount of items: 4
Items: 
Size: 292076 Color: 0
Size: 287017 Color: 1
Size: 254487 Color: 0
Size: 166412 Color: 1

Bin 970: 10 of cap free
Amount of items: 2
Items: 
Size: 777531 Color: 0
Size: 222460 Color: 1

Bin 971: 10 of cap free
Amount of items: 2
Items: 
Size: 662634 Color: 0
Size: 337357 Color: 1

Bin 972: 10 of cap free
Amount of items: 3
Items: 
Size: 741968 Color: 0
Size: 129532 Color: 1
Size: 128491 Color: 0

Bin 973: 10 of cap free
Amount of items: 3
Items: 
Size: 412860 Color: 0
Size: 397131 Color: 1
Size: 190000 Color: 0

Bin 974: 10 of cap free
Amount of items: 3
Items: 
Size: 441106 Color: 1
Size: 413427 Color: 0
Size: 145458 Color: 0

Bin 975: 10 of cap free
Amount of items: 2
Items: 
Size: 500975 Color: 0
Size: 499016 Color: 1

Bin 976: 10 of cap free
Amount of items: 2
Items: 
Size: 541088 Color: 0
Size: 458903 Color: 1

Bin 977: 10 of cap free
Amount of items: 2
Items: 
Size: 571838 Color: 0
Size: 428153 Color: 1

Bin 978: 10 of cap free
Amount of items: 2
Items: 
Size: 582832 Color: 0
Size: 417159 Color: 1

Bin 979: 10 of cap free
Amount of items: 2
Items: 
Size: 616446 Color: 0
Size: 383545 Color: 1

Bin 980: 10 of cap free
Amount of items: 2
Items: 
Size: 644800 Color: 1
Size: 355191 Color: 0

Bin 981: 10 of cap free
Amount of items: 2
Items: 
Size: 700893 Color: 1
Size: 299098 Color: 0

Bin 982: 10 of cap free
Amount of items: 2
Items: 
Size: 719231 Color: 1
Size: 280760 Color: 0

Bin 983: 10 of cap free
Amount of items: 2
Items: 
Size: 740742 Color: 0
Size: 259249 Color: 1

Bin 984: 10 of cap free
Amount of items: 2
Items: 
Size: 755106 Color: 1
Size: 244885 Color: 0

Bin 985: 10 of cap free
Amount of items: 2
Items: 
Size: 767631 Color: 1
Size: 232360 Color: 0

Bin 986: 10 of cap free
Amount of items: 2
Items: 
Size: 772070 Color: 0
Size: 227921 Color: 1

Bin 987: 10 of cap free
Amount of items: 2
Items: 
Size: 772407 Color: 1
Size: 227584 Color: 0

Bin 988: 10 of cap free
Amount of items: 2
Items: 
Size: 794815 Color: 0
Size: 205176 Color: 1

Bin 989: 10 of cap free
Amount of items: 2
Items: 
Size: 702615 Color: 1
Size: 297376 Color: 0

Bin 990: 10 of cap free
Amount of items: 2
Items: 
Size: 773765 Color: 0
Size: 226226 Color: 1

Bin 991: 10 of cap free
Amount of items: 2
Items: 
Size: 548302 Color: 1
Size: 451689 Color: 0

Bin 992: 10 of cap free
Amount of items: 5
Items: 
Size: 280447 Color: 0
Size: 180252 Color: 0
Size: 179863 Color: 1
Size: 179858 Color: 1
Size: 179571 Color: 0

Bin 993: 11 of cap free
Amount of items: 3
Items: 
Size: 440993 Color: 1
Size: 391121 Color: 0
Size: 167876 Color: 1

Bin 994: 11 of cap free
Amount of items: 2
Items: 
Size: 518756 Color: 1
Size: 481234 Color: 0

Bin 995: 11 of cap free
Amount of items: 2
Items: 
Size: 522119 Color: 1
Size: 477871 Color: 0

Bin 996: 11 of cap free
Amount of items: 2
Items: 
Size: 538241 Color: 1
Size: 461749 Color: 0

Bin 997: 11 of cap free
Amount of items: 2
Items: 
Size: 569718 Color: 0
Size: 430272 Color: 1

Bin 998: 11 of cap free
Amount of items: 2
Items: 
Size: 583420 Color: 0
Size: 416570 Color: 1

Bin 999: 11 of cap free
Amount of items: 2
Items: 
Size: 586662 Color: 1
Size: 413328 Color: 0

Bin 1000: 11 of cap free
Amount of items: 2
Items: 
Size: 606607 Color: 1
Size: 393383 Color: 0

Bin 1001: 11 of cap free
Amount of items: 2
Items: 
Size: 612835 Color: 0
Size: 387155 Color: 1

Bin 1002: 11 of cap free
Amount of items: 2
Items: 
Size: 613246 Color: 0
Size: 386744 Color: 1

Bin 1003: 11 of cap free
Amount of items: 2
Items: 
Size: 616949 Color: 1
Size: 383041 Color: 0

Bin 1004: 11 of cap free
Amount of items: 2
Items: 
Size: 621578 Color: 0
Size: 378412 Color: 1

Bin 1005: 11 of cap free
Amount of items: 2
Items: 
Size: 622431 Color: 0
Size: 377559 Color: 1

Bin 1006: 11 of cap free
Amount of items: 2
Items: 
Size: 630149 Color: 0
Size: 369841 Color: 1

Bin 1007: 11 of cap free
Amount of items: 2
Items: 
Size: 637738 Color: 1
Size: 362252 Color: 0

Bin 1008: 11 of cap free
Amount of items: 2
Items: 
Size: 662574 Color: 1
Size: 337416 Color: 0

Bin 1009: 11 of cap free
Amount of items: 2
Items: 
Size: 663284 Color: 1
Size: 336706 Color: 0

Bin 1010: 11 of cap free
Amount of items: 2
Items: 
Size: 670430 Color: 1
Size: 329560 Color: 0

Bin 1011: 11 of cap free
Amount of items: 2
Items: 
Size: 683289 Color: 1
Size: 316701 Color: 0

Bin 1012: 11 of cap free
Amount of items: 2
Items: 
Size: 685812 Color: 1
Size: 314178 Color: 0

Bin 1013: 11 of cap free
Amount of items: 2
Items: 
Size: 698253 Color: 0
Size: 301737 Color: 1

Bin 1014: 11 of cap free
Amount of items: 2
Items: 
Size: 700456 Color: 0
Size: 299534 Color: 1

Bin 1015: 11 of cap free
Amount of items: 2
Items: 
Size: 705651 Color: 0
Size: 294339 Color: 1

Bin 1016: 11 of cap free
Amount of items: 2
Items: 
Size: 723743 Color: 0
Size: 276247 Color: 1

Bin 1017: 11 of cap free
Amount of items: 2
Items: 
Size: 738619 Color: 1
Size: 261371 Color: 0

Bin 1018: 11 of cap free
Amount of items: 2
Items: 
Size: 765396 Color: 1
Size: 234594 Color: 0

Bin 1019: 11 of cap free
Amount of items: 2
Items: 
Size: 778474 Color: 1
Size: 221516 Color: 0

Bin 1020: 11 of cap free
Amount of items: 2
Items: 
Size: 794591 Color: 1
Size: 205399 Color: 0

Bin 1021: 11 of cap free
Amount of items: 7
Items: 
Size: 156224 Color: 1
Size: 155984 Color: 1
Size: 155689 Color: 1
Size: 154863 Color: 0
Size: 154527 Color: 0
Size: 111429 Color: 0
Size: 111274 Color: 1

Bin 1022: 11 of cap free
Amount of items: 2
Items: 
Size: 562361 Color: 0
Size: 437629 Color: 1

Bin 1023: 12 of cap free
Amount of items: 2
Items: 
Size: 518310 Color: 1
Size: 481679 Color: 0

Bin 1024: 12 of cap free
Amount of items: 2
Items: 
Size: 550163 Color: 0
Size: 449826 Color: 1

Bin 1025: 12 of cap free
Amount of items: 3
Items: 
Size: 446467 Color: 0
Size: 433571 Color: 1
Size: 119951 Color: 1

Bin 1026: 12 of cap free
Amount of items: 2
Items: 
Size: 530493 Color: 0
Size: 469496 Color: 1

Bin 1027: 12 of cap free
Amount of items: 2
Items: 
Size: 536803 Color: 0
Size: 463186 Color: 1

Bin 1028: 12 of cap free
Amount of items: 2
Items: 
Size: 558038 Color: 1
Size: 441951 Color: 0

Bin 1029: 12 of cap free
Amount of items: 2
Items: 
Size: 565178 Color: 1
Size: 434811 Color: 0

Bin 1030: 12 of cap free
Amount of items: 2
Items: 
Size: 581350 Color: 1
Size: 418639 Color: 0

Bin 1031: 12 of cap free
Amount of items: 2
Items: 
Size: 586961 Color: 1
Size: 413028 Color: 0

Bin 1032: 12 of cap free
Amount of items: 2
Items: 
Size: 597308 Color: 1
Size: 402681 Color: 0

Bin 1033: 12 of cap free
Amount of items: 2
Items: 
Size: 599957 Color: 1
Size: 400032 Color: 0

Bin 1034: 12 of cap free
Amount of items: 2
Items: 
Size: 612427 Color: 1
Size: 387562 Color: 0

Bin 1035: 12 of cap free
Amount of items: 2
Items: 
Size: 618778 Color: 1
Size: 381211 Color: 0

Bin 1036: 12 of cap free
Amount of items: 2
Items: 
Size: 620341 Color: 1
Size: 379648 Color: 0

Bin 1037: 12 of cap free
Amount of items: 2
Items: 
Size: 625919 Color: 1
Size: 374070 Color: 0

Bin 1038: 12 of cap free
Amount of items: 2
Items: 
Size: 635425 Color: 1
Size: 364564 Color: 0

Bin 1039: 12 of cap free
Amount of items: 2
Items: 
Size: 636310 Color: 0
Size: 363679 Color: 1

Bin 1040: 12 of cap free
Amount of items: 2
Items: 
Size: 651124 Color: 1
Size: 348865 Color: 0

Bin 1041: 12 of cap free
Amount of items: 2
Items: 
Size: 668796 Color: 1
Size: 331193 Color: 0

Bin 1042: 12 of cap free
Amount of items: 2
Items: 
Size: 724810 Color: 0
Size: 275179 Color: 1

Bin 1043: 12 of cap free
Amount of items: 2
Items: 
Size: 727660 Color: 0
Size: 272329 Color: 1

Bin 1044: 12 of cap free
Amount of items: 2
Items: 
Size: 736729 Color: 1
Size: 263260 Color: 0

Bin 1045: 12 of cap free
Amount of items: 2
Items: 
Size: 798252 Color: 1
Size: 201737 Color: 0

Bin 1046: 12 of cap free
Amount of items: 3
Items: 
Size: 354439 Color: 1
Size: 334530 Color: 0
Size: 311020 Color: 0

Bin 1047: 12 of cap free
Amount of items: 5
Items: 
Size: 280074 Color: 1
Size: 254422 Color: 1
Size: 158565 Color: 1
Size: 153999 Color: 0
Size: 152929 Color: 0

Bin 1048: 12 of cap free
Amount of items: 5
Items: 
Size: 330890 Color: 1
Size: 172177 Color: 0
Size: 167956 Color: 0
Size: 164779 Color: 1
Size: 164187 Color: 1

Bin 1049: 13 of cap free
Amount of items: 3
Items: 
Size: 699685 Color: 0
Size: 166786 Color: 0
Size: 133517 Color: 1

Bin 1050: 13 of cap free
Amount of items: 3
Items: 
Size: 363874 Color: 1
Size: 353409 Color: 0
Size: 282705 Color: 1

Bin 1051: 13 of cap free
Amount of items: 3
Items: 
Size: 414010 Color: 1
Size: 407363 Color: 1
Size: 178615 Color: 0

Bin 1052: 13 of cap free
Amount of items: 3
Items: 
Size: 457484 Color: 1
Size: 376159 Color: 0
Size: 166345 Color: 0

Bin 1053: 13 of cap free
Amount of items: 2
Items: 
Size: 506149 Color: 1
Size: 493839 Color: 0

Bin 1054: 13 of cap free
Amount of items: 2
Items: 
Size: 542047 Color: 0
Size: 457941 Color: 1

Bin 1055: 13 of cap free
Amount of items: 2
Items: 
Size: 559699 Color: 1
Size: 440289 Color: 0

Bin 1056: 13 of cap free
Amount of items: 2
Items: 
Size: 604844 Color: 1
Size: 395144 Color: 0

Bin 1057: 13 of cap free
Amount of items: 2
Items: 
Size: 646964 Color: 1
Size: 353024 Color: 0

Bin 1058: 13 of cap free
Amount of items: 2
Items: 
Size: 648951 Color: 0
Size: 351037 Color: 1

Bin 1059: 13 of cap free
Amount of items: 2
Items: 
Size: 651126 Color: 0
Size: 348862 Color: 1

Bin 1060: 13 of cap free
Amount of items: 2
Items: 
Size: 680937 Color: 1
Size: 319051 Color: 0

Bin 1061: 13 of cap free
Amount of items: 2
Items: 
Size: 706899 Color: 1
Size: 293089 Color: 0

Bin 1062: 13 of cap free
Amount of items: 2
Items: 
Size: 711698 Color: 1
Size: 288290 Color: 0

Bin 1063: 13 of cap free
Amount of items: 2
Items: 
Size: 727715 Color: 1
Size: 272273 Color: 0

Bin 1064: 13 of cap free
Amount of items: 2
Items: 
Size: 728288 Color: 1
Size: 271700 Color: 0

Bin 1065: 13 of cap free
Amount of items: 2
Items: 
Size: 735368 Color: 1
Size: 264620 Color: 0

Bin 1066: 13 of cap free
Amount of items: 2
Items: 
Size: 743868 Color: 0
Size: 256120 Color: 1

Bin 1067: 13 of cap free
Amount of items: 2
Items: 
Size: 787954 Color: 0
Size: 212034 Color: 1

Bin 1068: 13 of cap free
Amount of items: 5
Items: 
Size: 452780 Color: 0
Size: 154126 Color: 1
Size: 151515 Color: 0
Size: 128783 Color: 0
Size: 112784 Color: 1

Bin 1069: 13 of cap free
Amount of items: 3
Items: 
Size: 397651 Color: 1
Size: 360724 Color: 0
Size: 241613 Color: 0

Bin 1070: 13 of cap free
Amount of items: 3
Items: 
Size: 367756 Color: 1
Size: 330320 Color: 0
Size: 301912 Color: 1

Bin 1071: 13 of cap free
Amount of items: 6
Items: 
Size: 179840 Color: 1
Size: 174576 Color: 0
Size: 172921 Color: 0
Size: 171090 Color: 1
Size: 159756 Color: 1
Size: 141805 Color: 0

Bin 1072: 13 of cap free
Amount of items: 2
Items: 
Size: 630980 Color: 0
Size: 369008 Color: 1

Bin 1073: 13 of cap free
Amount of items: 2
Items: 
Size: 710035 Color: 0
Size: 289953 Color: 1

Bin 1074: 13 of cap free
Amount of items: 3
Items: 
Size: 368967 Color: 1
Size: 318847 Color: 1
Size: 312174 Color: 0

Bin 1075: 13 of cap free
Amount of items: 5
Items: 
Size: 214048 Color: 0
Size: 198500 Color: 1
Size: 198182 Color: 1
Size: 195061 Color: 0
Size: 194197 Color: 0

Bin 1076: 13 of cap free
Amount of items: 6
Items: 
Size: 189462 Color: 0
Size: 162719 Color: 0
Size: 162521 Color: 0
Size: 162059 Color: 1
Size: 161667 Color: 1
Size: 161560 Color: 1

Bin 1077: 13 of cap free
Amount of items: 5
Items: 
Size: 271800 Color: 0
Size: 185831 Color: 0
Size: 182584 Color: 0
Size: 181378 Color: 1
Size: 178395 Color: 1

Bin 1078: 14 of cap free
Amount of items: 3
Items: 
Size: 367018 Color: 1
Size: 316976 Color: 1
Size: 315993 Color: 0

Bin 1079: 14 of cap free
Amount of items: 3
Items: 
Size: 370365 Color: 1
Size: 318065 Color: 1
Size: 311557 Color: 0

Bin 1080: 14 of cap free
Amount of items: 2
Items: 
Size: 506737 Color: 0
Size: 493250 Color: 1

Bin 1081: 14 of cap free
Amount of items: 2
Items: 
Size: 526964 Color: 1
Size: 473023 Color: 0

Bin 1082: 14 of cap free
Amount of items: 2
Items: 
Size: 528547 Color: 0
Size: 471440 Color: 1

Bin 1083: 14 of cap free
Amount of items: 2
Items: 
Size: 549369 Color: 0
Size: 450618 Color: 1

Bin 1084: 14 of cap free
Amount of items: 2
Items: 
Size: 557241 Color: 0
Size: 442746 Color: 1

Bin 1085: 14 of cap free
Amount of items: 2
Items: 
Size: 568481 Color: 1
Size: 431506 Color: 0

Bin 1086: 14 of cap free
Amount of items: 2
Items: 
Size: 577077 Color: 1
Size: 422910 Color: 0

Bin 1087: 14 of cap free
Amount of items: 2
Items: 
Size: 590285 Color: 1
Size: 409702 Color: 0

Bin 1088: 14 of cap free
Amount of items: 2
Items: 
Size: 590466 Color: 1
Size: 409521 Color: 0

Bin 1089: 14 of cap free
Amount of items: 2
Items: 
Size: 596171 Color: 0
Size: 403816 Color: 1

Bin 1090: 14 of cap free
Amount of items: 2
Items: 
Size: 614217 Color: 0
Size: 385770 Color: 1

Bin 1091: 14 of cap free
Amount of items: 2
Items: 
Size: 617366 Color: 0
Size: 382621 Color: 1

Bin 1092: 14 of cap free
Amount of items: 2
Items: 
Size: 619590 Color: 1
Size: 380397 Color: 0

Bin 1093: 14 of cap free
Amount of items: 2
Items: 
Size: 631806 Color: 1
Size: 368181 Color: 0

Bin 1094: 14 of cap free
Amount of items: 2
Items: 
Size: 635510 Color: 1
Size: 364477 Color: 0

Bin 1095: 14 of cap free
Amount of items: 2
Items: 
Size: 655322 Color: 0
Size: 344665 Color: 1

Bin 1096: 14 of cap free
Amount of items: 2
Items: 
Size: 673750 Color: 1
Size: 326237 Color: 0

Bin 1097: 14 of cap free
Amount of items: 2
Items: 
Size: 675354 Color: 0
Size: 324633 Color: 1

Bin 1098: 14 of cap free
Amount of items: 2
Items: 
Size: 698272 Color: 0
Size: 301715 Color: 1

Bin 1099: 14 of cap free
Amount of items: 2
Items: 
Size: 707046 Color: 0
Size: 292941 Color: 1

Bin 1100: 14 of cap free
Amount of items: 2
Items: 
Size: 717613 Color: 0
Size: 282374 Color: 1

Bin 1101: 14 of cap free
Amount of items: 2
Items: 
Size: 765598 Color: 1
Size: 234389 Color: 0

Bin 1102: 14 of cap free
Amount of items: 2
Items: 
Size: 775566 Color: 1
Size: 224421 Color: 0

Bin 1103: 14 of cap free
Amount of items: 3
Items: 
Size: 360017 Color: 0
Size: 328908 Color: 1
Size: 311062 Color: 0

Bin 1104: 15 of cap free
Amount of items: 5
Items: 
Size: 398315 Color: 1
Size: 159512 Color: 1
Size: 149795 Color: 0
Size: 149784 Color: 0
Size: 142580 Color: 1

Bin 1105: 15 of cap free
Amount of items: 2
Items: 
Size: 534584 Color: 0
Size: 465402 Color: 1

Bin 1106: 15 of cap free
Amount of items: 2
Items: 
Size: 577922 Color: 0
Size: 422064 Color: 1

Bin 1107: 15 of cap free
Amount of items: 2
Items: 
Size: 579911 Color: 1
Size: 420075 Color: 0

Bin 1108: 15 of cap free
Amount of items: 2
Items: 
Size: 601936 Color: 0
Size: 398050 Color: 1

Bin 1109: 15 of cap free
Amount of items: 2
Items: 
Size: 629954 Color: 0
Size: 370032 Color: 1

Bin 1110: 15 of cap free
Amount of items: 2
Items: 
Size: 638003 Color: 1
Size: 361983 Color: 0

Bin 1111: 15 of cap free
Amount of items: 2
Items: 
Size: 649361 Color: 1
Size: 350625 Color: 0

Bin 1112: 15 of cap free
Amount of items: 2
Items: 
Size: 655974 Color: 1
Size: 344012 Color: 0

Bin 1113: 15 of cap free
Amount of items: 2
Items: 
Size: 656911 Color: 0
Size: 343075 Color: 1

Bin 1114: 15 of cap free
Amount of items: 2
Items: 
Size: 672650 Color: 0
Size: 327336 Color: 1

Bin 1115: 15 of cap free
Amount of items: 2
Items: 
Size: 681159 Color: 1
Size: 318827 Color: 0

Bin 1116: 15 of cap free
Amount of items: 2
Items: 
Size: 681696 Color: 1
Size: 318290 Color: 0

Bin 1117: 15 of cap free
Amount of items: 2
Items: 
Size: 698746 Color: 0
Size: 301240 Color: 1

Bin 1118: 15 of cap free
Amount of items: 2
Items: 
Size: 700401 Color: 1
Size: 299585 Color: 0

Bin 1119: 15 of cap free
Amount of items: 2
Items: 
Size: 752965 Color: 1
Size: 247021 Color: 0

Bin 1120: 15 of cap free
Amount of items: 2
Items: 
Size: 781949 Color: 1
Size: 218037 Color: 0

Bin 1121: 15 of cap free
Amount of items: 7
Items: 
Size: 180139 Color: 1
Size: 152488 Color: 0
Size: 137852 Color: 0
Size: 135846 Color: 1
Size: 135748 Color: 1
Size: 135698 Color: 0
Size: 122215 Color: 0

Bin 1122: 15 of cap free
Amount of items: 5
Items: 
Size: 266419 Color: 0
Size: 184215 Color: 0
Size: 183218 Color: 1
Size: 183081 Color: 1
Size: 183053 Color: 1

Bin 1123: 15 of cap free
Amount of items: 5
Items: 
Size: 235032 Color: 1
Size: 228362 Color: 1
Size: 198858 Color: 0
Size: 187078 Color: 0
Size: 150656 Color: 1

Bin 1124: 16 of cap free
Amount of items: 3
Items: 
Size: 408791 Color: 1
Size: 407525 Color: 0
Size: 183669 Color: 1

Bin 1125: 16 of cap free
Amount of items: 3
Items: 
Size: 412362 Color: 0
Size: 392252 Color: 0
Size: 195371 Color: 1

Bin 1126: 16 of cap free
Amount of items: 2
Items: 
Size: 501225 Color: 0
Size: 498760 Color: 1

Bin 1127: 16 of cap free
Amount of items: 2
Items: 
Size: 503909 Color: 0
Size: 496076 Color: 1

Bin 1128: 16 of cap free
Amount of items: 2
Items: 
Size: 508615 Color: 0
Size: 491370 Color: 1

Bin 1129: 16 of cap free
Amount of items: 2
Items: 
Size: 534980 Color: 0
Size: 465005 Color: 1

Bin 1130: 16 of cap free
Amount of items: 2
Items: 
Size: 536203 Color: 1
Size: 463782 Color: 0

Bin 1131: 16 of cap free
Amount of items: 2
Items: 
Size: 544381 Color: 1
Size: 455604 Color: 0

Bin 1132: 16 of cap free
Amount of items: 2
Items: 
Size: 546481 Color: 0
Size: 453504 Color: 1

Bin 1133: 16 of cap free
Amount of items: 2
Items: 
Size: 547816 Color: 0
Size: 452169 Color: 1

Bin 1134: 16 of cap free
Amount of items: 2
Items: 
Size: 557368 Color: 1
Size: 442617 Color: 0

Bin 1135: 16 of cap free
Amount of items: 2
Items: 
Size: 583922 Color: 0
Size: 416063 Color: 1

Bin 1136: 16 of cap free
Amount of items: 2
Items: 
Size: 585257 Color: 1
Size: 414728 Color: 0

Bin 1137: 16 of cap free
Amount of items: 2
Items: 
Size: 594132 Color: 0
Size: 405853 Color: 1

Bin 1138: 16 of cap free
Amount of items: 2
Items: 
Size: 610162 Color: 1
Size: 389823 Color: 0

Bin 1139: 16 of cap free
Amount of items: 2
Items: 
Size: 619283 Color: 1
Size: 380702 Color: 0

Bin 1140: 16 of cap free
Amount of items: 2
Items: 
Size: 622136 Color: 0
Size: 377849 Color: 1

Bin 1141: 16 of cap free
Amount of items: 2
Items: 
Size: 707742 Color: 1
Size: 292243 Color: 0

Bin 1142: 16 of cap free
Amount of items: 2
Items: 
Size: 710654 Color: 1
Size: 289331 Color: 0

Bin 1143: 16 of cap free
Amount of items: 2
Items: 
Size: 751096 Color: 1
Size: 248889 Color: 0

Bin 1144: 16 of cap free
Amount of items: 2
Items: 
Size: 775221 Color: 1
Size: 224764 Color: 0

Bin 1145: 16 of cap free
Amount of items: 2
Items: 
Size: 779047 Color: 1
Size: 220938 Color: 0

Bin 1146: 16 of cap free
Amount of items: 2
Items: 
Size: 792443 Color: 0
Size: 207542 Color: 1

Bin 1147: 16 of cap free
Amount of items: 2
Items: 
Size: 798731 Color: 0
Size: 201254 Color: 1

Bin 1148: 16 of cap free
Amount of items: 3
Items: 
Size: 391737 Color: 0
Size: 367168 Color: 1
Size: 241080 Color: 1

Bin 1149: 16 of cap free
Amount of items: 5
Items: 
Size: 290555 Color: 0
Size: 177395 Color: 1
Size: 177382 Color: 0
Size: 177339 Color: 0
Size: 177314 Color: 1

Bin 1150: 16 of cap free
Amount of items: 3
Items: 
Size: 768456 Color: 1
Size: 120368 Color: 0
Size: 111161 Color: 0

Bin 1151: 16 of cap free
Amount of items: 5
Items: 
Size: 246210 Color: 0
Size: 199828 Color: 1
Size: 199514 Color: 1
Size: 181178 Color: 0
Size: 173255 Color: 0

Bin 1152: 17 of cap free
Amount of items: 2
Items: 
Size: 510731 Color: 0
Size: 489253 Color: 1

Bin 1153: 17 of cap free
Amount of items: 2
Items: 
Size: 515104 Color: 1
Size: 484880 Color: 0

Bin 1154: 17 of cap free
Amount of items: 2
Items: 
Size: 516031 Color: 1
Size: 483953 Color: 0

Bin 1155: 17 of cap free
Amount of items: 2
Items: 
Size: 518420 Color: 0
Size: 481564 Color: 1

Bin 1156: 17 of cap free
Amount of items: 3
Items: 
Size: 553213 Color: 0
Size: 284426 Color: 1
Size: 162345 Color: 1

Bin 1157: 17 of cap free
Amount of items: 2
Items: 
Size: 570796 Color: 1
Size: 429188 Color: 0

Bin 1158: 17 of cap free
Amount of items: 2
Items: 
Size: 580374 Color: 1
Size: 419610 Color: 0

Bin 1159: 17 of cap free
Amount of items: 2
Items: 
Size: 588811 Color: 1
Size: 411173 Color: 0

Bin 1160: 17 of cap free
Amount of items: 2
Items: 
Size: 599297 Color: 0
Size: 400687 Color: 1

Bin 1161: 17 of cap free
Amount of items: 2
Items: 
Size: 600717 Color: 1
Size: 399267 Color: 0

Bin 1162: 17 of cap free
Amount of items: 2
Items: 
Size: 621999 Color: 0
Size: 377985 Color: 1

Bin 1163: 17 of cap free
Amount of items: 2
Items: 
Size: 653368 Color: 0
Size: 346616 Color: 1

Bin 1164: 17 of cap free
Amount of items: 2
Items: 
Size: 653699 Color: 0
Size: 346285 Color: 1

Bin 1165: 17 of cap free
Amount of items: 2
Items: 
Size: 659118 Color: 0
Size: 340866 Color: 1

Bin 1166: 17 of cap free
Amount of items: 2
Items: 
Size: 667079 Color: 0
Size: 332905 Color: 1

Bin 1167: 17 of cap free
Amount of items: 2
Items: 
Size: 683106 Color: 1
Size: 316878 Color: 0

Bin 1168: 17 of cap free
Amount of items: 2
Items: 
Size: 736531 Color: 1
Size: 263453 Color: 0

Bin 1169: 17 of cap free
Amount of items: 2
Items: 
Size: 773271 Color: 0
Size: 226713 Color: 1

Bin 1170: 17 of cap free
Amount of items: 2
Items: 
Size: 795287 Color: 1
Size: 204697 Color: 0

Bin 1171: 17 of cap free
Amount of items: 3
Items: 
Size: 575689 Color: 0
Size: 316837 Color: 1
Size: 107458 Color: 1

Bin 1172: 17 of cap free
Amount of items: 2
Items: 
Size: 562191 Color: 1
Size: 437793 Color: 0

Bin 1173: 17 of cap free
Amount of items: 2
Items: 
Size: 582564 Color: 0
Size: 417420 Color: 1

Bin 1174: 17 of cap free
Amount of items: 2
Items: 
Size: 617376 Color: 1
Size: 382608 Color: 0

Bin 1175: 17 of cap free
Amount of items: 5
Items: 
Size: 263059 Color: 1
Size: 184719 Color: 0
Size: 184710 Color: 0
Size: 183918 Color: 1
Size: 183578 Color: 1

Bin 1176: 17 of cap free
Amount of items: 5
Items: 
Size: 275655 Color: 1
Size: 181832 Color: 0
Size: 181235 Color: 0
Size: 180750 Color: 1
Size: 180512 Color: 1

Bin 1177: 18 of cap free
Amount of items: 2
Items: 
Size: 705859 Color: 0
Size: 294124 Color: 1

Bin 1178: 18 of cap free
Amount of items: 2
Items: 
Size: 522979 Color: 0
Size: 477004 Color: 1

Bin 1179: 18 of cap free
Amount of items: 2
Items: 
Size: 529226 Color: 1
Size: 470757 Color: 0

Bin 1180: 18 of cap free
Amount of items: 2
Items: 
Size: 576655 Color: 0
Size: 423328 Color: 1

Bin 1181: 18 of cap free
Amount of items: 2
Items: 
Size: 576895 Color: 1
Size: 423088 Color: 0

Bin 1182: 18 of cap free
Amount of items: 2
Items: 
Size: 576898 Color: 0
Size: 423085 Color: 1

Bin 1183: 18 of cap free
Amount of items: 2
Items: 
Size: 578310 Color: 0
Size: 421673 Color: 1

Bin 1184: 18 of cap free
Amount of items: 2
Items: 
Size: 621547 Color: 0
Size: 378436 Color: 1

Bin 1185: 18 of cap free
Amount of items: 2
Items: 
Size: 621677 Color: 1
Size: 378306 Color: 0

Bin 1186: 18 of cap free
Amount of items: 2
Items: 
Size: 627581 Color: 1
Size: 372402 Color: 0

Bin 1187: 18 of cap free
Amount of items: 2
Items: 
Size: 630014 Color: 1
Size: 369969 Color: 0

Bin 1188: 18 of cap free
Amount of items: 2
Items: 
Size: 640865 Color: 1
Size: 359118 Color: 0

Bin 1189: 18 of cap free
Amount of items: 2
Items: 
Size: 669130 Color: 1
Size: 330853 Color: 0

Bin 1190: 18 of cap free
Amount of items: 2
Items: 
Size: 676764 Color: 1
Size: 323219 Color: 0

Bin 1191: 18 of cap free
Amount of items: 2
Items: 
Size: 677898 Color: 0
Size: 322085 Color: 1

Bin 1192: 18 of cap free
Amount of items: 2
Items: 
Size: 688940 Color: 0
Size: 311043 Color: 1

Bin 1193: 18 of cap free
Amount of items: 2
Items: 
Size: 692596 Color: 1
Size: 307387 Color: 0

Bin 1194: 18 of cap free
Amount of items: 2
Items: 
Size: 708590 Color: 0
Size: 291393 Color: 1

Bin 1195: 18 of cap free
Amount of items: 2
Items: 
Size: 712852 Color: 1
Size: 287131 Color: 0

Bin 1196: 18 of cap free
Amount of items: 2
Items: 
Size: 730794 Color: 1
Size: 269189 Color: 0

Bin 1197: 18 of cap free
Amount of items: 2
Items: 
Size: 731070 Color: 1
Size: 268913 Color: 0

Bin 1198: 18 of cap free
Amount of items: 2
Items: 
Size: 747823 Color: 1
Size: 252160 Color: 0

Bin 1199: 18 of cap free
Amount of items: 2
Items: 
Size: 777439 Color: 0
Size: 222544 Color: 1

Bin 1200: 18 of cap free
Amount of items: 2
Items: 
Size: 780961 Color: 1
Size: 219022 Color: 0

Bin 1201: 18 of cap free
Amount of items: 2
Items: 
Size: 523391 Color: 1
Size: 476592 Color: 0

Bin 1202: 18 of cap free
Amount of items: 2
Items: 
Size: 664382 Color: 1
Size: 335601 Color: 0

Bin 1203: 19 of cap free
Amount of items: 2
Items: 
Size: 786326 Color: 1
Size: 213656 Color: 0

Bin 1204: 19 of cap free
Amount of items: 3
Items: 
Size: 611238 Color: 1
Size: 216761 Color: 0
Size: 171983 Color: 1

Bin 1205: 19 of cap free
Amount of items: 2
Items: 
Size: 505528 Color: 1
Size: 494454 Color: 0

Bin 1206: 19 of cap free
Amount of items: 2
Items: 
Size: 524543 Color: 0
Size: 475439 Color: 1

Bin 1207: 19 of cap free
Amount of items: 2
Items: 
Size: 536045 Color: 1
Size: 463937 Color: 0

Bin 1208: 19 of cap free
Amount of items: 2
Items: 
Size: 547291 Color: 0
Size: 452691 Color: 1

Bin 1209: 19 of cap free
Amount of items: 2
Items: 
Size: 547708 Color: 0
Size: 452274 Color: 1

Bin 1210: 19 of cap free
Amount of items: 2
Items: 
Size: 548715 Color: 0
Size: 451267 Color: 1

Bin 1211: 19 of cap free
Amount of items: 2
Items: 
Size: 591474 Color: 0
Size: 408508 Color: 1

Bin 1212: 19 of cap free
Amount of items: 2
Items: 
Size: 604177 Color: 0
Size: 395805 Color: 1

Bin 1213: 19 of cap free
Amount of items: 2
Items: 
Size: 618738 Color: 0
Size: 381244 Color: 1

Bin 1214: 19 of cap free
Amount of items: 2
Items: 
Size: 627857 Color: 1
Size: 372125 Color: 0

Bin 1215: 19 of cap free
Amount of items: 2
Items: 
Size: 637129 Color: 1
Size: 362853 Color: 0

Bin 1216: 19 of cap free
Amount of items: 2
Items: 
Size: 641542 Color: 1
Size: 358440 Color: 0

Bin 1217: 19 of cap free
Amount of items: 2
Items: 
Size: 656315 Color: 1
Size: 343667 Color: 0

Bin 1218: 19 of cap free
Amount of items: 2
Items: 
Size: 660807 Color: 0
Size: 339175 Color: 1

Bin 1219: 19 of cap free
Amount of items: 2
Items: 
Size: 703928 Color: 1
Size: 296054 Color: 0

Bin 1220: 19 of cap free
Amount of items: 2
Items: 
Size: 714481 Color: 1
Size: 285501 Color: 0

Bin 1221: 19 of cap free
Amount of items: 2
Items: 
Size: 734422 Color: 1
Size: 265560 Color: 0

Bin 1222: 19 of cap free
Amount of items: 2
Items: 
Size: 768272 Color: 0
Size: 231710 Color: 1

Bin 1223: 19 of cap free
Amount of items: 2
Items: 
Size: 784691 Color: 1
Size: 215291 Color: 0

Bin 1224: 19 of cap free
Amount of items: 2
Items: 
Size: 787890 Color: 0
Size: 212092 Color: 1

Bin 1225: 19 of cap free
Amount of items: 3
Items: 
Size: 374969 Color: 0
Size: 336324 Color: 1
Size: 288689 Color: 0

Bin 1226: 19 of cap free
Amount of items: 5
Items: 
Size: 260735 Color: 0
Size: 192338 Color: 1
Size: 192080 Color: 1
Size: 192072 Color: 1
Size: 162757 Color: 0

Bin 1227: 19 of cap free
Amount of items: 5
Items: 
Size: 247782 Color: 0
Size: 188997 Color: 1
Size: 188613 Color: 1
Size: 187342 Color: 0
Size: 187248 Color: 0

Bin 1228: 20 of cap free
Amount of items: 2
Items: 
Size: 512121 Color: 1
Size: 487860 Color: 0

Bin 1229: 20 of cap free
Amount of items: 3
Items: 
Size: 457064 Color: 1
Size: 439899 Color: 0
Size: 103018 Color: 1

Bin 1230: 20 of cap free
Amount of items: 3
Items: 
Size: 407237 Color: 1
Size: 330199 Color: 0
Size: 262545 Color: 1

Bin 1231: 20 of cap free
Amount of items: 3
Items: 
Size: 443650 Color: 0
Size: 283192 Color: 1
Size: 273139 Color: 1

Bin 1232: 20 of cap free
Amount of items: 2
Items: 
Size: 501551 Color: 1
Size: 498430 Color: 0

Bin 1233: 20 of cap free
Amount of items: 2
Items: 
Size: 508234 Color: 1
Size: 491747 Color: 0

Bin 1234: 20 of cap free
Amount of items: 2
Items: 
Size: 510311 Color: 0
Size: 489670 Color: 1

Bin 1235: 20 of cap free
Amount of items: 2
Items: 
Size: 535824 Color: 0
Size: 464157 Color: 1

Bin 1236: 20 of cap free
Amount of items: 2
Items: 
Size: 549758 Color: 1
Size: 450223 Color: 0

Bin 1237: 20 of cap free
Amount of items: 2
Items: 
Size: 574904 Color: 0
Size: 425077 Color: 1

Bin 1238: 20 of cap free
Amount of items: 2
Items: 
Size: 579349 Color: 0
Size: 420632 Color: 1

Bin 1239: 20 of cap free
Amount of items: 2
Items: 
Size: 587702 Color: 1
Size: 412279 Color: 0

Bin 1240: 20 of cap free
Amount of items: 2
Items: 
Size: 598374 Color: 1
Size: 401607 Color: 0

Bin 1241: 20 of cap free
Amount of items: 2
Items: 
Size: 614347 Color: 1
Size: 385634 Color: 0

Bin 1242: 20 of cap free
Amount of items: 2
Items: 
Size: 636230 Color: 0
Size: 363751 Color: 1

Bin 1243: 20 of cap free
Amount of items: 2
Items: 
Size: 669861 Color: 1
Size: 330120 Color: 0

Bin 1244: 20 of cap free
Amount of items: 2
Items: 
Size: 670800 Color: 1
Size: 329181 Color: 0

Bin 1245: 20 of cap free
Amount of items: 2
Items: 
Size: 697934 Color: 1
Size: 302047 Color: 0

Bin 1246: 20 of cap free
Amount of items: 2
Items: 
Size: 710865 Color: 1
Size: 289116 Color: 0

Bin 1247: 20 of cap free
Amount of items: 2
Items: 
Size: 749277 Color: 0
Size: 250704 Color: 1

Bin 1248: 20 of cap free
Amount of items: 2
Items: 
Size: 763592 Color: 1
Size: 236389 Color: 0

Bin 1249: 20 of cap free
Amount of items: 6
Items: 
Size: 171963 Color: 0
Size: 171885 Color: 0
Size: 169363 Color: 1
Size: 169087 Color: 1
Size: 168911 Color: 1
Size: 148772 Color: 0

Bin 1250: 20 of cap free
Amount of items: 2
Items: 
Size: 539004 Color: 0
Size: 460977 Color: 1

Bin 1251: 21 of cap free
Amount of items: 2
Items: 
Size: 573913 Color: 1
Size: 426067 Color: 0

Bin 1252: 21 of cap free
Amount of items: 2
Items: 
Size: 576719 Color: 1
Size: 423261 Color: 0

Bin 1253: 21 of cap free
Amount of items: 2
Items: 
Size: 588207 Color: 1
Size: 411773 Color: 0

Bin 1254: 21 of cap free
Amount of items: 2
Items: 
Size: 592129 Color: 0
Size: 407851 Color: 1

Bin 1255: 21 of cap free
Amount of items: 2
Items: 
Size: 594641 Color: 1
Size: 405339 Color: 0

Bin 1256: 21 of cap free
Amount of items: 2
Items: 
Size: 596039 Color: 1
Size: 403941 Color: 0

Bin 1257: 21 of cap free
Amount of items: 2
Items: 
Size: 596929 Color: 1
Size: 403051 Color: 0

Bin 1258: 21 of cap free
Amount of items: 2
Items: 
Size: 597685 Color: 0
Size: 402295 Color: 1

Bin 1259: 21 of cap free
Amount of items: 2
Items: 
Size: 602507 Color: 0
Size: 397473 Color: 1

Bin 1260: 21 of cap free
Amount of items: 2
Items: 
Size: 608998 Color: 1
Size: 390982 Color: 0

Bin 1261: 21 of cap free
Amount of items: 2
Items: 
Size: 651379 Color: 1
Size: 348601 Color: 0

Bin 1262: 21 of cap free
Amount of items: 2
Items: 
Size: 661016 Color: 0
Size: 338964 Color: 1

Bin 1263: 21 of cap free
Amount of items: 2
Items: 
Size: 664807 Color: 0
Size: 335173 Color: 1

Bin 1264: 21 of cap free
Amount of items: 2
Items: 
Size: 673427 Color: 1
Size: 326553 Color: 0

Bin 1265: 21 of cap free
Amount of items: 2
Items: 
Size: 675925 Color: 0
Size: 324055 Color: 1

Bin 1266: 21 of cap free
Amount of items: 2
Items: 
Size: 680126 Color: 0
Size: 319854 Color: 1

Bin 1267: 21 of cap free
Amount of items: 2
Items: 
Size: 699717 Color: 0
Size: 300263 Color: 1

Bin 1268: 21 of cap free
Amount of items: 2
Items: 
Size: 714914 Color: 0
Size: 285066 Color: 1

Bin 1269: 21 of cap free
Amount of items: 2
Items: 
Size: 741389 Color: 1
Size: 258591 Color: 0

Bin 1270: 21 of cap free
Amount of items: 2
Items: 
Size: 742349 Color: 0
Size: 257631 Color: 1

Bin 1271: 21 of cap free
Amount of items: 2
Items: 
Size: 743776 Color: 0
Size: 256204 Color: 1

Bin 1272: 21 of cap free
Amount of items: 2
Items: 
Size: 750130 Color: 1
Size: 249850 Color: 0

Bin 1273: 21 of cap free
Amount of items: 2
Items: 
Size: 794285 Color: 0
Size: 205695 Color: 1

Bin 1274: 21 of cap free
Amount of items: 2
Items: 
Size: 796243 Color: 1
Size: 203737 Color: 0

Bin 1275: 21 of cap free
Amount of items: 2
Items: 
Size: 764528 Color: 0
Size: 235452 Color: 1

Bin 1276: 21 of cap free
Amount of items: 3
Items: 
Size: 375307 Color: 1
Size: 312896 Color: 0
Size: 311777 Color: 0

Bin 1277: 21 of cap free
Amount of items: 5
Items: 
Size: 291688 Color: 1
Size: 178123 Color: 0
Size: 177183 Color: 1
Size: 176931 Color: 0
Size: 176055 Color: 1

Bin 1278: 21 of cap free
Amount of items: 6
Items: 
Size: 185508 Color: 0
Size: 163224 Color: 0
Size: 163063 Color: 1
Size: 162914 Color: 0
Size: 162784 Color: 1
Size: 162487 Color: 1

Bin 1279: 21 of cap free
Amount of items: 5
Items: 
Size: 238424 Color: 1
Size: 192734 Color: 1
Size: 189640 Color: 0
Size: 189599 Color: 0
Size: 189583 Color: 0

Bin 1280: 22 of cap free
Amount of items: 3
Items: 
Size: 699588 Color: 0
Size: 160615 Color: 0
Size: 139776 Color: 1

Bin 1281: 22 of cap free
Amount of items: 2
Items: 
Size: 505268 Color: 0
Size: 494711 Color: 1

Bin 1282: 22 of cap free
Amount of items: 2
Items: 
Size: 525825 Color: 0
Size: 474154 Color: 1

Bin 1283: 22 of cap free
Amount of items: 2
Items: 
Size: 536598 Color: 0
Size: 463381 Color: 1

Bin 1284: 22 of cap free
Amount of items: 2
Items: 
Size: 537774 Color: 1
Size: 462205 Color: 0

Bin 1285: 22 of cap free
Amount of items: 2
Items: 
Size: 556474 Color: 0
Size: 443505 Color: 1

Bin 1286: 22 of cap free
Amount of items: 2
Items: 
Size: 568007 Color: 0
Size: 431972 Color: 1

Bin 1287: 22 of cap free
Amount of items: 2
Items: 
Size: 588172 Color: 1
Size: 411807 Color: 0

Bin 1288: 22 of cap free
Amount of items: 2
Items: 
Size: 617079 Color: 0
Size: 382900 Color: 1

Bin 1289: 22 of cap free
Amount of items: 2
Items: 
Size: 622597 Color: 0
Size: 377382 Color: 1

Bin 1290: 22 of cap free
Amount of items: 2
Items: 
Size: 626741 Color: 1
Size: 373238 Color: 0

Bin 1291: 22 of cap free
Amount of items: 2
Items: 
Size: 640239 Color: 1
Size: 359740 Color: 0

Bin 1292: 22 of cap free
Amount of items: 2
Items: 
Size: 644201 Color: 1
Size: 355778 Color: 0

Bin 1293: 22 of cap free
Amount of items: 2
Items: 
Size: 658548 Color: 1
Size: 341431 Color: 0

Bin 1294: 22 of cap free
Amount of items: 2
Items: 
Size: 687633 Color: 1
Size: 312346 Color: 0

Bin 1295: 22 of cap free
Amount of items: 2
Items: 
Size: 692163 Color: 1
Size: 307816 Color: 0

Bin 1296: 22 of cap free
Amount of items: 2
Items: 
Size: 713532 Color: 0
Size: 286447 Color: 1

Bin 1297: 22 of cap free
Amount of items: 2
Items: 
Size: 741448 Color: 1
Size: 258531 Color: 0

Bin 1298: 22 of cap free
Amount of items: 2
Items: 
Size: 772083 Color: 0
Size: 227896 Color: 1

Bin 1299: 22 of cap free
Amount of items: 2
Items: 
Size: 753161 Color: 1
Size: 246818 Color: 0

Bin 1300: 22 of cap free
Amount of items: 2
Items: 
Size: 711672 Color: 1
Size: 288307 Color: 0

Bin 1301: 23 of cap free
Amount of items: 2
Items: 
Size: 518139 Color: 0
Size: 481839 Color: 1

Bin 1302: 23 of cap free
Amount of items: 2
Items: 
Size: 508678 Color: 1
Size: 491300 Color: 0

Bin 1303: 23 of cap free
Amount of items: 2
Items: 
Size: 514922 Color: 0
Size: 485056 Color: 1

Bin 1304: 23 of cap free
Amount of items: 2
Items: 
Size: 521771 Color: 0
Size: 478207 Color: 1

Bin 1305: 23 of cap free
Amount of items: 2
Items: 
Size: 558323 Color: 1
Size: 441655 Color: 0

Bin 1306: 23 of cap free
Amount of items: 2
Items: 
Size: 562708 Color: 1
Size: 437270 Color: 0

Bin 1307: 23 of cap free
Amount of items: 2
Items: 
Size: 564820 Color: 1
Size: 435158 Color: 0

Bin 1308: 23 of cap free
Amount of items: 2
Items: 
Size: 570102 Color: 1
Size: 429876 Color: 0

Bin 1309: 23 of cap free
Amount of items: 2
Items: 
Size: 572779 Color: 1
Size: 427199 Color: 0

Bin 1310: 23 of cap free
Amount of items: 2
Items: 
Size: 573305 Color: 1
Size: 426673 Color: 0

Bin 1311: 23 of cap free
Amount of items: 2
Items: 
Size: 590030 Color: 1
Size: 409948 Color: 0

Bin 1312: 23 of cap free
Amount of items: 2
Items: 
Size: 598836 Color: 1
Size: 401142 Color: 0

Bin 1313: 23 of cap free
Amount of items: 2
Items: 
Size: 601817 Color: 0
Size: 398161 Color: 1

Bin 1314: 23 of cap free
Amount of items: 2
Items: 
Size: 602230 Color: 1
Size: 397748 Color: 0

Bin 1315: 23 of cap free
Amount of items: 2
Items: 
Size: 604801 Color: 0
Size: 395177 Color: 1

Bin 1316: 23 of cap free
Amount of items: 2
Items: 
Size: 614622 Color: 1
Size: 385356 Color: 0

Bin 1317: 23 of cap free
Amount of items: 2
Items: 
Size: 619851 Color: 1
Size: 380127 Color: 0

Bin 1318: 23 of cap free
Amount of items: 2
Items: 
Size: 624159 Color: 0
Size: 375819 Color: 1

Bin 1319: 23 of cap free
Amount of items: 2
Items: 
Size: 669354 Color: 0
Size: 330624 Color: 1

Bin 1320: 23 of cap free
Amount of items: 2
Items: 
Size: 693464 Color: 0
Size: 306514 Color: 1

Bin 1321: 23 of cap free
Amount of items: 2
Items: 
Size: 708020 Color: 0
Size: 291958 Color: 1

Bin 1322: 23 of cap free
Amount of items: 2
Items: 
Size: 769700 Color: 1
Size: 230278 Color: 0

Bin 1323: 23 of cap free
Amount of items: 2
Items: 
Size: 780090 Color: 1
Size: 219888 Color: 0

Bin 1324: 23 of cap free
Amount of items: 2
Items: 
Size: 797288 Color: 1
Size: 202690 Color: 0

Bin 1325: 23 of cap free
Amount of items: 3
Items: 
Size: 397609 Color: 1
Size: 360575 Color: 1
Size: 241794 Color: 0

Bin 1326: 23 of cap free
Amount of items: 5
Items: 
Size: 219218 Color: 1
Size: 197686 Color: 1
Size: 196455 Color: 1
Size: 193948 Color: 0
Size: 192671 Color: 0

Bin 1327: 23 of cap free
Amount of items: 5
Items: 
Size: 268409 Color: 0
Size: 183830 Color: 0
Size: 183745 Color: 0
Size: 182118 Color: 1
Size: 181876 Color: 1

Bin 1328: 24 of cap free
Amount of items: 2
Items: 
Size: 586222 Color: 1
Size: 413755 Color: 0

Bin 1329: 24 of cap free
Amount of items: 2
Items: 
Size: 540967 Color: 1
Size: 459010 Color: 0

Bin 1330: 24 of cap free
Amount of items: 2
Items: 
Size: 572861 Color: 0
Size: 427116 Color: 1

Bin 1331: 24 of cap free
Amount of items: 2
Items: 
Size: 589232 Color: 0
Size: 410745 Color: 1

Bin 1332: 24 of cap free
Amount of items: 2
Items: 
Size: 597487 Color: 0
Size: 402490 Color: 1

Bin 1333: 24 of cap free
Amount of items: 2
Items: 
Size: 627059 Color: 1
Size: 372918 Color: 0

Bin 1334: 24 of cap free
Amount of items: 2
Items: 
Size: 634096 Color: 0
Size: 365881 Color: 1

Bin 1335: 24 of cap free
Amount of items: 2
Items: 
Size: 666228 Color: 1
Size: 333749 Color: 0

Bin 1336: 24 of cap free
Amount of items: 2
Items: 
Size: 678452 Color: 1
Size: 321525 Color: 0

Bin 1337: 24 of cap free
Amount of items: 2
Items: 
Size: 693665 Color: 0
Size: 306312 Color: 1

Bin 1338: 24 of cap free
Amount of items: 2
Items: 
Size: 716599 Color: 1
Size: 283378 Color: 0

Bin 1339: 24 of cap free
Amount of items: 2
Items: 
Size: 722447 Color: 0
Size: 277530 Color: 1

Bin 1340: 24 of cap free
Amount of items: 2
Items: 
Size: 730761 Color: 1
Size: 269216 Color: 0

Bin 1341: 24 of cap free
Amount of items: 2
Items: 
Size: 744263 Color: 0
Size: 255714 Color: 1

Bin 1342: 24 of cap free
Amount of items: 2
Items: 
Size: 770988 Color: 0
Size: 228989 Color: 1

Bin 1343: 24 of cap free
Amount of items: 2
Items: 
Size: 779584 Color: 1
Size: 220393 Color: 0

Bin 1344: 24 of cap free
Amount of items: 2
Items: 
Size: 783626 Color: 0
Size: 216351 Color: 1

Bin 1345: 24 of cap free
Amount of items: 5
Items: 
Size: 283460 Color: 0
Size: 182817 Color: 0
Size: 178446 Color: 1
Size: 177682 Color: 0
Size: 177572 Color: 1

Bin 1346: 24 of cap free
Amount of items: 3
Items: 
Size: 361361 Color: 1
Size: 335016 Color: 0
Size: 303600 Color: 0

Bin 1347: 24 of cap free
Amount of items: 3
Items: 
Size: 349250 Color: 0
Size: 341263 Color: 1
Size: 309464 Color: 1

Bin 1348: 24 of cap free
Amount of items: 2
Items: 
Size: 695074 Color: 1
Size: 304903 Color: 0

Bin 1349: 24 of cap free
Amount of items: 3
Items: 
Size: 770860 Color: 1
Size: 118782 Color: 0
Size: 110335 Color: 1

Bin 1350: 25 of cap free
Amount of items: 2
Items: 
Size: 792508 Color: 0
Size: 207468 Color: 1

Bin 1351: 25 of cap free
Amount of items: 2
Items: 
Size: 694679 Color: 1
Size: 305297 Color: 0

Bin 1352: 25 of cap free
Amount of items: 2
Items: 
Size: 511336 Color: 1
Size: 488640 Color: 0

Bin 1353: 25 of cap free
Amount of items: 2
Items: 
Size: 513958 Color: 1
Size: 486018 Color: 0

Bin 1354: 25 of cap free
Amount of items: 2
Items: 
Size: 549994 Color: 1
Size: 449982 Color: 0

Bin 1355: 25 of cap free
Amount of items: 2
Items: 
Size: 561613 Color: 0
Size: 438363 Color: 1

Bin 1356: 25 of cap free
Amount of items: 2
Items: 
Size: 566870 Color: 0
Size: 433106 Color: 1

Bin 1357: 25 of cap free
Amount of items: 2
Items: 
Size: 570063 Color: 1
Size: 429913 Color: 0

Bin 1358: 25 of cap free
Amount of items: 2
Items: 
Size: 580737 Color: 1
Size: 419239 Color: 0

Bin 1359: 25 of cap free
Amount of items: 2
Items: 
Size: 586397 Color: 0
Size: 413579 Color: 1

Bin 1360: 25 of cap free
Amount of items: 2
Items: 
Size: 599514 Color: 0
Size: 400462 Color: 1

Bin 1361: 25 of cap free
Amount of items: 2
Items: 
Size: 612255 Color: 1
Size: 387721 Color: 0

Bin 1362: 25 of cap free
Amount of items: 2
Items: 
Size: 649681 Color: 0
Size: 350295 Color: 1

Bin 1363: 25 of cap free
Amount of items: 2
Items: 
Size: 666059 Color: 1
Size: 333917 Color: 0

Bin 1364: 25 of cap free
Amount of items: 2
Items: 
Size: 683606 Color: 0
Size: 316370 Color: 1

Bin 1365: 25 of cap free
Amount of items: 2
Items: 
Size: 730908 Color: 1
Size: 269068 Color: 0

Bin 1366: 25 of cap free
Amount of items: 2
Items: 
Size: 744633 Color: 0
Size: 255343 Color: 1

Bin 1367: 25 of cap free
Amount of items: 2
Items: 
Size: 763493 Color: 0
Size: 236483 Color: 1

Bin 1368: 25 of cap free
Amount of items: 2
Items: 
Size: 768923 Color: 1
Size: 231053 Color: 0

Bin 1369: 25 of cap free
Amount of items: 2
Items: 
Size: 784652 Color: 1
Size: 215324 Color: 0

Bin 1370: 25 of cap free
Amount of items: 2
Items: 
Size: 701866 Color: 1
Size: 298110 Color: 0

Bin 1371: 25 of cap free
Amount of items: 5
Items: 
Size: 227791 Color: 1
Size: 195734 Color: 1
Size: 195199 Color: 1
Size: 190672 Color: 0
Size: 190580 Color: 0

Bin 1372: 25 of cap free
Amount of items: 5
Items: 
Size: 246265 Color: 0
Size: 189470 Color: 1
Size: 189100 Color: 1
Size: 187672 Color: 1
Size: 187469 Color: 0

Bin 1373: 26 of cap free
Amount of items: 2
Items: 
Size: 501421 Color: 0
Size: 498554 Color: 1

Bin 1374: 26 of cap free
Amount of items: 2
Items: 
Size: 503771 Color: 0
Size: 496204 Color: 1

Bin 1375: 26 of cap free
Amount of items: 2
Items: 
Size: 518028 Color: 1
Size: 481947 Color: 0

Bin 1376: 26 of cap free
Amount of items: 2
Items: 
Size: 527375 Color: 1
Size: 472600 Color: 0

Bin 1377: 26 of cap free
Amount of items: 2
Items: 
Size: 550790 Color: 1
Size: 449185 Color: 0

Bin 1378: 26 of cap free
Amount of items: 2
Items: 
Size: 567277 Color: 1
Size: 432698 Color: 0

Bin 1379: 26 of cap free
Amount of items: 2
Items: 
Size: 572055 Color: 1
Size: 427920 Color: 0

Bin 1380: 26 of cap free
Amount of items: 2
Items: 
Size: 577518 Color: 1
Size: 422457 Color: 0

Bin 1381: 26 of cap free
Amount of items: 2
Items: 
Size: 622871 Color: 0
Size: 377104 Color: 1

Bin 1382: 26 of cap free
Amount of items: 2
Items: 
Size: 633562 Color: 0
Size: 366413 Color: 1

Bin 1383: 26 of cap free
Amount of items: 2
Items: 
Size: 634295 Color: 1
Size: 365680 Color: 0

Bin 1384: 26 of cap free
Amount of items: 2
Items: 
Size: 647124 Color: 1
Size: 352851 Color: 0

Bin 1385: 26 of cap free
Amount of items: 2
Items: 
Size: 662284 Color: 1
Size: 337691 Color: 0

Bin 1386: 26 of cap free
Amount of items: 2
Items: 
Size: 680779 Color: 1
Size: 319196 Color: 0

Bin 1387: 26 of cap free
Amount of items: 2
Items: 
Size: 697436 Color: 1
Size: 302539 Color: 0

Bin 1388: 26 of cap free
Amount of items: 2
Items: 
Size: 722958 Color: 1
Size: 277017 Color: 0

Bin 1389: 26 of cap free
Amount of items: 2
Items: 
Size: 733935 Color: 0
Size: 266040 Color: 1

Bin 1390: 26 of cap free
Amount of items: 2
Items: 
Size: 744411 Color: 0
Size: 255564 Color: 1

Bin 1391: 26 of cap free
Amount of items: 2
Items: 
Size: 744804 Color: 0
Size: 255171 Color: 1

Bin 1392: 26 of cap free
Amount of items: 2
Items: 
Size: 765621 Color: 0
Size: 234354 Color: 1

Bin 1393: 26 of cap free
Amount of items: 2
Items: 
Size: 782364 Color: 1
Size: 217611 Color: 0

Bin 1394: 26 of cap free
Amount of items: 2
Items: 
Size: 792935 Color: 1
Size: 207040 Color: 0

Bin 1395: 26 of cap free
Amount of items: 2
Items: 
Size: 798195 Color: 1
Size: 201780 Color: 0

Bin 1396: 26 of cap free
Amount of items: 2
Items: 
Size: 788578 Color: 0
Size: 211397 Color: 1

Bin 1397: 27 of cap free
Amount of items: 2
Items: 
Size: 501486 Color: 0
Size: 498488 Color: 1

Bin 1398: 27 of cap free
Amount of items: 3
Items: 
Size: 413960 Color: 1
Size: 403117 Color: 0
Size: 182897 Color: 0

Bin 1399: 27 of cap free
Amount of items: 2
Items: 
Size: 510062 Color: 0
Size: 489912 Color: 1

Bin 1400: 27 of cap free
Amount of items: 2
Items: 
Size: 510049 Color: 1
Size: 489925 Color: 0

Bin 1401: 27 of cap free
Amount of items: 2
Items: 
Size: 541462 Color: 1
Size: 458512 Color: 0

Bin 1402: 27 of cap free
Amount of items: 2
Items: 
Size: 548844 Color: 1
Size: 451130 Color: 0

Bin 1403: 27 of cap free
Amount of items: 2
Items: 
Size: 567722 Color: 0
Size: 432252 Color: 1

Bin 1404: 27 of cap free
Amount of items: 2
Items: 
Size: 583524 Color: 1
Size: 416450 Color: 0

Bin 1405: 27 of cap free
Amount of items: 2
Items: 
Size: 591726 Color: 1
Size: 408248 Color: 0

Bin 1406: 27 of cap free
Amount of items: 2
Items: 
Size: 608419 Color: 0
Size: 391555 Color: 1

Bin 1407: 27 of cap free
Amount of items: 2
Items: 
Size: 629297 Color: 1
Size: 370677 Color: 0

Bin 1408: 27 of cap free
Amount of items: 2
Items: 
Size: 648493 Color: 0
Size: 351481 Color: 1

Bin 1409: 27 of cap free
Amount of items: 2
Items: 
Size: 665025 Color: 0
Size: 334949 Color: 1

Bin 1410: 27 of cap free
Amount of items: 2
Items: 
Size: 671293 Color: 0
Size: 328681 Color: 1

Bin 1411: 27 of cap free
Amount of items: 2
Items: 
Size: 697030 Color: 0
Size: 302944 Color: 1

Bin 1412: 27 of cap free
Amount of items: 2
Items: 
Size: 698591 Color: 0
Size: 301383 Color: 1

Bin 1413: 27 of cap free
Amount of items: 2
Items: 
Size: 711007 Color: 0
Size: 288967 Color: 1

Bin 1414: 27 of cap free
Amount of items: 2
Items: 
Size: 711426 Color: 1
Size: 288548 Color: 0

Bin 1415: 27 of cap free
Amount of items: 2
Items: 
Size: 714674 Color: 0
Size: 285300 Color: 1

Bin 1416: 27 of cap free
Amount of items: 2
Items: 
Size: 720105 Color: 0
Size: 279869 Color: 1

Bin 1417: 27 of cap free
Amount of items: 2
Items: 
Size: 731324 Color: 0
Size: 268650 Color: 1

Bin 1418: 27 of cap free
Amount of items: 2
Items: 
Size: 742235 Color: 0
Size: 257739 Color: 1

Bin 1419: 27 of cap free
Amount of items: 2
Items: 
Size: 750342 Color: 1
Size: 249632 Color: 0

Bin 1420: 27 of cap free
Amount of items: 2
Items: 
Size: 769117 Color: 0
Size: 230857 Color: 1

Bin 1421: 27 of cap free
Amount of items: 2
Items: 
Size: 777591 Color: 1
Size: 222383 Color: 0

Bin 1422: 28 of cap free
Amount of items: 2
Items: 
Size: 788924 Color: 1
Size: 211049 Color: 0

Bin 1423: 28 of cap free
Amount of items: 2
Items: 
Size: 503530 Color: 0
Size: 496443 Color: 1

Bin 1424: 28 of cap free
Amount of items: 2
Items: 
Size: 550676 Color: 0
Size: 449297 Color: 1

Bin 1425: 28 of cap free
Amount of items: 2
Items: 
Size: 591006 Color: 0
Size: 408967 Color: 1

Bin 1426: 28 of cap free
Amount of items: 2
Items: 
Size: 603553 Color: 0
Size: 396420 Color: 1

Bin 1427: 28 of cap free
Amount of items: 2
Items: 
Size: 604996 Color: 0
Size: 394977 Color: 1

Bin 1428: 28 of cap free
Amount of items: 2
Items: 
Size: 614045 Color: 0
Size: 385928 Color: 1

Bin 1429: 28 of cap free
Amount of items: 2
Items: 
Size: 648311 Color: 0
Size: 351662 Color: 1

Bin 1430: 28 of cap free
Amount of items: 2
Items: 
Size: 650472 Color: 0
Size: 349501 Color: 1

Bin 1431: 28 of cap free
Amount of items: 2
Items: 
Size: 681473 Color: 1
Size: 318500 Color: 0

Bin 1432: 28 of cap free
Amount of items: 2
Items: 
Size: 695263 Color: 0
Size: 304710 Color: 1

Bin 1433: 28 of cap free
Amount of items: 2
Items: 
Size: 705453 Color: 1
Size: 294520 Color: 0

Bin 1434: 28 of cap free
Amount of items: 2
Items: 
Size: 709383 Color: 1
Size: 290590 Color: 0

Bin 1435: 28 of cap free
Amount of items: 2
Items: 
Size: 712541 Color: 1
Size: 287432 Color: 0

Bin 1436: 28 of cap free
Amount of items: 2
Items: 
Size: 729423 Color: 0
Size: 270550 Color: 1

Bin 1437: 28 of cap free
Amount of items: 2
Items: 
Size: 740755 Color: 0
Size: 259218 Color: 1

Bin 1438: 28 of cap free
Amount of items: 2
Items: 
Size: 753887 Color: 1
Size: 246086 Color: 0

Bin 1439: 28 of cap free
Amount of items: 2
Items: 
Size: 770629 Color: 1
Size: 229344 Color: 0

Bin 1440: 28 of cap free
Amount of items: 2
Items: 
Size: 784364 Color: 1
Size: 215609 Color: 0

Bin 1441: 28 of cap free
Amount of items: 2
Items: 
Size: 797983 Color: 1
Size: 201990 Color: 0

Bin 1442: 28 of cap free
Amount of items: 2
Items: 
Size: 689244 Color: 0
Size: 310729 Color: 1

Bin 1443: 28 of cap free
Amount of items: 2
Items: 
Size: 779322 Color: 0
Size: 220651 Color: 1

Bin 1444: 29 of cap free
Amount of items: 2
Items: 
Size: 521234 Color: 1
Size: 478738 Color: 0

Bin 1445: 29 of cap free
Amount of items: 2
Items: 
Size: 531923 Color: 0
Size: 468049 Color: 1

Bin 1446: 29 of cap free
Amount of items: 2
Items: 
Size: 540839 Color: 1
Size: 459133 Color: 0

Bin 1447: 29 of cap free
Amount of items: 2
Items: 
Size: 556522 Color: 0
Size: 443450 Color: 1

Bin 1448: 29 of cap free
Amount of items: 2
Items: 
Size: 568141 Color: 0
Size: 431831 Color: 1

Bin 1449: 29 of cap free
Amount of items: 2
Items: 
Size: 585407 Color: 0
Size: 414565 Color: 1

Bin 1450: 29 of cap free
Amount of items: 2
Items: 
Size: 598492 Color: 1
Size: 401480 Color: 0

Bin 1451: 29 of cap free
Amount of items: 2
Items: 
Size: 608056 Color: 1
Size: 391916 Color: 0

Bin 1452: 29 of cap free
Amount of items: 2
Items: 
Size: 624074 Color: 1
Size: 375898 Color: 0

Bin 1453: 29 of cap free
Amount of items: 2
Items: 
Size: 628546 Color: 0
Size: 371426 Color: 1

Bin 1454: 29 of cap free
Amount of items: 2
Items: 
Size: 670022 Color: 1
Size: 329950 Color: 0

Bin 1455: 29 of cap free
Amount of items: 2
Items: 
Size: 670748 Color: 0
Size: 329224 Color: 1

Bin 1456: 29 of cap free
Amount of items: 2
Items: 
Size: 673701 Color: 0
Size: 326271 Color: 1

Bin 1457: 29 of cap free
Amount of items: 2
Items: 
Size: 691508 Color: 0
Size: 308464 Color: 1

Bin 1458: 29 of cap free
Amount of items: 2
Items: 
Size: 703567 Color: 0
Size: 296405 Color: 1

Bin 1459: 29 of cap free
Amount of items: 2
Items: 
Size: 746147 Color: 1
Size: 253825 Color: 0

Bin 1460: 29 of cap free
Amount of items: 2
Items: 
Size: 785622 Color: 1
Size: 214350 Color: 0

Bin 1461: 29 of cap free
Amount of items: 2
Items: 
Size: 529407 Color: 0
Size: 470565 Color: 1

Bin 1462: 29 of cap free
Amount of items: 2
Items: 
Size: 506415 Color: 0
Size: 493557 Color: 1

Bin 1463: 30 of cap free
Amount of items: 2
Items: 
Size: 505998 Color: 1
Size: 493973 Color: 0

Bin 1464: 30 of cap free
Amount of items: 2
Items: 
Size: 514551 Color: 0
Size: 485420 Color: 1

Bin 1465: 30 of cap free
Amount of items: 2
Items: 
Size: 517478 Color: 1
Size: 482493 Color: 0

Bin 1466: 30 of cap free
Amount of items: 2
Items: 
Size: 540013 Color: 0
Size: 459958 Color: 1

Bin 1467: 30 of cap free
Amount of items: 2
Items: 
Size: 544420 Color: 0
Size: 455551 Color: 1

Bin 1468: 30 of cap free
Amount of items: 2
Items: 
Size: 545972 Color: 1
Size: 453999 Color: 0

Bin 1469: 30 of cap free
Amount of items: 2
Items: 
Size: 599001 Color: 0
Size: 400970 Color: 1

Bin 1470: 30 of cap free
Amount of items: 2
Items: 
Size: 607422 Color: 1
Size: 392549 Color: 0

Bin 1471: 30 of cap free
Amount of items: 2
Items: 
Size: 621903 Color: 1
Size: 378068 Color: 0

Bin 1472: 30 of cap free
Amount of items: 2
Items: 
Size: 626183 Color: 0
Size: 373788 Color: 1

Bin 1473: 30 of cap free
Amount of items: 2
Items: 
Size: 628881 Color: 0
Size: 371090 Color: 1

Bin 1474: 30 of cap free
Amount of items: 2
Items: 
Size: 631617 Color: 0
Size: 368354 Color: 1

Bin 1475: 30 of cap free
Amount of items: 2
Items: 
Size: 634726 Color: 0
Size: 365245 Color: 1

Bin 1476: 30 of cap free
Amount of items: 2
Items: 
Size: 642334 Color: 0
Size: 357637 Color: 1

Bin 1477: 30 of cap free
Amount of items: 2
Items: 
Size: 643565 Color: 1
Size: 356406 Color: 0

Bin 1478: 30 of cap free
Amount of items: 2
Items: 
Size: 652174 Color: 1
Size: 347797 Color: 0

Bin 1479: 30 of cap free
Amount of items: 2
Items: 
Size: 653818 Color: 0
Size: 346153 Color: 1

Bin 1480: 30 of cap free
Amount of items: 2
Items: 
Size: 676720 Color: 1
Size: 323251 Color: 0

Bin 1481: 30 of cap free
Amount of items: 2
Items: 
Size: 698696 Color: 1
Size: 301275 Color: 0

Bin 1482: 30 of cap free
Amount of items: 2
Items: 
Size: 699795 Color: 1
Size: 300176 Color: 0

Bin 1483: 30 of cap free
Amount of items: 2
Items: 
Size: 718468 Color: 0
Size: 281503 Color: 1

Bin 1484: 30 of cap free
Amount of items: 2
Items: 
Size: 722444 Color: 0
Size: 277527 Color: 1

Bin 1485: 30 of cap free
Amount of items: 2
Items: 
Size: 747401 Color: 1
Size: 252570 Color: 0

Bin 1486: 30 of cap free
Amount of items: 2
Items: 
Size: 762765 Color: 1
Size: 237206 Color: 0

Bin 1487: 30 of cap free
Amount of items: 2
Items: 
Size: 791090 Color: 0
Size: 208881 Color: 1

Bin 1488: 31 of cap free
Amount of items: 3
Items: 
Size: 379047 Color: 0
Size: 310521 Color: 1
Size: 310402 Color: 1

Bin 1489: 31 of cap free
Amount of items: 2
Items: 
Size: 535452 Color: 1
Size: 464518 Color: 0

Bin 1490: 31 of cap free
Amount of items: 2
Items: 
Size: 551819 Color: 1
Size: 448151 Color: 0

Bin 1491: 31 of cap free
Amount of items: 2
Items: 
Size: 575360 Color: 0
Size: 424610 Color: 1

Bin 1492: 31 of cap free
Amount of items: 2
Items: 
Size: 579240 Color: 1
Size: 420730 Color: 0

Bin 1493: 31 of cap free
Amount of items: 2
Items: 
Size: 600960 Color: 0
Size: 399010 Color: 1

Bin 1494: 31 of cap free
Amount of items: 2
Items: 
Size: 610022 Color: 1
Size: 389948 Color: 0

Bin 1495: 31 of cap free
Amount of items: 2
Items: 
Size: 702295 Color: 0
Size: 297675 Color: 1

Bin 1496: 31 of cap free
Amount of items: 2
Items: 
Size: 714712 Color: 0
Size: 285258 Color: 1

Bin 1497: 31 of cap free
Amount of items: 2
Items: 
Size: 741173 Color: 0
Size: 258797 Color: 1

Bin 1498: 31 of cap free
Amount of items: 2
Items: 
Size: 750223 Color: 0
Size: 249747 Color: 1

Bin 1499: 31 of cap free
Amount of items: 2
Items: 
Size: 755388 Color: 1
Size: 244582 Color: 0

Bin 1500: 31 of cap free
Amount of items: 2
Items: 
Size: 761829 Color: 1
Size: 238141 Color: 0

Bin 1501: 31 of cap free
Amount of items: 2
Items: 
Size: 765007 Color: 1
Size: 234963 Color: 0

Bin 1502: 31 of cap free
Amount of items: 3
Items: 
Size: 375046 Color: 1
Size: 358043 Color: 0
Size: 266881 Color: 1

Bin 1503: 31 of cap free
Amount of items: 3
Items: 
Size: 586273 Color: 0
Size: 285216 Color: 1
Size: 128481 Color: 0

Bin 1504: 32 of cap free
Amount of items: 2
Items: 
Size: 534859 Color: 1
Size: 465110 Color: 0

Bin 1505: 32 of cap free
Amount of items: 2
Items: 
Size: 535077 Color: 0
Size: 464892 Color: 1

Bin 1506: 32 of cap free
Amount of items: 2
Items: 
Size: 539211 Color: 0
Size: 460758 Color: 1

Bin 1507: 32 of cap free
Amount of items: 2
Items: 
Size: 540876 Color: 1
Size: 459093 Color: 0

Bin 1508: 32 of cap free
Amount of items: 2
Items: 
Size: 547424 Color: 1
Size: 452545 Color: 0

Bin 1509: 32 of cap free
Amount of items: 2
Items: 
Size: 556792 Color: 0
Size: 443177 Color: 1

Bin 1510: 32 of cap free
Amount of items: 2
Items: 
Size: 556972 Color: 1
Size: 442997 Color: 0

Bin 1511: 32 of cap free
Amount of items: 2
Items: 
Size: 563227 Color: 1
Size: 436742 Color: 0

Bin 1512: 32 of cap free
Amount of items: 2
Items: 
Size: 573760 Color: 1
Size: 426209 Color: 0

Bin 1513: 32 of cap free
Amount of items: 2
Items: 
Size: 583400 Color: 1
Size: 416569 Color: 0

Bin 1514: 32 of cap free
Amount of items: 2
Items: 
Size: 587019 Color: 0
Size: 412950 Color: 1

Bin 1515: 32 of cap free
Amount of items: 2
Items: 
Size: 601242 Color: 0
Size: 398727 Color: 1

Bin 1516: 32 of cap free
Amount of items: 2
Items: 
Size: 631554 Color: 0
Size: 368415 Color: 1

Bin 1517: 32 of cap free
Amount of items: 2
Items: 
Size: 659739 Color: 0
Size: 340230 Color: 1

Bin 1518: 32 of cap free
Amount of items: 2
Items: 
Size: 661109 Color: 0
Size: 338860 Color: 1

Bin 1519: 32 of cap free
Amount of items: 2
Items: 
Size: 673109 Color: 1
Size: 326860 Color: 0

Bin 1520: 32 of cap free
Amount of items: 2
Items: 
Size: 724177 Color: 0
Size: 275792 Color: 1

Bin 1521: 32 of cap free
Amount of items: 2
Items: 
Size: 726977 Color: 0
Size: 272992 Color: 1

Bin 1522: 32 of cap free
Amount of items: 2
Items: 
Size: 775406 Color: 1
Size: 224563 Color: 0

Bin 1523: 32 of cap free
Amount of items: 2
Items: 
Size: 631847 Color: 0
Size: 368122 Color: 1

Bin 1524: 33 of cap free
Amount of items: 2
Items: 
Size: 501314 Color: 0
Size: 498654 Color: 1

Bin 1525: 33 of cap free
Amount of items: 2
Items: 
Size: 509787 Color: 1
Size: 490181 Color: 0

Bin 1526: 33 of cap free
Amount of items: 2
Items: 
Size: 518131 Color: 0
Size: 481837 Color: 1

Bin 1527: 33 of cap free
Amount of items: 2
Items: 
Size: 525858 Color: 0
Size: 474110 Color: 1

Bin 1528: 33 of cap free
Amount of items: 2
Items: 
Size: 550849 Color: 1
Size: 449119 Color: 0

Bin 1529: 33 of cap free
Amount of items: 2
Items: 
Size: 584433 Color: 0
Size: 415535 Color: 1

Bin 1530: 33 of cap free
Amount of items: 2
Items: 
Size: 601649 Color: 1
Size: 398319 Color: 0

Bin 1531: 33 of cap free
Amount of items: 2
Items: 
Size: 605401 Color: 1
Size: 394567 Color: 0

Bin 1532: 33 of cap free
Amount of items: 2
Items: 
Size: 610678 Color: 0
Size: 389290 Color: 1

Bin 1533: 33 of cap free
Amount of items: 2
Items: 
Size: 618288 Color: 0
Size: 381680 Color: 1

Bin 1534: 33 of cap free
Amount of items: 2
Items: 
Size: 618377 Color: 1
Size: 381591 Color: 0

Bin 1535: 33 of cap free
Amount of items: 2
Items: 
Size: 687597 Color: 1
Size: 312371 Color: 0

Bin 1536: 33 of cap free
Amount of items: 2
Items: 
Size: 688245 Color: 0
Size: 311723 Color: 1

Bin 1537: 33 of cap free
Amount of items: 2
Items: 
Size: 714673 Color: 0
Size: 285295 Color: 1

Bin 1538: 33 of cap free
Amount of items: 2
Items: 
Size: 723361 Color: 0
Size: 276607 Color: 1

Bin 1539: 33 of cap free
Amount of items: 2
Items: 
Size: 725403 Color: 1
Size: 274565 Color: 0

Bin 1540: 33 of cap free
Amount of items: 2
Items: 
Size: 725802 Color: 1
Size: 274166 Color: 0

Bin 1541: 33 of cap free
Amount of items: 2
Items: 
Size: 765018 Color: 0
Size: 234950 Color: 1

Bin 1542: 33 of cap free
Amount of items: 2
Items: 
Size: 775289 Color: 0
Size: 224679 Color: 1

Bin 1543: 34 of cap free
Amount of items: 2
Items: 
Size: 513710 Color: 1
Size: 486257 Color: 0

Bin 1544: 34 of cap free
Amount of items: 2
Items: 
Size: 613564 Color: 0
Size: 386403 Color: 1

Bin 1545: 34 of cap free
Amount of items: 2
Items: 
Size: 614682 Color: 1
Size: 385285 Color: 0

Bin 1546: 34 of cap free
Amount of items: 2
Items: 
Size: 626937 Color: 0
Size: 373030 Color: 1

Bin 1547: 34 of cap free
Amount of items: 2
Items: 
Size: 654929 Color: 0
Size: 345038 Color: 1

Bin 1548: 34 of cap free
Amount of items: 2
Items: 
Size: 755709 Color: 0
Size: 244258 Color: 1

Bin 1549: 34 of cap free
Amount of items: 2
Items: 
Size: 771963 Color: 1
Size: 228004 Color: 0

Bin 1550: 34 of cap free
Amount of items: 2
Items: 
Size: 688665 Color: 0
Size: 311302 Color: 1

Bin 1551: 34 of cap free
Amount of items: 2
Items: 
Size: 758471 Color: 0
Size: 241496 Color: 1

Bin 1552: 34 of cap free
Amount of items: 2
Items: 
Size: 545765 Color: 1
Size: 454202 Color: 0

Bin 1553: 35 of cap free
Amount of items: 2
Items: 
Size: 500861 Color: 0
Size: 499105 Color: 1

Bin 1554: 35 of cap free
Amount of items: 2
Items: 
Size: 504028 Color: 0
Size: 495938 Color: 1

Bin 1555: 35 of cap free
Amount of items: 2
Items: 
Size: 505632 Color: 0
Size: 494334 Color: 1

Bin 1556: 35 of cap free
Amount of items: 2
Items: 
Size: 535656 Color: 1
Size: 464310 Color: 0

Bin 1557: 35 of cap free
Amount of items: 2
Items: 
Size: 539059 Color: 1
Size: 460907 Color: 0

Bin 1558: 35 of cap free
Amount of items: 2
Items: 
Size: 590230 Color: 0
Size: 409736 Color: 1

Bin 1559: 35 of cap free
Amount of items: 2
Items: 
Size: 599999 Color: 1
Size: 399967 Color: 0

Bin 1560: 35 of cap free
Amount of items: 2
Items: 
Size: 609672 Color: 1
Size: 390294 Color: 0

Bin 1561: 35 of cap free
Amount of items: 2
Items: 
Size: 615741 Color: 1
Size: 384225 Color: 0

Bin 1562: 35 of cap free
Amount of items: 2
Items: 
Size: 626890 Color: 1
Size: 373076 Color: 0

Bin 1563: 35 of cap free
Amount of items: 2
Items: 
Size: 718825 Color: 1
Size: 281141 Color: 0

Bin 1564: 35 of cap free
Amount of items: 2
Items: 
Size: 735976 Color: 0
Size: 263990 Color: 1

Bin 1565: 35 of cap free
Amount of items: 2
Items: 
Size: 741960 Color: 1
Size: 258006 Color: 0

Bin 1566: 35 of cap free
Amount of items: 2
Items: 
Size: 755080 Color: 0
Size: 244886 Color: 1

Bin 1567: 35 of cap free
Amount of items: 2
Items: 
Size: 794896 Color: 1
Size: 205070 Color: 0

Bin 1568: 35 of cap free
Amount of items: 2
Items: 
Size: 783182 Color: 1
Size: 216784 Color: 0

Bin 1569: 36 of cap free
Amount of items: 2
Items: 
Size: 504120 Color: 1
Size: 495845 Color: 0

Bin 1570: 36 of cap free
Amount of items: 2
Items: 
Size: 521540 Color: 1
Size: 478425 Color: 0

Bin 1571: 36 of cap free
Amount of items: 2
Items: 
Size: 535806 Color: 1
Size: 464159 Color: 0

Bin 1572: 36 of cap free
Amount of items: 2
Items: 
Size: 573937 Color: 0
Size: 426028 Color: 1

Bin 1573: 36 of cap free
Amount of items: 2
Items: 
Size: 575628 Color: 1
Size: 424337 Color: 0

Bin 1574: 36 of cap free
Amount of items: 2
Items: 
Size: 592537 Color: 0
Size: 407428 Color: 1

Bin 1575: 36 of cap free
Amount of items: 2
Items: 
Size: 600652 Color: 1
Size: 399313 Color: 0

Bin 1576: 36 of cap free
Amount of items: 2
Items: 
Size: 606753 Color: 0
Size: 393212 Color: 1

Bin 1577: 36 of cap free
Amount of items: 2
Items: 
Size: 614558 Color: 1
Size: 385407 Color: 0

Bin 1578: 36 of cap free
Amount of items: 2
Items: 
Size: 639039 Color: 0
Size: 360926 Color: 1

Bin 1579: 36 of cap free
Amount of items: 2
Items: 
Size: 671498 Color: 0
Size: 328467 Color: 1

Bin 1580: 36 of cap free
Amount of items: 2
Items: 
Size: 676996 Color: 0
Size: 322969 Color: 1

Bin 1581: 36 of cap free
Amount of items: 2
Items: 
Size: 689666 Color: 0
Size: 310299 Color: 1

Bin 1582: 36 of cap free
Amount of items: 2
Items: 
Size: 692273 Color: 0
Size: 307692 Color: 1

Bin 1583: 36 of cap free
Amount of items: 2
Items: 
Size: 756996 Color: 1
Size: 242969 Color: 0

Bin 1584: 36 of cap free
Amount of items: 2
Items: 
Size: 770041 Color: 0
Size: 229924 Color: 1

Bin 1585: 36 of cap free
Amount of items: 2
Items: 
Size: 522083 Color: 0
Size: 477882 Color: 1

Bin 1586: 36 of cap free
Amount of items: 2
Items: 
Size: 612142 Color: 1
Size: 387823 Color: 0

Bin 1587: 36 of cap free
Amount of items: 5
Items: 
Size: 299975 Color: 0
Size: 291126 Color: 0
Size: 142792 Color: 0
Size: 141072 Color: 1
Size: 125000 Color: 1

Bin 1588: 37 of cap free
Amount of items: 2
Items: 
Size: 506908 Color: 0
Size: 493056 Color: 1

Bin 1589: 37 of cap free
Amount of items: 2
Items: 
Size: 520540 Color: 1
Size: 479424 Color: 0

Bin 1590: 37 of cap free
Amount of items: 2
Items: 
Size: 545481 Color: 0
Size: 454483 Color: 1

Bin 1591: 37 of cap free
Amount of items: 2
Items: 
Size: 553589 Color: 1
Size: 446375 Color: 0

Bin 1592: 37 of cap free
Amount of items: 2
Items: 
Size: 580605 Color: 1
Size: 419359 Color: 0

Bin 1593: 37 of cap free
Amount of items: 2
Items: 
Size: 611552 Color: 1
Size: 388412 Color: 0

Bin 1594: 37 of cap free
Amount of items: 2
Items: 
Size: 612283 Color: 0
Size: 387681 Color: 1

Bin 1595: 37 of cap free
Amount of items: 2
Items: 
Size: 616831 Color: 1
Size: 383133 Color: 0

Bin 1596: 37 of cap free
Amount of items: 2
Items: 
Size: 617073 Color: 0
Size: 382891 Color: 1

Bin 1597: 37 of cap free
Amount of items: 2
Items: 
Size: 618297 Color: 1
Size: 381667 Color: 0

Bin 1598: 37 of cap free
Amount of items: 2
Items: 
Size: 622685 Color: 1
Size: 377279 Color: 0

Bin 1599: 37 of cap free
Amount of items: 2
Items: 
Size: 631341 Color: 1
Size: 368623 Color: 0

Bin 1600: 37 of cap free
Amount of items: 2
Items: 
Size: 650159 Color: 0
Size: 349805 Color: 1

Bin 1601: 37 of cap free
Amount of items: 2
Items: 
Size: 664101 Color: 0
Size: 335863 Color: 1

Bin 1602: 37 of cap free
Amount of items: 2
Items: 
Size: 673632 Color: 1
Size: 326332 Color: 0

Bin 1603: 37 of cap free
Amount of items: 2
Items: 
Size: 710271 Color: 0
Size: 289693 Color: 1

Bin 1604: 37 of cap free
Amount of items: 2
Items: 
Size: 712901 Color: 1
Size: 287063 Color: 0

Bin 1605: 37 of cap free
Amount of items: 2
Items: 
Size: 720843 Color: 0
Size: 279121 Color: 1

Bin 1606: 37 of cap free
Amount of items: 2
Items: 
Size: 744546 Color: 0
Size: 255418 Color: 1

Bin 1607: 37 of cap free
Amount of items: 2
Items: 
Size: 747240 Color: 0
Size: 252724 Color: 1

Bin 1608: 37 of cap free
Amount of items: 2
Items: 
Size: 754887 Color: 0
Size: 245077 Color: 1

Bin 1609: 37 of cap free
Amount of items: 2
Items: 
Size: 771337 Color: 0
Size: 228627 Color: 1

Bin 1610: 37 of cap free
Amount of items: 2
Items: 
Size: 794299 Color: 1
Size: 205665 Color: 0

Bin 1611: 37 of cap free
Amount of items: 2
Items: 
Size: 797509 Color: 1
Size: 202455 Color: 0

Bin 1612: 37 of cap free
Amount of items: 2
Items: 
Size: 524878 Color: 0
Size: 475086 Color: 1

Bin 1613: 37 of cap free
Amount of items: 2
Items: 
Size: 591760 Color: 0
Size: 408204 Color: 1

Bin 1614: 38 of cap free
Amount of items: 2
Items: 
Size: 508778 Color: 0
Size: 491185 Color: 1

Bin 1615: 38 of cap free
Amount of items: 2
Items: 
Size: 518418 Color: 0
Size: 481545 Color: 1

Bin 1616: 38 of cap free
Amount of items: 2
Items: 
Size: 518485 Color: 1
Size: 481478 Color: 0

Bin 1617: 38 of cap free
Amount of items: 2
Items: 
Size: 538978 Color: 1
Size: 460985 Color: 0

Bin 1618: 38 of cap free
Amount of items: 2
Items: 
Size: 542631 Color: 0
Size: 457332 Color: 1

Bin 1619: 38 of cap free
Amount of items: 2
Items: 
Size: 569093 Color: 0
Size: 430870 Color: 1

Bin 1620: 38 of cap free
Amount of items: 2
Items: 
Size: 588571 Color: 1
Size: 411392 Color: 0

Bin 1621: 38 of cap free
Amount of items: 2
Items: 
Size: 629472 Color: 1
Size: 370491 Color: 0

Bin 1622: 38 of cap free
Amount of items: 2
Items: 
Size: 664558 Color: 1
Size: 335405 Color: 0

Bin 1623: 38 of cap free
Amount of items: 2
Items: 
Size: 707991 Color: 1
Size: 291972 Color: 0

Bin 1624: 38 of cap free
Amount of items: 2
Items: 
Size: 767919 Color: 0
Size: 232044 Color: 1

Bin 1625: 38 of cap free
Amount of items: 2
Items: 
Size: 771856 Color: 1
Size: 228107 Color: 0

Bin 1626: 39 of cap free
Amount of items: 2
Items: 
Size: 508026 Color: 0
Size: 491936 Color: 1

Bin 1627: 39 of cap free
Amount of items: 2
Items: 
Size: 517422 Color: 0
Size: 482540 Color: 1

Bin 1628: 39 of cap free
Amount of items: 2
Items: 
Size: 525501 Color: 1
Size: 474461 Color: 0

Bin 1629: 39 of cap free
Amount of items: 2
Items: 
Size: 549396 Color: 0
Size: 450566 Color: 1

Bin 1630: 39 of cap free
Amount of items: 2
Items: 
Size: 558166 Color: 1
Size: 441796 Color: 0

Bin 1631: 39 of cap free
Amount of items: 2
Items: 
Size: 585580 Color: 0
Size: 414382 Color: 1

Bin 1632: 39 of cap free
Amount of items: 2
Items: 
Size: 601989 Color: 0
Size: 397973 Color: 1

Bin 1633: 39 of cap free
Amount of items: 2
Items: 
Size: 608557 Color: 1
Size: 391405 Color: 0

Bin 1634: 39 of cap free
Amount of items: 2
Items: 
Size: 612795 Color: 1
Size: 387167 Color: 0

Bin 1635: 39 of cap free
Amount of items: 2
Items: 
Size: 639043 Color: 1
Size: 360919 Color: 0

Bin 1636: 39 of cap free
Amount of items: 2
Items: 
Size: 644216 Color: 0
Size: 355746 Color: 1

Bin 1637: 39 of cap free
Amount of items: 2
Items: 
Size: 651921 Color: 0
Size: 348041 Color: 1

Bin 1638: 39 of cap free
Amount of items: 2
Items: 
Size: 658380 Color: 1
Size: 341582 Color: 0

Bin 1639: 39 of cap free
Amount of items: 2
Items: 
Size: 666491 Color: 0
Size: 333471 Color: 1

Bin 1640: 39 of cap free
Amount of items: 2
Items: 
Size: 690767 Color: 1
Size: 309195 Color: 0

Bin 1641: 39 of cap free
Amount of items: 2
Items: 
Size: 692071 Color: 0
Size: 307891 Color: 1

Bin 1642: 39 of cap free
Amount of items: 2
Items: 
Size: 695498 Color: 0
Size: 304464 Color: 1

Bin 1643: 39 of cap free
Amount of items: 2
Items: 
Size: 706042 Color: 0
Size: 293920 Color: 1

Bin 1644: 39 of cap free
Amount of items: 2
Items: 
Size: 728093 Color: 1
Size: 271869 Color: 0

Bin 1645: 39 of cap free
Amount of items: 2
Items: 
Size: 732052 Color: 1
Size: 267910 Color: 0

Bin 1646: 39 of cap free
Amount of items: 2
Items: 
Size: 746212 Color: 0
Size: 253750 Color: 1

Bin 1647: 39 of cap free
Amount of items: 2
Items: 
Size: 748464 Color: 1
Size: 251498 Color: 0

Bin 1648: 39 of cap free
Amount of items: 2
Items: 
Size: 749903 Color: 0
Size: 250059 Color: 1

Bin 1649: 39 of cap free
Amount of items: 2
Items: 
Size: 753504 Color: 1
Size: 246458 Color: 0

Bin 1650: 39 of cap free
Amount of items: 2
Items: 
Size: 770358 Color: 1
Size: 229604 Color: 0

Bin 1651: 39 of cap free
Amount of items: 2
Items: 
Size: 770873 Color: 0
Size: 229089 Color: 1

Bin 1652: 39 of cap free
Amount of items: 2
Items: 
Size: 779309 Color: 1
Size: 220653 Color: 0

Bin 1653: 40 of cap free
Amount of items: 2
Items: 
Size: 540474 Color: 1
Size: 459487 Color: 0

Bin 1654: 40 of cap free
Amount of items: 2
Items: 
Size: 543141 Color: 1
Size: 456820 Color: 0

Bin 1655: 40 of cap free
Amount of items: 2
Items: 
Size: 561193 Color: 0
Size: 438768 Color: 1

Bin 1656: 40 of cap free
Amount of items: 2
Items: 
Size: 565125 Color: 0
Size: 434836 Color: 1

Bin 1657: 40 of cap free
Amount of items: 2
Items: 
Size: 578740 Color: 0
Size: 421221 Color: 1

Bin 1658: 40 of cap free
Amount of items: 2
Items: 
Size: 612882 Color: 0
Size: 387079 Color: 1

Bin 1659: 40 of cap free
Amount of items: 2
Items: 
Size: 617274 Color: 1
Size: 382687 Color: 0

Bin 1660: 40 of cap free
Amount of items: 2
Items: 
Size: 670144 Color: 1
Size: 329817 Color: 0

Bin 1661: 40 of cap free
Amount of items: 2
Items: 
Size: 675780 Color: 1
Size: 324181 Color: 0

Bin 1662: 40 of cap free
Amount of items: 2
Items: 
Size: 679073 Color: 1
Size: 320888 Color: 0

Bin 1663: 40 of cap free
Amount of items: 2
Items: 
Size: 691384 Color: 0
Size: 308577 Color: 1

Bin 1664: 40 of cap free
Amount of items: 2
Items: 
Size: 712246 Color: 1
Size: 287715 Color: 0

Bin 1665: 40 of cap free
Amount of items: 2
Items: 
Size: 731097 Color: 1
Size: 268864 Color: 0

Bin 1666: 40 of cap free
Amount of items: 2
Items: 
Size: 735654 Color: 0
Size: 264307 Color: 1

Bin 1667: 40 of cap free
Amount of items: 2
Items: 
Size: 646175 Color: 1
Size: 353786 Color: 0

Bin 1668: 40 of cap free
Amount of items: 2
Items: 
Size: 656863 Color: 0
Size: 343098 Color: 1

Bin 1669: 40 of cap free
Amount of items: 2
Items: 
Size: 596985 Color: 0
Size: 402976 Color: 1

Bin 1670: 41 of cap free
Amount of items: 2
Items: 
Size: 528340 Color: 0
Size: 471620 Color: 1

Bin 1671: 41 of cap free
Amount of items: 2
Items: 
Size: 542752 Color: 0
Size: 457208 Color: 1

Bin 1672: 41 of cap free
Amount of items: 2
Items: 
Size: 548488 Color: 0
Size: 451472 Color: 1

Bin 1673: 41 of cap free
Amount of items: 2
Items: 
Size: 559321 Color: 1
Size: 440639 Color: 0

Bin 1674: 41 of cap free
Amount of items: 2
Items: 
Size: 592270 Color: 1
Size: 407690 Color: 0

Bin 1675: 41 of cap free
Amount of items: 2
Items: 
Size: 674471 Color: 0
Size: 325489 Color: 1

Bin 1676: 41 of cap free
Amount of items: 2
Items: 
Size: 679591 Color: 1
Size: 320369 Color: 0

Bin 1677: 41 of cap free
Amount of items: 2
Items: 
Size: 697520 Color: 0
Size: 302440 Color: 1

Bin 1678: 41 of cap free
Amount of items: 2
Items: 
Size: 701336 Color: 1
Size: 298624 Color: 0

Bin 1679: 41 of cap free
Amount of items: 2
Items: 
Size: 704808 Color: 0
Size: 295152 Color: 1

Bin 1680: 41 of cap free
Amount of items: 2
Items: 
Size: 709722 Color: 1
Size: 290238 Color: 0

Bin 1681: 41 of cap free
Amount of items: 2
Items: 
Size: 727902 Color: 0
Size: 272058 Color: 1

Bin 1682: 41 of cap free
Amount of items: 3
Items: 
Size: 367191 Color: 1
Size: 318017 Color: 1
Size: 314752 Color: 0

Bin 1683: 41 of cap free
Amount of items: 2
Items: 
Size: 745309 Color: 0
Size: 254651 Color: 1

Bin 1684: 41 of cap free
Amount of items: 2
Items: 
Size: 586869 Color: 0
Size: 413091 Color: 1

Bin 1685: 41 of cap free
Amount of items: 2
Items: 
Size: 543061 Color: 1
Size: 456899 Color: 0

Bin 1686: 41 of cap free
Amount of items: 2
Items: 
Size: 519039 Color: 1
Size: 480921 Color: 0

Bin 1687: 42 of cap free
Amount of items: 2
Items: 
Size: 725986 Color: 1
Size: 273973 Color: 0

Bin 1688: 42 of cap free
Amount of items: 2
Items: 
Size: 506340 Color: 0
Size: 493619 Color: 1

Bin 1689: 42 of cap free
Amount of items: 2
Items: 
Size: 507289 Color: 0
Size: 492670 Color: 1

Bin 1690: 42 of cap free
Amount of items: 2
Items: 
Size: 515099 Color: 0
Size: 484860 Color: 1

Bin 1691: 42 of cap free
Amount of items: 2
Items: 
Size: 547267 Color: 1
Size: 452692 Color: 0

Bin 1692: 42 of cap free
Amount of items: 2
Items: 
Size: 568281 Color: 1
Size: 431678 Color: 0

Bin 1693: 42 of cap free
Amount of items: 2
Items: 
Size: 578165 Color: 1
Size: 421794 Color: 0

Bin 1694: 42 of cap free
Amount of items: 2
Items: 
Size: 588704 Color: 0
Size: 411255 Color: 1

Bin 1695: 42 of cap free
Amount of items: 2
Items: 
Size: 593560 Color: 1
Size: 406399 Color: 0

Bin 1696: 42 of cap free
Amount of items: 2
Items: 
Size: 607963 Color: 1
Size: 391996 Color: 0

Bin 1697: 42 of cap free
Amount of items: 2
Items: 
Size: 609099 Color: 0
Size: 390860 Color: 1

Bin 1698: 42 of cap free
Amount of items: 2
Items: 
Size: 619143 Color: 1
Size: 380816 Color: 0

Bin 1699: 42 of cap free
Amount of items: 2
Items: 
Size: 620754 Color: 1
Size: 379205 Color: 0

Bin 1700: 42 of cap free
Amount of items: 2
Items: 
Size: 625811 Color: 1
Size: 374148 Color: 0

Bin 1701: 42 of cap free
Amount of items: 2
Items: 
Size: 644960 Color: 0
Size: 354999 Color: 1

Bin 1702: 42 of cap free
Amount of items: 2
Items: 
Size: 676840 Color: 1
Size: 323119 Color: 0

Bin 1703: 42 of cap free
Amount of items: 2
Items: 
Size: 677981 Color: 1
Size: 321978 Color: 0

Bin 1704: 42 of cap free
Amount of items: 2
Items: 
Size: 685484 Color: 1
Size: 314475 Color: 0

Bin 1705: 42 of cap free
Amount of items: 2
Items: 
Size: 709168 Color: 1
Size: 290791 Color: 0

Bin 1706: 42 of cap free
Amount of items: 2
Items: 
Size: 712386 Color: 1
Size: 287573 Color: 0

Bin 1707: 42 of cap free
Amount of items: 2
Items: 
Size: 728110 Color: 0
Size: 271849 Color: 1

Bin 1708: 42 of cap free
Amount of items: 2
Items: 
Size: 732669 Color: 0
Size: 267290 Color: 1

Bin 1709: 42 of cap free
Amount of items: 2
Items: 
Size: 741446 Color: 0
Size: 258513 Color: 1

Bin 1710: 42 of cap free
Amount of items: 2
Items: 
Size: 752128 Color: 1
Size: 247831 Color: 0

Bin 1711: 42 of cap free
Amount of items: 2
Items: 
Size: 764589 Color: 1
Size: 235370 Color: 0

Bin 1712: 42 of cap free
Amount of items: 2
Items: 
Size: 766662 Color: 1
Size: 233297 Color: 0

Bin 1713: 42 of cap free
Amount of items: 2
Items: 
Size: 766937 Color: 1
Size: 233022 Color: 0

Bin 1714: 42 of cap free
Amount of items: 3
Items: 
Size: 337986 Color: 1
Size: 336622 Color: 1
Size: 325351 Color: 0

Bin 1715: 43 of cap free
Amount of items: 2
Items: 
Size: 773160 Color: 0
Size: 226798 Color: 1

Bin 1716: 43 of cap free
Amount of items: 3
Items: 
Size: 433148 Color: 0
Size: 415009 Color: 1
Size: 151801 Color: 0

Bin 1717: 43 of cap free
Amount of items: 2
Items: 
Size: 511757 Color: 1
Size: 488201 Color: 0

Bin 1718: 43 of cap free
Amount of items: 2
Items: 
Size: 520814 Color: 0
Size: 479144 Color: 1

Bin 1719: 43 of cap free
Amount of items: 2
Items: 
Size: 526300 Color: 1
Size: 473658 Color: 0

Bin 1720: 43 of cap free
Amount of items: 2
Items: 
Size: 537922 Color: 0
Size: 462036 Color: 1

Bin 1721: 43 of cap free
Amount of items: 2
Items: 
Size: 564017 Color: 1
Size: 435941 Color: 0

Bin 1722: 43 of cap free
Amount of items: 2
Items: 
Size: 595046 Color: 1
Size: 404912 Color: 0

Bin 1723: 43 of cap free
Amount of items: 2
Items: 
Size: 604154 Color: 1
Size: 395804 Color: 0

Bin 1724: 43 of cap free
Amount of items: 2
Items: 
Size: 622032 Color: 0
Size: 377926 Color: 1

Bin 1725: 43 of cap free
Amount of items: 2
Items: 
Size: 640287 Color: 0
Size: 359671 Color: 1

Bin 1726: 43 of cap free
Amount of items: 2
Items: 
Size: 691269 Color: 0
Size: 308689 Color: 1

Bin 1727: 43 of cap free
Amount of items: 2
Items: 
Size: 709296 Color: 0
Size: 290662 Color: 1

Bin 1728: 43 of cap free
Amount of items: 2
Items: 
Size: 790622 Color: 0
Size: 209336 Color: 1

Bin 1729: 43 of cap free
Amount of items: 2
Items: 
Size: 762047 Color: 0
Size: 237911 Color: 1

Bin 1730: 43 of cap free
Amount of items: 2
Items: 
Size: 792086 Color: 1
Size: 207872 Color: 0

Bin 1731: 44 of cap free
Amount of items: 2
Items: 
Size: 514710 Color: 1
Size: 485247 Color: 0

Bin 1732: 44 of cap free
Amount of items: 2
Items: 
Size: 534646 Color: 0
Size: 465311 Color: 1

Bin 1733: 44 of cap free
Amount of items: 2
Items: 
Size: 569834 Color: 1
Size: 430123 Color: 0

Bin 1734: 44 of cap free
Amount of items: 2
Items: 
Size: 576034 Color: 0
Size: 423923 Color: 1

Bin 1735: 44 of cap free
Amount of items: 2
Items: 
Size: 585380 Color: 1
Size: 414577 Color: 0

Bin 1736: 44 of cap free
Amount of items: 2
Items: 
Size: 622201 Color: 0
Size: 377756 Color: 1

Bin 1737: 44 of cap free
Amount of items: 2
Items: 
Size: 646796 Color: 1
Size: 353161 Color: 0

Bin 1738: 44 of cap free
Amount of items: 2
Items: 
Size: 657406 Color: 1
Size: 342551 Color: 0

Bin 1739: 44 of cap free
Amount of items: 2
Items: 
Size: 732099 Color: 0
Size: 267858 Color: 1

Bin 1740: 44 of cap free
Amount of items: 2
Items: 
Size: 752831 Color: 0
Size: 247126 Color: 1

Bin 1741: 44 of cap free
Amount of items: 2
Items: 
Size: 753741 Color: 0
Size: 246216 Color: 1

Bin 1742: 44 of cap free
Amount of items: 2
Items: 
Size: 763835 Color: 0
Size: 236122 Color: 1

Bin 1743: 44 of cap free
Amount of items: 2
Items: 
Size: 767811 Color: 0
Size: 232146 Color: 1

Bin 1744: 44 of cap free
Amount of items: 2
Items: 
Size: 771253 Color: 1
Size: 228704 Color: 0

Bin 1745: 44 of cap free
Amount of items: 2
Items: 
Size: 780497 Color: 0
Size: 219460 Color: 1

Bin 1746: 44 of cap free
Amount of items: 2
Items: 
Size: 785813 Color: 0
Size: 214144 Color: 1

Bin 1747: 44 of cap free
Amount of items: 4
Items: 
Size: 267405 Color: 1
Size: 263803 Color: 1
Size: 235875 Color: 0
Size: 232874 Color: 0

Bin 1748: 45 of cap free
Amount of items: 2
Items: 
Size: 503781 Color: 1
Size: 496175 Color: 0

Bin 1749: 45 of cap free
Amount of items: 2
Items: 
Size: 517052 Color: 1
Size: 482904 Color: 0

Bin 1750: 45 of cap free
Amount of items: 2
Items: 
Size: 524372 Color: 1
Size: 475584 Color: 0

Bin 1751: 45 of cap free
Amount of items: 2
Items: 
Size: 525979 Color: 1
Size: 473977 Color: 0

Bin 1752: 45 of cap free
Amount of items: 2
Items: 
Size: 566333 Color: 1
Size: 433623 Color: 0

Bin 1753: 45 of cap free
Amount of items: 2
Items: 
Size: 619193 Color: 1
Size: 380763 Color: 0

Bin 1754: 45 of cap free
Amount of items: 2
Items: 
Size: 628600 Color: 0
Size: 371356 Color: 1

Bin 1755: 45 of cap free
Amount of items: 2
Items: 
Size: 632845 Color: 0
Size: 367111 Color: 1

Bin 1756: 45 of cap free
Amount of items: 2
Items: 
Size: 641210 Color: 0
Size: 358746 Color: 1

Bin 1757: 45 of cap free
Amount of items: 2
Items: 
Size: 675181 Color: 1
Size: 324775 Color: 0

Bin 1758: 45 of cap free
Amount of items: 2
Items: 
Size: 677403 Color: 0
Size: 322553 Color: 1

Bin 1759: 45 of cap free
Amount of items: 2
Items: 
Size: 784780 Color: 1
Size: 215176 Color: 0

Bin 1760: 45 of cap free
Amount of items: 2
Items: 
Size: 796939 Color: 0
Size: 203017 Color: 1

Bin 1761: 45 of cap free
Amount of items: 3
Items: 
Size: 370507 Color: 1
Size: 317006 Color: 0
Size: 312443 Color: 0

Bin 1762: 45 of cap free
Amount of items: 2
Items: 
Size: 778050 Color: 1
Size: 221906 Color: 0

Bin 1763: 45 of cap free
Amount of items: 2
Items: 
Size: 509417 Color: 0
Size: 490539 Color: 1

Bin 1764: 45 of cap free
Amount of items: 3
Items: 
Size: 764195 Color: 1
Size: 128062 Color: 1
Size: 107699 Color: 0

Bin 1765: 46 of cap free
Amount of items: 2
Items: 
Size: 548073 Color: 1
Size: 451882 Color: 0

Bin 1766: 46 of cap free
Amount of items: 2
Items: 
Size: 514734 Color: 0
Size: 485221 Color: 1

Bin 1767: 46 of cap free
Amount of items: 2
Items: 
Size: 517714 Color: 0
Size: 482241 Color: 1

Bin 1768: 46 of cap free
Amount of items: 2
Items: 
Size: 551834 Color: 0
Size: 448121 Color: 1

Bin 1769: 46 of cap free
Amount of items: 2
Items: 
Size: 565610 Color: 1
Size: 434345 Color: 0

Bin 1770: 46 of cap free
Amount of items: 2
Items: 
Size: 582422 Color: 0
Size: 417533 Color: 1

Bin 1771: 46 of cap free
Amount of items: 2
Items: 
Size: 594982 Color: 0
Size: 404973 Color: 1

Bin 1772: 46 of cap free
Amount of items: 2
Items: 
Size: 606045 Color: 0
Size: 393910 Color: 1

Bin 1773: 46 of cap free
Amount of items: 2
Items: 
Size: 612805 Color: 0
Size: 387150 Color: 1

Bin 1774: 46 of cap free
Amount of items: 2
Items: 
Size: 631158 Color: 1
Size: 368797 Color: 0

Bin 1775: 46 of cap free
Amount of items: 2
Items: 
Size: 637830 Color: 1
Size: 362125 Color: 0

Bin 1776: 46 of cap free
Amount of items: 2
Items: 
Size: 637969 Color: 0
Size: 361986 Color: 1

Bin 1777: 46 of cap free
Amount of items: 2
Items: 
Size: 643436 Color: 0
Size: 356519 Color: 1

Bin 1778: 46 of cap free
Amount of items: 2
Items: 
Size: 662934 Color: 0
Size: 337021 Color: 1

Bin 1779: 46 of cap free
Amount of items: 2
Items: 
Size: 663144 Color: 0
Size: 336811 Color: 1

Bin 1780: 46 of cap free
Amount of items: 2
Items: 
Size: 691750 Color: 0
Size: 308205 Color: 1

Bin 1781: 46 of cap free
Amount of items: 2
Items: 
Size: 691968 Color: 0
Size: 307987 Color: 1

Bin 1782: 46 of cap free
Amount of items: 2
Items: 
Size: 713926 Color: 0
Size: 286029 Color: 1

Bin 1783: 46 of cap free
Amount of items: 2
Items: 
Size: 725568 Color: 1
Size: 274387 Color: 0

Bin 1784: 46 of cap free
Amount of items: 2
Items: 
Size: 749646 Color: 1
Size: 250309 Color: 0

Bin 1785: 46 of cap free
Amount of items: 2
Items: 
Size: 760826 Color: 1
Size: 239129 Color: 0

Bin 1786: 46 of cap free
Amount of items: 2
Items: 
Size: 767331 Color: 1
Size: 232624 Color: 0

Bin 1787: 46 of cap free
Amount of items: 2
Items: 
Size: 798379 Color: 0
Size: 201576 Color: 1

Bin 1788: 46 of cap free
Amount of items: 2
Items: 
Size: 548837 Color: 0
Size: 451118 Color: 1

Bin 1789: 47 of cap free
Amount of items: 2
Items: 
Size: 793264 Color: 1
Size: 206690 Color: 0

Bin 1790: 47 of cap free
Amount of items: 2
Items: 
Size: 741790 Color: 1
Size: 258164 Color: 0

Bin 1791: 47 of cap free
Amount of items: 2
Items: 
Size: 550772 Color: 1
Size: 449182 Color: 0

Bin 1792: 47 of cap free
Amount of items: 2
Items: 
Size: 588467 Color: 0
Size: 411487 Color: 1

Bin 1793: 47 of cap free
Amount of items: 2
Items: 
Size: 611638 Color: 1
Size: 388316 Color: 0

Bin 1794: 47 of cap free
Amount of items: 2
Items: 
Size: 638070 Color: 0
Size: 361884 Color: 1

Bin 1795: 47 of cap free
Amount of items: 2
Items: 
Size: 645076 Color: 0
Size: 354878 Color: 1

Bin 1796: 47 of cap free
Amount of items: 2
Items: 
Size: 671496 Color: 0
Size: 328458 Color: 1

Bin 1797: 47 of cap free
Amount of items: 2
Items: 
Size: 693200 Color: 0
Size: 306754 Color: 1

Bin 1798: 47 of cap free
Amount of items: 2
Items: 
Size: 701943 Color: 0
Size: 298011 Color: 1

Bin 1799: 47 of cap free
Amount of items: 2
Items: 
Size: 706335 Color: 1
Size: 293619 Color: 0

Bin 1800: 47 of cap free
Amount of items: 2
Items: 
Size: 708786 Color: 0
Size: 291168 Color: 1

Bin 1801: 47 of cap free
Amount of items: 2
Items: 
Size: 766131 Color: 0
Size: 233823 Color: 1

Bin 1802: 47 of cap free
Amount of items: 2
Items: 
Size: 781506 Color: 1
Size: 218448 Color: 0

Bin 1803: 48 of cap free
Amount of items: 2
Items: 
Size: 723151 Color: 1
Size: 276802 Color: 0

Bin 1804: 48 of cap free
Amount of items: 2
Items: 
Size: 517618 Color: 1
Size: 482335 Color: 0

Bin 1805: 48 of cap free
Amount of items: 2
Items: 
Size: 531747 Color: 0
Size: 468206 Color: 1

Bin 1806: 48 of cap free
Amount of items: 2
Items: 
Size: 554016 Color: 1
Size: 445937 Color: 0

Bin 1807: 48 of cap free
Amount of items: 2
Items: 
Size: 555077 Color: 0
Size: 444876 Color: 1

Bin 1808: 48 of cap free
Amount of items: 2
Items: 
Size: 571749 Color: 0
Size: 428204 Color: 1

Bin 1809: 48 of cap free
Amount of items: 2
Items: 
Size: 577443 Color: 0
Size: 422510 Color: 1

Bin 1810: 48 of cap free
Amount of items: 2
Items: 
Size: 589002 Color: 1
Size: 410951 Color: 0

Bin 1811: 48 of cap free
Amount of items: 2
Items: 
Size: 622992 Color: 1
Size: 376961 Color: 0

Bin 1812: 48 of cap free
Amount of items: 2
Items: 
Size: 625618 Color: 0
Size: 374335 Color: 1

Bin 1813: 48 of cap free
Amount of items: 2
Items: 
Size: 628980 Color: 1
Size: 370973 Color: 0

Bin 1814: 48 of cap free
Amount of items: 2
Items: 
Size: 643378 Color: 0
Size: 356575 Color: 1

Bin 1815: 48 of cap free
Amount of items: 2
Items: 
Size: 648637 Color: 0
Size: 351316 Color: 1

Bin 1816: 48 of cap free
Amount of items: 2
Items: 
Size: 685574 Color: 1
Size: 314379 Color: 0

Bin 1817: 48 of cap free
Amount of items: 2
Items: 
Size: 689094 Color: 0
Size: 310859 Color: 1

Bin 1818: 48 of cap free
Amount of items: 2
Items: 
Size: 692403 Color: 1
Size: 307550 Color: 0

Bin 1819: 48 of cap free
Amount of items: 2
Items: 
Size: 720631 Color: 1
Size: 279322 Color: 0

Bin 1820: 48 of cap free
Amount of items: 2
Items: 
Size: 735751 Color: 1
Size: 264202 Color: 0

Bin 1821: 48 of cap free
Amount of items: 2
Items: 
Size: 748012 Color: 1
Size: 251941 Color: 0

Bin 1822: 48 of cap free
Amount of items: 2
Items: 
Size: 757259 Color: 1
Size: 242694 Color: 0

Bin 1823: 48 of cap free
Amount of items: 2
Items: 
Size: 789588 Color: 1
Size: 210365 Color: 0

Bin 1824: 48 of cap free
Amount of items: 3
Items: 
Size: 374961 Color: 0
Size: 320673 Color: 1
Size: 304319 Color: 0

Bin 1825: 48 of cap free
Amount of items: 2
Items: 
Size: 742641 Color: 1
Size: 257312 Color: 0

Bin 1826: 48 of cap free
Amount of items: 2
Items: 
Size: 663194 Color: 0
Size: 336759 Color: 1

Bin 1827: 49 of cap free
Amount of items: 2
Items: 
Size: 504272 Color: 1
Size: 495680 Color: 0

Bin 1828: 49 of cap free
Amount of items: 2
Items: 
Size: 527787 Color: 1
Size: 472165 Color: 0

Bin 1829: 49 of cap free
Amount of items: 2
Items: 
Size: 533192 Color: 0
Size: 466760 Color: 1

Bin 1830: 49 of cap free
Amount of items: 2
Items: 
Size: 562445 Color: 0
Size: 437507 Color: 1

Bin 1831: 49 of cap free
Amount of items: 2
Items: 
Size: 601280 Color: 0
Size: 398672 Color: 1

Bin 1832: 49 of cap free
Amount of items: 2
Items: 
Size: 624187 Color: 0
Size: 375765 Color: 1

Bin 1833: 49 of cap free
Amount of items: 2
Items: 
Size: 637182 Color: 1
Size: 362770 Color: 0

Bin 1834: 49 of cap free
Amount of items: 2
Items: 
Size: 640750 Color: 0
Size: 359202 Color: 1

Bin 1835: 49 of cap free
Amount of items: 2
Items: 
Size: 647324 Color: 0
Size: 352628 Color: 1

Bin 1836: 49 of cap free
Amount of items: 2
Items: 
Size: 709767 Color: 1
Size: 290185 Color: 0

Bin 1837: 49 of cap free
Amount of items: 2
Items: 
Size: 722496 Color: 1
Size: 277456 Color: 0

Bin 1838: 49 of cap free
Amount of items: 2
Items: 
Size: 734011 Color: 0
Size: 265941 Color: 1

Bin 1839: 49 of cap free
Amount of items: 2
Items: 
Size: 687702 Color: 1
Size: 312250 Color: 0

Bin 1840: 49 of cap free
Amount of items: 2
Items: 
Size: 589011 Color: 0
Size: 410941 Color: 1

Bin 1841: 50 of cap free
Amount of items: 2
Items: 
Size: 569094 Color: 1
Size: 430857 Color: 0

Bin 1842: 50 of cap free
Amount of items: 2
Items: 
Size: 581199 Color: 0
Size: 418752 Color: 1

Bin 1843: 50 of cap free
Amount of items: 2
Items: 
Size: 610713 Color: 1
Size: 389238 Color: 0

Bin 1844: 50 of cap free
Amount of items: 2
Items: 
Size: 627668 Color: 0
Size: 372283 Color: 1

Bin 1845: 50 of cap free
Amount of items: 2
Items: 
Size: 646216 Color: 0
Size: 353735 Color: 1

Bin 1846: 50 of cap free
Amount of items: 2
Items: 
Size: 680083 Color: 1
Size: 319868 Color: 0

Bin 1847: 50 of cap free
Amount of items: 2
Items: 
Size: 700167 Color: 1
Size: 299784 Color: 0

Bin 1848: 50 of cap free
Amount of items: 2
Items: 
Size: 745148 Color: 0
Size: 254803 Color: 1

Bin 1849: 50 of cap free
Amount of items: 2
Items: 
Size: 748597 Color: 1
Size: 251354 Color: 0

Bin 1850: 50 of cap free
Amount of items: 3
Items: 
Size: 377032 Color: 1
Size: 317344 Color: 1
Size: 305575 Color: 0

Bin 1851: 51 of cap free
Amount of items: 2
Items: 
Size: 502231 Color: 0
Size: 497719 Color: 1

Bin 1852: 51 of cap free
Amount of items: 2
Items: 
Size: 508209 Color: 0
Size: 491741 Color: 1

Bin 1853: 51 of cap free
Amount of items: 2
Items: 
Size: 533135 Color: 1
Size: 466815 Color: 0

Bin 1854: 51 of cap free
Amount of items: 2
Items: 
Size: 535257 Color: 0
Size: 464693 Color: 1

Bin 1855: 51 of cap free
Amount of items: 2
Items: 
Size: 537246 Color: 1
Size: 462704 Color: 0

Bin 1856: 51 of cap free
Amount of items: 2
Items: 
Size: 572538 Color: 1
Size: 427412 Color: 0

Bin 1857: 51 of cap free
Amount of items: 2
Items: 
Size: 578524 Color: 0
Size: 421426 Color: 1

Bin 1858: 51 of cap free
Amount of items: 2
Items: 
Size: 606745 Color: 0
Size: 393205 Color: 1

Bin 1859: 51 of cap free
Amount of items: 2
Items: 
Size: 634280 Color: 1
Size: 365670 Color: 0

Bin 1860: 51 of cap free
Amount of items: 2
Items: 
Size: 635013 Color: 1
Size: 364937 Color: 0

Bin 1861: 51 of cap free
Amount of items: 2
Items: 
Size: 635341 Color: 1
Size: 364609 Color: 0

Bin 1862: 51 of cap free
Amount of items: 2
Items: 
Size: 684132 Color: 0
Size: 315818 Color: 1

Bin 1863: 51 of cap free
Amount of items: 2
Items: 
Size: 726079 Color: 1
Size: 273871 Color: 0

Bin 1864: 51 of cap free
Amount of items: 2
Items: 
Size: 761246 Color: 0
Size: 238704 Color: 1

Bin 1865: 51 of cap free
Amount of items: 2
Items: 
Size: 797564 Color: 1
Size: 202386 Color: 0

Bin 1866: 51 of cap free
Amount of items: 2
Items: 
Size: 735970 Color: 1
Size: 263980 Color: 0

Bin 1867: 52 of cap free
Amount of items: 2
Items: 
Size: 554105 Color: 0
Size: 445844 Color: 1

Bin 1868: 52 of cap free
Amount of items: 2
Items: 
Size: 571821 Color: 0
Size: 428128 Color: 1

Bin 1869: 52 of cap free
Amount of items: 2
Items: 
Size: 574964 Color: 1
Size: 424985 Color: 0

Bin 1870: 52 of cap free
Amount of items: 2
Items: 
Size: 589123 Color: 1
Size: 410826 Color: 0

Bin 1871: 52 of cap free
Amount of items: 2
Items: 
Size: 597337 Color: 0
Size: 402612 Color: 1

Bin 1872: 52 of cap free
Amount of items: 2
Items: 
Size: 669432 Color: 0
Size: 330517 Color: 1

Bin 1873: 52 of cap free
Amount of items: 2
Items: 
Size: 680501 Color: 1
Size: 319448 Color: 0

Bin 1874: 52 of cap free
Amount of items: 2
Items: 
Size: 698676 Color: 1
Size: 301273 Color: 0

Bin 1875: 52 of cap free
Amount of items: 2
Items: 
Size: 699213 Color: 1
Size: 300736 Color: 0

Bin 1876: 52 of cap free
Amount of items: 2
Items: 
Size: 755513 Color: 0
Size: 244436 Color: 1

Bin 1877: 52 of cap free
Amount of items: 2
Items: 
Size: 767177 Color: 0
Size: 232772 Color: 1

Bin 1878: 52 of cap free
Amount of items: 2
Items: 
Size: 565884 Color: 1
Size: 434065 Color: 0

Bin 1879: 52 of cap free
Amount of items: 4
Items: 
Size: 284880 Color: 1
Size: 284532 Color: 1
Size: 237581 Color: 0
Size: 192956 Color: 0

Bin 1880: 53 of cap free
Amount of items: 2
Items: 
Size: 510045 Color: 0
Size: 489903 Color: 1

Bin 1881: 53 of cap free
Amount of items: 2
Items: 
Size: 533317 Color: 1
Size: 466631 Color: 0

Bin 1882: 53 of cap free
Amount of items: 2
Items: 
Size: 562736 Color: 1
Size: 437212 Color: 0

Bin 1883: 53 of cap free
Amount of items: 2
Items: 
Size: 566321 Color: 0
Size: 433627 Color: 1

Bin 1884: 53 of cap free
Amount of items: 2
Items: 
Size: 578843 Color: 1
Size: 421105 Color: 0

Bin 1885: 53 of cap free
Amount of items: 2
Items: 
Size: 602809 Color: 1
Size: 397139 Color: 0

Bin 1886: 53 of cap free
Amount of items: 2
Items: 
Size: 605989 Color: 1
Size: 393959 Color: 0

Bin 1887: 53 of cap free
Amount of items: 2
Items: 
Size: 628623 Color: 1
Size: 371325 Color: 0

Bin 1888: 53 of cap free
Amount of items: 2
Items: 
Size: 639542 Color: 0
Size: 360406 Color: 1

Bin 1889: 53 of cap free
Amount of items: 2
Items: 
Size: 673837 Color: 1
Size: 326111 Color: 0

Bin 1890: 53 of cap free
Amount of items: 2
Items: 
Size: 687858 Color: 1
Size: 312090 Color: 0

Bin 1891: 53 of cap free
Amount of items: 2
Items: 
Size: 700823 Color: 1
Size: 299125 Color: 0

Bin 1892: 53 of cap free
Amount of items: 2
Items: 
Size: 703768 Color: 0
Size: 296180 Color: 1

Bin 1893: 53 of cap free
Amount of items: 2
Items: 
Size: 707002 Color: 1
Size: 292946 Color: 0

Bin 1894: 53 of cap free
Amount of items: 2
Items: 
Size: 722290 Color: 0
Size: 277658 Color: 1

Bin 1895: 53 of cap free
Amount of items: 2
Items: 
Size: 724200 Color: 1
Size: 275748 Color: 0

Bin 1896: 53 of cap free
Amount of items: 2
Items: 
Size: 745797 Color: 1
Size: 254151 Color: 0

Bin 1897: 53 of cap free
Amount of items: 2
Items: 
Size: 749615 Color: 0
Size: 250333 Color: 1

Bin 1898: 53 of cap free
Amount of items: 2
Items: 
Size: 765098 Color: 1
Size: 234850 Color: 0

Bin 1899: 53 of cap free
Amount of items: 3
Items: 
Size: 755337 Color: 0
Size: 129588 Color: 0
Size: 115023 Color: 1

Bin 1900: 54 of cap free
Amount of items: 2
Items: 
Size: 663329 Color: 0
Size: 336618 Color: 1

Bin 1901: 54 of cap free
Amount of items: 2
Items: 
Size: 537800 Color: 1
Size: 462147 Color: 0

Bin 1902: 54 of cap free
Amount of items: 2
Items: 
Size: 546281 Color: 1
Size: 453666 Color: 0

Bin 1903: 54 of cap free
Amount of items: 2
Items: 
Size: 556134 Color: 0
Size: 443813 Color: 1

Bin 1904: 54 of cap free
Amount of items: 2
Items: 
Size: 608616 Color: 1
Size: 391331 Color: 0

Bin 1905: 54 of cap free
Amount of items: 2
Items: 
Size: 658938 Color: 1
Size: 341009 Color: 0

Bin 1906: 54 of cap free
Amount of items: 2
Items: 
Size: 676517 Color: 1
Size: 323430 Color: 0

Bin 1907: 54 of cap free
Amount of items: 2
Items: 
Size: 693000 Color: 1
Size: 306947 Color: 0

Bin 1908: 54 of cap free
Amount of items: 2
Items: 
Size: 707599 Color: 1
Size: 292348 Color: 0

Bin 1909: 54 of cap free
Amount of items: 2
Items: 
Size: 726522 Color: 0
Size: 273425 Color: 1

Bin 1910: 54 of cap free
Amount of items: 2
Items: 
Size: 757194 Color: 0
Size: 242753 Color: 1

Bin 1911: 55 of cap free
Amount of items: 2
Items: 
Size: 604653 Color: 1
Size: 395293 Color: 0

Bin 1912: 55 of cap free
Amount of items: 2
Items: 
Size: 619715 Color: 0
Size: 380231 Color: 1

Bin 1913: 55 of cap free
Amount of items: 2
Items: 
Size: 645966 Color: 1
Size: 353980 Color: 0

Bin 1914: 55 of cap free
Amount of items: 2
Items: 
Size: 675898 Color: 1
Size: 324048 Color: 0

Bin 1915: 55 of cap free
Amount of items: 2
Items: 
Size: 687879 Color: 0
Size: 312067 Color: 1

Bin 1916: 55 of cap free
Amount of items: 2
Items: 
Size: 792740 Color: 1
Size: 207206 Color: 0

Bin 1917: 55 of cap free
Amount of items: 2
Items: 
Size: 794427 Color: 1
Size: 205519 Color: 0

Bin 1918: 56 of cap free
Amount of items: 2
Items: 
Size: 501033 Color: 0
Size: 498912 Color: 1

Bin 1919: 56 of cap free
Amount of items: 2
Items: 
Size: 502989 Color: 1
Size: 496956 Color: 0

Bin 1920: 56 of cap free
Amount of items: 2
Items: 
Size: 533634 Color: 0
Size: 466311 Color: 1

Bin 1921: 56 of cap free
Amount of items: 2
Items: 
Size: 559677 Color: 0
Size: 440268 Color: 1

Bin 1922: 56 of cap free
Amount of items: 2
Items: 
Size: 567646 Color: 0
Size: 432299 Color: 1

Bin 1923: 56 of cap free
Amount of items: 2
Items: 
Size: 635101 Color: 1
Size: 364844 Color: 0

Bin 1924: 56 of cap free
Amount of items: 2
Items: 
Size: 685375 Color: 0
Size: 314570 Color: 1

Bin 1925: 56 of cap free
Amount of items: 2
Items: 
Size: 696510 Color: 0
Size: 303435 Color: 1

Bin 1926: 56 of cap free
Amount of items: 2
Items: 
Size: 702339 Color: 0
Size: 297606 Color: 1

Bin 1927: 56 of cap free
Amount of items: 2
Items: 
Size: 754207 Color: 1
Size: 245738 Color: 0

Bin 1928: 56 of cap free
Amount of items: 2
Items: 
Size: 793379 Color: 0
Size: 206566 Color: 1

Bin 1929: 56 of cap free
Amount of items: 2
Items: 
Size: 798940 Color: 0
Size: 201005 Color: 1

Bin 1930: 56 of cap free
Amount of items: 2
Items: 
Size: 547228 Color: 0
Size: 452717 Color: 1

Bin 1931: 57 of cap free
Amount of items: 2
Items: 
Size: 534441 Color: 1
Size: 465503 Color: 0

Bin 1932: 57 of cap free
Amount of items: 2
Items: 
Size: 567642 Color: 1
Size: 432302 Color: 0

Bin 1933: 57 of cap free
Amount of items: 2
Items: 
Size: 576455 Color: 1
Size: 423489 Color: 0

Bin 1934: 57 of cap free
Amount of items: 2
Items: 
Size: 605360 Color: 0
Size: 394584 Color: 1

Bin 1935: 57 of cap free
Amount of items: 2
Items: 
Size: 611911 Color: 1
Size: 388033 Color: 0

Bin 1936: 57 of cap free
Amount of items: 2
Items: 
Size: 614840 Color: 1
Size: 385104 Color: 0

Bin 1937: 57 of cap free
Amount of items: 2
Items: 
Size: 637807 Color: 0
Size: 362137 Color: 1

Bin 1938: 57 of cap free
Amount of items: 2
Items: 
Size: 643840 Color: 1
Size: 356104 Color: 0

Bin 1939: 57 of cap free
Amount of items: 2
Items: 
Size: 645234 Color: 0
Size: 354710 Color: 1

Bin 1940: 57 of cap free
Amount of items: 2
Items: 
Size: 672131 Color: 0
Size: 327813 Color: 1

Bin 1941: 57 of cap free
Amount of items: 2
Items: 
Size: 672216 Color: 0
Size: 327728 Color: 1

Bin 1942: 57 of cap free
Amount of items: 2
Items: 
Size: 707272 Color: 1
Size: 292672 Color: 0

Bin 1943: 57 of cap free
Amount of items: 2
Items: 
Size: 713112 Color: 1
Size: 286832 Color: 0

Bin 1944: 57 of cap free
Amount of items: 2
Items: 
Size: 735290 Color: 0
Size: 264654 Color: 1

Bin 1945: 57 of cap free
Amount of items: 2
Items: 
Size: 759601 Color: 1
Size: 240343 Color: 0

Bin 1946: 57 of cap free
Amount of items: 2
Items: 
Size: 762491 Color: 1
Size: 237453 Color: 0

Bin 1947: 57 of cap free
Amount of items: 2
Items: 
Size: 790059 Color: 0
Size: 209885 Color: 1

Bin 1948: 57 of cap free
Amount of items: 2
Items: 
Size: 794189 Color: 0
Size: 205755 Color: 1

Bin 1949: 57 of cap free
Amount of items: 3
Items: 
Size: 390870 Color: 0
Size: 314847 Color: 0
Size: 294227 Color: 1

Bin 1950: 57 of cap free
Amount of items: 2
Items: 
Size: 526943 Color: 1
Size: 473001 Color: 0

Bin 1951: 57 of cap free
Amount of items: 2
Items: 
Size: 521454 Color: 0
Size: 478490 Color: 1

Bin 1952: 58 of cap free
Amount of items: 2
Items: 
Size: 524732 Color: 1
Size: 475211 Color: 0

Bin 1953: 58 of cap free
Amount of items: 2
Items: 
Size: 531557 Color: 1
Size: 468386 Color: 0

Bin 1954: 58 of cap free
Amount of items: 2
Items: 
Size: 535488 Color: 1
Size: 464455 Color: 0

Bin 1955: 58 of cap free
Amount of items: 2
Items: 
Size: 553739 Color: 1
Size: 446204 Color: 0

Bin 1956: 58 of cap free
Amount of items: 2
Items: 
Size: 565835 Color: 0
Size: 434108 Color: 1

Bin 1957: 58 of cap free
Amount of items: 2
Items: 
Size: 578455 Color: 0
Size: 421488 Color: 1

Bin 1958: 58 of cap free
Amount of items: 2
Items: 
Size: 635789 Color: 0
Size: 364154 Color: 1

Bin 1959: 58 of cap free
Amount of items: 2
Items: 
Size: 691628 Color: 0
Size: 308315 Color: 1

Bin 1960: 58 of cap free
Amount of items: 2
Items: 
Size: 718390 Color: 0
Size: 281553 Color: 1

Bin 1961: 58 of cap free
Amount of items: 2
Items: 
Size: 743088 Color: 0
Size: 256855 Color: 1

Bin 1962: 58 of cap free
Amount of items: 2
Items: 
Size: 743906 Color: 1
Size: 256037 Color: 0

Bin 1963: 58 of cap free
Amount of items: 2
Items: 
Size: 744829 Color: 1
Size: 255114 Color: 0

Bin 1964: 58 of cap free
Amount of items: 2
Items: 
Size: 775269 Color: 0
Size: 224674 Color: 1

Bin 1965: 59 of cap free
Amount of items: 2
Items: 
Size: 513876 Color: 0
Size: 486066 Color: 1

Bin 1966: 59 of cap free
Amount of items: 2
Items: 
Size: 521995 Color: 1
Size: 477947 Color: 0

Bin 1967: 59 of cap free
Amount of items: 2
Items: 
Size: 539002 Color: 0
Size: 460940 Color: 1

Bin 1968: 59 of cap free
Amount of items: 2
Items: 
Size: 572847 Color: 1
Size: 427095 Color: 0

Bin 1969: 59 of cap free
Amount of items: 2
Items: 
Size: 580863 Color: 0
Size: 419079 Color: 1

Bin 1970: 59 of cap free
Amount of items: 2
Items: 
Size: 601807 Color: 0
Size: 398135 Color: 1

Bin 1971: 59 of cap free
Amount of items: 2
Items: 
Size: 613647 Color: 1
Size: 386295 Color: 0

Bin 1972: 59 of cap free
Amount of items: 2
Items: 
Size: 615825 Color: 1
Size: 384117 Color: 0

Bin 1973: 59 of cap free
Amount of items: 2
Items: 
Size: 636619 Color: 1
Size: 363323 Color: 0

Bin 1974: 59 of cap free
Amount of items: 2
Items: 
Size: 641716 Color: 1
Size: 358226 Color: 0

Bin 1975: 59 of cap free
Amount of items: 2
Items: 
Size: 693282 Color: 0
Size: 306660 Color: 1

Bin 1976: 59 of cap free
Amount of items: 2
Items: 
Size: 733646 Color: 1
Size: 266296 Color: 0

Bin 1977: 60 of cap free
Amount of items: 2
Items: 
Size: 539660 Color: 0
Size: 460281 Color: 1

Bin 1978: 60 of cap free
Amount of items: 2
Items: 
Size: 565390 Color: 0
Size: 434551 Color: 1

Bin 1979: 60 of cap free
Amount of items: 2
Items: 
Size: 606999 Color: 0
Size: 392942 Color: 1

Bin 1980: 60 of cap free
Amount of items: 2
Items: 
Size: 632911 Color: 1
Size: 367030 Color: 0

Bin 1981: 60 of cap free
Amount of items: 2
Items: 
Size: 653471 Color: 0
Size: 346470 Color: 1

Bin 1982: 60 of cap free
Amount of items: 2
Items: 
Size: 658316 Color: 0
Size: 341625 Color: 1

Bin 1983: 60 of cap free
Amount of items: 2
Items: 
Size: 687208 Color: 1
Size: 312733 Color: 0

Bin 1984: 60 of cap free
Amount of items: 2
Items: 
Size: 695667 Color: 0
Size: 304274 Color: 1

Bin 1985: 60 of cap free
Amount of items: 2
Items: 
Size: 706493 Color: 0
Size: 293448 Color: 1

Bin 1986: 60 of cap free
Amount of items: 2
Items: 
Size: 719432 Color: 1
Size: 280509 Color: 0

Bin 1987: 61 of cap free
Amount of items: 2
Items: 
Size: 631829 Color: 0
Size: 368111 Color: 1

Bin 1988: 61 of cap free
Amount of items: 3
Items: 
Size: 409923 Color: 1
Size: 311018 Color: 1
Size: 278999 Color: 0

Bin 1989: 61 of cap free
Amount of items: 2
Items: 
Size: 507376 Color: 1
Size: 492564 Color: 0

Bin 1990: 61 of cap free
Amount of items: 2
Items: 
Size: 527043 Color: 0
Size: 472897 Color: 1

Bin 1991: 61 of cap free
Amount of items: 2
Items: 
Size: 528931 Color: 1
Size: 471009 Color: 0

Bin 1992: 61 of cap free
Amount of items: 2
Items: 
Size: 584131 Color: 0
Size: 415809 Color: 1

Bin 1993: 61 of cap free
Amount of items: 2
Items: 
Size: 706766 Color: 1
Size: 293174 Color: 0

Bin 1994: 61 of cap free
Amount of items: 2
Items: 
Size: 714255 Color: 1
Size: 285685 Color: 0

Bin 1995: 61 of cap free
Amount of items: 2
Items: 
Size: 752085 Color: 0
Size: 247855 Color: 1

Bin 1996: 61 of cap free
Amount of items: 2
Items: 
Size: 761759 Color: 0
Size: 238181 Color: 1

Bin 1997: 61 of cap free
Amount of items: 2
Items: 
Size: 632608 Color: 1
Size: 367332 Color: 0

Bin 1998: 62 of cap free
Amount of items: 2
Items: 
Size: 505764 Color: 0
Size: 494175 Color: 1

Bin 1999: 62 of cap free
Amount of items: 2
Items: 
Size: 534228 Color: 1
Size: 465711 Color: 0

Bin 2000: 62 of cap free
Amount of items: 2
Items: 
Size: 539210 Color: 0
Size: 460729 Color: 1

Bin 2001: 62 of cap free
Amount of items: 2
Items: 
Size: 551889 Color: 0
Size: 448050 Color: 1

Bin 2002: 62 of cap free
Amount of items: 2
Items: 
Size: 555629 Color: 1
Size: 444310 Color: 0

Bin 2003: 62 of cap free
Amount of items: 2
Items: 
Size: 607945 Color: 1
Size: 391994 Color: 0

Bin 2004: 62 of cap free
Amount of items: 2
Items: 
Size: 635914 Color: 1
Size: 364025 Color: 0

Bin 2005: 62 of cap free
Amount of items: 2
Items: 
Size: 650929 Color: 1
Size: 349010 Color: 0

Bin 2006: 62 of cap free
Amount of items: 2
Items: 
Size: 670833 Color: 1
Size: 329106 Color: 0

Bin 2007: 62 of cap free
Amount of items: 2
Items: 
Size: 674149 Color: 1
Size: 325790 Color: 0

Bin 2008: 62 of cap free
Amount of items: 2
Items: 
Size: 711219 Color: 0
Size: 288720 Color: 1

Bin 2009: 62 of cap free
Amount of items: 2
Items: 
Size: 713935 Color: 1
Size: 286004 Color: 0

Bin 2010: 62 of cap free
Amount of items: 2
Items: 
Size: 722081 Color: 1
Size: 277858 Color: 0

Bin 2011: 62 of cap free
Amount of items: 2
Items: 
Size: 741620 Color: 0
Size: 258319 Color: 1

Bin 2012: 62 of cap free
Amount of items: 2
Items: 
Size: 764517 Color: 0
Size: 235422 Color: 1

Bin 2013: 62 of cap free
Amount of items: 2
Items: 
Size: 769648 Color: 0
Size: 230291 Color: 1

Bin 2014: 62 of cap free
Amount of items: 2
Items: 
Size: 754784 Color: 1
Size: 245155 Color: 0

Bin 2015: 63 of cap free
Amount of items: 3
Items: 
Size: 530342 Color: 0
Size: 259288 Color: 1
Size: 210308 Color: 1

Bin 2016: 63 of cap free
Amount of items: 2
Items: 
Size: 509351 Color: 1
Size: 490587 Color: 0

Bin 2017: 63 of cap free
Amount of items: 2
Items: 
Size: 527365 Color: 1
Size: 472573 Color: 0

Bin 2018: 63 of cap free
Amount of items: 2
Items: 
Size: 545519 Color: 1
Size: 454419 Color: 0

Bin 2019: 63 of cap free
Amount of items: 2
Items: 
Size: 555002 Color: 1
Size: 444936 Color: 0

Bin 2020: 63 of cap free
Amount of items: 2
Items: 
Size: 558349 Color: 0
Size: 441589 Color: 1

Bin 2021: 63 of cap free
Amount of items: 2
Items: 
Size: 597147 Color: 1
Size: 402791 Color: 0

Bin 2022: 63 of cap free
Amount of items: 2
Items: 
Size: 618922 Color: 0
Size: 381016 Color: 1

Bin 2023: 63 of cap free
Amount of items: 2
Items: 
Size: 619670 Color: 1
Size: 380268 Color: 0

Bin 2024: 63 of cap free
Amount of items: 2
Items: 
Size: 646350 Color: 1
Size: 353588 Color: 0

Bin 2025: 63 of cap free
Amount of items: 2
Items: 
Size: 668988 Color: 0
Size: 330950 Color: 1

Bin 2026: 63 of cap free
Amount of items: 2
Items: 
Size: 674225 Color: 0
Size: 325713 Color: 1

Bin 2027: 63 of cap free
Amount of items: 2
Items: 
Size: 675473 Color: 0
Size: 324465 Color: 1

Bin 2028: 63 of cap free
Amount of items: 2
Items: 
Size: 678199 Color: 0
Size: 321739 Color: 1

Bin 2029: 63 of cap free
Amount of items: 2
Items: 
Size: 691736 Color: 0
Size: 308202 Color: 1

Bin 2030: 63 of cap free
Amount of items: 2
Items: 
Size: 713392 Color: 0
Size: 286546 Color: 1

Bin 2031: 63 of cap free
Amount of items: 2
Items: 
Size: 717101 Color: 1
Size: 282837 Color: 0

Bin 2032: 63 of cap free
Amount of items: 2
Items: 
Size: 757322 Color: 1
Size: 242616 Color: 0

Bin 2033: 64 of cap free
Amount of items: 2
Items: 
Size: 570105 Color: 0
Size: 429832 Color: 1

Bin 2034: 64 of cap free
Amount of items: 2
Items: 
Size: 506397 Color: 1
Size: 493540 Color: 0

Bin 2035: 64 of cap free
Amount of items: 2
Items: 
Size: 511411 Color: 0
Size: 488526 Color: 1

Bin 2036: 64 of cap free
Amount of items: 2
Items: 
Size: 538800 Color: 0
Size: 461137 Color: 1

Bin 2037: 64 of cap free
Amount of items: 2
Items: 
Size: 539643 Color: 1
Size: 460294 Color: 0

Bin 2038: 64 of cap free
Amount of items: 2
Items: 
Size: 609774 Color: 1
Size: 390163 Color: 0

Bin 2039: 64 of cap free
Amount of items: 2
Items: 
Size: 665708 Color: 1
Size: 334229 Color: 0

Bin 2040: 64 of cap free
Amount of items: 2
Items: 
Size: 708687 Color: 1
Size: 291250 Color: 0

Bin 2041: 64 of cap free
Amount of items: 2
Items: 
Size: 712779 Color: 1
Size: 287158 Color: 0

Bin 2042: 64 of cap free
Amount of items: 2
Items: 
Size: 779103 Color: 1
Size: 220834 Color: 0

Bin 2043: 64 of cap free
Amount of items: 2
Items: 
Size: 794805 Color: 0
Size: 205132 Color: 1

Bin 2044: 64 of cap free
Amount of items: 2
Items: 
Size: 799218 Color: 0
Size: 200719 Color: 1

Bin 2045: 64 of cap free
Amount of items: 3
Items: 
Size: 377754 Color: 1
Size: 316366 Color: 0
Size: 305817 Color: 0

Bin 2046: 65 of cap free
Amount of items: 2
Items: 
Size: 512379 Color: 1
Size: 487557 Color: 0

Bin 2047: 65 of cap free
Amount of items: 2
Items: 
Size: 521599 Color: 0
Size: 478337 Color: 1

Bin 2048: 65 of cap free
Amount of items: 2
Items: 
Size: 524324 Color: 0
Size: 475612 Color: 1

Bin 2049: 65 of cap free
Amount of items: 2
Items: 
Size: 531582 Color: 0
Size: 468354 Color: 1

Bin 2050: 65 of cap free
Amount of items: 2
Items: 
Size: 553173 Color: 1
Size: 446763 Color: 0

Bin 2051: 65 of cap free
Amount of items: 2
Items: 
Size: 577767 Color: 0
Size: 422169 Color: 1

Bin 2052: 65 of cap free
Amount of items: 2
Items: 
Size: 600407 Color: 1
Size: 399529 Color: 0

Bin 2053: 65 of cap free
Amount of items: 2
Items: 
Size: 633598 Color: 1
Size: 366338 Color: 0

Bin 2054: 65 of cap free
Amount of items: 2
Items: 
Size: 650993 Color: 1
Size: 348943 Color: 0

Bin 2055: 65 of cap free
Amount of items: 2
Items: 
Size: 653527 Color: 1
Size: 346409 Color: 0

Bin 2056: 65 of cap free
Amount of items: 2
Items: 
Size: 659604 Color: 1
Size: 340332 Color: 0

Bin 2057: 65 of cap free
Amount of items: 2
Items: 
Size: 663439 Color: 1
Size: 336497 Color: 0

Bin 2058: 65 of cap free
Amount of items: 2
Items: 
Size: 674588 Color: 0
Size: 325348 Color: 1

Bin 2059: 65 of cap free
Amount of items: 2
Items: 
Size: 750128 Color: 0
Size: 249808 Color: 1

Bin 2060: 65 of cap free
Amount of items: 2
Items: 
Size: 754705 Color: 1
Size: 245231 Color: 0

Bin 2061: 65 of cap free
Amount of items: 2
Items: 
Size: 798903 Color: 1
Size: 201033 Color: 0

Bin 2062: 65 of cap free
Amount of items: 2
Items: 
Size: 677856 Color: 0
Size: 322080 Color: 1

Bin 2063: 65 of cap free
Amount of items: 3
Items: 
Size: 342688 Color: 0
Size: 335255 Color: 1
Size: 321993 Color: 1

Bin 2064: 65 of cap free
Amount of items: 2
Items: 
Size: 591838 Color: 1
Size: 408098 Color: 0

Bin 2065: 66 of cap free
Amount of items: 2
Items: 
Size: 561905 Color: 0
Size: 438030 Color: 1

Bin 2066: 66 of cap free
Amount of items: 2
Items: 
Size: 508695 Color: 0
Size: 491240 Color: 1

Bin 2067: 66 of cap free
Amount of items: 2
Items: 
Size: 569884 Color: 0
Size: 430051 Color: 1

Bin 2068: 66 of cap free
Amount of items: 2
Items: 
Size: 574131 Color: 1
Size: 425804 Color: 0

Bin 2069: 66 of cap free
Amount of items: 2
Items: 
Size: 575149 Color: 1
Size: 424786 Color: 0

Bin 2070: 66 of cap free
Amount of items: 2
Items: 
Size: 614088 Color: 0
Size: 385847 Color: 1

Bin 2071: 66 of cap free
Amount of items: 2
Items: 
Size: 685677 Color: 1
Size: 314258 Color: 0

Bin 2072: 66 of cap free
Amount of items: 2
Items: 
Size: 689003 Color: 0
Size: 310932 Color: 1

Bin 2073: 66 of cap free
Amount of items: 2
Items: 
Size: 691822 Color: 0
Size: 308113 Color: 1

Bin 2074: 66 of cap free
Amount of items: 2
Items: 
Size: 724340 Color: 1
Size: 275595 Color: 0

Bin 2075: 66 of cap free
Amount of items: 2
Items: 
Size: 769538 Color: 0
Size: 230397 Color: 1

Bin 2076: 66 of cap free
Amount of items: 2
Items: 
Size: 771352 Color: 1
Size: 228583 Color: 0

Bin 2077: 66 of cap free
Amount of items: 2
Items: 
Size: 778686 Color: 0
Size: 221249 Color: 1

Bin 2078: 66 of cap free
Amount of items: 2
Items: 
Size: 787505 Color: 1
Size: 212430 Color: 0

Bin 2079: 66 of cap free
Amount of items: 2
Items: 
Size: 792318 Color: 0
Size: 207617 Color: 1

Bin 2080: 66 of cap free
Amount of items: 2
Items: 
Size: 674689 Color: 0
Size: 325246 Color: 1

Bin 2081: 67 of cap free
Amount of items: 2
Items: 
Size: 699317 Color: 0
Size: 300617 Color: 1

Bin 2082: 67 of cap free
Amount of items: 2
Items: 
Size: 650093 Color: 1
Size: 349841 Color: 0

Bin 2083: 67 of cap free
Amount of items: 2
Items: 
Size: 562906 Color: 1
Size: 437028 Color: 0

Bin 2084: 67 of cap free
Amount of items: 2
Items: 
Size: 632142 Color: 1
Size: 367792 Color: 0

Bin 2085: 67 of cap free
Amount of items: 2
Items: 
Size: 686860 Color: 0
Size: 313074 Color: 1

Bin 2086: 67 of cap free
Amount of items: 2
Items: 
Size: 709492 Color: 0
Size: 290442 Color: 1

Bin 2087: 67 of cap free
Amount of items: 2
Items: 
Size: 746379 Color: 0
Size: 253555 Color: 1

Bin 2088: 67 of cap free
Amount of items: 2
Items: 
Size: 603659 Color: 1
Size: 396275 Color: 0

Bin 2089: 68 of cap free
Amount of items: 2
Items: 
Size: 795131 Color: 1
Size: 204802 Color: 0

Bin 2090: 68 of cap free
Amount of items: 2
Items: 
Size: 509481 Color: 0
Size: 490452 Color: 1

Bin 2091: 68 of cap free
Amount of items: 2
Items: 
Size: 559730 Color: 1
Size: 440203 Color: 0

Bin 2092: 68 of cap free
Amount of items: 2
Items: 
Size: 613051 Color: 0
Size: 386882 Color: 1

Bin 2093: 68 of cap free
Amount of items: 2
Items: 
Size: 655562 Color: 0
Size: 344371 Color: 1

Bin 2094: 68 of cap free
Amount of items: 2
Items: 
Size: 679578 Color: 1
Size: 320355 Color: 0

Bin 2095: 68 of cap free
Amount of items: 2
Items: 
Size: 729654 Color: 1
Size: 270279 Color: 0

Bin 2096: 68 of cap free
Amount of items: 2
Items: 
Size: 748717 Color: 1
Size: 251216 Color: 0

Bin 2097: 69 of cap free
Amount of items: 2
Items: 
Size: 527918 Color: 0
Size: 472014 Color: 1

Bin 2098: 69 of cap free
Amount of items: 2
Items: 
Size: 529885 Color: 1
Size: 470047 Color: 0

Bin 2099: 69 of cap free
Amount of items: 2
Items: 
Size: 643431 Color: 0
Size: 356501 Color: 1

Bin 2100: 69 of cap free
Amount of items: 2
Items: 
Size: 664791 Color: 0
Size: 335141 Color: 1

Bin 2101: 69 of cap free
Amount of items: 2
Items: 
Size: 681289 Color: 1
Size: 318643 Color: 0

Bin 2102: 69 of cap free
Amount of items: 2
Items: 
Size: 747368 Color: 0
Size: 252564 Color: 1

Bin 2103: 69 of cap free
Amount of items: 2
Items: 
Size: 753069 Color: 0
Size: 246863 Color: 1

Bin 2104: 69 of cap free
Amount of items: 2
Items: 
Size: 774589 Color: 1
Size: 225343 Color: 0

Bin 2105: 69 of cap free
Amount of items: 2
Items: 
Size: 776652 Color: 1
Size: 223280 Color: 0

Bin 2106: 69 of cap free
Amount of items: 2
Items: 
Size: 781114 Color: 0
Size: 218818 Color: 1

Bin 2107: 69 of cap free
Amount of items: 2
Items: 
Size: 560815 Color: 0
Size: 439117 Color: 1

Bin 2108: 69 of cap free
Amount of items: 2
Items: 
Size: 788922 Color: 1
Size: 211010 Color: 0

Bin 2109: 69 of cap free
Amount of items: 2
Items: 
Size: 564980 Color: 0
Size: 434952 Color: 1

Bin 2110: 70 of cap free
Amount of items: 2
Items: 
Size: 526511 Color: 1
Size: 473420 Color: 0

Bin 2111: 70 of cap free
Amount of items: 2
Items: 
Size: 530082 Color: 0
Size: 469849 Color: 1

Bin 2112: 70 of cap free
Amount of items: 2
Items: 
Size: 549816 Color: 1
Size: 450115 Color: 0

Bin 2113: 70 of cap free
Amount of items: 2
Items: 
Size: 571215 Color: 0
Size: 428716 Color: 1

Bin 2114: 70 of cap free
Amount of items: 2
Items: 
Size: 571712 Color: 1
Size: 428219 Color: 0

Bin 2115: 70 of cap free
Amount of items: 2
Items: 
Size: 575945 Color: 0
Size: 423986 Color: 1

Bin 2116: 70 of cap free
Amount of items: 2
Items: 
Size: 598214 Color: 0
Size: 401717 Color: 1

Bin 2117: 70 of cap free
Amount of items: 2
Items: 
Size: 726749 Color: 0
Size: 273182 Color: 1

Bin 2118: 70 of cap free
Amount of items: 2
Items: 
Size: 743271 Color: 1
Size: 256660 Color: 0

Bin 2119: 70 of cap free
Amount of items: 2
Items: 
Size: 797620 Color: 0
Size: 202311 Color: 1

Bin 2120: 70 of cap free
Amount of items: 2
Items: 
Size: 782015 Color: 1
Size: 217916 Color: 0

Bin 2121: 70 of cap free
Amount of items: 2
Items: 
Size: 619888 Color: 0
Size: 380043 Color: 1

Bin 2122: 70 of cap free
Amount of items: 2
Items: 
Size: 754901 Color: 1
Size: 245030 Color: 0

Bin 2123: 71 of cap free
Amount of items: 2
Items: 
Size: 663315 Color: 0
Size: 336615 Color: 1

Bin 2124: 71 of cap free
Amount of items: 2
Items: 
Size: 508587 Color: 0
Size: 491343 Color: 1

Bin 2125: 71 of cap free
Amount of items: 2
Items: 
Size: 515290 Color: 0
Size: 484640 Color: 1

Bin 2126: 71 of cap free
Amount of items: 2
Items: 
Size: 610594 Color: 1
Size: 389336 Color: 0

Bin 2127: 71 of cap free
Amount of items: 2
Items: 
Size: 656093 Color: 1
Size: 343837 Color: 0

Bin 2128: 71 of cap free
Amount of items: 2
Items: 
Size: 663868 Color: 1
Size: 336062 Color: 0

Bin 2129: 71 of cap free
Amount of items: 2
Items: 
Size: 672608 Color: 1
Size: 327322 Color: 0

Bin 2130: 71 of cap free
Amount of items: 2
Items: 
Size: 683662 Color: 1
Size: 316268 Color: 0

Bin 2131: 71 of cap free
Amount of items: 2
Items: 
Size: 738631 Color: 0
Size: 261299 Color: 1

Bin 2132: 71 of cap free
Amount of items: 2
Items: 
Size: 740222 Color: 1
Size: 259708 Color: 0

Bin 2133: 71 of cap free
Amount of items: 2
Items: 
Size: 780344 Color: 1
Size: 219586 Color: 0

Bin 2134: 71 of cap free
Amount of items: 2
Items: 
Size: 799237 Color: 1
Size: 200693 Color: 0

Bin 2135: 72 of cap free
Amount of items: 2
Items: 
Size: 721107 Color: 0
Size: 278822 Color: 1

Bin 2136: 72 of cap free
Amount of items: 2
Items: 
Size: 683566 Color: 1
Size: 316363 Color: 0

Bin 2137: 72 of cap free
Amount of items: 2
Items: 
Size: 507103 Color: 1
Size: 492826 Color: 0

Bin 2138: 72 of cap free
Amount of items: 2
Items: 
Size: 559236 Color: 1
Size: 440693 Color: 0

Bin 2139: 72 of cap free
Amount of items: 2
Items: 
Size: 592187 Color: 0
Size: 407742 Color: 1

Bin 2140: 72 of cap free
Amount of items: 2
Items: 
Size: 636957 Color: 0
Size: 362972 Color: 1

Bin 2141: 72 of cap free
Amount of items: 2
Items: 
Size: 692721 Color: 0
Size: 307208 Color: 1

Bin 2142: 72 of cap free
Amount of items: 2
Items: 
Size: 700522 Color: 1
Size: 299407 Color: 0

Bin 2143: 72 of cap free
Amount of items: 2
Items: 
Size: 713266 Color: 0
Size: 286663 Color: 1

Bin 2144: 72 of cap free
Amount of items: 2
Items: 
Size: 718828 Color: 0
Size: 281101 Color: 1

Bin 2145: 72 of cap free
Amount of items: 2
Items: 
Size: 733728 Color: 1
Size: 266201 Color: 0

Bin 2146: 72 of cap free
Amount of items: 2
Items: 
Size: 742517 Color: 0
Size: 257412 Color: 1

Bin 2147: 72 of cap free
Amount of items: 2
Items: 
Size: 759711 Color: 1
Size: 240218 Color: 0

Bin 2148: 73 of cap free
Amount of items: 2
Items: 
Size: 508075 Color: 1
Size: 491853 Color: 0

Bin 2149: 73 of cap free
Amount of items: 2
Items: 
Size: 540320 Color: 0
Size: 459608 Color: 1

Bin 2150: 73 of cap free
Amount of items: 2
Items: 
Size: 593814 Color: 1
Size: 406114 Color: 0

Bin 2151: 73 of cap free
Amount of items: 2
Items: 
Size: 594239 Color: 1
Size: 405689 Color: 0

Bin 2152: 73 of cap free
Amount of items: 2
Items: 
Size: 623314 Color: 1
Size: 376614 Color: 0

Bin 2153: 73 of cap free
Amount of items: 2
Items: 
Size: 677570 Color: 1
Size: 322358 Color: 0

Bin 2154: 73 of cap free
Amount of items: 2
Items: 
Size: 711284 Color: 0
Size: 288644 Color: 1

Bin 2155: 73 of cap free
Amount of items: 2
Items: 
Size: 745719 Color: 1
Size: 254209 Color: 0

Bin 2156: 73 of cap free
Amount of items: 2
Items: 
Size: 776203 Color: 1
Size: 223725 Color: 0

Bin 2157: 73 of cap free
Amount of items: 2
Items: 
Size: 798366 Color: 0
Size: 201562 Color: 1

Bin 2158: 74 of cap free
Amount of items: 2
Items: 
Size: 527126 Color: 0
Size: 472801 Color: 1

Bin 2159: 74 of cap free
Amount of items: 2
Items: 
Size: 546167 Color: 0
Size: 453760 Color: 1

Bin 2160: 74 of cap free
Amount of items: 2
Items: 
Size: 568466 Color: 0
Size: 431461 Color: 1

Bin 2161: 74 of cap free
Amount of items: 2
Items: 
Size: 578331 Color: 1
Size: 421596 Color: 0

Bin 2162: 74 of cap free
Amount of items: 2
Items: 
Size: 677529 Color: 0
Size: 322398 Color: 1

Bin 2163: 74 of cap free
Amount of items: 2
Items: 
Size: 711387 Color: 1
Size: 288540 Color: 0

Bin 2164: 74 of cap free
Amount of items: 2
Items: 
Size: 729876 Color: 1
Size: 270051 Color: 0

Bin 2165: 74 of cap free
Amount of items: 2
Items: 
Size: 770447 Color: 0
Size: 229480 Color: 1

Bin 2166: 74 of cap free
Amount of items: 2
Items: 
Size: 528310 Color: 0
Size: 471617 Color: 1

Bin 2167: 75 of cap free
Amount of items: 2
Items: 
Size: 720223 Color: 0
Size: 279703 Color: 1

Bin 2168: 75 of cap free
Amount of items: 2
Items: 
Size: 704276 Color: 0
Size: 295650 Color: 1

Bin 2169: 75 of cap free
Amount of items: 2
Items: 
Size: 605493 Color: 0
Size: 394433 Color: 1

Bin 2170: 75 of cap free
Amount of items: 2
Items: 
Size: 612420 Color: 1
Size: 387506 Color: 0

Bin 2171: 75 of cap free
Amount of items: 2
Items: 
Size: 630373 Color: 1
Size: 369553 Color: 0

Bin 2172: 75 of cap free
Amount of items: 2
Items: 
Size: 642814 Color: 0
Size: 357112 Color: 1

Bin 2173: 75 of cap free
Amount of items: 2
Items: 
Size: 681539 Color: 1
Size: 318387 Color: 0

Bin 2174: 75 of cap free
Amount of items: 2
Items: 
Size: 690019 Color: 0
Size: 309907 Color: 1

Bin 2175: 75 of cap free
Amount of items: 2
Items: 
Size: 723555 Color: 0
Size: 276371 Color: 1

Bin 2176: 75 of cap free
Amount of items: 2
Items: 
Size: 724936 Color: 1
Size: 274990 Color: 0

Bin 2177: 75 of cap free
Amount of items: 2
Items: 
Size: 795428 Color: 1
Size: 204498 Color: 0

Bin 2178: 75 of cap free
Amount of items: 2
Items: 
Size: 611863 Color: 0
Size: 388063 Color: 1

Bin 2179: 76 of cap free
Amount of items: 2
Items: 
Size: 771319 Color: 0
Size: 228606 Color: 1

Bin 2180: 76 of cap free
Amount of items: 2
Items: 
Size: 526287 Color: 1
Size: 473638 Color: 0

Bin 2181: 76 of cap free
Amount of items: 2
Items: 
Size: 529221 Color: 0
Size: 470704 Color: 1

Bin 2182: 76 of cap free
Amount of items: 2
Items: 
Size: 544412 Color: 1
Size: 455513 Color: 0

Bin 2183: 76 of cap free
Amount of items: 2
Items: 
Size: 573557 Color: 1
Size: 426368 Color: 0

Bin 2184: 76 of cap free
Amount of items: 2
Items: 
Size: 574586 Color: 1
Size: 425339 Color: 0

Bin 2185: 76 of cap free
Amount of items: 2
Items: 
Size: 629865 Color: 0
Size: 370060 Color: 1

Bin 2186: 76 of cap free
Amount of items: 2
Items: 
Size: 674295 Color: 1
Size: 325630 Color: 0

Bin 2187: 76 of cap free
Amount of items: 2
Items: 
Size: 724397 Color: 0
Size: 275528 Color: 1

Bin 2188: 76 of cap free
Amount of items: 2
Items: 
Size: 739567 Color: 0
Size: 260358 Color: 1

Bin 2189: 76 of cap free
Amount of items: 2
Items: 
Size: 762180 Color: 0
Size: 237745 Color: 1

Bin 2190: 76 of cap free
Amount of items: 2
Items: 
Size: 779217 Color: 1
Size: 220708 Color: 0

Bin 2191: 76 of cap free
Amount of items: 2
Items: 
Size: 780143 Color: 1
Size: 219782 Color: 0

Bin 2192: 76 of cap free
Amount of items: 2
Items: 
Size: 793613 Color: 0
Size: 206312 Color: 1

Bin 2193: 77 of cap free
Amount of items: 2
Items: 
Size: 569622 Color: 1
Size: 430302 Color: 0

Bin 2194: 77 of cap free
Amount of items: 2
Items: 
Size: 610002 Color: 1
Size: 389922 Color: 0

Bin 2195: 77 of cap free
Amount of items: 2
Items: 
Size: 649103 Color: 1
Size: 350821 Color: 0

Bin 2196: 77 of cap free
Amount of items: 2
Items: 
Size: 651230 Color: 1
Size: 348694 Color: 0

Bin 2197: 77 of cap free
Amount of items: 2
Items: 
Size: 671766 Color: 1
Size: 328158 Color: 0

Bin 2198: 77 of cap free
Amount of items: 2
Items: 
Size: 722203 Color: 1
Size: 277721 Color: 0

Bin 2199: 77 of cap free
Amount of items: 2
Items: 
Size: 776438 Color: 1
Size: 223486 Color: 0

Bin 2200: 77 of cap free
Amount of items: 2
Items: 
Size: 504024 Color: 1
Size: 495900 Color: 0

Bin 2201: 77 of cap free
Amount of items: 4
Items: 
Size: 394235 Color: 0
Size: 299962 Color: 1
Size: 168888 Color: 0
Size: 136839 Color: 1

Bin 2202: 78 of cap free
Amount of items: 2
Items: 
Size: 514452 Color: 1
Size: 485471 Color: 0

Bin 2203: 78 of cap free
Amount of items: 2
Items: 
Size: 524975 Color: 1
Size: 474948 Color: 0

Bin 2204: 78 of cap free
Amount of items: 2
Items: 
Size: 544737 Color: 1
Size: 455186 Color: 0

Bin 2205: 78 of cap free
Amount of items: 2
Items: 
Size: 594404 Color: 1
Size: 405519 Color: 0

Bin 2206: 78 of cap free
Amount of items: 2
Items: 
Size: 612633 Color: 0
Size: 387290 Color: 1

Bin 2207: 78 of cap free
Amount of items: 2
Items: 
Size: 627344 Color: 0
Size: 372579 Color: 1

Bin 2208: 78 of cap free
Amount of items: 2
Items: 
Size: 722940 Color: 1
Size: 276983 Color: 0

Bin 2209: 78 of cap free
Amount of items: 2
Items: 
Size: 726219 Color: 0
Size: 273704 Color: 1

Bin 2210: 78 of cap free
Amount of items: 2
Items: 
Size: 770293 Color: 0
Size: 229630 Color: 1

Bin 2211: 79 of cap free
Amount of items: 2
Items: 
Size: 592342 Color: 1
Size: 407580 Color: 0

Bin 2212: 79 of cap free
Amount of items: 2
Items: 
Size: 604120 Color: 1
Size: 395802 Color: 0

Bin 2213: 79 of cap free
Amount of items: 2
Items: 
Size: 604518 Color: 1
Size: 395404 Color: 0

Bin 2214: 79 of cap free
Amount of items: 2
Items: 
Size: 648460 Color: 1
Size: 351462 Color: 0

Bin 2215: 79 of cap free
Amount of items: 2
Items: 
Size: 654344 Color: 1
Size: 345578 Color: 0

Bin 2216: 79 of cap free
Amount of items: 2
Items: 
Size: 678695 Color: 1
Size: 321227 Color: 0

Bin 2217: 79 of cap free
Amount of items: 2
Items: 
Size: 691821 Color: 0
Size: 308101 Color: 1

Bin 2218: 79 of cap free
Amount of items: 2
Items: 
Size: 693317 Color: 1
Size: 306605 Color: 0

Bin 2219: 80 of cap free
Amount of items: 2
Items: 
Size: 701449 Color: 1
Size: 298472 Color: 0

Bin 2220: 80 of cap free
Amount of items: 2
Items: 
Size: 771903 Color: 0
Size: 228018 Color: 1

Bin 2221: 80 of cap free
Amount of items: 2
Items: 
Size: 534332 Color: 0
Size: 465589 Color: 1

Bin 2222: 80 of cap free
Amount of items: 2
Items: 
Size: 543919 Color: 1
Size: 456002 Color: 0

Bin 2223: 80 of cap free
Amount of items: 2
Items: 
Size: 570154 Color: 1
Size: 429767 Color: 0

Bin 2224: 80 of cap free
Amount of items: 2
Items: 
Size: 583906 Color: 0
Size: 416015 Color: 1

Bin 2225: 80 of cap free
Amount of items: 2
Items: 
Size: 617569 Color: 1
Size: 382352 Color: 0

Bin 2226: 80 of cap free
Amount of items: 2
Items: 
Size: 636336 Color: 1
Size: 363585 Color: 0

Bin 2227: 80 of cap free
Amount of items: 2
Items: 
Size: 701555 Color: 1
Size: 298366 Color: 0

Bin 2228: 80 of cap free
Amount of items: 2
Items: 
Size: 722787 Color: 1
Size: 277134 Color: 0

Bin 2229: 80 of cap free
Amount of items: 2
Items: 
Size: 725624 Color: 0
Size: 274297 Color: 1

Bin 2230: 80 of cap free
Amount of items: 2
Items: 
Size: 749113 Color: 1
Size: 250808 Color: 0

Bin 2231: 80 of cap free
Amount of items: 2
Items: 
Size: 778897 Color: 1
Size: 221024 Color: 0

Bin 2232: 81 of cap free
Amount of items: 2
Items: 
Size: 548185 Color: 1
Size: 451735 Color: 0

Bin 2233: 81 of cap free
Amount of items: 2
Items: 
Size: 595518 Color: 0
Size: 404402 Color: 1

Bin 2234: 81 of cap free
Amount of items: 2
Items: 
Size: 602913 Color: 1
Size: 397007 Color: 0

Bin 2235: 81 of cap free
Amount of items: 2
Items: 
Size: 647765 Color: 0
Size: 352155 Color: 1

Bin 2236: 81 of cap free
Amount of items: 2
Items: 
Size: 677173 Color: 1
Size: 322747 Color: 0

Bin 2237: 81 of cap free
Amount of items: 2
Items: 
Size: 726623 Color: 1
Size: 273297 Color: 0

Bin 2238: 81 of cap free
Amount of items: 2
Items: 
Size: 760466 Color: 0
Size: 239454 Color: 1

Bin 2239: 81 of cap free
Amount of items: 2
Items: 
Size: 770846 Color: 0
Size: 229074 Color: 1

Bin 2240: 81 of cap free
Amount of items: 3
Items: 
Size: 774781 Color: 0
Size: 114652 Color: 1
Size: 110487 Color: 0

Bin 2241: 82 of cap free
Amount of items: 2
Items: 
Size: 512850 Color: 0
Size: 487069 Color: 1

Bin 2242: 82 of cap free
Amount of items: 2
Items: 
Size: 532523 Color: 0
Size: 467396 Color: 1

Bin 2243: 82 of cap free
Amount of items: 2
Items: 
Size: 602162 Color: 0
Size: 397757 Color: 1

Bin 2244: 82 of cap free
Amount of items: 2
Items: 
Size: 690250 Color: 0
Size: 309669 Color: 1

Bin 2245: 82 of cap free
Amount of items: 2
Items: 
Size: 794787 Color: 1
Size: 205132 Color: 0

Bin 2246: 82 of cap free
Amount of items: 2
Items: 
Size: 667554 Color: 1
Size: 332365 Color: 0

Bin 2247: 83 of cap free
Amount of items: 2
Items: 
Size: 533351 Color: 0
Size: 466567 Color: 1

Bin 2248: 83 of cap free
Amount of items: 2
Items: 
Size: 574565 Color: 0
Size: 425353 Color: 1

Bin 2249: 83 of cap free
Amount of items: 2
Items: 
Size: 600665 Color: 0
Size: 399253 Color: 1

Bin 2250: 83 of cap free
Amount of items: 2
Items: 
Size: 605246 Color: 1
Size: 394672 Color: 0

Bin 2251: 83 of cap free
Amount of items: 2
Items: 
Size: 626674 Color: 0
Size: 373244 Color: 1

Bin 2252: 83 of cap free
Amount of items: 2
Items: 
Size: 645387 Color: 0
Size: 354531 Color: 1

Bin 2253: 83 of cap free
Amount of items: 2
Items: 
Size: 655474 Color: 0
Size: 344444 Color: 1

Bin 2254: 83 of cap free
Amount of items: 2
Items: 
Size: 666359 Color: 1
Size: 333559 Color: 0

Bin 2255: 83 of cap free
Amount of items: 2
Items: 
Size: 670413 Color: 0
Size: 329505 Color: 1

Bin 2256: 83 of cap free
Amount of items: 2
Items: 
Size: 776749 Color: 1
Size: 223169 Color: 0

Bin 2257: 83 of cap free
Amount of items: 2
Items: 
Size: 794627 Color: 0
Size: 205291 Color: 1

Bin 2258: 83 of cap free
Amount of items: 2
Items: 
Size: 538386 Color: 1
Size: 461532 Color: 0

Bin 2259: 84 of cap free
Amount of items: 2
Items: 
Size: 661026 Color: 1
Size: 338891 Color: 0

Bin 2260: 84 of cap free
Amount of items: 2
Items: 
Size: 681630 Color: 1
Size: 318287 Color: 0

Bin 2261: 84 of cap free
Amount of items: 2
Items: 
Size: 683373 Color: 1
Size: 316544 Color: 0

Bin 2262: 84 of cap free
Amount of items: 2
Items: 
Size: 796551 Color: 1
Size: 203366 Color: 0

Bin 2263: 85 of cap free
Amount of items: 2
Items: 
Size: 585704 Color: 0
Size: 414212 Color: 1

Bin 2264: 85 of cap free
Amount of items: 2
Items: 
Size: 621721 Color: 1
Size: 378195 Color: 0

Bin 2265: 85 of cap free
Amount of items: 2
Items: 
Size: 651528 Color: 0
Size: 348388 Color: 1

Bin 2266: 85 of cap free
Amount of items: 2
Items: 
Size: 659466 Color: 1
Size: 340450 Color: 0

Bin 2267: 85 of cap free
Amount of items: 2
Items: 
Size: 787725 Color: 1
Size: 212191 Color: 0

Bin 2268: 86 of cap free
Amount of items: 2
Items: 
Size: 520143 Color: 0
Size: 479772 Color: 1

Bin 2269: 86 of cap free
Amount of items: 2
Items: 
Size: 528557 Color: 1
Size: 471358 Color: 0

Bin 2270: 86 of cap free
Amount of items: 2
Items: 
Size: 539786 Color: 1
Size: 460129 Color: 0

Bin 2271: 86 of cap free
Amount of items: 2
Items: 
Size: 591979 Color: 0
Size: 407936 Color: 1

Bin 2272: 86 of cap free
Amount of items: 2
Items: 
Size: 605082 Color: 1
Size: 394833 Color: 0

Bin 2273: 86 of cap free
Amount of items: 2
Items: 
Size: 622331 Color: 1
Size: 377584 Color: 0

Bin 2274: 86 of cap free
Amount of items: 2
Items: 
Size: 646719 Color: 0
Size: 353196 Color: 1

Bin 2275: 86 of cap free
Amount of items: 2
Items: 
Size: 648091 Color: 1
Size: 351824 Color: 0

Bin 2276: 86 of cap free
Amount of items: 2
Items: 
Size: 654284 Color: 0
Size: 345631 Color: 1

Bin 2277: 86 of cap free
Amount of items: 2
Items: 
Size: 712614 Color: 0
Size: 287301 Color: 1

Bin 2278: 86 of cap free
Amount of items: 2
Items: 
Size: 767343 Color: 0
Size: 232572 Color: 1

Bin 2279: 86 of cap free
Amount of items: 2
Items: 
Size: 777864 Color: 0
Size: 222051 Color: 1

Bin 2280: 87 of cap free
Amount of items: 2
Items: 
Size: 500474 Color: 0
Size: 499440 Color: 1

Bin 2281: 87 of cap free
Amount of items: 2
Items: 
Size: 511479 Color: 1
Size: 488435 Color: 0

Bin 2282: 87 of cap free
Amount of items: 2
Items: 
Size: 573549 Color: 1
Size: 426365 Color: 0

Bin 2283: 87 of cap free
Amount of items: 2
Items: 
Size: 575592 Color: 1
Size: 424322 Color: 0

Bin 2284: 87 of cap free
Amount of items: 2
Items: 
Size: 576978 Color: 0
Size: 422936 Color: 1

Bin 2285: 87 of cap free
Amount of items: 2
Items: 
Size: 629312 Color: 0
Size: 370602 Color: 1

Bin 2286: 87 of cap free
Amount of items: 2
Items: 
Size: 632631 Color: 0
Size: 367283 Color: 1

Bin 2287: 87 of cap free
Amount of items: 2
Items: 
Size: 659174 Color: 1
Size: 340740 Color: 0

Bin 2288: 87 of cap free
Amount of items: 2
Items: 
Size: 723587 Color: 1
Size: 276327 Color: 0

Bin 2289: 87 of cap free
Amount of items: 2
Items: 
Size: 745612 Color: 0
Size: 254302 Color: 1

Bin 2290: 88 of cap free
Amount of items: 2
Items: 
Size: 518346 Color: 1
Size: 481567 Color: 0

Bin 2291: 88 of cap free
Amount of items: 2
Items: 
Size: 601041 Color: 1
Size: 398872 Color: 0

Bin 2292: 88 of cap free
Amount of items: 2
Items: 
Size: 609179 Color: 1
Size: 390734 Color: 0

Bin 2293: 88 of cap free
Amount of items: 2
Items: 
Size: 664773 Color: 1
Size: 335140 Color: 0

Bin 2294: 88 of cap free
Amount of items: 2
Items: 
Size: 671039 Color: 1
Size: 328874 Color: 0

Bin 2295: 88 of cap free
Amount of items: 2
Items: 
Size: 763378 Color: 0
Size: 236535 Color: 1

Bin 2296: 88 of cap free
Amount of items: 2
Items: 
Size: 764581 Color: 0
Size: 235332 Color: 1

Bin 2297: 89 of cap free
Amount of items: 2
Items: 
Size: 525313 Color: 0
Size: 474599 Color: 1

Bin 2298: 89 of cap free
Amount of items: 2
Items: 
Size: 559313 Color: 1
Size: 440599 Color: 0

Bin 2299: 89 of cap free
Amount of items: 2
Items: 
Size: 577457 Color: 1
Size: 422455 Color: 0

Bin 2300: 89 of cap free
Amount of items: 2
Items: 
Size: 771091 Color: 1
Size: 228821 Color: 0

Bin 2301: 89 of cap free
Amount of items: 2
Items: 
Size: 714332 Color: 1
Size: 285580 Color: 0

Bin 2302: 90 of cap free
Amount of items: 2
Items: 
Size: 772072 Color: 1
Size: 227839 Color: 0

Bin 2303: 90 of cap free
Amount of items: 2
Items: 
Size: 542744 Color: 0
Size: 457167 Color: 1

Bin 2304: 90 of cap free
Amount of items: 2
Items: 
Size: 579623 Color: 1
Size: 420288 Color: 0

Bin 2305: 90 of cap free
Amount of items: 2
Items: 
Size: 591088 Color: 1
Size: 408823 Color: 0

Bin 2306: 90 of cap free
Amount of items: 2
Items: 
Size: 596394 Color: 1
Size: 403517 Color: 0

Bin 2307: 90 of cap free
Amount of items: 2
Items: 
Size: 639641 Color: 1
Size: 360270 Color: 0

Bin 2308: 90 of cap free
Amount of items: 2
Items: 
Size: 701966 Color: 1
Size: 297945 Color: 0

Bin 2309: 90 of cap free
Amount of items: 2
Items: 
Size: 759500 Color: 1
Size: 240411 Color: 0

Bin 2310: 90 of cap free
Amount of items: 2
Items: 
Size: 769400 Color: 1
Size: 230511 Color: 0

Bin 2311: 90 of cap free
Amount of items: 2
Items: 
Size: 651431 Color: 1
Size: 348480 Color: 0

Bin 2312: 90 of cap free
Amount of items: 2
Items: 
Size: 795592 Color: 0
Size: 204319 Color: 1

Bin 2313: 91 of cap free
Amount of items: 2
Items: 
Size: 513975 Color: 0
Size: 485935 Color: 1

Bin 2314: 91 of cap free
Amount of items: 2
Items: 
Size: 527248 Color: 0
Size: 472662 Color: 1

Bin 2315: 91 of cap free
Amount of items: 2
Items: 
Size: 538584 Color: 0
Size: 461326 Color: 1

Bin 2316: 91 of cap free
Amount of items: 2
Items: 
Size: 557441 Color: 1
Size: 442469 Color: 0

Bin 2317: 91 of cap free
Amount of items: 2
Items: 
Size: 642685 Color: 0
Size: 357225 Color: 1

Bin 2318: 91 of cap free
Amount of items: 2
Items: 
Size: 703858 Color: 0
Size: 296052 Color: 1

Bin 2319: 91 of cap free
Amount of items: 2
Items: 
Size: 733886 Color: 1
Size: 266024 Color: 0

Bin 2320: 91 of cap free
Amount of items: 2
Items: 
Size: 767966 Color: 1
Size: 231944 Color: 0

Bin 2321: 91 of cap free
Amount of items: 2
Items: 
Size: 782968 Color: 1
Size: 216942 Color: 0

Bin 2322: 91 of cap free
Amount of items: 2
Items: 
Size: 775945 Color: 1
Size: 223965 Color: 0

Bin 2323: 91 of cap free
Amount of items: 2
Items: 
Size: 770088 Color: 0
Size: 229822 Color: 1

Bin 2324: 92 of cap free
Amount of items: 2
Items: 
Size: 504765 Color: 0
Size: 495144 Color: 1

Bin 2325: 92 of cap free
Amount of items: 2
Items: 
Size: 505380 Color: 1
Size: 494529 Color: 0

Bin 2326: 92 of cap free
Amount of items: 2
Items: 
Size: 512168 Color: 1
Size: 487741 Color: 0

Bin 2327: 92 of cap free
Amount of items: 2
Items: 
Size: 520807 Color: 1
Size: 479102 Color: 0

Bin 2328: 92 of cap free
Amount of items: 2
Items: 
Size: 521044 Color: 0
Size: 478865 Color: 1

Bin 2329: 92 of cap free
Amount of items: 2
Items: 
Size: 605048 Color: 0
Size: 394861 Color: 1

Bin 2330: 92 of cap free
Amount of items: 2
Items: 
Size: 627635 Color: 0
Size: 372274 Color: 1

Bin 2331: 92 of cap free
Amount of items: 2
Items: 
Size: 639889 Color: 0
Size: 360020 Color: 1

Bin 2332: 92 of cap free
Amount of items: 2
Items: 
Size: 652318 Color: 1
Size: 347591 Color: 0

Bin 2333: 92 of cap free
Amount of items: 2
Items: 
Size: 737000 Color: 1
Size: 262909 Color: 0

Bin 2334: 93 of cap free
Amount of items: 2
Items: 
Size: 541575 Color: 1
Size: 458333 Color: 0

Bin 2335: 93 of cap free
Amount of items: 2
Items: 
Size: 534321 Color: 0
Size: 465587 Color: 1

Bin 2336: 93 of cap free
Amount of items: 2
Items: 
Size: 550515 Color: 0
Size: 449393 Color: 1

Bin 2337: 93 of cap free
Amount of items: 2
Items: 
Size: 621719 Color: 1
Size: 378189 Color: 0

Bin 2338: 93 of cap free
Amount of items: 2
Items: 
Size: 661203 Color: 1
Size: 338705 Color: 0

Bin 2339: 93 of cap free
Amount of items: 2
Items: 
Size: 709850 Color: 0
Size: 290058 Color: 1

Bin 2340: 93 of cap free
Amount of items: 2
Items: 
Size: 798381 Color: 1
Size: 201527 Color: 0

Bin 2341: 93 of cap free
Amount of items: 2
Items: 
Size: 623854 Color: 0
Size: 376054 Color: 1

Bin 2342: 93 of cap free
Amount of items: 2
Items: 
Size: 571877 Color: 1
Size: 428031 Color: 0

Bin 2343: 94 of cap free
Amount of items: 5
Items: 
Size: 368708 Color: 1
Size: 160139 Color: 0
Size: 158122 Color: 1
Size: 157402 Color: 1
Size: 155536 Color: 0

Bin 2344: 94 of cap free
Amount of items: 3
Items: 
Size: 347140 Color: 0
Size: 336288 Color: 1
Size: 316479 Color: 1

Bin 2345: 94 of cap free
Amount of items: 2
Items: 
Size: 574112 Color: 1
Size: 425795 Color: 0

Bin 2346: 94 of cap free
Amount of items: 2
Items: 
Size: 631976 Color: 0
Size: 367931 Color: 1

Bin 2347: 94 of cap free
Amount of items: 2
Items: 
Size: 687269 Color: 1
Size: 312638 Color: 0

Bin 2348: 94 of cap free
Amount of items: 2
Items: 
Size: 735853 Color: 1
Size: 264054 Color: 0

Bin 2349: 94 of cap free
Amount of items: 2
Items: 
Size: 796357 Color: 0
Size: 203550 Color: 1

Bin 2350: 95 of cap free
Amount of items: 2
Items: 
Size: 626538 Color: 1
Size: 373368 Color: 0

Bin 2351: 95 of cap free
Amount of items: 2
Items: 
Size: 687834 Color: 1
Size: 312072 Color: 0

Bin 2352: 95 of cap free
Amount of items: 2
Items: 
Size: 722619 Color: 1
Size: 277287 Color: 0

Bin 2353: 95 of cap free
Amount of items: 2
Items: 
Size: 734993 Color: 0
Size: 264913 Color: 1

Bin 2354: 95 of cap free
Amount of items: 2
Items: 
Size: 792313 Color: 0
Size: 207593 Color: 1

Bin 2355: 96 of cap free
Amount of items: 2
Items: 
Size: 516839 Color: 0
Size: 483066 Color: 1

Bin 2356: 96 of cap free
Amount of items: 2
Items: 
Size: 522772 Color: 0
Size: 477133 Color: 1

Bin 2357: 96 of cap free
Amount of items: 2
Items: 
Size: 536017 Color: 0
Size: 463888 Color: 1

Bin 2358: 96 of cap free
Amount of items: 2
Items: 
Size: 540113 Color: 1
Size: 459792 Color: 0

Bin 2359: 96 of cap free
Amount of items: 2
Items: 
Size: 546586 Color: 1
Size: 453319 Color: 0

Bin 2360: 96 of cap free
Amount of items: 2
Items: 
Size: 547217 Color: 0
Size: 452688 Color: 1

Bin 2361: 96 of cap free
Amount of items: 2
Items: 
Size: 573080 Color: 0
Size: 426825 Color: 1

Bin 2362: 96 of cap free
Amount of items: 2
Items: 
Size: 599803 Color: 0
Size: 400102 Color: 1

Bin 2363: 96 of cap free
Amount of items: 2
Items: 
Size: 623202 Color: 1
Size: 376703 Color: 0

Bin 2364: 96 of cap free
Amount of items: 2
Items: 
Size: 632630 Color: 0
Size: 367275 Color: 1

Bin 2365: 96 of cap free
Amount of items: 2
Items: 
Size: 639188 Color: 0
Size: 360717 Color: 1

Bin 2366: 96 of cap free
Amount of items: 2
Items: 
Size: 682838 Color: 1
Size: 317067 Color: 0

Bin 2367: 96 of cap free
Amount of items: 2
Items: 
Size: 760501 Color: 1
Size: 239404 Color: 0

Bin 2368: 97 of cap free
Amount of items: 2
Items: 
Size: 599088 Color: 0
Size: 400816 Color: 1

Bin 2369: 97 of cap free
Amount of items: 2
Items: 
Size: 516536 Color: 1
Size: 483368 Color: 0

Bin 2370: 97 of cap free
Amount of items: 2
Items: 
Size: 607543 Color: 0
Size: 392361 Color: 1

Bin 2371: 97 of cap free
Amount of items: 2
Items: 
Size: 611872 Color: 1
Size: 388032 Color: 0

Bin 2372: 97 of cap free
Amount of items: 2
Items: 
Size: 614117 Color: 1
Size: 385787 Color: 0

Bin 2373: 97 of cap free
Amount of items: 2
Items: 
Size: 635180 Color: 0
Size: 364724 Color: 1

Bin 2374: 97 of cap free
Amount of items: 2
Items: 
Size: 671840 Color: 0
Size: 328064 Color: 1

Bin 2375: 97 of cap free
Amount of items: 2
Items: 
Size: 714988 Color: 1
Size: 284916 Color: 0

Bin 2376: 97 of cap free
Amount of items: 2
Items: 
Size: 777490 Color: 1
Size: 222414 Color: 0

Bin 2377: 97 of cap free
Amount of items: 2
Items: 
Size: 775107 Color: 1
Size: 224797 Color: 0

Bin 2378: 97 of cap free
Amount of items: 3
Items: 
Size: 334352 Color: 0
Size: 333368 Color: 1
Size: 332184 Color: 0

Bin 2379: 98 of cap free
Amount of items: 2
Items: 
Size: 589691 Color: 0
Size: 410212 Color: 1

Bin 2380: 98 of cap free
Amount of items: 2
Items: 
Size: 598208 Color: 0
Size: 401695 Color: 1

Bin 2381: 98 of cap free
Amount of items: 2
Items: 
Size: 603771 Color: 0
Size: 396132 Color: 1

Bin 2382: 98 of cap free
Amount of items: 2
Items: 
Size: 608503 Color: 0
Size: 391400 Color: 1

Bin 2383: 98 of cap free
Amount of items: 2
Items: 
Size: 609422 Color: 1
Size: 390481 Color: 0

Bin 2384: 98 of cap free
Amount of items: 2
Items: 
Size: 630590 Color: 1
Size: 369313 Color: 0

Bin 2385: 98 of cap free
Amount of items: 2
Items: 
Size: 651088 Color: 1
Size: 348815 Color: 0

Bin 2386: 98 of cap free
Amount of items: 2
Items: 
Size: 727711 Color: 1
Size: 272192 Color: 0

Bin 2387: 98 of cap free
Amount of items: 2
Items: 
Size: 790850 Color: 0
Size: 209053 Color: 1

Bin 2388: 98 of cap free
Amount of items: 2
Items: 
Size: 792584 Color: 0
Size: 207319 Color: 1

Bin 2389: 99 of cap free
Amount of items: 2
Items: 
Size: 597907 Color: 0
Size: 401995 Color: 1

Bin 2390: 99 of cap free
Amount of items: 2
Items: 
Size: 602129 Color: 1
Size: 397773 Color: 0

Bin 2391: 99 of cap free
Amount of items: 2
Items: 
Size: 644608 Color: 1
Size: 355294 Color: 0

Bin 2392: 99 of cap free
Amount of items: 2
Items: 
Size: 660420 Color: 0
Size: 339482 Color: 1

Bin 2393: 99 of cap free
Amount of items: 2
Items: 
Size: 752399 Color: 0
Size: 247503 Color: 1

Bin 2394: 100 of cap free
Amount of items: 2
Items: 
Size: 758881 Color: 0
Size: 241020 Color: 1

Bin 2395: 100 of cap free
Amount of items: 2
Items: 
Size: 697886 Color: 0
Size: 302015 Color: 1

Bin 2396: 100 of cap free
Amount of items: 2
Items: 
Size: 500107 Color: 0
Size: 499794 Color: 1

Bin 2397: 100 of cap free
Amount of items: 2
Items: 
Size: 532992 Color: 1
Size: 466909 Color: 0

Bin 2398: 100 of cap free
Amount of items: 2
Items: 
Size: 536148 Color: 1
Size: 463753 Color: 0

Bin 2399: 100 of cap free
Amount of items: 2
Items: 
Size: 539400 Color: 0
Size: 460501 Color: 1

Bin 2400: 100 of cap free
Amount of items: 2
Items: 
Size: 577435 Color: 0
Size: 422466 Color: 1

Bin 2401: 100 of cap free
Amount of items: 2
Items: 
Size: 590993 Color: 0
Size: 408908 Color: 1

Bin 2402: 100 of cap free
Amount of items: 2
Items: 
Size: 595408 Color: 1
Size: 404493 Color: 0

Bin 2403: 100 of cap free
Amount of items: 2
Items: 
Size: 600494 Color: 1
Size: 399407 Color: 0

Bin 2404: 100 of cap free
Amount of items: 2
Items: 
Size: 687341 Color: 0
Size: 312560 Color: 1

Bin 2405: 100 of cap free
Amount of items: 2
Items: 
Size: 714000 Color: 0
Size: 285901 Color: 1

Bin 2406: 100 of cap free
Amount of items: 2
Items: 
Size: 739811 Color: 0
Size: 260090 Color: 1

Bin 2407: 100 of cap free
Amount of items: 2
Items: 
Size: 771223 Color: 1
Size: 228678 Color: 0

Bin 2408: 100 of cap free
Amount of items: 2
Items: 
Size: 784883 Color: 1
Size: 215018 Color: 0

Bin 2409: 100 of cap free
Amount of items: 2
Items: 
Size: 744537 Color: 1
Size: 255364 Color: 0

Bin 2410: 101 of cap free
Amount of items: 2
Items: 
Size: 541670 Color: 0
Size: 458230 Color: 1

Bin 2411: 101 of cap free
Amount of items: 2
Items: 
Size: 730003 Color: 0
Size: 269897 Color: 1

Bin 2412: 101 of cap free
Amount of items: 2
Items: 
Size: 502013 Color: 1
Size: 497887 Color: 0

Bin 2413: 101 of cap free
Amount of items: 2
Items: 
Size: 505271 Color: 1
Size: 494629 Color: 0

Bin 2414: 101 of cap free
Amount of items: 2
Items: 
Size: 506990 Color: 0
Size: 492910 Color: 1

Bin 2415: 101 of cap free
Amount of items: 2
Items: 
Size: 513779 Color: 1
Size: 486121 Color: 0

Bin 2416: 101 of cap free
Amount of items: 2
Items: 
Size: 522461 Color: 1
Size: 477439 Color: 0

Bin 2417: 101 of cap free
Amount of items: 2
Items: 
Size: 589401 Color: 1
Size: 410499 Color: 0

Bin 2418: 101 of cap free
Amount of items: 2
Items: 
Size: 597232 Color: 0
Size: 402668 Color: 1

Bin 2419: 101 of cap free
Amount of items: 2
Items: 
Size: 621604 Color: 0
Size: 378296 Color: 1

Bin 2420: 101 of cap free
Amount of items: 2
Items: 
Size: 645614 Color: 1
Size: 354286 Color: 0

Bin 2421: 101 of cap free
Amount of items: 2
Items: 
Size: 657478 Color: 0
Size: 342422 Color: 1

Bin 2422: 101 of cap free
Amount of items: 2
Items: 
Size: 775894 Color: 0
Size: 224006 Color: 1

Bin 2423: 101 of cap free
Amount of items: 2
Items: 
Size: 785450 Color: 1
Size: 214450 Color: 0

Bin 2424: 101 of cap free
Amount of items: 2
Items: 
Size: 793483 Color: 1
Size: 206417 Color: 0

Bin 2425: 102 of cap free
Amount of items: 2
Items: 
Size: 689217 Color: 0
Size: 310682 Color: 1

Bin 2426: 102 of cap free
Amount of items: 2
Items: 
Size: 544493 Color: 1
Size: 455406 Color: 0

Bin 2427: 102 of cap free
Amount of items: 2
Items: 
Size: 629959 Color: 1
Size: 369940 Color: 0

Bin 2428: 102 of cap free
Amount of items: 2
Items: 
Size: 754042 Color: 1
Size: 245857 Color: 0

Bin 2429: 102 of cap free
Amount of items: 2
Items: 
Size: 758687 Color: 1
Size: 241212 Color: 0

Bin 2430: 103 of cap free
Amount of items: 2
Items: 
Size: 518593 Color: 0
Size: 481305 Color: 1

Bin 2431: 103 of cap free
Amount of items: 2
Items: 
Size: 531720 Color: 0
Size: 468178 Color: 1

Bin 2432: 103 of cap free
Amount of items: 2
Items: 
Size: 600177 Color: 1
Size: 399721 Color: 0

Bin 2433: 103 of cap free
Amount of items: 2
Items: 
Size: 636741 Color: 0
Size: 363157 Color: 1

Bin 2434: 103 of cap free
Amount of items: 2
Items: 
Size: 647880 Color: 0
Size: 352018 Color: 1

Bin 2435: 103 of cap free
Amount of items: 2
Items: 
Size: 690009 Color: 0
Size: 309889 Color: 1

Bin 2436: 103 of cap free
Amount of items: 2
Items: 
Size: 726492 Color: 0
Size: 273406 Color: 1

Bin 2437: 103 of cap free
Amount of items: 2
Items: 
Size: 730492 Color: 0
Size: 269406 Color: 1

Bin 2438: 103 of cap free
Amount of items: 2
Items: 
Size: 773089 Color: 1
Size: 226809 Color: 0

Bin 2439: 104 of cap free
Amount of items: 2
Items: 
Size: 520377 Color: 0
Size: 479520 Color: 1

Bin 2440: 104 of cap free
Amount of items: 2
Items: 
Size: 523096 Color: 1
Size: 476801 Color: 0

Bin 2441: 104 of cap free
Amount of items: 2
Items: 
Size: 523617 Color: 0
Size: 476280 Color: 1

Bin 2442: 104 of cap free
Amount of items: 2
Items: 
Size: 560315 Color: 1
Size: 439582 Color: 0

Bin 2443: 104 of cap free
Amount of items: 2
Items: 
Size: 700190 Color: 0
Size: 299707 Color: 1

Bin 2444: 104 of cap free
Amount of items: 2
Items: 
Size: 716171 Color: 0
Size: 283726 Color: 1

Bin 2445: 104 of cap free
Amount of items: 2
Items: 
Size: 722360 Color: 1
Size: 277537 Color: 0

Bin 2446: 104 of cap free
Amount of items: 2
Items: 
Size: 786695 Color: 0
Size: 213202 Color: 1

Bin 2447: 104 of cap free
Amount of items: 2
Items: 
Size: 792531 Color: 1
Size: 207366 Color: 0

Bin 2448: 105 of cap free
Amount of items: 3
Items: 
Size: 604232 Color: 0
Size: 236122 Color: 1
Size: 159542 Color: 0

Bin 2449: 105 of cap free
Amount of items: 2
Items: 
Size: 516392 Color: 0
Size: 483504 Color: 1

Bin 2450: 105 of cap free
Amount of items: 2
Items: 
Size: 590540 Color: 1
Size: 409356 Color: 0

Bin 2451: 105 of cap free
Amount of items: 2
Items: 
Size: 601686 Color: 1
Size: 398210 Color: 0

Bin 2452: 105 of cap free
Amount of items: 2
Items: 
Size: 645021 Color: 1
Size: 354875 Color: 0

Bin 2453: 105 of cap free
Amount of items: 2
Items: 
Size: 668877 Color: 1
Size: 331019 Color: 0

Bin 2454: 105 of cap free
Amount of items: 2
Items: 
Size: 693763 Color: 1
Size: 306133 Color: 0

Bin 2455: 105 of cap free
Amount of items: 2
Items: 
Size: 708686 Color: 1
Size: 291210 Color: 0

Bin 2456: 105 of cap free
Amount of items: 2
Items: 
Size: 724582 Color: 0
Size: 275314 Color: 1

Bin 2457: 105 of cap free
Amount of items: 2
Items: 
Size: 740847 Color: 0
Size: 259049 Color: 1

Bin 2458: 105 of cap free
Amount of items: 2
Items: 
Size: 741329 Color: 1
Size: 258567 Color: 0

Bin 2459: 105 of cap free
Amount of items: 2
Items: 
Size: 613914 Color: 0
Size: 385982 Color: 1

Bin 2460: 106 of cap free
Amount of items: 2
Items: 
Size: 532257 Color: 0
Size: 467638 Color: 1

Bin 2461: 106 of cap free
Amount of items: 2
Items: 
Size: 682718 Color: 0
Size: 317177 Color: 1

Bin 2462: 106 of cap free
Amount of items: 2
Items: 
Size: 594489 Color: 1
Size: 405406 Color: 0

Bin 2463: 107 of cap free
Amount of items: 2
Items: 
Size: 501797 Color: 1
Size: 498097 Color: 0

Bin 2464: 107 of cap free
Amount of items: 2
Items: 
Size: 538048 Color: 1
Size: 461846 Color: 0

Bin 2465: 107 of cap free
Amount of items: 2
Items: 
Size: 558354 Color: 1
Size: 441540 Color: 0

Bin 2466: 107 of cap free
Amount of items: 2
Items: 
Size: 574569 Color: 1
Size: 425325 Color: 0

Bin 2467: 107 of cap free
Amount of items: 2
Items: 
Size: 641336 Color: 1
Size: 358558 Color: 0

Bin 2468: 107 of cap free
Amount of items: 2
Items: 
Size: 654638 Color: 1
Size: 345256 Color: 0

Bin 2469: 107 of cap free
Amount of items: 2
Items: 
Size: 658295 Color: 0
Size: 341599 Color: 1

Bin 2470: 107 of cap free
Amount of items: 2
Items: 
Size: 692051 Color: 1
Size: 307843 Color: 0

Bin 2471: 107 of cap free
Amount of items: 2
Items: 
Size: 696173 Color: 1
Size: 303721 Color: 0

Bin 2472: 107 of cap free
Amount of items: 2
Items: 
Size: 795445 Color: 0
Size: 204449 Color: 1

Bin 2473: 108 of cap free
Amount of items: 2
Items: 
Size: 551643 Color: 0
Size: 448250 Color: 1

Bin 2474: 108 of cap free
Amount of items: 2
Items: 
Size: 561796 Color: 0
Size: 438097 Color: 1

Bin 2475: 108 of cap free
Amount of items: 2
Items: 
Size: 596518 Color: 1
Size: 403375 Color: 0

Bin 2476: 108 of cap free
Amount of items: 2
Items: 
Size: 742062 Color: 1
Size: 257831 Color: 0

Bin 2477: 108 of cap free
Amount of items: 2
Items: 
Size: 746869 Color: 0
Size: 253024 Color: 1

Bin 2478: 108 of cap free
Amount of items: 2
Items: 
Size: 758941 Color: 1
Size: 240952 Color: 0

Bin 2479: 108 of cap free
Amount of items: 2
Items: 
Size: 762997 Color: 0
Size: 236896 Color: 1

Bin 2480: 109 of cap free
Amount of items: 2
Items: 
Size: 514502 Color: 0
Size: 485390 Color: 1

Bin 2481: 109 of cap free
Amount of items: 2
Items: 
Size: 528888 Color: 0
Size: 471004 Color: 1

Bin 2482: 109 of cap free
Amount of items: 2
Items: 
Size: 636374 Color: 0
Size: 363518 Color: 1

Bin 2483: 109 of cap free
Amount of items: 2
Items: 
Size: 665508 Color: 0
Size: 334384 Color: 1

Bin 2484: 109 of cap free
Amount of items: 2
Items: 
Size: 687005 Color: 0
Size: 312887 Color: 1

Bin 2485: 109 of cap free
Amount of items: 2
Items: 
Size: 720955 Color: 0
Size: 278937 Color: 1

Bin 2486: 109 of cap free
Amount of items: 2
Items: 
Size: 757860 Color: 0
Size: 242032 Color: 1

Bin 2487: 109 of cap free
Amount of items: 2
Items: 
Size: 778773 Color: 0
Size: 221119 Color: 1

Bin 2488: 109 of cap free
Amount of items: 2
Items: 
Size: 779884 Color: 0
Size: 220008 Color: 1

Bin 2489: 109 of cap free
Amount of items: 2
Items: 
Size: 796094 Color: 0
Size: 203798 Color: 1

Bin 2490: 110 of cap free
Amount of items: 2
Items: 
Size: 501304 Color: 1
Size: 498587 Color: 0

Bin 2491: 110 of cap free
Amount of items: 2
Items: 
Size: 536146 Color: 1
Size: 463745 Color: 0

Bin 2492: 110 of cap free
Amount of items: 2
Items: 
Size: 581294 Color: 0
Size: 418597 Color: 1

Bin 2493: 110 of cap free
Amount of items: 2
Items: 
Size: 584276 Color: 0
Size: 415615 Color: 1

Bin 2494: 110 of cap free
Amount of items: 2
Items: 
Size: 654057 Color: 1
Size: 345834 Color: 0

Bin 2495: 110 of cap free
Amount of items: 2
Items: 
Size: 744761 Color: 0
Size: 255130 Color: 1

Bin 2496: 110 of cap free
Amount of items: 2
Items: 
Size: 754438 Color: 0
Size: 245453 Color: 1

Bin 2497: 111 of cap free
Amount of items: 2
Items: 
Size: 531060 Color: 0
Size: 468830 Color: 1

Bin 2498: 111 of cap free
Amount of items: 2
Items: 
Size: 560508 Color: 1
Size: 439382 Color: 0

Bin 2499: 111 of cap free
Amount of items: 2
Items: 
Size: 575743 Color: 0
Size: 424147 Color: 1

Bin 2500: 111 of cap free
Amount of items: 2
Items: 
Size: 608823 Color: 0
Size: 391067 Color: 1

Bin 2501: 111 of cap free
Amount of items: 2
Items: 
Size: 617359 Color: 0
Size: 382531 Color: 1

Bin 2502: 111 of cap free
Amount of items: 2
Items: 
Size: 662203 Color: 0
Size: 337687 Color: 1

Bin 2503: 111 of cap free
Amount of items: 2
Items: 
Size: 674409 Color: 0
Size: 325481 Color: 1

Bin 2504: 111 of cap free
Amount of items: 2
Items: 
Size: 690685 Color: 0
Size: 309205 Color: 1

Bin 2505: 111 of cap free
Amount of items: 2
Items: 
Size: 731690 Color: 0
Size: 268200 Color: 1

Bin 2506: 111 of cap free
Amount of items: 2
Items: 
Size: 732144 Color: 1
Size: 267746 Color: 0

Bin 2507: 111 of cap free
Amount of items: 2
Items: 
Size: 730587 Color: 1
Size: 269303 Color: 0

Bin 2508: 112 of cap free
Amount of items: 2
Items: 
Size: 503100 Color: 0
Size: 496789 Color: 1

Bin 2509: 112 of cap free
Amount of items: 2
Items: 
Size: 613546 Color: 0
Size: 386343 Color: 1

Bin 2510: 112 of cap free
Amount of items: 2
Items: 
Size: 731742 Color: 1
Size: 268147 Color: 0

Bin 2511: 112 of cap free
Amount of items: 2
Items: 
Size: 737563 Color: 0
Size: 262326 Color: 1

Bin 2512: 113 of cap free
Amount of items: 3
Items: 
Size: 738234 Color: 1
Size: 137413 Color: 0
Size: 124241 Color: 1

Bin 2513: 113 of cap free
Amount of items: 2
Items: 
Size: 579515 Color: 0
Size: 420373 Color: 1

Bin 2514: 113 of cap free
Amount of items: 2
Items: 
Size: 512493 Color: 1
Size: 487395 Color: 0

Bin 2515: 113 of cap free
Amount of items: 2
Items: 
Size: 524067 Color: 1
Size: 475821 Color: 0

Bin 2516: 113 of cap free
Amount of items: 2
Items: 
Size: 552502 Color: 1
Size: 447386 Color: 0

Bin 2517: 113 of cap free
Amount of items: 2
Items: 
Size: 584116 Color: 1
Size: 415772 Color: 0

Bin 2518: 113 of cap free
Amount of items: 2
Items: 
Size: 740227 Color: 0
Size: 259661 Color: 1

Bin 2519: 114 of cap free
Amount of items: 2
Items: 
Size: 777843 Color: 0
Size: 222044 Color: 1

Bin 2520: 114 of cap free
Amount of items: 2
Items: 
Size: 789614 Color: 0
Size: 210273 Color: 1

Bin 2521: 114 of cap free
Amount of items: 2
Items: 
Size: 505176 Color: 0
Size: 494711 Color: 1

Bin 2522: 114 of cap free
Amount of items: 2
Items: 
Size: 553367 Color: 0
Size: 446520 Color: 1

Bin 2523: 114 of cap free
Amount of items: 2
Items: 
Size: 657281 Color: 0
Size: 342606 Color: 1

Bin 2524: 114 of cap free
Amount of items: 2
Items: 
Size: 739754 Color: 1
Size: 260133 Color: 0

Bin 2525: 114 of cap free
Amount of items: 2
Items: 
Size: 759976 Color: 0
Size: 239911 Color: 1

Bin 2526: 114 of cap free
Amount of items: 2
Items: 
Size: 616717 Color: 0
Size: 383170 Color: 1

Bin 2527: 115 of cap free
Amount of items: 2
Items: 
Size: 524611 Color: 1
Size: 475275 Color: 0

Bin 2528: 115 of cap free
Amount of items: 2
Items: 
Size: 635163 Color: 1
Size: 364723 Color: 0

Bin 2529: 115 of cap free
Amount of items: 2
Items: 
Size: 692958 Color: 0
Size: 306928 Color: 1

Bin 2530: 115 of cap free
Amount of items: 2
Items: 
Size: 719147 Color: 0
Size: 280739 Color: 1

Bin 2531: 115 of cap free
Amount of items: 2
Items: 
Size: 751019 Color: 0
Size: 248867 Color: 1

Bin 2532: 116 of cap free
Amount of items: 2
Items: 
Size: 761518 Color: 0
Size: 238367 Color: 1

Bin 2533: 116 of cap free
Amount of items: 2
Items: 
Size: 511196 Color: 0
Size: 488689 Color: 1

Bin 2534: 116 of cap free
Amount of items: 2
Items: 
Size: 568239 Color: 1
Size: 431646 Color: 0

Bin 2535: 116 of cap free
Amount of items: 2
Items: 
Size: 569386 Color: 0
Size: 430499 Color: 1

Bin 2536: 116 of cap free
Amount of items: 2
Items: 
Size: 588962 Color: 1
Size: 410923 Color: 0

Bin 2537: 116 of cap free
Amount of items: 2
Items: 
Size: 720248 Color: 1
Size: 279637 Color: 0

Bin 2538: 117 of cap free
Amount of items: 2
Items: 
Size: 541572 Color: 1
Size: 458312 Color: 0

Bin 2539: 117 of cap free
Amount of items: 2
Items: 
Size: 510550 Color: 1
Size: 489334 Color: 0

Bin 2540: 117 of cap free
Amount of items: 2
Items: 
Size: 512827 Color: 0
Size: 487057 Color: 1

Bin 2541: 117 of cap free
Amount of items: 2
Items: 
Size: 526185 Color: 0
Size: 473699 Color: 1

Bin 2542: 117 of cap free
Amount of items: 2
Items: 
Size: 546156 Color: 1
Size: 453728 Color: 0

Bin 2543: 117 of cap free
Amount of items: 2
Items: 
Size: 595709 Color: 0
Size: 404175 Color: 1

Bin 2544: 117 of cap free
Amount of items: 2
Items: 
Size: 612556 Color: 1
Size: 387328 Color: 0

Bin 2545: 117 of cap free
Amount of items: 2
Items: 
Size: 654848 Color: 1
Size: 345036 Color: 0

Bin 2546: 117 of cap free
Amount of items: 2
Items: 
Size: 721993 Color: 0
Size: 277891 Color: 1

Bin 2547: 117 of cap free
Amount of items: 2
Items: 
Size: 736691 Color: 0
Size: 263193 Color: 1

Bin 2548: 117 of cap free
Amount of items: 2
Items: 
Size: 648705 Color: 0
Size: 351179 Color: 1

Bin 2549: 117 of cap free
Amount of items: 2
Items: 
Size: 755062 Color: 1
Size: 244822 Color: 0

Bin 2550: 118 of cap free
Amount of items: 2
Items: 
Size: 586193 Color: 0
Size: 413690 Color: 1

Bin 2551: 118 of cap free
Amount of items: 2
Items: 
Size: 534941 Color: 0
Size: 464942 Color: 1

Bin 2552: 118 of cap free
Amount of items: 2
Items: 
Size: 580586 Color: 1
Size: 419297 Color: 0

Bin 2553: 118 of cap free
Amount of items: 2
Items: 
Size: 610990 Color: 0
Size: 388893 Color: 1

Bin 2554: 118 of cap free
Amount of items: 2
Items: 
Size: 684667 Color: 0
Size: 315216 Color: 1

Bin 2555: 118 of cap free
Amount of items: 2
Items: 
Size: 717500 Color: 1
Size: 282383 Color: 0

Bin 2556: 119 of cap free
Amount of items: 2
Items: 
Size: 609035 Color: 1
Size: 390847 Color: 0

Bin 2557: 119 of cap free
Amount of items: 2
Items: 
Size: 506219 Color: 0
Size: 493663 Color: 1

Bin 2558: 119 of cap free
Amount of items: 2
Items: 
Size: 619657 Color: 1
Size: 380225 Color: 0

Bin 2559: 119 of cap free
Amount of items: 2
Items: 
Size: 637576 Color: 0
Size: 362306 Color: 1

Bin 2560: 119 of cap free
Amount of items: 2
Items: 
Size: 704874 Color: 1
Size: 295008 Color: 0

Bin 2561: 119 of cap free
Amount of items: 2
Items: 
Size: 714307 Color: 0
Size: 285575 Color: 1

Bin 2562: 119 of cap free
Amount of items: 2
Items: 
Size: 753995 Color: 0
Size: 245887 Color: 1

Bin 2563: 119 of cap free
Amount of items: 2
Items: 
Size: 793834 Color: 0
Size: 206048 Color: 1

Bin 2564: 120 of cap free
Amount of items: 2
Items: 
Size: 795089 Color: 1
Size: 204792 Color: 0

Bin 2565: 120 of cap free
Amount of items: 2
Items: 
Size: 522205 Color: 1
Size: 477676 Color: 0

Bin 2566: 120 of cap free
Amount of items: 2
Items: 
Size: 542378 Color: 0
Size: 457503 Color: 1

Bin 2567: 120 of cap free
Amount of items: 2
Items: 
Size: 583258 Color: 1
Size: 416623 Color: 0

Bin 2568: 120 of cap free
Amount of items: 2
Items: 
Size: 646351 Color: 0
Size: 353530 Color: 1

Bin 2569: 120 of cap free
Amount of items: 2
Items: 
Size: 689906 Color: 1
Size: 309975 Color: 0

Bin 2570: 120 of cap free
Amount of items: 2
Items: 
Size: 710826 Color: 1
Size: 289055 Color: 0

Bin 2571: 120 of cap free
Amount of items: 2
Items: 
Size: 734778 Color: 0
Size: 265103 Color: 1

Bin 2572: 120 of cap free
Amount of items: 2
Items: 
Size: 794770 Color: 0
Size: 205111 Color: 1

Bin 2573: 121 of cap free
Amount of items: 2
Items: 
Size: 505495 Color: 1
Size: 494385 Color: 0

Bin 2574: 121 of cap free
Amount of items: 2
Items: 
Size: 560007 Color: 0
Size: 439873 Color: 1

Bin 2575: 121 of cap free
Amount of items: 2
Items: 
Size: 670238 Color: 1
Size: 329642 Color: 0

Bin 2576: 121 of cap free
Amount of items: 2
Items: 
Size: 706773 Color: 0
Size: 293107 Color: 1

Bin 2577: 121 of cap free
Amount of items: 2
Items: 
Size: 713374 Color: 0
Size: 286506 Color: 1

Bin 2578: 121 of cap free
Amount of items: 2
Items: 
Size: 716751 Color: 1
Size: 283129 Color: 0

Bin 2579: 121 of cap free
Amount of items: 2
Items: 
Size: 598413 Color: 1
Size: 401467 Color: 0

Bin 2580: 121 of cap free
Amount of items: 5
Items: 
Size: 322549 Color: 1
Size: 193160 Color: 1
Size: 162420 Color: 0
Size: 162088 Color: 0
Size: 159663 Color: 1

Bin 2581: 122 of cap free
Amount of items: 2
Items: 
Size: 589557 Color: 0
Size: 410322 Color: 1

Bin 2582: 122 of cap free
Amount of items: 2
Items: 
Size: 599060 Color: 1
Size: 400819 Color: 0

Bin 2583: 122 of cap free
Amount of items: 2
Items: 
Size: 621108 Color: 0
Size: 378771 Color: 1

Bin 2584: 122 of cap free
Amount of items: 2
Items: 
Size: 743147 Color: 1
Size: 256732 Color: 0

Bin 2585: 122 of cap free
Amount of items: 2
Items: 
Size: 749837 Color: 0
Size: 250042 Color: 1

Bin 2586: 122 of cap free
Amount of items: 2
Items: 
Size: 779140 Color: 0
Size: 220739 Color: 1

Bin 2587: 122 of cap free
Amount of items: 2
Items: 
Size: 782182 Color: 0
Size: 217697 Color: 1

Bin 2588: 122 of cap free
Amount of items: 2
Items: 
Size: 791576 Color: 1
Size: 208303 Color: 0

Bin 2589: 123 of cap free
Amount of items: 2
Items: 
Size: 582638 Color: 0
Size: 417240 Color: 1

Bin 2590: 123 of cap free
Amount of items: 2
Items: 
Size: 513421 Color: 0
Size: 486457 Color: 1

Bin 2591: 123 of cap free
Amount of items: 2
Items: 
Size: 579077 Color: 1
Size: 420801 Color: 0

Bin 2592: 123 of cap free
Amount of items: 2
Items: 
Size: 672436 Color: 0
Size: 327442 Color: 1

Bin 2593: 124 of cap free
Amount of items: 2
Items: 
Size: 567877 Color: 0
Size: 432000 Color: 1

Bin 2594: 124 of cap free
Amount of items: 2
Items: 
Size: 673341 Color: 0
Size: 326536 Color: 1

Bin 2595: 124 of cap free
Amount of items: 2
Items: 
Size: 686531 Color: 0
Size: 313346 Color: 1

Bin 2596: 124 of cap free
Amount of items: 2
Items: 
Size: 750388 Color: 1
Size: 249489 Color: 0

Bin 2597: 125 of cap free
Amount of items: 2
Items: 
Size: 582626 Color: 1
Size: 417250 Color: 0

Bin 2598: 125 of cap free
Amount of items: 2
Items: 
Size: 602453 Color: 0
Size: 397423 Color: 1

Bin 2599: 125 of cap free
Amount of items: 2
Items: 
Size: 615043 Color: 0
Size: 384833 Color: 1

Bin 2600: 125 of cap free
Amount of items: 2
Items: 
Size: 634930 Color: 0
Size: 364946 Color: 1

Bin 2601: 125 of cap free
Amount of items: 2
Items: 
Size: 640417 Color: 0
Size: 359459 Color: 1

Bin 2602: 125 of cap free
Amount of items: 2
Items: 
Size: 651719 Color: 0
Size: 348157 Color: 1

Bin 2603: 125 of cap free
Amount of items: 2
Items: 
Size: 697745 Color: 1
Size: 302131 Color: 0

Bin 2604: 125 of cap free
Amount of items: 2
Items: 
Size: 721707 Color: 1
Size: 278169 Color: 0

Bin 2605: 125 of cap free
Amount of items: 2
Items: 
Size: 751399 Color: 0
Size: 248477 Color: 1

Bin 2606: 125 of cap free
Amount of items: 2
Items: 
Size: 797114 Color: 1
Size: 202762 Color: 0

Bin 2607: 125 of cap free
Amount of items: 2
Items: 
Size: 759157 Color: 1
Size: 240719 Color: 0

Bin 2608: 125 of cap free
Amount of items: 5
Items: 
Size: 308735 Color: 0
Size: 174330 Color: 0
Size: 173570 Color: 0
Size: 172386 Color: 1
Size: 170855 Color: 1

Bin 2609: 126 of cap free
Amount of items: 2
Items: 
Size: 668667 Color: 0
Size: 331208 Color: 1

Bin 2610: 126 of cap free
Amount of items: 2
Items: 
Size: 708796 Color: 1
Size: 291079 Color: 0

Bin 2611: 126 of cap free
Amount of items: 2
Items: 
Size: 745169 Color: 1
Size: 254706 Color: 0

Bin 2612: 127 of cap free
Amount of items: 2
Items: 
Size: 564150 Color: 1
Size: 435724 Color: 0

Bin 2613: 127 of cap free
Amount of items: 2
Items: 
Size: 593135 Color: 1
Size: 406739 Color: 0

Bin 2614: 127 of cap free
Amount of items: 2
Items: 
Size: 609978 Color: 1
Size: 389896 Color: 0

Bin 2615: 127 of cap free
Amount of items: 2
Items: 
Size: 748220 Color: 0
Size: 251654 Color: 1

Bin 2616: 128 of cap free
Amount of items: 2
Items: 
Size: 531548 Color: 0
Size: 468325 Color: 1

Bin 2617: 128 of cap free
Amount of items: 2
Items: 
Size: 540287 Color: 1
Size: 459586 Color: 0

Bin 2618: 128 of cap free
Amount of items: 2
Items: 
Size: 549001 Color: 1
Size: 450872 Color: 0

Bin 2619: 128 of cap free
Amount of items: 2
Items: 
Size: 612706 Color: 1
Size: 387167 Color: 0

Bin 2620: 128 of cap free
Amount of items: 2
Items: 
Size: 615205 Color: 1
Size: 384668 Color: 0

Bin 2621: 128 of cap free
Amount of items: 2
Items: 
Size: 618547 Color: 1
Size: 381326 Color: 0

Bin 2622: 128 of cap free
Amount of items: 2
Items: 
Size: 727240 Color: 1
Size: 272633 Color: 0

Bin 2623: 128 of cap free
Amount of items: 2
Items: 
Size: 507524 Color: 1
Size: 492349 Color: 0

Bin 2624: 129 of cap free
Amount of items: 2
Items: 
Size: 603084 Color: 0
Size: 396788 Color: 1

Bin 2625: 129 of cap free
Amount of items: 2
Items: 
Size: 637751 Color: 0
Size: 362121 Color: 1

Bin 2626: 129 of cap free
Amount of items: 2
Items: 
Size: 668616 Color: 1
Size: 331256 Color: 0

Bin 2627: 129 of cap free
Amount of items: 2
Items: 
Size: 676826 Color: 1
Size: 323046 Color: 0

Bin 2628: 129 of cap free
Amount of items: 2
Items: 
Size: 797255 Color: 0
Size: 202617 Color: 1

Bin 2629: 130 of cap free
Amount of items: 2
Items: 
Size: 547357 Color: 0
Size: 452514 Color: 1

Bin 2630: 130 of cap free
Amount of items: 2
Items: 
Size: 522701 Color: 1
Size: 477170 Color: 0

Bin 2631: 130 of cap free
Amount of items: 2
Items: 
Size: 602669 Color: 0
Size: 397202 Color: 1

Bin 2632: 130 of cap free
Amount of items: 2
Items: 
Size: 623863 Color: 1
Size: 376008 Color: 0

Bin 2633: 130 of cap free
Amount of items: 2
Items: 
Size: 650227 Color: 1
Size: 349644 Color: 0

Bin 2634: 130 of cap free
Amount of items: 2
Items: 
Size: 676086 Color: 0
Size: 323785 Color: 1

Bin 2635: 130 of cap free
Amount of items: 2
Items: 
Size: 676481 Color: 1
Size: 323390 Color: 0

Bin 2636: 130 of cap free
Amount of items: 2
Items: 
Size: 680047 Color: 1
Size: 319824 Color: 0

Bin 2637: 130 of cap free
Amount of items: 2
Items: 
Size: 730143 Color: 1
Size: 269728 Color: 0

Bin 2638: 130 of cap free
Amount of items: 2
Items: 
Size: 509165 Color: 0
Size: 490706 Color: 1

Bin 2639: 131 of cap free
Amount of items: 2
Items: 
Size: 701822 Color: 1
Size: 298048 Color: 0

Bin 2640: 131 of cap free
Amount of items: 4
Items: 
Size: 301875 Color: 1
Size: 301703 Color: 1
Size: 237850 Color: 0
Size: 158442 Color: 0

Bin 2641: 131 of cap free
Amount of items: 2
Items: 
Size: 527877 Color: 0
Size: 471993 Color: 1

Bin 2642: 131 of cap free
Amount of items: 2
Items: 
Size: 603748 Color: 0
Size: 396122 Color: 1

Bin 2643: 131 of cap free
Amount of items: 2
Items: 
Size: 644775 Color: 1
Size: 355095 Color: 0

Bin 2644: 131 of cap free
Amount of items: 2
Items: 
Size: 661055 Color: 0
Size: 338815 Color: 1

Bin 2645: 131 of cap free
Amount of items: 2
Items: 
Size: 680282 Color: 0
Size: 319588 Color: 1

Bin 2646: 131 of cap free
Amount of items: 2
Items: 
Size: 712460 Color: 0
Size: 287410 Color: 1

Bin 2647: 131 of cap free
Amount of items: 2
Items: 
Size: 716081 Color: 1
Size: 283789 Color: 0

Bin 2648: 131 of cap free
Amount of items: 2
Items: 
Size: 777986 Color: 0
Size: 221884 Color: 1

Bin 2649: 131 of cap free
Amount of items: 2
Items: 
Size: 778163 Color: 1
Size: 221707 Color: 0

Bin 2650: 131 of cap free
Amount of items: 2
Items: 
Size: 788230 Color: 0
Size: 211640 Color: 1

Bin 2651: 132 of cap free
Amount of items: 2
Items: 
Size: 783546 Color: 1
Size: 216323 Color: 0

Bin 2652: 132 of cap free
Amount of items: 2
Items: 
Size: 594486 Color: 1
Size: 405383 Color: 0

Bin 2653: 132 of cap free
Amount of items: 2
Items: 
Size: 551349 Color: 1
Size: 448520 Color: 0

Bin 2654: 132 of cap free
Amount of items: 2
Items: 
Size: 617489 Color: 0
Size: 382380 Color: 1

Bin 2655: 132 of cap free
Amount of items: 2
Items: 
Size: 653982 Color: 0
Size: 345887 Color: 1

Bin 2656: 132 of cap free
Amount of items: 2
Items: 
Size: 711329 Color: 1
Size: 288540 Color: 0

Bin 2657: 132 of cap free
Amount of items: 2
Items: 
Size: 792530 Color: 1
Size: 207339 Color: 0

Bin 2658: 132 of cap free
Amount of items: 2
Items: 
Size: 799835 Color: 0
Size: 200034 Color: 1

Bin 2659: 133 of cap free
Amount of items: 2
Items: 
Size: 513636 Color: 0
Size: 486232 Color: 1

Bin 2660: 133 of cap free
Amount of items: 2
Items: 
Size: 542596 Color: 0
Size: 457272 Color: 1

Bin 2661: 133 of cap free
Amount of items: 2
Items: 
Size: 605652 Color: 1
Size: 394216 Color: 0

Bin 2662: 133 of cap free
Amount of items: 2
Items: 
Size: 612796 Color: 0
Size: 387072 Color: 1

Bin 2663: 134 of cap free
Amount of items: 2
Items: 
Size: 506491 Color: 0
Size: 493376 Color: 1

Bin 2664: 134 of cap free
Amount of items: 2
Items: 
Size: 553790 Color: 0
Size: 446077 Color: 1

Bin 2665: 134 of cap free
Amount of items: 2
Items: 
Size: 587399 Color: 0
Size: 412468 Color: 1

Bin 2666: 134 of cap free
Amount of items: 2
Items: 
Size: 654271 Color: 0
Size: 345596 Color: 1

Bin 2667: 134 of cap free
Amount of items: 2
Items: 
Size: 656734 Color: 1
Size: 343133 Color: 0

Bin 2668: 135 of cap free
Amount of items: 2
Items: 
Size: 610497 Color: 0
Size: 389369 Color: 1

Bin 2669: 135 of cap free
Amount of items: 2
Items: 
Size: 627039 Color: 1
Size: 372827 Color: 0

Bin 2670: 136 of cap free
Amount of items: 2
Items: 
Size: 500224 Color: 0
Size: 499641 Color: 1

Bin 2671: 136 of cap free
Amount of items: 2
Items: 
Size: 501410 Color: 0
Size: 498455 Color: 1

Bin 2672: 136 of cap free
Amount of items: 2
Items: 
Size: 566091 Color: 0
Size: 433774 Color: 1

Bin 2673: 136 of cap free
Amount of items: 2
Items: 
Size: 597678 Color: 0
Size: 402187 Color: 1

Bin 2674: 136 of cap free
Amount of items: 2
Items: 
Size: 653445 Color: 0
Size: 346420 Color: 1

Bin 2675: 136 of cap free
Amount of items: 2
Items: 
Size: 671345 Color: 0
Size: 328520 Color: 1

Bin 2676: 136 of cap free
Amount of items: 2
Items: 
Size: 684386 Color: 0
Size: 315479 Color: 1

Bin 2677: 136 of cap free
Amount of items: 2
Items: 
Size: 695889 Color: 0
Size: 303976 Color: 1

Bin 2678: 136 of cap free
Amount of items: 2
Items: 
Size: 704353 Color: 0
Size: 295512 Color: 1

Bin 2679: 136 of cap free
Amount of items: 2
Items: 
Size: 523351 Color: 0
Size: 476514 Color: 1

Bin 2680: 136 of cap free
Amount of items: 2
Items: 
Size: 561503 Color: 1
Size: 438362 Color: 0

Bin 2681: 137 of cap free
Amount of items: 2
Items: 
Size: 511715 Color: 0
Size: 488149 Color: 1

Bin 2682: 137 of cap free
Amount of items: 2
Items: 
Size: 536320 Color: 1
Size: 463544 Color: 0

Bin 2683: 137 of cap free
Amount of items: 2
Items: 
Size: 588322 Color: 0
Size: 411542 Color: 1

Bin 2684: 137 of cap free
Amount of items: 2
Items: 
Size: 649578 Color: 1
Size: 350286 Color: 0

Bin 2685: 137 of cap free
Amount of items: 2
Items: 
Size: 674828 Color: 1
Size: 325036 Color: 0

Bin 2686: 137 of cap free
Amount of items: 2
Items: 
Size: 691840 Color: 1
Size: 308024 Color: 0

Bin 2687: 137 of cap free
Amount of items: 2
Items: 
Size: 774044 Color: 1
Size: 225820 Color: 0

Bin 2688: 137 of cap free
Amount of items: 2
Items: 
Size: 797059 Color: 0
Size: 202805 Color: 1

Bin 2689: 138 of cap free
Amount of items: 2
Items: 
Size: 752178 Color: 1
Size: 247685 Color: 0

Bin 2690: 138 of cap free
Amount of items: 2
Items: 
Size: 596135 Color: 0
Size: 403728 Color: 1

Bin 2691: 138 of cap free
Amount of items: 2
Items: 
Size: 655142 Color: 1
Size: 344721 Color: 0

Bin 2692: 138 of cap free
Amount of items: 2
Items: 
Size: 692437 Color: 0
Size: 307426 Color: 1

Bin 2693: 138 of cap free
Amount of items: 2
Items: 
Size: 728305 Color: 1
Size: 271558 Color: 0

Bin 2694: 138 of cap free
Amount of items: 2
Items: 
Size: 671528 Color: 1
Size: 328335 Color: 0

Bin 2695: 139 of cap free
Amount of items: 2
Items: 
Size: 520202 Color: 1
Size: 479660 Color: 0

Bin 2696: 139 of cap free
Amount of items: 2
Items: 
Size: 542736 Color: 0
Size: 457126 Color: 1

Bin 2697: 139 of cap free
Amount of items: 2
Items: 
Size: 568043 Color: 0
Size: 431819 Color: 1

Bin 2698: 139 of cap free
Amount of items: 2
Items: 
Size: 577185 Color: 0
Size: 422677 Color: 1

Bin 2699: 139 of cap free
Amount of items: 2
Items: 
Size: 701453 Color: 0
Size: 298409 Color: 1

Bin 2700: 139 of cap free
Amount of items: 2
Items: 
Size: 792840 Color: 0
Size: 207022 Color: 1

Bin 2701: 140 of cap free
Amount of items: 2
Items: 
Size: 665787 Color: 0
Size: 334074 Color: 1

Bin 2702: 140 of cap free
Amount of items: 2
Items: 
Size: 764028 Color: 1
Size: 235833 Color: 0

Bin 2703: 140 of cap free
Amount of items: 2
Items: 
Size: 530509 Color: 0
Size: 469352 Color: 1

Bin 2704: 140 of cap free
Amount of items: 2
Items: 
Size: 584395 Color: 0
Size: 415466 Color: 1

Bin 2705: 140 of cap free
Amount of items: 2
Items: 
Size: 649645 Color: 0
Size: 350216 Color: 1

Bin 2706: 140 of cap free
Amount of items: 2
Items: 
Size: 731665 Color: 0
Size: 268196 Color: 1

Bin 2707: 140 of cap free
Amount of items: 2
Items: 
Size: 723731 Color: 1
Size: 276130 Color: 0

Bin 2708: 141 of cap free
Amount of items: 2
Items: 
Size: 555702 Color: 0
Size: 444158 Color: 1

Bin 2709: 141 of cap free
Amount of items: 2
Items: 
Size: 672176 Color: 1
Size: 327684 Color: 0

Bin 2710: 141 of cap free
Amount of items: 2
Items: 
Size: 674814 Color: 0
Size: 325046 Color: 1

Bin 2711: 141 of cap free
Amount of items: 2
Items: 
Size: 731054 Color: 0
Size: 268806 Color: 1

Bin 2712: 141 of cap free
Amount of items: 2
Items: 
Size: 774326 Color: 0
Size: 225534 Color: 1

Bin 2713: 141 of cap free
Amount of items: 2
Items: 
Size: 790605 Color: 0
Size: 209255 Color: 1

Bin 2714: 142 of cap free
Amount of items: 2
Items: 
Size: 627288 Color: 0
Size: 372571 Color: 1

Bin 2715: 142 of cap free
Amount of items: 2
Items: 
Size: 500634 Color: 0
Size: 499225 Color: 1

Bin 2716: 142 of cap free
Amount of items: 2
Items: 
Size: 516333 Color: 1
Size: 483526 Color: 0

Bin 2717: 142 of cap free
Amount of items: 2
Items: 
Size: 574909 Color: 1
Size: 424950 Color: 0

Bin 2718: 142 of cap free
Amount of items: 2
Items: 
Size: 781468 Color: 0
Size: 218391 Color: 1

Bin 2719: 142 of cap free
Amount of items: 2
Items: 
Size: 784346 Color: 1
Size: 215513 Color: 0

Bin 2720: 142 of cap free
Amount of items: 2
Items: 
Size: 538209 Color: 1
Size: 461650 Color: 0

Bin 2721: 143 of cap free
Amount of items: 2
Items: 
Size: 526022 Color: 0
Size: 473836 Color: 1

Bin 2722: 143 of cap free
Amount of items: 2
Items: 
Size: 569431 Color: 1
Size: 430427 Color: 0

Bin 2723: 143 of cap free
Amount of items: 2
Items: 
Size: 610372 Color: 1
Size: 389486 Color: 0

Bin 2724: 143 of cap free
Amount of items: 2
Items: 
Size: 634419 Color: 0
Size: 365439 Color: 1

Bin 2725: 143 of cap free
Amount of items: 2
Items: 
Size: 646663 Color: 0
Size: 353195 Color: 1

Bin 2726: 143 of cap free
Amount of items: 2
Items: 
Size: 690502 Color: 1
Size: 309356 Color: 0

Bin 2727: 144 of cap free
Amount of items: 2
Items: 
Size: 577437 Color: 1
Size: 422420 Color: 0

Bin 2728: 144 of cap free
Amount of items: 2
Items: 
Size: 615639 Color: 1
Size: 384218 Color: 0

Bin 2729: 145 of cap free
Amount of items: 2
Items: 
Size: 578198 Color: 0
Size: 421658 Color: 1

Bin 2730: 145 of cap free
Amount of items: 2
Items: 
Size: 680211 Color: 1
Size: 319645 Color: 0

Bin 2731: 145 of cap free
Amount of items: 2
Items: 
Size: 690352 Color: 0
Size: 309504 Color: 1

Bin 2732: 145 of cap free
Amount of items: 2
Items: 
Size: 703734 Color: 1
Size: 296122 Color: 0

Bin 2733: 145 of cap free
Amount of items: 2
Items: 
Size: 786409 Color: 1
Size: 213447 Color: 0

Bin 2734: 146 of cap free
Amount of items: 2
Items: 
Size: 753098 Color: 1
Size: 246757 Color: 0

Bin 2735: 146 of cap free
Amount of items: 2
Items: 
Size: 695845 Color: 1
Size: 304010 Color: 0

Bin 2736: 146 of cap free
Amount of items: 2
Items: 
Size: 548841 Color: 1
Size: 451014 Color: 0

Bin 2737: 146 of cap free
Amount of items: 2
Items: 
Size: 600781 Color: 0
Size: 399074 Color: 1

Bin 2738: 146 of cap free
Amount of items: 2
Items: 
Size: 660059 Color: 0
Size: 339796 Color: 1

Bin 2739: 146 of cap free
Amount of items: 2
Items: 
Size: 707497 Color: 0
Size: 292358 Color: 1

Bin 2740: 146 of cap free
Amount of items: 2
Items: 
Size: 723922 Color: 1
Size: 275933 Color: 0

Bin 2741: 146 of cap free
Amount of items: 2
Items: 
Size: 763823 Color: 1
Size: 236032 Color: 0

Bin 2742: 147 of cap free
Amount of items: 2
Items: 
Size: 627800 Color: 0
Size: 372054 Color: 1

Bin 2743: 147 of cap free
Amount of items: 2
Items: 
Size: 529203 Color: 1
Size: 470651 Color: 0

Bin 2744: 147 of cap free
Amount of items: 2
Items: 
Size: 647822 Color: 1
Size: 352032 Color: 0

Bin 2745: 147 of cap free
Amount of items: 2
Items: 
Size: 788466 Color: 0
Size: 211388 Color: 1

Bin 2746: 148 of cap free
Amount of items: 2
Items: 
Size: 590684 Color: 0
Size: 409169 Color: 1

Bin 2747: 148 of cap free
Amount of items: 2
Items: 
Size: 686978 Color: 0
Size: 312875 Color: 1

Bin 2748: 148 of cap free
Amount of items: 2
Items: 
Size: 694859 Color: 1
Size: 304994 Color: 0

Bin 2749: 148 of cap free
Amount of items: 2
Items: 
Size: 733473 Color: 0
Size: 266380 Color: 1

Bin 2750: 148 of cap free
Amount of items: 2
Items: 
Size: 776181 Color: 1
Size: 223672 Color: 0

Bin 2751: 148 of cap free
Amount of items: 2
Items: 
Size: 777358 Color: 0
Size: 222495 Color: 1

Bin 2752: 148 of cap free
Amount of items: 2
Items: 
Size: 570418 Color: 1
Size: 429435 Color: 0

Bin 2753: 149 of cap free
Amount of items: 2
Items: 
Size: 592717 Color: 1
Size: 407135 Color: 0

Bin 2754: 149 of cap free
Amount of items: 2
Items: 
Size: 500607 Color: 1
Size: 499245 Color: 0

Bin 2755: 149 of cap free
Amount of items: 2
Items: 
Size: 526894 Color: 1
Size: 472958 Color: 0

Bin 2756: 149 of cap free
Amount of items: 2
Items: 
Size: 572682 Color: 1
Size: 427170 Color: 0

Bin 2757: 149 of cap free
Amount of items: 2
Items: 
Size: 641646 Color: 1
Size: 358206 Color: 0

Bin 2758: 149 of cap free
Amount of items: 2
Items: 
Size: 710812 Color: 1
Size: 289040 Color: 0

Bin 2759: 149 of cap free
Amount of items: 2
Items: 
Size: 733851 Color: 1
Size: 266001 Color: 0

Bin 2760: 149 of cap free
Amount of items: 2
Items: 
Size: 739566 Color: 0
Size: 260286 Color: 1

Bin 2761: 149 of cap free
Amount of items: 2
Items: 
Size: 774363 Color: 1
Size: 225489 Color: 0

Bin 2762: 149 of cap free
Amount of items: 2
Items: 
Size: 758496 Color: 1
Size: 241356 Color: 0

Bin 2763: 150 of cap free
Amount of items: 2
Items: 
Size: 619957 Color: 1
Size: 379894 Color: 0

Bin 2764: 150 of cap free
Amount of items: 2
Items: 
Size: 682107 Color: 0
Size: 317744 Color: 1

Bin 2765: 150 of cap free
Amount of items: 2
Items: 
Size: 711754 Color: 1
Size: 288097 Color: 0

Bin 2766: 151 of cap free
Amount of items: 2
Items: 
Size: 699960 Color: 0
Size: 299890 Color: 1

Bin 2767: 151 of cap free
Amount of items: 2
Items: 
Size: 530880 Color: 0
Size: 468970 Color: 1

Bin 2768: 151 of cap free
Amount of items: 2
Items: 
Size: 568249 Color: 0
Size: 431601 Color: 1

Bin 2769: 151 of cap free
Amount of items: 2
Items: 
Size: 756402 Color: 0
Size: 243448 Color: 1

Bin 2770: 151 of cap free
Amount of items: 2
Items: 
Size: 770592 Color: 1
Size: 229258 Color: 0

Bin 2771: 152 of cap free
Amount of items: 2
Items: 
Size: 515642 Color: 1
Size: 484207 Color: 0

Bin 2772: 152 of cap free
Amount of items: 2
Items: 
Size: 574526 Color: 0
Size: 425323 Color: 1

Bin 2773: 152 of cap free
Amount of items: 2
Items: 
Size: 584584 Color: 1
Size: 415265 Color: 0

Bin 2774: 152 of cap free
Amount of items: 2
Items: 
Size: 628086 Color: 0
Size: 371763 Color: 1

Bin 2775: 152 of cap free
Amount of items: 2
Items: 
Size: 769573 Color: 1
Size: 230276 Color: 0

Bin 2776: 152 of cap free
Amount of items: 2
Items: 
Size: 557140 Color: 0
Size: 442709 Color: 1

Bin 2777: 152 of cap free
Amount of items: 2
Items: 
Size: 664210 Color: 0
Size: 335639 Color: 1

Bin 2778: 153 of cap free
Amount of items: 2
Items: 
Size: 612693 Color: 1
Size: 387155 Color: 0

Bin 2779: 153 of cap free
Amount of items: 2
Items: 
Size: 621313 Color: 0
Size: 378535 Color: 1

Bin 2780: 153 of cap free
Amount of items: 2
Items: 
Size: 623266 Color: 0
Size: 376582 Color: 1

Bin 2781: 153 of cap free
Amount of items: 2
Items: 
Size: 643941 Color: 1
Size: 355907 Color: 0

Bin 2782: 153 of cap free
Amount of items: 2
Items: 
Size: 670702 Color: 0
Size: 329146 Color: 1

Bin 2783: 153 of cap free
Amount of items: 2
Items: 
Size: 680764 Color: 0
Size: 319084 Color: 1

Bin 2784: 153 of cap free
Amount of items: 2
Items: 
Size: 535991 Color: 0
Size: 463857 Color: 1

Bin 2785: 154 of cap free
Amount of items: 2
Items: 
Size: 500229 Color: 1
Size: 499618 Color: 0

Bin 2786: 154 of cap free
Amount of items: 2
Items: 
Size: 679042 Color: 1
Size: 320805 Color: 0

Bin 2787: 154 of cap free
Amount of items: 2
Items: 
Size: 693547 Color: 1
Size: 306300 Color: 0

Bin 2788: 154 of cap free
Amount of items: 2
Items: 
Size: 785447 Color: 1
Size: 214400 Color: 0

Bin 2789: 155 of cap free
Amount of items: 2
Items: 
Size: 679039 Color: 0
Size: 320807 Color: 1

Bin 2790: 155 of cap free
Amount of items: 2
Items: 
Size: 792832 Color: 0
Size: 207014 Color: 1

Bin 2791: 156 of cap free
Amount of items: 2
Items: 
Size: 628652 Color: 0
Size: 371193 Color: 1

Bin 2792: 156 of cap free
Amount of items: 2
Items: 
Size: 705071 Color: 1
Size: 294774 Color: 0

Bin 2793: 157 of cap free
Amount of items: 2
Items: 
Size: 665475 Color: 0
Size: 334369 Color: 1

Bin 2794: 157 of cap free
Amount of items: 2
Items: 
Size: 718449 Color: 0
Size: 281395 Color: 1

Bin 2795: 158 of cap free
Amount of items: 2
Items: 
Size: 506739 Color: 1
Size: 493104 Color: 0

Bin 2796: 158 of cap free
Amount of items: 2
Items: 
Size: 534772 Color: 0
Size: 465071 Color: 1

Bin 2797: 158 of cap free
Amount of items: 2
Items: 
Size: 555444 Color: 0
Size: 444399 Color: 1

Bin 2798: 159 of cap free
Amount of items: 2
Items: 
Size: 576120 Color: 1
Size: 423722 Color: 0

Bin 2799: 159 of cap free
Amount of items: 2
Items: 
Size: 640063 Color: 0
Size: 359779 Color: 1

Bin 2800: 159 of cap free
Amount of items: 2
Items: 
Size: 647045 Color: 0
Size: 352797 Color: 1

Bin 2801: 159 of cap free
Amount of items: 2
Items: 
Size: 730285 Color: 1
Size: 269557 Color: 0

Bin 2802: 159 of cap free
Amount of items: 2
Items: 
Size: 753865 Color: 1
Size: 245977 Color: 0

Bin 2803: 159 of cap free
Amount of items: 2
Items: 
Size: 782998 Color: 0
Size: 216844 Color: 1

Bin 2804: 160 of cap free
Amount of items: 2
Items: 
Size: 779417 Color: 1
Size: 220424 Color: 0

Bin 2805: 160 of cap free
Amount of items: 2
Items: 
Size: 510146 Color: 0
Size: 489695 Color: 1

Bin 2806: 160 of cap free
Amount of items: 2
Items: 
Size: 544628 Color: 0
Size: 455213 Color: 1

Bin 2807: 160 of cap free
Amount of items: 2
Items: 
Size: 652947 Color: 0
Size: 346894 Color: 1

Bin 2808: 160 of cap free
Amount of items: 2
Items: 
Size: 686363 Color: 1
Size: 313478 Color: 0

Bin 2809: 160 of cap free
Amount of items: 2
Items: 
Size: 722917 Color: 1
Size: 276924 Color: 0

Bin 2810: 161 of cap free
Amount of items: 2
Items: 
Size: 508766 Color: 0
Size: 491074 Color: 1

Bin 2811: 161 of cap free
Amount of items: 2
Items: 
Size: 512750 Color: 1
Size: 487090 Color: 0

Bin 2812: 161 of cap free
Amount of items: 2
Items: 
Size: 531527 Color: 0
Size: 468313 Color: 1

Bin 2813: 161 of cap free
Amount of items: 2
Items: 
Size: 573041 Color: 1
Size: 426799 Color: 0

Bin 2814: 161 of cap free
Amount of items: 2
Items: 
Size: 693910 Color: 1
Size: 305930 Color: 0

Bin 2815: 162 of cap free
Amount of items: 2
Items: 
Size: 508001 Color: 1
Size: 491838 Color: 0

Bin 2816: 162 of cap free
Amount of items: 2
Items: 
Size: 646063 Color: 1
Size: 353776 Color: 0

Bin 2817: 162 of cap free
Amount of items: 2
Items: 
Size: 731458 Color: 0
Size: 268381 Color: 1

Bin 2818: 162 of cap free
Amount of items: 2
Items: 
Size: 744244 Color: 1
Size: 255595 Color: 0

Bin 2819: 162 of cap free
Amount of items: 2
Items: 
Size: 755987 Color: 1
Size: 243852 Color: 0

Bin 2820: 163 of cap free
Amount of items: 2
Items: 
Size: 568992 Color: 0
Size: 430846 Color: 1

Bin 2821: 163 of cap free
Amount of items: 2
Items: 
Size: 570234 Color: 0
Size: 429604 Color: 1

Bin 2822: 163 of cap free
Amount of items: 2
Items: 
Size: 643633 Color: 1
Size: 356205 Color: 0

Bin 2823: 163 of cap free
Amount of items: 2
Items: 
Size: 648006 Color: 0
Size: 351832 Color: 1

Bin 2824: 163 of cap free
Amount of items: 2
Items: 
Size: 741875 Color: 0
Size: 257963 Color: 1

Bin 2825: 164 of cap free
Amount of items: 2
Items: 
Size: 513387 Color: 0
Size: 486450 Color: 1

Bin 2826: 164 of cap free
Amount of items: 2
Items: 
Size: 527535 Color: 0
Size: 472302 Color: 1

Bin 2827: 164 of cap free
Amount of items: 2
Items: 
Size: 561312 Color: 0
Size: 438525 Color: 1

Bin 2828: 164 of cap free
Amount of items: 2
Items: 
Size: 664438 Color: 0
Size: 335399 Color: 1

Bin 2829: 164 of cap free
Amount of items: 2
Items: 
Size: 698668 Color: 1
Size: 301169 Color: 0

Bin 2830: 164 of cap free
Amount of items: 2
Items: 
Size: 700378 Color: 0
Size: 299459 Color: 1

Bin 2831: 165 of cap free
Amount of items: 2
Items: 
Size: 630891 Color: 0
Size: 368945 Color: 1

Bin 2832: 165 of cap free
Amount of items: 2
Items: 
Size: 658679 Color: 0
Size: 341157 Color: 1

Bin 2833: 165 of cap free
Amount of items: 2
Items: 
Size: 503274 Color: 1
Size: 496562 Color: 0

Bin 2834: 165 of cap free
Amount of items: 2
Items: 
Size: 503082 Color: 0
Size: 496754 Color: 1

Bin 2835: 165 of cap free
Amount of items: 2
Items: 
Size: 630979 Color: 1
Size: 368857 Color: 0

Bin 2836: 165 of cap free
Amount of items: 2
Items: 
Size: 668378 Color: 1
Size: 331458 Color: 0

Bin 2837: 166 of cap free
Amount of items: 2
Items: 
Size: 613914 Color: 1
Size: 385921 Color: 0

Bin 2838: 166 of cap free
Amount of items: 2
Items: 
Size: 621890 Color: 1
Size: 377945 Color: 0

Bin 2839: 166 of cap free
Amount of items: 2
Items: 
Size: 795081 Color: 1
Size: 204754 Color: 0

Bin 2840: 167 of cap free
Amount of items: 2
Items: 
Size: 559969 Color: 0
Size: 439865 Color: 1

Bin 2841: 167 of cap free
Amount of items: 2
Items: 
Size: 660755 Color: 0
Size: 339079 Color: 1

Bin 2842: 167 of cap free
Amount of items: 2
Items: 
Size: 667322 Color: 1
Size: 332512 Color: 0

Bin 2843: 168 of cap free
Amount of items: 2
Items: 
Size: 536679 Color: 0
Size: 463154 Color: 1

Bin 2844: 168 of cap free
Amount of items: 2
Items: 
Size: 547346 Color: 0
Size: 452487 Color: 1

Bin 2845: 168 of cap free
Amount of items: 2
Items: 
Size: 553149 Color: 0
Size: 446684 Color: 1

Bin 2846: 168 of cap free
Amount of items: 2
Items: 
Size: 625101 Color: 1
Size: 374732 Color: 0

Bin 2847: 168 of cap free
Amount of items: 2
Items: 
Size: 754348 Color: 1
Size: 245485 Color: 0

Bin 2848: 168 of cap free
Amount of items: 2
Items: 
Size: 754807 Color: 0
Size: 245026 Color: 1

Bin 2849: 169 of cap free
Amount of items: 2
Items: 
Size: 777462 Color: 1
Size: 222370 Color: 0

Bin 2850: 169 of cap free
Amount of items: 2
Items: 
Size: 618038 Color: 0
Size: 381794 Color: 1

Bin 2851: 169 of cap free
Amount of items: 2
Items: 
Size: 645166 Color: 0
Size: 354666 Color: 1

Bin 2852: 170 of cap free
Amount of items: 2
Items: 
Size: 547686 Color: 1
Size: 452145 Color: 0

Bin 2853: 170 of cap free
Amount of items: 2
Items: 
Size: 557710 Color: 0
Size: 442121 Color: 1

Bin 2854: 170 of cap free
Amount of items: 2
Items: 
Size: 600830 Color: 1
Size: 399001 Color: 0

Bin 2855: 170 of cap free
Amount of items: 2
Items: 
Size: 673305 Color: 0
Size: 326526 Color: 1

Bin 2856: 170 of cap free
Amount of items: 2
Items: 
Size: 690968 Color: 1
Size: 308863 Color: 0

Bin 2857: 170 of cap free
Amount of items: 2
Items: 
Size: 717315 Color: 0
Size: 282516 Color: 1

Bin 2858: 170 of cap free
Amount of items: 2
Items: 
Size: 724574 Color: 1
Size: 275257 Color: 0

Bin 2859: 170 of cap free
Amount of items: 2
Items: 
Size: 757851 Color: 0
Size: 241980 Color: 1

Bin 2860: 171 of cap free
Amount of items: 2
Items: 
Size: 536092 Color: 1
Size: 463738 Color: 0

Bin 2861: 171 of cap free
Amount of items: 2
Items: 
Size: 545036 Color: 1
Size: 454794 Color: 0

Bin 2862: 171 of cap free
Amount of items: 2
Items: 
Size: 556537 Color: 1
Size: 443293 Color: 0

Bin 2863: 171 of cap free
Amount of items: 2
Items: 
Size: 631508 Color: 1
Size: 368322 Color: 0

Bin 2864: 172 of cap free
Amount of items: 2
Items: 
Size: 509288 Color: 1
Size: 490541 Color: 0

Bin 2865: 172 of cap free
Amount of items: 2
Items: 
Size: 511451 Color: 1
Size: 488378 Color: 0

Bin 2866: 172 of cap free
Amount of items: 2
Items: 
Size: 548092 Color: 0
Size: 451737 Color: 1

Bin 2867: 172 of cap free
Amount of items: 2
Items: 
Size: 616438 Color: 0
Size: 383391 Color: 1

Bin 2868: 172 of cap free
Amount of items: 2
Items: 
Size: 752332 Color: 1
Size: 247497 Color: 0

Bin 2869: 173 of cap free
Amount of items: 2
Items: 
Size: 566639 Color: 0
Size: 433189 Color: 1

Bin 2870: 173 of cap free
Amount of items: 2
Items: 
Size: 526679 Color: 0
Size: 473149 Color: 1

Bin 2871: 173 of cap free
Amount of items: 2
Items: 
Size: 568620 Color: 0
Size: 431208 Color: 1

Bin 2872: 173 of cap free
Amount of items: 2
Items: 
Size: 734502 Color: 1
Size: 265326 Color: 0

Bin 2873: 173 of cap free
Amount of items: 2
Items: 
Size: 742618 Color: 1
Size: 257210 Color: 0

Bin 2874: 173 of cap free
Amount of items: 2
Items: 
Size: 781620 Color: 0
Size: 218208 Color: 1

Bin 2875: 174 of cap free
Amount of items: 2
Items: 
Size: 536283 Color: 0
Size: 463544 Color: 1

Bin 2876: 174 of cap free
Amount of items: 2
Items: 
Size: 540732 Color: 0
Size: 459095 Color: 1

Bin 2877: 174 of cap free
Amount of items: 2
Items: 
Size: 553976 Color: 1
Size: 445851 Color: 0

Bin 2878: 174 of cap free
Amount of items: 2
Items: 
Size: 680635 Color: 1
Size: 319192 Color: 0

Bin 2879: 174 of cap free
Amount of items: 2
Items: 
Size: 735854 Color: 0
Size: 263973 Color: 1

Bin 2880: 174 of cap free
Amount of items: 2
Items: 
Size: 767269 Color: 1
Size: 232558 Color: 0

Bin 2881: 174 of cap free
Amount of items: 2
Items: 
Size: 798336 Color: 1
Size: 201491 Color: 0

Bin 2882: 175 of cap free
Amount of items: 2
Items: 
Size: 500216 Color: 0
Size: 499610 Color: 1

Bin 2883: 175 of cap free
Amount of items: 2
Items: 
Size: 531355 Color: 1
Size: 468471 Color: 0

Bin 2884: 175 of cap free
Amount of items: 2
Items: 
Size: 674046 Color: 1
Size: 325780 Color: 0

Bin 2885: 175 of cap free
Amount of items: 2
Items: 
Size: 746355 Color: 0
Size: 253471 Color: 1

Bin 2886: 176 of cap free
Amount of items: 2
Items: 
Size: 515099 Color: 1
Size: 484726 Color: 0

Bin 2887: 176 of cap free
Amount of items: 2
Items: 
Size: 566066 Color: 0
Size: 433759 Color: 1

Bin 2888: 176 of cap free
Amount of items: 2
Items: 
Size: 724909 Color: 0
Size: 274916 Color: 1

Bin 2889: 176 of cap free
Amount of items: 2
Items: 
Size: 757256 Color: 1
Size: 242569 Color: 0

Bin 2890: 177 of cap free
Amount of items: 2
Items: 
Size: 517189 Color: 0
Size: 482635 Color: 1

Bin 2891: 177 of cap free
Amount of items: 2
Items: 
Size: 586517 Color: 0
Size: 413307 Color: 1

Bin 2892: 177 of cap free
Amount of items: 2
Items: 
Size: 734252 Color: 0
Size: 265572 Color: 1

Bin 2893: 178 of cap free
Amount of items: 2
Items: 
Size: 673948 Color: 0
Size: 325875 Color: 1

Bin 2894: 178 of cap free
Amount of items: 2
Items: 
Size: 554283 Color: 0
Size: 445540 Color: 1

Bin 2895: 178 of cap free
Amount of items: 2
Items: 
Size: 558921 Color: 1
Size: 440902 Color: 0

Bin 2896: 178 of cap free
Amount of items: 2
Items: 
Size: 576929 Color: 0
Size: 422894 Color: 1

Bin 2897: 178 of cap free
Amount of items: 2
Items: 
Size: 582141 Color: 1
Size: 417682 Color: 0

Bin 2898: 178 of cap free
Amount of items: 2
Items: 
Size: 619633 Color: 0
Size: 380190 Color: 1

Bin 2899: 178 of cap free
Amount of items: 2
Items: 
Size: 692486 Color: 1
Size: 307337 Color: 0

Bin 2900: 178 of cap free
Amount of items: 2
Items: 
Size: 696487 Color: 0
Size: 303336 Color: 1

Bin 2901: 178 of cap free
Amount of items: 2
Items: 
Size: 792906 Color: 1
Size: 206917 Color: 0

Bin 2902: 178 of cap free
Amount of items: 2
Items: 
Size: 637079 Color: 1
Size: 362744 Color: 0

Bin 2903: 179 of cap free
Amount of items: 2
Items: 
Size: 548815 Color: 0
Size: 451007 Color: 1

Bin 2904: 179 of cap free
Amount of items: 2
Items: 
Size: 530233 Color: 1
Size: 469589 Color: 0

Bin 2905: 179 of cap free
Amount of items: 2
Items: 
Size: 564651 Color: 0
Size: 435171 Color: 1

Bin 2906: 179 of cap free
Amount of items: 2
Items: 
Size: 649814 Color: 1
Size: 350008 Color: 0

Bin 2907: 179 of cap free
Amount of items: 2
Items: 
Size: 717260 Color: 1
Size: 282562 Color: 0

Bin 2908: 179 of cap free
Amount of items: 2
Items: 
Size: 760277 Color: 1
Size: 239545 Color: 0

Bin 2909: 179 of cap free
Amount of items: 2
Items: 
Size: 571899 Color: 0
Size: 427923 Color: 1

Bin 2910: 180 of cap free
Amount of items: 2
Items: 
Size: 502341 Color: 1
Size: 497480 Color: 0

Bin 2911: 180 of cap free
Amount of items: 2
Items: 
Size: 643227 Color: 1
Size: 356594 Color: 0

Bin 2912: 180 of cap free
Amount of items: 2
Items: 
Size: 719762 Color: 0
Size: 280059 Color: 1

Bin 2913: 180 of cap free
Amount of items: 2
Items: 
Size: 749042 Color: 0
Size: 250779 Color: 1

Bin 2914: 181 of cap free
Amount of items: 2
Items: 
Size: 759051 Color: 0
Size: 240769 Color: 1

Bin 2915: 181 of cap free
Amount of items: 2
Items: 
Size: 514416 Color: 1
Size: 485404 Color: 0

Bin 2916: 181 of cap free
Amount of items: 2
Items: 
Size: 636811 Color: 1
Size: 363009 Color: 0

Bin 2917: 181 of cap free
Amount of items: 2
Items: 
Size: 675456 Color: 0
Size: 324364 Color: 1

Bin 2918: 182 of cap free
Amount of items: 2
Items: 
Size: 527173 Color: 1
Size: 472646 Color: 0

Bin 2919: 182 of cap free
Amount of items: 2
Items: 
Size: 597425 Color: 0
Size: 402394 Color: 1

Bin 2920: 182 of cap free
Amount of items: 2
Items: 
Size: 666725 Color: 0
Size: 333094 Color: 1

Bin 2921: 182 of cap free
Amount of items: 2
Items: 
Size: 703159 Color: 0
Size: 296660 Color: 1

Bin 2922: 183 of cap free
Amount of items: 2
Items: 
Size: 780255 Color: 1
Size: 219563 Color: 0

Bin 2923: 183 of cap free
Amount of items: 2
Items: 
Size: 565765 Color: 1
Size: 434053 Color: 0

Bin 2924: 183 of cap free
Amount of items: 2
Items: 
Size: 617826 Color: 1
Size: 381992 Color: 0

Bin 2925: 183 of cap free
Amount of items: 2
Items: 
Size: 707212 Color: 1
Size: 292606 Color: 0

Bin 2926: 183 of cap free
Amount of items: 2
Items: 
Size: 775685 Color: 1
Size: 224133 Color: 0

Bin 2927: 183 of cap free
Amount of items: 2
Items: 
Size: 604155 Color: 0
Size: 395663 Color: 1

Bin 2928: 184 of cap free
Amount of items: 2
Items: 
Size: 600420 Color: 0
Size: 399397 Color: 1

Bin 2929: 184 of cap free
Amount of items: 2
Items: 
Size: 620434 Color: 0
Size: 379383 Color: 1

Bin 2930: 184 of cap free
Amount of items: 2
Items: 
Size: 632394 Color: 0
Size: 367423 Color: 1

Bin 2931: 184 of cap free
Amount of items: 2
Items: 
Size: 647354 Color: 1
Size: 352463 Color: 0

Bin 2932: 184 of cap free
Amount of items: 2
Items: 
Size: 657047 Color: 1
Size: 342770 Color: 0

Bin 2933: 184 of cap free
Amount of items: 2
Items: 
Size: 675143 Color: 1
Size: 324674 Color: 0

Bin 2934: 184 of cap free
Amount of items: 2
Items: 
Size: 705405 Color: 1
Size: 294412 Color: 0

Bin 2935: 184 of cap free
Amount of items: 2
Items: 
Size: 734175 Color: 1
Size: 265642 Color: 0

Bin 2936: 185 of cap free
Amount of items: 2
Items: 
Size: 576399 Color: 0
Size: 423417 Color: 1

Bin 2937: 185 of cap free
Amount of items: 2
Items: 
Size: 690315 Color: 1
Size: 309501 Color: 0

Bin 2938: 185 of cap free
Amount of items: 2
Items: 
Size: 798870 Color: 1
Size: 200946 Color: 0

Bin 2939: 186 of cap free
Amount of items: 2
Items: 
Size: 599753 Color: 0
Size: 400062 Color: 1

Bin 2940: 186 of cap free
Amount of items: 2
Items: 
Size: 692213 Color: 0
Size: 307602 Color: 1

Bin 2941: 187 of cap free
Amount of items: 2
Items: 
Size: 539534 Color: 0
Size: 460280 Color: 1

Bin 2942: 187 of cap free
Amount of items: 2
Items: 
Size: 584103 Color: 1
Size: 415711 Color: 0

Bin 2943: 187 of cap free
Amount of items: 2
Items: 
Size: 775863 Color: 0
Size: 223951 Color: 1

Bin 2944: 188 of cap free
Amount of items: 2
Items: 
Size: 548343 Color: 0
Size: 451470 Color: 1

Bin 2945: 188 of cap free
Amount of items: 2
Items: 
Size: 645294 Color: 1
Size: 354519 Color: 0

Bin 2946: 188 of cap free
Amount of items: 2
Items: 
Size: 753343 Color: 0
Size: 246470 Color: 1

Bin 2947: 188 of cap free
Amount of items: 2
Items: 
Size: 758654 Color: 0
Size: 241159 Color: 1

Bin 2948: 188 of cap free
Amount of items: 2
Items: 
Size: 775667 Color: 0
Size: 224146 Color: 1

Bin 2949: 188 of cap free
Amount of items: 2
Items: 
Size: 786456 Color: 0
Size: 213357 Color: 1

Bin 2950: 189 of cap free
Amount of items: 2
Items: 
Size: 570963 Color: 0
Size: 428849 Color: 1

Bin 2951: 189 of cap free
Amount of items: 2
Items: 
Size: 734497 Color: 1
Size: 265315 Color: 0

Bin 2952: 190 of cap free
Amount of items: 2
Items: 
Size: 532129 Color: 1
Size: 467682 Color: 0

Bin 2953: 190 of cap free
Amount of items: 2
Items: 
Size: 722858 Color: 0
Size: 276953 Color: 1

Bin 2954: 190 of cap free
Amount of items: 2
Items: 
Size: 787995 Color: 1
Size: 211816 Color: 0

Bin 2955: 191 of cap free
Amount of items: 2
Items: 
Size: 533535 Color: 1
Size: 466275 Color: 0

Bin 2956: 191 of cap free
Amount of items: 2
Items: 
Size: 536912 Color: 1
Size: 462898 Color: 0

Bin 2957: 191 of cap free
Amount of items: 2
Items: 
Size: 582597 Color: 1
Size: 417213 Color: 0

Bin 2958: 191 of cap free
Amount of items: 2
Items: 
Size: 588661 Color: 1
Size: 411149 Color: 0

Bin 2959: 192 of cap free
Amount of items: 2
Items: 
Size: 665699 Color: 1
Size: 334110 Color: 0

Bin 2960: 193 of cap free
Amount of items: 2
Items: 
Size: 699241 Color: 0
Size: 300567 Color: 1

Bin 2961: 193 of cap free
Amount of items: 2
Items: 
Size: 737474 Color: 1
Size: 262334 Color: 0

Bin 2962: 193 of cap free
Amount of items: 2
Items: 
Size: 778128 Color: 0
Size: 221680 Color: 1

Bin 2963: 194 of cap free
Amount of items: 2
Items: 
Size: 511145 Color: 0
Size: 488662 Color: 1

Bin 2964: 194 of cap free
Amount of items: 2
Items: 
Size: 520602 Color: 1
Size: 479205 Color: 0

Bin 2965: 194 of cap free
Amount of items: 2
Items: 
Size: 619075 Color: 1
Size: 380732 Color: 0

Bin 2966: 194 of cap free
Amount of items: 2
Items: 
Size: 624090 Color: 0
Size: 375717 Color: 1

Bin 2967: 194 of cap free
Amount of items: 2
Items: 
Size: 633613 Color: 0
Size: 366194 Color: 1

Bin 2968: 194 of cap free
Amount of items: 2
Items: 
Size: 751010 Color: 0
Size: 248797 Color: 1

Bin 2969: 195 of cap free
Amount of items: 2
Items: 
Size: 653616 Color: 1
Size: 346190 Color: 0

Bin 2970: 195 of cap free
Amount of items: 2
Items: 
Size: 655363 Color: 1
Size: 344443 Color: 0

Bin 2971: 196 of cap free
Amount of items: 2
Items: 
Size: 775368 Color: 0
Size: 224437 Color: 1

Bin 2972: 196 of cap free
Amount of items: 2
Items: 
Size: 577025 Color: 1
Size: 422780 Color: 0

Bin 2973: 196 of cap free
Amount of items: 2
Items: 
Size: 601446 Color: 0
Size: 398359 Color: 1

Bin 2974: 196 of cap free
Amount of items: 2
Items: 
Size: 609489 Color: 0
Size: 390316 Color: 1

Bin 2975: 196 of cap free
Amount of items: 2
Items: 
Size: 611828 Color: 0
Size: 387977 Color: 1

Bin 2976: 196 of cap free
Amount of items: 2
Items: 
Size: 710320 Color: 0
Size: 289485 Color: 1

Bin 2977: 196 of cap free
Amount of items: 2
Items: 
Size: 742852 Color: 0
Size: 256953 Color: 1

Bin 2978: 196 of cap free
Amount of items: 2
Items: 
Size: 778562 Color: 0
Size: 221243 Color: 1

Bin 2979: 197 of cap free
Amount of items: 2
Items: 
Size: 524724 Color: 0
Size: 475080 Color: 1

Bin 2980: 198 of cap free
Amount of items: 2
Items: 
Size: 581930 Color: 1
Size: 417873 Color: 0

Bin 2981: 198 of cap free
Amount of items: 2
Items: 
Size: 703698 Color: 1
Size: 296105 Color: 0

Bin 2982: 198 of cap free
Amount of items: 2
Items: 
Size: 767303 Color: 0
Size: 232500 Color: 1

Bin 2983: 199 of cap free
Amount of items: 2
Items: 
Size: 554046 Color: 0
Size: 445756 Color: 1

Bin 2984: 199 of cap free
Amount of items: 2
Items: 
Size: 610751 Color: 0
Size: 389051 Color: 1

Bin 2985: 199 of cap free
Amount of items: 2
Items: 
Size: 700615 Color: 1
Size: 299187 Color: 0

Bin 2986: 199 of cap free
Amount of items: 2
Items: 
Size: 711027 Color: 1
Size: 288775 Color: 0

Bin 2987: 199 of cap free
Amount of items: 2
Items: 
Size: 587763 Color: 0
Size: 412039 Color: 1

Bin 2988: 200 of cap free
Amount of items: 2
Items: 
Size: 557748 Color: 1
Size: 442053 Color: 0

Bin 2989: 200 of cap free
Amount of items: 2
Items: 
Size: 725779 Color: 0
Size: 274022 Color: 1

Bin 2990: 201 of cap free
Amount of items: 2
Items: 
Size: 775266 Color: 1
Size: 224534 Color: 0

Bin 2991: 201 of cap free
Amount of items: 2
Items: 
Size: 583280 Color: 0
Size: 416520 Color: 1

Bin 2992: 201 of cap free
Amount of items: 2
Items: 
Size: 603078 Color: 1
Size: 396722 Color: 0

Bin 2993: 201 of cap free
Amount of items: 2
Items: 
Size: 624310 Color: 1
Size: 375490 Color: 0

Bin 2994: 201 of cap free
Amount of items: 2
Items: 
Size: 627292 Color: 1
Size: 372508 Color: 0

Bin 2995: 201 of cap free
Amount of items: 2
Items: 
Size: 644732 Color: 1
Size: 355068 Color: 0

Bin 2996: 202 of cap free
Amount of items: 2
Items: 
Size: 562741 Color: 0
Size: 437058 Color: 1

Bin 2997: 202 of cap free
Amount of items: 2
Items: 
Size: 649251 Color: 1
Size: 350548 Color: 0

Bin 2998: 202 of cap free
Amount of items: 2
Items: 
Size: 769530 Color: 0
Size: 230269 Color: 1

Bin 2999: 202 of cap free
Amount of items: 2
Items: 
Size: 786996 Color: 1
Size: 212803 Color: 0

Bin 3000: 203 of cap free
Amount of items: 2
Items: 
Size: 712854 Color: 0
Size: 286944 Color: 1

Bin 3001: 203 of cap free
Amount of items: 2
Items: 
Size: 516151 Color: 0
Size: 483647 Color: 1

Bin 3002: 203 of cap free
Amount of items: 2
Items: 
Size: 716217 Color: 1
Size: 283581 Color: 0

Bin 3003: 203 of cap free
Amount of items: 2
Items: 
Size: 784534 Color: 0
Size: 215264 Color: 1

Bin 3004: 204 of cap free
Amount of items: 2
Items: 
Size: 579436 Color: 0
Size: 420361 Color: 1

Bin 3005: 204 of cap free
Amount of items: 2
Items: 
Size: 563717 Color: 0
Size: 436080 Color: 1

Bin 3006: 204 of cap free
Amount of items: 2
Items: 
Size: 678448 Color: 1
Size: 321349 Color: 0

Bin 3007: 205 of cap free
Amount of items: 2
Items: 
Size: 508860 Color: 1
Size: 490936 Color: 0

Bin 3008: 205 of cap free
Amount of items: 2
Items: 
Size: 777047 Color: 0
Size: 222749 Color: 1

Bin 3009: 206 of cap free
Amount of items: 2
Items: 
Size: 635111 Color: 0
Size: 364684 Color: 1

Bin 3010: 206 of cap free
Amount of items: 2
Items: 
Size: 677926 Color: 1
Size: 321869 Color: 0

Bin 3011: 207 of cap free
Amount of items: 2
Items: 
Size: 667311 Color: 1
Size: 332483 Color: 0

Bin 3012: 207 of cap free
Amount of items: 2
Items: 
Size: 685870 Color: 1
Size: 313924 Color: 0

Bin 3013: 207 of cap free
Amount of items: 2
Items: 
Size: 789799 Color: 1
Size: 209995 Color: 0

Bin 3014: 207 of cap free
Amount of items: 2
Items: 
Size: 791239 Color: 0
Size: 208555 Color: 1

Bin 3015: 207 of cap free
Amount of items: 2
Items: 
Size: 680760 Color: 0
Size: 319034 Color: 1

Bin 3016: 208 of cap free
Amount of items: 2
Items: 
Size: 510144 Color: 0
Size: 489649 Color: 1

Bin 3017: 208 of cap free
Amount of items: 2
Items: 
Size: 608478 Color: 0
Size: 391315 Color: 1

Bin 3018: 208 of cap free
Amount of items: 2
Items: 
Size: 548086 Color: 0
Size: 451707 Color: 1

Bin 3019: 209 of cap free
Amount of items: 2
Items: 
Size: 799584 Color: 1
Size: 200208 Color: 0

Bin 3020: 209 of cap free
Amount of items: 2
Items: 
Size: 520135 Color: 1
Size: 479657 Color: 0

Bin 3021: 209 of cap free
Amount of items: 2
Items: 
Size: 575887 Color: 1
Size: 423905 Color: 0

Bin 3022: 209 of cap free
Amount of items: 2
Items: 
Size: 759598 Color: 0
Size: 240194 Color: 1

Bin 3023: 209 of cap free
Amount of items: 2
Items: 
Size: 796698 Color: 0
Size: 203094 Color: 1

Bin 3024: 210 of cap free
Amount of items: 2
Items: 
Size: 739258 Color: 0
Size: 260533 Color: 1

Bin 3025: 210 of cap free
Amount of items: 2
Items: 
Size: 745063 Color: 0
Size: 254728 Color: 1

Bin 3026: 210 of cap free
Amount of items: 2
Items: 
Size: 750118 Color: 0
Size: 249673 Color: 1

Bin 3027: 210 of cap free
Amount of items: 2
Items: 
Size: 781099 Color: 0
Size: 218692 Color: 1

Bin 3028: 211 of cap free
Amount of items: 2
Items: 
Size: 619147 Color: 0
Size: 380643 Color: 1

Bin 3029: 211 of cap free
Amount of items: 2
Items: 
Size: 683681 Color: 0
Size: 316109 Color: 1

Bin 3030: 211 of cap free
Amount of items: 2
Items: 
Size: 729617 Color: 1
Size: 270173 Color: 0

Bin 3031: 212 of cap free
Amount of items: 3
Items: 
Size: 726579 Color: 1
Size: 150114 Color: 0
Size: 123096 Color: 0

Bin 3032: 212 of cap free
Amount of items: 2
Items: 
Size: 719136 Color: 1
Size: 280653 Color: 0

Bin 3033: 212 of cap free
Amount of items: 2
Items: 
Size: 767783 Color: 0
Size: 232006 Color: 1

Bin 3034: 213 of cap free
Amount of items: 2
Items: 
Size: 584912 Color: 0
Size: 414876 Color: 1

Bin 3035: 213 of cap free
Amount of items: 2
Items: 
Size: 635999 Color: 0
Size: 363789 Color: 1

Bin 3036: 213 of cap free
Amount of items: 2
Items: 
Size: 509714 Color: 1
Size: 490074 Color: 0

Bin 3037: 213 of cap free
Amount of items: 2
Items: 
Size: 641186 Color: 0
Size: 358602 Color: 1

Bin 3038: 214 of cap free
Amount of items: 2
Items: 
Size: 501137 Color: 0
Size: 498650 Color: 1

Bin 3039: 214 of cap free
Amount of items: 2
Items: 
Size: 542971 Color: 0
Size: 456816 Color: 1

Bin 3040: 214 of cap free
Amount of items: 2
Items: 
Size: 561078 Color: 1
Size: 438709 Color: 0

Bin 3041: 214 of cap free
Amount of items: 2
Items: 
Size: 690658 Color: 0
Size: 309129 Color: 1

Bin 3042: 215 of cap free
Amount of items: 2
Items: 
Size: 635106 Color: 0
Size: 364680 Color: 1

Bin 3043: 215 of cap free
Amount of items: 2
Items: 
Size: 689679 Color: 1
Size: 310107 Color: 0

Bin 3044: 216 of cap free
Amount of items: 2
Items: 
Size: 509269 Color: 1
Size: 490516 Color: 0

Bin 3045: 216 of cap free
Amount of items: 2
Items: 
Size: 543911 Color: 1
Size: 455874 Color: 0

Bin 3046: 216 of cap free
Amount of items: 2
Items: 
Size: 786346 Color: 1
Size: 213439 Color: 0

Bin 3047: 216 of cap free
Amount of items: 2
Items: 
Size: 780714 Color: 0
Size: 219071 Color: 1

Bin 3048: 216 of cap free
Amount of items: 2
Items: 
Size: 688328 Color: 0
Size: 311457 Color: 1

Bin 3049: 217 of cap free
Amount of items: 2
Items: 
Size: 520068 Color: 0
Size: 479716 Color: 1

Bin 3050: 217 of cap free
Amount of items: 2
Items: 
Size: 575507 Color: 1
Size: 424277 Color: 0

Bin 3051: 217 of cap free
Amount of items: 2
Items: 
Size: 685326 Color: 0
Size: 314458 Color: 1

Bin 3052: 218 of cap free
Amount of items: 2
Items: 
Size: 659741 Color: 1
Size: 340042 Color: 0

Bin 3053: 219 of cap free
Amount of items: 2
Items: 
Size: 568708 Color: 1
Size: 431074 Color: 0

Bin 3054: 219 of cap free
Amount of items: 2
Items: 
Size: 595460 Color: 0
Size: 404322 Color: 1

Bin 3055: 219 of cap free
Amount of items: 2
Items: 
Size: 646686 Color: 1
Size: 353096 Color: 0

Bin 3056: 219 of cap free
Amount of items: 2
Items: 
Size: 669266 Color: 0
Size: 330516 Color: 1

Bin 3057: 219 of cap free
Amount of items: 2
Items: 
Size: 721647 Color: 0
Size: 278135 Color: 1

Bin 3058: 219 of cap free
Amount of items: 2
Items: 
Size: 740944 Color: 1
Size: 258838 Color: 0

Bin 3059: 219 of cap free
Amount of items: 2
Items: 
Size: 789213 Color: 1
Size: 210569 Color: 0

Bin 3060: 220 of cap free
Amount of items: 2
Items: 
Size: 507365 Color: 0
Size: 492416 Color: 1

Bin 3061: 220 of cap free
Amount of items: 2
Items: 
Size: 578988 Color: 1
Size: 420793 Color: 0

Bin 3062: 220 of cap free
Amount of items: 2
Items: 
Size: 583219 Color: 1
Size: 416562 Color: 0

Bin 3063: 220 of cap free
Amount of items: 2
Items: 
Size: 658544 Color: 1
Size: 341237 Color: 0

Bin 3064: 220 of cap free
Amount of items: 2
Items: 
Size: 736350 Color: 0
Size: 263431 Color: 1

Bin 3065: 220 of cap free
Amount of items: 2
Items: 
Size: 794753 Color: 1
Size: 205028 Color: 0

Bin 3066: 221 of cap free
Amount of items: 2
Items: 
Size: 526986 Color: 0
Size: 472794 Color: 1

Bin 3067: 221 of cap free
Amount of items: 2
Items: 
Size: 513199 Color: 1
Size: 486581 Color: 0

Bin 3068: 221 of cap free
Amount of items: 2
Items: 
Size: 582859 Color: 1
Size: 416921 Color: 0

Bin 3069: 221 of cap free
Amount of items: 2
Items: 
Size: 758899 Color: 1
Size: 240881 Color: 0

Bin 3070: 222 of cap free
Amount of items: 2
Items: 
Size: 696243 Color: 0
Size: 303536 Color: 1

Bin 3071: 222 of cap free
Amount of items: 2
Items: 
Size: 703372 Color: 1
Size: 296407 Color: 0

Bin 3072: 222 of cap free
Amount of items: 2
Items: 
Size: 739782 Color: 0
Size: 259997 Color: 1

Bin 3073: 223 of cap free
Amount of items: 2
Items: 
Size: 551917 Color: 1
Size: 447861 Color: 0

Bin 3074: 223 of cap free
Amount of items: 2
Items: 
Size: 708844 Color: 0
Size: 290934 Color: 1

Bin 3075: 223 of cap free
Amount of items: 4
Items: 
Size: 313506 Color: 1
Size: 302575 Color: 1
Size: 241758 Color: 0
Size: 141939 Color: 0

Bin 3076: 224 of cap free
Amount of items: 2
Items: 
Size: 704769 Color: 0
Size: 295008 Color: 1

Bin 3077: 224 of cap free
Amount of items: 2
Items: 
Size: 795333 Color: 0
Size: 204444 Color: 1

Bin 3078: 224 of cap free
Amount of items: 2
Items: 
Size: 798335 Color: 1
Size: 201442 Color: 0

Bin 3079: 224 of cap free
Amount of items: 2
Items: 
Size: 584753 Color: 1
Size: 415024 Color: 0

Bin 3080: 225 of cap free
Amount of items: 2
Items: 
Size: 667434 Color: 0
Size: 332342 Color: 1

Bin 3081: 225 of cap free
Amount of items: 2
Items: 
Size: 720560 Color: 1
Size: 279216 Color: 0

Bin 3082: 225 of cap free
Amount of items: 2
Items: 
Size: 767931 Color: 1
Size: 231845 Color: 0

Bin 3083: 226 of cap free
Amount of items: 2
Items: 
Size: 735429 Color: 1
Size: 264346 Color: 0

Bin 3084: 226 of cap free
Amount of items: 2
Items: 
Size: 550914 Color: 0
Size: 448861 Color: 1

Bin 3085: 226 of cap free
Amount of items: 2
Items: 
Size: 602999 Color: 0
Size: 396776 Color: 1

Bin 3086: 226 of cap free
Amount of items: 2
Items: 
Size: 650778 Color: 0
Size: 348997 Color: 1

Bin 3087: 227 of cap free
Amount of items: 2
Items: 
Size: 664936 Color: 1
Size: 334838 Color: 0

Bin 3088: 227 of cap free
Amount of items: 2
Items: 
Size: 544047 Color: 0
Size: 455727 Color: 1

Bin 3089: 227 of cap free
Amount of items: 2
Items: 
Size: 563392 Color: 1
Size: 436382 Color: 0

Bin 3090: 227 of cap free
Amount of items: 2
Items: 
Size: 569078 Color: 1
Size: 430696 Color: 0

Bin 3091: 228 of cap free
Amount of items: 2
Items: 
Size: 620411 Color: 1
Size: 379362 Color: 0

Bin 3092: 228 of cap free
Amount of items: 2
Items: 
Size: 698619 Color: 0
Size: 301154 Color: 1

Bin 3093: 229 of cap free
Amount of items: 2
Items: 
Size: 764237 Color: 0
Size: 235535 Color: 1

Bin 3094: 230 of cap free
Amount of items: 2
Items: 
Size: 526840 Color: 1
Size: 472931 Color: 0

Bin 3095: 230 of cap free
Amount of items: 2
Items: 
Size: 532090 Color: 1
Size: 467681 Color: 0

Bin 3096: 230 of cap free
Amount of items: 2
Items: 
Size: 577744 Color: 0
Size: 422027 Color: 1

Bin 3097: 230 of cap free
Amount of items: 2
Items: 
Size: 657659 Color: 1
Size: 342112 Color: 0

Bin 3098: 230 of cap free
Amount of items: 2
Items: 
Size: 764685 Color: 1
Size: 235086 Color: 0

Bin 3099: 230 of cap free
Amount of items: 2
Items: 
Size: 771957 Color: 1
Size: 227814 Color: 0

Bin 3100: 231 of cap free
Amount of items: 2
Items: 
Size: 701440 Color: 0
Size: 298330 Color: 1

Bin 3101: 231 of cap free
Amount of items: 2
Items: 
Size: 769298 Color: 1
Size: 230472 Color: 0

Bin 3102: 232 of cap free
Amount of items: 2
Items: 
Size: 541640 Color: 0
Size: 458129 Color: 1

Bin 3103: 232 of cap free
Amount of items: 2
Items: 
Size: 552792 Color: 1
Size: 446977 Color: 0

Bin 3104: 232 of cap free
Amount of items: 2
Items: 
Size: 606469 Color: 1
Size: 393300 Color: 0

Bin 3105: 232 of cap free
Amount of items: 2
Items: 
Size: 618381 Color: 0
Size: 381388 Color: 1

Bin 3106: 232 of cap free
Amount of items: 2
Items: 
Size: 661521 Color: 0
Size: 338248 Color: 1

Bin 3107: 232 of cap free
Amount of items: 2
Items: 
Size: 691486 Color: 0
Size: 308283 Color: 1

Bin 3108: 232 of cap free
Amount of items: 2
Items: 
Size: 765768 Color: 1
Size: 234001 Color: 0

Bin 3109: 233 of cap free
Amount of items: 2
Items: 
Size: 605638 Color: 1
Size: 394130 Color: 0

Bin 3110: 233 of cap free
Amount of items: 2
Items: 
Size: 504103 Color: 0
Size: 495665 Color: 1

Bin 3111: 233 of cap free
Amount of items: 2
Items: 
Size: 638581 Color: 1
Size: 361187 Color: 0

Bin 3112: 233 of cap free
Amount of items: 2
Items: 
Size: 660695 Color: 0
Size: 339073 Color: 1

Bin 3113: 233 of cap free
Amount of items: 2
Items: 
Size: 726076 Color: 0
Size: 273692 Color: 1

Bin 3114: 233 of cap free
Amount of items: 2
Items: 
Size: 561497 Color: 1
Size: 438271 Color: 0

Bin 3115: 234 of cap free
Amount of items: 2
Items: 
Size: 524166 Color: 0
Size: 475601 Color: 1

Bin 3116: 234 of cap free
Amount of items: 2
Items: 
Size: 503270 Color: 1
Size: 496497 Color: 0

Bin 3117: 234 of cap free
Amount of items: 2
Items: 
Size: 616701 Color: 1
Size: 383066 Color: 0

Bin 3118: 234 of cap free
Amount of items: 2
Items: 
Size: 626933 Color: 0
Size: 372834 Color: 1

Bin 3119: 234 of cap free
Amount of items: 2
Items: 
Size: 639627 Color: 1
Size: 360140 Color: 0

Bin 3120: 234 of cap free
Amount of items: 2
Items: 
Size: 701853 Color: 0
Size: 297914 Color: 1

Bin 3121: 235 of cap free
Amount of items: 2
Items: 
Size: 587700 Color: 1
Size: 412066 Color: 0

Bin 3122: 235 of cap free
Amount of items: 2
Items: 
Size: 589290 Color: 1
Size: 410476 Color: 0

Bin 3123: 235 of cap free
Amount of items: 2
Items: 
Size: 594130 Color: 1
Size: 405636 Color: 0

Bin 3124: 235 of cap free
Amount of items: 2
Items: 
Size: 597830 Color: 0
Size: 401936 Color: 1

Bin 3125: 235 of cap free
Amount of items: 2
Items: 
Size: 697311 Color: 1
Size: 302455 Color: 0

Bin 3126: 236 of cap free
Amount of items: 2
Items: 
Size: 544790 Color: 0
Size: 454975 Color: 1

Bin 3127: 236 of cap free
Amount of items: 2
Items: 
Size: 588225 Color: 0
Size: 411540 Color: 1

Bin 3128: 236 of cap free
Amount of items: 2
Items: 
Size: 609957 Color: 1
Size: 389808 Color: 0

Bin 3129: 237 of cap free
Amount of items: 2
Items: 
Size: 619316 Color: 1
Size: 380448 Color: 0

Bin 3130: 237 of cap free
Amount of items: 2
Items: 
Size: 782936 Color: 1
Size: 216828 Color: 0

Bin 3131: 239 of cap free
Amount of items: 2
Items: 
Size: 730503 Color: 1
Size: 269259 Color: 0

Bin 3132: 239 of cap free
Amount of items: 2
Items: 
Size: 509357 Color: 0
Size: 490405 Color: 1

Bin 3133: 239 of cap free
Amount of items: 2
Items: 
Size: 606829 Color: 1
Size: 392933 Color: 0

Bin 3134: 241 of cap free
Amount of items: 2
Items: 
Size: 771745 Color: 0
Size: 228015 Color: 1

Bin 3135: 242 of cap free
Amount of items: 2
Items: 
Size: 525491 Color: 0
Size: 474268 Color: 1

Bin 3136: 242 of cap free
Amount of items: 2
Items: 
Size: 529581 Color: 1
Size: 470178 Color: 0

Bin 3137: 242 of cap free
Amount of items: 2
Items: 
Size: 738471 Color: 1
Size: 261288 Color: 0

Bin 3138: 243 of cap free
Amount of items: 2
Items: 
Size: 591590 Color: 1
Size: 408168 Color: 0

Bin 3139: 244 of cap free
Amount of items: 2
Items: 
Size: 522675 Color: 0
Size: 477082 Color: 1

Bin 3140: 244 of cap free
Amount of items: 2
Items: 
Size: 749722 Color: 0
Size: 250035 Color: 1

Bin 3141: 245 of cap free
Amount of items: 2
Items: 
Size: 515555 Color: 1
Size: 484201 Color: 0

Bin 3142: 245 of cap free
Amount of items: 2
Items: 
Size: 652172 Color: 1
Size: 347584 Color: 0

Bin 3143: 245 of cap free
Amount of items: 2
Items: 
Size: 652906 Color: 0
Size: 346850 Color: 1

Bin 3144: 245 of cap free
Amount of items: 2
Items: 
Size: 704284 Color: 1
Size: 295472 Color: 0

Bin 3145: 246 of cap free
Amount of items: 2
Items: 
Size: 742601 Color: 1
Size: 257154 Color: 0

Bin 3146: 247 of cap free
Amount of items: 2
Items: 
Size: 592653 Color: 1
Size: 407101 Color: 0

Bin 3147: 247 of cap free
Amount of items: 2
Items: 
Size: 632108 Color: 0
Size: 367646 Color: 1

Bin 3148: 247 of cap free
Amount of items: 2
Items: 
Size: 552779 Color: 1
Size: 446975 Color: 0

Bin 3149: 247 of cap free
Amount of items: 2
Items: 
Size: 692815 Color: 1
Size: 306939 Color: 0

Bin 3150: 247 of cap free
Amount of items: 2
Items: 
Size: 721901 Color: 0
Size: 277853 Color: 1

Bin 3151: 247 of cap free
Amount of items: 2
Items: 
Size: 792183 Color: 0
Size: 207571 Color: 1

Bin 3152: 248 of cap free
Amount of items: 2
Items: 
Size: 536498 Color: 1
Size: 463255 Color: 0

Bin 3153: 249 of cap free
Amount of items: 2
Items: 
Size: 567191 Color: 0
Size: 432561 Color: 1

Bin 3154: 249 of cap free
Amount of items: 2
Items: 
Size: 637369 Color: 1
Size: 362383 Color: 0

Bin 3155: 249 of cap free
Amount of items: 2
Items: 
Size: 672071 Color: 1
Size: 327681 Color: 0

Bin 3156: 250 of cap free
Amount of items: 2
Items: 
Size: 702471 Color: 1
Size: 297280 Color: 0

Bin 3157: 250 of cap free
Amount of items: 2
Items: 
Size: 619940 Color: 1
Size: 379811 Color: 0

Bin 3158: 250 of cap free
Amount of items: 2
Items: 
Size: 748998 Color: 0
Size: 250753 Color: 1

Bin 3159: 250 of cap free
Amount of items: 2
Items: 
Size: 559897 Color: 1
Size: 439854 Color: 0

Bin 3160: 251 of cap free
Amount of items: 2
Items: 
Size: 757636 Color: 1
Size: 242114 Color: 0

Bin 3161: 252 of cap free
Amount of items: 6
Items: 
Size: 208233 Color: 0
Size: 163381 Color: 0
Size: 158053 Color: 1
Size: 157869 Color: 1
Size: 157392 Color: 0
Size: 154821 Color: 1

Bin 3162: 252 of cap free
Amount of items: 2
Items: 
Size: 527501 Color: 0
Size: 472248 Color: 1

Bin 3163: 252 of cap free
Amount of items: 2
Items: 
Size: 601161 Color: 0
Size: 398588 Color: 1

Bin 3164: 252 of cap free
Amount of items: 2
Items: 
Size: 505081 Color: 0
Size: 494668 Color: 1

Bin 3165: 253 of cap free
Amount of items: 2
Items: 
Size: 637655 Color: 1
Size: 362093 Color: 0

Bin 3166: 253 of cap free
Amount of items: 2
Items: 
Size: 533194 Color: 1
Size: 466554 Color: 0

Bin 3167: 253 of cap free
Amount of items: 2
Items: 
Size: 640633 Color: 1
Size: 359115 Color: 0

Bin 3168: 253 of cap free
Amount of items: 2
Items: 
Size: 736640 Color: 0
Size: 263108 Color: 1

Bin 3169: 253 of cap free
Amount of items: 2
Items: 
Size: 751412 Color: 1
Size: 248336 Color: 0

Bin 3170: 254 of cap free
Amount of items: 2
Items: 
Size: 655309 Color: 1
Size: 344438 Color: 0

Bin 3171: 254 of cap free
Amount of items: 2
Items: 
Size: 510443 Color: 0
Size: 489304 Color: 1

Bin 3172: 254 of cap free
Amount of items: 2
Items: 
Size: 591051 Color: 1
Size: 408696 Color: 0

Bin 3173: 254 of cap free
Amount of items: 2
Items: 
Size: 628061 Color: 1
Size: 371686 Color: 0

Bin 3174: 254 of cap free
Amount of items: 2
Items: 
Size: 687944 Color: 0
Size: 311803 Color: 1

Bin 3175: 254 of cap free
Amount of items: 2
Items: 
Size: 639892 Color: 1
Size: 359855 Color: 0

Bin 3176: 255 of cap free
Amount of items: 2
Items: 
Size: 681625 Color: 1
Size: 318121 Color: 0

Bin 3177: 256 of cap free
Amount of items: 2
Items: 
Size: 677816 Color: 0
Size: 321929 Color: 1

Bin 3178: 257 of cap free
Amount of items: 2
Items: 
Size: 538933 Color: 0
Size: 460811 Color: 1

Bin 3179: 257 of cap free
Amount of items: 2
Items: 
Size: 778845 Color: 1
Size: 220899 Color: 0

Bin 3180: 258 of cap free
Amount of items: 2
Items: 
Size: 565732 Color: 0
Size: 434011 Color: 1

Bin 3181: 258 of cap free
Amount of items: 2
Items: 
Size: 625985 Color: 0
Size: 373758 Color: 1

Bin 3182: 258 of cap free
Amount of items: 2
Items: 
Size: 733200 Color: 1
Size: 266543 Color: 0

Bin 3183: 259 of cap free
Amount of items: 2
Items: 
Size: 582762 Color: 0
Size: 416980 Color: 1

Bin 3184: 260 of cap free
Amount of items: 2
Items: 
Size: 549387 Color: 0
Size: 450354 Color: 1

Bin 3185: 260 of cap free
Amount of items: 2
Items: 
Size: 772667 Color: 1
Size: 227074 Color: 0

Bin 3186: 261 of cap free
Amount of items: 2
Items: 
Size: 551367 Color: 0
Size: 448373 Color: 1

Bin 3187: 261 of cap free
Amount of items: 2
Items: 
Size: 682138 Color: 1
Size: 317602 Color: 0

Bin 3188: 262 of cap free
Amount of items: 2
Items: 
Size: 775220 Color: 1
Size: 224519 Color: 0

Bin 3189: 262 of cap free
Amount of items: 2
Items: 
Size: 669666 Color: 0
Size: 330073 Color: 1

Bin 3190: 263 of cap free
Amount of items: 2
Items: 
Size: 700734 Color: 0
Size: 299004 Color: 1

Bin 3191: 263 of cap free
Amount of items: 2
Items: 
Size: 555650 Color: 0
Size: 444088 Color: 1

Bin 3192: 263 of cap free
Amount of items: 2
Items: 
Size: 643222 Color: 1
Size: 356516 Color: 0

Bin 3193: 263 of cap free
Amount of items: 2
Items: 
Size: 701158 Color: 1
Size: 298580 Color: 0

Bin 3194: 263 of cap free
Amount of items: 2
Items: 
Size: 764235 Color: 0
Size: 235503 Color: 1

Bin 3195: 263 of cap free
Amount of items: 2
Items: 
Size: 764995 Color: 1
Size: 234743 Color: 0

Bin 3196: 264 of cap free
Amount of items: 2
Items: 
Size: 641571 Color: 1
Size: 358166 Color: 0

Bin 3197: 264 of cap free
Amount of items: 2
Items: 
Size: 673748 Color: 1
Size: 325989 Color: 0

Bin 3198: 265 of cap free
Amount of items: 2
Items: 
Size: 614340 Color: 1
Size: 385396 Color: 0

Bin 3199: 265 of cap free
Amount of items: 2
Items: 
Size: 727556 Color: 0
Size: 272180 Color: 1

Bin 3200: 266 of cap free
Amount of items: 2
Items: 
Size: 645219 Color: 1
Size: 354516 Color: 0

Bin 3201: 266 of cap free
Amount of items: 2
Items: 
Size: 725239 Color: 1
Size: 274496 Color: 0

Bin 3202: 266 of cap free
Amount of items: 2
Items: 
Size: 755653 Color: 1
Size: 244082 Color: 0

Bin 3203: 267 of cap free
Amount of items: 2
Items: 
Size: 525334 Color: 1
Size: 474400 Color: 0

Bin 3204: 267 of cap free
Amount of items: 2
Items: 
Size: 794108 Color: 1
Size: 205626 Color: 0

Bin 3205: 268 of cap free
Amount of items: 2
Items: 
Size: 661389 Color: 1
Size: 338344 Color: 0

Bin 3206: 268 of cap free
Amount of items: 2
Items: 
Size: 682682 Color: 0
Size: 317051 Color: 1

Bin 3207: 268 of cap free
Amount of items: 2
Items: 
Size: 726053 Color: 0
Size: 273680 Color: 1

Bin 3208: 269 of cap free
Amount of items: 2
Items: 
Size: 586737 Color: 0
Size: 412995 Color: 1

Bin 3209: 269 of cap free
Amount of items: 2
Items: 
Size: 608466 Color: 0
Size: 391266 Color: 1

Bin 3210: 270 of cap free
Amount of items: 2
Items: 
Size: 758879 Color: 1
Size: 240852 Color: 0

Bin 3211: 270 of cap free
Amount of items: 2
Items: 
Size: 789793 Color: 1
Size: 209938 Color: 0

Bin 3212: 270 of cap free
Amount of items: 2
Items: 
Size: 789875 Color: 0
Size: 209856 Color: 1

Bin 3213: 271 of cap free
Amount of items: 2
Items: 
Size: 657649 Color: 0
Size: 342081 Color: 1

Bin 3214: 271 of cap free
Amount of items: 2
Items: 
Size: 741248 Color: 0
Size: 258482 Color: 1

Bin 3215: 271 of cap free
Amount of items: 2
Items: 
Size: 796123 Color: 1
Size: 203607 Color: 0

Bin 3216: 272 of cap free
Amount of items: 2
Items: 
Size: 584025 Color: 1
Size: 415704 Color: 0

Bin 3217: 272 of cap free
Amount of items: 2
Items: 
Size: 723964 Color: 0
Size: 275765 Color: 1

Bin 3218: 272 of cap free
Amount of items: 2
Items: 
Size: 789201 Color: 1
Size: 210528 Color: 0

Bin 3219: 273 of cap free
Amount of items: 2
Items: 
Size: 604479 Color: 1
Size: 395249 Color: 0

Bin 3220: 273 of cap free
Amount of items: 2
Items: 
Size: 662342 Color: 0
Size: 337386 Color: 1

Bin 3221: 274 of cap free
Amount of items: 2
Items: 
Size: 540719 Color: 1
Size: 459008 Color: 0

Bin 3222: 274 of cap free
Amount of items: 2
Items: 
Size: 506126 Color: 0
Size: 493601 Color: 1

Bin 3223: 275 of cap free
Amount of items: 2
Items: 
Size: 735839 Color: 1
Size: 263887 Color: 0

Bin 3224: 275 of cap free
Amount of items: 2
Items: 
Size: 581803 Color: 0
Size: 417923 Color: 1

Bin 3225: 275 of cap free
Amount of items: 2
Items: 
Size: 658539 Color: 1
Size: 341187 Color: 0

Bin 3226: 276 of cap free
Amount of items: 2
Items: 
Size: 546078 Color: 1
Size: 453647 Color: 0

Bin 3227: 276 of cap free
Amount of items: 2
Items: 
Size: 568096 Color: 1
Size: 431629 Color: 0

Bin 3228: 276 of cap free
Amount of items: 2
Items: 
Size: 696919 Color: 1
Size: 302806 Color: 0

Bin 3229: 276 of cap free
Amount of items: 2
Items: 
Size: 737459 Color: 1
Size: 262266 Color: 0

Bin 3230: 277 of cap free
Amount of items: 2
Items: 
Size: 591556 Color: 1
Size: 408168 Color: 0

Bin 3231: 277 of cap free
Amount of items: 2
Items: 
Size: 635310 Color: 1
Size: 364414 Color: 0

Bin 3232: 278 of cap free
Amount of items: 2
Items: 
Size: 531750 Color: 1
Size: 467973 Color: 0

Bin 3233: 278 of cap free
Amount of items: 2
Items: 
Size: 720165 Color: 1
Size: 279558 Color: 0

Bin 3234: 279 of cap free
Amount of items: 2
Items: 
Size: 643074 Color: 0
Size: 356648 Color: 1

Bin 3235: 279 of cap free
Amount of items: 2
Items: 
Size: 729516 Color: 0
Size: 270206 Color: 1

Bin 3236: 280 of cap free
Amount of items: 2
Items: 
Size: 520897 Color: 0
Size: 478824 Color: 1

Bin 3237: 280 of cap free
Amount of items: 2
Items: 
Size: 570792 Color: 1
Size: 428929 Color: 0

Bin 3238: 280 of cap free
Amount of items: 2
Items: 
Size: 642740 Color: 1
Size: 356981 Color: 0

Bin 3239: 280 of cap free
Amount of items: 2
Items: 
Size: 678562 Color: 0
Size: 321159 Color: 1

Bin 3240: 280 of cap free
Amount of items: 2
Items: 
Size: 766429 Color: 1
Size: 233292 Color: 0

Bin 3241: 281 of cap free
Amount of items: 2
Items: 
Size: 576827 Color: 0
Size: 422893 Color: 1

Bin 3242: 281 of cap free
Amount of items: 2
Items: 
Size: 637073 Color: 1
Size: 362647 Color: 0

Bin 3243: 281 of cap free
Amount of items: 2
Items: 
Size: 798292 Color: 1
Size: 201428 Color: 0

Bin 3244: 282 of cap free
Amount of items: 2
Items: 
Size: 613904 Color: 0
Size: 385815 Color: 1

Bin 3245: 282 of cap free
Amount of items: 2
Items: 
Size: 507518 Color: 1
Size: 492201 Color: 0

Bin 3246: 282 of cap free
Amount of items: 2
Items: 
Size: 520079 Color: 1
Size: 479640 Color: 0

Bin 3247: 282 of cap free
Amount of items: 2
Items: 
Size: 665672 Color: 0
Size: 334047 Color: 1

Bin 3248: 282 of cap free
Amount of items: 2
Items: 
Size: 731367 Color: 0
Size: 268352 Color: 1

Bin 3249: 282 of cap free
Amount of items: 2
Items: 
Size: 642210 Color: 1
Size: 357509 Color: 0

Bin 3250: 282 of cap free
Amount of items: 2
Items: 
Size: 572315 Color: 1
Size: 427404 Color: 0

Bin 3251: 283 of cap free
Amount of items: 2
Items: 
Size: 680700 Color: 0
Size: 319018 Color: 1

Bin 3252: 283 of cap free
Amount of items: 2
Items: 
Size: 510120 Color: 0
Size: 489598 Color: 1

Bin 3253: 283 of cap free
Amount of items: 2
Items: 
Size: 635740 Color: 1
Size: 363978 Color: 0

Bin 3254: 283 of cap free
Amount of items: 2
Items: 
Size: 774666 Color: 1
Size: 225052 Color: 0

Bin 3255: 283 of cap free
Amount of items: 2
Items: 
Size: 797944 Color: 1
Size: 201774 Color: 0

Bin 3256: 284 of cap free
Amount of items: 2
Items: 
Size: 707889 Color: 0
Size: 291828 Color: 1

Bin 3257: 285 of cap free
Amount of items: 2
Items: 
Size: 516652 Color: 1
Size: 483064 Color: 0

Bin 3258: 285 of cap free
Amount of items: 2
Items: 
Size: 594623 Color: 1
Size: 405093 Color: 0

Bin 3259: 285 of cap free
Amount of items: 2
Items: 
Size: 619104 Color: 0
Size: 380612 Color: 1

Bin 3260: 285 of cap free
Amount of items: 2
Items: 
Size: 562242 Color: 1
Size: 437474 Color: 0

Bin 3261: 286 of cap free
Amount of items: 2
Items: 
Size: 599031 Color: 1
Size: 400684 Color: 0

Bin 3262: 286 of cap free
Amount of items: 2
Items: 
Size: 634659 Color: 1
Size: 365056 Color: 0

Bin 3263: 286 of cap free
Amount of items: 2
Items: 
Size: 717864 Color: 0
Size: 281851 Color: 1

Bin 3264: 287 of cap free
Amount of items: 2
Items: 
Size: 620415 Color: 0
Size: 379299 Color: 1

Bin 3265: 287 of cap free
Amount of items: 2
Items: 
Size: 668833 Color: 1
Size: 330881 Color: 0

Bin 3266: 288 of cap free
Amount of items: 2
Items: 
Size: 605916 Color: 0
Size: 393797 Color: 1

Bin 3267: 288 of cap free
Amount of items: 2
Items: 
Size: 705138 Color: 0
Size: 294575 Color: 1

Bin 3268: 290 of cap free
Amount of items: 2
Items: 
Size: 518834 Color: 0
Size: 480877 Color: 1

Bin 3269: 290 of cap free
Amount of items: 2
Items: 
Size: 632508 Color: 1
Size: 367203 Color: 0

Bin 3270: 290 of cap free
Amount of items: 2
Items: 
Size: 677875 Color: 1
Size: 321836 Color: 0

Bin 3271: 291 of cap free
Amount of items: 2
Items: 
Size: 694565 Color: 1
Size: 305145 Color: 0

Bin 3272: 292 of cap free
Amount of items: 2
Items: 
Size: 722175 Color: 1
Size: 277534 Color: 0

Bin 3273: 293 of cap free
Amount of items: 2
Items: 
Size: 624005 Color: 0
Size: 375703 Color: 1

Bin 3274: 293 of cap free
Amount of items: 2
Items: 
Size: 773572 Color: 0
Size: 226136 Color: 1

Bin 3275: 294 of cap free
Amount of items: 2
Items: 
Size: 539755 Color: 1
Size: 459952 Color: 0

Bin 3276: 294 of cap free
Amount of items: 2
Items: 
Size: 647323 Color: 0
Size: 352384 Color: 1

Bin 3277: 294 of cap free
Amount of items: 2
Items: 
Size: 767181 Color: 1
Size: 232526 Color: 0

Bin 3278: 295 of cap free
Amount of items: 2
Items: 
Size: 520029 Color: 0
Size: 479677 Color: 1

Bin 3279: 295 of cap free
Amount of items: 2
Items: 
Size: 541048 Color: 1
Size: 458658 Color: 0

Bin 3280: 295 of cap free
Amount of items: 2
Items: 
Size: 714947 Color: 0
Size: 284759 Color: 1

Bin 3281: 296 of cap free
Amount of items: 2
Items: 
Size: 515246 Color: 0
Size: 484459 Color: 1

Bin 3282: 296 of cap free
Amount of items: 2
Items: 
Size: 529072 Color: 0
Size: 470633 Color: 1

Bin 3283: 296 of cap free
Amount of items: 2
Items: 
Size: 736656 Color: 1
Size: 263049 Color: 0

Bin 3284: 297 of cap free
Amount of items: 2
Items: 
Size: 774622 Color: 0
Size: 225082 Color: 1

Bin 3285: 297 of cap free
Amount of items: 2
Items: 
Size: 670696 Color: 1
Size: 329008 Color: 0

Bin 3286: 298 of cap free
Amount of items: 2
Items: 
Size: 643221 Color: 1
Size: 356482 Color: 0

Bin 3287: 299 of cap free
Amount of items: 5
Items: 
Size: 283409 Color: 1
Size: 254650 Color: 1
Size: 169550 Color: 0
Size: 166078 Color: 1
Size: 126015 Color: 0

Bin 3288: 299 of cap free
Amount of items: 2
Items: 
Size: 736625 Color: 0
Size: 263077 Color: 1

Bin 3289: 299 of cap free
Amount of items: 2
Items: 
Size: 766738 Color: 0
Size: 232964 Color: 1

Bin 3290: 300 of cap free
Amount of items: 2
Items: 
Size: 798885 Color: 0
Size: 200816 Color: 1

Bin 3291: 300 of cap free
Amount of items: 2
Items: 
Size: 784321 Color: 1
Size: 215380 Color: 0

Bin 3292: 300 of cap free
Amount of items: 2
Items: 
Size: 787682 Color: 0
Size: 212019 Color: 1

Bin 3293: 300 of cap free
Amount of items: 2
Items: 
Size: 642728 Color: 1
Size: 356973 Color: 0

Bin 3294: 300 of cap free
Amount of items: 2
Items: 
Size: 727987 Color: 1
Size: 271714 Color: 0

Bin 3295: 301 of cap free
Amount of items: 2
Items: 
Size: 613573 Color: 1
Size: 386127 Color: 0

Bin 3296: 302 of cap free
Amount of items: 2
Items: 
Size: 636622 Color: 0
Size: 363077 Color: 1

Bin 3297: 302 of cap free
Amount of items: 2
Items: 
Size: 685673 Color: 0
Size: 314026 Color: 1

Bin 3298: 302 of cap free
Amount of items: 2
Items: 
Size: 756355 Color: 1
Size: 243344 Color: 0

Bin 3299: 303 of cap free
Amount of items: 2
Items: 
Size: 718767 Color: 0
Size: 280931 Color: 1

Bin 3300: 303 of cap free
Amount of items: 2
Items: 
Size: 781093 Color: 1
Size: 218605 Color: 0

Bin 3301: 303 of cap free
Amount of items: 2
Items: 
Size: 575424 Color: 0
Size: 424274 Color: 1

Bin 3302: 305 of cap free
Amount of items: 3
Items: 
Size: 738134 Color: 1
Size: 131370 Color: 0
Size: 130192 Color: 1

Bin 3303: 305 of cap free
Amount of items: 2
Items: 
Size: 573979 Color: 1
Size: 425717 Color: 0

Bin 3304: 305 of cap free
Amount of items: 2
Items: 
Size: 601633 Color: 1
Size: 398063 Color: 0

Bin 3305: 305 of cap free
Amount of items: 2
Items: 
Size: 652883 Color: 0
Size: 346813 Color: 1

Bin 3306: 306 of cap free
Amount of items: 2
Items: 
Size: 584270 Color: 0
Size: 415425 Color: 1

Bin 3307: 306 of cap free
Amount of items: 2
Items: 
Size: 621262 Color: 1
Size: 378433 Color: 0

Bin 3308: 306 of cap free
Amount of items: 2
Items: 
Size: 653360 Color: 0
Size: 346335 Color: 1

Bin 3309: 307 of cap free
Amount of items: 2
Items: 
Size: 729506 Color: 0
Size: 270188 Color: 1

Bin 3310: 307 of cap free
Amount of items: 2
Items: 
Size: 510370 Color: 1
Size: 489324 Color: 0

Bin 3311: 307 of cap free
Amount of items: 2
Items: 
Size: 516185 Color: 1
Size: 483509 Color: 0

Bin 3312: 307 of cap free
Amount of items: 2
Items: 
Size: 768857 Color: 1
Size: 230837 Color: 0

Bin 3313: 307 of cap free
Amount of items: 2
Items: 
Size: 526085 Color: 1
Size: 473609 Color: 0

Bin 3314: 308 of cap free
Amount of items: 2
Items: 
Size: 698591 Color: 0
Size: 301102 Color: 1

Bin 3315: 309 of cap free
Amount of items: 2
Items: 
Size: 600325 Color: 1
Size: 399367 Color: 0

Bin 3316: 309 of cap free
Amount of items: 2
Items: 
Size: 713481 Color: 1
Size: 286211 Color: 0

Bin 3317: 310 of cap free
Amount of items: 2
Items: 
Size: 719574 Color: 1
Size: 280117 Color: 0

Bin 3318: 310 of cap free
Amount of items: 2
Items: 
Size: 747771 Color: 0
Size: 251920 Color: 1

Bin 3319: 311 of cap free
Amount of items: 2
Items: 
Size: 788863 Color: 1
Size: 210827 Color: 0

Bin 3320: 311 of cap free
Amount of items: 2
Items: 
Size: 578151 Color: 1
Size: 421539 Color: 0

Bin 3321: 311 of cap free
Amount of items: 2
Items: 
Size: 615128 Color: 1
Size: 384562 Color: 0

Bin 3322: 311 of cap free
Amount of items: 2
Items: 
Size: 708619 Color: 1
Size: 291071 Color: 0

Bin 3323: 312 of cap free
Amount of items: 2
Items: 
Size: 616698 Color: 0
Size: 382991 Color: 1

Bin 3324: 312 of cap free
Amount of items: 2
Items: 
Size: 533455 Color: 1
Size: 466234 Color: 0

Bin 3325: 312 of cap free
Amount of items: 2
Items: 
Size: 535207 Color: 0
Size: 464482 Color: 1

Bin 3326: 313 of cap free
Amount of items: 2
Items: 
Size: 762920 Color: 1
Size: 236768 Color: 0

Bin 3327: 314 of cap free
Amount of items: 2
Items: 
Size: 517975 Color: 0
Size: 481712 Color: 1

Bin 3328: 314 of cap free
Amount of items: 2
Items: 
Size: 730956 Color: 1
Size: 268731 Color: 0

Bin 3329: 314 of cap free
Amount of items: 2
Items: 
Size: 751539 Color: 0
Size: 248148 Color: 1

Bin 3330: 314 of cap free
Amount of items: 2
Items: 
Size: 761676 Color: 0
Size: 238011 Color: 1

Bin 3331: 315 of cap free
Amount of items: 2
Items: 
Size: 526019 Color: 0
Size: 473667 Color: 1

Bin 3332: 315 of cap free
Amount of items: 2
Items: 
Size: 757839 Color: 0
Size: 241847 Color: 1

Bin 3333: 316 of cap free
Amount of items: 2
Items: 
Size: 668525 Color: 0
Size: 331160 Color: 1

Bin 3334: 316 of cap free
Amount of items: 2
Items: 
Size: 790987 Color: 1
Size: 208698 Color: 0

Bin 3335: 317 of cap free
Amount of items: 2
Items: 
Size: 553584 Color: 1
Size: 446100 Color: 0

Bin 3336: 317 of cap free
Amount of items: 2
Items: 
Size: 718364 Color: 0
Size: 281320 Color: 1

Bin 3337: 317 of cap free
Amount of items: 2
Items: 
Size: 747611 Color: 1
Size: 252073 Color: 0

Bin 3338: 318 of cap free
Amount of items: 2
Items: 
Size: 576796 Color: 0
Size: 422887 Color: 1

Bin 3339: 318 of cap free
Amount of items: 2
Items: 
Size: 680163 Color: 0
Size: 319520 Color: 1

Bin 3340: 318 of cap free
Amount of items: 2
Items: 
Size: 748585 Color: 1
Size: 251098 Color: 0

Bin 3341: 319 of cap free
Amount of items: 2
Items: 
Size: 539736 Color: 0
Size: 459946 Color: 1

Bin 3342: 319 of cap free
Amount of items: 2
Items: 
Size: 677858 Color: 1
Size: 321824 Color: 0

Bin 3343: 319 of cap free
Amount of items: 2
Items: 
Size: 701825 Color: 0
Size: 297857 Color: 1

Bin 3344: 319 of cap free
Amount of items: 2
Items: 
Size: 711095 Color: 0
Size: 288587 Color: 1

Bin 3345: 320 of cap free
Amount of items: 2
Items: 
Size: 530152 Color: 1
Size: 469529 Color: 0

Bin 3346: 320 of cap free
Amount of items: 2
Items: 
Size: 550318 Color: 1
Size: 449363 Color: 0

Bin 3347: 320 of cap free
Amount of items: 2
Items: 
Size: 593726 Color: 0
Size: 405955 Color: 1

Bin 3348: 320 of cap free
Amount of items: 2
Items: 
Size: 734413 Color: 1
Size: 265268 Color: 0

Bin 3349: 321 of cap free
Amount of items: 2
Items: 
Size: 585210 Color: 1
Size: 414470 Color: 0

Bin 3350: 322 of cap free
Amount of items: 2
Items: 
Size: 520588 Color: 1
Size: 479091 Color: 0

Bin 3351: 323 of cap free
Amount of items: 2
Items: 
Size: 626885 Color: 0
Size: 372793 Color: 1

Bin 3352: 323 of cap free
Amount of items: 2
Items: 
Size: 754559 Color: 1
Size: 245119 Color: 0

Bin 3353: 323 of cap free
Amount of items: 2
Items: 
Size: 684424 Color: 1
Size: 315254 Color: 0

Bin 3354: 323 of cap free
Amount of items: 2
Items: 
Size: 513952 Color: 1
Size: 485726 Color: 0

Bin 3355: 323 of cap free
Amount of items: 2
Items: 
Size: 516635 Color: 1
Size: 483043 Color: 0

Bin 3356: 324 of cap free
Amount of items: 2
Items: 
Size: 536912 Color: 1
Size: 462765 Color: 0

Bin 3357: 324 of cap free
Amount of items: 2
Items: 
Size: 710107 Color: 1
Size: 289570 Color: 0

Bin 3358: 325 of cap free
Amount of items: 2
Items: 
Size: 552190 Color: 0
Size: 447486 Color: 1

Bin 3359: 325 of cap free
Amount of items: 2
Items: 
Size: 553581 Color: 1
Size: 446095 Color: 0

Bin 3360: 325 of cap free
Amount of items: 2
Items: 
Size: 762103 Color: 0
Size: 237573 Color: 1

Bin 3361: 326 of cap free
Amount of items: 2
Items: 
Size: 672381 Color: 1
Size: 327294 Color: 0

Bin 3362: 326 of cap free
Amount of items: 2
Items: 
Size: 756286 Color: 0
Size: 243389 Color: 1

Bin 3363: 327 of cap free
Amount of items: 2
Items: 
Size: 500930 Color: 1
Size: 498744 Color: 0

Bin 3364: 327 of cap free
Amount of items: 2
Items: 
Size: 772637 Color: 1
Size: 227037 Color: 0

Bin 3365: 327 of cap free
Amount of items: 2
Items: 
Size: 755632 Color: 1
Size: 244042 Color: 0

Bin 3366: 329 of cap free
Amount of items: 2
Items: 
Size: 566273 Color: 1
Size: 433399 Color: 0

Bin 3367: 329 of cap free
Amount of items: 2
Items: 
Size: 586790 Color: 1
Size: 412882 Color: 0

Bin 3368: 329 of cap free
Amount of items: 2
Items: 
Size: 602572 Color: 1
Size: 397100 Color: 0

Bin 3369: 329 of cap free
Amount of items: 2
Items: 
Size: 792372 Color: 1
Size: 207300 Color: 0

Bin 3370: 331 of cap free
Amount of items: 2
Items: 
Size: 521334 Color: 0
Size: 478336 Color: 1

Bin 3371: 331 of cap free
Amount of items: 2
Items: 
Size: 505545 Color: 0
Size: 494125 Color: 1

Bin 3372: 331 of cap free
Amount of items: 2
Items: 
Size: 537576 Color: 1
Size: 462094 Color: 0

Bin 3373: 331 of cap free
Amount of items: 2
Items: 
Size: 555137 Color: 1
Size: 444533 Color: 0

Bin 3374: 332 of cap free
Amount of items: 2
Items: 
Size: 544314 Color: 1
Size: 455355 Color: 0

Bin 3375: 332 of cap free
Amount of items: 2
Items: 
Size: 577388 Color: 1
Size: 422281 Color: 0

Bin 3376: 332 of cap free
Amount of items: 2
Items: 
Size: 658499 Color: 1
Size: 341170 Color: 0

Bin 3377: 332 of cap free
Amount of items: 2
Items: 
Size: 673298 Color: 0
Size: 326371 Color: 1

Bin 3378: 333 of cap free
Amount of items: 2
Items: 
Size: 650099 Color: 0
Size: 349569 Color: 1

Bin 3379: 333 of cap free
Amount of items: 2
Items: 
Size: 693175 Color: 1
Size: 306493 Color: 0

Bin 3380: 333 of cap free
Amount of items: 2
Items: 
Size: 723936 Color: 0
Size: 275732 Color: 1

Bin 3381: 334 of cap free
Amount of items: 2
Items: 
Size: 781092 Color: 1
Size: 218575 Color: 0

Bin 3382: 334 of cap free
Amount of items: 2
Items: 
Size: 532153 Color: 0
Size: 467514 Color: 1

Bin 3383: 334 of cap free
Amount of items: 2
Items: 
Size: 799806 Color: 0
Size: 199861 Color: 1

Bin 3384: 335 of cap free
Amount of items: 2
Items: 
Size: 558286 Color: 1
Size: 441380 Color: 0

Bin 3385: 336 of cap free
Amount of items: 2
Items: 
Size: 779790 Color: 0
Size: 219875 Color: 1

Bin 3386: 337 of cap free
Amount of items: 2
Items: 
Size: 580747 Color: 0
Size: 418917 Color: 1

Bin 3387: 337 of cap free
Amount of items: 2
Items: 
Size: 611164 Color: 0
Size: 388500 Color: 1

Bin 3388: 337 of cap free
Amount of items: 2
Items: 
Size: 629799 Color: 1
Size: 369865 Color: 0

Bin 3389: 337 of cap free
Amount of items: 2
Items: 
Size: 797907 Color: 1
Size: 201757 Color: 0

Bin 3390: 338 of cap free
Amount of items: 2
Items: 
Size: 524630 Color: 0
Size: 475033 Color: 1

Bin 3391: 339 of cap free
Amount of items: 2
Items: 
Size: 739958 Color: 1
Size: 259704 Color: 0

Bin 3392: 339 of cap free
Amount of items: 2
Items: 
Size: 703727 Color: 0
Size: 295935 Color: 1

Bin 3393: 339 of cap free
Amount of items: 2
Items: 
Size: 761036 Color: 1
Size: 238626 Color: 0

Bin 3394: 341 of cap free
Amount of items: 2
Items: 
Size: 564008 Color: 1
Size: 435652 Color: 0

Bin 3395: 341 of cap free
Amount of items: 2
Items: 
Size: 671243 Color: 0
Size: 328417 Color: 1

Bin 3396: 341 of cap free
Amount of items: 2
Items: 
Size: 725175 Color: 0
Size: 274485 Color: 1

Bin 3397: 342 of cap free
Amount of items: 2
Items: 
Size: 671707 Color: 1
Size: 327952 Color: 0

Bin 3398: 343 of cap free
Amount of items: 2
Items: 
Size: 641115 Color: 1
Size: 358543 Color: 0

Bin 3399: 344 of cap free
Amount of items: 2
Items: 
Size: 742287 Color: 0
Size: 257370 Color: 1

Bin 3400: 345 of cap free
Amount of items: 2
Items: 
Size: 533929 Color: 0
Size: 465727 Color: 1

Bin 3401: 346 of cap free
Amount of items: 2
Items: 
Size: 525066 Color: 0
Size: 474589 Color: 1

Bin 3402: 346 of cap free
Amount of items: 2
Items: 
Size: 553587 Color: 0
Size: 446068 Color: 1

Bin 3403: 346 of cap free
Amount of items: 2
Items: 
Size: 730500 Color: 1
Size: 269155 Color: 0

Bin 3404: 347 of cap free
Amount of items: 2
Items: 
Size: 513616 Color: 0
Size: 486038 Color: 1

Bin 3405: 347 of cap free
Amount of items: 2
Items: 
Size: 554254 Color: 0
Size: 445400 Color: 1

Bin 3406: 348 of cap free
Amount of items: 2
Items: 
Size: 544005 Color: 0
Size: 455648 Color: 1

Bin 3407: 348 of cap free
Amount of items: 2
Items: 
Size: 605448 Color: 0
Size: 394205 Color: 1

Bin 3408: 348 of cap free
Amount of items: 2
Items: 
Size: 663689 Color: 1
Size: 335964 Color: 0

Bin 3409: 349 of cap free
Amount of items: 2
Items: 
Size: 746264 Color: 0
Size: 253388 Color: 1

Bin 3410: 349 of cap free
Amount of items: 2
Items: 
Size: 746982 Color: 0
Size: 252670 Color: 1

Bin 3411: 349 of cap free
Amount of items: 2
Items: 
Size: 763244 Color: 0
Size: 236408 Color: 1

Bin 3412: 350 of cap free
Amount of items: 2
Items: 
Size: 581194 Color: 0
Size: 418457 Color: 1

Bin 3413: 350 of cap free
Amount of items: 2
Items: 
Size: 597094 Color: 1
Size: 402557 Color: 0

Bin 3414: 350 of cap free
Amount of items: 2
Items: 
Size: 631381 Color: 0
Size: 368270 Color: 1

Bin 3415: 350 of cap free
Amount of items: 2
Items: 
Size: 652136 Color: 1
Size: 347515 Color: 0

Bin 3416: 352 of cap free
Amount of items: 2
Items: 
Size: 643191 Color: 1
Size: 356458 Color: 0

Bin 3417: 352 of cap free
Amount of items: 2
Items: 
Size: 592997 Color: 1
Size: 406652 Color: 0

Bin 3418: 353 of cap free
Amount of items: 2
Items: 
Size: 704238 Color: 1
Size: 295410 Color: 0

Bin 3419: 353 of cap free
Amount of items: 2
Items: 
Size: 760678 Color: 0
Size: 238970 Color: 1

Bin 3420: 354 of cap free
Amount of items: 2
Items: 
Size: 697235 Color: 1
Size: 302412 Color: 0

Bin 3421: 355 of cap free
Amount of items: 2
Items: 
Size: 559276 Color: 0
Size: 440370 Color: 1

Bin 3422: 355 of cap free
Amount of items: 2
Items: 
Size: 600413 Color: 0
Size: 399233 Color: 1

Bin 3423: 355 of cap free
Amount of items: 2
Items: 
Size: 734916 Color: 0
Size: 264730 Color: 1

Bin 3424: 356 of cap free
Amount of items: 2
Items: 
Size: 556408 Color: 1
Size: 443237 Color: 0

Bin 3425: 356 of cap free
Amount of items: 2
Items: 
Size: 586351 Color: 0
Size: 413294 Color: 1

Bin 3426: 356 of cap free
Amount of items: 2
Items: 
Size: 602983 Color: 0
Size: 396662 Color: 1

Bin 3427: 356 of cap free
Amount of items: 2
Items: 
Size: 714895 Color: 1
Size: 284750 Color: 0

Bin 3428: 356 of cap free
Amount of items: 2
Items: 
Size: 768708 Color: 0
Size: 230937 Color: 1

Bin 3429: 357 of cap free
Amount of items: 2
Items: 
Size: 534578 Color: 0
Size: 465066 Color: 1

Bin 3430: 357 of cap free
Amount of items: 2
Items: 
Size: 728081 Color: 0
Size: 271563 Color: 1

Bin 3431: 358 of cap free
Amount of items: 2
Items: 
Size: 546022 Color: 0
Size: 453621 Color: 1

Bin 3432: 359 of cap free
Amount of items: 2
Items: 
Size: 717174 Color: 0
Size: 282468 Color: 1

Bin 3433: 359 of cap free
Amount of items: 2
Items: 
Size: 508370 Color: 1
Size: 491272 Color: 0

Bin 3434: 361 of cap free
Amount of items: 2
Items: 
Size: 623612 Color: 0
Size: 376028 Color: 1

Bin 3435: 362 of cap free
Amount of items: 2
Items: 
Size: 683810 Color: 1
Size: 315829 Color: 0

Bin 3436: 363 of cap free
Amount of items: 2
Items: 
Size: 657615 Color: 0
Size: 342023 Color: 1

Bin 3437: 364 of cap free
Amount of items: 2
Items: 
Size: 522668 Color: 1
Size: 476969 Color: 0

Bin 3438: 364 of cap free
Amount of items: 2
Items: 
Size: 700697 Color: 0
Size: 298940 Color: 1

Bin 3439: 364 of cap free
Amount of items: 2
Items: 
Size: 709512 Color: 1
Size: 290125 Color: 0

Bin 3440: 367 of cap free
Amount of items: 2
Items: 
Size: 714184 Color: 1
Size: 285450 Color: 0

Bin 3441: 367 of cap free
Amount of items: 2
Items: 
Size: 524047 Color: 0
Size: 475587 Color: 1

Bin 3442: 368 of cap free
Amount of items: 2
Items: 
Size: 527408 Color: 0
Size: 472225 Color: 1

Bin 3443: 368 of cap free
Amount of items: 2
Items: 
Size: 611727 Color: 1
Size: 387906 Color: 0

Bin 3444: 369 of cap free
Amount of items: 2
Items: 
Size: 567844 Color: 0
Size: 431788 Color: 1

Bin 3445: 370 of cap free
Amount of items: 2
Items: 
Size: 537337 Color: 0
Size: 462294 Color: 1

Bin 3446: 370 of cap free
Amount of items: 2
Items: 
Size: 656802 Color: 0
Size: 342829 Color: 1

Bin 3447: 370 of cap free
Amount of items: 2
Items: 
Size: 772468 Color: 0
Size: 227163 Color: 1

Bin 3448: 370 of cap free
Amount of items: 2
Items: 
Size: 796098 Color: 1
Size: 203533 Color: 0

Bin 3449: 370 of cap free
Amount of items: 2
Items: 
Size: 561782 Color: 0
Size: 437849 Color: 1

Bin 3450: 371 of cap free
Amount of items: 2
Items: 
Size: 768219 Color: 0
Size: 231411 Color: 1

Bin 3451: 371 of cap free
Amount of items: 2
Items: 
Size: 776294 Color: 0
Size: 223336 Color: 1

Bin 3452: 372 of cap free
Amount of items: 2
Items: 
Size: 790946 Color: 1
Size: 208683 Color: 0

Bin 3453: 372 of cap free
Amount of items: 2
Items: 
Size: 554553 Color: 1
Size: 445076 Color: 0

Bin 3454: 372 of cap free
Amount of items: 2
Items: 
Size: 627196 Color: 1
Size: 372433 Color: 0

Bin 3455: 372 of cap free
Amount of items: 2
Items: 
Size: 647323 Color: 0
Size: 352306 Color: 1

Bin 3456: 374 of cap free
Amount of items: 2
Items: 
Size: 635711 Color: 1
Size: 363916 Color: 0

Bin 3457: 375 of cap free
Amount of items: 2
Items: 
Size: 570236 Color: 1
Size: 429390 Color: 0

Bin 3458: 376 of cap free
Amount of items: 2
Items: 
Size: 616700 Color: 1
Size: 382925 Color: 0

Bin 3459: 376 of cap free
Amount of items: 2
Items: 
Size: 707873 Color: 0
Size: 291752 Color: 1

Bin 3460: 376 of cap free
Amount of items: 2
Items: 
Size: 741323 Color: 1
Size: 258302 Color: 0

Bin 3461: 378 of cap free
Amount of items: 2
Items: 
Size: 547165 Color: 0
Size: 452458 Color: 1

Bin 3462: 378 of cap free
Amount of items: 2
Items: 
Size: 675417 Color: 0
Size: 324206 Color: 1

Bin 3463: 379 of cap free
Amount of items: 2
Items: 
Size: 596256 Color: 1
Size: 403366 Color: 0

Bin 3464: 379 of cap free
Amount of items: 2
Items: 
Size: 601596 Color: 1
Size: 398026 Color: 0

Bin 3465: 380 of cap free
Amount of items: 2
Items: 
Size: 788376 Color: 0
Size: 211245 Color: 1

Bin 3466: 381 of cap free
Amount of items: 2
Items: 
Size: 728446 Color: 0
Size: 271174 Color: 1

Bin 3467: 383 of cap free
Amount of items: 2
Items: 
Size: 667284 Color: 0
Size: 332334 Color: 1

Bin 3468: 384 of cap free
Amount of items: 2
Items: 
Size: 505265 Color: 1
Size: 494352 Color: 0

Bin 3469: 384 of cap free
Amount of items: 2
Items: 
Size: 711067 Color: 0
Size: 288550 Color: 1

Bin 3470: 385 of cap free
Amount of items: 2
Items: 
Size: 646551 Color: 0
Size: 353065 Color: 1

Bin 3471: 386 of cap free
Amount of items: 2
Items: 
Size: 500170 Color: 1
Size: 499445 Color: 0

Bin 3472: 386 of cap free
Amount of items: 2
Items: 
Size: 649615 Color: 0
Size: 350000 Color: 1

Bin 3473: 386 of cap free
Amount of items: 2
Items: 
Size: 725910 Color: 1
Size: 273705 Color: 0

Bin 3474: 386 of cap free
Amount of items: 2
Items: 
Size: 650052 Color: 1
Size: 349563 Color: 0

Bin 3475: 387 of cap free
Amount of items: 2
Items: 
Size: 591526 Color: 1
Size: 408088 Color: 0

Bin 3476: 388 of cap free
Amount of items: 2
Items: 
Size: 656035 Color: 1
Size: 343578 Color: 0

Bin 3477: 388 of cap free
Amount of items: 2
Items: 
Size: 700081 Color: 1
Size: 299532 Color: 0

Bin 3478: 389 of cap free
Amount of items: 2
Items: 
Size: 758992 Color: 0
Size: 240620 Color: 1

Bin 3479: 390 of cap free
Amount of items: 2
Items: 
Size: 588247 Color: 1
Size: 411364 Color: 0

Bin 3480: 390 of cap free
Amount of items: 2
Items: 
Size: 566578 Color: 0
Size: 433033 Color: 1

Bin 3481: 390 of cap free
Amount of items: 2
Items: 
Size: 755575 Color: 1
Size: 244036 Color: 0

Bin 3482: 391 of cap free
Amount of items: 2
Items: 
Size: 678293 Color: 1
Size: 321317 Color: 0

Bin 3483: 392 of cap free
Amount of items: 2
Items: 
Size: 703660 Color: 1
Size: 295949 Color: 0

Bin 3484: 395 of cap free
Amount of items: 2
Items: 
Size: 595322 Color: 1
Size: 404284 Color: 0

Bin 3485: 395 of cap free
Amount of items: 2
Items: 
Size: 658451 Color: 1
Size: 341155 Color: 0

Bin 3486: 395 of cap free
Amount of items: 2
Items: 
Size: 506613 Color: 1
Size: 492993 Color: 0

Bin 3487: 396 of cap free
Amount of items: 2
Items: 
Size: 521942 Color: 0
Size: 477663 Color: 1

Bin 3488: 396 of cap free
Amount of items: 2
Items: 
Size: 629750 Color: 1
Size: 369855 Color: 0

Bin 3489: 396 of cap free
Amount of items: 2
Items: 
Size: 676959 Color: 1
Size: 322646 Color: 0

Bin 3490: 396 of cap free
Amount of items: 2
Items: 
Size: 759914 Color: 0
Size: 239691 Color: 1

Bin 3491: 397 of cap free
Amount of items: 2
Items: 
Size: 754768 Color: 0
Size: 244836 Color: 1

Bin 3492: 397 of cap free
Amount of items: 2
Items: 
Size: 710078 Color: 1
Size: 289526 Color: 0

Bin 3493: 397 of cap free
Amount of items: 2
Items: 
Size: 775106 Color: 1
Size: 224498 Color: 0

Bin 3494: 397 of cap free
Amount of items: 2
Items: 
Size: 745863 Color: 1
Size: 253741 Color: 0

Bin 3495: 399 of cap free
Amount of items: 2
Items: 
Size: 517940 Color: 0
Size: 481662 Color: 1

Bin 3496: 399 of cap free
Amount of items: 2
Items: 
Size: 697219 Color: 1
Size: 302383 Color: 0

Bin 3497: 400 of cap free
Amount of items: 2
Items: 
Size: 567119 Color: 0
Size: 432482 Color: 1

Bin 3498: 401 of cap free
Amount of items: 2
Items: 
Size: 591455 Color: 0
Size: 408145 Color: 1

Bin 3499: 404 of cap free
Amount of items: 2
Items: 
Size: 556903 Color: 0
Size: 442694 Color: 1

Bin 3500: 404 of cap free
Amount of items: 2
Items: 
Size: 704662 Color: 0
Size: 294935 Color: 1

Bin 3501: 404 of cap free
Amount of items: 2
Items: 
Size: 789084 Color: 0
Size: 210513 Color: 1

Bin 3502: 405 of cap free
Amount of items: 2
Items: 
Size: 793587 Color: 0
Size: 206009 Color: 1

Bin 3503: 406 of cap free
Amount of items: 2
Items: 
Size: 780582 Color: 0
Size: 219013 Color: 1

Bin 3504: 406 of cap free
Amount of items: 2
Items: 
Size: 713351 Color: 0
Size: 286244 Color: 1

Bin 3505: 406 of cap free
Amount of items: 2
Items: 
Size: 796991 Color: 0
Size: 202604 Color: 1

Bin 3506: 407 of cap free
Amount of items: 2
Items: 
Size: 619051 Color: 0
Size: 380543 Color: 1

Bin 3507: 407 of cap free
Amount of items: 2
Items: 
Size: 734368 Color: 1
Size: 265226 Color: 0

Bin 3508: 408 of cap free
Amount of items: 2
Items: 
Size: 707851 Color: 0
Size: 291742 Color: 1

Bin 3509: 408 of cap free
Amount of items: 2
Items: 
Size: 568433 Color: 0
Size: 431160 Color: 1

Bin 3510: 409 of cap free
Amount of items: 2
Items: 
Size: 505488 Color: 0
Size: 494104 Color: 1

Bin 3511: 411 of cap free
Amount of items: 2
Items: 
Size: 785502 Color: 0
Size: 214088 Color: 1

Bin 3512: 415 of cap free
Amount of items: 2
Items: 
Size: 525436 Color: 0
Size: 474150 Color: 1

Bin 3513: 416 of cap free
Amount of items: 2
Items: 
Size: 686761 Color: 0
Size: 312824 Color: 1

Bin 3514: 417 of cap free
Amount of items: 2
Items: 
Size: 664936 Color: 0
Size: 334648 Color: 1

Bin 3515: 419 of cap free
Amount of items: 2
Items: 
Size: 677102 Color: 0
Size: 322480 Color: 1

Bin 3516: 419 of cap free
Amount of items: 2
Items: 
Size: 629821 Color: 0
Size: 369761 Color: 1

Bin 3517: 421 of cap free
Amount of items: 2
Items: 
Size: 522630 Color: 0
Size: 476950 Color: 1

Bin 3518: 421 of cap free
Amount of items: 2
Items: 
Size: 654619 Color: 0
Size: 344961 Color: 1

Bin 3519: 422 of cap free
Amount of items: 2
Items: 
Size: 787675 Color: 0
Size: 211904 Color: 1

Bin 3520: 422 of cap free
Amount of items: 2
Items: 
Size: 508747 Color: 1
Size: 490832 Color: 0

Bin 3521: 422 of cap free
Amount of items: 2
Items: 
Size: 584019 Color: 1
Size: 415560 Color: 0

Bin 3522: 422 of cap free
Amount of items: 2
Items: 
Size: 701762 Color: 0
Size: 297817 Color: 1

Bin 3523: 425 of cap free
Amount of items: 2
Items: 
Size: 618536 Color: 1
Size: 381040 Color: 0

Bin 3524: 425 of cap free
Amount of items: 2
Items: 
Size: 662345 Color: 1
Size: 337231 Color: 0

Bin 3525: 426 of cap free
Amount of items: 2
Items: 
Size: 770013 Color: 0
Size: 229562 Color: 1

Bin 3526: 427 of cap free
Amount of items: 2
Items: 
Size: 720499 Color: 0
Size: 279075 Color: 1

Bin 3527: 428 of cap free
Amount of items: 2
Items: 
Size: 516075 Color: 0
Size: 483498 Color: 1

Bin 3528: 428 of cap free
Amount of items: 2
Items: 
Size: 542322 Color: 1
Size: 457251 Color: 0

Bin 3529: 428 of cap free
Amount of items: 2
Items: 
Size: 547143 Color: 0
Size: 452430 Color: 1

Bin 3530: 429 of cap free
Amount of items: 2
Items: 
Size: 650041 Color: 0
Size: 349531 Color: 1

Bin 3531: 429 of cap free
Amount of items: 2
Items: 
Size: 714175 Color: 1
Size: 285397 Color: 0

Bin 3532: 430 of cap free
Amount of items: 2
Items: 
Size: 646031 Color: 1
Size: 353540 Color: 0

Bin 3533: 430 of cap free
Amount of items: 2
Items: 
Size: 782251 Color: 1
Size: 217320 Color: 0

Bin 3534: 432 of cap free
Amount of items: 2
Items: 
Size: 685674 Color: 1
Size: 313895 Color: 0

Bin 3535: 432 of cap free
Amount of items: 2
Items: 
Size: 760671 Color: 0
Size: 238898 Color: 1

Bin 3536: 433 of cap free
Amount of items: 2
Items: 
Size: 591426 Color: 0
Size: 408142 Color: 1

Bin 3537: 434 of cap free
Amount of items: 2
Items: 
Size: 711885 Color: 0
Size: 287682 Color: 1

Bin 3538: 435 of cap free
Amount of items: 2
Items: 
Size: 723483 Color: 1
Size: 276083 Color: 0

Bin 3539: 435 of cap free
Amount of items: 2
Items: 
Size: 617238 Color: 1
Size: 382328 Color: 0

Bin 3540: 436 of cap free
Amount of items: 2
Items: 
Size: 709729 Color: 0
Size: 289836 Color: 1

Bin 3541: 436 of cap free
Amount of items: 2
Items: 
Size: 589376 Color: 0
Size: 410189 Color: 1

Bin 3542: 436 of cap free
Amount of items: 2
Items: 
Size: 679498 Color: 1
Size: 320067 Color: 0

Bin 3543: 437 of cap free
Amount of items: 2
Items: 
Size: 609333 Color: 0
Size: 390231 Color: 1

Bin 3544: 437 of cap free
Amount of items: 2
Items: 
Size: 787486 Color: 1
Size: 212078 Color: 0

Bin 3545: 437 of cap free
Amount of items: 2
Items: 
Size: 740419 Color: 1
Size: 259145 Color: 0

Bin 3546: 438 of cap free
Amount of items: 2
Items: 
Size: 536581 Color: 0
Size: 462982 Color: 1

Bin 3547: 438 of cap free
Amount of items: 2
Items: 
Size: 750110 Color: 0
Size: 249453 Color: 1

Bin 3548: 439 of cap free
Amount of items: 2
Items: 
Size: 727164 Color: 1
Size: 272398 Color: 0

Bin 3549: 440 of cap free
Amount of items: 2
Items: 
Size: 541579 Color: 0
Size: 457982 Color: 1

Bin 3550: 440 of cap free
Amount of items: 2
Items: 
Size: 705747 Color: 1
Size: 293814 Color: 0

Bin 3551: 440 of cap free
Amount of items: 2
Items: 
Size: 598360 Color: 1
Size: 401201 Color: 0

Bin 3552: 444 of cap free
Amount of items: 2
Items: 
Size: 771004 Color: 1
Size: 228553 Color: 0

Bin 3553: 444 of cap free
Amount of items: 2
Items: 
Size: 641710 Color: 0
Size: 357847 Color: 1

Bin 3554: 444 of cap free
Amount of items: 2
Items: 
Size: 765453 Color: 0
Size: 234104 Color: 1

Bin 3555: 445 of cap free
Amount of items: 2
Items: 
Size: 652805 Color: 0
Size: 346751 Color: 1

Bin 3556: 446 of cap free
Amount of items: 2
Items: 
Size: 529391 Color: 0
Size: 470164 Color: 1

Bin 3557: 446 of cap free
Amount of items: 2
Items: 
Size: 633887 Color: 1
Size: 365668 Color: 0

Bin 3558: 447 of cap free
Amount of items: 2
Items: 
Size: 534394 Color: 1
Size: 465160 Color: 0

Bin 3559: 448 of cap free
Amount of items: 2
Items: 
Size: 615862 Color: 0
Size: 383691 Color: 1

Bin 3560: 448 of cap free
Amount of items: 2
Items: 
Size: 666093 Color: 1
Size: 333460 Color: 0

Bin 3561: 449 of cap free
Amount of items: 2
Items: 
Size: 711883 Color: 0
Size: 287669 Color: 1

Bin 3562: 449 of cap free
Amount of items: 2
Items: 
Size: 537329 Color: 0
Size: 462223 Color: 1

Bin 3563: 449 of cap free
Amount of items: 2
Items: 
Size: 544241 Color: 1
Size: 455311 Color: 0

Bin 3564: 451 of cap free
Amount of items: 2
Items: 
Size: 732381 Color: 0
Size: 267169 Color: 1

Bin 3565: 451 of cap free
Amount of items: 2
Items: 
Size: 568029 Color: 1
Size: 431521 Color: 0

Bin 3566: 451 of cap free
Amount of items: 2
Items: 
Size: 565505 Color: 1
Size: 434045 Color: 0

Bin 3567: 452 of cap free
Amount of items: 2
Items: 
Size: 756656 Color: 0
Size: 242893 Color: 1

Bin 3568: 454 of cap free
Amount of items: 2
Items: 
Size: 616688 Color: 1
Size: 382859 Color: 0

Bin 3569: 455 of cap free
Amount of items: 2
Items: 
Size: 500939 Color: 0
Size: 498607 Color: 1

Bin 3570: 455 of cap free
Amount of items: 2
Items: 
Size: 647765 Color: 1
Size: 351781 Color: 0

Bin 3571: 455 of cap free
Amount of items: 2
Items: 
Size: 733013 Color: 1
Size: 266533 Color: 0

Bin 3572: 456 of cap free
Amount of items: 2
Items: 
Size: 789866 Color: 0
Size: 209679 Color: 1

Bin 3573: 456 of cap free
Amount of items: 2
Items: 
Size: 553126 Color: 0
Size: 446419 Color: 1

Bin 3574: 456 of cap free
Amount of items: 2
Items: 
Size: 755511 Color: 0
Size: 244034 Color: 1

Bin 3575: 457 of cap free
Amount of items: 2
Items: 
Size: 584534 Color: 1
Size: 415010 Color: 0

Bin 3576: 457 of cap free
Amount of items: 2
Items: 
Size: 589855 Color: 1
Size: 409689 Color: 0

Bin 3577: 458 of cap free
Amount of items: 2
Items: 
Size: 543015 Color: 1
Size: 456528 Color: 0

Bin 3578: 459 of cap free
Amount of items: 2
Items: 
Size: 636234 Color: 1
Size: 363308 Color: 0

Bin 3579: 459 of cap free
Amount of items: 2
Items: 
Size: 778834 Color: 1
Size: 220708 Color: 0

Bin 3580: 460 of cap free
Amount of items: 2
Items: 
Size: 790389 Color: 1
Size: 209152 Color: 0

Bin 3581: 460 of cap free
Amount of items: 2
Items: 
Size: 508529 Color: 0
Size: 491012 Color: 1

Bin 3582: 461 of cap free
Amount of items: 2
Items: 
Size: 781458 Color: 1
Size: 218082 Color: 0

Bin 3583: 464 of cap free
Amount of items: 2
Items: 
Size: 647761 Color: 0
Size: 351776 Color: 1

Bin 3584: 465 of cap free
Amount of items: 2
Items: 
Size: 538020 Color: 1
Size: 461516 Color: 0

Bin 3585: 465 of cap free
Amount of items: 2
Items: 
Size: 597053 Color: 1
Size: 402483 Color: 0

Bin 3586: 465 of cap free
Amount of items: 2
Items: 
Size: 698061 Color: 1
Size: 301475 Color: 0

Bin 3587: 465 of cap free
Amount of items: 2
Items: 
Size: 658518 Color: 0
Size: 341018 Color: 1

Bin 3588: 466 of cap free
Amount of items: 2
Items: 
Size: 799750 Color: 0
Size: 199785 Color: 1

Bin 3589: 467 of cap free
Amount of items: 2
Items: 
Size: 636587 Color: 0
Size: 362947 Color: 1

Bin 3590: 469 of cap free
Amount of items: 2
Items: 
Size: 544591 Color: 0
Size: 454941 Color: 1

Bin 3591: 470 of cap free
Amount of items: 2
Items: 
Size: 653969 Color: 1
Size: 345562 Color: 0

Bin 3592: 472 of cap free
Amount of items: 2
Items: 
Size: 509934 Color: 0
Size: 489595 Color: 1

Bin 3593: 473 of cap free
Amount of items: 2
Items: 
Size: 702336 Color: 1
Size: 297192 Color: 0

Bin 3594: 473 of cap free
Amount of items: 2
Items: 
Size: 725555 Color: 0
Size: 273973 Color: 1

Bin 3595: 474 of cap free
Amount of items: 2
Items: 
Size: 754529 Color: 1
Size: 244998 Color: 0

Bin 3596: 475 of cap free
Amount of items: 2
Items: 
Size: 655965 Color: 1
Size: 343561 Color: 0

Bin 3597: 477 of cap free
Amount of items: 2
Items: 
Size: 710249 Color: 0
Size: 289275 Color: 1

Bin 3598: 477 of cap free
Amount of items: 2
Items: 
Size: 784432 Color: 0
Size: 215092 Color: 1

Bin 3599: 478 of cap free
Amount of items: 2
Items: 
Size: 603438 Color: 0
Size: 396085 Color: 1

Bin 3600: 479 of cap free
Amount of items: 2
Items: 
Size: 771846 Color: 1
Size: 227676 Color: 0

Bin 3601: 480 of cap free
Amount of items: 2
Items: 
Size: 559826 Color: 1
Size: 439695 Color: 0

Bin 3602: 480 of cap free
Amount of items: 2
Items: 
Size: 612535 Color: 1
Size: 386986 Color: 0

Bin 3603: 481 of cap free
Amount of items: 2
Items: 
Size: 503979 Color: 1
Size: 495541 Color: 0

Bin 3604: 481 of cap free
Amount of items: 2
Items: 
Size: 604770 Color: 0
Size: 394750 Color: 1

Bin 3605: 481 of cap free
Amount of items: 2
Items: 
Size: 721598 Color: 1
Size: 277922 Color: 0

Bin 3606: 484 of cap free
Amount of items: 2
Items: 
Size: 504449 Color: 0
Size: 495068 Color: 1

Bin 3607: 484 of cap free
Amount of items: 2
Items: 
Size: 518080 Color: 1
Size: 481437 Color: 0

Bin 3608: 488 of cap free
Amount of items: 2
Items: 
Size: 749507 Color: 1
Size: 250006 Color: 0

Bin 3609: 489 of cap free
Amount of items: 2
Items: 
Size: 792007 Color: 0
Size: 207505 Color: 1

Bin 3610: 489 of cap free
Amount of items: 2
Items: 
Size: 798860 Color: 0
Size: 200652 Color: 1

Bin 3611: 489 of cap free
Amount of items: 2
Items: 
Size: 559137 Color: 1
Size: 440375 Color: 0

Bin 3612: 490 of cap free
Amount of items: 2
Items: 
Size: 546483 Color: 1
Size: 453028 Color: 0

Bin 3613: 490 of cap free
Amount of items: 2
Items: 
Size: 750087 Color: 0
Size: 249424 Color: 1

Bin 3614: 491 of cap free
Amount of items: 2
Items: 
Size: 754729 Color: 0
Size: 244781 Color: 1

Bin 3615: 492 of cap free
Amount of items: 2
Items: 
Size: 556357 Color: 1
Size: 443152 Color: 0

Bin 3616: 492 of cap free
Amount of items: 2
Items: 
Size: 683047 Color: 1
Size: 316462 Color: 0

Bin 3617: 494 of cap free
Amount of items: 2
Items: 
Size: 620368 Color: 0
Size: 379139 Color: 1

Bin 3618: 495 of cap free
Amount of items: 2
Items: 
Size: 629200 Color: 1
Size: 370306 Color: 0

Bin 3619: 495 of cap free
Amount of items: 2
Items: 
Size: 689163 Color: 1
Size: 310343 Color: 0

Bin 3620: 495 of cap free
Amount of items: 2
Items: 
Size: 708776 Color: 0
Size: 290730 Color: 1

Bin 3621: 496 of cap free
Amount of items: 2
Items: 
Size: 677782 Color: 1
Size: 321723 Color: 0

Bin 3622: 497 of cap free
Amount of items: 2
Items: 
Size: 561690 Color: 0
Size: 437814 Color: 1

Bin 3623: 497 of cap free
Amount of items: 2
Items: 
Size: 539713 Color: 1
Size: 459791 Color: 0

Bin 3624: 497 of cap free
Amount of items: 2
Items: 
Size: 597045 Color: 1
Size: 402459 Color: 0

Bin 3625: 498 of cap free
Amount of items: 2
Items: 
Size: 712698 Color: 1
Size: 286805 Color: 0

Bin 3626: 499 of cap free
Amount of items: 2
Items: 
Size: 568028 Color: 1
Size: 431474 Color: 0

Bin 3627: 500 of cap free
Amount of items: 2
Items: 
Size: 599587 Color: 0
Size: 399914 Color: 1

Bin 3628: 500 of cap free
Amount of items: 2
Items: 
Size: 719715 Color: 0
Size: 279786 Color: 1

Bin 3629: 503 of cap free
Amount of items: 2
Items: 
Size: 769233 Color: 1
Size: 230265 Color: 0

Bin 3630: 503 of cap free
Amount of items: 2
Items: 
Size: 752380 Color: 0
Size: 247118 Color: 1

Bin 3631: 505 of cap free
Amount of items: 2
Items: 
Size: 751820 Color: 1
Size: 247676 Color: 0

Bin 3632: 506 of cap free
Amount of items: 2
Items: 
Size: 522576 Color: 1
Size: 476919 Color: 0

Bin 3633: 506 of cap free
Amount of items: 2
Items: 
Size: 537326 Color: 0
Size: 462169 Color: 1

Bin 3634: 506 of cap free
Amount of items: 2
Items: 
Size: 798595 Color: 1
Size: 200900 Color: 0

Bin 3635: 507 of cap free
Amount of items: 2
Items: 
Size: 500075 Color: 0
Size: 499419 Color: 1

Bin 3636: 508 of cap free
Amount of items: 2
Items: 
Size: 571707 Color: 0
Size: 427786 Color: 1

Bin 3637: 508 of cap free
Amount of items: 2
Items: 
Size: 618323 Color: 0
Size: 381170 Color: 1

Bin 3638: 508 of cap free
Amount of items: 2
Items: 
Size: 764187 Color: 0
Size: 235306 Color: 1

Bin 3639: 510 of cap free
Amount of items: 2
Items: 
Size: 558227 Color: 1
Size: 441264 Color: 0

Bin 3640: 512 of cap free
Amount of items: 2
Items: 
Size: 604376 Color: 1
Size: 395113 Color: 0

Bin 3641: 516 of cap free
Amount of items: 2
Items: 
Size: 636192 Color: 1
Size: 363293 Color: 0

Bin 3642: 518 of cap free
Amount of items: 2
Items: 
Size: 662172 Color: 0
Size: 337311 Color: 1

Bin 3643: 519 of cap free
Amount of items: 2
Items: 
Size: 675321 Color: 0
Size: 324161 Color: 1

Bin 3644: 519 of cap free
Amount of items: 2
Items: 
Size: 735669 Color: 1
Size: 263813 Color: 0

Bin 3645: 520 of cap free
Amount of items: 2
Items: 
Size: 708461 Color: 1
Size: 291020 Color: 0

Bin 3646: 521 of cap free
Amount of items: 2
Items: 
Size: 686250 Color: 1
Size: 313230 Color: 0

Bin 3647: 521 of cap free
Amount of items: 2
Items: 
Size: 690751 Color: 1
Size: 308729 Color: 0

Bin 3648: 521 of cap free
Amount of items: 2
Items: 
Size: 508476 Color: 0
Size: 491004 Color: 1

Bin 3649: 523 of cap free
Amount of items: 2
Items: 
Size: 565468 Color: 1
Size: 434010 Color: 0

Bin 3650: 527 of cap free
Amount of items: 2
Items: 
Size: 775086 Color: 0
Size: 224388 Color: 1

Bin 3651: 527 of cap free
Amount of items: 2
Items: 
Size: 770981 Color: 1
Size: 228493 Color: 0

Bin 3652: 527 of cap free
Amount of items: 2
Items: 
Size: 545355 Color: 0
Size: 454119 Color: 1

Bin 3653: 527 of cap free
Amount of items: 2
Items: 
Size: 581667 Color: 0
Size: 417807 Color: 1

Bin 3654: 528 of cap free
Amount of items: 2
Items: 
Size: 638968 Color: 0
Size: 360505 Color: 1

Bin 3655: 528 of cap free
Amount of items: 2
Items: 
Size: 625876 Color: 1
Size: 373597 Color: 0

Bin 3656: 529 of cap free
Amount of items: 2
Items: 
Size: 641064 Color: 0
Size: 358408 Color: 1

Bin 3657: 531 of cap free
Amount of items: 2
Items: 
Size: 579337 Color: 1
Size: 420133 Color: 0

Bin 3658: 532 of cap free
Amount of items: 2
Items: 
Size: 783492 Color: 1
Size: 215977 Color: 0

Bin 3659: 532 of cap free
Amount of items: 2
Items: 
Size: 566455 Color: 0
Size: 433014 Color: 1

Bin 3660: 535 of cap free
Amount of items: 2
Items: 
Size: 514315 Color: 1
Size: 485151 Color: 0

Bin 3661: 535 of cap free
Amount of items: 2
Items: 
Size: 530150 Color: 1
Size: 469316 Color: 0

Bin 3662: 537 of cap free
Amount of items: 2
Items: 
Size: 662234 Color: 1
Size: 337230 Color: 0

Bin 3663: 538 of cap free
Amount of items: 2
Items: 
Size: 605297 Color: 0
Size: 394166 Color: 1

Bin 3664: 538 of cap free
Amount of items: 2
Items: 
Size: 681980 Color: 1
Size: 317483 Color: 0

Bin 3665: 540 of cap free
Amount of items: 2
Items: 
Size: 711619 Color: 1
Size: 287842 Color: 0

Bin 3666: 541 of cap free
Amount of items: 2
Items: 
Size: 535142 Color: 0
Size: 464318 Color: 1

Bin 3667: 543 of cap free
Amount of items: 2
Items: 
Size: 646649 Color: 1
Size: 352809 Color: 0

Bin 3668: 544 of cap free
Amount of items: 2
Items: 
Size: 692645 Color: 0
Size: 306812 Color: 1

Bin 3669: 544 of cap free
Amount of items: 2
Items: 
Size: 717076 Color: 1
Size: 282381 Color: 0

Bin 3670: 544 of cap free
Amount of items: 2
Items: 
Size: 718398 Color: 1
Size: 281059 Color: 0

Bin 3671: 544 of cap free
Amount of items: 2
Items: 
Size: 754687 Color: 0
Size: 244770 Color: 1

Bin 3672: 545 of cap free
Amount of items: 2
Items: 
Size: 785307 Color: 1
Size: 214149 Color: 0

Bin 3673: 546 of cap free
Amount of items: 2
Items: 
Size: 796092 Color: 1
Size: 203363 Color: 0

Bin 3674: 547 of cap free
Amount of items: 2
Items: 
Size: 678297 Color: 0
Size: 321157 Color: 1

Bin 3675: 548 of cap free
Amount of items: 2
Items: 
Size: 765369 Color: 0
Size: 234084 Color: 1

Bin 3676: 550 of cap free
Amount of items: 2
Items: 
Size: 622851 Color: 1
Size: 376600 Color: 0

Bin 3677: 551 of cap free
Amount of items: 2
Items: 
Size: 677071 Color: 0
Size: 322379 Color: 1

Bin 3678: 551 of cap free
Amount of items: 2
Items: 
Size: 666035 Color: 1
Size: 333415 Color: 0

Bin 3679: 551 of cap free
Amount of items: 2
Items: 
Size: 696168 Color: 0
Size: 303282 Color: 1

Bin 3680: 553 of cap free
Amount of items: 2
Items: 
Size: 561995 Color: 1
Size: 437453 Color: 0

Bin 3681: 555 of cap free
Amount of items: 2
Items: 
Size: 521860 Color: 0
Size: 477586 Color: 1

Bin 3682: 556 of cap free
Amount of items: 2
Items: 
Size: 554547 Color: 1
Size: 444898 Color: 0

Bin 3683: 560 of cap free
Amount of items: 2
Items: 
Size: 643056 Color: 0
Size: 356385 Color: 1

Bin 3684: 562 of cap free
Amount of items: 2
Items: 
Size: 585172 Color: 1
Size: 414267 Color: 0

Bin 3685: 564 of cap free
Amount of items: 2
Items: 
Size: 738580 Color: 0
Size: 260857 Color: 1

Bin 3686: 565 of cap free
Amount of items: 2
Items: 
Size: 746783 Color: 0
Size: 252653 Color: 1

Bin 3687: 567 of cap free
Amount of items: 2
Items: 
Size: 615815 Color: 0
Size: 383619 Color: 1

Bin 3688: 567 of cap free
Amount of items: 2
Items: 
Size: 554539 Color: 1
Size: 444895 Color: 0

Bin 3689: 568 of cap free
Amount of items: 2
Items: 
Size: 540426 Color: 1
Size: 459007 Color: 0

Bin 3690: 569 of cap free
Amount of items: 2
Items: 
Size: 584114 Color: 0
Size: 415318 Color: 1

Bin 3691: 570 of cap free
Amount of items: 2
Items: 
Size: 509639 Color: 1
Size: 489792 Color: 0

Bin 3692: 570 of cap free
Amount of items: 2
Items: 
Size: 596170 Color: 1
Size: 403261 Color: 0

Bin 3693: 570 of cap free
Amount of items: 2
Items: 
Size: 772447 Color: 0
Size: 226984 Color: 1

Bin 3694: 573 of cap free
Amount of items: 2
Items: 
Size: 721590 Color: 1
Size: 277838 Color: 0

Bin 3695: 574 of cap free
Amount of items: 2
Items: 
Size: 546424 Color: 1
Size: 453003 Color: 0

Bin 3696: 575 of cap free
Amount of items: 2
Items: 
Size: 655119 Color: 1
Size: 344307 Color: 0

Bin 3697: 576 of cap free
Amount of items: 2
Items: 
Size: 771810 Color: 1
Size: 227615 Color: 0

Bin 3698: 577 of cap free
Amount of items: 2
Items: 
Size: 632498 Color: 1
Size: 366926 Color: 0

Bin 3699: 577 of cap free
Amount of items: 2
Items: 
Size: 790504 Color: 0
Size: 208920 Color: 1

Bin 3700: 580 of cap free
Amount of items: 2
Items: 
Size: 593498 Color: 0
Size: 405923 Color: 1

Bin 3701: 581 of cap free
Amount of items: 2
Items: 
Size: 654566 Color: 0
Size: 344854 Color: 1

Bin 3702: 583 of cap free
Amount of items: 2
Items: 
Size: 734362 Color: 1
Size: 265056 Color: 0

Bin 3703: 584 of cap free
Amount of items: 2
Items: 
Size: 574744 Color: 1
Size: 424673 Color: 0

Bin 3704: 584 of cap free
Amount of items: 2
Items: 
Size: 755462 Color: 0
Size: 243955 Color: 1

Bin 3705: 584 of cap free
Amount of items: 2
Items: 
Size: 689957 Color: 0
Size: 309460 Color: 1

Bin 3706: 587 of cap free
Amount of items: 2
Items: 
Size: 545303 Color: 0
Size: 454111 Color: 1

Bin 3707: 589 of cap free
Amount of items: 2
Items: 
Size: 598289 Color: 1
Size: 401123 Color: 0

Bin 3708: 589 of cap free
Amount of items: 2
Items: 
Size: 663633 Color: 1
Size: 335779 Color: 0

Bin 3709: 589 of cap free
Amount of items: 2
Items: 
Size: 665636 Color: 0
Size: 333776 Color: 1

Bin 3710: 590 of cap free
Amount of items: 2
Items: 
Size: 796922 Color: 0
Size: 202489 Color: 1

Bin 3711: 590 of cap free
Amount of items: 2
Items: 
Size: 600346 Color: 0
Size: 399065 Color: 1

Bin 3712: 590 of cap free
Amount of items: 2
Items: 
Size: 610299 Color: 1
Size: 389112 Color: 0

Bin 3713: 590 of cap free
Amount of items: 2
Items: 
Size: 714107 Color: 1
Size: 285304 Color: 0

Bin 3714: 590 of cap free
Amount of items: 2
Items: 
Size: 561651 Color: 0
Size: 437760 Color: 1

Bin 3715: 592 of cap free
Amount of items: 2
Items: 
Size: 571672 Color: 0
Size: 427737 Color: 1

Bin 3716: 594 of cap free
Amount of items: 2
Items: 
Size: 699762 Color: 0
Size: 299645 Color: 1

Bin 3717: 595 of cap free
Amount of items: 2
Items: 
Size: 557499 Color: 0
Size: 441907 Color: 1

Bin 3718: 596 of cap free
Amount of items: 2
Items: 
Size: 577983 Color: 0
Size: 421422 Color: 1

Bin 3719: 599 of cap free
Amount of items: 2
Items: 
Size: 625866 Color: 1
Size: 373536 Color: 0

Bin 3720: 600 of cap free
Amount of items: 2
Items: 
Size: 666003 Color: 1
Size: 333398 Color: 0

Bin 3721: 600 of cap free
Amount of items: 2
Items: 
Size: 499977 Color: 1
Size: 499424 Color: 0

Bin 3722: 603 of cap free
Amount of items: 2
Items: 
Size: 701726 Color: 0
Size: 297672 Color: 1

Bin 3723: 604 of cap free
Amount of items: 2
Items: 
Size: 636146 Color: 1
Size: 363251 Color: 0

Bin 3724: 609 of cap free
Amount of items: 2
Items: 
Size: 698541 Color: 1
Size: 300851 Color: 0

Bin 3725: 610 of cap free
Amount of items: 2
Items: 
Size: 526045 Color: 1
Size: 473346 Color: 0

Bin 3726: 614 of cap free
Amount of items: 2
Items: 
Size: 579297 Color: 0
Size: 420090 Color: 1

Bin 3727: 616 of cap free
Amount of items: 2
Items: 
Size: 558136 Color: 1
Size: 441249 Color: 0

Bin 3728: 617 of cap free
Amount of items: 2
Items: 
Size: 747349 Color: 1
Size: 252035 Color: 0

Bin 3729: 617 of cap free
Amount of items: 2
Items: 
Size: 638326 Color: 1
Size: 361058 Color: 0

Bin 3730: 617 of cap free
Amount of items: 2
Items: 
Size: 794980 Color: 1
Size: 204404 Color: 0

Bin 3731: 619 of cap free
Amount of items: 2
Items: 
Size: 651207 Color: 1
Size: 348175 Color: 0

Bin 3732: 622 of cap free
Amount of items: 2
Items: 
Size: 696138 Color: 0
Size: 303241 Color: 1

Bin 3733: 622 of cap free
Amount of items: 6
Items: 
Size: 184363 Color: 0
Size: 165100 Color: 0
Size: 164204 Color: 1
Size: 163360 Color: 1
Size: 163322 Color: 1
Size: 159030 Color: 0

Bin 3734: 624 of cap free
Amount of items: 2
Items: 
Size: 582564 Color: 1
Size: 416813 Color: 0

Bin 3735: 625 of cap free
Amount of items: 2
Items: 
Size: 793464 Color: 1
Size: 205912 Color: 0

Bin 3736: 627 of cap free
Amount of items: 2
Items: 
Size: 752911 Color: 0
Size: 246463 Color: 1

Bin 3737: 628 of cap free
Amount of items: 2
Items: 
Size: 795269 Color: 0
Size: 204104 Color: 1

Bin 3738: 629 of cap free
Amount of items: 2
Items: 
Size: 596572 Color: 0
Size: 402800 Color: 1

Bin 3739: 629 of cap free
Amount of items: 2
Items: 
Size: 773919 Color: 1
Size: 225453 Color: 0

Bin 3740: 632 of cap free
Amount of items: 2
Items: 
Size: 515947 Color: 0
Size: 483422 Color: 1

Bin 3741: 633 of cap free
Amount of items: 2
Items: 
Size: 571597 Color: 1
Size: 427771 Color: 0

Bin 3742: 634 of cap free
Amount of items: 2
Items: 
Size: 778771 Color: 1
Size: 220596 Color: 0

Bin 3743: 635 of cap free
Amount of items: 2
Items: 
Size: 506116 Color: 0
Size: 493250 Color: 1

Bin 3744: 635 of cap free
Amount of items: 2
Items: 
Size: 675092 Color: 1
Size: 324274 Color: 0

Bin 3745: 636 of cap free
Amount of items: 2
Items: 
Size: 548735 Color: 1
Size: 450630 Color: 0

Bin 3746: 636 of cap free
Amount of items: 2
Items: 
Size: 784284 Color: 0
Size: 215081 Color: 1

Bin 3747: 636 of cap free
Amount of items: 2
Items: 
Size: 641679 Color: 0
Size: 357686 Color: 1

Bin 3748: 637 of cap free
Amount of items: 2
Items: 
Size: 589772 Color: 1
Size: 409592 Color: 0

Bin 3749: 644 of cap free
Amount of items: 2
Items: 
Size: 556699 Color: 0
Size: 442658 Color: 1

Bin 3750: 644 of cap free
Amount of items: 2
Items: 
Size: 608150 Color: 0
Size: 391207 Color: 1

Bin 3751: 646 of cap free
Amount of items: 2
Items: 
Size: 618221 Color: 0
Size: 381134 Color: 1

Bin 3752: 646 of cap free
Amount of items: 2
Items: 
Size: 747724 Color: 0
Size: 251631 Color: 1

Bin 3753: 650 of cap free
Amount of items: 2
Items: 
Size: 577869 Color: 1
Size: 421482 Color: 0

Bin 3754: 655 of cap free
Amount of items: 2
Items: 
Size: 777789 Color: 0
Size: 221557 Color: 1

Bin 3755: 656 of cap free
Amount of items: 2
Items: 
Size: 739669 Color: 1
Size: 259676 Color: 0

Bin 3756: 660 of cap free
Amount of items: 2
Items: 
Size: 748344 Color: 1
Size: 250997 Color: 0

Bin 3757: 662 of cap free
Amount of items: 2
Items: 
Size: 628323 Color: 0
Size: 371016 Color: 1

Bin 3758: 664 of cap free
Amount of items: 2
Items: 
Size: 555441 Color: 0
Size: 443896 Color: 1

Bin 3759: 664 of cap free
Amount of items: 2
Items: 
Size: 691643 Color: 1
Size: 307694 Color: 0

Bin 3760: 665 of cap free
Amount of items: 2
Items: 
Size: 659403 Color: 1
Size: 339933 Color: 0

Bin 3761: 666 of cap free
Amount of items: 2
Items: 
Size: 701035 Color: 1
Size: 298300 Color: 0

Bin 3762: 667 of cap free
Amount of items: 2
Items: 
Size: 662089 Color: 0
Size: 337245 Color: 1

Bin 3763: 669 of cap free
Amount of items: 2
Items: 
Size: 517931 Color: 1
Size: 481401 Color: 0

Bin 3764: 669 of cap free
Amount of items: 2
Items: 
Size: 567987 Color: 1
Size: 431345 Color: 0

Bin 3765: 671 of cap free
Amount of items: 2
Items: 
Size: 715992 Color: 0
Size: 283338 Color: 1

Bin 3766: 672 of cap free
Amount of items: 2
Items: 
Size: 717159 Color: 0
Size: 282170 Color: 1

Bin 3767: 673 of cap free
Amount of items: 2
Items: 
Size: 796090 Color: 1
Size: 203238 Color: 0

Bin 3768: 673 of cap free
Amount of items: 2
Items: 
Size: 534359 Color: 1
Size: 464969 Color: 0

Bin 3769: 673 of cap free
Amount of items: 2
Items: 
Size: 738555 Color: 0
Size: 260773 Color: 1

Bin 3770: 676 of cap free
Amount of items: 2
Items: 
Size: 582561 Color: 0
Size: 416764 Color: 1

Bin 3771: 676 of cap free
Amount of items: 2
Items: 
Size: 749501 Color: 1
Size: 249824 Color: 0

Bin 3772: 676 of cap free
Amount of items: 2
Items: 
Size: 767648 Color: 1
Size: 231677 Color: 0

Bin 3773: 677 of cap free
Amount of items: 2
Items: 
Size: 656776 Color: 0
Size: 342548 Color: 1

Bin 3774: 677 of cap free
Amount of items: 2
Items: 
Size: 556685 Color: 0
Size: 442639 Color: 1

Bin 3775: 679 of cap free
Amount of items: 2
Items: 
Size: 714752 Color: 1
Size: 284570 Color: 0

Bin 3776: 680 of cap free
Amount of items: 2
Items: 
Size: 633783 Color: 1
Size: 365538 Color: 0

Bin 3777: 682 of cap free
Amount of items: 2
Items: 
Size: 599414 Color: 0
Size: 399905 Color: 1

Bin 3778: 684 of cap free
Amount of items: 2
Items: 
Size: 764962 Color: 1
Size: 234355 Color: 0

Bin 3779: 685 of cap free
Amount of items: 2
Items: 
Size: 500903 Color: 1
Size: 498413 Color: 0

Bin 3780: 687 of cap free
Amount of items: 2
Items: 
Size: 542224 Color: 1
Size: 457090 Color: 0

Bin 3781: 691 of cap free
Amount of items: 2
Items: 
Size: 604246 Color: 1
Size: 395064 Color: 0

Bin 3782: 691 of cap free
Amount of items: 2
Items: 
Size: 611136 Color: 0
Size: 388174 Color: 1

Bin 3783: 693 of cap free
Amount of items: 2
Items: 
Size: 768658 Color: 0
Size: 230650 Color: 1

Bin 3784: 696 of cap free
Amount of items: 2
Items: 
Size: 573991 Color: 0
Size: 425314 Color: 1

Bin 3785: 696 of cap free
Amount of items: 2
Items: 
Size: 601320 Color: 1
Size: 397985 Color: 0

Bin 3786: 697 of cap free
Amount of items: 2
Items: 
Size: 525059 Color: 1
Size: 474245 Color: 0

Bin 3787: 699 of cap free
Amount of items: 2
Items: 
Size: 714003 Color: 1
Size: 285299 Color: 0

Bin 3788: 700 of cap free
Amount of items: 2
Items: 
Size: 782170 Color: 1
Size: 217131 Color: 0

Bin 3789: 701 of cap free
Amount of items: 2
Items: 
Size: 614591 Color: 0
Size: 384709 Color: 1

Bin 3790: 702 of cap free
Amount of items: 2
Items: 
Size: 605531 Color: 1
Size: 393768 Color: 0

Bin 3791: 704 of cap free
Amount of items: 2
Items: 
Size: 751326 Color: 0
Size: 247971 Color: 1

Bin 3792: 705 of cap free
Amount of items: 2
Items: 
Size: 539380 Color: 0
Size: 459916 Color: 1

Bin 3793: 705 of cap free
Amount of items: 2
Items: 
Size: 676679 Color: 1
Size: 322617 Color: 0

Bin 3794: 707 of cap free
Amount of items: 2
Items: 
Size: 701704 Color: 0
Size: 297590 Color: 1

Bin 3795: 708 of cap free
Amount of items: 2
Items: 
Size: 547082 Color: 0
Size: 452211 Color: 1

Bin 3796: 708 of cap free
Amount of items: 2
Items: 
Size: 596144 Color: 1
Size: 403149 Color: 0

Bin 3797: 708 of cap free
Amount of items: 2
Items: 
Size: 637467 Color: 0
Size: 361826 Color: 1

Bin 3798: 709 of cap free
Amount of items: 2
Items: 
Size: 533578 Color: 0
Size: 465714 Color: 1

Bin 3799: 711 of cap free
Amount of items: 2
Items: 
Size: 766008 Color: 0
Size: 233282 Color: 1

Bin 3800: 711 of cap free
Amount of items: 2
Items: 
Size: 601704 Color: 0
Size: 397586 Color: 1

Bin 3801: 713 of cap free
Amount of items: 2
Items: 
Size: 687691 Color: 1
Size: 311597 Color: 0

Bin 3802: 713 of cap free
Amount of items: 2
Items: 
Size: 732292 Color: 0
Size: 266996 Color: 1

Bin 3803: 720 of cap free
Amount of items: 2
Items: 
Size: 550304 Color: 1
Size: 448977 Color: 0

Bin 3804: 722 of cap free
Amount of items: 2
Items: 
Size: 506865 Color: 0
Size: 492414 Color: 1

Bin 3805: 722 of cap free
Amount of items: 2
Items: 
Size: 714806 Color: 0
Size: 284473 Color: 1

Bin 3806: 724 of cap free
Amount of items: 2
Items: 
Size: 526039 Color: 1
Size: 473238 Color: 0

Bin 3807: 724 of cap free
Amount of items: 2
Items: 
Size: 715860 Color: 1
Size: 283417 Color: 0

Bin 3808: 725 of cap free
Amount of items: 2
Items: 
Size: 577800 Color: 1
Size: 421476 Color: 0

Bin 3809: 725 of cap free
Amount of items: 2
Items: 
Size: 760438 Color: 0
Size: 238838 Color: 1

Bin 3810: 727 of cap free
Amount of items: 2
Items: 
Size: 537106 Color: 0
Size: 462168 Color: 1

Bin 3811: 729 of cap free
Amount of items: 2
Items: 
Size: 519972 Color: 0
Size: 479300 Color: 1

Bin 3812: 730 of cap free
Amount of items: 2
Items: 
Size: 569936 Color: 1
Size: 429335 Color: 0

Bin 3813: 731 of cap free
Amount of items: 2
Items: 
Size: 766042 Color: 1
Size: 233228 Color: 0

Bin 3814: 734 of cap free
Amount of items: 2
Items: 
Size: 696121 Color: 0
Size: 303146 Color: 1

Bin 3815: 735 of cap free
Amount of items: 2
Items: 
Size: 531637 Color: 1
Size: 467629 Color: 0

Bin 3816: 735 of cap free
Amount of items: 2
Items: 
Size: 626977 Color: 1
Size: 372289 Color: 0

Bin 3817: 741 of cap free
Amount of items: 2
Items: 
Size: 548693 Color: 1
Size: 450567 Color: 0

Bin 3818: 743 of cap free
Amount of items: 2
Items: 
Size: 509626 Color: 1
Size: 489632 Color: 0

Bin 3819: 744 of cap free
Amount of items: 2
Items: 
Size: 556632 Color: 0
Size: 442625 Color: 1

Bin 3820: 746 of cap free
Amount of items: 2
Items: 
Size: 592648 Color: 1
Size: 406607 Color: 0

Bin 3821: 746 of cap free
Amount of items: 2
Items: 
Size: 721621 Color: 0
Size: 277634 Color: 1

Bin 3822: 746 of cap free
Amount of items: 2
Items: 
Size: 799439 Color: 1
Size: 199816 Color: 0

Bin 3823: 747 of cap free
Amount of items: 2
Items: 
Size: 597037 Color: 1
Size: 402217 Color: 0

Bin 3824: 749 of cap free
Amount of items: 2
Items: 
Size: 689812 Color: 0
Size: 309440 Color: 1

Bin 3825: 749 of cap free
Amount of items: 2
Items: 
Size: 750016 Color: 0
Size: 249236 Color: 1

Bin 3826: 750 of cap free
Amount of items: 2
Items: 
Size: 662080 Color: 0
Size: 337171 Color: 1

Bin 3827: 755 of cap free
Amount of items: 2
Items: 
Size: 687662 Color: 1
Size: 311584 Color: 0

Bin 3828: 756 of cap free
Amount of items: 2
Items: 
Size: 616389 Color: 1
Size: 382856 Color: 0

Bin 3829: 757 of cap free
Amount of items: 2
Items: 
Size: 632369 Color: 1
Size: 366875 Color: 0

Bin 3830: 757 of cap free
Amount of items: 2
Items: 
Size: 673482 Color: 1
Size: 325762 Color: 0

Bin 3831: 758 of cap free
Amount of items: 2
Items: 
Size: 593442 Color: 0
Size: 405801 Color: 1

Bin 3832: 760 of cap free
Amount of items: 2
Items: 
Size: 658231 Color: 0
Size: 341010 Color: 1

Bin 3833: 760 of cap free
Amount of items: 2
Items: 
Size: 563645 Color: 0
Size: 435596 Color: 1

Bin 3834: 763 of cap free
Amount of items: 2
Items: 
Size: 572656 Color: 1
Size: 426582 Color: 0

Bin 3835: 763 of cap free
Amount of items: 2
Items: 
Size: 625786 Color: 0
Size: 373452 Color: 1

Bin 3836: 766 of cap free
Amount of items: 2
Items: 
Size: 669232 Color: 0
Size: 330003 Color: 1

Bin 3837: 769 of cap free
Amount of items: 2
Items: 
Size: 588216 Color: 0
Size: 411016 Color: 1

Bin 3838: 769 of cap free
Amount of items: 2
Items: 
Size: 798638 Color: 0
Size: 200594 Color: 1

Bin 3839: 772 of cap free
Amount of items: 2
Items: 
Size: 754662 Color: 0
Size: 244567 Color: 1

Bin 3840: 774 of cap free
Amount of items: 2
Items: 
Size: 709131 Color: 1
Size: 290096 Color: 0

Bin 3841: 775 of cap free
Amount of items: 2
Items: 
Size: 662168 Color: 1
Size: 337058 Color: 0

Bin 3842: 777 of cap free
Amount of items: 2
Items: 
Size: 503750 Color: 1
Size: 495474 Color: 0

Bin 3843: 777 of cap free
Amount of items: 2
Items: 
Size: 795126 Color: 0
Size: 204098 Color: 1

Bin 3844: 779 of cap free
Amount of items: 2
Items: 
Size: 778904 Color: 0
Size: 220318 Color: 1

Bin 3845: 780 of cap free
Amount of items: 2
Items: 
Size: 517837 Color: 1
Size: 481384 Color: 0

Bin 3846: 780 of cap free
Amount of items: 2
Items: 
Size: 593438 Color: 0
Size: 405783 Color: 1

Bin 3847: 793 of cap free
Amount of items: 2
Items: 
Size: 770975 Color: 1
Size: 228233 Color: 0

Bin 3848: 795 of cap free
Amount of items: 2
Items: 
Size: 561578 Color: 0
Size: 437628 Color: 1

Bin 3849: 796 of cap free
Amount of items: 2
Items: 
Size: 736348 Color: 1
Size: 262857 Color: 0

Bin 3850: 796 of cap free
Amount of items: 2
Items: 
Size: 713907 Color: 1
Size: 285298 Color: 0

Bin 3851: 797 of cap free
Amount of items: 2
Items: 
Size: 522427 Color: 1
Size: 476777 Color: 0

Bin 3852: 808 of cap free
Amount of items: 2
Items: 
Size: 547615 Color: 1
Size: 451578 Color: 0

Bin 3853: 808 of cap free
Amount of items: 2
Items: 
Size: 742267 Color: 0
Size: 256926 Color: 1

Bin 3854: 813 of cap free
Amount of items: 2
Items: 
Size: 724300 Color: 0
Size: 274888 Color: 1

Bin 3855: 813 of cap free
Amount of items: 2
Items: 
Size: 641633 Color: 0
Size: 357555 Color: 1

Bin 3856: 814 of cap free
Amount of items: 2
Items: 
Size: 613383 Color: 0
Size: 385804 Color: 1

Bin 3857: 816 of cap free
Amount of items: 2
Items: 
Size: 609232 Color: 0
Size: 389953 Color: 1

Bin 3858: 816 of cap free
Amount of items: 2
Items: 
Size: 715855 Color: 1
Size: 283330 Color: 0

Bin 3859: 817 of cap free
Amount of items: 2
Items: 
Size: 725539 Color: 1
Size: 273645 Color: 0

Bin 3860: 821 of cap free
Amount of items: 2
Items: 
Size: 716999 Color: 1
Size: 282181 Color: 0

Bin 3861: 827 of cap free
Amount of items: 2
Items: 
Size: 546980 Color: 0
Size: 452194 Color: 1

Bin 3862: 829 of cap free
Amount of items: 2
Items: 
Size: 582418 Color: 0
Size: 416754 Color: 1

Bin 3863: 833 of cap free
Amount of items: 2
Items: 
Size: 600122 Color: 0
Size: 399046 Color: 1

Bin 3864: 833 of cap free
Amount of items: 2
Items: 
Size: 608107 Color: 0
Size: 391061 Color: 1

Bin 3865: 836 of cap free
Amount of items: 2
Items: 
Size: 745851 Color: 0
Size: 253314 Color: 1

Bin 3866: 836 of cap free
Amount of items: 2
Items: 
Size: 506536 Color: 1
Size: 492629 Color: 0

Bin 3867: 837 of cap free
Amount of items: 2
Items: 
Size: 734608 Color: 0
Size: 264556 Color: 1

Bin 3868: 837 of cap free
Amount of items: 2
Items: 
Size: 799363 Color: 1
Size: 199801 Color: 0

Bin 3869: 837 of cap free
Amount of items: 2
Items: 
Size: 621825 Color: 0
Size: 377339 Color: 1

Bin 3870: 839 of cap free
Amount of items: 2
Items: 
Size: 747640 Color: 0
Size: 251522 Color: 1

Bin 3871: 841 of cap free
Amount of items: 2
Items: 
Size: 615051 Color: 1
Size: 384109 Color: 0

Bin 3872: 846 of cap free
Amount of items: 2
Items: 
Size: 642206 Color: 1
Size: 356949 Color: 0

Bin 3873: 847 of cap free
Amount of items: 2
Items: 
Size: 513611 Color: 0
Size: 485543 Color: 1

Bin 3874: 848 of cap free
Amount of items: 2
Items: 
Size: 561808 Color: 1
Size: 437345 Color: 0

Bin 3875: 848 of cap free
Amount of items: 2
Items: 
Size: 535976 Color: 1
Size: 463177 Color: 0

Bin 3876: 849 of cap free
Amount of items: 2
Items: 
Size: 592559 Color: 1
Size: 406593 Color: 0

Bin 3877: 850 of cap free
Amount of items: 2
Items: 
Size: 637393 Color: 0
Size: 361758 Color: 1

Bin 3878: 851 of cap free
Amount of items: 2
Items: 
Size: 734595 Color: 0
Size: 264555 Color: 1

Bin 3879: 858 of cap free
Amount of items: 2
Items: 
Size: 563607 Color: 0
Size: 435536 Color: 1

Bin 3880: 860 of cap free
Amount of items: 2
Items: 
Size: 541399 Color: 0
Size: 457742 Color: 1

Bin 3881: 864 of cap free
Amount of items: 2
Items: 
Size: 571599 Color: 0
Size: 427538 Color: 1

Bin 3882: 867 of cap free
Amount of items: 2
Items: 
Size: 505069 Color: 0
Size: 494065 Color: 1

Bin 3883: 868 of cap free
Amount of items: 2
Items: 
Size: 620217 Color: 0
Size: 378916 Color: 1

Bin 3884: 873 of cap free
Amount of items: 2
Items: 
Size: 515865 Color: 0
Size: 483263 Color: 1

Bin 3885: 874 of cap free
Amount of items: 2
Items: 
Size: 788855 Color: 0
Size: 210272 Color: 1

Bin 3886: 875 of cap free
Amount of items: 2
Items: 
Size: 613463 Color: 1
Size: 385663 Color: 0

Bin 3887: 876 of cap free
Amount of items: 2
Items: 
Size: 585058 Color: 1
Size: 414067 Color: 0

Bin 3888: 876 of cap free
Amount of items: 2
Items: 
Size: 615804 Color: 0
Size: 383321 Color: 1

Bin 3889: 879 of cap free
Amount of items: 2
Items: 
Size: 784137 Color: 0
Size: 214985 Color: 1

Bin 3890: 884 of cap free
Amount of items: 2
Items: 
Size: 599223 Color: 0
Size: 399894 Color: 1

Bin 3891: 886 of cap free
Amount of items: 2
Items: 
Size: 592532 Color: 1
Size: 406583 Color: 0

Bin 3892: 886 of cap free
Amount of items: 2
Items: 
Size: 699614 Color: 1
Size: 299501 Color: 0

Bin 3893: 888 of cap free
Amount of items: 2
Items: 
Size: 716953 Color: 1
Size: 282160 Color: 0

Bin 3894: 889 of cap free
Amount of items: 2
Items: 
Size: 598041 Color: 1
Size: 401071 Color: 0

Bin 3895: 896 of cap free
Amount of items: 2
Items: 
Size: 753443 Color: 1
Size: 245662 Color: 0

Bin 3896: 896 of cap free
Amount of items: 2
Items: 
Size: 635061 Color: 0
Size: 364044 Color: 1

Bin 3897: 896 of cap free
Amount of items: 2
Items: 
Size: 653598 Color: 1
Size: 345507 Color: 0

Bin 3898: 898 of cap free
Amount of items: 2
Items: 
Size: 775638 Color: 1
Size: 223465 Color: 0

Bin 3899: 900 of cap free
Amount of items: 2
Items: 
Size: 659778 Color: 0
Size: 339323 Color: 1

Bin 3900: 901 of cap free
Amount of items: 2
Items: 
Size: 787469 Color: 0
Size: 211631 Color: 1

Bin 3901: 903 of cap free
Amount of items: 2
Items: 
Size: 676830 Color: 0
Size: 322268 Color: 1

Bin 3902: 909 of cap free
Amount of items: 2
Items: 
Size: 704609 Color: 0
Size: 294483 Color: 1

Bin 3903: 911 of cap free
Amount of items: 2
Items: 
Size: 796683 Color: 0
Size: 202407 Color: 1

Bin 3904: 911 of cap free
Amount of items: 2
Items: 
Size: 730928 Color: 0
Size: 268162 Color: 1

Bin 3905: 912 of cap free
Amount of items: 2
Items: 
Size: 526874 Color: 0
Size: 472215 Color: 1

Bin 3906: 919 of cap free
Amount of items: 2
Items: 
Size: 517705 Color: 1
Size: 481377 Color: 0

Bin 3907: 919 of cap free
Amount of items: 2
Items: 
Size: 611134 Color: 0
Size: 387948 Color: 1

Bin 3908: 924 of cap free
Amount of items: 2
Items: 
Size: 626618 Color: 0
Size: 372459 Color: 1

Bin 3909: 926 of cap free
Amount of items: 2
Items: 
Size: 524910 Color: 1
Size: 474165 Color: 0

Bin 3910: 930 of cap free
Amount of items: 2
Items: 
Size: 760893 Color: 1
Size: 238178 Color: 0

Bin 3911: 931 of cap free
Amount of items: 2
Items: 
Size: 574478 Color: 1
Size: 424592 Color: 0

Bin 3912: 931 of cap free
Amount of items: 2
Items: 
Size: 776682 Color: 0
Size: 222388 Color: 1

Bin 3913: 933 of cap free
Amount of items: 2
Items: 
Size: 609227 Color: 0
Size: 389841 Color: 1

Bin 3914: 934 of cap free
Amount of items: 2
Items: 
Size: 667000 Color: 1
Size: 332067 Color: 0

Bin 3915: 936 of cap free
Amount of items: 2
Items: 
Size: 704596 Color: 0
Size: 294469 Color: 1

Bin 3916: 937 of cap free
Amount of items: 2
Items: 
Size: 590983 Color: 1
Size: 408081 Color: 0

Bin 3917: 938 of cap free
Amount of items: 2
Items: 
Size: 554045 Color: 0
Size: 445018 Color: 1

Bin 3918: 943 of cap free
Amount of items: 2
Items: 
Size: 573863 Color: 0
Size: 425195 Color: 1

Bin 3919: 943 of cap free
Amount of items: 2
Items: 
Size: 649569 Color: 0
Size: 349489 Color: 1

Bin 3920: 944 of cap free
Amount of items: 2
Items: 
Size: 586122 Color: 0
Size: 412935 Color: 1

Bin 3921: 945 of cap free
Amount of items: 2
Items: 
Size: 579150 Color: 0
Size: 419906 Color: 1

Bin 3922: 945 of cap free
Amount of items: 2
Items: 
Size: 571548 Color: 0
Size: 427508 Color: 1

Bin 3923: 950 of cap free
Amount of items: 2
Items: 
Size: 582552 Color: 1
Size: 416499 Color: 0

Bin 3924: 953 of cap free
Amount of items: 2
Items: 
Size: 686164 Color: 1
Size: 312884 Color: 0

Bin 3925: 953 of cap free
Amount of items: 2
Items: 
Size: 669125 Color: 0
Size: 329923 Color: 1

Bin 3926: 954 of cap free
Amount of items: 2
Items: 
Size: 659343 Color: 1
Size: 339704 Color: 0

Bin 3927: 957 of cap free
Amount of items: 2
Items: 
Size: 506498 Color: 1
Size: 492546 Color: 0

Bin 3928: 958 of cap free
Amount of items: 2
Items: 
Size: 571537 Color: 0
Size: 427506 Color: 1

Bin 3929: 959 of cap free
Amount of items: 2
Items: 
Size: 515545 Color: 1
Size: 483497 Color: 0

Bin 3930: 959 of cap free
Amount of items: 2
Items: 
Size: 715787 Color: 1
Size: 283255 Color: 0

Bin 3931: 960 of cap free
Amount of items: 2
Items: 
Size: 790867 Color: 1
Size: 208174 Color: 0

Bin 3932: 960 of cap free
Amount of items: 2
Items: 
Size: 515842 Color: 0
Size: 483199 Color: 1

Bin 3933: 960 of cap free
Amount of items: 2
Items: 
Size: 550150 Color: 1
Size: 448891 Color: 0

Bin 3934: 960 of cap free
Amount of items: 2
Items: 
Size: 707387 Color: 0
Size: 291654 Color: 1

Bin 3935: 962 of cap free
Amount of items: 2
Items: 
Size: 513904 Color: 1
Size: 485135 Color: 0

Bin 3936: 962 of cap free
Amount of items: 2
Items: 
Size: 794966 Color: 0
Size: 204073 Color: 1

Bin 3937: 963 of cap free
Amount of items: 2
Items: 
Size: 611547 Color: 1
Size: 387491 Color: 0

Bin 3938: 965 of cap free
Amount of items: 2
Items: 
Size: 660895 Color: 1
Size: 338141 Color: 0

Bin 3939: 965 of cap free
Amount of items: 2
Items: 
Size: 704573 Color: 0
Size: 294463 Color: 1

Bin 3940: 967 of cap free
Amount of items: 2
Items: 
Size: 505012 Color: 0
Size: 494022 Color: 1

Bin 3941: 968 of cap free
Amount of items: 2
Items: 
Size: 502004 Color: 0
Size: 497029 Color: 1

Bin 3942: 969 of cap free
Amount of items: 2
Items: 
Size: 546964 Color: 0
Size: 452068 Color: 1

Bin 3943: 970 of cap free
Amount of items: 2
Items: 
Size: 659710 Color: 0
Size: 339321 Color: 1

Bin 3944: 972 of cap free
Amount of items: 2
Items: 
Size: 622452 Color: 1
Size: 376577 Color: 0

Bin 3945: 974 of cap free
Amount of items: 2
Items: 
Size: 636345 Color: 0
Size: 362682 Color: 1

Bin 3946: 974 of cap free
Amount of items: 2
Items: 
Size: 736342 Color: 1
Size: 262685 Color: 0

Bin 3947: 976 of cap free
Amount of items: 2
Items: 
Size: 525020 Color: 0
Size: 474005 Color: 1

Bin 3948: 976 of cap free
Amount of items: 2
Items: 
Size: 707190 Color: 1
Size: 291835 Color: 0

Bin 3949: 977 of cap free
Amount of items: 2
Items: 
Size: 533437 Color: 0
Size: 465587 Color: 1

Bin 3950: 979 of cap free
Amount of items: 2
Items: 
Size: 732174 Color: 0
Size: 266848 Color: 1

Bin 3951: 985 of cap free
Amount of items: 2
Items: 
Size: 787383 Color: 1
Size: 211633 Color: 0

Bin 3952: 985 of cap free
Amount of items: 2
Items: 
Size: 676433 Color: 1
Size: 322583 Color: 0

Bin 3953: 985 of cap free
Amount of items: 2
Items: 
Size: 758875 Color: 1
Size: 240141 Color: 0

Bin 3954: 988 of cap free
Amount of items: 2
Items: 
Size: 503666 Color: 1
Size: 495347 Color: 0

Bin 3955: 989 of cap free
Amount of items: 2
Items: 
Size: 736327 Color: 1
Size: 262685 Color: 0

Bin 3956: 995 of cap free
Amount of items: 2
Items: 
Size: 523811 Color: 1
Size: 475195 Color: 0

Bin 3957: 1003 of cap free
Amount of items: 2
Items: 
Size: 567951 Color: 1
Size: 431047 Color: 0

Bin 3958: 1003 of cap free
Amount of items: 2
Items: 
Size: 680905 Color: 1
Size: 318093 Color: 0

Bin 3959: 1004 of cap free
Amount of items: 2
Items: 
Size: 745679 Color: 1
Size: 253318 Color: 0

Bin 3960: 1009 of cap free
Amount of items: 2
Items: 
Size: 593218 Color: 0
Size: 405774 Color: 1

Bin 3961: 1013 of cap free
Amount of items: 2
Items: 
Size: 658178 Color: 0
Size: 340810 Color: 1

Bin 3962: 1014 of cap free
Amount of items: 2
Items: 
Size: 547554 Color: 1
Size: 451433 Color: 0

Bin 3963: 1016 of cap free
Amount of items: 2
Items: 
Size: 778623 Color: 1
Size: 220362 Color: 0

Bin 3964: 1017 of cap free
Amount of items: 2
Items: 
Size: 537507 Color: 1
Size: 461477 Color: 0

Bin 3965: 1018 of cap free
Amount of items: 2
Items: 
Size: 673465 Color: 1
Size: 325518 Color: 0

Bin 3966: 1027 of cap free
Amount of items: 2
Items: 
Size: 642985 Color: 0
Size: 355989 Color: 1

Bin 3967: 1037 of cap free
Amount of items: 2
Items: 
Size: 522378 Color: 1
Size: 476586 Color: 0

Bin 3968: 1037 of cap free
Amount of items: 2
Items: 
Size: 576354 Color: 0
Size: 422610 Color: 1

Bin 3969: 1038 of cap free
Amount of items: 2
Items: 
Size: 588118 Color: 0
Size: 410845 Color: 1

Bin 3970: 1039 of cap free
Amount of items: 2
Items: 
Size: 547531 Color: 1
Size: 451431 Color: 0

Bin 3971: 1039 of cap free
Amount of items: 2
Items: 
Size: 728067 Color: 0
Size: 270895 Color: 1

Bin 3972: 1039 of cap free
Amount of items: 2
Items: 
Size: 597946 Color: 1
Size: 401016 Color: 0

Bin 3973: 1040 of cap free
Amount of items: 2
Items: 
Size: 628265 Color: 0
Size: 370696 Color: 1

Bin 3974: 1043 of cap free
Amount of items: 2
Items: 
Size: 561785 Color: 1
Size: 437173 Color: 0

Bin 3975: 1044 of cap free
Amount of items: 2
Items: 
Size: 581159 Color: 0
Size: 417798 Color: 1

Bin 3976: 1045 of cap free
Amount of items: 2
Items: 
Size: 625595 Color: 1
Size: 373361 Color: 0

Bin 3977: 1047 of cap free
Amount of items: 2
Items: 
Size: 653473 Color: 1
Size: 345481 Color: 0

Bin 3978: 1051 of cap free
Amount of items: 2
Items: 
Size: 679436 Color: 1
Size: 319514 Color: 0

Bin 3979: 1053 of cap free
Amount of items: 2
Items: 
Size: 747056 Color: 1
Size: 251892 Color: 0

Bin 3980: 1070 of cap free
Amount of items: 2
Items: 
Size: 644510 Color: 1
Size: 354421 Color: 0

Bin 3981: 1071 of cap free
Amount of items: 2
Items: 
Size: 605331 Color: 1
Size: 393599 Color: 0

Bin 3982: 1073 of cap free
Amount of items: 2
Items: 
Size: 750998 Color: 0
Size: 247930 Color: 1

Bin 3983: 1074 of cap free
Amount of items: 2
Items: 
Size: 517587 Color: 1
Size: 481340 Color: 0

Bin 3984: 1077 of cap free
Amount of items: 2
Items: 
Size: 785012 Color: 1
Size: 213912 Color: 0

Bin 3985: 1078 of cap free
Amount of items: 2
Items: 
Size: 567789 Color: 0
Size: 431134 Color: 1

Bin 3986: 1079 of cap free
Amount of items: 2
Items: 
Size: 724441 Color: 1
Size: 274481 Color: 0

Bin 3987: 1081 of cap free
Amount of items: 2
Items: 
Size: 531316 Color: 1
Size: 467604 Color: 0

Bin 3988: 1085 of cap free
Amount of items: 2
Items: 
Size: 799163 Color: 1
Size: 199753 Color: 0

Bin 3989: 1092 of cap free
Amount of items: 2
Items: 
Size: 783940 Color: 0
Size: 214969 Color: 1

Bin 3990: 1094 of cap free
Amount of items: 2
Items: 
Size: 531522 Color: 0
Size: 467385 Color: 1

Bin 3991: 1096 of cap free
Amount of items: 2
Items: 
Size: 764550 Color: 1
Size: 234355 Color: 0

Bin 3992: 1101 of cap free
Amount of items: 2
Items: 
Size: 743537 Color: 1
Size: 255363 Color: 0

Bin 3993: 1101 of cap free
Amount of items: 2
Items: 
Size: 692175 Color: 0
Size: 306725 Color: 1

Bin 3994: 1102 of cap free
Amount of items: 2
Items: 
Size: 701386 Color: 0
Size: 297513 Color: 1

Bin 3995: 1104 of cap free
Amount of items: 2
Items: 
Size: 653457 Color: 1
Size: 345440 Color: 0

Bin 3996: 1108 of cap free
Amount of items: 2
Items: 
Size: 680031 Color: 0
Size: 318862 Color: 1

Bin 3997: 1108 of cap free
Amount of items: 2
Items: 
Size: 517396 Color: 0
Size: 481497 Color: 1

Bin 3998: 1108 of cap free
Amount of items: 2
Items: 
Size: 639356 Color: 1
Size: 359537 Color: 0

Bin 3999: 1110 of cap free
Amount of items: 2
Items: 
Size: 583790 Color: 0
Size: 415101 Color: 1

Bin 4000: 1111 of cap free
Amount of items: 2
Items: 
Size: 790461 Color: 0
Size: 208429 Color: 1

Bin 4001: 1112 of cap free
Amount of items: 2
Items: 
Size: 596534 Color: 0
Size: 402355 Color: 1

Bin 4002: 1118 of cap free
Amount of items: 2
Items: 
Size: 543722 Color: 1
Size: 455161 Color: 0

Bin 4003: 1119 of cap free
Amount of items: 2
Items: 
Size: 775016 Color: 0
Size: 223866 Color: 1

Bin 4004: 1119 of cap free
Amount of items: 2
Items: 
Size: 576276 Color: 0
Size: 422606 Color: 1

Bin 4005: 1120 of cap free
Amount of items: 2
Items: 
Size: 537431 Color: 1
Size: 461450 Color: 0

Bin 4006: 1120 of cap free
Amount of items: 2
Items: 
Size: 506380 Color: 1
Size: 492501 Color: 0

Bin 4007: 1121 of cap free
Amount of items: 2
Items: 
Size: 642909 Color: 0
Size: 355971 Color: 1

Bin 4008: 1123 of cap free
Amount of items: 2
Items: 
Size: 528735 Color: 0
Size: 470143 Color: 1

Bin 4009: 1127 of cap free
Amount of items: 2
Items: 
Size: 620112 Color: 0
Size: 378762 Color: 1

Bin 4010: 1128 of cap free
Amount of items: 2
Items: 
Size: 517534 Color: 1
Size: 481339 Color: 0

Bin 4011: 1131 of cap free
Amount of items: 2
Items: 
Size: 541231 Color: 0
Size: 457639 Color: 1

Bin 4012: 1135 of cap free
Amount of items: 2
Items: 
Size: 509588 Color: 1
Size: 489278 Color: 0

Bin 4013: 1140 of cap free
Amount of items: 2
Items: 
Size: 647260 Color: 0
Size: 351601 Color: 1

Bin 4014: 1142 of cap free
Amount of items: 2
Items: 
Size: 509718 Color: 0
Size: 489141 Color: 1

Bin 4015: 1146 of cap free
Amount of items: 2
Items: 
Size: 533297 Color: 0
Size: 465558 Color: 1

Bin 4016: 1149 of cap free
Amount of items: 2
Items: 
Size: 796670 Color: 0
Size: 202182 Color: 1

Bin 4017: 1151 of cap free
Amount of items: 2
Items: 
Size: 761358 Color: 0
Size: 237492 Color: 1

Bin 4018: 1154 of cap free
Amount of items: 2
Items: 
Size: 711324 Color: 1
Size: 287523 Color: 0

Bin 4019: 1164 of cap free
Amount of items: 2
Items: 
Size: 798563 Color: 0
Size: 200274 Color: 1

Bin 4020: 1166 of cap free
Amount of items: 2
Items: 
Size: 629170 Color: 1
Size: 369665 Color: 0

Bin 4021: 1170 of cap free
Amount of items: 2
Items: 
Size: 532974 Color: 1
Size: 465857 Color: 0

Bin 4022: 1171 of cap free
Amount of items: 2
Items: 
Size: 677738 Color: 1
Size: 321092 Color: 0

Bin 4023: 1172 of cap free
Amount of items: 2
Items: 
Size: 733847 Color: 1
Size: 264982 Color: 0

Bin 4024: 1173 of cap free
Amount of items: 2
Items: 
Size: 582448 Color: 1
Size: 416380 Color: 0

Bin 4025: 1176 of cap free
Amount of items: 2
Items: 
Size: 543411 Color: 0
Size: 455414 Color: 1

Bin 4026: 1188 of cap free
Amount of items: 2
Items: 
Size: 738535 Color: 0
Size: 260278 Color: 1

Bin 4027: 1192 of cap free
Amount of items: 2
Items: 
Size: 517475 Color: 1
Size: 481334 Color: 0

Bin 4028: 1194 of cap free
Amount of items: 2
Items: 
Size: 747358 Color: 0
Size: 251449 Color: 1

Bin 4029: 1194 of cap free
Amount of items: 2
Items: 
Size: 589269 Color: 1
Size: 409538 Color: 0

Bin 4030: 1195 of cap free
Amount of items: 2
Items: 
Size: 673921 Color: 0
Size: 324885 Color: 1

Bin 4031: 1196 of cap free
Amount of items: 2
Items: 
Size: 639340 Color: 1
Size: 359465 Color: 0

Bin 4032: 1196 of cap free
Amount of items: 2
Items: 
Size: 534553 Color: 0
Size: 464252 Color: 1

Bin 4033: 1197 of cap free
Amount of items: 2
Items: 
Size: 582394 Color: 0
Size: 416410 Color: 1

Bin 4034: 1198 of cap free
Amount of items: 2
Items: 
Size: 617819 Color: 0
Size: 380984 Color: 1

Bin 4035: 1200 of cap free
Amount of items: 2
Items: 
Size: 691480 Color: 1
Size: 307321 Color: 0

Bin 4036: 1200 of cap free
Amount of items: 2
Items: 
Size: 658128 Color: 0
Size: 340673 Color: 1

Bin 4037: 1202 of cap free
Amount of items: 2
Items: 
Size: 778604 Color: 1
Size: 220195 Color: 0

Bin 4038: 1202 of cap free
Amount of items: 2
Items: 
Size: 715724 Color: 1
Size: 283075 Color: 0

Bin 4039: 1205 of cap free
Amount of items: 2
Items: 
Size: 613194 Color: 0
Size: 385602 Color: 1

Bin 4040: 1206 of cap free
Amount of items: 2
Items: 
Size: 734247 Color: 0
Size: 264548 Color: 1

Bin 4041: 1208 of cap free
Amount of items: 2
Items: 
Size: 531473 Color: 0
Size: 467320 Color: 1

Bin 4042: 1209 of cap free
Amount of items: 2
Items: 
Size: 675171 Color: 0
Size: 323621 Color: 1

Bin 4043: 1211 of cap free
Amount of items: 2
Items: 
Size: 730704 Color: 0
Size: 268086 Color: 1

Bin 4044: 1211 of cap free
Amount of items: 2
Items: 
Size: 692148 Color: 0
Size: 306642 Color: 1

Bin 4045: 1213 of cap free
Amount of items: 2
Items: 
Size: 760804 Color: 1
Size: 237984 Color: 0

Bin 4046: 1223 of cap free
Amount of items: 2
Items: 
Size: 728020 Color: 0
Size: 270758 Color: 1

Bin 4047: 1230 of cap free
Amount of items: 2
Items: 
Size: 640621 Color: 1
Size: 358150 Color: 0

Bin 4048: 1233 of cap free
Amount of items: 2
Items: 
Size: 588073 Color: 0
Size: 410695 Color: 1

Bin 4049: 1236 of cap free
Amount of items: 2
Items: 
Size: 626775 Color: 1
Size: 371990 Color: 0

Bin 4050: 1242 of cap free
Amount of items: 2
Items: 
Size: 625402 Color: 1
Size: 373357 Color: 0

Bin 4051: 1243 of cap free
Amount of items: 2
Items: 
Size: 644504 Color: 1
Size: 354254 Color: 0

Bin 4052: 1248 of cap free
Amount of items: 2
Items: 
Size: 715692 Color: 1
Size: 283061 Color: 0

Bin 4053: 1253 of cap free
Amount of items: 2
Items: 
Size: 666704 Color: 1
Size: 332044 Color: 0

Bin 4054: 1260 of cap free
Amount of items: 2
Items: 
Size: 537389 Color: 1
Size: 461352 Color: 0

Bin 4055: 1267 of cap free
Amount of items: 2
Items: 
Size: 690152 Color: 1
Size: 308582 Color: 0

Bin 4056: 1268 of cap free
Amount of items: 2
Items: 
Size: 778587 Color: 1
Size: 220146 Color: 0

Bin 4057: 1268 of cap free
Amount of items: 2
Items: 
Size: 654441 Color: 0
Size: 344292 Color: 1

Bin 4058: 1269 of cap free
Amount of items: 2
Items: 
Size: 788797 Color: 1
Size: 209935 Color: 0

Bin 4059: 1279 of cap free
Amount of items: 2
Items: 
Size: 728011 Color: 0
Size: 270711 Color: 1

Bin 4060: 1280 of cap free
Amount of items: 2
Items: 
Size: 619585 Color: 1
Size: 379136 Color: 0

Bin 4061: 1280 of cap free
Amount of items: 2
Items: 
Size: 750879 Color: 0
Size: 247842 Color: 1

Bin 4062: 1281 of cap free
Amount of items: 2
Items: 
Size: 721461 Color: 0
Size: 277259 Color: 1

Bin 4063: 1287 of cap free
Amount of items: 2
Items: 
Size: 673879 Color: 0
Size: 324835 Color: 1

Bin 4064: 1297 of cap free
Amount of items: 2
Items: 
Size: 592993 Color: 0
Size: 405711 Color: 1

Bin 4065: 1298 of cap free
Amount of items: 2
Items: 
Size: 590808 Color: 1
Size: 407895 Color: 0

Bin 4066: 1302 of cap free
Amount of items: 2
Items: 
Size: 764393 Color: 1
Size: 234306 Color: 0

Bin 4067: 1304 of cap free
Amount of items: 2
Items: 
Size: 666667 Color: 1
Size: 332030 Color: 0

Bin 4068: 1306 of cap free
Amount of items: 2
Items: 
Size: 649436 Color: 0
Size: 349259 Color: 1

Bin 4069: 1311 of cap free
Amount of items: 2
Items: 
Size: 736623 Color: 0
Size: 262067 Color: 1

Bin 4070: 1321 of cap free
Amount of items: 2
Items: 
Size: 553947 Color: 0
Size: 444733 Color: 1

Bin 4071: 1321 of cap free
Amount of items: 2
Items: 
Size: 634891 Color: 0
Size: 363789 Color: 1

Bin 4072: 1321 of cap free
Amount of items: 2
Items: 
Size: 652796 Color: 0
Size: 345884 Color: 1

Bin 4073: 1326 of cap free
Amount of items: 2
Items: 
Size: 554396 Color: 1
Size: 444279 Color: 0

Bin 4074: 1326 of cap free
Amount of items: 2
Items: 
Size: 690101 Color: 1
Size: 308574 Color: 0

Bin 4075: 1338 of cap free
Amount of items: 2
Items: 
Size: 731652 Color: 1
Size: 267011 Color: 0

Bin 4076: 1345 of cap free
Amount of items: 2
Items: 
Size: 610721 Color: 0
Size: 387935 Color: 1

Bin 4077: 1346 of cap free
Amount of items: 2
Items: 
Size: 517444 Color: 1
Size: 481211 Color: 0

Bin 4078: 1346 of cap free
Amount of items: 2
Items: 
Size: 596302 Color: 0
Size: 402353 Color: 1

Bin 4079: 1350 of cap free
Amount of items: 2
Items: 
Size: 676781 Color: 0
Size: 321870 Color: 1

Bin 4080: 1358 of cap free
Amount of items: 2
Items: 
Size: 608995 Color: 0
Size: 389648 Color: 1

Bin 4081: 1362 of cap free
Amount of items: 2
Items: 
Size: 522142 Color: 1
Size: 476497 Color: 0

Bin 4082: 1371 of cap free
Amount of items: 2
Items: 
Size: 596287 Color: 0
Size: 402343 Color: 1

Bin 4083: 1377 of cap free
Amount of items: 2
Items: 
Size: 685285 Color: 0
Size: 313339 Color: 1

Bin 4084: 1379 of cap free
Amount of items: 2
Items: 
Size: 519945 Color: 0
Size: 478677 Color: 1

Bin 4085: 1382 of cap free
Amount of items: 2
Items: 
Size: 536463 Color: 0
Size: 462156 Color: 1

Bin 4086: 1383 of cap free
Amount of items: 2
Items: 
Size: 656680 Color: 0
Size: 341938 Color: 1

Bin 4087: 1389 of cap free
Amount of items: 2
Items: 
Size: 517120 Color: 0
Size: 481492 Color: 1

Bin 4088: 1391 of cap free
Amount of items: 2
Items: 
Size: 756187 Color: 0
Size: 242423 Color: 1

Bin 4089: 1394 of cap free
Amount of items: 2
Items: 
Size: 750843 Color: 0
Size: 247764 Color: 1

Bin 4090: 1395 of cap free
Amount of items: 2
Items: 
Size: 613014 Color: 0
Size: 385592 Color: 1

Bin 4091: 1399 of cap free
Amount of items: 2
Items: 
Size: 539603 Color: 1
Size: 458999 Color: 0

Bin 4092: 1405 of cap free
Amount of items: 2
Items: 
Size: 701382 Color: 0
Size: 297214 Color: 1

Bin 4093: 1409 of cap free
Amount of items: 2
Items: 
Size: 703189 Color: 1
Size: 295403 Color: 0

Bin 4094: 1410 of cap free
Amount of items: 2
Items: 
Size: 517115 Color: 0
Size: 481476 Color: 1

Bin 4095: 1425 of cap free
Amount of items: 2
Items: 
Size: 615770 Color: 0
Size: 382806 Color: 1

Bin 4096: 1437 of cap free
Amount of items: 2
Items: 
Size: 652694 Color: 0
Size: 345870 Color: 1

Bin 4097: 1439 of cap free
Amount of items: 2
Items: 
Size: 692786 Color: 1
Size: 305776 Color: 0

Bin 4098: 1443 of cap free
Amount of items: 2
Items: 
Size: 740636 Color: 0
Size: 257922 Color: 1

Bin 4099: 1455 of cap free
Amount of items: 2
Items: 
Size: 775096 Color: 1
Size: 223450 Color: 0

Bin 4100: 1458 of cap free
Amount of items: 2
Items: 
Size: 567926 Color: 1
Size: 430617 Color: 0

Bin 4101: 1458 of cap free
Amount of items: 2
Items: 
Size: 786966 Color: 1
Size: 211577 Color: 0

Bin 4102: 1464 of cap free
Amount of items: 2
Items: 
Size: 577373 Color: 1
Size: 421164 Color: 0

Bin 4103: 1468 of cap free
Amount of items: 2
Items: 
Size: 730483 Color: 0
Size: 268050 Color: 1

Bin 4104: 1473 of cap free
Amount of items: 2
Items: 
Size: 635588 Color: 1
Size: 362940 Color: 0

Bin 4105: 1474 of cap free
Amount of items: 2
Items: 
Size: 679358 Color: 1
Size: 319169 Color: 0

Bin 4106: 1476 of cap free
Amount of items: 2
Items: 
Size: 616222 Color: 1
Size: 382303 Color: 0

Bin 4107: 1480 of cap free
Amount of items: 2
Items: 
Size: 793097 Color: 1
Size: 205424 Color: 0

Bin 4108: 1484 of cap free
Amount of items: 2
Items: 
Size: 503613 Color: 1
Size: 494904 Color: 0

Bin 4109: 1495 of cap free
Amount of items: 2
Items: 
Size: 597896 Color: 1
Size: 400610 Color: 0

Bin 4110: 1505 of cap free
Amount of items: 2
Items: 
Size: 517361 Color: 1
Size: 481135 Color: 0

Bin 4111: 1514 of cap free
Amount of items: 2
Items: 
Size: 610649 Color: 0
Size: 387838 Color: 1

Bin 4112: 1515 of cap free
Amount of items: 2
Items: 
Size: 590677 Color: 1
Size: 407809 Color: 0

Bin 4113: 1518 of cap free
Amount of items: 2
Items: 
Size: 660637 Color: 1
Size: 337846 Color: 0

Bin 4114: 1519 of cap free
Amount of items: 2
Items: 
Size: 538794 Color: 0
Size: 459688 Color: 1

Bin 4115: 1519 of cap free
Amount of items: 2
Items: 
Size: 708433 Color: 1
Size: 290049 Color: 0

Bin 4116: 1521 of cap free
Amount of items: 2
Items: 
Size: 509230 Color: 1
Size: 489250 Color: 0

Bin 4117: 1532 of cap free
Amount of items: 2
Items: 
Size: 613242 Color: 1
Size: 385227 Color: 0

Bin 4118: 1539 of cap free
Amount of items: 2
Items: 
Size: 706743 Color: 1
Size: 291719 Color: 0

Bin 4119: 1548 of cap free
Amount of items: 2
Items: 
Size: 790385 Color: 1
Size: 208068 Color: 0

Bin 4120: 1548 of cap free
Amount of items: 2
Items: 
Size: 588945 Color: 1
Size: 409508 Color: 0

Bin 4121: 1553 of cap free
Amount of items: 2
Items: 
Size: 679303 Color: 1
Size: 319145 Color: 0

Bin 4122: 1563 of cap free
Amount of items: 2
Items: 
Size: 537317 Color: 1
Size: 461121 Color: 0

Bin 4123: 1567 of cap free
Amount of items: 2
Items: 
Size: 783572 Color: 0
Size: 214862 Color: 1

Bin 4124: 1573 of cap free
Amount of items: 2
Items: 
Size: 775046 Color: 1
Size: 223382 Color: 0

Bin 4125: 1574 of cap free
Amount of items: 2
Items: 
Size: 549619 Color: 1
Size: 448808 Color: 0

Bin 4126: 1577 of cap free
Amount of items: 2
Items: 
Size: 640918 Color: 0
Size: 357506 Color: 1

Bin 4127: 1577 of cap free
Amount of items: 2
Items: 
Size: 504403 Color: 0
Size: 494021 Color: 1

Bin 4128: 1581 of cap free
Amount of items: 2
Items: 
Size: 616173 Color: 1
Size: 382247 Color: 0

Bin 4129: 1593 of cap free
Amount of items: 2
Items: 
Size: 674538 Color: 1
Size: 323870 Color: 0

Bin 4130: 1606 of cap free
Amount of items: 2
Items: 
Size: 685086 Color: 0
Size: 313309 Color: 1

Bin 4131: 1611 of cap free
Amount of items: 2
Items: 
Size: 691962 Color: 0
Size: 306428 Color: 1

Bin 4132: 1621 of cap free
Amount of items: 2
Items: 
Size: 767758 Color: 0
Size: 230622 Color: 1

Bin 4133: 1638 of cap free
Amount of items: 2
Items: 
Size: 739573 Color: 1
Size: 258790 Color: 0

Bin 4134: 1638 of cap free
Amount of items: 2
Items: 
Size: 567641 Color: 0
Size: 430722 Color: 1

Bin 4135: 1651 of cap free
Amount of items: 2
Items: 
Size: 738530 Color: 0
Size: 259820 Color: 1

Bin 4136: 1653 of cap free
Amount of items: 2
Items: 
Size: 604732 Color: 0
Size: 393616 Color: 1

Bin 4137: 1673 of cap free
Amount of items: 2
Items: 
Size: 652531 Color: 0
Size: 345797 Color: 1

Bin 4138: 1676 of cap free
Amount of items: 2
Items: 
Size: 721376 Color: 0
Size: 276949 Color: 1

Bin 4139: 1695 of cap free
Amount of items: 2
Items: 
Size: 616078 Color: 1
Size: 382228 Color: 0

Bin 4140: 1695 of cap free
Amount of items: 2
Items: 
Size: 738500 Color: 0
Size: 259806 Color: 1

Bin 4141: 1699 of cap free
Amount of items: 2
Items: 
Size: 677526 Color: 1
Size: 320776 Color: 0

Bin 4142: 1705 of cap free
Amount of items: 2
Items: 
Size: 790262 Color: 1
Size: 208034 Color: 0

Bin 4143: 1709 of cap free
Amount of items: 2
Items: 
Size: 701295 Color: 0
Size: 296997 Color: 1

Bin 4144: 1718 of cap free
Amount of items: 2
Items: 
Size: 567600 Color: 0
Size: 430683 Color: 1

Bin 4145: 1722 of cap free
Amount of items: 2
Items: 
Size: 770898 Color: 1
Size: 227381 Color: 0

Bin 4146: 1728 of cap free
Amount of items: 2
Items: 
Size: 692739 Color: 1
Size: 305534 Color: 0

Bin 4147: 1728 of cap free
Amount of items: 2
Items: 
Size: 621815 Color: 1
Size: 376458 Color: 0

Bin 4148: 1736 of cap free
Amount of items: 2
Items: 
Size: 515452 Color: 1
Size: 482813 Color: 0

Bin 4149: 1742 of cap free
Amount of items: 2
Items: 
Size: 774496 Color: 0
Size: 223763 Color: 1

Bin 4150: 1743 of cap free
Amount of items: 2
Items: 
Size: 575870 Color: 0
Size: 422388 Color: 1

Bin 4151: 1744 of cap free
Amount of items: 2
Items: 
Size: 561309 Color: 1
Size: 436948 Color: 0

Bin 4152: 1753 of cap free
Amount of items: 2
Items: 
Size: 616025 Color: 1
Size: 382223 Color: 0

Bin 4153: 1761 of cap free
Amount of items: 2
Items: 
Size: 621501 Color: 0
Size: 376739 Color: 1

Bin 4154: 1763 of cap free
Amount of items: 2
Items: 
Size: 596051 Color: 0
Size: 402187 Color: 1

Bin 4155: 1765 of cap free
Amount of items: 2
Items: 
Size: 536200 Color: 0
Size: 462036 Color: 1

Bin 4156: 1769 of cap free
Amount of items: 2
Items: 
Size: 521982 Color: 1
Size: 476250 Color: 0

Bin 4157: 1772 of cap free
Amount of items: 2
Items: 
Size: 677468 Color: 1
Size: 320761 Color: 0

Bin 4158: 1776 of cap free
Amount of items: 2
Items: 
Size: 668513 Color: 0
Size: 329712 Color: 1

Bin 4159: 1797 of cap free
Amount of items: 2
Items: 
Size: 567587 Color: 0
Size: 430617 Color: 1

Bin 4160: 1802 of cap free
Amount of items: 2
Items: 
Size: 706967 Color: 0
Size: 291232 Color: 1

Bin 4161: 1802 of cap free
Amount of items: 2
Items: 
Size: 649417 Color: 0
Size: 348782 Color: 1

Bin 4162: 1813 of cap free
Amount of items: 2
Items: 
Size: 528915 Color: 1
Size: 469273 Color: 0

Bin 4163: 1819 of cap free
Amount of items: 2
Items: 
Size: 588018 Color: 0
Size: 410164 Color: 1

Bin 4164: 1827 of cap free
Amount of items: 2
Items: 
Size: 513058 Color: 1
Size: 485116 Color: 0

Bin 4165: 1831 of cap free
Amount of items: 2
Items: 
Size: 786713 Color: 1
Size: 211457 Color: 0

Bin 4166: 1832 of cap free
Amount of items: 2
Items: 
Size: 567878 Color: 1
Size: 430291 Color: 0

Bin 4167: 1845 of cap free
Amount of items: 2
Items: 
Size: 580537 Color: 0
Size: 417619 Color: 1

Bin 4168: 1853 of cap free
Amount of items: 2
Items: 
Size: 679655 Color: 0
Size: 318493 Color: 1

Bin 4169: 1861 of cap free
Amount of items: 2
Items: 
Size: 648978 Color: 1
Size: 349162 Color: 0

Bin 4170: 1867 of cap free
Amount of items: 2
Items: 
Size: 706453 Color: 1
Size: 291681 Color: 0

Bin 4171: 1876 of cap free
Amount of items: 2
Items: 
Size: 505961 Color: 1
Size: 492164 Color: 0

Bin 4172: 1878 of cap free
Amount of items: 2
Items: 
Size: 649371 Color: 0
Size: 348752 Color: 1

Bin 4173: 1885 of cap free
Amount of items: 2
Items: 
Size: 573954 Color: 1
Size: 424162 Color: 0

Bin 4174: 1889 of cap free
Amount of items: 2
Items: 
Size: 778576 Color: 1
Size: 219536 Color: 0

Bin 4175: 1891 of cap free
Amount of items: 2
Items: 
Size: 604584 Color: 0
Size: 393526 Color: 1

Bin 4176: 1906 of cap free
Amount of items: 2
Items: 
Size: 721333 Color: 0
Size: 276762 Color: 1

Bin 4177: 1914 of cap free
Amount of items: 2
Items: 
Size: 605226 Color: 1
Size: 392861 Color: 0

Bin 4178: 1930 of cap free
Amount of items: 2
Items: 
Size: 613054 Color: 1
Size: 385017 Color: 0

Bin 4179: 1937 of cap free
Amount of items: 2
Items: 
Size: 567573 Color: 0
Size: 430491 Color: 1

Bin 4180: 1939 of cap free
Amount of items: 2
Items: 
Size: 646500 Color: 0
Size: 351562 Color: 1

Bin 4181: 1940 of cap free
Amount of items: 2
Items: 
Size: 539567 Color: 1
Size: 458494 Color: 0

Bin 4182: 1959 of cap free
Amount of items: 2
Items: 
Size: 576940 Color: 1
Size: 421102 Color: 0

Bin 4183: 1961 of cap free
Amount of items: 2
Items: 
Size: 758677 Color: 1
Size: 239363 Color: 0

Bin 4184: 1962 of cap free
Amount of items: 2
Items: 
Size: 668377 Color: 0
Size: 329662 Color: 1

Bin 4185: 1971 of cap free
Amount of items: 2
Items: 
Size: 542929 Color: 1
Size: 455101 Color: 0

Bin 4186: 1981 of cap free
Amount of items: 2
Items: 
Size: 590536 Color: 1
Size: 407484 Color: 0

Bin 4187: 1988 of cap free
Amount of items: 2
Items: 
Size: 642545 Color: 0
Size: 355468 Color: 1

Bin 4188: 1998 of cap free
Amount of items: 2
Items: 
Size: 775011 Color: 1
Size: 222992 Color: 0

Bin 4189: 2011 of cap free
Amount of items: 2
Items: 
Size: 621674 Color: 1
Size: 376316 Color: 0

Bin 4190: 2022 of cap free
Amount of items: 2
Items: 
Size: 548026 Color: 0
Size: 449953 Color: 1

Bin 4191: 2027 of cap free
Amount of items: 2
Items: 
Size: 610433 Color: 0
Size: 387541 Color: 1

Bin 4192: 2029 of cap free
Amount of items: 2
Items: 
Size: 576884 Color: 1
Size: 421088 Color: 0

Bin 4193: 2036 of cap free
Amount of items: 2
Items: 
Size: 628329 Color: 1
Size: 369636 Color: 0

Bin 4194: 2045 of cap free
Amount of items: 2
Items: 
Size: 561027 Color: 1
Size: 436929 Color: 0

Bin 4195: 2046 of cap free
Amount of items: 2
Items: 
Size: 576880 Color: 1
Size: 421075 Color: 0

Bin 4196: 2048 of cap free
Amount of items: 2
Items: 
Size: 549549 Color: 1
Size: 448404 Color: 0

Bin 4197: 2049 of cap free
Amount of items: 2
Items: 
Size: 767691 Color: 0
Size: 230261 Color: 1

Bin 4198: 2052 of cap free
Amount of items: 2
Items: 
Size: 727822 Color: 0
Size: 270127 Color: 1

Bin 4199: 2053 of cap free
Amount of items: 2
Items: 
Size: 752349 Color: 0
Size: 245599 Color: 1

Bin 4200: 2056 of cap free
Amount of items: 2
Items: 
Size: 570633 Color: 0
Size: 427312 Color: 1

Bin 4201: 2066 of cap free
Amount of items: 2
Items: 
Size: 649245 Color: 0
Size: 348690 Color: 1

Bin 4202: 2069 of cap free
Amount of items: 2
Items: 
Size: 560616 Color: 0
Size: 437316 Color: 1

Bin 4203: 2075 of cap free
Amount of items: 2
Items: 
Size: 596048 Color: 0
Size: 401878 Color: 1

Bin 4204: 2092 of cap free
Amount of items: 2
Items: 
Size: 528743 Color: 1
Size: 469166 Color: 0

Bin 4205: 2092 of cap free
Amount of items: 2
Items: 
Size: 646500 Color: 0
Size: 351409 Color: 1

Bin 4206: 2094 of cap free
Amount of items: 2
Items: 
Size: 536166 Color: 0
Size: 461741 Color: 1

Bin 4207: 2098 of cap free
Amount of items: 2
Items: 
Size: 616023 Color: 1
Size: 381880 Color: 0

Bin 4208: 2109 of cap free
Amount of items: 2
Items: 
Size: 786684 Color: 1
Size: 211208 Color: 0

Bin 4209: 2120 of cap free
Amount of items: 2
Items: 
Size: 547367 Color: 1
Size: 450514 Color: 0

Bin 4210: 2128 of cap free
Amount of items: 2
Items: 
Size: 691459 Color: 0
Size: 306414 Color: 1

Bin 4211: 2139 of cap free
Amount of items: 2
Items: 
Size: 721296 Color: 0
Size: 276566 Color: 1

Bin 4212: 2143 of cap free
Amount of items: 2
Items: 
Size: 532892 Color: 1
Size: 464966 Color: 0

Bin 4213: 2146 of cap free
Amount of items: 2
Items: 
Size: 668193 Color: 0
Size: 329662 Color: 1

Bin 4214: 2149 of cap free
Amount of items: 2
Items: 
Size: 738389 Color: 0
Size: 259463 Color: 1

Bin 4215: 2161 of cap free
Amount of items: 2
Items: 
Size: 723872 Color: 0
Size: 273968 Color: 1

Bin 4216: 2164 of cap free
Amount of items: 2
Items: 
Size: 581820 Color: 1
Size: 416017 Color: 0

Bin 4217: 2167 of cap free
Amount of items: 2
Items: 
Size: 798183 Color: 0
Size: 199651 Color: 1

Bin 4218: 2168 of cap free
Amount of items: 2
Items: 
Size: 538520 Color: 0
Size: 459313 Color: 1

Bin 4219: 2169 of cap free
Amount of items: 2
Items: 
Size: 593631 Color: 1
Size: 404201 Color: 0

Bin 4220: 2173 of cap free
Amount of items: 2
Items: 
Size: 571536 Color: 1
Size: 426292 Color: 0

Bin 4221: 2180 of cap free
Amount of items: 2
Items: 
Size: 685041 Color: 0
Size: 312780 Color: 1

Bin 4222: 2199 of cap free
Amount of items: 2
Items: 
Size: 604416 Color: 0
Size: 393386 Color: 1

Bin 4223: 2199 of cap free
Amount of items: 2
Items: 
Size: 649244 Color: 0
Size: 348558 Color: 1

Bin 4224: 2201 of cap free
Amount of items: 2
Items: 
Size: 670575 Color: 1
Size: 327225 Color: 0

Bin 4225: 2252 of cap free
Amount of items: 2
Items: 
Size: 615941 Color: 1
Size: 381808 Color: 0

Bin 4226: 2258 of cap free
Amount of items: 2
Items: 
Size: 505726 Color: 1
Size: 492017 Color: 0

Bin 4227: 2262 of cap free
Amount of items: 2
Items: 
Size: 532867 Color: 1
Size: 464872 Color: 0

Bin 4228: 2284 of cap free
Amount of items: 2
Items: 
Size: 646331 Color: 0
Size: 351386 Color: 1

Bin 4229: 2292 of cap free
Amount of items: 2
Items: 
Size: 604349 Color: 0
Size: 393360 Color: 1

Bin 4230: 2298 of cap free
Amount of items: 2
Items: 
Size: 539254 Color: 1
Size: 458449 Color: 0

Bin 4231: 2309 of cap free
Amount of items: 2
Items: 
Size: 558116 Color: 1
Size: 439576 Color: 0

Bin 4232: 2311 of cap free
Amount of items: 2
Items: 
Size: 581676 Color: 1
Size: 416014 Color: 0

Bin 4233: 2318 of cap free
Amount of items: 2
Items: 
Size: 646307 Color: 0
Size: 351376 Color: 1

Bin 4234: 2326 of cap free
Amount of items: 2
Items: 
Size: 670540 Color: 1
Size: 327135 Color: 0

Bin 4235: 2331 of cap free
Amount of items: 2
Items: 
Size: 575402 Color: 0
Size: 422268 Color: 1

Bin 4236: 2332 of cap free
Amount of items: 2
Items: 
Size: 587548 Color: 0
Size: 410121 Color: 1

Bin 4237: 2367 of cap free
Amount of items: 2
Items: 
Size: 548023 Color: 0
Size: 449611 Color: 1

Bin 4238: 2388 of cap free
Amount of items: 2
Items: 
Size: 560955 Color: 1
Size: 436658 Color: 0

Bin 4239: 2413 of cap free
Amount of items: 2
Items: 
Size: 508355 Color: 1
Size: 489233 Color: 0

Bin 4240: 2418 of cap free
Amount of items: 2
Items: 
Size: 593481 Color: 1
Size: 404102 Color: 0

Bin 4241: 2424 of cap free
Amount of items: 2
Items: 
Size: 552970 Color: 0
Size: 444607 Color: 1

Bin 4242: 2429 of cap free
Amount of items: 2
Items: 
Size: 752951 Color: 1
Size: 244621 Color: 0

Bin 4243: 2449 of cap free
Amount of items: 2
Items: 
Size: 532837 Color: 1
Size: 464715 Color: 0

Bin 4244: 2450 of cap free
Amount of items: 2
Items: 
Size: 706962 Color: 0
Size: 290589 Color: 1

Bin 4245: 2453 of cap free
Amount of items: 2
Items: 
Size: 615893 Color: 1
Size: 381655 Color: 0

Bin 4246: 2485 of cap free
Amount of items: 2
Items: 
Size: 552914 Color: 0
Size: 444602 Color: 1

Bin 4247: 2490 of cap free
Amount of items: 2
Items: 
Size: 774570 Color: 1
Size: 222941 Color: 0

Bin 4248: 2499 of cap free
Amount of items: 2
Items: 
Size: 676705 Color: 0
Size: 320797 Color: 1

Bin 4249: 2501 of cap free
Amount of items: 2
Items: 
Size: 711651 Color: 0
Size: 285849 Color: 1

Bin 4250: 2519 of cap free
Amount of items: 2
Items: 
Size: 621254 Color: 1
Size: 376228 Color: 0

Bin 4251: 2522 of cap free
Amount of items: 2
Items: 
Size: 660619 Color: 1
Size: 336860 Color: 0

Bin 4252: 2524 of cap free
Amount of items: 2
Items: 
Size: 508354 Color: 1
Size: 489123 Color: 0

Bin 4253: 2528 of cap free
Amount of items: 2
Items: 
Size: 604124 Color: 0
Size: 393349 Color: 1

Bin 4254: 2542 of cap free
Amount of items: 2
Items: 
Size: 673879 Color: 0
Size: 323580 Color: 1

Bin 4255: 2553 of cap free
Amount of items: 2
Items: 
Size: 519900 Color: 0
Size: 477548 Color: 1

Bin 4256: 2563 of cap free
Amount of items: 2
Items: 
Size: 575178 Color: 0
Size: 422260 Color: 1

Bin 4257: 2579 of cap free
Amount of items: 2
Items: 
Size: 576718 Color: 1
Size: 420704 Color: 0

Bin 4258: 2591 of cap free
Amount of items: 2
Items: 
Size: 735292 Color: 1
Size: 262118 Color: 0

Bin 4259: 2591 of cap free
Amount of items: 2
Items: 
Size: 533184 Color: 0
Size: 464226 Color: 1

Bin 4260: 2594 of cap free
Amount of items: 2
Items: 
Size: 760392 Color: 0
Size: 237015 Color: 1

Bin 4261: 2605 of cap free
Amount of items: 2
Items: 
Size: 508459 Color: 0
Size: 488937 Color: 1

Bin 4262: 2620 of cap free
Amount of items: 2
Items: 
Size: 705712 Color: 1
Size: 291669 Color: 0

Bin 4263: 2621 of cap free
Amount of items: 2
Items: 
Size: 723809 Color: 0
Size: 273571 Color: 1

Bin 4264: 2634 of cap free
Amount of items: 2
Items: 
Size: 727274 Color: 0
Size: 270093 Color: 1

Bin 4265: 2637 of cap free
Amount of items: 2
Items: 
Size: 635286 Color: 1
Size: 362078 Color: 0

Bin 4266: 2647 of cap free
Amount of items: 2
Items: 
Size: 519850 Color: 0
Size: 477504 Color: 1

Bin 4267: 2651 of cap free
Amount of items: 2
Items: 
Size: 780543 Color: 0
Size: 216807 Color: 1

Bin 4268: 2664 of cap free
Amount of items: 2
Items: 
Size: 533141 Color: 0
Size: 464196 Color: 1

Bin 4269: 2676 of cap free
Amount of items: 2
Items: 
Size: 679274 Color: 1
Size: 318051 Color: 0

Bin 4270: 2678 of cap free
Amount of items: 2
Items: 
Size: 543237 Color: 0
Size: 454086 Color: 1

Bin 4271: 2689 of cap free
Amount of items: 2
Items: 
Size: 519845 Color: 0
Size: 477467 Color: 1

Bin 4272: 2700 of cap free
Amount of items: 2
Items: 
Size: 519842 Color: 0
Size: 477459 Color: 1

Bin 4273: 2718 of cap free
Amount of items: 2
Items: 
Size: 705686 Color: 1
Size: 291597 Color: 0

Bin 4274: 2738 of cap free
Amount of items: 2
Items: 
Size: 587390 Color: 0
Size: 409873 Color: 1

Bin 4275: 2747 of cap free
Amount of items: 2
Items: 
Size: 703461 Color: 0
Size: 293793 Color: 1

Bin 4276: 2758 of cap free
Amount of items: 2
Items: 
Size: 723680 Color: 0
Size: 273563 Color: 1

Bin 4277: 2761 of cap free
Amount of items: 2
Items: 
Size: 646160 Color: 0
Size: 351080 Color: 1

Bin 4278: 2765 of cap free
Amount of items: 2
Items: 
Size: 515797 Color: 0
Size: 481439 Color: 1

Bin 4279: 2766 of cap free
Amount of items: 2
Items: 
Size: 739235 Color: 1
Size: 258000 Color: 0

Bin 4280: 2766 of cap free
Amount of items: 2
Items: 
Size: 612594 Color: 0
Size: 384641 Color: 1

Bin 4281: 2791 of cap free
Amount of items: 2
Items: 
Size: 724114 Color: 1
Size: 273096 Color: 0

Bin 4282: 2796 of cap free
Amount of items: 2
Items: 
Size: 612594 Color: 0
Size: 384611 Color: 1

Bin 4283: 2837 of cap free
Amount of items: 2
Items: 
Size: 575137 Color: 0
Size: 422027 Color: 1

Bin 4284: 2840 of cap free
Amount of items: 2
Items: 
Size: 646137 Color: 0
Size: 351024 Color: 1

Bin 4285: 2842 of cap free
Amount of items: 2
Items: 
Size: 604083 Color: 0
Size: 393076 Color: 1

Bin 4286: 2842 of cap free
Amount of items: 2
Items: 
Size: 597033 Color: 1
Size: 400126 Color: 0

Bin 4287: 2855 of cap free
Amount of items: 2
Items: 
Size: 515731 Color: 0
Size: 481415 Color: 1

Bin 4288: 2869 of cap free
Amount of items: 2
Items: 
Size: 560480 Color: 1
Size: 436652 Color: 0

Bin 4289: 2875 of cap free
Amount of items: 2
Items: 
Size: 635160 Color: 1
Size: 361966 Color: 0

Bin 4290: 2884 of cap free
Amount of items: 2
Items: 
Size: 521958 Color: 1
Size: 475159 Color: 0

Bin 4291: 2921 of cap free
Amount of items: 2
Items: 
Size: 711587 Color: 0
Size: 285493 Color: 1

Bin 4292: 2936 of cap free
Amount of items: 2
Items: 
Size: 773326 Color: 0
Size: 223739 Color: 1

Bin 4293: 2946 of cap free
Amount of items: 2
Items: 
Size: 595253 Color: 0
Size: 401802 Color: 1

Bin 4294: 2954 of cap free
Amount of items: 2
Items: 
Size: 740553 Color: 0
Size: 256494 Color: 1

Bin 4295: 2976 of cap free
Amount of items: 2
Items: 
Size: 646013 Color: 0
Size: 351012 Color: 1

Bin 4296: 2981 of cap free
Amount of items: 2
Items: 
Size: 587162 Color: 0
Size: 409858 Color: 1

Bin 4297: 2983 of cap free
Amount of items: 2
Items: 
Size: 634359 Color: 0
Size: 362659 Color: 1

Bin 4298: 3034 of cap free
Amount of items: 2
Items: 
Size: 542956 Color: 0
Size: 454011 Color: 1

Bin 4299: 3099 of cap free
Amount of items: 2
Items: 
Size: 644465 Color: 1
Size: 352437 Color: 0

Bin 4300: 3118 of cap free
Amount of items: 2
Items: 
Size: 645942 Color: 0
Size: 350941 Color: 1

Bin 4301: 3126 of cap free
Amount of items: 2
Items: 
Size: 797822 Color: 0
Size: 199053 Color: 1

Bin 4302: 3172 of cap free
Amount of items: 2
Items: 
Size: 754620 Color: 0
Size: 242209 Color: 1

Bin 4303: 3173 of cap free
Amount of items: 2
Items: 
Size: 498448 Color: 1
Size: 498380 Color: 0

Bin 4304: 3179 of cap free
Amount of items: 3
Items: 
Size: 367158 Color: 1
Size: 343458 Color: 0
Size: 286206 Color: 0

Bin 4305: 3190 of cap free
Amount of items: 2
Items: 
Size: 685529 Color: 1
Size: 311282 Color: 0

Bin 4306: 3196 of cap free
Amount of items: 2
Items: 
Size: 745734 Color: 0
Size: 251071 Color: 1

Bin 4307: 3238 of cap free
Amount of items: 2
Items: 
Size: 652511 Color: 0
Size: 344252 Color: 1

Bin 4308: 3242 of cap free
Amount of items: 2
Items: 
Size: 498396 Color: 1
Size: 498363 Color: 0

Bin 4309: 3250 of cap free
Amount of items: 2
Items: 
Size: 744971 Color: 1
Size: 251780 Color: 0

Bin 4310: 3278 of cap free
Amount of items: 2
Items: 
Size: 652507 Color: 0
Size: 344216 Color: 1

Bin 4311: 3308 of cap free
Amount of items: 2
Items: 
Size: 603864 Color: 1
Size: 392829 Color: 0

Bin 4312: 3323 of cap free
Amount of items: 2
Items: 
Size: 797410 Color: 1
Size: 199268 Color: 0

Bin 4313: 3325 of cap free
Amount of items: 2
Items: 
Size: 595198 Color: 0
Size: 401478 Color: 1

Bin 4314: 3354 of cap free
Amount of items: 2
Items: 
Size: 744961 Color: 1
Size: 251686 Color: 0

Bin 4315: 3361 of cap free
Amount of items: 2
Items: 
Size: 581633 Color: 1
Size: 415007 Color: 0

Bin 4316: 3381 of cap free
Amount of items: 2
Items: 
Size: 504342 Color: 0
Size: 492278 Color: 1

Bin 4317: 3396 of cap free
Amount of items: 2
Items: 
Size: 620086 Color: 0
Size: 376519 Color: 1

Bin 4318: 3449 of cap free
Amount of items: 2
Items: 
Size: 576558 Color: 1
Size: 419994 Color: 0

Bin 4319: 3478 of cap free
Amount of items: 2
Items: 
Size: 595182 Color: 0
Size: 401341 Color: 1

Bin 4320: 3544 of cap free
Amount of items: 2
Items: 
Size: 515346 Color: 1
Size: 481111 Color: 0

Bin 4321: 3545 of cap free
Amount of items: 2
Items: 
Size: 723532 Color: 0
Size: 272924 Color: 1

Bin 4322: 3574 of cap free
Amount of items: 2
Items: 
Size: 754618 Color: 0
Size: 241809 Color: 1

Bin 4323: 3574 of cap free
Amount of items: 2
Items: 
Size: 498218 Color: 0
Size: 498209 Color: 1

Bin 4324: 3585 of cap free
Amount of items: 2
Items: 
Size: 508358 Color: 0
Size: 488058 Color: 1

Bin 4325: 3587 of cap free
Amount of items: 2
Items: 
Size: 634560 Color: 1
Size: 361854 Color: 0

Bin 4326: 3599 of cap free
Amount of items: 2
Items: 
Size: 731593 Color: 1
Size: 264809 Color: 0

Bin 4327: 3606 of cap free
Amount of items: 2
Items: 
Size: 608974 Color: 0
Size: 387421 Color: 1

Bin 4328: 3611 of cap free
Amount of items: 2
Items: 
Size: 603580 Color: 1
Size: 392810 Color: 0

Bin 4329: 3613 of cap free
Amount of items: 2
Items: 
Size: 508355 Color: 0
Size: 488033 Color: 1

Bin 4330: 3623 of cap free
Amount of items: 2
Items: 
Size: 552876 Color: 0
Size: 443502 Color: 1

Bin 4331: 3626 of cap free
Amount of items: 2
Items: 
Size: 576420 Color: 1
Size: 419955 Color: 0

Bin 4332: 3639 of cap free
Amount of items: 2
Items: 
Size: 652465 Color: 0
Size: 343897 Color: 1

Bin 4333: 3640 of cap free
Amount of items: 2
Items: 
Size: 760357 Color: 0
Size: 236004 Color: 1

Bin 4334: 3641 of cap free
Amount of items: 2
Items: 
Size: 595098 Color: 0
Size: 401262 Color: 1

Bin 4335: 3648 of cap free
Amount of items: 2
Items: 
Size: 611351 Color: 1
Size: 385002 Color: 0

Bin 4336: 3693 of cap free
Amount of items: 2
Items: 
Size: 508286 Color: 0
Size: 488022 Color: 1

Bin 4337: 3720 of cap free
Amount of items: 2
Items: 
Size: 652430 Color: 0
Size: 343851 Color: 1

Bin 4338: 3729 of cap free
Amount of items: 2
Items: 
Size: 603507 Color: 1
Size: 392765 Color: 0

Bin 4339: 3737 of cap free
Amount of items: 2
Items: 
Size: 739211 Color: 1
Size: 257053 Color: 0

Bin 4340: 3741 of cap free
Amount of items: 2
Items: 
Size: 731467 Color: 1
Size: 264793 Color: 0

Bin 4341: 3742 of cap free
Amount of items: 2
Items: 
Size: 754557 Color: 0
Size: 241702 Color: 1

Bin 4342: 3753 of cap free
Amount of items: 2
Items: 
Size: 508274 Color: 0
Size: 487974 Color: 1

Bin 4343: 3798 of cap free
Amount of items: 2
Items: 
Size: 552810 Color: 0
Size: 443393 Color: 1

Bin 4344: 3816 of cap free
Amount of items: 2
Items: 
Size: 515001 Color: 0
Size: 481184 Color: 1

Bin 4345: 3821 of cap free
Amount of items: 2
Items: 
Size: 797006 Color: 1
Size: 199174 Color: 0

Bin 4346: 3847 of cap free
Amount of items: 2
Items: 
Size: 515074 Color: 1
Size: 481080 Color: 0

Bin 4347: 3927 of cap free
Amount of items: 2
Items: 
Size: 672644 Color: 0
Size: 323430 Color: 1

Bin 4348: 3972 of cap free
Amount of items: 2
Items: 
Size: 634311 Color: 0
Size: 361718 Color: 1

Bin 4349: 3975 of cap free
Amount of items: 2
Items: 
Size: 644365 Color: 1
Size: 351661 Color: 0

Bin 4350: 3986 of cap free
Amount of items: 2
Items: 
Size: 576108 Color: 1
Size: 419907 Color: 0

Bin 4351: 4002 of cap free
Amount of items: 2
Items: 
Size: 553253 Color: 1
Size: 442746 Color: 0

Bin 4352: 4062 of cap free
Amount of items: 2
Items: 
Size: 576105 Color: 1
Size: 419834 Color: 0

Bin 4353: 4098 of cap free
Amount of items: 2
Items: 
Size: 503855 Color: 0
Size: 492048 Color: 1

Bin 4354: 4120 of cap free
Amount of items: 2
Items: 
Size: 773817 Color: 1
Size: 222064 Color: 0

Bin 4355: 4155 of cap free
Amount of items: 2
Items: 
Size: 672427 Color: 0
Size: 323419 Color: 1

Bin 4356: 4166 of cap free
Amount of items: 2
Items: 
Size: 723472 Color: 1
Size: 272363 Color: 0

Bin 4357: 4186 of cap free
Amount of items: 2
Items: 
Size: 659138 Color: 1
Size: 336677 Color: 0

Bin 4358: 4232 of cap free
Amount of items: 2
Items: 
Size: 659130 Color: 1
Size: 336639 Color: 0

Bin 4359: 4264 of cap free
Amount of items: 2
Items: 
Size: 640808 Color: 0
Size: 354929 Color: 1

Bin 4360: 4266 of cap free
Amount of items: 2
Items: 
Size: 714737 Color: 1
Size: 280998 Color: 0

Bin 4361: 4337 of cap free
Amount of items: 2
Items: 
Size: 531061 Color: 1
Size: 464603 Color: 0

Bin 4362: 4350 of cap free
Amount of items: 2
Items: 
Size: 603002 Color: 1
Size: 392649 Color: 0

Bin 4363: 4365 of cap free
Amount of items: 2
Items: 
Size: 503768 Color: 0
Size: 491868 Color: 1

Bin 4364: 4376 of cap free
Amount of items: 2
Items: 
Size: 514958 Color: 0
Size: 480667 Color: 1

Bin 4365: 4486 of cap free
Amount of items: 2
Items: 
Size: 759522 Color: 0
Size: 235993 Color: 1

Bin 4366: 4490 of cap free
Amount of items: 2
Items: 
Size: 530916 Color: 1
Size: 464595 Color: 0

Bin 4367: 4493 of cap free
Amount of items: 2
Items: 
Size: 723429 Color: 1
Size: 272079 Color: 0

Bin 4368: 4496 of cap free
Amount of items: 2
Items: 
Size: 633727 Color: 1
Size: 361778 Color: 0

Bin 4369: 4511 of cap free
Amount of items: 2
Items: 
Size: 581454 Color: 1
Size: 414036 Color: 0

Bin 4370: 4519 of cap free
Amount of items: 2
Items: 
Size: 672128 Color: 0
Size: 323354 Color: 1

Bin 4371: 4528 of cap free
Amount of items: 2
Items: 
Size: 547069 Color: 1
Size: 448404 Color: 0

Bin 4372: 4538 of cap free
Amount of items: 2
Items: 
Size: 514902 Color: 0
Size: 480561 Color: 1

Bin 4373: 4546 of cap free
Amount of items: 2
Items: 
Size: 644251 Color: 1
Size: 351204 Color: 0

Bin 4374: 4620 of cap free
Amount of items: 2
Items: 
Size: 581421 Color: 1
Size: 413960 Color: 0

Bin 4375: 4624 of cap free
Amount of items: 2
Items: 
Size: 633699 Color: 1
Size: 361678 Color: 0

Bin 4376: 4717 of cap free
Amount of items: 2
Items: 
Size: 671973 Color: 0
Size: 323311 Color: 1

Bin 4377: 5010 of cap free
Amount of items: 2
Items: 
Size: 658366 Color: 1
Size: 336625 Color: 0

Bin 4378: 5014 of cap free
Amount of items: 2
Items: 
Size: 573657 Color: 0
Size: 421330 Color: 1

Bin 4379: 5087 of cap free
Amount of items: 2
Items: 
Size: 723368 Color: 1
Size: 271546 Color: 0

Bin 4380: 5119 of cap free
Amount of items: 2
Items: 
Size: 581031 Color: 1
Size: 413851 Color: 0

Bin 4381: 5157 of cap free
Amount of items: 2
Items: 
Size: 723342 Color: 1
Size: 271502 Color: 0

Bin 4382: 5214 of cap free
Amount of items: 2
Items: 
Size: 533077 Color: 0
Size: 461710 Color: 1

Bin 4383: 5288 of cap free
Amount of items: 2
Items: 
Size: 730194 Color: 0
Size: 264519 Color: 1

Bin 4384: 5366 of cap free
Amount of items: 2
Items: 
Size: 602108 Color: 1
Size: 392527 Color: 0

Bin 4385: 5398 of cap free
Amount of items: 2
Items: 
Size: 602078 Color: 1
Size: 392525 Color: 0

Bin 4386: 5433 of cap free
Amount of items: 2
Items: 
Size: 552691 Color: 0
Size: 441877 Color: 1

Bin 4387: 5463 of cap free
Amount of items: 2
Items: 
Size: 532910 Color: 0
Size: 461628 Color: 1

Bin 4388: 5650 of cap free
Amount of items: 2
Items: 
Size: 573369 Color: 0
Size: 420982 Color: 1

Bin 4389: 5653 of cap free
Amount of items: 2
Items: 
Size: 552671 Color: 0
Size: 441677 Color: 1

Bin 4390: 5671 of cap free
Amount of items: 2
Items: 
Size: 764926 Color: 0
Size: 229404 Color: 1

Bin 4391: 5723 of cap free
Amount of items: 2
Items: 
Size: 545892 Color: 1
Size: 448386 Color: 0

Bin 4392: 5739 of cap free
Amount of items: 2
Items: 
Size: 505142 Color: 1
Size: 489120 Color: 0

Bin 4393: 5809 of cap free
Amount of items: 2
Items: 
Size: 552623 Color: 0
Size: 441569 Color: 1

Bin 4394: 5862 of cap free
Amount of items: 2
Items: 
Size: 545859 Color: 1
Size: 448280 Color: 0

Bin 4395: 5886 of cap free
Amount of items: 2
Items: 
Size: 552611 Color: 0
Size: 441504 Color: 1

Bin 4396: 5901 of cap free
Amount of items: 2
Items: 
Size: 558100 Color: 1
Size: 436000 Color: 0

Bin 4397: 5907 of cap free
Amount of items: 2
Items: 
Size: 645712 Color: 0
Size: 348382 Color: 1

Bin 4398: 5970 of cap free
Amount of items: 2
Items: 
Size: 552568 Color: 0
Size: 441463 Color: 1

Bin 4399: 6176 of cap free
Amount of items: 2
Items: 
Size: 512994 Color: 1
Size: 480831 Color: 0

Bin 4400: 6307 of cap free
Amount of items: 2
Items: 
Size: 501889 Color: 0
Size: 491805 Color: 1

Bin 4401: 6329 of cap free
Amount of items: 2
Items: 
Size: 601174 Color: 1
Size: 392498 Color: 0

Bin 4402: 6719 of cap free
Amount of items: 2
Items: 
Size: 512675 Color: 1
Size: 480607 Color: 0

Bin 4403: 6992 of cap free
Amount of items: 2
Items: 
Size: 512455 Color: 1
Size: 480554 Color: 0

Bin 4404: 7105 of cap free
Amount of items: 2
Items: 
Size: 558032 Color: 1
Size: 434864 Color: 0

Bin 4405: 7126 of cap free
Amount of items: 2
Items: 
Size: 572999 Color: 0
Size: 419876 Color: 1

Bin 4406: 7141 of cap free
Amount of items: 2
Items: 
Size: 512346 Color: 1
Size: 480514 Color: 0

Bin 4407: 7269 of cap free
Amount of items: 2
Items: 
Size: 713802 Color: 1
Size: 278930 Color: 0

Bin 4408: 7412 of cap free
Amount of items: 2
Items: 
Size: 796956 Color: 1
Size: 195633 Color: 0

Bin 4409: 7528 of cap free
Amount of items: 2
Items: 
Size: 796952 Color: 1
Size: 195521 Color: 0

Bin 4410: 7594 of cap free
Amount of items: 2
Items: 
Size: 631408 Color: 1
Size: 360999 Color: 0

Bin 4411: 7826 of cap free
Amount of items: 2
Items: 
Size: 796827 Color: 1
Size: 195348 Color: 0

Bin 4412: 7989 of cap free
Amount of items: 2
Items: 
Size: 796782 Color: 1
Size: 195230 Color: 0

Bin 4413: 8019 of cap free
Amount of items: 2
Items: 
Size: 796627 Color: 0
Size: 195355 Color: 1

Bin 4414: 8023 of cap free
Amount of items: 2
Items: 
Size: 607450 Color: 0
Size: 384528 Color: 1

Bin 4415: 8091 of cap free
Amount of items: 2
Items: 
Size: 503196 Color: 1
Size: 488714 Color: 0

Bin 4416: 8457 of cap free
Amount of items: 2
Items: 
Size: 502934 Color: 1
Size: 488610 Color: 0

Bin 4417: 8490 of cap free
Amount of items: 2
Items: 
Size: 528393 Color: 1
Size: 463118 Color: 0

Bin 4418: 8699 of cap free
Amount of items: 2
Items: 
Size: 684921 Color: 0
Size: 306381 Color: 1

Bin 4419: 8840 of cap free
Amount of items: 2
Items: 
Size: 749688 Color: 0
Size: 241473 Color: 1

Bin 4420: 8845 of cap free
Amount of items: 2
Items: 
Size: 512091 Color: 1
Size: 479065 Color: 0

Bin 4421: 8900 of cap free
Amount of items: 2
Items: 
Size: 512086 Color: 1
Size: 479015 Color: 0

Bin 4422: 9529 of cap free
Amount of items: 2
Items: 
Size: 684272 Color: 0
Size: 306200 Color: 1

Bin 4423: 9697 of cap free
Amount of items: 2
Items: 
Size: 580512 Color: 0
Size: 409792 Color: 1

Bin 4424: 10278 of cap free
Amount of items: 2
Items: 
Size: 501854 Color: 0
Size: 487869 Color: 1

Bin 4425: 10438 of cap free
Amount of items: 2
Items: 
Size: 796528 Color: 0
Size: 193035 Color: 1

Bin 4426: 10489 of cap free
Amount of items: 2
Items: 
Size: 752895 Color: 1
Size: 236617 Color: 0

Bin 4427: 10867 of cap free
Amount of items: 2
Items: 
Size: 752778 Color: 1
Size: 236356 Color: 0

Bin 4428: 11122 of cap free
Amount of items: 2
Items: 
Size: 739203 Color: 1
Size: 249676 Color: 0

Bin 4429: 12204 of cap free
Amount of items: 2
Items: 
Size: 759415 Color: 0
Size: 228382 Color: 1

Bin 4430: 12495 of cap free
Amount of items: 2
Items: 
Size: 615540 Color: 0
Size: 371966 Color: 1

Bin 4431: 13266 of cap free
Amount of items: 2
Items: 
Size: 796318 Color: 0
Size: 190417 Color: 1

Bin 4432: 14361 of cap free
Amount of items: 2
Items: 
Size: 667904 Color: 0
Size: 317736 Color: 1

Bin 4433: 15263 of cap free
Amount of items: 2
Items: 
Size: 513354 Color: 0
Size: 471384 Color: 1

Bin 4434: 15313 of cap free
Amount of items: 2
Items: 
Size: 795940 Color: 0
Size: 188748 Color: 1

Bin 4435: 15505 of cap free
Amount of items: 2
Items: 
Size: 796083 Color: 1
Size: 188413 Color: 0

Bin 4436: 15985 of cap free
Amount of items: 2
Items: 
Size: 689673 Color: 1
Size: 294343 Color: 0

Bin 4437: 16234 of cap free
Amount of items: 2
Items: 
Size: 795802 Color: 1
Size: 187965 Color: 0

Bin 4438: 17681 of cap free
Amount of items: 2
Items: 
Size: 794732 Color: 1
Size: 187588 Color: 0

Bin 4439: 18339 of cap free
Amount of items: 2
Items: 
Size: 600692 Color: 1
Size: 380970 Color: 0

Bin 4440: 18495 of cap free
Amount of items: 2
Items: 
Size: 664613 Color: 0
Size: 316893 Color: 1

Bin 4441: 19179 of cap free
Amount of items: 2
Items: 
Size: 794584 Color: 0
Size: 186238 Color: 1

Bin 4442: 19654 of cap free
Amount of items: 2
Items: 
Size: 545645 Color: 1
Size: 434702 Color: 0

Bin 4443: 19776 of cap free
Amount of items: 2
Items: 
Size: 792720 Color: 1
Size: 187505 Color: 0

Bin 4444: 20464 of cap free
Amount of items: 2
Items: 
Size: 545608 Color: 1
Size: 433929 Color: 0

Bin 4445: 22184 of cap free
Amount of items: 2
Items: 
Size: 794161 Color: 0
Size: 183656 Color: 1

Bin 4446: 23478 of cap free
Amount of items: 2
Items: 
Size: 665517 Color: 1
Size: 311006 Color: 0

Bin 4447: 25678 of cap free
Amount of items: 2
Items: 
Size: 790174 Color: 1
Size: 184149 Color: 0

Bin 4448: 26912 of cap free
Amount of items: 2
Items: 
Size: 656502 Color: 0
Size: 316587 Color: 1

Bin 4449: 27714 of cap free
Amount of items: 2
Items: 
Size: 542065 Color: 1
Size: 430222 Color: 0

Bin 4450: 27887 of cap free
Amount of items: 2
Items: 
Size: 793495 Color: 0
Size: 178619 Color: 1

Bin 4451: 29706 of cap free
Amount of items: 2
Items: 
Size: 793124 Color: 0
Size: 177171 Color: 1

Bin 4452: 30836 of cap free
Amount of items: 2
Items: 
Size: 658179 Color: 1
Size: 310986 Color: 0

Bin 4453: 32525 of cap free
Amount of items: 2
Items: 
Size: 786304 Color: 1
Size: 181172 Color: 0

Bin 4454: 34191 of cap free
Amount of items: 2
Items: 
Size: 792581 Color: 0
Size: 173229 Color: 1

Bin 4455: 35589 of cap free
Amount of items: 2
Items: 
Size: 531469 Color: 0
Size: 432943 Color: 1

Bin 4456: 36420 of cap free
Amount of items: 2
Items: 
Size: 784293 Color: 1
Size: 179288 Color: 0

Bin 4457: 39284 of cap free
Amount of items: 2
Items: 
Size: 791865 Color: 0
Size: 168852 Color: 1

Bin 4458: 40412 of cap free
Amount of items: 2
Items: 
Size: 784254 Color: 1
Size: 175335 Color: 0

Bin 4459: 40775 of cap free
Amount of items: 2
Items: 
Size: 656369 Color: 0
Size: 302857 Color: 1

Bin 4460: 42425 of cap free
Amount of items: 2
Items: 
Size: 788818 Color: 0
Size: 168758 Color: 1

Bin 4461: 44015 of cap free
Amount of items: 2
Items: 
Size: 784048 Color: 1
Size: 171938 Color: 0

Bin 4462: 44262 of cap free
Amount of items: 2
Items: 
Size: 787366 Color: 0
Size: 168373 Color: 1

Bin 4463: 46097 of cap free
Amount of items: 2
Items: 
Size: 781972 Color: 1
Size: 171932 Color: 0

Bin 4464: 55476 of cap free
Amount of items: 2
Items: 
Size: 787204 Color: 0
Size: 157321 Color: 1

Bin 4465: 64531 of cap free
Amount of items: 2
Items: 
Size: 778380 Color: 1
Size: 157090 Color: 0

Bin 4466: 65263 of cap free
Amount of items: 2
Items: 
Size: 777940 Color: 1
Size: 156798 Color: 0

Bin 4467: 73227 of cap free
Amount of items: 2
Items: 
Size: 778517 Color: 0
Size: 148257 Color: 1

Bin 4468: 82466 of cap free
Amount of items: 2
Items: 
Size: 768391 Color: 1
Size: 149144 Color: 0

Bin 4469: 84562 of cap free
Amount of items: 2
Items: 
Size: 772126 Color: 0
Size: 143313 Color: 1

Bin 4470: 235649 of cap free
Amount of items: 1
Items: 
Size: 764352 Color: 1

Bin 4471: 240618 of cap free
Amount of items: 1
Items: 
Size: 759383 Color: 0

Bin 4472: 250426 of cap free
Amount of items: 1
Items: 
Size: 749575 Color: 0

Bin 4473: 255039 of cap free
Amount of items: 1
Items: 
Size: 744962 Color: 0

Bin 4474: 255283 of cap free
Amount of items: 1
Items: 
Size: 744718 Color: 0

Total size: 4469576565
Total free space: 4427909

