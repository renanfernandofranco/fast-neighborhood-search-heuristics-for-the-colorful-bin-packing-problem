Capicity Bin: 1000001
Lower Bound: 229

Bins used: 231
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 678413 Color: 7
Size: 189996 Color: 8
Size: 131592 Color: 3

Bin 2: 1 of cap free
Amount of items: 3
Items: 
Size: 724853 Color: 3
Size: 171202 Color: 0
Size: 103945 Color: 6

Bin 3: 1 of cap free
Amount of items: 3
Items: 
Size: 716758 Color: 11
Size: 176170 Color: 11
Size: 107072 Color: 7

Bin 4: 3 of cap free
Amount of items: 3
Items: 
Size: 789106 Color: 2
Size: 105850 Color: 0
Size: 105042 Color: 9

Bin 5: 4 of cap free
Amount of items: 3
Items: 
Size: 761212 Color: 7
Size: 135075 Color: 13
Size: 103710 Color: 6

Bin 6: 5 of cap free
Amount of items: 2
Items: 
Size: 726519 Color: 14
Size: 273477 Color: 17

Bin 7: 5 of cap free
Amount of items: 2
Items: 
Size: 757309 Color: 8
Size: 242687 Color: 3

Bin 8: 5 of cap free
Amount of items: 3
Items: 
Size: 509347 Color: 19
Size: 339855 Color: 4
Size: 150794 Color: 1

Bin 9: 8 of cap free
Amount of items: 3
Items: 
Size: 654093 Color: 1
Size: 188435 Color: 0
Size: 157465 Color: 12

Bin 10: 9 of cap free
Amount of items: 3
Items: 
Size: 763704 Color: 0
Size: 122919 Color: 14
Size: 113369 Color: 16

Bin 11: 11 of cap free
Amount of items: 3
Items: 
Size: 689651 Color: 4
Size: 186664 Color: 18
Size: 123675 Color: 5

Bin 12: 11 of cap free
Amount of items: 3
Items: 
Size: 743394 Color: 2
Size: 140663 Color: 11
Size: 115933 Color: 13

Bin 13: 14 of cap free
Amount of items: 3
Items: 
Size: 774069 Color: 13
Size: 125116 Color: 7
Size: 100802 Color: 12

Bin 14: 14 of cap free
Amount of items: 4
Items: 
Size: 649241 Color: 15
Size: 132592 Color: 8
Size: 115983 Color: 6
Size: 102171 Color: 1

Bin 15: 15 of cap free
Amount of items: 2
Items: 
Size: 724276 Color: 13
Size: 275710 Color: 7

Bin 16: 15 of cap free
Amount of items: 3
Items: 
Size: 661581 Color: 3
Size: 201440 Color: 11
Size: 136965 Color: 12

Bin 17: 16 of cap free
Amount of items: 3
Items: 
Size: 647681 Color: 2
Size: 239525 Color: 10
Size: 112779 Color: 8

Bin 18: 16 of cap free
Amount of items: 3
Items: 
Size: 550502 Color: 15
Size: 292959 Color: 13
Size: 156524 Color: 7

Bin 19: 17 of cap free
Amount of items: 3
Items: 
Size: 750146 Color: 5
Size: 142189 Color: 4
Size: 107649 Color: 19

Bin 20: 18 of cap free
Amount of items: 3
Items: 
Size: 726479 Color: 18
Size: 165806 Color: 19
Size: 107698 Color: 9

Bin 21: 19 of cap free
Amount of items: 3
Items: 
Size: 739648 Color: 6
Size: 149298 Color: 18
Size: 111036 Color: 6

Bin 22: 25 of cap free
Amount of items: 2
Items: 
Size: 661704 Color: 7
Size: 338272 Color: 11

Bin 23: 26 of cap free
Amount of items: 3
Items: 
Size: 754631 Color: 15
Size: 143023 Color: 6
Size: 102321 Color: 8

Bin 24: 28 of cap free
Amount of items: 3
Items: 
Size: 638107 Color: 12
Size: 240035 Color: 2
Size: 121831 Color: 9

Bin 25: 29 of cap free
Amount of items: 3
Items: 
Size: 451477 Color: 0
Size: 432176 Color: 16
Size: 116319 Color: 14

Bin 26: 32 of cap free
Amount of items: 3
Items: 
Size: 707122 Color: 15
Size: 159032 Color: 4
Size: 133815 Color: 18

Bin 27: 40 of cap free
Amount of items: 2
Items: 
Size: 655219 Color: 6
Size: 344742 Color: 8

Bin 28: 41 of cap free
Amount of items: 3
Items: 
Size: 364084 Color: 16
Size: 323795 Color: 8
Size: 312081 Color: 15

Bin 29: 41 of cap free
Amount of items: 3
Items: 
Size: 550749 Color: 0
Size: 292662 Color: 15
Size: 156549 Color: 17

Bin 30: 41 of cap free
Amount of items: 2
Items: 
Size: 621120 Color: 7
Size: 378840 Color: 18

Bin 31: 46 of cap free
Amount of items: 2
Items: 
Size: 568788 Color: 14
Size: 431167 Color: 2

Bin 32: 47 of cap free
Amount of items: 2
Items: 
Size: 724778 Color: 3
Size: 275176 Color: 11

Bin 33: 48 of cap free
Amount of items: 3
Items: 
Size: 651683 Color: 16
Size: 183660 Color: 2
Size: 164610 Color: 19

Bin 34: 48 of cap free
Amount of items: 2
Items: 
Size: 569504 Color: 13
Size: 430449 Color: 7

Bin 35: 52 of cap free
Amount of items: 2
Items: 
Size: 757905 Color: 19
Size: 242044 Color: 17

Bin 36: 53 of cap free
Amount of items: 3
Items: 
Size: 651500 Color: 7
Size: 223649 Color: 0
Size: 124799 Color: 1

Bin 37: 61 of cap free
Amount of items: 3
Items: 
Size: 638760 Color: 5
Size: 207994 Color: 16
Size: 153186 Color: 15

Bin 38: 63 of cap free
Amount of items: 2
Items: 
Size: 773328 Color: 9
Size: 226610 Color: 4

Bin 39: 65 of cap free
Amount of items: 3
Items: 
Size: 630525 Color: 4
Size: 198515 Color: 1
Size: 170896 Color: 11

Bin 40: 68 of cap free
Amount of items: 2
Items: 
Size: 583244 Color: 1
Size: 416689 Color: 6

Bin 41: 75 of cap free
Amount of items: 3
Items: 
Size: 681445 Color: 19
Size: 188589 Color: 6
Size: 129892 Color: 12

Bin 42: 78 of cap free
Amount of items: 2
Items: 
Size: 685217 Color: 18
Size: 314706 Color: 15

Bin 43: 79 of cap free
Amount of items: 2
Items: 
Size: 521777 Color: 11
Size: 478145 Color: 19

Bin 44: 81 of cap free
Amount of items: 3
Items: 
Size: 654762 Color: 0
Size: 191058 Color: 12
Size: 154100 Color: 9

Bin 45: 93 of cap free
Amount of items: 3
Items: 
Size: 719803 Color: 13
Size: 171033 Color: 4
Size: 109072 Color: 0

Bin 46: 96 of cap free
Amount of items: 3
Items: 
Size: 713503 Color: 2
Size: 182388 Color: 10
Size: 104014 Color: 4

Bin 47: 96 of cap free
Amount of items: 3
Items: 
Size: 764970 Color: 16
Size: 117485 Color: 10
Size: 117450 Color: 9

Bin 48: 101 of cap free
Amount of items: 2
Items: 
Size: 723175 Color: 7
Size: 276725 Color: 16

Bin 49: 105 of cap free
Amount of items: 2
Items: 
Size: 559802 Color: 14
Size: 440094 Color: 2

Bin 50: 106 of cap free
Amount of items: 3
Items: 
Size: 737942 Color: 3
Size: 151115 Color: 14
Size: 110838 Color: 18

Bin 51: 107 of cap free
Amount of items: 2
Items: 
Size: 684750 Color: 2
Size: 315144 Color: 0

Bin 52: 112 of cap free
Amount of items: 2
Items: 
Size: 733352 Color: 6
Size: 266537 Color: 1

Bin 53: 121 of cap free
Amount of items: 2
Items: 
Size: 592128 Color: 8
Size: 407752 Color: 16

Bin 54: 126 of cap free
Amount of items: 2
Items: 
Size: 560213 Color: 11
Size: 439662 Color: 1

Bin 55: 133 of cap free
Amount of items: 2
Items: 
Size: 605572 Color: 16
Size: 394296 Color: 5

Bin 56: 134 of cap free
Amount of items: 2
Items: 
Size: 749895 Color: 9
Size: 249972 Color: 8

Bin 57: 141 of cap free
Amount of items: 3
Items: 
Size: 726316 Color: 2
Size: 164982 Color: 19
Size: 108562 Color: 0

Bin 58: 152 of cap free
Amount of items: 3
Items: 
Size: 648701 Color: 16
Size: 201240 Color: 5
Size: 149908 Color: 10

Bin 59: 153 of cap free
Amount of items: 2
Items: 
Size: 540337 Color: 5
Size: 459511 Color: 17

Bin 60: 154 of cap free
Amount of items: 2
Items: 
Size: 627638 Color: 13
Size: 372209 Color: 18

Bin 61: 158 of cap free
Amount of items: 2
Items: 
Size: 504382 Color: 19
Size: 495461 Color: 14

Bin 62: 165 of cap free
Amount of items: 2
Items: 
Size: 535227 Color: 19
Size: 464609 Color: 0

Bin 63: 167 of cap free
Amount of items: 2
Items: 
Size: 593760 Color: 7
Size: 406074 Color: 13

Bin 64: 195 of cap free
Amount of items: 3
Items: 
Size: 349957 Color: 4
Size: 330170 Color: 5
Size: 319679 Color: 1

Bin 65: 198 of cap free
Amount of items: 2
Items: 
Size: 652015 Color: 11
Size: 347788 Color: 1

Bin 66: 206 of cap free
Amount of items: 2
Items: 
Size: 656840 Color: 8
Size: 342955 Color: 15

Bin 67: 207 of cap free
Amount of items: 2
Items: 
Size: 653661 Color: 7
Size: 346133 Color: 3

Bin 68: 208 of cap free
Amount of items: 2
Items: 
Size: 771431 Color: 3
Size: 228362 Color: 17

Bin 69: 212 of cap free
Amount of items: 2
Items: 
Size: 584130 Color: 17
Size: 415659 Color: 15

Bin 70: 220 of cap free
Amount of items: 3
Items: 
Size: 723712 Color: 14
Size: 140366 Color: 1
Size: 135703 Color: 13

Bin 71: 259 of cap free
Amount of items: 2
Items: 
Size: 767456 Color: 11
Size: 232286 Color: 8

Bin 72: 259 of cap free
Amount of items: 2
Items: 
Size: 615038 Color: 2
Size: 384704 Color: 1

Bin 73: 263 of cap free
Amount of items: 2
Items: 
Size: 513698 Color: 14
Size: 486040 Color: 18

Bin 74: 276 of cap free
Amount of items: 2
Items: 
Size: 702186 Color: 19
Size: 297539 Color: 8

Bin 75: 276 of cap free
Amount of items: 2
Items: 
Size: 691728 Color: 13
Size: 307997 Color: 9

Bin 76: 283 of cap free
Amount of items: 2
Items: 
Size: 620598 Color: 3
Size: 379120 Color: 0

Bin 77: 290 of cap free
Amount of items: 3
Items: 
Size: 461377 Color: 10
Size: 392363 Color: 13
Size: 145971 Color: 6

Bin 78: 293 of cap free
Amount of items: 2
Items: 
Size: 534443 Color: 12
Size: 465265 Color: 2

Bin 79: 301 of cap free
Amount of items: 2
Items: 
Size: 771744 Color: 6
Size: 227956 Color: 16

Bin 80: 302 of cap free
Amount of items: 2
Items: 
Size: 662343 Color: 6
Size: 337356 Color: 4

Bin 81: 305 of cap free
Amount of items: 2
Items: 
Size: 730113 Color: 8
Size: 269583 Color: 16

Bin 82: 311 of cap free
Amount of items: 2
Items: 
Size: 520095 Color: 16
Size: 479595 Color: 9

Bin 83: 325 of cap free
Amount of items: 2
Items: 
Size: 526178 Color: 17
Size: 473498 Color: 19

Bin 84: 343 of cap free
Amount of items: 2
Items: 
Size: 541765 Color: 1
Size: 457893 Color: 7

Bin 85: 354 of cap free
Amount of items: 2
Items: 
Size: 624568 Color: 4
Size: 375079 Color: 2

Bin 86: 355 of cap free
Amount of items: 3
Items: 
Size: 521953 Color: 12
Size: 302526 Color: 7
Size: 175167 Color: 4

Bin 87: 359 of cap free
Amount of items: 2
Items: 
Size: 761895 Color: 15
Size: 237747 Color: 12

Bin 88: 368 of cap free
Amount of items: 2
Items: 
Size: 695883 Color: 15
Size: 303750 Color: 16

Bin 89: 372 of cap free
Amount of items: 2
Items: 
Size: 738083 Color: 4
Size: 261546 Color: 5

Bin 90: 380 of cap free
Amount of items: 2
Items: 
Size: 719943 Color: 0
Size: 279678 Color: 5

Bin 91: 386 of cap free
Amount of items: 2
Items: 
Size: 554640 Color: 9
Size: 444975 Color: 16

Bin 92: 417 of cap free
Amount of items: 2
Items: 
Size: 670316 Color: 14
Size: 329268 Color: 12

Bin 93: 419 of cap free
Amount of items: 2
Items: 
Size: 590089 Color: 19
Size: 409493 Color: 13

Bin 94: 455 of cap free
Amount of items: 2
Items: 
Size: 788843 Color: 4
Size: 210703 Color: 9

Bin 95: 480 of cap free
Amount of items: 2
Items: 
Size: 783577 Color: 12
Size: 215944 Color: 1

Bin 96: 480 of cap free
Amount of items: 2
Items: 
Size: 796414 Color: 15
Size: 203107 Color: 7

Bin 97: 502 of cap free
Amount of items: 2
Items: 
Size: 617328 Color: 1
Size: 382171 Color: 7

Bin 98: 503 of cap free
Amount of items: 2
Items: 
Size: 654589 Color: 19
Size: 344909 Color: 3

Bin 99: 514 of cap free
Amount of items: 2
Items: 
Size: 512370 Color: 11
Size: 487117 Color: 10

Bin 100: 523 of cap free
Amount of items: 2
Items: 
Size: 539094 Color: 1
Size: 460384 Color: 9

Bin 101: 537 of cap free
Amount of items: 2
Items: 
Size: 506381 Color: 18
Size: 493083 Color: 2

Bin 102: 542 of cap free
Amount of items: 2
Items: 
Size: 634814 Color: 0
Size: 364645 Color: 7

Bin 103: 578 of cap free
Amount of items: 2
Items: 
Size: 571657 Color: 4
Size: 427766 Color: 8

Bin 104: 582 of cap free
Amount of items: 2
Items: 
Size: 610185 Color: 11
Size: 389234 Color: 7

Bin 105: 619 of cap free
Amount of items: 2
Items: 
Size: 698272 Color: 1
Size: 301110 Color: 5

Bin 106: 630 of cap free
Amount of items: 2
Items: 
Size: 514359 Color: 10
Size: 485012 Color: 17

Bin 107: 634 of cap free
Amount of items: 2
Items: 
Size: 663926 Color: 0
Size: 335441 Color: 16

Bin 108: 661 of cap free
Amount of items: 2
Items: 
Size: 669340 Color: 5
Size: 330000 Color: 13

Bin 109: 682 of cap free
Amount of items: 3
Items: 
Size: 710979 Color: 5
Size: 182233 Color: 15
Size: 106107 Color: 11

Bin 110: 697 of cap free
Amount of items: 2
Items: 
Size: 532245 Color: 0
Size: 467059 Color: 3

Bin 111: 702 of cap free
Amount of items: 2
Items: 
Size: 606433 Color: 2
Size: 392866 Color: 19

Bin 112: 740 of cap free
Amount of items: 2
Items: 
Size: 738742 Color: 7
Size: 260519 Color: 3

Bin 113: 745 of cap free
Amount of items: 2
Items: 
Size: 597331 Color: 13
Size: 401925 Color: 1

Bin 114: 746 of cap free
Amount of items: 2
Items: 
Size: 528773 Color: 3
Size: 470482 Color: 9

Bin 115: 748 of cap free
Amount of items: 2
Items: 
Size: 507296 Color: 1
Size: 491957 Color: 17

Bin 116: 757 of cap free
Amount of items: 2
Items: 
Size: 585960 Color: 4
Size: 413284 Color: 6

Bin 117: 799 of cap free
Amount of items: 2
Items: 
Size: 539063 Color: 2
Size: 460139 Color: 9

Bin 118: 802 of cap free
Amount of items: 2
Items: 
Size: 712180 Color: 13
Size: 287019 Color: 6

Bin 119: 894 of cap free
Amount of items: 2
Items: 
Size: 685419 Color: 19
Size: 313688 Color: 14

Bin 120: 917 of cap free
Amount of items: 2
Items: 
Size: 616941 Color: 5
Size: 382143 Color: 13

Bin 121: 940 of cap free
Amount of items: 2
Items: 
Size: 641649 Color: 18
Size: 357412 Color: 8

Bin 122: 941 of cap free
Amount of items: 2
Items: 
Size: 797328 Color: 18
Size: 201732 Color: 4

Bin 123: 948 of cap free
Amount of items: 2
Items: 
Size: 730897 Color: 8
Size: 268156 Color: 15

Bin 124: 956 of cap free
Amount of items: 2
Items: 
Size: 580203 Color: 17
Size: 418842 Color: 9

Bin 125: 958 of cap free
Amount of items: 3
Items: 
Size: 370375 Color: 16
Size: 336270 Color: 10
Size: 292398 Color: 3

Bin 126: 1016 of cap free
Amount of items: 2
Items: 
Size: 740844 Color: 16
Size: 258141 Color: 19

Bin 127: 1054 of cap free
Amount of items: 2
Items: 
Size: 687178 Color: 16
Size: 311769 Color: 19

Bin 128: 1095 of cap free
Amount of items: 2
Items: 
Size: 728274 Color: 5
Size: 270632 Color: 8

Bin 129: 1173 of cap free
Amount of items: 2
Items: 
Size: 510102 Color: 10
Size: 488726 Color: 0

Bin 130: 1193 of cap free
Amount of items: 2
Items: 
Size: 780739 Color: 3
Size: 218069 Color: 8

Bin 131: 1201 of cap free
Amount of items: 2
Items: 
Size: 762968 Color: 18
Size: 235832 Color: 12

Bin 132: 1207 of cap free
Amount of items: 2
Items: 
Size: 565736 Color: 8
Size: 433058 Color: 10

Bin 133: 1213 of cap free
Amount of items: 2
Items: 
Size: 790472 Color: 18
Size: 208316 Color: 15

Bin 134: 1219 of cap free
Amount of items: 2
Items: 
Size: 552067 Color: 11
Size: 446715 Color: 14

Bin 135: 1231 of cap free
Amount of items: 2
Items: 
Size: 787233 Color: 0
Size: 211537 Color: 15

Bin 136: 1317 of cap free
Amount of items: 2
Items: 
Size: 743469 Color: 10
Size: 255215 Color: 0

Bin 137: 1357 of cap free
Amount of items: 2
Items: 
Size: 677669 Color: 0
Size: 320975 Color: 5

Bin 138: 1404 of cap free
Amount of items: 2
Items: 
Size: 525464 Color: 18
Size: 473133 Color: 4

Bin 139: 1551 of cap free
Amount of items: 2
Items: 
Size: 587350 Color: 7
Size: 411100 Color: 14

Bin 140: 1592 of cap free
Amount of items: 2
Items: 
Size: 609676 Color: 3
Size: 388733 Color: 11

Bin 141: 1626 of cap free
Amount of items: 2
Items: 
Size: 540658 Color: 12
Size: 457717 Color: 19

Bin 142: 1664 of cap free
Amount of items: 2
Items: 
Size: 634504 Color: 18
Size: 363833 Color: 12

Bin 143: 1679 of cap free
Amount of items: 2
Items: 
Size: 653616 Color: 6
Size: 344706 Color: 18

Bin 144: 1702 of cap free
Amount of items: 2
Items: 
Size: 718031 Color: 18
Size: 280268 Color: 10

Bin 145: 1707 of cap free
Amount of items: 2
Items: 
Size: 765453 Color: 5
Size: 232841 Color: 16

Bin 146: 1724 of cap free
Amount of items: 3
Items: 
Size: 727861 Color: 14
Size: 151744 Color: 10
Size: 118672 Color: 18

Bin 147: 1768 of cap free
Amount of items: 2
Items: 
Size: 516569 Color: 0
Size: 481664 Color: 9

Bin 148: 1838 of cap free
Amount of items: 2
Items: 
Size: 693421 Color: 4
Size: 304742 Color: 5

Bin 149: 1870 of cap free
Amount of items: 2
Items: 
Size: 605903 Color: 4
Size: 392228 Color: 11

Bin 150: 1881 of cap free
Amount of items: 3
Items: 
Size: 645201 Color: 0
Size: 196787 Color: 15
Size: 156132 Color: 19

Bin 151: 1891 of cap free
Amount of items: 2
Items: 
Size: 686954 Color: 15
Size: 311156 Color: 2

Bin 152: 1993 of cap free
Amount of items: 2
Items: 
Size: 584833 Color: 9
Size: 413175 Color: 5

Bin 153: 2004 of cap free
Amount of items: 2
Items: 
Size: 644812 Color: 12
Size: 353185 Color: 0

Bin 154: 2009 of cap free
Amount of items: 2
Items: 
Size: 639361 Color: 19
Size: 358631 Color: 12

Bin 155: 2311 of cap free
Amount of items: 2
Items: 
Size: 727342 Color: 9
Size: 270348 Color: 5

Bin 156: 2314 of cap free
Amount of items: 2
Items: 
Size: 561551 Color: 15
Size: 436136 Color: 9

Bin 157: 2331 of cap free
Amount of items: 2
Items: 
Size: 712188 Color: 6
Size: 285482 Color: 17

Bin 158: 2394 of cap free
Amount of items: 2
Items: 
Size: 510045 Color: 17
Size: 487562 Color: 11

Bin 159: 2459 of cap free
Amount of items: 2
Items: 
Size: 662901 Color: 10
Size: 334641 Color: 7

Bin 160: 2471 of cap free
Amount of items: 2
Items: 
Size: 565595 Color: 6
Size: 431935 Color: 7

Bin 161: 2564 of cap free
Amount of items: 2
Items: 
Size: 587048 Color: 0
Size: 410389 Color: 3

Bin 162: 2647 of cap free
Amount of items: 2
Items: 
Size: 528108 Color: 1
Size: 469246 Color: 5

Bin 163: 2786 of cap free
Amount of items: 2
Items: 
Size: 596667 Color: 1
Size: 400548 Color: 9

Bin 164: 2825 of cap free
Amount of items: 2
Items: 
Size: 505548 Color: 13
Size: 491628 Color: 2

Bin 165: 2869 of cap free
Amount of items: 2
Items: 
Size: 751299 Color: 11
Size: 245833 Color: 12

Bin 166: 2991 of cap free
Amount of items: 2
Items: 
Size: 742949 Color: 9
Size: 254061 Color: 10

Bin 167: 3000 of cap free
Amount of items: 2
Items: 
Size: 624147 Color: 17
Size: 372854 Color: 4

Bin 168: 3029 of cap free
Amount of items: 2
Items: 
Size: 765427 Color: 6
Size: 231545 Color: 14

Bin 169: 3226 of cap free
Amount of items: 2
Items: 
Size: 739623 Color: 3
Size: 257152 Color: 11

Bin 170: 3250 of cap free
Amount of items: 2
Items: 
Size: 683447 Color: 16
Size: 313304 Color: 1

Bin 171: 3271 of cap free
Amount of items: 2
Items: 
Size: 772437 Color: 4
Size: 224293 Color: 18

Bin 172: 3273 of cap free
Amount of items: 2
Items: 
Size: 739613 Color: 1
Size: 257115 Color: 0

Bin 173: 3327 of cap free
Amount of items: 2
Items: 
Size: 665143 Color: 7
Size: 331531 Color: 12

Bin 174: 3360 of cap free
Amount of items: 2
Items: 
Size: 616820 Color: 19
Size: 379821 Color: 13

Bin 175: 3482 of cap free
Amount of items: 2
Items: 
Size: 569107 Color: 4
Size: 427412 Color: 8

Bin 176: 3491 of cap free
Amount of items: 2
Items: 
Size: 575227 Color: 13
Size: 421283 Color: 19

Bin 177: 3570 of cap free
Amount of items: 2
Items: 
Size: 575216 Color: 0
Size: 421215 Color: 6

Bin 178: 3608 of cap free
Amount of items: 2
Items: 
Size: 596310 Color: 12
Size: 400083 Color: 8

Bin 179: 3857 of cap free
Amount of items: 2
Items: 
Size: 560390 Color: 6
Size: 435754 Color: 5

Bin 180: 3911 of cap free
Amount of items: 2
Items: 
Size: 655350 Color: 14
Size: 340740 Color: 17

Bin 181: 3975 of cap free
Amount of items: 2
Items: 
Size: 553395 Color: 1
Size: 442631 Color: 0

Bin 182: 4067 of cap free
Amount of items: 2
Items: 
Size: 726644 Color: 16
Size: 269290 Color: 8

Bin 183: 4069 of cap free
Amount of items: 2
Items: 
Size: 510008 Color: 5
Size: 485924 Color: 12

Bin 184: 4182 of cap free
Amount of items: 2
Items: 
Size: 560171 Color: 3
Size: 435648 Color: 18

Bin 185: 4327 of cap free
Amount of items: 2
Items: 
Size: 515139 Color: 18
Size: 480535 Color: 4

Bin 186: 4331 of cap free
Amount of items: 2
Items: 
Size: 616705 Color: 1
Size: 378965 Color: 9

Bin 187: 4878 of cap free
Amount of items: 2
Items: 
Size: 595722 Color: 16
Size: 399401 Color: 0

Bin 188: 5134 of cap free
Amount of items: 2
Items: 
Size: 631042 Color: 9
Size: 363825 Color: 6

Bin 189: 5229 of cap free
Amount of items: 2
Items: 
Size: 704652 Color: 16
Size: 290120 Color: 10

Bin 190: 5265 of cap free
Amount of items: 2
Items: 
Size: 584807 Color: 16
Size: 409929 Color: 18

Bin 191: 5393 of cap free
Amount of items: 2
Items: 
Size: 608737 Color: 9
Size: 385871 Color: 16

Bin 192: 5439 of cap free
Amount of items: 2
Items: 
Size: 715770 Color: 18
Size: 278792 Color: 13

Bin 193: 5622 of cap free
Amount of items: 2
Items: 
Size: 691249 Color: 19
Size: 303130 Color: 12

Bin 194: 6184 of cap free
Amount of items: 2
Items: 
Size: 614345 Color: 19
Size: 379472 Color: 1

Bin 195: 6192 of cap free
Amount of items: 2
Items: 
Size: 559044 Color: 12
Size: 434765 Color: 8

Bin 196: 6738 of cap free
Amount of items: 2
Items: 
Size: 538344 Color: 10
Size: 454919 Color: 17

Bin 197: 7435 of cap free
Amount of items: 2
Items: 
Size: 725236 Color: 2
Size: 267330 Color: 8

Bin 198: 8702 of cap free
Amount of items: 2
Items: 
Size: 760885 Color: 18
Size: 230414 Color: 15

Bin 199: 10065 of cap free
Amount of items: 2
Items: 
Size: 569095 Color: 14
Size: 420841 Color: 2

Bin 200: 10074 of cap free
Amount of items: 2
Items: 
Size: 593717 Color: 19
Size: 396210 Color: 17

Bin 201: 10653 of cap free
Amount of items: 2
Items: 
Size: 737006 Color: 12
Size: 252342 Color: 5

Bin 202: 10759 of cap free
Amount of items: 2
Items: 
Size: 688954 Color: 10
Size: 300288 Color: 19

Bin 203: 11343 of cap free
Amount of items: 2
Items: 
Size: 723390 Color: 10
Size: 265268 Color: 4

Bin 204: 11829 of cap free
Amount of items: 2
Items: 
Size: 788398 Color: 15
Size: 199774 Color: 6

Bin 205: 13415 of cap free
Amount of items: 2
Items: 
Size: 509724 Color: 2
Size: 476862 Color: 11

Bin 206: 14402 of cap free
Amount of items: 2
Items: 
Size: 593389 Color: 7
Size: 392210 Color: 12

Bin 207: 15819 of cap free
Amount of items: 2
Items: 
Size: 653117 Color: 15
Size: 331065 Color: 0

Bin 208: 15819 of cap free
Amount of items: 2
Items: 
Size: 614287 Color: 7
Size: 369895 Color: 3

Bin 209: 18647 of cap free
Amount of items: 2
Items: 
Size: 505522 Color: 16
Size: 475832 Color: 17

Bin 210: 18996 of cap free
Amount of items: 2
Items: 
Size: 682125 Color: 6
Size: 298880 Color: 12

Bin 211: 20085 of cap free
Amount of items: 2
Items: 
Size: 714947 Color: 1
Size: 264969 Color: 13

Bin 212: 22514 of cap free
Amount of items: 2
Items: 
Size: 501728 Color: 1
Size: 475759 Color: 19

Bin 213: 23594 of cap free
Amount of items: 2
Items: 
Size: 680518 Color: 12
Size: 295889 Color: 19

Bin 214: 23648 of cap free
Amount of items: 2
Items: 
Size: 785559 Color: 19
Size: 190794 Color: 16

Bin 215: 25523 of cap free
Amount of items: 2
Items: 
Size: 784656 Color: 7
Size: 189822 Color: 0

Bin 216: 26381 of cap free
Amount of items: 2
Items: 
Size: 553370 Color: 0
Size: 420250 Color: 12

Bin 217: 31461 of cap free
Amount of items: 2
Items: 
Size: 499467 Color: 17
Size: 469073 Color: 15

Bin 218: 32563 of cap free
Amount of items: 2
Items: 
Size: 549544 Color: 5
Size: 417894 Color: 8

Bin 219: 32674 of cap free
Amount of items: 2
Items: 
Size: 549801 Color: 8
Size: 417526 Color: 6

Bin 220: 33426 of cap free
Amount of items: 2
Items: 
Size: 783295 Color: 11
Size: 183280 Color: 2

Bin 221: 36274 of cap free
Amount of items: 2
Items: 
Size: 547738 Color: 17
Size: 415989 Color: 18

Bin 222: 39216 of cap free
Amount of items: 2
Items: 
Size: 777714 Color: 0
Size: 183071 Color: 9

Bin 223: 49907 of cap free
Amount of items: 2
Items: 
Size: 652962 Color: 7
Size: 297132 Color: 12

Bin 224: 69413 of cap free
Amount of items: 2
Items: 
Size: 466730 Color: 4
Size: 463858 Color: 17

Bin 225: 82601 of cap free
Amount of items: 2
Items: 
Size: 463564 Color: 14
Size: 453836 Color: 19

Bin 226: 222507 of cap free
Amount of items: 1
Items: 
Size: 777494 Color: 2

Bin 227: 224001 of cap free
Amount of items: 1
Items: 
Size: 776000 Color: 17

Bin 228: 224200 of cap free
Amount of items: 1
Items: 
Size: 775801 Color: 2

Bin 229: 224222 of cap free
Amount of items: 1
Items: 
Size: 775779 Color: 5

Bin 230: 228998 of cap free
Amount of items: 1
Items: 
Size: 771003 Color: 0

Bin 231: 243738 of cap free
Amount of items: 1
Items: 
Size: 756263 Color: 8

Total size: 228666558
Total free space: 2333673

