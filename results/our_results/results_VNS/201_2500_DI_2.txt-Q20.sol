Capicity Bin: 2048
Lower Bound: 65

Bins used: 66
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 1027 Color: 4
Size: 429 Color: 3
Size: 342 Color: 5
Size: 186 Color: 8
Size: 64 Color: 5

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 1298 Color: 4
Size: 682 Color: 8
Size: 68 Color: 9

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 1398 Color: 7
Size: 614 Color: 19
Size: 36 Color: 10

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 1465 Color: 7
Size: 487 Color: 10
Size: 96 Color: 1

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 1534 Color: 17
Size: 430 Color: 10
Size: 84 Color: 16

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 1587 Color: 6
Size: 385 Color: 3
Size: 76 Color: 15

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 1588 Color: 7
Size: 388 Color: 18
Size: 72 Color: 12

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 1621 Color: 16
Size: 281 Color: 1
Size: 146 Color: 12

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 1659 Color: 4
Size: 265 Color: 7
Size: 124 Color: 17

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 1673 Color: 17
Size: 275 Color: 14
Size: 100 Color: 15

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 1682 Color: 4
Size: 198 Color: 15
Size: 168 Color: 5

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 1711 Color: 13
Size: 273 Color: 2
Size: 64 Color: 17

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 1738 Color: 7
Size: 190 Color: 14
Size: 120 Color: 18

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 1742 Color: 6
Size: 258 Color: 11
Size: 48 Color: 1

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 1761 Color: 4
Size: 241 Color: 19
Size: 46 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 1758 Color: 19
Size: 242 Color: 11
Size: 48 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 1810 Color: 6
Size: 202 Color: 3
Size: 36 Color: 15

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 1814 Color: 6
Size: 126 Color: 19
Size: 108 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 1818 Color: 10
Size: 160 Color: 11
Size: 70 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 1826 Color: 8
Size: 220 Color: 7
Size: 2 Color: 14

Bin 21: 1 of cap free
Amount of items: 3
Items: 
Size: 1280 Color: 19
Size: 731 Color: 13
Size: 36 Color: 7

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 1283 Color: 13
Size: 696 Color: 16
Size: 68 Color: 2

Bin 23: 1 of cap free
Amount of items: 2
Items: 
Size: 1314 Color: 9
Size: 733 Color: 4

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 1385 Color: 7
Size: 626 Color: 17
Size: 36 Color: 3

Bin 25: 1 of cap free
Amount of items: 2
Items: 
Size: 1438 Color: 13
Size: 609 Color: 11

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 1463 Color: 2
Size: 542 Color: 15
Size: 42 Color: 10

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 1629 Color: 16
Size: 382 Color: 10
Size: 36 Color: 11

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 1733 Color: 1
Size: 314 Color: 8

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 1781 Color: 13
Size: 208 Color: 4
Size: 58 Color: 13

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 1793 Color: 9
Size: 246 Color: 17
Size: 8 Color: 10

Bin 31: 2 of cap free
Amount of items: 3
Items: 
Size: 1319 Color: 3
Size: 631 Color: 0
Size: 96 Color: 6

Bin 32: 2 of cap free
Amount of items: 3
Items: 
Size: 1527 Color: 0
Size: 489 Color: 8
Size: 30 Color: 15

Bin 33: 2 of cap free
Amount of items: 2
Items: 
Size: 1574 Color: 15
Size: 472 Color: 8

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 1713 Color: 15
Size: 313 Color: 4
Size: 20 Color: 17

Bin 35: 2 of cap free
Amount of items: 2
Items: 
Size: 1721 Color: 14
Size: 325 Color: 1

Bin 36: 2 of cap free
Amount of items: 2
Items: 
Size: 1743 Color: 19
Size: 303 Color: 14

Bin 37: 3 of cap free
Amount of items: 3
Items: 
Size: 1086 Color: 4
Size: 903 Color: 14
Size: 56 Color: 17

Bin 38: 3 of cap free
Amount of items: 2
Items: 
Size: 1582 Color: 16
Size: 463 Color: 17

Bin 39: 3 of cap free
Amount of items: 3
Items: 
Size: 1674 Color: 6
Size: 351 Color: 9
Size: 20 Color: 10

Bin 40: 4 of cap free
Amount of items: 5
Items: 
Size: 1026 Color: 18
Size: 424 Color: 11
Size: 338 Color: 2
Size: 194 Color: 5
Size: 62 Color: 1

Bin 41: 4 of cap free
Amount of items: 3
Items: 
Size: 1514 Color: 6
Size: 482 Color: 8
Size: 48 Color: 4

Bin 42: 4 of cap free
Amount of items: 3
Items: 
Size: 1590 Color: 18
Size: 378 Color: 3
Size: 76 Color: 4

Bin 43: 4 of cap free
Amount of items: 2
Items: 
Size: 1598 Color: 13
Size: 446 Color: 17

Bin 44: 4 of cap free
Amount of items: 2
Items: 
Size: 1646 Color: 16
Size: 398 Color: 13

Bin 45: 5 of cap free
Amount of items: 3
Items: 
Size: 1416 Color: 8
Size: 551 Color: 2
Size: 76 Color: 19

Bin 46: 5 of cap free
Amount of items: 3
Items: 
Size: 1822 Color: 14
Size: 213 Color: 2
Size: 8 Color: 13

Bin 47: 6 of cap free
Amount of items: 3
Items: 
Size: 1535 Color: 13
Size: 431 Color: 9
Size: 76 Color: 4

Bin 48: 6 of cap free
Amount of items: 2
Items: 
Size: 1685 Color: 3
Size: 357 Color: 13

Bin 49: 7 of cap free
Amount of items: 5
Items: 
Size: 1025 Color: 19
Size: 318 Color: 9
Size: 316 Color: 9
Size: 262 Color: 12
Size: 120 Color: 0

Bin 50: 7 of cap free
Amount of items: 3
Items: 
Size: 1173 Color: 9
Size: 828 Color: 17
Size: 40 Color: 14

Bin 51: 7 of cap free
Amount of items: 3
Items: 
Size: 1287 Color: 19
Size: 706 Color: 4
Size: 48 Color: 0

Bin 52: 7 of cap free
Amount of items: 2
Items: 
Size: 1531 Color: 8
Size: 510 Color: 18

Bin 53: 12 of cap free
Amount of items: 2
Items: 
Size: 1234 Color: 18
Size: 802 Color: 16

Bin 54: 13 of cap free
Amount of items: 2
Items: 
Size: 1754 Color: 15
Size: 281 Color: 19

Bin 55: 19 of cap free
Amount of items: 2
Items: 
Size: 1390 Color: 18
Size: 639 Color: 6

Bin 56: 20 of cap free
Amount of items: 2
Items: 
Size: 1291 Color: 9
Size: 737 Color: 15

Bin 57: 20 of cap free
Amount of items: 2
Items: 
Size: 1593 Color: 7
Size: 435 Color: 18

Bin 58: 20 of cap free
Amount of items: 2
Items: 
Size: 1638 Color: 18
Size: 390 Color: 5

Bin 59: 22 of cap free
Amount of items: 3
Items: 
Size: 1031 Color: 13
Size: 849 Color: 16
Size: 146 Color: 0

Bin 60: 24 of cap free
Amount of items: 2
Items: 
Size: 1389 Color: 6
Size: 635 Color: 14

Bin 61: 25 of cap free
Amount of items: 2
Items: 
Size: 1169 Color: 16
Size: 854 Color: 11

Bin 62: 25 of cap free
Amount of items: 2
Items: 
Size: 1470 Color: 4
Size: 553 Color: 11

Bin 63: 32 of cap free
Amount of items: 2
Items: 
Size: 1165 Color: 18
Size: 851 Color: 7

Bin 64: 33 of cap free
Amount of items: 3
Items: 
Size: 1202 Color: 0
Size: 550 Color: 19
Size: 263 Color: 10

Bin 65: 34 of cap free
Amount of items: 18
Items: 
Size: 170 Color: 7
Size: 168 Color: 16
Size: 144 Color: 13
Size: 140 Color: 19
Size: 132 Color: 15
Size: 126 Color: 13
Size: 126 Color: 4
Size: 120 Color: 1
Size: 110 Color: 18
Size: 108 Color: 8
Size: 108 Color: 0
Size: 96 Color: 4
Size: 88 Color: 10
Size: 86 Color: 15
Size: 84 Color: 2
Size: 76 Color: 8
Size: 72 Color: 10
Size: 60 Color: 17

Bin 66: 1648 of cap free
Amount of items: 7
Items: 
Size: 76 Color: 0
Size: 60 Color: 12
Size: 56 Color: 18
Size: 54 Color: 17
Size: 54 Color: 14
Size: 52 Color: 19
Size: 48 Color: 5

Total size: 133120
Total free space: 2048

