Capicity Bin: 16608
Lower Bound: 198

Bins used: 199
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 8322 Color: 3
Size: 6904 Color: 4
Size: 1382 Color: 4

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 9784 Color: 2
Size: 5688 Color: 6
Size: 1136 Color: 3

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 10434 Color: 18
Size: 5854 Color: 2
Size: 320 Color: 3

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 11384 Color: 9
Size: 4856 Color: 4
Size: 368 Color: 16

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 11762 Color: 2
Size: 4546 Color: 3
Size: 300 Color: 10

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 11778 Color: 7
Size: 4026 Color: 16
Size: 804 Color: 13

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 11848 Color: 17
Size: 4360 Color: 4
Size: 400 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 12472 Color: 6
Size: 3816 Color: 5
Size: 320 Color: 2

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 12830 Color: 0
Size: 3466 Color: 2
Size: 312 Color: 11

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 13000 Color: 4
Size: 2988 Color: 1
Size: 620 Color: 8

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 13192 Color: 0
Size: 2812 Color: 1
Size: 604 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 13238 Color: 12
Size: 2810 Color: 10
Size: 560 Color: 9

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 13272 Color: 1
Size: 3144 Color: 1
Size: 192 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 13296 Color: 1
Size: 3016 Color: 11
Size: 296 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 13321 Color: 1
Size: 2741 Color: 14
Size: 546 Color: 9

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 13276 Color: 7
Size: 2740 Color: 7
Size: 592 Color: 14

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 13345 Color: 3
Size: 2983 Color: 18
Size: 280 Color: 14

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 13373 Color: 3
Size: 1995 Color: 13
Size: 1240 Color: 15

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 13393 Color: 2
Size: 2681 Color: 12
Size: 534 Color: 14

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 13397 Color: 5
Size: 2075 Color: 4
Size: 1136 Color: 15

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 13562 Color: 4
Size: 2594 Color: 1
Size: 452 Color: 9

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 13588 Color: 17
Size: 1644 Color: 13
Size: 1376 Color: 11

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 13640 Color: 10
Size: 2776 Color: 3
Size: 192 Color: 9

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 13723 Color: 16
Size: 2257 Color: 2
Size: 628 Color: 1

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 13808 Color: 7
Size: 2228 Color: 2
Size: 572 Color: 8

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 13866 Color: 18
Size: 2278 Color: 14
Size: 464 Color: 13

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 13924 Color: 12
Size: 2244 Color: 11
Size: 440 Color: 13

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 14004 Color: 17
Size: 2276 Color: 17
Size: 328 Color: 18

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 14056 Color: 16
Size: 1976 Color: 15
Size: 576 Color: 12

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 14091 Color: 6
Size: 2099 Color: 11
Size: 418 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 14102 Color: 15
Size: 1934 Color: 3
Size: 572 Color: 12

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 14114 Color: 8
Size: 1710 Color: 1
Size: 784 Color: 19

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 14212 Color: 19
Size: 2056 Color: 3
Size: 340 Color: 14

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 14228 Color: 5
Size: 2068 Color: 15
Size: 312 Color: 1

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 14319 Color: 8
Size: 1649 Color: 18
Size: 640 Color: 18

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 14310 Color: 3
Size: 1448 Color: 14
Size: 850 Color: 17

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 14328 Color: 8
Size: 1240 Color: 1
Size: 1040 Color: 17

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 14396 Color: 9
Size: 1684 Color: 18
Size: 528 Color: 13

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 14452 Color: 3
Size: 1476 Color: 16
Size: 680 Color: 6

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 14522 Color: 6
Size: 1558 Color: 11
Size: 528 Color: 18

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 14525 Color: 19
Size: 1691 Color: 2
Size: 392 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 14601 Color: 17
Size: 1673 Color: 1
Size: 334 Color: 13

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 14616 Color: 19
Size: 1088 Color: 1
Size: 904 Color: 3

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 14697 Color: 13
Size: 1493 Color: 5
Size: 418 Color: 5

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 14712 Color: 16
Size: 1532 Color: 2
Size: 364 Color: 17

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 14769 Color: 3
Size: 1691 Color: 13
Size: 148 Color: 12

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 14728 Color: 9
Size: 1576 Color: 0
Size: 304 Color: 11

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 14740 Color: 13
Size: 1380 Color: 16
Size: 488 Color: 9

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 14800 Color: 3
Size: 1376 Color: 1
Size: 432 Color: 13

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 14812 Color: 2
Size: 1232 Color: 9
Size: 564 Color: 12

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 14814 Color: 17
Size: 1382 Color: 18
Size: 412 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 14886 Color: 19
Size: 1376 Color: 3
Size: 346 Color: 13

Bin 53: 1 of cap free
Amount of items: 4
Items: 
Size: 8313 Color: 11
Size: 6902 Color: 12
Size: 912 Color: 19
Size: 480 Color: 9

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 9486 Color: 13
Size: 5939 Color: 3
Size: 1182 Color: 0

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 10196 Color: 14
Size: 6103 Color: 0
Size: 308 Color: 14

Bin 56: 1 of cap free
Amount of items: 3
Items: 
Size: 10861 Color: 6
Size: 5332 Color: 14
Size: 414 Color: 9

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 12921 Color: 17
Size: 2822 Color: 7
Size: 864 Color: 7

Bin 58: 1 of cap free
Amount of items: 3
Items: 
Size: 12977 Color: 11
Size: 1988 Color: 2
Size: 1642 Color: 5

Bin 59: 1 of cap free
Amount of items: 3
Items: 
Size: 13174 Color: 10
Size: 3105 Color: 12
Size: 328 Color: 5

Bin 60: 1 of cap free
Amount of items: 2
Items: 
Size: 13236 Color: 4
Size: 3371 Color: 6

Bin 61: 1 of cap free
Amount of items: 3
Items: 
Size: 13260 Color: 8
Size: 3027 Color: 7
Size: 320 Color: 16

Bin 62: 1 of cap free
Amount of items: 3
Items: 
Size: 14123 Color: 2
Size: 1420 Color: 5
Size: 1064 Color: 3

Bin 63: 1 of cap free
Amount of items: 3
Items: 
Size: 14556 Color: 12
Size: 1531 Color: 3
Size: 520 Color: 8

Bin 64: 1 of cap free
Amount of items: 3
Items: 
Size: 14849 Color: 10
Size: 1380 Color: 11
Size: 378 Color: 13

Bin 65: 1 of cap free
Amount of items: 2
Items: 
Size: 14908 Color: 17
Size: 1699 Color: 19

Bin 66: 2 of cap free
Amount of items: 3
Items: 
Size: 9394 Color: 14
Size: 6914 Color: 3
Size: 298 Color: 4

Bin 67: 2 of cap free
Amount of items: 3
Items: 
Size: 9478 Color: 14
Size: 6504 Color: 19
Size: 624 Color: 15

Bin 68: 2 of cap free
Amount of items: 3
Items: 
Size: 9531 Color: 5
Size: 5763 Color: 1
Size: 1312 Color: 16

Bin 69: 2 of cap free
Amount of items: 3
Items: 
Size: 9655 Color: 13
Size: 6607 Color: 0
Size: 344 Color: 18

Bin 70: 2 of cap free
Amount of items: 3
Items: 
Size: 11092 Color: 19
Size: 5142 Color: 5
Size: 372 Color: 5

Bin 71: 2 of cap free
Amount of items: 3
Items: 
Size: 11086 Color: 11
Size: 4534 Color: 5
Size: 986 Color: 17

Bin 72: 2 of cap free
Amount of items: 3
Items: 
Size: 11826 Color: 14
Size: 4604 Color: 14
Size: 176 Color: 9

Bin 73: 2 of cap free
Amount of items: 3
Items: 
Size: 12450 Color: 3
Size: 2082 Color: 18
Size: 2074 Color: 1

Bin 74: 2 of cap free
Amount of items: 3
Items: 
Size: 12466 Color: 7
Size: 3846 Color: 10
Size: 294 Color: 15

Bin 75: 2 of cap free
Amount of items: 3
Items: 
Size: 12760 Color: 12
Size: 3524 Color: 3
Size: 322 Color: 5

Bin 76: 2 of cap free
Amount of items: 2
Items: 
Size: 13158 Color: 10
Size: 3448 Color: 14

Bin 77: 2 of cap free
Amount of items: 2
Items: 
Size: 13958 Color: 19
Size: 2648 Color: 5

Bin 78: 2 of cap free
Amount of items: 3
Items: 
Size: 14127 Color: 4
Size: 2071 Color: 12
Size: 408 Color: 1

Bin 79: 2 of cap free
Amount of items: 2
Items: 
Size: 14374 Color: 17
Size: 2232 Color: 13

Bin 80: 2 of cap free
Amount of items: 2
Items: 
Size: 14558 Color: 7
Size: 2048 Color: 19

Bin 81: 3 of cap free
Amount of items: 3
Items: 
Size: 9188 Color: 2
Size: 5919 Color: 15
Size: 1498 Color: 1

Bin 82: 3 of cap free
Amount of items: 3
Items: 
Size: 9791 Color: 1
Size: 5950 Color: 2
Size: 864 Color: 4

Bin 83: 3 of cap free
Amount of items: 3
Items: 
Size: 10709 Color: 9
Size: 4920 Color: 3
Size: 976 Color: 7

Bin 84: 3 of cap free
Amount of items: 3
Items: 
Size: 11994 Color: 5
Size: 4211 Color: 9
Size: 400 Color: 5

Bin 85: 3 of cap free
Amount of items: 3
Items: 
Size: 12380 Color: 12
Size: 3409 Color: 10
Size: 816 Color: 6

Bin 86: 3 of cap free
Amount of items: 3
Items: 
Size: 13265 Color: 6
Size: 3168 Color: 3
Size: 172 Color: 11

Bin 87: 3 of cap free
Amount of items: 3
Items: 
Size: 13437 Color: 3
Size: 1672 Color: 12
Size: 1496 Color: 14

Bin 88: 4 of cap free
Amount of items: 9
Items: 
Size: 8306 Color: 12
Size: 1190 Color: 2
Size: 1188 Color: 16
Size: 1188 Color: 11
Size: 1184 Color: 10
Size: 1184 Color: 5
Size: 956 Color: 7
Size: 720 Color: 0
Size: 688 Color: 6

Bin 89: 4 of cap free
Amount of items: 5
Items: 
Size: 8312 Color: 11
Size: 4042 Color: 3
Size: 3138 Color: 1
Size: 560 Color: 4
Size: 552 Color: 2

Bin 90: 4 of cap free
Amount of items: 3
Items: 
Size: 10442 Color: 0
Size: 4580 Color: 5
Size: 1582 Color: 11

Bin 91: 4 of cap free
Amount of items: 3
Items: 
Size: 10712 Color: 12
Size: 5348 Color: 6
Size: 544 Color: 0

Bin 92: 4 of cap free
Amount of items: 3
Items: 
Size: 11780 Color: 13
Size: 2733 Color: 5
Size: 2091 Color: 0

Bin 93: 4 of cap free
Amount of items: 3
Items: 
Size: 12344 Color: 2
Size: 4028 Color: 6
Size: 232 Color: 8

Bin 94: 4 of cap free
Amount of items: 2
Items: 
Size: 13901 Color: 0
Size: 2703 Color: 6

Bin 95: 4 of cap free
Amount of items: 3
Items: 
Size: 14081 Color: 1
Size: 1467 Color: 3
Size: 1056 Color: 10

Bin 96: 4 of cap free
Amount of items: 2
Items: 
Size: 14742 Color: 19
Size: 1862 Color: 12

Bin 97: 4 of cap free
Amount of items: 2
Items: 
Size: 14888 Color: 4
Size: 1716 Color: 19

Bin 98: 5 of cap free
Amount of items: 8
Items: 
Size: 8305 Color: 19
Size: 1386 Color: 2
Size: 1382 Color: 8
Size: 1380 Color: 11
Size: 1320 Color: 17
Size: 1296 Color: 11
Size: 1190 Color: 14
Size: 344 Color: 16

Bin 99: 5 of cap free
Amount of items: 7
Items: 
Size: 8308 Color: 7
Size: 1804 Color: 5
Size: 1621 Color: 9
Size: 1564 Color: 8
Size: 1520 Color: 3
Size: 1162 Color: 6
Size: 624 Color: 18

Bin 100: 5 of cap free
Amount of items: 3
Items: 
Size: 9635 Color: 14
Size: 5928 Color: 11
Size: 1040 Color: 4

Bin 101: 5 of cap free
Amount of items: 3
Items: 
Size: 10024 Color: 5
Size: 6187 Color: 15
Size: 392 Color: 12

Bin 102: 5 of cap free
Amount of items: 2
Items: 
Size: 10792 Color: 8
Size: 5811 Color: 11

Bin 103: 5 of cap free
Amount of items: 3
Items: 
Size: 12519 Color: 0
Size: 3124 Color: 5
Size: 960 Color: 13

Bin 104: 5 of cap free
Amount of items: 3
Items: 
Size: 13222 Color: 3
Size: 2677 Color: 10
Size: 704 Color: 0

Bin 105: 5 of cap free
Amount of items: 2
Items: 
Size: 13741 Color: 5
Size: 2862 Color: 13

Bin 106: 5 of cap free
Amount of items: 3
Items: 
Size: 13957 Color: 1
Size: 2136 Color: 3
Size: 510 Color: 18

Bin 107: 5 of cap free
Amount of items: 2
Items: 
Size: 14423 Color: 9
Size: 2180 Color: 12

Bin 108: 5 of cap free
Amount of items: 2
Items: 
Size: 14581 Color: 17
Size: 2022 Color: 16

Bin 109: 6 of cap free
Amount of items: 3
Items: 
Size: 9483 Color: 12
Size: 5681 Color: 0
Size: 1438 Color: 11

Bin 110: 6 of cap free
Amount of items: 2
Items: 
Size: 10358 Color: 8
Size: 6244 Color: 6

Bin 111: 6 of cap free
Amount of items: 3
Items: 
Size: 12081 Color: 10
Size: 4257 Color: 14
Size: 264 Color: 0

Bin 112: 6 of cap free
Amount of items: 3
Items: 
Size: 12565 Color: 15
Size: 3773 Color: 17
Size: 264 Color: 8

Bin 113: 6 of cap free
Amount of items: 2
Items: 
Size: 13724 Color: 9
Size: 2878 Color: 8

Bin 114: 6 of cap free
Amount of items: 3
Items: 
Size: 13878 Color: 10
Size: 2260 Color: 0
Size: 464 Color: 3

Bin 115: 6 of cap free
Amount of items: 3
Items: 
Size: 14115 Color: 18
Size: 2391 Color: 4
Size: 96 Color: 6

Bin 116: 7 of cap free
Amount of items: 2
Items: 
Size: 11170 Color: 4
Size: 5431 Color: 16

Bin 117: 7 of cap free
Amount of items: 2
Items: 
Size: 14390 Color: 14
Size: 2211 Color: 5

Bin 118: 8 of cap free
Amount of items: 3
Items: 
Size: 11116 Color: 6
Size: 5292 Color: 7
Size: 192 Color: 0

Bin 119: 8 of cap free
Amount of items: 3
Items: 
Size: 12356 Color: 1
Size: 4004 Color: 14
Size: 240 Color: 1

Bin 120: 8 of cap free
Amount of items: 2
Items: 
Size: 13146 Color: 8
Size: 3454 Color: 1

Bin 121: 8 of cap free
Amount of items: 3
Items: 
Size: 13710 Color: 19
Size: 2352 Color: 1
Size: 538 Color: 14

Bin 122: 8 of cap free
Amount of items: 2
Items: 
Size: 14596 Color: 16
Size: 2004 Color: 17

Bin 123: 9 of cap free
Amount of items: 2
Items: 
Size: 14773 Color: 8
Size: 1826 Color: 12

Bin 124: 11 of cap free
Amount of items: 3
Items: 
Size: 10681 Color: 5
Size: 5496 Color: 18
Size: 420 Color: 13

Bin 125: 11 of cap free
Amount of items: 2
Items: 
Size: 14335 Color: 17
Size: 2262 Color: 0

Bin 126: 11 of cap free
Amount of items: 2
Items: 
Size: 14665 Color: 19
Size: 1932 Color: 18

Bin 127: 11 of cap free
Amount of items: 2
Items: 
Size: 14946 Color: 18
Size: 1651 Color: 15

Bin 128: 12 of cap free
Amount of items: 3
Items: 
Size: 12836 Color: 17
Size: 3548 Color: 3
Size: 212 Color: 0

Bin 129: 12 of cap free
Amount of items: 2
Items: 
Size: 13800 Color: 18
Size: 2796 Color: 3

Bin 130: 12 of cap free
Amount of items: 2
Items: 
Size: 14470 Color: 0
Size: 2126 Color: 6

Bin 131: 13 of cap free
Amount of items: 3
Items: 
Size: 11447 Color: 1
Size: 4602 Color: 3
Size: 546 Color: 0

Bin 132: 13 of cap free
Amount of items: 2
Items: 
Size: 11804 Color: 3
Size: 4791 Color: 14

Bin 133: 13 of cap free
Amount of items: 2
Items: 
Size: 14099 Color: 13
Size: 2496 Color: 15

Bin 134: 13 of cap free
Amount of items: 2
Items: 
Size: 14488 Color: 16
Size: 2107 Color: 9

Bin 135: 14 of cap free
Amount of items: 2
Items: 
Size: 14164 Color: 17
Size: 2430 Color: 12

Bin 136: 16 of cap free
Amount of items: 3
Items: 
Size: 10156 Color: 1
Size: 6144 Color: 12
Size: 292 Color: 11

Bin 137: 16 of cap free
Amount of items: 2
Items: 
Size: 14068 Color: 4
Size: 2524 Color: 17

Bin 138: 16 of cap free
Amount of items: 2
Items: 
Size: 14248 Color: 13
Size: 2344 Color: 7

Bin 139: 16 of cap free
Amount of items: 2
Items: 
Size: 14824 Color: 16
Size: 1768 Color: 2

Bin 140: 17 of cap free
Amount of items: 3
Items: 
Size: 9463 Color: 0
Size: 5938 Color: 15
Size: 1190 Color: 7

Bin 141: 17 of cap free
Amount of items: 2
Items: 
Size: 13894 Color: 15
Size: 2697 Color: 13

Bin 142: 17 of cap free
Amount of items: 2
Items: 
Size: 14186 Color: 6
Size: 2405 Color: 14

Bin 143: 17 of cap free
Amount of items: 3
Items: 
Size: 14638 Color: 6
Size: 1895 Color: 17
Size: 58 Color: 16

Bin 144: 21 of cap free
Amount of items: 2
Items: 
Size: 12860 Color: 3
Size: 3727 Color: 7

Bin 145: 21 of cap free
Amount of items: 2
Items: 
Size: 13944 Color: 5
Size: 2643 Color: 9

Bin 146: 22 of cap free
Amount of items: 3
Items: 
Size: 14714 Color: 5
Size: 1800 Color: 15
Size: 72 Color: 10

Bin 147: 22 of cap free
Amount of items: 2
Items: 
Size: 14765 Color: 11
Size: 1821 Color: 7

Bin 148: 22 of cap free
Amount of items: 2
Items: 
Size: 14844 Color: 9
Size: 1742 Color: 5

Bin 149: 26 of cap free
Amount of items: 2
Items: 
Size: 13432 Color: 0
Size: 3150 Color: 10

Bin 150: 26 of cap free
Amount of items: 2
Items: 
Size: 14122 Color: 13
Size: 2460 Color: 19

Bin 151: 27 of cap free
Amount of items: 2
Items: 
Size: 11640 Color: 4
Size: 4941 Color: 5

Bin 152: 28 of cap free
Amount of items: 2
Items: 
Size: 13694 Color: 10
Size: 2886 Color: 0

Bin 153: 28 of cap free
Amount of items: 2
Items: 
Size: 14292 Color: 8
Size: 2288 Color: 4

Bin 154: 30 of cap free
Amount of items: 3
Items: 
Size: 13649 Color: 8
Size: 2833 Color: 12
Size: 96 Color: 1

Bin 155: 30 of cap free
Amount of items: 3
Items: 
Size: 14817 Color: 0
Size: 1737 Color: 7
Size: 24 Color: 14

Bin 156: 32 of cap free
Amount of items: 6
Items: 
Size: 8309 Color: 7
Size: 2210 Color: 2
Size: 2090 Color: 11
Size: 2079 Color: 1
Size: 1200 Color: 16
Size: 688 Color: 19

Bin 157: 32 of cap free
Amount of items: 2
Items: 
Size: 14290 Color: 7
Size: 2286 Color: 6

Bin 158: 33 of cap free
Amount of items: 2
Items: 
Size: 14152 Color: 6
Size: 2423 Color: 18

Bin 159: 36 of cap free
Amount of items: 2
Items: 
Size: 10613 Color: 15
Size: 5959 Color: 5

Bin 160: 37 of cap free
Amount of items: 2
Items: 
Size: 12270 Color: 12
Size: 4301 Color: 5

Bin 161: 37 of cap free
Amount of items: 2
Items: 
Size: 14727 Color: 10
Size: 1844 Color: 0

Bin 162: 44 of cap free
Amount of items: 3
Items: 
Size: 8681 Color: 17
Size: 7603 Color: 1
Size: 280 Color: 11

Bin 163: 44 of cap free
Amount of items: 2
Items: 
Size: 10609 Color: 7
Size: 5955 Color: 2

Bin 164: 49 of cap free
Amount of items: 2
Items: 
Size: 13486 Color: 12
Size: 3073 Color: 18

Bin 165: 50 of cap free
Amount of items: 2
Items: 
Size: 11557 Color: 3
Size: 5001 Color: 2

Bin 166: 52 of cap free
Amount of items: 2
Items: 
Size: 14644 Color: 16
Size: 1912 Color: 17

Bin 167: 54 of cap free
Amount of items: 2
Items: 
Size: 8330 Color: 19
Size: 8224 Color: 3

Bin 168: 54 of cap free
Amount of items: 2
Items: 
Size: 11344 Color: 4
Size: 5210 Color: 7

Bin 169: 58 of cap free
Amount of items: 2
Items: 
Size: 12232 Color: 4
Size: 4318 Color: 12

Bin 170: 58 of cap free
Amount of items: 2
Items: 
Size: 14132 Color: 19
Size: 2418 Color: 6

Bin 171: 62 of cap free
Amount of items: 2
Items: 
Size: 13940 Color: 0
Size: 2606 Color: 9

Bin 172: 64 of cap free
Amount of items: 2
Items: 
Size: 13942 Color: 1
Size: 2602 Color: 11

Bin 173: 69 of cap free
Amount of items: 2
Items: 
Size: 12883 Color: 14
Size: 3656 Color: 1

Bin 174: 71 of cap free
Amount of items: 2
Items: 
Size: 12137 Color: 3
Size: 4400 Color: 8

Bin 175: 71 of cap free
Amount of items: 2
Items: 
Size: 13329 Color: 10
Size: 3208 Color: 17

Bin 176: 74 of cap free
Amount of items: 2
Items: 
Size: 11154 Color: 17
Size: 5380 Color: 18

Bin 177: 78 of cap free
Amount of items: 33
Items: 
Size: 768 Color: 15
Size: 744 Color: 12
Size: 720 Color: 15
Size: 704 Color: 10
Size: 692 Color: 14
Size: 688 Color: 0
Size: 672 Color: 11
Size: 614 Color: 17
Size: 560 Color: 1
Size: 534 Color: 2
Size: 496 Color: 12
Size: 484 Color: 8
Size: 480 Color: 17
Size: 476 Color: 14
Size: 468 Color: 7
Size: 456 Color: 9
Size: 452 Color: 19
Size: 450 Color: 0
Size: 448 Color: 19
Size: 448 Color: 17
Size: 440 Color: 10
Size: 440 Color: 9
Size: 440 Color: 7
Size: 440 Color: 1
Size: 416 Color: 5
Size: 416 Color: 2
Size: 414 Color: 14
Size: 414 Color: 0
Size: 412 Color: 7
Size: 384 Color: 19
Size: 368 Color: 6
Size: 304 Color: 6
Size: 288 Color: 6

Bin 178: 84 of cap free
Amount of items: 3
Items: 
Size: 8316 Color: 4
Size: 4152 Color: 4
Size: 4056 Color: 12

Bin 179: 85 of cap free
Amount of items: 2
Items: 
Size: 14119 Color: 12
Size: 2404 Color: 7

Bin 180: 92 of cap free
Amount of items: 2
Items: 
Size: 13660 Color: 12
Size: 2856 Color: 14

Bin 181: 106 of cap free
Amount of items: 19
Items: 
Size: 1072 Color: 16
Size: 1064 Color: 8
Size: 1040 Color: 11
Size: 1024 Color: 2
Size: 998 Color: 7
Size: 920 Color: 8
Size: 912 Color: 14
Size: 912 Color: 3
Size: 908 Color: 6
Size: 890 Color: 16
Size: 860 Color: 9
Size: 850 Color: 13
Size: 840 Color: 15
Size: 804 Color: 9
Size: 800 Color: 17
Size: 800 Color: 13
Size: 704 Color: 0
Size: 624 Color: 19
Size: 480 Color: 19

Bin 182: 110 of cap free
Amount of items: 2
Items: 
Size: 11501 Color: 8
Size: 4997 Color: 10

Bin 183: 112 of cap free
Amount of items: 2
Items: 
Size: 10545 Color: 9
Size: 5951 Color: 7

Bin 184: 120 of cap free
Amount of items: 2
Items: 
Size: 10260 Color: 14
Size: 6228 Color: 3

Bin 185: 133 of cap free
Amount of items: 3
Items: 
Size: 8808 Color: 13
Size: 6913 Color: 2
Size: 754 Color: 7

Bin 186: 136 of cap free
Amount of items: 2
Items: 
Size: 13324 Color: 16
Size: 3148 Color: 14

Bin 187: 137 of cap free
Amount of items: 3
Items: 
Size: 9459 Color: 5
Size: 6014 Color: 0
Size: 998 Color: 19

Bin 188: 144 of cap free
Amount of items: 2
Items: 
Size: 12846 Color: 8
Size: 3618 Color: 1

Bin 189: 158 of cap free
Amount of items: 3
Items: 
Size: 8314 Color: 13
Size: 5942 Color: 0
Size: 2194 Color: 5

Bin 190: 172 of cap free
Amount of items: 2
Items: 
Size: 9140 Color: 14
Size: 7296 Color: 10

Bin 191: 179 of cap free
Amount of items: 2
Items: 
Size: 9507 Color: 13
Size: 6922 Color: 1

Bin 192: 191 of cap free
Amount of items: 2
Items: 
Size: 9496 Color: 1
Size: 6921 Color: 7

Bin 193: 199 of cap free
Amount of items: 3
Items: 
Size: 8317 Color: 15
Size: 6906 Color: 1
Size: 1186 Color: 7

Bin 194: 208 of cap free
Amount of items: 2
Items: 
Size: 10212 Color: 13
Size: 6188 Color: 8

Bin 195: 221 of cap free
Amount of items: 2
Items: 
Size: 9470 Color: 0
Size: 6917 Color: 14

Bin 196: 224 of cap free
Amount of items: 2
Items: 
Size: 8328 Color: 16
Size: 8056 Color: 9

Bin 197: 224 of cap free
Amount of items: 2
Items: 
Size: 9124 Color: 16
Size: 7260 Color: 9

Bin 198: 225 of cap free
Amount of items: 2
Items: 
Size: 9467 Color: 3
Size: 6916 Color: 11

Bin 199: 11344 of cap free
Amount of items: 17
Items: 
Size: 384 Color: 13
Size: 384 Color: 10
Size: 352 Color: 17
Size: 352 Color: 9
Size: 344 Color: 10
Size: 336 Color: 12
Size: 336 Color: 8
Size: 304 Color: 2
Size: 288 Color: 18
Size: 288 Color: 6
Size: 288 Color: 0
Size: 284 Color: 14
Size: 276 Color: 9
Size: 272 Color: 11
Size: 264 Color: 7
Size: 256 Color: 19
Size: 256 Color: 16

Total size: 3288384
Total free space: 16608

