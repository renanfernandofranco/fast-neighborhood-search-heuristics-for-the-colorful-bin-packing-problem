Capicity Bin: 5856
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 2941 Color: 13
Size: 2431 Color: 19
Size: 484 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 3324 Color: 14
Size: 2320 Color: 17
Size: 212 Color: 6

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3772 Color: 5
Size: 1812 Color: 7
Size: 272 Color: 19

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3946 Color: 15
Size: 1794 Color: 14
Size: 116 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 4082 Color: 15
Size: 1590 Color: 13
Size: 184 Color: 5

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 4252 Color: 4
Size: 1188 Color: 19
Size: 416 Color: 19

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 4351 Color: 14
Size: 1249 Color: 3
Size: 256 Color: 17

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 4681 Color: 1
Size: 981 Color: 5
Size: 194 Color: 10

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 4692 Color: 14
Size: 956 Color: 14
Size: 208 Color: 15

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 4840 Color: 6
Size: 876 Color: 16
Size: 140 Color: 6

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 4844 Color: 1
Size: 884 Color: 8
Size: 128 Color: 9

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 4886 Color: 0
Size: 486 Color: 16
Size: 484 Color: 4

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 4920 Color: 0
Size: 480 Color: 12
Size: 456 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 4959 Color: 19
Size: 621 Color: 14
Size: 276 Color: 14

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 4984 Color: 8
Size: 640 Color: 1
Size: 232 Color: 4

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 4996 Color: 16
Size: 440 Color: 4
Size: 420 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 5003 Color: 7
Size: 739 Color: 13
Size: 114 Color: 4

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 17
Size: 668 Color: 14
Size: 128 Color: 12

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 5061 Color: 16
Size: 663 Color: 19
Size: 132 Color: 9

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 5062 Color: 5
Size: 780 Color: 0
Size: 14 Color: 1

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 5106 Color: 6
Size: 486 Color: 12
Size: 264 Color: 18

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 5108 Color: 0
Size: 624 Color: 13
Size: 124 Color: 18

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 5111 Color: 15
Size: 601 Color: 4
Size: 144 Color: 1

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 5140 Color: 15
Size: 572 Color: 6
Size: 144 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 5149 Color: 18
Size: 575 Color: 15
Size: 132 Color: 2

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 19
Size: 561 Color: 14
Size: 112 Color: 18

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 5186 Color: 16
Size: 486 Color: 14
Size: 184 Color: 16

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 5192 Color: 4
Size: 352 Color: 7
Size: 312 Color: 1

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 5218 Color: 12
Size: 506 Color: 4
Size: 132 Color: 14

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 5234 Color: 5
Size: 480 Color: 18
Size: 142 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 5236 Color: 7
Size: 360 Color: 13
Size: 260 Color: 1

Bin 32: 1 of cap free
Amount of items: 6
Items: 
Size: 2932 Color: 17
Size: 1083 Color: 15
Size: 810 Color: 8
Size: 782 Color: 7
Size: 152 Color: 13
Size: 96 Color: 16

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 3333 Color: 0
Size: 2426 Color: 13
Size: 96 Color: 8

Bin 34: 1 of cap free
Amount of items: 3
Items: 
Size: 4161 Color: 10
Size: 883 Color: 2
Size: 811 Color: 7

Bin 35: 1 of cap free
Amount of items: 3
Items: 
Size: 4284 Color: 6
Size: 1411 Color: 3
Size: 160 Color: 19

Bin 36: 1 of cap free
Amount of items: 3
Items: 
Size: 4675 Color: 12
Size: 1084 Color: 6
Size: 96 Color: 12

Bin 37: 1 of cap free
Amount of items: 3
Items: 
Size: 4689 Color: 1
Size: 1048 Color: 19
Size: 118 Color: 6

Bin 38: 1 of cap free
Amount of items: 2
Items: 
Size: 4797 Color: 7
Size: 1058 Color: 10

Bin 39: 1 of cap free
Amount of items: 3
Items: 
Size: 4955 Color: 1
Size: 480 Color: 14
Size: 420 Color: 9

Bin 40: 1 of cap free
Amount of items: 3
Items: 
Size: 4951 Color: 18
Size: 584 Color: 0
Size: 320 Color: 7

Bin 41: 1 of cap free
Amount of items: 3
Items: 
Size: 5007 Color: 0
Size: 724 Color: 2
Size: 124 Color: 8

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 5055 Color: 1
Size: 568 Color: 10
Size: 232 Color: 19

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 5084 Color: 15
Size: 755 Color: 7
Size: 16 Color: 3

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5154 Color: 2
Size: 669 Color: 19
Size: 32 Color: 1

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5167 Color: 16
Size: 672 Color: 7
Size: 16 Color: 7

Bin 46: 2 of cap free
Amount of items: 26
Items: 
Size: 416 Color: 0
Size: 366 Color: 16
Size: 364 Color: 2
Size: 316 Color: 10
Size: 304 Color: 17
Size: 280 Color: 11
Size: 280 Color: 1
Size: 256 Color: 0
Size: 250 Color: 14
Size: 250 Color: 4
Size: 248 Color: 16
Size: 248 Color: 11
Size: 196 Color: 7
Size: 194 Color: 10
Size: 194 Color: 6
Size: 192 Color: 19
Size: 192 Color: 8
Size: 192 Color: 0
Size: 168 Color: 7
Size: 160 Color: 9
Size: 150 Color: 13
Size: 150 Color: 9
Size: 144 Color: 13
Size: 136 Color: 15
Size: 104 Color: 8
Size: 104 Color: 2

Bin 47: 2 of cap free
Amount of items: 4
Items: 
Size: 2937 Color: 18
Size: 2433 Color: 11
Size: 364 Color: 15
Size: 120 Color: 7

Bin 48: 2 of cap free
Amount of items: 5
Items: 
Size: 2938 Color: 7
Size: 2440 Color: 16
Size: 208 Color: 8
Size: 168 Color: 17
Size: 100 Color: 11

Bin 49: 2 of cap free
Amount of items: 3
Items: 
Size: 3212 Color: 3
Size: 2434 Color: 18
Size: 208 Color: 10

Bin 50: 2 of cap free
Amount of items: 3
Items: 
Size: 3336 Color: 18
Size: 2422 Color: 0
Size: 96 Color: 7

Bin 51: 2 of cap free
Amount of items: 3
Items: 
Size: 4032 Color: 5
Size: 1160 Color: 1
Size: 662 Color: 8

Bin 52: 2 of cap free
Amount of items: 3
Items: 
Size: 4674 Color: 4
Size: 626 Color: 19
Size: 554 Color: 8

Bin 53: 2 of cap free
Amount of items: 2
Items: 
Size: 4728 Color: 19
Size: 1126 Color: 6

Bin 54: 2 of cap free
Amount of items: 2
Items: 
Size: 4881 Color: 13
Size: 973 Color: 6

Bin 55: 2 of cap free
Amount of items: 2
Items: 
Size: 5103 Color: 14
Size: 751 Color: 9

Bin 56: 2 of cap free
Amount of items: 2
Items: 
Size: 5250 Color: 6
Size: 604 Color: 17

Bin 57: 3 of cap free
Amount of items: 3
Items: 
Size: 3663 Color: 4
Size: 2094 Color: 0
Size: 96 Color: 11

Bin 58: 3 of cap free
Amount of items: 3
Items: 
Size: 3935 Color: 3
Size: 1818 Color: 8
Size: 100 Color: 13

Bin 59: 3 of cap free
Amount of items: 3
Items: 
Size: 4355 Color: 7
Size: 1178 Color: 11
Size: 320 Color: 13

Bin 60: 3 of cap free
Amount of items: 3
Items: 
Size: 4506 Color: 12
Size: 1251 Color: 0
Size: 96 Color: 17

Bin 61: 3 of cap free
Amount of items: 3
Items: 
Size: 4513 Color: 14
Size: 1232 Color: 13
Size: 108 Color: 18

Bin 62: 3 of cap free
Amount of items: 3
Items: 
Size: 4684 Color: 3
Size: 985 Color: 15
Size: 184 Color: 15

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 4697 Color: 9
Size: 1060 Color: 8
Size: 96 Color: 0

Bin 64: 3 of cap free
Amount of items: 2
Items: 
Size: 4876 Color: 12
Size: 977 Color: 10

Bin 65: 4 of cap free
Amount of items: 3
Items: 
Size: 3724 Color: 3
Size: 2000 Color: 10
Size: 128 Color: 11

Bin 66: 4 of cap free
Amount of items: 2
Items: 
Size: 4885 Color: 7
Size: 967 Color: 18

Bin 67: 4 of cap free
Amount of items: 2
Items: 
Size: 5160 Color: 17
Size: 692 Color: 9

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 5172 Color: 4
Size: 680 Color: 6

Bin 69: 4 of cap free
Amount of items: 2
Items: 
Size: 5266 Color: 11
Size: 586 Color: 2

Bin 70: 5 of cap free
Amount of items: 2
Items: 
Size: 5260 Color: 0
Size: 591 Color: 13

Bin 71: 6 of cap free
Amount of items: 5
Items: 
Size: 2933 Color: 17
Size: 1244 Color: 5
Size: 1227 Color: 8
Size: 224 Color: 9
Size: 222 Color: 14

Bin 72: 6 of cap free
Amount of items: 4
Items: 
Size: 3678 Color: 0
Size: 1580 Color: 7
Size: 416 Color: 17
Size: 176 Color: 1

Bin 73: 6 of cap free
Amount of items: 2
Items: 
Size: 4028 Color: 9
Size: 1822 Color: 3

Bin 74: 6 of cap free
Amount of items: 2
Items: 
Size: 4446 Color: 5
Size: 1404 Color: 10

Bin 75: 6 of cap free
Amount of items: 3
Items: 
Size: 4590 Color: 6
Size: 726 Color: 13
Size: 534 Color: 6

Bin 76: 6 of cap free
Amount of items: 2
Items: 
Size: 4924 Color: 12
Size: 926 Color: 16

Bin 77: 6 of cap free
Amount of items: 2
Items: 
Size: 5122 Color: 6
Size: 728 Color: 4

Bin 78: 7 of cap free
Amount of items: 2
Items: 
Size: 4436 Color: 2
Size: 1413 Color: 16

Bin 79: 7 of cap free
Amount of items: 2
Items: 
Size: 4545 Color: 6
Size: 1304 Color: 1

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 4564 Color: 19
Size: 761 Color: 1
Size: 524 Color: 17

Bin 81: 8 of cap free
Amount of items: 3
Items: 
Size: 3604 Color: 16
Size: 2100 Color: 10
Size: 144 Color: 17

Bin 82: 8 of cap free
Amount of items: 2
Items: 
Size: 3964 Color: 8
Size: 1884 Color: 11

Bin 83: 8 of cap free
Amount of items: 2
Items: 
Size: 4532 Color: 1
Size: 1316 Color: 0

Bin 84: 8 of cap free
Amount of items: 3
Items: 
Size: 4716 Color: 15
Size: 972 Color: 13
Size: 160 Color: 1

Bin 85: 8 of cap free
Amount of items: 2
Items: 
Size: 5137 Color: 3
Size: 711 Color: 15

Bin 86: 8 of cap free
Amount of items: 2
Items: 
Size: 5204 Color: 16
Size: 644 Color: 19

Bin 87: 10 of cap free
Amount of items: 3
Items: 
Size: 2954 Color: 3
Size: 2772 Color: 9
Size: 120 Color: 11

Bin 88: 10 of cap free
Amount of items: 3
Items: 
Size: 3944 Color: 7
Size: 1586 Color: 15
Size: 316 Color: 13

Bin 89: 10 of cap free
Amount of items: 3
Items: 
Size: 4616 Color: 8
Size: 1198 Color: 16
Size: 32 Color: 18

Bin 90: 10 of cap free
Amount of items: 2
Items: 
Size: 4642 Color: 10
Size: 1204 Color: 9

Bin 91: 10 of cap free
Amount of items: 2
Items: 
Size: 4990 Color: 3
Size: 856 Color: 4

Bin 92: 10 of cap free
Amount of items: 3
Items: 
Size: 5202 Color: 13
Size: 628 Color: 16
Size: 16 Color: 15

Bin 93: 11 of cap free
Amount of items: 3
Items: 
Size: 3331 Color: 5
Size: 2098 Color: 0
Size: 416 Color: 11

Bin 94: 11 of cap free
Amount of items: 3
Items: 
Size: 4359 Color: 17
Size: 1262 Color: 7
Size: 224 Color: 1

Bin 95: 11 of cap free
Amount of items: 2
Items: 
Size: 5136 Color: 8
Size: 709 Color: 4

Bin 96: 12 of cap free
Amount of items: 7
Items: 
Size: 2930 Color: 4
Size: 724 Color: 14
Size: 722 Color: 4
Size: 548 Color: 19
Size: 494 Color: 0
Size: 218 Color: 2
Size: 208 Color: 6

Bin 97: 12 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 5
Size: 1402 Color: 1
Size: 168 Color: 13

Bin 98: 12 of cap free
Amount of items: 2
Items: 
Size: 4812 Color: 14
Size: 1032 Color: 17

Bin 99: 15 of cap free
Amount of items: 2
Items: 
Size: 5028 Color: 10
Size: 813 Color: 2

Bin 100: 16 of cap free
Amount of items: 2
Items: 
Size: 4196 Color: 1
Size: 1644 Color: 2

Bin 101: 16 of cap free
Amount of items: 2
Items: 
Size: 5048 Color: 4
Size: 792 Color: 12

Bin 102: 17 of cap free
Amount of items: 2
Items: 
Size: 4517 Color: 6
Size: 1322 Color: 1

Bin 103: 17 of cap free
Amount of items: 2
Items: 
Size: 4746 Color: 18
Size: 1093 Color: 6

Bin 104: 18 of cap free
Amount of items: 3
Items: 
Size: 2940 Color: 1
Size: 2284 Color: 2
Size: 614 Color: 19

Bin 105: 18 of cap free
Amount of items: 3
Items: 
Size: 4685 Color: 2
Size: 1121 Color: 4
Size: 32 Color: 12

Bin 106: 21 of cap free
Amount of items: 3
Items: 
Size: 3116 Color: 9
Size: 2437 Color: 7
Size: 282 Color: 13

Bin 107: 22 of cap free
Amount of items: 2
Items: 
Size: 4922 Color: 10
Size: 912 Color: 0

Bin 108: 22 of cap free
Amount of items: 2
Items: 
Size: 4988 Color: 13
Size: 846 Color: 6

Bin 109: 23 of cap free
Amount of items: 3
Items: 
Size: 3892 Color: 5
Size: 1829 Color: 9
Size: 112 Color: 0

Bin 110: 24 of cap free
Amount of items: 2
Items: 
Size: 4472 Color: 19
Size: 1360 Color: 2

Bin 111: 26 of cap free
Amount of items: 2
Items: 
Size: 4986 Color: 17
Size: 844 Color: 6

Bin 112: 28 of cap free
Amount of items: 2
Items: 
Size: 4842 Color: 17
Size: 986 Color: 12

Bin 113: 30 of cap free
Amount of items: 2
Items: 
Size: 4346 Color: 14
Size: 1480 Color: 18

Bin 114: 36 of cap free
Amount of items: 2
Items: 
Size: 4296 Color: 3
Size: 1524 Color: 19

Bin 115: 40 of cap free
Amount of items: 2
Items: 
Size: 4561 Color: 7
Size: 1255 Color: 16

Bin 116: 41 of cap free
Amount of items: 8
Items: 
Size: 2929 Color: 17
Size: 522 Color: 2
Size: 500 Color: 15
Size: 484 Color: 12
Size: 484 Color: 1
Size: 368 Color: 13
Size: 304 Color: 11
Size: 224 Color: 9

Bin 117: 42 of cap free
Amount of items: 4
Items: 
Size: 2936 Color: 17
Size: 2204 Color: 12
Size: 562 Color: 7
Size: 112 Color: 2

Bin 118: 43 of cap free
Amount of items: 3
Items: 
Size: 3052 Color: 14
Size: 2441 Color: 12
Size: 320 Color: 8

Bin 119: 48 of cap free
Amount of items: 2
Items: 
Size: 4468 Color: 15
Size: 1340 Color: 3

Bin 120: 52 of cap free
Amount of items: 3
Items: 
Size: 3954 Color: 4
Size: 1594 Color: 10
Size: 256 Color: 0

Bin 121: 62 of cap free
Amount of items: 2
Items: 
Size: 3962 Color: 6
Size: 1832 Color: 17

Bin 122: 66 of cap free
Amount of items: 2
Items: 
Size: 3346 Color: 2
Size: 2444 Color: 16

Bin 123: 70 of cap free
Amount of items: 2
Items: 
Size: 4178 Color: 17
Size: 1608 Color: 18

Bin 124: 72 of cap free
Amount of items: 2
Items: 
Size: 3342 Color: 3
Size: 2442 Color: 14

Bin 125: 76 of cap free
Amount of items: 2
Items: 
Size: 3664 Color: 16
Size: 2116 Color: 9

Bin 126: 78 of cap free
Amount of items: 3
Items: 
Size: 2946 Color: 17
Size: 2636 Color: 3
Size: 196 Color: 15

Bin 127: 79 of cap free
Amount of items: 2
Items: 
Size: 3672 Color: 12
Size: 2105 Color: 3

Bin 128: 83 of cap free
Amount of items: 2
Items: 
Size: 3670 Color: 13
Size: 2103 Color: 4

Bin 129: 85 of cap free
Amount of items: 2
Items: 
Size: 4168 Color: 9
Size: 1603 Color: 10

Bin 130: 90 of cap free
Amount of items: 2
Items: 
Size: 4165 Color: 11
Size: 1601 Color: 14

Bin 131: 92 of cap free
Amount of items: 2
Items: 
Size: 3933 Color: 1
Size: 1831 Color: 16

Bin 132: 93 of cap free
Amount of items: 2
Items: 
Size: 3659 Color: 12
Size: 2104 Color: 9

Bin 133: 3970 of cap free
Amount of items: 15
Items: 
Size: 162 Color: 18
Size: 152 Color: 18
Size: 144 Color: 4
Size: 144 Color: 1
Size: 136 Color: 15
Size: 136 Color: 13
Size: 128 Color: 14
Size: 120 Color: 15
Size: 116 Color: 14
Size: 112 Color: 12
Size: 112 Color: 11
Size: 112 Color: 9
Size: 112 Color: 6
Size: 100 Color: 19
Size: 100 Color: 8

Total size: 772992
Total free space: 5856

