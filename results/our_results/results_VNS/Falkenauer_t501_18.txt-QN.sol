Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 501

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 481
Size: 263 Color: 96
Size: 260 Color: 78

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 446
Size: 293 Color: 223
Size: 253 Color: 38

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 467
Size: 271 Color: 136
Size: 265 Color: 109

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 420
Size: 303 Color: 251
Size: 259 Color: 70

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 384
Size: 305 Color: 254
Size: 293 Color: 222

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 471
Size: 270 Color: 126
Size: 263 Color: 94

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 454
Size: 282 Color: 179
Size: 258 Color: 63

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 477
Size: 271 Color: 138
Size: 257 Color: 52

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 484
Size: 265 Color: 105
Size: 255 Color: 48

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 419
Size: 301 Color: 244
Size: 264 Color: 101

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 475
Size: 274 Color: 151
Size: 256 Color: 51

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 357
Size: 368 Color: 346
Size: 259 Color: 68

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 450
Size: 281 Color: 172
Size: 262 Color: 85

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 431
Size: 305 Color: 255
Size: 252 Color: 26

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 497
Size: 251 Color: 24
Size: 251 Color: 20

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 334
Size: 331 Color: 294
Size: 307 Color: 261

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 411
Size: 294 Color: 225
Size: 277 Color: 163

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 498
Size: 252 Color: 27
Size: 250 Color: 11

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 377
Size: 339 Color: 303
Size: 271 Color: 134

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 448
Size: 294 Color: 228
Size: 252 Color: 31

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 463
Size: 274 Color: 149
Size: 264 Color: 100

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 400
Size: 304 Color: 252
Size: 273 Color: 141

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 468
Size: 274 Color: 148
Size: 262 Color: 86

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 464
Size: 275 Color: 153
Size: 262 Color: 90

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 368
Size: 327 Color: 289
Size: 293 Color: 218

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 453
Size: 271 Color: 133
Size: 270 Color: 128

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 415
Size: 293 Color: 221
Size: 274 Color: 145

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 491
Size: 257 Color: 58
Size: 250 Color: 5

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 350
Size: 369 Color: 349
Size: 262 Color: 87

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 439
Size: 283 Color: 180
Size: 269 Color: 122

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 380
Size: 352 Color: 317
Size: 250 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 494
Size: 254 Color: 41
Size: 250 Color: 10

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 411 Color: 390
Size: 317 Color: 278
Size: 272 Color: 140

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 443
Size: 298 Color: 234
Size: 251 Color: 18

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 353
Size: 330 Color: 293
Size: 299 Color: 236

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 367
Size: 369 Color: 348
Size: 252 Color: 36

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 428
Size: 282 Color: 177
Size: 277 Color: 159

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 426
Size: 297 Color: 232
Size: 264 Color: 99

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 344
Size: 359 Color: 332
Size: 274 Color: 150

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 355 Color: 322
Size: 345 Color: 310
Size: 300 Color: 241

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 436
Size: 300 Color: 240
Size: 255 Color: 45

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 338
Size: 357 Color: 328
Size: 281 Color: 174

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 366
Size: 327 Color: 288
Size: 294 Color: 224

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 466
Size: 286 Color: 198
Size: 250 Color: 7

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 434
Size: 292 Color: 216
Size: 264 Color: 103

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 413
Size: 296 Color: 230
Size: 273 Color: 144

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 351 Color: 316
Size: 350 Color: 315
Size: 299 Color: 237

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 465
Size: 283 Color: 181
Size: 254 Color: 42

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 414
Size: 316 Color: 274
Size: 253 Color: 39

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 495
Size: 253 Color: 37
Size: 250 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 320
Size: 345 Color: 311
Size: 302 Color: 249

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 449
Size: 286 Color: 200
Size: 259 Color: 72

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 496
Size: 252 Color: 28
Size: 250 Color: 2

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 482 Color: 486
Size: 260 Color: 73
Size: 258 Color: 59

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 379
Size: 318 Color: 281
Size: 287 Color: 202

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 392
Size: 327 Color: 290
Size: 261 Color: 80

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 437
Size: 285 Color: 195
Size: 269 Color: 124

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 376 Color: 363
Size: 366 Color: 342
Size: 258 Color: 62

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 493
Size: 255 Color: 46
Size: 251 Color: 17

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 473
Size: 270 Color: 127
Size: 262 Color: 89

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 441
Size: 288 Color: 207
Size: 261 Color: 84

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 386 Color: 374
Size: 313 Color: 270
Size: 301 Color: 246

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 499
Size: 251 Color: 19
Size: 250 Color: 12

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 408
Size: 315 Color: 273
Size: 259 Color: 71

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 351
Size: 332 Color: 295
Size: 299 Color: 238

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 451 Color: 442
Size: 278 Color: 166
Size: 271 Color: 137

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 401
Size: 289 Color: 208
Size: 288 Color: 206

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 404 Color: 386
Size: 317 Color: 279
Size: 279 Color: 170

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 485
Size: 263 Color: 98
Size: 256 Color: 50

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 396
Size: 323 Color: 284
Size: 260 Color: 74

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 398
Size: 294 Color: 226
Size: 286 Color: 199

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 364
Size: 362 Color: 335
Size: 261 Color: 82

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 353 Color: 319
Size: 336 Color: 301
Size: 311 Color: 267

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 343
Size: 362 Color: 337
Size: 271 Color: 135

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 488
Size: 263 Color: 95
Size: 252 Color: 30

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 325
Size: 350 Color: 314
Size: 293 Color: 219

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 385
Size: 346 Color: 312
Size: 252 Color: 35

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 405
Size: 326 Color: 287
Size: 250 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 416
Size: 316 Color: 276
Size: 251 Color: 16

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 372
Size: 338 Color: 302
Size: 278 Color: 168

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 358
Size: 335 Color: 299
Size: 292 Color: 215

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 407
Size: 306 Color: 258
Size: 269 Color: 125

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 447
Size: 283 Color: 185
Size: 263 Color: 92

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 378
Size: 323 Color: 285
Size: 282 Color: 178

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 483 Color: 487
Size: 260 Color: 76
Size: 257 Color: 53

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 489
Size: 262 Color: 88
Size: 250 Color: 1

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 323
Size: 354 Color: 321
Size: 289 Color: 209

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 447 Color: 438
Size: 298 Color: 235
Size: 255 Color: 47

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 457
Size: 285 Color: 194
Size: 254 Color: 43

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 480
Size: 273 Color: 143
Size: 251 Color: 22

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 425
Size: 284 Color: 190
Size: 277 Color: 160

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 395
Size: 301 Color: 243
Size: 285 Color: 197

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 345
Size: 342 Color: 307
Size: 290 Color: 210

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 409
Size: 313 Color: 271
Size: 259 Color: 67

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 362 Color: 336
Size: 321 Color: 282
Size: 317 Color: 277

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 461
Size: 278 Color: 165
Size: 260 Color: 75

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 425 Color: 406
Size: 294 Color: 227
Size: 281 Color: 173

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 400 Color: 381
Size: 330 Color: 292
Size: 270 Color: 129

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 458
Size: 274 Color: 147
Size: 265 Color: 106

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 383
Size: 325 Color: 286
Size: 273 Color: 142

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 479
Size: 269 Color: 123
Size: 255 Color: 49

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 330
Size: 358 Color: 329
Size: 284 Color: 189

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 424
Size: 312 Color: 268
Size: 250 Color: 13

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 391
Size: 305 Color: 253
Size: 283 Color: 186

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 435
Size: 288 Color: 205
Size: 267 Color: 117

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 422
Size: 283 Color: 184
Size: 279 Color: 169

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 383 Color: 371
Size: 309 Color: 266
Size: 308 Color: 262

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 469
Size: 277 Color: 162
Size: 258 Color: 64

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 354
Size: 370 Color: 352
Size: 259 Color: 69

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 387
Size: 298 Color: 233
Size: 297 Color: 231

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 347
Size: 344 Color: 308
Size: 288 Color: 204

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 429
Size: 287 Color: 201
Size: 272 Color: 139

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 452
Size: 278 Color: 167
Size: 263 Color: 97

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 455
Size: 281 Color: 175
Size: 259 Color: 66

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 340
Size: 362 Color: 339
Size: 275 Color: 156

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 359 Color: 331
Size: 357 Color: 326
Size: 284 Color: 191

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 410
Size: 307 Color: 260
Size: 265 Color: 104

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 373
Size: 352 Color: 318
Size: 263 Color: 93

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 474
Size: 266 Color: 111
Size: 266 Color: 110

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 359
Size: 334 Color: 296
Size: 293 Color: 220

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 393
Size: 308 Color: 263
Size: 280 Color: 171

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 456
Size: 283 Color: 183
Size: 257 Color: 54

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 397
Size: 316 Color: 275
Size: 265 Color: 108

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 460
Size: 283 Color: 182
Size: 255 Color: 44

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 440 Color: 427
Size: 302 Color: 250
Size: 258 Color: 61

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 440
Size: 285 Color: 196
Size: 266 Color: 116

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 417
Size: 315 Color: 272
Size: 251 Color: 21

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 483
Size: 264 Color: 102
Size: 257 Color: 55

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 491 Color: 490
Size: 257 Color: 57
Size: 252 Color: 34

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 361
Size: 365 Color: 341
Size: 260 Color: 77

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 388
Size: 327 Color: 291
Size: 268 Color: 121

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 478
Size: 266 Color: 113
Size: 262 Color: 91

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 375
Size: 360 Color: 333
Size: 252 Color: 33

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 492
Size: 254 Color: 40
Size: 252 Color: 32

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 362
Size: 357 Color: 324
Size: 268 Color: 119

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 421
Size: 301 Color: 242
Size: 261 Color: 83

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 399
Size: 301 Color: 245
Size: 277 Color: 161

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 418
Size: 285 Color: 192
Size: 281 Color: 176

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 459
Size: 287 Color: 203
Size: 251 Color: 25

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 430
Size: 308 Color: 264
Size: 250 Color: 3

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 470
Size: 284 Color: 188
Size: 250 Color: 14

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 412
Size: 305 Color: 256
Size: 266 Color: 115

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 365
Size: 348 Color: 313
Size: 275 Color: 155

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 482
Size: 266 Color: 112
Size: 257 Color: 56

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 402 Color: 382
Size: 323 Color: 283
Size: 275 Color: 157

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 356
Size: 345 Color: 309
Size: 283 Color: 187

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 472
Size: 267 Color: 118
Size: 266 Color: 114

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 476
Size: 271 Color: 130
Size: 258 Color: 65

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 342 Color: 306
Size: 341 Color: 305
Size: 317 Color: 280

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 451
Size: 292 Color: 217
Size: 250 Color: 15

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 376
Size: 336 Color: 300
Size: 274 Color: 146

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 445
Size: 295 Color: 229
Size: 252 Color: 29

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 432
Size: 306 Color: 257
Size: 250 Color: 6

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 360
Size: 334 Color: 297
Size: 291 Color: 212

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 370
Size: 340 Color: 304
Size: 278 Color: 164

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 408 Color: 389
Size: 300 Color: 239
Size: 292 Color: 214

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 462 Color: 462
Size: 261 Color: 81
Size: 277 Color: 158

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 499 Color: 500
Size: 251 Color: 23
Size: 250 Color: 8

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 403
Size: 309 Color: 265
Size: 268 Color: 120

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 404
Size: 306 Color: 259
Size: 271 Color: 131

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 444
Size: 290 Color: 211
Size: 258 Color: 60

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 394
Size: 312 Color: 269
Size: 275 Color: 154

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 423
Size: 302 Color: 247
Size: 260 Color: 79

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 369
Size: 334 Color: 298
Size: 285 Color: 193

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 433
Size: 291 Color: 213
Size: 265 Color: 107

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 355
Size: 357 Color: 327
Size: 271 Color: 132

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 423 Color: 402
Size: 302 Color: 248
Size: 275 Color: 152

Total size: 167000
Total free space: 0

