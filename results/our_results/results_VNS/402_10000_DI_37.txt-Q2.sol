Capicity Bin: 8080
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 5
Items: 
Size: 4043 Color: 0
Size: 2221 Color: 1
Size: 672 Color: 1
Size: 632 Color: 0
Size: 512 Color: 1

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4250 Color: 0
Size: 3182 Color: 0
Size: 648 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5013 Color: 0
Size: 2557 Color: 1
Size: 510 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5433 Color: 0
Size: 2207 Color: 1
Size: 440 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5572 Color: 1
Size: 2220 Color: 1
Size: 288 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5582 Color: 1
Size: 2110 Color: 0
Size: 388 Color: 1

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5951 Color: 0
Size: 1729 Color: 1
Size: 400 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5972 Color: 1
Size: 1764 Color: 0
Size: 344 Color: 1

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6355 Color: 0
Size: 1533 Color: 0
Size: 192 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6464 Color: 0
Size: 862 Color: 0
Size: 754 Color: 1

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 0
Size: 1112 Color: 1
Size: 442 Color: 1

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6564 Color: 0
Size: 1052 Color: 0
Size: 464 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6676 Color: 0
Size: 1220 Color: 1
Size: 184 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6766 Color: 1
Size: 1118 Color: 1
Size: 196 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6851 Color: 1
Size: 965 Color: 0
Size: 264 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6946 Color: 0
Size: 684 Color: 1
Size: 450 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 7084 Color: 1
Size: 684 Color: 0
Size: 312 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 7204 Color: 0
Size: 636 Color: 1
Size: 240 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 7226 Color: 1
Size: 544 Color: 0
Size: 310 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 7260 Color: 1
Size: 632 Color: 1
Size: 188 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 7268 Color: 1
Size: 724 Color: 0
Size: 88 Color: 0

Bin 22: 1 of cap free
Amount of items: 9
Items: 
Size: 4041 Color: 0
Size: 706 Color: 0
Size: 672 Color: 0
Size: 636 Color: 0
Size: 580 Color: 1
Size: 544 Color: 1
Size: 514 Color: 1
Size: 226 Color: 1
Size: 160 Color: 0

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 4581 Color: 0
Size: 3362 Color: 0
Size: 136 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 5004 Color: 0
Size: 2531 Color: 1
Size: 544 Color: 0

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 5369 Color: 1
Size: 2294 Color: 1
Size: 416 Color: 0

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 2213 Color: 0
Size: 176 Color: 1

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 1
Size: 1545 Color: 0
Size: 258 Color: 0

Bin 28: 1 of cap free
Amount of items: 3
Items: 
Size: 6380 Color: 1
Size: 1551 Color: 0
Size: 148 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6531 Color: 1
Size: 1234 Color: 1
Size: 314 Color: 0

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6579 Color: 0
Size: 1268 Color: 1
Size: 232 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6721 Color: 1
Size: 1222 Color: 1
Size: 136 Color: 0

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6729 Color: 1
Size: 908 Color: 0
Size: 442 Color: 0

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6797 Color: 1
Size: 1002 Color: 0
Size: 280 Color: 0

Bin 34: 2 of cap free
Amount of items: 3
Items: 
Size: 4989 Color: 1
Size: 2917 Color: 0
Size: 172 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 5393 Color: 0
Size: 2261 Color: 1
Size: 424 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 5693 Color: 0
Size: 2241 Color: 1
Size: 144 Color: 0

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 5780 Color: 1
Size: 1566 Color: 0
Size: 732 Color: 1

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 1
Size: 2082 Color: 0
Size: 160 Color: 1

Bin 39: 2 of cap free
Amount of items: 4
Items: 
Size: 5930 Color: 0
Size: 1102 Color: 1
Size: 942 Color: 1
Size: 104 Color: 0

Bin 40: 2 of cap free
Amount of items: 3
Items: 
Size: 6334 Color: 1
Size: 1580 Color: 0
Size: 164 Color: 1

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 6620 Color: 1
Size: 1458 Color: 0

Bin 42: 3 of cap free
Amount of items: 3
Items: 
Size: 4557 Color: 1
Size: 3202 Color: 0
Size: 318 Color: 1

Bin 43: 3 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 0
Size: 2537 Color: 1
Size: 256 Color: 0

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 5890 Color: 0
Size: 1755 Color: 0
Size: 432 Color: 1

Bin 45: 3 of cap free
Amount of items: 3
Items: 
Size: 5946 Color: 0
Size: 1735 Color: 1
Size: 396 Color: 0

Bin 46: 3 of cap free
Amount of items: 3
Items: 
Size: 6379 Color: 1
Size: 1558 Color: 0
Size: 140 Color: 0

Bin 47: 3 of cap free
Amount of items: 3
Items: 
Size: 6555 Color: 0
Size: 1226 Color: 1
Size: 296 Color: 0

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 6988 Color: 0
Size: 1025 Color: 1
Size: 64 Color: 1

Bin 49: 3 of cap free
Amount of items: 2
Items: 
Size: 7046 Color: 1
Size: 1031 Color: 0

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 4533 Color: 1
Size: 3367 Color: 0
Size: 176 Color: 0

Bin 51: 4 of cap free
Amount of items: 4
Items: 
Size: 5668 Color: 1
Size: 2180 Color: 0
Size: 156 Color: 1
Size: 72 Color: 0

Bin 52: 4 of cap free
Amount of items: 3
Items: 
Size: 5975 Color: 0
Size: 1949 Color: 1
Size: 152 Color: 0

Bin 53: 4 of cap free
Amount of items: 2
Items: 
Size: 7044 Color: 0
Size: 1032 Color: 1

Bin 54: 5 of cap free
Amount of items: 2
Items: 
Size: 5743 Color: 0
Size: 2332 Color: 1

Bin 55: 5 of cap free
Amount of items: 3
Items: 
Size: 6439 Color: 1
Size: 1508 Color: 0
Size: 128 Color: 1

Bin 56: 5 of cap free
Amount of items: 3
Items: 
Size: 6876 Color: 1
Size: 1055 Color: 0
Size: 144 Color: 0

Bin 57: 6 of cap free
Amount of items: 2
Items: 
Size: 4798 Color: 1
Size: 3276 Color: 0

Bin 58: 7 of cap free
Amount of items: 2
Items: 
Size: 6262 Color: 1
Size: 1811 Color: 0

Bin 59: 7 of cap free
Amount of items: 3
Items: 
Size: 6610 Color: 0
Size: 1271 Color: 1
Size: 192 Color: 1

Bin 60: 7 of cap free
Amount of items: 2
Items: 
Size: 6940 Color: 1
Size: 1133 Color: 0

Bin 61: 8 of cap free
Amount of items: 4
Items: 
Size: 4242 Color: 1
Size: 2744 Color: 1
Size: 946 Color: 0
Size: 140 Color: 0

Bin 62: 8 of cap free
Amount of items: 2
Items: 
Size: 5980 Color: 0
Size: 2092 Color: 1

Bin 63: 9 of cap free
Amount of items: 2
Items: 
Size: 6195 Color: 1
Size: 1876 Color: 0

Bin 64: 9 of cap free
Amount of items: 2
Items: 
Size: 6820 Color: 1
Size: 1251 Color: 0

Bin 65: 10 of cap free
Amount of items: 3
Items: 
Size: 5938 Color: 1
Size: 1756 Color: 1
Size: 376 Color: 0

Bin 66: 10 of cap free
Amount of items: 2
Items: 
Size: 6938 Color: 1
Size: 1132 Color: 0

Bin 67: 10 of cap free
Amount of items: 2
Items: 
Size: 7234 Color: 0
Size: 836 Color: 1

Bin 68: 11 of cap free
Amount of items: 2
Items: 
Size: 6227 Color: 1
Size: 1842 Color: 0

Bin 69: 12 of cap free
Amount of items: 2
Items: 
Size: 5330 Color: 1
Size: 2738 Color: 0

Bin 70: 12 of cap free
Amount of items: 2
Items: 
Size: 6823 Color: 0
Size: 1245 Color: 1

Bin 71: 13 of cap free
Amount of items: 3
Items: 
Size: 4484 Color: 1
Size: 2911 Color: 0
Size: 672 Color: 1

Bin 72: 14 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 1
Size: 3186 Color: 0
Size: 636 Color: 0

Bin 73: 14 of cap free
Amount of items: 3
Items: 
Size: 4258 Color: 1
Size: 3528 Color: 1
Size: 280 Color: 0

Bin 74: 15 of cap free
Amount of items: 2
Items: 
Size: 6485 Color: 0
Size: 1580 Color: 1

Bin 75: 15 of cap free
Amount of items: 4
Items: 
Size: 6890 Color: 1
Size: 1071 Color: 0
Size: 56 Color: 1
Size: 48 Color: 0

Bin 76: 15 of cap free
Amount of items: 2
Items: 
Size: 6893 Color: 0
Size: 1172 Color: 1

Bin 77: 15 of cap free
Amount of items: 2
Items: 
Size: 7094 Color: 1
Size: 971 Color: 0

Bin 78: 16 of cap free
Amount of items: 2
Items: 
Size: 6436 Color: 0
Size: 1628 Color: 1

Bin 79: 16 of cap free
Amount of items: 2
Items: 
Size: 6923 Color: 0
Size: 1141 Color: 1

Bin 80: 18 of cap free
Amount of items: 3
Items: 
Size: 5909 Color: 0
Size: 1571 Color: 0
Size: 582 Color: 1

Bin 81: 18 of cap free
Amount of items: 2
Items: 
Size: 7013 Color: 0
Size: 1049 Color: 1

Bin 82: 19 of cap free
Amount of items: 2
Items: 
Size: 6679 Color: 1
Size: 1382 Color: 0

Bin 83: 20 of cap free
Amount of items: 4
Items: 
Size: 4156 Color: 0
Size: 2662 Color: 1
Size: 714 Color: 1
Size: 528 Color: 0

Bin 84: 20 of cap free
Amount of items: 4
Items: 
Size: 4890 Color: 0
Size: 2922 Color: 1
Size: 136 Color: 1
Size: 112 Color: 0

Bin 85: 20 of cap free
Amount of items: 2
Items: 
Size: 7242 Color: 1
Size: 818 Color: 0

Bin 86: 24 of cap free
Amount of items: 2
Items: 
Size: 6132 Color: 1
Size: 1924 Color: 0

Bin 87: 24 of cap free
Amount of items: 2
Items: 
Size: 7102 Color: 0
Size: 954 Color: 1

Bin 88: 25 of cap free
Amount of items: 2
Items: 
Size: 6815 Color: 0
Size: 1240 Color: 1

Bin 89: 28 of cap free
Amount of items: 2
Items: 
Size: 6954 Color: 1
Size: 1098 Color: 0

Bin 90: 29 of cap free
Amount of items: 2
Items: 
Size: 6882 Color: 0
Size: 1169 Color: 1

Bin 91: 30 of cap free
Amount of items: 2
Items: 
Size: 4042 Color: 1
Size: 4008 Color: 0

Bin 92: 31 of cap free
Amount of items: 2
Items: 
Size: 4684 Color: 0
Size: 3365 Color: 1

Bin 93: 31 of cap free
Amount of items: 2
Items: 
Size: 5045 Color: 1
Size: 3004 Color: 0

Bin 94: 31 of cap free
Amount of items: 2
Items: 
Size: 6758 Color: 0
Size: 1291 Color: 1

Bin 95: 32 of cap free
Amount of items: 2
Items: 
Size: 7178 Color: 0
Size: 870 Color: 1

Bin 96: 35 of cap free
Amount of items: 2
Items: 
Size: 5037 Color: 0
Size: 3008 Color: 1

Bin 97: 35 of cap free
Amount of items: 2
Items: 
Size: 5468 Color: 1
Size: 2577 Color: 0

Bin 98: 36 of cap free
Amount of items: 2
Items: 
Size: 6713 Color: 0
Size: 1331 Color: 1

Bin 99: 36 of cap free
Amount of items: 2
Items: 
Size: 6917 Color: 1
Size: 1127 Color: 0

Bin 100: 38 of cap free
Amount of items: 2
Items: 
Size: 7038 Color: 1
Size: 1004 Color: 0

Bin 101: 38 of cap free
Amount of items: 2
Items: 
Size: 7220 Color: 0
Size: 822 Color: 1

Bin 102: 39 of cap free
Amount of items: 2
Items: 
Size: 6602 Color: 1
Size: 1439 Color: 0

Bin 103: 40 of cap free
Amount of items: 2
Items: 
Size: 6214 Color: 0
Size: 1826 Color: 1

Bin 104: 40 of cap free
Amount of items: 2
Items: 
Size: 6996 Color: 0
Size: 1044 Color: 1

Bin 105: 42 of cap free
Amount of items: 4
Items: 
Size: 5874 Color: 0
Size: 956 Color: 0
Size: 762 Color: 1
Size: 446 Color: 1

Bin 106: 44 of cap free
Amount of items: 2
Items: 
Size: 6600 Color: 1
Size: 1436 Color: 0

Bin 107: 46 of cap free
Amount of items: 2
Items: 
Size: 7166 Color: 0
Size: 868 Color: 1

Bin 108: 54 of cap free
Amount of items: 27
Items: 
Size: 376 Color: 0
Size: 356 Color: 0
Size: 356 Color: 0
Size: 354 Color: 0
Size: 352 Color: 0
Size: 350 Color: 1
Size: 346 Color: 0
Size: 344 Color: 1
Size: 344 Color: 0
Size: 320 Color: 0
Size: 312 Color: 1
Size: 308 Color: 1
Size: 308 Color: 0
Size: 296 Color: 1
Size: 286 Color: 1
Size: 280 Color: 1
Size: 272 Color: 1
Size: 272 Color: 0
Size: 254 Color: 1
Size: 250 Color: 1
Size: 248 Color: 1
Size: 248 Color: 0
Size: 244 Color: 1
Size: 244 Color: 0
Size: 240 Color: 1
Size: 240 Color: 0
Size: 226 Color: 1

Bin 109: 58 of cap free
Amount of items: 2
Items: 
Size: 6724 Color: 1
Size: 1298 Color: 0

Bin 110: 63 of cap free
Amount of items: 2
Items: 
Size: 6426 Color: 1
Size: 1591 Color: 0

Bin 111: 64 of cap free
Amount of items: 4
Items: 
Size: 4046 Color: 0
Size: 2900 Color: 0
Size: 702 Color: 1
Size: 368 Color: 1

Bin 112: 66 of cap free
Amount of items: 2
Items: 
Size: 6364 Color: 0
Size: 1650 Color: 1

Bin 113: 69 of cap free
Amount of items: 2
Items: 
Size: 5999 Color: 1
Size: 2012 Color: 0

Bin 114: 71 of cap free
Amount of items: 4
Items: 
Size: 4044 Color: 0
Size: 2402 Color: 1
Size: 891 Color: 0
Size: 672 Color: 1

Bin 115: 72 of cap free
Amount of items: 2
Items: 
Size: 4266 Color: 0
Size: 3742 Color: 1

Bin 116: 73 of cap free
Amount of items: 2
Items: 
Size: 6587 Color: 1
Size: 1420 Color: 0

Bin 117: 74 of cap free
Amount of items: 2
Items: 
Size: 4802 Color: 0
Size: 3204 Color: 1

Bin 118: 79 of cap free
Amount of items: 2
Items: 
Size: 6219 Color: 1
Size: 1782 Color: 0

Bin 119: 82 of cap free
Amount of items: 2
Items: 
Size: 6007 Color: 0
Size: 1991 Color: 1

Bin 120: 83 of cap free
Amount of items: 2
Items: 
Size: 5425 Color: 1
Size: 2572 Color: 0

Bin 121: 84 of cap free
Amount of items: 2
Items: 
Size: 6202 Color: 0
Size: 1794 Color: 1

Bin 122: 86 of cap free
Amount of items: 2
Items: 
Size: 5246 Color: 0
Size: 2748 Color: 1

Bin 123: 93 of cap free
Amount of items: 2
Items: 
Size: 6618 Color: 0
Size: 1369 Color: 1

Bin 124: 96 of cap free
Amount of items: 2
Items: 
Size: 5420 Color: 1
Size: 2564 Color: 0

Bin 125: 98 of cap free
Amount of items: 2
Items: 
Size: 4788 Color: 0
Size: 3194 Color: 1

Bin 126: 99 of cap free
Amount of items: 2
Items: 
Size: 5044 Color: 1
Size: 2937 Color: 0

Bin 127: 106 of cap free
Amount of items: 17
Items: 
Size: 592 Color: 0
Size: 590 Color: 0
Size: 586 Color: 0
Size: 512 Color: 1
Size: 506 Color: 0
Size: 504 Color: 1
Size: 504 Color: 0
Size: 496 Color: 1
Size: 496 Color: 0
Size: 456 Color: 0
Size: 440 Color: 1
Size: 432 Color: 0
Size: 416 Color: 0
Size: 364 Color: 1
Size: 364 Color: 1
Size: 360 Color: 1
Size: 356 Color: 1

Bin 128: 106 of cap free
Amount of items: 2
Items: 
Size: 6188 Color: 0
Size: 1786 Color: 1

Bin 129: 127 of cap free
Amount of items: 2
Items: 
Size: 4589 Color: 0
Size: 3364 Color: 1

Bin 130: 127 of cap free
Amount of items: 2
Items: 
Size: 4996 Color: 0
Size: 2957 Color: 1

Bin 131: 131 of cap free
Amount of items: 2
Items: 
Size: 5417 Color: 1
Size: 2532 Color: 0

Bin 132: 134 of cap free
Amount of items: 2
Items: 
Size: 6171 Color: 0
Size: 1775 Color: 1

Bin 133: 4674 of cap free
Amount of items: 17
Items: 
Size: 232 Color: 0
Size: 224 Color: 1
Size: 224 Color: 0
Size: 220 Color: 0
Size: 216 Color: 0
Size: 212 Color: 1
Size: 210 Color: 1
Size: 208 Color: 1
Size: 208 Color: 1
Size: 204 Color: 0
Size: 200 Color: 0
Size: 188 Color: 1
Size: 184 Color: 1
Size: 172 Color: 0
Size: 168 Color: 1
Size: 168 Color: 0
Size: 168 Color: 0

Total size: 1066560
Total free space: 8080

