Capicity Bin: 7896
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 9
Items: 
Size: 3950 Color: 1
Size: 656 Color: 0
Size: 592 Color: 0
Size: 570 Color: 0
Size: 560 Color: 0
Size: 496 Color: 1
Size: 484 Color: 1
Size: 440 Color: 1
Size: 148 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 5594 Color: 0
Size: 2124 Color: 1
Size: 178 Color: 1

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 5804 Color: 1
Size: 1922 Color: 0
Size: 170 Color: 1

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 5849 Color: 1
Size: 1707 Color: 0
Size: 340 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5966 Color: 0
Size: 1634 Color: 0
Size: 296 Color: 1

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 6202 Color: 1
Size: 1262 Color: 1
Size: 432 Color: 0

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 6213 Color: 1
Size: 1283 Color: 1
Size: 400 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 6276 Color: 0
Size: 1484 Color: 1
Size: 136 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6386 Color: 1
Size: 1356 Color: 0
Size: 154 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6556 Color: 0
Size: 1076 Color: 1
Size: 264 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6602 Color: 1
Size: 1082 Color: 1
Size: 212 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6575 Color: 0
Size: 873 Color: 0
Size: 448 Color: 1

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6608 Color: 1
Size: 1122 Color: 0
Size: 166 Color: 1

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6609 Color: 0
Size: 973 Color: 1
Size: 314 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6685 Color: 0
Size: 947 Color: 0
Size: 264 Color: 1

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6714 Color: 1
Size: 742 Color: 1
Size: 440 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6724 Color: 0
Size: 608 Color: 1
Size: 564 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6839 Color: 1
Size: 841 Color: 0
Size: 216 Color: 1

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6940 Color: 1
Size: 632 Color: 1
Size: 324 Color: 0

Bin 20: 1 of cap free
Amount of items: 7
Items: 
Size: 3954 Color: 1
Size: 961 Color: 0
Size: 764 Color: 1
Size: 722 Color: 0
Size: 674 Color: 1
Size: 572 Color: 0
Size: 248 Color: 1

Bin 21: 1 of cap free
Amount of items: 5
Items: 
Size: 4076 Color: 0
Size: 1548 Color: 0
Size: 1073 Color: 1
Size: 852 Color: 0
Size: 346 Color: 1

Bin 22: 1 of cap free
Amount of items: 3
Items: 
Size: 4636 Color: 1
Size: 2873 Color: 0
Size: 386 Color: 1

Bin 23: 1 of cap free
Amount of items: 3
Items: 
Size: 4872 Color: 0
Size: 2847 Color: 0
Size: 176 Color: 1

Bin 24: 1 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 2167 Color: 0
Size: 260 Color: 1

Bin 25: 1 of cap free
Amount of items: 3
Items: 
Size: 5573 Color: 0
Size: 2098 Color: 0
Size: 224 Color: 1

Bin 26: 1 of cap free
Amount of items: 3
Items: 
Size: 5589 Color: 0
Size: 2142 Color: 1
Size: 164 Color: 0

Bin 27: 1 of cap free
Amount of items: 3
Items: 
Size: 5666 Color: 0
Size: 1923 Color: 0
Size: 306 Color: 1

Bin 28: 1 of cap free
Amount of items: 2
Items: 
Size: 6033 Color: 0
Size: 1862 Color: 1

Bin 29: 1 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 0
Size: 1686 Color: 0
Size: 160 Color: 1

Bin 30: 1 of cap free
Amount of items: 3
Items: 
Size: 6247 Color: 0
Size: 1468 Color: 1
Size: 180 Color: 0

Bin 31: 1 of cap free
Amount of items: 3
Items: 
Size: 6322 Color: 0
Size: 949 Color: 0
Size: 624 Color: 1

Bin 32: 1 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 0
Size: 1289 Color: 1
Size: 132 Color: 1

Bin 33: 1 of cap free
Amount of items: 3
Items: 
Size: 6905 Color: 1
Size: 830 Color: 0
Size: 160 Color: 1

Bin 34: 2 of cap free
Amount of items: 19
Items: 
Size: 512 Color: 0
Size: 494 Color: 0
Size: 492 Color: 0
Size: 480 Color: 1
Size: 480 Color: 1
Size: 438 Color: 1
Size: 434 Color: 1
Size: 424 Color: 0
Size: 416 Color: 1
Size: 416 Color: 1
Size: 416 Color: 0
Size: 408 Color: 1
Size: 390 Color: 0
Size: 380 Color: 1
Size: 376 Color: 0
Size: 372 Color: 1
Size: 368 Color: 0
Size: 342 Color: 0
Size: 256 Color: 1

Bin 35: 2 of cap free
Amount of items: 3
Items: 
Size: 4446 Color: 1
Size: 3284 Color: 1
Size: 164 Color: 0

Bin 36: 2 of cap free
Amount of items: 3
Items: 
Size: 5265 Color: 0
Size: 2181 Color: 0
Size: 448 Color: 1

Bin 37: 2 of cap free
Amount of items: 3
Items: 
Size: 5330 Color: 1
Size: 2406 Color: 0
Size: 158 Color: 0

Bin 38: 2 of cap free
Amount of items: 3
Items: 
Size: 6065 Color: 0
Size: 1721 Color: 1
Size: 108 Color: 1

Bin 39: 2 of cap free
Amount of items: 2
Items: 
Size: 6178 Color: 1
Size: 1716 Color: 0

Bin 40: 2 of cap free
Amount of items: 2
Items: 
Size: 6729 Color: 1
Size: 1165 Color: 0

Bin 41: 2 of cap free
Amount of items: 2
Items: 
Size: 6761 Color: 0
Size: 1133 Color: 1

Bin 42: 2 of cap free
Amount of items: 2
Items: 
Size: 6822 Color: 1
Size: 1072 Color: 0

Bin 43: 2 of cap free
Amount of items: 2
Items: 
Size: 6868 Color: 1
Size: 1026 Color: 0

Bin 44: 3 of cap free
Amount of items: 3
Items: 
Size: 4465 Color: 1
Size: 3286 Color: 1
Size: 142 Color: 0

Bin 45: 3 of cap free
Amount of items: 4
Items: 
Size: 4869 Color: 0
Size: 2724 Color: 1
Size: 168 Color: 1
Size: 132 Color: 0

Bin 46: 3 of cap free
Amount of items: 2
Items: 
Size: 5428 Color: 0
Size: 2465 Color: 1

Bin 47: 3 of cap free
Amount of items: 2
Items: 
Size: 5833 Color: 0
Size: 2060 Color: 1

Bin 48: 3 of cap free
Amount of items: 3
Items: 
Size: 6465 Color: 1
Size: 892 Color: 0
Size: 536 Color: 0

Bin 49: 4 of cap free
Amount of items: 3
Items: 
Size: 4425 Color: 0
Size: 2576 Color: 1
Size: 891 Color: 0

Bin 50: 4 of cap free
Amount of items: 3
Items: 
Size: 4449 Color: 0
Size: 3291 Color: 1
Size: 152 Color: 0

Bin 51: 4 of cap free
Amount of items: 3
Items: 
Size: 5281 Color: 1
Size: 2475 Color: 0
Size: 136 Color: 1

Bin 52: 4 of cap free
Amount of items: 2
Items: 
Size: 6351 Color: 1
Size: 1541 Color: 0

Bin 53: 6 of cap free
Amount of items: 3
Items: 
Size: 5817 Color: 0
Size: 1553 Color: 0
Size: 520 Color: 1

Bin 54: 6 of cap free
Amount of items: 2
Items: 
Size: 6666 Color: 0
Size: 1224 Color: 1

Bin 55: 6 of cap free
Amount of items: 2
Items: 
Size: 6849 Color: 0
Size: 1041 Color: 1

Bin 56: 6 of cap free
Amount of items: 2
Items: 
Size: 7030 Color: 1
Size: 860 Color: 0

Bin 57: 7 of cap free
Amount of items: 3
Items: 
Size: 4481 Color: 1
Size: 3188 Color: 1
Size: 220 Color: 0

Bin 58: 7 of cap free
Amount of items: 3
Items: 
Size: 6044 Color: 1
Size: 1733 Color: 0
Size: 112 Color: 1

Bin 59: 7 of cap free
Amount of items: 2
Items: 
Size: 6259 Color: 0
Size: 1630 Color: 1

Bin 60: 7 of cap free
Amount of items: 2
Items: 
Size: 6809 Color: 1
Size: 1080 Color: 0

Bin 61: 7 of cap free
Amount of items: 2
Items: 
Size: 7090 Color: 0
Size: 799 Color: 1

Bin 62: 8 of cap free
Amount of items: 30
Items: 
Size: 350 Color: 1
Size: 344 Color: 1
Size: 336 Color: 1
Size: 336 Color: 1
Size: 310 Color: 1
Size: 304 Color: 1
Size: 304 Color: 0
Size: 296 Color: 1
Size: 296 Color: 0
Size: 288 Color: 0
Size: 284 Color: 1
Size: 280 Color: 0
Size: 280 Color: 0
Size: 274 Color: 1
Size: 272 Color: 1
Size: 256 Color: 0
Size: 248 Color: 0
Size: 248 Color: 0
Size: 238 Color: 0
Size: 236 Color: 1
Size: 234 Color: 0
Size: 232 Color: 1
Size: 232 Color: 0
Size: 224 Color: 1
Size: 214 Color: 1
Size: 208 Color: 0
Size: 208 Color: 0
Size: 206 Color: 0
Size: 206 Color: 0
Size: 144 Color: 1

Bin 63: 8 of cap free
Amount of items: 3
Items: 
Size: 4748 Color: 1
Size: 2988 Color: 0
Size: 152 Color: 0

Bin 64: 8 of cap free
Amount of items: 2
Items: 
Size: 5010 Color: 0
Size: 2878 Color: 1

Bin 65: 8 of cap free
Amount of items: 2
Items: 
Size: 6866 Color: 0
Size: 1022 Color: 1

Bin 66: 9 of cap free
Amount of items: 2
Items: 
Size: 6980 Color: 0
Size: 907 Color: 1

Bin 67: 10 of cap free
Amount of items: 3
Items: 
Size: 4244 Color: 0
Size: 3290 Color: 1
Size: 352 Color: 0

Bin 68: 10 of cap free
Amount of items: 3
Items: 
Size: 5356 Color: 0
Size: 2426 Color: 1
Size: 104 Color: 1

Bin 69: 10 of cap free
Amount of items: 2
Items: 
Size: 6483 Color: 0
Size: 1403 Color: 1

Bin 70: 10 of cap free
Amount of items: 3
Items: 
Size: 6612 Color: 1
Size: 1186 Color: 0
Size: 88 Color: 1

Bin 71: 11 of cap free
Amount of items: 2
Items: 
Size: 6397 Color: 1
Size: 1488 Color: 0

Bin 72: 11 of cap free
Amount of items: 2
Items: 
Size: 7058 Color: 0
Size: 827 Color: 1

Bin 73: 11 of cap free
Amount of items: 2
Items: 
Size: 7078 Color: 1
Size: 807 Color: 0

Bin 74: 12 of cap free
Amount of items: 3
Items: 
Size: 4918 Color: 0
Size: 2818 Color: 1
Size: 148 Color: 1

Bin 75: 13 of cap free
Amount of items: 2
Items: 
Size: 6356 Color: 1
Size: 1527 Color: 0

Bin 76: 14 of cap free
Amount of items: 2
Items: 
Size: 6102 Color: 1
Size: 1780 Color: 0

Bin 77: 14 of cap free
Amount of items: 2
Items: 
Size: 6902 Color: 1
Size: 980 Color: 0

Bin 78: 15 of cap free
Amount of items: 2
Items: 
Size: 6467 Color: 1
Size: 1414 Color: 0

Bin 79: 15 of cap free
Amount of items: 2
Items: 
Size: 6551 Color: 0
Size: 1330 Color: 1

Bin 80: 16 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 1
Size: 2116 Color: 0

Bin 81: 17 of cap free
Amount of items: 2
Items: 
Size: 5942 Color: 0
Size: 1937 Color: 1

Bin 82: 17 of cap free
Amount of items: 2
Items: 
Size: 6745 Color: 1
Size: 1134 Color: 0

Bin 83: 17 of cap free
Amount of items: 2
Items: 
Size: 6778 Color: 0
Size: 1101 Color: 1

Bin 84: 19 of cap free
Amount of items: 8
Items: 
Size: 3949 Color: 0
Size: 656 Color: 0
Size: 656 Color: 0
Size: 656 Color: 0
Size: 568 Color: 1
Size: 504 Color: 1
Size: 504 Color: 1
Size: 384 Color: 1

Bin 85: 20 of cap free
Amount of items: 2
Items: 
Size: 6865 Color: 0
Size: 1011 Color: 1

Bin 86: 20 of cap free
Amount of items: 3
Items: 
Size: 6934 Color: 0
Size: 862 Color: 1
Size: 80 Color: 0

Bin 87: 21 of cap free
Amount of items: 2
Items: 
Size: 4939 Color: 0
Size: 2936 Color: 1

Bin 88: 21 of cap free
Amount of items: 2
Items: 
Size: 6009 Color: 1
Size: 1866 Color: 0

Bin 89: 21 of cap free
Amount of items: 2
Items: 
Size: 6302 Color: 1
Size: 1573 Color: 0

Bin 90: 21 of cap free
Amount of items: 2
Items: 
Size: 6889 Color: 0
Size: 986 Color: 1

Bin 91: 22 of cap free
Amount of items: 2
Items: 
Size: 6499 Color: 1
Size: 1375 Color: 0

Bin 92: 22 of cap free
Amount of items: 2
Items: 
Size: 7006 Color: 0
Size: 868 Color: 1

Bin 93: 23 of cap free
Amount of items: 2
Items: 
Size: 6939 Color: 0
Size: 934 Color: 1

Bin 94: 24 of cap free
Amount of items: 2
Items: 
Size: 5844 Color: 0
Size: 2028 Color: 1

Bin 95: 24 of cap free
Amount of items: 2
Items: 
Size: 6124 Color: 1
Size: 1748 Color: 0

Bin 96: 24 of cap free
Amount of items: 2
Items: 
Size: 6621 Color: 1
Size: 1251 Color: 0

Bin 97: 24 of cap free
Amount of items: 2
Items: 
Size: 6860 Color: 1
Size: 1012 Color: 0

Bin 98: 26 of cap free
Amount of items: 2
Items: 
Size: 7068 Color: 1
Size: 802 Color: 0

Bin 99: 28 of cap free
Amount of items: 2
Items: 
Size: 6970 Color: 0
Size: 898 Color: 1

Bin 100: 31 of cap free
Amount of items: 2
Items: 
Size: 6674 Color: 1
Size: 1191 Color: 0

Bin 101: 32 of cap free
Amount of items: 2
Items: 
Size: 5382 Color: 1
Size: 2482 Color: 0

Bin 102: 32 of cap free
Amount of items: 2
Items: 
Size: 6550 Color: 1
Size: 1314 Color: 0

Bin 103: 32 of cap free
Amount of items: 2
Items: 
Size: 6684 Color: 0
Size: 1180 Color: 1

Bin 104: 34 of cap free
Amount of items: 2
Items: 
Size: 6538 Color: 0
Size: 1324 Color: 1

Bin 105: 34 of cap free
Amount of items: 3
Items: 
Size: 6929 Color: 0
Size: 861 Color: 1
Size: 72 Color: 0

Bin 106: 35 of cap free
Amount of items: 2
Items: 
Size: 6828 Color: 0
Size: 1033 Color: 1

Bin 107: 36 of cap free
Amount of items: 2
Items: 
Size: 3956 Color: 1
Size: 3904 Color: 0

Bin 108: 45 of cap free
Amount of items: 2
Items: 
Size: 5658 Color: 0
Size: 2193 Color: 1

Bin 109: 47 of cap free
Amount of items: 2
Items: 
Size: 6484 Color: 1
Size: 1365 Color: 0

Bin 110: 48 of cap free
Amount of items: 2
Items: 
Size: 4804 Color: 0
Size: 3044 Color: 1

Bin 111: 48 of cap free
Amount of items: 2
Items: 
Size: 6657 Color: 1
Size: 1191 Color: 0

Bin 112: 48 of cap free
Amount of items: 2
Items: 
Size: 7098 Color: 1
Size: 750 Color: 0

Bin 113: 49 of cap free
Amount of items: 2
Items: 
Size: 4986 Color: 0
Size: 2861 Color: 1

Bin 114: 53 of cap free
Amount of items: 3
Items: 
Size: 6827 Color: 0
Size: 976 Color: 1
Size: 40 Color: 0

Bin 115: 57 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 1
Size: 2205 Color: 0
Size: 1124 Color: 0

Bin 116: 59 of cap free
Amount of items: 2
Items: 
Size: 6084 Color: 0
Size: 1753 Color: 1

Bin 117: 64 of cap free
Amount of items: 2
Items: 
Size: 6316 Color: 0
Size: 1516 Color: 1

Bin 118: 65 of cap free
Amount of items: 2
Items: 
Size: 5251 Color: 0
Size: 2580 Color: 1

Bin 119: 65 of cap free
Amount of items: 2
Items: 
Size: 5874 Color: 0
Size: 1957 Color: 1

Bin 120: 66 of cap free
Amount of items: 2
Items: 
Size: 6396 Color: 1
Size: 1434 Color: 0

Bin 121: 68 of cap free
Amount of items: 2
Items: 
Size: 6649 Color: 0
Size: 1179 Color: 1

Bin 122: 76 of cap free
Amount of items: 2
Items: 
Size: 4927 Color: 1
Size: 2893 Color: 0

Bin 123: 76 of cap free
Amount of items: 2
Items: 
Size: 5297 Color: 1
Size: 2523 Color: 0

Bin 124: 77 of cap free
Amount of items: 3
Items: 
Size: 5793 Color: 0
Size: 1252 Color: 1
Size: 774 Color: 1

Bin 125: 79 of cap free
Amount of items: 2
Items: 
Size: 5549 Color: 1
Size: 2268 Color: 0

Bin 126: 86 of cap free
Amount of items: 2
Items: 
Size: 6312 Color: 0
Size: 1498 Color: 1

Bin 127: 88 of cap free
Amount of items: 2
Items: 
Size: 5180 Color: 1
Size: 2628 Color: 0

Bin 128: 88 of cap free
Amount of items: 2
Items: 
Size: 5364 Color: 0
Size: 2444 Color: 1

Bin 129: 89 of cap free
Amount of items: 2
Items: 
Size: 4518 Color: 1
Size: 3289 Color: 0

Bin 130: 97 of cap free
Amount of items: 7
Items: 
Size: 3951 Color: 1
Size: 692 Color: 0
Size: 682 Color: 0
Size: 666 Color: 0
Size: 656 Color: 1
Size: 578 Color: 1
Size: 574 Color: 1

Bin 131: 102 of cap free
Amount of items: 2
Items: 
Size: 4972 Color: 0
Size: 2822 Color: 1

Bin 132: 108 of cap free
Amount of items: 2
Items: 
Size: 4316 Color: 1
Size: 3472 Color: 0

Bin 133: 5134 of cap free
Amount of items: 15
Items: 
Size: 204 Color: 1
Size: 200 Color: 1
Size: 200 Color: 0
Size: 200 Color: 0
Size: 196 Color: 0
Size: 194 Color: 0
Size: 192 Color: 1
Size: 190 Color: 0
Size: 188 Color: 1
Size: 184 Color: 0
Size: 176 Color: 0
Size: 174 Color: 1
Size: 168 Color: 1
Size: 168 Color: 0
Size: 128 Color: 1

Total size: 1042272
Total free space: 7896

