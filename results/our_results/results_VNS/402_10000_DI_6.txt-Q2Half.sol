Capicity Bin: 6528
Lower Bound: 132

Bins used: 133
Amount of Colors: 2

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 3966 Color: 1
Size: 2086 Color: 1
Size: 476 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 1
Size: 2138 Color: 1
Size: 84 Color: 0

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 3332 Color: 1
Size: 2724 Color: 1
Size: 472 Color: 0

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 3265 Color: 1
Size: 2721 Color: 1
Size: 542 Color: 0

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 3268 Color: 1
Size: 2120 Color: 1
Size: 1140 Color: 0

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 3266 Color: 1
Size: 2722 Color: 1
Size: 540 Color: 0

Bin 7: 0 of cap free
Amount of items: 4
Items: 
Size: 3272 Color: 1
Size: 1768 Color: 0
Size: 1344 Color: 1
Size: 144 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 3273 Color: 1
Size: 2713 Color: 1
Size: 542 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 3274 Color: 1
Size: 2714 Color: 1
Size: 540 Color: 0

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 3282 Color: 1
Size: 2706 Color: 1
Size: 540 Color: 0

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 3284 Color: 1
Size: 2708 Color: 1
Size: 536 Color: 0

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 3290 Color: 1
Size: 2702 Color: 1
Size: 536 Color: 0

Bin 13: 0 of cap free
Amount of items: 4
Items: 
Size: 3464 Color: 1
Size: 2728 Color: 1
Size: 304 Color: 0
Size: 32 Color: 0

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 3764 Color: 1
Size: 2268 Color: 1
Size: 496 Color: 0

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 3787 Color: 1
Size: 2285 Color: 1
Size: 456 Color: 0

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 3992 Color: 1
Size: 2424 Color: 1
Size: 112 Color: 0

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 4200 Color: 1
Size: 2208 Color: 1
Size: 120 Color: 0

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 4232 Color: 1
Size: 2120 Color: 1
Size: 176 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 4340 Color: 1
Size: 1772 Color: 1
Size: 416 Color: 0

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 4373 Color: 1
Size: 1797 Color: 1
Size: 358 Color: 0

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 4404 Color: 1
Size: 1828 Color: 1
Size: 296 Color: 0

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 4405 Color: 1
Size: 1771 Color: 1
Size: 352 Color: 0

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 4424 Color: 1
Size: 1928 Color: 1
Size: 176 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 4510 Color: 1
Size: 1490 Color: 1
Size: 528 Color: 0

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 4584 Color: 1
Size: 1576 Color: 1
Size: 368 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 4604 Color: 1
Size: 1566 Color: 1
Size: 358 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 4648 Color: 1
Size: 1504 Color: 1
Size: 376 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 4650 Color: 1
Size: 1654 Color: 1
Size: 224 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 4710 Color: 1
Size: 1518 Color: 1
Size: 300 Color: 0

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 4776 Color: 1
Size: 1380 Color: 1
Size: 372 Color: 0

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 4831 Color: 1
Size: 1385 Color: 1
Size: 312 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 4870 Color: 1
Size: 1322 Color: 1
Size: 336 Color: 0

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 4902 Color: 1
Size: 1382 Color: 1
Size: 244 Color: 0

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 4942 Color: 1
Size: 1234 Color: 1
Size: 352 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 4968 Color: 1
Size: 1192 Color: 1
Size: 368 Color: 0

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 5012 Color: 1
Size: 1268 Color: 1
Size: 248 Color: 0

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 5035 Color: 1
Size: 1241 Color: 1
Size: 252 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 5039 Color: 1
Size: 1241 Color: 1
Size: 248 Color: 0

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 5048 Color: 1
Size: 1240 Color: 1
Size: 240 Color: 0

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 5050 Color: 1
Size: 1196 Color: 1
Size: 282 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 5060 Color: 1
Size: 1228 Color: 1
Size: 240 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 5072 Color: 1
Size: 1232 Color: 1
Size: 224 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 5098 Color: 1
Size: 1070 Color: 1
Size: 360 Color: 0

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 5112 Color: 1
Size: 1304 Color: 1
Size: 112 Color: 0

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 5164 Color: 1
Size: 1044 Color: 1
Size: 320 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 5178 Color: 1
Size: 1126 Color: 1
Size: 224 Color: 0

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 5183 Color: 1
Size: 1121 Color: 1
Size: 224 Color: 0

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 5198 Color: 1
Size: 1110 Color: 1
Size: 220 Color: 0

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 5204 Color: 1
Size: 1108 Color: 1
Size: 216 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 5205 Color: 1
Size: 1103 Color: 1
Size: 220 Color: 0

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 5240 Color: 1
Size: 1012 Color: 1
Size: 276 Color: 0

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 5246 Color: 1
Size: 814 Color: 1
Size: 468 Color: 0

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 5252 Color: 1
Size: 1080 Color: 1
Size: 196 Color: 0

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 5284 Color: 1
Size: 1008 Color: 1
Size: 236 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 5303 Color: 1
Size: 1021 Color: 1
Size: 204 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 5310 Color: 1
Size: 1018 Color: 1
Size: 200 Color: 0

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 5316 Color: 1
Size: 930 Color: 1
Size: 282 Color: 0

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 5342 Color: 1
Size: 990 Color: 1
Size: 196 Color: 0

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 5348 Color: 1
Size: 908 Color: 1
Size: 272 Color: 0

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 5352 Color: 1
Size: 968 Color: 1
Size: 208 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 5414 Color: 1
Size: 834 Color: 1
Size: 280 Color: 0

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 5442 Color: 1
Size: 906 Color: 1
Size: 180 Color: 0

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 5444 Color: 1
Size: 884 Color: 1
Size: 200 Color: 0

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 5450 Color: 1
Size: 902 Color: 1
Size: 176 Color: 0

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 5464 Color: 1
Size: 888 Color: 1
Size: 176 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 5468 Color: 1
Size: 748 Color: 1
Size: 312 Color: 0

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 5524 Color: 1
Size: 844 Color: 1
Size: 160 Color: 0

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 5548 Color: 1
Size: 820 Color: 1
Size: 160 Color: 0

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 5554 Color: 1
Size: 766 Color: 1
Size: 208 Color: 0

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 5578 Color: 1
Size: 826 Color: 1
Size: 124 Color: 0

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 5592 Color: 1
Size: 724 Color: 1
Size: 212 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 5606 Color: 1
Size: 794 Color: 1
Size: 128 Color: 0

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 5610 Color: 1
Size: 734 Color: 1
Size: 184 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 5624 Color: 1
Size: 792 Color: 1
Size: 112 Color: 0

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 5636 Color: 1
Size: 728 Color: 1
Size: 164 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 5654 Color: 1
Size: 730 Color: 1
Size: 144 Color: 0

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 5656 Color: 1
Size: 720 Color: 1
Size: 152 Color: 0

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 5660 Color: 1
Size: 738 Color: 1
Size: 130 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 5688 Color: 1
Size: 712 Color: 1
Size: 128 Color: 0

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 5690 Color: 1
Size: 686 Color: 1
Size: 152 Color: 0

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 5710 Color: 1
Size: 770 Color: 1
Size: 48 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 5720 Color: 1
Size: 644 Color: 1
Size: 164 Color: 0

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 5722 Color: 1
Size: 542 Color: 0
Size: 264 Color: 1

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 5732 Color: 1
Size: 604 Color: 1
Size: 192 Color: 0

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 5738 Color: 1
Size: 590 Color: 1
Size: 200 Color: 0

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 5756 Color: 1
Size: 720 Color: 1
Size: 52 Color: 0

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 5770 Color: 1
Size: 634 Color: 1
Size: 124 Color: 0

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 5788 Color: 1
Size: 728 Color: 1
Size: 12 Color: 0

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 5812 Color: 1
Size: 400 Color: 1
Size: 316 Color: 0

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 5832 Color: 1
Size: 580 Color: 1
Size: 116 Color: 0

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 5834 Color: 1
Size: 582 Color: 1
Size: 112 Color: 0

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 5836 Color: 1
Size: 416 Color: 0
Size: 276 Color: 1

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 5848 Color: 1
Size: 644 Color: 1
Size: 36 Color: 0

Bin 94: 1 of cap free
Amount of items: 3
Items: 
Size: 3624 Color: 1
Size: 2711 Color: 1
Size: 192 Color: 0

Bin 95: 1 of cap free
Amount of items: 3
Items: 
Size: 4069 Color: 1
Size: 2394 Color: 1
Size: 64 Color: 0

Bin 96: 1 of cap free
Amount of items: 3
Items: 
Size: 4096 Color: 1
Size: 2351 Color: 1
Size: 80 Color: 0

Bin 97: 1 of cap free
Amount of items: 3
Items: 
Size: 4478 Color: 1
Size: 1801 Color: 1
Size: 248 Color: 0

Bin 98: 1 of cap free
Amount of items: 3
Items: 
Size: 4512 Color: 1
Size: 1567 Color: 1
Size: 448 Color: 0

Bin 99: 1 of cap free
Amount of items: 3
Items: 
Size: 4623 Color: 1
Size: 1784 Color: 1
Size: 120 Color: 0

Bin 100: 1 of cap free
Amount of items: 3
Items: 
Size: 4678 Color: 1
Size: 1429 Color: 1
Size: 420 Color: 0

Bin 101: 1 of cap free
Amount of items: 3
Items: 
Size: 4835 Color: 1
Size: 1564 Color: 1
Size: 128 Color: 0

Bin 102: 1 of cap free
Amount of items: 3
Items: 
Size: 4867 Color: 1
Size: 1420 Color: 1
Size: 240 Color: 0

Bin 103: 1 of cap free
Amount of items: 3
Items: 
Size: 4876 Color: 1
Size: 1411 Color: 1
Size: 240 Color: 0

Bin 104: 1 of cap free
Amount of items: 3
Items: 
Size: 5157 Color: 1
Size: 1258 Color: 1
Size: 112 Color: 0

Bin 105: 1 of cap free
Amount of items: 5
Items: 
Size: 2717 Color: 1
Size: 1682 Color: 1
Size: 1604 Color: 1
Size: 304 Color: 0
Size: 220 Color: 0

Bin 106: 1 of cap free
Amount of items: 4
Items: 
Size: 3269 Color: 1
Size: 2114 Color: 1
Size: 984 Color: 0
Size: 160 Color: 0

Bin 107: 2 of cap free
Amount of items: 3
Items: 
Size: 5368 Color: 1
Size: 702 Color: 1
Size: 456 Color: 0

Bin 108: 2 of cap free
Amount of items: 3
Items: 
Size: 3658 Color: 1
Size: 2724 Color: 1
Size: 144 Color: 0

Bin 109: 2 of cap free
Amount of items: 3
Items: 
Size: 3994 Color: 1
Size: 2308 Color: 1
Size: 224 Color: 0

Bin 110: 2 of cap free
Amount of items: 3
Items: 
Size: 4026 Color: 1
Size: 1972 Color: 1
Size: 528 Color: 0

Bin 111: 2 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 1
Size: 2172 Color: 1
Size: 80 Color: 0

Bin 112: 2 of cap free
Amount of items: 3
Items: 
Size: 4308 Color: 1
Size: 1882 Color: 1
Size: 336 Color: 0

Bin 113: 2 of cap free
Amount of items: 3
Items: 
Size: 4649 Color: 1
Size: 1589 Color: 1
Size: 288 Color: 0

Bin 114: 2 of cap free
Amount of items: 3
Items: 
Size: 4660 Color: 1
Size: 1526 Color: 1
Size: 340 Color: 0

Bin 115: 2 of cap free
Amount of items: 3
Items: 
Size: 4828 Color: 1
Size: 1194 Color: 1
Size: 504 Color: 0

Bin 116: 2 of cap free
Amount of items: 3
Items: 
Size: 5130 Color: 1
Size: 988 Color: 1
Size: 408 Color: 0

Bin 117: 2 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 1
Size: 584 Color: 1
Size: 136 Color: 0

Bin 118: 2 of cap free
Amount of items: 4
Items: 
Size: 4742 Color: 1
Size: 1464 Color: 1
Size: 160 Color: 0
Size: 160 Color: 0

Bin 119: 3 of cap free
Amount of items: 3
Items: 
Size: 4135 Color: 1
Size: 1854 Color: 1
Size: 536 Color: 0

Bin 120: 3 of cap free
Amount of items: 3
Items: 
Size: 4164 Color: 1
Size: 1881 Color: 1
Size: 480 Color: 0

Bin 121: 4 of cap free
Amount of items: 3
Items: 
Size: 3812 Color: 1
Size: 2568 Color: 1
Size: 144 Color: 0

Bin 122: 4 of cap free
Amount of items: 3
Items: 
Size: 5009 Color: 1
Size: 1415 Color: 1
Size: 100 Color: 0

Bin 123: 4 of cap free
Amount of items: 2
Items: 
Size: 5764 Color: 1
Size: 760 Color: 0

Bin 124: 6 of cap free
Amount of items: 3
Items: 
Size: 5822 Color: 1
Size: 620 Color: 1
Size: 80 Color: 0

Bin 125: 6 of cap free
Amount of items: 3
Items: 
Size: 3690 Color: 1
Size: 2640 Color: 1
Size: 192 Color: 0

Bin 126: 6 of cap free
Amount of items: 3
Items: 
Size: 4124 Color: 1
Size: 2366 Color: 1
Size: 32 Color: 0

Bin 127: 7 of cap free
Amount of items: 3
Items: 
Size: 4271 Color: 1
Size: 1710 Color: 1
Size: 540 Color: 0

Bin 128: 7 of cap free
Amount of items: 3
Items: 
Size: 3709 Color: 1
Size: 2668 Color: 1
Size: 144 Color: 0

Bin 129: 11 of cap free
Amount of items: 5
Items: 
Size: 3300 Color: 1
Size: 2149 Color: 1
Size: 668 Color: 1
Size: 256 Color: 0
Size: 144 Color: 0

Bin 130: 22 of cap free
Amount of items: 6
Items: 
Size: 2051 Color: 1
Size: 2004 Color: 1
Size: 1267 Color: 1
Size: 568 Color: 0
Size: 424 Color: 0
Size: 192 Color: 0

Bin 131: 998 of cap free
Amount of items: 1
Items: 
Size: 5530 Color: 1

Bin 132: 2159 of cap free
Amount of items: 1
Items: 
Size: 4369 Color: 1

Bin 133: 3251 of cap free
Amount of items: 1
Items: 
Size: 3277 Color: 1

Total size: 861696
Total free space: 6528

