Capicity Bin: 1000
Lower Bound: 83

Bins used: 83
Amount of Colors: 5

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 419 Color: 2
Size: 317 Color: 4
Size: 264 Color: 0

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 405 Color: 0
Size: 314 Color: 0
Size: 281 Color: 4

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 430 Color: 4
Size: 299 Color: 0
Size: 271 Color: 2

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 1
Size: 263 Color: 1
Size: 250 Color: 4

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 310 Color: 2
Size: 269 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 3
Size: 264 Color: 4
Size: 252 Color: 3

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 2
Size: 356 Color: 2
Size: 270 Color: 0

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 2
Size: 360 Color: 3
Size: 253 Color: 0

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 3
Size: 353 Color: 3
Size: 263 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 4
Size: 273 Color: 1
Size: 261 Color: 3

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 363 Color: 4
Size: 335 Color: 1
Size: 302 Color: 3

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 0
Size: 286 Color: 0
Size: 281 Color: 3

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 2
Size: 311 Color: 4
Size: 288 Color: 3

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 324 Color: 0
Size: 266 Color: 4

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 396 Color: 4
Size: 349 Color: 2
Size: 255 Color: 3

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 496 Color: 2
Size: 254 Color: 4
Size: 250 Color: 4

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 2
Size: 292 Color: 0
Size: 252 Color: 3

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 345 Color: 0
Size: 282 Color: 3

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 0
Size: 285 Color: 3
Size: 256 Color: 4

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 467 Color: 1
Size: 274 Color: 2
Size: 259 Color: 4

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 2
Size: 308 Color: 1
Size: 278 Color: 1

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 2
Size: 343 Color: 0
Size: 250 Color: 4

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 255 Color: 3
Size: 251 Color: 0

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 286 Color: 0
Size: 269 Color: 2

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 350 Color: 4
Size: 260 Color: 0

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 431 Color: 3
Size: 311 Color: 3
Size: 258 Color: 0

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 4
Size: 329 Color: 0
Size: 274 Color: 4

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 3
Size: 329 Color: 1
Size: 290 Color: 0

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 481 Color: 1
Size: 267 Color: 0
Size: 252 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 4
Size: 300 Color: 0
Size: 250 Color: 2

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 442 Color: 1
Size: 287 Color: 4
Size: 271 Color: 0

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 4
Size: 269 Color: 3
Size: 255 Color: 4

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 2
Size: 285 Color: 4
Size: 262 Color: 1

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 365 Color: 3
Size: 383 Color: 0
Size: 252 Color: 0

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 4
Size: 263 Color: 3
Size: 253 Color: 2

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 2
Size: 256 Color: 1
Size: 250 Color: 4

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 448 Color: 4
Size: 281 Color: 1
Size: 271 Color: 0

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 382 Color: 0
Size: 368 Color: 3
Size: 250 Color: 2

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 0
Size: 366 Color: 3
Size: 257 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 4
Size: 346 Color: 3
Size: 264 Color: 3

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 490 Color: 0
Size: 256 Color: 3
Size: 254 Color: 3

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 4
Size: 351 Color: 1
Size: 280 Color: 3

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 1
Size: 326 Color: 3
Size: 297 Color: 2

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 4
Size: 368 Color: 2
Size: 259 Color: 3

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 1
Size: 286 Color: 4
Size: 265 Color: 0

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 401 Color: 0
Size: 311 Color: 4
Size: 288 Color: 3

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 4
Size: 264 Color: 2
Size: 259 Color: 3

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 1
Size: 257 Color: 4
Size: 300 Color: 4

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 3
Size: 346 Color: 2
Size: 260 Color: 2

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 0
Size: 279 Color: 3
Size: 260 Color: 2

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 2
Size: 283 Color: 2
Size: 254 Color: 3

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 3
Size: 362 Color: 2
Size: 271 Color: 3

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 3
Size: 333 Color: 0
Size: 280 Color: 3

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 458 Color: 1
Size: 285 Color: 0
Size: 257 Color: 0

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 372 Color: 3
Size: 324 Color: 2
Size: 304 Color: 0

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 3
Size: 339 Color: 2
Size: 311 Color: 4

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 0
Size: 324 Color: 2
Size: 319 Color: 3

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 4
Size: 259 Color: 2
Size: 253 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 403 Color: 4
Size: 307 Color: 2
Size: 290 Color: 4

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 4
Size: 315 Color: 4
Size: 272 Color: 0

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 357 Color: 1
Size: 269 Color: 3

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 3
Size: 319 Color: 3
Size: 290 Color: 2

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 2
Size: 296 Color: 3
Size: 271 Color: 4

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 469 Color: 3
Size: 276 Color: 0
Size: 255 Color: 3

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 4
Size: 340 Color: 4
Size: 253 Color: 0

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 3
Size: 281 Color: 0
Size: 260 Color: 3

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 421 Color: 1
Size: 298 Color: 2
Size: 281 Color: 1

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 4
Size: 345 Color: 0
Size: 281 Color: 3

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 3
Size: 367 Color: 0
Size: 258 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 0
Size: 332 Color: 2
Size: 278 Color: 1

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 4
Size: 339 Color: 4
Size: 270 Color: 0

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 2
Size: 287 Color: 3
Size: 256 Color: 4

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 0
Size: 322 Color: 1
Size: 254 Color: 0

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 4
Size: 287 Color: 3
Size: 272 Color: 1

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 374 Color: 3
Size: 356 Color: 2
Size: 270 Color: 4

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 397 Color: 3
Size: 323 Color: 0
Size: 280 Color: 3

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 2
Size: 333 Color: 0
Size: 294 Color: 2

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 2
Size: 362 Color: 4
Size: 265 Color: 0

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 0
Size: 303 Color: 3
Size: 263 Color: 1

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 3
Size: 301 Color: 4
Size: 256 Color: 1

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 0
Size: 318 Color: 2
Size: 290 Color: 0

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 392 Color: 1
Size: 335 Color: 2
Size: 273 Color: 1

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 414 Color: 4
Size: 329 Color: 4
Size: 257 Color: 0

Total size: 83000
Total free space: 0

