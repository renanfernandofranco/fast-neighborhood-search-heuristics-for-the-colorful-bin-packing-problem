Capicity Bin: 7568
Lower Bound: 132

Bins used: 133
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 17
Items: 
Size: 666 Color: 19
Size: 654 Color: 9
Size: 548 Color: 18
Size: 544 Color: 12
Size: 536 Color: 1
Size: 528 Color: 11
Size: 472 Color: 18
Size: 472 Color: 9
Size: 472 Color: 3
Size: 468 Color: 11
Size: 464 Color: 12
Size: 464 Color: 6
Size: 464 Color: 1
Size: 300 Color: 4
Size: 260 Color: 8
Size: 132 Color: 14
Size: 124 Color: 19

Bin 2: 0 of cap free
Amount of items: 5
Items: 
Size: 3789 Color: 1
Size: 1793 Color: 8
Size: 1152 Color: 4
Size: 634 Color: 11
Size: 200 Color: 5

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 4306 Color: 12
Size: 2722 Color: 12
Size: 540 Color: 15

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 4788 Color: 0
Size: 2660 Color: 4
Size: 120 Color: 15

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 5083 Color: 12
Size: 2357 Color: 19
Size: 128 Color: 17

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 5090 Color: 15
Size: 2268 Color: 15
Size: 210 Color: 11

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 5879 Color: 9
Size: 1409 Color: 19
Size: 280 Color: 8

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 5990 Color: 10
Size: 1478 Color: 2
Size: 100 Color: 14

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 6041 Color: 17
Size: 1159 Color: 1
Size: 368 Color: 4

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 6172 Color: 7
Size: 1032 Color: 8
Size: 364 Color: 17

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 6174 Color: 16
Size: 1164 Color: 8
Size: 230 Color: 13

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 6188 Color: 10
Size: 1044 Color: 7
Size: 336 Color: 6

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 6191 Color: 8
Size: 1149 Color: 2
Size: 228 Color: 19

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 6192 Color: 16
Size: 1144 Color: 13
Size: 232 Color: 10

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 6301 Color: 17
Size: 951 Color: 0
Size: 316 Color: 6

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 6324 Color: 15
Size: 628 Color: 10
Size: 616 Color: 12

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 6338 Color: 8
Size: 1026 Color: 17
Size: 204 Color: 10

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 6340 Color: 9
Size: 1028 Color: 18
Size: 200 Color: 6

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 6341 Color: 16
Size: 1023 Color: 2
Size: 204 Color: 16

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 6426 Color: 13
Size: 822 Color: 1
Size: 320 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 6372 Color: 1
Size: 812 Color: 16
Size: 384 Color: 14

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 6474 Color: 13
Size: 1022 Color: 14
Size: 72 Color: 5

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 6460 Color: 0
Size: 652 Color: 3
Size: 456 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 6500 Color: 5
Size: 1004 Color: 13
Size: 64 Color: 4

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 6526 Color: 8
Size: 886 Color: 15
Size: 156 Color: 6

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 6574 Color: 13
Size: 764 Color: 19
Size: 230 Color: 19

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 6563 Color: 10
Size: 789 Color: 2
Size: 216 Color: 0

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 6572 Color: 18
Size: 828 Color: 17
Size: 168 Color: 10

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 6621 Color: 16
Size: 703 Color: 15
Size: 244 Color: 10

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 6622 Color: 15
Size: 706 Color: 7
Size: 240 Color: 13

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 6663 Color: 12
Size: 749 Color: 12
Size: 156 Color: 11

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 6668 Color: 6
Size: 544 Color: 6
Size: 356 Color: 17

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 6700 Color: 13
Size: 628 Color: 11
Size: 240 Color: 6

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 6697 Color: 15
Size: 727 Color: 7
Size: 144 Color: 6

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 6703 Color: 8
Size: 673 Color: 19
Size: 192 Color: 19

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 6754 Color: 13
Size: 674 Color: 4
Size: 140 Color: 1

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 6743 Color: 17
Size: 689 Color: 6
Size: 136 Color: 12

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 6757 Color: 19
Size: 677 Color: 1
Size: 134 Color: 9

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 6770 Color: 19
Size: 608 Color: 17
Size: 190 Color: 1

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 6773 Color: 8
Size: 663 Color: 2
Size: 132 Color: 0

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 6786 Color: 9
Size: 528 Color: 16
Size: 254 Color: 13

Bin 42: 1 of cap free
Amount of items: 3
Items: 
Size: 4696 Color: 13
Size: 2723 Color: 6
Size: 148 Color: 12

Bin 43: 1 of cap free
Amount of items: 3
Items: 
Size: 4727 Color: 10
Size: 2706 Color: 1
Size: 134 Color: 8

Bin 44: 1 of cap free
Amount of items: 3
Items: 
Size: 5095 Color: 10
Size: 2308 Color: 1
Size: 164 Color: 16

Bin 45: 1 of cap free
Amount of items: 3
Items: 
Size: 5381 Color: 16
Size: 2066 Color: 7
Size: 120 Color: 17

Bin 46: 1 of cap free
Amount of items: 3
Items: 
Size: 5659 Color: 17
Size: 1484 Color: 19
Size: 424 Color: 14

Bin 47: 1 of cap free
Amount of items: 3
Items: 
Size: 5667 Color: 12
Size: 1162 Color: 16
Size: 738 Color: 11

Bin 48: 1 of cap free
Amount of items: 3
Items: 
Size: 5726 Color: 18
Size: 1585 Color: 7
Size: 256 Color: 15

Bin 49: 1 of cap free
Amount of items: 2
Items: 
Size: 5758 Color: 14
Size: 1809 Color: 0

Bin 50: 1 of cap free
Amount of items: 3
Items: 
Size: 6049 Color: 16
Size: 836 Color: 13
Size: 682 Color: 10

Bin 51: 1 of cap free
Amount of items: 3
Items: 
Size: 6325 Color: 4
Size: 1006 Color: 9
Size: 236 Color: 13

Bin 52: 1 of cap free
Amount of items: 3
Items: 
Size: 6427 Color: 2
Size: 600 Color: 12
Size: 540 Color: 19

Bin 53: 1 of cap free
Amount of items: 3
Items: 
Size: 6482 Color: 14
Size: 829 Color: 12
Size: 256 Color: 17

Bin 54: 1 of cap free
Amount of items: 3
Items: 
Size: 6506 Color: 11
Size: 721 Color: 8
Size: 340 Color: 8

Bin 55: 1 of cap free
Amount of items: 3
Items: 
Size: 6573 Color: 16
Size: 676 Color: 13
Size: 318 Color: 7

Bin 56: 1 of cap free
Amount of items: 2
Items: 
Size: 6623 Color: 12
Size: 944 Color: 7

Bin 57: 1 of cap free
Amount of items: 3
Items: 
Size: 6761 Color: 7
Size: 630 Color: 11
Size: 176 Color: 13

Bin 58: 2 of cap free
Amount of items: 3
Items: 
Size: 4301 Color: 3
Size: 3153 Color: 13
Size: 112 Color: 11

Bin 59: 2 of cap free
Amount of items: 3
Items: 
Size: 5399 Color: 12
Size: 2071 Color: 14
Size: 96 Color: 7

Bin 60: 2 of cap free
Amount of items: 2
Items: 
Size: 5780 Color: 13
Size: 1786 Color: 19

Bin 61: 2 of cap free
Amount of items: 3
Items: 
Size: 5996 Color: 17
Size: 1442 Color: 13
Size: 128 Color: 10

Bin 62: 2 of cap free
Amount of items: 2
Items: 
Size: 6788 Color: 6
Size: 778 Color: 9

Bin 63: 3 of cap free
Amount of items: 3
Items: 
Size: 5091 Color: 6
Size: 2342 Color: 9
Size: 132 Color: 13

Bin 64: 3 of cap free
Amount of items: 3
Items: 
Size: 5586 Color: 13
Size: 1815 Color: 7
Size: 164 Color: 15

Bin 65: 3 of cap free
Amount of items: 3
Items: 
Size: 5798 Color: 12
Size: 1611 Color: 17
Size: 156 Color: 6

Bin 66: 3 of cap free
Amount of items: 3
Items: 
Size: 5806 Color: 13
Size: 1591 Color: 0
Size: 168 Color: 15

Bin 67: 3 of cap free
Amount of items: 2
Items: 
Size: 6810 Color: 17
Size: 755 Color: 14

Bin 68: 4 of cap free
Amount of items: 2
Items: 
Size: 5538 Color: 18
Size: 2026 Color: 17

Bin 69: 4 of cap free
Amount of items: 3
Items: 
Size: 5628 Color: 5
Size: 1786 Color: 9
Size: 150 Color: 13

Bin 70: 4 of cap free
Amount of items: 2
Items: 
Size: 6145 Color: 1
Size: 1419 Color: 3

Bin 71: 4 of cap free
Amount of items: 2
Items: 
Size: 6274 Color: 4
Size: 1290 Color: 2

Bin 72: 4 of cap free
Amount of items: 3
Items: 
Size: 6329 Color: 14
Size: 1187 Color: 15
Size: 48 Color: 6

Bin 73: 5 of cap free
Amount of items: 3
Items: 
Size: 6561 Color: 9
Size: 954 Color: 3
Size: 48 Color: 2

Bin 74: 5 of cap free
Amount of items: 2
Items: 
Size: 6575 Color: 19
Size: 988 Color: 14

Bin 75: 5 of cap free
Amount of items: 2
Items: 
Size: 6671 Color: 12
Size: 892 Color: 14

Bin 76: 6 of cap free
Amount of items: 3
Items: 
Size: 4274 Color: 14
Size: 3156 Color: 1
Size: 132 Color: 0

Bin 77: 6 of cap free
Amount of items: 3
Items: 
Size: 4722 Color: 6
Size: 2700 Color: 9
Size: 140 Color: 11

Bin 78: 6 of cap free
Amount of items: 2
Items: 
Size: 4852 Color: 8
Size: 2710 Color: 5

Bin 79: 6 of cap free
Amount of items: 2
Items: 
Size: 6114 Color: 9
Size: 1448 Color: 3

Bin 80: 7 of cap free
Amount of items: 3
Items: 
Size: 5220 Color: 13
Size: 1308 Color: 10
Size: 1033 Color: 16

Bin 81: 8 of cap free
Amount of items: 2
Items: 
Size: 6503 Color: 15
Size: 1057 Color: 7

Bin 82: 9 of cap free
Amount of items: 2
Items: 
Size: 6652 Color: 10
Size: 907 Color: 4

Bin 83: 10 of cap free
Amount of items: 2
Items: 
Size: 6596 Color: 10
Size: 962 Color: 19

Bin 84: 11 of cap free
Amount of items: 7
Items: 
Size: 3785 Color: 15
Size: 1151 Color: 5
Size: 949 Color: 2
Size: 724 Color: 13
Size: 624 Color: 4
Size: 164 Color: 11
Size: 160 Color: 14

Bin 85: 11 of cap free
Amount of items: 2
Items: 
Size: 6431 Color: 19
Size: 1126 Color: 0

Bin 86: 12 of cap free
Amount of items: 3
Items: 
Size: 5637 Color: 1
Size: 1823 Color: 2
Size: 96 Color: 8

Bin 87: 12 of cap free
Amount of items: 2
Items: 
Size: 6686 Color: 15
Size: 870 Color: 5

Bin 88: 13 of cap free
Amount of items: 2
Items: 
Size: 6725 Color: 5
Size: 830 Color: 7

Bin 89: 13 of cap free
Amount of items: 2
Items: 
Size: 6764 Color: 2
Size: 791 Color: 18

Bin 90: 14 of cap free
Amount of items: 2
Items: 
Size: 5180 Color: 11
Size: 2374 Color: 2

Bin 91: 15 of cap free
Amount of items: 5
Items: 
Size: 3786 Color: 10
Size: 1214 Color: 13
Size: 1156 Color: 13
Size: 1037 Color: 1
Size: 360 Color: 18

Bin 92: 15 of cap free
Amount of items: 2
Items: 
Size: 6142 Color: 16
Size: 1411 Color: 5

Bin 93: 15 of cap free
Amount of items: 3
Items: 
Size: 6187 Color: 8
Size: 1318 Color: 17
Size: 48 Color: 0

Bin 94: 15 of cap free
Amount of items: 2
Items: 
Size: 6722 Color: 17
Size: 831 Color: 5

Bin 95: 16 of cap free
Amount of items: 3
Items: 
Size: 5458 Color: 7
Size: 1942 Color: 8
Size: 152 Color: 15

Bin 96: 16 of cap free
Amount of items: 2
Items: 
Size: 6362 Color: 1
Size: 1190 Color: 8

Bin 97: 16 of cap free
Amount of items: 2
Items: 
Size: 6762 Color: 1
Size: 790 Color: 5

Bin 98: 18 of cap free
Amount of items: 2
Items: 
Size: 6040 Color: 16
Size: 1510 Color: 14

Bin 99: 21 of cap free
Amount of items: 2
Items: 
Size: 6501 Color: 12
Size: 1046 Color: 17

Bin 100: 22 of cap free
Amount of items: 3
Items: 
Size: 4330 Color: 15
Size: 3052 Color: 9
Size: 164 Color: 1

Bin 101: 22 of cap free
Amount of items: 2
Items: 
Size: 5138 Color: 14
Size: 2408 Color: 8

Bin 102: 22 of cap free
Amount of items: 2
Items: 
Size: 5838 Color: 17
Size: 1708 Color: 9

Bin 103: 23 of cap free
Amount of items: 2
Items: 
Size: 4729 Color: 13
Size: 2816 Color: 19

Bin 104: 25 of cap free
Amount of items: 3
Items: 
Size: 6179 Color: 12
Size: 1316 Color: 17
Size: 48 Color: 11

Bin 105: 25 of cap free
Amount of items: 2
Items: 
Size: 6270 Color: 0
Size: 1273 Color: 9

Bin 106: 26 of cap free
Amount of items: 3
Items: 
Size: 4804 Color: 2
Size: 2324 Color: 0
Size: 414 Color: 1

Bin 107: 28 of cap free
Amount of items: 3
Items: 
Size: 3908 Color: 3
Size: 3304 Color: 15
Size: 328 Color: 6

Bin 108: 28 of cap free
Amount of items: 2
Items: 
Size: 6458 Color: 10
Size: 1082 Color: 3

Bin 109: 29 of cap free
Amount of items: 2
Items: 
Size: 5391 Color: 18
Size: 2148 Color: 15

Bin 110: 29 of cap free
Amount of items: 2
Items: 
Size: 6582 Color: 11
Size: 957 Color: 5

Bin 111: 30 of cap free
Amount of items: 2
Items: 
Size: 4380 Color: 16
Size: 3158 Color: 14

Bin 112: 33 of cap free
Amount of items: 3
Items: 
Size: 4322 Color: 13
Size: 2322 Color: 11
Size: 891 Color: 13

Bin 113: 36 of cap free
Amount of items: 2
Items: 
Size: 3796 Color: 15
Size: 3736 Color: 2

Bin 114: 37 of cap free
Amount of items: 2
Items: 
Size: 5162 Color: 15
Size: 2369 Color: 2

Bin 115: 38 of cap free
Amount of items: 2
Items: 
Size: 5524 Color: 19
Size: 2006 Color: 10

Bin 116: 39 of cap free
Amount of items: 2
Items: 
Size: 5875 Color: 5
Size: 1654 Color: 14

Bin 117: 40 of cap free
Amount of items: 2
Items: 
Size: 4782 Color: 7
Size: 2746 Color: 2

Bin 118: 41 of cap free
Amount of items: 2
Items: 
Size: 6638 Color: 7
Size: 889 Color: 16

Bin 119: 42 of cap free
Amount of items: 26
Items: 
Size: 456 Color: 19
Size: 448 Color: 15
Size: 412 Color: 17
Size: 412 Color: 14
Size: 404 Color: 13
Size: 400 Color: 3
Size: 368 Color: 15
Size: 320 Color: 6
Size: 296 Color: 14
Size: 292 Color: 16
Size: 288 Color: 17
Size: 288 Color: 14
Size: 282 Color: 18
Size: 282 Color: 11
Size: 260 Color: 5
Size: 256 Color: 9
Size: 236 Color: 10
Size: 232 Color: 11
Size: 224 Color: 4
Size: 224 Color: 2
Size: 212 Color: 12
Size: 206 Color: 2
Size: 200 Color: 8
Size: 176 Color: 17
Size: 176 Color: 8
Size: 176 Color: 4

Bin 120: 44 of cap free
Amount of items: 2
Items: 
Size: 6022 Color: 9
Size: 1502 Color: 7

Bin 121: 65 of cap free
Amount of items: 2
Items: 
Size: 5643 Color: 16
Size: 1860 Color: 9

Bin 122: 66 of cap free
Amount of items: 2
Items: 
Size: 5796 Color: 4
Size: 1706 Color: 19

Bin 123: 72 of cap free
Amount of items: 3
Items: 
Size: 5340 Color: 0
Size: 1794 Color: 7
Size: 362 Color: 11

Bin 124: 72 of cap free
Amount of items: 2
Items: 
Size: 6004 Color: 11
Size: 1492 Color: 0

Bin 125: 81 of cap free
Amount of items: 2
Items: 
Size: 5426 Color: 1
Size: 2061 Color: 14

Bin 126: 81 of cap free
Amount of items: 2
Items: 
Size: 5867 Color: 1
Size: 1620 Color: 0

Bin 127: 82 of cap free
Amount of items: 2
Items: 
Size: 4332 Color: 4
Size: 3154 Color: 14

Bin 128: 82 of cap free
Amount of items: 2
Items: 
Size: 5522 Color: 18
Size: 1964 Color: 9

Bin 129: 85 of cap free
Amount of items: 2
Items: 
Size: 4758 Color: 7
Size: 2725 Color: 4

Bin 130: 100 of cap free
Amount of items: 3
Items: 
Size: 3794 Color: 3
Size: 2367 Color: 16
Size: 1307 Color: 12

Bin 131: 104 of cap free
Amount of items: 2
Items: 
Size: 3788 Color: 17
Size: 3676 Color: 1

Bin 132: 118 of cap free
Amount of items: 2
Items: 
Size: 4299 Color: 4
Size: 3151 Color: 6

Bin 133: 5534 of cap free
Amount of items: 13
Items: 
Size: 206 Color: 9
Size: 188 Color: 3
Size: 188 Color: 0
Size: 172 Color: 10
Size: 160 Color: 1
Size: 152 Color: 18
Size: 148 Color: 7
Size: 144 Color: 18
Size: 144 Color: 8
Size: 144 Color: 4
Size: 132 Color: 2
Size: 128 Color: 17
Size: 128 Color: 13

Total size: 998976
Total free space: 7568

