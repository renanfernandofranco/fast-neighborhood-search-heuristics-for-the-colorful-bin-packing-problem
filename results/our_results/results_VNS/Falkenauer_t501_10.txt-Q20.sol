Capicity Bin: 1000
Lower Bound: 167

Bins used: 167
Amount of Colors: 20

Bin 1: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 13
Size: 255 Color: 1
Size: 252 Color: 8

Bin 2: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 16
Size: 290 Color: 15
Size: 253 Color: 11

Bin 3: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 8
Size: 327 Color: 0
Size: 325 Color: 5

Bin 4: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 18
Size: 257 Color: 12
Size: 256 Color: 19

Bin 5: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 17
Size: 308 Color: 16
Size: 254 Color: 4

Bin 6: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 18
Size: 302 Color: 15
Size: 254 Color: 12

Bin 7: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 1
Size: 365 Color: 13
Size: 265 Color: 3

Bin 8: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 6
Size: 345 Color: 9
Size: 278 Color: 13

Bin 9: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 11
Size: 255 Color: 13
Size: 250 Color: 1

Bin 10: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 18
Size: 320 Color: 18
Size: 307 Color: 16

Bin 11: 0 of cap free
Amount of items: 3
Items: 
Size: 460 Color: 18
Size: 272 Color: 18
Size: 268 Color: 16

Bin 12: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 8
Size: 278 Color: 1
Size: 250 Color: 11

Bin 13: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 7
Size: 348 Color: 8
Size: 302 Color: 16

Bin 14: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 19
Size: 287 Color: 17
Size: 269 Color: 7

Bin 15: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 19
Size: 360 Color: 7
Size: 253 Color: 14

Bin 16: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 14
Size: 339 Color: 8
Size: 270 Color: 8

Bin 17: 0 of cap free
Amount of items: 3
Items: 
Size: 454 Color: 0
Size: 295 Color: 17
Size: 251 Color: 1

Bin 18: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 11
Size: 302 Color: 12
Size: 261 Color: 0

Bin 19: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 1
Size: 264 Color: 1
Size: 250 Color: 7

Bin 20: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 12
Size: 255 Color: 12
Size: 250 Color: 7

Bin 21: 0 of cap free
Amount of items: 3
Items: 
Size: 464 Color: 7
Size: 274 Color: 5
Size: 262 Color: 16

Bin 22: 0 of cap free
Amount of items: 3
Items: 
Size: 437 Color: 16
Size: 296 Color: 8
Size: 267 Color: 14

Bin 23: 0 of cap free
Amount of items: 3
Items: 
Size: 409 Color: 19
Size: 303 Color: 12
Size: 288 Color: 12

Bin 24: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 0
Size: 321 Color: 5
Size: 299 Color: 10

Bin 25: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 10
Size: 275 Color: 14
Size: 270 Color: 7

Bin 26: 0 of cap free
Amount of items: 3
Items: 
Size: 480 Color: 19
Size: 269 Color: 15
Size: 251 Color: 4

Bin 27: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 12
Size: 296 Color: 16
Size: 251 Color: 15

Bin 28: 0 of cap free
Amount of items: 3
Items: 
Size: 412 Color: 10
Size: 297 Color: 12
Size: 291 Color: 11

Bin 29: 0 of cap free
Amount of items: 3
Items: 
Size: 493 Color: 15
Size: 254 Color: 8
Size: 253 Color: 4

Bin 30: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 0
Size: 324 Color: 1
Size: 269 Color: 14

Bin 31: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 8
Size: 358 Color: 9
Size: 262 Color: 9

Bin 32: 0 of cap free
Amount of items: 3
Items: 
Size: 492 Color: 14
Size: 256 Color: 14
Size: 252 Color: 16

Bin 33: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 279 Color: 10
Size: 264 Color: 15

Bin 34: 0 of cap free
Amount of items: 3
Items: 
Size: 358 Color: 9
Size: 352 Color: 0
Size: 290 Color: 14

Bin 35: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 16
Size: 366 Color: 14
Size: 265 Color: 9

Bin 36: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 13
Size: 331 Color: 13
Size: 281 Color: 15

Bin 37: 0 of cap free
Amount of items: 3
Items: 
Size: 468 Color: 17
Size: 271 Color: 7
Size: 261 Color: 10

Bin 38: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 9
Size: 252 Color: 5
Size: 250 Color: 8

Bin 39: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 14
Size: 373 Color: 7
Size: 250 Color: 2

Bin 40: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 9
Size: 359 Color: 19
Size: 250 Color: 2

Bin 41: 0 of cap free
Amount of items: 3
Items: 
Size: 438 Color: 7
Size: 288 Color: 8
Size: 274 Color: 0

Bin 42: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 17
Size: 340 Color: 18
Size: 269 Color: 0

Bin 43: 0 of cap free
Amount of items: 3
Items: 
Size: 474 Color: 15
Size: 271 Color: 8
Size: 255 Color: 18

Bin 44: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 7
Size: 329 Color: 7
Size: 298 Color: 15

Bin 45: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 357 Color: 8
Size: 275 Color: 3

Bin 46: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 19
Size: 364 Color: 15
Size: 268 Color: 10

Bin 47: 0 of cap free
Amount of items: 3
Items: 
Size: 398 Color: 10
Size: 320 Color: 15
Size: 282 Color: 1

Bin 48: 0 of cap free
Amount of items: 3
Items: 
Size: 459 Color: 12
Size: 286 Color: 4
Size: 255 Color: 8

Bin 49: 0 of cap free
Amount of items: 3
Items: 
Size: 367 Color: 18
Size: 363 Color: 13
Size: 270 Color: 0

Bin 50: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 8
Size: 334 Color: 14
Size: 272 Color: 9

Bin 51: 0 of cap free
Amount of items: 3
Items: 
Size: 413 Color: 1
Size: 318 Color: 6
Size: 269 Color: 5

Bin 52: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 10
Size: 339 Color: 17
Size: 272 Color: 15

Bin 53: 0 of cap free
Amount of items: 3
Items: 
Size: 384 Color: 2
Size: 351 Color: 19
Size: 265 Color: 17

Bin 54: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 15
Size: 313 Color: 5
Size: 306 Color: 8

Bin 55: 0 of cap free
Amount of items: 3
Items: 
Size: 441 Color: 11
Size: 300 Color: 14
Size: 259 Color: 5

Bin 56: 0 of cap free
Amount of items: 3
Items: 
Size: 444 Color: 12
Size: 282 Color: 16
Size: 274 Color: 15

Bin 57: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 16
Size: 313 Color: 5
Size: 292 Color: 5

Bin 58: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 8
Size: 315 Color: 17
Size: 259 Color: 4

Bin 59: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 8
Size: 358 Color: 0
Size: 274 Color: 14

Bin 60: 0 of cap free
Amount of items: 3
Items: 
Size: 350 Color: 2
Size: 325 Color: 19
Size: 325 Color: 12

Bin 61: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 1
Size: 269 Color: 12
Size: 254 Color: 12

Bin 62: 0 of cap free
Amount of items: 3
Items: 
Size: 471 Color: 6
Size: 266 Color: 19
Size: 263 Color: 6

Bin 63: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 13
Size: 294 Color: 2
Size: 279 Color: 13

Bin 64: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 3
Size: 329 Color: 13
Size: 261 Color: 11

Bin 65: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 15
Size: 291 Color: 6
Size: 276 Color: 16

Bin 66: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 7
Size: 358 Color: 19
Size: 254 Color: 13

Bin 67: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 1
Size: 279 Color: 8
Size: 266 Color: 5

Bin 68: 0 of cap free
Amount of items: 3
Items: 
Size: 498 Color: 14
Size: 252 Color: 14
Size: 250 Color: 2

Bin 69: 0 of cap free
Amount of items: 3
Items: 
Size: 391 Color: 19
Size: 346 Color: 10
Size: 263 Color: 2

Bin 70: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 0
Size: 309 Color: 17
Size: 265 Color: 9

Bin 71: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 15
Size: 297 Color: 4
Size: 288 Color: 14

Bin 72: 0 of cap free
Amount of items: 3
Items: 
Size: 369 Color: 10
Size: 334 Color: 7
Size: 297 Color: 9

Bin 73: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 10
Size: 294 Color: 11
Size: 256 Color: 19

Bin 74: 0 of cap free
Amount of items: 3
Items: 
Size: 373 Color: 1
Size: 361 Color: 13
Size: 266 Color: 9

Bin 75: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 3
Size: 368 Color: 1
Size: 254 Color: 0

Bin 76: 0 of cap free
Amount of items: 3
Items: 
Size: 395 Color: 8
Size: 315 Color: 17
Size: 290 Color: 4

Bin 77: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 11
Size: 293 Color: 0
Size: 290 Color: 9

Bin 78: 0 of cap free
Amount of items: 3
Items: 
Size: 446 Color: 1
Size: 299 Color: 0
Size: 255 Color: 4

Bin 79: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 5
Size: 269 Color: 16
Size: 266 Color: 14

Bin 80: 0 of cap free
Amount of items: 3
Items: 
Size: 389 Color: 8
Size: 338 Color: 11
Size: 273 Color: 19

Bin 81: 0 of cap free
Amount of items: 3
Items: 
Size: 433 Color: 13
Size: 289 Color: 0
Size: 278 Color: 18

Bin 82: 0 of cap free
Amount of items: 3
Items: 
Size: 485 Color: 7
Size: 262 Color: 19
Size: 253 Color: 10

Bin 83: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 4
Size: 286 Color: 16
Size: 257 Color: 19

Bin 84: 0 of cap free
Amount of items: 3
Items: 
Size: 429 Color: 5
Size: 318 Color: 14
Size: 253 Color: 11

Bin 85: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 7
Size: 300 Color: 8
Size: 250 Color: 19

Bin 86: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 17
Size: 325 Color: 3
Size: 253 Color: 10

Bin 87: 0 of cap free
Amount of items: 3
Items: 
Size: 439 Color: 0
Size: 294 Color: 12
Size: 267 Color: 7

Bin 88: 0 of cap free
Amount of items: 3
Items: 
Size: 477 Color: 14
Size: 272 Color: 5
Size: 251 Color: 6

Bin 89: 0 of cap free
Amount of items: 3
Items: 
Size: 461 Color: 16
Size: 275 Color: 14
Size: 264 Color: 3

Bin 90: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 11
Size: 341 Color: 11
Size: 284 Color: 3

Bin 91: 0 of cap free
Amount of items: 3
Items: 
Size: 406 Color: 6
Size: 337 Color: 8
Size: 257 Color: 9

Bin 92: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 14
Size: 293 Color: 2
Size: 251 Color: 11

Bin 93: 0 of cap free
Amount of items: 3
Items: 
Size: 375 Color: 9
Size: 374 Color: 0
Size: 251 Color: 6

Bin 94: 0 of cap free
Amount of items: 3
Items: 
Size: 420 Color: 17
Size: 324 Color: 9
Size: 256 Color: 7

Bin 95: 0 of cap free
Amount of items: 3
Items: 
Size: 330 Color: 8
Size: 284 Color: 2
Size: 386 Color: 3

Bin 96: 0 of cap free
Amount of items: 3
Items: 
Size: 466 Color: 5
Size: 282 Color: 12
Size: 252 Color: 12

Bin 97: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 14
Size: 277 Color: 9
Size: 250 Color: 6

Bin 98: 0 of cap free
Amount of items: 3
Items: 
Size: 436 Color: 8
Size: 312 Color: 3
Size: 252 Color: 18

Bin 99: 0 of cap free
Amount of items: 3
Items: 
Size: 415 Color: 14
Size: 302 Color: 7
Size: 283 Color: 16

Bin 100: 0 of cap free
Amount of items: 3
Items: 
Size: 428 Color: 17
Size: 298 Color: 7
Size: 274 Color: 7

Bin 101: 0 of cap free
Amount of items: 3
Items: 
Size: 418 Color: 5
Size: 329 Color: 6
Size: 253 Color: 10

Bin 102: 0 of cap free
Amount of items: 3
Items: 
Size: 449 Color: 2
Size: 296 Color: 6
Size: 255 Color: 11

Bin 103: 0 of cap free
Amount of items: 3
Items: 
Size: 452 Color: 9
Size: 276 Color: 13
Size: 272 Color: 10

Bin 104: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 1
Size: 318 Color: 2
Size: 265 Color: 14

Bin 105: 0 of cap free
Amount of items: 3
Items: 
Size: 463 Color: 13
Size: 275 Color: 3
Size: 262 Color: 3

Bin 106: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 6
Size: 276 Color: 19
Size: 254 Color: 3

Bin 107: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 11
Size: 368 Color: 14
Size: 253 Color: 0

Bin 108: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 3
Size: 274 Color: 8
Size: 256 Color: 13

Bin 109: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 11
Size: 292 Color: 2
Size: 265 Color: 13

Bin 110: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 19
Size: 337 Color: 11
Size: 275 Color: 13

Bin 111: 0 of cap free
Amount of items: 3
Items: 
Size: 497 Color: 4
Size: 253 Color: 15
Size: 250 Color: 7

Bin 112: 0 of cap free
Amount of items: 3
Items: 
Size: 399 Color: 2
Size: 310 Color: 2
Size: 291 Color: 0

Bin 113: 0 of cap free
Amount of items: 3
Items: 
Size: 380 Color: 5
Size: 354 Color: 1
Size: 266 Color: 7

Bin 114: 0 of cap free
Amount of items: 3
Items: 
Size: 407 Color: 10
Size: 327 Color: 12
Size: 266 Color: 9

Bin 115: 0 of cap free
Amount of items: 3
Items: 
Size: 378 Color: 2
Size: 317 Color: 13
Size: 305 Color: 5

Bin 116: 0 of cap free
Amount of items: 3
Items: 
Size: 453 Color: 1
Size: 286 Color: 3
Size: 261 Color: 2

Bin 117: 0 of cap free
Amount of items: 3
Items: 
Size: 434 Color: 2
Size: 308 Color: 11
Size: 258 Color: 3

Bin 118: 0 of cap free
Amount of items: 3
Items: 
Size: 494 Color: 19
Size: 256 Color: 16
Size: 250 Color: 14

Bin 119: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 3
Size: 317 Color: 4
Size: 315 Color: 7

Bin 120: 0 of cap free
Amount of items: 3
Items: 
Size: 371 Color: 6
Size: 317 Color: 19
Size: 312 Color: 14

Bin 121: 0 of cap free
Amount of items: 3
Items: 
Size: 390 Color: 1
Size: 352 Color: 9
Size: 258 Color: 5

Bin 122: 0 of cap free
Amount of items: 3
Items: 
Size: 424 Color: 12
Size: 294 Color: 7
Size: 282 Color: 2

Bin 123: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 268 Color: 9
Size: 267 Color: 7

Bin 124: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 6
Size: 325 Color: 17
Size: 287 Color: 5

Bin 125: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 4
Size: 287 Color: 18
Size: 258 Color: 5

Bin 126: 0 of cap free
Amount of items: 3
Items: 
Size: 465 Color: 2
Size: 269 Color: 19
Size: 266 Color: 15

Bin 127: 0 of cap free
Amount of items: 3
Items: 
Size: 495 Color: 1
Size: 255 Color: 12
Size: 250 Color: 11

Bin 128: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 12
Size: 369 Color: 15
Size: 252 Color: 16

Bin 129: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 6
Size: 280 Color: 8
Size: 265 Color: 19

Bin 130: 0 of cap free
Amount of items: 3
Items: 
Size: 361 Color: 9
Size: 356 Color: 17
Size: 283 Color: 10

Bin 131: 0 of cap free
Amount of items: 3
Items: 
Size: 388 Color: 14
Size: 322 Color: 9
Size: 290 Color: 4

Bin 132: 0 of cap free
Amount of items: 3
Items: 
Size: 488 Color: 17
Size: 261 Color: 13
Size: 251 Color: 12

Bin 133: 0 of cap free
Amount of items: 3
Items: 
Size: 394 Color: 19
Size: 346 Color: 8
Size: 260 Color: 8

Bin 134: 0 of cap free
Amount of items: 3
Items: 
Size: 484 Color: 6
Size: 263 Color: 16
Size: 253 Color: 1

Bin 135: 0 of cap free
Amount of items: 3
Items: 
Size: 377 Color: 3
Size: 350 Color: 5
Size: 273 Color: 0

Bin 136: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 16
Size: 286 Color: 19
Size: 257 Color: 5

Bin 137: 0 of cap free
Amount of items: 3
Items: 
Size: 456 Color: 12
Size: 287 Color: 7
Size: 257 Color: 4

Bin 138: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 0
Size: 332 Color: 12
Size: 298 Color: 4

Bin 139: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 15
Size: 350 Color: 8
Size: 280 Color: 6

Bin 140: 0 of cap free
Amount of items: 3
Items: 
Size: 457 Color: 1
Size: 284 Color: 1
Size: 259 Color: 7

Bin 141: 0 of cap free
Amount of items: 3
Items: 
Size: 443 Color: 7
Size: 298 Color: 6
Size: 259 Color: 17

Bin 142: 0 of cap free
Amount of items: 3
Items: 
Size: 473 Color: 3
Size: 266 Color: 15
Size: 261 Color: 10

Bin 143: 0 of cap free
Amount of items: 3
Items: 
Size: 445 Color: 1
Size: 297 Color: 0
Size: 258 Color: 3

Bin 144: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 6
Size: 338 Color: 15
Size: 305 Color: 18

Bin 145: 0 of cap free
Amount of items: 3
Items: 
Size: 393 Color: 5
Size: 345 Color: 2
Size: 262 Color: 2

Bin 146: 0 of cap free
Amount of items: 3
Items: 
Size: 426 Color: 11
Size: 299 Color: 10
Size: 275 Color: 17

Bin 147: 0 of cap free
Amount of items: 3
Items: 
Size: 357 Color: 5
Size: 355 Color: 12
Size: 288 Color: 17

Bin 148: 0 of cap free
Amount of items: 3
Items: 
Size: 368 Color: 6
Size: 352 Color: 5
Size: 280 Color: 4

Bin 149: 0 of cap free
Amount of items: 3
Items: 
Size: 370 Color: 9
Size: 349 Color: 6
Size: 281 Color: 7

Bin 150: 0 of cap free
Amount of items: 3
Items: 
Size: 379 Color: 10
Size: 353 Color: 14
Size: 268 Color: 17

Bin 151: 0 of cap free
Amount of items: 3
Items: 
Size: 381 Color: 12
Size: 352 Color: 15
Size: 267 Color: 1

Bin 152: 0 of cap free
Amount of items: 3
Items: 
Size: 387 Color: 0
Size: 315 Color: 15
Size: 298 Color: 18

Bin 153: 0 of cap free
Amount of items: 3
Items: 
Size: 385 Color: 18
Size: 354 Color: 10
Size: 261 Color: 17

Bin 154: 0 of cap free
Amount of items: 3
Items: 
Size: 348 Color: 1
Size: 262 Color: 6
Size: 390 Color: 2

Bin 155: 0 of cap free
Amount of items: 3
Items: 
Size: 410 Color: 6
Size: 335 Color: 3
Size: 255 Color: 8

Bin 156: 0 of cap free
Amount of items: 3
Items: 
Size: 417 Color: 5
Size: 295 Color: 2
Size: 288 Color: 15

Bin 157: 0 of cap free
Amount of items: 3
Items: 
Size: 422 Color: 2
Size: 292 Color: 17
Size: 286 Color: 3

Bin 158: 0 of cap free
Amount of items: 3
Items: 
Size: 427 Color: 10
Size: 300 Color: 1
Size: 273 Color: 9

Bin 159: 0 of cap free
Amount of items: 3
Items: 
Size: 435 Color: 7
Size: 306 Color: 2
Size: 259 Color: 12

Bin 160: 0 of cap free
Amount of items: 3
Items: 
Size: 472 Color: 6
Size: 266 Color: 6
Size: 262 Color: 16

Bin 161: 0 of cap free
Amount of items: 3
Items: 
Size: 450 Color: 18
Size: 290 Color: 7
Size: 260 Color: 14

Bin 162: 0 of cap free
Amount of items: 3
Items: 
Size: 455 Color: 7
Size: 287 Color: 3
Size: 258 Color: 14

Bin 163: 0 of cap free
Amount of items: 3
Items: 
Size: 470 Color: 9
Size: 266 Color: 18
Size: 264 Color: 19

Bin 164: 0 of cap free
Amount of items: 3
Items: 
Size: 476 Color: 17
Size: 272 Color: 1
Size: 252 Color: 14

Bin 165: 0 of cap free
Amount of items: 3
Items: 
Size: 479 Color: 16
Size: 264 Color: 17
Size: 257 Color: 4

Bin 166: 0 of cap free
Amount of items: 3
Items: 
Size: 486 Color: 3
Size: 263 Color: 11
Size: 251 Color: 3

Bin 167: 0 of cap free
Amount of items: 3
Items: 
Size: 487 Color: 14
Size: 260 Color: 4
Size: 253 Color: 19

Total size: 167000
Total free space: 0

